package b.a.a;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class a$i
  implements a.s
{
  private final File a = new File(System.getProperty("java.io.tmpdir"));
  private final List<a.r> b;
  
  public a$i()
  {
    if (!a.exists()) {
      a.mkdirs();
    }
    b = new ArrayList();
  }
  
  public final void a()
  {
    Iterator localIterator = b.iterator();
    while (localIterator.hasNext())
    {
      a.r localr = (a.r)localIterator.next();
      try
      {
        localr.a();
      }
      catch (Exception localException)
      {
        a.c().log(Level.WARNING, "could not delete file ", localException);
      }
    }
    b.clear();
  }
  
  public final a.r b()
    throws Exception
  {
    a.h localh = new a.h(a);
    b.add(localh);
    return localh;
  }
}

/* Location:
 * Qualified Name:     b.a.a.a.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */