package b.a.a;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

final class a$n$a
  extends FilterOutputStream
{
  public a$n$a(OutputStream paramOutputStream)
  {
    super(paramOutputStream);
  }
  
  public final void a()
    throws IOException
  {
    out.write("0\r\n\r\n".getBytes());
  }
  
  public final void write(int paramInt)
    throws IOException
  {
    write(new byte[] { (byte)paramInt }, 0, 1);
  }
  
  public final void write(byte[] paramArrayOfByte)
    throws IOException
  {
    write(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public final void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if (paramInt2 == 0) {
      return;
    }
    out.write(String.format("%x\r\n", new Object[] { Integer.valueOf(paramInt2) }).getBytes());
    out.write(paramArrayOfByte, paramInt1, paramInt2);
    out.write("\r\n".getBytes());
  }
}

/* Location:
 * Qualified Name:     b.a.a.a.n.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */