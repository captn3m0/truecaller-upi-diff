package b.a.a;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class a$p
  implements Runnable
{
  private final int b = 5000;
  private IOException c;
  private boolean d = false;
  
  public a$p(a parama) {}
  
  public final void run()
  {
    try
    {
      Object localObject2 = a.d(a);
      Object localObject1;
      if (a.b(a) != null) {
        localObject1 = new InetSocketAddress(a.b(a), a.c(a));
      } else {
        localObject1 = new InetSocketAddress(a.c(a));
      }
      ((ServerSocket)localObject2).bind((SocketAddress)localObject1);
      d = true;
      do
      {
        try
        {
          localObject1 = a.d(a).accept();
          if (b > 0) {
            ((Socket)localObject1).setSoTimeout(b);
          }
          localObject2 = ((Socket)localObject1).getInputStream();
          a.c.b(new a.b(a, (InputStream)localObject2, (Socket)localObject1));
        }
        catch (IOException localIOException1)
        {
          a.c().log(Level.FINE, "Communication with the client broken", localIOException1);
        }
      } while (!a.d(a).isClosed());
      return;
    }
    catch (IOException localIOException2)
    {
      c = localIOException2;
    }
  }
}

/* Location:
 * Qualified Name:     b.a.a.a.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */