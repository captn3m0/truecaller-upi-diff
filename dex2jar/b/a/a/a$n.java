package b.a.a;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPOutputStream;

public final class a$n
  implements Closeable
{
  String a;
  a.m b;
  boolean c;
  boolean d;
  private b e;
  private InputStream f;
  private long g;
  private final Map<String, String> h = new HashMap() {};
  private final Map<String, String> i = new HashMap();
  private boolean j;
  
  protected a$n(b paramb, String paramString, InputStream paramInputStream, long paramLong)
  {
    e = paramb;
    a = paramString;
    boolean bool = false;
    if (paramInputStream == null)
    {
      f = new ByteArrayInputStream(new byte[0]);
      g = 0L;
    }
    else
    {
      f = paramInputStream;
      g = paramLong;
    }
    if (g < 0L) {
      bool = true;
    }
    j = bool;
    d = true;
  }
  
  private long a(PrintWriter paramPrintWriter, long paramLong)
  {
    Object localObject = a("content-length");
    long l = paramLong;
    if (localObject != null) {}
    try
    {
      l = Long.parseLong((String)localObject);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      for (;;) {}
    }
    a.c().severe("content-length was no number ".concat(String.valueOf(localObject)));
    l = paramLong;
    localObject = new StringBuilder("Content-Length: ");
    ((StringBuilder)localObject).append(l);
    ((StringBuilder)localObject).append("\r\n");
    paramPrintWriter.print(((StringBuilder)localObject).toString());
    return l;
  }
  
  private void a(OutputStream paramOutputStream, long paramLong)
    throws IOException
  {
    if (c)
    {
      paramOutputStream = new GZIPOutputStream(paramOutputStream);
      b(paramOutputStream, -1L);
      paramOutputStream.finish();
      return;
    }
    b(paramOutputStream, paramLong);
  }
  
  private static void a(PrintWriter paramPrintWriter, String paramString1, String paramString2)
  {
    paramPrintWriter.append(paramString1).append(": ").append(paramString2).append("\r\n");
  }
  
  private void b(OutputStream paramOutputStream, long paramLong)
    throws IOException
  {
    byte[] arrayOfByte = new byte['䀀'];
    int k;
    if (paramLong == -1L) {
      k = 1;
    } else {
      k = 0;
    }
    while ((paramLong > 0L) || (k != 0))
    {
      long l = 16384L;
      if (k == 0) {
        l = Math.min(paramLong, 16384L);
      }
      int m = f.read(arrayOfByte, 0, (int)l);
      if (m <= 0) {
        break;
      }
      paramOutputStream.write(arrayOfByte, 0, m);
      if (k == 0) {
        paramLong -= m;
      }
    }
  }
  
  public final String a(String paramString)
  {
    return (String)i.get(paramString.toLowerCase());
  }
  
  protected final void a(OutputStream paramOutputStream)
  {
    Object localObject = new SimpleDateFormat("E, d MMM yyyy HH:mm:ss 'GMT'", Locale.US);
    ((SimpleDateFormat)localObject).setTimeZone(TimeZone.getTimeZone("GMT"));
    for (;;)
    {
      try
      {
        if (e != null)
        {
          PrintWriter localPrintWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(paramOutputStream, new a.c(a).a())), false);
          localPrintWriter.append("HTTP/1.1 ").append(e.a()).append(" \r\n");
          if (a != null) {
            a(localPrintWriter, "Content-Type", a);
          }
          if (a("date") == null) {
            a(localPrintWriter, "Date", ((SimpleDateFormat)localObject).format(new Date()));
          }
          localObject = h.entrySet().iterator();
          if (((Iterator)localObject).hasNext())
          {
            Map.Entry localEntry = (Map.Entry)((Iterator)localObject).next();
            a(localPrintWriter, (String)localEntry.getKey(), (String)localEntry.getValue());
            continue;
          }
          if (a("connection") == null)
          {
            if (d)
            {
              localObject = "keep-alive";
              a(localPrintWriter, "Connection", (String)localObject);
            }
          }
          else
          {
            if (a("content-length") != null) {
              c = false;
            }
            if (c)
            {
              a(localPrintWriter, "Content-Encoding", "gzip");
              j = true;
            }
            if (f == null) {
              break label457;
            }
            l1 = g;
            long l2;
            if ((b != a.m.e) && (j))
            {
              a(localPrintWriter, "Transfer-Encoding", "chunked");
              l2 = l1;
            }
            else
            {
              l2 = l1;
              if (!c) {
                l2 = a(localPrintWriter, l1);
              }
            }
            localPrintWriter.append("\r\n");
            localPrintWriter.flush();
            if ((b != a.m.e) && (j))
            {
              localObject = new a(paramOutputStream);
              a((OutputStream)localObject, -1L);
              ((a)localObject).a();
            }
            else
            {
              a(paramOutputStream, l2);
            }
            paramOutputStream.flush();
            a.a(f);
          }
        }
        else
        {
          throw new Error("sendResponse(): Status can't be null.");
        }
      }
      catch (IOException paramOutputStream)
      {
        a.c().log(Level.SEVERE, "Could not send response to the client", paramOutputStream);
        return;
      }
      localObject = "close";
      continue;
      label457:
      long l1 = 0L;
    }
  }
  
  public final void a(String paramString1, String paramString2)
  {
    h.put(paramString1, paramString2);
  }
  
  public final void close()
    throws IOException
  {
    InputStream localInputStream = f;
    if (localInputStream != null) {
      localInputStream.close();
    }
  }
  
  static final class a
    extends FilterOutputStream
  {
    public a(OutputStream paramOutputStream)
    {
      super();
    }
    
    public final void a()
      throws IOException
    {
      out.write("0\r\n\r\n".getBytes());
    }
    
    public final void write(int paramInt)
      throws IOException
    {
      write(new byte[] { (byte)paramInt }, 0, 1);
    }
    
    public final void write(byte[] paramArrayOfByte)
      throws IOException
    {
      write(paramArrayOfByte, 0, paramArrayOfByte.length);
    }
    
    public final void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
      throws IOException
    {
      if (paramInt2 == 0) {
        return;
      }
      out.write(String.format("%x\r\n", new Object[] { Integer.valueOf(paramInt2) }).getBytes());
      out.write(paramArrayOfByte, paramInt1, paramInt2);
      out.write("\r\n".getBytes());
    }
  }
  
  public static abstract interface b
  {
    public abstract String a();
  }
  
  public static enum c
    implements a.n.b
  {
    private final int G;
    private final String H;
    
    private c(int paramInt, String paramString)
    {
      G = paramInt;
      H = paramString;
    }
    
    public final String a()
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(G);
      localStringBuilder.append(" ");
      localStringBuilder.append(H);
      return localStringBuilder.toString();
    }
  }
}

/* Location:
 * Qualified Name:     b.a.a.a.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */