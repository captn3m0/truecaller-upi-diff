package b.a.a;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public final class a$e
  implements Iterable<String>
{
  private final HashMap<String, String> b = new HashMap();
  private final ArrayList<a.d> c = new ArrayList();
  
  public a$e(Map<String, String> paramMap)
  {
    String[] arrayOfString;
    paramMap = (String)arrayOfString.get("cookie");
    if (paramMap != null)
    {
      paramMap = paramMap.split(";");
      int j = paramMap.length;
      int i = 0;
      while (i < j)
      {
        arrayOfString = paramMap[i].trim().split("=");
        if (arrayOfString.length == 2) {
          b.put(arrayOfString[0], arrayOfString[1]);
        }
        i += 1;
      }
    }
  }
  
  public final void a(a.n paramn)
  {
    Iterator localIterator = c.iterator();
    while (localIterator.hasNext())
    {
      a.d locald = (a.d)localIterator.next();
      paramn.a("Set-Cookie", String.format("%s=%s; expires=%s", new Object[] { a, b, c }));
    }
  }
  
  public final Iterator<String> iterator()
  {
    return b.keySet().iterator();
  }
}

/* Location:
 * Qualified Name:     b.a.a.a.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */