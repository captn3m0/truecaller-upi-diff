package b.a.a;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class a$c
{
  private static final Pattern e = Pattern.compile("[ |\t]*([^/^ ^;^,]+/[^ ^;^,]+)", 2);
  private static final Pattern f = Pattern.compile("[ |\t]*(charset)[ |\t]*=[ |\t]*['|\"]?([^\"^'^;^,]*)['|\"]?", 2);
  private static final Pattern g = Pattern.compile("[ |\t]*(boundary)[ |\t]*=[ |\t]*['|\"]?([^\"^'^;^,]*)['|\"]?", 2);
  final String a;
  final String b;
  final String c;
  final String d;
  
  public a$c(String paramString)
  {
    a = paramString;
    if (paramString != null)
    {
      b = a(paramString, e, "", 1);
      c = a(paramString, f, null, 2);
    }
    else
    {
      b = "";
      c = "UTF-8";
    }
    if ("multipart/form-data".equalsIgnoreCase(b))
    {
      d = a(paramString, g, null, 2);
      return;
    }
    d = null;
  }
  
  private static String a(String paramString1, Pattern paramPattern, String paramString2, int paramInt)
  {
    paramString1 = paramPattern.matcher(paramString1);
    if (paramString1.find()) {
      return paramString1.group(paramInt);
    }
    return paramString2;
  }
  
  public final String a()
  {
    String str2 = c;
    String str1 = str2;
    if (str2 == null) {
      str1 = "US-ASCII";
    }
    return str1;
  }
}

/* Location:
 * Qualified Name:     b.a.a.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */