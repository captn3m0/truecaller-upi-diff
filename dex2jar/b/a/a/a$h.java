package b.a.a;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public final class a$h
  implements a.r
{
  private final File a;
  private final OutputStream b;
  
  public a$h(File paramFile)
    throws IOException
  {
    a = File.createTempFile("NanoHTTPD-", "", paramFile);
    b = new FileOutputStream(a);
  }
  
  public final void a()
    throws Exception
  {
    a.a(b);
    if (a.delete()) {
      return;
    }
    StringBuilder localStringBuilder = new StringBuilder("could not delete temporary file: ");
    localStringBuilder.append(a.getAbsolutePath());
    throw new Exception(localStringBuilder.toString());
  }
  
  public final String b()
  {
    return a.getAbsolutePath();
  }
}

/* Location:
 * Qualified Name:     b.a.a.a.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */