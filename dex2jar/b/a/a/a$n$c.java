package b.a.a;

public enum a$n$c
  implements a.n.b
{
  private final int G;
  private final String H;
  
  private a$n$c(int paramInt, String paramString)
  {
    G = paramInt;
    H = paramString;
  }
  
  public final String a()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(G);
    localStringBuilder.append(" ");
    localStringBuilder.append(H);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     b.a.a.a.n.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */