package b.a.a;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class a$f
  implements a.a
{
  private long a;
  private final List<a.b> b = Collections.synchronizedList(new ArrayList());
  
  public final void a()
  {
    Iterator localIterator = new ArrayList(b).iterator();
    while (localIterator.hasNext())
    {
      a.b localb = (a.b)localIterator.next();
      a.a(a);
      a.a(b);
    }
  }
  
  public final void a(a.b paramb)
  {
    b.remove(paramb);
  }
  
  public final void b(a.b paramb)
  {
    a += 1L;
    Thread localThread = new Thread(paramb);
    localThread.setDaemon(true);
    StringBuilder localStringBuilder = new StringBuilder("NanoHttpd Request Processor (#");
    localStringBuilder.append(a);
    localStringBuilder.append(")");
    localThread.setName(localStringBuilder.toString());
    b.add(paramb);
    localThread.start();
  }
}

/* Location:
 * Qualified Name:     b.a.a.a.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */