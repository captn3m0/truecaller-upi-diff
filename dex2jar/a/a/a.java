package a.a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public final class a
  implements Serializable, Comparable<a>
{
  private static int k = 2400000;
  public Integer a;
  public Integer b;
  private String c;
  private Integer d;
  private Integer e;
  private Integer f;
  private Integer g;
  private Integer h;
  private boolean i = true;
  private int j;
  
  public a(Integer paramInteger1, Integer paramInteger2, Integer paramInteger3, Integer paramInteger4, Integer paramInteger5, Integer paramInteger6, Integer paramInteger7)
  {
    a = paramInteger1;
    b = paramInteger2;
    d = paramInteger3;
    e = paramInteger4;
    f = paramInteger5;
    g = paramInteger6;
    h = paramInteger7;
    n();
  }
  
  public static a a(TimeZone paramTimeZone)
  {
    long l = System.currentTimeMillis();
    paramTimeZone = new GregorianCalendar(paramTimeZone);
    paramTimeZone.setTimeInMillis(l);
    paramTimeZone = new a(Integer.valueOf(paramTimeZone.get(1)), Integer.valueOf(paramTimeZone.get(2) + 1), Integer.valueOf(paramTimeZone.get(5)), Integer.valueOf(paramTimeZone.get(11)), Integer.valueOf(paramTimeZone.get(12)), Integer.valueOf(paramTimeZone.get(13)), Integer.valueOf(paramTimeZone.get(14) * 1000 * 1000));
    d locald = d.c;
    paramTimeZone.l();
    if (d.g != locald)
    {
      if (d.f == locald) {
        return new a(a, b, d, e, f, g, null);
      }
      if (d.e == locald) {
        return new a(a, b, d, e, f, null, null);
      }
      if (d.d == locald) {
        return new a(a, b, d, e, null, null, null);
      }
      if (d.c == locald) {
        return new a(a, b, d, null, null, null, null);
      }
      if (d.b == locald) {
        return new a(a, b, null, null, null, null, null);
      }
      if (d.a == locald) {
        return new a(a, null, null, null, null, null, null);
      }
      return null;
    }
    throw new IllegalArgumentException("It makes no sense to truncate to nanosecond precision, since that's the highest precision available.");
  }
  
  static Integer a(Integer paramInteger1, Integer paramInteger2)
  {
    if ((paramInteger1 != null) && (paramInteger2 != null))
    {
      if (paramInteger2.intValue() == 1) {}
      do
      {
        do
        {
          return Integer.valueOf(31);
          if (paramInteger2.intValue() == 2)
          {
            int m;
            if (b(paramInteger1)) {
              m = 29;
            } else {
              m = 28;
            }
            return Integer.valueOf(m);
          }
        } while (paramInteger2.intValue() == 3);
        if (paramInteger2.intValue() == 4) {}
        do
        {
          do
          {
            do
            {
              return Integer.valueOf(30);
              if (paramInteger2.intValue() == 5) {
                break;
              }
            } while (paramInteger2.intValue() == 6);
            if ((paramInteger2.intValue() == 7) || (paramInteger2.intValue() == 8)) {
              break;
            }
          } while (paramInteger2.intValue() == 9);
          if (paramInteger2.intValue() == 10) {
            break;
          }
        } while (paramInteger2.intValue() == 11);
      } while (paramInteger2.intValue() == 12);
      throw new AssertionError("Month is out of range 1..12:".concat(String.valueOf(paramInteger2)));
    }
    return null;
  }
  
  private static void a(Integer paramInteger, int paramInt1, int paramInt2, String paramString)
  {
    if (paramInteger != null)
    {
      if ((paramInteger.intValue() >= paramInt1) && (paramInteger.intValue() <= paramInt2)) {
        return;
      }
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramString);
      localStringBuilder.append(" is not in the range ");
      localStringBuilder.append(paramInt1);
      localStringBuilder.append("..");
      localStringBuilder.append(paramInt2);
      localStringBuilder.append(". Value is:");
      localStringBuilder.append(paramInteger);
      throw new b(localStringBuilder.toString());
    }
  }
  
  private static void a(Integer paramInteger1, Integer paramInteger2, Integer paramInteger3)
  {
    if (b(paramInteger1, paramInteger2, paramInteger3))
    {
      if (paramInteger3.intValue() <= a(paramInteger1, paramInteger2).intValue()) {
        return;
      }
      StringBuilder localStringBuilder = new StringBuilder("The day-of-the-month value '");
      localStringBuilder.append(paramInteger3);
      localStringBuilder.append("' exceeds the number of days in the month: ");
      localStringBuilder.append(a(paramInteger1, paramInteger2));
      throw new b(localStringBuilder.toString());
    }
  }
  
  private static void a(String paramString, Object paramObject, StringBuilder paramStringBuilder)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString);
    localStringBuilder.append(":");
    localStringBuilder.append(String.valueOf(paramObject));
    localStringBuilder.append(" ");
    paramStringBuilder.append(localStringBuilder.toString());
  }
  
  private static boolean a(Object... paramVarArgs)
  {
    int m = 0;
    while (m < 3)
    {
      if (paramVarArgs[m] == null) {
        return false;
      }
      m += 1;
    }
    return true;
  }
  
  private static boolean b(Integer paramInteger)
  {
    if (paramInteger.intValue() % 100 == 0)
    {
      if (paramInteger.intValue() % 400 == 0) {
        return true;
      }
    }
    else if (paramInteger.intValue() % 4 == 0) {
      return true;
    }
    return false;
  }
  
  private static boolean b(Integer paramInteger1, Integer paramInteger2, Integer paramInteger3)
  {
    return a(new Object[] { paramInteger1, paramInteger2, paramInteger3 });
  }
  
  private int c(a parama)
  {
    if (this == parama) {
      return 0;
    }
    l();
    parama.l();
    e.a locala = e.a.a;
    int m = e.a(a, a, locala);
    if (m != 0) {
      return m;
    }
    m = e.a(b, b, locala);
    if (m != 0) {
      return m;
    }
    m = e.a(d, d, locala);
    if (m != 0) {
      return m;
    }
    m = e.a(e, e, locala);
    if (m != 0) {
      return m;
    }
    m = e.a(f, f, locala);
    if (m != 0) {
      return m;
    }
    m = e.a(g, g, locala);
    if (m != 0) {
      return m;
    }
    m = e.a(h, h, locala);
    if (m != 0) {
      return m;
    }
    return 0;
  }
  
  private boolean k()
  {
    return a(new d[] { d.a, d.b, d.c });
  }
  
  private void l()
  {
    if (!i) {
      o();
    }
  }
  
  private int m()
  {
    int m = a.intValue();
    int n = b.intValue();
    int i1 = d.intValue();
    int i2 = (n - 14) / 12;
    return (m + 4800 + i2) * 1461 / 4 + (n - 2 - i2 * 12) * 367 / 12 - (m + 4900 + i2) / 100 * 3 / 4 + i1 - 32075;
  }
  
  private void n()
  {
    a(a, 1, 9999, "Year");
    a(b, 1, 12, "Month");
    a(d, 1, 31, "Day");
    a(e, 0, 23, "Hour");
    a(f, 0, 59, "Minute");
    a(g, 0, 59, "Second");
    a(h, 0, 999999999, "Nanosecond");
    a(a, b, d);
  }
  
  private void o()
  {
    a locala = new d().a(c);
    a = a;
    b = b;
    d = d;
    e = e;
    f = f;
    g = g;
    h = h;
    n();
  }
  
  private Object[] p()
  {
    return new Object[] { a, b, d, e, f, g, h };
  }
  
  private String q()
  {
    if (a(new d[] { d.a })) {
      if (b(new d[] { d.b, d.c, d.d, d.e, d.f, d.g })) {
        return "YYYY";
      }
    }
    if (a(new d[] { d.a, d.b })) {
      if (b(new d[] { d.c, d.d, d.e, d.f, d.g })) {
        return "YYYY-MM";
      }
    }
    if (a(new d[] { d.a, d.b, d.c })) {
      if (b(new d[] { d.d, d.e, d.f, d.g })) {
        return "YYYY-MM-DD";
      }
    }
    if (a(new d[] { d.a, d.b, d.c, d.d })) {
      if (b(new d[] { d.e, d.f, d.g })) {
        return "YYYY-MM-DD hh";
      }
    }
    if (a(new d[] { d.a, d.b, d.c, d.d, d.e })) {
      if (b(new d[] { d.f, d.g })) {
        return "YYYY-MM-DD hh:mm";
      }
    }
    if (a(new d[] { d.a, d.b, d.c, d.d, d.e, d.f })) {
      if (b(new d[] { d.g })) {
        return "YYYY-MM-DD hh:mm:ss";
      }
    }
    if (a(new d[] { d.a, d.b, d.c, d.d, d.e, d.f, d.g })) {
      return "YYYY-MM-DD hh:mm:ss.fffffffff";
    }
    if (b(new d[] { d.a, d.b, d.c })) {
      if (a(new d[] { d.d, d.e, d.f, d.g })) {
        return "hh:mm:ss.fffffffff";
      }
    }
    if (b(new d[] { d.a, d.b, d.c, d.g })) {
      if (a(new d[] { d.d, d.e, d.f })) {
        return "hh:mm:ss";
      }
    }
    if (b(new d[] { d.a, d.b, d.c, d.f, d.g })) {
      if (a(new d[] { d.d, d.e })) {
        return "hh:mm";
      }
    }
    return null;
  }
  
  public final a a(Integer paramInteger)
  {
    j();
    j();
    int m = m();
    int n = k;
    n = m - 1 - n + 1 + n + paramInteger.intValue() + 68569;
    m = n * 4 / 146097;
    int i1 = n - (146097 * m + 3) / 4;
    n = (i1 + 1) * 4000 / 1461001;
    i1 = i1 - n * 1461 / 4 + 31;
    int i2 = i1 * 80 / 2447;
    int i3 = i2 * 2447 / 80;
    int i4 = i2 / 11;
    paramInteger = new a(Integer.valueOf((m - 49) * 100 + n + i4), Integer.valueOf(i2 + 2 - i4 * 12), Integer.valueOf(i1 - i3), null, null, null, null);
    return new a(paramInteger.a(), paramInteger.b(), paramInteger.c(), e, f, g, h);
  }
  
  public final a a(Integer paramInteger1, Integer paramInteger2, Integer paramInteger3, Integer paramInteger4, Integer paramInteger5, Integer paramInteger6, Integer paramInteger7, a parama)
  {
    return new c(this, parama).a(true, Integer.valueOf(paramInteger1.intValue()), Integer.valueOf(paramInteger2.intValue()), Integer.valueOf(paramInteger3.intValue()), Integer.valueOf(paramInteger4.intValue()), Integer.valueOf(paramInteger5.intValue()), Integer.valueOf(paramInteger6.intValue()), Integer.valueOf(paramInteger7.intValue()));
  }
  
  public final Integer a()
  {
    l();
    return a;
  }
  
  public final String a(String paramString)
  {
    paramString = new b(paramString);
    b = new ArrayList();
    a = new ArrayList();
    paramString.a();
    paramString.a(this);
    return paramString.b();
  }
  
  public final boolean a(a parama)
  {
    return c(parama) < 0;
  }
  
  public final boolean a(d... paramVarArgs)
  {
    l();
    int n = paramVarArgs.length;
    int m = 0;
    boolean bool1;
    for (boolean bool2 = true; m < n; bool2 = bool1)
    {
      d locald = paramVarArgs[m];
      if (d.g == locald) {
        if ((!bool2) || (h == null)) {}
      }
      for (;;)
      {
        bool1 = true;
        break;
        label100:
        label123:
        label146:
        label169:
        do
        {
          do
          {
            do
            {
              do
              {
                do
                {
                  do
                  {
                    bool1 = false;
                    break label196;
                    if (d.f != locald) {
                      break;
                    }
                  } while ((!bool2) || (g == null));
                  break;
                  if (d.e != locald) {
                    break label100;
                  }
                } while ((!bool2) || (f == null));
                break;
                if (d.d != locald) {
                  break label123;
                }
              } while ((!bool2) || (e == null));
              break;
              if (d.c != locald) {
                break label146;
              }
            } while ((!bool2) || (d == null));
            break;
            if (d.b != locald) {
              break label169;
            }
          } while ((!bool2) || (b == null));
          break;
          bool1 = bool2;
          if (d.a != locald) {
            break label196;
          }
        } while ((!bool2) || (a == null));
      }
      label196:
      m += 1;
    }
    return bool2;
  }
  
  public final a b(Integer paramInteger1, Integer paramInteger2, Integer paramInteger3, Integer paramInteger4, Integer paramInteger5, Integer paramInteger6, Integer paramInteger7, a parama)
  {
    return new c(this, parama).a(false, Integer.valueOf(paramInteger1.intValue()), Integer.valueOf(paramInteger2.intValue()), Integer.valueOf(paramInteger3.intValue()), Integer.valueOf(paramInteger4.intValue()), Integer.valueOf(paramInteger5.intValue()), Integer.valueOf(paramInteger6.intValue()), Integer.valueOf(paramInteger7.intValue()));
  }
  
  public final Integer b()
  {
    l();
    return b;
  }
  
  public final boolean b(a parama)
  {
    return c(parama) > 0;
  }
  
  public final boolean b(d... paramVarArgs)
  {
    l();
    int n = paramVarArgs.length;
    int m = 0;
    boolean bool1;
    for (boolean bool2 = true; m < n; bool2 = bool1)
    {
      d locald = paramVarArgs[m];
      if (d.g == locald) {
        if ((!bool2) || (h != null)) {}
      }
      for (;;)
      {
        bool1 = true;
        break;
        label100:
        label123:
        label146:
        label169:
        do
        {
          do
          {
            do
            {
              do
              {
                do
                {
                  do
                  {
                    bool1 = false;
                    break label196;
                    if (d.f != locald) {
                      break;
                    }
                  } while ((!bool2) || (g != null));
                  break;
                  if (d.e != locald) {
                    break label100;
                  }
                } while ((!bool2) || (f != null));
                break;
                if (d.d != locald) {
                  break label123;
                }
              } while ((!bool2) || (e != null));
              break;
              if (d.c != locald) {
                break label146;
              }
            } while ((!bool2) || (d != null));
            break;
            if (d.b != locald) {
              break label169;
            }
          } while ((!bool2) || (b != null));
          break;
          bool1 = bool2;
          if (d.a != locald) {
            break label196;
          }
        } while ((!bool2) || (a != null));
      }
      label196:
      m += 1;
    }
    return bool2;
  }
  
  public final Integer c()
  {
    l();
    return d;
  }
  
  public final Integer d()
  {
    l();
    return e;
  }
  
  public final Integer e()
  {
    l();
    return f;
  }
  
  public final boolean equals(Object paramObject)
  {
    l();
    Object localObject1;
    if (this == paramObject) {
      localObject1 = Boolean.TRUE;
    } else if (!getClass().isInstance(paramObject)) {
      localObject1 = Boolean.FALSE;
    } else {
      localObject1 = null;
    }
    Object localObject2 = localObject1;
    if (localObject1 == null)
    {
      localObject1 = (a)paramObject;
      ((a)localObject1).l();
      paramObject = p();
      localObject1 = ((a)localObject1).p();
      boolean bool2 = false;
      int m = 0;
      while (m < 7)
      {
        localObject2 = paramObject[m];
        Object localObject3 = localObject1[m];
        if ((!e.a(localObject2)) && (!e.a(localObject3)))
        {
          if (localObject2 == null)
          {
            if (localObject3 == null) {
              bool1 = true;
            } else {
              bool1 = false;
            }
          }
          else {
            bool1 = localObject2.equals(localObject3);
          }
          if (!bool1)
          {
            bool1 = bool2;
            break label167;
          }
          m += 1;
        }
        else
        {
          throw new IllegalArgumentException("This method does not currently support arrays.");
        }
      }
      boolean bool1 = true;
      label167:
      localObject2 = Boolean.valueOf(bool1);
    }
    return ((Boolean)localObject2).booleanValue();
  }
  
  public final Integer f()
  {
    l();
    return g;
  }
  
  public final Integer g()
  {
    l();
    return h;
  }
  
  public final Integer h()
  {
    j();
    return Integer.valueOf((m() + 1) % 7 + 1);
  }
  
  public final int hashCode()
  {
    if (j == 0)
    {
      l();
      Object[] arrayOfObject = p();
      int n = 23;
      int m = 0;
      while (m < 7)
      {
        n = e.a(n, arrayOfObject[m]);
        m += 1;
      }
      j = n;
    }
    return j;
  }
  
  public final int i()
  {
    j();
    return a(a, b).intValue();
  }
  
  public final void j()
  {
    l();
    if (k()) {
      return;
    }
    throw new c("DateTime does not include year/month/day.");
  }
  
  public final String toString()
  {
    if (f.a(c)) {
      return c;
    }
    if (q() != null) {
      return a(q());
    }
    StringBuilder localStringBuilder = new StringBuilder();
    a("Y", a, localStringBuilder);
    a("M", b, localStringBuilder);
    a("D", d, localStringBuilder);
    a("h", e, localStringBuilder);
    a("m", f, localStringBuilder);
    a("s", g, localStringBuilder);
    a("f", h, localStringBuilder);
    return localStringBuilder.toString().trim();
  }
  
  public static enum a
  {
    private a() {}
  }
  
  static final class b
    extends RuntimeException
  {
    b(String paramString)
    {
      super();
    }
  }
  
  static final class c
    extends RuntimeException
  {
    c(String paramString)
    {
      super();
    }
  }
  
  public static enum d
  {
    private d() {}
  }
}

/* Location:
 * Qualified Name:     a.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */