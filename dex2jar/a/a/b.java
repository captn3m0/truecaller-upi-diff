package a.a;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class b
{
  private static final Pattern i = Pattern.compile("\\|[^\\|]*\\|");
  private static final Pattern j = Pattern.compile("f{1,9}");
  private static final List<String> k;
  Collection<c> a;
  Collection<b> b;
  private final String c;
  private final Locale d;
  private final Map<Locale, List<String>> e = new LinkedHashMap();
  private final Map<Locale, List<String>> f = new LinkedHashMap();
  private final Map<Locale, List<String>> g = new LinkedHashMap();
  private final a h;
  
  static
  {
    ArrayList localArrayList = new ArrayList();
    k = localArrayList;
    localArrayList.add("YYYY");
    k.add("YY");
    k.add("MMMM");
    k.add("MMM");
    k.add("MM");
    k.add("M");
    k.add("DD");
    k.add("D");
    k.add("WWWW");
    k.add("WWW");
    k.add("hh12");
    k.add("h12");
    k.add("hh");
    k.add("h");
    k.add("mm");
    k.add("m");
    k.add("ss");
    k.add("s");
    k.add("a");
    k.add("fffffffff");
    k.add("ffffffff");
    k.add("fffffff");
    k.add("ffffff");
    k.add("fffff");
    k.add("ffff");
    k.add("fff");
    k.add("ff");
    k.add("f");
  }
  
  b(String paramString)
  {
    c = paramString;
    d = null;
    h = null;
    if (f.a(c)) {
      return;
    }
    throw new IllegalArgumentException("DateTime format has no content.");
  }
  
  private c a(int paramInt)
  {
    Iterator localIterator = a.iterator();
    Object localObject = null;
    while (localIterator.hasNext())
    {
      c localc = (c)localIterator.next();
      if (a == paramInt) {
        localObject = localc;
      }
    }
    return (c)localObject;
  }
  
  private static String a(Integer paramInteger)
  {
    for (paramInteger = a(paramInteger); paramInteger.length() < 9; paramInteger = "0".concat(String.valueOf(paramInteger))) {}
    return paramInteger;
  }
  
  private static String a(Object paramObject)
  {
    String str = "";
    if (paramObject != null) {
      str = String.valueOf(paramObject);
    }
    return str;
  }
  
  private static String a(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    int m = 1;
    while (m <= paramString.length())
    {
      localStringBuilder.append("@");
      m += 1;
    }
    return localStringBuilder.toString();
  }
  
  private String b(Integer paramInteger)
  {
    if (paramInteger != null)
    {
      if (h != null) {
        return c(paramInteger);
      }
      if (d != null) {
        return d(paramInteger);
      }
      paramInteger = new StringBuilder("Your date pattern requires either a Locale, or your own custom localizations for text:");
      paramInteger.append(f.a(c));
      throw new IllegalArgumentException(paramInteger.toString());
    }
    return "";
  }
  
  private static String b(String paramString)
  {
    String str = paramString;
    if (f.a(paramString))
    {
      str = paramString;
      if (paramString.length() == 1) {
        str = "0".concat(String.valueOf(paramString));
      }
    }
    return str;
  }
  
  private String c(Integer paramInteger)
  {
    return (String)h.a.get(paramInteger.intValue() - 1);
  }
  
  private static String c(String paramString)
  {
    String str = paramString;
    if (f.a(paramString))
    {
      str = paramString;
      if (paramString.length() >= 3) {
        str = paramString.substring(0, 3);
      }
    }
    return str;
  }
  
  private String d(Integer paramInteger)
  {
    if (!e.containsKey(d))
    {
      ArrayList localArrayList = new ArrayList();
      SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("MMMM", d);
      int m = 0;
      while (m <= 11)
      {
        GregorianCalendar localGregorianCalendar = new GregorianCalendar();
        localGregorianCalendar.set(1, 2000);
        localGregorianCalendar.set(2, m);
        localGregorianCalendar.set(5, 15);
        localArrayList.add(localSimpleDateFormat.format(localGregorianCalendar.getTime()));
        m += 1;
      }
      e.put(d, localArrayList);
    }
    return (String)((List)e.get(d)).get(paramInteger.intValue() - 1);
  }
  
  private String e(Integer paramInteger)
  {
    if (paramInteger != null)
    {
      if (h != null) {
        return f(paramInteger);
      }
      if (d != null) {
        return g(paramInteger);
      }
      paramInteger = new StringBuilder("Your date pattern requires either a Locale, or your own custom localizations for text:");
      paramInteger.append(f.a(c));
      throw new IllegalArgumentException(paramInteger.toString());
    }
    return "";
  }
  
  private String f(Integer paramInteger)
  {
    return (String)h.b.get(paramInteger.intValue() - 1);
  }
  
  private String g(Integer paramInteger)
  {
    if (!f.containsKey(d))
    {
      ArrayList localArrayList = new ArrayList();
      SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("EEEE", d);
      int m = 8;
      while (m <= 14)
      {
        GregorianCalendar localGregorianCalendar = new GregorianCalendar();
        localGregorianCalendar.set(1, 2009);
        localGregorianCalendar.set(2, 1);
        localGregorianCalendar.set(5, m);
        localArrayList.add(localSimpleDateFormat.format(localGregorianCalendar.getTime()));
        m += 1;
      }
      f.put(d, localArrayList);
    }
    return (String)((List)f.get(d)).get(paramInteger.intValue() - 1);
  }
  
  private static Integer h(Integer paramInteger)
  {
    Integer localInteger = paramInteger;
    if (paramInteger != null)
    {
      if (paramInteger.intValue() == 0) {
        return Integer.valueOf(12);
      }
      localInteger = paramInteger;
      if (paramInteger.intValue() > 12) {
        localInteger = Integer.valueOf(paramInteger.intValue() - 12);
      }
    }
    return localInteger;
  }
  
  private String i(Integer paramInteger)
  {
    SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("a", d);
    GregorianCalendar localGregorianCalendar = new GregorianCalendar();
    localGregorianCalendar.set(1, 2000);
    localGregorianCalendar.set(2, 6);
    localGregorianCalendar.set(5, 15);
    localGregorianCalendar.set(11, paramInteger.intValue());
    return localSimpleDateFormat.format(localGregorianCalendar.getTime());
  }
  
  final void a()
  {
    Matcher localMatcher = i.matcher(c);
    while (localMatcher.find())
    {
      b localb = new b((byte)0);
      a = localMatcher.start();
      b = (localMatcher.end() - 1);
      b.add(localb);
    }
  }
  
  final void a(a parama)
  {
    String str1 = c;
    Iterator localIterator = k.iterator();
    while (localIterator.hasNext())
    {
      String str2 = (String)localIterator.next();
      Matcher localMatcher = Pattern.compile(str2).matcher(str1);
      while (localMatcher.find())
      {
        c localc = new c((byte)0);
        a = localMatcher.start();
        b = (localMatcher.end() - 1);
        Object localObject1 = b.iterator();
        Object localObject2;
        while (((Iterator)localObject1).hasNext())
        {
          localObject2 = (b)((Iterator)localObject1).next();
          if ((a <= a) && (a <= b))
          {
            m = 1;
            break label153;
          }
        }
        int m = 0;
        label153:
        if (m == 0)
        {
          localObject2 = localMatcher.group();
          if ("YYYY".equals(localObject2)) {
            localObject1 = parama.a();
          }
          for (;;)
          {
            localObject1 = a(localObject1);
            break label907;
            if ("YY".equals(localObject2))
            {
              localObject1 = a(parama.a());
              if (f.a((String)localObject1))
              {
                localObject1 = ((String)localObject1).substring(2);
                break label907;
              }
              localObject1 = "";
              break label907;
            }
            if ("MMMM".equals(localObject2))
            {
              localObject1 = b(Integer.valueOf(parama.b().intValue()));
              break label907;
            }
            if ("MMM".equals(localObject2))
            {
              localObject1 = b(Integer.valueOf(parama.b().intValue()));
              label280:
              localObject1 = c((String)localObject1);
              break label907;
            }
            if ("MM".equals(localObject2)) {
              localObject1 = parama.b();
            }
            for (;;)
            {
              localObject1 = b(a(localObject1));
              break label907;
              if ("M".equals(localObject2))
              {
                localObject1 = parama.b();
                break;
              }
              if ("DD".equals(localObject2))
              {
                localObject1 = parama.c();
              }
              else
              {
                if ("D".equals(localObject2))
                {
                  localObject1 = parama.c();
                  break;
                }
                if ("WWWW".equals(localObject2))
                {
                  localObject1 = e(Integer.valueOf(parama.h().intValue()));
                  break label907;
                }
                if ("WWW".equals(localObject2))
                {
                  localObject1 = e(Integer.valueOf(parama.h().intValue()));
                  break label280;
                }
                if ("hh".equals(localObject2))
                {
                  localObject1 = parama.d();
                }
                else
                {
                  if ("h".equals(localObject2))
                  {
                    localObject1 = parama.d();
                    break;
                  }
                  if ("h12".equals(localObject2))
                  {
                    localObject1 = h(parama.d());
                    break;
                  }
                  if ("hh12".equals(localObject2))
                  {
                    localObject1 = h(parama.d());
                  }
                  else
                  {
                    if ("a".equals(localObject2))
                    {
                      localObject1 = Integer.valueOf(parama.d().intValue());
                      if (localObject1 != null)
                      {
                        if (h != null)
                        {
                          if (((Integer)localObject1).intValue() < 12) {
                            localObject1 = h.c.get(0);
                          } else {
                            localObject1 = h.c.get(1);
                          }
                          localObject1 = (String)localObject1;
                          break label907;
                        }
                        localObject2 = d;
                        if (localObject2 != null)
                        {
                          if (!g.containsKey(localObject2))
                          {
                            localObject2 = new ArrayList();
                            ((List)localObject2).add(i(Integer.valueOf(6)));
                            ((List)localObject2).add(i(Integer.valueOf(18)));
                            g.put(d, localObject2);
                          }
                          if (((Integer)localObject1).intValue() < 12) {
                            localObject1 = ((List)g.get(d)).get(0);
                          } else {
                            localObject1 = ((List)g.get(d)).get(1);
                          }
                          localObject1 = (String)localObject1;
                          break label907;
                        }
                        parama = new StringBuilder("Your date pattern requires either a Locale, or your own custom localizations for text:");
                        parama.append(f.a(c));
                        throw new IllegalArgumentException(parama.toString());
                      }
                      localObject1 = "";
                      break label907;
                    }
                    if ("mm".equals(localObject2))
                    {
                      localObject1 = parama.e();
                    }
                    else
                    {
                      if ("m".equals(localObject2))
                      {
                        localObject1 = parama.e();
                        break;
                      }
                      if (!"ss".equals(localObject2)) {
                        break label826;
                      }
                      localObject1 = parama.f();
                    }
                  }
                }
              }
            }
            label826:
            if (!"s".equals(localObject2)) {
              break;
            }
            localObject1 = parama.f();
          }
          if (((String)localObject2).startsWith("f"))
          {
            if (j.matcher((CharSequence)localObject2).matches())
            {
              localObject1 = a(parama.g());
              m = ((String)localObject2).length();
              if ((f.a((String)localObject1)) && (((String)localObject1).length() >= m)) {
                localObject1 = ((String)localObject1).substring(0, m);
              }
              label907:
              c = ((String)localObject1);
              a.add(localc);
            }
            else
            {
              throw new IllegalArgumentException("Unknown token in date formatting pattern: ".concat(String.valueOf(localObject2)));
            }
          }
          else {
            throw new IllegalArgumentException("Unknown token in date formatting pattern: ".concat(String.valueOf(localObject2)));
          }
        }
      }
      str1 = str1.replace(str2, a(str2));
    }
  }
  
  final String b()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    int n;
    for (int m = 0; m < c.length(); m = n + 1)
    {
      String str = c.substring(m, m + 1);
      c localc = a(m);
      if (localc != null)
      {
        localStringBuilder.append(c);
        n = b;
      }
      else
      {
        n = m;
        if (!"|".equals(str))
        {
          localStringBuilder.append(str);
          n = m;
        }
      }
    }
    return localStringBuilder.toString();
  }
  
  final class a
  {
    List<String> a;
    List<String> b;
    List<String> c;
  }
  
  static final class b
  {
    int a;
    int b;
  }
  
  static final class c
  {
    int a;
    int b;
    String c;
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder("Start:");
      localStringBuilder.append(a);
      localStringBuilder.append(" End:");
      localStringBuilder.append(b);
      localStringBuilder.append(" '");
      localStringBuilder.append(c);
      localStringBuilder.append("'");
      return localStringBuilder.toString();
    }
  }
}

/* Location:
 * Qualified Name:     a.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */