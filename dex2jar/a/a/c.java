package a.a;

final class c
{
  private final a a;
  private boolean b;
  private a.a c;
  private int d;
  private int e;
  private int f;
  private int g;
  private int h;
  private int i;
  private int j;
  private Integer k;
  private Integer l;
  private Integer m;
  private Integer n;
  private Integer o;
  private Integer p;
  private Integer q;
  
  c(a parama, a.a parama1)
  {
    a = parama;
    parama = a;
    a.d locald1 = a.d.a;
    int i2 = 0;
    a.d locald2 = a.d.b;
    int i3 = 1;
    if (parama.a(new a.d[] { locald1, locald2, a.d.c, a.d.d, a.d.e, a.d.f })) {}
    do
    {
      do
      {
        i1 = 1;
        break label217;
        if (!a.a(new a.d[] { a.d.a, a.d.b, a.d.c })) {
          break;
        }
      } while (a.b(new a.d[] { a.d.d, a.d.e, a.d.f }));
      if (!a.b(new a.d[] { a.d.a, a.d.b, a.d.c })) {
        break;
      }
    } while (a.a(new a.d[] { a.d.d, a.d.e, a.d.f }));
    int i1 = 0;
    label217:
    if (i1 != 0)
    {
      if (a.a() == null) {
        i1 = 1;
      } else {
        i1 = a.a().intValue();
      }
      k = Integer.valueOf(i1);
      if (a.b() == null) {
        i1 = 1;
      } else {
        i1 = a.b().intValue();
      }
      l = Integer.valueOf(i1);
      if (a.c() == null) {
        i1 = i3;
      } else {
        i1 = a.c().intValue();
      }
      m = Integer.valueOf(i1);
      if (a.d() == null) {
        i1 = 0;
      } else {
        i1 = a.d().intValue();
      }
      n = Integer.valueOf(i1);
      if (a.e() == null) {
        i1 = 0;
      } else {
        i1 = a.e().intValue();
      }
      o = Integer.valueOf(i1);
      if (a.f() == null) {
        i1 = 0;
      } else {
        i1 = a.f().intValue();
      }
      p = Integer.valueOf(i1);
      if (a.g() == null) {
        i1 = i2;
      } else {
        i1 = a.g().intValue();
      }
      q = Integer.valueOf(i1);
      c = parama1;
      return;
    }
    throw new IllegalArgumentException("For interval calculations, DateTime must have year-month-day, or hour-minute-second, or both.");
  }
  
  private void a()
  {
    if (b) {}
    for (int i1 = k.intValue() + d;; i1 = a.a().intValue() - d)
    {
      k = Integer.valueOf(i1);
      return;
    }
  }
  
  private static void a(Integer paramInteger)
  {
    if ((paramInteger.intValue() >= 0) && (paramInteger.intValue() <= 999999999)) {
      return;
    }
    throw new IllegalArgumentException("Nanosecond interval is not in the range 0..999999999");
  }
  
  private static void a(Integer paramInteger, String paramString)
  {
    if ((paramInteger.intValue() >= 0) && (paramInteger.intValue() <= 9999)) {
      return;
    }
    paramInteger = new StringBuilder();
    paramInteger.append(paramString);
    paramInteger.append(" is not in the range 0..9999");
    throw new IllegalArgumentException(paramInteger.toString());
  }
  
  private void b()
  {
    int i1 = 0;
    while (i1 < e)
    {
      i();
      i1 += 1;
    }
  }
  
  private void c()
  {
    int i1 = 0;
    while (i1 < f)
    {
      j();
      i1 += 1;
    }
  }
  
  private void d()
  {
    int i1 = 0;
    while (i1 < g)
    {
      m();
      i1 += 1;
    }
  }
  
  private void e()
  {
    int i1 = 0;
    while (i1 < h)
    {
      n();
      i1 += 1;
    }
  }
  
  private void f()
  {
    int i1 = 0;
    while (i1 < i)
    {
      o();
      i1 += 1;
    }
  }
  
  private void g()
  {
    int i1;
    if (b) {
      i1 = q.intValue() + j;
    } else {
      i1 = q.intValue() - j;
    }
    q = Integer.valueOf(i1);
    if (q.intValue() > 999999999)
    {
      o();
      q = Integer.valueOf(q.intValue() - 999999999 - 1);
      return;
    }
    if (q.intValue() < 0)
    {
      o();
      q = Integer.valueOf(q.intValue() + 999999999 + 1);
    }
  }
  
  private void h()
  {
    if (b) {}
    for (int i1 = k.intValue() + 1;; i1 = k.intValue() - 1)
    {
      k = Integer.valueOf(i1);
      return;
    }
  }
  
  private void i()
  {
    int i1;
    if (b) {
      i1 = l.intValue() + 1;
    } else {
      i1 = l.intValue() - 1;
    }
    l = Integer.valueOf(i1);
    if (l.intValue() > 12)
    {
      l = Integer.valueOf(1);
      h();
      return;
    }
    if (l.intValue() <= 0)
    {
      l = Integer.valueOf(12);
      h();
    }
  }
  
  private void j()
  {
    int i1;
    if (b) {
      i1 = m.intValue() + 1;
    } else {
      i1 = m.intValue() - 1;
    }
    m = Integer.valueOf(i1);
    if (m.intValue() > k())
    {
      m = Integer.valueOf(1);
      i();
      return;
    }
    if (m.intValue() <= 0)
    {
      m = Integer.valueOf(l());
      i();
    }
  }
  
  private int k()
  {
    return a.a(k, l).intValue();
  }
  
  private int l()
  {
    Integer localInteger1;
    Integer localInteger2;
    if (l.intValue() > 1)
    {
      localInteger1 = k;
      localInteger2 = Integer.valueOf(l.intValue() - 1);
    }
    else
    {
      localInteger1 = Integer.valueOf(k.intValue() - 1);
      localInteger2 = Integer.valueOf(12);
    }
    return a.a(localInteger1, localInteger2).intValue();
  }
  
  private void m()
  {
    int i1;
    if (b) {
      i1 = n.intValue() + 1;
    } else {
      i1 = n.intValue() - 1;
    }
    n = Integer.valueOf(i1);
    if (n.intValue() > 23)
    {
      n = Integer.valueOf(0);
      j();
      return;
    }
    if (n.intValue() < 0)
    {
      n = Integer.valueOf(23);
      j();
    }
  }
  
  private void n()
  {
    int i1;
    if (b) {
      i1 = o.intValue() + 1;
    } else {
      i1 = o.intValue() - 1;
    }
    o = Integer.valueOf(i1);
    if (o.intValue() > 59)
    {
      o = Integer.valueOf(0);
      m();
      return;
    }
    if (o.intValue() < 0)
    {
      o = Integer.valueOf(59);
      m();
    }
  }
  
  private void o()
  {
    int i1;
    if (b) {
      i1 = p.intValue() + 1;
    } else {
      i1 = p.intValue() - 1;
    }
    p = Integer.valueOf(i1);
    if (p.intValue() > 59)
    {
      p = Integer.valueOf(0);
      n();
      return;
    }
    if (p.intValue() < 0)
    {
      p = Integer.valueOf(59);
      n();
    }
  }
  
  private void p()
  {
    int i1 = k();
    if (m.intValue() > i1) {
      if (a.a.d != c)
      {
        if (a.a.b == c)
        {
          m = Integer.valueOf(1);
          i();
          return;
        }
        if (a.a.a == c)
        {
          m = Integer.valueOf(i1);
          return;
        }
        if (a.a.c == c)
        {
          m = Integer.valueOf(m.intValue() - i1);
          i();
        }
      }
      else
      {
        StringBuilder localStringBuilder = new StringBuilder("Day Overflow: Year:");
        localStringBuilder.append(k);
        localStringBuilder.append(" Month:");
        localStringBuilder.append(l);
        localStringBuilder.append(" has ");
        localStringBuilder.append(i1);
        localStringBuilder.append(" days, but day has value:");
        localStringBuilder.append(m);
        localStringBuilder.append(" To avoid these exceptions, please specify a different DayOverflow policy.");
        throw new RuntimeException(localStringBuilder.toString());
      }
    }
  }
  
  final a a(boolean paramBoolean, Integer paramInteger1, Integer paramInteger2, Integer paramInteger3, Integer paramInteger4, Integer paramInteger5, Integer paramInteger6, Integer paramInteger7)
  {
    b = paramBoolean;
    d = paramInteger1.intValue();
    e = paramInteger2.intValue();
    f = paramInteger3.intValue();
    g = paramInteger4.intValue();
    h = paramInteger5.intValue();
    i = paramInteger6.intValue();
    j = paramInteger7.intValue();
    a(Integer.valueOf(d), "Year");
    a(Integer.valueOf(e), "Month");
    a(Integer.valueOf(f), "Day");
    a(Integer.valueOf(g), "Hour");
    a(Integer.valueOf(h), "Minute");
    a(Integer.valueOf(i), "Second");
    a(Integer.valueOf(j));
    a();
    b();
    p();
    c();
    d();
    e();
    f();
    g();
    return new a(k, l, m, n, o, p, q);
  }
}

/* Location:
 * Qualified Name:     a.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */