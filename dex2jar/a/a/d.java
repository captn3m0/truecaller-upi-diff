package a.a;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class d
{
  private static final Pattern a = Pattern.compile("(\\d{1,4})-(\\d\\d)-(\\d\\d)|(\\d{1,4})-(\\d\\d)|(\\d{1,4})");
  private static final Integer b = Integer.valueOf("9");
  private static final Pattern c = Pattern.compile("(\\d\\d)\\:(\\d\\d)\\:(\\d\\d)\\.(\\d{1,9})|(\\d\\d)\\:(\\d\\d)\\:(\\d\\d)|(\\d\\d)\\:(\\d\\d)|(\\d\\d)");
  private Integer d;
  private Integer e;
  private Integer f;
  private Integer g;
  private Integer h;
  private Integer i;
  private Integer j;
  
  private static String a(Matcher paramMatcher, int... paramVarArgs)
  {
    int m = paramVarArgs.length;
    String str1 = null;
    int k = 0;
    String str2;
    for (;;)
    {
      str2 = str1;
      if (k >= m) {
        break;
      }
      str1 = paramMatcher.group(paramVarArgs[k]);
      str2 = str1;
      if (str1 != null) {
        break;
      }
      k += 1;
    }
    return str2;
  }
  
  private a b(String paramString)
  {
    a locala = new a((byte)0);
    int m = c(paramString);
    int k;
    if ((m > 0) && (m < paramString.length())) {
      k = 1;
    } else {
      k = 0;
    }
    if (k != 0)
    {
      a = paramString.substring(0, m);
      paramString = paramString.substring(m + 1);
    }
    while (d(paramString))
    {
      b = paramString;
      return locala;
    }
    a = paramString;
    return locala;
  }
  
  private static int c(String paramString)
  {
    int m = paramString.indexOf(" ");
    int k = m;
    if (m == -1) {
      k = paramString.indexOf("T");
    }
    return k;
  }
  
  private static boolean d(String paramString)
  {
    if (paramString.length() >= 2) {
      return ":".equals(paramString.substring(2, 3));
    }
    return false;
  }
  
  private void e(String paramString)
  {
    Matcher localMatcher = a.matcher(paramString);
    if (localMatcher.matches())
    {
      paramString = a(localMatcher, new int[] { 1, 4, 6 });
      if (paramString != null) {
        d = Integer.valueOf(paramString);
      }
      paramString = a(localMatcher, new int[] { 2, 5 });
      if (paramString != null) {
        e = Integer.valueOf(paramString);
      }
      paramString = a(localMatcher, new int[] { 3 });
      if (paramString != null) {
        f = Integer.valueOf(paramString);
      }
      return;
    }
    throw new b("Unexpected format for date:".concat(String.valueOf(paramString)));
  }
  
  private void f(String paramString)
  {
    Matcher localMatcher = c.matcher(paramString);
    if (localMatcher.matches())
    {
      paramString = a(localMatcher, new int[] { 1, 5, 8, 10 });
      if (paramString != null) {
        g = Integer.valueOf(paramString);
      }
      paramString = a(localMatcher, new int[] { 2, 6, 9 });
      if (paramString != null) {
        h = Integer.valueOf(paramString);
      }
      paramString = a(localMatcher, new int[] { 3, 7 });
      if (paramString != null) {
        i = Integer.valueOf(paramString);
      }
      paramString = a(localMatcher, new int[] { 4 });
      if (paramString != null) {
        j = Integer.valueOf(g(paramString));
      }
      return;
    }
    throw new b("Unexpected format for time:".concat(String.valueOf(paramString)));
  }
  
  private static String g(String paramString)
  {
    paramString = new StringBuilder(paramString);
    while (paramString.length() < b.intValue()) {
      paramString.append("0");
    }
    return paramString.toString();
  }
  
  final a a(String paramString)
  {
    if (paramString != null)
    {
      paramString = b(paramString.trim());
      if (paramString.a()) {
        e(a);
      }
      do
      {
        f(b);
        break;
        if (paramString.b())
        {
          e(a);
          break;
        }
      } while (paramString.c());
      return new a(d, e, f, g, h, i, j);
    }
    throw new NullPointerException("DateTime string is null");
  }
  
  final class a
  {
    String a;
    String b;
    
    private a() {}
    
    final boolean a()
    {
      return (a != null) && (b != null);
    }
    
    final boolean b()
    {
      return b == null;
    }
    
    final boolean c()
    {
      return a == null;
    }
  }
  
  static final class b
    extends RuntimeException
  {
    b(String paramString)
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     a.a.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */