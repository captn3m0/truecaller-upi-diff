package a.a;

import java.lang.reflect.Array;

final class e
{
  static int a(int paramInt, Object paramObject)
  {
    int j = 0;
    if (paramObject == null) {
      return paramInt * 37 + 0;
    }
    if (!a(paramObject)) {
      return paramInt * 37 + paramObject.hashCode();
    }
    int k = Array.getLength(paramObject);
    int i = paramInt;
    paramInt = j;
    while (paramInt < k)
    {
      i = a(i, Array.get(paramObject, paramInt));
      paramInt += 1;
    }
    return i;
  }
  
  static <T extends Comparable<T>> int a(T paramT1, T paramT2, a parama)
  {
    if ((paramT1 != null) && (paramT2 != null)) {
      return paramT1.compareTo(paramT2);
    }
    if ((paramT1 != null) || (paramT2 != null))
    {
      if ((paramT1 == null) && (paramT2 != null))
      {
        i = -1;
        break label52;
      }
      if ((paramT1 != null) && (paramT2 == null))
      {
        i = 1;
        break label52;
      }
    }
    int i = 0;
    label52:
    int j = i;
    if (a.b == parama) {
      j = i * -1;
    }
    return j;
  }
  
  static boolean a(Object paramObject)
  {
    return (paramObject != null) && (paramObject.getClass().isArray());
  }
  
  static enum a
  {
    private a() {}
  }
}

/* Location:
 * Qualified Name:     a.a.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */