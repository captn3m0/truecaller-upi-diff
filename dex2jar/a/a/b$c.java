package a.a;

final class b$c
{
  int a;
  int b;
  String c;
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("Start:");
    localStringBuilder.append(a);
    localStringBuilder.append(" End:");
    localStringBuilder.append(b);
    localStringBuilder.append(" '");
    localStringBuilder.append(c);
    localStringBuilder.append("'");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     a.a.b.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */