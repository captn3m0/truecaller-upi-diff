package com.daamitt.prime.sdk;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import com.daamitt.prime.sdk.a.c.a;
import com.daamitt.prime.sdk.a.c.b;
import com.daamitt.prime.sdk.a.h;
import com.daamitt.prime.sdk.a.i;
import com.daamitt.prime.sdk.a.k;
import com.daamitt.prime.sdk.a.l;
import com.daamitt.prime.sdk.a.m;
import com.daamitt.prime.sdk.b.f;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;

public class b
{
  static final String a = "b";
  static b b;
  Context c;
  com.daamitt.prime.sdk.b.b d;
  android.support.v4.content.d e;
  SharedPreferences f;
  String g;
  public String h;
  HashMap<String, ArrayList<h>> i = null;
  
  private b(Context paramContext)
  {
    c = paramContext;
    e = android.support.v4.content.d.a(paramContext);
    d = com.daamitt.prime.sdk.b.b.a(paramContext);
    f = PreferenceManager.getDefaultSharedPreferences(c);
  }
  
  public static b a(Context paramContext)
  {
    try
    {
      if (b == null) {
        b = new b(paramContext);
      }
      com.daamitt.prime.sdk.a.e.a();
      paramContext = b;
      return paramContext;
    }
    finally {}
  }
  
  private void a(com.daamitt.prime.sdk.a.a parama)
  {
    SharedPreferences.Editor localEditor = f.edit();
    Object localObject2;
    Object localObject1;
    Object localObject3;
    Object localObject4;
    StringBuilder localStringBuilder;
    if (i == 2)
    {
      localObject2 = d.a(1);
      localObject1 = d.a(4);
      localObject2 = ((ArrayList)localObject2).iterator();
      while (((Iterator)localObject2).hasNext())
      {
        localObject3 = (com.daamitt.prime.sdk.a.a)((Iterator)localObject2).next();
        localObject4 = d.replace("debit", "").trim();
        if ((TextUtils.equals(d.trim(), (CharSequence)localObject4)) && (!((com.daamitt.prime.sdk.a.a)localObject3).e()) && (!parama.e()))
        {
          localObject4 = f;
          localStringBuilder = new StringBuilder();
          localStringBuilder.append(((com.daamitt.prime.sdk.a.a)localObject3).b());
          localStringBuilder.append("->");
          localStringBuilder.append(parama.b());
          if (!((SharedPreferences)localObject4).getBoolean(localStringBuilder.toString(), false))
          {
            d.a((com.daamitt.prime.sdk.a.a)localObject3, parama);
            com.daamitt.prime.sdk.a.e.b();
            localObject4 = new StringBuilder();
            ((StringBuilder)localObject4).append(((com.daamitt.prime.sdk.a.a)localObject3).b());
            ((StringBuilder)localObject4).append("->");
            ((StringBuilder)localObject4).append(parama.b());
            localEditor.putBoolean(((StringBuilder)localObject4).toString(), true);
          }
        }
      }
      localObject1 = ((ArrayList)localObject1).iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (com.daamitt.prime.sdk.a.a)((Iterator)localObject1).next();
        if ((TextUtils.equals(d.trim(), d.replace("billpay", "").trim())) && (!parama.e()) && (!((com.daamitt.prime.sdk.a.a)localObject2).e()))
        {
          localObject3 = f;
          localObject4 = new StringBuilder();
          ((StringBuilder)localObject4).append(((com.daamitt.prime.sdk.a.a)localObject2).b());
          ((StringBuilder)localObject4).append("->");
          ((StringBuilder)localObject4).append(parama.b());
          if (!((SharedPreferences)localObject3).getBoolean(((StringBuilder)localObject4).toString(), false))
          {
            com.daamitt.prime.sdk.a.e.b();
            d.a((com.daamitt.prime.sdk.a.a)localObject2, parama);
            localObject3 = new StringBuilder();
            ((StringBuilder)localObject3).append(((com.daamitt.prime.sdk.a.a)localObject2).b());
            ((StringBuilder)localObject3).append("->");
            ((StringBuilder)localObject3).append(parama.b());
            localEditor.putBoolean(((StringBuilder)localObject3).toString(), true);
          }
        }
      }
    }
    if (i == 1)
    {
      localObject3 = d.a(2).iterator();
      localObject1 = null;
      for (;;)
      {
        localObject2 = localObject1;
        if (!((Iterator)localObject3).hasNext()) {
          break;
        }
        localObject2 = (com.daamitt.prime.sdk.a.a)((Iterator)localObject3).next();
        localObject4 = d.replace("debit", "").trim();
        if ((TextUtils.equals(d.trim(), (CharSequence)localObject4)) && (!((com.daamitt.prime.sdk.a.a)localObject2).e()))
        {
          localObject4 = f;
          localStringBuilder = new StringBuilder();
          localStringBuilder.append(parama.b());
          localStringBuilder.append("->");
          localStringBuilder.append(((com.daamitt.prime.sdk.a.a)localObject2).b());
          if (!((SharedPreferences)localObject4).getBoolean(localStringBuilder.toString(), false))
          {
            if (localObject1 != null)
            {
              localObject2 = null;
              break;
            }
            localObject1 = localObject2;
          }
        }
      }
      if (localObject2 != null)
      {
        d.a(parama, (com.daamitt.prime.sdk.a.a)localObject2);
        com.daamitt.prime.sdk.a.e.b();
        localObject1 = new StringBuilder();
        ((StringBuilder)localObject1).append(parama.b());
        ((StringBuilder)localObject1).append("->");
        ((StringBuilder)localObject1).append(((com.daamitt.prime.sdk.a.a)localObject2).b());
        localEditor.putBoolean(((StringBuilder)localObject1).toString(), true);
      }
    }
    else if (i == 4)
    {
      localObject3 = d.a(2).iterator();
      localObject1 = null;
      for (;;)
      {
        localObject2 = localObject1;
        if (!((Iterator)localObject3).hasNext()) {
          break;
        }
        localObject2 = (com.daamitt.prime.sdk.a.a)((Iterator)localObject3).next();
        localObject4 = d.replace("billpay", "").trim();
        if ((TextUtils.equals(d.trim(), (CharSequence)localObject4)) && (!((com.daamitt.prime.sdk.a.a)localObject2).e()))
        {
          localObject4 = f;
          localStringBuilder = new StringBuilder();
          localStringBuilder.append(parama.b());
          localStringBuilder.append("->");
          localStringBuilder.append(((com.daamitt.prime.sdk.a.a)localObject2).b());
          if (!((SharedPreferences)localObject4).getBoolean(localStringBuilder.toString(), false))
          {
            if (localObject1 != null)
            {
              localObject2 = null;
              break;
            }
            localObject1 = localObject2;
          }
        }
      }
      if (localObject2 != null)
      {
        d.a(parama, (com.daamitt.prime.sdk.a.a)localObject2);
        com.daamitt.prime.sdk.a.e.b();
        localObject1 = new StringBuilder();
        ((StringBuilder)localObject1).append(parama.b());
        ((StringBuilder)localObject1).append("->");
        ((StringBuilder)localObject1).append(((com.daamitt.prime.sdk.a.a)localObject2).b());
        localEditor.putBoolean(((StringBuilder)localObject1).toString(), true);
      }
    }
    else if (i == 3)
    {
      localObject2 = d.a(3).iterator();
      while (((Iterator)localObject2).hasNext())
      {
        localObject1 = (com.daamitt.prime.sdk.a.a)((Iterator)localObject2).next();
        if (TextUtils.equals(f.toUpperCase(), "XXXX"))
        {
          if ((TextUtils.equals(d, d)) && (a != a) && (!((com.daamitt.prime.sdk.a.a)localObject1).e()) && (!parama.e()))
          {
            localObject3 = f;
            localObject4 = new StringBuilder();
            ((StringBuilder)localObject4).append(parama.b());
            ((StringBuilder)localObject4).append("->");
            ((StringBuilder)localObject4).append(((com.daamitt.prime.sdk.a.a)localObject1).b());
            if (!((SharedPreferences)localObject3).getBoolean(((StringBuilder)localObject4).toString(), false))
            {
              d.a(parama, (com.daamitt.prime.sdk.a.a)localObject1);
              com.daamitt.prime.sdk.a.e.b();
              localObject2 = new StringBuilder();
              ((StringBuilder)localObject2).append(parama.b());
              ((StringBuilder)localObject2).append("->");
              ((StringBuilder)localObject2).append(((com.daamitt.prime.sdk.a.a)localObject1).b());
              localEditor.putBoolean(((StringBuilder)localObject2).toString(), true);
            }
          }
        }
        else if ((TextUtils.equals(d, d)) && (a != a) && (!((com.daamitt.prime.sdk.a.a)localObject1).e()) && (!parama.e()))
        {
          localObject3 = f;
          localObject4 = new StringBuilder();
          ((StringBuilder)localObject4).append(((com.daamitt.prime.sdk.a.a)localObject1).b());
          ((StringBuilder)localObject4).append("->");
          ((StringBuilder)localObject4).append(parama.b());
          if (!((SharedPreferences)localObject3).getBoolean(((StringBuilder)localObject4).toString(), false))
          {
            d.a((com.daamitt.prime.sdk.a.a)localObject1, parama);
            com.daamitt.prime.sdk.a.e.b();
            localObject2 = new StringBuilder();
            ((StringBuilder)localObject2).append(((com.daamitt.prime.sdk.a.a)localObject1).b());
            ((StringBuilder)localObject2).append("->");
            ((StringBuilder)localObject2).append(parama.b());
            localEditor.putBoolean(((StringBuilder)localObject2).toString(), true);
          }
        }
      }
    }
    localEditor.apply();
  }
  
  private void a(com.daamitt.prime.sdk.a.d paramd1, com.daamitt.prime.sdk.a.d paramd2)
  {
    com.daamitt.prime.sdk.a.e.a();
    Object localObject1 = J;
    boolean bool4 = false;
    Object localObject2;
    Object localObject3;
    label82:
    int j;
    if (localObject1 != null)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        localObject2 = a.iterator();
        while (((Iterator)localObject2).hasNext())
        {
          localObject3 = (c.a)((Iterator)localObject2).next();
          if (f)
          {
            bool1 = f;
            break label82;
          }
        }
        boolean bool1 = false;
        localObject1 = b.iterator();
        for (;;)
        {
          bool2 = bool4;
          bool3 = bool1;
          if (!((Iterator)localObject1).hasNext()) {
            break;
          }
          localObject3 = (c.a)((Iterator)localObject1).next();
          if (f)
          {
            bool2 = f;
            bool3 = bool1;
            break;
          }
          localObject2 = paramd2.b(a);
          localObject3 = b;
          if (!TextUtils.isEmpty((CharSequence)localObject2))
          {
            String str = com.daamitt.prime.sdk.a.d.a;
            com.daamitt.prime.sdk.a.e.a();
            switch (((String)localObject3).hashCode())
            {
            default: 
              break;
            case 984038195: 
              if (((String)localObject3).equals("event_info")) {
                j = 2;
              }
              break;
            case 951526432: 
              if (((String)localObject3).equals("contact")) {
                j = 1;
              }
              break;
            case 943500218: 
              if (((String)localObject3).equals("event_location")) {
                j = 3;
              }
              break;
            case 3076014: 
              if (((String)localObject3).equals("date")) {
                j = 5;
              }
              break;
            case 111156: 
              if (((String)localObject3).equals("pnr")) {
                j = 4;
              }
              break;
            case -1413853096: 
              if (((String)localObject3).equals("amount")) {
                j = 0;
              }
              break;
            }
            j = -1;
            switch (j)
            {
            default: 
              break;
            case 5: 
              d = new Date(Long.valueOf((String)localObject2).longValue());
              break;
            case 4: 
              c = ((String)localObject2);
              break;
            case 3: 
              f = ((String)localObject2);
              break;
            case 2: 
              g = ((String)localObject2);
              break;
            case 1: 
              j = ((String)localObject2);
              break;
            case 0: 
              i = Double.valueOf((String)localObject2).doubleValue();
            }
          }
        }
      }
    }
    boolean bool3 = false;
    boolean bool2 = bool4;
    if (bool2) {
      j = h | 0x1;
    } else {
      j = h & 0xFFFFFFFE;
    }
    h = j;
    if (bool3)
    {
      j = h | 0x4 | 0x1;
      h = j;
      localObject1 = new ContentValues();
      ((ContentValues)localObject1).put("flags", Integer.valueOf(j));
      localObject2 = d.e;
      if (o >= 0L)
      {
        localObject3 = com.daamitt.prime.sdk.b.c.a;
        com.daamitt.prime.sdk.a.e.a();
        localObject2 = b;
        localObject3 = new StringBuilder("_id = ");
        ((StringBuilder)localObject3).append(o);
        ((SQLiteDatabase)localObject2).update("events", (ContentValues)localObject1, ((StringBuilder)localObject3).toString(), null);
      }
      else
      {
        localObject1 = com.daamitt.prime.sdk.b.c.a;
        com.daamitt.prime.sdk.a.e.a();
      }
      long l = C;
      paramd2 = d.a(l);
      localObject1 = new ContentValues();
      ((ContentValues)localObject1).put("previousUUID", H);
      d.a(C, (ContentValues)localObject1);
    }
  }
  
  private void a(l paraml1, l paraml2)
  {
    Object localObject1 = J;
    boolean bool3 = false;
    int j;
    if (localObject1 != null)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        Object localObject2 = a.iterator();
        boolean bool1 = false;
        Object localObject3;
        while (((Iterator)localObject2).hasNext())
        {
          localObject3 = (c.a)((Iterator)localObject2).next();
          if (f) {
            bool1 = f;
          }
        }
        localObject1 = b;
        bool2 = bool1;
        if (localObject1 == null) {
          break label414;
        }
        localObject1 = ((ArrayList)localObject1).iterator();
        bool2 = false;
        while (((Iterator)localObject1).hasNext())
        {
          localObject3 = (c.a)((Iterator)localObject1).next();
          bool3 = bool2;
          if (f) {
            bool3 = f;
          }
          localObject2 = paraml2.b(a);
          localObject3 = b;
          bool2 = bool3;
          if (!TextUtils.isEmpty((CharSequence)localObject2))
          {
            String str = l.a;
            com.daamitt.prime.sdk.a.e.a();
            j = ((String)localObject3).hashCode();
            if (j != -1413853096)
            {
              if (j != -264918006)
              {
                if (j != 110749)
                {
                  if ((j == 3076014) && (((String)localObject3).equals("date")))
                  {
                    j = 3;
                    break label285;
                  }
                }
                else if (((String)localObject3).equals("pan"))
                {
                  j = 1;
                  break label285;
                }
              }
              else if (((String)localObject3).equals("statement_type"))
              {
                j = 2;
                break label285;
              }
            }
            else if (((String)localObject3).equals("amount"))
            {
              j = 0;
              break label285;
            }
            j = -1;
            switch (j)
            {
            default: 
              bool2 = bool3;
              break;
            case 3: 
              g = new Date(Long.valueOf((String)localObject2).longValue());
              bool2 = bool3;
              break;
            case 2: 
              i = Integer.valueOf((String)localObject2).intValue();
              bool2 = bool3;
              break;
            case 1: 
              b = ((String)localObject2);
              bool2 = bool3;
              break;
            case 0: 
              label285:
              d = Double.valueOf((String)localObject2).doubleValue();
              bool2 = bool3;
            }
          }
        }
        bool3 = bool2;
        bool2 = bool1;
        break label414;
      }
    }
    boolean bool2 = false;
    label414:
    if (bool3) {
      j = j | 0x2;
    } else {
      j = j & 0xFFFFFFFD;
    }
    j = j;
    if (bool2)
    {
      j |= 0x2;
      j = j;
      localObject1 = new ContentValues();
      ((ContentValues)localObject1).put("status", Integer.valueOf(j));
      d.a(paraml2, (ContentValues)localObject1);
    }
    else
    {
      j &= 0xFFFFFFFD;
      j = j;
      localObject1 = new ContentValues();
      ((ContentValues)localObject1).put("status", Integer.valueOf(j));
      ((ContentValues)localObject1).put("wSmsId", Long.valueOf(C));
      d.a(paraml2, (ContentValues)localObject1);
    }
    long l = C;
    paraml2 = d.a(l);
    localObject1 = new ContentValues();
    ((ContentValues)localObject1).put("previousUUID", H);
    d.a(C, (ContentValues)localObject1);
  }
  
  private void a(m paramm1, m paramm2)
  {
    com.daamitt.prime.sdk.a.e.a();
    Object localObject1 = J;
    boolean bool4 = false;
    int j;
    if (localObject1 != null)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        Object localObject2 = a.iterator();
        bool2 = false;
        bool1 = false;
        Object localObject3;
        while (((Iterator)localObject2).hasNext())
        {
          localObject3 = (c.a)((Iterator)localObject2).next();
          bool3 = bool2;
          if (f) {
            bool3 = f;
          }
          bool2 = bool3;
          if (g)
          {
            bool1 = g;
            bool2 = bool3;
          }
        }
        localObject1 = b;
        if (localObject1 != null)
        {
          localObject1 = ((ArrayList)localObject1).iterator();
          bool4 = false;
          bool3 = false;
          while (((Iterator)localObject1).hasNext())
          {
            localObject3 = (c.a)((Iterator)localObject1).next();
            boolean bool5 = bool4;
            if (f) {
              bool5 = f;
            }
            boolean bool6 = bool3;
            if (g) {
              bool6 = g;
            }
            localObject2 = paramm2.b(a);
            localObject3 = b;
            bool4 = bool5;
            bool3 = bool6;
            if (!TextUtils.isEmpty((CharSequence)localObject2))
            {
              String str = m.a;
              com.daamitt.prime.sdk.a.e.a();
              switch (((String)localObject3).hashCode())
              {
              default: 
                break;
              case 509054971: 
                if (((String)localObject3).equals("transaction_type")) {
                  j = 4;
                }
                break;
              case 3387378: 
                if (((String)localObject3).equals("note")) {
                  j = 1;
                }
                break;
              case 3076014: 
                if (((String)localObject3).equals("date")) {
                  j = 5;
                }
                break;
              case 111188: 
                if (((String)localObject3).equals("pos")) {
                  j = 2;
                }
                break;
              case 110749: 
                if (((String)localObject3).equals("pan")) {
                  j = 3;
                }
                break;
              case -1413853096: 
                if (((String)localObject3).equals("amount")) {
                  j = 0;
                }
                break;
              }
              j = -1;
              switch (j)
              {
              default: 
                bool4 = bool5;
                bool3 = bool6;
                break;
              case 5: 
                ad = new Date(Long.valueOf((String)localObject2).longValue());
                bool4 = bool5;
                bool3 = bool6;
                break;
              case 4: 
                j = Integer.valueOf((String)localObject2).intValue();
                bool4 = bool5;
                bool3 = bool6;
                break;
              case 3: 
                b = ((String)localObject2);
                bool4 = bool5;
                bool3 = bool6;
                break;
              case 2: 
                c = ((String)localObject2);
                m = ((String)localObject2);
                bool4 = bool5;
                bool3 = bool6;
                break;
              case 1: 
                V = ((String)localObject2);
                bool4 = bool5;
                bool3 = bool6;
                break;
              case 0: 
                f = Double.valueOf((String)localObject2).doubleValue();
                bool4 = bool5;
                bool3 = bool6;
              }
            }
          }
          break label606;
        }
        bool3 = false;
        break label606;
      }
    }
    boolean bool2 = false;
    boolean bool1 = false;
    boolean bool3 = false;
    label606:
    if (bool4) {
      j = Y | 0x10;
    } else {
      j = Y & 0xFFFFFFEF;
    }
    if (bool3) {
      j |= 0x100;
    } else {
      j &= 0xFEFF;
    }
    Y = j;
    if (bool1) {
      j = Y | 0x100;
    } else {
      j = Y & 0xFEFF;
    }
    if (bool2)
    {
      j |= 0x10;
      Y = j;
      localObject1 = new ContentValues();
      ((ContentValues)localObject1).put("flags", Integer.valueOf(j));
      d.a(paramm2, (ContentValues)localObject1);
    }
    else
    {
      j &= 0xFFFFFFEF;
      Y = j;
      localObject1 = new ContentValues();
      ((ContentValues)localObject1).put("flags", Integer.valueOf(j));
      ((ContentValues)localObject1).put("wSmsId", Long.valueOf(C));
      d.a(paramm2, (ContentValues)localObject1);
    }
    long l = C;
    paramm2 = d.a(l);
    localObject1 = new ContentValues();
    ((ContentValues)localObject1).put("previousUUID", H);
    d.a(C, (ContentValues)localObject1);
  }
  
  public final void a(String paramString)
  {
    com.daamitt.prime.sdk.a.e.a();
    i = new HashMap();
    Object localObject1 = new JSONObject(paramString);
    h = ((JSONObject)localObject1).getString("version");
    com.daamitt.prime.sdk.a.e.b();
    paramString = ((JSONObject)localObject1).getJSONArray("rules");
    g = ((JSONObject)localObject1).optString("blacklist_regex");
    int j = 0;
    while (j < paramString.length())
    {
      Object localObject2 = paramString.getJSONObject(j);
      String str1 = ((JSONObject)localObject2).optString("name", "Unknown");
      long l = ((JSONObject)localObject2).getLong("sender_UID");
      boolean bool = ((JSONObject)localObject2).optBoolean("set_account_as_expense", true);
      JSONArray localJSONArray = ((JSONObject)localObject2).getJSONArray("patterns");
      JSONObject localJSONObject1 = ((JSONObject)localObject2).optJSONObject("sms_preprocessor");
      localObject1 = new ArrayList();
      int k = 0;
      while (k < localJSONArray.length())
      {
        JSONObject localJSONObject2 = localJSONArray.getJSONObject(k);
        h localh = new h();
        String str2 = localJSONObject2.getString("regex");
        if (str2.contains("\\\\")) {
          a = str2.replaceAll("\\\\", "\\");
        } else {
          a = str2;
        }
        b = Pattern.compile(str2);
        c = localJSONObject2.getString("account_type");
        d = localJSONObject2.getString("sms_type");
        f = localJSONObject2.optString("account_name_override");
        j = localJSONObject2.getLong("pattern_UID");
        k = localJSONObject2.getLong("sort_UID");
        l = l;
        m = localJSONObject2.optBoolean("reparse", false);
        g = localJSONObject2.optJSONObject("data_fields");
        h = localJSONObject1;
        e = str1;
        if (localJSONObject2.has("set_account_as_expense")) {
          i = localJSONObject2.optBoolean("set_account_as_expense", true);
        } else {
          i = bool;
        }
        ((ArrayList)localObject1).add(localh);
        k += 1;
      }
      Collections.sort((List)localObject1, -..Lambda.b.PEB0H7o8fLABaP65zRnHEKlTBfo.INSTANCE);
      localObject2 = ((JSONObject)localObject2).getJSONArray("senders");
      k = 0;
      while (k < ((JSONArray)localObject2).length())
      {
        i.put(((JSONArray)localObject2).getString(k), localObject1);
        k += 1;
      }
      j += 1;
    }
  }
  
  final void a(String paramString1, String paramString2, Date paramDate, long paramLong, int paramInt, String paramString3)
  {
    paramString1 = a.a(paramString1, paramString2, paramDate, i, g);
    if ((paramString1 != null) && (!paramString1.isEmpty()))
    {
      paramDate = paramString1.iterator();
      while (paramDate.hasNext())
      {
        paramString2 = (k)paramDate.next();
        if (paramString2 != null)
        {
          M = paramString3;
          Q = paramInt;
          if (s != null)
          {
            C = paramLong;
            if (v == 99) {
              paramString1 = d.a(s, "Messages", 99);
            } else {
              paramString1 = d.a(s, "Messages", 9);
            }
            w = a;
            Object localObject1 = new ContentValues();
            Object localObject2 = d.d;
            Object localObject3 = new ContentValues();
            ((ContentValues)localObject3).put("sender", p);
            ((ContentValues)localObject3).put("date", Long.valueOf(r.getTime()));
            ((ContentValues)localObject3).put("body", q);
            ((ContentValues)localObject3).put("smsId", Long.valueOf(C));
            if (paramString1 != null) {
              ((ContentValues)localObject3).put("accountId", Integer.valueOf(a));
            } else {
              ((ContentValues)localObject3).put("accountId", Integer.valueOf(w));
            }
            ((ContentValues)localObject3).put("smsFlags", Integer.valueOf(G));
            ((ContentValues)localObject3).put("parsed", Boolean.FALSE);
            Object localObject4 = D;
            if (localObject4 != null)
            {
              ((ContentValues)localObject3).put("lat", Double.valueOf(((Location)localObject4).getLatitude()));
              ((ContentValues)localObject3).put("long", Double.valueOf(((Location)localObject4).getLongitude()));
              ((ContentValues)localObject3).put("locAccuracy", Float.valueOf(((Location)localObject4).getAccuracy()));
            }
            H = UUID.randomUUID().toString();
            ((ContentValues)localObject3).put("UUID", H);
            if ((F != null) && (F.j != 0L)) {
              ((ContentValues)localObject3).put("patternUID", Long.valueOf(F.j));
            }
            ((ContentValues)localObject3).put("URI", M);
            int j;
            if ((G & 0x10) == 16) {
              j = 1;
            } else {
              j = 0;
            }
            if (j != 0) {
              ((ContentValues)localObject3).put("probability", Double.valueOf(N));
            }
            ((ContentValues)localObject3).put("simSubscriptionId", Integer.valueOf(O));
            ((ContentValues)localObject3).put("simSlotId", Integer.valueOf(P));
            ((ContentValues)localObject3).put("threadId", Integer.valueOf(Q));
            ((ContentValues)localObject3).put("creator", R);
            if (S != Double.MIN_VALUE) {
              ((ContentValues)localObject3).put("metaData", Double.valueOf(S));
            }
            o = a.insert("sms", null, (ContentValues)localObject3);
            long l = o;
            int k;
            if ((paramString2 instanceof m))
            {
              localObject2 = (m)paramString2;
              C = o;
              if (K)
              {
                paramString1 = ((m)localObject2).a();
                paramString2 = d;
                localObject3 = s;
                paramString2 = a.a((String)localObject3);
                paramString1 = d.b.a(paramString2, paramString1, (m)localObject2);
                if (paramString1 != null) {
                  a((m)localObject2, paramString1);
                } else if (((m)localObject2).b()) {
                  Y |= 0x10;
                } else {
                  Y &= 0xFFFFFFEF;
                }
              }
              if ((j != 12) && (j != 17))
              {
                paramString1 = d;
                paramString2 = new StringBuilder();
                paramString2.append(s);
                paramString2.append(" ");
                paramString2.append(m.a(j, v));
                paramString1 = paramString1.a(paramString2.toString(), b, v, L, B);
              }
              else
              {
                paramString1 = d;
                paramString2 = new StringBuilder();
                paramString2.append(s);
                paramString2.append(" ");
                paramString2.append(com.daamitt.prime.sdk.a.a.a(v));
                paramString1 = paramString1.a(paramString2.toString(), b, v, L, B);
              }
              if (s) {
                a(paramString1);
              }
              w = a;
              z = k;
              d = paramString1.c();
              y = paramString1.a();
              if (!k) {
                Y |= 0x8;
              }
              boolean bool2;
              boolean bool1;
              if (i != null)
              {
                paramString2 = d.b(a);
                if (paramString2 != null) {
                  paramString1 = paramString2;
                }
                if (m != null)
                {
                  bool2 = com.daamitt.prime.sdk.a.b.a(r, m.c);
                  bool1 = com.daamitt.prime.sdk.a.b.a(r, m.d);
                }
                else
                {
                  bool1 = true;
                  bool2 = true;
                }
                paramString2 = new com.daamitt.prime.sdk.a.b();
                localObject3 = new ContentValues();
                if (bool2)
                {
                  a = i.a;
                  c = r;
                }
                else
                {
                  a = Double.MIN_VALUE;
                }
                if (bool1)
                {
                  b = i.b;
                  d = r;
                }
                else
                {
                  b = Double.MIN_VALUE;
                }
                m = paramString2;
                com.daamitt.prime.sdk.b.a.a((ContentValues)localObject3, m);
                if (((ContentValues)localObject3).size() > 0)
                {
                  ((ContentValues)localObject3).put("updatedTime", Long.valueOf(System.currentTimeMillis()));
                  j = l;
                  if (((ContentValues)localObject3).containsKey("balance")) {
                    j &= 0xFFFFFFFB;
                  } else {
                    j |= 0x4;
                  }
                  if (((ContentValues)localObject3).containsKey("outstandingBalance")) {
                    j &= 0xFFFFFFF7;
                  } else {
                    j |= 0x8;
                  }
                  l = j;
                  ((ContentValues)localObject3).put("flags", Integer.valueOf(j));
                  d.a(paramString1, (ContentValues)localObject3);
                }
                else
                {
                  i = null;
                }
              }
              else
              {
                paramString2 = d.b(a);
                if (paramString2 != null) {
                  paramString1 = paramString2;
                }
                k = l;
                if (m != null)
                {
                  bool2 = com.daamitt.prime.sdk.a.b.a(ad, m.c);
                  bool1 = com.daamitt.prime.sdk.a.b.a(ad, m.d);
                }
                else
                {
                  bool1 = true;
                  bool2 = true;
                }
                j = k;
                if (bool2) {
                  j = k | 0x4;
                }
                k = j;
                if (bool1) {
                  k = j | 0x8;
                }
                l = k;
                paramString2 = new ContentValues();
                paramString2.put("updatedTime", Long.valueOf(System.currentTimeMillis()));
                paramString2.put("flags", Integer.valueOf(k));
                d.a(paramString1, paramString2);
              }
              if ((j == 12) || (j == 17)) {
                Y |= 0x10;
              }
              paramString1 = T;
              if (!TextUtils.isEmpty(paramString1))
              {
                if (!ac) {
                  if (((j == 4) || (j == 18)) && (f < 10000.0D))
                  {
                    ((m)localObject2).n();
                    if (j == 4) {
                      T = "walnut_bills";
                    }
                  }
                  else if (((j == 4) || (j == 18)) && (f >= 10000.0D))
                  {
                    ((m)localObject2).m();
                    T = "walnut_transfer";
                  }
                  else if (j == 5)
                  {
                    ((m)localObject2).n();
                  }
                  else if ((m.b(j)) && (paramString1.equals("other")))
                  {
                    ((m)localObject2).m();
                  }
                }
                if ((m.a(j)) && (paramString1.equals("other"))) {
                  T = "walnut_bills";
                }
              }
              o = d.b.a((m)localObject2);
              ((ContentValues)localObject1).put("parsed", Boolean.TRUE);
              d.a(l, (ContentValues)localObject1);
            }
            else if ((paramString2 instanceof l))
            {
              paramString1 = (l)paramString2;
              localObject2 = d;
              localObject3 = new StringBuilder();
              ((StringBuilder)localObject3).append(s);
              ((StringBuilder)localObject3).append(" ");
              ((StringBuilder)localObject3).append(l.a(i));
              localObject2 = ((com.daamitt.prime.sdk.b.b)localObject2).a(((StringBuilder)localObject3).toString(), b, v, L, B);
              C = o;
              w = a;
              z = k;
              x = n;
              y = ((com.daamitt.prime.sdk.a.a)localObject2).a();
              c = ((com.daamitt.prime.sdk.a.a)localObject2).c();
              paramString2 = Calendar.getInstance();
              paramString2.setTime(r);
              paramString2.add(5, -1);
              if (g.before(paramString2.getTime()))
              {
                j = 0;
              }
              else
              {
                paramString2.add(5, 1);
                paramString2.add(2, 2);
                if (g.after(paramString2.getTime())) {
                  j = 0;
                } else {
                  j = 1;
                }
              }
              if (j != 0)
              {
                if (f != -1.0D)
                {
                  paramString2 = d.b;
                  localObject3 = a.a(C);
                  localObject4 = new m(null, null, null);
                  C = C;
                  ((m)localObject4).m();
                  w = a;
                  ((m)localObject4).a(f, Double.valueOf(f), r, d, 16);
                  if (i == 10) {
                    T = "walnut_bills";
                  }
                  if (!k) {
                    Y |= 0x8;
                  }
                  o = paramString2.a((m)localObject4);
                }
                if (K)
                {
                  paramString2 = paramString1.a();
                  paramString2 = d.c.a(paramString2, paramString1);
                  if (paramString2 != null) {
                    a(paramString1, paramString2);
                  } else if (paramString1.b()) {
                    j |= 0x2;
                  } else {
                    j &= 0xFFFFFFFD;
                  }
                }
                if (!d.c.a(paramString1, (com.daamitt.prime.sdk.a.a)localObject2))
                {
                  paramString2 = d.c;
                  localObject2 = new ContentValues();
                  ((ContentValues)localObject2).put("wSmsId", Long.valueOf(C));
                  ((ContentValues)localObject2).put("amount", Double.valueOf(d));
                  ((ContentValues)localObject2).put("minDueAmount", Double.valueOf(e));
                  ((ContentValues)localObject2).put("dueDate", Long.valueOf(g.getTime()));
                  if (h != null) {
                    ((ContentValues)localObject2).put("paymentDate", Long.valueOf(h.getTime()));
                  }
                  ((ContentValues)localObject2).put("type", Integer.valueOf(i));
                  ((ContentValues)localObject2).put("accountId", Integer.valueOf(w));
                  ((ContentValues)localObject2).put("status", Integer.valueOf(j));
                  k = UUID.randomUUID().toString();
                  ((ContentValues)localObject2).put("UUID", k);
                  ((ContentValues)localObject2).put("billType", Integer.valueOf(l));
                  if (!TextUtils.isEmpty(m)) {
                    ((ContentValues)localObject2).put("txnUUID", m);
                  }
                  o = a.insert("statements", null, (ContentValues)localObject2);
                }
                else
                {
                  o = -1L;
                }
                ((ContentValues)localObject1).put("parsed", Boolean.TRUE);
                d.a(l, (ContentValues)localObject1);
              }
            }
            else if ((paramString2 instanceof com.daamitt.prime.sdk.a.d))
            {
              paramString1 = (com.daamitt.prime.sdk.a.d)paramString2;
              C = o;
              if (K)
              {
                paramString2 = paramString1.a();
                if ((paramString2 != null) && (!paramString2.isEmpty()))
                {
                  paramString2 = d.e.a(paramString2, paramString1);
                  if (paramString2 != null)
                  {
                    if ((h & 0x1) == 0) {
                      l = true;
                    }
                    a(paramString1, paramString2);
                  }
                  else if (paramString1.b())
                  {
                    h |= 0x1;
                  }
                  else
                  {
                    h &= 0xFFFFFFFE;
                  }
                }
              }
              paramString2 = d.e.b;
              localObject2 = new StringBuilder("name=? AND dueDate=");
              ((StringBuilder)localObject2).append(d.getTime());
              ((StringBuilder)localObject2).append(" AND pnr=? AND flags & 4 =0");
              localObject2 = ((StringBuilder)localObject2).toString();
              localObject3 = b;
              k = 0;
              localObject4 = c;
              paramString2 = paramString2.query("events", new String[] { "_id" }, (String)localObject2, new String[] { localObject3, localObject4 }, null, null, null);
              if ((paramString2 != null) && (paramString2.getCount() > 0))
              {
                localObject2 = com.daamitt.prime.sdk.b.c.a;
                com.daamitt.prime.sdk.a.e.b();
                paramString2.close();
                j = 1;
              }
              else
              {
                j = k;
                if (paramString2 != null)
                {
                  paramString2.close();
                  j = k;
                }
              }
              if (j == 0)
              {
                paramString2 = d.e;
                localObject2 = new ContentValues();
                ((ContentValues)localObject2).put("wSmsId", Long.valueOf(C));
                ((ContentValues)localObject2).put("name", b);
                ((ContentValues)localObject2).put("pnr", c);
                ((ContentValues)localObject2).put("dueDate", Long.valueOf(d.getTime()));
                ((ContentValues)localObject2).put("type", Integer.valueOf(e));
                if (f != null) {
                  ((ContentValues)localObject2).put("location", f);
                }
                if (g != null) {
                  ((ContentValues)localObject2).put("info", g);
                }
                ((ContentValues)localObject2).put("UUID", UUID.randomUUID().toString());
                ((ContentValues)localObject2).put("flags", Integer.valueOf(h));
                ((ContentValues)localObject2).put("amount", Double.valueOf(i));
                ((ContentValues)localObject2).put("reminderTimeSpan", Long.valueOf(m));
                if (j != null) {
                  ((ContentValues)localObject2).put("contact", j);
                }
                o = b.insert("events", null, (ContentValues)localObject2);
                if ((i != 0.0D) && (k))
                {
                  paramString2 = d.b;
                  localObject3 = a.a(C);
                  j = w;
                  localObject2 = new m(null, null, null);
                  C = C;
                  ((m)localObject2).m();
                  w = j;
                  ((m)localObject2).a(b, Double.valueOf(i), r, b, 13);
                  if ((3 == e) || (2 == e) || (5 == e)) {
                    T = "walnut_travel";
                  }
                  if (i < 0.0D)
                  {
                    localObject3 = new StringBuilder();
                    ((StringBuilder)localObject3).append(b);
                    ((StringBuilder)localObject3).append(" refund");
                    V = ((StringBuilder)localObject3).toString();
                  }
                  o = paramString2.a((m)localObject2);
                }
              }
              ((ContentValues)localObject1).put("parsed", Boolean.TRUE);
              d.a(l, (ContentValues)localObject1);
            }
            else
            {
              j = 0;
              localObject1 = i.a(paramString1.a());
              if ((l & 0x80) == 128) {
                j = 1;
              }
              if ((j == 0) && (!((String)localObject1).matches("(?i).*[0-9]{10}\\s*")))
              {
                paramString2 = paramString2.c(paramString1.a());
                if (paramString2 != null)
                {
                  com.daamitt.prime.sdk.a.e.a();
                  e = paramString2;
                  l |= 0x80;
                  paramString2 = d.a;
                  localObject1 = new ContentValues();
                  ((ContentValues)localObject1).put("displayName", paramString1.a());
                  ((ContentValues)localObject1).put("flags", Integer.valueOf(l));
                  ((ContentValues)localObject1).put("updatedTime", Long.valueOf(System.currentTimeMillis()));
                  b.a(paramString1, (ContentValues)localObject1);
                }
              }
            }
          }
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */