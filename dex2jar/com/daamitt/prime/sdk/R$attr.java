package com.daamitt.prime.sdk;

public final class R$attr
{
  public static final int font = 2130968998;
  public static final int fontProviderAuthority = 2130969000;
  public static final int fontProviderCerts = 2130969001;
  public static final int fontProviderFetchStrategy = 2130969002;
  public static final int fontProviderFetchTimeout = 2130969003;
  public static final int fontProviderPackage = 2130969004;
  public static final int fontProviderQuery = 2130969005;
  public static final int fontStyle = 2130969006;
  public static final int fontWeight = 2130969008;
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.R.attr
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */