package com.daamitt.prime.sdk;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.Telephony.Sms.Inbox;
import android.support.annotation.Keep;
import android.text.TextUtils;
import com.daamitt.prime.sdk.a.j;
import com.daamitt.prime.sdk.b.c;
import io.reactivex.k;
import java.util.Date;
import org.json.JSONException;

@Keep
public final class PrimeSdk
{
  private static final String EXTRA_MAX = "prime.sdk.EXTRA_MAX";
  private static final String EXTRA_PROGRESS = "prime.sdk.EXTRA_PROGRESS";
  private static final String PRIME_FINISH = "prime.sdk.PRIME_FINISH";
  private static final String PRIME_PROGRESS = "prime.sdk.PRIME_PROGRESS";
  private static final String PRIME_REQUEST_FOR_READ_SMS_PERM = "prime.sdk.PRIME_REQUEST_FOR_READ_SMS_PERM";
  private static final String TAG = "PrimeSdk";
  private static PrimeSdk sInstance;
  private io.reactivex.a.a mCompositeDisposable = new io.reactivex.a.a();
  private b mPrimeParsingManager = null;
  private BroadcastReceiver mReceiver = new BroadcastReceiver()
  {
    public final void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
    {
      if (TextUtils.equals(paramAnonymousIntent.getAction(), "prime.sdk.PRIME_PROGRESS"))
      {
        int i = paramAnonymousIntent.getIntExtra("prime.sdk.EXTRA_PROGRESS", 0);
        int j = paramAnonymousIntent.getIntExtra("prime.sdk.EXTRA_MAX", 0);
        mScoreCallback.onScoreDataProgress(i, j);
        return;
      }
      if (TextUtils.equals(paramAnonymousIntent.getAction(), "prime.sdk.PRIME_FINISH"))
      {
        if (mCompositeDisposable == null)
        {
          com.daamitt.prime.sdk.a.e.c();
          android.support.v4.content.d.a(paramAnonymousContext).a(this);
          PrimeSdk.this.clearStorage(paramAnonymousContext);
          return;
        }
        paramAnonymousIntent = new j();
        long l = System.currentTimeMillis();
        mCompositeDisposable.a(paramAnonymousIntent.a(paramAnonymousContext).a(io.reactivex.android.b.a.a()).a(new -..Lambda.PrimeSdk.1.g05bcbMyFXjnhQvrfzzurqMGYaY(this, l, paramAnonymousIntent, paramAnonymousContext), new -..Lambda.PrimeSdk.1.0nKziViHl7uhR-vAcI4vMAYz0-E(this, paramAnonymousContext)));
        android.support.v4.content.d.a(paramAnonymousContext).a(this);
        return;
      }
      if (TextUtils.equals(paramAnonymousIntent.getAction(), "prime.sdk.PRIME_REQUEST_FOR_READ_SMS_PERM"))
      {
        mScoreCallback.onError("Cannot perform scoring without read SMS permission");
        android.support.v4.content.d.a(paramAnonymousContext).a(this);
      }
    }
  };
  private ScoreCallback mScoreCallback = null;
  
  static void broadcastFinish(android.support.v4.content.d paramd)
  {
    paramd.a(new Intent("prime.sdk.PRIME_FINISH"));
  }
  
  static void broadcastProgress(android.support.v4.content.d paramd, int paramInt1, int paramInt2)
  {
    Intent localIntent = new Intent("prime.sdk.PRIME_PROGRESS");
    localIntent.putExtra("prime.sdk.EXTRA_PROGRESS", paramInt1);
    localIntent.putExtra("prime.sdk.EXTRA_MAX", paramInt2);
    paramd.a(localIntent);
  }
  
  static void broadcastReadSmsPermissionRequest(android.support.v4.content.d paramd)
  {
    paramd.a(new Intent("prime.sdk.PRIME_REQUEST_FOR_READ_SMS_PERM"));
  }
  
  private void clearStorage(Context paramContext)
  {
    PreferenceManager.getDefaultSharedPreferences(paramContext.getApplicationContext()).edit().clear().apply();
    paramContext = com.daamitt.prime.sdk.b.b.a(paramContext).getWritableDatabase();
    com.daamitt.prime.sdk.b.a.b(paramContext);
    com.daamitt.prime.sdk.b.f.b(paramContext);
    com.daamitt.prime.sdk.b.e.b(paramContext);
    com.daamitt.prime.sdk.b.d.b(paramContext);
    c.b(paramContext);
  }
  
  public static PrimeSdk getInstance()
  {
    try
    {
      if (sInstance == null) {
        sInstance = new PrimeSdk();
      }
      com.daamitt.prime.sdk.a.e.a();
      PrimeSdk localPrimeSdk = sInstance;
      return localPrimeSdk;
    }
    finally {}
  }
  
  private void registerCallback(Context paramContext, ScoreCallback paramScoreCallback)
  {
    mScoreCallback = paramScoreCallback;
    paramScoreCallback = new IntentFilter("prime.sdk.PRIME_PROGRESS");
    paramScoreCallback.addAction("prime.sdk.PRIME_FINISH");
    paramScoreCallback.addAction("prime.sdk.PRIME_REQUEST_FOR_READ_SMS_PERM");
    android.support.v4.content.d.a(paramContext).a(mReceiver);
    android.support.v4.content.d.a(paramContext).a(mReceiver, paramScoreCallback);
  }
  
  public final void destroy(Context paramContext)
  {
    com.daamitt.prime.sdk.a.e.a();
    android.support.v4.content.d.a(paramContext).a(mReceiver);
    paramContext = mCompositeDisposable;
    if ((paramContext != null) && (!b)) {
      mCompositeDisposable.a();
    }
    paramContext = mPrimeParsingManager;
    if (paramContext != null)
    {
      Object localObject = b.a;
      localObject = b.b;
      com.daamitt.prime.sdk.a.e.a();
      b.b = null;
      c = null;
      d = null;
      f = null;
      i = null;
      g = null;
      h = null;
      mPrimeParsingManager = null;
    }
    mCompositeDisposable = null;
    mScoreCallback = null;
    sInstance = null;
  }
  
  public final void enableLogging(boolean paramBoolean)
  {
    com.daamitt.prime.sdk.a.e.a = paramBoolean;
  }
  
  public final void getPreScoreData(Context paramContext, PreScoreCallback paramPreScoreCallback)
  {
    
    if (android.support.v4.content.b.a(paramContext, "android.permission.READ_SMS") != 0)
    {
      paramPreScoreCallback.onError("Cannot perform scoring without read SMS permission");
      return;
    }
    if (mCompositeDisposable == null)
    {
      paramPreScoreCallback.onError("Destroy has been called on this object.");
      return;
    }
    com.daamitt.prime.sdk.a.f localf = new com.daamitt.prime.sdk.a.f();
    long l = System.currentTimeMillis();
    mCompositeDisposable.a(localf.a(paramContext).a(io.reactivex.android.b.a.a()).a(new -..Lambda.PrimeSdk.V_izwBet5CCXFQP6GlG22dOOcbk(l, paramPreScoreCallback, localf), new -..Lambda.PrimeSdk.H1EPxNo75BO-zPisMlnj0J1z3LM(paramPreScoreCallback)));
  }
  
  public final void getScoreData(Context paramContext, String paramString, ScoreCallback paramScoreCallback)
  {
    
    if (android.support.v4.content.b.a(paramContext, "android.permission.READ_SMS") != 0)
    {
      paramScoreCallback.onError("Cannot perform scoring without read SMS permission");
      return;
    }
    if (TextUtils.isEmpty(paramString))
    {
      paramScoreCallback.onError("Rules not present");
      return;
    }
    mPrimeParsingManager = b.a(paramContext);
    for (;;)
    {
      long l1;
      long l2;
      int j;
      try
      {
        mPrimeParsingManager.a(paramString);
        registerCallback(paramContext, paramScoreCallback);
      }
      catch (JSONException paramContext)
      {
        b localb;
        continue;
      }
      try
      {
        localb = mPrimeParsingManager;
        paramContext = b.a;
        paramContext = b.b;
        com.daamitt.prime.sdk.a.e.a();
        l1 = System.currentTimeMillis();
        l2 = l1 - 16416000000L;
        try
        {
          localObject1 = Uri.parse("content://sms/inbox");
          paramString = "dateSent > ".concat(String.valueOf(l2));
          k = 0;
          paramContext = null;
        }
        catch (RuntimeException paramContext)
        {
          Object localObject1;
          int k;
          int i;
          int m;
          throw paramContext;
        }
      }
      catch (RuntimeException paramContext)
      {
        continue;
      }
      try
      {
        paramString = c.getContentResolver().query((Uri)localObject1, new String[] { "_id", "body", "address", "date_sent", "date", "thread_id", "case when date_sent IS NOT 0 then date_sent else date END as dateSent" }, paramString, null, "dateSent ASC");
        i = 0;
        paramContext = paramString;
      }
      catch (SQLiteException paramString) {}catch (SecurityException paramString)
      {
        continue;
        if (j != 0) {
          continue;
        }
        l1 = l2;
        if (l2 > 0L) {
          continue;
        }
        continue;
        l2 = 0L;
        continue;
      }
    }
    paramString = c.getContentResolver().query((Uri)localObject1, new String[] { "_id", "body", "address", "date", "thread_id" }, "date > ? ", new String[] { String.valueOf(l2) }, "date ASC");
    i = 1;
    paramContext = paramString;
    break label276;
    broadcastReadSmsPermissionRequest(e);
    i = 0;
    label276:
    if (paramContext != null)
    {
      m = paramContext.getCount();
      if (paramContext.getCount() > 0) {
        com.daamitt.prime.sdk.a.e.b();
      }
      paramContext.moveToFirst();
      j = i;
      i = m;
      while (!paramContext.isAfterLast())
      {
        paramString = paramContext.getString(paramContext.getColumnIndexOrThrow("body"));
        localObject1 = paramContext.getString(paramContext.getColumnIndexOrThrow("address"));
        m = paramContext.getInt(paramContext.getColumnIndexOrThrow("thread_id"));
        if (j != 0) {
          break label743;
        }
        l2 = paramContext.getLong(paramContext.getColumnIndex("date_sent"));
        paramContext.getLong(paramContext.getColumnIndex("date_sent"));
        paramContext.getLong(paramContext.getColumnIndexOrThrow("date"));
        com.daamitt.prime.sdk.a.e.b();
        break label743;
        com.daamitt.prime.sdk.a.e.b();
        l1 = paramContext.getLong(paramContext.getColumnIndexOrThrow("date"));
        long l3 = paramContext.getInt(paramContext.getColumnIndexOrThrow("_id"));
        Date localDate = new Date(l1);
        if ((localObject1 == null) || (paramString == null)) {
          break label768;
        }
        com.daamitt.prime.sdk.a.e.b();
        if (l1 <= 0L) {
          break label762;
        }
        l2 = 86400000L;
        boolean bool = d.d.a((String)localObject1, paramString, localDate, l2);
        if (!bool)
        {
          try
          {
            Object localObject2 = new StringBuilder();
            ((StringBuilder)localObject2).append(Telephony.Sms.Inbox.CONTENT_URI);
            ((StringBuilder)localObject2).append("/");
            ((StringBuilder)localObject2).append(l3);
            localObject2 = ((StringBuilder)localObject2).toString();
            try
            {
              localb.a((String)localObject1, paramString, localDate, l3, m, (String)localObject2);
            }
            catch (RuntimeException paramString) {}
            com.daamitt.prime.sdk.a.e.c();
          }
          catch (RuntimeException paramString) {}
          if (i == null) {
            throw paramString;
          }
        }
        else
        {
          com.daamitt.prime.sdk.a.e.b();
        }
        paramContext.moveToNext();
        k += 1;
        broadcastProgress(e, k, i);
        l2 = l1;
      }
      paramContext.close();
    }
    broadcastFinish(e);
    paramContext = b.a;
    com.daamitt.prime.sdk.a.e.b();
    return;
    paramContext = "Something went wrong while parsing";
    if (mPrimeParsingManager.i == null)
    {
      paramContext = "Rules not available";
      if (mCompositeDisposable == null) {
        paramContext = "Destroy has been called on this object.";
      }
    }
    paramScoreCallback.onError(paramContext);
    return;
    paramScoreCallback.onError("Rules file not formatted correctly");
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.PrimeSdk
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */