package com.daamitt.prime.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.d;
import android.text.TextUtils;
import com.daamitt.prime.sdk.a.e;
import com.daamitt.prime.sdk.a.j;
import io.reactivex.k;

final class PrimeSdk$1
  extends BroadcastReceiver
{
  PrimeSdk$1(PrimeSdk paramPrimeSdk) {}
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    if (TextUtils.equals(paramIntent.getAction(), "prime.sdk.PRIME_PROGRESS"))
    {
      int i = paramIntent.getIntExtra("prime.sdk.EXTRA_PROGRESS", 0);
      int j = paramIntent.getIntExtra("prime.sdk.EXTRA_MAX", 0);
      PrimeSdk.access$000(a).onScoreDataProgress(i, j);
      return;
    }
    if (TextUtils.equals(paramIntent.getAction(), "prime.sdk.PRIME_FINISH"))
    {
      if (PrimeSdk.access$100(a) == null)
      {
        PrimeSdk.access$200();
        e.c();
        d.a(paramContext).a(this);
        PrimeSdk.access$300(a, paramContext);
        return;
      }
      paramIntent = new j();
      long l = System.currentTimeMillis();
      PrimeSdk.access$100(a).a(paramIntent.a(paramContext).a(io.reactivex.android.b.a.a()).a(new -..Lambda.PrimeSdk.1.g05bcbMyFXjnhQvrfzzurqMGYaY(this, l, paramIntent, paramContext), new -..Lambda.PrimeSdk.1.0nKziViHl7uhR-vAcI4vMAYz0-E(this, paramContext)));
      d.a(paramContext).a(this);
      return;
    }
    if (TextUtils.equals(paramIntent.getAction(), "prime.sdk.PRIME_REQUEST_FOR_READ_SMS_PERM"))
    {
      PrimeSdk.access$000(a).onError("Cannot perform scoring without read SMS permission");
      d.a(paramContext).a(this);
    }
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.PrimeSdk.1
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */