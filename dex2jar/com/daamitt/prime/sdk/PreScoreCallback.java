package com.daamitt.prime.sdk;

import android.support.annotation.Keep;

@Keep
public abstract interface PreScoreCallback
{
  public abstract void onError(String paramString);
  
  public abstract void onPreScoreData(String paramString);
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.PreScoreCallback
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */