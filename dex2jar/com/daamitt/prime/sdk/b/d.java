package com.daamitt.prime.sdk.b;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import com.daamitt.prime.sdk.a.e;
import com.daamitt.prime.sdk.a.i.a;
import com.daamitt.prime.sdk.a.k;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class d
{
  private static final String c = "d";
  private static d d;
  private static String e = "COUNT(*) as bounceCount ";
  private static String f = "COUNT(*) as notPaidCount ";
  public SQLiteDatabase a = null;
  public String[] b = { "_id", "smsId", "sender", "date", "body", "lat", "long", "locAccuracy", "tags", "accountId", "parsed", "UUID", "modifyCount", "smsFlags", "patternUID", "previousUUID", "URI", "probability", "simSubscriptionId", "simSlotId", "threadId", "metaData" };
  
  public static k a(Cursor paramCursor)
  {
    int n = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("_id"));
    Object localObject = paramCursor.getString(paramCursor.getColumnIndexOrThrow("sender"));
    Date localDate = new Date(paramCursor.getLong(paramCursor.getColumnIndexOrThrow("date")));
    String str4 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("body"));
    int i1 = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("accountId"));
    long l = paramCursor.getLong(paramCursor.getColumnIndexOrThrow("smsId"));
    int i = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("smsFlags"));
    String str1 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("UUID"));
    String str2 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("previousUUID"));
    String str3 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("URI"));
    double d1 = paramCursor.getDouble(paramCursor.getColumnIndexOrThrow("probability"));
    int j = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("simSubscriptionId"));
    int k = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("simSlotId"));
    int m = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("threadId"));
    localObject = new k((String)localObject, str4, localDate);
    C = l;
    o = n;
    w = i1;
    n = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("parsed"));
    boolean bool = true;
    if (n != 1) {
      bool = false;
    }
    E = bool;
    G = i;
    H = str1;
    I = str2;
    M = str3;
    ((k)localObject).a(d1);
    O = j;
    P = k;
    Q = m;
    return (k)localObject;
  }
  
  public static d a(b paramb)
  {
    if (d == null)
    {
      d locald = new d();
      d = locald;
      a = paramb.getWritableDatabase();
    }
    return d;
  }
  
  public static void a(SQLiteDatabase paramSQLiteDatabase)
  {
    e.b();
    paramSQLiteDatabase.execSQL("create table if not exists sms(_id integer primary key autoincrement,smsId integer not null,sender text not null,date integer not null,body text not null,lat double default 360,long double default 360,locAccuracy double default -1, tags text not null default other,accountId integer not null,parsed boolean default 0,UUID text,modifyCount integer default 1,smsFlags integer default 0,patternUID  integer default 0,previousUUID text,URI text, probability real,simSubscriptionId integer,simSlotId integer,threadId integer,creator text,metaData real);");
    paramSQLiteDatabase.execSQL("create trigger if not exists SmsTriggerModifiedFlag After update on sms for each row  Begin  Update sms Set modifyCount = modifyCount + 1  Where _id =  New._id;  End; ");
  }
  
  public static void a(SQLiteDatabase paramSQLiteDatabase, int paramInt)
  {
    
    if (paramInt != 1) {
      return;
    }
    e.b();
    paramSQLiteDatabase.execSQL("ALTER TABLE sms ADD COLUMN metaData real");
  }
  
  public static void b(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.beginTransaction();
    try
    {
      paramSQLiteDatabase.execSQL("drop table if exists sms");
      paramSQLiteDatabase.execSQL("drop trigger if exists SmsTriggerModifiedFlag");
      a(paramSQLiteDatabase);
      paramSQLiteDatabase.setTransactionSuccessful();
      return;
    }
    finally
    {
      paramSQLiteDatabase.endTransaction();
    }
  }
  
  public final int a(long paramLong, ContentValues paramContentValues)
  {
    if (paramLong >= 0L) {
      return a.update("sms", paramContentValues, "_id = ".concat(String.valueOf(paramLong)), null);
    }
    e.a();
    return -1;
  }
  
  public final k a(long paramLong)
  {
    Cursor localCursor = a.query("sms", b, "_id = ".concat(String.valueOf(paramLong)), null, null, null, null);
    k localk = null;
    if ((localCursor != null) && (localCursor.getCount() > 0))
    {
      localCursor.moveToFirst();
      if (!localCursor.isAfterLast()) {
        localk = a(localCursor);
      }
      localCursor.close();
      return localk;
    }
    if (localCursor != null) {
      localCursor.close();
    }
    return null;
  }
  
  public final boolean a(String paramString1, String paramString2, Date paramDate, long paramLong)
  {
    String str = "COUNT(_id) AS ".concat(String.valueOf("total"));
    paramString1 = paramString1.replace("-", "");
    String[] arrayOfString;
    if (paramLong > 0L)
    {
      long l1 = paramDate.getTime();
      long l2 = paramDate.getTime();
      paramString1 = paramString1.toUpperCase();
      paramDate = "date >=? AND date <=? AND  replace(sender,'-','') =? AND body =? ";
      arrayOfString = new String[] { String.valueOf(l1 - paramLong), String.valueOf(l2 + paramLong), paramString1, paramString2 };
    }
    else
    {
      arrayOfString = new String[] { String.valueOf(paramDate.getTime()), paramString1.toUpperCase(), paramString2 };
      paramDate = "date =? AND replace(sender,'-','') =? AND body =? ";
    }
    paramString1 = null;
    try
    {
      paramDate = a.query("sms", new String[] { str }, paramDate, arrayOfString, null, null, null);
      paramString1 = paramDate;
      paramDate.moveToFirst();
      paramString1 = paramDate;
      if (!paramDate.isAfterLast())
      {
        paramString1 = paramDate;
        i = paramDate.getInt(paramDate.getColumnIndex("total"));
        paramString1 = paramDate;
      }
      else
      {
        i = 0;
        paramString1 = paramDate;
      }
    }
    catch (SQLiteException paramDate)
    {
      int i;
      int j;
      for (;;) {}
    }
    j = paramString2.codePointCount(0, paramString2.length());
    i = 0;
    while (i < j)
    {
      String.format("%04x", new Object[] { Integer.valueOf(paramString2.codePointAt(i)) });
      i += 1;
    }
    e.c();
    i = 0;
    if (paramString1 != null) {
      paramString1.close();
    }
    return i > 0;
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2)
  {
    int k = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    e.a();
    if (paramCalendar1.before(paramCalendar2))
    {
      String[] arrayOfString = new String[k];
      Object localObject = new StringBuilder(" date between ");
      ((StringBuilder)localObject).append(paramCalendar1.getTime().getTime());
      ((StringBuilder)localObject).append(" AND ");
      ((StringBuilder)localObject).append(paramCalendar2.getTime().getTime());
      ((StringBuilder)localObject).append(" AND parsed != 1 ");
      paramCalendar2 = ((StringBuilder)localObject).toString();
      localObject = Calendar.getInstance().getTimeZone();
      long l2 = ((TimeZone)localObject).getRawOffset() + ((TimeZone)localObject).getDSTSavings();
      long l1 = l2 / 3600000L;
      l2 = l2 % 3600000L / 60000L;
      localObject = new StringBuilder("strftime('%j',date(date/1000,'unixepoch','");
      ((StringBuilder)localObject).append(l1);
      ((StringBuilder)localObject).append(" hours','");
      ((StringBuilder)localObject).append(l2);
      ((StringBuilder)localObject).append(" minutes')) as Day");
      localObject = ((StringBuilder)localObject).toString();
      int j = 0;
      String str = e;
      paramCalendar2 = a.query("sms", new String[] { localObject, str }, paramCalendar2, null, "Day", null, null, null);
      if ((paramCalendar2 != null) && (paramCalendar2.getCount() > 0))
      {
        paramCalendar2.moveToFirst();
        int m = paramCalendar1.get(6);
        int n = paramCalendar1.getActualMaximum(6);
        while (!paramCalendar2.isAfterLast())
        {
          localObject = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("Day"));
          paramCalendar1 = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("bounceCount"));
          try
          {
            i = Integer.parseInt((String)localObject);
          }
          catch (NumberFormatException localNumberFormatException)
          {
            int i;
            for (;;) {}
          }
          i = -1;
          if ((i != -1) && (paramCalendar1 != null)) {
            if (i >= m) {
              arrayOfString[(k - (i - m) - 1)] = paramCalendar1;
            } else {
              arrayOfString[(k - (n - m + i) - 1)] = paramCalendar1;
            }
          }
          paramCalendar2.moveToNext();
        }
      }
      i = j;
      if (paramCalendar2 != null)
      {
        paramCalendar2.close();
        i = j;
      }
      while (i < k)
      {
        if (TextUtils.isEmpty(arrayOfString[i])) {
          arrayOfString[i] = "";
        }
        i += 1;
      }
      return arrayOfString;
    }
    return null;
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt)
  {
    int j = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    e.a();
    if (paramCalendar1.before(paramCalendar2))
    {
      String[] arrayOfString = new String[j];
      Object localObject = new StringBuilder(" date between ");
      ((StringBuilder)localObject).append(paramCalendar1.getTime().getTime());
      ((StringBuilder)localObject).append(" AND ");
      ((StringBuilder)localObject).append(paramCalendar2.getTime().getTime());
      paramCalendar2 = ((StringBuilder)localObject).toString();
      if (paramInt != 0)
      {
        localObject = new StringBuilder();
        ((StringBuilder)localObject).append(paramCalendar2);
        ((StringBuilder)localObject).append(" AND smsFlags & ");
        ((StringBuilder)localObject).append(paramInt);
        ((StringBuilder)localObject).append(" != 0 ");
        paramCalendar2 = ((StringBuilder)localObject).toString();
      }
      localObject = Calendar.getInstance().getTimeZone();
      long l2 = ((TimeZone)localObject).getRawOffset() + ((TimeZone)localObject).getDSTSavings();
      long l1 = l2 / 3600000L;
      l2 = l2 % 3600000L / 60000L;
      localObject = new StringBuilder("strftime('%j',date(date/1000,'unixepoch','");
      ((StringBuilder)localObject).append(l1);
      ((StringBuilder)localObject).append(" hours','");
      ((StringBuilder)localObject).append(l2);
      ((StringBuilder)localObject).append(" minutes')) as Day");
      localObject = ((StringBuilder)localObject).toString();
      int i = 0;
      String str = e;
      paramCalendar2 = a.query("sms", new String[] { localObject, str }, paramCalendar2, null, "Day", null, null, null);
      if ((paramCalendar2 != null) && (paramCalendar2.getCount() > 0))
      {
        paramCalendar2.moveToFirst();
        int k = paramCalendar1.get(6);
        int m = paramCalendar1.getActualMaximum(6);
        while (!paramCalendar2.isAfterLast())
        {
          localObject = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("Day"));
          paramCalendar1 = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("bounceCount"));
          try
          {
            paramInt = Integer.parseInt((String)localObject);
          }
          catch (NumberFormatException localNumberFormatException)
          {
            for (;;) {}
          }
          paramInt = -1;
          if ((paramInt != -1) && (paramCalendar1 != null)) {
            if (paramInt >= k) {
              arrayOfString[(j - (paramInt - k) - 1)] = paramCalendar1;
            } else {
              arrayOfString[(j - (m - k + paramInt) - 1)] = paramCalendar1;
            }
          }
          paramCalendar2.moveToNext();
        }
      }
      paramInt = i;
      if (paramCalendar2 != null)
      {
        paramCalendar2.close();
        paramInt = i;
      }
      while (paramInt < j)
      {
        if (TextUtils.isEmpty(arrayOfString[paramInt])) {
          arrayOfString[paramInt] = "";
        }
        paramInt += 1;
      }
      return arrayOfString;
    }
    return null;
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2, ArrayList<String> paramArrayList)
  {
    int k = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    e.a();
    if (paramCalendar1.before(paramCalendar2))
    {
      String[] arrayOfString = new String[k];
      Object localObject = new StringBuilder(" date between ");
      ((StringBuilder)localObject).append(paramCalendar1.getTime().getTime());
      ((StringBuilder)localObject).append(" AND ");
      ((StringBuilder)localObject).append(paramCalendar2.getTime().getTime());
      ((StringBuilder)localObject).append(" AND smsFlags & 16777216 != 0 ");
      paramCalendar2 = ((StringBuilder)localObject).toString();
      int j = 0;
      if ((paramArrayList != null) && (paramArrayList.size() > 0))
      {
        localObject = new StringBuilder();
        ((StringBuilder)localObject).append(paramCalendar2);
        ((StringBuilder)localObject).append(" AND ( ");
        paramCalendar2 = ((StringBuilder)localObject).toString();
        i = 0;
        while (i < paramArrayList.size())
        {
          String str = (String)paramArrayList.get(i);
          localObject = paramCalendar2;
          if (i != 0)
          {
            localObject = new StringBuilder();
            ((StringBuilder)localObject).append(paramCalendar2);
            ((StringBuilder)localObject).append(" OR ");
            localObject = ((StringBuilder)localObject).toString();
          }
          paramCalendar2 = new StringBuilder();
          paramCalendar2.append((String)localObject);
          paramCalendar2.append(" lower(sender) LIKE '");
          paramCalendar2.append(str);
          paramCalendar2.append("' ");
          paramCalendar2 = paramCalendar2.toString();
          i += 1;
        }
        paramArrayList = new StringBuilder();
        paramArrayList.append(paramCalendar2);
        paramArrayList.append(")");
        paramCalendar2 = paramArrayList.toString();
      }
      paramArrayList = Calendar.getInstance().getTimeZone();
      long l2 = paramArrayList.getRawOffset() + paramArrayList.getDSTSavings();
      long l1 = l2 / 3600000L;
      l2 = l2 % 3600000L / 60000L;
      paramArrayList = new StringBuilder("strftime('%j',date(date/1000,'unixepoch','");
      paramArrayList.append(l1);
      paramArrayList.append(" hours','");
      paramArrayList.append(l2);
      paramArrayList.append(" minutes')) as Day");
      paramArrayList = paramArrayList.toString();
      localObject = f;
      paramCalendar2 = a.query("sms", new String[] { paramArrayList, localObject }, paramCalendar2, null, "Day", null, null, null);
      if ((paramCalendar2 != null) && (paramCalendar2.getCount() > 0))
      {
        paramCalendar2.moveToFirst();
        int m = paramCalendar1.get(6);
        int n = paramCalendar1.getActualMaximum(6);
        while (!paramCalendar2.isAfterLast())
        {
          paramArrayList = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("Day"));
          paramCalendar1 = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("notPaidCount"));
          try
          {
            i = Integer.parseInt(paramArrayList);
          }
          catch (NumberFormatException paramArrayList)
          {
            for (;;) {}
          }
          i = -1;
          if ((i != -1) && (paramCalendar1 != null)) {
            if (i >= m) {
              arrayOfString[(k - (i - m) - 1)] = paramCalendar1;
            } else {
              arrayOfString[(k - (n - m + i) - 1)] = paramCalendar1;
            }
          }
          paramCalendar2.moveToNext();
        }
      }
      int i = j;
      if (paramCalendar2 != null)
      {
        paramCalendar2.close();
        i = j;
      }
      while (i < k)
      {
        if (TextUtils.isEmpty(arrayOfString[i])) {
          arrayOfString[i] = "";
        }
        i += 1;
      }
      return arrayOfString;
    }
    return null;
  }
  
  public final String[] b(Calendar paramCalendar1, Calendar paramCalendar2)
  {
    int k = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    e.a();
    if (paramCalendar1.before(paramCalendar2))
    {
      String[] arrayOfString = new String[k];
      Object localObject = new StringBuilder(" date between ");
      ((StringBuilder)localObject).append(paramCalendar1.getTime().getTime());
      ((StringBuilder)localObject).append(" AND ");
      ((StringBuilder)localObject).append(paramCalendar2.getTime().getTime());
      ((StringBuilder)localObject).append(" AND parsed != 1  AND smsFlags & 134217728 != 0 ");
      paramCalendar2 = ((StringBuilder)localObject).toString();
      localObject = Calendar.getInstance().getTimeZone();
      long l2 = ((TimeZone)localObject).getRawOffset() + ((TimeZone)localObject).getDSTSavings();
      long l1 = l2 / 3600000L;
      l2 = l2 % 3600000L / 60000L;
      localObject = new StringBuilder("strftime('%j',date(date/1000,'unixepoch','");
      ((StringBuilder)localObject).append(l1);
      ((StringBuilder)localObject).append(" hours','");
      ((StringBuilder)localObject).append(l2);
      ((StringBuilder)localObject).append(" minutes')) as Day");
      localObject = ((StringBuilder)localObject).toString();
      int j = 0;
      paramCalendar2 = a.query("sms", new String[] { localObject, "metaData" }, paramCalendar2, null, "Day", " MAX(metaData)", null, null);
      if ((paramCalendar2 != null) && (paramCalendar2.getCount() > 0))
      {
        paramCalendar2.moveToFirst();
        int m = paramCalendar1.get(6);
        int n = paramCalendar1.getActualMaximum(6);
        while (!paramCalendar2.isAfterLast())
        {
          localObject = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("Day"));
          paramCalendar1 = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("metaData"));
          try
          {
            i = Integer.parseInt((String)localObject);
          }
          catch (NumberFormatException localNumberFormatException)
          {
            int i;
            int i1;
            for (;;) {}
          }
          i = -1;
          if ((i != -1) && (paramCalendar1 != null))
          {
            i1 = (int)Double.valueOf(paramCalendar1).doubleValue();
            if (i >= m) {
              arrayOfString[(k - (i - m) - 1)] = String.valueOf(i1);
            } else {
              arrayOfString[(k - (n - m + i) - 1)] = String.valueOf(i1);
            }
          }
          paramCalendar2.moveToNext();
        }
      }
      i = j;
      if (paramCalendar2 != null)
      {
        paramCalendar2.close();
        i = j;
      }
      while (i < k)
      {
        if (TextUtils.isEmpty(arrayOfString[i])) {
          arrayOfString[i] = "";
        }
        i += 1;
      }
      return arrayOfString;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.b.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */