package com.daamitt.prime.sdk.b;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.daamitt.prime.sdk.a.k;
import com.daamitt.prime.sdk.a.l;
import com.daamitt.prime.sdk.a.m;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class b
  extends SQLiteOpenHelper
{
  private static final String f = "b";
  private static b g;
  public a a;
  public f b;
  public e c;
  public d d;
  public c e;
  
  private b(Context paramContext)
  {
    super(paramContext, "sdk.db", null, 2);
  }
  
  public static b a(Context paramContext)
  {
    try
    {
      if (g == null)
      {
        paramContext = new b(paramContext);
        g = paramContext;
        a = a.a(paramContext);
        paramContext = g;
        b = f.a(paramContext);
        paramContext = g;
        c = e.a(paramContext);
        paramContext = g;
        d = d.a(paramContext);
        paramContext = g;
        e = c.a(paramContext);
      }
      paramContext = g;
      return paramContext;
    }
    finally {}
  }
  
  public final int a(long paramLong, ContentValues paramContentValues)
  {
    return d.a(paramLong, paramContentValues);
  }
  
  public final int a(com.daamitt.prime.sdk.a.a parama, ContentValues paramContentValues)
  {
    return a.a(parama, paramContentValues);
  }
  
  public final int a(l paraml, ContentValues paramContentValues)
  {
    return c.a(paraml, paramContentValues);
  }
  
  public final com.daamitt.prime.sdk.a.a a(String paramString1, String paramString2, int paramInt)
  {
    return a.a(paramString1, paramString2, paramInt);
  }
  
  public final com.daamitt.prime.sdk.a.a a(String paramString1, String paramString2, int paramInt, boolean paramBoolean, String paramString3)
  {
    return a.a(paramString1, paramString2, paramInt, paramBoolean, paramString3);
  }
  
  public final k a(long paramLong)
  {
    return d.a(paramLong);
  }
  
  public final k a(String paramString)
  {
    Object localObject2 = d;
    Object localObject1 = null;
    if (paramString == null) {
      return null;
    }
    localObject2 = a.query("sms", b, "UUID =?", new String[] { paramString }, null, null, null);
    paramString = (String)localObject1;
    if (localObject2 != null)
    {
      paramString = (String)localObject1;
      if (((Cursor)localObject2).getCount() > 0)
      {
        ((Cursor)localObject2).moveToFirst();
        paramString = (String)localObject1;
        if (!((Cursor)localObject2).isAfterLast()) {
          paramString = d.a((Cursor)localObject2);
        }
      }
    }
    if (localObject2 != null) {
      ((Cursor)localObject2).close();
    }
    return paramString;
  }
  
  public final ArrayList<com.daamitt.prime.sdk.a.a> a(int paramInt)
  {
    return a.a(paramInt);
  }
  
  public final ArrayList<Integer> a(int[] paramArrayOfInt)
  {
    return b.a(paramArrayOfInt);
  }
  
  public final void a(com.daamitt.prime.sdk.a.a parama1, com.daamitt.prime.sdk.a.a parama2)
  {
    if ((3 == i) || (6 == i)) {
      c.a(parama1, parama2);
    }
    com.daamitt.prime.sdk.a.b localb1 = m;
    com.daamitt.prime.sdk.a.b localb2 = m;
    if ((c != null) && ((c == null) || (c.after(c))))
    {
      c = c;
      a = a;
    }
    if ((d != null) && ((d == null) || (d.after(d))))
    {
      c = d;
      b = b;
    }
    long l = System.currentTimeMillis();
    b.a(parama1, l);
    a.a(parama1, parama2, l);
  }
  
  public final void a(m paramm, ContentValues paramContentValues)
  {
    b.a(paramm, paramContentValues);
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt)
  {
    return b.d(paramCalendar1, paramCalendar2, paramInt);
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt, String paramString1, String paramString2)
  {
    return b.a(paramCalendar1, paramCalendar2, paramInt, paramString1, paramString2);
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2, ArrayList<String> paramArrayList)
  {
    return d.a(paramCalendar1, paramCalendar2, paramArrayList);
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2, int[] paramArrayOfInt, int paramInt)
  {
    return e.a(paramCalendar1, paramCalendar2, paramArrayOfInt, paramInt);
  }
  
  public final com.daamitt.prime.sdk.a.a b(int paramInt)
  {
    return a.b(paramInt);
  }
  
  public final com.daamitt.prime.sdk.a.a b(long paramLong)
  {
    return a.b(paramLong);
  }
  
  public final ArrayList<com.daamitt.prime.sdk.a.a> b(int[] paramArrayOfInt)
  {
    return a.b(paramArrayOfInt);
  }
  
  public final String[] b(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt)
  {
    return d.a(paramCalendar1, paramCalendar2, paramInt);
  }
  
  public final String c(long paramLong)
  {
    return a.c(paramLong);
  }
  
  public final String c(int[] paramArrayOfInt)
  {
    a locala = a;
    int i = 0;
    while (i <= 0)
    {
      Object localObject = locala.a(paramArrayOfInt[0]);
      ArrayList localArrayList = new ArrayList();
      localArrayList.add(localObject);
      localObject = "UUID IN ".concat(String.valueOf(locala.a(0, (String)localObject, localArrayList)));
      localObject = a.query("accounts", new String[] { "_id" }, (String)localObject, null, null, null, null);
      if ((localObject != null) && (((Cursor)localObject).getCount() > 0))
      {
        paramArrayOfInt = new ArrayList();
        ((Cursor)localObject).moveToFirst();
        while (!((Cursor)localObject).isAfterLast())
        {
          paramArrayOfInt.add(((Cursor)localObject).getString(((Cursor)localObject).getColumnIndexOrThrow("_id")));
          ((Cursor)localObject).moveToNext();
        }
        paramArrayOfInt = a.a((String[])paramArrayOfInt.toArray(new String[paramArrayOfInt.size()]));
        ((Cursor)localObject).close();
        return paramArrayOfInt;
      }
      if (localObject != null) {
        ((Cursor)localObject).close();
      }
      i += 1;
    }
    return a.a(paramArrayOfInt);
  }
  
  public void onCreate(SQLiteDatabase paramSQLiteDatabase)
  {
    a.a(paramSQLiteDatabase);
    f.a(paramSQLiteDatabase);
    e.a(paramSQLiteDatabase);
    d.a(paramSQLiteDatabase);
    c.a(paramSQLiteDatabase);
  }
  
  public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    com.daamitt.prime.sdk.a.e.b();
    d.a(paramSQLiteDatabase, paramInt1);
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.b.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */