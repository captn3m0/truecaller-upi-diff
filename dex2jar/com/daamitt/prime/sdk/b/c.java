package com.daamitt.prime.sdk.b;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.text.TextUtils;
import com.daamitt.prime.sdk.a.c.a;
import com.daamitt.prime.sdk.a.d;
import com.daamitt.prime.sdk.a.e;
import com.daamitt.prime.sdk.a.h;
import com.daamitt.prime.sdk.a.i.a;
import com.daamitt.prime.sdk.a.k;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.TimeZone;

public class c
{
  public static final String a = "c";
  private static String[] d = { "_id", "wSmsId", "name", "pnr", "dueDate", "type", "flags", "location", "info", "contact", "UUID", "modifyCount", "amount", "reminderTimeSpan" };
  private static String[] e = { "events._id", "events.wSmsId", "events.name", "events.pnr", "events.dueDate", "events.type", "events.flags", "events.location", "events.info", "events.contact", "events.UUID", "events.modifyCount", "events.amount", "events.reminderTimeSpan" };
  private static c f;
  private static String g = "COUNT(*) as eventCount ";
  private static String h = "events JOIN sms ON events.wSmsId = sms._id";
  public SQLiteDatabase b = null;
  private b c;
  
  private c(b paramb)
  {
    c = paramb;
  }
  
  private static d a(Cursor paramCursor)
  {
    int i = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("_id"));
    int j = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("wSmsId"));
    String str1 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("name"));
    String str2 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("pnr"));
    String str3 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("location"));
    String str4 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("info"));
    int k = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("flags"));
    double d1 = paramCursor.getDouble(paramCursor.getColumnIndexOrThrow("amount"));
    String str5 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("contact"));
    long l = paramCursor.getLong(paramCursor.getColumnIndexOrThrow("reminderTimeSpan"));
    String str6 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("UUID"));
    Object localObject = Calendar.getInstance();
    ((Calendar)localObject).setTimeInMillis(paramCursor.getLong(paramCursor.getColumnIndexOrThrow("dueDate")));
    localObject = ((Calendar)localObject).getTime();
    int m = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("type"));
    paramCursor = new d(null, null, null);
    o = i;
    C = j;
    u = 5;
    paramCursor.a(null, "Events");
    paramCursor.a(str1, str2, (Date)localObject, m);
    f = str3;
    g = str4;
    h = k;
    paramCursor.j = str5;
    paramCursor.i = d1;
    paramCursor.m = l;
    n = str6;
    return paramCursor;
  }
  
  public static c a(b paramb)
  {
    if (f == null)
    {
      c localc = new c(paramb);
      f = localc;
      b = paramb.getWritableDatabase();
    }
    return f;
  }
  
  public static void a(SQLiteDatabase paramSQLiteDatabase)
  {
    e.b();
    paramSQLiteDatabase.execSQL("create table if not exists events(_id integer primary key autoincrement, wSmsId integer not null, name text not null, pnr text not null, dueDate integer not null, type integer not null, flags integer default 0,location text, info text, contact text,UUID text,modifyCount integer default 1,amount real default 0,reminderTimeSpan integer default 1800000);");
    paramSQLiteDatabase.execSQL("create trigger if not exists EventsTriggerModifiedFlag After update on events for each row  Begin  Update events Set modifyCount = modifyCount + 1  Where _id =  New._id;  End; ");
  }
  
  public static void b(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.beginTransaction();
    try
    {
      paramSQLiteDatabase.execSQL("drop table if exists events");
      paramSQLiteDatabase.execSQL("drop trigger if exists EventsTriggerModifiedFlag");
      a(paramSQLiteDatabase);
      paramSQLiteDatabase.setTransactionSuccessful();
      return;
    }
    finally
    {
      paramSQLiteDatabase.endTransaction();
    }
  }
  
  public final d a(ArrayList<c.a> paramArrayList, d paramd)
  {
    e.a();
    StringBuilder localStringBuilder = new StringBuilder();
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = paramArrayList.iterator();
    int j = 0;
    Object localObject1;
    Object localObject2;
    long l1;
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      int i = 1;
      if (!bool) {
        break;
      }
      c.a locala = (c.a)localIterator.next();
      paramArrayList = a;
      switch (paramArrayList.hashCode())
      {
      default: 
        break;
      case 984038195: 
        if (paramArrayList.equals("event_info")) {
          i = 2;
        }
        break;
      case 951526432: 
        if (paramArrayList.equals("contact")) {
          i = 4;
        }
        break;
      case 943500218: 
        if (paramArrayList.equals("event_location")) {
          i = 3;
        }
        break;
      case 3373707: 
        if (paramArrayList.equals("name")) {
          i = 0;
        }
        break;
      case 3076014: 
        if (paramArrayList.equals("date")) {
          i = 5;
        }
        break;
      case 111156: 
        if (!paramArrayList.equals("pnr")) {
          break;
        }
        break;
      case -102973965: 
        if (paramArrayList.equals("sms_time")) {
          i = 6;
        }
        break;
      }
      i = -1;
      switch (i)
      {
      default: 
        break;
      case 6: 
        paramArrayList = "date";
        break;
      case 5: 
        paramArrayList = "dueDate";
        break;
      case 4: 
        paramArrayList = "contact";
        break;
      case 3: 
        paramArrayList = "location";
        break;
      case 2: 
        paramArrayList = "info";
        break;
      case 1: 
        paramArrayList = "pnr";
        break;
      case 0: 
        paramArrayList = "name";
      }
      localObject1 = b;
      localObject2 = paramd.b((String)localObject1);
      if (localObject2 != null) {
        localObject1 = localObject2;
      }
      localObject2 = c;
      if ((!TextUtils.equals(paramArrayList, "dueDate")) && (!TextUtils.equals(paramArrayList, "date")))
      {
        if (TextUtils.equals(paramArrayList, "deleted"))
        {
          i = d;
          if (i == 3)
          {
            if (!TextUtils.isEmpty(localStringBuilder)) {
              localStringBuilder.append(" AND ");
            }
            localStringBuilder.append("flags & 4 = 0");
            j = i;
          }
          else
          {
            j = i;
            if (i == 2)
            {
              if (!TextUtils.isEmpty(localStringBuilder)) {
                localStringBuilder.append(" AND ");
              }
              localStringBuilder.append("flags & 4 != 0");
              j = i;
            }
          }
        }
        else if (TextUtils.equals(paramArrayList, "pattern_UID"))
        {
          i = d;
          if (i == 3)
          {
            if (!TextUtils.isEmpty(localStringBuilder)) {
              localStringBuilder.append(" AND ");
            }
            localStringBuilder.append("patternUID !=?");
            localArrayList.add(String.valueOf(F.j));
          }
          else if (i == 2)
          {
            if (!TextUtils.isEmpty(localStringBuilder)) {
              localStringBuilder.append(" AND ");
            }
            localStringBuilder.append("patternUID =?");
            localArrayList.add(String.valueOf(F.j));
          }
        }
        else if (TextUtils.equals((CharSequence)localObject2, "contains"))
        {
          if (!TextUtils.isEmpty(localStringBuilder)) {
            localStringBuilder.append(" AND ");
          }
          localStringBuilder.append(paramArrayList);
          localStringBuilder.append(" LIKE '%");
          localStringBuilder.append((String)localObject1);
          localStringBuilder.append("%'");
        }
        else if (TextUtils.equals((CharSequence)localObject2, "exact"))
        {
          if (!TextUtils.isEmpty(localStringBuilder)) {
            localStringBuilder.append(" AND ");
          }
          localStringBuilder.append(paramArrayList);
          localStringBuilder.append(" =? ");
          localArrayList.add(localObject1);
        }
      }
      else
      {
        if (!TextUtils.isEmpty(localStringBuilder)) {
          localStringBuilder.append(" AND ");
        }
        l1 = e;
        long l2 = Long.valueOf((String)localObject1).longValue();
        long l3 = Long.valueOf((String)localObject1).longValue();
        localStringBuilder.append(paramArrayList);
        localStringBuilder.append(" >=? AND ");
        localStringBuilder.append(paramArrayList);
        localStringBuilder.append(" <=?");
        localArrayList.add(String.valueOf(l2 - l1));
        localArrayList.add(String.valueOf(l3 + l1));
      }
    }
    paramArrayList = new SQLiteQueryBuilder();
    paramArrayList.setTables("events JOIN sms ON sms._id = events.wSmsId");
    try
    {
      paramd = paramArrayList.query(b, e, localStringBuilder.toString(), (String[])localArrayList.toArray(new String[localArrayList.size()]), null, null, "events._id DESC");
    }
    catch (RuntimeException paramArrayList)
    {
      for (;;) {}
    }
    e.b();
    paramd = null;
    if ((paramd != null) && (paramd.getCount() > 0))
    {
      paramd.moveToFirst();
      if (!paramd.isAfterLast())
      {
        localObject1 = a(paramd);
        paramArrayList = (ArrayList<c.a>)localObject1;
        if ((h & 0x1) != 0) {
          break label1086;
        }
        paramArrayList = (ArrayList<c.a>)localObject1;
        if (j != 1) {
          break label1086;
        }
        l1 = C;
        localObject2 = c.a(l1);
        paramArrayList = (ArrayList<c.a>)localObject1;
        while (I != null)
        {
          localObject2 = c.a(I);
          l1 = o;
          localObject1 = b.query("events", d, "wSmsId = ".concat(String.valueOf(l1)), null, null, null, null);
          if ((localObject1 != null) && (((Cursor)localObject1).getCount() > 0))
          {
            ((Cursor)localObject1).moveToFirst();
            if (!((Cursor)localObject1).isAfterLast())
            {
              paramArrayList = a((Cursor)localObject1);
              break label1057;
            }
          }
          paramArrayList = null;
          label1057:
          if (localObject1 != null) {
            ((Cursor)localObject1).close();
          }
          if ((h & 0x1) != 0) {
            break;
          }
        }
      }
    }
    paramArrayList = null;
    label1086:
    if (paramd != null) {
      paramd.close();
    }
    return paramArrayList;
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2, int[] paramArrayOfInt, int paramInt)
  {
    int k = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    e.c();
    boolean bool = paramCalendar1.before(paramCalendar2);
    StringBuilder localStringBuilder = null;
    if (bool)
    {
      String[] arrayOfString = new String[k];
      int j = 0;
      Object localObject = localStringBuilder;
      int i;
      if (paramArrayOfInt != null)
      {
        localObject = localStringBuilder;
        if (paramArrayOfInt.length > 0)
        {
          localObject = new StringBuilder(paramArrayOfInt.length * 2 - 1);
          ((StringBuilder)localObject).append(paramArrayOfInt[0]);
          i = 1;
          while (i < paramArrayOfInt.length)
          {
            localStringBuilder = new StringBuilder(",");
            localStringBuilder.append(paramArrayOfInt[i]);
            ((StringBuilder)localObject).append(localStringBuilder.toString());
            i += 1;
          }
          localObject = ((StringBuilder)localObject).toString();
        }
      }
      paramArrayOfInt = new StringBuilder("events.type in ( ");
      paramArrayOfInt.append((String)localObject);
      paramArrayOfInt.append("  )  AND sms.date between ");
      paramArrayOfInt.append(paramCalendar1.getTime().getTime());
      paramArrayOfInt.append(" AND ");
      paramArrayOfInt.append(paramCalendar2.getTime().getTime());
      paramArrayOfInt.append(" AND sms.body NOTNULL");
      paramArrayOfInt = paramArrayOfInt.toString();
      paramCalendar2 = paramArrayOfInt;
      if (paramInt != 0)
      {
        paramCalendar2 = new StringBuilder();
        paramCalendar2.append(paramArrayOfInt);
        paramCalendar2.append(" AND flags & ");
        paramCalendar2.append(paramInt);
        paramCalendar2.append(" != 0 ");
        paramCalendar2 = paramCalendar2.toString();
      }
      paramArrayOfInt = new StringBuilder();
      paramArrayOfInt.append(paramCalendar2);
      paramArrayOfInt.append(" AND flags & 8 = 0 ");
      paramCalendar2 = paramArrayOfInt.toString();
      paramArrayOfInt = Calendar.getInstance().getTimeZone();
      long l2 = paramArrayOfInt.getRawOffset() + paramArrayOfInt.getDSTSavings();
      long l1 = l2 / 3600000L;
      l2 = l2 % 3600000L / 60000L;
      paramArrayOfInt = new StringBuilder("strftime('%j',date(date/1000,'unixepoch','");
      paramArrayOfInt.append(l1);
      paramArrayOfInt.append(" hours','");
      paramArrayOfInt.append(l2);
      paramArrayOfInt.append(" minutes')) as Day");
      paramArrayOfInt = paramArrayOfInt.toString();
      localObject = g;
      paramCalendar2 = b.query(h, new String[] { paramArrayOfInt, localObject }, paramCalendar2, null, "Day", null, null, null);
      if ((paramCalendar2 != null) && (paramCalendar2.getCount() > 0))
      {
        paramCalendar2.moveToFirst();
        i = paramCalendar1.get(6);
        int m = paramCalendar1.getActualMaximum(6);
        while (!paramCalendar2.isAfterLast())
        {
          paramArrayOfInt = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("Day"));
          paramCalendar1 = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("eventCount"));
          try
          {
            paramInt = Integer.parseInt(paramArrayOfInt);
          }
          catch (NumberFormatException paramArrayOfInt)
          {
            for (;;) {}
          }
          paramInt = -1;
          if ((paramInt != -1) && (paramCalendar1 != null))
          {
            if (paramInt >= i) {
              paramInt = k - (paramInt - i) - 1;
            } else {
              paramInt = k - (m - i + paramInt) - 1;
            }
            if (paramInt >= 0) {
              arrayOfString[paramInt] = paramCalendar1;
            }
          }
          paramCalendar2.moveToNext();
        }
      }
      paramInt = j;
      if (paramCalendar2 != null)
      {
        paramCalendar2.close();
        paramInt = j;
      }
      while (paramInt < k)
      {
        if (TextUtils.isEmpty(arrayOfString[paramInt])) {
          arrayOfString[paramInt] = "";
        }
        paramInt += 1;
      }
      return arrayOfString;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.b.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */