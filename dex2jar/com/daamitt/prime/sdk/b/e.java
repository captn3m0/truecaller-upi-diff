package com.daamitt.prime.sdk.b;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.text.TextUtils;
import com.daamitt.prime.sdk.a.a;
import com.daamitt.prime.sdk.a.c.a;
import com.daamitt.prime.sdk.a.h;
import com.daamitt.prime.sdk.a.i.a;
import com.daamitt.prime.sdk.a.k;
import com.daamitt.prime.sdk.a.l;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.TimeZone;

public class e
{
  private static final String b = "e";
  private static String[] d = { "_id", "wSmsId", "amount", "minDueAmount", "dueDate", "type", "status", "accountId", "UUID", "modifyCount", "paymentDate", "billType", "txnUUID" };
  private static String[] e = { "statements._id", "statements.wSmsId", "statements.amount", "statements.minDueAmount", "statements.dueDate", "statements.type", "statements.status", "statements.accountId", "statements.UUID", "statements.modifyCount", "statements.paymentDate", "statements.billType", "statements.txnUUID", "pan" };
  private static e f;
  private static String g = "SUM(amount) as Amount ";
  private static String h = "statements JOIN sms ON statements.wSmsId = sms._id";
  public SQLiteDatabase a = null;
  private b c;
  
  private e(b paramb)
  {
    c = paramb;
  }
  
  private int a(long paramLong)
  {
    com.daamitt.prime.sdk.a.e.a();
    String str = "update statements set status = status | 2 where _id = ".concat(String.valueOf(paramLong));
    a.execSQL(str);
    return 1;
  }
  
  private static l a(Cursor paramCursor)
  {
    int i = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("_id"));
    int j = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("wSmsId"));
    double d1 = paramCursor.getDouble(paramCursor.getColumnIndexOrThrow("amount"));
    double d2 = paramCursor.getDouble(paramCursor.getColumnIndexOrThrow("minDueAmount"));
    Object localObject1 = Calendar.getInstance();
    ((Calendar)localObject1).setTimeInMillis(paramCursor.getLong(paramCursor.getColumnIndexOrThrow("dueDate")));
    localObject1 = ((Calendar)localObject1).getTime();
    Object localObject2 = Calendar.getInstance();
    ((Calendar)localObject2).setTimeInMillis(paramCursor.getLong(paramCursor.getColumnIndexOrThrow("paymentDate")));
    localObject2 = ((Calendar)localObject2).getTime();
    int k = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("type"));
    int m = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("accountId"));
    int n = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("status"));
    String str = paramCursor.getString(paramCursor.getColumnIndex("UUID"));
    int i1 = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("billType"));
    paramCursor = paramCursor.getString(paramCursor.getColumnIndexOrThrow("txnUUID"));
    l locall = new l(null, null, null);
    o = i;
    C = j;
    w = m;
    j = n;
    u = 7;
    locall.a(null, "Bills");
    locall.a(null, d1, (Date)localObject1, k);
    h = ((Date)localObject2);
    k = str;
    l = i1;
    e = d2;
    m = paramCursor;
    return locall;
  }
  
  public static e a(b paramb)
  {
    if (f == null)
    {
      e locale = new e(paramb);
      f = locale;
      a = paramb.getWritableDatabase();
    }
    return f;
  }
  
  public static String a(int[] paramArrayOfInt)
  {
    if ((paramArrayOfInt != null) && (paramArrayOfInt.length > 0))
    {
      int j = paramArrayOfInt.length;
      int i = 1;
      StringBuilder localStringBuilder1 = new StringBuilder(j * 2 - 1);
      localStringBuilder1.append(paramArrayOfInt[0]);
      while (i < paramArrayOfInt.length)
      {
        StringBuilder localStringBuilder2 = new StringBuilder(",");
        localStringBuilder2.append(paramArrayOfInt[i]);
        localStringBuilder1.append(localStringBuilder2.toString());
        i += 1;
      }
      return localStringBuilder1.toString();
    }
    return null;
  }
  
  public static void a(SQLiteDatabase paramSQLiteDatabase)
  {
    com.daamitt.prime.sdk.a.e.b();
    paramSQLiteDatabase.execSQL("create table if not exists statements(_id integer primary key autoincrement, wSmsId integer not null, amount real not null, minDueAmount real default -1, dueDate integer not null, type integer not null, status integer default 0, accountId integer not null,UUID text,modifyCount integer default 1, paymentDate integer, billType integer,txnUUID text);");
    paramSQLiteDatabase.execSQL("create trigger if not exists StmtTriggerModifiedFlag After update on statements for each row  Begin  Update statements Set modifyCount = modifyCount + 1  Where _id =  New._id;  End; ");
  }
  
  private String b(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt)
  {
    StringBuilder localStringBuilder = new StringBuilder("sms.date between ");
    localStringBuilder.append(paramCalendar1.getTime().getTime());
    localStringBuilder.append(" AND ");
    localStringBuilder.append(paramCalendar2.getTime().getTime());
    paramCalendar2 = localStringBuilder.toString();
    paramCalendar1 = paramCalendar2;
    if (paramInt != 0)
    {
      paramCalendar1 = new StringBuilder();
      paramCalendar1.append(paramCalendar2);
      paramCalendar1.append(" AND statements.accountId IN (");
      paramCalendar1.append(c.c(new int[] { paramInt }));
      paramCalendar1.append(")");
      paramCalendar1 = paramCalendar1.toString();
    }
    return paramCalendar1;
  }
  
  public static void b(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.beginTransaction();
    try
    {
      paramSQLiteDatabase.execSQL("drop table if exists statements");
      paramSQLiteDatabase.execSQL("drop trigger if exists StmtTriggerModifiedFlag");
      a(paramSQLiteDatabase);
      paramSQLiteDatabase.setTransactionSuccessful();
      return;
    }
    finally
    {
      paramSQLiteDatabase.endTransaction();
    }
  }
  
  public final int a(l paraml, ContentValues paramContentValues)
  {
    if ((o >= 0L) && (w >= 0))
    {
      SQLiteDatabase localSQLiteDatabase = a;
      StringBuilder localStringBuilder = new StringBuilder("_id = ");
      localStringBuilder.append(o);
      return localSQLiteDatabase.update("statements", paramContentValues, localStringBuilder.toString(), null);
    }
    com.daamitt.prime.sdk.a.e.a();
    return -1;
  }
  
  public final l a(ArrayList<c.a> paramArrayList, l paraml)
  {
    com.daamitt.prime.sdk.a.e.a();
    StringBuilder localStringBuilder = new StringBuilder();
    Object localObject1 = new StringBuilder("statements.accountId = ");
    ((StringBuilder)localObject1).append(w);
    localStringBuilder.append(((StringBuilder)localObject1).toString());
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = paramArrayList.iterator();
    int j = 0;
    label196:
    Object localObject2;
    long l1;
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      int i = 1;
      if (!bool) {
        break;
      }
      c.a locala = (c.a)localIterator.next();
      paramArrayList = a;
      int k = paramArrayList.hashCode();
      if (k != -1413853096)
      {
        if (k != -102973965)
        {
          if (k != 110749)
          {
            if ((k == 3076014) && (paramArrayList.equals("date"))) {
              break label196;
            }
          }
          else if (paramArrayList.equals("pan"))
          {
            i = 2;
            break label196;
          }
        }
        else if (paramArrayList.equals("sms_time"))
        {
          i = 3;
          break label196;
        }
      }
      else if (paramArrayList.equals("amount"))
      {
        i = 0;
        break label196;
      }
      i = -1;
      switch (i)
      {
      default: 
        break;
      case 3: 
        paramArrayList = "date";
        break;
      case 2: 
        paramArrayList = "pan";
        break;
      case 1: 
        paramArrayList = "dueDate";
        break;
      case 0: 
        paramArrayList = "amount";
      }
      localObject1 = b;
      localObject2 = paraml.b((String)localObject1);
      if (localObject2 != null) {
        localObject1 = localObject2;
      }
      localObject2 = c;
      com.daamitt.prime.sdk.a.e.a();
      if ((!TextUtils.equals(paramArrayList, "dueDate")) && (!TextUtils.equals(paramArrayList, "date")))
      {
        if (TextUtils.equals(paramArrayList, "deleted"))
        {
          i = d;
          if (i == 3)
          {
            if (!TextUtils.isEmpty(localStringBuilder)) {
              localStringBuilder.append(" AND ");
            }
            localStringBuilder.append("statements.status & 2 = 0");
            j = i;
          }
          else
          {
            j = i;
            if (i == 2)
            {
              if (!TextUtils.isEmpty(localStringBuilder)) {
                localStringBuilder.append(" AND ");
              }
              localStringBuilder.append("statements.status & 2 != 0");
              j = i;
            }
          }
        }
        else if (TextUtils.equals(paramArrayList, "pattern_UID"))
        {
          i = d;
          if (i == 3)
          {
            if (!TextUtils.isEmpty(localStringBuilder)) {
              localStringBuilder.append(" AND ");
            }
            localStringBuilder.append("patternUID !=?");
            localArrayList.add(String.valueOf(F.j));
          }
          else if (i == 2)
          {
            if (!TextUtils.isEmpty(localStringBuilder)) {
              localStringBuilder.append(" AND ");
            }
            localStringBuilder.append("patternUID =?");
            localArrayList.add(String.valueOf(F.j));
          }
        }
        else if (TextUtils.equals((CharSequence)localObject2, "contains"))
        {
          if (!TextUtils.isEmpty(localStringBuilder)) {
            localStringBuilder.append(" AND ");
          }
          localStringBuilder.append(paramArrayList);
          localStringBuilder.append(" LIKE '%");
          localStringBuilder.append((String)localObject1);
          localStringBuilder.append("%'");
        }
        else if (TextUtils.equals((CharSequence)localObject2, "exact"))
        {
          if (!TextUtils.isEmpty(localStringBuilder)) {
            localStringBuilder.append(" AND ");
          }
          localStringBuilder.append(paramArrayList);
          localStringBuilder.append(" =? ");
          localArrayList.add(localObject1);
        }
      }
      else
      {
        if (!TextUtils.isEmpty(localStringBuilder)) {
          localStringBuilder.append(" AND ");
        }
        l1 = e;
        long l2 = Long.valueOf((String)localObject1).longValue();
        long l3 = Long.valueOf((String)localObject1).longValue();
        localStringBuilder.append(paramArrayList);
        localStringBuilder.append(" >=? AND ");
        localStringBuilder.append(paramArrayList);
        localStringBuilder.append(" <=?");
        localArrayList.add(String.valueOf(l2 - l1));
        localArrayList.add(String.valueOf(l3 + l1));
      }
    }
    paramArrayList = new SQLiteQueryBuilder();
    paramArrayList.setTables("statements JOIN accounts ON statements.accountId = accounts._id JOIN sms ON sms._id = statements.wSmsId");
    try
    {
      paraml = paramArrayList.query(a, e, localStringBuilder.toString(), (String[])localArrayList.toArray(new String[localArrayList.size()]), null, null, "statements._id DESC");
    }
    catch (RuntimeException paramArrayList)
    {
      for (;;) {}
    }
    com.daamitt.prime.sdk.a.e.b();
    paraml = null;
    if ((paraml != null) && (paraml.getCount() > 0))
    {
      paraml.moveToFirst();
      if (!paraml.isAfterLast())
      {
        localObject1 = a(paraml);
        b = paraml.getString(paraml.getColumnIndexOrThrow("pan"));
        paramArrayList = (ArrayList<c.a>)localObject1;
        if ((j & 0x2) != 0) {
          break label1034;
        }
        paramArrayList = (ArrayList<c.a>)localObject1;
        if (j != 1) {
          break label1034;
        }
        l1 = C;
        localObject2 = c.a(l1);
        paramArrayList = (ArrayList<c.a>)localObject1;
        while (I != null)
        {
          localObject2 = c.a(I);
          l1 = o;
          localObject1 = a.query("statements", d, "wSmsId = ".concat(String.valueOf(l1)), null, null, null, null);
          if ((localObject1 != null) && (((Cursor)localObject1).getCount() > 0))
          {
            ((Cursor)localObject1).moveToFirst();
            if (!((Cursor)localObject1).isAfterLast())
            {
              paramArrayList = a((Cursor)localObject1);
              break label1005;
            }
          }
          paramArrayList = null;
          label1005:
          if (localObject1 != null) {
            ((Cursor)localObject1).close();
          }
          if ((j & 0x2) != 0) {
            break;
          }
        }
      }
    }
    paramArrayList = null;
    label1034:
    if (paraml != null) {
      paraml.close();
    }
    return paramArrayList;
  }
  
  public final void a(a parama1, a parama2)
  {
    Object localObject1 = a;
    Object localObject2 = d;
    Object localObject3 = new StringBuilder("accountId =");
    ((StringBuilder)localObject3).append(a);
    parama1 = ((SQLiteDatabase)localObject1).query("statements", (String[])localObject2, ((StringBuilder)localObject3).toString(), null, null, null, null);
    if ((parama1 != null) && (parama1.getCount() > 0))
    {
      parama1.moveToFirst();
      while (!parama1.isAfterLast())
      {
        localObject1 = a(parama1);
        localObject2 = a;
        localObject3 = d;
        StringBuilder localStringBuilder = new StringBuilder("accountId =");
        localStringBuilder.append(a);
        localStringBuilder.append(" AND dueDate =");
        localStringBuilder.append(g.getTime());
        localObject2 = ((SQLiteDatabase)localObject2).query("statements", (String[])localObject3, localStringBuilder.toString(), null, null, null, null);
        if ((localObject2 != null) && (((Cursor)localObject2).getCount() > 0))
        {
          ((Cursor)localObject2).moveToFirst();
          while (!((Cursor)localObject2).isAfterLast())
          {
            if (Math.abs(ad - d) < 1.0D)
            {
              a(o);
              break;
            }
            ((Cursor)localObject2).moveToNext();
          }
          ((Cursor)localObject2).close();
        }
        parama1.moveToNext();
      }
      parama1.close();
    }
    if (parama1 != null) {
      parama1.close();
    }
  }
  
  public final boolean a(l paraml, a parama)
  {
    Object localObject = new StringBuilder("accountId IN (");
    ((StringBuilder)localObject).append(c.c(w));
    ((StringBuilder)localObject).append(") AND dueDate='");
    ((StringBuilder)localObject).append(g.getTime());
    ((StringBuilder)localObject).append("' AND statements.status & 2 = 0 ");
    localObject = ((StringBuilder)localObject).toString();
    localObject = a.query("statements", d, (String)localObject, null, null, null, null);
    if ((localObject != null) && (((Cursor)localObject).getCount() > 0))
    {
      ((Cursor)localObject).moveToFirst();
      while (!((Cursor)localObject).isAfterLast())
      {
        if (Math.abs(ad - d) < 1.0D)
        {
          com.daamitt.prime.sdk.a.e.b();
          ((Cursor)localObject).close();
          i = 1;
          break label183;
        }
        ((Cursor)localObject).moveToNext();
      }
    }
    if (localObject != null) {
      ((Cursor)localObject).close();
    }
    int i = 0;
    label183:
    if (i != 0) {
      return true;
    }
    if (i == 6)
    {
      localObject = new StringBuilder("Select statements.* from statements, accounts Where statements.accountId = accounts._id AND dueDate=");
      ((StringBuilder)localObject).append(g.getTime());
      ((StringBuilder)localObject).append(" AND statements.status & 2 = 0  AND (accounts.type = 3 OR accounts.type = 6) AND accounts.name=? ");
      localObject = ((StringBuilder)localObject).toString();
      com.daamitt.prime.sdk.a.e.a();
      parama = a.rawQuery((String)localObject, new String[] { d });
      if ((parama != null) && (parama.getCount() > 0))
      {
        parama.moveToFirst();
        while (!parama.isAfterLast())
        {
          if (Math.abs(ad - d) < 1.0D)
          {
            com.daamitt.prime.sdk.a.e.b();
            parama.close();
            return true;
          }
          parama.moveToNext();
        }
        parama.close();
      }
      if (parama != null)
      {
        parama.close();
        return false;
      }
    }
    else
    {
      localObject = new StringBuilder("Select statements.*,accounts.type as ");
      ((StringBuilder)localObject).append("accType");
      ((StringBuilder)localObject).append(" from statements, accounts Where statements.accountId = accounts._id AND dueDate=");
      ((StringBuilder)localObject).append(g.getTime());
      ((StringBuilder)localObject).append(" AND statements.status & 2 = 0  AND (accounts.type = 3 OR accounts.type = 6) AND accounts.name=? ");
      localObject = ((StringBuilder)localObject).toString();
      com.daamitt.prime.sdk.a.e.a();
      parama = a.rawQuery((String)localObject, new String[] { d });
      if ((parama != null) && (parama.getCount() > 0))
      {
        parama.moveToFirst();
        while (!parama.isAfterLast())
        {
          localObject = a(parama);
          if (Math.abs(d - d) < 1.0D)
          {
            if (parama.getInt(parama.getColumnIndexOrThrow("accType")) == 6)
            {
              com.daamitt.prime.sdk.a.e.b();
              a(o);
              parama.close();
              return false;
            }
            com.daamitt.prime.sdk.a.e.b();
            parama.close();
            return true;
          }
          parama.moveToNext();
        }
        parama.close();
      }
      if (parama != null) {
        parama.close();
      }
    }
    return false;
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt)
  {
    int j = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    com.daamitt.prime.sdk.a.e.a();
    if (paramCalendar1.before(paramCalendar2))
    {
      String[] arrayOfString = new String[j];
      paramCalendar2 = b(paramCalendar1, paramCalendar2, paramInt);
      Object localObject = Calendar.getInstance().getTimeZone();
      long l2 = ((TimeZone)localObject).getRawOffset() + ((TimeZone)localObject).getDSTSavings();
      long l1 = l2 / 3600000L;
      l2 = l2 % 3600000L / 60000L;
      localObject = new StringBuilder("strftime('%j',date(date/1000,'unixepoch','");
      ((StringBuilder)localObject).append(l1);
      ((StringBuilder)localObject).append(" hours','");
      ((StringBuilder)localObject).append(l2);
      ((StringBuilder)localObject).append(" minutes')) as Day");
      localObject = ((StringBuilder)localObject).toString();
      int i = 0;
      String str = g;
      paramCalendar2 = a.query(h, new String[] { localObject, str }, paramCalendar2, null, "Day", null, null, null);
      if ((paramCalendar2 != null) && (paramCalendar2.getCount() > 0))
      {
        paramCalendar2.moveToFirst();
        int k = paramCalendar1.get(6);
        int m = paramCalendar1.getActualMaximum(6);
        while (!paramCalendar2.isAfterLast())
        {
          localObject = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("Day"));
          paramCalendar1 = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("Amount"));
          try
          {
            paramInt = Integer.parseInt((String)localObject);
          }
          catch (NumberFormatException localNumberFormatException)
          {
            localNumberFormatException.printStackTrace();
            paramInt = -1;
          }
          if ((paramInt != -1) && (paramCalendar1 != null))
          {
            int n = (int)Double.valueOf(paramCalendar1).doubleValue();
            if (paramInt >= k) {
              paramInt = j - (paramInt - k) - 1;
            } else {
              paramInt = j - (m - k + paramInt) - 1;
            }
            if (paramInt >= 0) {
              arrayOfString[paramInt] = String.valueOf(n);
            }
          }
          paramCalendar2.moveToNext();
        }
      }
      paramInt = i;
      if (paramCalendar2 != null)
      {
        paramCalendar2.close();
        paramInt = i;
      }
      while (paramInt < j)
      {
        if (TextUtils.isEmpty(arrayOfString[paramInt])) {
          arrayOfString[paramInt] = "";
        }
        paramInt += 1;
      }
      return arrayOfString;
    }
    return null;
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt, int[] paramArrayOfInt1, int[] paramArrayOfInt2)
  {
    int j = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    com.daamitt.prime.sdk.a.e.a();
    if (paramCalendar1.before(paramCalendar2))
    {
      String[] arrayOfString = new String[j];
      paramCalendar2 = b(paramCalendar1, paramCalendar2, paramInt);
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramCalendar2);
      localStringBuilder.append(" AND statements.type IN (");
      localStringBuilder.append(a(paramArrayOfInt1));
      localStringBuilder.append(")");
      paramCalendar2 = localStringBuilder.toString();
      paramArrayOfInt1 = new StringBuilder();
      paramArrayOfInt1.append(paramCalendar2);
      paramArrayOfInt1.append(" AND sms.patternUID IN (");
      paramArrayOfInt1.append(a(paramArrayOfInt2));
      paramArrayOfInt1.append(")");
      paramCalendar2 = paramArrayOfInt1.toString();
      paramArrayOfInt1 = Calendar.getInstance().getTimeZone();
      long l2 = paramArrayOfInt1.getRawOffset() + paramArrayOfInt1.getDSTSavings();
      long l1 = l2 / 3600000L;
      l2 = l2 % 3600000L / 60000L;
      paramArrayOfInt1 = new StringBuilder("strftime('%j',date(date/1000,'unixepoch','");
      paramArrayOfInt1.append(l1);
      paramArrayOfInt1.append(" hours','");
      paramArrayOfInt1.append(l2);
      paramArrayOfInt1.append(" minutes')) as Day");
      paramArrayOfInt1 = paramArrayOfInt1.toString();
      int i = 0;
      paramCalendar2 = a.query(h, new String[] { paramArrayOfInt1, "amount" }, paramCalendar2, null, "Day", " MAX(amount)", null, null);
      if ((paramCalendar2 != null) && (paramCalendar2.getCount() > 0))
      {
        paramCalendar2.moveToFirst();
        int k = paramCalendar1.get(6);
        int m = paramCalendar1.getActualMaximum(6);
        while (!paramCalendar2.isAfterLast())
        {
          paramArrayOfInt1 = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("Day"));
          paramCalendar1 = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("amount"));
          try
          {
            paramInt = Integer.parseInt(paramArrayOfInt1);
          }
          catch (NumberFormatException paramArrayOfInt1)
          {
            int n;
            for (;;) {}
          }
          paramInt = -1;
          if ((paramInt != -1) && (paramCalendar1 != null))
          {
            n = (int)Double.valueOf(paramCalendar1).doubleValue();
            if (paramInt >= k) {
              paramInt = j - (paramInt - k) - 1;
            } else {
              paramInt = j - (m - k + paramInt) - 1;
            }
            if (paramInt >= 0) {
              arrayOfString[paramInt] = String.valueOf(n);
            }
          }
          paramCalendar2.moveToNext();
        }
      }
      paramInt = i;
      if (paramCalendar2 != null)
      {
        paramCalendar2.close();
        paramInt = i;
      }
      while (paramInt < j)
      {
        if (TextUtils.isEmpty(arrayOfString[paramInt])) {
          arrayOfString[paramInt] = "";
        }
        paramInt += 1;
      }
      return arrayOfString;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.b.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */