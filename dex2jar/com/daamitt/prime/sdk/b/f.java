package com.daamitt.prime.sdk.b;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.location.Location;
import android.text.TextUtils;
import com.daamitt.prime.sdk.a.a;
import com.daamitt.prime.sdk.a.c.a;
import com.daamitt.prime.sdk.a.e;
import com.daamitt.prime.sdk.a.h;
import com.daamitt.prime.sdk.a.i.a;
import com.daamitt.prime.sdk.a.k;
import com.daamitt.prime.sdk.a.m;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.TimeZone;
import java.util.UUID;

public class f
{
  public static String[] b = { "transactions._id", "wSmsId", "pos", "amount", "txnDate", "transactions.type", "transactions.flags", "transactions.accountId", "merchantId", "categories", "placeId", "placeName", "placeDisplayName", "placeLat", "placeLon", "txnTags", "txnNote", "txnPhoto", "txnPhotoServerPath", "txnBalance", "txnOutstandingBalance", "conversionRate", "currencyAmount", "currencySymbol", "recursionAccountUUID", "sender", "date", "body", "ifnull(lat, 360) as lat", "ifnull(long, 360) as long", "ifnull(locAccuracy, -1) as locAccuracy", "tags", "name", "displayName", "displayPan", "enabled", "accounts.flags as accountFlags", "transactions.UUID" };
  private static final String d = "f";
  private static String f = "SUM(amount) as Amount ";
  private static String g = "transactions JOIN accounts ON transactions.accountId = accounts._id";
  private static String[] h = { "_id", "wSmsId", "pos", "amount", "txnDate", "type", "flags", "accountId", "merchantId", "categories", "placeId", "placeName", "placeDisplayName", "placeLat", "placeLon", "txnTags", "txnNote", "txnPhoto", "UUID", "modifyCount", "txnBalance", "txnOutstandingBalance", "txnPhotoServerPath", "conversionRate", "currencyAmount", "currencySymbol", "txnRecursionAccountID", "recursionAccountUUID" };
  private static f i;
  public b a;
  String[] c = { "transactions._id", "transactions.wSmsId", "transactions.pos", "transactions.amount", "transactions.txnDate", "transactions.type", "transactions.flags", "transactions.accountId", "transactions.merchantId", "transactions.categories", "transactions.placeId", "transactions.placeName", "transactions.placeDisplayName", "transactions.placeLat", "transactions.placeLon", "transactions.txnTags", "transactions.txnNote", "transactions.txnPhoto", "transactions.txnPhotoServerPath", "transactions.UUID", "transactions.modifyCount", "transactions.txnBalance", "transactions.txnOutstandingBalance", "transactions.conversionRate", "transactions.currencyAmount", "transactions.currencySymbol", "transactions.txnRecursionAccountID", "transactions.recursionAccountUUID", "pan" };
  private SQLiteDatabase e = null;
  
  private f(b paramb)
  {
    a = paramb;
  }
  
  private static m a(Cursor paramCursor)
  {
    int j = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("_id"));
    int k = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("wSmsId"));
    Object localObject2 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("pos"));
    double d1 = paramCursor.getDouble(paramCursor.getColumnIndexOrThrow("amount"));
    Object localObject1 = Calendar.getInstance();
    ((Calendar)localObject1).setTimeInMillis(paramCursor.getLong(paramCursor.getColumnIndexOrThrow("txnDate")));
    Date localDate = ((Calendar)localObject1).getTime();
    int m = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("type"));
    int n = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("flags"));
    int i1 = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("accountId"));
    double d2 = paramCursor.getDouble(paramCursor.getColumnIndexOrThrow("txnBalance"));
    double d3 = paramCursor.getDouble(paramCursor.getColumnIndexOrThrow("txnOutstandingBalance"));
    double d4 = paramCursor.getDouble(paramCursor.getColumnIndexOrThrow("conversionRate"));
    double d5 = paramCursor.getDouble(paramCursor.getColumnIndexOrThrow("currencyAmount"));
    String str = paramCursor.getString(paramCursor.getColumnIndexOrThrow("currencySymbol"));
    localObject1 = new m(null, null, null);
    o = j;
    C = k;
    w = i1;
    u = 3;
    ((m)localObject1).a(null, "Spends");
    U = paramCursor.getString(paramCursor.getColumnIndexOrThrow("txnTags"));
    V = paramCursor.getString(paramCursor.getColumnIndexOrThrow("txnNote"));
    Z = paramCursor.getString(paramCursor.getColumnIndexOrThrow("txnPhoto"));
    aa = paramCursor.getString(paramCursor.getColumnIndexOrThrow("txnPhotoServerPath"));
    ((m)localObject1).a(null, Double.valueOf(d1), localDate, (String)localObject2, m);
    T = paramCursor.getString(paramCursor.getColumnIndexOrThrow("categories"));
    Y = n;
    ab = paramCursor.getString(paramCursor.getColumnIndexOrThrow("UUID"));
    localObject2 = new com.daamitt.prime.sdk.a.b();
    a = d2;
    b = d3;
    i = ((com.daamitt.prime.sdk.a.b)localObject2);
    k = paramCursor.getLong(paramCursor.getColumnIndexOrThrow("merchantId"));
    l = paramCursor.getString(paramCursor.getColumnIndexOrThrow("placeId"));
    m = paramCursor.getString(paramCursor.getColumnIndexOrThrow("placeName"));
    n = paramCursor.getString(paramCursor.getColumnIndexOrThrow("placeDisplayName"));
    g = d4;
    h = d5;
    e = str;
    W = paramCursor.getString(paramCursor.getColumnIndexOrThrow("recursionAccountUUID"));
    d1 = paramCursor.getDouble(paramCursor.getColumnIndex("placeLat"));
    d2 = paramCursor.getDouble(paramCursor.getColumnIndex("placeLon"));
    if ((d1 != 360.0D) && (d2 != 360.0D))
    {
      paramCursor = new Location("walnutloc");
      paramCursor.setLatitude(d1);
      paramCursor.setLongitude(d2);
      X = paramCursor;
    }
    return (m)localObject1;
  }
  
  public static f a(b paramb)
  {
    if (i == null)
    {
      f localf = new f(paramb);
      i = localf;
      e = paramb.getWritableDatabase();
    }
    return i;
  }
  
  private static String a(int paramInt)
  {
    int j = 1;
    StringBuilder localStringBuilder = new StringBuilder(paramInt * 2 - 1);
    localStringBuilder.append("?");
    while (j < paramInt)
    {
      localStringBuilder.append(",?");
      j += 1;
    }
    return localStringBuilder.toString();
  }
  
  private String a(Calendar paramCalendar1, Calendar paramCalendar2, String paramString1, int paramInt, String paramString2)
  {
    StringBuilder localStringBuilder = new StringBuilder("transactions.type in ( ");
    localStringBuilder.append(paramString2);
    localStringBuilder.append("  )  AND txnDate between ");
    localStringBuilder.append(paramCalendar1.getTime().getTime());
    localStringBuilder.append(" AND ");
    localStringBuilder.append(paramCalendar2.getTime().getTime());
    localStringBuilder.append(" AND transactions.flags & 128 = 0 ");
    paramCalendar2 = localStringBuilder.toString();
    paramCalendar1 = paramCalendar2;
    if (!TextUtils.isEmpty(paramString1))
    {
      paramCalendar1 = new StringBuilder();
      paramCalendar1.append(paramCalendar2);
      paramCalendar1.append(" AND accounts.type in ( ");
      paramCalendar1.append(paramString1);
      paramCalendar1.append("  ) ");
      paramCalendar1 = paramCalendar1.toString();
    }
    paramCalendar2 = paramCalendar1;
    if (paramInt != 0)
    {
      paramCalendar2 = new StringBuilder();
      paramCalendar2.append(paramCalendar1);
      paramCalendar2.append(" AND accounts._id IN (");
      paramCalendar2.append(a.c(new int[] { paramInt }));
      paramCalendar2.append(")");
      paramCalendar2 = paramCalendar2.toString();
    }
    return paramCalendar2;
  }
  
  public static void a(SQLiteDatabase paramSQLiteDatabase)
  {
    e.b();
    paramSQLiteDatabase.execSQL("create table if not exists transactions(_id integer primary key autoincrement,wSmsId integer not null, pos text not null,amount real not null,txnDate integer not null,type integer not null,flags integer default 0,accountId integer not null,merchantId integer default -1,categories text not null default other,placeId text default null,placeName text default null,placeDisplayName text default null,placeLat double default 360,placeLon double default 360,txnTags text,txnNote text,txnPhoto text,UUID text,modifyCount integer default 1,txnBalance real,txnOutstandingBalance real,txnPhotoServerPath text,conversionRate real default 1,currencyAmount real default 0,currencySymbol text, txnRecursionAccountID int default 0,recursionAccountUUID text );");
    e.b();
    paramSQLiteDatabase.execSQL("create trigger if not exists TxnTriggerModifiedFlag After update on transactions for each row  Begin  Update transactions Set modifyCount = modifyCount + 1  Where _id =  New._id;  End; ");
  }
  
  private static String b(int[] paramArrayOfInt)
  {
    if ((paramArrayOfInt != null) && (paramArrayOfInt.length > 0))
    {
      int k = paramArrayOfInt.length;
      int j = 1;
      StringBuilder localStringBuilder1 = new StringBuilder(k * 2 - 1);
      localStringBuilder1.append(paramArrayOfInt[0]);
      while (j < paramArrayOfInt.length)
      {
        StringBuilder localStringBuilder2 = new StringBuilder(",");
        localStringBuilder2.append(paramArrayOfInt[j]);
        localStringBuilder1.append(localStringBuilder2.toString());
        j += 1;
      }
      return localStringBuilder1.toString();
    }
    return null;
  }
  
  public static void b(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.beginTransaction();
    try
    {
      paramSQLiteDatabase.execSQL("drop table if exists transactions");
      paramSQLiteDatabase.execSQL("drop trigger if exists TxnTriggerModifiedFlag");
      a(paramSQLiteDatabase);
      paramSQLiteDatabase.setTransactionSuccessful();
      return;
    }
    finally
    {
      paramSQLiteDatabase.endTransaction();
    }
  }
  
  private String f(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt)
  {
    StringBuilder localStringBuilder = new StringBuilder("transactions.type in ( ");
    localStringBuilder.append(m.e());
    localStringBuilder.append("  )  and txnDate between ");
    localStringBuilder.append(paramCalendar1.getTime().getTime());
    localStringBuilder.append(" AND ");
    localStringBuilder.append(paramCalendar2.getTime().getTime());
    localStringBuilder.append(" AND txnBalance NOTNULL  AND accountId IN (");
    localStringBuilder.append(a.c(new int[] { paramInt }));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final int a(m paramm, ContentValues paramContentValues)
  {
    if (o >= 0L)
    {
      SQLiteDatabase localSQLiteDatabase = e;
      StringBuilder localStringBuilder = new StringBuilder("_id = ");
      localStringBuilder.append(o);
      return localSQLiteDatabase.update("transactions", paramContentValues, localStringBuilder.toString(), null);
    }
    e.a();
    return -1;
  }
  
  public final long a(m paramm)
  {
    SQLiteQueryBuilder localSQLiteQueryBuilder;
    if (paramm.l())
    {
      localObject1 = new StringBuilder("sms._id < ");
      ((StringBuilder)localObject1).append(C);
      ((StringBuilder)localObject1).append(" AND body =? AND transactions.flags & 16 = 0 ");
      localObject1 = ((StringBuilder)localObject1).toString();
      localObject2 = q;
      localSQLiteQueryBuilder = new SQLiteQueryBuilder();
      localSQLiteQueryBuilder.setTables("sms JOIN transactions ON transactions.wSmsId = sms._id");
      localObject2 = localSQLiteQueryBuilder.query(e, new String[] { "sms._id" }, (String)localObject1, new String[] { localObject2 }, null, null, null);
      if (localObject2 != null)
      {
        ((Cursor)localObject2).getCount();
        e.a();
      }
      localObject1 = localObject2;
      if (localObject2 != null)
      {
        localObject1 = localObject2;
        if (((Cursor)localObject2).getCount() > 0)
        {
          e.a();
          paramm.o();
          paramm.m();
          localObject1 = localObject2;
        }
      }
    }
    else if ((paramm.k()) && (r != null))
    {
      localObject1 = new StringBuilder("date > ");
      ((StringBuilder)localObject1).append(r.getTime() - 3600000L);
      ((StringBuilder)localObject1).append(" AND sms._id < ");
      ((StringBuilder)localObject1).append(C);
      ((StringBuilder)localObject1).append(" AND body =? AND transactions.flags & 16 = 0 ");
      localObject1 = ((StringBuilder)localObject1).toString();
      localObject2 = q;
      localSQLiteQueryBuilder = new SQLiteQueryBuilder();
      localSQLiteQueryBuilder.setTables("sms JOIN transactions ON transactions.wSmsId = sms._id");
      localObject2 = localSQLiteQueryBuilder.query(e, new String[] { "sms._id" }, (String)localObject1, new String[] { localObject2 }, null, null, "sms._id DESC", "1");
      localObject1 = localObject2;
      if (localObject2 != null)
      {
        localObject1 = localObject2;
        if (((Cursor)localObject2).getCount() > 0)
        {
          e.a();
          paramm.o();
          paramm.m();
          localObject1 = localObject2;
        }
      }
    }
    else
    {
      localObject1 = null;
    }
    if (localObject1 != null) {
      ((Cursor)localObject1).close();
    }
    Object localObject1 = new ContentValues();
    ((ContentValues)localObject1).put("wSmsId", Long.valueOf(C));
    ((ContentValues)localObject1).put("pos", c.toLowerCase());
    if (n != null) {
      ((ContentValues)localObject1).put("placeDisplayName", n);
    }
    ((ContentValues)localObject1).put("placeName", m);
    ((ContentValues)localObject1).put("placeDisplayName", n);
    ((ContentValues)localObject1).put("amount", Double.valueOf(f));
    ((ContentValues)localObject1).put("txnDate", Long.valueOf(ad.getTime()));
    ((ContentValues)localObject1).put("type", Integer.valueOf(j));
    ((ContentValues)localObject1).put("flags", Integer.valueOf(Y));
    ((ContentValues)localObject1).put("accountId", Integer.valueOf(w));
    ((ContentValues)localObject1).put("txnNote", V);
    if (l != null) {
      ((ContentValues)localObject1).put("placeId", l);
    }
    if (k != -1L) {
      ((ContentValues)localObject1).put("merchantId", Long.valueOf(k));
    }
    if (T != null) {
      ((ContentValues)localObject1).put("categories", T);
    }
    if (D != null)
    {
      ((ContentValues)localObject1).put("placeLat", Double.valueOf(D.getLatitude()));
      ((ContentValues)localObject1).put("placeLon", Double.valueOf(D.getLongitude()));
    }
    if (U != null) {
      ((ContentValues)localObject1).put("txnTags", U);
    }
    if (V != null) {
      ((ContentValues)localObject1).put("txnNote", V);
    }
    if (Z != null) {
      ((ContentValues)localObject1).put("txnPhoto", Z);
    }
    if (aa != null) {
      ((ContentValues)localObject1).put("txnPhotoServerPath", aa);
    }
    ((ContentValues)localObject1).put("currencyAmount", Double.valueOf(h));
    if (!TextUtils.isEmpty(e)) {
      ((ContentValues)localObject1).put("currencySymbol", e);
    }
    Object localObject2 = UUID.randomUUID().toString();
    ab = ((String)localObject2);
    ((ContentValues)localObject1).put("UUID", (String)localObject2);
    if (i != null)
    {
      ((ContentValues)localObject1).put("txnBalance", Double.valueOf(i.a));
      ((ContentValues)localObject1).put("txnOutstandingBalance", Double.valueOf(i.b));
    }
    return e.insert("transactions", null, (ContentValues)localObject1);
  }
  
  public final m a(ArrayList<a> paramArrayList, ArrayList<c.a> paramArrayList1, m paramm)
  {
    e.a();
    StringBuilder localStringBuilder = new StringBuilder();
    int j;
    if (!paramArrayList.isEmpty())
    {
      localStringBuilder.append("transactions.accountId IN(");
      j = 0;
      while (j < paramArrayList.size())
      {
        localStringBuilder.append(geta);
        if (j == paramArrayList.size() - 1) {
          localStringBuilder.append(")");
        } else {
          localStringBuilder.append(",");
        }
        j += 1;
      }
    }
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = paramArrayList1.iterator();
    int k = 0;
    Object localObject;
    long l1;
    while (localIterator.hasNext())
    {
      c.a locala = (c.a)localIterator.next();
      paramArrayList = a;
      j = -1;
      switch (paramArrayList.hashCode())
      {
      default: 
        break;
      case 3387378: 
        if (paramArrayList.equals("note")) {
          j = 1;
        }
        break;
      case 3076014: 
        if (paramArrayList.equals("date")) {
          j = 3;
        }
        break;
      case 111188: 
        if (paramArrayList.equals("pos")) {
          j = 2;
        }
        break;
      case 110749: 
        if (paramArrayList.equals("pan")) {
          j = 4;
        }
        break;
      case -1413853096: 
        if (paramArrayList.equals("amount")) {
          j = 0;
        }
        break;
      }
      switch (j)
      {
      default: 
        break;
      case 4: 
        paramArrayList = "pan";
        break;
      case 3: 
        paramArrayList = "txnDate";
        break;
      case 2: 
        paramArrayList = "pos";
        break;
      case 1: 
        paramArrayList = "txnNote";
        break;
      case 0: 
        paramArrayList = "amount";
      }
      paramArrayList1 = b;
      localObject = paramm.b(paramArrayList1);
      if (localObject != null) {
        paramArrayList1 = (ArrayList<c.a>)localObject;
      }
      localObject = c;
      e.a();
      if (TextUtils.equals(paramArrayList, "txnDate"))
      {
        if (!TextUtils.isEmpty(localStringBuilder)) {
          localStringBuilder.append(" AND ");
        }
        l1 = e;
        long l2 = Long.valueOf(paramArrayList1).longValue();
        long l3 = Long.valueOf(paramArrayList1).longValue();
        localStringBuilder.append(paramArrayList);
        localStringBuilder.append(" >=? AND ");
        localStringBuilder.append(paramArrayList);
        localStringBuilder.append(" <=?");
        localArrayList.add(String.valueOf(l2 - l1));
        localArrayList.add(String.valueOf(l3 + l1));
      }
      else if (TextUtils.equals(paramArrayList, "deleted"))
      {
        k = d;
        if (k == 3)
        {
          if (!TextUtils.isEmpty(localStringBuilder)) {
            localStringBuilder.append(" AND ");
          }
          localStringBuilder.append("transactions.flags & 16 = 0");
        }
        else if (k == 2)
        {
          if (!TextUtils.isEmpty(localStringBuilder)) {
            localStringBuilder.append(" AND ");
          }
          localStringBuilder.append("transactions.flags & 16 != 0");
        }
        if (!TextUtils.isEmpty(localStringBuilder)) {
          localStringBuilder.append(" AND ");
        }
        localStringBuilder.append("transactions.flags & 256 != 0");
      }
      else if (TextUtils.equals(paramArrayList, "pattern_UID"))
      {
        j = d;
        if (j == 3)
        {
          if (!TextUtils.isEmpty(localStringBuilder)) {
            localStringBuilder.append(" AND ");
          }
          localStringBuilder.append("patternUID !=?");
          localArrayList.add(String.valueOf(F.j));
        }
        else if (j == 2)
        {
          if (!TextUtils.isEmpty(localStringBuilder)) {
            localStringBuilder.append(" AND ");
          }
          localStringBuilder.append("patternUID =?");
          localArrayList.add(String.valueOf(F.j));
        }
      }
      else if (TextUtils.equals((CharSequence)localObject, "contains"))
      {
        if (!TextUtils.isEmpty(localStringBuilder)) {
          localStringBuilder.append(" AND ");
        }
        localStringBuilder.append(paramArrayList);
        localStringBuilder.append(" LIKE '%");
        localStringBuilder.append(paramArrayList1);
        localStringBuilder.append("%'");
      }
      else if (TextUtils.equals((CharSequence)localObject, "exact"))
      {
        if (!TextUtils.isEmpty(localStringBuilder)) {
          localStringBuilder.append(" AND ");
        }
        localStringBuilder.append(paramArrayList);
        localStringBuilder.append(" =? ");
        localArrayList.add(paramArrayList1);
      }
    }
    paramArrayList = new SQLiteQueryBuilder();
    paramArrayList.setTables("transactions JOIN accounts ON transactions.accountId = accounts._id JOIN sms ON sms._id = transactions.wSmsId");
    try
    {
      paramArrayList1 = paramArrayList.query(e, c, localStringBuilder.toString(), (String[])localArrayList.toArray(new String[localArrayList.size()]), null, null, "transactions._id DESC");
    }
    catch (RuntimeException paramArrayList)
    {
      for (;;) {}
    }
    e.b();
    paramArrayList1 = null;
    if ((paramArrayList1 != null) && (paramArrayList1.getCount() > 0))
    {
      paramArrayList1.moveToFirst();
      if (!paramArrayList1.isAfterLast())
      {
        paramm = a(paramArrayList1);
        b = paramArrayList1.getString(paramArrayList1.getColumnIndexOrThrow("pan"));
        paramArrayList = paramm;
        if ((Y & 0x10) != 0) {
          break label1118;
        }
        paramArrayList = paramm;
        if (k != 1) {
          break label1118;
        }
        l1 = C;
        localObject = a.a(l1);
        paramArrayList = paramm;
        while (I != null)
        {
          localObject = a.a(I);
          l1 = o;
          paramm = e.query("transactions", h, "wSmsId = ".concat(String.valueOf(l1)), null, null, null, null);
          if ((paramm != null) && (paramm.getCount() > 0))
          {
            paramm.moveToFirst();
            if (!paramm.isAfterLast())
            {
              paramArrayList = a(paramm);
              break label1090;
            }
          }
          paramArrayList = null;
          label1090:
          if (paramm != null) {
            paramm.close();
          }
          if ((Y & 0x10) != 0) {
            break;
          }
        }
      }
    }
    paramArrayList = null;
    label1118:
    if (paramArrayList1 != null) {
      paramArrayList1.close();
    }
    return paramArrayList;
  }
  
  public final ArrayList<Integer> a(int[] paramArrayOfInt)
  {
    ArrayList localArrayList = new ArrayList();
    StringBuilder localStringBuilder = new StringBuilder(" WHERE type IN ( ");
    localStringBuilder.append(b(paramArrayOfInt));
    localStringBuilder.append(" )");
    paramArrayOfInt = "SELECT DISTINCT accountId FROM transactions".concat(String.valueOf(localStringBuilder.toString()));
    paramArrayOfInt = e.rawQuery(paramArrayOfInt, null);
    if ((paramArrayOfInt != null) && (paramArrayOfInt.moveToFirst())) {
      while (!paramArrayOfInt.isAfterLast())
      {
        localArrayList.add(Integer.valueOf(paramArrayOfInt.getInt(paramArrayOfInt.getColumnIndexOrThrow("accountId"))));
        paramArrayOfInt.moveToNext();
      }
    }
    if (paramArrayOfInt != null) {
      paramArrayOfInt.close();
    }
    return localArrayList;
  }
  
  public final ArrayList<k> a(int[] paramArrayOfInt1, int[] paramArrayOfInt2, Date paramDate1, Date paramDate2)
  {
    Object localObject3 = new ArrayList();
    Object localObject2 = "";
    Object localObject4 = new ArrayList();
    if (((paramArrayOfInt1 != null) && (paramArrayOfInt1.length > 1)) || (paramArrayOfInt1 == null))
    {
      localObject1 = new StringBuilder("accounts.enabled=");
      ((StringBuilder)localObject1).append(a(1));
      localObject2 = ((StringBuilder)localObject1).toString();
      ((ArrayList)localObject4).add("1");
      k = 1;
    }
    else
    {
      k = 0;
    }
    Object localObject1 = localObject2;
    int j = k;
    Object localObject5;
    if (paramArrayOfInt2 != null)
    {
      localObject5 = new StringBuilder();
      if (k != 0) {
        localObject1 = " AND ";
      } else {
        localObject1 = "";
      }
      ((StringBuilder)localObject5).append((String)localObject1);
      ((StringBuilder)localObject5).append("transactions.type IN (");
      ((StringBuilder)localObject5).append(a(paramArrayOfInt2.length));
      ((StringBuilder)localObject5).append(")");
      localObject1 = ((String)localObject2).concat(((StringBuilder)localObject5).toString());
      k = paramArrayOfInt2.length;
      j = 0;
      while (j < k)
      {
        ((ArrayList)localObject4).add(String.valueOf(paramArrayOfInt2[j]));
        j += 1;
      }
      j = 1;
    }
    paramArrayOfInt2 = (int[])localObject1;
    int k = j;
    if (paramArrayOfInt1 != null)
    {
      localObject2 = new StringBuilder();
      if (j != 0) {
        paramArrayOfInt2 = " AND ";
      } else {
        paramArrayOfInt2 = "";
      }
      ((StringBuilder)localObject2).append(paramArrayOfInt2);
      ((StringBuilder)localObject2).append("transactions.accountId IN (");
      ((StringBuilder)localObject2).append(b(paramArrayOfInt1));
      ((StringBuilder)localObject2).append(")");
      paramArrayOfInt2 = ((String)localObject1).concat(((StringBuilder)localObject2).toString());
      k = 1;
    }
    paramArrayOfInt1 = paramArrayOfInt2;
    j = k;
    if (paramDate1 != null)
    {
      localObject1 = new StringBuilder();
      if (k != 0) {
        paramArrayOfInt1 = " AND ";
      } else {
        paramArrayOfInt1 = "";
      }
      ((StringBuilder)localObject1).append(paramArrayOfInt1);
      ((StringBuilder)localObject1).append("txnDate >=?");
      paramArrayOfInt1 = paramArrayOfInt2.concat(((StringBuilder)localObject1).toString());
      ((ArrayList)localObject4).add(String.valueOf(paramDate1.getTime()));
      j = 1;
    }
    if (paramDate2 != null)
    {
      paramDate1 = new StringBuilder();
      if (j != 0) {
        paramArrayOfInt2 = " AND ";
      } else {
        paramArrayOfInt2 = "";
      }
      paramDate1.append(paramArrayOfInt2);
      paramDate1.append("txnDate <=?");
      paramArrayOfInt1 = paramArrayOfInt1.concat(paramDate1.toString());
      ((ArrayList)localObject4).add(String.valueOf(paramDate2.getTime()));
    }
    paramArrayOfInt2 = new SQLiteQueryBuilder();
    paramArrayOfInt2.setTables("transactions JOIN accounts ON transactions.accountId = accounts._id LEFT OUTER JOIN sms ON transactions.wSmsId = sms._id");
    paramArrayOfInt2 = paramArrayOfInt2.query(e, b, paramArrayOfInt1, (String[])((ArrayList)localObject4).toArray(new String[0]), null, null, "txnDate ASC");
    paramArrayOfInt2.moveToFirst();
    paramArrayOfInt1 = (int[])localObject3;
    while (!paramArrayOfInt2.isAfterLast())
    {
      j = paramArrayOfInt2.getInt(paramArrayOfInt2.getColumnIndexOrThrow("_id"));
      paramDate1 = paramArrayOfInt2.getString(paramArrayOfInt2.getColumnIndexOrThrow("sender"));
      paramDate2 = Calendar.getInstance();
      paramDate2.setTimeInMillis(paramArrayOfInt2.getLong(paramArrayOfInt2.getColumnIndexOrThrow("date")));
      localObject3 = paramDate2.getTime();
      localObject4 = paramArrayOfInt2.getString(paramArrayOfInt2.getColumnIndexOrThrow("body"));
      paramDate2 = paramArrayOfInt2.getString(paramArrayOfInt2.getColumnIndexOrThrow("pos"));
      double d1 = paramArrayOfInt2.getDouble(paramArrayOfInt2.getColumnIndexOrThrow("amount"));
      localObject1 = Calendar.getInstance();
      ((Calendar)localObject1).setTimeInMillis(paramArrayOfInt2.getLong(paramArrayOfInt2.getColumnIndexOrThrow("txnDate")));
      localObject5 = ((Calendar)localObject1).getTime();
      k = paramArrayOfInt2.getInt(paramArrayOfInt2.getColumnIndexOrThrow("type"));
      int m = paramArrayOfInt2.getInt(paramArrayOfInt2.getColumnIndexOrThrow("flags"));
      int n = paramArrayOfInt2.getInt(paramArrayOfInt2.getColumnIndexOrThrow("accountId"));
      String str1 = paramArrayOfInt2.getString(paramArrayOfInt2.getColumnIndex("displayPan"));
      String str2 = paramArrayOfInt2.getString(paramArrayOfInt2.getColumnIndexOrThrow("name"));
      int i1 = paramArrayOfInt2.getInt(paramArrayOfInt2.getColumnIndexOrThrow("wSmsId"));
      paramArrayOfInt2.getString(paramArrayOfInt2.getColumnIndexOrThrow("tags"));
      double d2 = paramArrayOfInt2.getDouble(paramArrayOfInt2.getColumnIndexOrThrow("txnBalance"));
      double d3 = paramArrayOfInt2.getDouble(paramArrayOfInt2.getColumnIndexOrThrow("txnOutstandingBalance"));
      String str3 = paramArrayOfInt2.getString(paramArrayOfInt2.getColumnIndexOrThrow("name"));
      String str4 = paramArrayOfInt2.getString(paramArrayOfInt2.getColumnIndexOrThrow("displayName"));
      int i2 = paramArrayOfInt2.getColumnIndex("accountFlags");
      double d4 = paramArrayOfInt2.getDouble(paramArrayOfInt2.getColumnIndexOrThrow("conversionRate"));
      double d5 = paramArrayOfInt2.getDouble(paramArrayOfInt2.getColumnIndexOrThrow("currencyAmount"));
      localObject1 = paramArrayOfInt2.getString(paramArrayOfInt2.getColumnIndexOrThrow("currencySymbol"));
      boolean bool;
      if (i2 != -1)
      {
        if ((paramArrayOfInt2.getInt(i2) & 0x10) == 0) {
          bool = true;
        } else {
          bool = false;
        }
      }
      else {
        bool = true;
      }
      localObject2 = paramArrayOfInt2.getString(paramArrayOfInt2.getColumnIndexOrThrow("UUID"));
      paramDate1 = new m(paramDate1, (String)localObject4, (Date)localObject3);
      o = j;
      w = n;
      A = str3;
      y = str4;
      C = i1;
      u = 3;
      paramDate1.a(str2, "Spends");
      paramDate1.a(str1, Double.valueOf(d1), (Date)localObject5, paramDate2, k);
      Y = m;
      paramDate2 = new com.daamitt.prime.sdk.a.b();
      a = d2;
      b = d3;
      i = paramDate2;
      L = bool;
      ab = ((String)localObject2);
      k = paramArrayOfInt2.getLong(paramArrayOfInt2.getColumnIndexOrThrow("merchantId"));
      T = paramArrayOfInt2.getString(paramArrayOfInt2.getColumnIndexOrThrow("categories"));
      U = paramArrayOfInt2.getString(paramArrayOfInt2.getColumnIndexOrThrow("txnTags"));
      V = paramArrayOfInt2.getString(paramArrayOfInt2.getColumnIndexOrThrow("txnNote"));
      Z = paramArrayOfInt2.getString(paramArrayOfInt2.getColumnIndexOrThrow("txnPhoto"));
      aa = paramArrayOfInt2.getString(paramArrayOfInt2.getColumnIndexOrThrow("txnPhotoServerPath"));
      l = paramArrayOfInt2.getString(paramArrayOfInt2.getColumnIndexOrThrow("placeId"));
      m = paramArrayOfInt2.getString(paramArrayOfInt2.getColumnIndexOrThrow("placeName"));
      n = paramArrayOfInt2.getString(paramArrayOfInt2.getColumnIndexOrThrow("placeDisplayName"));
      g = d4;
      h = d5;
      e = ((String)localObject1);
      W = paramArrayOfInt2.getString(paramArrayOfInt2.getColumnIndexOrThrow("recursionAccountUUID"));
      d1 = paramArrayOfInt2.getDouble(paramArrayOfInt2.getColumnIndex("placeLat"));
      d2 = paramArrayOfInt2.getDouble(paramArrayOfInt2.getColumnIndex("placeLon"));
      if ((d1 != 360.0D) && (d2 != 360.0D))
      {
        paramDate2 = new Location("walnutloc");
        paramDate2.setLatitude(d1);
        paramDate2.setLongitude(d2);
        X = paramDate2;
      }
      float f1 = paramArrayOfInt2.getFloat(paramArrayOfInt2.getColumnIndex("locAccuracy"));
      if (f1 != -1.0F)
      {
        d1 = paramArrayOfInt2.getDouble(paramArrayOfInt2.getColumnIndex("lat"));
        d2 = paramArrayOfInt2.getDouble(paramArrayOfInt2.getColumnIndex("long"));
        paramDate2 = new Location("walnutloc");
        paramDate2.setAccuracy(f1);
        paramDate2.setLatitude(d1);
        paramDate2.setLongitude(d2);
        D = paramDate2;
      }
      paramArrayOfInt1.add(paramDate1);
      paramArrayOfInt2.moveToNext();
    }
    paramArrayOfInt2.close();
    return paramArrayOfInt1;
  }
  
  public final void a(a parama, long paramLong)
  {
    m localm = new m(null, null, null);
    C = 0L;
    Y = 16;
    w = a;
    localm.a("DUMMY", Double.valueOf(0.0D), new Date(paramLong), "DUMMY", 11);
    a(localm);
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt)
  {
    int k = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    e.a();
    if (paramCalendar1.before(paramCalendar2))
    {
      String[] arrayOfString = new String[k];
      paramCalendar2 = f(paramCalendar1, paramCalendar2, paramInt);
      Object localObject = Calendar.getInstance().getTimeZone();
      long l2 = ((TimeZone)localObject).getRawOffset() + ((TimeZone)localObject).getDSTSavings();
      long l1 = l2 / 3600000L;
      l2 = l2 % 3600000L / 60000L;
      localObject = new StringBuilder("strftime('%j',date(txnDate/1000,'unixepoch','");
      ((StringBuilder)localObject).append(l1);
      ((StringBuilder)localObject).append(" hours','");
      ((StringBuilder)localObject).append(l2);
      ((StringBuilder)localObject).append(" minutes')) as Day");
      localObject = ((StringBuilder)localObject).toString();
      int j = 0;
      paramCalendar2 = e.query("transactions", new String[] { localObject, "txnBalance" }, paramCalendar2, null, "Day", " MAX(txnDate)", null, null);
      if ((paramCalendar2 != null) && (paramCalendar2.getCount() > 0))
      {
        paramCalendar2.moveToFirst();
        int m = paramCalendar1.get(6);
        int n = paramCalendar1.getActualMaximum(6);
        while (!paramCalendar2.isAfterLast())
        {
          localObject = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("Day"));
          paramCalendar1 = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("txnBalance"));
          try
          {
            paramInt = Integer.parseInt((String)localObject);
          }
          catch (NumberFormatException localNumberFormatException)
          {
            int i1;
            for (;;) {}
          }
          paramInt = -1;
          if ((paramInt != -1) && (paramCalendar1 != null))
          {
            i1 = (int)Double.valueOf(paramCalendar1).doubleValue();
            if (paramInt >= m) {
              arrayOfString[(k - (paramInt - m) - 1)] = String.valueOf(i1);
            } else {
              arrayOfString[(k - (n - m + paramInt) - 1)] = String.valueOf(i1);
            }
          }
          paramCalendar2.moveToNext();
        }
      }
      paramInt = j;
      if (paramCalendar2 != null)
      {
        paramCalendar2.close();
        paramInt = j;
      }
      while (paramInt < k)
      {
        if (TextUtils.isEmpty(arrayOfString[paramInt])) {
          arrayOfString[paramInt] = "";
        }
        paramInt += 1;
      }
      return arrayOfString;
    }
    return null;
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt, String paramString1, String paramString2)
  {
    int k = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    e.a();
    if (paramCalendar1.before(paramCalendar2))
    {
      String[] arrayOfString = new String[k];
      paramCalendar2 = a(paramCalendar1, paramCalendar2, paramString1, paramInt, paramString2);
      paramString1 = Calendar.getInstance().getTimeZone();
      long l2 = paramString1.getRawOffset() + paramString1.getDSTSavings();
      long l1 = l2 / 3600000L;
      l2 = l2 % 3600000L / 60000L;
      paramString1 = new StringBuilder("strftime('%j',date(txnDate/1000,'unixepoch','");
      paramString1.append(l1);
      paramString1.append(" hours','");
      paramString1.append(l2);
      paramString1.append(" minutes')) as Day");
      paramString1 = paramString1.toString();
      int j = 0;
      paramString2 = f;
      paramCalendar2 = e.query(g, new String[] { paramString1, paramString2 }, paramCalendar2, null, "Day", null, null, null);
      if ((paramCalendar2 != null) && (paramCalendar2.getCount() > 0))
      {
        paramCalendar2.moveToFirst();
        int m = paramCalendar1.get(6);
        int n = paramCalendar1.getActualMaximum(6);
        while (!paramCalendar2.isAfterLast())
        {
          paramString1 = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("Day"));
          paramCalendar1 = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("Amount"));
          try
          {
            paramInt = Integer.parseInt(paramString1);
          }
          catch (NumberFormatException paramString1)
          {
            int i1;
            for (;;) {}
          }
          paramInt = -1;
          if ((paramInt != -1) && (paramCalendar1 != null))
          {
            i1 = (int)Double.valueOf(paramCalendar1).doubleValue();
            if (paramInt >= m) {
              arrayOfString[(k - (paramInt - m) - 1)] = String.valueOf(i1);
            } else {
              arrayOfString[(k - (n - m + paramInt) - 1)] = String.valueOf(i1);
            }
          }
          paramCalendar2.moveToNext();
        }
      }
      paramInt = j;
      if (paramCalendar2 != null)
      {
        paramCalendar2.close();
        paramInt = j;
      }
      while (paramInt < k)
      {
        if (TextUtils.isEmpty(arrayOfString[paramInt])) {
          arrayOfString[paramInt] = "";
        }
        paramInt += 1;
      }
      return arrayOfString;
    }
    return null;
  }
  
  public final String[] b(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt)
  {
    int k = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    e.a();
    if (paramCalendar1.before(paramCalendar2))
    {
      String[] arrayOfString = new String[k];
      int j = 0;
      Object localObject = new StringBuilder("transactions.type in ( ");
      ((StringBuilder)localObject).append(m.f());
      ((StringBuilder)localObject).append("  )  and txnDate between ");
      ((StringBuilder)localObject).append(paramCalendar1.getTime().getTime());
      ((StringBuilder)localObject).append(" AND ");
      ((StringBuilder)localObject).append(paramCalendar2.getTime().getTime());
      ((StringBuilder)localObject).append(" AND txnBalance NOTNULL  AND accountId IN (");
      ((StringBuilder)localObject).append(a.c(new int[] { paramInt }));
      ((StringBuilder)localObject).append(")");
      paramCalendar2 = ((StringBuilder)localObject).toString();
      localObject = Calendar.getInstance().getTimeZone();
      long l2 = ((TimeZone)localObject).getRawOffset() + ((TimeZone)localObject).getDSTSavings();
      long l1 = l2 / 3600000L;
      l2 = l2 % 3600000L / 60000L;
      localObject = new StringBuilder("strftime('%j',date(txnDate/1000,'unixepoch','");
      ((StringBuilder)localObject).append(l1);
      ((StringBuilder)localObject).append(" hours','");
      ((StringBuilder)localObject).append(l2);
      ((StringBuilder)localObject).append(" minutes')) as Day");
      localObject = ((StringBuilder)localObject).toString();
      paramCalendar2 = e.query("transactions", new String[] { localObject, "txnBalance" }, paramCalendar2, null, "Day", " MIN(txnBalance)", null, null);
      if ((paramCalendar2 != null) && (paramCalendar2.getCount() > 0))
      {
        paramCalendar2.moveToFirst();
        int m = paramCalendar1.get(6);
        int n = paramCalendar1.getActualMaximum(6);
        while (!paramCalendar2.isAfterLast())
        {
          localObject = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("Day"));
          paramCalendar1 = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("txnBalance"));
          try
          {
            paramInt = Integer.parseInt((String)localObject);
          }
          catch (NumberFormatException localNumberFormatException)
          {
            int i1;
            for (;;) {}
          }
          paramInt = -1;
          if ((paramInt != -1) && (paramCalendar1 != null))
          {
            i1 = (int)Double.valueOf(paramCalendar1).doubleValue();
            if (paramInt >= m) {
              arrayOfString[(k - (paramInt - m) - 1)] = String.valueOf(i1);
            } else {
              arrayOfString[(k - (n - m + paramInt) - 1)] = String.valueOf(i1);
            }
          }
          paramCalendar2.moveToNext();
        }
      }
      paramInt = j;
      if (paramCalendar2 != null)
      {
        paramCalendar2.close();
        paramInt = j;
      }
      while (paramInt < k)
      {
        if (TextUtils.isEmpty(arrayOfString[paramInt])) {
          arrayOfString[paramInt] = "";
        }
        paramInt += 1;
      }
      return arrayOfString;
    }
    return null;
  }
  
  public final String[][] c(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt)
  {
    int j = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    e.a();
    if (paramCalendar1.before(paramCalendar2))
    {
      String[][] arrayOfString = (String[][])Array.newInstance(String.class, new int[] { 2, j });
      new SQLiteQueryBuilder().setTables(g);
      paramCalendar2 = f(paramCalendar1, paramCalendar2, paramInt);
      Object localObject = Calendar.getInstance().getTimeZone();
      long l2 = ((TimeZone)localObject).getRawOffset() + ((TimeZone)localObject).getDSTSavings();
      long l1 = l2 / 3600000L;
      l2 = l2 % 3600000L / 60000L;
      localObject = new StringBuilder("strftime('%j',date(txnDate/1000,'unixepoch','");
      ((StringBuilder)localObject).append(l1);
      ((StringBuilder)localObject).append(" hours','");
      ((StringBuilder)localObject).append(l2);
      ((StringBuilder)localObject).append(" minutes')) as Day");
      localObject = ((StringBuilder)localObject).toString();
      paramCalendar2 = e.query(g, new String[] { localObject, "txnBalance", "txnOutstandingBalance" }, paramCalendar2, null, "Day, accountId", null, null, null);
      if ((paramCalendar2 != null) && (paramCalendar2.getCount() > 0))
      {
        paramCalendar2.moveToFirst();
        int k = paramCalendar1.get(6);
        int m = paramCalendar1.getActualMaximum(6);
        while (!paramCalendar2.isAfterLast())
        {
          String str = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("Day"));
          paramCalendar1 = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("txnBalance"));
          localObject = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("txnOutstandingBalance"));
          try
          {
            paramInt = Integer.parseInt(str);
          }
          catch (NumberFormatException localNumberFormatException)
          {
            int n;
            double d1;
            int i1;
            for (;;) {}
          }
          paramInt = -1;
          if ((paramInt != -1) && (paramCalendar1 != null) && (localObject != null))
          {
            n = (int)Double.valueOf(paramCalendar1).doubleValue();
            d1 = Double.valueOf((String)localObject).doubleValue();
            if (d1 > Double.MIN_VALUE)
            {
              i1 = (int)d1;
              if (paramInt >= k)
              {
                paramCalendar1 = arrayOfString[0];
                paramInt = j - (paramInt - k) - 1;
                paramCalendar1[paramInt] = String.valueOf(n);
                arrayOfString[1][paramInt] = String.valueOf(i1);
              }
              else
              {
                paramCalendar1 = arrayOfString[0];
                paramInt = j - (m - k + paramInt) - 1;
                paramCalendar1[paramInt] = String.valueOf(n);
                arrayOfString[1][paramInt] = String.valueOf(i1);
              }
            }
          }
          paramCalendar2.moveToNext();
        }
      }
      if (paramCalendar2 != null) {
        paramCalendar2.close();
      }
      paramInt = 0;
      while (paramInt < j)
      {
        paramCalendar1 = arrayOfString[0][paramInt];
        paramCalendar2 = arrayOfString[1][paramInt];
        if (TextUtils.isEmpty(paramCalendar1)) {
          arrayOfString[0][paramInt] = "";
        }
        if (TextUtils.isEmpty(paramCalendar2)) {
          arrayOfString[1][paramInt] = "";
        }
        paramInt += 1;
      }
      return arrayOfString;
    }
    return null;
  }
  
  public final String[] d(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt)
  {
    int k = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    e.a();
    if (paramCalendar1.before(paramCalendar2))
    {
      String[] arrayOfString = new String[k];
      paramCalendar2 = a(paramCalendar1, paramCalendar2, null, 0, m.d());
      if (paramInt != 0)
      {
        localObject = new StringBuilder();
        ((StringBuilder)localObject).append(paramCalendar2);
        ((StringBuilder)localObject).append(" AND transactions.flags & ");
        ((StringBuilder)localObject).append(paramInt);
        ((StringBuilder)localObject).append(" != 0 ");
        paramCalendar2 = ((StringBuilder)localObject).toString();
      }
      Object localObject = Calendar.getInstance().getTimeZone();
      long l2 = ((TimeZone)localObject).getRawOffset() + ((TimeZone)localObject).getDSTSavings();
      long l1 = l2 / 3600000L;
      l2 = l2 % 3600000L / 60000L;
      localObject = new StringBuilder("strftime('%j',date(txnDate/1000,'unixepoch','");
      ((StringBuilder)localObject).append(l1);
      ((StringBuilder)localObject).append(" hours','");
      ((StringBuilder)localObject).append(l2);
      ((StringBuilder)localObject).append(" minutes')) as Day");
      localObject = ((StringBuilder)localObject).toString();
      int j = 0;
      String str = f;
      paramCalendar2 = e.query(g, new String[] { localObject, str }, paramCalendar2, null, "Day", null, null, null);
      if ((paramCalendar2 != null) && (paramCalendar2.getCount() > 0))
      {
        paramCalendar2.moveToFirst();
        int m = paramCalendar1.get(6);
        int n = paramCalendar1.getActualMaximum(6);
        while (!paramCalendar2.isAfterLast())
        {
          localObject = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("Day"));
          paramCalendar1 = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("Amount"));
          try
          {
            paramInt = Integer.parseInt((String)localObject);
          }
          catch (NumberFormatException localNumberFormatException)
          {
            int i1;
            for (;;) {}
          }
          paramInt = -1;
          if ((paramInt != -1) && (paramCalendar1 != null))
          {
            i1 = (int)Double.valueOf(paramCalendar1).doubleValue();
            if (paramInt >= m) {
              arrayOfString[(k - (paramInt - m) - 1)] = String.valueOf(i1);
            } else {
              arrayOfString[(k - (n - m + paramInt) - 1)] = String.valueOf(i1);
            }
          }
          paramCalendar2.moveToNext();
        }
      }
      paramInt = j;
      if (paramCalendar2 != null)
      {
        paramCalendar2.close();
        paramInt = j;
      }
      while (paramInt < k)
      {
        if (TextUtils.isEmpty(arrayOfString[paramInt])) {
          arrayOfString[paramInt] = "";
        }
        paramInt += 1;
      }
      return arrayOfString;
    }
    return null;
  }
  
  public final String[] e(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt)
  {
    int k = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    e.a();
    if (paramCalendar1.before(paramCalendar2))
    {
      String[] arrayOfString = new String[k];
      int j = 0;
      Object localObject = new StringBuilder("transactions.type = 17 AND transactions.flags & 2097152 != 0  AND txnDate between ");
      ((StringBuilder)localObject).append(paramCalendar1.getTime().getTime());
      ((StringBuilder)localObject).append(" AND ");
      ((StringBuilder)localObject).append(paramCalendar2.getTime().getTime());
      ((StringBuilder)localObject).append(" AND accountId IN (");
      ((StringBuilder)localObject).append(a.c(new int[] { paramInt }));
      ((StringBuilder)localObject).append(")");
      paramCalendar2 = ((StringBuilder)localObject).toString();
      localObject = Calendar.getInstance().getTimeZone();
      long l2 = ((TimeZone)localObject).getRawOffset() + ((TimeZone)localObject).getDSTSavings();
      long l1 = l2 / 3600000L;
      l2 = l2 % 3600000L / 60000L;
      localObject = new StringBuilder("strftime('%j',date(txnDate/1000,'unixepoch','");
      ((StringBuilder)localObject).append(l1);
      ((StringBuilder)localObject).append(" hours','");
      ((StringBuilder)localObject).append(l2);
      ((StringBuilder)localObject).append(" minutes')) as Day");
      localObject = ((StringBuilder)localObject).toString();
      paramCalendar2 = e.query("transactions", new String[] { localObject, "amount" }, paramCalendar2, null, "Day", " MAX(amount)", null, null);
      if ((paramCalendar2 != null) && (paramCalendar2.getCount() > 0))
      {
        paramCalendar2.moveToFirst();
        int m = paramCalendar1.get(6);
        int n = paramCalendar1.getActualMaximum(6);
        while (!paramCalendar2.isAfterLast())
        {
          localObject = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("Day"));
          paramCalendar1 = paramCalendar2.getString(paramCalendar2.getColumnIndexOrThrow("amount"));
          try
          {
            paramInt = Integer.parseInt((String)localObject);
          }
          catch (NumberFormatException localNumberFormatException)
          {
            int i1;
            for (;;) {}
          }
          paramInt = -1;
          if ((paramInt != -1) && (paramCalendar1 != null))
          {
            i1 = (int)Double.valueOf(paramCalendar1).doubleValue();
            if (paramInt >= m) {
              arrayOfString[(k - (paramInt - m) - 1)] = String.valueOf(i1);
            } else {
              arrayOfString[(k - (n - m + paramInt) - 1)] = String.valueOf(i1);
            }
          }
          paramCalendar2.moveToNext();
        }
      }
      paramInt = j;
      if (paramCalendar2 != null)
      {
        paramCalendar2.close();
        paramInt = j;
      }
      while (paramInt < k)
      {
        if (TextUtils.isEmpty(arrayOfString[paramInt])) {
          arrayOfString[paramInt] = "";
        }
        paramInt += 1;
      }
      return arrayOfString;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.b.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */