package com.daamitt.prime.sdk.b;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.daamitt.prime.sdk.a.e;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class a
{
  private static String c = "a";
  private static a e;
  SQLiteDatabase a = null;
  public b b;
  private String[] d = { "_id", "name", "displayName", "pan", "displayPan", "type", "flags", "startDate", "endDate", "enabled", "UUID", "modifyCount", "MUUID", "balance", "outstandingBalance", "balLastSyncTime", "outBalLastSyncTime", "updatedTime", "accountColor", "cardIssuer", "recursionFlag" };
  
  private a(b paramb)
  {
    b = paramb;
  }
  
  private static com.daamitt.prime.sdk.a.a a(Cursor paramCursor)
  {
    com.daamitt.prime.sdk.a.a locala = new com.daamitt.prime.sdk.a.a(paramCursor.getString(paramCursor.getColumnIndexOrThrow("name")), paramCursor.getString(paramCursor.getColumnIndexOrThrow("pan")), paramCursor.getInt(paramCursor.getColumnIndexOrThrow("type")));
    e = paramCursor.getString(paramCursor.getColumnIndexOrThrow("displayName"));
    g = paramCursor.getString(paramCursor.getColumnIndexOrThrow("displayPan"));
    a = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("_id"));
    l = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("flags"));
    b = paramCursor.getString(paramCursor.getColumnIndexOrThrow("UUID"));
    c = paramCursor.getString(paramCursor.getColumnIndexOrThrow("MUUID"));
    int i = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("enabled"));
    boolean bool = true;
    if (i != 1) {
      bool = false;
    }
    k = bool;
    p = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("startDate"));
    q = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("endDate"));
    j = paramCursor.getLong(paramCursor.getColumnIndexOrThrow("updatedTime"));
    m = b(paramCursor);
    n = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("accountColor"));
    b = paramCursor.getString(paramCursor.getColumnIndexOrThrow("UUID"));
    h = paramCursor.getString(paramCursor.getColumnIndexOrThrow("cardIssuer"));
    r = paramCursor.getInt(paramCursor.getColumnIndexOrThrow("recursionFlag"));
    return locala;
  }
  
  public static a a(b paramb)
  {
    if (e == null)
    {
      a locala = new a(paramb);
      e = locala;
      a = paramb.getWritableDatabase();
    }
    return e;
  }
  
  static String a(int[] paramArrayOfInt)
  {
    if ((paramArrayOfInt != null) && (paramArrayOfInt.length > 0))
    {
      int j = paramArrayOfInt.length;
      int i = 1;
      StringBuilder localStringBuilder1 = new StringBuilder(j * 2 - 1);
      localStringBuilder1.append(paramArrayOfInt[0]);
      while (i < paramArrayOfInt.length)
      {
        StringBuilder localStringBuilder2 = new StringBuilder(",");
        localStringBuilder2.append(paramArrayOfInt[i]);
        localStringBuilder1.append(localStringBuilder2.toString());
        i += 1;
      }
      return localStringBuilder1.toString();
    }
    return null;
  }
  
  static String a(String[] paramArrayOfString)
  {
    if ((paramArrayOfString != null) && (paramArrayOfString.length > 0))
    {
      int j = paramArrayOfString.length;
      int i = 1;
      StringBuilder localStringBuilder1 = new StringBuilder(j * 2 - 1);
      localStringBuilder1.append(paramArrayOfString[0]);
      while (i < paramArrayOfString.length)
      {
        StringBuilder localStringBuilder2 = new StringBuilder(",");
        localStringBuilder2.append(paramArrayOfString[i]);
        localStringBuilder1.append(localStringBuilder2.toString());
        i += 1;
      }
      return localStringBuilder1.toString();
    }
    return null;
  }
  
  public static void a(ContentValues paramContentValues, com.daamitt.prime.sdk.a.b paramb)
  {
    if (paramb != null)
    {
      if (a != Double.MIN_VALUE)
      {
        paramContentValues.put("balance", Double.valueOf(a));
        if (c != null) {
          paramContentValues.put("balLastSyncTime", Long.valueOf(c.getTime()));
        }
      }
      if (b != Double.MIN_VALUE)
      {
        paramContentValues.put("outstandingBalance", Double.valueOf(b));
        if (d != null) {
          paramContentValues.put("outBalLastSyncTime", Long.valueOf(d.getTime()));
        }
      }
    }
  }
  
  public static void a(SQLiteDatabase paramSQLiteDatabase)
  {
    e.b();
    paramSQLiteDatabase.execSQL("create table if not exists accounts(_id integer primary key autoincrement, name text not null, displayName text, pan text not null,displayPan text,type integer not null, flags integer default 0,startDate integer,endDate integer,enabled boolean default 1,UUID text,modifyCount integer default 1,MUUID text,balance real,outstandingBalance real,balLastSyncTime integer, outBalLastSyncTime integer, updatedTime integer,accountColor integer,cardIssuer text,recursionFlag int default -1);");
    paramSQLiteDatabase.execSQL("create trigger if not exists AccountsTriggerModifiedFlag After update on accounts for each row  Begin  Update accounts Set modifyCount = modifyCount + 1  Where _id =  New._id;  End; ");
    paramSQLiteDatabase.execSQL("PRAGMA recursive_triggers = false;");
  }
  
  private static com.daamitt.prime.sdk.a.b b(Cursor paramCursor)
  {
    com.daamitt.prime.sdk.a.b localb = new com.daamitt.prime.sdk.a.b();
    a = paramCursor.getDouble(paramCursor.getColumnIndexOrThrow("balance"));
    b = paramCursor.getDouble(paramCursor.getColumnIndexOrThrow("outstandingBalance"));
    long l1 = paramCursor.getLong(paramCursor.getColumnIndexOrThrow("balLastSyncTime"));
    long l2 = paramCursor.getLong(paramCursor.getColumnIndexOrThrow("outBalLastSyncTime"));
    if (l1 != 0L)
    {
      paramCursor = Calendar.getInstance();
      paramCursor.setTimeInMillis(l1);
      c = paramCursor.getTime();
    }
    if (l2 != 0L)
    {
      paramCursor = Calendar.getInstance();
      paramCursor.setTimeInMillis(l2);
      d = paramCursor.getTime();
    }
    return localb;
  }
  
  private String b(String paramString)
  {
    Object localObject1 = a;
    Object localObject2 = new StringBuilder("UUID = '");
    ((StringBuilder)localObject2).append(paramString);
    ((StringBuilder)localObject2).append("'");
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1 = ((SQLiteDatabase)localObject1).query("accounts", new String[] { "MUUID" }, (String)localObject2, null, null, null, null);
    if ((localObject1 != null) && (((Cursor)localObject1).getCount() > 0))
    {
      localObject2 = new ArrayList();
      while (((Cursor)localObject1).moveToNext())
      {
        String str = ((Cursor)localObject1).getString(((Cursor)localObject1).getColumnIndex("MUUID"));
        ((ArrayList)localObject2).add(str);
        a(1, str, (ArrayList)localObject2);
      }
    }
    if (localObject1 != null) {
      ((Cursor)localObject1).close();
    }
    return paramString;
  }
  
  public static void b(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.beginTransaction();
    try
    {
      paramSQLiteDatabase.execSQL("drop table if exists accounts");
      paramSQLiteDatabase.execSQL("drop trigger if exists AccountsTriggerModifiedFlag");
      a(paramSQLiteDatabase);
      paramSQLiteDatabase.setTransactionSuccessful();
      return;
    }
    finally
    {
      paramSQLiteDatabase.endTransaction();
    }
  }
  
  private String d(long paramLong)
  {
    String str = "_id = ".concat(String.valueOf(paramLong));
    Cursor localCursor = a.query("accounts", new String[] { "MUUID" }, str, null, null, null, null);
    localCursor.moveToFirst();
    if (!localCursor.isAfterLast()) {
      str = localCursor.getString(localCursor.getColumnIndexOrThrow("MUUID"));
    } else {
      str = null;
    }
    localCursor.close();
    return str;
  }
  
  public final int a(com.daamitt.prime.sdk.a.a parama, ContentValues paramContentValues)
  {
    if (a >= 0)
    {
      SQLiteDatabase localSQLiteDatabase = a;
      StringBuilder localStringBuilder = new StringBuilder("_id = ");
      localStringBuilder.append(a);
      return localSQLiteDatabase.update("accounts", paramContentValues, localStringBuilder.toString(), null);
    }
    return -1;
  }
  
  public final com.daamitt.prime.sdk.a.a a(String paramString1, String paramString2, int paramInt)
  {
    return a(paramString1, paramString2, paramInt, true, null);
  }
  
  public final com.daamitt.prime.sdk.a.a a(String paramString1, String paramString2, int paramInt, boolean paramBoolean, String paramString3)
  {
    a.beginTransaction();
    try
    {
      Object localObject = a.query("accounts", d, "name=? AND pan=?", new String[] { paramString1, paramString2 }, null, null, null);
      if ((localObject != null) && (((Cursor)localObject).getCount() > 0))
      {
        ((Cursor)localObject).moveToFirst();
        paramString1 = a((Cursor)localObject);
        ((Cursor)localObject).close();
        return paramString1;
      }
      if (localObject != null) {
        ((Cursor)localObject).close();
      }
      localObject = new ContentValues();
      ((ContentValues)localObject).put("name", paramString1);
      if (!TextUtils.isEmpty(paramString3)) {
        paramString1 = paramString3;
      }
      ((ContentValues)localObject).put("displayName", paramString1);
      ((ContentValues)localObject).put("pan", paramString2);
      if (!TextUtils.isEmpty(null)) {
        paramString2 = null;
      }
      ((ContentValues)localObject).put("displayPan", paramString2);
      ((ContentValues)localObject).put("type", Integer.valueOf(paramInt));
      if (!paramBoolean) {
        ((ContentValues)localObject).put("flags", Integer.valueOf(16));
      }
      ((ContentValues)localObject).put("updatedTime", Long.valueOf(System.currentTimeMillis()));
      ((ContentValues)localObject).put("UUID", UUID.randomUUID().toString());
      long l = a.insertOrThrow("accounts", null, (ContentValues)localObject);
      paramString1 = a.query("accounts", d, "_id = ".concat(String.valueOf(l)), null, null, null, null);
      paramString1.moveToFirst();
      paramString2 = a(paramString1);
      s = true;
      paramString1.close();
      a.setTransactionSuccessful();
      return paramString2;
    }
    finally
    {
      a.endTransaction();
    }
  }
  
  public final String a(int paramInt, String paramString, ArrayList<String> paramArrayList)
  {
    Object localObject1 = a;
    Object localObject2 = new StringBuilder("MUUID = '");
    ((StringBuilder)localObject2).append(paramString);
    ((StringBuilder)localObject2).append("'");
    localObject2 = ((StringBuilder)localObject2).toString();
    Cursor localCursor = ((SQLiteDatabase)localObject1).query("accounts", new String[] { "UUID" }, (String)localObject2, null, null, null, null);
    localObject1 = "";
    localObject2 = localObject1;
    if (localCursor != null)
    {
      localObject2 = localObject1;
      if (localCursor.getCount() > 0)
      {
        localCursor.moveToNext();
        for (;;)
        {
          localObject2 = localObject1;
          if (localCursor.isAfterLast()) {
            break;
          }
          String str = localCursor.getString(localCursor.getColumnIndex("UUID"));
          localObject2 = localObject1;
          if (!TextUtils.equals(paramString, str))
          {
            localObject2 = localObject1;
            if (!paramArrayList.contains(str))
            {
              paramArrayList.add(str);
              localObject2 = new StringBuilder();
              ((StringBuilder)localObject2).append((String)localObject1);
              ((StringBuilder)localObject2).append(a(paramInt + 1, str, paramArrayList));
              localObject2 = ((StringBuilder)localObject2).toString();
            }
          }
          localCursor.moveToNext();
          localObject1 = localObject2;
        }
      }
    }
    if (localCursor != null) {
      localCursor.close();
    }
    paramArrayList = new StringBuilder();
    paramArrayList.append((String)localObject2);
    paramArrayList.append("'");
    paramArrayList.append(paramString);
    paramArrayList.append("'");
    paramString = paramArrayList.toString();
    if (paramInt > 0)
    {
      paramArrayList = new StringBuilder();
      paramArrayList.append(paramString);
      paramArrayList.append(", ");
      return paramArrayList.toString();
    }
    paramArrayList = new StringBuilder("(");
    paramArrayList.append(paramString);
    paramArrayList.append(")");
    return paramArrayList.toString();
  }
  
  public final String a(long paramLong)
  {
    String str = "_id = ".concat(String.valueOf(paramLong));
    Cursor localCursor = a.query("accounts", new String[] { "UUID" }, str, null, null, null, null);
    localCursor.moveToFirst();
    if (!localCursor.isAfterLast()) {
      str = localCursor.getString(localCursor.getColumnIndexOrThrow("UUID"));
    } else {
      str = null;
    }
    localCursor.close();
    return str;
  }
  
  public final ArrayList<com.daamitt.prime.sdk.a.a> a(int paramInt)
  {
    ArrayList localArrayList = new ArrayList();
    Object localObject = "flags & 1 = 0 AND flags & 2 = 0 AND enabled = 1 AND flags & 64  = 0 AND type = ".concat(String.valueOf(paramInt));
    localObject = a.query("accounts", d, (String)localObject, null, null, null, null);
    if ((localObject != null) && (((Cursor)localObject).getCount() > 0))
    {
      ((Cursor)localObject).moveToFirst();
      while (!((Cursor)localObject).isAfterLast())
      {
        localArrayList.add(a((Cursor)localObject));
        ((Cursor)localObject).moveToNext();
      }
    }
    if (localObject != null) {
      ((Cursor)localObject).close();
    }
    return localArrayList;
  }
  
  public final ArrayList<com.daamitt.prime.sdk.a.a> a(String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    StringBuilder localStringBuilder = new StringBuilder("name LIKE '");
    localStringBuilder.append(paramString);
    localStringBuilder.append("%' AND flags & 1 = 0 AND flags & 2 = 0  AND flags & 64  = 0  AND (type = 1 OR type = 2 OR type = 3 )");
    paramString = localStringBuilder.toString();
    paramString = a.query("accounts", d, paramString, null, null, null, null);
    if ((paramString != null) && (paramString.getCount() > 0))
    {
      paramString.moveToFirst();
      while (!paramString.isAfterLast())
      {
        localArrayList.add(a(paramString));
        paramString.moveToNext();
      }
    }
    if (paramString != null) {
      paramString.close();
    }
    return localArrayList;
  }
  
  public final void a(com.daamitt.prime.sdk.a.a parama1, com.daamitt.prime.sdk.a.a parama2, long paramLong)
  {
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("updatedTime", Long.valueOf(paramLong));
    localContentValues.put("MUUID", a(a));
    a(parama1, localContentValues);
    Object localObject1 = new ContentValues();
    a((ContentValues)localObject1, m);
    Object localObject2;
    Object localObject3;
    if (((ContentValues)localObject1).size() > 0)
    {
      localObject2 = a;
      localObject3 = new StringBuilder();
      ((StringBuilder)localObject3).append(a);
      ((SQLiteDatabase)localObject2).update("accounts", (ContentValues)localObject1, "_id =?", new String[] { ((StringBuilder)localObject3).toString() });
    }
    parama2 = a(a);
    localObject1 = a.query("accounts", d, "MUUID =?", new String[] { parama2 }, null, null, null);
    if ((localObject1 != null) && (((Cursor)localObject1).getCount() > 0))
    {
      ((Cursor)localObject1).moveToFirst();
      while (!((Cursor)localObject1).isAfterLast())
      {
        localObject2 = a((Cursor)localObject1);
        localObject3 = a;
        StringBuilder localStringBuilder = new StringBuilder("Update transactions Set modifyCount = modifyCount + 1 Where accountId = ");
        localStringBuilder.append(a);
        ((SQLiteDatabase)localObject3).execSQL(localStringBuilder.toString());
        ((Cursor)localObject1).moveToNext();
      }
      ((Cursor)localObject1).close();
      a.update("accounts", localContentValues, "MUUID =?", new String[] { parama2 });
    }
    if (localObject1 != null) {
      ((Cursor)localObject1).close();
    }
    parama2 = new StringBuilder("update accounts set flags = flags | 2 where _id = ");
    parama2.append(a);
    parama1 = parama2.toString();
    a.execSQL(parama1);
  }
  
  public final com.daamitt.prime.sdk.a.a b(int paramInt)
  {
    Object localObject = "_id = ".concat(String.valueOf(paramInt));
    localObject = a.query("accounts", d, (String)localObject, null, null, null, null);
    if ((localObject != null) && (((Cursor)localObject).getCount() > 0))
    {
      ((Cursor)localObject).moveToFirst();
      com.daamitt.prime.sdk.a.a locala = a((Cursor)localObject);
      ((Cursor)localObject).close();
      return locala;
    }
    if (localObject != null) {
      ((Cursor)localObject).close();
    }
    return null;
  }
  
  public final com.daamitt.prime.sdk.a.a b(long paramLong)
  {
    Object localObject = new StringBuilder("_id IN(");
    ((StringBuilder)localObject).append(c(paramLong));
    ((StringBuilder)localObject).append(") AND flags & 1 = 0 AND flags & 64 = 0 ");
    localObject = ((StringBuilder)localObject).toString();
    Cursor localCursor = a.query("accounts", d, (String)localObject, null, null, null, null);
    if ((localCursor != null) && (localCursor.getCount() > 0))
    {
      localCursor.moveToFirst();
      localObject = a(localCursor);
    }
    else
    {
      localObject = null;
    }
    if (localCursor != null) {
      localCursor.close();
    }
    return (com.daamitt.prime.sdk.a.a)localObject;
  }
  
  public final ArrayList<com.daamitt.prime.sdk.a.a> b(int[] paramArrayOfInt)
  {
    ArrayList localArrayList = new ArrayList();
    paramArrayOfInt = a(paramArrayOfInt);
    StringBuilder localStringBuilder = new StringBuilder("accounts.flags & 1 = 0 AND accounts.flags & 2 = 0 AND accounts.enabled = 1 AND accounts.flags & 64  = 0 AND accounts.type IN  (");
    localStringBuilder.append(paramArrayOfInt);
    localStringBuilder.append(")");
    paramArrayOfInt = localStringBuilder.toString();
    localStringBuilder = new StringBuilder("Select accounts.*, count(*) as count from accounts, transactions where accounts._id = transactions.accountId AND ");
    localStringBuilder.append(paramArrayOfInt);
    localStringBuilder.append(" group by accountId order by count desc, type asc, pan asc");
    paramArrayOfInt = localStringBuilder.toString();
    paramArrayOfInt = a.rawQuery(paramArrayOfInt, null);
    if ((paramArrayOfInt != null) && (paramArrayOfInt.getCount() > 0))
    {
      paramArrayOfInt.moveToFirst();
      while (!paramArrayOfInt.isAfterLast())
      {
        localArrayList.add(a(paramArrayOfInt));
        paramArrayOfInt.moveToNext();
      }
    }
    if (paramArrayOfInt != null) {
      paramArrayOfInt.close();
    }
    return localArrayList;
  }
  
  public final String c(long paramLong)
  {
    Object localObject1 = d(paramLong);
    if (!TextUtils.isEmpty((CharSequence)localObject1))
    {
      localObject1 = b((String)localObject1);
      Object localObject2 = new StringBuilder("UUID = '");
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append("'");
      localObject1 = ((StringBuilder)localObject2).toString();
      localObject1 = a.query("accounts", new String[] { "_id" }, (String)localObject1, null, null, null, null);
      if ((localObject1 != null) && (((Cursor)localObject1).getCount() > 0))
      {
        ((Cursor)localObject1).moveToFirst();
        localObject2 = ((Cursor)localObject1).getString(((Cursor)localObject1).getColumnIndexOrThrow("_id"));
        ((Cursor)localObject1).close();
        return (String)localObject2;
      }
      if (localObject1 != null) {
        ((Cursor)localObject1).close();
      }
    }
    return String.valueOf(paramLong);
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.b.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */