package com.daamitt.prime.sdk;

import android.text.TextUtils;
import com.daamitt.prime.sdk.a.b;
import com.daamitt.prime.sdk.a.c;
import com.daamitt.prime.sdk.a.c.a;
import com.daamitt.prime.sdk.a.c.b;
import com.daamitt.prime.sdk.a.d;
import com.daamitt.prime.sdk.a.e;
import com.daamitt.prime.sdk.a.h;
import com.daamitt.prime.sdk.a.k;
import com.daamitt.prime.sdk.a.l;
import com.daamitt.prime.sdk.a.m;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a
{
  private static final String a = "a";
  private static final Pattern b = Pattern.compile("(?i)(?:(?:.*)?(?:(?:[Rr][Ss][.]?)|(?:[Ii][Nn][Rr][.]?)))[ ]?([1-9][0-9,.]*).*");
  private static final Pattern c = Pattern.compile("(?i).*salary.*");
  private static final Pattern d = Pattern.compile("(?i).* sal .*");
  private static final Pattern e = Pattern.compile("(?i).*credit.*");
  private static final Pattern f = Pattern.compile("(?i).*deposit.*");
  private static final Pattern g = Pattern.compile("(?i).*reimb.*");
  private static final Pattern h = Pattern.compile("(?i).*debit.*");
  private static final ArrayList<Pattern> i = new ArrayList(Arrays.asList(new Pattern[] { Pattern.compile("(?i).*reliance[ ]?fresh.*"), Pattern.compile("(?i).*big[ ]?bas.*"), Pattern.compile("(?i).*he[i]?r[i]?tage[ ]?.*"), Pattern.compile("(?i)food[ ]?bazaar"), Pattern.compile("(?i)(.*metro[ ]?cash.*)|(^cash n carry$$)"), Pattern.compile("(?i)(.*nilgiri.*)|(nilagiri[s]?)"), Pattern.compile("(?i).*sar[a]?vana.*"), Pattern.compile("(?i).*grofers.*"), Pattern.compile("(?i)^srs[ ]?val.*"), Pattern.compile("(?i)(.*d[ ]?mart.*)|(d[-]?mart)|(^avenue.*)"), Pattern.compile("(?i)(.*more[ ]?mega.*)|(^more)|(^more[.]?super.*)"), Pattern.compile("(?i).*vishal[ ]?m.*"), Pattern.compile("(?i).*easy[ ]?day.*"), Pattern.compile("(?i)(^spar[ ]hyper.*)|(^spar[ ]?sln.*)"), Pattern.compile("(?i).*star[ ]?baz.*"), Pattern.compile("(?i).*vijetha.*"), Pattern.compile("(?i).*m[.][ ]?k[.]?[ ]?ret.*"), Pattern.compile("(?i)(.*big[ ]?baz[z]?a[a]?r.*)|(^fbb$$)(big[_]?bazar)"), Pattern.compile("(?i).*hyper[ ]?city.*"), Pattern.compile("(?i).*village[ ]?hyper.*"), Pattern.compile("(?i).*haiko.*"), Pattern.compile("(?i).*city[ ]super.*"), Pattern.compile("(?i).*twenty[ ]?four.*"), Pattern.compile("(?i)^grace[ ]sup.*"), Pattern.compile("(?i)^sansar[ ]sup.*"), Pattern.compile("(?i).*sahakari[ ]?bhan.*"), Pattern.compile("(?i).*food[ ]?wor.*"), Pattern.compile("(?i)^show[ ]?off.*"), Pattern.compile("(?i)^grace[ ]sup.*"), Pattern.compile("(?i)^zopnow$$"), Pattern.compile("(?i)swaraj.*mark"), Pattern.compile("(?i)^green city.*super"), Pattern.compile("(?i)m[ .]?k[ .]?ahmed"), Pattern.compile("(?i)royal.*mart") }));
  private static final ArrayList<Pattern> j = new ArrayList(Arrays.asList(new Pattern[] { Pattern.compile("(?i)(^uber$$)|((^uber [^cafe].*)&(^uber [^lounge].*))|(.*^uber\\d.*)"), Pattern.compile("(?i)(^(?:www.)?ola[ ]?(?:cabs)?.*)|(.*ani[ ]?tech.*)"), Pattern.compile("(?i)(.*indian rail.*)|(.*irct[c]?.*)"), Pattern.compile("(?i)(.*red[ ]?bus.*)"), Pattern.compile("(?i)(.*make[ ]?my.*)"), Pattern.compile("(?i)(.*goibi.*)|(.*ibibo.*)"), Pattern.compile("(?i)(.*ksrtc.*)|(karnataka state road)"), Pattern.compile("(?i)(.*abhi[ ]?bus.*)"), Pattern.compile("(?i)(.*apsrtc.*)"), Pattern.compile("(?i)(.*indigo.*)"), Pattern.compile("(?i)(.*airbnb.*)"), Pattern.compile("(?i)(.*jet[ ]?air.*)"), Pattern.compile("(?i)(.*msrtc.*)"), Pattern.compile("(?i)(tnstcltd)"), Pattern.compile("(?i)(.*travelyaari.*)"), Pattern.compile("(?i)(.*irc[ ]?logistics.*)|(^irc$$)"), Pattern.compile("(?i)(.*dhanunjaya.*)"), Pattern.compile("(?i).*spicejet.*"), Pattern.compile("(?i).*yatra.*") }));
  private static final ArrayList<Pattern> k = new ArrayList(Arrays.asList(new Pattern[] { Pattern.compile("(?i)(amazon attire)"), Pattern.compile("(?i)(.*amazon.*)|(^amaz$$)"), Pattern.compile("(?i)(men[']?s aven.*)"), Pattern.compile("(?i)(.*pantaloons.*)"), Pattern.compile("(?i)(.*mark[s]?[ ]?spen.*)|(.*spencer[s]?)|(.*mark[s]?[ ]?and[ ]?.*)"), Pattern.compile("(?i)(.*life[ ]?style.*)"), Pattern.compile("(?i)(.*bata.*)"), Pattern.compile("(?i)(.*shopper.*)"), Pattern.compile("(?i)(.*westside.*)"), Pattern.compile("(?i)(.*croma.*)"), Pattern.compile("(?i)(.*brand[ ]fac.*)"), Pattern.compile("(?i)(.*central.*)"), Pattern.compile("(?i)(.*amway.*)"), Pattern.compile("(?i)(.*seasons.*)"), Pattern.compile("(?i)(.*health[ ]?and[ ]?gl.*)"), Pattern.compile("(?i)(.*snap[ ]?deal.*)"), Pattern.compile("(?i)(.*myntra.*)"), Pattern.compile("(?i)(.*ebay.*)"), Pattern.compile("(?i)(.*apple[ ]?itune.*)"), Pattern.compile("(?i)(.*jabong.*)"), Pattern.compile("(?i)(.*shop[ ]?cl.*)"), Pattern.compile("(?i)(.*ali[ ]?exp.*)"), Pattern.compile("(?i)(^easy[ ]?mobile.*)"), Pattern.compile("(?i)(^fab[ ]?india.*)"), Pattern.compile("(?i)(^aiswarya$$)"), Pattern.compile("(?i)(^max$$)|(.*max[ ]?hyper.*)"), Pattern.compile("(?i)(.*max[ ]?shoppee.*)"), Pattern.compile("(?i)(.*titan.*)"), Pattern.compile("(?i)(^zara$$)"), Pattern.compile("(?i)(.*windmills[ ]?craftworks.*)"), Pattern.compile("(?i)(.*bionic[ ]nat.*)"), Pattern.compile("(?i)(.*raymond[s]?.*)"), Pattern.compile("(?i)(.*herbalife.*)"), Pattern.compile("(?i)(^jockey.*)"), Pattern.compile("(?i)(^blackberry[s]?.*)"), Pattern.compile("(?i)(^mochi$$)"), Pattern.compile("(?i)(^soch$$)"), Pattern.compile("(?i)(.*first[ ]?cry.*)"), Pattern.compile("(?i)(^khadims$$)"), Pattern.compile("(?i)(^poorvika[ ]?mob.*)"), Pattern.compile("(?i)(^levi[']?s.*)"), Pattern.compile("(?i)(^forever[ ]?21.*)"), Pattern.compile("(?i)(.*jaihind.*)"), Pattern.compile("(?i)(.*masoom[ ]?play.*)"), Pattern.compile("(?i)(.*decathlon[ ]?[s]?.*)"), Pattern.compile("(?i)(.*madura[ ]?.*)"), Pattern.compile("(?i)(^show[ ]?off.*)"), Pattern.compile("(?i)(.*future[ ]?life.*)"), Pattern.compile("(?i)(^lulu[ ]?.*)"), Pattern.compile("(?i)(^woodland$$)"), Pattern.compile("(?i)(.*vijay[ ]?sal.*)"), Pattern.compile("(?i)(.*body[ ]?shop.*)"), Pattern.compile("(?i)(.*grt.*)"), Pattern.compile("(?i)(.*india[ ]?idea.*)"), Pattern.compile("(?i)(.*sherali.*)"), Pattern.compile("(?i)(.*globus.*)"), Pattern.compile("(?i)(^lakme[ ]?.*)"), Pattern.compile("(?i)(^smartbuy.*)"), Pattern.compile("(?i)(.*chennai shopping mall.*)"), Pattern.compile("(?i)(.*flipk.*)|(^ws ret.*)"), Pattern.compile("(?i)(.*reliance[ ]?trends.*)|(^reliance[ ]ma.*)|(.*reliance[_]?foo.*)|(.*reliance[ ]foot.*)"), Pattern.compile("(?i)(.*apple[ ]?itune.*)"), Pattern.compile("(?i)(^aiswarya$$)"), Pattern.compile("(?i)(.*pothys.*)") }));
  private static final ArrayList<Pattern> l = new ArrayList(Arrays.asList(new Pattern[] { Pattern.compile("(?i)(.*juice[ ]?square.*)"), Pattern.compile("(?i)(^via$$)|(via[ ]?south)|(via[ ]?milano)"), Pattern.compile("(?i)(.*facebake.*)"), Pattern.compile("(?i)(^cbtl.*)"), Pattern.compile("(?i)(^tea$$)"), Pattern.compile("(?i)(^hazel[ ]?nut.*)"), Pattern.compile("(?i)(^baskin[ ]?rob.*)"), Pattern.compile("(?i)(^foodexo.*)"), Pattern.compile("(?i)(.*sodexo.*)"), Pattern.compile("(?i)(.*freshmenu.*)"), Pattern.compile("(?i)(^chilis$$)"), Pattern.compile("(?i)(.*house[ ]?of[ ]?spi.*)"), Pattern.compile("(?i)(.*mdp[ ]?cof.*)"), Pattern.compile("(?i)(.*cream[ ]?stone.*)"), Pattern.compile("(?i)(.*m[a]?c[ ]*donald.*)|(.*hard[ ]?castle.*)"), Pattern.compile("(?i)mcdeliv"), Pattern.compile("(?i)(.*cafe[ ]*coffee.*)|([ ]*ccd[^a-z].*)"), Pattern.compile("(?i)( PHD )|(.*pizza[ ]?hut[ ]?delivery.*)"), Pattern.compile("(?i)(.*pizza[ ]?hut.*)"), Pattern.compile("(?i)smokin.*joe"), Pattern.compile("(?i)cali.*pizza"), Pattern.compile("(?i)cali.*burrito"), Pattern.compile("(?i)ibaco"), Pattern.compile("(?i)just.*bake"), Pattern.compile("(?i)arbor[ ]?brew"), Pattern.compile("(?i)toit"), Pattern.compile("(?i)madhuloka"), Pattern.compile("(?i)^lake.*forest"), Pattern.compile("(?i)(.*burger[ ]?king)"), Pattern.compile("(?i)^anand sweet"), Pattern.compile("(?i)(.*domino.*)|(jubilant[ ]*fo)"), Pattern.compile("(?i)(haldiram)"), Pattern.compile("(?i)(bikaner[vw]a[a]?la)"), Pattern.compile("(?i)(rajdhani)"), Pattern.compile("(?i)k[ ]?c[ ]?s[ ]?(snack.*)?"), Pattern.compile("(?i)karachi[ ]?((bak.*)|(sweet.*)).*"), Pattern.compile("(?i)(star[ ]?buck)|(tata starbu)"), Pattern.compile("(?i)(sub[ ]?way)"), Pattern.compile("(?i)food[ ]?pan"), Pattern.compile("(?i)box8"), Pattern.compile("(?i)just[ ]?eat"), Pattern.compile("(?i)(.*zomato.*)"), Pattern.compile("(?i)(kfc)|(kentucky)"), Pattern.compile("(?i)([kc]rispy.*[kc]re[am][me])"), Pattern.compile("(?i)(barbeq.*nation)|(bbq.*nation)"), Pattern.compile("(?i)(main.*china)"), Pattern.compile("(?i)(faaso[']?s)|(fasso[']?s)"), Pattern.compile("(?i)(tiny[ ]?owl)"), Pattern.compile("(?i)(swiggy)|(bundl)"), Pattern.compile("(?i)(costa)"), Pattern.compile("(?i)(dunkin)"), Pattern.compile("(?i)(barista)|(lavazza)"), Pattern.compile("(?i)ice cap"), Pattern.compile("(?i)(sar[a]?van[a]?.*bha[vw]an)"), Pattern.compile("(?i)kant[h]?i.*sweet"), Pattern.compile("(?i)(tamanna)|(hotel taman)"), Pattern.compile("(?i)au bon pain"), Pattern.compile("(?i)^om sweet"), Pattern.compile("(?i)gud dhani"), Pattern.compile("(?i)hamza (ba.*)?"), Pattern.compile("(?i)hotel green city"), Pattern.compile("(?i)srm catering"), Pattern.compile("(?i)bar stock"), Pattern.compile("(?i)irish house"), Pattern.compile("(?i)t[ .]?g[ .]?i[ .]*f"), Pattern.compile("(?i)cha[a]?yos"), Pattern.compile("(?i)sai.*dosa"), Pattern.compile("(?i)theobroma"), Pattern.compile("(?i)chaturvedis"), Pattern.compile("(?i)kesar[']?[s]?sweet"), Pattern.compile("(?i)momoe vas"), Pattern.compile("(?i)anjappar.*chettinad"), Pattern.compile("(?i)benjarong"), Pattern.compile("(?i)meghana foo"), Pattern.compile("(?i)meghana.*spic"), Pattern.compile("(?i)bindras"), Pattern.compile("(?i)kovai.*pa.*am"), Pattern.compile("(?i)bindra reso"), Pattern.compile("(?i)(mad.*over.*do)|(m[.]o[.]d)"), Pattern.compile("(?i)vassi.*palaz"), Pattern.compile("(?i)tiswa"), Pattern.compile("(?i)chutneys"), Pattern.compile("(?i)cbtl"), Pattern.compile("(?i)^residency fresh"), Pattern.compile("(?i)swaraj bar"), Pattern.compile("(?i)drops[ ]?total"), Pattern.compile("(?i)^balaji wine"), Pattern.compile("(?i)sai balaji wine"), Pattern.compile("(?i)sr(i|e)[e]? balaji wine"), Pattern.compile("(?i)^(the )?beer.*cafe"), Pattern.compile("(?i)discovery[ ]?wine"), Pattern.compile("(?i)new paradise rest"), Pattern.compile("(?i)(^thalappakat$$)"), Pattern.compile("(?i)(dind.*thalappakat(ti)?$$)"), Pattern.compile("(?i)(^chil[l]?i[e]?[']?[s]?[ ]*$$)"), Pattern.compile("(?i)nkp.*empire"), Pattern.compile("(?i)beijing[ ]?bit"), Pattern.compile("(?i)plaza.*pre"), Pattern.compile("(?i)mast.*kal"), Pattern.compile("(?i)sri.*krishna.*sagar"), Pattern.compile("(?i)(^sangeetha( veg.*)$$)"), Pattern.compile("(?i)moriz"), Pattern.compile("(?i)nagarjuna"), Pattern.compile("(?i)sagar ratna"), Pattern.compile("(?i)^inchara.*((fam.*)|(gard.*)|(rest.*))"), Pattern.compile("(?i)currys.*crunch"), Pattern.compile("(?i)nut.*n.*spic"), Pattern.compile("(?i)citrus cafe"), Pattern.compile("(?i)corner.*hou"), Pattern.compile("(?i)ubiquitous"), Pattern.compile("(?i)papa.*jo[h]n"), Pattern.compile("(?i)nagarjuna.*chim"), Pattern.compile("(?i).*chefs corner.*"), Pattern.compile("(?i).*hungerbox.*") }));
  private static final ArrayList<Pattern> m = new ArrayList(Arrays.asList(new Pattern[] { Pattern.compile("(?i).*(EMI|emi) (of|for) Rs(.|)([\\d,.]*).*[Aa]\\/[Cc] (\\S*).*on ([0-9A-Za-z-:\\/]*)."), Pattern.compile("(?i)Your EMI Conversion request is processed"), Pattern.compile("(?i).*(Loan|loan) [Aa]\\/[Cc] (\\S*).*EMI will Rs. ([\\d,.]*).*"), Pattern.compile("(?i)You have opted for EMI facility.*"), Pattern.compile("(?i).*NETSECURE code.*"), Pattern.compile("(?i).*EMI conversion.*successfully.*"), Pattern.compile("(?i).*(has bounced|did not clear|is due|is in overdue| in arrears|insufficient funds).*"), Pattern.compile("(?i).*[Pp]ersonal loan of Rs\\.* ([\\d,.]*).*EMI Rs\\.* ([\\d,.]*).*is credited.*"), Pattern.compile("(?i).*we have foreclosed.*loan of Rs\\.*\\ *([\\d,.]*).*") }));
  private static final ArrayList<Pattern> n = new ArrayList(Arrays.asList(new Pattern[] { Pattern.compile("(?i).*revised T&C .* (loan|LOAN|Loan) & (EMI|emi) (Cards|cards)"), Pattern.compile("(?i).*(EMI|emi) (Card|card) holder"), Pattern.compile("(?i).*EMI of Rs.([\\d,.]*).*Loan#(\\S*) is due on ([0-9A-za-z-:\\/]*)."), Pattern.compile("(?i).*Total loan amt on your Bajaj EMI Card is upgraded to.*"), Pattern.compile("(?i).*[Yy]our BFL CD loan has been booked.*No. (\\S*) for Rs. ([\\d,.]*).*(EMI|emi) (of|for) Rs. ([\\d,.]*).*due on ([0-9A-za-z-:\\/]*)."), Pattern.compile("(?i)You are eligible for an EMI card with limit of Rs. ([\\d,.]*)"), Pattern.compile("(?i)Now swipe your EMI card.*"), Pattern.compile("(?i).*your (EMI|emi) (Card|card) available loan amount is Rs.([\\d,.]*).*"), Pattern.compile("(?i).*[Yy]our (EMI|emi) (card|Card) (number|no) (\\S*) has been delivered.*"), Pattern.compile("(?i).*[Aa]vailable loan (Amt|amount).*(EMI|emi) (Card|card) ending (with)* (\\S*).*is Rs\\.* ([\\d,.]*)"), Pattern.compile("(?i).*[Aa]vailable (Loan|loan) (Amt|Amount|amount).*(EMI|emi) (Card|card) ending (\\d*).*Rs. ([\\d,.]*).*"), Pattern.compile("(?i).*BFL CD loan has been booked.*[Nn]o\\.*(\\S*) for Rs. ([\\d,.]*).* EMI for Rs. ([\\d,.]*).*due on ([0-9A-za-z-:\\/]*)"), Pattern.compile("(?i).*pay your (EMI|emi) (ONLINE|online).*loan (account|acct) (no|no\\.|number) is (\\S*).*"), Pattern.compile("(?i).*financed for.*ending (\\S*) is Rs.([\\d,.]*).*(EMI|emi) of Rs.([\\d,.]*).*"), Pattern.compile("(?i).*loan (amt|amount) on (ur|your).*(EMI|emi) (Card|card) upgraded to Rs\\.* ([\\d,.]*).*"), Pattern.compile("(?i).*4 digit PIN.* (EMI|emi) card number (\\S*).*"), Pattern.compile("(?i).*(EMI|emi) (Card|card) (number|No|no)\\.* (\\S*) is now unblocked."), Pattern.compile("(?i).*BFL (EMI|emi) (Card|card).*(loan|Loan) (amount|amt) on (your|ur) (Card|card) is Rs([\\d,.]*).*"), Pattern.compile("(?i).*Transaction for Rs\\.*([\\d,.]*) on your (EMI|emi) (Card|card) (\\S*) is successful."), Pattern.compile("(?i).*(EMI|emi) (Card|card) is upgraded to (Rs|INR|inr)\\.* ([\\d,.]*).*"), Pattern.compile("(?i).*entered wrong pin .*"), Pattern.compile("(?i).*( suspended | outstanding | bounced |did not clear|overdue| in arrears|insufficient funds|non receipt).*") }));
  private static final ArrayList<Pattern> o = new ArrayList(Arrays.asList(new Pattern[] { Pattern.compile("(?i)EMI due on([0-9A-za-z-:\\/]*) in a\\/c ([A-Za-z0-9]*)"), Pattern.compile("(?i)\\D* EMI on your account ([A-Za-z0-9]*) revised"), Pattern.compile("(?i).* [Ff]lexipay [Ss]ervice [Rr]equest .* has been processed.*"), Pattern.compile("(?i).*Flexipay.*will be processed within.*"), Pattern.compile("(?i).*Balance Transfer\\D*([\\d,.]+).*EMI\\D*([\\d,.]+).*processed within.*"), Pattern.compile("(?i).*Balance Transfer\\D*([\\d,.]+).*EMI\\D*([\\d,.]+).*processed successfully.*"), Pattern.compile("(?i).*request for Encash.*processed within.*"), Pattern.compile("(?i).*Encash.*processed successfully.*"), Pattern.compile("(?i).*Flexipay.*been booked.*"), Pattern.compile("(?i).*(not recd|over credit limit|unpaid|has bounced|did not clear|overdue| in arrears|insufficient funds|cancelled).*") }));
  private static final ArrayList<Pattern> p = new ArrayList(Arrays.asList(new Pattern[] { Pattern.compile("(?i)Payment of Rs.([\\d,.]*).* (Loan|loan|LOAN) ([A-Za-z0-9]*) on (\\w{3} \\d{2} \\d{4})."), Pattern.compile("(?i)Your (EMI|emi) (of|for).*(Loan|loan) ([A-Za-z0-9]*) is due on (\\d+\\D+ \\w{3}\\'\\d{2})"), Pattern.compile("(?i).*loan details are as follows- Amount (INR|inr|Rs|Rs.) ([\\d,.]*).*EMI (INR|inr|Rs|Rs.) ([\\d,.]*)"), Pattern.compile("(?i).*(over credit limit|unpaid|has bounced|did not clear|overdue| in arrears|insufficient funds).*") }));
  private static final ArrayList<Pattern> q = new ArrayList(Arrays.asList(new Pattern[] { Pattern.compile("(?i)Dear Customer,( )*Gentle reminder that your EMI of (Rs\\D*|INR\\D*|inr\\D*)([\\d,.]*)\\D*loan a\\/c no. (\\S*) is due on ([0-9A-Za-z-:\\/]*)."), Pattern.compile("(?i)Dear Customer,( )*Gentle reminder\\D*([\\d,.]*)\\D*a\\/c( )*no.(\\S*) is due on ([0-9A-Za-z-:\\/]*)."), Pattern.compile("(?i)Dear Customer, the EMI on your HDFC Loan acct no. (\\S*) will be debited from your nominated acct. for a value of (Rs.|INR|inr)([\\d,.]*) on ([0-9A-Za-z-:\\/]*)"), Pattern.compile("(?i)Dear Customer, First EMI due on ([0-9A-za-z-:\\/]*) is (Rs|INR|inr) ([\\d,.]*) on HDFC Bank loan (\\S*) \\D*"), Pattern.compile("(?i)Alert from HDFC Bank. Next EMI of (Rs.|INR|inr)([\\d,.]*) on your loan account: (\\S*) due on ([0-9A-za-z-:\\/]*).\\D*"), Pattern.compile("(?i)Dear Customer. Greetings from HDFC Bank. We wish to remind you that an EMI of (Rs|INR|inr) ([\\d,.]*)\\/- on your HDFC loan A\\/c (\\S*) falls due on the ([0-9A-za-z-:\\/]*)."), Pattern.compile("(?i)Dear HDFC Bank Cardmember, (\\d|\\D)* is converted to EMI"), Pattern.compile("(?i)Dear Sir, Loan application of (\\D*) of (\\D* (loan|LOAN)),(\\d|\\D)* is Disbursed"), Pattern.compile("(?i)Dear Customer, First EMI due on ([0-9A-Za-z-:\\/]*) is Rs ([\\d,.]*) \\D*(\\S*)"), Pattern.compile("(?i)(HDFC|hdfc) (EMI|emi) alert \\D*loan ac no (\\S*)"), Pattern.compile("(?i)Dear Customer, Greetings from (HDFC|hdfc) (BANK|bank). Timely \\D*Rs.([\\d,.]*)\\D*No: (\\S*) is due on ([0-9A-Za-z-:\\/]*)"), Pattern.compile("(?i)\\D*, Txn request on \\S* for Rs.([0-9,.]*)\\D* is converted to (EMI|emi)"), Pattern.compile("(?i)Dear Customer, ride home your dream bike with an HDFC Bank Two Wheeler Loan."), Pattern.compile("(?i)Your Loan account (\\S*) is now in much better health."), Pattern.compile("(?i)Get Insta.*"), Pattern.compile("(?i)Convert.* EMI.*"), Pattern.compile("(?i)Get Jumbo Loan of Rs\\D*([\\d,.]+).*"), Pattern.compile("(?i)Pre-approved Insta Loan\\D*Rs\\D*([\\d,.]+).*"), Pattern.compile("(?i).*Pre-approved EMI Gold Loan.*"), Pattern.compile("(?i).*Insta Loan upto Rs\\.*\\D*([\\d,.]+).*"), Pattern.compile("(?i).*(over credit limit|unpaid|has bounced|did not clear|overdue| in arrears|insufficient funds).*") }));
  private static final ArrayList<Pattern> r = new ArrayList(Arrays.asList(new Pattern[] { Pattern.compile("(?i)Dear Customer, EMI of Rs. ([\\d,.]*) (\\d|\\D)* Loan (\\S*) is due on ([0-9A-za-z-:\\/]*)."), Pattern.compile("(?i).*EMI (for|of) Rs.( |)([\\d,.]*).* Loan (Acct|Account)( |)(\\S*).*on ([0-9A-za-z-:\\/]*)"), Pattern.compile("(?i).*credit card.*"), Pattern.compile("(?i).*(over credit limit|unpaid|has bounced|did not clear|overdue| in arrears|insufficient funds|cancelled).*") }));
  private static final ArrayList<Pattern> s = new ArrayList(Arrays.asList(new Pattern[] { Pattern.compile("(?i).*bounced.*"), Pattern.compile("(?i).*cheque.*") }));
  private static final ArrayList<Pattern> t = new ArrayList(Arrays.asList(new Pattern[] { Pattern.compile("(?i).*premium.*"), Pattern.compile("(?i).*insurance.*") }));
  private static final ArrayList<Pattern> u = new ArrayList(Arrays.asList(new Pattern[] { Pattern.compile("(?i).* get .*insurance.*to (enrol|subscribe).*"), Pattern.compile("(?i)sms (pmsby|pmjjby|muthoot|family).*"), Pattern.compile("(?i).*dear customer, pay your.*insurance.*just a few clicks.*"), Pattern.compile("(?i).*get.*insurance.*save tax.*"), Pattern.compile("(?i).*your.*(has been|is) credited.*"), Pattern.compile("(?i)Dear Customer, due premiums? No more a hassle.*"), Pattern.compile("(?i).*get.*insurance.*save up.*"), Pattern.compile("(?i).*regular use of Axis Bank Debit Card.*"), Pattern.compile("(?i).*get.*insurance.*to know.*"), Pattern.compile("(?i).*bajaj.*brin?gs you (tax saving|health insurance).*") }));
  private static final ArrayList<Pattern> v = new ArrayList(Arrays.asList(new Pattern[] { Pattern.compile("(?i)Tired of tracking.*"), Pattern.compile("(?i)1082.*"), Pattern.compile("(?i).*StanChart credit card.*"), Pattern.compile("(?i).*To enroll, SMS.*"), Pattern.compile("(?i).*Your premium is due.*"), Pattern.compile("(?i).*Thank you for your payment.*"), Pattern.compile("(?i).*due for renewal.*"), Pattern.compile("(?i).*pay premium at your.*"), Pattern.compile("(?i).*premium payment for your policy.*"), Pattern.compile("(?i).*premium was due for policy.*"), Pattern.compile("(?i).*renewal premium payment for your.*"), Pattern.compile("(?i).*revival of.*policy.*"), Pattern.compile("(?i).*Premium for.*policy.*due.*"), Pattern.compile("(?i).*pay your.*policy.*due premium using.*"), Pattern.compile("(?i).*kindly pay your renewal.*"), Pattern.compile("(?i).*pay your renewal premium of.*"), Pattern.compile("(?i).*pay your premium before.*"), Pattern.compile("(?i).*dear policyholder.*"), Pattern.compile("(?i).*dear customer.*"), Pattern.compile("(?i).*You can now pay the premium.*policy.*"), Pattern.compile("(?i).*Your a/c no..*registered for.*with.*"), Pattern.compile("(?i).*insurance expiring on.*"), Pattern.compile("(?i).*Your premium of Rs..*"), Pattern.compile("(?i).*Enhance your life insurance.*"), Pattern.compile("(?i).*health insurance.*due for.*enewal.*"), Pattern.compile("(?i).*insurance will lapse due.*"), Pattern.compile("(?i).*insurance.*is due in.*"), Pattern.compile("(?i).*your.*insurance policy.*due on.*"), Pattern.compile("(?i).*insurance premium has been debited.*"), Pattern.compile("(?i)dear.*customer.*your.*insurance.*renewed.*"), Pattern.compile("(?i).*your health.*policy.*expires.*"), Pattern.compile("(?i).*towards your.*insurance policy.*"), Pattern.compile("(?i).*insurance.*expiring soon.*"), Pattern.compile("(?i).*insurance is due on.*") }));
  
  private static c.a a(JSONObject paramJSONObject, Matcher paramMatcher)
  {
    String str1 = paramJSONObject.optString("parent_field");
    JSONObject localJSONObject = paramJSONObject.optJSONObject("child_field");
    int i2 = 0;
    boolean bool1 = paramJSONObject.optBoolean("deleted", false);
    boolean bool2 = paramJSONObject.optBoolean("incomplete", false);
    String str2 = paramJSONObject.optString("match_type");
    boolean bool3 = TextUtils.equals(str2, "delta");
    long l1 = -1L;
    if (bool3) {
      l1 = paramJSONObject.optLong("match_value", -1L);
    }
    int i1;
    if (localJSONObject != null)
    {
      i1 = localJSONObject.optInt("group_id", -1);
      if (i1 >= 0)
      {
        paramJSONObject = c(str1, paramMatcher.group(i1));
      }
      else
      {
        paramMatcher = localJSONObject.optString("field", "Unknown");
        paramJSONObject = paramMatcher;
        if (TextUtils.equals(paramMatcher, "Unknown")) {
          paramJSONObject = localJSONObject.optString("value", "Unknown");
        }
      }
    }
    else
    {
      paramJSONObject = paramJSONObject.optString("child_field");
    }
    if (!TextUtils.equals(str1, "deleted"))
    {
      i1 = i2;
      if (!TextUtils.equals(str1, "pattern_UID")) {}
    }
    else if (TextUtils.equals(str2, "exact"))
    {
      i1 = 2;
    }
    else if (TextUtils.equals(str2, "contains"))
    {
      i1 = 1;
    }
    else
    {
      i1 = i2;
      if (TextUtils.equals(str2, "none")) {
        i1 = 3;
      }
    }
    paramMatcher = new c.a();
    a = str1;
    b = paramJSONObject;
    f = bool1;
    g = bool2;
    c = str2;
    d = i1;
    e = l1;
    return paramMatcher;
  }
  
  private static c.b a(JSONObject paramJSONObject)
  {
    c.b localb = new c.b();
    int i2 = 0;
    for (;;)
    {
      try
      {
        localObject = paramJSONObject.getJSONArray("parent_override");
        if (localObject != null)
        {
          i1 = 0;
          if (i1 < ((JSONArray)localObject).length())
          {
            c.a locala = a(((JSONArray)localObject).getJSONObject(i1), null);
            if (a == null) {
              a = new ArrayList();
            }
            a.add(locala);
            i1 += 1;
            continue;
            e.b();
          }
        }
      }
      catch (JSONException localJSONException)
      {
        Object localObject;
        int i1;
        continue;
      }
      try
      {
        paramJSONObject = paramJSONObject.getJSONArray("child_override");
        if (paramJSONObject != null)
        {
          i1 = i2;
          if (i1 < paramJSONObject.length())
          {
            localObject = a(paramJSONObject.getJSONObject(i1), null);
            if (b == null) {
              b = new ArrayList();
            }
            b.add(localObject);
            i1 += 1;
            continue;
            e.b();
          }
        }
      }
      catch (JSONException paramJSONObject) {}
    }
    return localb;
  }
  
  private static k a(String paramString1, String paramString2, String paramString3, Date paramDate)
  {
    paramString2 = new k(paramString2, paramString3, paramDate);
    paramString2.a(paramString1, "Messages");
    v = 99;
    return paramString2;
  }
  
  private static m a(Matcher paramMatcher, h paramh, String paramString1, String paramString2, Date paramDate)
  {
    try
    {
      localm = new m(paramString1, paramString2, paramDate);
      E = true;
      v = com.daamitt.prime.sdk.a.a.a(c);
      localm.a(e, "Spends");
      B = f;
      localJSONObject = g;
      if (localJSONObject.has("set_txn_as_expense"))
      {
        if (localJSONObject.optBoolean("set_txn_as_expense", true)) {
          localm.n();
        } else {
          localm.m();
        }
        ac = true;
      }
      else
      {
        localm.n();
      }
      L = i;
      paramString1 = localJSONObject.optJSONObject("pos");
      if (paramString1 == null) {
        break label2283;
      }
      if (paramString1.optBoolean("set_no_pos", false)) {
        localm.j();
      }
      i1 = paramString1.optInt("group_id", -1);
      if (i1 >= 0)
      {
        paramh = paramMatcher.group(i1);
      }
      else
      {
        paramh = paramString1.optJSONArray("group_ids");
        if (paramh != null)
        {
          paramString1 = new StringBuilder();
          i1 = 0;
          while (i1 < paramh.length())
          {
            if (i1 > 0) {
              paramString1.append(" - ");
            }
            paramString1.append(paramMatcher.group(paramh.getInt(i1)));
            i1 += 1;
          }
          paramh = paramString1.toString();
        }
        else
        {
          paramh = paramString1.optString("value");
          localm.j();
        }
      }
    }
    catch (JSONException paramMatcher)
    {
      m localm;
      JSONObject localJSONObject;
      Object localObject4;
      String str1;
      Object localObject2;
      Object localObject1;
      String str2;
      paramMatcher.printStackTrace();
      return null;
    }
    catch (NumberFormatException paramMatcher)
    {
      for (;;)
      {
        label936:
        label1732:
        continue;
        label2283:
        paramh = null;
        continue;
        label2288:
        i1 += 1;
        continue;
        paramString1 = paramh;
        continue;
        label2302:
        h localh = paramh;
        continue;
        label2308:
        Object localObject3 = null;
        continue;
        label2314:
        continue;
        label2317:
        paramh = null;
        continue;
        label2322:
        localObject3 = paramDate;
        continue;
        label2329:
        paramh = null;
        continue;
        label2334:
        int i1 = 0;
        continue;
        label2340:
        label2343:
        i1 = 0;
        continue;
        label2349:
        i1 = 0;
      }
    }
    paramString1 = paramh;
    if (TextUtils.isEmpty(paramh))
    {
      if (localm.k()) {
        e.b();
      }
      paramString1 = "Unknown";
      localm.j();
    }
    paramh = d(paramString1);
    paramString1 = localJSONObject.optString("transaction_type", null);
    localObject4 = paramh;
    str1 = paramString1;
    if (paramString1 == null)
    {
      localObject2 = localJSONObject.getJSONObject("transaction_type_rule");
      i1 = ((JSONObject)localObject2).getInt("group_id");
      localObject4 = paramh;
      str1 = paramString1;
      if (i1 >= 0)
      {
        localObject1 = paramMatcher.group(i1).trim().toLowerCase();
        localObject2 = ((JSONObject)localObject2).getJSONArray("rules");
        i1 = 0;
        localObject4 = paramh;
        str1 = paramString1;
        if (i1 < ((JSONArray)localObject2).length())
        {
          localObject3 = ((JSONArray)localObject2).getJSONObject(i1);
          if (!((String)localObject1).contains(((JSONObject)localObject3).getString("value"))) {
            break label2288;
          }
          paramString1 = ((JSONObject)localObject3).getString("txn_type");
          localObject1 = ((JSONObject)localObject3).optString("acc_type_override");
          if (((JSONObject)localObject3).optBoolean("set_no_pos", false)) {
            localm.j();
          }
          if (!((String)localObject1).isEmpty()) {
            v = com.daamitt.prime.sdk.a.a.a((String)localObject1);
          }
          localObject1 = ((JSONObject)localObject3).optString("pos_override");
          localObject4 = paramh;
          str1 = paramString1;
          if (!((String)localObject1).isEmpty())
          {
            localm.j();
            localObject4 = localObject1;
            str1 = paramString1;
          }
        }
      }
    }
    if (v == 9999)
    {
      e.b();
      return null;
    }
    paramh = localJSONObject.optJSONObject("pan");
    if (paramh != null)
    {
      i1 = paramh.optInt("group_id", -1);
      if (i1 >= 0)
      {
        str2 = paramMatcher.group(i1);
      }
      else
      {
        str2 = paramh.optString("value", "Unknown");
        Y |= 0x2;
      }
      localObject2 = localJSONObject.optJSONObject("account_balance");
      paramh = Double.valueOf(Double.MIN_VALUE);
      if (localObject2 != null)
      {
        i1 = ((JSONObject)localObject2).optInt("group_id", -1);
        if (i1 < 0) {}
      }
    }
    else
    {
      try
      {
        localObject1 = Double.valueOf(paramMatcher.group(i1).replace(",", ""));
        paramString1 = (String)localObject1;
        paramh = (h)localObject1;
        if (((JSONObject)localObject2).optString("txn_direction", "").equalsIgnoreCase("incoming"))
        {
          paramh = (h)localObject1;
          d1 = -((Double)localObject1).doubleValue();
          paramString1 = Double.valueOf(d1);
        }
      }
      catch (NumberFormatException paramString1)
      {
        double d1;
        for (;;) {}
      }
      e.d();
      paramString1 = paramh;
      localObject2 = localJSONObject.optJSONObject("outstanding_balance");
      paramh = Double.valueOf(Double.MIN_VALUE);
      if (localObject2 == null) {
        break label2302;
      }
      i1 = ((JSONObject)localObject2).optInt("group_id", -1);
      if (i1 < 0) {
        break label2302;
      }
      for (;;)
      {
        try
        {
          localObject1 = Double.valueOf(paramMatcher.group(i1).replace(",", ""));
          paramh = (h)localObject1;
          bool = ((JSONObject)localObject2).optString("txn_direction", "").equalsIgnoreCase("incoming");
          paramh = (h)localObject1;
          if (bool) {
            paramh = (h)localObject1;
          }
        }
        catch (NumberFormatException localNumberFormatException)
        {
          boolean bool;
          Object localObject6;
          Object localObject5;
          continue;
        }
        try
        {
          d1 = -((Double)localObject1).doubleValue();
          paramh = Double.valueOf(d1);
          localObject1 = paramh;
        }
        catch (JSONException paramMatcher) {}
        try
        {
          e.d();
          localObject1 = paramh;
          localObject3 = localJSONObject.optJSONObject("amount");
          localObject2 = Double.valueOf(0.0D);
          if (localObject3 != null)
          {
            i1 = ((JSONObject)localObject3).optInt("group_id", -1);
            if (i1 < 0) {
              continue;
            }
          }
        }
        catch (NumberFormatException paramMatcher)
        {
          continue;
        }
        try
        {
          paramh = Double.valueOf(paramMatcher.group(i1).replace(",", ""));
        }
        catch (NumberFormatException paramh) {}
      }
      e.d();
      paramh = Double.valueOf(0.0D);
      localObject2 = paramh;
      if (((JSONObject)localObject3).optString("txn_direction", "").equalsIgnoreCase("incoming"))
      {
        localObject2 = Double.valueOf(-paramh.doubleValue());
        break label936;
        if (m.a(str1) != 12) {
          return null;
        }
      }
      paramh = localJSONObject.optJSONObject("date");
      if ((paramh == null) || (paramh.optBoolean("use_sms_time", false))) {
        break label2322;
      }
      i1 = paramh.optInt("group_id", -1);
      if (i1 < 0) {
        break label2308;
      }
      localObject3 = paramMatcher.group(i1);
      localObject6 = paramh.optJSONArray("formats");
      if (localObject6 == null) {
        break label2317;
      }
      i1 = 0;
      paramh = null;
      localObject5 = localObject3;
      localObject3 = localObject6;
      for (;;)
      {
        if (i1 >= ((JSONArray)localObject3).length()) {
          break label2314;
        }
        localObject6 = ((JSONArray)localObject3).getJSONObject(i1);
        if (((JSONObject)localObject6).optBoolean("use_sms_time", false))
        {
          paramh = paramDate;
          break;
        }
        localObject6 = ((JSONObject)localObject6).optString("format");
        for (;;)
        {
          try
          {
            localObject6 = b((String)localObject5, (String)localObject6);
          }
          catch (ParseException localParseException)
          {
            int i2;
            continue;
          }
          try
          {
            if (((Date)localObject6).getYear() == 70) {
              ((Date)localObject6).setYear(paramDate.getYear());
            }
            i2 = Y;
          }
          catch (ParseException paramh)
          {
            continue;
          }
          try
          {
            Y = (i2 | 0x4);
            paramh = (h)localObject6;
          }
          catch (ParseException paramh) {}
        }
        paramh = (h)localObject6;
        e.b();
        i1 += 1;
      }
      localObject3 = paramh;
      if (paramh == null)
      {
        e.b();
        localObject3 = paramDate;
      }
      paramh = localJSONObject.optJSONObject("note");
      if (paramh != null)
      {
        i1 = paramh.optInt("group_id", -1);
        paramh = paramh.optString("prefix", "");
        if (i1 >= 0) {
          if (paramh.isEmpty())
          {
            if (!TextUtils.isEmpty(paramMatcher.group(i1).trim())) {
              V = paramMatcher.group(i1).trim();
            }
          }
          else
          {
            localObject5 = new StringBuilder();
            ((StringBuilder)localObject5).append(paramh);
            ((StringBuilder)localObject5).append(" ");
            ((StringBuilder)localObject5).append(paramMatcher.group(i1));
            V = ((StringBuilder)localObject5).toString();
          }
        }
      }
      paramh = localJSONObject.optJSONObject("currency");
      if (paramh != null)
      {
        i1 = paramh.optInt("group_id", -1);
        if (i1 >= 0)
        {
          paramh = paramMatcher.group(i1).trim().toUpperCase();
          e = paramh;
          if ((!TextUtils.equals(paramh, "INR")) && (!TextUtils.equals(paramh, "RS")))
          {
            Y |= 0x400;
            h = ((Double)localObject2).doubleValue();
            g = 0.0D;
          }
        }
      }
      if (localJSONObject.optBoolean("deleted", false)) {
        Y |= 0x10;
      }
      if (localJSONObject.optBoolean("incomplete", false)) {
        Y |= 0x100;
      }
      localm.a(d(str2), (Double)localObject2, (Date)localObject3, (String)localObject4, m.a(str1));
      m = ((String)localObject4);
      if (paramString1.doubleValue() == Double.MIN_VALUE) {
        if (((Double)localObject1).doubleValue() == Double.MIN_VALUE) {
          break label2329;
        }
      }
      localObject2 = new b();
      a = paramString1.doubleValue();
      b = ((Double)localObject1).doubleValue();
      if (paramString1.doubleValue() != Double.MIN_VALUE) {
        c = paramDate;
      }
      paramh = (h)localObject2;
      if (((Double)localObject1).doubleValue() != Double.MIN_VALUE)
      {
        d = paramDate;
        paramh = (h)localObject2;
      }
      i = paramh;
      if ((j == 3) || (j == 15)) {
        localm.j();
      }
      bool = localJSONObject.optBoolean("enable_chaining", false);
      K = bool;
      if (bool) {}
      try
      {
        paramString1 = localJSONObject.getJSONObject("chaining_rule");
        if (paramString1 == null) {
          break label1732;
        }
        paramh = new c();
        paramDate = paramString1.optJSONArray("parent_selection");
        if ((paramDate != null) && (paramDate.length() > 0))
        {
          i1 = 0;
          while (i1 < paramDate.length())
          {
            paramh.a(a(paramDate.getJSONObject(i1), paramMatcher));
            i1 += 1;
          }
        }
        paramMatcher = paramString1.optJSONObject("parent_match");
        if (paramMatcher != null) {
          b = a(paramMatcher);
        }
        paramMatcher = paramString1.optJSONObject("parent_nomatch");
        if (paramMatcher != null) {
          c = a(paramMatcher);
        }
        J = paramh;
      }
      catch (JSONException paramMatcher)
      {
        for (;;) {}
      }
      e.b();
      if (a((String)localObject4, i))
      {
        Y |= 0x20000;
      }
      else if (a((String)localObject4, k))
      {
        Y |= 0x10000;
      }
      else if (a((String)localObject4, l))
      {
        Y |= 0x80000;
      }
      else if (a((String)localObject4, j))
      {
        Y |= 0x40000;
      }
      else
      {
        paramMatcher = paramString2.toLowerCase();
        if (!((Pattern)t.get(0)).matcher(paramMatcher).matches()) {
          break label2343;
        }
        if (!((Pattern)t.get(1)).matcher(paramMatcher).matches()) {
          break label2340;
        }
        if ((((Pattern)u.get(0)).matcher(paramMatcher).matches()) || (((Pattern)u.get(1)).matcher(paramMatcher).matches()) || (((Pattern)u.get(2)).matcher(paramMatcher).matches()) || (((Pattern)u.get(3)).matcher(paramMatcher).matches()) || (((Pattern)u.get(4)).matcher(paramMatcher).matches()) || (((Pattern)u.get(5)).matcher(paramMatcher).matches()) || (((Pattern)u.get(6)).matcher(paramMatcher).matches()) || (((Pattern)u.get(7)).matcher(paramMatcher).matches()) || (((Pattern)u.get(8)).matcher(paramMatcher).matches()) || (((Pattern)u.get(9)).matcher(paramMatcher).matches())) {
          break label2334;
        }
        i1 = 1;
        if (i1 != 0) {
          Y |= 0x100000;
        }
      }
      i2 = 1;
      if (j == 17)
      {
        if ((!c.matcher(paramString2.toLowerCase()).matches()) && (!d.matcher(paramString2.toLowerCase()).matches())) {
          break label2349;
        }
        i1 = i2;
        if (!e.matcher(paramString2.toLowerCase()).matches())
        {
          if (!f.matcher(paramString2.toLowerCase()).matches()) {
            break label2349;
          }
          i1 = i2;
        }
        if (i1 != 0) {
          Y |= 0x200000;
        }
      }
      return localm;
      return null;
      return null;
      return null;
    }
  }
  
  private static ArrayList<k> a(String paramString1, String paramString2, String paramString3, Date paramDate, HashMap<String, ArrayList<h>> paramHashMap, String paramString4)
  {
    ArrayList localArrayList2 = (ArrayList)paramHashMap.get(paramString1.trim());
    Object localObject1;
    if ((localArrayList2 != null) && (localArrayList2.size() > 0))
    {
      paramString1 = paramString3.replaceAll("\\s{2,}", " ");
      ArrayList localArrayList1 = new ArrayList();
      e.a();
      paramHashMap = null;
      localObject1 = paramString1;
      while (((String)localObject1).length() > 0)
      {
        int i2 = 0;
        while (i2 < localArrayList2.size())
        {
          h localh = (h)localArrayList2.get(i2);
          Object localObject2 = h;
          String str1 = paramString1;
          String str2;
          if (localObject2 != null) {
            try
            {
              str1 = paramString1.replaceAll(((JSONObject)localObject2).getString("replace"), ((JSONObject)localObject2).getString("by"));
            }
            catch (JSONException localJSONException)
            {
              localJSONException.printStackTrace();
              str2 = paramString1;
            }
          }
          Matcher localMatcher = b.matcher((CharSequence)localObject1);
          if (localMatcher.find())
          {
            localObject2 = localMatcher.replaceFirst("");
            e.a();
            i1 = com.daamitt.prime.sdk.a.a.a(c);
            if ((i1 != 9999) && (i1 != 99)) {
              localObject1 = paramHashMap;
            }
          }
          try
          {
            if (d.equalsIgnoreCase("transaction"))
            {
              localObject1 = paramHashMap;
              paramHashMap = a(localMatcher, localh, paramString2, paramString3, paramDate);
              paramString1 = paramHashMap;
              if (paramHashMap != null)
              {
                localObject1 = paramHashMap;
                b(paramHashMap);
                paramString1 = paramHashMap;
              }
            }
            else
            {
              localObject1 = paramHashMap;
              if (d.equalsIgnoreCase("statement"))
              {
                localObject1 = paramHashMap;
                paramHashMap = b(localMatcher, localh, paramString2, paramString3, paramDate);
                paramString1 = paramHashMap;
                if (paramHashMap != null)
                {
                  localObject1 = paramHashMap;
                  b(paramHashMap);
                  paramString1 = paramHashMap;
                }
              }
              else
              {
                paramString1 = paramHashMap;
                localObject1 = paramHashMap;
                if (d.equalsIgnoreCase("event"))
                {
                  localObject1 = paramHashMap;
                  paramString1 = c(localMatcher, localh, paramString2, paramString3, paramDate);
                }
              }
            }
          }
          catch (RuntimeException paramString1)
          {
            for (;;) {}
          }
          e.d();
          paramString1 = (String)localObject1;
          if (paramString1 != null)
          {
            F = localh;
            localArrayList1.add(paramString1);
          }
          if (m)
          {
            i1 = 1;
            localObject1 = localObject2;
            paramHashMap = paramString1;
            paramString1 = str2;
            break label443;
          }
          localObject1 = localObject2;
          i1 = 0;
          paramHashMap = paramString1;
          paramString1 = str2;
          break label443;
          localObject1 = localObject2;
          i1 = 0;
          paramString1 = str2;
          break label443;
          i2 += 1;
          paramString1 = str2;
        }
        int i1 = 0;
        label443:
        if ((i2 == localArrayList2.size()) || (i1 == 0)) {
          break;
        }
      }
      paramHashMap = localArrayList1;
      if (localArrayList1.isEmpty())
      {
        paramHashMap = localArrayList1;
        if (!a(paramString3, paramString4))
        {
          paramString1 = new k(paramString2, paramString3, paramDate);
          u = 9;
          v = 9;
          paramString1.a(get0e, "Messages");
          if (a(paramString3)) {
            G |= 0x8000;
          } else if (b(paramString3)) {
            G |= 0x10000;
          }
          b(paramString1);
          a(paramString1);
          localArrayList1.add(paramString1);
          return localArrayList1;
        }
      }
    }
    else
    {
      localObject1 = new ArrayList();
      paramHashMap = (HashMap<String, ArrayList<h>>)localObject1;
      if (!a(paramString3, paramString4))
      {
        paramString1 = a(paramString1, paramString2, paramString3, paramDate);
        if (a(paramString3)) {
          G |= 0x8000;
        } else if (b(paramString3)) {
          G |= 0x10000;
        }
        b(paramString1);
        a(paramString1);
        ((ArrayList)localObject1).add(paramString1);
        paramHashMap = (HashMap<String, ArrayList<h>>)localObject1;
      }
    }
    return paramHashMap;
  }
  
  public static ArrayList<k> a(String paramString1, String paramString2, Date paramDate, HashMap<String, ArrayList<h>> paramHashMap, String paramString3)
  {
    String str = paramString1.toUpperCase();
    if ((!str.matches("(?i)[A-Z]{2}-?[!,A-Z,0-9]{1,8}\\s*")) && (!str.matches("(?i)[0-9]{1,9}\\s*"))) {
      return null;
    }
    paramString1 = "Unknown";
    Object localObject = str.split("-");
    int i2 = localObject.length;
    int i1 = 1;
    if (i2 == 2) {
      return a(localObject[1], str, paramString2, paramDate, paramHashMap, paramString3);
    }
    if (localObject.length == 1)
    {
      if (str.matches("(?i)[0-9]{1,9}\\s*")) {
        return a(localObject[0], str, paramString2, paramDate, paramHashMap, paramString3);
      }
      localObject = (ArrayList)paramHashMap.get(str.substring(2).trim());
      if ((localObject != null) && (((ArrayList)localObject).size() > 0))
      {
        paramString1 = str.substring(2);
      }
      else
      {
        localObject = (ArrayList)paramHashMap.get(str.trim());
        if ((localObject != null) && (!((ArrayList)localObject).isEmpty())) {
          paramString1 = str;
        } else {
          i1 = 0;
        }
      }
      if (i1 == 0) {
        if (str.length() >= 7) {
          paramString1 = str.substring(2);
        } else {
          paramString1 = str;
        }
      }
      return a(paramString1, str, paramString2, paramDate, paramHashMap, paramString3);
    }
    paramString1 = new ArrayList();
    if (!a(paramString2, paramString3)) {
      paramString1.add(a("Unknown", str, paramString2, paramDate));
    }
    return paramString1;
  }
  
  private static void a(k paramk)
  {
    Object localObject = q;
    if ((localObject != null) && (!TextUtils.isEmpty(((String)localObject).trim())) && ((c.matcher(((String)localObject).toLowerCase()).matches()) || (d.matcher(((String)localObject).toLowerCase()).matches())) && ((e.matcher(((String)localObject).toLowerCase()).matches()) || (f.matcher(((String)localObject).toLowerCase()).matches())) && (!g.matcher(((String)localObject).toLowerCase()).matches()) && (!h.matcher(((String)localObject).toLowerCase()).matches())) {}
    for (;;)
    {
      try
      {
        if (b.matcher(((String)localObject).toLowerCase()).matches())
        {
          localObject = b.matcher(((String)localObject).toLowerCase());
          boolean bool = ((Matcher)localObject).find();
          if (bool) {
            d1 = Double.MIN_VALUE;
          }
        }
      }
      catch (IllegalStateException localIllegalStateException)
      {
        double d1;
        double d2;
        localIllegalStateException.getMessage();
        e.c();
      }
      try
      {
        d2 = Double.valueOf(((Matcher)localObject).group(1).replace(",", "")).doubleValue();
        d1 = d2;
      }
      catch (NumberFormatException localNumberFormatException) {}
    }
    e.a();
    S = d1;
    G |= 0x8000000;
  }
  
  private static boolean a(String paramString)
  {
    return (((Pattern)s.get(0)).matcher(paramString.toLowerCase()).matches()) && (((Pattern)s.get(1)).matcher(paramString.toLowerCase()).matches());
  }
  
  private static boolean a(String paramString1, String paramString2)
  {
    paramString1 = paramString1.replaceAll("\n", "").toLowerCase();
    return (paramString2 != null) && (paramString1.matches(paramString2));
  }
  
  private static boolean a(String paramString, ArrayList<Pattern> paramArrayList)
  {
    paramArrayList = paramArrayList.iterator();
    while (paramArrayList.hasNext()) {
      if (((Pattern)paramArrayList.next()).matcher(paramString).find()) {
        return true;
      }
    }
    return false;
  }
  
  private static l b(Matcher paramMatcher, h paramh, String paramString1, String paramString2, Date paramDate)
  {
    try
    {
      localJSONObject1 = g;
      str3 = localJSONObject1.getString("statement_type");
      localObject3 = localJSONObject1.optJSONObject("pan");
      if (localObject3 == null) {
        break label817;
      }
      i1 = ((JSONObject)localObject3).optInt("group_id", -1);
      if (i1 < 0) {
        break label804;
      }
      localObject1 = paramMatcher.group(i1);
    }
    catch (JSONException paramMatcher)
    {
      JSONObject localJSONObject1;
      String str3;
      int i1;
      Object localObject1;
      JSONObject localJSONObject2;
      Object localObject6;
      Object localObject5;
      paramMatcher.printStackTrace();
      return null;
    }
    catch (NumberFormatException paramMatcher)
    {
      for (;;)
      {
        label775:
        continue;
        label804:
        Object localObject2 = null;
        continue;
        Object localObject3 = localObject2;
        continue;
        label817:
        localObject3 = null;
        continue;
        label823:
        String str1 = null;
        continue;
        label829:
        String str2 = null;
        continue;
        label835:
        Object localObject4 = null;
        continue;
        label841:
        continue;
        label844:
        localObject2 = null;
      }
    }
    if (localObject1 == null)
    {
      localObject3 = ((JSONObject)localObject3).optString("value", "Unknown");
      localJSONObject2 = localJSONObject1.optJSONObject("amount");
      if (localJSONObject2 == null) {
        break label823;
      }
      i1 = localJSONObject2.optInt("group_id", -1);
      if (i1 >= 0) {
        str1 = paramMatcher.group(i1).replace(",", "");
      } else {
        str1 = localJSONObject2.optString("value");
      }
      localObject1 = localJSONObject1.optJSONObject("min_due_amount");
      if (localObject1 == null) {
        break label829;
      }
      i1 = ((JSONObject)localObject1).optInt("group_id", -1);
      if (i1 < 0) {
        break label829;
      }
      str2 = paramMatcher.group(i1).replace(",", "");
      localObject1 = localJSONObject1.optJSONObject("date");
      if (localObject1 == null) {
        break label844;
      }
      i1 = ((JSONObject)localObject1).optInt("group_id", -1);
      if (i1 < 0) {
        break label835;
      }
      localObject4 = paramMatcher.group(i1).trim().toLowerCase();
      localObject6 = ((JSONObject)localObject1).optJSONArray("formats");
      if (localObject6 == null) {
        break label844;
      }
      i1 = 0;
      localObject1 = null;
      for (;;)
      {
        if (i1 >= ((JSONArray)localObject6).length()) {
          break label841;
        }
        localObject5 = ((JSONArray)localObject6).getJSONObject(i1);
        if (((JSONObject)localObject5).optBoolean("use_sms_time", false))
        {
          localObject1 = paramDate;
          break;
        }
        localObject5 = ((JSONObject)localObject5).optString("format");
        for (;;)
        {
          try
          {
            localObject5 = b((String)localObject4, (String)localObject5);
          }
          catch (ParseException localParseException2)
          {
            boolean bool;
            continue;
          }
          try
          {
            if (((Date)localObject5).getYear() == 70)
            {
              ((Date)localObject5).setYear(paramDate.getYear());
              if (((Date)localObject5).before(paramDate)) {
                ((Date)localObject5).setYear(paramDate.getYear() + 1);
              }
            }
            localObject1 = localObject5;
          }
          catch (ParseException localParseException1) {}
        }
        localObject1 = localObject5;
        e.b();
        i1 += 1;
      }
      if (localObject1 == null)
      {
        e.b();
        return null;
      }
      localObject5 = "-1";
      localObject6 = localJSONObject1.optJSONObject("create_txn");
      localObject4 = localObject5;
      if (localObject6 != null)
      {
        localObject6 = ((JSONObject)localObject6).optJSONObject("amount");
        localObject4 = localObject5;
        if (localObject6 != null)
        {
          i1 = ((JSONObject)localObject6).optInt("group_id", -1);
          if (i1 >= 0) {
            localObject4 = paramMatcher.group(i1).replace(",", "");
          } else {
            localObject4 = localJSONObject2.optString("value");
          }
        }
      }
      paramString1 = new l(paramString1, paramString2, paramDate);
      if (localJSONObject1.optBoolean("deleted", false)) {
        j |= 0x2;
      }
      u = 7;
      E = true;
      v = com.daamitt.prime.sdk.a.a.a(c);
      B = f;
      paramString1.a(e, "Bills");
      paramString1.a(d((String)localObject3), Double.valueOf(str1).doubleValue(), (Date)localObject1, l.a(str3));
      L = i;
      if (!TextUtils.isEmpty(str2)) {
        e = Double.valueOf(str2).doubleValue();
      }
      f = Double.valueOf((String)localObject4).doubleValue();
      i1 = 0;
      bool = localJSONObject1.optBoolean("enable_chaining", false);
      K = bool;
      if (bool) {}
      try
      {
        paramString2 = localJSONObject1.getJSONObject("chaining_rule");
        if (paramString2 == null) {
          break label775;
        }
        paramh = new c();
        paramDate = paramString2.optJSONArray("parent_selection");
        if ((paramDate != null) && (paramDate.length() > 0)) {
          while (i1 < paramDate.length())
          {
            paramh.a(a(paramDate.getJSONObject(i1), paramMatcher));
            i1 += 1;
          }
        }
        paramMatcher = paramString2.optJSONObject("parent_match");
        if (paramMatcher != null) {
          b = a(paramMatcher);
        }
        paramMatcher = paramString2.optJSONObject("parent_nomatch");
        if (paramMatcher != null) {
          c = a(paramMatcher);
        }
        J = paramh;
        return paramString1;
      }
      catch (JSONException paramMatcher)
      {
        for (;;) {}
      }
      e.b();
      return paramString1;
      return null;
    }
  }
  
  private static Date b(String paramString1, String paramString2)
  {
    return new SimpleDateFormat(paramString2).parse(paramString1);
  }
  
  private static boolean b(k paramk)
  {
    String str = q;
    if ((str != null) && (!TextUtils.isEmpty(str.trim())) && (!str.toLowerCase().trim().matches("(?i).* emi .*"))) {
      return false;
    }
    str = p;
    if (str.matches("(?i).*axis.*")) {
      return c(paramk);
    }
    if ((!str.matches("(?i).*bajajf")) && (!str.matches("(?i).*040801")))
    {
      if (str.matches("(?i).*icici.*")) {
        return h(paramk);
      }
      if (str.matches("(?i).*sbi.*")) {
        return e(paramk);
      }
      if (str.matches("(?i).*citi.*")) {
        return f(paramk);
      }
      if ((!str.matches("(?i).*hdfc.*")) && (!str.matches("(?i).*020001")) && (!str.matches("(?i).*080008"))) {
        return false;
      }
      return g(paramk);
    }
    return d(paramk);
  }
  
  private static boolean b(String paramString)
  {
    paramString = paramString.toLowerCase();
    if ((((Pattern)t.get(0)).matcher(paramString).matches()) && (((Pattern)t.get(1)).matcher(paramString).matches()) && (!((Pattern)v.get(0)).matcher(paramString).matches()) && (!((Pattern)v.get(1)).matcher(paramString).matches()) && (!((Pattern)v.get(2)).matcher(paramString).matches()) && (!((Pattern)v.get(3)).matcher(paramString).matches()) && ((((Pattern)v.get(4)).matcher(paramString).matches()) || (((Pattern)v.get(5)).matcher(paramString).matches()) || (((Pattern)v.get(6)).matcher(paramString).matches()) || (((Pattern)v.get(7)).matcher(paramString).matches()) || (((Pattern)v.get(8)).matcher(paramString).matches()) || (((Pattern)v.get(9)).matcher(paramString).matches()) || (((Pattern)v.get(10)).matcher(paramString).matches()) || (((Pattern)v.get(11)).matcher(paramString).matches()) || (((Pattern)v.get(12)).matcher(paramString).matches()) || (((Pattern)v.get(13)).matcher(paramString).matches()) || (((Pattern)v.get(14)).matcher(paramString).matches()) || (((Pattern)v.get(15)).matcher(paramString).matches()) || (((Pattern)v.get(16)).matcher(paramString).matches()) || (((Pattern)v.get(17)).matcher(paramString).matches()) || (((Pattern)v.get(18)).matcher(paramString).matches()) || (((Pattern)v.get(19)).matcher(paramString).matches()) || (((Pattern)v.get(20)).matcher(paramString).matches()) || (((Pattern)v.get(21)).matcher(paramString).matches()) || (((Pattern)v.get(22)).matcher(paramString).matches()) || (((Pattern)v.get(23)).matcher(paramString).matches()) || (((Pattern)v.get(24)).matcher(paramString).matches()) || (((Pattern)v.get(25)).matcher(paramString).matches()) || (((Pattern)v.get(26)).matcher(paramString).matches()) || (((Pattern)v.get(27)).matcher(paramString).matches()) || (((Pattern)v.get(28)).matcher(paramString).matches()) || (((Pattern)v.get(29)).matcher(paramString).matches()) || (((Pattern)v.get(30)).matcher(paramString).matches()) || (((Pattern)v.get(31)).matcher(paramString).matches()) || (((Pattern)v.get(32)).matcher(paramString).matches()) || (((Pattern)v.get(33)).matcher(paramString).matches()))) {
      return (!((Pattern)u.get(0)).matcher(paramString).matches()) && (!((Pattern)u.get(1)).matcher(paramString).matches()) && (!((Pattern)u.get(2)).matcher(paramString).matches()) && (!((Pattern)u.get(3)).matcher(paramString).matches()) && (!((Pattern)u.get(4)).matcher(paramString).matches()) && (!((Pattern)u.get(5)).matcher(paramString).matches()) && (!((Pattern)u.get(6)).matcher(paramString).matches()) && (!((Pattern)u.get(7)).matcher(paramString).matches()) && (!((Pattern)u.get(8)).matcher(paramString).matches()) && (!((Pattern)u.get(9)).matcher(paramString).matches());
    }
    return false;
  }
  
  private static d c(Matcher paramMatcher, h paramh, String paramString1, String paramString2, Date paramDate)
  {
    try
    {
      localJSONObject = g;
      str3 = localJSONObject.getString("event_type");
      localObject1 = localJSONObject.optJSONObject("name");
      if (localObject1 == null) {
        break label1528;
      }
      i1 = ((JSONObject)localObject1).optInt("group_id", -1);
      if (i1 >= 0) {
        str1 = paramMatcher.group(i1);
      } else {
        str1 = ((JSONObject)localObject1).optString("value", "Unknown");
      }
    }
    catch (JSONException paramMatcher)
    {
      JSONObject localJSONObject;
      String str3;
      paramMatcher.printStackTrace();
      return null;
    }
    catch (NumberFormatException paramMatcher)
    {
      label398:
      label1366:
      label1528:
      label1540:
      label1546:
      label1552:
      label1558:
      label1567:
      label1573:
      label1583:
      label1592:
      label1601:
      label1607:
      label1610:
      label1616:
      label1624:
      label1631:
      label1634:
      for (;;)
      {
        continue;
        String str1 = null;
        continue;
        String str2 = null;
        continue;
        Object localObject2 = null;
        continue;
        continue;
        continue;
        double d1 = 0.0D;
        continue;
        boolean bool1 = false;
        d1 = 0.0D;
        continue;
        Object localObject1 = null;
        continue;
        int i1 = 1;
        localObject1 = localNumberFormatException;
        continue;
        i1 = 1;
        localObject1 = null;
        continue;
        int i2 = 1;
        Object localObject4 = null;
        continue;
        Object localObject5 = null;
        continue;
        localObject1 = null;
        continue;
        long l1 = 1800000L;
        continue;
        paramh = "";
        continue;
      }
    }
    localObject1 = localJSONObject.optJSONObject("pnr");
    if (localObject1 != null)
    {
      i1 = ((JSONObject)localObject1).optInt("group_id", -1);
      if (i1 >= 0)
      {
        str2 = paramMatcher.group(i1).replace(",", "");
        localObject2 = localJSONObject.optJSONObject("contact");
        if (localObject2 == null) {
          break label1540;
        }
        localObject1 = ((JSONObject)localObject2).optString("prefix", "");
        i1 = ((JSONObject)localObject2).optInt("group_id", -1);
        if (i1 < 0) {
          break label1540;
        }
        if (((String)localObject1).length() > 0)
        {
          localObject2 = new StringBuilder();
          ((StringBuilder)localObject2).append((String)localObject1);
          ((StringBuilder)localObject2).append(" ");
          ((StringBuilder)localObject2).append(paramMatcher.group(i1));
          localObject2 = ((StringBuilder)localObject2).toString();
        }
        else
        {
          localObject2 = paramMatcher.group(i1);
        }
        localObject1 = localJSONObject.optJSONObject("amount");
        if (localObject1 == null) {
          break label1558;
        }
        i1 = ((JSONObject)localObject1).optInt("group_id", -1);
        if (i1 >= 0) {}
        try
        {
          d1 = Double.valueOf(paramMatcher.group(i1).replace(",", "")).doubleValue();
        }
        catch (NumberFormatException localNumberFormatException)
        {
          Object localObject3;
          Object localObject6;
          for (;;) {}
        }
        e.d();
        d1 = 0.0D;
        if (!((JSONObject)localObject1).optString("txn_direction", "").equalsIgnoreCase("incoming")) {
          break label1546;
        }
        d1 = -d1;
        break label398;
        localObject3 = ((JSONObject)localObject1).optJSONArray("group_ids");
        if (localObject3 == null) {
          break label1552;
        }
        i1 = 0;
        d1 = 0.0D;
        while (i1 < ((JSONArray)localObject3).length())
        {
          d1 += Double.valueOf(paramMatcher.group(((JSONArray)localObject3).getInt(i1)).replace(",", "")).doubleValue();
          i1 += 1;
        }
        bool1 = ((JSONObject)localObject1).optBoolean("create_txn", false);
        localObject3 = localJSONObject.optJSONObject("date");
        if (localObject3 == null) {
          break label1592;
        }
        if (((JSONObject)localObject3).optBoolean("use_sms_time", false))
        {
          localObject3 = paramDate;
          i2 = 1;
        }
        else
        {
          localObject1 = ((JSONObject)localObject3).optJSONArray("group_ids");
          if (localObject1 != null)
          {
            localObject5 = new StringBuilder();
            i1 = 0;
            while (i1 < ((JSONArray)localObject1).length())
            {
              if (i1 > 0) {
                ((StringBuilder)localObject5).append(" ");
              }
              ((StringBuilder)localObject5).append(paramMatcher.group(((JSONArray)localObject1).getInt(i1)));
              i1 += 1;
            }
            localObject1 = ((StringBuilder)localObject5).toString();
          }
          else
          {
            i1 = ((JSONObject)localObject3).optInt("group_id", -1);
            if (i1 < 0) {
              break label1567;
            }
            localObject1 = paramMatcher.group(i1);
          }
          localObject5 = ((JSONObject)localObject3).optJSONArray("formats");
          if (localObject5 == null) {
            break label1583;
          }
          i1 = 0;
          localObject3 = null;
          for (;;)
          {
            if (i1 >= ((JSONArray)localObject5).length()) {
              break label1573;
            }
            localObject6 = ((JSONArray)localObject5).getJSONObject(i1);
            if (((JSONObject)localObject6).optBoolean("use_sms_time", false))
            {
              localObject1 = paramDate;
              i1 = 1;
              break;
            }
            String str4 = ((JSONObject)localObject6).optString("format");
            try
            {
              localObject6 = b((String)localObject1, str4);
              localObject3 = localObject6;
              if (!str4.contains("HH"))
              {
                localObject3 = localObject6;
                boolean bool2 = str4.contains("hh");
                if (!bool2)
                {
                  i1 = 0;
                  localObject1 = localObject6;
                  break;
                }
              }
              i1 = 1;
              localObject1 = localObject6;
            }
            catch (ParseException localParseException)
            {
              for (;;) {}
            }
            e.b();
            i1 += 1;
          }
          i2 = i1;
          localObject3 = localObject1;
          if (localObject1 == null)
          {
            e.b();
            localObject3 = paramDate;
            i2 = i1;
          }
        }
        if (((Date)localObject3).getYear() == 70) {
          ((Date)localObject3).setYear(paramDate.getYear());
        }
        localObject1 = localJSONObject.optJSONObject("event_info");
        if (localObject1 == null) {
          break label1601;
        }
        i1 = ((JSONObject)localObject1).optInt("group_id", -1);
        if (i1 >= 0) {
          localObject5 = paramMatcher.group(i1).trim();
        } else {
          localObject5 = ((JSONObject)localObject1).optString("value");
        }
        localObject1 = localJSONObject.optJSONObject("event_location");
        if (localObject1 == null) {
          break label1610;
        }
        i1 = ((JSONObject)localObject1).optInt("group_id", -1);
        if (i1 >= 0)
        {
          localObject1 = paramMatcher.group(i1);
        }
        else
        {
          localObject1 = ((JSONObject)localObject1).optJSONArray("group_ids");
          if (localObject1 == null) {
            break label1607;
          }
          localObject6 = new StringBuilder();
          i1 = 0;
          while (i1 < ((JSONArray)localObject1).length())
          {
            if (i1 > 0) {
              ((StringBuilder)localObject6).append("-");
            }
            ((StringBuilder)localObject6).append(paramMatcher.group(((JSONArray)localObject1).getInt(i1)));
            i1 += 1;
          }
          localObject1 = ((StringBuilder)localObject6).toString();
        }
        localObject6 = localJSONObject.optJSONObject("event_reminder_span");
        if (localObject6 == null) {
          break label1616;
        }
        l1 = 60000L * ((JSONObject)localObject6).optInt("value", 30);
        paramDate = new d(paramString1, paramString2, paramDate);
        i1 = d.a(str3);
        paramDate.a(e, "events");
        u = 5;
        E = true;
        v = com.daamitt.prime.sdk.a.a.a(c);
        m = l1;
        if (str2 == null) {
          break label1624;
        }
        paramh = d(str2);
        paramString1 = paramh;
        if (2 == i1) {
          paramString1 = paramh.replace(" ", "");
        }
        paramDate.a(d(str1), paramString1, (Date)localObject3, i1);
        g = ((String)localObject5);
        k = bool1;
        if (TextUtils.equals("Cancelled", (CharSequence)localObject5)) {
          h |= 0x8;
        }
        if (i2 != 0) {
          break label1631;
        }
        h |= 0x2;
        if (d1 != 0.0D) {
          i = d1;
        }
        if (TextUtils.isEmpty((CharSequence)localObject2)) {
          break label1634;
        }
        j = ((String)localObject2);
        f = ((String)localObject1);
        if (localJSONObject.optBoolean("deleted", false)) {
          h |= 0x1;
        }
        i1 = 0;
        bool1 = localJSONObject.optBoolean("enable_chaining", false);
        K = bool1;
        if (bool1) {}
        try
        {
          paramString1 = localJSONObject.getJSONObject("chaining_rule");
          if (paramString1 == null) {
            break label1366;
          }
          paramh = new c();
          localObject1 = paramString1.optJSONArray("parent_selection");
          if ((localObject1 != null) && (((JSONArray)localObject1).length() > 0)) {
            while (i1 < ((JSONArray)localObject1).length())
            {
              paramh.a(a(((JSONArray)localObject1).getJSONObject(i1), paramMatcher));
              i1 += 1;
            }
          }
          paramMatcher = paramString1.optJSONObject("parent_match");
          if (paramMatcher != null) {
            b = a(paramMatcher);
          }
          paramMatcher = paramString1.optJSONObject("parent_nomatch");
          if (paramMatcher != null) {
            c = a(paramMatcher);
          }
          J = paramh;
        }
        catch (JSONException paramMatcher)
        {
          for (;;) {}
        }
        e.b();
        if (e == 5)
        {
          paramMatcher = c(paramString2);
          if ((paramMatcher != null) && (!TextUtils.isEmpty(paramMatcher.trim())) && ((paramMatcher.trim().equalsIgnoreCase("3A")) || (paramMatcher.trim().equalsIgnoreCase("2A")) || (paramMatcher.trim().equalsIgnoreCase("1A")) || (paramMatcher.trim().equalsIgnoreCase("CC")) || (paramMatcher.trim().equalsIgnoreCase("FC")) || (paramMatcher.trim().equalsIgnoreCase("3E")) || (paramMatcher.trim().equalsIgnoreCase("EC")))) {
            h &= 0x10;
          }
        }
        return paramDate;
        return null;
      }
    }
  }
  
  private static String c(String paramString)
  {
    String str4 = paramString.replaceAll("\\s{2,}", " ");
    String str1 = null;
    paramString = str1;
    Matcher localMatcher;
    try
    {
      if (str4.matches("(?i).*DOJ:.*,TIME:.*"))
      {
        localMatcher = Pattern.compile("(?i).*DOJ:[\\w-]+,(?:TIME):?\\s?[\\w.:]+?,(.*?),.*").matcher(str4);
        paramString = str1;
        if (localMatcher.find()) {
          paramString = localMatcher.group(1);
        }
      }
    }
    catch (IllegalStateException paramString)
    {
      paramString.getMessage();
      e.c();
      paramString = str1;
    }
    str1 = paramString;
    if (paramString == null)
    {
      str1 = paramString;
      try
      {
        if (!str4.matches("(?i).*,.*"))
        {
          str1 = paramString;
          if (!str4.matches("(?i).*DOJ:\\d\\d-\\d\\d-\\d\\d\\d\\d.*"))
          {
            localMatcher = Pattern.compile("(?i).*DOJ:\\d\\d-\\d\\d-\\d\\d(\\w\\w).*").matcher(str4);
            str1 = paramString;
            if (localMatcher.find()) {
              str1 = localMatcher.group(1);
            }
          }
        }
      }
      catch (IllegalStateException localIllegalStateException1)
      {
        localIllegalStateException1.getMessage();
        e.c();
        str2 = paramString;
      }
    }
    paramString = str2;
    if (str2 == null)
    {
      paramString = str2;
      try
      {
        if (!str4.matches("(?i).*,.*"))
        {
          paramString = str2;
          if (str4.matches("(?i).*TIME:N.A..*"))
          {
            localMatcher = Pattern.compile("(?i).*DOJ:\\d\\d-\\d\\d-\\d\\d\\d\\d(?:TIME):N.A.(\\w\\w).*").matcher(str4);
            paramString = str2;
            if (localMatcher.find()) {
              paramString = localMatcher.group(1);
            }
          }
        }
      }
      catch (IllegalStateException paramString)
      {
        paramString.getMessage();
        e.c();
        paramString = str2;
      }
    }
    String str2 = paramString;
    String str3;
    if (paramString == null)
    {
      str2 = paramString;
      try
      {
        if (!str4.matches("(?i).*,.*"))
        {
          str2 = paramString;
          if (str4.matches("(?i).*DOJ:\\d\\d-\\d\\d-\\d\\d\\d\\d.*"))
          {
            localMatcher = Pattern.compile("(?i).*DOJ:\\d\\d-\\d\\d-\\d\\d\\d\\d(?:TIME)?:?\\d?\\d?:?\\d?\\d?(\\w\\w).*").matcher(str4);
            str2 = paramString;
            if (localMatcher.find()) {
              str2 = localMatcher.group(1);
            }
          }
        }
      }
      catch (IllegalStateException localIllegalStateException2)
      {
        localIllegalStateException2.getMessage();
        e.c();
        str3 = paramString;
      }
    }
    if (str3 == null) {
      try
      {
        paramString = Pattern.compile("(?i).*DOJ:[\\w-]+,(.*?),.*").matcher(str4);
        if (paramString.find())
        {
          paramString = paramString.group(1);
          return paramString;
        }
      }
      catch (IllegalStateException paramString)
      {
        paramString.getMessage();
        e.c();
      }
    }
    return str3;
  }
  
  private static String c(String paramString1, String paramString2)
  {
    int i1 = paramString1.hashCode();
    if (i1 != -1413853096)
    {
      if ((i1 == 111156) && (paramString1.equals("pnr")))
      {
        i1 = 1;
        break label60;
      }
    }
    else if (paramString1.equals("amount"))
    {
      i1 = 0;
      break label60;
    }
    i1 = -1;
    switch (i1)
    {
    default: 
      return paramString2;
    case 1: 
      label60:
      return d(paramString2);
    }
    try
    {
      d1 = Double.valueOf(paramString2.replace(",", "")).doubleValue();
    }
    catch (NumberFormatException paramString1)
    {
      double d1;
      for (;;) {}
    }
    d1 = 0.0D;
    return String.valueOf(d1);
  }
  
  private static boolean c(k paramk)
  {
    String str = q.toLowerCase();
    if (((Pattern)m.get(0)).matcher(str).matches())
    {
      G |= 0x20000;
      return true;
    }
    if (((Pattern)m.get(1)).matcher(str).matches())
    {
      G |= 0x40000;
      return true;
    }
    if (((Pattern)m.get(2)).matcher(str).matches())
    {
      G |= 0x80000;
      return true;
    }
    if ((!((Pattern)m.get(3)).matcher(str).matches()) && (!((Pattern)m.get(4)).matcher(str).matches()) && (!((Pattern)m.get(5)).matcher(str).matches()))
    {
      if (((Pattern)m.get(6)).matcher(str).matches())
      {
        G |= 0x1000000;
        return true;
      }
      if (((Pattern)m.get(7)).matcher(str).matches())
      {
        G |= 0x80000;
        return true;
      }
      if (((Pattern)m.get(8)).matcher(str).matches())
      {
        G |= 0x200000;
        return true;
      }
      return false;
    }
    G |= 0x40000;
    return true;
  }
  
  private static String d(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return paramString.replaceAll("(?i)[^a-z0-9\\)\\]]*$", "").replaceAll("(?i)^[^a-z0-9\\)\\]]*", "");
  }
  
  private static boolean d(k paramk)
  {
    String str = q.toLowerCase();
    if ((!((Pattern)n.get(0)).matcher(str).matches()) && (!((Pattern)n.get(1)).matcher(str).matches()) && (!((Pattern)n.get(2)).matcher(str).matches()))
    {
      if (((Pattern)n.get(3)).matcher(str).matches())
      {
        G |= 0x20000;
        return true;
      }
      if (((Pattern)n.get(4)).matcher(str).matches())
      {
        G |= 0x400000;
        return true;
      }
      if (((Pattern)n.get(5)).matcher(str).matches())
      {
        G |= 0x20000;
        return true;
      }
      if (((Pattern)n.get(6)).matcher(str).matches())
      {
        G |= 0x80000;
        return true;
      }
      if ((!((Pattern)n.get(7)).matcher(str).matches()) && (!((Pattern)n.get(8)).matcher(str).matches()))
      {
        if (((Pattern)n.get(9)).matcher(str).matches())
        {
          G |= 0x80000;
          return true;
        }
        if ((!((Pattern)n.get(10)).matcher(str).matches()) && (!((Pattern)n.get(11)).matcher(str).matches()) && (!((Pattern)n.get(12)).matcher(str).matches()) && (!((Pattern)n.get(13)).matcher(str).matches()))
        {
          if (((Pattern)n.get(14)).matcher(str).matches())
          {
            G |= 0x80000;
            return true;
          }
          if ((!((Pattern)n.get(15)).matcher(str).matches()) && (!((Pattern)n.get(16)).matcher(str).matches()) && (!((Pattern)n.get(17)).matcher(str).matches()) && (!((Pattern)n.get(18)).matcher(str).matches()) && (!((Pattern)n.get(19)).matcher(str).matches()) && (!((Pattern)n.get(20)).matcher(str).matches()))
          {
            if (((Pattern)n.get(21)).matcher(str).matches())
            {
              G |= 0x1000000;
              return true;
            }
            return false;
          }
          G |= 0x400000;
          return true;
        }
        G |= 0x400000;
        return true;
      }
      G |= 0x400000;
      return true;
    }
    G |= 0x400000;
    return true;
  }
  
  private static boolean e(k paramk)
  {
    String str = q.toLowerCase();
    if (((Pattern)o.get(0)).matcher(str).matches())
    {
      G |= 0x20000;
      return true;
    }
    if (((Pattern)o.get(1)).matcher(str).matches())
    {
      G |= 0x400000;
      return true;
    }
    if (((Pattern)o.get(2)).matcher(str).matches())
    {
      G |= 0x80000;
      return true;
    }
    if ((!((Pattern)o.get(3)).matcher(str).matches()) && (!((Pattern)o.get(4)).matcher(str).matches()))
    {
      if (((Pattern)o.get(5)).matcher(str).matches())
      {
        G |= 0x80000;
        return true;
      }
      if (((Pattern)o.get(6)).matcher(str).matches())
      {
        G |= 0x100000;
        return true;
      }
      if ((!((Pattern)o.get(7)).matcher(str).matches()) && (!((Pattern)o.get(8)).matcher(str).matches()))
      {
        if (((Pattern)o.get(9)).matcher(str).matches())
        {
          G |= 0x1000000;
          return true;
        }
        return false;
      }
      G |= 0x80000;
      return true;
    }
    G |= 0x100000;
    return true;
  }
  
  private static boolean f(k paramk)
  {
    String str = q.toLowerCase();
    if (((Pattern)p.get(0)).matcher(str).matches())
    {
      G |= 0x400000;
      return true;
    }
    if (((Pattern)p.get(1)).matcher(str).matches())
    {
      G |= 0x20000;
      return true;
    }
    if (((Pattern)p.get(2)).matcher(str).matches())
    {
      G |= 0x80000;
      return true;
    }
    if (((Pattern)p.get(3)).matcher(str).matches())
    {
      G |= 0x1000000;
      return true;
    }
    return false;
  }
  
  private static boolean g(k paramk)
  {
    String str = q.toLowerCase();
    if ((!((Pattern)q.get(0)).matcher(str).matches()) && (!((Pattern)q.get(1)).matcher(str).matches()) && (!((Pattern)q.get(2)).matcher(str).matches()) && (!((Pattern)q.get(3)).matcher(str).matches()) && (!((Pattern)q.get(4)).matcher(str).matches()) && (!((Pattern)q.get(5)).matcher(str).matches()))
    {
      if ((!((Pattern)q.get(6)).matcher(str).matches()) && (!((Pattern)q.get(7)).matcher(str).matches()))
      {
        if ((!((Pattern)q.get(8)).matcher(str).matches()) && (!((Pattern)q.get(9)).matcher(str).matches()) && (!((Pattern)q.get(10)).matcher(str).matches()))
        {
          if (((Pattern)q.get(11)).matcher(str).matches())
          {
            G |= 0x80000;
            return true;
          }
          if (((Pattern)q.get(12)).matcher(str).matches())
          {
            G |= 0x800000;
            return true;
          }
          if (((Pattern)q.get(13)).matcher(str).matches())
          {
            G |= 0x2000000;
            return true;
          }
          if ((!((Pattern)q.get(14)).matcher(str).matches()) && (!((Pattern)q.get(15)).matcher(str).matches()) && (!((Pattern)q.get(16)).matcher(str).matches()) && (!((Pattern)q.get(17)).matcher(str).matches()) && (!((Pattern)q.get(18)).matcher(str).matches()) && (!((Pattern)q.get(19)).matcher(str).matches()))
          {
            if (((Pattern)q.get(20)).matcher(str).matches())
            {
              G |= 0x1000000;
              return true;
            }
            return false;
          }
          G |= 0x800000;
          return true;
        }
        G |= 0x20000;
        return true;
      }
      G |= 0x80000;
      return true;
    }
    G |= 0x20000;
    return true;
  }
  
  private static boolean h(k paramk)
  {
    String str = q.toLowerCase();
    if ((!((Pattern)r.get(0)).matcher(str).matches()) && (!((Pattern)r.get(1)).matcher(str).matches()))
    {
      if ((((Pattern)r.get(2)).matcher(str).matches()) && (!((Pattern)r.get(3)).matcher(str).matches()))
      {
        G |= 0x4000000;
        return true;
      }
      if (((Pattern)r.get(3)).matcher(str).matches())
      {
        G |= 0x1000000;
        return true;
      }
      return false;
    }
    G |= 0x20000;
    return true;
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */