package com.daamitt.prime.sdk.a;

import android.content.Context;
import android.text.TextUtils;
import com.google.gson.a.c;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public final class j
{
  @com.google.gson.a.a
  @c(a="v_26")
  private HashMap<String, String[]> A;
  @com.google.gson.a.a
  @c(a="v_27")
  private HashMap<String, String[]> B;
  @com.google.gson.a.a
  @c(a="v_28")
  private String[] C;
  @com.google.gson.a.a
  @c(a="v_29")
  private HashMap<String, Integer> D;
  @com.google.gson.a.a
  @c(a="v_30")
  private HashMap<String, Integer> E;
  @com.google.gson.a.a
  @c(a="v_31")
  private HashMap<String, String> F;
  @com.google.gson.a.a
  @c(a="v_00")
  private String[] a;
  @com.google.gson.a.a
  @c(a="v_01")
  private String[] b;
  @com.google.gson.a.a
  @c(a="v_02")
  private HashMap<String, String[]> c;
  @com.google.gson.a.a
  @c(a="v_03")
  private HashMap<String, String[]> d;
  @com.google.gson.a.a
  @c(a="v_04")
  private HashMap<String, String[]> e;
  @com.google.gson.a.a
  @c(a="v_05")
  private HashMap<String, String[]> f;
  @com.google.gson.a.a
  @c(a="v_06")
  private HashMap<String, String[]> g;
  @com.google.gson.a.a
  @c(a="v_07")
  private HashMap<String, String[]> h;
  @com.google.gson.a.a
  @c(a="v_08")
  private HashMap<String, String[]> i;
  @com.google.gson.a.a
  @c(a="v_09")
  private HashMap<String, String[]> j;
  @com.google.gson.a.a
  @c(a="v_10")
  private HashMap<String, String[]> k;
  @com.google.gson.a.a
  @c(a="v_11")
  private HashMap<String, String[]> l;
  @com.google.gson.a.a
  @c(a="v_12")
  private String[] m;
  @com.google.gson.a.a
  @c(a="v_13")
  private String[] n;
  @com.google.gson.a.a
  @c(a="v_14")
  private HashMap<String, String[]> o;
  @com.google.gson.a.a
  @c(a="v_15")
  private String[] p;
  @com.google.gson.a.a
  @c(a="v_16")
  private String[] q;
  @com.google.gson.a.a
  @c(a="v_17")
  private String[] r;
  @com.google.gson.a.a
  @c(a="v_18")
  private String[] s;
  @com.google.gson.a.a
  @c(a="v_19")
  private String[] t;
  @com.google.gson.a.a
  @c(a="v_20")
  private String[] u;
  @com.google.gson.a.a
  @c(a="v_21")
  private String[] v;
  @com.google.gson.a.a
  @c(a="v_22")
  private String[] w;
  @com.google.gson.a.a
  @c(a="v_23")
  private String[] x;
  @com.google.gson.a.a
  @c(a="v_24")
  private HashMap<String, String[]> y;
  @com.google.gson.a.a
  @c(a="v_25")
  private String[] z;
  
  private static Calendar a(Calendar paramCalendar)
  {
    paramCalendar.set(11, 0);
    paramCalendar.set(12, 0);
    paramCalendar.set(13, 0);
    paramCalendar.set(14, 0);
    return paramCalendar;
  }
  
  private void a(String paramString, HashMap<Integer, a> paramHashMap, ArrayList<k> paramArrayList)
  {
    int i1 = 1;
    while (i1 < paramArrayList.size())
    {
      Object localObject5 = (k)paramArrayList.get(i1 - 1);
      Object localObject4 = (k)paramArrayList.get(i1);
      a locala1 = (a)paramHashMap.get(Integer.valueOf(w));
      a locala2 = (a)paramHashMap.get(Integer.valueOf(w));
      Object localObject2 = locala2.d();
      Object localObject1 = localObject2;
      if (i != 1)
      {
        localObject1 = localObject2;
        if (i == 1) {
          localObject1 = locala1.d();
        }
      }
      Object localObject3 = locala2.d();
      localObject2 = localObject3;
      if (i != 2)
      {
        localObject2 = localObject3;
        if (i == 2) {
          localObject2 = locala1.d();
        }
      }
      localObject3 = (m)localObject5;
      localObject4 = (m)localObject4;
      localObject5 = new StringBuilder();
      ((StringBuilder)localObject5).append(paramString);
      ((StringBuilder)localObject5).append("_");
      ((StringBuilder)localObject5).append((String)localObject1);
      ((StringBuilder)localObject5).append("_");
      ((StringBuilder)localObject5).append((String)localObject2);
      localObject1 = ((StringBuilder)localObject5).toString();
      if ((i != i) && (!TextUtils.equals(locala1.d(), locala2.d())) && (i != null) && (i.a > 0.0D) && (i != null) && (i.a > 0.0D) && (Math.abs(i.a - i.a - f) < 1.0D)) {
        if (E.containsKey(localObject1))
        {
          int i2 = ((Integer)E.get(localObject1)).intValue();
          E.put(localObject1, Integer.valueOf(i2 + 1));
        }
        else
        {
          E.put(localObject1, Integer.valueOf(1));
        }
      }
      i1 += 1;
    }
  }
  
  private static int[] a(List<Integer> paramList)
  {
    int[] arrayOfInt = new int[paramList.size()];
    int i1 = 0;
    while (i1 < arrayOfInt.length)
    {
      arrayOfInt[i1] = ((Integer)paramList.get(i1)).intValue();
      i1 += 1;
    }
    return arrayOfInt;
  }
  
  private static Calendar b(Calendar paramCalendar)
  {
    paramCalendar.set(11, 23);
    paramCalendar.set(12, 59);
    paramCalendar.set(13, 59);
    paramCalendar.set(14, 999);
    return paramCalendar;
  }
  
  public final io.reactivex.k<String> a(Context paramContext)
  {
    return io.reactivex.k.a(Arrays.asList(new io.reactivex.k[] { io.reactivex.k.a(new -..Lambda.j.qvd7RHnHbSRfhbl5cLSIPFm7NRc(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.ZV_EFkwBPra5lghqf3F7YwIshiY(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.QyN0J46Jx5vhMPwPJsGMo5URlJs(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.P4k9GE_rwPOcIP2QMgAm2a4bsFs(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.bPJCmj6Jeu8vOba70vzZJbYyIxA(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.JturQhz75MLkIChgwNsk6S6ny1Q(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.tkAIJ6olByMr1Sg64Tb12K1NGgc(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.p6riCNkt7WokzvpHKJ_v-6lqN9Y(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.1pskZWBUm4W-f_RHPZ7LRr_ilxo(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.cTYFocjl_m1UgDvSDFQUAnQt99E(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.69ddauwtav6x6cYSiFvSDY81m9w(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.YmagHgIZryxITzaVbvL8pEcUU1o(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.xkxlCrZB-JDRHr1PcMWaoF7zjiY(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.M07sWhhSAZfUFGEGMDlrp7lBRBo(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.lrWGjwRs76uagLiu-IWANY1OADA(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.PHj5ag8MkxEWcfuqLEXwIO_g_ws(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.cJYovwWf4hQS50ZMAx8Wps3-XdU(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.sXZEzNXd06xlhtkMM7hvJmMtf04(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.htoaonATVf4eWD-X4Iuzw06V0-o(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.aYZLvOKT9gZuj0dOq-Vs3ZZPdcM(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.g1BoMh6qnqNbN8pnT535xQFcuI0(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.BWD-YMepf65sAsnONrnKbKtpE-w(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.9X_Z8vdoYoP5otiXw3Oglzvs61A(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.6MuyvFjH0KMIbhUHzJESJEtFtKw(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.aDSUuvfJX1Ij4c0Ie0At39yTa58(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.48t0dvjTZR4CB359DxfdqGSKu14(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.Cv9lZgNO1yyp3WIYpAXUBadMzz4(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.n2-BMuJDE3dYdY-qrtT5u36vSxo(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.Ds8J7CNDrPFUXVN_1BiJMQCP1ig(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.GMts-r4630U19uwh3hL0RgzLDik(this, paramContext)), io.reactivex.k.a(new -..Lambda.j.sC-L9kxc2JrS2O-Fu9Sjv40WjCk(this, paramContext)) }), -..Lambda.j.7P9x_y2mMlatVQdvVdI8V3Q7vl8.INSTANCE).b(io.reactivex.g.a.c());
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.a.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */