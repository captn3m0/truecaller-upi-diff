package com.daamitt.prime.sdk.a;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashMap;

public class a
{
  private static final String t = "a";
  public int a;
  public String b;
  public String c;
  public String d;
  public String e;
  public String f;
  public String g;
  public String h;
  public int i;
  public long j;
  public boolean k;
  public int l;
  public b m;
  public int n = -1;
  public boolean o = false;
  public int p;
  public int q;
  public int r = -1;
  public boolean s = false;
  private HashMap<String, ArrayList<k>> u;
  private int v;
  private HashMap<Long, Double> w;
  private HashMap<Long, Integer> x;
  private HashMap<Long, String> y;
  
  private a() {}
  
  public a(String paramString1, String paramString2, int paramInt)
  {
    u = new HashMap();
    w = new HashMap();
    y = new HashMap();
    x = new HashMap();
    d = paramString1;
    f = paramString2;
    i = 99;
    v = 1;
    i = paramInt;
    a = -1;
    l = 0;
    k = true;
    r = -1;
  }
  
  public static int a(String paramString)
  {
    if (paramString.equalsIgnoreCase("debit_card")) {
      return 1;
    }
    if (paramString.equalsIgnoreCase("bank")) {
      return 2;
    }
    if (paramString.equalsIgnoreCase("credit_card")) {
      return 3;
    }
    if (paramString.equalsIgnoreCase("bill_pay")) {
      return 4;
    }
    if (paramString.equalsIgnoreCase("phone")) {
      return 5;
    }
    if (paramString.equalsIgnoreCase("generic")) {
      return 9;
    }
    if (paramString.equalsIgnoreCase("filter")) {
      return 10;
    }
    if (paramString.equalsIgnoreCase("placeholder")) {
      return 6;
    }
    if (paramString.equalsIgnoreCase("unknown")) {
      return 99;
    }
    if (paramString.equalsIgnoreCase("ignore")) {
      return 9999;
    }
    if (paramString.equalsIgnoreCase("prepaid")) {
      return 17;
    }
    if (paramString.equalsIgnoreCase("prepaid_dth")) {
      return 18;
    }
    if (paramString.equalsIgnoreCase("electricity")) {
      return 19;
    }
    if (paramString.equalsIgnoreCase("insurance")) {
      return 20;
    }
    if (paramString.equalsIgnoreCase("loan")) {
      return 21;
    }
    if (paramString.equalsIgnoreCase("gas")) {
      return 22;
    }
    return 99;
  }
  
  public static String a(int paramInt)
  {
    if (paramInt != 3) {
      return "";
    }
    return "credit";
  }
  
  public final String a()
  {
    if (TextUtils.isEmpty(e)) {
      return d;
    }
    return e;
  }
  
  public final String b()
  {
    String str = a().trim();
    Object localObject = str;
    if (!"unknown".equalsIgnoreCase(c()))
    {
      localObject = str;
      if (!TextUtils.isEmpty(c()))
      {
        localObject = new StringBuilder();
        ((StringBuilder)localObject).append(str);
        ((StringBuilder)localObject).append(" - ");
        ((StringBuilder)localObject).append(c());
        localObject = ((StringBuilder)localObject).toString();
      }
    }
    return (String)localObject;
  }
  
  public final String c()
  {
    if (TextUtils.isEmpty(g)) {
      return f;
    }
    return g;
  }
  
  public final String d()
  {
    String str1;
    if (TextUtils.isEmpty(g)) {
      str1 = f;
    } else {
      str1 = g;
    }
    String str2 = str1;
    if (str1.length() > 3) {
      str2 = str1.substring(str1.length() - 3);
    }
    return str2;
  }
  
  public final boolean e()
  {
    return (l & 0x2) == 2;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("[");
    localStringBuilder.append(d);
    localStringBuilder.append("] [");
    localStringBuilder.append(f);
    localStringBuilder.append("] type ");
    localStringBuilder.append(i);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */