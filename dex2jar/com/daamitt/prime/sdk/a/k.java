package com.daamitt.prime.sdk.a;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class k
  implements Parcelable
{
  public static final Parcelable.Creator<k> CREATOR = new Parcelable.Creator() {};
  private static final String a = "k";
  public String A;
  public String B;
  public long C;
  public Location D;
  public boolean E;
  public h F;
  public int G;
  public String H;
  public String I;
  public c J;
  public boolean K;
  public boolean L = true;
  public String M;
  public double N;
  public int O;
  public int P;
  public int Q;
  public String R;
  public double S = Double.MIN_VALUE;
  public long o;
  public String p;
  public String q;
  public Date r;
  public String s;
  String t;
  public int u;
  public int v;
  public int w;
  public int x;
  public String y;
  public boolean z = true;
  
  public k() {}
  
  public k(Parcel paramParcel)
  {
    o = paramParcel.readLong();
    C = paramParcel.readLong();
    p = paramParcel.readString();
    q = paramParcel.readString();
    r = new Date(paramParcel.readLong());
    D = new Location("wLocation");
    D.setLatitude(paramParcel.readDouble());
    D.setLongitude(paramParcel.readDouble());
    D.setAccuracy(paramParcel.readFloat());
    M = paramParcel.readString();
    N = paramParcel.readDouble();
    O = paramParcel.readInt();
    P = paramParcel.readInt();
    Q = paramParcel.readInt();
    S = paramParcel.readDouble();
  }
  
  public k(String paramString1, String paramString2, Date paramDate)
  {
    p = paramString1;
    q = paramString2;
    r = paramDate;
    C = -1L;
    o = -1L;
    u = 99;
    v = 99;
    w = -1;
    E = false;
  }
  
  public final ArrayList<c.a> a()
  {
    c localc = J;
    if (localc != null) {
      return a;
    }
    return null;
  }
  
  public final void a(double paramDouble)
  {
    N = paramDouble;
    G |= 0x10;
  }
  
  public final void a(String paramString1, String paramString2)
  {
    s = paramString1;
    t = paramString2;
  }
  
  public final boolean b()
  {
    Object localObject = J;
    if (localObject != null)
    {
      localObject = c;
      if (localObject != null)
      {
        localObject = b.iterator();
        while (((Iterator)localObject).hasNext())
        {
          c.a locala = (c.a)((Iterator)localObject).next();
          if (f) {
            return f;
          }
        }
      }
    }
    return false;
  }
  
  public final String c(String paramString)
  {
    Object localObject = paramString.replaceAll("\\+", "").toCharArray();
    paramString = "(?i)\\b(";
    int i = 0;
    while (i < localObject.length)
    {
      StringBuilder localStringBuilder;
      if (i < localObject.length - 1)
      {
        localStringBuilder = new StringBuilder();
        localStringBuilder.append(paramString);
        localStringBuilder.append(localObject[i]);
        localStringBuilder.append("[a-z]*?\\s?");
        paramString = localStringBuilder.toString();
      }
      else
      {
        localStringBuilder = new StringBuilder();
        localStringBuilder.append(paramString);
        localStringBuilder.append(localObject[i]);
        localStringBuilder.append("[a-z]*");
        paramString = localStringBuilder.toString();
      }
      i += 1;
    }
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append(paramString);
    ((StringBuilder)localObject).append(")");
    paramString = Pattern.compile(((StringBuilder)localObject).toString()).matcher(q);
    if (paramString.find())
    {
      if (paramString.group(1).length() <= 20) {
        return paramString.group(1);
      }
      return null;
    }
    return null;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder1 = new StringBuilder();
    StringBuilder localStringBuilder2 = new StringBuilder("\nnumber :");
    localStringBuilder2.append(p);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\ndate :");
    localStringBuilder2.append(r);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\ncategory :");
    localStringBuilder2.append(s);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\nsubCategory :");
    localStringBuilder2.append(t);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\nsmsType :");
    localStringBuilder2.append(u);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\naccountType :");
    localStringBuilder2.append(v);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\naccountId :");
    localStringBuilder2.append(w);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\nparsed :");
    localStringBuilder2.append(E);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\nUUID :");
    localStringBuilder2.append(H);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\nURI :");
    localStringBuilder2.append(M);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\nprobability :");
    localStringBuilder2.append(N);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\nbody :");
    localStringBuilder2.append(q);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\nuri :");
    localStringBuilder2.append(M);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\nthreadID :");
    localStringBuilder2.append(Q);
    localStringBuilder1.append(localStringBuilder2.toString());
    return localStringBuilder1.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeLong(o);
    paramParcel.writeLong(C);
    paramParcel.writeString(p);
    paramParcel.writeString(q);
    paramParcel.writeLong(r.getTime());
    Location localLocation = D;
    if (localLocation != null)
    {
      paramParcel.writeDouble(localLocation.getLatitude());
      paramParcel.writeDouble(D.getLongitude());
      paramParcel.writeFloat(D.getAccuracy());
    }
    else
    {
      paramParcel.writeDouble(-1.0D);
      paramParcel.writeDouble(-1.0D);
      paramParcel.writeFloat(-1.0F);
    }
    paramParcel.writeString(M);
    paramParcel.writeDouble(N);
    paramParcel.writeInt(O);
    paramParcel.writeInt(P);
    paramParcel.writeInt(Q);
    paramParcel.writeDouble(S);
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.a.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */