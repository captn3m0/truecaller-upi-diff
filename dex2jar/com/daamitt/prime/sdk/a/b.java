package com.daamitt.prime.sdk.a;

import java.util.Date;

public final class b
{
  public double a;
  public double b;
  public Date c;
  public Date d;
  
  public static boolean a(Date paramDate1, Date paramDate2)
  {
    if ((paramDate1 != null) && (paramDate2 != null))
    {
      if (paramDate1.equals(paramDate2)) {
        return true;
      }
      return paramDate1.after(paramDate2);
    }
    return true;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("AccountBalance{balance=");
    localStringBuilder.append(a);
    localStringBuilder.append(", outstandingBalance=");
    localStringBuilder.append(b);
    localStringBuilder.append(", lastBalSyncdate=");
    localStringBuilder.append(c);
    localStringBuilder.append(", lastOutbalSyncdate=");
    localStringBuilder.append(d);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */