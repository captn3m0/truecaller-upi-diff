package com.daamitt.prime.sdk.a;

import java.util.Date;

public class l
  extends k
{
  public static final String a = "l";
  public String b;
  public String c;
  public double d;
  public double e = -1.0D;
  public double f;
  public Date g;
  public Date h;
  public int i;
  public int j;
  public String k;
  public int l;
  public String m;
  public m n = null;
  
  public l(String paramString1, String paramString2, Date paramDate)
  {
    super(paramString1, paramString2, paramDate);
  }
  
  public static int a(String paramString)
  {
    if (paramString.equalsIgnoreCase("bill")) {
      return 1;
    }
    if (paramString.equalsIgnoreCase("balance")) {
      return 2;
    }
    if (paramString.equalsIgnoreCase("credit_card_bill")) {
      return 3;
    }
    if (paramString.equalsIgnoreCase("mobile_bill")) {
      return 4;
    }
    if (paramString.equalsIgnoreCase("dth_recharge")) {
      return 10;
    }
    if (paramString.equalsIgnoreCase("electricity_bill")) {
      return 11;
    }
    if (paramString.equalsIgnoreCase("insurance_premium")) {
      return 12;
    }
    if (paramString.equalsIgnoreCase("loan_emi")) {
      return 13;
    }
    if (paramString.equalsIgnoreCase("internet_bill")) {
      return 14;
    }
    if (paramString.equalsIgnoreCase("gas_bill")) {
      return 15;
    }
    if (paramString.equalsIgnoreCase("default")) {
      return 9;
    }
    return 9;
  }
  
  public static String a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return "";
    case 4: 
      return "";
    }
    return "credit";
  }
  
  public final void a(String paramString, double paramDouble, Date paramDate, int paramInt)
  {
    b = paramString;
    d = paramDouble;
    g = paramDate;
    i = paramInt;
  }
  
  public final String b(String paramString)
  {
    switch (paramString.hashCode())
    {
    default: 
      break;
    case 3076014: 
      if (paramString.equals("date")) {
        i1 = 3;
      }
      break;
    case 110749: 
      if (paramString.equals("pan")) {
        i1 = 1;
      }
      break;
    case -102973965: 
      if (paramString.equals("sms_time")) {
        i1 = 4;
      }
      break;
    case -264918006: 
      if (paramString.equals("statement_type")) {
        i1 = 2;
      }
      break;
    case -1413853096: 
      if (paramString.equals("amount")) {
        i1 = 0;
      }
      break;
    }
    int i1 = -1;
    switch (i1)
    {
    default: 
      return null;
    case 4: 
      return String.valueOf(r.getTime());
    case 3: 
      return String.valueOf(g.getTime());
    case 2: 
      return String.valueOf(i);
    case 1: 
      return String.valueOf(b);
    }
    return String.valueOf(d);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder1 = new StringBuilder();
    localStringBuilder1.append(super.toString());
    StringBuilder localStringBuilder2 = new StringBuilder("\npan : ");
    localStringBuilder2.append(b);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\nstmtType : ");
    localStringBuilder2.append(i);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\namount : ");
    localStringBuilder2.append(d);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\nminDueAmount : ");
    localStringBuilder2.append(e);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\nDueDate : ");
    localStringBuilder2.append(g);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\nflags : ");
    localStringBuilder2.append(j);
    localStringBuilder1.append(localStringBuilder2.toString());
    return localStringBuilder1.toString();
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.a.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */