package com.daamitt.prime.sdk.a;

import c.g.b.k;

public final class g$a<T>
  extends g<T>
{
  final T a;
  
  public g$a(T paramT)
  {
    super((byte)0);
    a = paramT;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject) {
      if ((paramObject instanceof a))
      {
        paramObject = (a)paramObject;
        if (k.a(a, a)) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    Object localObject = a;
    if (localObject != null) {
      return localObject.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("Content(data=");
    localStringBuilder.append(a);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.a.g.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */