package com.daamitt.prime.sdk.a;

import android.text.TextUtils;
import java.util.Calendar;

public final class i
{
  public static String a(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return "";
    }
    StringBuilder localStringBuilder = new StringBuilder();
    int j = paramString.length();
    int i = 0;
    while (i < j)
    {
      char c = paramString.charAt(i);
      int k = Character.digit(c, 10);
      if (k != -1) {
        localStringBuilder.append(k);
      } else if ((localStringBuilder.length() == 0) && (c == '+')) {
        localStringBuilder.append(c);
      }
      i += 1;
    }
    return localStringBuilder.toString();
  }
  
  public static final class a
  {
    public static int a(Calendar paramCalendar1, Calendar paramCalendar2)
    {
      Calendar localCalendar = Calendar.getInstance();
      localCalendar.setTimeInMillis(paramCalendar1.getTimeInMillis());
      int i = 0;
      localCalendar.set(11, 0);
      localCalendar.set(12, 0);
      localCalendar.set(13, 0);
      localCalendar.set(14, 0);
      paramCalendar1 = Calendar.getInstance();
      paramCalendar1.setTimeInMillis(paramCalendar2.getTimeInMillis());
      paramCalendar1.set(11, 23);
      paramCalendar1.set(12, 59);
      paramCalendar1.set(13, 59);
      paramCalendar1.set(14, 999);
      while (localCalendar.getTimeInMillis() < paramCalendar1.getTimeInMillis())
      {
        localCalendar.add(6, 1);
        i += 1;
      }
      return i;
    }
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.a.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */