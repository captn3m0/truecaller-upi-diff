package com.daamitt.prime.sdk.a;

import android.location.Location;
import java.util.Date;

public class m
  extends k
{
  public static final String a = "m";
  public String T;
  public String U;
  public String V;
  public String W;
  public Location X;
  public int Y;
  public String Z;
  public String aa;
  public String ab;
  public boolean ac = false;
  public Date ad;
  public String b;
  public String c;
  public String d;
  public String e;
  public double f;
  public double g;
  public double h;
  public b i;
  public int j;
  public long k;
  public String l;
  public String m;
  public String n;
  
  public m(String paramString1, String paramString2, Date paramDate)
  {
    super(paramString1, paramString2, paramDate);
  }
  
  public static int a(String paramString)
  {
    if (paramString == null) {
      return 9;
    }
    if (paramString.equalsIgnoreCase("credit_card")) {
      return 1;
    }
    if (paramString.equalsIgnoreCase("debit_card")) {
      return 2;
    }
    if (paramString.equalsIgnoreCase("debit_atm")) {
      return 3;
    }
    if (paramString.equalsIgnoreCase("net_banking")) {
      return 4;
    }
    if (paramString.equalsIgnoreCase("bill_pay")) {
      return 5;
    }
    if (paramString.equalsIgnoreCase("ecs")) {
      return 6;
    }
    if (paramString.equalsIgnoreCase("cheque")) {
      return 10;
    }
    if (paramString.equalsIgnoreCase("default")) {
      return 9;
    }
    if (paramString.equalsIgnoreCase("balance")) {
      return 12;
    }
    if (paramString.equalsIgnoreCase("credit")) {
      return 17;
    }
    if (paramString.equalsIgnoreCase("credit_atm")) {
      return 15;
    }
    if (paramString.equalsIgnoreCase("debit_prepaid")) {
      return 16;
    }
    if (paramString.equalsIgnoreCase("upi")) {
      return 18;
    }
    return 9;
  }
  
  public static String a(int paramInt1, int paramInt2)
  {
    if (paramInt1 != 10)
    {
      if (paramInt1 != 15)
      {
        if (paramInt1 != 18)
        {
          switch (paramInt1)
          {
          default: 
            return "";
          case 6: 
            return "";
          case 5: 
            return "";
          case 4: 
            return "";
          case 3: 
            if (paramInt2 == 2) {
              return "";
            }
            return "debit";
          case 2: 
            return "debit";
          }
          return "credit";
        }
        return "";
      }
      return "credit";
    }
    return "";
  }
  
  public static boolean a(int paramInt)
  {
    return (paramInt == 5) || (paramInt == 6);
  }
  
  public static boolean b(int paramInt)
  {
    return (paramInt == 4) || (paramInt == 5) || (paramInt == 10) || (paramInt == 9) || (paramInt == 13);
  }
  
  public static int[] c()
  {
    return new int[] { 2, 3, 4, 5, 6, 9, 10, 12 };
  }
  
  public static String d()
  {
    return "1 , 2 , 3 , 4 , 5 , 6 , 7 , 8, 9 , 14 , 16 , 10 , 13 , 15 , 18";
  }
  
  public static String e()
  {
    return "1 , 2 , 3 , 4 , 5 , 6 , 7 , 8, 9 , 14 , 16 , 10 , 13 , 15 , 12 , 17 , 18";
  }
  
  public static String f()
  {
    return "2 , 3 , 4 , 5 , 6 , 10 , 12 , 17";
  }
  
  public static String g()
  {
    return "3 , 15";
  }
  
  public static String h()
  {
    return "4";
  }
  
  public static String i()
  {
    return "17";
  }
  
  public final void a(String paramString1, Double paramDouble, Date paramDate, String paramString2, int paramInt)
  {
    f = paramDouble.doubleValue();
    ad = paramDate;
    j = paramInt;
    b = paramString1;
    c = paramString2;
    k = -1L;
    T = "other";
  }
  
  public final String b(String paramString)
  {
    switch (paramString.hashCode())
    {
    default: 
      break;
    case 509054971: 
      if (paramString.equals("transaction_type")) {
        i1 = 4;
      }
      break;
    case 3387378: 
      if (paramString.equals("note")) {
        i1 = 1;
      }
      break;
    case 3076014: 
      if (paramString.equals("date")) {
        i1 = 5;
      }
      break;
    case 111188: 
      if (paramString.equals("pos")) {
        i1 = 2;
      }
      break;
    case 110749: 
      if (paramString.equals("pan")) {
        i1 = 3;
      }
      break;
    case -1413853096: 
      if (paramString.equals("amount")) {
        i1 = 0;
      }
      break;
    }
    int i1 = -1;
    switch (i1)
    {
    default: 
      return null;
    case 5: 
      return String.valueOf(ad.getTime());
    case 4: 
      return String.valueOf(j);
    case 3: 
      return String.valueOf(b);
    case 2: 
      return c;
    case 1: 
      return V;
    }
    return String.valueOf(f);
  }
  
  public final void j()
  {
    Y |= 0x1;
  }
  
  public final boolean k()
  {
    return (Y & 0x1) != 1;
  }
  
  public final boolean l()
  {
    return (Y & 0x4) == 4;
  }
  
  public final void m()
  {
    Y |= 0x20;
  }
  
  public final void n()
  {
    Y &= 0xFFFFFFDF;
  }
  
  public final void o()
  {
    Y |= 0x80;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder1 = new StringBuilder();
    localStringBuilder1.append(super.toString());
    StringBuilder localStringBuilder2 = new StringBuilder("\n_id : ");
    localStringBuilder2.append(o);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\npan : ");
    localStringBuilder2.append(b);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\npos : ");
    localStringBuilder2.append(c);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\namount : ");
    localStringBuilder2.append(f);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\ntxnType : ");
    localStringBuilder2.append(j);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\ntxnCategories : ");
    localStringBuilder2.append(T);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\ntxnTags : ");
    localStringBuilder2.append(U);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\nplaceId : ");
    localStringBuilder2.append(l);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\nplaceName : ");
    localStringBuilder2.append(m);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\nbal : ");
    localStringBuilder2.append(i);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\ndisplayPan : ");
    localStringBuilder2.append(d);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\naccountDisplayName : ");
    localStringBuilder2.append(y);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\naccountType : ");
    localStringBuilder2.append(v);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\ntxnDate : ");
    localStringBuilder2.append(ad);
    localStringBuilder1.append(localStringBuilder2.toString());
    localStringBuilder2 = new StringBuilder("\ndate : ");
    localStringBuilder2.append(r);
    localStringBuilder1.append(localStringBuilder2.toString());
    return localStringBuilder1.toString();
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.a.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */