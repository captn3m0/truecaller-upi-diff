package com.daamitt.prime.sdk.a;

import java.util.Date;

public class d
  extends k
{
  public static final String a = "d";
  public String b;
  public String c;
  public Date d;
  public int e;
  public String f;
  public String g;
  public int h;
  public double i;
  public String j;
  public boolean k;
  public boolean l;
  public long m;
  public String n;
  
  public d(String paramString1, String paramString2, Date paramDate)
  {
    super(paramString1, paramString2, paramDate);
  }
  
  public static int a(String paramString)
  {
    if (paramString.equalsIgnoreCase("movie")) {
      return 1;
    }
    if (paramString.equalsIgnoreCase("taxi")) {
      return 2;
    }
    if (paramString.equalsIgnoreCase("flight")) {
      return 3;
    }
    if (paramString.equalsIgnoreCase("shipment")) {
      return 4;
    }
    if (paramString.equalsIgnoreCase("train")) {
      return 5;
    }
    if (paramString.equalsIgnoreCase("default")) {
      return 9;
    }
    return 9;
  }
  
  public final void a(String paramString1, String paramString2, Date paramDate, int paramInt)
  {
    b = paramString1;
    c = paramString2;
    d = paramDate;
    e = paramInt;
  }
  
  public final String b(String paramString)
  {
    switch (paramString.hashCode())
    {
    default: 
      break;
    case 984038195: 
      if (paramString.equals("event_info")) {
        i1 = 2;
      }
      break;
    case 951526432: 
      if (paramString.equals("contact")) {
        i1 = 1;
      }
      break;
    case 943500218: 
      if (paramString.equals("event_location")) {
        i1 = 3;
      }
      break;
    case 3076014: 
      if (paramString.equals("date")) {
        i1 = 5;
      }
      break;
    case 111156: 
      if (paramString.equals("pnr")) {
        i1 = 4;
      }
      break;
    case -102973965: 
      if (paramString.equals("sms_time")) {
        i1 = 6;
      }
      break;
    case -1413853096: 
      if (paramString.equals("amount")) {
        i1 = 0;
      }
      break;
    }
    int i1 = -1;
    switch (i1)
    {
    default: 
      return null;
    case 6: 
      return String.valueOf(r.getTime());
    case 5: 
      return String.valueOf(d.getTime());
    case 4: 
      return c;
    case 3: 
      return f;
    case 2: 
      return g;
    case 1: 
      return j;
    }
    return String.valueOf(i);
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.a.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */