package com.daamitt.prime.sdk.a;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.Telephony.Sms.Inbox;
import com.google.gson.a.c;
import io.reactivex.k;
import java.util.ArrayList;
import java.util.Arrays;

public class f
{
  private static final String a = "f";
  @com.google.gson.a.a
  @c(a="v_00")
  private int[] b;
  @com.google.gson.a.a
  @c(a="v_01")
  private int[] c;
  @com.google.gson.a.a
  @c(a="v_02")
  private int[] d;
  @com.google.gson.a.a
  @c(a="v_04")
  private int[] e;
  @com.google.gson.a.a
  @c(a="v_05")
  private int[] f;
  @com.google.gson.a.a
  @c(a="v_06")
  private int[] g;
  @com.google.gson.a.a
  @c(a="v_07")
  private int[] h;
  @com.google.gson.a.a
  @c(a="v_08")
  private int[] i;
  @com.google.gson.a.a
  @c(a="v_09")
  private int[] j;
  @com.google.gson.a.a
  @c(a="v_10")
  private int[] k;
  @com.google.gson.a.a
  @c(a="v_11")
  private int[] l;
  @com.google.gson.a.a
  @c(a="v_012")
  private int[] m;
  @com.google.gson.a.a
  @c(a="v_13")
  private int[] n;
  @com.google.gson.a.a
  @c(a="v_14")
  private int[] o;
  @com.google.gson.a.a
  @c(a="v_15")
  private int[] p;
  @com.google.gson.a.a
  @c(a="v_16")
  private int[] q;
  @com.google.gson.a.a
  @c(a="v_17")
  private int[] r;
  @com.google.gson.a.a
  @c(a="v_18")
  private int[] s;
  
  private static int[] a(Context paramContext, String paramString)
  {
    try
    {
      paramContext = paramContext.getContentResolver().query(Telephony.Sms.Inbox.CONTENT_URI, new String[] { "(case when datetime(date/1000, 'unixepoch')>=datetime('now','-1 months') then 1\nwhen datetime(date/1000, 'unixepoch')>=datetime('now','-2 months') then 2\nwhen datetime(date/1000, 'unixepoch')>=datetime('now','-3 months') then 3\nwhen datetime(date/1000, 'unixepoch')>=datetime('now','-4 months') then 4\nwhen datetime(date/1000, 'unixepoch')>=datetime('now','-5 months') then 5\nwhen datetime(date/1000, 'unixepoch')>=datetime('now','-6 months') then 6 end) as month " }, paramString, null, null);
      paramString = new int[6];
      int i1 = 0;
      while (i1 < 6)
      {
        paramString[i1] = 0;
        i1 += 1;
      }
      if (paramContext != null)
      {
        paramContext.moveToFirst();
        while (!paramContext.isAfterLast())
        {
          i1 = paramContext.getInt(0);
          if ((i1 > 0) && (i1 < 7))
          {
            i1 -= 1;
            paramString[i1] += 1;
          }
          paramContext.moveToNext();
        }
        paramContext.close();
      }
      return paramString;
    }
    catch (SecurityException paramContext)
    {
      throw paramContext;
    }
  }
  
  private static int[] a(Context paramContext, ArrayList<String> paramArrayList1, ArrayList<String> paramArrayList2)
  {
    Object localObject = new StringBuilder("datetime(date/1000, 'unixepoch') >= datetime('now','-6 months') AND (");
    int i1 = 0;
    while (i1 < paramArrayList1.size())
    {
      if (i1 != 0) {
        ((StringBuilder)localObject).append(" OR ");
      }
      ((StringBuilder)localObject).append("body LIKE ? ");
      i1 += 1;
    }
    if (paramArrayList2 != null)
    {
      i1 = 0;
      while (i1 < paramArrayList1.size())
      {
        ((StringBuilder)localObject).append(" OR ");
        ((StringBuilder)localObject).append("lower(body) LIKE ? ");
        i1 += 1;
      }
      paramArrayList1.addAll(paramArrayList2);
    }
    ((StringBuilder)localObject).append(")");
    try
    {
      paramContext = paramContext.getContentResolver();
      paramArrayList2 = Telephony.Sms.Inbox.CONTENT_URI;
      localObject = ((StringBuilder)localObject).toString();
      paramArrayList1 = (String[])paramArrayList1.toArray(new String[paramArrayList1.size()]);
      paramContext = paramContext.query(paramArrayList2, new String[] { "(case when datetime(date/1000, 'unixepoch')>=datetime('now','-1 months') then 1\nwhen datetime(date/1000, 'unixepoch')>=datetime('now','-2 months') then 2\nwhen datetime(date/1000, 'unixepoch')>=datetime('now','-3 months') then 3\nwhen datetime(date/1000, 'unixepoch')>=datetime('now','-4 months') then 4\nwhen datetime(date/1000, 'unixepoch')>=datetime('now','-5 months') then 5\nwhen datetime(date/1000, 'unixepoch')>=datetime('now','-6 months') then 6 end) as month " }, (String)localObject, paramArrayList1, null);
      paramArrayList1 = new int[6];
      i1 = 0;
      while (i1 < 6)
      {
        paramArrayList1[i1] = 0;
        i1 += 1;
      }
      if (paramContext != null)
      {
        paramContext.moveToFirst();
        while (!paramContext.isAfterLast())
        {
          i1 = paramContext.getInt(0);
          if ((i1 > 0) && (i1 < 7))
          {
            i1 -= 1;
            paramArrayList1[i1] += 1;
          }
          paramContext.moveToNext();
        }
        paramContext.close();
      }
      return paramArrayList1;
    }
    catch (SecurityException paramContext)
    {
      throw paramContext;
    }
  }
  
  public final k<String> a(Context paramContext)
  {
    return k.a(Arrays.asList(new k[] { k.a(new -..Lambda.f.SNuiBE0AGXVKBSzoNs3pkECHEd0(this, paramContext)).b(io.reactivex.g.a.b()), k.a(new -..Lambda.f.1bf6vpbldhAt42N4mBoHIfIwcKM(this, paramContext)).b(io.reactivex.g.a.b()), k.a(new -..Lambda.f.pu17R9OnYzRIq_0EDGSaJY2pzAA(this, paramContext)).b(io.reactivex.g.a.b()), k.a(new -..Lambda.f.7Nvqy5_AEmt5LdVUsKs1MCvX1eM(this, paramContext)).b(io.reactivex.g.a.b()), k.a(new -..Lambda.f.L88A9kDX6PaKjyUYyvMVI_XDVAs(this, paramContext)).b(io.reactivex.g.a.b()), k.a(new -..Lambda.f.gn7BXQb1Jj8EKWX77PmqCX9yDlU(this, paramContext)).b(io.reactivex.g.a.b()), k.a(new -..Lambda.f._gWz0i7SHcqrxgMh7FtVRSGVmRM(this, paramContext)).b(io.reactivex.g.a.b()), k.a(new -..Lambda.f.YWBtGRd7eafWfCfFQQUPY-w3h0I(this, paramContext)).b(io.reactivex.g.a.b()), k.a(new -..Lambda.f.f_PqfwnbqyDucNejEekUFMIESZE(this, paramContext)).b(io.reactivex.g.a.b()), k.a(new -..Lambda.f.FTx5vKJpPhn2kRhhbTyHusSA-Sw(this, paramContext)).b(io.reactivex.g.a.b()), k.a(new -..Lambda.f.uZ05SDtP8pVZpe7JMdtUA5hYyjk(this, paramContext)).b(io.reactivex.g.a.b()), k.a(new -..Lambda.f.Vw3xfRQ8OqSPae9siyqybhfQFFw(this, paramContext)).b(io.reactivex.g.a.b()), k.a(new -..Lambda.f.QJJjl1bzHZ0Diw51I8Kbg97-ITE(this, paramContext)).b(io.reactivex.g.a.b()), k.a(new -..Lambda.f.p2lnPuEExFykEWsEyXcg0ugdN6Y(this, paramContext)).b(io.reactivex.g.a.b()), k.a(new -..Lambda.f.zBB-jZVXXq1ZNZFpF_sI8J1kXFI(this, paramContext)).b(io.reactivex.g.a.b()), k.a(new -..Lambda.f.lIfMgURLBbljPtiftNlwZjkiQNE(this, paramContext)).b(io.reactivex.g.a.b()), k.a(new -..Lambda.f.H6u39WDSRIXTFJFkuf1xYrFWdSg(this, paramContext)).b(io.reactivex.g.a.b()), k.a(new -..Lambda.f.OJ8k2u5ZQZ8PdQtbApPl5vUnw7I(this, paramContext)).b(io.reactivex.g.a.b()) }), -..Lambda.f.pIZjUsvVPdru3kiZ5DD56IUpWfc.INSTANCE).b(io.reactivex.g.a.b());
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.a.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */