package com.a.a;

import com.a.a.a.e;
import java.util.Iterator;

public class y
  extends b
  implements ac, ag, Cloneable
{
  public long[] a;
  protected int b;
  protected int c;
  protected int d;
  protected int e;
  protected boolean f;
  protected double g;
  protected s h;
  
  public y()
  {
    this(4, 0.75D);
  }
  
  public y(int paramInt, double paramDouble)
  {
    this(paramInt, paramDouble, r.d());
  }
  
  private y(int paramInt, double paramDouble, s params)
  {
    h = params;
    q.a(paramDouble);
    g = paramDouble;
    if ((paramInt > e) || (a == null))
    {
      params = a;
      a(q.a(paramInt, g));
      if (params != null)
      {
        if (b() == 0) {
          paramInt = 1;
        } else {
          paramInt = 0;
        }
        if (paramInt == 0) {
          a(params);
        }
      }
    }
  }
  
  private void a(int paramInt)
  {
    if ((!i) && (Integer.bitCount(paramInt) != 1)) {
      throw new AssertionError();
    }
    int j = h.a(paramInt);
    long[] arrayOfLong = a;
    try
    {
      a = new long[paramInt + 1];
      e = q.b(paramInt, g);
      d = j;
      c = (paramInt - 1);
      return;
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      a = arrayOfLong;
      if (a == null) {
        j = 0;
      } else {
        j = b();
      }
      throw new f("Not enough memory to allocate buffers for rehashing: %,d -> %,d", localOutOfMemoryError, new Object[] { Integer.valueOf(j), Integer.valueOf(paramInt) });
    }
  }
  
  private void a(long[] paramArrayOfLong)
  {
    if (!i) {
      q.a(paramArrayOfLong.length - 1);
    }
    long[] arrayOfLong = a;
    int m = c;
    int j = paramArrayOfLong.length - 1;
    for (;;)
    {
      int k = j - 1;
      if (k < 0) {
        break;
      }
      long l = paramArrayOfLong[k];
      j = k;
      if (l != 0L)
      {
        for (j = c(l) & m; arrayOfLong[j] != 0L; j = j + 1 & m) {}
        arrayOfLong[j] = l;
        j = k;
      }
    }
  }
  
  private int c(long paramLong)
  {
    if ((!i) && (paramLong == 0L)) {
      throw new AssertionError();
    }
    return d.a(paramLong, d);
  }
  
  private y c()
  {
    try
    {
      y localy = (y)super.clone();
      a = ((long[])a.clone());
      f = f;
      h = h.a();
      return localy;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      throw new RuntimeException(localCloneNotSupportedException);
    }
  }
  
  public final boolean a(long paramLong)
  {
    if (paramLong == 0L) {
      return f;
    }
    long[] arrayOfLong = a;
    int k = c;
    for (int j = c(paramLong) & k;; j = j + 1 & k)
    {
      long l = arrayOfLong[j];
      if (l == 0L) {
        break;
      }
      if (l == paramLong) {
        return true;
      }
    }
    return false;
  }
  
  public final long[] a()
  {
    long[] arrayOfLong1 = new long[b()];
    boolean bool = f;
    int k = 0;
    int j;
    if (bool)
    {
      arrayOfLong1[0] = 0L;
      j = 1;
    }
    else
    {
      j = 0;
    }
    long[] arrayOfLong2 = a;
    int n = c;
    while (k <= n)
    {
      long l = arrayOfLong2[k];
      int m = j;
      if (l != 0L)
      {
        arrayOfLong1[j] = l;
        m = j + 1;
      }
      k += 1;
      j = m;
    }
    return arrayOfLong1;
  }
  
  public final int b()
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.provideAs(TypeTransformer.java:780)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e1expr(TypeTransformer.java:496)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:713)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e2expr(TypeTransformer.java:632)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:716)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.s1stmt(TypeTransformer.java:810)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.sxStmt(TypeTransformer.java:840)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:206)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
  }
  
  public final boolean b(long paramLong)
  {
    if (paramLong == 0L)
    {
      if ((!i) && (a[(c + 1)] != 0L)) {
        throw new AssertionError();
      }
      boolean bool = f;
      f = true;
      return bool ^ true;
    }
    long[] arrayOfLong = a;
    int k = c;
    for (int j = c(paramLong) & k;; j = j + 1 & k)
    {
      long l = arrayOfLong[j];
      if (l == 0L) {
        break;
      }
      if (l == paramLong) {
        return false;
      }
    }
    k = b;
    int m = e;
    if (k == m)
    {
      if ((!i) && ((k != m) || (a[j] != 0L) || (paramLong == 0L))) {
        throw new AssertionError();
      }
      arrayOfLong = a;
      a(q.a(c + 1, b(), g));
      if ((!i) && (a.length <= arrayOfLong.length)) {
        throw new AssertionError();
      }
      arrayOfLong[j] = paramLong;
      a(arrayOfLong);
    }
    else
    {
      arrayOfLong[j] = paramLong;
    }
    b += 1;
    return true;
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject != null) && (getClass() == paramObject.getClass()))
    {
      paramObject = (ag)getClass().cast(paramObject);
      int j;
      if (((ag)paramObject).b() != b())
      {
        j = 0;
      }
      else
      {
        paramObject = ((ag)paramObject).iterator();
        while (((Iterator)paramObject).hasNext()) {
          if (!a(nextb))
          {
            j = 0;
            break label87;
          }
        }
        j = 1;
      }
      label87:
      if (j != 0) {
        return true;
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    int j;
    if (f) {
      j = -559038737;
    } else {
      j = 0;
    }
    long[] arrayOfLong = a;
    int k = c;
    while (k >= 0)
    {
      long l = arrayOfLong[k];
      int m = j;
      if (l != 0L) {
        m = j + (int)d.a(l);
      }
      k -= 1;
      j = m;
    }
    return j;
  }
  
  public Iterator<e> iterator()
  {
    return new a();
  }
  
  protected final class a
    extends a<e>
  {
    private final e c = new e();
    private final int d = c + 1;
    private int e = -1;
    
    public a() {}
  }
}

/* Location:
 * Qualified Name:     com.a.a.y
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */