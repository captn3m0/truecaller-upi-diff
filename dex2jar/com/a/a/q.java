package com.a.a;

public final class q
{
  static int a(int paramInt, double paramDouble)
  {
    if (paramInt >= 0)
    {
      double d = paramInt;
      Double.isNaN(d);
      long l2 = Math.ceil(d / paramDouble);
      long l1 = l2;
      if (l2 == paramInt) {
        l1 = l2 + 1L;
      }
      l1 -= 1L;
      l1 |= l1 >> 1;
      l1 |= l1 >> 2;
      l1 |= l1 >> 4;
      l1 |= l1 >> 8;
      l1 |= l1 >> 16;
      l1 = Math.max(4L, (l1 | l1 >> 32) + 1L);
      if (l1 <= 1073741824L) {
        return (int)l1;
      }
      throw new f("Maximum array size exceeded for this load factor (elements: %d, load factor: %f)", new Object[] { Integer.valueOf(paramInt), Double.valueOf(paramDouble) });
    }
    throw new IllegalArgumentException("Number of elements must be >= 0: ".concat(String.valueOf(paramInt)));
  }
  
  static int a(int paramInt1, int paramInt2, double paramDouble)
  {
    if (!a) {
      a(paramInt1);
    }
    if (paramInt1 != 1073741824) {
      return paramInt1 << 1;
    }
    throw new f("Maximum array size exceeded for this load factor (elements: %d, load factor: %f)", new Object[] { Integer.valueOf(paramInt2), Double.valueOf(paramDouble) });
  }
  
  static void a(double paramDouble)
  {
    if ((paramDouble >= 0.009999999776482582D) && (paramDouble <= 0.9900000095367432D)) {
      return;
    }
    throw new f("The load factor should be in range [%.2f, %.2f]: %f", new Object[] { Double.valueOf(0.009999999776482582D), Double.valueOf(0.9900000095367432D), Double.valueOf(paramDouble) });
  }
  
  static boolean a(int paramInt)
  {
    if ((!a) && (paramInt <= 1)) {
      throw new AssertionError();
    }
    if (!a)
    {
      if (e.a(paramInt) == paramInt) {
        return true;
      }
      throw new AssertionError();
    }
    return true;
  }
  
  static int b(int paramInt, double paramDouble)
  {
    if (!a) {
      a(paramInt);
    }
    double d = paramInt;
    Double.isNaN(d);
    return Math.min(paramInt - 1, (int)Math.ceil(d * paramDouble));
  }
}

/* Location:
 * Qualified Name:     com.a.a.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */