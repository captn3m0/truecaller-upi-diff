package com.a.a;

import com.a.a.a.g;
import java.util.Iterator;

public abstract interface ad<VType>
  extends Iterable<g<VType>>
{
  public abstract int a();
  
  public abstract boolean a(long paramLong);
  
  public abstract ai<VType> b();
  
  public abstract Iterator<g<VType>> iterator();
}

/* Location:
 * Qualified Name:     com.a.a.ad
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */