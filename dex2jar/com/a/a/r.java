package com.a.a;

import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class r
{
  private static a a;
  private static final s b = new s()
  {
    public final int a(int paramAnonymousInt)
    {
      return d.a(paramAnonymousInt);
    }
    
    public final s a()
    {
      return this;
    }
  };
  
  public static s a()
  {
    return aj.a;
  }
  
  @Deprecated
  public static s b()
  {
    return b;
  }
  
  @Deprecated
  public static s c()
  {
    new s()
    {
      public final int a(int paramAnonymousInt)
      {
        return 0;
      }
      
      public final s a()
      {
        return this;
      }
    };
  }
  
  public static s d()
  {
    if (a == null) {
      try
      {
        String str = (String)AccessController.doPrivileged(new PrivilegedAction() {});
        if (str != null)
        {
          a[] arrayOfa = a.values();
          int j = arrayOfa.length;
          int i = 0;
          while (i < j)
          {
            a locala = arrayOfa[i];
            if (locala.name().equalsIgnoreCase(str))
            {
              a = locala;
              break;
            }
            i += 1;
          }
        }
        try
        {
          s locals = (s)a.call();
          return locals;
        }
        catch (Exception localException)
        {
          throw new RuntimeException(localException);
        }
      }
      catch (SecurityException localSecurityException)
      {
        Logger.getLogger(p.class.getName()).log(Level.INFO, "Failed to read 'tests.seed' property for initial random seed.", localSecurityException);
        if (a == null) {
          a = a.a;
        }
      }
    }
  }
  
  public static abstract enum a
    implements Callable<s>
  {
    private a() {}
  }
}

/* Location:
 * Qualified Name:     com.a.a.r
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */