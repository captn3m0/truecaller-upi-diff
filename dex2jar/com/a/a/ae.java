package com.a.a;

import com.a.a.a.g;
import com.a.a.a.h;
import java.util.Iterator;
import java.util.Objects;

public class ae<VType>
  implements af<VType>, Cloneable
{
  public long[] a;
  public Object[] b;
  protected int c;
  protected int d;
  protected int e;
  protected int f;
  protected boolean g;
  protected double h;
  protected s i;
  
  public ae()
  {
    this(4);
  }
  
  public ae(int paramInt)
  {
    this(paramInt, 0.75D);
  }
  
  public ae(int paramInt, double paramDouble)
  {
    this(paramInt, paramDouble, r.d());
  }
  
  private ae(int paramInt, double paramDouble, s params)
  {
    i = params;
    q.a(paramDouble);
    h = paramDouble;
    if ((paramInt > f) || (a == null))
    {
      params = a;
      Object[] arrayOfObject = (Object[])b;
      a(q.a(paramInt, h));
      if (params != null)
      {
        if (a() == 0) {
          paramInt = 1;
        } else {
          paramInt = 0;
        }
        if (paramInt == 0) {
          a(params, arrayOfObject);
        }
      }
    }
  }
  
  public ae(ad<? extends VType> paramad)
  {
    this(paramad.a());
    paramad = paramad.iterator();
    while (paramad.hasNext())
    {
      g localg = (g)paramad.next();
      a(b, c);
    }
  }
  
  private void a(int paramInt)
  {
    if ((!j) && (Integer.bitCount(paramInt) != 1)) {
      throw new AssertionError();
    }
    int k = i.a(paramInt);
    long[] arrayOfLong = a;
    Object[] arrayOfObject = (Object[])b;
    int m = paramInt + 1;
    try
    {
      a = new long[m];
      b = ((Object[])new Object[m]);
      f = q.b(paramInt, h);
      c = k;
      e = (paramInt - 1);
      return;
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      a = arrayOfLong;
      b = arrayOfObject;
      throw new f("Not enough memory to allocate buffers for rehashing: %,d -> %,d", localOutOfMemoryError, new Object[] { Integer.valueOf(e + 1), Integer.valueOf(paramInt) });
    }
  }
  
  private void a(int paramInt, long paramLong, VType paramVType)
  {
    if ((!j) && ((d != f) || (a[paramInt] != 0L) || (paramLong == 0L))) {
      throw new AssertionError();
    }
    long[] arrayOfLong = a;
    Object[] arrayOfObject = (Object[])b;
    a(q.a(e + 1, a(), h));
    if ((!j) && (a.length <= arrayOfLong.length)) {
      throw new AssertionError();
    }
    arrayOfLong[paramInt] = paramLong;
    arrayOfObject[paramInt] = paramVType;
    a(arrayOfLong, arrayOfObject);
  }
  
  private void a(long[] paramArrayOfLong, VType[] paramArrayOfVType)
  {
    if (!j) {
      if (paramArrayOfLong.length == paramArrayOfVType.length) {
        q.a(paramArrayOfLong.length - 1);
      } else {
        throw new AssertionError();
      }
    }
    long[] arrayOfLong = a;
    Object[] arrayOfObject = (Object[])b;
    int n = e;
    int k = paramArrayOfLong.length - 1;
    arrayOfLong[(arrayOfLong.length - 1)] = paramArrayOfLong[k];
    arrayOfObject[(arrayOfObject.length - 1)] = paramArrayOfVType[k];
    for (;;)
    {
      int m = k - 1;
      if (m < 0) {
        break;
      }
      long l = paramArrayOfLong[m];
      k = m;
      if (l != 0L)
      {
        for (k = c(l) & n; arrayOfLong[k] != 0L; k = k + 1 & n) {}
        arrayOfLong[k] = l;
        arrayOfObject[k] = paramArrayOfVType[m];
        k = m;
      }
    }
  }
  
  private int c(long paramLong)
  {
    if ((!j) && (paramLong == 0L)) {
      throw new AssertionError();
    }
    return d.a(paramLong, c);
  }
  
  private ae<VType> c()
  {
    try
    {
      ae localae = (ae)super.clone();
      a = ((long[])a.clone());
      b = ((Object[])b.clone());
      g = g;
      i = i.a();
      return localae;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      throw new RuntimeException(localCloneNotSupportedException);
    }
  }
  
  public final int a()
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.provideAs(TypeTransformer.java:780)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e1expr(TypeTransformer.java:496)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:713)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e2expr(TypeTransformer.java:632)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:716)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.s1stmt(TypeTransformer.java:810)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.sxStmt(TypeTransformer.java:840)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:206)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
  }
  
  public final VType a(long paramLong, VType paramVType)
  {
    if ((!j) && (d >= e + 1)) {
      throw new AssertionError();
    }
    int m = e;
    VType ?;
    if (paramLong == 0L)
    {
      g = true;
      localObject = b;
      k = m + 1;
      ? = localObject[k];
      localObject[k] = paramVType;
      return ?;
    }
    Object localObject = a;
    for (int k = c(paramLong) & m;; k = k + 1 & m)
    {
      long l = localObject[k];
      if (l == 0L) {
        break;
      }
      if (l == paramLong)
      {
        localObject = b;
        ? = localObject[k];
        localObject[k] = paramVType;
        return ?;
      }
    }
    if (d == f)
    {
      a(k, paramLong, paramVType);
    }
    else
    {
      localObject[k] = paramLong;
      b[k] = paramVType;
    }
    d += 1;
    return null;
  }
  
  public final boolean a(long paramLong)
  {
    if (paramLong == 0L) {
      return g;
    }
    long[] arrayOfLong = a;
    int m = e;
    for (int k = c(paramLong) & m;; k = k + 1 & m)
    {
      long l = arrayOfLong[k];
      if (l == 0L) {
        break;
      }
      if (l == paramLong) {
        return true;
      }
    }
    return false;
  }
  
  public final VType b(long paramLong)
  {
    if (paramLong == 0L)
    {
      if (g) {
        return (VType)b[(e + 1)];
      }
      return null;
    }
    long[] arrayOfLong = a;
    int m = e;
    for (int k = c(paramLong) & m;; k = k + 1 & m)
    {
      long l = arrayOfLong[k];
      if (l == 0L) {
        break;
      }
      if (l == paramLong) {
        return (VType)b[k];
      }
    }
    return null;
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject != null) && (getClass() == paramObject.getClass()))
    {
      paramObject = (ae)getClass().cast(paramObject);
      int k;
      if (((ae)paramObject).a() != a())
      {
        k = 0;
      }
      else
      {
        paramObject = ((ae)paramObject).iterator();
        while (((Iterator)paramObject).hasNext())
        {
          g localg = (g)((Iterator)paramObject).next();
          long l = b;
          if ((!a(l)) || (!Objects.equals(b(l), c)))
          {
            k = 0;
            break label105;
          }
        }
        k = 1;
      }
      label105:
      if (k != 0) {
        return true;
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    int k;
    if (g) {
      k = -559038737;
    } else {
      k = 0;
    }
    Iterator localIterator = iterator();
    while (localIterator.hasNext())
    {
      g localg = (g)localIterator.next();
      k += (int)d.a(b) + d.a(c);
    }
    return k;
  }
  
  public Iterator<g<VType>> iterator()
  {
    return new a();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[");
    Iterator localIterator = iterator();
    for (int k = 1; localIterator.hasNext(); k = 0)
    {
      g localg = (g)localIterator.next();
      if (k == 0) {
        localStringBuilder.append(", ");
      }
      localStringBuilder.append(b);
      localStringBuilder.append("=>");
      localStringBuilder.append(c);
    }
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
  
  final class a
    extends a<g<VType>>
  {
    private final g<VType> c = new g();
    private final int d = e + 1;
    private int e = -1;
    
    public a() {}
  }
  
  final class b
    extends c<VType>
  {
    private final ae<VType> b = ae.this;
    
    private b() {}
    
    public final int a()
    {
      return b.a();
    }
    
    public final Iterator<h<VType>> iterator()
    {
      return new ae.c(ae.this);
    }
  }
  
  final class c
    extends a<h<VType>>
  {
    private final h<VType> c = new h();
    private final int d = e + 1;
    private int e = -1;
    
    public c() {}
  }
}

/* Location:
 * Qualified Name:     com.a.a.ae
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */