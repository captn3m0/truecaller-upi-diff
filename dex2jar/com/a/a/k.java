package com.a.a;

import com.a.a.a.b;
import java.util.Iterator;

public class k
  implements l, Cloneable
{
  public char[] a;
  public int[] b;
  protected int c;
  protected int d;
  protected int e;
  protected int f;
  protected boolean g;
  protected double h;
  protected s i;
  
  public k()
  {
    this((byte)0);
  }
  
  private k(byte paramByte)
  {
    this(4, 0.75D);
  }
  
  public k(int paramInt, double paramDouble)
  {
    this(paramInt, paramDouble, r.d());
  }
  
  private k(int paramInt, double paramDouble, s params)
  {
    i = params;
    q.a(paramDouble);
    h = paramDouble;
    if ((paramInt > f) || (a == null))
    {
      params = a;
      int[] arrayOfInt = b;
      a(q.a(paramInt, h));
      if (params != null)
      {
        if (a() == 0) {
          paramInt = 1;
        } else {
          paramInt = 0;
        }
        if (paramInt == 0) {
          a(params, arrayOfInt);
        }
      }
    }
  }
  
  private int a()
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.provideAs(TypeTransformer.java:780)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e1expr(TypeTransformer.java:496)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:713)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e2expr(TypeTransformer.java:632)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:716)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.s1stmt(TypeTransformer.java:810)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.sxStmt(TypeTransformer.java:840)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:206)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
  }
  
  private void a(int paramInt)
  {
    if ((!j) && (Integer.bitCount(paramInt) != 1)) {
      throw new AssertionError();
    }
    int k = i.a(paramInt);
    char[] arrayOfChar = a;
    int[] arrayOfInt = b;
    int m = paramInt + 1;
    try
    {
      a = new char[m];
      b = new int[m];
      f = q.b(paramInt, h);
      c = k;
      e = (paramInt - 1);
      return;
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      a = arrayOfChar;
      b = arrayOfInt;
      throw new f("Not enough memory to allocate buffers for rehashing: %,d -> %,d", localOutOfMemoryError, new Object[] { Integer.valueOf(e + 1), Integer.valueOf(paramInt) });
    }
  }
  
  private void a(char[] paramArrayOfChar, int[] paramArrayOfInt)
  {
    if (!j) {
      if (paramArrayOfChar.length == paramArrayOfInt.length) {
        q.a(paramArrayOfChar.length - 1);
      } else {
        throw new AssertionError();
      }
    }
    char[] arrayOfChar = a;
    int[] arrayOfInt = b;
    int n = e;
    int k = paramArrayOfChar.length - 1;
    arrayOfChar[(arrayOfChar.length - 1)] = paramArrayOfChar[k];
    arrayOfInt[(arrayOfInt.length - 1)] = paramArrayOfInt[k];
    for (;;)
    {
      int m = k - 1;
      if (m < 0) {
        break;
      }
      char c1 = paramArrayOfChar[m];
      k = m;
      if (c1 != 0)
      {
        for (k = b(c1) & n; arrayOfChar[k] != 0; k = k + 1 & n) {}
        arrayOfChar[k] = c1;
        arrayOfInt[k] = paramArrayOfInt[m];
        k = m;
      }
    }
  }
  
  private int b(char paramChar)
  {
    if ((!j) && (paramChar == 0)) {
      throw new AssertionError();
    }
    return d.a(paramChar, c);
  }
  
  private k b()
  {
    try
    {
      k localk = (k)super.clone();
      a = ((char[])a.clone());
      b = ((int[])b.clone());
      g = g;
      i = i.a();
      return localk;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      throw new RuntimeException(localCloneNotSupportedException);
    }
  }
  
  public final int a(char paramChar)
  {
    if (paramChar == 0)
    {
      if (g) {
        return b[(e + 1)];
      }
      return -1;
    }
    char[] arrayOfChar = a;
    int m = e;
    for (int k = b(paramChar) & m;; k = k + 1 & m)
    {
      char c1 = arrayOfChar[k];
      if (c1 == 0) {
        break;
      }
      if (c1 == paramChar) {
        return b[k];
      }
    }
    return -1;
  }
  
  public final int a(char paramChar, int paramInt)
  {
    if ((!j) && (d >= e + 1)) {
      throw new AssertionError();
    }
    char c1 = e;
    if (paramChar == 0)
    {
      g = true;
      localObject = b;
      k = c1 + '\001';
      c1 = localObject[k];
      localObject[k] = paramInt;
      return c1;
    }
    Object localObject = a;
    for (int k = b(paramChar) & c1;; k = k + 1 & c1)
    {
      c2 = localObject[k];
      if (c2 == 0) {
        break;
      }
      if (c2 == paramChar)
      {
        localObject = b;
        c1 = localObject[k];
        localObject[k] = paramInt;
        return c1;
      }
    }
    c1 = d;
    char c2 = f;
    if (c1 == c2)
    {
      if ((!j) && ((c1 != c2) || (a[k] != 0) || (paramChar == 0))) {
        throw new AssertionError();
      }
      localObject = a;
      int[] arrayOfInt = b;
      a(q.a(e + 1, a(), h));
      if ((!j) && (a.length <= localObject.length)) {
        throw new AssertionError();
      }
      localObject[k] = paramChar;
      arrayOfInt[k] = paramInt;
      a((char[])localObject, arrayOfInt);
    }
    else
    {
      localObject[k] = paramChar;
      b[k] = paramInt;
    }
    d += 1;
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject != null) && (getClass() == paramObject.getClass()))
    {
      paramObject = (k)getClass().cast(paramObject);
      int k;
      if (((k)paramObject).a() != a())
      {
        k = 0;
      }
      else
      {
        paramObject = ((k)paramObject).iterator();
        while (((Iterator)paramObject).hasNext())
        {
          b localb = (b)((Iterator)paramObject).next();
          char c1 = b;
          boolean bool;
          char[] arrayOfChar;
          int m;
          char c2;
          if (c1 == 0)
          {
            bool = g;
          }
          else
          {
            arrayOfChar = a;
            m = e;
            for (k = b(c1) & m;; k = k + 1 & m)
            {
              c2 = arrayOfChar[k];
              if (c2 == 0) {
                break;
              }
              if (c2 == c1)
              {
                bool = true;
                break label144;
              }
            }
            bool = false;
          }
          label144:
          if (bool)
          {
            if (c1 == 0)
            {
              if (g) {
                k = b[(e + 1)];
              } else {
                k = 0;
              }
            }
            else
            {
              arrayOfChar = a;
              m = e;
              for (k = b(c1) & m;; k = k + 1 & m)
              {
                c2 = arrayOfChar[k];
                if (c2 == 0) {
                  break;
                }
                if (c2 == c1)
                {
                  k = b[k];
                  break label240;
                }
              }
              k = 0;
            }
            label240:
            if (k == c) {
              break;
            }
          }
          else
          {
            k = 0;
            break label256;
          }
        }
        k = 1;
      }
      label256:
      if (k != 0) {
        return true;
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    int k;
    if (g) {
      k = -559038737;
    } else {
      k = 0;
    }
    Iterator localIterator = iterator();
    while (localIterator.hasNext())
    {
      b localb = (b)localIterator.next();
      k += d.a(b) + d.a(c);
    }
    return k;
  }
  
  public Iterator<b> iterator()
  {
    return new a();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[");
    Iterator localIterator = iterator();
    for (int k = 1; localIterator.hasNext(); k = 0)
    {
      b localb = (b)localIterator.next();
      if (k == 0) {
        localStringBuilder.append(", ");
      }
      localStringBuilder.append(b);
      localStringBuilder.append("=>");
      localStringBuilder.append(c);
    }
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
  
  final class a
    extends a<b>
  {
    private final b c = new b();
    private final int d = e + 1;
    private int e = -1;
    
    public a() {}
  }
}

/* Location:
 * Qualified Name:     com.a.a.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */