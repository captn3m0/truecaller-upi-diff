package com.a.a;

import com.a.a.a.c;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;

public class n<VType>
  implements o<VType>, Cloneable
{
  public char[] a;
  public Object[] b;
  protected int c;
  protected int d;
  protected int e;
  protected int f;
  protected boolean g;
  protected double h;
  protected s i;
  
  public n()
  {
    this((byte)0);
  }
  
  private n(byte paramByte)
  {
    this(4, 0.75D);
  }
  
  public n(int paramInt, double paramDouble)
  {
    this(paramInt, paramDouble, r.d());
  }
  
  private n(int paramInt, double paramDouble, s params)
  {
    i = params;
    q.a(paramDouble);
    h = paramDouble;
    if ((paramInt > f) || (a == null))
    {
      params = a;
      Object[] arrayOfObject = (Object[])b;
      a(q.a(paramInt, h));
      if (params != null)
      {
        if (b() == 0) {
          paramInt = 1;
        } else {
          paramInt = 0;
        }
        if (paramInt == 0) {
          a(params, arrayOfObject);
        }
      }
    }
  }
  
  private void a(int paramInt)
  {
    if ((!j) && (Integer.bitCount(paramInt) != 1)) {
      throw new AssertionError();
    }
    int k = i.a(paramInt);
    char[] arrayOfChar = a;
    Object[] arrayOfObject = (Object[])b;
    int m = paramInt + 1;
    try
    {
      a = new char[m];
      b = ((Object[])new Object[m]);
      f = q.b(paramInt, h);
      c = k;
      e = (paramInt - 1);
      return;
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      a = arrayOfChar;
      b = arrayOfObject;
      throw new f("Not enough memory to allocate buffers for rehashing: %,d -> %,d", localOutOfMemoryError, new Object[] { Integer.valueOf(e + 1), Integer.valueOf(paramInt) });
    }
  }
  
  private void a(char[] paramArrayOfChar, VType[] paramArrayOfVType)
  {
    if (!j) {
      if (paramArrayOfChar.length == paramArrayOfVType.length) {
        q.a(paramArrayOfChar.length - 1);
      } else {
        throw new AssertionError();
      }
    }
    char[] arrayOfChar = a;
    Object[] arrayOfObject = (Object[])b;
    int n = e;
    int k = paramArrayOfChar.length - 1;
    arrayOfChar[(arrayOfChar.length - 1)] = paramArrayOfChar[k];
    arrayOfObject[(arrayOfObject.length - 1)] = paramArrayOfVType[k];
    for (;;)
    {
      int m = k - 1;
      if (m < 0) {
        break;
      }
      char c1 = paramArrayOfChar[m];
      k = m;
      if (c1 != 0)
      {
        for (k = b(c1) & n; arrayOfChar[k] != 0; k = k + 1 & n) {}
        arrayOfChar[k] = c1;
        arrayOfObject[k] = paramArrayOfVType[m];
        k = m;
      }
    }
  }
  
  private int b()
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.provideAs(TypeTransformer.java:780)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e1expr(TypeTransformer.java:496)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:713)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e2expr(TypeTransformer.java:632)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:716)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.s1stmt(TypeTransformer.java:810)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.sxStmt(TypeTransformer.java:840)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:206)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
  }
  
  private int b(char paramChar)
  {
    if ((!j) && (paramChar == 0)) {
      throw new AssertionError();
    }
    return d.a(paramChar, c);
  }
  
  private n<VType> c()
  {
    try
    {
      n localn = (n)super.clone();
      a = ((char[])a.clone());
      b = ((Object[])b.clone());
      g = g;
      i = i.a();
      return localn;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      throw new RuntimeException(localCloneNotSupportedException);
    }
  }
  
  public final VType a(char paramChar)
  {
    if (paramChar == 0)
    {
      if (g) {
        return (VType)b[(e + 1)];
      }
      return null;
    }
    char[] arrayOfChar = a;
    int m = e;
    for (int k = b(paramChar) & m;; k = k + 1 & m)
    {
      char c1 = arrayOfChar[k];
      if (c1 == 0) {
        break;
      }
      if (c1 == paramChar) {
        return (VType)b[k];
      }
    }
    return null;
  }
  
  public final VType a(char paramChar, VType paramVType)
  {
    if ((!j) && (d >= e + 1)) {
      throw new AssertionError();
    }
    char c1 = e;
    Object localObject2;
    if (paramChar == 0)
    {
      g = true;
      localObject1 = b;
      k = c1 + '\001';
      localObject2 = localObject1[k];
      localObject1[k] = paramVType;
      return (VType)localObject2;
    }
    Object localObject1 = a;
    for (int k = b(paramChar) & c1;; k = k + 1 & c1)
    {
      c2 = localObject1[k];
      if (c2 == 0) {
        break;
      }
      if (c2 == paramChar)
      {
        localObject1 = b;
        localObject2 = localObject1[k];
        localObject1[k] = paramVType;
        return (VType)localObject2;
      }
    }
    c1 = d;
    char c2 = f;
    if (c1 == c2)
    {
      if ((!j) && ((c1 != c2) || (a[k] != 0) || (paramChar == 0))) {
        throw new AssertionError();
      }
      localObject1 = a;
      localObject2 = (Object[])b;
      a(q.a(e + 1, b(), h));
      if ((!j) && (a.length <= localObject1.length)) {
        throw new AssertionError();
      }
      localObject1[k] = paramChar;
      localObject2[k] = paramVType;
      a((char[])localObject1, (Object[])localObject2);
    }
    else
    {
      localObject1[k] = paramChar;
      b[k] = paramVType;
    }
    d += 1;
    return null;
  }
  
  public final void a()
  {
    d = 0;
    g = false;
    Arrays.fill(a, '\000');
    Arrays.fill(b, null);
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject != null) && (getClass() == paramObject.getClass()))
    {
      paramObject = (n)getClass().cast(paramObject);
      int k;
      if (((n)paramObject).b() != b())
      {
        k = 0;
      }
      else
      {
        paramObject = ((n)paramObject).iterator();
        while (((Iterator)paramObject).hasNext())
        {
          c localc = (c)((Iterator)paramObject).next();
          char c1 = b;
          boolean bool;
          if (c1 == 0)
          {
            bool = g;
          }
          else
          {
            char[] arrayOfChar = a;
            int m = e;
            for (k = b(c1) & m;; k = k + 1 & m)
            {
              char c2 = arrayOfChar[k];
              if (c2 == 0) {
                break;
              }
              if (c2 == c1)
              {
                bool = true;
                break label144;
              }
            }
            bool = false;
          }
          label144:
          if ((!bool) || (!Objects.equals(a(c1), c)))
          {
            k = 0;
            break label172;
          }
        }
        k = 1;
      }
      label172:
      if (k != 0) {
        return true;
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    int k;
    if (g) {
      k = -559038737;
    } else {
      k = 0;
    }
    Iterator localIterator = iterator();
    while (localIterator.hasNext())
    {
      c localc = (c)localIterator.next();
      k += d.a(b) + d.a(c);
    }
    return k;
  }
  
  public Iterator<c<VType>> iterator()
  {
    return new a();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[");
    Iterator localIterator = iterator();
    for (int k = 1; localIterator.hasNext(); k = 0)
    {
      c localc = (c)localIterator.next();
      if (k == 0) {
        localStringBuilder.append(", ");
      }
      localStringBuilder.append(b);
      localStringBuilder.append("=>");
      localStringBuilder.append(c);
    }
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
  
  final class a
    extends a<c<VType>>
  {
    private final c<VType> c = new c();
    private final int d = e + 1;
    private int e = -1;
    
    public a() {}
  }
}

/* Location:
 * Qualified Name:     com.a.a.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */