package com.a.a;

import java.util.concurrent.atomic.AtomicLong;

public final class aj
  implements s
{
  public static final aj a = new aj();
  protected final AtomicLong b;
  
  public aj()
  {
    this(p.a());
  }
  
  private aj(long paramLong)
  {
    b = new AtomicLong(paramLong);
  }
  
  public final int a(int paramInt)
  {
    return (int)d.a(b.incrementAndGet());
  }
  
  public final s a()
  {
    return this;
  }
}

/* Location:
 * Qualified Name:     com.a.a.aj
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */