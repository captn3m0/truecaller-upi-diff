package com.a.a;

import com.a.a.a.h;
import java.util.Iterator;
import java.util.Objects;

public class u<VType>
  implements v<VType>, Cloneable
{
  public int[] a;
  public Object[] b;
  protected int c;
  protected int d;
  protected int e;
  protected int f;
  protected boolean g;
  protected double h;
  protected s i;
  
  public u()
  {
    this((byte)0);
  }
  
  private u(byte paramByte)
  {
    this(4, 0.75D);
  }
  
  public u(int paramInt, double paramDouble)
  {
    this(paramInt, paramDouble, r.d());
  }
  
  private u(int paramInt, double paramDouble, s params)
  {
    i = params;
    q.a(paramDouble);
    h = paramDouble;
    if ((paramInt > f) || (a == null))
    {
      params = a;
      Object[] arrayOfObject = (Object[])b;
      c(q.a(paramInt, h));
      if (params != null)
      {
        if (b() == 0) {
          paramInt = 1;
        } else {
          paramInt = 0;
        }
        if (paramInt == 0) {
          a(params, arrayOfObject);
        }
      }
    }
  }
  
  private void a(int[] paramArrayOfInt, VType[] paramArrayOfVType)
  {
    if (!j) {
      if (paramArrayOfInt.length == paramArrayOfVType.length) {
        q.a(paramArrayOfInt.length - 1);
      } else {
        throw new AssertionError();
      }
    }
    int[] arrayOfInt = a;
    Object[] arrayOfObject = (Object[])b;
    int n = e;
    int k = paramArrayOfInt.length - 1;
    arrayOfInt[(arrayOfInt.length - 1)] = paramArrayOfInt[k];
    arrayOfObject[(arrayOfObject.length - 1)] = paramArrayOfVType[k];
    for (;;)
    {
      int m = k - 1;
      if (m < 0) {
        break;
      }
      int i1 = paramArrayOfInt[m];
      k = m;
      if (i1 != 0)
      {
        for (k = b(i1) & n; arrayOfInt[k] != 0; k = k + 1 & n) {}
        arrayOfInt[k] = i1;
        arrayOfObject[k] = paramArrayOfVType[m];
        k = m;
      }
    }
  }
  
  private int b(int paramInt)
  {
    if ((!j) && (paramInt == 0)) {
      throw new AssertionError();
    }
    return d.a(paramInt ^ c);
  }
  
  private u<VType> c()
  {
    try
    {
      u localu = (u)super.clone();
      a = ((int[])a.clone());
      b = ((Object[])b.clone());
      g = g;
      i = i.a();
      return localu;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      throw new RuntimeException(localCloneNotSupportedException);
    }
  }
  
  private void c(int paramInt)
  {
    if ((!j) && (Integer.bitCount(paramInt) != 1)) {
      throw new AssertionError();
    }
    int k = i.a(paramInt);
    int[] arrayOfInt = a;
    Object[] arrayOfObject = (Object[])b;
    int m = paramInt + 1;
    try
    {
      a = new int[m];
      b = ((Object[])new Object[m]);
      f = q.b(paramInt, h);
      c = k;
      e = (paramInt - 1);
      return;
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      a = arrayOfInt;
      b = arrayOfObject;
      throw new f("Not enough memory to allocate buffers for rehashing: %,d -> %,d", localOutOfMemoryError, new Object[] { Integer.valueOf(e + 1), Integer.valueOf(paramInt) });
    }
  }
  
  public final VType a(int paramInt)
  {
    if (paramInt == 0)
    {
      if (g) {
        return (VType)b[(e + 1)];
      }
      return null;
    }
    int[] arrayOfInt = a;
    int m = e;
    for (int k = b(paramInt) & m;; k = k + 1 & m)
    {
      int n = arrayOfInt[k];
      if (n == 0) {
        break;
      }
      if (n == paramInt) {
        return (VType)b[k];
      }
    }
    return null;
  }
  
  public final VType a(int paramInt, VType paramVType)
  {
    if ((!j) && (d >= e + 1)) {
      throw new AssertionError();
    }
    int m = e;
    Object localObject2;
    if (paramInt == 0)
    {
      g = true;
      localObject1 = b;
      paramInt = m + 1;
      localObject2 = localObject1[paramInt];
      localObject1[paramInt] = paramVType;
      return (VType)localObject2;
    }
    Object localObject1 = a;
    for (int k = b(paramInt) & m;; k = k + 1 & m)
    {
      n = localObject1[k];
      if (n == 0) {
        break;
      }
      if (n == paramInt)
      {
        localObject1 = b;
        localObject2 = localObject1[k];
        localObject1[k] = paramVType;
        return (VType)localObject2;
      }
    }
    m = d;
    int n = f;
    if (m == n)
    {
      if ((!j) && ((m != n) || (a[k] != 0) || (paramInt == 0))) {
        throw new AssertionError();
      }
      localObject1 = a;
      localObject2 = (Object[])b;
      c(q.a(e + 1, b(), h));
      if ((!j) && (a.length <= localObject1.length)) {
        throw new AssertionError();
      }
      localObject1[k] = paramInt;
      localObject2[k] = paramVType;
      a((int[])localObject1, (Object[])localObject2);
    }
    else
    {
      localObject1[k] = paramInt;
      b[k] = paramVType;
    }
    d += 1;
    return null;
  }
  
  public final int b()
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.provideAs(TypeTransformer.java:780)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e1expr(TypeTransformer.java:496)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:713)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e2expr(TypeTransformer.java:632)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:716)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.s1stmt(TypeTransformer.java:810)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.sxStmt(TypeTransformer.java:840)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:206)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject != null) && (getClass() == paramObject.getClass()))
    {
      paramObject = (u)getClass().cast(paramObject);
      int k;
      if (((u)paramObject).b() != b())
      {
        k = 0;
      }
      else
      {
        paramObject = ((u)paramObject).iterator();
        while (((Iterator)paramObject).hasNext())
        {
          com.a.a.a.d locald = (com.a.a.a.d)((Iterator)paramObject).next();
          int m = b;
          boolean bool;
          if (m == 0)
          {
            bool = g;
          }
          else
          {
            int[] arrayOfInt = a;
            int n = e;
            for (k = b(m) & n;; k = k + 1 & n)
            {
              int i1 = arrayOfInt[k];
              if (i1 == 0) {
                break;
              }
              if (i1 == m)
              {
                bool = true;
                break label144;
              }
            }
            bool = false;
          }
          label144:
          if ((!bool) || (!Objects.equals(a(m), c)))
          {
            k = 0;
            break label172;
          }
        }
        k = 1;
      }
      label172:
      if (k != 0) {
        return true;
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    int k;
    if (g) {
      k = -559038737;
    } else {
      k = 0;
    }
    Iterator localIterator = iterator();
    while (localIterator.hasNext())
    {
      com.a.a.a.d locald = (com.a.a.a.d)localIterator.next();
      k += d.a(b) + d.a(c);
    }
    return k;
  }
  
  public Iterator<com.a.a.a.d<VType>> iterator()
  {
    return new a();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[");
    Iterator localIterator = iterator();
    for (int k = 1; localIterator.hasNext(); k = 0)
    {
      com.a.a.a.d locald = (com.a.a.a.d)localIterator.next();
      if (k == 0) {
        localStringBuilder.append(", ");
      }
      localStringBuilder.append(b);
      localStringBuilder.append("=>");
      localStringBuilder.append(c);
    }
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
  
  final class a
    extends a<com.a.a.a.d<VType>>
  {
    private final com.a.a.a.d<VType> c = new com.a.a.a.d();
    private final int d = e + 1;
    private int e = -1;
    
    public a() {}
  }
  
  final class b
    extends c<VType>
  {
    private final u<VType> b = u.this;
    
    private b() {}
    
    public final int a()
    {
      return b.b();
    }
    
    public final Iterator<h<VType>> iterator()
    {
      return new u.c(u.this);
    }
  }
  
  final class c
    extends a<h<VType>>
  {
    private final h<VType> c = new h();
    private final int d = e + 1;
    private int e = -1;
    
    public c() {}
  }
}

/* Location:
 * Qualified Name:     com.a.a.u
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */