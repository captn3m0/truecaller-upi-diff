package com.a.a;

import com.a.a.a.f;
import java.util.Iterator;

public abstract interface z
  extends Iterable<f>
{
  public abstract boolean a();
  
  public abstract Iterator<f> iterator();
}

/* Location:
 * Qualified Name:     com.a.a.z
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */