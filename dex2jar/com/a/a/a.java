package com.a.a;

import java.util.Iterator;
import java.util.NoSuchElementException;

public abstract class a<E>
  implements Iterator<E>
{
  int a = 0;
  private E b;
  
  protected abstract E a();
  
  public boolean hasNext()
  {
    if (a == 0)
    {
      a = 1;
      b = a();
    }
    return a == 1;
  }
  
  public E next()
  {
    if (hasNext())
    {
      a = 0;
      return (E)b;
    }
    throw new NoSuchElementException();
  }
  
  public void remove()
  {
    throw new UnsupportedOperationException();
  }
}

/* Location:
 * Qualified Name:     com.a.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */