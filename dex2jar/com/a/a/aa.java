package com.a.a;

import java.util.Iterator;

public class aa
  implements ab, Cloneable
{
  public long[] a;
  public int[] b;
  protected int c;
  protected int d;
  protected int e;
  protected int f;
  protected boolean g;
  protected double h;
  protected s i;
  
  public aa()
  {
    this(4);
  }
  
  public aa(int paramInt)
  {
    this(paramInt, (byte)0);
  }
  
  private aa(int paramInt, byte paramByte)
  {
    this(paramInt, r.d());
  }
  
  private aa(int paramInt, s params)
  {
    i = params;
    q.a(0.75D);
    h = 0.75D;
    if ((paramInt > f) || (a == null))
    {
      params = a;
      int[] arrayOfInt = b;
      a(q.a(paramInt, h));
      if ((params != null) && (!a())) {
        a(params, arrayOfInt);
      }
    }
  }
  
  private void a(int paramInt)
  {
    if ((!j) && (Integer.bitCount(paramInt) != 1)) {
      throw new AssertionError();
    }
    int k = i.a(paramInt);
    long[] arrayOfLong = a;
    int[] arrayOfInt = b;
    int m = paramInt + 1;
    try
    {
      a = new long[m];
      b = new int[m];
      f = q.b(paramInt, h);
      c = k;
      e = (paramInt - 1);
      return;
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      a = arrayOfLong;
      b = arrayOfInt;
      throw new f("Not enough memory to allocate buffers for rehashing: %,d -> %,d", localOutOfMemoryError, new Object[] { Integer.valueOf(e + 1), Integer.valueOf(paramInt) });
    }
  }
  
  private void a(long[] paramArrayOfLong, int[] paramArrayOfInt)
  {
    if (!j) {
      if (paramArrayOfLong.length == paramArrayOfInt.length) {
        q.a(paramArrayOfLong.length - 1);
      } else {
        throw new AssertionError();
      }
    }
    long[] arrayOfLong = a;
    int[] arrayOfInt = b;
    int n = e;
    int k = paramArrayOfLong.length - 1;
    arrayOfLong[(arrayOfLong.length - 1)] = paramArrayOfLong[k];
    arrayOfInt[(arrayOfInt.length - 1)] = paramArrayOfInt[k];
    for (;;)
    {
      int m = k - 1;
      if (m < 0) {
        break;
      }
      long l = paramArrayOfLong[m];
      k = m;
      if (l != 0L)
      {
        for (k = b(l) & n; arrayOfLong[k] != 0L; k = k + 1 & n) {}
        arrayOfLong[k] = l;
        arrayOfInt[k] = paramArrayOfInt[m];
        k = m;
      }
    }
  }
  
  private int b()
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.provideAs(TypeTransformer.java:780)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e1expr(TypeTransformer.java:496)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:713)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e2expr(TypeTransformer.java:632)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:716)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.s1stmt(TypeTransformer.java:810)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.sxStmt(TypeTransformer.java:840)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:206)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
  }
  
  private int b(long paramLong)
  {
    if ((!j) && (paramLong == 0L)) {
      throw new AssertionError();
    }
    return d.a(paramLong, c);
  }
  
  private aa c()
  {
    try
    {
      aa localaa = (aa)super.clone();
      a = ((long[])a.clone());
      b = ((int[])b.clone());
      g = g;
      i = i.a();
      return localaa;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      throw new RuntimeException(localCloneNotSupportedException);
    }
  }
  
  public final int a(long paramLong)
  {
    if (paramLong == 0L)
    {
      if (g) {
        return b[(e + 1)];
      }
      return 0;
    }
    long[] arrayOfLong = a;
    int m = e;
    for (int k = b(paramLong) & m;; k = k + 1 & m)
    {
      long l = arrayOfLong[k];
      if (l == 0L) {
        break;
      }
      if (l == paramLong) {
        return b[k];
      }
    }
    return 0;
  }
  
  public final int a(long paramLong, int paramInt)
  {
    if ((!j) && (d >= e + 1)) {
      throw new AssertionError();
    }
    int m = e;
    if (paramLong == 0L)
    {
      g = true;
      localObject = b;
      k = m + 1;
      m = localObject[k];
      localObject[k] = paramInt;
      return m;
    }
    Object localObject = a;
    for (int k = b(paramLong) & m;; k = k + 1 & m)
    {
      long l = localObject[k];
      if (l == 0L) {
        break;
      }
      if (l == paramLong)
      {
        localObject = b;
        m = localObject[k];
        localObject[k] = paramInt;
        return m;
      }
    }
    m = d;
    int n = f;
    if (m == n)
    {
      if ((!j) && ((m != n) || (a[k] != 0L) || (paramLong == 0L))) {
        throw new AssertionError();
      }
      localObject = a;
      int[] arrayOfInt = b;
      a(q.a(e + 1, b(), h));
      if ((!j) && (a.length <= localObject.length)) {
        throw new AssertionError();
      }
      localObject[k] = paramLong;
      arrayOfInt[k] = paramInt;
      a((long[])localObject, arrayOfInt);
    }
    else
    {
      localObject[k] = paramLong;
      b[k] = paramInt;
    }
    d += 1;
    return 0;
  }
  
  public final boolean a()
  {
    return b() == 0;
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject != null) && (getClass() == paramObject.getClass()))
    {
      paramObject = (aa)getClass().cast(paramObject);
      int k;
      if (((aa)paramObject).b() != b())
      {
        k = 0;
      }
      else
      {
        paramObject = ((aa)paramObject).iterator();
        while (((Iterator)paramObject).hasNext())
        {
          com.a.a.a.f localf = (com.a.a.a.f)((Iterator)paramObject).next();
          long l1 = b;
          boolean bool;
          long[] arrayOfLong;
          int m;
          long l2;
          if (l1 == 0L)
          {
            bool = g;
          }
          else
          {
            arrayOfLong = a;
            m = e;
            for (k = b(l1) & m;; k = k + 1 & m)
            {
              l2 = arrayOfLong[k];
              if (l2 == 0L) {
                break;
              }
              if (l2 == l1)
              {
                bool = true;
                break label150;
              }
            }
            bool = false;
          }
          label150:
          if (bool)
          {
            if (l1 == 0L)
            {
              if (g) {
                k = b[(e + 1)];
              } else {
                k = 0;
              }
            }
            else
            {
              arrayOfLong = a;
              m = e;
              for (k = b(l1) & m;; k = k + 1 & m)
              {
                l2 = arrayOfLong[k];
                if (l2 == 0L) {
                  break;
                }
                if (l2 == l1)
                {
                  k = b[k];
                  break label251;
                }
              }
              k = 0;
            }
            label251:
            if (k == c) {
              break;
            }
          }
          else
          {
            k = 0;
            break label267;
          }
        }
        k = 1;
      }
      label267:
      if (k != 0) {
        return true;
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    int k;
    if (g) {
      k = -559038737;
    } else {
      k = 0;
    }
    Iterator localIterator = iterator();
    while (localIterator.hasNext())
    {
      com.a.a.a.f localf = (com.a.a.a.f)localIterator.next();
      k += (int)d.a(b) + d.a(c);
    }
    return k;
  }
  
  public Iterator<com.a.a.a.f> iterator()
  {
    return new a();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[");
    Iterator localIterator = iterator();
    for (int k = 1; localIterator.hasNext(); k = 0)
    {
      com.a.a.a.f localf = (com.a.a.a.f)localIterator.next();
      if (k == 0) {
        localStringBuilder.append(", ");
      }
      localStringBuilder.append(b);
      localStringBuilder.append("=>");
      localStringBuilder.append(c);
    }
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
  
  final class a
    extends a<com.a.a.a.f>
  {
    private final com.a.a.a.f c = new com.a.a.a.f();
    private final int d = e + 1;
    private int e = -1;
    
    public a() {}
  }
}

/* Location:
 * Qualified Name:     com.a.a.aa
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */