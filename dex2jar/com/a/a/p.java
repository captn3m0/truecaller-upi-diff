package com.a.a;

import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class p
{
  private static String a;
  private static final String b = new String();
  
  public static long a()
  {
    if (a == null) {
      try
      {
        a = (String)AccessController.doPrivileged(new PrivilegedAction() {});
      }
      catch (SecurityException localSecurityException)
      {
        a = b;
        Logger.getLogger(p.class.getName()).log(Level.INFO, "Failed to read 'tests.seed' property for initial random seed.", localSecurityException);
      }
    }
    String str = a;
    long l;
    if (str != b) {
      l = str.hashCode();
    } else {
      l = System.nanoTime() ^ System.identityHashCode(new Object());
    }
    return d.a(l);
  }
}

/* Location:
 * Qualified Name:     com.a.a.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */