package com.a.a;

import com.a.a.a.e;
import java.util.Iterator;

public abstract interface x
  extends Iterable<e>
{
  public abstract boolean a(long paramLong);
  
  public abstract int b();
  
  public abstract Iterator<e> iterator();
}

/* Location:
 * Qualified Name:     com.a.a.x
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */