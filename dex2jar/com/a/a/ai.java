package com.a.a;

import com.a.a.a.h;
import java.util.Iterator;

public abstract interface ai<KType>
  extends Iterable<h<KType>>
{
  public abstract int a();
  
  public abstract Iterator<h<KType>> iterator();
}

/* Location:
 * Qualified Name:     com.a.a.ai
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */