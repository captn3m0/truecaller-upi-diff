package com.a.a;

import java.util.IllegalFormatException;
import java.util.Locale;

public final class f
  extends RuntimeException
{
  private f(String paramString)
  {
    super(paramString);
  }
  
  public f(String paramString, Throwable paramThrowable, Object... paramVarArgs)
  {
    super(a(paramString, paramThrowable, paramVarArgs), paramThrowable);
  }
  
  public f(String paramString, Object... paramVarArgs)
  {
    this(paramString, null, paramVarArgs);
  }
  
  private static String a(String paramString, Throwable paramThrowable, Object... paramVarArgs)
  {
    try
    {
      paramVarArgs = String.format(Locale.ROOT, paramString, paramVarArgs);
      return paramVarArgs;
    }
    catch (IllegalFormatException paramVarArgs)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramString);
      localStringBuilder.append(" [ILLEGAL FORMAT, ARGS SUPPRESSED]");
      paramString = new f(localStringBuilder.toString());
      if (paramThrowable != null) {
        paramString.addSuppressed(paramThrowable);
      }
      paramString.addSuppressed(paramVarArgs);
      throw paramString;
    }
  }
}

/* Location:
 * Qualified Name:     com.a.a.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */