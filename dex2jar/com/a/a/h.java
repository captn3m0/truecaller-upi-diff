package com.a.a;

import java.util.Iterator;

public class h
  implements i, Cloneable
{
  public char[] a;
  public char[] b;
  protected int c;
  protected int d;
  protected int e;
  protected int f;
  protected boolean g;
  protected double h;
  protected s i;
  
  public h()
  {
    this(4);
  }
  
  public h(int paramInt)
  {
    this(paramInt, 0.75D);
  }
  
  public h(int paramInt, double paramDouble)
  {
    this(paramInt, paramDouble, r.d());
  }
  
  private h(int paramInt, double paramDouble, s params)
  {
    i = params;
    q.a(paramDouble);
    h = paramDouble;
    if ((paramInt > f) || (a == null))
    {
      params = a;
      char[] arrayOfChar = b;
      a(q.a(paramInt, h));
      if (params != null)
      {
        if (a() == 0) {
          paramInt = 1;
        } else {
          paramInt = 0;
        }
        if (paramInt == 0) {
          a(params, arrayOfChar);
        }
      }
    }
  }
  
  private int a()
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.provideAs(TypeTransformer.java:780)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e1expr(TypeTransformer.java:496)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:713)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e2expr(TypeTransformer.java:632)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:716)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.s1stmt(TypeTransformer.java:810)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.sxStmt(TypeTransformer.java:840)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:206)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
  }
  
  private int a(char paramChar)
  {
    if ((!j) && (paramChar == 0)) {
      throw new AssertionError();
    }
    return d.a(paramChar, c);
  }
  
  private void a(int paramInt)
  {
    if ((!j) && (Integer.bitCount(paramInt) != 1)) {
      throw new AssertionError();
    }
    int k = i.a(paramInt);
    char[] arrayOfChar1 = a;
    char[] arrayOfChar2 = b;
    int m = paramInt + 1;
    try
    {
      a = new char[m];
      b = new char[m];
      f = q.b(paramInt, h);
      c = k;
      e = (paramInt - 1);
      return;
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      a = arrayOfChar1;
      b = arrayOfChar2;
      throw new f("Not enough memory to allocate buffers for rehashing: %,d -> %,d", localOutOfMemoryError, new Object[] { Integer.valueOf(e + 1), Integer.valueOf(paramInt) });
    }
  }
  
  private void a(char[] paramArrayOfChar1, char[] paramArrayOfChar2)
  {
    if (!j) {
      if (paramArrayOfChar1.length == paramArrayOfChar2.length) {
        q.a(paramArrayOfChar1.length - 1);
      } else {
        throw new AssertionError();
      }
    }
    char[] arrayOfChar1 = a;
    char[] arrayOfChar2 = b;
    int n = e;
    int k = paramArrayOfChar1.length - 1;
    arrayOfChar1[(arrayOfChar1.length - 1)] = paramArrayOfChar1[k];
    arrayOfChar2[(arrayOfChar2.length - 1)] = paramArrayOfChar2[k];
    for (;;)
    {
      int m = k - 1;
      if (m < 0) {
        break;
      }
      char c1 = paramArrayOfChar1[m];
      k = m;
      if (c1 != 0)
      {
        for (k = a(c1) & n; arrayOfChar1[k] != 0; k = k + 1 & n) {}
        arrayOfChar1[k] = c1;
        arrayOfChar2[k] = paramArrayOfChar2[m];
        k = m;
      }
    }
  }
  
  private h b()
  {
    try
    {
      h localh = (h)super.clone();
      a = ((char[])a.clone());
      b = ((char[])b.clone());
      g = g;
      i = i.a();
      return localh;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      throw new RuntimeException(localCloneNotSupportedException);
    }
  }
  
  public final char a(char paramChar1, char paramChar2)
  {
    if ((!j) && (d >= e + 1)) {
      throw new AssertionError();
    }
    char c1 = e;
    if (paramChar1 == 0)
    {
      g = true;
      arrayOfChar1 = b;
      k = c1 + '\001';
      paramChar1 = arrayOfChar1[k];
      arrayOfChar1[k] = paramChar2;
      return paramChar1;
    }
    char[] arrayOfChar1 = a;
    for (int k = a(paramChar1) & c1;; k = k + 1 & c1)
    {
      c2 = arrayOfChar1[k];
      if (c2 == 0) {
        break;
      }
      if (c2 == paramChar1)
      {
        arrayOfChar1 = b;
        paramChar1 = arrayOfChar1[k];
        arrayOfChar1[k] = paramChar2;
        return paramChar1;
      }
    }
    c1 = d;
    char c2 = f;
    if (c1 == c2)
    {
      if ((!j) && ((c1 != c2) || (a[k] != 0) || (paramChar1 == 0))) {
        throw new AssertionError();
      }
      arrayOfChar1 = a;
      char[] arrayOfChar2 = b;
      a(q.a(e + 1, a(), h));
      if ((!j) && (a.length <= arrayOfChar1.length)) {
        throw new AssertionError();
      }
      arrayOfChar1[k] = paramChar1;
      arrayOfChar2[k] = paramChar2;
      a(arrayOfChar1, arrayOfChar2);
    }
    else
    {
      arrayOfChar1[k] = paramChar1;
      b[k] = paramChar2;
    }
    d += 1;
    return '\000';
  }
  
  public final int a(g paramg)
  {
    int k = a();
    paramg = paramg.iterator();
    while (paramg.hasNext())
    {
      com.a.a.a.a locala = (com.a.a.a.a)paramg.next();
      a(b, c);
    }
    return a() - k;
  }
  
  public final char b(char paramChar1, char paramChar2)
  {
    if (paramChar1 == 0)
    {
      if (g) {
        return b[(e + 1)];
      }
      return paramChar2;
    }
    char[] arrayOfChar = a;
    int m = e;
    for (int k = a(paramChar1) & m;; k = k + 1 & m)
    {
      char c1 = arrayOfChar[k];
      if (c1 == 0) {
        break;
      }
      if (c1 == paramChar1) {
        return b[k];
      }
    }
    return paramChar2;
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject != null) && (getClass() == paramObject.getClass()))
    {
      paramObject = (h)getClass().cast(paramObject);
      int k;
      if (((h)paramObject).a() != a())
      {
        k = 0;
      }
      else
      {
        paramObject = ((h)paramObject).iterator();
        while (((Iterator)paramObject).hasNext())
        {
          com.a.a.a.a locala = (com.a.a.a.a)((Iterator)paramObject).next();
          char c1 = b;
          boolean bool;
          char[] arrayOfChar;
          int m;
          char c2;
          if (c1 == 0)
          {
            bool = g;
          }
          else
          {
            arrayOfChar = a;
            m = e;
            for (k = a(c1) & m;; k = k + 1 & m)
            {
              c2 = arrayOfChar[k];
              if (c2 == 0) {
                break;
              }
              if (c2 == c1)
              {
                bool = true;
                break label144;
              }
            }
            bool = false;
          }
          label144:
          if (bool)
          {
            if (c1 == 0)
            {
              if (g) {
                k = b[(e + 1)];
              } else {
                k = 0;
              }
            }
            else
            {
              arrayOfChar = a;
              m = e;
              for (k = a(c1) & m;; k = k + 1 & m)
              {
                c2 = arrayOfChar[k];
                if (c2 == 0) {
                  break;
                }
                if (c2 == c1)
                {
                  k = b[k];
                  break label240;
                }
              }
              k = 0;
            }
            label240:
            if (k == c) {
              break;
            }
          }
          else
          {
            k = 0;
            break label256;
          }
        }
        k = 1;
      }
      label256:
      if (k != 0) {
        return true;
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    int k;
    if (g) {
      k = -559038737;
    } else {
      k = 0;
    }
    Iterator localIterator = iterator();
    while (localIterator.hasNext())
    {
      com.a.a.a.a locala = (com.a.a.a.a)localIterator.next();
      k += d.a(b) + d.a(c);
    }
    return k;
  }
  
  public Iterator<com.a.a.a.a> iterator()
  {
    return new a();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[");
    Iterator localIterator = iterator();
    for (int k = 1; localIterator.hasNext(); k = 0)
    {
      com.a.a.a.a locala = (com.a.a.a.a)localIterator.next();
      if (k == 0) {
        localStringBuilder.append(", ");
      }
      localStringBuilder.append(b);
      localStringBuilder.append("=>");
      localStringBuilder.append(c);
    }
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
  
  final class a
    extends a<com.a.a.a.a>
  {
    private final com.a.a.a.a c = new com.a.a.a.a();
    private final int d = e + 1;
    private int e = -1;
    
    public a() {}
  }
}

/* Location:
 * Qualified Name:     com.a.a.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */