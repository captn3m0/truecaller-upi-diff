package com.a.a.a;

public final class h<KType>
{
  public int a;
  public KType b;
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("[cursor, index: ");
    localStringBuilder.append(a);
    localStringBuilder.append(", value: ");
    localStringBuilder.append(b);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.a.a.a.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */