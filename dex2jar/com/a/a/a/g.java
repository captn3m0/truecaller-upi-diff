package com.a.a.a;

public final class g<VType>
{
  public int a;
  public long b;
  public VType c;
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("[cursor, index: ");
    localStringBuilder.append(a);
    localStringBuilder.append(", key: ");
    localStringBuilder.append(b);
    localStringBuilder.append(", value: ");
    localStringBuilder.append(c);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.a.a.a.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */