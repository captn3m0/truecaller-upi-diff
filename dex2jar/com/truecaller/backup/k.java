package com.truecaller.backup;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class k
  implements d<com.truecaller.callhistory.k>
{
  private final Provider<Context> a;
  
  private k(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static k a(Provider<Context> paramProvider)
  {
    return new k(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */