package com.truecaller.backup;

import c.a.ag;
import c.g.b.k;
import c.k.i;
import c.n;
import c.t;
import com.truecaller.common.g.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class d
{
  public static final d.a b = new d.a((byte)0);
  final a a;
  
  public d(a parama)
  {
    a = parama;
  }
  
  private static String a(Map<String, String> paramMap)
  {
    Collection localCollection = (Collection)new ArrayList(paramMap.size());
    paramMap = paramMap.entrySet().iterator();
    while (paramMap.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)paramMap.next();
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append((String)localEntry.getKey());
      localStringBuilder.append('=');
      localStringBuilder.append((String)localEntry.getValue());
      localCollection.add(localStringBuilder.toString());
    }
    return c.a.m.a((Iterable)localCollection, (CharSequence)",", null, null, 0, null, null, 62);
  }
  
  static Map<String, String> a(String paramString)
  {
    Object localObject1 = (Iterable)c.n.m.c((CharSequence)paramString, new String[] { "," }, false, 6);
    paramString = (Map)new LinkedHashMap(i.c(ag.a(c.a.m.a((Iterable)localObject1, 10)), 16));
    localObject1 = ((Iterable)localObject1).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      Object localObject2 = c.n.m.c((CharSequence)((Iterator)localObject1).next(), new String[] { "=" }, false, 6);
      localObject2 = t.a((String)((List)localObject2).get(0), (String)((List)localObject2).get(1));
      paramString.put(a, b);
    }
    return paramString;
  }
  
  public final void a(String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    k.b(paramString, "backupAction");
    paramString = t.a("backup_action_key", paramString);
    Object localObject = t.a("backup_file_exists_key", String.valueOf(paramBoolean1));
    int i = 1;
    String str = a(ag.a(new n[] { paramString, localObject, t.a("account_state_valid", String.valueOf(paramBoolean2)) }));
    localObject = a.b("accountAutobackupLogInfo", "");
    k.a(localObject, "it");
    if (((CharSequence)localObject).length() <= 0) {
      i = 0;
    }
    paramString = (String)localObject;
    if (i != 0)
    {
      paramString = new StringBuilder();
      paramString.append((String)localObject);
      paramString.append(";");
      paramString = paramString.toString();
    }
    localObject = a;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString);
    localStringBuilder.append(str);
    ((a)localObject).a("accountAutobackupLogInfo", localStringBuilder.toString());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */