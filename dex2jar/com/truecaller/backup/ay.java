package com.truecaller.backup;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import c.a.m;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.a;
import com.truecaller.content.TruecallerContract.ah;
import javax.inject.Inject;

public final class ay
  implements ax
{
  private final String a;
  private final Context b;
  
  @Inject
  public ay(Context paramContext)
  {
    b = paramContext;
    a = "(contact_source & 2) = 2";
  }
  
  public final void a()
  {
    ContentProviderOperation localContentProviderOperation1 = ContentProviderOperation.newUpdate(TruecallerContract.ah.a()).withValue("tc_flag", Integer.valueOf(1)).build();
    ContentProviderOperation localContentProviderOperation2 = ContentProviderOperation.newUpdate(TruecallerContract.a.a()).withValue("tc_flag", Integer.valueOf(1)).build();
    b.getContentResolver().applyBatch(TruecallerContract.a(), m.d(new ContentProviderOperation[] { localContentProviderOperation1, localContentProviderOperation2 }));
  }
  
  public final void b()
  {
    ContentProviderOperation localContentProviderOperation1 = ContentProviderOperation.newUpdate(TruecallerContract.ah.a()).withValue("tc_flag", Integer.valueOf(2)).build();
    ContentProviderOperation localContentProviderOperation2 = ContentProviderOperation.newUpdate(TruecallerContract.a.a()).withValue("tc_flag", Integer.valueOf(2)).build();
    Object localObject = new ContentValues();
    ((ContentValues)localObject).put("contact_source", Integer.valueOf(32));
    ((ContentValues)localObject).putNull("contact_phonebook_id");
    ((ContentValues)localObject).putNull("contact_phonebook_hash");
    ((ContentValues)localObject).putNull("contact_phonebook_lookup");
    ContentProviderOperation localContentProviderOperation3 = ContentProviderOperation.newUpdate(TruecallerContract.ah.a()).withSelection(a, null).withValues((ContentValues)localObject).build();
    localObject = ContentProviderOperation.newUpdate(TruecallerContract.a.a()).withSelection(a, null).withValues((ContentValues)localObject).build();
    b.getContentResolver().applyBatch(TruecallerContract.a(), m.d(new ContentProviderOperation[] { localContentProviderOperation1, localContentProviderOperation2, localContentProviderOperation3, localObject }));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.ay
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */