package com.truecaller.backup;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class az
  implements d<ay>
{
  private final Provider<Context> a;
  
  private az(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static az a(Provider<Context> paramProvider)
  {
    return new az(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.az
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */