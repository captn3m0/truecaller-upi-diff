package com.truecaller.backup;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import c.a.f;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.log.AssertionUtil;
import com.truecaller.multisim.h;
import com.truecaller.utils.a;
import com.truecaller.utils.extensions.d;
import com.truecaller.utils.extensions.g;
import com.truecaller.utils.l;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collection;
import javax.inject.Inject;

public final class aq
  implements ap
{
  private final ContentResolver a;
  private final h b;
  private final l c;
  private final com.truecaller.callhistory.k d;
  private final a e;
  private final b f;
  
  @Inject
  public aq(Context paramContext, h paramh, l paraml, com.truecaller.callhistory.k paramk, a parama, b paramb)
  {
    b = paramh;
    c = paraml;
    d = paramk;
    e = parama;
    f = paramb;
    a = paramContext.getContentResolver();
  }
  
  private final void a(long paramLong, ArrayList<ContentProviderOperation> paramArrayList)
  {
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("tc_flag", Integer.valueOf(2));
    localContentValues.putNull("call_log_id");
    paramArrayList.add(ContentProviderOperation.newUpdate(TruecallerContract.n.a()).withValues(localContentValues).withSelection("_id=".concat(String.valueOf(paramLong)), null).build());
    a(paramArrayList, false);
  }
  
  private final void a(ArrayList<ContentProviderOperation> paramArrayList, boolean paramBoolean)
  {
    if ((paramArrayList.size() < 50) && (!paramBoolean)) {
      return;
    }
    a.applyBatch(TruecallerContract.a(), new ArrayList((Collection)paramArrayList));
    paramArrayList.clear();
  }
  
  public final void a()
  {
    if (c.a(new String[] { "android.permission.READ_CALL_LOG" }))
    {
      if (!c.a(new String[] { "android.permission.READ_PHONE_STATE" })) {
        return;
      }
      long l1 = e.a();
      Object localObject1 = d.b();
      Object localObject2 = b.d();
      if (localObject2 != null)
      {
        c.g.b.k.a(localObject1, "projection");
        localObject2 = (String[])f.a((Object[])localObject1, localObject2);
        if (localObject2 != null) {
          localObject1 = localObject2;
        }
      }
      b localb;
      try
      {
        localObject1 = a.query(d.a(), (String[])localObject1, null, null, "date DESC, _id DESC");
        if (localObject1 != null) {
          localObject1 = new as((Cursor)localObject1);
        } else {
          localObject1 = null;
        }
      }
      catch (IllegalArgumentException localIllegalArgumentException)
      {
        AssertionUtil.reportThrowableButNeverCrash((Throwable)localIllegalArgumentException);
        localb = null;
      }
      if (localb == null) {
        return;
      }
      localObject2 = a.query(TruecallerContract.n.a(), new String[] { "_id", "call_log_id", "timestamp" }, "call_log_id>=0", null, "timestamp DESC, call_log_id DESC");
      if (localObject2 != null)
      {
        localObject2 = new bf((Cursor)localObject2);
        ArrayList localArrayList = new ArrayList();
        boolean bool2 = localb.moveToNext();
        boolean bool1 = ((bf)localObject2).moveToNext();
        boolean bool3;
        for (;;)
        {
          bool3 = bool1;
          if (!bool2) {
            break;
          }
          bool3 = bool1;
          if (!bool1) {
            break;
          }
          g localg = c;
          Cursor localCursor1 = (Cursor)localb;
          l2 = ((Number)localg.a(localCursor1, as.a[1])).longValue();
          localg = c;
          Cursor localCursor2 = (Cursor)localObject2;
          long l3 = ((Number)localg.a(localCursor2, bf.a[2])).longValue();
          long l4 = ((Number)b.a(localCursor1, as.a[0])).longValue();
          long l5 = ((Number)b.a(localCursor2, bf.a[1])).longValue();
          if (l2 > l3)
          {
            bool2 = localb.moveToNext();
          }
          else if (l2 < l3)
          {
            a(((bf)localObject2).a(), localArrayList);
            bool1 = ((bf)localObject2).moveToNext();
          }
          else if (l4 > l5)
          {
            bool2 = localb.moveToNext();
          }
          else if (l4 < l5)
          {
            a(((bf)localObject2).a(), localArrayList);
            bool1 = ((bf)localObject2).moveToNext();
          }
          else
          {
            l2 = ((bf)localObject2).a();
            localArrayList.add(ContentProviderOperation.newDelete(TruecallerContract.n.a()).withSelection("_id=".concat(String.valueOf(l2)), null).build());
            a(localArrayList, false);
            bool1 = ((bf)localObject2).moveToNext();
          }
        }
        while (bool3)
        {
          a(((bf)localObject2).a(), localArrayList);
          bool3 = ((bf)localObject2).moveToNext();
        }
        a(localArrayList, true);
        d.a((Closeable)localb);
        d.a((Closeable)localObject2);
        long l2 = e.a();
        localb = f;
        localObject2 = new e.a("BackupDuration").a("Segment", "CallLog");
        double d1 = l2 - l1;
        Double.isNaN(d1);
        localObject2 = ((e.a)localObject2).a(Double.valueOf(d1 / 1000.0D)).a();
        c.g.b.k.a(localObject2, "AnalyticsEvent.Builder(B…\n                .build()");
        localb.b((e)localObject2);
        return;
      }
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.aq
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */