package com.truecaller.backup;

import android.content.Context;
import c.d.f;
import com.truecaller.analytics.b;
import com.truecaller.common.account.r;
import com.truecaller.utils.l;
import com.truecaller.utils.t;
import javax.inject.Provider;

public final class bb
  implements c
{
  private Provider<e> A;
  private final com.truecaller.common.a b;
  private final com.truecaller.analytics.d c;
  private Provider<Context> d;
  private Provider<com.truecaller.multisim.h> e;
  private Provider<l> f;
  private Provider<com.truecaller.callhistory.k> g;
  private Provider<com.truecaller.utils.a> h;
  private Provider<b> i;
  private Provider<aq> j;
  private Provider<ap> k;
  private Provider<ay> l;
  private Provider<ax> m;
  private Provider<com.truecaller.common.g.a> n;
  private Provider<bl> o;
  private Provider<bk> p;
  private Provider<f> q;
  private Provider<f> r;
  private Provider<f> s;
  private Provider<ah> t;
  private Provider<bd> u;
  private Provider<bc> v;
  private Provider<com.truecaller.featuretoggles.e> w;
  private Provider<r> x;
  private Provider<com.truecaller.utils.d> y;
  private Provider<g> z;
  
  private bb(com.truecaller.common.a parama, t paramt, com.truecaller.analytics.d paramd)
  {
    b = parama;
    c = paramd;
    d = new c(parama);
    e = new h(parama);
    f = new m(paramt);
    g = k.a(d);
    h = new k(paramt);
    i = new b(paramd);
    j = ar.a(d, e, f, g, h, i);
    k = dagger.a.c.a(j);
    l = az.a(d);
    m = dagger.a.c.a(l);
    n = new e(parama);
    o = bm.a(d, n, h, i);
    p = dagger.a.c.a(o);
    q = new j(parama);
    r = new d(parama);
    s = new g(parama);
    t = ai.a(s, d, n, i);
    u = be.a(d, n, q, r, t);
    v = dagger.a.c.a(u);
    w = new f(parama);
    x = new i(parama);
    y = new l(paramt);
    z = h.a(d, k, m, p, v, r, w, x, n, y, t);
    A = dagger.a.c.a(z);
  }
  
  public static a e()
  {
    return new a((byte)0);
  }
  
  public final e a()
  {
    return (e)A.get();
  }
  
  public final bc b()
  {
    return (bc)v.get();
  }
  
  public final ag c()
  {
    return new ah((f)dagger.a.g.a(b.t(), "Cannot return null from a non-@Nullable component method"), (Context)dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method"), (com.truecaller.common.g.a)dagger.a.g.a(b.c(), "Cannot return null from a non-@Nullable component method"), (b)dagger.a.g.a(c.c(), "Cannot return null from a non-@Nullable component method"));
  }
  
  public final bk d()
  {
    return (bk)p.get();
  }
  
  public static final class a
  {
    public com.truecaller.common.a a;
    public t b;
    public com.truecaller.analytics.d c;
  }
  
  static final class b
    implements Provider<b>
  {
    private final com.truecaller.analytics.d a;
    
    b(com.truecaller.analytics.d paramd)
    {
      a = paramd;
    }
  }
  
  static final class c
    implements Provider<Context>
  {
    private final com.truecaller.common.a a;
    
    c(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
  
  static final class d
    implements Provider<f>
  {
    private final com.truecaller.common.a a;
    
    d(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
  
  static final class e
    implements Provider<com.truecaller.common.g.a>
  {
    private final com.truecaller.common.a a;
    
    e(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
  
  static final class f
    implements Provider<com.truecaller.featuretoggles.e>
  {
    private final com.truecaller.common.a a;
    
    f(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
  
  static final class g
    implements Provider<f>
  {
    private final com.truecaller.common.a a;
    
    g(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
  
  static final class h
    implements Provider<com.truecaller.multisim.h>
  {
    private final com.truecaller.common.a a;
    
    h(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
  
  static final class i
    implements Provider<r>
  {
    private final com.truecaller.common.a a;
    
    i(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
  
  static final class j
    implements Provider<f>
  {
    private final com.truecaller.common.a a;
    
    j(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
  
  static final class k
    implements Provider<com.truecaller.utils.a>
  {
    private final t a;
    
    k(t paramt)
    {
      a = paramt;
    }
  }
  
  static final class l
    implements Provider<com.truecaller.utils.d>
  {
    private final t a;
    
    l(t paramt)
    {
      a = paramt;
    }
  }
  
  static final class m
    implements Provider<l>
  {
    private final t a;
    
    m(t paramt)
    {
      a = paramt;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.bb
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */