package com.truecaller.backup;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import androidx.work.p;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.bp;
import com.truecaller.common.background.TrackedWorker;
import com.truecaller.common.g.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;

public final class BackupLogWorker
  extends TrackedWorker
{
  public static final BackupLogWorker.a d = new BackupLogWorker.a((byte)0);
  @Inject
  public b b;
  @Inject
  public a c;
  
  public BackupLogWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public static final void e()
  {
    p localp = p.a();
    k.a(localp, "WorkManager.getInstance()");
    localp.a("BackupLogWorker", androidx.work.g.a, d.a().b());
  }
  
  public final b b()
  {
    b localb = b;
    if (localb == null) {
      k.a("analytics");
    }
    return localb;
  }
  
  public final boolean c()
  {
    Object localObject = c;
    if (localObject == null) {
      k.a("coreSettings");
    }
    localObject = ((a)localObject).a("accountAutobackupLogInfo");
    if (localObject != null)
    {
      int i;
      if (((CharSequence)localObject).length() > 0) {
        i = 1;
      } else {
        i = 0;
      }
      if (i == 1) {
        return true;
      }
    }
    return false;
  }
  
  public final ListenableWorker.a d()
  {
    Object localObject1 = c;
    if (localObject1 == null) {
      k.a("coreSettings");
    }
    Object localObject2 = new d((a)localObject1);
    Object localObject3 = a.a("accountAutobackupLogInfo");
    localObject1 = null;
    if (localObject3 != null)
    {
      k.a(localObject3, "settings.getString(CoreS…_LOG_INFO) ?: return null");
      a.a("accountAutobackupLogInfo", null);
      localObject2 = (Iterable)c.n.m.c((CharSequence)localObject3, new String[] { ";" }, false, 6);
      localObject1 = (Collection)new ArrayList(c.a.m.a((Iterable)localObject2, 10));
      localObject2 = ((Iterable)localObject2).iterator();
      while (((Iterator)localObject2).hasNext()) {
        ((Collection)localObject1).add(d.a((String)((Iterator)localObject2).next()));
      }
      localObject1 = (List)localObject1;
    }
    if (localObject1 == null)
    {
      localObject1 = ListenableWorker.a.c();
      k.a(localObject1, "Result.failure()");
      return (ListenableWorker.a)localObject1;
    }
    localObject3 = ((Iterable)localObject1).iterator();
    while (((Iterator)localObject3).hasNext())
    {
      Map localMap = (Map)((Iterator)localObject3).next();
      b localb = b();
      e.a locala = new e.a("AccountBackup");
      localObject2 = (String)localMap.get("backup_action_key");
      localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = "";
      }
      locala = locala.a("BackupAction", (String)localObject1);
      localObject2 = (String)localMap.get("backup_file_exists_key");
      localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = "false";
      }
      locala = locala.a("BackupFileExists", (String)localObject1);
      localObject2 = (String)localMap.get("account_state_valid");
      localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = "false";
      }
      localObject1 = locala.a("AccountStateValid", (String)localObject1).a();
      k.a(localObject1, "AnalyticsEvent.Builder(A…                 .build()");
      localb.b((e)localObject1);
    }
    localObject1 = ListenableWorker.a.a();
    k.a(localObject1, "Result.success()");
    return (ListenableWorker.a)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.BackupLogWorker
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */