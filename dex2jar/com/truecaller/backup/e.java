package com.truecaller.backup;

import android.support.v4.app.Fragment;
import c.d.c;

public abstract interface e
{
  public abstract Object a(Fragment paramFragment, c<? super Boolean> paramc);
  
  public abstract Object a(c<? super Boolean> paramc);
  
  public abstract boolean a();
  
  public abstract Object b(Fragment paramFragment, c<? super Boolean> paramc);
  
  public abstract Object b(c<? super BackupResult> paramc);
  
  public abstract void b();
  
  public abstract Object c();
  
  public abstract Object c(c<? super BackupResult> paramc);
  
  public abstract void d();
}

/* Location:
 * Qualified Name:     com.truecaller.backup.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */