package com.truecaller.backup;

import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.h;
import com.truecaller.messaging.transport.ad.b;
import dagger.a.d;
import javax.inject.Provider;

public final class cb
  implements d<ca>
{
  private final Provider<ad.b> a;
  private final Provider<h> b;
  private final Provider<e> c;
  
  private cb(Provider<ad.b> paramProvider, Provider<h> paramProvider1, Provider<e> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static cb a(Provider<ad.b> paramProvider, Provider<h> paramProvider1, Provider<e> paramProvider2)
  {
    return new cb(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.cb
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */