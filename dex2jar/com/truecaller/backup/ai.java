package com.truecaller.backup;

import android.content.Context;
import c.d.f;
import com.truecaller.analytics.b;
import com.truecaller.common.g.a;
import dagger.a.d;
import javax.inject.Provider;

public final class ai
  implements d<ah>
{
  private final Provider<f> a;
  private final Provider<Context> b;
  private final Provider<a> c;
  private final Provider<b> d;
  
  private ai(Provider<f> paramProvider, Provider<Context> paramProvider1, Provider<a> paramProvider2, Provider<b> paramProvider3)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static ai a(Provider<f> paramProvider, Provider<Context> paramProvider1, Provider<a> paramProvider2, Provider<b> paramProvider3)
  {
    return new ai(paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.ai
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */