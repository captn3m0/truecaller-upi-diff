package com.truecaller.backup;

import android.content.ContentProviderOperation;
import android.content.Intent;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.aa;
import com.truecaller.featuretoggles.e;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.h;
import com.truecaller.messaging.transport.a;
import com.truecaller.messaging.transport.ad;
import com.truecaller.messaging.transport.ad.a.a;
import com.truecaller.messaging.transport.ad.b;
import com.truecaller.messaging.transport.f;
import com.truecaller.messaging.transport.i;
import com.truecaller.messaging.transport.l;
import com.truecaller.messaging.transport.l.a;
import com.truecaller.messaging.transport.l.b;
import com.truecaller.utils.q;
import java.util.List;
import java.util.Set;
import org.a.a.a.g;

public abstract class af
  implements l<ad>
{
  private final ad.b a;
  private final h b;
  private final e c;
  
  public af(ad.b paramb, h paramh, e parame)
  {
    a = paramb;
    b = paramh;
    c = parame;
  }
  
  public final long a(long paramLong)
  {
    throw ((Throwable)new IllegalStateException("Backup transport can not be used to store a message."));
  }
  
  public final long a(f paramf, i parami, r paramr, org.a.a.b paramb1, org.a.a.b paramb2, int paramInt, List<ContentProviderOperation> paramList, q paramq, boolean paramBoolean1, boolean paramBoolean2, Set<Long> paramSet)
  {
    c.g.b.k.b(paramf, "threadInfoCache");
    c.g.b.k.b(parami, "participantCache");
    c.g.b.k.b(paramr, "localCursor");
    c.g.b.k.b(paramb1, "timeTo");
    c.g.b.k.b(paramb2, "timeFrom");
    c.g.b.k.b(paramList, "operations");
    c.g.b.k.b(paramq, "trace");
    c.g.b.k.b(paramSet, "messagesToClassify");
    if (c.w().a()) {
      while (paramr.moveToNext())
      {
        if (paramr.o() == 0) {
          paramSet.add(Long.valueOf(paramr.a()));
        }
        if (paramSet.size() >= paramInt) {
          return l.b.a(paramr.e());
        }
      }
    }
    return Long.MIN_VALUE;
  }
  
  public final l.a a(Message paramMessage, Participant[] paramArrayOfParticipant)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramArrayOfParticipant, "recipients");
    throw ((Throwable)new IllegalStateException("Backup transport can not be used to enqueue a message."));
  }
  
  public final String a()
  {
    return "backup";
  }
  
  public final String a(String paramString)
  {
    c.g.b.k.b(paramString, "simToken");
    return "-1";
  }
  
  public final void a(Intent paramIntent, int paramInt)
  {
    c.g.b.k.b(paramIntent, "intent");
    throw ((Throwable)new IllegalStateException("Backup transport can not be used to handle received messages."));
  }
  
  public final void a(Message paramMessage, String paramString1, String paramString2)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramString2, "initiatedVia");
    throw ((Throwable)new IllegalStateException("Backup transport does not support sending reactions"));
  }
  
  public final void a(org.a.a.b paramb)
  {
    c.g.b.k.b(paramb, "time");
    b.a(4, a);
  }
  
  public final boolean a(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    throw ((Throwable)new IllegalStateException("Backup transport can not be used to store a message."));
  }
  
  public final boolean a(Message paramMessage, Entity paramEntity)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramEntity, "entity");
    return false;
  }
  
  public final boolean a(Message paramMessage, ad paramad)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramad, "transaction");
    paramad.a(paramad.a(TruecallerContract.aa.a(paramMessage.a())).a("status", Integer.valueOf(9)).a());
    return true;
  }
  
  public final boolean a(Participant paramParticipant)
  {
    c.g.b.k.b(paramParticipant, "participant");
    return false;
  }
  
  public final boolean a(TransportInfo paramTransportInfo, long paramLong1, long paramLong2, ad paramad)
  {
    c.g.b.k.b(paramTransportInfo, "info");
    c.g.b.k.b(paramad, "transaction");
    paramad.a(paramad.a(TruecallerContract.aa.a(paramTransportInfo.c())).a("read", Integer.valueOf(1)).a("seen", Integer.valueOf(1)).a());
    return true;
  }
  
  public final boolean a(TransportInfo paramTransportInfo, ad paramad)
  {
    c.g.b.k.b(paramTransportInfo, "info");
    c.g.b.k.b(paramad, "transaction");
    paramad.a(paramad.a(TruecallerContract.aa.a(paramTransportInfo.c())).a("seen", Integer.valueOf(1)).a());
    return true;
  }
  
  public final boolean a(TransportInfo paramTransportInfo, ad paramad, boolean paramBoolean)
  {
    c.g.b.k.b(paramTransportInfo, "info");
    c.g.b.k.b(paramad, "transaction");
    paramad.a(paramad.b(TruecallerContract.aa.a(paramTransportInfo.c())).a());
    return true;
  }
  
  public final boolean a(ad paramad)
  {
    c.g.b.k.b(paramad, "transaction");
    int i = 0;
    try
    {
      if (paramad.a()) {
        return false;
      }
      paramad = a.a(paramad);
      c.g.b.k.a(paramad, "transactionExecutor.execute(transaction)");
      int j = paramad.length;
      if (j == 0) {
        i = 1;
      }
      return i ^ 0x1;
    }
    catch (Exception paramad)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramad);
    }
    return false;
  }
  
  public final boolean a(String paramString, a parama)
  {
    c.g.b.k.b(paramString, "text");
    c.g.b.k.b(parama, "result");
    return false;
  }
  
  public final ad b()
  {
    return new ad(TruecallerContract.a());
  }
  
  public final void b(long paramLong)
  {
    throw ((Throwable)new IllegalStateException("Backup transport can not retry messages."));
  }
  
  public final boolean b(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return false;
  }
  
  public final boolean b(ad paramad)
  {
    c.g.b.k.b(paramad, "transaction");
    return (!paramad.a()) && (c.g.b.k.a(paramad.c(), TruecallerContract.a()));
  }
  
  public final boolean c()
  {
    return false;
  }
  
  public final boolean c(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return false;
  }
  
  public final com.truecaller.messaging.transport.k d(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    throw ((Throwable)new IllegalStateException("Backup transport can not be used to send a message."));
  }
  
  public final org.a.a.b d()
  {
    return new org.a.a.b(b.a(4));
  }
  
  public final int e(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return 0;
  }
  
  public final boolean e()
  {
    return false;
  }
  
  public final boolean f(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.af
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */