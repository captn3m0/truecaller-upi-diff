package com.truecaller.backup;

import android.content.Context;
import com.truecaller.analytics.b;
import com.truecaller.callhistory.k;
import com.truecaller.multisim.h;
import com.truecaller.utils.a;
import com.truecaller.utils.l;
import dagger.a.d;
import javax.inject.Provider;

public final class ar
  implements d<aq>
{
  private final Provider<Context> a;
  private final Provider<h> b;
  private final Provider<l> c;
  private final Provider<k> d;
  private final Provider<a> e;
  private final Provider<b> f;
  
  private ar(Provider<Context> paramProvider, Provider<h> paramProvider1, Provider<l> paramProvider2, Provider<k> paramProvider3, Provider<a> paramProvider4, Provider<b> paramProvider5)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
  }
  
  public static ar a(Provider<Context> paramProvider, Provider<h> paramProvider1, Provider<l> paramProvider2, Provider<k> paramProvider3, Provider<a> paramProvider4, Provider<b> paramProvider5)
  {
    return new ar(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.ar
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */