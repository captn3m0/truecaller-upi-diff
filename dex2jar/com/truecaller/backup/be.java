package com.truecaller.backup;

import android.content.Context;
import c.d.f;
import com.truecaller.common.g.a;
import dagger.a.d;
import javax.inject.Provider;

public final class be
  implements d<bd>
{
  private final Provider<Context> a;
  private final Provider<a> b;
  private final Provider<f> c;
  private final Provider<f> d;
  private final Provider<ag> e;
  
  private be(Provider<Context> paramProvider, Provider<a> paramProvider1, Provider<f> paramProvider2, Provider<f> paramProvider3, Provider<ag> paramProvider4)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static be a(Provider<Context> paramProvider, Provider<a> paramProvider1, Provider<f> paramProvider2, Provider<f> paramProvider3, Provider<ag> paramProvider4)
  {
    return new be(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.be
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */