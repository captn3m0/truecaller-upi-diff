package com.truecaller.backup;

import android.content.Context;
import c.d.f;
import com.truecaller.common.account.r;
import com.truecaller.common.g.a;
import com.truecaller.featuretoggles.e;
import javax.inject.Provider;

public final class h
  implements dagger.a.d<g>
{
  private final Provider<Context> a;
  private final Provider<ap> b;
  private final Provider<ax> c;
  private final Provider<bk> d;
  private final Provider<bc> e;
  private final Provider<f> f;
  private final Provider<e> g;
  private final Provider<r> h;
  private final Provider<a> i;
  private final Provider<com.truecaller.utils.d> j;
  private final Provider<ag> k;
  
  private h(Provider<Context> paramProvider, Provider<ap> paramProvider1, Provider<ax> paramProvider2, Provider<bk> paramProvider3, Provider<bc> paramProvider4, Provider<f> paramProvider5, Provider<e> paramProvider6, Provider<r> paramProvider7, Provider<a> paramProvider8, Provider<com.truecaller.utils.d> paramProvider9, Provider<ag> paramProvider10)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
    k = paramProvider10;
  }
  
  public static h a(Provider<Context> paramProvider, Provider<ap> paramProvider1, Provider<ax> paramProvider2, Provider<bk> paramProvider3, Provider<bc> paramProvider4, Provider<f> paramProvider5, Provider<e> paramProvider6, Provider<r> paramProvider7, Provider<a> paramProvider8, Provider<com.truecaller.utils.d> paramProvider9, Provider<ag> paramProvider10)
  {
    return new h(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */