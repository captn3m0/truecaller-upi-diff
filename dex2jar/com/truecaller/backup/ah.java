package com.truecaller.backup;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.analytics.b;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.extensions.d;
import java.io.Closeable;
import java.io.File;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.g;

public final class ah
  implements ag
{
  final com.truecaller.common.g.a a;
  private final c.n.k b;
  private final f c;
  private final Context d;
  private final b e;
  
  @Inject
  public ah(@Named("IO") f paramf, Context paramContext, com.truecaller.common.g.a parama, b paramb)
  {
    c = paramf;
    d = paramContext;
    a = parama;
    e = paramb;
    b = new c.n.k("^\\++");
  }
  
  public final File a()
  {
    try
    {
      Object localObject = com.truecaller.content.c.ag.a(d, com.truecaller.content.c.ag.b(), e);
      c.g.b.k.a(localObject, "TruecallerDatabaseHelper…ableHelpers(), analytics)");
      localObject = ((com.truecaller.content.c.ag)localObject).getWritableDatabase().rawQuery("PRAGMA wal_checkpoint(FULL)", null);
      c.g.b.k.a(localObject, "cursor");
      ((Cursor)localObject).getCount();
      d.a((Closeable)localObject);
    }
    catch (Exception localException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localException);
    }
    return d.getDatabasePath("tc.db");
  }
  
  public final Object a(c<? super x> paramc)
  {
    if ((paramc instanceof ah.a))
    {
      localObject1 = (ah.a)paramc;
      if ((b & 0x80000000) != 0)
      {
        b += Integer.MIN_VALUE;
        paramc = (c<? super x>)localObject1;
        break label48;
      }
    }
    paramc = new ah.a(this, paramc);
    label48:
    Object localObject2 = a;
    Object localObject1 = c.d.a.a.a;
    switch (b)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 1: 
      if ((localObject2 instanceof o.b)) {
        throw a;
      }
      break;
    case 0: 
      if ((localObject2 instanceof o.b)) {
        break label183;
      }
      if (a.e("key_backup_fetched_timestamp")) {
        return x.a;
      }
      localObject2 = c;
      m localm = (m)new ah.b(this, null);
      d = this;
      b = 1;
      if (g.a((f)localObject2, localm, paramc) == localObject1) {
        return localObject1;
      }
      break;
    }
    return x.a;
    label183:
    throw a;
  }
  
  public final String a(BackupFile paramBackupFile)
  {
    c.g.b.k.b(paramBackupFile, "backupFile");
    Object localObject = a.a("profileNumber");
    if (localObject == null) {
      return null;
    }
    c.g.b.k.a(localObject, "coreSettings.getString(C…LE_NUMBER) ?: return null");
    StringBuilder localStringBuilder = new StringBuilder();
    localObject = (CharSequence)localObject;
    localStringBuilder.append(b.a((CharSequence)localObject, ""));
    localStringBuilder.append(paramBackupFile.getNameSuffix());
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.ah
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */