package com.truecaller.backup;

import android.content.Context;
import android.support.v4.app.Fragment;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import c.o.b;
import com.truecaller.common.account.r;
import com.truecaller.utils.d;
import javax.inject.Inject;
import javax.inject.Named;

public final class g
  implements e
{
  private final Context a;
  private final ap b;
  private final ax c;
  private final bk d;
  private final bc e;
  private final f f;
  private final com.truecaller.featuretoggles.e g;
  private final r h;
  private final com.truecaller.common.g.a i;
  private final d j;
  private final ag k;
  
  @Inject
  public g(Context paramContext, ap paramap, ax paramax, bk parambk, bc parambc, @Named("Async") f paramf, @Named("features_registry") com.truecaller.featuretoggles.e parame, r paramr, com.truecaller.common.g.a parama, d paramd, ag paramag)
  {
    a = paramContext;
    b = paramap;
    c = paramax;
    d = parambk;
    e = parambc;
    f = paramf;
    g = parame;
    h = paramr;
    i = parama;
    j = paramd;
    k = paramag;
  }
  
  public final Object a(Fragment paramFragment, c<? super Boolean> paramc)
  {
    return e.a(paramFragment, paramc);
  }
  
  public final Object a(c<? super Boolean> paramc)
  {
    return e.a(paramc);
  }
  
  public final boolean a()
  {
    return (g.l().a()) && (h.c()) && (i.b("backup_enabled")) && ((k.a(j.l(), "kenzo") ^ true));
  }
  
  public final Object b(Fragment paramFragment, c<? super Boolean> paramc)
  {
    return e.b(paramFragment, paramc);
  }
  
  public final Object b(c<? super BackupResult> paramc)
  {
    Object localObject1;
    if ((paramc instanceof g.b))
    {
      localObject1 = (g.b)paramc;
      if ((b & 0x80000000) != 0)
      {
        b += Integer.MIN_VALUE;
        paramc = (c<? super BackupResult>)localObject1;
        break label48;
      }
    }
    paramc = new g.b(this, paramc);
    label48:
    Object localObject2 = a;
    c.d.a.a locala = c.d.a.a.a;
    switch (b)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 2: 
      if (!(localObject2 instanceof o.b)) {
        break label246;
      }
      throw a;
    case 1: 
      localObject1 = (g)d;
      if ((localObject2 instanceof o.b)) {
        throw a;
      }
      break;
    case 0: 
      if ((localObject2 instanceof o.b)) {
        break label264;
      }
      localObject1 = e;
      d = this;
      b = 1;
      localObject2 = ((bc)localObject1).a(null, paramc);
      if (localObject2 == locala) {
        return locala;
      }
      localObject1 = this;
    }
    if (!((Boolean)localObject2).booleanValue()) {
      return BackupResult.ErrorNetwork;
    }
    localObject2 = f;
    m localm = (m)new g.c((g)localObject1, null);
    d = localObject1;
    b = 2;
    paramc = kotlinx.coroutines.g.a((f)localObject2, localm, paramc);
    localObject2 = paramc;
    if (paramc == locala) {
      return locala;
    }
    label246:
    paramc = (BackupResult)localObject2;
    if (paramc != BackupResult.Success) {
      return paramc;
    }
    return BackupResult.Success;
    label264:
    throw a;
  }
  
  public final void b()
  {
    e.a();
  }
  
  public final Object c()
  {
    String str = k.a(BackupFile.DB);
    if (str == null) {
      return c.d.b.a.b.a(0L);
    }
    return e.c(str);
  }
  
  public final Object c(c<? super BackupResult> paramc)
  {
    Object localObject1;
    if ((paramc instanceof g.d))
    {
      localObject1 = (g.d)paramc;
      if ((b & 0x80000000) != 0)
      {
        b += Integer.MIN_VALUE;
        paramc = (c<? super BackupResult>)localObject1;
        break label48;
      }
    }
    paramc = new g.d(this, paramc);
    label48:
    Object localObject2 = a;
    c.d.a.a locala = c.d.a.a.a;
    switch (b)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 2: 
      if (!(localObject2 instanceof o.b)) {
        break label246;
      }
      throw a;
    case 1: 
      localObject1 = (g)d;
      if ((localObject2 instanceof o.b)) {
        throw a;
      }
      break;
    case 0: 
      if ((localObject2 instanceof o.b)) {
        break label264;
      }
      localObject1 = e;
      d = this;
      b = 1;
      localObject2 = ((bc)localObject1).a(null, paramc);
      if (localObject2 == locala) {
        return locala;
      }
      localObject1 = this;
    }
    if (!((Boolean)localObject2).booleanValue()) {
      return BackupResult.ErrorNetwork;
    }
    localObject2 = f;
    m localm = (m)new g.e((g)localObject1, null);
    d = localObject1;
    b = 2;
    paramc = kotlinx.coroutines.g.a((f)localObject2, localm, paramc);
    localObject2 = paramc;
    if (paramc == locala) {
      return locala;
    }
    label246:
    paramc = (BackupResult)localObject2;
    if (paramc != BackupResult.Success) {
      return paramc;
    }
    return BackupResult.Success;
    label264:
    throw a;
  }
  
  public final void d() {}
}

/* Location:
 * Qualified Name:     com.truecaller.backup.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */