package com.truecaller.backup;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import c.d.c;
import c.d.h;
import c.g.a.m;
import c.g.b.k;
import c.l;
import c.n;
import c.o;
import c.o.a;
import c.o.b;
import c.x;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions.Builder;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveClient;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.drive.DriveResourceClient;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.MetadataChangeSet.Builder;
import com.google.android.gms.drive.metadata.CustomPropertyKey;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query.Builder;
import com.google.android.gms.drive.query.SearchableField;
import com.google.android.gms.drive.query.SortOrder;
import com.google.android.gms.drive.query.SortOrder.Builder;
import com.google.android.gms.drive.query.SortableField;
import com.google.android.gms.tasks.Task;
import com.truecaller.log.AssertionUtil;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;
import kotlinx.coroutines.g;

public final class bd
  implements bc
{
  DriveClient a;
  private DriveResourceClient b;
  private c<? super x> c;
  private final Context d;
  private final com.truecaller.common.g.a e;
  private final c.d.f f;
  private final c.d.f g;
  private final ag h;
  
  @Inject
  public bd(Context paramContext, com.truecaller.common.g.a parama, @Named("UI") c.d.f paramf1, @Named("Async") c.d.f paramf2, ag paramag)
  {
    d = paramContext;
    e = parama;
    f = paramf1;
    g = paramf2;
    h = paramag;
  }
  
  private final GoogleSignInClient a(Activity paramActivity)
  {
    GoogleSignInOptions localGoogleSignInOptions = new GoogleSignInOptions.Builder().a(Drive.c, new Scope[0]).a(Drive.b, new Scope[0]).b().d();
    if (paramActivity == null)
    {
      paramActivity = GoogleSignIn.a(d, localGoogleSignInOptions);
      k.a(paramActivity, "GoogleSignIn.getClient(context, signInOptions)");
      return paramActivity;
    }
    paramActivity = GoogleSignIn.a(paramActivity, localGoogleSignInOptions);
    k.a(paramActivity, "GoogleSignIn.getClient(activity, signInOptions)");
    return paramActivity;
  }
  
  private final BackupResult a(String paramString, Map<String, String> paramMap, c.g.a.b<? super OutputStream, x> paramb)
  {
    int i = 1;
    "Writing to file with title: ".concat(String.valueOf(paramString));
    DriveResourceClient localDriveResourceClient = b;
    if (localDriveResourceClient == null) {
      return BackupResult.ErrorClient;
    }
    Object localObject3 = d(paramString);
    Object localObject2 = null;
    Object localObject1 = localObject3;
    if (localObject3 == null)
    {
      "Creating file with title: ".concat(String.valueOf(paramString));
      localObject1 = d();
      if (localObject1 == null)
      {
        localObject1 = null;
      }
      else
      {
        paramString = new MetadataChangeSet.Builder().b(paramString).a("application/json").a();
        localObject3 = b;
        if (localObject3 != null)
        {
          paramString = ((DriveResourceClient)localObject3).createFile((DriveFolder)localObject1, paramString, null);
          if (paramString != null)
          {
            localObject1 = (DriveFile)a(paramString);
            break label135;
          }
        }
        localObject1 = null;
      }
    }
    label135:
    if (localObject1 == null) {
      return BackupResult.ErrorFile;
    }
    if (paramMap != null)
    {
      paramString = new MetadataChangeSet.Builder();
      paramMap = ((Iterable)paramMap.entrySet()).iterator();
      while (paramMap.hasNext())
      {
        localObject3 = (Map.Entry)paramMap.next();
        paramString.a(new CustomPropertyKey((String)((Map.Entry)localObject3).getKey(), 1), (String)((Map.Entry)localObject3).getValue());
      }
      paramString = paramString.a();
    }
    else
    {
      paramString = null;
    }
    paramMap = localDriveResourceClient.openFile((DriveFile)localObject1, 536870912);
    if (paramMap != null)
    {
      paramMap = (DriveContents)a(paramMap);
      if (paramMap != null) {
        try
        {
          localObject1 = paramMap.getOutputStream();
          k.a(localObject1, "driveContents.outputStream");
          paramb.invoke(localObject1);
          paramMap = localDriveResourceClient.commitContents(paramMap, paramString);
          paramString = (String)localObject2;
          if (paramMap != null) {
            paramString = b(paramMap);
          }
          if (paramString == null) {
            i = 0;
          }
          if (i != 0) {
            return BackupResult.Success;
          }
          return BackupResult.ErrorCommit;
        }
        catch (IOException paramString)
        {
          AssertionUtil.reportThrowableButNeverCrash((Throwable)paramString);
          return BackupResult.ErrorWrite;
        }
      }
    }
    return BackupResult.ErrorOpen;
  }
  
  private final InputStream a(DriveFile paramDriveFile)
  {
    DriveResourceClient localDriveResourceClient = b;
    if (localDriveResourceClient == null) {
      return null;
    }
    paramDriveFile = localDriveResourceClient.openFile(paramDriveFile, 268435456);
    if (paramDriveFile != null)
    {
      paramDriveFile = (DriveContents)a(paramDriveFile);
      if (paramDriveFile != null)
      {
        paramDriveFile = paramDriveFile.getInputStream();
        if (paramDriveFile == null) {
          return null;
        }
        return paramDriveFile;
      }
    }
    return null;
  }
  
  private final <T> T a(Task<T> paramTask)
  {
    return (T)cf.a(paramTask, (c.g.a.b)new bd.a(this));
  }
  
  private final boolean a(GoogleSignInAccount paramGoogleSignInAccount, Activity paramActivity)
  {
    if (paramActivity != null) {}
    try
    {
      Context localContext = (Context)paramActivity;
      break label17;
      localContext = d;
      label17:
      a = Drive.a(localContext, paramGoogleSignInAccount);
      if (paramActivity != null) {
        paramActivity = (Context)paramActivity;
      } else {
        paramActivity = d;
      }
      b = Drive.b(paramActivity, paramGoogleSignInAccount);
      return true;
    }
    catch (Exception paramGoogleSignInAccount)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramGoogleSignInAccount);
    }
    return false;
  }
  
  private final x b(Task<Void> paramTask)
  {
    return cf.b(paramTask, (c.g.a.b)new bd.b(this));
  }
  
  private final DriveFile d(String paramString)
  {
    DriveFolder localDriveFolder = d();
    if (localDriveFolder == null) {
      return null;
    }
    Object localObject = new SortOrder.Builder().a(SortableField.c).a();
    paramString = new Query.Builder().a(Filters.a(SearchableField.a, paramString)).a((SortOrder)localObject).a();
    localObject = b;
    if (localObject != null)
    {
      paramString = ((DriveResourceClient)localObject).queryChildren(localDriveFolder, paramString);
      if (paramString != null)
      {
        paramString = (MetadataBuffer)a(paramString);
        if (paramString == null) {
          return null;
        }
        if (paramString.getCount() == 0) {
          return null;
        }
        paramString = paramString.a(0);
        k.a(paramString, "metadataBuffer[0]");
        paramString = paramString.getDriveId();
        if (paramString != null) {
          return paramString.a();
        }
        return null;
      }
    }
    return null;
  }
  
  private final DriveFolder d()
  {
    boolean bool = e.b("backupForceRootFolder");
    Object localObject;
    if (bool == true)
    {
      localObject = b;
      if (localObject != null)
      {
        localObject = ((DriveResourceClient)localObject).getRootFolder();
        if (localObject != null) {
          return (DriveFolder)a((Task)localObject);
        }
      }
      return null;
    }
    if (!bool)
    {
      localObject = b;
      if (localObject != null)
      {
        localObject = ((DriveResourceClient)localObject).getAppFolder();
        if (localObject != null) {
          return (DriveFolder)a((Task)localObject);
        }
      }
      return null;
    }
    throw new l();
  }
  
  public final Object a(Fragment paramFragment, c<? super Boolean> paramc)
  {
    if ((paramc instanceof bd.e))
    {
      localObject1 = (bd.e)paramc;
      if ((b & 0x80000000) != 0)
      {
        b += Integer.MIN_VALUE;
        paramc = (c<? super Boolean>)localObject1;
        break label55;
      }
    }
    paramc = new bd.e(this, paramc);
    label55:
    Object localObject2 = a;
    c.d.a.a locala = c.d.a.a.a;
    switch (b)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 1: 
      localObject1 = (android.support.v4.app.f)g;
      paramFragment = (bd)d;
      if (!(localObject2 instanceof o.b)) {
        paramc = (c<? super Boolean>)localObject1;
      } else {
        throw a;
      }
      break;
    case 0: 
      if ((localObject2 instanceof o.b)) {
        break label500;
      }
      Object localObject3 = GoogleSignIn.a(d);
      if (paramFragment == null)
      {
        if (localObject3 == null) {
          return Boolean.FALSE;
        }
        return Boolean.valueOf(a((GoogleSignInAccount)localObject3, null));
      }
      localObject1 = paramFragment.getActivity();
      if (localObject1 == null) {
        return Boolean.FALSE;
      }
      k.a(localObject1, "parentFragment.activity ?: return false");
      int j = GoogleApiAvailability.a().a((Context)localObject1);
      if (j != 0)
      {
        e.b((kotlinx.coroutines.ag)bg.a, f, (m)new bd.f((android.support.v4.app.f)localObject1, j, null), 2);
        return Boolean.FALSE;
      }
      localObject2 = a((Activity)localObject1);
      if (!e.b("backupSignInRequired")) {
        if (GoogleSignIn.a((GoogleSignInAccount)localObject3, new Scope[] { Drive.c }))
        {
          i = 0;
          break label313;
        }
      }
      int i = 1;
      label313:
      if ((localObject3 == null) || (i != 0))
      {
        e.b("backupSignInRequired", false);
        d = this;
        e = paramFragment;
        f = localObject3;
        g = localObject1;
        i = j;
        h = localObject2;
        b = 1;
        localObject3 = new h(c.d.a.b.a(paramc));
        c = ((c)localObject3);
        e.b((kotlinx.coroutines.ag)bg.a, f, (m)new bd.d(null, this, paramFragment, (GoogleSignInClient)localObject2), 2);
        paramFragment = ((h)localObject3).b();
        if (paramFragment == c.d.a.a.a) {
          k.b(paramc, "frame");
        }
        if (paramFragment == locala) {
          return locala;
        }
      }
      paramFragment = this;
      paramc = (c<? super Boolean>)localObject1;
    }
    Object localObject1 = GoogleSignIn.a((Context)paramc);
    if (localObject1 == null) {
      return Boolean.FALSE;
    }
    k.a(localObject1, "GoogleSignIn.getLastSign…activity) ?: return false");
    return Boolean.valueOf(paramFragment.a((GoogleSignInAccount)localObject1, (Activity)paramc));
    label500:
    throw a;
  }
  
  public final Object a(c<? super Boolean> paramc)
  {
    return g.a(g, (m)new bd.c(this, null), paramc);
  }
  
  public final Object a(String paramString)
  {
    "Reading file with title: ".concat(String.valueOf(paramString));
    paramString = d(paramString);
    if (paramString == null) {
      return null;
    }
    return a(paramString);
  }
  
  public final Object a(String paramString, InputStream paramInputStream, Map<String, String> paramMap)
  {
    return a(paramString, paramMap, (c.g.a.b)new bd.h(paramInputStream));
  }
  
  public final Object a(String paramString, byte[] paramArrayOfByte)
  {
    return a(paramString, null, (c.g.a.b)new bd.g(paramArrayOfByte));
  }
  
  public final void a()
  {
    c();
  }
  
  public final Object b()
  {
    String str = h.a(BackupFile.CALL_LOG);
    if (str == null) {
      return c.d.b.a.b.a(0L);
    }
    return c(str);
  }
  
  public final Object b(Fragment paramFragment, c<? super Boolean> paramc)
  {
    if (GoogleSignIn.a(d) != null)
    {
      Task localTask = a(null).b();
      if (localTask != null) {
        b(localTask);
      }
    }
    return a(paramFragment, paramc);
  }
  
  public final Object b(String paramString)
  {
    "Reading file with title: ".concat(String.valueOf(paramString));
    Object localObject1 = b;
    if (localObject1 == null) {
      return null;
    }
    paramString = d(paramString);
    if (paramString == null) {
      return null;
    }
    localObject1 = ((DriveResourceClient)localObject1).getMetadata((DriveResource)paramString);
    k.a(localObject1, "driveResourceClient.getMetadata(driveFile)");
    localObject1 = (Metadata)a((Task)localObject1);
    if (localObject1 == null) {
      return null;
    }
    paramString = a(paramString);
    if (paramString == null) {
      return null;
    }
    Object localObject2 = ((Metadata)localObject1).getCustomProperties();
    k.a(localObject2, "metadata.customProperties");
    localObject1 = (Map)new LinkedHashMap(c.a.ag.a(((Map)localObject2).size()));
    localObject2 = ((Iterable)((Map)localObject2).entrySet()).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      Map.Entry localEntry = (Map.Entry)((Iterator)localObject2).next();
      Object localObject3 = localEntry.getKey();
      k.a(localObject3, "it.key");
      ((Map)localObject1).put(((CustomPropertyKey)localObject3).a(), localEntry.getValue());
    }
    return new n(paramString, localObject1);
  }
  
  public final Object c(String paramString)
  {
    "Finding last modified time for: ".concat(String.valueOf(paramString));
    DriveResourceClient localDriveResourceClient = b;
    long l2 = 0L;
    if (localDriveResourceClient == null) {
      return c.d.b.a.b.a(0L);
    }
    paramString = d(paramString);
    if (paramString == null) {
      return c.d.b.a.b.a(0L);
    }
    paramString = localDriveResourceClient.getMetadata((DriveResource)paramString);
    long l1 = l2;
    if (paramString != null)
    {
      paramString = (Metadata)a(paramString);
      l1 = l2;
      if (paramString != null)
      {
        paramString = paramString.getModifiedDate();
        l1 = l2;
        if (paramString != null) {
          l1 = c.d.b.a.b.a(paramString.getTime()).longValue();
        }
      }
    }
    return c.d.b.a.b.a(l1);
  }
  
  final void c()
  {
    c localc = c;
    if (localc != null)
    {
      x localx = x.a;
      o.a locala = o.a;
      localc.b(o.d(localx));
    }
    c = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.bd
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */