package com.truecaller.backup;

import android.annotation.SuppressLint;
import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import c.a.m;
import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.aa;
import com.truecaller.utils.extensions.d;
import com.truecaller.utils.extensions.g;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public final class bl
  implements bk
{
  final ContentResolver a;
  private final com.truecaller.common.g.a b;
  private final com.truecaller.utils.a c;
  private final b d;
  
  @Inject
  public bl(Context paramContext, com.truecaller.common.g.a parama, com.truecaller.utils.a parama1, b paramb)
  {
    b = parama;
    c = parama1;
    d = paramb;
    a = paramContext.getContentResolver();
  }
  
  @SuppressLint({"Recycle"})
  private final bj a(int paramInt)
  {
    Cursor localCursor = a.query(TruecallerContract.aa.a(), bn.a(), "transport=?", new String[] { String.valueOf(paramInt) }, "date DESC, participant_id DESC");
    if (localCursor != null) {
      return new bj(localCursor);
    }
    return null;
  }
  
  public final void a()
  {
    ContentProviderOperation localContentProviderOperation = ContentProviderOperation.newUpdate(TruecallerContract.aa.a()).withSelection("transport=?", new String[] { "0" }).withValue("transport", Integer.valueOf(4)).build();
    a.applyBatch(TruecallerContract.a(), m.d(new ContentProviderOperation[] { localContentProviderOperation }));
    b.b("deleteBackupDuplicates", true);
  }
  
  public final void b()
  {
    for (;;)
    {
      try
      {
        boolean bool1 = b.a("deleteBackupDuplicates", false);
        if (!bool1) {
          return;
        }
        long l1 = c.a();
        Object localObject1 = a(4);
        if (localObject1 == null) {
          return;
        }
        Object localObject3 = a(0);
        if (localObject3 == null) {
          return;
        }
        List localList = (List)new ArrayList();
        c.g.a.a locala = (c.g.a.a)new bl.a(this, localList);
        bool1 = ((bj)localObject1).moveToNext();
        boolean bool2 = ((bj)localObject3).moveToNext();
        long l2;
        if ((bool1) && (bool2))
        {
          l2 = ((bj)localObject1).b();
          long l3 = ((bj)localObject3).b();
          long l4 = ((bj)localObject1).c();
          long l5 = ((bj)localObject3).c();
          if ((((Number)b.a((Cursor)localObject1, bj.a[3])).intValue() & 0x34) != 0)
          {
            i = 1;
            ContentProviderOperation localContentProviderOperation;
            if (i != 0)
            {
              localContentProviderOperation = ContentProviderOperation.newDelete(TruecallerContract.aa.a(((bj)localObject1).a())).build();
              k.a(localContentProviderOperation, "ContentProviderOperation…                 .build()");
              localList.add(localContentProviderOperation);
              if (localList.size() == 50) {
                locala.invoke();
              }
              bool1 = ((bj)localObject1).moveToNext();
              continue;
            }
            if (l2 <= l3)
            {
              if (l2 < l3)
              {
                bool2 = ((bj)localObject3).moveToNext();
                continue;
              }
              if (l4 <= l5)
              {
                if (l4 < l5)
                {
                  bool2 = ((bj)localObject3).moveToNext();
                  continue;
                }
                localContentProviderOperation = ContentProviderOperation.newDelete(TruecallerContract.aa.a(((bj)localObject1).a())).build();
                k.a(localContentProviderOperation, "ContentProviderOperation…                 .build()");
                localList.add(localContentProviderOperation);
                if (localList.size() == 50) {
                  locala.invoke();
                }
              }
            }
            bool1 = ((bj)localObject1).moveToNext();
          }
        }
        else
        {
          locala.invoke();
          d.a((Closeable)localObject1);
          d.a((Closeable)localObject3);
          l2 = c.a();
          localObject1 = d;
          localObject3 = new e.a("BackupDuration").a("Segment", "Messages");
          double d1 = l2 - l1;
          Double.isNaN(d1);
          d1 /= 1000.0D;
          localObject3 = ((e.a)localObject3).a(Double.valueOf(d1)).a();
          k.a(localObject3, "AnalyticsEvent.Builder(B…\n                .build()");
          ((b)localObject1).b((e)localObject3);
          b.b("deleteBackupDuplicates", false);
          return;
        }
      }
      finally {}
      int i = 0;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.bl
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */