package com.truecaller.backup;

import android.content.Context;
import com.truecaller.analytics.b;
import dagger.a.d;
import javax.inject.Provider;

public final class bm
  implements d<bl>
{
  private final Provider<Context> a;
  private final Provider<com.truecaller.common.g.a> b;
  private final Provider<com.truecaller.utils.a> c;
  private final Provider<b> d;
  
  private bm(Provider<Context> paramProvider, Provider<com.truecaller.common.g.a> paramProvider1, Provider<com.truecaller.utils.a> paramProvider2, Provider<b> paramProvider3)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static bm a(Provider<Context> paramProvider, Provider<com.truecaller.common.g.a> paramProvider1, Provider<com.truecaller.utils.a> paramProvider2, Provider<b> paramProvider3)
  {
    return new bm(paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.backup.bm
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */