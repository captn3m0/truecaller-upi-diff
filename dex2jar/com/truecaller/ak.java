package com.truecaller;

import android.content.Context;
import com.truecaller.util.bt;
import dagger.a.d;
import javax.inject.Provider;

public final class ak
  implements d<bt>
{
  private final c a;
  private final Provider<Context> b;
  
  private ak(c paramc, Provider<Context> paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static ak a(c paramc, Provider<Context> paramProvider)
  {
    return new ak(paramc, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ak
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */