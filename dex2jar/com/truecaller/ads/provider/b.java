package com.truecaller.ads.provider;

import c.a.f;
import com.google.android.gms.ads.AdSize;
import com.truecaller.ads.j;
import com.truecaller.analytics.e.a;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.inject.Inject;

public final class b
  implements a
{
  private final Map<String, Long> a;
  private final Map<String, Long> b;
  private final com.truecaller.analytics.b c;
  private final com.truecaller.utils.a d;
  
  @Inject
  public b(com.truecaller.analytics.b paramb, com.truecaller.utils.a parama)
  {
    c = paramb;
    d = parama;
    a = ((Map)new LinkedHashMap());
    b = ((Map)new LinkedHashMap());
  }
  
  private static String a(long paramLong)
  {
    Object localObject = c.a();
    int k = localObject.length;
    int i = 0;
    while (i < k)
    {
      long l = localObject[i];
      int j;
      if (paramLong < l) {
        j = 1;
      } else {
        j = 0;
      }
      if (j != 0)
      {
        localObject = Long.valueOf(l);
        break label62;
      }
      i += 1;
    }
    localObject = null;
    label62:
    if (localObject != null) {
      paramLong = ((Long)localObject).longValue();
    } else {
      paramLong = f.a(c.a());
    }
    return String.valueOf(paramLong);
  }
  
  private final void a(String paramString, com.truecaller.ads.provider.fetch.c paramc, Integer paramInteger)
  {
    String str1 = g;
    String str2 = b;
    String str3 = a.f.c;
    c.g.b.k.a(str3, "config.campaignConfig.placement");
    a(this, paramString, str1, str2, str3, a.h, null, null, paramInteger, 96);
  }
  
  private final void a(String paramString, com.truecaller.ads.provider.holders.e parame)
  {
    if (!parame.f()) {
      return;
    }
    com.truecaller.ads.provider.fetch.c localc = parame.h();
    String str1 = g;
    String str2 = b;
    String str3 = a.f.c;
    c.g.b.k.a(str3, "config.campaignConfig.placement");
    a(this, paramString, str1, str2, str3, a.h, parame.b(), parame.c(), null, 128);
  }
  
  private static void a(String paramString1, Long paramLong, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, Integer paramInteger)
  {
    String str1 = (String)c.a.m.f(c.n.m.c((CharSequence)paramString1, new String[] { "-" }, false, 6));
    long l;
    if (paramLong != null) {
      l = paramLong.longValue();
    } else {
      l = 0L;
    }
    String str2 = c.n.m.a(String.valueOf(l), 7, ' ');
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(c.n.m.g(paramString2));
    if (paramInteger != null)
    {
      paramLong = ":".concat(String.valueOf(((Number)paramInteger).intValue()));
      paramString1 = paramLong;
      if (paramLong != null) {}
    }
    else
    {
      paramString1 = "  ";
    }
    localStringBuilder.append(paramString1);
    paramString1 = localStringBuilder.toString();
    paramLong = new StringBuilder("[");
    paramLong.append(str1);
    paramLong.append("] T:");
    paramLong.append(str2);
    paramLong.append(' ');
    paramLong.append(paramString1);
    paramLong.append(' ');
    paramLong.append(paramString3);
    paramLong.append(' ');
    paramLong.append(paramString4);
    paramLong.append(' ');
    paramLong.append(paramString5);
    paramLong.append(' ');
    paramLong.append(paramString6);
    paramLong.append(' ');
    paramLong.append(paramString7);
    paramLong.append(' ');
    paramLong.toString();
  }
  
  private final void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, Integer paramInteger)
  {
    Long localLong = c(paramString2);
    e.a locala = new e.a("Ads");
    locala.a("Event", paramString1);
    locala.a("UnitId", paramString3);
    locala.a("Placement", paramString4);
    if (paramString5 != null) {
      locala.a("Context", paramString5);
    }
    if (paramString6 != null) {
      locala.a("AdType", paramString6);
    }
    if (paramString7 != null) {
      locala.a("AdSubtype", paramString7);
    }
    if (paramInteger != null) {
      locala.a("Fail", ((Number)paramInteger).intValue());
    }
    if (localLong != null)
    {
      long l = ((Number)localLong).longValue();
      locala.a("TimeBucket", a(l));
      double d1 = l;
      Double.isNaN(d1);
      locala.a(Double.valueOf(d1 / 1000.0D));
    }
    a(paramString2, localLong, paramString1, paramString3, paramString4, paramString5, paramString6, paramString7, paramInteger);
    paramString1 = c;
    paramString2 = locala.a();
    c.g.b.k.a(paramString2, "builder.build()");
    paramString1.a(paramString2);
  }
  
  private final Long c(String paramString)
  {
    long l = d.c();
    Long localLong = (Long)b.get(paramString);
    b.put(paramString, Long.valueOf(l));
    if (localLong != null) {
      return Long.valueOf((l - ((Number)localLong).longValue()) / 1000000L);
    }
    return null;
  }
  
  public final void a(com.truecaller.ads.provider.fetch.c paramc)
  {
    c.g.b.k.b(paramc, "adRequest");
    a("requested", paramc, null);
  }
  
  public final void a(com.truecaller.ads.provider.fetch.c paramc, int paramInt)
  {
    c.g.b.k.b(paramc, "adRequest");
    a("failed", paramc, Integer.valueOf(paramInt));
    b.remove(g);
  }
  
  public final void a(com.truecaller.ads.provider.holders.e parame)
  {
    c.g.b.k.b(parame, "adHolder");
    a("loaded", parame);
  }
  
  public final void a(com.truecaller.ads.provider.holders.e parame, String paramString)
  {
    c.g.b.k.b(parame, "adHolder");
    c.g.b.k.b(paramString, "event");
    a(paramString, parame);
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "placement");
    a.put(paramString, Long.valueOf(d.c()));
    com.truecaller.analytics.b localb = c;
    paramString = new e.a("AdsKeywords").a("Event", "requested").a("Placement", paramString).a();
    c.g.b.k.a(paramString, "AnalyticsEvent.Builder(A…\n                .build()");
    localb.a(paramString);
  }
  
  public final void a(String paramString, AdSize paramAdSize)
  {
    c.g.b.k.b(paramString, "adUnit");
    c.g.b.k.b(paramAdSize, "size");
    b.remove("legacyBanner");
    a(this, "requested", "legacyBanner", paramString, "AFTERCALL", "afterCall", "aftercallBanner", paramAdSize.toString(), null, 128);
  }
  
  public final void a(String paramString, AdSize paramAdSize, int paramInt)
  {
    c.g.b.k.b(paramString, "adUnit");
    c.g.b.k.b(paramAdSize, "size");
    a("failed", "legacyBanner", paramString, "AFTERCALL", "afterCall", "aftercallBanner", paramAdSize.toString(), Integer.valueOf(paramInt));
  }
  
  public final void a(String... paramVarArgs)
  {
    c.g.b.k.b(paramVarArgs, "placements");
    int j = paramVarArgs.length;
    int i = 0;
    while (i < j)
    {
      Object localObject = paramVarArgs[i];
      com.truecaller.analytics.b localb = c;
      localObject = new e.a("AdsKeywords").a("Event", "preloaded").a("Placement", (String)localObject).a();
      c.g.b.k.a(localObject, "AnalyticsEvent.Builder(A…                 .build()");
      localb.a((com.truecaller.analytics.e)localObject);
      i += 1;
    }
  }
  
  public final void b(com.truecaller.ads.provider.fetch.c paramc)
  {
    c.g.b.k.b(paramc, "adRequest");
    a("canceled", paramc, null);
    b.remove(g);
  }
  
  public final void b(com.truecaller.ads.provider.holders.e parame)
  {
    c.g.b.k.b(parame, "adHolder");
    a("expired", parame);
    b.remove(hg);
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "placement");
    long l2 = d.c();
    Object localObject = (Long)a.remove(paramString);
    if (localObject != null) {
      l1 = ((Long)localObject).longValue();
    } else {
      l1 = l2;
    }
    long l1 = (l2 - l1) / 1000000L;
    localObject = c;
    paramString = new e.a("AdsKeywords").a("Event", "received").a("Placement", paramString).a("TimeBucket", a(l1));
    double d1 = l1;
    Double.isNaN(d1);
    paramString = paramString.a(Double.valueOf(d1 / 1000.0D)).a();
    c.g.b.k.a(paramString, "AnalyticsEvent.Builder(A…\n                .build()");
    ((com.truecaller.analytics.b)localObject).a(paramString);
  }
  
  public final void b(String paramString, AdSize paramAdSize)
  {
    c.g.b.k.b(paramString, "adUnit");
    c.g.b.k.b(paramAdSize, "size");
    a(this, "loaded", "legacyBanner", paramString, "AFTERCALL", "afterCall", "aftercallBanner", paramAdSize.toString(), null, 128);
    a(this, "displayed", "legacyBanner", paramString, "AFTERCALL", "afterCall", "aftercallBanner", paramAdSize.toString(), null, 128);
  }
  
  public final void c(com.truecaller.ads.provider.holders.e parame)
  {
    c.g.b.k.b(parame, "adHolder");
    a("dropped", parame);
    b.remove(hg);
  }
  
  public final void c(String paramString, AdSize paramAdSize)
  {
    c.g.b.k.b(paramString, "adUnit");
    c.g.b.k.b(paramAdSize, "size");
    a(this, "opened", "legacyBanner", paramString, "AFTERCALL", "afterCall", "aftercallBanner", paramAdSize.toString(), null, 128);
  }
  
  public final void d(com.truecaller.ads.provider.holders.e parame)
  {
    c.g.b.k.b(parame, "adHolder");
    a("displayed", parame);
  }
  
  public final void e(com.truecaller.ads.provider.holders.e parame)
  {
    c.g.b.k.b(parame, "adHolder");
    a("opened", parame);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */