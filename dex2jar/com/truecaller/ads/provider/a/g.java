package com.truecaller.ads.provider.a;

import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d<f>
{
  private final Provider<a> a;
  
  private g(Provider<a> paramProvider)
  {
    a = paramProvider;
  }
  
  public static g a(Provider<a> paramProvider)
  {
    return new g(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.a.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */