package com.truecaller.ads.provider.a;

import c.d.c;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import c.o.b;
import com.truecaller.ads.campaigns.AdCampaigns;
import com.truecaller.ads.campaigns.e;
import com.truecaller.ads.j;
import com.truecaller.ads.j.a;
import java.util.Arrays;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.g;

public final class d
  implements a
{
  final e a;
  final com.truecaller.ads.provider.a b;
  private final f c;
  private final f d;
  
  @Inject
  public d(@Named("UI") f paramf1, @Named("Async") f paramf2, e parame, com.truecaller.ads.provider.a parama)
  {
    c = paramf1;
    d = paramf2;
    a = parame;
    b = parama;
  }
  
  public final Object a(j paramj, c<? super AdCampaigns> paramc)
  {
    if ((paramc instanceof d.d))
    {
      locald = (d.d)paramc;
      if ((b & 0x80000000) != 0)
      {
        b += Integer.MIN_VALUE;
        break label51;
      }
    }
    d.d locald = new d.d(this, paramc);
    label51:
    Object localObject1 = a;
    c.d.a.a locala = c.d.a.a.a;
    switch (b)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 3: 
      paramj = (AdCampaigns)f;
      if (!(localObject1 instanceof o.b)) {
        return paramj;
      }
      throw a;
    case 2: 
      paramj = (j)e;
      paramc = (d)d;
      if (!(localObject1 instanceof o.b)) {
        break label319;
      }
      throw a;
    case 1: 
      paramj = (j)e;
      paramc = (d)d;
      if ((localObject1 instanceof o.b)) {
        throw a;
      }
      break;
    case 0: 
      if ((localObject1 instanceof o.b)) {
        break label446;
      }
      paramc = c;
      localObject1 = (m)new d.e(this, paramj, null);
      d = this;
      e = paramj;
      b = 1;
      if (g.a(paramc, (m)localObject1, locald) == locala) {
        return locala;
      }
      paramc = this;
    }
    d = paramc;
    e = paramj;
    b = 2;
    Object localObject2 = paramc.b(paramj, locald);
    localObject1 = localObject2;
    if (localObject2 == locala) {
      return locala;
    }
    label319:
    localObject2 = (AdCampaigns[])localObject1;
    if (localObject2 != null)
    {
      int j = localObject2.length;
      int i = 0;
      while (i < j)
      {
        localObject1 = localObject2[i];
        if (k.a(c, a)) {
          break label378;
        }
        i += 1;
      }
    }
    localObject1 = null;
    label378:
    localObject2 = c;
    m localm = (m)new d.f(paramc, (AdCampaigns)localObject1, paramj, null);
    d = paramc;
    e = paramj;
    f = localObject1;
    b = 3;
    if (g.a((f)localObject2, localm, locald) == locala) {
      return locala;
    }
    return localObject1;
    label446:
    throw a;
  }
  
  public final void a(String... paramVarArgs)
  {
    k.b(paramVarArgs, "placements");
    b.a((String[])Arrays.copyOf(paramVarArgs, 8));
    j localj = new j.a(paramVarArgs[0]).a();
    k.a(localj, "CampaignConfig.Builder(placements[0]).build()");
    a.a(a, b, paramVarArgs, d, e, f, g);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.a.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */