package com.truecaller.ads.provider.a;

import c.d.f;
import com.truecaller.ads.provider.a;
import javax.inject.Provider;

public final class e
  implements dagger.a.d<d>
{
  private final Provider<f> a;
  private final Provider<f> b;
  private final Provider<com.truecaller.ads.campaigns.e> c;
  private final Provider<a> d;
  
  private e(Provider<f> paramProvider1, Provider<f> paramProvider2, Provider<com.truecaller.ads.campaigns.e> paramProvider, Provider<a> paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider;
    d = paramProvider3;
  }
  
  public static e a(Provider<f> paramProvider1, Provider<f> paramProvider2, Provider<com.truecaller.ads.campaigns.e> paramProvider, Provider<a> paramProvider3)
  {
    return new e(paramProvider1, paramProvider2, paramProvider, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.a.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */