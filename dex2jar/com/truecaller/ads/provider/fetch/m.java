package com.truecaller.ads.provider.fetch;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import c.a.ag;
import c.t;
import c.u;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdLoader.Builder;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.VideoOptions.Builder;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest.Builder;
import com.google.android.gms.ads.formats.NativeAdOptions.Builder;
import com.google.android.gms.ads.formats.NativeAppInstallAd.OnAppInstallAdLoadedListener;
import com.google.android.gms.ads.formats.NativeContentAd.OnContentAdLoadedListener;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd.OnCustomClickListener;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd.OnCustomTemplateAdLoadedListener;
import com.google.android.gms.ads.formats.OnPublisherAdViewLoadedListener;
import com.mopub.mobileads.dfp.adapters.MoPubAdapter;
import com.mopub.mobileads.dfp.adapters.MoPubAdapter.BundleBuilder;
import com.truecaller.ads.CustomTemplate;
import com.truecaller.util.co;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.inject.Inject;

public final class m
  implements l, p
{
  private final Map<String, j> a;
  private final Context b;
  private final com.truecaller.utils.d c;
  private final com.truecaller.utils.a d;
  private final com.truecaller.common.h.c e;
  
  @Inject
  public m(Context paramContext, com.truecaller.utils.d paramd, com.truecaller.utils.a parama, com.truecaller.common.h.c paramc)
  {
    b = paramContext;
    c = paramd;
    d = parama;
    e = paramc;
    a = ((Map)new LinkedHashMap());
  }
  
  private final Map<String, String> a(Context paramContext, String[] paramArrayOfString)
  {
    c.n localn1 = t.a("buildname", e.f());
    c.n localn2 = t.a("appversion", "10.41.6");
    if (c.a()) {
      localObject = "t";
    } else {
      localObject = "f";
    }
    Object localObject = ag.b(new c.n[] { localn1, localn2, t.a("sms", localObject) });
    if (e.a()) {
      ((Map)localObject).put("OEM_build", null);
    }
    try
    {
      paramContext = com.truecaller.common.h.k.d(paramContext);
      if (!TextUtils.isEmpty((CharSequence)paramContext)) {
        ((Map)localObject).put("carrier", paramContext);
      }
    }
    catch (SecurityException paramContext)
    {
      int k;
      int i;
      for (;;) {}
    }
    paramContext = com.truecaller.common.h.k.a();
    if (!TextUtils.isEmpty((CharSequence)paramContext)) {
      ((Map)localObject).put("device", paramContext);
    }
    paramContext = paramArrayOfString;
    if (paramArrayOfString == null) {
      paramContext = new String[0];
    }
    paramArrayOfString = (Collection)new ArrayList();
    k = paramContext.length;
    i = 0;
    while (i < k)
    {
      localn1 = paramContext[i];
      int j;
      if (((CharSequence)localn1).length() == 0) {
        j = 1;
      } else {
        j = 0;
      }
      if (j == 0) {
        paramArrayOfString.add(localn1);
      }
      i += 1;
    }
    paramArrayOfString = ((List)paramArrayOfString).iterator();
    while (paramArrayOfString.hasNext())
    {
      paramContext = (CharSequence)paramArrayOfString.next();
      paramContext = (Collection)new c.n.k(":").a(paramContext, 2);
      if (paramContext != null)
      {
        paramContext = paramContext.toArray(new String[0]);
        if (paramContext != null)
        {
          paramContext = (String[])paramContext;
          localn1 = paramContext[0];
          if (paramContext.length > 1) {
            paramContext = paramContext[1];
          } else {
            paramContext = null;
          }
          ((Map)localObject).put(localn1, paramContext);
        }
        else
        {
          throw new u("null cannot be cast to non-null type kotlin.Array<T>");
        }
      }
      else
      {
        throw new u("null cannot be cast to non-null type java.util.Collection<T>");
      }
    }
    return (Map<String, String>)localObject;
  }
  
  public final PublisherAdRequest a(Context paramContext, String[] paramArrayOfString, boolean paramBoolean)
  {
    c.g.b.k.b(paramContext, "context");
    PublisherAdRequest.Builder localBuilder = new PublisherAdRequest.Builder();
    Object localObject1 = co.b(paramContext);
    if (localObject1 != null) {
      localBuilder.a((Location)localObject1);
    }
    localBuilder.a(MoPubAdapter.class, new MoPubAdapter.BundleBuilder().setPrivacyIconSize(14).build());
    Object localObject2 = new Bundle();
    if (paramBoolean) {
      localObject1 = "0";
    } else {
      localObject1 = "1";
    }
    ((Bundle)localObject2).putString("npa", (String)localObject1);
    localBuilder.a(AdMobAdapter.class, (Bundle)localObject2);
    localObject2 = a(paramContext, paramArrayOfString).entrySet().iterator();
    while (((Iterator)localObject2).hasNext())
    {
      paramContext = (Map.Entry)((Iterator)localObject2).next();
      paramArrayOfString = (String)paramContext.getKey();
      localObject1 = (String)paramContext.getValue();
      paramContext = (Context)localObject1;
      if (TextUtils.isEmpty((CharSequence)localObject1)) {
        paramContext = paramArrayOfString;
      }
      localBuilder.a(paramArrayOfString, paramContext);
    }
    paramContext = localBuilder.a();
    c.g.b.k.a(paramContext, "builder.build()");
    return paramContext;
  }
  
  public final Object a(c paramc, c.d.c<? super e> paramc1)
    throws d
  {
    kotlinx.coroutines.k localk = new kotlinx.coroutines.k(c.d.a.b.a(paramc1), 1);
    Object localObject2 = (kotlinx.coroutines.j)localk;
    com.truecaller.ads.k localk1 = a;
    try
    {
      AdLoader.Builder localBuilder = new AdLoader.Builder(b, b);
      b localb = new b();
      a = ((c.g.a.b)new m.a((kotlinx.coroutines.j)localObject2));
      localBuilder.a((AdListener)localb);
      Object localObject1 = localBuilder.a((NativeAppInstallAd.OnAppInstallAdLoadedListener)new m.b((kotlinx.coroutines.j)localObject2, localb, this, paramc)).a((NativeContentAd.OnContentAdLoadedListener)new m.c((kotlinx.coroutines.j)localObject2, localb, this, paramc));
      Object localObject3 = new NativeAdOptions.Builder();
      ((NativeAdOptions.Builder)localObject3).b();
      ((NativeAdOptions.Builder)localObject3).a(m ^ true);
      int j = g;
      int i = j;
      if (com.truecaller.common.e.f.b()) {
        switch (j)
        {
        default: 
          i = j;
          break;
        case 3: 
          i = 2;
          break;
        case 2: 
          i = 3;
          break;
        case 1: 
          i = 0;
          break;
        case 0: 
          i = 1;
        }
      }
      ((NativeAdOptions.Builder)localObject3).a(i);
      ((NativeAdOptions.Builder)localObject3).a();
      ((NativeAdOptions.Builder)localObject3).a(new VideoOptions.Builder().a(i ^ true).b(j).a());
      ((AdLoader.Builder)localObject1).a(((NativeAdOptions.Builder)localObject3).c());
      if (!d.isEmpty())
      {
        localObject1 = (OnPublisherAdViewLoadedListener)new m.d((kotlinx.coroutines.j)localObject2, localb, this, paramc);
        localObject3 = (Collection)d;
        if (localObject3 != null)
        {
          localObject3 = ((Collection)localObject3).toArray(new AdSize[0]);
          if (localObject3 != null)
          {
            localObject3 = (AdSize[])localObject3;
            localBuilder.a((OnPublisherAdViewLoadedListener)localObject1, (AdSize[])Arrays.copyOf((Object[])localObject3, localObject3.length));
          }
          else
          {
            throw new u("null cannot be cast to non-null type kotlin.Array<T>");
          }
        }
        else
        {
          throw new u("null cannot be cast to non-null type java.util.Collection<T>");
        }
      }
      localObject3 = ((Iterable)e).iterator();
      while (((Iterator)localObject3).hasNext())
      {
        CustomTemplate localCustomTemplate = (CustomTemplate)((Iterator)localObject3).next();
        boolean bool = openUrl;
        if (bool == true)
        {
          localObject1 = null;
        }
        else
        {
          if (bool) {
            break label509;
          }
          localObject1 = (NativeCustomTemplateAd.OnCustomClickListener)new m.e(localb, localBuilder, (kotlinx.coroutines.j)localObject2, this, paramc);
        }
        localBuilder.a(templateId, (NativeCustomTemplateAd.OnCustomTemplateAdLoadedListener)new m.f(localb, localBuilder, (kotlinx.coroutines.j)localObject2, this, paramc), (NativeCustomTemplateAd.OnCustomClickListener)localObject1);
        continue;
        label509:
        throw new c.l();
      }
      localObject1 = b;
      localObject2 = c;
      a.put(a, new j(d.a(), a, a((Context)localObject1, (String[])localObject2)));
      localBuilder.a().a(a(b, c, e));
    }
    catch (Exception paramc)
    {
      for (;;) {}
    }
    n.a((kotlinx.coroutines.j)localObject2, new d(com.truecaller.ads.f.b()));
    paramc = localk.h();
    if (paramc == c.d.a.a.a) {
      c.g.b.k.b(paramc1, "frame");
    }
    return paramc;
  }
  
  public final Set<j> a()
  {
    return c.a.m.i((Iterable)a.values());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */