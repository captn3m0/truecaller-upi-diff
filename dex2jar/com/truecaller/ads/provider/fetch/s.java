package com.truecaller.ads.provider.fetch;

import com.truecaller.common.background.b;
import com.truecaller.common.h.ac;
import dagger.a.d;
import javax.inject.Provider;

public final class s
  implements d<q>
{
  private final Provider<com.truecaller.common.network.optout.a> a;
  private final Provider<com.truecaller.utils.a> b;
  private final Provider<com.truecaller.i.a> c;
  private final Provider<b> d;
  private final Provider<ac> e;
  private final Provider<com.truecaller.ads.provider.fetch.a.a> f;
  
  private s(Provider<com.truecaller.common.network.optout.a> paramProvider, Provider<com.truecaller.utils.a> paramProvider1, Provider<com.truecaller.i.a> paramProvider2, Provider<b> paramProvider3, Provider<ac> paramProvider4, Provider<com.truecaller.ads.provider.fetch.a.a> paramProvider5)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
  }
  
  public static s a(Provider<com.truecaller.common.network.optout.a> paramProvider, Provider<com.truecaller.utils.a> paramProvider1, Provider<com.truecaller.i.a> paramProvider2, Provider<b> paramProvider3, Provider<ac> paramProvider4, Provider<com.truecaller.ads.provider.fetch.a.a> paramProvider5)
  {
    return new s(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.s
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */