package com.truecaller.ads.provider.fetch;

import c.g.a.m;
import c.g.b.k;
import com.truecaller.common.background.b;
import com.truecaller.common.background.b.a;
import com.truecaller.common.h.ac;
import com.truecaller.common.network.optout.OptOutRestAdapter.OptOutsDto;
import com.truecaller.old.data.access.Settings;
import java.util.List;
import javax.inject.Inject;

public final class q
  implements AdsConfigurationManager
{
  private AdsConfigurationManager.a a;
  private long b;
  private boolean c;
  private final com.truecaller.common.network.optout.a d;
  private final com.truecaller.utils.a e;
  private final com.truecaller.i.a f;
  private final b g;
  private final ac h;
  private final com.truecaller.ads.provider.fetch.a.a i;
  
  @Inject
  public q(com.truecaller.common.network.optout.a parama, com.truecaller.utils.a parama1, com.truecaller.i.a parama2, b paramb, ac paramac, com.truecaller.ads.provider.fetch.a.a parama3)
  {
    d = parama;
    e = parama1;
    f = parama2;
    g = paramb;
    h = paramac;
    i = parama3;
    b = f.a("adsTargetingRefreshTimestamp", 0L);
    parama = f.b("adsTargetingLastValue", AdsConfigurationManager.TargetingState.UNKNOWN.getKey());
    if (k.a(parama, AdsConfigurationManager.TargetingState.TARGETING.getKey())) {
      parama = AdsConfigurationManager.TargetingState.TARGETING;
    } else if (k.a(parama, AdsConfigurationManager.TargetingState.NON_TARGETING.getKey())) {
      parama = AdsConfigurationManager.TargetingState.NON_TARGETING;
    } else {
      parama = AdsConfigurationManager.TargetingState.UNKNOWN;
    }
    parama1 = f.b("promotionConsentLastValue", AdsConfigurationManager.PromotionState.UNKNOWN.getKey());
    if (k.a(parama1, AdsConfigurationManager.PromotionState.OPT_IN.getKey())) {
      parama1 = AdsConfigurationManager.PromotionState.OPT_IN;
    } else if (k.a(parama1, AdsConfigurationManager.PromotionState.OPT_OUT.getKey())) {
      parama1 = AdsConfigurationManager.PromotionState.OPT_OUT;
    } else {
      parama1 = AdsConfigurationManager.PromotionState.UNKNOWN;
    }
    a = new AdsConfigurationManager.a(parama, parama1);
    a(a);
    g();
  }
  
  private final void a(AdsConfigurationManager.a parama)
  {
    i.a(parama);
  }
  
  private final void g()
  {
    if (b == 0L) {
      f();
    }
  }
  
  private final void h()
  {
    b = e.a();
    f.b("adsTargetingRefreshTimestamp", b);
    a(a);
  }
  
  public final void a(AdsConfigurationManager.PromotionState paramPromotionState)
  {
    k.b(paramPromotionState, "state");
    a = AdsConfigurationManager.a.a(a, null, paramPromotionState, 1);
    f.a("promotionConsentLastValue", a.b.getKey());
    h();
  }
  
  public final void a(AdsConfigurationManager.TargetingState paramTargetingState)
  {
    k.b(paramTargetingState, "state");
    a = AdsConfigurationManager.a.a(a, paramTargetingState, null, 2);
    f.a("adsTargetingLastValue", a.a.getKey());
    h();
  }
  
  public final void a(AdsConfigurationManager.TargetingState paramTargetingState, AdsConfigurationManager.PromotionState paramPromotionState)
  {
    k.b(paramTargetingState, "targetingState");
    k.b(paramPromotionState, "promotionState");
    a = AdsConfigurationManager.a.a(paramTargetingState, paramPromotionState);
    f.a("adsTargetingLastValue", a.a.getKey());
    f.a("promotionConsentLastValue", a.b.getKey());
    h();
  }
  
  public final boolean a()
  {
    if (!h.a()) {
      return true;
    }
    g();
    return a.a == AdsConfigurationManager.TargetingState.TARGETING;
  }
  
  public final AdsConfigurationManager.PromotionState b()
  {
    g();
    return a.b;
  }
  
  public final AdsConfigurationManager.a c()
  {
    Object localObject = d.e();
    if (localObject == null) {
      return null;
    }
    boolean bool1 = ((OptOutRestAdapter.OptOutsDto)localObject).getOptIns().contains("ads");
    boolean bool2 = ((OptOutRestAdapter.OptOutsDto)localObject).getOptOuts().contains("ads");
    AdsConfigurationManager.TargetingState localTargetingState;
    if (bool1) {
      localTargetingState = AdsConfigurationManager.TargetingState.TARGETING;
    } else if (bool2) {
      localTargetingState = AdsConfigurationManager.TargetingState.NON_TARGETING;
    } else if (((OptOutRestAdapter.OptOutsDto)localObject).getConsentRefresh()) {
      localTargetingState = AdsConfigurationManager.TargetingState.TARGETING;
    } else {
      localTargetingState = AdsConfigurationManager.TargetingState.UNKNOWN;
    }
    bool1 = ((OptOutRestAdapter.OptOutsDto)localObject).getOptIns().contains("dm");
    bool2 = ((OptOutRestAdapter.OptOutsDto)localObject).getOptOuts().contains("dm");
    if (bool1)
    {
      localObject = AdsConfigurationManager.PromotionState.OPT_IN;
    }
    else if (bool2)
    {
      localObject = AdsConfigurationManager.PromotionState.OPT_OUT;
    }
    else
    {
      ((OptOutRestAdapter.OptOutsDto)localObject).getConsentRefresh();
      localObject = AdsConfigurationManager.PromotionState.UNKNOWN;
    }
    return new AdsConfigurationManager.a(localTargetingState, (AdsConfigurationManager.PromotionState)localObject);
  }
  
  public final boolean d()
  {
    return Settings.h();
  }
  
  public final void e()
  {
    f.d("adsTargetingRefreshTimestamp");
    f.d("adsTargetingLastValue");
    f.d("promotionConsentLastValue");
    b = 0L;
    a = new AdsConfigurationManager.a(AdsConfigurationManager.TargetingState.UNKNOWN, AdsConfigurationManager.PromotionState.UNKNOWN);
  }
  
  public final void f()
  {
    if (!c)
    {
      c = true;
      g.a(10025, null, (b.a)new r((m)new q.a((q)this)), null);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */