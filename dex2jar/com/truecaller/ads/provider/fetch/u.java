package com.truecaller.ads.provider.fetch;

import c.d.f;
import javax.inject.Provider;

public final class u
  implements dagger.a.d<t>
{
  private final Provider<f> a;
  private final Provider<com.truecaller.utils.a> b;
  private final Provider<com.truecaller.utils.d> c;
  private final Provider<com.truecaller.ads.provider.a> d;
  private final Provider<com.truecaller.i.a> e;
  private final Provider<com.truecaller.ads.provider.a.a> f;
  private final Provider<l> g;
  private final Provider<AdsConfigurationManager> h;
  
  private u(Provider<f> paramProvider, Provider<com.truecaller.utils.a> paramProvider1, Provider<com.truecaller.utils.d> paramProvider2, Provider<com.truecaller.ads.provider.a> paramProvider3, Provider<com.truecaller.i.a> paramProvider4, Provider<com.truecaller.ads.provider.a.a> paramProvider5, Provider<l> paramProvider6, Provider<AdsConfigurationManager> paramProvider7)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
  }
  
  public static u a(Provider<f> paramProvider, Provider<com.truecaller.utils.a> paramProvider1, Provider<com.truecaller.utils.d> paramProvider2, Provider<com.truecaller.ads.provider.a> paramProvider3, Provider<com.truecaller.i.a> paramProvider4, Provider<com.truecaller.ads.provider.a.a> paramProvider5, Provider<l> paramProvider6, Provider<AdsConfigurationManager> paramProvider7)
  {
    return new u(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.u
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */