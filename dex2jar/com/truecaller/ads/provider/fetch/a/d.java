package com.truecaller.ads.provider.fetch.a;

import android.content.Context;
import c.d.f;
import com.truecaller.common.h.ac;
import javax.inject.Provider;

public final class d
  implements dagger.a.d<b>
{
  private final Provider<ac> a;
  private final Provider<Context> b;
  private final Provider<f> c;
  
  private d(Provider<ac> paramProvider, Provider<Context> paramProvider1, Provider<f> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static d a(Provider<ac> paramProvider, Provider<Context> paramProvider1, Provider<f> paramProvider2)
  {
    return new d(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.a.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */