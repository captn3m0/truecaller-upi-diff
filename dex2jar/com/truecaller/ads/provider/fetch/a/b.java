package com.truecaller.ads.provider.fetch.a;

import android.content.Context;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.mopub.common.MoPub;
import com.truecaller.ads.provider.fetch.AdsConfigurationManager.a;
import com.truecaller.common.h.ac;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.e;

public final class b
  implements a, ag
{
  public AdsConfigurationManager.a a;
  final ac b;
  final Context c;
  private boolean d;
  private final f e;
  
  @Inject
  public b(ac paramac, Context paramContext, @Named("UI") f paramf)
  {
    b = paramac;
    c = paramContext;
    e = paramf;
  }
  
  public final f V_()
  {
    return e;
  }
  
  public final void a(AdsConfigurationManager.a parama)
  {
    k.b(parama, "targetingState");
    if (a != null)
    {
      AdsConfigurationManager.a locala = a;
      if (locala == null) {
        k.a("currentState");
      }
      if (!(k.a(locala, parama) ^ true)) {}
    }
    else
    {
      a = parama;
      e.b(this, e, (m)new b.a(this, null), 2);
      if (!MoPub.isSdkInitialized())
      {
        if (!d)
        {
          d = true;
          e.b(this, e, (m)new b.b(this, null), 2);
        }
      }
      else {
        e.b(this, e, (m)new b.c(this, null), 2);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */