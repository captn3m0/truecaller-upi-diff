package com.truecaller.ads.provider.fetch;

import com.truecaller.utils.d;
import javax.inject.Inject;
import javax.inject.Named;

public final class t
  implements g
{
  private final c.d.f a;
  private final com.truecaller.utils.a b;
  private final d c;
  private final com.truecaller.ads.provider.a d;
  private final com.truecaller.i.a e;
  private final com.truecaller.ads.provider.a.a f;
  private final l g;
  private final AdsConfigurationManager h;
  
  @Inject
  public t(@Named("UI") c.d.f paramf, com.truecaller.utils.a parama, d paramd, com.truecaller.ads.provider.a parama1, com.truecaller.i.a parama2, com.truecaller.ads.provider.a.a parama3, l paraml, AdsConfigurationManager paramAdsConfigurationManager)
  {
    a = paramf;
    b = parama;
    c = paramd;
    d = parama1;
    e = parama2;
    f = parama3;
    g = paraml;
    h = paramAdsConfigurationManager;
  }
  
  public final f a(k paramk, com.truecaller.ads.k paramk1)
  {
    c.g.b.k.b(paramk, "callback");
    c.g.b.k.b(paramk1, "config");
    return (f)new h(paramk1, a, paramk, b, c, d, e, f, g, h);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.t
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */