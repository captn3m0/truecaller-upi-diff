package com.truecaller.ads.provider.fetch;

import android.content.Context;
import com.truecaller.common.h.c;
import com.truecaller.utils.a;
import javax.inject.Provider;

public final class o
  implements dagger.a.d<m>
{
  private final Provider<Context> a;
  private final Provider<com.truecaller.utils.d> b;
  private final Provider<a> c;
  private final Provider<c> d;
  
  private o(Provider<Context> paramProvider, Provider<com.truecaller.utils.d> paramProvider1, Provider<a> paramProvider2, Provider<c> paramProvider3)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static o a(Provider<Context> paramProvider, Provider<com.truecaller.utils.d> paramProvider1, Provider<a> paramProvider2, Provider<c> paramProvider3)
  {
    return new o(paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */