package com.truecaller.ads.provider;

import com.truecaller.ads.provider.a.b;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import dagger.a.d;
import javax.inject.Provider;

public final class k
  implements d<f<b>>
{
  private final Provider<b> a;
  private final Provider<i> b;
  
  private k(Provider<b> paramProvider, Provider<i> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static k a(Provider<b> paramProvider, Provider<i> paramProvider1)
  {
    return new k(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */