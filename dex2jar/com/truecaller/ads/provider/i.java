package com.truecaller.ads.provider;

import com.truecaller.androidactors.k;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class i
  implements d<com.truecaller.androidactors.i>
{
  private final Provider<k> a;
  
  private i(Provider<k> paramProvider)
  {
    a = paramProvider;
  }
  
  public static i a(Provider<k> paramProvider)
  {
    return new i(paramProvider);
  }
  
  public static com.truecaller.androidactors.i a(k paramk)
  {
    return (com.truecaller.androidactors.i)g.a(paramk.a("adsProvider"), "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */