package com.truecaller.ads.provider;

import com.truecaller.ads.provider.b.i;
import com.truecaller.ads.provider.fetch.AdsConfigurationManager;
import com.truecaller.analytics.ae;
import com.truecaller.featuretoggles.e;
import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d<g>
{
  private final Provider<c.d.f> a;
  private final Provider<com.truecaller.androidactors.f<ae>> b;
  private final Provider<AdsConfigurationManager> c;
  private final Provider<e> d;
  private final Provider<com.truecaller.ads.provider.fetch.g> e;
  private final Provider<i> f;
  
  private h(Provider<c.d.f> paramProvider, Provider<com.truecaller.androidactors.f<ae>> paramProvider1, Provider<AdsConfigurationManager> paramProvider2, Provider<e> paramProvider3, Provider<com.truecaller.ads.provider.fetch.g> paramProvider4, Provider<i> paramProvider5)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
  }
  
  public static h a(Provider<c.d.f> paramProvider, Provider<com.truecaller.androidactors.f<ae>> paramProvider1, Provider<AdsConfigurationManager> paramProvider2, Provider<e> paramProvider3, Provider<com.truecaller.ads.provider.fetch.g> paramProvider4, Provider<i> paramProvider5)
  {
    return new h(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */