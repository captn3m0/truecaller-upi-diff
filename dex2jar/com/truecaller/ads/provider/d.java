package com.truecaller.ads.provider;

import com.truecaller.utils.a;
import javax.inject.Provider;

public final class d
  implements dagger.a.d<b>
{
  private final Provider<com.truecaller.analytics.b> a;
  private final Provider<a> b;
  
  private d(Provider<com.truecaller.analytics.b> paramProvider, Provider<a> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static d a(Provider<com.truecaller.analytics.b> paramProvider, Provider<a> paramProvider1)
  {
    return new d(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */