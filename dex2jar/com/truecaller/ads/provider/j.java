package com.truecaller.ads.provider;

import android.content.Context;
import com.truecaller.ads.campaigns.e;
import com.truecaller.ads.campaigns.f;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class j
  implements d<e>
{
  private final Provider<Context> a;
  
  private j(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static e a(Context paramContext)
  {
    return (e)g.a(f.a(paramContext), "Cannot return null from a non-@Nullable @Provides method");
  }
  
  public static j a(Provider<Context> paramProvider)
  {
    return new j(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */