package com.truecaller.ads.provider;

import dagger.a.d;
import javax.inject.Provider;

public final class n
  implements d<m>
{
  private final Provider<com.truecaller.utils.n> a;
  
  private n(Provider<com.truecaller.utils.n> paramProvider)
  {
    a = paramProvider;
  }
  
  public static n a(Provider<com.truecaller.utils.n> paramProvider)
  {
    return new n(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */