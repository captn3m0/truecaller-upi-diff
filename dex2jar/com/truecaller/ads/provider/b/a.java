package com.truecaller.ads.provider.b;

import com.truecaller.ads.provider.fetch.c;
import com.truecaller.ads.provider.holders.g;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e.a;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bn;

public final class a
  implements i, ag
{
  private final Map<com.truecaller.ads.k, f> a;
  private final AtomicLong b;
  private final c.d.f c;
  private final com.truecaller.i.a d;
  private final com.truecaller.featuretoggles.e e;
  private final j f;
  
  @Inject
  public a(@Named("UI") c.d.f paramf, com.truecaller.i.a parama, com.truecaller.featuretoggles.e parame, j paramj)
  {
    c = paramf;
    d = parama;
    e = parame;
    f = paramj;
    a = ((Map)new LinkedHashMap());
    b = new AtomicLong();
  }
  
  public final c.d.f V_()
  {
    return c;
  }
  
  final long a()
  {
    return TimeUnit.SECONDS.toMillis(d.a("adsFeatureHouseAdsTimeout", 0L));
  }
  
  public final void a(com.truecaller.ads.k paramk)
  {
    c.g.b.k.b(paramk, "config");
    paramk = (f)a.remove(paramk);
    if (paramk != null)
    {
      paramk = e;
      if (paramk != null)
      {
        paramk.n();
        return;
      }
    }
  }
  
  public final void a(com.truecaller.ads.k paramk, h paramh)
  {
    c.g.b.k.b(paramk, "config");
    c.g.b.k.b(paramh, "listener");
    a(paramk);
    if (a() > 0L)
    {
      com.truecaller.featuretoggles.e locale = e;
      if ((l.a(locale, com.truecaller.featuretoggles.e.a[33]).a()) && (l)) {
        a.put(paramk, new f(paramk, paramh));
      }
    }
  }
  
  public final com.truecaller.ads.provider.holders.e b(com.truecaller.ads.k paramk)
  {
    c.g.b.k.b(paramk, "config");
    Object localObject = (f)a.get(paramk);
    if (localObject == null) {
      return null;
    }
    if (c(paramk))
    {
      d = true;
      localObject = f.a();
      if (localObject == null) {
        return null;
      }
      String str = a;
      StringBuilder localStringBuilder1 = new StringBuilder("house ");
      StringBuilder localStringBuilder2 = new StringBuilder("0000");
      localStringBuilder2.append(b.getAndIncrement());
      localStringBuilder2.append('}');
      localStringBuilder1.append(c.n.m.b(localStringBuilder2.toString(), 5));
      return (com.truecaller.ads.provider.holders.e)new g((e)localObject, new c(paramk, str, null, null, null, false, false, localStringBuilder1.toString()));
    }
    return null;
  }
  
  public final boolean c(com.truecaller.ads.k paramk)
  {
    c.g.b.k.b(paramk, "config");
    paramk = (f)a.get(paramk);
    if (paramk == null) {
      return false;
    }
    return ((c) || (b)) && (!d);
  }
  
  public final void d(com.truecaller.ads.k paramk)
  {
    c.g.b.k.b(paramk, "config");
    f localf1 = (f)a.get(paramk);
    if (localf1 == null) {
      return;
    }
    d = false;
    if (!localf1.a())
    {
      f localf2 = (f)a.get(paramk);
      if (localf2 != null)
      {
        bn localbn = e;
        if (localbn != null) {
          localbn.n();
        }
        e = kotlinx.coroutines.e.b(this, null, (c.g.a.m)new a.a(this, localf2, paramk, null), 3);
      }
    }
    a += 1;
  }
  
  public final void e(com.truecaller.ads.k paramk)
  {
    c.g.b.k.b(paramk, "config");
    paramk = (f)a.get(paramk);
    if (paramk == null) {
      return;
    }
    a -= 1;
    if (paramk.a()) {
      return;
    }
    bn localbn = e;
    if (localbn != null) {
      localbn.n();
    }
    c = false;
    b = false;
  }
  
  public final void f(com.truecaller.ads.k paramk)
  {
    c.g.b.k.b(paramk, "config");
    f localf = (f)a.get(paramk);
    if (localf == null) {
      return;
    }
    a -= 1;
    if (localf.a()) {
      return;
    }
    bn localbn = e;
    if (localbn != null) {
      localbn.n();
    }
    b = true;
    g(paramk);
  }
  
  final void g(com.truecaller.ads.k paramk)
  {
    if (c(paramk))
    {
      Object localObject = (f)a.get(paramk);
      if (localObject != null)
      {
        localObject = f;
        if (localObject != null)
        {
          ((h)localObject).d(paramk);
          return;
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.b.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */