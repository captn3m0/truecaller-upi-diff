package com.truecaller.ads.provider.b;

import c.a.y;
import java.util.List;
import javax.inject.Inject;

public final class c
  implements j
{
  private List<e> a;
  private int b;
  private final g c;
  
  @Inject
  public c(g paramg)
  {
    c = paramg;
    a = ((List)y.a);
    b = -1;
  }
  
  public final e a()
  {
    a = c.a();
    if (a.isEmpty()) {
      return null;
    }
    b += 1;
    b %= a.size();
    return (e)a.get(b);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.b.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */