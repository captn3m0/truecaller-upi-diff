package com.truecaller.ads.provider.b;

import javax.inject.Provider;

public final class d
  implements dagger.a.d<c>
{
  private final Provider<g> a;
  
  private d(Provider<g> paramProvider)
  {
    a = paramProvider;
  }
  
  public static d a(Provider<g> paramProvider)
  {
    return new d(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.b.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */