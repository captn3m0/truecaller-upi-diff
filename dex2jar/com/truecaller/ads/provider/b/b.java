package com.truecaller.ads.provider.b;

import c.d.f;
import com.truecaller.featuretoggles.e;
import dagger.a.d;
import javax.inject.Provider;

public final class b
  implements d<a>
{
  private final Provider<f> a;
  private final Provider<com.truecaller.i.a> b;
  private final Provider<e> c;
  private final Provider<j> d;
  
  private b(Provider<f> paramProvider, Provider<com.truecaller.i.a> paramProvider1, Provider<e> paramProvider2, Provider<j> paramProvider3)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static b a(Provider<f> paramProvider, Provider<com.truecaller.i.a> paramProvider1, Provider<e> paramProvider2, Provider<j> paramProvider3)
  {
    return new b(paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.b.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */