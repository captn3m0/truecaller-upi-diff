package com.truecaller.ads.provider;

import com.google.android.gms.ads.AdSize;
import com.truecaller.ads.provider.fetch.c;
import com.truecaller.ads.provider.holders.e;

public abstract interface a
{
  public abstract void a(c paramc);
  
  public abstract void a(c paramc, int paramInt);
  
  public abstract void a(e parame);
  
  public abstract void a(e parame, String paramString);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString, AdSize paramAdSize);
  
  public abstract void a(String paramString, AdSize paramAdSize, int paramInt);
  
  public abstract void a(String... paramVarArgs);
  
  public abstract void b(c paramc);
  
  public abstract void b(e parame);
  
  public abstract void b(String paramString);
  
  public abstract void b(String paramString, AdSize paramAdSize);
  
  public abstract void c(e parame);
  
  public abstract void c(String paramString, AdSize paramAdSize);
  
  public abstract void d(e parame);
  
  public abstract void e(e parame);
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */