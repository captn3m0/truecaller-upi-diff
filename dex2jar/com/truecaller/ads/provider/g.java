package com.truecaller.ads.provider;

import c.a.m;
import com.truecaller.ads.provider.b.h;
import com.truecaller.ads.provider.b.i;
import com.truecaller.ads.provider.fetch.AdsConfigurationManager;
import com.truecaller.ads.provider.fetch.c;
import com.truecaller.analytics.ae;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.log.AssertionUtil;
import com.truecaller.tracking.events.b.a;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import org.apache.a.a;
import org.apache.a.d.d;

public final class g
  implements h, f, com.truecaller.ads.provider.fetch.k, ag
{
  private final Map<com.truecaller.ads.k, com.truecaller.ads.provider.fetch.f> a;
  private final Map<com.truecaller.ads.k, Set<com.truecaller.ads.g>> b;
  private final c.d.f c;
  private final com.truecaller.androidactors.f<ae> d;
  private final AdsConfigurationManager e;
  private final com.truecaller.featuretoggles.e f;
  private final com.truecaller.ads.provider.fetch.g g;
  private final i h;
  
  @Inject
  public g(@Named("UI") c.d.f paramf, com.truecaller.androidactors.f<ae> paramf1, AdsConfigurationManager paramAdsConfigurationManager, com.truecaller.featuretoggles.e parame, com.truecaller.ads.provider.fetch.g paramg, i parami)
  {
    c = paramf;
    d = paramf1;
    e = paramAdsConfigurationManager;
    f = parame;
    g = paramg;
    h = parami;
    a = ((Map)new LinkedHashMap());
    b = ((Map)new LinkedHashMap());
  }
  
  private final com.truecaller.ads.provider.fetch.f f(com.truecaller.ads.k paramk)
  {
    Object localObject2 = (com.truecaller.ads.provider.fetch.f)a.get(paramk);
    Object localObject1 = localObject2;
    if (localObject2 == null)
    {
      localObject2 = ((Iterable)a.keySet()).iterator();
      while (((Iterator)localObject2).hasNext())
      {
        localObject1 = ((Iterator)localObject2).next();
        com.truecaller.ads.k localk = (com.truecaller.ads.k)localObject1;
        int i;
        if ((c.g.b.k.a(a, a)) && (c.g.b.k.a(b, b))) {
          i = 1;
        } else {
          i = 0;
        }
        if (i != 0) {
          break label112;
        }
      }
      localObject1 = null;
      label112:
      localObject1 = (com.truecaller.ads.k)localObject1;
      if (localObject1 != null)
      {
        localObject2 = (com.truecaller.ads.provider.fetch.f)a.remove(localObject1);
        if (localObject2 != null)
        {
          ((com.truecaller.ads.provider.fetch.f)localObject2).b();
          h.a((com.truecaller.ads.k)localObject1);
        }
        b.remove(localObject1);
      }
      localObject1 = g.a((com.truecaller.ads.provider.fetch.k)this, paramk);
      a.put(paramk, localObject1);
      if (l)
      {
        h.a(paramk, (h)this);
        return (com.truecaller.ads.provider.fetch.f)localObject1;
      }
      h.a(paramk);
    }
    return (com.truecaller.ads.provider.fetch.f)localObject1;
  }
  
  private final Set<com.truecaller.ads.g> g(com.truecaller.ads.k paramk)
  {
    Set localSet2 = (Set)b.get(paramk);
    Set localSet1 = localSet2;
    if (localSet2 == null)
    {
      localSet1 = (Set)new LinkedHashSet();
      b.put(paramk, localSet1);
    }
    return localSet1;
  }
  
  public final c.d.f V_()
  {
    return c;
  }
  
  public final com.truecaller.ads.provider.holders.e a(com.truecaller.ads.k paramk, int paramInt)
  {
    c.g.b.k.b(paramk, "config");
    if (!a()) {
      return null;
    }
    com.truecaller.ads.provider.holders.e locale2 = f(paramk).a(paramInt);
    com.truecaller.ads.provider.holders.e locale1 = locale2;
    if (locale2 == null) {
      locale1 = h.b(paramk);
    }
    return locale1;
  }
  
  public final void a(com.truecaller.ads.k paramk, com.truecaller.ads.g paramg)
  {
    c.g.b.k.b(paramk, "config");
    c.g.b.k.b(paramg, "listener");
    "Subscribing to ".concat(String.valueOf(paramk));
    g(paramk).add(paramg);
    f(paramk).a(true);
  }
  
  public final void a(com.truecaller.ads.k paramk, com.truecaller.ads.provider.holders.e parame, int paramInt)
  {
    c.g.b.k.b(paramk, "config");
    c.g.b.k.b(parame, "ad");
    Object localObject = h;
    if (localObject != null)
    {
      localObject = com.truecaller.tracking.events.b.b().b((CharSequence)localObject).a((CharSequence)hb).a(Integer.valueOf(paramInt)).c((CharSequence)parame.b()).d((CharSequence)parame.c());
      try
      {
        ((ae)d.a()).a((d)((b.a)localObject).a());
      }
      catch (a locala)
      {
        AssertionUtil.reportThrowableButNeverCrash((Throwable)locala);
      }
    }
    paramk = ((Iterable)g(paramk)).iterator();
    while (paramk.hasNext()) {
      ((com.truecaller.ads.g)paramk.next()).a(parame, paramInt);
    }
  }
  
  public final boolean a()
  {
    return e.d();
  }
  
  public final boolean a(com.truecaller.ads.k paramk)
  {
    c.g.b.k.b(paramk, "config");
    return (a()) && ((f(paramk).a()) || (h.c(paramk)));
  }
  
  public final void b(com.truecaller.ads.k paramk)
  {
    c.g.b.k.b(paramk, "config");
    if (!a()) {
      return;
    }
    f(paramk).d();
  }
  
  public final void b(com.truecaller.ads.k paramk, int paramInt)
  {
    c.g.b.k.b(paramk, "config");
    Iterator localIterator = ((Iterable)m.l((Iterable)g(paramk))).iterator();
    while (localIterator.hasNext()) {
      ((com.truecaller.ads.g)localIterator.next()).a(paramInt);
    }
    h.f(paramk);
  }
  
  public final void b(com.truecaller.ads.k paramk, com.truecaller.ads.g paramg)
  {
    c.g.b.k.b(paramk, "config");
    c.g.b.k.b(paramg, "listener");
    if ((g(paramk).remove(paramg)) && (g(paramk).isEmpty()))
    {
      f(paramk).a(false);
      "Unsubscribing from ".concat(String.valueOf(paramk));
    }
  }
  
  public final boolean b()
  {
    com.truecaller.featuretoggles.e locale = f;
    return k.a(locale, com.truecaller.featuretoggles.e.a[32]).a();
  }
  
  public final void c(com.truecaller.ads.k paramk)
  {
    c.g.b.k.b(paramk, "config");
    h.e(paramk);
    paramk = ((Iterable)m.l((Iterable)g(paramk))).iterator();
    while (paramk.hasNext()) {
      ((com.truecaller.ads.g)paramk.next()).a();
    }
  }
  
  public final void d()
  {
    Iterator localIterator = ((Iterable)m.i((Iterable)a.values())).iterator();
    while (localIterator.hasNext()) {
      ((com.truecaller.ads.provider.fetch.f)localIterator.next()).b();
    }
    a.clear();
  }
  
  public final void d(com.truecaller.ads.k paramk)
  {
    c.g.b.k.b(paramk, "config");
    paramk = ((Iterable)m.l((Iterable)g(paramk))).iterator();
    while (paramk.hasNext()) {
      ((com.truecaller.ads.g)paramk.next()).a();
    }
  }
  
  public final void e(com.truecaller.ads.k paramk)
  {
    c.g.b.k.b(paramk, "config");
    h.d(paramk);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */