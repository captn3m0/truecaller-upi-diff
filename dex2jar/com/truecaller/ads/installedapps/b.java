package com.truecaller.ads.installedapps;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class b
  implements d<InstalledAppsDatabase>
{
  private final a a;
  private final Provider<Context> b;
  
  private b(a parama, Provider<Context> paramProvider)
  {
    a = parama;
    b = paramProvider;
  }
  
  public static b a(a parama, Provider<Context> paramProvider)
  {
    return new b(parama, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.installedapps.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */