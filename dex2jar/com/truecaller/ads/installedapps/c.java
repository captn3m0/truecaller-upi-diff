package com.truecaller.ads.installedapps;

import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d<e>
{
  private final a a;
  private final Provider<InstalledAppsDatabase> b;
  
  private c(a parama, Provider<InstalledAppsDatabase> paramProvider)
  {
    a = parama;
    b = paramProvider;
  }
  
  public static c a(a parama, Provider<InstalledAppsDatabase> paramProvider)
  {
    return new c(parama, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.installedapps.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */