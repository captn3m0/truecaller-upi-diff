package com.truecaller.ads;

import android.content.Context;
import c.f;
import c.g;
import c.g.a.a;
import c.k;
import com.d.b.p;
import com.d.b.w;

public final class i
{
  private static Context b;
  private static final f<p> c;
  private static final f d;
  private static final f e = g.a(k.a, (a)i.a.a);
  
  static
  {
    f localf = g.a(k.a, (a)i.b.a);
    c = localf;
    d = localf;
  }
  
  public static final f<p> a()
  {
    return c;
  }
  
  public static final w b()
  {
    return (w)e.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */