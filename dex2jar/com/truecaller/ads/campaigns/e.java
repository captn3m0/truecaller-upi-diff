package com.truecaller.ads.campaigns;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

public abstract interface e
{
  public abstract String a();
  
  public abstract String a(int paramInt);
  
  public abstract Future<AdCampaigns[]> a(String paramString1, int paramInt, String[] paramArrayOfString, Integer paramInteger, String paramString2, String paramString3, List<String> paramList);
  
  public abstract void a(String paramString, b paramb);
  
  public abstract void b();
  
  public abstract List<Map<String, String>> c();
}

/* Location:
 * Qualified Name:     com.truecaller.ads.campaigns.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */