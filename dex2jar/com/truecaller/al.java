package com.truecaller;

import android.content.Context;
import com.truecaller.data.access.m;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class al
  implements d<m>
{
  private final c a;
  private final Provider<Context> b;
  
  private al(c paramc, Provider<Context> paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static al a(c paramc, Provider<Context> paramProvider)
  {
    return new al(paramc, paramProvider);
  }
  
  public static m a(Context paramContext)
  {
    return (m)g.a(c.c(paramContext), "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.al
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */