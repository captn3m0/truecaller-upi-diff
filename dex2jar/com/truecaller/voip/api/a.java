package com.truecaller.voip.api;

import e.b;
import e.b.f;
import e.b.o;
import e.b.s;

public abstract interface a
{
  @o(a="v0/id")
  public abstract b<VoipIdDto> a();
  
  @o(a="v0/token/rtc")
  public abstract b<RtcTokenDto> a(@e.b.a RtcTokenRequestDto paramRtcTokenRequestDto);
  
  @o(a="v0/wakeup")
  public abstract b<Void> a(@e.b.a WakeUpDto paramWakeUpDto);
  
  @f(a="v0/id/{number}")
  public abstract b<VoipIdDto> a(@s(a="number") String paramString);
  
  @o(a="v0/token/rtm")
  public abstract b<RtmTokenDto> b();
  
  @f(a="v0/phone/{voipId}")
  public abstract b<VoipNumberDto> b(@s(a="voipId") String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.voip.api.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */