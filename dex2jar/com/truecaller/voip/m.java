package com.truecaller.voip;

import com.truecaller.common.account.r;
import com.truecaller.featuretoggles.e;
import javax.inject.Provider;

public final class m
  implements dagger.a.d<l>
{
  private final Provider<e> a;
  private final Provider<r> b;
  private final Provider<com.truecaller.utils.d> c;
  
  private m(Provider<e> paramProvider, Provider<r> paramProvider1, Provider<com.truecaller.utils.d> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static m a(Provider<e> paramProvider, Provider<r> paramProvider1, Provider<com.truecaller.utils.d> paramProvider2)
  {
    return new m(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */