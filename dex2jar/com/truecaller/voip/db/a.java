package com.truecaller.voip.db;

import java.util.List;

public abstract interface a
{
  public abstract VoipIdCache a(String paramString);
  
  public abstract List<VoipAvailability> a(String[] paramArrayOfString);
  
  public abstract void a(VoipIdCache paramVoipIdCache);
  
  public abstract void a(List<VoipAvailability> paramList);
  
  public abstract VoipIdCache b(String paramString);
  
  public abstract void b(VoipIdCache paramVoipIdCache);
}

/* Location:
 * Qualified Name:     com.truecaller.voip.db.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */