package com.truecaller.voip.db;

import android.content.Context;
import c.g.b.k;
import java.util.List;
import javax.inject.Inject;

public final class d
  implements c
{
  private final Context a;
  
  @Inject
  public d(Context paramContext)
  {
    a = paramContext;
  }
  
  private final a a()
  {
    VoipDatabase localVoipDatabase = VoipDatabase.g.a(a);
    if (localVoipDatabase != null) {
      return localVoipDatabase.h();
    }
    return null;
  }
  
  public final List<VoipAvailability> a(String[] paramArrayOfString)
  {
    k.b(paramArrayOfString, "phoneNumbers");
    a locala = a();
    if (locala != null) {
      return locala.a(paramArrayOfString);
    }
    return null;
  }
  
  public final void a(List<VoipAvailability> paramList)
  {
    k.b(paramList, "availabilities");
    a locala = a();
    if (locala != null)
    {
      locala.a(paramList);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.db.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */