package com.truecaller.voip.incoming.blocked;

import android.content.Context;
import javax.inject.Inject;

public final class d
  implements b.c
{
  private final Context a;
  
  @Inject
  public d(Context paramContext)
  {
    a = paramContext;
  }
  
  /* Error */
  public final Object a(c.d.c<? super java.util.List<b.a>> paramc)
  {
    // Byte code:
    //   0: aload_1
    //   1: instanceof 33
    //   4: ifeq +34 -> 38
    //   7: aload_1
    //   8: checkcast 33	com/truecaller/voip/incoming/blocked/d$a
    //   11: astore_3
    //   12: aload_3
    //   13: getfield 36	com/truecaller/voip/incoming/blocked/d$a:b	I
    //   16: ldc 37
    //   18: iand
    //   19: ifeq +19 -> 38
    //   22: aload_3
    //   23: aload_3
    //   24: getfield 36	com/truecaller/voip/incoming/blocked/d$a:b	I
    //   27: ldc 37
    //   29: iadd
    //   30: putfield 36	com/truecaller/voip/incoming/blocked/d$a:b	I
    //   33: aload_3
    //   34: astore_1
    //   35: goto +13 -> 48
    //   38: new 33	com/truecaller/voip/incoming/blocked/d$a
    //   41: dup
    //   42: aload_0
    //   43: aload_1
    //   44: invokespecial 40	com/truecaller/voip/incoming/blocked/d$a:<init>	(Lcom/truecaller/voip/incoming/blocked/d;Lc/d/c;)V
    //   47: astore_1
    //   48: aload_1
    //   49: getfield 43	com/truecaller/voip/incoming/blocked/d$a:a	Ljava/lang/Object;
    //   52: astore_3
    //   53: getstatic 48	c/d/a/a:a	Lc/d/a/a;
    //   56: astore 4
    //   58: aload_1
    //   59: getfield 36	com/truecaller/voip/incoming/blocked/d$a:b	I
    //   62: tableswitch	default:+22->84, 0:+56->118, 1:+32->94
    //   84: new 50	java/lang/IllegalStateException
    //   87: dup
    //   88: ldc 52
    //   90: invokespecial 55	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   93: athrow
    //   94: aload_3
    //   95: instanceof 57
    //   98: ifne +8 -> 106
    //   101: aload_3
    //   102: astore_1
    //   103: goto +81 -> 184
    //   106: aload_3
    //   107: checkcast 57	c/o$b
    //   110: getfield 60	c/o$b:a	Ljava/lang/Throwable;
    //   113: athrow
    //   114: astore_1
    //   115: goto +256 -> 371
    //   118: aload_3
    //   119: instanceof 57
    //   122: ifne +258 -> 380
    //   125: aload_0
    //   126: getfield 24	com/truecaller/voip/incoming/blocked/d:a	Landroid/content/Context;
    //   129: invokevirtual 66	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   132: astore_3
    //   133: aload_3
    //   134: ldc 68
    //   136: invokestatic 70	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   139: invokestatic 76	com/truecaller/content/TruecallerContract$n:d	()Landroid/net/Uri;
    //   142: astore 5
    //   144: aload 5
    //   146: ldc 78
    //   148: invokestatic 70	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   151: aload_1
    //   152: aload_0
    //   153: putfield 80	com/truecaller/voip/incoming/blocked/d$a:d	Ljava/lang/Object;
    //   156: aload_1
    //   157: iconst_1
    //   158: putfield 36	com/truecaller/voip/incoming/blocked/d$a:b	I
    //   161: aload_3
    //   162: aload 5
    //   164: ldc 82
    //   166: ldc 84
    //   168: aload_1
    //   169: invokestatic 89	com/truecaller/utils/extensions/h:b	(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lc/d/c;)Ljava/lang/Object;
    //   172: astore_3
    //   173: aload_3
    //   174: astore_1
    //   175: aload_3
    //   176: aload 4
    //   178: if_acmpne +6 -> 184
    //   181: aload 4
    //   183: areturn
    //   184: aload_1
    //   185: checkcast 91	android/database/Cursor
    //   188: astore_1
    //   189: aload_1
    //   190: ifnull +188 -> 378
    //   193: new 93	com/truecaller/voip/incoming/blocked/a
    //   196: dup
    //   197: aload_1
    //   198: invokespecial 96	com/truecaller/voip/incoming/blocked/a:<init>	(Landroid/database/Cursor;)V
    //   201: checkcast 91	android/database/Cursor
    //   204: astore 5
    //   206: aload 5
    //   208: checkcast 98	java/io/Closeable
    //   211: astore 4
    //   213: new 100	java/util/ArrayList
    //   216: dup
    //   217: invokespecial 101	java/util/ArrayList:<init>	()V
    //   220: checkcast 103	java/util/Collection
    //   223: astore 6
    //   225: aload 5
    //   227: invokeinterface 107 1 0
    //   232: ifeq +99 -> 331
    //   235: aload 5
    //   237: checkcast 93	com/truecaller/voip/incoming/blocked/a
    //   240: astore 7
    //   242: aload 7
    //   244: invokevirtual 110	com/truecaller/voip/incoming/blocked/a:a	()Ljava/lang/String;
    //   247: astore_1
    //   248: aload 7
    //   250: invokevirtual 110	com/truecaller/voip/incoming/blocked/a:a	()Ljava/lang/String;
    //   253: checkcast 112	java/lang/CharSequence
    //   256: astore_3
    //   257: aload_3
    //   258: ifnull +135 -> 393
    //   261: aload_3
    //   262: invokestatic 117	c/n/m:a	(Ljava/lang/CharSequence;)Z
    //   265: ifeq +123 -> 388
    //   268: goto +125 -> 393
    //   271: aload_1
    //   272: astore_3
    //   273: aload_1
    //   274: ifnonnull +9 -> 283
    //   277: aload 7
    //   279: invokevirtual 119	com/truecaller/voip/incoming/blocked/a:b	()Ljava/lang/String;
    //   282: astore_3
    //   283: aload 6
    //   285: new 121	com/truecaller/voip/incoming/blocked/b$a
    //   288: dup
    //   289: aload_3
    //   290: aload 7
    //   292: invokevirtual 119	com/truecaller/voip/incoming/blocked/a:b	()Ljava/lang/String;
    //   295: aload 7
    //   297: getfield 124	com/truecaller/voip/incoming/blocked/a:b	Lcom/truecaller/utils/extensions/g;
    //   300: aload 7
    //   302: checkcast 91	android/database/Cursor
    //   305: getstatic 127	com/truecaller/voip/incoming/blocked/a:a	[Lc/l/g;
    //   308: iconst_2
    //   309: aaload
    //   310: invokevirtual 132	com/truecaller/utils/extensions/g:a	(Landroid/database/Cursor;Lc/l/g;)Ljava/lang/Object;
    //   313: checkcast 134	java/lang/Number
    //   316: invokevirtual 138	java/lang/Number:longValue	()J
    //   319: invokespecial 141	com/truecaller/voip/incoming/blocked/b$a:<init>	(Ljava/lang/String;Ljava/lang/String;J)V
    //   322: invokeinterface 145 2 0
    //   327: pop
    //   328: goto -103 -> 225
    //   331: aload 6
    //   333: checkcast 147	java/util/List
    //   336: astore_1
    //   337: aload 4
    //   339: aconst_null
    //   340: invokestatic 152	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   343: aload_1
    //   344: checkcast 154	java/lang/Iterable
    //   347: invokestatic 160	c/a/m:g	(Ljava/lang/Iterable;)Ljava/util/List;
    //   350: astore_1
    //   351: aload_1
    //   352: areturn
    //   353: astore_3
    //   354: aconst_null
    //   355: astore_1
    //   356: goto +7 -> 363
    //   359: astore_1
    //   360: aload_1
    //   361: athrow
    //   362: astore_3
    //   363: aload 4
    //   365: aload_1
    //   366: invokestatic 152	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   369: aload_3
    //   370: athrow
    //   371: aload_1
    //   372: checkcast 31	java/lang/Throwable
    //   375: invokestatic 166	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   378: aconst_null
    //   379: areturn
    //   380: aload_3
    //   381: checkcast 57	c/o$b
    //   384: getfield 60	c/o$b:a	Ljava/lang/Throwable;
    //   387: athrow
    //   388: iconst_0
    //   389: istore_2
    //   390: goto +5 -> 395
    //   393: iconst_1
    //   394: istore_2
    //   395: iload_2
    //   396: iconst_1
    //   397: ixor
    //   398: ifeq +6 -> 404
    //   401: goto -130 -> 271
    //   404: aconst_null
    //   405: astore_1
    //   406: goto -135 -> 271
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	409	0	this	d
    //   0	409	1	paramc	c.d.c<? super java.util.List<b.a>>
    //   389	9	2	i	int
    //   11	279	3	localObject1	Object
    //   353	1	3	localObject2	Object
    //   362	19	3	localObject3	Object
    //   56	308	4	localObject4	Object
    //   142	94	5	localObject5	Object
    //   223	109	6	localCollection	java.util.Collection
    //   240	61	7	locala	a
    // Exception table:
    //   from	to	target	type
    //   94	101	114	android/database/SQLException
    //   106	114	114	android/database/SQLException
    //   125	173	114	android/database/SQLException
    //   184	189	114	android/database/SQLException
    //   193	213	114	android/database/SQLException
    //   337	351	114	android/database/SQLException
    //   363	371	114	android/database/SQLException
    //   213	225	353	finally
    //   225	257	353	finally
    //   261	268	353	finally
    //   277	283	353	finally
    //   283	328	353	finally
    //   331	337	353	finally
    //   213	225	359	java/lang/Throwable
    //   225	257	359	java/lang/Throwable
    //   261	268	359	java/lang/Throwable
    //   277	283	359	java/lang/Throwable
    //   283	328	359	java/lang/Throwable
    //   331	337	359	java/lang/Throwable
    //   360	362	362	finally
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.blocked.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */