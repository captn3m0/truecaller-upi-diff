package com.truecaller.voip.incoming.blocked;

import android.app.NotificationManager;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v4.app.z.d;
import android.support.v4.app.z.f;
import android.support.v4.app.z.g;
import android.text.format.DateUtils;
import androidx.work.ListenableWorker.a;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import c.a.m;
import c.g.b.k;
import c.g.b.z;
import c.l;
import com.truecaller.utils.extensions.i;
import com.truecaller.voip.R.color;
import com.truecaller.voip.R.drawable;
import com.truecaller.voip.R.id;
import com.truecaller.voip.R.string;
import com.truecaller.voip.j.a;
import com.truecaller.voip.util.af;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;

public final class VoipBlockedCallsWorker
  extends Worker
  implements b.d
{
  public static final VoipBlockedCallsWorker.a e = new VoipBlockedCallsWorker.a((byte)0);
  @Inject
  public b.b b;
  @Inject
  public com.truecaller.notificationchannels.b c;
  @Inject
  public af d;
  private final Context f;
  
  public VoipBlockedCallsWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    f = paramContext;
  }
  
  private final z.d c()
  {
    Context localContext = f;
    com.truecaller.notificationchannels.b localb = c;
    if (localb == null) {
      k.a("notificationChannelProvider");
    }
    return new z.d(localContext, localb.g()).c(4).f(android.support.v4.content.b.c(f, R.color.truecaller_blue_all_themes)).a(R.drawable.ic_notification_blocked_call).e();
  }
  
  public final ListenableWorker.a a()
  {
    if (isStopped())
    {
      localObject = ListenableWorker.a.a();
      k.a(localObject, "Result.success()");
      return (ListenableWorker.a)localObject;
    }
    Object localObject = com.truecaller.voip.j.a;
    j.a.a().a(this);
    localObject = b;
    if (localObject == null) {
      k.a("presenter");
    }
    ((b.b)localObject).a(this);
    localObject = b;
    if (localObject == null) {
      k.a("presenter");
    }
    return ((b.b)localObject).a();
  }
  
  public final void a(b.a parama)
  {
    k.b(parama, "blockedCall");
    Object localObject1 = z.a;
    localObject1 = f.getString(R.string.voip_notification_blocked_calls_single_content);
    k.a(localObject1, "context.getString(R.stri…ked_calls_single_content)");
    localObject1 = String.format((String)localObject1, Arrays.copyOf(new Object[] { a, b }, 2));
    k.a(localObject1, "java.lang.String.format(format, *args)");
    Object localObject2 = c();
    if (c > 0L) {
      ((z.d)localObject2).a(c);
    }
    localObject1 = ((z.d)localObject2).a((CharSequence)f.getString(R.string.voip_notification_blocked_calls_single_title)).b((CharSequence)localObject1).a(BitmapFactory.decodeResource(f.getResources(), R.drawable.ic_notification_call_blocked_standard));
    localObject2 = d;
    if (localObject2 == null) {
      k.a("intentProvider");
    }
    localObject1 = ((z.d)localObject1).a(((af)localObject2).a());
    localObject2 = d;
    if (localObject2 == null) {
      k.a("intentProvider");
    }
    parama = ((z.d)localObject1).b(((af)localObject2).a(c)).h();
    i.f(f).notify(R.id.voip_blocked_call_notification, parama);
  }
  
  public final void a(List<b.a> paramList, int paramInt)
  {
    k.b(paramList, "blockedCallsToShow");
    Object localObject2 = f.getString(R.string.voip_notification_blocked_calls_grouped_content, new Object[] { Integer.valueOf(paramInt) });
    if (paramInt > paramList.size()) {
      localObject1 = f.getString(R.string.voip_notification_blocked_calls_grouped_summary, new Object[] { Integer.valueOf(paramInt - paramList.size()) });
    } else {
      localObject1 = "";
    }
    z.f localf = new z.f();
    localObject2 = (CharSequence)localObject2;
    localf.a((CharSequence)localObject2);
    localf.b((CharSequence)localObject1);
    Iterator localIterator = ((Iterable)paramList).iterator();
    while (localIterator.hasNext())
    {
      b.a locala = (b.a)localIterator.next();
      boolean bool = DateUtils.isToday(c);
      if (bool == true)
      {
        localObject1 = com.truecaller.common.h.j.f(f, c);
      }
      else
      {
        if (bool) {
          break label249;
        }
        localObject1 = com.truecaller.common.h.j.e(f, c);
      }
      k.a(localObject1, "when (DateUtils.isToday(….timestamp)\n            }");
      localf.c((CharSequence)f.getString(R.string.voip_notification_blocked_calls_grouped_caller, new Object[] { localObject1, a, b }));
      continue;
      label249:
      throw new l();
    }
    Object localObject1 = c().a((CharSequence)f.getString(R.string.voip_notification_blocked_calls_grouped_title)).b((CharSequence)localObject2);
    localObject2 = d;
    if (localObject2 == null) {
      k.a("intentProvider");
    }
    localObject1 = ((z.d)localObject1).a(((af)localObject2).a());
    localObject2 = d;
    if (localObject2 == null) {
      k.a("intentProvider");
    }
    paramList = ((z.d)localObject1).b(((af)localObject2).a(dc)).a().a((z.g)localf).h();
    i.f(f).notify(R.id.voip_blocked_call_notification, paramList);
  }
  
  public final void b()
  {
    i.f(f).cancel(R.id.voip_blocked_call_notification);
  }
  
  public final void onStopped()
  {
    super.onStopped();
    if (b != null)
    {
      b.b localb = b;
      if (localb == null) {
        k.a("presenter");
      }
      localb.y_();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.blocked.VoipBlockedCallsWorker
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */