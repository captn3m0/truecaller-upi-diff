package com.truecaller.voip.incoming.blocked;

import androidx.work.ListenableWorker.a;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.ba;
import java.util.concurrent.CancellationException;
import javax.inject.Inject;
import javax.inject.Named;

public final class c
  extends ba<b.d>
  implements b.b
{
  private final b.c c;
  private final c.d.f d;
  private final c.d.f e;
  
  @Inject
  public c(b.c paramc, @Named("UI") c.d.f paramf1, @Named("IO") c.d.f paramf2)
  {
    super(paramf1);
    c = paramc;
    d = paramf1;
    e = paramf2;
  }
  
  public final ListenableWorker.a a()
  {
    try
    {
      locala = (ListenableWorker.a)kotlinx.coroutines.f.a((m)new c.a(this, null));
    }
    catch (CancellationException localCancellationException)
    {
      ListenableWorker.a locala;
      for (;;) {}
    }
    locala = ListenableWorker.a.a();
    k.a(locala, "try {\n        runBlockin…   Result.success()\n    }");
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.blocked.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */