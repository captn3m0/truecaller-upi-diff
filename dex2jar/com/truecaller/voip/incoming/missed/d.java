package com.truecaller.voip.incoming.missed;

import androidx.work.ListenableWorker.a;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.voip.util.w;
import java.util.concurrent.CancellationException;
import javax.inject.Inject;
import javax.inject.Named;

public final class d
  extends ba<c.d>
  implements c.c
{
  private final c.d.f c;
  private final c.b d;
  private final w e;
  
  @Inject
  public d(@Named("UI") c.d.f paramf1, @Named("Async") c.d.f paramf2, c.b paramb, w paramw)
  {
    super(paramf1);
    c = paramf2;
    d = paramb;
    e = paramw;
  }
  
  public final ListenableWorker.a a()
  {
    try
    {
      locala = (ListenableWorker.a)kotlinx.coroutines.f.a(V_(), (m)new d.c(this, null));
    }
    catch (CancellationException localCancellationException)
    {
      ListenableWorker.a locala;
      for (;;) {}
    }
    locala = ListenableWorker.a.a();
    k.a(locala, "try {\n        TLog.d(\"Ch…   Result.success()\n    }");
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.missed.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */