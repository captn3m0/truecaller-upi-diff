package com.truecaller.voip.incoming.missed;

import android.graphics.Bitmap;
import java.util.List;

public abstract interface c$d
{
  public abstract void a(c.a parama, Bitmap paramBitmap);
  
  public abstract void a(List<c.a> paramList, int paramInt);
  
  public abstract void b();
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.missed.c.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */