package com.truecaller.voip.incoming.missed;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import c.g.b.k;
import com.truecaller.utils.extensions.i;
import com.truecaller.voip.R.id;
import com.truecaller.voip.R.string;
import com.truecaller.voip.j;
import com.truecaller.voip.j.a;
import com.truecaller.voip.util.z;
import javax.inject.Inject;

public final class MissedVoipCallMessageBroadcast
  extends BroadcastReceiver
{
  public static final MissedVoipCallMessageBroadcast.a b = new MissedVoipCallMessageBroadcast.a((byte)0);
  @Inject
  public z a;
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramContext != null)
    {
      if (paramIntent == null) {
        return;
      }
      j.a locala = j.a;
      j.a.a().a(this);
      i.f(paramContext).cancel(R.id.voip_incoming_service_missed_call_notification);
      paramContext.sendBroadcast(new Intent("android.intent.action.CLOSE_SYSTEM_DIALOGS"));
      if (paramIntent.hasExtra("com.truecaller.voip.extra.EXTRA_NUMBER"))
      {
        paramContext = a;
        if (paramContext == null) {
          k.a("voipCallMessage");
        }
        paramIntent = paramIntent.getStringExtra("com.truecaller.voip.extra.EXTRA_NUMBER");
        k.a(paramIntent, "intent.getStringExtra(EXTRA_NUMBER)");
        paramContext.a(paramIntent, Integer.valueOf(R.string.voip_empty));
      }
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.missed.MissedVoipCallMessageBroadcast
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */