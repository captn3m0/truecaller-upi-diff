package com.truecaller.voip.incoming.missed;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.support.v4.app.z.d;
import android.support.v4.app.z.f;
import android.support.v4.app.z.g;
import android.text.format.DateUtils;
import androidx.work.ListenableWorker.a;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import c.a.m;
import c.g.b.k;
import c.l;
import com.truecaller.utils.extensions.i;
import com.truecaller.voip.R.color;
import com.truecaller.voip.R.drawable;
import com.truecaller.voip.R.id;
import com.truecaller.voip.R.plurals;
import com.truecaller.voip.R.string;
import com.truecaller.voip.incall.VoipService;
import com.truecaller.voip.incall.VoipService.a;
import com.truecaller.voip.j.a;
import com.truecaller.voip.util.af;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;

public final class MissedVoipCallsWorker
  extends Worker
  implements c.d
{
  public static final MissedVoipCallsWorker.a e = new MissedVoipCallsWorker.a((byte)0);
  @Inject
  public c.c b;
  @Inject
  public com.truecaller.notificationchannels.b c;
  @Inject
  public af d;
  private final Context f;
  
  public MissedVoipCallsWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    f = paramContext;
  }
  
  private final z.d c()
  {
    Context localContext = f;
    com.truecaller.notificationchannels.b localb = c;
    if (localb == null) {
      k.a("callingNotificationChannelProvider");
    }
    return new z.d(localContext, localb.X_()).c(4).f(android.support.v4.content.b.c(f, R.color.truecaller_blue_all_themes)).a(R.drawable.ic_notification_call_missed).e();
  }
  
  public final ListenableWorker.a a()
  {
    if (isStopped())
    {
      localObject = ListenableWorker.a.a();
      k.a(localObject, "Result.success()");
      return (ListenableWorker.a)localObject;
    }
    Object localObject = com.truecaller.voip.j.a;
    j.a.a().a(this);
    localObject = b;
    if (localObject == null) {
      k.a("presenter");
    }
    ((c.c)localObject).a(this);
    localObject = b;
    if (localObject == null) {
      k.a("presenter");
    }
    localObject = ((c.c)localObject).a();
    c.c localc = b;
    if (localc == null) {
      k.a("presenter");
    }
    localc.y_();
    return (ListenableWorker.a)localObject;
  }
  
  public final void a(c.a parama, Bitmap paramBitmap)
  {
    k.b(parama, "missedCall");
    Object localObject1 = VoipService.e;
    localObject1 = VoipService.a.a(f, b);
    ((Intent)localObject1).putExtra("com.truecaller.voip.incoming.EXTRA_FROM_MISSED_CALL", true);
    Object localObject2 = f;
    int i = R.id.voip_notification_missed_action_call_back;
    k.b(localObject2, "receiver$0");
    k.b(localObject1, "intent");
    if (Build.VERSION.SDK_INT >= 26)
    {
      localObject1 = PendingIntent.getForegroundService((Context)localObject2, i, (Intent)localObject1, 134217728);
      k.a(localObject1, "PendingIntent.getForegro…questCode, intent, flags)");
    }
    else
    {
      localObject1 = PendingIntent.getService((Context)localObject2, i, (Intent)localObject1, 134217728);
      k.a(localObject1, "PendingIntent.getService…questCode, intent, flags)");
    }
    localObject2 = f;
    Object localObject3 = MissedVoipCallMessageBroadcast.b;
    Object localObject4 = f;
    localObject3 = b;
    k.b(localObject4, "context");
    k.b(localObject3, "number");
    localObject4 = new Intent((Context)localObject4, MissedVoipCallMessageBroadcast.class);
    ((Intent)localObject4).putExtra("com.truecaller.voip.extra.EXTRA_NUMBER", (String)localObject3);
    localObject2 = PendingIntent.getBroadcast((Context)localObject2, 0, (Intent)localObject4, 134217728);
    localObject3 = c();
    if (d > 0L) {
      ((z.d)localObject3).a(d);
    }
    localObject1 = ((z.d)localObject3).a(R.drawable.ic_notification_call, (CharSequence)f.getString(R.string.voip_button_notification_call_back), (PendingIntent)localObject1).a(R.drawable.ic_sms, (CharSequence)f.getString(R.string.voip_button_notification_message), (PendingIntent)localObject2);
    if (paramBitmap != null) {
      ((z.d)localObject1).a(paramBitmap);
    }
    paramBitmap = ((z.d)localObject1).a((CharSequence)f.getResources().getQuantityString(R.plurals.voip_notification_missed_grouped_title, 1, new Object[] { f.getString(R.string.voip_text) })).b((CharSequence)a);
    localObject1 = d;
    if (localObject1 == null) {
      k.a("missedCallIntentProvider");
    }
    paramBitmap = paramBitmap.a(((af)localObject1).a());
    localObject1 = d;
    if (localObject1 == null) {
      k.a("missedCallIntentProvider");
    }
    parama = paramBitmap.b(((af)localObject1).a(d)).h();
    i.f(f).notify(R.id.voip_incoming_service_missed_call_notification, parama);
  }
  
  public final void a(List<c.a> paramList, int paramInt)
  {
    k.b(paramList, "missedCallsToShow");
    String str = f.getResources().getQuantityString(R.plurals.voip_notification_missed_grouped_title, paramInt, new Object[] { f.getString(R.string.voip_text) });
    Object localObject2 = f;
    int i = R.string.voip_notification_missed_grouped_message;
    if (paramInt > 99)
    {
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append(paramInt);
      ((StringBuilder)localObject1).append('+');
      localObject1 = ((StringBuilder)localObject1).toString();
    }
    else
    {
      localObject1 = Integer.valueOf(paramInt);
    }
    Object localObject1 = ((Context)localObject2).getString(i, new Object[] { localObject1, f.getString(R.string.voip_text) });
    localObject2 = new z.f();
    CharSequence localCharSequence = (CharSequence)localObject1;
    ((z.f)localObject2).a(localCharSequence);
    Iterator localIterator = ((Iterable)paramList).iterator();
    while (localIterator.hasNext())
    {
      c.a locala = (c.a)localIterator.next();
      boolean bool = DateUtils.isToday(d);
      if (bool == true)
      {
        localObject1 = com.truecaller.common.h.j.f(f, d);
      }
      else
      {
        if (bool) {
          break label282;
        }
        localObject1 = com.truecaller.common.h.j.e(f, d);
      }
      k.a(localObject1, "when (DateUtils.isToday(….timestamp)\n            }");
      ((z.f)localObject2).c((CharSequence)f.getString(R.string.voip_notification_missed_grouped_time_and_caller, new Object[] { localObject1, a }));
      continue;
      label282:
      throw new l();
    }
    if (paramInt > paramList.size()) {
      ((z.f)localObject2).c((CharSequence)f.getString(R.string.voip_notification_missed_grouped_more, new Object[] { Integer.valueOf(paramInt - paramList.size()) }));
    }
    long l = dd;
    paramList = c().a((CharSequence)str).b(localCharSequence);
    localObject1 = d;
    if (localObject1 == null) {
      k.a("missedCallIntentProvider");
    }
    paramList = paramList.a(((af)localObject1).a());
    localObject1 = d;
    if (localObject1 == null) {
      k.a("missedCallIntentProvider");
    }
    paramList = paramList.b(((af)localObject1).a(l)).a().a((z.g)localObject2).h();
    i.f(f).notify(R.id.voip_incoming_service_missed_call_notification, paramList);
  }
  
  public final void b()
  {
    i.f(f).cancel(R.id.voip_incoming_service_missed_call_notification);
  }
  
  public final void onStopped()
  {
    super.onStopped();
    if (b != null)
    {
      c.c localc = b;
      if (localc == null) {
        k.a("presenter");
      }
      localc.y_();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.missed.MissedVoipCallsWorker
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */