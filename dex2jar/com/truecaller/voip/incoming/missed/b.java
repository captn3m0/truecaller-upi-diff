package com.truecaller.voip.incoming.missed;

import android.content.Context;
import javax.inject.Inject;

public final class b
  implements c.b
{
  private final Context a;
  
  @Inject
  public b(Context paramContext)
  {
    a = paramContext;
  }
  
  /* Error */
  public final Object a(c.d.c<? super java.util.List<c.a>> paramc)
  {
    // Byte code:
    //   0: aload_1
    //   1: instanceof 33
    //   4: ifeq +34 -> 38
    //   7: aload_1
    //   8: checkcast 33	com/truecaller/voip/incoming/missed/b$a
    //   11: astore_3
    //   12: aload_3
    //   13: getfield 36	com/truecaller/voip/incoming/missed/b$a:b	I
    //   16: ldc 37
    //   18: iand
    //   19: ifeq +19 -> 38
    //   22: aload_3
    //   23: aload_3
    //   24: getfield 36	com/truecaller/voip/incoming/missed/b$a:b	I
    //   27: ldc 37
    //   29: iadd
    //   30: putfield 36	com/truecaller/voip/incoming/missed/b$a:b	I
    //   33: aload_3
    //   34: astore_1
    //   35: goto +13 -> 48
    //   38: new 33	com/truecaller/voip/incoming/missed/b$a
    //   41: dup
    //   42: aload_0
    //   43: aload_1
    //   44: invokespecial 40	com/truecaller/voip/incoming/missed/b$a:<init>	(Lcom/truecaller/voip/incoming/missed/b;Lc/d/c;)V
    //   47: astore_1
    //   48: aload_1
    //   49: getfield 43	com/truecaller/voip/incoming/missed/b$a:a	Ljava/lang/Object;
    //   52: astore_3
    //   53: getstatic 48	c/d/a/a:a	Lc/d/a/a;
    //   56: astore 4
    //   58: aload_1
    //   59: getfield 36	com/truecaller/voip/incoming/missed/b$a:b	I
    //   62: tableswitch	default:+22->84, 0:+56->118, 1:+32->94
    //   84: new 50	java/lang/IllegalStateException
    //   87: dup
    //   88: ldc 52
    //   90: invokespecial 55	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   93: athrow
    //   94: aload_3
    //   95: instanceof 57
    //   98: ifne +8 -> 106
    //   101: aload_3
    //   102: astore_1
    //   103: goto +81 -> 184
    //   106: aload_3
    //   107: checkcast 57	c/o$b
    //   110: getfield 60	c/o$b:a	Ljava/lang/Throwable;
    //   113: athrow
    //   114: astore_1
    //   115: goto +277 -> 392
    //   118: aload_3
    //   119: instanceof 57
    //   122: ifne +279 -> 401
    //   125: aload_0
    //   126: getfield 24	com/truecaller/voip/incoming/missed/b:a	Landroid/content/Context;
    //   129: invokevirtual 66	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   132: astore_3
    //   133: aload_3
    //   134: ldc 68
    //   136: invokestatic 70	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   139: invokestatic 76	com/truecaller/content/TruecallerContract$n:d	()Landroid/net/Uri;
    //   142: astore 5
    //   144: aload 5
    //   146: ldc 78
    //   148: invokestatic 70	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   151: aload_1
    //   152: aload_0
    //   153: putfield 80	com/truecaller/voip/incoming/missed/b$a:d	Ljava/lang/Object;
    //   156: aload_1
    //   157: iconst_1
    //   158: putfield 36	com/truecaller/voip/incoming/missed/b$a:b	I
    //   161: aload_3
    //   162: aload 5
    //   164: ldc 82
    //   166: ldc 84
    //   168: aload_1
    //   169: invokestatic 89	com/truecaller/utils/extensions/h:b	(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lc/d/c;)Ljava/lang/Object;
    //   172: astore_3
    //   173: aload_3
    //   174: astore_1
    //   175: aload_3
    //   176: aload 4
    //   178: if_acmpne +6 -> 184
    //   181: aload 4
    //   183: areturn
    //   184: aload_1
    //   185: checkcast 91	android/database/Cursor
    //   188: astore_1
    //   189: aload_1
    //   190: ifnull +209 -> 399
    //   193: new 93	com/truecaller/voip/incoming/missed/a
    //   196: dup
    //   197: aload_1
    //   198: invokespecial 96	com/truecaller/voip/incoming/missed/a:<init>	(Landroid/database/Cursor;)V
    //   201: checkcast 91	android/database/Cursor
    //   204: astore 5
    //   206: aload 5
    //   208: checkcast 98	java/io/Closeable
    //   211: astore 4
    //   213: new 100	java/util/ArrayList
    //   216: dup
    //   217: invokespecial 101	java/util/ArrayList:<init>	()V
    //   220: checkcast 103	java/util/Collection
    //   223: astore 6
    //   225: aload 5
    //   227: invokeinterface 107 1 0
    //   232: ifeq +120 -> 352
    //   235: aload 5
    //   237: checkcast 93	com/truecaller/voip/incoming/missed/a
    //   240: astore 7
    //   242: aload 7
    //   244: invokevirtual 110	com/truecaller/voip/incoming/missed/a:a	()Ljava/lang/String;
    //   247: astore_1
    //   248: aload 7
    //   250: invokevirtual 110	com/truecaller/voip/incoming/missed/a:a	()Ljava/lang/String;
    //   253: checkcast 112	java/lang/CharSequence
    //   256: astore_3
    //   257: aload_3
    //   258: ifnull +156 -> 414
    //   261: aload_3
    //   262: invokestatic 117	c/n/m:a	(Ljava/lang/CharSequence;)Z
    //   265: ifeq +144 -> 409
    //   268: goto +146 -> 414
    //   271: aload_1
    //   272: astore_3
    //   273: aload_1
    //   274: ifnonnull +9 -> 283
    //   277: aload 7
    //   279: invokevirtual 119	com/truecaller/voip/incoming/missed/a:b	()Ljava/lang/String;
    //   282: astore_3
    //   283: aload 6
    //   285: new 121	com/truecaller/voip/incoming/missed/c$a
    //   288: dup
    //   289: aload_3
    //   290: aload 7
    //   292: invokevirtual 119	com/truecaller/voip/incoming/missed/a:b	()Ljava/lang/String;
    //   295: aload 7
    //   297: getfield 124	com/truecaller/voip/incoming/missed/a:b	Lcom/truecaller/utils/extensions/g;
    //   300: aload 7
    //   302: checkcast 91	android/database/Cursor
    //   305: getstatic 127	com/truecaller/voip/incoming/missed/a:a	[Lc/l/g;
    //   308: iconst_2
    //   309: aaload
    //   310: invokevirtual 132	com/truecaller/utils/extensions/g:a	(Landroid/database/Cursor;Lc/l/g;)Ljava/lang/Object;
    //   313: checkcast 134	java/lang/String
    //   316: aload 7
    //   318: getfield 137	com/truecaller/voip/incoming/missed/a:c	Lcom/truecaller/utils/extensions/g;
    //   321: aload 7
    //   323: checkcast 91	android/database/Cursor
    //   326: getstatic 127	com/truecaller/voip/incoming/missed/a:a	[Lc/l/g;
    //   329: iconst_3
    //   330: aaload
    //   331: invokevirtual 132	com/truecaller/utils/extensions/g:a	(Landroid/database/Cursor;Lc/l/g;)Ljava/lang/Object;
    //   334: checkcast 139	java/lang/Number
    //   337: invokevirtual 143	java/lang/Number:longValue	()J
    //   340: invokespecial 146	com/truecaller/voip/incoming/missed/c$a:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    //   343: invokeinterface 150 2 0
    //   348: pop
    //   349: goto -124 -> 225
    //   352: aload 6
    //   354: checkcast 152	java/util/List
    //   357: astore_1
    //   358: aload 4
    //   360: aconst_null
    //   361: invokestatic 157	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   364: aload_1
    //   365: checkcast 159	java/lang/Iterable
    //   368: invokestatic 165	c/a/m:g	(Ljava/lang/Iterable;)Ljava/util/List;
    //   371: astore_1
    //   372: aload_1
    //   373: areturn
    //   374: astore_3
    //   375: aconst_null
    //   376: astore_1
    //   377: goto +7 -> 384
    //   380: astore_1
    //   381: aload_1
    //   382: athrow
    //   383: astore_3
    //   384: aload 4
    //   386: aload_1
    //   387: invokestatic 157	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   390: aload_3
    //   391: athrow
    //   392: aload_1
    //   393: checkcast 31	java/lang/Throwable
    //   396: invokestatic 171	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   399: aconst_null
    //   400: areturn
    //   401: aload_3
    //   402: checkcast 57	c/o$b
    //   405: getfield 60	c/o$b:a	Ljava/lang/Throwable;
    //   408: athrow
    //   409: iconst_0
    //   410: istore_2
    //   411: goto +5 -> 416
    //   414: iconst_1
    //   415: istore_2
    //   416: iload_2
    //   417: iconst_1
    //   418: ixor
    //   419: ifeq +6 -> 425
    //   422: goto -151 -> 271
    //   425: aconst_null
    //   426: astore_1
    //   427: goto -156 -> 271
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	430	0	this	b
    //   0	430	1	paramc	c.d.c<? super java.util.List<c.a>>
    //   410	9	2	i	int
    //   11	279	3	localObject1	Object
    //   374	1	3	localObject2	Object
    //   383	19	3	localObject3	Object
    //   56	329	4	localObject4	Object
    //   142	94	5	localObject5	Object
    //   223	130	6	localCollection	java.util.Collection
    //   240	82	7	locala	a
    // Exception table:
    //   from	to	target	type
    //   94	101	114	android/database/SQLException
    //   106	114	114	android/database/SQLException
    //   125	173	114	android/database/SQLException
    //   184	189	114	android/database/SQLException
    //   193	213	114	android/database/SQLException
    //   358	372	114	android/database/SQLException
    //   384	392	114	android/database/SQLException
    //   213	225	374	finally
    //   225	257	374	finally
    //   261	268	374	finally
    //   277	283	374	finally
    //   283	349	374	finally
    //   352	358	374	finally
    //   213	225	380	java/lang/Throwable
    //   225	257	380	java/lang/Throwable
    //   261	268	380	java/lang/Throwable
    //   277	283	380	java/lang/Throwable
    //   283	349	380	java/lang/Throwable
    //   352	358	380	java/lang/Throwable
    //   381	383	383	finally
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.missed.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */