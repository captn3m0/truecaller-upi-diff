package com.truecaller.voip.incoming;

import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.support.v4.app.z.a;
import android.support.v4.app.z.a.a;
import android.support.v4.app.z.d;
import android.support.v4.content.b;
import android.telephony.TelephonyManager;
import androidx.work.g;
import androidx.work.k.a;
import androidx.work.p;
import c.d.f;
import c.g.a.m;
import com.truecaller.utils.extensions.i;
import com.truecaller.voip.R.color;
import com.truecaller.voip.R.drawable;
import com.truecaller.voip.R.id;
import com.truecaller.voip.R.string;
import com.truecaller.voip.VoipUser;
import com.truecaller.voip.incall.VoipService;
import com.truecaller.voip.incall.VoipService.a;
import com.truecaller.voip.incoming.blocked.VoipBlockedCallsWorker;
import com.truecaller.voip.incoming.missed.MissedVoipCallsWorker;
import com.truecaller.voip.incoming.missed.MissedVoipCallsWorker.a;
import com.truecaller.voip.incoming.ui.IncomingVoipActivity;
import com.truecaller.voip.incoming.ui.IncomingVoipActivity.a;
import com.truecaller.voip.j;
import com.truecaller.voip.j.a;
import com.truecaller.voip.util.w;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.e;

public final class IncomingVoipService
  extends Service
  implements b.d, ag
{
  public static final IncomingVoipService.a e = new IncomingVoipService.a((byte)0);
  private static boolean h;
  @Inject
  @Named("UI")
  public f a;
  @Inject
  @Named("IO")
  public f b;
  @Inject
  public b.c c;
  @Inject
  public w d;
  private BroadcastReceiver f;
  private z.d g;
  
  private final void i()
  {
    z.d locald = g;
    if (locald != null)
    {
      startForeground(R.id.voip_incoming_service_foreground_notification, locald.h());
      return;
    }
  }
  
  public final f V_()
  {
    f localf = a;
    if (localf == null) {
      c.g.b.k.a("uiContext");
    }
    return localf;
  }
  
  public final void a()
  {
    IncomingVoipActivity.a locala = IncomingVoipActivity.a;
    startActivity(IncomingVoipActivity.a.a((Context)this, false, false));
  }
  
  public final void a(int paramInt)
  {
    z.d locald = g;
    if (locald == null) {
      return;
    }
    f localf = b;
    if (localf == null) {
      c.g.b.k.a("asyncContext");
    }
    e.b(this, localf, (m)new IncomingVoipService.c(this, paramInt, locald, null), 2);
  }
  
  public final void a(Bitmap paramBitmap)
  {
    c.g.b.k.b(paramBitmap, "icon");
    z.d locald = g;
    if (locald != null) {
      locald.a(paramBitmap);
    }
    i();
  }
  
  public final void a(VoipUser paramVoipUser, String paramString)
  {
    c.g.b.k.b(paramVoipUser, "voipUser");
    c.g.b.k.b(paramString, "channelId");
    Object localObject = VoipService.e;
    localObject = (Context)this;
    c.g.b.k.b(localObject, "context");
    c.g.b.k.b(paramVoipUser, "voipUser");
    c.g.b.k.b(paramString, "channelId");
    Intent localIntent = new Intent((Context)localObject, VoipService.class);
    localIntent.putExtra("com.truecaller.voip.extra.EXTRA_USER_ID", (Parcelable)paramVoipUser);
    localIntent.putExtra("com.truecaller.voip.extra.EXTRA_CHANNEL_ID", paramString);
    b.a((Context)localObject, localIntent);
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "title");
    z.d locald = g;
    if (locald != null) {
      locald.a((CharSequence)paramString);
    }
    i();
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "channelId");
    Object localObject2 = (CharSequence)getString(R.string.voip_button_notification_answer);
    Object localObject1 = IncomingVoipActivity.a;
    localObject1 = (Context)this;
    Object localObject3 = IncomingVoipActivity.a.a((Context)localObject1, true, true);
    localObject3 = PendingIntent.getActivity((Context)localObject1, R.id.voip_incoming_notification_action_answer, (Intent)localObject3, 134217728);
    c.g.b.k.a(localObject3, "PendingIntent.getActivit…tent.FLAG_UPDATE_CURRENT)");
    localObject2 = new z.a.a(0, (CharSequence)localObject2, (PendingIntent)localObject3).a();
    localObject3 = (CharSequence)getString(R.string.voip_button_notification_decline);
    c.g.b.k.b(localObject1, "context");
    Object localObject4 = new Intent((Context)localObject1, IncomingVoipService.class);
    ((Intent)localObject4).setAction("com.truecaller.voip.incoming.ACTION_NOTIFICATION");
    ((Intent)localObject4).putExtra("com.truecaller.voip.incoming.EXTRA_ACTION_REJECT_CALL", true);
    localObject4 = PendingIntent.getService((Context)localObject1, R.id.voip_incoming_notification_action_decline, (Intent)localObject4, 134217728);
    c.g.b.k.a(localObject4, "PendingIntent.getService…tent.FLAG_UPDATE_CURRENT)");
    localObject3 = new z.a.a(0, (CharSequence)localObject3, (PendingIntent)localObject4).a();
    localObject4 = IncomingVoipActivity.a;
    localObject4 = PendingIntent.getActivity((Context)localObject1, 0, IncomingVoipActivity.a.a((Context)localObject1, false, false), 0);
    g = new z.d((Context)localObject1, paramString).a(R.drawable.ic_voip_notification).b().d().f(b.c((Context)localObject1, R.color.voip_header_color)).b((CharSequence)getString(R.string.voip_status_incoming_audio_call, new Object[] { getString(R.string.voip_text) })).a((z.a)localObject3).a((z.a)localObject2).a((PendingIntent)localObject4).e(1);
    i();
  }
  
  public final boolean b()
  {
    VoipService.a locala = VoipService.e;
    return (VoipService.p()) || (i.b(this).getCallState() != 0);
  }
  
  public final void d()
  {
    MissedVoipCallsWorker.a locala = MissedVoipCallsWorker.e;
    MissedVoipCallsWorker.a.a();
  }
  
  public final void e()
  {
    Object localObject = VoipBlockedCallsWorker.e;
    localObject = new k.a(VoipBlockedCallsWorker.class).c();
    c.g.b.k.a(localObject, "OneTimeWorkRequest.Build…\n                .build()");
    localObject = (androidx.work.k)localObject;
    p.a().a("com.truecaller.voip.incoming.blocked.BlockedVoipCallsWorker", g.a, (androidx.work.k)localObject);
  }
  
  public final void f()
  {
    sendBroadcast(new Intent("android.intent.action.CLOSE_SYSTEM_DIALOGS"));
  }
  
  public final void g()
  {
    stopForeground(true);
    stopSelf();
  }
  
  public final void onCreate()
  {
    super.onCreate();
    h = true;
    Object localObject = j.a;
    j.a.a().a(this);
    f = ((BroadcastReceiver)new IncomingVoipService.b(this));
    localObject = new IntentFilter();
    ((IntentFilter)localObject).addAction("android.intent.action.SCREEN_OFF");
    ((IntentFilter)localObject).addAction("android.media.VOLUME_CHANGED_ACTION");
    registerReceiver(f, (IntentFilter)localObject);
  }
  
  public final void onDestroy()
  {
    h = false;
    unregisterReceiver(f);
    b.c localc = c;
    if (localc == null) {
      c.g.b.k.a("presenter");
    }
    localc.y_();
    super.onDestroy();
  }
  
  public final int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    String str3 = null;
    String str1;
    if (paramIntent != null) {
      str1 = paramIntent.getAction();
    } else {
      str1 = null;
    }
    String str2;
    if (paramIntent != null) {
      str2 = paramIntent.getStringExtra("com.truecaller.voip.extra.EXTRA_VOIP_ID");
    } else {
      str2 = null;
    }
    if (paramIntent != null) {
      str3 = paramIntent.getStringExtra("com.truecaller.voip.extra.EXTRA_CHANNEL_ID");
    }
    b.c localc = c;
    if (localc == null) {
      c.g.b.k.a("presenter");
    }
    localc.a(this);
    if (str1 == null)
    {
      paramIntent = c;
      if (paramIntent == null) {
        c.g.b.k.a("presenter");
      }
      paramIntent.a(str2, str3);
    }
    else if (paramIntent.getBooleanExtra("com.truecaller.voip.incoming.EXTRA_ACTION_REJECT_CALL", false))
    {
      paramIntent = c;
      if (paramIntent == null) {
        c.g.b.k.a("presenter");
      }
      paramIntent.h();
    }
    return 2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.IncomingVoipService
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */