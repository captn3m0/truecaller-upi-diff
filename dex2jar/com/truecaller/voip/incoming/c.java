package com.truecaller.voip.incoming;

import c.g.a.a;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.notificationchannels.b;
import com.truecaller.utils.n;
import com.truecaller.voip.R.color;
import com.truecaller.voip.R.string;
import com.truecaller.voip.VoipState;
import com.truecaller.voip.VoipStateReason;
import com.truecaller.voip.VoipUser;
import com.truecaller.voip.ag;
import com.truecaller.voip.manager.rtm.RtmMsgAction;
import com.truecaller.voip.util.VoipAnalyticsContext;
import com.truecaller.voip.util.VoipAnalyticsNotificationAction;
import com.truecaller.voip.util.VoipEventType;
import com.truecaller.voip.util.ab;
import com.truecaller.voip.util.an;
import com.truecaller.voip.util.au;
import com.truecaller.voip.util.o;
import com.truecaller.voip.util.s;
import com.truecaller.voip.util.w;
import com.truecaller.voip.util.y;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.e;

public final class c
  extends ba<b.d>
  implements b.c
{
  final kotlinx.coroutines.a.h<VoipUser> c;
  private String d;
  private VoipUser e;
  private boolean f;
  private String g;
  private ag h;
  private b.b i;
  private final kotlinx.coroutines.a.h<ag> j;
  private final c.d.f k;
  private final com.truecaller.voip.manager.rtm.i l;
  private final au m;
  private final b n;
  private final w o;
  private final y p;
  private final n q;
  private final s r;
  private final com.truecaller.voip.manager.rtm.h s;
  private final com.truecaller.voip.util.f t;
  private final o u;
  private final an v;
  
  @Inject
  public c(@Named("UI") c.d.f paramf1, @Named("IO") c.d.f paramf2, com.truecaller.voip.manager.rtm.i parami, au paramau, b paramb, w paramw, y paramy, n paramn, s params, com.truecaller.voip.manager.rtm.h paramh, com.truecaller.voip.util.f paramf, o paramo, an paraman)
  {
    super(paramf1);
    k = paramf2;
    l = parami;
    m = paramau;
    n = paramb;
    o = paramw;
    p = paramy;
    q = paramn;
    r = params;
    s = paramh;
    t = paramf;
    u = paramo;
    v = paraman;
    h = new ag(null, null, 0, 0, false, null, false, 255);
    c = kotlinx.coroutines.a.i.a(-1);
    j = kotlinx.coroutines.a.i.a(-1);
  }
  
  private final bn a(RtmMsgAction paramRtmMsgAction)
  {
    return e.b(this, null, (m)new c.j(this, paramRtmMsgAction, null), 3);
  }
  
  private final void a(VoipState paramVoipState, VoipStateReason paramVoipStateReason)
  {
    int i1;
    if (paramVoipState == h.a)
    {
      i1 = 0;
    }
    else
    {
      localObject = h.a;
      switch (d.d[localObject.ordinal()])
      {
      default: 
        i1 = 1;
        break;
      case 1: 
      case 2: 
      case 3: 
      case 4: 
      case 5: 
      case 6: 
        i1 = 0;
      }
    }
    if (i1 == 0) {
      return;
    }
    Object localObject = new StringBuilder("Setting state: ");
    ((StringBuilder)localObject).append(paramVoipState.name());
    ((StringBuilder)localObject).toString();
    a locala = (a)c.ab.a;
    ag localag;
    switch (d.c[paramVoipState.ordinal()])
    {
    default: 
      locala = (a)new c.r(this);
      localObject = (a)new c.s(this);
      localag = new ag(paramVoipState, null, R.string.voip_status_call_ended, R.color.voip_call_status_error_color, true, "Error. Exiting...", false, 134);
      paramVoipState = locala;
      break;
    case 7: 
      locala = (a)new c.p(this);
      localObject = (a)new c.q(this);
      localag = new ag(paramVoipState, null, R.string.voip_status_call_ended, R.color.voip_call_status_error_color, true, "Call cancelled. Exiting...", false, 134);
      paramVoipState = locala;
      break;
    case 6: 
      locala = (a)new c.n(this);
      localObject = (a)new c.o(this);
      localag = new ag(paramVoipState, null, 0, 0, false, "Call blocked. Exiting...", false, 190);
      paramVoipState = locala;
      break;
    case 5: 
      locala = (a)new c.z(this);
      localObject = (a)new c.aa(this);
      localag = new ag(paramVoipState, null, R.string.voip_status_no_answer, R.color.voip_call_status_error_color, true, "No answer. Exiting...", false, 134);
      paramVoipState = locala;
      break;
    case 4: 
      locala = (a)new c.x(this);
      localObject = (a)new c.y(this);
      localag = new ag(paramVoipState, null, R.string.voip_status_rejected, R.color.voip_call_status_error_color, true, "Incoming call is rejected. Exiting...", false, 134);
      paramVoipState = locala;
      break;
    case 3: 
      localObject = (a)new c.w(this);
      localag = new ag(paramVoipState, null, R.string.voip_status_connecting, R.color.voip_call_status_warning_color, true, "Incoming call is accepted. Opening VoIP screen...", false, 134);
      paramVoipState = locala;
      break;
    case 2: 
      locala = (a)new c.u(this);
      localObject = (a)new c.v(this);
      localag = new ag(paramVoipState, null, R.string.voip_status_incoming, R.color.voip_call_status_neutral_color, false, "Incoming call is received.", false, 134);
      paramVoipState = locala;
      break;
    case 1: 
      locala = (a)new c.m(this);
      localObject = (a)new c.t(this);
      localag = new ag(paramVoipState, null, R.string.voip_status_connecting, R.color.voip_call_status_warning_color, true, "Initializing and resolving user details...", false, 134);
      paramVoipState = locala;
    }
    h = localag;
    h = ag.a(h, null, paramVoipStateReason, null, 0, 0, false, null, false, 253);
    paramVoipState.invoke();
    paramVoipState = i;
    if (paramVoipState != null) {
      paramVoipState.a(h.d, h.c(), h.d());
    }
    paramVoipState = i;
    if (paramVoipState != null) {
      paramVoipState.a(h.e);
    }
    j.d_(h);
    ((a)localObject).invoke();
  }
  
  private final void a(VoipUser paramVoipUser)
  {
    p.a(new ab(b, VoipEventType.MISSED, 0L, null, 12));
    paramVoipUser = (b.d)b;
    if (paramVoipUser != null)
    {
      paramVoipUser.d();
      return;
    }
  }
  
  private final void a(String paramString)
  {
    p.a(new ab(paramString, VoipEventType.BLOCKED, 0L, null, 12));
    paramString = (b.d)b;
    if (paramString != null)
    {
      paramString.e();
      return;
    }
  }
  
  private final String j()
  {
    if (f)
    {
      VoipUser localVoipUser = e;
      if (localVoipUser == null) {
        k.a("voipUser");
      }
      return a;
    }
    return g;
  }
  
  public final kotlinx.coroutines.a.h<VoipUser> a()
  {
    return c;
  }
  
  public final bn a(String paramString1, String paramString2)
  {
    return e.b(this, null, (m)new c.e(this, paramString1, paramString2, null), 3);
  }
  
  public final void a(b.b paramb)
  {
    i = paramb;
  }
  
  public final kotlinx.coroutines.a.h<ag> b()
  {
    return j;
  }
  
  public final void c()
  {
    if (f) {
      a(VoipState.ACCEPTED, null);
    }
  }
  
  public final void e()
  {
    a(VoipState.REJECTED, null);
  }
  
  public final void f()
  {
    i();
  }
  
  public final ag g()
  {
    return h;
  }
  
  public final void h()
  {
    u.a(VoipAnalyticsContext.NOTIFICATION.getValue(), VoipAnalyticsNotificationAction.REJECTED);
    b.d locald = (b.d)b;
    if (locald != null) {
      locald.f();
    }
    e();
  }
  
  public final void i()
  {
    t.c();
    r.b();
  }
  
  public final void y_()
  {
    t.c();
    r.f();
    if (h.a != VoipState.ACCEPTED) {
      s.b();
    }
    super.y_();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */