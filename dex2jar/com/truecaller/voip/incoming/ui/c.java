package com.truecaller.voip.incoming.ui;

import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.utils.extensions.j;
import com.truecaller.voip.R.id;
import com.truecaller.voip.R.string;
import com.truecaller.voip.VoipUser;
import com.truecaller.voip.ag;
import com.truecaller.voip.util.VoipAnalyticsContext;
import com.truecaller.voip.util.VoipAnalyticsFailedCallAction;
import com.truecaller.voip.util.VoipAnalyticsInCallUiAction;
import com.truecaller.voip.util.VoipAnalyticsNotificationAction;
import com.truecaller.voip.util.at;
import com.truecaller.voip.util.o;
import com.truecaller.voip.util.z;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.a.h;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.e;

public final class c
  extends ba<b.b>
  implements com.truecaller.voip.incoming.b.b, b.a
{
  private com.truecaller.voip.incoming.b.a c;
  private boolean d;
  private boolean e;
  private final z f;
  private final at g;
  private final o h;
  
  @Inject
  public c(@Named("UI") f paramf, z paramz, at paramat, o paramo)
  {
    super(paramf);
    f = paramz;
    g = paramat;
    h = paramo;
  }
  
  private final void i()
  {
    if (c == null) {
      return;
    }
    if (e) {
      j();
    }
  }
  
  private final void j()
  {
    b.b localb = (b.b)b;
    boolean bool = true;
    if ((localb != null) && (localb.b() == true))
    {
      k();
      return;
    }
    localb = (b.b)b;
    if ((localb == null) || (localb.d())) {
      bool = false;
    }
    d = bool;
    localb = (b.b)b;
    if (localb != null)
    {
      localb.e();
      return;
    }
  }
  
  private final bn k()
  {
    return e.b(this, null, (m)new c.a(this, null), 3);
  }
  
  public final void a()
  {
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.k();
      return;
    }
  }
  
  public final void a(float paramFloat, int paramInt1, int paramInt2, int paramInt3)
  {
    if ((paramFloat < 0.95D) && (paramInt1 != R.id.incoming_call_answer_end_set)) {
      paramInt1 = 0;
    } else {
      paramInt1 = 1;
    }
    if ((paramInt2 == R.id.incoming_call_answer_start_set) && (paramInt3 == R.id.incoming_call_answer_end_set) && (paramInt1 != 0))
    {
      h.a(VoipAnalyticsInCallUiAction.ACCEPT);
      j();
    }
  }
  
  public final void a(int paramInt)
  {
    Object localObject = c;
    if (localObject != null)
    {
      localObject = ((com.truecaller.voip.incoming.b.a)localObject).a();
      if (localObject != null)
      {
        VoipUser localVoipUser = (VoipUser)j.a((h)localObject);
        if (localVoipUser != null)
        {
          if (paramInt != R.string.voip_reject_message_custom_option) {
            localObject = Integer.valueOf(paramInt);
          } else {
            localObject = null;
          }
          f.a(b, (Integer)localObject);
          localObject = c;
          if (localObject != null) {
            ((com.truecaller.voip.incoming.b.a)localObject).e();
          }
        }
      }
    }
    h.a(VoipAnalyticsInCallUiAction.REJECT_WITH_MESSAGE);
  }
  
  public final void a(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.a(paramInt1, paramInt2, paramBoolean);
      return;
    }
  }
  
  public final void a(com.truecaller.voip.incoming.b.a parama)
  {
    k.b(parama, "binderPresenter");
    c = parama;
    ag localag = parama.g();
    b.b localb = (b.b)b;
    if (localb != null) {
      localb.a(d, localag.c(), localag.d());
    }
    localb = (b.b)b;
    if (localb != null) {
      localb.c(e);
    }
    parama.a((com.truecaller.voip.incoming.b.b)this);
    j.a(this, parama.a(), (m)new c.b(this, null));
    i();
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "text");
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.c(paramString);
      return;
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      k();
      return;
    }
    b.b localb = (b.b)b;
    if (localb != null) {
      localb.f();
    }
    localb = (b.b)b;
    if (localb != null) {
      localb.g();
    }
    h.a(VoipAnalyticsContext.VOIP_IN_CALL_UI.getValue(), VoipAnalyticsFailedCallAction.NO_MIC_PERMISSION);
    if (d)
    {
      localb = (b.b)b;
      if (localb != null)
      {
        if (!localb.d())
        {
          localb = (b.b)b;
          if (localb != null) {
            localb.h();
          }
        }
      }
      else {}
    }
  }
  
  public final void a(boolean paramBoolean1, boolean paramBoolean2)
  {
    e = paramBoolean1;
    if ((paramBoolean1) && (paramBoolean2)) {
      h.a(VoipAnalyticsContext.NOTIFICATION.getValue(), VoipAnalyticsNotificationAction.ANSWERED);
    }
    i();
  }
  
  public final void b()
  {
    com.truecaller.voip.incoming.b.a locala = c;
    if (locala != null) {
      locala.e();
    }
    h.a(VoipAnalyticsInCallUiAction.REJECT);
  }
  
  public final void c()
  {
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.i();
      return;
    }
  }
  
  public final void e()
  {
    com.truecaller.voip.incoming.b.a locala = c;
    if (locala != null)
    {
      locala.f();
      return;
    }
  }
  
  public final void f()
  {
    b.b localb = (b.b)b;
    if (localb != null) {
      localb.j();
    }
    h.a(VoipAnalyticsInCallUiAction.DISMISS);
  }
  
  public final void g()
  {
    h.a(VoipAnalyticsInCallUiAction.BACK);
  }
  
  public final void h()
  {
    b.b localb = (b.b)b;
    if (localb != null) {
      localb.l();
    }
    c = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.ui.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */