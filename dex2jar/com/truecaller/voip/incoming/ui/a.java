package com.truecaller.voip.incoming.ui;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.constraint.motion.MotionLayout;
import android.support.constraint.motion.MotionLayout.d;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.content.b;
import android.support.v4.view.r;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import c.a.m;
import c.g.b.k;
import c.u;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.truecaller.common.h.aq.d;
import com.truecaller.tcpermissions.l;
import com.truecaller.utils.extensions.i;
import com.truecaller.utils.extensions.t;
import com.truecaller.voip.R.anim;
import com.truecaller.voip.R.array;
import com.truecaller.voip.R.color;
import com.truecaller.voip.R.drawable;
import com.truecaller.voip.R.id;
import com.truecaller.voip.R.layout;
import com.truecaller.voip.R.string;
import com.truecaller.voip.incoming.IncomingVoipService;
import com.truecaller.voip.j;
import com.truecaller.voip.j.a;
import java.util.HashMap;
import java.util.List;
import javax.inject.Inject;

public final class a
  extends Fragment
  implements b.b
{
  @Inject
  public b.a a;
  @Inject
  public l b;
  private MotionLayout c;
  private FloatingActionButton d;
  private FloatingActionButton e;
  private FloatingActionButton f;
  private TextView g;
  private TextView h;
  private TextView i;
  private ImageView j;
  private ImageButton k;
  private TextView l;
  private View m;
  private View n;
  private View o;
  private ImageView p;
  private ImageView q;
  private ServiceConnection r;
  private HashMap s;
  
  public final b.a a()
  {
    b.a locala = a;
    if (locala == null) {
      k.a("presenter");
    }
    return locala;
  }
  
  public final void a(int paramInt)
  {
    ImageView localImageView = q;
    if (localImageView == null) {
      k.a("truecallerLogoView");
    }
    localImageView.setImageResource(paramInt);
  }
  
  public final void a(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    Object localObject = getContext();
    if (localObject == null) {
      return;
    }
    k.a(localObject, "context ?: return");
    paramInt2 = b.c((Context)localObject, paramInt2);
    localObject = h;
    if (localObject == null) {
      k.a("statusTextView");
    }
    ((TextView)localObject).setText(paramInt1);
    localObject = h;
    if (localObject == null) {
      k.a("statusTextView");
    }
    ((TextView)localObject).setTextColor(paramInt2);
    localObject = p;
    if (localObject == null) {
      k.a("callStateRingView");
    }
    localObject = ((ImageView)localObject).getDrawable();
    if (localObject != null)
    {
      localObject = (com.truecaller.voip.a.a)localObject;
      if (paramBoolean) {
        ((com.truecaller.voip.a.a)localObject).c();
      } else {
        ((com.truecaller.voip.a.a)localObject).a(0);
      }
      localObject = p;
      if (localObject == null) {
        k.a("callStateRingView");
      }
      t.a((View)localObject, paramBoolean);
      return;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.voip.view.CallStateAvatarRingDrawable");
  }
  
  final void a(Bundle paramBundle)
  {
    b.a locala = a;
    if (locala == null) {
      k.a("presenter");
    }
    boolean bool2 = false;
    boolean bool1;
    if (paramBundle != null) {
      bool1 = paramBundle.getBoolean("com.truecaller.voip.incoming.ui.EXTRA_ACCEPT_CALL", false);
    } else {
      bool1 = false;
    }
    if (paramBundle != null) {
      bool2 = paramBundle.getBoolean("com.truecaller.voip.incoming.ui.EXTRA_VOIP_NOTIFICATION_ACTION", false);
    }
    locala.a(bool1, bool2);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "profileName");
    TextView localTextView = g;
    if (localTextView == null) {
      k.a("profileNameTextView");
    }
    localTextView.setText((CharSequence)paramString);
    paramString = g;
    if (paramString == null) {
      k.a("profileNameTextView");
    }
    paramString.setSelected(true);
  }
  
  public final void b(int paramInt)
  {
    Object localObject1 = getActivity();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "activity ?: return");
    if (Build.VERSION.SDK_INT >= 21)
    {
      localObject2 = ((f)localObject1).getWindow();
      k.a(localObject2, "context.window");
      ((Window)localObject2).setStatusBarColor(b.c((Context)localObject1, R.color.voip_status_bar_spam_color));
    }
    Object localObject2 = n;
    if (localObject2 == null) {
      k.a("headerCoverView");
    }
    ((View)localObject2).setBackgroundColor(b.c((Context)localObject1, R.color.voip_spam_color));
    localObject1 = o;
    if (localObject1 == null) {
      k.a("headerArcView");
    }
    ((View)localObject1).setBackgroundResource(R.drawable.background_voip_spam_header_view);
    localObject1 = k;
    if (localObject1 == null) {
      k.a("minimiseButton");
    }
    ((ImageButton)localObject1).setBackgroundResource(R.drawable.background_voip_minimise_spam_call);
    localObject1 = j;
    if (localObject1 == null) {
      k.a("profilePictureImageView");
    }
    ((ImageView)localObject1).setImageDrawable(null);
    localObject1 = j;
    if (localObject1 == null) {
      k.a("profilePictureImageView");
    }
    ((ImageView)localObject1).setImageResource(R.drawable.ic_avatar_voip_spam);
    localObject1 = i;
    if (localObject1 == null) {
      k.a("spamScoreTextView");
    }
    ((TextView)localObject1).setText((CharSequence)getString(R.string.voip_spam_reports_score, new Object[] { Integer.valueOf(paramInt) }));
    localObject1 = i;
    if (localObject1 == null) {
      k.a("spamScoreTextView");
    }
    t.a((View)localObject1);
  }
  
  public final void b(String paramString)
  {
    Object localObject = (CharSequence)paramString;
    int i1;
    if ((localObject != null) && (((CharSequence)localObject).length() != 0)) {
      i1 = 0;
    } else {
      i1 = 1;
    }
    if (i1 != 0) {
      return;
    }
    localObject = requireContext();
    k.a(localObject, "requireContext()");
    paramString = w.a((Context)localObject).a(paramString).a(R.drawable.ic_avatar_voip_default).a((ai)aq.d.b());
    c = true;
    paramString = paramString.b();
    localObject = j;
    if (localObject == null) {
      k.a("profilePictureImageView");
    }
    paramString.a((ImageView)localObject, null);
  }
  
  public final boolean b()
  {
    l locall = b;
    if (locall == null) {
      k.a("tcPermissionsUtil");
    }
    return locall.h();
  }
  
  public final void c()
  {
    Object localObject = getActivity();
    if (localObject == null) {
      return;
    }
    k.a(localObject, "activity ?: return");
    FloatingActionButton localFloatingActionButton = d;
    if (localFloatingActionButton == null) {
      k.a("acceptCallButton");
    }
    r.a((View)localFloatingActionButton, ColorStateList.valueOf(b.c((Context)localObject, R.color.voip_action_end_call_background_color)));
    localObject = d;
    if (localObject == null) {
      k.a("acceptCallButton");
    }
    ((FloatingActionButton)localObject).setImageResource(R.drawable.ic_button_voip_hangup);
    a(R.string.voip_status_connecting, R.color.voip_call_status_warning_color, true);
    localObject = c;
    if (localObject == null) {
      k.a("motionLayoutView");
    }
    ((MotionLayout)localObject).c();
    ((MotionLayout)localObject).setTransitionListener(null);
    ((MotionLayout)localObject).a(R.id.incoming_call_accepted_start_set, R.id.incoming_call_accepted_end_set);
    localObject = c;
    if (localObject == null) {
      k.a("motionLayoutView");
    }
    ((MotionLayout)localObject).c();
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "message");
    paramString = l;
    if (paramString == null) {
      k.a("logTextView");
    }
    t.b((View)paramString);
  }
  
  public final boolean d()
  {
    l locall = b;
    if (locall == null) {
      k.a("tcPermissionsUtil");
    }
    String[] arrayOfString = locall.e();
    int i2 = arrayOfString.length;
    int i1 = 0;
    while (i1 < i2)
    {
      locall = arrayOfString[i1];
      if (shouldShowRequestPermissionRationale(locall)) {
        break label59;
      }
      i1 += 1;
    }
    locall = null;
    label59:
    return locall != null;
  }
  
  public final void e()
  {
    l locall = b;
    if (locall == null) {
      k.a("tcPermissionsUtil");
    }
    requestPermissions(locall.e(), 1000);
  }
  
  public final void f()
  {
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    k.a(localContext, "context ?: return");
    i.a(localContext, R.string.voip_permissions_denied_explanation, null, 1, 2);
  }
  
  public final void g()
  {
    MotionLayout localMotionLayout = c;
    if (localMotionLayout == null) {
      k.a("motionLayoutView");
    }
    localMotionLayout.b();
  }
  
  public final void h()
  {
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    k.a(localContext, "context ?: return");
    startActivity(new Intent("android.settings.APPLICATION_DETAILS_SETTINGS").setData(Uri.fromParts("package", localContext.getPackageName(), null)));
  }
  
  public final void i()
  {
    List localList = m.b(new Integer[] { Integer.valueOf(R.string.voip_reject_message_first_option), Integer.valueOf(R.string.voip_reject_message_second_option), Integer.valueOf(R.string.voip_reject_message_third_option), Integer.valueOf(R.string.voip_reject_message_custom_option) });
    new AlertDialog.Builder((Context)getActivity()).setItems(R.array.voip_button_message_options, (DialogInterface.OnClickListener)new a.f(this, localList)).show();
  }
  
  public final void j()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final void k()
  {
    MotionLayout localMotionLayout = c;
    if (localMotionLayout == null) {
      k.a("motionLayoutView");
    }
    localMotionLayout.a(R.id.incoming_call_ended_start_set, R.id.incoming_call_ended_end_set);
    localMotionLayout = c;
    if (localMotionLayout == null) {
      k.a("motionLayoutView");
    }
    localMotionLayout.c();
  }
  
  public final void l()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    k.b(paramContext, "context");
    super.onAttach(paramContext);
    paramContext = j.a;
    j.a.a().a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    paramLayoutInflater = paramLayoutInflater.inflate(R.layout.fragment_voip_incoming, paramViewGroup, false);
    k.a(paramLayoutInflater, "inflater.inflate(R.layou…coming, container, false)");
    return paramLayoutInflater;
  }
  
  public final void onDestroyView()
  {
    Object localObject = a;
    if (localObject == null) {
      k.a("presenter");
    }
    ((b.a)localObject).y_();
    localObject = r;
    if (localObject != null)
    {
      Context localContext = getContext();
      if (localContext != null) {
        localContext.unbindService((ServiceConnection)localObject);
      }
    }
    super.onDestroyView();
    localObject = s;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    k.b(paramArrayOfString, "permissions");
    k.b(paramArrayOfInt, "grantResults");
    if (paramInt != 1000) {
      return;
    }
    paramArrayOfString = a;
    if (paramArrayOfString == null) {
      k.a("presenter");
    }
    paramArrayOfInt = b;
    if (paramArrayOfInt == null) {
      k.a("tcPermissionsUtil");
    }
    paramArrayOfString.a(paramArrayOfInt.h());
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    paramBundle = paramView.findViewById(R.id.motion_layout);
    k.a(paramBundle, "view.findViewById(R.id.motion_layout)");
    c = ((MotionLayout)paramBundle);
    paramBundle = paramView.findViewById(R.id.button_accept_call);
    k.a(paramBundle, "view.findViewById(R.id.button_accept_call)");
    d = ((FloatingActionButton)paramBundle);
    paramBundle = paramView.findViewById(R.id.button_reject_call);
    k.a(paramBundle, "view.findViewById(R.id.button_reject_call)");
    e = ((FloatingActionButton)paramBundle);
    paramBundle = paramView.findViewById(R.id.button_message);
    k.a(paramBundle, "view.findViewById(R.id.button_message)");
    f = ((FloatingActionButton)paramBundle);
    paramBundle = paramView.findViewById(R.id.text_profile_name);
    k.a(paramBundle, "view.findViewById(R.id.text_profile_name)");
    g = ((TextView)paramBundle);
    paramBundle = paramView.findViewById(R.id.text_status);
    k.a(paramBundle, "view.findViewById(R.id.text_status)");
    h = ((TextView)paramBundle);
    paramBundle = paramView.findViewById(R.id.text_spam_score);
    k.a(paramBundle, "view.findViewById(R.id.text_spam_score)");
    i = ((TextView)paramBundle);
    paramBundle = paramView.findViewById(R.id.image_profile_picture);
    k.a(paramBundle, "view.findViewById(R.id.image_profile_picture)");
    j = ((ImageView)paramBundle);
    paramBundle = paramView.findViewById(R.id.button_minimise);
    k.a(paramBundle, "view.findViewById(R.id.button_minimise)");
    k = ((ImageButton)paramBundle);
    paramBundle = paramView.findViewById(R.id.text_log);
    k.a(paramBundle, "view.findViewById(R.id.text_log)");
    l = ((TextView)paramBundle);
    paramBundle = paramView.findViewById(R.id.view_answer_arrows);
    k.a(paramBundle, "view.findViewById(R.id.view_answer_arrows)");
    m = paramBundle;
    paramBundle = paramView.findViewById(R.id.view_header_cover);
    k.a(paramBundle, "view.findViewById(R.id.view_header_cover)");
    n = paramBundle;
    paramBundle = paramView.findViewById(R.id.view_header);
    k.a(paramBundle, "view.findViewById(R.id.view_header)");
    o = paramBundle;
    paramBundle = paramView.findViewById(R.id.image_call_state_ring);
    k.a(paramBundle, "view.findViewById(R.id.image_call_state_ring)");
    p = ((ImageView)paramBundle);
    paramBundle = paramView.findViewById(R.id.image_logo);
    k.a(paramBundle, "view.findViewById(R.id.image_logo)");
    q = ((ImageView)paramBundle);
    paramBundle = p;
    if (paramBundle == null) {
      k.a("callStateRingView");
    }
    Context localContext = paramView.getContext();
    k.a(localContext, "view.context");
    paramBundle.setImageDrawable((Drawable)new com.truecaller.voip.a.a(localContext));
    paramBundle = a;
    if (paramBundle == null) {
      k.a("presenter");
    }
    paramBundle.a(this);
    a(getArguments());
    paramView = paramView.getContext();
    k.a(paramView, "view.context");
    paramBundle = (ServiceConnection)new a.a(this);
    r = paramBundle;
    paramView.bindService(new Intent(paramView, IncomingVoipService.class), paramBundle, 0);
    paramView = m;
    if (paramView == null) {
      k.a("answerArrowsView");
    }
    paramView.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.anim_voip_answer_arrows));
    paramView = e;
    if (paramView == null) {
      k.a("rejectCallButton");
    }
    paramView.setOnClickListener((View.OnClickListener)new a.b(this));
    paramView = f;
    if (paramView == null) {
      k.a("rejectMessageButton");
    }
    paramView.setOnClickListener((View.OnClickListener)new a.c(this));
    paramView = k;
    if (paramView == null) {
      k.a("minimiseButton");
    }
    paramView.setOnClickListener((View.OnClickListener)new a.d(this));
    paramView = c;
    if (paramView == null) {
      k.a("motionLayoutView");
    }
    paramView.setTransitionListener((MotionLayout.d)new a.e(this));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incoming.ui.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */