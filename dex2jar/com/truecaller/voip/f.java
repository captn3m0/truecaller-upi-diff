package com.truecaller.voip;

import android.content.Context;
import c.g.b.k;
import c.l;
import c.u;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.callhistory.a;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.voip.util.VoipEventType;
import com.truecaller.voip.util.ab;
import com.truecaller.voip.util.y;
import java.util.concurrent.TimeUnit;

public final class f
  implements y
{
  private final Context a;
  
  public f(Context paramContext)
  {
    a = paramContext;
  }
  
  public final void a(ab paramab)
  {
    k.b(paramab, "event");
    HistoryEvent localHistoryEvent = new HistoryEvent(a);
    k.b(paramab, "receiver$0");
    VoipEventType localVoipEventType = b;
    int k = g.a[localVoipEventType.ordinal()];
    int i = 3;
    int j = 1;
    switch (k)
    {
    default: 
      throw new l();
    case 2: 
      i = 2;
      break;
    case 1: 
      i = 1;
    }
    localHistoryEvent.a(i);
    k.b(paramab, "receiver$0");
    localVoipEventType = b;
    i = j;
    if (g.b[localVoipEventType.ordinal()] != 1) {
      i = 0;
    }
    localHistoryEvent.b(i);
    localHistoryEvent.b("com.truecaller.voip.manager.VOIP");
    localHistoryEvent.b(TimeUnit.MILLISECONDS.toSeconds(c));
    paramab = d;
    if (paramab != null) {
      localHistoryEvent.a(((Number)paramab).longValue());
    }
    paramab = a.getApplicationContext();
    if (paramab != null)
    {
      paramab = ((bk)paramab).a();
      k.a(paramab, "(context.applicationCont…GraphHolder).objectsGraph");
      paramab = paramab.ad();
      k.a(paramab, "graph.callHistoryManager()");
      ((a)paramab.a()).a(localHistoryEvent);
      return;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.GraphHolder");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */