package com.truecaller.voip;

import android.content.Context;
import c.d.f;
import com.truecaller.tcpermissions.l;
import com.truecaller.voip.db.a;
import com.truecaller.voip.manager.rtm.i;
import com.truecaller.voip.util.ac;
import com.truecaller.voip.util.an;
import com.truecaller.voip.util.aq;
import com.truecaller.voip.util.y;
import dagger.a.d;
import javax.inject.Provider;

public final class q
  implements d<o>
{
  private final Provider<f> a;
  private final Provider<f> b;
  private final Provider<Context> c;
  private final Provider<l> d;
  private final Provider<com.truecaller.tcpermissions.o> e;
  private final Provider<k> f;
  private final Provider<com.truecaller.voip.manager.h> g;
  private final Provider<com.truecaller.voip.manager.rtm.h> h;
  private final Provider<i> i;
  private final Provider<y> j;
  private final Provider<a> k;
  private final Provider<aq> l;
  private final Provider<com.truecaller.voip.util.o> m;
  private final Provider<ac> n;
  private final Provider<an> o;
  
  private q(Provider<f> paramProvider1, Provider<f> paramProvider2, Provider<Context> paramProvider, Provider<l> paramProvider3, Provider<com.truecaller.tcpermissions.o> paramProvider4, Provider<k> paramProvider5, Provider<com.truecaller.voip.manager.h> paramProvider6, Provider<com.truecaller.voip.manager.rtm.h> paramProvider7, Provider<i> paramProvider8, Provider<y> paramProvider9, Provider<a> paramProvider10, Provider<aq> paramProvider11, Provider<com.truecaller.voip.util.o> paramProvider12, Provider<ac> paramProvider13, Provider<an> paramProvider14)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
    k = paramProvider10;
    l = paramProvider11;
    m = paramProvider12;
    n = paramProvider13;
    o = paramProvider14;
  }
  
  public static q a(Provider<f> paramProvider1, Provider<f> paramProvider2, Provider<Context> paramProvider, Provider<l> paramProvider3, Provider<com.truecaller.tcpermissions.o> paramProvider4, Provider<k> paramProvider5, Provider<com.truecaller.voip.manager.h> paramProvider6, Provider<com.truecaller.voip.manager.rtm.h> paramProvider7, Provider<i> paramProvider8, Provider<y> paramProvider9, Provider<a> paramProvider10, Provider<aq> paramProvider11, Provider<com.truecaller.voip.util.o> paramProvider12, Provider<ac> paramProvider13, Provider<an> paramProvider14)
  {
    return new q(paramProvider1, paramProvider2, paramProvider, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10, paramProvider11, paramProvider12, paramProvider13, paramProvider14);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */