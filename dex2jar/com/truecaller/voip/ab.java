package com.truecaller.voip;

import android.content.Context;
import com.truecaller.voip.db.a;
import dagger.a.d;
import javax.inject.Provider;

public final class ab
  implements d<a>
{
  private final Provider<Context> a;
  
  private ab(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static ab a(Provider<Context> paramProvider)
  {
    return new ab(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.ab
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */