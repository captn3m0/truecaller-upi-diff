package com.truecaller.voip.callconnection;

import android.content.Context;
import c.d.f;
import javax.inject.Provider;

public final class e
  implements dagger.a.d<d>
{
  private final Provider<f> a;
  private final Provider<Context> b;
  
  private e(Provider<f> paramProvider, Provider<Context> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static e a(Provider<f> paramProvider, Provider<Context> paramProvider1)
  {
    return new e(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.callconnection.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */