package com.truecaller.voip.callconnection;

import android.content.Context;
import c.d.f;
import c.g.b.k;
import javax.inject.Inject;
import javax.inject.Named;

public final class d
  implements c
{
  private final f a;
  private final Context b;
  
  @Inject
  public d(@Named("UI") f paramf, Context paramContext)
  {
    a = paramf;
    b = paramContext;
  }
  
  public final a a()
  {
    a locala = new a(a, b, "", false);
    locala.a(4);
    return locala;
  }
  
  public final a a(String paramString, boolean paramBoolean)
  {
    k.b(paramString, "number");
    return new a(a, b, paramString, paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.callconnection.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */