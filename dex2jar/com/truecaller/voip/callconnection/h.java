package com.truecaller.voip.callconnection;

import com.truecaller.common.h.u;
import com.truecaller.voip.k;
import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d<g>
{
  private final Provider<k> a;
  private final Provider<c> b;
  private final Provider<u> c;
  
  private h(Provider<k> paramProvider, Provider<c> paramProvider1, Provider<u> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static h a(Provider<k> paramProvider, Provider<c> paramProvider1, Provider<u> paramProvider2)
  {
    return new h(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.callconnection.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */