package com.truecaller.voip.callconnection;

import android.content.Context;
import android.os.Build.VERSION;
import android.telecom.CallAudioState;
import c.x;
import com.truecaller.common.h.u;
import com.truecaller.voip.incall.VoipService;
import com.truecaller.voip.incall.VoipService.a;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.inject.Inject;

public final class g
  implements f
{
  final Map<String, a> a;
  private int b;
  private String c;
  private final com.truecaller.voip.k d;
  private final c e;
  private final u f;
  
  @Inject
  public g(com.truecaller.voip.k paramk, c paramc, u paramu)
  {
    d = paramk;
    e = paramc;
    f = paramu;
    b = Build.VERSION.SDK_INT;
    a = ((Map)new LinkedHashMap());
  }
  
  private final a a(String paramString, boolean paramBoolean)
  {
    paramString = f.b(paramString);
    if (paramString == null) {
      return null;
    }
    return e.a(paramString, paramBoolean);
  }
  
  private final void a(a parama)
  {
    StringBuilder localStringBuilder = new StringBuilder("Registering connection with number: ");
    localStringBuilder.append(c);
    localStringBuilder.toString();
    a.put(c, parama);
    parama.a((c.g.a.a)new g.a(this, parama));
  }
  
  private final boolean c()
  {
    return (b < 26) || (!d.a());
  }
  
  public final void a(String paramString, c.g.a.b<? super CallAudioState, x> paramb)
  {
    c.g.b.k.b(paramString, "number");
    if (c()) {
      return;
    }
    paramString = (a)a.get(paramString);
    if (paramString == null) {
      return;
    }
    a = paramb;
    paramb = paramString.getCallAudioState();
    if (paramb != null)
    {
      paramString = a;
      if (paramString != null)
      {
        paramString.invoke(paramb);
        return;
      }
      return;
    }
  }
  
  public final boolean a()
  {
    if (c()) {
      return false;
    }
    if (!(a.isEmpty() ^ true)) {
      return c != null;
    }
    return true;
  }
  
  public final boolean a(String paramString)
  {
    c.g.b.k.b(paramString, "number");
    if (c()) {
      return false;
    }
    paramString = f.b(paramString);
    if (paramString == null) {
      return false;
    }
    return (a.get(paramString) != null) || (c.g.b.k.a(c, paramString));
  }
  
  public final a b(String paramString)
  {
    c.g.b.k.b(paramString, "number");
    if (c()) {
      return e.a();
    }
    a locala = a(paramString, true);
    if (locala == null) {
      return e.a();
    }
    Context localContext = b;
    VoipService.a locala1 = VoipService.e;
    android.support.v4.content.b.a(localContext, VoipService.a.a(b, c));
    if (c.g.b.k.a(paramString, c)) {
      c = null;
    }
    if ((a.isEmpty() ^ true)) {
      return e.a();
    }
    locala.a();
    a(locala);
    return locala;
  }
  
  public final void b()
  {
    if (c()) {
      return;
    }
    a.clear();
    c = null;
  }
  
  public final a c(String paramString)
  {
    c.g.b.k.b(paramString, "number");
    if (c()) {
      return e.a();
    }
    paramString = a(paramString, false);
    if (paramString == null) {
      return e.a();
    }
    paramString.a();
    a(paramString);
    return paramString;
  }
  
  public final void d(String paramString)
  {
    c.g.b.k.b(paramString, "number");
    if (c()) {
      return;
    }
    c = paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.callconnection.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */