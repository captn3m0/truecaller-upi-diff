package com.truecaller.voip.callconnection;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.Uri;
import android.support.v4.content.b;
import android.telecom.Connection;
import android.telecom.ConnectionRequest;
import android.telecom.ConnectionService;
import android.telecom.PhoneAccountHandle;
import c.g.b.k;
import com.truecaller.voip.incall.VoipService;
import com.truecaller.voip.incall.VoipService.a;
import com.truecaller.voip.j;
import com.truecaller.voip.j.a;
import javax.inject.Inject;

@TargetApi(26)
public final class VoipCallConnectionService
  extends ConnectionService
{
  @Inject
  public f a;
  
  public final void onCreate()
  {
    super.onCreate();
    j.a locala = j.a;
    j.a.a().a(this);
  }
  
  public final Connection onCreateIncomingConnection(PhoneAccountHandle paramPhoneAccountHandle, ConnectionRequest paramConnectionRequest)
  {
    StringBuilder localStringBuilder = new StringBuilder("New incoming connection. Request:");
    Object localObject = null;
    if (paramConnectionRequest != null) {
      paramPhoneAccountHandle = paramConnectionRequest.getAddress();
    } else {
      paramPhoneAccountHandle = null;
    }
    localStringBuilder.append(paramPhoneAccountHandle);
    localStringBuilder.append(" Extras:");
    paramPhoneAccountHandle = (PhoneAccountHandle)localObject;
    if (paramConnectionRequest != null) {
      paramPhoneAccountHandle = paramConnectionRequest.getExtras();
    }
    localStringBuilder.append(paramPhoneAccountHandle);
    localStringBuilder.toString();
    if (paramConnectionRequest != null)
    {
      paramPhoneAccountHandle = paramConnectionRequest.getAddress();
      if (paramPhoneAccountHandle != null)
      {
        paramPhoneAccountHandle = paramPhoneAccountHandle.getSchemeSpecificPart();
        if (paramPhoneAccountHandle != null)
        {
          paramConnectionRequest = a;
          if (paramConnectionRequest == null) {
            k.a("connectionManager");
          }
          return (Connection)paramConnectionRequest.c(paramPhoneAccountHandle);
        }
      }
    }
    paramPhoneAccountHandle = Connection.createCanceledConnection();
    k.a(paramPhoneAccountHandle, "Connection.createCanceledConnection()");
    return paramPhoneAccountHandle;
  }
  
  public final void onCreateIncomingConnectionFailed(PhoneAccountHandle paramPhoneAccountHandle, ConnectionRequest paramConnectionRequest)
  {
    StringBuilder localStringBuilder = new StringBuilder("Incoming connection is failed. Request: ");
    Object localObject2 = null;
    if (paramConnectionRequest != null) {
      localObject1 = paramConnectionRequest.getAddress();
    } else {
      localObject1 = null;
    }
    localStringBuilder.append(localObject1);
    localStringBuilder.append(" Extras:");
    Object localObject1 = localObject2;
    if (paramConnectionRequest != null) {
      localObject1 = paramConnectionRequest.getExtras();
    }
    localStringBuilder.append(localObject1);
    localStringBuilder.toString();
    super.onCreateIncomingConnectionFailed(paramPhoneAccountHandle, paramConnectionRequest);
  }
  
  public final Connection onCreateOutgoingConnection(PhoneAccountHandle paramPhoneAccountHandle, ConnectionRequest paramConnectionRequest)
  {
    StringBuilder localStringBuilder = new StringBuilder("New outgoing connection. Request:");
    Object localObject = null;
    if (paramConnectionRequest != null) {
      paramPhoneAccountHandle = paramConnectionRequest.getAddress();
    } else {
      paramPhoneAccountHandle = null;
    }
    localStringBuilder.append(paramPhoneAccountHandle);
    localStringBuilder.append(" Extras:");
    paramPhoneAccountHandle = (PhoneAccountHandle)localObject;
    if (paramConnectionRequest != null) {
      paramPhoneAccountHandle = paramConnectionRequest.getExtras();
    }
    localStringBuilder.append(paramPhoneAccountHandle);
    localStringBuilder.toString();
    if (paramConnectionRequest != null)
    {
      paramPhoneAccountHandle = paramConnectionRequest.getAddress();
      if (paramPhoneAccountHandle != null)
      {
        paramPhoneAccountHandle = paramPhoneAccountHandle.getSchemeSpecificPart();
        if (paramPhoneAccountHandle != null)
        {
          paramConnectionRequest = a;
          if (paramConnectionRequest == null) {
            k.a("connectionManager");
          }
          return (Connection)paramConnectionRequest.b(paramPhoneAccountHandle);
        }
      }
    }
    paramPhoneAccountHandle = Connection.createCanceledConnection();
    k.a(paramPhoneAccountHandle, "Connection.createCanceledConnection()");
    return paramPhoneAccountHandle;
  }
  
  public final void onCreateOutgoingConnectionFailed(PhoneAccountHandle paramPhoneAccountHandle, ConnectionRequest paramConnectionRequest)
  {
    StringBuilder localStringBuilder = new StringBuilder("Outgoing connection is failed. Request: ");
    Object localObject2 = null;
    if (paramConnectionRequest != null) {
      localObject1 = paramConnectionRequest.getAddress();
    } else {
      localObject1 = null;
    }
    localStringBuilder.append(localObject1);
    localStringBuilder.append(" Extras:");
    Object localObject1 = localObject2;
    if (paramConnectionRequest != null) {
      localObject1 = paramConnectionRequest.getExtras();
    }
    localStringBuilder.append(localObject1);
    localStringBuilder.toString();
    super.onCreateOutgoingConnectionFailed(paramPhoneAccountHandle, paramConnectionRequest);
    if (paramConnectionRequest != null)
    {
      paramPhoneAccountHandle = paramConnectionRequest.getAddress();
      if (paramPhoneAccountHandle != null)
      {
        paramPhoneAccountHandle = paramPhoneAccountHandle.getSchemeSpecificPart();
        if (paramPhoneAccountHandle == null) {
          return;
        }
        paramConnectionRequest = (Context)this;
        localObject1 = VoipService.e;
        b.a(paramConnectionRequest, VoipService.a.a(paramConnectionRequest, paramPhoneAccountHandle));
        return;
      }
    }
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    f localf = a;
    if (localf == null) {
      k.a("connectionManager");
    }
    localf.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.callconnection.VoipCallConnectionService
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */