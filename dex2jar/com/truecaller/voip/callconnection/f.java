package com.truecaller.voip.callconnection;

import android.telecom.CallAudioState;
import c.g.a.b;
import c.x;

public abstract interface f
{
  public abstract void a(String paramString, b<? super CallAudioState, x> paramb);
  
  public abstract boolean a();
  
  public abstract boolean a(String paramString);
  
  public abstract a b(String paramString);
  
  public abstract void b();
  
  public abstract a c(String paramString);
  
  public abstract void d(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.voip.callconnection.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */