package com.truecaller.voip;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import c.a.m;
import c.g.b.k;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.messaging.conversation.ConversationActivity;
import com.truecaller.messaging.data.o;
import com.truecaller.messaging.data.types.Draft;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.g.d;
import com.truecaller.voip.util.z;
import java.util.Collection;
import javax.inject.Inject;

public final class h
  implements z
{
  final Context a;
  
  @Inject
  public h(Context paramContext)
  {
    a = paramContext;
  }
  
  static void a(Context paramContext, Participant[] paramArrayOfParticipant, String paramString)
  {
    Intent localIntent = new Intent(paramContext, ConversationActivity.class);
    if (paramArrayOfParticipant != null) {
      localIntent.putExtra("participants", (Parcelable[])paramArrayOfParticipant);
    }
    if (paramString != null)
    {
      paramArrayOfParticipant = new Intent();
      paramArrayOfParticipant.putExtra("android.intent.extra.TEXT", paramString);
      localIntent.putExtra("send_intent", (Parcelable)paramArrayOfParticipant);
    }
    localIntent.setFlags(268435456);
    paramContext.startActivity(localIntent);
  }
  
  private final com.truecaller.common.h.u d()
  {
    com.truecaller.common.h.u localu = a().V();
    k.a(localu, "graph.phoneNumberHelper()");
    return localu;
  }
  
  final bp a()
  {
    Object localObject = a.getApplicationContext();
    if (localObject != null)
    {
      localObject = ((bk)localObject).a();
      k.a(localObject, "(context.applicationCont…GraphHolder).objectsGraph");
      return (bp)localObject;
    }
    throw new c.u("null cannot be cast to non-null type com.truecaller.GraphHolder");
  }
  
  public final void a(String paramString, final Integer paramInteger)
  {
    k.b(paramString, "number");
    paramString = (Collection)m.a(Participant.a(paramString, d(), d().a()));
    if (paramString != null)
    {
      paramString = paramString.toArray(new Participant[0]);
      if (paramString != null)
      {
        paramString = (Participant[])paramString;
        if (paramInteger == null)
        {
          a(a, paramString, null);
          return;
        }
        f localf = a().q();
        k.a(localf, "graph.fetchMessageStorage()");
        k.a(((o)localf.a()).a(paramString, 1).a((ac)new a(this, paramInteger)), "fetchMessageStorage.tell…g(message))\n            }");
        return;
      }
      throw new c.u("null cannot be cast to non-null type kotlin.Array<T>");
    }
    throw new c.u("null cannot be cast to non-null type java.util.Collection<T>");
  }
  
  final d b()
  {
    d locald = a().bW();
    k.a(locald, "graph.draftSender()");
    return locald;
  }
  
  final com.truecaller.multisim.h c()
  {
    com.truecaller.multisim.h localh = a().U();
    k.a(localh, "graph.multiSimManager()");
    return localh;
  }
  
  static final class a<R>
    implements ac<Draft>
  {
    a(h paramh, Integer paramInteger) {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */