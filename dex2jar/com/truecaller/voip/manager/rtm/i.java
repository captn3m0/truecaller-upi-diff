package com.truecaller.voip.manager.rtm;

import c.d.c;
import c.g.a.a;
import c.g.a.m;
import c.x;
import com.truecaller.voip.VoipUser;
import kotlinx.coroutines.a.h;
import kotlinx.coroutines.ag;

public abstract interface i
{
  public abstract Object a(VoipUser paramVoipUser, RtmMsg paramRtmMsg, boolean paramBoolean, a<x> parama, c<? super Integer> paramc);
  
  public abstract Object a(String paramString, RtmMsg paramRtmMsg, c<? super Integer> paramc);
  
  public abstract h<RtmMsg> a();
  
  public abstract void a(ag paramag, m<? super RtmMsg, ? super c<? super x>, ? extends Object> paramm);
  
  public abstract void b(ag paramag, m<? super Integer, ? super c<? super x>, ? extends Object> paramm);
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.rtm.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */