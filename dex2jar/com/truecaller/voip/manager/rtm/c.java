package com.truecaller.voip.manager.rtm;

import android.content.Context;
import c.d.a.b;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.log.AssertionUtil;
import com.truecaller.voip.R.string;
import com.truecaller.voip.VoipUser;
import com.truecaller.voip.util.o;
import io.agora.rtm.IStateListener;
import io.agora.rtm.RtmClient;
import io.agora.rtm.RtmClientListener;
import io.agora.rtm.RtmMessage;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.a.h;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

public final class c
  implements i, ag
{
  final h<RtmMsg> a;
  final h<Integer> b;
  public final RtmClient c;
  final com.truecaller.featuretoggles.e d;
  final Context e;
  final com.truecaller.voip.api.a f;
  final o g;
  private final c.d.f h;
  private final c.d.f i;
  private final com.google.gson.f j;
  
  @Inject
  public c(@Named("UI") c.d.f paramf1, @Named("IO") c.d.f paramf2, @Named("features_registry") com.truecaller.featuretoggles.e parame, Context paramContext, com.google.gson.f paramf, com.truecaller.voip.api.a parama, o paramo)
  {
    h = paramf1;
    i = paramf2;
    d = parame;
    e = paramContext;
    j = paramf;
    f = parama;
    g = paramo;
    a = kotlinx.coroutines.a.i.a(10);
    b = kotlinx.coroutines.a.i.a(10);
    paramf1 = e;
    paramf1 = RtmClient.createInstance(paramf1, paramf1.getString(R.string.voip_agora_app_id), (RtmClientListener)new c.c(this));
    c.g.b.k.a(paramf1, "RtmClient.createInstance…        }\n        }\n    )");
    c = paramf1;
  }
  
  public final c.d.f V_()
  {
    return h;
  }
  
  final RtmMsg a(String paramString1, String paramString2)
  {
    try
    {
      paramString2 = (RtmMsg)j.a(paramString2, RtmMsg.class);
      paramString2.getAction().name();
      paramString2.getChannelId().length();
      paramString2.setSenderId(paramString1);
      return paramString2;
    }
    catch (Exception paramString2)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramString2);
      kotlinx.coroutines.e.b(this, i, (m)new c.a(this, paramString1, null), 2);
    }
    return null;
  }
  
  public final Object a(VoipUser paramVoipUser, RtmMsg paramRtmMsg, boolean paramBoolean, c.g.a.a<x> parama, c.d.c<? super Integer> paramc)
  {
    Object localObject1;
    if ((paramc instanceof c.e))
    {
      localObject1 = (c.e)paramc;
      if ((b & 0x80000000) != 0)
      {
        b += Integer.MIN_VALUE;
        paramc = (c.d.c<? super Integer>)localObject1;
        break label58;
      }
    }
    paramc = new c.e(this, paramc);
    label58:
    Object localObject2 = a;
    c.d.a.a locala = c.d.a.a.a;
    switch (b)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 2: 
      if (!(localObject2 instanceof o.b)) {
        return localObject2;
      }
      throw a;
    case 1: 
      parama = (c.g.a.a)g;
      paramBoolean = h;
      paramRtmMsg = (RtmMsg)f;
      paramVoipUser = (VoipUser)e;
      localObject1 = (c)d;
      if ((localObject2 instanceof o.b)) {
        throw a;
      }
      break;
    case 0: 
      if ((localObject2 instanceof o.b)) {
        break label382;
      }
      localObject1 = a;
      d = this;
      e = paramVoipUser;
      f = paramRtmMsg;
      h = paramBoolean;
      g = parama;
      b = 1;
      localObject2 = a((String)localObject1, paramRtmMsg, paramc);
      if (localObject2 == locala) {
        return locala;
      }
      localObject1 = this;
    }
    int k = ((Number)localObject2).intValue();
    if ((paramBoolean) && (k != 3))
    {
      d = localObject1;
      e = paramVoipUser;
      f = paramRtmMsg;
      h = paramBoolean;
      g = parama;
      i = k;
      b = 2;
      paramVoipUser = g.a(i, (m)new c.b((c)localObject1, paramVoipUser, paramRtmMsg, parama, null), paramc);
      if (paramVoipUser == locala) {
        return locala;
      }
      return paramVoipUser;
    }
    return Integer.valueOf(k);
    label382:
    throw a;
  }
  
  public final Object a(String paramString, RtmMsg paramRtmMsg, c.d.c<? super Integer> paramc)
  {
    kotlinx.coroutines.k localk = new kotlinx.coroutines.k(b.a(paramc), 1);
    kotlinx.coroutines.j localj = (kotlinx.coroutines.j)localk;
    RtmMessage localRtmMessage = RtmMessage.createMessage();
    localRtmMessage.setText(j.b(paramRtmMsg));
    paramRtmMsg = new StringBuilder("Sending RTM message to user ");
    paramRtmMsg.append(paramString);
    paramRtmMsg.append(". Message is:\n");
    c.g.b.k.a(localRtmMessage, "rtmMessage");
    paramRtmMsg.append(localRtmMessage.getText());
    paramRtmMsg.toString();
    c.sendMessageToPeer(paramString, localRtmMessage, (IStateListener)new c.d(localj));
    paramString = localk.h();
    if (paramString == c.d.a.a.a) {
      c.g.b.k.b(paramc, "frame");
    }
    return paramString;
  }
  
  public final h<RtmMsg> a()
  {
    return a;
  }
  
  public final void a(ag paramag, m<? super RtmMsg, ? super c.d.c<? super x>, ? extends Object> paramm)
  {
    c.g.b.k.b(paramag, "scope");
    c.g.b.k.b(paramm, "block");
    com.truecaller.utils.extensions.j.a(paramag, a, paramm);
  }
  
  public final void b(ag paramag, m<? super Integer, ? super c.d.c<? super x>, ? extends Object> paramm)
  {
    c.g.b.k.b(paramag, "scope");
    c.g.b.k.b(paramm, "block");
    com.truecaller.utils.extensions.j.a(paramag, b, paramm);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.rtm.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */