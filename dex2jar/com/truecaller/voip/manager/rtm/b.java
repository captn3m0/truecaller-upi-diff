package com.truecaller.voip.manager.rtm;

import c.d.f;
import com.truecaller.voip.util.ac;
import com.truecaller.voip.util.aq;
import com.truecaller.voip.util.o;
import dagger.a.d;
import io.agora.rtm.RtmClient;
import javax.inject.Provider;

public final class b
  implements d<a>
{
  private final Provider<f> a;
  private final Provider<RtmClient> b;
  private final Provider<ac> c;
  private final Provider<aq> d;
  private final Provider<i> e;
  private final Provider<o> f;
  
  private b(Provider<f> paramProvider, Provider<RtmClient> paramProvider1, Provider<ac> paramProvider2, Provider<aq> paramProvider3, Provider<i> paramProvider4, Provider<o> paramProvider5)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
  }
  
  public static b a(Provider<f> paramProvider, Provider<RtmClient> paramProvider1, Provider<ac> paramProvider2, Provider<aq> paramProvider3, Provider<i> paramProvider4, Provider<o> paramProvider5)
  {
    return new b(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.rtm.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */