package com.truecaller.voip.manager.rtm;

import android.content.Context;
import com.truecaller.voip.api.a;
import com.truecaller.voip.util.o;
import dagger.a.d;
import javax.inject.Provider;

public final class e
  implements d<c>
{
  private final Provider<c.d.f> a;
  private final Provider<c.d.f> b;
  private final Provider<com.truecaller.featuretoggles.e> c;
  private final Provider<Context> d;
  private final Provider<com.google.gson.f> e;
  private final Provider<a> f;
  private final Provider<o> g;
  
  private e(Provider<c.d.f> paramProvider1, Provider<c.d.f> paramProvider2, Provider<com.truecaller.featuretoggles.e> paramProvider, Provider<Context> paramProvider3, Provider<com.google.gson.f> paramProvider4, Provider<a> paramProvider5, Provider<o> paramProvider6)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
  }
  
  public static e a(Provider<c.d.f> paramProvider1, Provider<c.d.f> paramProvider2, Provider<com.truecaller.featuretoggles.e> paramProvider, Provider<Context> paramProvider3, Provider<com.google.gson.f> paramProvider4, Provider<a> paramProvider5, Provider<o> paramProvider6)
  {
    return new e(paramProvider1, paramProvider2, paramProvider, paramProvider3, paramProvider4, paramProvider5, paramProvider6);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.rtm.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */