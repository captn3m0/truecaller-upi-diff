package com.truecaller.voip.manager.rtm;

import c.d.c;
import c.g.a.m;
import c.o.b;
import com.truecaller.voip.util.ac;
import com.truecaller.voip.util.aq;
import com.truecaller.voip.util.o;
import io.agora.rtm.RtmClient;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.cn;
import kotlinx.coroutines.e;
import kotlinx.coroutines.e.b;
import kotlinx.coroutines.e.d;

public final class a
  implements h, ag
{
  boolean a;
  final b b;
  String c;
  final RtmClient d;
  final ac e;
  final aq f;
  final o g;
  private final c.d.f h;
  private final i i;
  
  @Inject
  public a(@Named("IO") c.d.f paramf, RtmClient paramRtmClient, ac paramac, aq paramaq, i parami, o paramo)
  {
    h = paramf;
    d = paramRtmClient;
    e = paramac;
    f = paramaq;
    i = parami;
    g = paramo;
    b = d.a();
    i.b((ag)this, (m)new a.b(this, null));
  }
  
  public final c.d.f V_()
  {
    return h;
  }
  
  public final Object a(long paramLong, c<? super g> paramc)
  {
    if ((paramc instanceof a.c))
    {
      localObject = (a.c)paramc;
      if ((b & 0x80000000) != 0)
      {
        b += Integer.MIN_VALUE;
        paramc = (c<? super g>)localObject;
        break label53;
      }
    }
    paramc = new a.c(this, paramc);
    label53:
    Object localObject = a;
    c.d.a.a locala = c.d.a.a.a;
    switch (b)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 1: 
      if (!(localObject instanceof o.b)) {
        paramc = (c<? super g>)localObject;
      } else {
        throw a;
      }
      break;
    case 0: 
      if ((localObject instanceof o.b)) {
        break label214;
      }
      localObject = (m)new a.d(this, null);
      d = this;
      e = paramLong;
      b = 1;
      localObject = cn.a(paramLong, (m)localObject, paramc);
      paramc = (c<? super g>)localObject;
      if (localObject == locala) {
        return locala;
      }
      break;
    }
    localObject = (g)paramc;
    paramc = (c<? super g>)localObject;
    if (localObject == null) {
      paramc = (g)new f(FailedRtmLoginReason.TIMED_OUT);
    }
    return paramc;
    label214:
    throw a;
  }
  
  public final String a()
  {
    return c;
  }
  
  public final Object b(long paramLong, c<? super Boolean> paramc)
  {
    return cn.a(paramLong, (m)new a.h(this, null), paramc);
  }
  
  public final void b()
  {
    e.b(this, null, (m)new a.i(this, null), 3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.rtm.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */