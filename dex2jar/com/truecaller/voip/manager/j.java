package com.truecaller.voip.manager;

import c.d.f;
import com.truecaller.voip.k;
import com.truecaller.voip.util.ac;
import com.truecaller.voip.util.ah;
import com.truecaller.voip.util.l;
import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d<i>
{
  private final Provider<f> a;
  private final Provider<k> b;
  private final Provider<ah> c;
  private final Provider<l> d;
  private final Provider<ac> e;
  
  private j(Provider<f> paramProvider, Provider<k> paramProvider1, Provider<ah> paramProvider2, Provider<l> paramProvider3, Provider<ac> paramProvider4)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static j a(Provider<f> paramProvider, Provider<k> paramProvider1, Provider<ah> paramProvider2, Provider<l> paramProvider3, Provider<ac> paramProvider4)
  {
    return new j(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */