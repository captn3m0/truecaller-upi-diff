package com.truecaller.voip.manager;

import c.d.f;
import c.g.a.b;
import c.g.a.m;
import com.truecaller.utils.extensions.c;
import com.truecaller.voip.k;
import com.truecaller.voip.util.ac;
import com.truecaller.voip.util.ah;
import com.truecaller.voip.util.l;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.e;

public final class i
  implements h, ag
{
  volatile bn a;
  final k b;
  final ah c;
  final l d;
  final ac e;
  private final f f;
  
  @Inject
  public i(@Named("IO") f paramf, k paramk, ah paramah, l paraml, ac paramac)
  {
    f = paramf;
    b = paramk;
    c = paramah;
    d = paraml;
    e = paramac;
  }
  
  public final f V_()
  {
    return f;
  }
  
  public final void a()
  {
    for (;;)
    {
      try
      {
        if (a != null)
        {
          localObject1 = a;
          if (localObject1 == null) {
            break label101;
          }
          localObject1 = Boolean.valueOf(((bn)localObject1).av_());
          if (c.a((Boolean)localObject1)) {
            return;
          }
        }
        a = e.b(this, null, (m)new i.a(this, null), 3);
        Object localObject1 = a;
        if (localObject1 != null)
        {
          ((bn)localObject1).a_((b)new i.b(this));
          return;
        }
        return;
      }
      finally {}
      label101:
      Object localObject3 = null;
    }
  }
  
  public final void b()
  {
    c.d("reportedVoipState");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */