package com.truecaller.voip.manager;

import android.content.Context;
import c.g.a.m;
import c.g.b.k;
import c.o.b;
import c.x;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.extensions.j;
import com.truecaller.voip.R.string;
import com.truecaller.voip.api.RtcTokenDto;
import com.truecaller.voip.util.aq;
import io.agora.rtc.Constants.AudioProfile;
import io.agora.rtc.Constants.AudioScenario;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.a.h;
import kotlinx.coroutines.a.i;
import kotlinx.coroutines.a.u;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

public final class a
  implements g, ag
{
  final h<VoipMsg> a;
  final RtcEngine b;
  e c;
  final aq d;
  private final a.d e;
  private final c.d.f f;
  
  @Inject
  public a(Context paramContext, @Named("IO") c.d.f paramf, aq paramaq)
  {
    f = paramf;
    d = paramaq;
    a = i.a(10);
    e = new a.d(this);
    try
    {
      paramContext = RtcEngine.create(paramContext, paramContext.getString(R.string.voip_agora_app_id), (IRtcEngineEventHandler)e);
      paramContext.setDefaultAudioRoutetoSpeakerphone(false);
      paramContext.setChannelProfile(0);
      paramContext.setAudioProfile(Constants.AudioProfile.getValue(Constants.AudioProfile.SPEECH_STANDARD), Constants.AudioScenario.getValue(Constants.AudioScenario.DEFAULT));
    }
    catch (Exception paramContext)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramContext);
      paramContext = null;
    }
    b = paramContext;
  }
  
  public final c.d.f V_()
  {
    return f;
  }
  
  public final e a()
  {
    return c;
  }
  
  public final Object a(String paramString, c.d.c<? super c> paramc)
  {
    if ((paramc instanceof a.a))
    {
      localObject3 = (a.a)paramc;
      if ((b & 0x80000000) != 0)
      {
        b += Integer.MIN_VALUE;
        break label51;
      }
    }
    Object localObject3 = new a.a(this, paramc);
    label51:
    paramc = a;
    c.d.a.a locala = c.d.a.a.a;
    Object localObject2;
    switch (b)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 2: 
      paramString = (u)g;
      localObject4 = (String)e;
      localObject3 = (a)d;
      try
      {
        if (!(paramc instanceof o.b))
        {
          String str = paramString;
          paramString = (String)localObject4;
          localObject4 = paramc;
          break label483;
        }
        throw a;
      }
      finally
      {
        paramc = paramString;
        paramString = (String)localObject1;
        break label557;
      }
    case 1: 
      paramString = (String)e;
      localObject2 = (a)d;
      if ((paramc instanceof o.b)) {
        throw a;
      }
      break;
    case 0: 
      if ((paramc instanceof o.b)) {
        break label565;
      }
      "Joining channel ".concat(String.valueOf(paramString));
      paramc = d;
      d = this;
      e = paramString;
      b = 1;
      paramc = paramc.a(paramString, (c.d.c)localObject3);
      if (paramc == locala) {
        return locala;
      }
      localObject2 = this;
    }
    RtcTokenDto localRtcTokenDto = (RtcTokenDto)paramc;
    if (localRtcTokenDto == null)
    {
      "Cannot fetch rtc token for channel:".concat(String.valueOf(paramString));
      return new d(FailedChannelJoinReason.GET_TOKEN_FAILED);
    }
    Object localObject4 = a.a();
    paramc = b;
    Object localObject5;
    if (paramc != null) {
      localObject5 = Integer.valueOf(paramc.joinChannel(localRtcTokenDto.getToken(), paramString, null, localRtcTokenDto.getUid()));
    } else {
      localObject5 = null;
    }
    if ((localObject5 == null) || (((Integer)localObject5).intValue() != 0))
    {
      paramc = new StringBuilder("Cannot join to channel ");
      paramc.append(paramString);
      paramc.append(", returning false. Return code ");
      paramc.append(localObject5);
      paramc.toString();
      return new d(FailedChannelJoinReason.RTC_JOIN_FAILED);
    }
    paramc = (c.d.c<? super c>)localObject4;
    for (;;)
    {
      label483:
      try
      {
        d = localObject2;
        paramc = (c.d.c<? super c>)localObject4;
        e = paramString;
        paramc = (c.d.c<? super c>)localObject4;
        f = localRtcTokenDto;
        paramc = (c.d.c<? super c>)localObject4;
        g = localObject4;
        paramc = (c.d.c<? super c>)localObject4;
        h = localObject5;
        paramc = (c.d.c<? super c>)localObject4;
        b = 2;
        paramc = (c.d.c<? super c>)localObject4;
        localObject5 = ((a)localObject2).a((u)localObject4, (c.d.c)localObject3);
        if (localObject5 != locala) {
          break label573;
        }
        return locala;
      }
      finally {}
      paramc = (c.d.c<? super c>)localObject2;
      if (((Boolean)localObject4).booleanValue())
      {
        paramc = (c.d.c<? super c>)localObject2;
        kotlinx.coroutines.e.b((ag)localObject3, null, (m)new a.b((a)localObject3, paramString, null), 3);
        paramc = (c.d.c<? super c>)localObject2;
        paramString = (c)f.a;
      }
      else
      {
        paramc = (c.d.c<? super c>)localObject2;
        paramString = (c)new d(FailedChannelJoinReason.RTC_JOIN_FAILED);
      }
      ((u)localObject2).n();
      return paramString;
      label557:
      paramc.n();
      throw paramString;
      label565:
      throw a;
      label573:
      localObject3 = localObject2;
      localObject2 = localObject4;
      localObject4 = localObject5;
    }
  }
  
  public final void a(ag paramag, m<? super VoipMsg, ? super c.d.c<? super x>, ? extends Object> paramm)
  {
    k.b(paramag, "scope");
    k.b(paramm, "block");
    j.a(paramag, a, paramm);
  }
  
  public final void a(boolean paramBoolean)
  {
    RtcEngine localRtcEngine = b;
    if (localRtcEngine != null)
    {
      localRtcEngine.setEnableSpeakerphone(paramBoolean);
      return;
    }
  }
  
  public final void b()
  {
    RtcEngine localRtcEngine = b;
    if (localRtcEngine != null)
    {
      localRtcEngine.leaveChannel();
      return;
    }
  }
  
  public final void b(boolean paramBoolean)
  {
    RtcEngine localRtcEngine = b;
    if (localRtcEngine != null)
    {
      localRtcEngine.muteLocalAudioStream(paramBoolean);
      return;
    }
  }
  
  public final void c(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      localRtcEngine = b;
      if (localRtcEngine != null) {
        localRtcEngine.enableAudio();
      }
      return;
    }
    RtcEngine localRtcEngine = b;
    if (localRtcEngine != null)
    {
      localRtcEngine.disableAudio();
      return;
    }
  }
  
  public final void d()
  {
    RtcEngine localRtcEngine = b;
    if (localRtcEngine != null) {
      localRtcEngine.leaveChannel();
    }
    kotlinx.coroutines.e.b((ag)bg.a, f, (m)new a.e(null), 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.manager.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */