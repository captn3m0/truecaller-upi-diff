package com.truecaller.voip;

import c.g;
import c.g.a.a;
import c.n.m;
import com.truecaller.common.account.r;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.utils.d;
import java.util.Iterator;
import javax.inject.Inject;
import javax.inject.Named;

public final class l
  implements k
{
  String b;
  private final c.f c;
  private final e d;
  private final r e;
  private final d f;
  
  @Inject
  public l(@Named("features_registry") e parame, r paramr, d paramd)
  {
    d = parame;
    e = paramr;
    f = paramd;
    b = "release";
    c = g.a((a)new l.a(this));
  }
  
  public final boolean a()
  {
    if ((d.B().a()) && (e.c()) && (((Boolean)c.b()).booleanValue()))
    {
      Object localObject1 = d;
      localObject1 = ((com.truecaller.featuretoggles.f)O.a((e)localObject1, e.a[111])).e();
      boolean bool = m.a((CharSequence)localObject1);
      Object localObject3 = null;
      if (!(bool ^ true)) {
        localObject1 = null;
      }
      if (localObject1 != null)
      {
        Object localObject2 = m.c((CharSequence)localObject1, new String[] { "," }, false, 6);
        if (localObject2 != null)
        {
          localObject1 = f.l();
          if (!(m.a((CharSequence)localObject1) ^ true)) {
            localObject1 = null;
          }
          if (localObject1 == null)
          {
            i = 1;
            break label229;
          }
          Iterator localIterator = ((Iterable)localObject2).iterator();
          do
          {
            localObject2 = localObject3;
            if (!localIterator.hasNext()) {
              break;
            }
            localObject2 = localIterator.next();
          } while (!m.a((String)localObject1, (String)localObject2, true));
          if (localObject2 == null)
          {
            i = 1;
            break label229;
          }
          i = 0;
          break label229;
        }
      }
      int i = 1;
      label229:
      if (i != 0) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */