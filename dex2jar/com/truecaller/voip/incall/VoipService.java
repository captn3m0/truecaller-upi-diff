package com.truecaller.voip.incall;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.z.a.a;
import android.support.v4.app.z.d;
import android.support.v4.content.b;
import android.widget.Toast;
import c.d.f;
import c.g.a.m;
import com.truecaller.utils.extensions.i;
import com.truecaller.utils.extensions.n;
import com.truecaller.voip.R.color;
import com.truecaller.voip.R.drawable;
import com.truecaller.voip.R.id;
import com.truecaller.voip.R.string;
import com.truecaller.voip.VoipUser;
import com.truecaller.voip.incall.ui.VoipActivity;
import com.truecaller.voip.j;
import com.truecaller.voip.j.a;
import com.truecaller.voip.util.w;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.a.h;
import kotlinx.coroutines.e;

public final class VoipService
  extends Service
  implements b.d, kotlinx.coroutines.ag
{
  public static final VoipService.a e = new VoipService.a((byte)0);
  private static boolean h;
  @Inject
  @Named("UI")
  public f a;
  @Inject
  @Named("IO")
  public f b;
  @Inject
  public b.c c;
  @Inject
  public w d;
  private z.d f;
  private PowerManager.WakeLock g;
  
  private final Intent q()
  {
    Intent localIntent = new Intent((Context)this, VoipActivity.class);
    localIntent.setFlags(268435456);
    return localIntent;
  }
  
  private final void r()
  {
    z.d locald = f;
    if (locald == null) {
      return;
    }
    startForeground(R.id.voip_service_foreground_notification, locald.h());
  }
  
  public final f V_()
  {
    f localf = a;
    if (localf == null) {
      c.g.b.k.a("uiContext");
    }
    return localf;
  }
  
  public final h<VoipUser> a()
  {
    b.c localc = c;
    if (localc == null) {
      c.g.b.k.a("presenter");
    }
    return localc.a();
  }
  
  public final void a(int paramInt)
  {
    z.d locald = f;
    if (locald == null) {
      return;
    }
    f localf = b;
    if (localf == null) {
      c.g.b.k.a("asyncContext");
    }
    e.b(this, localf, (m)new VoipService.b(this, paramInt, locald, null), 2);
  }
  
  public final void a(Bitmap paramBitmap)
  {
    c.g.b.k.b(paramBitmap, "icon");
    z.d locald = f;
    if (locald != null) {
      locald.a(paramBitmap);
    }
    r();
  }
  
  public final void a(b.b paramb)
  {
    b.c localc = c;
    if (localc == null) {
      c.g.b.k.a("presenter");
    }
    localc.a(paramb);
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "title");
    z.d locald = f;
    if (locald != null) {
      locald.a((CharSequence)paramString);
    }
    r();
  }
  
  public final void a(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      localWakeLock = g;
      if ((localWakeLock != null) && (!localWakeLock.isHeld())) {
        localWakeLock.acquire(TimeUnit.HOURS.toMillis(5L));
      }
      return;
    }
    PowerManager.WakeLock localWakeLock = g;
    if (localWakeLock != null)
    {
      if (localWakeLock.isHeld()) {
        localWakeLock.release();
      }
      return;
    }
  }
  
  public final void a(boolean paramBoolean, long paramLong)
  {
    z.d locald = f;
    if (locald != null) {
      locald.a(paramBoolean);
    }
    locald = f;
    if (locald != null) {
      locald.a(paramLong);
    }
    r();
  }
  
  public final h<com.truecaller.voip.ag> b()
  {
    b.c localc = c;
    if (localc == null) {
      c.g.b.k.a("presenter");
    }
    return localc.b();
  }
  
  public final void b(int paramInt)
  {
    String str = getString(paramInt);
    c.g.b.k.a(str, "getString(id)");
    b(str);
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "text");
    z.d locald = f;
    if (locald != null) {
      locald.b((CharSequence)paramString);
    }
    r();
  }
  
  public final void b(boolean paramBoolean)
  {
    b.c localc = c;
    if (localc == null) {
      c.g.b.k.a("presenter");
    }
    localc.a(paramBoolean);
  }
  
  public final void c(boolean paramBoolean)
  {
    b.c localc = c;
    if (localc == null) {
      c.g.b.k.a("presenter");
    }
    localc.b(paramBoolean);
  }
  
  public final h<com.truecaller.voip.manager.k> d()
  {
    b.c localc = c;
    if (localc == null) {
      c.g.b.k.a("presenter");
    }
    return localc.c();
  }
  
  public final void d(boolean paramBoolean)
  {
    b.c localc = c;
    if (localc == null) {
      c.g.b.k.a("presenter");
    }
    localc.c(paramBoolean);
  }
  
  public final void e()
  {
    stopForeground(true);
    stopSelf();
  }
  
  public final com.truecaller.voip.ag f()
  {
    b.c localc = c;
    if (localc == null) {
      c.g.b.k.a("presenter");
    }
    return localc.f();
  }
  
  public final long g()
  {
    b.c localc = c;
    if (localc == null) {
      c.g.b.k.a("presenter");
    }
    return localc.g();
  }
  
  public final void h()
  {
    sendBroadcast(new Intent("android.intent.action.CLOSE_SYSTEM_DIALOGS"));
    i.f(this).cancel(R.id.voip_incoming_service_missed_call_notification);
  }
  
  public final void i()
  {
    Object localObject1 = q();
    z.d locald = f;
    if (locald == null)
    {
      localObject1 = null;
    }
    else
    {
      Context localContext = (Context)this;
      int i = R.id.voip_incoming_notification_action_hang_up;
      Object localObject2 = new Intent(localContext, VoipService.class);
      ((Intent)localObject2).setAction("com.truecaller.voip.incoming.ACTION_NOTIFICATION");
      localObject2 = PendingIntent.getService(localContext, i, (Intent)localObject2, 134217728);
      localObject1 = locald.a(new z.a.a(0, (CharSequence)getString(R.string.voip_button_notification_hang_up), (PendingIntent)localObject2).a()).a(PendingIntent.getActivity(localContext, 0, (Intent)localObject1, 0)).h();
    }
    if (localObject1 == null) {
      return;
    }
    startForeground(R.id.voip_service_foreground_notification, (Notification)localObject1);
  }
  
  public final void j()
  {
    startActivity(q());
  }
  
  public final void k()
  {
    sendBroadcast(new Intent("android.intent.action.CLOSE_SYSTEM_DIALOGS"));
  }
  
  public final void l()
  {
    Toast.makeText((Context)this, (CharSequence)getString(R.string.voip_error_already_in_another_call, new Object[] { getString(R.string.voip_text) }), 1).show();
  }
  
  public final void m()
  {
    b.c localc = c;
    if (localc == null) {
      c.g.b.k.a("presenter");
    }
    localc.h();
  }
  
  public final void n()
  {
    b.c localc = c;
    if (localc == null) {
      c.g.b.k.a("presenter");
    }
    localc.i();
  }
  
  public final void o()
  {
    b.c localc = c;
    if (localc == null) {
      c.g.b.k.a("presenter");
    }
    localc.j();
  }
  
  public final void onCreate()
  {
    super.onCreate();
    h = true;
    Object localObject = j.a;
    j.a.a().a(this);
    localObject = (Context)this;
    b.c localc = c;
    if (localc == null) {
      c.g.b.k.a("presenter");
    }
    f = new z.d((Context)localObject, localc.e()).a(R.drawable.ic_voip_notification).b().d().f(b.c((Context)localObject, R.color.voip_header_color)).e(1);
    g = n.a(i.g(this));
  }
  
  public final void onDestroy()
  {
    h = false;
    b.c localc = c;
    if (localc == null) {
      c.g.b.k.a("presenter");
    }
    localc.y_();
    super.onDestroy();
  }
  
  public final int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    String str3 = null;
    String str1;
    if (paramIntent != null) {
      str1 = paramIntent.getAction();
    } else {
      str1 = null;
    }
    String str2;
    if (paramIntent != null) {
      str2 = paramIntent.getStringExtra("com.truecaller.voip.extra.EXTRA_NUMBER");
    } else {
      str2 = null;
    }
    VoipUser localVoipUser;
    if (paramIntent != null) {
      localVoipUser = (VoipUser)paramIntent.getParcelableExtra("com.truecaller.voip.extra.EXTRA_USER_ID");
    } else {
      localVoipUser = null;
    }
    if (paramIntent != null) {
      str3 = paramIntent.getStringExtra("com.truecaller.voip.extra.EXTRA_CHANNEL_ID");
    }
    boolean bool = false;
    if (paramIntent != null) {
      bool = paramIntent.getBooleanExtra("com.truecaller.voip.incoming.EXTRA_FROM_MISSED_CALL", false);
    }
    paramIntent = c;
    if (paramIntent == null) {
      c.g.b.k.a("presenter");
    }
    paramIntent.a(this);
    if (str1 == null)
    {
      paramIntent = c;
      if (paramIntent == null) {
        c.g.b.k.a("presenter");
      }
      paramIntent.a(str2, localVoipUser, str3, bool);
    }
    else
    {
      paramIntent = c;
      if (paramIntent == null) {
        c.g.b.k.a("presenter");
      }
      paramIntent.a(str1);
    }
    return 2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.VoipService
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */