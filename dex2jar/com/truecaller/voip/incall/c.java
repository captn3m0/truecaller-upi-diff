package com.truecaller.voip.incall;

import c.g.a.m;
import c.l;
import com.truecaller.ba;
import com.truecaller.notificationchannels.b;
import com.truecaller.utils.n;
import com.truecaller.voip.R.color;
import com.truecaller.voip.R.string;
import com.truecaller.voip.VoipState;
import com.truecaller.voip.VoipStateReason;
import com.truecaller.voip.VoipUser;
import com.truecaller.voip.ag;
import com.truecaller.voip.manager.g;
import com.truecaller.voip.util.AudioRoute;
import com.truecaller.voip.util.VoipAnalyticsCallDirection;
import com.truecaller.voip.util.VoipAnalyticsContext;
import com.truecaller.voip.util.VoipAnalyticsNotificationAction;
import com.truecaller.voip.util.aa;
import com.truecaller.voip.util.an;
import com.truecaller.voip.util.au;
import com.truecaller.voip.util.j;
import com.truecaller.voip.util.o;
import com.truecaller.voip.util.s;
import com.truecaller.voip.util.w;
import com.truecaller.voip.util.y;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.a.p;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.e;

public final class c
  extends ba<b.d>
  implements b.c
{
  private final j A;
  private final com.truecaller.voip.util.f B;
  private final n C;
  private final s D;
  private final com.truecaller.voip.manager.rtm.h E;
  private final o F;
  private final an G;
  b.b c;
  private String d;
  private VoipUser e;
  private boolean f;
  private String g;
  private ag h;
  private com.truecaller.voip.manager.k i;
  private com.truecaller.voip.manager.k j;
  private boolean k;
  private long l;
  private final kotlinx.coroutines.a.h<VoipUser> m;
  private final kotlinx.coroutines.a.h<ag> n;
  private final kotlinx.coroutines.a.h<Boolean> o;
  private final p<com.truecaller.voip.manager.k> p;
  private final c.d.f q;
  private final c.d.f r;
  private final g s;
  private final com.truecaller.voip.manager.rtm.i t;
  private final aa u;
  private final au v;
  private final b w;
  private final com.truecaller.utils.a x;
  private final w y;
  private final y z;
  
  @Inject
  public c(@Named("UI") c.d.f paramf1, @Named("IO") c.d.f paramf2, g paramg, com.truecaller.voip.manager.rtm.i parami, aa paramaa, au paramau, b paramb, com.truecaller.utils.a parama, w paramw, y paramy, j paramj, com.truecaller.voip.util.f paramf, n paramn, s params, com.truecaller.voip.manager.rtm.h paramh, o paramo, an paraman)
  {
    super(paramf1);
    q = paramf1;
    r = paramf2;
    s = paramg;
    t = parami;
    u = paramaa;
    v = paramau;
    w = paramb;
    x = parama;
    y = paramw;
    z = paramy;
    A = paramj;
    B = paramf;
    C = paramn;
    D = params;
    E = paramh;
    F = paramo;
    G = paraman;
    h = new ag(null, null, 0, 0, false, null, false, 255);
    i = new com.truecaller.voip.manager.k();
    j = new com.truecaller.voip.manager.k();
    m = kotlinx.coroutines.a.i.a(-1);
    n = kotlinx.coroutines.a.i.a(-1);
    o = kotlinx.coroutines.a.i.a(-1);
    p = new p(i);
  }
  
  private final void a(VoipState paramVoipState, VoipStateReason paramVoipStateReason)
  {
    if (!a(paramVoipState)) {
      return;
    }
    Object localObject = new StringBuilder("Setting state: ");
    ((StringBuilder)localObject).append(paramVoipState.name());
    ((StringBuilder)localObject).toString();
    c.g.a.a locala = (c.g.a.a)c.ak.a;
    localObject = (c.g.a.a)c.aj.a;
    switch (d.f[paramVoipState.ordinal()])
    {
    default: 
      throw new l();
    case 14: 
      localObject = (c.g.a.a)new c.aa(this);
      paramVoipState = new ag(paramVoipState, null, R.string.voip_status_call_failed, R.color.voip_call_status_error_color, true, "Call failed. Exiting...", false, 134);
      break;
    case 13: 
      localObject = (c.g.a.a)new c.z(this);
      paramVoipState = new ag(paramVoipState, null, R.string.voip_status_call_ended, R.color.voip_call_status_error_color, true, "Call ended. Exiting...", false, 134);
      break;
    case 12: 
      localObject = (c.g.a.a)new c.y(this);
      paramVoipState = new ag(paramVoipState, null, 0, 0, false, "Call blocked. Exiting...", false, 190);
      break;
    case 11: 
      locala = (c.g.a.a)new c.w(this);
      localObject = (c.g.a.a)new c.x(this);
      paramVoipState = new ag(paramVoipState, null, 0, R.color.voip_call_status_ok_color, true, "Channel joined. Say hello!", true, 14);
      break;
    case 10: 
      locala = (c.g.a.a)new c.u(this);
      localObject = (c.g.a.a)new c.v(this);
      paramVoipState = new ag(paramVoipState, null, R.string.voip_status_busy, R.color.voip_call_status_warning_color, true, "User is in another call. Exiting...", false, 134);
      break;
    case 9: 
      locala = (c.g.a.a)new c.ah(this);
      localObject = (c.g.a.a)new c.ai(this);
      paramVoipState = new ag(paramVoipState, null, R.string.voip_status_rejected, R.color.voip_call_status_error_color, true, "Invite rejected. Exiting...", false, 134);
      break;
    case 8: 
      paramVoipState = new ag(paramVoipState, null, 0, 0, false, "Invite accepted.", false, 190);
      break;
    case 7: 
      locala = (c.g.a.a)new c.af(this);
      localObject = (c.g.a.a)new c.ag(this);
      paramVoipState = new ag(paramVoipState, null, R.string.voip_status_offline, R.color.voip_call_status_error_color, true, "Invite failed. User is offline. Exiting...", false, 134);
      break;
    case 6: 
      localObject = (c.g.a.a)new c.ae(this);
      paramVoipState = new ag(paramVoipState, null, R.string.voip_status_no_answer, R.color.voip_call_status_error_color, true, "User did not answer. Exiting...", false, 134);
      break;
    case 5: 
      localObject = (c.g.a.a)new c.ad(this);
      paramVoipState = new ag(paramVoipState, null, R.string.voip_status_ringing, R.color.voip_call_status_ok_color, true, "Invite received. Ringing...", false, 134);
      break;
    case 4: 
      paramVoipState = new ag(paramVoipState, null, R.string.voip_status_connecting, R.color.voip_call_status_warning_color, true, "Invite is received by peer. Waiting for ringing message...", false, 134);
      break;
    case 3: 
      locala = (c.g.a.a)new c.ac(this);
      paramVoipState = new ag(paramVoipState, null, R.string.voip_status_connecting, R.color.voip_call_status_warning_color, true, "Inviting user to voip call...", false, 134);
      break;
    case 2: 
      locala = (c.g.a.a)new c.t(this);
      localObject = (c.g.a.a)new c.ab(this);
      paramVoipState = new ag(paramVoipState, null, R.string.voip_status_connecting, R.color.voip_call_status_warning_color, true, "Initializing, resolving the user...", false, 134);
      break;
    case 1: 
      paramVoipState = h;
    }
    h = paramVoipState;
    h = ag.a(h, null, paramVoipStateReason, null, 0, 0, false, null, false, 253);
    locala.invoke();
    m();
    ((c.g.a.a)localObject).invoke();
    n.d_(h);
  }
  
  private final void a(com.truecaller.voip.manager.k paramk)
  {
    if (c.g.b.k.a(paramk, i)) {
      return;
    }
    i = paramk;
    p.d_(paramk);
  }
  
  private final boolean a(VoipState paramVoipState)
  {
    if (paramVoipState == h.a) {
      return false;
    }
    return !q();
  }
  
  private final void d(boolean paramBoolean)
  {
    if (paramBoolean != i.b)
    {
      if (q()) {
        return;
      }
      a(com.truecaller.voip.manager.k.a(i, false, paramBoolean, null, 5));
      n();
      m();
      k();
      o.d_(Boolean.valueOf(paramBoolean));
      return;
    }
  }
  
  private final bn k()
  {
    return e.b(this, null, (m)new c.r(this, null), 3);
  }
  
  private final VoipAnalyticsCallDirection l()
  {
    if (k) {
      return VoipAnalyticsCallDirection.INCOMING;
    }
    return VoipAnalyticsCallDirection.OUTGOING;
  }
  
  private final void m()
  {
    ag localag = p();
    Object localObject = (b.d)b;
    if (localObject != null) {
      ((b.d)localObject).b(localag.a());
    }
    localObject = c;
    if (localObject != null) {
      ((b.b)localObject).a(localag.a(), localag.c(), localag.d());
    }
    localObject = (b.d)b;
    if (localObject != null) {
      ((b.d)localObject).a(localag.b(), l);
    }
    localObject = c;
    if (localObject != null) {
      ((b.b)localObject).a(localag.b(), l);
    }
    localObject = c;
    if (localObject != null) {
      ((b.b)localObject).a(e);
    }
    B.a(h.a, h.c, i, j);
  }
  
  private final void n()
  {
    g localg = s;
    boolean bool;
    if (i.c.a == AudioRoute.SPEAKER) {
      bool = true;
    } else {
      bool = false;
    }
    localg.a(bool);
    s.c(i.b ^ true);
    s.b(i.a);
  }
  
  private final bn o()
  {
    return e.b(this, null, (m)new c.a(this, null), 3);
  }
  
  private final ag p()
  {
    if ((!i.b) && (!j.b))
    {
      if ((j.a) && (h.a == VoipState.ONGOING)) {
        return ag.a(h, null, null, null, R.string.voip_status_call_muted, 0, false, "Peer has muted the microphone.", false, 55);
      }
      return h;
    }
    return new ag(null, null, R.string.voip_status_on_hold, R.color.voip_call_status_warning_color, true, "Call is on hold...", false, 135);
  }
  
  private final boolean q()
  {
    VoipState localVoipState = h.a;
    switch (d.i[localVoipState.ordinal()])
    {
    default: 
      return false;
    }
    return true;
  }
  
  private final String r()
  {
    if (f)
    {
      VoipUser localVoipUser = e;
      if (localVoipUser == null) {
        c.g.b.k.a("voipUser");
      }
      return b;
    }
    return g;
  }
  
  public final kotlinx.coroutines.a.h<VoipUser> a()
  {
    return m;
  }
  
  public final bn a(String paramString1, VoipUser paramVoipUser, String paramString2, boolean paramBoolean)
  {
    return e.b(this, null, (m)new c.m(this, paramBoolean, paramString1, paramString2, paramVoipUser, null), 3);
  }
  
  public final void a(b.b paramb)
  {
    c = paramb;
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "action");
    if (paramString.hashCode() != 1547796818) {
      return;
    }
    if (paramString.equals("com.truecaller.voip.incoming.ACTION_NOTIFICATION"))
    {
      F.a(VoipAnalyticsContext.NOTIFICATION.getValue(), VoipAnalyticsNotificationAction.HANG_UP);
      h();
      paramString = (b.d)b;
      if (paramString != null)
      {
        paramString.k();
        return;
      }
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    if (paramBoolean) {
      localObject = AudioRoute.SPEAKER;
    } else {
      localObject = AudioRoute.EARPIECE;
    }
    Object localObject = com.truecaller.voip.util.a.a(i.c, (AudioRoute)localObject);
    a(com.truecaller.voip.manager.k.a(i, false, false, (com.truecaller.voip.util.a)localObject, 3));
    n();
  }
  
  public final kotlinx.coroutines.a.h<ag> b()
  {
    return n;
  }
  
  public final void b(boolean paramBoolean)
  {
    a(com.truecaller.voip.manager.k.a(i, paramBoolean, false, null, 6));
    n();
  }
  
  public final void c(boolean paramBoolean)
  {
    if (paramBoolean) {
      localObject = AudioRoute.BLUETOOTH;
    } else {
      localObject = AudioRoute.EARPIECE;
    }
    Object localObject = com.truecaller.voip.util.a.a(i.c, (AudioRoute)localObject);
    a(com.truecaller.voip.manager.k.a(i, false, false, (com.truecaller.voip.util.a)localObject, 3));
  }
  
  public final String e()
  {
    return w.h();
  }
  
  public final ag f()
  {
    return p();
  }
  
  public final long g()
  {
    return l;
  }
  
  public final void h()
  {
    e.b(this, null, (m)new c.l(this, null), 3);
    Object localObject = h.a;
    if (d.a[localObject.ordinal()] != 1) {
      localObject = VoipStateReason.HUNG_UP;
    } else {
      localObject = VoipStateReason.INVITE_CANCELLED;
    }
    a(VoipState.ENDED, (VoipStateReason)localObject);
  }
  
  public final void i()
  {
    d(true);
  }
  
  public final void j()
  {
    d(false);
  }
  
  public final void y_()
  {
    Object localObject = c;
    if (localObject != null) {
      ((b.b)localObject).a();
    }
    s.d();
    localObject = (b.d)b;
    if (localObject != null) {
      ((b.d)localObject).a(false);
    }
    D.f();
    E.b();
    D.d();
    super.y_();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */