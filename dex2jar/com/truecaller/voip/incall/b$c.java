package com.truecaller.voip.incall;

import com.truecaller.bm;
import com.truecaller.voip.VoipUser;
import com.truecaller.voip.ag;
import com.truecaller.voip.manager.k;
import kotlinx.coroutines.a.h;
import kotlinx.coroutines.bn;

public abstract interface b$c
  extends bm<b.d>
{
  public abstract h<VoipUser> a();
  
  public abstract bn a(String paramString1, VoipUser paramVoipUser, String paramString2, boolean paramBoolean);
  
  public abstract void a(b.b paramb);
  
  public abstract void a(String paramString);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract h<ag> b();
  
  public abstract void b(boolean paramBoolean);
  
  public abstract h<k> c();
  
  public abstract void c(boolean paramBoolean);
  
  public abstract String e();
  
  public abstract ag f();
  
  public abstract long g();
  
  public abstract void h();
  
  public abstract void i();
  
  public abstract void j();
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.b.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */