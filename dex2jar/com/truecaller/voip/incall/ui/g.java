package com.truecaller.voip.incall.ui;

import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.utils.a;
import com.truecaller.utils.extensions.j;
import com.truecaller.voip.ag;
import com.truecaller.voip.incall.b.a;
import com.truecaller.voip.incall.b.b;
import com.truecaller.voip.util.VoipAnalyticsInCallUiAction;
import com.truecaller.voip.util.at;
import com.truecaller.voip.util.o;
import javax.inject.Inject;
import javax.inject.Named;

public final class g
  extends ba<f.b>
  implements f.a
{
  private b.a c;
  private final g.a d;
  private final f e;
  private final a f;
  private final at g;
  private final o h;
  
  @Inject
  public g(@Named("UI") f paramf, a parama, at paramat, o paramo)
  {
    super(paramf);
    e = paramf;
    f = parama;
    g = paramat;
    h = paramo;
    d = new g.a(this);
  }
  
  private final long a(long paramLong)
  {
    return f.b() - (f.a() - paramLong);
  }
  
  public final void a()
  {
    b.a locala = c;
    if (locala != null) {
      locala.a(null);
    }
    c = null;
  }
  
  public final void a(b.a parama)
  {
    k.b(parama, "binderView");
    parama.a((b.b)d);
    j.a(this, parama.a(), (m)new g.c(this, null));
    j.a(this, parama.d(), (m)new g.b(this, null));
    ag localag = parama.f();
    f.b localb = (f.b)b;
    if (localb != null) {
      localb.a(localag.a(), localag.c(), localag.d());
    }
    localb = (f.b)b;
    if (localb != null) {
      localb.a(localag.b(), a(parama.g()));
    }
    localb = (f.b)b;
    if (localb != null) {
      localb.c(e);
    }
    c = parama;
  }
  
  public final void a(boolean paramBoolean)
  {
    if (paramBoolean) {
      return;
    }
    f.b localb = (f.b)b;
    if (localb != null)
    {
      localb.g();
      return;
    }
  }
  
  public final void b()
  {
    Object localObject = c;
    if (localObject != null) {
      ((b.a)localObject).m();
    }
    localObject = (f.b)b;
    if (localObject != null) {
      ((f.b)localObject).d();
    }
    h.a(VoipAnalyticsInCallUiAction.REJECT);
  }
  
  public final void b(boolean paramBoolean)
  {
    Object localObject = c;
    if (localObject != null) {
      ((b.a)localObject).b(paramBoolean);
    }
    if (paramBoolean) {
      localObject = VoipAnalyticsInCallUiAction.SPEAKER_ON;
    } else {
      localObject = VoipAnalyticsInCallUiAction.SPEAKER_OFF;
    }
    h.a((VoipAnalyticsInCallUiAction)localObject);
  }
  
  public final void c()
  {
    f.b localb = (f.b)b;
    if (localb != null) {
      localb.e();
    }
    h.a(VoipAnalyticsInCallUiAction.DISMISS);
  }
  
  public final void c(boolean paramBoolean)
  {
    Object localObject = c;
    if (localObject != null) {
      ((b.a)localObject).c(paramBoolean);
    }
    if (paramBoolean) {
      localObject = VoipAnalyticsInCallUiAction.MIC_OFF;
    } else {
      localObject = VoipAnalyticsInCallUiAction.MIC_ON;
    }
    h.a((VoipAnalyticsInCallUiAction)localObject);
  }
  
  public final void d(boolean paramBoolean)
  {
    b.a locala = c;
    if (locala != null)
    {
      locala.d(paramBoolean);
      return;
    }
  }
  
  public final void e()
  {
    h.a(VoipAnalyticsInCallUiAction.BACK);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.ui.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */