package com.truecaller.voip.incall.ui;

import android.animation.AnimatorSet;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.constraint.motion.MotionLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.text.method.MovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;
import c.g.a.m;
import c.g.b.k;
import c.u;
import c.x;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.truecaller.common.h.aq.d;
import com.truecaller.utils.extensions.t;
import com.truecaller.voip.R.color;
import com.truecaller.voip.R.drawable;
import com.truecaller.voip.R.id;
import com.truecaller.voip.R.layout;
import com.truecaller.voip.R.string;
import com.truecaller.voip.incall.VoipService;
import com.truecaller.voip.j;
import com.truecaller.voip.j.a;
import java.util.HashMap;
import javax.inject.Inject;

public final class a
  extends Fragment
  implements f.b
{
  @Inject
  public f.a a;
  private MotionLayout b;
  private FloatingActionButton c;
  private Chronometer d;
  private ImageView e;
  private TextView f;
  private TextView g;
  private TextView h;
  private TextView i;
  private ToggleButton j;
  private ToggleButton k;
  private ToggleButton l;
  private ImageButton m;
  private View n;
  private View o;
  private ImageView p;
  private ImageView q;
  private TextView r;
  private final a.b s = new a.b(this);
  private final m<CompoundButton, Boolean, x> t = (m)new a.f(this);
  private final m<CompoundButton, Boolean, x> u = (m)new a.c(this);
  private final m<CompoundButton, Boolean, x> v = (m)new a.a(this);
  private HashMap w;
  
  private static void a(ToggleButton paramToggleButton, boolean paramBoolean, m<? super CompoundButton, ? super Boolean, x> paramm)
  {
    paramToggleButton.setOnCheckedChangeListener(null);
    paramToggleButton.setChecked(paramBoolean);
    if (paramm != null) {
      paramm = new b(paramm);
    }
    paramToggleButton.setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)paramm);
  }
  
  public final f.a a()
  {
    f.a locala = a;
    if (locala == null) {
      k.a("presenter");
    }
    return locala;
  }
  
  public final void a(int paramInt)
  {
    ImageView localImageView = q;
    if (localImageView == null) {
      k.a("truecallerLogoView");
    }
    localImageView.setImageResource(paramInt);
  }
  
  public final void a(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    Object localObject = getContext();
    if (localObject == null) {
      return;
    }
    k.a(localObject, "context ?: return");
    int i1 = android.support.v4.content.b.c((Context)localObject, paramInt2);
    localObject = h;
    if (localObject == null) {
      k.a("statusTextView");
    }
    ((TextView)localObject).setText(paramInt1);
    localObject = h;
    if (localObject == null) {
      k.a("statusTextView");
    }
    ((TextView)localObject).setTextColor(i1);
    localObject = p;
    if (localObject == null) {
      k.a("callStateRingView");
    }
    localObject = ((ImageView)localObject).getDrawable();
    if (localObject != null)
    {
      localObject = (com.truecaller.voip.a.a)localObject;
      if (paramInt2 == R.color.voip_call_status_warning_color)
      {
        ((com.truecaller.voip.a.a)localObject).c();
      }
      else if (paramInt2 == R.color.voip_call_status_ok_color)
      {
        ((com.truecaller.voip.a.a)localObject).a(android.support.v4.content.b.c(e, R.color.voip_call_status_ok_color));
        if (a.isEmpty()) {
          if (!b)
          {
            ((com.truecaller.voip.a.a)localObject).a();
          }
          else
          {
            ((com.truecaller.voip.a.a)localObject).b();
            c.start();
          }
        }
      }
      else if (paramInt2 == R.color.voip_call_status_error_color)
      {
        ((com.truecaller.voip.a.a)localObject).a(android.support.v4.content.b.c(e, R.color.voip_call_status_error_color));
        if (!a.isEmpty())
        {
          ((com.truecaller.voip.a.a)localObject).b();
          ((com.truecaller.voip.a.a)localObject).a();
          d.start();
        }
      }
      else
      {
        ((com.truecaller.voip.a.a)localObject).a(android.support.v4.content.b.c(e, R.color.voip_call_status_neutral_color));
      }
      localObject = p;
      if (localObject == null) {
        k.a("callStateRingView");
      }
      t.a((View)localObject, paramBoolean);
      return;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.voip.view.CallStateAvatarRingDrawable");
  }
  
  public final void a(String paramString)
  {
    TextView localTextView = g;
    if (localTextView == null) {
      k.a("profileNameTextView");
    }
    localTextView.setText((CharSequence)paramString);
    paramString = g;
    if (paramString == null) {
      k.a("profileNameTextView");
    }
    paramString.setSelected(true);
  }
  
  public final void a(boolean paramBoolean)
  {
    ToggleButton localToggleButton = k;
    if (localToggleButton == null) {
      k.a("speakerToggleButton");
    }
    a(localToggleButton, paramBoolean, t);
  }
  
  public final void a(boolean paramBoolean, long paramLong)
  {
    Chronometer localChronometer = d;
    if (localChronometer == null) {
      k.a("chronometer");
    }
    t.a((View)localChronometer, paramBoolean);
    if (paramBoolean)
    {
      localChronometer = d;
      if (localChronometer == null) {
        k.a("chronometer");
      }
      localChronometer.setBase(paramLong);
      localChronometer = d;
      if (localChronometer == null) {
        k.a("chronometer");
      }
      localChronometer.start();
      return;
    }
    localChronometer = d;
    if (localChronometer == null) {
      k.a("chronometer");
    }
    localChronometer.stop();
  }
  
  public final void a(boolean paramBoolean, String paramString)
  {
    k.b(paramString, "deviceName");
    Object localObject = l;
    if (localObject == null) {
      k.a("bluetoothToggleButton");
    }
    t.a((View)localObject);
    localObject = l;
    if (localObject == null) {
      k.a("bluetoothToggleButton");
    }
    ((ToggleButton)localObject).setEnabled(true);
    localObject = l;
    if (localObject == null) {
      k.a("bluetoothToggleButton");
    }
    a((ToggleButton)localObject, paramBoolean, v);
    localObject = r;
    if (localObject == null) {
      k.a("bluetoothTextView");
    }
    t.a((View)localObject);
    localObject = r;
    if (localObject == null) {
      k.a("bluetoothTextView");
    }
    ((TextView)localObject).setText((CharSequence)paramString);
    paramString = r;
    if (paramString == null) {
      k.a("bluetoothTextView");
    }
    paramString.setTextColor(-1);
  }
  
  public final void b()
  {
    Object localObject = l;
    if (localObject == null) {
      k.a("bluetoothToggleButton");
    }
    t.b((View)localObject);
    localObject = r;
    if (localObject == null) {
      k.a("bluetoothTextView");
    }
    t.b((View)localObject);
  }
  
  public final void b(int paramInt)
  {
    Object localObject1 = getActivity();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "activity ?: return");
    if (Build.VERSION.SDK_INT >= 21)
    {
      localObject2 = ((f)localObject1).getWindow();
      k.a(localObject2, "context.window");
      ((Window)localObject2).setStatusBarColor(android.support.v4.content.b.c((Context)localObject1, R.color.voip_status_bar_spam_color));
    }
    Object localObject2 = n;
    if (localObject2 == null) {
      k.a("headerCoverView");
    }
    ((View)localObject2).setBackgroundColor(android.support.v4.content.b.c((Context)localObject1, R.color.voip_spam_color));
    localObject1 = o;
    if (localObject1 == null) {
      k.a("headerArcView");
    }
    ((View)localObject1).setBackgroundResource(R.drawable.background_voip_spam_header_view);
    localObject1 = m;
    if (localObject1 == null) {
      k.a("minimiseButton");
    }
    ((ImageButton)localObject1).setBackgroundResource(R.drawable.background_voip_minimise_spam_call);
    localObject1 = e;
    if (localObject1 == null) {
      k.a("profilePictureImageView");
    }
    ((ImageView)localObject1).setImageDrawable(null);
    localObject1 = e;
    if (localObject1 == null) {
      k.a("profilePictureImageView");
    }
    ((ImageView)localObject1).setImageResource(R.drawable.ic_avatar_voip_spam);
    localObject1 = f;
    if (localObject1 == null) {
      k.a("spamScoreTextView");
    }
    ((TextView)localObject1).setText((CharSequence)getString(R.string.voip_spam_reports_score, new Object[] { Integer.valueOf(paramInt) }));
    localObject1 = f;
    if (localObject1 == null) {
      k.a("spamScoreTextView");
    }
    t.a((View)localObject1);
  }
  
  public final void b(String paramString)
  {
    Object localObject = (CharSequence)paramString;
    int i1;
    if ((localObject != null) && (((CharSequence)localObject).length() != 0)) {
      i1 = 0;
    } else {
      i1 = 1;
    }
    if (i1 != 0) {
      return;
    }
    localObject = requireContext();
    k.a(localObject, "requireContext()");
    paramString = w.a((Context)localObject).a(paramString).a(R.drawable.ic_avatar_voip_default).a((ai)aq.d.b());
    c = true;
    paramString = paramString.b();
    localObject = e;
    if (localObject == null) {
      k.a("profilePictureImageView");
    }
    paramString.a((ImageView)localObject, null);
  }
  
  public final void b(boolean paramBoolean)
  {
    ToggleButton localToggleButton = j;
    if (localToggleButton == null) {
      k.a("muteToggleButton");
    }
    a(localToggleButton, paramBoolean, u);
  }
  
  public final void c()
  {
    Object localObject = l;
    if (localObject == null) {
      k.a("bluetoothToggleButton");
    }
    t.a((View)localObject);
    localObject = r;
    if (localObject == null) {
      k.a("bluetoothTextView");
    }
    t.a((View)localObject);
    localObject = l;
    if (localObject == null) {
      k.a("bluetoothToggleButton");
    }
    ((ToggleButton)localObject).setEnabled(false);
    localObject = r;
    if (localObject == null) {
      k.a("bluetoothTextView");
    }
    ((TextView)localObject).setText(R.string.voip_button_bluetooth);
    localObject = r;
    if (localObject == null) {
      k.a("bluetoothTextView");
    }
    ((TextView)localObject).setTextColor(android.support.v4.content.b.c(requireContext(), R.color.voip_action_text_color_disabled));
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "message");
    paramString = i;
    if (paramString == null) {
      k.a("logTextView");
    }
    t.b((View)paramString);
  }
  
  public final void d()
  {
    Object localObject = c;
    if (localObject == null) {
      k.a("endCallButton");
    }
    ((FloatingActionButton)localObject).setEnabled(false);
    localObject = j;
    if (localObject == null) {
      k.a("muteToggleButton");
    }
    ((ToggleButton)localObject).setEnabled(false);
    localObject = k;
    if (localObject == null) {
      k.a("speakerToggleButton");
    }
    ((ToggleButton)localObject).setEnabled(false);
    localObject = l;
    if (localObject == null) {
      k.a("bluetoothToggleButton");
    }
    ((ToggleButton)localObject).setEnabled(false);
  }
  
  public final void e()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final void f()
  {
    MotionLayout localMotionLayout = b;
    if (localMotionLayout == null) {
      k.a("motionLayoutView");
    }
    localMotionLayout.a(R.id.outgoing_call_ended_start_set, R.id.outgoing_call_ended_end_set);
    localMotionLayout = b;
    if (localMotionLayout == null) {
      k.a("motionLayoutView");
    }
    localMotionLayout.c();
  }
  
  public final void g()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    k.b(paramContext, "context");
    super.onAttach(paramContext);
    paramContext = j.a;
    j.a.a().a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    paramLayoutInflater = paramLayoutInflater.inflate(R.layout.fragment_voip, paramViewGroup, false);
    k.a(paramLayoutInflater, "inflater.inflate(R.layou…t_voip, container, false)");
    return paramLayoutInflater;
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    Object localObject = a;
    if (localObject == null) {
      k.a("presenter");
    }
    ((f.a)localObject).y_();
    localObject = w;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onStart()
  {
    super.onStart();
    Object localObject = requireContext();
    k.a(localObject, "requireContext()");
    boolean bool = ((Context)localObject).bindService(new Intent((Context)localObject, VoipService.class), (ServiceConnection)s, 0);
    localObject = a;
    if (localObject == null) {
      k.a("presenter");
    }
    ((f.a)localObject).a(bool);
  }
  
  public final void onStop()
  {
    super.onStop();
    requireContext().unbindService((ServiceConnection)s);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    paramBundle = paramView.findViewById(R.id.motion_layout);
    k.a(paramBundle, "view.findViewById(R.id.motion_layout)");
    b = ((MotionLayout)paramBundle);
    paramBundle = paramView.findViewById(R.id.button_end_call);
    k.a(paramBundle, "view.findViewById(R.id.button_end_call)");
    c = ((FloatingActionButton)paramBundle);
    paramBundle = paramView.findViewById(R.id.chronometer);
    k.a(paramBundle, "view.findViewById(R.id.chronometer)");
    d = ((Chronometer)paramBundle);
    paramBundle = paramView.findViewById(R.id.text_profile_name);
    k.a(paramBundle, "view.findViewById(R.id.text_profile_name)");
    g = ((TextView)paramBundle);
    paramBundle = paramView.findViewById(R.id.text_status);
    k.a(paramBundle, "view.findViewById(R.id.text_status)");
    h = ((TextView)paramBundle);
    paramBundle = paramView.findViewById(R.id.image_profile_picture);
    k.a(paramBundle, "view.findViewById(R.id.image_profile_picture)");
    e = ((ImageView)paramBundle);
    paramBundle = paramView.findViewById(R.id.text_spam_score);
    k.a(paramBundle, "view.findViewById(R.id.text_spam_score)");
    f = ((TextView)paramBundle);
    paramBundle = paramView.findViewById(R.id.text_log);
    k.a(paramBundle, "view.findViewById(R.id.text_log)");
    i = ((TextView)paramBundle);
    paramBundle = paramView.findViewById(R.id.toggle_mute);
    k.a(paramBundle, "view.findViewById(R.id.toggle_mute)");
    j = ((ToggleButton)paramBundle);
    paramBundle = paramView.findViewById(R.id.toggle_speaker);
    k.a(paramBundle, "view.findViewById(R.id.toggle_speaker)");
    k = ((ToggleButton)paramBundle);
    paramBundle = paramView.findViewById(R.id.toggle_bluetooth);
    k.a(paramBundle, "view.findViewById(R.id.toggle_bluetooth)");
    l = ((ToggleButton)paramBundle);
    paramBundle = paramView.findViewById(R.id.button_minimise);
    k.a(paramBundle, "view.findViewById(R.id.button_minimise)");
    m = ((ImageButton)paramBundle);
    paramBundle = paramView.findViewById(R.id.view_header_cover);
    k.a(paramBundle, "view.findViewById(R.id.view_header_cover)");
    n = paramBundle;
    paramBundle = paramView.findViewById(R.id.view_header);
    k.a(paramBundle, "view.findViewById(R.id.view_header)");
    o = paramBundle;
    paramBundle = paramView.findViewById(R.id.image_call_state_ring);
    k.a(paramBundle, "view.findViewById(R.id.image_call_state_ring)");
    p = ((ImageView)paramBundle);
    paramBundle = paramView.findViewById(R.id.image_logo);
    k.a(paramBundle, "view.findViewById(R.id.image_logo)");
    q = ((ImageView)paramBundle);
    paramBundle = p;
    if (paramBundle == null) {
      k.a("callStateRingView");
    }
    Object localObject = paramView.getContext();
    k.a(localObject, "view.context");
    paramBundle.setImageDrawable((Drawable)new com.truecaller.voip.a.a((Context)localObject));
    paramView = paramView.findViewById(R.id.text_bluetooth);
    k.a(paramView, "view.findViewById(R.id.text_bluetooth)");
    r = ((TextView)paramView);
    paramView = i;
    if (paramView == null) {
      k.a("logTextView");
    }
    paramView.setMovementMethod((MovementMethod)new ScrollingMovementMethod());
    paramView = c;
    if (paramView == null) {
      k.a("endCallButton");
    }
    paramView.setOnClickListener((View.OnClickListener)new a.d(this));
    localObject = k;
    if (localObject == null) {
      k.a("speakerToggleButton");
    }
    paramBundle = t;
    paramView = paramBundle;
    if (paramBundle != null) {
      paramView = new b(paramBundle);
    }
    ((ToggleButton)localObject).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)paramView);
    localObject = j;
    if (localObject == null) {
      k.a("muteToggleButton");
    }
    paramBundle = u;
    paramView = paramBundle;
    if (paramBundle != null) {
      paramView = new b(paramBundle);
    }
    ((ToggleButton)localObject).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)paramView);
    localObject = l;
    if (localObject == null) {
      k.a("bluetoothToggleButton");
    }
    paramBundle = v;
    paramView = paramBundle;
    if (paramBundle != null) {
      paramView = new b(paramBundle);
    }
    ((ToggleButton)localObject).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)paramView);
    paramView = m;
    if (paramView == null) {
      k.a("minimiseButton");
    }
    paramView.setOnClickListener((View.OnClickListener)new a.e(this));
    paramView = a;
    if (paramView == null) {
      k.a("presenter");
    }
    paramView.a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.ui.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */