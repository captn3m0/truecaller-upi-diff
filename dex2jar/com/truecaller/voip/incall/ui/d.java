package com.truecaller.voip.incall.ui;

import c.d.f;
import c.g.a.m;
import c.g.b.k;
import c.l;
import com.truecaller.ba;
import com.truecaller.utils.a;
import com.truecaller.utils.extensions.j;
import javax.inject.Inject;
import javax.inject.Named;

public final class d
  extends ba<c.b>
  implements c.a
{
  private final f c;
  private final a d;
  
  @Inject
  public d(@Named("UI") f paramf, a parama)
  {
    super(paramf);
    c = paramf;
    d = parama;
  }
  
  public final void a()
  {
    c.b localb = (c.b)b;
    if (localb != null)
    {
      localb.c();
      return;
    }
  }
  
  public final void a(com.truecaller.voip.incall.b.a parama)
  {
    k.b(parama, "binderView");
    j.a(this, parama.b(), (m)new d.a(this, parama, null));
  }
  
  public final void a(com.truecaller.voip.incoming.b.a parama)
  {
    k.b(parama, "binderPresenter");
    j.a(this, parama.b(), (m)new d.b(this, null));
  }
  
  public final void b()
  {
    Object localObject = (c.b)b;
    if (localObject != null)
    {
      localObject = ((c.b)localObject).getServiceType();
      if (localObject != null)
      {
        switch (e.c[localObject.ordinal()])
        {
        default: 
          throw new l();
        case 2: 
          localObject = (c.b)b;
          if (localObject != null) {
            ((c.b)localObject).e();
          }
          return;
        }
        localObject = (c.b)b;
        if (localObject != null) {
          ((c.b)localObject).d();
        }
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.ui.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */