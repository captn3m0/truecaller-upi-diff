package com.truecaller.voip.incall.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build.VERSION;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Chronometer;
import android.widget.TextView;
import c.g;
import c.g.a.a;
import c.g.b.k;
import c.u;
import com.truecaller.utils.extensions.t;
import com.truecaller.voip.R.attr;
import com.truecaller.voip.R.color;
import com.truecaller.voip.R.layout;
import com.truecaller.voip.R.string;
import com.truecaller.voip.incall.VoipService;
import com.truecaller.voip.incoming.IncomingVoipService;
import com.truecaller.voip.incoming.ui.IncomingVoipActivity;
import com.truecaller.voip.j;
import com.truecaller.voip.j.a;
import javax.inject.Inject;

public final class VoipInAppNotificationView
  extends ConstraintLayout
  implements c.b
{
  @Inject
  public c.a l;
  private final c.f m = g.a((a)new VoipInAppNotificationView.b(this));
  private final c.f n = g.a((a)new VoipInAppNotificationView.a(this));
  private ServiceType o;
  private final VoipInAppNotificationView.c p;
  
  public VoipInAppNotificationView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private VoipInAppNotificationView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, (byte)0);
    paramAttributeSet = j.a;
    j.a.a().a(this);
    View.inflate(paramContext, R.layout.view_voip_in_app_notification, (ViewGroup)this);
    setBackgroundColor(android.support.v4.content.b.f.b(paramContext.getResources(), R.color.voip_in_app_notification_background_color, null));
    t.b(this);
    setOnClickListener((View.OnClickListener)new VoipInAppNotificationView.1(this));
    p = new VoipInAppNotificationView.c(this);
  }
  
  private final void g()
  {
    getContext().bindService(new Intent(getContext(), VoipService.class), (ServiceConnection)p, 0);
    getContext().bindService(new Intent(getContext(), IncomingVoipService.class), (ServiceConnection)p, 0);
  }
  
  private final Chronometer getChronometer()
  {
    return (Chronometer)n.b();
  }
  
  private final TextView getNameTextView()
  {
    return (TextView)m.b();
  }
  
  public final void a(String paramString, long paramLong)
  {
    k.b(paramString, "name");
    t.a(this);
    f();
    getNameTextView().setText((CharSequence)paramString);
    t.a((View)getChronometer());
    getChronometer().setBase(paramLong);
    getChronometer().start();
  }
  
  public final void ai_()
  {
    t.a(this);
    f();
    t.b((View)getChronometer());
    getNameTextView().setText((CharSequence)getContext().getString(R.string.voip_in_app_notification_outgoing_call));
  }
  
  public final void b()
  {
    t.a(this);
    f();
    t.b((View)getChronometer());
    getNameTextView().setText((CharSequence)getContext().getString(R.string.voip_in_app_notification_incoming_call));
  }
  
  public final void c()
  {
    t.b(this);
    if ((Build.VERSION.SDK_INT >= 21) && ((getContext() instanceof Activity)))
    {
      Object localObject = getContext();
      if (localObject != null)
      {
        localObject = ((Activity)localObject).getWindow();
        k.a(localObject, "(context as Activity).window");
        ((Window)localObject).setStatusBarColor(com.truecaller.utils.ui.b.a(getContext(), R.attr.theme_statusBarColor));
      }
      else
      {
        throw new u("null cannot be cast to non-null type android.app.Activity");
      }
    }
    getChronometer().stop();
  }
  
  public final void d()
  {
    getContext().startActivity(new Intent(getContext(), VoipActivity.class));
  }
  
  public final void e()
  {
    getContext().startActivity(new Intent(getContext(), IncomingVoipActivity.class));
  }
  
  public final void f()
  {
    if (Build.VERSION.SDK_INT < 21) {
      return;
    }
    if ((getContext() instanceof Activity))
    {
      Object localObject = getContext();
      if (localObject != null)
      {
        localObject = ((Activity)localObject).getWindow();
        k.a(localObject, "(context as Activity).window");
        ((Window)localObject).setStatusBarColor(android.support.v4.content.b.c(getContext(), R.color.voip_in_app_notification_status_bar_color));
        return;
      }
      throw new u("null cannot be cast to non-null type android.app.Activity");
    }
  }
  
  public final c.a getPresenter()
  {
    c.a locala = l;
    if (locala == null) {
      k.a("presenter");
    }
    return locala;
  }
  
  public final ServiceType getServiceType()
  {
    return o;
  }
  
  protected final void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    g();
    c.a locala = l;
    if (locala == null) {
      k.a("presenter");
    }
    locala.a(this);
  }
  
  protected final void onDetachedFromWindow()
  {
    c.a locala = l;
    if (locala == null) {
      k.a("presenter");
    }
    locala.y_();
    getContext().unbindService((ServiceConnection)p);
    super.onDetachedFromWindow();
  }
  
  public final void setPresenter(c.a parama)
  {
    k.b(parama, "<set-?>");
    l = parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.incall.ui.VoipInAppNotificationView
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */