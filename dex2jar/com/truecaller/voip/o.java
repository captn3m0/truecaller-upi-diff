package com.truecaller.voip;

import android.content.Context;
import c.d.f;
import c.g.a.m;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.UnmutedException.d;
import com.truecaller.tcpermissions.l;
import com.truecaller.voip.incoming.missed.MissedVoipCallsWorker;
import com.truecaller.voip.incoming.missed.MissedVoipCallsWorker.a;
import com.truecaller.voip.manager.rtm.i;
import com.truecaller.voip.util.VoipAnalyticsCallDirection;
import com.truecaller.voip.util.VoipAnalyticsState;
import com.truecaller.voip.util.ac;
import com.truecaller.voip.util.an;
import com.truecaller.voip.util.aq;
import com.truecaller.voip.util.n;
import com.truecaller.voip.util.y;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.e;

public final class o
  implements d, ag
{
  private final f b;
  private final f c;
  private final Context d;
  private final l e;
  private final com.truecaller.tcpermissions.o f;
  private final k g;
  private final com.truecaller.voip.manager.h h;
  private final dagger.a<com.truecaller.voip.manager.rtm.h> i;
  private final dagger.a<i> j;
  private final dagger.a<y> k;
  private final dagger.a<com.truecaller.voip.db.a> l;
  private final dagger.a<aq> m;
  private final com.truecaller.voip.util.o n;
  private final ac o;
  private final an p;
  
  @Inject
  public o(@Named("UI") f paramf1, @Named("IO") f paramf2, Context paramContext, l paraml, com.truecaller.tcpermissions.o paramo, k paramk, com.truecaller.voip.manager.h paramh, dagger.a<com.truecaller.voip.manager.rtm.h> parama, dagger.a<i> parama1, dagger.a<y> parama2, dagger.a<com.truecaller.voip.db.a> parama3, dagger.a<aq> parama4, com.truecaller.voip.util.o paramo1, ac paramac, an paraman)
  {
    b = paramf1;
    c = paramf2;
    d = paramContext;
    e = paraml;
    f = paramo;
    g = paramk;
    h = paramh;
    i = parama;
    j = parama1;
    k = parama2;
    l = parama3;
    m = parama4;
    n = paramo1;
    o = paramac;
    p = paraman;
  }
  
  public final f V_()
  {
    return b;
  }
  
  public final void a(af paramaf)
  {
    c.g.b.k.b(paramaf, "notification");
    "New voip push notification is received. Sender id ".concat(String.valueOf(paramaf));
    if (!a()) {
      return;
    }
    if (c == null)
    {
      localObject = new StringBuilder("Invalid voip notification. Sender id is null. Action: ");
      ((StringBuilder)localObject).append(b);
      ((StringBuilder)localObject).append('.');
      AssertionUtil.reportThrowableButNeverCrash((Throwable)new UnmutedException.d(((StringBuilder)localObject).toString()));
      return;
    }
    Object localObject = b;
    if (localObject == null) {
      return;
    }
    if (((String)localObject).hashCode() != 3625376) {
      return;
    }
    if (((String)localObject).equals("voip"))
    {
      localObject = h;
      if (localObject != null) {
        com.truecaller.voip.util.o.a.a(n, new n(VoipAnalyticsCallDirection.INCOMING, (String)localObject, null, 60), VoipAnalyticsState.WAKE_UP_RECEIVED);
      }
      e.b(this, c, (m)new o.a(this, paramaf, null), 2);
    }
  }
  
  public final void a(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "number");
    c.g.b.k.b(paramString2, "analyticsContext");
    if (!a()) {
      return;
    }
    e.b(this, null, (m)new o.c(this, paramString2, paramString1, null), 3);
  }
  
  public final boolean a()
  {
    return g.a();
  }
  
  public final boolean a(String paramString)
  {
    return p.c(paramString);
  }
  
  public final void b()
  {
    h.a();
  }
  
  public final void b(String paramString)
  {
    h.b();
    e.b(this, null, (m)new o.b(this, paramString, null), 3);
  }
  
  public final void d()
  {
    if (!a()) {
      return;
    }
    MissedVoipCallsWorker.a locala = MissedVoipCallsWorker.e;
    MissedVoipCallsWorker.a.a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */