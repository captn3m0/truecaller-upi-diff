package com.truecaller.voip;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import c.g.b.k;
import com.truecaller.notifications.MissedCallsNotificationActionReceiver;
import com.truecaller.voip.util.af;

public final class s
  implements af
{
  private final Context a;
  
  public s(Context paramContext)
  {
    a = paramContext;
  }
  
  public final PendingIntent a()
  {
    Object localObject = new Intent(a, MissedCallsNotificationActionReceiver.class).setAction("com.truecaller.OPEN_APP");
    localObject = PendingIntent.getBroadcast(a, 2131364149, (Intent)localObject, 134217728);
    k.a(localObject, "PendingIntent.getBroadca…CURRENT\n                )");
    k.a(localObject, "Intent(context, MissedCa…          )\n            }");
    return (PendingIntent)localObject;
  }
  
  public final PendingIntent a(long paramLong)
  {
    Object localObject = new Intent(a, MissedCallsNotificationActionReceiver.class).setAction("com.truecaller.CLEAR_MISSED_CALLS").putExtra("lastTimestamp", paramLong);
    if (paramLong != 0L) {
      ((Intent)localObject).putExtra("lastTimestamp", paramLong);
    }
    localObject = PendingIntent.getBroadcast(a, 2131364147, (Intent)localObject, 134217728);
    k.a(localObject, "PendingIntent.getBroadca…CURRENT\n                )");
    k.a(localObject, "Intent(context, MissedCa…          )\n            }");
    return (PendingIntent)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.s
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */