package com.truecaller.voip;

import android.content.Context;
import c.g.b.k;
import c.u;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.presence.c;
import com.truecaller.voip.util.l;

public final class b
  implements l
{
  private final Context a;
  
  public b(Context paramContext)
  {
    a = paramContext;
  }
  
  public final boolean a()
  {
    Object localObject = a.getApplicationContext();
    if (localObject != null)
    {
      localObject = ((bk)localObject).a();
      k.a(localObject, "(context.applicationCont…GraphHolder).objectsGraph");
      localObject = ((bp)localObject).ae();
      k.a(localObject, "graph.presenceManager()");
      Boolean localBoolean = (Boolean)((c)((f)localObject).a()).a().d();
      localObject = localBoolean;
      if (localBoolean == null) {
        localObject = Boolean.FALSE;
      }
      k.a(localObject, "presenceManager.tell().r…Settings().get() ?: false");
      return ((Boolean)localObject).booleanValue();
    }
    throw new u("null cannot be cast to non-null type com.truecaller.GraphHolder");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */