package com.truecaller.voip;

import android.content.Intent;
import android.support.v4.app.f;
import com.truecaller.data.entity.Contact;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.old.data.entity.Notification;
import java.util.List;

public abstract interface ai
{
  public abstract void a(Intent paramIntent);
  
  public abstract void a(Contact paramContact, e parame);
  
  public abstract void a(Participant paramParticipant, e parame);
  
  public abstract void a(Notification paramNotification, long paramLong);
  
  public abstract void a(List<String> paramList, ae paramae);
  
  public abstract boolean a(f paramf, Contact paramContact, String paramString);
  
  public abstract boolean a(String paramString1, String paramString2);
}

/* Location:
 * Qualified Name:     com.truecaller.voip.ai
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */