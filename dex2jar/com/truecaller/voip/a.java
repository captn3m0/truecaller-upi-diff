package com.truecaller.voip;

import android.content.Context;
import com.truecaller.voip.callconnection.VoipCallConnectionService;
import com.truecaller.voip.incall.VoipService;
import com.truecaller.voip.incall.ui.VoipInAppNotificationView;
import com.truecaller.voip.incoming.IncomingVoipService;
import com.truecaller.voip.incoming.blocked.VoipBlockedCallsWorker;
import com.truecaller.voip.incoming.missed.MissedVoipCallMessageBroadcast;
import com.truecaller.voip.incoming.missed.MissedVoipCallsWorker;
import com.truecaller.voip.manager.j;
import com.truecaller.voip.util.ai;
import com.truecaller.voip.util.aj;
import com.truecaller.voip.util.ak;
import com.truecaller.voip.util.am;
import com.truecaller.voip.util.ao;
import com.truecaller.voip.util.ap;
import com.truecaller.voip.util.aq;
import com.truecaller.voip.util.ar;
import com.truecaller.voip.util.as;
import com.truecaller.voip.util.av;
import com.truecaller.voip.util.p;
import io.agora.rtm.RtmClient;
import javax.inject.Provider;

public final class a
  implements c
{
  private Provider<p> A;
  private Provider<com.truecaller.voip.manager.rtm.c> B;
  private Provider<RtmClient> C;
  private Provider<ar> D;
  private Provider<aq> E;
  private Provider<com.truecaller.voip.manager.rtm.a> F;
  private Provider<com.truecaller.voip.manager.rtm.h> G;
  private Provider<com.truecaller.voip.callconnection.d> H;
  private Provider<com.truecaller.common.h.u> I;
  private Provider<com.truecaller.voip.callconnection.g> J;
  private Provider<com.truecaller.voip.callconnection.f> K;
  private Provider<ao> L;
  private Provider<o> M;
  private Provider<d> N;
  private final com.truecaller.common.a b;
  private final com.truecaller.utils.t c;
  private final com.truecaller.analytics.d d;
  private final com.truecaller.notificationchannels.n e;
  private final com.truecaller.tcpermissions.e f;
  private Provider<c.d.f> g;
  private Provider<c.d.f> h;
  private Provider<Context> i;
  private Provider<com.truecaller.tcpermissions.l> j;
  private Provider<com.truecaller.tcpermissions.o> k;
  private Provider<com.truecaller.featuretoggles.e> l;
  private Provider<com.truecaller.common.account.r> m;
  private Provider<com.truecaller.utils.d> n;
  private Provider<l> o;
  private Provider<k> p;
  private Provider<ai> q;
  private Provider<com.truecaller.voip.db.a> r;
  private Provider<com.truecaller.utils.a> s;
  private Provider<com.truecaller.voip.util.ad> t;
  private Provider<com.truecaller.voip.util.ac> u;
  private Provider<com.truecaller.voip.manager.i> v;
  private Provider<com.truecaller.voip.manager.h> w;
  private Provider<com.google.gson.f> x;
  private Provider<com.truecaller.analytics.b> y;
  private Provider<com.truecaller.androidactors.f<com.truecaller.analytics.ae>> z;
  
  private a(com.truecaller.common.a parama, com.truecaller.utils.t paramt, com.truecaller.notificationchannels.n paramn, com.truecaller.tcpermissions.e parame, com.truecaller.analytics.d paramd)
  {
    b = parama;
    c = paramt;
    d = paramd;
    e = paramn;
    f = parame;
    g = new i(parama);
    h = new f(parama);
    i = new d(parama);
    j = new j(parame);
    k = new k(parame);
    l = new e(parama);
    m = new h(parama);
    n = new m(paramt);
    o = m.a(l, m, n);
    p = dagger.a.c.a(o);
    q = aj.a(i);
    r = ab.a(i);
    s = new l(paramt);
    t = com.truecaller.voip.util.ae.a(h, m, ad.a(), r, s);
    u = dagger.a.c.a(t);
    v = j.a(h, p, q, z.a(), u);
    w = dagger.a.c.a(v);
    x = dagger.a.c.a(w.a());
    y = new b(paramd);
    z = new c(paramd);
    A = com.truecaller.voip.util.r.a(h, y, z, v.a());
    B = dagger.a.c.a(com.truecaller.voip.manager.rtm.e.a(g, h, l, i, x, ad.a(), A));
    C = dagger.a.c.a(u.a(B));
    D = as.a(h, ad.a());
    E = dagger.a.c.a(D);
    F = com.truecaller.voip.manager.rtm.b.a(h, C, u, E, B, A);
    G = dagger.a.c.a(F);
    H = com.truecaller.voip.callconnection.e.a(g, i);
    I = new g(parama);
    J = com.truecaller.voip.callconnection.h.a(p, H, I);
    K = dagger.a.c.a(J);
    L = ap.a(l, n, i, p, K);
    M = q.a(g, h, i, j, k, p, w, G, B, x.a(), r, E, A, u, L);
    N = dagger.a.c.a(M);
  }
  
  public static a a()
  {
    return new a((byte)0);
  }
  
  private p f()
  {
    return new p((c.d.f)dagger.a.g.a(b.t(), "Cannot return null from a non-@Nullable component method"), (com.truecaller.analytics.b)dagger.a.g.a(d.c(), "Cannot return null from a non-@Nullable component method"), (com.truecaller.androidactors.f)dagger.a.g.a(d.f(), "Cannot return null from a non-@Nullable component method"), v.b());
  }
  
  private av g()
  {
    return new av((com.truecaller.voip.util.ac)u.get(), v.b());
  }
  
  private com.truecaller.voip.util.x h()
  {
    return new com.truecaller.voip.util.x((Context)dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method"));
  }
  
  private ao i()
  {
    return new ao((com.truecaller.featuretoggles.e)dagger.a.g.a(b.v(), "Cannot return null from a non-@Nullable component method"), (com.truecaller.utils.d)dagger.a.g.a(c.c(), "Cannot return null from a non-@Nullable component method"), (Context)dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method"), (k)p.get(), (com.truecaller.voip.callconnection.f)K.get());
  }
  
  private com.truecaller.voip.util.g j()
  {
    return new com.truecaller.voip.util.g((Context)dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method"));
  }
  
  private com.truecaller.voip.util.t k()
  {
    return new com.truecaller.voip.util.t((Context)dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method"), (com.truecaller.utils.d)dagger.a.g.a(c.c(), "Cannot return null from a non-@Nullable component method"), (com.truecaller.voip.callconnection.f)K.get());
  }
  
  public final void a(VoipCallConnectionService paramVoipCallConnectionService)
  {
    a = ((com.truecaller.voip.callconnection.f)K.get());
  }
  
  public final void a(VoipService paramVoipService)
  {
    a = ((c.d.f)dagger.a.g.a(b.r(), "Cannot return null from a non-@Nullable component method"));
    b = ((c.d.f)dagger.a.g.a(b.t(), "Cannot return null from a non-@Nullable component method"));
    c = new com.truecaller.voip.incall.c((c.d.f)dagger.a.g.a(b.r(), "Cannot return null from a non-@Nullable component method"), (c.d.f)dagger.a.g.a(b.t(), "Cannot return null from a non-@Nullable component method"), new com.truecaller.voip.manager.a((Context)dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method"), (c.d.f)dagger.a.g.a(b.t(), "Cannot return null from a non-@Nullable component method"), (aq)E.get()), (com.truecaller.voip.manager.rtm.i)B.get(), v.b(), g(), (com.truecaller.notificationchannels.b)dagger.a.g.a(e.d(), "Cannot return null from a non-@Nullable component method"), (com.truecaller.utils.a)dagger.a.g.a(c.d(), "Cannot return null from a non-@Nullable component method"), h(), x.b(), new com.truecaller.voip.util.k((Context)dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method"), i()), j(), (com.truecaller.utils.n)dagger.a.g.a(c.g(), "Cannot return null from a non-@Nullable component method"), k(), (com.truecaller.voip.manager.rtm.h)G.get(), f(), i());
    d = h();
  }
  
  public final void a(VoipInAppNotificationView paramVoipInAppNotificationView)
  {
    l = new com.truecaller.voip.incall.ui.d((c.d.f)dagger.a.g.a(b.r(), "Cannot return null from a non-@Nullable component method"), (com.truecaller.utils.a)dagger.a.g.a(c.d(), "Cannot return null from a non-@Nullable component method"));
  }
  
  public final void a(com.truecaller.voip.incall.ui.a parama)
  {
    a = new com.truecaller.voip.incall.ui.g((c.d.f)dagger.a.g.a(b.r(), "Cannot return null from a non-@Nullable component method"), (com.truecaller.utils.a)dagger.a.g.a(c.d(), "Cannot return null from a non-@Nullable component method"), aa.a(), f());
  }
  
  public final void a(IncomingVoipService paramIncomingVoipService)
  {
    a = ((c.d.f)dagger.a.g.a(b.r(), "Cannot return null from a non-@Nullable component method"));
    b = ((c.d.f)dagger.a.g.a(b.t(), "Cannot return null from a non-@Nullable component method"));
    c = new com.truecaller.voip.incoming.c((c.d.f)dagger.a.g.a(b.r(), "Cannot return null from a non-@Nullable component method"), (c.d.f)dagger.a.g.a(b.t(), "Cannot return null from a non-@Nullable component method"), (com.truecaller.voip.manager.rtm.i)B.get(), g(), (com.truecaller.notificationchannels.b)dagger.a.g.a(e.d(), "Cannot return null from a non-@Nullable component method"), h(), x.b(), (com.truecaller.utils.n)dagger.a.g.a(c.g(), "Cannot return null from a non-@Nullable component method"), k(), (com.truecaller.voip.manager.rtm.h)G.get(), j(), f(), i());
    d = h();
  }
  
  public final void a(VoipBlockedCallsWorker paramVoipBlockedCallsWorker)
  {
    b = new com.truecaller.voip.incoming.blocked.c(new com.truecaller.voip.incoming.blocked.d((Context)dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method")), (c.d.f)dagger.a.g.a(b.r(), "Cannot return null from a non-@Nullable component method"), (c.d.f)dagger.a.g.a(b.t(), "Cannot return null from a non-@Nullable component method"));
    c = ((com.truecaller.notificationchannels.b)dagger.a.g.a(e.d(), "Cannot return null from a non-@Nullable component method"));
    d = y.a();
  }
  
  public final void a(MissedVoipCallMessageBroadcast paramMissedVoipCallMessageBroadcast)
  {
    a = ac.a();
  }
  
  public final void a(MissedVoipCallsWorker paramMissedVoipCallsWorker)
  {
    b = new com.truecaller.voip.incoming.missed.d((c.d.f)dagger.a.g.a(b.r(), "Cannot return null from a non-@Nullable component method"), (c.d.f)dagger.a.g.a(b.s(), "Cannot return null from a non-@Nullable component method"), new com.truecaller.voip.incoming.missed.b((Context)dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method")), h());
    c = ((com.truecaller.notificationchannels.b)dagger.a.g.a(e.d(), "Cannot return null from a non-@Nullable component method"));
    d = y.a();
  }
  
  public final void a(com.truecaller.voip.incoming.ui.a parama)
  {
    a = new com.truecaller.voip.incoming.ui.c((c.d.f)dagger.a.g.a(b.r(), "Cannot return null from a non-@Nullable component method"), ac.a(), aa.a(), f());
    b = ((com.truecaller.tcpermissions.l)dagger.a.g.a(f.c(), "Cannot return null from a non-@Nullable component method"));
  }
  
  public final d b()
  {
    return (d)N.get();
  }
  
  public final com.truecaller.voip.db.c c()
  {
    return new com.truecaller.voip.db.d((Context)dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method"));
  }
  
  public final ak d()
  {
    return new am(new ai((Context)dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method")), (k)p.get());
  }
  
  public final com.truecaller.voip.util.o e()
  {
    return f();
  }
  
  public static final class a
  {
    public com.truecaller.common.a a;
    public com.truecaller.utils.t b;
    public com.truecaller.notificationchannels.n c;
    public com.truecaller.tcpermissions.e d;
    public com.truecaller.analytics.d e;
  }
  
  static final class b
    implements Provider<com.truecaller.analytics.b>
  {
    private final com.truecaller.analytics.d a;
    
    b(com.truecaller.analytics.d paramd)
    {
      a = paramd;
    }
  }
  
  static final class c
    implements Provider<com.truecaller.androidactors.f<com.truecaller.analytics.ae>>
  {
    private final com.truecaller.analytics.d a;
    
    c(com.truecaller.analytics.d paramd)
    {
      a = paramd;
    }
  }
  
  static final class d
    implements Provider<Context>
  {
    private final com.truecaller.common.a a;
    
    d(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
  
  static final class e
    implements Provider<com.truecaller.featuretoggles.e>
  {
    private final com.truecaller.common.a a;
    
    e(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
  
  static final class f
    implements Provider<c.d.f>
  {
    private final com.truecaller.common.a a;
    
    f(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
  
  static final class g
    implements Provider<com.truecaller.common.h.u>
  {
    private final com.truecaller.common.a a;
    
    g(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
  
  static final class h
    implements Provider<com.truecaller.common.account.r>
  {
    private final com.truecaller.common.a a;
    
    h(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
  
  static final class i
    implements Provider<c.d.f>
  {
    private final com.truecaller.common.a a;
    
    i(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
  
  static final class j
    implements Provider<com.truecaller.tcpermissions.l>
  {
    private final com.truecaller.tcpermissions.e a;
    
    j(com.truecaller.tcpermissions.e parame)
    {
      a = parame;
    }
  }
  
  static final class k
    implements Provider<com.truecaller.tcpermissions.o>
  {
    private final com.truecaller.tcpermissions.e a;
    
    k(com.truecaller.tcpermissions.e parame)
    {
      a = parame;
    }
  }
  
  static final class l
    implements Provider<com.truecaller.utils.a>
  {
    private final com.truecaller.utils.t a;
    
    l(com.truecaller.utils.t paramt)
    {
      a = paramt;
    }
  }
  
  static final class m
    implements Provider<com.truecaller.utils.d>
  {
    private final com.truecaller.utils.t a;
    
    m(com.truecaller.utils.t paramt)
    {
      a = paramt;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */