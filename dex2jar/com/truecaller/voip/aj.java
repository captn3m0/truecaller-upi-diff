package com.truecaller.voip;

import android.content.Context;
import android.content.Intent;
import c.g.b.k;
import com.truecaller.callhistory.a;
import com.truecaller.common.account.r;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.featuretoggles.b;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.old.data.access.Settings;
import com.truecaller.old.data.entity.Notification;
import com.truecaller.voip.db.VoipAvailability;
import com.truecaller.voip.db.c;
import com.truecaller.voip.util.VoipAnalyticsFailedCallAction;
import com.truecaller.voip.util.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;

public final class aj
  implements ai, ag
{
  final c.d.f a;
  final c b;
  final o c;
  private final c.d.f d;
  private final Context e;
  private final d f;
  private final com.truecaller.utils.i g;
  private final com.truecaller.common.h.u h;
  private final com.truecaller.featuretoggles.e i;
  private final com.truecaller.androidactors.f<a> j;
  private final r k;
  
  @Inject
  public aj(@Named("UI") c.d.f paramf1, @Named("IO") c.d.f paramf2, Context paramContext, d paramd, c paramc, com.truecaller.utils.i parami, com.truecaller.common.h.u paramu, o paramo, com.truecaller.featuretoggles.e parame, com.truecaller.androidactors.f<a> paramf, r paramr)
  {
    a = paramf1;
    d = paramf2;
    e = paramContext;
    f = paramd;
    b = paramc;
    g = parami;
    h = paramu;
    c = paramo;
    i = parame;
    j = paramf;
    k = paramr;
  }
  
  public final c.d.f V_()
  {
    return a;
  }
  
  final List<VoipAvailability> a(List<String> paramList)
  {
    String str1 = k.b();
    Object localObject1 = (Iterable)paramList;
    paramList = (Collection)new ArrayList(c.a.m.a((Iterable)localObject1, 10));
    localObject1 = ((Iterable)localObject1).iterator();
    while (((Iterator)localObject1).hasNext()) {
      paramList.add(al.a((String)((Iterator)localObject1).next()));
    }
    localObject1 = (Iterable)paramList;
    paramList = (Collection)new ArrayList();
    localObject1 = ((Iterable)localObject1).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      Object localObject2 = ((Iterator)localObject1).next();
      String str2 = (String)localObject2;
      if (!k.a(str1, h.b(str2))) {
        paramList.add(localObject2);
      }
    }
    paramList = ((Collection)paramList).toArray(new String[0]);
    if (paramList != null)
    {
      paramList = (String[])paramList;
      return b.a(paramList);
    }
    throw new c.u("null cannot be cast to non-null type kotlin.Array<T>");
  }
  
  public final void a(Intent paramIntent)
  {
    k.b(paramIntent, "intent");
    if (f.a())
    {
      if (!paramIntent.hasExtra("com.truecaller.datamanager.EXTRA_PRESENCE")) {
        return;
      }
      paramIntent = paramIntent.getSerializableExtra("com.truecaller.datamanager.EXTRA_PRESENCE");
      if (paramIntent != null)
      {
        paramIntent = (ArrayList)paramIntent;
        kotlinx.coroutines.e.b(this, d, (c.g.a.m)new aj.d(this, paramIntent, null), 2);
        return;
      }
      throw new c.u("null cannot be cast to non-null type kotlin.collections.ArrayList<com.truecaller.presence.Presence> /* = java.util.ArrayList<com.truecaller.presence.Presence> */");
    }
  }
  
  public final void a(Contact paramContact, e parame)
  {
    k.b(paramContact, "contact");
    k.b(parame, "listener");
    if (!f.a())
    {
      parame.onVoipAvailabilityLoaded(false);
      return;
    }
    kotlinx.coroutines.e.b(this, d, (c.g.a.m)new aj.b(this, paramContact, parame, null), 2);
  }
  
  public final void a(Participant paramParticipant, e parame)
  {
    k.b(paramParticipant, "participant");
    k.b(parame, "listener");
    if (!f.a())
    {
      parame.onVoipAvailabilityLoaded(false);
      return;
    }
    kotlinx.coroutines.e.b(this, d, (c.g.a.m)new aj.c(this, paramParticipant, parame, null), 2);
  }
  
  public final void a(Notification paramNotification, long paramLong)
  {
    k.b(paramNotification, "notification");
    Object localObject1 = Settings.b("qa_voip_notification_rtm_token");
    k.a(localObject1, "it");
    boolean bool = c.n.m.a((CharSequence)localObject1);
    Object localObject3 = null;
    if (bool) {
      localObject1 = null;
    }
    Object localObject2 = localObject1;
    if (localObject1 == null) {
      localObject2 = paramNotification.k();
    }
    String str1 = paramNotification.h();
    String str2 = paramNotification.i();
    String str3 = paramNotification.a();
    String str4 = paramNotification.l();
    localObject1 = paramNotification.n();
    if (localObject1 != null) {
      localObject1 = c.n.m.b((String)localObject1);
    } else {
      localObject1 = null;
    }
    String str5 = paramNotification.j();
    String str6 = paramNotification.m();
    paramNotification = (Notification)localObject3;
    if (str6 != null) {
      paramNotification = c.n.m.d(str6);
    }
    paramNotification = new af(paramLong, str1, str2, str3, (String)localObject2, str4, (Integer)localObject1, str5, paramNotification);
    f.a(paramNotification);
  }
  
  final void a(String paramString, Contact paramContact)
  {
    paramContact = paramContact.A();
    k.a(paramContact, "contact.numbers");
    paramContact = (Number)c.a.m.e(paramContact);
    if (paramContact != null)
    {
      paramContact = paramContact.a();
      if (paramContact != null)
      {
        String str = h.b(paramContact);
        if (str != null) {
          paramContact = str;
        }
        c.a(paramString, paramContact);
        return;
      }
    }
  }
  
  public final void a(List<String> paramList, ae paramae)
  {
    k.b(paramList, "normalizedNumbers");
    k.b(paramae, "listener");
    kotlinx.coroutines.e.b(this, d, (c.g.a.m)new aj.a(this, paramList, paramae, null), 2);
  }
  
  public final boolean a(android.support.v4.app.f paramf, Contact paramContact, String paramString)
  {
    k.b(paramString, "analyticsContext");
    if (paramContact == null) {
      return false;
    }
    if (!g.a())
    {
      com.truecaller.utils.extensions.i.a(e, 2131888998, null, 0, 6);
      a(paramString, paramContact);
      c.a(paramString, VoipAnalyticsFailedCallAction.OFFLINE);
      return false;
    }
    kotlinx.coroutines.e.b(this, d, (c.g.a.m)new aj.e(this, paramContact, paramString, paramf, null), 2);
    return true;
  }
  
  public final boolean a(String paramString1, String paramString2)
  {
    k.b(paramString1, "number");
    k.b(paramString2, "analyticsContext");
    String str = h.b(paramString1);
    if (str != null) {
      paramString1 = str;
    }
    c.a(paramString2, paramString1);
    if (!g.a())
    {
      com.truecaller.utils.extensions.i.a(e, 2131888998, null, 0, 6);
      c.a(paramString2, VoipAnalyticsFailedCallAction.OFFLINE);
      return false;
    }
    if (k.a(k.b(), paramString1)) {
      return false;
    }
    f.a(paramString1, paramString2);
    if (i.e().a()) {
      ((a)j.a()).b(paramString1);
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.aj
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */