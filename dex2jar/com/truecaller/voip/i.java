package com.truecaller.voip;

import android.content.Context;
import android.net.Uri;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.u;
import c.x;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.data.entity.Contact;
import com.truecaller.filters.FilterManager;
import com.truecaller.log.AssertionUtil;
import com.truecaller.network.search.j;
import com.truecaller.network.search.l;
import com.truecaller.network.search.n;
import com.truecaller.voip.util.aa;
import java.io.IOException;
import java.util.UUID;
import javax.inject.Inject;
import kotlinx.coroutines.g;

public final class i
  implements aa
{
  private final Context a;
  
  @Inject
  public i(Context paramContext)
  {
    a = paramContext;
  }
  
  private final FilterManager b()
  {
    FilterManager localFilterManager = a().P();
    c.g.b.k.a(localFilterManager, "graph.filterManager()");
    return localFilterManager;
  }
  
  final bp a()
  {
    Object localObject = a.getApplicationContext();
    if (localObject != null)
    {
      localObject = ((bk)localObject).a();
      c.g.b.k.a(localObject, "(context.applicationCont…GraphHolder).objectsGraph");
      return (bp)localObject;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.GraphHolder");
  }
  
  public final Object a(final String paramString, c<? super com.truecaller.voip.util.ag> paramc)
  {
    c.d.f localf = a().bm();
    c.g.b.k.a(localf, "graph.asyncCoroutineContext()");
    return g.a(localf, (m)new a(this, paramString, null), paramc);
  }
  
  @c.d.b.a.f(b="VoipCallerInfoProviderImpl.kt", c={}, d="invokeSuspend", e="com.truecaller.voip.VoipCallerInfoProviderImpl$searchCaller$2")
  static final class a
    extends c.d.b.a.k
    implements m<kotlinx.coroutines.ag, c<? super com.truecaller.voip.util.ag>, Object>
  {
    int a;
    private kotlinx.coroutines.ag d;
    
    a(i parami, String paramString, c paramc)
    {
      super(paramc);
    }
    
    public final c<x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new a(b, paramString, paramc);
      d = ((kotlinx.coroutines.ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      Object localObject = a.a;
      if (a == 0) {
        if ((paramObject instanceof o.b)) {}
      }
      for (;;)
      {
        try
        {
          paramObject = b.a().u();
          c.g.b.k.a(paramObject, "graph.searchManager()");
          localObject = UUID.randomUUID();
          c.g.b.k.a(localObject, "UUID.randomUUID()");
          paramObject = ((l)paramObject).a((UUID)localObject, "voip").b().a(paramString).a().a(4).f();
          if (paramObject != null)
          {
            localObject = ((n)paramObject).a();
            if (localObject != null)
            {
              c.g.b.k.a(localObject, "contact");
              String str = ((Contact)localObject).s();
              c.g.b.k.a(str, "contact.displayNameOrNumber");
              paramObject = ((Contact)localObject).a(false);
              if (paramObject == null) {
                break label189;
              }
              paramObject = ((Uri)paramObject).toString();
              paramObject = new com.truecaller.voip.util.ag(str, (String)paramObject, paramString, i.a(b, (Contact)localObject), i.b(b, (Contact)localObject), ((Contact)localObject).Z());
              return paramObject;
            }
          }
        }
        catch (IOException paramObject)
        {
          AssertionUtil.reportThrowableButNeverCrash((Throwable)paramObject);
        }
        return null;
        throw a;
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        label189:
        paramObject = null;
      }
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((a)a(paramObject1, (c)paramObject2)).a(x.a);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */