package com.truecaller.voip;

import com.truecaller.voip.util.aa;
import com.truecaller.voip.util.at;
import com.truecaller.voip.util.l;
import com.truecaller.voip.util.y;
import com.truecaller.voip.util.z;

public abstract interface d
{
  public static final a a = a.a;
  
  public abstract void a(af paramaf);
  
  public abstract void a(String paramString1, String paramString2);
  
  public abstract boolean a();
  
  public abstract boolean a(String paramString);
  
  public abstract void b();
  
  public abstract void b(String paramString);
  
  public abstract void d();
  
  public static final class a
  {
    private static y b;
    private static com.truecaller.voip.util.af c;
    private static aa d;
    private static z e;
    private static l f;
    private static at g;
    
    public static y a()
    {
      return b;
    }
    
    public static void a(aa paramaa)
    {
      d = paramaa;
    }
    
    public static void a(com.truecaller.voip.util.af paramaf)
    {
      c = paramaf;
    }
    
    public static void a(at paramat)
    {
      g = paramat;
    }
    
    public static void a(l paraml)
    {
      f = paraml;
    }
    
    public static void a(y paramy)
    {
      b = paramy;
    }
    
    public static void a(z paramz)
    {
      e = paramz;
    }
    
    public static com.truecaller.voip.util.af b()
    {
      return c;
    }
    
    public static aa c()
    {
      return d;
    }
    
    public static z d()
    {
      return e;
    }
    
    public static l e()
    {
      return f;
    }
    
    public static at f()
    {
      return g;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */