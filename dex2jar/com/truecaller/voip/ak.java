package com.truecaller.voip;

import android.content.Context;
import com.truecaller.callhistory.a;
import com.truecaller.common.account.r;
import com.truecaller.common.h.u;
import com.truecaller.featuretoggles.e;
import com.truecaller.utils.i;
import com.truecaller.voip.db.c;
import com.truecaller.voip.util.o;
import javax.inject.Provider;

public final class ak
  implements dagger.a.d<aj>
{
  private final Provider<c.d.f> a;
  private final Provider<c.d.f> b;
  private final Provider<Context> c;
  private final Provider<d> d;
  private final Provider<c> e;
  private final Provider<i> f;
  private final Provider<u> g;
  private final Provider<o> h;
  private final Provider<e> i;
  private final Provider<com.truecaller.androidactors.f<a>> j;
  private final Provider<r> k;
  
  private ak(Provider<c.d.f> paramProvider1, Provider<c.d.f> paramProvider2, Provider<Context> paramProvider, Provider<d> paramProvider3, Provider<c> paramProvider4, Provider<i> paramProvider5, Provider<u> paramProvider6, Provider<o> paramProvider7, Provider<e> paramProvider8, Provider<com.truecaller.androidactors.f<a>> paramProvider9, Provider<r> paramProvider10)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
    k = paramProvider10;
  }
  
  public static ak a(Provider<c.d.f> paramProvider1, Provider<c.d.f> paramProvider2, Provider<Context> paramProvider, Provider<d> paramProvider3, Provider<c> paramProvider4, Provider<i> paramProvider5, Provider<u> paramProvider6, Provider<o> paramProvider7, Provider<e> paramProvider8, Provider<com.truecaller.androidactors.f<a>> paramProvider9, Provider<r> paramProvider10)
  {
    return new ak(paramProvider1, paramProvider2, paramProvider, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.ak
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */