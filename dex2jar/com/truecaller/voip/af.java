package com.truecaller.voip;

import c.g.b.k;

public final class af
{
  final long a;
  final String b;
  final String c;
  final String d;
  final String e;
  final String f;
  final Integer g;
  final String h;
  final Long i;
  
  public af(long paramLong, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, Integer paramInteger, String paramString6, Long paramLong1)
  {
    a = paramLong;
    b = paramString1;
    c = paramString2;
    d = paramString3;
    e = paramString4;
    f = paramString5;
    g = paramInteger;
    h = paramString6;
    i = paramLong1;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      if ((paramObject instanceof af))
      {
        paramObject = (af)paramObject;
        int j;
        if (a == a) {
          j = 1;
        } else {
          j = 0;
        }
        if ((j != 0) && (k.a(b, b)) && (k.a(c, c)) && (k.a(d, d)) && (k.a(e, e)) && (k.a(f, f)) && (k.a(g, g)) && (k.a(h, h)) && (k.a(i, i))) {
          return true;
        }
      }
      return false;
    }
    return true;
  }
  
  public final int hashCode()
  {
    long l = a;
    int i5 = (int)(l ^ l >>> 32);
    Object localObject = b;
    int i4 = 0;
    int j;
    if (localObject != null) {
      j = localObject.hashCode();
    } else {
      j = 0;
    }
    localObject = c;
    int k;
    if (localObject != null) {
      k = localObject.hashCode();
    } else {
      k = 0;
    }
    localObject = d;
    int m;
    if (localObject != null) {
      m = localObject.hashCode();
    } else {
      m = 0;
    }
    localObject = e;
    int n;
    if (localObject != null) {
      n = localObject.hashCode();
    } else {
      n = 0;
    }
    localObject = f;
    int i1;
    if (localObject != null) {
      i1 = localObject.hashCode();
    } else {
      i1 = 0;
    }
    localObject = g;
    int i2;
    if (localObject != null) {
      i2 = localObject.hashCode();
    } else {
      i2 = 0;
    }
    localObject = h;
    int i3;
    if (localObject != null) {
      i3 = localObject.hashCode();
    } else {
      i3 = 0;
    }
    localObject = i;
    if (localObject != null) {
      i4 = localObject.hashCode();
    }
    return (((((((i5 * 31 + j) * 31 + k) * 31 + m) * 31 + n) * 31 + i1) * 31 + i2) * 31 + i3) * 31 + i4;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("VoipPushNotification(sentTime=");
    localStringBuilder.append(a);
    localStringBuilder.append(", action=");
    localStringBuilder.append(b);
    localStringBuilder.append(", senderId=");
    localStringBuilder.append(c);
    localStringBuilder.append(", senderNumber=");
    localStringBuilder.append(d);
    localStringBuilder.append(", rtmToken=");
    localStringBuilder.append(e);
    localStringBuilder.append(", rtcToken=");
    localStringBuilder.append(f);
    localStringBuilder.append(", rtcTokenUid=");
    localStringBuilder.append(g);
    localStringBuilder.append(", channelId=");
    localStringBuilder.append(h);
    localStringBuilder.append(", idExpiryEpochSeconds=");
    localStringBuilder.append(i);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.af
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */