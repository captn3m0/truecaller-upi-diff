package com.truecaller.voip.util;

import c.d.f;
import com.truecaller.voip.api.a;
import dagger.a.d;
import javax.inject.Provider;

public final class as
  implements d<ar>
{
  private final Provider<f> a;
  private final Provider<a> b;
  
  private as(Provider<f> paramProvider, Provider<a> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static as a(Provider<f> paramProvider, Provider<a> paramProvider1)
  {
    return new as(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.as
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */