package com.truecaller.voip.util;

import android.content.Context;
import c.g.b.k;
import com.truecaller.utils.a.a;
import javax.inject.Inject;

public final class ai
  extends a
  implements ah
{
  private final int b = 1;
  private final String c = "voip_settings";
  
  @Inject
  public ai(Context paramContext)
  {
    super(paramContext);
  }
  
  public final int a()
  {
    return b;
  }
  
  public final void a(int paramInt, Context paramContext)
  {
    k.b(paramContext, "context");
    if (paramInt <= 0) {
      d("ownVoipId");
    }
  }
  
  public final String b()
  {
    return c;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.ai
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */