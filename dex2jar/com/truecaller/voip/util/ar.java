package com.truecaller.voip.util;

import c.d.c;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import c.n;
import c.t;
import com.truecaller.log.AssertionUtil;
import com.truecaller.voip.api.RtcTokenDto;
import com.truecaller.voip.api.a;
import e.b;
import e.r;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.g;

public final class ar
  implements aq
{
  String a;
  n<String, RtcTokenDto> b;
  final a c;
  private final f d;
  
  @Inject
  public ar(@Named("IO") f paramf, a parama)
  {
    d = paramf;
    c = parama;
  }
  
  static <T> T a(b<T> paramb)
  {
    try
    {
      paramb = paramb.c();
      if (paramb != null)
      {
        paramb = paramb.e();
        return paramb;
      }
    }
    catch (Exception paramb)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramb);
    }
    return null;
  }
  
  public final Object a(c<? super String> paramc)
  {
    return g.a(d, (m)new ar.b(this, null), paramc);
  }
  
  public final Object a(String paramString, c<? super RtcTokenDto> paramc)
  {
    return g.a(d, (m)new ar.a(this, paramString, null), paramc);
  }
  
  public final void a()
  {
    a = null;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "token");
    a = paramString;
  }
  
  public final void a(String paramString, RtcTokenDto paramRtcTokenDto)
  {
    k.b(paramString, "channelId");
    k.b(paramRtcTokenDto, "token");
    b = t.a(paramString, paramRtcTokenDto);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.ar
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */