package com.truecaller.voip.util;

import android.content.Context;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import c.g.a.b;
import javax.inject.Inject;
import kotlinx.coroutines.a.y;

public final class k
  implements j
{
  final Context a;
  final an b;
  
  @Inject
  public k(Context paramContext, an paraman)
  {
    a = paramContext;
    b = paraman;
  }
  
  static i a(int paramInt, boolean paramBoolean)
  {
    switch (paramInt)
    {
    default: 
      return null;
    case 2: 
      return (i)new i.b(paramBoolean);
    case 1: 
      return (i)new i.c(paramBoolean);
    }
    return (i)new i.a(paramBoolean);
  }
  
  public final i a()
  {
    boolean bool = b.c(null);
    i locali2 = a(com.truecaller.utils.extensions.i.b(a).getCallState(), bool);
    i locali1 = locali2;
    if (locali2 == null) {
      locali1 = (i)new i.a(bool);
    }
    return locali1;
  }
  
  public final void a(y<? super i> paramy)
  {
    c.g.b.k.b(paramy, "channel");
    k.b localb = new k.b(this, paramy);
    paramy.a((b)new k.a(this, localb));
    com.truecaller.utils.extensions.i.b(a).listen((PhoneStateListener)localb, 32);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */