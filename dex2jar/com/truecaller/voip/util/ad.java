package com.truecaller.voip.util;

import android.database.sqlite.SQLiteException;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.x;
import com.truecaller.log.AssertionUtil;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.g;

public final class ad
  implements ac
{
  final com.truecaller.common.account.r a;
  final com.truecaller.voip.api.a b;
  final com.truecaller.voip.db.a c;
  private final f d;
  private final com.truecaller.utils.a e;
  
  @Inject
  public ad(@Named("IO") f paramf, com.truecaller.common.account.r paramr, com.truecaller.voip.api.a parama, com.truecaller.voip.db.a parama1, com.truecaller.utils.a parama2)
  {
    d = paramf;
    a = paramr;
    b = parama;
    c = parama1;
    e = parama2;
  }
  
  static <T> T a(com.truecaller.voip.db.a parama, c.g.a.b<? super com.truecaller.voip.db.a, ? extends T> paramb)
  {
    try
    {
      parama = paramb.invoke(parama);
      return parama;
    }
    catch (SQLiteException parama)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)parama);
    }
    return null;
  }
  
  static <T> T a(e.b<T> paramb)
  {
    try
    {
      paramb = paramb.c();
      if (paramb != null)
      {
        paramb = paramb.e();
        return paramb;
      }
    }
    catch (Exception paramb)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramb);
    }
    return null;
  }
  
  public final Object a(c<? super String> paramc)
  {
    return g.a(d, (m)new ad.f(this, null), paramc);
  }
  
  public final Object a(String paramString, c<? super String> paramc)
  {
    return g.a(d, (m)new ad.d(this, paramString, null), paramc);
  }
  
  public final Object b(String paramString, c<? super String> paramc)
  {
    return g.a(d, (m)new ad.e(this, paramString, null), paramc);
  }
  
  public final Object c(String paramString, c<? super x> paramc)
  {
    return g.a(d, (m)new ad.c(this, paramString, null), paramc);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.ad
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */