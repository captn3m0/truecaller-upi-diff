package com.truecaller.voip.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.View;
import com.getkeepsafe.taptargetview.b;
import com.getkeepsafe.taptargetview.c;
import com.getkeepsafe.taptargetview.c.a;
import com.truecaller.voip.R.color;
import com.truecaller.voip.R.string;
import javax.inject.Inject;

public final class am
  implements ak
{
  c a;
  private final ah b;
  private final com.truecaller.voip.k c;
  
  @Inject
  public am(ah paramah, com.truecaller.voip.k paramk)
  {
    b = paramah;
    c = paramk;
  }
  
  public final void a(Activity paramActivity, View paramView, al paramal)
  {
    c.g.b.k.b(paramActivity, "activity");
    c.g.b.k.b(paramView, "view");
    c.g.b.k.b(paramal, "callback");
    if (c.a())
    {
      if (b.b("showCaseDisplayed")) {
        return;
      }
      b.b("showCaseDisplayed", true);
      Object localObject = (Context)paramActivity;
      float f = paramView.getWidth() / 2;
      localObject = ((Context)localObject).getResources();
      c.g.b.k.a(localObject, "context.resources");
      int i = (int)(f / (getDisplayMetricsdensityDpi / 160.0F));
      if (i <= 30) {
        i = 30;
      }
      paramal = new am.a(this, paramal);
      if (a())
      {
        localObject = a;
        if (localObject != null) {
          ((c)localObject).a(false);
        }
      }
      a = c.a(paramActivity, b.a(paramView, (CharSequence)paramActivity.getString(R.string.voip_showcase_title, new Object[] { paramActivity.getString(R.string.voip_text) }), (CharSequence)paramActivity.getString(R.string.voip_showcase_message, new Object[] { paramActivity.getString(R.string.voip_text) })).a(R.color.voip_showcase_color).b().b(R.color.voip_showcase_color).c().d().c(R.color.voip_showcase_text_color).a(Typeface.SANS_SERIF).d(R.color.voip_showcase_color).e().f().g().a().e(i), (c.a)paramal);
      return;
    }
  }
  
  public final boolean a()
  {
    c localc = a;
    if (localc != null) {
      return localc.a();
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.am
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */