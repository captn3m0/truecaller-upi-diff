package com.truecaller.voip.util;

import c.g.b.k;

public final class ab
{
  public final String a;
  public final VoipEventType b;
  public final long c;
  public final Long d;
  
  private ab(String paramString, VoipEventType paramVoipEventType, long paramLong, Long paramLong1)
  {
    a = paramString;
    b = paramVoipEventType;
    c = paramLong;
    d = paramLong1;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      if ((paramObject instanceof ab))
      {
        paramObject = (ab)paramObject;
        if ((k.a(a, a)) && (k.a(b, b)))
        {
          int i;
          if (c == c) {
            i = 1;
          } else {
            i = 0;
          }
          if ((i != 0) && (k.a(d, d))) {
            return true;
          }
        }
      }
      return false;
    }
    return true;
  }
  
  public final int hashCode()
  {
    Object localObject = a;
    int k = 0;
    int i;
    if (localObject != null) {
      i = localObject.hashCode();
    } else {
      i = 0;
    }
    localObject = b;
    int j;
    if (localObject != null) {
      j = localObject.hashCode();
    } else {
      j = 0;
    }
    long l = c;
    int m = (int)(l ^ l >>> 32);
    localObject = d;
    if (localObject != null) {
      k = localObject.hashCode();
    }
    return ((i * 31 + j) * 31 + m) * 31 + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("VoipHistoryEvent(number=");
    localStringBuilder.append(a);
    localStringBuilder.append(", type=");
    localStringBuilder.append(b);
    localStringBuilder.append(", duration=");
    localStringBuilder.append(c);
    localStringBuilder.append(", timestamp=");
    localStringBuilder.append(d);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.ab
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */