package com.truecaller.voip.util;

import c.d.f;
import com.truecaller.common.account.r;
import dagger.a.d;
import javax.inject.Provider;

public final class ae
  implements d<ad>
{
  private final Provider<f> a;
  private final Provider<r> b;
  private final Provider<com.truecaller.voip.api.a> c;
  private final Provider<com.truecaller.voip.db.a> d;
  private final Provider<com.truecaller.utils.a> e;
  
  private ae(Provider<f> paramProvider, Provider<r> paramProvider1, Provider<com.truecaller.voip.api.a> paramProvider2, Provider<com.truecaller.voip.db.a> paramProvider3, Provider<com.truecaller.utils.a> paramProvider4)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static ae a(Provider<f> paramProvider, Provider<r> paramProvider1, Provider<com.truecaller.voip.api.a> paramProvider2, Provider<com.truecaller.voip.db.a> paramProvider3, Provider<com.truecaller.utils.a> paramProvider4)
  {
    return new ae(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.ae
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */