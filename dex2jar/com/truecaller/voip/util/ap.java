package com.truecaller.voip.util;

import android.content.Context;
import com.truecaller.featuretoggles.e;
import com.truecaller.voip.callconnection.f;
import com.truecaller.voip.k;
import javax.inject.Provider;

public final class ap
  implements dagger.a.d<ao>
{
  private final Provider<e> a;
  private final Provider<com.truecaller.utils.d> b;
  private final Provider<Context> c;
  private final Provider<k> d;
  private final Provider<f> e;
  
  private ap(Provider<e> paramProvider, Provider<com.truecaller.utils.d> paramProvider1, Provider<Context> paramProvider2, Provider<k> paramProvider3, Provider<f> paramProvider4)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static ap a(Provider<e> paramProvider, Provider<com.truecaller.utils.d> paramProvider1, Provider<Context> paramProvider2, Provider<k> paramProvider3, Provider<f> paramProvider4)
  {
    return new ap(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.ap
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */