package com.truecaller.voip.util;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothProfile.ServiceListener;
import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioAttributes.Builder;
import android.media.AudioFocusRequest.Builder;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build.VERSION;
import c.g.a.m;
import c.g.b.k;
import c.l;
import c.x;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.d;
import com.truecaller.utils.extensions.i;
import com.truecaller.voip.callconnection.f;
import javax.inject.Inject;
import kotlinx.coroutines.a.y;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bn;

public final class t
  implements s
{
  MediaPlayer a;
  final Context b;
  final f c;
  private e d;
  private c e;
  private final d f;
  
  @Inject
  public t(Context paramContext, d paramd, f paramf)
  {
    b = paramContext;
    f = paramd;
    c = paramf;
    if (e == null)
    {
      paramd = new c();
      paramContext = BluetoothAdapter.getDefaultAdapter();
      if (paramContext != null)
      {
        if (!paramContext.isEnabled()) {
          paramContext = null;
        }
        if (paramContext != null) {
          paramContext.getProfileProxy(b, (BluetoothProfile.ServiceListener)paramd, 1);
        }
      }
      e = paramd;
    }
  }
  
  static x a(MediaPlayer paramMediaPlayer)
  {
    Object localObject2 = null;
    Object localObject1 = localObject2;
    if (paramMediaPlayer != null) {}
    for (;;)
    {
      try
      {
        if (!(paramMediaPlayer.isPlaying() ^ true)) {
          break label51;
        }
        localObject1 = localObject2;
        if (paramMediaPlayer != null)
        {
          paramMediaPlayer.start();
          localObject1 = x.a;
        }
      }
      catch (IllegalStateException paramMediaPlayer)
      {
        AssertionUtil.reportThrowableButNeverCrash((Throwable)paramMediaPlayer);
        return x.a;
      }
      return (x)localObject1;
      label51:
      paramMediaPlayer = null;
    }
  }
  
  private final e a(b paramb, AudioManager.OnAudioFocusChangeListener paramOnAudioFocusChangeListener)
  {
    if (Build.VERSION.SDK_INT >= 26)
    {
      switch (u.a[paramb.ordinal()])
      {
      default: 
        throw new l();
      case 2: 
        paramb = i();
        break;
      case 1: 
        paramb = h();
      }
      paramb = new AudioFocusRequest.Builder(4).setOnAudioFocusChangeListener(paramOnAudioFocusChangeListener).setAudioAttributes(paramb).build();
      i.i(b).requestAudioFocus(paramb);
      k.a(paramb, "focusRequest");
      return (e)new t.a(this, paramb);
    }
    int i;
    switch (u.b[paramb.ordinal()])
    {
    default: 
      throw new l();
    case 2: 
      i = 0;
      break;
    case 1: 
      i = 2;
    }
    i.i(b).requestAudioFocus(paramOnAudioFocusChangeListener, i, 4);
    return (e)new t.b(this, paramOnAudioFocusChangeListener);
  }
  
  static x b(MediaPlayer paramMediaPlayer)
  {
    Object localObject2 = null;
    Object localObject1 = localObject2;
    if (paramMediaPlayer != null) {}
    for (;;)
    {
      try
      {
        if (!paramMediaPlayer.isPlaying()) {
          break label49;
        }
        localObject1 = localObject2;
        if (paramMediaPlayer != null)
        {
          paramMediaPlayer.pause();
          localObject1 = x.a;
        }
      }
      catch (IllegalStateException paramMediaPlayer)
      {
        AssertionUtil.reportThrowableButNeverCrash((Throwable)paramMediaPlayer);
        return x.a;
      }
      return (x)localObject1;
      label49:
      paramMediaPlayer = null;
    }
  }
  
  private static x c(MediaPlayer paramMediaPlayer)
  {
    if (paramMediaPlayer != null) {
      try
      {
        paramMediaPlayer.stop();
        paramMediaPlayer = x.a;
        return paramMediaPlayer;
      }
      catch (IllegalStateException paramMediaPlayer)
      {
        AssertionUtil.reportThrowableButNeverCrash((Throwable)paramMediaPlayer);
        return x.a;
      }
    }
    return null;
  }
  
  private final MediaPlayer g()
  {
    try
    {
      MediaPlayer localMediaPlayer = new MediaPlayer();
      if (Build.VERSION.SDK_INT >= 21) {
        localMediaPlayer.setAudioAttributes(h());
      } else {
        localMediaPlayer.setAudioStreamType(2);
      }
      localMediaPlayer.setLooping(true);
      Context localContext = b;
      Object localObject = new StringBuilder("android.resource://");
      ((StringBuilder)localObject).append(f.n());
      ((StringBuilder)localObject).append(v.a());
      localObject = Uri.parse(((StringBuilder)localObject).toString());
      k.a(localObject, "Uri.parse(\"android.resou…geName()}$VOIP_TONE_URI\")");
      localMediaPlayer.setDataSource(localContext, (Uri)localObject);
      localMediaPlayer.prepare();
      return localMediaPlayer;
    }
    catch (Exception localException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localException);
    }
    return null;
  }
  
  private static AudioAttributes h()
  {
    return new AudioAttributes.Builder().setContentType(2).setUsage(6).build();
  }
  
  private static AudioAttributes i()
  {
    return new AudioAttributes.Builder().setContentType(1).setUsage(2).build();
  }
  
  public final void a()
  {
    Object localObject2 = a;
    Object localObject1 = localObject2;
    if (localObject2 == null)
    {
      localObject1 = g();
      a = ((MediaPlayer)localObject1);
    }
    if (localObject1 == null) {
      return;
    }
    if (d == null)
    {
      localObject2 = (AudioManager.OnAudioFocusChangeListener)new t.e(this);
      d = a(b.a, (AudioManager.OnAudioFocusChangeListener)localObject2);
    }
    a((MediaPlayer)localObject1);
  }
  
  @TargetApi(26)
  public final void a(String paramString, y<? super a> paramy)
  {
    k.b(paramString, "number");
    k.b(paramy, "channel");
    c.a(paramString, (c.g.a.b)new t.c(this, paramy));
    paramy.a((c.g.a.b)new t.d(this, paramString));
  }
  
  public final void a(ag paramag)
  {
    k.b(paramag, "scope");
    Object localObject = (AudioManager.OnAudioFocusChangeListener)t.h.a;
    localObject = a(b.b, (AudioManager.OnAudioFocusChangeListener)localObject);
    kotlinx.coroutines.e.b(paramag, null, (m)new t.f(null), 3).a_((c.g.a.b)new t.g((e)localObject));
  }
  
  public final void b()
  {
    Object localObject = a;
    if (localObject == null) {
      return;
    }
    c((MediaPlayer)localObject);
    localObject = d;
    if (localObject != null) {
      ((e)localObject).a();
    }
    d = null;
  }
  
  public final void c()
  {
    i.i(b).setMode(3);
  }
  
  public final void d()
  {
    i.i(b).setMode(0);
  }
  
  public final void e()
  {
    i.i(b).setSpeakerphoneOn(false);
  }
  
  public final void f()
  {
    b();
    Object localObject = a;
    if (localObject != null)
    {
      ((MediaPlayer)localObject).release();
      localObject = x.a;
    }
    a = null;
    localObject = e;
    if (localObject != null) {
      ((c)localObject).a();
    }
    e = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.t
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */