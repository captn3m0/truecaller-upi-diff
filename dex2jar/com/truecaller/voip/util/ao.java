package com.truecaller.voip.util;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcelable;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccount.Builder;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import c.n.m;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.d;
import com.truecaller.utils.extensions.i;
import com.truecaller.utils.extensions.r;
import com.truecaller.voip.R.string;
import com.truecaller.voip.callconnection.VoipCallConnectionService;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;

public final class ao
  implements an
{
  private final e a;
  private final d b;
  private final Context c;
  private final com.truecaller.voip.k d;
  private final com.truecaller.voip.callconnection.f e;
  
  @Inject
  public ao(@Named("features_registry") e parame, d paramd, Context paramContext, com.truecaller.voip.k paramk, com.truecaller.voip.callconnection.f paramf)
  {
    a = parame;
    b = paramd;
    c = paramContext;
    d = paramk;
    e = paramf;
  }
  
  private boolean a()
  {
    if (d.a()) {
      if (Build.VERSION.SDK_INT < 26) {
        return false;
      }
    }
    int i;
    label309:
    label314:
    label316:
    label323:
    label333:
    do
    {
      for (;;)
      {
        Object localObject3;
        try
        {
          localTelecomManager = i.c(c);
          localPhoneAccountHandle = b();
          PhoneAccount localPhoneAccount = localTelecomManager.getPhoneAccount(localPhoneAccountHandle);
          Object localObject1 = a;
          localObject1 = ((com.truecaller.featuretoggles.f)P.a((e)localObject1, e.a[112])).e();
          boolean bool = m.a((CharSequence)localObject1);
          Object localObject4 = null;
          if (!(bool ^ true)) {
            break label309;
          }
          if (localObject1 == null) {
            break label333;
          }
          localObject3 = m.c((CharSequence)localObject1, new String[] { "," }, false, 6);
          if (localObject3 == null) {
            break label333;
          }
          if ((((List)localObject3).size() == 1) && (c.g.b.k.a((String)((List)localObject3).get(0), "AllModels")))
          {
            i = 1;
          }
          else
          {
            localObject1 = b.l();
            if (!(m.a((CharSequence)localObject1) ^ true)) {
              break label314;
            }
            break label316;
            Iterator localIterator = ((Iterable)localObject3).iterator();
            localObject3 = localObject4;
            if (!localIterator.hasNext()) {
              break label323;
            }
            localObject3 = localIterator.next();
            if (!m.a((String)localObject1, (String)localObject3, true)) {
              continue;
            }
            break label323;
          }
          if (localPhoneAccount == null) {
            break;
          }
          if (i != 0)
          {
            localTelecomManager.unregisterPhoneAccount(localPhoneAccountHandle);
            return false;
          }
          return true;
        }
        catch (SecurityException localSecurityException)
        {
          TelecomManager localTelecomManager;
          PhoneAccountHandle localPhoneAccountHandle;
          AssertionUtil.reportThrowableButNeverCrash((Throwable)localSecurityException);
          return false;
        }
        localTelecomManager.registerPhoneAccount(PhoneAccount.builder(localPhoneAccountHandle, (CharSequence)c.getString(R.string.voip_text)).setCapabilities(2048).addSupportedUriScheme("tel").build());
        return true;
        return false;
        Object localObject2 = null;
        continue;
        localObject2 = null;
        if (localObject2 == null) {
          if ((goto 333) && (localObject3 != null)) {
            i = 1;
          } else {
            i = 0;
          }
        }
      }
    } while (i == 0);
    return false;
  }
  
  @TargetApi(26)
  private final PhoneAccountHandle b()
  {
    return new PhoneAccountHandle(new ComponentName(c, VoipCallConnectionService.class), "TruecallerVoipAccount");
  }
  
  @SuppressLint({"NewApi"})
  public final boolean a(String paramString)
  {
    c.g.b.k.b(paramString, "number");
    if (!a()) {
      return false;
    }
    try
    {
      TelecomManager localTelecomManager = i.c(c);
      Uri localUri = r.a(paramString);
      Bundle localBundle = new Bundle();
      localBundle.putParcelable("android.telecom.extra.PHONE_ACCOUNT_HANDLE", (Parcelable)b());
      localTelecomManager.placeCall(localUri, localBundle);
      e.d(paramString);
      return true;
    }
    catch (SecurityException paramString)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramString);
    }
    return false;
  }
  
  @SuppressLint({"NewApi"})
  public final boolean b(String paramString)
  {
    c.g.b.k.b(paramString, "number");
    if (!a()) {
      return false;
    }
    try
    {
      PhoneAccountHandle localPhoneAccountHandle = b();
      TelecomManager localTelecomManager = i.c(c);
      Bundle localBundle = new Bundle();
      localBundle.putParcelable("android.telecom.extra.PHONE_ACCOUNT_HANDLE", (Parcelable)localPhoneAccountHandle);
      localBundle.putParcelable("android.telecom.extra.INCOMING_CALL_ADDRESS", (Parcelable)r.a(paramString));
      localTelecomManager.addNewIncomingCall(localPhoneAccountHandle, localBundle);
      return true;
    }
    catch (SecurityException paramString)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramString);
    }
    return false;
  }
  
  public final boolean c(String paramString)
  {
    "Checking if there is voip call for number: ".concat(String.valueOf(paramString));
    if (paramString == null) {
      return e.a();
    }
    return e.a(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.ao
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */