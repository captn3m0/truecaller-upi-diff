package com.truecaller.voip.util;

import c.g.a.a;
import c.g.a.m;
import c.g.b.k;
import c.g.b.v.c;
import c.l;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.e.a;
import com.truecaller.tracking.events.af;
import com.truecaller.tracking.events.af.a;
import com.truecaller.tracking.events.ag.a;
import com.truecaller.tracking.events.ah;
import com.truecaller.tracking.events.ah.a;
import com.truecaller.tracking.events.ao;
import com.truecaller.tracking.events.ao.a;
import com.truecaller.voip.VoipUser;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.a.u;
import kotlinx.coroutines.bn;
import org.apache.a.d.d;

public final class p
  implements o, kotlinx.coroutines.ag
{
  final com.truecaller.analytics.b a;
  final com.truecaller.androidactors.f<ae> b;
  final aa c;
  private final c.d.f d;
  
  @Inject
  public p(@Named("IO") c.d.f paramf, com.truecaller.analytics.b paramb, com.truecaller.androidactors.f<ae> paramf1, aa paramaa)
  {
    d = paramf;
    a = paramb;
    b = paramf1;
    c = paramaa;
  }
  
  static VoipAnalyticsState a(VoipAnalyticsCallDirection paramVoipAnalyticsCallDirection)
  {
    switch (q.d[paramVoipAnalyticsCallDirection.ordinal()])
    {
    default: 
      throw new l();
    case 2: 
      return VoipAnalyticsState.INIT_FAILED;
    }
    return VoipAnalyticsState.FAILED;
  }
  
  public final c.d.f V_()
  {
    return d;
  }
  
  public final void a(VoipAnalyticsInCallUiAction paramVoipAnalyticsInCallUiAction)
  {
    k.b(paramVoipAnalyticsInCallUiAction, "action");
    com.truecaller.analytics.b localb = a;
    com.truecaller.analytics.e locale = new e.a("ViewAction").a("Action", paramVoipAnalyticsInCallUiAction.getValue()).a("Context", VoipAnalyticsContext.VOIP_IN_CALL_UI.getValue()).a();
    k.a(locale, "AnalyticsEvent.Builder(E…\n                .build()");
    localb.b(locale);
    ((ae)b.a()).a((d)af.b().b((CharSequence)paramVoipAnalyticsInCallUiAction.getValue()).a((CharSequence)VoipAnalyticsContext.VOIP_IN_CALL_UI.getValue()).a());
  }
  
  public final void a(n paramn, VoipAnalyticsState paramVoipAnalyticsState, VoipAnalyticsStateReason paramVoipAnalyticsStateReason)
  {
    k.b(paramn, "callInfo");
    k.b(paramVoipAnalyticsState, "state");
    com.truecaller.analytics.b localb = a;
    e.a locala = new e.a("VoipStateChanged").a("Direction", a.getValue()).a("State", paramVoipAnalyticsState.getValue());
    k.a(locala, "AnalyticsEvent.Builder(E…PARAM_STATE, state.value)");
    Object localObject2 = null;
    if (paramVoipAnalyticsStateReason != null) {
      localObject1 = paramVoipAnalyticsStateReason.getValue();
    } else {
      localObject1 = null;
    }
    if (localObject1 != null) {
      locala.a("Reason", (String)localObject1);
    }
    Object localObject1 = locala.a();
    k.a(localObject1, "AnalyticsEvent.Builder(E…\n                .build()");
    localb.b((com.truecaller.analytics.e)localObject1);
    localObject1 = ah.b().a((CharSequence)a.getValue()).b((CharSequence)paramVoipAnalyticsState.getValue()).d((CharSequence)b);
    paramVoipAnalyticsState = (VoipAnalyticsState)localObject2;
    if (paramVoipAnalyticsStateReason != null) {
      paramVoipAnalyticsState = paramVoipAnalyticsStateReason.getValue();
    }
    paramn = ((ah.a)localObject1).c((CharSequence)paramVoipAnalyticsState).e((CharSequence)c).f((CharSequence)e).a(d).b(f);
    ((ae)b.a()).a((d)paramn.a());
  }
  
  public final void a(String paramString, long paramLong)
  {
    k.b(paramString, "channelId");
    com.truecaller.analytics.b localb = a;
    com.truecaller.analytics.e locale = new e.a("VoipCallFinished").a(Double.valueOf(paramLong)).a();
    k.a(locale, "AnalyticsEvent.Builder(E…\n                .build()");
    localb.b(locale);
    ((ae)b.a()).a((d)com.truecaller.tracking.events.ag.b().a((CharSequence)paramString).a(paramLong).a());
  }
  
  public final void a(String paramString, VoipAnalyticsFailedCallAction paramVoipAnalyticsFailedCallAction)
  {
    k.b(paramString, "analyticsContext");
    k.b(paramVoipAnalyticsFailedCallAction, "action");
    com.truecaller.analytics.b localb = a;
    com.truecaller.analytics.e locale = new e.a("ViewAction").a("Context", paramString).a("Action", paramVoipAnalyticsFailedCallAction.getValue()).a();
    k.a(locale, "AnalyticsEvent.Builder(E…\n                .build()");
    localb.b(locale);
    ((ae)b.a()).a((d)af.b().a((CharSequence)paramString).b((CharSequence)paramVoipAnalyticsFailedCallAction.getValue()).a());
  }
  
  public final void a(String paramString, VoipAnalyticsNotificationAction paramVoipAnalyticsNotificationAction)
  {
    k.b(paramString, "analyticsContext");
    k.b(paramVoipAnalyticsNotificationAction, "action");
    com.truecaller.analytics.b localb = a;
    com.truecaller.analytics.e locale = new e.a("ViewAction").a("Context", paramString).a("Action", paramVoipAnalyticsNotificationAction.getValue()).a();
    k.a(locale, "AnalyticsEvent.Builder(E…\n                .build()");
    localb.b(locale);
    ((ae)b.a()).a((d)af.b().a((CharSequence)paramString).b((CharSequence)paramVoipAnalyticsNotificationAction.getValue()).a());
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "analyticsContext");
    k.b(paramString2, "number");
    kotlinx.coroutines.e.b(this, null, (m)new p.e(this, paramString2, paramString1, null), 3);
  }
  
  public final void a(String paramString1, String paramString2, int paramInt, boolean paramBoolean)
  {
    k.b(paramString1, "voipId");
    k.b(paramString2, "token");
    Object localObject = a;
    com.truecaller.analytics.e locale = new e.a("VoipRTMLoginError").a("ErrorCode", paramInt).a("IsRetryAttempt", paramBoolean).a();
    k.a(locale, "AnalyticsEvent.Builder(E…\n                .build()");
    ((com.truecaller.analytics.b)localObject).b(locale);
    localObject = (Map)new HashMap();
    ((Map)localObject).put("ErrorCode", String.valueOf(paramInt));
    ((Map)localObject).put("IsRetryAttempt", String.valueOf(paramBoolean));
    ((Map)localObject).put("VoipId", paramString1);
    ((Map)localObject).put("Token", paramString2);
    paramString1 = ao.b().a((CharSequence)"VoipRTMLoginError").a((Map)localObject).a();
    ((ae)b.a()).a((d)paramString1);
  }
  
  public final void a(kotlinx.coroutines.ag paramag, VoipAnalyticsCallDirection paramVoipAnalyticsCallDirection, String paramString, a<String> parama, a<Integer> parama1, u<com.truecaller.voip.ag> paramu, u<VoipUser> paramu1, u<Boolean> paramu2)
  {
    k.b(paramag, "scope");
    k.b(paramVoipAnalyticsCallDirection, "direction");
    k.b(paramString, "channelId");
    k.b(paramu, "statesChannel");
    k.b(paramu1, "usersChannel");
    v.c localc = new v.c();
    a = null;
    paramVoipAnalyticsCallDirection = (a)new p.f(localc, paramu1, paramVoipAnalyticsCallDirection, paramString, parama, parama1);
    kotlinx.coroutines.e.b(paramag, d, (m)new p.c(this, paramu, paramVoipAnalyticsCallDirection, null), 2).a_((c.g.a.b)new p.d(paramu));
    if (paramu2 != null)
    {
      kotlinx.coroutines.e.b(paramag, d, (m)new p.a(this, paramu2, paramVoipAnalyticsCallDirection, null), 2).a_((c.g.a.b)new p.b(paramu2));
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */