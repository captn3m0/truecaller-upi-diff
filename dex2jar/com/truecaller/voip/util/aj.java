package com.truecaller.voip.util;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class aj
  implements d<ai>
{
  private final Provider<Context> a;
  
  private aj(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static aj a(Provider<Context> paramProvider)
  {
    return new aj(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.aj
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */