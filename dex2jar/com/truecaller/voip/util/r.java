package com.truecaller.voip.util;

import com.truecaller.analytics.ae;
import com.truecaller.analytics.b;
import dagger.a.d;
import javax.inject.Provider;

public final class r
  implements d<p>
{
  private final Provider<c.d.f> a;
  private final Provider<b> b;
  private final Provider<com.truecaller.androidactors.f<ae>> c;
  private final Provider<aa> d;
  
  private r(Provider<c.d.f> paramProvider, Provider<b> paramProvider1, Provider<com.truecaller.androidactors.f<ae>> paramProvider2, Provider<aa> paramProvider3)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static r a(Provider<c.d.f> paramProvider, Provider<b> paramProvider1, Provider<com.truecaller.androidactors.f<ae>> paramProvider2, Provider<aa> paramProvider3)
  {
    return new r(paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.r
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */