package com.truecaller.voip.util;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioAttributes.Builder;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Build.VERSION;
import android.os.VibrationEffect;
import android.os.Vibrator;
import c.l;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.extensions.i;
import com.truecaller.voip.ConnectionState;
import com.truecaller.voip.VoipState;
import javax.inject.Inject;

public final class g
  implements f
{
  private final ToneGenerator a;
  private final Vibrator b;
  private final AudioManager c;
  
  @Inject
  public g(Context paramContext)
  {
    b = i.h(paramContext);
    c = i.i(paramContext);
    try
    {
      paramContext = new ToneGenerator(0, 100);
    }
    catch (RuntimeException paramContext)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramContext);
      paramContext = null;
    }
    a = paramContext;
  }
  
  private static AudioAttributes e()
  {
    return new AudioAttributes.Builder().setContentType(4).setUsage(6).build();
  }
  
  public final void a()
  {
    if (!b.hasVibrator()) {
      return;
    }
    if (c.getRingerMode() == 0) {
      return;
    }
    if (Build.VERSION.SDK_INT >= 26)
    {
      b.vibrate(VibrationEffect.createOneShot(400L, -1));
      return;
    }
    b.vibrate(400L);
  }
  
  public final void a(VoipState paramVoipState, ConnectionState paramConnectionState, com.truecaller.voip.manager.k paramk1, com.truecaller.voip.manager.k paramk2)
  {
    c.g.b.k.b(paramVoipState, "voipState");
    c.g.b.k.b(paramConnectionState, "connectionState");
    c.g.b.k.b(paramk1, "serviceSetting");
    c.g.b.k.b(paramk2, "peerServiceSetting");
    boolean bool = b;
    paramk1 = null;
    if (bool) {
      paramConnectionState = paramk1;
    } else if (b) {
      paramConnectionState = Integer.valueOf(22);
    } else {
      switch (h.a[paramConnectionState.ordinal()])
      {
      default: 
        throw new l();
      case 3: 
        paramConnectionState = Integer.valueOf(96);
        break;
      case 2: 
        paramConnectionState = Integer.valueOf(22);
        break;
      case 1: 
        paramConnectionState = paramk1;
        switch (h.b[paramVoipState.ordinal()])
        {
        default: 
          throw new l();
        case 12: 
          paramConnectionState = paramk1;
          if (a) {
            paramConnectionState = Integer.valueOf(19);
          }
          break;
        case 6: 
        case 7: 
        case 8: 
        case 9: 
        case 10: 
        case 11: 
          paramConnectionState = Integer.valueOf(96);
          break;
        case 5: 
          paramConnectionState = Integer.valueOf(23);
          break;
        case 1: 
        case 2: 
        case 3: 
        case 4: 
          paramConnectionState = Integer.valueOf(22);
        }
        break;
      }
    }
    if (paramConnectionState == null)
    {
      paramVoipState = a;
      if (paramVoipState != null) {
        paramVoipState.stopTone();
      }
      return;
    }
    paramVoipState = a;
    if (paramVoipState != null)
    {
      paramVoipState.startTone(paramConnectionState.intValue());
      return;
    }
  }
  
  public final void b()
  {
    if (!b.hasVibrator()) {
      return;
    }
    if (c.getRingerMode() == 0) {
      return;
    }
    long[] arrayOfLong = new long[2];
    long[] tmp27_26 = arrayOfLong;
    tmp27_26[0] = 1000L;
    long[] tmp33_27 = tmp27_26;
    tmp33_27[1] = 1000L;
    tmp33_27;
    if (Build.VERSION.SDK_INT >= 26)
    {
      b.vibrate(VibrationEffect.createWaveform(arrayOfLong, 0), e());
      return;
    }
    if (Build.VERSION.SDK_INT >= 21)
    {
      b.vibrate(arrayOfLong, 0, e());
      return;
    }
    b.vibrate(arrayOfLong, 0);
  }
  
  public final void c()
  {
    if (!b.hasVibrator()) {
      return;
    }
    b.cancel();
  }
  
  public final void d()
  {
    ToneGenerator localToneGenerator = a;
    if (localToneGenerator != null) {
      localToneGenerator.stopTone();
    }
    localToneGenerator = a;
    if (localToneGenerator != null) {
      localToneGenerator.release();
    }
    c();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */