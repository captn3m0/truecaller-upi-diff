package com.truecaller.voip.util;

import c.d.a.a;
import c.d.c;
import c.o.b;
import com.truecaller.voip.VoipUser;
import javax.inject.Inject;

public final class av
  implements au
{
  private final ac a;
  private final aa b;
  
  @Inject
  public av(ac paramac, aa paramaa)
  {
    a = paramac;
    b = paramaa;
  }
  
  public final Object a(ag paramag, c<? super VoipUser> paramc)
  {
    if ((paramc instanceof av.b))
    {
      localObject = (av.b)paramc;
      if ((b & 0x80000000) != 0)
      {
        b += Integer.MIN_VALUE;
        paramc = (c<? super VoipUser>)localObject;
        break label48;
      }
    }
    paramc = new av.b(this, paramc);
    label48:
    Object localObject = a;
    a locala = a.a;
    switch (b)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 1: 
      paramag = (ag)e;
      if (!(localObject instanceof o.b)) {
        paramc = (c<? super VoipUser>)localObject;
      } else {
        throw a;
      }
      break;
    case 0: 
      if ((localObject instanceof o.b)) {
        break label241;
      }
      localObject = new StringBuilder("Fetching voip user from number:");
      ((StringBuilder)localObject).append(c);
      ((StringBuilder)localObject).toString();
      localObject = a;
      String str = c;
      d = this;
      e = paramag;
      b = 1;
      localObject = ((ac)localObject).a(str, paramc);
      paramc = (c<? super VoipUser>)localObject;
      if (localObject == locala) {
        return locala;
      }
      break;
    }
    paramc = (String)paramc;
    if (paramc == null)
    {
      paramc = new StringBuilder("Cannot fetch voip id from number:");
      paramc.append(c);
      paramc.toString();
      return null;
    }
    return aw.a(paramag, paramc);
    label241:
    throw a;
  }
  
  public final Object a(String paramString, c<? super VoipUser> paramc)
  {
    Object localObject1;
    if ((paramc instanceof av.a))
    {
      localObject1 = (av.a)paramc;
      if ((b & 0x80000000) != 0)
      {
        b += Integer.MIN_VALUE;
        paramc = (c<? super VoipUser>)localObject1;
        break label48;
      }
    }
    paramc = new av.a(this, paramc);
    label48:
    Object localObject3 = a;
    a locala = a.a;
    switch (b)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 2: 
      if (!(localObject3 instanceof o.b)) {
        return localObject3;
      }
      throw a;
    case 1: 
      localObject1 = (String)e;
      paramString = (av)d;
      if ((localObject3 instanceof o.b)) {
        throw a;
      }
      break;
    case 0: 
      if ((localObject3 instanceof o.b)) {
        break label285;
      }
      "Fetching voip user from voip id:".concat(String.valueOf(paramString));
      localObject1 = a;
      d = this;
      e = paramString;
      b = 1;
      localObject3 = ((ac)localObject1).b(paramString, paramc);
      if (localObject3 == locala) {
        return locala;
      }
      localObject2 = this;
      localObject1 = paramString;
      paramString = (String)localObject2;
    }
    Object localObject2 = (String)localObject3;
    if (localObject2 == null)
    {
      "Cannot fetch number from voip id:".concat(String.valueOf(localObject1));
      return null;
    }
    d = paramString;
    e = localObject1;
    f = localObject2;
    b = 2;
    paramString = paramString.a((String)localObject1, (String)localObject2, paramc);
    if (paramString == locala) {
      return locala;
    }
    return paramString;
    label285:
    throw a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.av
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */