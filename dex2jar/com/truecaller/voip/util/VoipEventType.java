package com.truecaller.voip.util;

public enum VoipEventType
{
  static
  {
    VoipEventType localVoipEventType1 = new VoipEventType("INCOMING", 0);
    INCOMING = localVoipEventType1;
    VoipEventType localVoipEventType2 = new VoipEventType("OUTGOING", 1);
    OUTGOING = localVoipEventType2;
    VoipEventType localVoipEventType3 = new VoipEventType("MISSED", 2);
    MISSED = localVoipEventType3;
    VoipEventType localVoipEventType4 = new VoipEventType("BLOCKED", 3);
    BLOCKED = localVoipEventType4;
    $VALUES = new VoipEventType[] { localVoipEventType1, localVoipEventType2, localVoipEventType3, localVoipEventType4 };
  }
  
  private VoipEventType() {}
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.VoipEventType
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */