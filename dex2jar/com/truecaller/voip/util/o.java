package com.truecaller.voip.util;

import c.g.a.a;
import com.truecaller.voip.VoipUser;
import kotlinx.coroutines.a.u;

public abstract interface o
{
  public abstract void a(VoipAnalyticsInCallUiAction paramVoipAnalyticsInCallUiAction);
  
  public abstract void a(n paramn, VoipAnalyticsState paramVoipAnalyticsState, VoipAnalyticsStateReason paramVoipAnalyticsStateReason);
  
  public abstract void a(String paramString, long paramLong);
  
  public abstract void a(String paramString, VoipAnalyticsFailedCallAction paramVoipAnalyticsFailedCallAction);
  
  public abstract void a(String paramString, VoipAnalyticsNotificationAction paramVoipAnalyticsNotificationAction);
  
  public abstract void a(String paramString1, String paramString2);
  
  public abstract void a(String paramString1, String paramString2, int paramInt, boolean paramBoolean);
  
  public abstract void a(kotlinx.coroutines.ag paramag, VoipAnalyticsCallDirection paramVoipAnalyticsCallDirection, String paramString, a<String> parama, a<Integer> parama1, u<com.truecaller.voip.ag> paramu, u<VoipUser> paramu1, u<Boolean> paramu2);
}

/* Location:
 * Qualified Name:     com.truecaller.voip.util.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */