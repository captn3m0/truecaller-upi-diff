package com.truecaller.voip;

import com.truecaller.voip.manager.rtm.c;
import dagger.a.d;
import io.agora.rtm.RtmClient;
import javax.inject.Provider;

public final class u
  implements d<RtmClient>
{
  private final Provider<c> a;
  
  private u(Provider<c> paramProvider)
  {
    a = paramProvider;
  }
  
  public static u a(Provider<c> paramProvider)
  {
    return new u(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.u
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */