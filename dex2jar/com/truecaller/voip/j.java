package com.truecaller.voip;

import c.g.b.k;
import com.truecaller.voip.callconnection.VoipCallConnectionService;
import com.truecaller.voip.db.c;
import com.truecaller.voip.incall.VoipService;
import com.truecaller.voip.incall.ui.VoipInAppNotificationView;
import com.truecaller.voip.incoming.IncomingVoipService;
import com.truecaller.voip.incoming.blocked.VoipBlockedCallsWorker;
import com.truecaller.voip.incoming.missed.MissedVoipCallMessageBroadcast;
import com.truecaller.voip.incoming.missed.MissedVoipCallsWorker;
import com.truecaller.voip.util.ak;
import com.truecaller.voip.util.o;

public abstract interface j
{
  public static final a a = a.b;
  
  public abstract void a(VoipCallConnectionService paramVoipCallConnectionService);
  
  public abstract void a(VoipService paramVoipService);
  
  public abstract void a(VoipInAppNotificationView paramVoipInAppNotificationView);
  
  public abstract void a(com.truecaller.voip.incall.ui.a parama);
  
  public abstract void a(IncomingVoipService paramIncomingVoipService);
  
  public abstract void a(VoipBlockedCallsWorker paramVoipBlockedCallsWorker);
  
  public abstract void a(MissedVoipCallMessageBroadcast paramMissedVoipCallMessageBroadcast);
  
  public abstract void a(MissedVoipCallsWorker paramMissedVoipCallsWorker);
  
  public abstract void a(com.truecaller.voip.incoming.ui.a parama);
  
  public abstract d b();
  
  public abstract c c();
  
  public abstract ak d();
  
  public abstract o e();
  
  public static final class a
  {
    public static j a;
    
    public static j a()
    {
      j localj = a;
      if (localj == null) {
        k.a("instance");
      }
      return localj;
    }
    
    public static void a(j paramj)
    {
      k.b(paramj, "<set-?>");
      a = paramj;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */