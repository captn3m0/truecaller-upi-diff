package com.truecaller.voip;

import android.content.Context;
import c.g.b.k;
import com.google.gson.f;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.network.util.h;
import com.truecaller.voip.db.VoipDatabase;
import com.truecaller.voip.db.VoipDatabase.a;
import com.truecaller.voip.manager.rtm.c;
import com.truecaller.voip.util.aa;
import com.truecaller.voip.util.af;
import com.truecaller.voip.util.at;
import com.truecaller.voip.util.l;
import com.truecaller.voip.util.y;
import com.truecaller.voip.util.z;
import io.agora.rtm.RtmClient;

public abstract class t
{
  public static final t.a a = new t.a((byte)0);
  
  public static final f a()
  {
    return new f();
  }
  
  public static final com.truecaller.voip.db.a a(Context paramContext)
  {
    k.b(paramContext, "context");
    paramContext = VoipDatabase.g.a(paramContext);
    if (paramContext != null)
    {
      paramContext = paramContext.h();
      if (paramContext != null) {
        return paramContext;
      }
    }
    throw ((Throwable)new IllegalStateException("Cannot initialize voip database"));
  }
  
  public static final RtmClient a(c paramc)
  {
    k.b(paramc, "manager");
    return c;
  }
  
  public static final com.truecaller.voip.api.a b()
  {
    return (com.truecaller.voip.api.a)h.a(KnownEndpoints.VOIP, com.truecaller.voip.api.a.class);
  }
  
  public static final y c()
  {
    Object localObject = d.a;
    localObject = d.a.a();
    if (localObject != null) {
      return (y)localObject;
    }
    throw ((Throwable)new IllegalStateException("Voip history model should be set"));
  }
  
  public static final af d()
  {
    Object localObject = d.a;
    localObject = d.a.b();
    if (localObject != null) {
      return (af)localObject;
    }
    throw ((Throwable)new IllegalStateException("Voip missed call provider should be set"));
  }
  
  public static final aa e()
  {
    Object localObject = d.a;
    localObject = d.a.c();
    if (localObject != null) {
      return (aa)localObject;
    }
    throw ((Throwable)new IllegalStateException("Voip caller info provider should be set"));
  }
  
  public static final z f()
  {
    Object localObject = d.a;
    localObject = d.a.d();
    if (localObject != null) {
      return (z)localObject;
    }
    throw ((Throwable)new IllegalStateException("Voip call message should be set"));
  }
  
  public static final l g()
  {
    Object localObject = d.a;
    localObject = d.a.e();
    if (localObject != null) {
      return (l)localObject;
    }
    throw ((Throwable)new IllegalStateException("Presence reporter should be set"));
  }
  
  public static final at h()
  {
    Object localObject = d.a;
    localObject = d.a.f();
    if (localObject != null) {
      return (at)localObject;
    }
    throw ((Throwable)new IllegalStateException("Logo helper should be set"));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.voip.t
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */