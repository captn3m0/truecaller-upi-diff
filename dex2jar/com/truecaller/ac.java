package com.truecaller;

import android.content.ContentResolver;
import c.d.f;
import com.truecaller.common.h.u;
import com.truecaller.data.access.i;
import dagger.a.d;
import javax.inject.Provider;

public final class ac
  implements d<i>
{
  private final c a;
  private final Provider<ContentResolver> b;
  private final Provider<u> c;
  private final Provider<com.truecaller.data.access.c> d;
  private final Provider<f> e;
  private final Provider<f> f;
  
  private ac(c paramc, Provider<ContentResolver> paramProvider, Provider<u> paramProvider1, Provider<com.truecaller.data.access.c> paramProvider2, Provider<f> paramProvider3, Provider<f> paramProvider4)
  {
    a = paramc;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
    f = paramProvider4;
  }
  
  public static ac a(c paramc, Provider<ContentResolver> paramProvider, Provider<u> paramProvider1, Provider<com.truecaller.data.access.c> paramProvider2, Provider<f> paramProvider3, Provider<f> paramProvider4)
  {
    return new ac(paramc, paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ac
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */