package com.truecaller;

import com.truecaller.backup.BackupTask;
import com.truecaller.backup.RestoreService;
import com.truecaller.backup.aa;
import com.truecaller.backup.ac.a;
import com.truecaller.backup.ad;
import com.truecaller.backup.ae;
import com.truecaller.backup.am;
import com.truecaller.backup.an;
import com.truecaller.backup.ao;
import com.truecaller.backup.au;
import com.truecaller.backup.av;
import com.truecaller.backup.aw;
import com.truecaller.backup.b;
import com.truecaller.backup.bp;
import com.truecaller.backup.bq.a;
import com.truecaller.backup.br;
import com.truecaller.backup.bs;
import com.truecaller.backup.bt.a;
import com.truecaller.backup.bu;
import com.truecaller.backup.bw;
import com.truecaller.backup.bx;
import com.truecaller.backup.by;
import com.truecaller.backup.bz;
import com.truecaller.backup.e;
import com.truecaller.backup.i;
import com.truecaller.backup.l;
import com.truecaller.backup.m;
import com.truecaller.backup.n;
import com.truecaller.backup.o.a;
import com.truecaller.backup.p;
import com.truecaller.backup.q;
import com.truecaller.backup.v;
import com.truecaller.backup.w.a;
import com.truecaller.backup.x;
import com.truecaller.backup.y;
import com.truecaller.backup.z;
import dagger.a.c;
import dagger.a.g;
import javax.inject.Provider;

final class be$b
  implements b
{
  private Provider<e> b = c.a(l.a(be.as(a)));
  private Provider<x> c = y.a(be.o(a), be.l(a), be.G(a), b, be.T(a), be.r(a), be.aL(a), be.S(a), be.W(a), be.am(a), be.aM(a), be.aN(a));
  private Provider<w.a> d = c.a(c);
  private Provider<p> e = q.a(be.l(a), be.G(a), b, be.T(a), be.r(a), be.am(a), be.aM(a), be.aL(a), be.S(a), be.W(a), be.aN(a));
  private Provider<o.a> f = c.a(e);
  private Provider<com.google.gson.f> g = c.a(m.a());
  private Provider<av> h = aw.a(be.o(a), be.aO(a), be.aP(a), be.S(a), g, be.D(a));
  private Provider<au> i = c.a(h);
  private Provider<an> j = ao.a(be.o(a), be.aP(a), be.k(a), g, be.D(a));
  private Provider<am> k = c.a(j);
  private Provider<com.truecaller.backup.f> l = i.a(be.l(a), be.aP(a), i, k, be.a(a), be.C(a), be.r(a), be.v(a));
  private Provider<e> m = c.a(l);
  private Provider<br> n = bs.a(be.o(a), be.l(a), be.G(a), b, m, be.T(a), be.r(a), be.S(a), be.W(a));
  private Provider<bq.a> o = c.a(n);
  private Provider<z> p = c.a(aa.a(be.G(a), be.o(a), g, be.aL(a), be.v(a), be.r(a), be.aQ(a), be.w(a), be.aR(a), be.aM(a), be.ax(a), be.y(a), be.aS(a), be.E(a), be.aT(a)));
  private Provider<by> q = bz.a(be.o(a), g, be.aP(a), p, be.D(a));
  private Provider<bx> r = c.a(q);
  private Provider<bu> s = bw.a(be.l(a), be.G(a), b, m, r, be.aU(a), be.S(a), be.b(a), be.al(a));
  private Provider<bt.a> t = c.a(s);
  private Provider<ad> u = ae.a(be.l(a), be.G(a), b, r, be.r(a), be.aU(a), be.S(a));
  private Provider<ac.a> v = c.a(u);
  
  private be$b(be parambe) {}
  
  public final e a()
  {
    return (e)b.get();
  }
  
  public final void a(BackupTask paramBackupTask)
  {
    a = ((ac.a)v.get());
    b = ((com.truecaller.notifications.a)be.aV(a).get());
  }
  
  public final void a(RestoreService paramRestoreService)
  {
    a = ((bt.a)t.get());
    b = ((com.truecaller.notifications.a)be.aV(a).get());
  }
  
  public final void a(bp parambp)
  {
    a = ((bq.a)o.get());
    b = ((c.d.f)g.a(be.f(a).r(), "Cannot return null from a non-@Nullable component method"));
  }
  
  public final void a(n paramn)
  {
    a = ((o.a)f.get());
  }
  
  public final void a(v paramv)
  {
    a = ((w.a)d.get());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.be.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */