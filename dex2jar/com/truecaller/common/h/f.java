package com.truecaller.common.h;

import android.annotation.TargetApi;
import android.content.Context;
import android.telecom.TelecomManager;
import c.u;
import com.truecaller.common.a.a;
import javax.inject.Inject;

@TargetApi(28)
public final class f
  implements d
{
  private final TelecomManager a;
  private final Context b;
  private final a c;
  
  @Inject
  public f(Context paramContext, a parama)
  {
    b = paramContext;
    c = parama;
    paramContext = b.getSystemService("telecom");
    if (paramContext != null)
    {
      a = ((TelecomManager)paramContext);
      return;
    }
    throw new u("null cannot be cast to non-null type android.telecom.TelecomManager");
  }
  
  public final boolean a()
  {
    try
    {
      boolean bool = a.endCall();
      return bool;
    }
    catch (Exception localException)
    {
      c.a(2, localException);
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */