package com.truecaller.common.h;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipData.Item;
import android.content.Context;
import android.content.Intent;
import android.content.Intent.ShortcutIconResource;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.widget.Toast;
import com.truecaller.common.R.string;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.d;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.c.a.a.a.k;

public final class o
{
  private static final String[] a = { "android.intent.extra.TEXT", "sms_body" };
  
  @TargetApi(23)
  public static Intent a(Context paramContext)
  {
    Intent localIntent = new Intent("android.telecom.action.CHANGE_DEFAULT_DIALER");
    localIntent.putExtra("android.telecom.extra.CHANGE_DEFAULT_DIALER_PACKAGE_NAME", paramContext.getPackageName());
    return localIntent;
  }
  
  public static Intent a(Context paramContext, int paramInt1, int paramInt2, Intent paramIntent)
  {
    Intent localIntent = new Intent();
    localIntent.putExtra("android.intent.extra.shortcut.NAME", paramContext.getString(paramInt1));
    localIntent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(paramContext, paramInt2));
    localIntent.putExtra("android.intent.extra.shortcut.INTENT", paramIntent);
    localIntent.putExtra("duplicate", false);
    return localIntent;
  }
  
  public static Intent a(Uri paramUri)
  {
    return new Intent("android.intent.action.VIEW", paramUri);
  }
  
  public static Intent a(String paramString)
  {
    return a(Uri.parse(paramString));
  }
  
  public static ArrayList<Uri> a(Intent paramIntent)
  {
    ArrayList localArrayList = new ArrayList();
    Object localObject = paramIntent.getClipData();
    if (localObject != null)
    {
      int i = 0;
      while (i < ((ClipData)localObject).getItemCount())
      {
        Uri localUri = ((ClipData)localObject).getItemAt(i).getUri();
        if (localUri != null) {
          localArrayList.add(localUri);
        }
        i += 1;
      }
    }
    if ("android.intent.action.SEND_MULTIPLE".equals(paramIntent.getAction()))
    {
      paramIntent = paramIntent.getParcelableArrayListExtra("android.intent.extra.STREAM");
      if (paramIntent != null)
      {
        paramIntent = paramIntent.iterator();
        while (paramIntent.hasNext())
        {
          localObject = (Uri)paramIntent.next();
          if (!localArrayList.contains(localObject)) {
            localArrayList.add(localObject);
          }
        }
      }
    }
    else if ("android.intent.action.SEND".equals(paramIntent.getAction()))
    {
      paramIntent = (Uri)paramIntent.getParcelableExtra("android.intent.extra.STREAM");
      if ((paramIntent != null) && (!localArrayList.contains(paramIntent))) {
        localArrayList.add(paramIntent);
      }
    }
    if (localArrayList.isEmpty()) {
      return null;
    }
    return localArrayList;
  }
  
  private static void a(Context paramContext, ActivityNotFoundException paramActivityNotFoundException)
  {
    d.a(paramActivityNotFoundException);
    Toast.makeText(paramContext, R.string.StrAppNotFound, 0).show();
  }
  
  public static void a(Context paramContext, Intent paramIntent, String paramString)
  {
    if (TextUtils.equals(paramIntent.getAction(), paramString))
    {
      paramString = paramContext.getPackageName();
      paramContext = paramContext.getPackageManager().queryIntentActivities(paramIntent, 0).iterator();
      while (paramContext.hasNext()) {
        if (TextUtils.equals(nextactivityInfo.packageName, paramString))
        {
          paramIntent.setPackage(paramString);
          return;
        }
      }
    }
  }
  
  public static void a(Intent paramIntent1, Intent paramIntent2)
  {
    paramIntent1 = paramIntent1.getClipData();
    if ((paramIntent1 != null) && (paramIntent1.getItemCount() > 0))
    {
      paramIntent2.setClipData(paramIntent1);
      paramIntent2.addFlags(1);
    }
  }
  
  public static boolean a(Activity paramActivity, Intent paramIntent, int paramInt)
  {
    try
    {
      paramActivity.startActivityForResult(paramIntent, paramInt);
      return true;
    }
    catch (ActivityNotFoundException paramIntent)
    {
      a(paramActivity, paramIntent);
    }
    return false;
  }
  
  public static boolean a(Context paramContext, Intent paramIntent)
  {
    try
    {
      paramContext.startActivity(paramIntent);
      return true;
    }
    catch (ActivityNotFoundException paramIntent)
    {
      a(paramContext, paramIntent);
    }
    return false;
  }
  
  public static boolean a(Context paramContext, String paramString)
  {
    return a(paramContext, new Intent("android.intent.action.DIAL", Uri.parse("tel:".concat(String.valueOf(paramString)))));
  }
  
  public static boolean a(Fragment paramFragment, Intent paramIntent)
  {
    try
    {
      paramFragment.startActivity(paramIntent);
      return true;
    }
    catch (ActivityNotFoundException paramIntent)
    {
      paramFragment = paramFragment.getContext();
      if (paramFragment != null) {
        a(paramFragment, paramIntent);
      }
    }
    return false;
  }
  
  public static boolean a(Fragment paramFragment, Intent paramIntent, int paramInt)
  {
    try
    {
      paramFragment.startActivityForResult(paramIntent, paramInt);
      return true;
    }
    catch (SecurityException paramFragment)
    {
      AssertionUtil.reportThrowableButNeverCrash(paramFragment);
    }
    catch (ActivityNotFoundException paramIntent)
    {
      paramFragment = paramFragment.getContext();
      if (paramFragment != null) {
        a(paramFragment, paramIntent);
      }
    }
    return false;
  }
  
  public static Intent b(Uri paramUri)
  {
    return new Intent("android.intent.action.SENDTO", paramUri);
  }
  
  public static Intent b(String paramString)
  {
    return c("tel:".concat(String.valueOf(paramString)));
  }
  
  public static String b(Intent paramIntent)
  {
    Object localObject = new StringBuilder();
    String[] arrayOfString = a;
    int k = arrayOfString.length;
    int j = 0;
    int i = 0;
    while (i < k)
    {
      String str = paramIntent.getStringExtra(arrayOfString[i]);
      if (!k.b(str))
      {
        if (((StringBuilder)localObject).length() > 0) {
          ((StringBuilder)localObject).append("\n");
        }
        ((StringBuilder)localObject).append(str);
      }
      i += 1;
    }
    localObject = ((StringBuilder)localObject).toString();
    if (!((String)localObject).isEmpty()) {
      return (String)localObject;
    }
    if (("android.intent.action.SENDTO".equals(paramIntent.getAction())) || ("android.intent.action.VIEW".equals(paramIntent.getAction())))
    {
      paramIntent = paramIntent.getData();
      if (paramIntent != null)
      {
        paramIntent = paramIntent.getEncodedQuery();
        if (!TextUtils.isEmpty(paramIntent))
        {
          paramIntent = k.a(paramIntent, '&');
          k = paramIntent.length;
          i = j;
          while (i < k)
          {
            localObject = paramIntent[i];
            if (((String)localObject).startsWith("body=")) {}
            try
            {
              localObject = URLDecoder.decode(((String)localObject).substring(5), "UTF-8");
              return (String)localObject;
            }
            catch (UnsupportedEncodingException localUnsupportedEncodingException)
            {
              for (;;) {}
            }
            i += 1;
          }
        }
      }
    }
    return null;
  }
  
  public static boolean b(Context paramContext)
  {
    return d(paramContext, c(paramContext));
  }
  
  public static boolean b(Context paramContext, Intent paramIntent)
  {
    return paramContext.getPackageManager().queryIntentActivities(paramIntent, 65536).size() > 0;
  }
  
  public static boolean b(Context paramContext, String paramString)
  {
    return a(paramContext, c(paramString));
  }
  
  public static Intent c(Uri paramUri)
  {
    return new Intent("android.intent.action.EDIT", paramUri);
  }
  
  private static Intent c(String paramString)
  {
    return new Intent("android.intent.action.CALL", Uri.parse(paramString));
  }
  
  public static String c(Context paramContext)
  {
    String str = paramContext.getPackageName();
    paramContext = str;
    if (str.endsWith(".debug")) {
      paramContext = str.substring(0, str.length() - 6);
    }
    return String.format("market://details?id=%s", new Object[] { paramContext });
  }
  
  public static boolean c(Context paramContext, String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder("geo:0,0?q=");
    localStringBuilder.append(Uri.encode(paramString));
    return a(paramContext, new Intent("android.intent.action.VIEW", Uri.parse(localStringBuilder.toString())));
  }
  
  public static boolean d(Context paramContext)
  {
    return a(Uri.parse("market://details?id=%s")).resolveActivity(paramContext.getPackageManager()) != null;
  }
  
  public static boolean d(Context paramContext, String paramString)
  {
    return (TextUtils.isEmpty(paramString)) || (a(paramContext, a(Uri.parse(paramString))));
  }
  
  public static boolean e(Context paramContext, String paramString)
  {
    return a(paramContext, new Intent("android.intent.action.SENDTO", Uri.fromParts("mailto", paramString, null)));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */