package com.truecaller.common.h;

import c.g.a.b;
import c.g.b.j;
import c.g.b.w;
import c.l.c;
import c.n.m;

final class v$b
  extends j
  implements b<CharSequence, Boolean>
{
  public static final b a = new b();
  
  public final c a()
  {
    return w.a(m.class, "common_release");
  }
  
  public final String b()
  {
    return "isNotBlank";
  }
  
  public final String c()
  {
    return "isNotBlank(Ljava/lang/CharSequence;)Z";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.v.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */