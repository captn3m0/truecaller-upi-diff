package com.truecaller.common.h;

import c.n.m;
import com.google.c.a.m.a;
import com.truecaller.common.account.r;
import com.truecaller.common.c;
import com.truecaller.common.network.KnownDomain;
import java.util.Collection;
import java.util.Iterator;
import javax.inject.Inject;

public final class ad
  implements ac
{
  private final com.truecaller.common.g.a a;
  private final dagger.a<r> b;
  
  @Inject
  public ad(com.truecaller.common.g.a parama, dagger.a<r> parama1)
  {
    a = parama;
    b = parama1;
  }
  
  public final boolean a()
  {
    if (a.e("featureRegion1_qa")) {
      return a.b("featureRegion1_qa");
    }
    if (a.a("key_region_1_timestamp", 0L) > 0L) {
      return a.b("featureRegion1");
    }
    String str2 = ((r)b.get()).a();
    String str1 = str2;
    if (str2 == null) {
      str1 = a.a("profileCountryIso");
    }
    if (str1 != null) {
      return b(str1);
    }
    return true;
  }
  
  public final boolean a(String paramString)
  {
    c.g.b.k.b(paramString, "normalizedNumber");
    Object localObject2 = com.google.c.a.k.a();
    localObject1 = null;
    try
    {
      localObject2 = ((com.google.c.a.k)localObject2).b(ab);
      paramString = (String)localObject1;
      if (localObject2 != null)
      {
        boolean bool = b((String)localObject2);
        paramString = Boolean.valueOf(bool);
      }
    }
    catch (Exception paramString)
    {
      for (;;)
      {
        paramString = (String)localObject1;
      }
    }
    if (paramString != null) {
      return paramString.booleanValue();
    }
    return true;
  }
  
  public final KnownDomain b()
  {
    if (a()) {
      return KnownDomain.DOMAIN_REGION_1;
    }
    return KnownDomain.DOMAIN_OTHER_REGIONS;
  }
  
  public final boolean b(String paramString)
  {
    c.g.b.k.b(paramString, "countryIso");
    Object localObject = (Iterable)c.a();
    if ((!(localObject instanceof Collection)) || (!((Collection)localObject).isEmpty()))
    {
      localObject = ((Iterable)localObject).iterator();
      while (((Iterator)localObject).hasNext()) {
        if (m.a((String)((Iterator)localObject).next(), paramString, true)) {
          return true;
        }
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.ad
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */