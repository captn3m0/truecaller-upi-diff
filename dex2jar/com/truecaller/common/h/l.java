package com.truecaller.common.h;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.Build.VERSION;
import android.util.TypedValue;
import com.truecaller.common.e.f;

public class l
{
  private static final android.support.v4.e.a a = ;
  
  public static int a(Context paramContext)
  {
    return (int)paramContext.getResources().getDimension(2131165925);
  }
  
  public static int a(Context paramContext, float paramFloat)
  {
    return (int)TypedValue.applyDimension(1, paramFloat, paramContext.getResources().getDisplayMetrics());
  }
  
  public static Bitmap a(Drawable paramDrawable)
  {
    if (paramDrawable == null) {
      return null;
    }
    if ((paramDrawable instanceof BitmapDrawable)) {
      return ((BitmapDrawable)paramDrawable).getBitmap();
    }
    boolean bool = paramDrawable instanceof ShapeDrawable;
    int j = 1;
    int i;
    if ((!bool) && (!(paramDrawable instanceof GradientDrawable)))
    {
      if (paramDrawable.getIntrinsicWidth() > 0)
      {
        if (paramDrawable.getIntrinsicHeight() <= 0) {
          return null;
        }
        j = paramDrawable.getIntrinsicWidth();
        i = paramDrawable.getIntrinsicHeight();
      }
      else
      {
        return null;
      }
    }
    else {
      i = 1;
    }
    Bitmap localBitmap = Bitmap.createBitmap(j, i, Bitmap.Config.ARGB_8888);
    Canvas localCanvas = new Canvas(localBitmap);
    paramDrawable.setBounds(0, 0, localCanvas.getWidth(), localCanvas.getHeight());
    paramDrawable.draw(localCanvas);
    return localBitmap;
  }
  
  public static Drawable a(Context paramContext, int paramInt)
  {
    Resources localResources = paramContext.getResources();
    if (Build.VERSION.SDK_INT >= 21) {
      return localResources.getDrawable(paramInt, paramContext.getTheme());
    }
    return localResources.getDrawable(paramInt);
  }
  
  public static Drawable a(Context paramContext, int paramInt1, int paramInt2)
  {
    paramContext = android.support.v4.graphics.drawable.a.e(a(paramContext, paramInt1).mutate());
    android.support.v4.graphics.drawable.a.a(paramContext, paramInt2);
    return paramContext;
  }
  
  public static String a(CharSequence paramCharSequence)
  {
    if (am.b(paramCharSequence)) {
      return "";
    }
    if (!f.b()) {
      return paramCharSequence.toString();
    }
    return a.a(paramCharSequence.toString());
  }
  
  public static int b(Context paramContext, float paramFloat)
  {
    return (int)TypedValue.applyDimension(2, paramFloat, paramContext.getResources().getDisplayMetrics());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */