package com.truecaller.common.h;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.truecaller.utils.c;
import com.truecaller.utils.t;
import com.truecaller.utils.t.a;
import java.util.Locale;
import org.c.a.a.a.c.e;

public final class k
{
  private static int a = -1;
  private static int b = -1;
  
  public static String a()
  {
    return am.n(Build.MODEL).trim();
  }
  
  public static boolean a(Context paramContext)
  {
    return c.a().a(paramContext).a().c().d("com.truecaller.qa");
  }
  
  public static long b(Context paramContext)
  {
    try
    {
      long l = getPackageManagergetPackageInfogetPackageName0firstInstallTime;
      return l;
    }
    catch (Exception paramContext)
    {
      com.truecaller.log.d.a(paramContext);
    }
    return 0L;
  }
  
  public static String b()
  {
    return am.n(Build.MANUFACTURER).trim();
  }
  
  public static String c()
  {
    String str1 = a();
    String str2 = b();
    Object localObject = str1;
    if (!str1.toLowerCase(Locale.ENGLISH).startsWith(str2.toLowerCase(Locale.ENGLISH)))
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append(str2);
      ((StringBuilder)localObject).append(" ");
      ((StringBuilder)localObject).append(str1);
      localObject = ((StringBuilder)localObject).toString();
    }
    if (am.a((CharSequence)localObject)) {
      return e.a((String)localObject);
    }
    return "Unknown";
  }
  
  public static String c(Context paramContext)
  {
    return ((TelephonyManager)paramContext.getSystemService("phone")).getLine1Number();
  }
  
  public static String d(Context paramContext)
  {
    return ((TelephonyManager)paramContext.getSystemService("phone")).getNetworkOperatorName();
  }
  
  @Deprecated
  public static boolean d()
  {
    return Build.VERSION.SDK_INT >= 21;
  }
  
  public static String e(Context paramContext)
  {
    return ((TelephonyManager)paramContext.getSystemService("phone")).getSubscriberId();
  }
  
  @Deprecated
  public static boolean e()
  {
    return Build.VERSION.SDK_INT >= 22;
  }
  
  public static String f(Context paramContext)
  {
    TelephonyManager localTelephonyManager = (TelephonyManager)paramContext.getSystemService("phone");
    String str = localTelephonyManager.getNetworkCountryIso();
    paramContext = str;
    if (TextUtils.isEmpty(str)) {
      paramContext = localTelephonyManager.getSimCountryIso();
    }
    return am.c(paramContext, Locale.ENGLISH);
  }
  
  @Deprecated
  public static boolean f()
  {
    return Build.VERSION.SDK_INT >= 23;
  }
  
  public static String g(Context paramContext)
  {
    TelephonyManager localTelephonyManager = (TelephonyManager)paramContext.getSystemService("phone");
    String str = localTelephonyManager.getSimCountryIso();
    paramContext = str;
    if (TextUtils.isEmpty(str)) {
      paramContext = localTelephonyManager.getNetworkCountryIso();
    }
    return am.c(paramContext, Locale.ENGLISH);
  }
  
  @Deprecated
  public static boolean g()
  {
    return Build.VERSION.SDK_INT >= 24;
  }
  
  public static int h(Context paramContext)
  {
    return ((ActivityManager)paramContext.getSystemService("activity")).getLargeMemoryClass();
  }
  
  @Deprecated
  public static boolean h()
  {
    return Build.VERSION.SDK_INT >= 26;
  }
  
  @SuppressLint({"HardwareIds"})
  public static String i(Context paramContext)
  {
    return Settings.Secure.getString(paramContext.getContentResolver(), "android_id");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */