package com.truecaller.common.h;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import c.u;
import com.bumptech.glide.load.b.a.e;
import com.truecaller.common.R.dimen;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.MessageDigest;

public final class p
  extends b
{
  private final Context b;
  private final int c;
  private final int d;
  private final String e;
  
  public p(Context paramContext, int paramInt1, int paramInt2, String paramString)
  {
    super("com.truecaller.common.util.LabelTransformation");
    b = paramContext;
    c = paramInt1;
    d = paramInt2;
    e = paramString;
  }
  
  public final Bitmap a(e parame, Bitmap paramBitmap, int paramInt1, int paramInt2)
  {
    c.g.b.k.b(parame, "pool");
    c.g.b.k.b(paramBitmap, "toTransform");
    paramInt1 = b.getResources().getDimensionPixelSize(R.dimen.label_padding_small);
    paramInt2 = b.getResources().getDimensionPixelSize(R.dimen.label_padding_medium);
    parame = new Paint();
    parame.setColor(c);
    TextPaint localTextPaint = new TextPaint();
    localTextPaint.setColor(d);
    localTextPaint.setAntiAlias(true);
    localTextPaint.setTypeface(Typeface.create("sans-serif-medium", 0));
    localTextPaint.setTextSize(b.getResources().getDimensionPixelSize(R.dimen.label_text_size));
    int i = paramBitmap.getWidth();
    String str = TextUtils.ellipsize((CharSequence)e, localTextPaint, i - paramInt2 * 2, TextUtils.TruncateAt.END).toString();
    Rect localRect = new Rect();
    localTextPaint.getTextBounds(str, 0, str.length(), localRect);
    paramBitmap = paramBitmap.copy(Bitmap.Config.ARGB_8888, true);
    Canvas localCanvas = new Canvas(paramBitmap);
    int j = localRect.height();
    localCanvas.drawRect(0.0F, 0.0F, i, j + paramInt1 * 2, parame);
    i = localRect.height();
    localCanvas.drawText(str, paramInt2, i + paramInt1, (Paint)localTextPaint);
    c.g.b.k.a(paramBitmap, "resultBitmap");
    return paramBitmap;
  }
  
  public final void a(MessageDigest paramMessageDigest)
  {
    c.g.b.k.b(paramMessageDigest, "messageDigest");
    super.a(paramMessageDigest);
    paramMessageDigest.update(ByteBuffer.allocate(8).putInt(c).putInt(d).array());
    Object localObject = e;
    Charset localCharset = Charset.forName("UTF-8");
    c.g.b.k.a(localCharset, "Charset.forName(\"UTF-8\")");
    if (localObject != null)
    {
      localObject = ((String)localObject).getBytes(localCharset);
      c.g.b.k.a(localObject, "(this as java.lang.String).getBytes(charset)");
      paramMessageDigest.update((byte[])localObject);
      return;
    }
    throw new u("null cannot be cast to non-null type java.lang.String");
  }
  
  public final boolean equals(Object paramObject)
  {
    if (super.equals(paramObject)) {
      if (paramObject != null)
      {
        paramObject = (p)paramObject;
        if ((c == c) && (d == d) && (c.g.b.k.a(e, e))) {
          return true;
        }
      }
      else
      {
        throw new u("null cannot be cast to non-null type com.truecaller.common.util.LabelTransformation");
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    return a(new int[] { super.hashCode(), com.bumptech.glide.g.k.b(c), com.bumptech.glide.g.k.b(d), e.hashCode() });
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */