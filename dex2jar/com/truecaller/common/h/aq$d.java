package com.truecaller.common.h;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import com.d.b.ai;

public class aq$d
  implements ai
{
  private static final d a = new d();
  
  public static d b()
  {
    return a;
  }
  
  public final Bitmap a(Bitmap paramBitmap)
  {
    Bitmap localBitmap = Bitmap.createBitmap(paramBitmap.getWidth(), paramBitmap.getHeight(), Bitmap.Config.ARGB_8888);
    Canvas localCanvas = new Canvas(localBitmap);
    Paint localPaint = new Paint();
    localPaint.setAntiAlias(true);
    a(localCanvas, paramBitmap, localPaint, new RectF(0.0F, 0.0F, localBitmap.getWidth(), localBitmap.getHeight()));
    paramBitmap.recycle();
    return localBitmap;
  }
  
  public String a()
  {
    return "RoundedImageTransformation";
  }
  
  void a(Canvas paramCanvas, Bitmap paramBitmap, Paint paramPaint, RectF paramRectF)
  {
    Shader.TileMode localTileMode = Shader.TileMode.CLAMP;
    paramPaint.setShader(new BitmapShader(paramBitmap, localTileMode, localTileMode));
    paramCanvas.drawOval(paramRectF, paramPaint);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.aq.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */