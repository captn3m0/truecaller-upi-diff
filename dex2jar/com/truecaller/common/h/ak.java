package com.truecaller.common.h;

import android.content.Context;
import android.telephony.TelephonyManager;
import c.f;
import c.g;
import c.g.a.a;
import c.g.b.k;
import c.g.b.l;
import c.u;
import com.truecaller.utils.n;
import javax.inject.Inject;

public final class ak
  implements aj
{
  private final f b;
  private final Context c;
  private final n d;
  
  @Inject
  public ak(Context paramContext, n paramn)
  {
    c = paramContext;
    d = paramn;
    b = g.a((a)new a(this));
  }
  
  private final String b()
  {
    return (String)b.b();
  }
  
  public final String a()
  {
    Integer localInteger = Integer.valueOf(ab.a());
    int i;
    if (((Number)localInteger).intValue() != 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i == 0) {
      localInteger = null;
    }
    if (localInteger != null)
    {
      i = ((Number)localInteger).intValue();
      return d.a(i, new Object[0]);
    }
    return null;
  }
  
  public final boolean a(CharSequence paramCharSequence)
  {
    k.b(paramCharSequence, "phoneNumber");
    return ab.b(paramCharSequence);
  }
  
  public final boolean a(String paramString)
  {
    return ab.a(c, paramString);
  }
  
  public final boolean a(String... paramVarArgs)
  {
    k.b(paramVarArgs, "phoneNumbers");
    Object localObject = (CharSequence)b();
    int i;
    if ((localObject != null) && (((CharSequence)localObject).length() != 0)) {
      i = 0;
    } else {
      i = 1;
    }
    if (i == 0)
    {
      int j = paramVarArgs.length;
      i = 0;
      while (i < j)
      {
        localObject = paramVarArgs[i];
        if (k.a(b(), localObject))
        {
          i = 1;
          break label85;
        }
        i += 1;
      }
      i = 0;
      label85:
      if (i != 0) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean b(String paramString)
  {
    k.b(paramString, "phoneNumber");
    return ab.a((CharSequence)paramString);
  }
  
  static final class a
    extends l
    implements a<String>
  {
    a(ak paramak)
    {
      super();
    }
    
    private String a()
    {
      try
      {
        Object localObject = ak.a(a).getSystemService("phone");
        if (localObject != null) {
          return ((TelephonyManager)localObject).getVoiceMailNumber();
        }
        throw new u("null cannot be cast to non-null type android.telephony.TelephonyManager");
      }
      catch (SecurityException localSecurityException)
      {
        for (;;) {}
      }
      return null;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.ak
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */