package com.truecaller.common.h;

import c.g.b.k;

public final class au
{
  public final Integer a;
  public final Integer b;
  public final Integer c;
  
  public au(Integer paramInteger1, Integer paramInteger2, Integer paramInteger3)
  {
    a = paramInteger1;
    b = paramInteger2;
    c = paramInteger3;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject) {
      if ((paramObject instanceof au))
      {
        paramObject = (au)paramObject;
        if ((k.a(a, a)) && (k.a(b, b)) && (k.a(c, c))) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    Integer localInteger = a;
    int k = 0;
    int i;
    if (localInteger != null) {
      i = localInteger.hashCode();
    } else {
      i = 0;
    }
    localInteger = b;
    int j;
    if (localInteger != null) {
      j = localInteger.hashCode();
    } else {
      j = 0;
    }
    localInteger = c;
    if (localInteger != null) {
      k = localInteger.hashCode();
    }
    return (i * 31 + j) * 31 + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("Version(major=");
    localStringBuilder.append(a);
    localStringBuilder.append(", minor=");
    localStringBuilder.append(b);
    localStringBuilder.append(", build=");
    localStringBuilder.append(c);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.au
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */