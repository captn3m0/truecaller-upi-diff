package com.truecaller.common.h;

import c.a.m;
import c.g.a.b;
import c.m.i;
import c.n;
import com.google.c.a.k.c;
import com.google.c.a.m.a;
import com.truecaller.common.c;
import com.truecaller.common.network.e;
import com.truecaller.common.network.e.a;
import com.truecaller.common.network.e.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;

public final class s
  implements r
{
  private final ac a;
  private final dagger.a<com.google.c.a.k> b;
  
  @Inject
  public s(ac paramac, dagger.a<com.google.c.a.k> parama)
  {
    a = paramac;
    b = parama;
  }
  
  public final e a(m.a parama)
  {
    c.g.b.k.b(parama, "number");
    com.google.c.a.k localk = (com.google.c.a.k)b.get();
    String str = localk.d(parama);
    if (!localk.a(parama, str)) {
      return (e)e.a.a;
    }
    c.g.b.k.a(str, "regionCodeForNumber");
    parama = c.a(str);
    if (parama != a.b()) {
      return (e)new e.b(parama);
    }
    return (e)e.a.a;
  }
  
  public final Map<e, Collection<String>> a(Iterable<? extends m.a> paramIterable)
  {
    c.g.b.k.b(paramIterable, "numbers");
    com.google.c.a.k localk = (com.google.c.a.k)b.get();
    Object localObject3 = a.b();
    Object localObject2 = c.m.l.c(m.n(paramIterable), (b)new a(localk));
    paramIterable = new ArrayList();
    Object localObject1 = new ArrayList();
    localObject2 = ((i)localObject2).a();
    while (((Iterator)localObject2).hasNext())
    {
      localObject4 = ((Iterator)localObject2).next();
      localObject5 = (n)localObject4;
      if (localk.a((m.a)a, (String)b)) {
        paramIterable.add(localObject4);
      } else {
        ((ArrayList)localObject1).add(localObject4);
      }
    }
    localObject1 = new n(paramIterable, localObject1);
    paramIterable = (List)a;
    Object localObject6 = (List)b;
    Object localObject4 = (Map)new LinkedHashMap();
    Object localObject5 = (c.g.a.a)b.a;
    Iterator localIterator = ((Iterable)paramIterable).iterator();
    while (localIterator.hasNext())
    {
      paramIterable = (n)localIterator.next();
      m.a locala = (m.a)a;
      paramIterable = (String)b;
      c.g.b.k.a(paramIterable, "regionCode");
      paramIterable = c.a(paramIterable);
      int i;
      if (paramIterable == localObject3) {
        i = 1;
      } else {
        i = 0;
      }
      if (i != 0) {
        paramIterable = null;
      }
      if (paramIterable != null) {
        paramIterable = (e)new e.b(paramIterable);
      } else {
        paramIterable = (e)e.a.a;
      }
      localObject2 = ((Map)localObject4).get(paramIterable);
      localObject1 = localObject2;
      if (localObject2 == null)
      {
        localObject1 = ((c.g.a.a)localObject5).invoke();
        ((Map)localObject4).put(paramIterable, localObject1);
      }
      paramIterable = (List)localObject1;
      localObject1 = localk.a(locala, k.c.a);
      c.g.b.k.a(localObject1, "phoneNumberUtil.format(number, E164)");
      paramIterable.add(localObject1);
    }
    localObject2 = ((Iterable)localObject6).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject3 = (m.a)nexta;
      localObject6 = e.a.a;
      localObject1 = ((Map)localObject4).get(localObject6);
      paramIterable = (Iterable<? extends m.a>)localObject1;
      if (localObject1 == null)
      {
        paramIterable = ((c.g.a.a)localObject5).invoke();
        ((Map)localObject4).put(localObject6, paramIterable);
      }
      paramIterable = (List)paramIterable;
      localObject1 = localk.a((m.a)localObject3, k.c.a);
      c.g.b.k.a(localObject1, "phoneNumberUtil.format(number, E164)");
      paramIterable.add(localObject1);
    }
    return (Map<e, Collection<String>>)localObject4;
  }
  
  static final class a
    extends c.g.b.l
    implements b<m.a, n<? extends m.a, ? extends String>>
  {
    a(com.google.c.a.k paramk)
    {
      super();
    }
  }
  
  static final class b
    extends c.g.b.l
    implements c.g.a.a<List<String>>
  {
    public static final b a = new b();
    
    b()
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.s
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */