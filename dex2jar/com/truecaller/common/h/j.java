package com.truecaller.common.h;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.text.format.DateUtils;
import com.truecaller.common.R.string;
import com.truecaller.common.e.b.a;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import org.a.a.d.i.a;
import org.a.a.x;

public final class j
{
  protected static java.text.DateFormat a = null;
  protected static java.text.DateFormat b = null;
  private static final long c = TimeUnit.DAYS.toMillis(1L);
  private static final long d = TimeUnit.DAYS.toHours(1L);
  private static final long e = TimeUnit.MINUTES.toSeconds(1L);
  private static final long f = TimeUnit.HOURS.toSeconds(1L);
  private static final StringBuilder g = new StringBuilder(32);
  private static final Formatter h = new Formatter(g, com.truecaller.common.e.f.a());
  private static final SimpleDateFormat i = new SimpleDateFormat("yyyy-MM-dd");
  private static final SimpleDateFormat j = new SimpleDateFormat("HH:mm");
  private static final SimpleDateFormat k = new SimpleDateFormat("dd/MM");
  private static final SimpleDateFormat l = new SimpleDateFormat("MM/dd");
  
  public static int a()
  {
    Calendar localCalendar = Calendar.getInstance();
    return localCalendar.get(11) * 60 + localCalendar.get(12);
  }
  
  public static CharSequence a(Context paramContext, long paramLong, boolean paramBoolean)
  {
    try
    {
      g.setLength(0);
      long l2 = System.currentTimeMillis();
      long l1 = TimeZone.getDefault().getOffset(l2);
      l2 = (l2 + l1) / c;
      l1 = (l1 + paramLong) / c;
      if (l2 == l1)
      {
        paramContext = f(paramContext, paramLong);
        return paramContext;
      }
      String str1;
      if ((!paramBoolean) && (l2 - l1 == 1L)) {
        str1 = am.b(paramContext.getResources().getString(R.string.yesterday), com.truecaller.common.e.f.a());
      } else if (l2 - l1 >= 7L) {
        str1 = d(paramContext, paramLong);
      } else {
        str1 = DateUtils.formatDateRange(paramContext, h, paramLong, paramLong, 32770).toString();
      }
      String str2 = str1;
      if (paramBoolean) {
        str2 = String.format("%s, %s", new Object[] { f(paramContext, paramLong), str1 });
      }
      return str2;
    }
    finally {}
  }
  
  public static String a(long paramLong)
  {
    if (paramLong == 0L) {
      return "";
    }
    return String.valueOf(DateUtils.getRelativeTimeSpanString(paramLong, System.currentTimeMillis(), 60000L, 524288));
  }
  
  @SuppressLint({"StringFormatInvalid"})
  public static String a(Context paramContext, int paramInt)
  {
    if (paramInt < 0) {
      return "";
    }
    long l1 = paramInt;
    long l2 = e;
    if (l1 < l2) {
      return paramContext.getString(R.string.duration_sec, new Object[] { Integer.valueOf(paramInt) });
    }
    long l3 = f;
    if (l1 < l3)
    {
      l1 /= l2;
      return paramContext.getString(R.string.duration_min, new Object[] { Long.valueOf(l1) });
    }
    l1 /= l3;
    l2 = d;
    return paramContext.getString(R.string.duration_hour, new Object[] { Long.valueOf(l1 % l2) });
  }
  
  public static String a(Context paramContext, long paramLong)
  {
    Calendar localCalendar1 = Calendar.getInstance(com.truecaller.common.e.f.a());
    localCalendar1.setTimeInMillis(paramLong);
    paramLong = (System.currentTimeMillis() - paramLong) / 1000L;
    if (paramLong < TimeUnit.MINUTES.toSeconds(1L)) {
      return paramContext.getResources().getString(R.string.now);
    }
    if (paramLong < TimeUnit.MINUTES.toSeconds(10L))
    {
      paramLong = TimeUnit.SECONDS.toMinutes(paramLong);
      return paramContext.getResources().getString(R.string.n_minutes_ago, new Object[] { Long.valueOf(paramLong) });
    }
    Object localObject = Calendar.getInstance(com.truecaller.common.e.f.a());
    Calendar localCalendar2 = Calendar.getInstance(com.truecaller.common.e.f.a());
    localCalendar2.add(6, -1);
    Calendar localCalendar3 = Calendar.getInstance(com.truecaller.common.e.f.a());
    localCalendar3.add(6, -7);
    boolean bool = com.truecaller.common.e.f.c();
    if ((localCalendar1.get(1) == ((Calendar)localObject).get(1)) && (localCalendar1.get(6) == ((Calendar)localObject).get(6)))
    {
      localObject = android.text.format.DateFormat.getTimeFormat(paramContext);
      paramContext = (Context)localObject;
      if (localObject == null) {
        paramContext = java.text.DateFormat.getTimeInstance(3, com.truecaller.common.e.f.a());
      }
      return paramContext.format(localCalendar1.getTime());
    }
    if (localCalendar1.get(6) == localCalendar2.get(6)) {
      return paramContext.getResources().getString(R.string.yesterday);
    }
    if (localCalendar1.after(localCalendar3)) {
      return new SimpleDateFormat("EEEE", com.truecaller.common.e.f.a()).format(localCalendar1.getTime());
    }
    if (localCalendar1.get(1) == ((Calendar)localObject).get(1))
    {
      if (bool)
      {
        paramContext = com.truecaller.common.e.b.a(new b.a(localCalendar1.get(1), localCalendar1.get(2), localCalendar1.get(5)));
        localObject = paramContext.a();
        return String.format("%d %s", new Object[] { Integer.valueOf(c), localObject });
      }
      paramContext = new SimpleDateFormat("dd MMM", com.truecaller.common.e.f.a());
      paramContext.setCalendar(localCalendar1);
      return paramContext.format(localCalendar1.getTime());
    }
    if (bool)
    {
      paramContext = com.truecaller.common.e.b.a(new b.a(localCalendar1.get(1), localCalendar1.get(2), localCalendar1.get(5)));
      localObject = paramContext.a();
      return String.format("%d %s %d", new Object[] { Integer.valueOf(c), localObject, Integer.valueOf(a) });
    }
    paramContext = java.text.DateFormat.getDateInstance(2, com.truecaller.common.e.f.a());
    paramContext.setCalendar(localCalendar1);
    return paramContext.format(localCalendar1.getTime());
  }
  
  public static org.a.a.b a(String paramString)
  {
    if (!TextUtils.isEmpty(paramString)) {}
    try
    {
      paramString = i.a.a().a(org.a.a.f.a).b(paramString);
      return paramString;
    }
    catch (Exception paramString)
    {
      for (;;) {}
    }
    return null;
  }
  
  @Deprecated
  public static boolean a(long paramLong1, long paramLong2)
  {
    return System.currentTimeMillis() - paramLong1 > paramLong2;
  }
  
  public static CharSequence b(Context paramContext, long paramLong)
  {
    try
    {
      g.setLength(0);
      long l1 = Math.abs(System.currentTimeMillis() - paramLong) / 60000L;
      if (l1 == 0L)
      {
        paramContext = paramContext.getString(R.string.now);
        return paramContext;
      }
      if (l1 <= 10L)
      {
        paramContext = DateUtils.getRelativeTimeSpanString(paramLong, System.currentTimeMillis(), 60000L, 524288);
        return paramContext;
      }
      paramContext = DateUtils.formatDateRange(paramContext, h, paramLong, paramLong, 524289).toString();
      return paramContext;
    }
    finally {}
  }
  
  public static void b()
  {
    a = null;
    b = null;
  }
  
  public static String c()
  {
    int m = org.a.a.f.a().b(org.a.a.b.ay_().a());
    TimeUnit localTimeUnit = TimeUnit.MILLISECONDS;
    long l1 = m;
    m = (int)localTimeUnit.toHours(l1);
    int n = Math.abs((int)(TimeUnit.MILLISECONDS.toMinutes(l1) - m * TimeUnit.HOURS.toMinutes(1L)));
    return String.format(Locale.ENGLISH, "GMT%+03d:%02d", new Object[] { Integer.valueOf(m), Integer.valueOf(n) });
  }
  
  @SuppressLint({"StringFormatInvalid"})
  public static String c(Context paramContext, long paramLong)
  {
    if (paramLong < 0L) {
      return "";
    }
    long l1 = e;
    if (paramLong < l1) {
      return paramContext.getString(R.string.duration_s, new Object[] { Long.valueOf(paramLong) });
    }
    long l2 = f;
    if (paramLong < l2)
    {
      l2 = paramLong / l1;
      return paramContext.getString(R.string.duration_ms, new Object[] { Long.valueOf(l2), Long.valueOf(paramLong % l1) });
    }
    l2 = paramLong / l2;
    long l3 = d;
    long l4 = paramLong / l1;
    return paramContext.getString(R.string.duration_hms, new Object[] { Long.valueOf(l2 % l3), Long.valueOf(l4 % l1), Long.valueOf(paramLong % l1) });
  }
  
  /* Error */
  public static String d(Context paramContext, long paramLong)
  {
    // Byte code:
    //   0: ldc 2
    //   2: monitorenter
    //   3: getstatic 96	com/truecaller/common/h/j:a	Ljava/text/DateFormat;
    //   6: ifnonnull +10 -> 16
    //   9: aload_0
    //   10: invokestatic 388	android/text/format/DateFormat:getDateFormat	(Landroid/content/Context;)Ljava/text/DateFormat;
    //   13: putstatic 96	com/truecaller/common/h/j:a	Ljava/text/DateFormat;
    //   16: getstatic 96	com/truecaller/common/h/j:a	Ljava/text/DateFormat;
    //   19: new 390	java/util/Date
    //   22: dup
    //   23: lload_1
    //   24: invokespecial 392	java/util/Date:<init>	(J)V
    //   27: invokevirtual 270	java/text/DateFormat:format	(Ljava/util/Date;)Ljava/lang/String;
    //   30: astore_0
    //   31: ldc 2
    //   33: monitorexit
    //   34: aload_0
    //   35: areturn
    //   36: astore_0
    //   37: goto +29 -> 66
    //   40: getstatic 82	com/truecaller/common/h/j:i	Ljava/text/SimpleDateFormat;
    //   43: new 390	java/util/Date
    //   46: dup
    //   47: lload_1
    //   48: invokespecial 392	java/util/Date:<init>	(J)V
    //   51: invokevirtual 393	java/text/SimpleDateFormat:format	(Ljava/util/Date;)Ljava/lang/String;
    //   54: astore_0
    //   55: ldc 2
    //   57: monitorexit
    //   58: aload_0
    //   59: areturn
    //   60: ldc 2
    //   62: monitorexit
    //   63: ldc -74
    //   65: areturn
    //   66: ldc 2
    //   68: monitorexit
    //   69: aload_0
    //   70: athrow
    //   71: astore_0
    //   72: goto -32 -> 40
    //   75: astore_0
    //   76: goto -16 -> 60
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	79	0	paramContext	Context
    //   0	79	1	paramLong	long
    // Exception table:
    //   from	to	target	type
    //   3	16	36	finally
    //   16	31	36	finally
    //   40	55	36	finally
    //   3	16	71	java/lang/Exception
    //   16	31	71	java/lang/Exception
    //   40	55	75	java/lang/Exception
  }
  
  public static String e(Context paramContext, long paramLong)
  {
    int m = 77;
    try
    {
      paramContext = android.text.format.DateFormat.getDateFormatOrder(paramContext);
      int i1 = paramContext.length;
      m = 0;
      while (m < i1)
      {
        int n = paramContext[m];
        if ((n != 100) && (n != 77))
        {
          m += 1;
        }
        else
        {
          m = n;
          break label61;
        }
      }
      m = 100;
    }
    catch (IllegalArgumentException paramContext)
    {
      label61:
      for (;;) {}
    }
    if (m == 100) {
      paramContext = k;
    } else {
      paramContext = l;
    }
    return paramContext.format(new Date(paramLong));
  }
  
  /* Error */
  public static String f(Context paramContext, long paramLong)
  {
    // Byte code:
    //   0: ldc 2
    //   2: monitorenter
    //   3: getstatic 98	com/truecaller/common/h/j:b	Ljava/text/DateFormat;
    //   6: ifnonnull +10 -> 16
    //   9: aload_0
    //   10: invokestatic 257	android/text/format/DateFormat:getTimeFormat	(Landroid/content/Context;)Ljava/text/DateFormat;
    //   13: putstatic 98	com/truecaller/common/h/j:b	Ljava/text/DateFormat;
    //   16: getstatic 98	com/truecaller/common/h/j:b	Ljava/text/DateFormat;
    //   19: new 390	java/util/Date
    //   22: dup
    //   23: lload_1
    //   24: invokespecial 392	java/util/Date:<init>	(J)V
    //   27: invokevirtual 270	java/text/DateFormat:format	(Ljava/util/Date;)Ljava/lang/String;
    //   30: astore_0
    //   31: ldc 2
    //   33: monitorexit
    //   34: aload_0
    //   35: areturn
    //   36: astore_0
    //   37: goto +29 -> 66
    //   40: getstatic 86	com/truecaller/common/h/j:j	Ljava/text/SimpleDateFormat;
    //   43: new 390	java/util/Date
    //   46: dup
    //   47: lload_1
    //   48: invokespecial 392	java/util/Date:<init>	(J)V
    //   51: invokevirtual 393	java/text/SimpleDateFormat:format	(Ljava/util/Date;)Ljava/lang/String;
    //   54: astore_0
    //   55: ldc 2
    //   57: monitorexit
    //   58: aload_0
    //   59: areturn
    //   60: ldc 2
    //   62: monitorexit
    //   63: ldc -74
    //   65: areturn
    //   66: ldc 2
    //   68: monitorexit
    //   69: aload_0
    //   70: athrow
    //   71: astore_0
    //   72: goto -32 -> 40
    //   75: astore_0
    //   76: goto -16 -> 60
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	79	0	paramContext	Context
    //   0	79	1	paramLong	long
    // Exception table:
    //   from	to	target	type
    //   3	16	36	finally
    //   16	31	36	finally
    //   40	55	36	finally
    //   3	16	71	java/lang/Exception
    //   16	31	71	java/lang/Exception
    //   40	55	75	java/lang/Exception
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */