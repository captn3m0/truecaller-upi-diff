package com.truecaller.common.h;

import android.graphics.Bitmap;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import com.d.b.ai;
import com.truecaller.common.b.a;
import com.truecaller.log.d;
import com.truecaller.utils.extensions.b;
import java.lang.reflect.Array;

public class aq$b
  implements ai
{
  private static boolean a;
  private static RenderScript b;
  private final float c = 25.0F;
  
  private static RenderScript b()
  {
    if (!a) {
      try
      {
        if (!a)
        {
          a = true;
          try
          {
            b = RenderScript.create(a.F());
          }
          catch (RuntimeException localRuntimeException)
          {
            d.a(localRuntimeException);
          }
        }
      }
      finally {}
    }
    return b;
  }
  
  public final Bitmap a(Bitmap paramBitmap)
  {
    int i = paramBitmap.getWidth();
    int j = paramBitmap.getHeight();
    if ((i >= 8) && (j >= 8))
    {
      Object localObject4 = b();
      Object localObject3;
      if (localObject4 != null) {
        try
        {
          i = paramBitmap.getWidth();
          j = paramBitmap.getHeight();
          Bitmap localBitmap = Bitmap.createScaledBitmap(paramBitmap, i / 4, j / 4, true);
          localObject2 = Allocation.createFromBitmap((RenderScript)localObject4, localBitmap);
          localObject3 = Allocation.createTyped((RenderScript)localObject4, ((Allocation)localObject2).getType());
          localObject4 = ScriptIntrinsicBlur.create((RenderScript)localObject4, Element.U8_4((RenderScript)localObject4));
          ((ScriptIntrinsicBlur)localObject4).setRadius(c);
          ((ScriptIntrinsicBlur)localObject4).setInput((Allocation)localObject2);
          ((ScriptIntrinsicBlur)localObject4).forEach((Allocation)localObject3);
          ((Allocation)localObject3).copyTo(localBitmap);
          localObject2 = Bitmap.createScaledBitmap(localBitmap, i, j, true);
          if (localObject2 != localBitmap) {
            localBitmap.recycle();
          }
          if (localObject2 != paramBitmap) {
            paramBitmap.recycle();
          }
          return (Bitmap)localObject2;
        }
        catch (RuntimeException localRuntimeException)
        {
          d.a(localRuntimeException, "Could not blur image");
        }
      }
      int i14 = paramBitmap.getWidth();
      int i15 = paramBitmap.getHeight();
      Object localObject2 = Bitmap.createScaledBitmap(paramBitmap, i14 / 4, i15 / 4, true);
      int i7 = (int)c;
      if (i7 <= 0)
      {
        localObject1 = localObject2;
        localObject4 = localObject1;
      }
      else
      {
        localObject1 = ((Bitmap)localObject2).copy(b.a((Bitmap)localObject2), true);
        int i16 = ((Bitmap)localObject1).getWidth();
        i = ((Bitmap)localObject1).getHeight();
        j = i16 * i;
        int[] arrayOfInt2 = new int[j];
        ((Bitmap)localObject1).getPixels(arrayOfInt2, 0, i16, 0, 0, i16, i);
        int i20 = i16 - 1;
        int i17 = i - 1;
        int i18 = i7 + i7 + 1;
        int[] arrayOfInt3 = new int[j];
        int[] arrayOfInt4 = new int[j];
        int[] arrayOfInt5 = new int[j];
        int[] arrayOfInt1 = new int[Math.max(i16, i)];
        j = i18 + 1 >> 1;
        int k = j * j;
        int m = k * 256;
        localObject3 = new int[m];
        j = 0;
        while (j < m)
        {
          localObject3[j] = (j / k);
          j += 1;
        }
        int[][] arrayOfInt = (int[][])Array.newInstance(Integer.TYPE, new int[] { i18, 3 });
        int i19 = i7 + 1;
        int i8 = 0;
        int i9 = 0;
        int i6 = 0;
        int i10;
        int n;
        int i1;
        int i2;
        int i3;
        int i4;
        int i5;
        int i11;
        int i12;
        int i13;
        while (i8 < i)
        {
          i10 = -i7;
          n = 0;
          i1 = 0;
          i2 = 0;
          m = 0;
          k = 0;
          j = 0;
          i3 = 0;
          i4 = 0;
          i5 = 0;
          while (i10 <= i7)
          {
            i11 = arrayOfInt2[(i9 + Math.min(i20, Math.max(i10, 0)))];
            localObject4 = arrayOfInt[(i10 + i7)];
            localObject4[0] = ((i11 & 0xFF0000) >> 16);
            localObject4[1] = ((i11 & 0xFF00) >> 8);
            localObject4[2] = (i11 & 0xFF);
            i11 = i19 - Math.abs(i10);
            n += localObject4[0] * i11;
            i1 += localObject4[1] * i11;
            i2 += localObject4[2] * i11;
            if (i10 > 0)
            {
              i3 += localObject4[0];
              i4 += localObject4[1];
              i5 += localObject4[2];
            }
            else
            {
              m += localObject4[0];
              k += localObject4[1];
              j += localObject4[2];
            }
            i10 += 1;
          }
          i12 = i7;
          i13 = 0;
          i10 = i4;
          i11 = i3;
          i4 = n;
          i3 = i12;
          n = i13;
          while (n < i16)
          {
            arrayOfInt3[i9] = localObject3[i4];
            arrayOfInt4[i9] = localObject3[i1];
            arrayOfInt5[i9] = localObject3[i2];
            localObject4 = arrayOfInt[((i3 - i7 + i18) % i18)];
            int i21 = localObject4[0];
            i13 = localObject4[1];
            i12 = localObject4[2];
            if (i8 == 0) {
              arrayOfInt1[n] = Math.min(n + i7 + 1, i20);
            }
            int i22 = arrayOfInt2[(i6 + arrayOfInt1[n])];
            localObject4[0] = ((i22 & 0xFF0000) >> 16);
            localObject4[1] = ((i22 & 0xFF00) >> 8);
            localObject4[2] = (i22 & 0xFF);
            i11 += localObject4[0];
            i10 += localObject4[1];
            i5 += localObject4[2];
            i4 = i4 - m + i11;
            i1 = i1 - k + i10;
            i2 = i2 - j + i5;
            i3 = (i3 + 1) % i18;
            localObject4 = arrayOfInt[(i3 % i18)];
            m = m - i21 + localObject4[0];
            k = k - i13 + localObject4[1];
            j = j - i12 + localObject4[2];
            i11 -= localObject4[0];
            i10 -= localObject4[1];
            i5 -= localObject4[2];
            i9 += 1;
            n += 1;
          }
          i6 += i16;
          i8 += 1;
        }
        localObject4 = localObject2;
        k = 0;
        j = i;
        localObject2 = arrayOfInt1;
        i = k;
        while (i < i16)
        {
          i8 = -i7;
          i9 = i8 * i16;
          i1 = 0;
          i2 = 0;
          i3 = 0;
          n = 0;
          m = 0;
          k = 0;
          i6 = 0;
          i4 = 0;
          i5 = 0;
          while (i8 <= i7)
          {
            i11 = Math.max(0, i9) + i;
            arrayOfInt1 = arrayOfInt[(i8 + i7)];
            arrayOfInt1[0] = arrayOfInt3[i11];
            arrayOfInt1[1] = arrayOfInt4[i11];
            arrayOfInt1[2] = arrayOfInt5[i11];
            i12 = i19 - Math.abs(i8);
            i10 = i1 + arrayOfInt3[i11] * i12;
            i2 += arrayOfInt4[i11] * i12;
            i3 += arrayOfInt5[i11] * i12;
            if (i8 > 0)
            {
              i6 += arrayOfInt1[0];
              i4 += arrayOfInt1[1];
              i5 += arrayOfInt1[2];
            }
            else
            {
              n += arrayOfInt1[0];
              m += arrayOfInt1[1];
              k += arrayOfInt1[2];
            }
            i1 = i9;
            if (i8 < i17) {
              i1 = i9 + i16;
            }
            i8 += 1;
            i9 = i1;
            i1 = i10;
          }
          i11 = i;
          i10 = i6;
          i12 = 0;
          i6 = i7;
          i8 = i5;
          i9 = i4;
          i4 = i6;
          i6 = i1;
          i5 = i11;
          i1 = i12;
          while (i1 < j)
          {
            arrayOfInt2[i5] = (arrayOfInt2[i5] & 0xFF000000 | localObject3[i6] << 16 | localObject3[i2] << 8 | localObject3[i3]);
            arrayOfInt1 = arrayOfInt[((i4 - i7 + i18) % i18)];
            i13 = arrayOfInt1[0];
            i12 = arrayOfInt1[1];
            i11 = arrayOfInt1[2];
            if (i == 0) {
              localObject2[i1] = (Math.min(i1 + i19, i17) * i16);
            }
            i20 = localObject2[i1] + i;
            arrayOfInt1[0] = arrayOfInt3[i20];
            arrayOfInt1[1] = arrayOfInt4[i20];
            arrayOfInt1[2] = arrayOfInt5[i20];
            i10 += arrayOfInt1[0];
            i9 += arrayOfInt1[1];
            i8 += arrayOfInt1[2];
            i6 = i6 - n + i10;
            i2 = i2 - m + i9;
            i3 = i3 - k + i8;
            i4 = (i4 + 1) % i18;
            arrayOfInt1 = arrayOfInt[i4];
            n = n - i13 + arrayOfInt1[0];
            m = m - i12 + arrayOfInt1[1];
            k = k - i11 + arrayOfInt1[2];
            i10 -= arrayOfInt1[0];
            i9 -= arrayOfInt1[1];
            i8 -= arrayOfInt1[2];
            i5 += i16;
            i1 += 1;
          }
          i += 1;
        }
        ((Bitmap)localObject1).setPixels(arrayOfInt2, 0, i16, 0, 0, i16, j);
        localObject2 = localObject1;
      }
      ((Bitmap)localObject4).recycle();
      Object localObject1 = Bitmap.createScaledBitmap((Bitmap)localObject2, i14, i15, true);
      ((Bitmap)localObject2).recycle();
      if (localObject1 != paramBitmap) {
        paramBitmap.recycle();
      }
      return (Bitmap)localObject1;
    }
    return paramBitmap;
  }
  
  public final String a()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(getClass().getSimpleName());
    localStringBuilder.append("-");
    localStringBuilder.append(c);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.aq.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */