package com.truecaller.common.h;

import com.google.c.a.k;
import com.truecaller.common.account.r;
import com.truecaller.multisim.h;
import dagger.a.d;
import javax.inject.Provider;

public final class w
  implements d<v>
{
  private final Provider<k> a;
  private final Provider<h> b;
  private final Provider<r> c;
  private final Provider<aj> d;
  
  private w(Provider<k> paramProvider, Provider<h> paramProvider1, Provider<r> paramProvider2, Provider<aj> paramProvider3)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static w a(Provider<k> paramProvider, Provider<h> paramProvider1, Provider<r> paramProvider2, Provider<aj> paramProvider3)
  {
    return new w(paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.w
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */