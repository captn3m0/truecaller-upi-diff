package com.truecaller.common.h;

import android.text.TextUtils;
import c.g.a.b;
import c.g.b.j;
import c.g.b.w;
import c.m.i;
import com.google.c.a.g;
import com.google.c.a.k.c;
import com.google.c.a.k.d;
import com.google.c.a.m.a;
import com.google.c.a.n;
import com.truecaller.common.account.r;
import com.truecaller.multisim.h;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;
import javax.inject.Inject;

public final class v
  implements u
{
  private final com.google.c.a.k a;
  private final h b;
  private final r c;
  private final aj d;
  
  @Inject
  public v(com.google.c.a.k paramk, h paramh, r paramr, aj paramaj)
  {
    a = paramk;
    b = paramh;
    c = paramr;
    d = paramaj;
  }
  
  private final String a(String paramString1, k.c paramc, String paramString2, String paramString3, boolean paramBoolean)
  {
    paramString1 = b(paramString1, paramString2, paramString3);
    if (paramString1 != null)
    {
      if ((paramBoolean) && (!b)) {
        return null;
      }
      return a.a(a, paramc);
    }
    return null;
  }
  
  private final a d(String paramString1, String paramString2)
  {
    try
    {
      paramString1 = a.a((CharSequence)paramString1, org.c.a.a.a.k.c(paramString2, Locale.ENGLISH));
      paramString1 = new a(paramString1, a.c(paramString1));
      return paramString1;
    }
    catch (g paramString1)
    {
      for (;;) {}
    }
    return null;
  }
  
  public final String a()
  {
    String str = b.f();
    c.g.b.k.a(str, "multiSimManager.defaultSimToken");
    return str;
  }
  
  public final String a(String paramString)
  {
    c.g.b.k.b(paramString, "simToken");
    String str = c.b();
    if (str != null) {
      return a(this, str, k.c.a, b(), paramString, false, 8);
    }
    return null;
  }
  
  public final String a(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "number");
    return a(this, paramString1, k.c.c, paramString2, null, false, 12);
  }
  
  public final String a(String paramString1, String paramString2, String paramString3)
  {
    c.g.b.k.b(paramString1, "number");
    c.g.b.k.b(paramString2, "simToken");
    return a(this, paramString1, k.c.a, paramString3, paramString2, false, 8);
  }
  
  public final Collection<m.a> a(Collection<String> paramCollection)
  {
    c.g.b.k.b(paramCollection, "numbers");
    return (Collection)c.m.l.d(c.m.l.d(c.m.l.a(c.a.m.n((Iterable)paramCollection), (b)c.a), (b)new d(this)));
  }
  
  final a b(String paramString1, String paramString2, String paramString3)
  {
    Object localObject = paramString3;
    if (paramString3 == null) {
      localObject = a();
    }
    paramString3 = b.d((String)localObject);
    localObject = c.m.l.a(c.m.l.c(c.m.l.a(new String[] { paramString2, b.e((String)localObject), paramString3, b() })), (b)b.a).a();
    paramString3 = null;
    while (((Iterator)localObject).hasNext())
    {
      String str = (String)((Iterator)localObject).next();
      if (paramString3 != null) {
        paramString2 = Boolean.valueOf(b);
      } else {
        paramString2 = null;
      }
      if (!com.truecaller.utils.extensions.c.a(paramString2))
      {
        paramString2 = d(paramString1, str);
        if (paramString2 != null)
        {
          int i;
          if ((!b) && (paramString3 != null)) {
            i = 0;
          } else {
            i = 1;
          }
          if (i == 0) {
            paramString2 = null;
          }
          if (paramString2 != null) {
            paramString3 = paramString2;
          }
        }
      }
    }
    return paramString3;
  }
  
  public final String b()
  {
    return c.a();
  }
  
  public final String b(String paramString)
  {
    c.g.b.k.b(paramString, "number");
    return a(this, paramString, k.c.a, null, null, false, 14);
  }
  
  public final String b(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "number");
    c.g.b.k.b(paramString2, "simToken");
    return a(this, paramString1, k.c.a, null, paramString2, false, 10);
  }
  
  public final m.a c(String paramString)
  {
    c.g.b.k.b(paramString, "number");
    if (c.n.m.a((CharSequence)paramString)) {
      return null;
    }
    paramString = b(paramString, null, null);
    if (paramString != null) {
      return a;
    }
    return null;
  }
  
  public final String c(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "number");
    c.g.b.k.b(paramString2, "simToken");
    return a(this, paramString1, k.c.a, null, paramString2, true, 2);
  }
  
  public final int d(String paramString)
  {
    c.g.b.k.b(paramString, "numberStr");
    k.d locald2 = k.d.l;
    Object localObject2 = n.a();
    String str = b();
    Object localObject1 = locald2;
    k.d locald1;
    if (!TextUtils.isEmpty((CharSequence)str)) {
      if (((n)localObject2).a((CharSequence)paramString, str)) {
        localObject1 = k.d.c;
      } else {
        try
        {
          localObject1 = a.a((CharSequence)paramString, str);
          localObject1 = a.b((m.a)localObject1);
          c.g.b.k.a(localObject1, "phoneNumberUtil.getNumberType(parsedNumber)");
        }
        catch (g localg)
        {
          localObject2 = new StringBuilder("Invalid number, cannot parse ");
          ((StringBuilder)localObject2).append(paramString);
          ((StringBuilder)localObject2).append(" using ");
          ((StringBuilder)localObject2).append(str);
          ((StringBuilder)localObject2).append(", ");
          ((StringBuilder)localObject2).append(localg.getMessage());
          ((StringBuilder)localObject2).toString();
          locald1 = locald2;
        }
      }
    }
    return ab.a(locald1);
  }
  
  public final String e(String paramString)
  {
    c.g.b.k.b(paramString, "phoneNumber");
    try
    {
      paramString = a.d(a.a((CharSequence)paramString, null));
      return paramString;
    }
    catch (g paramString) {}
    return null;
  }
  
  static final class a
  {
    final m.a a;
    final boolean b;
    
    public a(m.a parama, boolean paramBoolean)
    {
      a = parama;
      b = paramBoolean;
    }
    
    public final boolean equals(Object paramObject)
    {
      if (this != paramObject)
      {
        if ((paramObject instanceof a))
        {
          paramObject = (a)paramObject;
          if (c.g.b.k.a(a, a))
          {
            int i;
            if (b == b) {
              i = 1;
            } else {
              i = 0;
            }
            if (i != 0) {
              return true;
            }
          }
        }
        return false;
      }
      return true;
    }
    
    public final int hashCode()
    {
      throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder("Result(phoneNumber=");
      localStringBuilder.append(a);
      localStringBuilder.append(", isValidNumber=");
      localStringBuilder.append(b);
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class b
    extends j
    implements b<CharSequence, Boolean>
  {
    public static final b a = new b();
    
    public final c.l.c a()
    {
      return w.a(c.n.m.class, "common_release");
    }
    
    public final String b()
    {
      return "isNotBlank";
    }
    
    public final String c()
    {
      return "isNotBlank(Ljava/lang/CharSequence;)Z";
    }
  }
  
  static final class c
    extends j
    implements b<CharSequence, Boolean>
  {
    public static final c a = new c();
    
    public final c.l.c a()
    {
      return w.a(c.n.m.class, "common_release");
    }
    
    public final String b()
    {
      return "isNotEmpty";
    }
    
    public final String c()
    {
      return "isNotEmpty(Ljava/lang/CharSequence;)Z";
    }
  }
  
  static final class d
    extends c.g.b.l
    implements b<String, m.a>
  {
    d(v paramv)
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.v
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */