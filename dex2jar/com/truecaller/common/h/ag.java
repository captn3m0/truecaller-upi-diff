package com.truecaller.common.h;

import com.truecaller.featuretoggles.e;
import dagger.a.d;
import javax.inject.Provider;

public final class ag
  implements d<af>
{
  private final Provider<e> a;
  
  private ag(Provider<e> paramProvider)
  {
    a = paramProvider;
  }
  
  public static ag a(Provider<e> paramProvider)
  {
    return new ag(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.ag
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */