package com.truecaller.common.h;

import com.truecaller.common.account.r;
import com.truecaller.common.g.a;
import dagger.a.d;
import javax.inject.Provider;

public final class ae
  implements d<ad>
{
  private final Provider<a> a;
  private final Provider<r> b;
  
  private ae(Provider<a> paramProvider, Provider<r> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static ae a(Provider<a> paramProvider, Provider<r> paramProvider1)
  {
    return new ae(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.ae
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */