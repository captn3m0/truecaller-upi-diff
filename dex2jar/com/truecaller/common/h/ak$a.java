package com.truecaller.common.h;

import android.content.Context;
import android.telephony.TelephonyManager;
import c.g.a.a;
import c.g.b.l;
import c.u;

final class ak$a
  extends l
  implements a<String>
{
  ak$a(ak paramak)
  {
    super(0);
  }
  
  private String a()
  {
    try
    {
      Object localObject = ak.a(a).getSystemService("phone");
      if (localObject != null) {
        return ((TelephonyManager)localObject).getVoiceMailNumber();
      }
      throw new u("null cannot be cast to non-null type android.telephony.TelephonyManager");
    }
    catch (SecurityException localSecurityException)
    {
      for (;;) {}
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.ak.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */