package com.truecaller.common.h;

import android.text.TextUtils;
import com.truecaller.common.account.r;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Inject;

public final class t
{
  private static final Pattern a = Pattern.compile("(^|\\s)\\+?\\d([ -]?\\d){5,}");
  private final String b;
  
  @Inject
  public t(r paramr)
  {
    b = paramr.a();
  }
  
  public t(String paramString)
  {
    b = paramString;
  }
  
  public final List<String> a(String paramString)
  {
    if ((!TextUtils.isEmpty(paramString)) && (b != null))
    {
      paramString = paramString.replace("‪", "").replace("‬", "").replace("tel:", "");
      paramString = a.matcher(paramString);
      ArrayList localArrayList = new ArrayList(paramString.groupCount());
      while (paramString.find())
      {
        String str = paramString.group().trim();
        if (ab.b(str, b)) {
          localArrayList.add(str);
        }
      }
      return localArrayList;
    }
    return Collections.emptyList();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.t
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */