package com.truecaller.common.h;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.ServiceConfigurationError;

final class a$a
  implements Iterator<S>
{
  private final Queue<String> b;
  
  private a$a(a parama)
  {
    b = new LinkedList(a.a(parama));
  }
  
  public final boolean hasNext()
  {
    return !b.isEmpty();
  }
  
  public final S next()
  {
    String str = (String)b.remove();
    try
    {
      Object localObject = a.c(a).cast(a.b(a).loadClass(str).newInstance());
      return (S)localObject;
    }
    catch (Exception localException)
    {
      throw new ServiceConfigurationError("Couldn't instantiate class ".concat(String.valueOf(str)), localException);
    }
  }
  
  public final void remove()
  {
    throw new UnsupportedOperationException();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */