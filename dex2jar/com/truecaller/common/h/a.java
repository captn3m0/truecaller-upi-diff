package com.truecaller.common.h;

import android.content.Context;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.ServiceConfigurationError;
import java.util.Set;

public final class a<S>
  implements Iterable<S>
{
  private final Class<S> a;
  private final ClassLoader b;
  private final Set<String> c;
  
  private a(Context paramContext, Class<S> paramClass, ClassLoader paramClassLoader)
  {
    a = paramClass;
    b = paramClassLoader;
    c = new HashSet();
    a(paramContext);
  }
  
  public static <S> a<S> a(Context paramContext, Class<S> paramClass, ClassLoader paramClassLoader)
  {
    return new a(paramContext.getApplicationContext(), paramClass, paramClassLoader);
  }
  
  /* Error */
  private void a(Context paramContext)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 5
    //   3: aconst_null
    //   4: astore 4
    //   6: aload 4
    //   8: astore_2
    //   9: aload 5
    //   11: astore_3
    //   12: aload_1
    //   13: invokevirtual 54	android/content/Context:getAssets	()Landroid/content/res/AssetManager;
    //   16: astore_1
    //   17: aload 4
    //   19: astore_2
    //   20: aload 5
    //   22: astore_3
    //   23: new 56	java/lang/StringBuilder
    //   26: dup
    //   27: ldc 58
    //   29: invokespecial 61	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   32: astore 6
    //   34: aload 4
    //   36: astore_2
    //   37: aload 5
    //   39: astore_3
    //   40: aload 6
    //   42: aload_0
    //   43: getfield 24	com/truecaller/common/h/a:a	Ljava/lang/Class;
    //   46: invokevirtual 67	java/lang/Class:getName	()Ljava/lang/String;
    //   49: invokevirtual 71	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   52: pop
    //   53: aload 4
    //   55: astore_2
    //   56: aload 5
    //   58: astore_3
    //   59: aload_1
    //   60: aload 6
    //   62: invokevirtual 74	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   65: iconst_2
    //   66: invokevirtual 80	android/content/res/AssetManager:open	(Ljava/lang/String;I)Ljava/io/InputStream;
    //   69: astore_1
    //   70: aload_1
    //   71: astore_2
    //   72: aload_1
    //   73: astore_3
    //   74: new 82	java/io/BufferedReader
    //   77: dup
    //   78: new 84	java/io/InputStreamReader
    //   81: dup
    //   82: aload_1
    //   83: invokespecial 87	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   86: invokespecial 90	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   89: astore 4
    //   91: aload_1
    //   92: astore_2
    //   93: aload_1
    //   94: astore_3
    //   95: aload 4
    //   97: invokevirtual 93	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   100: astore 5
    //   102: aload 5
    //   104: ifnull +34 -> 138
    //   107: aload_1
    //   108: astore_2
    //   109: aload_1
    //   110: astore_3
    //   111: aload 5
    //   113: invokestatic 99	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   116: ifne -25 -> 91
    //   119: aload_1
    //   120: astore_2
    //   121: aload_1
    //   122: astore_3
    //   123: aload_0
    //   124: getfield 31	com/truecaller/common/h/a:c	Ljava/util/Set;
    //   127: aload 5
    //   129: invokeinterface 105 2 0
    //   134: pop
    //   135: goto -44 -> 91
    //   138: aload_1
    //   139: ifnull +54 -> 193
    //   142: aload_1
    //   143: invokevirtual 110	java/io/InputStream:close	()V
    //   146: return
    //   147: astore_1
    //   148: goto +46 -> 194
    //   151: aload_3
    //   152: astore_2
    //   153: new 56	java/lang/StringBuilder
    //   156: dup
    //   157: ldc 112
    //   159: invokespecial 61	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   162: astore_1
    //   163: aload_3
    //   164: astore_2
    //   165: aload_1
    //   166: aload_0
    //   167: getfield 24	com/truecaller/common/h/a:a	Ljava/lang/Class;
    //   170: invokevirtual 67	java/lang/Class:getName	()Ljava/lang/String;
    //   173: invokevirtual 71	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   176: pop
    //   177: aload_3
    //   178: astore_2
    //   179: aload_1
    //   180: invokevirtual 74	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   183: pop
    //   184: aload_3
    //   185: ifnull +8 -> 193
    //   188: aload_3
    //   189: invokevirtual 110	java/io/InputStream:close	()V
    //   192: return
    //   193: return
    //   194: aload_2
    //   195: ifnull +7 -> 202
    //   198: aload_2
    //   199: invokevirtual 110	java/io/InputStream:close	()V
    //   202: aload_1
    //   203: athrow
    //   204: astore_1
    //   205: goto -54 -> 151
    //   208: astore_1
    //   209: return
    //   210: astore_1
    //   211: return
    //   212: astore_2
    //   213: goto -11 -> 202
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	216	0	this	a
    //   0	216	1	paramContext	Context
    //   8	191	2	localObject1	Object
    //   212	1	2	localIOException	java.io.IOException
    //   11	178	3	localObject2	Object
    //   4	92	4	localBufferedReader	java.io.BufferedReader
    //   1	127	5	str	String
    //   32	29	6	localStringBuilder	StringBuilder
    // Exception table:
    //   from	to	target	type
    //   12	17	147	finally
    //   23	34	147	finally
    //   40	53	147	finally
    //   59	70	147	finally
    //   74	91	147	finally
    //   95	102	147	finally
    //   111	119	147	finally
    //   123	135	147	finally
    //   153	163	147	finally
    //   165	177	147	finally
    //   179	184	147	finally
    //   12	17	204	java/io/IOException
    //   23	34	204	java/io/IOException
    //   40	53	204	java/io/IOException
    //   59	70	204	java/io/IOException
    //   74	91	204	java/io/IOException
    //   95	102	204	java/io/IOException
    //   111	119	204	java/io/IOException
    //   123	135	204	java/io/IOException
    //   142	146	208	java/io/IOException
    //   188	192	210	java/io/IOException
    //   198	202	212	java/io/IOException
  }
  
  public final Iterator<S> iterator()
  {
    return new a((byte)0);
  }
  
  final class a
    implements Iterator<S>
  {
    private final Queue<String> b = new LinkedList(a.a(a.this));
    
    private a() {}
    
    public final boolean hasNext()
    {
      return !b.isEmpty();
    }
    
    public final S next()
    {
      String str = (String)b.remove();
      try
      {
        Object localObject = a.c(a.this).cast(a.b(a.this).loadClass(str).newInstance());
        return (S)localObject;
      }
      catch (Exception localException)
      {
        throw new ServiceConfigurationError("Couldn't instantiate class ".concat(String.valueOf(str)), localException);
      }
    }
    
    public final void remove()
    {
      throw new UnsupportedOperationException();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */