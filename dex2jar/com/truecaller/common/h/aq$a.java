package com.truecaller.common.h;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader.TileMode;
import com.d.b.ai;

public final class aq$a
  implements ai
{
  private final int a;
  private final float b;
  
  public aq$a(int paramInt)
  {
    this(paramInt, (byte)0);
  }
  
  private aq$a(int paramInt, byte paramByte)
  {
    a = paramInt;
    b = -1.0F;
  }
  
  public final Bitmap a(Bitmap paramBitmap)
  {
    Bitmap localBitmap = Bitmap.createBitmap(paramBitmap.getWidth(), paramBitmap.getHeight(), Bitmap.Config.ARGB_8888);
    Canvas localCanvas = new Canvas(localBitmap);
    Paint localPaint = new Paint();
    float f = b;
    if (f <= 0.0F) {
      f = localBitmap.getHeight();
    }
    localCanvas.drawBitmap(paramBitmap, 0.0F, 0.0F, localPaint);
    localPaint.setShader(new LinearGradient(0.0F, localBitmap.getHeight() - f, 0.0F, localBitmap.getHeight(), 0, a, Shader.TileMode.CLAMP));
    localCanvas.drawPaint(localPaint);
    paramBitmap.recycle();
    return localBitmap;
  }
  
  public final String a()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(getClass().getSimpleName());
    localStringBuilder.append(" ");
    localStringBuilder.append(Integer.toHexString(a));
    localStringBuilder.append(b);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.aq.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */