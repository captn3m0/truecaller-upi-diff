package com.truecaller.common.h;

import c.a.f;
import c.u;
import com.bumptech.glide.load.d.a.e;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Arrays;

public abstract class b
  extends e
{
  private final byte[] b;
  private final String c;
  
  public b(String paramString)
  {
    c = paramString;
    paramString = c;
    Charset localCharset = Charset.forName("UTF-8");
    c.g.b.k.a(localCharset, "Charset.forName(\"UTF-8\")");
    if (paramString != null)
    {
      paramString = paramString.getBytes(localCharset);
      c.g.b.k.a(paramString, "(this as java.lang.String).getBytes(charset)");
      b = paramString;
      return;
    }
    throw new u("null cannot be cast to non-null type java.lang.String");
  }
  
  public final int a(int... paramVarArgs)
  {
    c.g.b.k.b(paramVarArgs, "hashCodeValues");
    if (paramVarArgs.length == 1) {
      return f.b(paramVarArgs);
    }
    int i = f.b(paramVarArgs);
    int j = paramVarArgs.length;
    c.g.b.k.b(paramVarArgs, "receiver$0");
    int k = paramVarArgs.length;
    if (j <= k)
    {
      paramVarArgs = Arrays.copyOfRange(paramVarArgs, 1, j);
      c.g.b.k.a(paramVarArgs, "java.util.Arrays.copyOfR…this, fromIndex, toIndex)");
      return com.bumptech.glide.g.k.b(i, a(Arrays.copyOf(paramVarArgs, paramVarArgs.length)));
    }
    paramVarArgs = new StringBuilder("toIndex (");
    paramVarArgs.append(j);
    paramVarArgs.append(") is greater than size (");
    paramVarArgs.append(k);
    paramVarArgs.append(").");
    throw ((Throwable)new IndexOutOfBoundsException(paramVarArgs.toString()));
  }
  
  public void a(MessageDigest paramMessageDigest)
  {
    c.g.b.k.b(paramMessageDigest, "messageDigest");
    paramMessageDigest.update(b);
  }
  
  public boolean equals(Object paramObject)
  {
    return paramObject instanceof m;
  }
  
  public int hashCode()
  {
    return c.hashCode();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */