package com.truecaller.common.h;

import android.content.Context;
import android.content.res.AssetManager;
import android.text.TextUtils;
import com.google.gson.f;
import com.truecaller.common.network.country.CountryListDto;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.common.network.country.CountryListDto.b;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.log.d;
import e.b;
import e.r;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import okhttp3.ad;

public final class h
{
  private static final String[] a = { "1403", "1587", "1780", "1825", "1236", "1250", "1604", "1672", "1778", "1204", "1431", "1506", "1709", "1902", "1226", "1249", "1289", "1343", "1365", "1416", "1437", "1519", "1613", "1647", "1705", "1807", "1905", "1418", "1438", "1450", "1514", "1579", "1581", "1819", "1873", "1306", "1639", "1867" };
  private static final String[] b = { "733622", "76", "77" };
  private static final Map<String, String[]> c;
  private static final List<String> d = Arrays.asList(new String[] { "tw", "hk", "mo" });
  private static final ReentrantLock e;
  private static final Condition f;
  private static volatile CountryListDto g;
  private static Map<String, CountryListDto.a> h;
  private static Map<String, CountryListDto.a> i;
  private static Map<String, CountryListDto.a> j;
  private static Map<String, List<String>> k;
  
  static
  {
    Object localObject = new HashMap();
    c = (Map)localObject;
    ((Map)localObject).put("ca", a);
    c.put("kz", b);
    localObject = new ReentrantLock();
    e = (ReentrantLock)localObject;
    f = ((ReentrantLock)localObject).newCondition();
  }
  
  /* Error */
  public static CountryListDto.a a(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: ifnonnull +5 -> 6
    //   4: aconst_null
    //   5: areturn
    //   6: getstatic 153	com/truecaller/common/h/h:e	Ljava/util/concurrent/locks/ReentrantLock;
    //   9: invokevirtual 166	java/util/concurrent/locks/ReentrantLock:lock	()V
    //   12: getstatic 168	com/truecaller/common/h/h:h	Ljava/util/Map;
    //   15: astore_1
    //   16: aload_1
    //   17: ifnonnull +11 -> 28
    //   20: getstatic 159	com/truecaller/common/h/h:f	Ljava/util/concurrent/locks/Condition;
    //   23: invokeinterface 173 1 0
    //   28: getstatic 168	com/truecaller/common/h/h:h	Ljava/util/Map;
    //   31: astore_1
    //   32: getstatic 153	com/truecaller/common/h/h:e	Ljava/util/concurrent/locks/ReentrantLock;
    //   35: invokevirtual 176	java/util/concurrent/locks/ReentrantLock:unlock	()V
    //   38: aload_1
    //   39: ifnull +17 -> 56
    //   42: aload_1
    //   43: aload_0
    //   44: invokevirtual 180	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   47: invokeinterface 184 2 0
    //   52: checkcast 186	com/truecaller/common/network/country/CountryListDto$a
    //   55: areturn
    //   56: aconst_null
    //   57: areturn
    //   58: astore_0
    //   59: getstatic 153	com/truecaller/common/h/h:e	Ljava/util/concurrent/locks/ReentrantLock;
    //   62: invokevirtual 176	java/util/concurrent/locks/ReentrantLock:unlock	()V
    //   65: aload_0
    //   66: athrow
    //   67: astore_1
    //   68: goto -40 -> 28
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	71	0	paramString	String
    //   15	28	1	localMap	Map
    //   67	1	1	localInterruptedException	InterruptedException
    // Exception table:
    //   from	to	target	type
    //   12	16	58	finally
    //   20	28	58	finally
    //   28	32	58	finally
    //   20	28	67	java/lang/InterruptedException
  }
  
  private static <T> T a(Context paramContext, String paramString, Type paramType)
  {
    Object localObject1 = new File(paramContext.getFilesDir(), paramString);
    if (((File)localObject1).exists()) {
      try
      {
        localObject1 = new FileReader((File)localObject1);
        try
        {
          Object localObject2 = new f().a((Reader)localObject1, paramType);
          return (T)localObject2;
        }
        finally
        {
          ((Reader)localObject1).close();
        }
        return (T)b(paramContext, paramString, paramType);
      }
      catch (Exception localException)
      {
        d.a(localException, "Failed to read country list from file");
      }
    }
  }
  
  public static List<CountryListDto.a> a()
  {
    CountryListDto localCountryListDto = c();
    if (!a(localCountryListDto)) {
      return Collections.emptyList();
    }
    return countryList.b;
  }
  
  private static List<CountryListDto.a> a(List<CountryListDto.a> paramList)
  {
    if ("HUAWEI_STORE".equalsIgnoreCase(com.truecaller.common.b.a.F().e().f()))
    {
      Iterator localIterator = paramList.iterator();
      while (localIterator.hasNext())
      {
        CountryListDto.a locala = (CountryListDto.a)localIterator.next();
        if (d.contains(c)) {
          localIterator.remove();
        }
      }
    }
    return Collections.unmodifiableList(paramList);
  }
  
  public static void a(Context paramContext)
  {
    e.lock();
    for (;;)
    {
      try
      {
        CountryListDto localCountryListDto = g;
        m = 1;
        if (localCountryListDto == null)
        {
          bool = true;
          AssertionUtil.OnlyInDebug.isTrue(bool, new String[0]);
          localCountryListDto = (CountryListDto)a(paramContext, "countries.json", CountryListDto.class);
          k = (Map)a(paramContext, "countries_languages.json", com.google.gson.c.a {});
          if (localCountryListDto == null) {
            break label214;
          }
          bool = true;
          AssertionUtil.isTrue(bool, new String[0]);
          b(localCountryListDto);
          if ((localCountryListDto == null) || (countryList == null)) {
            break label219;
          }
          if (countryList.b != null) {
            countryList.b = a(countryList.b);
          }
          if (countryList.a != null) {
            break label219;
          }
          Object localObject = k.g(paramContext);
          if (TextUtils.isEmpty((CharSequence)localObject)) {
            break label219;
          }
          localObject = a((String)localObject);
          if (localObject == null) {
            break label219;
          }
          countryList.a = ((CountryListDto.a)localObject);
          g = localCountryListDto;
          f.signalAll();
          e.unlock();
          if (m != 0) {
            a(paramContext, localCountryListDto);
          }
          return;
        }
      }
      finally
      {
        e.unlock();
      }
      boolean bool = false;
      continue;
      label214:
      bool = false;
      continue;
      label219:
      int m = 0;
    }
  }
  
  private static void a(Context paramContext, CountryListDto paramCountryListDto)
  {
    paramContext = new File(paramContext.getFilesDir(), "countries.json");
    try
    {
      paramContext = new FileWriter(paramContext);
      try
      {
        new f().a(paramCountryListDto, paramContext);
        return;
      }
      finally
      {
        paramContext.close();
      }
      return;
    }
    catch (Exception paramContext)
    {
      d.a(paramContext);
    }
  }
  
  private static boolean a(CountryListDto paramCountryListDto)
  {
    return (paramCountryListDto != null) && (countryList != null) && (countryList.b != null);
  }
  
  public static CountryListDto.a b()
  {
    CountryListDto localCountryListDto = c();
    if (!a(localCountryListDto)) {
      return null;
    }
    return countryList.a;
  }
  
  /* Error */
  public static CountryListDto.a b(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: ifnonnull +5 -> 6
    //   4: aconst_null
    //   5: areturn
    //   6: getstatic 153	com/truecaller/common/h/h:e	Ljava/util/concurrent/locks/ReentrantLock;
    //   9: invokevirtual 166	java/util/concurrent/locks/ReentrantLock:lock	()V
    //   12: getstatic 371	com/truecaller/common/h/h:i	Ljava/util/Map;
    //   15: astore_1
    //   16: aload_1
    //   17: ifnonnull +11 -> 28
    //   20: getstatic 159	com/truecaller/common/h/h:f	Ljava/util/concurrent/locks/Condition;
    //   23: invokeinterface 173 1 0
    //   28: getstatic 371	com/truecaller/common/h/h:i	Ljava/util/Map;
    //   31: astore_1
    //   32: getstatic 153	com/truecaller/common/h/h:e	Ljava/util/concurrent/locks/ReentrantLock;
    //   35: invokevirtual 176	java/util/concurrent/locks/ReentrantLock:unlock	()V
    //   38: aload_1
    //   39: ifnull +20 -> 59
    //   42: aload_1
    //   43: aload_0
    //   44: invokestatic 376	com/truecaller/common/h/am:i	(Ljava/lang/String;)Ljava/lang/String;
    //   47: invokestatic 379	com/truecaller/common/h/am:m	(Ljava/lang/String;)Ljava/lang/String;
    //   50: invokeinterface 184 2 0
    //   55: checkcast 186	com/truecaller/common/network/country/CountryListDto$a
    //   58: areturn
    //   59: aconst_null
    //   60: areturn
    //   61: astore_0
    //   62: getstatic 153	com/truecaller/common/h/h:e	Ljava/util/concurrent/locks/ReentrantLock;
    //   65: invokevirtual 176	java/util/concurrent/locks/ReentrantLock:unlock	()V
    //   68: aload_0
    //   69: athrow
    //   70: astore_1
    //   71: goto -43 -> 28
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	74	0	paramString	String
    //   15	28	1	localMap	Map
    //   70	1	1	localInterruptedException	InterruptedException
    // Exception table:
    //   from	to	target	type
    //   12	16	61	finally
    //   20	28	61	finally
    //   28	32	61	finally
    //   20	28	70	java/lang/InterruptedException
  }
  
  private static <T> T b(Context paramContext, String paramString, Type paramType)
  {
    try
    {
      paramContext = paramContext.getAssets().open(paramString);
      try
      {
        paramString = new f().a(new InputStreamReader(paramContext), paramType);
        return paramString;
      }
      finally
      {
        paramContext.close();
      }
      return null;
    }
    catch (Exception paramContext)
    {
      d.a(paramContext, "Failed to read countries from assets");
    }
  }
  
  public static void b(Context paramContext)
  {
    Object localObject = c();
    if (localObject == null) {
      localObject = "";
    }
    for (;;)
    {
      try
      {
        localObject = countryListChecksum;
        localObject = com.truecaller.common.network.country.a.a((String)localObject).c();
        if (a.c())
        {
          localObject = (CountryListDto)b;
          if (localObject != null)
          {
            CountryListDto.b localb = countryList;
            if (localb != null)
            {
              e.lock();
              try
              {
                boolean bool = a(g);
                n = 1;
                i1 = 1;
                int m;
                if (!bool)
                {
                  if ((countryList != null) && (countryList.b != null)) {
                    countryList.b = a(countryList.b);
                  }
                  g = (CountryListDto)localObject;
                  m = 1;
                }
                else
                {
                  if (countryList.b == null)
                  {
                    countryList.b = gcountryList.b;
                    n = 0;
                    m = 0;
                  }
                  else
                  {
                    countryList.b = a(countryList.b);
                    n = 1;
                    m = 1;
                  }
                  if (countryList.a != null) {
                    break label320;
                  }
                  countryList.a = gcountryList.a;
                  if (TextUtils.equals(countryListChecksum, gcountryListChecksum)) {
                    break label325;
                  }
                  g = (CountryListDto)localObject;
                  i1 |= n;
                  n = m;
                  m = i1;
                }
                if (n != 0) {
                  b((CountryListDto)localObject);
                }
                e.unlock();
                if (m != 0) {
                  a(paramContext, (CountryListDto)localObject);
                }
                return;
              }
              finally
              {
                e.unlock();
              }
            }
          }
        }
        return;
      }
      catch (RuntimeException paramContext) {}catch (IOException paramContext) {}
      d.a(paramContext, "Unable to load countries from network");
      return;
      label320:
      int n = 1;
      continue;
      label325:
      int i1 = 0;
    }
  }
  
  private static void b(CountryListDto paramCountryListDto)
  {
    HashMap localHashMap1 = new HashMap();
    HashMap localHashMap2 = new HashMap();
    HashMap localHashMap3 = new HashMap();
    if ((paramCountryListDto != null) && (countryList != null) && (countryList.b != null))
    {
      paramCountryListDto = countryList.b.iterator();
      while (paramCountryListDto.hasNext())
      {
        CountryListDto.a locala = (CountryListDto.a)paramCountryListDto.next();
        if (!TextUtils.isEmpty(c))
        {
          Object localObject = am.d(c, Locale.ENGLISH);
          localHashMap1.put(localObject, locala);
          localHashMap2.put(am.m(am.i(b)), locala);
          localObject = (String[])c.get(localObject);
          if ((localObject != null) && (localObject.length > 0))
          {
            int n = localObject.length;
            int m = 0;
            while (m < n)
            {
              localHashMap3.put(localObject[m], locala);
              m += 1;
            }
          }
          else
          {
            localHashMap3.put(d, locala);
          }
        }
      }
    }
    h = Collections.unmodifiableMap(localHashMap1);
    i = Collections.unmodifiableMap(localHashMap2);
    j = Collections.unmodifiableMap(localHashMap3);
  }
  
  public static CountryListDto.a c(Context paramContext)
  {
    return a(((com.truecaller.common.b.a)paramContext.getApplicationContext()).H());
  }
  
  /* Error */
  public static CountryListDto.a c(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: ifnonnull +5 -> 6
    //   4: aconst_null
    //   5: areturn
    //   6: getstatic 153	com/truecaller/common/h/h:e	Ljava/util/concurrent/locks/ReentrantLock;
    //   9: invokevirtual 166	java/util/concurrent/locks/ReentrantLock:lock	()V
    //   12: getstatic 456	com/truecaller/common/h/h:j	Ljava/util/Map;
    //   15: astore_1
    //   16: aload_1
    //   17: ifnonnull +11 -> 28
    //   20: getstatic 159	com/truecaller/common/h/h:f	Ljava/util/concurrent/locks/Condition;
    //   23: invokeinterface 173 1 0
    //   28: getstatic 456	com/truecaller/common/h/h:j	Ljava/util/Map;
    //   31: astore_2
    //   32: getstatic 153	com/truecaller/common/h/h:e	Ljava/util/concurrent/locks/ReentrantLock;
    //   35: invokevirtual 176	java/util/concurrent/locks/ReentrantLock:unlock	()V
    //   38: ldc_w 405
    //   41: astore_1
    //   42: aload_0
    //   43: ldc_w 466
    //   46: invokevirtual 469	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   49: ifeq +12 -> 61
    //   52: aload_0
    //   53: iconst_1
    //   54: invokevirtual 473	java/lang/String:substring	(I)Ljava/lang/String;
    //   57: astore_1
    //   58: goto +19 -> 77
    //   61: aload_0
    //   62: ldc_w 475
    //   65: invokevirtual 469	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   68: ifeq +9 -> 77
    //   71: aload_0
    //   72: iconst_2
    //   73: invokevirtual 473	java/lang/String:substring	(I)Ljava/lang/String;
    //   76: astore_1
    //   77: aload_1
    //   78: iconst_0
    //   79: bipush 6
    //   81: aload_1
    //   82: invokevirtual 479	java/lang/String:length	()I
    //   85: invokestatic 485	java/lang/Math:min	(II)I
    //   88: invokevirtual 488	java/lang/String:substring	(II)Ljava/lang/String;
    //   91: astore_0
    //   92: aload_0
    //   93: invokestatic 348	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   96: ifne +35 -> 131
    //   99: aload_2
    //   100: aload_0
    //   101: invokeinterface 184 2 0
    //   106: checkcast 186	com/truecaller/common/network/country/CountryListDto$a
    //   109: astore_1
    //   110: aload_1
    //   111: ifnull +5 -> 116
    //   114: aload_1
    //   115: areturn
    //   116: aload_0
    //   117: iconst_0
    //   118: aload_0
    //   119: invokevirtual 479	java/lang/String:length	()I
    //   122: iconst_1
    //   123: isub
    //   124: invokevirtual 488	java/lang/String:substring	(II)Ljava/lang/String;
    //   127: astore_0
    //   128: goto -36 -> 92
    //   131: aconst_null
    //   132: areturn
    //   133: astore_0
    //   134: getstatic 153	com/truecaller/common/h/h:e	Ljava/util/concurrent/locks/ReentrantLock;
    //   137: invokevirtual 176	java/util/concurrent/locks/ReentrantLock:unlock	()V
    //   140: aload_0
    //   141: athrow
    //   142: astore_1
    //   143: goto -115 -> 28
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	146	0	paramString	String
    //   15	100	1	localObject	Object
    //   142	1	1	localInterruptedException	InterruptedException
    //   31	69	2	localMap	Map
    // Exception table:
    //   from	to	target	type
    //   12	16	133	finally
    //   20	28	133	finally
    //   28	32	133	finally
    //   20	28	142	java/lang/InterruptedException
  }
  
  private static CountryListDto c()
  {
    e.lock();
    for (;;)
    {
      try
      {
        localCountryListDto = g;
        if (localCountryListDto != null) {}
      }
      finally
      {
        CountryListDto localCountryListDto;
        e.unlock();
      }
      try
      {
        f.await();
      }
      catch (InterruptedException localInterruptedException) {}
    }
    localCountryListDto = g;
    e.unlock();
    return localCountryListDto;
  }
  
  public static List<String> d(String paramString)
  {
    if (k.containsKey(paramString)) {
      return (List)k.get(paramString);
    }
    return (List)k.get("ZZ");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */