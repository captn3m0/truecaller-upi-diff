package com.truecaller.common.h;

import android.text.TextUtils;
import android.util.Patterns;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.c.a.a.a.k;

public final class am
  extends k
{
  private static final Set<Character> a = Collections.unmodifiableSet(new HashSet(Arrays.asList(new Character[] { Character.valueOf(','), Character.valueOf(';'), Character.valueOf('p'), Character.valueOf('P'), Character.valueOf('w'), Character.valueOf('W'), Character.valueOf('N'), Character.valueOf('*'), Character.valueOf('#') })));
  
  public static int a(String paramString1, String paramString2, boolean paramBoolean)
  {
    int i;
    if (paramString1 == null) {
      i = 1;
    } else {
      i = 0;
    }
    int j;
    if (paramString2 == null) {
      j = 1;
    } else {
      j = 0;
    }
    if ((i ^ j) != 0)
    {
      if (paramString1 == null) {
        return -1;
      }
      return 1;
    }
    if ((paramString1 == null) && (paramString2 == null)) {
      return 0;
    }
    if (paramBoolean) {
      return paramString1.compareToIgnoreCase(paramString2);
    }
    return paramString1.compareTo(paramString2);
  }
  
  public static String a(Object paramObject)
  {
    if (paramObject == null) {
      return null;
    }
    return paramObject.toString();
  }
  
  public static String a(String paramString)
  {
    if (a(paramString)) {
      return paramString;
    }
    return "";
  }
  
  public static String a(String paramString, Locale paramLocale)
  {
    if (!TextUtils.isEmpty(paramString))
    {
      Locale localLocale = paramLocale;
      if (paramLocale == null) {
        localLocale = Locale.ENGLISH;
      }
      return paramString.toUpperCase(localLocale);
    }
    return paramString;
  }
  
  public static String a(String paramString, CharSequence... paramVarArgs)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    int j = paramVarArgs.length;
    int i = 0;
    while (i < j)
    {
      CharSequence localCharSequence = paramVarArgs[i];
      if (!b(localCharSequence))
      {
        if (!b(localStringBuilder)) {
          localStringBuilder.append(paramString);
        }
        localStringBuilder.append(localCharSequence);
      }
      i += 1;
    }
    return localStringBuilder.toString();
  }
  
  public static String a(String... paramVarArgs)
  {
    return a(", ", paramVarArgs);
  }
  
  public static boolean a(CharSequence paramCharSequence)
  {
    return (e(paramCharSequence)) && (!"null".equals(paramCharSequence));
  }
  
  public static boolean a(String paramString, int paramInt)
  {
    if (!b(paramString)) {}
    try
    {
      paramString = d(paramString);
      if (paramString != null)
      {
        new BigInteger(paramString);
        if (paramInt != -1)
        {
          int i = paramString.length();
          if (i < paramInt) {
            return false;
          }
        }
        return true;
      }
      return false;
    }
    catch (NumberFormatException|NullPointerException paramString) {}
    return false;
  }
  
  public static String b(String paramString, Locale paramLocale)
  {
    if (paramString.length() <= 1) {
      return paramString.toUpperCase(paramLocale);
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString.substring(0, 1).toUpperCase(paramLocale));
    localStringBuilder.append(paramString.substring(1));
    return localStringBuilder.toString();
  }
  
  public static String b(String... paramVarArgs)
  {
    int j = paramVarArgs.length;
    int i = 0;
    while (i < j)
    {
      String str = paramVarArgs[i];
      if (e(str)) {
        return str;
      }
      i += 1;
    }
    return null;
  }
  
  public static boolean b(String paramString)
  {
    return a(paramString, -1);
  }
  
  public static boolean c(String paramString)
  {
    return (!TextUtils.isEmpty(paramString)) && (Patterns.EMAIL_ADDRESS.matcher(paramString).matches());
  }
  
  public static String d(String paramString)
  {
    Object localObject = paramString;
    if (!b(paramString))
    {
      localObject = new StringBuilder();
      int m = paramString.length();
      int j = 0;
      int i;
      for (int k = 0; j < m; k = i)
      {
        char c = paramString.charAt(j);
        if (Character.isDigit(c))
        {
          ((StringBuilder)localObject).append(c);
          i = 1;
        }
        else if ((c == '+') && (k == 0))
        {
          ((StringBuilder)localObject).append(c);
          i = 1;
        }
        else
        {
          i = k;
          if (a.contains(Character.valueOf(c)))
          {
            ((StringBuilder)localObject).append(c);
            i = k;
          }
        }
        j += 1;
      }
      localObject = ((StringBuilder)localObject).toString();
    }
    return (String)localObject;
  }
  
  public static String e(String paramString)
  {
    String str = d(paramString);
    paramString = str;
    if (!TextUtils.isEmpty(str)) {
      paramString = str.replace("+", "00");
    }
    return paramString;
  }
  
  public static int f(String paramString)
  {
    try
    {
      int i = Integer.parseInt(d(paramString));
      return i;
    }
    catch (RuntimeException paramString)
    {
      for (;;) {}
    }
    return 0;
  }
  
  public static int g(String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return 0;
    }
    try
    {
      int i = Integer.parseInt(paramString);
      return i;
    }
    catch (NumberFormatException paramString) {}
    return 0;
  }
  
  public static long h(String paramString)
  {
    try
    {
      long l = Long.parseLong(d(paramString));
      return l;
    }
    catch (RuntimeException paramString)
    {
      for (;;) {}
    }
    return 0L;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.am
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */