package com.truecaller.common.h;

import android.content.Context;
import com.truecaller.multisim.h;
import dagger.a.d;
import javax.inject.Provider;

public final class y
  implements d<h>
{
  private final x a;
  private final Provider<Context> b;
  
  private y(x paramx, Provider<Context> paramProvider)
  {
    a = paramx;
    b = paramProvider;
  }
  
  public static y a(x paramx, Provider<Context> paramProvider)
  {
    return new y(paramx, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.y
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */