package com.truecaller.common.h;

import c.g.a.a;
import com.truecaller.common.network.e;
import com.truecaller.common.network.e.b;

public class i
{
  private final a<Boolean> a;
  
  public i(a<Boolean> parama)
  {
    a = parama;
  }
  
  private static boolean a(e parame, boolean paramBoolean)
  {
    return (paramBoolean) && ((parame instanceof e.b));
  }
  
  public final boolean a(e parame)
  {
    return a(parame, ((Boolean)a.invoke()).booleanValue());
  }
  
  public final e.b b(e parame)
  {
    if (!a(parame, ((Boolean)a.invoke()).booleanValue())) {
      parame = null;
    }
    e locale = parame;
    if (!(parame instanceof e.b)) {
      locale = null;
    }
    return (e.b)locale;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */