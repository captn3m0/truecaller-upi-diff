package com.truecaller.common.h;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import c.u;
import com.bumptech.glide.load.b.a.e;
import java.nio.ByteBuffer;
import java.security.MessageDigest;

public final class ap
  extends b
{
  private final int b;
  
  public ap(int paramInt)
  {
    super("com.truecaller.common.util.TintTransformation");
    b = paramInt;
  }
  
  public final Bitmap a(e parame, Bitmap paramBitmap, int paramInt1, int paramInt2)
  {
    c.g.b.k.b(parame, "pool");
    c.g.b.k.b(paramBitmap, "toTransform");
    parame = paramBitmap.copy(Bitmap.Config.ARGB_8888, true);
    new Canvas(parame).drawColor(b);
    c.g.b.k.a(parame, "tintBitmap");
    return parame;
  }
  
  public final void a(MessageDigest paramMessageDigest)
  {
    c.g.b.k.b(paramMessageDigest, "messageDigest");
    super.a(paramMessageDigest);
    paramMessageDigest.update(ByteBuffer.allocate(4).putInt(b).array());
  }
  
  public final boolean equals(Object paramObject)
  {
    if (super.equals(paramObject)) {
      if (paramObject != null)
      {
        if (b == b) {
          return true;
        }
      }
      else {
        throw new u("null cannot be cast to non-null type com.truecaller.common.util.TintTransformation");
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    return a(new int[] { super.hashCode(), com.bumptech.glide.g.k.b(b) });
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.ap
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */