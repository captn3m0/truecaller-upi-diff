package com.truecaller.common.h;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.v4.content.FileProvider;
import com.d.b.d;
import com.d.b.w;
import java.io.File;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class n
{
  private static Uri a;
  private static Uri b;
  private static Set<Uri> c = new HashSet();
  
  public static Intent a()
  {
    return new Intent().setType("image/*").setAction("android.intent.action.GET_CONTENT");
  }
  
  public static Intent a(Context paramContext)
  {
    Uri localUri = b(paramContext);
    Intent localIntent = new Intent("android.media.action.IMAGE_CAPTURE").putExtra("return-data", false).putExtra("output", localUri);
    if (Build.VERSION.SDK_INT < 21)
    {
      Iterator localIterator = paramContext.getPackageManager().queryIntentActivities(localIntent, 65536).iterator();
      while (localIterator.hasNext()) {
        paramContext.grantUriPermission(nextactivityInfo.packageName, localUri, 3);
      }
    }
    return localIntent;
  }
  
  public static Intent a(Context paramContext, Uri paramUri)
  {
    paramUri = new Intent("com.android.camera.action.CROP").setDataAndType(paramUri, "image/*").putExtra("outputX", 800).putExtra("outputY", 800).putExtra("aspectX", 1).putExtra("aspectY", 1).putExtra("scale", true).putExtra("scaleUpIfNeeded", true).putExtra("return-data", false);
    if (b == null) {
      b = FileProvider.a(paramContext, ai.a(paramContext), new File(paramContext.getCacheDir(), "crop.jpg"));
    }
    paramUri = paramUri.putExtra("output", b);
    paramUri.addFlags(1);
    paramUri.addFlags(2);
    Iterator localIterator = paramContext.getPackageManager().queryIntentActivities(paramUri, 65536).iterator();
    while (localIterator.hasNext())
    {
      String str = nextactivityInfo.packageName;
      a(paramContext, a, new String[] { str });
      a(paramContext, b, new String[] { str });
    }
    return paramUri;
  }
  
  private static void a(Context paramContext, Uri paramUri, String... paramVarArgs)
  {
    int i = 0;
    while (i <= 0)
    {
      paramContext.grantUriPermission(paramVarArgs[0], paramUri, 3);
      i += 1;
    }
    c.add(paramUri);
  }
  
  public static void a(Uri paramUri, Context paramContext)
  {
    if (paramUri != null)
    {
      paramContext = w.a(paramContext);
      if (paramUri != null)
      {
        g.b(paramUri.toString());
        return;
      }
      throw new IllegalArgumentException("uri == null");
    }
  }
  
  /* Error */
  public static byte[] a(android.content.ContentResolver paramContentResolver, Uri paramUri, int paramInt1, int paramInt2, int paramInt3, android.graphics.Bitmap.CompressFormat paramCompressFormat, int paramInt4)
    throws java.io.IOException, java.lang.SecurityException
  {
    // Byte code:
    //   0: new 200	android/graphics/BitmapFactory$Options
    //   3: dup
    //   4: invokespecial 201	android/graphics/BitmapFactory$Options:<init>	()V
    //   7: astore 8
    //   9: aload 8
    //   11: iconst_0
    //   12: putfield 205	android/graphics/BitmapFactory$Options:inScaled	Z
    //   15: aload 8
    //   17: iconst_0
    //   18: putfield 208	android/graphics/BitmapFactory$Options:inDensity	I
    //   21: aload 8
    //   23: iconst_0
    //   24: putfield 211	android/graphics/BitmapFactory$Options:inTargetDensity	I
    //   27: aload 8
    //   29: iload_2
    //   30: iload_3
    //   31: idiv
    //   32: putfield 214	android/graphics/BitmapFactory$Options:inSampleSize	I
    //   35: aload 8
    //   37: iconst_0
    //   38: putfield 217	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
    //   41: aload 8
    //   43: iconst_0
    //   44: putfield 220	android/graphics/BitmapFactory$Options:inMutable	Z
    //   47: aload_0
    //   48: aload_1
    //   49: invokevirtual 226	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   52: astore_1
    //   53: aload_1
    //   54: aconst_null
    //   55: aload 8
    //   57: invokestatic 232	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   60: astore 7
    //   62: aload 8
    //   64: getfield 235	android/graphics/BitmapFactory$Options:outWidth	I
    //   67: iload_3
    //   68: if_icmpgt +16 -> 84
    //   71: aload 7
    //   73: astore_0
    //   74: aload 8
    //   76: getfield 238	android/graphics/BitmapFactory$Options:outHeight	I
    //   79: iload 4
    //   81: if_icmple +13 -> 94
    //   84: aload 7
    //   86: iload_3
    //   87: iload 4
    //   89: iconst_0
    //   90: invokestatic 244	android/graphics/Bitmap:createScaledBitmap	(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    //   93: astore_0
    //   94: new 246	java/io/ByteArrayOutputStream
    //   97: dup
    //   98: invokespecial 247	java/io/ByteArrayOutputStream:<init>	()V
    //   101: astore 7
    //   103: aload_0
    //   104: aload 5
    //   106: iload 6
    //   108: aload 7
    //   110: invokevirtual 251	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   113: pop
    //   114: aload 7
    //   116: invokevirtual 255	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   119: astore_0
    //   120: aload_1
    //   121: invokestatic 260	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   124: aload_0
    //   125: areturn
    //   126: astore 5
    //   128: aload_1
    //   129: astore_0
    //   130: aload 5
    //   132: astore_1
    //   133: goto +6 -> 139
    //   136: astore_1
    //   137: aconst_null
    //   138: astore_0
    //   139: aload_0
    //   140: invokestatic 260	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   143: aload_1
    //   144: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	145	0	paramContentResolver	android.content.ContentResolver
    //   0	145	1	paramUri	Uri
    //   0	145	2	paramInt1	int
    //   0	145	3	paramInt2	int
    //   0	145	4	paramInt3	int
    //   0	145	5	paramCompressFormat	android.graphics.Bitmap.CompressFormat
    //   0	145	6	paramInt4	int
    //   60	55	7	localObject	Object
    //   7	68	8	localOptions	android.graphics.BitmapFactory.Options
    // Exception table:
    //   from	to	target	type
    //   53	71	126	finally
    //   74	84	126	finally
    //   84	94	126	finally
    //   94	120	126	finally
    //   47	53	136	finally
  }
  
  public static Uri b(Context paramContext)
  {
    if (a == null) {
      a = FileProvider.a(paramContext, ai.a(paramContext), new File(paramContext.getCacheDir(), "capture.jpg"));
    }
    return a;
  }
  
  public static Uri c(Context paramContext)
  {
    return Uri.fromFile(new File(paramContext.getCacheDir(), "crop.jpg"));
  }
  
  public static Uri d(Context paramContext)
  {
    return Uri.fromFile(new File(paramContext.getCacheDir(), "capture.jpg"));
  }
  
  public static void e(Context paramContext)
  {
    Iterator localIterator = c.iterator();
    while (localIterator.hasNext()) {
      paramContext.revokeUriPermission((Uri)localIterator.next(), 3);
    }
  }
  
  public static void f(Context paramContext)
  {
    File localFile = paramContext.getCacheDir();
    new File(localFile, "capture.jpg").delete();
    new File(localFile, "crop.jpg").delete();
    e(paramContext);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */