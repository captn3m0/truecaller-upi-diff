package com.truecaller.common.h;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Build.VERSION;
import com.truecaller.log.AssertionUtil;
import java.io.File;
import java.io.FilenameFilter;

public final class ai
{
  private static Intent a(Intent paramIntent, String paramString, IntentSender paramIntentSender)
  {
    if ((Build.VERSION.SDK_INT >= 22) && (paramIntentSender != null)) {
      return Intent.createChooser(paramIntent, paramString, paramIntentSender).setFlags(268435456);
    }
    return Intent.createChooser(paramIntent, paramString).setFlags(268435456);
  }
  
  public static Intent a(String paramString1, String paramString2, CharSequence paramCharSequence)
  {
    return a(paramString1, paramString2, paramCharSequence, null, null, null);
  }
  
  public static Intent a(String paramString1, String paramString2, CharSequence paramCharSequence, Uri paramUri, String paramString3, IntentSender paramIntentSender)
  {
    Intent localIntent = new Intent("android.intent.action.SEND");
    if (paramString2 != null) {
      localIntent.putExtra("android.intent.extra.SUBJECT", paramString2);
    }
    localIntent.putExtra("android.intent.extra.TEXT", paramCharSequence);
    if (paramUri != null)
    {
      if ("file".equalsIgnoreCase(paramUri.getScheme()))
      {
        paramString2 = new StringBuilder("File URI for intent: ");
        paramString2.append(paramUri.toString());
        AssertionUtil.reportWeirdnessButNeverCrash(paramString2.toString());
      }
      localIntent.setType(paramString3).putExtra("android.intent.extra.STREAM", paramUri);
    }
    else
    {
      localIntent.setType("text/plain");
    }
    return a(localIntent, paramString1, paramIntentSender);
  }
  
  /* Error */
  public static Uri a(Context paramContext, android.graphics.Bitmap paramBitmap)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 6
    //   3: aload_1
    //   4: ifnonnull +5 -> 9
    //   7: aconst_null
    //   8: areturn
    //   9: ldc 100
    //   11: invokestatic 105	android/os/Environment:getExternalStorageState	()Ljava/lang/String;
    //   14: invokevirtual 109	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   17: ifne +9 -> 26
    //   20: aconst_null
    //   21: astore 4
    //   23: goto +112 -> 135
    //   26: aload_0
    //   27: invokevirtual 115	android/content/Context:getExternalCacheDir	()Ljava/io/File;
    //   30: astore 4
    //   32: aload 4
    //   34: ifnonnull +9 -> 43
    //   37: aconst_null
    //   38: astore 4
    //   40: goto +95 -> 135
    //   43: aload 4
    //   45: new 6	com/truecaller/common/h/ai$1
    //   48: dup
    //   49: invokespecial 118	com/truecaller/common/h/ai$1:<init>	()V
    //   52: invokevirtual 124	java/io/File:listFiles	(Ljava/io/FilenameFilter;)[Ljava/io/File;
    //   55: astore 5
    //   57: aload 5
    //   59: arraylength
    //   60: istore_3
    //   61: iconst_0
    //   62: istore_2
    //   63: iload_2
    //   64: iload_3
    //   65: if_icmpge +18 -> 83
    //   68: aload 5
    //   70: iload_2
    //   71: aaload
    //   72: invokevirtual 128	java/io/File:delete	()Z
    //   75: pop
    //   76: iload_2
    //   77: iconst_1
    //   78: iadd
    //   79: istore_2
    //   80: goto -17 -> 63
    //   83: new 66	java/lang/StringBuilder
    //   86: dup
    //   87: ldc -126
    //   89: invokespecial 69	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   92: astore 5
    //   94: aload 5
    //   96: invokestatic 136	java/lang/System:currentTimeMillis	()J
    //   99: invokevirtual 139	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   102: pop
    //   103: aload 5
    //   105: ldc -115
    //   107: invokevirtual 76	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   110: pop
    //   111: aload 5
    //   113: ldc -113
    //   115: invokevirtual 76	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   118: pop
    //   119: new 120	java/io/File
    //   122: dup
    //   123: aload 4
    //   125: aload 5
    //   127: invokevirtual 77	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   130: invokespecial 146	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   133: astore 4
    //   135: aload 4
    //   137: ifnonnull +5 -> 142
    //   140: aconst_null
    //   141: areturn
    //   142: new 148	java/io/BufferedOutputStream
    //   145: dup
    //   146: new 150	java/io/FileOutputStream
    //   149: dup
    //   150: aload 4
    //   152: invokespecial 153	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   155: invokespecial 156	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   158: astore 5
    //   160: aload_1
    //   161: getstatic 162	android/graphics/Bitmap$CompressFormat:JPEG	Landroid/graphics/Bitmap$CompressFormat;
    //   164: bipush 90
    //   166: aload 5
    //   168: invokevirtual 168	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   171: pop
    //   172: aload_0
    //   173: aload_0
    //   174: invokestatic 171	com/truecaller/common/h/ai:a	(Landroid/content/Context;)Ljava/lang/String;
    //   177: aload 4
    //   179: invokestatic 176	android/support/v4/content/FileProvider:a	(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Landroid/net/Uri;
    //   182: astore_0
    //   183: aload 5
    //   185: invokestatic 181	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   188: aload_0
    //   189: areturn
    //   190: astore_1
    //   191: aload 5
    //   193: astore_0
    //   194: goto +7 -> 201
    //   197: astore_1
    //   198: aload 6
    //   200: astore_0
    //   201: aload_0
    //   202: invokestatic 181	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   205: aload_1
    //   206: athrow
    //   207: aconst_null
    //   208: astore 5
    //   210: aload 5
    //   212: invokestatic 181	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   215: aconst_null
    //   216: areturn
    //   217: astore_0
    //   218: goto -11 -> 207
    //   221: astore_0
    //   222: goto -12 -> 210
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	225	0	paramContext	Context
    //   0	225	1	paramBitmap	android.graphics.Bitmap
    //   62	18	2	i	int
    //   60	6	3	j	int
    //   21	157	4	localFile	File
    //   55	156	5	localObject1	Object
    //   1	198	6	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   160	183	190	finally
    //   142	160	197	finally
    //   142	160	217	java/io/IOException
    //   160	183	221	java/io/IOException
  }
  
  public static String a(Context paramContext)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramContext.getApplicationContext().getPackageName());
    localStringBuilder.append(".fileprovider");
    return localStringBuilder.toString();
  }
  
  public static boolean a(Context paramContext, String paramString1, String paramString2, CharSequence paramCharSequence, Uri paramUri)
  {
    return a(paramContext, paramString1, paramString2, paramCharSequence, paramUri, null);
  }
  
  public static boolean a(Context paramContext, String paramString1, String paramString2, CharSequence paramCharSequence, Uri paramUri, IntentSender paramIntentSender)
  {
    if ((k.e()) && (paramIntentSender != null)) {
      return o.a(paramContext, a(paramString1, paramString2, paramCharSequence, paramUri, "image/jpeg", paramIntentSender));
    }
    return o.a(paramContext, a(paramString1, paramString2, paramCharSequence, paramUri, "image/jpeg", null));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.ai
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */