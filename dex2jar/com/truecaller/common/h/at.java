package com.truecaller.common.h;

import android.content.Context;
import com.truecaller.common.a.a;
import javax.inject.Provider;

public final class at
  implements dagger.a.d<d>
{
  private final Provider<Context> a;
  private final Provider<a> b;
  
  private at(Provider<Context> paramProvider, Provider<a> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static at a(Provider<Context> paramProvider, Provider<a> paramProvider1)
  {
    return new at(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.at
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */