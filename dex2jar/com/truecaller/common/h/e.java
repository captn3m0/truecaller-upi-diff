package com.truecaller.common.h;

import android.content.Context;
import android.telephony.TelephonyManager;
import c.g.b.k;
import c.u;
import com.truecaller.common.a.a;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import javax.inject.Inject;

public final class e
  implements d
{
  private final TelephonyManager a;
  private final Context b;
  private final a c;
  
  @Inject
  public e(Context paramContext, a parama)
  {
    b = paramContext;
    c = parama;
    paramContext = b.getSystemService("phone");
    if (paramContext != null)
    {
      a = ((TelephonyManager)paramContext);
      return;
    }
    throw new u("null cannot be cast to non-null type android.telephony.TelephonyManager");
  }
  
  private static Object a(Object paramObject, String paramString)
  {
    paramString = paramObject.getClass().getDeclaredMethod(paramString, new Class[0]);
    paramString.setAccessible(true);
    return paramString.invoke(paramObject, new Object[0]);
  }
  
  private static Object a(Object paramObject, String paramString, Object... paramVarArgs)
  {
    Class localClass = paramObject.getClass();
    Object localObject = (Collection)new ArrayList(1);
    int i = 0;
    while (i <= 0)
    {
      ((Collection)localObject).add(paramVarArgs[0].getClass());
      i += 1;
    }
    localObject = ((Collection)localObject).toArray(new Class[0]);
    if (localObject != null)
    {
      localObject = (Class[])localObject;
      paramString = localClass.getDeclaredMethod(paramString, (Class[])Arrays.copyOf((Object[])localObject, localObject.length));
      paramString.setAccessible(true);
      return paramString.invoke(paramObject, new Object[] { paramVarArgs });
    }
    throw new u("null cannot be cast to non-null type kotlin.Array<T>");
  }
  
  public final boolean a()
  {
    try
    {
      Object localObject1 = a(a, "getITelephony");
      if (localObject1 != null) {
        a(localObject1, "endCall");
      }
      return true;
    }
    catch (Exception localException1)
    {
      c.a(0, localException1);
      try
      {
        Object localObject2 = a(a, "getITelephonyMSim");
        if (localObject2 != null)
        {
          if (k.a(a(localObject2, "endCall", new Object[] { Integer.valueOf(0) }), Boolean.FALSE)) {
            a(localObject2, "endCall", new Object[] { Integer.valueOf(1) });
          }
          return true;
        }
      }
      catch (Exception localException2)
      {
        c.a(1, localException2);
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */