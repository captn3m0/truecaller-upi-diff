package com.truecaller.common.h;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import com.google.c.a.g;
import com.google.c.a.g.a;
import com.google.c.a.k.c;
import com.google.c.a.m.a;
import com.google.c.a.n;
import com.truecaller.common.b.a;
import com.truecaller.log.AssertionUtil;
import java.util.Locale;

@Deprecated
public final class aa
{
  public static String a(String paramString)
    throws g
  {
    return a(paramString, a.F().H(), k.c.a);
  }
  
  public static String a(String paramString1, String paramString2)
    throws g
  {
    return a(paramString1, paramString2, k.c.a);
  }
  
  private static String a(String paramString1, String paramString2, k.c paramc)
    throws g
  {
    if (am.b(paramString1)) {}
    try
    {
      boolean bool = PhoneNumberUtils.isEmergencyNumber(paramString1);
      if (bool) {
        return paramString1;
      }
    }
    catch (Exception localException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localException);
      Object localObject = c(paramString2);
      n localn;
      if (!TextUtils.isEmpty((CharSequence)localObject))
      {
        paramString2 = com.google.c.a.k.a();
        localn = n.a();
      }
      try
      {
        localObject = paramString2.a(paramString1, (String)localObject);
        if (!paramString2.e((m.a)localObject)) {
          return paramString1;
        }
        if (localn.a((m.a)localObject)) {
          return paramString1;
        }
        return paramString2.a((m.a)localObject, paramc);
      }
      catch (IllegalStateException paramString2)
      {
        return paramString1;
      }
      throw new g(g.a.a, "Bad country ISO code, ".concat(String.valueOf(paramString2)));
      throw paramString1;
      throw new g(g.a.b, paramString1);
    }
    finally
    {
      for (;;) {}
    }
  }
  
  public static String b(String paramString)
  {
    if (!TextUtils.isEmpty(paramString)) {}
    try
    {
      String str = a(paramString);
      return str;
    }
    catch (g localg) {}
    return paramString;
    return paramString;
  }
  
  public static String b(String paramString1, String paramString2)
  {
    if (!TextUtils.isEmpty(paramString1)) {}
    try
    {
      paramString2 = a(paramString1, paramString2, k.c.a);
      return paramString2;
    }
    catch (g paramString2) {}
    return paramString1;
    return paramString1;
  }
  
  public static String c(String paramString)
  {
    String str = f(paramString);
    paramString = str;
    if (TextUtils.isEmpty(str))
    {
      a locala = a.F();
      str = f(locala.H());
      paramString = str;
      if (TextUtils.isEmpty(str)) {
        paramString = f(k.f(locala));
      }
      if ((TextUtils.isEmpty(paramString)) && (!locala.o())) {
        return f(getResourcesgetConfigurationlocale.getCountry());
      }
    }
    return paramString;
  }
  
  public static String c(String paramString1, String paramString2)
  {
    if (TextUtils.isEmpty(paramString2)) {
      paramString2 = b(paramString1);
    } else {
      paramString2 = b(paramString1, paramString2);
    }
    if (!TextUtils.isEmpty(paramString2)) {
      paramString1 = paramString2;
    }
    paramString2 = paramString1;
    if (paramString1.startsWith("+")) {
      paramString2 = paramString1.substring(1);
    }
    return paramString2;
  }
  
  public static String d(String paramString)
  {
    try
    {
      String str = a(paramString, a.F().H(), k.c.b);
      return str;
    }
    catch (g localg) {}
    return paramString;
  }
  
  public static String d(String paramString1, String paramString2)
  {
    String str = paramString2;
    if (TextUtils.isEmpty(paramString2)) {
      str = a.F().H();
    }
    try
    {
      if (!TextUtils.isEmpty(str))
      {
        if (str.equals(String.valueOf(aab))) {
          return a(paramString1, str, k.c.c);
        }
        return a(paramString1, str, k.c.b);
      }
      throw new g(g.a.a, "Bad country ISO code, ".concat(String.valueOf(str)));
    }
    catch (g paramString2) {}
    return paramString1;
  }
  
  public static String e(String paramString)
  {
    return d(paramString, null);
  }
  
  private static String f(String paramString)
  {
    if ((!am.d(paramString)) && (paramString.length() == 2)) {
      return am.c(paramString, Locale.ENGLISH);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.aa
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */