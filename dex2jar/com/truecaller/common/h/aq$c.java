package com.truecaller.common.h;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;

public final class aq$c
  extends aq.d
{
  private final float a;
  private final float b;
  private final float c;
  private final float d;
  private final boolean e;
  
  public aq$c(float paramFloat)
  {
    this(paramFloat, paramFloat, paramFloat, paramFloat);
  }
  
  public aq$c(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    a = paramFloat1;
    b = paramFloat2;
    c = paramFloat3;
    d = paramFloat4;
    boolean bool;
    if ((paramFloat1 == paramFloat2) && (paramFloat2 == paramFloat3) && (paramFloat3 == paramFloat4)) {
      bool = true;
    } else {
      bool = false;
    }
    e = bool;
  }
  
  public final String a()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(super.a());
    localStringBuilder.append(a);
    localStringBuilder.append("x");
    localStringBuilder.append(b);
    localStringBuilder.append("x");
    localStringBuilder.append(c);
    localStringBuilder.append("x");
    localStringBuilder.append(d);
    return localStringBuilder.toString();
  }
  
  final void a(Canvas paramCanvas, Bitmap paramBitmap, Paint paramPaint, RectF paramRectF)
  {
    Object localObject = Shader.TileMode.CLAMP;
    paramPaint.setShader(new BitmapShader(paramBitmap, (Shader.TileMode)localObject, (Shader.TileMode)localObject));
    if (e)
    {
      float f = a;
      paramCanvas.drawRoundRect(paramRectF, f, f, paramPaint);
      return;
    }
    paramBitmap = new float[8];
    paramBitmap[0] = a;
    paramBitmap[1] = paramBitmap[0];
    paramBitmap[2] = b;
    paramBitmap[3] = paramBitmap[2];
    paramBitmap[4] = c;
    paramBitmap[5] = paramBitmap[4];
    paramBitmap[6] = d;
    paramBitmap[7] = paramBitmap[6];
    localObject = new Path();
    ((Path)localObject).addRoundRect(paramRectF, paramBitmap, Path.Direction.CW);
    paramCanvas.drawPath((Path)localObject, paramPaint);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.aq.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */