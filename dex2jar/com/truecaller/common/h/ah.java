package com.truecaller.common.h;

import com.google.c.a.h;
import com.google.c.a.k;
import java.util.Iterator;
import java.util.Locale;

public final class ah
{
  static final String a = Character.toString('‎');
  static final String b = Character.toString('‬');
  
  public static String a(String paramString)
  {
    return a(paramString, null);
  }
  
  public static String a(String paramString1, String paramString2)
  {
    if ((paramString1 != null) && (!paramString1.isEmpty()))
    {
      if (paramString2 == null) {
        paramString2 = null;
      } else {
        paramString2 = paramString2.toUpperCase(Locale.ENGLISH);
      }
      paramString2 = k.a().c(paramString1, paramString2).iterator();
      if (!paramString2.hasNext()) {
        return paramString1;
      }
      paramString1 = new StringBuilder(paramString1);
      int i = 0;
      while (paramString2.hasNext())
      {
        h localh = (h)paramString2.next();
        paramString1.insert(a + i, a);
        i += a.length();
        paramString1.insert(localh.a() + i, b);
        i += b.length();
      }
      return paramString1.toString();
    }
    return "";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.ah
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */