package com.truecaller.common.h;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build.VERSION;
import android.telephony.PhoneNumberUtils;
import com.google.c.a.g;
import com.google.c.a.g.a;
import com.google.c.a.k;
import com.google.c.a.k.d;
import com.truecaller.common.R.string;
import com.truecaller.utils.f;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ab
{
  private static final List<String> a = Collections.singletonList("IT");
  private static final Pattern b = Pattern.compile("[a-zA-Z]{2}-?([a-zA-Z]{6})");
  private static final Pattern c = Pattern.compile("[,;pPwW*#]");
  
  public static int a()
  {
    return Resources.getSystem().getIdentifier("emergency_call_dialog_number_for_display", "string", "android");
  }
  
  public static int a(int paramInt, k.d paramd)
  {
    int i;
    switch (paramInt)
    {
    default: 
      i = 0;
      break;
    case 0: 
    case 1: 
    case 2: 
    case 3: 
    case 4: 
    case 5: 
    case 6: 
    case 7: 
    case 8: 
    case 9: 
    case 10: 
    case 11: 
    case 12: 
    case 13: 
    case 14: 
    case 15: 
    case 16: 
    case 17: 
    case 18: 
    case 19: 
    case 20: 
      i = 1;
    }
    if (i != 0) {
      return paramInt;
    }
    return a(paramd);
  }
  
  public static int a(k.d paramd)
  {
    int i = 10;
    if (paramd != null) {}
    switch (1.b[paramd.ordinal()])
    {
    default: 
      return 7;
    case 11: 
      return 7;
    case 9: 
      return 6;
    case 8: 
      return 2;
    case 7: 
      return 7;
    case 3: 
      return 2;
    case 2: 
      return 2;
    case 1: 
      return 1;
      i = 7;
    }
    return i;
  }
  
  public static k.d a(String paramString, k.d paramd)
  {
    k.d locald = paramd;
    if (paramString != null) {}
    try
    {
      locald = k.d.valueOf(paramString.toUpperCase());
      return locald;
    }
    catch (IllegalArgumentException paramString) {}
    return paramd;
  }
  
  public static String a(Context paramContext, String paramString, boolean paramBoolean)
  {
    if (d(paramString)) {
      return paramString;
    }
    if (paramBoolean) {
      return paramContext.getString(R.string.HistoryHiddenNumber);
    }
    return paramContext.getString(R.string.HistoryCallerUnknown);
  }
  
  public static String a(Intent paramIntent, Context paramContext)
  {
    Object localObject2 = paramIntent.getData();
    Object localObject1 = null;
    if (localObject2 == null) {
      return null;
    }
    paramIntent = ((Uri)localObject2).getScheme();
    if ((!paramIntent.equals("tel")) && (!paramIntent.equals("sip")) && (!paramIntent.equalsIgnoreCase("truecaller")))
    {
      if (paramContext == null) {
        return null;
      }
      paramIntent = ((Uri)localObject2).getAuthority();
      if ("contacts".equals(paramIntent))
      {
        paramIntent = "number";
      }
      else
      {
        if (!"com.android.contacts".equals(paramIntent)) {
          break label154;
        }
        paramIntent = "data1";
      }
      localObject2 = paramContext.getContentResolver().query((Uri)localObject2, new String[] { paramIntent }, null, null, null);
      if (localObject2 != null)
      {
        paramContext = (Context)localObject1;
        try
        {
          if (((Cursor)localObject2).moveToFirst()) {
            paramContext = ((Cursor)localObject2).getString(((Cursor)localObject2).getColumnIndex(paramIntent));
          }
          return paramContext;
        }
        finally
        {
          ((Cursor)localObject2).close();
        }
      }
      return null;
      label154:
      return null;
    }
    return ((Uri)localObject2).getSchemeSpecificPart();
  }
  
  public static boolean a(int paramInt)
  {
    String str = k.a().b(paramInt);
    return a.contains(str);
  }
  
  public static boolean a(Context paramContext, String paramString)
  {
    f localf = f.a;
    if (!f.a(paramString)) {
      return false;
    }
    if (Build.VERSION.SDK_INT >= 21) {
      return PhoneNumberUtils.isLocalEmergencyNumber(paramContext, paramString);
    }
    return PhoneNumberUtils.isEmergencyNumber(paramString);
  }
  
  public static boolean a(CharSequence paramCharSequence)
  {
    if (am.b(paramCharSequence)) {
      return false;
    }
    int i = 0;
    while (i < paramCharSequence.length())
    {
      int j = paramCharSequence.charAt(i);
      if (((j < 48) || (j > 57)) && (j != 42) && (j != 35) && (j != 43)) {
        j = 0;
      } else {
        j = 1;
      }
      if (j != 0) {
        return true;
      }
      i += 1;
    }
    return b(paramCharSequence);
  }
  
  public static boolean a(String paramString)
  {
    return (am.b(paramString)) || (paramString.equals("-2")) || (paramString.equals("-1"));
  }
  
  public static boolean a(String paramString1, String paramString2)
  {
    return a(paramString1, paramString2, false);
  }
  
  public static boolean a(String paramString1, String paramString2, boolean paramBoolean)
  {
    if (am.a(paramString1, paramString2)) {
      return !am.b(paramString1);
    }
    paramString1 = k.a().a(paramString1, paramString2);
    switch (1.a[paramString1.ordinal()])
    {
    default: 
      return false;
    case 2: 
      return !paramBoolean;
    }
    return true;
  }
  
  public static boolean b(CharSequence paramCharSequence)
  {
    if (paramCharSequence == null) {
      return false;
    }
    paramCharSequence = paramCharSequence.toString();
    return (paramCharSequence.contains("@")) || (paramCharSequence.contains("%40"));
  }
  
  public static boolean b(String paramString)
  {
    return c.matcher(paramString).find();
  }
  
  public static boolean b(String paramString1, String paramString2)
  {
    if (am.b(paramString1)) {
      return false;
    }
    try
    {
      if (!am.b(paramString2))
      {
        k localk = k.a();
        return localk.c(localk.a(paramString1, paramString2));
      }
      throw new g(g.a.a, "Bad country ISO code, ".concat(String.valueOf(paramString2)));
    }
    catch (g paramString1) {}
    return false;
  }
  
  public static String c(String paramString)
  {
    Matcher localMatcher = b.matcher(paramString);
    if (!localMatcher.matches()) {
      return paramString;
    }
    return localMatcher.group(1).toUpperCase();
  }
  
  public static boolean d(String paramString)
  {
    return (paramString != null) && (am.b(paramString)) && (paramString.length() >= 3);
  }
  
  public static boolean e(String paramString)
  {
    return (paramString != null) && (am.b(paramString)) && (paramString.length() >= 6);
  }
  
  public static boolean f(String paramString)
  {
    return (am.a(paramString)) && (paramString.length() < 20);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.ab
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */