package com.truecaller.common.h;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import c.g.b.k;
import com.bumptech.glide.load.b.a.e;

public final class m
  extends b
{
  public m()
  {
    super("com.truecaller.common.util.GradientTransformation");
  }
  
  public final Bitmap a(e parame, Bitmap paramBitmap, int paramInt1, int paramInt2)
  {
    k.b(parame, "pool");
    k.b(paramBitmap, "toTransform");
    paramInt1 = paramBitmap.getWidth();
    paramInt2 = paramBitmap.getHeight();
    parame = paramBitmap.copy(Bitmap.Config.ARGB_8888, true);
    paramBitmap = new Canvas(parame);
    int i = Color.argb(0, 0, 0, 0);
    int j = Color.argb(51, 0, 0, 0);
    float f1 = paramInt1 / 2;
    float f2 = paramInt2 / 2;
    float f3 = paramInt2;
    Object localObject = Shader.TileMode.CLAMP;
    localObject = new LinearGradient(f1, f2, f1, f3, new int[] { i, j }, null, (Shader.TileMode)localObject);
    Paint localPaint = new Paint(4);
    localPaint.setDither(true);
    localPaint.setFilterBitmap(true);
    localPaint.setShader((Shader)localObject);
    paramBitmap.drawPaint(localPaint);
    k.a(parame, "gradientBitmap");
    return parame;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */