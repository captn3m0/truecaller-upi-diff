package com.truecaller.common.b;

import com.truecaller.common.g.a;
import com.truecaller.common.h.ac;
import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d<com.truecaller.common.network.c>
{
  private final a.a a;
  private final Provider<a> b;
  private final Provider<ac> c;
  
  private c(a.a parama, Provider<a> paramProvider, Provider<ac> paramProvider1)
  {
    a = parama;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static c a(a.a parama, Provider<a> paramProvider, Provider<ac> paramProvider1)
  {
    return new c(parama, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.b.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */