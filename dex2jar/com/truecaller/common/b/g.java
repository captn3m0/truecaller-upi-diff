package com.truecaller.common.b;

import c.g.b.k;
import com.truecaller.log.b;

public final class g
  implements Thread.UncaughtExceptionHandler
{
  private final Thread.UncaughtExceptionHandler a;
  
  public g(Thread.UncaughtExceptionHandler paramUncaughtExceptionHandler)
  {
    a = paramUncaughtExceptionHandler;
  }
  
  public final void uncaughtException(Thread paramThread, Throwable paramThrowable)
  {
    k.b(paramThread, "thread");
    if (!(paramThrowable instanceof VirtualMachineError))
    {
      localObject = b.a(paramThrowable);
      if (localObject == null)
      {
        if (paramThrowable != null) {
          paramThrowable = paramThrowable.getClass();
        } else {
          paramThrowable = null;
        }
        paramThrowable = (Throwable)new f(paramThrowable);
      }
      else
      {
        paramThrowable = (Throwable)localObject;
      }
    }
    Object localObject = a;
    if (localObject != null)
    {
      ((Thread.UncaughtExceptionHandler)localObject).uncaughtException(paramThread, paramThrowable);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.b.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */