package com.truecaller.common.b;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.j;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.security.ProviderInstaller;
import com.truecaller.common.account.r;
import com.truecaller.common.h.ac;
import com.truecaller.common.h.aj;
import com.truecaller.common.h.ak;
import com.truecaller.common.h.am;
import com.truecaller.common.h.h;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.n;

public abstract class a
  extends Application
{
  private static volatile a a;
  private static boolean b;
  public com.truecaller.common.background.b c;
  private boolean d = false;
  
  public a()
  {
    b = false;
    a = this;
    AssertionUtil.setIsDebugBuild(false);
  }
  
  public static a F()
  {
    boolean bool;
    if (a != null) {
      bool = true;
    } else {
      bool = false;
    }
    AssertionUtil.isTrue(bool, new String[0]);
    return a;
  }
  
  public static boolean G()
  {
    return b;
  }
  
  public final String H()
  {
    return am.n(u().k().a());
  }
  
  public final com.truecaller.common.background.b I()
  {
    return c;
  }
  
  public final void J()
  {
    c.a();
  }
  
  public abstract Intent a(Context paramContext);
  
  public void a(Activity paramActivity) {}
  
  public void a(boolean paramBoolean)
  {
    u().e().c();
    J();
  }
  
  public final void a(int... paramVarArgs)
  {
    c.a(10004, paramVarArgs);
  }
  
  public abstract boolean a(j paramj);
  
  public boolean a(String paramString1, boolean paramBoolean, String paramString2)
    throws SecurityException
  {
    int i;
    if ((paramString1 != null) && (u().k().a(paramString1, paramString2))) {
      i = 1;
    } else {
      i = 0;
    }
    if ((i == 0) && (!paramBoolean)) {
      return false;
    }
    a(paramBoolean);
    return true;
  }
  
  public abstract com.truecaller.common.profile.e b();
  
  public abstract com.truecaller.common.f.c c();
  
  public abstract com.truecaller.common.f.b d();
  
  public abstract com.truecaller.common.h.c e();
  
  public abstract com.truecaller.featuretoggles.e f();
  
  public abstract com.truecaller.content.d.a g();
  
  public abstract Boolean h();
  
  protected void i()
  {
    e.a = getSharedPreferences(w(), 0);
  }
  
  protected void j()
  {
    h.a(getApplicationContext());
  }
  
  public abstract String k();
  
  public abstract String l();
  
  public abstract boolean m();
  
  public abstract String n();
  
  public boolean o()
  {
    return true;
  }
  
  public void onCreate()
  {
    c = new com.truecaller.common.background.d(this);
    super.onCreate();
    boolean bool2 = true;
    try
    {
      ProviderInstaller.a(this);
      d = true;
    }
    catch (GooglePlayServicesRepairableException|GooglePlayServicesNotAvailableException localGooglePlayServicesRepairableException)
    {
      boolean bool1;
      for (;;) {}
    }
    d = false;
    i();
    bool1 = bool2;
    if (!b) {
      if (e.a("qaEnableLogging", false)) {
        bool1 = bool2;
      } else {
        bool1 = false;
      }
    }
    com.truecaller.log.f.a = bool1;
    c.a(new -..Lambda.LM_tgxF0BTlT3KWXc2q6MILp_BM(this));
    c.e();
  }
  
  public abstract boolean p();
  
  public boolean q()
  {
    return false;
  }
  
  public abstract com.truecaller.common.a u();
  
  public abstract com.truecaller.analytics.d v();
  
  public abstract String w();
  
  public class a
  {
    public a() {}
    
    static aj a(Context paramContext, n paramn)
    {
      return new ak(paramContext, paramn);
    }
    
    public static com.truecaller.common.network.c a(com.truecaller.common.g.a parama, ac paramac)
    {
      return new com.truecaller.common.network.d(parama, paramac);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.b.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */