package com.truecaller.common.b;

import android.content.Context;
import com.truecaller.common.h.aj;
import com.truecaller.utils.n;
import javax.inject.Provider;

public final class d
  implements dagger.a.d<aj>
{
  private final a.a a;
  private final Provider<Context> b;
  private final Provider<n> c;
  
  private d(a.a parama, Provider<Context> paramProvider, Provider<n> paramProvider1)
  {
    a = parama;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static d a(a.a parama, Provider<Context> paramProvider, Provider<n> paramProvider1)
  {
    return new d(parama, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.b.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */