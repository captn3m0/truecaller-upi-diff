package com.truecaller.common.account;

import android.accounts.AccountManager;
import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class a
  implements d<AccountManager>
{
  private final Provider<Context> a;
  
  private a(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static a a(Provider<Context> paramProvider)
  {
    return new a(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */