package com.truecaller.common.account;

import android.content.Context;
import dagger.a.d;
import java.io.File;
import javax.inject.Provider;

public final class e
  implements d<File>
{
  private final Provider<Context> a;
  
  private e(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static e a(Provider<Context> paramProvider)
  {
    return new e(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */