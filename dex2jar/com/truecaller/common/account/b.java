package com.truecaller.common.account;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class b
  implements d<String>
{
  private final Provider<Context> a;
  
  private b(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static b a(Provider<Context> paramProvider)
  {
    return new b(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */