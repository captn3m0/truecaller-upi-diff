package com.truecaller.common.account;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.text.TextUtils;
import com.truecaller.common.R.string;

public final class n
  extends AbstractAccountAuthenticator
{
  protected final Context a;
  
  public n(Context paramContext)
  {
    super(paramContext);
    a = paramContext;
  }
  
  public final Bundle addAccount(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, String paramString1, String paramString2, String[] paramArrayOfString, Bundle paramBundle)
    throws NetworkErrorException
  {
    paramAccountAuthenticatorResponse = new Intent("android.intent.action.MAIN");
    paramAccountAuthenticatorResponse.setPackage(a.getApplicationInfo().packageName);
    paramAccountAuthenticatorResponse.addCategory("android.intent.category.DEFAULT");
    paramAccountAuthenticatorResponse.setFlags(268435456);
    paramString1 = new Bundle(1);
    paramString1.putParcelable("intent", paramAccountAuthenticatorResponse);
    return paramString1;
  }
  
  public final Bundle confirmCredentials(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, Account paramAccount, Bundle paramBundle)
    throws NetworkErrorException
  {
    paramAccountAuthenticatorResponse = new Bundle(1);
    paramAccountAuthenticatorResponse.putBoolean("booleanResult", true);
    return paramAccountAuthenticatorResponse;
  }
  
  public final Bundle editProperties(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, String paramString)
  {
    return null;
  }
  
  public final Bundle getAuthToken(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, Account paramAccount, String paramString, Bundle paramBundle)
    throws NetworkErrorException
  {
    if ((!"com.truecaller.auth_token_default".equals(paramString)) && (!"installation_id".equals(paramString)))
    {
      paramAccountAuthenticatorResponse = new Bundle(2);
      paramAccountAuthenticatorResponse.putInt("errorCode", 7);
      paramAccountAuthenticatorResponse.putString("errorMessage", "Unsupported auth token type");
      return paramAccountAuthenticatorResponse;
    }
    paramAccountAuthenticatorResponse = a.getString(R.string.authenticator_account_type);
    if (!paramAccountAuthenticatorResponse.equals(type))
    {
      paramAccountAuthenticatorResponse = new Bundle(2);
      paramAccountAuthenticatorResponse.putInt("errorCode", 7);
      paramAccountAuthenticatorResponse.putString("errorMessage", "Wrong account type");
      return paramAccountAuthenticatorResponse;
    }
    paramString = AccountManager.get(a).peekAuthToken(paramAccount, paramString);
    if (!TextUtils.isEmpty(paramString))
    {
      paramBundle = new Bundle(3);
      paramBundle.putString("authAccount", name);
      paramBundle.putString("accountType", paramAccountAuthenticatorResponse);
      paramBundle.putString("authtoken", paramString);
      return paramBundle;
    }
    paramAccountAuthenticatorResponse = new Bundle(2);
    paramAccountAuthenticatorResponse.putInt("errorCode", 5);
    paramAccountAuthenticatorResponse.putString("errorMessage", "Missing register ID");
    return paramAccountAuthenticatorResponse;
  }
  
  public final String getAuthTokenLabel(String paramString)
  {
    if ("installation_id".equals(paramString)) {
      return a.getString(R.string.authenticator_account_name);
    }
    return null;
  }
  
  public final Bundle hasFeatures(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, Account paramAccount, String[] paramArrayOfString)
    throws NetworkErrorException
  {
    return null;
  }
  
  public final Bundle updateCredentials(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, Account paramAccount, String paramString, Bundle paramBundle)
    throws NetworkErrorException
  {
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */