package com.truecaller.common.account;

import android.accounts.AccountManager;
import android.app.backup.BackupManager;
import com.truecaller.common.g.a;
import dagger.a.d;
import java.io.File;
import javax.inject.Provider;

public final class q
  implements d<p>
{
  private final Provider<String> a;
  private final Provider<String> b;
  private final Provider<File> c;
  private final Provider<AccountManager> d;
  private final Provider<BackupManager> e;
  private final Provider<a> f;
  
  private q(Provider<String> paramProvider1, Provider<String> paramProvider2, Provider<File> paramProvider, Provider<AccountManager> paramProvider3, Provider<BackupManager> paramProvider4, Provider<a> paramProvider5)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
  }
  
  public static q a(Provider<String> paramProvider1, Provider<String> paramProvider2, Provider<File> paramProvider, Provider<AccountManager> paramProvider3, Provider<BackupManager> paramProvider4, Provider<a> paramProvider5)
  {
    return new q(paramProvider1, paramProvider2, paramProvider, paramProvider3, paramProvider4, paramProvider5);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */