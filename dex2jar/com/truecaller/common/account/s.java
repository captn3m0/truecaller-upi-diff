package com.truecaller.common.account;

import c.g.b.k;
import c.x;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.common.network.account.ExchangeCredentialsResponseDto;
import com.truecaller.common.network.c;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;

public final class s
  implements r
{
  private long a;
  private int b;
  private final Object c;
  private final Object d;
  private final com.truecaller.common.g.a e;
  private final com.truecaller.utils.a f;
  private final c g;
  private final o h;
  private final b i;
  private final com.truecaller.common.account.a.a j;
  private final g k;
  
  @Inject
  public s(com.truecaller.common.g.a parama, com.truecaller.utils.a parama1, c paramc, o paramo, b paramb, com.truecaller.common.account.a.a parama2, g paramg)
  {
    e = parama;
    f = parama1;
    g = paramc;
    h = paramo;
    i = paramb;
    j = parama2;
    k = paramg;
    c = new Object();
    d = new Object();
  }
  
  private final String a(String paramString)
  {
    try
    {
      Object localObject = k.a(paramString);
      ExchangeCredentialsResponseDto localExchangeCredentialsResponseDto = (ExchangeCredentialsResponseDto)((e.r)localObject).e();
      if ((((e.r)localObject).d()) && (localExchangeCredentialsResponseDto != null))
      {
        localObject = localExchangeCredentialsResponseDto.getDomain();
        if (localObject != null) {
          g.a((String)localObject);
        }
        a = 0L;
        b = 0;
        l = TimeUnit.SECONDS.toMillis(localExchangeCredentialsResponseDto.getTtl());
        if ((k.a(localExchangeCredentialsResponseDto.getState(), "exchanged")) && (localExchangeCredentialsResponseDto.getInstallationId() != null))
        {
          a(localExchangeCredentialsResponseDto.getInstallationId(), l);
          return localExchangeCredentialsResponseDto.getInstallationId();
        }
        a(paramString, l);
        return paramString;
      }
      if (((e.r)localObject).b() == 401)
      {
        a(paramString, "ExchangeCredentials");
        return null;
      }
      long l = Math.min(u.a() << b, u.b());
      a = (f.b() + l);
      b += 1;
      return paramString;
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
    a = (f.b() + u.c());
    return paramString;
  }
  
  private final void a(j paramj)
  {
    a(a, 0L, c, b);
  }
  
  private final j f()
  {
    for (;;)
    {
      synchronized (c)
      {
        Object localObject1 = e.a("installationId");
        Object localObject4 = e.a("profileNumber");
        String str = e.a("profileCountryIso");
        if ((localObject1 != null) && (str != null) && (localObject4 != null))
        {
          localObject1 = new j((String)localObject1, (String)localObject4, str);
          return (j)localObject1;
        }
        localObject1 = j.a();
        if (localObject1 != null)
        {
          a((j)localObject1);
          j.b();
          localObject4 = localObject1;
          if (localObject1 == null)
          {
            localObject4 = h.a();
            if (localObject4 != null) {
              a((j)localObject4);
            } else {
              localObject4 = null;
            }
          }
          return (j)localObject4;
        }
      }
      Object localObject3 = null;
    }
  }
  
  public final String a()
  {
    j localj = f();
    if (localj != null) {
      return c;
    }
    return null;
  }
  
  public final void a(String paramString, long paramLong)
  {
    k.b(paramString, "newInstallationId");
    synchronized (c)
    {
      e.a("installationId", paramString);
      e.b("installationIdFetchTime", f.a());
      e.b("installationIdTtl", paramLong);
      String str1 = e.a("profileNumber");
      if (str1 == null) {
        return;
      }
      String str2 = e.a("profileCountryIso");
      if (str2 == null) {
        return;
      }
      h.a(new j(paramString, str1, str2));
      paramString = x.a;
      return;
    }
  }
  
  public final void a(String paramString1, long paramLong, String paramString2, String paramString3)
  {
    k.b(paramString1, "installationId");
    k.b(paramString2, "countryIso");
    k.b(paramString3, "normalizedNumber");
    synchronized (c)
    {
      e.a("installationId", paramString1);
      e.b("installationIdTtl", paramLong);
      e.b("installationIdFetchTime", f.a());
      e.a("profileCountryIso", paramString2);
      e.a("profileNumber", paramString3);
      h.a(new j(paramString1, paramString3, paramString2));
      paramString1 = x.a;
      return;
    }
  }
  
  public final boolean a(String paramString1, String paramString2)
  {
    k.b(paramString1, "installationId");
    k.b(paramString2, "context");
    synchronized (c)
    {
      boolean bool = k.a(e.a("installationId"), paramString1);
      if ((bool ^ true)) {
        return false;
      }
      e.d("installationId");
      e.d("installationIdFetchTime");
      e.d("installationIdTtl");
      h.a(paramString1);
      paramString1 = i;
      paramString2 = new e.a("Logout").a("Context", paramString2).a();
      k.a(paramString2, "AnalyticsEvent.Builder(L…CONTEXT, context).build()");
      paramString1.b(paramString2);
      return true;
    }
  }
  
  public final String b()
  {
    j localj = f();
    if (localj != null) {
      return b;
    }
    return null;
  }
  
  public final boolean c()
  {
    return f() != null;
  }
  
  public final String d()
  {
    j localj = f();
    if (localj != null) {
      return a;
    }
    return null;
  }
  
  public final String e()
  {
    synchronized (d)
    {
      Object localObject1 = f();
      if (localObject1 != null)
      {
        localObject1 = a;
        if (localObject1 != null)
        {
          long l1 = e.a("installationIdFetchTime", 0L);
          long l2 = e.a("installationIdTtl", 0L);
          long l3 = f.a();
          if (((l2 + l1 <= l3) || (l1 >= l3)) && (a <= f.b())) {
            localObject1 = a((String)localObject1);
          }
          return (String)localObject1;
        }
      }
      return null;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.s
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */