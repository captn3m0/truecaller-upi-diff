package com.truecaller.common.account.b;

import android.content.Context;
import android.content.SharedPreferences;
import c.g.b.k;

public final class b
  extends com.truecaller.utils.a.a
  implements a
{
  private final int b = 1;
  private final String c = "account";
  
  public b(SharedPreferences paramSharedPreferences)
  {
    super(paramSharedPreferences);
  }
  
  public final int a()
  {
    return b;
  }
  
  public final void a(int paramInt, Context paramContext)
  {
    k.b(paramContext, "context");
  }
  
  public final String b()
  {
    return c;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.b.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */