package com.truecaller.common.account.a;

import android.accounts.Account;
import android.accounts.AccountManager;
import c.a.f;
import c.g.b.k;
import com.truecaller.common.account.j;
import javax.inject.Inject;
import javax.inject.Named;

public final class b
  implements a
{
  private final AccountManager a;
  private final String b;
  
  @Inject
  public b(AccountManager paramAccountManager, @Named("account_type") String paramString)
  {
    a = paramAccountManager;
    b = paramString;
  }
  
  private final Account c()
  {
    Account[] arrayOfAccount = a.getAccountsByType(b);
    k.a(arrayOfAccount, "accountManager.getAccountsByType(accountType)");
    return (Account)f.c(arrayOfAccount);
  }
  
  public final j a()
  {
    Object localObject = c();
    if (localObject == null) {
      return null;
    }
    if (k.a(a.getUserData((Account)localObject, "isMigratedToSettings"), "true")) {
      return null;
    }
    String str1 = a.peekAuthToken((Account)localObject, "installation_id");
    if (str1 == null) {
      return null;
    }
    String str2 = a.getUserData((Account)localObject, "phone_number");
    if (str2 == null) {
      return null;
    }
    localObject = a.getUserData((Account)localObject, "country_code");
    if (localObject == null) {
      return null;
    }
    return new j(str1, str2, (String)localObject);
  }
  
  public final void b()
  {
    Account localAccount = c();
    if (localAccount != null)
    {
      a.setUserData(localAccount, "isMigratedToSettings", "true");
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */