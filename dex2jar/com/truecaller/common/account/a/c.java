package com.truecaller.common.account.a;

import android.accounts.AccountManager;
import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d<b>
{
  private final Provider<AccountManager> a;
  private final Provider<String> b;
  
  private c(Provider<AccountManager> paramProvider, Provider<String> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static c a(Provider<AccountManager> paramProvider, Provider<String> paramProvider1)
  {
    return new c(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */