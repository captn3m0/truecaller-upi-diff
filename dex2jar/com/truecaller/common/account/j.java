package com.truecaller.common.account;

import c.g.b.k;

public final class j
{
  final String a;
  final String b;
  final String c;
  
  public j(String paramString1, String paramString2, String paramString3)
  {
    a = paramString1;
    b = paramString2;
    c = paramString3;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject) {
      if ((paramObject instanceof j))
      {
        paramObject = (j)paramObject;
        if ((k.a(a, a)) && (k.a(b, b)) && (k.a(c, c))) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    String str = a;
    int k = 0;
    int i;
    if (str != null) {
      i = str.hashCode();
    } else {
      i = 0;
    }
    str = b;
    int j;
    if (str != null) {
      j = str.hashCode();
    } else {
      j = 0;
    }
    str = c;
    if (str != null) {
      k = str.hashCode();
    }
    return (i * 31 + j) * 31 + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("AccountState(installationId=");
    localStringBuilder.append(a);
    localStringBuilder.append(", normalizedNumber=");
    localStringBuilder.append(b);
    localStringBuilder.append(", countryIso=");
    localStringBuilder.append(c);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */