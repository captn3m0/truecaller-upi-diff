package com.truecaller.common.account;

import com.truecaller.common.account.b.a;
import dagger.a.d;
import javax.inject.Provider;

public final class m
  implements d<l>
{
  private final Provider<a> a;
  private final Provider<g> b;
  
  private m(Provider<a> paramProvider, Provider<g> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static m a(Provider<a> paramProvider, Provider<g> paramProvider1)
  {
    return new m(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */