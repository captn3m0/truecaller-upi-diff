package com.truecaller.common.account;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.backup.BackupManager;
import c.a.f;
import c.g.b.k;
import com.truecaller.common.g.a;
import java.io.File;
import javax.inject.Inject;
import javax.inject.Named;

public final class p
  implements o
{
  private final String a;
  private final String b;
  private final File c;
  private final AccountManager d;
  private final BackupManager e;
  private final a f;
  
  @Inject
  public p(@Named("account_name") String paramString1, @Named("account_type") String paramString2, @Named("backup_file") File paramFile, AccountManager paramAccountManager, BackupManager paramBackupManager, a parama)
  {
    a = paramString1;
    b = paramString2;
    c = paramFile;
    d = paramAccountManager;
    e = paramBackupManager;
    f = parama;
  }
  
  /* Error */
  private static j a(File paramFile)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 71	java/io/File:exists	()Z
    //   4: ifne +5 -> 9
    //   7: aconst_null
    //   8: areturn
    //   9: new 73	java/io/DataInputStream
    //   12: dup
    //   13: new 75	java/io/FileInputStream
    //   16: dup
    //   17: aload_0
    //   18: invokespecial 78	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   21: checkcast 80	java/io/InputStream
    //   24: invokespecial 83	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
    //   27: checkcast 85	java/io/Closeable
    //   30: astore_3
    //   31: aload_3
    //   32: checkcast 73	java/io/DataInputStream
    //   35: astore 4
    //   37: aload 4
    //   39: invokevirtual 89	java/io/DataInputStream:readInt	()I
    //   42: istore_1
    //   43: iload_1
    //   44: iconst_2
    //   45: if_icmpeq +10 -> 55
    //   48: aload_3
    //   49: aconst_null
    //   50: invokestatic 94	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   53: aconst_null
    //   54: areturn
    //   55: aload 4
    //   57: invokevirtual 98	java/io/DataInputStream:readUTF	()Ljava/lang/String;
    //   60: astore_0
    //   61: aload_0
    //   62: ldc 100
    //   64: invokestatic 102	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   67: aload 4
    //   69: invokevirtual 98	java/io/DataInputStream:readUTF	()Ljava/lang/String;
    //   72: astore_2
    //   73: aload_2
    //   74: ldc 100
    //   76: invokestatic 102	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   79: aload 4
    //   81: invokevirtual 98	java/io/DataInputStream:readUTF	()Ljava/lang/String;
    //   84: astore 4
    //   86: aload 4
    //   88: ldc 100
    //   90: invokestatic 102	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   93: new 104	com/truecaller/common/account/j
    //   96: dup
    //   97: aload_0
    //   98: aload 4
    //   100: aload_2
    //   101: invokespecial 107	com/truecaller/common/account/j:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   104: astore_0
    //   105: aload_3
    //   106: aconst_null
    //   107: invokestatic 94	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   110: aload_0
    //   111: areturn
    //   112: astore_2
    //   113: aconst_null
    //   114: astore_0
    //   115: goto +7 -> 122
    //   118: astore_0
    //   119: aload_0
    //   120: athrow
    //   121: astore_2
    //   122: aload_3
    //   123: aload_0
    //   124: invokestatic 94	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   127: aload_2
    //   128: athrow
    //   129: astore_0
    //   130: aload_0
    //   131: checkcast 65	java/lang/Throwable
    //   134: invokestatic 113	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   137: aconst_null
    //   138: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	139	0	paramFile	File
    //   42	4	1	i	int
    //   72	29	2	str	String
    //   112	1	2	localObject1	Object
    //   121	7	2	localObject2	Object
    //   30	93	3	localCloseable	java.io.Closeable
    //   35	64	4	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   31	43	112	finally
    //   55	105	112	finally
    //   31	43	118	java/lang/Throwable
    //   55	105	118	java/lang/Throwable
    //   119	121	121	finally
    //   9	31	129	java/io/IOException
    //   48	53	129	java/io/IOException
    //   105	110	129	java/io/IOException
    //   122	129	129	java/io/IOException
  }
  
  private final Account b()
  {
    Account[] arrayOfAccount = d.getAccountsByType(b);
    k.a(arrayOfAccount, "accountManager.getAccountsByType(accountType)");
    return (Account)f.c(arrayOfAccount);
  }
  
  public final j a()
  {
    Object localObject2 = b();
    if (localObject2 != null)
    {
      localObject1 = d.peekAuthToken((Account)localObject2, "installation_id_backup");
      String str = d.getUserData((Account)localObject2, "normalized_number_backup");
      localObject2 = d.getUserData((Account)localObject2, "country_code_backup");
      if ((localObject1 != null) && (str != null) && (localObject2 != null)) {
        localObject1 = new j((String)localObject1, str, (String)localObject2);
      } else {
        localObject1 = null;
      }
      if (localObject1 != null)
      {
        f.a("accountRestorationSource", "restored_from_account_manager");
        return (j)localObject1;
      }
    }
    Object localObject1 = a(c);
    if (localObject1 != null)
    {
      if (f.b("accountFileWasRestoredByAutobackup"))
      {
        f.a("accountRestorationSource", "restored_from_autobackup");
        f.d("accountFileWasRestoredByAutobackup");
        return (j)localObject1;
      }
      f.a("accountRestorationSource", "restored_from_file");
      return (j)localObject1;
    }
    return null;
  }
  
  /* Error */
  public final void a(j paramj)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc -85
    //   3: invokestatic 32	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_0
    //   7: invokespecial 132	com/truecaller/common/account/p:b	()Landroid/accounts/Account;
    //   10: astore 4
    //   12: aconst_null
    //   13: astore_3
    //   14: aload 4
    //   16: astore_2
    //   17: aload 4
    //   19: ifnonnull +38 -> 57
    //   22: aload 4
    //   24: astore_2
    //   25: aload_0
    //   26: getfield 53	com/truecaller/common/account/p:d	Landroid/accounts/AccountManager;
    //   29: new 129	android/accounts/Account
    //   32: dup
    //   33: aload_0
    //   34: getfield 47	com/truecaller/common/account/p:a	Ljava/lang/String;
    //   37: aload_0
    //   38: getfield 49	com/truecaller/common/account/p:b	Ljava/lang/String;
    //   41: invokespecial 173	android/accounts/Account:<init>	(Ljava/lang/String;Ljava/lang/String;)V
    //   44: aconst_null
    //   45: aconst_null
    //   46: invokevirtual 177	android/accounts/AccountManager:addAccountExplicitly	(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z
    //   49: ifeq +8 -> 57
    //   52: aload_0
    //   53: invokespecial 132	com/truecaller/common/account/p:b	()Landroid/accounts/Account;
    //   56: astore_2
    //   57: aload_2
    //   58: ifnull +45 -> 103
    //   61: aload_0
    //   62: getfield 53	com/truecaller/common/account/p:d	Landroid/accounts/AccountManager;
    //   65: aload_2
    //   66: ldc -122
    //   68: aload_1
    //   69: getfield 178	com/truecaller/common/account/j:a	Ljava/lang/String;
    //   72: invokevirtual 182	android/accounts/AccountManager:setAuthToken	(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    //   75: aload_0
    //   76: getfield 53	com/truecaller/common/account/p:d	Landroid/accounts/AccountManager;
    //   79: aload_2
    //   80: ldc -116
    //   82: aload_1
    //   83: getfield 183	com/truecaller/common/account/j:b	Ljava/lang/String;
    //   86: invokevirtual 186	android/accounts/AccountManager:setUserData	(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    //   89: aload_0
    //   90: getfield 53	com/truecaller/common/account/p:d	Landroid/accounts/AccountManager;
    //   93: aload_2
    //   94: ldc -111
    //   96: aload_1
    //   97: getfield 188	com/truecaller/common/account/j:c	Ljava/lang/String;
    //   100: invokevirtual 186	android/accounts/AccountManager:setUserData	(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    //   103: aload_0
    //   104: getfield 51	com/truecaller/common/account/p:c	Ljava/io/File;
    //   107: astore_2
    //   108: new 190	java/io/DataOutputStream
    //   111: dup
    //   112: new 192	java/io/FileOutputStream
    //   115: dup
    //   116: aload_2
    //   117: invokespecial 193	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   120: checkcast 195	java/io/OutputStream
    //   123: invokespecial 198	java/io/DataOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   126: checkcast 85	java/io/Closeable
    //   129: astore 4
    //   131: aload_3
    //   132: astore_2
    //   133: aload 4
    //   135: checkcast 190	java/io/DataOutputStream
    //   138: astore 5
    //   140: aload_3
    //   141: astore_2
    //   142: aload 5
    //   144: iconst_2
    //   145: invokevirtual 202	java/io/DataOutputStream:writeInt	(I)V
    //   148: aload_3
    //   149: astore_2
    //   150: aload 5
    //   152: aload_1
    //   153: getfield 178	com/truecaller/common/account/j:a	Ljava/lang/String;
    //   156: invokevirtual 205	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   159: aload_3
    //   160: astore_2
    //   161: aload 5
    //   163: aload_1
    //   164: getfield 188	com/truecaller/common/account/j:c	Ljava/lang/String;
    //   167: invokevirtual 205	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   170: aload_3
    //   171: astore_2
    //   172: aload 5
    //   174: aload_1
    //   175: getfield 183	com/truecaller/common/account/j:b	Ljava/lang/String;
    //   178: invokevirtual 205	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   181: aload_3
    //   182: astore_2
    //   183: getstatic 210	c/x:a	Lc/x;
    //   186: astore_1
    //   187: aload 4
    //   189: aconst_null
    //   190: invokestatic 94	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   193: goto +28 -> 221
    //   196: astore_1
    //   197: goto +8 -> 205
    //   200: astore_1
    //   201: aload_1
    //   202: astore_2
    //   203: aload_1
    //   204: athrow
    //   205: aload 4
    //   207: aload_2
    //   208: invokestatic 94	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   211: aload_1
    //   212: athrow
    //   213: astore_1
    //   214: aload_1
    //   215: checkcast 65	java/lang/Throwable
    //   218: invokestatic 113	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   221: aload_0
    //   222: getfield 55	com/truecaller/common/account/p:e	Landroid/app/backup/BackupManager;
    //   225: invokevirtual 215	android/app/backup/BackupManager:dataChanged	()V
    //   228: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	229	0	this	p
    //   0	229	1	paramj	j
    //   16	192	2	localObject1	Object
    //   13	169	3	localObject2	Object
    //   10	196	4	localObject3	Object
    //   138	35	5	localDataOutputStream	java.io.DataOutputStream
    // Exception table:
    //   from	to	target	type
    //   133	140	196	finally
    //   142	148	196	finally
    //   150	159	196	finally
    //   161	170	196	finally
    //   172	181	196	finally
    //   183	187	196	finally
    //   203	205	196	finally
    //   133	140	200	java/lang/Throwable
    //   142	148	200	java/lang/Throwable
    //   150	159	200	java/lang/Throwable
    //   161	170	200	java/lang/Throwable
    //   172	181	200	java/lang/Throwable
    //   183	187	200	java/lang/Throwable
    //   108	131	213	java/io/IOException
    //   187	193	213	java/io/IOException
    //   205	213	213	java/io/IOException
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "installationId");
    d.invalidateAuthToken(b, paramString);
    c.delete();
    e.dataChanged();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */