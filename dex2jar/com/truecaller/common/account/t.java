package com.truecaller.common.account;

import com.truecaller.analytics.b;
import com.truecaller.common.network.c;
import dagger.a.d;
import javax.inject.Provider;

public final class t
  implements d<s>
{
  private final Provider<com.truecaller.common.g.a> a;
  private final Provider<com.truecaller.utils.a> b;
  private final Provider<c> c;
  private final Provider<o> d;
  private final Provider<b> e;
  private final Provider<com.truecaller.common.account.a.a> f;
  private final Provider<g> g;
  
  private t(Provider<com.truecaller.common.g.a> paramProvider, Provider<com.truecaller.utils.a> paramProvider1, Provider<c> paramProvider2, Provider<o> paramProvider3, Provider<b> paramProvider4, Provider<com.truecaller.common.account.a.a> paramProvider5, Provider<g> paramProvider6)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
  }
  
  public static t a(Provider<com.truecaller.common.g.a> paramProvider, Provider<com.truecaller.utils.a> paramProvider1, Provider<c> paramProvider2, Provider<o> paramProvider3, Provider<b> paramProvider4, Provider<com.truecaller.common.account.a.a> paramProvider5, Provider<g> paramProvider6)
  {
    return new t(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.t
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */