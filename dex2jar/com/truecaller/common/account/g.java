package com.truecaller.common.account;

import com.truecaller.common.network.account.ExchangeCredentialsResponseDto;
import com.truecaller.common.network.account.TemporaryTokenDto;
import e.r;
import java.io.IOException;

public abstract interface g
{
  public abstract r<TemporaryTokenDto> a()
    throws IOException;
  
  public abstract r<ExchangeCredentialsResponseDto> a(String paramString)
    throws IOException;
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */