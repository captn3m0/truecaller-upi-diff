package com.truecaller.common.account;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class TruecallerAccountAuthenticatorService
  extends Service
{
  private n a;
  
  public IBinder onBind(Intent paramIntent)
  {
    if (paramIntent == null) {
      return null;
    }
    if ("android.accounts.AccountAuthenticator".equals(paramIntent.getAction())) {
      return a.getIBinder();
    }
    return null;
  }
  
  public void onCreate()
  {
    super.onCreate();
    a = new n(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.TruecallerAccountAuthenticatorService
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */