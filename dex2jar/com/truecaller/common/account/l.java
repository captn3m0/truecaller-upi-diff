package com.truecaller.common.account;

import com.truecaller.common.account.b.a;
import javax.inject.Inject;

public final class l
  implements k
{
  private final a a;
  private final g b;
  
  @Inject
  public l(a parama, g paramg)
  {
    a = parama;
    b = paramg;
  }
  
  public final String a()
  {
    try
    {
      String str2 = a.a("auth_token_cross_domain");
      String str1 = str2;
      if (str2 == null) {
        str1 = b();
      }
      return str1;
    }
    finally {}
  }
  
  /* Error */
  public final String b()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aconst_null
    //   3: astore_3
    //   4: aload_0
    //   5: getfield 29	com/truecaller/common/account/l:b	Lcom/truecaller/common/account/g;
    //   8: invokeinterface 48 1 0
    //   13: astore_1
    //   14: aload_1
    //   15: invokevirtual 54	e/r:d	()Z
    //   18: ifne +91 -> 109
    //   21: aload_1
    //   22: invokevirtual 58	e/r:f	()Lokhttp3/ae;
    //   25: astore 4
    //   27: aload_3
    //   28: astore_2
    //   29: aload 4
    //   31: ifnull +218 -> 249
    //   34: aload 4
    //   36: invokevirtual 63	okhttp3/ae:f	()Ljava/io/Reader;
    //   39: astore 4
    //   41: aload_3
    //   42: astore_2
    //   43: aload 4
    //   45: ifnull +204 -> 249
    //   48: new 65	com/google/gson/f
    //   51: dup
    //   52: invokespecial 66	com/google/gson/f:<init>	()V
    //   55: aload 4
    //   57: ldc 68
    //   59: invokevirtual 71	com/google/gson/f:a	(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;
    //   62: checkcast 68	com/truecaller/common/network/account/TokenErrorResponseDto
    //   65: astore 4
    //   67: aload_3
    //   68: astore_2
    //   69: aload 4
    //   71: ifnull +178 -> 249
    //   74: new 73	com/truecaller/log/UnmutedException$a
    //   77: dup
    //   78: aload_1
    //   79: invokevirtual 76	e/r:b	()I
    //   82: aload 4
    //   84: invokevirtual 79	com/truecaller/common/network/account/TokenErrorResponseDto:getStatus	()I
    //   87: invokestatic 85	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   90: aload 4
    //   92: invokevirtual 88	com/truecaller/common/network/account/TokenErrorResponseDto:getMessage	()Ljava/lang/String;
    //   95: invokespecial 91	com/truecaller/log/UnmutedException$a:<init>	(ILjava/lang/Integer;Ljava/lang/String;)V
    //   98: checkcast 93	java/lang/Throwable
    //   101: invokestatic 98	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   104: aload_3
    //   105: astore_2
    //   106: goto +143 -> 249
    //   109: aload_1
    //   110: invokevirtual 102	e/r:e	()Ljava/lang/Object;
    //   113: checkcast 104	com/truecaller/common/network/account/TemporaryTokenDto
    //   116: astore_1
    //   117: aload_3
    //   118: astore_2
    //   119: aload_1
    //   120: ifnull +129 -> 249
    //   123: aload_1
    //   124: invokevirtual 107	com/truecaller/common/network/account/TemporaryTokenDto:getToken	()Ljava/lang/String;
    //   127: astore_1
    //   128: aload_3
    //   129: astore_2
    //   130: aload_1
    //   131: ifnull +118 -> 249
    //   134: aload_1
    //   135: checkcast 109	java/lang/CharSequence
    //   138: invokestatic 114	c/n/m:a	(Ljava/lang/CharSequence;)Z
    //   141: iconst_1
    //   142: ixor
    //   143: ifeq +114 -> 257
    //   146: goto +3 -> 149
    //   149: aload_3
    //   150: astore_2
    //   151: aload_1
    //   152: ifnull +97 -> 249
    //   155: aload_0
    //   156: getfield 27	com/truecaller/common/account/l:a	Lcom/truecaller/common/account/b/a;
    //   159: ldc 34
    //   161: aload_1
    //   162: invokeinterface 117 3 0
    //   167: aload_1
    //   168: astore_2
    //   169: goto +80 -> 249
    //   172: astore_1
    //   173: goto +80 -> 253
    //   176: astore_1
    //   177: aload_1
    //   178: instanceof 119
    //   181: ifne +30 -> 211
    //   184: aload_1
    //   185: instanceof 121
    //   188: ifeq +6 -> 194
    //   191: goto +20 -> 211
    //   194: aload_1
    //   195: instanceof 123
    //   198: ifeq +8 -> 206
    //   201: aload_3
    //   202: astore_2
    //   203: goto +46 -> 249
    //   206: aload_1
    //   207: checkcast 93	java/lang/Throwable
    //   210: athrow
    //   211: new 125	java/lang/StringBuilder
    //   214: dup
    //   215: ldc 127
    //   217: invokespecial 130	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   220: astore_2
    //   221: aload_2
    //   222: aload_1
    //   223: invokevirtual 131	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   226: invokevirtual 135	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   229: pop
    //   230: new 73	com/truecaller/log/UnmutedException$a
    //   233: dup
    //   234: aload_2
    //   235: invokevirtual 138	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   238: invokespecial 139	com/truecaller/log/UnmutedException$a:<init>	(Ljava/lang/String;)V
    //   241: checkcast 93	java/lang/Throwable
    //   244: invokestatic 98	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   247: aload_3
    //   248: astore_2
    //   249: aload_0
    //   250: monitorexit
    //   251: aload_2
    //   252: areturn
    //   253: aload_0
    //   254: monitorexit
    //   255: aload_1
    //   256: athrow
    //   257: aconst_null
    //   258: astore_1
    //   259: goto -110 -> 149
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	262	0	this	l
    //   13	155	1	localObject1	Object
    //   172	1	1	localObject2	Object
    //   176	80	1	localException	Exception
    //   258	1	1	localObject3	Object
    //   28	224	2	localObject4	Object
    //   3	245	3	localObject5	Object
    //   25	66	4	localObject6	Object
    // Exception table:
    //   from	to	target	type
    //   4	27	172	finally
    //   34	41	172	finally
    //   48	67	172	finally
    //   74	104	172	finally
    //   109	117	172	finally
    //   123	128	172	finally
    //   134	146	172	finally
    //   155	167	172	finally
    //   177	191	172	finally
    //   194	201	172	finally
    //   206	211	172	finally
    //   211	247	172	finally
    //   4	27	176	java/lang/Exception
    //   34	41	176	java/lang/Exception
    //   48	67	176	java/lang/Exception
    //   74	104	176	java/lang/Exception
    //   109	117	176	java/lang/Exception
    //   123	128	176	java/lang/Exception
    //   134	146	176	java/lang/Exception
    //   155	167	176	java/lang/Exception
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */