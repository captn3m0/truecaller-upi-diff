package com.truecaller.common.account;

import android.content.Context;
import com.truecaller.common.account.b.a;
import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d<a>
{
  private final Provider<Context> a;
  
  private c(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static c a(Provider<Context> paramProvider)
  {
    return new c(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */