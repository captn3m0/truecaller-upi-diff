package com.truecaller.common.enhancedsearch;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import androidx.work.c.a;
import androidx.work.e;
import androidx.work.e.a;
import androidx.work.g;
import androidx.work.j;
import androidx.work.k.a;
import androidx.work.p;
import c.n.m;
import com.truecaller.log.d;
import java.io.IOException;
import javax.inject.Inject;

public final class EnhancedSearchStateWorker
  extends Worker
{
  public static final a d = new a((byte)0);
  @Inject
  public com.truecaller.common.account.r b;
  @Inject
  public com.truecaller.common.g.a c;
  private final Context e;
  
  public EnhancedSearchStateWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    e = paramContext;
    paramContext = com.truecaller.common.b.a.F();
    c.g.b.k.a(paramContext, "ApplicationBase.getAppBase()");
    paramContext.u().a(this);
  }
  
  public static final void a(boolean paramBoolean)
  {
    a.a(paramBoolean);
  }
  
  public final ListenableWorker.a a()
  {
    Object localObject = b;
    if (localObject == null) {
      c.g.b.k.a("accountManager");
    }
    if (!((com.truecaller.common.account.r)localObject).c())
    {
      localObject = ListenableWorker.a.a();
      c.g.b.k.a(localObject, "Result.success()");
      return (ListenableWorker.a)localObject;
    }
    for (boolean bool = getInputData().a("enhanced_search_value");; bool = false)
    {
      try
      {
        localObject = com.truecaller.common.network.c.a.a(bool).c();
        c.g.b.k.a(localObject, "response");
        if (((e.r)localObject).d())
        {
          localObject = (com.truecaller.common.network.g.a)((e.r)localObject).e();
          if (localObject == null) {
            continue;
          }
          localObject = a;
          if (localObject == null) {
            continue;
          }
          bool = m.a(a, "ENABLED", true);
          localObject = c;
          if (localObject == null) {
            c.g.b.k.a("coreSettings");
          }
          ((com.truecaller.common.g.a)localObject).b("backup", bool);
          localObject = c;
          if (localObject == null) {
            c.g.b.k.a("coreSettings");
          }
          ((com.truecaller.common.g.a)localObject).b("core_enhancedSearchReported", true);
          localObject = ListenableWorker.a.a();
          c.g.b.k.a(localObject, "Result.success()");
          return (ListenableWorker.a)localObject;
        }
      }
      catch (RuntimeException localRuntimeException)
      {
        d.a((Throwable)localRuntimeException);
      }
      catch (IOException localIOException)
      {
        d.a((Throwable)localIOException);
      }
      ListenableWorker.a locala = ListenableWorker.a.b();
      c.g.b.k.a(locala, "Result.retry()");
      return locala;
    }
  }
  
  public static final class a
  {
    public static void a(boolean paramBoolean)
    {
      p.a().a("EnhancedSearchStateWorker", g.a, (androidx.work.k)((k.a)((k.a)new k.a(EnhancedSearchStateWorker.class).a(new c.a().a(j.b).a())).a(new e.a().a("enhanced_search_value", paramBoolean).a())).c());
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.enhancedsearch.EnhancedSearchStateWorker
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */