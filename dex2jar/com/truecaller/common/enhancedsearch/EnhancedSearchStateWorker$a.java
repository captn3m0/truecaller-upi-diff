package com.truecaller.common.enhancedsearch;

import androidx.work.c.a;
import androidx.work.e.a;
import androidx.work.g;
import androidx.work.j;
import androidx.work.k;
import androidx.work.k.a;
import androidx.work.p;

public final class EnhancedSearchStateWorker$a
{
  public static void a(boolean paramBoolean)
  {
    p.a().a("EnhancedSearchStateWorker", g.a, (k)((k.a)((k.a)new k.a(EnhancedSearchStateWorker.class).a(new c.a().a(j.b).a())).a(new e.a().a("enhanced_search_value", paramBoolean).a())).c());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.enhancedsearch.EnhancedSearchStateWorker.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */