package com.truecaller.common.background;

import android.annotation.TargetApi;
import android.app.job.JobInfo;
import android.app.job.JobInfo.Builder;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.PersistableBundle;
import com.truecaller.common.b.a;
import com.truecaller.log.AssertionUtil;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

@TargetApi(21)
public final class NativeScheduler
  extends JobService
  implements b.a<JobParameters>
{
  private JobScheduler a;
  
  public final void onCreate()
  {
    super.onCreate();
    a = ((JobScheduler)getSystemService("jobscheduler"));
  }
  
  public final boolean onStartJob(JobParameters paramJobParameters)
  {
    PersistableBundle localPersistableBundle = paramJobParameters.getExtras();
    try
    {
      int i = paramJobParameters.getJobId();
      int j = Build.VERSION.SDK_INT;
      Object localObject2 = null;
      if (j >= 24)
      {
        localObject1 = a.getPendingJob(i);
      }
      else
      {
        localObject1 = a.getAllPendingJobs();
        if (localObject1 != null)
        {
          localObject3 = ((List)localObject1).iterator();
          while (((Iterator)localObject3).hasNext())
          {
            localObject1 = (JobInfo)((Iterator)localObject3).next();
            j = ((JobInfo)localObject1).getId();
            if (i == j) {
              break label99;
            }
          }
        }
        localObject1 = null;
      }
      label99:
      if (localPersistableBundle.containsKey("tag"))
      {
        if ((localObject1 != null) && (((JobInfo)localObject1).isPeriodic())) {
          a.cancel(paramJobParameters.getJobId());
        }
        return false;
      }
      Object localObject3 = (a)getApplication();
      Object localObject1 = localObject2;
      if (localPersistableBundle.containsKey("params")) {
        localObject1 = new Bundle(localPersistableBundle.getPersistableBundle("params"));
      }
      c.a(paramJobParameters.getJobId(), (Bundle)localObject1, this, paramJobParameters);
      return true;
    }
    catch (RuntimeException localRuntimeException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localRuntimeException);
      jobFinished(paramJobParameters, true);
    }
    return false;
  }
  
  public final boolean onStopJob(JobParameters paramJobParameters)
  {
    return false;
  }
  
  static final class a
    implements c
  {
    private final Context a;
    private final JobScheduler b;
    
    a(Context paramContext)
    {
      a = paramContext.getApplicationContext();
      b = ((JobScheduler)paramContext.getSystemService("jobscheduler"));
    }
    
    private static JobInfo.Builder a(PersistentBackgroundTask paramPersistentBackgroundTask, ComponentName paramComponentName, int paramInt)
    {
      paramPersistentBackgroundTask = paramPersistentBackgroundTask.b();
      PersistableBundle localPersistableBundle1 = new PersistableBundle(1);
      PersistableBundle localPersistableBundle2 = new PersistableBundle();
      paramPersistentBackgroundTask.a(localPersistableBundle2);
      if (!localPersistableBundle2.isEmpty()) {
        localPersistableBundle1.putPersistableBundle("params", localPersistableBundle2);
      }
      paramComponentName = new JobInfo.Builder(paramInt, paramComponentName).setRequiresCharging(e).setExtras(localPersistableBundle1).setPersisted(true);
      if (a != 0)
      {
        paramComponentName.setPeriodic(paramPersistentBackgroundTask.a(TimeUnit.MILLISECONDS));
      }
      else
      {
        paramComponentName.setMinimumLatency(paramPersistentBackgroundTask.c(TimeUnit.MILLISECONDS));
        paramComponentName.setOverrideDeadline(paramPersistentBackgroundTask.d(TimeUnit.MILLISECONDS));
      }
      switch (b)
      {
      default: 
        paramInt = 0;
        break;
      case 2: 
        paramInt = 2;
        break;
      case 1: 
        paramInt = 1;
      }
      paramComponentName.setRequiredNetworkType(paramInt);
      paramComponentName.setBackoffCriteria(f, 1);
      return paramComponentName;
    }
    
    private JobInfo a(int paramInt)
    {
      Iterator localIterator = b.getAllPendingJobs().iterator();
      while (localIterator.hasNext())
      {
        JobInfo localJobInfo = (JobInfo)localIterator.next();
        if (localJobInfo.getId() == paramInt) {
          return localJobInfo;
        }
      }
      return null;
    }
    
    public final void a(List<PersistentBackgroundTask> paramList)
    {
      ComponentName localComponentName = new ComponentName(a.getPackageName(), NativeScheduler.class.getName());
      paramList = paramList.iterator();
      while (paramList.hasNext())
      {
        Object localObject = (PersistentBackgroundTask)paramList.next();
        JobInfo localJobInfo = a(((PersistentBackgroundTask)localObject).a());
        if (localJobInfo != null)
        {
          localObject = a((PersistentBackgroundTask)localObject, localComponentName, localJobInfo.getId()).build();
          int i;
          if ((((JobInfo)localObject).getId() == localJobInfo.getId()) && (((JobInfo)localObject).isRequireCharging() == localJobInfo.isRequireCharging()) && (((JobInfo)localObject).isRequireDeviceIdle() == localJobInfo.isRequireDeviceIdle()) && (((JobInfo)localObject).getNetworkType() == localJobInfo.getNetworkType()) && (((JobInfo)localObject).getMinLatencyMillis() == localJobInfo.getMinLatencyMillis()) && (((JobInfo)localObject).getMaxExecutionDelayMillis() == localJobInfo.getMaxExecutionDelayMillis()) && (((JobInfo)localObject).isPeriodic() == localJobInfo.isPeriodic()) && (((JobInfo)localObject).isPersisted() == localJobInfo.isPersisted()) && (((JobInfo)localObject).getIntervalMillis() == localJobInfo.getIntervalMillis()) && (((JobInfo)localObject).getInitialBackoffMillis() == localJobInfo.getInitialBackoffMillis()) && (((JobInfo)localObject).getBackoffPolicy() == localJobInfo.getBackoffPolicy())) {
            i = 1;
          } else {
            i = 0;
          }
          if (i == 0) {
            b.schedule((JobInfo)localObject);
          }
        }
        else
        {
          localJobInfo = a((PersistentBackgroundTask)localObject, localComponentName, ((PersistentBackgroundTask)localObject).a()).build();
          b.schedule(localJobInfo);
        }
      }
    }
    
    public final boolean a()
    {
      return false;
    }
    
    public final boolean a(e parame)
    {
      return true;
    }
    
    public final void b(List<PersistentBackgroundTask> paramList)
    {
      paramList = paramList.iterator();
      while (paramList.hasNext())
      {
        PersistentBackgroundTask localPersistentBackgroundTask = (PersistentBackgroundTask)paramList.next();
        b.cancel(localPersistentBackgroundTask.a());
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.NativeScheduler
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */