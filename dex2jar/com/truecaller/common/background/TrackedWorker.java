package com.truecaller.common.background;

import android.content.Context;
import android.os.SystemClock;
import androidx.work.ListenableWorker.a;
import androidx.work.ListenableWorker.a.a;
import androidx.work.ListenableWorker.a.b;
import androidx.work.ListenableWorker.a.c;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.debug.log.a;

public abstract class TrackedWorker
  extends Worker
{
  public TrackedWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
  }
  
  private static String a(ListenableWorker.a parama)
  {
    if ((parama instanceof ListenableWorker.a.c)) {
      return "Success";
    }
    if ((parama instanceof ListenableWorker.a.b)) {
      return "Retry";
    }
    if ((parama instanceof ListenableWorker.a.a)) {
      return "Failure";
    }
    return "Unknown";
  }
  
  private static void a(String paramString)
  {
    a.a(new Object[] { paramString });
  }
  
  public final ListenableWorker.a a()
  {
    Object localObject1 = getClass().getSimpleName();
    k.a(localObject1, "javaClass.simpleName");
    if (!c())
    {
      localObject2 = new StringBuilder("Worker ");
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append(" was not run");
      a(((StringBuilder)localObject2).toString());
      localObject1 = ListenableWorker.a.a();
      k.a(localObject1, "Result.success()");
      return (ListenableWorker.a)localObject1;
    }
    long l = SystemClock.elapsedRealtime();
    Object localObject2 = d();
    l = SystemClock.elapsedRealtime() - l;
    Object localObject3 = new StringBuilder("Worker ");
    ((StringBuilder)localObject3).append((String)localObject1);
    ((StringBuilder)localObject3).append(" finished with result ");
    ((StringBuilder)localObject3).append(a((ListenableWorker.a)localObject2));
    ((StringBuilder)localObject3).append(" after ");
    ((StringBuilder)localObject3).append(l);
    ((StringBuilder)localObject3).append(" ms");
    a(((StringBuilder)localObject3).toString());
    localObject3 = b();
    localObject1 = new e.a("BackgroundWork").a("WorkName", (String)localObject1).a("Result", a((ListenableWorker.a)localObject2)).a(Double.valueOf(l)).a();
    k.a(localObject1, "AnalyticsEvent.Builder(B…\n                .build()");
    ((b)localObject3).b((e)localObject1);
    return (ListenableWorker.a)localObject2;
  }
  
  public abstract b b();
  
  public abstract boolean c();
  
  public abstract ListenableWorker.a d();
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.TrackedWorker
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */