package com.truecaller.common.background;

import com.truecaller.log.AssertionUtil.OnlyInDebug;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public final class e$a
{
  final int a;
  public int b = 0;
  public boolean c = false;
  long d = 0L;
  long e = 0L;
  long f = 0L;
  long g = 0L;
  long h = TimeUnit.MINUTES.toMillis(5L);
  Map<String, Object> i;
  
  public e$a(int paramInt)
  {
    a = paramInt;
  }
  
  public final a a()
  {
    c = false;
    return this;
  }
  
  public final a a(int paramInt)
  {
    b = paramInt;
    return this;
  }
  
  public final a a(long paramLong, TimeUnit paramTimeUnit)
  {
    int j = a;
    boolean bool = true;
    if (j != 1) {
      bool = false;
    }
    AssertionUtil.OnlyInDebug.isTrue(bool, new String[] { "Only periodic tasks can have period" });
    d = paramTimeUnit.toMillis(paramLong);
    return this;
  }
  
  public final a a(String paramString1, String paramString2)
  {
    AssertionUtil.OnlyInDebug.isTrue(true, new String[] { "Can not save null as string parameter" });
    if (i == null) {
      i = new HashMap();
    }
    i.put(paramString1, paramString2);
    return this;
  }
  
  public final a a(TimeUnit paramTimeUnit)
  {
    boolean bool;
    if (a == 0) {
      bool = true;
    } else {
      bool = false;
    }
    AssertionUtil.OnlyInDebug.isTrue(bool, new String[] { "Only one off tasks can have start deadline" });
    g = paramTimeUnit.toMillis(5L);
    return this;
  }
  
  public final a b(long paramLong, TimeUnit paramTimeUnit)
  {
    int j = a;
    boolean bool = true;
    if (j != 1) {
      bool = false;
    }
    AssertionUtil.OnlyInDebug.isTrue(bool, new String[] { "Only periodic tasks can have flexibility" });
    e = paramTimeUnit.toMillis(paramLong);
    return this;
  }
  
  public final e b()
  {
    int j = a;
    boolean bool = true;
    if (j == 1)
    {
      if (d <= 0L) {
        bool = false;
      }
      AssertionUtil.OnlyInDebug.isTrue(bool, new String[] { "Period not specified for periodic tasks" });
    }
    return new e(this, (byte)0);
  }
  
  public final a c(long paramLong, TimeUnit paramTimeUnit)
  {
    h = paramTimeUnit.toMillis(paramLong);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.e.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */