package com.truecaller.common.background;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.OneoffTask.Builder;
import com.google.android.gms.gcm.PeriodicTask.Builder;
import com.google.android.gms.gcm.Task;
import com.google.android.gms.gcm.Task.Builder;
import com.google.android.gms.gcm.TaskParams;
import com.truecaller.common.b.a;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public final class GcmScheduler
  extends GcmTaskService
{
  public final int a(TaskParams paramTaskParams)
  {
    Object localObject = new StringBuilder("onRunTask() taskParams: ");
    ((StringBuilder)localObject).append(org.c.a.a.a.a.b.a(paramTaskParams));
    ((StringBuilder)localObject).toString();
    AssertionUtil.OnlyInDebug.notOnMainThread(new String[0]);
    localObject = a;
    try
    {
      int i = Integer.parseInt((String)localObject);
      paramTaskParams = getApplicationc.a(i, b);
      switch (1.a[paramTaskParams.ordinal()])
      {
      default: 
        switch (1.a[paramTaskParams.ordinal()])
        {
        default: 
          AssertionUtil.isTrue(false, new String[0]);
          return 2;
        }
        break;
      }
      GcmNetworkManager.a(this).a((String)localObject, GcmScheduler.class);
      return 2;
      return 2;
      return 1;
      return 0;
    }
    catch (NumberFormatException paramTaskParams) {}
    return 2;
  }
  
  public final void a()
  {
    super.a();
    getApplicationc.a();
  }
  
  static final class a
    implements c
  {
    private final SharedPreferences a;
    private final Context b;
    
    a(Context paramContext)
    {
      b = paramContext.getApplicationContext();
      a = b.getSharedPreferences("gcm-scheduler-engine-initial", 0);
    }
    
    public final void a(List<PersistentBackgroundTask> paramList)
    {
      GcmNetworkManager localGcmNetworkManager = GcmNetworkManager.a(b);
      Iterator localIterator = paramList.iterator();
      while (localIterator.hasNext())
      {
        PersistentBackgroundTask localPersistentBackgroundTask = (PersistentBackgroundTask)localIterator.next();
        e locale = localPersistentBackgroundTask.b();
        int j = localPersistentBackgroundTask.a();
        if (a != 0)
        {
          paramList = new PeriodicTask.Builder();
          a = locale.a(TimeUnit.SECONDS);
          b = locale.b(TimeUnit.SECONDS);
        }
        else
        {
          paramList = new OneoffTask.Builder();
          long l1 = locale.c(TimeUnit.SECONDS);
          long l2 = locale.d(TimeUnit.SECONDS);
          a = l1;
          b = l2;
        }
        int i;
        switch (b)
        {
        default: 
          i = 2;
          break;
        case 2: 
          i = 1;
          break;
        case 1: 
          i = 0;
        }
        paramList.a(i);
        paramList.a(e);
        paramList.a(String.valueOf(j)).a(GcmScheduler.class).c();
        Bundle localBundle = new Bundle();
        locale.a(localBundle);
        if (!localBundle.isEmpty()) {
          paramList.a(localBundle);
        }
        paramList = paramList.b();
        localGcmNetworkManager.a(d, GcmScheduler.class);
        localGcmNetworkManager.a(paramList);
        paramList = String.valueOf(localPersistentBackgroundTask.a());
        if ((a == 1) && (!a.getBoolean(paramList, false)))
        {
          a.edit().putBoolean(paramList, true).apply();
          b).c.b(localPersistentBackgroundTask.a());
        }
      }
    }
    
    public final boolean a()
    {
      return false;
    }
    
    public final boolean a(e parame)
    {
      return true;
    }
    
    public final void b(List<PersistentBackgroundTask> paramList)
    {
      GcmNetworkManager localGcmNetworkManager = GcmNetworkManager.a(b);
      paramList = paramList.iterator();
      while (paramList.hasNext())
      {
        String str = String.valueOf(((PersistentBackgroundTask)paramList.next()).a());
        localGcmNetworkManager.a(str, GcmScheduler.class);
        a.edit().putBoolean(str, false).apply();
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.GcmScheduler
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */