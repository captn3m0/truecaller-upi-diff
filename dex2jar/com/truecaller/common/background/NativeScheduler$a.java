package com.truecaller.common.background;

import android.app.job.JobInfo;
import android.app.job.JobInfo.Builder;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.PersistableBundle;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

final class NativeScheduler$a
  implements c
{
  private final Context a;
  private final JobScheduler b;
  
  NativeScheduler$a(Context paramContext)
  {
    a = paramContext.getApplicationContext();
    b = ((JobScheduler)paramContext.getSystemService("jobscheduler"));
  }
  
  private static JobInfo.Builder a(PersistentBackgroundTask paramPersistentBackgroundTask, ComponentName paramComponentName, int paramInt)
  {
    paramPersistentBackgroundTask = paramPersistentBackgroundTask.b();
    PersistableBundle localPersistableBundle1 = new PersistableBundle(1);
    PersistableBundle localPersistableBundle2 = new PersistableBundle();
    paramPersistentBackgroundTask.a(localPersistableBundle2);
    if (!localPersistableBundle2.isEmpty()) {
      localPersistableBundle1.putPersistableBundle("params", localPersistableBundle2);
    }
    paramComponentName = new JobInfo.Builder(paramInt, paramComponentName).setRequiresCharging(e).setExtras(localPersistableBundle1).setPersisted(true);
    if (a != 0)
    {
      paramComponentName.setPeriodic(paramPersistentBackgroundTask.a(TimeUnit.MILLISECONDS));
    }
    else
    {
      paramComponentName.setMinimumLatency(paramPersistentBackgroundTask.c(TimeUnit.MILLISECONDS));
      paramComponentName.setOverrideDeadline(paramPersistentBackgroundTask.d(TimeUnit.MILLISECONDS));
    }
    switch (b)
    {
    default: 
      paramInt = 0;
      break;
    case 2: 
      paramInt = 2;
      break;
    case 1: 
      paramInt = 1;
    }
    paramComponentName.setRequiredNetworkType(paramInt);
    paramComponentName.setBackoffCriteria(f, 1);
    return paramComponentName;
  }
  
  private JobInfo a(int paramInt)
  {
    Iterator localIterator = b.getAllPendingJobs().iterator();
    while (localIterator.hasNext())
    {
      JobInfo localJobInfo = (JobInfo)localIterator.next();
      if (localJobInfo.getId() == paramInt) {
        return localJobInfo;
      }
    }
    return null;
  }
  
  public final void a(List<PersistentBackgroundTask> paramList)
  {
    ComponentName localComponentName = new ComponentName(a.getPackageName(), NativeScheduler.class.getName());
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      Object localObject = (PersistentBackgroundTask)paramList.next();
      JobInfo localJobInfo = a(((PersistentBackgroundTask)localObject).a());
      if (localJobInfo != null)
      {
        localObject = a((PersistentBackgroundTask)localObject, localComponentName, localJobInfo.getId()).build();
        int i;
        if ((((JobInfo)localObject).getId() == localJobInfo.getId()) && (((JobInfo)localObject).isRequireCharging() == localJobInfo.isRequireCharging()) && (((JobInfo)localObject).isRequireDeviceIdle() == localJobInfo.isRequireDeviceIdle()) && (((JobInfo)localObject).getNetworkType() == localJobInfo.getNetworkType()) && (((JobInfo)localObject).getMinLatencyMillis() == localJobInfo.getMinLatencyMillis()) && (((JobInfo)localObject).getMaxExecutionDelayMillis() == localJobInfo.getMaxExecutionDelayMillis()) && (((JobInfo)localObject).isPeriodic() == localJobInfo.isPeriodic()) && (((JobInfo)localObject).isPersisted() == localJobInfo.isPersisted()) && (((JobInfo)localObject).getIntervalMillis() == localJobInfo.getIntervalMillis()) && (((JobInfo)localObject).getInitialBackoffMillis() == localJobInfo.getInitialBackoffMillis()) && (((JobInfo)localObject).getBackoffPolicy() == localJobInfo.getBackoffPolicy())) {
          i = 1;
        } else {
          i = 0;
        }
        if (i == 0) {
          b.schedule((JobInfo)localObject);
        }
      }
      else
      {
        localJobInfo = a((PersistentBackgroundTask)localObject, localComponentName, ((PersistentBackgroundTask)localObject).a()).build();
        b.schedule(localJobInfo);
      }
    }
  }
  
  public final boolean a()
  {
    return false;
  }
  
  public final boolean a(e parame)
  {
    return true;
  }
  
  public final void b(List<PersistentBackgroundTask> paramList)
  {
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      PersistentBackgroundTask localPersistentBackgroundTask = (PersistentBackgroundTask)paramList.next();
      b.cancel(localPersistentBackgroundTask.a());
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.NativeScheduler.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */