package com.truecaller.common.background;

import android.util.SparseArray;
import java.util.ArrayList;

final class d$a$1
  implements Runnable
{
  d$a$1(d.a parama) {}
  
  public final void run()
  {
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    SparseArray localSparseArray = d.a.a(a).a();
    int j = localSparseArray.size();
    int i = 0;
    while (i < j)
    {
      d.a.a(a, (PersistentBackgroundTask)localSparseArray.valueAt(i), localArrayList1, localArrayList2);
      i += 1;
    }
    d.a.a(a, localArrayList1, localArrayList2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.d.a.1
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */