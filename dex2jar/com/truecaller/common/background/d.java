package com.truecaller.common.background;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.util.SparseArray;
import com.google.android.gms.common.GoogleApiAvailability;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.common.h.k;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import javax.inject.Inject;

public final class d
  implements b
{
  final a a;
  @Inject
  public com.truecaller.analytics.b b;
  private final Context c;
  private final f d;
  private final Executor e = Executors.newCachedThreadPool();
  private final c f;
  
  public d(Context paramContext)
  {
    c = paramContext.getApplicationContext();
    d = new f(paramContext);
    if (k.d()) {
      f = new NativeScheduler.a(paramContext);
    } else if (a(paramContext)) {
      f = new GcmScheduler.a(paramContext);
    } else {
      f = new AlarmScheduler.a(paramContext);
    }
    HandlerThread localHandlerThread = new HandlerThread("Scheduler");
    localHandlerThread.start();
    a = new a(localHandlerThread.getLooper(), c, d, f, (byte)0);
    ((com.truecaller.common.b.a)paramContext.getApplicationContext()).u().a(this);
  }
  
  static void a(PersistentBackgroundTask paramPersistentBackgroundTask, String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder("[Task: ");
    localStringBuilder.append(paramPersistentBackgroundTask.getClass().getSimpleName());
    localStringBuilder.append("] ");
    localStringBuilder.append(paramString);
    com.truecaller.debug.log.a.a(new Object[] { localStringBuilder.toString() });
  }
  
  private static boolean a(Context paramContext)
  {
    int k = GoogleApiAvailability.a().a(paramContext);
    int i;
    if (k != 0) {
      i = -1;
    }
    try
    {
      int j = getPackageManagergetPackageInfo"com.google.android.gms"0versionCode;
      i = j;
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      for (;;) {}
    }
    paramContext = new StringBuilder("Old/missing Play Services. isGooglePlayServicesAvailable: ");
    paramContext.append(k);
    paramContext.append(" version: ");
    paramContext.append(i);
    paramContext.toString();
    return false;
    return true;
  }
  
  public final ExecutionResult a(int paramInt)
  {
    return c(paramInt, null);
  }
  
  public final ExecutionResult a(int paramInt, Bundle paramBundle)
  {
    return c(paramInt, paramBundle);
  }
  
  public final void a()
  {
    a locala = a;
    locala.post(a);
  }
  
  public final <T> void a(final int paramInt, final Bundle paramBundle, final b.a<T> parama, final T paramT)
  {
    a(new Runnable()
    {
      public final void run()
      {
        ExecutionResult localExecutionResult = c(paramInt, paramBundle);
        d.a locala = a;
        locala.sendMessage(locala.obtainMessage(1, new d.b(localExecutionResult, parama, paramT, (byte)0)));
      }
    });
  }
  
  public final void a(int paramInt, int... paramVarArgs)
  {
    a(0L, paramInt, paramVarArgs);
  }
  
  public final void a(long paramLong, int paramInt, int... paramVarArgs)
  {
    a.a(paramLong, paramInt, paramVarArgs);
  }
  
  public final void a(Runnable paramRunnable)
  {
    e.execute(paramRunnable);
  }
  
  public final void b()
  {
    PersistentBackgroundTask localPersistentBackgroundTask = (PersistentBackgroundTask)d.a().get(10002);
    if (localPersistentBackgroundTask != null) {
      localPersistentBackgroundTask.d(c);
    }
  }
  
  public final void b(int paramInt)
  {
    b(paramInt, null);
  }
  
  public final void b(final int paramInt, final Bundle paramBundle)
  {
    a(new Runnable()
    {
      public final void run()
      {
        c(paramInt, paramBundle);
      }
    });
  }
  
  final ExecutionResult c(int paramInt, Bundle paramBundle)
  {
    PowerManager.WakeLock localWakeLock = ((PowerManager)c.getSystemService("power")).newWakeLock(1, "Task Scheduler");
    localWakeLock.setReferenceCounted(false);
    localWakeLock.acquire(180000L);
    for (;;)
    {
      try
      {
        PersistentBackgroundTask localPersistentBackgroundTask = (PersistentBackgroundTask)d.a().get(paramInt);
        long l;
        if (localPersistentBackgroundTask == null)
        {
          paramBundle = ExecutionResult.NotFound;
        }
        else if (!localPersistentBackgroundTask.a(c))
        {
          paramBundle = ExecutionResult.Inactive;
          a(localPersistentBackgroundTask, "Task is inactive");
        }
        else if (f.a(localPersistentBackgroundTask.b()))
        {
          l = SystemClock.elapsedRealtime();
          a(localPersistentBackgroundTask, "Execute task");
          paramBundle = localPersistentBackgroundTask.b(c, paramBundle);
        }
        switch (3.a[paramBundle.ordinal()])
        {
        case 3: 
          continue;
          paramBundle = ExecutionResult.Skip;
          break;
        case 2: 
          paramBundle = ExecutionResult.Retry;
          break;
        case 1: 
          paramBundle = ExecutionResult.Success;
          continue;
          Object localObject = new StringBuilder();
          ((StringBuilder)localObject).append(localPersistentBackgroundTask.getClass().getSimpleName());
          ((StringBuilder)localObject).append(": Incorrect result - ");
          ((StringBuilder)localObject).append(paramBundle);
          AssertionUtil.AlwaysFatal.fail(new String[] { ((StringBuilder)localObject).toString() });
          a(localPersistentBackgroundTask, "Incorrect result: ".concat(String.valueOf(paramBundle)));
          paramBundle = ExecutionResult.Inactive;
          double d1 = SystemClock.elapsedRealtime() - l;
          localObject = new e.a("BackgroundTask").a("TaskName", localPersistentBackgroundTask.getClass().getSimpleName()).a("Result", paramBundle.name());
          a = Double.valueOf(d1);
          localObject = ((e.a)localObject).a();
          b.b((e)localObject);
          localObject = new StringBuilder("Execution result: ");
          ((StringBuilder)localObject).append(paramBundle.name());
          a(localPersistentBackgroundTask, ((StringBuilder)localObject).toString());
          continue;
          paramBundle = ExecutionResult.Retry;
          a(localPersistentBackgroundTask, "No conditions to start task. Will retry it later.");
          return paramBundle;
        }
      }
      finally
      {
        localWakeLock.release();
      }
    }
  }
  
  public final void c()
  {
    PersistentBackgroundTask localPersistentBackgroundTask = (PersistentBackgroundTask)d.a().get(10002);
    if (localPersistentBackgroundTask != null) {
      f.b(Collections.singletonList(localPersistentBackgroundTask));
    }
  }
  
  public final void d()
  {
    if (f.a()) {
      a();
    }
  }
  
  public final void e()
  {
    a();
  }
  
  static final class a
    extends Handler
  {
    final Runnable a = new Runnable()
    {
      public final void run()
      {
        ArrayList localArrayList1 = new ArrayList();
        ArrayList localArrayList2 = new ArrayList();
        SparseArray localSparseArray = d.a.a(d.a.this).a();
        int j = localSparseArray.size();
        int i = 0;
        while (i < j)
        {
          d.a.a(d.a.this, (PersistentBackgroundTask)localSparseArray.valueAt(i), localArrayList1, localArrayList2);
          i += 1;
        }
        d.a.a(d.a.this, localArrayList1, localArrayList2);
      }
    };
    private final int b = 0;
    private final int c = 1;
    private final Context d;
    private final c e;
    private final f f;
    
    private a(Looper paramLooper, Context paramContext, f paramf, c paramc)
    {
      super();
      e = paramc;
      f = paramf;
      d = paramContext;
    }
    
    private void a(PersistentBackgroundTask paramPersistentBackgroundTask, List<PersistentBackgroundTask> paramList1, List<PersistentBackgroundTask> paramList2)
    {
      if (paramPersistentBackgroundTask == null) {
        return;
      }
      boolean bool = paramPersistentBackgroundTask.b(d);
      if ((paramPersistentBackgroundTask.a(d)) && (bool))
      {
        paramList1.add(paramPersistentBackgroundTask);
        d.a(paramPersistentBackgroundTask, "was scheduled for launch");
        return;
      }
      paramList2.add(paramPersistentBackgroundTask);
      if (!bool)
      {
        d.a(paramPersistentBackgroundTask, " was successfully executed recently and was NOT scheduled for launch");
        return;
      }
      d.a(paramPersistentBackgroundTask, "is disabled and was NOT scheduled for launch");
    }
    
    private void a(List<PersistentBackgroundTask> paramList1, List<PersistentBackgroundTask> paramList2)
    {
      try
      {
        if (!paramList2.isEmpty()) {
          e.b(paramList2);
        }
        if (!paramList1.isEmpty()) {
          e.a(paramList1);
        }
        return;
      }
      catch (RuntimeException paramList1)
      {
        AssertionUtil.reportThrowableButNeverCrash(paramList1);
      }
    }
    
    final void a(long paramLong, int paramInt, int... paramVarArgs)
    {
      if (paramVarArgs != null) {
        paramVarArgs = org.c.a.a.a.a.b(paramVarArgs, paramInt);
      } else {
        paramVarArgs = new int[] { paramInt };
      }
      sendMessageDelayed(obtainMessage(0, paramVarArgs), paramLong);
    }
    
    public final void handleMessage(Message paramMessage)
    {
      switch (what)
      {
      default: 
        
      case 1: 
        paramMessage = (d.b)obj;
        if (paramMessage != null)
        {
          b.a(a, c);
          return;
        }
        break;
      case 0: 
        paramMessage = (int[])obj;
        if (paramMessage != null)
        {
          ArrayList localArrayList1 = new ArrayList();
          ArrayList localArrayList2 = new ArrayList();
          int j = paramMessage.length;
          int i = 0;
          while (i < j)
          {
            int k = paramMessage[i];
            a((PersistentBackgroundTask)f.a().get(k), localArrayList1, localArrayList2);
            i += 1;
          }
          a(localArrayList1, localArrayList2);
          return;
        }
        break;
      }
    }
  }
  
  static final class b<T>
  {
    final ExecutionResult a;
    final b.a<T> b;
    final T c;
    
    private b(ExecutionResult paramExecutionResult, b.a<T> parama, T paramT)
    {
      b = parama;
      c = paramT;
      a = paramExecutionResult;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */