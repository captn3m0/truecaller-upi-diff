package com.truecaller.common.background;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.SparseArray;
import com.truecaller.log.AssertionUtil;
import java.util.ArrayList;
import java.util.List;
import org.c.a.a.a.a;

final class d$a
  extends Handler
{
  final Runnable a = new Runnable()
  {
    public final void run()
    {
      ArrayList localArrayList1 = new ArrayList();
      ArrayList localArrayList2 = new ArrayList();
      SparseArray localSparseArray = d.a.a(d.a.this).a();
      int j = localSparseArray.size();
      int i = 0;
      while (i < j)
      {
        d.a.a(d.a.this, (PersistentBackgroundTask)localSparseArray.valueAt(i), localArrayList1, localArrayList2);
        i += 1;
      }
      d.a.a(d.a.this, localArrayList1, localArrayList2);
    }
  };
  private final int b = 0;
  private final int c = 1;
  private final Context d;
  private final c e;
  private final f f;
  
  private d$a(Looper paramLooper, Context paramContext, f paramf, c paramc)
  {
    super(paramLooper);
    e = paramc;
    f = paramf;
    d = paramContext;
  }
  
  private void a(PersistentBackgroundTask paramPersistentBackgroundTask, List<PersistentBackgroundTask> paramList1, List<PersistentBackgroundTask> paramList2)
  {
    if (paramPersistentBackgroundTask == null) {
      return;
    }
    boolean bool = paramPersistentBackgroundTask.b(d);
    if ((paramPersistentBackgroundTask.a(d)) && (bool))
    {
      paramList1.add(paramPersistentBackgroundTask);
      d.a(paramPersistentBackgroundTask, "was scheduled for launch");
      return;
    }
    paramList2.add(paramPersistentBackgroundTask);
    if (!bool)
    {
      d.a(paramPersistentBackgroundTask, " was successfully executed recently and was NOT scheduled for launch");
      return;
    }
    d.a(paramPersistentBackgroundTask, "is disabled and was NOT scheduled for launch");
  }
  
  private void a(List<PersistentBackgroundTask> paramList1, List<PersistentBackgroundTask> paramList2)
  {
    try
    {
      if (!paramList2.isEmpty()) {
        e.b(paramList2);
      }
      if (!paramList1.isEmpty()) {
        e.a(paramList1);
      }
      return;
    }
    catch (RuntimeException paramList1)
    {
      AssertionUtil.reportThrowableButNeverCrash(paramList1);
    }
  }
  
  final void a(long paramLong, int paramInt, int... paramVarArgs)
  {
    if (paramVarArgs != null) {
      paramVarArgs = a.b(paramVarArgs, paramInt);
    } else {
      paramVarArgs = new int[] { paramInt };
    }
    sendMessageDelayed(obtainMessage(0, paramVarArgs), paramLong);
  }
  
  public final void handleMessage(Message paramMessage)
  {
    switch (what)
    {
    default: 
      
    case 1: 
      paramMessage = (d.b)obj;
      if (paramMessage != null)
      {
        b.a(a, c);
        return;
      }
      break;
    case 0: 
      paramMessage = (int[])obj;
      if (paramMessage != null)
      {
        ArrayList localArrayList1 = new ArrayList();
        ArrayList localArrayList2 = new ArrayList();
        int j = paramMessage.length;
        int i = 0;
        while (i < j)
        {
          int k = paramMessage[i];
          a((PersistentBackgroundTask)f.a().get(k), localArrayList1, localArrayList2);
          i += 1;
        }
        a(localArrayList1, localArrayList2);
        return;
      }
      break;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.d.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */