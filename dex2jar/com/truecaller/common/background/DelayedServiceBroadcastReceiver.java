package com.truecaller.common.background;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.v;
import c.g.b.k;

public final class DelayedServiceBroadcastReceiver
  extends BroadcastReceiver
{
  public static final a a = new a((byte)0);
  
  public static final Intent a(Context paramContext, ComponentName paramComponentName, Bundle paramBundle)
  {
    k.b(paramContext, "context");
    k.b(paramComponentName, "componentName");
    paramContext = new Intent(paramContext, DelayedServiceBroadcastReceiver.class).putExtra("component_name", (Parcelable)paramComponentName).putExtra("job_id", 2131363822);
    if (paramBundle != null) {
      paramContext.putExtra("payload", paramBundle);
    }
    k.a(paramContext, "intent");
    return paramContext;
  }
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    k.b(paramContext, "context");
    k.b(paramIntent, "intent");
    Intent localIntent = new Intent();
    ComponentName localComponentName = (ComponentName)paramIntent.getParcelableExtra("component_name");
    if (localComponentName == null) {
      return;
    }
    Integer localInteger = Integer.valueOf(paramIntent.getIntExtra("job_id", -1));
    int i;
    if (((Number)localInteger).intValue() != -1) {
      i = 1;
    } else {
      i = 0;
    }
    if (i == 0) {
      localInteger = null;
    }
    if (localInteger != null)
    {
      i = localInteger.intValue();
      paramIntent = (Bundle)paramIntent.getParcelableExtra("payload");
      if (paramIntent != null) {
        localIntent.putExtras(paramIntent);
      }
      v.a(paramContext, localComponentName, i, localIntent);
      return;
    }
  }
  
  public static final class a {}
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.DelayedServiceBroadcastReceiver
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */