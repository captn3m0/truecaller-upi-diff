package com.truecaller.common.background;

import android.annotation.TargetApi;
import android.os.Bundle;
import android.os.PersistableBundle;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public final class e
{
  final int a;
  final int b;
  final long c;
  final long d;
  final boolean e;
  final long f;
  private final Map<String, Object> g;
  
  private e(a parama)
  {
    a = a;
    b = b;
    e = c;
    f = h;
    g = i;
    if (a != 0)
    {
      c = d;
      d = e;
      return;
    }
    c = f;
    d = g;
    boolean bool;
    if (c < d) {
      bool = true;
    } else {
      bool = false;
    }
    AssertionUtil.OnlyInDebug.isTrue(bool, new String[0]);
  }
  
  final long a(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(c, TimeUnit.MILLISECONDS);
  }
  
  final void a(Bundle paramBundle)
  {
    Object localObject1 = g;
    if (localObject1 != null)
    {
      if (((Map)localObject1).isEmpty()) {
        return;
      }
      localObject1 = g.entrySet().iterator();
      while (((Iterator)localObject1).hasNext())
      {
        Map.Entry localEntry = (Map.Entry)((Iterator)localObject1).next();
        Object localObject2 = localEntry.getValue();
        if ((localObject2 instanceof String)) {
          paramBundle.putString((String)localEntry.getKey(), (String)localObject2);
        } else if ((localObject2 instanceof Integer)) {
          paramBundle.putInt((String)localEntry.getKey(), ((Integer)localObject2).intValue());
        }
      }
      return;
    }
  }
  
  @TargetApi(21)
  final void a(PersistableBundle paramPersistableBundle)
  {
    Object localObject1 = g;
    if (localObject1 != null)
    {
      if (((Map)localObject1).isEmpty()) {
        return;
      }
      localObject1 = g.entrySet().iterator();
      while (((Iterator)localObject1).hasNext())
      {
        Map.Entry localEntry = (Map.Entry)((Iterator)localObject1).next();
        Object localObject2 = localEntry.getValue();
        if ((localObject2 instanceof String)) {
          paramPersistableBundle.putString((String)localEntry.getKey(), (String)localObject2);
        } else if ((localObject2 instanceof Integer)) {
          paramPersistableBundle.putInt((String)localEntry.getKey(), ((Integer)localObject2).intValue());
        }
      }
      return;
    }
  }
  
  final long b(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(d, TimeUnit.MILLISECONDS);
  }
  
  final long c(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(c, TimeUnit.MILLISECONDS);
  }
  
  final long d(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(d, TimeUnit.MILLISECONDS);
  }
  
  public static final class a
  {
    final int a;
    public int b = 0;
    public boolean c = false;
    long d = 0L;
    long e = 0L;
    long f = 0L;
    long g = 0L;
    long h = TimeUnit.MINUTES.toMillis(5L);
    Map<String, Object> i;
    
    public a(int paramInt)
    {
      a = paramInt;
    }
    
    public final a a()
    {
      c = false;
      return this;
    }
    
    public final a a(int paramInt)
    {
      b = paramInt;
      return this;
    }
    
    public final a a(long paramLong, TimeUnit paramTimeUnit)
    {
      int j = a;
      boolean bool = true;
      if (j != 1) {
        bool = false;
      }
      AssertionUtil.OnlyInDebug.isTrue(bool, new String[] { "Only periodic tasks can have period" });
      d = paramTimeUnit.toMillis(paramLong);
      return this;
    }
    
    public final a a(String paramString1, String paramString2)
    {
      AssertionUtil.OnlyInDebug.isTrue(true, new String[] { "Can not save null as string parameter" });
      if (i == null) {
        i = new HashMap();
      }
      i.put(paramString1, paramString2);
      return this;
    }
    
    public final a a(TimeUnit paramTimeUnit)
    {
      boolean bool;
      if (a == 0) {
        bool = true;
      } else {
        bool = false;
      }
      AssertionUtil.OnlyInDebug.isTrue(bool, new String[] { "Only one off tasks can have start deadline" });
      g = paramTimeUnit.toMillis(5L);
      return this;
    }
    
    public final a b(long paramLong, TimeUnit paramTimeUnit)
    {
      int j = a;
      boolean bool = true;
      if (j != 1) {
        bool = false;
      }
      AssertionUtil.OnlyInDebug.isTrue(bool, new String[] { "Only periodic tasks can have flexibility" });
      e = paramTimeUnit.toMillis(paramLong);
      return this;
    }
    
    public final e b()
    {
      int j = a;
      boolean bool = true;
      if (j == 1)
      {
        if (d <= 0L) {
          bool = false;
        }
        AssertionUtil.OnlyInDebug.isTrue(bool, new String[] { "Period not specified for periodic tasks" });
      }
      return new e(this, (byte)0);
    }
    
    public final a c(long paramLong, TimeUnit paramTimeUnit)
    {
      h = paramTimeUnit.toMillis(paramLong);
      return this;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */