package com.truecaller.common.background;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.v;

public final class a
{
  public static void a(Context paramContext, Class<? extends v> paramClass, long paramLong, Bundle paramBundle, int paramInt)
  {
    paramClass = PendingIntent.getBroadcast(paramContext, paramInt, DelayedServiceBroadcastReceiver.a(paramContext, new ComponentName(paramContext.getApplicationContext(), paramClass), paramBundle), 134217728);
    long l = SystemClock.elapsedRealtime();
    paramContext = (AlarmManager)paramContext.getSystemService("alarm");
    if (paramContext == null) {
      return;
    }
    paramContext.set(2, l + paramLong, paramClass);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */