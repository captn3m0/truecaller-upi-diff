package com.truecaller.common.background;

import android.content.Context;
import android.util.SparseArray;
import com.truecaller.common.h.a;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import java.util.Iterator;

final class f
{
  private final Context a;
  private volatile SparseArray<PersistentBackgroundTask> b = null;
  
  f(Context paramContext)
  {
    a = paramContext.getApplicationContext();
  }
  
  final SparseArray<PersistentBackgroundTask> a()
  {
    Object localObject1 = b;
    if (localObject1 == null) {
      try
      {
        Object localObject2 = b;
        localObject1 = localObject2;
        if (localObject2 == null)
        {
          localObject1 = new SparseArray();
          localObject2 = a.a(a, PersistentBackgroundTask.class, PersistentBackgroundTask.class.getClassLoader()).iterator();
          while (((Iterator)localObject2).hasNext())
          {
            PersistentBackgroundTask localPersistentBackgroundTask = (PersistentBackgroundTask)((Iterator)localObject2).next();
            int i = localPersistentBackgroundTask.a();
            if (((SparseArray)localObject1).indexOfKey(i) >= 0)
            {
              StringBuilder localStringBuilder = new StringBuilder("Background tasks [");
              localStringBuilder.append(localPersistentBackgroundTask.getClass().getSimpleName());
              localStringBuilder.append("] and [");
              localStringBuilder.append(((PersistentBackgroundTask)((SparseArray)localObject1).get(i)).getClass().getSimpleName());
              localStringBuilder.append("] have te same id [");
              localStringBuilder.append(i);
              localStringBuilder.append("] ");
              AssertionUtil.AlwaysFatal.fail(new String[] { localStringBuilder.toString() });
            }
            ((SparseArray)localObject1).put(i, localPersistentBackgroundTask);
          }
          b = ((SparseArray)localObject1);
        }
        return (SparseArray<PersistentBackgroundTask>)localObject1;
      }
      finally {}
    }
    return localSparseArray;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */