package com.truecaller.common.background;

import androidx.work.c.a;
import androidx.work.j;
import androidx.work.k.a;
import androidx.work.m;
import androidx.work.m.a;
import androidx.work.q.a;
import c.l.b;
import c.n;
import java.util.concurrent.TimeUnit;

public final class g
{
  private org.a.a.h a;
  private final c.a b;
  private n<? extends androidx.work.a, org.a.a.h> c;
  private final b<? extends TrackedWorker> d;
  private final org.a.a.h e;
  
  public g(b<? extends TrackedWorker> paramb, org.a.a.h paramh)
  {
    d = paramb;
    e = paramh;
    b = new c.a();
  }
  
  private final void a(q.a<?, ?> parama)
  {
    parama.a(b.a());
    n localn = c;
    if (localn != null)
    {
      parama.a((androidx.work.a)a, b).b, TimeUnit.MILLISECONDS);
      return;
    }
  }
  
  public final m a()
  {
    if (e != null)
    {
      Object localObject = a;
      if (localObject == null) {
        localObject = new m.a(c.g.a.a(d), e.b, TimeUnit.MILLISECONDS);
      } else {
        localObject = new m.a(c.g.a.a(d), e.b, TimeUnit.MILLISECONDS, b, TimeUnit.MILLISECONDS);
      }
      a((q.a)localObject);
      localObject = ((m.a)localObject).c();
      c.g.b.k.a(localObject, "when (val flex: Duration…t) }\n            .build()");
      return (m)localObject;
    }
    throw ((Throwable)new IllegalStateException("Interval of a periodic request can not be null".toString()));
  }
  
  public final g a(androidx.work.a parama, org.a.a.h paramh)
  {
    c.g.b.k.b(parama, "backoffPolicy");
    c.g.b.k.b(paramh, "backoffDelay");
    c = new n(parama, paramh);
    return this;
  }
  
  public final g a(j paramj)
  {
    c.g.b.k.b(paramj, "networkType");
    b.a(paramj);
    return this;
  }
  
  public final g a(org.a.a.h paramh)
  {
    c.g.b.k.b(paramh, "interval");
    a = paramh;
    return this;
  }
  
  public final g a(boolean paramBoolean)
  {
    b.a(paramBoolean);
    return this;
  }
  
  public final androidx.work.k b()
  {
    Object localObject = new k.a(c.g.a.a(d));
    a((q.a)localObject);
    localObject = ((k.a)localObject).c();
    c.g.b.k.a(localObject, "OneTimeWorkRequest.Build…t) }\n            .build()");
    return (androidx.work.k)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */