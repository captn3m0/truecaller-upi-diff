package com.truecaller.common.background;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.text.TextUtils;
import com.truecaller.common.b.a;
import com.truecaller.utils.i;
import com.truecaller.utils.t;
import com.truecaller.utils.t.a;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.c.a.a.a.j;

public final class AlarmScheduler
  extends Service
  implements b.a<b>
{
  private static final long a = TimeUnit.MINUTES.toMillis(1L);
  private f b;
  private final Set<Integer> c = new HashSet();
  private int d;
  
  private static PendingIntent a(Context paramContext, int paramInt1, Bundle paramBundle, int paramInt2, boolean paramBoolean)
  {
    Intent localIntent = new Intent(paramContext, AlarmScheduler.class);
    localIntent.setAction("com.truecaller.common.background.ACTION_FALLBACK_EXECUTE");
    localIntent.setData(new Uri.Builder().scheme("job").appendPath(String.valueOf(paramInt1)).build());
    localIntent.putExtra("retries", paramInt2);
    if ((paramBundle != null) && (!paramBundle.isEmpty())) {
      localIntent.putExtra("task_params", paramBundle);
    }
    if (paramBoolean) {
      paramInt1 = 134217728;
    } else {
      paramInt1 = 536870912;
    }
    return PendingIntent.getService(paramContext, 0, localIntent, paramInt1);
  }
  
  private static void a(AlarmManager paramAlarmManager, e parame, long paramLong, PendingIntent paramPendingIntent)
  {
    if (a == 0)
    {
      l = paramLong;
      if (paramLong == 0L) {
        l = j.a(parame.c(TimeUnit.MILLISECONDS), parame.d(TimeUnit.MILLISECONDS));
      }
      paramAlarmManager.set(2, SystemClock.elapsedRealtime() + l, paramPendingIntent);
      return;
    }
    long l = parame.a(TimeUnit.MILLISECONDS);
    if (paramLong == 0L) {
      paramLong = j.a(1000L, 300000L);
    } else {
      paramLong = Math.min(l, paramLong);
    }
    paramAlarmManager.setInexactRepeating(2, SystemClock.elapsedRealtime() + paramLong, l, paramPendingIntent);
  }
  
  private static void b(AlarmManager paramAlarmManager, Context paramContext, int paramInt)
  {
    paramContext = a(paramContext, paramInt, null, 0, false);
    if (paramContext == null) {
      return;
    }
    paramAlarmManager.cancel(paramContext);
  }
  
  public final IBinder onBind(Intent paramIntent)
  {
    return null;
  }
  
  public final void onCreate()
  {
    super.onCreate();
    b = new f(this);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    b = null;
  }
  
  public final int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    if ((paramIntent != null) && (TextUtils.equals("com.truecaller.common.background.ACTION_FALLBACK_EXECUTE", paramIntent.getAction())) && (paramIntent.getData() != null)) {
      for (;;)
      {
        try
        {
          int i = Integer.parseInt(paramIntent.getData().getPath());
          synchronized (c)
          {
            c.add(Integer.valueOf(paramInt2));
            d = paramInt2;
            ??? = paramIntent.getBundleExtra("task_params");
            paramInt1 = paramIntent.getIntExtra("retries", 0);
            getApplicationc.a(i, (Bundle)???, this, new b(i, (Bundle)???, paramInt1, paramInt2, (byte)0));
            return 3;
          }
        }
        catch (NumberFormatException localNumberFormatException)
        {
          continue;
        }
        synchronized (c)
        {
          if (c.isEmpty()) {
            stopSelf(paramInt2);
          }
          return super.onStartCommand(paramIntent, paramInt1, paramInt2);
        }
      }
    }
    return super.onStartCommand(paramIntent, paramInt1, paramInt2);
  }
  
  static final class a
    implements c
  {
    private final Context a;
    private final i b;
    
    a(Context paramContext)
    {
      a = paramContext.getApplicationContext();
      b = com.truecaller.utils.c.a().a(a).a().f();
    }
    
    public final void a(List<PersistentBackgroundTask> paramList)
    {
      AlarmManager localAlarmManager = (AlarmManager)a.getSystemService("alarm");
      paramList = paramList.iterator();
      while (paramList.hasNext())
      {
        PersistentBackgroundTask localPersistentBackgroundTask = (PersistentBackgroundTask)paramList.next();
        e locale = localPersistentBackgroundTask.b();
        Bundle localBundle = new Bundle();
        locale.a(localBundle);
        AlarmScheduler.a(localAlarmManager, locale, AlarmScheduler.a(a, localPersistentBackgroundTask.a(), localBundle));
        a.getApplicationContext()).c.b(localPersistentBackgroundTask.a());
      }
    }
    
    public final boolean a()
    {
      return true;
    }
    
    public final boolean a(e parame)
    {
      switch (b)
      {
      default: 
        return true;
      case 2: 
        return (b.a()) && (b.d());
      }
      return b.a();
    }
    
    public final void b(List<PersistentBackgroundTask> paramList)
    {
      AlarmManager localAlarmManager = (AlarmManager)a.getSystemService("alarm");
      paramList = paramList.iterator();
      while (paramList.hasNext())
      {
        PersistentBackgroundTask localPersistentBackgroundTask = (PersistentBackgroundTask)paramList.next();
        AlarmScheduler.a(localAlarmManager, a, localPersistentBackgroundTask.a());
      }
    }
  }
  
  static final class b
  {
    final int a;
    final Bundle b;
    final int c;
    final int d;
    
    private b(int paramInt1, Bundle paramBundle, int paramInt2, int paramInt3)
    {
      a = paramInt1;
      b = paramBundle;
      c = paramInt2;
      d = paramInt3;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.AlarmScheduler
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */