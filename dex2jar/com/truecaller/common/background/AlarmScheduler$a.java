package com.truecaller.common.background;

import android.app.AlarmManager;
import android.content.Context;
import android.os.Bundle;
import com.truecaller.common.b.a;
import com.truecaller.utils.i;
import com.truecaller.utils.t;
import com.truecaller.utils.t.a;
import java.util.Iterator;
import java.util.List;

final class AlarmScheduler$a
  implements c
{
  private final Context a;
  private final i b;
  
  AlarmScheduler$a(Context paramContext)
  {
    a = paramContext.getApplicationContext();
    b = com.truecaller.utils.c.a().a(a).a().f();
  }
  
  public final void a(List<PersistentBackgroundTask> paramList)
  {
    AlarmManager localAlarmManager = (AlarmManager)a.getSystemService("alarm");
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      PersistentBackgroundTask localPersistentBackgroundTask = (PersistentBackgroundTask)paramList.next();
      e locale = localPersistentBackgroundTask.b();
      Bundle localBundle = new Bundle();
      locale.a(localBundle);
      AlarmScheduler.a(localAlarmManager, locale, AlarmScheduler.a(a, localPersistentBackgroundTask.a(), localBundle));
      a.getApplicationContext()).c.b(localPersistentBackgroundTask.a());
    }
  }
  
  public final boolean a()
  {
    return true;
  }
  
  public final boolean a(e parame)
  {
    switch (b)
    {
    default: 
      return true;
    case 2: 
      return (b.a()) && (b.d());
    }
    return b.a();
  }
  
  public final void b(List<PersistentBackgroundTask> paramList)
  {
    AlarmManager localAlarmManager = (AlarmManager)a.getSystemService("alarm");
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      PersistentBackgroundTask localPersistentBackgroundTask = (PersistentBackgroundTask)paramList.next();
      AlarmScheduler.a(localAlarmManager, a, localPersistentBackgroundTask.a());
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.AlarmScheduler.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */