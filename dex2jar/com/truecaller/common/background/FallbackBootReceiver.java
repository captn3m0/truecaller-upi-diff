package com.truecaller.common.background;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.truecaller.common.b.a;

public class FallbackBootReceiver
  extends BroadcastReceiver
{
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    if (TextUtils.equals("android.intent.action.BOOT_COMPLETED", paramIntent.getAction())) {
      getApplicationContextc.d();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.FallbackBootReceiver
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */