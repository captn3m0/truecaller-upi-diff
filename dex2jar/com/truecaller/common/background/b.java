package com.truecaller.common.background;

import android.os.Bundle;

public abstract interface b
{
  public abstract ExecutionResult a(int paramInt);
  
  public abstract ExecutionResult a(int paramInt, Bundle paramBundle);
  
  public abstract void a();
  
  public abstract <T> void a(int paramInt, Bundle paramBundle, a<T> parama, T paramT);
  
  public abstract void a(int paramInt, int... paramVarArgs);
  
  public abstract void a(long paramLong, int paramInt, int... paramVarArgs);
  
  public abstract void a(Runnable paramRunnable);
  
  public abstract void b();
  
  public abstract void b(int paramInt);
  
  public abstract void b(int paramInt, Bundle paramBundle);
  
  public abstract void c();
  
  public abstract void d();
  
  public abstract void e();
  
  public static abstract interface a<T>
  {
    public abstract void a(ExecutionResult paramExecutionResult, T paramT);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */