package com.truecaller.common;

import c.f;
import c.g;
import c.g.a.a;
import c.g.b.k;
import c.g.b.l;
import c.n.m;
import com.truecaller.common.network.KnownDomain;
import java.util.Iterator;
import java.util.List;

public final class c
{
  private static final f b = g.a((a)a.a);
  
  public static final KnownDomain a(String paramString)
  {
    k.b(paramString, "receiver$0");
    Iterator localIterator = ((Iterable)a()).iterator();
    Object localObject;
    while (localIterator.hasNext())
    {
      localObject = localIterator.next();
      if (m.a((String)localObject, paramString, true))
      {
        paramString = (String)localObject;
        break label53;
      }
    }
    paramString = null;
    label53:
    if ((String)paramString != null)
    {
      localObject = KnownDomain.DOMAIN_REGION_1;
      paramString = (String)localObject;
      if (localObject != null) {}
    }
    else
    {
      paramString = KnownDomain.DOMAIN_OTHER_REGIONS;
    }
    return paramString;
  }
  
  public static final List<String> a()
  {
    return (List)b.b();
  }
  
  static final class a
    extends l
    implements a<List<? extends String>>
  {
    public static final a a = new a();
    
    a()
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */