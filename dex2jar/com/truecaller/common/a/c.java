package com.truecaller.common.a;

import javax.inject.Provider;

public final class c
  implements dagger.a.d<b>
{
  private final Provider<com.truecaller.analytics.b> a;
  private final Provider<com.truecaller.utils.d> b;
  
  private c(Provider<com.truecaller.analytics.b> paramProvider, Provider<com.truecaller.utils.d> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static c a(Provider<com.truecaller.analytics.b> paramProvider, Provider<com.truecaller.utils.d> paramProvider1)
  {
    return new c(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */