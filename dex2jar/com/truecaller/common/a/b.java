package com.truecaller.common.a;

import c.g.b.k;
import com.truecaller.analytics.e.a;
import com.truecaller.utils.d;
import java.lang.reflect.InvocationTargetException;
import javax.inject.Inject;

public final class b
  implements a
{
  private final com.truecaller.analytics.b a;
  private final d b;
  
  @Inject
  public b(com.truecaller.analytics.b paramb, d paramd)
  {
    a = paramb;
    b = paramd;
  }
  
  public final void a(int paramInt, Exception paramException)
  {
    k.b(paramException, "e");
    com.truecaller.analytics.b localb = a;
    e.a locala = new e.a("DeclineCallErrors");
    switch (paramInt)
    {
    default: 
      localObject = "Default";
      break;
    case 2: 
      localObject = "TelecomManager";
      break;
    case 1: 
      localObject = "Msim";
    }
    Object localObject = locala.a("Method", (String)localObject);
    if ((paramException instanceof InvocationTargetException))
    {
      if ((((InvocationTargetException)paramException).getTargetException() instanceof SecurityException)) {
        paramException = "Security";
      } else {
        paramException = "Unknown";
      }
    }
    else if ((paramException instanceof SecurityException)) {
      paramException = "Security";
    } else if ((paramException instanceof NoSuchMethodException)) {
      paramException = "Reflection";
    } else {
      paramException = "Unknown";
    }
    locala = ((e.a)localObject).a("Reason", paramException);
    localObject = b.i();
    paramException = (Exception)localObject;
    if (localObject == null) {
      paramException = "";
    }
    paramException = locala.a("SecurityPatchVersion", paramException).a();
    k.a(paramException, "AnalyticsEvent.Builder(C…\n                .build()");
    localb.b(paramException);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */