package com.truecaller.common.c.b;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.DatabaseUtils;
import android.database.DefaultDatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import com.truecaller.common.c.a.e;
import com.truecaller.log.AssertionUtil;
import java.util.Collection;

public final class a
  extends SQLiteOpenHelper
  implements e
{
  private final c[] a;
  private final d[] b;
  
  private a(Context paramContext, String paramString, int paramInt, c[] paramArrayOfc, d[] paramArrayOfd)
  {
    super(paramContext, paramString, null, paramInt, new a((byte)0));
    a = paramArrayOfc;
    b = paramArrayOfd;
  }
  
  public static a a(Context paramContext, String paramString, int paramInt, c[] paramArrayOfc, d[] paramArrayOfd)
  {
    return new a(paramContext, paramString, paramInt, paramArrayOfc, paramArrayOfd);
  }
  
  public static void a(Cursor paramCursor, Collection<ContentValues> paramCollection)
  {
    if (paramCursor != null)
    {
      if (paramCursor.getCount() <= 0) {
        return;
      }
      paramCursor.moveToPosition(-1);
      while (paramCursor.moveToNext())
      {
        ContentValues localContentValues = new ContentValues();
        DatabaseUtils.cursorRowToContentValues(paramCursor, localContentValues);
        if (localContentValues.size() > 0) {
          paramCollection.add(localContentValues);
        }
      }
      return;
    }
  }
  
  public final SQLiteDatabase a_(Context paramContext)
    throws SQLiteException
  {
    return getWritableDatabase();
  }
  
  public final void onCreate(SQLiteDatabase paramSQLiteDatabase)
  {
    c[] arrayOfc = a;
    int j = arrayOfc.length;
    int i = 0;
    while (i < j)
    {
      c localc = arrayOfc[i];
      if (localc != null) {
        localc.a(paramSQLiteDatabase);
      }
      i += 1;
    }
  }
  
  public final void onOpen(SQLiteDatabase paramSQLiteDatabase)
  {
    super.onOpen(paramSQLiteDatabase);
    b.b(paramSQLiteDatabase);
    paramSQLiteDatabase.beginTransaction();
    try
    {
      b.a(paramSQLiteDatabase, "view");
      paramSQLiteDatabase.setTransactionSuccessful();
      return;
    }
    finally
    {
      paramSQLiteDatabase.endTransaction();
    }
  }
  
  public final void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    c[] arrayOfc = a;
    int j = arrayOfc.length;
    int i = 0;
    while (i < j)
    {
      c localc = arrayOfc[i];
      if (localc != null) {
        localc.a(paramSQLiteDatabase, paramInt1, paramInt2);
      }
      i += 1;
    }
  }
  
  static final class a
    implements DatabaseErrorHandler
  {
    public final void onCorruption(SQLiteDatabase paramSQLiteDatabase)
    {
      AssertionUtil.isTrue(false, new String[] { paramSQLiteDatabase.toString() });
      new DefaultDatabaseErrorHandler().onCorruption(paramSQLiteDatabase);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.b.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */