package com.truecaller.common.c.b;

import android.database.sqlite.SQLiteDatabase;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import java.util.ArrayList;
import java.util.List;

public abstract class c
{
  protected final String a;
  protected final a[] b;
  
  protected c(String paramString, a[] paramArrayOfa)
  {
    a = paramString;
    b = paramArrayOfa;
  }
  
  protected final void a(SQLiteDatabase paramSQLiteDatabase)
  {
    boolean bool;
    if (b.length > 0) {
      bool = true;
    } else {
      bool = false;
    }
    AssertionUtil.AlwaysFatal.isTrue(bool, new String[0]);
    AssertionUtil.AlwaysFatal.isTrue(b[0].a.equals("_id"), new String[0]);
    AssertionUtil.AlwaysFatal.isTrue(b[0].b.equals("INTEGER PRIMARY KEY"), new String[0]);
    ArrayList localArrayList = new ArrayList();
    StringBuilder localStringBuilder1 = new StringBuilder();
    Object localObject = new StringBuilder("CREATE TABLE '");
    ((StringBuilder)localObject).append(a);
    ((StringBuilder)localObject).append("' (");
    localStringBuilder1.append(((StringBuilder)localObject).toString());
    int i = 0;
    while (i < b.length)
    {
      if (i > 0) {
        localStringBuilder1.append(", ");
      }
      localObject = b[i];
      if (c) {
        localArrayList.add(localObject);
      }
      localStringBuilder1.append("'");
      localStringBuilder1.append(a);
      localStringBuilder1.append("' ");
      localStringBuilder1.append(b);
      i += 1;
    }
    localStringBuilder1.append(");");
    localStringBuilder1.toString();
    paramSQLiteDatabase.execSQL(localStringBuilder1.toString());
    i = 0;
    while (i < localArrayList.size())
    {
      localStringBuilder1 = new StringBuilder();
      localObject = (a)localArrayList.get(i);
      localStringBuilder1.append("CREATE INDEX ");
      StringBuilder localStringBuilder2 = new StringBuilder();
      localStringBuilder2.append(a);
      localStringBuilder2.append("_");
      localStringBuilder2.append(a);
      localStringBuilder2.append("_idx");
      localStringBuilder1.append(localStringBuilder2.toString());
      localStringBuilder1.append(" ON ");
      localStringBuilder1.append(a);
      localStringBuilder1.append(" (");
      localStringBuilder1.append(a);
      localStringBuilder1.append(");");
      localStringBuilder1.toString();
      paramSQLiteDatabase.execSQL(localStringBuilder1.toString());
      i += 1;
    }
  }
  
  protected void a(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    paramSQLiteDatabase = new StringBuilder("table: ");
    paramSQLiteDatabase.append(a);
    paramSQLiteDatabase.append(" unsupported onUpgrade(oldVersion: ");
    paramSQLiteDatabase.append(paramInt1);
    paramSQLiteDatabase.append(", newVersion: ");
    paramSQLiteDatabase.append(paramInt2);
    paramSQLiteDatabase.append(")");
    AssertionUtil.AlwaysFatal.isTrue(false, new String[] { paramSQLiteDatabase.toString() });
  }
  
  protected final void b(SQLiteDatabase paramSQLiteDatabase)
  {
    StringBuilder localStringBuilder = new StringBuilder("DROP TABLE IF EXISTS ");
    localStringBuilder.append(a);
    paramSQLiteDatabase.execSQL(localStringBuilder.toString());
  }
  
  public static final class a
  {
    public final String a;
    public final String b;
    public final boolean c;
    
    public a(String paramString1, String paramString2)
    {
      a = paramString1;
      b = paramString2;
      c = false;
    }
    
    public a(String paramString1, String paramString2, byte paramByte)
    {
      a = paramString1;
      b = paramString2;
      c = true;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.b.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */