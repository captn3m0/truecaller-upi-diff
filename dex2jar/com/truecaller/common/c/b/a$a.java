package com.truecaller.common.c.b;

import android.database.DatabaseErrorHandler;
import android.database.DefaultDatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import com.truecaller.log.AssertionUtil;

final class a$a
  implements DatabaseErrorHandler
{
  public final void onCorruption(SQLiteDatabase paramSQLiteDatabase)
  {
    AssertionUtil.isTrue(false, new String[] { paramSQLiteDatabase.toString() });
    new DefaultDatabaseErrorHandler().onCorruption(paramSQLiteDatabase);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.b.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */