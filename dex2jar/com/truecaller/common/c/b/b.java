package com.truecaller.common.c.b;

import android.content.ComponentName;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.OperationCanceledException;
import android.text.TextUtils;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import java.io.PrintStream;

public final class b
{
  public static String a = null;
  private static final String[] b = { "_count" };
  
  public static int a(ContentResolver paramContentResolver, Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    return b(paramContentResolver, paramUri, paramString, paramArrayOfString);
  }
  
  private static Cursor a(SQLiteDatabase paramSQLiteDatabase, String paramString, String[] paramArrayOfString)
  {
    return paramSQLiteDatabase.query("sqlite_master", null, paramString, paramArrayOfString, null, null, null);
  }
  
  public static String a(Context paramContext, Class<? extends ContentProvider> paramClass)
  {
    Object localObject = a;
    if (localObject != null) {
      return (String)localObject;
    }
    localObject = new ComponentName(paramContext, paramClass);
    paramClass = null;
    try
    {
      paramContext = getPackageManagergetProviderInfo0authority;
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      AssertionUtil.shouldNeverHappen(paramContext, new String[0]);
      paramContext = paramClass;
    }
    AssertionUtil.AlwaysFatal.isTrue(TextUtils.isEmpty(paramContext) ^ true, new String[0]);
    return paramContext;
  }
  
  public static String a(String paramString, String... paramVarArgs)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    int i = 0;
    while (i < paramVarArgs.length)
    {
      if (i > 0) {
        localStringBuilder.append(", ");
      }
      String str = paramVarArgs[i];
      localStringBuilder.append(paramString);
      localStringBuilder.append('.');
      localStringBuilder.append(str);
      localStringBuilder.append(" AS ");
      localStringBuilder.append(str);
      i += 1;
    }
    return localStringBuilder.toString();
  }
  
  public static void a(ContentValues paramContentValues, String paramString, Object paramObject)
  {
    if (paramObject == null)
    {
      paramContentValues.putNull(paramString);
      return;
    }
    if ((paramObject instanceof String))
    {
      paramContentValues.put(paramString, (String)paramObject);
      return;
    }
    if ((paramObject instanceof Integer))
    {
      paramContentValues.put(paramString, (Integer)paramObject);
      return;
    }
    if ((paramObject instanceof Long))
    {
      paramContentValues.put(paramString, (Long)paramObject);
      return;
    }
    if ((paramObject instanceof Boolean))
    {
      paramContentValues.put(paramString, (Boolean)paramObject);
      return;
    }
    if ((paramObject instanceof Float))
    {
      paramContentValues.put(paramString, (Float)paramObject);
      return;
    }
    if ((paramObject instanceof Double))
    {
      paramContentValues.put(paramString, (Double)paramObject);
      return;
    }
    if ((paramObject instanceof byte[]))
    {
      paramContentValues.put(paramString, (byte[])paramObject);
      return;
    }
    if ((paramObject instanceof Short))
    {
      paramContentValues.put(paramString, (Short)paramObject);
      return;
    }
    paramContentValues = new StringBuilder("Unknown value type, ");
    paramContentValues.append(paramObject.getClass());
    throw new IllegalArgumentException(paramContentValues.toString());
  }
  
  public static void a(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.enableWriteAheadLogging();
  }
  
  private static void a(SQLiteDatabase paramSQLiteDatabase, Cursor paramCursor, PrintStream paramPrintStream)
  {
    String str3 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("type"));
    String str2 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("name"));
    String str1 = paramCursor.getString(paramCursor.getColumnIndexOrThrow("sql"));
    paramCursor = str1;
    if (str1 == null) {
      paramCursor = "<none>";
    }
    paramPrintStream.println(String.format("%s, name=%s, sql=%s", new Object[] { str3, str2, paramCursor }));
    if (("view".equals(str3)) || ("table".equals(str3)))
    {
      paramCursor = paramSQLiteDatabase.query(str2, null, null, null, null, null, null);
      if (paramCursor != null)
      {
        int i = 0;
        try
        {
          while (paramCursor.moveToNext())
          {
            int k = paramCursor.getColumnCount();
            paramPrintStream.println(String.format("%s row %d, %d columns", new Object[] { str2, Integer.valueOf(i), Integer.valueOf(k) }));
            int j = 0;
            while (j < k)
            {
              str1 = paramCursor.getColumnName(j);
              if (paramCursor.isNull(j)) {
                paramSQLiteDatabase = "NULL";
              } else {
                paramSQLiteDatabase = paramCursor.getString(j);
              }
              paramPrintStream.println(String.format("%s=%s", new Object[] { str1, paramSQLiteDatabase }));
              j += 1;
            }
            i += 1;
          }
          return;
        }
        finally
        {
          paramCursor.close();
        }
      }
    }
  }
  
  public static void a(SQLiteDatabase paramSQLiteDatabase, PrintStream paramPrintStream)
  {
    Cursor localCursor = a(paramSQLiteDatabase, null, null);
    try
    {
      while (localCursor.moveToNext()) {
        a(paramSQLiteDatabase, localCursor, paramPrintStream);
      }
      return;
    }
    finally
    {
      localCursor.close();
    }
  }
  
  public static void a(SQLiteDatabase paramSQLiteDatabase, String paramString)
  {
    b(paramSQLiteDatabase, "SELECT 'drop ' || type || ' ' || name || ';' FROM sqlite_master WHERE name !='android_metadata' AND type=?", new String[] { paramString });
  }
  
  public static void a(SQLiteDatabase paramSQLiteDatabase, String paramString, PrintStream paramPrintStream)
  {
    paramString = a(paramSQLiteDatabase, "name=?", new String[] { paramString });
    try
    {
      while (paramString.moveToNext()) {
        a(paramSQLiteDatabase, paramString, paramPrintStream);
      }
      return;
    }
    finally
    {
      paramString.close();
    }
  }
  
  public static void a(SQLiteDatabase paramSQLiteDatabase, String paramString1, String paramString2)
  {
    b(paramSQLiteDatabase, "SELECT 'drop ' || type || ' ' || name || ';' FROM sqlite_master WHERE name !='android_metadata' AND type=? and name=?", new String[] { paramString1, paramString2 });
  }
  
  private static int b(ContentResolver paramContentResolver, Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    int j = -1;
    int k = j;
    try
    {
      paramContentResolver = paramContentResolver.query(paramUri.buildUpon().appendPath("count").build(), b, paramString, paramArrayOfString, null, null);
      if (paramContentResolver != null)
      {
        int i = j;
        try
        {
          if (paramContentResolver.moveToNext()) {
            i = paramContentResolver.getInt(0);
          }
          k = i;
          paramContentResolver.close();
          return i;
        }
        finally
        {
          k = j;
          paramContentResolver.close();
          k = j;
        }
      }
      return -1;
    }
    catch (SQLiteException|OperationCanceledException paramContentResolver) {}
    return k;
  }
  
  public static String b(String paramString, String... paramVarArgs)
  {
    StringBuilder localStringBuilder = new StringBuilder("CREATE INDEX IF NOT EXISTS idx_");
    localStringBuilder.append(paramString);
    localStringBuilder.append("_");
    localStringBuilder.append(TextUtils.join("_", paramVarArgs));
    localStringBuilder.append(" ON ");
    localStringBuilder.append(paramString);
    localStringBuilder.append("(");
    localStringBuilder.append(TextUtils.join(",", paramVarArgs));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public static void b(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.setForeignKeyConstraintsEnabled(true);
  }
  
  private static void b(SQLiteDatabase paramSQLiteDatabase, String paramString, String[] paramArrayOfString)
  {
    paramString = paramSQLiteDatabase.rawQuery(paramString, paramArrayOfString);
    if (paramString != null) {
      try
      {
        while (paramString.moveToNext()) {
          paramSQLiteDatabase.execSQL(paramString.getString(0));
        }
        return;
      }
      finally
      {
        paramString.close();
      }
    }
  }
  
  /* Error */
  public static boolean b(SQLiteDatabase paramSQLiteDatabase, String paramString1, String paramString2)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 7
    //   3: aconst_null
    //   4: astore 6
    //   6: aload 6
    //   8: astore 4
    //   10: aload 7
    //   12: astore 5
    //   14: new 80	java/lang/StringBuilder
    //   17: dup
    //   18: ldc_w 307
    //   21: invokespecial 147	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   24: astore 8
    //   26: aload 6
    //   28: astore 4
    //   30: aload 7
    //   32: astore 5
    //   34: aload 8
    //   36: aload_1
    //   37: invokevirtual 88	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   40: pop
    //   41: aload 6
    //   43: astore 4
    //   45: aload 7
    //   47: astore 5
    //   49: aload 8
    //   51: ldc_w 309
    //   54: invokevirtual 88	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   57: pop
    //   58: aload 6
    //   60: astore 4
    //   62: aload 7
    //   64: astore 5
    //   66: aload_0
    //   67: aload 8
    //   69: invokevirtual 97	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   72: aconst_null
    //   73: invokevirtual 299	android/database/sqlite/SQLiteDatabase:rawQuery	(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    //   76: astore_0
    //   77: aload_0
    //   78: astore 4
    //   80: aload_0
    //   81: astore 5
    //   83: aload_0
    //   84: aload_2
    //   85: invokeinterface 312 2 0
    //   90: istore_3
    //   91: iload_3
    //   92: iconst_m1
    //   93: if_icmpeq +15 -> 108
    //   96: aload_0
    //   97: ifnull +9 -> 106
    //   100: aload_0
    //   101: invokeinterface 227 1 0
    //   106: iconst_1
    //   107: ireturn
    //   108: aload_0
    //   109: ifnull +9 -> 118
    //   112: aload_0
    //   113: invokeinterface 227 1 0
    //   118: iconst_0
    //   119: ireturn
    //   120: astore_0
    //   121: goto +55 -> 176
    //   124: astore_0
    //   125: aload 5
    //   127: astore 4
    //   129: new 80	java/lang/StringBuilder
    //   132: dup
    //   133: ldc_w 314
    //   136: invokespecial 147	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   139: astore_1
    //   140: aload 5
    //   142: astore 4
    //   144: aload_1
    //   145: aload_0
    //   146: invokevirtual 317	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   149: invokevirtual 88	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   152: pop
    //   153: aload 5
    //   155: astore 4
    //   157: aload_1
    //   158: invokevirtual 97	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   161: pop
    //   162: aload 5
    //   164: ifnull +10 -> 174
    //   167: aload 5
    //   169: invokeinterface 227 1 0
    //   174: iconst_0
    //   175: ireturn
    //   176: aload 4
    //   178: ifnull +10 -> 188
    //   181: aload 4
    //   183: invokeinterface 227 1 0
    //   188: aload_0
    //   189: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	190	0	paramSQLiteDatabase	SQLiteDatabase
    //   0	190	1	paramString1	String
    //   0	190	2	paramString2	String
    //   90	4	3	i	int
    //   8	174	4	localObject1	Object
    //   12	156	5	localObject2	Object
    //   4	55	6	localObject3	Object
    //   1	62	7	localObject4	Object
    //   24	44	8	localStringBuilder	StringBuilder
    // Exception table:
    //   from	to	target	type
    //   14	26	120	finally
    //   34	41	120	finally
    //   49	58	120	finally
    //   66	77	120	finally
    //   83	91	120	finally
    //   129	140	120	finally
    //   144	153	120	finally
    //   157	162	120	finally
    //   14	26	124	java/lang/Exception
    //   34	41	124	java/lang/Exception
    //   49	58	124	java/lang/Exception
    //   66	77	124	java/lang/Exception
    //   83	91	124	java/lang/Exception
  }
  
  public static void c(SQLiteDatabase paramSQLiteDatabase)
  {
    b(paramSQLiteDatabase, "SELECT 'drop ' || type || ' ' || name || ';' FROM sqlite_master WHERE name !='android_metadata' AND type != 'index'", null);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.b.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */