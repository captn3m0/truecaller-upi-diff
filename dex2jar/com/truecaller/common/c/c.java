package com.truecaller.common.c;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteTransactionListener;
import android.net.Uri;
import android.net.Uri.Builder;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import org.c.a.a.a.k;

public abstract class c
  extends d
  implements SQLiteTransactionListener
{
  private static AtomicBoolean b = new AtomicBoolean(false);
  protected final String a = getClass().getSimpleName();
  private final ThreadLocal<Boolean> c = new ThreadLocal();
  private final ThreadLocal<Set<Uri>> d = new ThreadLocal() {};
  private volatile boolean e;
  private volatile SQLiteDatabase f;
  
  private static Uri b(Uri paramUri)
  {
    if (!k.g(paramUri.getLastPathSegment())) {
      return paramUri;
    }
    String str = paramUri.getPath();
    return paramUri.buildUpon().path(str.substring(0, str.lastIndexOf('/'))).build();
  }
  
  public static void b()
  {
    b.set(true);
  }
  
  private boolean e()
  {
    return c.get() == Boolean.TRUE;
  }
  
  protected abstract int a(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString);
  
  protected abstract int a(Uri paramUri, String paramString, String[] paramArrayOfString);
  
  /* Error */
  protected final int a(Uri paramUri, ContentValues[] paramArrayOfContentValues)
  {
    // Byte code:
    //   0: aload_2
    //   1: arraylength
    //   2: istore 4
    //   4: aload_0
    //   5: getfield 58	com/truecaller/common/c/c:d	Ljava/lang/ThreadLocal;
    //   8: invokevirtual 119	java/lang/ThreadLocal:remove	()V
    //   11: aload_0
    //   12: invokevirtual 122	com/truecaller/common/c/c:c	()Landroid/database/sqlite/SQLiteDatabase;
    //   15: astore 6
    //   17: aload 6
    //   19: aload_0
    //   20: invokevirtual 128	android/database/sqlite/SQLiteDatabase:beginTransactionWithListener	(Landroid/database/sqlite/SQLiteTransactionListener;)V
    //   23: aload_2
    //   24: arraylength
    //   25: istore 5
    //   27: iconst_0
    //   28: istore_3
    //   29: iload_3
    //   30: iload 5
    //   32: if_icmpge +32 -> 64
    //   35: aload_0
    //   36: aload_1
    //   37: aload_2
    //   38: iload_3
    //   39: aaload
    //   40: invokevirtual 131	com/truecaller/common/c/c:a	(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    //   43: ifnull +8 -> 51
    //   46: aload_0
    //   47: iconst_1
    //   48: putfield 133	com/truecaller/common/c/c:e	Z
    //   51: aload 6
    //   53: invokevirtual 136	android/database/sqlite/SQLiteDatabase:yieldIfContendedSafely	()Z
    //   56: pop
    //   57: iload_3
    //   58: iconst_1
    //   59: iadd
    //   60: istore_3
    //   61: goto -32 -> 29
    //   64: aload_0
    //   65: invokevirtual 138	com/truecaller/common/c/c:d	()V
    //   68: aload 6
    //   70: invokevirtual 141	android/database/sqlite/SQLiteDatabase:setTransactionSuccessful	()V
    //   73: aload 6
    //   75: invokevirtual 144	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   78: aload_0
    //   79: iconst_1
    //   80: invokevirtual 146	com/truecaller/common/c/c:a	(Z)V
    //   83: iload 4
    //   85: ireturn
    //   86: astore_1
    //   87: goto +6 -> 93
    //   90: astore_1
    //   91: aload_1
    //   92: athrow
    //   93: aload 6
    //   95: invokevirtual 144	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   98: aload_0
    //   99: iconst_0
    //   100: invokevirtual 146	com/truecaller/common/c/c:a	(Z)V
    //   103: aload_1
    //   104: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	105	0	this	c
    //   0	105	1	paramUri	Uri
    //   0	105	2	paramArrayOfContentValues	ContentValues[]
    //   28	33	3	i	int
    //   2	82	4	j	int
    //   25	8	5	k	int
    //   15	79	6	localSQLiteDatabase	SQLiteDatabase
    // Exception table:
    //   from	to	target	type
    //   23	27	86	finally
    //   35	51	86	finally
    //   51	57	86	finally
    //   64	73	86	finally
    //   91	93	86	finally
    //   23	27	90	java/lang/RuntimeException
    //   35	51	90	java/lang/RuntimeException
    //   51	57	90	java/lang/RuntimeException
    //   64	73	90	java/lang/RuntimeException
  }
  
  protected abstract SQLiteDatabase a(Context paramContext);
  
  protected abstract Uri a(Uri paramUri, ContentValues paramContentValues);
  
  protected void a()
  {
    f = null;
  }
  
  public final void a(Uri paramUri)
  {
    if (paramUri != null) {
      ((Set)d.get()).add(b(paramUri));
    }
  }
  
  public final void a(Collection<Uri> paramCollection)
  {
    if (paramCollection != null)
    {
      paramCollection = paramCollection.iterator();
      while (paramCollection.hasNext()) {
        a((Uri)paramCollection.next());
      }
    }
  }
  
  protected void a(boolean paramBoolean)
  {
    if ((e) && (paramBoolean))
    {
      e = false;
      Object localObject = (Collection)d.get();
      if ((localObject != null) && (!((Collection)localObject).isEmpty()))
      {
        localObject = ((Collection)localObject).iterator();
        while (((Iterator)localObject).hasNext())
        {
          Uri localUri = (Uri)((Iterator)localObject).next();
          Context localContext = getContext();
          if (localContext != null) {
            localContext.getContentResolver().notifyChange(localUri, null, false);
          }
        }
      }
    }
    d.remove();
  }
  
  /* Error */
  protected final android.content.ContentProviderResult[] a(java.util.ArrayList<android.content.ContentProviderOperation> paramArrayList)
    throws android.content.OperationApplicationException
  {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual 205	java/util/ArrayList:size	()I
    //   4: istore_3
    //   5: iload_3
    //   6: ifne +8 -> 14
    //   9: iconst_0
    //   10: anewarray 207	android/content/ContentProviderResult
    //   13: areturn
    //   14: aload_0
    //   15: invokevirtual 122	com/truecaller/common/c/c:c	()Landroid/database/sqlite/SQLiteDatabase;
    //   18: astore 4
    //   20: aload 4
    //   22: aload_0
    //   23: invokevirtual 128	android/database/sqlite/SQLiteDatabase:beginTransactionWithListener	(Landroid/database/sqlite/SQLiteTransactionListener;)V
    //   26: aload_0
    //   27: getfield 53	com/truecaller/common/c/c:c	Ljava/lang/ThreadLocal;
    //   30: getstatic 111	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   33: invokevirtual 210	java/lang/ThreadLocal:set	(Ljava/lang/Object;)V
    //   36: aload_0
    //   37: getfield 58	com/truecaller/common/c/c:d	Ljava/lang/ThreadLocal;
    //   40: invokevirtual 119	java/lang/ThreadLocal:remove	()V
    //   43: iload_3
    //   44: anewarray 207	android/content/ContentProviderResult
    //   47: astore 5
    //   49: iconst_0
    //   50: istore_2
    //   51: iload_2
    //   52: iload_3
    //   53: if_icmpge +54 -> 107
    //   56: aload_1
    //   57: iload_2
    //   58: invokevirtual 213	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   61: checkcast 215	android/content/ContentProviderOperation
    //   64: astore 6
    //   66: iload_2
    //   67: ifle +20 -> 87
    //   70: aload 6
    //   72: invokevirtual 218	android/content/ContentProviderOperation:isYieldAllowed	()Z
    //   75: ifeq +12 -> 87
    //   78: aload 4
    //   80: ldc2_w 219
    //   83: invokevirtual 223	android/database/sqlite/SQLiteDatabase:yieldIfContendedSafely	(J)Z
    //   86: pop
    //   87: aload 5
    //   89: iload_2
    //   90: aload 6
    //   92: aload_0
    //   93: aload 5
    //   95: iload_2
    //   96: invokevirtual 227	android/content/ContentProviderOperation:apply	(Landroid/content/ContentProvider;[Landroid/content/ContentProviderResult;I)Landroid/content/ContentProviderResult;
    //   99: aastore
    //   100: iload_2
    //   101: iconst_1
    //   102: iadd
    //   103: istore_2
    //   104: goto -53 -> 51
    //   107: aload_0
    //   108: invokevirtual 138	com/truecaller/common/c/c:d	()V
    //   111: aload 4
    //   113: invokevirtual 141	android/database/sqlite/SQLiteDatabase:setTransactionSuccessful	()V
    //   116: aload_0
    //   117: getfield 53	com/truecaller/common/c/c:c	Ljava/lang/ThreadLocal;
    //   120: getstatic 230	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   123: invokevirtual 210	java/lang/ThreadLocal:set	(Ljava/lang/Object;)V
    //   126: aload 4
    //   128: invokevirtual 144	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   131: aload_0
    //   132: iconst_1
    //   133: invokevirtual 146	com/truecaller/common/c/c:a	(Z)V
    //   136: aload 5
    //   138: areturn
    //   139: astore_1
    //   140: goto +6 -> 146
    //   143: astore_1
    //   144: aload_1
    //   145: athrow
    //   146: aload_0
    //   147: getfield 53	com/truecaller/common/c/c:c	Ljava/lang/ThreadLocal;
    //   150: getstatic 230	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   153: invokevirtual 210	java/lang/ThreadLocal:set	(Ljava/lang/Object;)V
    //   156: aload 4
    //   158: invokevirtual 144	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   161: aload_0
    //   162: iconst_0
    //   163: invokevirtual 146	com/truecaller/common/c/c:a	(Z)V
    //   166: aload_1
    //   167: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	168	0	this	c
    //   0	168	1	paramArrayList	java.util.ArrayList<android.content.ContentProviderOperation>
    //   50	54	2	i	int
    //   4	50	3	j	int
    //   18	139	4	localSQLiteDatabase	SQLiteDatabase
    //   47	90	5	arrayOfContentProviderResult	android.content.ContentProviderResult[]
    //   64	27	6	localContentProviderOperation	android.content.ContentProviderOperation
    // Exception table:
    //   from	to	target	type
    //   26	49	139	finally
    //   56	66	139	finally
    //   70	87	139	finally
    //   87	100	139	finally
    //   107	116	139	finally
    //   144	146	139	finally
    //   26	49	143	java/lang/RuntimeException
    //   56	66	143	java/lang/RuntimeException
    //   70	87	143	java/lang/RuntimeException
    //   87	100	143	java/lang/RuntimeException
    //   107	116	143	java/lang/RuntimeException
  }
  
  /* Error */
  protected final int b(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial 234	com/truecaller/common/c/c:e	()Z
    //   4: istore 6
    //   6: aload_0
    //   7: invokevirtual 122	com/truecaller/common/c/c:c	()Landroid/database/sqlite/SQLiteDatabase;
    //   10: astore 7
    //   12: iload 6
    //   14: ifne +78 -> 92
    //   17: aload_0
    //   18: getfield 58	com/truecaller/common/c/c:d	Ljava/lang/ThreadLocal;
    //   21: invokevirtual 119	java/lang/ThreadLocal:remove	()V
    //   24: aload 7
    //   26: aload_0
    //   27: invokevirtual 128	android/database/sqlite/SQLiteDatabase:beginTransactionWithListener	(Landroid/database/sqlite/SQLiteTransactionListener;)V
    //   30: aload_0
    //   31: aload_1
    //   32: aload_2
    //   33: aload_3
    //   34: aload 4
    //   36: invokevirtual 236	com/truecaller/common/c/c:a	(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   39: istore 5
    //   41: iload 5
    //   43: ifle +8 -> 51
    //   46: aload_0
    //   47: iconst_1
    //   48: putfield 133	com/truecaller/common/c/c:e	Z
    //   51: aload_0
    //   52: invokevirtual 138	com/truecaller/common/c/c:d	()V
    //   55: aload 7
    //   57: invokevirtual 141	android/database/sqlite/SQLiteDatabase:setTransactionSuccessful	()V
    //   60: aload 7
    //   62: invokevirtual 144	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   65: aload_0
    //   66: iconst_1
    //   67: invokevirtual 146	com/truecaller/common/c/c:a	(Z)V
    //   70: iload 5
    //   72: ireturn
    //   73: astore_1
    //   74: goto +6 -> 80
    //   77: astore_1
    //   78: aload_1
    //   79: athrow
    //   80: aload 7
    //   82: invokevirtual 144	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   85: aload_0
    //   86: iconst_0
    //   87: invokevirtual 146	com/truecaller/common/c/c:a	(Z)V
    //   90: aload_1
    //   91: athrow
    //   92: aload_0
    //   93: aload_1
    //   94: aload_2
    //   95: aload_3
    //   96: aload 4
    //   98: invokevirtual 236	com/truecaller/common/c/c:a	(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   101: istore 5
    //   103: iload 5
    //   105: ifle +8 -> 113
    //   108: aload_0
    //   109: iconst_1
    //   110: putfield 133	com/truecaller/common/c/c:e	Z
    //   113: iload 5
    //   115: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	116	0	this	c
    //   0	116	1	paramUri	Uri
    //   0	116	2	paramContentValues	ContentValues
    //   0	116	3	paramString	String
    //   0	116	4	paramArrayOfString	String[]
    //   39	75	5	i	int
    //   4	9	6	bool	boolean
    //   10	71	7	localSQLiteDatabase	SQLiteDatabase
    // Exception table:
    //   from	to	target	type
    //   30	41	73	finally
    //   46	51	73	finally
    //   51	60	73	finally
    //   78	80	73	finally
    //   30	41	77	java/lang/RuntimeException
    //   46	51	77	java/lang/RuntimeException
    //   51	60	77	java/lang/RuntimeException
  }
  
  /* Error */
  protected final int b(Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial 234	com/truecaller/common/c/c:e	()Z
    //   4: istore 5
    //   6: aload_0
    //   7: invokevirtual 122	com/truecaller/common/c/c:c	()Landroid/database/sqlite/SQLiteDatabase;
    //   10: astore 6
    //   12: iload 5
    //   14: ifne +76 -> 90
    //   17: aload_0
    //   18: getfield 58	com/truecaller/common/c/c:d	Ljava/lang/ThreadLocal;
    //   21: invokevirtual 119	java/lang/ThreadLocal:remove	()V
    //   24: aload 6
    //   26: aload_0
    //   27: invokevirtual 128	android/database/sqlite/SQLiteDatabase:beginTransactionWithListener	(Landroid/database/sqlite/SQLiteTransactionListener;)V
    //   30: aload_0
    //   31: aload_1
    //   32: aload_2
    //   33: aload_3
    //   34: invokevirtual 238	com/truecaller/common/c/c:a	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   37: istore 4
    //   39: iload 4
    //   41: ifle +8 -> 49
    //   44: aload_0
    //   45: iconst_1
    //   46: putfield 133	com/truecaller/common/c/c:e	Z
    //   49: aload_0
    //   50: invokevirtual 138	com/truecaller/common/c/c:d	()V
    //   53: aload 6
    //   55: invokevirtual 141	android/database/sqlite/SQLiteDatabase:setTransactionSuccessful	()V
    //   58: aload 6
    //   60: invokevirtual 144	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   63: aload_0
    //   64: iconst_1
    //   65: invokevirtual 146	com/truecaller/common/c/c:a	(Z)V
    //   68: iload 4
    //   70: ireturn
    //   71: astore_1
    //   72: goto +6 -> 78
    //   75: astore_1
    //   76: aload_1
    //   77: athrow
    //   78: aload 6
    //   80: invokevirtual 144	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   83: aload_0
    //   84: iconst_0
    //   85: invokevirtual 146	com/truecaller/common/c/c:a	(Z)V
    //   88: aload_1
    //   89: athrow
    //   90: aload_0
    //   91: aload_1
    //   92: aload_2
    //   93: aload_3
    //   94: invokevirtual 238	com/truecaller/common/c/c:a	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   97: istore 4
    //   99: iload 4
    //   101: ifle +8 -> 109
    //   104: aload_0
    //   105: iconst_1
    //   106: putfield 133	com/truecaller/common/c/c:e	Z
    //   109: iload 4
    //   111: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	112	0	this	c
    //   0	112	1	paramUri	Uri
    //   0	112	2	paramString	String
    //   0	112	3	paramArrayOfString	String[]
    //   37	73	4	i	int
    //   4	9	5	bool	boolean
    //   10	69	6	localSQLiteDatabase	SQLiteDatabase
    // Exception table:
    //   from	to	target	type
    //   30	39	71	finally
    //   44	49	71	finally
    //   49	58	71	finally
    //   76	78	71	finally
    //   30	39	75	java/lang/RuntimeException
    //   44	49	75	java/lang/RuntimeException
    //   49	58	75	java/lang/RuntimeException
  }
  
  /* Error */
  protected final Uri b(Uri paramUri, ContentValues paramContentValues)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 122	com/truecaller/common/c/c:c	()Landroid/database/sqlite/SQLiteDatabase;
    //   4: astore_3
    //   5: aload_0
    //   6: invokespecial 234	com/truecaller/common/c/c:e	()Z
    //   9: ifne +68 -> 77
    //   12: aload_0
    //   13: getfield 58	com/truecaller/common/c/c:d	Ljava/lang/ThreadLocal;
    //   16: invokevirtual 119	java/lang/ThreadLocal:remove	()V
    //   19: aload_3
    //   20: aload_0
    //   21: invokevirtual 128	android/database/sqlite/SQLiteDatabase:beginTransactionWithListener	(Landroid/database/sqlite/SQLiteTransactionListener;)V
    //   24: aload_0
    //   25: aload_1
    //   26: aload_2
    //   27: invokevirtual 131	com/truecaller/common/c/c:a	(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    //   30: astore_1
    //   31: aload_1
    //   32: ifnull +8 -> 40
    //   35: aload_0
    //   36: iconst_1
    //   37: putfield 133	com/truecaller/common/c/c:e	Z
    //   40: aload_0
    //   41: invokevirtual 138	com/truecaller/common/c/c:d	()V
    //   44: aload_3
    //   45: invokevirtual 141	android/database/sqlite/SQLiteDatabase:setTransactionSuccessful	()V
    //   48: aload_3
    //   49: invokevirtual 144	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   52: aload_0
    //   53: iconst_1
    //   54: invokevirtual 146	com/truecaller/common/c/c:a	(Z)V
    //   57: aload_1
    //   58: areturn
    //   59: astore_1
    //   60: goto +6 -> 66
    //   63: astore_1
    //   64: aload_1
    //   65: athrow
    //   66: aload_3
    //   67: invokevirtual 144	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   70: aload_0
    //   71: iconst_0
    //   72: invokevirtual 146	com/truecaller/common/c/c:a	(Z)V
    //   75: aload_1
    //   76: athrow
    //   77: aload_0
    //   78: aload_1
    //   79: aload_2
    //   80: invokevirtual 131	com/truecaller/common/c/c:a	(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    //   83: astore_1
    //   84: aload_1
    //   85: ifnull +8 -> 93
    //   88: aload_0
    //   89: iconst_1
    //   90: putfield 133	com/truecaller/common/c/c:e	Z
    //   93: aload_1
    //   94: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	95	0	this	c
    //   0	95	1	paramUri	Uri
    //   0	95	2	paramContentValues	ContentValues
    //   4	63	3	localSQLiteDatabase	SQLiteDatabase
    // Exception table:
    //   from	to	target	type
    //   24	31	59	finally
    //   35	40	59	finally
    //   40	48	59	finally
    //   64	66	59	finally
    //   24	31	63	java/lang/RuntimeException
    //   35	40	63	java/lang/RuntimeException
    //   40	48	63	java/lang/RuntimeException
  }
  
  public final SQLiteDatabase c()
  {
    if (b.compareAndSet(true, false)) {
      a();
    }
    Object localObject = f;
    if (localObject == null) {
      try
      {
        SQLiteDatabase localSQLiteDatabase2 = f;
        localObject = localSQLiteDatabase2;
        if (localSQLiteDatabase2 == null)
        {
          localObject = a(getContext());
          f = ((SQLiteDatabase)localObject);
        }
        return (SQLiteDatabase)localObject;
      }
      finally {}
    }
    return localSQLiteDatabase1;
  }
  
  protected void d() {}
  
  public void onBegin() {}
  
  public void onCommit() {}
  
  public boolean onCreate()
  {
    return true;
  }
  
  public void onRollback() {}
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */