package com.truecaller.common.c;

import android.database.ContentObserver;
import android.os.Handler;
import android.os.Looper;

public abstract class b
  extends ContentObserver
{
  private final Handler a;
  private final long b;
  private final Runnable c;
  
  public b(Handler paramHandler)
  {
    this(paramHandler, 300L);
  }
  
  public b(Handler paramHandler, long paramLong)
  {
    super(paramHandler);
    Handler localHandler = paramHandler;
    if (paramHandler == null) {
      localHandler = new Handler(Looper.getMainLooper());
    }
    a = localHandler;
    b = paramLong;
    c = new Runnable()
    {
      public final void run()
      {
        a();
      }
    };
  }
  
  public abstract void a();
  
  protected final void a(long paramLong)
  {
    a.removeCallbacks(c);
    a.postDelayed(c, paramLong);
  }
  
  public final void onChange(boolean paramBoolean)
  {
    a(b);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */