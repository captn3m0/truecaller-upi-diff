package com.truecaller.common.c;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.text.TextUtils;
import com.truecaller.common.c.a.a.a;
import com.truecaller.common.c.a.a.b;
import com.truecaller.common.c.a.a.c;
import com.truecaller.common.c.a.a.d;
import com.truecaller.common.c.a.a.e;
import com.truecaller.common.c.a.a.f;
import com.truecaller.common.c.a.a.g;
import com.truecaller.common.c.a.a.h;
import com.truecaller.common.c.a.e;
import com.truecaller.common.c.b.b;

public abstract class a
  extends c
{
  private volatile com.truecaller.common.c.a.c b;
  
  private com.truecaller.common.c.a.c e()
  {
    Object localObject = b;
    if (localObject == null) {
      try
      {
        com.truecaller.common.c.a.c localc2 = b;
        localObject = localc2;
        if (localc2 == null)
        {
          localObject = b(getContext());
          b = ((com.truecaller.common.c.a.c)localObject);
        }
        return (com.truecaller.common.c.a.c)localObject;
      }
      finally {}
    }
    return localc1;
  }
  
  protected final int a(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    com.truecaller.common.c.a.a locala = e().b(paramUri);
    a.h localh = m;
    int j;
    int i;
    if (localh != null)
    {
      j = localh.a(this, locala, paramUri, paramContentValues, paramString, paramArrayOfString);
      if (j != -1)
      {
        paramString = q;
        i = j;
        if (paramString != null) {
          i = paramString.a(this, locala, paramUri, paramContentValues, j);
        }
        a(j);
        return i;
      }
    }
    if (d)
    {
      if (e)
      {
        paramString = android.support.v4.a.a.a(paramString, "_id=?");
        paramArrayOfString = android.support.v4.a.a.a(paramArrayOfString, new String[] { paramUri.getLastPathSegment() });
      }
      j = c().updateWithOnConflict(g, paramContentValues, paramString, paramArrayOfString, b);
      if (j > 0)
      {
        if (c) {
          a(i);
        }
        a(j);
      }
      paramString = q;
      i = j;
      if (paramString != null) {
        i = paramString.a(this, locala, paramUri, paramContentValues, j);
      }
      return i;
    }
    throw new SQLiteException("Cannot update ".concat(String.valueOf(paramUri)));
  }
  
  protected final int a(Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    com.truecaller.common.c.a.a locala = e().b(paramUri);
    Object localObject = n;
    int j;
    int i;
    if (localObject != null)
    {
      j = ((a.e)localObject).a(this, locala, paramUri, paramString, paramArrayOfString);
      if (j != -1)
      {
        localObject = r;
        i = j;
        if (localObject != null) {
          i = ((a.a)localObject).a(this, locala, paramUri, paramString, paramArrayOfString, j);
        }
        a(j);
        return i;
      }
    }
    if (d)
    {
      if (e)
      {
        paramString = android.support.v4.a.a.a(paramString, "_id=?");
        paramArrayOfString = android.support.v4.a.a.a(paramArrayOfString, new String[] { paramUri.getLastPathSegment() });
      }
      else if (paramString == null)
      {
        paramString = "1";
        paramArrayOfString = null;
      }
      j = c().delete(g, paramString, paramArrayOfString);
      if (j > 0)
      {
        if (c) {
          a(i);
        }
        a(j);
      }
      localObject = r;
      i = j;
      if (localObject != null) {
        i = ((a.a)localObject).a(this, locala, paramUri, paramString, paramArrayOfString, j);
      }
      return i;
    }
    throw new SQLiteException("Cannot delete from ".concat(String.valueOf(paramUri)));
  }
  
  protected final Cursor a(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal)
  {
    com.truecaller.common.c.a.a locala = e().b(paramUri);
    Object localObject = k;
    if (localObject != null) {
      return ((a.g)localObject).a(this, locala, paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, paramString2, paramCancellationSignal);
    }
    if (c)
    {
      if (e)
      {
        paramString1 = android.support.v4.a.a.a(paramString1, "_id=?");
        localObject = android.support.v4.a.a.a(paramArrayOfString2, new String[] { paramUri.getLastPathSegment() });
        paramArrayOfString2 = paramString1;
        paramString1 = (String)localObject;
      }
      else
      {
        localObject = paramString1;
        paramString1 = paramArrayOfString2;
        paramArrayOfString2 = (String[])localObject;
      }
      if (f)
      {
        paramArrayOfString1 = c();
        if (TextUtils.isEmpty(paramArrayOfString2)) {
          paramUri = "";
        } else {
          paramUri = " WHERE ".concat(String.valueOf(paramArrayOfString2));
        }
        paramArrayOfString2 = new StringBuilder("SELECT COUNT(*) AS _count FROM ");
        paramArrayOfString2.append(g);
        paramArrayOfString2.append(paramUri);
        paramUri = paramArrayOfString1.rawQuery(paramArrayOfString2.toString(), paramString1);
      }
      else
      {
        localObject = new SQLiteQueryBuilder();
        ((SQLiteQueryBuilder)localObject).setTables(g);
        paramUri = ((SQLiteQueryBuilder)localObject).query(c(), paramArrayOfString1, paramArrayOfString2, paramString1, null, null, paramString2, paramUri.getQueryParameter("limit"), paramCancellationSignal);
      }
      if (paramUri != null)
      {
        paramArrayOfString1 = getContext();
        if (paramArrayOfString1 != null) {
          paramUri.setNotificationUri(paramArrayOfString1.getContentResolver(), i);
        }
      }
      return paramUri;
    }
    throw new SQLiteException("Cannot read from ".concat(String.valueOf(paramUri)));
  }
  
  protected final SQLiteDatabase a(Context paramContext)
  {
    com.truecaller.common.c.a.c localc = e();
    if (b != null) {
      return b.a_(paramContext);
    }
    throw new IllegalStateException("No SQLiteDatabaseFactory defined");
  }
  
  protected final Uri a(Uri paramUri, ContentValues paramContentValues)
  {
    com.truecaller.common.c.a.a locala = e().b(paramUri);
    Object localObject1 = l;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = ((a.f)localObject1).a(this, locala, paramUri, paramContentValues);
      if (localObject2 != null)
      {
        a.b localb = p;
        localObject1 = localObject2;
        if (localb != null) {
          localObject1 = localb.a(this, paramUri, paramContentValues, (Uri)localObject2);
        }
        if (c) {
          a((Uri)localObject1);
        }
        a(j);
        return (Uri)localObject1;
      }
    }
    if (d)
    {
      if ((!e) && (paramContentValues.getAsLong("_id") == null))
      {
        long l = c().insertWithOnConflict(g, "_id", paramContentValues, a);
        if (l > 0L)
        {
          localObject1 = ContentUris.withAppendedId(i, l);
          if (c) {
            a(i);
          }
          a(j);
          localObject2 = p;
          if (localObject2 != null)
          {
            paramContentValues = ((a.b)localObject2).a(this, paramUri, paramContentValues, (Uri)localObject1);
            if (paramContentValues != null) {
              return paramContentValues;
            }
            throw new SQLiteException("Could not insert into ".concat(String.valueOf(paramUri)));
          }
          return (Uri)localObject1;
        }
        throw new SQLiteException("Could not insert into ".concat(String.valueOf(paramUri)));
      }
      localObject1 = new StringBuilder("Cannot insert into a row, ");
      ((StringBuilder)localObject1).append(paramUri);
      ((StringBuilder)localObject1).append(", values=");
      ((StringBuilder)localObject1).append(paramContentValues);
      throw new SQLiteException(((StringBuilder)localObject1).toString());
    }
    throw new SQLiteException("Cannot insert into ".concat(String.valueOf(paramUri)));
  }
  
  protected final Bundle a(String paramString1, String paramString2, Bundle paramBundle)
  {
    Object localObject = e();
    localObject = ((com.truecaller.common.c.a.c)localObject).a(a.buildUpon().appendPath(paramString1).build());
    if (localObject != null)
    {
      localObject = o;
      if (localObject != null) {
        return ((a.d)localObject).a();
      }
    }
    if (paramString1.equals("dump")) {
      if (TextUtils.isEmpty(paramString2)) {
        b.a(c(), System.out);
      } else {
        b.a(c(), paramString2, System.out);
      }
    }
    return super.call(paramString1, paramString2, paramBundle);
  }
  
  protected final void a()
  {
    super.a();
    try
    {
      b = null;
      return;
    }
    finally {}
  }
  
  protected abstract com.truecaller.common.c.a.c b(Context paramContext);
  
  public String getType(Uri paramUri)
  {
    return ebh;
  }
  
  public boolean onCreate()
  {
    return super.onCreate();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */