package com.truecaller.common.c.a;

import android.content.UriMatcher;
import android.net.Uri;
import android.text.TextUtils;
import android.util.SparseArray;

public final class d
{
  final SparseArray<a> a = new SparseArray();
  final UriMatcher b = new UriMatcher(-1);
  public e c;
  public String d;
  public Uri e;
  private int f;
  
  public final b a(String paramString)
  {
    if ((!TextUtils.isEmpty(d)) && (!TextUtils.isEmpty(paramString)))
    {
      String str = d;
      int i = f + 1;
      f = i;
      return new b(this, str, paramString, i);
    }
    throw new IllegalArgumentException();
  }
  
  public final c a()
  {
    return new c(e, a, b, c);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.a.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */