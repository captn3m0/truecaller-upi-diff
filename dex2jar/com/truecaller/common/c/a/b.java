package com.truecaller.common.c.a;

import android.content.UriMatcher;
import android.net.Uri;
import android.text.TextUtils;
import android.util.SparseArray;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class b
{
  public String a;
  public boolean b;
  public boolean c;
  public boolean d;
  public int e;
  public a.g f;
  public a.f g;
  public a.h h;
  public a.e i;
  public a.b j;
  public a.c k;
  public a.a l;
  private final d m;
  private final String n;
  private final String o;
  private final int p;
  private int q;
  private Boolean r;
  private Boolean s;
  private Set<Uri> t;
  private a.d u;
  
  protected b(d paramd, String paramString1, String paramString2, int paramInt)
  {
    m = paramd;
    n = paramString1;
    o = paramString2;
    p = paramInt;
    e = 2;
    q = 2;
  }
  
  private static void a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      throw new IllegalStateException("Invalid conflict resolution method, ".concat(String.valueOf(paramInt)));
    }
  }
  
  public final b a(Collection<Uri> paramCollection)
  {
    if (!paramCollection.isEmpty())
    {
      if (t == null) {
        t = new HashSet();
      }
      t.addAll(paramCollection);
    }
    return this;
  }
  
  public final b a(boolean paramBoolean)
  {
    s = Boolean.valueOf(paramBoolean);
    return this;
  }
  
  public final b a(Uri... paramVarArgs)
  {
    if (t == null) {
      t = new HashSet();
    }
    Collections.addAll(t, paramVarArgs);
    return this;
  }
  
  public final d a()
  {
    if ((d) && (b)) {
      throw new IllegalStateException("Cannot combine \"count()\" with \"row()\"");
    }
    Object localObject1 = t;
    HashSet localHashSet;
    if (localObject1 != null)
    {
      if ((!d) && (!c)) {
        localHashSet = new HashSet((Collection)localObject1);
      } else {
        throw new IllegalStateException("Cannot use \"alsoNotify(Uri)\" for views or counts");
      }
    }
    else {
      localHashSet = null;
    }
    localObject1 = a;
    if ((localObject1 != null) && ((TextUtils.isEmpty(((String)localObject1).trim())) || (a.contains("#")) || (a.contains("*"))))
    {
      localObject1 = new StringBuilder("Bad path, '");
      ((StringBuilder)localObject1).append(a);
      ((StringBuilder)localObject1).append("'");
      throw new IllegalStateException(((StringBuilder)localObject1).toString());
    }
    a(e);
    a(q);
    Object localObject2 = a;
    localObject1 = localObject2;
    if (localObject2 == null) {
      localObject1 = o;
    }
    localObject2 = localObject1;
    if (d)
    {
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append("/count");
      localObject2 = ((StringBuilder)localObject2).toString();
    }
    localObject1 = localObject2;
    if (b)
    {
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append((String)localObject2);
      ((StringBuilder)localObject1).append("/#");
      localObject1 = ((StringBuilder)localObject1).toString();
    }
    if (b)
    {
      localObject2 = new StringBuilder("vnd.");
      ((StringBuilder)localObject2).append(n);
      ((StringBuilder)localObject2).append(".cursor.item/");
      ((StringBuilder)localObject2).append(o);
      localObject2 = ((StringBuilder)localObject2).toString();
    }
    else
    {
      localObject2 = new StringBuilder("vnd.");
      ((StringBuilder)localObject2).append(n);
      ((StringBuilder)localObject2).append(".cursor.dir/");
      ((StringBuilder)localObject2).append(o);
      localObject2 = ((StringBuilder)localObject2).toString();
    }
    Object localObject3 = r;
    boolean bool2;
    if (localObject3 == null) {
      bool2 = true;
    } else {
      bool2 = ((Boolean)localObject3).booleanValue();
    }
    localObject3 = s;
    boolean bool1;
    if (localObject3 == null)
    {
      if ((!d) && (!c)) {
        bool1 = true;
      } else {
        bool1 = false;
      }
    }
    else {
      bool1 = ((Boolean)localObject3).booleanValue();
    }
    localObject3 = m;
    int i1 = p;
    localObject2 = new a(i1, o, e, q, bool2, bool1, b, d, (String)localObject2, b(), localHashSet, f, g, h, i, u, j, k, l);
    if (a.indexOfKey(i1) < 0)
    {
      a.put(i1, localObject2);
      m.b.addURI(n, (String)localObject1, p);
      return m;
    }
    localObject1 = new StringBuilder("Duplicated match, previous match=");
    ((StringBuilder)localObject1).append(a.get(i1));
    ((StringBuilder)localObject1).append(", replaced by match=");
    ((StringBuilder)localObject1).append(localObject2);
    throw new IllegalArgumentException(((StringBuilder)localObject1).toString());
  }
  
  public final Uri b()
  {
    Object localObject2 = a;
    Object localObject1 = localObject2;
    if (localObject2 == null) {
      localObject1 = o;
    }
    localObject2 = new StringBuilder("content://");
    ((StringBuilder)localObject2).append(n);
    ((StringBuilder)localObject2).append("/");
    ((StringBuilder)localObject2).append((String)localObject1);
    return Uri.parse(((StringBuilder)localObject2).toString());
  }
  
  public final b b(boolean paramBoolean)
  {
    r = Boolean.valueOf(paramBoolean);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */