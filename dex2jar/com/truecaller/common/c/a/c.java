package com.truecaller.common.c.a;

import android.content.UriMatcher;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.util.SparseArray;

public final class c
{
  public final Uri a;
  public final e b;
  private final SparseArray<a> c;
  private final UriMatcher d;
  
  protected c(Uri paramUri, SparseArray<a> paramSparseArray, UriMatcher paramUriMatcher, e parame)
  {
    a = paramUri;
    c = paramSparseArray;
    d = paramUriMatcher;
    b = parame;
  }
  
  public final a a(Uri paramUri)
  {
    return (a)c.get(d.match(paramUri), null);
  }
  
  public final a b(Uri paramUri)
  {
    a locala = a(paramUri);
    if (locala != null) {
      return locala;
    }
    throw new SQLiteException("Unsupported uri, uri=".concat(String.valueOf(paramUri)));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */