package com.truecaller.common.c.a;

import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.text.TextUtils;
import java.util.Set;

public final class a
{
  public final int a;
  public final int b;
  public final boolean c;
  public final boolean d;
  public final boolean e;
  public final boolean f;
  public final String g;
  public final String h;
  public final Uri i;
  public final Set<Uri> j;
  public final g k;
  public final f l;
  public final h m;
  public final e n;
  public final d o;
  public final b p;
  public final c q;
  public final a r;
  private final int s;
  
  protected a(int paramInt1, String paramString1, int paramInt2, int paramInt3, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, String paramString2, Uri paramUri, Set<Uri> paramSet, g paramg, f paramf, h paramh, e parame, d paramd, b paramb, c paramc, a parama)
  {
    s = paramInt1;
    g = paramString1;
    a = paramInt2;
    b = paramInt3;
    c = paramBoolean1;
    d = paramBoolean2;
    e = paramBoolean3;
    f = paramBoolean4;
    h = paramString2;
    i = paramUri;
    j = paramSet;
    k = paramg;
    l = paramf;
    m = paramh;
    n = parame;
    o = paramd;
    p = paramb;
    q = paramc;
    r = parama;
  }
  
  public final Uri a(long paramLong)
  {
    return ContentUris.withAppendedId(i, paramLong);
  }
  
  public final String a()
  {
    return g;
  }
  
  public final Uri b()
  {
    return i;
  }
  
  public final boolean equals(Object paramObject)
  {
    if ((paramObject instanceof a))
    {
      if (paramObject == this) {
        return true;
      }
      paramObject = (a)paramObject;
      return (s == s) && (TextUtils.equals(h, h)) && (TextUtils.equals(g, g));
    }
    return false;
  }
  
  public final int hashCode()
  {
    return s + h.hashCode() * 13 + g.hashCode() * 27;
  }
  
  public final String toString()
  {
    return String.format("{match=0x%08X, table=%s, type=%s, alsoNotify=%s, r=%b, w=%b, c=%b}", new Object[] { Integer.valueOf(s), g, h, j, Boolean.valueOf(c), Boolean.valueOf(d), Boolean.valueOf(f) });
  }
  
  public static abstract interface a
  {
    public abstract int a(com.truecaller.common.c.a parama, a parama1, Uri paramUri, String paramString, String[] paramArrayOfString, int paramInt);
  }
  
  public static abstract interface b
  {
    public abstract Uri a(com.truecaller.common.c.a parama, Uri paramUri1, ContentValues paramContentValues, Uri paramUri2);
  }
  
  public static abstract interface c
  {
    public abstract int a(com.truecaller.common.c.a parama, a parama1, Uri paramUri, ContentValues paramContentValues, int paramInt);
  }
  
  public static abstract interface d
  {
    public abstract Bundle a();
  }
  
  public static abstract interface e
  {
    public abstract int a(com.truecaller.common.c.a parama, a parama1, Uri paramUri, String paramString, String[] paramArrayOfString);
  }
  
  public static abstract interface f
  {
    public abstract Uri a(com.truecaller.common.c.a parama, a parama1, Uri paramUri, ContentValues paramContentValues);
  }
  
  public static abstract interface g
  {
    public abstract Cursor a(com.truecaller.common.c.a parama, a parama1, Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal);
  }
  
  public static abstract interface h
  {
    public abstract int a(com.truecaller.common.c.a parama, a parama1, Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */