package com.truecaller.common.c;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.CancellationSignal;
import java.util.ArrayList;

public abstract class d
  extends ContentProvider
{
  private b a = a.a;
  
  protected abstract int a(Uri paramUri, ContentValues[] paramArrayOfContentValues);
  
  protected abstract Cursor a(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal);
  
  protected Bundle a(String paramString1, String paramString2, Bundle paramBundle)
  {
    return super.call(paramString1, paramString2, paramBundle);
  }
  
  protected abstract ContentProviderResult[] a(ArrayList<ContentProviderOperation> paramArrayList)
    throws OperationApplicationException;
  
  public final ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> paramArrayList)
    throws OperationApplicationException
  {
    try
    {
      paramArrayList = a(paramArrayList);
      return paramArrayList;
    }
    finally {}
  }
  
  protected abstract int b(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString);
  
  protected abstract int b(Uri paramUri, String paramString, String[] paramArrayOfString);
  
  protected abstract Uri b(Uri paramUri, ContentValues paramContentValues);
  
  public final int bulkInsert(Uri paramUri, ContentValues[] paramArrayOfContentValues)
  {
    try
    {
      int i = a(paramUri, paramArrayOfContentValues);
      return i;
    }
    finally {}
  }
  
  public final Bundle call(String paramString1, String paramString2, Bundle paramBundle)
  {
    try
    {
      paramString1 = a(paramString1, paramString2, paramBundle);
      return paramString1;
    }
    finally {}
  }
  
  public final int delete(Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    try
    {
      int i = b(paramUri, paramString, paramArrayOfString);
      return i;
    }
    finally {}
  }
  
  public final Uri insert(Uri paramUri, ContentValues paramContentValues)
  {
    try
    {
      paramUri = b(paramUri, paramContentValues);
      return paramUri;
    }
    finally {}
  }
  
  public final Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
  {
    return query(paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, paramString2, null);
  }
  
  public Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal)
  {
    try
    {
      paramUri = a(paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, paramString2, paramCancellationSignal);
      return paramUri;
    }
    finally {}
  }
  
  public final int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    try
    {
      int i = b(paramUri, paramContentValues, paramString, paramArrayOfString);
      return i;
    }
    finally {}
  }
  
  static final class a
    implements d.b
  {
    static final d.b a = new a();
  }
  
  static abstract interface b {}
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */