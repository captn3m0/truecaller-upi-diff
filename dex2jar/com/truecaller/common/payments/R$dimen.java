package com.truecaller.common.payments;

public final class R$dimen
{
  public static final int compat_button_inset_horizontal_material = 2131165424;
  public static final int compat_button_inset_vertical_material = 2131165425;
  public static final int compat_button_padding_horizontal_material = 2131165426;
  public static final int compat_button_padding_vertical_material = 2131165427;
  public static final int compat_control_corner_material = 2131165428;
  public static final int notification_action_icon_size = 2131165984;
  public static final int notification_action_text_size = 2131165985;
  public static final int notification_big_circle_margin = 2131165987;
  public static final int notification_content_margin_start = 2131165988;
  public static final int notification_large_icon_height = 2131165989;
  public static final int notification_large_icon_width = 2131165990;
  public static final int notification_main_column_padding_top = 2131165991;
  public static final int notification_media_narrow_margin = 2131165992;
  public static final int notification_right_icon_size = 2131165993;
  public static final int notification_right_side_padding_top = 2131165994;
  public static final int notification_small_icon_background_padding = 2131165995;
  public static final int notification_small_icon_size_as_large = 2131165996;
  public static final int notification_subtext_size = 2131165997;
  public static final int notification_top_pad = 2131165998;
  public static final int notification_top_pad_large_text = 2131165999;
}

/* Location:
 * Qualified Name:     com.truecaller.common.payments.R.dimen
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */