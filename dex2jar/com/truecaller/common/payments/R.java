package com.truecaller.common.payments;

public final class R
{
  public static final class attr
  {
    public static final int font = 2130968998;
    public static final int fontProviderAuthority = 2130969000;
    public static final int fontProviderCerts = 2130969001;
    public static final int fontProviderFetchStrategy = 2130969002;
    public static final int fontProviderFetchTimeout = 2130969003;
    public static final int fontProviderPackage = 2130969004;
    public static final int fontProviderQuery = 2130969005;
    public static final int fontStyle = 2130969006;
    public static final int fontWeight = 2130969008;
  }
  
  public static final class bool
  {
    public static final int abc_action_bar_embed_tabs = 2131034113;
  }
  
  public static final class color
  {
    public static final int notification_action_color_filter = 2131100296;
    public static final int notification_icon_bg_color = 2131100298;
    public static final int ripple_material_light = 2131100477;
    public static final int secondary_text_default_material_light = 2131100488;
  }
  
  public static final class dimen
  {
    public static final int compat_button_inset_horizontal_material = 2131165424;
    public static final int compat_button_inset_vertical_material = 2131165425;
    public static final int compat_button_padding_horizontal_material = 2131165426;
    public static final int compat_button_padding_vertical_material = 2131165427;
    public static final int compat_control_corner_material = 2131165428;
    public static final int notification_action_icon_size = 2131165984;
    public static final int notification_action_text_size = 2131165985;
    public static final int notification_big_circle_margin = 2131165987;
    public static final int notification_content_margin_start = 2131165988;
    public static final int notification_large_icon_height = 2131165989;
    public static final int notification_large_icon_width = 2131165990;
    public static final int notification_main_column_padding_top = 2131165991;
    public static final int notification_media_narrow_margin = 2131165992;
    public static final int notification_right_icon_size = 2131165993;
    public static final int notification_right_side_padding_top = 2131165994;
    public static final int notification_small_icon_background_padding = 2131165995;
    public static final int notification_small_icon_size_as_large = 2131165996;
    public static final int notification_subtext_size = 2131165997;
    public static final int notification_top_pad = 2131165998;
    public static final int notification_top_pad_large_text = 2131165999;
  }
  
  public static final class drawable
  {
    public static final int notification_action_background = 2131234779;
    public static final int notification_bg = 2131234780;
    public static final int notification_bg_low = 2131234781;
    public static final int notification_bg_low_normal = 2131234782;
    public static final int notification_bg_low_pressed = 2131234783;
    public static final int notification_bg_normal = 2131234784;
    public static final int notification_bg_normal_pressed = 2131234785;
    public static final int notification_icon_background = 2131234786;
    public static final int notification_template_icon_bg = 2131234788;
    public static final int notification_template_icon_low_bg = 2131234789;
    public static final int notification_tile_bg = 2131234790;
    public static final int notify_panel_notification_icon_bg = 2131234791;
  }
  
  public static final class id
  {
    public static final int action_container = 2131361851;
    public static final int action_divider = 2131361862;
    public static final int action_image = 2131361871;
    public static final int action_text = 2131361938;
    public static final int actions = 2131361945;
    public static final int async = 2131362051;
    public static final int blocking = 2131362124;
    public static final int chronometer = 2131362468;
    public static final int forever = 2131363142;
    public static final int icon = 2131363301;
    public static final int icon_group = 2131363304;
    public static final int info = 2131363451;
    public static final int italic = 2131363495;
    public static final int line1 = 2131363637;
    public static final int line3 = 2131363638;
    public static final int normal = 2131363813;
    public static final int notification_background = 2131363818;
    public static final int notification_main_column = 2131363820;
    public static final int notification_main_column_container = 2131363821;
    public static final int right_icon = 2131364178;
    public static final int right_side = 2131364179;
    public static final int text = 2131364681;
    public static final int text2 = 2131364683;
    public static final int time = 2131364859;
    public static final int title = 2131364884;
  }
  
  public static final class integer
  {
    public static final int status_bar_notification_info_maxnum = 2131427351;
  }
  
  public static final class layout
  {
    public static final int notification_action = 2131559045;
    public static final int notification_action_tombstone = 2131559046;
    public static final int notification_template_custom_big = 2131559055;
    public static final int notification_template_icon_group = 2131559056;
    public static final int notification_template_part_chronometer = 2131559060;
    public static final int notification_template_part_time = 2131559061;
  }
  
  public static final class string
  {
    public static final int status_bar_notification_info_overflow = 2131888855;
  }
  
  public static final class style
  {
    public static final int TextAppearance_Compat_Notification = 2131952010;
    public static final int TextAppearance_Compat_Notification_Info = 2131952011;
    public static final int TextAppearance_Compat_Notification_Line2 = 2131952013;
    public static final int TextAppearance_Compat_Notification_Time = 2131952016;
    public static final int TextAppearance_Compat_Notification_Title = 2131952018;
    public static final int Widget_Compat_NotificationActionContainer = 2131952288;
    public static final int Widget_Compat_NotificationActionText = 2131952289;
  }
  
  public static final class styleable
  {
    public static final int[] FontFamily = { 2130969000, 2130969001, 2130969002, 2130969003, 2130969004, 2130969005 };
    public static final int[] FontFamilyFont = { 16844082, 16844083, 16844095, 16844143, 16844144, 2130968998, 2130969006, 2130969007, 2130969008, 2130969645 };
    public static final int FontFamilyFont_android_font = 0;
    public static final int FontFamilyFont_android_fontStyle = 2;
    public static final int FontFamilyFont_android_fontVariationSettings = 4;
    public static final int FontFamilyFont_android_fontWeight = 1;
    public static final int FontFamilyFont_android_ttcIndex = 3;
    public static final int FontFamilyFont_font = 5;
    public static final int FontFamilyFont_fontStyle = 6;
    public static final int FontFamilyFont_fontVariationSettings = 7;
    public static final int FontFamilyFont_fontWeight = 8;
    public static final int FontFamilyFont_ttcIndex = 9;
    public static final int FontFamily_fontProviderAuthority = 0;
    public static final int FontFamily_fontProviderCerts = 1;
    public static final int FontFamily_fontProviderFetchStrategy = 2;
    public static final int FontFamily_fontProviderFetchTimeout = 3;
    public static final int FontFamily_fontProviderPackage = 4;
    public static final int FontFamily_fontProviderQuery = 5;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.payments.R
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */