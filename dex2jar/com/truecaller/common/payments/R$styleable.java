package com.truecaller.common.payments;

public final class R$styleable
{
  public static final int[] FontFamily = { 2130969000, 2130969001, 2130969002, 2130969003, 2130969004, 2130969005 };
  public static final int[] FontFamilyFont = { 16844082, 16844083, 16844095, 16844143, 16844144, 2130968998, 2130969006, 2130969007, 2130969008, 2130969645 };
  public static final int FontFamilyFont_android_font = 0;
  public static final int FontFamilyFont_android_fontStyle = 2;
  public static final int FontFamilyFont_android_fontVariationSettings = 4;
  public static final int FontFamilyFont_android_fontWeight = 1;
  public static final int FontFamilyFont_android_ttcIndex = 3;
  public static final int FontFamilyFont_font = 5;
  public static final int FontFamilyFont_fontStyle = 6;
  public static final int FontFamilyFont_fontVariationSettings = 7;
  public static final int FontFamilyFont_fontWeight = 8;
  public static final int FontFamilyFont_ttcIndex = 9;
  public static final int FontFamily_fontProviderAuthority = 0;
  public static final int FontFamily_fontProviderCerts = 1;
  public static final int FontFamily_fontProviderFetchStrategy = 2;
  public static final int FontFamily_fontProviderFetchTimeout = 3;
  public static final int FontFamily_fontProviderPackage = 4;
  public static final int FontFamily_fontProviderQuery = 5;
}

/* Location:
 * Qualified Name:     com.truecaller.common.payments.R.styleable
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */