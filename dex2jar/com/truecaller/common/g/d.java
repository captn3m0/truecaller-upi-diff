package com.truecaller.common.g;

import android.content.SharedPreferences;
import javax.inject.Provider;

public final class d
  implements dagger.a.d<a>
{
  private final c a;
  private final Provider<SharedPreferences> b;
  
  private d(c paramc, Provider<SharedPreferences> paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static d a(c paramc, Provider<SharedPreferences> paramProvider)
  {
    return new d(paramc, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.g.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */