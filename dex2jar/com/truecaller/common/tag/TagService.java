package com.truecaller.common.tag;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.af;
import com.truecaller.common.R.id;
import com.truecaller.common.account.r;

public class TagService
  extends af
{
  public static void a(Context paramContext)
  {
    Intent localIntent = new Intent();
    localIntent.putExtra("action", 0);
    a(paramContext.getApplicationContext(), TagService.class, R.id.tag_service_job_id, localIntent);
  }
  
  public final void a(Intent paramIntent)
  {
    if (!com.truecaller.common.b.a.F().u().k().c()) {
      return;
    }
    if (paramIntent.getIntExtra("action", -1) != 0) {
      return;
    }
    d.a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.tag.TagService
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */