package com.truecaller.common.tag.sync;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import androidx.work.c.a;
import androidx.work.g;
import androidx.work.j;
import androidx.work.k.a;
import androidx.work.p;
import com.truecaller.common.account.r;
import com.truecaller.common.tag.d;
import javax.inject.Inject;

public final class TagsUploadWorker
  extends Worker
{
  public static final TagsUploadWorker.a c = new TagsUploadWorker.a((byte)0);
  @Inject
  public r b;
  private final Context d;
  
  public TagsUploadWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    d = paramContext;
    paramContext = com.truecaller.common.b.a.F();
    c.g.b.k.a(paramContext, "ApplicationBase.getAppBase()");
    paramContext.u().a(this);
  }
  
  public static final void b()
  {
    p.a().a("TagsUploadWorker", g.a, (androidx.work.k)((k.a)new k.a(TagsUploadWorker.class).a(new c.a().a(j.b).a())).c());
  }
  
  public final ListenableWorker.a a()
  {
    Object localObject = b;
    if (localObject == null) {
      c.g.b.k.a("accountManager");
    }
    if (!((r)localObject).c())
    {
      localObject = ListenableWorker.a.a();
      c.g.b.k.a(localObject, "Result.success()");
      return (ListenableWorker.a)localObject;
    }
    boolean bool1 = d.d(d);
    boolean bool2 = d.e(d);
    if ((bool1) && (bool2))
    {
      localObject = ListenableWorker.a.a();
      c.g.b.k.a(localObject, "Result.success()");
      return (ListenableWorker.a)localObject;
    }
    localObject = ListenableWorker.a.b();
    c.g.b.k.a(localObject, "Result.retry()");
    return (ListenableWorker.a)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.tag.sync.TagsUploadWorker
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */