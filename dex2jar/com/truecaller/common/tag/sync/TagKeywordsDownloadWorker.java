package com.truecaller.common.tag.sync;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import androidx.work.p;
import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.common.account.r;
import com.truecaller.common.b.e;
import com.truecaller.common.background.TrackedWorker;
import com.truecaller.common.h.ac;
import com.truecaller.common.tag.d;
import javax.inject.Inject;

public final class TagKeywordsDownloadWorker
  extends TrackedWorker
{
  public static final TagKeywordsDownloadWorker.a e = new TagKeywordsDownloadWorker.a((byte)0);
  @Inject
  public b b;
  @Inject
  public r c;
  @Inject
  public ac d;
  private final Context f;
  
  public TagKeywordsDownloadWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    f = paramContext;
    paramContext = com.truecaller.common.b.a.F();
    k.a(paramContext, "ApplicationBase.getAppBase()");
    paramContext.u().a(this);
  }
  
  public static final void e()
  {
    p localp = p.a();
    k.a(localp, "WorkManager.getInstance()");
    localp.a("TagKeywordsDownloadWorkerOneOff", androidx.work.g.a, e.a().b());
  }
  
  public final b b()
  {
    b localb = b;
    if (localb == null) {
      k.a("analytics");
    }
    return localb;
  }
  
  public final boolean c()
  {
    Object localObject = c;
    if (localObject == null) {
      k.a("accountManager");
    }
    if ((((r)localObject).c()) && (e.a("featureAutoTagging", false)))
    {
      localObject = d;
      if (localObject == null) {
        k.a("regionUtils");
      }
      if (!((ac)localObject).a()) {
        return true;
      }
    }
    return false;
  }
  
  public final ListenableWorker.a d()
  {
    if (d.c(f))
    {
      int i;
      if (e.a("tagsKeywordsFeatureCurrentVersion", 0L) != e.a("tagsKeywordsFeatureLastVersion", 0L)) {
        i = 1;
      } else {
        i = 0;
      }
      if (i != 0) {
        e.b("tagsPhonebookForcedUpload", true);
      }
      e.b("tagsKeywordsFeatureLastVersion", e.a("tagsKeywordsFeatureCurrentVersion", 0L));
      locala = ListenableWorker.a.a();
      k.a(locala, "Result.success()");
      return locala;
    }
    ListenableWorker.a locala = ListenableWorker.a.b();
    k.a(locala, "Result.retry()");
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.tag.sync.TagKeywordsDownloadWorker
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */