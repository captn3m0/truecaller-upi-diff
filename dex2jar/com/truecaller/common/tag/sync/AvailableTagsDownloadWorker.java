package com.truecaller.common.tag.sync;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.common.account.r;
import com.truecaller.common.background.TrackedWorker;
import com.truecaller.common.tag.d;
import javax.inject.Inject;

public final class AvailableTagsDownloadWorker
  extends TrackedWorker
{
  public static final AvailableTagsDownloadWorker.a d = new AvailableTagsDownloadWorker.a((byte)0);
  @Inject
  public b b;
  @Inject
  public r c;
  private final Context e;
  
  public AvailableTagsDownloadWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    e = paramContext;
    paramContext = com.truecaller.common.b.a.F();
    k.a(paramContext, "ApplicationBase.getAppBase()");
    paramContext.u().a(this);
  }
  
  public static final void e()
  {
    d.b();
  }
  
  public final b b()
  {
    b localb = b;
    if (localb == null) {
      k.a("analytics");
    }
    return localb;
  }
  
  public final boolean c()
  {
    r localr = c;
    if (localr == null) {
      k.a("accountManager");
    }
    return localr.c();
  }
  
  public final ListenableWorker.a d()
  {
    ListenableWorker.a locala;
    if (d.b(e)) {
      locala = ListenableWorker.a.a();
    } else {
      locala = ListenableWorker.a.b();
    }
    k.a(locala, "if (TagManager.fetchAvai…ess() else Result.retry()");
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.tag.sync.AvailableTagsDownloadWorker
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */