package com.truecaller.common.tag.network;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
public @interface TagRestModel$Context
{
  public static final int AFTER_CALL = 1;
  public static final int BUSINESS_PROFILE = 4;
  public static final int DETAIL_VIEW = 2;
  public static final int NONE = 0;
  public static final int PROFILE = 3;
}

/* Location:
 * Qualified Name:     com.truecaller.common.tag.network.TagRestModel.Context
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */