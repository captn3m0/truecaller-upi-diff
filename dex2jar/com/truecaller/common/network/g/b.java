package com.truecaller.common.network.g;

import c.g.b.k;
import com.google.gson.a.c;

public final class b
{
  @c(a="BACKUP_STATUS")
  public final String a;
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject) {
      if ((paramObject instanceof b))
      {
        paramObject = (b)paramObject;
        if (k.a(a, a)) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    String str = a;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("EnhancedSearchBackupService(backupStatus=");
    localStringBuilder.append(a);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.g.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */