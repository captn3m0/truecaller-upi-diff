package com.truecaller.common.network.g;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class c
{
  public static final String a;
  private static final Map<String, String> b;
  private static final Map<String, String> c;
  
  static
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(c.class.getName());
    localStringBuilder.append("#ACTION_PROFILE_REFRESHED");
    a = localStringBuilder.toString();
    b = new HashMap();
    c = new HashMap();
    b.put("profileFirstName", "first_name");
    b.put("profileLastName", "last_name");
    b.put("profileNumber", "phone_number");
    b.put("profileNationalNumber", "national_number");
    b.put("profileStatus", "status_message");
    b.put("profileCity", "city");
    b.put("profileStreet", "street");
    b.put("profileZip", "zipcode");
    b.put("profileEmail", "email");
    b.put("profileWeb", "url");
    b.put("profileFacebook", "facebook_id");
    b.put("profileTwitter", "twitter_id");
    b.put("profileGender", "gender");
    b.put("profileAvatar", "avatar_url");
    b.put("profileCompanyName", "w_company");
    b.put("profileCompanyJob", "w_title");
    b.put("profileAcceptAuto", "auto_accept");
    b.put("profileTag", "tag");
    c.put("profileBusiness", "w_is_business_number");
  }
  
  private c()
  {
    throw new AssertionError();
  }
  
  public static String a(String paramString)
  {
    return (String)b.get(paramString);
  }
  
  public static <T> void a(Map<String, T> paramMap1, Map<String, T> paramMap2)
  {
    Iterator localIterator = paramMap1.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      String str = (String)b.get(localEntry.getKey());
      paramMap1 = str;
      if (str == null) {
        paramMap1 = (String)c.get(localEntry.getKey());
      }
      if (paramMap1 != null) {
        paramMap2.put(paramMap1, localEntry.getValue());
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.g.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */