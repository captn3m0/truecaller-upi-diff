package com.truecaller.common.network;

public enum KnownDomain
{
  private final String value;
  
  static
  {
    KnownDomain localKnownDomain1 = new KnownDomain("DOMAIN_REGION_1", 0, "eu");
    DOMAIN_REGION_1 = localKnownDomain1;
    KnownDomain localKnownDomain2 = new KnownDomain("DOMAIN_OTHER_REGIONS", 1, "noneu");
    DOMAIN_OTHER_REGIONS = localKnownDomain2;
    $VALUES = new KnownDomain[] { localKnownDomain1, localKnownDomain2 };
  }
  
  private KnownDomain(String paramString)
  {
    value = paramString;
  }
  
  public final String getValue()
  {
    return value;
  }
  
  public final String toString()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.KnownDomain
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */