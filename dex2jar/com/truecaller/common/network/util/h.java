package com.truecaller.common.network.util;

import c.g.b.k;

public final class h
{
  public static final h a = new h();
  
  public static final <T> T a(KnownEndpoints paramKnownEndpoints, Class<T> paramClass)
  {
    k.b(paramKnownEndpoints, "endpoint");
    k.b(paramClass, "api");
    return (T)new a().a(paramKnownEndpoints).a(paramClass).b(paramClass);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.util.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */