package com.truecaller.common.network.util;

public enum AuthRequirement
{
  static
  {
    AuthRequirement localAuthRequirement1 = new AuthRequirement("REQUIRED", 0);
    REQUIRED = localAuthRequirement1;
    AuthRequirement localAuthRequirement2 = new AuthRequirement("OPTIONAL", 1);
    OPTIONAL = localAuthRequirement2;
    AuthRequirement localAuthRequirement3 = new AuthRequirement("NONE", 2);
    NONE = localAuthRequirement3;
    $VALUES = new AuthRequirement[] { localAuthRequirement1, localAuthRequirement2, localAuthRequirement3 };
  }
  
  private AuthRequirement() {}
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.util.AuthRequirement
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */