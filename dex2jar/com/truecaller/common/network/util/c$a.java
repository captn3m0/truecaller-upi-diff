package com.truecaller.common.network.util;

import c.d.a.a;
import c.d.b.a.f;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import java.net.InetAddress;
import kotlinx.coroutines.ag;

@f(b="DnsUtil.kt", c={}, d="invokeSuspend", e="com.truecaller.common.network.util.DnsUtil$lookupDnsSuspended$2")
final class c$a
  extends c.d.b.a.k
  implements m<ag, c<? super Object>, Object>
{
  int a;
  private ag c;
  
  c$a(String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new a(b, paramc);
    c = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    if ((a != 0) || (!(paramObject instanceof o.b))) {}
    try
    {
      paramObject = InetAddress.getAllByName(b);
      return paramObject;
    }
    catch (Exception paramObject)
    {
      for (;;) {}
    }
    return x.a;
    throw a;
    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((a)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.util.c.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */