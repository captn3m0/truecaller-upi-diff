package com.truecaller.common.network.util;

import c.g.b.k;
import c.l;
import com.truecaller.common.network.KnownDomain;
import okhttp3.u;
import okhttp3.u.a;

public enum KnownEndpoints
{
  public static final a Companion = new a((byte)0);
  private static boolean isStaging;
  private final String euHost;
  private final String key;
  private final String nonEuHost;
  private final String stagingEndpoint;
  private final Integer stagingPort;
  
  static
  {
    KnownEndpoints localKnownEndpoints1 = new KnownEndpoints("SEARCH", 0, "search5", "search5-eu", "search5-noneu", null, Integer.valueOf(16080), 8, null);
    SEARCH = localKnownEndpoints1;
    KnownEndpoints localKnownEndpoints2 = new KnownEndpoints("REQUEST", 1, "request3", "request3-eu", "request3-noneu", null, null, 24, null);
    REQUEST = localKnownEndpoints2;
    KnownEndpoints localKnownEndpoints3 = new KnownEndpoints("CONTACTREQUEST", 2, "contact-request", "contact-request-eu", "contact-request-noneu", null, null, 24, null);
    CONTACTREQUEST = localKnownEndpoints3;
    KnownEndpoints localKnownEndpoints4 = new KnownEndpoints("PREMIUM", 3, "premium", "premium-eu", "premium-noneu", null, null, 24, null);
    PREMIUM = localKnownEndpoints4;
    KnownEndpoints localKnownEndpoints5 = new KnownEndpoints("CONTACT", 4, "contact-upload4", "contact-upload4-eu", "contact-upload4-noneu", null, Integer.valueOf(19010), 8, null);
    CONTACT = localKnownEndpoints5;
    KnownEndpoints localKnownEndpoints6 = new KnownEndpoints("NOTIFICATION", 5, "notifications5", "notifications5-eu", "notifications5-noneu", null, Integer.valueOf(19000), 8, null);
    NOTIFICATION = localKnownEndpoints6;
    KnownEndpoints localKnownEndpoints7 = new KnownEndpoints("BATCHLOG", 6, "batchlogging4", "batchlogging4-eu", "batchlogging4-noneu", null, Integer.valueOf(23250), 8, null);
    BATCHLOG = localKnownEndpoints7;
    KnownEndpoints localKnownEndpoints8 = new KnownEndpoints("PHONEBOOK", 7, "phonebook5", "phonebook5-eu", "phonebook5-noneu", null, Integer.valueOf(28220), 8, null);
    PHONEBOOK = localKnownEndpoints8;
    KnownEndpoints localKnownEndpoints9 = new KnownEndpoints("TAGGING", 8, "tagging5", "tagging5-eu", "tagging5-noneu", null, Integer.valueOf(20150), 8, null);
    TAGGING = localKnownEndpoints9;
    KnownEndpoints localKnownEndpoints10 = new KnownEndpoints("FILTER", 9, "filter-store4", "filter-store4-eu", "filter-store4-noneu", null, Integer.valueOf(16010), 8, null);
    FILTER = localKnownEndpoints10;
    KnownEndpoints localKnownEndpoints11 = new KnownEndpoints("EDGE", 10, "edge-locations5", "edge-locations5-eu", "edge-locations5-noneu", null, Integer.valueOf(19050), 8, null);
    EDGE = localKnownEndpoints11;
    KnownEndpoints localKnownEndpoints12 = new KnownEndpoints("FEEDBACK", 11, "feedback", "feedback-eu", "feedback-noneu", null, null, 24, null);
    FEEDBACK = localKnownEndpoints12;
    KnownEndpoints localKnownEndpoints13 = new KnownEndpoints("API", 12, "api4", "api4-eu", "api4-noneu", null, Integer.valueOf(17010), 8, null);
    API = localKnownEndpoints13;
    KnownEndpoints localKnownEndpoints14 = new KnownEndpoints("ADS", 13, "ads5", "ads5-eu", "ads5-noneu", null, Integer.valueOf(19060), 8, null);
    ADS = localKnownEndpoints14;
    KnownEndpoints localKnownEndpoints15 = new KnownEndpoints("CALLMEBACK", 14, "callmeback", "callmeback-eu", "callmeback-noneu", null, Integer.valueOf(16020), 8, null);
    CALLMEBACK = localKnownEndpoints15;
    KnownEndpoints localKnownEndpoints16 = new KnownEndpoints("USERAPPS", 15, "userapps", "userapps-eu", "userapps-noneu", null, Integer.valueOf(16050), 8, null);
    USERAPPS = localKnownEndpoints16;
    KnownEndpoints localKnownEndpoints17 = new KnownEndpoints("REFERRAL", 16, "referrals", "referrals-eu", "referrals-noneu", null, Integer.valueOf(19080), 8, null);
    REFERRAL = localKnownEndpoints17;
    KnownEndpoints localKnownEndpoints18 = new KnownEndpoints("PROFILE", 17, "profile4", "profile4-eu", "profile4-noneu", null, null, 24, null);
    PROFILE = localKnownEndpoints18;
    KnownEndpoints localKnownEndpoints19 = new KnownEndpoints("LEADGEN", 18, "leadgen", "leadgen-eu", "leadgen-noneu", null, Integer.valueOf(16350), 8, null);
    LEADGEN = localKnownEndpoints19;
    KnownEndpoints localKnownEndpoints20 = new KnownEndpoints("BACKUP", 19, "backup", "backup-eu", "backup-noneu", null, null, 24, null);
    BACKUP = localKnownEndpoints20;
    KnownEndpoints localKnownEndpoints21 = new KnownEndpoints("TOPSPAMMERS", 20, "topspammers", "topspammers-eu", "topspammers-noneu", null, Integer.valueOf(16610), 8, null);
    TOPSPAMMERS = localKnownEndpoints21;
    KnownEndpoints localKnownEndpoints22 = new KnownEndpoints("PUSHID", 21, "pushid", "pushid-eu", "pushid-noneu", null, null, 24, null);
    PUSHID = localKnownEndpoints22;
    KnownEndpoints localKnownEndpoints23 = new KnownEndpoints("IMAGES", 22, "images", "images-eu", "images-noneu", null, null, 24, null);
    IMAGES = localKnownEndpoints23;
    KnownEndpoints localKnownEndpoints24 = new KnownEndpoints("USERARCHIVE", 23, "user-archive", "user-archive-eu", "user-archive-noneu", null, null, 24, null);
    USERARCHIVE = localKnownEndpoints24;
    KnownEndpoints localKnownEndpoints25 = new KnownEndpoints("ACCOUNT", 24, "account", "account-eu", "account-noneu", null, Integer.valueOf(18870), 8, null);
    ACCOUNT = localKnownEndpoints25;
    KnownEndpoints localKnownEndpoints26 = new KnownEndpoints("FLASH", 25, "flash", "flash-eu", "flash-noneu", null, null, 24, null);
    FLASH = localKnownEndpoints26;
    KnownEndpoints localKnownEndpoints27 = new KnownEndpoints("OPTOUT", 26, "opt-out", "opt-out-eu", "opt-out-noneu", null, null, 24, null);
    OPTOUT = localKnownEndpoints27;
    KnownEndpoints localKnownEndpoints28 = new KnownEndpoints("PRESENCE_GRPC", 27, "presence-grpc", "presence-grpc-eu", "presence-grpc-noneu", null, null, 24, null);
    PRESENCE_GRPC = localKnownEndpoints28;
    KnownEndpoints localKnownEndpoints29 = new KnownEndpoints("PROFILE_VIEW", 28, "profile-view", "profile-view-eu", "profile-view-noneu", null, null, 24, null);
    PROFILE_VIEW = localKnownEndpoints29;
    KnownEndpoints localKnownEndpoints30 = new KnownEndpoints("SPAM_URL", 29, "link-reports", "link-reports-eu", "link-reports-noneu", null, null, 24, null);
    SPAM_URL = localKnownEndpoints30;
    KnownEndpoints localKnownEndpoints31 = new KnownEndpoints("MESSENGER", 30, "messenger", "messenger-eu", "messenger-noneu", null, null, 24, null);
    MESSENGER = localKnownEndpoints31;
    KnownEndpoints localKnownEndpoints32 = new KnownEndpoints("CLIENT_SEARCH", 31, "client-search", "client-search-eu", "client-search-noneu", null, null, 24, null);
    CLIENT_SEARCH = localKnownEndpoints32;
    KnownEndpoints localKnownEndpoints33 = new KnownEndpoints("LAST_ACTIVITY", 32, "lastactivity", "lastactivity-eu", "lastactivity-noneu", null, null, 24, null);
    LAST_ACTIVITY = localKnownEndpoints33;
    KnownEndpoints localKnownEndpoints34 = new KnownEndpoints("VOIP", 33, "voip", "voip-eu", "voip-noneu", null, null, 24, null);
    VOIP = localKnownEndpoints34;
    $VALUES = new KnownEndpoints[] { localKnownEndpoints1, localKnownEndpoints2, localKnownEndpoints3, localKnownEndpoints4, localKnownEndpoints5, localKnownEndpoints6, localKnownEndpoints7, localKnownEndpoints8, localKnownEndpoints9, localKnownEndpoints10, localKnownEndpoints11, localKnownEndpoints12, localKnownEndpoints13, localKnownEndpoints14, localKnownEndpoints15, localKnownEndpoints16, localKnownEndpoints17, localKnownEndpoints18, localKnownEndpoints19, localKnownEndpoints20, localKnownEndpoints21, localKnownEndpoints22, localKnownEndpoints23, localKnownEndpoints24, localKnownEndpoints25, localKnownEndpoints26, localKnownEndpoints27, localKnownEndpoints28, localKnownEndpoints29, localKnownEndpoints30, localKnownEndpoints31, localKnownEndpoints32, localKnownEndpoints33, localKnownEndpoints34 };
  }
  
  private KnownEndpoints(String paramString1, String paramString2, String paramString3, String paramString4, Integer paramInteger)
  {
    key = paramString1;
    euHost = paramString2;
    nonEuHost = paramString3;
    stagingEndpoint = paramString4;
    stagingPort = paramInteger;
  }
  
  public static final void switchToStaging()
  {
    access$setStaging$cp(true);
  }
  
  public final String getHost(KnownDomain paramKnownDomain)
  {
    k.b(paramKnownDomain, "domain");
    switch (f.a[paramKnownDomain.ordinal()])
    {
    default: 
      throw new l();
    case 2: 
      paramKnownDomain = nonEuHost;
      break;
    case 1: 
      paramKnownDomain = euHost;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramKnownDomain);
    localStringBuilder.append(".truecaller.com");
    return localStringBuilder.toString();
  }
  
  public final String getKey()
  {
    return key;
  }
  
  public final u url()
  {
    if ((isStaging) && (stagingPort != null) && (stagingEndpoint != null))
    {
      localObject = new u.a().a("http");
      localStringBuilder = new StringBuilder();
      localStringBuilder.append(stagingEndpoint);
      localStringBuilder.append(".truecaller.net");
      localObject = ((u.a)localObject).b(localStringBuilder.toString()).a(stagingPort.intValue()).b();
      k.a(localObject, "HttpUrl.Builder()\n      …\n                .build()");
      return (u)localObject;
    }
    Object localObject = new u.a().a("https");
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(key);
    localStringBuilder.append(".truecaller.com");
    localObject = ((u.a)localObject).b(localStringBuilder.toString()).b();
    k.a(localObject, "HttpUrl.Builder()\n      …\n                .build()");
    return (u)localObject;
  }
  
  public static final class a {}
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.util.KnownEndpoints
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */