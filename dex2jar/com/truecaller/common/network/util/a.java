package com.truecaller.common.network.util;

import c.g.b.k;
import com.truecaller.common.network.b;
import com.truecaller.common.network.d.e;
import e.f.a;
import e.s;
import e.s.a;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import okhttp3.u;
import okhttp3.v;
import okhttp3.y;
import okhttp3.y.a;

public final class a
{
  public List<v> a;
  public List<v> b;
  private u c;
  private String d;
  private f.a e;
  private y f;
  private int g;
  private TimeUnit h;
  private boolean i;
  
  private final s.a a()
  {
    Object localObject1 = new s.a();
    Object localObject2 = c;
    if (localObject2 == null) {
      k.a("endpoint");
    }
    s.a locala = ((s.a)localObject1).a((u)localObject2);
    localObject2 = e;
    localObject1 = localObject2;
    if (localObject2 == null) {
      localObject1 = (f.a)e.a.a.a.a();
    }
    locala = locala.a((f.a)localObject1);
    k.a(locala, "Retrofit.Builder()\n     …onverterFactory.create())");
    localObject2 = f;
    localObject1 = localObject2;
    if (localObject2 == null)
    {
      localObject1 = d.d;
      localObject1 = d.b();
    }
    int j;
    if ((g > 0) && (h != null)) {
      j = 1;
    } else {
      j = 0;
    }
    if ((a == null) && (b == null) && (d == null))
    {
      localObject2 = localObject1;
      if (j == 0) {}
    }
    else
    {
      localObject1 = ((y)localObject1).c();
      localObject2 = d;
      if (localObject2 != null)
      {
        localObject2 = ((Iterable)b.a((String)localObject2)).iterator();
        while (((Iterator)localObject2).hasNext()) {
          ((y.a)localObject1).a((v)((Iterator)localObject2).next());
        }
        ((y.a)localObject1).a((v)new com.truecaller.common.network.d.a());
      }
      localObject2 = a;
      if (localObject2 != null) {
        ((y.a)localObject1).b().addAll((Collection)localObject2);
      }
      localObject2 = b;
      if (localObject2 != null) {
        ((y.a)localObject1).c().addAll((Collection)localObject2);
      }
      if (j != 0)
      {
        localObject2 = h;
        if (localObject2 != null) {
          if (i) {
            ((y.a)localObject1).a((v)new e(((TimeUnit)localObject2).toMillis(g)));
          } else {
            ((y.a)localObject1).a(g, (TimeUnit)localObject2);
          }
        }
      }
      localObject2 = ((y.a)localObject1).d();
      k.a(localObject2, "build()");
      k.a(localObject2, "client.newBuilder().run …    build()\n            }");
    }
    localObject1 = locala.a((y)localObject2);
    k.a(localObject1, "builder.client(client)");
    return (s.a)localObject1;
  }
  
  public final a a(int paramInt, TimeUnit paramTimeUnit, boolean paramBoolean)
  {
    g = paramInt;
    h = paramTimeUnit;
    i = paramBoolean;
    return this;
  }
  
  public final a a(KnownEndpoints paramKnownEndpoints)
  {
    k.b(paramKnownEndpoints, "endpoint");
    c = paramKnownEndpoints.url();
    return this;
  }
  
  public final a a(f.a parama)
  {
    k.b(parama, "factory");
    e = parama;
    return this;
  }
  
  public final a a(Class<?> paramClass)
  {
    k.b(paramClass, "api");
    paramClass = paramClass.getSimpleName();
    k.a(paramClass, "api.simpleName");
    d = paramClass;
    return this;
  }
  
  public final a a(y paramy)
  {
    k.b(paramy, "client");
    f = paramy;
    return this;
  }
  
  public final <T> T b(Class<T> paramClass)
  {
    k.b(paramClass, "api");
    return (T)a().b().a(paramClass);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.util.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */