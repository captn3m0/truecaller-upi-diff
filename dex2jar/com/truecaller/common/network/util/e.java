package com.truecaller.common.network.util;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import d.d;
import java.io.IOException;
import okhttp3.ac;
import okhttp3.w;

public final class e
  extends ac
{
  private final Bitmap a;
  private final int b;
  
  public e(Bitmap paramBitmap)
  {
    a = paramBitmap;
    b = 75;
  }
  
  public final w a()
  {
    return g.c;
  }
  
  public final void a(d paramd)
    throws IOException
  {
    a.compress(Bitmap.CompressFormat.JPEG, b, paramd.c());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.util.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */