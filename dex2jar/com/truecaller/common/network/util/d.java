package com.truecaller.common.network.util;

import c.f;
import c.g;
import c.g.b.k;
import c.g.b.l;
import com.truecaller.common.network.a.b;
import com.truecaller.common.network.d.c;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import okhttp3.q.a;
import okhttp3.v;
import okhttp3.y;
import okhttp3.y.a;

public final class d
{
  public static c b;
  public static com.truecaller.common.network.f.a c;
  public static final d d = new d();
  private static final f e = g.a((c.g.a.a)b.a);
  private static final f f = g.a((c.g.a.a)a.a);
  
  public static final y a()
  {
    return (y)e.b();
  }
  
  public static final y a(b paramb)
  {
    k.b(paramb, "config");
    paramb = b(paramb).d();
    k.a(paramb, "createOkHttpClientBuilder(config).build()");
    return paramb;
  }
  
  @Inject
  public static void a(c paramc)
  {
    k.b(paramc, "<set-?>");
    b = paramc;
  }
  
  @Inject
  public static void a(com.truecaller.common.network.f.a parama)
  {
    k.b(parama, "<set-?>");
    c = parama;
  }
  
  public static final y.a b(b paramb)
  {
    k.b(paramb, "config");
    y.a locala = new y.a();
    locala.a(10L, TimeUnit.SECONDS);
    locala.b(20L, TimeUnit.SECONDS);
    paramb = paramb.b().iterator();
    while (paramb.hasNext())
    {
      com.truecaller.common.network.a.a locala1 = (com.truecaller.common.network.a.a)paramb.next();
      Object localObject = b;
      if (localObject == null) {
        k.a("interceptorFactory");
      }
      k.a(locala1, "attr");
      localObject = ((c)localObject).a(locala1);
      if (localObject != null)
      {
        if (a) {
          locala.b((v)localObject);
        } else {
          locala.a((v)localObject);
        }
        if (b != null) {
          locala.a(b);
        }
      }
    }
    locala.a((v)new com.truecaller.common.network.d.d());
    paramb = c;
    if (paramb == null) {
      k.a("httpAnalyticsLoggerFactory");
    }
    locala.a((q.a)paramb);
    return locala;
  }
  
  public static y b()
  {
    return (y)f.b();
  }
  
  public static final y c()
  {
    return a(new b());
  }
  
  public static final y.a d()
  {
    return b(new b());
  }
  
  static final class a
    extends l
    implements c.g.a.a<y>
  {
    public static final a a = new a();
    
    a()
    {
      super();
    }
  }
  
  static final class b
    extends l
    implements c.g.a.a<y>
  {
    public static final b a = new b();
    
    b()
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.util.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */