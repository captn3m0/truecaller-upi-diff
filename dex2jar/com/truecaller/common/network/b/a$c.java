package com.truecaller.common.network.b;

import com.google.gson.a.c;

public final class a$c
{
  @c(a="upgradePath")
  public String a;
  @c(a="downloadLink")
  public String b;
  @c(a="notifyFreqInDays")
  public int c;
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("UpgradeStatus{upgradePath='");
    localStringBuilder.append(a);
    localStringBuilder.append('\'');
    localStringBuilder.append(", downloadLink='");
    localStringBuilder.append(b);
    localStringBuilder.append('\'');
    localStringBuilder.append(", frequency=");
    localStringBuilder.append(c);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.b.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */