package com.truecaller.common.network.b;

import com.google.gson.a.c;

public final class a
{
  @c(a="features")
  public b a;
  @c(a="upgradeStatus")
  public c b;
  @c(a="ab_testing")
  public a c;
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("ConfigDto{features=");
    localStringBuilder.append(a);
    localStringBuilder.append(", abTesting=");
    localStringBuilder.append(c);
    localStringBuilder.append(", upgradeStatus=");
    localStringBuilder.append(b);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
  
  public static final class a {}
  
  public static final class b
  {
    @c(a="featureReferralIconInFavouriteContact")
    public String A;
    @c(a="featureReferralIconInContactDetail")
    public String B;
    @c(a="featureReferralIconInSearch")
    public String C;
    @c(a="featureReferralNavDrawer")
    public String D;
    @c(a="featureReferralShareApps")
    public String E;
    @c(a="featureRedeemGoPro")
    public String F;
    @c(a="featurePromoSpamOffCount")
    public String G;
    @c(a="featurePromoIncomingMsgCount")
    public String H;
    @c(a="featureBackup")
    public String I;
    @c(a="featureSwish")
    public String J;
    @c(a="featureReferralAfterCallPromo")
    public String K;
    @c(a="featureOfflineDirectory")
    public String L;
    @c(a="featureContactsWithoutIdentity")
    public String M;
    @c(a="featureNumberScanner")
    public String N;
    @c(a="featurePauseUploads")
    public String O;
    @c(a="featureGlobalPromoPeriod")
    public String P;
    @c(a="featurePromoPeriod")
    public String Q;
    @c(a="featurePromoDismissedDelay")
    public String R;
    @c(a="featureOtpNotification")
    public String S;
    @c(a="featureOtpParserRegex")
    public String T;
    @c(a="featureIm")
    public String U;
    @c(a="featureTcPay")
    public String V;
    @c(a="featureEnableUtilities")
    public String W;
    @c(a="featureMultiplePsp")
    public String X;
    @c(a="featureUseBankSmsData")
    public String Y;
    @c(a="featureWhatsAppCalls")
    public String Z;
    @c(a="featureEUp")
    public String a;
    @c(a="featureGetImUserMissTtl")
    public String aA;
    @c(a="featureShareImageInFlash")
    public String aB;
    @c(a="featureUrlMinSpamScore")
    public String aC;
    @c(a="featureImMaxMediaSize")
    public String aD;
    @c(a="featureImTracingEnabled")
    public String aE;
    @c(a="featureMaxEventsBatchSize")
    public String aF;
    @c(a="featureMinEventsBatchSize")
    public String aG;
    @c(a="featureUploadEventsJitter")
    public String aH;
    @c(a="featureEnableGoldCallerIdForContacts")
    public String aI;
    @c(a="featureImPromoAfterCallPeriodDays")
    public String aJ;
    @c(a="featureUpdateAppPromoPeriodDays")
    public String aK;
    @c(a="featureBusinessProfiles")
    public String aL;
    @c(a="featureCreateBusinessProfiles")
    public String aM;
    @c(a="featureNormalizeShortCodes")
    public String aN;
    @c(a="featureShowUserJoinedImNotification")
    public String aO;
    @c(a="featureSmartNotificationPayAction")
    public String aP;
    @c(a="featureImEmojiPoke")
    public String aQ;
    @c(a="featureSdkScanner")
    public String aR;
    @c(a="featureTcPaySmsBindingDeliveryCheck")
    public String aS;
    @c(a="featureReactions")
    public String aT;
    @c(a="featureReactionsEmojis")
    public String aU;
    @c(a="featureContactsListV2")
    public String aV;
    @c(a="featurePayHomeCarousel")
    public String aW;
    @c(a="featurePayRewards")
    public String aX;
    @c(a="featurePayGooglePlayRecharge")
    public String aY;
    @c(a="featurePayBbpsReminders")
    public String aZ;
    @c(a="featureTcCredit")
    public String aa;
    @c(a="featureCleverTap")
    public String ab;
    @c(a="featureAdCtpVideoRotation")
    public String ac;
    @c(a="featureDisabledExtendedPrivacy")
    public String ad;
    @c(a="featureCallRecording")
    public String ae;
    @c(a="featureCallRecordingLicense")
    public String af;
    @c(a="featureShowOptInReadMore")
    public String ag;
    @c(a="featurePresenceInterval")
    public String ah;
    @c(a="featurePresenceInitialDelay")
    public String ai;
    @c(a="featurePresenceStopTime")
    public String aj;
    @c(a="featurePresenceRecheckTime")
    public String ak;
    @c(a="featureWhoViewedMe")
    public String al;
    @c(a="featureWhoViewdMeNewViewIntervalInDays")
    public String am;
    @c(a="featureWhoViewdMeShowNotificationAfterXDays")
    public String an;
    @c(a="featureWhoViewdMeShowNotificationAfterXLookups")
    public String ao;
    @c(a="featureWhoViewedMeIncognito")
    public String ap;
    @c(a="featureBusinessSuggestion")
    public String aq;
    @c(a="featureWhoViewedMePBContact")
    public String ar;
    @c(a="featureWhoViewedMeACSEnabled")
    public String as;
    @c(a="featureSmsCategorizer")
    public String at;
    @c(a="featureBlockByCountry")
    public String au;
    @c(a="featureHouseAdsTimeout")
    public String av;
    @c(a="featureAdsUnifiedDetails")
    public String aw;
    @c(a="featureAdsUnifiedBlock")
    public String ax;
    @c(a="featureAdRetentionTime")
    public String ay;
    @c(a="featureSmartNotifications")
    public String az;
    @c(a="featureNUp")
    public String b;
    @c(a="featureContactJobAsPremium")
    public String bA;
    @c(a="featureContactWebsiteAsPremium")
    public String bB;
    @c(a="featureContactSocialAsPremium")
    public String bC;
    @c(a="featureContactAboutAsPremium")
    public String bD;
    @c(a="featureInsightsSMSParsing")
    public String bE;
    @c(a="featureInsightsSMSPersistence")
    public String bF;
    @c(a="featureImGroupMaxParticipantCount")
    public String bG;
    @c(a="featureImGroupBatchParticipantCount")
    public String bH;
    @c(a="featureVoiceClip")
    public String bI;
    @c(a="featureHttpAnalyticsHosts")
    public String bJ;
    @c(a="featureImVoiceClipMaxDurationMins")
    public String bK;
    @c(a="featureFiveBottomTabsWithBlockingPremium")
    public String bL;
    @c(a="featureTopSpammersMaxSize")
    public String bM;
    @c(a="featureTopSpammersPremiumMaxSize")
    public String bN;
    @c(a="featureSdkScannerIgnoreFilter")
    public String bO;
    @c(a="featurePayShortcutIcon")
    public String bP;
    @c(a="featurePremiumTab")
    public String bQ;
    @c(a="featurePayHomeRevamp")
    public String bR;
    @c(a="featureInCallUI")
    public String bS;
    @c(a="featureCrossDcSearch")
    public String bT;
    @c(a="featurePayRegistrationV2")
    public String bU;
    @c(a="featureEngagementRewards")
    public String bV;
    @c(a="featurePayAppIxigo")
    public String bW;
    @c(a="featureAppsInstalledHeartbeat")
    public String bX;
    @c(a="featurePayInstantReward")
    public String ba;
    @c(a="featurePayAppUpdatePopUp")
    public String bb;
    @c(a="featureSearchHitTtl")
    public String bc;
    @c(a="featureSearchMissTtl")
    public String bd;
    @c(a="featureDisableAftercallIfLandscape")
    public String be;
    @c(a="featureBlockHiddenNumbersAsPremium")
    public String bf;
    @c(a="featureBlockTopSpammersAsPremium")
    public String bg;
    @c(a="featureBlockNonPhonebook")
    public String bh;
    @c(a="featureBlockNonPhonebookAsPremium")
    public String bi;
    @c(a="featureBlockForeignNumbers")
    public String bj;
    @c(a="featureBlockForeignNumbersAsPremium")
    public String bk;
    @c(a="featureBlockNeighbourSpoofing")
    public String bl;
    @c(a="featureBlockNeighbourSpoofingAsPremium")
    public String bm;
    @c(a="featureBlockRegisteredTelemarketersAsPremium")
    public String bn;
    @c(a="featureIMReply")
    public String bo;
    @c(a="featureConvertBusinessProfileToPrivate")
    public String bp;
    @c(a="featureVoIP")
    public String bq;
    @c(a="featureVoipAfterCallPromoDays")
    public String br;
    @c(a="featureTruecallerX")
    public String bs;
    @c(a="featureLargeDetailsViewAd")
    public String bt;
    @c(a="featureImNewJoinersPeriodDays")
    public String bu;
    @c(a="featureVisiblePushCallerId")
    public String bv;
    @c(a="featureContactFieldsPremiumForUgc")
    public String bw;
    @c(a="featureContactFieldsPremiumForProfile")
    public String bx;
    @c(a="featureContactEmailAsPremium")
    public String by;
    @c(a="featureContactAddressAsPremium")
    public String bz;
    @c(a="featureCUp")
    public String c;
    @c(a="featureInviteSms")
    public String d;
    @c(a="featureNameSuggestions")
    public String e;
    @c(a="featureOutgoingSearch")
    public String f;
    @c(a="featureSmsSearch")
    public String g;
    @c(a="featureStatsSearch")
    public String h;
    @c(a="searchVersion")
    public String i;
    @c(a="featureAds")
    public String j;
    @c(a="featureAdsInterstitial")
    public String k;
    @c(a="featureTagUpload")
    public String l;
    @c(a="featureAvailability")
    public String m;
    @c(a="featureLoggingEnabled")
    public String n;
    @c(a="featureTcPayOnboarding")
    public String o;
    @c(a="featureOperatorCustomization")
    public String p;
    @c(a="featureCacheAdAfterCall")
    public String q;
    @c(a="featureDisableOutgoingOutside")
    public String r;
    @c(a="featureHideDialpad")
    public String s;
    @c(a="featureFlash")
    public String t;
    @c(a="featureReferralIconInAfterCall")
    public String u;
    @c(a="featureReferralAfterCallSaveContact")
    public String v;
    @c(a="featureReferralIconInUserBusy")
    public String w;
    @c(a="featureReferralIconInContacts")
    public String x;
    @c(a="featureReferralDeeplink")
    public String y;
    @c(a="featureReferralIconInInboxOverflow")
    public String z;
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder("Features{featureEmailUpload='");
      localStringBuilder.append(a);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureNameUpload='");
      localStringBuilder.append(b);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureClearUpload='");
      localStringBuilder.append(c);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureInviteSms='");
      localStringBuilder.append(d);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureNameSuggestions='");
      localStringBuilder.append(e);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureOutgoingSearch='");
      localStringBuilder.append(f);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureSmsSearch='");
      localStringBuilder.append(g);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureStatsSearch='");
      localStringBuilder.append(h);
      localStringBuilder.append('\'');
      localStringBuilder.append(", searchVersion='");
      localStringBuilder.append(i);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureAds='");
      localStringBuilder.append(j);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureAdsInterstitial='");
      localStringBuilder.append(k);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureTagUpload='");
      localStringBuilder.append(l);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureAvailability='");
      localStringBuilder.append(m);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureLoggingEnabled='");
      localStringBuilder.append(n);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureOperatorCustomization='");
      localStringBuilder.append(p);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureCacheAdAfterCall='");
      localStringBuilder.append(q);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureDisableOutgoingOutside='");
      localStringBuilder.append(r);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureHideDialpad='");
      localStringBuilder.append(s);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureFlash='");
      localStringBuilder.append(t);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureReferralIconInAfterCall='");
      localStringBuilder.append(u);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureReferralAfterCallSaveContact='");
      localStringBuilder.append(v);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureReferralIconInUserBusy='");
      localStringBuilder.append(w);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureReferralIconInContacts='");
      localStringBuilder.append(x);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureReferralDeeplink='");
      localStringBuilder.append(y);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureReferralIconInInboxOverflow='");
      localStringBuilder.append(z);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureReferralIconInFavouriteContact='");
      localStringBuilder.append(A);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureReferralIconInContactDetail='");
      localStringBuilder.append(B);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureReferralIconInSearch='");
      localStringBuilder.append(C);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureReferralNavDrawer='");
      localStringBuilder.append(D);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureReferralShareApps='");
      localStringBuilder.append(E);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureRedeemGoPro='");
      localStringBuilder.append(F);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featurePromoSpamOffCount='");
      localStringBuilder.append(G);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featurePromoIncomingMsgCount='");
      localStringBuilder.append(H);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureBackup='");
      localStringBuilder.append(I);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureSwish='");
      localStringBuilder.append(J);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureReferralAfterCallPromo='");
      localStringBuilder.append(K);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureOfflineDirectory='");
      localStringBuilder.append(L);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureNumberScanner='");
      localStringBuilder.append(N);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureDisableUgc='");
      localStringBuilder.append(O);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureGlobalUnimportantPromoPeriodDays='");
      localStringBuilder.append(P);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureUnimportantPromoPeriodDays='");
      localStringBuilder.append(Q);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureUnimportantPromoDismissedDelayDays='");
      localStringBuilder.append(R);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureOTPNotificationEnabled='");
      localStringBuilder.append(S);
      localStringBuilder.append('\'');
      localStringBuilder.append(", otpParserRegex='");
      localStringBuilder.append(T);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureIm='");
      localStringBuilder.append(U);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureTcPay='");
      localStringBuilder.append(V);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureTcPayOnboarding='");
      localStringBuilder.append(o);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureCleverTap='");
      localStringBuilder.append(ab);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureEnableUtilities='");
      localStringBuilder.append(W);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureAdCtpVideoRotation='");
      localStringBuilder.append(ac);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureDisabledRegion1='");
      localStringBuilder.append(ad);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureCallRecording='");
      localStringBuilder.append(ae);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureCallRecordingLicense='");
      localStringBuilder.append(af);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureShowOptInReadMore='");
      localStringBuilder.append(ag);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featurePresenceInterval='");
      localStringBuilder.append(ah);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featurePresenceInitialDelay='");
      localStringBuilder.append(ai);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featurePresenceStopTime='");
      localStringBuilder.append(aj);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featurePresenceRecheckTime='");
      localStringBuilder.append(ak);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureWhoViewedMe='");
      localStringBuilder.append(al);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureUgcContactsWithoutIdentity='");
      localStringBuilder.append(M);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureWhoViewdMeNewViewIntervalInDays='");
      localStringBuilder.append(am);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureWhoViewdMeShowNotificationAfterXDays='");
      localStringBuilder.append(an);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureWhoViewdMeShowNotificationAfterXLookups='");
      localStringBuilder.append(ao);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureWhoViewedMeIncognito='");
      localStringBuilder.append(ap);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureWhoViewedMePBContact='");
      localStringBuilder.append(ar);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureWhoViewedMeACSEnabled='");
      localStringBuilder.append(as);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureSmsCategorizer='");
      localStringBuilder.append(at);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureHouseAdsTimeout='");
      localStringBuilder.append(av);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureAdsUnifiedDetails='");
      localStringBuilder.append(aw);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureAdsUnifiedBlock='");
      localStringBuilder.append(ax);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureAdRetentionTime='");
      localStringBuilder.append(ay);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureMultiplePsp='");
      localStringBuilder.append(X);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureSmartNotifications='");
      localStringBuilder.append(az);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureGetImUserMissTtl='");
      localStringBuilder.append(aA);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureUseBankSmsData='");
      localStringBuilder.append(Y);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureShareImageInFlash='");
      localStringBuilder.append(aB);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureUrlMinSpamScore='");
      localStringBuilder.append(aC);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureImMaxMediaSize='");
      localStringBuilder.append(aD);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureImTracingEnabled='");
      localStringBuilder.append(aE);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureWhatsAppCalls='");
      localStringBuilder.append(Z);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureTcCredit='");
      localStringBuilder.append(aa);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureMaxEventsBatchSize='");
      localStringBuilder.append(aF);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureMinEventsBatchSize='");
      localStringBuilder.append(aG);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureUploadEventsJitter='");
      localStringBuilder.append(aH);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureEnableGoldCallerIdForContacts='");
      localStringBuilder.append(aI);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureImPromoAfterCallPeriodDays='");
      localStringBuilder.append(aJ);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureUpdateAppPromoPeriodDays='");
      localStringBuilder.append(aK);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureBusinessProfiles='");
      localStringBuilder.append(aL);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureCreateBusinessProfiles='");
      localStringBuilder.append(aM);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureNormalizeShortCodes='");
      localStringBuilder.append(aN);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureShowUserJoinedImNotification='");
      localStringBuilder.append(aO);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureSmartNotificationPayAction='");
      localStringBuilder.append(aP);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureSdkScanner='");
      localStringBuilder.append(aR);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureTcPaySmsBindingDeliveryCheck='");
      localStringBuilder.append(aS);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureReactions='");
      localStringBuilder.append(aT);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureReactionsEmojis='");
      localStringBuilder.append(aU);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureContactsListV2='");
      localStringBuilder.append(aV);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featurePayHomeCarousel='");
      localStringBuilder.append(aW);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featurePayRewards='");
      localStringBuilder.append(aX);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featurePayGooglePlayRecharge='");
      localStringBuilder.append(aY);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featurePayAppUpdatePopUp='");
      localStringBuilder.append(bb);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureSearchHitTtl='");
      localStringBuilder.append(bc);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureSearchMissTtl='");
      localStringBuilder.append(bd);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureDisableAftercallIfLandscape='");
      localStringBuilder.append(be);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureBlockNonPhonebook='");
      localStringBuilder.append(bh);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureBlockNonPhonebookAsPremium='");
      localStringBuilder.append(bi);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureBlockForeignNumbers='");
      localStringBuilder.append(bj);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureBlockForeignNumbersAsPremium='");
      localStringBuilder.append(bk);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureBlockNeighbourSpoofing='");
      localStringBuilder.append(bl);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureBlockNeighbourSpoofingAsPremium='");
      localStringBuilder.append(bm);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureBlockHiddenNumbersAsPremium='");
      localStringBuilder.append(bf);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureBlockTopSpammersAsPremium='");
      localStringBuilder.append(bg);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureBlockIndianRegisteredTelemarketersAsPremium='");
      localStringBuilder.append(bn);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureReply='");
      localStringBuilder.append(bo);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureConvertBusinessProfileToPrivate='");
      localStringBuilder.append(bp);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureVoIP='");
      localStringBuilder.append(bq);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureTruecallerX='");
      localStringBuilder.append(bs);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureLargeDetailsViewAd='");
      localStringBuilder.append(bt);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureImNewJoinersPeriodDays='");
      localStringBuilder.append(bu);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureVisiblePushCallerId='");
      localStringBuilder.append(bv);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureContactFieldsPremiumForUgc='");
      localStringBuilder.append(bw);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureContactFieldsPremiumForProfile='");
      localStringBuilder.append(bx);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureContactEmailAsPremium='");
      localStringBuilder.append(by);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureContactAddressAsPremium='");
      localStringBuilder.append(bz);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureContactJobAsPremium='");
      localStringBuilder.append(bA);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureContactWebsiteAsPremium='");
      localStringBuilder.append(bB);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureContactSocialAsPremium='");
      localStringBuilder.append(bC);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureContactAboutAsPremium='");
      localStringBuilder.append(bD);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureImGroupMaxParticipantCount='");
      localStringBuilder.append(bG);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureImGroupBatchParticipantCount='");
      localStringBuilder.append(bH);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureVoiceClip='");
      localStringBuilder.append(bI);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureVoipAfterCallPromoDays='");
      localStringBuilder.append(br);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureHttpAnalyticsHosts='");
      localStringBuilder.append(bJ);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureInsightsSMSParsing='");
      localStringBuilder.append(bE);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureImVoiceClipMaxDurationMins='");
      localStringBuilder.append(bK);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureFiveBottomTabsWithBlockingPremium='");
      localStringBuilder.append(bL);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureTopSpammersMaxSize='");
      localStringBuilder.append(bM);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureTopSpammersPremiumMaxSize='");
      localStringBuilder.append(bN);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureSdkScannerIgnoreFilter='");
      localStringBuilder.append(bO);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featurePayShortcutIcon='");
      localStringBuilder.append(bP);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featurePremiumTabForIndia='");
      localStringBuilder.append(bQ);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featurePayHomeRevamp='");
      localStringBuilder.append(bR);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureInCallUI='");
      localStringBuilder.append(bS);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureCrossDcSearch='");
      localStringBuilder.append(bT);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featurePayRegistrationV2='");
      localStringBuilder.append(bU);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureInsightsSMSPersistence='");
      localStringBuilder.append(bF);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureEngagementRewards='");
      localStringBuilder.append(bV);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featurePayBbpsReminders='");
      localStringBuilder.append(aZ);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featurePayAppIxigo='");
      localStringBuilder.append(bW);
      localStringBuilder.append('\'');
      localStringBuilder.append(", featureAppsInstalledHeartbeat='");
      localStringBuilder.append(bX);
      localStringBuilder.append('\'');
      localStringBuilder.append('}');
      return localStringBuilder.toString();
    }
  }
  
  public static final class c
  {
    @c(a="upgradePath")
    public String a;
    @c(a="downloadLink")
    public String b;
    @c(a="notifyFreqInDays")
    public int c;
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder("UpgradeStatus{upgradePath='");
      localStringBuilder.append(a);
      localStringBuilder.append('\'');
      localStringBuilder.append(", downloadLink='");
      localStringBuilder.append(b);
      localStringBuilder.append('\'');
      localStringBuilder.append(", frequency=");
      localStringBuilder.append(c);
      localStringBuilder.append('}');
      return localStringBuilder.toString();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.b.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */