package com.truecaller.common.network;

import com.truecaller.common.account.r;
import com.truecaller.common.h.i;
import com.truecaller.log.AssertionUtil;
import java.io.IOException;
import okhttp3.ab;
import okhttp3.ab.a;
import okhttp3.ad;
import okhttp3.u;
import okhttp3.u.a;
import okhttp3.v;
import okhttp3.v.a;

public final class a
  implements v
{
  private final boolean a;
  private final r b;
  private final dagger.a<com.truecaller.common.account.k> c;
  private final i d;
  
  public a(boolean paramBoolean, r paramr, dagger.a<com.truecaller.common.account.k> parama, i parami)
  {
    a = paramBoolean;
    b = paramr;
    c = parama;
    d = parami;
  }
  
  public final ad intercept(v.a parama)
    throws IOException
  {
    c.g.b.k.b(parama, "chain");
    try
    {
      ab localab = parama.a();
      Object localObject = d;
      c.g.b.k.a(localab, "request");
      boolean bool = ((i)localObject).a(f.a(localab));
      if (bool) {
        localObject = ((com.truecaller.common.account.k)c.get()).a();
      } else if (a) {
        localObject = b.e();
      } else {
        localObject = b.d();
      }
      if (localObject != null)
      {
        u localu = localab.a().j().a("encoding", "json").b();
        localObject = localab.e().b("Authorization", "Bearer ".concat(String.valueOf(localObject))).a(localu).a();
      }
      else
      {
        if (a) {
          break label179;
        }
        localObject = localab;
        if (bool) {
          break label179;
        }
      }
      parama = parama.a((ab)localObject);
      c.g.b.k.a(parama, "chain.proceed(request)");
      return parama;
      label179:
      if (!b.c()) {
        AssertionUtil.isTrue(false, new String[] { "Bug in application code. You should not do these requests if not everything is initialized. This is to prevent that (potentially lots of) bad backend requests are made." });
      }
      throw ((Throwable)new IOException("Bug in application code. You should not do these requests if not everything is initialized. This is to prevent that (potentially lots of) bad backend requests are made."));
    }
    catch (SecurityException parama)
    {
      throw ((Throwable)new IOException((Throwable)parama));
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */