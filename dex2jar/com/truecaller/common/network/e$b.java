package com.truecaller.common.network;

import c.g.b.k;

public final class e$b
  extends e
{
  public final KnownDomain a;
  
  public e$b(KnownDomain paramKnownDomain)
  {
    super((byte)0);
    a = paramKnownDomain;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject) {
      if ((paramObject instanceof b))
      {
        paramObject = (b)paramObject;
        if (k.a(a, a)) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    KnownDomain localKnownDomain = a;
    if (localKnownDomain != null) {
      return localKnownDomain.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("Specific(domain=");
    localStringBuilder.append(a);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.e.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */