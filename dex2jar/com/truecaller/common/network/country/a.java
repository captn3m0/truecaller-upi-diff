package com.truecaller.common.network.country;

import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.network.util.d;
import e.b.f;
import e.b.t;

public final class a
{
  public static final a a = new a();
  
  public static final e.b<CountryListDto> a(String paramString)
  {
    com.truecaller.common.network.util.a locala = new com.truecaller.common.network.util.a().a(KnownEndpoints.REQUEST).a(a.class);
    com.truecaller.common.network.a.b localb = new com.truecaller.common.network.a.b();
    localb.a();
    return ((a)locala.a(d.a(localb)).b(a.class)).a(paramString);
  }
  
  static abstract interface a
  {
    @f(a="/?countrylist=3&encoding=json")
    public abstract e.b<CountryListDto> a(@t(a="checksum") String paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.country.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */