package com.truecaller.common.network.country;

import com.google.gson.a.c;
import java.util.List;

public class CountryListDto
{
  @c(a="COUNTRY_LIST")
  public b countryList;
  @c(a="COUNTRY_LIST_CHECKSUM")
  public String countryListChecksum;
  
  public static class a
  {
    @c(a="CID")
    public String a;
    @c(a="CN")
    public String b;
    @c(a="CCN")
    public String c;
    @c(a="CC")
    public String d;
  }
  
  public static final class b
  {
    @c(a="COUNTRY_SUGGESTION")
    public CountryListDto.a a;
    @c(a="C")
    public List<CountryListDto.a> b;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.country.CountryListDto
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */