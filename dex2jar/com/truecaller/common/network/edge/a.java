package com.truecaller.common.network.edge;

import com.truecaller.common.network.util.AuthRequirement;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.network.util.d;
import e.b.f;
import e.b.t;

public final class a
{
  public static e.b<EdgeDto> a(String paramString1, String paramString2, String paramString3)
  {
    return ((a)new com.truecaller.common.network.util.a().a(KnownEndpoints.EDGE).a(a.class).a(d.a(new com.truecaller.common.network.a.b().a(AuthRequirement.OPTIONAL).a())).b(a.class)).a(paramString1, paramString2, paramString3);
  }
  
  static abstract interface a
  {
    @f(a="/v2")
    public abstract e.b<EdgeDto> a(@t(a="networkCountryCode") String paramString1, @t(a="phoneCountryCode") String paramString2, @t(a="phoneNumber") String paramString3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.edge.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */