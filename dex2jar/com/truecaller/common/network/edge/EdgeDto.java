package com.truecaller.common.network.edge;

import c.a.m;
import com.google.gson.a.c;
import java.util.List;
import java.util.Map;

public final class EdgeDto
{
  @c(a="data")
  private Map<String, Map<String, a>> data;
  @c(a="ttl")
  private int timeToLive;
  
  public final Map<String, Map<String, a>> getData()
  {
    return data;
  }
  
  public final int getTimeToLive()
  {
    return timeToLive;
  }
  
  public final void setData(Map<String, Map<String, a>> paramMap)
  {
    data = paramMap;
  }
  
  public final void setTimeToLive(int paramInt)
  {
    timeToLive = paramInt;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("EdgeDto(data=");
    localStringBuilder.append(data);
    localStringBuilder.append(", timeToLive=");
    localStringBuilder.append(timeToLive);
    localStringBuilder.append(')');
    return localStringBuilder.toString();
  }
  
  public static final class a
  {
    @c(a="edges")
    public List<String> a;
    
    public a() {}
    
    public a(String paramString)
    {
      this();
      a = m.c(new String[] { paramString });
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder("Endpoint(edges=");
      localStringBuilder.append(a);
      localStringBuilder.append(')');
      return localStringBuilder.toString();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.edge.EdgeDto
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */