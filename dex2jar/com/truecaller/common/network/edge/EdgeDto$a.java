package com.truecaller.common.network.edge;

import c.a.m;
import com.google.gson.a.c;
import java.util.List;

public final class EdgeDto$a
{
  @c(a="edges")
  public List<String> a;
  
  public EdgeDto$a() {}
  
  public EdgeDto$a(String paramString)
  {
    this();
    a = m.c(new String[] { paramString });
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("Endpoint(edges=");
    localStringBuilder.append(a);
    localStringBuilder.append(')');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.edge.EdgeDto.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */