package com.truecaller.common.network;

import c.g.b.k;
import c.n.m;
import com.truecaller.common.g.a;
import com.truecaller.common.h.ac;

public final class d
  implements c
{
  private final a a;
  private final ac b;
  
  public d(a parama, ac paramac)
  {
    a = parama;
    b = paramac;
  }
  
  public final String a()
  {
    String str2 = a.a("networkDomain");
    String str1 = str2;
    if (str2 == null) {
      str1 = b();
    }
    return str1;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "value");
    String str;
    if (!m.a((CharSequence)paramString)) {
      str = paramString;
    } else {
      str = null;
    }
    if (str != null) {
      a.a("networkDomain", paramString);
    }
  }
  
  public final String b()
  {
    if (b.a()) {}
    for (KnownDomain localKnownDomain = KnownDomain.DOMAIN_REGION_1;; localKnownDomain = KnownDomain.DOMAIN_OTHER_REGIONS) {
      return localKnownDomain.getValue();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */