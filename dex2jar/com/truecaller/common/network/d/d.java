package com.truecaller.common.network.d;

import c.g.b.k;
import java.io.IOException;
import okhttp3.ad;
import okhttp3.v;
import okhttp3.v.a;

public final class d
  implements v
{
  public final ad intercept(v.a parama)
    throws IOException
  {
    k.b(parama, "chain");
    try
    {
      parama = parama.a(parama.a());
      k.a(parama, "chain.proceed(chain.request())");
      return parama;
    }
    catch (SecurityException parama)
    {
      throw ((Throwable)new a(parama));
    }
  }
  
  static final class a
    extends IOException
  {
    public a(SecurityException paramSecurityException)
    {
      super();
      initCause((Throwable)paramSecurityException);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.d.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */