package com.truecaller.common.network.d;

import c.g.b.k;
import com.truecaller.common.network.util.b;
import com.truecaller.common.network.util.c;
import java.util.concurrent.TimeUnit;
import okhttp3.ab;
import okhttp3.ad;
import okhttp3.u;
import okhttp3.v;
import okhttp3.v.a;

public final class e
  implements v
{
  private final long a;
  
  public e(long paramLong)
  {
    a = paramLong;
  }
  
  public final ad intercept(v.a parama)
  {
    k.b(parama, "chain");
    ab localab = parama.a();
    Object localObject = localab.a();
    if (localObject != null) {
      localObject = ((u)localObject).f();
    } else {
      localObject = null;
    }
    long l2 = a;
    long l1 = l2;
    if (localObject != null)
    {
      c localc = c.a;
      long l3 = c.a((String)localObject, l2);
      l1 = l2;
      if (l3 > 0L) {
        l1 = l2 - l3;
      }
      if (l1 <= 0L)
      {
        parama = new StringBuilder("Timed out after DNS lookup for ");
        parama.append(a);
        parama.append(" milliseconds");
        throw ((Throwable)new b(parama.toString()));
      }
    }
    parama = parama.a((int)l1, TimeUnit.MILLISECONDS).a(localab);
    k.a(parama, "chain.withConnectTimeout…        .proceed(request)");
    return parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.d.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */