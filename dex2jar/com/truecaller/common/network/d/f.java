package com.truecaller.common.network.d;

import android.os.Build.VERSION;
import java.io.IOException;
import okhttp3.ab;
import okhttp3.ab.a;
import okhttp3.ad;
import okhttp3.v;
import okhttp3.v.a;

public final class f
  implements v
{
  private final String a;
  private final String b;
  private final String c;
  
  public f(String paramString1, String paramString2)
  {
    b = paramString1;
    c = paramString2;
    paramString1 = Build.VERSION.RELEASE;
    c.g.b.k.a(paramString1, "Build.VERSION.RELEASE");
    paramString1 = (CharSequence)paramString1;
    a = new c.n.k("[^\\x20-\\x7E]").a(paramString1, "");
  }
  
  public final ad intercept(v.a parama)
    throws IOException
  {
    c.g.b.k.b(parama, "chain");
    ab.a locala = parama.a().e().b("User-Agent");
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(b);
    localStringBuilder.append('/');
    localStringBuilder.append(c);
    localStringBuilder.append(" (Android;");
    localStringBuilder.append(a);
    localStringBuilder.append(')');
    parama = parama.a(locala.b("User-Agent", localStringBuilder.toString()).a());
    c.g.b.k.a(parama, "chain.proceed(userAgent)");
    return parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.d.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */