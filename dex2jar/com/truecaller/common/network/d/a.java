package com.truecaller.common.network.d;

import android.os.SystemClock;
import android.text.TextUtils;
import com.truecaller.common.network.util.KnownEndpoints;
import d.c;
import d.e;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import okhttp3.ab;
import okhttp3.ad;
import okhttp3.ae;
import okhttp3.t;
import okhttp3.u;
import okhttp3.v;
import okhttp3.v.a;
import okhttp3.w;

public final class a
  implements v
{
  private static void a(StringBuilder paramStringBuilder, t paramt)
  {
    if (paramt != null)
    {
      if (a.length / 2 == 0) {
        return;
      }
      Object localObject1 = new TreeSet(String.CASE_INSENSITIVE_ORDER);
      int i = 0;
      int j = a.length / 2;
      while (i < j)
      {
        ((TreeSet)localObject1).add(paramt.a(i));
        i += 1;
      }
      localObject1 = Collections.unmodifiableSet((Set)localObject1).iterator();
      while (((Iterator)localObject1).hasNext())
      {
        String str1 = (String)((Iterator)localObject1).next();
        Object localObject2 = paramt.b(str1);
        if (localObject2 != null)
        {
          localObject2 = ((List)localObject2).iterator();
          while (((Iterator)localObject2).hasNext())
          {
            String str2 = (String)((Iterator)localObject2).next();
            paramStringBuilder.append("\n    ");
            paramStringBuilder.append(str1);
            paramStringBuilder.append(": ");
            paramStringBuilder.append(str2);
          }
        }
      }
      return;
    }
  }
  
  private static void a(ab paramab, boolean paramBoolean, long paramLong)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("--> ");
    localStringBuilder.append(b);
    localStringBuilder.append(" ");
    localStringBuilder.append(a);
    localStringBuilder.append(" time spent: ");
    localStringBuilder.append(paramLong);
    localStringBuilder.append("ms");
    if (paramBoolean) {
      a(localStringBuilder, c);
    }
    com.truecaller.debug.log.a.a(new Object[] { localStringBuilder.toString() });
  }
  
  public final ad intercept(v.a parama)
    throws IOException
  {
    localObject2 = parama.a();
    bool = TextUtils.equals(REQUESTurlb, a.b);
    l = SystemClock.elapsedRealtime();
    localObject1 = localObject2;
    for (;;)
    {
      try
      {
        localad = parama.a((ab)localObject2);
        localObject1 = localObject2;
        localObject2 = a;
        localObject1 = localObject2;
        a((ab)localObject2, bool, SystemClock.elapsedRealtime() - l);
        localObject1 = localObject2;
        parama = b;
        localObject1 = localObject2;
        localObject3 = a;
        localObject1 = localObject2;
        localStringBuilder = new StringBuilder();
        localObject1 = localObject2;
        localStringBuilder.append("<-- ");
        localObject1 = localObject2;
        localStringBuilder.append(parama);
        localObject1 = localObject2;
        localStringBuilder.append(" ");
        localObject1 = localObject2;
        localStringBuilder.append(localObject3);
        localObject1 = localObject2;
        localStringBuilder.append(" status code: ");
        localObject1 = localObject2;
        localStringBuilder.append(c);
        if (bool)
        {
          localObject1 = localObject2;
          a(localStringBuilder, f);
          localObject1 = localObject2;
        }
      }
      catch (Exception parama)
      {
        ad localad;
        Object localObject3;
        StringBuilder localStringBuilder;
        a((ab)localObject1, bool, SystemClock.elapsedRealtime() - l);
        localObject2 = new StringBuilder("<-- ");
        ((StringBuilder)localObject2).append(b);
        ((StringBuilder)localObject2).append(" ");
        ((StringBuilder)localObject2).append(a);
        ((StringBuilder)localObject2).append(" error:");
        ((StringBuilder)localObject2).append(parama.toString());
        com.truecaller.debug.log.a.a(new Object[] { ((StringBuilder)localObject2).toString() });
        throw parama;
      }
      try
      {
        parama = g;
        if (parama != null)
        {
          localObject1 = localObject2;
          localObject3 = parama.c();
          localObject1 = localObject2;
          ((e)localObject3).b(Long.MAX_VALUE);
          localObject1 = localObject2;
          c localc = ((e)localObject3).b();
          localObject1 = localObject2;
          w localw = parama.a();
          localObject1 = localObject2;
          localObject3 = Charset.forName("UTF-8");
          parama = (v.a)localObject3;
          if (localw != null)
          {
            localObject1 = localObject2;
            parama = localw.a((Charset)localObject3);
          }
          localObject1 = localObject2;
          localStringBuilder.append("\n    ");
          localObject1 = localObject2;
          localStringBuilder.append(localc.u().a(parama));
        }
      }
      catch (IOException|UnsupportedCharsetException parama) {}
    }
    localObject1 = localObject2;
    com.truecaller.debug.log.a.a(new Object[] { localStringBuilder.toString() });
    return localad;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.d.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */