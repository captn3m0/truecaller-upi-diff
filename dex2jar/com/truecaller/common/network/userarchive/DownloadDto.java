package com.truecaller.common.network.userarchive;

import android.support.annotation.Keep;

@Keep
public class DownloadDto
{
  public long expiration;
  public String url;
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.userarchive.DownloadDto
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */