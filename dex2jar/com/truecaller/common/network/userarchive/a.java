package com.truecaller.common.network.userarchive;

import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.network.util.h;
import e.b;
import e.b.f;

public final class a
{
  public static final a a = new a();
  
  public static b<DownloadDto> a()
  {
    return ((a)h.a(KnownEndpoints.USERARCHIVE, a.class)).a();
  }
  
  static abstract interface a
  {
    @f(a="/v1/download")
    public abstract b<DownloadDto> a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.userarchive.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */