package com.truecaller.common.network.optout;

import com.truecaller.common.h.q;
import com.truecaller.utils.extensions.c;
import e.r;
import javax.inject.Inject;

public final class b
  implements a
{
  public final boolean a()
  {
    Object localObject = q.a(OptOutRestAdapter.a());
    if (localObject != null) {
      localObject = Boolean.valueOf(((r)localObject).d());
    } else {
      localObject = null;
    }
    return c.a((Boolean)localObject);
  }
  
  public final boolean b()
  {
    Object localObject = q.a(OptOutRestAdapter.b());
    if (localObject != null) {
      localObject = Boolean.valueOf(((r)localObject).d());
    } else {
      localObject = null;
    }
    return c.a((Boolean)localObject);
  }
  
  public final boolean c()
  {
    Object localObject = q.a(OptOutRestAdapter.c());
    if (localObject != null) {
      localObject = Boolean.valueOf(((r)localObject).d());
    } else {
      localObject = null;
    }
    return c.a((Boolean)localObject);
  }
  
  public final boolean d()
  {
    Object localObject = q.a(OptOutRestAdapter.d());
    if (localObject != null) {
      localObject = Boolean.valueOf(((r)localObject).d());
    } else {
      localObject = null;
    }
    return c.a((Boolean)localObject);
  }
  
  public final OptOutRestAdapter.OptOutsDto e()
  {
    r localr = q.a(OptOutRestAdapter.e());
    if (localr != null) {
      return (OptOutRestAdapter.OptOutsDto)localr.e();
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.optout.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */