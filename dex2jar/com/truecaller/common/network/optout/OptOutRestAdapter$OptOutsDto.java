package com.truecaller.common.network.optout;

import android.support.annotation.Keep;
import java.util.List;

@Keep
public final class OptOutRestAdapter$OptOutsDto
{
  private final boolean consentRefresh;
  private final List<String> optIns;
  private final List<String> optOuts;
  
  public OptOutRestAdapter$OptOutsDto(List<String> paramList1, List<String> paramList2, boolean paramBoolean)
  {
    optOuts = paramList1;
    optIns = paramList2;
    consentRefresh = paramBoolean;
  }
  
  public final boolean getConsentRefresh()
  {
    return consentRefresh;
  }
  
  public final List<String> getOptIns()
  {
    return optIns;
  }
  
  public final List<String> getOptOuts()
  {
    return optOuts;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.optout.OptOutRestAdapter.OptOutsDto
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */