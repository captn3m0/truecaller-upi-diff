package com.truecaller.common.network.optout;

import e.b.f;
import e.b.o;
import e.b.s;

abstract interface OptOutRestAdapter$a
{
  @f(a="/v1/optouts")
  public abstract e.b<OptOutRestAdapter.OptOutsDto> a();
  
  @o(a="/v1/optout/{type}")
  public abstract e.b<Void> a(@s(a="type") String paramString);
  
  @e.b.b(a="/v1/optout/{type}")
  public abstract e.b<Void> b(@s(a="type") String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.optout.OptOutRestAdapter.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */