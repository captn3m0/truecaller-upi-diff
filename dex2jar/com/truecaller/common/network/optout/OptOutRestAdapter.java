package com.truecaller.common.network.optout;

import android.support.annotation.Keep;
import c.g;
import c.g.a.a;
import c.g.b.l;
import e.b.o;
import e.b.s;
import java.util.List;

public final class OptOutRestAdapter
{
  public static final OptOutRestAdapter b = new OptOutRestAdapter();
  private static final c.f c = g.a((a)b.a);
  
  public static final e.b<Void> a()
  {
    return f().a("dm");
  }
  
  public static final e.b<Void> b()
  {
    return f().a("ads");
  }
  
  public static final e.b<Void> c()
  {
    return f().b("dm");
  }
  
  public static final e.b<Void> d()
  {
    return f().b("ads");
  }
  
  public static final e.b<OptOutsDto> e()
  {
    return f().a();
  }
  
  private static a f()
  {
    return (a)c.b();
  }
  
  @Keep
  public static final class OptOutsDto
  {
    private final boolean consentRefresh;
    private final List<String> optIns;
    private final List<String> optOuts;
    
    public OptOutsDto(List<String> paramList1, List<String> paramList2, boolean paramBoolean)
    {
      optOuts = paramList1;
      optIns = paramList2;
      consentRefresh = paramBoolean;
    }
    
    public final boolean getConsentRefresh()
    {
      return consentRefresh;
    }
    
    public final List<String> getOptIns()
    {
      return optIns;
    }
    
    public final List<String> getOptOuts()
    {
      return optOuts;
    }
  }
  
  static abstract interface a
  {
    @e.b.f(a="/v1/optouts")
    public abstract e.b<OptOutRestAdapter.OptOutsDto> a();
    
    @o(a="/v1/optout/{type}")
    public abstract e.b<Void> a(@s(a="type") String paramString);
    
    @e.b.b(a="/v1/optout/{type}")
    public abstract e.b<Void> b(@s(a="type") String paramString);
  }
  
  static final class b
    extends l
    implements a<OptOutRestAdapter.a>
  {
    public static final b a = new b();
    
    b()
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.optout.OptOutRestAdapter
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */