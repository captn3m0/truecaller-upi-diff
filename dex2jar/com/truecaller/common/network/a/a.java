package com.truecaller.common.network.a;

import c.a.m;
import c.g.b.k;
import com.truecaller.common.network.util.AuthRequirement;
import java.util.List;
import okhttp3.z;

public abstract class a
  implements Comparable<a>
{
  public final boolean a;
  public final List<z> b;
  private final byte c;
  
  private a(byte paramByte, boolean paramBoolean, List<? extends z> paramList)
  {
    c = paramByte;
    a = paramBoolean;
    b = paramList;
  }
  
  public static final class a
    extends a
  {
    public final AuthRequirement c;
    
    public a(AuthRequirement paramAuthRequirement)
    {
      super(false, null, 6);
      c = paramAuthRequirement;
    }
    
    public final boolean equals(Object paramObject)
    {
      if (this != paramObject) {
        if ((paramObject instanceof a))
        {
          paramObject = (a)paramObject;
          if (k.a(c, c)) {}
        }
        else
        {
          return false;
        }
      }
      return true;
    }
    
    public final int hashCode()
    {
      AuthRequirement localAuthRequirement = c;
      if (localAuthRequirement != null) {
        return localAuthRequirement.hashCode();
      }
      return 0;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder("AuthRequired(authReq=");
      localStringBuilder.append(c);
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  public static final class b
    extends a
  {
    public final boolean c;
    
    public b(boolean paramBoolean)
    {
      super(false, null, 6);
      c = paramBoolean;
    }
    
    public final boolean equals(Object paramObject)
    {
      if (this != paramObject)
      {
        if ((paramObject instanceof b))
        {
          paramObject = (b)paramObject;
          int i;
          if (c == c) {
            i = 1;
          } else {
            i = 0;
          }
          if (i != 0) {
            return true;
          }
        }
        return false;
      }
      return true;
    }
    
    public final int hashCode()
    {
      throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder("CheckCredentials(allowed=");
      localStringBuilder.append(c);
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  public static final class c
    extends a
  {
    public static final c c = new c();
    
    private c()
    {
      super(true, null, 4);
    }
  }
  
  public static final class d
    extends a
  {
    public static final d c = new d();
    
    private d()
    {
      super(false, m.b(new z[] { z.b, z.d }), 2);
    }
  }
  
  public static final class e
    extends a
  {
    public final boolean c;
    
    public e(boolean paramBoolean)
    {
      super(false, null, 6);
      c = paramBoolean;
    }
    
    public final boolean equals(Object paramObject)
    {
      if (this != paramObject)
      {
        if ((paramObject instanceof e))
        {
          paramObject = (e)paramObject;
          int i;
          if (c == c) {
            i = 1;
          } else {
            i = 0;
          }
          if (i != 0) {
            return true;
          }
        }
        return false;
      }
      return true;
    }
    
    public final int hashCode()
    {
      throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder("EdgeLocation(allowed=");
      localStringBuilder.append(c);
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  public static final class f
    extends a
  {
    public static final f c = new f();
    
    private f()
    {
      super(false, null, 6);
    }
  }
  
  public static final class g
    extends a
  {
    public static final g c = new g();
    
    private g()
    {
      super(false, null, 6);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */