package com.truecaller.common.network.a;

import c.g.b.k;
import com.truecaller.common.network.util.AuthRequirement;

public final class a$a
  extends a
{
  public final AuthRequirement c;
  
  public a$a(AuthRequirement paramAuthRequirement)
  {
    super((byte)2, false, null, 6);
    c = paramAuthRequirement;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject) {
      if ((paramObject instanceof a))
      {
        paramObject = (a)paramObject;
        if (k.a(c, c)) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    AuthRequirement localAuthRequirement = c;
    if (localAuthRequirement != null) {
      return localAuthRequirement.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("AuthRequired(authReq=");
    localStringBuilder.append(c);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.a.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */