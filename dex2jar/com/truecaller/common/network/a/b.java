package com.truecaller.common.network.a;

import c.a.f;
import c.g.b.k;
import com.truecaller.common.network.util.AuthRequirement;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

public final class b
{
  private a.a a;
  private a.e b;
  private a.b c;
  
  public final b a()
  {
    b = new a.e(false);
    return this;
  }
  
  public final b a(AuthRequirement paramAuthRequirement)
  {
    k.b(paramAuthRequirement, "authRequirement");
    a = new a.a(paramAuthRequirement);
    return this;
  }
  
  public final b a(boolean paramBoolean)
  {
    c = new a.b(paramBoolean);
    return this;
  }
  
  public final SortedSet<a> b()
  {
    a[] arrayOfa = new a[7];
    Object localObject = a;
    if (localObject == null) {
      localObject = new a.a(AuthRequirement.NONE);
    }
    arrayOfa[0] = ((a)localObject);
    localObject = b;
    if (localObject == null) {
      localObject = new a.e(true);
    }
    arrayOfa[1] = ((a)localObject);
    localObject = c;
    if (localObject == null) {
      localObject = new a.b(true);
    }
    arrayOfa[2] = ((a)localObject);
    arrayOfa[3] = ((a)a.g.c);
    arrayOfa[4] = ((a)a.f.c);
    arrayOfa[5] = ((a)a.d.c);
    arrayOfa[6] = ((a)a.c.c);
    k.b(arrayOfa, "elements");
    return (SortedSet)f.b(arrayOfa, (Collection)new TreeSet());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */