package com.truecaller.common.network.account;

import android.support.annotation.Keep;

@Keep
public final class ExchangeCredentialsRequestDto
{
  private final String installationId;
  
  public ExchangeCredentialsRequestDto(String paramString)
  {
    installationId = paramString;
  }
  
  public final String getInstallationId()
  {
    return installationId;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.account.ExchangeCredentialsRequestDto
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */