package com.truecaller.common.network.account;

import android.support.annotation.Keep;
import java.util.List;

@Keep
public final class DeviceDto
{
  private final String deviceId;
  private final String language;
  private final String manufacturer;
  private final String model;
  private final String osName;
  private final String osVersion;
  private final List<String> simSerials;
  
  public DeviceDto(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, List<String> paramList)
  {
    deviceId = paramString1;
    osName = paramString2;
    osVersion = paramString3;
    manufacturer = paramString4;
    model = paramString5;
    language = paramString6;
    simSerials = paramList;
  }
  
  public final String getDeviceId()
  {
    return deviceId;
  }
  
  public final String getLanguage()
  {
    return language;
  }
  
  public final String getManufacturer()
  {
    return manufacturer;
  }
  
  public final String getModel()
  {
    return model;
  }
  
  public final String getOsName()
  {
    return osName;
  }
  
  public final String getOsVersion()
  {
    return osVersion;
  }
  
  public final List<String> getSimSerials()
  {
    return simSerials;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.account.DeviceDto
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */