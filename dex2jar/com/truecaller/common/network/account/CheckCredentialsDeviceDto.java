package com.truecaller.common.network.account;

import android.support.annotation.Keep;

@Keep
public final class CheckCredentialsDeviceDto
{
  private final String deviceId;
  private final String manufacturer;
  private final String model;
  
  public CheckCredentialsDeviceDto(String paramString1, String paramString2, String paramString3)
  {
    deviceId = paramString1;
    model = paramString2;
    manufacturer = paramString3;
  }
  
  public final String getDeviceId()
  {
    return deviceId;
  }
  
  public final String getManufacturer()
  {
    return manufacturer;
  }
  
  public final String getModel()
  {
    return model;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.account.CheckCredentialsDeviceDto
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */