package com.truecaller.common.network.account;

import android.support.annotation.Keep;
import java.util.List;

@Keep
public final class TokenResponseDto
{
  public static final a Companion = new a((byte)0);
  public static final String METHOD_CALL = "call";
  public static final String METHOD_SMS = "sms";
  public static final int STATUS_ALREADY_VERIFIED = 3;
  public static final int STATUS_PHONE_NUMBER_BLOCKED = 6;
  public static final int STATUS_PHONE_NUMBER_LIMIT_REACHED = 5;
  public static final int STATUS_REQUEST_ID_LIMIT_REACHED = 4;
  public static final int STATUS_TOKEN_INVALID = 11;
  public static final int STATUS_TOKEN_PENDING = 9;
  public static final int STATUS_TOKEN_RETRY_LIMIT_REACHED = 7;
  public static final int STATUS_TOKEN_SENT = 1;
  public static final int STATUS_TOKEN_TIMED_OUT = 8;
  public static final int STATUS_VERIFIED = 2;
  private final List<String> clis;
  private final String domain;
  private final String installationId;
  private final String message;
  private final String method;
  private final String parsedCountryCode;
  private final Long parsedPhoneNumber;
  private final String pattern;
  private final String requestId;
  private final int status;
  private final Long tokenTtl;
  private final Long ttl;
  private final Long userId;
  
  public TokenResponseDto(int paramInt, String paramString1, Long paramLong1, String paramString2, String paramString3, String paramString4, String paramString5, Long paramLong2, Long paramLong3, String paramString6, String paramString7, Long paramLong4, List<String> paramList)
  {
    status = paramInt;
    message = paramString1;
    parsedPhoneNumber = paramLong1;
    parsedCountryCode = paramString2;
    domain = paramString3;
    requestId = paramString4;
    method = paramString5;
    tokenTtl = paramLong2;
    ttl = paramLong3;
    pattern = paramString6;
    installationId = paramString7;
    userId = paramLong4;
    clis = paramList;
  }
  
  public final List<String> getClis()
  {
    return clis;
  }
  
  public final String getDomain()
  {
    return domain;
  }
  
  public final String getInstallationId()
  {
    return installationId;
  }
  
  public final String getMessage()
  {
    return message;
  }
  
  public final String getMethod()
  {
    return method;
  }
  
  public final String getParsedCountryCode()
  {
    return parsedCountryCode;
  }
  
  public final Long getParsedPhoneNumber()
  {
    return parsedPhoneNumber;
  }
  
  public final String getPattern()
  {
    return pattern;
  }
  
  public final String getRequestId()
  {
    return requestId;
  }
  
  public final int getStatus()
  {
    return status;
  }
  
  public final Long getTokenTtl()
  {
    return tokenTtl;
  }
  
  public final Long getTtl()
  {
    return ttl;
  }
  
  public final Long getUserId()
  {
    return userId;
  }
  
  public static final class a {}
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.account.TokenResponseDto
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */