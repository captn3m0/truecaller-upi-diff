package com.truecaller.common.network.account;

import c.g.b.k;
import com.truecaller.common.network.util.AuthRequirement;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.network.util.d;
import com.truecaller.common.network.util.h;
import e.b.f;
import e.b.o;
import e.b.p;
import java.util.ArrayList;
import java.util.List;
import okhttp3.ab;
import okhttp3.ab.a;
import okhttp3.ad;
import okhttp3.ae;
import okhttp3.v;
import okhttp3.v.a;

public final class a
{
  public static final a a = new a();
  
  public static a a(boolean paramBoolean)
  {
    com.truecaller.common.network.util.a locala = new com.truecaller.common.network.util.a().a(KnownEndpoints.ACCOUNT).a(a.class);
    Object localObject = new com.truecaller.common.network.a.b();
    ((com.truecaller.common.network.a.b)localObject).a(AuthRequirement.NONE);
    ((com.truecaller.common.network.a.b)localObject).a(paramBoolean);
    locala = locala.a(d.a((com.truecaller.common.network.a.b)localObject));
    localObject = (v)b.a;
    k.b(localObject, "interceptor");
    if (b == null) {
      b = ((List)new ArrayList());
    }
    List localList = b;
    if (localList != null) {
      localList.add(localObject);
    }
    return (a)locala.b(a.class);
  }
  
  public static e.b<ae> a()
  {
    return ((a)h.a(KnownEndpoints.ACCOUNT, a.class)).b();
  }
  
  public static e.b<CheckCredentialsResponseDto> a(CheckCredentialsRequestDto paramCheckCredentialsRequestDto)
  {
    k.b(paramCheckCredentialsRequestDto, "requestDto");
    com.truecaller.common.network.util.a locala = new com.truecaller.common.network.util.a().a(KnownEndpoints.ACCOUNT).a(a.class);
    com.truecaller.common.network.a.b localb = new com.truecaller.common.network.a.b();
    localb.a(AuthRequirement.REQUIRED);
    localb.a(false);
    return ((a)locala.a(d.a(localb)).b(a.class)).a(paramCheckCredentialsRequestDto);
  }
  
  public static e.b<ae> a(InstallationDetailsDto paramInstallationDetailsDto)
  {
    k.b(paramInstallationDetailsDto, "requestDto");
    return ((a)h.a(KnownEndpoints.ACCOUNT, a.class)).a(paramInstallationDetailsDto);
  }
  
  public static e.b<ae> b()
  {
    return ((a)h.a(KnownEndpoints.ACCOUNT, a.class)).a();
  }
  
  public static e.b<TemporaryTokenDto> c()
  {
    return ((a)h.a(KnownEndpoints.ACCOUNT, a.class)).c();
  }
  
  public static abstract interface a
  {
    @o(a="/v1/deactivateAndDelete")
    public abstract e.b<ae> a();
    
    @o(a="/v2.1/credentials/check")
    public abstract e.b<CheckCredentialsResponseDto> a(@e.b.a CheckCredentialsRequestDto paramCheckCredentialsRequestDto);
    
    @o(a="/v1/credentials/exchange")
    public abstract e.b<ExchangeCredentialsResponseDto> a(@e.b.a ExchangeCredentialsRequestDto paramExchangeCredentialsRequestDto);
    
    @p(a="/v1/installation")
    public abstract e.b<ae> a(@e.b.a InstallationDetailsDto paramInstallationDetailsDto);
    
    @o(a="/v1/sendToken")
    public abstract e.b<TokenResponseDto> a(@e.b.a SendTokenRequestDto paramSendTokenRequestDto);
    
    @o(a="/v1/verifyToken")
    public abstract e.b<TokenResponseDto> a(@e.b.a VerifyTokenRequestDto paramVerifyTokenRequestDto);
    
    @o(a="/v1/deactivate")
    public abstract e.b<ae> b();
    
    @f(a="/v1/token/crossDomain")
    public abstract e.b<TemporaryTokenDto> c();
  }
  
  static final class b
    implements v
  {
    public static final b a = new b();
    
    public final ad intercept(v.a parama)
    {
      return parama.a(parama.a().e().b("clientSecret", "lvc22mp3l1sfv6ujg83rd17btt").a());
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.account.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */