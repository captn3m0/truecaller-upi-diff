package com.truecaller.common.network.account;

import e.b;
import e.b.a;
import e.b.f;
import e.b.o;
import e.b.p;
import okhttp3.ae;

public abstract interface a$a
{
  @o(a="/v1/deactivateAndDelete")
  public abstract b<ae> a();
  
  @o(a="/v2.1/credentials/check")
  public abstract b<CheckCredentialsResponseDto> a(@a CheckCredentialsRequestDto paramCheckCredentialsRequestDto);
  
  @o(a="/v1/credentials/exchange")
  public abstract b<ExchangeCredentialsResponseDto> a(@a ExchangeCredentialsRequestDto paramExchangeCredentialsRequestDto);
  
  @p(a="/v1/installation")
  public abstract b<ae> a(@a InstallationDetailsDto paramInstallationDetailsDto);
  
  @o(a="/v1/sendToken")
  public abstract b<TokenResponseDto> a(@a SendTokenRequestDto paramSendTokenRequestDto);
  
  @o(a="/v1/verifyToken")
  public abstract b<TokenResponseDto> a(@a VerifyTokenRequestDto paramVerifyTokenRequestDto);
  
  @o(a="/v1/deactivate")
  public abstract b<ae> b();
  
  @f(a="/v1/token/crossDomain")
  public abstract b<TemporaryTokenDto> c();
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.account.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */