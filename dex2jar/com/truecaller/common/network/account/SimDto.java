package com.truecaller.common.network.account;

import android.support.annotation.Keep;

@Keep
public final class SimDto
{
  private final String imsi;
  private final String mcc;
  private final String mnc;
  private final String msin;
  private final String operator;
  
  public SimDto(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    imsi = paramString1;
    operator = paramString2;
    mcc = paramString3;
    mnc = paramString4;
    msin = paramString5;
  }
  
  public final String getImsi()
  {
    return imsi;
  }
  
  public final String getMcc()
  {
    return mcc;
  }
  
  public final String getMnc()
  {
    return mnc;
  }
  
  public final String getMsin()
  {
    return msin;
  }
  
  public final String getOperator()
  {
    return operator;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.account.SimDto
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */