package com.truecaller.common.network.account;

import android.support.annotation.Keep;

@Keep
public final class AppDto
{
  private final Integer buildVersion;
  private final int majorVersion;
  private final int minorVersion;
  private final String store;
  
  public AppDto(int paramInt1, int paramInt2, Integer paramInteger, String paramString)
  {
    majorVersion = paramInt1;
    minorVersion = paramInt2;
    buildVersion = paramInteger;
    store = paramString;
  }
  
  public final Integer getBuildVersion()
  {
    return buildVersion;
  }
  
  public final int getMajorVersion()
  {
    return majorVersion;
  }
  
  public final int getMinorVersion()
  {
    return minorVersion;
  }
  
  public final String getStore()
  {
    return store;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.account.AppDto
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */