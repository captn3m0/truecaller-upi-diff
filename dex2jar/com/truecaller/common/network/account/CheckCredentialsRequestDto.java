package com.truecaller.common.network.account;

import android.support.annotation.Keep;

@Keep
public final class CheckCredentialsRequestDto
{
  public static final a Companion = new a((byte)0);
  public static final String REASON_RECEIVED_UNAUTHORIZED = "received_unauthorized";
  public static final String REASON_RESTORED_FROM_ACCOUNT_MANAGER = "restored_from_account_manager";
  public static final String REASON_RESTORED_FROM_AUTOBACKUP = "restored_from_autobackup";
  public static final String REASON_RESTORED_FROM_FILE = "restored_from_file";
  private final CheckCredentialsDeviceDto device;
  private final String endpoint;
  private final String reason;
  
  public CheckCredentialsRequestDto(String paramString1, String paramString2, CheckCredentialsDeviceDto paramCheckCredentialsDeviceDto)
  {
    reason = paramString1;
    endpoint = paramString2;
    device = paramCheckCredentialsDeviceDto;
  }
  
  public final CheckCredentialsDeviceDto getDevice()
  {
    return device;
  }
  
  public final String getEndpoint()
  {
    return endpoint;
  }
  
  public final String getReason()
  {
    return reason;
  }
  
  public static final class a {}
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.account.CheckCredentialsRequestDto
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */