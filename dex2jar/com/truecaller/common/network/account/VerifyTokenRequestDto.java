package com.truecaller.common.network.account;

import android.support.annotation.Keep;

@Keep
public final class VerifyTokenRequestDto
{
  private final String countryCode;
  private final String phoneNumber;
  private final String requestId;
  private final String token;
  
  public VerifyTokenRequestDto(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    requestId = paramString1;
    phoneNumber = paramString2;
    countryCode = paramString3;
    token = paramString4;
  }
  
  public final String getCountryCode()
  {
    return countryCode;
  }
  
  public final String getPhoneNumber()
  {
    return phoneNumber;
  }
  
  public final String getRequestId()
  {
    return requestId;
  }
  
  public final String getToken()
  {
    return token;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.account.VerifyTokenRequestDto
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */