package com.truecaller.common.network.account;

import android.support.annotation.Keep;

@Keep
public final class SendTokenRequestDto
{
  private final String countryCode;
  private final InstallationDetailsDto installationDetails;
  private final String phoneNumber;
  private final int sequenceNo;
  
  public SendTokenRequestDto(String paramString1, String paramString2, int paramInt, InstallationDetailsDto paramInstallationDetailsDto)
  {
    phoneNumber = paramString1;
    countryCode = paramString2;
    sequenceNo = paramInt;
    installationDetails = paramInstallationDetailsDto;
  }
  
  public final String getCountryCode()
  {
    return countryCode;
  }
  
  public final InstallationDetailsDto getInstallationDetails()
  {
    return installationDetails;
  }
  
  public final String getPhoneNumber()
  {
    return phoneNumber;
  }
  
  public final int getSequenceNo()
  {
    return sequenceNo;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.account.SendTokenRequestDto
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */