package com.truecaller.common.network.account;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION;
import c.u;
import com.truecaller.common.b.a;
import com.truecaller.common.h.au;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.h;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import javax.inject.Inject;

public final class c
  implements b
{
  private final Context a;
  private final com.truecaller.common.h.c b;
  private final h c;
  
  @Inject
  public c(Context paramContext, com.truecaller.common.h.c paramc, h paramh)
  {
    a = paramContext;
    b = paramc;
    c = paramh;
  }
  
  private final DeviceDto a(List<? extends SimInfo> paramList)
  {
    String str1 = com.truecaller.common.h.k.i(a);
    c.g.b.k.a(str1, "DeviceInfoUtils.getDeviceId(context)");
    String str2 = Build.VERSION.RELEASE;
    String str3 = com.truecaller.common.h.k.b();
    String str4 = com.truecaller.common.h.k.a();
    Object localObject1 = Locale.getDefault();
    c.g.b.k.a(localObject1, "Locale.getDefault()");
    localObject1 = ((Locale)localObject1).getLanguage();
    if (paramList != null)
    {
      Object localObject2 = (Iterable)paramList;
      paramList = (Collection)new ArrayList();
      localObject2 = ((Iterable)localObject2).iterator();
      while (((Iterator)localObject2).hasNext())
      {
        String str5 = nexth;
        if (str5 != null) {
          paramList.add(str5);
        }
      }
      paramList = (List)paramList;
      if (!(((Collection)paramList).isEmpty() ^ true)) {
        paramList = null;
      }
    }
    else
    {
      paramList = null;
    }
    return new DeviceDto(str1, "Android", str2, str3, str4, (String)localObject1, paramList);
  }
  
  @SuppressLint({"HardwareIds"})
  private static List<SimDto> b(List<? extends SimInfo> paramList)
  {
    if (paramList != null)
    {
      paramList = (Iterable)paramList;
      Collection localCollection = (Collection)new ArrayList();
      Iterator localIterator = paramList.iterator();
      for (;;)
      {
        boolean bool = localIterator.hasNext();
        int k = 1;
        if (!bool) {
          break;
        }
        Object localObject = (SimInfo)localIterator.next();
        paramList = (CharSequence)i;
        int j;
        if ((paramList != null) && (paramList.length() != 0)) {
          j = 0;
        } else {
          j = 1;
        }
        int i = k;
        if (j != 0)
        {
          paramList = (CharSequence)d;
          if ((paramList != null) && (paramList.length() != 0)) {
            j = 0;
          } else {
            j = 1;
          }
          i = k;
          if (j != 0) {
            if (e.length() >= 4) {
              i = k;
            } else {
              i = 0;
            }
          }
        }
        if (i != 0)
        {
          String str1 = i;
          String str2 = d;
          if (e.length() >= 4)
          {
            paramList = e;
            c.g.b.k.a(paramList, "simInfo.mccMnc");
            if (paramList != null)
            {
              paramList = paramList.substring(0, 3);
              c.g.b.k.a(paramList, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            }
            else
            {
              throw new u("null cannot be cast to non-null type java.lang.String");
            }
          }
          else
          {
            paramList = null;
          }
          if (e.length() >= 4)
          {
            localObject = e;
            c.g.b.k.a(localObject, "simInfo.mccMnc");
            if (localObject != null)
            {
              localObject = ((String)localObject).substring(3);
              c.g.b.k.a(localObject, "(this as java.lang.String).substring(startIndex)");
            }
            else
            {
              throw new u("null cannot be cast to non-null type java.lang.String");
            }
          }
          else
          {
            localObject = null;
          }
          paramList = new SimDto(str1, str2, paramList, (String)localObject, null);
        }
        else
        {
          paramList = null;
        }
        if (paramList != null) {
          localCollection.add(paramList);
        }
      }
      paramList = (List)localCollection;
      if ((((Collection)paramList).isEmpty() ^ true)) {
        return paramList;
      }
    }
    return null;
  }
  
  public final InstallationDetailsDto a()
  {
    List localList = c.h();
    c.g.b.k.a(localList, "it");
    if (!(((Collection)localList).isEmpty() ^ true)) {
      localList = null;
    }
    Object localObject = a.F();
    c.g.b.k.a(localObject, "ApplicationBase.getAppBase()");
    localObject = ((a)localObject).n();
    DeviceDto localDeviceDto = a(localList);
    Integer localInteger = b.e().a;
    if (localInteger != null)
    {
      int i = localInteger.intValue();
      localInteger = b.e().b;
      if (localInteger != null) {
        return new InstallationDetailsDto((String)localObject, localDeviceDto, new AppDto(i, localInteger.intValue(), b.e().c, b.f()), b(localList));
      }
      throw ((Throwable)new IllegalArgumentException("Minor build version is missing"));
    }
    throw ((Throwable)new IllegalArgumentException("Major build version is missing"));
  }
  
  public final CheckCredentialsDeviceDto b()
  {
    String str = com.truecaller.common.h.k.i(a);
    c.g.b.k.a(str, "DeviceInfoUtils.getDeviceId(context)");
    return new CheckCredentialsDeviceDto(str, com.truecaller.common.h.k.a(), com.truecaller.common.h.k.b());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.account.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */