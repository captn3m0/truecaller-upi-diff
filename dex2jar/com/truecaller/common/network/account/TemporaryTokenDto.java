package com.truecaller.common.network.account;

import android.support.annotation.Keep;

@Keep
public final class TemporaryTokenDto
{
  private final String token;
  
  public TemporaryTokenDto(String paramString)
  {
    token = paramString;
  }
  
  public final String getToken()
  {
    return token;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.account.TemporaryTokenDto
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */