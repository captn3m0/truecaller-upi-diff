package com.truecaller.common.network.account;

import android.support.annotation.Keep;

@Keep
public final class CheckCredentialsResponseDto
{
  private final String domain;
  private final String installationId;
  private final long nextCallDuration;
  private final Long ttl;
  
  public CheckCredentialsResponseDto(long paramLong, String paramString1, String paramString2, Long paramLong1)
  {
    nextCallDuration = paramLong;
    domain = paramString1;
    installationId = paramString2;
    ttl = paramLong1;
  }
  
  public final String getDomain()
  {
    return domain;
  }
  
  public final String getInstallationId()
  {
    return installationId;
  }
  
  public final long getNextCallDuration()
  {
    return nextCallDuration;
  }
  
  public final Long getTtl()
  {
    return ttl;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.account.CheckCredentialsResponseDto
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */