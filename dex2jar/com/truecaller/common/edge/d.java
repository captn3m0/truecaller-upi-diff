package com.truecaller.common.edge;

import android.content.Context;
import android.telephony.TelephonyManager;
import com.truecaller.common.account.r;
import javax.inject.Provider;

public final class d
  implements dagger.a.d<a>
{
  private final c a;
  private final Provider<r> b;
  private final Provider<com.truecaller.common.g.a> c;
  private final Provider<TelephonyManager> d;
  private final Provider<Context> e;
  
  private d(c paramc, Provider<r> paramProvider, Provider<com.truecaller.common.g.a> paramProvider1, Provider<TelephonyManager> paramProvider2, Provider<Context> paramProvider3)
  {
    a = paramc;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
  }
  
  public static d a(c paramc, Provider<r> paramProvider, Provider<com.truecaller.common.g.a> paramProvider1, Provider<TelephonyManager> paramProvider2, Provider<Context> paramProvider3)
  {
    return new d(paramc, paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.edge.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */