package com.truecaller.common.edge;

import android.content.Context;
import android.telephony.TelephonyManager;
import dagger.a.d;
import javax.inject.Provider;

public final class e
  implements d<TelephonyManager>
{
  private final Provider<Context> a;
  
  private e(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static e a(Provider<Context> paramProvider)
  {
    return new e(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.edge.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */