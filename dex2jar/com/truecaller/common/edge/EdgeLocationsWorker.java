package com.truecaller.common.edge;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import androidx.work.j;
import c.g.b.k;
import c.g.b.w;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.common.account.r;
import com.truecaller.common.background.TrackedWorker;
import com.truecaller.common.background.g;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;

public final class EdgeLocationsWorker
  extends TrackedWorker
{
  public static final a f = new a((byte)0);
  @Inject
  public a b;
  @Inject
  public com.truecaller.common.g.a c;
  @Inject
  public b d;
  @Inject
  public r e;
  
  public EdgeLocationsWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = com.truecaller.common.b.a.F();
    k.a(paramContext, "ApplicationBase.getAppBase()");
    paramContext.u().a(this);
  }
  
  public final b b()
  {
    b localb = d;
    if (localb == null) {
      k.a("analytics");
    }
    return localb;
  }
  
  public final boolean c()
  {
    r localr = e;
    if (localr == null) {
      k.a("accountManager");
    }
    return localr.c();
  }
  
  public final ListenableWorker.a d()
  {
    long l1 = System.currentTimeMillis();
    Object localObject1 = c;
    if (localObject1 == null) {
      k.a("coreSettings");
    }
    localObject1 = Long.valueOf(((com.truecaller.common.g.a)localObject1).a("edgeLocationsLastRequestTime", 0L));
    long l2 = ((Number)localObject1).longValue();
    int j = 1;
    if (l2 > 0L) {
      i = 1;
    } else {
      i = 0;
    }
    if (i == 0) {
      localObject1 = null;
    }
    Object localObject2;
    if (localObject1 != null)
    {
      l2 = ((Long)localObject1).longValue();
      if (l2 > l1)
      {
        localObject1 = b;
        if (localObject1 == null) {
          k.a("edgeLocationsManager");
        }
        ((a)localObject1).c();
        double d1 = TimeUnit.MILLISECONDS.toDays(l2 - l1);
        localObject1 = b();
        localObject2 = new e.a("IllegalEdgeLocationTtl").a(Double.valueOf(d1)).a();
        k.a(localObject2, "AnalyticsEvent.Builder(C…ueToSum(daysDiff).build()");
        ((b)localObject1).b((e)localObject2);
      }
      else
      {
        localObject1 = c;
        if (localObject1 == null) {
          k.a("coreSettings");
        }
        if (((com.truecaller.common.g.a)localObject1).a("edgeLocationsExpiration", 0L) > l1)
        {
          i = j;
          break label215;
        }
      }
    }
    int i = 0;
    label215:
    if (i != 0)
    {
      localObject1 = ListenableWorker.a.a();
      k.a(localObject1, "Result.success()");
      return (ListenableWorker.a)localObject1;
    }
    try
    {
      localObject1 = b;
      if (localObject1 == null) {
        k.a("edgeLocationsManager");
      }
      if (((a)localObject1).a()) {
        localObject1 = ListenableWorker.a.a();
      }
      for (localObject2 = "Result.success()";; localObject2 = "Result.failure()")
      {
        k.a(localObject1, (String)localObject2);
        return (ListenableWorker.a)localObject1;
        localObject1 = ListenableWorker.a.c();
      }
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
    localObject1 = ListenableWorker.a.c();
    k.a(localObject1, "Result.failure()");
    return (ListenableWorker.a)localObject1;
  }
  
  public static final class a
    implements com.truecaller.common.background.h
  {
    public final g a()
    {
      g localg = new g(w.a(EdgeLocationsWorker.class), org.a.a.h.b(3L));
      org.a.a.h localh = org.a.a.h.b(1L);
      k.a(localh, "Duration.standardHours(1)");
      return localg.a(localh).a(j.b);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.edge.EdgeLocationsWorker
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */