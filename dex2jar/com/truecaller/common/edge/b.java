package com.truecaller.common.edge;

import android.telephony.TelephonyManager;
import c.f;
import c.g.b.k;
import c.g.b.l;
import c.x;
import com.truecaller.common.network.edge.EdgeDto;
import com.truecaller.common.network.edge.EdgeDto.a;
import com.truecaller.log.UnmutedException.b;
import com.truecaller.log.d;
import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

public final class b
  implements a
{
  private final File b;
  private EdgeDto c;
  private final f d;
  private final com.truecaller.common.account.r e;
  private final com.truecaller.common.g.a f;
  private final TelephonyManager g;
  
  /* Error */
  public b(com.truecaller.common.account.r paramr, com.truecaller.common.g.a parama, TelephonyManager paramTelephonyManager, File paramFile)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 54
    //   3: invokestatic 59	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_2
    //   7: ldc 61
    //   9: invokestatic 59	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   12: aload_3
    //   13: ldc 63
    //   15: invokestatic 59	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   18: aload 4
    //   20: ldc 65
    //   22: invokestatic 59	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   25: aload_0
    //   26: invokespecial 67	java/lang/Object:<init>	()V
    //   29: aload_0
    //   30: aload_1
    //   31: putfield 69	com/truecaller/common/edge/b:e	Lcom/truecaller/common/account/r;
    //   34: aload_0
    //   35: aload_2
    //   36: putfield 71	com/truecaller/common/edge/b:f	Lcom/truecaller/common/g/a;
    //   39: aload_0
    //   40: aload_3
    //   41: putfield 73	com/truecaller/common/edge/b:g	Landroid/telephony/TelephonyManager;
    //   44: aload_0
    //   45: new 75	java/io/File
    //   48: dup
    //   49: aload 4
    //   51: ldc 77
    //   53: invokespecial 80	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   56: putfield 82	com/truecaller/common/edge/b:b	Ljava/io/File;
    //   59: aload_0
    //   60: getstatic 85	com/truecaller/common/edge/b$a:a	Lcom/truecaller/common/edge/b$a;
    //   63: checkcast 87	c/g/a/a
    //   66: invokestatic 92	c/g:a	(Lc/g/a/a;)Lc/f;
    //   69: putfield 94	com/truecaller/common/edge/b:d	Lc/f;
    //   72: aload_0
    //   73: getfield 82	com/truecaller/common/edge/b:b	Ljava/io/File;
    //   76: invokevirtual 98	java/io/File:exists	()Z
    //   79: ifeq +157 -> 236
    //   82: aload_0
    //   83: monitorenter
    //   84: aload_0
    //   85: getfield 82	com/truecaller/common/edge/b:b	Ljava/io/File;
    //   88: astore_1
    //   89: getstatic 103	c/n/d:a	Ljava/nio/charset/Charset;
    //   92: astore_2
    //   93: new 105	java/io/InputStreamReader
    //   96: dup
    //   97: new 107	java/io/FileInputStream
    //   100: dup
    //   101: aload_1
    //   102: invokespecial 110	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   105: checkcast 112	java/io/InputStream
    //   108: aload_2
    //   109: invokespecial 115	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V
    //   112: checkcast 117	java/io/Closeable
    //   115: astore_3
    //   116: aconst_null
    //   117: astore_2
    //   118: aload_2
    //   119: astore_1
    //   120: aload_3
    //   121: checkcast 105	java/io/InputStreamReader
    //   124: astore 4
    //   126: aload_2
    //   127: astore_1
    //   128: new 119	com/google/gson/f
    //   131: dup
    //   132: invokespecial 120	com/google/gson/f:<init>	()V
    //   135: aload 4
    //   137: checkcast 122	java/io/Reader
    //   140: ldc 124
    //   142: invokevirtual 127	com/google/gson/f:a	(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;
    //   145: checkcast 124	com/truecaller/common/network/edge/EdgeDto
    //   148: astore_2
    //   149: aload_3
    //   150: aconst_null
    //   151: invokestatic 132	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   154: aload_0
    //   155: aload_2
    //   156: invokespecial 135	com/truecaller/common/edge/b:b	(Lcom/truecaller/common/network/edge/EdgeDto;)Z
    //   159: pop
    //   160: aload_0
    //   161: monitorexit
    //   162: return
    //   163: astore_2
    //   164: goto +8 -> 172
    //   167: astore_2
    //   168: aload_2
    //   169: astore_1
    //   170: aload_2
    //   171: athrow
    //   172: aload_3
    //   173: aload_1
    //   174: invokestatic 132	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   177: aload_2
    //   178: athrow
    //   179: astore_1
    //   180: aload_0
    //   181: monitorexit
    //   182: aload_1
    //   183: athrow
    //   184: astore_1
    //   185: aload_1
    //   186: instanceof 137
    //   189: ifeq +40 -> 229
    //   192: new 139	java/lang/StringBuilder
    //   195: dup
    //   196: ldc -115
    //   198: invokespecial 144	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   201: astore_2
    //   202: aload_2
    //   203: aload_1
    //   204: invokevirtual 148	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   207: invokevirtual 152	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   210: pop
    //   211: new 154	com/truecaller/log/UnmutedException$b
    //   214: dup
    //   215: aload_2
    //   216: invokevirtual 157	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   219: invokespecial 158	com/truecaller/log/UnmutedException$b:<init>	(Ljava/lang/String;)V
    //   222: checkcast 52	java/lang/Throwable
    //   225: invokestatic 163	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   228: return
    //   229: aload_1
    //   230: checkcast 52	java/lang/Throwable
    //   233: invokestatic 163	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   236: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	237	0	this	b
    //   0	237	1	paramr	com.truecaller.common.account.r
    //   0	237	2	parama	com.truecaller.common.g.a
    //   0	237	3	paramTelephonyManager	TelephonyManager
    //   0	237	4	paramFile	File
    // Exception table:
    //   from	to	target	type
    //   120	126	163	finally
    //   128	149	163	finally
    //   170	172	163	finally
    //   120	126	167	java/lang/Throwable
    //   128	149	167	java/lang/Throwable
    //   84	116	179	finally
    //   149	160	179	finally
    //   172	179	179	finally
    //   82	84	184	java/lang/Exception
    //   160	162	184	java/lang/Exception
    //   180	184	184	java/lang/Exception
  }
  
  private final String a(EdgeDto paramEdgeDto, String paramString1, String paramString2)
  {
    if (paramEdgeDto != null) {
      try
      {
        paramEdgeDto = paramEdgeDto.getData();
        if (paramEdgeDto != null)
        {
          paramEdgeDto = (Map)paramEdgeDto.get(paramString1);
          if (paramEdgeDto != null)
          {
            paramEdgeDto = (EdgeDto.a)paramEdgeDto.get(paramString2);
            if (paramEdgeDto != null) {
              paramEdgeDto = a;
            }
          }
        }
      }
      finally {}
    }
    paramEdgeDto = null;
    if (paramEdgeDto != null)
    {
      paramEdgeDto = (String)c.a.m.e(paramEdgeDto);
      if ((paramEdgeDto != null) && (!c.n.m.a((CharSequence)paramEdgeDto))) {
        return paramEdgeDto;
      }
    }
    return null;
  }
  
  /* Error */
  private final boolean a(EdgeDto paramEdgeDto)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 82	com/truecaller/common/edge/b:b	Ljava/io/File;
    //   4: astore 4
    //   6: getstatic 103	c/n/d:a	Ljava/nio/charset/Charset;
    //   9: astore 5
    //   11: new 199	java/io/OutputStreamWriter
    //   14: dup
    //   15: new 201	java/io/FileOutputStream
    //   18: dup
    //   19: aload 4
    //   21: invokespecial 202	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   24: checkcast 204	java/io/OutputStream
    //   27: aload 5
    //   29: invokespecial 207	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V
    //   32: checkcast 117	java/io/Closeable
    //   35: astore 6
    //   37: aconst_null
    //   38: astore 5
    //   40: aload 5
    //   42: astore 4
    //   44: aload 6
    //   46: checkcast 199	java/io/OutputStreamWriter
    //   49: astore 7
    //   51: aload 5
    //   53: astore 4
    //   55: new 119	com/google/gson/f
    //   58: dup
    //   59: invokespecial 120	com/google/gson/f:<init>	()V
    //   62: aload_1
    //   63: aload 7
    //   65: checkcast 209	java/lang/Appendable
    //   68: invokevirtual 212	com/google/gson/f:a	(Ljava/lang/Object;Ljava/lang/Appendable;)V
    //   71: aload 5
    //   73: astore 4
    //   75: getstatic 217	c/x:a	Lc/x;
    //   78: astore 5
    //   80: aload 6
    //   82: aconst_null
    //   83: invokestatic 132	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   86: aload_1
    //   87: invokevirtual 221	com/truecaller/common/network/edge/EdgeDto:getTimeToLive	()I
    //   90: ifle +77 -> 167
    //   93: invokestatic 227	java/lang/System:currentTimeMillis	()J
    //   96: getstatic 233	java/util/concurrent/TimeUnit:SECONDS	Ljava/util/concurrent/TimeUnit;
    //   99: aload_1
    //   100: invokevirtual 221	com/truecaller/common/network/edge/EdgeDto:getTimeToLive	()I
    //   103: i2l
    //   104: invokevirtual 237	java/util/concurrent/TimeUnit:toMillis	(J)J
    //   107: ladd
    //   108: lstore_2
    //   109: new 139	java/lang/StringBuilder
    //   112: dup
    //   113: ldc -17
    //   115: invokespecial 144	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   118: astore 4
    //   120: aload 4
    //   122: aload_1
    //   123: invokevirtual 242	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   126: pop
    //   127: aload 4
    //   129: ldc -12
    //   131: invokevirtual 152	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   134: pop
    //   135: aload 4
    //   137: new 246	java/util/Date
    //   140: dup
    //   141: lload_2
    //   142: invokespecial 249	java/util/Date:<init>	(J)V
    //   145: invokevirtual 242	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   148: pop
    //   149: aload 4
    //   151: invokevirtual 157	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   154: pop
    //   155: aload_0
    //   156: getfield 71	com/truecaller/common/edge/b:f	Lcom/truecaller/common/g/a;
    //   159: ldc -5
    //   161: lload_2
    //   162: invokeinterface 256 4 0
    //   167: iconst_1
    //   168: ireturn
    //   169: astore_1
    //   170: goto +9 -> 179
    //   173: astore_1
    //   174: aload_1
    //   175: astore 4
    //   177: aload_1
    //   178: athrow
    //   179: aload 6
    //   181: aload 4
    //   183: invokestatic 132	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   186: aload_1
    //   187: athrow
    //   188: astore_1
    //   189: aload_1
    //   190: checkcast 52	java/lang/Throwable
    //   193: invokestatic 163	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   196: iconst_0
    //   197: ireturn
    //   198: astore_1
    //   199: aload_1
    //   200: checkcast 52	java/lang/Throwable
    //   203: invokestatic 163	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   206: iconst_0
    //   207: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	208	0	this	b
    //   0	208	1	paramEdgeDto	EdgeDto
    //   108	54	2	l	long
    //   4	178	4	localObject1	Object
    //   9	70	5	localObject2	Object
    //   35	145	6	localCloseable	java.io.Closeable
    //   49	15	7	localOutputStreamWriter	java.io.OutputStreamWriter
    // Exception table:
    //   from	to	target	type
    //   44	51	169	finally
    //   55	71	169	finally
    //   75	80	169	finally
    //   177	179	169	finally
    //   44	51	173	java/lang/Throwable
    //   55	71	173	java/lang/Throwable
    //   75	80	173	java/lang/Throwable
    //   0	37	188	java/lang/RuntimeException
    //   80	167	188	java/lang/RuntimeException
    //   179	188	188	java/lang/RuntimeException
    //   0	37	198	java/io/IOException
    //   80	167	198	java/io/IOException
    //   179	188	198	java/io/IOException
  }
  
  private final boolean b(EdgeDto paramEdgeDto)
  {
    c = paramEdgeDto;
    if (paramEdgeDto != null) {
      paramEdgeDto = paramEdgeDto.getData();
    } else {
      paramEdgeDto = null;
    }
    return paramEdgeDto != null;
  }
  
  public final String a(String paramString1, String paramString2)
  {
    k.b(paramString1, "domain");
    k.b(paramString2, "edgeName");
    String str2 = a(c, paramString1, paramString2);
    String str1 = str2;
    if (str2 == null) {
      str1 = a((EdgeDto)d.b(), paramString1, paramString2);
    }
    return str1;
  }
  
  public final boolean a()
  {
    Object localObject3 = e.b();
    Object localObject1 = localObject3;
    if (localObject3 == null) {
      localObject1 = f.a("profileNumber");
    }
    if (localObject1 == null)
    {
      d.a((Throwable)new UnmutedException.b("Trying to call edge location without phone number"));
      return false;
    }
    String str = e.a();
    localObject3 = str;
    if (str == null) {
      localObject3 = f.a("profileCountryIso");
    }
    if (localObject3 == null)
    {
      d.a((Throwable)new UnmutedException.b("Trying to call edge location without profile country code"));
      return false;
    }
    f.b("edgeLocationsLastRequestTime", System.currentTimeMillis());
    localObject1 = com.truecaller.common.network.edge.a.a(g.getNetworkCountryIso(), (String)localObject3, (String)localObject1).c();
    k.a(localObject1, "response");
    if (!((e.r)localObject1).d()) {
      return false;
    }
    localObject1 = (EdgeDto)((e.r)localObject1).e();
    if (localObject1 == null) {
      return false;
    }
    k.a(localObject1, "response.body() ?: return false");
    try
    {
      if (b((EdgeDto)localObject1))
      {
        boolean bool = a((EdgeDto)localObject1);
        return bool;
      }
      localObject1 = x.a;
      return true;
    }
    finally {}
  }
  
  public final boolean a(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "domain");
    k.b(paramString2, "edgeName");
    k.b(paramString3, "edgeHost");
    try
    {
      Object localObject2 = c;
      Object localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = new EdgeDto();
      }
      if (((EdgeDto)localObject1).getData() == null) {
        ((EdgeDto)localObject1).setData((Map)new LinkedHashMap());
      }
      localObject2 = ((EdgeDto)localObject1).getData();
      if (localObject2 != null)
      {
        localObject3 = (Map)((Map)localObject2).get(paramString1);
        localObject2 = localObject3;
        if (localObject3 != null) {}
      }
      else
      {
        localObject2 = (Map)new LinkedHashMap();
      }
      Object localObject3 = new EdgeDto.a();
      a = c.a.m.c(new String[] { paramString3 });
      ((Map)localObject2).put(paramString2, localObject3);
      paramString2 = ((EdgeDto)localObject1).getData();
      if (paramString2 != null) {
        paramString2.put(paramString1, localObject2);
      }
      c = ((EdgeDto)localObject1);
      boolean bool = a((EdgeDto)localObject1);
      return bool;
    }
    finally {}
  }
  
  public final void b(String paramString1, String paramString2)
  {
    k.b(paramString1, "domain");
    k.b(paramString2, "edgeName");
    for (;;)
    {
      try
      {
        EdgeDto localEdgeDto = c;
        if (localEdgeDto != null)
        {
          Map localMap = localEdgeDto.getData();
          if (localMap != null)
          {
            paramString1 = (Map)localMap.get(paramString1);
            if (paramString1 != null)
            {
              paramString1 = (EdgeDto.a)paramString1.remove(paramString2);
              if (paramString1 != null) {
                a(localEdgeDto);
              }
              paramString1 = x.a;
            }
          }
        }
        else
        {
          return;
        }
      }
      finally {}
      paramString1 = null;
    }
  }
  
  public final boolean b()
  {
    return (f.a("edgeLocationsLastRequestTime", 0L) != 0L) && (c != null);
  }
  
  public final void c()
  {
    try
    {
      b.delete();
      c = null;
      x localx = x.a;
      f.d("edgeLocationsExpiration");
      f.d("edgeLocationsLastRequestTime");
      return;
    }
    finally {}
  }
  
  static final class a
    extends l
    implements c.g.a.a<EdgeDto>
  {
    public static final a a = new a();
    
    a()
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.edge.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */