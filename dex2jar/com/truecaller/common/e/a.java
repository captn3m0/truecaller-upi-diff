package com.truecaller.common.e;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public final class a
{
  private static final char[] a = { 64991 };
  private final Paint b;
  private final Rect c;
  
  public a()
  {
    this(new Paint());
  }
  
  private a(Paint paramPaint)
  {
    b = paramPaint;
    c = new Rect();
    b.getTextBounds(a, 0, 1, c);
  }
  
  private void a(char[] paramArrayOfChar, Bitmap paramBitmap)
  {
    paramBitmap.eraseColor(0);
    new Canvas(paramBitmap).drawText(paramArrayOfChar, 0, paramArrayOfChar.length, 0.0F, 0.0F, b);
  }
  
  private boolean a(char paramChar)
  {
    Object localObject2 = new Rect();
    char[] arrayOfChar = new char[1];
    arrayOfChar[0] = paramChar;
    b.getTextBounds(arrayOfChar, 0, 1, (Rect)localObject2);
    if (c.equals(localObject2))
    {
      if ((((Rect)localObject2).width() == 0) && (((Rect)localObject2).height() == 0)) {
        return false;
      }
      Bitmap localBitmap = Bitmap.createBitmap(((Rect)localObject2).width(), ((Rect)localObject2).height(), Bitmap.Config.ARGB_8888);
      localObject2 = Bitmap.createBitmap(((Rect)localObject2).width(), ((Rect)localObject2).height(), Bitmap.Config.ARGB_8888);
      try
      {
        a(arrayOfChar, localBitmap);
        a(a, (Bitmap)localObject2);
        boolean bool = localBitmap.sameAs((Bitmap)localObject2);
        return true ^ bool;
      }
      finally
      {
        localBitmap.recycle();
        ((Bitmap)localObject2).recycle();
      }
    }
    return true;
  }
  
  public final boolean a(String paramString)
  {
    paramString = paramString.toCharArray();
    int j = paramString.length;
    int i = 0;
    while (i < j)
    {
      char c1 = paramString[i];
      if ((!Character.isWhitespace(c1)) && (!a(c1))) {
        return false;
      }
      i += 1;
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.e.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */