package com.truecaller.common.e;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build.VERSION;
import c.n.m;
import c.u;
import com.truecaller.common.h.h;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public final class e
{
  public static c a(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "countryIso");
    c.g.b.k.b(paramString2, "usedLanguageISO");
    paramString1 = h.d(paramString1).iterator();
    String str;
    while (paramString1.hasNext())
    {
      str = (String)paramString1.next();
      if (!m.a(str, paramString2, true))
      {
        c.g.b.k.a(str, "lang");
        return b(str);
      }
    }
    paramString1 = d.a;
    paramString1 = h.d(d.b()).iterator();
    while (paramString1.hasNext())
    {
      str = (String)paramString1.next();
      if (!m.a(str, paramString2, true))
      {
        c.g.b.k.a(str, "lang");
        return b(str);
      }
    }
    paramString1 = d.a;
    return d.a();
  }
  
  public static Locale a(Context paramContext)
  {
    c.g.b.k.b(paramContext, "context");
    try
    {
      paramContext = paramContext.getResources();
      c.g.b.k.a(paramContext, "context.resources");
      paramContext = getConfigurationlocale;
      return paramContext;
    }
    catch (NullPointerException paramContext)
    {
      for (;;) {}
    }
    return null;
  }
  
  public static Locale a(Locale paramLocale)
  {
    c.g.b.k.b(paramLocale, "defaultLocale");
    Locale localLocale = c();
    if (localLocale == null) {
      return paramLocale;
    }
    return localLocale;
  }
  
  public static Set<String> a()
  {
    Locale[] arrayOfLocale = Locale.getAvailableLocales();
    HashSet localHashSet = new HashSet();
    int j = arrayOfLocale.length;
    int i = 0;
    while (i < j)
    {
      Object localObject2 = arrayOfLocale[i];
      c.g.b.k.a(localObject2, "locale");
      String str = ((Locale)localObject2).getLanguage();
      c.g.b.k.a(str, "locale.language");
      if (str != null)
      {
        str = str.toLowerCase();
        c.g.b.k.a(str, "(this as java.lang.String).toLowerCase()");
        Object localObject1 = ((Locale)localObject2).getCountry();
        c.g.b.k.a(localObject1, "locale.country");
        if (localObject1 != null)
        {
          localObject1 = ((String)localObject1).toLowerCase();
          c.g.b.k.a(localObject1, "(this as java.lang.String).toLowerCase()");
          localObject2 = ((Locale)localObject2).getVariant();
          c.g.b.k.a(localObject2, "locale.variant");
          if (localObject2 != null)
          {
            localObject2 = ((String)localObject2).toLowerCase();
            c.g.b.k.a(localObject2, "(this as java.lang.String).toLowerCase()");
            localHashSet.add(str);
            if (c.g.b.k.a(str, "zh")) {
              if (((String)localObject1).length() > 2)
              {
                localObject1 = new StringBuilder();
                ((StringBuilder)localObject1).append(str);
                ((StringBuilder)localObject1).append("_");
                ((StringBuilder)localObject1).append((String)localObject2);
                localHashSet.add(((StringBuilder)localObject1).toString());
              }
              else
              {
                localObject2 = new StringBuilder();
                ((StringBuilder)localObject2).append(str);
                ((StringBuilder)localObject2).append("_");
                ((StringBuilder)localObject2).append((String)localObject1);
                localHashSet.add(((StringBuilder)localObject2).toString());
              }
            }
            i += 1;
          }
          else
          {
            throw new u("null cannot be cast to non-null type java.lang.String");
          }
        }
        else
        {
          throw new u("null cannot be cast to non-null type java.lang.String");
        }
      }
      else
      {
        throw new u("null cannot be cast to non-null type java.lang.String");
      }
    }
    return (Set)localHashSet;
  }
  
  public static boolean a(String paramString)
  {
    c.g.b.k.b(paramString, "iso");
    Object localObject1 = d.a;
    localObject1 = d.c();
    int k = localObject1.length;
    int i = 0;
    while (i < k)
    {
      Object localObject2 = localObject1[i];
      String str = com.truecaller.common.h.k.b();
      c.g.b.k.a(str, "DeviceInfoUtils.getDeviceManufacturer()");
      Locale localLocale = Locale.ENGLISH;
      c.g.b.k.a(localLocale, "Locale.ENGLISH");
      if (str != null)
      {
        str = str.toLowerCase(localLocale);
        c.g.b.k.a(str, "(this as java.lang.String).toLowerCase(locale)");
        if (m.b(str, localObject2[0], false))
        {
          int m = localObject2.length;
          int j = 0;
          while (j < m)
          {
            if (c.g.b.k.a(localObject2[j], paramString)) {
              return false;
            }
            j += 1;
          }
        }
        i += 1;
      }
      else
      {
        throw new u("null cannot be cast to non-null type java.lang.String");
      }
    }
    return true;
  }
  
  public static c b(String paramString)
  {
    c.g.b.k.b(paramString, "languageISO");
    Iterator localIterator = b().iterator();
    while (localIterator.hasNext())
    {
      c localc = (c)localIterator.next();
      if (m.a(paramString, b, true)) {
        return localc;
      }
    }
    paramString = d.a;
    return d.a();
  }
  
  public static List<c> b()
  {
    Object localObject1 = d.a;
    Object localObject2 = d.g();
    localObject1 = new ArrayList();
    Set localSet = a();
    localObject2 = ((List)localObject2).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      c localc = (c)((Iterator)localObject2).next();
      String str = b;
      if (str != null)
      {
        str = str.toLowerCase();
        c.g.b.k.a(str, "(this as java.lang.String).toLowerCase()");
        if ((localSet.contains(str)) && (a(str))) {
          ((ArrayList)localObject1).add(localc);
        }
      }
      else
      {
        throw new u("null cannot be cast to non-null type java.lang.String");
      }
    }
    return (List)localObject1;
  }
  
  public static Locale c()
  {
    try
    {
      Object localObject1 = Class.forName("android.app.ActivityManagerNative");
      Method localMethod = ((Class)localObject1).getMethod("getDefault", new Class[0]);
      c.g.b.k.a(localMethod, "getDefault");
      localMethod.setAccessible(true);
      Object localObject2 = localMethod.invoke(localObject1, new Object[0]);
      if (Build.VERSION.SDK_INT >= 26) {
        localObject1 = Class.forName(localObject2.getClass().getName());
      }
      localObject1 = ((Class)localObject1).getMethod("getConfiguration", new Class[0]).invoke(localMethod.invoke(null, new Object[0]), new Object[0]);
      if (localObject1 != null) {
        return locale;
      }
      throw new u("null cannot be cast to non-null type android.content.res.Configuration");
    }
    catch (Exception localException)
    {
      com.truecaller.log.d.a((Throwable)localException);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.e.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */