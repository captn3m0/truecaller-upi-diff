package com.truecaller.common.e;

import android.annotation.SuppressLint;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

@SuppressLint({"SwitchIntDef", "WrongConstant"})
public final class b
  extends Calendar
{
  public static int[] a = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
  public static int[] b = { 31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29 };
  static final int[] c = { 0, 1, 0, 1, 0, 1, 1, 7, 1, 0, 0, 0, 0, 0, 0, -46800000, 0 };
  static final int[] d = { 1, 292269054, 11, 52, 4, 28, 365, 6, 4, 1, 11, 23, 59, 59, 999, 50400000, 1200000 };
  static final int[] e = { 1, 292278994, 11, 53, 6, 31, 366, 6, 6, 1, 11, 23, 59, 59, 999, 50400000, 7200000 };
  private static TimeZone f = TimeZone.getDefault();
  private static boolean g = false;
  private GregorianCalendar h;
  
  private static int a(int paramInt1, int paramInt2)
  {
    switch (c(b(new a(paramInt2, 0, 1))))
    {
    default: 
      break;
    case 7: 
      paramInt1 -= 1;
      break;
    case 6: 
      paramInt1 += 5;
      break;
    case 5: 
      paramInt1 += 4;
      break;
    case 4: 
      paramInt1 += 3;
      break;
    case 3: 
      paramInt1 += 2;
      break;
    case 2: 
      paramInt1 += 1;
    }
    return paramInt1 / 7 + 1;
  }
  
  public static a a(a parama)
  {
    if ((b <= 11) && (b >= -11))
    {
      a -= 1600;
      c -= 1;
      int i = a;
      int j = (a + 3) / 4;
      int k = (a + 99) / 100;
      int n = (a + 399) / 400;
      int m = 0;
      i = i * 365 + j - k + n;
      j = 0;
      while (j < b)
      {
        i += a[j];
        j += 1;
      }
      j = i;
      if (b > 1) {
        if ((a % 4 != 0) || (a % 100 == 0))
        {
          j = i;
          if (a % 400 != 0) {}
        }
        else
        {
          j = i + 1;
        }
      }
      j = j + c - 79;
      i = j / 12053;
      j %= 12053;
      n = i * 33 + 979 + j / 1461 * 4;
      int i1 = j % 1461;
      k = m;
      i = i1;
      j = n;
      if (i1 >= 366)
      {
        i = i1 - 1;
        j = n + i / 365;
        i %= 365;
        k = m;
      }
      while (k < 11)
      {
        parama = b;
        if (i < parama[k]) {
          break;
        }
        i -= parama[k];
        k += 1;
      }
      return new a(j, k, i + 1);
    }
    throw new IllegalArgumentException();
  }
  
  private static boolean a(int paramInt)
  {
    paramInt %= 33;
    if ((paramInt != 1) && (paramInt != 5) && (paramInt != 9) && (paramInt != 13) && (paramInt != 17) && (paramInt != 22) && (paramInt != 26)) {
      return paramInt == 30;
    }
    return true;
  }
  
  private static a b(a parama)
  {
    if ((b <= 11) && (b >= -11))
    {
      a -= 979;
      c -= 1;
      int j = a * 365 + a / 33 * 8 + (a % 33 + 3) / 4;
      int i = 0;
      while (i < b)
      {
        j += b[i];
        i += 1;
      }
      i = j + c + 79;
      int k = i / 146097 * 400 + 1600;
      j = i % 146097;
      if (j >= 36525)
      {
        i = j - 1;
        k += i / 36524 * 100;
        j = i % 36524;
        if (j >= 365)
        {
          j += 1;
          i = 1;
        }
        else
        {
          i = 0;
        }
      }
      else
      {
        i = 1;
      }
      int n = k + j / 1461 * 4;
      int i1 = j % 1461;
      int m = n;
      j = i1;
      k = i;
      if (i1 >= 366)
      {
        i = i1 - 1;
        m = n + i / 365;
        j = i % 365;
        k = 0;
      }
      i = 0;
      for (;;)
      {
        i1 = a[i];
        if ((i == 1) && (k == 1)) {
          n = i;
        } else {
          n = 0;
        }
        if (j < i1 + n) {
          break;
        }
        i1 = a[i];
        if ((i == 1) && (k == 1)) {
          n = i;
        } else {
          n = 0;
        }
        j -= i1 + n;
        i += 1;
      }
      return new a(m, i, j + 1);
    }
    throw new IllegalArgumentException();
  }
  
  private static int c(a parama)
  {
    return new GregorianCalendar(a, b, c).get(7);
  }
  
  public final void add(int paramInt1, int paramInt2)
  {
    if (paramInt1 == 2)
    {
      paramInt1 = get(2) + paramInt2;
      add(1, paramInt1 / 12);
      paramInt1 %= 12;
      super.set(2, paramInt1);
      paramInt2 = get(5);
      localObject = b;
      if (paramInt2 > localObject[paramInt1])
      {
        super.set(5, localObject[paramInt1]);
        if ((get(2) == 11) && (a(get(1)))) {
          super.set(5, 30);
        }
      }
      complete();
      return;
    }
    if (paramInt1 == 1)
    {
      super.set(1, get(1) + paramInt2);
      if ((get(5) == 30) && (get(2) == 11) && (!a(get(1)))) {
        super.set(5, 29);
      }
      complete();
      return;
    }
    Object localObject = b(new a(get(1), get(2), get(5)));
    localObject = new GregorianCalendar(a, b, c, get(11), get(12), get(13));
    ((Calendar)localObject).add(paramInt1, paramInt2);
    a locala = a(new a(((Calendar)localObject).get(1), ((Calendar)localObject).get(2), ((Calendar)localObject).get(5)));
    super.set(1, a);
    super.set(2, b);
    super.set(5, c);
    super.set(11, ((Calendar)localObject).get(11));
    super.set(12, ((Calendar)localObject).get(12));
    super.set(13, ((Calendar)localObject).get(13));
    complete();
  }
  
  protected final void computeFields()
  {
    boolean bool = isTimeSet;
    if (!areFieldsSet)
    {
      setMinimalDaysInFirstWeek(1);
      setFirstDayOfWeek(7);
      int i = 0;
      int j = 0;
      while (i < fields[2])
      {
        j += b[i];
        i += 1;
      }
      super.set(6, j + fields[5]);
      super.set(7, c(b(new a(fields[1], fields[2], fields[5]))));
      if ((fields[5] > 0) && (fields[5] < 8)) {
        super.set(8, 1);
      }
      if ((7 < fields[5]) && (fields[5] < 15)) {
        super.set(8, 2);
      }
      if ((14 < fields[5]) && (fields[5] < 22)) {
        super.set(8, 3);
      }
      if ((21 < fields[5]) && (fields[5] < 29)) {
        super.set(8, 4);
      }
      if ((28 < fields[5]) && (fields[5] < 32)) {
        super.set(8, 5);
      }
      super.set(3, a(fields[6], fields[1]));
      super.set(4, a(fields[6], fields[1]) - a(fields[6] - fields[5], fields[1]) + 1);
      isTimeSet = bool;
    }
  }
  
  protected final void computeTime()
  {
    Object localObject;
    if ((!isTimeSet) && (!g))
    {
      localObject = GregorianCalendar.getInstance(f);
      if (!isSet(11)) {
        super.set(11, ((Calendar)localObject).get(11));
      }
      if (!isSet(10)) {
        super.set(10, ((Calendar)localObject).get(10));
      }
      if (!isSet(12)) {
        super.set(12, ((Calendar)localObject).get(12));
      }
      if (!isSet(13)) {
        super.set(13, ((Calendar)localObject).get(13));
      }
      if (!isSet(14)) {
        super.set(14, ((Calendar)localObject).get(14));
      }
      if (!isSet(15)) {
        super.set(15, ((Calendar)localObject).get(15));
      }
      if (!isSet(16)) {
        super.set(16, ((Calendar)localObject).get(16));
      }
      if (!isSet(9)) {
        super.set(9, ((Calendar)localObject).get(9));
      }
      if ((internalGet(11) >= 12) && (internalGet(11) <= 23))
      {
        super.set(9, 1);
        super.set(10, internalGet(11) - 12);
      }
      else
      {
        super.set(10, internalGet(11));
        super.set(9, 0);
      }
      a locala = b(new a(internalGet(1), internalGet(2), internalGet(5)));
      ((Calendar)localObject).set(a, b, c, internalGet(11), internalGet(12), internalGet(13));
      time = ((Calendar)localObject).getTimeInMillis();
      return;
    }
    if ((!isTimeSet) && (g))
    {
      if ((internalGet(11) >= 12) && (internalGet(11) <= 23))
      {
        super.set(9, 1);
        super.set(10, internalGet(11) - 12);
      }
      else
      {
        super.set(10, internalGet(11));
        super.set(9, 0);
      }
      h = new GregorianCalendar();
      super.set(15, f.getRawOffset());
      super.set(16, f.getDSTSavings());
      localObject = b(new a(internalGet(1), internalGet(2), internalGet(5)));
      h.set(a, b, c, internalGet(11), internalGet(12), internalGet(13));
      time = h.getTimeInMillis();
    }
  }
  
  public final int getGreatestMinimum(int paramInt)
  {
    return c[paramInt];
  }
  
  public final int getLeastMaximum(int paramInt)
  {
    return d[paramInt];
  }
  
  public final int getMaximum(int paramInt)
  {
    return e[paramInt];
  }
  
  public final int getMinimum(int paramInt)
  {
    return c[paramInt];
  }
  
  public final void roll(int paramInt1, int paramInt2)
  {
    if (paramInt2 == 0) {
      return;
    }
    if ((paramInt1 >= 0) && (paramInt1 < 15))
    {
      complete();
      int j = 30;
      int i = 0;
      int k = 0;
      int[] arrayOfInt;
      switch (paramInt1)
      {
      case 4: 
      case 8: 
      default: 
        throw new IllegalArgumentException();
      case 14: 
        paramInt2 = (internalGet(14) + paramInt2) % 1000;
        paramInt1 = paramInt2;
        if (paramInt2 < 0) {
          paramInt1 = paramInt2 + 1000;
        }
        super.set(14, paramInt1);
        return;
      case 13: 
        paramInt2 = (internalGet(13) + paramInt2) % 60;
        paramInt1 = paramInt2;
        if (paramInt2 < 0) {
          paramInt1 = paramInt2 + 60;
        }
        super.set(13, paramInt1);
        return;
      case 12: 
        paramInt2 = (internalGet(12) + paramInt2) % 60;
        paramInt1 = paramInt2;
        if (paramInt2 < 0) {
          paramInt1 = paramInt2 + 60;
        }
        super.set(12, paramInt1);
        return;
      case 11: 
        fields[11] = ((internalGet(11) + paramInt2) % 24);
        if (internalGet(11) < 0)
        {
          arrayOfInt = fields;
          arrayOfInt[11] += 24;
        }
        if (internalGet(11) < 12)
        {
          fields[9] = 0;
          fields[10] = internalGet(11);
        }
        else
        {
          fields[9] = 1;
          fields[10] = (internalGet(11) - 12);
        }
        break;
      case 10: 
        super.set(10, (internalGet(10) + paramInt2) % 12);
        if (internalGet(10) < 0)
        {
          arrayOfInt = fields;
          arrayOfInt[10] += 12;
        }
        if (internalGet(9) == 0)
        {
          super.set(11, internalGet(10));
          return;
        }
        super.set(11, internalGet(10) + 12);
        return;
      case 9: 
        if (paramInt2 % 2 == 0) {
          break label866;
        }
        if (internalGet(9) == 0) {
          fields[9] = 1;
        } else {
          fields[9] = 0;
        }
        if (get(9) == 0)
        {
          super.set(11, get(10));
          return;
        }
        super.set(11, get(10) + 12);
        return;
      case 7: 
        i = paramInt2 % 7;
        paramInt2 = k;
        paramInt1 = i;
        if (i < 0)
        {
          paramInt1 = i + 7;
          paramInt2 = k;
        }
        while (paramInt2 != paramInt1)
        {
          if (internalGet(7) == 6) {
            add(5, -6);
          } else {
            add(5, 1);
          }
          paramInt2 += 1;
        }
      case 6: 
        if (a(internalGet(1))) {
          paramInt1 = 366;
        } else {
          paramInt1 = 365;
        }
        paramInt2 = (internalGet(6) + paramInt2) % paramInt1;
        if (paramInt2 > 0) {
          paramInt1 = paramInt2;
        } else {
          paramInt1 = paramInt2 + paramInt1;
        }
        paramInt2 = 0;
        while (paramInt1 > i)
        {
          i += b[paramInt2];
          paramInt2 += 1;
        }
        super.set(2, paramInt2 - 1);
        super.set(5, b[internalGet(2)] - (i - paramInt1));
        return;
      case 5: 
        if ((get(2) >= 0) && (get(2) <= 5)) {
          paramInt1 = 31;
        } else {
          paramInt1 = 0;
        }
        i = paramInt1;
        if (6 <= get(2))
        {
          i = paramInt1;
          if (get(2) <= 10) {
            i = 30;
          }
        }
        if (get(2) == 11)
        {
          if (a(get(1))) {
            paramInt1 = j;
          } else {
            paramInt1 = 29;
          }
        }
        else {
          paramInt1 = i;
        }
        i = (get(5) + paramInt2) % paramInt1;
        paramInt2 = i;
        if (i < 0) {
          paramInt2 = i + paramInt1;
        }
        super.set(5, paramInt2);
        return;
      case 3: 
        return;
      case 2: 
        paramInt2 = (internalGet(2) + paramInt2) % 12;
        paramInt1 = paramInt2;
        if (paramInt2 < 0) {
          paramInt1 = paramInt2 + 12;
        }
        super.set(2, paramInt1);
        paramInt2 = b[paramInt1];
        paramInt1 = paramInt2;
        if (internalGet(2) == 11)
        {
          paramInt1 = paramInt2;
          if (a(internalGet(1))) {
            paramInt1 = 30;
          }
        }
        if (internalGet(5) <= paramInt1) {
          break label866;
        }
        super.set(5, paramInt1);
        return;
      }
      super.set(1, internalGet(1) + paramInt2);
      if ((internalGet(2) == 11) && (internalGet(5) == 30) && (!a(internalGet(1))))
      {
        super.set(5, 29);
        return;
      }
      label866:
      return;
    }
    throw new IllegalArgumentException();
  }
  
  public final void roll(int paramInt, boolean paramBoolean)
  {
    int i;
    if (paramBoolean) {
      i = 1;
    } else {
      i = -1;
    }
    roll(paramInt, i);
  }
  
  public final void set(int paramInt1, int paramInt2)
  {
    switch (paramInt1)
    {
    case 8: 
    case 9: 
    default: 
      super.set(paramInt1, paramInt2);
      return;
    case 10: 
    case 11: 
    case 12: 
    case 13: 
    case 14: 
    case 15: 
    case 16: 
      if ((isSet(1)) && (isSet(2)) && (isSet(5)) && (isSet(10)) && (isSet(11)) && (isSet(12)) && (isSet(13)) && (isSet(14)))
      {
        h = new GregorianCalendar();
        a locala = b(new a(internalGet(1), internalGet(2), internalGet(5)));
        h.set(a, b, c, internalGet(11), internalGet(12), internalGet(13));
        h.set(paramInt1, paramInt2);
        locala = a(new a(h.get(1), h.get(2), h.get(5)));
        super.set(1, a);
        super.set(2, b);
        super.set(5, c);
        super.set(11, h.get(11));
        super.set(12, h.get(12));
        super.set(13, h.get(13));
        return;
      }
      super.set(paramInt1, paramInt2);
      return;
    case 7: 
      if ((isSet(1)) && (isSet(2)) && (isSet(5)))
      {
        add(7, paramInt2 % 7 - get(7));
        return;
      }
      super.set(paramInt1, paramInt2);
      return;
    case 6: 
      if ((isSet(1)) && (isSet(2)) && (isSet(5)))
      {
        super.set(1, internalGet(1));
        super.set(2, 0);
        super.set(5, 0);
        add(paramInt1, paramInt2);
        return;
      }
      super.set(paramInt1, paramInt2);
      return;
    case 5: 
      super.set(paramInt1, 0);
      add(paramInt1, paramInt2);
      return;
    case 4: 
      if ((isSet(1)) && (isSet(2)) && (isSet(5)))
      {
        add(paramInt1, paramInt2 - get(4));
        return;
      }
      super.set(paramInt1, paramInt2);
      return;
    case 3: 
      if ((isSet(1)) && (isSet(2)) && (isSet(5)))
      {
        add(paramInt1, paramInt2 - get(3));
        return;
      }
      super.set(paramInt1, paramInt2);
      return;
    }
    if (paramInt2 > 11)
    {
      super.set(paramInt1, 11);
      add(paramInt1, paramInt2 - 11);
      return;
    }
    if (paramInt2 < 0)
    {
      super.set(paramInt1, 0);
      add(paramInt1, paramInt2);
      return;
    }
    super.set(paramInt1, paramInt2);
  }
  
  public static final class a
  {
    public int a;
    int b;
    public int c;
    
    public a(int paramInt1, int paramInt2, int paramInt3)
    {
      a = paramInt1;
      b = paramInt2;
      c = paramInt3;
    }
    
    public final String a()
    {
      switch (b)
      {
      default: 
        return "";
      case 11: 
        return "اسفند";
      case 10: 
        return "بهمن";
      case 9: 
        return "دی";
      case 8: 
        return "آذر";
      case 7: 
        return "آبان";
      case 6: 
        return "مهر";
      case 5: 
        return "شهريور";
      case 4: 
        return "مرداد";
      case 3: 
        return "تير";
      case 2: 
        return "خرداد";
      case 1: 
        return "ارديبهشت";
      }
      return "فروردين";
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(a);
      localStringBuilder.append("/");
      localStringBuilder.append(b);
      localStringBuilder.append("/");
      localStringBuilder.append(c);
      return localStringBuilder.toString();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.e.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */