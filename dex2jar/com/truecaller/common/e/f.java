package com.truecaller.common.e;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.text.TextUtils;
import java.util.Locale;

public final class f
{
  private static Locale a;
  private static Locale b;
  
  static
  {
    Locale localLocale = Locale.getDefault();
    a = localLocale;
    b = localLocale;
  }
  
  public static Locale a()
  {
    return b;
  }
  
  public static boolean a(Context paramContext, Locale paramLocale)
  {
    if (paramLocale == null) {
      return false;
    }
    b = paramLocale;
    boolean bool1 = a(paramContext.getApplicationContext().getResources(), paramLocale);
    boolean bool2 = a(Resources.getSystem(), paramLocale);
    boolean bool3 = a(paramContext.getResources(), paramLocale);
    Locale.setDefault(paramLocale);
    return (bool1) || (bool2) || (bool3);
  }
  
  private static boolean a(Resources paramResources, Locale paramLocale)
  {
    if (paramLocale == null) {
      return false;
    }
    Configuration localConfiguration = paramResources.getConfiguration();
    if (paramLocale.equals(locale)) {
      return false;
    }
    localConfiguration = new Configuration(localConfiguration);
    locale = paramLocale;
    localConfiguration.setLayoutDirection(paramLocale);
    paramResources.updateConfiguration(localConfiguration, paramResources.getDisplayMetrics());
    return true;
  }
  
  public static boolean b()
  {
    return TextUtils.getLayoutDirectionFromLocale(Locale.getDefault()) == 1;
  }
  
  public static boolean c()
  {
    Locale localLocale = b;
    return (localLocale != null) && (TextUtils.equals("fa", localLocale.getLanguage()));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.e.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */