package com.truecaller.common.e;

import c.a.m;
import java.util.List;

public final class d
{
  public static final d a = new d();
  private static final c b = new c("English", "en", "GB");
  private static final String c = "zz";
  private static final String[][] d = (String[][])new String[][] { { "lenovo", "pa", "gu", "si" } };
  private static final String[] e = { "zh_CN", "zh_TW" };
  private static final String[] f = { "ko" };
  private static final List<c> g = m.b(new c[] { b, new c("العربية", "ar", "SA"), new c("Български", "bg", "BG"), new c("简中", "zh_CN", "CN"), new c("繁中", "zh_TW", "CN"), new c("ελληνικά", "el", "GR"), new c("עברית", "iw", "IL"), new c("한국어", "ko", "KR"), new c("فارسی", "fa", "IR"), new c("Русский", "ru", "RU"), new c("Українська", "uk", "UA") });
  private static final List<c> h = m.b(new c[] { b, new c("العربية", "ar", "SA"), new c("বাংলা", "bn", "IN"), new c("Български", "bg", "BG"), new c("简中", "zh_CN", "CN"), new c("繁中", "zh_TW", "CN"), new c("Čeština", "cs", "CZ"), new c("Dansk", "da", "DK"), new c("Deutsch", "de", "DE"), new c("ગુજરાતી", "gu", "IN"), new c("Español", "es", "ES"), new c("Español (Latinoamericano)", "es", "MX"), new c("Suomi", "fi", "FI"), new c("Français", "fr", "FR"), new c("ελληνικά", "el", "GR"), new c("עברית", "iw", "IL"), new c("हिंदी", "hi", "IN"), new c("Hrvatski", "hr", "HR"), new c("Indonesia", "in", "ID"), new c("Italiano", "it", "IT"), new c("日本語", "ja", "JP"), new c("ಕನ್ನಡ", "kn", "IN"), new c("Kiswahili", "sw", "KE"), new c("한국어", "ko", "KR"), new c("मराठी", "mr", "IN"), new c("Magyar", "hu", "HU"), new c("Melayu", "ms", "MY"), new c("മലയാളം", "ml", "IN"), new c("Nederlands", "nl", "NL"), new c("नेपाली", "ne", "NP"), new c("Norsk", "nb", "NO"), new c("فارسی", "fa", "IR"), new c("Polski", "pl", "PL"), new c("Português (Brasil)", "pt", "BR"), new c("ਪੰਜਾਬੀ", "pa", "IN"), new c("Română", "ro", "RO"), new c("Русский", "ru", "RU"), new c("සිංහල", "si", "LK"), new c("Svenska", "sv", "SE"), new c("Tagalog", "tl", "PH"), new c("தமிழ்", "ta", "IN"), new c("తెలుగు", "te", "IN"), new c("ภาษาไทย", "th", "TH"), new c("Türkçe", "tr", "TR"), new c("Українська", "uk", "UA"), new c("اردو", "ur", "PK"), new c("tiếng Việt", "vi", "VN") });
  
  public static c a()
  {
    return b;
  }
  
  public static String b()
  {
    return c;
  }
  
  public static String[][] c()
  {
    return d;
  }
  
  public static String[] d()
  {
    return e;
  }
  
  public static String[] e()
  {
    return f;
  }
  
  public static List<c> f()
  {
    return g;
  }
  
  public static List<c> g()
  {
    return h;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.e.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */