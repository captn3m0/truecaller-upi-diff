package com.truecaller.common.profile;

import android.content.Context;
import javax.inject.Provider;

public final class d
  implements dagger.a.d<c>
{
  private final Provider<Context> a;
  
  private d(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static d a(Provider<Context> paramProvider)
  {
    return new d(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.profile.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */