package com.truecaller.common;

import android.content.Context;
import c.d.f;
import com.truecaller.common.account.k;
import com.truecaller.common.edge.EdgeLocationsWorker;
import com.truecaller.common.enhancedsearch.EnhancedSearchStateWorker;
import com.truecaller.common.h.ac;
import com.truecaller.common.h.aj;
import com.truecaller.common.h.an;
import com.truecaller.common.network.c;
import com.truecaller.common.tag.sync.AvailableTagsDownloadWorker;
import com.truecaller.common.tag.sync.TagKeywordsDownloadWorker;
import com.truecaller.common.tag.sync.TagsUploadWorker;
import com.truecaller.multisim.h;
import javax.inject.Named;

public abstract interface a
{
  public static final a a = a.a;
  
  public abstract void a(com.truecaller.common.background.d paramd);
  
  public abstract void a(EdgeLocationsWorker paramEdgeLocationsWorker);
  
  public abstract void a(EnhancedSearchStateWorker paramEnhancedSearchStateWorker);
  
  public abstract void a(AvailableTagsDownloadWorker paramAvailableTagsDownloadWorker);
  
  public abstract void a(TagKeywordsDownloadWorker paramTagKeywordsDownloadWorker);
  
  public abstract void a(TagsUploadWorker paramTagsUploadWorker);
  
  public abstract void a(com.truecaller.content.c.u paramu);
  
  @Named("ssl_patches_applied")
  public abstract boolean a();
  
  public abstract Context b();
  
  public abstract com.truecaller.common.g.a c();
  
  public abstract an d();
  
  public abstract com.truecaller.common.edge.a e();
  
  public abstract com.truecaller.common.profile.b f();
  
  public abstract h g();
  
  public abstract com.truecaller.common.h.u h();
  
  public abstract com.truecaller.common.h.r i();
  
  public abstract com.truecaller.common.network.account.b j();
  
  public abstract com.truecaller.common.account.r k();
  
  public abstract k l();
  
  public abstract c m();
  
  public abstract ac n();
  
  public abstract com.truecaller.common.network.optout.a o();
  
  public abstract com.truecaller.common.h.d p();
  
  public abstract aj q();
  
  @Named("UI")
  public abstract f r();
  
  @Named("Async")
  public abstract f s();
  
  @Named("IO")
  public abstract f t();
  
  @Named("CPU")
  public abstract f u();
  
  @Named("features_registry")
  public abstract com.truecaller.featuretoggles.e v();
  
  public abstract com.truecaller.common.e.e w();
  
  public static final class a {}
}

/* Location:
 * Qualified Name:     com.truecaller.common.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */