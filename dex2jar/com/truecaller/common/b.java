package com.truecaller.common;

import android.accounts.AccountManager;
import android.app.backup.BackupManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import com.truecaller.common.account.i;
import com.truecaller.common.account.l;
import com.truecaller.common.account.m;
import com.truecaller.common.account.p;
import com.truecaller.common.account.q;
import com.truecaller.common.b.a.a;
import com.truecaller.common.edge.EdgeLocationsWorker;
import com.truecaller.common.enhancedsearch.EnhancedSearchStateWorker;
import com.truecaller.common.h.ac;
import com.truecaller.common.h.ad;
import com.truecaller.common.h.ae;
import com.truecaller.common.h.aj;
import com.truecaller.common.h.an;
import com.truecaller.common.h.ar;
import com.truecaller.common.h.as;
import com.truecaller.common.h.at;
import com.truecaller.common.h.w;
import com.truecaller.common.h.x;
import com.truecaller.common.h.y;
import com.truecaller.common.h.z;
import com.truecaller.common.tag.sync.AvailableTagsDownloadWorker;
import com.truecaller.common.tag.sync.TagKeywordsDownloadWorker;
import com.truecaller.common.tag.sync.TagsUploadWorker;
import com.truecaller.content.aa;
import com.truecaller.multisim.h;
import com.truecaller.utils.n;
import dagger.a.g;
import java.io.File;
import javax.inject.Provider;

public final class b
  implements a
{
  private Provider<com.google.c.a.k> A;
  private Provider<n> B;
  private Provider<aj> C;
  private Provider<com.truecaller.common.h.v> D;
  private Provider<com.truecaller.common.h.u> E;
  private Provider<com.truecaller.common.account.b.a> F;
  private Provider<l> G;
  private Provider<com.truecaller.common.account.k> H;
  private Provider<com.truecaller.utils.d> I;
  private Provider<com.truecaller.common.a.b> J;
  private Provider<com.truecaller.common.a.a> K;
  private Provider<com.truecaller.common.h.d> L;
  private Provider<c.d.f> M;
  private Provider<com.truecaller.utils.s> N;
  private Provider<aa> O;
  private Provider<c.d.f> P;
  private Provider<c.d.f> Q;
  private Provider<c.d.f> R;
  private Provider<com.truecaller.common.e.e> S;
  private final a.a b;
  private final com.truecaller.analytics.d c;
  private final com.truecaller.utils.t d;
  private Provider<SharedPreferences> e;
  private Provider<com.truecaller.common.g.a> f;
  private Provider<com.truecaller.utils.a> g;
  private Provider<com.truecaller.common.account.r> h;
  private Provider<ad> i;
  private Provider<ac> j;
  private Provider<com.truecaller.common.network.c> k;
  private Provider<Context> l;
  private Provider<String> m;
  private Provider<String> n;
  private Provider<File> o;
  private Provider<AccountManager> p;
  private Provider<BackupManager> q;
  private Provider<p> r;
  private Provider<com.truecaller.analytics.b> s;
  private Provider<com.truecaller.common.account.a.b> t;
  private Provider<com.truecaller.common.account.s> u;
  private Provider<TelephonyManager> v;
  private Provider<com.truecaller.common.edge.a> w;
  private Provider<com.truecaller.common.profile.c> x;
  private Provider<com.truecaller.common.profile.b> y;
  private Provider<h> z;
  
  private b(a.a parama, com.truecaller.common.g.c paramc, com.truecaller.common.edge.c paramc1, x paramx, com.truecaller.content.a parama1, com.truecaller.common.d.a parama2, com.truecaller.utils.t paramt, com.truecaller.analytics.d paramd)
  {
    b = parama;
    c = paramd;
    d = paramt;
    e = dagger.a.c.a(com.truecaller.common.g.e.a(paramc));
    f = dagger.a.c.a(com.truecaller.common.g.d.a(paramc, e));
    g = new c(paramt);
    h = new dagger.a.b();
    i = ae.a(f, h);
    j = dagger.a.c.a(i);
    k = dagger.a.c.a(com.truecaller.common.b.c.a(parama, f, j));
    l = com.truecaller.common.b.b.a(parama);
    m = com.truecaller.common.account.b.a(l);
    n = com.truecaller.common.account.d.a(l);
    o = com.truecaller.common.account.e.a(l);
    p = com.truecaller.common.account.a.a(l);
    q = com.truecaller.common.account.f.a(l);
    r = q.a(m, n, o, p, q, f);
    s = new b(paramd);
    t = com.truecaller.common.account.a.c.a(p, n);
    u = com.truecaller.common.account.t.a(f, g, k, r, s, t, i.a());
    dagger.a.b.a(h, dagger.a.c.a(u));
    v = com.truecaller.common.edge.e.a(l);
    w = dagger.a.c.a(com.truecaller.common.edge.d.a(paramc1, h, f, v, l));
    x = com.truecaller.common.profile.d.a(l);
    y = dagger.a.c.a(x);
    z = dagger.a.c.a(y.a(paramx, l));
    A = dagger.a.c.a(z.a(paramx));
    B = new e(paramt);
    C = dagger.a.c.a(com.truecaller.common.b.d.a(parama, l, B));
    D = w.a(A, z, h, C);
    E = dagger.a.c.a(D);
    F = dagger.a.c.a(com.truecaller.common.account.c.a(l));
    G = m.a(F, i.a());
    H = dagger.a.c.a(G);
    I = new d(paramt);
    J = com.truecaller.common.a.c.a(s, I);
    K = dagger.a.c.a(J);
    L = dagger.a.c.a(at.a(l, K));
    M = dagger.a.c.a(com.truecaller.common.d.b.a(parama2));
    N = new f(paramt);
    O = dagger.a.c.a(com.truecaller.content.b.a(parama1, l, E, M, N));
    P = dagger.a.c.a(com.truecaller.common.d.e.a(parama2));
    Q = dagger.a.c.a(com.truecaller.common.d.d.a(parama2));
    R = dagger.a.c.a(com.truecaller.common.d.c.a(parama2));
    S = dagger.a.c.a(as.a());
  }
  
  public static a x()
  {
    return new a((byte)0);
  }
  
  public final void a(com.truecaller.common.background.d paramd)
  {
    b = ((com.truecaller.analytics.b)g.a(c.c(), "Cannot return null from a non-@Nullable component method"));
  }
  
  public final void a(EdgeLocationsWorker paramEdgeLocationsWorker)
  {
    b = ((com.truecaller.common.edge.a)w.get());
    c = ((com.truecaller.common.g.a)f.get());
    d = ((com.truecaller.analytics.b)g.a(c.c(), "Cannot return null from a non-@Nullable component method"));
    e = ((com.truecaller.common.account.r)h.get());
  }
  
  public final void a(EnhancedSearchStateWorker paramEnhancedSearchStateWorker)
  {
    b = ((com.truecaller.common.account.r)h.get());
    c = ((com.truecaller.common.g.a)f.get());
  }
  
  public final void a(AvailableTagsDownloadWorker paramAvailableTagsDownloadWorker)
  {
    b = ((com.truecaller.analytics.b)g.a(c.c(), "Cannot return null from a non-@Nullable component method"));
    c = ((com.truecaller.common.account.r)h.get());
  }
  
  public final void a(TagKeywordsDownloadWorker paramTagKeywordsDownloadWorker)
  {
    b = ((com.truecaller.analytics.b)g.a(c.c(), "Cannot return null from a non-@Nullable component method"));
    c = ((com.truecaller.common.account.r)h.get());
    d = ((ac)j.get());
  }
  
  public final void a(TagsUploadWorker paramTagsUploadWorker)
  {
    b = ((com.truecaller.common.account.r)h.get());
  }
  
  public final void a(com.truecaller.content.c.u paramu)
  {
    a = ((aa)O.get());
  }
  
  public final boolean a()
  {
    return com.truecaller.common.b.a.a(b.a);
  }
  
  public final Context b()
  {
    return com.truecaller.common.b.b.b(b);
  }
  
  public final com.truecaller.common.g.a c()
  {
    return (com.truecaller.common.g.a)f.get();
  }
  
  public final an d()
  {
    return new an((com.truecaller.utils.a)g.a(d.d(), "Cannot return null from a non-@Nullable component method"));
  }
  
  public final com.truecaller.common.edge.a e()
  {
    return (com.truecaller.common.edge.a)w.get();
  }
  
  public final com.truecaller.common.profile.b f()
  {
    return (com.truecaller.common.profile.b)y.get();
  }
  
  public final h g()
  {
    return (h)z.get();
  }
  
  public final com.truecaller.common.h.u h()
  {
    return (com.truecaller.common.h.u)E.get();
  }
  
  public final com.truecaller.common.h.r i()
  {
    return new com.truecaller.common.h.s((ac)j.get(), dagger.a.c.b(A));
  }
  
  public final com.truecaller.common.network.account.b j()
  {
    return new com.truecaller.common.network.account.c(com.truecaller.common.b.b.b(b), com.truecaller.common.account.v.a(), (h)z.get());
  }
  
  public final com.truecaller.common.account.r k()
  {
    return (com.truecaller.common.account.r)h.get();
  }
  
  public final com.truecaller.common.account.k l()
  {
    return (com.truecaller.common.account.k)H.get();
  }
  
  public final com.truecaller.common.network.c m()
  {
    return (com.truecaller.common.network.c)k.get();
  }
  
  public final ac n()
  {
    return (ac)j.get();
  }
  
  public final com.truecaller.common.network.optout.a o()
  {
    return new com.truecaller.common.network.optout.b();
  }
  
  public final com.truecaller.common.h.d p()
  {
    return (com.truecaller.common.h.d)L.get();
  }
  
  public final aj q()
  {
    return (aj)C.get();
  }
  
  public final c.d.f r()
  {
    return (c.d.f)P.get();
  }
  
  public final c.d.f s()
  {
    return (c.d.f)M.get();
  }
  
  public final c.d.f t()
  {
    return (c.d.f)Q.get();
  }
  
  public final c.d.f u()
  {
    return (c.d.f)R.get();
  }
  
  public final com.truecaller.featuretoggles.e v()
  {
    return ar.a(com.truecaller.common.b.b.b(b));
  }
  
  public final com.truecaller.common.e.e w()
  {
    return (com.truecaller.common.e.e)S.get();
  }
  
  public static final class a
  {
    public a.a a;
    public com.truecaller.common.g.c b;
    public com.truecaller.common.edge.c c;
    public x d;
    public com.truecaller.content.a e;
    public com.truecaller.common.d.a f;
    public com.truecaller.utils.t g;
    public com.truecaller.analytics.d h;
  }
  
  static final class b
    implements Provider<com.truecaller.analytics.b>
  {
    private final com.truecaller.analytics.d a;
    
    b(com.truecaller.analytics.d paramd)
    {
      a = paramd;
    }
  }
  
  static final class c
    implements Provider<com.truecaller.utils.a>
  {
    private final com.truecaller.utils.t a;
    
    c(com.truecaller.utils.t paramt)
    {
      a = paramt;
    }
  }
  
  static final class d
    implements Provider<com.truecaller.utils.d>
  {
    private final com.truecaller.utils.t a;
    
    d(com.truecaller.utils.t paramt)
    {
      a = paramt;
    }
  }
  
  static final class e
    implements Provider<n>
  {
    private final com.truecaller.utils.t a;
    
    e(com.truecaller.utils.t paramt)
    {
      a = paramt;
    }
  }
  
  static final class f
    implements Provider<com.truecaller.utils.s>
  {
    private final com.truecaller.utils.t a;
    
    f(com.truecaller.utils.t paramt)
    {
      a = paramt;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */