package com.truecaller.whoviewedme;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.support.v4.app.z.c;
import android.support.v4.app.z.d;
import android.support.v4.app.z.g;
import android.support.v4.content.b;
import c.g.b.k;
import c.n.m;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.callhistory.z;
import com.truecaller.data.access.c;
import com.truecaller.data.entity.Address;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.utils.extensions.d;
import java.io.Closeable;
import javax.inject.Inject;

public final class ab
{
  private final Context a;
  private final com.truecaller.i.e b;
  private final com.truecaller.utils.a c;
  private final c d;
  private final f<com.truecaller.callhistory.a> e;
  private final com.truecaller.notifications.a f;
  private final com.truecaller.notificationchannels.e g;
  
  @Inject
  public ab(Context paramContext, com.truecaller.i.e parame, com.truecaller.utils.a parama, c paramc, f<com.truecaller.callhistory.a> paramf, com.truecaller.notifications.a parama1, com.truecaller.notificationchannels.e parame1)
  {
    a = paramContext;
    b = parame;
    c = parama;
    d = paramc;
    e = paramf;
    f = parama1;
    g = parame1;
  }
  
  private static String a(Address paramAddress)
  {
    if (paramAddress != null)
    {
      if (paramAddress.getCityOrArea() != null) {
        return paramAddress.getCityOrArea();
      }
      String str = paramAddress.getCountryName();
      k.a(str, "address.countryName");
      if ((m.a((CharSequence)str) ^ true)) {
        return paramAddress.getCountryName();
      }
    }
    return null;
  }
  
  private final void a(String paramString, int paramInt)
  {
    Object localObject = WhoViewedMeActivity.a;
    localObject = WhoViewedMeActivity.a.a(a, WhoViewedMeLaunchContext.NOTIFICATION);
    PendingIntent localPendingIntent = PendingIntent.getActivity(a, 0, (Intent)localObject, 134217728);
    if (paramString == null) {
      paramString = a.getResources().getQuantityString(2131755038, paramInt, new Object[] { Integer.valueOf(paramInt) });
    } else {
      paramString = a.getResources().getQuantityString(2131755039, paramInt, new Object[] { Integer.valueOf(paramInt), paramString });
    }
    String str = a.getResources().getString(2131887334);
    localObject = g.o();
    if (localObject != null) {
      localObject = new z.d(a, (String)localObject);
    } else {
      localObject = new z.d(a);
    }
    paramString = (CharSequence)paramString;
    paramString = ((z.d)localObject).a(paramString).a(paramString);
    localObject = (CharSequence)str;
    paramString = paramString.b((CharSequence)localObject).f(b.c(a, 2131100594)).c(-1).a(BitmapFactory.decodeResource(a.getResources(), 2131234314)).a(2131234787).a((z.g)new z.c().b((CharSequence)localObject)).a(localPendingIntent).e().h();
    localObject = f;
    k.a(paramString, "notification");
    ((com.truecaller.notifications.a)localObject).a(null, 2131365540, paramString, "notificationWhoViewedMe", null);
    b.b("whoViewedMeNotificationTimestamp", c.a());
  }
  
  public final void a(int paramInt, Address paramAddress)
  {
    if (a(paramAddress) != null)
    {
      a(a(paramAddress), paramInt);
      return;
    }
    z localz = (z)((com.truecaller.callhistory.a)e.a()).b(6).d();
    if (localz != null)
    {
      if (localz.getCount() > 0)
      {
        paramAddress = null;
        while ((paramAddress == null) && (localz.moveToNext()))
        {
          c localc = d;
          Object localObject = localz.d();
          if (localObject != null)
          {
            localObject = ((HistoryEvent)localObject).s();
            if (localObject != null)
            {
              localObject = ((Contact)localObject).getTcId();
              if (localObject != null)
              {
                paramAddress = localc.a((String)localObject);
                if (paramAddress != null) {
                  paramAddress = paramAddress.g();
                } else {
                  paramAddress = null;
                }
                paramAddress = a(paramAddress);
              }
            }
          }
        }
        a(paramAddress, paramInt);
      }
      d.a((Closeable)localz);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.ab
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */