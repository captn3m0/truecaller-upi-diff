package com.truecaller.whoviewedme;

import com.truecaller.featuretoggles.d.b;
import com.truecaller.notificationchannels.e;
import javax.inject.Inject;

public final class h
  implements d.b
{
  private final String a;
  private final e b;
  
  @Inject
  public h(e parame)
  {
    b = parame;
    a = "featureWhoViewedMe";
  }
  
  public final String a()
  {
    return a;
  }
  
  public final void b()
  {
    b.p();
  }
  
  public final void c()
  {
    b.q();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */