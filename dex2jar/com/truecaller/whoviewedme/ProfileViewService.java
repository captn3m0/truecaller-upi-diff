package com.truecaller.whoviewedme;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.af;
import android.support.v4.app.v;
import c.g.b.k;
import c.u;
import com.truecaller.TrueApp;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.bp;
import com.truecaller.data.access.m;
import com.truecaller.data.entity.Contact;
import java.io.IOException;
import java.util.Iterator;
import javax.inject.Inject;

public final class ProfileViewService
  extends af
{
  public static final ProfileViewService.a n = new ProfileViewService.a((byte)0);
  @Inject
  public w j;
  @Inject
  public c k;
  @Inject
  public com.truecaller.analytics.b l;
  @Inject
  public m m;
  
  public static final void a(Context paramContext, long paramLong, boolean paramBoolean, int paramInt)
  {
    k.b(paramContext, "context");
    Intent localIntent = new Intent(paramContext, ProfileViewService.class).putExtra("EXTRA_AGGR_CONTACT_ID", paramLong).putExtra("EXTRA_SEARCH_TYPE", paramInt).putExtra("EXTRA_IS_PB_CONTACT", paramBoolean);
    v.a(paramContext.getApplicationContext(), ProfileViewService.class, 2131364005, localIntent);
  }
  
  public final void a(Intent paramIntent)
  {
    k.b(paramIntent, "intent");
    long l1 = paramIntent.getLongExtra("EXTRA_AGGR_CONTACT_ID", -1L);
    if (l1 < 0L) {
      return;
    }
    int i2 = paramIntent.getIntExtra("EXTRA_SEARCH_TYPE", 999);
    boolean bool2 = paramIntent.getBooleanExtra("EXTRA_IS_PB_CONTACT", true);
    paramIntent = m;
    if (paramIntent == null) {
      k.a("rawContactDao");
    }
    paramIntent = paramIntent.a(l1);
    k.a(paramIntent, "rawContactDao.getByAggre…edId(aggregatedContactId)");
    Iterator localIterator = ((Iterable)paramIntent).iterator();
    boolean bool1 = false;
    Object localObject1 = null;
    paramIntent = null;
    int i = 0;
    Object localObject2;
    while (localIterator.hasNext())
    {
      localObject2 = localIterator.next();
      Contact localContact = (Contact)localObject2;
      k.a(localContact, "c");
      int i1;
      if (localContact.getSource() == 1) {
        i1 = 1;
      } else {
        i1 = 0;
      }
      if (i1 != 0)
      {
        if (i != 0)
        {
          paramIntent = (Intent)localObject1;
          break label174;
        }
        paramIntent = (Intent)localObject2;
        i = 1;
      }
    }
    if (i == 0) {
      paramIntent = (Intent)localObject1;
    }
    label174:
    localObject1 = (Contact)paramIntent;
    if (localObject1 == null) {
      return;
    }
    paramIntent = ((Contact)localObject1).getTcId();
    if (paramIntent == null) {
      return;
    }
    k.a(paramIntent, "contact.tcId ?: return");
    boolean bool3 = ((Contact)localObject1).a(1);
    try
    {
      localObject1 = j;
      if (localObject1 == null) {
        k.a("whoViewedMeManager");
      }
      if (((w)localObject1).a(paramIntent, i2, bool3, bool2))
      {
        localObject1 = k;
        if (localObject1 == null) {
          k.a("profileViewDao");
        }
        ((c)localObject1).a(paramIntent);
        localObject1 = f.a;
        f.a(paramIntent).c();
        localObject2 = new e.a("WhoViewedMeEvent");
        localObject1 = l;
        if (localObject1 == null) {
          k.a("analytics");
        }
        localObject2 = ((e.a)localObject2).a();
        k.a(localObject2, "builder.build()");
        ((com.truecaller.analytics.b)localObject1).a((e)localObject2);
        bool1 = true;
      }
      localObject1 = j;
      if (localObject1 == null) {
        k.a("whoViewedMeManager");
      }
      ((w)localObject1).a(paramIntent, bool3, bool2, bool1);
      return;
    }
    catch (IOException paramIntent) {}
  }
  
  public final void onCreate()
  {
    super.onCreate();
    Application localApplication = getApplication();
    if (localApplication != null)
    {
      ((TrueApp)localApplication).a().a(this);
      return;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.TrueApp");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.ProfileViewService
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */