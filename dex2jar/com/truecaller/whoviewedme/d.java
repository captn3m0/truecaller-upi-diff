package com.truecaller.whoviewedme;

import android.content.ContentResolver;
import c.g.a.m;
import c.g.b.k;
import javax.inject.Inject;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.e;

public final class d
  implements c
{
  final ContentResolver a;
  
  @Inject
  public d(ContentResolver paramContentResolver)
  {
    a = paramContentResolver;
  }
  
  /* Error */
  public final int a(long paramLong)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 24	com/truecaller/whoviewedme/d:a	Landroid/content/ContentResolver;
    //   4: invokestatic 35	com/truecaller/content/TruecallerContract$n:d	()Landroid/net/Uri;
    //   7: iconst_1
    //   8: anewarray 37	java/lang/String
    //   11: dup
    //   12: iconst_0
    //   13: ldc 39
    //   15: aastore
    //   16: ldc 41
    //   18: iconst_1
    //   19: anewarray 37	java/lang/String
    //   22: dup
    //   23: iconst_0
    //   24: lload_1
    //   25: invokestatic 45	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   28: aastore
    //   29: aconst_null
    //   30: invokevirtual 51	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   33: astore 6
    //   35: aload 6
    //   37: checkcast 53	java/io/Closeable
    //   40: astore 5
    //   42: aconst_null
    //   43: astore 4
    //   45: aload 4
    //   47: astore_3
    //   48: new 55	java/util/ArrayList
    //   51: dup
    //   52: invokespecial 56	java/util/ArrayList:<init>	()V
    //   55: checkcast 58	java/util/Collection
    //   58: astore 7
    //   60: aload 4
    //   62: astore_3
    //   63: aload 6
    //   65: invokeinterface 64 1 0
    //   70: ifeq +27 -> 97
    //   73: aload 4
    //   75: astore_3
    //   76: aload 7
    //   78: aload 6
    //   80: ldc 66
    //   82: invokestatic 71	com/truecaller/utils/extensions/k:b	(Landroid/database/Cursor;Ljava/lang/String;)I
    //   85: invokestatic 76	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   88: invokeinterface 80 2 0
    //   93: pop
    //   94: goto -34 -> 60
    //   97: aload 4
    //   99: astore_3
    //   100: aload 7
    //   102: checkcast 82	java/util/List
    //   105: astore 4
    //   107: aload 5
    //   109: aconst_null
    //   110: invokestatic 87	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   113: aload 4
    //   115: invokestatic 93	c/a/m:e	(Ljava/util/List;)Ljava/lang/Object;
    //   118: checkcast 73	java/lang/Integer
    //   121: astore_3
    //   122: aload_3
    //   123: ifnull +8 -> 131
    //   126: aload_3
    //   127: invokevirtual 97	java/lang/Integer:intValue	()I
    //   130: ireturn
    //   131: iconst_0
    //   132: ireturn
    //   133: astore 4
    //   135: goto +11 -> 146
    //   138: astore 4
    //   140: aload 4
    //   142: astore_3
    //   143: aload 4
    //   145: athrow
    //   146: aload 5
    //   148: aload_3
    //   149: invokestatic 87	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   152: aload 4
    //   154: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	155	0	this	d
    //   0	155	1	paramLong	long
    //   47	102	3	localObject1	Object
    //   43	71	4	localList	java.util.List
    //   133	1	4	localObject2	Object
    //   138	15	4	localThrowable	Throwable
    //   40	107	5	localCloseable	java.io.Closeable
    //   33	46	6	localCursor	android.database.Cursor
    //   58	43	7	localCollection	java.util.Collection
    // Exception table:
    //   from	to	target	type
    //   48	60	133	finally
    //   63	73	133	finally
    //   76	94	133	finally
    //   100	107	133	finally
    //   143	146	133	finally
    //   48	60	138	java/lang/Throwable
    //   63	73	138	java/lang/Throwable
    //   76	94	138	java/lang/Throwable
    //   100	107	138	java/lang/Throwable
  }
  
  public final bn a()
  {
    return e.b((ag)bg.a, null, (m)new d.a(this, null), 3);
  }
  
  public final bn a(String paramString)
  {
    k.b(paramString, "tcId");
    return e.b((ag)bg.a, null, (m)new d.c(this, paramString, null), 3);
  }
  
  /* Error */
  public final long b()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 24	com/truecaller/whoviewedme/d:a	Landroid/content/ContentResolver;
    //   4: invokestatic 35	com/truecaller/content/TruecallerContract$n:d	()Landroid/net/Uri;
    //   7: iconst_1
    //   8: anewarray 37	java/lang/String
    //   11: dup
    //   12: iconst_0
    //   13: ldc -128
    //   15: aastore
    //   16: ldc -126
    //   18: aconst_null
    //   19: aconst_null
    //   20: invokevirtual 51	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   23: astore 4
    //   25: aload 4
    //   27: checkcast 53	java/io/Closeable
    //   30: astore_3
    //   31: aconst_null
    //   32: astore_2
    //   33: aload_2
    //   34: astore_1
    //   35: new 55	java/util/ArrayList
    //   38: dup
    //   39: invokespecial 56	java/util/ArrayList:<init>	()V
    //   42: checkcast 58	java/util/Collection
    //   45: astore 5
    //   47: aload_2
    //   48: astore_1
    //   49: aload 4
    //   51: invokeinterface 64 1 0
    //   56: ifeq +26 -> 82
    //   59: aload_2
    //   60: astore_1
    //   61: aload 5
    //   63: aload 4
    //   65: ldc -124
    //   67: invokestatic 136	com/truecaller/utils/extensions/k:c	(Landroid/database/Cursor;Ljava/lang/String;)J
    //   70: invokestatic 141	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   73: invokeinterface 80 2 0
    //   78: pop
    //   79: goto -32 -> 47
    //   82: aload_2
    //   83: astore_1
    //   84: aload 5
    //   86: checkcast 82	java/util/List
    //   89: astore_2
    //   90: aload_3
    //   91: aconst_null
    //   92: invokestatic 87	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   95: aload_2
    //   96: invokestatic 93	c/a/m:e	(Ljava/util/List;)Ljava/lang/Object;
    //   99: checkcast 138	java/lang/Long
    //   102: astore_1
    //   103: aload_1
    //   104: ifnull +8 -> 112
    //   107: aload_1
    //   108: invokevirtual 144	java/lang/Long:longValue	()J
    //   111: lreturn
    //   112: invokestatic 149	java/lang/System:currentTimeMillis	()J
    //   115: lreturn
    //   116: astore_2
    //   117: goto +8 -> 125
    //   120: astore_2
    //   121: aload_2
    //   122: astore_1
    //   123: aload_2
    //   124: athrow
    //   125: aload_3
    //   126: aload_1
    //   127: invokestatic 87	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   130: aload_2
    //   131: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	132	0	this	d
    //   34	93	1	localObject1	Object
    //   32	64	2	localList	java.util.List
    //   116	1	2	localObject2	Object
    //   120	11	2	localThrowable	Throwable
    //   30	96	3	localCloseable	java.io.Closeable
    //   23	41	4	localCursor	android.database.Cursor
    //   45	40	5	localCollection	java.util.Collection
    // Exception table:
    //   from	to	target	type
    //   35	47	116	finally
    //   49	59	116	finally
    //   61	79	116	finally
    //   84	90	116	finally
    //   123	125	116	finally
    //   35	47	120	java/lang/Throwable
    //   49	59	120	java/lang/Throwable
    //   61	79	120	java/lang/Throwable
    //   84	90	120	java/lang/Throwable
  }
  
  /* Error */
  public final long b(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 120
    //   3: invokestatic 19	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_0
    //   7: getfield 24	com/truecaller/whoviewedme/d:a	Landroid/content/ContentResolver;
    //   10: invokestatic 154	com/truecaller/content/TruecallerContract$ag:a	()Landroid/net/Uri;
    //   13: aconst_null
    //   14: ldc -100
    //   16: iconst_1
    //   17: anewarray 37	java/lang/String
    //   20: dup
    //   21: iconst_0
    //   22: aload_1
    //   23: aastore
    //   24: aconst_null
    //   25: invokevirtual 51	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   28: astore 4
    //   30: aload 4
    //   32: checkcast 53	java/io/Closeable
    //   35: astore_3
    //   36: aconst_null
    //   37: astore_2
    //   38: aload_2
    //   39: astore_1
    //   40: new 55	java/util/ArrayList
    //   43: dup
    //   44: invokespecial 56	java/util/ArrayList:<init>	()V
    //   47: checkcast 58	java/util/Collection
    //   50: astore 5
    //   52: aload_2
    //   53: astore_1
    //   54: aload 4
    //   56: invokeinterface 64 1 0
    //   61: ifeq +26 -> 87
    //   64: aload_2
    //   65: astore_1
    //   66: aload 5
    //   68: aload 4
    //   70: ldc -124
    //   72: invokestatic 136	com/truecaller/utils/extensions/k:c	(Landroid/database/Cursor;Ljava/lang/String;)J
    //   75: invokestatic 141	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   78: invokeinterface 80 2 0
    //   83: pop
    //   84: goto -32 -> 52
    //   87: aload_2
    //   88: astore_1
    //   89: aload 5
    //   91: checkcast 82	java/util/List
    //   94: astore_2
    //   95: aload_3
    //   96: aconst_null
    //   97: invokestatic 87	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   100: aload_2
    //   101: invokestatic 93	c/a/m:e	(Ljava/util/List;)Ljava/lang/Object;
    //   104: checkcast 138	java/lang/Long
    //   107: astore_1
    //   108: aload_1
    //   109: ifnull +8 -> 117
    //   112: aload_1
    //   113: invokevirtual 144	java/lang/Long:longValue	()J
    //   116: lreturn
    //   117: lconst_0
    //   118: lreturn
    //   119: astore_2
    //   120: goto +8 -> 128
    //   123: astore_2
    //   124: aload_2
    //   125: astore_1
    //   126: aload_2
    //   127: athrow
    //   128: aload_3
    //   129: aload_1
    //   130: invokestatic 87	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   133: aload_2
    //   134: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	135	0	this	d
    //   0	135	1	paramString	String
    //   37	64	2	localList	java.util.List
    //   119	1	2	localObject	Object
    //   123	11	2	localThrowable	Throwable
    //   35	94	3	localCloseable	java.io.Closeable
    //   28	41	4	localCursor	android.database.Cursor
    //   50	40	5	localCollection	java.util.Collection
    // Exception table:
    //   from	to	target	type
    //   40	52	119	finally
    //   54	64	119	finally
    //   66	84	119	finally
    //   89	95	119	finally
    //   126	128	119	finally
    //   40	52	123	java/lang/Throwable
    //   54	64	123	java/lang/Throwable
    //   66	84	123	java/lang/Throwable
    //   89	95	123	java/lang/Throwable
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */