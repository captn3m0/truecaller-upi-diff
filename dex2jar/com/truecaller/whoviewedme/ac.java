package com.truecaller.whoviewedme;

import android.content.Context;
import com.truecaller.androidactors.f;
import com.truecaller.data.access.c;
import dagger.a.d;
import javax.inject.Provider;

public final class ac
  implements d<ab>
{
  private final Provider<Context> a;
  private final Provider<com.truecaller.i.e> b;
  private final Provider<com.truecaller.utils.a> c;
  private final Provider<c> d;
  private final Provider<f<com.truecaller.callhistory.a>> e;
  private final Provider<com.truecaller.notifications.a> f;
  private final Provider<com.truecaller.notificationchannels.e> g;
  
  private ac(Provider<Context> paramProvider, Provider<com.truecaller.i.e> paramProvider1, Provider<com.truecaller.utils.a> paramProvider2, Provider<c> paramProvider3, Provider<f<com.truecaller.callhistory.a>> paramProvider4, Provider<com.truecaller.notifications.a> paramProvider5, Provider<com.truecaller.notificationchannels.e> paramProvider6)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
  }
  
  public static ac a(Provider<Context> paramProvider, Provider<com.truecaller.i.e> paramProvider1, Provider<com.truecaller.utils.a> paramProvider2, Provider<c> paramProvider3, Provider<f<com.truecaller.callhistory.a>> paramProvider4, Provider<com.truecaller.notifications.a> paramProvider5, Provider<com.truecaller.notificationchannels.e> paramProvider6)
  {
    return new ac(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.ac
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */