package com.truecaller.whoviewedme;

import com.truecaller.notificationchannels.e;
import dagger.a.d;
import javax.inject.Provider;

public final class i
  implements d<h>
{
  private final Provider<e> a;
  
  private i(Provider<e> paramProvider)
  {
    a = paramProvider;
  }
  
  public static i a(Provider<e> paramProvider)
  {
    return new i(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */