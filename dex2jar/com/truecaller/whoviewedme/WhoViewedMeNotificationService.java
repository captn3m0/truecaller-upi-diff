package com.truecaller.whoviewedme;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.af;
import android.support.v4.app.v;
import c.g.b.k;
import c.u;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.f;
import com.truecaller.bp;
import com.truecaller.callhistory.a;
import com.truecaller.callhistory.z;
import com.truecaller.common.h.q;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.log.AssertionUtil;
import com.truecaller.old.data.entity.Notification;
import com.truecaller.search.ContactDto;
import com.truecaller.search.ContactDto.Contact;
import com.truecaller.search.i;
import com.truecaller.search.j;
import e.r;
import java.io.Closeable;
import java.io.IOException;
import javax.inject.Inject;

public final class WhoViewedMeNotificationService
  extends af
{
  public static final WhoViewedMeNotificationService.a p = new WhoViewedMeNotificationService.a((byte)0);
  @Inject
  public f<a> j;
  @Inject
  public w k;
  @Inject
  public com.truecaller.i.e l;
  @Inject
  public com.truecaller.data.access.c m;
  @Inject
  public b n;
  @Inject
  public ab o;
  
  public static final void a(Context paramContext, Notification paramNotification)
  {
    k.b(paramContext, "context");
    k.b(paramNotification, "notification");
    Intent localIntent = new Intent(paramContext, WhoViewedMeNotificationService.class);
    localIntent.putExtra("EXTRA_TC_ID", paramNotification.a());
    v.a(paramContext, WhoViewedMeNotificationService.class, 2131365540, localIntent);
  }
  
  public final void a(Intent paramIntent)
  {
    k.b(paramIntent, "intent");
    Object localObject1 = k;
    if (localObject1 == null) {
      k.a("whoViewedMeManager");
    }
    String str;
    Object localObject2;
    boolean bool3;
    boolean bool2;
    if (((w)localObject1).a())
    {
      localObject1 = l;
      if (localObject1 == null) {
        k.a("generalSettings");
      }
      if (!((com.truecaller.i.e)localObject1).a("showProfileViewNotifications", true)) {
        return;
      }
      str = paramIntent.getStringExtra("EXTRA_TC_ID");
      paramIntent = m;
      if (paramIntent == null) {
        k.a("aggregateContactDao");
      }
      localObject2 = paramIntent.a(str);
      bool3 = false;
      bool2 = false;
      paramIntent = (Intent)localObject2;
      if (localObject2 == null)
      {
        paramIntent = i.a();
        k.a(str, "tcId");
        paramIntent = paramIntent.a(str);
      }
    }
    for (;;)
    {
      w localw;
      try
      {
        r localr = q.a(paramIntent);
        localw = null;
        if (localr == null) {
          break label634;
        }
        localObject1 = Boolean.valueOf(localr.d());
        paramIntent = (Intent)localObject2;
        if (com.truecaller.utils.extensions.c.a((Boolean)localObject1))
        {
          paramIntent = localw;
          if (localr != null)
          {
            localObject1 = (ContactDto)localr.e();
            paramIntent = localw;
            if (localObject1 != null)
            {
              k.a(localObject1, "contactDto");
              paramIntent = data;
              if (paramIntent == null) {
                break label640;
              }
              localObject1 = (ContactDto.Contact)c.a.m.a(paramIntent, 0);
              if (localObject1 == null) {
                break label646;
              }
              paramIntent = access;
              if ((c.n.m.a(paramIntent, "PRIVATE", true)) && (localObject1 != null)) {
                phones = null;
              }
              paramIntent = localw;
              if (localObject1 != null) {
                paramIntent = new Contact((ContactDto.Contact)localObject1);
              }
            }
          }
        }
      }
      catch (IOException paramIntent)
      {
        paramIntent.printStackTrace();
        paramIntent = (Intent)localObject2;
      }
      boolean bool1 = bool3;
      if (paramIntent != null)
      {
        localObject1 = j;
        if (localObject1 == null) {
          k.a("historyManager");
        }
        ((a)((f)localObject1).a()).a(new HistoryEvent(paramIntent, 6), paramIntent).d();
        localObject1 = j;
        if (localObject1 == null) {
          k.a("historyManager");
        }
        localObject1 = (z)((a)((f)localObject1).a()).b(6).d();
        bool1 = bool3;
        if (localObject1 != null)
        {
          if (((z)localObject1).getCount() > 0)
          {
            paramIntent = paramIntent.g();
            localObject2 = new Intent("com.truecaller.notification.action.NOTIFICATIONS_UPDATED");
            android.support.v4.content.d.a((Context)this).a((Intent)localObject2);
            localObject2 = k;
            if (localObject2 == null) {
              k.a("whoViewedMeManager");
            }
            if (((w)localObject2).f())
            {
              localObject2 = k;
              if (localObject2 == null) {
                k.a("whoViewedMeManager");
              }
              localObject2 = ((w)localObject2).h();
              localw = k;
              if (localw == null) {
                k.a("whoViewedMeManager");
              }
              int i = ((c)localObject2).a(localw.b());
              localObject2 = o;
              if (localObject2 == null) {
                k.a("whoViewedMeNotifier");
              }
              ((ab)localObject2).a(i, paramIntent);
            }
            localObject2 = new e.a("WhoViewedMeReceivedEvent");
            paramIntent = n;
            if (paramIntent == null) {
              k.a("analytics");
            }
            localObject2 = ((e.a)localObject2).a();
            k.a(localObject2, "builder.build()");
            paramIntent.b((com.truecaller.analytics.e)localObject2);
            bool1 = true;
          }
          else
          {
            AssertionUtil.reportWeirdnessButNeverCrash("No entries for Who Viewed Me after creating event");
            bool1 = bool2;
          }
          com.truecaller.utils.extensions.d.a((Closeable)localObject1);
        }
      }
      paramIntent = k;
      if (paramIntent == null) {
        k.a("whoViewedMeManager");
      }
      k.a(str, "tcId");
      paramIntent.a(str, bool1);
      return;
      return;
      label634:
      localObject1 = null;
      continue;
      label640:
      localObject1 = null;
      continue;
      label646:
      paramIntent = null;
    }
  }
  
  public final void onCreate()
  {
    super.onCreate();
    Application localApplication = getApplication();
    if (localApplication != null)
    {
      ((TrueApp)localApplication).a().a(this);
      return;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.TrueApp");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.WhoViewedMeNotificationService
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */