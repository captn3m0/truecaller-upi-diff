package com.truecaller.whoviewedme;

import android.content.ContentResolver;
import javax.inject.Provider;

public final class e
  implements dagger.a.d<d>
{
  private final Provider<ContentResolver> a;
  
  private e(Provider<ContentResolver> paramProvider)
  {
    a = paramProvider;
  }
  
  public static e a(Provider<ContentResolver> paramProvider)
  {
    return new e(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */