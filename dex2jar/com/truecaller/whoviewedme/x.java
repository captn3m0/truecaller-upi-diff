package com.truecaller.whoviewedme;

import c.g.b.k;
import c.l;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.bc;
import com.truecaller.androidactors.f;
import com.truecaller.common.g.a;
import com.truecaller.common.h.an;
import com.truecaller.tracking.events.ao;
import com.truecaller.tracking.events.ao.a;
import com.truecaller.util.al;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import org.a.a.a.g;
import org.apache.a.d.d;

public final class x
  implements w
{
  private final com.truecaller.featuretoggles.e a;
  private final al b;
  private final com.truecaller.i.e c;
  private final a d;
  private final c e;
  private final com.truecaller.common.f.c f;
  private final com.truecaller.analytics.b g;
  private final f<ae> h;
  private final an i;
  private final ab j;
  
  @Inject
  public x(com.truecaller.featuretoggles.e parame, al paramal, com.truecaller.i.e parame1, a parama, c paramc, com.truecaller.common.f.c paramc1, com.truecaller.analytics.b paramb, f<ae> paramf, an paraman, ab paramab)
  {
    a = parame;
    b = paramal;
    c = parame1;
    d = parama;
    e = paramc;
    f = paramc1;
    g = paramb;
    h = paramf;
    i = paraman;
    j = paramab;
  }
  
  public final void a(WhoViewedMeLaunchContext paramWhoViewedMeLaunchContext)
  {
    k.b(paramWhoViewedMeLaunchContext, "launchContext");
    switch (y.a[paramWhoViewedMeLaunchContext.ordinal()])
    {
    default: 
      throw new l();
    case 5: 
      paramWhoViewedMeLaunchContext = "unknown";
      break;
    case 4: 
      paramWhoViewedMeLaunchContext = "callLogPromo";
      break;
    case 3: 
      paramWhoViewedMeLaunchContext = "notification";
      break;
    case 2: 
      paramWhoViewedMeLaunchContext = "deepLink";
      break;
    case 1: 
      paramWhoViewedMeLaunchContext = "navigationDrawer";
    }
    paramWhoViewedMeLaunchContext = new bc("whoViewedMe", paramWhoViewedMeLaunchContext, Boolean.valueOf(f.d()), 4);
    g.b((com.truecaller.analytics.e)paramWhoViewedMeLaunchContext);
  }
  
  public final void a(String paramString, boolean paramBoolean)
  {
    k.b(paramString, "tcId");
    Object localObject = (Map)new HashMap();
    ((Map)localObject).put("Type", "Receiver");
    ((Map)localObject).put("TcId", paramString);
    ((Map)localObject).put("Status", String.valueOf(paramBoolean));
    paramString = ao.b().a((CharSequence)"WhoViewedMeEvent").a((Map)localObject);
    localObject = UUID.randomUUID().toString();
    k.a(localObject, "UUID.randomUUID().toString()");
    paramString = paramString.b((CharSequence)localObject).a();
    ((ae)h.a()).a((d)paramString);
  }
  
  public final void a(String paramString, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    k.b(paramString, "tcId");
    Map localMap = (Map)new HashMap();
    localMap.put("Type", "Sender");
    localMap.put("TcId", paramString);
    localMap.put("Status", String.valueOf(paramBoolean3));
    paramString = Boolean.toString(paramBoolean1);
    k.a(paramString, "toString(hasBadge)");
    localMap.put("UserBadge", paramString);
    paramString = Boolean.toString(paramBoolean2);
    k.a(paramString, "toString(isPBContact)");
    localMap.put("PBContact", paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    d.b("whoViewedMeIncognitoEnabled", paramBoolean);
  }
  
  public final boolean a()
  {
    return (b.a()) && (a.t().a());
  }
  
  public final boolean a(String paramString, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
  {
    k.b(paramString, "tcId");
    if ((a()) && (paramInt != 21))
    {
      if (((CharSequence)paramString).length() > 0) {
        paramInt = 1;
      } else {
        paramInt = 0;
      }
      if ((paramInt != 0) && (paramBoolean1) && ((d.a("whoViewedMePBContactEnabled", false)) || (!paramBoolean2)) && (!e()) && (System.currentTimeMillis() - e.b(paramString) > TimeUnit.DAYS.toMillis(d.a("featureWhoViewdMeNewViewIntervalInDays", 5L)))) {
        return true;
      }
    }
    return false;
  }
  
  public final long b()
  {
    com.truecaller.i.e locale = c;
    org.a.a.b localb = org.a.a.b.ay_().c(1);
    k.a(localb, "DateTime.now().minusDays(1)");
    return locale.a("whoViewedMeLastVisitTimestamp", a);
  }
  
  public final boolean c()
  {
    return "Pro".equals(d.a("whoViewedMeIncognitoBucket"));
  }
  
  public final int d()
  {
    return e.a(b());
  }
  
  public final boolean e()
  {
    return (f.d()) && (d.a("whoViewedMeIncognitoEnabled", true));
  }
  
  public final boolean f()
  {
    long l1 = c.a("whoViewedMeNotificationTimestamp", 0L);
    long l2 = d.a("featureWhoViewdMeShowNotificationAfterXLookups", 5L);
    long l3 = d.a("featureWhoViewdMeShowNotificationAfterXDays", 5L);
    return (e.a(l1) >= l2) || (System.currentTimeMillis() - l1 > TimeUnit.DAYS.toMillis(l3));
  }
  
  public final void g()
  {
    e.a();
  }
  
  public final c h()
  {
    return e;
  }
  
  public final void i()
  {
    c.b("whoViewedMeLastVisitTimestamp", System.currentTimeMillis());
  }
  
  public final int j()
  {
    return e.a(0L);
  }
  
  public final boolean k()
  {
    return d.a("whoViewedMeACSEnabled", false);
  }
  
  public final void l()
  {
    if (!f.d())
    {
      if (!a()) {
        return;
      }
      long l = c.a("whoViewedMeNotificationTimestamp", 0L);
      int k = e.a(0L);
      if ((k > 0) && (i.a(l, 30L, TimeUnit.DAYS))) {
        j.a(k, null);
      }
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.x
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */