package com.truecaller.whoviewedme;

import com.truecaller.analytics.ae;
import com.truecaller.analytics.b;
import com.truecaller.androidactors.f;
import com.truecaller.common.g.a;
import com.truecaller.common.h.an;
import com.truecaller.util.al;
import dagger.a.d;
import javax.inject.Provider;

public final class z
  implements d<x>
{
  private final Provider<com.truecaller.featuretoggles.e> a;
  private final Provider<al> b;
  private final Provider<com.truecaller.i.e> c;
  private final Provider<a> d;
  private final Provider<c> e;
  private final Provider<com.truecaller.common.f.c> f;
  private final Provider<b> g;
  private final Provider<f<ae>> h;
  private final Provider<an> i;
  private final Provider<ab> j;
  
  private z(Provider<com.truecaller.featuretoggles.e> paramProvider, Provider<al> paramProvider1, Provider<com.truecaller.i.e> paramProvider2, Provider<a> paramProvider3, Provider<c> paramProvider4, Provider<com.truecaller.common.f.c> paramProvider5, Provider<b> paramProvider6, Provider<f<ae>> paramProvider7, Provider<an> paramProvider8, Provider<ab> paramProvider9)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
  }
  
  public static z a(Provider<com.truecaller.featuretoggles.e> paramProvider, Provider<al> paramProvider1, Provider<com.truecaller.i.e> paramProvider2, Provider<a> paramProvider3, Provider<c> paramProvider4, Provider<com.truecaller.common.f.c> paramProvider5, Provider<b> paramProvider6, Provider<f<ae>> paramProvider7, Provider<an> paramProvider8, Provider<ab> paramProvider9)
  {
    return new z(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.whoviewedme.z
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */