package com.truecaller;

import android.content.ClipboardManager;
import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class t
  implements d<ClipboardManager>
{
  private final c a;
  private final Provider<Context> b;
  
  private t(c paramc, Provider<Context> paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static t a(c paramc, Provider<Context> paramProvider)
  {
    return new t(paramc, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.t
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */