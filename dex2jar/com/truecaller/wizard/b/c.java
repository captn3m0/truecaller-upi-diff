package com.truecaller.wizard.b;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import com.truecaller.common.b.e;
import com.truecaller.utils.t.a;
import com.truecaller.wizard.R.anim;
import com.truecaller.wizard.R.id;
import com.truecaller.wizard.R.layout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;

public abstract class c
  extends AppCompatActivity
{
  private static volatile boolean c;
  List<c.a> a;
  @Inject
  public com.truecaller.wizard.a.d b;
  private final c.c d = new c.c(this);
  private List<c.b> e;
  private boolean f;
  private final Map<String, b> g = new HashMap();
  
  private o a(b paramb, Bundle paramBundle)
  {
    paramb = Fragment.instantiate(this, a, paramBundle);
    return getSupportFragmentManager().a().a(R.anim.wizard_fragment_enter, R.anim.wizard_fragment_exit).b(R.id.wizardPage, paramb);
  }
  
  public static void a(Context paramContext, Class<? extends c> paramClass)
  {
    a(paramContext, paramClass, null, true);
  }
  
  public static void a(Context paramContext, Class<? extends c> paramClass, Bundle paramBundle, boolean paramBoolean)
  {
    com.truecaller.debug.log.a.a(new Object[] { "Wizard start. Class ", paramClass.getSimpleName() });
    paramClass = new Intent(paramContext, paramClass);
    if (paramBundle != null) {
      paramClass.putExtras(paramBundle);
    }
    paramClass.addFlags(65536);
    paramClass.addFlags(268435456);
    if (paramBoolean) {
      paramClass.addFlags(32768);
    }
    paramContext.startActivity(paramClass);
  }
  
  public static void a(Context paramContext, Class<? extends c> paramClass, String paramString)
  {
    boolean bool = e.a("wizard_FullyCompleted", false);
    com.truecaller.debug.log.a.a(new Object[] { "Wizard start. ResetAndStart ", paramClass.getSimpleName(), ", isCompleted: ", String.valueOf(bool) });
    if (bool) {
      a(false);
    }
    e.b("signUpOrigin", paramString);
    a(paramContext, paramClass, null, true);
  }
  
  public static void a(boolean paramBoolean)
  {
    e.b("wizard_RequiredStepsCompleted", paramBoolean);
    e.b("wizard_FullyCompleted", paramBoolean);
    e.b("wizard_StartPage");
  }
  
  public static void b(Context paramContext, Class<? extends c> paramClass)
  {
    com.truecaller.common.b.a.F().u().c().b("isUserChangingNumber", true);
    a(paramContext, paramClass, null, true);
  }
  
  public static void b(String paramString)
  {
    e.b("wizard_StartPage", paramString);
  }
  
  public static boolean e()
  {
    return e.a("wizard_RequiredStepsCompleted", false);
  }
  
  public static boolean f()
  {
    return e.a("wizard_FullyCompleted", false);
  }
  
  public static boolean g()
  {
    return e.a("wizard_OEMMode", false);
  }
  
  public static void h()
  {
    e.b("wizard_OEMMode", false);
  }
  
  public static boolean i()
  {
    return c;
  }
  
  public void a()
  {
    e.b("wizard_RequiredStepsCompleted", true);
  }
  
  public final void a(c.b paramb)
  {
    if (e == null) {
      e = new ArrayList(1);
    }
    e.add(paramb);
  }
  
  public final void a(String paramString)
  {
    a(paramString, null);
  }
  
  public final void a(String paramString, Bundle paramBundle)
  {
    b.a(paramString);
    d.a(paramString, paramBundle);
  }
  
  protected abstract void a(Map<String, b> paramMap);
  
  public void b()
  {
    b.c();
    if (!e.a("wizard_RequiredStepsCompleted", false)) {
      a();
    }
    e.b("wizard_FullyCompleted", true);
    e.b("wizard_StartPage");
    e.b("signUpOrigin");
    com.truecaller.common.b.a.F().u().c().d("isUserChangingNumber");
    finish();
  }
  
  public final void b(c.b paramb)
  {
    List localList = e;
    if (localList != null) {
      localList.remove(paramb);
    }
  }
  
  public abstract com.truecaller.wizard.d.c c();
  
  public final boolean c(String paramString)
  {
    return g.containsKey(paramString);
  }
  
  protected abstract String d();
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    Object localObject = a;
    if (localObject != null)
    {
      localObject = ((List)localObject).iterator();
      while (((Iterator)localObject).hasNext()) {
        if (((c.a)((Iterator)localObject).next()).a(paramInt1, paramInt2, paramIntent)) {}
      }
    }
  }
  
  public void onBackPressed()
  {
    List localList = e;
    if (localList != null)
    {
      int i = localList.size() - 1;
      while (i >= 0)
      {
        if (((c.b)e.get(i)).i()) {
          return;
        }
        i -= 1;
      }
    }
    if (e.a("wizard_OEMMode", false)) {
      return;
    }
    super.onBackPressed();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    a.a().a(com.truecaller.utils.c.a().a(this).a()).a(com.truecaller.common.b.a.F().u()).a().a(this);
    setContentView(R.layout.wizard_base);
    f = true;
    a(g);
    if (paramBundle == null)
    {
      String str = e.a("wizard_StartPage");
      paramBundle = str;
      if (TextUtils.isEmpty(str)) {
        paramBundle = d();
      }
      b.a();
      a(paramBundle, null);
    }
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    d.removeCallbacksAndMessages(null);
  }
  
  public void onPostResume()
  {
    super.onPostResume();
    f = true;
  }
  
  public void onSaveInstanceState(Bundle paramBundle)
  {
    f = false;
    super.onSaveInstanceState(paramBundle);
  }
  
  public void onStart()
  {
    super.onStart();
    c = true;
  }
  
  public void onStop()
  {
    super.onStop();
    c = false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.b.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */