package com.truecaller.wizard.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import java.util.Iterator;
import java.util.List;

public abstract class i$a
  extends BroadcastReceiver
{
  protected abstract void a(Context paramContext, String paramString);
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    paramIntent = paramIntent.getCharSequenceArrayListExtra("granted");
    if (paramIntent != null)
    {
      paramIntent = paramIntent.iterator();
      while (paramIntent.hasNext()) {
        a(paramContext, String.valueOf((CharSequence)paramIntent.next()));
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.utils.i.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */