package com.truecaller.wizard;

import android.app.NotificationManager;
import android.os.Bundle;
import android.text.TextUtils;
import com.truecaller.utils.l;
import com.truecaller.utils.t;
import com.truecaller.utils.t.a;
import com.truecaller.wizard.b.b;
import java.util.Map;

public abstract class TruecallerWizard
  extends com.truecaller.wizard.b.c
{
  private l c;
  
  public final void a(Map<String, b> paramMap)
  {
    paramMap.put("Page_Welcome", new b(i.class, false));
    paramMap.put("Page_EnterNumber", new b(e.class, true));
    paramMap.put("Page_Privacy", new b(com.truecaller.wizard.e.c.class, true));
    paramMap.put("Page_Verification", new b(com.truecaller.wizard.d.e.class, false));
    paramMap.put("Page_Success", new b(h.class, false));
    paramMap.put("Page_Profile", new b(f.class, true));
    paramMap.put("Page_AdsChoices", new b(com.truecaller.wizard.adschoices.e.class, true));
    paramMap.put("Page_AccessContacts", new b(a.class, true));
    paramMap.put("Page_DrawPermission", new b(d.class, true));
    paramMap.put("Page_DrawPermissionDetails", new b(c.class, false));
  }
  
  public final String d()
  {
    if ((!com.truecaller.common.b.a.F().u().c().a("isUserChangingNumber", false)) && (!com.truecaller.common.b.e.a("wizard_OEMMode", false))) {
      return "Page_Welcome";
    }
    return "Page_EnterNumber";
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setRequestedOrientation(1);
    c = com.truecaller.utils.c.a().a(this).a().b();
    if (!com.truecaller.common.b.e.a("wizard_HasSentFirstStartEvent", false)) {
      com.truecaller.common.b.e.b("wizard_HasSentFirstStartEvent", true);
    }
  }
  
  public void onResume()
  {
    super.onResume();
    NotificationManager localNotificationManager = (NotificationManager)getSystemService("notification");
    if (localNotificationManager != null) {
      localNotificationManager.cancel(R.id.wizard_notification);
    }
    if (!TextUtils.isEmpty(com.truecaller.common.b.e.a("wizard_StartPage"))) {
      if (!c.a(new String[] { "android.permission.READ_PHONE_STATE" })) {
        a(d(), null);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.wizard.TruecallerWizard
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */