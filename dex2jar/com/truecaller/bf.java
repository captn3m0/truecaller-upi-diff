package com.truecaller;

import android.content.SharedPreferences;
import com.truecaller.common.g.a;
import com.truecaller.common.g.d;
import com.truecaller.common.g.e;
import dagger.a.g;
import javax.inject.Provider;

public final class bf
  implements bq
{
  private Provider<SharedPreferences> a;
  private Provider<a> b;
  
  private bf(com.truecaller.common.g.c paramc)
  {
    a = dagger.a.c.a(e.a(paramc));
    b = dagger.a.c.a(d.a(paramc, a));
  }
  
  public static a a()
  {
    return new a((byte)0);
  }
  
  public final void a(TruecallerBackupAgent paramTruecallerBackupAgent)
  {
    a = ((a)b.get());
  }
  
  public static final class a
  {
    private com.truecaller.common.g.c a;
    
    public final a a(com.truecaller.common.g.c paramc)
    {
      a = ((com.truecaller.common.g.c)g.a(paramc));
      return this;
    }
    
    public final bq a()
    {
      g.a(a, com.truecaller.common.g.c.class);
      return new bf(a, (byte)0);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.bf
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */