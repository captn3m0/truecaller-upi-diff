package com.truecaller.calling;

import android.view.View;
import android.widget.TextView;
import c.f;
import c.g.b.k;
import com.truecaller.utils.extensions.p;
import com.truecaller.utils.extensions.t;

public final class q
  implements v
{
  private final f b;
  
  public q(View paramView)
  {
    b = t.a(paramView, 2131363509);
  }
  
  public final void e_(String paramString)
  {
    TextView localTextView = (TextView)b.b();
    k.a(localTextView, "subtitle");
    p.a(localTextView, paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */