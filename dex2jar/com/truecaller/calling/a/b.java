package com.truecaller.calling.a;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import c.g.b.k;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.e;
import javax.inject.Inject;

public final class b
  implements a
{
  public static final a a = new a((byte)0);
  private final ContentResolver b;
  
  @Inject
  public b(ContentResolver paramContentResolver)
  {
    b = paramContentResolver;
  }
  
  /* Error */
  public final com.truecaller.calling.a.a.a a(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 40
    //   3: invokestatic 30	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: invokestatic 46	java/lang/System:currentTimeMillis	()J
    //   9: lstore_2
    //   10: aload_1
    //   11: checkcast 48	java/lang/CharSequence
    //   14: invokestatic 53	c/n/m:a	(Ljava/lang/CharSequence;)Z
    //   17: istore 6
    //   19: aconst_null
    //   20: astore 8
    //   22: aconst_null
    //   23: astore 7
    //   25: iload 6
    //   27: ifeq +5 -> 32
    //   30: aconst_null
    //   31: areturn
    //   32: aload_0
    //   33: getfield 34	com/truecaller/calling/a/b:b	Landroid/content/ContentResolver;
    //   36: invokestatic 58	com/truecaller/content/TruecallerContract$e:a	()Landroid/net/Uri;
    //   39: aconst_null
    //   40: ldc 60
    //   42: iconst_1
    //   43: anewarray 62	java/lang/String
    //   46: dup
    //   47: iconst_0
    //   48: aload_1
    //   49: aastore
    //   50: aconst_null
    //   51: invokevirtual 68	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   54: astore 9
    //   56: aload 8
    //   58: astore_1
    //   59: aload 9
    //   61: ifnull +163 -> 224
    //   64: aload 9
    //   66: checkcast 70	java/io/Closeable
    //   69: astore 8
    //   71: aload 7
    //   73: astore_1
    //   74: aload 8
    //   76: checkcast 72	android/database/Cursor
    //   79: astore 9
    //   81: aload 7
    //   83: astore_1
    //   84: new 74	com/truecaller/calling/a/d
    //   87: dup
    //   88: aload 9
    //   90: invokespecial 77	com/truecaller/calling/a/d:<init>	(Landroid/database/Cursor;)V
    //   93: astore 10
    //   95: aload 7
    //   97: astore_1
    //   98: aload 9
    //   100: invokeinterface 81 1 0
    //   105: ifeq +86 -> 191
    //   108: aload 7
    //   110: astore_1
    //   111: aload 9
    //   113: ldc 83
    //   115: invokestatic 30	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   118: aload 7
    //   120: astore_1
    //   121: aload 9
    //   123: aload 10
    //   125: getfield 86	com/truecaller/calling/a/d:a	I
    //   128: invokeinterface 90 2 0
    //   133: astore 11
    //   135: aload 7
    //   137: astore_1
    //   138: aload 11
    //   140: ldc 92
    //   142: invokestatic 94	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   145: aload 7
    //   147: astore_1
    //   148: aload 9
    //   150: aload 10
    //   152: getfield 96	com/truecaller/calling/a/d:b	I
    //   155: invokeinterface 100 2 0
    //   160: ifle +106 -> 266
    //   163: iconst_1
    //   164: istore 6
    //   166: goto +3 -> 169
    //   169: aload 7
    //   171: astore_1
    //   172: new 102	com/truecaller/calling/a/a/a
    //   175: dup
    //   176: aload 11
    //   178: iload 6
    //   180: invokespecial 105	com/truecaller/calling/a/a/a:<init>	(Ljava/lang/String;Z)V
    //   183: astore 7
    //   185: aload 7
    //   187: astore_1
    //   188: goto +5 -> 193
    //   191: aconst_null
    //   192: astore_1
    //   193: aload 8
    //   195: aconst_null
    //   196: invokestatic 110	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   199: goto +25 -> 224
    //   202: astore 7
    //   204: goto +11 -> 215
    //   207: astore 7
    //   209: aload 7
    //   211: astore_1
    //   212: aload 7
    //   214: athrow
    //   215: aload 8
    //   217: aload_1
    //   218: invokestatic 110	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   221: aload 7
    //   223: athrow
    //   224: invokestatic 46	java/lang/System:currentTimeMillis	()J
    //   227: lstore 4
    //   229: new 112	java/lang/StringBuilder
    //   232: dup
    //   233: ldc 114
    //   235: invokespecial 117	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   238: astore 7
    //   240: aload 7
    //   242: lload 4
    //   244: lload_2
    //   245: lsub
    //   246: invokevirtual 121	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   249: pop
    //   250: aload 7
    //   252: ldc 123
    //   254: invokevirtual 126	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   257: pop
    //   258: aload 7
    //   260: invokevirtual 130	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   263: pop
    //   264: aload_1
    //   265: areturn
    //   266: iconst_0
    //   267: istore 6
    //   269: goto -100 -> 169
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	272	0	this	b
    //   0	272	1	paramString	String
    //   9	236	2	l1	long
    //   227	16	4	l2	long
    //   17	251	6	bool	boolean
    //   23	163	7	locala	com.truecaller.calling.a.a.a
    //   202	1	7	localObject	Object
    //   207	15	7	localThrowable	Throwable
    //   238	21	7	localStringBuilder	StringBuilder
    //   20	196	8	localCloseable	java.io.Closeable
    //   54	95	9	localCursor	android.database.Cursor
    //   93	58	10	locald	d
    //   133	44	11	str	String
    // Exception table:
    //   from	to	target	type
    //   74	81	202	finally
    //   84	95	202	finally
    //   98	108	202	finally
    //   111	118	202	finally
    //   121	135	202	finally
    //   138	145	202	finally
    //   148	163	202	finally
    //   172	185	202	finally
    //   212	215	202	finally
    //   74	81	207	java/lang/Throwable
    //   84	95	207	java/lang/Throwable
    //   98	108	207	java/lang/Throwable
    //   111	118	207	java/lang/Throwable
    //   121	135	207	java/lang/Throwable
    //   138	145	207	java/lang/Throwable
    //   148	163	207	java/lang/Throwable
    //   172	185	207	java/lang/Throwable
  }
  
  public final void a()
  {
    ContentProviderOperation localContentProviderOperation = ContentProviderOperation.newDelete(TruecallerContract.e.a()).build();
    b.applyBatch(TruecallerContract.a(), c.a.m.d(new ContentProviderOperation[] { localContentProviderOperation }));
  }
  
  public final void a(com.truecaller.calling.a.a.a parama)
  {
    k.b(parama, "contactSettings");
    long l1 = System.currentTimeMillis();
    if (c.n.m.a((CharSequence)a)) {
      return;
    }
    parama = ContentProviderOperation.newInsert(TruecallerContract.e.a()).withValues(parama.a()).build();
    b.applyBatch(TruecallerContract.a(), c.a.m.d(new ContentProviderOperation[] { parama }));
    long l2 = System.currentTimeMillis();
    parama = new StringBuilder("insertContactSettings, query took: ");
    parama.append(l2 - l1);
    parama.append("ms");
    parama.toString();
  }
  
  public final void b(com.truecaller.calling.a.a.a parama)
  {
    k.b(parama, "contactSettings");
    long l1 = System.currentTimeMillis();
    if (c.n.m.a((CharSequence)a)) {
      return;
    }
    parama = ContentProviderOperation.newUpdate(TruecallerContract.e.a()).withSelection("tc_id = ?", new String[] { a }).withValues(parama.a()).build();
    b.applyBatch(TruecallerContract.a(), c.a.m.d(new ContentProviderOperation[] { parama }));
    long l2 = System.currentTimeMillis();
    parama = new StringBuilder("updateContactSettings, query took: ");
    parama.append(l2 - l1);
    parama.append("ms");
    parama.toString();
  }
  
  public static final class a {}
}

/* Location:
 * Qualified Name:     com.truecaller.calling.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */