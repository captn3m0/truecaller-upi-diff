package com.truecaller.calling.a.a;

import android.content.ContentValues;

public final class a
{
  public String a;
  public boolean b;
  
  public a(String paramString, boolean paramBoolean)
  {
    a = paramString;
    b = paramBoolean;
  }
  
  public final ContentValues a()
  {
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("tc_id", a);
    localContentValues.put("hidden_from_identified", Boolean.valueOf(b));
    return localContentValues;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.a.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */