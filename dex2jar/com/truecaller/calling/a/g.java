package com.truecaller.calling.a;

import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d<f>
{
  private final Provider<a> a;
  private final Provider<c.d.f> b;
  
  private g(Provider<a> paramProvider, Provider<c.d.f> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static g a(Provider<a> paramProvider, Provider<c.d.f> paramProvider1)
  {
    return new g(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.a.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */