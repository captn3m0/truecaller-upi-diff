package com.truecaller.calling.a;

import android.content.ContentResolver;
import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d<b>
{
  private final Provider<ContentResolver> a;
  
  private c(Provider<ContentResolver> paramProvider)
  {
    a = paramProvider;
  }
  
  public static c a(Provider<ContentResolver> paramProvider)
  {
    return new c(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */