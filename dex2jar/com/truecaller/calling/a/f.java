package com.truecaller.calling.a;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

public final class f
  implements e
{
  final a a;
  final c.d.f b;
  
  @Inject
  public f(a parama, @Named("IO") c.d.f paramf)
  {
    a = parama;
    b = paramf;
  }
  
  public final com.truecaller.calling.a.a.a a(String paramString)
  {
    c.g.b.k.b(paramString, "tcId");
    return a.a(paramString);
  }
  
  public final void a()
  {
    a.a();
  }
  
  public final void a(final com.truecaller.calling.a.a.a parama)
  {
    c.g.b.k.b(parama, "contactSettings");
    kotlinx.coroutines.f.a((m)new b(this, parama, null));
  }
  
  public final void b(final com.truecaller.calling.a.a.a parama)
  {
    c.g.b.k.b(parama, "contactSettings");
    kotlinx.coroutines.f.a((m)new d(this, parama, null));
  }
  
  @c.d.b.a.f(b="ContactSettingsRepository.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.data.ContactSettingsRepositoryImpl$insertContactSettings$2")
  static final class a
    extends c.d.b.a.k
    implements m<ag, c<? super x>, Object>
  {
    int a;
    private ag d;
    
    a(f paramf, com.truecaller.calling.a.a.a parama, c paramc)
    {
      super(paramc);
    }
    
    public final c<x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new a(b, c, paramc);
      d = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      c.d.a.a locala = c.d.a.a.a;
      if (a == 0)
      {
        if (!(paramObject instanceof o.b))
        {
          b.a.a(c);
          return x.a;
        }
        throw a;
      }
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((a)a(paramObject1, (c)paramObject2)).a(x.a);
    }
  }
  
  @c.d.b.a.f(b="ContactSettingsRepository.kt", c={36}, d="invokeSuspend", e="com.truecaller.calling.data.ContactSettingsRepositoryImpl$insertContactSettingsSync$1")
  static final class b
    extends c.d.b.a.k
    implements m<ag, c<? super x>, Object>
  {
    int a;
    private ag d;
    
    b(f paramf, com.truecaller.calling.a.a.a parama, c paramc)
    {
      super(paramc);
    }
    
    public final c<x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new b(b, parama, paramc);
      d = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      c.d.a.a locala = c.d.a.a.a;
      switch (a)
      {
      default: 
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
      case 1: 
        if ((paramObject instanceof o.b)) {
          throw a;
        }
        break;
      case 0: 
        if ((paramObject instanceof o.b)) {
          break label113;
        }
        paramObject = b;
        com.truecaller.calling.a.a.a locala1 = parama;
        a = 1;
        if (g.a(b, (m)new f.a((f)paramObject, locala1, null), this) == locala) {
          return locala;
        }
        break;
      }
      return x.a;
      label113:
      throw a;
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((b)a(paramObject1, (c)paramObject2)).a(x.a);
    }
  }
  
  @c.d.b.a.f(b="ContactSettingsRepository.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.data.ContactSettingsRepositoryImpl$updateContactSettings$2")
  static final class c
    extends c.d.b.a.k
    implements m<ag, c<? super x>, Object>
  {
    int a;
    private ag d;
    
    c(f paramf, com.truecaller.calling.a.a.a parama, c paramc)
    {
      super(paramc);
    }
    
    public final c<x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new c(b, c, paramc);
      d = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      c.d.a.a locala = c.d.a.a.a;
      if (a == 0)
      {
        if (!(paramObject instanceof o.b))
        {
          b.a.b(c);
          return x.a;
        }
        throw a;
      }
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((c)a(paramObject1, (c)paramObject2)).a(x.a);
    }
  }
  
  @c.d.b.a.f(b="ContactSettingsRepository.kt", c={46}, d="invokeSuspend", e="com.truecaller.calling.data.ContactSettingsRepositoryImpl$updateContactSettingsSync$1")
  static final class d
    extends c.d.b.a.k
    implements m<ag, c<? super x>, Object>
  {
    int a;
    private ag d;
    
    d(f paramf, com.truecaller.calling.a.a.a parama, c paramc)
    {
      super(paramc);
    }
    
    public final c<x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new d(b, parama, paramc);
      d = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      c.d.a.a locala = c.d.a.a.a;
      switch (a)
      {
      default: 
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
      case 1: 
        if ((paramObject instanceof o.b)) {
          throw a;
        }
        break;
      case 0: 
        if ((paramObject instanceof o.b)) {
          break label113;
        }
        paramObject = b;
        com.truecaller.calling.a.a.a locala1 = parama;
        a = 1;
        if (g.a(b, (m)new f.c((f)paramObject, locala1, null), this) == locala) {
          return locala;
        }
        break;
      }
      return x.a;
      label113:
      throw a;
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((d)a(paramObject1, (c)paramObject2)).a(x.a);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.a.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */