package com.truecaller.calling.a;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;

@c.d.b.a.f(b="ContactSettingsRepository.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.data.ContactSettingsRepositoryImpl$updateContactSettings$2")
final class f$c
  extends c.d.b.a.k
  implements m<ag, c<? super x>, Object>
{
  int a;
  private ag d;
  
  f$c(f paramf, com.truecaller.calling.a.a.a parama, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new c(b, c, paramc);
    d = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    c.d.a.a locala = c.d.a.a.a;
    if (a == 0)
    {
      if (!(paramObject instanceof o.b))
      {
        b.a.b(c);
        return x.a;
      }
      throw a;
    }
    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((c)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.a.f.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */