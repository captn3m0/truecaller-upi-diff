package com.truecaller.calling.a;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

@c.d.b.a.f(b="ContactSettingsRepository.kt", c={46}, d="invokeSuspend", e="com.truecaller.calling.data.ContactSettingsRepositoryImpl$updateContactSettingsSync$1")
final class f$d
  extends c.d.b.a.k
  implements m<ag, c<? super x>, Object>
{
  int a;
  private ag d;
  
  f$d(f paramf, com.truecaller.calling.a.a.a parama, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new d(b, c, paramc);
    d = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    c.d.a.a locala = c.d.a.a.a;
    switch (a)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 1: 
      if ((paramObject instanceof o.b)) {
        throw a;
      }
      break;
    case 0: 
      if ((paramObject instanceof o.b)) {
        break label113;
      }
      paramObject = b;
      com.truecaller.calling.a.a.a locala1 = c;
      a = 1;
      if (g.a(b, (m)new f.c((f)paramObject, locala1, null), this) == locala) {
        return locala;
      }
      break;
    }
    return x.a;
    label113:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((d)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.a.f.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */