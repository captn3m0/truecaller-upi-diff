package com.truecaller.calling;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import c.f;
import c.g;
import c.g.a.a;
import c.g.b.l;
import com.truecaller.utils.extensions.t;

public final class i
  implements b
{
  private final f b;
  private final f c;
  private final f d;
  private final f e;
  private final f f;
  private boolean g;
  private ActionType h;
  
  public i(View paramView)
  {
    b = t.a(paramView, 2131361939);
    c = t.a(paramView, 2131361843);
    d = g.a((a)new b(paramView));
    e = g.a((a)new a(paramView));
    f = g.a((a)new c(paramView));
  }
  
  private final ImageView a()
  {
    return (ImageView)b.b();
  }
  
  private final View b()
  {
    return (View)c.b();
  }
  
  public final void a(com.truecaller.adapter_delegates.k paramk, RecyclerView.ViewHolder paramViewHolder)
  {
    c.g.b.k.b(paramk, "eventReceiver");
    c.g.b.k.b(paramViewHolder, "holder");
    View localView = b();
    c.g.b.k.a(localView, "actionTwoClickArea");
    com.truecaller.adapter_delegates.i.a(localView, paramk, paramViewHolder, (a)new d(this), "button");
  }
  
  public final void b(ActionType paramActionType)
  {
    if (paramActionType != null) {
      switch (j.a[paramActionType.ordinal()])
      {
      default: 
        break;
      case 4: 
      case 5: 
        localObject = (Drawable)f.b();
        break;
      case 2: 
      case 3: 
        localObject = (Drawable)e.b();
        break;
      case 1: 
        localObject = (Drawable)d.b();
        break;
      }
    }
    Object localObject = null;
    if (localObject != null)
    {
      h = paramActionType;
      paramActionType = a();
      t.a((View)paramActionType);
      paramActionType.setImageDrawable((Drawable)localObject);
      c(g);
      return;
    }
    paramActionType = (i)this;
    h = null;
    localObject = paramActionType.a();
    c.g.b.k.a(localObject, "actionTwoView");
    t.b((View)localObject);
    paramActionType = paramActionType.b();
    c.g.b.k.a(paramActionType, "actionTwoClickArea");
    t.b(paramActionType);
  }
  
  public final void c(boolean paramBoolean)
  {
    g = paramBoolean;
    if (h != null)
    {
      View localView = b();
      if (paramBoolean)
      {
        t.a(localView);
        return;
      }
      t.c(localView);
    }
  }
  
  static final class a
    extends l
    implements a<Drawable>
  {
    a(View paramView)
    {
      super();
    }
  }
  
  static final class b
    extends l
    implements a<Drawable>
  {
    b(View paramView)
    {
      super();
    }
  }
  
  static final class c
    extends l
    implements a<Drawable>
  {
    c(View paramView)
    {
      super();
    }
  }
  
  static final class d
    extends l
    implements a<String>
  {
    d(i parami)
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */