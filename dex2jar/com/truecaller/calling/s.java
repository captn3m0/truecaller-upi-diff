package com.truecaller.calling;

import android.view.View;
import android.widget.TextView;
import c.f;
import c.g.b.k;

public final class s
  implements t
{
  private final f b;
  
  public s(View paramView)
  {
    b = com.truecaller.utils.extensions.t.a(paramView, 2131363505);
  }
  
  public final void a(String paramString)
  {
    TextView localTextView = (TextView)b.b();
    if (paramString != null)
    {
      com.truecaller.utils.extensions.t.a((View)localTextView);
      localTextView.setText((CharSequence)paramString);
      return;
    }
    k.a(localTextView, "this");
    com.truecaller.utils.extensions.t.b((View)localTextView);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.s
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */