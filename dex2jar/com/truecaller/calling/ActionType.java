package com.truecaller.calling;

public enum ActionType
{
  public static final a Companion = new a((byte)0);
  private final String eventAction;
  
  static
  {
    ActionType localActionType1 = new ActionType("SMS", 0, "ItemEvent.ACTION_SMS");
    SMS = localActionType1;
    ActionType localActionType2 = new ActionType("FLASH", 1, "ItemEvent.ACTION_FLASH");
    FLASH = localActionType2;
    ActionType localActionType3 = new ActionType("PROFILE", 2, "ItemEvent.ACTION_OPEN_PROFILE");
    PROFILE = localActionType3;
    ActionType localActionType4 = new ActionType("CELLULAR_CALL", 3, "ItemEvent.ACTION_CELLULAR_CALL");
    CELLULAR_CALL = localActionType4;
    ActionType localActionType5 = new ActionType("CELLULAR_VIDEO_CALL", 4, "ItemEvent.ACTION_CELLULAR_VIDEO_CALL");
    CELLULAR_VIDEO_CALL = localActionType5;
    ActionType localActionType6 = new ActionType("WHATSAPP_CALL", 5, "ItemEvent.ACTION_WHATSAPP_CALL");
    WHATSAPP_CALL = localActionType6;
    ActionType localActionType7 = new ActionType("WHATSAPP_VIDEO_CALL", 6, "ItemEvent.ACTION_WHATSAPP_VIDEO_CALL");
    WHATSAPP_VIDEO_CALL = localActionType7;
    ActionType localActionType8 = new ActionType("VOIP_CALL", 7, "ItemEvent.ACTION_VOIP_CALL");
    VOIP_CALL = localActionType8;
    $VALUES = new ActionType[] { localActionType1, localActionType2, localActionType3, localActionType4, localActionType5, localActionType6, localActionType7, localActionType8 };
  }
  
  private ActionType(String paramString)
  {
    eventAction = paramString;
  }
  
  public final String getEventAction()
  {
    return eventAction;
  }
  
  public static final class a {}
}

/* Location:
 * Qualified Name:     com.truecaller.calling.ActionType
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */