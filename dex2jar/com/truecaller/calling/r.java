package com.truecaller.calling;

import c.g.b.k;
import c.n.m;
import com.truecaller.calling.dialer.ax;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.flashsdk.core.i;
import com.truecaller.utils.n;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class r
{
  public static final x a(Contact paramContact, i parami, String paramString)
  {
    k.b(paramContact, "receiver$0");
    k.b(parami, "flashPoint");
    k.b(paramString, "analyticsContext");
    if (!ae.a(parami)) {
      return null;
    }
    Object localObject1 = paramContact.A();
    k.a(localObject1, "numbers");
    Object localObject2 = (Iterable)localObject1;
    localObject1 = (Collection)new ArrayList();
    localObject2 = ((Iterable)localObject2).iterator();
    Object localObject3;
    while (((Iterator)localObject2).hasNext())
    {
      localObject3 = (Number)((Iterator)localObject2).next();
      k.a(localObject3, "it");
      localObject3 = ((Number)localObject3).a();
      if (localObject3 != null) {
        ((Collection)localObject1).add(localObject3);
      }
    }
    localObject2 = (Iterable)localObject1;
    localObject1 = (Collection)new ArrayList();
    localObject2 = ((Iterable)localObject2).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject3 = ((Iterator)localObject2).next();
      if (parami.b((String)localObject3)) {
        ((Collection)localObject1).add(localObject3);
      }
    }
    localObject1 = (Iterable)localObject1;
    parami = (Collection)new ArrayList();
    localObject1 = ((Iterable)localObject1).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = m.d(m.a((String)((Iterator)localObject1).next(), "+", ""));
      if (localObject2 != null) {
        parami.add(localObject2);
      }
    }
    parami = (List)parami;
    if (!(((Collection)parami).isEmpty() ^ true)) {
      parami = null;
    }
    if (parami != null)
    {
      paramContact = paramContact.s();
      k.a(paramContact, "displayNameOrNumber");
      return new x(parami, paramContact, paramString);
    }
    return null;
  }
  
  public static final String a(Contact paramContact, String paramString, n paramn, ax paramax)
  {
    k.b(paramString, "normalizedNumber");
    k.b(paramn, "resourceProvider");
    k.b(paramax, "numberTypeLabelProvider");
    if (paramContact != null)
    {
      paramContact = paramContact.A();
      if (paramContact != null)
      {
        Iterator localIterator = ((Iterable)paramContact).iterator();
        while (localIterator.hasNext())
        {
          paramContact = localIterator.next();
          Number localNumber = (Number)paramContact;
          k.a(localNumber, "it");
          if (k.a(localNumber.a(), paramString)) {
            break label90;
          }
        }
        paramContact = null;
        label90:
        paramContact = (Number)paramContact;
        if (paramContact != null) {
          return ah.a(paramContact, paramn, paramax);
        }
      }
    }
    return null;
  }
  
  public static final boolean a(Contact paramContact)
  {
    return (paramContact == null) || ((paramContact.getSource() & 0xD) == 0);
  }
  
  public static final boolean b(Contact paramContact)
  {
    return (paramContact != null) && (paramContact.getId() != null);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.r
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */