package com.truecaller.calling.b;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import c.a.m;
import c.g.a.b;
import c.g.b.k;
import com.truecaller.calling.initiate_call.h.b;
import java.util.List;

@SuppressLint({"NewApi", "MissingPermission"})
@TargetApi(23)
public final class g
  implements f
{
  final TelecomManager a;
  
  public g(TelecomManager paramTelecomManager)
  {
    a = paramTelecomManager;
  }
  
  public final List<h.b> a()
  {
    List localList = a.getCallCapablePhoneAccounts();
    k.a(localList, "telecomManager.callCapablePhoneAccounts");
    return c.m.l.d(c.m.l.c(c.m.l.d(m.n((Iterable)localList), (b)new a(this)), (b)b.a));
  }
  
  static final class a
    extends c.g.b.l
    implements b<PhoneAccountHandle, PhoneAccount>
  {
    a(g paramg)
    {
      super();
    }
  }
  
  static final class b
    extends c.g.b.l
    implements b<PhoneAccount, h.b>
  {
    public static final b a = new b();
    
    b()
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.b.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */