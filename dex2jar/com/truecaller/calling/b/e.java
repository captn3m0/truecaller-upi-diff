package com.truecaller.calling.b;

import android.graphics.drawable.Drawable;
import c.a.m;
import c.g.b.k;
import com.truecaller.calling.initiate_call.h.a;
import com.truecaller.multisim.SimInfo;
import com.truecaller.utils.n;

public final class e
  implements d
{
  private final com.truecaller.multisim.h a;
  private final n b;
  
  public e(com.truecaller.multisim.h paramh, n paramn)
  {
    a = paramh;
    b = paramn;
  }
  
  public final com.truecaller.calling.initiate_call.h a(int paramInt)
  {
    SimInfo localSimInfo = a.a(paramInt);
    String str1 = null;
    if (localSimInfo == null) {
      return null;
    }
    k.a(localSimInfo, "multiSimManager.getSimIn…ndex(slot) ?: return null");
    int i;
    if (paramInt == 0) {
      i = 2131233935;
    } else {
      i = 2131233936;
    }
    Drawable localDrawable = b.c(i);
    k.a(localDrawable, "resourceProvider.getDrawable(drawableRes)");
    String str2 = b.a(2130903061)[paramInt];
    String str3 = d;
    String str4 = c;
    if (j) {
      str1 = b.a(2131887947, new Object[0]);
    }
    str1 = m.a((Iterable)m.e(new String[] { str3, str4, str1 }), (CharSequence)", ", null, null, 0, null, null, 62);
    k.a(str2, "title");
    return (com.truecaller.calling.initiate_call.h)new h.a((CharSequence)str2, (CharSequence)str1, localDrawable, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.b.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */