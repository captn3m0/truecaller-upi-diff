package com.truecaller.calling;

import android.view.View;
import c.f;
import c.g.a.b;
import c.g.b.k;
import c.x;
import com.truecaller.calling.contacts_list.data.g;
import com.truecaller.data.entity.Contact;
import com.truecaller.ui.view.VoipTintedImageView;
import com.truecaller.utils.extensions.t;

public final class bf
  implements be
{
  private final f b;
  
  public bf(View paramView)
  {
    b = t.a(paramView, 2131361918);
  }
  
  public final void a(g paramg, Contact paramContact, b<? super Boolean, x> paramb)
  {
    k.b(paramg, "voipAvailabilityCache");
    k.b(paramContact, "contact");
    k.b(paramb, "callback");
    ((VoipTintedImageView)b.b()).a(paramg, paramContact, paramb);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.bf
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */