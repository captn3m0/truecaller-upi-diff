package com.truecaller.calling;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import c.a.y;
import c.f;
import c.g;
import c.g.a.a;
import c.g.b.l;
import com.truecaller.adapter_delegates.i;
import com.truecaller.flashsdk.ui.CompoundFlashButton;
import com.truecaller.utils.extensions.t;
import java.util.List;

public final class aa
  implements ac
{
  private final f b;
  private final f c;
  private final f d;
  private final f e;
  private final f f;
  private boolean g;
  private ActionType h;
  private final View i;
  
  public aa(View paramView)
  {
    i = paramView;
    b = t.a(i, 2131361918);
    c = t.a(i, 2131361842);
    d = t.a(i, 2131363109);
    e = g.a((a)new b(this));
    f = g.a((a)new a(this));
  }
  
  private final ImageView a()
  {
    return (ImageView)b.b();
  }
  
  private final View b()
  {
    return (View)c.b();
  }
  
  private final CompoundFlashButton c()
  {
    return (CompoundFlashButton)d.b();
  }
  
  public final void a(com.truecaller.adapter_delegates.k paramk, RecyclerView.ViewHolder paramViewHolder)
  {
    c.g.b.k.b(paramk, "eventReceiver");
    c.g.b.k.b(paramViewHolder, "holder");
    ImageView localImageView = a();
    c.g.b.k.a(localImageView, "actionOneView");
    i.a((View)localImageView, paramk, paramViewHolder, (a)new c(this), "button");
    b().setOnClickListener((View.OnClickListener)new d(this));
  }
  
  public final void a(ActionType paramActionType)
  {
    if (paramActionType != null) {
      switch (ab.a[paramActionType.ordinal()])
      {
      default: 
        break;
      case 2: 
      case 3: 
        localDrawable = (Drawable)f.b();
        break;
      case 1: 
        localDrawable = (Drawable)e.b();
        break;
      }
    }
    Drawable localDrawable = null;
    if (localDrawable != null)
    {
      h = paramActionType;
      paramActionType = a();
      t.a((View)paramActionType);
      paramActionType.setImageDrawable(localDrawable);
      b(g);
      return;
    }
    if (paramActionType == ActionType.FLASH)
    {
      h = paramActionType;
      paramActionType = a();
      c.g.b.k.a(paramActionType, "actionOneView");
      t.b((View)paramActionType);
      b(g);
      return;
    }
    h = null;
    paramActionType = a();
    c.g.b.k.a(paramActionType, "actionOneView");
    t.b((View)paramActionType);
    paramActionType = b();
    c.g.b.k.a(paramActionType, "actionOneClickArea");
    t.b(paramActionType);
  }
  
  public final void a(x paramx)
  {
    CompoundFlashButton localCompoundFlashButton = c();
    Object localObject2;
    Object localObject1;
    if (paramx != null)
    {
      localObject2 = a;
      localObject1 = localObject2;
      if (localObject2 != null) {}
    }
    else
    {
      localObject1 = (List)y.a;
    }
    String str;
    if (paramx != null)
    {
      str = ae.a(paramx);
      localObject2 = str;
      if (str != null) {}
    }
    else
    {
      localObject2 = "";
    }
    if (paramx != null)
    {
      str = c;
      paramx = str;
      if (str != null) {}
    }
    else
    {
      paramx = "";
    }
    localCompoundFlashButton.a((List)localObject1, (String)localObject2, paramx);
  }
  
  public final void b(boolean paramBoolean)
  {
    g = paramBoolean;
    if (h != null)
    {
      View localView = b();
      if (paramBoolean)
      {
        t.a(localView);
        return;
      }
      t.c(localView);
    }
  }
  
  static final class a
    extends l
    implements a<Drawable>
  {
    a(aa paramaa)
    {
      super();
    }
  }
  
  static final class b
    extends l
    implements a<Drawable>
  {
    b(aa paramaa)
    {
      super();
    }
  }
  
  static final class c
    extends l
    implements a<String>
  {
    c(aa paramaa)
    {
      super();
    }
  }
  
  static final class d
    implements View.OnClickListener
  {
    d(aa paramaa) {}
    
    public final void onClick(View paramView)
    {
      if (aa.a(a) == ActionType.FLASH)
      {
        aa.b(a).performClick();
        return;
      }
      aa.c(a).performClick();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.aa
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */