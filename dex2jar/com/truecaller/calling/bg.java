package com.truecaller.calling;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import c.f;
import c.g;
import c.g.b.l;
import com.truecaller.utils.extensions.t;

public final class bg
  implements a
{
  private final f b;
  private final f c;
  private final f d;
  private final f e;
  private ActionType f;
  private final View g;
  
  public bg(View paramView)
  {
    g = paramView;
    b = t.a(g, 2131361918);
    c = t.a(g, 2131361842);
    d = g.a((c.g.a.a)new a(this));
    e = g.a((c.g.a.a)new b(this));
  }
  
  public final ImageView a()
  {
    return (ImageView)b.b();
  }
  
  public final void a(ActionType paramActionType)
  {
    f = paramActionType;
    if (paramActionType != null) {
      switch (bh.a[paramActionType.ordinal()])
      {
      default: 
        break;
      case 2: 
        paramActionType = (Drawable)e.b();
        break;
      case 1: 
        paramActionType = (Drawable)d.b();
        break;
      }
    }
    paramActionType = null;
    if (paramActionType != null)
    {
      a().setImageDrawable(paramActionType);
      return;
    }
  }
  
  public final View b()
  {
    return (View)c.b();
  }
  
  public final void b(boolean paramBoolean)
  {
    if (f == null)
    {
      t.b((View)a());
      t.b(b());
      return;
    }
    Object localObject = a();
    if (paramBoolean) {
      t.a((View)localObject);
    } else {
      t.b((View)localObject);
    }
    localObject = b();
    if (paramBoolean)
    {
      t.a((View)localObject);
      return;
    }
    t.c((View)localObject);
  }
  
  static final class a
    extends l
    implements c.g.a.a<Drawable>
  {
    a(bg parambg)
    {
      super();
    }
  }
  
  static final class b
    extends l
    implements c.g.a.a<Drawable>
  {
    b(bg parambg)
    {
      super();
    }
  }
  
  public static final class c
    extends l
    implements c.g.a.a<String>
  {
    public c(bg parambg)
    {
      super();
    }
  }
  
  public static final class d
    implements View.OnClickListener
  {
    public d(bg parambg) {}
    
    public final void onClick(View paramView)
    {
      bg.b(a).performClick();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.bg
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */