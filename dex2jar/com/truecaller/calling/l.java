package com.truecaller.calling;

import android.content.Context;
import android.telephony.TelephonyManager;
import c.g.b.w;
import com.truecaller.aftercall.AfterCallPromotionActivity;
import com.truecaller.aftercall.PromotionType;
import com.truecaller.androidactors.f;
import com.truecaller.calling.recorder.CallRecordingManager;
import com.truecaller.common.account.r;
import com.truecaller.common.h.am;
import com.truecaller.common.h.an;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.filters.FilterManager;
import com.truecaller.filters.FilterManager.FilterAction;
import com.truecaller.filters.g;
import com.truecaller.i.c;
import com.truecaller.multisim.h;
import com.truecaller.util.cn;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Stack;

public final class l
  implements k
{
  private final LinkedList<aj> a;
  private final Stack<String> b;
  private final boolean c;
  private final com.truecaller.utils.d d;
  private final c e;
  private final an f;
  private final FilterManager g;
  private final h h;
  private final r i;
  private final com.truecaller.common.h.u j;
  private final cn k;
  private final com.truecaller.utils.a l;
  private final com.truecaller.utils.l m;
  private final e n;
  private final f<com.truecaller.callhistory.a> o;
  private final com.truecaller.voip.d p;
  
  public l(com.truecaller.utils.d paramd, c paramc, an paraman, FilterManager paramFilterManager, h paramh, r paramr, com.truecaller.common.h.u paramu, cn paramcn, com.truecaller.utils.a parama, com.truecaller.utils.l paraml, CallRecordingManager paramCallRecordingManager, e parame, f<com.truecaller.callhistory.a> paramf, com.truecaller.voip.d paramd1)
  {
    d = paramd;
    e = paramc;
    f = paraman;
    g = paramFilterManager;
    h = paramh;
    i = paramr;
    j = paramu;
    k = paramcn;
    l = parama;
    m = paraml;
    n = parame;
    o = paramf;
    p = paramd1;
    a = new LinkedList();
    b = new Stack();
    c = paramCallRecordingManager.a();
  }
  
  private final boolean a(String paramString)
  {
    if (paramString == null) {
      return false;
    }
    if (b.contains(paramString)) {
      return true;
    }
    Object localObject = (Iterable)b;
    Collection localCollection = (Collection)new ArrayList(c.a.m.a((Iterable)localObject, 10));
    localObject = ((Iterable)localObject).iterator();
    while (((Iterator)localObject).hasNext())
    {
      String str = (String)((Iterator)localObject).next();
      com.truecaller.common.h.u localu = j;
      c.g.b.k.a(str, "it");
      localCollection.add(localu.b(str));
    }
    return ((List)localCollection).contains(j.b(paramString));
  }
  
  private static boolean b(String paramString)
  {
    int i1;
    if (paramString != null) {
      i1 = c.n.m.a((CharSequence)paramString, '#', 0, 6);
    } else {
      i1 = -1;
    }
    return i1 >= 0;
  }
  
  private final void c(String paramString)
  {
    Iterator localIterator = a.iterator();
    c.g.b.k.a(localIterator, "lastStates.iterator()");
    while (localIterator.hasNext())
    {
      Object localObject = localIterator.next();
      c.g.b.k.a(localObject, "it.next()");
      if (c.g.b.k.a(paramString, a)) {
        localIterator.remove();
      }
    }
  }
  
  public final aj a(Context paramContext, aj paramaj)
  {
    c.g.b.k.b(paramContext, "context");
    c.g.b.k.b(paramaj, "newState");
    int i5 = 1;
    "Receiver received call state change to ".concat(String.valueOf(paramaj));
    int i4 = 0;
    int i2;
    if (d.h() < 26) {
      i2 = 1;
    } else {
      i2 = 0;
    }
    if (d.h() >= 28) {
      i1 = 1;
    } else {
      i1 = 0;
    }
    boolean bool = d.c();
    Object localObject2 = null;
    if (i1 != 0)
    {
      localObject1 = (aj)c.a.m.g((List)a);
      int i3;
      if ((localObject1 != null) && (!(c.g.b.k.a(w.a(localObject1.getClass()), w.a(paramaj.getClass())) ^ true))) {
        i3 = 0;
      } else {
        i3 = 1;
      }
      if ((i3 != 0) && (a == null))
      {
        a.add(paramaj);
        localObject1 = new StringBuilder("Dropping state ");
        ((StringBuilder)localObject1).append(paramaj);
        ((StringBuilder)localObject1).append(" as it is a new state change with null number");
        ((StringBuilder)localObject1).toString();
        i1 = 1;
        break label441;
      }
      if (localObject1 != null) {
        localObject1 = a;
      } else {
        localObject1 = null;
      }
      if ((localObject1 != null) && (a == null))
      {
        localObject1 = new StringBuilder("Dropping state ");
        ((StringBuilder)localObject1).append(paramaj);
        ((StringBuilder)localObject1).append(" as it's not a new state change, previous state had a number and this one doesn't");
        ((StringBuilder)localObject1).toString();
        i1 = 1;
        break label441;
      }
    }
    if ((i2 == 0) && (!bool)) {
      i2 = 0;
    } else {
      i2 = 1;
    }
    if ((i1 != 0) && (a == null)) {
      i1 = 1;
    } else {
      i1 = 0;
    }
    if ((i2 != 0) && (a.contains(paramaj)) && (i1 == 0))
    {
      localObject1 = new StringBuilder("Dropping state ");
      ((StringBuilder)localObject1).append(paramaj);
      ((StringBuilder)localObject1).append(" because it's a duplicate");
      ((StringBuilder)localObject1).toString();
      localObject1 = a;
      localObject3 = ((LinkedList)localObject1).get(((LinkedList)localObject1).indexOf(paramaj));
      localObject1 = localObject3;
      if (!(localObject3 instanceof aj.e)) {
        localObject1 = null;
      }
      localObject1 = (aj.e)localObject1;
      if ((paramaj instanceof aj.e))
      {
        if (localObject1 != null) {
          localObject1 = e;
        } else {
          localObject1 = null;
        }
        if (localObject1 != null) {}
      }
      else
      {
        i1 = 1;
        break label441;
      }
    }
    int i1 = 0;
    label441:
    if (i1 != 0) {
      return null;
    }
    if (p.a(a)) {
      return null;
    }
    Object localObject1 = a;
    Object localObject3 = (CharSequence)localObject1;
    if ((localObject3 != null) && (!c.n.m.a((CharSequence)localObject3))) {
      i1 = 0;
    } else {
      i1 = 1;
    }
    if (i1 != 0)
    {
      if (!b.isEmpty()) {
        if ((paramaj instanceof aj.c)) {
          a = ((String)b.peek());
        } else if ((paramaj instanceof aj.b)) {
          a = ((String)b.pop());
        }
      }
    }
    else
    {
      if ((b((String)localObject1)) && (!c)) {
        return null;
      }
      if ((paramaj instanceof aj.e))
      {
        com.truecaller.util.a.a(paramContext);
        b.push(a);
      }
      else if (((paramaj instanceof aj.b)) && (!b.isEmpty()))
      {
        b.pop();
      }
    }
    bool = paramaj instanceof aj.e;
    if (bool)
    {
      localObject1 = a;
      if (localObject1 != null) {
        c((String)localObject1);
      }
    }
    if (bool)
    {
      localObject1 = localObject2;
      if (!e.b("truecaller.call_in_progress"))
      {
        localObject1 = a;
        if (!i.c())
        {
          "Account not created, not inspecting block options for ".concat(String.valueOf(localObject1));
        }
        else
        {
          localObject3 = paramContext.getSystemService("phone");
          if (localObject3 == null) {
            break label990;
          }
          Object localObject4 = (TelephonyManager)localObject3;
          localObject3 = ((TelephonyManager)localObject4).getNetworkCountryIso();
          c.g.b.k.a(localObject3, "telephonyManager.networkCountryIso");
          Locale localLocale = Locale.ENGLISH;
          c.g.b.k.a(localLocale, "Locale.ENGLISH");
          if (localObject3 == null) {
            break label979;
          }
          localObject3 = ((String)localObject3).toUpperCase(localLocale);
          c.g.b.k.a(localObject3, "(this as java.lang.String).toUpperCase(locale)");
          localObject4 = ((TelephonyManager)localObject4).getSimCountryIso();
          c.g.b.k.a(localObject4, "telephonyManager.simCountryIso");
          localLocale = Locale.ENGLISH;
          c.g.b.k.a(localLocale, "Locale.ENGLISH");
          if (localObject4 == null) {
            break label968;
          }
          localObject4 = ((String)localObject4).toUpperCase(localLocale);
          c.g.b.k.a(localObject4, "(this as java.lang.String).toUpperCase(locale)");
          localObject1 = g.a((String)localObject1, null, (String)am.e((CharSequence)localObject3, (CharSequence)localObject4), true);
          c.g.b.k.a(localObject1, "filterManager.findFilter…o), false, true\n        )");
          if (h == FilterManager.FilterAction.FILTER_BLACKLISTED)
          {
            i1 = 1;
            break label894;
          }
        }
        i1 = 0;
        label894:
        localObject1 = localObject2;
        if (i1 != 0)
        {
          i1 = i4;
          if ((e.a("blockCallMethod", 0) & 0x8) == 0) {
            i1 = 1;
          }
          if (i1 != 0) {
            if (m.a(new String[] { "android.permission.CALL_PHONE" }))
            {
              i1 = 1;
              break label959;
            }
          }
          i1 = 3;
          label959:
          localObject1 = Integer.valueOf(i1);
          break label1001;
          label968:
          throw new c.u("null cannot be cast to non-null type java.lang.String");
          label979:
          throw new c.u("null cannot be cast to non-null type java.lang.String");
          label990:
          throw new c.u("null cannot be cast to non-null type android.telephony.TelephonyManager");
        }
      }
      label1001:
      if (h.j()) {
        i1 = com.truecaller.callhistory.k.a(paramContext).a(1);
      } else {
        i1 = -1;
      }
      localObject1 = (aj)new aj.e(a, b, Integer.valueOf(i1), (Integer)localObject1);
    }
    else if ((paramaj instanceof aj.c))
    {
      e.b("truecaller.call_in_progress", true);
      localObject1 = paramaj;
      if (a != null)
      {
        localObject1 = paramaj;
        if (!a(a)) {
          localObject1 = (aj)new aj.d(a, b);
        }
      }
    }
    else if ((paramaj instanceof aj.b))
    {
      e.b("truecaller.call_in_progress", false);
      if (a.size() > 0)
      {
        localObject1 = a;
        if ((((LinkedList)localObject1).get(((LinkedList)localObject1).size() - 1) instanceof aj.e))
        {
          i1 = i5;
          break label1185;
        }
      }
      i1 = 0;
      label1185:
      localObject1 = paramaj;
      if (i1 != 0)
      {
        localObject1 = paramaj;
        if (!i.c())
        {
          localObject1 = paramaj;
          if (!k.b())
          {
            AfterCallPromotionActivity.a(paramContext, PromotionType.SIGN_UP);
            localObject1 = paramaj;
          }
        }
      }
    }
    else
    {
      localObject1 = null;
    }
    if (localObject1 != null) {
      a.add(localObject1);
    }
    return (aj)localObject1;
  }
  
  public final aj a(aj.d paramd)
  {
    c.g.b.k.b(paramd, "phoneState");
    paramd = a;
    if (paramd != null)
    {
      if (b(paramd)) {
        return null;
      }
      if (p.a(paramd)) {
        return null;
      }
      if (n.e().a())
      {
        String str = j.b(paramd);
        if (str != null) {
          ((com.truecaller.callhistory.a)o.a()).b(str);
        }
      }
      b.add(paramd);
      c(paramd);
      boolean bool = f.a(e.a("featureOutgoingSearch", 0L), 86400000L);
      if ((am.b(paramd)) && (bool)) {
        return (aj)new aj.d(paramd, l.a());
      }
      return null;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */