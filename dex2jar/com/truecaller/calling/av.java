package com.truecaller.calling;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import c.f;
import c.g.b.k;
import c.l;
import c.n.m;
import com.truecaller.calling.dialer.CallIconType;
import com.truecaller.common.h.j;
import com.truecaller.utils.extensions.t;
import com.truecaller.utils.ui.b;

public final class av
  implements az, h
{
  private final Drawable A;
  private Integer B;
  private boolean C;
  private boolean D;
  private boolean E;
  private final f b;
  private final f c;
  private final f d;
  private final f e;
  private final f f;
  private final f g;
  private final f h;
  private final f i;
  private final f j;
  private final f k;
  private final ColorStateList l;
  private final ColorStateList m;
  private final Drawable n;
  private final Drawable o;
  private final Drawable p;
  private final Drawable q;
  private final Drawable r;
  private final Drawable s;
  private final Drawable t;
  private final Drawable u;
  private final Drawable v;
  private final Drawable w;
  private final Drawable x;
  private final Drawable y;
  private final Drawable z;
  
  public av(View paramView)
  {
    b = t.a(paramView, 2131364872);
    c = t.a(paramView, 2131362385);
    d = t.a(paramView, 2131362804);
    e = t.a(paramView, 2131364520);
    f = t.a(paramView, 2131365423);
    g = t.a(paramView, 2131363765);
    h = t.a(paramView, 2131363509);
    i = t.a(paramView, 2131362608);
    j = t.a(paramView, 2131363504);
    k = t.a(paramView, 2131362805);
    l = b.b(paramView.getContext(), 2130969585);
    m = b.b(paramView.getContext(), 2130969592);
    n = b.a(paramView.getContext(), 2131233892, 2130968889);
    o = b.a(paramView.getContext(), 2131234295, 2130968889);
    p = b.a(paramView.getContext(), 2131234205, 2130969533);
    q = b.a(paramView.getContext(), 2131234352, 2130969533);
    r = b.a(paramView.getContext(), 2131234275, 2130968889);
    s = b.a(paramView.getContext(), 2131234140, 2130969528);
    t = b.a(paramView.getContext(), 2131234549, 2130969593);
    u = b.a(paramView.getContext(), 2131234550, 2130969593);
    v = b.a(paramView.getContext(), 2131234549, 2130969585);
    w = b.a(paramView.getContext(), 2131234550, 2130969585);
    x = b.a(paramView.getContext(), 2131234658, 2130969183);
    y = b.a(paramView.getContext(), 2131234658, 2130969585);
    z = b.a(paramView.getContext(), 2131233801, 2130969183);
    A = b.a(paramView.getContext(), 2131233801, 2130969585);
    paramView = a();
    k.a(paramView, "timestampText");
    t.a((View)paramView);
    paramView = b();
    k.a(paramView, "callTypeIcon");
    t.a((View)paramView);
  }
  
  private final TextView a()
  {
    return (TextView)b.b();
  }
  
  private final void a(TextView paramTextView)
  {
    ColorStateList localColorStateList;
    if (C) {
      localColorStateList = l;
    } else {
      localColorStateList = m;
    }
    paramTextView.setTextColor(localColorStateList);
  }
  
  private final ImageView b()
  {
    return (ImageView)c.b();
  }
  
  private final TextView c()
  {
    return (TextView)d.b();
  }
  
  private final ImageView d()
  {
    return (ImageView)e.b();
  }
  
  private final ImageView e()
  {
    return (ImageView)f.b();
  }
  
  private final ImageView f()
  {
    return (ImageView)g.b();
  }
  
  private final TextView g()
  {
    return (TextView)h.b();
  }
  
  private final TextView h()
  {
    return (TextView)i.b();
  }
  
  private final TextView i()
  {
    return (TextView)j.b();
  }
  
  private final TextView j()
  {
    return (TextView)k.b();
  }
  
  private final void k()
  {
    Object localObject = B;
    if ((localObject != null) && (((Integer)localObject).intValue() == 0))
    {
      if (C) {
        localObject = v;
      } else {
        localObject = t;
      }
    }
    else if ((localObject != null) && (((Integer)localObject).intValue() == 1))
    {
      if (C) {
        localObject = w;
      } else {
        localObject = u;
      }
    }
    else {
      localObject = null;
    }
    if (localObject != null)
    {
      ImageView localImageView = d();
      k.a(localImageView, "simIndicator");
      t.a((View)localImageView);
      d().setImageDrawable((Drawable)localObject);
      return;
    }
    localObject = d();
    k.a(localObject, "simIndicator");
    t.b((View)localObject);
  }
  
  private final void l()
  {
    if (D)
    {
      localObject = e();
      k.a(localObject, "videoCallIndicator");
      t.a((View)localObject);
      ImageView localImageView = e();
      if (C) {
        localObject = y;
      } else {
        localObject = x;
      }
      localImageView.setImageDrawable((Drawable)localObject);
      return;
    }
    Object localObject = e();
    k.a(localObject, "videoCallIndicator");
    t.b((View)localObject);
  }
  
  public final void a(CallIconType paramCallIconType)
  {
    k.b(paramCallIconType, "callIconType");
    switch (aw.a[paramCallIconType.ordinal()])
    {
    default: 
      throw new l();
    case 6: 
      paramCallIconType = s;
      break;
    case 5: 
      paramCallIconType = r;
      break;
    case 4: 
      paramCallIconType = q;
      break;
    case 3: 
      paramCallIconType = p;
      break;
    case 2: 
      paramCallIconType = o;
      break;
    case 1: 
      paramCallIconType = n;
    }
    b().setImageDrawable(paramCallIconType);
  }
  
  public final void a(Integer paramInteger)
  {
    B = paramInteger;
    k();
  }
  
  public final void a(Long paramLong)
  {
    boolean bool;
    if (paramLong != null) {
      bool = true;
    } else {
      bool = false;
    }
    TextView localTextView = i();
    k.a(localTextView, "callDurationView");
    t.a((View)localTextView, bool);
    localTextView = j();
    k.a(localTextView, "delimiterTwo");
    t.a((View)localTextView, bool);
    if (paramLong != null)
    {
      long l1 = ((Number)paramLong).longValue();
      paramLong = i();
      k.a(paramLong, "callDurationView");
      localTextView = i();
      k.a(localTextView, "callDurationView");
      paramLong.setText((CharSequence)j.c(localTextView.getContext(), l1));
      return;
    }
  }
  
  public final void b(Integer paramInteger)
  {
    if (paramInteger != null)
    {
      ((Number)paramInteger).intValue();
      TextView localTextView1 = h();
      k.a(localTextView1, "groupCountText");
      t.a((View)localTextView1);
      localTextView1 = h();
      k.a(localTextView1, "groupCountText");
      TextView localTextView2 = h();
      k.a(localTextView2, "groupCountText");
      localTextView1.setText((CharSequence)localTextView2.getContext().getString(2131888175, new Object[] { paramInteger }));
      return;
    }
    paramInteger = h();
    k.a(paramInteger, "groupCountText");
    t.b((View)paramInteger);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "timestamp");
    TextView localTextView = a();
    k.a(localTextView, "timestampText");
    localTextView.setText((CharSequence)paramString);
  }
  
  public final void b_(boolean paramBoolean)
  {
    C = paramBoolean;
    TextView localTextView = a();
    k.a(localTextView, "timestampText");
    a(localTextView);
    k();
    localTextView = c();
    k.a(localTextView, "delimiter");
    a(localTextView);
    localTextView = g();
    k.a(localTextView, "subtitle");
    a(localTextView);
    localTextView = h();
    k.a(localTextView, "groupCountText");
    a(localTextView);
    l();
    localTextView = i();
    k.a(localTextView, "callDurationView");
    a(localTextView);
    localTextView = j();
    k.a(localTextView, "delimiterTwo");
    a(localTextView);
  }
  
  public final void e(boolean paramBoolean)
  {
    D = paramBoolean;
    l();
  }
  
  public final void e_(String paramString)
  {
    paramString = (CharSequence)paramString;
    int i1;
    if ((paramString != null) && (!m.a(paramString))) {
      i1 = 0;
    } else {
      i1 = 1;
    }
    if (i1 == 0)
    {
      TextView localTextView = g();
      k.a(localTextView, "subtitle");
      t.a((View)localTextView);
      localTextView = c();
      k.a(localTextView, "delimiter");
      t.a((View)localTextView);
      localTextView = g();
      k.a(localTextView, "subtitle");
      localTextView.setText(paramString);
      return;
    }
    paramString = g();
    k.a(paramString, "subtitle");
    t.b((View)paramString);
    paramString = c();
    k.a(paramString, "delimiter");
    t.b((View)paramString);
  }
  
  public final void f(boolean paramBoolean)
  {
    E = paramBoolean;
    if (E)
    {
      localObject = f();
      k.a(localObject, "callRecordingIndicator");
      t.a((View)localObject);
      ImageView localImageView = f();
      if (C) {
        localObject = A;
      } else {
        localObject = z;
      }
      localImageView.setImageDrawable((Drawable)localObject);
      return;
    }
    Object localObject = f();
    k.a(localObject, "callRecordingIndicator");
    t.b((View)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.av
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */