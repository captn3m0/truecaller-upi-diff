package com.truecaller.calling.after_call;

import com.truecaller.common.h.an;
import com.truecaller.voip.ai;
import dagger.a.d;
import javax.inject.Provider;

public final class f
  implements d<e>
{
  private final Provider<ai> a;
  private final Provider<an> b;
  private final Provider<com.truecaller.i.e> c;
  
  private f(Provider<ai> paramProvider, Provider<an> paramProvider1, Provider<com.truecaller.i.e> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static f a(Provider<ai> paramProvider, Provider<an> paramProvider1, Provider<com.truecaller.i.e> paramProvider2)
  {
    return new f(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */