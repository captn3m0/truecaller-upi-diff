package com.truecaller.calling.after_call;

import com.truecaller.analytics.ae;
import com.truecaller.androidactors.f;
import com.truecaller.common.h.am;
import com.truecaller.common.tag.c;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.Tag;
import com.truecaller.log.AssertionUtil;
import com.truecaller.tracking.events.al;
import com.truecaller.tracking.events.al.a;
import com.truecaller.tracking.events.ay;
import com.truecaller.tracking.events.ay.a;
import com.truecaller.tracking.events.bc;
import com.truecaller.tracking.events.bc.a;
import com.truecaller.tracking.events.z;
import com.truecaller.tracking.events.z.a;
import com.truecaller.util.ce;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import org.apache.a.a;

final class AfterCallActivity$g
{
  boolean a = false;
  
  private AfterCallActivity$g(AfterCallActivity paramAfterCallActivity) {}
  
  final void a(String paramString)
  {
    if (a) {
      return;
    }
    a = true;
    z.a locala = z.b();
    Object localObject2 = locala.a(UUID.randomUUID().toString());
    Object localObject1;
    if (AfterCallActivity.M(b) == 1) {
      localObject1 = "widget";
    } else {
      localObject1 = "afterCall";
    }
    ((z.a)localObject2).d((CharSequence)localObject1).c(String.valueOf(AfterCallActivity.L(b)));
    locala.b(null);
    boolean bool = false;
    locala.a(false);
    locala.b(false);
    ArrayList localArrayList = new ArrayList();
    if (AfterCallActivity.o(b) != null)
    {
      localObject1 = al.b().b(am.b(AfterCallActivity.o(b).z()) ^ true).a(AfterCallActivity.H(b)).a(Integer.valueOf(Math.max(0, AfterCallActivity.o(b).I()))).d(Boolean.valueOf(AfterCallActivity.o(b).U())).a(Boolean.valueOf(AfterCallActivity.I(b))).c(Boolean.valueOf(AfterCallActivity.J(b))).b(Boolean.valueOf(AfterCallActivity.K(b)));
      if ((AfterCallActivity.o(b).getSource() & 0x40) != 0) {
        bool = true;
      }
      al localal = ((al.a)localObject1).e(Boolean.valueOf(bool)).a();
      localObject4 = new ArrayList();
      localObject3 = new ArrayList();
      localObject2 = new ArrayList();
      localObject1 = AfterCallActivity.o(b).J().iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject5 = (Tag)((Iterator)localObject1).next();
        if (((Tag)localObject5).getSource() == 1) {
          ((List)localObject4).add(((Tag)localObject5).a());
        } else {
          ((List)localObject3).add(((Tag)localObject5).a());
        }
      }
      localObject1 = ce.a(AfterCallActivity.o(b));
      if (localObject1 != null) {
        ((List)localObject2).add(String.valueOf(a));
      }
      Object localObject5 = bc.b();
      localObject1 = localObject4;
      if (((List)localObject4).isEmpty()) {
        localObject1 = null;
      }
      localObject4 = ((bc.a)localObject5).a((List)localObject1);
      localObject1 = localObject3;
      if (((List)localObject3).isEmpty()) {
        localObject1 = null;
      }
      localObject3 = ((bc.a)localObject4).b((List)localObject1);
      localObject1 = localObject2;
      if (((List)localObject2).isEmpty()) {
        localObject1 = null;
      }
      localObject5 = ((bc.a)localObject3).c((List)localObject1).a();
      Iterator localIterator = AfterCallActivity.o(b).A().iterator();
      localObject1 = null;
      for (;;)
      {
        localObject4 = localal;
        localObject3 = localObject5;
        localObject2 = localObject1;
        if (!localIterator.hasNext()) {
          break;
        }
        localObject2 = (Number)localIterator.next();
        if ((((Number)localObject2).getSource() & 0x1) != 0) {
          localObject1 = ((Number)localObject2).b();
        }
      }
    }
    Object localObject4 = al.b().b(false).a(false).a(Integer.valueOf(0)).d(Boolean.FALSE).a(Boolean.FALSE).c(Boolean.FALSE).b(Boolean.FALSE).e(Boolean.FALSE).a();
    Object localObject3 = null;
    localObject2 = localObject3;
    localArrayList.add(ay.b().a(am.e(AfterCallActivity.e(b), AfterCallActivity.f(b))).a((bc)localObject3).a((al)localObject4).b(paramString).c((CharSequence)localObject2).a());
    locala.a(localArrayList);
    locala.b(null);
    try
    {
      ((ae)b.d.a()).a(locala.a());
      return;
    }
    catch (a paramString)
    {
      AssertionUtil.reportThrowableButNeverCrash(paramString);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.AfterCallActivity.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */