package com.truecaller.calling.after_call;

import android.widget.Toast;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.f;
import com.truecaller.tag.c;
import com.truecaller.ui.components.AfterCallHeaderView;
import com.truecaller.ui.components.a;

final class AfterCallActivity$7
  implements a
{
  AfterCallActivity$7(AfterCallActivity paramAfterCallActivity) {}
  
  public final void a()
  {
    AfterCallActivity.r(a).e();
  }
  
  public final void a(boolean paramBoolean)
  {
    int i;
    String str;
    if (paramBoolean)
    {
      i = 2;
      str = "yes";
    }
    else
    {
      str = "no";
      i = 1;
    }
    ((c)AfterCallActivity.p(a).a()).a(AfterCallActivity.o(a), i);
    AfterCallActivity.q(a).b(new e.a("ViewAction").a("Context", "afterCall").a("Action", "businessSuggestion").a("SubAction", str).a());
    AfterCallActivity.r(a).e();
    Toast.makeText(a, 2131887241, 1).show();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.AfterCallActivity.7
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */