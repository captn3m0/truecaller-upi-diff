package com.truecaller.calling.after_call;

import c.g.b.k;
import com.truecaller.common.h.an;
import com.truecaller.data.entity.Contact;
import com.truecaller.voip.ai;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;

public final class e
  implements d
{
  final an a;
  final com.truecaller.i.e b;
  private final ai c;
  
  @Inject
  public e(ai paramai, an paraman, com.truecaller.i.e parame)
  {
    c = paramai;
    a = paraman;
    b = parame;
  }
  
  final Long a()
  {
    Long localLong = Long.valueOf(b.a("feature_voip_promo_after_call_first_timestamp", 0L));
    int i;
    if (((Number)localLong).longValue() == 0L) {
      i = 1;
    } else {
      i = 0;
    }
    if (i == 0) {
      return localLong;
    }
    return null;
  }
  
  public final void a(Contact paramContact, final boolean paramBoolean, final c paramc)
  {
    k.b(paramContact, "contact");
    k.b(paramc, "listener");
    c.a(paramContact, (com.truecaller.voip.e)new a(this, paramBoolean, paramc));
  }
  
  public static final class a
    implements com.truecaller.voip.e
  {
    a(boolean paramBoolean, c paramc) {}
    
    public final void onVoipAvailabilityLoaded(boolean paramBoolean)
    {
      if ((paramBoolean) && (!paramBoolean))
      {
        e locale = a;
        Long localLong = locale.a();
        if (localLong != null)
        {
          long l = localLong.longValue();
          int i = b.a("feature_voip_promo_after_call_period_days", 5);
          paramBoolean = a.a(l, i, TimeUnit.DAYS);
        }
        else
        {
          paramBoolean = false;
        }
        if (!paramBoolean)
        {
          if (a.a() == null) {
            a.b.b("feature_voip_promo_after_call_first_timestamp", System.currentTimeMillis());
          }
          paramc.shouldPromoteVoip(true);
          return;
        }
      }
      paramc.shouldPromoteVoip(false);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */