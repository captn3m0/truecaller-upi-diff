package com.truecaller.calling.after_call;

import android.app.Activity;
import com.truecaller.filters.FilterManager;
import com.truecaller.filters.FilterManager.ActionSource;
import com.truecaller.filters.g;
import com.truecaller.ui.f;

final class AfterCallActivity$5
  extends AfterCallActivity.a
{
  AfterCallActivity$5(AfterCallActivity paramAfterCallActivity, Activity paramActivity, FilterManager paramFilterManager)
  {
    super(paramActivity, paramFilterManager);
  }
  
  public final void a()
  {
    new AfterCallActivity.d(a, (byte)0);
  }
  
  public final void a(String paramString)
  {
    if (AfterCallActivity.h(a))
    {
      AfterCallActivity.i(a);
      AfterCallActivity.j(a);
      return;
    }
    if (AfterCallActivity.k(a))
    {
      AfterCallActivity.l(a);
      AfterCallActivity.a(a, paramString);
      return;
    }
    if (AfterCallActivity.m(a))
    {
      AfterCallActivity.n(a);
      a.a(paramString);
    }
  }
  
  public final void b()
  {
    Object localObject1 = a.a.b;
    Object localObject2 = AfterCallActivity.e(a);
    String str1 = AfterCallActivity.f(a);
    String str2 = AfterCallActivity.g(a);
    boolean bool2 = false;
    localObject1 = ((FilterManager)localObject1).a((String)localObject2, str1, str2, false);
    localObject2 = a;
    if (j == FilterManager.ActionSource.CUSTOM_BLACKLIST) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    AfterCallActivity.a((AfterCallActivity)localObject2, bool1);
    localObject2 = a;
    if (j == FilterManager.ActionSource.CUSTOM_WHITELIST) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    AfterCallActivity.b((AfterCallActivity)localObject2, bool1);
    localObject2 = a;
    boolean bool1 = bool2;
    if (j == FilterManager.ActionSource.TOP_SPAMMER) {
      bool1 = true;
    }
    AfterCallActivity.c((AfterCallActivity)localObject2, bool1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.AfterCallActivity.5
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */