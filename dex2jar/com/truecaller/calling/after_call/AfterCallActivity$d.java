package com.truecaller.calling.after_call;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import com.truecaller.common.h.am;
import com.truecaller.data.access.c;
import com.truecaller.data.entity.Contact;
import com.truecaller.filters.FilterManager;
import com.truecaller.filters.FilterManager.ActionSource;
import com.truecaller.filters.g;
import com.truecaller.old.a.a;
import com.truecaller.old.a.b;
import java.util.Collection;
import java.util.Iterator;

@SuppressLint({"StaticFieldLeak"})
final class AfterCallActivity$d
  extends a
{
  private AfterCallActivity$d(AfterCallActivity paramAfterCallActivity) {}
  
  public final void a(Object paramObject)
  {
    if (paramObject != null) {
      AfterCallActivity.a(a, (Contact)paramObject);
    }
    AfterCallActivity.B(a);
    a.d();
    if (!AfterCallActivity.C(a))
    {
      AfterCallActivity.D(a);
      return;
    }
    AfterCallActivity.c(a);
    paramObject = (String)am.e(AfterCallActivity.f(a), AfterCallActivity.e(a));
    if ((AfterCallActivity.o(a) != null) && (!TextUtils.isEmpty((CharSequence)paramObject)) && (!AfterCallActivity.E(a)))
    {
      AfterCallActivity.F(a);
      Ga).a = true;
      localObject = a;
      b.b(new AfterCallActivity.f((AfterCallActivity)localObject, AfterCallActivity.o((AfterCallActivity)localObject), (String)paramObject), new Object[0]);
      return;
    }
    Object localObject = AfterCallActivity.G(a);
    if (AfterCallActivity.H(a)) {
      paramObject = "inPhonebook";
    } else {
      paramObject = "validCacheResult";
    }
    ((AfterCallActivity.g)localObject).a((String)paramObject);
  }
  
  protected final Object doInBackground(Object... paramVarArgs)
  {
    if (AfterCallActivity.o(a) == null) {
      return null;
    }
    paramVarArgs = new c(a.getApplicationContext()).a(AfterCallActivity.o(a).getTcId());
    Object localObject = a.a.b.a(AfterCallActivity.e(a), AfterCallActivity.f(a), false);
    AfterCallActivity.a(a, false);
    AfterCallActivity.b(a, false);
    AfterCallActivity.c(a, false);
    localObject = ((Collection)localObject).iterator();
    do
    {
      if (!((Iterator)localObject).hasNext()) {
        break;
      }
      g localg = (g)((Iterator)localObject).next();
      AfterCallActivity localAfterCallActivity = a;
      boolean bool1 = AfterCallActivity.I(localAfterCallActivity);
      boolean bool2 = true;
      if ((!bool1) && (j != FilterManager.ActionSource.CUSTOM_BLACKLIST)) {
        bool1 = false;
      } else {
        bool1 = true;
      }
      AfterCallActivity.a(localAfterCallActivity, bool1);
      localAfterCallActivity = a;
      if ((!AfterCallActivity.J(localAfterCallActivity)) && (j != FilterManager.ActionSource.CUSTOM_WHITELIST)) {
        bool1 = false;
      } else {
        bool1 = true;
      }
      AfterCallActivity.b(localAfterCallActivity, bool1);
      localAfterCallActivity = a;
      bool1 = bool2;
      if (!AfterCallActivity.K(localAfterCallActivity)) {
        if (j == FilterManager.ActionSource.TOP_SPAMMER) {
          bool1 = bool2;
        } else {
          bool1 = false;
        }
      }
      AfterCallActivity.c(localAfterCallActivity, bool1);
    } while ((!AfterCallActivity.J(a)) && ((!AfterCallActivity.I(a)) || (!AfterCallActivity.K(a))));
    if (paramVarArgs != null)
    {
      localObject = a;
      AfterCallActivity.d((AfterCallActivity)localObject, com.truecaller.search.f.a((Context)localObject, paramVarArgs));
    }
    return paramVarArgs;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.AfterCallActivity.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */