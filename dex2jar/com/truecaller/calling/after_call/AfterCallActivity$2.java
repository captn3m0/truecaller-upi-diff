package com.truecaller.calling.after_call;

import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;

final class AfterCallActivity$2
  implements ViewTreeObserver.OnPreDrawListener
{
  AfterCallActivity$2(AfterCallActivity paramAfterCallActivity) {}
  
  public final boolean onPreDraw()
  {
    AfterCallActivity.e(a, false);
    float f = AfterCallActivity.x(a).getTop() * 1.5F;
    AfterCallActivity.y(a).setTranslationY(f);
    AfterCallActivity.z(a).setFloatValues(new float[] { f, 0.0F });
    AfterCallActivity.z(a).start();
    AfterCallActivity.y(a).getViewTreeObserver().removeOnPreDrawListener(this);
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.AfterCallActivity.2
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */