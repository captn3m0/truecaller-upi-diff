package com.truecaller.calling.after_call;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class AfterCallActivity$c
  extends BroadcastReceiver
{
  private AfterCallActivity$c(AfterCallActivity paramAfterCallActivity) {}
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    AfterCallActivity.b(a, paramIntent.getAction());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.AfterCallActivity.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */