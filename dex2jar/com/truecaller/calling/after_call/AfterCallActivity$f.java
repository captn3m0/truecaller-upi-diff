package com.truecaller.calling.after_call;

import android.annotation.SuppressLint;
import com.truecaller.bp;
import com.truecaller.data.entity.Contact;
import com.truecaller.search.h;
import java.util.UUID;

@SuppressLint({"StaticFieldLeak"})
final class AfterCallActivity$f
  extends h
{
  AfterCallActivity$f(AfterCallActivity paramAfterCallActivity, Contact paramContact, String paramString)
  {
    super(paramAfterCallActivity, paramAfterCallActivity, b, d, paramContact, paramString, "afterCall", UUID.randomUUID(), AfterCallActivity.A(paramAfterCallActivity).ch());
  }
  
  public final void a(Object paramObject)
  {
    if ((paramObject instanceof Contact))
    {
      AfterCallActivity.a(a, (Contact)paramObject);
      a.d();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.AfterCallActivity.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */