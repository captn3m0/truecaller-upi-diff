package com.truecaller.calling.after_call;

import com.truecaller.common.h.an;
import java.util.concurrent.TimeUnit;

public final class e$a
  implements com.truecaller.voip.e
{
  e$a(boolean paramBoolean, c paramc) {}
  
  public final void onVoipAvailabilityLoaded(boolean paramBoolean)
  {
    if ((paramBoolean) && (!b))
    {
      e locale = a;
      Long localLong = locale.a();
      if (localLong != null)
      {
        long l = localLong.longValue();
        int i = b.a("feature_voip_promo_after_call_period_days", 5);
        paramBoolean = a.a(l, i, TimeUnit.DAYS);
      }
      else
      {
        paramBoolean = false;
      }
      if (!paramBoolean)
      {
        if (a.a() == null) {
          a.b.b("feature_voip_promo_after_call_first_timestamp", System.currentTimeMillis());
        }
        c.shouldPromoteVoip(true);
        return;
      }
    }
    c.shouldPromoteVoip(false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.e.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */