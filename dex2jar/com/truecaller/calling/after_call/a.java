package com.truecaller.calling.after_call;

import com.truecaller.analytics.b;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.common.h.an;
import com.truecaller.i.e;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.transport.im.bp;
import java.util.concurrent.TimeUnit;

public final class a
{
  boolean a;
  final b b;
  final e c;
  private com.truecaller.androidactors.k d;
  private final f<bp> e;
  private final bw f;
  private final an g;
  
  public a(com.truecaller.androidactors.k paramk, f<bp> paramf, bw parambw, b paramb, e parame, an paraman)
  {
    d = paramk;
    e = paramf;
    f = parambw;
    b = paramb;
    c = parame;
    g = paraman;
  }
  
  private final boolean a()
  {
    long l = c.a("feature_im_promo_after_call_first_timestamp", 0L);
    if (l == 0L) {
      return false;
    }
    int i = c.a("feature_im_promo_after_call_period_days", 5);
    return g.a(l, i, TimeUnit.DAYS);
  }
  
  public final void a(String paramString, boolean paramBoolean, final a parama)
  {
    c.g.b.k.b(paramString, "normalizedNumber");
    c.g.b.k.b(parama, "callback");
    if ((f.a()) && (!paramBoolean) && (!a()))
    {
      ((bp)e.a()).b(paramString).a(d.a(), (ac)new b(this, parama));
      return;
    }
    a = false;
    parama.onResult(false);
  }
  
  public static abstract interface a
  {
    public abstract void onResult(boolean paramBoolean);
  }
  
  static final class b<R>
    implements ac<Boolean>
  {
    b(a parama, a.a parama1) {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */