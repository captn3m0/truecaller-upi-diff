package com.truecaller.calling.after_call;

import c.g.b.k;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.util.af;
import com.truecaller.utils.extensions.c;

public final class h
  implements g
{
  private final com.truecaller.utils.a a;
  private final af b;
  private final com.truecaller.common.g.a c;
  
  public h(com.truecaller.utils.a parama, af paramaf, com.truecaller.common.g.a parama1)
  {
    a = parama;
    b = paramaf;
    c = parama1;
  }
  
  public final boolean a()
  {
    if (c.a("featureBusinessSuggestionMaxCount", 0L) == 0L) {
      return false;
    }
    if (b.a(c.a("businessSuggestionShownTimestanp", 0L))) {
      return c.a("businessSuggestionShownCount", 0L) < c.a("featureBusinessSuggestionMaxCount", 0L);
    }
    return true;
  }
  
  public final boolean a(Contact paramContact, HistoryEvent paramHistoryEvent)
  {
    k.b(paramContact, "contact");
    k.b(paramHistoryEvent, "event");
    if ((paramHistoryEvent.f() != 1) && (paramHistoryEvent.f() != 2)) {
      return false;
    }
    paramHistoryEvent = paramContact.t();
    if (paramHistoryEvent != null)
    {
      boolean bool;
      if (((CharSequence)paramHistoryEvent).length() > 0) {
        bool = true;
      } else {
        bool = false;
      }
      paramHistoryEvent = Boolean.valueOf(bool);
    }
    else
    {
      paramHistoryEvent = null;
    }
    if (c.a(paramHistoryEvent)) {
      return false;
    }
    if (paramContact.Z()) {
      return false;
    }
    int i;
    if ((paramContact.getSource() & 0x10) == 16) {
      i = 1;
    } else {
      i = 0;
    }
    return i == 0;
  }
  
  public final void b()
  {
    long l = c.a("businessSuggestionShownTimestanp", 0L);
    if ((b.a(l) ^ true))
    {
      c.b("businessSuggestionShownTimestanp", a.a());
      c.b("businessSuggestionShownCount", 1L);
      return;
    }
    l = c.a("businessSuggestionShownCount", 0L);
    c.b("businessSuggestionShownCount", l + 1L);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */