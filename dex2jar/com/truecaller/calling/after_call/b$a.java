package com.truecaller.calling.after_call;

import c.d.a.a;
import c.d.b.a.f;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.analytics.e.a;
import com.truecaller.data.entity.Contact;
import com.truecaller.util.w;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import kotlinx.coroutines.ag;

@f(b="AfterCallSaveContactAnalyticsLogger.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.after_call.AfterCallSaveContactAnalyticsLogger$log$1")
final class b$a
  extends c.d.b.a.k
  implements m<ag, c<? super x>, Object>
{
  int a;
  private ag d;
  
  b$a(b paramb, Contact paramContact, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new a(b, c, paramc);
    d = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    if (a == 0)
    {
      if (!(paramObject instanceof o.b))
      {
        paramObject = w.a(b.a, c.E());
        c.g.b.k.a(paramObject, "ContactUtil.getExternalA…ext, contact.phonebookId)");
        localObject1 = (Iterable)paramObject;
        paramObject = (Collection)new ArrayList();
        localObject1 = ((Iterable)localObject1).iterator();
        while (((Iterator)localObject1).hasNext())
        {
          Object localObject2 = ((Iterator)localObject1).next();
          if (c.g.b.k.a(d, "com.whatsapp")) {
            ((Collection)paramObject).add(localObject2);
          }
        }
        paramObject = b.b;
        localObject1 = new e.a("AfterCallSaveContact").a("hasTCProfile", c.L()).a("hasWhatsAppProfile", true).a("isGoldUser", c.M()).a("isProUser", c.a(4)).a();
        c.g.b.k.a(localObject1, "AnalyticsEvent.Builder(A…                 .build()");
        ((com.truecaller.analytics.b)paramObject).b((com.truecaller.analytics.e)localObject1);
        return x.a;
      }
      throw a;
    }
    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((a)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.after_call.b.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */