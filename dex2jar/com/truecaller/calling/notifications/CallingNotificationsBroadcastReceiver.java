package com.truecaller.calling.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import c.g.b.k;
import com.truecaller.common.b.a;
import com.truecaller.common.h.o;
import com.truecaller.wizard.utils.i;

public final class CallingNotificationsBroadcastReceiver
  extends BroadcastReceiver
{
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramContext != null)
    {
      if (paramIntent == null) {
        return;
      }
      String str = paramIntent.getAction();
      if (str != null)
      {
        int i = str.hashCode();
        if (i != -2104750529)
        {
          if ((i == -720889926) && (str.equals("com.truecaller.request_set_as_default_phone_app")))
          {
            paramContext = a.F();
            k.a(paramContext, "ApplicationBase.getAppBase()");
            paramContext = (Context)paramContext;
            o.a(paramContext, o.a(paramContext).addFlags(268435456));
          }
        }
        else if (str.equals("com.truecaller.request_allow_draw_over_other_apps"))
        {
          i.b(paramContext);
          Toast.makeText(paramContext, 2131886810, 1).show();
          return;
        }
      }
      paramContext = new StringBuilder("Unknown action ");
      paramContext.append(paramIntent.getAction());
      paramContext.append(" in onReceive");
      throw ((Throwable)new RuntimeException(paramContext.toString()));
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.notifications.CallingNotificationsBroadcastReceiver
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */