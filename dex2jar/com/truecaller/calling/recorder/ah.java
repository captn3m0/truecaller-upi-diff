package com.truecaller.calling.recorder;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.k;
import com.truecaller.callhistory.r;
import dagger.a.d;
import javax.inject.Provider;

public final class ah
  implements d<f<r>>
{
  private final Provider<r> a;
  private final Provider<k> b;
  
  private ah(Provider<r> paramProvider, Provider<k> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static ah a(Provider<r> paramProvider, Provider<k> paramProvider1)
  {
    return new ah(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.ah
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */