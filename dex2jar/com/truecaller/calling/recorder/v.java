package com.truecaller.calling.recorder;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.content.FileProvider;
import c.g.b.k;
import com.truecaller.common.h.ai;
import java.io.File;
import javax.inject.Inject;

public final class v
  implements u
{
  private final Context a;
  
  @Inject
  public v(Context paramContext)
  {
    a = paramContext;
  }
  
  public final Intent a(String paramString)
  {
    k.b(paramString, "path");
    Intent localIntent = new Intent("android.intent.action.VIEW");
    localIntent.setFlags(268435456);
    Context localContext = a;
    localIntent.setDataAndType(FileProvider.a(localContext, ai.a(localContext), new File(paramString)), "audio/*");
    localIntent.addFlags(1);
    return localIntent;
  }
  
  public final boolean a(Intent paramIntent)
  {
    k.b(paramIntent, "intent");
    try
    {
      a.startActivity(paramIntent);
      return true;
    }
    catch (ActivityNotFoundException paramIntent)
    {
      for (;;) {}
    }
    return false;
  }
  
  public final Intent b(String paramString)
  {
    k.b(paramString, "path");
    paramString = a(paramString);
    paramString.setAction("android.intent.action.SEND");
    paramString.putExtra("android.intent.extra.STREAM", (Parcelable)paramString.getData());
    paramString = Intent.createChooser(paramString, (CharSequence)a.getString(2131887198));
    k.a(paramString, "this");
    paramString.setFlags(268435456);
    k.a(paramString, "Intent.createChooser(sha….FLAG_ACTIVITY_NEW_TASK }");
    return paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.v
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */