package com.truecaller.calling.recorder;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import c.g.b.k;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import java.util.Iterator;

public final class CallRecordingSettingsActivity
  extends AppCompatActivity
{
  public final void onCreate(Bundle paramBundle)
  {
    setTheme(aresId);
    super.onCreate(paramBundle);
    setContentView(2131558438);
    setSupportActionBar((Toolbar)findViewById(2131364907));
    paramBundle = getSupportActionBar();
    if (paramBundle != null) {
      paramBundle.setDisplayHomeAsUpEnabled(true);
    }
    paramBundle = getSupportActionBar();
    if (paramBundle != null)
    {
      paramBundle.setDisplayShowHomeEnabled(true);
      return;
    }
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    k.b(paramMenuItem, "item");
    if (paramMenuItem.getItemId() != 16908332) {
      return super.onOptionsItemSelected(paramMenuItem);
    }
    onBackPressed();
    return true;
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    k.b(paramArrayOfString, "permissions");
    k.b(paramArrayOfInt, "grantResults");
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    Object localObject = getSupportFragmentManager();
    k.a(localObject, "supportFragmentManager");
    localObject = ((j)localObject).f();
    k.a(localObject, "supportFragmentManager.fragments");
    localObject = ((Iterable)localObject).iterator();
    while (((Iterator)localObject).hasNext()) {
      ((Fragment)((Iterator)localObject).next()).onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingSettingsActivity
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */