package com.truecaller.calling.recorder;

import android.content.Context;
import com.truecaller.callrecording.a;
import dagger.a.d;
import javax.inject.Provider;

public final class az
  implements d<ay>
{
  private final Provider<Context> a;
  private final Provider<a> b;
  private final Provider<h> c;
  
  private az(Provider<Context> paramProvider, Provider<a> paramProvider1, Provider<h> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static az a(Provider<Context> paramProvider, Provider<a> paramProvider1, Provider<h> paramProvider2)
  {
    return new az(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.az
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */