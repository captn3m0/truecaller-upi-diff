package com.truecaller.calling.recorder;

import c.d.b.a.f;
import c.d.c;
import c.g.a.m;
import c.o.b;
import kotlinx.coroutines.ag;
import org.a.a.aa;
import org.a.a.ab;
import org.a.a.b;
import org.a.a.d.n;
import org.a.a.r;
import org.a.a.s;

@f(b="CallRecordingFloatingButtonPresenter.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter$updateTimerLabel$1")
final class CallRecordingFloatingButtonPresenter$h
  extends c.d.b.a.k
  implements m<ag, c<? super c.x>, Object>
{
  int a;
  private ag c;
  
  CallRecordingFloatingButtonPresenter$h(CallRecordingFloatingButtonPresenter paramCallRecordingFloatingButtonPresenter, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<c.x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new h(b, paramc);
    c = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    if (a == 0)
    {
      if (!(paramObject instanceof o.b))
      {
        Object localObject2 = b.e;
        if (localObject2 != null)
        {
          paramObject = (bw)b.b;
          if (paramObject != null)
          {
            localObject1 = b.g;
            localObject2 = ab.a((org.a.a.x)localObject2, (org.a.a.x)new b(b.j.a()));
            c.g.b.k.a(localObject2, "Seconds.secondsBetween(i…ock.currentTimeMillis()))");
            localObject1 = ((n)localObject1).a((aa)r.a(((ab)localObject2).c()).a(s.a()));
            c.g.b.k.a(localObject1, "periodFormatter.print(\n …ndard()\n                )");
            ((bw)paramObject).setLabel((String)localObject1);
          }
        }
        return c.x.a;
      }
      throw a;
    }
    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((h)a(paramObject1, (c)paramObject2)).a(c.x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */