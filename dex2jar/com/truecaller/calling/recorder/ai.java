package com.truecaller.calling.recorder;

import android.content.ContentResolver;
import com.truecaller.calling.dialer.v;
import dagger.a.d;
import javax.inject.Provider;

public final class ai
  implements d<v>
{
  private final Provider<ContentResolver> a;
  
  private ai(Provider<ContentResolver> paramProvider)
  {
    a = paramProvider;
  }
  
  public static ai a(Provider<ContentResolver> paramProvider)
  {
    return new ai(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.ai
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */