package com.truecaller.calling.recorder;

public enum CallRecordingOnBoardingLaunchContext
{
  static
  {
    CallRecordingOnBoardingLaunchContext localCallRecordingOnBoardingLaunchContext1 = new CallRecordingOnBoardingLaunchContext("DEEP_LINK", 0);
    DEEP_LINK = localCallRecordingOnBoardingLaunchContext1;
    CallRecordingOnBoardingLaunchContext localCallRecordingOnBoardingLaunchContext2 = new CallRecordingOnBoardingLaunchContext("LIST", 1);
    LIST = localCallRecordingOnBoardingLaunchContext2;
    CallRecordingOnBoardingLaunchContext localCallRecordingOnBoardingLaunchContext3 = new CallRecordingOnBoardingLaunchContext("SETTINGS", 2);
    SETTINGS = localCallRecordingOnBoardingLaunchContext3;
    CallRecordingOnBoardingLaunchContext localCallRecordingOnBoardingLaunchContext4 = new CallRecordingOnBoardingLaunchContext("PROMO", 3);
    PROMO = localCallRecordingOnBoardingLaunchContext4;
    CallRecordingOnBoardingLaunchContext localCallRecordingOnBoardingLaunchContext5 = new CallRecordingOnBoardingLaunchContext("WHATS_NEW", 4);
    WHATS_NEW = localCallRecordingOnBoardingLaunchContext5;
    CallRecordingOnBoardingLaunchContext localCallRecordingOnBoardingLaunchContext6 = new CallRecordingOnBoardingLaunchContext("DIALOG", 5);
    DIALOG = localCallRecordingOnBoardingLaunchContext6;
    CallRecordingOnBoardingLaunchContext localCallRecordingOnBoardingLaunchContext7 = new CallRecordingOnBoardingLaunchContext("UNKNOWN", 6);
    UNKNOWN = localCallRecordingOnBoardingLaunchContext7;
    CallRecordingOnBoardingLaunchContext localCallRecordingOnBoardingLaunchContext8 = new CallRecordingOnBoardingLaunchContext("PAY_WALL", 7);
    PAY_WALL = localCallRecordingOnBoardingLaunchContext8;
    $VALUES = new CallRecordingOnBoardingLaunchContext[] { localCallRecordingOnBoardingLaunchContext1, localCallRecordingOnBoardingLaunchContext2, localCallRecordingOnBoardingLaunchContext3, localCallRecordingOnBoardingLaunchContext4, localCallRecordingOnBoardingLaunchContext5, localCallRecordingOnBoardingLaunchContext6, localCallRecordingOnBoardingLaunchContext7, localCallRecordingOnBoardingLaunchContext8 };
  }
  
  private CallRecordingOnBoardingLaunchContext() {}
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingOnBoardingLaunchContext
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */