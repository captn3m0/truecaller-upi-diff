package com.truecaller.calling.recorder;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources.Theme;
import android.os.Bundle;
import android.support.v4.app.e;
import android.support.v4.app.j;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import c.g.b.k;
import c.u;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.startup_dialogs.fragments.CallRecordingOnBoardingDialog;
import com.truecaller.startup_dialogs.fragments.CallRecordingOnBoardingDialog.Companion;
import com.truecaller.startup_dialogs.fragments.CallRecordingOnBoardingDialog.Companion.Mode;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.wizard.utils.i;
import javax.inject.Inject;

public final class CallRecordingOnBoardingActivity
  extends AppCompatActivity
  implements an
{
  @Inject
  public al a;
  
  public final al a()
  {
    al localal = a;
    if (localal == null) {
      k.a("presenter");
    }
    return localal;
  }
  
  public final void a(CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext)
  {
    k.b(paramCallRecordingOnBoardingLaunchContext, "launchContext");
    Object localObject = CallRecordingOnBoardingDialog.e;
    localObject = getSupportFragmentManager();
    k.a(localObject, "supportFragmentManager");
    CallRecordingOnBoardingDialog.Companion.a((j)localObject, false, paramCallRecordingOnBoardingLaunchContext);
  }
  
  public final void a(CharSequence paramCharSequence)
  {
    k.b(paramCharSequence, "message");
    new AlertDialog.Builder((Context)this).setTitle(2131887611).setMessage(paramCharSequence).setPositiveButton(2131887608, (DialogInterface.OnClickListener)new g(this)).setNegativeButton(2131887609, (DialogInterface.OnClickListener)new h(this)).setOnCancelListener((DialogInterface.OnCancelListener)new i(this)).show();
  }
  
  public final void a(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
  {
    k.b(paramCharSequence1, "message");
    k.b(paramCharSequence2, "ctaText");
    new AlertDialog.Builder((Context)this).setMessage(paramCharSequence1).setPositiveButton(2131887619, (DialogInterface.OnClickListener)new a(this)).setNegativeButton(paramCharSequence2, (DialogInterface.OnClickListener)new b(this)).setOnCancelListener((DialogInterface.OnCancelListener)new c(this)).show();
  }
  
  public final void a(String[] paramArrayOfString)
  {
    k.b(paramArrayOfString, "requiredPermissions");
    android.support.v4.app.a.a((Activity)this, paramArrayOfString, 102);
  }
  
  public final void b(CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext)
  {
    k.b(paramCallRecordingOnBoardingLaunchContext, "launchContext");
    Object localObject = CallRecordingOnBoardingDialog.e;
    localObject = getSupportFragmentManager();
    k.a(localObject, "supportFragmentManager");
    k.b(localObject, "fragmentManager");
    k.b(paramCallRecordingOnBoardingLaunchContext, "launchContext");
    CallRecordingOnBoardingDialog.Companion.a(CallRecordingOnBoardingDialog.Companion.Mode.PAY_WALL, paramCallRecordingOnBoardingLaunchContext).show((j)localObject, CallRecordingOnBoardingDialog.class.getName());
  }
  
  public final void b(CharSequence paramCharSequence)
  {
    k.b(paramCharSequence, "message");
    new AlertDialog.Builder((Context)this).setTitle(2131887583).setMessage(paramCharSequence).setPositiveButton(2131887580, (DialogInterface.OnClickListener)new d(this)).setNegativeButton(2131887581, (DialogInterface.OnClickListener)new e(this)).setOnCancelListener((DialogInterface.OnCancelListener)new f(this)).show();
  }
  
  public final void c(CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext)
  {
    k.b(paramCallRecordingOnBoardingLaunchContext, "launchContext");
    Object localObject = CallRecordingOnBoardingDialog.e;
    localObject = getSupportFragmentManager();
    k.a(localObject, "supportFragmentManager");
    k.b(localObject, "fragmentManager");
    k.b(paramCallRecordingOnBoardingLaunchContext, "launchContext");
    CallRecordingOnBoardingDialog.Companion.a(CallRecordingOnBoardingDialog.Companion.Mode.PAY_WALL_ON_EXPIRY, paramCallRecordingOnBoardingLaunchContext).show((j)localObject, CallRecordingOnBoardingDialog.class.getName());
  }
  
  public final void d(CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext)
  {
    k.b(paramCallRecordingOnBoardingLaunchContext, "launchContext");
    Object localObject = CallRecordingOnBoardingDialog.e;
    localObject = getSupportFragmentManager();
    k.a(localObject, "supportFragmentManager");
    CallRecordingOnBoardingDialog.Companion.a((j)localObject, true, paramCallRecordingOnBoardingLaunchContext);
  }
  
  public final void e(CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext)
  {
    k.b(paramCallRecordingOnBoardingLaunchContext, "launchContext");
    Object localObject = CallRecordingOnBoardingDialog.e;
    localObject = getSupportFragmentManager();
    k.a(localObject, "supportFragmentManager");
    k.b(localObject, "fragmentManager");
    k.b(paramCallRecordingOnBoardingLaunchContext, "launchContext");
    CallRecordingOnBoardingDialog.Companion.a(CallRecordingOnBoardingDialog.Companion.Mode.AFTER_ENABLE, paramCallRecordingOnBoardingLaunchContext).show((j)localObject, CallRecordingOnBoardingDialog.class.getName());
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    com.truecaller.utils.extensions.a.a(this);
    getTheme().applyStyle(aresId, false);
    paramBundle = getApplicationContext();
    if (paramBundle != null)
    {
      ((bk)paramBundle).a().cy().a(this);
      CallRecordingOnBoardingState localCallRecordingOnBoardingState = (CallRecordingOnBoardingState)getIntent().getSerializableExtra("State");
      paramBundle = getIntent().getSerializableExtra("LaunchContext");
      if (paramBundle != null)
      {
        paramBundle = (CallRecordingOnBoardingLaunchContext)paramBundle;
        al localal = a;
        if (localal == null) {
          k.a("presenter");
        }
        localal.a(this);
        localal = a;
        if (localal == null) {
          k.a("presenter");
        }
        if (paramBundle != null) {
          c = paramBundle;
        }
        if (localCallRecordingOnBoardingState == null)
        {
          localal.b();
          return;
        }
        paramBundle = localCallRecordingOnBoardingState;
        if (f.g())
        {
          paramBundle = localCallRecordingOnBoardingState;
          if (localCallRecordingOnBoardingState == CallRecordingOnBoardingState.WHATS_NEW) {
            paramBundle = CallRecordingOnBoardingState.PAY_WALL;
          }
        }
        a = paramBundle;
        localal.a();
        return;
      }
      throw new u("null cannot be cast to non-null type com.truecaller.calling.recorder.CallRecordingOnBoardingLaunchContext");
    }
    throw new u("null cannot be cast to non-null type com.truecaller.GraphHolder");
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    al localal = a;
    if (localal == null) {
      k.a("presenter");
    }
    localal.y_();
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    k.b(paramArrayOfString, "permissions");
    k.b(paramArrayOfInt, "grantResults");
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    al localal = a;
    if (localal == null) {
      k.a("presenter");
    }
    k.b(paramArrayOfString, "permissions");
    k.b(paramArrayOfInt, "grantResults");
    if (paramInt == 102)
    {
      i.a(paramArrayOfString, paramArrayOfInt);
      if (localal.c())
      {
        localal.e();
        paramArrayOfString = (an)b;
        if (paramArrayOfString != null) {
          paramArrayOfString.e(c);
        }
        return;
      }
      paramArrayOfString = (an)b;
      if (paramArrayOfString != null)
      {
        paramArrayOfString.finish();
        return;
      }
    }
  }
  
  static final class a
    implements DialogInterface.OnClickListener
  {
    a(CallRecordingOnBoardingActivity paramCallRecordingOnBoardingActivity) {}
    
    public final void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
      paramDialogInterface = a.a();
      e.b("callRecordingOnBoardDismissed", true);
      paramDialogInterface = (an)b;
      if (paramDialogInterface != null)
      {
        paramDialogInterface.finish();
        return;
      }
    }
  }
  
  static final class b
    implements DialogInterface.OnClickListener
  {
    b(CallRecordingOnBoardingActivity paramCallRecordingOnBoardingActivity) {}
    
    public final void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
      a.a().b();
    }
  }
  
  static final class c
    implements DialogInterface.OnCancelListener
  {
    c(CallRecordingOnBoardingActivity paramCallRecordingOnBoardingActivity) {}
    
    public final void onCancel(DialogInterface paramDialogInterface)
    {
      a.finish();
    }
  }
  
  static final class d
    implements DialogInterface.OnClickListener
  {
    d(CallRecordingOnBoardingActivity paramCallRecordingOnBoardingActivity) {}
    
    public final void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
      paramDialogInterface = a.a();
      an localan = (an)b;
      if (localan != null)
      {
        localan.a(d);
        return;
      }
    }
  }
  
  static final class e
    implements DialogInterface.OnClickListener
  {
    e(CallRecordingOnBoardingActivity paramCallRecordingOnBoardingActivity) {}
    
    public final void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
      a.finish();
    }
  }
  
  static final class f
    implements DialogInterface.OnCancelListener
  {
    f(CallRecordingOnBoardingActivity paramCallRecordingOnBoardingActivity) {}
    
    public final void onCancel(DialogInterface paramDialogInterface)
    {
      a.finish();
    }
  }
  
  static final class g
    implements DialogInterface.OnClickListener
  {
    g(CallRecordingOnBoardingActivity paramCallRecordingOnBoardingActivity) {}
    
    public final void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
      paramDialogInterface = a.a();
      e.b("callRecordingTermsAccepted", true);
      paramDialogInterface.b();
    }
  }
  
  static final class h
    implements DialogInterface.OnClickListener
  {
    h(CallRecordingOnBoardingActivity paramCallRecordingOnBoardingActivity) {}
    
    public final void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
      a.finish();
    }
  }
  
  static final class i
    implements DialogInterface.OnCancelListener
  {
    i(CallRecordingOnBoardingActivity paramCallRecordingOnBoardingActivity) {}
    
    public final void onCancel(DialogInterface paramDialogInterface)
    {
      a.finish();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingOnBoardingActivity
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */