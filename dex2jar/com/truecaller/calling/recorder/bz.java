package com.truecaller.calling.recorder;

import android.view.View;
import android.view.View.OnClickListener;
import c.f;
import c.g.b.k;
import com.truecaller.calling.ActionType;
import com.truecaller.calling.a;
import com.truecaller.ui.view.TintedImageView;
import com.truecaller.utils.extensions.t;

public final class bz
  implements a
{
  private final f b;
  private final f c;
  
  public bz(View paramView)
  {
    b = t.a(paramView, 2131361918);
    c = t.a(paramView, 2131361842);
    a().setTint(null);
    a().setImageResource(2131234388);
  }
  
  final TintedImageView a()
  {
    return (TintedImageView)b.b();
  }
  
  public final void a(ActionType paramActionType) {}
  
  final View b()
  {
    return (View)c.b();
  }
  
  public final void b(boolean paramBoolean)
  {
    Object localObject = b();
    k.a(localObject, "actionOneClickArea");
    t.a((View)localObject, paramBoolean);
    localObject = a();
    k.a(localObject, "actionOneView");
    t.a((View)localObject, paramBoolean);
  }
  
  static final class a
    implements View.OnClickListener
  {
    a(bz parambz) {}
    
    public final void onClick(View paramView)
    {
      bz.a(a).performClick();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.bz
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */