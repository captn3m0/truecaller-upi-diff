package com.truecaller.calling.recorder;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.a;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import c.f;
import c.g.b.k;
import com.truecaller.calling.ActionType;
import com.truecaller.util.at;
import com.truecaller.utils.extensions.t;

public final class cc
  implements com.truecaller.calling.b
{
  private final f b;
  private final f c;
  private final ColorStateList d;
  private final Drawable e;
  
  public cc(View paramView)
  {
    b = t.a(paramView, 2131361939);
    c = t.a(paramView, 2131361843);
    d = com.truecaller.utils.ui.b.b(paramView.getContext(), 2130969592);
    paramView = a.e(at.a(paramView.getContext(), 2131234256)).mutate();
    a.a(paramView, d);
    e = paramView;
    a().setImageDrawable(e);
  }
  
  final ImageView a()
  {
    return (ImageView)b.b();
  }
  
  final View b()
  {
    return (View)c.b();
  }
  
  public final void b(ActionType paramActionType) {}
  
  public final void c(boolean paramBoolean)
  {
    Object localObject = b();
    k.a(localObject, "actionTwoClickArea");
    t.a((View)localObject, paramBoolean);
    localObject = a();
    k.a(localObject, "actionTwoView");
    t.a((View)localObject, paramBoolean);
  }
  
  static final class a
    implements View.OnClickListener
  {
    a(cc paramcc) {}
    
    public final void onClick(View paramView)
    {
      cc.a(a).performClick();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.cc
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */