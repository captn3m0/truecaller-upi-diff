package com.truecaller.calling.recorder;

import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.view.View.OnClickListener;
import com.truecaller.R.id;

final class CallRecordingSettingsFragment$d
  implements View.OnClickListener
{
  CallRecordingSettingsFragment$d(CallRecordingSettingsFragment paramCallRecordingSettingsFragment) {}
  
  public final void onClick(View paramView)
  {
    ((SwitchCompat)a.a(R.id.settingRecordingEnabledSwitch)).toggle();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingSettingsFragment.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */