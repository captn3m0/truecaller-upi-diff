package com.truecaller.calling.recorder;

import c.d.b.a.b;
import c.d.b.a.f;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import java.util.Map;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;

@f(b="CallRecordingItemsPresenter.kt", c={143}, d="invokeSuspend", e="com.truecaller.calling.recorder.CallRecordingItemsPresenterImpl$onBindItem$1$2$fetchDurationJob$1")
final class ad$a
  extends c.d.b.a.k
  implements m<ag, c<? super x>, Object>
{
  Object a;
  Object b;
  int c;
  private ag n;
  
  ad$a(c paramc, aa paramaa1, ad paramad, Contact paramContact, boolean paramBoolean1, HistoryEvent paramHistoryEvent, String paramString, CallRecording paramCallRecording, boolean paramBoolean2, aa paramaa2, int paramInt)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new a(paramc, d, e, f, g, h, i, j, k, l, m);
    n = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    Map localMap;
    Object localObject2;
    switch (c)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 1: 
      localObject1 = (CallRecording)b;
      localMap = (Map)a;
      if (!(paramObject instanceof o.b)) {
        localObject2 = paramObject;
      } else {
        throw a;
      }
      break;
    case 0: 
      if ((paramObject instanceof o.b)) {
        break label196;
      }
      localMap = (Map)ad.a(e);
      paramObject = j;
      localObject2 = com.truecaller.data.entity.a.a((CallRecording)paramObject, ad.b(e), ad.c(e));
      a = localMap;
      b = paramObject;
      c = 1;
      localObject2 = ((ao)localObject2).a(this);
      if (localObject2 == localObject1) {
        return localObject1;
      }
      localObject1 = paramObject;
    }
    localMap.put(localObject1, b.a(((Number)localObject2).longValue() / 1000L));
    ad.d(e).e();
    return x.a;
    label196:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((a)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.ad.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */