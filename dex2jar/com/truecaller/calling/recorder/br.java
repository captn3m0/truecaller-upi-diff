package com.truecaller.calling.recorder;

import com.truecaller.bp;
import com.truecaller.utils.n;
import dagger.a.g;
import javax.inject.Provider;

public final class br
  implements ao
{
  private final bp a;
  private Provider<com.truecaller.common.f.c> b;
  private Provider<CallRecordingManager> c;
  private Provider<bv> d;
  private Provider<n> e;
  private Provider<ar> f;
  
  private br(ap paramap, bp parambp)
  {
    a = parambp;
    b = new c(parambp);
    c = new b(parambp);
    d = dagger.a.c.a(c);
    e = new d(parambp);
    f = dagger.a.c.a(aq.a(paramap, b, d, e, c));
  }
  
  public static a a()
  {
    return new a((byte)0);
  }
  
  public final void a(CallRecordingPromoViewImpl paramCallRecordingPromoViewImpl)
  {
    a = ((ar)f.get());
    b = ((com.truecaller.premium.br)g.a(a.bF(), "Cannot return null from a non-@Nullable component method"));
    c = ((aj)g.a(a.bU(), "Cannot return null from a non-@Nullable component method"));
  }
  
  public static final class a
  {
    private ap a;
    private bp b;
    
    public final ao a()
    {
      g.a(a, ap.class);
      g.a(b, bp.class);
      return new br(a, b, (byte)0);
    }
    
    public final a a(bp parambp)
    {
      b = ((bp)g.a(parambp));
      return this;
    }
    
    public final a a(ap paramap)
    {
      a = ((ap)g.a(paramap));
      return this;
    }
  }
  
  static final class b
    implements Provider<CallRecordingManager>
  {
    private final bp a;
    
    b(bp parambp)
    {
      a = parambp;
    }
  }
  
  static final class c
    implements Provider<com.truecaller.common.f.c>
  {
    private final bp a;
    
    c(bp parambp)
    {
      a = parambp;
    }
  }
  
  static final class d
    implements Provider<n>
  {
    private final bp a;
    
    d(bp parambp)
    {
      a = parambp;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.br
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */