package com.truecaller.calling.recorder;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import c.g.b.k;
import java.io.Serializable;
import javax.inject.Inject;

public final class aj
{
  public static Intent a(Context paramContext, CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext, CallRecordingOnBoardingState paramCallRecordingOnBoardingState)
  {
    Intent localIntent = new Intent(paramContext, CallRecordingOnBoardingActivity.class);
    localIntent.putExtra("LaunchContext", (Serializable)paramCallRecordingOnBoardingLaunchContext);
    if (paramCallRecordingOnBoardingState != null) {
      localIntent.putExtra("State", (Serializable)paramCallRecordingOnBoardingState);
    }
    if (!(paramContext instanceof Activity)) {
      localIntent.setFlags(268435456);
    }
    return localIntent;
  }
  
  public static void a(Context paramContext, CallRecordingOnBoardingState paramCallRecordingOnBoardingState, CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext)
  {
    k.b(paramContext, "context");
    k.b(paramCallRecordingOnBoardingState, "state");
    k.b(paramCallRecordingOnBoardingLaunchContext, "launchContext");
    paramContext.startActivity(a(paramContext, paramCallRecordingOnBoardingLaunchContext, paramCallRecordingOnBoardingState));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.aj
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */