package com.truecaller.calling.recorder;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.view.ActionMode.Callback;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import c.a.ae;
import c.a.m;
import c.g.b.k;
import c.g.b.l;
import c.k.i;
import c.u;
import com.truecaller.R.id;
import com.truecaller.adapter_delegates.a;
import com.truecaller.adapter_delegates.c;
import com.truecaller.adapter_delegates.g;
import com.truecaller.adapter_delegates.p;
import com.truecaller.adapter_delegates.s;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.premium.br;
import com.truecaller.ui.SingleActivity;
import com.truecaller.ui.SingleActivity.FragmentSingle;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.ui.details.d;
import com.truecaller.ui.q;
import com.truecaller.utils.extensions.t;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import javax.inject.Inject;
import javax.inject.Named;

public final class bj
  extends d
  implements bq
{
  public static final a f = new a((byte)0);
  @Inject
  public bl a;
  @Inject
  public ad b;
  @Inject
  @Named("list_promoview")
  public ar c;
  @Inject
  public br d;
  @Inject
  public aj e;
  private RecyclerView g;
  private a h;
  private com.truecaller.adapter_delegates.f i;
  private a l;
  private final Object m = new Object();
  private ActionMode n;
  private final g o = new g((byte)0);
  private final b p = new b(this);
  private HashMap q;
  
  private View a(int paramInt)
  {
    if (q == null) {
      q = new HashMap();
    }
    View localView2 = (View)q.get(Integer.valueOf(paramInt));
    View localView1 = localView2;
    if (localView2 == null)
    {
      localView1 = getView();
      if (localView1 == null) {
        return null;
      }
      localView1 = localView1.findViewById(paramInt);
      q.put(Integer.valueOf(paramInt), localView1);
    }
    return localView1;
  }
  
  public static final void a(Context paramContext)
  {
    k.b(paramContext, "context");
    paramContext.startActivity(SingleActivity.a(paramContext, SingleActivity.FragmentSingle.CALL_RECORDINGS));
  }
  
  public final void A_()
  {
    android.support.v4.app.f localf = getActivity();
    if (localf != null)
    {
      ((AppCompatActivity)localf).startSupportActionMode((ActionMode.Callback)p);
      return;
    }
    throw new u("null cannot be cast to non-null type android.support.v7.app.AppCompatActivity");
  }
  
  public final void B_()
  {
    ActionMode localActionMode = n;
    if (localActionMode != null)
    {
      int j = p.a;
      Object localObject = localActionMode.getTag();
      if (((localObject instanceof Integer)) && (j == ((Integer)localObject).intValue())) {
        j = 1;
      } else {
        j = 0;
      }
      if (j == 0) {
        localActionMode = null;
      }
      if (localActionMode != null)
      {
        localActionMode.finish();
        return;
      }
    }
  }
  
  public final void a(Contact paramContact, DetailsFragment.SourceType paramSourceType, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    k.b(paramContact, "contact");
    k.b(paramSourceType, "sourceType");
    android.support.v4.app.f localf = getActivity();
    if (localf == null) {
      return;
    }
    k.a(localf, "activity ?: return");
    DetailsFragment.a((Context)localf, paramContact, paramSourceType, paramBoolean1, paramBoolean2, paramBoolean3);
  }
  
  public final void a(HistoryEvent paramHistoryEvent, DetailsFragment.SourceType paramSourceType, boolean paramBoolean1, boolean paramBoolean2)
  {
    k.b(paramHistoryEvent, "historyEvent");
    k.b(paramSourceType, "sourceType");
    Object localObject1 = getActivity();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "activity ?: return");
    Context localContext = (Context)localObject1;
    localObject1 = paramHistoryEvent.s();
    if (localObject1 != null) {
      localObject1 = ((Contact)localObject1).getTcId();
    } else {
      localObject1 = null;
    }
    Object localObject2 = paramHistoryEvent.s();
    if (localObject2 != null) {
      localObject2 = ((Contact)localObject2).t();
    } else {
      localObject2 = null;
    }
    DetailsFragment.a(localContext, (String)localObject1, (String)localObject2, paramHistoryEvent.a(), paramHistoryEvent.b(), paramHistoryEvent.d(), paramSourceType, paramBoolean1, paramBoolean2);
  }
  
  public final void a(PremiumPresenterView.LaunchContext paramLaunchContext)
  {
    k.b(paramLaunchContext, "launchContext");
    if (d == null) {
      k.a("premiumScreenNavigator");
    }
    Context localContext = requireContext();
    k.a(localContext, "requireContext()");
    br.a(localContext, paramLaunchContext, "premiumCallRecording");
  }
  
  public final void a(String paramString, final Object paramObject, final by paramby)
  {
    k.b(paramString, "message");
    k.b(paramObject, "objectsDeleted");
    k.b(paramby, "caller");
    Object localObject = getContext();
    if (localObject != null)
    {
      localObject = new AlertDialog.Builder((Context)localObject);
      ((AlertDialog.Builder)localObject).setMessage((CharSequence)paramString);
      ((AlertDialog.Builder)localObject).setPositiveButton(2131887235, (DialogInterface.OnClickListener)new h(paramString, paramby, paramObject));
      ((AlertDialog.Builder)localObject).setNegativeButton(2131887197, (DialogInterface.OnClickListener)new i(paramString, paramby, paramObject));
      ((AlertDialog.Builder)localObject).show();
      return;
    }
  }
  
  public final void a(Set<Integer> paramSet)
  {
    k.b(paramSet, "items");
    paramSet = ((Iterable)paramSet).iterator();
    while (paramSet.hasNext())
    {
      int j = ((Number)paramSet.next()).intValue();
      com.truecaller.adapter_delegates.f localf = i;
      if (localf == null) {
        k.a("callRecordingsAdapter");
      }
      a locala = h;
      if (locala == null) {
        k.a("callRecordingsDelegate");
      }
      localf.notifyItemChanged(locala.a_(j));
    }
  }
  
  public final void a(boolean paramBoolean, String paramString)
  {
    Object localObject = getActivity();
    if (localObject != null) {
      ((android.support.v4.app.f)localObject).invalidateOptionsMenu();
    }
    localObject = (LinearLayout)a(R.id.emptyView);
    k.a(localObject, "emptyView");
    t.a((View)localObject, paramBoolean);
    if (paramString != null)
    {
      localObject = (TextView)a(R.id.emptyText);
      k.a(localObject, "emptyText");
      ((TextView)localObject).setText((CharSequence)paramString);
      return;
    }
  }
  
  public final void b(boolean paramBoolean)
  {
    Object localObject = b;
    if (localObject == null) {
      k.a("callRecordingsItemsPresenter");
    }
    a = paramBoolean;
    localObject = i;
    if (localObject == null) {
      k.a("callRecordingsAdapter");
    }
    ((com.truecaller.adapter_delegates.f)localObject).notifyDataSetChanged();
  }
  
  public final void c()
  {
    ActionMode localActionMode = n;
    if (localActionMode != null)
    {
      localActionMode.invalidate();
      return;
    }
  }
  
  public final void g()
  {
    Object localObject = i;
    if (localObject == null) {
      k.a("callRecordingsAdapter");
    }
    ((com.truecaller.adapter_delegates.f)localObject).notifyDataSetChanged();
    localObject = b;
    if (localObject == null) {
      k.a("callRecordingsItemsPresenter");
    }
    if (a) {
      c();
    }
  }
  
  public final void h()
  {
    Context localContext = getContext();
    if (localContext != null)
    {
      localContext.startActivity(new Intent(getContext(), CallRecordingSettingsActivity.class));
      return;
    }
  }
  
  public final void i()
  {
    android.support.v4.app.f localf = getActivity();
    if (localf != null)
    {
      if (e == null) {
        k.a("callRecordingOnBoardingNavigator");
      }
      k.a(localf, "it");
      aj.a((Context)localf, CallRecordingOnBoardingState.WHATS_NEW, CallRecordingOnBoardingLaunchContext.LIST);
      return;
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    k.b(paramContext, "context");
    super.onAttach(paramContext);
    if ((paramContext instanceof SingleActivity)) {
      ((SingleActivity)paramContext).setTitle(2131887579);
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getContext();
    if (paramBundle != null) {
      paramBundle = paramBundle.getApplicationContext();
    } else {
      paramBundle = null;
    }
    if (paramBundle != null)
    {
      ((bk)paramBundle).a().cy().a(this);
      paramBundle = b;
      if (paramBundle == null) {
        k.a("callRecordingsItemsPresenter");
      }
      h = ((a)new p((com.truecaller.adapter_delegates.b)paramBundle, 2131559005, (c.g.a.b)new c(this), (c.g.a.b)d.a));
      paramBundle = c;
      if (paramBundle == null) {
        k.a("promoPresenter");
      }
      l = ((a)new p((com.truecaller.adapter_delegates.b)paramBundle, 2131559090, (c.g.a.b)new e(this), (c.g.a.b)f.a));
      paramBundle = h;
      if (paramBundle == null) {
        k.a("callRecordingsDelegate");
      }
      a locala = l;
      if (locala == null) {
        k.a("promoDelegate");
      }
      i = new com.truecaller.adapter_delegates.f((a)paramBundle.a(locala, (s)o));
      return;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.GraphHolder");
  }
  
  public final void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    if (paramMenuInflater != null)
    {
      paramMenuInflater.inflate(2131623940, paramMenu);
      return;
    }
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558722, paramViewGroup, false);
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    Object localObject = a;
    if (localObject == null) {
      k.a("presenter");
    }
    ((bl)localObject).y_();
    localObject = q;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if ((paramMenuItem != null) && (paramMenuItem.getItemId() == 2131361845))
    {
      paramMenuItem = a;
      if (paramMenuItem == null) {
        k.a("presenter");
      }
      paramMenuItem.f();
    }
    return true;
  }
  
  public final void onPrepareOptionsMenu(Menu paramMenu)
  {
    super.onPrepareOptionsMenu(paramMenu);
    if (paramMenu != null)
    {
      paramMenu = paramMenu.findItem(2131361845);
      if (paramMenu != null)
      {
        bl localbl = a;
        if (localbl == null) {
          k.a("presenter");
        }
        paramMenu.setVisible(localbl.h());
        return;
      }
    }
  }
  
  public final void onResume()
  {
    super.onResume();
    bl localbl = a;
    if (localbl == null) {
      k.a("presenter");
    }
    localbl.g();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    paramBundle = paramView.findViewById(2131364092);
    k.a(paramBundle, "view.findViewById(R.id.recyclerView)");
    g = ((RecyclerView)paramBundle);
    ((Button)a(R.id.settingsButton)).setOnClickListener((View.OnClickListener)new g(this));
    paramBundle = g;
    if (paramBundle == null) {
      k.a("list");
    }
    paramBundle.setLayoutManager((RecyclerView.LayoutManager)new LinearLayoutManager(getContext()));
    paramBundle = g;
    if (paramBundle == null) {
      k.a("list");
    }
    paramBundle.addItemDecoration((RecyclerView.ItemDecoration)new q(paramView.getContext(), 2131559205, 0));
    paramView = a;
    if (paramView == null) {
      k.a("presenter");
    }
    paramView.a(this);
    paramView = g;
    if (paramView == null) {
      k.a("list");
    }
    paramBundle = i;
    if (paramBundle == null) {
      k.a("callRecordingsAdapter");
    }
    paramView.setAdapter((RecyclerView.Adapter)paramBundle);
    setHasOptionsMenu(true);
  }
  
  public final void toast(String paramString)
  {
    k.b(paramString, "message");
    Toast.makeText((Context)getActivity(), (CharSequence)paramString, 0).show();
  }
  
  public final bl z_()
  {
    bl localbl = a;
    if (localbl == null) {
      k.a("presenter");
    }
    return localbl;
  }
  
  public static final class a {}
  
  public static final class b
    implements ActionMode.Callback
  {
    final int a = 1;
    
    public final boolean onActionItemClicked(ActionMode paramActionMode, MenuItem paramMenuItem)
    {
      k.b(paramActionMode, "actionMode");
      k.b(paramMenuItem, "menuItem");
      return b.z_().a(a, paramMenuItem.getItemId());
    }
    
    public final boolean onCreateActionMode(ActionMode paramActionMode, Menu paramMenu)
    {
      k.b(paramActionMode, "actionMode");
      k.b(paramMenu, "menu");
      Integer localInteger = Integer.valueOf(b.z_().c(a));
      int i;
      if (((Number)localInteger).intValue() > 0) {
        i = 1;
      } else {
        i = 0;
      }
      if (i == 0) {
        localInteger = null;
      }
      if (localInteger != null)
      {
        i = ((Number)localInteger).intValue();
        paramActionMode.getMenuInflater().inflate(i, paramMenu);
      }
      paramActionMode.setTag(Integer.valueOf(a));
      bj.a(b, paramActionMode);
      b.z_().a(a);
      return true;
    }
    
    public final void onDestroyActionMode(ActionMode paramActionMode)
    {
      k.b(paramActionMode, "actionMode");
      b.z_().b(a);
    }
    
    public final boolean onPrepareActionMode(ActionMode paramActionMode, Menu paramMenu)
    {
      k.b(paramActionMode, "actionMode");
      k.b(paramMenu, "menu");
      Object localObject = b.z_().c();
      if (localObject != null) {
        paramActionMode.setTitle((CharSequence)localObject);
      }
      localObject = (Iterable)i.b(0, paramMenu.size());
      paramActionMode = (Collection)new ArrayList(m.a((Iterable)localObject, 10));
      localObject = ((Iterable)localObject).iterator();
      while (((Iterator)localObject).hasNext()) {
        paramActionMode.add(paramMenu.getItem(((ae)localObject).a()));
      }
      paramActionMode = ((Iterable)paramActionMode).iterator();
      while (paramActionMode.hasNext())
      {
        paramMenu = (MenuItem)paramActionMode.next();
        k.a(paramMenu, "it");
        paramMenu.setVisible(b.z_().b(a, paramMenu.getItemId()));
      }
      return true;
    }
  }
  
  static final class c
    extends l
    implements c.g.a.b<View, ab>
  {
    c(bj parambj)
    {
      super();
    }
  }
  
  static final class d
    extends l
    implements c.g.a.b<ab, ab>
  {
    public static final d a = new d();
    
    d()
    {
      super();
    }
  }
  
  static final class e
    extends l
    implements c.g.a.b<View, aw>
  {
    e(bj parambj)
    {
      super();
    }
  }
  
  static final class f
    extends l
    implements c.g.a.b<aw, aw>
  {
    public static final f a = new f();
    
    f()
    {
      super();
    }
  }
  
  static final class g
    implements View.OnClickListener
  {
    g(bj parambj) {}
    
    public final void onClick(View paramView)
    {
      a.z_().f();
    }
  }
  
  static final class h
    implements DialogInterface.OnClickListener
  {
    h(String paramString, by paramby, Object paramObject) {}
    
    public final void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
      paramby.a(paramObject);
    }
  }
  
  static final class i
    implements DialogInterface.OnClickListener
  {
    i(String paramString, by paramby, Object paramObject) {}
    
    public final void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
      paramby.b(paramObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.bj
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */