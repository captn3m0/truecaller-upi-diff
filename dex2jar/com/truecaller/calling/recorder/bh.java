package com.truecaller.calling.recorder;

import com.truecaller.common.f.c;
import com.truecaller.common.g.a;
import com.truecaller.utils.l;
import com.truecaller.utils.n;
import com.truecaller.wizard.utils.PermissionPoller;
import javax.inject.Provider;

public final class bh
  implements dagger.a.d<bg>
{
  private final Provider<a> a;
  private final Provider<c> b;
  private final Provider<CallRecordingManager> c;
  private final Provider<n> d;
  private final Provider<l> e;
  private final Provider<com.truecaller.utils.d> f;
  private final Provider<PermissionPoller> g;
  private final Provider<h> h;
  
  private bh(Provider<a> paramProvider, Provider<c> paramProvider1, Provider<CallRecordingManager> paramProvider2, Provider<n> paramProvider3, Provider<l> paramProvider4, Provider<com.truecaller.utils.d> paramProvider5, Provider<PermissionPoller> paramProvider6, Provider<h> paramProvider7)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
  }
  
  public static bh a(Provider<a> paramProvider, Provider<c> paramProvider1, Provider<CallRecordingManager> paramProvider2, Provider<n> paramProvider3, Provider<l> paramProvider4, Provider<com.truecaller.utils.d> paramProvider5, Provider<PermissionPoller> paramProvider6, Provider<h> paramProvider7)
  {
    return new bh(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.bh
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */