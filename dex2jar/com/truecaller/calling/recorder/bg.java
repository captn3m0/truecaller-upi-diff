package com.truecaller.calling.recorder;

import c.a.m;
import c.g.b.k;
import com.truecaller.bb;
import com.truecaller.common.f.c;
import com.truecaller.common.g.a;
import com.truecaller.utils.d;
import com.truecaller.utils.l;
import com.truecaller.wizard.utils.PermissionPoller;
import com.truecaller.wizard.utils.PermissionPoller.Permission;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;

public final class bg
  extends bb<CallRecordingSettingsMvp.b>
  implements CallRecordingSettingsMvp.a
{
  private final List<com.truecaller.ui.components.n> a;
  private final List<com.truecaller.ui.components.n> c;
  private final List<com.truecaller.ui.components.n> d;
  private final a e;
  private final c f;
  private final CallRecordingManager g;
  private final com.truecaller.utils.n h;
  private final l i;
  private final d j;
  private final PermissionPoller k;
  private final h l;
  
  @Inject
  public bg(a parama, c paramc, CallRecordingManager paramCallRecordingManager, com.truecaller.utils.n paramn, l paraml, d paramd, PermissionPoller paramPermissionPoller, h paramh)
  {
    e = parama;
    f = paramc;
    g = paramCallRecordingManager;
    h = paramn;
    i = paraml;
    j = paramd;
    k = paramPermissionPoller;
    l = paramh;
    a = m.b(new com.truecaller.ui.components.n[] { new com.truecaller.ui.components.n(0, 2131887575, 2131887576, CallRecordingSettingsMvp.RecordingModes.AUTO), new com.truecaller.ui.components.n(0, 2131887577, 2131887578, CallRecordingSettingsMvp.RecordingModes.MANUAL) });
    c = m.b(new com.truecaller.ui.components.n[] { new com.truecaller.ui.components.n(0, 2131887556, -1, CallRecordingSettingsMvp.CallsFilter.ALL_CALLS), new com.truecaller.ui.components.n(0, 2131887558, -1, CallRecordingSettingsMvp.CallsFilter.UNKNOWN_NUMBERS), new com.truecaller.ui.components.n(0, 2131887557, -1, CallRecordingSettingsMvp.CallsFilter.SELECTED_CONTACTS) });
    d = m.b(new com.truecaller.ui.components.n[] { new com.truecaller.ui.components.n(0, 2131887553, 2131887554, CallRecordingSettingsMvp.Configuration.DEFAULT), new com.truecaller.ui.components.n(0, 2131887555, -1, CallRecordingSettingsMvp.Configuration.SDK_MEDIA_RECORDER) });
  }
  
  private final void c(boolean paramBoolean)
  {
    e.b("callRecordingEnbaled", paramBoolean);
    CallRecordingSettingsMvp.b localb = (CallRecordingSettingsMvp.b)b;
    if (localb != null)
    {
      localb.c(paramBoolean);
      return;
    }
  }
  
  public final void a()
  {
    k.a();
    Object localObject1 = (CallRecordingSettingsMvp.b)b;
    if (localObject1 != null) {
      ((CallRecordingSettingsMvp.b)localObject1).b(f.d() ^ true);
    }
    boolean bool1 = f.d();
    boolean bool2 = false;
    int m;
    if ((!bool1) && (g.n() == FreeTrialStatus.EXPIRED)) {
      m = 1;
    } else {
      m = 0;
    }
    bool1 = bool2;
    if (e.b("callRecordingEnbaled"))
    {
      bool1 = bool2;
      if (m == 0) {
        bool1 = true;
      }
    }
    localObject1 = (CallRecordingSettingsMvp.b)b;
    if (localObject1 != null) {
      ((CallRecordingSettingsMvp.b)localObject1).a(bool1);
    }
    localObject1 = (CallRecordingSettingsMvp.b)b;
    if (localObject1 != null) {
      ((CallRecordingSettingsMvp.b)localObject1).c(bool1);
    }
    localObject1 = (CallRecordingSettingsMvp.b)b;
    if (localObject1 != null)
    {
      localObject2 = e.b("callRecordingStoragePath", bf.a());
      k.a(localObject2, "coreSettings.getString(\n…oragePath()\n            )");
      ((CallRecordingSettingsMvp.b)localObject1).a((String)localObject2);
    }
    Object localObject2 = ((Iterable)a).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject1 = ((Iterator)localObject2).next();
      if (k.a(((com.truecaller.ui.components.n)localObject1).q().toString(), e.b("callRecordingMode", CallRecordingSettingsMvp.RecordingModes.MANUAL.name()))) {
        break label264;
      }
    }
    localObject1 = null;
    label264:
    localObject1 = (com.truecaller.ui.components.n)localObject1;
    if (localObject1 != null)
    {
      localObject2 = (CallRecordingSettingsMvp.b)b;
      if (localObject2 != null) {
        ((CallRecordingSettingsMvp.b)localObject2).a((com.truecaller.ui.components.n)localObject1);
      }
    }
    localObject2 = ((Iterable)c).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject1 = ((Iterator)localObject2).next();
      if (k.a(((com.truecaller.ui.components.n)localObject1).q().toString(), e.b("callRecordingFilter", CallRecordingSettingsMvp.CallsFilter.ALL_CALLS.name()))) {
        break label372;
      }
    }
    localObject1 = null;
    label372:
    localObject1 = (com.truecaller.ui.components.n)localObject1;
    if (localObject1 != null)
    {
      localObject2 = (CallRecordingSettingsMvp.b)b;
      if (localObject2 != null) {
        ((CallRecordingSettingsMvp.b)localObject2).b((com.truecaller.ui.components.n)localObject1);
      }
    }
    localObject2 = ((Iterable)d).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject1 = ((Iterator)localObject2).next();
      com.truecaller.ui.components.n localn = (com.truecaller.ui.components.n)localObject1;
      if (l.d() == CallRecordingSettingsMvp.Configuration.SDK_MEDIA_RECORDER) {
        bool2 = k.a(localn.q().toString(), CallRecordingSettingsMvp.Configuration.SDK_MEDIA_RECORDER.name());
      } else {
        bool2 = k.a(localn.q().toString(), e.b("callRecordingConfiguration", CallRecordingSettingsMvp.Configuration.DEFAULT.name()));
      }
      if (bool2) {
        break label522;
      }
    }
    localObject1 = null;
    label522:
    localObject1 = (com.truecaller.ui.components.n)localObject1;
    if (localObject1 != null)
    {
      localObject2 = (CallRecordingSettingsMvp.b)b;
      if (localObject2 != null) {
        ((CallRecordingSettingsMvp.b)localObject2).c((com.truecaller.ui.components.n)localObject1);
      }
    }
    if (m != 0)
    {
      localObject1 = (CallRecordingSettingsMvp.b)b;
      if (localObject1 != null) {
        ((CallRecordingSettingsMvp.b)localObject1).b();
      }
    }
    if (bool1)
    {
      localObject1 = (CallRecordingSettingsMvp.b)b;
      if (localObject1 != null)
      {
        bool1 = i.a();
        bool2 = j.f();
        boolean bool3 = i.c();
        ((CallRecordingSettingsMvp.b)localObject1).a(bool1 ^ true, bool2 ^ true, true ^ i.a(new String[] { "android.permission.RECORD_AUDIO" }), bool3 ^ true);
        return;
      }
    }
  }
  
  public final void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    k.b(paramArrayOfString, "permissions");
    k.b(paramArrayOfInt, "grantResults");
    g.a(paramInt, paramArrayOfString, paramArrayOfInt);
  }
  
  public final void a(com.truecaller.ui.components.n paramn)
  {
    k.b(paramn, "modeSelected");
    e.a("callRecordingMode", paramn.q().toString());
    if (!e.b("callRecordingEnbaled")) {
      a(true);
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    CallRecordingSettingsMvp.b localb;
    if ((paramBoolean) && (g.g()))
    {
      localb = (CallRecordingSettingsMvp.b)b;
      if (localb != null)
      {
        localb.a(CallRecordingOnBoardingLaunchContext.PAY_WALL);
        return;
      }
      return;
    }
    if ((paramBoolean) && (!e.b("callRecordingPostEnableShown")))
    {
      localb = (CallRecordingSettingsMvp.b)b;
      if (localb != null) {
        localb.a(CallRecordingOnBoardingLaunchContext.SETTINGS);
      }
      return;
    }
    if (paramBoolean)
    {
      localb = (CallRecordingSettingsMvp.b)b;
      if (localb != null)
      {
        CharSequence localCharSequence = h.b(2131887610, new Object[0]);
        k.a(localCharSequence, "resourceProvider.getRich…recording_terms_subtitle)");
        localb.a(localCharSequence);
      }
      return;
    }
    c(false);
  }
  
  public final void b()
  {
    CallRecordingSettingsMvp.b localb = (CallRecordingSettingsMvp.b)b;
    if (localb != null)
    {
      localb.c();
      return;
    }
  }
  
  public final void b(com.truecaller.ui.components.n paramn)
  {
    k.b(paramn, "filterSelected");
    e.a("callRecordingFilter", paramn.q().toString());
  }
  
  public final void b(boolean paramBoolean)
  {
    c(paramBoolean);
  }
  
  public final void c()
  {
    k.a(PermissionPoller.Permission.DRAW_OVERLAY);
    CallRecordingSettingsMvp.b localb = (CallRecordingSettingsMvp.b)b;
    if (localb != null)
    {
      localb.d();
      return;
    }
  }
  
  public final void c(com.truecaller.ui.components.n paramn)
  {
    k.b(paramn, "configSelected");
    e.a("callRecordingConfiguration", paramn.q().toString());
  }
  
  public final void e()
  {
    k.a(PermissionPoller.Permission.BATTERY_OPTIMISATIONS);
    CallRecordingSettingsMvp.b localb = (CallRecordingSettingsMvp.b)b;
    if (localb != null)
    {
      localb.e();
      return;
    }
  }
  
  public final void f()
  {
    CallRecordingSettingsMvp.b localb = (CallRecordingSettingsMvp.b)b;
    if (localb != null)
    {
      localb.b("android.permission.WRITE_EXTERNAL_STORAGE");
      return;
    }
  }
  
  public final void g()
  {
    CallRecordingSettingsMvp.b localb = (CallRecordingSettingsMvp.b)b;
    if (localb != null)
    {
      localb.b("android.permission.RECORD_AUDIO");
      return;
    }
  }
  
  public final void y_()
  {
    super.y_();
    k.a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.bg
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */