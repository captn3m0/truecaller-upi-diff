package com.truecaller.calling.recorder;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import c.g.a.m;
import c.g.b.k;
import c.u;
import c.x;
import com.truecaller.R.id;
import com.truecaller.R.styleable;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import javax.inject.Inject;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;

public final class CallRecordingFloatingButton
  extends FrameLayout
  implements bw
{
  @Inject
  public CallRecordingFloatingButtonPresenter a;
  private int b = 2131362382;
  private HashMap c;
  
  public CallRecordingFloatingButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private CallRecordingFloatingButton(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    Context localContext = paramContext.getApplicationContext();
    if (localContext != null)
    {
      ((bk)localContext).a().cy().a(this);
      if (paramAttributeSet != null)
      {
        paramAttributeSet = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.CallRecordingFloatingButton, 0, 0);
        b = paramAttributeSet.getResourceId(0, 2131362382);
        paramAttributeSet.recycle();
      }
      LayoutInflater.from(paramContext).inflate(2131559200, (ViewGroup)this);
      paramContext = (ImageView)a(R.id.callRecordingTCLogo);
      k.a(paramContext, "callRecordingTCLogo");
      t.b((View)paramContext);
      return;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.GraphHolder");
  }
  
  private View a(int paramInt)
  {
    if (c == null) {
      c = new HashMap();
    }
    View localView2 = (View)c.get(Integer.valueOf(paramInt));
    View localView1 = localView2;
    if (localView2 == null)
    {
      localView1 = findViewById(paramInt);
      c.put(Integer.valueOf(paramInt), localView1);
    }
    return localView1;
  }
  
  private final void a(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean1, boolean paramBoolean2)
  {
    ((TextView)a(R.id.callRecordingLabel)).setTextColor(com.truecaller.utils.ui.b.a(getContext(), paramInt1));
    ((LinearLayout)a(R.id.callRecordingButton)).setBackgroundResource(paramInt2);
    ((ImageView)a(R.id.callRecordingIconLeft)).setImageResource(paramInt3);
    ImageView localImageView = (ImageView)a(R.id.callRecordingIconLeft);
    k.a(localImageView, "callRecordingIconLeft");
    t.a((View)localImageView, paramBoolean1);
    localImageView = (ImageView)a(R.id.callRecordingIconRight);
    k.a(localImageView, "callRecordingIconRight");
    t.a((View)localImageView, paramBoolean2);
  }
  
  public final void a()
  {
    a(2130968719, 2131230994, 2131234274, true, false);
    Object localObject = x.a;
    localObject = (TextView)a(R.id.callRecordingLabel);
    k.a(localObject, "callRecordingLabel");
    ((TextView)localObject).setGravity(8388611);
  }
  
  public final void a(boolean paramBoolean)
  {
    ImageView localImageView = (ImageView)a(R.id.callRecordingTCLogo);
    k.a(localImageView, "callRecordingTCLogo");
    t.a((View)localImageView, paramBoolean);
  }
  
  public final void b()
  {
    a(2130968718, 2131230995, 2131234274, false, true);
    Object localObject = x.a;
    localObject = (TextView)a(R.id.callRecordingLabel);
    k.a(localObject, "callRecordingLabel");
    ((TextView)localObject).setGravity(8388613);
  }
  
  public final void c()
  {
    a(2130968719, 2131230994, 2131234389, true, false);
    Object localObject = x.a;
    localObject = (TextView)a(R.id.callRecordingLabel);
    k.a(localObject, "callRecordingLabel");
    ((TextView)localObject).setGravity(8388611);
  }
  
  public final void d()
  {
    t.b(this);
  }
  
  public final CallRecordingFloatingButtonPresenter getPresenter()
  {
    CallRecordingFloatingButtonPresenter localCallRecordingFloatingButtonPresenter = a;
    if (localCallRecordingFloatingButtonPresenter == null) {
      k.a("presenter");
    }
    return localCallRecordingFloatingButtonPresenter;
  }
  
  protected final void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    CallRecordingFloatingButtonPresenter localCallRecordingFloatingButtonPresenter = a;
    if (localCallRecordingFloatingButtonPresenter == null) {
      k.a("presenter");
    }
    localCallRecordingFloatingButtonPresenter.a((bw)this);
    localCallRecordingFloatingButtonPresenter = a;
    if (localCallRecordingFloatingButtonPresenter == null) {
      k.a("presenter");
    }
    int i = b;
    d = i;
    if (i.f())
    {
      localCallRecordingFloatingButtonPresenter.b();
      return;
    }
    if (i == 2131362381)
    {
      c = CallRecordingFloatingButtonPresenter.CallRecordingButtonMode.ENDED;
      e.b((ag)bg.a, k, (m)new CallRecordingFloatingButtonPresenter.g(localCallRecordingFloatingButtonPresenter, null), 2);
    }
    localCallRecordingFloatingButtonPresenter.c();
  }
  
  protected final void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    CallRecordingFloatingButtonPresenter localCallRecordingFloatingButtonPresenter = a;
    if (localCallRecordingFloatingButtonPresenter == null) {
      k.a("presenter");
    }
    localCallRecordingFloatingButtonPresenter.y_();
  }
  
  public final void setCallRecording(CallRecording paramCallRecording)
  {
    k.b(paramCallRecording, "callRecording");
    CallRecordingFloatingButtonPresenter localCallRecordingFloatingButtonPresenter = a;
    if (localCallRecordingFloatingButtonPresenter == null) {
      k.a("presenter");
    }
    k.b(paramCallRecording, "callRecording");
    if (d == 2131362381) {
      e.b((ag)bg.a, k, (m)new CallRecordingFloatingButtonPresenter.f(localCallRecordingFloatingButtonPresenter, paramCallRecording, null), 2);
    }
  }
  
  public final void setLabel(String paramString)
  {
    k.b(paramString, "label");
    TextView localTextView = (TextView)a(R.id.callRecordingLabel);
    k.a(localTextView, "callRecordingLabel");
    localTextView.setText((CharSequence)paramString);
  }
  
  public final void setPhoneNumber(String paramString)
  {
    CallRecordingFloatingButtonPresenter localCallRecordingFloatingButtonPresenter = a;
    if (localCallRecordingFloatingButtonPresenter == null) {
      k.a("presenter");
    }
    h = paramString;
  }
  
  public final void setPresenter(CallRecordingFloatingButtonPresenter paramCallRecordingFloatingButtonPresenter)
  {
    k.b(paramCallRecordingFloatingButtonPresenter, "<set-?>");
    a = paramCallRecordingFloatingButtonPresenter;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingFloatingButton
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */