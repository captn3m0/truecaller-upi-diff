package com.truecaller.calling.recorder;

import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.bb;
import com.truecaller.common.f.c;
import com.truecaller.utils.l;
import com.truecaller.utils.n;
import javax.inject.Inject;

public final class al
  extends bb<an>
{
  CallRecordingOnBoardingState a;
  CallRecordingOnBoardingLaunchContext c;
  final String[] d;
  final com.truecaller.common.g.a e;
  final CallRecordingManager f;
  private final n g;
  private final com.truecaller.utils.a h;
  private final c i;
  private final b j;
  private final l k;
  
  @Inject
  public al(com.truecaller.common.g.a parama, n paramn, com.truecaller.utils.a parama1, c paramc, b paramb, l paraml, CallRecordingManager paramCallRecordingManager)
  {
    e = parama;
    g = paramn;
    h = parama1;
    i = paramc;
    j = paramb;
    k = paraml;
    f = paramCallRecordingManager;
    a = CallRecordingOnBoardingState.WHATS_NEW;
    c = CallRecordingOnBoardingLaunchContext.UNKNOWN;
    d = new String[] { "android.permission.RECORD_AUDIO", "android.permission.CALL_PHONE", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_PHONE_STATE" };
  }
  
  final void a()
  {
    Object localObject1 = a;
    Object localObject2;
    switch (am.a[localObject1.ordinal()])
    {
    default: 
      return;
    case 7: 
      localObject1 = (an)b;
      if (localObject1 != null) {
        ((an)localObject1).e(c);
      }
      e();
      return;
    case 6: 
      localObject1 = (an)b;
      if (localObject1 != null)
      {
        localObject2 = g.b(2131887582, new Object[0]);
        k.a(localObject2, "resourceProvider.getRich…ing_permissions_subtitle)");
        ((an)localObject1).b((CharSequence)localObject2);
      }
      return;
    case 5: 
      localObject1 = (an)b;
      if (localObject1 != null)
      {
        localObject2 = g.b(2131887610, new Object[0]);
        k.a(localObject2, "resourceProvider.getRich…recording_terms_subtitle)");
        ((an)localObject1).a((CharSequence)localObject2);
      }
      return;
    case 4: 
      localObject1 = (an)b;
      if (localObject1 != null)
      {
        localObject2 = g;
        int m;
        if (i.d()) {
          m = 2131887625;
        } else {
          m = 2131887621;
        }
        localObject2 = ((n)localObject2).b(m, new Object[0]);
        k.a(localObject2, "resourceProvider.getRich…      }\n                )");
        Object localObject3 = g;
        if (i.d()) {
          m = 2131887624;
        } else {
          m = 2131887620;
        }
        localObject3 = ((n)localObject3).a(m, new Object[0]);
        k.a(localObject3, "resourceProvider.getStri…      }\n                )");
        ((an)localObject1).a((CharSequence)localObject2, (CharSequence)localObject3);
      }
      return;
    case 3: 
      if (i.d())
      {
        localObject1 = (an)b;
        if (localObject1 != null) {
          ((an)localObject1).d(c);
        }
        return;
      }
      localObject1 = (an)b;
      if (localObject1 != null) {
        ((an)localObject1).a(c);
      }
      return;
    case 2: 
      localObject1 = (an)b;
      if (localObject1 != null) {
        ((an)localObject1).c(c);
      }
      return;
    }
    localObject1 = (an)b;
    if (localObject1 != null) {
      ((an)localObject1).b(c);
    }
  }
  
  final void b()
  {
    CallRecordingOnBoardingState localCallRecordingOnBoardingState;
    if (!e.b("callRecordingTermsAccepted"))
    {
      localCallRecordingOnBoardingState = CallRecordingOnBoardingState.TERMS;
    }
    else if (!c())
    {
      localCallRecordingOnBoardingState = CallRecordingOnBoardingState.PERMISSIONS;
    }
    else
    {
      e.b("callRecordingPostEnableShown");
      localCallRecordingOnBoardingState = CallRecordingOnBoardingState.POST_ENABLE;
    }
    a = localCallRecordingOnBoardingState;
    a();
  }
  
  final boolean c()
  {
    String[] arrayOfString = d;
    int n = arrayOfString.length;
    int m = 0;
    while (m < n)
    {
      String str = arrayOfString[m];
      if (!k.a(new String[] { str })) {
        return false;
      }
      m += 1;
    }
    return true;
  }
  
  final void e()
  {
    e.b("callRecordingPostEnableShown", true);
    e.b("key_call_recording_trial_start_timestamp", h.a());
    e.b("callRecordingEnbaled", true);
    Object localObject2 = new e.a("CallRecordingEnabled");
    ((e.a)localObject2).a("Source", c.name());
    if (i.d()) {
      localObject1 = "Premium";
    } else {
      localObject1 = "Free";
    }
    ((e.a)localObject2).a("PremiumStatus", (String)localObject1);
    Object localObject1 = j;
    localObject2 = ((e.a)localObject2).a();
    k.a(localObject2, "event.build()");
    ((b)localObject1).b((e)localObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.al
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */