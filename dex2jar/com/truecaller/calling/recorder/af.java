package com.truecaller.calling.recorder;

import android.content.Context;
import android.telephony.TelephonyManager;
import com.truecaller.analytics.b;
import com.truecaller.callerid.e;
import com.truecaller.common.f.c;
import com.truecaller.util.aq;
import com.truecaller.utils.l;
import com.truecaller.utils.n;
import dagger.a.d;
import javax.inject.Provider;

public final class af
  implements d<ae>
{
  private final Provider<Context> a;
  private final Provider<com.truecaller.common.g.a> b;
  private final Provider<com.truecaller.androidactors.f<e>> c;
  private final Provider<ax> d;
  private final Provider<com.truecaller.utils.a> e;
  private final Provider<aq> f;
  private final Provider<h> g;
  private final Provider<u> h;
  private final Provider<c> i;
  private final Provider<c.d.f> j;
  private final Provider<b> k;
  private final Provider<l> l;
  private final Provider<n> m;
  private final Provider<TelephonyManager> n;
  private final Provider<aj> o;
  private final Provider<cd> p;
  private final Provider<Long> q;
  
  private af(Provider<Context> paramProvider, Provider<com.truecaller.common.g.a> paramProvider1, Provider<com.truecaller.androidactors.f<e>> paramProvider2, Provider<ax> paramProvider3, Provider<com.truecaller.utils.a> paramProvider4, Provider<aq> paramProvider5, Provider<h> paramProvider6, Provider<u> paramProvider7, Provider<c> paramProvider8, Provider<c.d.f> paramProvider9, Provider<b> paramProvider10, Provider<l> paramProvider11, Provider<n> paramProvider12, Provider<TelephonyManager> paramProvider13, Provider<aj> paramProvider14, Provider<cd> paramProvider15, Provider<Long> paramProvider16)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
    k = paramProvider10;
    l = paramProvider11;
    m = paramProvider12;
    n = paramProvider13;
    o = paramProvider14;
    p = paramProvider15;
    q = paramProvider16;
  }
  
  public static af a(Provider<Context> paramProvider, Provider<com.truecaller.common.g.a> paramProvider1, Provider<com.truecaller.androidactors.f<e>> paramProvider2, Provider<ax> paramProvider3, Provider<com.truecaller.utils.a> paramProvider4, Provider<aq> paramProvider5, Provider<h> paramProvider6, Provider<u> paramProvider7, Provider<c> paramProvider8, Provider<c.d.f> paramProvider9, Provider<b> paramProvider10, Provider<l> paramProvider11, Provider<n> paramProvider12, Provider<TelephonyManager> paramProvider13, Provider<aj> paramProvider14, Provider<cd> paramProvider15, Provider<Long> paramProvider16)
  {
    return new af(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10, paramProvider11, paramProvider12, paramProvider13, paramProvider14, paramProvider15, paramProvider16);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.af
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */