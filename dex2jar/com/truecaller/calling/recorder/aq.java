package com.truecaller.calling.recorder;

import com.truecaller.common.f.c;
import com.truecaller.utils.n;
import dagger.a.d;
import javax.inject.Provider;

public final class aq
  implements d<ar>
{
  private final ap a;
  private final Provider<c> b;
  private final Provider<bv> c;
  private final Provider<n> d;
  private final Provider<CallRecordingManager> e;
  
  private aq(ap paramap, Provider<c> paramProvider, Provider<bv> paramProvider1, Provider<n> paramProvider2, Provider<CallRecordingManager> paramProvider3)
  {
    a = paramap;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
  }
  
  public static aq a(ap paramap, Provider<c> paramProvider, Provider<bv> paramProvider1, Provider<n> paramProvider2, Provider<CallRecordingManager> paramProvider3)
  {
    return new aq(paramap, paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.aq
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */