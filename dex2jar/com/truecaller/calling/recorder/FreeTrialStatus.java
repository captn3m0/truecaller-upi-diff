package com.truecaller.calling.recorder;

public enum FreeTrialStatus
{
  private final String freeTrialStatus;
  
  static
  {
    FreeTrialStatus localFreeTrialStatus1 = new FreeTrialStatus("NOT_STARTED", 0, "NotStarted");
    NOT_STARTED = localFreeTrialStatus1;
    FreeTrialStatus localFreeTrialStatus2 = new FreeTrialStatus("ACTIVE", 1, "NotStarted");
    ACTIVE = localFreeTrialStatus2;
    FreeTrialStatus localFreeTrialStatus3 = new FreeTrialStatus("EXPIRED", 2, "NotStarted");
    EXPIRED = localFreeTrialStatus3;
    $VALUES = new FreeTrialStatus[] { localFreeTrialStatus1, localFreeTrialStatus2, localFreeTrialStatus3 };
  }
  
  private FreeTrialStatus(String paramString)
  {
    freeTrialStatus = paramString;
  }
  
  public final String getFreeTrialStatus()
  {
    return freeTrialStatus;
  }
  
  public final boolean isActive()
  {
    return (FreeTrialStatus)this == ACTIVE;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.FreeTrialStatus
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */