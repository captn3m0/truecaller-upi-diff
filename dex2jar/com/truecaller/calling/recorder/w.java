package com.truecaller.calling.recorder;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class w
  implements d<v>
{
  private final Provider<Context> a;
  
  private w(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static w a(Provider<Context> paramProvider)
  {
    return new w(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.w
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */