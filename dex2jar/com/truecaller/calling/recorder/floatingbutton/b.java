package com.truecaller.calling.recorder.floatingbutton;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Vibrator;

public final class b
  extends a
{
  private boolean a = false;
  private boolean b = false;
  private boolean c = false;
  
  public b(Context paramContext)
  {
    super(paramContext);
  }
  
  private void a(int paramInt)
  {
    if (!isInEditMode())
    {
      AnimatorSet localAnimatorSet = (AnimatorSet)AnimatorInflater.loadAnimator(getContext(), paramInt);
      localAnimatorSet.setTarget(getChildAt(0));
      localAnimatorSet.start();
    }
  }
  
  final void a()
  {
    if (!a)
    {
      a = true;
      a(2130837509);
    }
  }
  
  @SuppressLint({"MissingPermission"})
  final void b()
  {
    if (!c)
    {
      ((Vibrator)getContext().getSystemService("vibrator")).vibrate(70L);
      c = true;
    }
  }
  
  final void c()
  {
    if (a)
    {
      a = false;
      a(2130837507);
    }
    c = false;
  }
  
  protected final void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    b = true;
  }
  
  protected final void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    b = false;
  }
  
  public final void setVisibility(int paramInt)
  {
    if ((b) && (paramInt != getVisibility())) {
      if (paramInt == 0) {
        a(2130837508);
      } else {
        a(2130837506);
      }
    }
    super.setVisibility(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */