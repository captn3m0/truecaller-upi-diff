package com.truecaller.calling.recorder.floatingbutton;

import android.telephony.TelephonyManager;
import c.g.b.k;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.w;
import com.truecaller.callhistory.a;
import com.truecaller.calling.recorder.CallRecordingFloatingButton;
import com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter;
import com.truecaller.calling.recorder.ba;
import com.truecaller.calling.recorder.ba.a;
import java.net.URLDecoder;
import java.util.TimerTask;

public final class g$c$d
  extends TimerTask
{
  public g$c$d(g.c paramc) {}
  
  public final void run()
  {
    if (a.a.c.getCallState() == 0)
    {
      Object localObject1 = g.c(a.a);
      if (localObject1 != null)
      {
        Object localObject2 = a;
        if (localObject2 == null) {
          k.a("presenter");
        }
        localObject1 = a;
        if (localObject1 != null)
        {
          localObject2 = m;
          k.b(localObject1, "recordingFileAbsolutePath");
          String str = URLDecoder.decode((String)localObject1, "UTF-8");
          k.a(str, "URLDecoder.decode(record…ileAbsolutePath, \"UTF-8\")");
          str = (String)c.a.m.d(c.n.m.c((CharSequence)c.a.m.f(c.n.m.c((CharSequence)str, new String[] { "-" }, false, 6)), new String[] { "." }, false, 6));
          a.c(str).a((ac)new ba.a((ba)localObject2, (String)localObject1));
        }
      }
      a.a.a();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.g.c.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */