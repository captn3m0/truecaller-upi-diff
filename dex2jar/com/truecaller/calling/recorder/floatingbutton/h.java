package com.truecaller.calling.recorder.floatingbutton;

import android.content.Context;
import android.content.res.Resources;
import android.telephony.TelephonyManager;
import c.d.f;
import com.truecaller.calling.recorder.CallRecordingManager;
import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d<g>
{
  private final Provider<Context> a;
  private final Provider<Resources> b;
  private final Provider<CallRecordingManager> c;
  private final Provider<f> d;
  private final Provider<TelephonyManager> e;
  private final Provider<Long> f;
  
  private h(Provider<Context> paramProvider, Provider<Resources> paramProvider1, Provider<CallRecordingManager> paramProvider2, Provider<f> paramProvider3, Provider<TelephonyManager> paramProvider4, Provider<Long> paramProvider5)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
  }
  
  public static h a(Provider<Context> paramProvider, Provider<Resources> paramProvider1, Provider<CallRecordingManager> paramProvider2, Provider<f> paramProvider3, Provider<TelephonyManager> paramProvider4, Provider<Long> paramProvider5)
  {
    return new h(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */