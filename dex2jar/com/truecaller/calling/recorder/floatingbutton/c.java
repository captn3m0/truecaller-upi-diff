package com.truecaller.calling.recorder.floatingbutton;

import android.view.View;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;

public final class c
{
  static c a;
  b b;
  WindowManager c;
  BubblesService d;
  b e;
  
  private View a()
  {
    return b.getChildAt(0);
  }
  
  private void c(BubbleLayout paramBubbleLayout)
  {
    View localView = a();
    int i = localView.getLeft();
    int j = localView.getMeasuredWidth() / 2;
    int k = localView.getTop();
    int m = localView.getMeasuredHeight() / 2;
    int n = paramBubbleLayout.getMeasuredWidth() / 2;
    int i1 = paramBubbleLayout.getMeasuredHeight() / 2;
    getViewParamsx = (i + j - n);
    getViewParamsy = (k + m - i1);
    c.updateViewLayout(paramBubbleLayout, paramBubbleLayout.getViewParams());
  }
  
  public final void a(BubbleLayout paramBubbleLayout)
  {
    if (b != null)
    {
      b localb = e;
      if ((localb == null) || (localb.a()))
      {
        b.setVisibility(0);
        if (b(paramBubbleLayout))
        {
          b.a();
          b.b();
          c(paramBubbleLayout);
          return;
        }
        b.c();
      }
    }
  }
  
  final boolean b(BubbleLayout paramBubbleLayout)
  {
    if (b.getVisibility() == 0)
    {
      View localView = a();
      int i = localView.getMeasuredHeight();
      int j = localView.getTop();
      if (getViewParamsy >= j - i) {
        return true;
      }
    }
    return false;
  }
  
  public static final class a
  {
    c a;
    
    public a(BubblesService paramBubblesService)
    {
      if (c.a == null) {
        c.a = new c();
      }
      a = c.a;
      a.d = paramBubblesService;
    }
  }
  
  public static abstract interface b
  {
    public abstract boolean a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */