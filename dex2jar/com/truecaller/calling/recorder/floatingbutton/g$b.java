package com.truecaller.calling.recorder.floatingbutton;

import c.d.a.a;
import c.d.b.a.f;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.calling.recorder.CallRecordingFloatingButton;
import kotlinx.coroutines.ag;

@f(b="CallRecordingFloatingButtonManager.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.recorder.floatingbutton.CallRecordingFloatingButtonManagerImpl$showBrandingView$1")
final class g$b
  extends c.d.b.a.k
  implements m<ag, c<? super x>, Object>
{
  int a;
  private ag d;
  
  g$b(g paramg, boolean paramBoolean, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new b(b, c, paramc);
    d = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    if (a == 0)
    {
      if (!(paramObject instanceof o.b))
      {
        paramObject = g.b(b);
        if (paramObject != null)
        {
          paramObject = (CallRecordingFloatingButton)((BubbleLayout)paramObject).findViewById(2131362380);
          if (paramObject != null) {
            ((CallRecordingFloatingButton)paramObject).a(c);
          }
        }
        return x.a;
      }
      throw a;
    }
    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((b)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.g.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */