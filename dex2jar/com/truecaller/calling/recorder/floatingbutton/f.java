package com.truecaller.calling.recorder.floatingbutton;

import com.truecaller.androidactors.u;
import com.truecaller.androidactors.v;

public final class f
  implements e
{
  private final v a;
  
  public f(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return e.class.equals(paramClass);
  }
  
  public final void a()
  {
    a.a(new a(new com.truecaller.androidactors.e(), (byte)0));
  }
  
  public final void a(String paramString, i parami)
  {
    a.a(new c(new com.truecaller.androidactors.e(), paramString, parami, (byte)0));
  }
  
  public final void a(boolean paramBoolean)
  {
    a.a(new b(new com.truecaller.androidactors.e(), paramBoolean, (byte)0));
  }
  
  static final class a
    extends u<e, Void>
  {
    private a(com.truecaller.androidactors.e parame)
    {
      super();
    }
    
    public final String toString()
    {
      return ".dismissCallRecordingButton()";
    }
  }
  
  static final class b
    extends u<e, Void>
  {
    private final boolean b;
    
    private b(com.truecaller.androidactors.e parame, boolean paramBoolean)
    {
      super();
      b = paramBoolean;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".showBrandingView(");
      localStringBuilder.append(a(Boolean.valueOf(b), 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class c
    extends u<e, Void>
  {
    private final String b;
    private final i c;
    
    private c(com.truecaller.androidactors.e parame, String paramString, i parami)
    {
      super();
      b = paramString;
      c = parami;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".showCallRecordingButton(");
      localStringBuilder.append(a(b, 2));
      localStringBuilder.append(",");
      localStringBuilder.append(a(c, 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */