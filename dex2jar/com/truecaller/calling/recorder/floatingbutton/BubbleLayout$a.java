package com.truecaller.calling.recorder.floatingbutton;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.WindowManager.LayoutParams;

final class BubbleLayout$a
  implements Runnable
{
  private Handler b = new Handler(Looper.getMainLooper());
  private float c;
  private float d;
  private long e;
  
  private BubbleLayout$a(BubbleLayout paramBubbleLayout) {}
  
  public final void run()
  {
    if ((a.getRootView() != null) && (a.getRootView().getParent() != null))
    {
      float f1 = Math.min(1.0F, (float)(System.currentTimeMillis() - e) / 400.0F);
      float f2 = c;
      float f3 = a.getViewParams().x;
      float f4 = d;
      float f5 = a.getViewParams().y;
      BubbleLayout.a(a, (f2 - f3) * f1, (f4 - f5) * f1);
      if (f1 < 1.0F) {
        b.post(this);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.BubbleLayout.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */