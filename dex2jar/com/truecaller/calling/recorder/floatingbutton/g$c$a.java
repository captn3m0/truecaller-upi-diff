package com.truecaller.calling.recorder.floatingbutton;

import android.os.Handler;
import android.os.Looper;
import c.g.b.k;
import com.truecaller.calling.recorder.CallRecordingFloatingButton;
import com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter;
import com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter.CallRecordingButtonMode;
import com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter.c;
import com.truecaller.calling.recorder.CallRecordingManager;
import java.util.Timer;
import java.util.TimerTask;
import org.a.a.b;

final class g$c$a
  implements BubbleLayout.b
{
  g$c$a(g.c paramc) {}
  
  public final void a()
  {
    Object localObject1 = new StringBuilder("showCallRecordingButton:: Debounce? ");
    ((StringBuilder)localObject1).append(g.d(a.a));
    ((StringBuilder)localObject1).toString();
    if (!g.d(a.a))
    {
      localObject1 = g.c(a.a);
      if (localObject1 != null)
      {
        CallRecordingFloatingButtonPresenter localCallRecordingFloatingButtonPresenter = a;
        if (localCallRecordingFloatingButtonPresenter == null) {
          k.a("presenter");
        }
        Object localObject2 = c;
        switch (com.truecaller.calling.recorder.k.a[localObject2.ordinal()])
        {
        default: 
          break;
        case 2: 
          c = CallRecordingFloatingButtonPresenter.CallRecordingButtonMode.ENDED;
          i.e();
          localObject2 = f;
          if (localObject2 != null) {
            ((Timer)localObject2).cancel();
          }
          f = null;
          a = null;
          localCallRecordingFloatingButtonPresenter.c();
          break;
        case 1: 
          c = CallRecordingFloatingButtonPresenter.CallRecordingButtonMode.RECORDING;
          i.a(h);
          e = new b(j.a());
          localObject2 = c.c.a.a("CallRecorderCountUpTimer");
          ((Timer)localObject2).schedule((TimerTask)new CallRecordingFloatingButtonPresenter.c(localCallRecordingFloatingButtonPresenter), 500L, 1000L);
          f = ((Timer)localObject2);
          localCallRecordingFloatingButtonPresenter.c();
        }
        ((CallRecordingFloatingButton)localObject1).performClick();
      }
      g.a(a.a, true);
      new Handler(Looper.getMainLooper()).postDelayed((Runnable)new Runnable()
      {
        public final void run()
        {
          g.a(a.a.a, false);
        }
      }, 1000L);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.g.c.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */