package com.truecaller.calling.recorder.floatingbutton;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.WindowManager;

final class d$1
  implements ServiceConnection
{
  d$1(d paramd) {}
  
  public final void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    paramComponentName = (BubblesService.a)paramIBinder;
    a.c = a;
    paramIBinder = a;
    paramComponentName = c;
    int i = d;
    if (i != 0)
    {
      if (c != null) {
        paramComponentName.b();
      }
      c = new b(paramComponentName);
      c.setWindowManager(d);
      c.setViewParams(BubblesService.c());
      c.setVisibility(8);
      LayoutInflater.from(paramComponentName).inflate(i, c, true);
      paramComponentName.a(c);
      paramIBinder = new c.a(paramComponentName);
      Object localObject = paramComponentName.a();
      a.c = ((WindowManager)localObject);
      localObject = f;
      a.e = ((c.b)localObject);
      localObject = c;
      a.b = ((b)localObject);
      e = a;
    }
    paramComponentName = a;
    b = true;
    if (e != null) {
      a.e.a();
    }
    paramComponentName = a.c;
    paramIBinder = a.f;
    f = paramIBinder;
    if (e != null) {
      e.e = paramIBinder;
    }
  }
  
  public final void onServiceDisconnected(ComponentName paramComponentName)
  {
    a.b = false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.d.1
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */