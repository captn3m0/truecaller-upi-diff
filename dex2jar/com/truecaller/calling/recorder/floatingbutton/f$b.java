package com.truecaller.calling.recorder.floatingbutton;

import com.truecaller.androidactors.u;

final class f$b
  extends u<e, Void>
{
  private final boolean b;
  
  private f$b(com.truecaller.androidactors.e parame, boolean paramBoolean)
  {
    super(parame);
    b = paramBoolean;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".showBrandingView(");
    localStringBuilder.append(a(Boolean.valueOf(b), 2));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.f.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */