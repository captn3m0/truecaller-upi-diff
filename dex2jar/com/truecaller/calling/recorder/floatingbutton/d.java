package com.truecaller.calling.recorder.floatingbutton;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import java.util.List;

public final class d
{
  @SuppressLint({"StaticFieldLeak"})
  static d a;
  boolean b;
  BubblesService c;
  int d;
  j e;
  c.b f;
  private Context g;
  private ServiceConnection h = new ServiceConnection()
  {
    public final void onServiceConnected(ComponentName paramAnonymousComponentName, IBinder paramAnonymousIBinder)
    {
      paramAnonymousComponentName = (BubblesService.a)paramAnonymousIBinder;
      c = a;
      paramAnonymousIBinder = d.this;
      paramAnonymousComponentName = c;
      int i = d;
      if (i != 0)
      {
        if (c != null) {
          paramAnonymousComponentName.b();
        }
        c = new b(paramAnonymousComponentName);
        c.setWindowManager(d);
        c.setViewParams(BubblesService.c());
        c.setVisibility(8);
        LayoutInflater.from(paramAnonymousComponentName).inflate(i, c, true);
        paramAnonymousComponentName.a(c);
        paramAnonymousIBinder = new c.a(paramAnonymousComponentName);
        Object localObject = paramAnonymousComponentName.a();
        a.c = ((WindowManager)localObject);
        localObject = f;
        a.e = ((c.b)localObject);
        localObject = c;
        a.b = ((b)localObject);
        e = a;
      }
      paramAnonymousComponentName = d.this;
      b = true;
      if (e != null) {
        e.a();
      }
      paramAnonymousComponentName = c;
      paramAnonymousIBinder = f;
      f = paramAnonymousIBinder;
      if (e != null) {
        e.e = paramAnonymousIBinder;
      }
    }
    
    public final void onServiceDisconnected(ComponentName paramAnonymousComponentName)
    {
      b = false;
    }
  };
  
  d(Context paramContext)
  {
    g = paramContext;
  }
  
  public final void a()
  {
    Context localContext = g;
    localContext.bindService(new Intent(localContext, BubblesService.class), h, 1);
  }
  
  public final void a(int paramInt)
  {
    if (b) {
      c.a(paramInt);
    }
  }
  
  public final void a(BubbleLayout paramBubbleLayout)
  {
    if (b) {
      c.a(paramBubbleLayout);
    }
  }
  
  public final void a(BubbleLayout paramBubbleLayout, int paramInt1, int paramInt2)
  {
    if (b)
    {
      BubblesService localBubblesService = c;
      WindowManager.LayoutParams localLayoutParams = BubblesService.a(paramInt1, paramInt2);
      paramBubbleLayout.setWindowManager(localBubblesService.a());
      paramBubbleLayout.setViewParams(localLayoutParams);
      paramBubbleLayout.setLayoutCoordinator(e);
      a.add(paramBubbleLayout);
      localBubblesService.a(paramBubbleLayout);
    }
  }
  
  public final void b()
  {
    g.unbindService(h);
  }
  
  public final void c()
  {
    if (b)
    {
      c.b();
      return;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(BubblesService.class.getSimpleName());
    localStringBuilder.append(" is not bounded.");
    localStringBuilder.toString();
  }
  
  public static final class a
  {
    private d a;
    
    public a(Context paramContext)
    {
      if (d.a == null) {
        d.a = new d(paramContext);
      }
      a = d.a;
    }
    
    public final a a()
    {
      a.d = 2131558509;
      return this;
    }
    
    public final a a(c.b paramb)
    {
      a.f = paramb;
      return this;
    }
    
    public final a a(j paramj)
    {
      a.e = paramj;
      return this;
    }
    
    public final d b()
    {
      return a;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */