package com.truecaller.calling.recorder.floatingbutton;

import com.truecaller.androidactors.u;

final class f$c
  extends u<e, Void>
{
  private final String b;
  private final i c;
  
  private f$c(com.truecaller.androidactors.e parame, String paramString, i parami)
  {
    super(parame);
    b = paramString;
    c = parami;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".showCallRecordingButton(");
    localStringBuilder.append(a(b, 2));
    localStringBuilder.append(",");
    localStringBuilder.append(a(c, 2));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.f.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */