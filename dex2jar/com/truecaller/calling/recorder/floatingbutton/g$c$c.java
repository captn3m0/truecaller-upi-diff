package com.truecaller.calling.recorder.floatingbutton;

import android.content.BroadcastReceiver;
import android.support.v4.content.d;
import com.truecaller.calling.recorder.CallRecordingFloatingButton;

final class g$c$c
  implements BubbleLayout.c
{
  g$c$c(BubbleLayout paramBubbleLayout, g.c paramc) {}
  
  public final void a()
  {
    d.a(a.getContext()).a((BroadcastReceiver)g.h(b.a));
    CallRecordingFloatingButton localCallRecordingFloatingButton = g.c(b.a);
    if (localCallRecordingFloatingButton != null)
    {
      localCallRecordingFloatingButton.a(true);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.g.c.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */