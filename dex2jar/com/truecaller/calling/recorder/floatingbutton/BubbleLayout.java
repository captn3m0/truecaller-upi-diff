package com.truecaller.calling.recorder.floatingbutton;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.graphics.Point;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;

public class BubbleLayout
  extends a
{
  d a;
  private float b;
  private float c;
  private int d;
  private int e;
  private b f;
  private long g;
  private a h = new a((byte)0);
  private int i;
  private WindowManager j;
  private boolean k = true;
  private c l;
  
  public BubbleLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    j = ((WindowManager)paramContext.getSystemService("window"));
    setClickable(true);
    setLongClickable(true);
    setOnLongClickListener(new -..Lambda.BubbleLayout.VRyMLcFZGM04YFKzy368ClEo_Rs(this));
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    if (!isInEditMode())
    {
      AnimatorSet localAnimatorSet = (AnimatorSet)AnimatorInflater.loadAnimator(getContext(), 2130837505);
      localAnimatorSet.setTarget(this);
      localAnimatorSet.start();
    }
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if (paramMotionEvent != null)
    {
      int m;
      Object localObject;
      switch (paramMotionEvent.getAction())
      {
      default: 
        break;
      case 2: 
        m = d;
        int n = (int)(paramMotionEvent.getRawX() - b);
        int i1 = e;
        int i2 = (int)(paramMotionEvent.getRawY() - c);
        getViewParamsx = (m + n);
        getViewParamsy = (i1 + i2);
        getWindowManager().updateViewLayout(this, getViewParams());
        if (getLayoutCoordinator() != null) {
          getLayoutCoordinator().a(this);
        }
        localObject = l;
        if (localObject != null) {
          ((c)localObject).a();
        }
        break;
      case 1: 
        if (k)
        {
          m = i / 2;
          float f1;
          if (getViewParamsx >= m) {
            f1 = i;
          } else {
            f1 = 0.0F;
          }
          a.a(h, f1, getViewParamsy);
        }
        if (getLayoutCoordinator() != null)
        {
          localObject = getLayoutCoordinator();
          if (b != null)
          {
            if (((c)localObject).b(this)) {
              d.a(this);
            }
            b.setVisibility(8);
          }
          if (!isInEditMode())
          {
            localObject = (AnimatorSet)AnimatorInflater.loadAnimator(getContext(), 2130837510);
            ((AnimatorSet)localObject).setTarget(this);
            ((AnimatorSet)localObject).start();
          }
        }
        if (System.currentTimeMillis() - g < 150L)
        {
          localObject = f;
          if (localObject != null) {
            ((b)localObject).a();
          }
        }
        break;
      case 0: 
        d = getViewParamsx;
        e = getViewParamsy;
        b = paramMotionEvent.getRawX();
        c = paramMotionEvent.getRawY();
        if (!isInEditMode())
        {
          localObject = (AnimatorSet)AnimatorInflater.loadAnimator(getContext(), 2130837504);
          ((AnimatorSet)localObject).setTarget(this);
          ((AnimatorSet)localObject).start();
        }
        g = System.currentTimeMillis();
        localObject = new DisplayMetrics();
        j.getDefaultDisplay().getMetrics((DisplayMetrics)localObject);
        localObject = getWindowManager().getDefaultDisplay();
        Point localPoint = new Point();
        ((Display)localObject).getSize(localPoint);
        i = (x - getWidth());
        a.a(h);
      }
    }
    return super.onTouchEvent(paramMotionEvent);
  }
  
  public void setOnBubbleClickListener(b paramb)
  {
    f = paramb;
  }
  
  public void setOnBubbleMovedListener(c paramc)
  {
    l = paramc;
  }
  
  public void setOnBubbleRemoveListener(d paramd)
  {
    a = paramd;
  }
  
  public void setShouldStickToWall(boolean paramBoolean)
  {
    k = paramBoolean;
  }
  
  final class a
    implements Runnable
  {
    private Handler b = new Handler(Looper.getMainLooper());
    private float c;
    private float d;
    private long e;
    
    private a() {}
    
    public final void run()
    {
      if ((getRootView() != null) && (getRootView().getParent() != null))
      {
        float f1 = Math.min(1.0F, (float)(System.currentTimeMillis() - e) / 400.0F);
        float f2 = c;
        float f3 = getViewParams().x;
        float f4 = d;
        float f5 = getViewParams().y;
        BubbleLayout.a(BubbleLayout.this, (f2 - f3) * f1, (f4 - f5) * f1);
        if (f1 < 1.0F) {
          b.post(this);
        }
      }
    }
  }
  
  public static abstract interface b
  {
    public abstract void a();
  }
  
  public static abstract interface c
  {
    public abstract void a();
  }
  
  public static abstract interface d
  {
    public abstract void a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.BubbleLayout
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */