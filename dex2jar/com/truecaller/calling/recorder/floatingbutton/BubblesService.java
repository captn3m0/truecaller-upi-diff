package com.truecaller.calling.recorder.floatingbutton;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BubblesService
  extends Service
{
  List<BubbleLayout> a = new ArrayList();
  final Handler b = new Handler(Looper.getMainLooper());
  b c;
  WindowManager d;
  c e;
  c.b f;
  private a g = new a();
  
  static WindowManager.LayoutParams a(int paramInt1, int paramInt2)
  {
    WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams(-2, -2, com.truecaller.callerid.b.c.a(TrueApp.y().a().bw()), 524296, -3);
    gravity = 8388659;
    x = paramInt1;
    y = paramInt2;
    return localLayoutParams;
  }
  
  private void b(BubbleLayout paramBubbleLayout)
  {
    if (paramBubbleLayout == null) {
      return;
    }
    b.post(new -..Lambda.BubblesService.zNp1oYJH4PhOKXSOTuhscvk7arg(this, paramBubbleLayout));
  }
  
  static WindowManager.LayoutParams c()
  {
    WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams(-1, -1, com.truecaller.callerid.b.c.a(TrueApp.y().a().bw()), 524296, -3);
    x = 0;
    y = 0;
    return localLayoutParams;
  }
  
  final WindowManager a()
  {
    if (d == null) {
      d = ((WindowManager)getSystemService("window"));
    }
    return d;
  }
  
  public final void a(int paramInt)
  {
    if (a.isEmpty()) {
      return;
    }
    BubbleLayout localBubbleLayout = (BubbleLayout)a.get(0);
    WindowManager.LayoutParams localLayoutParams = a(getViewParamsx, paramInt);
    localBubbleLayout.setViewParams(localLayoutParams);
    b.post(new -..Lambda.BubblesService.WU5TTlSiGKsoYVKjK5ocQn2p7qg(this, localBubbleLayout, localLayoutParams));
  }
  
  public final void a(BubbleLayout paramBubbleLayout)
  {
    b(paramBubbleLayout);
  }
  
  final void a(a parama)
  {
    b.post(new -..Lambda.BubblesService.6kL6qIJ9tiXKA5heZJ774PkQpw4(this, parama));
  }
  
  final void b()
  {
    b.post(new -..Lambda.BubblesService.Kqd19gcwp8oMi87hB1V1YWaNLIs(this));
  }
  
  public IBinder onBind(Intent paramIntent)
  {
    return g;
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    b();
  }
  
  public boolean onUnbind(Intent paramIntent)
  {
    Iterator localIterator = a.iterator();
    while (localIterator.hasNext()) {
      b((BubbleLayout)localIterator.next());
    }
    a.clear();
    return super.onUnbind(paramIntent);
  }
  
  public final class a
    extends Binder
  {
    public a() {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.BubblesService
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */