package com.truecaller.calling.recorder.floatingbutton;

import android.content.Context;

public final class d$a
{
  private d a;
  
  public d$a(Context paramContext)
  {
    if (d.a == null) {
      d.a = new d(paramContext);
    }
    a = d.a;
  }
  
  public final a a()
  {
    a.d = 2131558509;
    return this;
  }
  
  public final a a(c.b paramb)
  {
    a.f = paramb;
    return this;
  }
  
  public final a a(j paramj)
  {
    a.e = paramj;
    return this;
  }
  
  public final d b()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.d.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */