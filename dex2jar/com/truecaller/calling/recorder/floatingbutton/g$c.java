package com.truecaller.calling.recorder.floatingbutton;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import c.g.b.k;
import c.n;
import c.u;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.w;
import com.truecaller.calling.recorder.CallRecordingFloatingButton;
import com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter;
import com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter.CallRecordingButtonMode;
import com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter.c;
import com.truecaller.calling.recorder.CallRecordingManager;
import com.truecaller.calling.recorder.ba;
import com.truecaller.calling.recorder.ba.a;
import com.truecaller.old.data.access.Settings;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import java.net.URLDecoder;
import java.util.Timer;
import java.util.TimerTask;
import org.a.a.b;

final class g$c
  implements j
{
  g$c(g paramg, String paramString, i parami) {}
  
  public final void a()
  {
    g.a(a);
    Object localObject2 = a;
    Object localObject3 = LayoutInflater.from((Context)new ContextThemeWrapper(a, aresId));
    Object localObject1 = null;
    localObject3 = ((LayoutInflater)localObject3).inflate(2131558636, null);
    if (localObject3 != null)
    {
      g.a((g)localObject2, (BubbleLayout)localObject3);
      localObject2 = a;
      localObject3 = g.b((g)localObject2);
      if (localObject3 != null) {
        localObject1 = (CallRecordingFloatingButton)((BubbleLayout)localObject3).findViewById(2131362380);
      }
      g.a((g)localObject2, (CallRecordingFloatingButton)localObject1);
      localObject1 = g.c(a);
      if (localObject1 != null) {
        ((CallRecordingFloatingButton)localObject1).setPhoneNumber(b);
      }
      localObject1 = g.b(a);
      if (localObject1 != null)
      {
        ((BubbleLayout)localObject1).setOnBubbleClickListener((BubbleLayout.b)new a(this));
        ((BubbleLayout)localObject1).setOnBubbleRemoveListener((BubbleLayout.d)new b(this));
        ((BubbleLayout)localObject1).setOnBubbleMovedListener((BubbleLayout.c)new c((BubbleLayout)localObject1, this));
        localObject2 = g.a(a, Settings.c("callerIdLastYPosition"));
        int i = ((Number)a).intValue();
        int j = ((Number)b).intValue();
        localObject2 = g.e(a);
        if (localObject2 != null) {
          ((d)localObject2).a(g.b(a), i, j);
        }
        android.support.v4.content.d.a(((BubbleLayout)localObject1).getContext()).a((BroadcastReceiver)g.h(a), new IntentFilter("BroadcastCallerIdPosY"));
        localObject1 = c;
        if (localObject1 != null) {
          ((i)localObject1).onCallRecordingButtonInitialised();
        }
        localObject1 = a;
        long l1 = g.i((g)localObject1);
        long l2 = g.i(a);
        localObject2 = c.c.a.a("SafeRecordingCloser");
        ((Timer)localObject2).schedule((TimerTask)new d(this), l1, l2);
        g.a((g)localObject1, (Timer)localObject2);
        return;
      }
      return;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.calling.recorder.floatingbutton.BubbleLayout");
  }
  
  static final class a
    implements BubbleLayout.b
  {
    a(g.c paramc) {}
    
    public final void a()
    {
      Object localObject1 = new StringBuilder("showCallRecordingButton:: Debounce? ");
      ((StringBuilder)localObject1).append(g.d(a.a));
      ((StringBuilder)localObject1).toString();
      if (!g.d(a.a))
      {
        localObject1 = g.c(a.a);
        if (localObject1 != null)
        {
          CallRecordingFloatingButtonPresenter localCallRecordingFloatingButtonPresenter = a;
          if (localCallRecordingFloatingButtonPresenter == null) {
            k.a("presenter");
          }
          Object localObject2 = c;
          switch (com.truecaller.calling.recorder.k.a[localObject2.ordinal()])
          {
          default: 
            break;
          case 2: 
            c = CallRecordingFloatingButtonPresenter.CallRecordingButtonMode.ENDED;
            i.e();
            localObject2 = f;
            if (localObject2 != null) {
              ((Timer)localObject2).cancel();
            }
            f = null;
            a = null;
            localCallRecordingFloatingButtonPresenter.c();
            break;
          case 1: 
            c = CallRecordingFloatingButtonPresenter.CallRecordingButtonMode.RECORDING;
            i.a(h);
            e = new b(j.a());
            localObject2 = c.c.a.a("CallRecorderCountUpTimer");
            ((Timer)localObject2).schedule((TimerTask)new CallRecordingFloatingButtonPresenter.c(localCallRecordingFloatingButtonPresenter), 500L, 1000L);
            f = ((Timer)localObject2);
            localCallRecordingFloatingButtonPresenter.c();
          }
          ((CallRecordingFloatingButton)localObject1).performClick();
        }
        g.a(a.a, true);
        new Handler(Looper.getMainLooper()).postDelayed((Runnable)new Runnable()
        {
          public final void run()
          {
            g.a(a.a.a, false);
          }
        }, 1000L);
      }
    }
  }
  
  static final class b
    implements BubbleLayout.d
  {
    b(g.c paramc) {}
    
    public final void a()
    {
      Object localObject = g.e(a.a);
      if (localObject != null) {
        ((d)localObject).c();
      }
      localObject = g.e(a.a);
      if (localObject != null) {
        ((d)localObject).b();
      }
      g.f(a.a);
      localObject = g.g(a.a);
      if (localObject != null) {
        ((Timer)localObject).cancel();
      }
      g.a(a.a, null);
    }
  }
  
  static final class c
    implements BubbleLayout.c
  {
    c(BubbleLayout paramBubbleLayout, g.c paramc) {}
    
    public final void a()
    {
      android.support.v4.content.d.a(a.getContext()).a((BroadcastReceiver)g.h(jdField_thisa));
      CallRecordingFloatingButton localCallRecordingFloatingButton = g.c(jdField_thisa);
      if (localCallRecordingFloatingButton != null)
      {
        localCallRecordingFloatingButton.a(true);
        return;
      }
    }
  }
  
  public static final class d
    extends TimerTask
  {
    public d(g.c paramc) {}
    
    public final void run()
    {
      if (a.a.c.getCallState() == 0)
      {
        Object localObject1 = g.c(a.a);
        if (localObject1 != null)
        {
          Object localObject2 = a;
          if (localObject2 == null) {
            k.a("presenter");
          }
          localObject1 = a;
          if (localObject1 != null)
          {
            localObject2 = m;
            k.b(localObject1, "recordingFileAbsolutePath");
            String str = URLDecoder.decode((String)localObject1, "UTF-8");
            k.a(str, "URLDecoder.decode(record…ileAbsolutePath, \"UTF-8\")");
            str = (String)c.a.m.d(c.n.m.c((CharSequence)c.a.m.f(c.n.m.c((CharSequence)str, new String[] { "-" }, false, 6)), new String[] { "." }, false, 6));
            a.c(str).a((ac)new ba.a((ba)localObject2, (String)localObject1));
          }
        }
        a.a.a();
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.floatingbutton.g.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */