package com.truecaller.calling.recorder;

import android.content.Context;
import com.truecaller.wizard.utils.PermissionPoller;
import dagger.a.d;
import javax.inject.Provider;

public final class be
  implements d<PermissionPoller>
{
  private final bd a;
  private final Provider<Context> b;
  
  private be(bd parambd, Provider<Context> paramProvider)
  {
    a = parambd;
    b = paramProvider;
  }
  
  public static be a(bd parambd, Provider<Context> paramProvider)
  {
    return new be(parambd, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.be
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */