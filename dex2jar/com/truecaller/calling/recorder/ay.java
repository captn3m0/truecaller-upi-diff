package com.truecaller.calling.recorder;

import android.content.Context;
import c.g.b.k;
import com.nll.nativelibs.callrecording.AACCallRecorder;
import com.truecaller.callrecording.CallRecorder;
import com.truecaller.callrecording.CallRecorder.a;
import com.truecaller.callrecording.a;
import com.truecaller.callrecording.b;
import javax.inject.Inject;

public final class ay
  implements ax
{
  private final Context a;
  private final a b;
  private final h c;
  
  @Inject
  public ay(Context paramContext, a parama, h paramh)
  {
    a = paramContext;
    b = parama;
    c = paramh;
  }
  
  public final CallRecorder a(CallRecorder.a parama)
  {
    k.b(parama, "errorListener");
    if (c.d() == CallRecordingSettingsMvp.Configuration.SDK_MEDIA_RECORDER) {
      return (CallRecorder)new b();
    }
    return (CallRecorder)new AACCallRecorder(a, b, parama);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.ay
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */