package com.truecaller.calling.recorder;

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;

final class CallRecordingSettingsFragment$m
  implements DialogInterface.OnCancelListener
{
  CallRecordingSettingsFragment$m(CallRecordingSettingsFragment paramCallRecordingSettingsFragment) {}
  
  public final void onCancel(DialogInterface paramDialogInterface)
  {
    CallRecordingSettingsFragment.a(a, false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingSettingsFragment.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */