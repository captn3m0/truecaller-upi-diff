package com.truecaller.calling.recorder;

import com.truecaller.data.entity.CallRecording;
import org.a.a.b;

public abstract interface CallRecordingManager
  extends bv, g
{
  public abstract void a(bi parambi);
  
  public abstract void a(f paramf);
  
  public abstract void a(CallRecording paramCallRecording, PlaybackLaunchContext paramPlaybackLaunchContext);
  
  public abstract void a(String paramString);
  
  public abstract boolean a();
  
  public abstract boolean a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt);
  
  public abstract boolean a(CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext);
  
  public abstract String b();
  
  public abstract b c();
  
  public abstract boolean d();
  
  public abstract void e();
  
  public abstract boolean f();
  
  public abstract boolean g();
  
  public abstract void h();
  
  public abstract boolean i();
  
  public abstract boolean j();
  
  public static enum PlaybackLaunchContext
  {
    static
    {
      PlaybackLaunchContext localPlaybackLaunchContext1 = new PlaybackLaunchContext("CALL_LIST", 0);
      CALL_LIST = localPlaybackLaunchContext1;
      PlaybackLaunchContext localPlaybackLaunchContext2 = new PlaybackLaunchContext("AFTER_CALL", 1);
      AFTER_CALL = localPlaybackLaunchContext2;
      PlaybackLaunchContext localPlaybackLaunchContext3 = new PlaybackLaunchContext("RECORDINGS", 2);
      RECORDINGS = localPlaybackLaunchContext3;
      $VALUES = new PlaybackLaunchContext[] { localPlaybackLaunchContext1, localPlaybackLaunchContext2, localPlaybackLaunchContext3 };
    }
    
    private PlaybackLaunchContext() {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingManager
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */