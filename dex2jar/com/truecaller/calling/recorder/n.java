package com.truecaller.calling.recorder;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import com.truecaller.calling.recorder.floatingbutton.e;
import dagger.a.d;
import javax.inject.Provider;

public final class n
  implements d<f<e>>
{
  private final Provider<e> a;
  private final Provider<i> b;
  
  private n(Provider<e> paramProvider, Provider<i> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static n a(Provider<e> paramProvider, Provider<i> paramProvider1)
  {
    return new n(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */