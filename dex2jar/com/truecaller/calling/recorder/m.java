package com.truecaller.calling.recorder;

import com.truecaller.androidactors.k;
import dagger.a.d;
import javax.inject.Provider;

public final class m
  implements d<com.truecaller.androidactors.f<c>>
{
  private final Provider<k> a;
  private final Provider<bx> b;
  private final Provider<c.d.f> c;
  
  private m(Provider<k> paramProvider, Provider<bx> paramProvider1, Provider<c.d.f> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static m a(Provider<k> paramProvider, Provider<bx> paramProvider1, Provider<c.d.f> paramProvider2)
  {
    return new m(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */