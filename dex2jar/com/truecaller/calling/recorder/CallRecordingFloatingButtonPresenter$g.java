package com.truecaller.calling.recorder;

import c.d.a.a;
import c.d.b.a.f;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;
import org.a.a.aa;
import org.a.a.d.n;
import org.a.a.r;
import org.a.a.s;

@f(b="CallRecordingFloatingButtonPresenter.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter$setSource$1")
final class CallRecordingFloatingButtonPresenter$g
  extends c.d.b.a.k
  implements m<ag, c<? super x>, Object>
{
  int a;
  private ag c;
  
  CallRecordingFloatingButtonPresenter$g(CallRecordingFloatingButtonPresenter paramCallRecordingFloatingButtonPresenter, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new g(b, paramc);
    c = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    if (a == 0)
    {
      if (!(paramObject instanceof o.b))
      {
        paramObject = (bw)b.b;
        if (paramObject != null)
        {
          localObject = b.g.a((aa)r.a(0).a(s.a()));
          c.g.b.k.a(localObject, "periodFormatter.print(Pe…(0).normalizedStandard())");
          ((bw)paramObject).setLabel((String)localObject);
        }
        return x.a;
      }
      throw a;
    }
    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((g)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */