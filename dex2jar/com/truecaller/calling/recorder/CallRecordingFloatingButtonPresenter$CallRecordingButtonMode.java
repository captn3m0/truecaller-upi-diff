package com.truecaller.calling.recorder;

public enum CallRecordingFloatingButtonPresenter$CallRecordingButtonMode
{
  static
  {
    CallRecordingButtonMode localCallRecordingButtonMode1 = new CallRecordingButtonMode("NOT_STARTED", 0);
    NOT_STARTED = localCallRecordingButtonMode1;
    CallRecordingButtonMode localCallRecordingButtonMode2 = new CallRecordingButtonMode("RECORDING", 1);
    RECORDING = localCallRecordingButtonMode2;
    CallRecordingButtonMode localCallRecordingButtonMode3 = new CallRecordingButtonMode("ENDED", 2);
    ENDED = localCallRecordingButtonMode3;
    $VALUES = new CallRecordingButtonMode[] { localCallRecordingButtonMode1, localCallRecordingButtonMode2, localCallRecordingButtonMode3 };
  }
  
  private CallRecordingFloatingButtonPresenter$CallRecordingButtonMode() {}
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter.CallRecordingButtonMode
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */