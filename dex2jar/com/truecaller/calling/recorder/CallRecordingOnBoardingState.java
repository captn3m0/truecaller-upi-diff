package com.truecaller.calling.recorder;

public enum CallRecordingOnBoardingState
{
  static
  {
    CallRecordingOnBoardingState localCallRecordingOnBoardingState1 = new CallRecordingOnBoardingState("WHATS_NEW", 0);
    WHATS_NEW = localCallRecordingOnBoardingState1;
    CallRecordingOnBoardingState localCallRecordingOnBoardingState2 = new CallRecordingOnBoardingState("WHATS_NEW_NEGATIVE", 1);
    WHATS_NEW_NEGATIVE = localCallRecordingOnBoardingState2;
    CallRecordingOnBoardingState localCallRecordingOnBoardingState3 = new CallRecordingOnBoardingState("TERMS", 2);
    TERMS = localCallRecordingOnBoardingState3;
    CallRecordingOnBoardingState localCallRecordingOnBoardingState4 = new CallRecordingOnBoardingState("PERMISSIONS", 3);
    PERMISSIONS = localCallRecordingOnBoardingState4;
    CallRecordingOnBoardingState localCallRecordingOnBoardingState5 = new CallRecordingOnBoardingState("POST_ENABLE", 4);
    POST_ENABLE = localCallRecordingOnBoardingState5;
    CallRecordingOnBoardingState localCallRecordingOnBoardingState6 = new CallRecordingOnBoardingState("PAY_WALL", 5);
    PAY_WALL = localCallRecordingOnBoardingState6;
    CallRecordingOnBoardingState localCallRecordingOnBoardingState7 = new CallRecordingOnBoardingState("PAY_WALL_ON_EXPIRY", 6);
    PAY_WALL_ON_EXPIRY = localCallRecordingOnBoardingState7;
    $VALUES = new CallRecordingOnBoardingState[] { localCallRecordingOnBoardingState1, localCallRecordingOnBoardingState2, localCallRecordingOnBoardingState3, localCallRecordingOnBoardingState4, localCallRecordingOnBoardingState5, localCallRecordingOnBoardingState6, localCallRecordingOnBoardingState7 };
  }
  
  private CallRecordingOnBoardingState() {}
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingOnBoardingState
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */