package com.truecaller.calling.recorder;

public enum CallRecordingSettingsMvp$Configuration
{
  static
  {
    Configuration localConfiguration1 = new Configuration("DEFAULT", 0);
    DEFAULT = localConfiguration1;
    Configuration localConfiguration2 = new Configuration("SDK_MEDIA_RECORDER", 1);
    SDK_MEDIA_RECORDER = localConfiguration2;
    $VALUES = new Configuration[] { localConfiguration1, localConfiguration2 };
  }
  
  private CallRecordingSettingsMvp$Configuration() {}
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingSettingsMvp.Configuration
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */