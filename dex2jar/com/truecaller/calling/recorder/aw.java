package com.truecaller.calling.recorder;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import c.f;
import c.g.b.k;
import com.truecaller.utils.extensions.t;

public final class aw
  extends RecyclerView.ViewHolder
  implements av
{
  private final f b;
  private final f c;
  private final f d;
  private final f e;
  private final ar f;
  
  public aw(View paramView, ar paramar)
  {
    super(paramView);
    f = paramar;
    b = t.a(paramView, 2131362347);
    c = t.a(paramView, 2131364029);
    d = t.a(paramView, 2131364028);
    e = t.a(paramView, 2131364026);
    paramar = b();
    k.a(paramar, "titleView");
    paramar.setMaxLines(2);
    paramar = a();
    k.a(paramar, "upgradeView");
    paramar.setText((CharSequence)paramView.getContext().getString(2131887234));
    a().setOnClickListener((View.OnClickListener)new View.OnClickListener()
    {
      public final void onClick(View paramAnonymousView)
      {
        aw.a(a).a();
      }
    });
    ((ImageView)e.b()).setImageResource(2131233934);
  }
  
  private final TextView a()
  {
    return (TextView)b.b();
  }
  
  private final TextView b()
  {
    return (TextView)c.b();
  }
  
  public final void setCTATitle(String paramString)
  {
    k.b(paramString, "ctaTitle");
    TextView localTextView = a();
    if (localTextView != null)
    {
      localTextView.setText((CharSequence)paramString);
      return;
    }
  }
  
  public final void setText(String paramString)
  {
    k.b(paramString, "text");
    TextView localTextView = (TextView)d.b();
    k.a(localTextView, "textView");
    localTextView.setText((CharSequence)paramString);
  }
  
  public final void setTitle(String paramString)
  {
    k.b(paramString, "title");
    TextView localTextView = b();
    k.a(localTextView, "titleView");
    localTextView.setText((CharSequence)paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.aw
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */