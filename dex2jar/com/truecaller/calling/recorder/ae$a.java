package com.truecaller.calling.recorder;

import android.net.Uri;
import android.net.Uri.Builder;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.callerid.e;
import com.truecaller.callrecording.CallRecorder;
import com.truecaller.callrecording.CallRecorder.a;
import com.truecaller.log.AssertionUtil;
import java.util.Timer;
import java.util.TimerTask;
import kotlinx.coroutines.ag;

@c.d.b.a.f(b="CallRecordingManager.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.recorder.CallRecordingManagerImpl$startRecording$1")
final class ae$a
  extends c.d.b.a.k
  implements m<ag, c<? super x>, Object>
{
  int a;
  private ag d;
  
  ae$a(ae paramae, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new a(b, c, paramc);
    d = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = c.d.a.a.a;
    if (a == 0)
    {
      if (!(paramObject instanceof o.b))
      {
        ae localae = b;
        localObject = c;
        try
        {
          a = h.a((CallRecorder.a)localae);
          paramObject = a;
          if (paramObject != null) {
            ((CallRecorder)paramObject).setErrorListener((CallRecorder.a)localae);
          }
          Uri.Builder localBuilder = Uri.parse(localae.k()).buildUpon();
          StringBuilder localStringBuilder = new StringBuilder("TC-");
          localStringBuilder.append(e.a(org.a.a.f.a).a(i.a()));
          localStringBuilder.append('-');
          paramObject = localObject;
          if (localObject == null) {
            paramObject = "unknown";
          }
          localStringBuilder.append((String)paramObject);
          localStringBuilder.append(".m4a");
          b = localBuilder.appendPath(localStringBuilder.toString()).toString();
          paramObject = a;
          if (paramObject != null) {
            ((CallRecorder)paramObject).setOutputFile(b);
          }
          paramObject = a;
          if (paramObject != null) {
            ((CallRecorder)paramObject).prepare();
          }
          paramObject = a;
          if (paramObject != null) {
            ((CallRecorder)paramObject).start();
          }
          c = new org.a.a.b();
          paramObject = d;
          if (paramObject != null) {
            ((bi)paramObject).a();
          }
          if (a != null) {
            ((e)g.a()).c();
          }
          long l1 = k;
          long l2 = k;
          paramObject = c.c.a.a("SafeRecordingCloser");
          ((Timer)paramObject).schedule((TimerTask)new ae.b(localae), l1, l2);
          f = ((Timer)paramObject);
        }
        catch (Exception paramObject)
        {
          localae.onError((Exception)paramObject);
          AssertionUtil.reportThrowableButNeverCrash((Throwable)paramObject);
        }
        return x.a;
      }
      throw a;
    }
    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((a)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.ae.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */