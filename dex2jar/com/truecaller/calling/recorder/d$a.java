package com.truecaller.calling.recorder;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.CallRecording;

final class d$a
  extends u<c, Long>
{
  private final CallRecording b;
  
  private d$a(e parame, CallRecording paramCallRecording)
  {
    super(parame);
    b = paramCallRecording;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".getDuration(");
    localStringBuilder.append(a(b, 2));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.d.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */