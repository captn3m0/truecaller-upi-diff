package com.truecaller.calling.recorder;

import com.truecaller.common.g.a;
import com.truecaller.featuretoggles.e;
import com.truecaller.util.al;
import javax.inject.Provider;

public final class j
  implements dagger.a.d<i>
{
  private final Provider<e> a;
  private final Provider<al> b;
  private final Provider<com.truecaller.utils.d> c;
  private final Provider<a> d;
  
  private j(Provider<e> paramProvider, Provider<al> paramProvider1, Provider<com.truecaller.utils.d> paramProvider2, Provider<a> paramProvider3)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static j a(Provider<e> paramProvider, Provider<al> paramProvider1, Provider<com.truecaller.utils.d> paramProvider2, Provider<a> paramProvider3)
  {
    return new j(paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */