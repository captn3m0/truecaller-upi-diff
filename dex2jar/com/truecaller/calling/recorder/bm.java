package com.truecaller.calling.recorder;

import android.database.Cursor;
import c.a.m;
import c.a.y;
import c.g.a.b;
import c.g.b.aa;
import c.g.b.l;
import c.l.g;
import c.x;
import com.truecaller.androidactors.a;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.bb;
import com.truecaller.callhistory.r;
import com.truecaller.callhistory.z;
import com.truecaller.calling.dialer.bu;
import com.truecaller.calling.dialer.v;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.utils.n;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.inject.Inject;
import javax.inject.Named;

public final class bm
  extends bb<bq>
  implements bl
{
  final Set<Long> a;
  private z c;
  private a d;
  private a e;
  private final f<r> f;
  private final bu g;
  private final n h;
  private final v i;
  private final bv j;
  private final com.truecaller.androidactors.k k;
  
  @Inject
  public bm(f<r> paramf, bu parambu, n paramn, @Named("call_recording_data_observer") v paramv, bv parambv, com.truecaller.androidactors.k paramk)
  {
    f = paramf;
    g = parambu;
    h = paramn;
    i = paramv;
    j = parambv;
    k = paramk;
    a = ((Set)new LinkedHashSet());
  }
  
  private final b<z, x> i()
  {
    return (b)new b(this);
  }
  
  private final int j()
  {
    z localz = c;
    if (localz != null) {
      return localz.getCount();
    }
    return 0;
  }
  
  public final void D_()
  {
    d = ((r)f.a()).a().a(k.a(), (ac)new bn(i()));
  }
  
  public final z a(ad paramad, g<?> paramg)
  {
    c.g.b.k.b(paramad, "callRecordingItemsPresenter");
    c.g.b.k.b(paramg, "property");
    return c;
  }
  
  public final void a()
  {
    bq localbq = (bq)b;
    if (localbq != null)
    {
      localbq.i();
      return;
    }
  }
  
  public final void a(int paramInt) {}
  
  public final void a(Contact paramContact, DetailsFragment.SourceType paramSourceType, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    c.g.b.k.b(paramContact, "contact");
    c.g.b.k.b(paramSourceType, "sourceType");
    bq localbq = (bq)b;
    if (localbq != null)
    {
      localbq.a(paramContact, paramSourceType, paramBoolean1, paramBoolean2, paramBoolean3);
      return;
    }
  }
  
  public final void a(HistoryEvent paramHistoryEvent, DetailsFragment.SourceType paramSourceType, boolean paramBoolean1, boolean paramBoolean2)
  {
    c.g.b.k.b(paramHistoryEvent, "historyEvent");
    c.g.b.k.b(paramSourceType, "sourceType");
    bq localbq = (bq)b;
    if (localbq != null)
    {
      localbq.a(paramHistoryEvent, paramSourceType, paramBoolean1, paramBoolean2);
      return;
    }
  }
  
  public final void a(PremiumPresenterView.LaunchContext paramLaunchContext)
  {
    c.g.b.k.b(paramLaunchContext, "launchContext");
    bq localbq = (bq)b;
    if (localbq != null)
    {
      localbq.a(paramLaunchContext);
      return;
    }
  }
  
  public final void a(Object paramObject, by paramby)
  {
    c.g.b.k.b(paramObject, "objectsDeleted");
    c.g.b.k.b(paramby, "eventListener");
    bq localbq = (bq)b;
    if (localbq != null)
    {
      String str = h.a(2131887561, new Object[0]);
      c.g.b.k.a(str, "resourceProvider.getStri…_menu_delete_prompt_text)");
      localbq.a(str, paramObject, paramby);
      return;
    }
  }
  
  public final void a(Collection<String> paramCollection)
  {
    c.g.b.k.b(paramCollection, "normalizedNumbers");
    paramCollection = ((Iterable)m.i((Iterable)paramCollection)).iterator();
    while (paramCollection.hasNext())
    {
      Object localObject1 = (String)paramCollection.next();
      localObject1 = g.a((String)localObject1);
      if (localObject1 != null)
      {
        Object localObject2 = d;
        if (localObject2 != null) {
          ((a)localObject2).a();
        }
        d = ((r)f.a()).a().a(k.a(), (ac)new bo(i()));
        localObject2 = (bq)b;
        if (localObject2 != null) {
          ((bq)localObject2).a((Set)localObject1);
        }
      }
    }
  }
  
  public final void a(Set<String> paramSet)
  {
    c.g.b.k.b(paramSet, "normalizedNumbers");
    paramSet = ((Iterable)paramSet).iterator();
    while (paramSet.hasNext())
    {
      Object localObject = (String)paramSet.next();
      localObject = g.a((String)localObject);
      if (localObject != null)
      {
        bq localbq = (bq)b;
        if (localbq != null) {
          localbq.a((Set)localObject);
        }
      }
    }
  }
  
  public final boolean a(int paramInt1, int paramInt2)
  {
    if (paramInt1 != 1) {
      return false;
    }
    if (paramInt2 != 2131361848)
    {
      if (paramInt2 != 2131361934) {
        return true;
      }
      a.clear();
      Set localSet1 = a;
      Object localObject = c;
      if (localObject != null)
      {
        localObject = (Cursor)localObject;
        ((Cursor)localObject).moveToFirst();
        Set localSet2 = (Set)new LinkedHashSet();
        do
        {
          localSet2.add(Long.valueOf(((Cursor)localObject).getLong(((Cursor)localObject).getColumnIndex("history_call_recording_id"))));
        } while (((Cursor)localObject).moveToNext());
        localObject = (Collection)localSet2;
      }
      else
      {
        localObject = (Collection)y.a;
      }
      localSet1.addAll((Collection)localObject);
      localObject = (bq)b;
      if (localObject != null) {
        ((bq)localObject).g();
      }
      localObject = (bq)b;
      if (localObject != null)
      {
        ((bq)localObject).c();
        return true;
      }
    }
    else
    {
      a(a, (by)new a(this));
    }
    return true;
  }
  
  public final boolean a(CallRecording paramCallRecording)
  {
    c.g.b.k.b(paramCallRecording, "callRecording");
    return a.contains(Long.valueOf(a));
  }
  
  public final void b(int paramInt)
  {
    if (paramInt == 1)
    {
      a.clear();
      bq localbq = (bq)b;
      if (localbq != null)
      {
        localbq.b(false);
        return;
      }
    }
  }
  
  public final void b(CallRecording paramCallRecording)
  {
    c.g.b.k.b(paramCallRecording, "callRecording");
    long l = a;
    paramCallRecording = a;
    if (!paramCallRecording.remove(Long.valueOf(l))) {
      paramCallRecording.add(Long.valueOf(l));
    }
    if (paramCallRecording.isEmpty())
    {
      paramCallRecording = (bq)b;
      if (paramCallRecording != null) {
        paramCallRecording.B_();
      }
    }
    paramCallRecording = (bq)b;
    if (paramCallRecording != null) {
      paramCallRecording.g();
    }
    paramCallRecording = (bq)b;
    if (paramCallRecording != null)
    {
      paramCallRecording.c();
      return;
    }
  }
  
  public final boolean b()
  {
    bq localbq = (bq)b;
    if (localbq != null) {
      localbq.A_();
    }
    localbq = (bq)b;
    if (localbq != null) {
      localbq.b(true);
    }
    return true;
  }
  
  public final boolean b(int paramInt1, int paramInt2)
  {
    if (paramInt1 == 1)
    {
      if (paramInt2 != 2131361848)
      {
        if (paramInt2 != 2131361934) {
          return false;
        }
        return a.size() != j();
      }
      return true;
    }
    return false;
  }
  
  public final int c(int paramInt)
  {
    if (paramInt == 1) {
      return 2131623937;
    }
    return -1;
  }
  
  public final w<Boolean> c(CallRecording paramCallRecording)
  {
    c.g.b.k.b(paramCallRecording, "callRecording");
    a.remove(Long.valueOf(a));
    return ((r)f.a()).a(paramCallRecording);
  }
  
  public final String c()
  {
    return h.a(2131886292, new Object[] { Integer.valueOf(a.size()), Integer.valueOf(j()) });
  }
  
  public final void e()
  {
    bq localbq = (bq)b;
    if (localbq != null)
    {
      localbq.g();
      return;
    }
  }
  
  public final void f()
  {
    bq localbq = (bq)b;
    if (localbq != null)
    {
      localbq.h();
      return;
    }
  }
  
  public final void g()
  {
    bq localbq = (bq)b;
    if (localbq != null)
    {
      localbq.g();
      return;
    }
  }
  
  public final boolean h()
  {
    z localz = c;
    return (localz == null) || (localz.getCount() != 0);
  }
  
  public final void toast(String paramString)
  {
    c.g.b.k.b(paramString, "message");
    bq localbq = (bq)b;
    if (localbq != null)
    {
      localbq.toast(paramString);
      return;
    }
  }
  
  public final void y_()
  {
    super.y_();
    a locala = d;
    if (locala != null) {
      locala.a();
    }
    i.a(null);
    locala = e;
    if (locala != null)
    {
      locala.a();
      return;
    }
  }
  
  public static final class a
    implements by
  {
    public final void a(Object paramObject)
    {
      c.g.b.k.b(paramObject, "any");
      bm localbm = a;
      bm.a(localbm, ((r)bm.e(localbm).a()).a((Collection)m.g((Iterable)aa.b(paramObject))).a(bm.f(a).a(), (ac)new a(this)));
    }
    
    public final void b(Object paramObject)
    {
      c.g.b.k.b(paramObject, "any");
    }
    
    static final class a<R>
      implements ac<Boolean>
    {
      a(bm.a parama) {}
    }
  }
  
  static final class b
    extends l
    implements b<z, x>
  {
    b(bm parambm)
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.bm
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */