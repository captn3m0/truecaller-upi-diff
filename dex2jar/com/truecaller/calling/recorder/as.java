package com.truecaller.calling.recorder;

import c.g.b.k;
import c.l;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.utils.n;
import javax.inject.Inject;

public final class as
  extends com.truecaller.adapter_delegates.c<av>
  implements ar
{
  private final bv b;
  private final n c;
  private final cb d;
  private final bu e;
  private final com.truecaller.common.f.c f;
  private final bk g;
  private final CallRecordingManager h;
  
  @Inject
  public as(bv parambv, n paramn, cb paramcb, bu parambu, com.truecaller.common.f.c paramc, bk parambk, CallRecordingManager paramCallRecordingManager)
  {
    b = parambv;
    c = paramn;
    d = paramcb;
    e = parambu;
    f = paramc;
    g = parambk;
    h = paramCallRecordingManager;
  }
  
  private void b(av paramav)
  {
    k.b(paramav, "itemView");
    FreeTrialStatus localFreeTrialStatus = b.n();
    boolean bool = h.g();
    int i = 2131887567;
    if (bool)
    {
      localObject = c.a(2131887567, new Object[0]);
      k.a(localObject, "resourceProvider.getStri…all_recording_list_promo)");
      paramav.setText((String)localObject);
      localObject = c.a(2131887568, new Object[0]);
      k.a(localObject, "resourceProvider.getStri…ing_list_promo_cta_start)");
      paramav.setCTATitle((String)localObject);
      localObject = c.a(2131887629, new Object[0]);
      k.a(localObject, "resourceProvider.getStri…ecording_whats_new_title)");
      paramav.setTitle((String)localObject);
      return;
    }
    Object localObject = c;
    if (localFreeTrialStatus.isActive()) {
      i = 2131887570;
    }
    localObject = ((n)localObject).a(i, new Object[0]);
    k.a(localObject, "resourceProvider.getStri…      }\n                )");
    paramav.setText((String)localObject);
    switch (at.a[localFreeTrialStatus.ordinal()])
    {
    default: 
      throw new l();
    case 2: 
    case 3: 
      localObject = c.a(2131887569, new Object[0]);
      break;
    case 1: 
      localObject = c.a(2131887568, new Object[0]);
    }
    k.a(localObject, "when (freeTrialStatus) {…pgrade)\n                }");
    paramav.setCTATitle((String)localObject);
    switch (at.b[localFreeTrialStatus.ordinal()])
    {
    default: 
      throw new l();
    case 3: 
      localObject = c.a(2131887572, new Object[0]);
      break;
    case 2: 
      i = b.m();
      localObject = c.a(2131755040, i, new Object[] { Integer.valueOf(i) });
      break;
    case 1: 
      localObject = c.a(2131887571, new Object[0]);
    }
    k.a(localObject, "when (freeTrialStatus) {…_on_expiry)\n            }");
    paramav.setTitle((String)localObject);
  }
  
  public final void a()
  {
    if (h.g())
    {
      d.a(PremiumPresenterView.LaunchContext.CALL_RECORDING_PAY_WALL);
      return;
    }
    FreeTrialStatus localFreeTrialStatus = b.n();
    switch (at.c[localFreeTrialStatus.ordinal()])
    {
    default: 
      throw new l();
    case 2: 
    case 3: 
      d.a(PremiumPresenterView.LaunchContext.CALL_RECORDINGS);
      return;
    }
    e.a();
  }
  
  public final void a(av paramav)
  {
    b(paramav);
  }
  
  public final int getItemCount()
  {
    if (!f.d()) {
      return 1;
    }
    return 0;
  }
  
  public final long getItemId(int paramInt)
  {
    return 1L;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.as
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */