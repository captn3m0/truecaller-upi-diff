package com.truecaller.calling.recorder;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.transition.p;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.widget.CardView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import c.g.a.m;
import c.g.b.k;
import c.g.b.l;
import c.u;
import c.x;
import com.truecaller.R.id;
import com.truecaller.bk;
import com.truecaller.premium.br;
import com.truecaller.ui.components.ComboBase;
import com.truecaller.ui.components.ComboBase.a;
import com.truecaller.ui.components.n;
import com.truecaller.util.dh;
import com.truecaller.utils.extensions.t;
import com.truecaller.wizard.utils.i;
import java.util.HashMap;
import java.util.List;
import javax.inject.Inject;

public final class CallRecordingSettingsFragment
  extends Fragment
  implements CallRecordingSettingsMvp.b
{
  @Inject
  public CallRecordingSettingsMvp.a a;
  @Inject
  public br b;
  @Inject
  public aj c;
  private HashMap d;
  
  private final m<CompoundButton, Boolean, x> f()
  {
    return (m)new j(this);
  }
  
  public final View a(int paramInt)
  {
    if (d == null) {
      d = new HashMap();
    }
    View localView2 = (View)d.get(Integer.valueOf(paramInt));
    View localView1 = localView2;
    if (localView2 == null)
    {
      localView1 = getView();
      if (localView1 == null) {
        return null;
      }
      localView1 = localView1.findViewById(paramInt);
      d.put(Integer.valueOf(paramInt), localView1);
    }
    return localView1;
  }
  
  public final CallRecordingSettingsMvp.a a()
  {
    CallRecordingSettingsMvp.a locala = a;
    if (locala == null) {
      k.a("presenter");
    }
    return locala;
  }
  
  public final void a(CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext)
  {
    k.b(paramCallRecordingOnBoardingLaunchContext, "launchContext");
    f localf = getActivity();
    if (localf != null)
    {
      if (c == null) {
        k.a("callRecordingOnBoardingNavigator");
      }
      k.a(localf, "it");
      aj.a((Context)localf, CallRecordingOnBoardingState.WHATS_NEW, paramCallRecordingOnBoardingLaunchContext);
      return;
    }
  }
  
  public final void a(n paramn)
  {
    k.b(paramn, "mode");
    ComboBase localComboBase = (ComboBase)a(R.id.settingsCallRecordingMode);
    k.a(localComboBase, "settingsCallRecordingMode");
    localComboBase.setSelection(paramn);
  }
  
  public final void a(CharSequence paramCharSequence)
  {
    k.b(paramCharSequence, "message");
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    k.a(localContext, "context ?: return");
    new AlertDialog.Builder(localContext).setTitle(2131887611).setMessage(paramCharSequence).setPositiveButton(2131887608, (DialogInterface.OnClickListener)new k(this)).setNegativeButton(2131887609, (DialogInterface.OnClickListener)new l(this)).setOnCancelListener((DialogInterface.OnCancelListener)new m(this)).show();
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "path");
    TextView localTextView = (TextView)a(R.id.settingRecordingStoragePathDescription);
    k.a(localTextView, "settingRecordingStoragePathDescription");
    localTextView.setText((CharSequence)paramString);
  }
  
  public final void a(List<? extends n> paramList1, List<? extends n> paramList2, List<? extends n> paramList3)
  {
    k.b(paramList1, "modeItems");
    k.b(paramList2, "filterItems");
    k.b(paramList3, "configItems");
    ((ComboBase)a(R.id.settingsCallRecordingMode)).setData(paramList1);
    ((ComboBase)a(R.id.settingsCallRecordingCallsFilter)).setData(paramList2);
    ((ComboBase)a(R.id.settingsCallRecordingConfiguration)).setData(paramList3);
  }
  
  public final void a(boolean paramBoolean)
  {
    ((SwitchCompat)a(R.id.settingRecordingEnabledSwitch)).setOnCheckedChangeListener(null);
    SwitchCompat localSwitchCompat = (SwitchCompat)a(R.id.settingRecordingEnabledSwitch);
    k.a(localSwitchCompat, "settingRecordingEnabledSwitch");
    localSwitchCompat.setChecked(paramBoolean);
    ((SwitchCompat)a(R.id.settingRecordingEnabledSwitch)).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)new bc(f()));
  }
  
  public final void a(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4)
  {
    Object localObject = a(R.id.settingsCallRecordingTroubleshootingContainer);
    if (localObject != null)
    {
      p.a((ViewGroup)localObject);
      localObject = (CardView)a(R.id.troubleshootingDrawOverOtherApps);
      k.a(localObject, "troubleshootingDrawOverOtherApps");
      t.a((View)localObject, paramBoolean1);
      localObject = (CardView)a(R.id.troubleshootingBatteryOptimisation);
      k.a(localObject, "troubleshootingBatteryOptimisation");
      t.a((View)localObject, paramBoolean2);
      localObject = (CardView)a(R.id.troubleshootingAllowMicrophone);
      k.a(localObject, "troubleshootingAllowMicrophone");
      t.a((View)localObject, paramBoolean3);
      localObject = (CardView)a(R.id.troubleshootingAllowStorage);
      k.a(localObject, "troubleshootingAllowStorage");
      t.a((View)localObject, paramBoolean4);
      return;
    }
    throw new u("null cannot be cast to non-null type android.view.ViewGroup");
  }
  
  public final void b()
  {
    Object localObject = (ComboBase)a(R.id.settingsCallRecordingMode);
    k.a(localObject, "settingsCallRecordingMode");
    ((ComboBase)localObject).setEnabled(false);
    localObject = (SwitchCompat)a(R.id.settingRecordingEnabledSwitch);
    k.a(localObject, "settingRecordingEnabledSwitch");
    ((SwitchCompat)localObject).setEnabled(false);
    localObject = (ComboBase)a(R.id.settingsCallRecordingMode);
    k.a(localObject, "settingsCallRecordingMode");
    ((ComboBase)localObject).setEnabled(false);
    localObject = (ComboBase)a(R.id.settingsCallRecordingConfiguration);
    k.a(localObject, "settingsCallRecordingConfiguration");
    ((ComboBase)localObject).setEnabled(false);
    localObject = (ComboBase)a(R.id.settingsCallRecordingCallsFilter);
    k.a(localObject, "settingsCallRecordingCallsFilter");
    ((ComboBase)localObject).setEnabled(false);
    localObject = (LinearLayout)a(R.id.settingCallRecordingEnaledSwitchHolder);
    k.a(localObject, "settingCallRecordingEnaledSwitchHolder");
    ((LinearLayout)localObject).setEnabled(false);
  }
  
  public final void b(n paramn)
  {
    k.b(paramn, "filter");
    ComboBase localComboBase = (ComboBase)a(R.id.settingsCallRecordingCallsFilter);
    k.a(localComboBase, "settingsCallRecordingCallsFilter");
    localComboBase.setSelection(paramn);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "permission");
    i.a((Fragment)this, paramString, 102, false);
  }
  
  public final void b(boolean paramBoolean)
  {
    CallRecordingPromoViewImpl localCallRecordingPromoViewImpl = (CallRecordingPromoViewImpl)a(R.id.callRecordingPromoView);
    k.a(localCallRecordingPromoViewImpl, "callRecordingPromoView");
    t.a((View)localCallRecordingPromoViewImpl, paramBoolean);
    if (paramBoolean)
    {
      localCallRecordingPromoViewImpl = (CallRecordingPromoViewImpl)a(R.id.callRecordingPromoView);
      ar localar = a;
      if (localar == null) {
        k.a("presenter");
      }
      localar.a((av)localCallRecordingPromoViewImpl);
    }
  }
  
  public final void c()
  {
    dh.a(requireContext(), "https://support.truecaller.com/hc/en-us/articles/360001264545", false);
  }
  
  public final void c(n paramn)
  {
    k.b(paramn, "config");
    ComboBase localComboBase = (ComboBase)a(R.id.settingsCallRecordingConfiguration);
    k.a(localComboBase, "settingsCallRecordingConfiguration");
    localComboBase.setSelection(paramn);
  }
  
  public final void c(boolean paramBoolean)
  {
    View localView = a(R.id.settingsCallRecordingTroubleshootingContainer);
    k.a(localView, "settingsCallRecordingTroubleshootingContainer");
    t.a(localView, paramBoolean);
  }
  
  public final void d()
  {
    i.a((Activity)requireActivity());
  }
  
  public final void e()
  {
    i.b((Activity)requireActivity());
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = bs.a();
    Object localObject = requireContext();
    k.a(localObject, "requireContext()");
    localObject = ((Context)localObject).getApplicationContext();
    if (localObject != null)
    {
      paramContext = paramContext.a(((bk)localObject).a());
      localObject = requireActivity();
      k.a(localObject, "requireActivity()");
      paramContext.a(new bd(((f)localObject).getIntent())).a().a(this);
      return;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.GraphHolder");
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    return paramLayoutInflater.inflate(2131558786, paramViewGroup, false);
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    Object localObject = a;
    if (localObject == null) {
      k.a("presenter");
    }
    ((CallRecordingSettingsMvp.a)localObject).y_();
    localObject = d;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    k.b(paramArrayOfString, "permissions");
    k.b(paramArrayOfInt, "grantResults");
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    CallRecordingSettingsMvp.a locala = a;
    if (locala == null) {
      k.a("presenter");
    }
    locala.a(paramInt, paramArrayOfString, paramArrayOfInt);
  }
  
  public final void onResume()
  {
    super.onResume();
    CallRecordingSettingsMvp.a locala = a;
    if (locala == null) {
      k.a("presenter");
    }
    locala.a();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    ((ComboBase)a(R.id.settingsCallRecordingMode)).a((ComboBase.a)new a(this));
    ((ComboBase)a(R.id.settingsCallRecordingCallsFilter)).a((ComboBase.a)new b(this));
    ((ComboBase)a(R.id.settingsCallRecordingConfiguration)).a((ComboBase.a)new c(this));
    ((SwitchCompat)a(R.id.settingRecordingEnabledSwitch)).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)new bc(f()));
    ((LinearLayout)a(R.id.settingCallRecordingEnaledSwitchHolder)).setOnClickListener((View.OnClickListener)new d(this));
    ((ComboBase)a(R.id.settingsCallRecordingMode)).setListItemLayoutRes(2131559029);
    ((ComboBase)a(R.id.settingsCallRecordingCallsFilter)).setListItemLayoutRes(2131559029);
    ((ComboBase)a(R.id.settingsCallRecordingConfiguration)).setListItemLayoutRes(2131559029);
    paramView = a;
    if (paramView == null) {
      k.a("presenter");
    }
    paramView.a(this);
    paramView = (CardView)a(R.id.settingsCallRecordingCallsFilterContainer);
    k.a(paramView, "settingsCallRecordingCallsFilterContainer");
    t.b((View)paramView);
    paramView = (CardView)a(R.id.settingsCallRecordingStoragePathContainer);
    k.a(paramView, "settingsCallRecordingStoragePathContainer");
    t.b((View)paramView);
    ((CardView)a(R.id.troubleshootingVisitHelpCenter)).setOnClickListener((View.OnClickListener)new e(this));
    ((CardView)a(R.id.troubleshootingDrawOverOtherApps)).setOnClickListener((View.OnClickListener)new f(this));
    ((CardView)a(R.id.troubleshootingBatteryOptimisation)).setOnClickListener((View.OnClickListener)new g(this));
    ((CardView)a(R.id.troubleshootingAllowStorage)).setOnClickListener((View.OnClickListener)new h(this));
    ((CardView)a(R.id.troubleshootingAllowMicrophone)).setOnClickListener((View.OnClickListener)new i(this));
  }
  
  static final class a
    implements ComboBase.a
  {
    a(CallRecordingSettingsFragment paramCallRecordingSettingsFragment) {}
    
    public final void onSelectionChanged(ComboBase paramComboBase)
    {
      CallRecordingSettingsMvp.a locala = a.a();
      k.a(paramComboBase, "it");
      paramComboBase = paramComboBase.getSelection();
      k.a(paramComboBase, "it.selection");
      locala.a(paramComboBase);
    }
  }
  
  static final class b
    implements ComboBase.a
  {
    b(CallRecordingSettingsFragment paramCallRecordingSettingsFragment) {}
    
    public final void onSelectionChanged(ComboBase paramComboBase)
    {
      CallRecordingSettingsMvp.a locala = a.a();
      k.a(paramComboBase, "it");
      paramComboBase = paramComboBase.getSelection();
      k.a(paramComboBase, "it.selection");
      locala.b(paramComboBase);
    }
  }
  
  static final class c
    implements ComboBase.a
  {
    c(CallRecordingSettingsFragment paramCallRecordingSettingsFragment) {}
    
    public final void onSelectionChanged(ComboBase paramComboBase)
    {
      CallRecordingSettingsMvp.a locala = a.a();
      k.a(paramComboBase, "it");
      paramComboBase = paramComboBase.getSelection();
      k.a(paramComboBase, "it.selection");
      locala.c(paramComboBase);
    }
  }
  
  static final class d
    implements View.OnClickListener
  {
    d(CallRecordingSettingsFragment paramCallRecordingSettingsFragment) {}
    
    public final void onClick(View paramView)
    {
      ((SwitchCompat)a.a(R.id.settingRecordingEnabledSwitch)).toggle();
    }
  }
  
  static final class e
    implements View.OnClickListener
  {
    e(CallRecordingSettingsFragment paramCallRecordingSettingsFragment) {}
    
    public final void onClick(View paramView)
    {
      a.a().b();
    }
  }
  
  static final class f
    implements View.OnClickListener
  {
    f(CallRecordingSettingsFragment paramCallRecordingSettingsFragment) {}
    
    public final void onClick(View paramView)
    {
      a.a().c();
    }
  }
  
  static final class g
    implements View.OnClickListener
  {
    g(CallRecordingSettingsFragment paramCallRecordingSettingsFragment) {}
    
    public final void onClick(View paramView)
    {
      a.a().e();
    }
  }
  
  static final class h
    implements View.OnClickListener
  {
    h(CallRecordingSettingsFragment paramCallRecordingSettingsFragment) {}
    
    public final void onClick(View paramView)
    {
      a.a().f();
    }
  }
  
  static final class i
    implements View.OnClickListener
  {
    i(CallRecordingSettingsFragment paramCallRecordingSettingsFragment) {}
    
    public final void onClick(View paramView)
    {
      a.a().g();
    }
  }
  
  static final class j
    extends l
    implements m<CompoundButton, Boolean, x>
  {
    j(CallRecordingSettingsFragment paramCallRecordingSettingsFragment)
    {
      super();
    }
  }
  
  static final class k
    implements DialogInterface.OnClickListener
  {
    k(CallRecordingSettingsFragment paramCallRecordingSettingsFragment) {}
    
    public final void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
      CallRecordingSettingsFragment.a(a, true);
    }
  }
  
  static final class l
    implements DialogInterface.OnClickListener
  {
    l(CallRecordingSettingsFragment paramCallRecordingSettingsFragment) {}
    
    public final void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
      CallRecordingSettingsFragment.a(a, false);
    }
  }
  
  static final class m
    implements DialogInterface.OnCancelListener
  {
    m(CallRecordingSettingsFragment paramCallRecordingSettingsFragment) {}
    
    public final void onCancel(DialogInterface paramDialogInterface)
    {
      CallRecordingSettingsFragment.a(a, false);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingSettingsFragment
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */