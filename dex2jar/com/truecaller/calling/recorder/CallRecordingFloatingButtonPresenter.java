package com.truecaller.calling.recorder;

import c.d.c;
import c.g.a.m;
import c.o.b;
import com.truecaller.bb;
import com.truecaller.data.entity.CallRecording;
import java.util.Timer;
import java.util.TimerTask;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;
import org.a.a.aa;
import org.a.a.ab;
import org.a.a.b;
import org.a.a.d.n;
import org.a.a.d.o;
import org.a.a.r;
import org.a.a.s;

public final class CallRecordingFloatingButtonPresenter
  extends bb<bw>
  implements f
{
  public String a;
  public CallRecordingButtonMode c;
  int d;
  public b e;
  public Timer f;
  final n g;
  public String h;
  public final CallRecordingManager i;
  public final com.truecaller.utils.a j;
  final c.d.f k;
  final c.d.f l;
  public final ba m;
  final bx n;
  
  @Inject
  public CallRecordingFloatingButtonPresenter(CallRecordingManager paramCallRecordingManager, com.truecaller.utils.a parama, @Named("UI") c.d.f paramf1, @Named("Async") c.d.f paramf2, ba paramba, bx parambx)
  {
    i = paramCallRecordingManager;
    j = parama;
    k = paramf1;
    l = paramf2;
    m = paramba;
    n = parambx;
    c = CallRecordingButtonMode.NOT_STARTED;
    paramCallRecordingManager = new o();
    b = 4;
    a = 2;
    paramCallRecordingManager.a(5);
    paramCallRecordingManager = paramCallRecordingManager.a(":", ":", true);
    b = 4;
    a = 2;
    paramCallRecordingManager.a(6);
    g = paramCallRecordingManager.a();
  }
  
  public final void a()
  {
    c = CallRecordingButtonMode.ENDED;
    Timer localTimer = f;
    if (localTimer != null) {
      localTimer.cancel();
    }
    f = null;
    a = null;
    e.b((ag)bg.a, k, (m)new d(this, null), 2);
  }
  
  public final void a(bw parambw)
  {
    c.g.b.k.b(parambw, "presenterView");
    super.a(parambw);
    i.a((f)this);
    i.a((bi)new b(this));
  }
  
  final void b()
  {
    a = i.b();
    if (c == CallRecordingButtonMode.RECORDING) {
      return;
    }
    c = CallRecordingButtonMode.RECORDING;
    c();
    e = i.c();
    Timer localTimer = c.c.a.a("CallRecorderCountUpTimer");
    localTimer.schedule((TimerTask)new a(this), 500L, 1000L);
    f = localTimer;
  }
  
  public final void c()
  {
    e.b((ag)bg.a, k, (m)new e(this, null), 2);
  }
  
  public final void y_()
  {
    super.y_();
    Timer localTimer = f;
    if (localTimer != null) {
      localTimer.cancel();
    }
    a = null;
    i.a(null);
  }
  
  public static enum CallRecordingButtonMode
  {
    static
    {
      CallRecordingButtonMode localCallRecordingButtonMode1 = new CallRecordingButtonMode("NOT_STARTED", 0);
      NOT_STARTED = localCallRecordingButtonMode1;
      CallRecordingButtonMode localCallRecordingButtonMode2 = new CallRecordingButtonMode("RECORDING", 1);
      RECORDING = localCallRecordingButtonMode2;
      CallRecordingButtonMode localCallRecordingButtonMode3 = new CallRecordingButtonMode("ENDED", 2);
      ENDED = localCallRecordingButtonMode3;
      $VALUES = new CallRecordingButtonMode[] { localCallRecordingButtonMode1, localCallRecordingButtonMode2, localCallRecordingButtonMode3 };
    }
    
    private CallRecordingButtonMode() {}
  }
  
  public static final class a
    extends TimerTask
  {
    public a(CallRecordingFloatingButtonPresenter paramCallRecordingFloatingButtonPresenter) {}
    
    public final void run()
    {
      CallRecordingFloatingButtonPresenter.a(a);
    }
  }
  
  public static final class b
    implements bi
  {
    public final void a()
    {
      a.b();
    }
  }
  
  public static final class c
    extends TimerTask
  {
    public c(CallRecordingFloatingButtonPresenter paramCallRecordingFloatingButtonPresenter) {}
    
    public final void run()
    {
      CallRecordingFloatingButtonPresenter.a(a);
    }
  }
  
  @c.d.b.a.f(b="CallRecordingFloatingButtonPresenter.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter$onError$1")
  static final class d
    extends c.d.b.a.k
    implements m<ag, c<? super c.x>, Object>
  {
    int a;
    private ag c;
    
    d(CallRecordingFloatingButtonPresenter paramCallRecordingFloatingButtonPresenter, c paramc)
    {
      super(paramc);
    }
    
    public final c<c.x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new d(b, paramc);
      c = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      c.d.a.a locala = c.d.a.a.a;
      if (a == 0)
      {
        if (!(paramObject instanceof o.b))
        {
          paramObject = (bw)b.b;
          if (paramObject != null) {
            ((bw)paramObject).d();
          }
          return c.x.a;
        }
        throw a;
      }
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((d)a(paramObject1, (c)paramObject2)).a(c.x.a);
    }
  }
  
  @c.d.b.a.f(b="CallRecordingFloatingButtonPresenter.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter$refreshView$1")
  static final class e
    extends c.d.b.a.k
    implements m<ag, c<? super c.x>, Object>
  {
    int a;
    private ag c;
    
    e(CallRecordingFloatingButtonPresenter paramCallRecordingFloatingButtonPresenter, c paramc)
    {
      super(paramc);
    }
    
    public final c<c.x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new e(b, paramc);
      c = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      c.d.a.a locala = c.d.a.a.a;
      if (a == 0)
      {
        if (!(paramObject instanceof o.b))
        {
          paramObject = b.c;
          switch (k.b[paramObject.ordinal()])
          {
          default: 
            break;
          case 3: 
            paramObject = (bw)b.b;
            if (paramObject != null) {
              ((bw)paramObject).c();
            }
            break;
          case 2: 
            paramObject = (bw)b.b;
            if (paramObject != null) {
              ((bw)paramObject).b();
            }
            break;
          case 1: 
            paramObject = (bw)b.b;
            if (paramObject != null) {
              ((bw)paramObject).a();
            }
            break;
          }
          return c.x.a;
        }
        throw a;
      }
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((e)a(paramObject1, (c)paramObject2)).a(c.x.a);
    }
  }
  
  @c.d.b.a.f(b="CallRecordingFloatingButtonPresenter.kt", c={139}, d="invokeSuspend", e="com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter$setRecording$1")
  static final class f
    extends c.d.b.a.k
    implements m<ag, c<? super c.x>, Object>
  {
    Object a;
    Object b;
    Object c;
    int d;
    private ag g;
    
    f(CallRecordingFloatingButtonPresenter paramCallRecordingFloatingButtonPresenter, CallRecording paramCallRecording, c paramc)
    {
      super(paramc);
    }
    
    public final c<c.x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new f(e, f, paramc);
      g = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      Object localObject1 = c.d.a.a.a;
      bw localbw;
      Object localObject2;
      switch (d)
      {
      default: 
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
      case 1: 
        localObject1 = (n)c;
        localbw = (bw)b;
        if (!(paramObject instanceof o.b)) {
          localObject2 = paramObject;
        } else {
          throw a;
        }
        break;
      case 0: 
        if ((paramObject instanceof o.b)) {
          break label216;
        }
        localObject2 = com.truecaller.data.entity.a.a(f, e.n, e.l);
        localbw = (bw)e.b;
        if (localbw == null) {
          break label212;
        }
        paramObject = e.g;
        a = localObject2;
        b = localbw;
        c = paramObject;
        d = 1;
        localObject2 = ((ao)localObject2).a(this);
        if (localObject2 == localObject1) {
          return localObject1;
        }
        localObject1 = paramObject;
      }
      paramObject = ((n)localObject1).a((aa)r.b((int)((Number)localObject2).longValue()).a(s.a()));
      c.g.b.k.a(paramObject, "periodFormatter.print(Pe…()).normalizedStandard())");
      localbw.setLabel((String)paramObject);
      label212:
      return c.x.a;
      label216:
      throw a;
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((f)a(paramObject1, (c)paramObject2)).a(c.x.a);
    }
  }
  
  @c.d.b.a.f(b="CallRecordingFloatingButtonPresenter.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter$setSource$1")
  static final class g
    extends c.d.b.a.k
    implements m<ag, c<? super c.x>, Object>
  {
    int a;
    private ag c;
    
    g(CallRecordingFloatingButtonPresenter paramCallRecordingFloatingButtonPresenter, c paramc)
    {
      super(paramc);
    }
    
    public final c<c.x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new g(b, paramc);
      c = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      Object localObject = c.d.a.a.a;
      if (a == 0)
      {
        if (!(paramObject instanceof o.b))
        {
          paramObject = (bw)b.b;
          if (paramObject != null)
          {
            localObject = b.g.a((aa)r.a(0).a(s.a()));
            c.g.b.k.a(localObject, "periodFormatter.print(Pe…(0).normalizedStandard())");
            ((bw)paramObject).setLabel((String)localObject);
          }
          return c.x.a;
        }
        throw a;
      }
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((g)a(paramObject1, (c)paramObject2)).a(c.x.a);
    }
  }
  
  @c.d.b.a.f(b="CallRecordingFloatingButtonPresenter.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter$updateTimerLabel$1")
  static final class h
    extends c.d.b.a.k
    implements m<ag, c<? super c.x>, Object>
  {
    int a;
    private ag c;
    
    h(CallRecordingFloatingButtonPresenter paramCallRecordingFloatingButtonPresenter, c paramc)
    {
      super(paramc);
    }
    
    public final c<c.x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new h(b, paramc);
      c = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      Object localObject1 = c.d.a.a.a;
      if (a == 0)
      {
        if (!(paramObject instanceof o.b))
        {
          Object localObject2 = b.e;
          if (localObject2 != null)
          {
            paramObject = (bw)b.b;
            if (paramObject != null)
            {
              localObject1 = b.g;
              localObject2 = ab.a((org.a.a.x)localObject2, (org.a.a.x)new b(b.j.a()));
              c.g.b.k.a(localObject2, "Seconds.secondsBetween(i…ock.currentTimeMillis()))");
              localObject1 = ((n)localObject1).a((aa)r.a(((ab)localObject2).c()).a(s.a()));
              c.g.b.k.a(localObject1, "periodFormatter.print(\n …ndard()\n                )");
              ((bw)paramObject).setLabel((String)localObject1);
            }
          }
          return c.x.a;
        }
        throw a;
      }
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((h)a(paramObject1, (c)paramObject2)).a(c.x.a);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */