package com.truecaller.calling.recorder;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.k;
import com.truecaller.callhistory.r;
import com.truecaller.calling.dialer.bu;
import com.truecaller.calling.dialer.v;
import com.truecaller.utils.n;
import dagger.a.d;
import javax.inject.Provider;

public final class bp
  implements d<bm>
{
  private final Provider<f<r>> a;
  private final Provider<bu> b;
  private final Provider<n> c;
  private final Provider<v> d;
  private final Provider<bv> e;
  private final Provider<k> f;
  
  private bp(Provider<f<r>> paramProvider, Provider<bu> paramProvider1, Provider<n> paramProvider2, Provider<v> paramProvider3, Provider<bv> paramProvider4, Provider<k> paramProvider5)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
  }
  
  public static bp a(Provider<f<r>> paramProvider, Provider<bu> paramProvider1, Provider<n> paramProvider2, Provider<v> paramProvider3, Provider<bv> paramProvider4, Provider<k> paramProvider5)
  {
    return new bp(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.bp
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */