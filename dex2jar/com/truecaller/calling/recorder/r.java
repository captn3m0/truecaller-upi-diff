package com.truecaller.calling.recorder;

import android.content.Context;
import android.content.res.Resources;
import dagger.a.d;
import javax.inject.Provider;

public final class r
  implements d<Resources>
{
  private final Provider<Context> a;
  
  private r(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static r a(Provider<Context> paramProvider)
  {
    return new r(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.r
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */