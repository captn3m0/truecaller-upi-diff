package com.truecaller.calling.recorder;

public enum CallRecordingSettingsMvp$CallsFilter
{
  static
  {
    CallsFilter localCallsFilter1 = new CallsFilter("ALL_CALLS", 0);
    ALL_CALLS = localCallsFilter1;
    CallsFilter localCallsFilter2 = new CallsFilter("UNKNOWN_NUMBERS", 1);
    UNKNOWN_NUMBERS = localCallsFilter2;
    CallsFilter localCallsFilter3 = new CallsFilter("SELECTED_CONTACTS", 2);
    SELECTED_CONTACTS = localCallsFilter3;
    $VALUES = new CallsFilter[] { localCallsFilter1, localCallsFilter2, localCallsFilter3 };
  }
  
  private CallRecordingSettingsMvp$CallsFilter() {}
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingSettingsMvp.CallsFilter
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */