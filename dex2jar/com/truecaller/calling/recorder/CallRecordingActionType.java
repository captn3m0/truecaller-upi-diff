package com.truecaller.calling.recorder;

public enum CallRecordingActionType
{
  private final String eventAction;
  
  static
  {
    CallRecordingActionType localCallRecordingActionType1 = new CallRecordingActionType("PLAY_CALL_RECORDING", 0, "ItemEvent.ACTION_PLAY_CALL_RECORDING");
    PLAY_CALL_RECORDING = localCallRecordingActionType1;
    CallRecordingActionType localCallRecordingActionType2 = new CallRecordingActionType("SHOW_CALL_RECORDING_MENU_OPTIONS", 1, "ItemEvent.ACTION_SHOW_CALL_RECORDING_MENU_OPTIONS");
    SHOW_CALL_RECORDING_MENU_OPTIONS = localCallRecordingActionType2;
    $VALUES = new CallRecordingActionType[] { localCallRecordingActionType1, localCallRecordingActionType2 };
  }
  
  private CallRecordingActionType(String paramString)
  {
    eventAction = paramString;
  }
  
  public final String getEventAction()
  {
    return eventAction;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingActionType
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */