package com.truecaller.calling.recorder;

import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.PopupMenu.OnMenuItemClickListener;
import android.view.MenuItem;
import android.view.View;
import c.g.b.k;
import javax.inject.Inject;

public final class y
  implements x
{
  public final void a(final int paramInt, View paramView, ca paramca)
  {
    k.b(paramView, "anchorView");
    k.b(paramca, "clickListener");
    paramView = new PopupMenu(paramView.getContext(), paramView);
    paramView.setOnMenuItemClickListener((PopupMenu.OnMenuItemClickListener)new a(paramca, paramInt));
    paramView.inflate(2131623939);
    paramView.show();
  }
  
  static final class a
    implements PopupMenu.OnMenuItemClickListener
  {
    a(ca paramca, int paramInt) {}
    
    public final boolean onMenuItemClick(MenuItem paramMenuItem)
    {
      k.a(paramMenuItem, "item");
      int i = paramMenuItem.getItemId();
      if (i != 2131363506)
      {
        if (i != 2131363508)
        {
          if (i != 2131363512) {
            return false;
          }
          a.c(paramInt);
          return true;
        }
        a.a(paramInt);
        return true;
      }
      a.b(paramInt);
      return true;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.y
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */