package com.truecaller.calling.recorder;

import android.content.Context;
import com.truecaller.bp;
import com.truecaller.common.g.a;
import com.truecaller.premium.br;
import com.truecaller.utils.d;
import com.truecaller.utils.l;
import com.truecaller.utils.n;
import com.truecaller.wizard.utils.PermissionPoller;
import dagger.a.g;
import javax.inject.Provider;

public final class bs
  implements bb
{
  private final bp a;
  private Provider<a> b;
  private Provider<com.truecaller.common.f.c> c;
  private Provider<CallRecordingManager> d;
  private Provider<n> e;
  private Provider<l> f;
  private Provider<d> g;
  private Provider<Context> h;
  private Provider<PermissionPoller> i;
  private Provider<h> j;
  private Provider<bg> k;
  private Provider<CallRecordingSettingsMvp.a> l;
  
  private bs(bd parambd, bp parambp)
  {
    a = parambp;
    b = new e(parambp);
    c = new h(parambp);
    d = new d(parambp);
    e = new i(parambp);
    f = new g(parambp);
    g = new f(parambp);
    h = new b(parambp);
    i = dagger.a.c.a(be.a(parambd, h));
    j = new c(parambp);
    k = bh.a(b, c, d, e, f, g, i, j);
    l = dagger.a.c.a(k);
  }
  
  public static a a()
  {
    return new a((byte)0);
  }
  
  public final void a(CallRecordingSettingsFragment paramCallRecordingSettingsFragment)
  {
    a = ((CallRecordingSettingsMvp.a)l.get());
    b = ((br)g.a(a.bF(), "Cannot return null from a non-@Nullable component method"));
    c = ((aj)g.a(a.bU(), "Cannot return null from a non-@Nullable component method"));
  }
  
  public static final class a
  {
    private bd a;
    private bp b;
    
    public final bb a()
    {
      g.a(a, bd.class);
      g.a(b, bp.class);
      return new bs(a, b, (byte)0);
    }
    
    public final a a(bp parambp)
    {
      b = ((bp)g.a(parambp));
      return this;
    }
    
    public final a a(bd parambd)
    {
      a = ((bd)g.a(parambd));
      return this;
    }
  }
  
  static final class b
    implements Provider<Context>
  {
    private final bp a;
    
    b(bp parambp)
    {
      a = parambp;
    }
  }
  
  static final class c
    implements Provider<h>
  {
    private final bp a;
    
    c(bp parambp)
    {
      a = parambp;
    }
  }
  
  static final class d
    implements Provider<CallRecordingManager>
  {
    private final bp a;
    
    d(bp parambp)
    {
      a = parambp;
    }
  }
  
  static final class e
    implements Provider<a>
  {
    private final bp a;
    
    e(bp parambp)
    {
      a = parambp;
    }
  }
  
  static final class f
    implements Provider<d>
  {
    private final bp a;
    
    f(bp parambp)
    {
      a = parambp;
    }
  }
  
  static final class g
    implements Provider<l>
  {
    private final bp a;
    
    g(bp parambp)
    {
      a = parambp;
    }
  }
  
  static final class h
    implements Provider<com.truecaller.common.f.c>
  {
    private final bp a;
    
    h(bp parambp)
    {
      a = parambp;
    }
  }
  
  static final class i
    implements Provider<n>
  {
    private final bp a;
    
    i(bp parambp)
    {
      a = parambp;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.bs
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */