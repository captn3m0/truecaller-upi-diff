package com.truecaller.calling.recorder;

import android.content.Context;
import android.net.Uri;
import android.net.Uri.Builder;
import android.telephony.TelephonyManager;
import c.o.b;
import com.truecaller.analytics.e.a;
import com.truecaller.callrecording.CallRecorder;
import com.truecaller.callrecording.CallRecorder.RecordingState;
import com.truecaller.callrecording.CallRecorder.a;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.log.AssertionUtil;
import com.truecaller.old.data.access.Settings;
import com.truecaller.util.aq;
import com.truecaller.utils.l;
import com.truecaller.utils.n;
import com.truecaller.wizard.utils.i;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

public final class ae
  implements CallRecordingManager, g, CallRecorder.a
{
  CallRecorder a;
  String b;
  org.a.a.b c;
  bi d;
  final org.a.a.d.b e;
  Timer f;
  final com.truecaller.androidactors.f<com.truecaller.callerid.e> g;
  final ax h;
  final com.truecaller.utils.a i;
  final TelephonyManager j;
  final long k;
  private final String[] l;
  private f m;
  private long n;
  private final Context o;
  private final com.truecaller.common.g.a p;
  private final aq q;
  private final h r;
  private final u s;
  private final com.truecaller.common.f.c t;
  private final c.d.f u;
  private final com.truecaller.analytics.b v;
  private final l w;
  private final n x;
  private final aj y;
  private final cd z;
  
  @Inject
  public ae(Context paramContext, com.truecaller.common.g.a parama, com.truecaller.androidactors.f<com.truecaller.callerid.e> paramf, ax paramax, com.truecaller.utils.a parama1, aq paramaq, h paramh, u paramu, com.truecaller.common.f.c paramc, @Named("Async") c.d.f paramf1, com.truecaller.analytics.b paramb, l paraml, n paramn, TelephonyManager paramTelephonyManager, aj paramaj, cd paramcd, @Named("safe_call_recording_closer_duration") long paramLong)
  {
    o = paramContext;
    p = parama;
    g = paramf;
    h = paramax;
    i = parama1;
    q = paramaq;
    r = paramh;
    s = paramu;
    t = paramc;
    u = paramf1;
    v = paramb;
    w = paraml;
    x = paramn;
    j = paramTelephonyManager;
    y = paramaj;
    z = paramcd;
    k = paramLong;
    l = new String[] { "android.permission.RECORD_AUDIO", "android.permission.CALL_PHONE", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_PHONE_STATE" };
    e = org.a.a.d.a.a("yyMMdd-HHmmss");
  }
  
  private final void b(e.a parama)
  {
    if (t.d()) {
      localObject = "Premium";
    } else {
      localObject = "Free";
    }
    parama.a("PremiumStatus", (String)localObject);
    parama.a("TrialStatus", n().name());
    if (c.n.m.a(p.a("callRecordingMode"), CallRecordingSettingsMvp.RecordingModes.AUTO.name(), true)) {
      localObject = "Auto";
    } else {
      localObject = "Manual";
    }
    parama.a("RecordingMode", (String)localObject);
    if (r.d() == CallRecordingSettingsMvp.Configuration.SDK_MEDIA_RECORDER) {
      localObject = "SdkConfig";
    } else {
      localObject = "Default";
    }
    parama.a("RecordingConfig", (String)localObject);
    Object localObject = v;
    parama = parama.a();
    c.g.b.k.a(parama, "eventBuilder.build()");
    ((com.truecaller.analytics.b)localObject).a(parama);
  }
  
  public final void a(e.a parama)
  {
    c.g.b.k.b(parama, "eventBuilder");
    b(parama);
  }
  
  public final void a(bi parambi)
  {
    d = parambi;
  }
  
  public final void a(f paramf)
  {
    c.g.b.k.b(paramf, "listener");
    m = paramf;
  }
  
  public final void a(CallRecording paramCallRecording, CallRecordingManager.PlaybackLaunchContext paramPlaybackLaunchContext)
  {
    c.g.b.k.b(paramCallRecording, "callRecording");
    c.g.b.k.b(paramPlaybackLaunchContext, "playbackLaunchContext");
    if (a(CallRecordingOnBoardingLaunchContext.UNKNOWN)) {
      return;
    }
    Object localObject = s;
    if (!((u)localObject).a(((u)localObject).a(c)))
    {
      paramCallRecording = z;
      localObject = x.a(2131887612, new Object[0]);
      c.g.b.k.a(localObject, "resourceProvider.getStri…r_no_activity_found_play)");
      paramCallRecording.toast((String)localObject);
    }
    else
    {
      paramCallRecording = z;
      localObject = x.a(2131887616, new Object[0]);
      c.g.b.k.a(localObject, "resourceProvider.getStri…ecording_toast_item_play)");
      paramCallRecording.toast((String)localObject);
    }
    paramCallRecording = new e.a("CallRecordingPlayback").a("Source", paramPlaybackLaunchContext.name());
    c.g.b.k.a(paramCallRecording, "AnalyticsEvent.Builder(C…aybackLaunchContext.name)");
    b(paramCallRecording);
  }
  
  public final void a(final String paramString)
  {
    if (!a()) {
      return;
    }
    try
    {
      if ((!q.a(k())) && (!q.b(k())))
      {
        AssertionUtil.reportWeirdnessButNeverCrash("Failed to create recording directory");
        return;
      }
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append(k());
      ((StringBuilder)localObject).append("/.nomedia");
      localObject = ((StringBuilder)localObject).toString();
      if ((!q.d((String)localObject)) && (!q.e((String)localObject))) {
        AssertionUtil.reportWeirdnessButNeverCrash("Failed to create nomedia file");
      }
      kotlinx.coroutines.e.b((ag)bg.a, u, (c.g.a.m)new a(this, paramString, null), 2);
      return;
    }
    catch (SecurityException paramString)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramString);
    }
  }
  
  public final boolean a()
  {
    if ((r.a()) && (!g()) && ((n().isActive()) || (t.d())) && (p.b("callRecordingEnbaled")))
    {
      String[] arrayOfString = l;
      int i2 = arrayOfString.length;
      int i1 = 0;
      while (i1 < i2)
      {
        String str = arrayOfString[i1];
        if (!w.a(new String[] { str }))
        {
          i1 = 0;
          break label111;
        }
        i1 += 1;
      }
      i1 = 1;
      label111:
      if (i1 != 0) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    c.g.b.k.b(paramArrayOfString, "permissions");
    c.g.b.k.b(paramArrayOfInt, "grantResults");
    if (paramInt == 102)
    {
      i.a(paramArrayOfString, paramArrayOfInt);
      return true;
    }
    return false;
  }
  
  public final boolean a(CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext)
  {
    c.g.b.k.b(paramCallRecordingOnBoardingLaunchContext, "launchContext");
    if ((r.c()) && (!t.d()) && (n() == FreeTrialStatus.EXPIRED))
    {
      aj.a(o, CallRecordingOnBoardingState.PAY_WALL_ON_EXPIRY, paramCallRecordingOnBoardingLaunchContext);
      return true;
    }
    return false;
  }
  
  public final String b()
  {
    CallRecorder localCallRecorder = a;
    if (localCallRecorder != null) {
      return localCallRecorder.getOutputFile();
    }
    return null;
  }
  
  public final org.a.a.b c()
  {
    return c;
  }
  
  public final boolean d()
  {
    return c.n.m.a(p.a("callRecordingMode"), CallRecordingSettingsMvp.RecordingModes.AUTO.name(), true);
  }
  
  public final void e()
  {
    Object localObject = a;
    if (localObject != null) {
      localObject = ((CallRecorder)localObject).getRecordingState();
    } else {
      localObject = null;
    }
    if (localObject != CallRecorder.RecordingState.RECORDING) {
      return;
    }
    l();
  }
  
  public final boolean f()
  {
    CallRecorder localCallRecorder = a;
    if (localCallRecorder != null) {
      return localCallRecorder.isRecording();
    }
    return false;
  }
  
  public final boolean g()
  {
    return (r.b()) && (!t.d()) && (!n().isActive());
  }
  
  public final void h()
  {
    CallRecorder localCallRecorder = a;
    if (localCallRecorder != null) {
      localCallRecorder.setOutputFile(null);
    }
    b = null;
    c = null;
    n = 0L;
  }
  
  public final boolean i()
  {
    String str = b;
    if (str != null)
    {
      if (d())
      {
        int i1;
        if (((CharSequence)str).length() > 0) {
          i1 = 1;
        } else {
          i1 = 0;
        }
        if ((i1 != 0) && (c != null) && (n < TimeUnit.SECONDS.toMillis(4L)))
        {
          q.c(str);
          b = null;
          return true;
        }
      }
      return false;
    }
    return false;
  }
  
  public final boolean j()
  {
    return Settings.e("qaEnableRecorderLeak");
  }
  
  public final String k()
  {
    String str = p.b("callRecordingStoragePath", bf.a());
    c.g.b.k.a(str, "coreSettings.getString(\n…ngStoragePath()\n        )");
    return str;
  }
  
  final void l()
  {
    ((com.truecaller.callerid.e)g.a()).d();
    Object localObject = f;
    if (localObject != null) {
      ((Timer)localObject).cancel();
    }
    f = null;
    try
    {
      localObject = a;
      if (localObject != null) {
        ((CallRecorder)localObject).stop();
      }
      localObject = a;
      if (localObject != null) {
        ((CallRecorder)localObject).release();
      }
      localObject = a;
      if (localObject != null) {
        ((CallRecorder)localObject).reset();
      }
      localObject = c;
      if (localObject != null) {
        n = (i.a() - a);
      }
      return;
    }
    catch (Exception localException) {}
  }
  
  public final int m()
  {
    long l1 = p.a("key_call_recording_trial_start_timestamp", -1L);
    if (l1 == -1L) {
      return -1;
    }
    Object localObject = new org.a.a.b(l1).a(14);
    org.a.a.b localb = new org.a.a.b(i.a());
    if (localb.b((org.a.a.x)localObject)) {
      return -1;
    }
    localObject = org.a.a.g.a((org.a.a.x)localb.az_(), (org.a.a.x)((org.a.a.b)localObject).az_());
    c.g.b.k.a(localObject, "Days.daysBetween(today.w…e.withTimeAtStartOfDay())");
    return ((org.a.a.g)localObject).c();
  }
  
  public final FreeTrialStatus n()
  {
    if (p.a("key_call_recording_trial_start_timestamp", -1L) == -1L) {
      return FreeTrialStatus.NOT_STARTED;
    }
    if (m() == -1) {
      return FreeTrialStatus.EXPIRED;
    }
    return FreeTrialStatus.ACTIVE;
  }
  
  public final void onError(Exception paramException)
  {
    c.g.b.k.b(paramException, "e");
    b = null;
    l();
    paramException = m;
    if (paramException != null)
    {
      paramException.a();
      return;
    }
  }
  
  @c.d.b.a.f(b="CallRecordingManager.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.recorder.CallRecordingManagerImpl$startRecording$1")
  static final class a
    extends c.d.b.a.k
    implements c.g.a.m<ag, c.d.c<? super c.x>, Object>
  {
    int a;
    private ag d;
    
    a(ae paramae, String paramString, c.d.c paramc)
    {
      super(paramc);
    }
    
    public final c.d.c<c.x> a(Object paramObject, c.d.c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new a(b, paramString, paramc);
      d = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      Object localObject = c.d.a.a.a;
      if (a == 0)
      {
        if (!(paramObject instanceof o.b))
        {
          ae localae = b;
          localObject = paramString;
          try
          {
            a = h.a((CallRecorder.a)localae);
            paramObject = a;
            if (paramObject != null) {
              ((CallRecorder)paramObject).setErrorListener((CallRecorder.a)localae);
            }
            Uri.Builder localBuilder = Uri.parse(localae.k()).buildUpon();
            StringBuilder localStringBuilder = new StringBuilder("TC-");
            localStringBuilder.append(e.a(org.a.a.f.a).a(i.a()));
            localStringBuilder.append('-');
            paramObject = localObject;
            if (localObject == null) {
              paramObject = "unknown";
            }
            localStringBuilder.append((String)paramObject);
            localStringBuilder.append(".m4a");
            b = localBuilder.appendPath(localStringBuilder.toString()).toString();
            paramObject = a;
            if (paramObject != null) {
              ((CallRecorder)paramObject).setOutputFile(b);
            }
            paramObject = a;
            if (paramObject != null) {
              ((CallRecorder)paramObject).prepare();
            }
            paramObject = a;
            if (paramObject != null) {
              ((CallRecorder)paramObject).start();
            }
            c = new org.a.a.b();
            paramObject = d;
            if (paramObject != null) {
              ((bi)paramObject).a();
            }
            if (a != null) {
              ((com.truecaller.callerid.e)g.a()).c();
            }
            long l1 = k;
            long l2 = k;
            paramObject = c.c.a.a("SafeRecordingCloser");
            ((Timer)paramObject).schedule((TimerTask)new ae.b(localae), l1, l2);
            f = ((Timer)paramObject);
          }
          catch (Exception paramObject)
          {
            localae.onError((Exception)paramObject);
            AssertionUtil.reportThrowableButNeverCrash((Throwable)paramObject);
          }
          return c.x.a;
        }
        throw a;
      }
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((a)a(paramObject1, (c.d.c)paramObject2)).a(c.x.a);
    }
  }
  
  public static final class b
    extends TimerTask
  {
    public b(ae paramae) {}
    
    public final void run()
    {
      if (a.j.getCallState() == 0)
      {
        a.l();
        a.h();
        AssertionUtil.reportThrowableButNeverCrash((Throwable)new IllegalStateException("Call recording hasn't ended after call. Ending it forcefully!"));
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.ae
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */