package com.truecaller.calling.recorder;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import com.truecaller.data.entity.CallRecording;

public final class d
  implements c
{
  private final v a;
  
  public d(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return c.class.equals(paramClass);
  }
  
  public final w<Long> a(CallRecording paramCallRecording)
  {
    return w.a(a, new a(new e(), paramCallRecording, (byte)0));
  }
  
  static final class a
    extends u<c, Long>
  {
    private final CallRecording b;
    
    private a(e parame, CallRecording paramCallRecording)
    {
      super();
      b = paramCallRecording;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".getDuration(");
      localStringBuilder.append(a(b, 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */