package com.truecaller.calling.recorder;

public enum CallRecordingSettingsMvp$RecordingModes
{
  static
  {
    RecordingModes localRecordingModes1 = new RecordingModes("AUTO", 0);
    AUTO = localRecordingModes1;
    RecordingModes localRecordingModes2 = new RecordingModes("MANUAL", 1);
    MANUAL = localRecordingModes2;
    $VALUES = new RecordingModes[] { localRecordingModes1, localRecordingModes2 };
  }
  
  private CallRecordingSettingsMvp$RecordingModes() {}
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingSettingsMvp.RecordingModes
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */