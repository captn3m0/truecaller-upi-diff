package com.truecaller.calling.recorder;

import android.telephony.TelephonyManager;
import com.truecaller.log.AssertionUtil;
import java.util.TimerTask;

public final class ae$b
  extends TimerTask
{
  public ae$b(ae paramae) {}
  
  public final void run()
  {
    if (a.j.getCallState() == 0)
    {
      a.l();
      a.h();
      AssertionUtil.reportThrowableButNeverCrash((Throwable)new IllegalStateException("Call recording hasn't ended after call. Ending it forcefully!"));
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.ae.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */