package com.truecaller.calling.recorder;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class t
  implements d<cd>
{
  private final Provider<Context> a;
  
  private t(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static t a(Provider<Context> paramProvider)
  {
    return new t(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.t
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */