package com.truecaller.calling.recorder;

import c.d.a.a;
import c.d.b.a.f;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;

@f(b="CallRecordingFloatingButtonPresenter.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter$refreshView$1")
final class CallRecordingFloatingButtonPresenter$e
  extends c.d.b.a.k
  implements m<ag, c<? super x>, Object>
{
  int a;
  private ag c;
  
  CallRecordingFloatingButtonPresenter$e(CallRecordingFloatingButtonPresenter paramCallRecordingFloatingButtonPresenter, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new e(b, paramc);
    c = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    if (a == 0)
    {
      if (!(paramObject instanceof o.b))
      {
        paramObject = b.c;
        switch (k.b[paramObject.ordinal()])
        {
        default: 
          break;
        case 3: 
          paramObject = (bw)b.b;
          if (paramObject != null) {
            ((bw)paramObject).c();
          }
          break;
        case 2: 
          paramObject = (bw)b.b;
          if (paramObject != null) {
            ((bw)paramObject).b();
          }
          break;
        case 1: 
          paramObject = (bw)b.b;
          if (paramObject != null) {
            ((bw)paramObject).a();
          }
          break;
        }
        return x.a;
      }
      throw a;
    }
    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((e)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */