package com.truecaller.calling.recorder;

import com.truecaller.common.f.c;
import com.truecaller.utils.n;
import dagger.a.d;
import javax.inject.Provider;

public final class au
  implements d<as>
{
  private final Provider<bv> a;
  private final Provider<n> b;
  private final Provider<cb> c;
  private final Provider<bu> d;
  private final Provider<c> e;
  private final Provider<bk> f;
  private final Provider<CallRecordingManager> g;
  
  private au(Provider<bv> paramProvider, Provider<n> paramProvider1, Provider<cb> paramProvider2, Provider<bu> paramProvider3, Provider<c> paramProvider4, Provider<bk> paramProvider5, Provider<CallRecordingManager> paramProvider6)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
  }
  
  public static au a(Provider<bv> paramProvider, Provider<n> paramProvider1, Provider<cb> paramProvider2, Provider<bu> paramProvider3, Provider<c> paramProvider4, Provider<bk> paramProvider5, Provider<CallRecordingManager> paramProvider6)
  {
    return new au(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.au
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */