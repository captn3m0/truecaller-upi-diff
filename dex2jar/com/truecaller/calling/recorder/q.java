package com.truecaller.calling.recorder;

import com.truecaller.androidactors.i;
import com.truecaller.androidactors.k;
import dagger.a.d;
import javax.inject.Provider;

public final class q
  implements d<i>
{
  private final Provider<k> a;
  
  private q(Provider<k> paramProvider)
  {
    a = paramProvider;
  }
  
  public static q a(Provider<k> paramProvider)
  {
    return new q(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */