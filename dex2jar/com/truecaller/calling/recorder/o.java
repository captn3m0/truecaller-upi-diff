package com.truecaller.calling.recorder;

import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d<com.truecaller.callrecording.a>
{
  private final Provider<com.truecaller.common.g.a> a;
  
  private o(Provider<com.truecaller.common.g.a> paramProvider)
  {
    a = paramProvider;
  }
  
  public static o a(Provider<com.truecaller.common.g.a> paramProvider)
  {
    return new o(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */