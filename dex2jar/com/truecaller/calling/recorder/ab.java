package com.truecaller.calling.recorder;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import com.truecaller.adapter_delegates.i;
import com.truecaller.calling.ActionType;
import com.truecaller.calling.a;
import com.truecaller.calling.an;
import com.truecaller.calling.ao;
import com.truecaller.calling.au;
import com.truecaller.calling.av;
import com.truecaller.calling.az;
import com.truecaller.calling.b;
import com.truecaller.calling.ba;
import com.truecaller.calling.bc;
import com.truecaller.calling.bd;
import com.truecaller.calling.c;
import com.truecaller.calling.d;
import com.truecaller.calling.dialer.CallIconType;
import com.truecaller.calling.e;
import com.truecaller.calling.h;
import com.truecaller.calling.o;
import com.truecaller.calling.p;
import com.truecaller.search.local.model.c.a;
import com.truecaller.ui.q.a;

public final class ab
  extends RecyclerView.ViewHolder
  implements a, an, ao, az, b, bc, bd, c, e, h, p, aa, q.a
{
  private final View a;
  
  private ab(View paramView, com.truecaller.adapter_delegates.k paramk, av paramav, ba paramba, o paramo)
  {
    super(paramView);
    b = paramo;
    c = paramo;
    d = paramo;
    e = paramo;
    f = paramo;
    g = paramba;
    h = paramba;
    i = paramav;
    j = new d(paramView);
    k = new bz(paramView);
    l = new cc(paramView);
    m = new au();
    a = paramView;
    paramav = a;
    paramView = (RecyclerView.ViewHolder)this;
    i.a(paramav, paramk, paramView, null, 12);
    i.a(paramav, paramk, paramView);
    c.g.b.k.b(paramk, "eventReceiver");
    c.g.b.k.b(paramView, "holder");
    paramav = l;
    c.g.b.k.b(paramk, "eventReceiver");
    c.g.b.k.b(paramView, "holder");
    paramba = paramav.a();
    c.g.b.k.a(paramba, "actionTwoView");
    i.a((View)paramba, paramk, paramView, CallRecordingActionType.SHOW_CALL_RECORDING_MENU_OPTIONS.getEventAction(), 8);
    paramav.b().setOnClickListener((View.OnClickListener)new cc.a(paramav));
    c.g.b.k.b(paramk, "eventReceiver");
    c.g.b.k.b(paramView, "holder");
    d.a(paramk, paramView);
    c.g.b.k.b(paramk, "eventReceiver");
    c.g.b.k.b(paramView, "holder");
    paramav = k;
    c.g.b.k.b(paramk, "eventReceiver");
    c.g.b.k.b(paramView, "holder");
    paramba = paramav.a();
    c.g.b.k.a(paramba, "actionOneView");
    i.a((View)paramba, paramk, paramView, CallRecordingActionType.PLAY_CALL_RECORDING.getEventAction(), 8);
    paramav.b().setOnClickListener((View.OnClickListener)new bz.a(paramav));
  }
  
  public final String a()
  {
    return m.b;
  }
  
  public final void a(int paramInt)
  {
    f.a(paramInt);
  }
  
  public final void a(ActionType paramActionType) {}
  
  public final void a(CallIconType paramCallIconType)
  {
    c.g.b.k.b(paramCallIconType, "callIconType");
    i.a(paramCallIconType);
  }
  
  public final void a(c.a parama)
  {
    j.a(parama);
  }
  
  public final void a(Integer paramInteger)
  {
    i.a(paramInteger);
  }
  
  public final void a(Long paramLong)
  {
    i.a(paramLong);
  }
  
  public final void a(Object paramObject)
  {
    c.a(paramObject);
  }
  
  public final void a(boolean paramBoolean)
  {
    g.a(paramBoolean);
  }
  
  public final void a_(boolean paramBoolean)
  {
    b.a_(paramBoolean);
  }
  
  public final void b(ActionType paramActionType) {}
  
  public final void b(Integer paramInteger)
  {
    i.b(paramInteger);
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "timestamp");
    i.b(paramString);
  }
  
  public final void b(boolean paramBoolean)
  {
    k.b(paramBoolean);
  }
  
  public final boolean b()
  {
    return m.b();
  }
  
  public final void b_(String paramString)
  {
    h.b_(paramString);
  }
  
  public final void b_(boolean paramBoolean)
  {
    e.b_(paramBoolean);
  }
  
  public final void c(boolean paramBoolean)
  {
    l.c(paramBoolean);
  }
  
  public final void c_(String paramString)
  {
    m.c_(paramString);
  }
  
  public final void c_(boolean paramBoolean)
  {
    m.c_(paramBoolean);
  }
  
  public final void d(boolean paramBoolean)
  {
    d.d(paramBoolean);
  }
  
  public final void e(boolean paramBoolean)
  {
    i.e(paramBoolean);
  }
  
  public final void e_(String paramString)
  {
    i.e_(paramString);
  }
  
  public final void f(boolean paramBoolean)
  {
    i.f(paramBoolean);
  }
  
  public final void g(boolean paramBoolean)
  {
    a.setActivated(paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.ab
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */