package com.truecaller.calling.recorder;

import com.truecaller.bm;
import com.truecaller.ui.components.n;
import java.util.List;

public abstract interface CallRecordingSettingsMvp
{
  public static enum CallsFilter
  {
    static
    {
      CallsFilter localCallsFilter1 = new CallsFilter("ALL_CALLS", 0);
      ALL_CALLS = localCallsFilter1;
      CallsFilter localCallsFilter2 = new CallsFilter("UNKNOWN_NUMBERS", 1);
      UNKNOWN_NUMBERS = localCallsFilter2;
      CallsFilter localCallsFilter3 = new CallsFilter("SELECTED_CONTACTS", 2);
      SELECTED_CONTACTS = localCallsFilter3;
      $VALUES = new CallsFilter[] { localCallsFilter1, localCallsFilter2, localCallsFilter3 };
    }
    
    private CallsFilter() {}
  }
  
  public static enum Configuration
  {
    static
    {
      Configuration localConfiguration1 = new Configuration("DEFAULT", 0);
      DEFAULT = localConfiguration1;
      Configuration localConfiguration2 = new Configuration("SDK_MEDIA_RECORDER", 1);
      SDK_MEDIA_RECORDER = localConfiguration2;
      $VALUES = new Configuration[] { localConfiguration1, localConfiguration2 };
    }
    
    private Configuration() {}
  }
  
  public static enum RecordingModes
  {
    static
    {
      RecordingModes localRecordingModes1 = new RecordingModes("AUTO", 0);
      AUTO = localRecordingModes1;
      RecordingModes localRecordingModes2 = new RecordingModes("MANUAL", 1);
      MANUAL = localRecordingModes2;
      $VALUES = new RecordingModes[] { localRecordingModes1, localRecordingModes2 };
    }
    
    private RecordingModes() {}
  }
  
  public static abstract interface a
    extends bm<CallRecordingSettingsMvp.b>
  {
    public abstract void a();
    
    public abstract void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt);
    
    public abstract void a(n paramn);
    
    public abstract void a(boolean paramBoolean);
    
    public abstract void b();
    
    public abstract void b(n paramn);
    
    public abstract void b(boolean paramBoolean);
    
    public abstract void c();
    
    public abstract void c(n paramn);
    
    public abstract void e();
    
    public abstract void f();
    
    public abstract void g();
  }
  
  public static abstract interface b
  {
    public abstract void a(CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext);
    
    public abstract void a(n paramn);
    
    public abstract void a(CharSequence paramCharSequence);
    
    public abstract void a(String paramString);
    
    public abstract void a(List<? extends n> paramList1, List<? extends n> paramList2, List<? extends n> paramList3);
    
    public abstract void a(boolean paramBoolean);
    
    public abstract void a(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4);
    
    public abstract void b();
    
    public abstract void b(n paramn);
    
    public abstract void b(String paramString);
    
    public abstract void b(boolean paramBoolean);
    
    public abstract void c();
    
    public abstract void c(n paramn);
    
    public abstract void c(boolean paramBoolean);
    
    public abstract void d();
    
    public abstract void e();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingSettingsMvp
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */