package com.truecaller.calling.recorder;

import c.n.m;
import com.truecaller.common.g.a;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.featuretoggles.f;
import com.truecaller.util.al;
import com.truecaller.utils.d;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Inject;

public final class i
  implements h
{
  private final e a;
  private final al b;
  private final d c;
  private final a d;
  
  @Inject
  public i(e parame, al paramal, d paramd, a parama)
  {
    a = parame;
    b = paramal;
    c = paramd;
    d = parama;
  }
  
  private boolean e()
  {
    String str = c.l();
    Iterator localIterator = m.c((CharSequence)a.ak().e(), new String[] { "," }, true, 4).iterator();
    while (localIterator.hasNext()) {
      if (m.a(str, (String)localIterator.next(), true)) {
        return true;
      }
    }
    str = c.m();
    localIterator = m.c((CharSequence)a.al().e(), new String[] { "," }, true, 4).iterator();
    while (localIterator.hasNext()) {
      if (m.a(str, (String)localIterator.next(), true)) {
        return true;
      }
    }
    return false;
  }
  
  private boolean f()
  {
    return c.h() >= 19;
  }
  
  private boolean g()
  {
    Pattern localPattern = Pattern.compile(a.am().e());
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(c.h());
    localStringBuilder.append(' ');
    localStringBuilder.append(c.l());
    return localPattern.matcher((CharSequence)localStringBuilder.toString()).matches();
  }
  
  public final boolean a()
  {
    return (b.a()) && (a.s().a()) && (!e()) && (f()) && (!g());
  }
  
  public final boolean b()
  {
    e locale = a;
    return ((f)Q.a(locale, e.a[113])).a();
  }
  
  public final boolean c()
  {
    e locale = a;
    return ((f)R.a(locale, e.a[114])).a();
  }
  
  public final CallRecordingSettingsMvp.Configuration d()
  {
    if ((c.h() >= 21) && (!m.a(d.b("callRecordingConfiguration", ""), CallRecordingSettingsMvp.Configuration.SDK_MEDIA_RECORDER.toString(), true))) {
      return CallRecordingSettingsMvp.Configuration.DEFAULT;
    }
    return CallRecordingSettingsMvp.Configuration.SDK_MEDIA_RECORDER;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("CallRecordingFeatureHelper: Feature enabled: ");
    localStringBuilder.append(a.s().a());
    localStringBuilder.append(" \nBlack listed: ");
    localStringBuilder.append(e());
    localStringBuilder.append(" \nAndroid version supported: ");
    localStringBuilder.append(f());
    localStringBuilder.append(' ');
    localStringBuilder.append("\nDoes device match blacklist regex: ");
    localStringBuilder.append(g());
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */