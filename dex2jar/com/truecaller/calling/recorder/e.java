package com.truecaller.calling.recorder;

import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.androidactors.w;
import com.truecaller.data.entity.CallRecording;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;

public final class e
  implements c
{
  private final bx a;
  private final c.d.f b;
  
  @Inject
  public e(bx parambx, @Named("Async") c.d.f paramf)
  {
    a = parambx;
    b = paramf;
  }
  
  public final w<Long> a(final CallRecording paramCallRecording)
  {
    c.g.b.k.b(paramCallRecording, "callRecording");
    paramCallRecording = w.b(Long.valueOf(((Number)kotlinx.coroutines.f.a((m)new a(this, paramCallRecording, null))).longValue()));
    c.g.b.k.a(paramCallRecording, "Promise.wrap(duration)");
    return paramCallRecording;
  }
  
  @c.d.b.a.f(b="CallRecordingDurationProviderImpl.kt", c={17}, d="invokeSuspend", e="com.truecaller.calling.recorder.CallRecordingDurationProviderImpl$getDuration$duration$1")
  static final class a
    extends c.d.b.a.k
    implements m<ag, c.d.c<? super Long>, Object>
  {
    int a;
    private ag d;
    
    a(e parame, CallRecording paramCallRecording, c.d.c paramc)
    {
      super(paramc);
    }
    
    public final c.d.c<x> a(Object paramObject, c.d.c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new a(b, paramCallRecording, paramc);
      d = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      c.d.a.a locala = c.d.a.a.a;
      switch (a)
      {
      default: 
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
      case 1: 
        if (!(paramObject instanceof o.b)) {
          return paramObject;
        }
        throw a;
      }
      if (!(paramObject instanceof o.b))
      {
        paramObject = com.truecaller.data.entity.a.a(paramCallRecording, e.a(b), e.b(b));
        a = 1;
        paramObject = ((ao)paramObject).a(this);
        if (paramObject == locala) {
          return locala;
        }
        return paramObject;
      }
      throw a;
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((a)a(paramObject1, (c.d.c)paramObject2)).a(x.a);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */