package com.truecaller.calling.recorder;

import android.content.Context;
import com.truecaller.network.search.e;
import com.truecaller.network.search.e.a;
import dagger.a.d;
import javax.inject.Provider;

public final class ag
  implements d<e>
{
  private final Provider<Context> a;
  private final Provider<e.a> b;
  
  private ag(Provider<Context> paramProvider, Provider<e.a> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static ag a(Provider<Context> paramProvider, Provider<e.a> paramProvider1)
  {
    return new ag(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.ag
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */