package com.truecaller.calling.recorder;

import c.d.b.a.f;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.data.entity.CallRecording;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;

@f(b="CallRecordingDurationProviderImpl.kt", c={17}, d="invokeSuspend", e="com.truecaller.calling.recorder.CallRecordingDurationProviderImpl$getDuration$duration$1")
final class e$a
  extends c.d.b.a.k
  implements m<ag, c<? super Long>, Object>
{
  int a;
  private ag d;
  
  e$a(e parame, CallRecording paramCallRecording, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new a(b, c, paramc);
    d = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    c.d.a.a locala = c.d.a.a.a;
    switch (a)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 1: 
      if (!(paramObject instanceof o.b)) {
        return paramObject;
      }
      throw a;
    }
    if (!(paramObject instanceof o.b))
    {
      paramObject = com.truecaller.data.entity.a.a(c, e.a(b), e.b(b));
      a = 1;
      paramObject = ((ao)paramObject).a(this);
      if (paramObject == locala) {
        return locala;
      }
      return paramObject;
    }
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((a)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.e.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */