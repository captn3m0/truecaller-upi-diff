package com.truecaller.calling.recorder;

import c.d.b.a.f;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.data.entity.CallRecording;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;
import org.a.a.aa;
import org.a.a.d.n;
import org.a.a.r;
import org.a.a.s;

@f(b="CallRecordingFloatingButtonPresenter.kt", c={139}, d="invokeSuspend", e="com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter$setRecording$1")
final class CallRecordingFloatingButtonPresenter$f
  extends c.d.b.a.k
  implements m<ag, c<? super x>, Object>
{
  Object a;
  Object b;
  Object c;
  int d;
  private ag g;
  
  CallRecordingFloatingButtonPresenter$f(CallRecordingFloatingButtonPresenter paramCallRecordingFloatingButtonPresenter, CallRecording paramCallRecording, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new f(e, f, paramc);
    g = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    bw localbw;
    Object localObject2;
    switch (d)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 1: 
      localObject1 = (n)c;
      localbw = (bw)b;
      if (!(paramObject instanceof o.b)) {
        localObject2 = paramObject;
      } else {
        throw a;
      }
      break;
    case 0: 
      if ((paramObject instanceof o.b)) {
        break label216;
      }
      localObject2 = com.truecaller.data.entity.a.a(f, e.n, e.l);
      localbw = (bw)e.b;
      if (localbw == null) {
        break label212;
      }
      paramObject = e.g;
      a = localObject2;
      b = localbw;
      c = paramObject;
      d = 1;
      localObject2 = ((ao)localObject2).a(this);
      if (localObject2 == localObject1) {
        return localObject1;
      }
      localObject1 = paramObject;
    }
    paramObject = ((n)localObject1).a((aa)r.b((int)((Number)localObject2).longValue()).a(s.a()));
    c.g.b.k.a(paramObject, "periodFormatter.print(Pe…()).normalizedStandard())");
    localbw.setLabel((String)paramObject);
    label212:
    return x.a;
    label216:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((f)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */