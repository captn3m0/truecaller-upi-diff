package com.truecaller.calling.recorder;

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;

final class CallRecordingOnBoardingActivity$c
  implements DialogInterface.OnCancelListener
{
  CallRecordingOnBoardingActivity$c(CallRecordingOnBoardingActivity paramCallRecordingOnBoardingActivity) {}
  
  public final void onCancel(DialogInterface paramDialogInterface)
  {
    a.finish();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingOnBoardingActivity.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */