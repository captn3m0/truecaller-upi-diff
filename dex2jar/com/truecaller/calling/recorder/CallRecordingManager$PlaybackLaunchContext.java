package com.truecaller.calling.recorder;

public enum CallRecordingManager$PlaybackLaunchContext
{
  static
  {
    PlaybackLaunchContext localPlaybackLaunchContext1 = new PlaybackLaunchContext("CALL_LIST", 0);
    CALL_LIST = localPlaybackLaunchContext1;
    PlaybackLaunchContext localPlaybackLaunchContext2 = new PlaybackLaunchContext("AFTER_CALL", 1);
    AFTER_CALL = localPlaybackLaunchContext2;
    PlaybackLaunchContext localPlaybackLaunchContext3 = new PlaybackLaunchContext("RECORDINGS", 2);
    RECORDINGS = localPlaybackLaunchContext3;
    $VALUES = new PlaybackLaunchContext[] { localPlaybackLaunchContext1, localPlaybackLaunchContext2, localPlaybackLaunchContext3 };
  }
  
  private CallRecordingManager$PlaybackLaunchContext() {}
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingManager.PlaybackLaunchContext
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */