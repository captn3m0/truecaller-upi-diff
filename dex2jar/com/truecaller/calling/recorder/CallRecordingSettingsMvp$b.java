package com.truecaller.calling.recorder;

import com.truecaller.ui.components.n;
import java.util.List;

public abstract interface CallRecordingSettingsMvp$b
{
  public abstract void a(CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext);
  
  public abstract void a(n paramn);
  
  public abstract void a(CharSequence paramCharSequence);
  
  public abstract void a(String paramString);
  
  public abstract void a(List<? extends n> paramList1, List<? extends n> paramList2, List<? extends n> paramList3);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void a(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4);
  
  public abstract void b();
  
  public abstract void b(n paramn);
  
  public abstract void b(String paramString);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void c();
  
  public abstract void c(n paramn);
  
  public abstract void c(boolean paramBoolean);
  
  public abstract void d();
  
  public abstract void e();
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingSettingsMvp.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */