package com.truecaller.calling.recorder;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.bk;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import javax.inject.Inject;
import javax.inject.Named;

public final class CallRecordingPromoViewImpl
  extends FrameLayout
  implements av, bu, cb
{
  @Inject
  @Named("promo_view")
  public ar a;
  @Inject
  public com.truecaller.premium.br b;
  @Inject
  public aj c;
  private TextView d;
  private TextView e;
  private TextView f;
  private ImageView g;
  
  public CallRecordingPromoViewImpl(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private CallRecordingPromoViewImpl(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    paramAttributeSet = br.a();
    Context localContext = paramContext.getApplicationContext();
    if (localContext != null)
    {
      paramAttributeSet.a(((bk)localContext).a()).a(new ap((cb)this, (bu)this)).a().a(this);
      LayoutInflater.from(paramContext).inflate(2131559090, (ViewGroup)this);
      return;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.GraphHolder");
  }
  
  public final void a()
  {
    if (c == null) {
      k.a("callRecordingOnBoardingNavigator");
    }
    Context localContext = getContext();
    k.a(localContext, "context");
    aj.a(localContext, CallRecordingOnBoardingState.WHATS_NEW, CallRecordingOnBoardingLaunchContext.PROMO);
  }
  
  public final void a(PremiumPresenterView.LaunchContext paramLaunchContext)
  {
    k.b(paramLaunchContext, "launchContext");
    if (b == null) {
      k.a("premiumScreenNavigator");
    }
    paramLaunchContext = getContext();
    k.a(paramLaunchContext, "context");
    com.truecaller.premium.br.a(paramLaunchContext, PremiumPresenterView.LaunchContext.CALL_RECORDINGS, "premiumCallRecording");
  }
  
  public final aj getCallRecordingOnBoardingNavigator()
  {
    aj localaj = c;
    if (localaj == null) {
      k.a("callRecordingOnBoardingNavigator");
    }
    return localaj;
  }
  
  public final com.truecaller.premium.br getPremiumScreenNavigator()
  {
    com.truecaller.premium.br localbr = b;
    if (localbr == null) {
      k.a("premiumScreenNavigator");
    }
    return localbr;
  }
  
  public final ar getPresenter()
  {
    ar localar = a;
    if (localar == null) {
      k.a("presenter");
    }
    return localar;
  }
  
  protected final void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    ar localar = a;
    if (localar == null) {
      k.a("presenter");
    }
    localar.c(this);
    localar = a;
    if (localar == null) {
      k.a("presenter");
    }
    localar.a(this, 0);
  }
  
  protected final void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    ar localar = a;
    if (localar == null) {
      k.a("presenter");
    }
    localar.d(this);
  }
  
  protected final void onFinishInflate()
  {
    super.onFinishInflate();
    d = ((TextView)findViewById(2131362347));
    e = ((TextView)findViewById(2131364029));
    f = ((TextView)findViewById(2131364028));
    g = ((ImageView)findViewById(2131364026));
    Object localObject = e;
    if (localObject != null) {
      ((TextView)localObject).setMaxLines(2);
    }
    localObject = d;
    if (localObject != null) {
      ((TextView)localObject).setText((CharSequence)getContext().getString(2131887234));
    }
    localObject = d;
    if (localObject != null) {
      ((TextView)localObject).setOnClickListener((View.OnClickListener)new a(this));
    }
    localObject = g;
    if (localObject != null)
    {
      ((ImageView)localObject).setImageResource(2131233934);
      return;
    }
  }
  
  public final void setCTATitle(String paramString)
  {
    k.b(paramString, "ctaTitle");
    TextView localTextView = d;
    if (localTextView != null)
    {
      localTextView.setText((CharSequence)paramString);
      return;
    }
  }
  
  public final void setCallRecordingOnBoardingNavigator(aj paramaj)
  {
    k.b(paramaj, "<set-?>");
    c = paramaj;
  }
  
  public final void setPremiumScreenNavigator(com.truecaller.premium.br parambr)
  {
    k.b(parambr, "<set-?>");
    b = parambr;
  }
  
  public final void setPresenter(ar paramar)
  {
    k.b(paramar, "<set-?>");
    a = paramar;
  }
  
  public final void setText(String paramString)
  {
    k.b(paramString, "text");
    TextView localTextView = f;
    if (localTextView != null)
    {
      localTextView.setText((CharSequence)paramString);
      return;
    }
  }
  
  public final void setTitle(String paramString)
  {
    k.b(paramString, "title");
    TextView localTextView = e;
    if (localTextView != null)
    {
      localTextView.setText((CharSequence)paramString);
      return;
    }
  }
  
  static final class a
    implements View.OnClickListener
  {
    a(CallRecordingPromoViewImpl paramCallRecordingPromoViewImpl) {}
    
    public final void onClick(View paramView)
    {
      a.getPresenter().a();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.CallRecordingPromoViewImpl
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */