package com.truecaller.calling.recorder;

import c.o.b;
import com.truecaller.adapter_delegates.h;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.w;
import com.truecaller.callhistory.z;
import com.truecaller.calling.ActionType;
import com.truecaller.calling.dialer.ax;
import com.truecaller.calling.dialer.cb;
import com.truecaller.calling.dialer.t;
import com.truecaller.common.h.aj;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.network.search.e;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.util.af;
import com.truecaller.utils.n;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;
import kotlinx.coroutines.bn;

public final class ad
  extends com.truecaller.adapter_delegates.c<aa>
  implements ac, ca
{
  private final bk c;
  private final cb d;
  private final HashMap<CallRecording, Long> e;
  private final HashMap<aa, bn> f;
  private final bk g;
  private final aj h;
  private final n i;
  private final af j;
  private final ax k;
  private final t l;
  private final u m;
  private final x n;
  private final cd o;
  private final bt p;
  private final com.truecaller.calling.dialer.a q;
  private final com.truecaller.analytics.b r;
  private final com.truecaller.androidactors.k s;
  private final e t;
  private final c.d.f u;
  private final g v;
  private final bx w;
  private final CallRecordingManager x;
  private final c.d.f y;
  
  @Inject
  public ad(bk parambk, aj paramaj, n paramn, af paramaf, ax paramax, t paramt, u paramu, x paramx, cd paramcd, bt parambt, com.truecaller.calling.dialer.a parama, com.truecaller.analytics.b paramb, com.truecaller.androidactors.k paramk, @Named("call_recording_bulk_searcher") e parame, @Named("UI") c.d.f paramf1, g paramg, bx parambx, CallRecordingManager paramCallRecordingManager, @Named("Async") c.d.f paramf2)
  {
    g = parambk;
    h = paramaj;
    i = paramn;
    j = paramaf;
    k = paramax;
    l = paramt;
    m = paramu;
    n = paramx;
    o = paramcd;
    p = parambt;
    q = parama;
    r = paramb;
    s = paramk;
    t = parame;
    u = paramf1;
    v = paramg;
    w = parambx;
    x = paramCallRecordingManager;
    y = paramf2;
    parambk = g;
    c = parambk;
    d = parambk.a((ac)this);
    e = new HashMap();
    f = new HashMap();
  }
  
  private final z a()
  {
    return c.a(this, b[0]);
  }
  
  private final void a(String paramString1, String paramString2)
  {
    paramString2 = new e.a("ViewAction").a("Context", "notificationCallRecording").a("Action", paramString1).a("SubAction", paramString2);
    paramString1 = r;
    paramString2 = paramString2.a();
    c.g.b.k.a(paramString2, "eventBuilder.build()");
    paramString1.a(paramString2);
  }
  
  private final boolean a(int paramInt, String paramString)
  {
    HistoryEvent localHistoryEvent = d(paramInt);
    if (localHistoryEvent != null)
    {
      l.a(localHistoryEvent, DetailsFragment.SourceType.CallRecording, false, false);
      a("details", paramString);
      return true;
    }
    return false;
  }
  
  private final HistoryEvent d(int paramInt)
  {
    z localz = a();
    if (localz != null) {
      localz.moveToPosition(paramInt);
    }
    localz = a();
    if (localz != null) {
      return localz.d();
    }
    return null;
  }
  
  private final c.x e(int paramInt)
  {
    Object localObject = d(paramInt);
    if (localObject != null)
    {
      localObject = ((HistoryEvent)localObject).t();
      if (localObject != null)
      {
        bk localbk = g;
        c.g.b.k.a(localObject, "it");
        localbk.b((CallRecording)localObject);
        return c.x.a;
      }
    }
    return null;
  }
  
  public final void a(int paramInt)
  {
    if (x.a(CallRecordingOnBoardingLaunchContext.LIST)) {
      return;
    }
    Object localObject1 = d(paramInt);
    if (localObject1 != null)
    {
      localObject1 = ((HistoryEvent)localObject1).t();
      if (localObject1 != null)
      {
        localObject1 = c;
        if (localObject1 != null)
        {
          if (!(c.n.m.a((CharSequence)localObject1) ^ true)) {
            localObject1 = null;
          }
          if (localObject1 != null)
          {
            Object localObject2 = m;
            if (!((u)localObject2).a(((u)localObject2).b((String)localObject1)))
            {
              localObject1 = o;
              localObject2 = i.a(2131887613, new Object[0]);
              c.g.b.k.a(localObject2, "resourceProvider.getStri…_no_activity_found_share)");
              ((cd)localObject1).toast((String)localObject2);
              return;
            }
            v.a(new e.a("CallRecordingShared"));
            return;
          }
        }
      }
    }
  }
  
  public final boolean a(h paramh)
  {
    c.g.b.k.b(paramh, "event");
    int i1 = b;
    Object localObject = a;
    if (c.g.b.k.a(localObject, "ItemEvent.LONG_CLICKED"))
    {
      if (!a)
      {
        q.b();
        a = true;
        e(i1);
        return true;
      }
      return false;
    }
    if (c.g.b.k.a(localObject, "ItemEvent.CLICKED"))
    {
      if (a) {
        e(i1);
      }
      return true;
    }
    if (c.g.b.k.a(localObject, ActionType.PROFILE.getEventAction())) {
      return a(i1, "avatar");
    }
    if (c.g.b.k.a(localObject, CallRecordingActionType.PLAY_CALL_RECORDING.getEventAction()))
    {
      if (!x.a(CallRecordingOnBoardingLaunchContext.LIST))
      {
        paramh = d(i1);
        if (paramh != null)
        {
          paramh = paramh.t();
          if (paramh != null)
          {
            localObject = m;
            if (((u)localObject).a(((u)localObject).a(c)))
            {
              paramh = o;
              localObject = i.a(2131887616, new Object[0]);
              c.g.b.k.a(localObject, "resourceProvider.getStri…ecording_toast_item_play)");
              paramh.toast((String)localObject);
              paramh = v;
              localObject = new e.a("CallRecordingPlayback").a("Source", CallRecordingManager.PlaybackLaunchContext.RECORDINGS.name());
              c.g.b.k.a(localObject, "AnalyticsEvent.Builder(A…                        )");
              paramh.a((e.a)localObject);
              return true;
            }
            paramh = o;
            localObject = i.a(2131887612, new Object[0]);
            c.g.b.k.a(localObject, "resourceProvider.getStri…r_no_activity_found_play)");
            paramh.toast((String)localObject);
          }
        }
      }
      return true;
    }
    if (c.g.b.k.a(localObject, CallRecordingActionType.SHOW_CALL_RECORDING_MENU_OPTIONS.getEventAction()))
    {
      paramh = d;
      n.a(i1, paramh, (ca)this);
      return true;
    }
    return false;
  }
  
  public final void b(int paramInt)
  {
    Object localObject = d(paramInt);
    if (localObject != null)
    {
      localObject = ((HistoryEvent)localObject).t();
      if (localObject != null)
      {
        bt localbt = p;
        c.g.b.k.a(localObject, "callRecording");
        localbt.a(localObject, (by)new b(this));
        return;
      }
    }
  }
  
  public final void c(int paramInt)
  {
    a(paramInt, "callRecording");
  }
  
  public final int getItemCount()
  {
    z localz = a();
    if (localz != null) {
      return localz.getCount();
    }
    return 0;
  }
  
  public final long getItemId(int paramInt)
  {
    if (a() == null) {
      return -1L;
    }
    Object localObject = a();
    if (localObject != null) {
      ((z)localObject).moveToPosition(paramInt);
    }
    localObject = a();
    if (localObject != null)
    {
      localObject = ((z)localObject).d();
      if (localObject != null)
      {
        localObject = ((HistoryEvent)localObject).t();
        if (localObject != null) {
          return a;
        }
      }
    }
    return -1L;
  }
  
  @c.d.b.a.f(b="CallRecordingItemsPresenter.kt", c={143}, d="invokeSuspend", e="com.truecaller.calling.recorder.CallRecordingItemsPresenterImpl$onBindItem$1$2$fetchDurationJob$1")
  static final class a
    extends c.d.b.a.k
    implements c.g.a.m<ag, c.d.c<? super c.x>, Object>
  {
    Object a;
    Object b;
    int c;
    private ag n;
    
    a(c.d.c paramc, aa paramaa1, ad paramad, Contact paramContact, boolean paramBoolean1, HistoryEvent paramHistoryEvent, String paramString, CallRecording paramCallRecording, boolean paramBoolean2, aa paramaa2, int paramInt)
    {
      super(paramc);
    }
    
    public final c.d.c<c.x> a(Object paramObject, c.d.c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new a(paramc, d, e, f, g, h, i, j, k, l, m);
      n = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      Object localObject1 = c.d.a.a.a;
      Map localMap;
      Object localObject2;
      switch (c)
      {
      default: 
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
      case 1: 
        localObject1 = (CallRecording)b;
        localMap = (Map)a;
        if (!(paramObject instanceof o.b)) {
          localObject2 = paramObject;
        } else {
          throw a;
        }
        break;
      case 0: 
        if ((paramObject instanceof o.b)) {
          break label196;
        }
        localMap = (Map)ad.a(e);
        paramObject = j;
        localObject2 = com.truecaller.data.entity.a.a((CallRecording)paramObject, ad.b(e), ad.c(e));
        a = localMap;
        b = paramObject;
        c = 1;
        localObject2 = ((ao)localObject2).a(this);
        if (localObject2 == localObject1) {
          return localObject1;
        }
        localObject1 = paramObject;
      }
      localMap.put(localObject1, c.d.b.a.b.a(((Number)localObject2).longValue() / 1000L));
      ad.d(e).e();
      return c.x.a;
      label196:
      throw a;
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((a)a(paramObject1, (c.d.c)paramObject2)).a(c.x.a);
    }
  }
  
  public static final class b
    implements by
  {
    b(ad paramad) {}
    
    public final void a(Object paramObject)
    {
      c.g.b.k.b(paramObject, "any");
      ad.d(a).c((CallRecording)paramObject).a(ad.e(a).a(), (com.truecaller.androidactors.ac)new com.truecaller.androidactors.ac() {});
    }
    
    public final void b(Object paramObject)
    {
      c.g.b.k.b(paramObject, "any");
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.recorder.ad
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */