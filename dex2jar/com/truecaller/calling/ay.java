package com.truecaller.calling;

import android.view.View;
import android.widget.TextView;
import c.f;
import c.g.b.k;
import com.truecaller.utils.extensions.t;
import com.truecaller.utils.ui.b;

public final class ay
  implements ax
{
  private final f b;
  
  public ay(View paramView)
  {
    b = t.a(paramView, 2131364280);
    a().setTextColor(b.b(paramView.getContext(), 2130969585));
  }
  
  private final TextView a()
  {
    return (TextView)b.b();
  }
  
  public final void b(String paramString)
  {
    TextView localTextView = a();
    if (paramString != null)
    {
      t.a((View)localTextView);
      localTextView.setText((CharSequence)paramString);
      return;
    }
    k.a(localTextView, "this");
    t.b((View)localTextView);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.ay
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */