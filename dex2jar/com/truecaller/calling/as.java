package com.truecaller.calling;

import c.a.f;
import c.g.b.k;
import com.truecaller.i.c;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.h;
import com.truecaller.utils.n;
import java.util.Iterator;
import java.util.List;

public final class as
  implements ar
{
  private final c a;
  private final h b;
  private final n c;
  
  public as(c paramc, h paramh, n paramn)
  {
    a = paramc;
    b = paramh;
    c = paramn;
  }
  
  private void a(String paramString)
  {
    k.b(paramString, "simToken");
    a.a("selectedCallSimToken", paramString);
    b.a(paramString);
  }
  
  public final void a()
  {
    int i;
    switch (c())
    {
    default: 
      i = 0;
      break;
    case 1: 
      i = -1;
      break;
    case 0: 
      i = 1;
    }
    if (i != -1)
    {
      Object localObject = b.a(i);
      if (localObject == null) {
        return;
      }
      k.a(localObject, "multiSimManager.getSimIn…ex(currentSlot) ?: return");
      localObject = b;
      k.a(localObject, "simInfo.simToken");
      a((String)localObject);
      return;
    }
    a("-1");
  }
  
  public final String b()
  {
    int i = c();
    if (i != -1)
    {
      SimInfo localSimInfo = b.a(i);
      if (localSimInfo == null) {
        return null;
      }
      k.a(localSimInfo, "multiSimManager.getSimIn…rrentSlot) ?: return null");
      localObject = c.a(2130903061);
      k.a(localObject, "resourceProvider.getStri…ref_items_multi_sim_slot)");
      String str = (String)f.g((Object[])localObject).get(i);
      localObject = (CharSequence)d;
      if ((localObject != null) && (((CharSequence)localObject).length() != 0)) {
        i = 0;
      } else {
        i = 1;
      }
      localObject = str;
      if (i == 0)
      {
        localObject = new StringBuilder();
        ((StringBuilder)localObject).append(str);
        ((StringBuilder)localObject).append(" - ");
        ((StringBuilder)localObject).append(d);
        localObject = ((StringBuilder)localObject).toString();
      }
      localObject = c.a(2131888869, new Object[] { localObject });
      k.a(localObject, "resourceProvider.getStri…switched_to_sim, simName)");
      return (String)localObject;
    }
    Object localObject = c.a(2131888388, new Object[0]);
    k.a(localObject, "resourceProvider.getStri…ing.multi_sim_always_ask)");
    return (String)localObject;
  }
  
  public final int c()
  {
    Object localObject = e();
    if (k.a("-1", localObject)) {
      return -1;
    }
    localObject = b.b((String)localObject);
    if (localObject == null) {
      return -1;
    }
    k.a(localObject, "multiSimManager.getSimIn…mManager.SIM_SLOT_UNKNOWN");
    return a;
  }
  
  public final int d()
  {
    switch (c())
    {
    default: 
      return 2131234556;
    case 1: 
      return 2131234555;
    }
    return 2131234554;
  }
  
  public final String e()
  {
    String str = b.g();
    k.a(str, "multiSimManager.selectedCallSimToken");
    if (k.a(str, "-1"))
    {
      Object localObject2 = a.b("selectedCallSimToken", "-1");
      k.a(localObject2, "callingSettings.getStrin…KEN_UNKNOWN\n            )");
      Object localObject1 = b.h();
      k.a(localObject1, "multiSimManager.allSimInfos");
      Iterator localIterator = ((Iterable)localObject1).iterator();
      while (localIterator.hasNext())
      {
        localObject1 = localIterator.next();
        if (k.a(b, localObject2)) {
          break label109;
        }
      }
      localObject1 = null;
      label109:
      localObject2 = (SimInfo)localObject1;
      localObject1 = str;
      if (localObject2 != null)
      {
        localObject1 = b;
        if (localObject1 == null) {
          return str;
        }
      }
      return (String)localObject1;
    }
    return str;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.as
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */