package com.truecaller.calling.c;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.app.AlertDialog.Builder;
import android.view.View;
import c.u;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.calling.initiate_call.b;
import com.truecaller.calling.initiate_call.b.a.a;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.util.bj;
import com.truecaller.voip.ai;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import javax.inject.Inject;

public final class c
  extends com.truecaller.ui.dialogs.a
  implements h.f
{
  public static final a e = new a((byte)0);
  @Inject
  public h.e a;
  @Inject
  public h.b b;
  @Inject
  public b c;
  @Inject
  public ai d;
  private HashMap f;
  
  public static final void a(f paramf, Contact paramContact, List<? extends Number> paramList, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, String paramString)
  {
    a.a(paramf, paramContact, paramList, true, paramBoolean1, paramBoolean2, paramBoolean3, false, paramString);
  }
  
  public final View a(int paramInt)
  {
    if (f == null) {
      f = new HashMap();
    }
    View localView2 = (View)f.get(Integer.valueOf(paramInt));
    View localView1 = localView2;
    if (localView2 == null)
    {
      localView1 = getView();
      if (localView1 == null) {
        return null;
      }
      localView1 = localView1.findViewById(paramInt);
      f.put(Integer.valueOf(paramInt), localView1);
    }
    return localView1;
  }
  
  public final void a()
  {
    dismissAllowingStateLoss();
  }
  
  public final void a(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString2, "analyticsContext");
    paramString2 = getContext();
    if (paramString2 != null)
    {
      bj.a(paramString2, paramString1);
      return;
    }
  }
  
  public final void a(String paramString1, String paramString2, boolean paramBoolean, String paramString3)
  {
    c.g.b.k.b(paramString1, "number");
    c.g.b.k.b(paramString3, "analyticsContext");
    paramString1 = new b.a.a(paramString1, paramString3).a(paramString2).a(paramBoolean);
    paramString2 = c;
    if (paramString2 == null) {
      c.g.b.k.a("initiateCallHelper");
    }
    paramString2.a(paramString1.a());
  }
  
  public final void b()
  {
    HashMap localHashMap = f;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void b(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "number");
    c.g.b.k.b(paramString2, "analyticsContext");
    ai localai = d;
    if (localai == null) {
      c.g.b.k.a("voipUtil");
    }
    localai.a(paramString1, paramString2);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getActivity();
    ArrayList localArrayList = null;
    if (paramBundle != null) {
      paramBundle = paramBundle.getApplicationContext();
    } else {
      paramBundle = null;
    }
    if (paramBundle != null)
    {
      ((bk)paramBundle).a().bs().a().a(this);
      paramBundle = a;
      if (paramBundle == null) {
        c.g.b.k.a("presenter");
      }
      paramBundle.b(this);
      h.e locale = a;
      if (locale == null) {
        c.g.b.k.a("presenter");
      }
      paramBundle = getArguments();
      if (paramBundle != null) {
        paramBundle = (Contact)paramBundle.getParcelable("contact");
      } else {
        paramBundle = null;
      }
      Object localObject = getArguments();
      if (localObject != null) {
        localArrayList = ((Bundle)localObject).getParcelableArrayList("numbers");
      }
      localObject = getArguments();
      boolean bool1;
      if (localObject != null) {
        bool1 = ((Bundle)localObject).getBoolean("consider_primary");
      } else {
        bool1 = true;
      }
      localObject = getArguments();
      boolean bool2;
      if (localObject != null) {
        bool2 = ((Bundle)localObject).getBoolean("call");
      } else {
        bool2 = false;
      }
      localObject = getArguments();
      boolean bool3;
      if (localObject != null) {
        bool3 = ((Bundle)localObject).getBoolean("video_call");
      } else {
        bool3 = false;
      }
      localObject = getArguments();
      boolean bool4;
      if (localObject != null) {
        bool4 = ((Bundle)localObject).getBoolean("sms");
      } else {
        bool4 = false;
      }
      localObject = getArguments();
      boolean bool5;
      if (localObject != null) {
        bool5 = ((Bundle)localObject).getBoolean("voip_call");
      } else {
        bool5 = false;
      }
      localObject = getArguments();
      if (localObject != null)
      {
        String str = ((Bundle)localObject).getString("analytics_context");
        localObject = str;
        if (str != null) {}
      }
      else
      {
        localObject = "";
      }
      locale.a(paramBundle, localArrayList, bool1, bool2, bool3, bool4, bool5, (String)localObject);
      return;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.GraphHolder");
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    Object localObject1 = getActivity();
    if (localObject1 == null)
    {
      paramBundle = super.onCreateDialog(paramBundle);
      c.g.b.k.a(paramBundle, "super.onCreateDialog(savedInstanceState)");
      return paramBundle;
    }
    c.g.b.k.a(localObject1, "activity ?: return super…ialog(savedInstanceState)");
    localObject1 = (Context)localObject1;
    paramBundle = View.inflate((Context)localObject1, 2131558595, null);
    c.g.b.k.a(paramBundle, "view");
    Object localObject2 = a;
    if (localObject2 == null) {
      c.g.b.k.a("presenter");
    }
    localObject2 = (h.g.a)localObject2;
    Object localObject3 = b;
    if (localObject3 == null) {
      c.g.b.k.a("itemPresenter");
    }
    h.e locale = a;
    if (locale == null) {
      c.g.b.k.a("presenter");
    }
    localObject2 = new k(paramBundle, (h.g.a)localObject2, (h.b)localObject3, locale.b());
    localObject3 = a;
    if (localObject3 == null) {
      c.g.b.k.a("presenter");
    }
    ((h.e)localObject3).a(localObject2);
    localObject1 = new AlertDialog.Builder((Context)localObject1);
    localObject2 = a;
    if (localObject2 == null) {
      c.g.b.k.a("presenter");
    }
    paramBundle = ((AlertDialog.Builder)localObject1).setTitle((CharSequence)((h.e)localObject2).c()).setView(paramBundle).create();
    c.g.b.k.a(paramBundle, "AlertDialog.Builder(acti…ew)\n            .create()");
    return (Dialog)paramBundle;
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    h.e locale = a;
    if (locale == null) {
      c.g.b.k.a("presenter");
    }
    locale.x_();
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    h.e locale = a;
    if (locale == null) {
      c.g.b.k.a("presenter");
    }
    locale.y_();
    b();
  }
  
  public static final class a
  {
    public static void a(f paramf, Contact paramContact, List<? extends Number> paramList, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, String paramString)
    {
      c.g.b.k.b(paramString, "analyticsContext");
      c localc = new c();
      Bundle localBundle = localc.getArguments();
      if (localBundle != null) {
        localBundle.putParcelable("contact", (Parcelable)paramContact);
      }
      paramContact = localc.getArguments();
      if (paramContact != null) {
        paramContact.putParcelableArrayList("numbers", new ArrayList((Collection)paramList));
      }
      paramContact = localc.getArguments();
      if (paramContact != null) {
        paramContact.putBoolean("consider_primary", paramBoolean1);
      }
      paramContact = localc.getArguments();
      if (paramContact != null) {
        paramContact.putBoolean("call", paramBoolean2);
      }
      paramContact = localc.getArguments();
      if (paramContact != null) {
        paramContact.putBoolean("video_call", paramBoolean3);
      }
      paramContact = localc.getArguments();
      if (paramContact != null) {
        paramContact.putBoolean("sms", paramBoolean4);
      }
      paramContact = localc.getArguments();
      if (paramContact != null) {
        paramContact.putBoolean("voip_call", paramBoolean5);
      }
      paramContact = localc.getArguments();
      if (paramContact != null) {
        paramContact.putString("analytics_context", paramString);
      }
      com.truecaller.ui.dialogs.a.a(localc, paramf);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.c.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */