package com.truecaller.calling.c;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.f;
import c.g.b.k;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.ui.dialogs.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class c$a
{
  public static void a(f paramf, Contact paramContact, List<? extends Number> paramList, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, String paramString)
  {
    k.b(paramString, "analyticsContext");
    c localc = new c();
    Bundle localBundle = localc.getArguments();
    if (localBundle != null) {
      localBundle.putParcelable("contact", (Parcelable)paramContact);
    }
    paramContact = localc.getArguments();
    if (paramContact != null) {
      paramContact.putParcelableArrayList("numbers", new ArrayList((Collection)paramList));
    }
    paramContact = localc.getArguments();
    if (paramContact != null) {
      paramContact.putBoolean("consider_primary", paramBoolean1);
    }
    paramContact = localc.getArguments();
    if (paramContact != null) {
      paramContact.putBoolean("call", paramBoolean2);
    }
    paramContact = localc.getArguments();
    if (paramContact != null) {
      paramContact.putBoolean("video_call", paramBoolean3);
    }
    paramContact = localc.getArguments();
    if (paramContact != null) {
      paramContact.putBoolean("sms", paramBoolean4);
    }
    paramContact = localc.getArguments();
    if (paramContact != null) {
      paramContact.putBoolean("voip_call", paramBoolean5);
    }
    paramContact = localc.getArguments();
    if (paramContact != null) {
      paramContact.putString("analytics_context", paramString);
    }
    a.a(localc, paramf);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.c.c.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */