package com.truecaller.calling.c;

import c.l.g;
import com.truecaller.adapter_delegates.j;
import com.truecaller.bn;
import com.truecaller.calling.ag;
import com.truecaller.calling.bc;
import com.truecaller.calling.c;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import java.util.ArrayList;

public abstract interface h
{
  public static abstract interface a
  {
    public abstract void a(Number paramNumber, String paramString);
  }
  
  public static abstract interface b
    extends com.truecaller.adapter_delegates.b<h.c>, j
  {}
  
  public static abstract interface c
    extends ag, bc, c
  {
    public abstract void a(int paramInt);
    
    public abstract void c(boolean paramBoolean);
  }
  
  public static abstract interface d
  {
    public abstract b a(h.b paramb, g<?> paramg);
  }
  
  public static abstract interface e
    extends bn<h.f, h.g>, h.a, h.d, h.g.a
  {
    public abstract void a(Contact paramContact, ArrayList<Number> paramArrayList, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, String paramString);
    
    public abstract boolean b();
    
    public abstract String c();
  }
  
  public static abstract interface f
  {
    public abstract void a();
    
    public abstract void a(String paramString1, String paramString2);
    
    public abstract void a(String paramString1, String paramString2, boolean paramBoolean, String paramString3);
    
    public abstract void b(String paramString1, String paramString2);
  }
  
  public static abstract interface g
  {
    public abstract void a(int paramInt);
    
    public static abstract interface a
    {
      public abstract void a(boolean paramBoolean);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.c.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */