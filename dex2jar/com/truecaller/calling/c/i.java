package com.truecaller.calling.c;

import c.d.c;
import c.l.g;
import c.o.b;
import c.x;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.w;
import com.truecaller.bd;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.Number;
import com.truecaller.log.d;
import com.truecaller.multisim.h;
import com.truecaller.utils.n;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;

public final class i
  extends bd<h.f, h.g>
  implements h.e
{
  Contact a;
  b d;
  final com.truecaller.data.access.m e;
  private String f;
  private String g;
  private boolean h;
  private boolean i;
  private boolean j;
  private boolean k;
  private boolean l;
  private int m;
  private boolean n;
  private final com.truecaller.calling.ar o;
  private final h p;
  private final com.truecaller.callhistory.a q;
  private final n r;
  private final c.d.f s;
  private final c.d.f t;
  
  @Inject
  public i(com.truecaller.calling.ar paramar, h paramh, com.truecaller.callhistory.a parama, n paramn, com.truecaller.data.access.m paramm, @Named("Async") c.d.f paramf1, @Named("UI") c.d.f paramf2)
  {
    o = paramar;
    p = paramh;
    q = parama;
    r = paramn;
    e = paramm;
    s = paramf1;
    t = paramf2;
    f = "";
    g = "";
    m = o.c();
    d = new b(false, false, false, null, 15);
  }
  
  private final void a(final Number paramNumber)
  {
    if (!n) {
      return;
    }
    paramNumber = paramNumber.getId();
    if (paramNumber != null)
    {
      paramNumber = String.valueOf(paramNumber.longValue());
      if (paramNumber == null) {
        return;
      }
      e.b((ag)bg.a, s, (c.g.a.m)new c(this, paramNumber, null), 2);
      return;
    }
  }
  
  public final b a(h.b paramb, g<?> paramg)
  {
    c.g.b.k.b(paramb, "itemPresenter");
    c.g.b.k.b(paramg, "property");
    return d;
  }
  
  public final void a(Contact paramContact, ArrayList<Number> paramArrayList, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, String paramString)
  {
    c.g.b.k.b(paramString, "analyticsContext");
    if (paramContact == null)
    {
      paramContact = (h.f)c;
      if (paramContact != null)
      {
        paramContact.a();
        return;
      }
      return;
    }
    Object localObject1 = (Collection)paramArrayList;
    int i1;
    if ((localObject1 != null) && (!((Collection)localObject1).isEmpty())) {
      i1 = 0;
    } else {
      i1 = 1;
    }
    if (i1 != 0)
    {
      paramContact = (h.f)c;
      if (paramContact != null)
      {
        paramContact.a();
        return;
      }
      return;
    }
    a = paramContact;
    Object localObject2 = new StringBuilder();
    if (paramBoolean4) {
      localObject1 = r.a(2131888349, new Object[0]);
    } else if (paramBoolean5) {
      localObject1 = r.a(2131888352, new Object[0]);
    } else {
      localObject1 = r.a(2131888340, new Object[0]);
    }
    ((StringBuilder)localObject2).append((String)localObject1);
    localObject1 = paramContact.t();
    if (localObject1 != null) {
      localObject1 = " - ".concat(String.valueOf(localObject1));
    } else {
      localObject1 = null;
    }
    ((StringBuilder)localObject2).append((String)localObject1);
    f = ((StringBuilder)localObject2).toString();
    g = paramString;
    j = paramBoolean1;
    k = paramBoolean4;
    i = paramBoolean3;
    l = paramBoolean5;
    h = paramBoolean2;
    if (paramArrayList.size() == 1)
    {
      a((Number)c.a.m.d((List)paramArrayList), paramContact.s());
      paramContact = (h.f)c;
      if (paramContact != null)
      {
        paramContact.a();
        return;
      }
      return;
    }
    paramString = (Iterable)paramArrayList;
    localObject1 = paramString.iterator();
    while (((Iterator)localObject1).hasNext())
    {
      paramArrayList = ((Iterator)localObject1).next();
      localObject2 = (Number)paramArrayList;
      if ((paramBoolean1) && (((Number)localObject2).isPrimary())) {
        i1 = 1;
      } else {
        i1 = 0;
      }
      if (i1 != 0) {
        break label375;
      }
    }
    paramArrayList = null;
    label375:
    paramArrayList = (Number)paramArrayList;
    if (paramArrayList != null)
    {
      a(paramArrayList, paramContact.s());
      paramContact = (h.f)c;
      if (paramContact != null)
      {
        paramContact.a();
        return;
      }
      return;
    }
    d = new b(p.j(), paramBoolean4, paramBoolean5, null, 8);
    paramContact = paramString.iterator();
    while (paramContact.hasNext())
    {
      paramArrayList = (Number)paramContact.next();
      q.c(paramArrayList.a()).a((ac)new b(paramArrayList, this));
    }
  }
  
  public final void a(Number paramNumber, String paramString)
  {
    c.g.b.k.b(paramNumber, "number");
    String str = paramNumber.e();
    if (str == null)
    {
      paramNumber = (h.f)c;
      if (paramNumber != null)
      {
        paramNumber.a();
        return;
      }
      return;
    }
    a(paramNumber);
    if (h)
    {
      paramNumber = (h.f)c;
      if (paramNumber != null) {
        paramNumber.a(str, paramString, i, g);
      }
    }
    else if (k)
    {
      paramNumber = (h.f)c;
      if (paramNumber != null) {
        paramNumber.a(str, g);
      }
    }
    else if (l)
    {
      paramNumber = (h.f)c;
      if (paramNumber != null) {
        paramNumber.b(str, g);
      }
    }
    e.b((ag)bg.a, t, (c.g.a.m)new a(this, null), 2);
  }
  
  public final void a(boolean paramBoolean)
  {
    n = paramBoolean;
  }
  
  public final boolean b()
  {
    return j;
  }
  
  public final String c()
  {
    return f;
  }
  
  @c.d.b.a.f(b="SelectNumberPresenter.kt", c={130}, d="invokeSuspend", e="com.truecaller.calling.select_number.SelectNumberPresenter$callOrSmsNumber$1")
  static final class a
    extends c.d.b.a.k
    implements c.g.a.m<ag, c<? super x>, Object>
  {
    int a;
    private ag c;
    
    a(i parami, c paramc)
    {
      super(paramc);
    }
    
    public final c<x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new a(b, paramc);
      c = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      c.d.a.a locala = c.d.a.a.a;
      switch (a)
      {
      default: 
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
      case 1: 
        if ((paramObject instanceof o.b)) {
          throw a;
        }
        break;
      case 0: 
        if ((paramObject instanceof o.b)) {
          break label110;
        }
        a = 1;
        if (kotlinx.coroutines.ar.a(350L, this) == locala) {
          return locala;
        }
        break;
      }
      paramObject = (h.f)b.c;
      if (paramObject != null) {
        ((h.f)paramObject).a();
      }
      return x.a;
      label110:
      throw a;
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((a)a(paramObject1, (c)paramObject2)).a(x.a);
    }
  }
  
  static final class b<R>
    implements ac<HistoryEvent>
  {
    b(Number paramNumber, i parami) {}
  }
  
  @c.d.b.a.f(b="SelectNumberPresenter.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.select_number.SelectNumberPresenter$maybeSetDefaultNumber$1")
  static final class c
    extends c.d.b.a.k
    implements c.g.a.m<ag, c<? super x>, Object>
  {
    int a;
    private ag d;
    
    c(i parami, String paramString, c paramc)
    {
      super(paramc);
    }
    
    public final c<x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new c(b, paramNumber, paramc);
      d = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      Object localObject = c.d.a.a.a;
      if (a == 0)
      {
        if (!(paramObject instanceof o.b))
        {
          try
          {
            paramObject = b.e;
            localObject = paramNumber;
            Contact localContact = b.a;
            if (localContact == null) {
              c.g.b.k.a("contact");
            }
            ((com.truecaller.data.access.m)paramObject).a((String)localObject, localContact.E());
          }
          catch (Exception paramObject)
          {
            d.a((Throwable)paramObject, "Failed to set as default number");
          }
          return x.a;
        }
        throw a;
      }
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((c)a(paramObject1, (c)paramObject2)).a(x.a);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.c.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */