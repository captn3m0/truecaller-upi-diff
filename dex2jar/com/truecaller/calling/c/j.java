package com.truecaller.calling.c;

import c.d.f;
import com.truecaller.callhistory.a;
import com.truecaller.calling.ar;
import com.truecaller.data.access.m;
import com.truecaller.multisim.h;
import com.truecaller.utils.n;
import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d<i>
{
  private final Provider<ar> a;
  private final Provider<h> b;
  private final Provider<a> c;
  private final Provider<n> d;
  private final Provider<m> e;
  private final Provider<f> f;
  private final Provider<f> g;
  
  private j(Provider<ar> paramProvider, Provider<h> paramProvider1, Provider<a> paramProvider2, Provider<n> paramProvider3, Provider<m> paramProvider4, Provider<f> paramProvider5, Provider<f> paramProvider6)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
  }
  
  public static j a(Provider<ar> paramProvider, Provider<h> paramProvider1, Provider<a> paramProvider2, Provider<n> paramProvider3, Provider<m> paramProvider4, Provider<f> paramProvider5, Provider<f> paramProvider6)
  {
    return new j(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.c.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */