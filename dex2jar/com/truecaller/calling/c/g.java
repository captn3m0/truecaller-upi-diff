package com.truecaller.calling.c;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import c.f;
import com.truecaller.adapter_delegates.i;
import com.truecaller.adapter_delegates.k;
import com.truecaller.calling.ag;
import com.truecaller.calling.ap;
import com.truecaller.calling.ba;
import com.truecaller.calling.bc;
import com.truecaller.calling.c;
import com.truecaller.calling.d;
import com.truecaller.calling.dialer.CallIconType;
import com.truecaller.search.local.model.c.a;
import com.truecaller.utils.extensions.t;

public final class g
  extends RecyclerView.ViewHolder
  implements ag, bc, c, h.c
{
  private final f b;
  private final f c;
  private final f d;
  
  public g(View paramView, k paramk)
  {
    super(paramView);
    e = new ba(paramView);
    f = new ap(paramView);
    g = new d(paramView);
    b = t.a(paramView, 2131361918);
    c = t.a(paramView, 2131361939);
    d = t.a(paramView, 2131361843);
    RecyclerView.ViewHolder localViewHolder = (RecyclerView.ViewHolder)this;
    i.a(paramView, paramk, localViewHolder, null, 12);
    paramView = a();
    if (paramView != null) {
      i.a(paramView, paramk, localViewHolder, "ItemEvent.ACTION_SIM_TWO_CLICKED", 8);
    }
  }
  
  private final View a()
  {
    return (View)d.b();
  }
  
  public final void a(int paramInt)
  {
    ImageView localImageView = (ImageView)b.b();
    if (localImageView != null)
    {
      localImageView.setImageResource(paramInt);
      return;
    }
  }
  
  public final void a(CallIconType paramCallIconType)
  {
    f.a(paramCallIconType);
  }
  
  public final void a(c.a parama)
  {
    g.a(parama);
  }
  
  public final void a(Integer paramInteger)
  {
    f.a(paramInteger);
  }
  
  public final void a(Long paramLong)
  {
    f.a(paramLong);
  }
  
  public final void a(boolean paramBoolean)
  {
    f.a(paramBoolean);
  }
  
  public final void b(String paramString)
  {
    f.b(paramString);
  }
  
  public final void b(boolean paramBoolean)
  {
    f.b(paramBoolean);
  }
  
  public final void b_(String paramString)
  {
    e.b_(paramString);
  }
  
  public final void c(boolean paramBoolean)
  {
    View localView = (View)c.b();
    if (localView != null) {
      t.a(localView, paramBoolean);
    }
    localView = a();
    if (localView != null)
    {
      t.a(localView, paramBoolean);
      return;
    }
  }
  
  public final void e_(String paramString)
  {
    f.e_(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.c.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */