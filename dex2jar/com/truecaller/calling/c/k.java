package com.truecaller.calling.c;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import c.g.b.l;
import com.truecaller.adapter_delegates.a;
import com.truecaller.adapter_delegates.p;
import com.truecaller.utils.extensions.t;

public final class k
  implements h.g
{
  private final c.f b;
  private final c.f c;
  private final p<h.c, g> d;
  private final com.truecaller.adapter_delegates.f e;
  private final View f;
  private final h.g.a g;
  
  public k(View paramView, h.g.a parama, h.b paramb, final boolean paramBoolean)
  {
    f = paramView;
    g = parama;
    b = t.a(f, 2131364093);
    c = t.a(f, 2131364308);
    d = new p((com.truecaller.adapter_delegates.b)paramb, 2131559018, (c.g.a.b)new b(this), (c.g.a.b)c.a);
    e = new com.truecaller.adapter_delegates.f((a)d);
    paramView = (RecyclerView)b.b();
    paramView.setAdapter((RecyclerView.Adapter)e);
    paramView.setLayoutManager((RecyclerView.LayoutManager)new LinearLayoutManager(f.getContext()));
    paramView.setItemAnimator(null);
    paramView = (CheckBox)c.b();
    t.a((View)paramView, paramBoolean);
    paramView.setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)new a(this, paramBoolean));
  }
  
  public final void a(int paramInt)
  {
    e.notifyItemInserted(paramInt);
  }
  
  static final class a
    implements CompoundButton.OnCheckedChangeListener
  {
    a(k paramk, boolean paramBoolean) {}
    
    public final void onCheckedChanged(CompoundButton paramCompoundButton, boolean paramBoolean)
    {
      k.b(a).a(paramBoolean);
    }
  }
  
  static final class b
    extends l
    implements c.g.a.b<View, g>
  {
    b(k paramk)
    {
      super();
    }
  }
  
  static final class c
    extends l
    implements c.g.a.b<g, g>
  {
    public static final c a = new c();
    
    c()
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.c.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */