package com.truecaller.calling.c;

import c.g.b.k;
import com.truecaller.adapter_delegates.h;
import com.truecaller.calling.dialer.ax;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.multisim.ae;
import com.truecaller.util.af;
import com.truecaller.util.cf;
import com.truecaller.utils.n;
import java.util.ArrayList;
import javax.inject.Inject;

public final class e
  extends com.truecaller.adapter_delegates.c<h.c>
  implements h.b
{
  private final h.d c;
  private final h.a d;
  private final af e;
  private final ae f;
  private final com.truecaller.search.local.model.c g;
  private final n h;
  private final ax i;
  private final cf j;
  
  @Inject
  public e(h.d paramd, h.a parama, af paramaf, ae paramae, com.truecaller.search.local.model.c paramc, n paramn, ax paramax, cf paramcf)
  {
    d = parama;
    e = paramaf;
    f = paramae;
    g = paramc;
    h = paramn;
    i = paramax;
    j = paramcf;
    c = paramd;
  }
  
  private final b a()
  {
    return c.a((h.b)this, b[0]);
  }
  
  public final boolean a(h paramh)
  {
    k.b(paramh, "event");
    Object localObject = ad.get(b);
    k.a(localObject, "data.items[event.position]");
    d locald = (d)localObject;
    localObject = b;
    if (localObject != null)
    {
      localObject = ((HistoryEvent)localObject).s();
      if (localObject != null)
      {
        localObject = ((Contact)localObject).s();
        break label62;
      }
    }
    localObject = null;
    label62:
    k.a(a, "ItemEvent.ACTION_SIM_TWO_CLICKED");
    d.a(a, (String)localObject);
    return true;
  }
  
  public final int getItemCount()
  {
    return ad.size();
  }
  
  public final long getItemId(int paramInt)
  {
    return -1L;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.c.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */