package com.truecaller.calling.c;

import c.g.b.k;
import java.util.ArrayList;

public final class b
{
  final boolean a;
  final boolean b;
  final boolean c;
  final ArrayList<d> d;
  
  public b()
  {
    this(false, false, false, null, 15);
  }
  
  private b(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, ArrayList<d> paramArrayList)
  {
    a = paramBoolean1;
    b = paramBoolean2;
    c = paramBoolean3;
    d = paramArrayList;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      if ((paramObject instanceof b))
      {
        paramObject = (b)paramObject;
        int i;
        if (a == a) {
          i = 1;
        } else {
          i = 0;
        }
        if (i != 0)
        {
          if (b == b) {
            i = 1;
          } else {
            i = 0;
          }
          if (i != 0)
          {
            if (c == c) {
              i = 1;
            } else {
              i = 0;
            }
            if ((i != 0) && (k.a(d, d))) {
              return true;
            }
          }
        }
      }
      return false;
    }
    return true;
  }
  
  public final int hashCode()
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("SelectNumberData(multiSim=");
    localStringBuilder.append(a);
    localStringBuilder.append(", sms=");
    localStringBuilder.append(b);
    localStringBuilder.append(", voip=");
    localStringBuilder.append(c);
    localStringBuilder.append(", items=");
    localStringBuilder.append(d);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.c.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */