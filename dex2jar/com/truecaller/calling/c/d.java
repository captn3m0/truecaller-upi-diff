package com.truecaller.calling.c;

import c.g.b.k;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.Number;

public final class d
{
  final Number a;
  final HistoryEvent b;
  
  public d(Number paramNumber, HistoryEvent paramHistoryEvent)
  {
    a = paramNumber;
    b = paramHistoryEvent;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject) {
      if ((paramObject instanceof d))
      {
        paramObject = (d)paramObject;
        if ((k.a(a, a)) && (k.a(b, b))) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    Object localObject = a;
    int j = 0;
    int i;
    if (localObject != null) {
      i = localObject.hashCode();
    } else {
      i = 0;
    }
    localObject = b;
    if (localObject != null) {
      j = localObject.hashCode();
    }
    return i * 31 + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("SelectNumberItem(number=");
    localStringBuilder.append(a);
    localStringBuilder.append(", historyEvent=");
    localStringBuilder.append(b);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.c.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */