package com.truecaller.calling.c;

import c.d.a.a;
import c.d.b.a.f;
import c.d.c;
import c.o.b;
import c.x;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.d;
import kotlinx.coroutines.ag;

@f(b="SelectNumberPresenter.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.select_number.SelectNumberPresenter$maybeSetDefaultNumber$1")
final class i$c
  extends c.d.b.a.k
  implements c.g.a.m<ag, c<? super x>, Object>
{
  int a;
  private ag d;
  
  i$c(i parami, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new c(b, c, paramc);
    d = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    if (a == 0)
    {
      if (!(paramObject instanceof o.b))
      {
        try
        {
          paramObject = b.e;
          localObject = c;
          Contact localContact = b.a;
          if (localContact == null) {
            c.g.b.k.a("contact");
          }
          ((com.truecaller.data.access.m)paramObject).a((String)localObject, localContact.E());
        }
        catch (Exception paramObject)
        {
          d.a((Throwable)paramObject, "Failed to set as default number");
        }
        return x.a;
      }
      throw a;
    }
    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((c)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.c.i.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */