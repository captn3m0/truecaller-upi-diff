package com.truecaller.calling.c;

import com.truecaller.calling.dialer.ax;
import com.truecaller.multisim.ae;
import com.truecaller.search.local.model.c;
import com.truecaller.util.af;
import com.truecaller.util.cf;
import com.truecaller.utils.n;
import dagger.a.d;
import javax.inject.Provider;

public final class f
  implements d<e>
{
  private final Provider<h.d> a;
  private final Provider<h.a> b;
  private final Provider<af> c;
  private final Provider<ae> d;
  private final Provider<c> e;
  private final Provider<n> f;
  private final Provider<ax> g;
  private final Provider<cf> h;
  
  private f(Provider<h.d> paramProvider, Provider<h.a> paramProvider1, Provider<af> paramProvider2, Provider<ae> paramProvider3, Provider<c> paramProvider4, Provider<n> paramProvider5, Provider<ax> paramProvider6, Provider<cf> paramProvider7)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
  }
  
  public static f a(Provider<h.d> paramProvider, Provider<h.a> paramProvider1, Provider<af> paramProvider2, Provider<ae> paramProvider3, Provider<c> paramProvider4, Provider<n> paramProvider5, Provider<ax> paramProvider6, Provider<cf> paramProvider7)
  {
    return new f(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.c.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */