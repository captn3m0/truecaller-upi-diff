package com.truecaller.calling;

import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import com.truecaller.callerid.CallerIdService;
import com.truecaller.callerid.a.d;
import com.truecaller.common.h.u;
import com.truecaller.log.AssertionUtil;
import com.truecaller.service.CallStateService;
import com.truecaller.utils.l;

public final class al
  implements ak
{
  private final k a;
  private final f b;
  private final l c;
  private final com.truecaller.utils.a d;
  private final d e;
  private final u f;
  
  public al(k paramk, f paramf, l paraml, com.truecaller.utils.a parama, d paramd, u paramu)
  {
    a = paramk;
    b = paramf;
    c = paraml;
    d = parama;
    e = paramd;
    f = paramu;
  }
  
  public final void a(Context paramContext, Intent paramIntent)
  {
    c.g.b.k.b(paramContext, "context");
    c.g.b.k.b(paramIntent, "intent");
    if (!c.a(new String[] { "android.permission.READ_PHONE_STATE", "android.permission.READ_CONTACTS", "android.permission.WRITE_CONTACTS" })) {
      return;
    }
    Object localObject1 = aj.c;
    localObject1 = d;
    c.g.b.k.b(paramIntent, "intent");
    c.g.b.k.b(localObject1, "clock");
    Object localObject2 = paramIntent.getStringExtra("state");
    paramIntent = paramIntent.getStringExtra("incoming_number");
    if (c.g.b.k.a(localObject2, TelephonyManager.EXTRA_STATE_IDLE))
    {
      paramIntent = (aj)new aj.b(paramIntent, ((com.truecaller.utils.a)localObject1).a());
    }
    else if (c.g.b.k.a(localObject2, TelephonyManager.EXTRA_STATE_RINGING))
    {
      paramIntent = (aj)new aj.e(paramIntent, ((com.truecaller.utils.a)localObject1).a());
    }
    else if (c.g.b.k.a(localObject2, TelephonyManager.EXTRA_STATE_OFFHOOK))
    {
      paramIntent = (aj)new aj.c(paramIntent, ((com.truecaller.utils.a)localObject1).a());
    }
    else
    {
      AssertionUtil.reportWeirdnessButNeverCrash("Unknown state ".concat(String.valueOf(localObject2)));
      paramIntent = null;
    }
    if (paramIntent == null) {
      return;
    }
    localObject1 = a;
    if (localObject1 != null)
    {
      localObject1 = f.b((String)localObject1);
      if (localObject1 != null)
      {
        localObject2 = e;
        c.g.b.k.b(localObject1, "normalizedNumber");
        b.a((String)localObject1, 2131365509);
      }
    }
    paramIntent = a.a(paramContext, paramIntent);
    if (paramIntent == null) {
      return;
    }
    CallerIdService.a(paramContext, b.a(paramIntent).a());
    if (!CallStateService.a()) {
      CallStateService.a(paramContext);
    }
  }
  
  public final void b(Context paramContext, Intent paramIntent)
  {
    c.g.b.k.b(paramContext, "context");
    c.g.b.k.b(paramIntent, "intent");
    if (!c.a(new String[] { "android.permission.READ_PHONE_STATE", "android.permission.READ_CONTACTS", "android.permission.WRITE_CONTACTS", "android.permission.PROCESS_OUTGOING_CALLS" })) {
      return;
    }
    com.truecaller.util.a.a(paramContext);
    k localk = a;
    Object localObject = aj.c;
    localObject = d;
    c.g.b.k.b(paramIntent, "intent");
    c.g.b.k.b(localObject, "clock");
    paramIntent = localk.a(new aj.d(paramIntent.getStringExtra("android.intent.extra.PHONE_NUMBER"), ((com.truecaller.utils.a)localObject).a()));
    if (paramIntent == null) {
      return;
    }
    CallerIdService.a(paramContext, paramIntent.a());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.al
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */