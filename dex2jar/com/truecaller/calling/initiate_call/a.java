package com.truecaller.calling.initiate_call;

import android.net.Uri;
import android.telecom.PhoneAccountHandle;
import c.g.b.k;

public final class a
{
  final String a;
  final Uri b;
  final PhoneAccountHandle c;
  final String d;
  final boolean e;
  private final String f;
  private final String g;
  
  public a(String paramString1, String paramString2, Uri paramUri, PhoneAccountHandle paramPhoneAccountHandle, String paramString3, boolean paramBoolean, String paramString4)
  {
    a = paramString1;
    f = paramString2;
    b = paramUri;
    c = paramPhoneAccountHandle;
    d = paramString3;
    e = paramBoolean;
    g = paramString4;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      if ((paramObject instanceof a))
      {
        paramObject = (a)paramObject;
        if ((k.a(a, a)) && (k.a(f, f)) && (k.a(b, b)) && (k.a(c, c)) && (k.a(d, d)))
        {
          int i;
          if (e == e) {
            i = 1;
          } else {
            i = 0;
          }
          if ((i != 0) && (k.a(g, g))) {
            return true;
          }
        }
      }
      return false;
    }
    return true;
  }
  
  public final int hashCode()
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("CallIntent(action=");
    localStringBuilder.append(a);
    localStringBuilder.append(", analyticsContext=");
    localStringBuilder.append(f);
    localStringBuilder.append(", uri=");
    localStringBuilder.append(b);
    localStringBuilder.append(", sipAccount=");
    localStringBuilder.append(c);
    localStringBuilder.append(", simToken=");
    localStringBuilder.append(d);
    localStringBuilder.append(", isVideoCall=");
    localStringBuilder.append(e);
    localStringBuilder.append(", normalizedNumber=");
    localStringBuilder.append(g);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.initiate_call.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */