package com.truecaller.calling.initiate_call;

import com.truecaller.multisim.h;
import com.truecaller.service.a;
import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d<f>
{
  private final Provider<h> a;
  private final Provider<a> b;
  
  private g(Provider<h> paramProvider, Provider<a> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static g a(Provider<h> paramProvider, Provider<a> paramProvider1)
  {
    return new g(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.initiate_call.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */