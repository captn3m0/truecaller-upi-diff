package com.truecaller.calling.initiate_call;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import c.g.b.k;
import com.truecaller.bd;

final class SelectPhoneAccountActivity$b
  implements DialogInterface.OnClickListener
{
  SelectPhoneAccountActivity$b(SelectPhoneAccountActivity paramSelectPhoneAccountActivity, i parami) {}
  
  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    paramDialogInterface = (h)b.getItem(paramInt);
    if (paramDialogInterface == null) {
      return;
    }
    l locall = a.a();
    k.b(paramDialogInterface, "phoneAccountInfo");
    k.a locala;
    String str1;
    String str2;
    String str3;
    if ((paramDialogInterface instanceof h.b))
    {
      locala = (k.a)c;
      if (locala != null)
      {
        str1 = a;
        if (str1 == null) {
          k.a("number");
        }
        str2 = e;
        if (str2 == null) {
          k.a("analyticsContext");
        }
        str3 = d;
        if (str3 == null) {
          k.a("displayName");
        }
        locala.a(str1, str2, str3, null, d, f);
      }
    }
    else if ((paramDialogInterface instanceof h.a))
    {
      locala = (k.a)c;
      if (locala != null)
      {
        str1 = a;
        if (str1 == null) {
          k.a("number");
        }
        str2 = e;
        if (str2 == null) {
          k.a("analyticsContext");
        }
        str3 = d;
        if (str3 == null) {
          k.a("displayName");
        }
        locala.a(str1, str2, str3, Integer.valueOf(d), null, f);
      }
    }
    paramDialogInterface = (k.b)b;
    if (paramDialogInterface != null)
    {
      paramDialogInterface.b();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.initiate_call.SelectPhoneAccountActivity.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */