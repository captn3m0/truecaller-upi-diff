package com.truecaller.calling.initiate_call;

import android.app.Activity;
import android.content.Context;
import c.d.c;

public abstract interface e
{
  public abstract Object a(Context paramContext, String paramString1, String paramString2, c<? super Boolean> paramc);
  
  public abstract void a(Activity paramActivity);
  
  public abstract void a(Context paramContext, a parama);
  
  public abstract void a(Context paramContext, String paramString1, String paramString2, String paramString3, boolean paramBoolean);
  
  public abstract boolean a(Context paramContext);
  
  public abstract void b(Activity paramActivity);
}

/* Location:
 * Qualified Name:     com.truecaller.calling.initiate_call.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */