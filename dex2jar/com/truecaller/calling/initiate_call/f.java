package com.truecaller.calling.initiate_call;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v4.content.d;
import android.support.v7.app.AlertDialog.Builder;
import android.telecom.PhoneAccountHandle;
import c.d.c;
import c.g.b.k;
import com.truecaller.common.h.o;
import com.truecaller.multisim.h;
import javax.inject.Inject;

public final class f
  implements e
{
  private final h a;
  private final com.truecaller.service.a b;
  
  @Inject
  public f(h paramh, com.truecaller.service.a parama)
  {
    a = paramh;
    b = parama;
  }
  
  public final Object a(Context paramContext, String paramString1, String paramString2, c<? super Boolean> paramc)
  {
    return b.a(paramContext, paramString1, paramString2, paramc);
  }
  
  public final void a(Activity paramActivity)
  {
    k.b(paramActivity, "activity");
    new AlertDialog.Builder((Context)paramActivity).setTitle(2131887937).setMessage(2131887936).setPositiveButton(2131887217, null).show();
  }
  
  public final void a(Context paramContext, a parama)
  {
    k.b(paramContext, "context");
    k.b(parama, "callIntent");
    String str2 = a;
    Uri localUri = b;
    PhoneAccountHandle localPhoneAccountHandle = c;
    String str1 = d;
    boolean bool = e;
    parama = new Intent(str2, localUri);
    if (localPhoneAccountHandle != null) {
      parama.putExtra("android.telecom.extra.PHONE_ACCOUNT_HANDLE", (Parcelable)localPhoneAccountHandle);
    } else if (str1 != null) {
      a.a(parama, str1);
    }
    if (bool)
    {
      parama.putExtra("android.telecom.extra.START_CALL_WITH_VIDEO_STATE", 3);
      parama.putExtra("com.android.phone.extra.video", true);
    }
    parama.addFlags(268435456);
    try
    {
      paramContext.startActivity(parama);
      d.a(paramContext).b(parama);
      return;
    }
    catch (ActivityNotFoundException paramContext) {}catch (SecurityException paramContext) {}
  }
  
  public final void a(Context paramContext, String paramString1, String paramString2, String paramString3, boolean paramBoolean)
  {
    k.b(paramContext, "context");
    k.b(paramString1, "number");
    k.b(paramString2, "displayName");
    k.b(paramString3, "analyticsContext");
    Object localObject = SelectPhoneAccountActivity.c;
    k.b(paramContext, "context");
    k.b(paramString1, "number");
    k.b(paramString2, "displayName");
    k.b(paramString3, "analyticsContext");
    localObject = new Intent(paramContext, SelectPhoneAccountActivity.class);
    ((Intent)localObject).putExtra("extraNumber", paramString1);
    ((Intent)localObject).putExtra("extraDisplayName", paramString2);
    ((Intent)localObject).putExtra("extraAnalyticsContext", paramString3);
    ((Intent)localObject).putExtra("noCallMeBack", paramBoolean);
    ((Intent)localObject).addFlags(268435456);
    paramContext.startActivity((Intent)localObject);
  }
  
  @SuppressLint({"InlinedApi"})
  public final boolean a(Context paramContext)
  {
    k.b(paramContext, "activity");
    return o.a(paramContext, new Intent("android.telephony.action.CONFIGURE_VOICEMAIL"));
  }
  
  public final void b(Activity paramActivity)
  {
    k.b(paramActivity, "activity");
    Intent localIntent = new Intent("android.intent.action.CALL", Uri.fromParts("voicemail", "", null));
    o.a((Context)paramActivity, localIntent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.initiate_call.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */