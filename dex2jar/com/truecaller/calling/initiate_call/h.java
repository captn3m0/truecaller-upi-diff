package com.truecaller.calling.initiate_call;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.telecom.PhoneAccountHandle;

public abstract class h
{
  final CharSequence a;
  final CharSequence b;
  
  private h(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
  {
    a = paramCharSequence1;
    b = paramCharSequence2;
  }
  
  public static final class a
    extends h
  {
    final Drawable c;
    final int d;
    
    public a(CharSequence paramCharSequence1, CharSequence paramCharSequence2, Drawable paramDrawable, int paramInt)
    {
      super(paramCharSequence2, (byte)0);
      c = paramDrawable;
      d = paramInt;
    }
  }
  
  public static final class b
    extends h
  {
    final Icon c;
    final PhoneAccountHandle d;
    
    public b(CharSequence paramCharSequence1, CharSequence paramCharSequence2, Icon paramIcon, PhoneAccountHandle paramPhoneAccountHandle)
    {
      super(paramCharSequence2, (byte)0);
      c = paramIcon;
      d = paramPhoneAccountHandle;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.initiate_call.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */