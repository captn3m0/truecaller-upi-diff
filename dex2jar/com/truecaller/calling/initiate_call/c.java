package com.truecaller.calling.initiate_call;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.telecom.PhoneAccountHandle;
import android.telephony.TelephonyManager;
import c.o.b;
import c.u;
import c.x;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.e.a;
import com.truecaller.callerid.ai;
import com.truecaller.calling.ar;
import com.truecaller.common.h.aj;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.g;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.h;
import com.truecaller.tracking.events.f.a;
import com.truecaller.util.bz;
import com.truecaller.util.ca;
import com.truecaller.utils.l;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

public final class c
  implements b
{
  final bz a;
  final com.truecaller.androidactors.f<ae> b;
  final com.truecaller.analytics.b c;
  final l d;
  final g e;
  final h f;
  final ar g;
  final com.truecaller.utils.d h;
  final com.truecaller.i.c i;
  final com.truecaller.calling.b.c j;
  final aj k;
  final e l;
  final Context m;
  private final TelephonyManager n;
  private final c.d.f o;
  
  @Inject
  public c(bz parambz, com.truecaller.androidactors.f<ae> paramf, com.truecaller.analytics.b paramb, l paraml, g paramg, h paramh, ar paramar, com.truecaller.utils.d paramd, com.truecaller.i.c paramc, com.truecaller.calling.b.c paramc1, TelephonyManager paramTelephonyManager, aj paramaj, e parame, Context paramContext, @Named("UI") c.d.f paramf1)
  {
    a = parambz;
    b = paramf;
    c = paramb;
    d = paraml;
    e = paramg;
    f = paramh;
    g = paramar;
    h = paramd;
    i = paramc;
    j = paramc1;
    n = paramTelephonyManager;
    k = paramaj;
    l = parame;
    m = paramContext;
    o = paramf1;
  }
  
  @SuppressLint({"InlinedApi", "MissingPermission"})
  public final void a(Activity paramActivity)
  {
    c.g.b.k.b(paramActivity, "activity");
    boolean bool = d.a(new String[] { "android.permission.READ_PHONE_STATE", "android.permission.CALL_PHONE" });
    int i2 = 1;
    int i1;
    if (bool)
    {
      CharSequence localCharSequence = (CharSequence)n.getVoiceMailNumber();
      if ((localCharSequence != null) && (localCharSequence.length() != 0)) {
        i1 = 0;
      } else {
        i1 = 1;
      }
      if (i1 == 0)
      {
        l.b(paramActivity);
        return;
      }
    }
    if (h.h() >= 23) {
      i1 = i2;
    } else {
      i1 = 0;
    }
    if ((i1 != 0) && (l.a((Context)paramActivity))) {
      return;
    }
    l.a(paramActivity);
  }
  
  public final void a(final b.a parama)
  {
    c.g.b.k.b(parama, "callOptions");
    kotlinx.coroutines.e.b((ag)bg.a, o, (c.g.a.m)new a(this, parama, null), 2);
  }
  
  @c.d.b.a.f(b="InitiateCallHelper.kt", c={126}, d="invokeSuspend", e="com.truecaller.calling.initiate_call.InitiateCallHelperImpl$callTo$1")
  static final class a
    extends c.d.b.a.k
    implements c.g.a.m<ag, c.d.c<? super x>, Object>
  {
    Object a;
    Object b;
    Object c;
    int d;
    private ag g;
    
    a(c paramc, b.a parama, c.d.c paramc1)
    {
      super(paramc1);
    }
    
    public final c.d.c<x> a(Object paramObject, c.d.c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new a(e, parama, paramc);
      g = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      Object localObject6 = c.d.a.a.a;
      int i = d;
      boolean bool2 = true;
      switch (i)
      {
      default: 
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
      case 1: 
        localObject2 = (String)c;
        localObject4 = (Number)b;
        localObject1 = (e.a)a;
        if (!(paramObject instanceof o.b))
        {
          localObject3 = paramObject;
          paramObject = localObject4;
        }
        else
        {
          throw a;
        }
        break;
      case 0: 
        if ((paramObject instanceof o.b)) {
          break label1424;
        }
        if (paramaa == null) {
          break label1407;
        }
        if (((CharSequence)paramaa).length() == 0) {
          i = 1;
        } else {
          i = 0;
        }
        if (i != 0) {
          break label1407;
        }
        paramObject = new e.a("TruecallerCallInitiated").a("Context", paramab);
        if (!e.k.b(paramaa)) {
          return x.a;
        }
        localObject1 = e.a.a(paramaa);
        if (localObject1 != null)
        {
          ((ca)localObject1).a(e.m);
          return x.a;
        }
        localObject4 = e.e.a(new String[] { paramaa });
        if (localObject4 == null) {
          return x.a;
        }
        c.g.b.k.a(localObject4, "numberProvider.provideFr….number) ?: return@launch");
        localObject5 = paramaa;
        localObject1 = localObject5;
        localObject3 = localObject4;
        localObject2 = paramObject;
        if (paramaf) {
          break label432;
        }
        localObject1 = e.l;
        localObject2 = e.m;
        localObject3 = paramaa;
        str = ((Number)localObject4).a();
        a = paramObject;
        b = localObject4;
        c = localObject5;
        d = 1;
        localObject3 = ((e)localObject1).a((Context)localObject2, (String)localObject3, str, this);
        if (localObject3 == localObject6) {
          return localObject6;
        }
        localObject2 = localObject5;
        localObject1 = paramObject;
        paramObject = localObject4;
      }
      if (((Boolean)localObject3).booleanValue()) {
        return x.a;
      }
      Object localObject4 = localObject2;
      Object localObject2 = localObject1;
      Object localObject3 = paramObject;
      Object localObject1 = localObject4;
      label432:
      paramObject = e;
      localObject4 = paramad;
      Object localObject5 = paramag;
      c.g.b.k.b(localObject1, "phoneNumber");
      if ((f.e()) && (f.k()))
      {
        if (((localObject4 == null) || (((Integer)localObject4).intValue() != 0)) && ((localObject4 == null) || (((Integer)localObject4).intValue() != 1))) {
          i = 0;
        } else {
          i = 1;
        }
        int j;
        if (localObject5 != null) {
          j = 1;
        } else {
          j = 0;
        }
        if ((i == 0) && (j == 0))
        {
          if (k.a((String)localObject1))
          {
            i = 0;
          }
          else
          {
            if (j.a().size() > 1) {
              i = 1;
            } else {
              i = 0;
            }
            if (i == 0) {
              i = 0;
            } else if ((c.g.b.k.a(g.e(), "-1") ^ true)) {
              i = 0;
            } else {
              i = 1;
            }
          }
        }
        else {
          i = 0;
        }
      }
      else
      {
        i = 0;
      }
      if (i != 0)
      {
        localObject2 = e.l;
        localObject3 = e.m;
        paramObject = paramac;
        if (paramObject == null) {
          paramObject = localObject1;
        }
        ((e)localObject2).a((Context)localObject3, (String)localObject1, (String)paramObject, paramab, paramaf);
        return x.a;
      }
      paramObject = e;
      localObject4 = paramab;
      i.b("key_temp_latest_call_made_with_tc", true);
      i.b("lastCallMadeWithTcTime", System.currentTimeMillis());
      localObject5 = (CharSequence)i.a("key_last_call_origin");
      if ((localObject5 != null) && (((CharSequence)localObject5).length() != 0)) {
        i = 0;
      } else {
        i = 1;
      }
      if (i != 0) {
        i.a("key_last_call_origin", (String)localObject4);
      }
      Object localObject7 = e;
      localObject6 = paramab;
      localObject5 = paramag;
      boolean bool1 = paramae;
      paramObject = paramad;
      String str = ((Number)localObject3).a();
      c.g.b.k.a(str, "parsedNumber.normalizedNumber");
      if (d.a(new String[] { "android.permission.CALL_PRIVILEGED" })) {
        localObject4 = "android.intent.action.CALL_PRIVILEGED";
      } else {
        localObject4 = "android.intent.action.CALL";
      }
      if (h.h() >= 23) {
        i = 1;
      } else {
        i = 0;
      }
      if ((i == 0) || (localObject5 == null)) {
        localObject5 = null;
      }
      if ((i != 0) && (bool1)) {
        bool1 = true;
      } else {
        bool1 = false;
      }
      if ((f.k()) && (f.e()))
      {
        if (paramObject == null)
        {
          paramObject = g.e();
        }
        else
        {
          paramObject = f.a(((Integer)paramObject).intValue());
          if (paramObject != null) {
            paramObject = b;
          } else {
            paramObject = null;
          }
        }
      }
      else {
        paramObject = null;
      }
      localObject7 = k;
      CharSequence localCharSequence = (CharSequence)localObject1;
      if (((aj)localObject7).a(localCharSequence))
      {
        if (localObject1 != null)
        {
          localObject1 = c.n.m.b(localCharSequence).toString();
          if (c.n.m.b((String)localObject1, "sip:", false)) {
            localObject1 = Uri.parse((String)localObject1);
          } else {
            localObject1 = Uri.fromParts("sip", (String)localObject1, null);
          }
        }
        else
        {
          throw new u("null cannot be cast to non-null type kotlin.CharSequence");
        }
      }
      else {
        localObject1 = Uri.fromParts("tel", (String)localObject1, null);
      }
      localObject1 = new a((String)localObject4, (String)localObject6, (Uri)localObject1, (PhoneAccountHandle)localObject5, (String)paramObject, bool1, str);
      paramObject = e;
      localObject4 = paramab;
      localObject5 = com.truecaller.tracking.events.f.b();
      ((f.a)localObject5).b((CharSequence)localObject4);
      ((f.a)localObject5).a((CharSequence)((Number)localObject3).b());
      ((f.a)localObject5).c((CharSequence)ai.a());
      try
      {
        ((ae)b.a()).a((org.apache.a.d.d)((f.a)localObject5).a());
      }
      catch (org.apache.a.a paramObject)
      {
        AssertionUtil.reportThrowableButNeverCrash((Throwable)paramObject);
      }
      localObject3 = e;
      c.g.b.k.a(localObject2, "event");
      ((e.a)localObject2).a("IsVideo", e);
      if (c == null) {
        paramObject = "PSTN";
      } else {
        paramObject = "SIP";
      }
      ((e.a)localObject2).a("Network", (String)paramObject);
      ((e.a)localObject2).a("IntentAction", a);
      if (d != null) {
        bool1 = bool2;
      } else {
        bool1 = false;
      }
      ((e.a)localObject2).a("IsSpecificSim", bool1);
      paramObject = c;
      localObject2 = ((e.a)localObject2).a();
      c.g.b.k.a(localObject2, "event.build()");
      ((com.truecaller.analytics.b)paramObject).b((com.truecaller.analytics.e)localObject2);
      e.l.a(e.m, (a)localObject1);
      return x.a;
      label1407:
      AssertionUtil.OnlyInDebug.fail(new String[] { "Cannot call null or empty number" });
      return x.a;
      label1424:
      throw a;
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((a)a(paramObject1, (c.d.c)paramObject2)).a(x.a);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.initiate_call.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */