package com.truecaller.calling.initiate_call;

import android.telecom.PhoneAccountHandle;

public final class b$a$a
{
  PhoneAccountHandle a;
  private String b;
  private Integer c;
  private boolean d;
  private boolean e;
  private final String f;
  private final String g;
  
  public b$a$a(String paramString1, String paramString2)
  {
    f = paramString1;
    g = paramString2;
  }
  
  public final a a(Integer paramInteger)
  {
    a locala = (a)this;
    c = paramInteger;
    return locala;
  }
  
  public final a a(String paramString)
  {
    a locala = (a)this;
    b = paramString;
    return locala;
  }
  
  public final a a(boolean paramBoolean)
  {
    a locala = (a)this;
    d = paramBoolean;
    return locala;
  }
  
  public final b.a a()
  {
    return new b.a(f, g, b, c, d, e, a, (byte)0);
  }
  
  public final a b(boolean paramBoolean)
  {
    a locala = (a)this;
    e = paramBoolean;
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.initiate_call.b.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */