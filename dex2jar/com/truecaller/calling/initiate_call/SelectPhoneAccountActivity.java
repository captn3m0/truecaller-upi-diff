package com.truecaller.calling.initiate_call;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources.Theme;
import android.os.Bundle;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.telecom.PhoneAccountHandle;
import android.widget.ListAdapter;
import c.g.b.k;
import c.u;
import com.truecaller.bd;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.calling.b.c;
import com.truecaller.common.h.ab;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.utils.extensions.a;
import java.util.List;
import javax.inject.Inject;

public final class SelectPhoneAccountActivity
  extends AppCompatActivity
  implements k.a, k.b
{
  public static final a c = new a((byte)0);
  @Inject
  public l a;
  @Inject
  public b b;
  
  public final l a()
  {
    l locall = a;
    if (locall == null) {
      k.a("presenter");
    }
    return locall;
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, Integer paramInteger, PhoneAccountHandle paramPhoneAccountHandle, boolean paramBoolean)
  {
    k.b(paramString1, "number");
    k.b(paramString2, "analyticsContext");
    paramString1 = (b.a.a)new b.a.a(paramString1, paramString2).a(paramString3).a(paramInteger);
    a = paramPhoneAccountHandle;
    paramString1 = paramString1.b(paramBoolean);
    paramString2 = b;
    if (paramString2 == null) {
      k.a("initiateCallHelper");
    }
    paramString2.a(paramString1.a());
  }
  
  public final void a(final List<? extends h> paramList, String paramString)
  {
    k.b(paramList, "accounts");
    k.b(paramString, "displayString");
    Context localContext = (Context)this;
    paramList = new i(localContext, paramList);
    new AlertDialog.Builder(localContext).setTitle((CharSequence)getString(2131887929, new Object[] { paramString })).setAdapter((ListAdapter)paramList, (DialogInterface.OnClickListener)new b(this, paramList)).setCancelable(true).setOnCancelListener((DialogInterface.OnCancelListener)new c(this)).show();
  }
  
  public final void b()
  {
    finish();
  }
  
  public final void finish()
  {
    super.finish();
    overridePendingTransition(0, 0);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    a.a(this);
    paramBundle = getTheme();
    int i = aresId;
    boolean bool2 = false;
    paramBundle.applyStyle(i, false);
    paramBundle = getApplicationContext();
    String str1;
    String str2;
    if (paramBundle != null)
    {
      ((bk)paramBundle).a().cC().a(this);
      paramBundle = a;
      if (paramBundle == null) {
        k.a("presenter");
      }
      c = this;
      paramBundle = getIntent().getStringExtra("extraNumber");
      str1 = getIntent().getStringExtra("extraDisplayName");
      str2 = getIntent().getStringExtra("extraAnalyticsContext");
      bool1 = getIntent().getBooleanExtra("noCallMeBack", false);
      localObject1 = a;
      if (localObject1 == null) {
        k.a("presenter");
      }
      ((l)localObject1).a(this);
    }
    try
    {
      Object localObject2 = a;
      if (localObject2 == null) {
        k.a("presenter");
      }
      k.a(paramBundle, "number");
      k.a(str1, "displayName");
      k.a(str2, "analyticsContext");
      k.b(paramBundle, "number");
      k.b(str1, "displayName");
      k.b(str2, "analyticsContext");
      a = paramBundle;
      d = str1;
      e = str2;
      f = bool1;
      if (!ab.a((CharSequence)paramBundle))
      {
        AssertionUtil.OnlyInDebug.fail(new String[] { "Non-callable number was passed" });
        localObject1 = (k.b)b;
        if (localObject1 == null) {
          return;
        }
        ((k.b)localObject1).b();
        return;
      }
      localObject1 = g.a();
      localObject2 = (k.b)b;
      if (localObject2 != null)
      {
        ((k.b)localObject2).a((List)localObject1, str1);
        return;
      }
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      for (;;) {}
    }
    Object localObject1 = new StringBuilder("number is null: ");
    if (paramBundle == null) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    ((StringBuilder)localObject1).append(bool1);
    ((StringBuilder)localObject1).append(", displayName is null: ");
    if (str1 == null) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    ((StringBuilder)localObject1).append(bool1);
    ((StringBuilder)localObject1).append(", analyticsContext is null: ");
    boolean bool1 = bool2;
    if (str2 == null) {
      bool1 = true;
    }
    ((StringBuilder)localObject1).append(bool1);
    AssertionUtil.reportWeirdnessButNeverCrash(((StringBuilder)localObject1).toString());
    return;
    throw new u("null cannot be cast to non-null type com.truecaller.GraphHolder");
  }
  
  public static final class a {}
  
  static final class b
    implements DialogInterface.OnClickListener
  {
    b(SelectPhoneAccountActivity paramSelectPhoneAccountActivity, i parami) {}
    
    public final void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
      paramDialogInterface = (h)paramList.getItem(paramInt);
      if (paramDialogInterface == null) {
        return;
      }
      l locall = a.a();
      k.b(paramDialogInterface, "phoneAccountInfo");
      k.a locala;
      String str1;
      String str2;
      String str3;
      if ((paramDialogInterface instanceof h.b))
      {
        locala = (k.a)c;
        if (locala != null)
        {
          str1 = a;
          if (str1 == null) {
            k.a("number");
          }
          str2 = e;
          if (str2 == null) {
            k.a("analyticsContext");
          }
          str3 = d;
          if (str3 == null) {
            k.a("displayName");
          }
          locala.a(str1, str2, str3, null, d, f);
        }
      }
      else if ((paramDialogInterface instanceof h.a))
      {
        locala = (k.a)c;
        if (locala != null)
        {
          str1 = a;
          if (str1 == null) {
            k.a("number");
          }
          str2 = e;
          if (str2 == null) {
            k.a("analyticsContext");
          }
          str3 = d;
          if (str3 == null) {
            k.a("displayName");
          }
          locala.a(str1, str2, str3, Integer.valueOf(d), null, f);
        }
      }
      paramDialogInterface = (k.b)b;
      if (paramDialogInterface != null)
      {
        paramDialogInterface.b();
        return;
      }
    }
  }
  
  static final class c
    implements DialogInterface.OnCancelListener
  {
    c(SelectPhoneAccountActivity paramSelectPhoneAccountActivity) {}
    
    public final void onCancel(DialogInterface paramDialogInterface)
    {
      paramDialogInterface = (k.b)a.a().b;
      if (paramDialogInterface != null)
      {
        paramDialogInterface.b();
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.initiate_call.SelectPhoneAccountActivity
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */