package com.truecaller.calling.initiate_call;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Icon;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.utils.extensions.t;
import java.util.List;

public final class i
  extends ArrayAdapter<h>
{
  public i(Context paramContext, List<? extends h> paramList)
  {
    super(paramContext, 2131558923, paramList);
  }
  
  @SuppressLint({"NewApi"})
  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    k.b(paramViewGroup, "parent");
    int i = 0;
    if (paramView == null)
    {
      paramView = getContext().getSystemService("layout_inflater");
      if (paramView != null)
      {
        paramView = ((LayoutInflater)paramView).inflate(2131558923, paramViewGroup, false);
        k.a(paramView, "inflater.inflate(R.layou…er_dialog, parent, false)");
        paramViewGroup = new a(paramView);
        paramView.setTag(paramViewGroup);
      }
      else
      {
        throw new u("null cannot be cast to non-null type android.view.LayoutInflater");
      }
    }
    else
    {
      paramViewGroup = paramView.getTag();
      if (paramViewGroup == null) {
        break label253;
      }
      paramViewGroup = (a)paramViewGroup;
    }
    h localh = (h)getItem(paramInt);
    if (localh == null)
    {
      a.setText(null);
      b.setVisibility(8);
      c.setImageDrawable(null);
      return paramView;
    }
    a.setText(a);
    b.setText(b);
    View localView = (View)b;
    CharSequence localCharSequence = b;
    if (localCharSequence != null)
    {
      paramInt = i;
      if (localCharSequence.length() != 0) {}
    }
    else
    {
      paramInt = 1;
    }
    t.a(localView, paramInt ^ 0x1);
    if ((localh instanceof h.b))
    {
      c.setImageDrawable(c.loadDrawable(getContext()));
      return paramView;
    }
    if ((localh instanceof h.a)) {
      c.setImageDrawable(c);
    }
    return paramView;
    label253:
    throw new u("null cannot be cast to non-null type com.truecaller.calling.initiate_call.SelectPhoneAccountAdapter.ViewHolder");
  }
  
  static final class a
  {
    final TextView a;
    final TextView b;
    final ImageView c;
    
    public a(View paramView)
    {
      View localView = paramView.findViewById(2131364682);
      k.a(localView, "itemView.findViewById(R.id.text1)");
      a = ((TextView)localView);
      localView = paramView.findViewById(2131364683);
      k.a(localView, "itemView.findViewById(R.id.text2)");
      b = ((TextView)localView);
      paramView = paramView.findViewById(2131363934);
      k.a(paramView, "itemView.findViewById(R.id.phone_icon1)");
      c = ((ImageView)paramView);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.initiate_call.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */