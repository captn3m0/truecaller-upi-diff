package com.truecaller.calling.initiate_call;

import android.graphics.drawable.Icon;
import android.telecom.PhoneAccountHandle;

public final class h$b
  extends h
{
  final Icon c;
  final PhoneAccountHandle d;
  
  public h$b(CharSequence paramCharSequence1, CharSequence paramCharSequence2, Icon paramIcon, PhoneAccountHandle paramPhoneAccountHandle)
  {
    super(paramCharSequence1, paramCharSequence2, (byte)0);
    c = paramIcon;
    d = paramPhoneAccountHandle;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.initiate_call.h.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */