package com.truecaller.calling.initiate_call;

import android.content.Context;
import android.telephony.TelephonyManager;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.b;
import com.truecaller.calling.ar;
import com.truecaller.common.h.aj;
import com.truecaller.data.entity.g;
import com.truecaller.multisim.h;
import com.truecaller.util.bz;
import com.truecaller.utils.l;
import javax.inject.Provider;

public final class d
  implements dagger.a.d<c>
{
  private final Provider<bz> a;
  private final Provider<com.truecaller.androidactors.f<ae>> b;
  private final Provider<b> c;
  private final Provider<l> d;
  private final Provider<g> e;
  private final Provider<h> f;
  private final Provider<ar> g;
  private final Provider<com.truecaller.utils.d> h;
  private final Provider<com.truecaller.i.c> i;
  private final Provider<com.truecaller.calling.b.c> j;
  private final Provider<TelephonyManager> k;
  private final Provider<aj> l;
  private final Provider<e> m;
  private final Provider<Context> n;
  private final Provider<c.d.f> o;
  
  private d(Provider<bz> paramProvider, Provider<com.truecaller.androidactors.f<ae>> paramProvider1, Provider<b> paramProvider2, Provider<l> paramProvider3, Provider<g> paramProvider4, Provider<h> paramProvider5, Provider<ar> paramProvider6, Provider<com.truecaller.utils.d> paramProvider7, Provider<com.truecaller.i.c> paramProvider8, Provider<com.truecaller.calling.b.c> paramProvider9, Provider<TelephonyManager> paramProvider10, Provider<aj> paramProvider11, Provider<e> paramProvider12, Provider<Context> paramProvider13, Provider<c.d.f> paramProvider14)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
    k = paramProvider10;
    l = paramProvider11;
    m = paramProvider12;
    n = paramProvider13;
    o = paramProvider14;
  }
  
  public static d a(Provider<bz> paramProvider, Provider<com.truecaller.androidactors.f<ae>> paramProvider1, Provider<b> paramProvider2, Provider<l> paramProvider3, Provider<g> paramProvider4, Provider<h> paramProvider5, Provider<ar> paramProvider6, Provider<com.truecaller.utils.d> paramProvider7, Provider<com.truecaller.i.c> paramProvider8, Provider<com.truecaller.calling.b.c> paramProvider9, Provider<TelephonyManager> paramProvider10, Provider<aj> paramProvider11, Provider<e> paramProvider12, Provider<Context> paramProvider13, Provider<c.d.f> paramProvider14)
  {
    return new d(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10, paramProvider11, paramProvider12, paramProvider13, paramProvider14);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.initiate_call.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */