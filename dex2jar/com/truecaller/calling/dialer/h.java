package com.truecaller.calling.dialer;

import c.g.b.k;
import com.truecaller.calling.ActionType;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.util.cf;
import com.truecaller.utils.extensions.c;
import com.truecaller.utils.n;

public abstract class h
{
  public static final b e = new b((byte)0);
  public final boolean a;
  final Integer b;
  public final ActionType c;
  final ActionType d;
  
  private h(boolean paramBoolean, Integer paramInteger, ActionType paramActionType1, ActionType paramActionType2)
  {
    a = paramBoolean;
    b = paramInteger;
    c = paramActionType1;
    d = paramActionType2;
  }
  
  public static final h a(HistoryEvent paramHistoryEvent, cf paramcf)
  {
    return b.a(paramHistoryEvent, paramcf);
  }
  
  public final String a(n paramn)
  {
    k.b(paramn, "resourceProvider");
    Integer localInteger = b;
    if (localInteger != null) {
      return paramn.a(((Number)localInteger).intValue(), new Object[0]);
    }
    return null;
  }
  
  public static final class a
    extends h
  {
    public a(boolean paramBoolean)
    {
      super(null, localActionType, 10);
    }
  }
  
  public static final class b
  {
    public static h a(HistoryEvent paramHistoryEvent, cf paramcf)
    {
      k.b(paramHistoryEvent, "historyEvent");
      if (paramcf != null) {
        paramcf = Boolean.valueOf(paramcf.a(paramHistoryEvent.m()));
      } else {
        paramcf = null;
      }
      boolean bool = c.a(paramcf);
      if (b(paramHistoryEvent)) {
        return (h)new h.e(bool);
      }
      if (a(paramHistoryEvent)) {
        return (h)h.c.f;
      }
      if (c(paramHistoryEvent)) {
        return (h)new h.d();
      }
      return (h)new h.a(bool);
    }
    
    private static boolean a(HistoryEvent paramHistoryEvent)
    {
      return paramHistoryEvent.r() == 3;
    }
    
    private static boolean b(HistoryEvent paramHistoryEvent)
    {
      return k.a("com.whatsapp", paramHistoryEvent.q());
    }
    
    private static boolean c(HistoryEvent paramHistoryEvent)
    {
      return k.a("com.truecaller.voip.manager.VOIP", paramHistoryEvent.q());
    }
  }
  
  public static final class c
    extends h
  {
    public static final c f = new c();
    
    private c()
    {
      super(Integer.valueOf(2131888106), null, 12);
    }
  }
  
  public static final class d
    extends h
  {
    public d()
    {
      super(Integer.valueOf(2131889034), ActionType.VOIP_CALL, ActionType.CELLULAR_CALL, (byte)0);
    }
  }
  
  public static final class e
    extends h
  {
    public e(boolean paramBoolean)
    {
      super(Integer.valueOf(2131889049), localActionType, ActionType.CELLULAR_CALL, (byte)0);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */