package com.truecaller.calling.dialer;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import com.truecaller.common.c.b;

public abstract class c
  implements v
{
  v.a a;
  private final b b;
  private final ContentResolver c;
  private final Uri d;
  
  public c(ContentResolver paramContentResolver, Uri paramUri, final Long paramLong)
  {
    c = paramContentResolver;
    d = paramUri;
    if ((paramLong != null) && (paramLong.longValue() > 0L)) {
      paramContentResolver = (b)new a(this, paramLong, new Handler(), paramLong.longValue());
    } else {
      paramContentResolver = (b)new b(this, new Handler());
    }
    b = paramContentResolver;
  }
  
  protected abstract void a();
  
  public final void a(v.a parama)
  {
    v.a locala = a;
    int j = 1;
    int i;
    if (locala != null) {
      i = 1;
    } else {
      i = 0;
    }
    a = parama;
    if (a == null) {
      j = 0;
    }
    if ((j != 0) && (i == 0))
    {
      c.registerContentObserver(d, false, (ContentObserver)b);
      return;
    }
    if ((j == 0) && (i != 0)) {
      c.unregisterContentObserver((ContentObserver)b);
    }
  }
  
  public static final class a
    extends b
  {
    a(Long paramLong, Handler paramHandler, long paramLong1)
    {
      super(localObject);
    }
    
    public final void a()
    {
      a.a();
    }
  }
  
  public static final class b
    extends b
  {
    b(Handler paramHandler)
    {
      super();
    }
    
    public final void a()
    {
      a.a();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */