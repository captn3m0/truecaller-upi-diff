package com.truecaller.calling.dialer;

import c.g.b.k;
import com.truecaller.adapter_delegates.c;
import com.truecaller.adapter_delegates.h;
import com.truecaller.search.global.CompositeAdapterDelegate.SearchResultOrder;
import com.truecaller.util.al;
import javax.inject.Inject;

public final class bo
  extends c<bn.b>
  implements bn.a
{
  private final bm.a c;
  private final al d;
  private final com.truecaller.utils.n e;
  private final bd f;
  
  @Inject
  public bo(bm.a parama, al paramal, com.truecaller.utils.n paramn, bd parambd)
  {
    d = paramal;
    e = paramn;
    f = parambd;
    c = parama;
  }
  
  private final c.n<String, bz> a()
  {
    return c.a((ca)this, b[0]);
  }
  
  public final boolean a(h paramh)
  {
    k.b(paramh, "event");
    paramh = a;
    if ((paramh.hashCode() == -1743572928) && (paramh.equals("ItemEvent.CLICKED")))
    {
      f.a((String)aa, null, true, CompositeAdapterDelegate.SearchResultOrder.ORDER_TCM);
      return true;
    }
    return false;
  }
  
  public final int getItemCount()
  {
    return 1;
  }
  
  public final long getItemId(int paramInt)
  {
    return 1L;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bo
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */