package com.truecaller.calling.dialer;

import com.truecaller.data.entity.g;
import com.truecaller.flashsdk.core.i;
import com.truecaller.search.local.b.e;
import dagger.a.d;
import javax.inject.Provider;

public final class by
  implements d<bx>
{
  private final Provider<com.truecaller.search.local.model.c> a;
  private final Provider<bm.a> b;
  private final Provider<bd> c;
  private final Provider<e> d;
  private final Provider<com.truecaller.i.c> e;
  private final Provider<i> f;
  private final Provider<com.truecaller.analytics.b> g;
  private final Provider<com.truecaller.flashsdk.core.b> h;
  private final Provider<g> i;
  
  private by(Provider<com.truecaller.search.local.model.c> paramProvider, Provider<bm.a> paramProvider1, Provider<bd> paramProvider2, Provider<e> paramProvider3, Provider<com.truecaller.i.c> paramProvider4, Provider<i> paramProvider5, Provider<com.truecaller.analytics.b> paramProvider6, Provider<com.truecaller.flashsdk.core.b> paramProvider7, Provider<g> paramProvider8)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
  }
  
  public static by a(Provider<com.truecaller.search.local.model.c> paramProvider, Provider<bm.a> paramProvider1, Provider<bd> paramProvider2, Provider<e> paramProvider3, Provider<com.truecaller.i.c> paramProvider4, Provider<i> paramProvider5, Provider<com.truecaller.analytics.b> paramProvider6, Provider<com.truecaller.flashsdk.core.b> paramProvider7, Provider<g> paramProvider8)
  {
    return new by(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.by
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */