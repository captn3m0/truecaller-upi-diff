package com.truecaller.calling.dialer;

import c.a.m;
import com.truecaller.data.entity.Contact;
import java.util.Collection;
import java.util.List;

public abstract class bz
{
  public static final class a
    extends bz
  {
    final List<av> a;
    
    public a(List<av> paramList)
    {
      super();
      a = m.d((Collection)paramList);
    }
  }
  
  public static final class b
    extends bz
  {
    public static final b a = new b();
    
    private b()
    {
      super();
    }
  }
  
  public static final class c
    extends bz
  {
    final Contact a;
    
    public c(Contact paramContact)
    {
      super();
      a = paramContact;
    }
  }
  
  public static final class d
    extends bz
  {
    public static final d a = new d();
    
    private d()
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bz
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */