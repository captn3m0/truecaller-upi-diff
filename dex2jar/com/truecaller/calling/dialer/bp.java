package com.truecaller.calling.dialer;

import com.truecaller.util.al;
import com.truecaller.utils.n;
import dagger.a.d;
import javax.inject.Provider;

public final class bp
  implements d<bo>
{
  private final Provider<bm.a> a;
  private final Provider<al> b;
  private final Provider<n> c;
  private final Provider<bd> d;
  
  private bp(Provider<bm.a> paramProvider, Provider<al> paramProvider1, Provider<n> paramProvider2, Provider<bd> paramProvider3)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static bp a(Provider<bm.a> paramProvider, Provider<al> paramProvider1, Provider<n> paramProvider2, Provider<bd> paramProvider3)
  {
    return new bp(paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bp
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */