package com.truecaller.calling.dialer;

import android.view.View;
import c.a.ag;
import c.g.b.k;
import c.t;
import com.truecaller.adapter_delegates.c;
import com.truecaller.f.a.i;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.featuretoggles.f;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.inject.Inject;

public final class bf
  extends c<be.b>
  implements be.a
{
  private final Map<String, i> b;
  private i c;
  private final be.b.b d;
  
  @Inject
  public bf(be.b.b paramb, e parame, Set<i> paramSet)
  {
    d = paramb;
    paramSet = (Iterable)paramSet;
    paramb = (Collection)new ArrayList(c.a.m.a(paramSet, 10));
    paramSet = paramSet.iterator();
    while (paramSet.hasNext())
    {
      i locali = (i)paramSet.next();
      paramb.add(t.a(locali.e(), locali));
    }
    b = ag.a((Iterable)paramb);
    parame = (Iterable)c.n.m.c((CharSequence)((f)G.a(parame, e.a[92])).e(), new String[] { "," }, false, 6);
    paramb = (Collection)new ArrayList();
    parame = parame.iterator();
    while (parame.hasNext())
    {
      paramSet = (String)parame.next();
      paramSet = (i)b.get(paramSet);
      if (paramSet != null) {
        paramb.add(paramSet);
      }
    }
    parame = ((Iterable)paramb).iterator();
    while (parame.hasNext())
    {
      paramb = parame.next();
      if (((i)paramb).b()) {
        break label268;
      }
    }
    paramb = null;
    label268:
    c = ((i)paramb);
  }
  
  private final void b()
  {
    c = null;
    d.H();
  }
  
  public final void a()
  {
    i locali = c;
    if (locali != null) {
      locali.c();
    }
    b();
  }
  
  public final void a(View paramView)
  {
    k.b(paramView, "view");
    i locali = c;
    if (locali != null) {
      locali.a(paramView);
    }
    b();
  }
  
  public final int getItemCount()
  {
    if (c != null) {
      return 1;
    }
    return 0;
  }
  
  public final long getItemId(int paramInt)
  {
    return 1L;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bf
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */