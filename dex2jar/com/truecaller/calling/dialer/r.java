package com.truecaller.calling.dialer;

import android.content.ContentResolver;
import android.net.Uri;
import c.g.a.a;
import c.g.b.j;
import c.g.b.k;
import c.g.b.w;
import c.x;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

public final class r
  extends c
  implements s
{
  private final HashSet<q> b = new HashSet();
  private boolean c;
  
  public r(ContentResolver paramContentResolver, Uri paramUri, Long paramLong)
  {
    super(paramContentResolver, paramUri, paramLong);
  }
  
  protected final void a()
  {
    Object localObject = (Iterable)b;
    boolean bool1 = localObject instanceof Collection;
    boolean bool2 = false;
    if ((!bool1) || (!((Collection)localObject).isEmpty()))
    {
      localObject = ((Iterable)localObject).iterator();
      while (((Iterator)localObject).hasNext()) {
        if (!((q)((Iterator)localObject).next()).a())
        {
          i = 0;
          break label78;
        }
      }
    }
    int i = 1;
    label78:
    if (i != 0)
    {
      localObject = a;
      bool1 = bool2;
      if (localObject != null)
      {
        ((v.a)localObject).D_();
        bool1 = bool2;
      }
    }
    else
    {
      bool1 = true;
    }
    c = bool1;
  }
  
  public final void a(q paramq)
  {
    k.b(paramq, "condition");
    paramq.a((a)new a((r)this));
    b.add(paramq);
  }
  
  static final class a
    extends j
    implements a<x>
  {
    a(r paramr)
    {
      super(paramr);
    }
    
    public final c.l.c a()
    {
      return w.a(r.class);
    }
    
    public final String b()
    {
      return "onConditionChanged";
    }
    
    public final String c()
    {
      return "onConditionChanged()V";
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.r
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */