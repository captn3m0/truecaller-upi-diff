package com.truecaller.calling.dialer;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

public final class SelectionAwareEditText
  extends AppCompatEditText
{
  private int a = Integer.MIN_VALUE;
  private int b = Integer.MIN_VALUE;
  private a c;
  
  public SelectionAwareEditText(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private SelectionAwareEditText(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
  }
  
  private final void a(int paramInt1, int paramInt2)
  {
    if ((paramInt1 != a) || (paramInt2 != b))
    {
      a locala = c;
      if (locala != null) {
        locala.b(paramInt1, paramInt2);
      }
      a = paramInt1;
      b = paramInt2;
    }
  }
  
  public final a getSelectionChangeListener()
  {
    return c;
  }
  
  protected final void onSelectionChanged(int paramInt1, int paramInt2)
  {
    super.onSelectionChanged(paramInt1, paramInt2);
    a(paramInt2, paramInt2);
  }
  
  protected final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    super.onTextChanged(paramCharSequence, paramInt1, paramInt2, paramInt3);
    a(this);
  }
  
  public final void setSelectionChangeListener(a parama)
  {
    c = parama;
    a(this);
  }
  
  public static abstract interface a
  {
    public abstract void b(int paramInt1, int paramInt2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.SelectionAwareEditText
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */