package com.truecaller.calling.dialer;

import c.d.a.a;
import c.d.b.a.f;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ah;
import kotlinx.coroutines.ar;

@f(b="DialerPresenter.kt", c={843}, d="invokeSuspend", e="com.truecaller.calling.dialer.DialerPresenter$startT9Search$1$delayedUpdateJob$1")
final class af$k$a
  extends c.d.b.a.k
  implements m<ag, c<? super x>, Object>
{
  Object a;
  int b;
  private ag d;
  
  af$k$a(af.k paramk, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new a(c, paramc);
    d = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    switch (b)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 1: 
      localObject = (ag)a;
      if (!(paramObject instanceof o.b)) {
        paramObject = localObject;
      } else {
        throw a;
      }
      break;
    case 0: 
      if ((paramObject instanceof o.b)) {
        break label139;
      }
      paramObject = d;
      a = paramObject;
      b = 1;
      if (ar.a(300L, this) == localObject) {
        return localObject;
      }
      break;
    }
    if (ah.a((ag)paramObject)) {
      c.d.a(c.e, (bz)bz.b.a);
    }
    return x.a;
    label139:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((a)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.af.k.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */