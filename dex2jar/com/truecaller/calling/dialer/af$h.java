package com.truecaller.calling.dialer;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.l;
import c.o.b;
import c.x;
import com.truecaller.analytics.TimingEvent;
import com.truecaller.analytics.au;
import com.truecaller.bd;
import com.truecaller.callhistory.FilterType;
import com.truecaller.data.entity.HistoryEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bn;

@c.d.b.a.f(b="DialerPresenter.kt", c={888}, d="invokeSuspend", e="com.truecaller.calling.dialer.DialerPresenter$refreshCallHistory$1")
final class af$h
  extends c.d.b.a.k
  implements m<ag, c<? super x>, Object>
{
  Object a;
  Object b;
  int c;
  private ag h;
  
  af$h(af paramaf, FilterType paramFilterType, int paramInt, boolean paramBoolean, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new h(d, e, f, g, paramc);
    h = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    Object localObject2;
    Object localObject3;
    Object localObject4;
    int i;
    switch (c)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 1: 
      localObject1 = (af)b;
      if (!(paramObject instanceof o.b)) {
        localObject2 = paramObject;
      } else {
        throw a;
      }
      break;
    case 0: 
      if ((paramObject instanceof o.b)) {
        break label976;
      }
      if (d.L()) {
        paramObject = new at();
      } else {
        paramObject = new ar();
      }
      localObject2 = (as)paramObject;
      paramObject = d;
      localObject3 = r;
      localObject4 = d.h.plus(d.A);
      FilterType localFilterType = e;
      i = f;
      a = localObject2;
      b = paramObject;
      c = 1;
      localObject2 = ((g)localObject3).a((c.d.f)localObject4, localFilterType, (as)localObject2, Integer.valueOf(i), this);
      if (localObject2 == localObject1) {
        return localObject1;
      }
      localObject1 = paramObject;
    }
    f = ((List)localObject2);
    d.v.a();
    d.I();
    paramObject = d;
    if ((((Collection)d).isEmpty() ^ true))
    {
      localObject2 = (Iterable)f;
      localObject1 = (Collection)new ArrayList();
      localObject2 = ((Iterable)localObject2).iterator();
      while (((Iterator)localObject2).hasNext())
      {
        localObject3 = ((Iterator)localObject2).next();
        localObject4 = (Iterable)a;
        if ((!(localObject4 instanceof Collection)) || (!((Collection)localObject4).isEmpty()))
        {
          localObject4 = ((Iterable)localObject4).iterator();
          while (((Iterator)localObject4).hasNext())
          {
            long l = ((Number)((Iterator)localObject4).next()).longValue();
            if (d.contains(Long.valueOf(l)))
            {
              i = 1;
              break label396;
            }
          }
        }
        i = 0;
        label396:
        if (i != 0) {
          ((Collection)localObject1).add(localObject3);
        }
      }
      localObject2 = (Iterable)localObject1;
      localObject1 = (Collection)new ArrayList();
      localObject2 = ((Iterable)localObject2).iterator();
      while (((Iterator)localObject2).hasNext())
      {
        localObject3 = nextc.getId();
        if (localObject3 != null) {
          ((Collection)localObject1).add(localObject3);
        }
      }
      localObject1 = (List)localObject1;
      d.clear();
      d.addAll((Collection)localObject1);
      if (d.isEmpty())
      {
        localObject1 = (ae.b)c;
        if (localObject1 != null) {
          ((ae.b)localObject1).c();
        }
      }
      paramObject = (ae.b)c;
      if (paramObject != null) {
        ((ae.b)paramObject).d();
      }
    }
    paramObject = d;
    localObject1 = e;
    if (l != localObject1)
    {
      l = ((FilterType)localObject1);
      boolean bool1;
      if (localObject1 == FilterType.NONE) {
        bool1 = true;
      } else {
        bool1 = false;
      }
      if (!bool1)
      {
        localObject2 = (ae.c)b;
        if (localObject2 != null)
        {
          switch (ag.b[localObject1.ordinal()])
          {
          default: 
            throw new l();
          case 6: 
            i = 2131888106;
            break;
          case 5: 
            i = 2131886593;
            break;
          case 4: 
            i = 2131886595;
            break;
          case 3: 
            i = 2131886596;
            break;
          case 2: 
            i = 2131886594;
            break;
          case 1: 
            i = 0;
          }
          ((ae.c)localObject2).c(i);
        }
      }
      localObject1 = (ae.c)b;
      if (localObject1 != null) {
        ((ae.c)localObject1).f(((af)paramObject).G());
      }
      localObject1 = (ae.c)b;
      if (localObject1 != null) {
        ((ae.c)localObject1).m();
      }
      localObject1 = (ae.c)b;
      if (localObject1 != null)
      {
        boolean bool2;
        if ((((af)paramObject).M()) && (bool1)) {
          bool2 = true;
        } else {
          bool2 = false;
        }
        ((ae.c)localObject1).b(bool2);
      }
      localObject1 = (ae.c)b;
      if (localObject1 != null) {
        ((ae.c)localObject1).h(bool1);
      }
      paramObject = (ae.c)b;
      if (paramObject != null) {
        ((ae.c)paramObject).k();
      }
    }
    paramObject = (ae.c)d.b;
    if (paramObject != null) {
      ((ae.c)paramObject).a(d.J());
    }
    d.o.clear();
    paramObject = (ae.c)d.b;
    if (paramObject != null) {
      ((ae.c)paramObject).b();
    }
    if ((g) && ((((Collection)d.f).isEmpty() ^ true))) {
      af.a(d, null, false, 1);
    }
    d.y.a(TimingEvent.CALL_LOG_STARTUP);
    return x.a;
    label976:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((h)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.af.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */