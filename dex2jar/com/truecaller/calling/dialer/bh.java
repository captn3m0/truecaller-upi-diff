package com.truecaller.calling.dialer;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.truecaller.R.id;
import com.truecaller.ui.view.TintedImageView;
import java.util.HashMap;
import kotlinx.a.a.a;

public final class bh
  extends RecyclerView.ViewHolder
  implements be.b, a
{
  final View a;
  private HashMap b;
  
  public bh(View paramView, final be.b.a parama)
  {
    super(paramView);
    a = paramView;
    ((CardView)c(R.id.promoContainer)).setOnClickListener((View.OnClickListener)new View.OnClickListener()
    {
      public final void onClick(View paramAnonymousView)
      {
        parama.a(a.a);
      }
    });
    ((TintedImageView)c(R.id.close)).setOnClickListener((View.OnClickListener)new View.OnClickListener()
    {
      public final void onClick(View paramAnonymousView)
      {
        a.a();
      }
    });
  }
  
  private View c(int paramInt)
  {
    if (b == null) {
      b = new HashMap();
    }
    View localView2 = (View)b.get(Integer.valueOf(paramInt));
    View localView1 = localView2;
    if (localView2 == null)
    {
      localView1 = a();
      if (localView1 == null) {
        return null;
      }
      localView1 = localView1.findViewById(paramInt);
      b.put(Integer.valueOf(paramInt), localView1);
    }
    return localView1;
  }
  
  public final View a()
  {
    return a;
  }
  
  public final void a(int paramInt)
  {
    ((TextView)c(R.id.promoView)).setText(paramInt);
  }
  
  public final void b(int paramInt)
  {
    ((TextView)c(R.id.promoView)).setCompoundDrawablesWithIntrinsicBounds(paramInt, 0, 0, 0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bh
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */