package com.truecaller.calling.dialer;

import dagger.a.d;
import javax.inject.Provider;

public final class cp
  implements d<co>
{
  private final Provider<cn.a> a;
  private final Provider<cf.b.b> b;
  
  private cp(Provider<cn.a> paramProvider, Provider<cf.b.b> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static cp a(Provider<cn.a> paramProvider, Provider<cf.b.b> paramProvider1)
  {
    return new cp(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.cp
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */