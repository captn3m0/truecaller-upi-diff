package com.truecaller.calling.dialer;

import c.a.m;
import c.g.b.k;
import com.truecaller.common.h.ab;
import com.truecaller.data.entity.HistoryEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.a.a.b;

public final class ar
  implements as<x>
{
  private final String a = "Default";
  
  public final String a()
  {
    return a;
  }
  
  public final List<x> a(List<? extends HistoryEvent> paramList)
  {
    k.b(paramList, "events");
    ArrayList localArrayList = new ArrayList(paramList.size());
    long l1 = System.currentTimeMillis();
    Object localObject1 = paramList.iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      int j = 0;
      if (!bool) {
        break;
      }
      HistoryEvent localHistoryEvent1 = (HistoryEvent)((Iterator)localObject1).next();
      x localx = (x)m.g((List)localArrayList);
      if (localx != null)
      {
        HistoryEvent localHistoryEvent2 = c;
        int i = j;
        if (localHistoryEvent2.f() == localHistoryEvent1.f())
        {
          Object localObject2 = h.e;
          localObject2 = ab;
          h.b localb = h.e;
          i = j;
          if (k.a(localObject2, ab))
          {
            i = j;
            if (localHistoryEvent2.m() == localHistoryEvent1.m())
            {
              i = j;
              if (localHistoryEvent2.h() == localHistoryEvent1.h())
              {
                i = j;
                if (!ab.a(localHistoryEvent2.b()))
                {
                  i = j;
                  if (ab.a(localHistoryEvent2.b(), localHistoryEvent1.b(), true))
                  {
                    i = j;
                    if (k.a(new b(localHistoryEvent2.j()).e(), new b(localHistoryEvent1.j()).e())) {
                      i = 1;
                    }
                  }
                }
              }
            }
          }
        }
        if (i != 0)
        {
          localx.a(localHistoryEvent1);
          continue;
        }
      }
      localArrayList.add(new x(localHistoryEvent1));
    }
    long l2 = System.currentTimeMillis();
    localObject1 = new StringBuilder("Merged ");
    ((StringBuilder)localObject1).append(paramList.size());
    ((StringBuilder)localObject1).append(" history events in ");
    ((StringBuilder)localObject1).append(l2 - l1);
    ((StringBuilder)localObject1).append("ms");
    ((StringBuilder)localObject1).toString();
    return (List)localArrayList;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.ar
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */