package com.truecaller.calling.dialer;

import com.truecaller.flashsdk.core.b;
import com.truecaller.flashsdk.core.i;
import dagger.a.d;
import javax.inject.Provider;

public final class ac
  implements d<i>
{
  private final Provider<b> a;
  
  private ac(Provider<b> paramProvider)
  {
    a = paramProvider;
  }
  
  public static ac a(Provider<b> paramProvider)
  {
    return new ac(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.ac
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */