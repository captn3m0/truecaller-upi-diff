package com.truecaller.calling.dialer;

import com.truecaller.flashsdk.core.i;
import com.truecaller.util.cf;
import com.truecaller.voip.ai;
import dagger.a.d;
import javax.inject.Provider;

public final class ba
  implements d<az>
{
  private final Provider<i.a> a;
  private final Provider<bd> b;
  private final Provider<com.truecaller.analytics.b> c;
  private final Provider<a> d;
  private final Provider<i> e;
  private final Provider<com.truecaller.flashsdk.core.b> f;
  private final Provider<cf> g;
  private final Provider<ai> h;
  
  private ba(Provider<i.a> paramProvider, Provider<bd> paramProvider1, Provider<com.truecaller.analytics.b> paramProvider2, Provider<a> paramProvider3, Provider<i> paramProvider4, Provider<com.truecaller.flashsdk.core.b> paramProvider5, Provider<cf> paramProvider6, Provider<ai> paramProvider7)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
  }
  
  public static ba a(Provider<i.a> paramProvider, Provider<bd> paramProvider1, Provider<com.truecaller.analytics.b> paramProvider2, Provider<a> paramProvider3, Provider<i> paramProvider4, Provider<com.truecaller.flashsdk.core.b> paramProvider5, Provider<cf> paramProvider6, Provider<ai> paramProvider7)
  {
    return new ba(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.ba
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */