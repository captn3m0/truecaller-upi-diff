package com.truecaller.calling.dialer;

import android.os.CancellationSignal;
import c.d.a.a;
import c.d.b.a.f;
import c.d.c;
import c.g.a.m;
import c.n;
import c.o.b;
import c.x;
import com.truecaller.data.access.s;
import com.truecaller.data.entity.Contact;
import java.util.List;
import kotlinx.coroutines.ag;

@f(b="SearchPerformer.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.dialer.SearchPerformerImpl$getT9LocalContacts$2")
final class bs$b
  extends c.d.b.a.k
  implements m<ag, c<? super List<? extends n<? extends Contact, ? extends String>>>, Object>
{
  int a;
  private ag f;
  
  bs$b(bs parambs, String paramString, CancellationSignal paramCancellationSignal, Integer paramInteger, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new b(b, c, d, e, paramc);
    f = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    if (a == 0)
    {
      if (!(paramObject instanceof o.b))
      {
        localObject = b.a;
        String str = c;
        CancellationSignal localCancellationSignal = d;
        paramObject = e;
        if (paramObject != null)
        {
          ((Number)paramObject).intValue();
          paramObject = Integer.valueOf(0);
        }
        else
        {
          paramObject = null;
        }
        return ((s)localObject).a(str, localCancellationSignal, (Integer)paramObject, e);
      }
      throw a;
    }
    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((b)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bs.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */