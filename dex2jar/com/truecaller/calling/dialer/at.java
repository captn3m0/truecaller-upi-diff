package com.truecaller.calling.dialer;

import c.a.m;
import c.g.b.k;
import c.l;
import c.m.i;
import com.truecaller.data.entity.HistoryEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class at
  implements as<ce>
{
  private final String a = "Slim";
  
  public final String a()
  {
    return a;
  }
  
  public final List<ce> a(List<? extends HistoryEvent> paramList)
  {
    k.b(paramList, "events");
    long l1 = System.currentTimeMillis();
    paramList = m.n((Iterable)paramList);
    Map localMap = (Map)new LinkedHashMap();
    Iterator localIterator = paramList.a();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      int j = 0;
      if (!bool) {
        break;
      }
      Object localObject3 = localIterator.next();
      Object localObject2 = (HistoryEvent)localObject3;
      paramList = ((HistoryEvent)localObject2).b();
      if (paramList != null)
      {
        int i;
        if (((CharSequence)paramList).length() == 0) {
          i = 1;
        } else {
          i = 0;
        }
        if (i != 0) {
          paramList = null;
        }
        if (paramList != null)
        {
          StringBuilder localStringBuilder = new StringBuilder();
          localObject1 = ((HistoryEvent)localObject2).a();
          if (localObject1 != null) {
            paramList = (List<? extends HistoryEvent>)localObject1;
          }
          localStringBuilder.append(paramList);
          localStringBuilder.append("--");
          paramList = h.e;
          paramList = h.b.a((HistoryEvent)localObject2, null);
          if ((paramList instanceof h.e)) {
            i = j;
          } else if ((!(paramList instanceof h.c)) && (!(paramList instanceof h.a)))
          {
            if ((paramList instanceof h.d)) {
              i = 2;
            } else {
              throw new l();
            }
          }
          else {
            i = 1;
          }
          localStringBuilder.append(i);
          paramList = localStringBuilder.toString();
          break label245;
        }
      }
      paramList = String.valueOf(((HistoryEvent)localObject2).j());
      label245:
      localObject2 = localMap.get(paramList);
      localObject1 = localObject2;
      if (localObject2 == null)
      {
        localObject1 = new ArrayList();
        localMap.put(paramList, localObject1);
      }
      ((List)localObject1).add(localObject3);
    }
    paramList = (Collection)new ArrayList(localMap.size());
    Object localObject1 = localMap.entrySet().iterator();
    while (((Iterator)localObject1).hasNext()) {
      paramList.add(new ce((List)((Map.Entry)((Iterator)localObject1).next()).getValue()));
    }
    paramList = (List)paramList;
    long l2 = System.currentTimeMillis();
    localObject1 = new StringBuilder("Merged ");
    ((StringBuilder)localObject1).append(paramList.size());
    ((StringBuilder)localObject1).append(" history events in ");
    ((StringBuilder)localObject1).append(l2 - l1);
    ((StringBuilder)localObject1).append("ms");
    ((StringBuilder)localObject1).toString();
    return paramList;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.at
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */