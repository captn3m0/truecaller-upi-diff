package com.truecaller.calling.dialer;

import c.d.a.a;
import c.d.b.a.f;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.g;

@f(b="DialerPresenter.kt", c={952}, d="invokeSuspend", e="com.truecaller.calling.dialer.DialerPresenter$refreshSuggestedContacts$1")
final class af$i
  extends c.d.b.a.k
  implements m<ag, c<? super x>, Object>
{
  Object a;
  int b;
  private ag d;
  
  af$i(af paramaf, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new i(c, paramc);
    d = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    Object localObject2;
    switch (b)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 1: 
      localObject1 = (af)a;
      if (!(paramObject instanceof o.b)) {
        localObject2 = paramObject;
      } else {
        throw a;
      }
      break;
    case 0: 
      if ((paramObject instanceof o.b)) {
        break label176;
      }
      paramObject = c;
      a = paramObject;
      b = 1;
      localObject2 = g.a(h.plus(A), (m)new af.e((af)paramObject, null), this);
      if (localObject2 == localObject1) {
        return localObject1;
      }
      localObject1 = paramObject;
    }
    e = ((List)localObject2);
    c.w.a();
    paramObject = (ae.c)c.b;
    if (paramObject != null) {
      ((ae.c)paramObject).c();
    }
    return x.a;
    label176:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((i)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.af.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */