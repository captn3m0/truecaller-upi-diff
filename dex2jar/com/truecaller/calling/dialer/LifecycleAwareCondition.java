package com.truecaller.calling.dialer;

import android.arch.lifecycle.e;
import android.arch.lifecycle.e.a;
import android.arch.lifecycle.e.b;
import android.arch.lifecycle.g;
import c.g.a.a;
import c.x;

public final class LifecycleAwareCondition
  implements g, q
{
  private a<x> a;
  private final e b;
  private final e.b c;
  
  public LifecycleAwareCondition(e parame, e.b paramb)
  {
    b = parame;
    c = paramb;
    b.a((g)this);
  }
  
  @android.arch.lifecycle.q(a=e.a.ON_ANY)
  private final x onLifeCycleStateChange()
  {
    a locala = a;
    if (locala != null) {
      return (x)locala.invoke();
    }
    return null;
  }
  
  public final void a(a<x> parama)
  {
    a = parama;
  }
  
  public final boolean a()
  {
    return b.a().a(c);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.LifecycleAwareCondition
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */