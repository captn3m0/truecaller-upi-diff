package com.truecaller.calling.dialer;

import c.g.b.k;
import com.truecaller.adapter_delegates.c;
import com.truecaller.calling.dialer.a.a;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;

public final class cg
  extends c<cf.b>
  implements cf.a
{
  private cf.b b;
  
  private static void a(cf.b paramb)
  {
    boolean bool;
    if (paramb.a() <= 0) {
      bool = true;
    } else {
      bool = false;
    }
    paramb.a(bool);
  }
  
  public final void a()
  {
    cf.b localb = b;
    if (localb != null)
    {
      a(localb);
      return;
    }
  }
  
  public final void a(List<a> paramList1, List<a> paramList2)
  {
    k.b(paramList1, "oldItems");
    k.b(paramList2, "newItems");
    cf.b localb = b;
    if (localb != null)
    {
      localb.a(paramList1, paramList2);
      return;
    }
  }
  
  public final void a(Set<Integer> paramSet)
  {
    k.b(paramSet, "itemPositions");
    cf.b localb = b;
    if (localb != null)
    {
      localb.a(paramSet);
      return;
    }
  }
  
  public final int getItemCount()
  {
    return 1;
  }
  
  public final long getItemId(int paramInt)
  {
    return 1L;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.cg
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */