package com.truecaller.calling.dialer;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.string;
import com.truecaller.flashsdk.ui.b;
import com.truecaller.flashsdk.ui.b.a;

final class ai$d
  implements ViewTreeObserver.OnGlobalLayoutListener
{
  ai$d(ai paramai) {}
  
  public final void onGlobalLayout()
  {
    if ((ai.g(a)) && (!ai.i(a)))
    {
      if (ai.j(a) != null) {
        return;
      }
      Object localObject1 = ai.k(a);
      k.a(localObject1, "recycler");
      localObject1 = ((RecyclerView)localObject1).getAdapter();
      int i;
      if (localObject1 != null) {
        i = ((RecyclerView.Adapter)localObject1).getItemCount();
      } else {
        i = 0;
      }
      int j = 0;
      while (j < i)
      {
        localObject1 = ai.a(a, j);
        if ((localObject1 != null) && (((View)localObject1).getVisibility() == 0))
        {
          Object localObject2 = new int[2];
          ((View)localObject1).getLocationOnScreen((int[])localObject2);
          int k = localObject2[1];
          int m = ai.l(a);
          if (localObject2[1] > ai.m(a))
          {
            float f = k + m;
            localObject2 = ai.n(a);
            k.a(localObject2, "dialpadView");
            if (f < ((ViewGroup)localObject2).getY())
            {
              localObject2 = ((View)localObject1).getContext();
              k.a(localObject2, "flashButton.context");
              localObject1 = new b((Context)localObject2, (View)localObject1);
              if (b == null)
              {
                b = new PopupWindow(a, -1, -2, true);
                localObject2 = a.findViewById(R.id.text);
                k.a(localObject2, "contentView.findViewById(R.id.text)");
                localObject2 = (TextView)localObject2;
                Context localContext = a.getContext();
                k.a(localContext, "contentView.context");
                ((TextView)localObject2).setText(localContext.getResources().getText(R.string.flash_tooltip_text));
                localObject2 = b;
                if (localObject2 != null) {
                  ((PopupWindow)localObject2).setBackgroundDrawable((Drawable)new ColorDrawable());
                }
              }
              c.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)localObject1);
              localObject2 = b;
              if (localObject2 != null) {
                ((PopupWindow)localObject2).setOnDismissListener((PopupWindow.OnDismissListener)new b.a((b)localObject1));
              }
              localObject2 = b;
              if (localObject2 != null) {
                ((PopupWindow)localObject2).showAsDropDown(c, 0, 0);
              }
              ai.a(a, (b)localObject1);
              ai.a(a).C();
              ai.h(a);
              ai.o(a);
              return;
            }
          }
        }
        j += 1;
      }
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.ai.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */