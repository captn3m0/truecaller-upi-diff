package com.truecaller.calling.dialer;

import android.os.CancellationSignal;
import c.d.a.a;
import c.d.c;
import c.n;
import c.o.b;
import c.x;
import com.truecaller.data.entity.Contact;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ah;
import kotlinx.coroutines.ao;
import kotlinx.coroutines.ar;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.e;

@c.d.b.a.f(b="DialerPresenter.kt", c={850, 860}, d="invokeSuspend", e="com.truecaller.calling.dialer.DialerPresenter$startT9Search$1")
final class af$k
  extends c.d.b.a.k
  implements c.g.a.m<ag, c<? super x>, Object>
{
  Object a;
  Object b;
  int c;
  private ag f;
  
  af$k(af paramaf, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new k(d, e, paramc);
    f = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    Object localObject1;
    Object localObject3;
    Object localObject4;
    switch (c)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 2: 
      if (!(paramObject instanceof o.b)) {
        break label424;
      }
      throw a;
    case 1: 
      localObject1 = (ao)a;
      if ((paramObject instanceof o.b)) {
        throw a;
      }
      break;
    case 0: 
      if ((paramObject instanceof o.b)) {
        break label480;
      }
      paramObject = f;
      localObject1 = d;
      ((af)localObject1).a(e, (bz)g.b);
      localObject1 = e.a((ag)paramObject, ((ag)paramObject).V_(), (c.g.a.m)new a(this, null), 2);
      paramObject = d.s;
      localObject2 = d.j.plus(d.A);
      localObject3 = e;
      localObject4 = d.k;
      a = localObject1;
      c = 1;
      localObject2 = ((br)paramObject).a((c.d.f)localObject2, (String)localObject3, Integer.valueOf(100), (CancellationSignal)localObject4, this);
      paramObject = localObject2;
      if (localObject2 == locala) {
        return locala;
      }
      break;
    }
    Object localObject2 = (Iterable)paramObject;
    paramObject = (Collection)new ArrayList(c.a.m.a((Iterable)localObject2, 10));
    localObject2 = ((Iterable)localObject2).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject3 = (n)((Iterator)localObject2).next();
      ((Collection)paramObject).add(new av((Contact)a, (String)b, null));
    }
    paramObject = (List)paramObject;
    ((ao)localObject1).n();
    if (((List)paramObject).isEmpty())
    {
      d.a(e, (bz)bz.d.a);
      localObject2 = d.s;
      localObject3 = e;
      localObject4 = d.j.plus(d.A);
      a = localObject1;
      b = paramObject;
      c = 2;
      localObject1 = ((br)localObject2).a((String)localObject3, (c.d.f)localObject4, this);
      paramObject = localObject1;
      if (localObject1 == locala) {
        return locala;
      }
      label424:
      paramObject = (Contact)paramObject;
      d.a(e, (bz)new bz.c((Contact)paramObject));
    }
    else
    {
      d.a(e, (bz)new bz.a((List)paramObject));
    }
    return x.a;
    label480:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((k)a(paramObject1, (c)paramObject2)).a(x.a);
  }
  
  @c.d.b.a.f(b="DialerPresenter.kt", c={843}, d="invokeSuspend", e="com.truecaller.calling.dialer.DialerPresenter$startT9Search$1$delayedUpdateJob$1")
  static final class a
    extends c.d.b.a.k
    implements c.g.a.m<ag, c<? super x>, Object>
  {
    Object a;
    int b;
    private ag d;
    
    a(af.k paramk, c paramc)
    {
      super(paramc);
    }
    
    public final c<x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new a(c, paramc);
      d = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      Object localObject = a.a;
      switch (b)
      {
      default: 
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
      case 1: 
        localObject = (ag)a;
        if (!(paramObject instanceof o.b)) {
          paramObject = localObject;
        } else {
          throw a;
        }
        break;
      case 0: 
        if ((paramObject instanceof o.b)) {
          break label139;
        }
        paramObject = d;
        a = paramObject;
        b = 1;
        if (ar.a(300L, this) == localObject) {
          return localObject;
        }
        break;
      }
      if (ah.a((ag)paramObject)) {
        c.d.a(c.e, (bz)bz.b.a);
      }
      return x.a;
      label139:
      throw a;
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((a)a(paramObject1, (c)paramObject2)).a(x.a);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.af.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */