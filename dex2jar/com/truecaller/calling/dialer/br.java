package com.truecaller.calling.dialer;

import android.os.CancellationSignal;
import c.d.c;
import c.d.f;
import c.n;
import com.truecaller.data.entity.Contact;
import java.util.List;

public abstract interface br
{
  public abstract Object a(f paramf, String paramString, Integer paramInteger, CancellationSignal paramCancellationSignal, c<? super List<? extends n<? extends Contact, String>>> paramc);
  
  public abstract Object a(String paramString, f paramf, c<? super Contact> paramc);
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.br
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */