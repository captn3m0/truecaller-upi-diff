package com.truecaller.calling.dialer;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import c.f;
import c.g.a.b;
import c.g.a.q;
import c.g.b.l;
import com.truecaller.adapter_delegates.i;
import com.truecaller.calling.an;
import com.truecaller.calling.ao;
import com.truecaller.calling.ba;
import com.truecaller.calling.bc;
import com.truecaller.calling.c;
import com.truecaller.calling.d;
import com.truecaller.calling.o;
import com.truecaller.calling.p;
import com.truecaller.presence.a;
import com.truecaller.search.local.model.c.a;
import com.truecaller.ui.view.AvailabilityView;
import com.truecaller.utils.extensions.t;

public final class cj
  extends RecyclerView.ViewHolder
  implements an, ao, bc, c, ck.c, p
{
  int b;
  q<? super a, ? super Integer, ? super Context, ? extends CharSequence> c;
  private final f d;
  private final f e;
  private final f f;
  
  private cj(final View paramView, com.truecaller.adapter_delegates.k paramk, o paramo)
  {
    super(paramView);
    g = paramo;
    h = paramo;
    i = paramo;
    j = new ba(paramView);
    k = new d(paramView);
    b = -1;
    d = t.a(paramView, 2131363510);
    e = t.a(paramView, 2131363946);
    f = t.a(paramView, 2131362067);
    paramo = (RecyclerView.ViewHolder)this;
    i.a(paramView, paramk, paramo, null, 12);
    i.a(paramView, paramk, paramo);
    a().setSingleLine();
    paramk = a();
    c.g.b.k.a(paramk, "titleTextView");
    paramk.setHorizontalFadingEdgeEnabled(true);
    b().setSingleLine();
    paramk = b();
    c.g.b.k.a(paramk, "availability");
    paramk.setHorizontalFadingEdgeEnabled(true);
    b().setCustomTextProvider((b)new l(paramView) {});
  }
  
  private final TextView a()
  {
    return (TextView)d.b();
  }
  
  private final AvailabilityView b()
  {
    return (AvailabilityView)f.b();
  }
  
  public final void a(int paramInt)
  {
    i.a(paramInt);
  }
  
  public final void a(q<? super a, ? super Integer, ? super Context, ? extends CharSequence> paramq)
  {
    c = paramq;
  }
  
  public final void a(c.a parama)
  {
    k.a(parama);
  }
  
  public final void a(Object paramObject)
  {
    h.a(paramObject);
  }
  
  public final void a_(boolean paramBoolean)
  {
    g.a_(paramBoolean);
  }
  
  public final void b(int paramInt)
  {
    b = paramInt;
  }
  
  public final void b(boolean paramBoolean)
  {
    View localView = (View)e.b();
    c.g.b.k.a(localView, "pinBadge");
    t.a(localView, paramBoolean);
  }
  
  public final void b_(String paramString)
  {
    j.b_(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.cj
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */