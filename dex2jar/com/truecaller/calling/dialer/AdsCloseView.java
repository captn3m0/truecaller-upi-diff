package com.truecaller.calling.dialer;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import c.g.b.k;
import c.n.m;
import c.u;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.f;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.premium.br;
import com.truecaller.ui.view.TintedImageView;
import com.truecaller.utils.extensions.t;

public final class AdsCloseView
  extends TintedImageView
{
  private final bp a;
  private final String b;
  
  public AdsCloseView(final Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    paramAttributeSet = paramContext.getApplicationContext();
    if (paramAttributeSet != null)
    {
      paramAttributeSet = ((TrueApp)paramAttributeSet).a();
      k.a(paramAttributeSet, "(context.applicationCont… as TrueApp).objectsGraph");
      a = paramAttributeSet;
      b = a.aF().ag().e();
      if (m.a((CharSequence)b, (CharSequence)"megaAdsViews", false))
      {
        t.a(this);
        setOnClickListener((View.OnClickListener)new View.OnClickListener()
        {
          public final void onClick(View paramAnonymousView)
          {
            AdsCloseView.a(a);
            br.a(paramContext, PremiumPresenterView.LaunchContext.MEGA_ADS_CLOSE);
          }
        });
      }
      return;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.TrueApp");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.AdsCloseView
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */