package com.truecaller.calling.dialer;

import android.support.v7.view.ActionMode;
import android.support.v7.view.ActionMode.Callback;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import c.a.ae;
import c.a.m;
import c.g.b.k;
import c.k.i;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public final class l$a
  implements ActionMode.Callback
{
  final int a = 1;
  
  public final boolean onActionItemClicked(ActionMode paramActionMode, MenuItem paramMenuItem)
  {
    k.b(paramActionMode, "actionMode");
    k.b(paramMenuItem, "menuItem");
    return b.a().a(a, paramMenuItem.getItemId());
  }
  
  public final boolean onCreateActionMode(ActionMode paramActionMode, Menu paramMenu)
  {
    k.b(paramActionMode, "actionMode");
    k.b(paramMenu, "menu");
    Integer localInteger = Integer.valueOf(b.a().c(a));
    int i;
    if (((Number)localInteger).intValue() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i == 0) {
      localInteger = null;
    }
    if (localInteger != null)
    {
      i = ((Number)localInteger).intValue();
      paramActionMode.getMenuInflater().inflate(i, paramMenu);
    }
    paramActionMode.setTag(Integer.valueOf(a));
    l.a(b, paramActionMode);
    b.a().a(a);
    return true;
  }
  
  public final void onDestroyActionMode(ActionMode paramActionMode)
  {
    k.b(paramActionMode, "actionMode");
    b.a().b(a);
  }
  
  public final boolean onPrepareActionMode(ActionMode paramActionMode, Menu paramMenu)
  {
    k.b(paramActionMode, "actionMode");
    k.b(paramMenu, "menu");
    Object localObject = b.a().c();
    if (localObject != null) {
      paramActionMode.setTitle((CharSequence)localObject);
    }
    localObject = (Iterable)i.b(0, paramMenu.size());
    paramActionMode = (Collection)new ArrayList(m.a((Iterable)localObject, 10));
    localObject = ((Iterable)localObject).iterator();
    while (((Iterator)localObject).hasNext()) {
      paramActionMode.add(paramMenu.getItem(((ae)localObject).a()));
    }
    paramActionMode = ((Iterable)paramActionMode).iterator();
    while (paramActionMode.hasNext())
    {
      paramMenu = (MenuItem)paramActionMode.next();
      k.a(paramMenu, "it");
      paramMenu.setVisible(b.a().b(a, paramMenu.getItemId()));
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.l.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */