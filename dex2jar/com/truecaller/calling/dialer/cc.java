package com.truecaller.calling.dialer;

import c.g.b.k;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.inject.Inject;

public final class cc
  implements bu
{
  private final HashMap<String, Set<Integer>> a = new HashMap();
  
  public final Set<Integer> a(String paramString)
  {
    k.b(paramString, "searchTerm");
    return (Set)a.get(paramString);
  }
  
  public final void a()
  {
    a.clear();
  }
  
  public final void a(String paramString, int paramInt)
  {
    k.b(paramString, "searchTerm");
    Map localMap = (Map)a;
    Object localObject2 = localMap.get(paramString);
    Object localObject1 = localObject2;
    if (localObject2 == null)
    {
      localObject1 = new HashSet();
      localMap.put(paramString, localObject1);
    }
    ((Set)localObject1).add(Integer.valueOf(paramInt));
  }
  
  public final boolean a(int paramInt)
  {
    Object localObject = a.values();
    k.a(localObject, "keyToPositionsMap.values");
    localObject = (Iterable)localObject;
    if ((!(localObject instanceof Collection)) || (!((Collection)localObject).isEmpty()))
    {
      localObject = ((Iterable)localObject).iterator();
      while (((Iterator)localObject).hasNext()) {
        if (((Set)((Iterator)localObject).next()).contains(Integer.valueOf(paramInt))) {
          return true;
        }
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.cc
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */