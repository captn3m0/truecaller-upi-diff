package com.truecaller.calling.dialer;

public enum CallIconType
{
  static
  {
    CallIconType localCallIconType1 = new CallIconType("HUNG_UP_CALL_ICON", 0);
    HUNG_UP_CALL_ICON = localCallIconType1;
    CallIconType localCallIconType2 = new CallIconType("MUTED_CALL_ICON", 1);
    MUTED_CALL_ICON = localCallIconType2;
    CallIconType localCallIconType3 = new CallIconType("INCOMING_CALL_ICON", 2);
    INCOMING_CALL_ICON = localCallIconType3;
    CallIconType localCallIconType4 = new CallIconType("OUTGOING_CALL_ICON", 3);
    OUTGOING_CALL_ICON = localCallIconType4;
    CallIconType localCallIconType5 = new CallIconType("MISSED_CALL_ICON", 4);
    MISSED_CALL_ICON = localCallIconType5;
    CallIconType localCallIconType6 = new CallIconType("FLASH", 5);
    FLASH = localCallIconType6;
    $VALUES = new CallIconType[] { localCallIconType1, localCallIconType2, localCallIconType3, localCallIconType4, localCallIconType5, localCallIconType6 };
  }
  
  private CallIconType() {}
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.CallIconType
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */