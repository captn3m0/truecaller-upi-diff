package com.truecaller.calling.dialer;

import android.view.View;
import com.truecaller.bn;
import com.truecaller.calling.dialer.suggested_contacts.e;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.network.search.e.a;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import java.util.List;
import java.util.Set;

public abstract interface ae
{
  public static abstract interface a
    extends bn<ae.b, ae.c>, a, ae.c.b, aj.a.a, bd, be.b.b, bm.a, cf.b.a, cf.b.b, ck.a, cn.a, i.a, e.a
  {
    public abstract void a(v paramv);
    
    public abstract void a(String paramString);
    
    public abstract boolean h();
    
    public abstract void i();
    
    public abstract void j();
    
    public abstract void k();
    
    public abstract void l();
    
    public abstract void m();
    
    public abstract int n();
  }
  
  public static abstract interface b
    extends bd
  {
    public abstract void b();
    
    public abstract void c();
    
    public abstract void d();
    
    public abstract void h();
    
    public abstract void i();
  }
  
  public static abstract interface c
  {
    public abstract void a(int paramInt);
    
    public abstract void a(View paramView);
    
    public abstract void a(View paramView, e parame, String paramString);
    
    public abstract void a(aq paramaq);
    
    public abstract void a(e parame);
    
    public abstract void a(String paramString);
    
    public abstract void a(String paramString, PremiumPresenterView.LaunchContext paramLaunchContext);
    
    public abstract void a(String paramString1, String paramString2, String paramString3);
    
    public abstract void a(String paramString, boolean paramBoolean);
    
    public abstract void a(List<com.truecaller.calling.dialer.a.a> paramList1, List<com.truecaller.calling.dialer.a.a> paramList2);
    
    public abstract void a(Set<Integer> paramSet);
    
    public abstract void a(boolean paramBoolean);
    
    public abstract void b();
    
    public abstract void b(int paramInt);
    
    public abstract void b(String paramString);
    
    public abstract void b(Set<Integer> paramSet);
    
    public abstract void b(boolean paramBoolean);
    
    public abstract void c();
    
    public abstract void c(int paramInt);
    
    public abstract void c(String paramString);
    
    public abstract void c(Set<Integer> paramSet);
    
    public abstract void c(boolean paramBoolean);
    
    public abstract void d();
    
    public abstract void d(int paramInt);
    
    public abstract void d(boolean paramBoolean);
    
    public abstract void e();
    
    public abstract void e(boolean paramBoolean);
    
    public abstract void f();
    
    public abstract void f(boolean paramBoolean);
    
    public abstract void g();
    
    public abstract void g(boolean paramBoolean);
    
    public abstract void h();
    
    public abstract void h(boolean paramBoolean);
    
    public abstract void i();
    
    public abstract void j();
    
    public abstract void k();
    
    public abstract void l();
    
    public abstract void m();
    
    public abstract void n();
    
    public abstract void o();
    
    public static final class a {}
    
    public static abstract interface b
    {
      public abstract void A();
      
      public abstract void B();
      
      public abstract void C();
      
      public abstract void D();
      
      public abstract void a(long paramLong);
      
      public abstract void a(e parame);
      
      public abstract void a(String paramString, TruecallerContract.Filters.EntityType paramEntityType);
      
      public abstract void a(boolean paramBoolean);
      
      public abstract void b(e parame);
      
      public abstract void c(e parame);
      
      public abstract void d(int paramInt);
      
      public abstract void o();
      
      public abstract void p();
      
      public abstract void q();
      
      public abstract void r();
      
      public abstract void s();
      
      public abstract void t();
      
      public abstract void u();
      
      public abstract void v();
      
      public abstract void w();
      
      public abstract void x();
      
      public abstract void y();
      
      public abstract void z();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.ae
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */