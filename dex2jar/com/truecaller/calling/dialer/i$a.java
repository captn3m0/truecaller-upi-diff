package com.truecaller.calling.dialer;

import c.l.g;
import com.truecaller.calling.af;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.flashsdk.db.l;
import java.util.List;
import java.util.Map;

public abstract interface i$a
{
  public abstract cb a(i.b<?> paramb);
  
  public abstract List<aw> a(i.b<?> paramb, g<?> paramg);
  
  public abstract void a(HistoryEvent paramHistoryEvent);
  
  public abstract boolean b(HistoryEvent paramHistoryEvent);
  
  public abstract Map<Integer, l> e();
  
  public abstract af f();
  
  public abstract Map<Integer, Boolean> g();
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.i.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */