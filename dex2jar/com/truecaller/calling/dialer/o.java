package com.truecaller.calling.dialer;

import c.g.b.k;
import c.t;
import com.truecaller.calling.ActionType;
import com.truecaller.common.h.ab;
import com.truecaller.common.h.aj;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.g;
import com.truecaller.flashsdk.core.i;
import com.truecaller.flashsdk.db.l;
import com.truecaller.multisim.ae;
import com.truecaller.network.search.e;
import com.truecaller.util.cf;
import com.truecaller.voip.ai;
import javax.inject.Inject;
import javax.inject.Named;

public final class o
  extends j<n.b>
  implements n.a
{
  private final boolean g;
  private final boolean h;
  private final cb i;
  private c.n<? extends HistoryEvent, ? extends ActionType> j;
  private final com.truecaller.search.local.model.c k;
  private final com.truecaller.i.c l;
  private final com.truecaller.utils.n m;
  private final ax n;
  private final g o;
  private final com.truecaller.util.af p;
  private final ae q;
  private final aj r;
  private final e s;
  
  @Inject
  public o(i.a parama, @Named("DialerAvailabilityManager") com.truecaller.search.local.model.c paramc, a parama1, bd parambd, com.truecaller.analytics.b paramb, i parami, com.truecaller.flashsdk.core.b paramb1, com.truecaller.i.c paramc1, com.truecaller.utils.n paramn, ax paramax, g paramg, com.truecaller.util.af paramaf, ae paramae, com.truecaller.multisim.h paramh, aj paramaj, com.truecaller.calling.recorder.h paramh1, @Named("DialerBulkSearcher") e parame, cf paramcf, ai paramai)
  {
    super(parama, parambd, paramb, parama1, parami, paramb1, paramcf, paramai);
    k = paramc;
    l = paramc1;
    m = paramn;
    n = paramax;
    o = paramg;
    p = paramaf;
    q = paramae;
    r = paramaj;
    s = parame;
    g = paramh1.a();
    h = paramh.j();
    i = parama.a((i.b)this);
  }
  
  private final void a(HistoryEvent paramHistoryEvent, ActionType paramActionType)
  {
    if (!b()) {
      paramActionType = ActionType.PROFILE;
    }
    a(paramHistoryEvent, paramActionType, "item");
  }
  
  private final boolean b()
  {
    return k.a("call", l.a("callLogTapBehavior"));
  }
  
  public final boolean a(int paramInt)
  {
    return !c(paramInt);
  }
  
  public final boolean a(com.truecaller.adapter_delegates.h paramh)
  {
    k.b(paramh, "event");
    Object localObject1 = ActionType.Companion;
    Object localObject3 = a;
    k.b(localObject3, "action");
    ActionType[] arrayOfActionType = ActionType.values();
    int i2 = arrayOfActionType.length;
    int i1 = 0;
    Object localObject2;
    for (;;)
    {
      localObject2 = null;
      if (i1 >= i2) {
        break;
      }
      localObject1 = arrayOfActionType[i1];
      if (k.a(((ActionType)localObject1).getEventAction(), localObject3)) {
        break label77;
      }
      i1 += 1;
    }
    localObject1 = null;
    label77:
    if (localObject1 != null)
    {
      localObject3 = e;
      if (localObject3 != null) {
        localObject2 = localObject3.toString();
      }
      localObject2 = t.a(localObject1, localObject2);
      localObject1 = (ActionType)a;
      localObject2 = (String)b;
      return a(b(b), (ActionType)localObject1, (String)localObject2);
    }
    return super.a(paramh);
  }
  
  protected final boolean a(ActionType paramActionType, int paramInt)
  {
    k.b(paramActionType, "primaryAction");
    HistoryEvent localHistoryEvent = b(paramInt);
    if (ab.a(localHistoryEvent.b())) {
      return true;
    }
    if (!l.b("madeCallsFromCallLog"))
    {
      l.b("madeCallsFromCallLog", true);
      d.w_();
      j = t.a(localHistoryEvent, paramActionType);
      return true;
    }
    a(localHistoryEvent, paramActionType);
    return true;
  }
  
  public final boolean d(int paramInt)
  {
    return (!a) && (!ab.a(b(paramInt).b()));
  }
  
  public final boolean u_()
  {
    c.n localn = j;
    if (localn != null)
    {
      a((HistoryEvent)a, (ActionType)b);
      j = null;
      return true;
    }
    return false;
  }
  
  public static final class a
    implements l
  {
    a(o paramo, int paramInt) {}
    
    public final void a()
    {
      a.c.f().a(b);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */