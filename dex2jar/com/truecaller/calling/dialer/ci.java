package com.truecaller.calling.dialer;

import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.LinearLayout;
import c.g.b.k;
import c.g.b.l;
import com.truecaller.adapter_delegates.g;
import com.truecaller.adapter_delegates.p;
import com.truecaller.adapter_delegates.s;
import com.truecaller.utils.extensions.t;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class ci
  extends RecyclerView.ViewHolder
  implements cf.b
{
  private final c.f b;
  private final c.f c;
  private final p<ck.c, cj> d;
  private final p<cn.c, cq> e;
  private final com.truecaller.adapter_delegates.f f;
  private Parcelable g;
  
  public ci(View paramView, ck.b paramb, cn.b paramb1)
  {
    super(paramView);
    b = t.a(paramView, 2131364093);
    c = t.a(paramView, 2131363644);
    d = new p((com.truecaller.adapter_delegates.b)paramb, 2131559020, (c.g.a.b)new a(this), (c.g.a.b)b.a);
    e = new p((com.truecaller.adapter_delegates.b)paramb1, 2131559021, (c.g.a.b)new c(this), (c.g.a.b)d.a);
    paramb = new com.truecaller.adapter_delegates.f((com.truecaller.adapter_delegates.a)d.a((com.truecaller.adapter_delegates.a)e, (s)new g((byte)0)));
    paramb.setHasStableIds(true);
    f = paramb;
    paramb = d();
    k.a(paramb, "recycleView");
    paramb.setLayoutManager((RecyclerView.LayoutManager)new LinearLayoutManager(paramView.getContext(), 0, false));
    paramView = d();
    k.a(paramView, "recycleView");
    paramView.setAdapter((RecyclerView.Adapter)f);
  }
  
  private final RecyclerView d()
  {
    return (RecyclerView)b.b();
  }
  
  public final int a()
  {
    f.notifyDataSetChanged();
    return d.getItemCount();
  }
  
  public final void a(List<com.truecaller.calling.dialer.a.a> paramList1, List<com.truecaller.calling.dialer.a.a> paramList2)
  {
    k.b(paramList1, "oldItems");
    k.b(paramList2, "newItems");
    if (paramList1.size() < paramList2.size())
    {
      f.notifyItemInserted(0);
      return;
    }
    if (paramList1.size() > paramList2.size())
    {
      f.notifyItemRemoved(0);
      return;
    }
    f.notifyItemChanged(0);
  }
  
  public final void a(Set<Integer> paramSet)
  {
    k.b(paramSet, "itemPositions");
    paramSet = ((Iterable)paramSet).iterator();
    while (paramSet.hasNext())
    {
      int i = ((Number)paramSet.next()).intValue();
      f.notifyItemChanged(d.a_(i));
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject = d();
    k.a(localObject, "recycleView");
    t.a((View)localObject, paramBoolean ^ true);
    localObject = (LinearLayout)c.b();
    k.a(localObject, "emptyStateLinearLayout");
    t.a((View)localObject, paramBoolean);
  }
  
  public final void b()
  {
    Parcelable localParcelable = g;
    if (localParcelable != null)
    {
      Object localObject = d();
      k.a(localObject, "recycleView");
      localObject = ((RecyclerView)localObject).getLayoutManager();
      if (localObject != null) {
        ((RecyclerView.LayoutManager)localObject).onRestoreInstanceState(localParcelable);
      }
      g = null;
      return;
    }
  }
  
  public final void c()
  {
    Object localObject = d();
    k.a(localObject, "recycleView");
    localObject = ((RecyclerView)localObject).getLayoutManager();
    if (localObject != null) {
      localObject = ((RecyclerView.LayoutManager)localObject).onSaveInstanceState();
    } else {
      localObject = null;
    }
    g = ((Parcelable)localObject);
  }
  
  static final class a
    extends l
    implements c.g.a.b<View, cj>
  {
    a(ci paramci)
    {
      super();
    }
  }
  
  static final class b
    extends l
    implements c.g.a.b<cj, cj>
  {
    public static final b a = new b();
    
    b()
    {
      super();
    }
  }
  
  static final class c
    extends l
    implements c.g.a.b<View, cq>
  {
    c(ci paramci)
    {
      super();
    }
  }
  
  static final class d
    extends l
    implements c.g.a.b<cq, cq>
  {
    public static final d a = new d();
    
    d()
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.ci
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */