package com.truecaller.calling.dialer;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.calling.dialer.suggested_contacts.e;
import java.util.List;
import kotlinx.coroutines.ag;

@c.d.b.a.f(b="DialerPresenter.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.dialer.DialerPresenter$loadSuggestedContacts$2")
final class af$e
  extends c.d.b.a.k
  implements m<ag, c<? super List<? extends e>>, Object>
{
  int a;
  private ag c;
  
  af$e(af paramaf, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new e(b, paramc);
    c = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    if (a == 0)
    {
      if (!(paramObject instanceof o.b)) {
        return b.p.a(10);
      }
      throw a;
    }
    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((e)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.af.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */