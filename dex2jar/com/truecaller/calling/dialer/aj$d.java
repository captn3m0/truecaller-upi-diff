package com.truecaller.calling.dialer;

import com.truecaller.ui.keyboard.b;
import com.truecaller.ui.keyboard.c;
import com.truecaller.ui.view.BottomBar.DialpadState;
import com.truecaller.util.ca;

public abstract interface aj$d
  extends aj.a
{
  public abstract void a(BottomBar.DialpadState paramDialpadState);
  
  public abstract void a(ca paramca);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void b();
  
  public abstract void b(String paramString);
  
  public abstract void c();
  
  public abstract void c(String paramString);
  
  public abstract void d();
  
  public abstract void d(String paramString);
  
  public static abstract interface a
    extends SelectionAwareEditText.a, b, c
  {
    public abstract void a();
    
    public abstract void a(ap paramap);
    
    public abstract void a(String paramString);
    
    public abstract void b(String paramString);
    
    public abstract void c(String paramString);
    
    public abstract void d();
    
    public abstract void g();
    
    public abstract void h();
    
    public abstract void i();
    
    public abstract void j();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.aj.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */