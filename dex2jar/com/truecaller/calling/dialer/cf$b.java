package com.truecaller.calling.dialer;

import android.view.View;
import com.truecaller.calling.dialer.a.a;
import com.truecaller.calling.dialer.suggested_contacts.e;
import java.util.List;
import java.util.Set;

public abstract interface cf$b
{
  public abstract int a();
  
  public abstract void a(List<a> paramList1, List<a> paramList2);
  
  public abstract void a(Set<Integer> paramSet);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void b();
  
  public abstract void c();
  
  public static abstract interface a
  {
    public abstract void a(View paramView, e parame, String paramString);
    
    public abstract void a(String paramString1, String paramString2, int paramInt);
  }
  
  public static abstract interface b
  {
    public abstract void a(View paramView);
    
    public abstract void b(String paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.cf.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */