package com.truecaller.calling.dialer;

import c.d.a.a;
import c.d.b.a.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.data.entity.Contact;
import kotlinx.coroutines.ag;

@f(b="DialerPresenter.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.dialer.DialerPresenter$refetchContactIfNeeded$1$1")
final class af$g$1
  extends c.d.b.a.k
  implements m<ag, c.d.c<? super Contact>, Object>
{
  int a;
  private ag c;
  
  af$g$1(af.g paramg, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c<x> a(Object paramObject, c.d.c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new 1(b, paramc);
    c = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    if (a == 0)
    {
      if (!(paramObject instanceof o.b)) {
        return b.b.t.a(b.c);
      }
      throw a;
    }
    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((1)a(paramObject1, (c.d.c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.af.g.1
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */