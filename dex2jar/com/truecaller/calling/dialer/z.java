package com.truecaller.calling.dialer;

import android.content.ContentResolver;
import dagger.a.d;
import javax.inject.Provider;

public final class z
  implements d<s>
{
  private final Provider<ContentResolver> a;
  
  private z(Provider<ContentResolver> paramProvider)
  {
    a = paramProvider;
  }
  
  public static z a(Provider<ContentResolver> paramProvider)
  {
    return new z(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.z
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */