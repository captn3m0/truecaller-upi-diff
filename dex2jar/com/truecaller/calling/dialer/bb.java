package com.truecaller.calling.dialer;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import c.f;
import com.truecaller.adapter_delegates.h;
import com.truecaller.adapter_delegates.i;
import com.truecaller.calling.ao;
import com.truecaller.calling.ba;
import com.truecaller.calling.bc;
import com.truecaller.calling.o;
import com.truecaller.calling.p;
import com.truecaller.calling.w;
import com.truecaller.calling.x;
import com.truecaller.calling.y;
import com.truecaller.flashsdk.ui.ProgressAwareFlashButton;
import com.truecaller.flashsdk.ui.ProgressAwareFlashButton.b;
import com.truecaller.utils.extensions.t;
import com.truecaller.utils.ui.b;

public final class bb
  extends RecyclerView.ViewHolder
  implements ao, bc, ay.b, p, w
{
  private final f b;
  private final View c;
  private final Object d;
  
  private bb(View paramView, final com.truecaller.adapter_delegates.k paramk, Object paramObject, o paramo)
  {
    super(paramView);
    e = new y(paramView);
    f = paramo;
    g = paramo;
    h = new ba(paramView);
    c = paramView;
    d = paramObject;
    b = t.a(c, 2131363109);
    paramObject = c;
    ((View)paramObject).setOnClickListener((View.OnClickListener)new a(this, paramk));
    paramView = (RecyclerView.ViewHolder)this;
    i.a((View)paramObject, paramk, paramView);
    paramObject = a();
    ((ProgressAwareFlashButton)paramObject).setClickable(false);
    ((ProgressAwareFlashButton)paramObject).setThemeColor(b.a(((ProgressAwareFlashButton)paramObject).getContext(), 2130969528));
    c.g.b.k.b(paramObject, "receiver$0");
    c.g.b.k.b(paramk, "eventReceiver");
    c.g.b.k.b(paramView, "viewHolder");
    ((ProgressAwareFlashButton)paramObject).setFlashProgressFinishListener((ProgressAwareFlashButton.b)new bc.a(paramView, paramk));
  }
  
  private final ProgressAwareFlashButton a()
  {
    return (ProgressAwareFlashButton)b.b();
  }
  
  public final void a(int paramInt)
  {
    g.a(paramInt);
  }
  
  public final void a(x paramx)
  {
    e.a(paramx);
  }
  
  public final void a(Object paramObject)
  {
    f.a(paramObject);
  }
  
  public final void b_(String paramString)
  {
    h.b_(paramString);
  }
  
  public final void g(boolean paramBoolean)
  {
    c.setActivated(paramBoolean);
  }
  
  static final class a
    implements View.OnClickListener
  {
    a(bb parambb, com.truecaller.adapter_delegates.k paramk) {}
    
    public final void onClick(View paramView)
    {
      if (!paramk.a(new h("ItemEvent.CLICKED", (RecyclerView.ViewHolder)a, null, 12))) {
        bb.a(a).callOnClick();
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bb
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */