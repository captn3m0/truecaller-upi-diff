package com.truecaller.calling.dialer;

import c.g.b.k;

public abstract class aj$a$b
{
  public static final class a
    extends aj.a.b
  {
    public static final a a = new a();
    
    private a()
    {
      super();
    }
  }
  
  public static final class b
    extends aj.a.b
  {
    public static final b a = new b();
    
    private b()
    {
      super();
    }
  }
  
  public static final class c
    extends aj.a.b
  {
    final String a;
    
    public c(String paramString)
    {
      super();
      a = paramString;
    }
    
    public final boolean equals(Object paramObject)
    {
      if (this != paramObject) {
        if ((paramObject instanceof c))
        {
          paramObject = (c)paramObject;
          if (k.a(a, a)) {}
        }
        else
        {
          return false;
        }
      }
      return true;
    }
    
    public final int hashCode()
    {
      String str = a;
      if (str != null) {
        return str.hashCode();
      }
      return 0;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder("TapToPaste(number=");
      localStringBuilder.append(a);
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.aj.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */