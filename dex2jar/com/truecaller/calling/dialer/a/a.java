package com.truecaller.calling.dialer.a;

import c.g.b.k;

public final class a
{
  public final int a;
  public final int b;
  public final String c;
  
  public a(int paramInt1, int paramInt2, String paramString)
  {
    a = paramInt1;
    b = paramInt2;
    c = paramString;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      if ((paramObject instanceof a))
      {
        paramObject = (a)paramObject;
        int i;
        if (a == a) {
          i = 1;
        } else {
          i = 0;
        }
        if (i != 0)
        {
          if (b == b) {
            i = 1;
          } else {
            i = 0;
          }
          if ((i != 0) && (k.a(c, c))) {
            return true;
          }
        }
      }
      return false;
    }
    return true;
  }
  
  public final int hashCode()
  {
    int j = a;
    int k = b;
    String str = c;
    int i;
    if (str != null) {
      i = str.hashCode();
    } else {
      i = 0;
    }
    return (j * 31 + k) * 31 + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("SuggestedPremium(iconRes=");
    localStringBuilder.append(a);
    localStringBuilder.append(", titleRes=");
    localStringBuilder.append(b);
    localStringBuilder.append(", premiumPage=");
    localStringBuilder.append(c);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */