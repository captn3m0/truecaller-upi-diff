package com.truecaller.calling.dialer.a;

import c.a.m;
import c.a.y;
import c.g.b.k;
import com.truecaller.common.h.an;
import com.truecaller.i.e;
import com.truecaller.util.af;
import com.truecaller.util.al;
import java.util.List;
import javax.inject.Inject;

public final class c
  implements b
{
  private final al a;
  private final com.truecaller.common.f.c b;
  private final e c;
  private final an d;
  private final af e;
  
  @Inject
  public c(al paramal, com.truecaller.common.f.c paramc, e parame, an paraman, af paramaf)
  {
    a = paramal;
    b = paramc;
    c = parame;
    d = paraman;
    e = paramaf;
  }
  
  public final List<a> a()
  {
    boolean bool2 = a.a();
    boolean bool1 = true;
    if ((bool2) && (b.l()) && ((!b.d()) || (!k.a(b.k(), "gold"))))
    {
      long l1 = c.a("suggestedPremiumLastShownTimeStamp", 0L);
      long l2 = c.a("suggestedPremiumDismissedTimeStamp", 0L);
      if (l1 == 0L)
      {
        c.b("suggestedPremiumLastShownTimeStamp", d.a());
      }
      else if ((b.d()) && (k.a(b.k(), "regular")))
      {
        if (l2 == 0L) {
          bool1 = e.a(l1, d.a());
        } else {
          bool1 = false;
        }
      }
      else if (l2 == 0L)
      {
        if (!e.a(l1, d.a())) {
          if (e.e(l1) != e.e(d.a())) {
            c.b("suggestedPremiumLastShownTimeStamp", d.a());
          } else {
            bool1 = false;
          }
        }
      }
      else if (e.e(l1) != e.e(d.a()))
      {
        c.b("suggestedPremiumLastShownTimeStamp", d.a());
        c.b("suggestedPremiumDismissedTimeStamp", 0L);
      }
      else
      {
        bool1 = false;
      }
    }
    else
    {
      bool1 = false;
    }
    if (bool1)
    {
      String str = b.k();
      if ((str != null) && (str.hashCode() == 3387192) && (str.equals("none"))) {
        return m.a(new a(2131233940, 2131887252, "premium"));
      }
      return m.a(new a(2131233941, 2131887251, "gold"));
    }
    return (List)y.a;
  }
  
  public final void b()
  {
    c.b("suggestedPremiumDismissedTimeStamp", d.a());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */