package com.truecaller.calling.dialer.a;

import com.truecaller.common.h.an;
import com.truecaller.i.e;
import com.truecaller.util.af;
import com.truecaller.util.al;
import javax.inject.Provider;

public final class d
  implements dagger.a.d<c>
{
  private final Provider<al> a;
  private final Provider<com.truecaller.common.f.c> b;
  private final Provider<e> c;
  private final Provider<an> d;
  private final Provider<af> e;
  
  private d(Provider<al> paramProvider, Provider<com.truecaller.common.f.c> paramProvider1, Provider<e> paramProvider2, Provider<an> paramProvider3, Provider<af> paramProvider4)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static d a(Provider<al> paramProvider, Provider<com.truecaller.common.f.c> paramProvider1, Provider<e> paramProvider2, Provider<an> paramProvider3, Provider<af> paramProvider4)
  {
    return new d(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.a.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */