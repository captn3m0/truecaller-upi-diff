package com.truecaller.calling.dialer;

import com.truecaller.analytics.b;
import com.truecaller.calling.d.q;
import com.truecaller.common.account.r;
import com.truecaller.data.entity.g;
import com.truecaller.i.c;
import com.truecaller.util.al;
import com.truecaller.util.bz;
import dagger.a.d;
import javax.inject.Provider;

public final class an
  implements d<ak>
{
  private final Provider<bd> a;
  private final Provider<r> b;
  private final Provider<bz> c;
  private final Provider<al> d;
  private final Provider<c> e;
  private final Provider<q> f;
  private final Provider<aj.a.a> g;
  private final Provider<b> h;
  private final Provider<g> i;
  private final Provider<bm.a> j;
  
  private an(Provider<bd> paramProvider, Provider<r> paramProvider1, Provider<bz> paramProvider2, Provider<al> paramProvider3, Provider<c> paramProvider4, Provider<q> paramProvider5, Provider<aj.a.a> paramProvider6, Provider<b> paramProvider7, Provider<g> paramProvider8, Provider<bm.a> paramProvider9)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
  }
  
  public static an a(Provider<bd> paramProvider, Provider<r> paramProvider1, Provider<bz> paramProvider2, Provider<al> paramProvider3, Provider<c> paramProvider4, Provider<q> paramProvider5, Provider<aj.a.a> paramProvider6, Provider<b> paramProvider7, Provider<g> paramProvider8, Provider<bm.a> paramProvider9)
  {
    return new an(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.an
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */