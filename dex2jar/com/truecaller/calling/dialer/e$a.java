package com.truecaller.calling.dialer;

import c.d.b.a.f;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.androidactors.w;
import java.util.List;
import kotlinx.coroutines.ag;

@f(b="CallHistoryProvider.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.dialer.CallHistoryDataProvider$deleteHistoryAsync$1")
final class e$a
  extends c.d.b.a.k
  implements m<ag, c<? super Boolean>, Object>
{
  int a;
  private ag f;
  
  e$a(e parame, List paramList1, List paramList2, HistoryEventsScope paramHistoryEventsScope, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new a(b, c, d, e, paramc);
    f = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    c.d.a.a locala = c.d.a.a.a;
    if (a == 0)
    {
      if (!(paramObject instanceof o.b)) {
        return b.a.a(c, d, e).d();
      }
      throw a;
    }
    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((a)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.e.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */