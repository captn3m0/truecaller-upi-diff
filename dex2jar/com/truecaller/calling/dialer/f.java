package com.truecaller.calling.dialer;

import com.truecaller.analytics.au;
import com.truecaller.callhistory.a;
import dagger.a.d;
import javax.inject.Provider;

public final class f
  implements d<e>
{
  private final Provider<a> a;
  private final Provider<au> b;
  
  private f(Provider<a> paramProvider, Provider<au> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static f a(Provider<a> paramProvider, Provider<au> paramProvider1)
  {
    return new f(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */