package com.truecaller.calling.dialer;

import com.truecaller.data.access.s;
import com.truecaller.network.search.l;
import dagger.a.d;
import javax.inject.Provider;

public final class bt
  implements d<bs>
{
  private final Provider<s> a;
  private final Provider<l> b;
  
  private bt(Provider<s> paramProvider, Provider<l> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static bt a(Provider<s> paramProvider, Provider<l> paramProvider1)
  {
    return new bt(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bt
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */