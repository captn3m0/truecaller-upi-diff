package com.truecaller.calling.dialer;

import c.l.g;
import com.truecaller.adapter_delegates.j;
import com.truecaller.adapter_delegates.n;
import com.truecaller.calling.af;
import com.truecaller.calling.ao;
import com.truecaller.calling.bc;
import com.truecaller.calling.p;
import com.truecaller.calling.w;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.flashsdk.db.l;
import java.util.List;
import java.util.Map;

public abstract interface i
{
  public static abstract interface a
  {
    public abstract cb a(i.b<?> paramb);
    
    public abstract List<aw> a(i.b<?> paramb, g<?> paramg);
    
    public abstract void a(HistoryEvent paramHistoryEvent);
    
    public abstract boolean b(HistoryEvent paramHistoryEvent);
    
    public abstract Map<Integer, l> e();
    
    public abstract af f();
    
    public abstract Map<Integer, Boolean> g();
  }
  
  public static abstract interface b<V extends i.c>
    extends j, n<V>
  {}
  
  public static abstract interface c
    extends ao, bc, p, w
  {
    public abstract void g(boolean paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */