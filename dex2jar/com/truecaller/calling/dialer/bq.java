package com.truecaller.calling.dialer;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import c.f;
import com.truecaller.adapter_delegates.i;
import com.truecaller.utils.extensions.t;

public final class bq
  extends RecyclerView.ViewHolder
  implements bn.b
{
  private final f b;
  
  public bq(View paramView, com.truecaller.adapter_delegates.k paramk)
  {
    super(paramView);
    b = t.a(paramView, 2131363718);
    i.a(paramView, paramk, (RecyclerView.ViewHolder)this, null, 12);
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "searchText");
    TextView localTextView = (TextView)b.b();
    c.g.b.k.a(localTextView, "searchTextView");
    localTextView.setText((CharSequence)paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bq
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */