package com.truecaller.calling.dialer;

import android.view.View;
import com.truecaller.adapter_delegates.b;

public abstract interface be
{
  public static abstract interface a
    extends b<be.b>, be.b.a
  {}
  
  public static abstract interface b
  {
    public abstract void a(int paramInt);
    
    public abstract void b(int paramInt);
    
    public static abstract interface a
    {
      public abstract void a();
      
      public abstract void a(View paramView);
    }
    
    public static abstract interface b
    {
      public abstract void H();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.be
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */