package com.truecaller.calling.dialer;

import android.content.Context;
import com.truecaller.network.search.e;
import com.truecaller.network.search.e.a;
import dagger.a.d;
import javax.inject.Provider;

public final class ab
  implements d<e>
{
  private final Provider<Context> a;
  private final Provider<e.a> b;
  
  private ab(Provider<Context> paramProvider, Provider<e.a> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static ab a(Provider<Context> paramProvider, Provider<e.a> paramProvider1)
  {
    return new ab(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.ab
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */