package com.truecaller.calling.dialer.suggested_contacts;

import android.content.ComponentName;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.service.chooser.ChooserTarget;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.truecaller.common.h.aq.d;
import com.truecaller.data.entity.Contact;
import com.truecaller.messaging.sharing.SharingActivity;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;

@c.d.b.a.f(b="SuggestionsChooserTargetService.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.dialer.suggested_contacts.SuggestionsChooserTargetService$getChooserTargetsAsync$1")
final class SuggestionsChooserTargetService$a
  extends c.d.b.a.k
  implements m<ag, c<? super ArrayList<ChooserTarget>>, Object>
{
  int a;
  private ag c;
  
  SuggestionsChooserTargetService$a(SuggestionsChooserTargetService paramSuggestionsChooserTargetService, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new a(b, paramc);
    c = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    if (a == 0)
    {
      if (!(paramObject instanceof o.b))
      {
        paramObject = b.c;
        if (paramObject == null) {
          c.g.b.k.a("suggestedContactsManager");
        }
        paramObject = ((f)paramObject).a(4);
        ArrayList localArrayList = new ArrayList();
        SuggestionsChooserTargetService localSuggestionsChooserTargetService = b;
        Iterator localIterator = ((List)paramObject).iterator();
        while (localIterator.hasNext())
        {
          e locale = (e)localIterator.next();
          paramObject = b;
          if (paramObject != null)
          {
            localObject1 = ((Contact)paramObject).u();
            paramObject = ((Contact)paramObject).a(true);
            if (paramObject == null) {}
          }
          try
          {
            paramObject = w.a((Context)localSuggestionsChooserTargetService).a((Uri)paramObject).a((ai)aq.d.b()).a(2131166217, 2131166217).d();
            if (paramObject != null) {
              paramObject = Icon.createWithBitmap((Bitmap)paramObject);
            }
          }
          catch (IOException paramObject)
          {
            Object localObject2;
            float f;
            for (;;) {}
          }
          paramObject = null;
          break label166;
          paramObject = null;
          localObject1 = paramObject;
          label166:
          localObject2 = localObject1;
          if (org.c.a.a.a.k.b((CharSequence)localObject1)) {
            localObject2 = a;
          }
          if (paramObject == null) {
            paramObject = Icon.createWithResource((Context)localSuggestionsChooserTargetService, 2131233849);
          }
          if (!c) {
            f = 1.0F - localArrayList.size() / 4.0F;
          } else {
            f = 1.0F;
          }
          localObject1 = new Bundle();
          ((Bundle)localObject1).putParcelable("com.truecaller.suggestions.extra.PHONE_NUMBER", (Parcelable)Uri.fromParts("smsto", a, null));
          localArrayList.add(new ChooserTarget((CharSequence)localObject2, (Icon)paramObject, f, new ComponentName((Context)localSuggestionsChooserTargetService, SharingActivity.class), (Bundle)localObject1));
        }
        return localArrayList;
      }
      throw a;
    }
    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((a)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.suggested_contacts.SuggestionsChooserTargetService.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */