package com.truecaller.calling.dialer.suggested_contacts;

import com.truecaller.androidactors.f;
import javax.inject.Inject;

public final class a
  implements g
{
  private final f<com.truecaller.callhistory.a> a;
  
  @Inject
  public a(f<com.truecaller.callhistory.a> paramf)
  {
    a = paramf;
  }
  
  /* Error */
  public final java.util.List<c.n<String, com.truecaller.data.entity.Contact>> a(int paramInt)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 25	com/truecaller/calling/dialer/suggested_contacts/a:a	Lcom/truecaller/androidactors/f;
    //   4: invokeinterface 37 1 0
    //   9: checkcast 39	com/truecaller/callhistory/a
    //   12: iload_1
    //   13: invokeinterface 43 2 0
    //   18: invokevirtual 48	com/truecaller/androidactors/w:d	()Ljava/lang/Object;
    //   21: checkcast 50	com/truecaller/callhistory/z
    //   24: astore_2
    //   25: aload_2
    //   26: ifnonnull +10 -> 36
    //   29: getstatic 55	c/a/y:a	Lc/a/y;
    //   32: checkcast 57	java/util/List
    //   35: areturn
    //   36: aload_2
    //   37: ldc 59
    //   39: invokestatic 61	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   42: aload_2
    //   43: checkcast 63	android/database/Cursor
    //   46: astore 6
    //   48: aload 6
    //   50: checkcast 65	java/io/Closeable
    //   53: astore 5
    //   55: aconst_null
    //   56: astore 4
    //   58: aload 4
    //   60: astore_3
    //   61: new 67	java/util/ArrayList
    //   64: dup
    //   65: invokespecial 68	java/util/ArrayList:<init>	()V
    //   68: checkcast 70	java/util/Collection
    //   71: astore 7
    //   73: aload 4
    //   75: astore_3
    //   76: aload 6
    //   78: invokeinterface 74 1 0
    //   83: ifeq +114 -> 197
    //   86: aload 4
    //   88: astore_3
    //   89: aload 6
    //   91: checkcast 50	com/truecaller/callhistory/z
    //   94: astore_2
    //   95: aload 4
    //   97: astore_3
    //   98: aload_2
    //   99: invokeinterface 77 1 0
    //   104: astore 8
    //   106: aload 8
    //   108: ifnull +134 -> 242
    //   111: aload 4
    //   113: astore_3
    //   114: aload 8
    //   116: invokevirtual 82	com/truecaller/data/entity/HistoryEvent:a	()Ljava/lang/String;
    //   119: astore 8
    //   121: aload 8
    //   123: ifnonnull +6 -> 129
    //   126: goto +116 -> 242
    //   129: aload 4
    //   131: astore_3
    //   132: aload 8
    //   134: ldc 84
    //   136: invokestatic 61	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   139: aload 4
    //   141: astore_3
    //   142: aload_2
    //   143: invokeinterface 77 1 0
    //   148: astore_2
    //   149: aload_2
    //   150: ifnull +87 -> 237
    //   153: aload 4
    //   155: astore_3
    //   156: aload_2
    //   157: ldc 86
    //   159: invokestatic 61	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   162: aload 4
    //   164: astore_3
    //   165: new 88	c/n
    //   168: dup
    //   169: aload 8
    //   171: aload_2
    //   172: invokevirtual 92	com/truecaller/data/entity/HistoryEvent:s	()Lcom/truecaller/data/entity/Contact;
    //   175: invokespecial 95	c/n:<init>	(Ljava/lang/Object;Ljava/lang/Object;)V
    //   178: astore_2
    //   179: goto +3 -> 182
    //   182: aload 4
    //   184: astore_3
    //   185: aload 7
    //   187: aload_2
    //   188: invokeinterface 99 2 0
    //   193: pop
    //   194: goto -121 -> 73
    //   197: aload 4
    //   199: astore_3
    //   200: aload 7
    //   202: checkcast 57	java/util/List
    //   205: astore_2
    //   206: aload 5
    //   208: aconst_null
    //   209: invokestatic 104	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   212: aload_2
    //   213: checkcast 106	java/lang/Iterable
    //   216: invokestatic 112	c/a/m:e	(Ljava/lang/Iterable;)Ljava/util/List;
    //   219: areturn
    //   220: astore_2
    //   221: goto +8 -> 229
    //   224: astore_2
    //   225: aload_2
    //   226: astore_3
    //   227: aload_2
    //   228: athrow
    //   229: aload 5
    //   231: aload_3
    //   232: invokestatic 104	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   235: aload_2
    //   236: athrow
    //   237: aconst_null
    //   238: astore_2
    //   239: goto -57 -> 182
    //   242: aconst_null
    //   243: astore_2
    //   244: goto -62 -> 182
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	247	0	this	a
    //   0	247	1	paramInt	int
    //   24	189	2	localObject1	Object
    //   220	1	2	localObject2	Object
    //   224	12	2	localThrowable	Throwable
    //   238	6	2	localObject3	Object
    //   60	172	3	localObject4	Object
    //   56	142	4	localObject5	Object
    //   53	177	5	localCloseable	java.io.Closeable
    //   46	44	6	localCursor	android.database.Cursor
    //   71	130	7	localCollection	java.util.Collection
    //   104	66	8	localObject6	Object
    // Exception table:
    //   from	to	target	type
    //   61	73	220	finally
    //   76	86	220	finally
    //   89	95	220	finally
    //   98	106	220	finally
    //   114	121	220	finally
    //   132	139	220	finally
    //   142	149	220	finally
    //   156	162	220	finally
    //   165	179	220	finally
    //   185	194	220	finally
    //   200	206	220	finally
    //   227	229	220	finally
    //   61	73	224	java/lang/Throwable
    //   76	86	224	java/lang/Throwable
    //   89	95	224	java/lang/Throwable
    //   98	106	224	java/lang/Throwable
    //   114	121	224	java/lang/Throwable
    //   132	139	224	java/lang/Throwable
    //   142	149	224	java/lang/Throwable
    //   156	162	224	java/lang/Throwable
    //   165	179	224	java/lang/Throwable
    //   185	194	224	java/lang/Throwable
    //   200	206	224	java/lang/Throwable
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.suggested_contacts.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */