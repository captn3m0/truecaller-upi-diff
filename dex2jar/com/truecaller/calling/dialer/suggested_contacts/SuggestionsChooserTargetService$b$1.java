package com.truecaller.calling.dialer.suggested_contacts;

import android.service.chooser.ChooserTarget;
import c.d.a.a;
import c.d.b.a.f;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import java.util.ArrayList;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;

@f(b="SuggestionsChooserTargetService.kt", c={63}, d="invokeSuspend", e="com.truecaller.calling.dialer.suggested_contacts.SuggestionsChooserTargetService$onGetChooserTargets$1$1")
final class SuggestionsChooserTargetService$b$1
  extends c.d.b.a.k
  implements m<ag, c<? super ArrayList<ChooserTarget>>, Object>
{
  int a;
  private ag c;
  
  SuggestionsChooserTargetService$b$1(SuggestionsChooserTargetService.b paramb, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new 1(b, paramc);
    c = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    switch (a)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 1: 
      if (!(paramObject instanceof o.b)) {
        return paramObject;
      }
      throw a;
    }
    if (!(paramObject instanceof o.b))
    {
      paramObject = SuggestionsChooserTargetService.a(b.b);
      a = 1;
      paramObject = ((ao)paramObject).a(this);
      if (paramObject == locala) {
        return locala;
      }
      return paramObject;
    }
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((1)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.suggested_contacts.SuggestionsChooserTargetService.b.1
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */