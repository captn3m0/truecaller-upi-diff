package com.truecaller.calling.dialer.suggested_contacts;

import c.a.ag;
import c.b.a;
import c.g.b.k;
import c.n;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.AssertionUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.inject.Inject;

public final class c
  implements f
{
  private final String a;
  private final com.truecaller.i.c b;
  private final g c;
  private final com.truecaller.data.access.c d;
  
  @Inject
  public c(com.truecaller.i.c paramc, g paramg, com.truecaller.data.access.c paramc1)
  {
    b = paramc;
    c = paramg;
    d = paramc1;
    a = "_";
  }
  
  private static String a(String paramString)
  {
    paramString = (CharSequence)paramString;
    Collection localCollection = (Collection)new ArrayList(paramString.length());
    int i = 0;
    while (i < paramString.length())
    {
      char c2 = paramString.charAt(i);
      char c1 = c2;
      if (Character.isDigit(c2)) {
        c1 = '0';
      }
      localCollection.add(Character.valueOf(c1));
      i += 1;
    }
    return c.a.m.a((Iterable)localCollection, (CharSequence)"", null, null, 0, null, null, 62);
  }
  
  private final void a(List<String> paramList)
  {
    com.truecaller.i.c localc = b;
    if (paramList != null)
    {
      Object localObject1 = (Iterable)paramList;
      paramList = (Collection)new ArrayList(c.a.m.a((Iterable)localObject1, 10));
      int i = 0;
      localObject1 = ((Iterable)localObject1).iterator();
      while (((Iterator)localObject1).hasNext())
      {
        Object localObject2 = ((Iterator)localObject1).next();
        if (i < 0) {
          c.a.m.a();
        }
        localObject2 = (String)localObject2;
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append(i);
        localStringBuilder.append(a);
        localStringBuilder.append((String)localObject2);
        paramList.add(localStringBuilder.toString());
        i += 1;
      }
      paramList = c.a.m.i((Iterable)paramList);
    }
    else
    {
      paramList = null;
    }
    localc.a("pinnedSuggestions", paramList);
  }
  
  private final void a(Set<String> paramSet)
  {
    b.a("hiddenSuggestions", paramSet);
  }
  
  private final Set<String> c()
  {
    Set localSet = b.f("hiddenSuggestions");
    k.a(localSet, "callingSettings.getStrin…tings.HIDDEN_SUGGESTIONS)");
    return localSet;
  }
  
  private final List<String> d()
  {
    Object localObject1 = b.f("pinnedSuggestions");
    k.a(localObject1, "callingSettings.getStrin…tings.PINNED_SUGGESTIONS)");
    Object localObject2 = (Iterable)localObject1;
    localObject1 = (Collection)new ArrayList(c.a.m.a((Iterable)localObject2, 10));
    localObject2 = ((Iterable)localObject2).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject3 = (String)((Iterator)localObject2).next();
      k.a(localObject3, "it");
      ((Collection)localObject1).add(c.n.m.b((CharSequence)localObject3, new String[] { a }, true, 2));
    }
    localObject1 = (Iterable)localObject1;
    Object localObject3 = (Collection)new ArrayList();
    Iterator localIterator = ((Iterable)localObject1).iterator();
    while (localIterator.hasNext())
    {
      List localList = (List)localIterator.next();
      int i = localList.size();
      localObject1 = null;
      if (i == 2) {}
      try
      {
        localObject2 = new n(Integer.valueOf(Integer.parseInt((String)localList.get(0))), localList.get(1));
        localObject1 = localObject2;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        for (;;) {}
      }
      localObject2 = new StringBuilder("Cannot parse prefix ");
      ((StringBuilder)localObject2).append((String)localList.get(0));
      AssertionUtil.reportWeirdnessButNeverCrash(((StringBuilder)localObject2).toString());
      break label280;
      localObject2 = new StringBuilder("Cannot proceed prefixed string ");
      ((StringBuilder)localObject2).append(a((String)localList.get(0)));
      AssertionUtil.reportWeirdnessButNeverCrash(((StringBuilder)localObject2).toString());
      label280:
      if (localObject1 != null) {
        ((Collection)localObject3).add(localObject1);
      }
    }
    localObject2 = (Iterable)c.a.m.a((Iterable)localObject3, (Comparator)new a());
    localObject1 = (Collection)new ArrayList(c.a.m.a((Iterable)localObject2, 10));
    localObject2 = ((Iterable)localObject2).iterator();
    while (((Iterator)localObject2).hasNext()) {
      ((Collection)localObject1).add((String)nextb);
    }
    return (List)localObject1;
  }
  
  public final List<e> a(int paramInt)
  {
    Object localObject2 = (Iterable)c();
    Object localObject1 = (Collection)new ArrayList(c.a.m.a((Iterable)localObject2, 10));
    localObject2 = ((Iterable)localObject2).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject3 = (String)((Iterator)localObject2).next();
      k.a(localObject3, "it");
      ((Collection)localObject1).add(new e((String)localObject3, null, false));
    }
    localObject1 = (List)localObject1;
    localObject2 = c;
    int i;
    if (paramInt > 0) {
      i = ((List)localObject1).size() + paramInt;
    } else {
      i = 0;
    }
    localObject2 = (Iterable)((g)localObject2).a(i);
    Object localObject3 = ag.a((Iterable)localObject2);
    Object localObject5 = (Iterable)d();
    Object localObject4 = (Collection)new ArrayList(c.a.m.a((Iterable)localObject5, 10));
    localObject5 = ((Iterable)localObject5).iterator();
    while (((Iterator)localObject5).hasNext())
    {
      String str = (String)((Iterator)localObject5).next();
      ((Collection)localObject4).add(new e(str, (Contact)((Map)localObject3).get(str), true));
    }
    localObject3 = (List)localObject4;
    localObject4 = (Collection)new ArrayList(c.a.m.a((Iterable)localObject2, 10));
    localObject2 = ((Iterable)localObject2).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject5 = (n)((Iterator)localObject2).next();
      ((Collection)localObject4).add(new e((String)a, (Contact)b, false));
    }
    localObject2 = (List)localObject4;
    localObject2 = c.a.m.k((Iterable)c.a.m.c((Iterable)c.a.m.c((Collection)localObject3, (Iterable)localObject2), (Iterable)localObject1));
    localObject1 = localObject2;
    if (paramInt > 0) {
      localObject1 = c.a.m.d((Iterable)localObject2, paramInt);
    }
    localObject4 = ((Iterable)localObject1).iterator();
    while (((Iterator)localObject4).hasNext())
    {
      localObject5 = (e)((Iterator)localObject4).next();
      localObject3 = b;
      localObject2 = localObject3;
      if (localObject3 == null) {
        localObject2 = d.b(a);
      }
      b = ((Contact)localObject2);
    }
    return (List<e>)localObject1;
  }
  
  public final void a()
  {
    a(null);
  }
  
  public final void a(e parame)
  {
    k.b(parame, "suggestedContact");
    Set localSet = c();
    parame = a;
    k.b(localSet, "receiver$0");
    LinkedHashSet localLinkedHashSet = new LinkedHashSet(ag.a(localSet.size() + 1));
    localLinkedHashSet.addAll((Collection)localSet);
    localLinkedHashSet.add(parame);
    a((Set)localLinkedHashSet);
  }
  
  public final void b(e parame)
  {
    k.b(parame, "suggestedContact");
    Object localObject1 = c();
    parame = a;
    k.b(localObject1, "receiver$0");
    LinkedHashSet localLinkedHashSet = new LinkedHashSet(ag.a(((Set)localObject1).size()));
    localObject1 = ((Iterable)localObject1).iterator();
    int j = 0;
    while (((Iterator)localObject1).hasNext())
    {
      Object localObject2 = ((Iterator)localObject1).next();
      int m = 1;
      int k = j;
      int i = m;
      if (j == 0)
      {
        k = j;
        i = m;
        if (k.a(localObject2, parame))
        {
          k = 1;
          i = 0;
        }
      }
      j = k;
      if (i != 0)
      {
        ((Collection)localLinkedHashSet).add(localObject2);
        j = k;
      }
    }
    a((Set)localLinkedHashSet);
  }
  
  public final boolean b()
  {
    return !((Collection)c()).isEmpty();
  }
  
  public final void c(e parame)
  {
    k.b(parame, "suggestedContact");
    a(c.a.m.k((Iterable)c.a.m.a((Collection)d(), a)));
  }
  
  public final void d(e parame)
  {
    k.b(parame, "suggestedContact");
    Object localObject1 = (Iterable)d();
    parame = a;
    k.b(localObject1, "receiver$0");
    ArrayList localArrayList = new ArrayList(c.a.m.a((Iterable)localObject1, 10));
    localObject1 = ((Iterable)localObject1).iterator();
    int j = 0;
    while (((Iterator)localObject1).hasNext())
    {
      Object localObject2 = ((Iterator)localObject1).next();
      int m = 1;
      int k = j;
      int i = m;
      if (j == 0)
      {
        k = j;
        i = m;
        if (k.a(localObject2, parame))
        {
          k = 1;
          i = 0;
        }
      }
      j = k;
      if (i != 0)
      {
        ((Collection)localArrayList).add(localObject2);
        j = k;
      }
    }
    a((List)localArrayList);
  }
  
  public static final class a<T>
    implements Comparator<T>
  {
    public final int compare(T paramT1, T paramT2)
    {
      return a.a((Comparable)Integer.valueOf(((Number)a).intValue()), (Comparable)Integer.valueOf(((Number)a).intValue()));
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.suggested_contacts.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */