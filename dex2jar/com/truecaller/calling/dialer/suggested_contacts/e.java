package com.truecaller.calling.dialer.suggested_contacts;

import c.g.b.k;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.g;
import java.util.Iterator;

public final class e
{
  public final String a;
  public Contact b;
  public final boolean c;
  
  public e(String paramString, Contact paramContact, boolean paramBoolean)
  {
    a = paramString;
    b = paramContact;
    c = paramBoolean;
  }
  
  public final Number a(g paramg)
  {
    k.b(paramg, "numberProvider");
    Object localObject1 = b;
    if (localObject1 != null)
    {
      localObject1 = ((Contact)localObject1).A();
      if (localObject1 != null)
      {
        Object localObject2 = ((Iterable)localObject1).iterator();
        while (((Iterator)localObject2).hasNext())
        {
          localObject1 = ((Iterator)localObject2).next();
          Number localNumber = (Number)localObject1;
          k.a(localNumber, "it");
          if (k.a(localNumber.a(), a)) {
            break label83;
          }
        }
        localObject1 = null;
        label83:
        localObject2 = (Number)localObject1;
        localObject1 = localObject2;
        if (localObject2 != null) {
          return localObject1;
        }
      }
    }
    localObject1 = paramg.b(new String[] { a });
    k.a(localObject1, "numberProvider.provideFr…berSafe(normalizedNumber)");
    return (Number)localObject1;
  }
  
  public final boolean equals(Object paramObject)
  {
    return ((paramObject instanceof e)) && (k.a(a, a));
  }
  
  public final int hashCode()
  {
    return a.hashCode();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("SuggestedContact(normalizedNumber=");
    localStringBuilder.append(a);
    localStringBuilder.append(", contact=");
    localStringBuilder.append(b);
    localStringBuilder.append(", isPinned=");
    localStringBuilder.append(c);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.suggested_contacts.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */