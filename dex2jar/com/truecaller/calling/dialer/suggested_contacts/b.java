package com.truecaller.calling.dialer.suggested_contacts;

import com.truecaller.androidactors.f;
import dagger.a.d;
import javax.inject.Provider;

public final class b
  implements d<a>
{
  private final Provider<f<com.truecaller.callhistory.a>> a;
  
  private b(Provider<f<com.truecaller.callhistory.a>> paramProvider)
  {
    a = paramProvider;
  }
  
  public static b a(Provider<f<com.truecaller.callhistory.a>> paramProvider)
  {
    return new b(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.suggested_contacts.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */