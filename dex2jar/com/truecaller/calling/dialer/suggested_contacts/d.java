package com.truecaller.calling.dialer.suggested_contacts;

import javax.inject.Provider;

public final class d
  implements dagger.a.d<c>
{
  private final Provider<com.truecaller.i.c> a;
  private final Provider<g> b;
  private final Provider<com.truecaller.data.access.c> c;
  
  private d(Provider<com.truecaller.i.c> paramProvider, Provider<g> paramProvider1, Provider<com.truecaller.data.access.c> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static d a(Provider<com.truecaller.i.c> paramProvider, Provider<g> paramProvider1, Provider<com.truecaller.data.access.c> paramProvider2)
  {
    return new d(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.suggested_contacts.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */