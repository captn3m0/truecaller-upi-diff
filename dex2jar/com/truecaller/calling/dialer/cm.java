package com.truecaller.calling.dialer;

import com.truecaller.common.h.aj;
import com.truecaller.data.entity.g;
import com.truecaller.network.search.e;
import com.truecaller.search.local.model.c;
import com.truecaller.utils.n;
import dagger.a.d;
import javax.inject.Provider;

public final class cm
  implements d<cl>
{
  private final Provider<ck.a> a;
  private final Provider<c> b;
  private final Provider<n> c;
  private final Provider<e> d;
  private final Provider<cf.b.a> e;
  private final Provider<ax> f;
  private final Provider<g> g;
  private final Provider<aj> h;
  
  private cm(Provider<ck.a> paramProvider, Provider<c> paramProvider1, Provider<n> paramProvider2, Provider<e> paramProvider3, Provider<cf.b.a> paramProvider4, Provider<ax> paramProvider5, Provider<g> paramProvider6, Provider<aj> paramProvider7)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
  }
  
  public static cm a(Provider<ck.a> paramProvider, Provider<c> paramProvider1, Provider<n> paramProvider2, Provider<e> paramProvider3, Provider<cf.b.a> paramProvider4, Provider<ax> paramProvider5, Provider<g> paramProvider6, Provider<aj> paramProvider7)
  {
    return new cm(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.cm
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */