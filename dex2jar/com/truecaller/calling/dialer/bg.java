package com.truecaller.calling.dialer;

import com.truecaller.f.a.i;
import com.truecaller.featuretoggles.e;
import dagger.a.d;
import java.util.Set;
import javax.inject.Provider;

public final class bg
  implements d<bf>
{
  private final Provider<be.b.b> a;
  private final Provider<e> b;
  private final Provider<Set<i>> c;
  
  private bg(Provider<be.b.b> paramProvider, Provider<e> paramProvider1, Provider<Set<i>> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static bg a(Provider<be.b.b> paramProvider, Provider<e> paramProvider1, Provider<Set<i>> paramProvider2)
  {
    return new bg(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bg
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */