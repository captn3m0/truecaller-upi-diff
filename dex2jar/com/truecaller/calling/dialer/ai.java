package com.truecaller.calling.dialer;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.AppBarLayout.c;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.RecyclerView.OnItemTouchListener;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;
import android.widget.Toast;
import c.a.ae;
import c.a.ag;
import c.k.i;
import c.x;
import com.truecaller.adapter_delegates.p;
import com.truecaller.adapter_delegates.s;
import com.truecaller.ads.b.j;
import com.truecaller.ads.b.w;
import com.truecaller.calling.ActionType;
import com.truecaller.calling.dialer.suggested_contacts.e;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.flashsdk.R.string;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.premium.br;
import com.truecaller.ui.aa;
import com.truecaller.ui.aa.a;
import com.truecaller.ui.details.a.a;
import com.truecaller.ui.g.c;
import com.truecaller.ui.q;
import com.truecaller.ui.z;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class ai
  implements ae.c, kotlinx.a.a.a
{
  private final ViewTreeObserver.OnGlobalLayoutListener A;
  private final z B;
  private final aa C;
  private final ae.c.b D;
  private final View E;
  private final c.g.a.a<View> F;
  private final n.a G;
  private final cf.a H;
  private final be.a I;
  private final aj.b J;
  private final ay.a K;
  private final com.truecaller.common.ui.b.a L;
  private final br M;
  private HashMap N;
  private final View b;
  private final Object c;
  private final c.f d;
  private final c.f e;
  private final c.f f;
  private final c.f g;
  private final c.f h;
  private final c.f i;
  private final c.f j;
  private final int k;
  private final int l;
  private Toast m;
  private com.truecaller.flashsdk.ui.b n;
  private boolean o;
  private boolean p;
  private final p<bn.b, bq> q;
  private final p<bw.b, bv> r;
  private final p<cf.b, ci> s;
  private final com.truecaller.adapter_delegates.m t;
  private final com.truecaller.adapter_delegates.m u;
  private final p<bh, bh> v;
  private final com.truecaller.adapter_delegates.g w;
  private final com.truecaller.adapter_delegates.f x;
  private final com.truecaller.adapter_delegates.f y;
  private final h z;
  
  public ai(ae.c.b paramb, com.truecaller.common.ui.a parama, View paramView, c.g.a.a<? extends View> parama1, n.a parama2, cf.a parama3, be.a parama4, ck.b paramb1, final cn.b paramb2, w paramw, aj.b paramb3, bn.a parama5, bw.a parama6, ay.a parama7, com.truecaller.common.ui.b.a parama8, br parambr)
  {
    D = paramb;
    E = paramView;
    F = parama1;
    G = parama2;
    H = parama3;
    I = parama4;
    J = paramb3;
    K = parama7;
    L = parama8;
    M = parambr;
    b = E;
    c = new Object();
    d = com.truecaller.utils.extensions.t.a(E, 2131362833);
    e = com.truecaller.utils.extensions.t.a(E, 2131363459);
    f = com.truecaller.utils.extensions.t.a(E, 2131363475);
    g = com.truecaller.utils.extensions.t.a(E, 2131362376);
    h = com.truecaller.utils.extensions.t.a(E, 2131362378);
    i = com.truecaller.utils.extensions.t.a(E, 2131362377);
    j = com.truecaller.utils.extensions.t.a(E, 2131362375);
    paramb = E.getContext();
    c.g.b.k.a(paramb, "view.context");
    k = paramb.getResources().getDimensionPixelSize(2131165482);
    paramb = E.getContext();
    c.g.b.k.a(paramb, "view.context");
    l = paramb.getResources().getDimensionPixelSize(2131165497);
    q = new p((com.truecaller.adapter_delegates.b)parama5, 2131559016, (c.g.a.b)new i(this), (c.g.a.b)j.a);
    r = new p((com.truecaller.adapter_delegates.b)parama6, 2131559005, (c.g.a.b)new k(this), (c.g.a.b)l.a);
    s = new p((com.truecaller.adapter_delegates.b)H, 2131559019, (c.g.a.b)new z(paramb1, paramb2), (c.g.a.b)aa.a);
    t = j.a(paramw);
    u = new com.truecaller.adapter_delegates.m(new com.truecaller.adapter_delegates.l[] { new com.truecaller.adapter_delegates.l((com.truecaller.adapter_delegates.n)K, 2131365501, (c.g.a.b)new b(this)), new com.truecaller.adapter_delegates.l((com.truecaller.adapter_delegates.n)G, 2131365483, (c.g.a.b)new c(this)) });
    v = new p((com.truecaller.adapter_delegates.b)I, 2131559006, (c.g.a.b)new f(this), (c.g.a.b)g.a);
    w = new com.truecaller.adapter_delegates.g((byte)0);
    paramb = new com.truecaller.adapter_delegates.f((com.truecaller.adapter_delegates.a)a((com.truecaller.adapter_delegates.a)u).a((com.truecaller.adapter_delegates.a)s, (s)w).a((com.truecaller.adapter_delegates.a)v, (s)w));
    paramb.setHasStableIds(true);
    x = paramb;
    paramb = new com.truecaller.adapter_delegates.f((com.truecaller.adapter_delegates.a)q.a((com.truecaller.adapter_delegates.a)a((com.truecaller.adapter_delegates.a)r), (s)w));
    paramb.setHasStableIds(true);
    y = paramb;
    z = new h(this);
    A = ((ViewTreeObserver.OnGlobalLayoutListener)new d(this));
    B = new z((com.truecaller.adapter_delegates.k)x);
    C = new aa(E.getContext(), ag.a(new c.n[] { c.t.a(ActionType.WHATSAPP_CALL, Integer.valueOf(2131234681)), c.t.a(ActionType.WHATSAPP_VIDEO_CALL, Integer.valueOf(2131234681)) }), (aa.a)B);
    paramb = p();
    paramView = x;
    paramView.a(true);
    paramb.setAdapter((RecyclerView.Adapter)paramView);
    paramb.setLayoutManager((RecyclerView.LayoutManager)new LinearLayoutManager(E.getContext()));
    paramb.addOnScrollListener((RecyclerView.OnScrollListener)z);
    paramb.addOnItemTouchListener((RecyclerView.OnItemTouchListener)C);
    paramb.addItemDecoration((RecyclerView.ItemDecoration)C);
    paramb.setItemAnimator(null);
    paramb.addItemDecoration((RecyclerView.ItemDecoration)new q(E.getContext(), 2131559205, 0));
    paramb = q();
    paramView = paramb.getNavigationIcon();
    if (paramView != null)
    {
      paramView = paramView.mutate();
      if (paramView != null)
      {
        paramView.setColorFilter(com.truecaller.utils.ui.b.a(paramb.getContext(), 2130969592), PorterDuff.Mode.SRC_IN);
        paramb.setNavigationIcon(paramView);
      }
    }
    paramb.setNavigationOnClickListener((View.OnClickListener)new a(this));
    com.truecaller.utils.extensions.t.b((View)paramb);
    ((FloatingActionButton)e(com.truecaller.R.id.fab)).setOnClickListener((View.OnClickListener)new View.OnClickListener()
    {
      public final void onClick(View paramAnonymousView)
      {
        ai.a(a).B();
      }
    });
    parama.a((AppBarLayout.c)new AppBarLayout.c()
    {
      public final void a(AppBarLayout paramAnonymousAppBarLayout, int paramAnonymousInt)
      {
        float f = -paramAnonymousInt - (ai.p(a).getResources().getDimension(2131166181) + ai.p(a).getResources().getDimension(2131165496) * 2.0F);
        paramAnonymousAppBarLayout = (FloatingActionButton)a.e(com.truecaller.R.id.fab);
        c.g.b.k.a(paramAnonymousAppBarLayout, "fab");
        paramAnonymousAppBarLayout.setTranslationY(f);
        paramAnonymousAppBarLayout = ai.n(a);
        c.g.b.k.a(paramAnonymousAppBarLayout, "dialpadView");
        paramAnonymousAppBarLayout.setTranslationY(f);
      }
    });
  }
  
  private final com.truecaller.adapter_delegates.t a(com.truecaller.adapter_delegates.a parama)
  {
    return parama.a((com.truecaller.adapter_delegates.a)t, (s)new com.truecaller.adapter_delegates.o(2, 12, 4));
  }
  
  private final void a(int paramInt1, int paramInt2, c.g.a.a<x> parama)
  {
    new AlertDialog.Builder(E.getContext()).setTitle(paramInt1).setMessage(paramInt2).setPositiveButton(2131887217, (DialogInterface.OnClickListener)new u(parama)).setNegativeButton(2131887197, null).show();
  }
  
  private final RecyclerView p()
  {
    return (RecyclerView)d.b();
  }
  
  private final Toolbar q()
  {
    return (Toolbar)e.b();
  }
  
  private final View r()
  {
    return (View)g.b();
  }
  
  private final Button s()
  {
    return (Button)j.b();
  }
  
  private final void t()
  {
    RecyclerView localRecyclerView = p();
    c.g.b.k.a(localRecyclerView, "recycler");
    localRecyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(A);
  }
  
  public final View a()
  {
    return b;
  }
  
  public final void a(int paramInt)
  {
    new AlertDialog.Builder(E.getContext()).setMessage(paramInt).setPositiveButton(2131886289, (DialogInterface.OnClickListener)new v(this)).setNegativeButton(2131887197, null).show();
  }
  
  public final void a(View paramView)
  {
    c.g.b.k.b(paramView, "anchorView");
    paramView = new PopupMenu(E.getContext(), paramView);
    paramView.getMenuInflater().inflate(2131623977, paramView.getMenu());
    paramView.setOnMenuItemClickListener((PopupMenu.OnMenuItemClickListener)new y(this));
    paramView.show();
  }
  
  public final void a(View paramView, final e parame, String paramString)
  {
    c.g.b.k.b(paramView, "anchorView");
    c.g.b.k.b(parame, "suggestedContact");
    c.g.b.k.b(paramString, "displayName");
    Context localContext = E.getContext();
    PopupMenu localPopupMenu = new PopupMenu(localContext, paramView);
    localPopupMenu.getMenuInflater().inflate(2131623976, localPopupMenu.getMenu());
    paramView = localPopupMenu.getMenu().findItem(2131361870);
    c.g.b.k.a(paramView, "menuItemHide");
    paramView.setTitle((CharSequence)localContext.getString(2131887248, new Object[] { paramString }));
    paramView.setVisible(c ^ true);
    MenuItem localMenuItem = localPopupMenu.getMenu().findItem(2131361847);
    c.g.b.k.a(localMenuItem, "popup.menu.findItem(R.id…ion_change_pinning_state)");
    if (c) {
      paramView = (CharSequence)localContext.getString(2131887253, new Object[] { paramString });
    } else {
      paramView = (CharSequence)localContext.getString(2131887249, new Object[] { paramString });
    }
    localMenuItem.setTitle(paramView);
    localPopupMenu.setOnMenuItemClickListener((PopupMenu.OnMenuItemClickListener)new x(this, parame));
    localPopupMenu.show();
  }
  
  public final void a(aq paramaq)
  {
    if (paramaq == null)
    {
      x.a(false);
      paramaq = r();
      c.g.b.k.a(paramaq, "emptyView");
      com.truecaller.utils.extensions.t.a(paramaq, false);
      return;
    }
    x.a(true);
    Object localObject = r();
    c.g.b.k.a(localObject, "emptyView");
    com.truecaller.utils.extensions.t.a((View)localObject, true);
    ((TextView)h.b()).setText(a);
    s().setText(b);
    localObject = (TextView)i.b();
    c.g.b.k.a(localObject, "emptyViewText");
    com.truecaller.utils.extensions.t.a((View)localObject, c);
    s().setOnClickListener((View.OnClickListener)new m(this));
  }
  
  public final void a(final e parame)
  {
    c.g.b.k.b(parame, "suggestedContact");
    Snackbar.a((View)p(), 2131887247, 0).a(2131886464, (View.OnClickListener)new e(this, parame)).c();
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "message");
    new AlertDialog.Builder(E.getContext()).setMessage((CharSequence)paramString).setPositiveButton(2131886772, (DialogInterface.OnClickListener)new n(this)).setNegativeButton(2131887197, null).show();
  }
  
  public final void a(String paramString, PremiumPresenterView.LaunchContext paramLaunchContext)
  {
    c.g.b.k.b(paramString, "page");
    c.g.b.k.b(paramLaunchContext, "launchContext");
    Context localContext = E.getContext();
    c.g.b.k.a(localContext, "view.context");
    br.a(localContext, paramLaunchContext, paramString);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    c.g.b.k.b(paramString1, "number");
    c.g.b.k.b(paramString2, "name");
    new com.truecaller.ui.dialogs.o(E.getContext(), paramString2, paramString1, paramString3).show();
  }
  
  public final void a(String paramString, boolean paramBoolean)
  {
    paramString = new com.truecaller.ui.details.a(E.getContext(), paramString, paramBoolean, true);
    paramString.a((a.a)new o(this));
    paramString.show();
  }
  
  public final void a(List<com.truecaller.calling.dialer.a.a> paramList1, List<com.truecaller.calling.dialer.a.a> paramList2)
  {
    c.g.b.k.b(paramList1, "oldItems");
    c.g.b.k.b(paramList2, "newItems");
    H.a(paramList1, paramList2);
  }
  
  public final void a(Set<Integer> paramSet)
  {
    c.g.b.k.b(paramSet, "itemPositions");
    paramSet = ((Iterable)paramSet).iterator();
    while (paramSet.hasNext())
    {
      int i1 = ((Number)paramSet.next()).intValue();
      x.notifyItemChanged(u.a_(i1));
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    p = paramBoolean;
    if (paramBoolean)
    {
      RecyclerView localRecyclerView = p();
      c.g.b.k.a(localRecyclerView, "recycler");
      localRecyclerView.getViewTreeObserver().addOnGlobalLayoutListener(A);
    }
  }
  
  public final void b()
  {
    x.notifyDataSetChanged();
  }
  
  public final void b(int paramInt)
  {
    Object localObject2 = (Iterable)i.b(paramInt, paramInt + 1);
    Object localObject1 = (Collection)new ArrayList(c.a.m.a((Iterable)localObject2, 10));
    localObject2 = ((Iterable)localObject2).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      paramInt = ((ae)localObject2).a();
      ((Collection)localObject1).add(Integer.valueOf(r.a_(paramInt)));
    }
    localObject1 = ((Iterable)localObject1).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      paramInt = ((Number)((Iterator)localObject1).next()).intValue();
      y.notifyItemChanged(paramInt);
    }
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "dialogMessage");
    new AlertDialog.Builder(E.getContext()).setMessage((CharSequence)paramString).setNegativeButton(2131887216, (DialogInterface.OnClickListener)new p(this)).setPositiveButton(2131886772, (DialogInterface.OnClickListener)new q(this)).show();
  }
  
  public final void b(Set<Integer> paramSet)
  {
    c.g.b.k.b(paramSet, "itemPositions");
    H.a(paramSet);
  }
  
  public final void b(boolean paramBoolean)
  {
    s.a = (paramBoolean ^ true);
    x.notifyDataSetChanged();
  }
  
  public final void c()
  {
    H.a();
  }
  
  public final void c(int paramInt)
  {
    Toolbar localToolbar = q();
    c.g.b.k.a(localToolbar, "toolbar");
    localToolbar.setTitle((CharSequence)E.getContext().getString(paramInt));
  }
  
  public final void c(String paramString)
  {
    c.g.b.k.b(paramString, "message");
    Toast localToast = m;
    if (localToast != null) {
      localToast.cancel();
    }
    m = Toast.makeText(E.getContext(), (CharSequence)paramString, 0);
    paramString = m;
    if (paramString != null)
    {
      paramString.show();
      return;
    }
  }
  
  public final void c(Set<Integer> paramSet)
  {
    c.g.b.k.b(paramSet, "adsPositions");
    paramSet = ((Iterable)paramSet).iterator();
    while (paramSet.hasNext())
    {
      int i1 = ((Number)paramSet.next()).intValue();
      i1 = t.a_(i1);
      com.truecaller.adapter_delegates.f localf = x;
      localf.notifyItemRangeChanged(i1, localf.getItemCount() - i1);
    }
  }
  
  public final void c(boolean paramBoolean)
  {
    G.b(paramBoolean);
    K.b(paramBoolean);
    x.notifyDataSetChanged();
  }
  
  public final void d()
  {
    com.truecaller.ui.g localg = new com.truecaller.ui.g(E.getContext(), (View)F.invoke());
    localg.a((g.c)new w(this));
    localg.a();
  }
  
  public final void d(int paramInt)
  {
    ((FloatingActionButton)e(com.truecaller.R.id.fab)).setImageResource(paramInt);
  }
  
  public final void d(boolean paramBoolean)
  {
    Object localObject1;
    if (paramBoolean) {
      localObject1 = y;
    } else {
      localObject1 = x;
    }
    Object localObject2 = p();
    c.g.b.k.a(localObject2, "recycler");
    if ((c.g.b.k.a(((RecyclerView)localObject2).getAdapter(), localObject1) ^ true))
    {
      localObject2 = p();
      c.g.b.k.a(localObject2, "recycler");
      ((RecyclerView)localObject2).setAdapter((RecyclerView.Adapter)localObject1);
      localObject2 = B;
      localObject1 = (com.truecaller.adapter_delegates.k)localObject1;
      c.g.b.k.b(localObject1, "<set-?>");
      a = ((com.truecaller.adapter_delegates.k)localObject1);
    }
  }
  
  public final View e(int paramInt)
  {
    if (N == null) {
      N = new HashMap();
    }
    View localView2 = (View)N.get(Integer.valueOf(paramInt));
    View localView1 = localView2;
    if (localView2 == null)
    {
      localView1 = a();
      if (localView1 == null) {
        return null;
      }
      localView1 = localView1.findViewById(paramInt);
      N.put(Integer.valueOf(paramInt), localView1);
    }
    return localView1;
  }
  
  public final void e()
  {
    new AlertDialog.Builder(E.getContext()).setMessage(2131887245).setNegativeButton(2131887197, null).setPositiveButton(2131887244, (DialogInterface.OnClickListener)new r(this)).show();
  }
  
  public final void e(boolean paramBoolean)
  {
    q.a = (paramBoolean ^ true);
    y.notifyDataSetChanged();
  }
  
  public final void f()
  {
    a(2131888341, 2131888354, (c.g.a.a)new s(this));
  }
  
  public final void f(boolean paramBoolean)
  {
    Toolbar localToolbar = q();
    c.g.b.k.a(localToolbar, "toolbar");
    com.truecaller.utils.extensions.t.a((View)localToolbar, paramBoolean);
  }
  
  public final void g()
  {
    a(2131888342, 2131888355, (c.g.a.a)new t(this));
  }
  
  public final void g(boolean paramBoolean)
  {
    FloatingActionButton localFloatingActionButton = (FloatingActionButton)e(com.truecaller.R.id.fab);
    c.g.b.k.a(localFloatingActionButton, "fab");
    com.truecaller.utils.extensions.t.a((View)localFloatingActionButton, paramBoolean);
  }
  
  public final void h()
  {
    J.b();
  }
  
  public final void h(boolean paramBoolean)
  {
    v.a = (paramBoolean ^ true);
    x.notifyDataSetChanged();
    k();
  }
  
  public final void i()
  {
    o = true;
  }
  
  public final void j()
  {
    J.d();
  }
  
  public final void k()
  {
    p().scrollToPosition(0);
  }
  
  public final void l()
  {
    Toast.makeText(E.getContext(), 2131888358, 0).show();
  }
  
  public final void m()
  {
    com.truecaller.common.ui.b.a locala = L;
    if (locala != null)
    {
      locala.ag_();
      return;
    }
  }
  
  public final void n()
  {
    t();
  }
  
  public final void o()
  {
    x.notifyDataSetChanged();
    k();
  }
  
  static final class a
    implements View.OnClickListener
  {
    a(ai paramai) {}
    
    public final void onClick(View paramView)
    {
      ai.a(a).z();
    }
  }
  
  static final class aa
    extends c.g.b.l
    implements c.g.a.b<ci, ci>
  {
    public static final aa a = new aa();
    
    aa()
    {
      super();
    }
  }
  
  static final class b
    extends c.g.b.l
    implements c.g.a.b<ViewGroup, bb>
  {
    b(ai paramai)
    {
      super();
    }
  }
  
  static final class c
    extends c.g.b.l
    implements c.g.a.b<ViewGroup, m>
  {
    c(ai paramai)
    {
      super();
    }
  }
  
  static final class d
    implements ViewTreeObserver.OnGlobalLayoutListener
  {
    d(ai paramai) {}
    
    public final void onGlobalLayout()
    {
      if ((ai.g(a)) && (!ai.i(a)))
      {
        if (ai.j(a) != null) {
          return;
        }
        Object localObject1 = ai.k(a);
        c.g.b.k.a(localObject1, "recycler");
        localObject1 = ((RecyclerView)localObject1).getAdapter();
        int i;
        if (localObject1 != null) {
          i = ((RecyclerView.Adapter)localObject1).getItemCount();
        } else {
          i = 0;
        }
        int j = 0;
        while (j < i)
        {
          localObject1 = ai.a(a, j);
          if ((localObject1 != null) && (((View)localObject1).getVisibility() == 0))
          {
            Object localObject2 = new int[2];
            ((View)localObject1).getLocationOnScreen((int[])localObject2);
            int k = localObject2[1];
            int m = ai.l(a);
            if (localObject2[1] > ai.m(a))
            {
              float f = k + m;
              localObject2 = ai.n(a);
              c.g.b.k.a(localObject2, "dialpadView");
              if (f < ((ViewGroup)localObject2).getY())
              {
                localObject2 = ((View)localObject1).getContext();
                c.g.b.k.a(localObject2, "flashButton.context");
                localObject1 = new com.truecaller.flashsdk.ui.b((Context)localObject2, (View)localObject1);
                if (b == null)
                {
                  b = new PopupWindow(a, -1, -2, true);
                  localObject2 = a.findViewById(com.truecaller.flashsdk.R.id.text);
                  c.g.b.k.a(localObject2, "contentView.findViewById(R.id.text)");
                  localObject2 = (TextView)localObject2;
                  Context localContext = a.getContext();
                  c.g.b.k.a(localContext, "contentView.context");
                  ((TextView)localObject2).setText(localContext.getResources().getText(R.string.flash_tooltip_text));
                  localObject2 = b;
                  if (localObject2 != null) {
                    ((PopupWindow)localObject2).setBackgroundDrawable((Drawable)new ColorDrawable());
                  }
                }
                c.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)localObject1);
                localObject2 = b;
                if (localObject2 != null) {
                  ((PopupWindow)localObject2).setOnDismissListener((PopupWindow.OnDismissListener)new com.truecaller.flashsdk.ui.b.a((com.truecaller.flashsdk.ui.b)localObject1));
                }
                localObject2 = b;
                if (localObject2 != null) {
                  ((PopupWindow)localObject2).showAsDropDown(c, 0, 0);
                }
                ai.a(a, (com.truecaller.flashsdk.ui.b)localObject1);
                ai.a(a).C();
                ai.h(a);
                ai.o(a);
                return;
              }
            }
          }
          j += 1;
        }
        return;
      }
    }
  }
  
  static final class e
    implements View.OnClickListener
  {
    e(ai paramai, e parame) {}
    
    public final void onClick(View paramView)
    {
      ai.a(a).c(parame);
    }
  }
  
  static final class f
    extends c.g.b.l
    implements c.g.a.b<View, bh>
  {
    f(ai paramai)
    {
      super();
    }
  }
  
  static final class g
    extends c.g.b.l
    implements c.g.a.b<bh, bh>
  {
    public static final g a = new g();
    
    g()
    {
      super();
    }
  }
  
  public static final class h
    extends RecyclerView.OnScrollListener
  {
    public final void onScrollStateChanged(RecyclerView paramRecyclerView, int paramInt)
    {
      c.g.b.k.b(paramRecyclerView, "recyclerView");
      ai.a(a).d(paramInt);
    }
  }
  
  static final class i
    extends c.g.b.l
    implements c.g.a.b<View, bq>
  {
    i(ai paramai)
    {
      super();
    }
  }
  
  static final class j
    extends c.g.b.l
    implements c.g.a.b<bq, bq>
  {
    public static final j a = new j();
    
    j()
    {
      super();
    }
  }
  
  static final class k
    extends c.g.b.l
    implements c.g.a.b<View, bv>
  {
    k(ai paramai)
    {
      super();
    }
  }
  
  static final class l
    extends c.g.b.l
    implements c.g.a.b<bv, bv>
  {
    public static final l a = new l();
    
    l()
    {
      super();
    }
  }
  
  static final class m
    implements View.OnClickListener
  {
    m(ai paramai) {}
    
    public final void onClick(View paramView)
    {
      ai.a(a).A();
    }
  }
  
  static final class n
    implements DialogInterface.OnClickListener
  {
    n(ai paramai) {}
    
    public final void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
      ai.a(a).a(null, TruecallerContract.Filters.EntityType.UNKNOWN);
    }
  }
  
  static final class o
    implements a.a
  {
    o(ai paramai) {}
    
    public final void onClick(String paramString, TruecallerContract.Filters.EntityType paramEntityType)
    {
      c.g.b.k.b(paramString, "betterName");
      c.g.b.k.b(paramEntityType, "entityType");
      ai.a(a).a(paramString, paramEntityType);
    }
  }
  
  static final class p
    implements DialogInterface.OnClickListener
  {
    p(ai paramai) {}
    
    public final void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
      ai.a(a).x();
    }
  }
  
  static final class q
    implements DialogInterface.OnClickListener
  {
    q(ai paramai) {}
    
    public final void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
      ai.a(a).w();
    }
  }
  
  static final class r
    implements DialogInterface.OnClickListener
  {
    r(ai paramai) {}
    
    public final void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
      ai.a(a).q();
    }
  }
  
  static final class s
    extends c.g.b.l
    implements c.g.a.a<x>
  {
    s(ai paramai)
    {
      super();
    }
  }
  
  static final class t
    extends c.g.b.l
    implements c.g.a.a<x>
  {
    t(ai paramai)
    {
      super();
    }
  }
  
  static final class u
    implements DialogInterface.OnClickListener
  {
    u(c.g.a.a parama) {}
    
    public final void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
      a.invoke();
    }
  }
  
  static final class v
    implements DialogInterface.OnClickListener
  {
    v(ai paramai) {}
    
    public final void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
      ai.a(a).r();
    }
  }
  
  public static final class w
    implements g.c
  {
    public final boolean a(long paramLong)
    {
      if (paramLong == 9L) {
        ai.a(a).p();
      } else if (paramLong == 10L) {
        ai.b(a).h();
      } else if ((paramLong != 3L) && (paramLong != 5L) && (paramLong != 6L) && (paramLong != 11L) && (paramLong != 4L))
      {
        if (paramLong == 7L) {
          ai.a(a).s();
        } else if (paramLong == 12L) {
          ai.a(a).u();
        } else {
          return false;
        }
      }
      else {
        ai.a(a).a(paramLong);
      }
      return true;
    }
    
    public final boolean a(long paramLong, boolean paramBoolean)
    {
      if (paramLong == 8L)
      {
        ai.a(a).a(paramBoolean);
        return true;
      }
      return false;
    }
  }
  
  static final class x
    implements PopupMenu.OnMenuItemClickListener
  {
    x(ai paramai, e parame) {}
    
    public final boolean onMenuItemClick(MenuItem paramMenuItem)
    {
      c.g.b.k.a(paramMenuItem, "item");
      int i = paramMenuItem.getItemId();
      if (i != 2131361847)
      {
        if (i != 2131361870) {
          return false;
        }
        ai.a(a).a(parame);
        return true;
      }
      ai.a(a).b(parame);
      return true;
    }
  }
  
  static final class y
    implements PopupMenu.OnMenuItemClickListener
  {
    y(ai paramai) {}
    
    public final boolean onMenuItemClick(MenuItem paramMenuItem)
    {
      c.g.b.k.a(paramMenuItem, "item");
      if (paramMenuItem.getItemId() != 2131361860) {
        return false;
      }
      ai.a(a).D();
      return true;
    }
  }
  
  static final class z
    extends c.g.b.l
    implements c.g.a.b<View, ci>
  {
    z(ck.b paramb, cn.b paramb1)
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.ai
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */