package com.truecaller.calling.dialer;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;

@c.d.b.a.f(b="DialerPresenter.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.dialer.DialerPresenter$cancelMissedCallsNotification$1")
final class af$d
  extends c.d.b.a.k
  implements m<ag, c<? super x>, Object>
{
  int a;
  private ag c;
  
  af$d(af paramaf, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new d(b, paramc);
    c = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    c.d.a.a locala = c.d.a.a.a;
    if (a == 0)
    {
      if (!(paramObject instanceof o.b))
      {
        ((com.truecaller.callhistory.a)b.x.a()).f();
        return x.a;
      }
      throw a;
    }
    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((d)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.af.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */