package com.truecaller.calling.dialer;

import c.g.b.k;

public abstract interface aj$a
{
  public abstract void a(int paramInt1, int paramInt2);
  
  public abstract void a(int paramInt1, int paramInt2, String paramString);
  
  public abstract void a(b paramb);
  
  public abstract void a(String paramString);
  
  public static abstract interface a
  {
    public abstract void c(String paramString);
  }
  
  public static abstract class b
  {
    public static final class a
      extends aj.a.b
    {
      public static final a a = new a();
      
      private a()
      {
        super();
      }
    }
    
    public static final class b
      extends aj.a.b
    {
      public static final b a = new b();
      
      private b()
      {
        super();
      }
    }
    
    public static final class c
      extends aj.a.b
    {
      final String a;
      
      public c(String paramString)
      {
        super();
        a = paramString;
      }
      
      public final boolean equals(Object paramObject)
      {
        if (this != paramObject) {
          if ((paramObject instanceof c))
          {
            paramObject = (c)paramObject;
            if (k.a(a, a)) {}
          }
          else
          {
            return false;
          }
        }
        return true;
      }
      
      public final int hashCode()
      {
        String str = a;
        if (str != null) {
          return str.hashCode();
        }
        return 0;
      }
      
      public final String toString()
      {
        StringBuilder localStringBuilder = new StringBuilder("TapToPaste(number=");
        localStringBuilder.append(a);
        localStringBuilder.append(")");
        return localStringBuilder.toString();
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.aj.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */