package com.truecaller.calling.dialer;

import c.g.b.k;
import c.n;
import c.n.m;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.calling.d.q;
import com.truecaller.common.account.r;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.g;
import com.truecaller.search.global.CompositeAdapterDelegate.SearchResultOrder;
import com.truecaller.ui.keyboard.DialerKeypad.KeyActionState;
import com.truecaller.ui.view.BottomBar.DialpadState;
import com.truecaller.util.al;
import java.util.HashSet;
import java.util.List;
import javax.inject.Inject;

public final class ak
  extends com.truecaller.bd<aj.c, aj.d>
  implements aj.b
{
  private boolean d;
  private String e;
  private int f;
  private int g;
  private String h;
  private final HashSet<Character> i;
  private final bm.a j;
  private final bd k;
  private final r l;
  private final com.truecaller.util.bz m;
  private final al n;
  private final com.truecaller.i.c o;
  private final q p;
  private final aj.a.a q;
  private final b r;
  private final g s;
  
  @Inject
  public ak(bd parambd, r paramr, com.truecaller.util.bz parambz, al paramal, com.truecaller.i.c paramc, q paramq, aj.a.a parama, b paramb, g paramg, bm.a parama1)
  {
    k = parambd;
    l = paramr;
    m = parambz;
    n = paramal;
    o = paramc;
    p = paramq;
    q = parama;
    r = paramb;
    s = paramg;
    d = true;
    e = "";
    f = -1;
    g = -1;
    i = new HashSet();
    j = parama1;
  }
  
  private final void a(String paramString1, String paramString2, String paramString3)
  {
    paramString1 = new e.a("ViewAction").a("Context", paramString1).a("Action", paramString2);
    if (paramString3 != null) {
      paramString1.a("SubAction", paramString3);
    }
    paramString2 = r;
    paramString1 = paramString1.a();
    k.a(paramString1, "eventBuilder.build()");
    paramString2.b(paramString1);
  }
  
  private final boolean a(char paramChar)
  {
    if ((paramChar != ',') && (paramChar != ';')) {
      return true;
    }
    int i2 = f;
    int i1 = i2;
    if (i2 == -1) {
      i1 = e.length();
    }
    if (i1 != 0)
    {
      if (((CharSequence)e).length() == 0) {
        i2 = 1;
      } else {
        i2 = 0;
      }
      if (i2 != 0) {
        return false;
      }
      if (paramChar == ',') {
        return true;
      }
      if (e.charAt(i1 - 1) == ';') {
        return false;
      }
      paramChar = g;
      if ((paramChar != '￿') && (paramChar < e.length())) {
        return e.charAt(g) != ';';
      }
      return true;
    }
    return false;
  }
  
  private final boolean a(int paramInt)
  {
    if (paramInt == 1)
    {
      k.v_();
      return true;
    }
    if (2 > paramInt) {
      return false;
    }
    if (9 >= paramInt)
    {
      String str = p.a(paramInt);
      if (str != null)
      {
        k.a(str, null, false, "dialpad");
        return true;
      }
      k.b_(paramInt);
      return true;
    }
    return false;
  }
  
  private final void b(char paramChar)
  {
    if (f == -1)
    {
      locald = (aj.d)b;
      if (locald != null) {
        locald.a(String.valueOf(paramChar));
      }
      return;
    }
    aj.d locald = (aj.d)b;
    if (locald != null)
    {
      locald.a(f, g, String.valueOf(paramChar));
      return;
    }
  }
  
  private final boolean k()
  {
    return ((CharSequence)e).length() > 0;
  }
  
  private final void l()
  {
    if (!d)
    {
      d = true;
      aj.d locald = (aj.d)b;
      if (locald != null) {
        locald.a(d);
      }
      n();
    }
  }
  
  private final void m()
  {
    i.clear();
    aj.d locald = (aj.d)b;
    if (locald != null)
    {
      locald.c("");
      return;
    }
  }
  
  private final void n()
  {
    Object localObject;
    if ((d) && (k())) {
      localObject = BottomBar.DialpadState.NUMBER_ENTERED;
    } else if (d) {
      localObject = BottomBar.DialpadState.DIALPAD_UP;
    } else {
      localObject = BottomBar.DialpadState.DIALPAD_DOWN;
    }
    aj.d locald = (aj.d)b;
    if (locald != null) {
      locald.a((BottomBar.DialpadState)localObject);
    }
    locald = (aj.d)b;
    if (locald != null)
    {
      if (k()) {}
      for (localObject = aj.a.b.b.a;; localObject = aj.a.b.a.a)
      {
        localObject = (aj.a.b)localObject;
        break;
        localObject = h;
        if (localObject != null)
        {
          localObject = (aj.a.b)new aj.a.b.c((String)localObject);
          break;
        }
      }
      locald.a((aj.a.b)localObject);
      return;
    }
  }
  
  public final void a()
  {
    int i1 = f;
    aj.d locald;
    if (i1 == -1)
    {
      i1 = e.length();
      if (i1 > 0)
      {
        locald = (aj.d)b;
        if (locald != null) {
          locald.a(i1 - 1, i1);
        }
      }
    }
    else if (g <= i1)
    {
      if (i1 != 0)
      {
        locald = (aj.d)b;
        if (locald != null)
        {
          i1 = f;
          locald.a(i1 - 1, i1);
        }
      }
    }
    else
    {
      locald = (aj.d)b;
      if (locald != null)
      {
        locald.a(f, g);
        return;
      }
    }
  }
  
  public final void a(char paramChar, DialerKeypad.KeyActionState paramKeyActionState)
  {
    k.b(paramKeyActionState, "keyState");
    switch (al.a[paramKeyActionState.ordinal()])
    {
    default: 
      
    case 3: 
      i.remove(Character.valueOf(paramChar));
      return;
    case 2: 
      if (i.remove(Character.valueOf(paramChar)))
      {
        b(paramChar);
        return;
      }
      break;
    case 1: 
      i.add(Character.valueOf(paramChar));
      return;
    }
  }
  
  public final void a(ap paramap)
  {
    k.b(paramap, "editable");
    String str = paramap.a();
    if (k.a(e, str)) {
      return;
    }
    e = str;
    h = null;
    n();
    com.truecaller.util.ca localca = m.a(str);
    if (localca != null)
    {
      paramap.b();
      paramap = (aj.d)b;
      if (paramap != null) {
        paramap.a(localca);
      }
      return;
    }
    q.c(str);
  }
  
  public final void a(String paramString)
  {
    k.a(paramString, null, true, CompositeAdapterDelegate.SearchResultOrder.ORDER_TCM);
  }
  
  public final boolean a(int paramInt1, int paramInt2)
  {
    boolean bool2 = true;
    boolean bool1;
    switch (paramInt1)
    {
    case -4715: 
    case -4713: 
    default: 
      bool1 = false;
      break;
    case -4712: 
      bool1 = a(paramInt2);
      break;
    case -4714: 
      b('+');
      bool1 = bool2;
      break;
    case -4716: 
      bool1 = bool2;
      if (a(';'))
      {
        b(';');
        bool1 = bool2;
      }
      break;
    case -4717: 
      bool1 = bool2;
      if (a(','))
      {
        b(',');
        bool1 = bool2;
      }
      break;
    }
    if (bool1) {
      i.clear();
    }
    return bool1;
  }
  
  public final void b()
  {
    if (d)
    {
      d = false;
      aj.d locald = (aj.d)b;
      if (locald != null) {
        locald.a(d);
      }
      n();
    }
  }
  
  public final void b(int paramInt1, int paramInt2)
  {
    f = paramInt1;
    g = paramInt2;
  }
  
  public final void b(String paramString)
  {
    CharSequence localCharSequence = (CharSequence)paramString;
    int i1;
    if ((localCharSequence != null) && (!m.a(localCharSequence))) {
      i1 = 0;
    } else {
      i1 = 1;
    }
    if (i1 != 0) {
      paramString = null;
    }
    h = paramString;
    n();
  }
  
  public final void c()
  {
    Object localObject1 = (aj.d)b;
    if (localObject1 != null) {
      ((aj.d)localObject1).b();
    }
    localObject1 = (aj.d)b;
    if (localObject1 != null) {
      ((aj.d)localObject1).d();
    }
    localObject1 = o.a("lastCopied");
    if (localObject1 != null)
    {
      int i1 = ((CharSequence)localObject1).length();
      int i3 = 1;
      if (i1 > 0) {
        i1 = 1;
      } else {
        i1 = 0;
      }
      if (i1 == 0) {
        localObject1 = null;
      }
      if (localObject1 != null)
      {
        String[] arrayOfString = am.a();
        int i4 = arrayOfString.length;
        i1 = 0;
        int i2;
        Object localObject2;
        for (;;)
        {
          i2 = i3;
          if (i1 >= i4) {
            break;
          }
          localObject2 = arrayOfString[i1];
          aj.c localc = (aj.c)c;
          if (localc != null) {
            localObject2 = Boolean.valueOf(localc.b((String)localObject2));
          } else {
            localObject2 = null;
          }
          if (com.truecaller.utils.extensions.c.a((Boolean)localObject2))
          {
            i2 = 0;
            break;
          }
          i1 += 1;
        }
        if (i2 == 0) {
          localObject1 = null;
        }
        if (localObject1 != null)
        {
          if (o.b("lastCopiedFromTc"))
          {
            b((String)localObject1);
          }
          else
          {
            localObject2 = (aj.d)b;
            if (localObject2 != null)
            {
              k.a(localObject1, "number");
              ((aj.d)localObject2).d((String)localObject1);
            }
          }
          o.d("lastCopied");
          o.d("lastCopiedFromTc");
          return;
        }
      }
    }
  }
  
  public final void c(String paramString)
  {
    aj.d locald = (aj.d)b;
    if (locald != null)
    {
      String str = paramString;
      if (paramString == null) {
        str = "";
      }
      locald.c(str);
    }
    l();
  }
  
  public final void d()
  {
    m();
  }
  
  public final void e()
  {
    aj.d locald = (aj.d)b;
    if (locald != null)
    {
      locald.c();
      return;
    }
  }
  
  public final boolean f()
  {
    if (d)
    {
      b();
      return true;
    }
    if (k())
    {
      m();
      n();
      return true;
    }
    return false;
  }
  
  public final void g()
  {
    if ((d) && (k()))
    {
      if (e.length() == 1)
      {
        int i1 = e.charAt(0);
        if ((49 <= i1) && (57 >= i1))
        {
          localObject = Integer.valueOf(String.valueOf(e.charAt(0)));
          k.a(localObject, "Integer.valueOf(\"${currentText[0]}\")");
          a(((Integer)localObject).intValue());
          break label107;
        }
      }
      k.a(e, null, false, "dialpad");
      a(this, "call");
      label107:
      m();
      return;
    }
    d ^= true;
    Object localObject = (aj.d)b;
    if (localObject != null) {
      ((aj.d)localObject).a(d);
    }
    n();
  }
  
  public final void h()
  {
    String str = n.d();
    if (str != null)
    {
      aj.d locald = (aj.d)b;
      if (locald != null)
      {
        k.a(str, "it");
        locald.c(str);
      }
    }
    l();
    a("callLog", "menu", "paste");
  }
  
  public final void i()
  {
    String str = h;
    if (str != null)
    {
      c(str);
      return;
    }
  }
  
  public final void j()
  {
    Object localObject2 = (bz)j.a((ca)this, a[0]).b;
    boolean bool = localObject2 instanceof bz.c;
    Object localObject3 = null;
    if (bool)
    {
      localObject1 = a;
    }
    else
    {
      localObject1 = localObject3;
      if ((localObject2 instanceof bz.a))
      {
        localObject2 = a;
        int i1;
        if (((List)localObject2).size() == 1) {
          i1 = 1;
        } else {
          i1 = 0;
        }
        if (i1 == 0) {
          localObject2 = null;
        }
        localObject1 = localObject3;
        if (localObject2 != null)
        {
          localObject2 = (av)((List)localObject2).get(0);
          localObject1 = localObject3;
          if (localObject2 != null)
          {
            localObject2 = a;
            localObject1 = localObject3;
            if (localObject2 != null)
            {
              localObject1 = localObject3;
              if (!((Contact)localObject2).Z()) {
                localObject1 = localObject2;
              }
            }
          }
        }
      }
    }
    localObject2 = localObject1;
    if (localObject1 == null)
    {
      localObject2 = new Contact();
      ((Contact)localObject2).a(s.b(new String[] { e }));
    }
    Object localObject1 = (aj.c)c;
    if (localObject1 != null) {
      ((aj.c)localObject1).a((Contact)localObject2);
    }
    a(this, "save");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.ak
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */