package com.truecaller.calling.dialer;

import c.g.b.k;
import com.truecaller.bn;
import com.truecaller.data.entity.Contact;
import com.truecaller.ui.keyboard.b;
import com.truecaller.ui.keyboard.c;
import com.truecaller.ui.view.BottomBar.DialpadState;

public abstract interface aj
{
  public static abstract interface a
  {
    public abstract void a(int paramInt1, int paramInt2);
    
    public abstract void a(int paramInt1, int paramInt2, String paramString);
    
    public abstract void a(b paramb);
    
    public abstract void a(String paramString);
    
    public static abstract interface a
    {
      public abstract void c(String paramString);
    }
    
    public static abstract class b
    {
      public static final class a
        extends aj.a.b
      {
        public static final a a = new a();
        
        private a()
        {
          super();
        }
      }
      
      public static final class b
        extends aj.a.b
      {
        public static final b a = new b();
        
        private b()
        {
          super();
        }
      }
      
      public static final class c
        extends aj.a.b
      {
        final String a;
        
        public c(String paramString)
        {
          super();
          a = paramString;
        }
        
        public final boolean equals(Object paramObject)
        {
          if (this != paramObject) {
            if ((paramObject instanceof c))
            {
              paramObject = (c)paramObject;
              if (k.a(a, a)) {}
            }
            else
            {
              return false;
            }
          }
          return true;
        }
        
        public final int hashCode()
        {
          String str = a;
          if (str != null) {
            return str.hashCode();
          }
          return 0;
        }
        
        public final String toString()
        {
          StringBuilder localStringBuilder = new StringBuilder("TapToPaste(number=");
          localStringBuilder.append(a);
          localStringBuilder.append(")");
          return localStringBuilder.toString();
        }
      }
    }
  }
  
  public static abstract interface b
    extends bn<aj.c, aj.d>, aj.d.a, ca
  {
    public abstract void b();
    
    public abstract void c();
    
    public abstract void e();
    
    public abstract boolean f();
  }
  
  public static abstract interface c
  {
    public abstract void a(Contact paramContact);
    
    public abstract boolean b(String paramString);
  }
  
  public static abstract interface d
    extends aj.a
  {
    public abstract void a(BottomBar.DialpadState paramDialpadState);
    
    public abstract void a(com.truecaller.util.ca paramca);
    
    public abstract void a(boolean paramBoolean);
    
    public abstract void b();
    
    public abstract void b(String paramString);
    
    public abstract void c();
    
    public abstract void c(String paramString);
    
    public abstract void d();
    
    public abstract void d(String paramString);
    
    public static abstract interface a
      extends SelectionAwareEditText.a, b, c
    {
      public abstract void a();
      
      public abstract void a(ap paramap);
      
      public abstract void a(String paramString);
      
      public abstract void b(String paramString);
      
      public abstract void c(String paramString);
      
      public abstract void d();
      
      public abstract void g();
      
      public abstract void h();
      
      public abstract void i();
      
      public abstract void j();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.aj
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */