package com.truecaller.calling.dialer;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import c.g.a.a;
import c.g.b.l;
import com.truecaller.adapter_delegates.i.c;
import com.truecaller.calling.ActionType;
import com.truecaller.calling.aa;
import com.truecaller.calling.ac;
import com.truecaller.calling.an;
import com.truecaller.calling.ao;
import com.truecaller.calling.au;
import com.truecaller.calling.av;
import com.truecaller.calling.az;
import com.truecaller.calling.b;
import com.truecaller.calling.ba;
import com.truecaller.calling.bc;
import com.truecaller.calling.bd;
import com.truecaller.calling.c;
import com.truecaller.calling.d;
import com.truecaller.calling.e;
import com.truecaller.calling.h;
import com.truecaller.calling.o;
import com.truecaller.calling.p;
import com.truecaller.calling.x;
import com.truecaller.search.local.model.c.a;
import com.truecaller.ui.q.a;

public final class m
  extends RecyclerView.ViewHolder
  implements ac, an, ao, az, b, bc, bd, c, n.b, e, h, p, q.a
{
  private final View a;
  
  private m(View paramView, final com.truecaller.adapter_delegates.k paramk, av paramav, ba paramba, o paramo)
  {
    super(paramView);
    b = paramo;
    c = paramo;
    d = paramo;
    e = paramo;
    f = paramo;
    g = paramba;
    h = paramba;
    i = paramav;
    j = new d(paramView);
    k = new com.truecaller.calling.i(paramView);
    l = new aa(paramView);
    m = new au();
    a = paramView;
    paramView = a;
    paramav = (RecyclerView.ViewHolder)this;
    paramba = (a)new a(this, paramk);
    c.g.b.k.b(paramView, "receiver$0");
    c.g.b.k.b(paramk, "receiver");
    c.g.b.k.b(paramav, "holder");
    c.g.b.k.b("ItemEvent.CLICKED", "action");
    c.g.b.k.b(paramba, "dataHolder");
    paramView.setOnClickListener((View.OnClickListener)new i.c(paramView, paramk, "ItemEvent.CLICKED", paramav, paramba));
    com.truecaller.adapter_delegates.i.a(paramView, paramk, paramav);
    c.g.b.k.b(paramk, "eventReceiver");
    c.g.b.k.b(paramav, "holder");
    k.a(paramk, paramav);
    c.g.b.k.b(paramk, "eventReceiver");
    c.g.b.k.b(paramav, "holder");
    d.a(paramk, paramav);
    c.g.b.k.b(paramk, "eventReceiver");
    c.g.b.k.b(paramav, "holder");
    l.a(paramk, paramav);
  }
  
  public final String a()
  {
    return m.b;
  }
  
  public final void a(int paramInt)
  {
    e.a(paramInt);
  }
  
  public final void a(ActionType paramActionType)
  {
    l.a(paramActionType);
  }
  
  public final void a(CallIconType paramCallIconType)
  {
    c.g.b.k.b(paramCallIconType, "callIconType");
    i.a(paramCallIconType);
  }
  
  public final void a(x paramx)
  {
    l.a(paramx);
  }
  
  public final void a(c.a parama)
  {
    j.a(parama);
  }
  
  public final void a(Integer paramInteger)
  {
    i.a(paramInteger);
  }
  
  public final void a(Long paramLong)
  {
    i.a(paramLong);
  }
  
  public final void a(Object paramObject)
  {
    c.a(paramObject);
  }
  
  public final void a(boolean paramBoolean)
  {
    g.a(paramBoolean);
  }
  
  public final void a_(boolean paramBoolean)
  {
    b.a_(paramBoolean);
  }
  
  public final void b(ActionType paramActionType)
  {
    k.b(paramActionType);
  }
  
  public final void b(Integer paramInteger)
  {
    i.b(paramInteger);
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "timestamp");
    i.b(paramString);
  }
  
  public final void b(boolean paramBoolean)
  {
    l.b(paramBoolean);
  }
  
  public final boolean b()
  {
    return m.b();
  }
  
  public final void b_(String paramString)
  {
    h.b_(paramString);
  }
  
  public final void b_(boolean paramBoolean)
  {
    f.b_(paramBoolean);
  }
  
  public final void c(ActionType paramActionType)
  {
    a.setTag(paramActionType);
  }
  
  public final void c(boolean paramBoolean)
  {
    k.c(paramBoolean);
  }
  
  public final void c_(String paramString)
  {
    m.c_(paramString);
  }
  
  public final void c_(boolean paramBoolean)
  {
    m.c_(paramBoolean);
  }
  
  public final void d(boolean paramBoolean)
  {
    d.d(paramBoolean);
  }
  
  public final void e(boolean paramBoolean)
  {
    i.e(paramBoolean);
  }
  
  public final void e_(String paramString)
  {
    i.e_(paramString);
  }
  
  public final void f(boolean paramBoolean)
  {
    i.f(paramBoolean);
  }
  
  public final void g(boolean paramBoolean)
  {
    a.setActivated(paramBoolean);
  }
  
  static final class a
    extends l
    implements a<Object>
  {
    a(m paramm, com.truecaller.adapter_delegates.k paramk)
    {
      super();
    }
    
    public final Object invoke()
    {
      return m.a(a).getTag();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */