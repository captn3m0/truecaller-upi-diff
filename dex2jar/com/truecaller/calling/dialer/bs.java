package com.truecaller.calling.dialer;

import android.os.CancellationSignal;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.data.access.s;
import com.truecaller.data.entity.Contact;
import com.truecaller.network.search.j;
import com.truecaller.network.search.l;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import javax.inject.Inject;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ar;
import kotlinx.coroutines.g;

public final class bs
  implements br
{
  final s a;
  final l b;
  
  @Inject
  public bs(s params, l paraml)
  {
    a = params;
    b = paraml;
  }
  
  public final Object a(c.d.f paramf, final String paramString, final Integer paramInteger, final CancellationSignal paramCancellationSignal, c<? super List<? extends c.n<? extends Contact, String>>> paramc)
  {
    return g.a(paramf, (m)new b(this, paramString, paramCancellationSignal, paramInteger, null), paramc);
  }
  
  public final Object a(String paramString, c.d.f paramf, c<? super Contact> paramc)
  {
    return g.a(paramf, (m)new a(this, 1000L, paramString, null), paramc);
  }
  
  @c.d.b.a.f(b="SearchPerformer.kt", c={59}, d="invokeSuspend", e="com.truecaller.calling.dialer.SearchPerformerImpl$backendThrottledSearch$2")
  static final class a
    extends c.d.b.a.k
    implements m<ag, c<? super Contact>, Object>
  {
    int a;
    private ag e;
    
    a(bs parambs, long paramLong, String paramString, c paramc)
    {
      super(paramc);
    }
    
    public final c<x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new a(b, c, d, paramc);
      e = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      Object localObject = a.a;
      switch (a)
      {
      default: 
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
      case 1: 
        if ((paramObject instanceof o.b)) {
          throw a;
        }
        break;
      case 0: 
        if ((paramObject instanceof o.b)) {
          break label162;
        }
        long l = c;
        a = 1;
        if (ar.a(l, this) == localObject) {
          return localObject;
        }
        break;
      }
      paramObject = null;
      label162:
      try
      {
        localObject = b.b;
        UUID localUUID = UUID.randomUUID();
        c.g.b.k.a(localUUID, "UUID.randomUUID()");
        localObject = ((l)localObject).a(localUUID, "dialpad").b().a(d).d().a().a(4).f();
        if (localObject != null) {
          paramObject = ((com.truecaller.network.search.n)localObject).a();
        }
        return paramObject;
      }
      catch (IOException paramObject) {}
      throw a;
      return null;
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((a)a(paramObject1, (c)paramObject2)).a(x.a);
    }
  }
  
  @c.d.b.a.f(b="SearchPerformer.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.dialer.SearchPerformerImpl$getT9LocalContacts$2")
  static final class b
    extends c.d.b.a.k
    implements m<ag, c<? super List<? extends c.n<? extends Contact, ? extends String>>>, Object>
  {
    int a;
    private ag f;
    
    b(bs parambs, String paramString, CancellationSignal paramCancellationSignal, Integer paramInteger, c paramc)
    {
      super(paramc);
    }
    
    public final c<x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new b(b, paramString, paramCancellationSignal, paramInteger, paramc);
      f = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      Object localObject = a.a;
      if (a == 0)
      {
        if (!(paramObject instanceof o.b))
        {
          localObject = b.a;
          String str = paramString;
          CancellationSignal localCancellationSignal = paramCancellationSignal;
          paramObject = paramInteger;
          if (paramObject != null)
          {
            ((Number)paramObject).intValue();
            paramObject = Integer.valueOf(0);
          }
          else
          {
            paramObject = null;
          }
          return ((s)localObject).a(str, localCancellationSignal, (Integer)paramObject, paramInteger);
        }
        throw a;
      }
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((b)a(paramObject1, (c)paramObject2)).a(x.a);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bs
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */