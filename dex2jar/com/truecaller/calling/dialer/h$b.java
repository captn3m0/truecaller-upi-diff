package com.truecaller.calling.dialer;

import c.g.b.k;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.util.cf;
import com.truecaller.utils.extensions.c;

public final class h$b
{
  public static h a(HistoryEvent paramHistoryEvent, cf paramcf)
  {
    k.b(paramHistoryEvent, "historyEvent");
    if (paramcf != null) {
      paramcf = Boolean.valueOf(paramcf.a(paramHistoryEvent.m()));
    } else {
      paramcf = null;
    }
    boolean bool = c.a(paramcf);
    if (b(paramHistoryEvent)) {
      return (h)new h.e(bool);
    }
    if (a(paramHistoryEvent)) {
      return (h)h.c.f;
    }
    if (c(paramHistoryEvent)) {
      return (h)new h.d();
    }
    return (h)new h.a(bool);
  }
  
  private static boolean a(HistoryEvent paramHistoryEvent)
  {
    return paramHistoryEvent.r() == 3;
  }
  
  private static boolean b(HistoryEvent paramHistoryEvent)
  {
    return k.a("com.whatsapp", paramHistoryEvent.q());
  }
  
  private static boolean c(HistoryEvent paramHistoryEvent)
  {
    return k.a("com.truecaller.voip.manager.VOIP", paramHistoryEvent.q());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.h.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */