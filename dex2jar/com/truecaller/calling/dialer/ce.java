package com.truecaller.calling.dialer;

import c.a.m;
import c.g.a.b;
import c.g.b.k;
import c.g.b.l;
import c.m.i;
import c.m.s;
import com.truecaller.data.entity.HistoryEvent;
import java.util.Iterator;
import java.util.List;

public final class ce
  extends aw
{
  final int d;
  
  public ce(List<? extends HistoryEvent> paramList)
  {
    super((HistoryEvent)localObject, (byte)0);
    paramList = (Iterable)paramList;
    localObject = paramList.iterator();
    while (((Iterator)localObject).hasNext()) {
      a((HistoryEvent)((Iterator)localObject).next());
    }
    paramList = m.n(paramList);
    localObject = (b)a.a;
    k.b(paramList, "receiver$0");
    k.b(localObject, "predicate");
    paramList = (i)new s(paramList, (b)localObject);
    k.b(paramList, "receiver$0");
    paramList = paramList.a();
    while (paramList.hasNext())
    {
      paramList.next();
      i += 1;
      if (i < 0) {
        throw ((Throwable)new ArithmeticException("Count overflow has happened."));
      }
    }
    d = i;
  }
  
  static final class a
    extends l
    implements b<HistoryEvent, Boolean>
  {
    public static final a a = new a();
    
    a()
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.ce
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */