package com.truecaller.calling.dialer;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import c.f;
import com.truecaller.adapter_delegates.i;
import com.truecaller.adapter_delegates.k;
import com.truecaller.utils.extensions.t;

public final class cq
  extends RecyclerView.ViewHolder
  implements cn.c
{
  private final f b;
  private final f c;
  
  public cq(View paramView, k paramk)
  {
    super(paramView);
    b = t.a(paramView, 2131363301);
    c = t.a(paramView, 2131364884);
    RecyclerView.ViewHolder localViewHolder = (RecyclerView.ViewHolder)this;
    i.a(paramView, paramk, localViewHolder, null, 12);
    i.a(paramView, paramk, localViewHolder);
  }
  
  public final void a(int paramInt)
  {
    ((ImageView)b.b()).setImageResource(paramInt);
  }
  
  public final void b(int paramInt)
  {
    ((TextView)c.b()).setText(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.cq
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */