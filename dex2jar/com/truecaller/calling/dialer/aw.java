package com.truecaller.calling.dialer;

import c.a.an;
import c.g.b.k;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.search.local.model.i.a;
import java.util.LinkedHashSet;
import java.util.Set;

public abstract class aw
  implements i.a, Comparable<aw>
{
  final Set<Long> a;
  final Set<Long> b;
  final HistoryEvent c;
  
  private aw(HistoryEvent paramHistoryEvent)
  {
    c = paramHistoryEvent;
    paramHistoryEvent = c.getId();
    Set localSet;
    if (paramHistoryEvent != null)
    {
      localSet = an.b(new Long[] { paramHistoryEvent });
      paramHistoryEvent = localSet;
      if (localSet != null) {}
    }
    else
    {
      paramHistoryEvent = (Set)new LinkedHashSet();
    }
    a = paramHistoryEvent;
    paramHistoryEvent = c.i();
    if (paramHistoryEvent != null)
    {
      localSet = an.b(new Long[] { Long.valueOf(((Number)paramHistoryEvent).longValue()) });
      paramHistoryEvent = localSet;
      if (localSet != null) {}
    }
    else
    {
      paramHistoryEvent = (Set)new LinkedHashSet();
    }
    b = paramHistoryEvent;
  }
  
  public final String a()
  {
    Object localObject2 = c.a();
    Object localObject1 = localObject2;
    if (localObject2 == null) {
      localObject1 = c.b();
    }
    localObject2 = localObject1;
    if (localObject1 == null) {
      localObject2 = "";
    }
    return (String)localObject2;
  }
  
  public final void a(HistoryEvent paramHistoryEvent)
  {
    k.b(paramHistoryEvent, "event");
    Long localLong = paramHistoryEvent.getId();
    if (localLong != null) {
      a.add(localLong);
    }
    paramHistoryEvent = paramHistoryEvent.i();
    if (paramHistoryEvent != null)
    {
      long l = ((Number)paramHistoryEvent).longValue();
      b.add(Long.valueOf(l));
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.aw
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */