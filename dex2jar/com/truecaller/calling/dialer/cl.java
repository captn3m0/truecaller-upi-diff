package com.truecaller.calling.dialer;

import android.content.Context;
import android.view.View;
import c.g.a.q;
import c.g.b.k;
import c.g.b.l;
import com.truecaller.adapter_delegates.h;
import com.truecaller.common.h.aj;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.g;
import com.truecaller.presence.a;
import com.truecaller.utils.n;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;

public final class cl
  extends com.truecaller.adapter_delegates.c<ck.c>
  implements ck.b
{
  private final ck.a c;
  private final cb d;
  private final com.truecaller.search.local.model.c e;
  private final n f;
  private final com.truecaller.network.search.e g;
  private final cf.b.a h;
  private final ax i;
  private final g j;
  private final aj k;
  
  @Inject
  public cl(ck.a parama, @Named("DialerAvailabilityManager") com.truecaller.search.local.model.c paramc, n paramn, @Named("DialerBulkSearcher") com.truecaller.network.search.e parame, cf.b.a parama1, ax paramax, g paramg, aj paramaj)
  {
    e = paramc;
    f = paramn;
    g = parame;
    h = parama1;
    i = paramax;
    j = paramg;
    k = paramaj;
    c = parama;
    d = parama.a((ck.b)this);
  }
  
  private static String a(Contact paramContact, Number paramNumber, String paramString, n paramn, aj paramaj)
  {
    if (paramContact != null) {
      paramContact = paramContact.t();
    } else {
      paramContact = null;
    }
    if (paramContact != null)
    {
      int m;
      if (((CharSequence)paramContact).length() > 0) {
        m = 1;
      } else {
        m = 0;
      }
      if (m != 0) {
        return paramContact;
      }
    }
    if (paramaj.a(new String[] { paramString }))
    {
      paramContact = paramn.a(2131888906, new Object[0]);
      k.a(paramContact, "resourceProvider.getStri…(R.string.text_voicemail)");
      return paramContact;
    }
    if (paramaj.a(paramString))
    {
      paramNumber = paramaj.a();
      paramContact = paramNumber;
      if (paramNumber == null) {
        return paramString;
      }
    }
    else
    {
      paramNumber = paramNumber.n();
      paramContact = paramNumber;
      if (paramNumber == null) {
        paramContact = paramString;
      }
    }
    return paramContact;
  }
  
  private final List<com.truecaller.calling.dialer.suggested_contacts.e> a()
  {
    return c.a((ck.b)this, b[0]);
  }
  
  public final boolean a(h paramh)
  {
    k.b(paramh, "event");
    Object localObject = a;
    int m = ((String)localObject).hashCode();
    String str;
    if (m != -1743572928)
    {
      if ((m == -1314591573) && (((String)localObject).equals("ItemEvent.LONG_CLICKED")))
      {
        localObject = d;
        m = b;
        paramh = (com.truecaller.calling.dialer.suggested_contacts.e)a().get(m);
        str = a(b, paramh.a(j), a, f, k);
        h.a((View)localObject, paramh, str);
        return true;
      }
    }
    else if (((String)localObject).equals("ItemEvent.CLICKED"))
    {
      m = b;
      paramh = (com.truecaller.calling.dialer.suggested_contacts.e)a().get(m);
      localObject = h;
      str = a;
      paramh = b;
      if (paramh != null) {
        paramh = paramh.s();
      } else {
        paramh = null;
      }
      ((cf.b.a)localObject).a(str, paramh, m);
      return true;
    }
    return false;
  }
  
  public final int getItemCount()
  {
    return a().size();
  }
  
  public final long getItemId(int paramInt)
  {
    return ageta.hashCode();
  }
  
  static final class a
    extends l
    implements q<a, Integer, Context, CharSequence>
  {
    a(cl paramcl)
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.cl
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */