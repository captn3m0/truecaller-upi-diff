package com.truecaller.calling.dialer;

import c.g.b.k;
import com.truecaller.adapter_delegates.c;
import com.truecaller.adapter_delegates.h;
import com.truecaller.calling.dialer.a.a;
import java.util.List;
import javax.inject.Inject;

public final class co
  extends c<cn.c>
  implements cn.b
{
  private final cn.a c;
  private final cf.b.b d;
  
  @Inject
  public co(cn.a parama, cf.b.b paramb)
  {
    d = paramb;
    c = parama;
  }
  
  private final List<a> a()
  {
    return c.a((cn.b)this, b[0]);
  }
  
  public final boolean a(h paramh)
  {
    k.b(paramh, "event");
    String str = a;
    int i = str.hashCode();
    if (i != -1743572928)
    {
      if ((i == -1314591573) && (str.equals("ItemEvent.LONG_CLICKED")))
      {
        paramh = d;
        d.a(paramh);
        return true;
      }
    }
    else if (str.equals("ItemEvent.CLICKED"))
    {
      i = b;
      d.b(agetc);
      return true;
    }
    return false;
  }
  
  public final int getItemCount()
  {
    return a().size();
  }
  
  public final long getItemId(int paramInt)
  {
    return ((a)a().get(paramInt)).hashCode();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.co
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */