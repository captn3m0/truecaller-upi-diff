package com.truecaller.calling.dialer;

import c.d.a.a;
import c.d.b.a.f;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;
import com.truecaller.filters.FilterManager;
import kotlinx.coroutines.ag;

@f(b="DialerPresenter.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.dialer.DialerPresenter$onBlockSelectedCallsConfirmed$1$1")
final class af$f$1
  extends c.d.b.a.k
  implements m<ag, c<? super Integer>, Object>
{
  int a;
  private ag c;
  
  af$f$1(af.f paramf, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new 1(b, paramc);
    c = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    if (a == 0)
    {
      if (!(paramObject instanceof o.b)) {
        return Integer.valueOf(b.b.u.a(b.c, "PHONE_NUMBER", "callHistory", false, TruecallerContract.Filters.WildCardType.NONE, b.d));
      }
      throw a;
    }
    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((1)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.af.f.1
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */