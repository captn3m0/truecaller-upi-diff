package com.truecaller.calling.dialer;

import android.app.Activity;
import android.arch.lifecycle.e;
import android.arch.lifecycle.e.b;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.view.ActionMode.Callback;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import c.a.ae;
import c.g.b.k;
import c.u;
import c.x;
import com.truecaller.ads.b.w;
import com.truecaller.analytics.az;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.calling.c.c;
import com.truecaller.calling.c.c.a;
import com.truecaller.calling.initiate_call.b;
import com.truecaller.calling.initiate_call.b.a.a;
import com.truecaller.common.h.o;
import com.truecaller.common.ui.b.a;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.log.d;
import com.truecaller.messaging.defaultsms.DefaultSmsActivity;
import com.truecaller.referral.ReferralManager;
import com.truecaller.search.global.CompositeAdapterDelegate.SearchResultOrder;
import com.truecaller.search.global.n;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.ui.ac;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.ui.p;
import com.truecaller.ui.view.BottomBar;
import com.truecaller.util.bj;
import com.truecaller.util.br.a;
import com.truecaller.util.t;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import javax.inject.Inject;

public final class l
  extends Fragment
  implements DialogInterface.OnClickListener, az, ae.b, aj.c, com.truecaller.ui.ab, p
{
  @Inject
  public ae.a a;
  @Inject
  public n.a b;
  @Inject
  public cf.a c;
  @Inject
  public ck.b d;
  @Inject
  public cn.b e;
  @Inject
  public aj.b f;
  @Inject
  public s g;
  @Inject
  public bn.a h;
  @Inject
  public bw.a i;
  @Inject
  public be.a j;
  @Inject
  public ay.a k;
  @Inject
  public w l;
  @Inject
  public b m;
  @Inject
  public com.truecaller.premium.br n;
  public ReferralManager o;
  private ActionMode p;
  private final a q = new a(this);
  private HashMap r;
  
  public final boolean W_()
  {
    Object localObject = f;
    if (localObject == null) {
      k.a("dialpadPresenter");
    }
    if (!((aj.b)localObject).f())
    {
      localObject = a;
      if (localObject == null) {
        k.a("dialerPresenter");
      }
      if (!((ae.a)localObject).h()) {
        return false;
      }
    }
    return true;
  }
  
  public final ae.a a()
  {
    ae.a locala = a;
    if (locala == null) {
      k.a("dialerPresenter");
    }
    return locala;
  }
  
  public final void a(final Contact paramContact)
  {
    k.b(paramContact, "contact");
    try
    {
      com.truecaller.util.br.a(paramContact, (br.a)new b(this, paramContact)).show(getFragmentManager(), com.truecaller.util.br.a);
      return;
    }
    catch (ActivityNotFoundException paramContact)
    {
      d.a((Throwable)paramContact, "Cannot find an activity to insert contact");
    }
  }
  
  public final void a(Contact paramContact, DetailsFragment.SourceType paramSourceType, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    k.b(paramContact, "contact");
    k.b(paramSourceType, "sourceType");
    f localf = getActivity();
    if (localf == null) {
      return;
    }
    k.a(localf, "activity ?: return");
    DetailsFragment.a((Context)localf, paramContact, paramSourceType, paramBoolean1, paramBoolean2, paramBoolean3);
  }
  
  public final void a(Contact paramContact, String paramString)
  {
    k.b(paramContact, "contact");
    k.b(paramString, "analyticsContext");
    f localf = getActivity();
    if (localf == null) {
      return;
    }
    k.a(localf, "activity ?: return");
    c.a locala = c.e;
    c.a.a(localf, paramContact, paramContact.A(), false, true, false, false, paramString, 232);
  }
  
  public final void a(Contact paramContact, String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "fallbackNumber");
    k.b(paramString2, "callType");
    k.b(paramString3, "analyticsContext");
    f localf = getActivity();
    if (localf == null) {
      return;
    }
    k.a(localf, "activity ?: return");
    com.truecaller.calling.e.i.a((Activity)localf, paramContact, paramString1, paramString2, paramString3);
  }
  
  public final void a(HistoryEvent paramHistoryEvent, DetailsFragment.SourceType paramSourceType, boolean paramBoolean1, boolean paramBoolean2)
  {
    k.b(paramHistoryEvent, "historyEvent");
    k.b(paramSourceType, "sourceType");
    Object localObject1 = getActivity();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "activity ?: return");
    Context localContext = (Context)localObject1;
    localObject1 = paramHistoryEvent.s();
    if (localObject1 != null) {
      localObject1 = ((Contact)localObject1).getTcId();
    } else {
      localObject1 = null;
    }
    Object localObject2 = paramHistoryEvent.s();
    if (localObject2 != null) {
      localObject2 = ((Contact)localObject2).t();
    } else {
      localObject2 = null;
    }
    DetailsFragment.a(localContext, (String)localObject1, (String)localObject2, paramHistoryEvent.a(), paramHistoryEvent.b(), paramHistoryEvent.d(), paramSourceType, paramBoolean1, paramBoolean2);
  }
  
  public final void a(String paramString)
  {
    paramString = a;
    if (paramString == null) {
      k.a("dialerPresenter");
    }
    paramString.m();
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "number");
    k.b(paramString2, "analyticsContext");
    paramString2 = getActivity();
    if (paramString2 == null) {
      return;
    }
    k.a(paramString2, "activity ?: return");
    bj.a((Context)paramString2, paramString1);
  }
  
  public final void a(String paramString1, String paramString2, boolean paramBoolean, CompositeAdapterDelegate.SearchResultOrder paramSearchResultOrder)
  {
    k.b(paramSearchResultOrder, "searchOrder");
    f localf = getActivity();
    if (localf == null) {
      return;
    }
    k.a(localf, "activity ?: return");
    n.a((Activity)localf, paramString1, paramString2, paramBoolean, paramSearchResultOrder);
  }
  
  public final void a(String paramString1, String paramString2, boolean paramBoolean, String paramString3)
  {
    k.b(paramString1, "number");
    k.b(paramString3, "analyticsContext");
    paramString1 = new b.a.a(paramString1, paramString3).a(paramString2).a(paramBoolean);
    paramString2 = m;
    if (paramString2 == null) {
      k.a("initiateCallHelper");
    }
    paramString2.a(paramString1.a());
  }
  
  public final void a(boolean paramBoolean)
  {
    ae.a locala = a;
    if (locala == null) {
      k.a("dialerPresenter");
    }
    locala.k();
  }
  
  public final void b()
  {
    f localf = getActivity();
    if (localf != null)
    {
      ((AppCompatActivity)localf).startSupportActionMode((ActionMode.Callback)q);
      return;
    }
    throw new u("null cannot be cast to non-null type android.support.v7.app.AppCompatActivity");
  }
  
  public final void b(Contact paramContact, String paramString)
  {
    k.b(paramContact, "contact");
    k.b(paramString, "analyticsContext");
    f localf = getActivity();
    if (localf == null) {
      return;
    }
    k.a(localf, "activity ?: return");
    c.a locala = c.e;
    c.a.a(localf, paramContact, paramContact.A(), false, false, true, false, paramString, 184);
  }
  
  public final boolean b(String paramString)
  {
    k.b(paramString, "tag");
    j localj = getFragmentManager();
    if (localj != null) {
      paramString = localj.a(paramString);
    } else {
      paramString = null;
    }
    return paramString != null;
  }
  
  public final void b_(int paramInt)
  {
    com.truecaller.calling.d.m.a((Fragment)this, paramInt, null, true);
  }
  
  public final void c()
  {
    ActionMode localActionMode = p;
    if (localActionMode != null)
    {
      int i1 = q.a;
      Object localObject = localActionMode.getTag();
      if (((localObject instanceof Integer)) && (i1 == ((Integer)localObject).intValue())) {
        i1 = 1;
      } else {
        i1 = 0;
      }
      if (i1 == 0) {
        localActionMode = null;
      }
      if (localActionMode != null)
      {
        localActionMode.finish();
        return;
      }
    }
  }
  
  public final void d()
  {
    ActionMode localActionMode = p;
    if (localActionMode != null)
    {
      localActionMode.invalidate();
      return;
    }
  }
  
  public final int f()
  {
    ae.a locala = a;
    if (locala == null) {
      k.a("dialerPresenter");
    }
    return locala.n();
  }
  
  public final void h()
  {
    f localf = getActivity();
    Object localObject = localf;
    if (!(localf instanceof TruecallerInit)) {
      localObject = null;
    }
    localObject = (TruecallerInit)localObject;
    if (localObject == null) {
      return;
    }
    if (isAdded()) {
      ((TruecallerInit)localObject).a("contacts");
    }
  }
  
  public final void i()
  {
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    k.a(localContext, "context ?: return");
    startActivityForResult(DefaultSmsActivity.a(localContext, "callHistory"), 4);
  }
  
  public final void m()
  {
    if (a == null) {
      k.a("dialerPresenter");
    }
  }
  
  public final void n()
  {
    ae.a locala = a;
    if (locala == null) {
      k.a("dialerPresenter");
    }
    locala.j();
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (com.truecaller.calling.d.m.a(paramInt1, paramInt2, paramIntent, null)) {
      return;
    }
    c.g.a.a locala = (c.g.a.a)new c(this);
    int i1;
    if (paramInt1 == 4)
    {
      locala.invoke();
      i1 = 1;
    }
    else
    {
      i1 = 0;
    }
    if (i1 != 0) {
      return;
    }
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
  }
  
  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    if (paramInt == -1)
    {
      paramDialogInterface = b;
      if (paramDialogInterface == null) {
        k.a("completedCallLogItemsPresenter");
      }
      paramDialogInterface.u_();
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getContext();
    if (paramBundle != null) {
      paramBundle = paramBundle.getApplicationContext();
    } else {
      paramBundle = null;
    }
    if (paramBundle != null)
    {
      ((bk)paramBundle).a().a(new bi(o, getChildFragmentManager())).a(this);
      paramBundle = g;
      if (paramBundle == null) {
        k.a("callHistoryObserver");
      }
      Object localObject = getLifecycle();
      k.a(localObject, "lifecycle");
      paramBundle.a((q)new LifecycleAwareCondition((e)localObject, e.b.d));
      paramBundle = a;
      if (paramBundle == null) {
        k.a("dialerPresenter");
      }
      localObject = g;
      if (localObject == null) {
        k.a("callHistoryObserver");
      }
      paramBundle.a((v)localObject);
      setHasOptionsMenu(true);
      paramBundle = a;
      if (paramBundle == null) {
        k.a("dialerPresenter");
      }
      paramBundle.b(this);
      paramBundle = f;
      if (paramBundle == null) {
        k.a("dialpadPresenter");
      }
      paramBundle.b(this);
      return;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.GraphHolder");
  }
  
  public final void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    k.b(paramMenu, "menu");
    k.b(paramMenuInflater, "inflater");
    paramMenuInflater.inflate(2131623941, paramMenu);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558668, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    Object localObject = f;
    if (localObject == null) {
      k.a("dialpadPresenter");
    }
    ((aj.b)localObject).x_();
    localObject = a;
    if (localObject == null) {
      k.a("dialerPresenter");
    }
    ((ae.a)localObject).x_();
    super.onDestroy();
  }
  
  public final void onDestroyView()
  {
    Object localObject = a;
    if (localObject == null) {
      k.a("dialerPresenter");
    }
    ((ae.a)localObject).y_();
    localObject = f;
    if (localObject == null) {
      k.a("dialpadPresenter");
    }
    ((aj.b)localObject).y_();
    super.onDestroyView();
    localObject = r;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onHiddenChanged(boolean paramBoolean)
  {
    super.onHiddenChanged(paramBoolean);
    if (paramBoolean)
    {
      ae.a locala = a;
      if (locala == null) {
        k.a("dialerPresenter");
      }
      locala.l();
    }
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    k.b(paramMenuItem, "item");
    if (paramMenuItem.getItemId() != 2131361866) {
      return super.onOptionsItemSelected(paramMenuItem);
    }
    paramMenuItem = a;
    if (paramMenuItem == null) {
      k.a("dialerPresenter");
    }
    paramMenuItem.o();
    return true;
  }
  
  public final void onPause()
  {
    aj.b localb = f;
    if (localb == null) {
      k.a("dialpadPresenter");
    }
    localb.e();
    super.onPause();
  }
  
  public final void onResume()
  {
    super.onResume();
    Object localObject1 = getActivity();
    Intent localIntent;
    if (localObject1 != null)
    {
      k.a(localObject1, "activity ?: return");
      localIntent = ((f)localObject1).getIntent();
      if (localIntent != null)
      {
        localObject1 = localIntent.getAction();
        if (localObject1 != null)
        {
          int i1 = ((String)localObject1).hashCode();
          if (i1 != -1173708363 ? (i1 == -1173171990) && (((String)localObject1).equals("android.intent.action.VIEW")) : ((String)localObject1).equals("android.intent.action.DIAL")) {
            if (k.a(localIntent.getType(), "vnd.android.cursor.dir/calls"))
            {
              localObject1 = f;
              if (localObject1 == null) {
                k.a("dialpadPresenter");
              }
              ((aj.b)localObject1).d();
            }
          }
        }
      }
    }
    try
    {
      localObject1 = com.truecaller.common.h.ab.a(localIntent, getContext());
    }
    catch (SecurityException localSecurityException)
    {
      Object localObject2;
      for (;;) {}
    }
    localObject1 = null;
    localObject2 = f;
    if (localObject2 == null) {
      k.a("dialpadPresenter");
    }
    ((aj.b)localObject2).c(bj.b((String)localObject1));
    localIntent.setAction(null);
    if (localIntent.hasExtra("promotion_setting_key"))
    {
      localObject1 = localIntent.getStringExtra("promotion_setting_key");
      localObject2 = a;
      if (localObject2 == null) {
        k.a("dialerPresenter");
      }
      k.a(localObject1, "promotionType");
      ((ae.a)localObject2).a((String)localObject1);
      localIntent.removeExtra("promotion_setting_key");
    }
    localObject1 = f;
    if (localObject1 == null) {
      k.a("dialpadPresenter");
    }
    ((aj.b)localObject1).c();
    localObject1 = a;
    if (localObject1 == null) {
      k.a("dialerPresenter");
    }
    ((ae.a)localObject1).i();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    paramBundle = a;
    if (paramBundle == null) {
      k.a("dialerPresenter");
    }
    Object localObject1 = a;
    if (localObject1 == null) {
      k.a("dialerPresenter");
    }
    localObject1 = (ae.c.b)localObject1;
    Object localObject2 = getActivity();
    if (localObject2 != null)
    {
      localObject2 = (com.truecaller.common.ui.a)localObject2;
      Object localObject3 = (c.g.a.a)new d(this);
      n.a locala = b;
      if (locala == null) {
        k.a("completedCallLogItemsPresenter");
      }
      cf.a locala1 = c;
      if (locala1 == null) {
        k.a("suggestedBarPresenter");
      }
      be.a locala2 = j;
      if (locala2 == null) {
        k.a("promotionsPresenter");
      }
      ck.b localb = d;
      if (localb == null) {
        k.a("suggestedContactsPresenter");
      }
      cn.b localb1 = e;
      if (localb1 == null) {
        k.a("suggestedPremiumPresenter");
      }
      w localw = l;
      if (localw == null) {
        k.a("multiAdsPresenter");
      }
      aj.b localb2 = f;
      if (localb2 == null) {
        k.a("dialpadPresenter");
      }
      bn.a locala3 = h;
      if (locala3 == null) {
        k.a("searchMorePresenter");
      }
      bw.a locala4 = i;
      if (locala4 == null) {
        k.a("searchResultItemsPresenter");
      }
      ay.a locala5 = k;
      if (locala5 == null) {
        k.a("onGoingFlashPresenter");
      }
      b.a locala6 = (b.a)getActivity();
      com.truecaller.premium.br localbr = n;
      if (localbr == null) {
        k.a("premiumScreenNavigator");
      }
      paramBundle.a(new ai((ae.c.b)localObject1, (com.truecaller.common.ui.a)localObject2, paramView, (c.g.a.a)localObject3, locala, locala1, locala2, localb, localb1, localw, localb2, locala3, locala4, locala5, locala6, localbr));
      paramBundle = f;
      if (paramBundle == null) {
        k.a("dialpadPresenter");
      }
      localObject1 = (ConstraintLayout)paramView;
      localObject2 = f;
      if (localObject2 == null) {
        k.a("dialpadPresenter");
      }
      localObject2 = (aj.d.a)localObject2;
      paramView = paramView.findViewById(2131363475);
      k.a(paramView, "view.findViewById(R.id.input_window)");
      localObject3 = (ViewGroup)paramView;
      paramView = getActivity();
      if (paramView != null) {
        paramView = paramView.findViewById(2131362140);
      } else {
        paramView = null;
      }
      if (paramView != null)
      {
        paramBundle.a(new ao((ConstraintLayout)localObject1, (aj.d.a)localObject2, (ViewGroup)localObject3, (BottomBar)paramView));
        return;
      }
      throw new u("null cannot be cast to non-null type com.truecaller.ui.view.BottomBar");
    }
    throw new u("null cannot be cast to non-null type com.truecaller.common.ui.AppBarContainer");
  }
  
  public final void v_()
  {
    f localf = getActivity();
    if (localf != null)
    {
      b localb = m;
      if (localb == null) {
        k.a("initiateCallHelper");
      }
      k.a(localf, "it");
      localb.a((Activity)localf);
      return;
    }
  }
  
  public final void w_()
  {
    new ac().show(getChildFragmentManager(), null);
  }
  
  public static final class a
    implements ActionMode.Callback
  {
    final int a = 1;
    
    public final boolean onActionItemClicked(ActionMode paramActionMode, MenuItem paramMenuItem)
    {
      k.b(paramActionMode, "actionMode");
      k.b(paramMenuItem, "menuItem");
      return b.a().a(a, paramMenuItem.getItemId());
    }
    
    public final boolean onCreateActionMode(ActionMode paramActionMode, Menu paramMenu)
    {
      k.b(paramActionMode, "actionMode");
      k.b(paramMenu, "menu");
      Integer localInteger = Integer.valueOf(b.a().c(a));
      int i;
      if (((Number)localInteger).intValue() > 0) {
        i = 1;
      } else {
        i = 0;
      }
      if (i == 0) {
        localInteger = null;
      }
      if (localInteger != null)
      {
        i = ((Number)localInteger).intValue();
        paramActionMode.getMenuInflater().inflate(i, paramMenu);
      }
      paramActionMode.setTag(Integer.valueOf(a));
      l.a(b, paramActionMode);
      b.a().a(a);
      return true;
    }
    
    public final void onDestroyActionMode(ActionMode paramActionMode)
    {
      k.b(paramActionMode, "actionMode");
      b.a().b(a);
    }
    
    public final boolean onPrepareActionMode(ActionMode paramActionMode, Menu paramMenu)
    {
      k.b(paramActionMode, "actionMode");
      k.b(paramMenu, "menu");
      Object localObject = b.a().c();
      if (localObject != null) {
        paramActionMode.setTitle((CharSequence)localObject);
      }
      localObject = (Iterable)c.k.i.b(0, paramMenu.size());
      paramActionMode = (Collection)new ArrayList(c.a.m.a((Iterable)localObject, 10));
      localObject = ((Iterable)localObject).iterator();
      while (((Iterator)localObject).hasNext()) {
        paramActionMode.add(paramMenu.getItem(((ae)localObject).a()));
      }
      paramActionMode = ((Iterable)paramActionMode).iterator();
      while (paramActionMode.hasNext())
      {
        paramMenu = (MenuItem)paramActionMode.next();
        k.a(paramMenu, "it");
        paramMenu.setVisible(b.a().b(a, paramMenu.getItemId()));
      }
      return true;
    }
  }
  
  static final class b
    implements br.a
  {
    b(l paraml, Contact paramContact) {}
    
    public final void onContactPrepared(Contact paramContact, byte[] paramArrayOfByte)
    {
      k.b(paramContact, "<anonymous parameter 0>");
      o.a((Fragment)a, t.a(paramContact, paramArrayOfByte));
    }
  }
  
  static final class c
    extends c.g.b.l
    implements c.g.a.a<x>
  {
    c(l paraml)
    {
      super();
    }
  }
  
  static final class d
    extends c.g.b.l
    implements c.g.a.a<View>
  {
    d(l paraml)
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */