package com.truecaller.calling.dialer;

import android.content.Context;
import c.g.a.q;
import com.truecaller.calling.an;
import com.truecaller.calling.ao;
import com.truecaller.calling.bc;
import com.truecaller.calling.c;
import com.truecaller.calling.p;
import com.truecaller.presence.a;

public abstract interface ck$c
  extends an, ao, bc, c, p
{
  public abstract void a(q<? super a, ? super Integer, ? super Context, ? extends CharSequence> paramq);
  
  public abstract void b(int paramInt);
  
  public abstract void b(boolean paramBoolean);
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.ck.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */