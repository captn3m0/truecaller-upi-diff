package com.truecaller.calling.dialer;

import c.g.b.k;
import com.truecaller.data.entity.Contact;

public final class av
{
  final Contact a;
  final String b;
  final Long c;
  
  public av(Contact paramContact, String paramString, Long paramLong)
  {
    a = paramContact;
    b = paramString;
    c = paramLong;
  }
  
  private static av a(Contact paramContact, String paramString, Long paramLong)
  {
    k.b(paramContact, "contact");
    k.b(paramString, "matchedValue");
    return new av(paramContact, paramString, paramLong);
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject) {
      if ((paramObject instanceof av))
      {
        paramObject = (av)paramObject;
        if ((k.a(a, a)) && (k.a(b, b)) && (k.a(c, c))) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    Object localObject = a;
    int k = 0;
    int i;
    if (localObject != null) {
      i = localObject.hashCode();
    } else {
      i = 0;
    }
    localObject = b;
    int j;
    if (localObject != null) {
      j = localObject.hashCode();
    } else {
      j = 0;
    }
    localObject = c;
    if (localObject != null) {
      k = localObject.hashCode();
    }
    return (i * 31 + j) * 31 + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("LocalResult(contact=");
    localStringBuilder.append(a);
    localStringBuilder.append(", matchedValue=");
    localStringBuilder.append(b);
    localStringBuilder.append(", refetchStartedAt=");
    localStringBuilder.append(c);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.av
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */