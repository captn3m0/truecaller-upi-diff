package com.truecaller.calling.dialer;

import c.g.b.k;
import com.truecaller.adapter_delegates.c;
import com.truecaller.adapter_delegates.d;
import com.truecaller.analytics.e.a;
import com.truecaller.calling.ActionType;
import com.truecaller.calling.ae;
import com.truecaller.calling.af;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.flashsdk.core.i;
import com.truecaller.flashsdk.db.g;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.util.cf;
import com.truecaller.voip.ai;
import java.util.List;
import java.util.Map;

public abstract class j<View extends i.c>
  extends d<View>
  implements i.b<View>
{
  final i.a c;
  final bd d;
  final com.truecaller.flashsdk.core.b e;
  final cf f;
  private final i.a g;
  private final com.truecaller.analytics.b h;
  private final a i;
  private final i j;
  private final ai k;
  
  public j(i.a parama, bd parambd, com.truecaller.analytics.b paramb, a parama1, i parami, com.truecaller.flashsdk.core.b paramb1, cf paramcf, ai paramai)
  {
    c = parama;
    d = parambd;
    h = paramb;
    i = parama1;
    j = parami;
    e = paramb1;
    f = paramcf;
    k = paramai;
    g = c;
  }
  
  private final c.x a(HistoryEvent paramHistoryEvent)
  {
    paramHistoryEvent = paramHistoryEvent.s();
    if (paramHistoryEvent != null)
    {
      k.a(paramHistoryEvent, "contact");
      paramHistoryEvent = paramHistoryEvent.A();
      k.a(paramHistoryEvent, "contact.numbers");
      Object localObject = (com.truecaller.data.entity.Number)c.a.m.e(paramHistoryEvent);
      if (localObject != null)
      {
        paramHistoryEvent = k;
        localObject = ((com.truecaller.data.entity.Number)localObject).a();
        k.a(localObject, "number.normalizedNumber");
        paramHistoryEvent.a((String)localObject, "callLog");
      }
      return c.x.a;
    }
    return null;
  }
  
  private final void a(HistoryEvent paramHistoryEvent, String paramString)
  {
    bd localbd = d;
    paramHistoryEvent = paramHistoryEvent.b();
    k.a(paramHistoryEvent, "historyEvent.rawNumber");
    localbd.a(paramHistoryEvent, "callHistory");
    b("message", paramString);
  }
  
  private final void a(HistoryEvent paramHistoryEvent, boolean paramBoolean, String paramString)
  {
    String str2 = paramHistoryEvent.b();
    if (str2 == null) {
      return;
    }
    bd localbd = d;
    Object localObject = paramHistoryEvent.s();
    if (localObject != null)
    {
      String str1 = ((Contact)localObject).s();
      localObject = str1;
      if (str1 != null) {}
    }
    else
    {
      localObject = paramHistoryEvent.e();
    }
    localbd.a(str2, (String)localObject, paramBoolean, "callHistory");
    b("call", paramString);
  }
  
  private final boolean a(int paramInt, ActionType paramActionType)
  {
    a(b(paramInt), paramActionType, "swipe");
    return true;
  }
  
  private final void b(HistoryEvent paramHistoryEvent, String paramString)
  {
    d.a(paramHistoryEvent, DetailsFragment.SourceType.CallLog, false, false);
    b("details", paramString);
  }
  
  private final void b(String paramString1, String paramString2)
  {
    paramString1 = new e.a("ViewAction").a("Context", "callLog").a("Action", paramString1);
    if (paramString2 != null) {
      paramString1.a("SubAction", paramString2);
    }
    paramString2 = h;
    paramString1 = paramString1.a();
    k.a(paramString1, "eventBuilder.build()");
    paramString2.b(paramString1);
  }
  
  private final void c(HistoryEvent paramHistoryEvent, String paramString)
  {
    String str = paramHistoryEvent.a();
    if (str == null) {
      return;
    }
    k.a(str, "historyEvent.normalizedNumber ?: return");
    d.a(paramHistoryEvent.s(), str, paramString, "callHistory");
  }
  
  private final void e(int paramInt)
  {
    HistoryEvent localHistoryEvent = b(paramInt);
    c.a(localHistoryEvent);
  }
  
  protected final com.truecaller.calling.x a(String paramString1, String paramString2)
  {
    i locali = j;
    if ((ae.a(locali)) && (paramString1 != null))
    {
      if (paramString2 == null) {
        return null;
      }
      if (!locali.b(paramString1)) {
        paramString1 = null;
      }
      if (paramString1 != null)
      {
        paramString1 = c.n.m.d(c.n.m.a(paramString1, "+", ""));
        if (paramString1 != null) {
          return new com.truecaller.calling.x(c.a.m.a(Long.valueOf(((Number)paramString1).longValue())), paramString2, "callHistory");
        }
        return null;
      }
      return null;
    }
    return null;
  }
  
  protected final List<aw> a()
  {
    return g.a((i.b)this, b[0]);
  }
  
  public boolean a(com.truecaller.adapter_delegates.h paramh)
  {
    k.b(paramh, "event");
    int m = b;
    Object localObject2 = e;
    Object localObject1 = localObject2;
    if (!(localObject2 instanceof ActionType)) {
      localObject1 = null;
    }
    localObject2 = (ActionType)localObject1;
    localObject1 = localObject2;
    if (localObject2 == null) {
      localObject1 = ActionType.CELLULAR_CALL;
    }
    paramh = a;
    switch (paramh.hashCode())
    {
    default: 
      return false;
    case 1140909776: 
      if (paramh.equals("ItemEvent.INVALIDATE_ITEM"))
      {
        "invalidate view for position : ".concat(String.valueOf(m));
        c.f().a(m);
        return true;
      }
      break;
    case 39226006: 
      if (paramh.equals("ItemEvent.SWIPE_START")) {
        return d(m);
      }
      break;
    case -245760723: 
      if (paramh.equals("ItemEvent.SWIPE_COMPLETED_FROM_START")) {
        return a(m, (ActionType)localObject1);
      }
      break;
    case -1314591573: 
      if (paramh.equals("ItemEvent.LONG_CLICKED"))
      {
        if (!a)
        {
          i.b();
          e(m);
          return true;
        }
        return false;
      }
      break;
    case -1743572928: 
      if (paramh.equals("ItemEvent.CLICKED"))
      {
        if (a)
        {
          e(m);
          return true;
        }
        return a((ActionType)localObject1, m);
      }
      break;
    case -1837138842: 
      if (paramh.equals("ItemEvent.SWIPE_COMPLETED_FROM_END")) {
        return a(m, ActionType.SMS);
      }
      break;
    }
    return false;
  }
  
  protected abstract boolean a(ActionType paramActionType, int paramInt);
  
  protected final boolean a(HistoryEvent paramHistoryEvent, ActionType paramActionType, String paramString)
  {
    k.b(paramHistoryEvent, "historyEvent");
    k.b(paramActionType, "action");
    switch (k.a[paramActionType.ordinal()])
    {
    default: 
      return true;
    case 7: 
      a(paramHistoryEvent);
      return true;
    case 6: 
      b(paramHistoryEvent, paramString);
      return true;
    case 5: 
      a(paramHistoryEvent, paramString);
      return true;
    case 4: 
      a(paramHistoryEvent, false, paramString);
      return true;
    case 3: 
      a(paramHistoryEvent, true, paramString);
      return true;
    case 2: 
      c(paramHistoryEvent, "video");
      return true;
    }
    c(paramHistoryEvent, "call");
    return true;
  }
  
  protected final HistoryEvent b(int paramInt)
  {
    return agetc;
  }
  
  protected final boolean c(int paramInt)
  {
    HistoryEvent localHistoryEvent = b(paramInt);
    String str = localHistoryEvent.a();
    boolean bool2 = false;
    if (str == null) {
      return false;
    }
    k.a(str, "historyEvent.normalizedNumber ?: return false");
    Map localMap = c.g();
    Integer localInteger = Integer.valueOf(paramInt);
    Object localObject2 = localMap.get(localInteger);
    Object localObject1 = localObject2;
    if (localObject2 == null)
    {
      localObject1 = h.e;
      boolean bool1 = bool2;
      if (k.a(h.b.a(localHistoryEvent, f), h.c.f))
      {
        bool1 = bool2;
        if (System.currentTimeMillis() - localHistoryEvent.j() < 60000L)
        {
          bool1 = bool2;
          if (System.currentTimeMillis() - e.g(c.n.m.a(str, "+", "")).b < 60000L) {
            bool1 = true;
          }
        }
      }
      localObject1 = Boolean.valueOf(bool1);
      localMap.put(localInteger, localObject1);
    }
    return ((Boolean)localObject1).booleanValue();
  }
  
  public abstract boolean d(int paramInt);
  
  public int getItemCount()
  {
    return a().size();
  }
  
  public long getItemId(int paramInt)
  {
    Long localLong = b(paramInt).getId();
    if (localLong != null) {
      return localLong.longValue();
    }
    return -1L;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */