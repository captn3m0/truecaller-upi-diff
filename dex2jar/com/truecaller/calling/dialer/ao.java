package com.truecaller.calling.dialer;

import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.d;
import android.support.transition.n;
import android.support.transition.p;
import android.support.v7.app.AlertDialog.Builder;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import c.g.b.k;
import c.g.b.l;
import c.x;
import com.truecaller.R.id;
import com.truecaller.ui.keyboard.DialerKeypad;
import com.truecaller.ui.view.BottomBar;
import com.truecaller.ui.view.BottomBar.DialpadState;
import com.truecaller.ui.view.TintedImageView;
import com.truecaller.util.an;
import com.truecaller.util.ax;
import com.truecaller.util.bo;
import com.truecaller.util.ca;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import kotlinx.a.a.a;

public final class ao
  implements aj.d, a
{
  final aj.d.a a;
  private an b;
  private final ConstraintLayout c;
  private final ViewGroup d;
  private final BottomBar e;
  private HashMap f;
  
  public ao(ConstraintLayout paramConstraintLayout, aj.d.a parama, ViewGroup paramViewGroup, BottomBar paramBottomBar)
  {
    c = paramConstraintLayout;
    a = parama;
    d = paramViewGroup;
    e = paramBottomBar;
    paramConstraintLayout = (SelectionAwareEditText)a(R.id.inputField);
    paramConstraintLayout.addTextChangedListener((TextWatcher)new a(this));
    paramConstraintLayout.setSelectionChangeListener((SelectionAwareEditText.a)a);
    paramConstraintLayout.setOnTouchListener((View.OnTouchListener)new ax((EditText)paramConstraintLayout));
    if (Build.VERSION.SDK_INT >= 26) {
      paramConstraintLayout.setShowSoftInputOnFocus(false);
    }
    paramConstraintLayout = (DialerKeypad)a(R.id.dialpad);
    paramConstraintLayout.setDialpadListener((com.truecaller.ui.keyboard.c)a);
    paramConstraintLayout.setActionsListener((com.truecaller.ui.keyboard.b)a);
    paramConstraintLayout = (TintedImageView)a(R.id.delete);
    paramConstraintLayout.setOnClickListener((View.OnClickListener)new b(this));
    paramConstraintLayout.setOnLongClickListener((View.OnLongClickListener)new c(this));
    ((TintedImageView)a(R.id.addContact)).setOnClickListener((View.OnClickListener)new View.OnClickListener()
    {
      public final void onClick(View paramAnonymousView)
      {
        a.a.j();
      }
    });
    ((LinearLayout)a(R.id.tapToPasteContainer)).setOnClickListener((View.OnClickListener)new View.OnClickListener()
    {
      public final void onClick(View paramAnonymousView)
      {
        a.a.i();
      }
    });
  }
  
  public final View a(int paramInt)
  {
    if (f == null) {
      f = new HashMap();
    }
    View localView2 = (View)f.get(Integer.valueOf(paramInt));
    View localView1 = localView2;
    if (localView2 == null)
    {
      localView1 = a();
      if (localView1 == null) {
        return null;
      }
      localView1 = localView1.findViewById(paramInt);
      f.put(Integer.valueOf(paramInt), localView1);
    }
    return localView1;
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    SelectionAwareEditText localSelectionAwareEditText = (SelectionAwareEditText)a(R.id.inputField);
    k.a(localSelectionAwareEditText, "inputField");
    localSelectionAwareEditText.getEditableText().delete(paramInt1, paramInt2);
  }
  
  public final void a(int paramInt1, int paramInt2, String paramString)
  {
    k.b(paramString, "text");
    SelectionAwareEditText localSelectionAwareEditText = (SelectionAwareEditText)a(R.id.inputField);
    k.a(localSelectionAwareEditText, "inputField");
    localSelectionAwareEditText.getEditableText().replace(paramInt1, paramInt2, (CharSequence)paramString);
  }
  
  public final void a(aj.a.b paramb)
  {
    k.b(paramb, "mode");
    Object localObject = (LinearLayout)a(R.id.inputFieldContainer);
    k.a(localObject, "inputFieldContainer");
    t.a((View)localObject, paramb instanceof aj.a.b.b);
    localObject = (LinearLayout)a(R.id.tapToPasteContainer);
    k.a(localObject, "tapToPasteContainer");
    localObject = (View)localObject;
    boolean bool = paramb instanceof aj.a.b.c;
    t.a((View)localObject, bool);
    localObject = (TextView)a(R.id.tapToPasteNumber);
    k.a(localObject, "tapToPasteNumber");
    if (bool)
    {
      TextView localTextView = (TextView)a(R.id.tapToPasteNumber);
      k.a(localTextView, "tapToPasteNumber");
      paramb = (CharSequence)localTextView.getContext().getString(2131887278, new Object[] { a });
    }
    else
    {
      paramb = (CharSequence)"";
    }
    ((TextView)localObject).setText(paramb);
  }
  
  public final void a(BottomBar.DialpadState paramDialpadState)
  {
    k.b(paramDialpadState, "state");
    e.setDialpadState(paramDialpadState);
  }
  
  public final void a(ca paramca)
  {
    k.b(paramca, "sequenceResponse");
    Context localContext = d.getContext();
    k.a(localContext, "containerView.context");
    paramca.a(localContext);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "text");
    SelectionAwareEditText localSelectionAwareEditText = (SelectionAwareEditText)a(R.id.inputField);
    k.a(localSelectionAwareEditText, "inputField");
    localSelectionAwareEditText.getEditableText().append((CharSequence)paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject = (ViewGroup)c;
    android.support.transition.c localc = new android.support.transition.c();
    localc.a(2131362833, true);
    localc.c(100L);
    localc.b((TimeInterpolator)new AccelerateDecelerateInterpolator());
    p.a((ViewGroup)localObject, (n)localc);
    e.setShadowVisibility(paramBoolean ^ true);
    localObject = new d();
    ((d)localObject).a(c);
    int j = d.getId();
    int i = 3;
    ((d)localObject).b(j, 3);
    ((d)localObject).b(d.getId(), 4);
    j = d.getId();
    if (paramBoolean) {
      i = 4;
    }
    ((d)localObject).a(j, i);
    j = d.getId();
    if (paramBoolean) {
      i = 0;
    } else {
      i = 8;
    }
    ((d)localObject).c(j, i);
    ((d)localObject).b(c);
  }
  
  public final void b()
  {
    if (b == null) {
      b = new an(d.getContext());
    }
    ((DialerKeypad)a(R.id.dialpad)).setFeedback(b);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "isoCode");
    new d(paramString, (c.g.a.b)new e(this)).execute(new Void[0]);
  }
  
  public final void c()
  {
    ((DialerKeypad)a(R.id.dialpad)).setFeedback(null);
    an localan = b;
    if (localan != null) {
      localan.a();
    }
    b = null;
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "number");
    SelectionAwareEditText localSelectionAwareEditText = (SelectionAwareEditText)a(R.id.inputField);
    localSelectionAwareEditText.setText((CharSequence)paramString);
    paramString = localSelectionAwareEditText.getText();
    int i;
    if (paramString != null) {
      i = paramString.length();
    } else {
      i = 0;
    }
    localSelectionAwareEditText.setSelection(i);
    localSelectionAwareEditText.requestFocus();
    localSelectionAwareEditText.setInputType(524289);
    localSelectionAwareEditText.setTextIsSelectable(true);
  }
  
  public final void d()
  {
    ((DialerKeypad)a(R.id.dialpad)).a();
  }
  
  public final void d(final String paramString)
  {
    k.b(paramString, "number");
    Context localContext = c.getContext();
    if (localContext == null) {
      return;
    }
    new AlertDialog.Builder(localContext).setMessage((CharSequence)localContext.getString(2131886590, new Object[] { paramString })).setNegativeButton(2131886591, (DialogInterface.OnClickListener)new f(this, paramString)).setPositiveButton(2131886592, (DialogInterface.OnClickListener)new g(this, paramString)).setOnCancelListener((DialogInterface.OnCancelListener)new h(this, paramString)).show();
  }
  
  public static final class a
    implements TextWatcher
  {
    a(ao paramao) {}
    
    public final void afterTextChanged(Editable paramEditable)
    {
      k.b(paramEditable, "s");
      a.a.a((ap)new w(paramEditable));
    }
    
    public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
    
    public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  }
  
  static final class b
    implements View.OnClickListener
  {
    b(ao paramao) {}
    
    public final void onClick(View paramView)
    {
      a.a.a();
    }
  }
  
  static final class c
    implements View.OnLongClickListener
  {
    c(ao paramao) {}
    
    public final boolean onLongClick(View paramView)
    {
      a.a.d();
      return true;
    }
  }
  
  static final class d
    extends AsyncTask<Void, Void, bo>
  {
    private final String a;
    private final c.g.a.b<TextWatcher, x> b;
    
    public d(String paramString, c.g.a.b<? super TextWatcher, x> paramb)
    {
      a = paramString;
      b = paramb;
    }
  }
  
  static final class e
    extends l
    implements c.g.a.b<TextWatcher, x>
  {
    e(ao paramao)
    {
      super();
    }
  }
  
  static final class f
    implements DialogInterface.OnClickListener
  {
    f(ao paramao, String paramString) {}
    
    public final void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
      a.a.c(paramString);
    }
  }
  
  static final class g
    implements DialogInterface.OnClickListener
  {
    g(ao paramao, String paramString) {}
    
    public final void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
      a.a.a(paramString);
    }
  }
  
  static final class h
    implements DialogInterface.OnCancelListener
  {
    h(ao paramao, String paramString) {}
    
    public final void onCancel(DialogInterface paramDialogInterface)
    {
      a.a.b(paramString);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.ao
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */