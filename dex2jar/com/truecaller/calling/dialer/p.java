package com.truecaller.calling.dialer;

import com.truecaller.common.h.aj;
import com.truecaller.data.entity.g;
import com.truecaller.flashsdk.core.i;
import com.truecaller.multisim.ae;
import com.truecaller.network.search.e;
import com.truecaller.util.af;
import com.truecaller.util.cf;
import com.truecaller.utils.n;
import com.truecaller.voip.ai;
import dagger.a.d;
import javax.inject.Provider;

public final class p
  implements d<o>
{
  private final Provider<i.a> a;
  private final Provider<com.truecaller.search.local.model.c> b;
  private final Provider<a> c;
  private final Provider<bd> d;
  private final Provider<com.truecaller.analytics.b> e;
  private final Provider<i> f;
  private final Provider<com.truecaller.flashsdk.core.b> g;
  private final Provider<com.truecaller.i.c> h;
  private final Provider<n> i;
  private final Provider<ax> j;
  private final Provider<g> k;
  private final Provider<af> l;
  private final Provider<ae> m;
  private final Provider<com.truecaller.multisim.h> n;
  private final Provider<aj> o;
  private final Provider<com.truecaller.calling.recorder.h> p;
  private final Provider<e> q;
  private final Provider<cf> r;
  private final Provider<ai> s;
  
  private p(Provider<i.a> paramProvider, Provider<com.truecaller.search.local.model.c> paramProvider1, Provider<a> paramProvider2, Provider<bd> paramProvider3, Provider<com.truecaller.analytics.b> paramProvider4, Provider<i> paramProvider5, Provider<com.truecaller.flashsdk.core.b> paramProvider6, Provider<com.truecaller.i.c> paramProvider7, Provider<n> paramProvider8, Provider<ax> paramProvider9, Provider<g> paramProvider10, Provider<af> paramProvider11, Provider<ae> paramProvider12, Provider<com.truecaller.multisim.h> paramProvider13, Provider<aj> paramProvider14, Provider<com.truecaller.calling.recorder.h> paramProvider15, Provider<e> paramProvider16, Provider<cf> paramProvider17, Provider<ai> paramProvider18)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
    k = paramProvider10;
    l = paramProvider11;
    m = paramProvider12;
    n = paramProvider13;
    o = paramProvider14;
    p = paramProvider15;
    q = paramProvider16;
    r = paramProvider17;
    s = paramProvider18;
  }
  
  public static p a(Provider<i.a> paramProvider, Provider<com.truecaller.search.local.model.c> paramProvider1, Provider<a> paramProvider2, Provider<bd> paramProvider3, Provider<com.truecaller.analytics.b> paramProvider4, Provider<i> paramProvider5, Provider<com.truecaller.flashsdk.core.b> paramProvider6, Provider<com.truecaller.i.c> paramProvider7, Provider<n> paramProvider8, Provider<ax> paramProvider9, Provider<g> paramProvider10, Provider<af> paramProvider11, Provider<ae> paramProvider12, Provider<com.truecaller.multisim.h> paramProvider13, Provider<aj> paramProvider14, Provider<com.truecaller.calling.recorder.h> paramProvider15, Provider<e> paramProvider16, Provider<cf> paramProvider17, Provider<ai> paramProvider18)
  {
    return new p(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10, paramProvider11, paramProvider12, paramProvider13, paramProvider14, paramProvider15, paramProvider16, paramProvider17, paramProvider18);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */