package com.truecaller.calling.dialer;

public enum HistoryEventsScope
{
  static
  {
    HistoryEventsScope localHistoryEventsScope1 = new HistoryEventsScope("ONLY_CALL_EVENTS", 0);
    ONLY_CALL_EVENTS = localHistoryEventsScope1;
    HistoryEventsScope localHistoryEventsScope2 = new HistoryEventsScope("ONLY_FLASH_EVENTS", 1);
    ONLY_FLASH_EVENTS = localHistoryEventsScope2;
    HistoryEventsScope localHistoryEventsScope3 = new HistoryEventsScope("CALL_AND_FLASH_EVENTS", 2);
    CALL_AND_FLASH_EVENTS = localHistoryEventsScope3;
    $VALUES = new HistoryEventsScope[] { localHistoryEventsScope1, localHistoryEventsScope2, localHistoryEventsScope3 };
  }
  
  private HistoryEventsScope() {}
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.HistoryEventsScope
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */