package com.truecaller.calling.dialer;

import c.d.a.a;
import c.d.b.a.f;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.data.entity.Contact;
import com.truecaller.network.search.j;
import com.truecaller.network.search.l;
import com.truecaller.network.search.n;
import java.io.IOException;
import java.util.UUID;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ar;

@f(b="SearchPerformer.kt", c={59}, d="invokeSuspend", e="com.truecaller.calling.dialer.SearchPerformerImpl$backendThrottledSearch$2")
final class bs$a
  extends c.d.b.a.k
  implements m<ag, c<? super Contact>, Object>
{
  int a;
  private ag e;
  
  bs$a(bs parambs, long paramLong, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new a(b, c, d, paramc);
    e = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    switch (a)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 1: 
      if ((paramObject instanceof o.b)) {
        throw a;
      }
      break;
    case 0: 
      if ((paramObject instanceof o.b)) {
        break label162;
      }
      long l = c;
      a = 1;
      if (ar.a(l, this) == localObject) {
        return localObject;
      }
      break;
    }
    paramObject = null;
    label162:
    try
    {
      localObject = b.b;
      UUID localUUID = UUID.randomUUID();
      c.g.b.k.a(localUUID, "UUID.randomUUID()");
      localObject = ((l)localObject).a(localUUID, "dialpad").b().a(d).d().a().a(4).f();
      if (localObject != null) {
        paramObject = ((n)localObject).a();
      }
      return paramObject;
    }
    catch (IOException paramObject) {}
    throw a;
    return null;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((a)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bs.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */