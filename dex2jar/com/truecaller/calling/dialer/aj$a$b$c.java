package com.truecaller.calling.dialer;

import c.g.b.k;

public final class aj$a$b$c
  extends aj.a.b
{
  final String a;
  
  public aj$a$b$c(String paramString)
  {
    super((byte)0);
    a = paramString;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject) {
      if ((paramObject instanceof c))
      {
        paramObject = (c)paramObject;
        if (k.a(a, a)) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    String str = a;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("TapToPaste(number=");
    localStringBuilder.append(a);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.aj.a.b.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */