package com.truecaller.calling.dialer;

import com.truecaller.analytics.au;
import com.truecaller.calling.ar;
import com.truecaller.filters.FilterManager;
import com.truecaller.messaging.j;
import com.truecaller.multisim.h;
import com.truecaller.util.al;
import com.truecaller.utils.n;
import javax.inject.Provider;

public final class ah
  implements dagger.a.d<af>
{
  private final Provider<com.truecaller.calling.dialer.suggested_contacts.f> a;
  private final Provider<com.truecaller.calling.e.e> b;
  private final Provider<n> c;
  private final Provider<al> d;
  private final Provider<com.truecaller.i.c> e;
  private final Provider<g> f;
  private final Provider<br> g;
  private final Provider<com.truecaller.data.access.c> h;
  private final Provider<FilterManager> i;
  private final Provider<bu> j;
  private final Provider<com.truecaller.androidactors.f<com.truecaller.callhistory.a>> k;
  private final Provider<au> l;
  private final Provider<com.truecaller.analytics.b> m;
  private final Provider<h> n;
  private final Provider<com.truecaller.flashsdk.core.b> o;
  private final Provider<com.truecaller.androidactors.f<com.truecaller.tag.c>> p;
  private final Provider<c.d.f> q;
  private final Provider<c.d.f> r;
  private final Provider<com.truecaller.ads.provider.e> s;
  private final Provider<com.truecaller.search.local.model.c> t;
  private final Provider<ar> u;
  private final Provider<com.truecaller.i.a> v;
  private final Provider<j> w;
  private final Provider<com.truecaller.voip.d> x;
  private final Provider<com.truecaller.calling.dialer.a.b> y;
  private final Provider<com.truecaller.featuretoggles.e> z;
  
  private ah(Provider<com.truecaller.calling.dialer.suggested_contacts.f> paramProvider, Provider<com.truecaller.calling.e.e> paramProvider1, Provider<n> paramProvider2, Provider<al> paramProvider3, Provider<com.truecaller.i.c> paramProvider4, Provider<g> paramProvider5, Provider<br> paramProvider6, Provider<com.truecaller.data.access.c> paramProvider7, Provider<FilterManager> paramProvider8, Provider<bu> paramProvider9, Provider<com.truecaller.androidactors.f<com.truecaller.callhistory.a>> paramProvider10, Provider<au> paramProvider11, Provider<com.truecaller.analytics.b> paramProvider12, Provider<h> paramProvider13, Provider<com.truecaller.flashsdk.core.b> paramProvider14, Provider<com.truecaller.androidactors.f<com.truecaller.tag.c>> paramProvider15, Provider<c.d.f> paramProvider16, Provider<c.d.f> paramProvider17, Provider<com.truecaller.ads.provider.e> paramProvider18, Provider<com.truecaller.search.local.model.c> paramProvider19, Provider<ar> paramProvider20, Provider<com.truecaller.i.a> paramProvider21, Provider<j> paramProvider22, Provider<com.truecaller.voip.d> paramProvider23, Provider<com.truecaller.calling.dialer.a.b> paramProvider24, Provider<com.truecaller.featuretoggles.e> paramProvider25)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
    k = paramProvider10;
    l = paramProvider11;
    m = paramProvider12;
    n = paramProvider13;
    o = paramProvider14;
    p = paramProvider15;
    q = paramProvider16;
    r = paramProvider17;
    s = paramProvider18;
    t = paramProvider19;
    u = paramProvider20;
    v = paramProvider21;
    w = paramProvider22;
    x = paramProvider23;
    y = paramProvider24;
    z = paramProvider25;
  }
  
  public static ah a(Provider<com.truecaller.calling.dialer.suggested_contacts.f> paramProvider, Provider<com.truecaller.calling.e.e> paramProvider1, Provider<n> paramProvider2, Provider<al> paramProvider3, Provider<com.truecaller.i.c> paramProvider4, Provider<g> paramProvider5, Provider<br> paramProvider6, Provider<com.truecaller.data.access.c> paramProvider7, Provider<FilterManager> paramProvider8, Provider<bu> paramProvider9, Provider<com.truecaller.androidactors.f<com.truecaller.callhistory.a>> paramProvider10, Provider<au> paramProvider11, Provider<com.truecaller.analytics.b> paramProvider12, Provider<h> paramProvider13, Provider<com.truecaller.flashsdk.core.b> paramProvider14, Provider<com.truecaller.androidactors.f<com.truecaller.tag.c>> paramProvider15, Provider<c.d.f> paramProvider16, Provider<c.d.f> paramProvider17, Provider<com.truecaller.ads.provider.e> paramProvider18, Provider<com.truecaller.search.local.model.c> paramProvider19, Provider<ar> paramProvider20, Provider<com.truecaller.i.a> paramProvider21, Provider<j> paramProvider22, Provider<com.truecaller.voip.d> paramProvider23, Provider<com.truecaller.calling.dialer.a.b> paramProvider24, Provider<com.truecaller.featuretoggles.e> paramProvider25)
  {
    return new ah(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10, paramProvider11, paramProvider12, paramProvider13, paramProvider14, paramProvider15, paramProvider16, paramProvider17, paramProvider18, paramProvider19, paramProvider20, paramProvider21, paramProvider22, paramProvider23, paramProvider24, paramProvider25);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.ah
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */