package com.truecaller.calling.dialer;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import com.truecaller.calling.ActionType;
import com.truecaller.calling.aa;
import com.truecaller.calling.ac;
import com.truecaller.calling.ad;
import com.truecaller.calling.an;
import com.truecaller.calling.ao;
import com.truecaller.calling.b;
import com.truecaller.calling.ba;
import com.truecaller.calling.bb;
import com.truecaller.calling.bc;
import com.truecaller.calling.bd;
import com.truecaller.calling.c;
import com.truecaller.calling.d;
import com.truecaller.calling.e;
import com.truecaller.calling.o;
import com.truecaller.calling.p;
import com.truecaller.calling.u;
import com.truecaller.calling.v;
import com.truecaller.calling.x;
import com.truecaller.search.local.model.c.a;

public final class bv
  extends RecyclerView.ViewHolder
  implements ac, an, ao, b, bb, bc, bd, c, bw.b, e, p, u, v
{
  private final View a;
  
  private bv(View paramView, com.truecaller.adapter_delegates.k paramk, ba paramba, o paramo, ad paramad)
  {
    super(paramView);
    b = paramo;
    c = paramo;
    d = paramo;
    e = paramo;
    f = paramba;
    g = paramba;
    h = paramba;
    i = new ad(paramView);
    j = paramad;
    k = new d(paramView);
    l = new com.truecaller.calling.i(paramView);
    m = new aa(paramView);
    a = paramView;
    paramView = a;
    paramba = (RecyclerView.ViewHolder)this;
    com.truecaller.adapter_delegates.i.a(paramView, paramk, paramba, null, 12);
    c.g.b.k.b(paramk, "eventReceiver");
    c.g.b.k.b(paramba, "holder");
    l.a(paramk, paramba);
    c.g.b.k.b(paramk, "eventReceiver");
    c.g.b.k.b(paramba, "holder");
    d.a(paramk, paramba);
    c.g.b.k.b(paramk, "eventReceiver");
    c.g.b.k.b(paramba, "holder");
    m.a(paramk, paramba);
  }
  
  public final void a(int paramInt)
  {
    e.a(paramInt);
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    j.a(paramInt1, paramInt2);
  }
  
  public final void a(ActionType paramActionType)
  {
    m.a(paramActionType);
  }
  
  public final void a(x paramx)
  {
    m.a(paramx);
  }
  
  public final void a(c.a parama)
  {
    k.a(parama);
  }
  
  public final void a(Object paramObject)
  {
    b.a(paramObject);
  }
  
  public final void a(boolean paramBoolean)
  {
    h.a(paramBoolean);
  }
  
  public final void a_(int paramInt1, int paramInt2)
  {
    g.a_(paramInt1, paramInt2);
  }
  
  public final void a_(boolean paramBoolean)
  {
    c.a_(paramBoolean);
  }
  
  public final void b(ActionType paramActionType)
  {
    l.b(paramActionType);
  }
  
  public final void b(boolean paramBoolean)
  {
    m.b(paramBoolean);
  }
  
  public final void b_(String paramString)
  {
    f.b_(paramString);
  }
  
  public final void c(boolean paramBoolean)
  {
    l.c(paramBoolean);
  }
  
  public final void d(boolean paramBoolean)
  {
    d.d(paramBoolean);
  }
  
  public final void e_(String paramString)
  {
    i.e_(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bv
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */