package com.truecaller.calling.dialer;

import c.g.b.k;
import c.n;
import com.truecaller.adapter_delegates.h;
import com.truecaller.analytics.e.a;
import com.truecaller.calling.ActionType;
import com.truecaller.calling.af;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.g;
import com.truecaller.flashsdk.core.i;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;

public final class bx
  extends com.truecaller.adapter_delegates.c<bw.b>
  implements bw.a
{
  private final bm.a c;
  private final com.truecaller.search.local.model.c d;
  private final bm.a e;
  private final bd f;
  private final com.truecaller.search.local.b.e g;
  private final com.truecaller.i.c h;
  private final i i;
  private final com.truecaller.analytics.b j;
  private final com.truecaller.flashsdk.core.b k;
  private final g l;
  
  @Inject
  public bx(@Named("DialerAvailabilityManager") com.truecaller.search.local.model.c paramc, bm.a parama, bd parambd, com.truecaller.search.local.b.e parame, com.truecaller.i.c paramc1, i parami, com.truecaller.analytics.b paramb, com.truecaller.flashsdk.core.b paramb1, g paramg)
  {
    d = paramc;
    e = parama;
    f = parambd;
    g = parame;
    h = paramc1;
    i = parami;
    j = paramb;
    k = paramb1;
    l = paramg;
    c = e;
  }
  
  private final n<String, bz> a()
  {
    return c.a((ca)this, b[0]);
  }
  
  private static List<String> a(Contact paramContact)
  {
    paramContact = paramContact.A();
    k.a(paramContact, "numbers");
    Object localObject1 = (Iterable)paramContact;
    paramContact = (Collection)new ArrayList();
    localObject1 = ((Iterable)localObject1).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      Object localObject2 = (Number)((Iterator)localObject1).next();
      k.a(localObject2, "it");
      localObject2 = ((Number)localObject2).a();
      if (localObject2 != null) {
        paramContact.add(localObject2);
      }
    }
    return (List)paramContact;
  }
  
  private final boolean a(int paramInt)
  {
    f.a(c(paramInt), DetailsFragment.SourceType.SearchResult, true, true, true);
    return true;
  }
  
  private final boolean b()
  {
    return k.a("call", h.a("callLogTapBehavior"));
  }
  
  private final boolean b(int paramInt)
  {
    f.a(c(paramInt), "dialpadSearchResult");
    return true;
  }
  
  private final Contact c(int paramInt)
  {
    Object localObject1 = (bz)ab;
    if ((localObject1 instanceof bz.a))
    {
      localObject2 = a.get(paramInt)).a;
      localObject1 = localObject2;
      if (((Contact)localObject2).A().isEmpty())
      {
        String str = ((Contact)localObject2).p();
        localObject1 = localObject2;
        if (str != null)
        {
          ((Contact)localObject2).a(l.b(new String[] { str }));
          localObject1 = localObject2;
        }
      }
    }
    else if ((localObject1 instanceof bz.c))
    {
      localObject1 = a;
    }
    else
    {
      localObject1 = null;
    }
    Object localObject2 = localObject1;
    if (localObject1 == null)
    {
      localObject2 = new Contact();
      ((Contact)localObject2).l((String)aa);
      ((Contact)localObject2).a(l.b(new String[] { (String)aa }));
    }
    return (Contact)localObject2;
  }
  
  public final boolean a(h paramh)
  {
    k.b(paramh, "event");
    Object localObject = a;
    int m;
    if (k.a(localObject, "ItemEvent.CLICKED"))
    {
      m = b;
      if (b()) {
        return b(m);
      }
      return a(m);
    }
    if (k.a(localObject, "ItemEvent.SWIPE_START")) {
      return true;
    }
    if ((!k.a(localObject, "ItemEvent.SWIPE_COMPLETED_FROM_START")) && (!k.a(localObject, ActionType.CELLULAR_CALL.getEventAction())))
    {
      if ((!k.a(localObject, "ItemEvent.SWIPE_COMPLETED_FROM_END")) && (!k.a(localObject, ActionType.SMS.getEventAction())))
      {
        if (k.a(localObject, ActionType.PROFILE.getEventAction())) {
          return a(b);
        }
        return false;
      }
      m = b;
      localObject = new e.a("ViewAction").a("Action", "message").a("Context", "dialpadSearchResult");
      paramh = j;
      localObject = ((e.a)localObject).a();
      k.a(localObject, "event.build()");
      paramh.a((com.truecaller.analytics.e)localObject);
      f.b(c(m), "dialpadSearchResult");
      return true;
    }
    return b(b);
  }
  
  public final int getItemCount()
  {
    bz localbz = (bz)ab;
    if ((localbz instanceof bz.a)) {
      return a.size();
    }
    if (k.a(localbz, bz.b.a)) {
      return 0;
    }
    if ((!(localbz instanceof bz.c)) && (!k.a(localbz, bz.d.a))) {
      throw new c.l();
    }
    return 1;
  }
  
  public final long getItemId(int paramInt)
  {
    Long localLong = c(paramInt).getId();
    if (localLong != null) {
      return localLong.longValue();
    }
    return -1L;
  }
  
  public static final class a
    implements com.truecaller.flashsdk.db.l
  {
    a(bx parambx, int paramInt) {}
    
    public final void a()
    {
      bx.a(a).F().a(b);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.dialer.bx
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */