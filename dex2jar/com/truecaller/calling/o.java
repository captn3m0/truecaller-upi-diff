package com.truecaller.calling;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import c.f;
import com.truecaller.adapter_delegates.i;
import com.truecaller.ui.view.ContactPhoto;
import com.truecaller.utils.extensions.t;

public final class o
  implements an, ao, az, e, n, p
{
  private final f b;
  private final f c;
  private final Object d;
  private final az e;
  
  public o(View paramView, Object paramObject, az paramaz)
  {
    d = paramObject;
    e = paramaz;
    b = t.a(paramView, 2131362554);
    c = t.a(paramView, 2131362547);
  }
  
  private final ContactPhoto a()
  {
    return (ContactPhoto)b.b();
  }
  
  public final void a(int paramInt)
  {
    a().setContactBadgeDrawable(paramInt);
  }
  
  public final void a(com.truecaller.adapter_delegates.k paramk, RecyclerView.ViewHolder paramViewHolder)
  {
    c.g.b.k.b(paramk, "eventReceiver");
    c.g.b.k.b(paramViewHolder, "holder");
    ContactPhoto localContactPhoto = a();
    c.g.b.k.a(localContactPhoto, "photo");
    i.a((View)localContactPhoto, paramk, paramViewHolder, ActionType.PROFILE.getEventAction(), "avatar");
  }
  
  public final void a(Object paramObject)
  {
    a().a(paramObject, d);
  }
  
  public final void a_(boolean paramBoolean)
  {
    View localView = (View)c.b();
    if (localView != null)
    {
      t.a(localView, paramBoolean);
      return;
    }
  }
  
  public final void b(boolean paramBoolean)
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
  }
  
  public final void b_(boolean paramBoolean)
  {
    a().setIsSpam(paramBoolean);
    az localaz = e;
    if (localaz != null)
    {
      localaz.b_(paramBoolean);
      return;
    }
  }
  
  public final void d(boolean paramBoolean)
  {
    ContactPhoto localContactPhoto = a();
    c.g.b.k.a(localContactPhoto, "photo");
    localContactPhoto.setClickable(paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */