package com.truecaller.calling.e;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.startup_dialogs.StartupDialogDismissReason;
import com.truecaller.startup_dialogs.fragments.a;
import java.util.HashMap;

public abstract class h
  extends a
  implements View.OnClickListener
{
  private HashMap a;
  
  public abstract int a();
  
  public View a(int paramInt)
  {
    if (a == null) {
      a = new HashMap();
    }
    View localView2 = (View)a.get(Integer.valueOf(paramInt));
    View localView1 = localView2;
    if (localView2 == null)
    {
      localView1 = getView();
      if (localView1 == null) {
        return null;
      }
      localView1 = localView1.findViewById(paramInt);
      a.put(Integer.valueOf(paramInt), localView1);
    }
    return localView1;
  }
  
  public abstract int b();
  
  public void e()
  {
    Bundle localBundle2 = getArguments();
    Bundle localBundle1 = localBundle2;
    if (localBundle2 == null) {
      localBundle1 = new Bundle();
    }
    localBundle1.putString("StartupDialogDismissReason", StartupDialogDismissReason.USER_PRESSED_DISMISS_BUTTON.name());
    setArguments(localBundle1);
    super.e();
  }
  
  public void f()
  {
    HashMap localHashMap = a;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = super.onCreateDialog(paramBundle);
    k.a(paramBundle, "super.onCreateDialog(savedInstanceState)");
    Window localWindow = paramBundle.getWindow();
    if (localWindow != null) {
      localWindow.requestFeature(1);
    }
    return paramBundle;
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    paramLayoutInflater = paramLayoutInflater.inflate(2131558580, paramViewGroup, false);
    paramBundle = (TextView)paramLayoutInflater.findViewById(2131362303);
    paramViewGroup = (View.OnClickListener)this;
    paramBundle.setOnClickListener(paramViewGroup);
    paramBundle.setText(a());
    paramBundle = (TextView)paramLayoutInflater.findViewById(2131362326);
    paramBundle.setOnClickListener(paramViewGroup);
    paramBundle.setText(b());
    ((TextView)paramLayoutInflater.findViewById(2131363718)).setText(2131887324);
    ((ImageView)paramLayoutInflater.findViewById(2131363301)).setImageResource(2131234708);
    return paramLayoutInflater;
  }
  
  public void onStart()
  {
    super.onStart();
    Object localObject = getDialog();
    if (localObject != null)
    {
      localObject = ((Dialog)localObject).getWindow();
      if (localObject != null)
      {
        ((Window)localObject).setBackgroundDrawable((Drawable)new ColorDrawable(0));
        ((Window)localObject).setDimAmount(0.0F);
        WindowManager.LayoutParams localLayoutParams = ((Window)localObject).getAttributes();
        width = -1;
        height = -2;
        gravity = 80;
        windowAnimations = 2131951952;
        ((Window)localObject).setAttributes(localLayoutParams);
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.e.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */