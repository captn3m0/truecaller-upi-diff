package com.truecaller.calling.e;

import com.truecaller.common.account.r;
import com.truecaller.featuretoggles.e;
import com.truecaller.i.c;
import com.truecaller.utils.l;
import javax.inject.Provider;

public final class g
  implements dagger.a.d<f>
{
  private final Provider<c> a;
  private final Provider<e> b;
  private final Provider<com.truecaller.utils.d> c;
  private final Provider<l> d;
  private final Provider<r> e;
  
  private g(Provider<c> paramProvider, Provider<e> paramProvider1, Provider<com.truecaller.utils.d> paramProvider2, Provider<l> paramProvider3, Provider<r> paramProvider4)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static g a(Provider<c> paramProvider, Provider<e> paramProvider1, Provider<com.truecaller.utils.d> paramProvider2, Provider<l> paramProvider3, Provider<r> paramProvider4)
  {
    return new g(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.e.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */