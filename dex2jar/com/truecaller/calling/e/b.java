package com.truecaller.calling.e;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.view.View;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.ui.SettingsFragment;
import com.truecaller.ui.SettingsFragment.SettingsViewType;
import java.util.HashMap;

public final class b
  extends h
{
  private final int a = 2131887219;
  private final int b = 2131887231;
  private final String c = "WhatsAppAvailable";
  private HashMap d;
  
  public final int a()
  {
    return a;
  }
  
  public final View a(int paramInt)
  {
    if (d == null) {
      d = new HashMap();
    }
    View localView2 = (View)d.get(Integer.valueOf(paramInt));
    View localView1 = localView2;
    if (localView2 == null)
    {
      localView1 = getView();
      if (localView1 == null) {
        return null;
      }
      localView1 = localView1.findViewById(paramInt);
      d.put(Integer.valueOf(paramInt), localView1);
    }
    return localView1;
  }
  
  public final int b()
  {
    return b;
  }
  
  public final String c()
  {
    return c;
  }
  
  public final void e()
  {
    f localf = getActivity();
    if (localf != null) {
      localf.startActivity(SettingsFragment.a((Context)localf, SettingsFragment.SettingsViewType.SETTINGS_GENERAL));
    }
    super.e();
  }
  
  public final void f()
  {
    HashMap localHashMap = d;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = TrueApp.y();
    k.a(paramBundle, "TrueApp.getApp()");
    paramBundle.a().cz();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.e.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */