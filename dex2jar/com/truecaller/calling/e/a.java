package com.truecaller.calling.e;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.notifications.g;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.update.c;
import com.truecaller.wizard.utils.PermissionPoller;
import com.truecaller.wizard.utils.PermissionPoller.Permission;
import java.util.HashMap;
import javax.inject.Inject;

public final class a
  extends h
{
  @Inject
  public g a;
  private final int b = 2131887232;
  private final int c = 2131887215;
  private final String d = "WhatsAppEnable";
  private HashMap e;
  
  public final int a()
  {
    return b;
  }
  
  public final View a(int paramInt)
  {
    if (e == null) {
      e = new HashMap();
    }
    View localView2 = (View)e.get(Integer.valueOf(paramInt));
    View localView1 = localView2;
    if (localView2 == null)
    {
      localView1 = getView();
      if (localView1 == null) {
        return null;
      }
      localView1 = localView1.findViewById(paramInt);
      e.put(Integer.valueOf(paramInt), localView1);
    }
    return localView1;
  }
  
  public final int b()
  {
    return c;
  }
  
  public final String c()
  {
    return d;
  }
  
  public final void d()
  {
    Object localObject2 = getActivity();
    Object localObject1 = localObject2;
    if (!(localObject2 instanceof TruecallerInit)) {
      localObject1 = null;
    }
    localObject1 = (TruecallerInit)localObject1;
    if (localObject1 == null) {
      return;
    }
    localObject2 = a;
    if (localObject2 == null) {
      k.a("notificationAccessRequester");
    }
    if (((g)localObject2).a((Context)localObject1, 2131887137)) {
      ((TruecallerInit)localObject1).e().a(PermissionPoller.Permission.NOTIFICATION_ACCESS);
    }
    super.d();
  }
  
  public final void f()
  {
    HashMap localHashMap = e;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = TrueApp.y();
    k.a(paramBundle, "TrueApp.getApp()");
    paramBundle.a().cz().a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.e.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */