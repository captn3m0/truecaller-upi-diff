package com.truecaller.calling.e;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import c.g.b.k;
import c.n.m;
import c.u;
import c.x;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.e;
import com.truecaller.util.w;
import com.truecaller.utils.extensions.c;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public final class i
{
  public static final void a(Activity paramActivity, Contact paramContact, String paramString1, String paramString2, String paramString3)
  {
    k.b(paramActivity, "activity");
    k.b(paramString1, "fallBackNumber");
    k.b(paramString2, "callType");
    k.b(paramString3, "analyticsContext");
    paramString3 = (Context)paramActivity;
    if (paramContact != null) {
      paramContact = paramContact.E();
    } else {
      paramContact = null;
    }
    paramContact = w.a(paramString3, paramContact);
    k.a(paramContact, "ContactUtil.getExternalA…ty, contact?.phonebookId)");
    paramString3 = (Iterable)paramContact;
    paramContact = (Collection)new ArrayList();
    paramString3 = paramString3.iterator();
    while (paramString3.hasNext())
    {
      localObject = paramString3.next();
      String str = d;
      k.a(str, "it.packageName");
      if (m.b((CharSequence)str, (CharSequence)"com.whatsapp")) {
        paramContact.add(localObject);
      }
    }
    Object localObject = ((Iterable)paramContact).iterator();
    while (((Iterator)localObject).hasNext())
    {
      paramString3 = ((Iterator)localObject).next();
      paramContact = c;
      k.a(paramContact, "it.actionIntent");
      paramContact = paramContact.getType();
      if (paramContact != null) {
        paramContact = Boolean.valueOf(m.a((CharSequence)paramContact, (CharSequence)paramString2, true));
      } else {
        paramContact = null;
      }
      if (c.a(paramContact))
      {
        paramContact = paramString3;
        break label241;
      }
    }
    paramContact = null;
    label241:
    paramContact = (e)paramContact;
    if (paramContact != null)
    {
      paramActivity.startActivity(c);
      paramContact = x.a;
      if (k.a(paramString2, "call"))
      {
        a(paramActivity, "Audio");
        return;
      }
      a(paramActivity, "Video");
      return;
    }
    paramActivity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://api.whatsapp.com/send?phone=".concat(String.valueOf(paramString1)))));
    paramContact = x.a;
    a(paramActivity, "AppOpen");
  }
  
  private static final void a(Activity paramActivity, String paramString)
  {
    paramActivity = paramActivity.getApplication();
    if (paramActivity != null)
    {
      paramActivity = ((TrueApp)paramActivity).s();
      k.a(paramActivity, "(activity.application as TrueApp).analytics");
      paramString = new e.a("ViewAction").a("Context", "callLog").a("Action", "WhatsApp").a("SubAction", paramString).a();
      k.a(paramString, "eventBuilder.build()");
      paramActivity.a(paramString);
      return;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.TrueApp");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.e.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */