package com.truecaller.calling.e;

import com.truecaller.common.account.r;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.i.c;
import com.truecaller.utils.d;
import com.truecaller.utils.l;
import javax.inject.Inject;

public final class f
  implements e
{
  private final c a;
  private final com.truecaller.featuretoggles.e b;
  private final d c;
  private final l d;
  private final r e;
  
  @Inject
  public f(c paramc, com.truecaller.featuretoggles.e parame, d paramd, l paraml, r paramr)
  {
    a = paramc;
    b = parame;
    c = paramd;
    d = paraml;
    e = paramr;
  }
  
  public final boolean a()
  {
    if (c.h() < 21) {
      return false;
    }
    com.truecaller.featuretoggles.e locale = b;
    if (!g.a(locale, com.truecaller.featuretoggles.e.a[19]).a()) {
      return false;
    }
    if (!c.c("com.whatsapp")) {
      return false;
    }
    return e.c();
  }
  
  public final boolean b()
  {
    if (!a()) {
      return false;
    }
    if (!d.d()) {
      return false;
    }
    return a.a("whatsAppCallsEnabled", true);
  }
  
  public final boolean c()
  {
    return a.b("whatsAppCallsDetected");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.e.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */