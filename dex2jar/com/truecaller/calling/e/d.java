package com.truecaller.calling.e;

import android.annotation.SuppressLint;
import c.g.b.k;
import com.truecaller.callhistory.a;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import javax.inject.Inject;

public final class d
  implements c
{
  private final a a;
  private final com.truecaller.i.c b;
  
  @Inject
  public d(a parama, com.truecaller.i.c paramc)
  {
    a = parama;
    b = paramc;
  }
  
  @SuppressLint({"NewApi"})
  public final void a(int paramInt, boolean paramBoolean, Contact paramContact)
  {
    k.b(paramContact, "contact");
    paramContact = new HistoryEvent(paramContact, paramInt);
    if (paramBoolean) {
      paramContact.n();
    }
    paramContact.b("com.whatsapp");
    paramContact.a(paramContact.a());
    a.a(paramContact);
    b.b("whatsAppCallsDetected", true);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.e.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */