package com.truecaller.calling;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import c.f;
import c.g.b.k;
import com.truecaller.calling.dialer.CallIconType;
import com.truecaller.common.h.j;
import com.truecaller.utils.extensions.p;
import com.truecaller.utils.extensions.t;
import com.truecaller.utils.ui.b;

public final class ap
  implements ag
{
  private final f b;
  private final f c;
  private final f d;
  private final f e;
  private final f f;
  private final f g;
  private final f h;
  private final f i;
  private final Drawable j;
  private final Drawable k;
  private final Drawable l;
  private final Drawable m;
  private final Drawable n;
  private final Drawable o;
  
  public ap(View paramView)
  {
    b = t.a(paramView, 2131364872);
    c = t.a(paramView, 2131362385);
    d = t.a(paramView, 2131362804);
    e = t.a(paramView, 2131364520);
    f = t.a(paramView, 2131365423);
    g = t.a(paramView, 2131363509);
    h = t.a(paramView, 2131363504);
    i = t.a(paramView, 2131362805);
    j = b.a(paramView.getContext(), 2131234205, 2130969533);
    k = b.a(paramView.getContext(), 2131234352, 2130969533);
    l = b.a(paramView.getContext(), 2131234275, 2130968889);
    m = b.a(paramView.getContext(), 2131234549, 2130969593);
    n = b.a(paramView.getContext(), 2131234550, 2130969593);
    o = b.a(paramView.getContext(), 2131234658, 2130969183);
  }
  
  private final ImageView a()
  {
    return (ImageView)c.b();
  }
  
  private final TextView b()
  {
    return (TextView)d.b();
  }
  
  private final ImageView c()
  {
    return (ImageView)e.b();
  }
  
  private final ImageView d()
  {
    return (ImageView)f.b();
  }
  
  private final TextView e()
  {
    return (TextView)h.b();
  }
  
  public final void a(CallIconType paramCallIconType)
  {
    if (paramCallIconType != null) {
      switch (aq.a[paramCallIconType.ordinal()])
      {
      default: 
        break;
      case 3: 
        paramCallIconType = l;
        break;
      case 2: 
        paramCallIconType = k;
        break;
      case 1: 
        paramCallIconType = j;
        break;
      }
    }
    paramCallIconType = null;
    a().setImageDrawable(paramCallIconType);
    Object localObject = a();
    k.a(localObject, "callTypeIcon");
    localObject = (View)localObject;
    boolean bool;
    if (paramCallIconType != null) {
      bool = true;
    } else {
      bool = false;
    }
    t.a((View)localObject, bool);
  }
  
  public final void a(Integer paramInteger)
  {
    boolean bool = true;
    if ((paramInteger != null) && (paramInteger.intValue() == 0)) {
      paramInteger = m;
    } else if ((paramInteger != null) && (paramInteger.intValue() == 1)) {
      paramInteger = n;
    } else {
      paramInteger = null;
    }
    c().setImageDrawable(paramInteger);
    Object localObject = c();
    k.a(localObject, "simIndicator");
    localObject = (View)localObject;
    if (paramInteger == null) {
      bool = false;
    }
    t.a((View)localObject, bool);
  }
  
  public final void a(Long paramLong)
  {
    Object localObject1 = e();
    k.a(localObject1, "callDurationView");
    Object localObject2 = e();
    k.a(localObject2, "callDurationView");
    localObject2 = ((TextView)localObject2).getContext();
    long l1;
    if (paramLong != null) {
      l1 = paramLong.longValue();
    } else {
      l1 = 0L;
    }
    ((TextView)localObject1).setText((CharSequence)j.c((Context)localObject2, l1));
    localObject1 = e();
    k.a(localObject1, "callDurationView");
    localObject1 = (View)localObject1;
    boolean bool2 = true;
    boolean bool1;
    if (paramLong != null) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    t.a((View)localObject1, bool1);
    localObject1 = (TextView)i.b();
    k.a(localObject1, "delimiterTwo");
    localObject1 = (View)localObject1;
    if (paramLong != null) {
      bool1 = bool2;
    } else {
      bool1 = false;
    }
    t.a((View)localObject1, bool1);
  }
  
  public final void a(boolean paramBoolean)
  {
    ImageView localImageView = d();
    k.a(localImageView, "videoCallIndicator");
    t.a((View)localImageView, paramBoolean);
    d().setImageDrawable(o);
  }
  
  public final void b(String paramString)
  {
    TextView localTextView = (TextView)b.b();
    k.a(localTextView, "timestampText");
    p.a(localTextView, paramString);
  }
  
  public final void b(boolean paramBoolean)
  {
    TextView localTextView = b();
    k.a(localTextView, "delimiter");
    t.a((View)localTextView, paramBoolean);
  }
  
  public final void e_(String paramString)
  {
    Object localObject = (TextView)g.b();
    k.a(localObject, "subtitle");
    p.a((TextView)localObject, paramString);
    localObject = b();
    k.a(localObject, "delimiter");
    localObject = (View)localObject;
    boolean bool;
    if (paramString != null) {
      bool = true;
    } else {
      bool = false;
    }
    t.a((View)localObject, bool);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.ap
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */