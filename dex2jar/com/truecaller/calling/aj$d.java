package com.truecaller.calling;

import android.os.Bundle;

public final class aj$d
  extends aj
{
  public aj$d(String paramString, long paramLong)
  {
    super(paramString, paramLong, (byte)0);
  }
  
  public final Bundle a()
  {
    Bundle localBundle = new Bundle();
    localBundle.putInt("CALL_STATE", 0);
    if (a != null) {
      localBundle.putString("NUMBER", a);
    }
    return localBundle;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.aj.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */