package com.truecaller.calling;

import android.view.View;
import android.widget.TextView;
import c.f;
import c.g.b.k;
import com.truecaller.utils.extensions.t;

public final class ad
  implements u, v
{
  private final f b;
  
  public ad(View paramView)
  {
    b = t.a(paramView, 2131363509);
  }
  
  private final TextView a()
  {
    return (TextView)b.b();
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    TextView localTextView = a();
    k.a(localTextView, "subtitle");
    ae.a(localTextView, paramInt1, paramInt2);
  }
  
  public final void e_(String paramString)
  {
    TextView localTextView = a();
    if (paramString != null)
    {
      k.a(localTextView, "this");
      t.a((View)localTextView);
      localTextView.setText((CharSequence)paramString);
      return;
    }
    k.a(localTextView, "this");
    t.b((View)localTextView);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.ad
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */