package com.truecaller.calling;

import c.a.ag;
import c.t;
import com.truecaller.ui.q.b;
import java.util.Map;

public final class m
  extends au
  implements q.b
{
  public static final a a = new a((byte)0);
  private final Map<Character, Integer> c = ag.a(t.a(Character.valueOf('★'), Integer.valueOf(2131234571)));
  private Integer d;
  
  public final int c()
  {
    Integer localInteger = d;
    if (localInteger != null) {
      return localInteger.intValue();
    }
    return 2131233691;
  }
  
  public final void c_(String paramString)
  {
    Map localMap = c;
    Object localObject2 = null;
    if (paramString != null) {
      localObject1 = c.n.m.f((CharSequence)paramString);
    } else {
      localObject1 = null;
    }
    Object localObject1 = (Integer)localMap.get(localObject1);
    if (localObject1 != null)
    {
      d = Integer.valueOf(((Number)localObject1).intValue());
      localObject1 = localObject2;
      if (paramString != null) {
        localObject1 = c.n.m.h(paramString);
      }
      b = ((String)localObject1);
      return;
    }
    localObject1 = (m)this;
    d = null;
    b = paramString;
  }
  
  public static final class a {}
}

/* Location:
 * Qualified Name:     com.truecaller.calling.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */