package com.truecaller.calling;

import c.g.b.k;
import java.util.List;

public final class x
{
  final List<Long> a;
  final String b;
  final String c;
  
  public x(List<Long> paramList, String paramString1, String paramString2)
  {
    a = paramList;
    b = paramString1;
    c = paramString2;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject) {
      if ((paramObject instanceof x))
      {
        paramObject = (x)paramObject;
        if ((k.a(a, a)) && (k.a(b, b)) && (k.a(c, c))) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    Object localObject = a;
    int k = 0;
    int i;
    if (localObject != null) {
      i = localObject.hashCode();
    } else {
      i = 0;
    }
    localObject = b;
    int j;
    if (localObject != null) {
      j = localObject.hashCode();
    } else {
      j = 0;
    }
    localObject = c;
    if (localObject != null) {
      k = localObject.hashCode();
    }
    return (i * 31 + j) * 31 + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("FlashBinding(phoneNumber=");
    localStringBuilder.append(a);
    localStringBuilder.append(", name=");
    localStringBuilder.append(b);
    localStringBuilder.append(", analyticsContext=");
    localStringBuilder.append(c);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.x
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */