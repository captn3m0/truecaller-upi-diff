package com.truecaller.calling;

import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;
import c.a.ab;
import c.g.b.k;
import c.g.b.l;
import c.n;
import c.t;
import com.truecaller.flashsdk.core.i;
import com.truecaller.search.local.b.a;
import com.truecaller.search.local.b.e;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class ae
{
  private static final int a(String paramString1, String paramString2)
  {
    return c.n.m.a((CharSequence)paramString2, paramString1, 0, false, 6);
  }
  
  private static final n<Integer, Integer> a(e parame, String paramString1, String paramString2, String paramString3, boolean paramBoolean1, boolean paramBoolean2)
  {
    paramString3 = Integer.valueOf(a(paramString2, paramString3));
    int i;
    if (((Number)paramString3).intValue() >= 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i == 0) {
      paramString3 = null;
    }
    if (paramString3 != null)
    {
      ((Number)paramString3).intValue();
      if (!parame.a(paramString1, paramString2, paramBoolean1, paramBoolean2)) {
        paramString3 = null;
      }
      if (paramString3 != null)
      {
        i = ((Number)paramString3).intValue();
        return t.a(Integer.valueOf(a + i), Integer.valueOf(b + i));
      }
    }
    return null;
  }
  
  static final String a(x paramx)
  {
    int i;
    if (((CharSequence)b).length() == 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0)
    {
      paramx = (Long)c.a.m.e(a);
      if (paramx != null) {
        return String.valueOf(paramx.longValue());
      }
      return null;
    }
    return b;
  }
  
  public static final void a(TextView paramTextView, int paramInt1, int paramInt2)
  {
    k.b(paramTextView, "field");
    SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder(paramTextView.getText());
    localSpannableStringBuilder.setSpan(new ForegroundColorSpan(com.truecaller.utils.ui.b.a(paramTextView.getContext(), 2130968888)), paramInt1, paramInt2, 33);
    paramTextView.setText((CharSequence)localSpannableStringBuilder);
  }
  
  public static final boolean a(bb parambb, e parame, String paramString1, String paramString2, String paramString3, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    k.b(parambb, "receiver$0");
    k.b(parame, "searchMatcher");
    k.b(paramString1, "pattern");
    k.b(paramString2, "originalValue");
    k.b(paramString3, "formattedValue");
    return a(parame, paramString1, paramString2, paramString3, paramBoolean1, paramBoolean2, paramBoolean3, (c.g.a.b)new a(parambb));
  }
  
  public static final boolean a(u paramu, e parame, String paramString1, String paramString2, String paramString3, boolean paramBoolean1, boolean paramBoolean2)
  {
    k.b(paramu, "receiver$0");
    k.b(parame, "searchMatcher");
    k.b(paramString1, "pattern");
    k.b(paramString2, "originalValue");
    k.b(paramString3, "formattedValue");
    return a(parame, paramString1, paramString2, paramString3, paramBoolean1, paramBoolean2, false, (c.g.a.b)new b(paramu));
  }
  
  public static final boolean a(i parami)
  {
    k.b(parami, "receiver$0");
    return parami.b(null);
  }
  
  private static final boolean a(e parame, String paramString1, String paramString2, String paramString3, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, c.g.a.b<? super n<Integer, Integer>, Boolean> paramb)
  {
    if ((!paramBoolean1) && (!paramBoolean2))
    {
      if (paramBoolean3) {
        parame = c.n.m.a((CharSequence)paramString1, new char[] { ' ' }, 0, 6);
      } else {
        parame = c.a.m.a(paramString1);
      }
      paramString1 = ((Iterable)parame).iterator();
      paramBoolean1 = false;
      while (paramString1.hasNext())
      {
        parame = b(paramString3, (String)paramString1.next());
        if (parame != null) {
          parame = (Boolean)paramb.invoke(parame);
        } else {
          parame = null;
        }
        if ((!com.truecaller.utils.extensions.c.a(parame)) && (!paramBoolean1)) {
          paramBoolean1 = false;
        } else {
          paramBoolean1 = true;
        }
      }
      return paramBoolean1;
    }
    if (paramBoolean3) {
      paramString1 = c.n.m.a((CharSequence)paramString1, new char[] { ' ' }, 0, 6);
    } else {
      paramString1 = c.a.m.a(paramString1);
    }
    Iterator localIterator = ((Iterable)paramString1).iterator();
    paramBoolean3 = false;
    while (localIterator.hasNext())
    {
      paramString1 = a(parame, (String)localIterator.next(), paramString2, paramString3, paramBoolean1, paramBoolean2);
      if (paramString1 != null) {
        paramString1 = (Boolean)paramb.invoke(paramString1);
      } else {
        paramString1 = null;
      }
      if ((!com.truecaller.utils.extensions.c.a(paramString1)) && (!paramBoolean3)) {
        paramBoolean3 = false;
      } else {
        paramBoolean3 = true;
      }
    }
    return paramBoolean3;
  }
  
  private static n<Integer, Integer> b(String paramString1, String paramString2)
  {
    k.b(paramString1, "receiver$0");
    k.b(paramString2, "what");
    paramString2 = (CharSequence)paramString2;
    Object localObject1 = (Collection)new ArrayList(paramString2.length());
    int i = 0;
    while (i < paramString2.length())
    {
      ((Collection)localObject1).add(Character.valueOf(a.a(paramString2.charAt(i))));
      i += 1;
    }
    localObject1 = (Iterable)localObject1;
    paramString2 = (Collection)new ArrayList();
    localObject1 = ((Iterable)localObject1).iterator();
    Object localObject2;
    char c;
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      i = 1;
      if (!bool) {
        break;
      }
      localObject2 = ((Iterator)localObject1).next();
      c = ((Character)localObject2).charValue();
      if ((com.truecaller.search.local.b.c.c(c)) || (com.truecaller.search.local.b.c.b(c))) {
        i = 0;
      }
      if (i != 0) {
        paramString2.add(localObject2);
      }
    }
    localObject1 = (Iterable)paramString2;
    paramString2 = (Collection)new ArrayList(c.a.m.a((Iterable)localObject1, 10));
    localObject1 = ((Iterable)localObject1).iterator();
    while (((Iterator)localObject1).hasNext()) {
      paramString2.add(Character.valueOf(Character.toLowerCase(((Character)((Iterator)localObject1).next()).charValue())));
    }
    paramString2 = (List)paramString2;
    if (paramString2.isEmpty()) {
      return null;
    }
    paramString1 = (CharSequence)paramString1;
    localObject1 = (Collection)new ArrayList(paramString1.length());
    i = 0;
    while (i < paramString1.length())
    {
      ((Collection)localObject1).add(Character.valueOf(a.a(paramString1.charAt(i))));
      i += 1;
    }
    localObject1 = (Iterable)localObject1;
    paramString1 = (Collection)new ArrayList(c.a.m.a((Iterable)localObject1, 10));
    localObject1 = ((Iterable)localObject1).iterator();
    while (((Iterator)localObject1).hasNext()) {
      paramString1.add(Character.valueOf(Character.toLowerCase(((Character)((Iterator)localObject1).next()).charValue())));
    }
    localObject1 = c.a.m.j((Iterable)paramString1);
    paramString1 = (Collection)new ArrayList();
    localObject1 = ((Iterable)localObject1).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = ((Iterator)localObject1).next();
      c = ((Character)b).charValue();
      if ((!com.truecaller.search.local.b.c.c(c)) && (!com.truecaller.search.local.b.c.b(c))) {
        i = 1;
      } else {
        i = 0;
      }
      if (i != 0) {
        paramString1.add(localObject2);
      }
    }
    paramString1 = c.a.m.f((Iterable)paramString1, paramString2.size());
    if (paramString1.isEmpty()) {
      return null;
    }
    localObject1 = ((Iterable)paramString1).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      paramString1 = ((Iterator)localObject1).next();
      localObject2 = (List)paramString1;
      localObject2 = (Iterable)c.a.m.d((Iterable)paramString2, (Iterable)localObject2);
      if (!((Collection)localObject2).isEmpty())
      {
        localObject2 = ((Iterable)localObject2).iterator();
        while (((Iterator)localObject2).hasNext())
        {
          n localn = (n)((Iterator)localObject2).next();
          if (((Character)a).charValue() == ((Character)b).b).charValue()) {
            i = 1;
          } else {
            i = 0;
          }
          if (i == 0)
          {
            i = 0;
            break label680;
          }
        }
      }
      i = 1;
      label680:
      if (i != 0) {
        break label689;
      }
    }
    paramString1 = null;
    label689:
    paramString1 = (List)paramString1;
    if (paramString1 != null) {
      return t.a(Integer.valueOf(da), Integer.valueOf(fa + 1));
    }
    return null;
  }
  
  static final class a
    extends l
    implements c.g.a.b<n<? extends Integer, ? extends Integer>, Boolean>
  {
    a(bb parambb)
    {
      super();
    }
  }
  
  static final class b
    extends l
    implements c.g.a.b<n<? extends Integer, ? extends Integer>, Boolean>
  {
    b(u paramu)
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.ae
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */