package com.truecaller.calling.d;

import android.content.Context;
import android.net.Uri;
import c.d.a.a;
import c.d.b.a.f;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.TrueApp;
import com.truecaller.utils.extensions.r;
import kotlinx.coroutines.ag;

@f(b="SpeedDialOptionsDialog.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.speeddial.SpeedDialOptionsDialog$fetchNumber$2")
final class n$b
  extends c.d.b.a.k
  implements m<ag, c<? super String>, Object>
{
  int a;
  private ag c;
  
  n$b(Uri paramUri, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new b(b, paramc);
    c = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    if (a == 0)
    {
      if (!(paramObject instanceof o.b))
      {
        paramObject = b;
        if (paramObject != null)
        {
          localObject = TrueApp.x();
          c.g.b.k.a(localObject, "TrueApp.getAppContext()");
          return r.b((Uri)paramObject, (Context)localObject);
        }
        return null;
      }
      throw a;
    }
    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((b)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.n.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */