package com.truecaller.calling.d;

import android.content.Intent;
import android.net.Uri;
import c.d.a.a;
import c.d.b.a.f;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

@f(b="SpeedDialOptionsDialog.kt", c={87}, d="invokeSuspend", e="com.truecaller.calling.speeddial.SpeedDialOptionsDialog$onActivityResult$1")
final class n$c
  extends c.d.b.a.k
  implements m<ag, c<? super x>, Object>
{
  Object a;
  int b;
  private ag f;
  
  n$c(n paramn, Intent paramIntent, int paramInt, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new c(c, d, e, paramc);
    f = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    switch (b)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 1: 
      if ((paramObject instanceof o.b)) {
        throw a;
      }
      break;
    case 0: 
      if ((paramObject instanceof o.b)) {
        break label175;
      }
      paramObject = d;
      int i;
      if (e == -1) {
        i = 1;
      } else {
        i = 0;
      }
      if (i == 0) {
        paramObject = null;
      }
      if (paramObject == null) {
        break label171;
      }
      Object localObject = c;
      Uri localUri = ((Intent)paramObject).getData();
      a = paramObject;
      b = 1;
      localObject = g.a(a, (m)new n.b(localUri, null), this);
      paramObject = localObject;
      if (localObject == locala) {
        return locala;
      }
      break;
    }
    paramObject = (String)paramObject;
    if (paramObject != null) {
      n.a(c, (String)paramObject);
    }
    label171:
    return x.a;
    label175:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((c)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.n.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */