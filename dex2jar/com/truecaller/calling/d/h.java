package com.truecaller.calling.d;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import c.f;
import com.truecaller.adapter_delegates.i;
import com.truecaller.utils.extensions.t;
import com.truecaller.utils.ui.b;

public final class h
  extends RecyclerView.ViewHolder
  implements i.c
{
  private final f b;
  private final f c;
  private final f d;
  private final int e;
  private final int f;
  private final View g;
  
  public h(View paramView, com.truecaller.adapter_delegates.k paramk)
  {
    super(paramView);
    g = paramView;
    b = t.a(g, 2131364884);
    c = t.a(g, 2131363578);
    d = t.a(g, 2131362932);
    e = b.a(g.getContext(), 2130969591);
    f = b.a(g.getContext(), 2130969592);
    i.a(g, paramk, (RecyclerView.ViewHolder)this, null, 12);
  }
  
  private final TextView a()
  {
    return (TextView)b.b();
  }
  
  private final TextView b()
  {
    return (TextView)c.b();
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "title");
    TextView localTextView = a();
    c.g.b.k.a(localTextView, "this.title");
    localTextView.setText((CharSequence)paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    g.setClickable(paramBoolean);
    View localView = (View)d.b();
    c.g.b.k.a(localView, "this.editIcon");
    t.a(localView, paramBoolean);
  }
  
  public final void b(String paramString)
  {
    if (paramString != null)
    {
      TextView localTextView = b();
      c.g.b.k.a(localTextView, "this.label");
      localTextView.setText((CharSequence)paramString);
      paramString = b();
      c.g.b.k.a(paramString, "this.label");
      t.a((View)paramString);
      return;
    }
    paramString = ((h)this).b();
    c.g.b.k.a(paramString, "this.label");
    t.b((View)paramString);
  }
  
  public final void b(boolean paramBoolean)
  {
    TextView localTextView = a();
    int i;
    if (paramBoolean) {
      i = f;
    } else {
      i = e;
    }
    localTextView.setTextColor(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */