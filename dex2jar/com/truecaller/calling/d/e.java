package com.truecaller.calling.d;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.support.v4.view.r;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.State;
import android.view.View;
import c.g.b.k;
import com.truecaller.util.at;
import com.truecaller.utils.ui.b;

public final class e
  extends RecyclerView.ItemDecoration
{
  private final Paint a = new Paint(1);
  private final Paint b = new Paint(1);
  private final Rect c = new Rect();
  private final int d;
  private final int e;
  
  public e(Context paramContext)
  {
    a.setStyle(Paint.Style.STROKE);
    a.setStrokeWidth(at.a(paramContext, 1.0F));
    a.setColor(-7693147);
    b.setTextAlign(Paint.Align.CENTER);
    b.setTextSize(at.a(paramContext, 20.0F));
    b.setColor(b.a(paramContext, 2130969592));
    d = at.a(paramContext, 20.0F);
    e = paramContext.getResources().getDimensionPixelSize(2131165483);
  }
  
  public final void onDrawOver(Canvas paramCanvas, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    k.b(paramCanvas, "c");
    k.b(paramRecyclerView, "parent");
    k.b(paramState, "state");
    int m = paramRecyclerView.getChildCount();
    int i = 0;
    while (i < m)
    {
      paramState = paramRecyclerView.getChildAt(i);
      int n = paramRecyclerView.getChildAdapterPosition(paramState);
      int j;
      if (r.g((View)paramRecyclerView) == 1) {
        j = 1;
      } else {
        j = 0;
      }
      int k;
      if (j != 0)
      {
        k.a(paramState, "child");
        k = paramState.getRight() - e / 2;
      }
      else
      {
        k.a(paramState, "child");
        k = paramState.getLeft() + e / 2;
      }
      paramCanvas.drawCircle(k, paramState.getY() + paramState.getHeight() / 2, d, a);
      String str = String.valueOf(n + 1);
      b.getTextBounds(str, 0, 1, c);
      if (j != 0) {
        j = paramState.getRight() - e / 2;
      } else {
        j = paramState.getLeft() + e / 2;
      }
      paramCanvas.drawText(str, j, paramState.getY() + paramState.getHeight() / 2 + c.height() / 2, b);
      i += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */