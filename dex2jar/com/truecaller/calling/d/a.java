package com.truecaller.calling.d;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.PhoneNumberUtils;
import android.view.View;
import c.g.b.k;
import c.n.m;
import com.truecaller.common.h.ab;
import com.truecaller.ui.dialogs.e;
import java.io.Serializable;
import java.util.HashMap;

public final class a
  extends e
{
  public static final a a = new a((byte)0);
  private HashMap c;
  
  public final View a(int paramInt)
  {
    if (c == null) {
      c = new HashMap();
    }
    View localView2 = (View)c.get(Integer.valueOf(paramInt));
    View localView1 = localView2;
    if (localView2 == null)
    {
      localView1 = getView();
      if (localView1 == null) {
        return null;
      }
      localView1 = localView1.findViewById(paramInt);
      c.put(Integer.valueOf(paramInt), localView1);
    }
    return localView1;
  }
  
  public final void b()
  {
    HashMap localHashMap = c;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void b(int paramInt)
  {
    super.b(paramInt);
    if (paramInt == -1)
    {
      Object localObject1 = PhoneNumberUtils.stripSeparators(a());
      boolean bool = ab.a((CharSequence)localObject1);
      paramInt = 1;
      Integer localInteger = null;
      if ((bool ^ true)) {
        localObject1 = null;
      }
      if (localObject1 != null)
      {
        Object localObject2 = m.a((String)localObject1, "N", "");
        if (localObject2 != null)
        {
          if ((m.a((CharSequence)localObject2) ^ true))
          {
            localObject1 = getArguments();
            if (localObject1 != null) {
              localObject1 = ((Bundle)localObject1).getString("initial_text");
            } else {
              localObject1 = null;
            }
            if ((k.a(localObject2, localObject1) ^ true)) {}
          }
          else
          {
            paramInt = 0;
          }
          if (paramInt != 0) {
            localObject1 = localObject2;
          } else {
            localObject1 = null;
          }
          if (localObject1 != null)
          {
            localObject2 = new Intent();
            Bundle localBundle = getArguments();
            if (localBundle != null) {
              localInteger = Integer.valueOf(localBundle.getInt("speed_dial_key"));
            }
            ((Intent)localObject2).putExtra("speed_dial_key", (Serializable)localInteger);
            ((Intent)localObject2).putExtra("speed_dial_value", (String)localObject1);
            a(-1, (Intent)localObject2);
            return;
          }
        }
      }
    }
  }
  
  public static final class a {}
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */