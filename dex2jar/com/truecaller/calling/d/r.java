package com.truecaller.calling.d;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemAnimator;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.View;
import c.g.b.k;
import c.g.b.l;
import com.truecaller.adapter_delegates.a;
import com.truecaller.adapter_delegates.p;
import com.truecaller.utils.extensions.t;

public final class r
  implements l.c
{
  private final c.f b;
  private final p<h, h> c;
  private final com.truecaller.adapter_delegates.f d;
  
  public r(i.b paramb, View paramView)
  {
    b = t.a(paramView, 2131364092);
    c = new p((com.truecaller.adapter_delegates.b)paramb, 2131559037, (c.g.a.b)new a(this), (c.g.a.b)b.a);
    paramb = new com.truecaller.adapter_delegates.f((a)c);
    paramb.setHasStableIds(true);
    d = paramb;
    RecyclerView localRecyclerView = (RecyclerView)b.b();
    localRecyclerView.setAdapter((RecyclerView.Adapter)d);
    localRecyclerView.setLayoutManager((RecyclerView.LayoutManager)new LinearLayoutManager(paramView.getContext()));
    RecyclerView.ItemAnimator localItemAnimator = localRecyclerView.getItemAnimator();
    paramb = localItemAnimator;
    if (!(localItemAnimator instanceof SimpleItemAnimator)) {
      paramb = null;
    }
    paramb = (SimpleItemAnimator)paramb;
    if (paramb != null) {
      paramb.setSupportsChangeAnimations(false);
    }
    paramb = paramView.getContext();
    k.a(paramb, "view.context");
    localRecyclerView.addItemDecoration((RecyclerView.ItemDecoration)new e(paramb));
  }
  
  public final void a(int paramInt)
  {
    d.notifyItemChanged(c.a_(paramInt));
  }
  
  static final class a
    extends l
    implements c.g.a.b<View, h>
  {
    a(r paramr)
    {
      super();
    }
  }
  
  static final class b
    extends l
    implements c.g.a.b<h, h>
  {
    public static final b a = new b();
    
    b()
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.r
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */