package com.truecaller.calling.d;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import c.g.a.b;
import c.g.b.k;
import c.g.b.l;
import c.u;
import c.x;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.ui.o;
import java.util.HashMap;
import javax.inject.Inject;

public final class f
  extends o
  implements l.b
{
  @Inject
  public l.a a;
  @Inject
  public i.b b;
  private HashMap c;
  
  public final void a(int paramInt, String paramString)
  {
    m.a((Fragment)this, paramInt, paramString, false);
  }
  
  public final void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
    paramBundle = getActivity();
    if (paramBundle != null)
    {
      paramBundle.setTitle(2131887167);
      return;
    }
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (!m.a(paramInt1, paramInt2, paramIntent, (b)new a(this))) {
      super.onActivityResult(paramInt1, paramInt2, paramIntent);
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getContext();
    if (paramBundle != null) {
      paramBundle = paramBundle.getApplicationContext();
    } else {
      paramBundle = null;
    }
    if (paramBundle != null)
    {
      ((bk)paramBundle).a().cu().a(this);
      paramBundle = a;
      if (paramBundle == null) {
        k.a("speedDialPresenter");
      }
      paramBundle.b(this);
      return;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.GraphHolder");
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131559228, paramViewGroup, false);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    paramBundle = a;
    if (paramBundle == null) {
      k.a("speedDialPresenter");
    }
    i.b localb = b;
    if (localb == null) {
      k.a("speedDialItemsPresenter");
    }
    paramBundle.a(new r(localb, paramView));
  }
  
  static final class a
    extends l
    implements b<Integer, x>
  {
    a(f paramf)
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */