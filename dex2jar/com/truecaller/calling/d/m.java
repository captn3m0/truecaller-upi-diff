package com.truecaller.calling.d;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import c.g.a.b;
import c.g.b.k;
import c.k.h;
import c.x;
import com.truecaller.ui.SingleActivity;
import com.truecaller.ui.SingleActivity.FragmentSingle;
import com.truecaller.ui.dialogs.a;

public final class m
{
  public static final x a(Activity paramActivity)
  {
    if (paramActivity != null)
    {
      paramActivity.startActivity(SingleActivity.a((Context)paramActivity, SingleActivity.FragmentSingle.SPEED_DIAL));
      return x.a;
    }
    return null;
  }
  
  public static final void a(Fragment paramFragment, int paramInt, String paramString, boolean paramBoolean)
  {
    k.b(paramFragment, "fragment");
    f localf = paramFragment.getActivity();
    if (localf == null) {
      return;
    }
    k.a(localf, "fragment.activity ?: return");
    n.a locala = n.b;
    paramString = n.a.a(paramInt, paramString, paramBoolean);
    paramString.setTargetFragment(paramFragment, 1001);
    a.a(paramString, localf);
  }
  
  public static final boolean a(int paramInt1, int paramInt2, Intent paramIntent, b<? super Integer, x> paramb)
  {
    int i = 0;
    if (paramInt1 == 1001)
    {
      if (paramInt2 == -1) {
        paramInt1 = 1;
      } else {
        paramInt1 = 0;
      }
      if (paramInt1 == 0) {
        paramIntent = null;
      }
      if (paramIntent != null) {
        paramIntent = Integer.valueOf(paramIntent.getIntExtra("speed_dial_key", 0));
      } else {
        paramIntent = null;
      }
      h localh = new h(2, 9);
      paramInt1 = i;
      if (paramIntent != null)
      {
        paramInt1 = i;
        if (localh.a(paramIntent.intValue())) {
          paramInt1 = 1;
        }
      }
      if (paramInt1 == 0) {
        paramIntent = null;
      }
      if (paramIntent != null)
      {
        paramInt1 = ((Number)paramIntent).intValue();
        if (paramb != null) {
          paramb.invoke(Integer.valueOf(paramInt1));
        }
      }
      return true;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */