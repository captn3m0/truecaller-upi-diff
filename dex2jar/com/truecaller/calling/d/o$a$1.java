package com.truecaller.calling.d;

import c.d.a.a;
import c.d.b.a.f;
import c.d.c;
import c.g.a.m;
import c.n;
import c.o.b;
import c.x;
import com.truecaller.data.access.i;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import kotlinx.coroutines.ag;

@f(b="SpeedDialPresenter.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.speeddial.SpeedDialPresenter$fetchSpeedDialAndResolveContact$1$1")
final class o$a$1
  extends c.d.b.a.k
  implements m<ag, c<? super g>, Object>
{
  int a;
  private ag c;
  
  o$a$1(o.a parama, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new 1(b, paramc);
    c = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    if (a == 0)
    {
      if (!(paramObject instanceof o.b))
      {
        String str = b.f.e.a(b.g);
        if (str != null)
        {
          paramObject = b.f.g.a(str);
          Contact localContact = (Contact)a;
          localObject = (Number)b;
          paramObject = localObject;
          if (localObject == null) {
            paramObject = b.f.f.a(new String[] { str });
          }
          return new g(str, (Number)paramObject, localContact);
        }
        return null;
      }
      throw a;
    }
    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((1)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.o.a.1
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */