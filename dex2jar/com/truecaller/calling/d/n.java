package com.truecaller.calling.d;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import c.o.b;
import c.x;
import com.truecaller.R.id;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.util.at;
import com.truecaller.utils.extensions.r;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;
import kotlinx.coroutines.g;

public final class n
  extends com.truecaller.ui.dialogs.a
  implements View.OnClickListener
{
  public static final a b = new a((byte)0);
  final c.d.f a;
  private final bp c;
  private int d;
  private String e;
  private final q f;
  private final c.d.f g;
  private final Intent h;
  private HashMap i;
  
  public n()
  {
    Object localObject = TrueApp.y();
    c.g.b.k.a(localObject, "TrueApp.getApp()");
    localObject = ((TrueApp)localObject).a();
    c.g.b.k.a(localObject, "TrueApp.getApp().objectsGraph");
    c = ((bp)localObject);
    localObject = c.G();
    c.g.b.k.a(localObject, "graph.speedDialSettings()");
    f = ((q)localObject);
    localObject = c.bl();
    c.g.b.k.a(localObject, "graph.uiCoroutineContext()");
    g = ((c.d.f)localObject);
    localObject = c.bm();
    c.g.b.k.a(localObject, "graph.asyncCoroutineContext()");
    a = ((c.d.f)localObject);
    h = new Intent();
  }
  
  private final void a(String paramString)
  {
    f.a(d, paramString);
    Toast.makeText(getContext(), 2131887166, 0).show();
    a(-1, h);
    dismissAllowingStateLoss();
  }
  
  public final View a(int paramInt)
  {
    if (i == null) {
      i = new HashMap();
    }
    View localView2 = (View)i.get(Integer.valueOf(paramInt));
    View localView1 = localView2;
    if (localView2 == null)
    {
      localView1 = getView();
      if (localView1 == null) {
        return null;
      }
      localView1 = localView1.findViewById(paramInt);
      i.put(Integer.valueOf(paramInt), localView1);
    }
    return localView1;
  }
  
  public final void b()
  {
    HashMap localHashMap = i;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void onActivityResult(int paramInt1, final int paramInt2, final Intent paramIntent)
  {
    if (paramInt1 == 1003) {
      e.b((ag)bg.a, g, (c.g.a.m)new c(this, paramIntent, paramInt2, null), 2);
    }
    if (paramInt1 == 1002)
    {
      if (paramInt2 == -1) {
        paramInt1 = 1;
      } else {
        paramInt1 = 0;
      }
      if (paramInt1 == 0) {
        paramIntent = null;
      }
      if (paramIntent != null)
      {
        paramIntent = paramIntent.getStringExtra("speed_dial_value");
        if (paramIntent != null)
        {
          a(paramIntent);
          return;
        }
      }
    }
  }
  
  public final void onClick(View paramView)
  {
    c.g.b.k.b(paramView, "v");
    int j = paramView.getId();
    if (j != 2131361983)
    {
      paramView = null;
      if (j != 2131361987)
      {
        if (j != 2131362935)
        {
          if (j != 2131364111) {
            return;
          }
          f.a(d, null);
          a(-1, h);
          dismissAllowingStateLoss();
          return;
        }
        m.a((Activity)getActivity());
        dismissAllowingStateLoss();
        return;
      }
      Fragment localFragment = (Fragment)this;
      int k = d;
      String str = e;
      c.g.b.k.b(localFragment, "fragment");
      android.support.v4.app.f localf = localFragment.getActivity();
      if (localf == null) {
        return;
      }
      c.g.b.k.a(localf, "fragment.activity ?: return");
      Object localObject = a.a;
      a locala = new a();
      Bundle localBundle = new Bundle();
      localObject = Integer.valueOf(k);
      j = ((Number)localObject).intValue();
      if ((2 <= j) && (9 >= j)) {
        j = 1;
      } else {
        j = 0;
      }
      if (j != 0) {
        paramView = (View)localObject;
      }
      if (paramView != null)
      {
        localBundle.putInt("speed_dial_key", paramView.intValue());
        locala.setArguments(localBundle);
        a.a(locala, TrueApp.x().getString(2131887164, new Object[] { Integer.valueOf(k) }), str);
        locala.setTargetFragment(localFragment, 1002);
        com.truecaller.ui.dialogs.a.a(locala, localf);
        return;
      }
      throw ((Throwable)new IllegalArgumentException());
    }
    at.a((Fragment)this);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getArguments();
    if (paramBundle != null)
    {
      paramBundle = Integer.valueOf(paramBundle.getInt("speed_dial_key"));
      int j = ((Number)paramBundle).intValue();
      if ((2 <= j) && (9 >= j)) {
        j = 1;
      } else {
        j = 0;
      }
      Object localObject = null;
      if (j == 0) {
        paramBundle = null;
      }
      if (paramBundle != null)
      {
        d = paramBundle.intValue();
        Bundle localBundle = getArguments();
        paramBundle = (Bundle)localObject;
        if (localBundle != null) {
          paramBundle = localBundle.getString("speed_dial_value");
        }
        e = paramBundle;
        h.putExtra("speed_dial_key", d);
        return;
      }
    }
    throw ((Throwable)new IllegalArgumentException());
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    c.g.b.k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558597, paramViewGroup, false);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    c.g.b.k.b(paramView, "view");
    paramView = (TextView)a(R.id.title);
    c.g.b.k.a(paramView, "title");
    int k = d;
    int j = 0;
    paramView.setText((CharSequence)getString(2131887164, new Object[] { Integer.valueOf(k) }));
    paramView = (Button)a(R.id.edit_speed_dial);
    c.g.b.k.a(paramView, "edit_speed_dial");
    paramBundle = (View)paramView;
    paramView = getArguments();
    if (paramView != null) {
      paramView = Boolean.valueOf(paramView.getBoolean("show_options"));
    } else {
      paramView = null;
    }
    t.a(paramBundle, com.truecaller.utils.extensions.c.a(paramView));
    paramView = (Button)a(R.id.remove);
    c.g.b.k.a(paramView, "remove");
    paramView = (View)paramView;
    paramBundle = (CharSequence)e;
    if ((paramBundle == null) || (paramBundle.length() == 0)) {
      j = 1;
    }
    t.a(paramView, 0x1 ^ j);
    paramView = (Button)a(R.id.edit_speed_dial);
    paramBundle = (View.OnClickListener)this;
    paramView.setOnClickListener(paramBundle);
    ((Button)a(R.id.remove)).setOnClickListener(paramBundle);
    ((Button)a(R.id.add_phone_number)).setOnClickListener(paramBundle);
    ((Button)a(R.id.add_contact)).setOnClickListener(paramBundle);
  }
  
  public static final class a
  {
    public static n a(int paramInt, String paramString, boolean paramBoolean)
    {
      n localn = new n();
      Bundle localBundle = new Bundle();
      localBundle.putInt("speed_dial_key", paramInt);
      localBundle.putString("speed_dial_value", paramString);
      localBundle.putBoolean("show_options", paramBoolean);
      localn.setArguments(localBundle);
      localn.setStyle(1, 0);
      return localn;
    }
  }
  
  @c.d.b.a.f(b="SpeedDialOptionsDialog.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.speeddial.SpeedDialOptionsDialog$fetchNumber$2")
  static final class b
    extends c.d.b.a.k
    implements c.g.a.m<ag, c.d.c<? super String>, Object>
  {
    int a;
    private ag c;
    
    b(Uri paramUri, c.d.c paramc)
    {
      super(paramc);
    }
    
    public final c.d.c<x> a(Object paramObject, c.d.c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new b(b, paramc);
      c = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      Object localObject = c.d.a.a.a;
      if (a == 0)
      {
        if (!(paramObject instanceof o.b))
        {
          paramObject = b;
          if (paramObject != null)
          {
            localObject = TrueApp.x();
            c.g.b.k.a(localObject, "TrueApp.getAppContext()");
            return r.b((Uri)paramObject, (Context)localObject);
          }
          return null;
        }
        throw a;
      }
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((b)a(paramObject1, (c.d.c)paramObject2)).a(x.a);
    }
  }
  
  @c.d.b.a.f(b="SpeedDialOptionsDialog.kt", c={87}, d="invokeSuspend", e="com.truecaller.calling.speeddial.SpeedDialOptionsDialog$onActivityResult$1")
  static final class c
    extends c.d.b.a.k
    implements c.g.a.m<ag, c.d.c<? super x>, Object>
  {
    Object a;
    int b;
    private ag f;
    
    c(n paramn, Intent paramIntent, int paramInt, c.d.c paramc)
    {
      super(paramc);
    }
    
    public final c.d.c<x> a(Object paramObject, c.d.c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new c(c, paramIntent, paramInt2, paramc);
      f = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      c.d.a.a locala = c.d.a.a.a;
      switch (b)
      {
      default: 
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
      case 1: 
        if ((paramObject instanceof o.b)) {
          throw a;
        }
        break;
      case 0: 
        if ((paramObject instanceof o.b)) {
          break label175;
        }
        paramObject = paramIntent;
        int i;
        if (paramInt2 == -1) {
          i = 1;
        } else {
          i = 0;
        }
        if (i == 0) {
          paramObject = null;
        }
        if (paramObject == null) {
          break label171;
        }
        Object localObject = c;
        Uri localUri = ((Intent)paramObject).getData();
        a = paramObject;
        b = 1;
        localObject = g.a(a, (c.g.a.m)new n.b(localUri, null), this);
        paramObject = localObject;
        if (localObject == locala) {
          return locala;
        }
        break;
      }
      paramObject = (String)paramObject;
      if (paramObject != null) {
        n.a(c, (String)paramObject);
      }
      label171:
      return x.a;
      label175:
      throw a;
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((c)a(paramObject1, (c.d.c)paramObject2)).a(x.a);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */