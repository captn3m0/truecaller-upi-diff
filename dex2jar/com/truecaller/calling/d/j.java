package com.truecaller.calling.d;

import c.g.b.k;
import com.truecaller.adapter_delegates.c;
import com.truecaller.adapter_delegates.h;
import com.truecaller.calling.dialer.ax;
import com.truecaller.utils.n;
import java.util.List;
import javax.inject.Inject;

public final class j
  extends c<i.c>
  implements i.b
{
  private final i.a c;
  private final n d;
  private final b e;
  private final ax f;
  
  @Inject
  public j(n paramn, b paramb, i.a parama, ax paramax)
  {
    d = paramn;
    e = paramb;
    f = paramax;
    c = parama;
  }
  
  private final List<g> a()
  {
    return c.a((i.b)this, b[0]);
  }
  
  public final boolean a(h paramh)
  {
    k.b(paramh, "event");
    int i = b;
    if (k.a(a, "ItemEvent.CLICKED"))
    {
      b localb = e;
      paramh = (g)a().get(i);
      if (paramh != null) {
        paramh = a;
      } else {
        paramh = null;
      }
      localb.a(i + 1, paramh);
      return true;
    }
    return false;
  }
  
  public final int getItemCount()
  {
    return a().size();
  }
  
  public final long getItemId(int paramInt)
  {
    return paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */