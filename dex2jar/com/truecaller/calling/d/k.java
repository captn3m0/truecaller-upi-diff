package com.truecaller.calling.d;

import com.truecaller.calling.dialer.ax;
import com.truecaller.utils.n;
import dagger.a.d;
import javax.inject.Provider;

public final class k
  implements d<j>
{
  private final Provider<n> a;
  private final Provider<b> b;
  private final Provider<i.a> c;
  private final Provider<ax> d;
  
  private k(Provider<n> paramProvider, Provider<b> paramProvider1, Provider<i.a> paramProvider2, Provider<ax> paramProvider3)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static k a(Provider<n> paramProvider, Provider<b> paramProvider1, Provider<i.a> paramProvider2, Provider<ax> paramProvider3)
  {
    return new k(paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */