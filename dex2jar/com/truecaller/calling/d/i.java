package com.truecaller.calling.d;

import com.truecaller.adapter_delegates.b;
import com.truecaller.adapter_delegates.j;
import java.util.List;

public abstract interface i
{
  public static abstract interface a
  {
    public abstract List<g> a(i.b paramb, c.l.g<?> paramg);
  }
  
  public static abstract interface b
    extends b<i.c>, j
  {}
  
  public static abstract interface c
  {
    public abstract void a(String paramString);
    
    public abstract void a(boolean paramBoolean);
    
    public abstract void b(String paramString);
    
    public abstract void b(boolean paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */