package com.truecaller.calling.d;

import c.d.f;
import com.truecaller.data.access.i;
import com.truecaller.data.entity.g;
import dagger.a.d;
import javax.inject.Provider;

public final class p
  implements d<o>
{
  private final Provider<f> a;
  private final Provider<f> b;
  private final Provider<q> c;
  private final Provider<g> d;
  private final Provider<i> e;
  
  private p(Provider<f> paramProvider1, Provider<f> paramProvider2, Provider<q> paramProvider, Provider<g> paramProvider3, Provider<i> paramProvider4)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static p a(Provider<f> paramProvider1, Provider<f> paramProvider2, Provider<q> paramProvider, Provider<g> paramProvider3, Provider<i> paramProvider4)
  {
    return new p(paramProvider1, paramProvider2, paramProvider, paramProvider3, paramProvider4);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */