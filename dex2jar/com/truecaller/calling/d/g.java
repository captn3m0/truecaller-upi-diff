package com.truecaller.calling.d;

import c.g.b.k;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;

public final class g
{
  final String a;
  final Number b;
  final Contact c;
  
  public g(String paramString, Number paramNumber, Contact paramContact)
  {
    a = paramString;
    b = paramNumber;
    c = paramContact;
  }
  
  public final boolean equals(Object paramObject)
  {
    return ((paramObject instanceof g)) && (k.a(a, a));
  }
  
  public final int hashCode()
  {
    return a.hashCode();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("SpeedDialItem(originalValue=");
    localStringBuilder.append(a);
    localStringBuilder.append(", number=");
    localStringBuilder.append(b);
    localStringBuilder.append(", contact=");
    localStringBuilder.append(c);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */