package com.truecaller.calling.d;

import javax.inject.Inject;

public final class c
  implements q
{
  private final com.truecaller.i.c a;
  
  @Inject
  public c(com.truecaller.i.c paramc)
  {
    a = paramc;
  }
  
  private static String b(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      throw ((Throwable)new IllegalArgumentException());
    case 9: 
      return "speed_dial_9";
    case 8: 
      return "speed_dial_8";
    case 7: 
      return "speed_dial_7";
    case 6: 
      return "speed_dial_6";
    case 5: 
      return "speed_dial_5";
    case 4: 
      return "speed_dial_4";
    case 3: 
      return "speed_dial_3";
    }
    return "speed_dial_2";
  }
  
  public final String a(int paramInt)
  {
    return a.a(b(paramInt));
  }
  
  public final void a(int paramInt, String paramString)
  {
    a.a(b(paramInt), paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.d.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */