package com.truecaller.calling.contacts_list;

public enum ContactsHolder$FavoritesFilter
{
  static
  {
    FavoritesFilter localFavoritesFilter1 = new FavoritesFilter("INCLUDE_NON_FAVORITES", 0);
    INCLUDE_NON_FAVORITES = localFavoritesFilter1;
    FavoritesFilter localFavoritesFilter2 = new FavoritesFilter("FAVORITES_ONLY", 1);
    FAVORITES_ONLY = localFavoritesFilter2;
    $VALUES = new FavoritesFilter[] { localFavoritesFilter1, localFavoritesFilter2 };
  }
  
  private ContactsHolder$FavoritesFilter() {}
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.ContactsHolder.FavoritesFilter
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */