package com.truecaller.calling.contacts_list;

import c.g.b.k;
import com.truecaller.ads.b.w;
import com.truecaller.ads.provider.e;

public abstract interface ab
{
  public abstract a a(ContactsHolder.PhonebookFilter paramPhonebookFilter);
  
  public static final class a
  {
    final e a;
    final w b;
    
    public a(e parame, w paramw)
    {
      a = parame;
      b = paramw;
    }
    
    public final boolean equals(Object paramObject)
    {
      if (this != paramObject) {
        if ((paramObject instanceof a))
        {
          paramObject = (a)paramObject;
          if ((k.a(a, a)) && (k.a(b, b))) {}
        }
        else
        {
          return false;
        }
      }
      return true;
    }
    
    public final int hashCode()
    {
      Object localObject = a;
      int j = 0;
      int i;
      if (localObject != null) {
        i = localObject.hashCode();
      } else {
        i = 0;
      }
      localObject = b;
      if (localObject != null) {
        j = localObject.hashCode();
      }
      return i * 31 + j;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder("AdsPresenterWithLoader(adsLoader=");
      localStringBuilder.append(a);
      localStringBuilder.append(", multiAdsPresenter=");
      localStringBuilder.append(b);
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.ab
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */