package com.truecaller.calling.contacts_list;

import com.truecaller.bn;
import com.truecaller.calling.dialer.v;

public abstract interface p
{
  public static abstract interface a
    extends bn<p.b, p.c>, ContactsHolder, ae, b.a, b.c.b, p.c.a, x
  {
    public abstract void a(v paramv);
    
    public abstract void b(v paramv);
    
    public abstract boolean e();
    
    public abstract void f();
  }
  
  public static abstract interface b
    extends a, ae
  {}
  
  public static abstract interface c
    extends b.c.b
  {
    public abstract void a(ContactsHolder.PhonebookFilter paramPhonebookFilter, boolean paramBoolean);
    
    public abstract void b();
    
    public static abstract interface a
    {
      public abstract void a(ContactsHolder.PhonebookFilter paramPhonebookFilter);
      
      public abstract void a(ContactsHolder.PhonebookFilter paramPhonebookFilter, int paramInt);
      
      public abstract void g();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */