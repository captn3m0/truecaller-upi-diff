package com.truecaller.calling.contacts_list;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.calling.contacts_list.data.SortedContactsDao.b;
import com.truecaller.calling.contacts_list.data.SortedContactsRepository;
import com.truecaller.calling.contacts_list.data.SortedContactsRepository.ContactsLoadingMode;
import com.truecaller.calling.contacts_list.data.SortedContactsRepository.a;
import com.truecaller.calling.contacts_list.data.SortedContactsRepository.b;
import com.truecaller.calling.contacts_list.data.a.a;
import com.truecaller.calling.contacts_list.data.a.b;
import com.truecaller.data.entity.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

@c.d.b.a.f(b="ContactsListPresenter.kt", c={104, 110}, d="invokeSuspend", e="com.truecaller.calling.contacts_list.ContactsListPresenter$fetchContactList$1")
final class r$e
  extends c.d.b.a.k
  implements c.g.a.q<SortedContactsRepository.ContactsLoadingMode, com.truecaller.utils.extensions.q<SortedContactsRepository.ContactsLoadingMode>, c<? super x>, Object>
{
  Object a;
  Object b;
  Object c;
  long d;
  int e;
  private SortedContactsRepository.ContactsLoadingMode g;
  private com.truecaller.utils.extensions.q h;
  
  r$e(r paramr, c paramc)
  {
    super(3, paramc);
  }
  
  public final Object a(Object paramObject)
  {
    c.d.a.a locala = c.d.a.a.a;
    final long l;
    final SortedContactsRepository.ContactsLoadingMode localContactsLoadingMode;
    switch (e)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 2: 
      localObject1 = (SortedContactsRepository.a)c;
      l = d;
      localObject3 = (com.truecaller.utils.extensions.q)b;
      localContactsLoadingMode = (SortedContactsRepository.ContactsLoadingMode)a;
      if (!(paramObject instanceof o.b))
      {
        localObject2 = paramObject;
        paramObject = localObject3;
        break label464;
      }
      throw a;
    case 1: 
      l = d;
      localObject1 = (com.truecaller.utils.extensions.q)b;
      localContactsLoadingMode = (SortedContactsRepository.ContactsLoadingMode)a;
      if (!(paramObject instanceof o.b))
      {
        localObject2 = paramObject;
        paramObject = localObject1;
      }
      else
      {
        throw a;
      }
      break;
    case 0: 
      if ((paramObject instanceof o.b)) {
        break label734;
      }
      localContactsLoadingMode = g;
      paramObject = h;
      l = r.b(f).b();
      localObject1 = r.c(f);
      localObject2 = f.b();
      a = localContactsLoadingMode;
      b = paramObject;
      d = l;
      e = 1;
      localObject1 = ((SortedContactsRepository)localObject1).a((ContactsHolder.SortingMode)localObject2, localContactsLoadingMode, this);
      if (localObject1 == locala) {
        return locala;
      }
      localObject2 = localObject1;
    }
    Object localObject2 = (SortedContactsRepository.a)localObject2;
    Object localObject3 = r.d(f);
    r.b localb = new r.b(a);
    c.g.b.k.b(localb, "another");
    Object localObject1 = ContactsHolder.FavoritesFilter.values();
    int j = localObject1.length;
    int i = 0;
    while (i < j)
    {
      ContactsHolder.FavoritesFilter localFavoritesFilter = localObject1[i];
      ContactsHolder.PhonebookFilter[] arrayOfPhonebookFilter = ContactsHolder.PhonebookFilter.values();
      int m = arrayOfPhonebookFilter.length;
      int k = 0;
      while (k < m)
      {
        ContactsHolder.PhonebookFilter localPhonebookFilter = arrayOfPhonebookFilter[k];
        List localList = localb.a(localFavoritesFilter, localPhonebookFilter);
        if (localList != null) {
          ((r.b)localObject3).a(localFavoritesFilter, localPhonebookFilter, localList);
        }
        k += 1;
      }
      i += 1;
    }
    localObject1 = r.e(f);
    localObject3 = (m)new a(null, this, localContactsLoadingMode, l, (com.truecaller.utils.extensions.q)paramObject);
    a = localContactsLoadingMode;
    b = paramObject;
    d = l;
    c = localObject2;
    e = 2;
    localObject3 = g.a((c.d.f)localObject1, (m)localObject3, this);
    if (localObject3 == locala) {
      return locala;
    }
    localObject1 = localObject2;
    localObject2 = localObject3;
    label464:
    localObject2 = (List)localObject2;
    r.a(f, new r.a((SortedContactsRepository.b)new a.b((Iterable)localObject2), b));
    if (localContactsLoadingMode != SortedContactsRepository.ContactsLoadingMode.NON_PHONEBOOK_LIMITED)
    {
      localObject1 = r.f(f);
      if (localObject1 != null) {
        ((p.c)localObject1).a(ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY, r.d(f).a(ContactsHolder.FavoritesFilter.INCLUDE_NON_FAVORITES, ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY).isEmpty());
      }
    }
    if (localContactsLoadingMode != SortedContactsRepository.ContactsLoadingMode.PHONEBOOK_LIMITED)
    {
      localObject1 = r.f(f);
      if (localObject1 != null) {
        ((p.c)localObject1).a(ContactsHolder.PhonebookFilter.NON_PHONEBOOK_ONLY, r.d(f).a(ContactsHolder.FavoritesFilter.INCLUDE_NON_FAVORITES, ContactsHolder.PhonebookFilter.NON_PHONEBOOK_ONLY).isEmpty());
      }
    }
    localObject1 = r.f(f);
    if (localObject1 != null) {
      ((p.c)localObject1).b();
    }
    switch (u.b[localContactsLoadingMode.ordinal()])
    {
    default: 
      break;
    case 4: 
      ((com.truecaller.utils.extensions.q)paramObject).a(SortedContactsRepository.ContactsLoadingMode.FULL_WITH_ENTITIES);
      break;
    case 3: 
      ((com.truecaller.utils.extensions.q)paramObject).a(SortedContactsRepository.ContactsLoadingMode.FULL_INITIAL);
      break;
    case 2: 
      r.b(f, l);
      ((com.truecaller.utils.extensions.q)paramObject).a(SortedContactsRepository.ContactsLoadingMode.PHONEBOOK_INITIAL);
      break;
    case 1: 
      r.a(f, l);
      ((com.truecaller.utils.extensions.q)paramObject).a(SortedContactsRepository.ContactsLoadingMode.NON_PHONEBOOK_LIMITED);
    }
    return x.a;
    label734:
    throw a;
  }
  
  public final Object a(Object paramObject1, Object paramObject2, Object paramObject3)
  {
    paramObject1 = (SortedContactsRepository.ContactsLoadingMode)paramObject1;
    paramObject2 = (com.truecaller.utils.extensions.q)paramObject2;
    paramObject3 = (c)paramObject3;
    c.g.b.k.b(paramObject1, "loadingMode");
    c.g.b.k.b(paramObject2, "itself");
    c.g.b.k.b(paramObject3, "continuation");
    paramObject3 = new e(f, (c)paramObject3);
    g = ((SortedContactsRepository.ContactsLoadingMode)paramObject1);
    h = ((com.truecaller.utils.extensions.q)paramObject2);
    return ((e)paramObject3).a(x.a);
  }
  
  @c.d.b.a.f(b="ContactsListPresenter.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.contacts_list.ContactsListPresenter$fetchContactList$1$1$indexes$1")
  static final class a
    extends c.d.b.a.k
    implements m<ag, c<? super List<? extends a.a>>, Object>
  {
    int a;
    private ag f;
    
    a(c paramc, r.e parame, SortedContactsRepository.ContactsLoadingMode paramContactsLoadingMode, long paramLong, com.truecaller.utils.extensions.q paramq)
    {
      super(paramc);
    }
    
    public final c<x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new a(paramc, jdField_this, localContactsLoadingMode, l, e);
      f = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      Object localObject1 = c.d.a.a.a;
      if (a == 0)
      {
        if (!(paramObject instanceof o.b))
        {
          paramObject = (Iterable)r.d(jdField_thisf).a(ContactsHolder.FavoritesFilter.INCLUDE_NON_FAVORITES, ContactsHolder.PhonebookFilter.NON_PHONEBOOK_ONLY);
          Object localObject2 = (Map)new LinkedHashMap();
          Iterator localIterator = ((Iterable)paramObject).iterator();
          while (localIterator.hasNext())
          {
            Object localObject3 = localIterator.next();
            String str = b.c;
            localObject1 = ((Map)localObject2).get(str);
            paramObject = localObject1;
            if (localObject1 == null)
            {
              paramObject = new ArrayList();
              ((Map)localObject2).put(str, paramObject);
            }
            ((List)paramObject).add(localObject3);
          }
          paramObject = (Collection)new ArrayList(((Map)localObject2).size());
          localObject1 = ((Map)localObject2).entrySet().iterator();
          while (((Iterator)localObject1).hasNext())
          {
            localObject2 = (Map.Entry)((Iterator)localObject1).next();
            ((Collection)paramObject).add(new a.a((String)((Map.Entry)localObject2).getKey(), ((List)((Map.Entry)localObject2).getValue()).size()));
          }
          return (List)paramObject;
        }
        throw a;
      }
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((a)a(paramObject1, (c)paramObject2)).a(x.a);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.r.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */