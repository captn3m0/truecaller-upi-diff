package com.truecaller.calling.contacts_list;

import c.a.y;
import c.o.b;
import c.x;
import com.truecaller.bc;
import com.truecaller.bd;
import com.truecaller.calling.contacts_list.data.SortedContactsDao.b;
import com.truecaller.calling.contacts_list.data.SortedContactsRepository;
import com.truecaller.calling.contacts_list.data.SortedContactsRepository.ContactsLoadingMode;
import com.truecaller.calling.contacts_list.data.SortedContactsRepository.a;
import com.truecaller.calling.contacts_list.data.SortedContactsRepository.b;
import com.truecaller.calling.contacts_list.data.a.a;
import com.truecaller.calling.contacts_list.data.a.b;
import com.truecaller.calling.dialer.v;
import com.truecaller.calling.dialer.v.a;
import com.truecaller.data.entity.Contact;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.g;

public final class r
  extends bc<p.b, p.c>
  implements p.a
{
  private boolean d;
  private boolean e;
  private boolean f;
  private final c g;
  private final d h;
  private a i;
  private final b j;
  private v k;
  private v l;
  private final com.truecaller.utils.extensions.q<SortedContactsRepository.ContactsLoadingMode> m;
  private final SortedContactsRepository n;
  private final com.truecaller.i.c o;
  private final c.d.f p;
  private final c.d.f q;
  private final com.truecaller.common.account.r r;
  private final com.truecaller.search.local.model.c s;
  private final com.truecaller.analytics.b t;
  private final com.truecaller.utils.a u;
  
  @Inject
  public r(SortedContactsRepository paramSortedContactsRepository, com.truecaller.i.c paramc, @Named("UI") c.d.f paramf1, @Named("CPU") c.d.f paramf2, com.truecaller.common.account.r paramr, @Named("ContactsAvailabilityManager") com.truecaller.search.local.model.c paramc1, com.truecaller.analytics.b paramb, com.truecaller.utils.a parama)
  {
    super(paramf1);
    n = paramSortedContactsRepository;
    o = paramc;
    p = paramf1;
    q = paramf2;
    r = paramr;
    s = paramc1;
    t = paramb;
    u = parama;
    d = true;
    g = new c(this);
    h = new d(this);
    i = new a();
    j = new b();
    paramSortedContactsRepository = (c.g.a.q)new e(this, null);
    c.g.b.k.b(this, "receiver$0");
    c.g.b.k.b(paramSortedContactsRepository, "block");
    m = new com.truecaller.utils.extensions.q(this, paramSortedContactsRepository);
  }
  
  public final String a(int paramInt, ContactsHolder.PhonebookFilter paramPhonebookFilter)
  {
    c.g.b.k.b(paramPhonebookFilter, "phonebookFilter");
    Object localObject = i;
    c.g.b.k.b(paramPhonebookFilter, "phonebookFilter");
    localObject = a;
    int i1;
    switch (s.a[paramPhonebookFilter.ordinal()])
    {
    default: 
      throw new c.l();
    case 2: 
      i1 = 1;
      break;
    case 1: 
      i1 = 0;
    }
    paramPhonebookFilter = localObject[i1];
    if (paramPhonebookFilter != null)
    {
      localObject = paramPhonebookFilter.a(paramInt);
      paramPhonebookFilter = (ContactsHolder.PhonebookFilter)localObject;
      if (localObject == null) {
        paramPhonebookFilter = "?";
      }
      return paramPhonebookFilter;
    }
    return null;
  }
  
  public final List<SortedContactsDao.b> a(ContactsHolder.FavoritesFilter paramFavoritesFilter, ContactsHolder.PhonebookFilter paramPhonebookFilter)
  {
    c.g.b.k.b(paramFavoritesFilter, "favoritesFilter");
    c.g.b.k.b(paramPhonebookFilter, "phonebookFilter");
    return j.a(paramFavoritesFilter, paramPhonebookFilter);
  }
  
  public final void a(ContactsHolder.PhonebookFilter paramPhonebookFilter)
  {
    c.g.b.k.b(paramPhonebookFilter, "phonebookFilter");
    boolean bool;
    if (paramPhonebookFilter == ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY) {
      bool = true;
    } else {
      bool = false;
    }
    d = bool;
    paramPhonebookFilter = (p.b)c;
    if (paramPhonebookFilter != null)
    {
      paramPhonebookFilter.b();
      return;
    }
  }
  
  public final void a(ContactsHolder.PhonebookFilter paramPhonebookFilter, int paramInt)
  {
    c.g.b.k.b(paramPhonebookFilter, "phonebookFilter");
    c.g.b.k.b(paramPhonebookFilter, "phonebookFilter");
    c.g.b.k.b(paramPhonebookFilter, "phonebookFilter");
  }
  
  public final void a(ContactsHolder.SortingMode paramSortingMode)
  {
    c.g.b.k.b(paramSortingMode, "value");
    com.truecaller.i.c localc = o;
    int i1;
    switch (u.a[paramSortingMode.ordinal()])
    {
    default: 
      throw new c.l();
    case 2: 
      i1 = 1;
      break;
    case 1: 
      i1 = 0;
    }
    localc.b("sorting_mode", i1);
  }
  
  public final void a(v paramv)
  {
    c.g.b.k.b(paramv, "observer");
    k = paramv;
  }
  
  public final void a(Contact paramContact)
  {
    c.g.b.k.b(paramContact, "contact");
    p.b localb = (p.b)c;
    if (localb != null)
    {
      localb.a(paramContact);
      return;
    }
  }
  
  public final ContactsHolder.SortingMode b()
  {
    if (o.a("sorting_mode", 0) != 0) {
      return ContactsHolder.SortingMode.BY_LAST_NAME;
    }
    return ContactsHolder.SortingMode.BY_FIRST_NAME;
  }
  
  public final void b(v paramv)
  {
    c.g.b.k.b(paramv, "observer");
    l = paramv;
  }
  
  public final void b(Contact paramContact)
  {
    c.g.b.k.b(paramContact, "contact");
    p.b localb = (p.b)c;
    if (localb != null)
    {
      localb.b(paramContact);
      return;
    }
  }
  
  public final void c(Contact paramContact)
  {
    c.g.b.k.b(paramContact, "contact");
    p.b localb = (p.b)c;
    if (localb != null)
    {
      localb.c(paramContact);
      return;
    }
  }
  
  public final boolean e()
  {
    return d;
  }
  
  public final void f()
  {
    com.truecaller.utils.extensions.q localq = m;
    c.n();
    c = localq.a();
    localq.a(SortedContactsRepository.ContactsLoadingMode.PHONEBOOK_LIMITED);
  }
  
  public final void g()
  {
    h();
  }
  
  public final void h()
  {
    p.b localb = (p.b)c;
    if (localb != null)
    {
      localb.h();
      return;
    }
  }
  
  public final void i()
  {
    s.b();
  }
  
  public final void j()
  {
    s.c();
  }
  
  public final void s_()
  {
    p.c localc = (p.c)b;
    if (localc != null)
    {
      localc.s_();
      return;
    }
  }
  
  public final void t_()
  {
    p.b localb = (p.b)c;
    if (localb != null)
    {
      localb.t_();
      return;
    }
  }
  
  public final void y_()
  {
    super.y_();
    v localv = k;
    if (localv != null) {
      localv.a(null);
    }
    localv = l;
    if (localv != null)
    {
      localv.a(null);
      return;
    }
  }
  
  static final class a
  {
    final SortedContactsRepository.b[] a;
    
    public a(SortedContactsRepository.b paramb1, SortedContactsRepository.b paramb2)
    {
      a = new SortedContactsRepository.b[] { paramb1, paramb2 };
    }
  }
  
  static final class b
  {
    private final List<SortedContactsDao.b>[][] a;
    
    public b(List<SortedContactsDao.b> paramList)
    {
      int k = ContactsHolder.FavoritesFilter.values().length;
      Object localObject1 = new List[k][];
      int i = 0;
      int j;
      while (i < k)
      {
        localObject2 = new List[ContactsHolder.PhonebookFilter.values().length];
        int m = localObject2.length;
        j = 0;
        while (j < m)
        {
          localObject2[j] = ((List)y.a);
          j += 1;
        }
        localObject1[i] = localObject2;
        i += 1;
      }
      a = ((List[][])localObject1);
      paramList = (Iterable)paramList;
      Object localObject2 = (Map)new LinkedHashMap();
      Object localObject3 = paramList.iterator();
      Object localObject4;
      Object localObject5;
      for (;;)
      {
        boolean bool = ((Iterator)localObject3).hasNext();
        j = 1;
        if (!bool) {
          break;
        }
        localObject4 = ((Iterator)localObject3).next();
        paramList = a;
        i = j;
        if (paramList.E() == null)
        {
          i = j;
          if (!paramList.Y()) {
            i = 0;
          }
        }
        localObject5 = Integer.valueOf(i);
        localObject1 = ((Map)localObject2).get(localObject5);
        paramList = (List<SortedContactsDao.b>)localObject1;
        if (localObject1 == null)
        {
          paramList = new ArrayList();
          ((Map)localObject2).put(localObject5, paramList);
        }
        ((List)paramList).add(localObject4);
      }
      localObject3 = (List)((Map)localObject2).get(Integer.valueOf(0));
      if (localObject3 != null)
      {
        paramList = ContactsHolder.FavoritesFilter.INCLUDE_NON_FAVORITES;
        localObject1 = ContactsHolder.PhonebookFilter.NON_PHONEBOOK_ONLY;
        localObject4 = (Iterable)localObject3;
        localObject3 = (Collection)new ArrayList();
        localObject4 = ((Iterable)localObject4).iterator();
        while (((Iterator)localObject4).hasNext())
        {
          localObject5 = ((Iterator)localObject4).next();
          SortedContactsDao.b localb = (SortedContactsDao.b)localObject5;
          if ((!a.U()) && (!c)) {
            i = 0;
          } else {
            i = 1;
          }
          if (i == 0) {
            ((Collection)localObject3).add(localObject5);
          }
        }
        a(paramList, (ContactsHolder.PhonebookFilter)localObject1, (List)localObject3);
      }
      paramList = (List)((Map)localObject2).get(Integer.valueOf(1));
      if (paramList != null) {
        a(ContactsHolder.FavoritesFilter.INCLUDE_NON_FAVORITES, ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY, paramList);
      }
      a(ContactsHolder.FavoritesFilter.FAVORITES_ONLY, ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY, c.m.l.d(c.m.l.c(c.m.l.a(c.a.m.n((Iterable)a(ContactsHolder.FavoritesFilter.INCLUDE_NON_FAVORITES, ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY)), (c.g.a.b)1.a), (c.g.a.b)2.a)));
    }
    
    private static int a(ContactsHolder.FavoritesFilter paramFavoritesFilter)
    {
      switch (t.a[paramFavoritesFilter.ordinal()])
      {
      default: 
        throw new c.l();
      case 2: 
        return 1;
      }
      return 0;
    }
    
    private static int a(ContactsHolder.PhonebookFilter paramPhonebookFilter)
    {
      switch (t.b[paramPhonebookFilter.ordinal()])
      {
      default: 
        throw new c.l();
      case 2: 
        return 1;
      }
      return 0;
    }
    
    public final List<SortedContactsDao.b> a(ContactsHolder.FavoritesFilter paramFavoritesFilter, ContactsHolder.PhonebookFilter paramPhonebookFilter)
    {
      c.g.b.k.b(paramFavoritesFilter, "favoritesFilter");
      c.g.b.k.b(paramPhonebookFilter, "phonebookFilter");
      return a[a(paramFavoritesFilter)][a(paramPhonebookFilter)];
    }
    
    final void a(ContactsHolder.FavoritesFilter paramFavoritesFilter, ContactsHolder.PhonebookFilter paramPhonebookFilter, List<SortedContactsDao.b> paramList)
    {
      a[a(paramFavoritesFilter)][a(paramPhonebookFilter)] = paramList;
    }
  }
  
  public static final class c
    implements v.a
  {
    public final void D_()
    {
      r.a(a).a(SortedContactsRepository.ContactsLoadingMode.FULL_WITH_ENTITIES);
    }
  }
  
  public static final class d
    implements v.a
  {
    public final void D_()
    {
      r.a(a).a(SortedContactsRepository.ContactsLoadingMode.FULL_WITH_ENTITIES);
    }
  }
  
  @c.d.b.a.f(b="ContactsListPresenter.kt", c={104, 110}, d="invokeSuspend", e="com.truecaller.calling.contacts_list.ContactsListPresenter$fetchContactList$1")
  static final class e
    extends c.d.b.a.k
    implements c.g.a.q<SortedContactsRepository.ContactsLoadingMode, com.truecaller.utils.extensions.q<SortedContactsRepository.ContactsLoadingMode>, c.d.c<? super x>, Object>
  {
    Object a;
    Object b;
    Object c;
    long d;
    int e;
    private SortedContactsRepository.ContactsLoadingMode g;
    private com.truecaller.utils.extensions.q h;
    
    e(r paramr, c.d.c paramc)
    {
      super(paramc);
    }
    
    public final Object a(Object paramObject)
    {
      c.d.a.a locala = c.d.a.a.a;
      final long l;
      final SortedContactsRepository.ContactsLoadingMode localContactsLoadingMode;
      switch (e)
      {
      default: 
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
      case 2: 
        localObject1 = (SortedContactsRepository.a)c;
        l = d;
        localObject3 = (com.truecaller.utils.extensions.q)b;
        localContactsLoadingMode = (SortedContactsRepository.ContactsLoadingMode)a;
        if (!(paramObject instanceof o.b))
        {
          localObject2 = paramObject;
          paramObject = localObject3;
          break label464;
        }
        throw a;
      case 1: 
        l = d;
        localObject1 = (com.truecaller.utils.extensions.q)b;
        localContactsLoadingMode = (SortedContactsRepository.ContactsLoadingMode)a;
        if (!(paramObject instanceof o.b))
        {
          localObject2 = paramObject;
          paramObject = localObject1;
        }
        else
        {
          throw a;
        }
        break;
      case 0: 
        if ((paramObject instanceof o.b)) {
          break label734;
        }
        localContactsLoadingMode = g;
        paramObject = h;
        l = r.b(f).b();
        localObject1 = r.c(f);
        localObject2 = f.b();
        a = localContactsLoadingMode;
        b = paramObject;
        d = l;
        e = 1;
        localObject1 = ((SortedContactsRepository)localObject1).a((ContactsHolder.SortingMode)localObject2, localContactsLoadingMode, this);
        if (localObject1 == locala) {
          return locala;
        }
        localObject2 = localObject1;
      }
      Object localObject2 = (SortedContactsRepository.a)localObject2;
      Object localObject3 = r.d(f);
      r.b localb = new r.b(a);
      c.g.b.k.b(localb, "another");
      Object localObject1 = ContactsHolder.FavoritesFilter.values();
      int j = localObject1.length;
      int i = 0;
      while (i < j)
      {
        ContactsHolder.FavoritesFilter localFavoritesFilter = localObject1[i];
        ContactsHolder.PhonebookFilter[] arrayOfPhonebookFilter = ContactsHolder.PhonebookFilter.values();
        int m = arrayOfPhonebookFilter.length;
        int k = 0;
        while (k < m)
        {
          ContactsHolder.PhonebookFilter localPhonebookFilter = arrayOfPhonebookFilter[k];
          List localList = localb.a(localFavoritesFilter, localPhonebookFilter);
          if (localList != null) {
            ((r.b)localObject3).a(localFavoritesFilter, localPhonebookFilter, localList);
          }
          k += 1;
        }
        i += 1;
      }
      localObject1 = r.e(f);
      localObject3 = (c.g.a.m)new a(null, this, localContactsLoadingMode, l, (com.truecaller.utils.extensions.q)paramObject);
      a = localContactsLoadingMode;
      b = paramObject;
      d = l;
      c = localObject2;
      e = 2;
      localObject3 = g.a((c.d.f)localObject1, (c.g.a.m)localObject3, this);
      if (localObject3 == locala) {
        return locala;
      }
      localObject1 = localObject2;
      localObject2 = localObject3;
      label464:
      localObject2 = (List)localObject2;
      r.a(f, new r.a((SortedContactsRepository.b)new a.b((Iterable)localObject2), b));
      if (localContactsLoadingMode != SortedContactsRepository.ContactsLoadingMode.NON_PHONEBOOK_LIMITED)
      {
        localObject1 = r.f(f);
        if (localObject1 != null) {
          ((p.c)localObject1).a(ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY, r.d(f).a(ContactsHolder.FavoritesFilter.INCLUDE_NON_FAVORITES, ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY).isEmpty());
        }
      }
      if (localContactsLoadingMode != SortedContactsRepository.ContactsLoadingMode.PHONEBOOK_LIMITED)
      {
        localObject1 = r.f(f);
        if (localObject1 != null) {
          ((p.c)localObject1).a(ContactsHolder.PhonebookFilter.NON_PHONEBOOK_ONLY, r.d(f).a(ContactsHolder.FavoritesFilter.INCLUDE_NON_FAVORITES, ContactsHolder.PhonebookFilter.NON_PHONEBOOK_ONLY).isEmpty());
        }
      }
      localObject1 = r.f(f);
      if (localObject1 != null) {
        ((p.c)localObject1).b();
      }
      switch (u.b[localContactsLoadingMode.ordinal()])
      {
      default: 
        break;
      case 4: 
        ((com.truecaller.utils.extensions.q)paramObject).a(SortedContactsRepository.ContactsLoadingMode.FULL_WITH_ENTITIES);
        break;
      case 3: 
        ((com.truecaller.utils.extensions.q)paramObject).a(SortedContactsRepository.ContactsLoadingMode.FULL_INITIAL);
        break;
      case 2: 
        r.b(f, l);
        ((com.truecaller.utils.extensions.q)paramObject).a(SortedContactsRepository.ContactsLoadingMode.PHONEBOOK_INITIAL);
        break;
      case 1: 
        r.a(f, l);
        ((com.truecaller.utils.extensions.q)paramObject).a(SortedContactsRepository.ContactsLoadingMode.NON_PHONEBOOK_LIMITED);
      }
      return x.a;
      label734:
      throw a;
    }
    
    public final Object a(Object paramObject1, Object paramObject2, Object paramObject3)
    {
      paramObject1 = (SortedContactsRepository.ContactsLoadingMode)paramObject1;
      paramObject2 = (com.truecaller.utils.extensions.q)paramObject2;
      paramObject3 = (c.d.c)paramObject3;
      c.g.b.k.b(paramObject1, "loadingMode");
      c.g.b.k.b(paramObject2, "itself");
      c.g.b.k.b(paramObject3, "continuation");
      paramObject3 = new e(f, (c.d.c)paramObject3);
      g = ((SortedContactsRepository.ContactsLoadingMode)paramObject1);
      h = ((com.truecaller.utils.extensions.q)paramObject2);
      return ((e)paramObject3).a(x.a);
    }
    
    @c.d.b.a.f(b="ContactsListPresenter.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.contacts_list.ContactsListPresenter$fetchContactList$1$1$indexes$1")
    static final class a
      extends c.d.b.a.k
      implements c.g.a.m<ag, c.d.c<? super List<? extends a.a>>, Object>
    {
      int a;
      private ag f;
      
      a(c.d.c paramc, r.e parame, SortedContactsRepository.ContactsLoadingMode paramContactsLoadingMode, long paramLong, com.truecaller.utils.extensions.q paramq)
      {
        super(paramc);
      }
      
      public final c.d.c<x> a(Object paramObject, c.d.c<?> paramc)
      {
        c.g.b.k.b(paramc, "completion");
        paramc = new a(paramc, jdField_this, localContactsLoadingMode, l, e);
        f = ((ag)paramObject);
        return paramc;
      }
      
      public final Object a(Object paramObject)
      {
        Object localObject1 = c.d.a.a.a;
        if (a == 0)
        {
          if (!(paramObject instanceof o.b))
          {
            paramObject = (Iterable)r.d(jdField_thisf).a(ContactsHolder.FavoritesFilter.INCLUDE_NON_FAVORITES, ContactsHolder.PhonebookFilter.NON_PHONEBOOK_ONLY);
            Object localObject2 = (Map)new LinkedHashMap();
            Iterator localIterator = ((Iterable)paramObject).iterator();
            while (localIterator.hasNext())
            {
              Object localObject3 = localIterator.next();
              String str = b.c;
              localObject1 = ((Map)localObject2).get(str);
              paramObject = localObject1;
              if (localObject1 == null)
              {
                paramObject = new ArrayList();
                ((Map)localObject2).put(str, paramObject);
              }
              ((List)paramObject).add(localObject3);
            }
            paramObject = (Collection)new ArrayList(((Map)localObject2).size());
            localObject1 = ((Map)localObject2).entrySet().iterator();
            while (((Iterator)localObject1).hasNext())
            {
              localObject2 = (Map.Entry)((Iterator)localObject1).next();
              ((Collection)paramObject).add(new a.a((String)((Map.Entry)localObject2).getKey(), ((List)((Map.Entry)localObject2).getValue()).size()));
            }
            return (List)paramObject;
          }
          throw a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
      }
      
      public final Object invoke(Object paramObject1, Object paramObject2)
      {
        return ((a)a(paramObject1, (c.d.c)paramObject2)).a(x.a);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.r
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */