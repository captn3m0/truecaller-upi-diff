package com.truecaller.calling.contacts_list;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.view.View;
import c.g.b.k;
import c.g.b.l;
import c.u;
import com.truecaller.R.id;
import com.truecaller.adapter_delegates.a;
import com.truecaller.adapter_delegates.g;
import com.truecaller.adapter_delegates.m;
import com.truecaller.adapter_delegates.o;
import com.truecaller.adapter_delegates.p;
import com.truecaller.adapter_delegates.s;
import com.truecaller.ads.b.j;
import com.truecaller.ui.q;
import java.util.Iterator;
import java.util.Set;

public final class w
  implements p.c
{
  final com.truecaller.adapter_delegates.f b;
  final View c;
  private final c.f d;
  private final p<f, f> e;
  private final p<f, f> f;
  private final p<e, e> g;
  private final m h;
  private final p.c.a i;
  
  public w(p.c.a parama, View paramView, b.b paramb, final ContactsHolder.PhonebookFilter paramPhonebookFilter, final ContactsHolder paramContactsHolder, y paramy, com.truecaller.ads.b.w paramw)
  {
    i = parama;
    c = paramView;
    d = com.truecaller.utils.extensions.t.a(c, 2131362962);
    e = new p((com.truecaller.adapter_delegates.b)paramy.a(paramPhonebookFilter, ContactsHolder.FavoritesFilter.INCLUDE_NON_FAVORITES), 2131559064, (c.g.a.b)new e(this), (c.g.a.b)f.a);
    f = new p((com.truecaller.adapter_delegates.b)paramy.a(paramPhonebookFilter, ContactsHolder.FavoritesFilter.FAVORITES_ONLY), 2131558615, (c.g.a.b)new g(this), (c.g.a.b)h.a);
    g = new p((com.truecaller.adapter_delegates.b)paramb, 2131559004, (c.g.a.b)new c(paramb), (c.g.a.b)d.a);
    h = j.a(paramw);
    parama = new com.truecaller.adapter_delegates.f((a)((a)e.a((a)f, (s)new g((byte)0)).a((a)g, (s)new g((byte)0))).a((a)h, (s)new o(2, 12, 4)));
    parama.setHasStableIds(true);
    b = parama;
    parama = (RecyclerView)c.findViewById(R.id.contacts_list);
    paramView = b;
    paramView.a(true);
    parama.setAdapter((RecyclerView.Adapter)paramView);
    parama.setItemAnimator(null);
    parama.addItemDecoration((RecyclerView.ItemDecoration)new q(c.getContext(), 2131559208, 0));
    parama.addOnScrollListener((RecyclerView.OnScrollListener)new a(this, paramPhonebookFilter, paramContactsHolder));
    paramView = (FastScroller)c.findViewById(R.id.fast_scroller);
    k.a(parama, "this");
    paramb = (c.g.a.b)new b(this, paramPhonebookFilter, paramContactsHolder);
    k.b(parama, "recyclerView");
    k.b(paramb, "indexByPosition");
    a = parama;
    c = paramb;
    paramb = parama.getLayoutManager();
    if (paramb != null)
    {
      b = ((LinearLayoutManager)paramb);
      parama.addOnScrollListener((RecyclerView.OnScrollListener)new FastScroller.a(paramView));
      paramView.a();
      return;
    }
    throw new u("null cannot be cast to non-null type android.support.v7.widget.LinearLayoutManager");
  }
  
  private final View c()
  {
    return (View)d.b();
  }
  
  public final void a(ContactsHolder.PhonebookFilter paramPhonebookFilter, boolean paramBoolean)
  {
    k.b(paramPhonebookFilter, "phonebookFilter");
    b.a(paramBoolean);
    com.truecaller.utils.extensions.t.a(c(), paramBoolean);
  }
  
  public final void a(Set<Integer> paramSet)
  {
    k.b(paramSet, "adsPositions");
    paramSet = ((Iterable)paramSet).iterator();
    while (paramSet.hasNext())
    {
      int j = ((Number)paramSet.next()).intValue();
      j = h.a_(j);
      com.truecaller.adapter_delegates.f localf = b;
      localf.notifyItemRangeChanged(j, localf.getItemCount() - j);
    }
  }
  
  public final void b()
  {
    b.notifyDataSetChanged();
    ((FastScroller)c.findViewById(R.id.fast_scroller)).a();
  }
  
  public final void s_()
  {
    b.notifyDataSetChanged();
  }
  
  public static final class a
    extends RecyclerView.OnScrollListener
  {
    a(w paramw, ContactsHolder.PhonebookFilter paramPhonebookFilter, ContactsHolder paramContactsHolder) {}
    
    public final void onScrollStateChanged(RecyclerView paramRecyclerView, int paramInt)
    {
      k.b(paramRecyclerView, "recyclerView");
      w.b(a).a(paramPhonebookFilter, paramInt);
    }
  }
  
  static final class b
    extends l
    implements c.g.a.b<Integer, String>
  {
    b(w paramw, ContactsHolder.PhonebookFilter paramPhonebookFilter, ContactsHolder paramContactsHolder)
    {
      super();
    }
  }
  
  static final class c
    extends l
    implements c.g.a.b<View, e>
  {
    c(b.b paramb)
    {
      super();
    }
  }
  
  static final class d
    extends l
    implements c.g.a.b<e, e>
  {
    public static final d a = new d();
    
    d()
    {
      super();
    }
  }
  
  static final class e
    extends l
    implements c.g.a.b<View, f>
  {
    e(w paramw)
    {
      super();
    }
  }
  
  static final class f
    extends l
    implements c.g.a.b<f, f>
  {
    public static final f a = new f();
    
    f()
    {
      super();
    }
  }
  
  static final class g
    extends l
    implements c.g.a.b<View, f>
  {
    g(w paramw)
    {
      super();
    }
  }
  
  static final class h
    extends l
    implements c.g.a.b<f, f>
  {
    public static final h a = new h();
    
    h()
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.w
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */