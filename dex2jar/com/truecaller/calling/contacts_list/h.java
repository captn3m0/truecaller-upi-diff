package com.truecaller.calling.contacts_list;

import c.g.b.k;
import c.g.b.l;
import c.x;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.calling.ActionType;
import com.truecaller.calling.contacts_list.data.SortedContactsDao.b;
import com.truecaller.calling.contacts_list.data.g;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.UnmutedException.e;
import java.util.List;

public final class h
  extends com.truecaller.adapter_delegates.c<g.c>
  implements g.b
{
  private final g.a b;
  private final com.truecaller.search.local.model.c c;
  private final ae d;
  private final com.truecaller.analytics.b e;
  private final g f;
  
  public h(g.a parama, com.truecaller.search.local.model.c paramc, ae paramae, com.truecaller.analytics.b paramb, g paramg)
  {
    b = parama;
    c = paramc;
    d = paramae;
    e = paramb;
    f = paramg;
  }
  
  private final List<SortedContactsDao.b> a()
  {
    return b.a();
  }
  
  private final boolean b(com.truecaller.adapter_delegates.h paramh)
  {
    try
    {
      d.c(agetb)).a);
    }
    catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
    {
      for (;;) {}
    }
    AssertionUtil.reportThrowableButNeverCrash((Throwable)new UnmutedException.e(b, a().size()));
    return true;
  }
  
  public final boolean a(com.truecaller.adapter_delegates.h paramh)
  {
    k.b(paramh, "event");
    Object localObject1 = a;
    if (k.a(localObject1, "ItemEvent.CLICKED")) {
      return b(paramh);
    }
    if (k.a(localObject1, ActionType.SMS.getEventAction()))
    {
      Object localObject2 = new e.a("ViewAction").a("Action", "message").a("Context", "contacts");
      localObject1 = e;
      localObject2 = ((e.a)localObject2).a();
      k.a(localObject2, "analyticsEvent.build()");
      ((com.truecaller.analytics.b)localObject1).b((e)localObject2);
      d.a(agetb)).a);
      return true;
    }
    if (k.a(localObject1, ActionType.VOIP_CALL.getEventAction()))
    {
      d.b(agetb)).a);
      return true;
    }
    return false;
  }
  
  public final int getItemCount()
  {
    return a().size();
  }
  
  public final long getItemId(int paramInt)
  {
    Long localLong = ageta.getId();
    if (localLong != null) {
      return localLong.longValue();
    }
    return -1L;
  }
  
  static final class a
    extends l
    implements c.g.a.b<Boolean, x>
  {
    a(g.c paramc)
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */