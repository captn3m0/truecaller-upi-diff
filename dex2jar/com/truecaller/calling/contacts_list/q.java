package com.truecaller.calling.contacts_list;

import android.support.v4.view.o;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import c.a.ag;
import c.a.m;
import c.g.b.k;
import c.t;
import com.truecaller.ads.g;
import com.truecaller.ads.h;
import com.truecaller.ads.provider.e;
import com.truecaller.common.account.r;
import com.truecaller.i.a;
import com.truecaller.utils.extensions.c;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;

public final class q
  extends o
  implements p.c.a
{
  final w[] a;
  final ab.a[] b;
  final ContactsHolder.PhonebookFilter[] c;
  private final Map<a, Boolean>[] d;
  private final long e;
  private boolean f;
  private int g;
  private final ContactsHolder h;
  private final p.a i;
  private final ab j;
  private final com.truecaller.utils.n k;
  private final y l;
  private final r m;
  private final b.b n;
  
  @Inject
  public q(ContactsHolder paramContactsHolder, p.a parama, ab paramab, com.truecaller.utils.n paramn, y paramy, r paramr, b.b paramb, a parama1)
  {
    h = paramContactsHolder;
    i = parama;
    j = paramab;
    k = paramn;
    l = paramy;
    m = paramr;
    n = paramb;
    a = new w[2];
    b = new ab.a[2];
    c = new ContactsHolder.PhonebookFilter[] { ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY, ContactsHolder.PhonebookFilter.NON_PHONEBOOK_ONLY };
    paramContactsHolder = new Map[2];
    int i1 = 0;
    while (i1 < 2)
    {
      paramContactsHolder[i1] = ag.b(new c.n[] { t.a(a.a, Boolean.FALSE), t.a(a.b, Boolean.FALSE) });
      i1 += 1;
    }
    d = paramContactsHolder;
    e = TimeUnit.SECONDS.toMillis(parama1.a("adFeatureRetentionTime", 0L));
    g = -1;
  }
  
  private final void a(int paramInt, a parama, boolean paramBoolean)
  {
    Map localMap = d[paramInt];
    localMap.put(parama, Boolean.valueOf(paramBoolean));
    parama = b[paramInt];
    if (parama != null)
    {
      parama = a;
      if (parama != null)
      {
        if ((!c.a((Boolean)localMap.get(a.a))) && (!c.a((Boolean)localMap.get(a.b)))) {
          paramBoolean = false;
        } else {
          paramBoolean = true;
        }
        parama.a(paramBoolean);
        return;
      }
    }
  }
  
  public final void a(int paramInt)
  {
    int i1 = g;
    if (i1 != -1) {
      b(i1);
    }
    g = paramInt;
    a(paramInt, a.a, false);
    Object localObject = b[paramInt];
    if (localObject != null)
    {
      localObject = a;
      if (localObject != null)
      {
        ((e)localObject).f();
        w localw = a[paramInt];
        if (localw != null)
        {
          localw.a(((e)localObject).b());
          return;
        }
        return;
      }
    }
  }
  
  public final void a(ContactsHolder.PhonebookFilter paramPhonebookFilter)
  {
    k.b(paramPhonebookFilter, "phonebookFilter");
    k.b(paramPhonebookFilter, "phonebookFilter");
  }
  
  public final void a(ContactsHolder.PhonebookFilter paramPhonebookFilter, int paramInt)
  {
    k.b(paramPhonebookFilter, "phonebookFilter");
    int i1 = c.a.f.c(c, paramPhonebookFilter);
    switch (paramInt)
    {
    default: 
      return;
    case 2: 
      a(i1, a.b, true);
      return;
    case 1: 
      a(i1, a.b, true);
      return;
    }
    a(i1, a.b, false);
  }
  
  public final void b(int paramInt)
  {
    a(paramInt, a.a, true);
    Object localObject = b[paramInt];
    if (localObject != null)
    {
      localObject = a;
      if (localObject != null)
      {
        long l1 = e;
        if (l1 == 0L) {
          ((e)localObject).d();
        } else {
          ((e)localObject).a(l1);
        }
      }
    }
    g = -1;
  }
  
  public final void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject)
  {
    k.b(paramViewGroup, "container");
    k.b(paramObject, "object");
    paramViewGroup.removeView((View)paramObject);
  }
  
  public final void g() {}
  
  public final int getCount()
  {
    if (m.c()) {
      return 2;
    }
    return 1;
  }
  
  public final CharSequence getPageTitle(int paramInt)
  {
    return (CharSequence)k.a(2130903065)[paramInt];
  }
  
  public final Object instantiateItem(final ViewGroup paramViewGroup, int paramInt)
  {
    k.b(paramViewGroup, "container");
    Object localObject1 = LayoutInflater.from(paramViewGroup.getContext());
    int i2 = 0;
    localObject1 = ((LayoutInflater)localObject1).inflate(2131558534, paramViewGroup, false);
    paramViewGroup.addView((View)localObject1);
    Object localObject2 = c[paramInt];
    paramViewGroup = j.a((ContactsHolder.PhonebookFilter)localObject2);
    p.c.a locala = (p.c.a)this;
    k.a(localObject1, "pageView");
    localObject1 = new w(locala, (View)localObject1, n, (ContactsHolder.PhonebookFilter)localObject2, h, l, b);
    a[paramInt] = localObject1;
    b[paramInt] = paramViewGroup;
    localObject2 = a;
    int i1 = g;
    if (i1 == -1)
    {
      if (paramInt == 0)
      {
        g = 0;
        ((e)localObject2).a(false);
      }
    }
    else if (paramInt == i1) {
      ((e)localObject2).a(false);
    }
    paramViewGroup = a;
    paramViewGroup.a((g)new c((w)localObject1, paramViewGroup));
    if (!f)
    {
      paramViewGroup = a;
      int i3 = getCount();
      k.b(paramViewGroup, "receiver$0");
      if (i3 >= 0) {
        paramInt = 1;
      } else {
        paramInt = 0;
      }
      if (paramInt != 0)
      {
        if (i3 == 0)
        {
          paramViewGroup = (List)c.a.y.a;
        }
        else if (i3 >= paramViewGroup.length)
        {
          paramViewGroup = c.a.f.f(paramViewGroup);
        }
        else if (i3 == 1)
        {
          paramViewGroup = m.a(paramViewGroup[0]);
        }
        else
        {
          localObject2 = new ArrayList(i3);
          int i4 = paramViewGroup.length;
          i1 = 0;
          paramInt = 0;
          while (i1 < i4)
          {
            locala = paramViewGroup[i1];
            if (paramInt == i3) {
              break;
            }
            ((ArrayList)localObject2).add(locala);
            i1 += 1;
            paramInt += 1;
          }
          paramViewGroup = (List)localObject2;
        }
        paramViewGroup = (Iterable)paramViewGroup;
        if ((!(paramViewGroup instanceof Collection)) || (!((Collection)paramViewGroup).isEmpty()))
        {
          paramViewGroup = paramViewGroup.iterator();
          while (paramViewGroup.hasNext())
          {
            if ((w)paramViewGroup.next() != null) {
              paramInt = 1;
            } else {
              paramInt = 0;
            }
            if (paramInt == 0)
            {
              paramInt = i2;
              break label407;
            }
          }
        }
        paramInt = 1;
        label407:
        if (paramInt != 0)
        {
          f = true;
          i.a(new b(this));
          return localObject1;
        }
      }
      else
      {
        paramViewGroup = new StringBuilder("Requested element count ");
        paramViewGroup.append(i3);
        paramViewGroup.append(" is less than zero.");
        throw ((Throwable)new IllegalArgumentException(paramViewGroup.toString().toString()));
      }
    }
    return localObject1;
  }
  
  public final boolean isViewFromObject(View paramView, Object paramObject)
  {
    k.b(paramView, "view");
    k.b(paramObject, "object");
    w[] arrayOfw = a;
    int i3 = arrayOfw.length;
    int i1 = 0;
    while (i1 < i3)
    {
      w localw = arrayOfw[i1];
      int i2;
      if ((k.a(localw, paramObject)) && (k.a(c, paramView))) {
        i2 = 1;
      } else {
        i2 = 0;
      }
      if (i2 != 0) {
        return true;
      }
      i1 += 1;
    }
    return false;
  }
  
  static enum a
  {
    static
    {
      a locala1 = new a("TAB_SWITCH", 0);
      a = locala1;
      a locala2 = new a("SCROLL_STATE", 1);
      b = locala2;
      c = new a[] { locala1, locala2 };
    }
    
    private a() {}
  }
  
  public static final class b
    implements p.c
  {
    public final void a(ContactsHolder.PhonebookFilter paramPhonebookFilter, boolean paramBoolean)
    {
      k.b(paramPhonebookFilter, "phonebookFilter");
      Object localObject = a;
      localObject = a[c.a.f.c(c, paramPhonebookFilter)];
      if (localObject != null)
      {
        ((w)localObject).a(paramPhonebookFilter, paramBoolean);
        return;
      }
    }
    
    public final void b()
    {
      w[] arrayOfw = a.a;
      int j = arrayOfw.length;
      int i = 0;
      while (i < j)
      {
        w localw = arrayOfw[i];
        if (localw != null) {
          localw.b();
        }
        i += 1;
      }
    }
    
    public final void s_()
    {
      w[] arrayOfw = a.a;
      int j = arrayOfw.length;
      int i = 0;
      while (i < j)
      {
        w localw = arrayOfw[i];
        if (localw != null) {
          b.notifyDataSetChanged();
        }
        i += 1;
      }
    }
  }
  
  public static final class c
    extends h
  {
    c(w paramw, e parame) {}
    
    public final void a()
    {
      a.a(paramViewGroup.b());
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */