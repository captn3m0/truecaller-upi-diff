package com.truecaller.calling.contacts_list;

import dagger.a.d;
import javax.inject.Provider;

public final class ad
  implements d<ac>
{
  private final Provider<j> a;
  
  private ad(Provider<j> paramProvider)
  {
    a = paramProvider;
  }
  
  public static ad a(Provider<j> paramProvider)
  {
    return new ad(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.ad
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */