package com.truecaller.calling.contacts_list;

import android.content.ContentResolver;
import com.truecaller.calling.dialer.s;
import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d<s>
{
  private final Provider<ContentResolver> a;
  
  private o(Provider<ContentResolver> paramProvider)
  {
    a = paramProvider;
  }
  
  public static o a(Provider<ContentResolver> paramProvider)
  {
    return new o(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */