package com.truecaller.calling.contacts_list;

import com.truecaller.adapter_delegates.b;
import com.truecaller.adapter_delegates.j;
import com.truecaller.calling.a;
import com.truecaller.calling.ao;
import com.truecaller.calling.bc;
import com.truecaller.calling.be;
import com.truecaller.calling.c;
import com.truecaller.calling.contacts_list.data.SortedContactsDao.b;
import com.truecaller.calling.p;
import com.truecaller.calling.v;
import com.truecaller.ui.q.a;
import java.util.List;

public abstract interface g
{
  public static abstract interface a
  {
    public abstract List<SortedContactsDao.b> a();
    
    public abstract ContactsHolder.SortingMode b();
  }
  
  public static abstract interface b
    extends b<g.c>, j
  {}
  
  public static abstract interface c
    extends a, ao, bc, be, c, p, v, q.a
  {}
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */