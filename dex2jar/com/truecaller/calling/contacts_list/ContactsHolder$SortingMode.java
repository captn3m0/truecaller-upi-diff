package com.truecaller.calling.contacts_list;

public enum ContactsHolder$SortingMode
{
  static
  {
    SortingMode localSortingMode1 = new SortingMode("BY_FIRST_NAME", 0);
    BY_FIRST_NAME = localSortingMode1;
    SortingMode localSortingMode2 = new SortingMode("BY_LAST_NAME", 1);
    BY_LAST_NAME = localSortingMode2;
    $VALUES = new SortingMode[] { localSortingMode1, localSortingMode2 };
  }
  
  private ContactsHolder$SortingMode() {}
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.ContactsHolder.SortingMode
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */