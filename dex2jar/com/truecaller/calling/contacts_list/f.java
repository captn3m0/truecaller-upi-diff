package com.truecaller.calling.contacts_list;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import c.g.a.b;
import c.x;
import com.truecaller.adapter_delegates.i;
import com.truecaller.calling.ActionType;
import com.truecaller.calling.ao;
import com.truecaller.calling.au;
import com.truecaller.calling.ba;
import com.truecaller.calling.bc;
import com.truecaller.calling.be;
import com.truecaller.calling.bf;
import com.truecaller.calling.bg;
import com.truecaller.calling.bg.c;
import com.truecaller.calling.bg.d;
import com.truecaller.calling.c;
import com.truecaller.calling.contacts_list.data.g;
import com.truecaller.calling.d;
import com.truecaller.calling.m;
import com.truecaller.calling.o;
import com.truecaller.calling.p;
import com.truecaller.calling.q;
import com.truecaller.calling.v;
import com.truecaller.data.entity.Contact;
import com.truecaller.search.local.model.c.a;
import com.truecaller.ui.q.b;

public final class f
  extends RecyclerView.ViewHolder
  implements com.truecaller.calling.a, ao, bc, be, c, g.c, p, v, q.b
{
  private f(View paramView, com.truecaller.adapter_delegates.k paramk, ba paramba, o paramo)
  {
    super(paramView);
    a = paramo;
    b = paramba;
    c = new d(paramView);
    d = new q(paramView);
    e = new bg(paramView);
    f = paramo;
    g = new m();
    h = new bf(paramView);
    paramba = (RecyclerView.ViewHolder)this;
    i.a(paramView, paramk, paramba, null, 12);
    c.g.b.k.b(paramk, "eventReceiver");
    c.g.b.k.b(paramba, "holder");
    paramView = e;
    c.g.b.k.b(paramk, "eventReceiver");
    c.g.b.k.b(paramba, "holder");
    paramo = paramView.a();
    c.g.b.k.a(paramo, "actionOneView");
    i.a((View)paramo, paramk, paramba, (c.g.a.a)new bg.c(paramView), "button");
    paramView.b().setOnClickListener((View.OnClickListener)new bg.d(paramView));
  }
  
  public final String a()
  {
    return g.b;
  }
  
  public final void a(int paramInt)
  {
    f.a(paramInt);
  }
  
  public final void a(ActionType paramActionType)
  {
    e.a(paramActionType);
  }
  
  public final void a(g paramg, Contact paramContact, b<? super Boolean, x> paramb)
  {
    c.g.b.k.b(paramg, "voipAvailabilityCache");
    c.g.b.k.b(paramContact, "contact");
    c.g.b.k.b(paramb, "callback");
    h.a(paramg, paramContact, paramb);
  }
  
  public final void a(c.a parama)
  {
    c.a(parama);
  }
  
  public final void a(Object paramObject)
  {
    a.a(paramObject);
  }
  
  public final void b(boolean paramBoolean)
  {
    e.b(paramBoolean);
  }
  
  public final boolean b()
  {
    return g.b();
  }
  
  public final void b_(String paramString)
  {
    b.b_(paramString);
  }
  
  public final int c()
  {
    return g.c();
  }
  
  public final void c_(String paramString)
  {
    g.c_(paramString);
  }
  
  public final void c_(boolean paramBoolean)
  {
    g.c_(paramBoolean);
  }
  
  public final void e_(String paramString)
  {
    d.e_(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */