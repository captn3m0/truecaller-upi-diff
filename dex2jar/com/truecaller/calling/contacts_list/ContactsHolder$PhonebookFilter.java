package com.truecaller.calling.contacts_list;

public enum ContactsHolder$PhonebookFilter
{
  static
  {
    PhonebookFilter localPhonebookFilter1 = new PhonebookFilter("NON_PHONEBOOK_ONLY", 0);
    NON_PHONEBOOK_ONLY = localPhonebookFilter1;
    PhonebookFilter localPhonebookFilter2 = new PhonebookFilter("PHONEBOOK_ONLY", 1);
    PHONEBOOK_ONLY = localPhonebookFilter2;
    $VALUES = new PhonebookFilter[] { localPhonebookFilter1, localPhonebookFilter2 };
  }
  
  private ContactsHolder$PhonebookFilter() {}
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.ContactsHolder.PhonebookFilter
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */