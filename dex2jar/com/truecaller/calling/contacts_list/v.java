package com.truecaller.calling.contacts_list;

import c.d.f;
import com.truecaller.analytics.b;
import com.truecaller.calling.contacts_list.data.SortedContactsRepository;
import com.truecaller.utils.a;
import dagger.a.d;
import javax.inject.Provider;

public final class v
  implements d<r>
{
  private final Provider<SortedContactsRepository> a;
  private final Provider<com.truecaller.i.c> b;
  private final Provider<f> c;
  private final Provider<f> d;
  private final Provider<com.truecaller.common.account.r> e;
  private final Provider<com.truecaller.search.local.model.c> f;
  private final Provider<b> g;
  private final Provider<a> h;
  
  private v(Provider<SortedContactsRepository> paramProvider, Provider<com.truecaller.i.c> paramProvider1, Provider<f> paramProvider2, Provider<f> paramProvider3, Provider<com.truecaller.common.account.r> paramProvider4, Provider<com.truecaller.search.local.model.c> paramProvider5, Provider<b> paramProvider6, Provider<a> paramProvider7)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
  }
  
  public static v a(Provider<SortedContactsRepository> paramProvider, Provider<com.truecaller.i.c> paramProvider1, Provider<f> paramProvider2, Provider<f> paramProvider3, Provider<com.truecaller.common.account.r> paramProvider4, Provider<com.truecaller.search.local.model.c> paramProvider5, Provider<b> paramProvider6, Provider<a> paramProvider7)
  {
    return new v(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.v
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */