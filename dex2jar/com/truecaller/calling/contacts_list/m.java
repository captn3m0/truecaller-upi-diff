package com.truecaller.calling.contacts_list;

import com.truecaller.ads.provider.e;
import dagger.a.d;
import javax.inject.Provider;

public final class m
  implements d<e>
{
  private final l.a a;
  private final Provider<com.truecaller.ads.provider.f> b;
  private final Provider<c.d.f> c;
  
  private m(l.a parama, Provider<com.truecaller.ads.provider.f> paramProvider, Provider<c.d.f> paramProvider1)
  {
    a = parama;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static m a(l.a parama, Provider<com.truecaller.ads.provider.f> paramProvider, Provider<c.d.f> paramProvider1)
  {
    return new m(parama, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */