package com.truecaller.calling.contacts_list.data;

public enum SortedContactsDao$ContactFullness
{
  static
  {
    ContactFullness localContactFullness1 = new ContactFullness("BARE_MINIMUM", 0);
    BARE_MINIMUM = localContactFullness1;
    ContactFullness localContactFullness2 = new ContactFullness("COMPLETE_WITH_ENTITIES", 1);
    COMPLETE_WITH_ENTITIES = localContactFullness2;
    $VALUES = new ContactFullness[] { localContactFullness1, localContactFullness2 };
  }
  
  private SortedContactsDao$ContactFullness() {}
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.SortedContactsDao.ContactFullness
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */