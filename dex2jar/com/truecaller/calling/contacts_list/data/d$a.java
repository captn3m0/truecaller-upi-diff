package com.truecaller.calling.contacts_list.data;

import c.d.a.a;
import c.d.b.a.f;
import c.d.c;
import c.g.a.m;
import c.l;
import c.o.b;
import c.x;
import com.truecaller.calling.contacts_list.ContactsHolder.PhonebookFilter;
import com.truecaller.calling.contacts_list.ContactsHolder.SortingMode;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;
import kotlinx.coroutines.e;

@f(b="SortedContactsRepository.kt", c={122, 123, 133, 134, 135, 145, 146, 147}, d="invokeSuspend", e="com.truecaller.calling.contacts_list.data.SortedContactsRepositoryImpl$getContacts$2")
final class d$a
  extends c.d.b.a.k
  implements m<ag, c<? super SortedContactsRepository.a>, Object>
{
  Object a;
  Object b;
  Object c;
  Object d;
  Object e;
  int f;
  private ag l;
  
  d$a(d paramd, SortedContactsRepository.ContactsLoadingMode paramContactsLoadingMode, ContactsHolder.SortingMode paramSortingMode, boolean paramBoolean1, boolean paramBoolean2, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new a(g, h, i, j, k, paramc);
    l = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    switch (f)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 8: 
      localObject1 = (SortedContactsRepository.b)e;
      localObject2 = (List)d;
      if (!(paramObject instanceof o.b))
      {
        localObject3 = paramObject;
        break label691;
      }
      throw a;
    case 7: 
      localObject1 = (List)d;
      localObject2 = (ao)c;
      localObject3 = (ao)b;
      localObject4 = (ao)a;
      if (!(paramObject instanceof o.b))
      {
        localObject5 = paramObject;
        break label628;
      }
      throw a;
    case 6: 
      localObject1 = (ao)c;
      localObject2 = (ao)b;
      localObject3 = (ao)a;
      if ((paramObject instanceof o.b)) {
        throw a;
      }
      break;
    case 5: 
      localObject1 = (SortedContactsRepository.b)e;
      localObject2 = (List)d;
      if (!(paramObject instanceof o.b))
      {
        localObject3 = paramObject;
        break label926;
      }
      throw a;
    case 4: 
      localObject1 = (List)d;
      localObject2 = (ao)c;
      localObject3 = (ao)b;
      localObject4 = (ao)a;
      if (!(paramObject instanceof o.b))
      {
        localObject5 = paramObject;
        break label864;
      }
      throw a;
    case 3: 
      localObject1 = (ao)c;
      localObject2 = (ao)b;
      localObject3 = (ao)a;
      if (!(paramObject instanceof o.b)) {
        break label802;
      }
      throw a;
    case 2: 
      localObject1 = (List)c;
      if (!(paramObject instanceof o.b))
      {
        localObject2 = paramObject;
        break label1060;
      }
      throw a;
    case 1: 
      localObject1 = (ao)b;
      localObject2 = (ao)a;
      if (!(paramObject instanceof o.b)) {
        break label1016;
      }
      throw a;
    case 0: 
      if ((paramObject instanceof o.b)) {
        break label1150;
      }
      paramObject = l;
      localObject1 = h;
      switch (e.a[localObject1.ordinal()])
      {
      default: 
        throw new l();
      case 5: 
        localObject3 = e.a((ag)paramObject, null, (m)new f(this, null), 3);
        localObject2 = e.a((ag)paramObject, null, (m)new h(this, null), 3);
        localObject1 = e.a((ag)paramObject, null, (m)new c(this, null), 3);
        a = localObject3;
        b = localObject2;
        c = localObject1;
        f = 6;
        paramObject = ((ao)localObject1).a(this);
        if (paramObject == locala) {
          return locala;
        }
        break;
      }
      break;
    }
    paramObject = (List)paramObject;
    a = localObject3;
    b = localObject2;
    c = localObject1;
    d = paramObject;
    f = 7;
    Object localObject5 = ((ao)localObject3).a(this);
    if (localObject5 == locala) {
      return locala;
    }
    Object localObject4 = localObject3;
    Object localObject3 = localObject2;
    Object localObject2 = localObject1;
    Object localObject1 = paramObject;
    label628:
    paramObject = (SortedContactsRepository.b)localObject5;
    a = localObject4;
    b = localObject3;
    c = localObject2;
    d = localObject1;
    e = paramObject;
    f = 8;
    localObject3 = ((ao)localObject3).a(this);
    if (localObject3 == locala) {
      return locala;
    }
    localObject2 = localObject1;
    localObject1 = paramObject;
    label691:
    return new SortedContactsRepository.a((List)localObject2, (SortedContactsRepository.b)localObject1, (SortedContactsRepository.b)localObject3);
    localObject3 = e.a((ag)paramObject, null, (m)new e(this, null), 3);
    localObject2 = e.a((ag)paramObject, null, (m)new g(this, null), 3);
    localObject1 = e.a((ag)paramObject, null, (m)new b(this, null), 3);
    a = localObject3;
    b = localObject2;
    c = localObject1;
    f = 3;
    paramObject = ((ao)localObject1).a(this);
    if (paramObject == locala) {
      return locala;
    }
    label802:
    paramObject = (List)paramObject;
    a = localObject3;
    b = localObject2;
    c = localObject1;
    d = paramObject;
    f = 4;
    localObject5 = ((ao)localObject3).a(this);
    if (localObject5 == locala) {
      return locala;
    }
    localObject4 = localObject3;
    localObject3 = localObject2;
    localObject2 = localObject1;
    localObject1 = paramObject;
    label864:
    paramObject = (SortedContactsRepository.b)localObject5;
    a = localObject4;
    b = localObject3;
    c = localObject2;
    d = localObject1;
    e = paramObject;
    f = 5;
    localObject3 = ((ao)localObject3).a(this);
    if (localObject3 == locala) {
      return locala;
    }
    localObject2 = localObject1;
    localObject1 = paramObject;
    label926:
    return new SortedContactsRepository.a((List)localObject2, (SortedContactsRepository.b)localObject1, (SortedContactsRepository.b)localObject3);
    localObject2 = e.a((ag)paramObject, null, (m)new d(this, null), 3);
    localObject1 = e.a((ag)paramObject, null, (m)new a(this, null), 3);
    a = localObject2;
    b = localObject1;
    f = 1;
    localObject3 = ((ao)localObject1).a(this);
    paramObject = localObject3;
    if (localObject3 == locala) {
      return locala;
    }
    label1016:
    paramObject = (List)paramObject;
    a = localObject2;
    b = localObject1;
    c = paramObject;
    f = 2;
    localObject2 = ((ao)localObject2).a(this);
    if (localObject2 == locala) {
      return locala;
    }
    localObject1 = paramObject;
    label1060:
    return new SortedContactsRepository.a((List)localObject1, (SortedContactsRepository.b)localObject2, 2);
    return new SortedContactsRepository.a(g.a.a(i, SortedContactsDao.ContactFullness.BARE_MINIMUM, Integer.valueOf(100), ContactsHolder.PhonebookFilter.NON_PHONEBOOK_ONLY), null, 6);
    return new SortedContactsRepository.a(g.a.a(i, SortedContactsDao.ContactFullness.BARE_MINIMUM, Integer.valueOf(100), ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY), null, 6);
    label1150:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((a)a(paramObject1, (c)paramObject2)).a(x.a);
  }
  
  @f(b="SortedContactsRepository.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.contacts_list.data.SortedContactsRepositoryImpl$getContacts$2$contacts$1")
  static final class a
    extends c.d.b.a.k
    implements m<ag, c<? super List<? extends SortedContactsDao.b>>, Object>
  {
    int a;
    private ag c;
    
    a(d.a parama, c paramc)
    {
      super(paramc);
    }
    
    public final c<x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new a(b, paramc);
      c = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      a locala = a.a;
      if (a == 0)
      {
        if (!(paramObject instanceof o.b)) {
          return SortedContactsDao.a.a(b.g.a, b.i, SortedContactsDao.ContactFullness.BARE_MINIMUM, ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY, 4);
        }
        throw a;
      }
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((a)a(paramObject1, (c)paramObject2)).a(x.a);
    }
  }
  
  @f(b="SortedContactsRepository.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.contacts_list.data.SortedContactsRepositoryImpl$getContacts$2$contacts$2")
  static final class b
    extends c.d.b.a.k
    implements m<ag, c<? super List<? extends SortedContactsDao.b>>, Object>
  {
    int a;
    private ag c;
    
    b(d.a parama, c paramc)
    {
      super(paramc);
    }
    
    public final c<x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new b(b, paramc);
      c = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      a locala = a.a;
      if (a == 0)
      {
        if (!(paramObject instanceof o.b)) {
          return SortedContactsDao.a.a(b.g.a, b.i, SortedContactsDao.ContactFullness.BARE_MINIMUM, null, 12);
        }
        throw a;
      }
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((b)a(paramObject1, (c)paramObject2)).a(x.a);
    }
  }
  
  @f(b="SortedContactsRepository.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.contacts_list.data.SortedContactsRepositoryImpl$getContacts$2$contacts$3")
  static final class c
    extends c.d.b.a.k
    implements m<ag, c<? super List<? extends SortedContactsDao.b>>, Object>
  {
    int a;
    private ag c;
    
    c(d.a parama, c paramc)
    {
      super(paramc);
    }
    
    public final c<x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new c(b, paramc);
      c = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      a locala = a.a;
      if (a == 0)
      {
        if (!(paramObject instanceof o.b)) {
          return SortedContactsDao.a.a(b.g.a, b.i, SortedContactsDao.ContactFullness.COMPLETE_WITH_ENTITIES, null, 12);
        }
        throw a;
      }
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((c)a(paramObject1, (c)paramObject2)).a(x.a);
    }
  }
  
  @f(b="SortedContactsRepository.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.contacts_list.data.SortedContactsRepositoryImpl$getContacts$2$indexes$1")
  static final class d
    extends c.d.b.a.k
    implements m<ag, c<? super SortedContactsRepository.b>, Object>
  {
    int a;
    private ag c;
    
    d(d.a parama, c paramc)
    {
      super(paramc);
    }
    
    public final c<x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new d(b, paramc);
      c = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      a locala = a.a;
      if (a == 0)
      {
        if (!(paramObject instanceof o.b)) {
          return SortedContactsDao.a.a(b.g.a, b.i, ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY);
        }
        throw a;
      }
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((d)a(paramObject1, (c)paramObject2)).a(x.a);
    }
  }
  
  @f(b="SortedContactsRepository.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.contacts_list.data.SortedContactsRepositoryImpl$getContacts$2$nonPhonebookContactsIndexes$1")
  static final class e
    extends c.d.b.a.k
    implements m<ag, c<? super SortedContactsRepository.b>, Object>
  {
    int a;
    private ag c;
    
    e(d.a parama, c paramc)
    {
      super(paramc);
    }
    
    public final c<x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new e(b, paramc);
      c = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      a locala = a.a;
      if (a == 0)
      {
        if (!(paramObject instanceof o.b)) {
          return b.g.a.a(b.i, ContactsHolder.PhonebookFilter.NON_PHONEBOOK_ONLY, b.j, b.k);
        }
        throw a;
      }
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((e)a(paramObject1, (c)paramObject2)).a(x.a);
    }
  }
  
  @f(b="SortedContactsRepository.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.contacts_list.data.SortedContactsRepositoryImpl$getContacts$2$nonPhonebookContactsIndexes$2")
  static final class f
    extends c.d.b.a.k
    implements m<ag, c<? super SortedContactsRepository.b>, Object>
  {
    int a;
    private ag c;
    
    f(d.a parama, c paramc)
    {
      super(paramc);
    }
    
    public final c<x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new f(b, paramc);
      c = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      a locala = a.a;
      if (a == 0)
      {
        if (!(paramObject instanceof o.b)) {
          return b.g.a.a(b.i, ContactsHolder.PhonebookFilter.NON_PHONEBOOK_ONLY, b.j, b.k);
        }
        throw a;
      }
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((f)a(paramObject1, (c)paramObject2)).a(x.a);
    }
  }
  
  @f(b="SortedContactsRepository.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.contacts_list.data.SortedContactsRepositoryImpl$getContacts$2$phonebookContactsIndexes$1")
  static final class g
    extends c.d.b.a.k
    implements m<ag, c<? super SortedContactsRepository.b>, Object>
  {
    int a;
    private ag c;
    
    g(d.a parama, c paramc)
    {
      super(paramc);
    }
    
    public final c<x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new g(b, paramc);
      c = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      a locala = a.a;
      if (a == 0)
      {
        if (!(paramObject instanceof o.b)) {
          return SortedContactsDao.a.a(b.g.a, b.i, ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY);
        }
        throw a;
      }
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((g)a(paramObject1, (c)paramObject2)).a(x.a);
    }
  }
  
  @f(b="SortedContactsRepository.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.contacts_list.data.SortedContactsRepositoryImpl$getContacts$2$phonebookContactsIndexes$2")
  static final class h
    extends c.d.b.a.k
    implements m<ag, c<? super SortedContactsRepository.b>, Object>
  {
    int a;
    private ag c;
    
    h(d.a parama, c paramc)
    {
      super(paramc);
    }
    
    public final c<x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new h(b, paramc);
      c = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      a locala = a.a;
      if (a == 0)
      {
        if (!(paramObject instanceof o.b)) {
          return SortedContactsDao.a.a(b.g.a, b.i, ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY);
        }
        throw a;
      }
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((h)a(paramObject1, (c)paramObject2)).a(x.a);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.d.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */