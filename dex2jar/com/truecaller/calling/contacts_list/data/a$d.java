package com.truecaller.calling.contacts_list.data;

import android.database.Cursor;
import c.g.b.j;
import c.g.b.w;

final class a$d
  extends j
  implements c.g.a.b<Cursor, com.truecaller.data.entity.b>
{
  a$d(com.truecaller.data.c paramc)
  {
    super(1, paramc);
  }
  
  public final c.l.c a()
  {
    return w.a(com.truecaller.data.c.class);
  }
  
  public final String b()
  {
    return "getSortingData";
  }
  
  public final String c()
  {
    return "getSortingData(Landroid/database/Cursor;)Lcom/truecaller/data/entity/ContactSortingData;";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.a.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */