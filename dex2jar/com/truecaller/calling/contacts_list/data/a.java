package com.truecaller.calling.contacts_list.data;

import android.content.ContentResolver;
import android.database.Cursor;
import c.g.a.q;
import c.g.b.j;
import c.g.b.k;
import c.g.b.l;
import c.g.b.w;
import c.n;
import c.t;
import com.truecaller.calling.contacts_list.ContactsHolder.PhonebookFilter;
import com.truecaller.calling.contacts_list.ContactsHolder.SortingMode;
import com.truecaller.data.entity.Contact;
import java.util.Iterator;
import javax.inject.Inject;

public final class a
  implements SortedContactsDao
{
  private final String a;
  private final ContentResolver b;
  
  @Inject
  public a(ContentResolver paramContentResolver)
  {
    b = paramContentResolver;
    a = "data_type = 4 OR data_type = 1 OR data_type IS NULL OR data_type = 7";
  }
  
  /* Error */
  @android.annotation.SuppressLint({"Recycle"})
  public final SortedContactsRepository.b a(ContactsHolder.SortingMode paramSortingMode, ContactsHolder.PhonebookFilter paramPhonebookFilter, boolean paramBoolean1, boolean paramBoolean2)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 53
    //   3: invokestatic 32	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_2
    //   7: ldc 55
    //   9: invokestatic 32	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   12: invokestatic 61	java/lang/System:currentTimeMillis	()J
    //   15: lstore 5
    //   17: getstatic 67	com/truecaller/calling/contacts_list/data/b:d	[I
    //   20: aload_1
    //   21: invokevirtual 73	com/truecaller/calling/contacts_list/ContactsHolder$SortingMode:ordinal	()I
    //   24: iaload
    //   25: tableswitch	default:+23->48, 1:+38->63, 2:+31->56
    //   48: new 75	c/l
    //   51: dup
    //   52: invokespecial 76	c/l:<init>	()V
    //   55: athrow
    //   56: ldc 78
    //   58: astore 9
    //   60: goto +7 -> 67
    //   63: ldc 80
    //   65: astore 9
    //   67: getstatic 83	com/truecaller/calling/contacts_list/data/b:e	[I
    //   70: aload_2
    //   71: invokevirtual 86	com/truecaller/calling/contacts_list/ContactsHolder$PhonebookFilter:ordinal	()I
    //   74: iaload
    //   75: tableswitch	default:+21->96, 1:+36->111, 2:+29->104
    //   96: new 75	c/l
    //   99: dup
    //   100: invokespecial 76	c/l:<init>	()V
    //   103: athrow
    //   104: ldc 88
    //   106: astore 10
    //   108: goto +7 -> 115
    //   111: ldc 90
    //   113: astore 10
    //   115: aload 9
    //   117: aload 10
    //   119: iload_3
    //   120: iload 4
    //   122: invokestatic 95	com/truecaller/content/TruecallerContract$f:a	(Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/net/Uri;
    //   125: astore 9
    //   127: aload 9
    //   129: ldc 97
    //   131: invokestatic 99	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   134: aconst_null
    //   135: astore 11
    //   137: aload_0
    //   138: getfield 37	com/truecaller/calling/contacts_list/data/a:b	Landroid/content/ContentResolver;
    //   141: aload 9
    //   143: aconst_null
    //   144: aconst_null
    //   145: aconst_null
    //   146: aconst_null
    //   147: invokevirtual 105	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   150: astore 10
    //   152: aload 11
    //   154: astore 9
    //   156: aload 10
    //   158: ifnull +137 -> 295
    //   161: aload 10
    //   163: checkcast 107	java/io/Closeable
    //   166: astore 12
    //   168: new 109	java/util/ArrayList
    //   171: dup
    //   172: invokespecial 110	java/util/ArrayList:<init>	()V
    //   175: checkcast 112	java/util/Collection
    //   178: astore 9
    //   180: aload 10
    //   182: invokeinterface 118 1 0
    //   187: ifeq +53 -> 240
    //   190: aload 9
    //   192: new 8	com/truecaller/calling/contacts_list/data/a$a
    //   195: dup
    //   196: aload 10
    //   198: aload 10
    //   200: ldc 120
    //   202: invokeinterface 124 2 0
    //   207: invokeinterface 128 2 0
    //   212: aload 10
    //   214: aload 10
    //   216: ldc -126
    //   218: invokeinterface 124 2 0
    //   223: invokeinterface 134 2 0
    //   228: invokespecial 137	com/truecaller/calling/contacts_list/data/a$a:<init>	(Ljava/lang/String;I)V
    //   231: invokeinterface 141 2 0
    //   236: pop
    //   237: goto -57 -> 180
    //   240: aload 9
    //   242: checkcast 143	java/util/List
    //   245: astore 9
    //   247: aload 12
    //   249: aconst_null
    //   250: invokestatic 148	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   253: goto +42 -> 295
    //   256: astore 10
    //   258: aconst_null
    //   259: astore 9
    //   261: goto +10 -> 271
    //   264: astore 9
    //   266: aload 9
    //   268: athrow
    //   269: astore 10
    //   271: aload 12
    //   273: aload 9
    //   275: invokestatic 148	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   278: aload 10
    //   280: athrow
    //   281: astore 9
    //   283: aload 9
    //   285: checkcast 51	java/lang/Throwable
    //   288: invokestatic 154	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   291: aload 11
    //   293: astore 9
    //   295: aload 9
    //   297: astore 10
    //   299: aload 9
    //   301: ifnonnull +11 -> 312
    //   304: getstatic 159	c/a/y:a	Lc/a/y;
    //   307: checkcast 143	java/util/List
    //   310: astore 10
    //   312: invokestatic 61	java/lang/System:currentTimeMillis	()J
    //   315: lstore 7
    //   317: new 161	java/lang/StringBuilder
    //   320: dup
    //   321: ldc -93
    //   323: invokespecial 166	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   326: astore 9
    //   328: aload 9
    //   330: aload_1
    //   331: invokevirtual 170	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   334: pop
    //   335: aload 9
    //   337: ldc -84
    //   339: invokevirtual 175	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   342: pop
    //   343: aload 9
    //   345: aload_2
    //   346: invokevirtual 170	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   349: pop
    //   350: aload 9
    //   352: ldc -79
    //   354: invokevirtual 175	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   357: pop
    //   358: aload 9
    //   360: lload 7
    //   362: lload 5
    //   364: lsub
    //   365: invokevirtual 180	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   368: pop
    //   369: aload 9
    //   371: ldc -74
    //   373: invokevirtual 175	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   376: pop
    //   377: aload 9
    //   379: invokevirtual 186	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   382: pop
    //   383: new 11	com/truecaller/calling/contacts_list/data/a$b
    //   386: dup
    //   387: aload 10
    //   389: checkcast 188	java/lang/Iterable
    //   392: invokespecial 191	com/truecaller/calling/contacts_list/data/a$b:<init>	(Ljava/lang/Iterable;)V
    //   395: checkcast 193	com/truecaller/calling/contacts_list/data/SortedContactsRepository$b
    //   398: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	399	0	this	a
    //   0	399	1	paramSortingMode	ContactsHolder.SortingMode
    //   0	399	2	paramPhonebookFilter	ContactsHolder.PhonebookFilter
    //   0	399	3	paramBoolean1	boolean
    //   0	399	4	paramBoolean2	boolean
    //   15	348	5	l1	long
    //   315	46	7	l2	long
    //   58	202	9	localObject1	Object
    //   264	10	9	localThrowable	Throwable
    //   281	3	9	localSQLException	android.database.SQLException
    //   293	85	9	localObject2	Object
    //   106	109	10	localObject3	Object
    //   256	1	10	localObject4	Object
    //   269	10	10	localObject5	Object
    //   297	91	10	localObject6	Object
    //   135	157	11	localObject7	Object
    //   166	106	12	localCloseable	java.io.Closeable
    // Exception table:
    //   from	to	target	type
    //   168	180	256	finally
    //   180	237	256	finally
    //   240	247	256	finally
    //   168	180	264	java/lang/Throwable
    //   180	237	264	java/lang/Throwable
    //   240	247	264	java/lang/Throwable
    //   266	269	269	finally
    //   137	152	281	android/database/SQLException
    //   161	168	281	android/database/SQLException
    //   247	253	281	android/database/SQLException
    //   271	281	281	android/database/SQLException
  }
  
  /* Error */
  @android.annotation.SuppressLint({"Recycle"})
  public final java.util.List<SortedContactsDao.b> a(final ContactsHolder.SortingMode paramSortingMode, final SortedContactsDao.ContactFullness paramContactFullness, final Integer paramInteger, final ContactsHolder.PhonebookFilter paramPhonebookFilter)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 53
    //   3: invokestatic 32	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_2
    //   7: ldc -57
    //   9: invokestatic 32	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   12: invokestatic 61	java/lang/System:currentTimeMillis	()J
    //   15: lstore 7
    //   17: getstatic 201	com/truecaller/calling/contacts_list/data/b:a	[I
    //   20: aload_1
    //   21: invokevirtual 73	com/truecaller/calling/contacts_list/ContactsHolder$SortingMode:ordinal	()I
    //   24: iaload
    //   25: tableswitch	default:+23->48, 1:+43->68, 2:+31->56
    //   48: new 75	c/l
    //   51: dup
    //   52: invokespecial 76	c/l:<init>	()V
    //   55: athrow
    //   56: ldc -53
    //   58: ldc -51
    //   60: invokestatic 210	c/t:a	(Ljava/lang/Object;Ljava/lang/Object;)Lc/n;
    //   63: astore 14
    //   65: goto +12 -> 77
    //   68: ldc -44
    //   70: ldc -42
    //   72: invokestatic 210	c/t:a	(Ljava/lang/Object;Ljava/lang/Object;)Lc/n;
    //   75: astore 14
    //   77: aload 14
    //   79: getfield 219	c/n:a	Ljava/lang/Object;
    //   82: checkcast 221	java/lang/String
    //   85: astore 18
    //   87: aload 14
    //   89: getfield 223	c/n:b	Ljava/lang/Object;
    //   92: checkcast 221	java/lang/String
    //   95: astore 17
    //   97: getstatic 225	com/truecaller/calling/contacts_list/data/b:b	[I
    //   100: aload_2
    //   101: invokevirtual 228	com/truecaller/calling/contacts_list/data/SortedContactsDao$ContactFullness:ordinal	()I
    //   104: iaload
    //   105: tableswitch	default:+23->128, 1:+49->154, 2:+31->136
    //   128: new 75	c/l
    //   131: dup
    //   132: invokespecial 76	c/l:<init>	()V
    //   135: athrow
    //   136: new 216	c/n
    //   139: dup
    //   140: getstatic 234	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   143: invokestatic 237	com/truecaller/content/TruecallerContract$f:b	()Landroid/net/Uri;
    //   146: invokespecial 240	c/n:<init>	(Ljava/lang/Object;Ljava/lang/Object;)V
    //   149: astore 14
    //   151: goto +18 -> 169
    //   154: new 216	c/n
    //   157: dup
    //   158: getstatic 243	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   161: invokestatic 246	com/truecaller/content/TruecallerContract$f:c	()Landroid/net/Uri;
    //   164: invokespecial 240	c/n:<init>	(Ljava/lang/Object;Ljava/lang/Object;)V
    //   167: astore 14
    //   169: aload 14
    //   171: getfield 219	c/n:a	Ljava/lang/Object;
    //   174: checkcast 230	java/lang/Boolean
    //   177: invokevirtual 249	java/lang/Boolean:booleanValue	()Z
    //   180: istore 13
    //   182: aload 14
    //   184: getfield 223	c/n:b	Ljava/lang/Object;
    //   187: checkcast 251	android/net/Uri
    //   190: astore 14
    //   192: aload 14
    //   194: astore 15
    //   196: aload_3
    //   197: ifnull +23 -> 220
    //   200: aload 14
    //   202: invokevirtual 255	android/net/Uri:buildUpon	()Landroid/net/Uri$Builder;
    //   205: ldc_w 257
    //   208: aload_3
    //   209: invokestatic 261	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   212: invokevirtual 267	android/net/Uri$Builder:appendQueryParameter	(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;
    //   215: invokevirtual 270	android/net/Uri$Builder:build	()Landroid/net/Uri;
    //   218: astore 15
    //   220: iload 13
    //   222: ifeq +12 -> 234
    //   225: aload_0
    //   226: getfield 41	com/truecaller/calling/contacts_list/data/a:a	Ljava/lang/String;
    //   229: astore 16
    //   231: goto +6 -> 237
    //   234: aconst_null
    //   235: astore 16
    //   237: aload 4
    //   239: ifnonnull +6 -> 245
    //   242: goto +53 -> 295
    //   245: getstatic 272	com/truecaller/calling/contacts_list/data/b:c	[I
    //   248: aload 4
    //   250: invokevirtual 86	com/truecaller/calling/contacts_list/ContactsHolder$PhonebookFilter:ordinal	()I
    //   253: iaload
    //   254: tableswitch	default:+22->276, 1:+33->287, 2:+25->279
    //   276: goto +19 -> 295
    //   279: ldc_w 274
    //   282: astore 14
    //   284: goto +14 -> 298
    //   287: ldc_w 276
    //   290: astore 14
    //   292: goto +6 -> 298
    //   295: aconst_null
    //   296: astore 14
    //   298: iconst_2
    //   299: anewarray 221	java/lang/String
    //   302: dup
    //   303: iconst_0
    //   304: aload 16
    //   306: aastore
    //   307: dup
    //   308: iconst_1
    //   309: aload 14
    //   311: aastore
    //   312: invokestatic 281	c/a/m:e	([Ljava/lang/Object;)Ljava/util/List;
    //   315: astore 14
    //   317: aload 14
    //   319: invokeinterface 284 1 0
    //   324: iconst_1
    //   325: if_icmpne +16 -> 341
    //   328: aload 14
    //   330: invokestatic 287	c/a/m:d	(Ljava/util/List;)Ljava/lang/Object;
    //   333: checkcast 221	java/lang/String
    //   336: astore 14
    //   338: goto +31 -> 369
    //   341: aload 14
    //   343: checkcast 188	java/lang/Iterable
    //   346: ldc_w 289
    //   349: checkcast 291	java/lang/CharSequence
    //   352: aconst_null
    //   353: aconst_null
    //   354: iconst_0
    //   355: aconst_null
    //   356: getstatic 294	com/truecaller/calling/contacts_list/data/a$f:a	Lcom/truecaller/calling/contacts_list/data/a$f;
    //   359: checkcast 296	c/g/a/b
    //   362: bipush 30
    //   364: invokestatic 299	c/a/m:a	(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lc/g/a/b;I)Ljava/lang/String;
    //   367: astore 14
    //   369: invokestatic 61	java/lang/System:currentTimeMillis	()J
    //   372: lstore 9
    //   374: aload_0
    //   375: getfield 37	com/truecaller/calling/contacts_list/data/a:b	Landroid/content/ContentResolver;
    //   378: aload 15
    //   380: aconst_null
    //   381: aload 14
    //   383: aconst_null
    //   384: aload 18
    //   386: invokevirtual 105	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   389: astore 16
    //   391: invokestatic 61	java/lang/System:currentTimeMillis	()J
    //   394: lstore 11
    //   396: new 161	java/lang/StringBuilder
    //   399: dup
    //   400: ldc_w 301
    //   403: invokespecial 166	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   406: astore 18
    //   408: aload 18
    //   410: aload 15
    //   412: invokevirtual 170	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   415: pop
    //   416: aload 18
    //   418: ldc_w 303
    //   421: invokevirtual 175	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   424: pop
    //   425: aload 18
    //   427: aload 14
    //   429: invokevirtual 175	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   432: pop
    //   433: aload 18
    //   435: ldc_w 305
    //   438: invokevirtual 175	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   441: pop
    //   442: aload 18
    //   444: lload 11
    //   446: lload 9
    //   448: lsub
    //   449: invokevirtual 180	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   452: pop
    //   453: aload 18
    //   455: ldc -74
    //   457: invokevirtual 175	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   460: pop
    //   461: aload 18
    //   463: invokevirtual 186	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   466: pop
    //   467: aload 16
    //   469: ifnull +309 -> 778
    //   472: new 307	com/truecaller/data/c
    //   475: dup
    //   476: aload 16
    //   478: aload 17
    //   480: invokespecial 310	com/truecaller/data/c:<init>	(Landroid/database/Cursor;Ljava/lang/String;)V
    //   483: astore 18
    //   485: aload 16
    //   487: ldc_w 312
    //   490: invokeinterface 315 2 0
    //   495: istore 6
    //   497: iload 13
    //   499: ifeq +64 -> 563
    //   502: new 16	com/truecaller/calling/contacts_list/data/a$d
    //   505: dup
    //   506: aload 18
    //   508: invokespecial 318	com/truecaller/calling/contacts_list/data/a$d:<init>	(Lcom/truecaller/data/c;)V
    //   511: checkcast 296	c/g/a/b
    //   514: astore 14
    //   516: new 18	com/truecaller/calling/contacts_list/data/a$e
    //   519: dup
    //   520: iload 6
    //   522: invokespecial 321	com/truecaller/calling/contacts_list/data/a$e:<init>	(I)V
    //   525: checkcast 296	c/g/a/b
    //   528: astore 15
    //   530: aload 16
    //   532: aload 14
    //   534: aload 15
    //   536: new 14	com/truecaller/calling/contacts_list/data/a$c
    //   539: dup
    //   540: aload 17
    //   542: iload 13
    //   544: aload_0
    //   545: aload_1
    //   546: aload_2
    //   547: aload_3
    //   548: aload 4
    //   550: invokespecial 324	com/truecaller/calling/contacts_list/data/a$c:<init>	(Ljava/lang/String;ZLcom/truecaller/calling/contacts_list/data/a;Lcom/truecaller/calling/contacts_list/ContactsHolder$SortingMode;Lcom/truecaller/calling/contacts_list/data/SortedContactsDao$ContactFullness;Ljava/lang/Integer;Lcom/truecaller/calling/contacts_list/ContactsHolder$PhonebookFilter;)V
    //   553: checkcast 326	c/g/a/q
    //   556: invokestatic 331	com/truecaller/util/z:a	(Landroid/database/Cursor;Lc/g/a/b;Lc/g/a/b;Lc/g/a/q;)Ljava/util/List;
    //   559: astore_1
    //   560: goto +173 -> 733
    //   563: new 333	com/truecaller/data/access/e
    //   566: dup
    //   567: aload 16
    //   569: invokespecial 336	com/truecaller/data/access/e:<init>	(Landroid/database/Cursor;)V
    //   572: astore 17
    //   574: aload 16
    //   576: checkcast 107	java/io/Closeable
    //   579: astore 15
    //   581: new 109	java/util/ArrayList
    //   584: dup
    //   585: invokespecial 110	java/util/ArrayList:<init>	()V
    //   588: checkcast 112	java/util/Collection
    //   591: astore 19
    //   593: aload 16
    //   595: invokeinterface 118 1 0
    //   600: ifeq +121 -> 721
    //   603: aload 17
    //   605: aload 16
    //   607: invokevirtual 339	com/truecaller/data/access/e:a	(Landroid/database/Cursor;)Lcom/truecaller/data/entity/Contact;
    //   610: astore 14
    //   612: aload 14
    //   614: invokestatic 342	com/truecaller/util/z:a	(Lcom/truecaller/data/entity/Contact;)Z
    //   617: ifeq +359 -> 976
    //   620: aload 4
    //   622: getstatic 346	com/truecaller/calling/contacts_list/ContactsHolder$PhonebookFilter:NON_PHONEBOOK_ONLY	Lcom/truecaller/calling/contacts_list/ContactsHolder$PhonebookFilter;
    //   625: if_acmpne +345 -> 970
    //   628: aload 14
    //   630: ifnull +335 -> 965
    //   633: aload 14
    //   635: invokevirtual 351	com/truecaller/data/entity/Contact:U	()Z
    //   638: invokestatic 354	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   641: astore_1
    //   642: goto +3 -> 645
    //   645: aload_1
    //   646: invokestatic 359	com/truecaller/utils/extensions/c:a	(Ljava/lang/Boolean;)Z
    //   649: ifne +327 -> 976
    //   652: goto +318 -> 970
    //   655: aload_1
    //   656: ifnull +345 -> 1001
    //   659: aload_1
    //   660: ldc_w 361
    //   663: invokestatic 99	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   666: aload 18
    //   668: aload 16
    //   670: invokevirtual 364	com/truecaller/data/c:a	(Landroid/database/Cursor;)Lcom/truecaller/data/entity/b;
    //   673: astore 14
    //   675: aload 16
    //   677: iload 6
    //   679: invokeinterface 134 2 0
    //   684: ifle +311 -> 995
    //   687: iconst_1
    //   688: istore 13
    //   690: goto +3 -> 693
    //   693: new 366	com/truecaller/calling/contacts_list/data/SortedContactsDao$b
    //   696: dup
    //   697: aload_1
    //   698: aload 14
    //   700: iload 13
    //   702: invokespecial 369	com/truecaller/calling/contacts_list/data/SortedContactsDao$b:<init>	(Lcom/truecaller/data/entity/Contact;Lcom/truecaller/data/entity/b;Z)V
    //   705: astore_1
    //   706: goto +3 -> 709
    //   709: aload 19
    //   711: aload_1
    //   712: invokeinterface 141 2 0
    //   717: pop
    //   718: goto -125 -> 593
    //   721: aload 19
    //   723: checkcast 143	java/util/List
    //   726: astore_1
    //   727: aload 15
    //   729: aconst_null
    //   730: invokestatic 148	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   733: aload_1
    //   734: ifnull +44 -> 778
    //   737: aload_1
    //   738: checkcast 188	java/lang/Iterable
    //   741: invokestatic 372	c/a/m:e	(Ljava/lang/Iterable;)Ljava/util/List;
    //   744: astore_1
    //   745: goto +35 -> 780
    //   748: astore_1
    //   749: aconst_null
    //   750: astore 14
    //   752: goto +9 -> 761
    //   755: astore 14
    //   757: aload 14
    //   759: athrow
    //   760: astore_1
    //   761: aload 15
    //   763: aload 14
    //   765: invokestatic 148	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   768: aload_1
    //   769: athrow
    //   770: astore_1
    //   771: goto +13 -> 784
    //   774: astore_1
    //   775: goto +59 -> 834
    //   778: aconst_null
    //   779: astore_1
    //   780: goto +67 -> 847
    //   783: astore_1
    //   784: aconst_null
    //   785: astore 14
    //   787: new 161	java/lang/StringBuilder
    //   790: dup
    //   791: ldc_w 374
    //   794: invokespecial 166	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   797: astore 15
    //   799: aload 15
    //   801: aload_1
    //   802: invokevirtual 377	java/lang/IllegalStateException:getMessage	()Ljava/lang/String;
    //   805: invokevirtual 175	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   808: pop
    //   809: new 379	com/truecaller/log/UnmutedException$d
    //   812: dup
    //   813: aload 15
    //   815: invokevirtual 186	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   818: invokespecial 380	com/truecaller/log/UnmutedException$d:<init>	(Ljava/lang/String;)V
    //   821: checkcast 51	java/lang/Throwable
    //   824: invokestatic 154	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   827: aload 14
    //   829: astore_1
    //   830: goto +17 -> 847
    //   833: astore_1
    //   834: aconst_null
    //   835: astore 14
    //   837: aload_1
    //   838: checkcast 51	java/lang/Throwable
    //   841: invokestatic 154	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   844: aload 14
    //   846: astore_1
    //   847: aload_1
    //   848: astore 14
    //   850: aload_1
    //   851: ifnonnull +11 -> 862
    //   854: getstatic 159	c/a/y:a	Lc/a/y;
    //   857: checkcast 143	java/util/List
    //   860: astore 14
    //   862: invokestatic 61	java/lang/System:currentTimeMillis	()J
    //   865: lstore 9
    //   867: new 161	java/lang/StringBuilder
    //   870: dup
    //   871: ldc_w 382
    //   874: invokespecial 166	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   877: astore_1
    //   878: aload_1
    //   879: aload_2
    //   880: invokevirtual 170	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   883: pop
    //   884: aload_1
    //   885: ldc -84
    //   887: invokevirtual 175	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   890: pop
    //   891: aload_1
    //   892: aload 4
    //   894: invokevirtual 170	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   897: pop
    //   898: aload_1
    //   899: ldc_w 384
    //   902: invokevirtual 175	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   905: pop
    //   906: aload_1
    //   907: aload_3
    //   908: invokevirtual 170	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   911: pop
    //   912: aload_1
    //   913: ldc_w 386
    //   916: invokevirtual 175	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   919: pop
    //   920: aload_1
    //   921: aload 14
    //   923: invokeinterface 284 1 0
    //   928: invokevirtual 389	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   931: pop
    //   932: aload_1
    //   933: ldc_w 391
    //   936: invokevirtual 175	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   939: pop
    //   940: aload_1
    //   941: lload 9
    //   943: lload 7
    //   945: lsub
    //   946: invokevirtual 180	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   949: pop
    //   950: aload_1
    //   951: ldc -74
    //   953: invokevirtual 175	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   956: pop
    //   957: aload_1
    //   958: invokevirtual 186	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   961: pop
    //   962: aload 14
    //   964: areturn
    //   965: aconst_null
    //   966: astore_1
    //   967: goto -322 -> 645
    //   970: iconst_1
    //   971: istore 5
    //   973: goto +6 -> 979
    //   976: iconst_0
    //   977: istore 5
    //   979: iload 5
    //   981: ifeq +9 -> 990
    //   984: aload 14
    //   986: astore_1
    //   987: goto -332 -> 655
    //   990: aconst_null
    //   991: astore_1
    //   992: goto -337 -> 655
    //   995: iconst_0
    //   996: istore 13
    //   998: goto -305 -> 693
    //   1001: aconst_null
    //   1002: astore_1
    //   1003: goto -294 -> 709
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1006	0	this	a
    //   0	1006	1	paramSortingMode	ContactsHolder.SortingMode
    //   0	1006	2	paramContactFullness	SortedContactsDao.ContactFullness
    //   0	1006	3	paramInteger	Integer
    //   0	1006	4	paramPhonebookFilter	ContactsHolder.PhonebookFilter
    //   971	9	5	i	int
    //   495	183	6	j	int
    //   15	929	7	l1	long
    //   372	570	9	l2	long
    //   394	51	11	l3	long
    //   180	817	13	bool	boolean
    //   63	688	14	localObject1	Object
    //   755	9	14	localThrowable	Throwable
    //   785	200	14	localObject2	Object
    //   194	620	15	localObject3	Object
    //   229	447	16	localObject4	Object
    //   95	509	17	localObject5	Object
    //   85	582	18	localObject6	Object
    //   591	131	19	localCollection	java.util.Collection
    // Exception table:
    //   from	to	target	type
    //   581	593	748	finally
    //   593	628	748	finally
    //   633	642	748	finally
    //   645	652	748	finally
    //   659	687	748	finally
    //   693	706	748	finally
    //   709	718	748	finally
    //   721	727	748	finally
    //   581	593	755	java/lang/Throwable
    //   593	628	755	java/lang/Throwable
    //   633	642	755	java/lang/Throwable
    //   645	652	755	java/lang/Throwable
    //   659	687	755	java/lang/Throwable
    //   693	706	755	java/lang/Throwable
    //   709	718	755	java/lang/Throwable
    //   721	727	755	java/lang/Throwable
    //   757	760	760	finally
    //   530	560	770	java/lang/IllegalStateException
    //   563	581	770	java/lang/IllegalStateException
    //   727	733	770	java/lang/IllegalStateException
    //   737	745	770	java/lang/IllegalStateException
    //   761	770	770	java/lang/IllegalStateException
    //   530	560	774	android/database/SQLException
    //   563	581	774	android/database/SQLException
    //   727	733	774	android/database/SQLException
    //   737	745	774	android/database/SQLException
    //   761	770	774	android/database/SQLException
    //   369	467	783	java/lang/IllegalStateException
    //   472	497	783	java/lang/IllegalStateException
    //   502	530	783	java/lang/IllegalStateException
    //   369	467	833	android/database/SQLException
    //   472	497	833	android/database/SQLException
    //   502	530	833	android/database/SQLException
  }
  
  public static final class a
  {
    String a;
    int b;
    
    public a(String paramString, int paramInt)
    {
      a = paramString;
      b = paramInt;
      if (b > 0) {
        paramInt = 1;
      } else {
        paramInt = 0;
      }
      if (paramInt != 0) {
        return;
      }
      throw ((Throwable)new IllegalArgumentException("Failed requirement.".toString()));
    }
    
    public final boolean equals(Object paramObject)
    {
      if (this != paramObject)
      {
        if ((paramObject instanceof a))
        {
          paramObject = (a)paramObject;
          if (k.a(a, a))
          {
            int i;
            if (b == b) {
              i = 1;
            } else {
              i = 0;
            }
            if (i != 0) {
              return true;
            }
          }
        }
        return false;
      }
      return true;
    }
    
    public final int hashCode()
    {
      String str = a;
      int i;
      if (str != null) {
        i = str.hashCode();
      } else {
        i = 0;
      }
      return i * 31 + b;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder("FastScrollIndex(groupLabel=");
      localStringBuilder.append(a);
      localStringBuilder.append(", count=");
      localStringBuilder.append(b);
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  public static final class b
    implements SortedContactsRepository.b
  {
    private Iterable<a.a> a;
    
    public b(Iterable<a.a> paramIterable)
    {
      a = paramIterable;
    }
    
    public final String a(int paramInt)
    {
      Object localObject2 = a;
      Object localObject1 = t.a(Integer.valueOf(-1), null);
      localObject2 = ((Iterable)localObject2).iterator();
      while (((Iterator)localObject2).hasNext())
      {
        a.a locala = (a.a)((Iterator)localObject2).next();
        int i = ((Number)a).intValue();
        localObject1 = (String)b;
        String str = a;
        int j = b;
        if (paramInt <= i) {
          localObject1 = t.a(Integer.valueOf(paramInt), localObject1);
        } else {
          localObject1 = t.a(Integer.valueOf(i + j), str);
        }
      }
      return (String)b;
    }
  }
  
  static final class c
    extends l
    implements q<Contact, com.truecaller.data.entity.b, Boolean, SortedContactsDao.b>
  {
    c(String paramString, boolean paramBoolean, a parama, ContactsHolder.SortingMode paramSortingMode, SortedContactsDao.ContactFullness paramContactFullness, Integer paramInteger, ContactsHolder.PhonebookFilter paramPhonebookFilter)
    {
      super();
    }
  }
  
  static final class d
    extends j
    implements c.g.a.b<Cursor, com.truecaller.data.entity.b>
  {
    d(com.truecaller.data.c paramc)
    {
      super(paramc);
    }
    
    public final c.l.c a()
    {
      return w.a(com.truecaller.data.c.class);
    }
    
    public final String b()
    {
      return "getSortingData";
    }
    
    public final String c()
    {
      return "getSortingData(Landroid/database/Cursor;)Lcom/truecaller/data/entity/ContactSortingData;";
    }
  }
  
  static final class e
    extends l
    implements c.g.a.b<Cursor, Boolean>
  {
    e(int paramInt)
    {
      super();
    }
  }
  
  static final class f
    extends l
    implements c.g.a.b<String, String>
  {
    public static final f a = new f();
    
    f()
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */