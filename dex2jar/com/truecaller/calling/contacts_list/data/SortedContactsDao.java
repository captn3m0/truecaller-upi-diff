package com.truecaller.calling.contacts_list.data;

import c.g.b.k;
import com.truecaller.calling.contacts_list.ContactsHolder.PhonebookFilter;
import com.truecaller.calling.contacts_list.ContactsHolder.SortingMode;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.b;
import java.util.List;

public abstract interface SortedContactsDao
{
  public abstract SortedContactsRepository.b a(ContactsHolder.SortingMode paramSortingMode, ContactsHolder.PhonebookFilter paramPhonebookFilter, boolean paramBoolean1, boolean paramBoolean2);
  
  public abstract List<b> a(ContactsHolder.SortingMode paramSortingMode, ContactFullness paramContactFullness, Integer paramInteger, ContactsHolder.PhonebookFilter paramPhonebookFilter);
  
  public static enum ContactFullness
  {
    static
    {
      ContactFullness localContactFullness1 = new ContactFullness("BARE_MINIMUM", 0);
      BARE_MINIMUM = localContactFullness1;
      ContactFullness localContactFullness2 = new ContactFullness("COMPLETE_WITH_ENTITIES", 1);
      COMPLETE_WITH_ENTITIES = localContactFullness2;
      $VALUES = new ContactFullness[] { localContactFullness1, localContactFullness2 };
    }
    
    private ContactFullness() {}
  }
  
  public static final class a {}
  
  public static final class b
  {
    public final Contact a;
    public final b b;
    public final boolean c;
    
    public b(Contact paramContact, b paramb, boolean paramBoolean)
    {
      a = paramContact;
      b = paramb;
      c = paramBoolean;
    }
    
    public final boolean equals(Object paramObject)
    {
      if (this != paramObject)
      {
        if ((paramObject instanceof b))
        {
          paramObject = (b)paramObject;
          if ((k.a(a, a)) && (k.a(b, b)))
          {
            int i;
            if (c == c) {
              i = 1;
            } else {
              i = 0;
            }
            if (i != 0) {
              return true;
            }
          }
        }
        return false;
      }
      return true;
    }
    
    public final int hashCode()
    {
      throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder("SortedContact(contact=");
      localStringBuilder.append(a);
      localStringBuilder.append(", sortingData=");
      localStringBuilder.append(b);
      localStringBuilder.append(", isHidden=");
      localStringBuilder.append(c);
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.SortedContactsDao
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */