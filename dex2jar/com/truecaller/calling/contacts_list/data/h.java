package com.truecaller.calling.contacts_list.data;

import c.g.b.k;
import com.truecaller.data.entity.Contact;
import javax.inject.Inject;

public final class h
  implements g
{
  private final android.support.v4.f.g<String, Boolean> a = new android.support.v4.f.g(256);
  
  public final Boolean a(Contact paramContact)
  {
    k.b(paramContact, "contact");
    paramContact = paramContact.getTcId();
    if (paramContact != null) {
      return (Boolean)a.get(paramContact);
    }
    return null;
  }
  
  public final void a(Contact paramContact, boolean paramBoolean)
  {
    k.b(paramContact, "contact");
    paramContact = paramContact.getTcId();
    if (paramContact != null)
    {
      a.put(paramContact, Boolean.valueOf(paramBoolean));
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */