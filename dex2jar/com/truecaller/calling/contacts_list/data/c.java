package com.truecaller.calling.contacts_list.data;

import android.content.ContentResolver;
import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d<a>
{
  private final Provider<ContentResolver> a;
  
  private c(Provider<ContentResolver> paramProvider)
  {
    a = paramProvider;
  }
  
  public static c a(Provider<ContentResolver> paramProvider)
  {
    return new c(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */