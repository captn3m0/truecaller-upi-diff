package com.truecaller.calling.contacts_list.data;

import c.g.b.k;
import java.util.List;

public final class SortedContactsRepository$a
{
  public final List<SortedContactsDao.b> a;
  public final SortedContactsRepository.b b;
  private final SortedContactsRepository.b c;
  
  public SortedContactsRepository$a(List<SortedContactsDao.b> paramList, SortedContactsRepository.b paramb1, SortedContactsRepository.b paramb2)
  {
    a = paramList;
    c = paramb1;
    b = paramb2;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject) {
      if ((paramObject instanceof a))
      {
        paramObject = (a)paramObject;
        if ((k.a(a, a)) && (k.a(c, c)) && (k.a(b, b))) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    Object localObject = a;
    int k = 0;
    int i;
    if (localObject != null) {
      i = localObject.hashCode();
    } else {
      i = 0;
    }
    localObject = c;
    int j;
    if (localObject != null) {
      j = localObject.hashCode();
    } else {
      j = 0;
    }
    localObject = b;
    if (localObject != null) {
      k = localObject.hashCode();
    }
    return (i * 31 + j) * 31 + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("ContactsWithIndexes(contacts=");
    localStringBuilder.append(a);
    localStringBuilder.append(", nonPhonebookContactsIndexes=");
    localStringBuilder.append(c);
    localStringBuilder.append(", phonebookContactsIndexes=");
    localStringBuilder.append(b);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.SortedContactsRepository.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */