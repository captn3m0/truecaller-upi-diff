package com.truecaller.calling.contacts_list.data;

import c.d.a.a;
import c.d.b.a.f;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.calling.contacts_list.ContactsHolder.PhonebookFilter;
import kotlinx.coroutines.ag;

@f(b="SortedContactsRepository.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.contacts_list.data.SortedContactsRepositoryImpl$getContacts$2$phonebookContactsIndexes$2")
final class d$a$h
  extends c.d.b.a.k
  implements m<ag, c<? super SortedContactsRepository.b>, Object>
{
  int a;
  private ag c;
  
  d$a$h(d.a parama, c paramc)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new h(b, paramc);
    c = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    if (a == 0)
    {
      if (!(paramObject instanceof o.b)) {
        return SortedContactsDao.a.a(b.g.a, b.i, ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY);
      }
      throw a;
    }
    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((h)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.d.a.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */