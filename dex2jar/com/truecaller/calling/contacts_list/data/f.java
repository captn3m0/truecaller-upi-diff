package com.truecaller.calling.contacts_list.data;

import javax.inject.Provider;

public final class f
  implements dagger.a.d<d>
{
  private final Provider<SortedContactsDao> a;
  private final Provider<c.d.f> b;
  
  private f(Provider<SortedContactsDao> paramProvider, Provider<c.d.f> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static f a(Provider<SortedContactsDao> paramProvider, Provider<c.d.f> paramProvider1)
  {
    return new f(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */