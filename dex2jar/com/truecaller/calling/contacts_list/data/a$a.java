package com.truecaller.calling.contacts_list.data;

import c.g.b.k;

public final class a$a
{
  String a;
  int b;
  
  public a$a(String paramString, int paramInt)
  {
    a = paramString;
    b = paramInt;
    if (b > 0) {
      paramInt = 1;
    } else {
      paramInt = 0;
    }
    if (paramInt != 0) {
      return;
    }
    throw ((Throwable)new IllegalArgumentException("Failed requirement.".toString()));
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      if ((paramObject instanceof a))
      {
        paramObject = (a)paramObject;
        if (k.a(a, a))
        {
          int i;
          if (b == b) {
            i = 1;
          } else {
            i = 0;
          }
          if (i != 0) {
            return true;
          }
        }
      }
      return false;
    }
    return true;
  }
  
  public final int hashCode()
  {
    String str = a;
    int i;
    if (str != null) {
      i = str.hashCode();
    } else {
      i = 0;
    }
    return i * 31 + b;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("FastScrollIndex(groupLabel=");
    localStringBuilder.append(a);
    localStringBuilder.append(", count=");
    localStringBuilder.append(b);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */