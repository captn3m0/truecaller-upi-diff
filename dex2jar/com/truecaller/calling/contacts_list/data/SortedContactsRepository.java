package com.truecaller.calling.contacts_list.data;

import c.d.c;
import c.g.b.k;
import com.truecaller.calling.contacts_list.ContactsHolder.SortingMode;
import java.util.List;

public abstract interface SortedContactsRepository
{
  public abstract Object a(ContactsHolder.SortingMode paramSortingMode, ContactsLoadingMode paramContactsLoadingMode, c<? super a> paramc);
  
  public static enum ContactsLoadingMode
  {
    static
    {
      ContactsLoadingMode localContactsLoadingMode1 = new ContactsLoadingMode("PHONEBOOK_LIMITED", 0);
      PHONEBOOK_LIMITED = localContactsLoadingMode1;
      ContactsLoadingMode localContactsLoadingMode2 = new ContactsLoadingMode("NON_PHONEBOOK_LIMITED", 1);
      NON_PHONEBOOK_LIMITED = localContactsLoadingMode2;
      ContactsLoadingMode localContactsLoadingMode3 = new ContactsLoadingMode("PHONEBOOK_INITIAL", 2);
      PHONEBOOK_INITIAL = localContactsLoadingMode3;
      ContactsLoadingMode localContactsLoadingMode4 = new ContactsLoadingMode("FULL_INITIAL", 3);
      FULL_INITIAL = localContactsLoadingMode4;
      ContactsLoadingMode localContactsLoadingMode5 = new ContactsLoadingMode("FULL_WITH_ENTITIES", 4);
      FULL_WITH_ENTITIES = localContactsLoadingMode5;
      $VALUES = new ContactsLoadingMode[] { localContactsLoadingMode1, localContactsLoadingMode2, localContactsLoadingMode3, localContactsLoadingMode4, localContactsLoadingMode5 };
    }
    
    private ContactsLoadingMode() {}
  }
  
  public static final class a
  {
    public final List<SortedContactsDao.b> a;
    public final SortedContactsRepository.b b;
    private final SortedContactsRepository.b c;
    
    public a(List<SortedContactsDao.b> paramList, SortedContactsRepository.b paramb1, SortedContactsRepository.b paramb2)
    {
      a = paramList;
      c = paramb1;
      b = paramb2;
    }
    
    public final boolean equals(Object paramObject)
    {
      if (this != paramObject) {
        if ((paramObject instanceof a))
        {
          paramObject = (a)paramObject;
          if ((k.a(a, a)) && (k.a(c, c)) && (k.a(b, b))) {}
        }
        else
        {
          return false;
        }
      }
      return true;
    }
    
    public final int hashCode()
    {
      Object localObject = a;
      int k = 0;
      int i;
      if (localObject != null) {
        i = localObject.hashCode();
      } else {
        i = 0;
      }
      localObject = c;
      int j;
      if (localObject != null) {
        j = localObject.hashCode();
      } else {
        j = 0;
      }
      localObject = b;
      if (localObject != null) {
        k = localObject.hashCode();
      }
      return (i * 31 + j) * 31 + k;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder("ContactsWithIndexes(contacts=");
      localStringBuilder.append(a);
      localStringBuilder.append(", nonPhonebookContactsIndexes=");
      localStringBuilder.append(c);
      localStringBuilder.append(", phonebookContactsIndexes=");
      localStringBuilder.append(b);
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  public static abstract interface b
  {
    public abstract String a(int paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.SortedContactsRepository
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */