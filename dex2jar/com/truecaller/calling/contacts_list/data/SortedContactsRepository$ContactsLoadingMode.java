package com.truecaller.calling.contacts_list.data;

public enum SortedContactsRepository$ContactsLoadingMode
{
  static
  {
    ContactsLoadingMode localContactsLoadingMode1 = new ContactsLoadingMode("PHONEBOOK_LIMITED", 0);
    PHONEBOOK_LIMITED = localContactsLoadingMode1;
    ContactsLoadingMode localContactsLoadingMode2 = new ContactsLoadingMode("NON_PHONEBOOK_LIMITED", 1);
    NON_PHONEBOOK_LIMITED = localContactsLoadingMode2;
    ContactsLoadingMode localContactsLoadingMode3 = new ContactsLoadingMode("PHONEBOOK_INITIAL", 2);
    PHONEBOOK_INITIAL = localContactsLoadingMode3;
    ContactsLoadingMode localContactsLoadingMode4 = new ContactsLoadingMode("FULL_INITIAL", 3);
    FULL_INITIAL = localContactsLoadingMode4;
    ContactsLoadingMode localContactsLoadingMode5 = new ContactsLoadingMode("FULL_WITH_ENTITIES", 4);
    FULL_WITH_ENTITIES = localContactsLoadingMode5;
    $VALUES = new ContactsLoadingMode[] { localContactsLoadingMode1, localContactsLoadingMode2, localContactsLoadingMode3, localContactsLoadingMode4, localContactsLoadingMode5 };
  }
  
  private SortedContactsRepository$ContactsLoadingMode() {}
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.data.SortedContactsRepository.ContactsLoadingMode
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */