package com.truecaller.calling.contacts_list;

import android.content.ContentResolver;
import com.truecaller.calling.dialer.s;
import dagger.a.d;
import javax.inject.Provider;

public final class n
  implements d<s>
{
  private final Provider<ContentResolver> a;
  
  private n(Provider<ContentResolver> paramProvider)
  {
    a = paramProvider;
  }
  
  public static n a(Provider<ContentResolver> paramProvider)
  {
    return new n(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */