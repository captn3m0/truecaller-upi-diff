package com.truecaller.calling.contacts_list;

import c.d.a.a;
import c.d.b.a.f;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.calling.contacts_list.data.SortedContactsDao.b;
import com.truecaller.calling.contacts_list.data.SortedContactsRepository.ContactsLoadingMode;
import com.truecaller.calling.contacts_list.data.a.a;
import com.truecaller.data.entity.b;
import com.truecaller.utils.extensions.q;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import kotlinx.coroutines.ag;

@f(b="ContactsListPresenter.kt", c={}, d="invokeSuspend", e="com.truecaller.calling.contacts_list.ContactsListPresenter$fetchContactList$1$1$indexes$1")
final class r$e$a
  extends c.d.b.a.k
  implements m<ag, c<? super List<? extends a.a>>, Object>
{
  int a;
  private ag f;
  
  r$e$a(c paramc, r.e parame, SortedContactsRepository.ContactsLoadingMode paramContactsLoadingMode, long paramLong, q paramq)
  {
    super(2, paramc);
  }
  
  public final c<x> a(Object paramObject, c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new a(paramc, b, c, d, e);
    f = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    if (a == 0)
    {
      if (!(paramObject instanceof o.b))
      {
        paramObject = (Iterable)r.d(b.f).a(ContactsHolder.FavoritesFilter.INCLUDE_NON_FAVORITES, ContactsHolder.PhonebookFilter.NON_PHONEBOOK_ONLY);
        Object localObject2 = (Map)new LinkedHashMap();
        Iterator localIterator = ((Iterable)paramObject).iterator();
        while (localIterator.hasNext())
        {
          Object localObject3 = localIterator.next();
          String str = b.c;
          localObject1 = ((Map)localObject2).get(str);
          paramObject = localObject1;
          if (localObject1 == null)
          {
            paramObject = new ArrayList();
            ((Map)localObject2).put(str, paramObject);
          }
          ((List)paramObject).add(localObject3);
        }
        paramObject = (Collection)new ArrayList(((Map)localObject2).size());
        localObject1 = ((Map)localObject2).entrySet().iterator();
        while (((Iterator)localObject1).hasNext())
        {
          localObject2 = (Map.Entry)((Iterator)localObject1).next();
          ((Collection)paramObject).add(new a.a((String)((Map.Entry)localObject2).getKey(), ((List)((Map.Entry)localObject2).getValue()).size()));
        }
        return (List)paramObject;
      }
      throw a;
    }
    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((a)a(paramObject1, (c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.r.e.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */