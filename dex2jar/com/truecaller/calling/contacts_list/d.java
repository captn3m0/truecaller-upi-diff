package com.truecaller.calling.contacts_list;

import com.truecaller.analytics.b;
import com.truecaller.backup.e;
import com.truecaller.utils.n;
import javax.inject.Provider;

public final class d
  implements dagger.a.d<c>
{
  private final Provider<b.c.b> a;
  private final Provider<b.a> b;
  private final Provider<com.truecaller.i.c> c;
  private final Provider<e> d;
  private final Provider<b> e;
  private final Provider<n> f;
  
  private d(Provider<b.c.b> paramProvider, Provider<b.a> paramProvider1, Provider<com.truecaller.i.c> paramProvider2, Provider<e> paramProvider3, Provider<b> paramProvider4, Provider<n> paramProvider5)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
  }
  
  public static d a(Provider<b.c.b> paramProvider, Provider<b.a> paramProvider1, Provider<com.truecaller.i.c> paramProvider2, Provider<e> paramProvider3, Provider<b> paramProvider4, Provider<n> paramProvider5)
  {
    return new d(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */