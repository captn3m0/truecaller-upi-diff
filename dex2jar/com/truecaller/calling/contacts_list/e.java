package com.truecaller.calling.contacts_list;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.R.id;

public final class e
  extends RecyclerView.ViewHolder
  implements b.c
{
  public e(View paramView, b.c.a parama)
  {
    super(paramView);
    ((Button)paramView.findViewById(R.id.btn_turn_on_backup)).setOnClickListener((View.OnClickListener)new View.OnClickListener()
    {
      public final void onClick(View paramAnonymousView)
      {
        a.a();
      }
    });
    ((Button)paramView.findViewById(R.id.btn_dismiss_backup)).setOnClickListener((View.OnClickListener)new View.OnClickListener()
    {
      public final void onClick(View paramAnonymousView)
      {
        a.b();
      }
    });
  }
  
  public final void a(CharSequence paramCharSequence)
  {
    Object localObject = itemView;
    k.a(localObject, "itemView");
    localObject = (TextView)((View)localObject).findViewById(R.id.title);
    k.a(localObject, "itemView.title");
    ((TextView)localObject).setText(paramCharSequence);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */