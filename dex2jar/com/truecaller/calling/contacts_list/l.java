package com.truecaller.calling.contacts_list;

import android.content.ContentResolver;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.calling.dialer.r;
import com.truecaller.calling.dialer.s;
import com.truecaller.content.TruecallerContract.e;
import com.truecaller.content.TruecallerContract.f;
import javax.inject.Named;

public abstract class l
{
  public static final b a = new b((byte)0);
  
  @Named("PhonebookObserver")
  public static final s a(ContentResolver paramContentResolver)
  {
    k.b(paramContentResolver, "contentResolver");
    Uri localUri = TruecallerContract.f.b();
    k.a(localUri, "TruecallerContract.Conta…rtedContactsWithDataUri()");
    return (s)new r(paramContentResolver, localUri, Long.valueOf(-1L));
  }
  
  @Named("ContactsSettingsObserver")
  public static final s b(ContentResolver paramContentResolver)
  {
    k.b(paramContentResolver, "contentResolver");
    Uri localUri = TruecallerContract.e.a();
    k.a(localUri, "TruecallerContract.Conta…ingsTable.getContentUri()");
    return (s)new r(paramContentResolver, localUri, Long.valueOf(-1L));
  }
  
  public static final class a
  {
    private final ContactsHolder.PhonebookFilter a;
    
    public a(ContactsHolder.PhonebookFilter paramPhonebookFilter)
    {
      a = paramPhonebookFilter;
    }
  }
  
  public static final class b {}
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */