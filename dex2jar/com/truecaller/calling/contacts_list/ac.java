package com.truecaller.calling.contacts_list;

import c.g.b.k;
import com.truecaller.ads.provider.e;
import javax.inject.Inject;

public final class ac
  implements ab
{
  private final j a;
  
  @Inject
  public ac(j paramj)
  {
    a = paramj;
  }
  
  public final ab.a a(ContactsHolder.PhonebookFilter paramPhonebookFilter)
  {
    k.b(paramPhonebookFilter, "phonebookFilter");
    paramPhonebookFilter = a.a(new l.a(paramPhonebookFilter));
    e locale = paramPhonebookFilter.b();
    locale.a(true);
    return new ab.a(locale, paramPhonebookFilter.a());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.ac
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */