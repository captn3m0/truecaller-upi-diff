package com.truecaller.calling.contacts_list;

import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.backup.e;
import com.truecaller.utils.n;
import javax.inject.Inject;

public final class c
  extends com.truecaller.adapter_delegates.c<b.c>
  implements b.b
{
  private final b.c.b b;
  private final b.a c;
  private final com.truecaller.i.c d;
  private final e e;
  private final b f;
  private final n g;
  
  @Inject
  public c(b.c.b paramb, b.a parama, com.truecaller.i.c paramc, e parame, b paramb1, n paramn)
  {
    b = paramb;
    c = parama;
    d = paramc;
    e = parame;
    f = paramb1;
    g = paramn;
  }
  
  private final void a(String paramString)
  {
    b localb = f;
    paramString = new e.a("ViewAction").a("Action", paramString).a("Context", "contacts").a();
    k.a(paramString, "AnalyticsEvent.Builder(A…\n                .build()");
    localb.b(paramString);
  }
  
  private final void c()
  {
    d.a_("contactListPromoteBackupCount");
    b.s_();
  }
  
  public final void a()
  {
    a("backupPromoClicked");
    c.t_();
    c();
  }
  
  public final void b()
  {
    a("backupPromoDismissed");
    c();
  }
  
  public final int getItemCount()
  {
    int i;
    if ((d.a("contactListPromoteBackupCount", 0) <= 0) && (!e.a())) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      return 1;
    }
    return 0;
  }
  
  public final long getItemId(int paramInt)
  {
    return 1L;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */