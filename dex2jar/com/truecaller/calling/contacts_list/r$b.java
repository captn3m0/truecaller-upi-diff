package com.truecaller.calling.contacts_list;

import c.a.m;
import c.a.y;
import c.g.a.b;
import c.g.b.k;
import com.truecaller.calling.contacts_list.data.SortedContactsDao.b;
import com.truecaller.data.entity.Contact;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

final class r$b
{
  private final List<SortedContactsDao.b>[][] a;
  
  public r$b(List<SortedContactsDao.b> paramList)
  {
    int k = ContactsHolder.FavoritesFilter.values().length;
    Object localObject1 = new List[k][];
    int i = 0;
    int j;
    while (i < k)
    {
      localObject2 = new List[ContactsHolder.PhonebookFilter.values().length];
      int m = localObject2.length;
      j = 0;
      while (j < m)
      {
        localObject2[j] = ((List)y.a);
        j += 1;
      }
      localObject1[i] = localObject2;
      i += 1;
    }
    a = ((List[][])localObject1);
    paramList = (Iterable)paramList;
    Object localObject2 = (Map)new LinkedHashMap();
    Object localObject3 = paramList.iterator();
    Object localObject4;
    Object localObject5;
    for (;;)
    {
      boolean bool = ((Iterator)localObject3).hasNext();
      j = 1;
      if (!bool) {
        break;
      }
      localObject4 = ((Iterator)localObject3).next();
      paramList = a;
      i = j;
      if (paramList.E() == null)
      {
        i = j;
        if (!paramList.Y()) {
          i = 0;
        }
      }
      localObject5 = Integer.valueOf(i);
      localObject1 = ((Map)localObject2).get(localObject5);
      paramList = (List<SortedContactsDao.b>)localObject1;
      if (localObject1 == null)
      {
        paramList = new ArrayList();
        ((Map)localObject2).put(localObject5, paramList);
      }
      ((List)paramList).add(localObject4);
    }
    localObject3 = (List)((Map)localObject2).get(Integer.valueOf(0));
    if (localObject3 != null)
    {
      paramList = ContactsHolder.FavoritesFilter.INCLUDE_NON_FAVORITES;
      localObject1 = ContactsHolder.PhonebookFilter.NON_PHONEBOOK_ONLY;
      localObject4 = (Iterable)localObject3;
      localObject3 = (Collection)new ArrayList();
      localObject4 = ((Iterable)localObject4).iterator();
      while (((Iterator)localObject4).hasNext())
      {
        localObject5 = ((Iterator)localObject4).next();
        SortedContactsDao.b localb = (SortedContactsDao.b)localObject5;
        if ((!a.U()) && (!c)) {
          i = 0;
        } else {
          i = 1;
        }
        if (i == 0) {
          ((Collection)localObject3).add(localObject5);
        }
      }
      a(paramList, (ContactsHolder.PhonebookFilter)localObject1, (List)localObject3);
    }
    paramList = (List)((Map)localObject2).get(Integer.valueOf(1));
    if (paramList != null) {
      a(ContactsHolder.FavoritesFilter.INCLUDE_NON_FAVORITES, ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY, paramList);
    }
    a(ContactsHolder.FavoritesFilter.FAVORITES_ONLY, ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY, c.m.l.d(c.m.l.c(c.m.l.a(m.n((Iterable)a(ContactsHolder.FavoritesFilter.INCLUDE_NON_FAVORITES, ContactsHolder.PhonebookFilter.PHONEBOOK_ONLY)), (b)1.a), (b)2.a)));
  }
  
  private static int a(ContactsHolder.FavoritesFilter paramFavoritesFilter)
  {
    switch (t.a[paramFavoritesFilter.ordinal()])
    {
    default: 
      throw new c.l();
    case 2: 
      return 1;
    }
    return 0;
  }
  
  private static int a(ContactsHolder.PhonebookFilter paramPhonebookFilter)
  {
    switch (t.b[paramPhonebookFilter.ordinal()])
    {
    default: 
      throw new c.l();
    case 2: 
      return 1;
    }
    return 0;
  }
  
  public final List<SortedContactsDao.b> a(ContactsHolder.FavoritesFilter paramFavoritesFilter, ContactsHolder.PhonebookFilter paramPhonebookFilter)
  {
    k.b(paramFavoritesFilter, "favoritesFilter");
    k.b(paramPhonebookFilter, "phonebookFilter");
    return a[a(paramFavoritesFilter)][a(paramPhonebookFilter)];
  }
  
  final void a(ContactsHolder.FavoritesFilter paramFavoritesFilter, ContactsHolder.PhonebookFilter paramPhonebookFilter, List<SortedContactsDao.b> paramList)
  {
    a[a(paramFavoritesFilter)][a(paramPhonebookFilter)] = paramList;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.r.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */