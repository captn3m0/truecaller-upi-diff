package com.truecaller.calling.contacts_list;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

final class k$b
  implements DialogInterface.OnClickListener
{
  k$b(k paramk) {}
  
  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    a.d().a(ContactsHolder.SortingMode.values()[paramInt]);
    a.d().f();
    paramDialogInterface.dismiss();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.k.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */