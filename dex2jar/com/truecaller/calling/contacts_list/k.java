package com.truecaller.calling.contacts_list;

import android.arch.lifecycle.e.b;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.f;
import android.support.v4.view.ViewPager.i;
import android.support.v4.view.o;
import android.support.v7.app.AlertDialog.Builder;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import c.u;
import com.truecaller.R.id;
import com.truecaller.backup.n;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.calling.c.c;
import com.truecaller.calling.c.c.a;
import com.truecaller.calling.dialer.LifecycleAwareCondition;
import com.truecaller.calling.dialer.s;
import com.truecaller.calling.dialer.v;
import com.truecaller.common.ui.b;
import com.truecaller.data.entity.Contact;
import com.truecaller.ui.ab;
import com.truecaller.ui.ae;
import com.truecaller.ui.ae.a;
import com.truecaller.ui.components.FloatingActionButton.a;
import com.truecaller.ui.components.FloatingActionButton.b;
import com.truecaller.ui.components.FloatingActionButton.c;
import com.truecaller.ui.components.FloatingActionButton.c.a;
import com.truecaller.ui.components.i;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.util.y;
import com.truecaller.utils.extensions.t;
import com.truecaller.voip.ai;
import java.util.HashMap;
import javax.inject.Inject;
import javax.inject.Named;

public final class k
  extends Fragment
  implements p.b, b, ab, ae, FloatingActionButton.c
{
  @Inject
  public p.a a;
  @Inject
  @Named("PhonebookObserver")
  public s b;
  @Inject
  @Named("ContactsSettingsObserver")
  public s c;
  @Inject
  public q d;
  @Inject
  public ai e;
  private final int f;
  private boolean g = true;
  private HashMap h;
  
  private View a(int paramInt)
  {
    if (h == null) {
      h = new HashMap();
    }
    View localView2 = (View)h.get(Integer.valueOf(paramInt));
    View localView1 = localView2;
    if (localView2 == null)
    {
      localView1 = getView();
      if (localView1 == null) {
        return null;
      }
      localView1 = localView1.findViewById(paramInt);
      h.put(Integer.valueOf(paramInt), localView1);
    }
    return localView1;
  }
  
  public final void a()
  {
    Object localObject = (TabLayout)a(R.id.tabs_layout);
    c.g.b.k.a(localObject, "tabs_layout");
    t.b((View)localObject);
    g = false;
    f localf = getActivity();
    localObject = localf;
    if (!(localf instanceof ae.a)) {
      localObject = null;
    }
    localObject = (ae.a)localObject;
    if (localObject != null)
    {
      ((ae.a)localObject).d();
      return;
    }
  }
  
  public final void a(Contact paramContact)
  {
    c.g.b.k.b(paramContact, "contact");
    f localf = getActivity();
    if (localf == null) {
      return;
    }
    c.g.b.k.a(localf, "activity ?: return");
    c.a locala = c.e;
    c.a.a(localf, paramContact, paramContact.A(), false, false, true, false, "contacts", 184);
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject = a;
    if (localObject == null) {
      c.g.b.k.a("presenter");
    }
    ((p.a)localObject).j();
    localObject = d;
    if (localObject == null) {
      c.g.b.k.a("pagerAdapter");
    }
    ViewPager localViewPager = (ViewPager)a(R.id.view_pager);
    c.g.b.k.a(localViewPager, "view_pager");
    ((q)localObject).b(localViewPager.getCurrentItem());
  }
  
  public final void b()
  {
    f localf = getActivity();
    Object localObject = localf;
    if (!(localf instanceof FloatingActionButton.c.a)) {
      localObject = null;
    }
    localObject = (FloatingActionButton.c.a)localObject;
    if (localObject != null)
    {
      ((FloatingActionButton.c.a)localObject).h();
      return;
    }
  }
  
  public final void b(Contact paramContact)
  {
    c.g.b.k.b(paramContact, "contact");
    ai localai = e;
    if (localai == null) {
      c.g.b.k.a("voipUtil");
    }
    localai.a(getActivity(), paramContact, "contacts");
  }
  
  public final void c(Contact paramContact)
  {
    c.g.b.k.b(paramContact, "contact");
    DetailsFragment.b(getContext(), paramContact, DetailsFragment.SourceType.Contacts, false, true);
  }
  
  public final p.a d()
  {
    p.a locala = a;
    if (locala == null) {
      c.g.b.k.a("presenter");
    }
    return locala;
  }
  
  public final q e()
  {
    q localq = d;
    if (localq == null) {
      c.g.b.k.a("pagerAdapter");
    }
    return localq;
  }
  
  public final int f()
  {
    return f;
  }
  
  public final boolean g()
  {
    return g;
  }
  
  public final void h()
  {
    y.a((Fragment)this);
  }
  
  public final int i()
  {
    return 2131233811;
  }
  
  public final i[] j()
  {
    return null;
  }
  
  public final FloatingActionButton.a k()
  {
    return (FloatingActionButton.a)new a(this);
  }
  
  public final boolean l()
  {
    p.a locala = a;
    if (locala == null) {
      c.g.b.k.a("presenter");
    }
    return locala.e();
  }
  
  public final void m() {}
  
  public final void n()
  {
    Object localObject = a;
    if (localObject == null) {
      c.g.b.k.a("presenter");
    }
    ((p.a)localObject).i();
    localObject = d;
    if (localObject == null) {
      c.g.b.k.a("pagerAdapter");
    }
    ViewPager localViewPager = (ViewPager)a(R.id.view_pager);
    c.g.b.k.a(localViewPager, "view_pager");
    ((q)localObject).a(localViewPager.getCurrentItem());
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getContext();
    if (paramBundle != null) {
      paramBundle = paramBundle.getApplicationContext();
    } else {
      paramBundle = null;
    }
    if (paramBundle != null)
    {
      ((bk)paramBundle).a().ct().a(this);
      setHasOptionsMenu(true);
      paramBundle = a;
      if (paramBundle == null) {
        c.g.b.k.a("presenter");
      }
      s locals = b;
      if (locals == null) {
        c.g.b.k.a("contactsListObserver");
      }
      android.arch.lifecycle.e locale = getLifecycle();
      c.g.b.k.a(locale, "lifecycle");
      locals.a((com.truecaller.calling.dialer.q)new LifecycleAwareCondition(locale, e.b.d));
      paramBundle.a((v)locals);
      paramBundle = a;
      if (paramBundle == null) {
        c.g.b.k.a("presenter");
      }
      locals = c;
      if (locals == null) {
        c.g.b.k.a("contactsSettingsObserver");
      }
      locale = getLifecycle();
      c.g.b.k.a(locale, "lifecycle");
      locals.a((com.truecaller.calling.dialer.q)new LifecycleAwareCondition(locale, e.b.d));
      paramBundle.b((v)locals);
      paramBundle = a;
      if (paramBundle == null) {
        c.g.b.k.a("presenter");
      }
      paramBundle.b(this);
      return;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.GraphHolder");
  }
  
  public final void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    c.g.b.k.b(paramMenu, "menu");
    c.g.b.k.b(paramMenuInflater, "inflater");
    paramMenuInflater.inflate(2131623942, paramMenu);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    c.g.b.k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131559063, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    p.a locala = a;
    if (locala == null) {
      c.g.b.k.a("presenter");
    }
    locala.x_();
    super.onDestroy();
  }
  
  public final void onDestroyView()
  {
    Object localObject = a;
    if (localObject == null) {
      c.g.b.k.a("presenter");
    }
    ((p.a)localObject).y_();
    localObject = d;
    if (localObject == null) {
      c.g.b.k.a("pagerAdapter");
    }
    localObject = b;
    int j = localObject.length;
    int i = 0;
    while (i < j)
    {
      com.truecaller.ads.provider.e locale = localObject[i];
      if (locale != null)
      {
        locale = a;
        if (locale != null)
        {
          locale.e();
          locale.a(null);
        }
      }
      i += 1;
    }
    super.onDestroyView();
    localObject = h;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    c.g.b.k.b(paramMenuItem, "item");
    if (paramMenuItem.getItemId() != 2131364554) {
      return super.onOptionsItemSelected(paramMenuItem);
    }
    paramMenuItem = getContext();
    if (paramMenuItem != null)
    {
      paramMenuItem = new AlertDialog.Builder(paramMenuItem);
      p.a locala = a;
      if (locala == null) {
        c.g.b.k.a("presenter");
      }
      paramMenuItem.setSingleChoiceItems(2130903063, locala.b().ordinal(), (DialogInterface.OnClickListener)new b(this)).show();
      return true;
    }
    return false;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    c.g.b.k.b(paramView, "view");
    paramView = (ViewPager)a(R.id.view_pager);
    c.g.b.k.a(paramView, "view_pager");
    paramBundle = d;
    if (paramBundle == null) {
      c.g.b.k.a("pagerAdapter");
    }
    paramView.setAdapter((o)paramBundle);
    ((ViewPager)a(R.id.view_pager)).a((ViewPager.f)new c(this));
  }
  
  public final void t_()
  {
    new n().show(getFragmentManager(), n.class.getSimpleName());
  }
  
  public static final class a
    extends FloatingActionButton.b
  {
    public final void a()
    {
      a.d().g();
    }
  }
  
  static final class b
    implements DialogInterface.OnClickListener
  {
    b(k paramk) {}
    
    public final void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
      a.d().a(ContactsHolder.SortingMode.values()[paramInt]);
      a.d().f();
      paramDialogInterface.dismiss();
    }
  }
  
  public static final class c
    extends ViewPager.i
  {
    public final void onPageSelected(int paramInt)
    {
      a.d().a(a.e().c[paramInt]);
      a.e().a(paramInt);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */