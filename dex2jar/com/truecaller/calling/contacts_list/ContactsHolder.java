package com.truecaller.calling.contacts_list;

import com.truecaller.calling.contacts_list.data.SortedContactsDao.b;
import java.util.List;

public abstract interface ContactsHolder
{
  public abstract String a(int paramInt, PhonebookFilter paramPhonebookFilter);
  
  public abstract List<SortedContactsDao.b> a(FavoritesFilter paramFavoritesFilter, PhonebookFilter paramPhonebookFilter);
  
  public abstract void a(SortingMode paramSortingMode);
  
  public abstract SortingMode b();
  
  public static enum FavoritesFilter
  {
    static
    {
      FavoritesFilter localFavoritesFilter1 = new FavoritesFilter("INCLUDE_NON_FAVORITES", 0);
      INCLUDE_NON_FAVORITES = localFavoritesFilter1;
      FavoritesFilter localFavoritesFilter2 = new FavoritesFilter("FAVORITES_ONLY", 1);
      FAVORITES_ONLY = localFavoritesFilter2;
      $VALUES = new FavoritesFilter[] { localFavoritesFilter1, localFavoritesFilter2 };
    }
    
    private FavoritesFilter() {}
  }
  
  public static enum PhonebookFilter
  {
    static
    {
      PhonebookFilter localPhonebookFilter1 = new PhonebookFilter("NON_PHONEBOOK_ONLY", 0);
      NON_PHONEBOOK_ONLY = localPhonebookFilter1;
      PhonebookFilter localPhonebookFilter2 = new PhonebookFilter("PHONEBOOK_ONLY", 1);
      PHONEBOOK_ONLY = localPhonebookFilter2;
      $VALUES = new PhonebookFilter[] { localPhonebookFilter1, localPhonebookFilter2 };
    }
    
    private PhonebookFilter() {}
  }
  
  public static enum SortingMode
  {
    static
    {
      SortingMode localSortingMode1 = new SortingMode("BY_FIRST_NAME", 0);
      BY_FIRST_NAME = localSortingMode1;
      SortingMode localSortingMode2 = new SortingMode("BY_LAST_NAME", 1);
      BY_LAST_NAME = localSortingMode2;
      $VALUES = new SortingMode[] { localSortingMode1, localSortingMode2 };
    }
    
    private SortingMode() {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.ContactsHolder
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */