package com.truecaller.calling.contacts_list;

import com.truecaller.analytics.b;
import com.truecaller.calling.contacts_list.data.g;
import com.truecaller.search.local.model.c;
import dagger.a.d;
import javax.inject.Provider;

public final class aa
  implements d<z>
{
  private final Provider<ContactsHolder> a;
  private final Provider<c> b;
  private final Provider<ae> c;
  private final Provider<b> d;
  private final Provider<g> e;
  
  private aa(Provider<ContactsHolder> paramProvider, Provider<c> paramProvider1, Provider<ae> paramProvider2, Provider<b> paramProvider3, Provider<g> paramProvider4)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static aa a(Provider<ContactsHolder> paramProvider, Provider<c> paramProvider1, Provider<ae> paramProvider2, Provider<b> paramProvider3, Provider<g> paramProvider4)
  {
    return new aa(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.aa
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */