package com.truecaller.calling.contacts_list;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import c.g.a.b;
import c.g.b.k;
import c.k.e;
import c.k.h;
import c.k.i;
import com.truecaller.R.id;
import com.truecaller.R.styleable;
import com.truecaller.ui.view.TintedImageView;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class FastScroller
  extends RelativeLayout
{
  RecyclerView a;
  LinearLayoutManager b;
  b<? super Integer, String> c;
  private final int d;
  private int e;
  private boolean f;
  private HashMap g;
  
  public FastScroller(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.FastScroller, 0, 0);
    d = paramContext.getDimensionPixelSize(0, 100);
    paramContext.recycle();
  }
  
  private View a(int paramInt)
  {
    if (g == null) {
      g = new HashMap();
    }
    View localView2 = (View)g.get(Integer.valueOf(paramInt));
    View localView1 = localView2;
    if (localView2 == null)
    {
      localView1 = findViewById(paramInt);
      g.put(Integer.valueOf(paramInt), localView1);
    }
    return localView1;
  }
  
  private final void b()
  {
    if (!f)
    {
      RecyclerView localRecyclerView = a;
      if (localRecyclerView == null) {
        k.a("recyclerView");
      }
      int i = localRecyclerView.computeVerticalScrollOffset();
      localRecyclerView = a;
      if (localRecyclerView == null) {
        k.a("recyclerView");
      }
      int j = localRecyclerView.computeVerticalScrollRange();
      float f1 = i / j;
      setContainerAndScrollBarPosition(getHeight() * f1);
    }
  }
  
  private final void setContainerAndScrollBarPosition(float paramFloat)
  {
    Object localObject = (TintedImageView)a(R.id.fast_scroller_bar);
    k.a(localObject, "fast_scroller_bar");
    int i = ((TintedImageView)localObject).getHeight();
    localObject = (FrameLayout)a(R.id.fast_scroller_bubble);
    k.a(localObject, "fast_scroller_bubble");
    int j = ((FrameLayout)localObject).getHeight();
    localObject = (TintedImageView)a(R.id.fast_scroller_bar);
    k.a(localObject, "fast_scroller_bar");
    int k = i / 2;
    ((TintedImageView)localObject).setY(i.a((int)(paramFloat - k), (e)new h(0, getHeight() - i)));
    localObject = (FrameLayout)a(R.id.fast_scroller_bubble);
    k.a(localObject, "fast_scroller_bubble");
    ((FrameLayout)localObject).setY(i.a((int)(paramFloat - j), (e)new h(0, getHeight() - j - k)));
  }
  
  private final void setRecyclerViewPosition(float paramFloat)
  {
    if (e > 0)
    {
      paramFloat /= getHeight();
      int i = e;
      i = i.a((int)(paramFloat * i), (e)i.b(0, i));
      "cl: scrollToPositionWithOffset ".concat(String.valueOf(i));
      Object localObject = b;
      if (localObject == null) {
        k.a("layoutManager");
      }
      ((LinearLayoutManager)localObject).scrollToPositionWithOffset(i, 0);
      localObject = (TextView)a(R.id.fast_scroller_bubble_text);
      k.a(localObject, "fast_scroller_bubble_text");
      b localb = c;
      if (localb == null) {
        k.a("indexByPosition");
      }
      ((TextView)localObject).setText((CharSequence)localb.invoke(Integer.valueOf(i)));
      localObject = new StringBuilder("cl: indexByPosition, group = ");
      ((StringBuilder)localObject).append((TextView)a(R.id.fast_scroller_bubble_text));
      ((StringBuilder)localObject).append(".text");
      ((StringBuilder)localObject).toString();
    }
  }
  
  public final void a()
  {
    Object localObject = b;
    if (localObject == null) {
      k.a("layoutManager");
    }
    int j = ((LinearLayoutManager)localObject).findLastVisibleItemPosition();
    localObject = b;
    if (localObject == null) {
      k.a("layoutManager");
    }
    int k = ((LinearLayoutManager)localObject).findFirstVisibleItemPosition();
    boolean bool = true;
    localObject = a;
    if (localObject == null) {
      k.a("recyclerView");
    }
    localObject = ((RecyclerView)localObject).getAdapter();
    int i;
    if (localObject != null) {
      i = ((RecyclerView.Adapter)localObject).getItemCount();
    } else {
      i = 0;
    }
    e = i;
    if (e <= j - k + 1) {
      bool = false;
    }
    t.a(this, bool);
    b();
  }
  
  public final boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    k.b(paramMotionEvent, "event");
    if (!f)
    {
      int i;
      if (getLayoutDirection() == 1)
      {
        if (paramMotionEvent.getX() < d) {
          i = 1;
        } else {
          i = 0;
        }
      }
      else if (paramMotionEvent.getX() > getWidth() - d) {
        i = 1;
      } else {
        i = 0;
      }
      if (i == 0) {
        return super.onTouchEvent(paramMotionEvent);
      }
    }
    switch (paramMotionEvent.getAction())
    {
    default: 
      return super.onTouchEvent(paramMotionEvent);
    case 2: 
      setContainerAndScrollBarPosition(paramMotionEvent.getY());
      setRecyclerViewPosition(paramMotionEvent.getY());
      return true;
    case 1: 
    case 3: 
      f = false;
      paramMotionEvent = (FrameLayout)a(R.id.fast_scroller_bubble);
      k.a(paramMotionEvent, "fast_scroller_bubble");
      t.c((View)paramMotionEvent);
      paramMotionEvent = (TintedImageView)a(R.id.fast_scroller_bar);
      k.a(paramMotionEvent, "fast_scroller_bar");
      paramMotionEvent.setSelected(false);
      return true;
    }
    f = true;
    Object localObject = (FrameLayout)a(R.id.fast_scroller_bubble);
    k.a(localObject, "fast_scroller_bubble");
    t.a((View)localObject);
    localObject = (TintedImageView)a(R.id.fast_scroller_bar);
    k.a(localObject, "fast_scroller_bar");
    ((TintedImageView)localObject).setSelected(true);
    setContainerAndScrollBarPosition(paramMotionEvent.getY());
    setRecyclerViewPosition(paramMotionEvent.getY());
    return true;
  }
  
  public static final class a
    extends RecyclerView.OnScrollListener
  {
    public final void onScrolled(RecyclerView paramRecyclerView, int paramInt1, int paramInt2)
    {
      k.b(paramRecyclerView, "recyclerView");
      FastScroller.a(a);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.FastScroller
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */