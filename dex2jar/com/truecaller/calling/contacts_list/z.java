package com.truecaller.calling.contacts_list;

import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.calling.contacts_list.data.SortedContactsDao.b;
import com.truecaller.calling.contacts_list.data.g;
import com.truecaller.search.local.model.c;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;

public final class z
  implements y
{
  final ContactsHolder a;
  private final c b;
  private final ae c;
  private final b d;
  private final g e;
  
  @Inject
  public z(ContactsHolder paramContactsHolder, @Named("ContactsAvailabilityManager") c paramc, ae paramae, b paramb, g paramg)
  {
    a = paramContactsHolder;
    b = paramc;
    c = paramae;
    d = paramb;
    e = paramg;
  }
  
  public final g.b a(final ContactsHolder.PhonebookFilter paramPhonebookFilter, final ContactsHolder.FavoritesFilter paramFavoritesFilter)
  {
    k.b(paramPhonebookFilter, "phonebookFilter");
    k.b(paramFavoritesFilter, "favoritesFilter");
    return (g.b)new h((g.a)new a(this, paramFavoritesFilter, paramPhonebookFilter), b, c, d, e);
  }
  
  public static final class a
    implements g.a
  {
    a(ContactsHolder.FavoritesFilter paramFavoritesFilter, ContactsHolder.PhonebookFilter paramPhonebookFilter) {}
    
    public final List<SortedContactsDao.b> a()
    {
      return a.a.a(paramFavoritesFilter, paramPhonebookFilter);
    }
    
    public final ContactsHolder.SortingMode b()
    {
      return a.a.b();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.contacts_list.z
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */