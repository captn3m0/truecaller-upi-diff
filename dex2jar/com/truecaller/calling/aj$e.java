package com.truecaller.calling;

import android.os.Bundle;

public final class aj$e
  extends aj
{
  final Integer d;
  public final Integer e;
  
  public aj$e(String paramString, long paramLong, Integer paramInteger1, Integer paramInteger2)
  {
    super(paramString, paramLong, (byte)0);
    d = paramInteger1;
    e = paramInteger2;
  }
  
  public final Bundle a()
  {
    Bundle localBundle = new Bundle();
    localBundle.putInt("CALL_STATE", 1);
    Integer localInteger = e;
    if (localInteger != null) {
      localBundle.putInt("ACTION", ((Number)localInteger).intValue());
    }
    if (a != null) {
      localBundle.putString("NUMBER", a);
    }
    localInteger = d;
    if (localInteger != null) {
      localBundle.putInt("SIM_SLOT_INDEX", ((Number)localInteger).intValue());
    }
    return localBundle;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.aj.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */