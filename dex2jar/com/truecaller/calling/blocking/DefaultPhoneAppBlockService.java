package com.truecaller.calling.blocking;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.telecom.Call.Details;
import android.telecom.CallScreeningService;
import android.telecom.CallScreeningService.CallResponse.Builder;
import c.n.m;
import c.u;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.bp;
import com.truecaller.callerid.CallerIdService;
import com.truecaller.calling.aj;
import com.truecaller.calling.aj.b;
import com.truecaller.calling.aj.e;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.h.c;

public final class DefaultPhoneAppBlockService
  extends CallScreeningService
{
  private final void a(Call.Details paramDetails)
  {
    respondToCall(paramDetails, new CallScreeningService.CallResponse.Builder().setDisallowCall(false).setSkipNotification(false).build());
  }
  
  public final void onScreenCall(Call.Details paramDetails)
  {
    c.g.b.k.b(paramDetails, "details");
    Object localObject1 = paramDetails.getIntentExtras();
    Object localObject2 = null;
    if (localObject1 != null) {
      localObject1 = (Uri)((Bundle)localObject1).getParcelable("android.telecom.extra.INCOMING_CALL_ADDRESS");
    } else {
      localObject1 = null;
    }
    if (localObject1 != null) {
      localObject2 = ((Uri)localObject1).getSchemeSpecificPart();
    }
    localObject1 = Uri.decode((String)localObject2);
    localObject2 = (CharSequence)localObject1;
    int i;
    if ((localObject2 != null) && (!m.a((CharSequence)localObject2))) {
      i = 0;
    } else {
      i = 1;
    }
    if (i != 0) {
      return;
    }
    localObject2 = getApplicationContext();
    if (localObject2 != null)
    {
      localObject2 = ((TrueApp)localObject2).a();
      c.g.b.k.a(localObject2, "(applicationContext as TrueApp).objectsGraph");
      Object localObject3 = ((bp)localObject2).aF();
      c.g.b.k.a(localObject3, "trueGraph.featuresRegistry()");
      if (f.a((e)localObject3, e.a[17]).a())
      {
        com.truecaller.calling.k localk = ((bp)localObject2).ac();
        c.g.b.k.a(localk, "trueGraph.callProcessor()");
        Object localObject4 = new aj.e((String)localObject1, System.currentTimeMillis());
        localObject3 = (Context)this;
        localObject4 = localk.a((Context)localObject3, (aj)localObject4);
        if (localObject4 == null)
        {
          a(paramDetails);
          return;
        }
        if ((localObject4 instanceof aj.e))
        {
          CallerIdService.a((Context)localObject3, ((aj)localObject4).a());
          localObject4 = e;
          if ((localObject4 != null) && (((Integer)localObject4).intValue() == 1))
          {
            respondToCall(paramDetails, new CallScreeningService.CallResponse.Builder().setDisallowCall(true).setSkipNotification(true).build());
            paramDetails = localk.a((Context)localObject3, (aj)new aj.b((String)localObject1, System.currentTimeMillis()));
            if (paramDetails != null) {
              CallerIdService.a((Context)localObject3, paramDetails.a());
            }
            return;
          }
          if ((localObject4 != null) && (((Integer)localObject4).intValue() == 3))
          {
            ((c)((bp)localObject2).aN().a()).a().c();
          }
          else
          {
            a(paramDetails);
            return;
          }
        }
        a(paramDetails);
      }
      return;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.TrueApp");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.blocking.DefaultPhoneAppBlockService
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */