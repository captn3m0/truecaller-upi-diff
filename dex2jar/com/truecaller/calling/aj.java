package com.truecaller.calling;

import android.os.Bundle;
import c.g.b.k;
import c.u;
import java.util.concurrent.TimeUnit;

public abstract class aj
{
  public static final a c = new a((byte)0);
  String a;
  final long b;
  
  private aj(String paramString, long paramLong)
  {
    a = paramString;
    b = paramLong;
  }
  
  public abstract Bundle a();
  
  public boolean equals(Object paramObject)
  {
    if ((aj)this == paramObject) {
      return true;
    }
    Class localClass = getClass();
    Object localObject;
    if (paramObject != null) {
      localObject = paramObject.getClass();
    } else {
      localObject = null;
    }
    if ((k.a(localClass, localObject) ^ true)) {
      return false;
    }
    if (paramObject != null)
    {
      localObject = a;
      paramObject = (aj)paramObject;
      if ((k.a(localObject, a) ^ true)) {
        return false;
      }
      return Math.abs(b - b) <= TimeUnit.SECONDS.toMillis(15L);
    }
    throw new u("null cannot be cast to non-null type com.truecaller.calling.PhoneState");
  }
  
  public int hashCode()
  {
    String str = a;
    int i;
    if (str != null) {
      i = str.hashCode();
    } else {
      i = 0;
    }
    return i + getClass().hashCode();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(getClass().getSimpleName());
    localStringBuilder.append(", number: ");
    localStringBuilder.append(a);
    localStringBuilder.append(", time: ");
    localStringBuilder.append(b);
    return localStringBuilder.toString();
  }
  
  public static final class a {}
  
  public static final class b
    extends aj
  {
    public b(String paramString, long paramLong)
    {
      super(paramLong, (byte)0);
    }
    
    public final Bundle a()
    {
      Bundle localBundle = new Bundle();
      localBundle.putInt("CALL_STATE", 3);
      if (a != null) {
        localBundle.putString("NUMBER", a);
      }
      return localBundle;
    }
  }
  
  public static final class c
    extends aj
  {
    public c(String paramString, long paramLong)
    {
      super(paramLong, (byte)0);
    }
    
    public final Bundle a()
    {
      Bundle localBundle = new Bundle();
      localBundle.putInt("CALL_STATE", 2);
      if (a != null) {
        localBundle.putString("NUMBER", a);
      }
      return localBundle;
    }
  }
  
  public static final class d
    extends aj
  {
    public d(String paramString, long paramLong)
    {
      super(paramLong, (byte)0);
    }
    
    public final Bundle a()
    {
      Bundle localBundle = new Bundle();
      localBundle.putInt("CALL_STATE", 0);
      if (a != null) {
        localBundle.putString("NUMBER", a);
      }
      return localBundle;
    }
  }
  
  public static final class e
    extends aj
  {
    final Integer d;
    public final Integer e;
    
    public e(String paramString, long paramLong, Integer paramInteger1, Integer paramInteger2)
    {
      super(paramLong, (byte)0);
      d = paramInteger1;
      e = paramInteger2;
    }
    
    public final Bundle a()
    {
      Bundle localBundle = new Bundle();
      localBundle.putInt("CALL_STATE", 1);
      Integer localInteger = e;
      if (localInteger != null) {
        localBundle.putInt("ACTION", ((Number)localInteger).intValue());
      }
      if (a != null) {
        localBundle.putString("NUMBER", a);
      }
      localInteger = d;
      if (localInteger != null) {
        localBundle.putInt("SIM_SLOT_INDEX", ((Number)localInteger).intValue());
      }
      return localBundle;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.aj
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */