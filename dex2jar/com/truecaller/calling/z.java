package com.truecaller.calling;

import android.view.View;
import android.view.View.OnClickListener;
import c.a.y;
import c.f;
import c.g.b.k;
import com.truecaller.flashsdk.ui.CompoundFlashButton;
import com.truecaller.utils.extensions.t;
import java.util.List;

public final class z
  implements w
{
  private final f b;
  private final f c;
  
  public z(View paramView)
  {
    b = t.a(paramView, 2131363109);
    c = t.a(paramView, 2131361842);
    b().setOnClickListener((View.OnClickListener)new View.OnClickListener()
    {
      public final void onClick(View paramAnonymousView)
      {
        z.a(a).performClick();
      }
    });
  }
  
  private final CompoundFlashButton a()
  {
    return (CompoundFlashButton)b.b();
  }
  
  private final View b()
  {
    return (View)c.b();
  }
  
  public final void a(x paramx)
  {
    CompoundFlashButton localCompoundFlashButton = a();
    Object localObject2;
    if (paramx != null)
    {
      localObject2 = a;
      localObject1 = localObject2;
      if (localObject2 != null) {}
    }
    else
    {
      localObject1 = (List)y.a;
    }
    Object localObject3;
    if (paramx != null)
    {
      localObject3 = ae.a(paramx);
      localObject2 = localObject3;
      if (localObject3 != null) {}
    }
    else
    {
      localObject2 = "";
    }
    if (paramx != null)
    {
      String str = c;
      localObject3 = str;
      if (str != null) {}
    }
    else
    {
      localObject3 = "";
    }
    localCompoundFlashButton.a((List)localObject1, (String)localObject2, (String)localObject3);
    Object localObject1 = b();
    k.a(localObject1, "actionOneClickArea");
    boolean bool;
    if (paramx != null) {
      bool = true;
    } else {
      bool = false;
    }
    t.a((View)localObject1, bool);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.z
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */