package com.truecaller.calling;

import c.g.b.k;
import com.truecaller.androidactors.w;
import com.truecaller.common.h.d;

public final class g
  implements f
{
  private final com.truecaller.androidactors.f<com.truecaller.h.c> a;
  private final d b;
  private final com.truecaller.i.c c;
  
  public g(com.truecaller.androidactors.f<com.truecaller.h.c> paramf, d paramd, com.truecaller.i.c paramc)
  {
    a = paramf;
    b = paramd;
    c = paramc;
  }
  
  public final aj a(aj paramaj)
  {
    k.b(paramaj, "phoneState");
    if ((paramaj instanceof aj.e))
    {
      aj.e locale = (aj.e)paramaj;
      Integer localInteger = e;
      if ((localInteger != null) && (localInteger.intValue() == 1))
      {
        ((com.truecaller.h.c)a.a()).a().c();
        if (!b.a())
        {
          c.b("failedToBlockAtLeastOnce", true);
          return (aj)new aj.e(a, b, d, Integer.valueOf(12785645));
        }
        return paramaj;
      }
      if (localInteger == null) {
        return paramaj;
      }
      if (localInteger.intValue() == 3)
      {
        ((com.truecaller.h.c)a.a()).a().c();
        return paramaj;
      }
    }
    return paramaj;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */