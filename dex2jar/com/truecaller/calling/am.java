package com.truecaller.calling;

import android.content.Context;
import c.g.b.k;
import c.u;
import com.truecaller.TrueApp;
import com.truecaller.bp;

public final class am
{
  public static final ak a(Context paramContext)
  {
    k.b(paramContext, "context");
    paramContext = paramContext.getApplicationContext();
    if (paramContext != null)
    {
      paramContext = ((TrueApp)paramContext).a().ab();
      k.a(paramContext, "trueApp.objectsGraph.phoneStateHandler()");
      return paramContext;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.TrueApp");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.am
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */