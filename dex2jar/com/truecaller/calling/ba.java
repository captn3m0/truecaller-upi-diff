package com.truecaller.calling;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.m;
import android.view.View;
import android.widget.TextView;
import c.f;
import c.g;
import c.g.a.a;
import c.g.b.k;
import c.g.b.l;
import com.truecaller.utils.extensions.t;
import com.truecaller.utils.ui.b;

public final class ba
  implements az, bb, bc, bd
{
  private final f b;
  private final ColorStateList c;
  private final ColorStateList d;
  private final f e;
  private final az f;
  
  public ba(View paramView, az paramaz)
  {
    f = paramaz;
    b = g.a((a)new a(paramView));
    c = b.b(paramView.getContext(), 2130969585);
    d = b.b(paramView.getContext(), 2130969591);
    e = t.a(paramView, 2131363510);
  }
  
  private final TextView a()
  {
    return (TextView)e.b();
  }
  
  public final void a(boolean paramBoolean)
  {
    TextView localTextView = a();
    Drawable localDrawable;
    if (paramBoolean) {
      localDrawable = (Drawable)b.b();
    } else {
      localDrawable = null;
    }
    m.a(localTextView, null, localDrawable);
  }
  
  public final void a_(int paramInt1, int paramInt2)
  {
    TextView localTextView = a();
    k.a(localTextView, "titleTextView");
    ae.a(localTextView, paramInt1, paramInt2);
  }
  
  public final void b_(String paramString)
  {
    TextView localTextView = a();
    k.a(localTextView, "titleTextView");
    localTextView.setText((CharSequence)paramString);
  }
  
  public final void b_(boolean paramBoolean)
  {
    TextView localTextView = a();
    if (paramBoolean) {
      localObject = c;
    } else {
      localObject = d;
    }
    localTextView.setTextColor((ColorStateList)localObject);
    Object localObject = f;
    if (localObject != null)
    {
      ((az)localObject).b_(paramBoolean);
      return;
    }
  }
  
  static final class a
    extends l
    implements a<Drawable>
  {
    a(View paramView)
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.calling.ba
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */