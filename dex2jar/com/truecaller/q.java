package com.truecaller;

import javax.inject.Provider;

public final class q
  implements dagger.a.d<com.truecaller.calling.f>
{
  private final c a;
  private final Provider<com.truecaller.androidactors.f<com.truecaller.h.c>> b;
  private final Provider<com.truecaller.common.h.d> c;
  private final Provider<com.truecaller.i.c> d;
  
  private q(c paramc, Provider<com.truecaller.androidactors.f<com.truecaller.h.c>> paramProvider, Provider<com.truecaller.common.h.d> paramProvider1, Provider<com.truecaller.i.c> paramProvider2)
  {
    a = paramc;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
  }
  
  public static q a(c paramc, Provider<com.truecaller.androidactors.f<com.truecaller.h.c>> paramProvider, Provider<com.truecaller.common.h.d> paramProvider1, Provider<com.truecaller.i.c> paramProvider2)
  {
    return new q(paramc, paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */