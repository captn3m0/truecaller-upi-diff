package com.truecaller;

import com.truecaller.analytics.b;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.k;
import com.truecaller.calling.after_call.a;
import com.truecaller.common.h.an;
import com.truecaller.i.e;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.transport.im.bp;
import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d<a>
{
  private final c a;
  private final Provider<k> b;
  private final Provider<f<bp>> c;
  private final Provider<bw> d;
  private final Provider<b> e;
  private final Provider<e> f;
  private final Provider<an> g;
  
  private g(c paramc, Provider<k> paramProvider, Provider<f<bp>> paramProvider1, Provider<bw> paramProvider2, Provider<b> paramProvider3, Provider<e> paramProvider4, Provider<an> paramProvider5)
  {
    a = paramc;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
    f = paramProvider4;
    g = paramProvider5;
  }
  
  public static g a(c paramc, Provider<k> paramProvider, Provider<f<bp>> paramProvider1, Provider<bw> paramProvider2, Provider<b> paramProvider3, Provider<e> paramProvider4, Provider<an> paramProvider5)
  {
    return new g(paramc, paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */