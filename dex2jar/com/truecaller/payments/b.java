package com.truecaller.payments;

import c.g.b.k;
import c.n.m;
import com.truecaller.common.h.u;
import javax.inject.Inject;

public final class b
  implements a
{
  private final u a;
  
  @Inject
  public b(u paramu)
  {
    a = paramu;
  }
  
  public final String a(String paramString)
  {
    k.b(paramString, "number");
    if ((k.a(a.e(paramString), "IN") ^ true)) {
      return null;
    }
    if (paramString.length() < 10) {
      return null;
    }
    return m.b(paramString, 10);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.payments.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */