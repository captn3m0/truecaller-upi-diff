package com.truecaller.payments;

import android.content.Context;
import com.truecaller.truepay.app.fcm.TruepayFcmManager;
import dagger.a.d;
import javax.inject.Provider;

public final class f
  implements d<TruepayFcmManager>
{
  private final Provider<Context> a;
  
  private f(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static f a(Provider<Context> paramProvider)
  {
    return new f(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.payments.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */