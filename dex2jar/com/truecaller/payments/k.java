package com.truecaller.payments;

import com.truecaller.common.h.ab;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.h;
import com.truecaller.truepay.d;

public final class k
  implements j
{
  private final e a;
  private final h b;
  private final d c;
  
  public k(e parame, h paramh, d paramd)
  {
    a = parame;
    b = paramh;
    c = paramd;
  }
  
  public final void a(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    if (!a.n().a()) {
      return;
    }
    if (c.c == 1)
    {
      SimInfo localSimInfo = b.b(l);
      if (localSimInfo == null) {
        return;
      }
      c.g.b.k.a(localSimInfo, "multiSimManager.getSimIn…ssage.simToken) ?: return");
      paramMessage = ab.c(c.f);
      c.g.b.k.a(paramMessage, "PhoneNumberUtils.stripAl…cipant.normalizedAddress)");
      c.a(paramMessage, a);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.payments.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */