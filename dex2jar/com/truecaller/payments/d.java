package com.truecaller.payments;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import com.truecaller.payments.network.b;
import javax.inject.Provider;

public final class d
  implements dagger.a.d<f<b>>
{
  private final Provider<b> a;
  private final Provider<i> b;
  
  private d(Provider<b> paramProvider, Provider<i> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static d a(Provider<b> paramProvider, Provider<i> paramProvider1)
  {
    return new d(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.payments.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */