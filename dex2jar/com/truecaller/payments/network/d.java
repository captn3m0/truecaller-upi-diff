package com.truecaller.payments.network;

import c.g.b.k;
import com.truecaller.androidactors.w;
import com.truecaller.log.AssertionUtil;
import e.r;
import java.io.IOException;

public final class d
  implements b
{
  private static String b()
  {
    Object localObject3 = null;
    int i = 0;
    Object localObject1 = null;
    for (;;)
    {
      Object localObject2 = localObject1;
      if (i < 2) {}
      try
      {
        localObject2 = a.a().c();
        localObject1 = localObject2;
        int j = ((r)localObject2).b();
        localObject1 = localObject2;
        if (j >= 500) {}
      }
      catch (IOException localIOException2)
      {
        try
        {
          for (;;)
          {
            Thread.sleep(500L);
            i += 1;
            break;
            if ((localObject2 != null) && (((r)localObject2).d())) {
              try
              {
                localObject2 = (RestModel.TcTokenResponse)((r)localObject2).e();
                localObject1 = localObject3;
                if (localObject2 != null) {
                  localObject1 = ((RestModel.TcTokenResponse)localObject2).getToken();
                }
                return (String)localObject1;
              }
              catch (IOException localIOException1)
              {
                AssertionUtil.reportThrowableButNeverCrash((Throwable)localIOException1);
              }
            }
            return null;
            localIOException2 = localIOException2;
          }
        }
        catch (InterruptedException localInterruptedException)
        {
          for (;;) {}
        }
      }
    }
  }
  
  public final w<String> a()
  {
    w localw = w.b(b());
    k.a(localw, "Promise.wrap(result)");
    return localw;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.payments.network.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */