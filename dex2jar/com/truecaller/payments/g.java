package com.truecaller.payments;

import com.truecaller.featuretoggles.e;
import com.truecaller.multisim.h;
import javax.inject.Provider;

public final class g
  implements dagger.a.d<j>
{
  private final Provider<e> a;
  private final Provider<h> b;
  private final Provider<com.truecaller.truepay.d> c;
  
  private g(Provider<e> paramProvider, Provider<h> paramProvider1, Provider<com.truecaller.truepay.d> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static g a(Provider<e> paramProvider, Provider<h> paramProvider1, Provider<com.truecaller.truepay.d> paramProvider2)
  {
    return new g(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.payments.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */