package com.truecaller.payments;

import com.truecaller.androidactors.i;
import com.truecaller.androidactors.k;
import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d<i>
{
  private final Provider<k> a;
  
  private c(Provider<k> paramProvider)
  {
    a = paramProvider;
  }
  
  public static c a(Provider<k> paramProvider)
  {
    return new c(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.payments.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */