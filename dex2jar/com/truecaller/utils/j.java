package com.truecaller.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import c.g.b.k;
import javax.inject.Inject;

@SuppressLint({"MissingPermission"})
public final class j
  implements i
{
  private final Context a;
  
  @Inject
  public j(Context paramContext)
  {
    a = paramContext;
  }
  
  public final boolean a()
  {
    NetworkInfo localNetworkInfo = com.truecaller.utils.extensions.i.d(a).getActiveNetworkInfo();
    if (localNetworkInfo != null) {
      return localNetworkInfo.isConnected();
    }
    return false;
  }
  
  public final String b()
  {
    NetworkInfo localNetworkInfo = com.truecaller.utils.extensions.i.d(a).getActiveNetworkInfo();
    if (localNetworkInfo != null) {
      localObject = Integer.valueOf(localNetworkInfo.getType());
    } else {
      localObject = null;
    }
    if (localObject == null) {
      return "no-connection";
    }
    if ((((Integer)localObject).intValue() != 0) && (((Integer)localObject).intValue() != 4) && (((Integer)localObject).intValue() != 5) && (((Integer)localObject).intValue() != 2) && (((Integer)localObject).intValue() != 3))
    {
      localObject = localNetworkInfo.getTypeName();
      k.a(localObject, "typeName");
      return (String)localObject;
    }
    Object localObject = localNetworkInfo.getSubtypeName();
    k.a(localObject, "subtypeName");
    return (String)localObject;
  }
  
  public final boolean c()
  {
    NetworkInfo localNetworkInfo = com.truecaller.utils.extensions.i.d(a).getActiveNetworkInfo();
    return (localNetworkInfo != null) && (localNetworkInfo.getType() == 0);
  }
  
  public final boolean d()
  {
    NetworkInfo localNetworkInfo = com.truecaller.utils.extensions.i.d(a).getActiveNetworkInfo();
    return (localNetworkInfo != null) && (localNetworkInfo.getType() == 1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */