package com.truecaller.utils;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Build.VERSION;
import android.provider.Settings;
import android.support.v4.app.ac;
import android.support.v4.content.b;
import c.a.f;
import c.g.b.k;
import c.n;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;

public final class m
  implements l
{
  private final Context a;
  
  @Inject
  public m(Context paramContext)
  {
    a = paramContext;
  }
  
  public final boolean a()
  {
    int i;
    if (Build.VERSION.SDK_INT < 23) {
      i = 1;
    } else {
      i = 0;
    }
    if (i == 0) {
      return Settings.canDrawOverlays(a);
    }
    return true;
  }
  
  public final boolean a(String... paramVarArgs)
  {
    k.b(paramVarArgs, "permissions");
    boolean bool = false;
    try
    {
      int k = paramVarArgs.length;
      int i = 0;
      while (i < k)
      {
        String str = paramVarArgs[i];
        int j = b.a(a, str);
        if (j != 0) {
          j = 1;
        } else {
          j = 0;
        }
        if (j != 0)
        {
          paramVarArgs = str;
          break label66;
        }
        i += 1;
      }
      paramVarArgs = null;
      label66:
      if (paramVarArgs == null) {
        bool = true;
      }
      return bool;
    }
    catch (RuntimeException paramVarArgs) {}
    return false;
  }
  
  public final boolean a(String[] paramArrayOfString1, int[] paramArrayOfInt, String... paramVarArgs)
  {
    k.b(paramArrayOfString1, "permissions");
    k.b(paramArrayOfInt, "grantResults");
    k.b(paramVarArgs, "desiredPermissions");
    paramArrayOfInt = (Iterable)f.a(paramArrayOfString1, (Iterable)f.a(paramArrayOfInt));
    paramArrayOfString1 = (Collection)new ArrayList();
    paramArrayOfInt = paramArrayOfInt.iterator();
    while (paramArrayOfInt.hasNext())
    {
      Object localObject = paramArrayOfInt.next();
      int i;
      if (((Number)b).intValue() == 0) {
        i = 1;
      } else {
        i = 0;
      }
      if (i != 0) {
        paramArrayOfString1.add(localObject);
      }
    }
    paramArrayOfInt = (Iterable)paramArrayOfString1;
    paramArrayOfString1 = (Collection)new ArrayList(c.a.m.a(paramArrayOfInt, 10));
    paramArrayOfInt = paramArrayOfInt.iterator();
    while (paramArrayOfInt.hasNext()) {
      paramArrayOfString1.add((String)nexta);
    }
    return ((List)paramArrayOfString1).containsAll((Collection)f.a(paramVarArgs));
  }
  
  public final boolean b()
  {
    return a(new String[] { "android.permission.READ_EXTERNAL_STORAGE" });
  }
  
  public final boolean c()
  {
    return a(new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" });
  }
  
  public final boolean d()
  {
    Object localObject = ac.b(a);
    k.a(localObject, "NotificationManagerCompa…ListenerPackages(context)");
    Iterator localIterator = ((Iterable)localObject).iterator();
    while (localIterator.hasNext())
    {
      localObject = localIterator.next();
      if (k.a((String)localObject, a.getPackageName())) {
        break label62;
      }
    }
    localObject = null;
    label62:
    return (String)localObject != null;
  }
  
  public final boolean e()
  {
    if (Build.VERSION.SDK_INT < 24) {
      return true;
    }
    Object localObject2 = a.getSystemService("notification");
    Object localObject1 = localObject2;
    if (!(localObject2 instanceof NotificationManager)) {
      localObject1 = null;
    }
    localObject1 = (NotificationManager)localObject1;
    if (localObject1 != null) {
      return ((NotificationManager)localObject1).isNotificationPolicyAccessGranted();
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */