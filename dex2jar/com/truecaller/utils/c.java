package com.truecaller.utils;

import android.content.Context;

public final class c
  implements t
{
  private final Context a;
  private final u b;
  
  private c(u paramu, Context paramContext)
  {
    a = paramContext;
    b = paramu;
  }
  
  public static t.a a()
  {
    return new a((byte)0);
  }
  
  private m h()
  {
    return new m(a);
  }
  
  public final l b()
  {
    return h();
  }
  
  public final d c()
  {
    return new e(a, h(), v.a(a));
  }
  
  public final a d()
  {
    return new b();
  }
  
  public final s e()
  {
    return new g();
  }
  
  public final i f()
  {
    return new j(a);
  }
  
  public final n g()
  {
    return new p(a);
  }
  
  static final class a
    implements t.a
  {
    private u a;
    private Context b;
    
    public final t a()
    {
      if (a == null) {
        a = new u();
      }
      dagger.a.g.a(b, Context.class);
      return new c(a, b, (byte)0);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */