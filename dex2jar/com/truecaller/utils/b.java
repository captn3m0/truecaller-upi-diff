package com.truecaller.utils;

import android.os.SystemClock;
import javax.inject.Inject;

public final class b
  implements a
{
  public final long a()
  {
    return System.currentTimeMillis();
  }
  
  public final long b()
  {
    return SystemClock.elapsedRealtime();
  }
  
  public final long c()
  {
    return System.nanoTime();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */