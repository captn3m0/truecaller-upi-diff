package com.truecaller.utils.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import c.a.aa;
import c.a.m;
import c.g.b.k;
import java.util.LinkedHashSet;
import java.util.Set;

public abstract class a
  implements c
{
  final SharedPreferences a;
  
  public a(SharedPreferences paramSharedPreferences)
  {
    a = paramSharedPreferences;
  }
  
  private String c()
  {
    StringBuilder localStringBuilder = new StringBuilder("VERSION_");
    localStringBuilder.append(b());
    return localStringBuilder.toString();
  }
  
  public abstract int a();
  
  /* Error */
  public final int a(SharedPreferences paramSharedPreferences, Set<String> paramSet)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 48
    //   3: invokestatic 18	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_1
    //   7: invokeinterface 54 1 0
    //   12: astore 10
    //   14: aload 10
    //   16: ldc 56
    //   18: invokestatic 58	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   21: aload 10
    //   23: invokeinterface 64 1 0
    //   28: istore 6
    //   30: aconst_null
    //   31: astore 11
    //   33: iload 6
    //   35: iconst_1
    //   36: ixor
    //   37: ifeq +6 -> 43
    //   40: goto +6 -> 46
    //   43: aconst_null
    //   44: astore 10
    //   46: aload 10
    //   48: ifnonnull +5 -> 53
    //   51: iconst_0
    //   52: ireturn
    //   53: new 66	com/truecaller/utils/a/b
    //   56: dup
    //   57: aload_0
    //   58: invokespecial 69	com/truecaller/utils/a/b:<init>	(Lcom/truecaller/utils/a/a;)V
    //   61: checkcast 71	java/io/Closeable
    //   64: astore 12
    //   66: aload 11
    //   68: astore 9
    //   70: aload 12
    //   72: checkcast 66	com/truecaller/utils/a/b
    //   75: astore 13
    //   77: aload 11
    //   79: astore 9
    //   81: aload 10
    //   83: invokestatic 76	c/a/ag:c	(Ljava/util/Map;)Lc/m/i;
    //   86: new 78	com/truecaller/utils/a/a$a
    //   89: dup
    //   90: aload 10
    //   92: aload_2
    //   93: aload_1
    //   94: invokespecial 81	com/truecaller/utils/a/a$a:<init>	(Ljava/util/Map;Ljava/util/Set;Landroid/content/SharedPreferences;)V
    //   97: checkcast 83	c/g/a/b
    //   100: invokestatic 88	c/m/l:a	(Lc/m/i;Lc/g/a/b;)Lc/m/i;
    //   103: invokeinterface 93 1 0
    //   108: astore 14
    //   110: aload 11
    //   112: astore 9
    //   114: aload 14
    //   116: invokeinterface 98 1 0
    //   121: ifeq +612 -> 733
    //   124: aload 11
    //   126: astore 9
    //   128: aload 14
    //   130: invokeinterface 102 1 0
    //   135: checkcast 104	java/util/Map$Entry
    //   138: astore 15
    //   140: aload 11
    //   142: astore 9
    //   144: aload 15
    //   146: invokeinterface 107 1 0
    //   151: checkcast 109	java/lang/String
    //   154: astore_2
    //   155: aload 11
    //   157: astore 9
    //   159: aload 15
    //   161: invokeinterface 112 1 0
    //   166: astore 15
    //   168: aload 11
    //   170: astore 9
    //   172: aload 15
    //   174: instanceof 114
    //   177: ifeq +48 -> 225
    //   180: aload 11
    //   182: astore 9
    //   184: aload 15
    //   186: checkcast 116	java/lang/Number
    //   189: invokevirtual 120	java/lang/Number:longValue	()J
    //   192: lstore 7
    //   194: aload 11
    //   196: astore 9
    //   198: aload_2
    //   199: ldc 122
    //   201: invokestatic 18	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   204: aload 11
    //   206: astore 9
    //   208: aload 13
    //   210: invokevirtual 125	com/truecaller/utils/a/b:a	()Landroid/content/SharedPreferences$Editor;
    //   213: aload_2
    //   214: lload 7
    //   216: invokeinterface 131 4 0
    //   221: pop
    //   222: goto +418 -> 640
    //   225: aload 11
    //   227: astore 9
    //   229: aload 15
    //   231: instanceof 133
    //   234: ifeq +49 -> 283
    //   237: aload 11
    //   239: astore 9
    //   241: aload 15
    //   243: checkcast 116	java/lang/Number
    //   246: invokevirtual 137	java/lang/Number:doubleValue	()D
    //   249: dstore_3
    //   250: aload 11
    //   252: astore 9
    //   254: aload_2
    //   255: ldc 122
    //   257: invokestatic 18	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   260: aload 11
    //   262: astore 9
    //   264: aload 13
    //   266: invokevirtual 125	com/truecaller/utils/a/b:a	()Landroid/content/SharedPreferences$Editor;
    //   269: aload_2
    //   270: dload_3
    //   271: invokestatic 141	java/lang/Double:doubleToRawLongBits	(D)J
    //   274: invokeinterface 131 4 0
    //   279: pop
    //   280: goto +360 -> 640
    //   283: aload 11
    //   285: astore 9
    //   287: aload 15
    //   289: instanceof 143
    //   292: ifeq +48 -> 340
    //   295: aload 11
    //   297: astore 9
    //   299: aload 15
    //   301: checkcast 116	java/lang/Number
    //   304: invokevirtual 147	java/lang/Number:floatValue	()F
    //   307: fstore 5
    //   309: aload 11
    //   311: astore 9
    //   313: aload_2
    //   314: ldc 122
    //   316: invokestatic 18	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   319: aload 11
    //   321: astore 9
    //   323: aload 13
    //   325: invokevirtual 125	com/truecaller/utils/a/b:a	()Landroid/content/SharedPreferences$Editor;
    //   328: aload_2
    //   329: fload 5
    //   331: invokeinterface 151 3 0
    //   336: pop
    //   337: goto +303 -> 640
    //   340: aload 11
    //   342: astore 9
    //   344: aload 15
    //   346: instanceof 153
    //   349: ifeq +24 -> 373
    //   352: aload 11
    //   354: astore 9
    //   356: aload 13
    //   358: aload_2
    //   359: aload 15
    //   361: checkcast 116	java/lang/Number
    //   364: invokevirtual 156	java/lang/Number:intValue	()I
    //   367: invokevirtual 159	com/truecaller/utils/a/b:b	(Ljava/lang/String;I)V
    //   370: goto +270 -> 640
    //   373: aload 11
    //   375: astore 9
    //   377: aload 15
    //   379: instanceof 161
    //   382: ifeq +48 -> 430
    //   385: aload 11
    //   387: astore 9
    //   389: aload 15
    //   391: checkcast 161	java/lang/Boolean
    //   394: invokevirtual 164	java/lang/Boolean:booleanValue	()Z
    //   397: istore 6
    //   399: aload 11
    //   401: astore 9
    //   403: aload_2
    //   404: ldc 122
    //   406: invokestatic 18	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   409: aload 11
    //   411: astore 9
    //   413: aload 13
    //   415: invokevirtual 125	com/truecaller/utils/a/b:a	()Landroid/content/SharedPreferences$Editor;
    //   418: aload_2
    //   419: iload 6
    //   421: invokeinterface 168 3 0
    //   426: pop
    //   427: goto +213 -> 640
    //   430: aload 11
    //   432: astore 9
    //   434: aload 15
    //   436: instanceof 109
    //   439: ifeq +45 -> 484
    //   442: aload 11
    //   444: astore 9
    //   446: aload 15
    //   448: checkcast 109	java/lang/String
    //   451: astore 15
    //   453: aload 11
    //   455: astore 9
    //   457: aload_2
    //   458: ldc 122
    //   460: invokestatic 18	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   463: aload 11
    //   465: astore 9
    //   467: aload 13
    //   469: invokevirtual 125	com/truecaller/utils/a/b:a	()Landroid/content/SharedPreferences$Editor;
    //   472: aload_2
    //   473: aload 15
    //   475: invokeinterface 172 3 0
    //   480: pop
    //   481: goto +159 -> 640
    //   484: aload 11
    //   486: astore 9
    //   488: aload 15
    //   490: instanceof 174
    //   493: ifeq +281 -> 774
    //   496: aload 11
    //   498: astore 9
    //   500: aload 15
    //   502: checkcast 174	java/util/Set
    //   505: astore 15
    //   507: aload 11
    //   509: astore 9
    //   511: aload_2
    //   512: ldc 122
    //   514: invokestatic 18	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   517: aload 11
    //   519: astore 9
    //   521: aload 15
    //   523: ldc -80
    //   525: invokestatic 18	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   528: aload 11
    //   530: astore 9
    //   532: aload 15
    //   534: checkcast 178	java/lang/Iterable
    //   537: invokeinterface 181 1 0
    //   542: astore 16
    //   544: aload 11
    //   546: astore 9
    //   548: aload 16
    //   550: invokeinterface 98 1 0
    //   555: ifeq +54 -> 609
    //   558: aload 11
    //   560: astore 9
    //   562: aload 16
    //   564: invokeinterface 102 1 0
    //   569: instanceof 109
    //   572: istore 6
    //   574: aload 11
    //   576: astore 9
    //   578: getstatic 186	c/z:a	Z
    //   581: ifeq -37 -> 544
    //   584: iload 6
    //   586: ifeq +6 -> 592
    //   589: goto -45 -> 544
    //   592: aload 11
    //   594: astore 9
    //   596: new 188	java/lang/AssertionError
    //   599: dup
    //   600: ldc -66
    //   602: invokespecial 193	java/lang/AssertionError:<init>	(Ljava/lang/Object;)V
    //   605: checkcast 46	java/lang/Throwable
    //   608: athrow
    //   609: aload 11
    //   611: astore 9
    //   613: aload_2
    //   614: ldc 122
    //   616: invokestatic 18	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   619: aload 11
    //   621: astore 9
    //   623: aload 13
    //   625: invokevirtual 125	com/truecaller/utils/a/b:a	()Landroid/content/SharedPreferences$Editor;
    //   628: aload_2
    //   629: aload 15
    //   631: invokeinterface 197 3 0
    //   636: pop
    //   637: goto +3 -> 640
    //   640: aload 11
    //   642: astore 9
    //   644: aload_1
    //   645: invokeinterface 200 1 0
    //   650: aload_2
    //   651: invokeinterface 204 2 0
    //   656: invokeinterface 207 1 0
    //   661: pop
    //   662: goto -552 -> 110
    //   665: aload 11
    //   667: astore 9
    //   669: new 28	java/lang/StringBuilder
    //   672: dup
    //   673: ldc -47
    //   675: invokespecial 33	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   678: astore_1
    //   679: aload 11
    //   681: astore 9
    //   683: aload_1
    //   684: aload 15
    //   686: invokevirtual 213	java/lang/Object:getClass	()Ljava/lang/Class;
    //   689: invokevirtual 216	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   692: pop
    //   693: aload 11
    //   695: astore 9
    //   697: aload_1
    //   698: ldc -38
    //   700: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   703: pop
    //   704: aload 11
    //   706: astore 9
    //   708: aload_1
    //   709: aload_2
    //   710: invokevirtual 39	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   713: pop
    //   714: aload 11
    //   716: astore 9
    //   718: new 220	java/lang/IllegalStateException
    //   721: dup
    //   722: aload_1
    //   723: invokevirtual 42	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   726: invokespecial 221	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   729: checkcast 46	java/lang/Throwable
    //   732: athrow
    //   733: aload 11
    //   735: astore 9
    //   737: getstatic 226	c/x:a	Lc/x;
    //   740: astore_1
    //   741: aload 12
    //   743: aconst_null
    //   744: invokestatic 231	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   747: aload 10
    //   749: invokeinterface 234 1 0
    //   754: ireturn
    //   755: astore_1
    //   756: goto +9 -> 765
    //   759: astore_1
    //   760: aload_1
    //   761: astore 9
    //   763: aload_1
    //   764: athrow
    //   765: aload 12
    //   767: aload 9
    //   769: invokestatic 231	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   772: aload_1
    //   773: athrow
    //   774: aload 15
    //   776: ifnonnull -111 -> 665
    //   779: goto -139 -> 640
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	782	0	this	a
    //   0	782	1	paramSharedPreferences	SharedPreferences
    //   0	782	2	paramSet	Set<String>
    //   249	22	3	d	double
    //   307	23	5	f	float
    //   28	557	6	bool	boolean
    //   192	23	7	l	long
    //   68	700	9	localObject1	Object
    //   12	736	10	localMap	java.util.Map
    //   31	703	11	localObject2	Object
    //   64	702	12	localCloseable	java.io.Closeable
    //   75	549	13	localb	b
    //   108	21	14	localIterator1	java.util.Iterator
    //   138	637	15	localObject3	Object
    //   542	21	16	localIterator2	java.util.Iterator
    // Exception table:
    //   from	to	target	type
    //   70	77	755	finally
    //   81	110	755	finally
    //   114	124	755	finally
    //   128	140	755	finally
    //   144	155	755	finally
    //   159	168	755	finally
    //   172	180	755	finally
    //   184	194	755	finally
    //   198	204	755	finally
    //   208	222	755	finally
    //   229	237	755	finally
    //   241	250	755	finally
    //   254	260	755	finally
    //   264	280	755	finally
    //   287	295	755	finally
    //   299	309	755	finally
    //   313	319	755	finally
    //   323	337	755	finally
    //   344	352	755	finally
    //   356	370	755	finally
    //   377	385	755	finally
    //   389	399	755	finally
    //   403	409	755	finally
    //   413	427	755	finally
    //   434	442	755	finally
    //   446	453	755	finally
    //   457	463	755	finally
    //   467	481	755	finally
    //   488	496	755	finally
    //   500	507	755	finally
    //   511	517	755	finally
    //   521	528	755	finally
    //   532	544	755	finally
    //   548	558	755	finally
    //   562	574	755	finally
    //   578	584	755	finally
    //   596	609	755	finally
    //   613	619	755	finally
    //   623	637	755	finally
    //   644	662	755	finally
    //   669	679	755	finally
    //   683	693	755	finally
    //   697	704	755	finally
    //   708	714	755	finally
    //   718	733	755	finally
    //   737	741	755	finally
    //   763	765	755	finally
    //   70	77	759	java/lang/Throwable
    //   81	110	759	java/lang/Throwable
    //   114	124	759	java/lang/Throwable
    //   128	140	759	java/lang/Throwable
    //   144	155	759	java/lang/Throwable
    //   159	168	759	java/lang/Throwable
    //   172	180	759	java/lang/Throwable
    //   184	194	759	java/lang/Throwable
    //   198	204	759	java/lang/Throwable
    //   208	222	759	java/lang/Throwable
    //   229	237	759	java/lang/Throwable
    //   241	250	759	java/lang/Throwable
    //   254	260	759	java/lang/Throwable
    //   264	280	759	java/lang/Throwable
    //   287	295	759	java/lang/Throwable
    //   299	309	759	java/lang/Throwable
    //   313	319	759	java/lang/Throwable
    //   323	337	759	java/lang/Throwable
    //   344	352	759	java/lang/Throwable
    //   356	370	759	java/lang/Throwable
    //   377	385	759	java/lang/Throwable
    //   389	399	759	java/lang/Throwable
    //   403	409	759	java/lang/Throwable
    //   413	427	759	java/lang/Throwable
    //   434	442	759	java/lang/Throwable
    //   446	453	759	java/lang/Throwable
    //   457	463	759	java/lang/Throwable
    //   467	481	759	java/lang/Throwable
    //   488	496	759	java/lang/Throwable
    //   500	507	759	java/lang/Throwable
    //   511	517	759	java/lang/Throwable
    //   521	528	759	java/lang/Throwable
    //   532	544	759	java/lang/Throwable
    //   548	558	759	java/lang/Throwable
    //   562	574	759	java/lang/Throwable
    //   578	584	759	java/lang/Throwable
    //   596	609	759	java/lang/Throwable
    //   613	619	759	java/lang/Throwable
    //   623	637	759	java/lang/Throwable
    //   644	662	759	java/lang/Throwable
    //   669	679	759	java/lang/Throwable
    //   683	693	759	java/lang/Throwable
    //   697	704	759	java/lang/Throwable
    //   708	714	759	java/lang/Throwable
    //   718	733	759	java/lang/Throwable
    //   737	741	759	java/lang/Throwable
  }
  
  public int a(String paramString, int paramInt)
  {
    k.b(paramString, "key");
    return a.getInt(paramString, paramInt);
  }
  
  public final long a(String paramString, long paramLong)
  {
    k.b(paramString, "key");
    return a.getLong(paramString, paramLong);
  }
  
  public final String a(String paramString)
  {
    k.b(paramString, "key");
    return a.getString(paramString, null);
  }
  
  public abstract void a(int paramInt, Context paramContext);
  
  public final void a(Context paramContext)
  {
    k.b(paramContext, "context");
    int i = a.getInt(c(), 0);
    int j = a();
    if (i < j) {
      a(i, paramContext);
    }
    a.edit().putInt(c(), j).apply();
  }
  
  public final void a(String paramString, double paramDouble)
  {
    k.b(paramString, "key");
    a.edit().putLong(paramString, Double.doubleToRawLongBits(paramDouble)).apply();
  }
  
  public final void a(String paramString, Long paramLong)
  {
    k.b(paramString, "key");
    if (paramLong == null)
    {
      d(paramString);
      return;
    }
    b(paramString, paramLong.longValue());
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "key");
    a.edit().putString(paramString1, paramString2).apply();
  }
  
  public final void a(String paramString, Set<String> paramSet)
  {
    k.b(paramString, "key");
    a.edit().putStringSet(paramString, paramSet).apply();
  }
  
  public final boolean a(String paramString, boolean paramBoolean)
  {
    k.b(paramString, "key");
    return a.getBoolean(paramString, paramBoolean);
  }
  
  public final int a_(String paramString)
  {
    k.b(paramString, "key");
    int i = a(paramString, 0) + 1;
    b(paramString, i);
    return i;
  }
  
  public abstract String b();
  
  public final String b(String paramString1, String paramString2)
  {
    k.b(paramString1, "key");
    k.b(paramString2, "defaultValue");
    String str = a.getString(paramString1, paramString2);
    paramString1 = str;
    if (str == null) {
      paramString1 = paramString2;
    }
    return paramString1;
  }
  
  public void b(String paramString, int paramInt)
  {
    k.b(paramString, "key");
    a.edit().putInt(paramString, paramInt).apply();
  }
  
  public final void b(String paramString, long paramLong)
  {
    k.b(paramString, "key");
    a.edit().putLong(paramString, paramLong).apply();
  }
  
  public final void b(String paramString, boolean paramBoolean)
  {
    k.b(paramString, "key");
    a.edit().putBoolean(paramString, paramBoolean).apply();
  }
  
  public final boolean b(String paramString)
  {
    k.b(paramString, "key");
    return a.getBoolean(paramString, false);
  }
  
  public final double c(String paramString)
  {
    k.b(paramString, "key");
    return Double.longBitsToDouble(a.getLong(paramString, Double.doubleToLongBits(0.0D)));
  }
  
  public final void d(String paramString)
  {
    k.b(paramString, "key");
    a.edit().remove(paramString).apply();
  }
  
  public final boolean e(String paramString)
  {
    k.b(paramString, "key");
    return a.contains(paramString);
  }
  
  public final Set<String> f(String paramString)
  {
    k.b(paramString, "key");
    paramString = a.getStringSet(paramString, (Set)aa.a);
    if (paramString != null)
    {
      Set localSet = m.l((Iterable)paramString);
      paramString = localSet;
      if (localSet != null) {}
    }
    else
    {
      paramString = (Set)new LinkedHashSet();
    }
    return paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */