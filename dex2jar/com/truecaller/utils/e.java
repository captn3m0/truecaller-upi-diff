package com.truecaller.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Point;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.PowerManager;
import android.provider.Settings.System;
import android.provider.Telephony.Sms;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.view.Display;
import android.view.WindowManager;
import c.g.b.k;
import c.n.m;
import c.u;
import com.truecaller.utils.extensions.i;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;

public final class e
  implements d
{
  private final Context a;
  private final l b;
  private final String c;
  
  @Inject
  public e(Context paramContext, l paraml, @Named("applicationId") String paramString)
  {
    a = paramContext;
    b = paraml;
    c = paramString;
  }
  
  private PackageInfo a(String paramString, int paramInt)
  {
    k.b(paramString, "packageName");
    try
    {
      paramString = a.getPackageManager().getPackageInfo(paramString, paramInt);
      return paramString;
    }
    catch (PackageManager.NameNotFoundException paramString)
    {
      for (;;) {}
    }
    return null;
  }
  
  private boolean t()
  {
    return a.getPackageManager().hasSystemFeature("android.hardware.telephony");
  }
  
  public final boolean a()
  {
    return a(k());
  }
  
  public final boolean a(String paramString)
  {
    return (b.a(new String[] { "android.permission.RECEIVE_SMS" })) && (b(paramString));
  }
  
  public final boolean b()
  {
    return i.b(a).getPhoneType() == 2;
  }
  
  public final boolean b(String paramString)
  {
    return k.a(c, paramString);
  }
  
  public final boolean c()
  {
    return m.a(a.getPackageName(), j(), true);
  }
  
  public final boolean c(String paramString)
  {
    k.b(paramString, "pkgName");
    return a(paramString, 128) != null;
  }
  
  public final boolean d()
  {
    return b(k());
  }
  
  public final boolean d(String paramString)
  {
    k.b(paramString, "packageName");
    PackageManager localPackageManager = a.getPackageManager();
    return (localPackageManager != null) && (localPackageManager.checkSignatures("com.truecaller", paramString) == 0);
  }
  
  public final boolean e()
  {
    return m.a(Build.BRAND, "HUAWEI", true);
  }
  
  public final boolean f()
  {
    if (Build.VERSION.SDK_INT >= 23)
    {
      Object localObject = a.getSystemService("power");
      if (localObject != null) {
        return ((PowerManager)localObject).isIgnoringBatteryOptimizations(n());
      }
      throw new u("null cannot be cast to non-null type android.os.PowerManager");
    }
    return true;
  }
  
  public final boolean g()
  {
    return b.a(new String[] { "android.permission.MODIFY_PHONE_STATE" });
  }
  
  public final int h()
  {
    return Build.VERSION.SDK_INT;
  }
  
  public final String i()
  {
    try
    {
      String str = Build.VERSION.SECURITY_PATCH;
      return str;
    }
    catch (Throwable localThrowable)
    {
      for (;;) {}
    }
    return null;
  }
  
  public final String j()
  {
    if ((Build.VERSION.SDK_INT >= 23) && (t())) {
      return i.c(a).getDefaultDialerPackage();
    }
    return null;
  }
  
  @SuppressLint({"NewApi"})
  public final String k()
  {
    if (t()) {
      return Telephony.Sms.getDefaultSmsPackage(a);
    }
    return null;
  }
  
  public final String l()
  {
    return Build.DEVICE;
  }
  
  public final String m()
  {
    return Build.MANUFACTURER;
  }
  
  public final String n()
  {
    Context localContext = a.getApplicationContext();
    k.a(localContext, "context.applicationContext");
    return localContext.getPackageName();
  }
  
  public final int o()
  {
    PackageInfo localPackageInfo = a("com.google.android.gms", 0);
    if (localPackageInfo == null) {
      return -1;
    }
    return versionCode;
  }
  
  public final boolean p()
  {
    if (Build.VERSION.SDK_INT >= 23) {
      return Settings.System.canWrite(a);
    }
    return true;
  }
  
  public final boolean q()
  {
    return a.getPackageManager().hasSystemFeature("android.hardware.camera");
  }
  
  public final boolean r()
  {
    Point localPoint = new Point();
    Object localObject = a.getSystemService("window");
    if (localObject != null)
    {
      ((WindowManager)localObject).getDefaultDisplay().getSize(localPoint);
      return x > y;
    }
    throw new u("null cannot be cast to non-null type android.view.WindowManager");
  }
  
  public final List<String> s()
  {
    Object localObject1;
    if (Build.VERSION.SDK_INT >= 21)
    {
      localObject2 = Build.SUPPORTED_ABIS;
      localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = new String[0];
      }
    }
    else
    {
      localObject1 = new String[2];
      localObject1[0] = Build.CPU_ABI;
      localObject1[1] = Build.CPU_ABI2;
    }
    Object localObject2 = (Collection)new ArrayList();
    int k = localObject1.length;
    int i = 0;
    while (i < k)
    {
      Object localObject3 = localObject1[i];
      CharSequence localCharSequence = (CharSequence)localObject3;
      int j;
      if ((localCharSequence != null) && (!m.a(localCharSequence))) {
        j = 0;
      } else {
        j = 1;
      }
      if (j == 0) {
        ((Collection)localObject2).add(localObject3);
      }
      i += 1;
    }
    return (List)localObject2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */