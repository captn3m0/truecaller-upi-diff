package com.truecaller.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public final class h
{
  private static Boolean a;
  
  /* Error */
  public static boolean a()
  {
    // Byte code:
    //   0: getstatic 20	com/truecaller/utils/h:a	Ljava/lang/Boolean;
    //   3: ifnonnull +111 -> 114
    //   6: getstatic 26	android/os/Build:MANUFACTURER	Ljava/lang/String;
    //   9: ldc 28
    //   11: invokevirtual 34	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
    //   14: ifeq +64 -> 78
    //   17: ldc 36
    //   19: invokestatic 42	java/lang/Class:forName	(Ljava/lang/String;)Ljava/lang/Class;
    //   22: ldc 44
    //   24: iconst_2
    //   25: anewarray 38	java/lang/Class
    //   28: dup
    //   29: iconst_0
    //   30: ldc 30
    //   32: aastore
    //   33: dup
    //   34: iconst_1
    //   35: getstatic 50	java/lang/Integer:TYPE	Ljava/lang/Class;
    //   38: aastore
    //   39: invokevirtual 54	java/lang/Class:getMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   42: aconst_null
    //   43: iconst_2
    //   44: anewarray 4	java/lang/Object
    //   47: dup
    //   48: iconst_0
    //   49: ldc 56
    //   51: aastore
    //   52: dup
    //   53: iconst_1
    //   54: iconst_m1
    //   55: invokestatic 60	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   58: aastore
    //   59: invokevirtual 66	java/lang/reflect/Method:invoke	(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    //   62: iconst_m1
    //   63: invokestatic 60	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   66: invokevirtual 70	java/lang/Object:equals	(Ljava/lang/Object;)Z
    //   69: ifne +9 -> 78
    //   72: getstatic 75	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   75: putstatic 20	com/truecaller/utils/h:a	Ljava/lang/Boolean;
    //   78: getstatic 20	com/truecaller/utils/h:a	Ljava/lang/Boolean;
    //   81: ifnonnull +33 -> 114
    //   84: goto +24 -> 108
    //   87: astore_0
    //   88: getstatic 20	com/truecaller/utils/h:a	Ljava/lang/Boolean;
    //   91: ifnonnull +9 -> 100
    //   94: getstatic 78	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   97: putstatic 20	com/truecaller/utils/h:a	Ljava/lang/Boolean;
    //   100: aload_0
    //   101: athrow
    //   102: getstatic 20	com/truecaller/utils/h:a	Ljava/lang/Boolean;
    //   105: ifnonnull +9 -> 114
    //   108: getstatic 78	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   111: putstatic 20	com/truecaller/utils/h:a	Ljava/lang/Boolean;
    //   114: getstatic 20	com/truecaller/utils/h:a	Ljava/lang/Boolean;
    //   117: invokevirtual 81	java/lang/Boolean:booleanValue	()Z
    //   120: ireturn
    //   121: astore_0
    //   122: goto -20 -> 102
    // Local variable table:
    //   start	length	slot	name	signature
    //   87	14	0	localObject	Object
    //   121	1	0	localClassNotFoundException	ClassNotFoundException
    // Exception table:
    //   from	to	target	type
    //   6	78	87	finally
    //   6	78	121	java/lang/ClassNotFoundException
    //   6	78	121	java/lang/NoSuchMethodException
    //   6	78	121	java/lang/IllegalAccessException
    //   6	78	121	java/lang/reflect/InvocationTargetException
  }
  
  public static String b()
  {
    if (a()) {}
    try
    {
      Object localObject = Class.forName("android.os.SystemProperties");
      localObject = (String)((Class)localObject).getMethod("get", new Class[] { String.class }).invoke(localObject, new Object[] { "ro.build.version.incremental" });
      return (String)localObject;
    }
    catch (ClassNotFoundException|NoSuchMethodException|IllegalAccessException|InvocationTargetException localClassNotFoundException)
    {
      for (;;) {}
    }
    return "ErrorWhileReadingProperty";
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */