package com.truecaller.utils.extensions;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources.Theme;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.v4.content.b;
import android.support.v4.content.d;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.util.TypedValue;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import c.g.b.k;
import c.u;

public final class i
{
  public static final int a(Context paramContext, int paramInt)
  {
    k.b(paramContext, "receiver$0");
    TypedValue localTypedValue = new TypedValue();
    paramContext.getTheme().resolveAttribute(paramInt, localTypedValue, true);
    if (resourceId == 0) {
      return data;
    }
    return b.c(paramContext, resourceId);
  }
  
  public static final Toast a(Context paramContext)
  {
    return a(paramContext, 2131886537, null, 0, 6);
  }
  
  private static Toast a(Context paramContext, int paramInt1, CharSequence paramCharSequence, int paramInt2)
  {
    k.b(paramContext, "receiver$0");
    CharSequence localCharSequence = paramCharSequence;
    if (paramCharSequence == null) {
      localCharSequence = (CharSequence)paramContext.getString(paramInt1);
    }
    paramContext = Toast.makeText(paramContext, localCharSequence, paramInt2);
    paramContext.show();
    k.a(paramContext, "Toast.makeText(this, mes…        .apply { show() }");
    return paramContext;
  }
  
  public static final void a(Context paramContext, BroadcastReceiver paramBroadcastReceiver, String... paramVarArgs)
  {
    k.b(paramContext, "receiver$0");
    k.b(paramBroadcastReceiver, "broadcastReceiver");
    k.b(paramVarArgs, "actions");
    paramContext = d.a(paramContext);
    k.a(paramContext, "LocalBroadcastManager.getInstance(this)");
    IntentFilter localIntentFilter = new IntentFilter();
    int j = paramVarArgs.length;
    int i = 0;
    while (i < j)
    {
      localIntentFilter.addAction(paramVarArgs[i]);
      i += 1;
    }
    paramContext.a(paramBroadcastReceiver, localIntentFilter);
  }
  
  public static final void a(Context paramContext, String paramString, Bundle paramBundle)
  {
    k.b(paramContext, "receiver$0");
    k.b(paramString, "action");
    paramString = new Intent(paramString);
    paramString.putExtras(paramBundle);
    d.a(paramContext).b(paramString);
  }
  
  public static final TelephonyManager b(Context paramContext)
  {
    k.b(paramContext, "receiver$0");
    paramContext = paramContext.getSystemService("phone");
    if (paramContext != null) {
      return (TelephonyManager)paramContext;
    }
    throw new u("null cannot be cast to non-null type android.telephony.TelephonyManager");
  }
  
  public static final TelecomManager c(Context paramContext)
  {
    k.b(paramContext, "receiver$0");
    paramContext = paramContext.getSystemService("telecom");
    if (paramContext != null) {
      return (TelecomManager)paramContext;
    }
    throw new u("null cannot be cast to non-null type android.telecom.TelecomManager");
  }
  
  public static final ConnectivityManager d(Context paramContext)
  {
    k.b(paramContext, "receiver$0");
    paramContext = paramContext.getSystemService("connectivity");
    if (paramContext != null) {
      return (ConnectivityManager)paramContext;
    }
    throw new u("null cannot be cast to non-null type android.net.ConnectivityManager");
  }
  
  public static final InputMethodManager e(Context paramContext)
  {
    k.b(paramContext, "receiver$0");
    paramContext = paramContext.getSystemService("input_method");
    if (paramContext != null) {
      return (InputMethodManager)paramContext;
    }
    throw new u("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
  }
  
  public static final NotificationManager f(Context paramContext)
  {
    k.b(paramContext, "receiver$0");
    paramContext = paramContext.getSystemService("notification");
    if (paramContext != null) {
      return (NotificationManager)paramContext;
    }
    throw new u("null cannot be cast to non-null type android.app.NotificationManager");
  }
  
  public static final PowerManager g(Context paramContext)
  {
    k.b(paramContext, "receiver$0");
    paramContext = paramContext.getSystemService("power");
    if (paramContext != null) {
      return (PowerManager)paramContext;
    }
    throw new u("null cannot be cast to non-null type android.os.PowerManager");
  }
  
  public static final Vibrator h(Context paramContext)
  {
    k.b(paramContext, "receiver$0");
    paramContext = paramContext.getSystemService("vibrator");
    if (paramContext != null) {
      return (Vibrator)paramContext;
    }
    throw new u("null cannot be cast to non-null type android.os.Vibrator");
  }
  
  public static final AudioManager i(Context paramContext)
  {
    k.b(paramContext, "receiver$0");
    paramContext = paramContext.getSystemService("audio");
    if (paramContext != null) {
      return (AudioManager)paramContext;
    }
    throw new u("null cannot be cast to non-null type android.media.AudioManager");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */