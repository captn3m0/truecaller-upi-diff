package com.truecaller.utils.extensions;

import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import c.d.f;
import c.g.a.b;
import c.g.a.m;
import c.g.b.k;
import c.x;
import java.lang.reflect.Constructor;
import kotlinx.coroutines.a.h;
import kotlinx.coroutines.a.q;
import kotlinx.coroutines.a.r;
import kotlinx.coroutines.a.s;
import kotlinx.coroutines.a.u;
import kotlinx.coroutines.a.w;
import kotlinx.coroutines.a.y;
import kotlinx.coroutines.aa;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ai;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.e;

public final class j
{
  private static Handler a(Looper paramLooper)
  {
    k.b(paramLooper, "receiver$0");
    if (Build.VERSION.SDK_INT >= 28)
    {
      paramLooper = Handler.createAsync(paramLooper);
      k.a(paramLooper, "Handler.createAsync(this)");
      return paramLooper;
    }
    try
    {
      Constructor localConstructor = Handler.class.getDeclaredConstructor(new Class[] { Looper.class, Handler.Callback.class, Boolean.TYPE });
      k.a(localConstructor, "Handler::class.java.getD…vaPrimitiveType\n        )");
      paramLooper = localConstructor.newInstance(new Object[] { paramLooper, null, Boolean.TRUE });
      k.a(paramLooper, "constructor.newInstance(this, null, true)");
      return (Handler)paramLooper;
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
      for (;;) {}
    }
    return new Handler(paramLooper);
  }
  
  public static final <T> T a(h<T> paramh)
  {
    k.b(paramh, "receiver$0");
    paramh = paramh.a();
    try
    {
      Object localObject1 = paramh.au_();
      return (T)localObject1;
    }
    finally
    {
      paramh.n();
    }
  }
  
  public static final <T> y<T> a(ag paramag, int paramInt, b<? super T, x> paramb)
  {
    k.b(paramag, "receiver$0");
    k.b(paramb, "action");
    paramb = (m)new j.a(paramb, null);
    f localf = (f)c.d.g.a;
    ai localai = ai.a;
    k.b(paramag, "receiver$0");
    k.b(localf, "context");
    k.b(localai, "start");
    k.b(paramb, "block");
    localf = aa.a(paramag, localf);
    if (paramInt != Integer.MAX_VALUE) {
      switch (paramInt)
      {
      default: 
        paramag = (kotlinx.coroutines.a.j)new kotlinx.coroutines.a.g(paramInt);
        break;
      case 0: 
        paramag = (kotlinx.coroutines.a.j)new w();
        break;
      case -1: 
        paramag = (kotlinx.coroutines.a.j)new q();
        break;
      }
    } else {
      paramag = (kotlinx.coroutines.a.j)new s();
    }
    if (localai.a()) {
      paramag = (kotlinx.coroutines.a.d)new r(localf, paramag, paramb);
    } else {
      paramag = new kotlinx.coroutines.a.d(localf, paramag, true);
    }
    paramag.a(localai, paramag, paramb);
    return (y)paramag;
  }
  
  public static final kotlinx.coroutines.android.c a()
  {
    Looper localLooper = Looper.getMainLooper();
    k.a(localLooper, "Looper.getMainLooper()");
    return kotlinx.coroutines.android.d.a(a(localLooper), "Main");
  }
  
  public static final <T> void a(ag paramag, h<T> paramh, m<? super T, ? super c.d.c<? super x>, ? extends Object> paramm)
  {
    k.b(paramag, "receiver$0");
    k.b(paramh, "channel");
    k.b(paramm, "block");
    paramh = paramh.a();
    e.b(paramag, null, (m)new j.b(paramh, paramm, null), 3).a_((b)new j.c(paramh));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */