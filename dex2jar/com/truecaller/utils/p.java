package com.truecaller.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.text.Html;
import c.g.b.k;
import com.truecaller.utils.ui.b;
import java.util.Arrays;
import javax.inject.Inject;

public final class p
  implements o
{
  private final Context a;
  
  @Inject
  public p(Context paramContext)
  {
    a = paramContext;
  }
  
  public final Drawable a(int paramInt1, int paramInt2)
  {
    Drawable localDrawable = b.a(a, paramInt1, paramInt2);
    k.a(localDrawable, "ThemeUtils.getTintedDraw…, drawableRes, colorAttr)");
    return localDrawable;
  }
  
  public final String a(int paramInt1, int paramInt2, Object... paramVarArgs)
  {
    k.b(paramVarArgs, "formatArgs");
    paramVarArgs = a.getResources().getQuantityString(paramInt1, paramInt2, Arrays.copyOf(paramVarArgs, paramVarArgs.length));
    k.a(paramVarArgs, "context.resources.getQua…d, quantity, *formatArgs)");
    return paramVarArgs;
  }
  
  public final String a(int paramInt, Object... paramVarArgs)
  {
    k.b(paramVarArgs, "formatArgs");
    paramVarArgs = a.getString(paramInt, Arrays.copyOf(paramVarArgs, paramVarArgs.length));
    k.a(paramVarArgs, "context.getString(resId, *formatArgs)");
    return paramVarArgs;
  }
  
  public final String[] a(int paramInt)
  {
    String[] arrayOfString = a.getResources().getStringArray(paramInt);
    k.a(arrayOfString, "context.resources.getStringArray(resId)");
    return arrayOfString;
  }
  
  public final int b(int paramInt)
  {
    return a.getResources().getDimensionPixelSize(paramInt);
  }
  
  public final CharSequence b(int paramInt, Object... paramVarArgs)
  {
    k.b(paramVarArgs, "formatArgs");
    if (Build.VERSION.SDK_INT >= 24)
    {
      paramVarArgs = Html.fromHtml(a(paramInt, Arrays.copyOf(paramVarArgs, paramVarArgs.length)), 0);
      k.a(paramVarArgs, "Html.fromHtml(getString(…AGRAPH_LINES_CONSECUTIVE)");
      return (CharSequence)paramVarArgs;
    }
    paramVarArgs = Html.fromHtml(a(paramInt, Arrays.copyOf(paramVarArgs, paramVarArgs.length)));
    k.a(paramVarArgs, "Html.fromHtml(getString(resId, *formatArgs))");
    return (CharSequence)paramVarArgs;
  }
  
  public final Drawable c(int paramInt)
  {
    if (Build.VERSION.SDK_INT >= 21)
    {
      localDrawable = a.getDrawable(paramInt);
      k.a(localDrawable, "context.getDrawable(drawableRes)");
      return localDrawable;
    }
    Drawable localDrawable = a.getResources().getDrawable(paramInt);
    k.a(localDrawable, "context.resources.getDrawable(drawableRes)");
    return localDrawable;
  }
  
  public final int d(int paramInt)
  {
    return a.getResources().getColor(paramInt);
  }
  
  public final int e(int paramInt)
  {
    return b.a(a, paramInt);
  }
  
  public final Drawable f(int paramInt)
  {
    return b.c(a, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */