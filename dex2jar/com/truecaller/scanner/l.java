package com.truecaller.scanner;

import c.g.b.k;
import c.u;
import com.truecaller.common.g.a;
import com.truecaller.utils.d;
import java.util.Set;

public final class l
{
  private final d a;
  private final a b;
  
  public l(d paramd, a parama)
  {
    a = paramd;
    b = parama;
  }
  
  public final boolean a()
  {
    if (b.a("featureNumberScanner", false))
    {
      int i = a.o();
      if (i != -1)
      {
        if (i >= m.a())
        {
          Set localSet = m.b();
          String str = a.l();
          if (str != null)
          {
            str = str.toLowerCase();
            k.a(str, "(this as java.lang.String).toLowerCase()");
            if (localSet.contains(str)) {
              return false;
            }
          }
          else
          {
            throw new u("null cannot be cast to non-null type java.lang.String");
          }
        }
        return true;
      }
      return false;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.scanner.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */