package com.truecaller.log;

import c.g.b.k;
import com.crashlytics.android.a;
import io.grpc.bc;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import javax.net.ssl.SSLException;

public final class d
{
  public static boolean a;
  
  public static final void a(int paramInt, String paramString1, String paramString2)
  {
    k.b(paramString1, "tag");
    k.b(paramString2, "msg");
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString1);
    localStringBuilder.append(": ");
    localStringBuilder.append(paramString2);
    localStringBuilder.toString();
    if (paramInt != 6) {}
  }
  
  public static final void a(String paramString)
  {
    k.b(paramString, "msg");
  }
  
  public static final void a(Throwable paramThrowable)
  {
    k.b(paramThrowable, "throwable");
    a(paramThrowable, null);
  }
  
  public static final void a(Throwable paramThrowable, String paramString)
  {
    k.b(paramThrowable, "exception");
    if (paramString == null) {
      paramThrowable.getMessage();
    }
    if ((a) && (b(paramThrowable))) {
      a.a(b.a(paramThrowable));
    }
  }
  
  private static final boolean b(Throwable paramThrowable)
  {
    return (!(paramThrowable instanceof ConnectException)) && (!(paramThrowable instanceof UnknownHostException)) && (!(paramThrowable instanceof SocketTimeoutException)) && (!(paramThrowable instanceof SocketException)) && (!(paramThrowable instanceof SSLException)) && (!(paramThrowable instanceof bc));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.log.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */