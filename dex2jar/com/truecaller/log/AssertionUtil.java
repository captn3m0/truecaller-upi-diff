package com.truecaller.log;

import android.os.Debug;
import android.os.Handler;
import android.os.Looper;

public final class AssertionUtil
{
  private static final String NOT_ON_MAIN_EXPLANATION = "Should be executing on main thread, but isn't!";
  private static final String ON_MAIN_EXPLANATION = "Should NOT be executing on main thread, but is! Naughty naughty!!";
  private static boolean sDisableAsserts = false;
  private static boolean sIsDebugBuild;
  
  public static void isFalse(boolean paramBoolean, String... paramVarArgs)
  {
    if ((!sDisableAsserts) && (paramBoolean))
    {
      reportMessages(paramVarArgs);
      if (sIsDebugBuild)
      {
        throwHard(new TcAssertionError(summarize(paramVarArgs), null));
        return;
      }
      reportThrowableButNeverCrash(new TcDryAssertionError(summarize(paramVarArgs), null));
    }
  }
  
  public static void isNotNull(Object paramObject, String... paramVarArgs)
  {
    if ((!sDisableAsserts) && (paramObject == null))
    {
      reportMessages(paramVarArgs);
      if (sIsDebugBuild)
      {
        throwHard(new TcAssertionError(summarize(paramVarArgs), null));
        return;
      }
      reportThrowableButNeverCrash(new TcDryAssertionError(summarize(paramVarArgs), null));
    }
  }
  
  public static void isNull(Object paramObject, String... paramVarArgs)
  {
    if ((!sDisableAsserts) && (paramObject != null))
    {
      reportMessages(paramVarArgs);
      if (sIsDebugBuild)
      {
        throwHard(new TcAssertionError(summarize(paramVarArgs), null));
        return;
      }
      reportThrowableButNeverCrash(new TcDryAssertionError(summarize(paramVarArgs), null));
    }
  }
  
  private static boolean isOnMainThread()
  {
    return Looper.getMainLooper().getThread() == Thread.currentThread();
  }
  
  public static void isTrue(boolean paramBoolean, String... paramVarArgs)
  {
    if ((!sDisableAsserts) && (!paramBoolean))
    {
      reportMessages(paramVarArgs);
      if (sIsDebugBuild)
      {
        throwHard(new TcAssertionError(summarize(paramVarArgs), null));
        return;
      }
      reportThrowableButNeverCrash(new TcDryAssertionError(summarize(paramVarArgs), null));
    }
  }
  
  public static void notOnMainThread(String... paramVarArgs)
  {
    if ((!sDisableAsserts) && (isOnMainThread()))
    {
      reportMessages(paramVarArgs);
      if (sIsDebugBuild)
      {
        localStringBuilder = new StringBuilder("Should NOT be executing on main thread, but is! Naughty naughty!! ");
        localStringBuilder.append(summarize(paramVarArgs));
        throwHard(new TcAssertionError(localStringBuilder.toString(), null));
        return;
      }
      StringBuilder localStringBuilder = new StringBuilder("Should NOT be executing on main thread, but is! Naughty naughty!! ");
      localStringBuilder.append(summarize(paramVarArgs));
      reportThrowableButNeverCrash(new TcDryAssertionError(localStringBuilder.toString(), null));
    }
  }
  
  public static void onMainThread(String... paramVarArgs)
  {
    if ((!sDisableAsserts) && (!isOnMainThread()))
    {
      reportMessages(paramVarArgs);
      if (sIsDebugBuild)
      {
        localStringBuilder = new StringBuilder("Should be executing on main thread, but isn't! ");
        localStringBuilder.append(summarize(paramVarArgs));
        throwHard(new TcAssertionError(localStringBuilder.toString(), null));
        return;
      }
      StringBuilder localStringBuilder = new StringBuilder("Should be executing on main thread, but isn't! ");
      localStringBuilder.append(summarize(paramVarArgs));
      reportThrowableButNeverCrash(new TcDryAssertionError(localStringBuilder.toString(), null));
    }
  }
  
  public static void onSameThread(Thread paramThread, String... paramVarArgs)
  {
    if ((!sDisableAsserts) && (Thread.currentThread() != paramThread))
    {
      reportMessages(paramVarArgs);
      if (sIsDebugBuild)
      {
        StringBuilder localStringBuilder = new StringBuilder("Must be executed on thread [");
        localStringBuilder.append(paramThread.getName());
        localStringBuilder.append("] but was on thread [");
        localStringBuilder.append(Thread.currentThread().getName());
        localStringBuilder.append("] ");
        localStringBuilder.append(summarize(paramVarArgs));
        throwHard(new TcAssertionError(localStringBuilder.toString(), null));
        return;
      }
      paramThread = new StringBuilder("Should NOT be executing on main thread, but is! Naughty naughty!! ");
      paramThread.append(summarize(paramVarArgs));
      reportThrowableButNeverCrash(new TcDryAssertionError(paramThread.toString(), null));
    }
  }
  
  private static void removeMyselfFromTopOfStacktrace(Throwable paramThrowable)
  {
    StackTraceElement[] arrayOfStackTraceElement = paramThrowable.getStackTrace();
    Object localObject = AssertionUtil.class.getCanonicalName();
    int i = 0;
    while ((i < arrayOfStackTraceElement.length - 1) && (arrayOfStackTraceElement[i].getClassName().startsWith((String)localObject))) {
      i += 1;
    }
    if (i > 0)
    {
      localObject = new StackTraceElement[arrayOfStackTraceElement.length - i];
      System.arraycopy(arrayOfStackTraceElement, i, localObject, 0, localObject.length);
      paramThrowable.setStackTrace((StackTraceElement[])localObject);
    }
  }
  
  public static void report(String... paramVarArgs)
  {
    if (sDisableAsserts) {
      return;
    }
    reportWithSummary(summarize(paramVarArgs), paramVarArgs);
  }
  
  private static void reportMessages(String... paramVarArgs)
  {
    int j = paramVarArgs.length;
    int i = 0;
    while (i < j)
    {
      String str = paramVarArgs[i];
      if ((!sIsDebugBuild) && (!Debug.isDebuggerConnected())) {
        d.a(str);
      }
      i += 1;
    }
  }
  
  public static void reportThrowableButNeverCrash(Throwable paramThrowable)
  {
    if (sDisableAsserts) {
      return;
    }
    if ((!sIsDebugBuild) && (!Debug.isDebuggerConnected()))
    {
      d.a(paramThrowable);
      return;
    }
    paramThrowable.printStackTrace();
  }
  
  public static void reportWeirdnessButNeverCrash(String paramString)
  {
    shouldNeverHappen(new TcDryAssertionError(paramString, null), new String[0]);
  }
  
  public static void reportWithSummary(String paramString, String... paramVarArgs)
  {
    if (sDisableAsserts) {
      return;
    }
    reportMessages(paramVarArgs);
    reportThrowableButNeverCrash(new TcDryAssertionError(paramString, null));
  }
  
  public static void setDisableAsserts(boolean paramBoolean)
  {
    sDisableAsserts = paramBoolean;
  }
  
  public static void setIsDebugBuild(boolean paramBoolean)
  {
    sIsDebugBuild = paramBoolean;
  }
  
  public static void shouldNeverHappen(Throwable paramThrowable, String... paramVarArgs)
  {
    if (sDisableAsserts) {
      return;
    }
    reportMessages(paramVarArgs);
    if (sIsDebugBuild)
    {
      paramVarArgs = new TcAssertionError(summarize(paramVarArgs), null);
      paramVarArgs.initCause(paramThrowable);
      throwHard(paramVarArgs);
      return;
    }
    paramVarArgs = new TcDryAssertionError(summarize(paramVarArgs), null);
    paramVarArgs.initCause(paramThrowable);
    reportThrowableButNeverCrash(paramVarArgs);
  }
  
  private static String summarize(String[] paramArrayOfString)
  {
    if ((paramArrayOfString != null) && (paramArrayOfString.length > 0) && (paramArrayOfString[0] != null)) {
      return paramArrayOfString[0];
    }
    return "";
  }
  
  private static void throwHard(TcAssertionError paramTcAssertionError)
  {
    if (sDisableAsserts) {
      return;
    }
    reportThrowableButNeverCrash(paramTcAssertionError);
    try
    {
      new Handler(Looper.getMainLooper()).post(new Runnable()
      {
        public final void run()
        {
          throw val$e;
        }
      });
      throw paramTcAssertionError;
    }
    catch (Throwable localThrowable)
    {
      for (;;) {}
    }
  }
  
  static class TcAssertionError
    extends AssertionError
  {
    private TcAssertionError(Object paramObject)
    {
      super();
      AssertionUtil.removeMyselfFromTopOfStacktrace(this);
    }
  }
  
  static class TcDryAssertionError
    extends AssertionError
  {
    private TcDryAssertionError(Object paramObject)
    {
      super();
      AssertionUtil.removeMyselfFromTopOfStacktrace(this);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.log.AssertionUtil
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */