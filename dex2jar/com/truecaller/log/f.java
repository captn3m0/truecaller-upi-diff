package com.truecaller.log;

import android.os.Bundle;
import android.util.Log;
import java.util.Iterator;
import java.util.Set;

public final class f
{
  public static boolean a;
  
  public static String a(Throwable paramThrowable)
  {
    StringBuilder localStringBuilder = new StringBuilder("Exception: ");
    localStringBuilder.append(paramThrowable.getMessage());
    localStringBuilder.append(", Stack: ");
    localStringBuilder.append(Log.getStackTraceString(paramThrowable));
    return localStringBuilder.toString();
  }
  
  public static void a(Bundle paramBundle)
  {
    if (paramBundle != null)
    {
      Iterator localIterator = paramBundle.keySet().iterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        StringBuilder localStringBuilder = new StringBuilder("[");
        localStringBuilder.append(str);
        localStringBuilder.append("=");
        localStringBuilder.append(paramBundle.get(str));
        localStringBuilder.append("]");
        localStringBuilder.toString();
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.log.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */