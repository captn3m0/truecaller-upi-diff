package com.truecaller.push;

import android.content.Context;
import com.facebook.appevents.g;
import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d<g>
{
  private final Provider<Context> a;
  
  private j(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static j a(Provider<Context> paramProvider)
  {
    return new j(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.push.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */