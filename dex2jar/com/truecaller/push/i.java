package com.truecaller.push;

import android.content.Context;
import c.g.b.k;
import com.facebook.appevents.g;

public abstract class i
{
  public static final i.a a = new i.a((byte)0);
  
  public static final g a(Context paramContext)
  {
    k.b(paramContext, "context");
    paramContext = g.a(paramContext);
    k.a(paramContext, "AppEventsLogger.newLogger(context)");
    return paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.push.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */