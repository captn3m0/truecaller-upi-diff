package com.truecaller.push;

import android.content.Context;
import com.facebook.appevents.g;
import com.truecaller.common.g.a;
import javax.inject.Provider;

public final class d
  implements dagger.a.d<c>
{
  private final Provider<Context> a;
  private final Provider<a> b;
  private final Provider<g> c;
  
  private d(Provider<Context> paramProvider, Provider<a> paramProvider1, Provider<g> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static d a(Provider<Context> paramProvider, Provider<a> paramProvider1, Provider<g> paramProvider2)
  {
    return new d(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.push.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */