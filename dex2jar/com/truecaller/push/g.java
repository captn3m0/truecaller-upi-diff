package com.truecaller.push;

import com.truecaller.common.account.r;
import com.truecaller.i.e;
import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d<f>
{
  private final Provider<e> a;
  private final Provider<r> b;
  private final Provider<c.d.f> c;
  
  private g(Provider<e> paramProvider, Provider<r> paramProvider1, Provider<c.d.f> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static g a(Provider<e> paramProvider, Provider<r> paramProvider1, Provider<c.d.f> paramProvider2)
  {
    return new g(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.push.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */