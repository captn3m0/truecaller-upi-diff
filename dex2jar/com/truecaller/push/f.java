package com.truecaller.push;

import c.g.a.m;
import c.g.b.k;
import com.facebook.appevents.g;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.iid.FirebaseInstanceId;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.d;
import e.b;
import java.util.concurrent.ExecutionException;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

public final class f
  implements e
{
  private final com.truecaller.i.e a;
  private final com.truecaller.common.account.r b;
  private final c.d.f c;
  
  @Inject
  public f(com.truecaller.i.e parame, com.truecaller.common.account.r paramr, @Named("Async") c.d.f paramf)
  {
    a = parame;
    b = paramr;
    c = paramf;
  }
  
  private static void c(String paramString)
  {
    com.truecaller.debug.log.a.a(new Object[] { paramString });
  }
  
  public final String a()
  {
    return a.a("fcmRegisteredOnServer");
  }
  
  public final boolean a(String paramString)
  {
    if (!b()) {
      return false;
    }
    String str;
    if (paramString == null) {
      str = c();
    } else {
      str = paramString;
    }
    Object localObject1 = (CharSequence)str;
    int i;
    if ((localObject1 != null) && (((CharSequence)localObject1).length() != 0)) {
      i = 0;
    } else {
      i = 1;
    }
    if (i != 0)
    {
      paramString = new StringBuilder();
      paramString.append(e.class.getName());
      paramString.append(": push token is NULL");
      c(paramString.toString());
      d.a((Throwable)new a());
      return false;
    }
    a.a("gcmRegistrationId", str);
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append(e.class.getName());
    ((StringBuilder)localObject1).append(": push token for registration: ");
    ((StringBuilder)localObject1).append(paramString);
    c(((StringBuilder)localObject1).toString());
    Object localObject2 = new PushIdDto(str, 1);
    localObject1 = null;
    try
    {
      localObject2 = h.a((PushIdDto)localObject2).c();
      localObject1 = localObject2;
    }
    catch (Exception localException)
    {
      d.a((Throwable)localException);
    }
    catch (SecurityException localSecurityException)
    {
      AssertionUtil.shouldNeverHappen((Throwable)localSecurityException, new String[0]);
    }
    if ((localObject1 != null) && (((e.r)localObject1).d() == true))
    {
      localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append(e.class.getName());
      ((StringBuilder)localObject1).append(": push token is registered: ");
      ((StringBuilder)localObject1).append(paramString);
      c(((StringBuilder)localObject1).toString());
      a.a("fcmRegisteredOnServer", str);
      g.a(str);
      return true;
    }
    return false;
  }
  
  public final void b(String paramString)
  {
    kotlinx.coroutines.e.b((ag)bg.a, c, (m)new f.a(this, paramString, null), 2);
  }
  
  public final boolean b()
  {
    return b.c();
  }
  
  public final String c()
  {
    Object localObject1 = FirebaseInstanceId.a();
    k.a(localObject1, "FirebaseInstanceId.getInstance()");
    localObject1 = ((FirebaseInstanceId)localObject1).e();
    k.a(localObject1, "FirebaseInstanceId.getInstance().instanceId");
    Object localObject2;
    try
    {
      Tasks.a((Task)localObject1);
    }
    catch (InterruptedException localInterruptedException)
    {
      Task localTask = Tasks.a((Exception)localInterruptedException);
      k.a(localTask, "Tasks.forException(e)");
    }
    catch (ExecutionException localExecutionException)
    {
      localObject2 = Tasks.a((Exception)localExecutionException);
      k.a(localObject2, "Tasks.forException(e)");
    }
    if (((Task)localObject2).b())
    {
      localObject2 = (com.google.firebase.iid.a)((Task)localObject2).d();
      if (localObject2 != null) {
        return ((com.google.firebase.iid.a)localObject2).a();
      }
      return null;
    }
    return a.a("gcmRegistrationId");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.push.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */