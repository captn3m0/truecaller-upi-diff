package com.truecaller.push;

import android.content.Context;
import android.os.Bundle;
import c.g.b.k;
import c.m.o.a;
import com.facebook.appevents.g;
import com.google.gson.f;
import com.google.gson.o;
import com.google.gson.q;
import com.truecaller.common.g.a;
import com.truecaller.log.d;
import com.truecaller.network.notification.c.a.a;
import com.truecaller.old.data.entity.Notification;
import com.truecaller.old.data.entity.Notification.NotificationState;
import com.truecaller.util.NotificationUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.json.JSONArray;
import org.json.JSONObject;

public final class c
  implements b
{
  private final List<Bundle> a;
  private final Context b;
  private final a c;
  private final g d;
  
  @Inject
  public c(Context paramContext, a parama, g paramg)
  {
    b = paramContext;
    c = parama;
    d = paramg;
    a = ((List)new ArrayList());
  }
  
  private static Notification a(Bundle paramBundle)
  {
    Object localObject1 = paramBundle.getString("e");
    paramBundle = paramBundle.getString("a");
    Object localObject2 = (CharSequence)localObject1;
    int j = 0;
    int i;
    if ((localObject2 != null) && (((CharSequence)localObject2).length() != 0)) {
      i = 0;
    } else {
      i = 1;
    }
    if (i != 0)
    {
      localObject2 = (CharSequence)paramBundle;
      if (localObject2 != null)
      {
        i = j;
        if (((CharSequence)localObject2).length() != 0) {}
      }
      else
      {
        i = 1;
      }
      if (i != 0) {}
    }
    else
    {
      try
      {
        localObject2 = new o();
        if (localObject1 != null)
        {
          localObject1 = q.a((String)localObject1);
          k.a(localObject1, "JsonParser().parse(it)");
          ((o)localObject2).a("e", (com.google.gson.l)((com.google.gson.l)localObject1).i());
        }
        if (paramBundle != null)
        {
          paramBundle = q.a(paramBundle);
          k.a(paramBundle, "JsonParser().parse(it)");
          ((o)localObject2).a("a", (com.google.gson.l)paramBundle.i());
        }
        paramBundle = new Notification((o)localObject2, Notification.NotificationState.NEW);
        return paramBundle;
      }
      catch (RuntimeException paramBundle)
      {
        paramBundle = (Throwable)paramBundle;
        localObject1 = new StringBuilder();
        ((StringBuilder)localObject1).append(b.class);
        ((StringBuilder)localObject1).append(" asNotification - error while parsing notification");
        d.a(paramBundle, ((StringBuilder)localObject1).toString());
      }
    }
    return null;
  }
  
  private void d()
  {
    Object localObject = c.m.l.c(c.a.m.n((Iterable)a), (c.g.a.b)c.a.a);
    JSONArray localJSONArray = new JSONArray();
    localObject = ((c.m.i)localObject).a();
    while (((Iterator)localObject).hasNext())
    {
      localJSONArray = localJSONArray.put((JSONObject)((Iterator)localObject).next());
      k.a(localJSONArray, "jsonArray.put(jsonObject)");
    }
    k.a(localJSONArray, "payloads.asSequence()\n  …onArray.put(jsonObject) }");
    c.a("payloads", localJSONArray.toString());
  }
  
  public final c.a.a a(Map<String, String> paramMap)
  {
    if (paramMap != null)
    {
      Object localObject2 = (String)paramMap.get("c");
      Object localObject1 = (String)paramMap.get("c.d");
      paramMap = (String)paramMap.get("c.o");
      CharSequence localCharSequence = (CharSequence)localObject2;
      int k = 1;
      int j = 0;
      if ((localCharSequence != null) && (localCharSequence.length() != 0)) {
        i = 0;
      } else {
        i = 1;
      }
      if (i == 0) {
        return (c.a.a)new f().a((String)localObject2, c.a.a.class);
      }
      localObject2 = (CharSequence)localObject1;
      if ((localObject2 != null) && (((CharSequence)localObject2).length() != 0)) {
        i = 0;
      } else {
        i = 1;
      }
      if (i != 0)
      {
        localObject2 = (CharSequence)paramMap;
        i = k;
        if (localObject2 != null) {
          if (((CharSequence)localObject2).length() == 0) {
            i = k;
          } else {
            i = 0;
          }
        }
        if (i != 0) {
          return null;
        }
      }
      localObject2 = new c.a.a();
      if (localObject1 != null)
      {
        localObject1 = c.n.m.b((String)localObject1);
        if (localObject1 != null)
        {
          i = ((Integer)localObject1).intValue();
          break label215;
        }
      }
      int i = 0;
      label215:
      a = i;
      i = j;
      if (paramMap != null)
      {
        paramMap = c.n.m.b(paramMap);
        i = j;
        if (paramMap != null) {
          i = paramMap.intValue();
        }
      }
      b = i;
      return (c.a.a)localObject2;
    }
    return null;
  }
  
  public final void a()
  {
    Iterator localIterator = a.iterator();
    while (localIterator.hasNext())
    {
      Bundle localBundle = (Bundle)localIterator.next();
      d.a(localBundle);
    }
  }
  
  public final void a(Bundle paramBundle, long paramLong)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(b.class);
    localStringBuilder.append(" onMessage with Intent");
    localStringBuilder.toString();
    if (paramBundle != null)
    {
      a.add(paramBundle);
      d();
      paramBundle = a(paramBundle);
      if (paramBundle != null) {
        try
        {
          NotificationUtil.a(paramBundle, b, paramLong);
        }
        catch (RuntimeException paramBundle)
        {
          paramBundle = (Throwable)paramBundle;
          localStringBuilder = new StringBuilder();
          localStringBuilder.append(b.class);
          localStringBuilder.append(" onNotification - error while handling notification");
          d.a(paramBundle, localStringBuilder.toString());
        }
      }
    }
    NotificationUtil.b(b);
  }
  
  public final void b()
  {
    Object localObject = c.a("payloads");
    if (localObject == null) {
      return;
    }
    k.a(localObject, "coreSettings.getString(P…SSAGE_PAYLOADS) ?: return");
    JSONArray localJSONArray = new JSONArray((String)localObject);
    localObject = c.a.m.n((Iterable)c.k.i.b(0, localJSONArray.length()));
    Collection localCollection = (Collection)a;
    Iterator localIterator1 = ((c.m.i)localObject).a();
    while (localIterator1.hasNext())
    {
      JSONObject localJSONObject = localJSONArray.getJSONObject(((Number)localIterator1.next()).intValue());
      if (localJSONObject != null)
      {
        localObject = localJSONObject.keys();
        k.a(localObject, "jsonObject.keys()");
        k.b(localObject, "receiver$0");
        localObject = c.m.l.b((c.m.i)new o.a((Iterator)localObject));
        Bundle localBundle = new Bundle();
        Iterator localIterator2 = ((c.m.i)localObject).a();
        for (;;)
        {
          localObject = localBundle;
          if (!localIterator2.hasNext()) {
            break;
          }
          localObject = (String)localIterator2.next();
          localBundle.putString((String)localObject, localJSONObject.getString((String)localObject));
        }
      }
      localObject = null;
      if (localObject != null) {
        localCollection.add(localObject);
      }
    }
  }
  
  public final void c()
  {
    a.clear();
    c.d("payloads");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.push.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */