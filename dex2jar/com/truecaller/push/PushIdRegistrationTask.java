package com.truecaller.push;

import android.content.Context;
import android.os.Bundle;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.background.e.a;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;

public final class PushIdRegistrationTask
  extends PersistentBackgroundTask
{
  @Inject
  public e a;
  
  public PushIdRegistrationTask()
  {
    TrueApp localTrueApp = TrueApp.y();
    k.a(localTrueApp, "TrueApp.getApp()");
    localTrueApp.a().a(this);
  }
  
  public final int a()
  {
    return 10024;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    k.b(paramContext, "context");
    paramContext = a;
    if (paramContext == null) {
      k.a("pushIdManager");
    }
    if (paramContext.a(null)) {
      return PersistentBackgroundTask.RunResult.Success;
    }
    return PersistentBackgroundTask.RunResult.FailedRetry;
  }
  
  public final boolean a(Context paramContext)
  {
    k.b(paramContext, "serviceContext");
    paramContext = a;
    if (paramContext == null) {
      k.a("pushIdManager");
    }
    return paramContext.b();
  }
  
  public final com.truecaller.common.background.e b()
  {
    com.truecaller.common.background.e locale = new e.a(1).a(1L, TimeUnit.DAYS).b(2L, TimeUnit.HOURS).c(1L, TimeUnit.HOURS).a(1).b();
    k.a(locale, "TaskConfiguration.Builde…YPE_ANY)\n        .build()");
    return locale;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.push.PushIdRegistrationTask
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */