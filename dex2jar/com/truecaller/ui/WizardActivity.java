package com.truecaller.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ac;
import androidx.work.p;
import com.truecaller.TrueApp;
import com.truecaller.ads.provider.fetch.AdsConfigurationManager;
import com.truecaller.analytics.AppHeartBeatTask;
import com.truecaller.analytics.AppSettingsTask;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.bb;
import com.truecaller.backup.BackupLogWorker;
import com.truecaller.backup.BackupLogWorker.a;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.background.g;
import com.truecaller.common.background.h;
import com.truecaller.common.tag.TagService;
import com.truecaller.common.tag.sync.AvailableTagsDownloadWorker;
import com.truecaller.common.tag.sync.AvailableTagsDownloadWorker.a;
import com.truecaller.common.tag.sync.TagKeywordsDownloadWorker;
import com.truecaller.common.tag.sync.TagKeywordsDownloadWorker.a;
import com.truecaller.f.a.j;
import com.truecaller.filters.sync.FilterRestoreWorker;
import com.truecaller.filters.sync.TopSpammersSyncRecurringWorker;
import com.truecaller.filters.sync.TopSpammersSyncRecurringWorker.a;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.transport.im.FetchImContactsWorker;
import com.truecaller.network.spamUrls.FetchSpamLinksWhiteListWorker;
import com.truecaller.network.spamUrls.FetchSpamLinksWhiteListWorker.a;
import com.truecaller.old.data.access.Settings;
import com.truecaller.old.data.access.d.a;
import com.truecaller.presence.SendPresenceSettingWorker;
import com.truecaller.referral.ReferralManager;
import com.truecaller.referral.w;
import com.truecaller.tracking.events.k;
import com.truecaller.tracking.events.k.a;
import com.truecaller.wizard.TruecallerWizard;
import com.truecaller.wizard.d.c;
import java.util.concurrent.TimeUnit;

public class WizardActivity
  extends TruecallerWizard
{
  private bp c;
  private final com.truecaller.common.e.e d = new com.truecaller.common.e.e();
  
  public final void a()
  {
    super.a();
    TagService.a(this);
    ac.a(this).a(null, 2131362836);
  }
  
  public final void b()
  {
    setResult(-1);
    super.b();
    boolean bool = com.truecaller.common.b.e.a("languageAuto", true);
    int i = 0;
    if (!bool)
    {
      localObject1 = TrueApp.x();
      localObject3 = new com.truecaller.old.data.entity.b(com.truecaller.common.e.e.b(com.truecaller.common.b.e.a("language")));
      Settings.a("languageAuto", false);
      Settings.a((Context)localObject1, (com.truecaller.old.data.entity.b)localObject3);
      localObject1 = (com.truecaller.common.b.a)getApplication();
      c.a(new d.a((com.truecaller.common.b.a)localObject1));
    }
    Settings.a(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(1L));
    c.aS().a("KeyCallLogPromoDisabledUntil", System.currentTimeMillis() + j.a());
    Object localObject1 = com.truecaller.common.b.a.F();
    AppSettingsTask.a(c);
    AppHeartBeatTask.b(c);
    ((com.truecaller.common.b.a)localObject1).J();
    com.truecaller.util.f.a(getApplicationContext(), 0);
    Object localObject3 = getIntent();
    try
    {
      localObject1 = c.f();
      localObject3 = ((Intent)localObject3).getStringExtra("EXTRA_REG_NUDGE");
      if (localObject3 != null)
      {
        ((ae)((com.truecaller.androidactors.f)localObject1).a()).a(k.b().a("RegistrationNudge").b((CharSequence)localObject3).a());
      }
      else if (com.truecaller.common.b.e.a("regNudgeBadgeSet", false))
      {
        com.truecaller.util.f.a(getApplicationContext(), 0);
        ((ae)((com.truecaller.androidactors.f)localObject1).a()).a(k.b().a("RegistrationNudge").b("Badge").a());
      }
    }
    catch (org.apache.a.a locala)
    {
      AssertionUtil.reportThrowableButNeverCrash(locala);
    }
    Object localObject2 = c.I();
    localObject3 = c.f();
    if (((com.truecaller.common.g.a)localObject2).b("core_viewed_region_1"))
    {
      bb.a((com.truecaller.androidactors.f)localObject3, "consentWizard", "viewed");
      ((com.truecaller.common.g.a)localObject2).d("core_viewed_region_1");
    }
    if (((com.truecaller.common.g.a)localObject2).b("core_accepted_region_1"))
    {
      bb.a((com.truecaller.androidactors.f)localObject3, "consentWizard", "accepted");
      ((com.truecaller.common.g.a)localObject2).d("core_accepted_region_1");
    }
    c.aw().f();
    localObject2 = FetchImContactsWorker.d;
    localObject3 = SendPresenceSettingWorker.e;
    AvailableTagsDownloadWorker.a locala1 = AvailableTagsDownloadWorker.d;
    TagKeywordsDownloadWorker.a locala2 = TagKeywordsDownloadWorker.e;
    FetchSpamLinksWhiteListWorker.a locala3 = FetchSpamLinksWhiteListWorker.e;
    TopSpammersSyncRecurringWorker.a locala4 = TopSpammersSyncRecurringWorker.e;
    BackupLogWorker.a locala5 = BackupLogWorker.d;
    p localp = p.a();
    while (i < 7)
    {
      localp.a(new h[] { localObject2, localObject3, locala1, locala2, locala3, locala4, locala5 }[i].a().b());
      i += 1;
    }
    FilterRestoreWorker.b();
    if ((getIntent() == null) || (!getIntent().hasExtra("extraRequestCode"))) {
      TruecallerInit.a(this, "calls", "wizard");
    }
  }
  
  public final c c()
  {
    return new WizardActivity.a(c.ad());
  }
  
  public void onCreate(Bundle paramBundle)
  {
    c = ((bk)getApplication()).a();
    super.onCreate(paramBundle);
    setResult(0);
    paramBundle = w.a(this, "ReferralManagerImpl");
    if (paramBundle != null) {
      paramBundle.a(getApplicationContext());
    }
    if (getIntent().getStringExtra("EXTRA_REG_NUDGE") != null) {
      com.truecaller.common.b.e.b("signUpOrigin", "notificationRegNudge");
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.WizardActivity
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */