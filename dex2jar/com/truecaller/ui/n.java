package com.truecaller.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.ViewGroup.MarginLayoutParams;
import com.truecaller.TrueApp;
import com.truecaller.analytics.bb;
import com.truecaller.androidactors.f;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.b.a;
import com.truecaller.log.d;
import com.truecaller.old.data.access.Settings;
import com.truecaller.tcpermissions.l;
import com.truecaller.ui.components.ScrimInsetsFrameLayout;
import com.truecaller.ui.components.ScrimInsetsFrameLayout.a;
import com.truecaller.util.at;

public abstract class n
  extends AppCompatActivity
  implements ScrimInsetsFrameLayout.a
{
  protected Fragment e;
  protected l f;
  public boolean g;
  Toolbar h;
  
  public final void a(Rect paramRect)
  {
    Toolbar localToolbar = h;
    if (localToolbar != null)
    {
      ViewGroup.MarginLayoutParams localMarginLayoutParams = (ViewGroup.MarginLayoutParams)localToolbar.getLayoutParams();
      topMargin = top;
      localToolbar.setLayoutParams(localMarginLayoutParams);
    }
  }
  
  protected final void a(o paramo)
  {
    b(paramo);
  }
  
  protected boolean a()
  {
    return true;
  }
  
  protected void b(o paramo)
  {
    getSupportFragmentManager().a().b(2131362594, paramo, null).c();
    e = paramo;
  }
  
  protected boolean b()
  {
    return false;
  }
  
  protected void c() {}
  
  protected int g()
  {
    return 2130969592;
  }
  
  public void onBackPressed()
  {
    if (b()) {
      return;
    }
    Fragment localFragment = e;
    if (((localFragment instanceof p)) && (((p)localFragment).W_())) {
      return;
    }
    try
    {
      super.onBackPressed();
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      d.a(localIllegalStateException);
    }
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    Settings.c(this);
    super.onConfigurationChanged(getResources().getConfiguration());
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Settings.c(this);
    if (paramBundle == null)
    {
      Object localObject = a.F();
      paramBundle = getIntent();
      if (paramBundle != null)
      {
        localObject = ((bk)((Context)localObject).getApplicationContext()).a().f();
        String str1 = paramBundle.getStringExtra("AppUserInteraction.Context");
        String str2 = paramBundle.getStringExtra("AppUserInteraction.Action");
        if ((!TextUtils.isEmpty(str1)) && (!TextUtils.isEmpty(str2)))
        {
          bb.a((f)localObject, str1, str2);
        }
        else if (("android.intent.action.MAIN".equals(paramBundle.getAction())) && (paramBundle.hasCategory("android.intent.category.LAUNCHER")))
        {
          paramBundle = paramBundle.getComponent();
          if ((paramBundle != null) && (paramBundle.getClassName().equals(ContactsActivity.class.getName()))) {
            bb.a((f)localObject, "contactsIcon", "openApp");
          } else {
            bb.a((f)localObject, "appIcon", "openApp");
          }
        }
      }
    }
    f = TrueApp.y().a().bt();
  }
  
  public void onDestroy()
  {
    g = true;
    super.onDestroy();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(getClass().getSimpleName());
    localStringBuilder.append("#onDestroy()");
    localStringBuilder.toString();
  }
  
  public void onPause()
  {
    super.onPause();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(getClass().getSimpleName());
    localStringBuilder.append("#onPause()");
    localStringBuilder.toString();
  }
  
  public void onPostCreate(Bundle paramBundle)
  {
    super.onPostCreate(paramBundle);
    paramBundle = (ScrimInsetsFrameLayout)findViewById(2131362435);
    if (paramBundle != null) {
      paramBundle.setOnInsetsCallback(this);
    }
  }
  
  public boolean onPrepareOptionsMenu(Menu paramMenu)
  {
    int j = g();
    int i = 0;
    while (i < paramMenu.size())
    {
      at.a(this, paramMenu.getItem(i), j);
      i += 1;
    }
    return super.onPrepareOptionsMenu(paramMenu);
  }
  
  public void onResume()
  {
    super.onResume();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(getClass().getSimpleName());
    localStringBuilder.append("#onResume()");
    localStringBuilder.toString();
    if ((a()) && (!f.f()))
    {
      c();
      RequiredPermissionsActivity.a(this);
      finish();
      return;
    }
    supportInvalidateOptionsMenu();
  }
  
  public void onStart()
  {
    super.onStart();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(getClass().getSimpleName());
    localStringBuilder.append("#onStart()");
    localStringBuilder.toString();
  }
  
  public void onStop()
  {
    super.onStop();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(getClass().getSimpleName());
    localStringBuilder.append("#onStop()");
    localStringBuilder.toString();
  }
  
  public void setSupportActionBar(Toolbar paramToolbar)
  {
    h = paramToolbar;
    super.setSupportActionBar(paramToolbar);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */