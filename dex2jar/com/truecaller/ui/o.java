package com.truecaller.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;
import com.truecaller.common.b.a;
import com.truecaller.log.d;
import com.truecaller.old.a.c;
import com.truecaller.ui.dialogs.d.a;
import com.truecaller.ui.dialogs.g;
import com.truecaller.util.dh;
import com.truecaller.util.r;
import com.truecaller.wizard.utils.i;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class o
  extends Fragment
  implements c, d.a, p
{
  private final AtomicBoolean a = new AtomicBoolean(false);
  protected Dialog j;
  public boolean k;
  
  public static void a(Context paramContext, String paramString)
  {
    paramContext.sendBroadcast(new Intent(paramString));
  }
  
  public boolean W_()
  {
    return false;
  }
  
  protected void a() {}
  
  public final void ac_()
  {
    b(2131886537);
  }
  
  public final void b(int paramInt)
  {
    Context localContext = getContext();
    if (localContext != null) {
      Toast.makeText(localContext, paramInt, 0).show();
    }
  }
  
  public final void c(String paramString)
  {
    try
    {
      Context localContext = getContext();
      if (localContext != null) {
        Toast.makeText(localContext, paramString, 0).show();
      }
      return;
    }
    finally {}
  }
  
  public final void d(String paramString)
  {
    dh.a(getActivity(), paramString, false);
  }
  
  public void d_(boolean paramBoolean)
  {
    if (isFinishing()) {
      return;
    }
    try
    {
      if (j == null) {
        j = new g(getActivity(), paramBoolean);
      }
      j.show();
      return;
    }
    catch (Exception localException)
    {
      d.a(localException, "TCActivity Exception while showing loading dialog");
    }
  }
  
  public void e()
  {
    if (isFinishing()) {
      return;
    }
    try
    {
      if (j != null) {
        j.dismiss();
      }
      return;
    }
    catch (Exception localException)
    {
      d.a(localException, "TCActivity Exception while dismissing loading dialog");
    }
  }
  
  protected final void e(String paramString)
  {
    r.a(getActivity(), paramString, null);
    Toast.makeText(getContext(), 2131887203, 0).show();
  }
  
  public boolean isFinishing()
  {
    return (getActivity() == null) || (getActivity().isFinishing());
  }
  
  protected final ActionBar k()
  {
    return ((n)getActivity()).getSupportActionBar();
  }
  
  protected final Toolbar l()
  {
    return getActivityh;
  }
  
  protected final View o()
  {
    if (getActivity() == null) {
      return null;
    }
    return getActivity().findViewById(16908290);
  }
  
  public void onAttach(Activity paramActivity)
  {
    k = ((a)paramActivity.getApplication()).p();
    super.onAttach(paramActivity);
    a.set(true);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    a();
  }
  
  public void onDetach()
  {
    super.onDetach();
    a.set(false);
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    i.a(paramArrayOfString, paramArrayOfInt);
  }
  
  public void onResume()
  {
    super.onResume();
    a locala = (a)getActivity().getApplication();
    k = locala.p();
    if ((locala.o()) && (!k))
    {
      TruecallerInit.b(getActivity(), "search", null);
      getActivity().finish();
      return;
    }
    p();
  }
  
  public void onStart()
  {
    super.onStart();
  }
  
  public void onStop()
  {
    e();
    j = null;
    super.onStop();
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    if ((!isAdded()) || (!a.get()))
    {
      TruecallerInit.b(getActivity(), "search", null);
      getActivity().finish();
    }
  }
  
  public void p() {}
  
  public void q() {}
  
  public final void r() {}
  
  public final boolean s()
  {
    return (getActivity() != null) && (!isDetached());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */