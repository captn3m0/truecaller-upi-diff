package com.truecaller.ui.dialogs;

import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.SearchView;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.libraries.nbu.engagementrewards.api.EngagementRewardsConstants.RewardsEnvironment;
import com.google.gson.q;
import com.inmobi.sdk.InMobiSdk;
import com.inmobi.sdk.InMobiSdk.LogLevel;
import com.truecaller.TrueApp;
import com.truecaller.ads.qa.QaCampaignsActivity;
import com.truecaller.ads.qa.QaKeywordsActivity;
import com.truecaller.aftercall.AfterCallPromotionActivity;
import com.truecaller.aftercall.PromotionType;
import com.truecaller.analytics.AppHeartBeatTask;
import com.truecaller.analytics.AppSettingsTask;
import com.truecaller.analytics.InstalledAppsHeartbeatWorker;
import com.truecaller.analytics.InstalledAppsHeartbeatWorker.a;
import com.truecaller.analytics.ae;
import com.truecaller.backup.BackupLogWorker;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.edge.EdgeLocationsWorker;
import com.truecaller.common.edge.EdgeLocationsWorker.a;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.tag.sync.AvailableTagsDownloadWorker;
import com.truecaller.common.tag.sync.TagKeywordsDownloadWorker;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.content.c.ad;
import com.truecaller.content.c.ag;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.Number;
import com.truecaller.engagementrewards.EngagementRewardActionType;
import com.truecaller.engagementrewards.EngagementRewardState;
import com.truecaller.feature_toggles.control_panel.FeaturesControlPanelActivity;
import com.truecaller.filters.FilterManager;
import com.truecaller.log.UnmutedException.e;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.data.types.Conversation.a;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.old.data.access.Settings;
import com.truecaller.old.data.access.Settings.BuildName;
import com.truecaller.old.data.entity.Notification.NotificationState;
import com.truecaller.presence.AvailabilityTrigger;
import com.truecaller.profile.BusinessProfileOnboardingActivity;
import com.truecaller.referral.ReferralManager;
import com.truecaller.referral.am;
import com.truecaller.referral.an;
import com.truecaller.remote_explorer.activities.PreferenceClientActivity;
import com.truecaller.remote_explorer.server.HttpServerService;
import com.truecaller.service.AlarmReceiver;
import com.truecaller.service.AlarmReceiver.AlarmType;
import com.truecaller.service.MissedCallsNotificationService;
import com.truecaller.service.RefreshContactIndexingService;
import com.truecaller.service.RefreshContactIndexingService.a;
import com.truecaller.service.RefreshT9MappingService;
import com.truecaller.service.SyncPhoneBookService;
import com.truecaller.ui.QaOtpListActivity;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.ui.components.ComboBase;
import com.truecaller.ui.components.n;
import com.truecaller.util.ai;
import com.truecaller.util.at;
import com.truecaller.util.bl;
import com.truecaller.util.bt;
import com.truecaller.util.co;
import java.io.File;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import kotlinx.coroutines.ao;

public final class l
  extends AppCompatDialogFragment
  implements View.OnClickListener
{
  private final com.truecaller.premium.data.m A;
  private final com.truecaller.androidactors.f<com.truecaller.config.a> B;
  private final com.truecaller.calling.a.e C;
  private final com.truecaller.androidactors.f<com.truecaller.presence.c> D;
  private final com.truecaller.engagementrewards.ui.d E;
  private final com.truecaller.filters.v a;
  private final FilterManager b;
  private final com.truecaller.data.entity.g c;
  private final com.truecaller.i.c d;
  private final com.truecaller.i.e e;
  private final com.truecaller.common.g.a f;
  private final com.truecaller.androidactors.f<t> g;
  private final com.truecaller.notificationchannels.e h;
  private final com.truecaller.notificationchannels.b i;
  private final com.truecaller.notifications.a j;
  private com.truecaller.aftercall.a k;
  private ReferralManager l;
  private final com.truecaller.f.a m;
  private final com.truecaller.flashsdk.core.b n;
  private final com.truecaller.common.f.c o;
  private final com.truecaller.common.edge.a p;
  private final com.truecaller.common.h.ac q;
  private final com.truecaller.whoviewedme.w r;
  private final com.truecaller.notificationchannels.p s;
  private final com.truecaller.ads.provider.f t;
  private final com.truecaller.i.a u;
  private final com.truecaller.messaging.h v;
  private final com.truecaller.utils.l w;
  private final com.truecaller.featuretoggles.e x;
  private final com.truecaller.clevertap.l y;
  private final bt z;
  
  public l()
  {
    bp localbp = TrueApp.y().a();
    com.truecaller.common.a locala = yb;
    a = localbp.Q();
    b = localbp.P();
    c = localbp.aa();
    d = localbp.D();
    e = localbp.F();
    f = localbp.I();
    k = localbp.Y();
    g = localbp.p();
    h = localbp.aC();
    i = localbp.aD();
    j = localbp.W();
    m = localbp.aS();
    n = localbp.aV();
    o = localbp.ai();
    p = locala.e();
    q = locala.n();
    r = localbp.bk();
    s = localbp.aB();
    t = localbp.aq();
    u = localbp.as();
    v = localbp.C();
    w = localbp.bw();
    x = localbp.aF();
    y = localbp.aM();
    z = localbp.bC();
    A = localbp.al();
    B = localbp.aZ();
    C = localbp.ct().a();
    D = localbp.ae();
    E = localbp.k();
  }
  
  private com.truecaller.callerid.i a(String paramString, int paramInt1, int paramInt2)
  {
    Contact localContact = new Contact();
    localContact.l("Sample contact");
    localContact.a(new Number(paramString));
    return new com.truecaller.callerid.i(paramInt1, paramInt2, c.b(new String[] { paramString }), 0, false, System.currentTimeMillis(), localContact, null, b.a(paramString));
  }
  
  private static String a()
  {
    try
    {
      Object localObject;
      int i1;
      do
      {
        do
        {
          Iterator localIterator1 = Collections.list(NetworkInterface.getNetworkInterfaces()).iterator();
          Iterator localIterator2;
          while (!localIterator2.hasNext())
          {
            if (!localIterator1.hasNext()) {
              break;
            }
            localIterator2 = Collections.list(((NetworkInterface)localIterator1.next()).getInetAddresses()).iterator();
          }
          localObject = (InetAddress)localIterator2.next();
        } while (((InetAddress)localObject).isLoopbackAddress());
        localObject = ((InetAddress)localObject).getHostAddress();
        i1 = ((String)localObject).indexOf(':');
        if (i1 < 0) {
          i1 = 1;
        } else {
          i1 = 0;
        }
      } while (i1 == 0);
      return (String)localObject;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    return "";
  }
  
  private static void a(boolean paramBoolean)
  {
    an localan = new an();
    int i1 = 0;
    while (i1 < 13)
    {
      localan.a(new String[] { "featureReferralNavigationDrawer", "featureReferralDeeplink", "featureSearchBarIcon", "featureInboxOverflow", "featureContactDetail", "featureContacts", "featureUserBusyPrompt", "featureAftercall", "featureAftercallSaveContact", "featureGoPro", "featurePushNotification", "featureReferralDeeplink", "featureReferralAfterCallPromo" }[i1], paramBoolean);
      i1 += 1;
    }
  }
  
  private void b()
  {
    d.d("lastCallMadeWithTcTime");
    d.d("lastDialerPromotionTime");
  }
  
  @SuppressLint({"PrivateApi"})
  private void c()
  {
    for (;;)
    {
      int i1;
      try
      {
        Object localObject2 = getContext().getDatabasePath("test.db").getParentFile();
        Object localObject1 = Class.forName("android.os.FileUtils").getMethod("setPermissions", new Class[] { String.class, Integer.TYPE, Integer.TYPE, Integer.TYPE });
        TreeSet localTreeSet = new TreeSet();
        localObject2 = ((File)localObject2).listFiles();
        int i2 = localObject2.length;
        i1 = 0;
        if (i1 < i2)
        {
          Object localObject3 = localObject2[i1];
          try
          {
            if (!((File)localObject3).getName().endsWith(".db")) {
              break label239;
            }
            ((Method)localObject1).invoke(null, new Object[] { ((File)localObject3).getAbsolutePath(), Integer.valueOf(420), Integer.valueOf(-1), Integer.valueOf(-1) });
            localTreeSet.add(((File)localObject3).getAbsolutePath());
            "File permissions changed for ".concat(String.valueOf(localObject3));
          }
          catch (Throwable localThrowable2)
          {
            localThrowable2.printStackTrace();
          }
        }
        else
        {
          if (!localTreeSet.isEmpty())
          {
            localObject1 = getContext();
            localObject2 = new StringBuilder("Permissions changed for: ");
            ((StringBuilder)localObject2).append(TextUtils.join(",", localTreeSet));
            Toast.makeText((Context)localObject1, ((StringBuilder)localObject2).toString(), 1).show();
          }
          return;
        }
      }
      catch (Throwable localThrowable1)
      {
        localThrowable1.printStackTrace();
        return;
      }
      label239:
      i1 += 1;
    }
  }
  
  private void d()
  {
    AlarmReceiver.AlarmType[] arrayOfAlarmType = AlarmReceiver.AlarmType.values();
    int i2 = arrayOfAlarmType.length;
    int i1 = 0;
    while (i1 < i2)
    {
      Settings.d(arrayOfAlarmType[i1].name(), 0L);
      i1 += 1;
    }
    AlarmReceiver.a(getContext(), false);
  }
  
  private void e()
  {
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    if (!w.c())
    {
      requestPermissions(new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" }, 1);
      return;
    }
    Toast.makeText(localContext, "App is exporting DB Schema...", 0).show();
    ai.a(localContext).a_(new -..Lambda.l.RXOpnNj1_UpFWeBtcoixF1oEjqE(localContext));
  }
  
  private void f()
  {
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    if (!w.c())
    {
      requestPermissions(new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" }, 2);
      return;
    }
    Toast.makeText(localContext, "App is exporting logs...", 0).show();
    com.truecaller.debug.log.a.a(localContext).a_(new -..Lambda.l.4uNUEC9kKe3ZbMNfktnEgZ2a3_g(localContext));
  }
  
  private void g()
  {
    if (Build.VERSION.SDK_INT < 26)
    {
      Toast.makeText(getContext(), "Method tracing requires Android O or above", 1).show();
      return;
    }
    Toast.makeText(getContext(), "Method tracing requires a debuggable build", 1).show();
  }
  
  private void h()
  {
    if (!w.c())
    {
      requestPermissions(new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" }, 0);
      return;
    }
    Toast.makeText(getContext(), "App is dumping its heap. It will cause UI freeze.", 0).show();
    getView().postDelayed(new -..Lambda.l.Uk7KctX9iZGGfPfgfY6ovWtmN0A(this), 100L);
  }
  
  @SuppressLint({"MissingPermission"})
  public final void onClick(View paramView)
  {
    Object localObject1 = TrueApp.y().a();
    Object localObject2 = com.truecaller.common.b.a.F();
    int i3 = paramView.getId();
    int i1 = 0;
    int i2 = 0;
    Object localObject3;
    switch (i3)
    {
    default: 
      switch (i3)
      {
      default: 
        switch (i3)
        {
        default: 
          switch (i3)
          {
          default: 
            switch (i3)
            {
            default: 
              switch (i3)
              {
              default: 
                switch (i3)
                {
                default: 
                  switch (i3)
                  {
                  default: 
                    switch (i3)
                    {
                    default: 
                      switch (i3)
                      {
                      default: 
                        switch (i3)
                        {
                        default: 
                          switch (i3)
                          {
                          default: 
                            switch (i3)
                            {
                            default: 
                              switch (i3)
                              {
                              default: 
                                switch (i3)
                                {
                                default: 
                                  return;
                                case 2131363838: 
                                  TruecallerInit.a(getActivity(), "banking", "banking");
                                  return;
                                case 2131363071: 
                                  c.b(10009);
                                  return;
                                case 2131363054: 
                                  startActivity(new Intent(getContext(), FeaturesControlPanelActivity.class));
                                  return;
                                case 2131363014: 
                                  e();
                                  return;
                                case 2131362731: 
                                  paramView = LayoutInflater.from(getContext()).inflate(2131559077, null);
                                  localObject1 = (EditText)paramView.findViewById(2131362754);
                                  ((EditText)localObject1).setText(Settings.b("qa_voip_notification_rtm_token"));
                                  new AlertDialog.Builder(getContext()).setTitle("New RTM token").setView(paramView).setPositiveButton(2131887217, new -..Lambda.l.o1-9hr6nBsWnb7crVLy06DTUzsw(this, (EditText)localObject1)).setNegativeButton(2131887197, null).setNeutralButton("Clear", new -..Lambda.l._MDFmz1ryJGgriIMIPRr2Q37txY(this)).show();
                                  return;
                                case 2131362701: 
                                  ((bp)localObject1).j().a(EngagementRewardActionType.BUY_PREMIUM_ANNUAL, EngagementRewardState.COMPLETED);
                                  ((bp)localObject1).k().a();
                                  return;
                                case 2131362698: 
                                  m.f("KeyCallLogPromoDisabledUntil");
                                  a(true);
                                  return;
                                case 2131362696: 
                                  if (getContext() != null)
                                  {
                                    if (Settings.b("qaEnableHttpServer", false))
                                    {
                                      HttpServerService.a(getContext());
                                      return;
                                    }
                                    getContext().stopService(new Intent(getContext(), HttpServerService.class));
                                    return;
                                  }
                                  break;
                                case 2131362686: 
                                  a(false);
                                  return;
                                }
                                break;
                              case 2131362791: 
                                e.b("whatsNewDialogShownRevision", 0);
                                e.b("mdauPromoShownTimes", 0);
                                e.b("mdauPromoShownTimestamp", 0L);
                                e.d("whatsNewShownTimestamp");
                                return;
                              case 2131362790: 
                                getActivity().getSupportFragmentManager().a().a(new com.truecaller.startup_dialogs.fragments.l(), com.truecaller.startup_dialogs.fragments.l.class.getSimpleName()).f();
                                return;
                              case 2131362789: 
                                getActivity().getSupportFragmentManager().a().a(new com.truecaller.startup_dialogs.fragments.g(), com.truecaller.startup_dialogs.fragments.g.class.getSimpleName()).d();
                                return;
                              case 2131362788: 
                                new com.truecaller.old.data.access.i(getContext()).b();
                                c.b(10015);
                                return;
                              case 2131362787: 
                                paramView = com.truecaller.common.network.util.d.c();
                                ((ae)((bk)getActivity().getApplication()).a().f().a()).a(paramView).c();
                                return;
                              case 2131362786: 
                                c.b(10015);
                                return;
                              case 2131362785: 
                                com.truecaller.common.b.e.b("tagsPhonebookForcedUpload", true);
                                c.b(10015);
                                return;
                              case 2131362784: 
                                paramView = new Intent("com.google.android.gms.gcm.ACTION_TASK_READY");
                                paramView.setPackage(getContext().getPackageName());
                                localObject1 = getContext().getPackageManager().queryIntentServices(paramView, 0);
                                if ((localObject1 != null) && (!((List)localObject1).isEmpty()))
                                {
                                  paramView = new ArrayList();
                                  localObject1 = ((List)localObject1).iterator();
                                  i1 = i2;
                                  while (((Iterator)localObject1).hasNext())
                                  {
                                    localObject2 = (ResolveInfo)((Iterator)localObject1).next();
                                    localObject3 = new Intent("com.google.android.gms.gcm.SERVICE_ACTION_INITIALIZE");
                                    ((Intent)localObject3).setClassName(serviceInfo.packageName, serviceInfo.name);
                                    localObject2 = getContext().startService((Intent)localObject3);
                                    if (localObject2 == null) {
                                      i1 += 1;
                                    } else {
                                      paramView.add(localObject2);
                                    }
                                  }
                                  localObject1 = getContext();
                                  localObject2 = new StringBuilder("Started ");
                                  ((StringBuilder)localObject2).append(paramView);
                                  ((StringBuilder)localObject2).append(", failed to start ");
                                  ((StringBuilder)localObject2).append(i1);
                                  ((StringBuilder)localObject2).append(" services");
                                  Toast.makeText((Context)localObject1, ((StringBuilder)localObject2).toString(), 1).show();
                                }
                                return;
                              case 2131362783: 
                                paramView = EdgeLocationsWorker.f.a().b();
                                androidx.work.p.a().a(paramView);
                                return;
                              case 2131362782: 
                                new AlertDialog.Builder(getContext()).setTitle("Enter background task ID").setView(2131559075).setPositiveButton(2131887217, new -..Lambda.l.bWOt0585QvJ8i-EstHzi2R3Kovg(this)).setNegativeButton(2131887197, null).show();
                                return;
                              case 2131362781: 
                                AvailableTagsDownloadWorker.e();
                                return;
                              case 2131362780: 
                                TagKeywordsDownloadWorker.e();
                                return;
                              case 2131362779: 
                                ((t)g.a()).a(true);
                                return;
                              }
                              break;
                            case 2131362775: 
                              new l.c(this, new String[] { "0731256247", "0761840301", "+911244130150" }, (byte)0).a();
                              return;
                            case 2131362774: 
                              paramView = TrueApp.y().a().M();
                              localObject1 = new Message.a().a(Entity.a("text/plain", "Your otp is 767676"));
                              c = Participant.b("46763185096", TrueApp.y().a().V(), "-1");
                              localObject1 = ((Message.a)localObject1).b();
                              localObject2 = new Conversation.a();
                              localObject3 = c;
                              l.add(localObject3);
                              localObject2 = ((Conversation.a)localObject2).a();
                              ((com.truecaller.messaging.notifications.a)paramView.a()).a(Collections.singletonMap(localObject2, Collections.singletonList(localObject1)));
                              return;
                            case 2131362773: 
                              paramView = new com.truecaller.callerid.g(getContext(), d, h, i, j);
                              if (Math.random() > 0.5D) {
                                i1 = 1;
                              } else {
                                i1 = 3;
                              }
                              paramView.a(a("+123456789", 3, i1));
                              if (Math.random() > 0.5D)
                              {
                                if (Math.random() > 0.5D) {
                                  i1 = 1;
                                } else {
                                  i1 = 3;
                                }
                                paramView.a(a("+198765432", 3, i1));
                              }
                              break;
                            }
                            break;
                          }
                          break;
                        }
                        break;
                      }
                      break;
                    }
                    break;
                  }
                  break;
                }
                break;
              }
              break;
            }
            break;
          }
          break;
        }
        break;
      }
      break;
    }
    try
    {
      paramView = new com.truecaller.old.data.entity.Notification(q.a("{\n   \"e\": {\"s\":2,\"c\":1443107255,\"t\":2,\"i\":391912021},\n   \"a\": {\"v\":\"10.00\",\"u\":\"http://truecaller.com\"}\n }").i(), Notification.NotificationState.NEW);
      bl.a(getContext(), paramView);
      paramView = new ContentValues();
      paramView.put("new", Integer.valueOf(1));
      paramView.put("is_read", Integer.valueOf(0));
      getContext().getContentResolver().update(TruecallerContract.n.a(), paramView, "_id= (SELECT MAX(_id) FROM history WHERE type=3)", null);
      MissedCallsNotificationService.a(getContext());
      bl.a(getContext(), 4, false, "qa");
      paramView = AlarmReceiver.AlarmType.TYPE_NOTIFICATION_ACCESS.getNotification(getContext());
      if (paramView != null)
      {
        flags &= 0xFFFFFFFD;
        android.support.v4.app.ac.a(getContext()).a(null, AlarmReceiver.AlarmType.TYPE_NOTIFICATION_ACCESS.getNotificationId(), paramView);
      }
      paramView = AlarmReceiver.AlarmType.TYPE_UPDATE_SPAM.getNotification(getContext());
      if (paramView != null)
      {
        android.support.v4.app.ac.a(getContext()).a(null, AlarmReceiver.AlarmType.TYPE_UPDATE_SPAM.getNotificationId(), paramView);
        return;
        b();
        localObject1 = a("0000000000", 0, 0);
        localObject2 = com.truecaller.callerid.j.b((com.truecaller.callerid.i)localObject1);
        localObject1 = k.a((com.truecaller.callerid.i)localObject1, (HistoryEvent)localObject2);
        localObject3 = paramView.getContext();
        com.truecaller.i.c localc = d;
        if (localObject1 != null) {
          paramView = (View)localObject1;
        } else {
          paramView = PromotionType.SIGN_UP;
        }
        AfterCallPromotionActivity.a((Context)localObject3, localc, paramView, (HistoryEvent)localObject2);
        return;
        d.b("afterCallPromoteContactsPermissionTimestamp", 0L);
        AfterCallPromotionActivity.a(getContext(), PromotionType.CONTACT_PERMISSION);
        return;
        localObject1 = com.truecaller.common.b.e.a("forcedUpdate_updateType");
        paramView = (View)localObject1;
        if (TextUtils.isEmpty((CharSequence)localObject1)) {
          paramView = "NO_UPDATE";
        }
        paramView = new AlertDialog.Builder(getContext()).setTitle("Current: ".concat(String.valueOf(paramView)));
        localObject1 = -..Lambda.l.UhErUcYo-yuXJCrxlboUx7PiN_A.INSTANCE;
        paramView.setItems(new String[] { "No update", "Optional update", "Required update", "Version discontinued" }, (DialogInterface.OnClickListener)localObject1).show();
        return;
        com.truecaller.common.h.o.a((Context)localObject2, com.truecaller.common.h.o.a((Context)localObject2));
        return;
        paramView = ThemeManager.Theme.values();
        localObject1 = new String[paramView.length];
        localObject2 = ThemeManager.a();
        i2 = 0;
        while (i1 < paramView.length)
        {
          localObject1[i1] = getContext().getString(displayName);
          if (paramView[i1] == localObject2) {
            i2 = i1;
          }
          i1 += 1;
        }
        new AlertDialog.Builder(getContext()).setSingleChoiceItems((CharSequence[])localObject1, i2, new -..Lambda.l.S7JmBLJ3ff6IUkTnAL-C8u5vHWo(this, paramView)).show();
        return;
        Settings.a("madeCallsFromCallLog", false);
        return;
        if ((TrueApp.y().E()) && (getContext() != null))
        {
          TrueApp.y().a().bO().a(getContext());
          return;
          c.b(20001);
          return;
          if ((TrueApp.y().E()) && (getContext() != null))
          {
            TrueApp.y().a().bO().a();
            return;
            com.truecaller.truepay.app.fcm.a.a(getContext());
            return;
            c.b(20003);
            return;
            ((com.truecaller.config.a)B.a()).b().c();
            return;
            if (TrueApp.y().E())
            {
              TrueApp.y().a().bO().b();
              return;
              if ((TrueApp.y().E()) && (getContext() != null))
              {
                TrueApp.y().a().bO().b(getContext());
                return;
                c.b(20004);
                return;
                e.b("backupOnboardingAvailable", true);
                e.b("backupOnboardingShown", false);
                f.b("key_backup_fetched_timestamp", System.currentTimeMillis());
                return;
                f.d("featureRegion1_qa");
                at.c(getView(), 2131362778, q.a());
                f.d("featureRegion1_qa");
                break label2702;
                if (co.a(getContext()))
                {
                  paramView = TrueApp.y().a();
                  ((com.truecaller.callhistory.a)paramView.ad().a()).a();
                  SyncPhoneBookService.a(getContext());
                  ((t)paramView.p().a()).a(true);
                  i1 = 1;
                }
                else
                {
                  i1 = 0;
                }
                com.truecaller.ads.campaigns.f.a(getContext()).b();
                if (i1 != 0)
                {
                  Toast.makeText(getContext(), "Provider has been reset, syncing call log and phone book", 0).show();
                  return;
                }
                Toast.makeText(getContext(), "Could not reset provider", 1).show();
                return;
                n.j();
                return;
                b();
                Toast.makeText(getContext(), "Dialer promotions reset", 0).show();
                return;
                C.a();
                Toast.makeText(getContext(), "Contacts settings reseted", 1).show();
                return;
                label2702:
                f.d("core_agreed_region_1");
                return;
                d();
                return;
                new l.b(getActivity()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
                return;
                f.b("key_last_set_status_time", 0L);
                f.a("last_availability_update_success", null);
                ((com.truecaller.presence.c)D.a()).a(AvailabilityTrigger.USER_ACTION, true);
                return;
                Settings.f("initialCallLogSyncComplete");
                return;
                WebView.setWebContentsDebuggingEnabled(true);
                Toast.makeText(getContext(), "Remote WebView debugging enabled", 0).show();
                return;
                if (getContext() != null)
                {
                  paramView = new AlertDialog.Builder(getContext()).setTitle("How to edit preferences remotely ?");
                  localObject1 = new StringBuilder("Connect your phone and your laptop on the same wifi.\nEnter 'http://");
                  ((StringBuilder)localObject1).append(a());
                  ((StringBuilder)localObject1).append(":8080/' in your browser search bar.\nClick on the connect button in your browser.\nAccept the connection on your phone.");
                  paramView.setMessage(((StringBuilder)localObject1).toString()).setPositiveButton("OK", null).create().show();
                  break label3657;
                  RefreshT9MappingService.a(paramView.getContext());
                  return;
                  s.c();
                  Toast.makeText(getContext(), "Notification channels are recreated", 0).show();
                  return;
                  paramView = TrueApp.y().a().c();
                  paramView = ag.a((Context)localObject2, ag.b(), paramView).getWritableDatabase();
                  new ad().a(paramView);
                  return;
                  paramView = new RefreshContactIndexingService.a(paramView.getContext());
                  localObject1 = new Intent(a, RefreshContactIndexingService.class).setAction("RefreshContactIndexingService.action.sync").putExtra("RefreshContactIndexingService.extra.rebuild_all", true);
                  android.support.v4.app.v.a(a, RefreshContactIndexingService.class, 2131364100, (Intent)localObject1);
                  return;
                  c();
                  return;
                  c.b(10024);
                  return;
                  AsyncTask.execute(-..Lambda.l.k6EOQyAa571SG-2NmskBR3aHxd4.INSTANCE);
                  return;
                  startActivity(PreferenceClientActivity.a(getContext()));
                  return;
                  TrueApp.y().a().B().a((Context)localObject2, 2131888934);
                  return;
                  com.truecaller.wizard.utils.i.a(getActivity());
                  return;
                  startActivity(new Intent(getContext(), QaOtpListActivity.class));
                  return;
                  i1 = ((TelephonyManager)getContext().getSystemService("phone")).getCallState();
                  paramView = "";
                  switch (i1)
                  {
                  default: 
                    break;
                  case 2: 
                    paramView = "Offhook";
                    break;
                  case 1: 
                    paramView = "Ringing";
                    break;
                  case 0: 
                    paramView = "Idle";
                  }
                  Toast.makeText(getContext(), "Current native call state is: ".concat(String.valueOf(paramView)), 0).show();
                  return;
                  new m().show(getFragmentManager(), "qa_mock_im");
                  return;
                  g();
                  return;
                  new AlertDialog.Builder(getContext()).setTitle("Enter LeadGen ID").setView(2131559075).setPositiveButton(2131887217, new -..Lambda.l.Ba3y2NT9lE0MuQrLRj0OTYHVldU(this)).setNegativeButton(2131887197, null).show();
                  return;
                  QaKeywordsActivity.a(getActivity());
                  return;
                  androidx.work.p.a().a(InstalledAppsHeartbeatWorker.f.a().b());
                  return;
                  paramView = ((bk)getActivity().getApplicationContext()).a().aA();
                  paramView.a(2);
                  paramView.a(1);
                  paramView.a(0);
                  return;
                  AppSettingsTask.a(c);
                  AppHeartBeatTask.c(c);
                  return;
                  f();
                  return;
                  u.b("adsFeatureHouseAdsTimeout", 1L);
                  return;
                  f.b("featureAdCtpRotation", true);
                  Toast.makeText(getContext(), "Rotation forced until feature flag sync", 0).show();
                  return;
                  startActivity(n.a(getContext(), null, null, null, null, false, null));
                  return;
                  getActivity().getSupportFragmentManager().a().a(new com.truecaller.startup_dialogs.fragments.d(), com.truecaller.startup_dialogs.fragments.d.class.getSimpleName()).d();
                  return;
                  getContext().getContentResolver().call(TruecallerContract.b, "dump", null, null);
                  return;
                  h();
                  return;
                  f.b("core_enhancedSearchReported", false);
                  return;
                  t.d();
                  return;
                  throw new UnmutedException.e(1, 0);
                  "".substring(0, 1);
                  return;
                  paramView = paramView.getContext();
                  localObject1 = AccountManager.get(paramView);
                  localObject2 = paramView.getString(2131887458);
                  localObject3 = ((AccountManager)localObject1).getAccountsByType((String)localObject2);
                  if (localObject3.length == 0)
                  {
                    Toast.makeText(paramView, "System account does not exist", 0).show();
                    return;
                  }
                  switch (org.c.a.a.a.j.a())
                  {
                  default: 
                    return;
                  case 1: 
                    ((AccountManager)localObject1).setUserData(localObject3[0], "countryCode", null);
                    Toast.makeText(paramView, "country code has been removed", 0).show();
                    return;
                  }
                  ((AccountManager)localObject1).invalidateAuthToken((String)localObject2, ((AccountManager)localObject1).peekAuthToken(localObject3[0], "installation_id"));
                  Toast.makeText(paramView, "installation ID has been removed", 0).show();
                  return;
                }
                label3657:
                y.a();
                return;
                paramView = l;
                if (paramView != null)
                {
                  paramView.e();
                  return;
                  r.g();
                  return;
                  p.c();
                  Toast.makeText(getContext(), "Edge locations cleared", 0).show();
                  return;
                  getContext().getSharedPreferences("callMeBackNotifications", 0).edit().clear().apply();
                  return;
                  c.b(10032);
                  return;
                  QaCampaignsActivity.a(getActivity());
                  return;
                  paramView = ((bp)localObject1).ao();
                  localObject2 = ((bp)localObject1).bx();
                  localObject3 = new StringBuilder("Device: ");
                  ((StringBuilder)localObject3).append(((com.truecaller.utils.d)localObject2).l());
                  ((StringBuilder)localObject3).append("\nManufacturer: ");
                  ((StringBuilder)localObject3).append(((com.truecaller.utils.d)localObject2).m());
                  ((StringBuilder)localObject3).append("\nAndroid version: ");
                  ((StringBuilder)localObject3).append(Build.VERSION.SDK_INT);
                  ((StringBuilder)localObject3).append("\nDevice blacklist: ");
                  ((StringBuilder)localObject3).append(paramView.a(x.ak().d()));
                  ((StringBuilder)localObject3).append("\nDevice blacklist regex: ");
                  ((StringBuilder)localObject3).append(paramView.a(x.am().d()));
                  ((StringBuilder)localObject3).append("\nManufacturer blacklist: ");
                  ((StringBuilder)localObject3).append(paramView.a(x.al().d()));
                  ((StringBuilder)localObject3).append("\nSupported Android version: >=21\n\nFeature supported: ");
                  ((StringBuilder)localObject3).append(((bp)localObject1).bh().a());
                  ((StringBuilder)localObject3).append("\n\n");
                  ((StringBuilder)localObject3).append(((bp)localObject1).bh().toString());
                  paramView = ((StringBuilder)localObject3).toString();
                  if (getContext() != null)
                  {
                    new AlertDialog.Builder(getContext()).setMessage(paramView).show();
                    return;
                  }
                }
              }
            }
          }
        }
      }
      return;
      new l.2(this, new String[] { "0735342770" }).a();
      return;
      startActivity(BusinessProfileOnboardingActivity.a(getActivity(), false));
      return;
      e.b("backupOnboardingAvailable", true);
      e.b("backupOnboardingShown", false);
      f.b("key_backup_fetched_timestamp", 0L);
      return;
      BackupLogWorker.e();
      return;
      InMobiSdk.setLogLevel(InMobiSdk.LogLevel.DEBUG);
      return;
      new AlertDialog.Builder(getContext()).setTitle("Add top spammer").setView(2131559076).setPositiveButton(2131887235, new -..Lambda.l.WJ7UBcBuN6zwRLFDjX5l-_rgpn4(this)).setNegativeButton(2131887197, null).show();
      return;
      new AlertDialog.Builder(getContext()).setTitle("Add edge end-point").setView(2131559074).setPositiveButton(2131887217, new -..Lambda.l._yvCF_mjlrjIxe-re9dbyGeSAj8(this)).setNegativeButton(2131887197, null).show();
      return;
      new com.truecaller.abtest.b().show(getFragmentManager(), "");
      return;
      b.d();
      return;
      Settings.f("premiumTimestamp");
      Settings.f("premiumRenewable");
      Settings.f("premiumGraceExpiration");
      Settings.f("premiumLevel");
      f.d("premiumDuration");
      f.d("premiumLastFetchDate");
      o.a(0);
      A.a();
      return;
      f.d("profileLastName");
      return;
      f.d("profileFirstName");
      return;
    }
    catch (Exception paramView)
    {
      for (;;) {}
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    l = com.truecaller.referral.w.a(this, "ReferralManagerImpl");
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return paramLayoutInflater.cloneInContext(new ContextThemeWrapper(paramLayoutInflater.getContext(), aresId)).inflate(2131558584, paramViewGroup, false);
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    if (paramInt == 0)
    {
      if (paramArrayOfInt[0] == 0)
      {
        h();
        return;
      }
      Toast.makeText(getContext(), "We need media storage access for saving heap info file. Please try again and grant permission when android will ask about it", 1).show();
      return;
    }
    if (paramInt == 1)
    {
      if (paramArrayOfInt[0] == 0)
      {
        e();
        return;
      }
      Toast.makeText(getContext(), "We need media storage access for exporting DB Schema. Please try again and grant permission when android will ask about it", 1).show();
      return;
    }
    if (paramInt == 2)
    {
      if (paramArrayOfInt[0] == 0)
      {
        f();
        return;
      }
      Toast.makeText(getContext(), "We need media storage access for exporting logs. Please try again and grant permission when android will ask about it", 1).show();
      return;
    }
    if (paramInt == 3)
    {
      if (paramArrayOfInt[0] == 0)
      {
        g();
        return;
      }
      Toast.makeText(getContext(), "We need media storage access for method tracing. Please try again and grant permission when android will ask about it", 1).show();
    }
  }
  
  @SuppressLint({"SetTextI18n"})
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    paramBundle = new ArrayList();
    Object localObject1 = Settings.BuildName.values();
    int i2 = localObject1.length;
    int i1 = 0;
    while (i1 < i2)
    {
      localObject2 = localObject1[i1];
      paramBundle.add(new n(((Settings.BuildName)localObject2).name(), "", ((Settings.BuildName)localObject2).name()));
      i1 += 1;
    }
    at.a(paramView, 2131362732, paramBundle, "BUILD_KEY").a(-..Lambda.l.WDZeFYiq-gYI2PLNzAkDXhUWmbM.INSTANCE);
    paramBundle = new ArrayList();
    localObject1 = EngagementRewardState.values();
    i2 = localObject1.length;
    i1 = 0;
    while (i1 < i2)
    {
      localObject2 = localObject1[i1];
      paramBundle.add(new n(((EngagementRewardState)localObject2).name(), "", ((EngagementRewardState)localObject2).name()));
      i1 += 1;
    }
    at.a(paramView, 2131362703, paramBundle, "qa_engagement_reward_state").a(-..Lambda.l.x54uZRTInzYOLsjSe1_f69OwX5g.INSTANCE);
    paramBundle = new com.truecaller.engagementrewards.ui.g(getContext(), getFragmentManager(), E);
    localObject1 = new ArrayList();
    Object localObject2 = a.keySet().iterator();
    while (((Iterator)localObject2).hasNext())
    {
      str = (String)((Iterator)localObject2).next();
      ((List)localObject1).add(new n(str, "", str));
    }
    localObject2 = at.h(paramView, 2131362702);
    ((ComboBase)localObject2).setData((List)localObject1);
    ((ComboBase)localObject2).a(new -..Lambda.l._451E4Kd0OpqmwrXiOzarjsexac(paramBundle));
    paramBundle = new ArrayList();
    paramBundle.add(new n(EngagementRewardsConstants.RewardsEnvironment.PROD_ENVIRONMENT.name(), "", EngagementRewardsConstants.RewardsEnvironment.PROD_ENVIRONMENT));
    paramBundle.add(new n(EngagementRewardsConstants.RewardsEnvironment.STAGING_ENVIRONMENT.name(), "", EngagementRewardsConstants.RewardsEnvironment.STAGING_ENVIRONMENT));
    at.a(paramView, 2131362700, paramBundle, "qaEngagementRewardEnv").a(-..Lambda.l.8O0EhynwNAWI_xEzZh8SMWUYduc.INSTANCE);
    at.a(paramView, 2131362707, Settings.e("qaForceAds"), -..Lambda.l.4GlBxExP6MMmv9v2EozZqiT9Ars.INSTANCE);
    at.a(paramView, 2131362685, u.b("adsQaDisableRequests"), new -..Lambda.l._TqKXfgaVd5kVPCOWvu-WSYHlMc(this));
    at.a(paramView, 2131362768, Settings.e("qaServer"), -..Lambda.l.P-uXlQNHUz3l9w9b34gBx8XeBR8.INSTANCE);
    at.a(paramView, 2131362778, q.a(), new -..Lambda.l.tBuYrs9xmoonpuhnIhfdY4eJT7c(this));
    at.a(paramView, 2131362704, Settings.e("qaReferralFakeSendSms"), -..Lambda.l.s4YmF-HLlg5xElTfp3X4dKyoTSg.INSTANCE);
    at.a(paramView, 2131362710, Settings.e("qaEnableLogging"), -..Lambda.l.5Q-pE0UuGJworg7jr3x6iPz3eWQ.INSTANCE);
    at.a(paramView, 2131362711, Settings.e("qaForceShowReferral"), -..Lambda.l.5KYvnD8X0QMxj44rG4OpE9k_FLM.INSTANCE);
    at.a(paramView, 2131362695, Settings.e("qaEnableDomainFronting"), -..Lambda.l.rKQ9E16r1qV_NGT_aJM66vXyMuI.INSTANCE);
    at.a(paramView, 2131362696, Settings.e("qaEnableHttpServer"), -..Lambda.l.1AOC0BIhx2c-Lu1dW_e4ZqDlShQ.INSTANCE);
    at.a(paramView, 2131362669, f.b("backupForceRootFolder"), new -..Lambda.l.K1AiKlRzdZVx3X5XpeJ6afFZW9g(this));
    at.a(paramView, 2131362793, f.b("whoViewedMePBContactEnabled"), new -..Lambda.l.sMz-0iMoSZChlVb4nJGwnUdiWqs(this));
    at.a(paramView, 2131362792, f.b("whoViewedMeACSEnabled"), new -..Lambda.l.xCkFe4ER4dLKVTEq4LUu6SZopls(this));
    at.a(paramView, 2131362694, f.b("featureCleverTap"), new -..Lambda.l.F9bh7NsYSt64SnJ9hrXtOKixc-A(this));
    at.a(paramView, 2131362716, z.c(), new -..Lambda.l.vNuGhakMUJiO6LfYDM_LsCcsbuw(this));
    at.a(paramView, 2131362714, z.d(), new -..Lambda.l.Pue1mB6Nzs2yl0oa4reTG4_wN3Q(this));
    at.a(paramView, 2131362717, z.e(), new -..Lambda.l.DLCEsP32SDJ5JqGGTTyvyIXFJzk(this));
    at.a(paramView, 2131362715, "messenger-dev-se1.truecaller.com".equals(p.a("eu", KnownEndpoints.MESSENGER.getKey())), new -..Lambda.l.4zuRlfXRpOILUlHWSlNTg-CnpR0(this));
    at.a(paramView, 2131362697, Settings.e("qaEnableRecorderLeak"), -..Lambda.l.89qNr2PHVT5lMQOOwOl1KSGLxN8.INSTANCE);
    at.a(paramView, 2131362699, Settings.e("qaEnableRewardForMonthlySubs"), -..Lambda.l.oeaoYaOvfijSO2aR-oxZ8cHZeok.INSTANCE);
    at.a(paramView, 2131362788, this);
    at.a(paramView, 2131362786, this);
    at.a(paramView, 2131362785, this);
    at.a(paramView, 2131362758, this);
    at.a(paramView, 2131362762, this);
    at.a(paramView, 2131362759, this);
    at.a(paramView, 2131362755, this);
    at.a(paramView, 2131362760, this);
    at.a(paramView, 2131362756, this);
    at.a(paramView, 2131362763, this);
    at.a(paramView, 2131362761, this);
    at.a(paramView, 2131362757, this);
    at.a(paramView, 2131362683, this);
    at.a(paramView, 2131362684, this);
    at.a(paramView, 2131362745, this);
    at.a(paramView, 2131362748, this);
    at.a(paramView, 2131362782, this);
    at.a(paramView, 2131362784, this);
    at.a(paramView, 2131362675, this);
    at.a(paramView, 2131362722, this);
    at.a(paramView, 2131362723, this);
    at.a(paramView, 2131362742, this);
    at.a(paramView, 2131362708, this);
    at.a(paramView, 2131362688, this);
    at.a(paramView, 2131362709, this);
    at.a(paramView, 2131362668, this);
    at.a(paramView, 2131363014, this);
    at.a(paramView, 2131362751, this);
    at.a(paramView, 2131362691, this);
    at.a(paramView, 2131362736, this);
    at.a(paramView, 2131362746, this);
    at.a(paramView, 2131362666, this);
    at.a(paramView, 2131362678, this);
    at.a(paramView, 2131362783, this);
    at.a(paramView, 2131362743, this);
    at.a(paramView, 2131362689, this);
    at.a(paramView, 2131362667, this);
    at.a(paramView, 2131362498, this);
    at.a(paramView, 2131362787, this);
    at.a(paramView, 2131362781, this);
    at.a(paramView, 2131362780, this);
    at.a(paramView, 2131362670, this);
    at.a(paramView, 2131362740, this);
    at.a(paramView, 2131362737, this);
    at.a(paramView, 2131362790, this);
    at.a(paramView, 2131362791, this);
    at.a(paramView, 2131362671, this);
    at.a(paramView, 2131362753, this);
    at.a(paramView, 2131362789, this);
    at.a(paramView, 2131362495, this);
    at.a(paramView, 2131362496, this);
    at.a(paramView, 2131362705, this);
    at.a(paramView, 2131362773, this);
    at.a(paramView, 2131362735, this);
    at.a(paramView, 2131362734, this);
    at.a(paramView, 2131362771, this);
    at.a(paramView, 2131362682, this);
    at.a(paramView, 2131362749, this);
    at.a(paramView, 2131362772, this);
    at.a(paramView, 2131362770, this);
    at.a(paramView, 2131362713, this);
    at.a(paramView, 2131362720, this);
    at.a(paramView, 2131362766, this);
    at.a(paramView, 2131362719, this);
    at.a(paramView, 2131362712, this);
    at.a(paramView, 2131362724, this);
    at.a(paramView, 2131362744, this);
    at.a(paramView, 2131362752, this);
    at.a(paramView, 2131362747, this);
    at.a(paramView, 2131362779, this);
    at.a(paramView, 2131362738, this);
    at.a(paramView, 2131362767, this);
    at.a(paramView, 2131362672, this);
    at.a(paramView, 2131362775, this);
    at.a(paramView, 2131362673, this);
    at.a(paramView, 2131362677, this);
    at.a(paramView, 2131363838, this);
    at.a(paramView, 2131362680, this);
    at.a(paramView, 2131362686, this);
    at.a(paramView, 2131362698, this);
    at.a(paramView, 2131362665, this);
    at.a(paramView, 2131362497, this);
    at.a(paramView, 2131363071, this);
    at.a(paramView, 2131362690, this);
    at.a(paramView, 2131363054, this);
    at.a(paramView, 2131362774, this);
    at.a(paramView, 2131362727, this);
    at.a(paramView, 2131362725, this);
    at.a(paramView, 2131362750, this);
    at.a(paramView, 2131362706, this);
    at.a(paramView, 2131362679, this);
    at.a(paramView, 2131362739, this);
    at.a(paramView, 2131362728, this);
    at.a(paramView, 2131362729, this);
    at.a(paramView, 2131362769, this);
    at.a(paramView, 2131362674, this);
    at.a(paramView, 2131362733, this);
    at.a(paramView, 2131362696, this);
    at.a(paramView, 2131362741, this);
    at.a(paramView, 2131362681, this);
    at.a(paramView, 2131362676, this);
    at.a(paramView, 2131362731, this);
    at.a(paramView, 2131362726, this);
    at.a(paramView, 2131362701, this);
    paramBundle = Locale.US;
    Settings.a();
    paramBundle = String.format(paramBundle, "%s v%s(%d) | %d", new Object[] { "Release", "10.41.6", Integer.valueOf(1041006), Long.valueOf(f.a("profileUserId", 0L)) });
    localObject2 = (TextView)paramView.findViewById(2131362847);
    ((TextView)localObject2).setText(paramBundle);
    localObject1 = co.b(requireContext());
    if (com.truecaller.util.k.a(requireContext(), "COMPILE_TIME") != null)
    {
      paramBundle = new Date(((Long)com.truecaller.util.k.a(requireContext(), "COMPILE_TIME")).longValue());
      paramBundle = new SimpleDateFormat("HH:mm dd/MM/yyyy", Locale.US).format(paramBundle);
    }
    else
    {
      paramBundle = "Not Available";
    }
    String str = com.truecaller.common.h.k.c();
    if (localObject1 == null)
    {
      localObject1 = "";
    }
    else
    {
      StringBuilder localStringBuilder = new StringBuilder("\nLat: ");
      localStringBuilder.append(((Location)localObject1).getLatitude());
      localStringBuilder.append("\nLon: ");
      localStringBuilder.append(((Location)localObject1).getLongitude());
      localObject1 = localStringBuilder.toString();
    }
    paramBundle = String.format("Build time: %s\nDevice: %s%s", new Object[] { paramBundle, str, localObject1 });
    ((TextView)paramView.findViewById(2131362838)).setText(paramBundle);
    paramBundle = (TextView)paramView.findViewById(2131362721);
    localObject1 = new StringBuilder("Address : ");
    ((StringBuilder)localObject1).append(a());
    ((StringBuilder)localObject1).append(":8080");
    paramBundle.setText(((StringBuilder)localObject1).toString());
    paramBundle = new l.a(this, (ViewGroup)paramView.findViewById(2131362730));
    localObject1 = (SearchView)paramView.findViewById(2131362765);
    ((SearchView)localObject1).setOnSearchClickListener(new -..Lambda.l.drCTXwn42Bx4PLoqR3CB3-Tp6sk((TextView)localObject2));
    ((SearchView)localObject1).setOnCloseListener(new -..Lambda.l.pfRe1B8ovAi4d82zkoeMvtEbjuk(paramBundle, (TextView)localObject2));
    ((SearchView)localObject1).setOnQueryTextListener(new l.1(this, paramBundle));
    ((NestedScrollView)paramView.findViewById(2131362764)).setOnScrollChangeListener(new -..Lambda.l.6GVD6UJpx9Uo-BVlb20OP1otlHI((SearchView)localObject1));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */