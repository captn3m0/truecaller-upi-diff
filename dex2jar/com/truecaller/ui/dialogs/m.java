package com.truecaller.ui.dialogs;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton.OnCheckedChangeListener;
import c.g.a.b;
import c.k.i;
import com.google.c.a.g;
import com.truecaller.R.id;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.f;
import com.truecaller.bp;
import com.truecaller.content.TruecallerContract.u;
import com.truecaller.messaging.data.o;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Reaction;
import com.truecaller.messaging.h;
import com.truecaller.messaging.transport.im.a.a;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import javax.inject.Inject;

public final class m
  extends AppCompatDialogFragment
{
  @Inject
  public f<o> a;
  @Inject
  public f<a> b;
  @Inject
  public h c;
  private final Random d = new Random();
  private final com.google.c.a.k e = com.google.c.a.k.a();
  private final List<String> f = (List)new ArrayList();
  private HashMap g;
  
  private final Message a(Participant paramParticipant, boolean paramBoolean)
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
  }
  
  private final <T> T a(List<? extends T> paramList)
  {
    return (T)paramList.get(d.nextInt(paramList.size()));
  }
  
  private final String a()
  {
    for (;;)
    {
      String str = (String)a(f);
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(c.n.m.a(str, 4));
      localStringBuilder.append(c.a.m.a((Iterable)i.b(0, str.length() - 4), (CharSequence)"", null, null, 0, null, (b)new m.d(this), 30));
      str = localStringBuilder.toString();
      try
      {
        boolean bool = e.c(e.a((CharSequence)str, null));
        if (bool) {
          return str;
        }
      }
      catch (g localg) {}
    }
  }
  
  private static void a(List<ContentProviderOperation> paramList, Reaction[] paramArrayOfReaction, int paramInt)
  {
    if (paramArrayOfReaction != null)
    {
      int j = paramArrayOfReaction.length;
      int i = 0;
      while (i < j)
      {
        Object localObject = paramArrayOfReaction[i];
        localObject = ContentProviderOperation.newInsert(TruecallerContract.u.a()).withValueBackReference("message_id", paramInt).withValue("emoji", d).withValue("send_date", Long.valueOf(e)).withValue("from_peer_id", c).build();
        c.g.b.k.a(localObject, "it");
        paramList.add(localObject);
        i += 1;
      }
      return;
    }
  }
  
  public final View a(int paramInt)
  {
    if (g == null) {
      g = new HashMap();
    }
    View localView2 = (View)g.get(Integer.valueOf(paramInt));
    View localView1 = localView2;
    if (localView2 == null)
    {
      localView1 = getView();
      if (localView1 == null) {
        return null;
      }
      localView1 = localView1.findViewById(paramInt);
      g.put(Integer.valueOf(paramInt), localView1);
    }
    return localView1;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = TrueApp.y();
    c.g.b.k.a(paramBundle, "TrueApp.getApp()");
    paramBundle.a().a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    c.g.b.k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558585, paramViewGroup, false);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    c.g.b.k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    ((Button)a(R.id.add_button)).setOnClickListener((View.OnClickListener)new m.f(this));
    ((CheckBox)a(R.id.existing_conversations_check_box)).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)new m.g(this));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */