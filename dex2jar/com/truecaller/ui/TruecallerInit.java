package com.truecaller.ui;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract.Contacts;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.AppBarLayout.Behavior;
import android.support.design.widget.AppBarLayout.b;
import android.support.design.widget.AppBarLayout.c;
import android.support.design.widget.CoordinatorLayout.b;
import android.support.design.widget.CoordinatorLayout.e;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.r;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.appinvite.AppInvite;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.truecaller.TrueApp;
import com.truecaller.analytics.TimingEvent;
import com.truecaller.analytics.au;
import com.truecaller.analytics.az;
import com.truecaller.analytics.bb;
import com.truecaller.analytics.bc;
import com.truecaller.bp;
import com.truecaller.calling.dialer.aj.b;
import com.truecaller.calling.recorder.CallRecordingManager;
import com.truecaller.calling.recorder.CallRecordingOnBoardingLaunchContext;
import com.truecaller.calling.recorder.CallRecordingOnBoardingState;
import com.truecaller.calling.recorder.aj;
import com.truecaller.calling.recorder.bj;
import com.truecaller.common.h.ac;
import com.truecaller.common.h.am;
import com.truecaller.common.ui.b.a;
import com.truecaller.consentrefresh.ConsentRefreshActivity;
import com.truecaller.engagementrewards.EngagementRewardActionType;
import com.truecaller.engagementrewards.EngagementRewardState;
import com.truecaller.filters.blockedevents.BlockedEventsActivity;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.data.ak;
import com.truecaller.messaging.data.al;
import com.truecaller.messaging.data.al.a;
import com.truecaller.old.data.access.Settings;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.premium.ba;
import com.truecaller.premium.ba.b;
import com.truecaller.premium.br;
import com.truecaller.profile.EditMeFormFragment;
import com.truecaller.profile.ProfileActivity;
import com.truecaller.profile.business.CreateBusinessProfileActivity;
import com.truecaller.referral.ReferralManager;
import com.truecaller.referral.ReferralManager.ReferralLaunchContext;
import com.truecaller.referral.ai;
import com.truecaller.scanner.NumberDetectorProcessor.ScanType;
import com.truecaller.scanner.NumberScannerActivity;
import com.truecaller.scanner.barcode.BarcodeCaptureActivity;
import com.truecaller.scanner.f.a;
import com.truecaller.scanner.s;
import com.truecaller.sdk.ConfirmProfileActivity;
import com.truecaller.service.AlarmReceiver;
import com.truecaller.service.DataManagerService;
import com.truecaller.service.SyncPhoneBookService;
import com.truecaller.startup_dialogs.StartupDialogDismissReason;
import com.truecaller.startup_dialogs.StartupDialogType;
import com.truecaller.tracking.events.ao;
import com.truecaller.tracking.events.ao.a;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.base.views.fragments.TcPayOnFragmentInteractionListener;
import com.truecaller.truepay.app.ui.transaction.views.activities.TransactionActivity;
import com.truecaller.ui.components.DrawerHeaderView;
import com.truecaller.ui.components.DrawerHeaderView.a;
import com.truecaller.ui.components.FloatingActionButton.c;
import com.truecaller.ui.components.FloatingActionButton.c.a;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.ui.view.BadgeDrawerArrowDrawable;
import com.truecaller.ui.view.BadgeDrawerArrowDrawable.BadgeTag;
import com.truecaller.ui.view.BottomBar;
import com.truecaller.ui.view.BottomBar.a;
import com.truecaller.update.ForcedUpdate;
import com.truecaller.util.b.e.1;
import com.truecaller.util.b.j.a;
import com.truecaller.util.co;
import com.truecaller.whoviewedme.WhoViewedMeActivity;
import com.truecaller.whoviewedme.WhoViewedMeLaunchContext;
import com.truecaller.wizard.utils.PermissionPoller;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;

public class TruecallerInit
  extends n
  implements GoogleApiClient.OnConnectionFailedListener, com.truecaller.common.ui.a, b.a, al.a, f.a, com.truecaller.startup_dialogs.a, TcPayOnFragmentInteractionListener, ae.a, ag.a, DrawerHeaderView.a, FloatingActionButton.c.a, BottomBar.a
{
  private AppBarLayout.c A;
  private String B;
  private int C = 0;
  private boolean D = false;
  private boolean E = false;
  private boolean F = false;
  private boolean G = true;
  private boolean H = false;
  private com.truecaller.service.h<Binder> I;
  private PermissionPoller J;
  private String K = null;
  private BroadcastReceiver L;
  private android.support.v4.app.j M;
  private ReferralManager N;
  private GoogleApiClient O;
  private com.truecaller.featuretoggles.e P;
  private com.truecaller.utils.d Q;
  private com.truecaller.i.e R;
  private com.truecaller.utils.l S;
  private com.truecaller.scanner.n T;
  private al U;
  private final BroadcastReceiver V = new TruecallerInit.1(this);
  private final BroadcastReceiver W = new TruecallerInit.2(this);
  private final BroadcastReceiver X = new TruecallerInit.3(this);
  @SuppressLint({"HandlerLeak"})
  private Handler Y = new TruecallerInit.4(this);
  final bp a = v.a();
  protected Toolbar b;
  protected View c;
  protected AppBarLayout d;
  protected DrawerLayout i;
  protected NavigationView j;
  protected DrawerHeaderView k;
  protected ActionBarDrawerToggle l;
  protected BottomBar m;
  protected BadgeDrawerArrowDrawable n;
  @Inject
  public com.truecaller.startup_dialogs.c o;
  @Inject
  public com.truecaller.g.a p;
  @Inject
  public au q;
  @Inject
  public br r;
  @Inject
  public com.truecaller.analytics.a.f s;
  @Inject
  public ag t;
  com.truecaller.common.g.a u;
  private TrueApp v = TrueApp.y();
  private TruecallerInit.b w;
  private boolean x = true;
  private com.truecaller.ui.view.c y;
  private com.truecaller.ui.view.c z;
  
  public static Intent a(Context paramContext, String paramString)
  {
    return a(paramContext, "calls", paramString, null);
  }
  
  public static Intent a(Context paramContext, String paramString1, String paramString2)
  {
    return a(paramContext, paramString1, paramString2, null);
  }
  
  public static Intent a(Context paramContext, String paramString1, String paramString2, String paramString3)
  {
    paramContext = new Intent(paramContext, TruecallerInit.class).putExtra("ARG_FRAGMENT", paramString1).setFlags(335609856);
    bb.a(paramContext, paramString2, paramString3);
    return paramContext;
  }
  
  public static void a(Activity paramActivity, String paramString1, String paramString2)
  {
    paramString1 = new Intent(paramActivity, TruecallerInit.class).putExtra("ARG_FRAGMENT", paramString1).setFlags(67174400);
    bb.a(paramString1, paramString2, null);
    paramActivity.startActivity(paramString1);
  }
  
  private static void a(Context paramContext)
  {
    com.truecaller.common.h.o.a(paramContext, com.truecaller.common.h.o.a(paramContext).addFlags(268435456));
  }
  
  public static void a(Context paramContext, String paramString1, boolean paramBoolean, String paramString2)
  {
    paramString1 = a(paramContext, paramString1, paramString2, null);
    paramString1.addFlags(268435456);
    if (paramBoolean) {
      paramString1.addFlags(32768);
    }
    paramContext.startActivity(paramString1);
  }
  
  private void a(Intent paramIntent)
  {
    if (android.support.v4.app.a.a(this, "android.permission.CALL_PHONE") != 0)
    {
      android.support.v4.app.a.a(this, new String[] { "android.permission.CALL_PHONE" }, 7004);
      return;
    }
    com.truecaller.util.a.a(this, paramIntent);
  }
  
  private void a(Fragment paramFragment)
  {
    if ((paramFragment instanceof com.truecaller.common.ui.b))
    {
      int i1 = ((com.truecaller.common.ui.b)paramFragment).f();
      c.setVisibility(i1);
    }
  }
  
  private void a(Menu paramMenu)
  {
    paramMenu.findItem(2131362904).setVisible(a.bh().a());
  }
  
  private void a(Integer paramInteger)
  {
    BottomBar localBottomBar = m;
    if ((localBottomBar != null) && (paramInteger != null)) {
      localBottomBar.a("calls", paramInteger.intValue());
    }
  }
  
  public static void b(Context paramContext, String paramString)
  {
    a(paramContext, "calls", false, paramString);
  }
  
  public static void b(Context paramContext, String paramString1, String paramString2)
  {
    a(paramContext, paramString1, false, paramString2);
  }
  
  private void b(Intent paramIntent)
  {
    paramIntent = paramIntent.getData();
    String str1;
    Object localObject;
    if ((paramIntent != null) && (!am.b(paramIntent.getHost())) && (paramIntent.getHost().equals(getString(2131888543))))
    {
      if ((v.isTcPayEnabled()) && (getString(2131888546).equals(paramIntent.getPath())))
      {
        str1 = paramIntent.getQueryParameter(getString(2131888554));
        localObject = paramIntent.getQueryParameter(getString(2131888541));
        String str2 = paramIntent.getQueryParameter(getString(2131888539));
        String str3 = paramIntent.getQueryParameter(getString(2131888539));
        TransactionActivity.startForSend(this, str2, str1, paramIntent.getQueryParameter(getString(2131888545)), (String)localObject, str3);
        return;
      }
      Toast.makeText(this, 2131888542, 1).show();
      return;
    }
    if ((paramIntent != null) && (!am.b(paramIntent.getHost())) && (paramIntent.getHost().equals(getString(2131888080))) && (Settings.g()))
    {
      str1 = paramIntent.getQueryParameter(getString(2131888108));
      paramIntent = paramIntent.getQueryParameter(getString(2131888107));
      if ((!am.b(str1)) && (str1.length() > 7))
      {
        localObject = new StringBuilder("+");
        ((StringBuilder)localObject).append(str1.trim());
        localObject = ((StringBuilder)localObject).toString();
        if (!v.b((String)localObject)) {}
      }
    }
    try
    {
      com.truecaller.flashsdk.core.c.a().a(this, Long.parseLong(str1.trim()), paramIntent, "deepLink");
      return;
    }
    catch (NumberFormatException paramIntent)
    {
      for (;;) {}
    }
    Toast.makeText(this, 2131888457, 0).show();
  }
  
  public static void c(Context paramContext, String paramString)
  {
    a(paramContext, "calls", true, paramString);
  }
  
  private boolean c(Intent paramIntent)
  {
    try
    {
      com.truecaller.common.h.ab.a(paramIntent, getApplicationContext());
      return true;
    }
    catch (SecurityException paramIntent)
    {
      for (;;) {}
    }
    return false;
  }
  
  private void d(Intent paramIntent)
  {
    com.truecaller.notifications.m.a(paramIntent.getStringExtra("HTML_PAGE")).show(getFragmentManager(), "dialog");
  }
  
  private void d(String paramString)
  {
    if ((getIntent().getExtras() != null) && ("app_shortcut".equals(getIntent().getExtras().getString("deeplink_source")))) {
      K = "homescreenShortcut";
    }
    TrueApp.y().a().c().a(new bc(paramString, K));
    HashMap localHashMap = new HashMap();
    localHashMap.put("ViewId", paramString);
    paramString = K;
    if (paramString != null) {
      localHashMap.put("Context", paramString);
    }
    paramString = ao.b().a("ViewVisited").a(localHashMap).b(com.truecaller.truepay.data.b.a.a()).a();
    ((com.truecaller.analytics.ae)a.f().a()).a(paramString);
  }
  
  private void n()
  {
    Object localObject2 = getIntent();
    Bundle localBundle = ((Intent)localObject2).getExtras();
    if (localBundle == null) {
      return;
    }
    String str1 = localBundle.getString("view");
    String str2 = localBundle.getString("subview", SettingsFragment.SettingsViewType.SETTINGS_MAIN.name());
    Object localObject1 = localBundle.getString("c");
    int i2 = 1;
    StringBuilder localStringBuilder = new StringBuilder("processDeepLinks:: Action: ");
    localStringBuilder.append(((Intent)localObject2).getAction());
    localStringBuilder.append("Screen: ");
    localStringBuilder.append(str1);
    localStringBuilder.append(" sub screen: ");
    localStringBuilder.append(str2);
    localStringBuilder.toString();
    int i3 = 0;
    if (am.a(((Intent)localObject2).getAction(), "android.intent.action.VIEW"))
    {
      if (str1 == null) {
        return;
      }
      int i1 = str1.hashCode();
      int i4 = 4;
      switch (i1)
      {
      default: 
        break;
      case 1449811358: 
        if (str1.equals("defaultdialer")) {
          i1 = 10;
        }
        break;
      case 1434631203: 
        if (str1.equals("settings")) {
          i1 = 1;
        }
        break;
      case 1101712723: 
        if (str1.equals("callrecording")) {
          i1 = 9;
        }
        break;
      case 85982298: 
        if (str1.equals("whoviewedme")) {
          i1 = 8;
        }
        break;
      case 3552126: 
        if (str1.equals("tabs")) {
          i1 = 4;
        }
        break;
      case -21437972: 
        if (str1.equals("blocked")) {
          i1 = 2;
        }
        break;
      case -91022241: 
        if (str1.equals("editprofile")) {
          i1 = 7;
        }
        break;
      case -722568291: 
        if (str1.equals("referral")) {
          i1 = 0;
        }
        break;
      case -874822710: 
        if (str1.equals("themes")) {
          i1 = 5;
        }
        break;
      case -966943889: 
        if (str1.equals("qamenu")) {
          i1 = 6;
        }
        break;
      case -1035287946: 
        if (str1.equals("detailview")) {
          i1 = 3;
        }
        break;
      }
      i1 = -1;
      switch (i1)
      {
      default: 
        break;
      case 10: 
        a(this);
        i1 = i2;
        break;
      case 9: 
        if ((str2.hashCode() == -1323191193) && (str2.equals("onboard"))) {
          i1 = i3;
        } else {
          i1 = -1;
        }
        if (i1 != 0)
        {
          i1 = i2;
          if (a.bh().a())
          {
            bj.a(this);
            i1 = i2;
          }
        }
        else
        {
          i1 = i2;
          if (a.bh().a())
          {
            if (a.bg().a()) {
              localObject1 = CallRecordingOnBoardingState.POST_ENABLE;
            } else {
              localObject1 = CallRecordingOnBoardingState.WHATS_NEW;
            }
            aj.a(this, (CallRecordingOnBoardingState)localObject1, CallRecordingOnBoardingLaunchContext.DEEP_LINK);
            i1 = i2;
          }
        }
        break;
      case 8: 
        startActivity(WhoViewedMeActivity.a(this, WhoViewedMeLaunchContext.DEEPLINK));
        break;
      case 7: 
        t();
        i1 = i2;
        break;
      case 6: 
        v.a(M);
        i1 = i2;
        break;
      case 5: 
        startActivity(SingleActivity.a(this, SingleActivity.FragmentSingle.THEME_SELECTOR));
        i1 = i2;
        break;
      case 4: 
        switch (str2.hashCode())
        {
        default: 
          break;
        case 1382682413: 
          if (str2.equals("payments")) {
            i1 = i4;
          }
          break;
        case 3045982: 
          if (str2.equals("call")) {
            i1 = 1;
          }
          break;
        case -318452137: 
          if (str2.equals("premium")) {
            i1 = 6;
          }
          break;
        case -337045466: 
          if (str2.equals("banking")) {
            i1 = 3;
          }
          break;
        case -462094004: 
          if (str2.equals("messages")) {
            i1 = 0;
          }
          break;
        case -567451565: 
          if (str2.equals("contacts")) {
            i1 = 2;
          }
          break;
        case -664572875: 
          if (str2.equals("blocking")) {
            i1 = 5;
          }
          break;
        }
        i1 = -1;
        switch (i1)
        {
        default: 
          i1 = 0;
          break;
        case 6: 
          getIntent().putExtra("ARG_FRAGMENT", "premium");
          i1 = i2;
          break;
        case 5: 
          getIntent().putExtra("ARG_FRAGMENT", "blocking");
          i1 = i2;
          break;
        case 4: 
          getIntent().putExtra("ARG_FRAGMENT", "payments");
          i1 = i2;
          break;
        case 3: 
          getIntent().putExtra("ARG_FRAGMENT", "banking");
          i1 = i2;
          break;
        case 2: 
          getIntent().putExtra("ARG_FRAGMENT", "contacts");
          i1 = i2;
          break;
        case 1: 
          getIntent().putExtra("ARG_FRAGMENT", "calls");
          i1 = i2;
          break;
        case 0: 
          getIntent().putExtra("ARG_FRAGMENT", "messages");
          i1 = i2;
        }
        break;
      case 3: 
        localObject1 = localBundle.getString("tel");
        if (localObject1 != null)
        {
          DetailsFragment.a(this, null, null, (String)localObject1, null, null, DetailsFragment.SourceType.DeepLink, true, 4);
          i1 = i2;
        }
        break;
      case 2: 
        startActivity(new Intent(this, BlockedEventsActivity.class));
        i1 = i2;
        break;
      case 1: 
        localObject1 = SettingsFragment.SettingsViewType.valueOf(am.l(str2));
        "processDeepLinks:: Settings screen: ".concat(String.valueOf(localObject1));
        SettingsFragment.b(this, (SettingsFragment.SettingsViewType)localObject1);
        i1 = i2;
        break;
      case 0: 
        localObject2 = N;
        i1 = i2;
        if (localObject2 == null) {
          break label1175;
        }
        ((ReferralManager)localObject2).b((String)localObject1);
        i1 = i2;
        break;
      }
      i1 = 0;
      label1175:
      if (i1 != 0) {
        getIntent().setAction(null);
      }
      return;
    }
  }
  
  private void o()
  {
    if (!v.isTcPayEnabled()) {
      return;
    }
    Bundle localBundle = getIntent().getExtras();
    if (localBundle == null) {
      return;
    }
    Object localObject = localBundle.getString("bank_symbol");
    String str = localBundle.getString("acc_number");
    if (!TextUtils.isEmpty((CharSequence)localObject))
    {
      getIntent().setAction(null);
      a("banking");
      if (Truepay.getInstance().isRegistrationComplete()) {
        localObject = com.truecaller.truepay.app.ui.dashboard.views.b.d.a((String)localObject, str);
      } else {
        localObject = Truepay.getInstance().getBankingFragment();
      }
      replaceFragment((Fragment)localObject);
      localObject = Boolean.TRUE;
    }
    else
    {
      localObject = Boolean.FALSE;
    }
    if ((!((Boolean)localObject).booleanValue()) && (localBundle.getBoolean("show_instant_reward")))
    {
      localObject = localBundle.getString("instant_reward_content");
      a("banking");
      if ((Truepay.getInstance().isRegistrationComplete()) && (localObject != null)) {
        localObject = com.truecaller.truepay.app.ui.dashboard.views.b.d.a((String)localObject, Boolean.TRUE);
      } else {
        localObject = Truepay.getInstance().getBankingFragment();
      }
      replaceFragment((Fragment)localObject);
    }
  }
  
  private void p()
  {
    boolean bool = P.e().a();
    Object localObject2;
    if (bool) {
      localObject2 = "messages";
    } else {
      localObject2 = "calls";
    }
    Intent localIntent = getIntent();
    int i2 = 0;
    int i3 = 0;
    Object localObject3 = localObject2;
    if (localIntent != null)
    {
      String str = localIntent.getAction();
      Object localObject1;
      int i1;
      if ((!"android.intent.action.DIAL".equals(str)) && (!"android.intent.action.VIEW".equals(str)))
      {
        localObject3 = localIntent.getExtras();
        localObject1 = localObject2;
        i1 = i3;
        if (localObject3 != null)
        {
          localObject2 = ((Bundle)localObject3).getString("ARG_FRAGMENT", (String)localObject2);
          localIntent.removeExtra("ARG_FRAGMENT");
          localObject1 = localObject2;
          if (bool)
          {
            localObject1 = localObject2;
            if ("calls".equals(localObject2)) {
              localObject1 = "messages";
            }
          }
          if ((!"banking".equals(localObject1)) && (!"payments".equals(localObject1))) {
            i1 = 0;
          } else {
            i1 = 1;
          }
          if ((!"blocking".equals(localObject1)) && (!"premium".equals(localObject1))) {
            i2 = 0;
          } else {
            i2 = 1;
          }
          localObject2 = localObject1;
          if (i1 != 0)
          {
            localObject2 = localObject1;
            if (!P.n().a()) {
              localObject2 = "calls";
            }
          }
          localObject3 = localObject2;
          if (i2 != 0) {
            if (H)
            {
              localObject3 = localObject2;
              if (!v.isTcPayEnabled()) {}
            }
            else
            {
              localObject3 = "calls";
            }
          }
          localObject1 = localObject3;
          i1 = i3;
          if ("search".equals(localObject3))
          {
            localObject1 = "calls";
            i1 = 1;
          }
        }
      }
      else if ("android.intent.action.DIAL".equals(str))
      {
        localObject1 = "calls";
        i1 = i3;
      }
      else
      {
        localObject1 = localObject2;
        i1 = i3;
        if ("android.intent.action.VIEW".equals(str))
        {
          localObject1 = localObject2;
          i1 = i3;
          if (c(localIntent))
          {
            localObject1 = "calls";
            i1 = i3;
          }
        }
      }
      localObject3 = localObject1;
      i2 = i1;
      if ("com.truecaller.intent.action.CUSTOM_WEB_VIEW_MAIN_DISPLAY".equals(str))
      {
        d(localIntent);
        i2 = i1;
        localObject3 = localObject1;
      }
    }
    m.a((String)localObject3);
    if (i2 != 0) {
      com.truecaller.search.global.n.a(this);
    }
    o();
  }
  
  private void q()
  {
    com.truecaller.ui.components.FloatingActionButton localFloatingActionButton = f();
    if (localFloatingActionButton == null) {
      return;
    }
    if (((e instanceof FloatingActionButton.c)) && (((FloatingActionButton.c)e).l()))
    {
      FloatingActionButton.c localc = (FloatingActionButton.c)e;
      localFloatingActionButton.setFabActionListener(localc.k());
      Drawable localDrawable = com.truecaller.utils.ui.b.a(this, localc.i(), 2130968965);
      int i1 = com.truecaller.utils.ui.b.a(this, 2130968964);
      localFloatingActionButton.setDrawable(localDrawable);
      localFloatingActionButton.setBackgroundColor(i1);
      localFloatingActionButton.setMenuItems(localc.j());
      localFloatingActionButton.a(true);
      return;
    }
    localFloatingActionButton.a(false);
  }
  
  private void r()
  {
    if (!"calls".equalsIgnoreCase(B))
    {
      if (m == null) {
        return;
      }
      ((com.truecaller.callhistory.a)a.ad().a()).e().a(a.m().a(), new -..Lambda.TruecallerInit.qPWuhqV4mnv5MaVuefYRqvDyB-E(this));
      return;
    }
  }
  
  private void s()
  {
    boolean bool = e instanceof ae;
    int i3 = 0;
    int i1;
    if ((bool) && (((ae)e).g())) {
      i1 = 1;
    } else {
      i1 = 0;
    }
    AppBarLayout.b localb = (AppBarLayout.b)c.getLayoutParams();
    Resources localResources = getResources();
    int i2;
    if (i1 != 0) {
      i2 = 2131165489;
    } else {
      i2 = 2131165496;
    }
    bottomMargin = localResources.getDimensionPixelSize(i2);
    c.setLayoutParams(localb);
    if (i1 != 0) {
      i1 = i3;
    } else {
      i1 = getResources().getDimensionPixelSize(2131166268);
    }
    r.b(d, i1);
  }
  
  private void t()
  {
    if (u())
    {
      if (!P.ar().a()) {
        startActivityForResult(CreateBusinessProfileActivity.b(this), 7001);
      }
    }
    else
    {
      if (P.j().a())
      {
        startActivityForResult(new Intent(this, ProfileActivity.class), 7001);
        return;
      }
      startActivityForResult(com.truecaller.profile.a.b(this), 7001);
    }
  }
  
  private boolean u()
  {
    return u.a("profileBusiness", false);
  }
  
  private void v()
  {
    com.truecaller.featuretoggles.e locale = P;
    if (n.a(locale, com.truecaller.featuretoggles.e.a[41]).a()) {
      startActivityForResult(new Intent(this, BarcodeCaptureActivity.class), 7005);
    } else {
      startActivityForResult(NumberScannerActivity.a(this, NumberDetectorProcessor.ScanType.SCAN_PHONE), 7003);
    }
    i.c();
  }
  
  private void w()
  {
    if (G) {
      t.a(new WeakReference(this));
    }
  }
  
  private void x()
  {
    if (v.isTcPayEnabled()) {
      if ("banking".equals(B)) {
        d("banking");
      } else if ("payments".equals(B)) {
        d("payments");
      }
    }
    Y.removeMessages(1);
    if ((e instanceof az))
    {
      Message localMessage = Y.obtainMessage(1, e);
      Y.sendMessageDelayed(localMessage, 1000L);
    }
  }
  
  private void y()
  {
    if (j.getHeaderCount() < 2) {
      return;
    }
    com.truecaller.common.f.c localc = a.ai();
    Object localObject = j;
    int i1 = 1;
    localObject = ((NavigationView)localObject).a(1);
    ImageView localImageView1 = (ImageView)((View)localObject).findViewById(2131362084);
    TextView localTextView1 = (TextView)((View)localObject).findViewById(2131364884);
    TextView localTextView2 = (TextView)((View)localObject).findViewById(2131363752);
    ImageView localImageView2 = (ImageView)((View)localObject).findViewById(2131363301);
    ImageView localImageView3 = (ImageView)((View)localObject).findViewById(2131362085);
    boolean bool = localc.o();
    int i3 = 8;
    if (bool)
    {
      com.truecaller.ui.view.c localc1 = new com.truecaller.ui.view.c(this, false, false, 2130969559);
      localImageView1.setImageDrawable(localc1);
      localc1.a(true);
      localImageView1.setVisibility(0);
    }
    else
    {
      localImageView1.setVisibility(8);
      localImageView1.setImageDrawable(null);
    }
    if ((!a.j().b()) || (!localc.d()) || (a.j().a(EngagementRewardActionType.BUY_PREMIUM_ANNUAL) != EngagementRewardState.COMPLETED)) {
      i1 = 0;
    }
    if (!localc.d())
    {
      localTextView1.setText(2131886314);
    }
    else if (localc.k().equals("regular"))
    {
      localTextView1.setText(2131886824);
      localImageView2.setImageResource(2131234161);
      if (i1 != 0) {
        i2 = 0;
      } else {
        i2 = 8;
      }
      localImageView3.setVisibility(i2);
      int i2 = i3;
      if (i1 != 0) {
        i2 = 0;
      }
      localTextView2.setVisibility(i2);
    }
    else if (localc.k().equals("gold"))
    {
      localTextView1.setText(2131886823);
    }
    if (i1 != 0)
    {
      ((View)localObject).setOnClickListener(new -..Lambda.TruecallerInit.7vGYRsSgfCAgLjSNgCiabfnJEVU(this));
      return;
    }
    ((View)localObject).setOnClickListener(new -..Lambda.TruecallerInit.d5jW3thcXpyo3WWRQlDFMCDqCeo(this));
  }
  
  private void z()
  {
    if (k != null)
    {
      if (!v.p())
      {
        k.b();
        return;
      }
      String str3 = com.truecaller.profile.c.a(u);
      String str4 = u.a("profileAvatar");
      boolean bool = u();
      Uri localUri = null;
      if (bool) {
        str1 = com.truecaller.profile.c.c(u);
      } else {
        str1 = null;
      }
      String str2 = str1;
      if (am.d(str1)) {
        str2 = com.truecaller.profile.c.b(u);
      }
      DrawerHeaderView localDrawerHeaderView = k;
      String str1 = str3;
      if (str3 == null) {
        str1 = "";
      }
      if (!am.d(str4)) {
        localUri = Uri.parse(str4);
      }
      localDrawerHeaderView.a(str2, str1, localUri);
    }
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    Object localObject = n;
    if (localObject != null)
    {
      int i1 = paramInt1 + paramInt2;
      if (b.b != i1)
      {
        b.b = i1;
        ((BadgeDrawerArrowDrawable)localObject).invalidateSelf();
      }
    }
    localObject = y;
    if (localObject != null) {
      ((com.truecaller.ui.view.c)localObject).a(paramInt1);
    }
    localObject = z;
    if (localObject != null) {
      ((com.truecaller.ui.view.c)localObject).a(paramInt2);
    }
  }
  
  public final void a(AppBarLayout.c paramc)
  {
    AppBarLayout.c localc = A;
    if (localc != null) {
      d.b(localc);
    }
    A = paramc;
    d.a(paramc);
  }
  
  public final void a(ak paramak)
  {
    int i3 = a;
    int i4 = b;
    if (m != null)
    {
      boolean bool2 = Q.d();
      boolean bool1 = true;
      int i2 = 0;
      if (bool2) {
        if (S.a(new String[] { "android.permission.READ_SMS" }))
        {
          i1 = 1;
          break label69;
        }
      }
      int i1 = 0;
      label69:
      bool2 = a.aG().a();
      boolean bool3 = R.a("notDefaultSmsBadgeShown", false);
      paramak = m;
      if ((bool3) || (i1 != 0) || (bool2)) {
        bool1 = false;
      }
      if (("messages".hashCode() != -462094004) || (!"messages".equals("messages"))) {
        i2 = -1;
      }
      if (i2 == 0)
      {
        com.truecaller.util.at.a(paramak.getContext(), a.getDrawable(), bool1);
        a.refreshDrawableState();
        a.invalidate();
      }
      if ((i1 != 0) || (bool2)) {
        m.a("messages", Integer.valueOf(i3 + i4).intValue());
      }
    }
  }
  
  public final void a(StartupDialogType paramStartupDialogType, StartupDialogDismissReason paramStartupDialogDismissReason)
  {
    o.a(paramStartupDialogType, paramStartupDialogDismissReason);
  }
  
  public final void a(String paramString)
  {
    m.a(paramString);
  }
  
  public final void ag_()
  {
    a(e);
  }
  
  public final void ah_()
  {
    v();
  }
  
  protected final void b(o paramo)
  {
    AssertionUtil.OnlyInDebug.fail(new String[] { "switchFragment() is unavailable for TruecallerInit" });
  }
  
  @TargetApi(21)
  public final void b(String paramString)
  {
    android.support.v4.app.o localo = M.a();
    localo.a(true);
    localo.a(0);
    Object localObject2 = M.a(String.valueOf(paramString));
    Object localObject1;
    int i1;
    int i2;
    if (localObject2 != null)
    {
      localObject1 = localObject2;
      if (!(localObject2 instanceof com.truecaller.truepay.app.ui.base.views.fragments.b)) {}
    }
    else
    {
      i1 = -1;
      switch (paramString.hashCode())
      {
      default: 
        break;
      case 1382682413: 
        if (paramString.equals("payments")) {
          i1 = 4;
        }
        break;
      case 94425557: 
        if (paramString.equals("calls")) {
          i1 = 0;
        }
        break;
      case -318452137: 
        if (paramString.equals("premium")) {
          i1 = 6;
        }
        break;
      case -337045466: 
        if (paramString.equals("banking")) {
          i1 = 3;
        }
        break;
      case -462094004: 
        if (paramString.equals("messages")) {
          i1 = 1;
        }
        break;
      case -567451565: 
        if (paramString.equals("contacts")) {
          i1 = 2;
        }
        break;
      case -664572875: 
        if (paramString.equals("blocking")) {
          i1 = 5;
        }
        break;
      }
      switch (i1)
      {
      default: 
        localObject1 = localObject2;
        break;
      case 6: 
        i1 = getResources().getDimensionPixelSize(2131165352);
        i2 = getResources().getDimensionPixelSize(2131165495);
        localObject1 = ba.a(PremiumPresenterView.LaunchContext.BOTTOM_BAR, new ba.b(null, 2131233992, com.truecaller.util.at.a(this, 16.0F), i1 + i2));
        break;
      case 5: 
        localObject1 = new com.truecaller.filters.blockedevents.e();
        break;
      case 4: 
        localObject1 = Truepay.getInstance().getPaymentsHomeFragment();
        break;
      case 3: 
        localObject1 = Truepay.getInstance().getBankingFragment();
        break;
      case 2: 
        localObject1 = P;
        if (e.a((com.truecaller.featuretoggles.e)localObject1, com.truecaller.featuretoggles.e.a[16]).a()) {
          localObject1 = new com.truecaller.calling.contacts_list.k();
        } else {
          localObject1 = new i();
        }
        break;
      case 1: 
        localObject1 = new com.truecaller.messaging.conversationlist.h();
        break;
      case 0: 
        q.a(TimingEvent.CALL_LOG_STARTUP, null, "fragment:V2");
        localObject1 = new com.truecaller.calling.dialer.l();
        o = N;
      }
      localo.a(2131363156, (Fragment)localObject1, String.valueOf(paramString));
    }
    if (localObject1 != null)
    {
      if (Q.h() >= 21) {
        i1 = 1;
      } else {
        i1 = 0;
      }
      if ((!"banking".equals(paramString)) && (!"payments".equals(paramString))) {
        i2 = 0;
      } else {
        i2 = 1;
      }
      if ((v.isTcPayEnabled()) && (i2 != 0))
      {
        if (i1 != 0) {
          getWindow().setStatusBarColor(android.support.v4.content.b.c(this, 2131100521));
        }
        if ("banking".equals(paramString)) {
          Truepay.getInstance().incrementBankingClicked();
        } else if ("payments".equals(paramString)) {
          Truepay.getInstance().incrementPaymentsClicked();
        }
      }
      if ((e != null) && (D) && ((e instanceof ab))) {
        ((ab)e).a(true);
      }
      localObject2 = M.f();
      if (localObject2 != null)
      {
        localObject2 = ((List)localObject2).iterator();
        while (((Iterator)localObject2).hasNext())
        {
          Fragment localFragment = (Fragment)((Iterator)localObject2).next();
          if ((localFragment != null) && (!localFragment.isHidden())) {
            if ((localFragment instanceof android.support.v4.app.e)) {
              localo.a(localFragment);
            } else {
              localo.b(localFragment);
            }
          }
        }
      }
      localObject2 = b.getMenu();
      if (localObject2 != null) {
        ((Menu)localObject2).close();
      }
      localo.c((Fragment)localObject1);
      localo.d();
      B = paramString;
      e = ((Fragment)localObject1);
      x();
      if (M.b())
      {
        paramString = d;
        if (paramString != null) {
          paramString.a(true, false, true);
        }
        q();
      }
      if ((D) && ((e instanceof ab))) {
        ((ab)e).n();
      }
      a(e);
      s();
    }
  }
  
  protected final boolean b()
  {
    if (i.d())
    {
      i.c();
      return true;
    }
    com.truecaller.ui.components.FloatingActionButton localFloatingActionButton = f();
    if ((localFloatingActionButton != null) && (a))
    {
      localFloatingActionButton.c();
      return true;
    }
    return false;
  }
  
  public final void c(String paramString)
  {
    d.a(true, true, true);
    if ((v.isTcPayEnabled()) && ((paramString.equals("payments")) || (paramString.equals("banking")))) {
      return;
    }
    if ((e instanceof ab)) {
      ((ab)e).m();
    }
  }
  
  public final void d()
  {
    s();
  }
  
  public final PermissionPoller e()
  {
    if (J == null)
    {
      J = new PermissionPoller(v, Y, a(this, null));
      J = new PermissionPoller(v, Y, a(this, null));
    }
    return J;
  }
  
  public final com.truecaller.ui.components.FloatingActionButton f()
  {
    View localView = findViewById(2131363120);
    if ((localView instanceof com.truecaller.ui.components.FloatingActionButton)) {
      return (com.truecaller.ui.components.FloatingActionButton)localView;
    }
    return null;
  }
  
  protected final int g()
  {
    return 2130969581;
  }
  
  public final void h()
  {
    q();
  }
  
  public final void i()
  {
    aj.b localb = e).f;
    if (localb == null) {
      c.g.b.k.a("dialpadPresenter");
    }
    localb.g();
  }
  
  public final void j()
  {
    android.support.v4.app.o localo = M.a();
    Fragment localFragment1 = M.a("payments");
    Fragment localFragment2 = M.a("premium");
    if (localFragment1 != null) {
      localo.a(localFragment1);
    }
    if (localFragment2 != null) {
      localo.a(localFragment2);
    }
    localo.d();
  }
  
  public final void k()
  {
    t();
  }
  
  public final void l()
  {
    if (u())
    {
      if (!P.ar().a()) {
        startActivityForResult(CreateBusinessProfileActivity.b(this), 7001);
      }
    }
    else {
      startActivityForResult(EditMeFormFragment.a(this), 7001);
    }
  }
  
  public final void m()
  {
    com.truecaller.wizard.b.c.a(this, WizardActivity.class, "sideBar");
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    if (o.a(paramInt1))
    {
      "onActivityResult:: Handled by startup dialog resolver. Req code: ".concat(String.valueOf(paramInt1));
      return;
    }
    if ((paramInt1 == 7001) && (paramInt2 == -1) && (k != null))
    {
      z();
      return;
    }
    if ((paramInt1 == 7003) && (paramInt2 == -1))
    {
      paramIntent = paramIntent.getExtras();
      if (paramIntent != null)
      {
        paramIntent = paramIntent.getStringArrayList("extra_results");
        if (paramIntent != null)
        {
          if (paramIntent.size() > 1)
          {
            new AlertDialog.Builder(this).setTitle(2131888737).setAdapter(new s(this, paramIntent), new -..Lambda.TruecallerInit.jp0IW3rAM69h-eCVmwD4P8zbeNs(this, paramIntent)).create().show();
            return;
          }
          com.truecaller.scanner.f.a((String)paramIntent.get(0), this);
        }
      }
      return;
    }
    if ((paramInt1 == 7005) && (paramInt2 == -1))
    {
      paramIntent = paramIntent.getExtras();
      if (paramIntent != null)
      {
        paramIntent = paramIntent.getString("extra_barcode_value");
        if (paramIntent != null)
        {
          localObject = T.b(paramIntent);
          paramIntent = new Bundle();
          paramIntent.putString("qr_scan_code", (String)a);
          paramIntent.putString("qr_partner_name", (String)b);
          localObject = new Intent(this, ConfirmProfileActivity.class);
          ((Intent)localObject).putExtras(paramIntent);
          startActivity((Intent)localObject);
        }
      }
      return;
    }
    Object localObject = M.f().iterator();
    while (((Iterator)localObject).hasNext())
    {
      Fragment localFragment = (Fragment)((Iterator)localObject).next();
      if ((localFragment != null) && (!localFragment.isHidden())) {
        localFragment.onActivityResult(paramInt1, paramInt2, paramIntent);
      }
    }
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    l.onConfigurationChanged(paramConfiguration);
  }
  
  public void onConnectionFailed(ConnectionResult paramConnectionResult) {}
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i1;
    if ((v.o()) && ((!v.p()) || (!com.truecaller.common.b.e.a("wizard_FullyCompleted", false))))
    {
      if (com.truecaller.common.b.e.a("silentLoginFailed", false)) {
        v.a(false);
      }
      if (getIntent().hasExtra("EXTRA_REG_NUDGE")) {
        com.truecaller.wizard.b.c.a(this, WizardActivity.class, getIntent().getExtras(), false);
      } else {
        com.truecaller.wizard.b.c.a(this, WizardActivity.class, null, false);
      }
      i1 = 1;
    }
    else if (ForcedUpdate.a(this, false))
    {
      i1 = 1;
    }
    else if (!f.f())
    {
      RequiredPermissionsActivity.a(this);
      i1 = 1;
    }
    else
    {
      i1 = 0;
    }
    if (i1 != 0)
    {
      overridePendingTransition(0, 0);
      finish();
      return;
    }
    setTheme(aresId);
    a.cs().a(this);
    P = a.aF();
    Q = a.bx();
    u = a.I();
    R = a.F();
    S = a.bw();
    T = a.bX();
    U = a.bY();
    M = getSupportFragmentManager();
    p.a(this);
    a.ao().a();
    if (GoogleApiAvailability.a().a(this) == 0)
    {
      N = com.truecaller.referral.w.a(this, "ReferralManagerImpl");
      if (N != null)
      {
        O = new GoogleApiClient.Builder(this).a(this, this).a(AppInvite.a).b();
        N.a(getIntent().getData());
      }
    }
    if ((P.ap().a()) && (v.p())) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    H = bool1;
    paramBundle = getIntent();
    K = paramBundle.getStringExtra("AppUserInteraction.Context");
    a(paramBundle);
    if (Q.h() >= 21)
    {
      localObject1 = getWindow();
      ((Window)localObject1).addFlags(Integer.MIN_VALUE);
      ((Window)localObject1).setStatusBarColor(0);
    }
    Object localObject1 = a.J();
    if ((v.p()) && (((ac)localObject1).a()) && (!u.b("core_agreed_region_1")))
    {
      ConsentRefreshActivity.b(this);
      overridePendingTransition(0, 0);
      finish();
      return;
    }
    o.a(this);
    o.a();
    setContentView(2131558477);
    b = ((Toolbar)findViewById(2131363715));
    c = findViewById(2131364917);
    d = ((AppBarLayout)findViewById(2131362038));
    i = ((DrawerLayout)findViewById(2131362909));
    j = ((NavigationView)findViewById(2131363793));
    m = ((BottomBar)findViewById(2131362140));
    if (!v.p()) {
      G = false;
    }
    I = new com.truecaller.service.h(this, DataManagerService.class);
    AlarmReceiver.a(paramBundle);
    AlarmReceiver.a(this, false);
    w = new TruecallerInit.b(this);
    getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_URI, true, w);
    n();
    localObject1 = new TruecallerInit.5(this);
    ((AppBarLayout.Behavior)localObject1).a(new TruecallerInit.6(this));
    ((CoordinatorLayout.e)d.getLayoutParams()).a((CoordinatorLayout.b)localObject1);
    localObject1 = (ImageView)findViewById(2131364985);
    if (co.c(this)) {
      ((ImageView)localObject1).setImageResource(2131234510);
    }
    com.truecaller.utils.ui.b.a((ImageView)localObject1, com.truecaller.utils.ui.b.a(this, 2130969581));
    com.truecaller.util.at.a(b);
    b.setOnClickListener(new -..Lambda.TruecallerInit.OPGNt9NxO81Yh3bayuiHiKEM2gA(this));
    setSupportActionBar(b);
    localObject1 = getSupportActionBar();
    if (localObject1 != null)
    {
      ((ActionBar)localObject1).setDisplayHomeAsUpEnabled(true);
      ((ActionBar)localObject1).setHomeButtonEnabled(true);
      ((ActionBar)localObject1).setTitle(0);
    }
    l = new TruecallerInit.7(this, this, i, b);
    n = new BadgeDrawerArrowDrawable(this);
    n.setColor(com.truecaller.utils.ui.b.a(this, 2130969581));
    l.setDrawerArrowDrawable(n);
    i.a(l);
    localObject1 = i;
    Object localObject2 = android.support.v4.content.b.a(((DrawerLayout)localObject1).getContext(), 2131231159);
    if (!DrawerLayout.c)
    {
      m = ((Drawable)localObject2);
      ((DrawerLayout)localObject1).a();
      ((DrawerLayout)localObject1).invalidate();
    }
    l.syncState();
    m.setup(this);
    m.getFab().setOnLongClickListener(new -..Lambda.TruecallerInit.KnohTY0PNh-K6mDnqHnzlwBAoWo(this));
    m.b("messages").setOnLongClickListener(new -..Lambda.TruecallerInit.03x4bTudoIqpVz1CZkaBr8i4NdY(this));
    m.b("contacts").setOnLongClickListener(new -..Lambda.TruecallerInit.V2KbhKI5Y66X8wypkZB1hSiXOHU(this));
    p();
    boolean bool3 = v.p();
    localObject1 = j.getMenu();
    if (bool3)
    {
      localObject2 = getLayoutInflater().inflate(2131558832, j, false);
      j.c.a((View)localObject2);
      y();
    }
    localObject2 = ((Menu)localObject1).findItem(2131362903);
    if ((H) && (!v.isTcPayEnabled())) {
      bool1 = false;
    } else {
      bool1 = true;
    }
    ((MenuItem)localObject2).setVisible(bool1);
    localObject2 = ((Menu)localObject1).findItem(2131362910);
    Object localObject3 = (ImageView)((MenuItem)localObject2).getActionView();
    y = new com.truecaller.ui.view.c(this, false, false, 2130969528);
    ((ImageView)localObject3).setImageDrawable(y);
    ((MenuItem)localObject2).setVisible(bool3);
    k = ((DrawerHeaderView)j.a(0));
    k.setDrawerHeaderListener(this);
    j.setNavigationItemSelectedListener(new -..Lambda.TruecallerInit.sk2X065LqlaYETShpqc1T8pVtL4(this));
    i.a(new TruecallerInit.a(this, (byte)0));
    localObject2 = N;
    if ((localObject2 != null) && (((ReferralManager)localObject2).c(ReferralManager.ReferralLaunchContext.NAVIGATION_DRAWER))) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    boolean bool2;
    if ((v.o()) && (!bool1)) {
      bool2 = true;
    } else {
      bool2 = false;
    }
    localObject2 = ((Menu)localObject1).findItem(2131362916);
    ((MenuItem)localObject2).setVisible(a.bk().a());
    localObject2 = (ImageView)((MenuItem)localObject2).getActionView();
    z = new com.truecaller.ui.view.c(this, false, false, 2130969528);
    ((ImageView)localObject2).setImageDrawable(z);
    ((Menu)localObject1).findItem(2131362913).setVisible(bool3);
    ((Menu)localObject1).findItem(2131362915).setVisible(bool2);
    localObject2 = ((Menu)localObject1).findItem(2131362912);
    ((MenuItem)localObject2).setVisible(bool1);
    if (bool1)
    {
      localObject3 = a.ao().a("referalInvite_18335");
      if ("inviteEarn".equalsIgnoreCase((String)localObject3)) {
        ((MenuItem)localObject2).setTitle(2131888644);
      } else if ("referEarn".equalsIgnoreCase((String)localObject3)) {
        ((MenuItem)localObject2).setTitle(2131888645);
      } else {
        ((MenuItem)localObject2).setTitle(2131888643);
      }
    }
    boolean bool1 = u.a("featureOfflineDirectory", false);
    j.getMenu().findItem(2131362905).setVisible(bool1);
    a((Menu)localObject1);
    if (v.isTcPayEnabled())
    {
      bool1 = Truepay.getInstance().shouldShowBankingAsNew();
      bool2 = Truepay.getInstance().shouldShowPaymentsAsNew();
      localObject1 = m;
      if ((b != null) && (c != null)) {
        if (bool1)
        {
          b.setVisibility(0);
          c.setVisibility(8);
        }
        else if ((bool2) && (!d))
        {
          b.setVisibility(8);
          c.setVisibility(0);
        }
        else
        {
          b.setVisibility(8);
          c.setVisibility(8);
        }
      }
    }
    localObject1 = a.aI();
    if (Q.h() >= 26) {
      i1 = 1;
    } else {
      i1 = 0;
    }
    bool1 = ((com.truecaller.common.h.c)localObject1).a();
    if ((i1 == 0) && (!bool1))
    {
      localObject2 = a.aA();
      if ((!((com.truecaller.common.h.c)localObject1).b()) && (!Settings.e("dialerShortcutInstalled")))
      {
        ((com.truecaller.util.d.a)localObject2).a(0);
        Settings.a("dialerShortcutInstalled", true);
      }
      if (!Settings.e("messagesShortcutInstalled"))
      {
        ((com.truecaller.util.d.a)localObject2).a(1);
        Settings.a("messagesShortcutInstalled", true);
      }
      if (!Settings.e("shortcutInstalled"))
      {
        ((com.truecaller.util.d.a)localObject2).a(2);
        Settings.a("shortcutInstalled", true);
      }
    }
    if (S.a(new String[] { "android.permission.READ_SMS" })) {
      a.C().k(false);
    }
    F = true;
    b(paramBundle);
    localObject1 = N;
    if (localObject1 != null) {
      ai.a(paramBundle, (ReferralManager)localObject1);
    }
    if (a.C().t() == 0) {
      v.a(new int[0]);
    }
    if (a.aq().a()) {
      a.at().a(new String[] { "HISTORY", "NOTIFICATIONS", "SEARCHRESULTS", "BLOCK", "BLOCK_UPDATE", "CALLLOG", "CONTACTS", "INBOX" });
    }
    if (P.l().a()) {
      a.W().a(2131364166);
    }
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131623958, paramMenu);
    MenuItem localMenuItem = paramMenu.findItem(2131361846);
    j.a locala = com.truecaller.util.b.at.a(this).a();
    if (com.truecaller.util.b.j.a(locala))
    {
      localMenuItem.setVisible(true);
      localMenuItem.setIcon(b);
      localMenuItem.setTitle(d);
    }
    return super.onCreateOptionsMenu(paramMenu);
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    if (!F) {
      return;
    }
    if (L != null) {
      android.support.v4.content.d.a(this).a(L);
    }
    com.truecaller.util.b.at.a = null;
    getContentResolver().unregisterContentObserver(w);
    w = null;
    Object localObject = Y;
    if (localObject != null)
    {
      ((Handler)localObject).removeMessages(1);
      Y = null;
    }
    localObject = I;
    if (localObject != null) {
      ((com.truecaller.service.h)localObject).b();
    }
    localObject = J;
    if (localObject != null) {
      ((PermissionPoller)localObject).a();
    }
  }
  
  public void onHamburgerClicked()
  {
    if ((v.isTcPayEnabled()) && ((B.equals("banking")) || (B.equals("payments")))) {
      i.b();
    }
  }
  
  public void onNewIntent(Intent paramIntent)
  {
    super.onNewIntent(paramIntent);
    setIntent(paramIntent);
    n();
    K = paramIntent.getStringExtra("AppUserInteraction.Context");
    a(paramIntent);
    if (m == null) {
      return;
    }
    p();
    b(paramIntent);
    ReferralManager localReferralManager = N;
    if (localReferralManager != null)
    {
      if (O != null) {
        localReferralManager.a(paramIntent.getData());
      }
      ai.a(paramIntent, N);
    }
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (l.onOptionsItemSelected(paramMenuItem)) {
      return true;
    }
    if (paramMenuItem.getItemId() == 2131361846)
    {
      j.a locala = com.truecaller.util.b.at.a(this).a();
      if (com.truecaller.util.b.j.a(locala))
      {
        Object localObject = View.inflate(this, 2131559161, null);
        AlertDialog localAlertDialog = new AlertDialog.Builder(this).setView((View)localObject).create();
        TextView localTextView = (TextView)((View)localObject).findViewById(2131364807);
        localTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, c, 0);
        localTextView.setText(d);
        localObject = (ListView)((View)localObject).findViewById(2131363665);
        ((ListView)localObject).setAdapter(new com.truecaller.util.b.e.a(this, getResources().getStringArray(e)));
        ((ListView)localObject).setOnItemClickListener(new e.1(localAlertDialog, this, locala));
        localAlertDialog.show();
        TrueApp.y().a().c().a(new com.truecaller.analytics.e.a("CARRIER_Menu_Opened").a("Partner", a).a());
      }
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }
  
  public void onPause()
  {
    super.onPause();
    if ((e instanceof ab)) {
      ((ab)e).a(false);
    }
    D = false;
  }
  
  @SuppressLint({"MissingPermission"})
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    if ((paramInt == 7004) && (paramArrayOfInt.length > 0) && (paramArrayOfInt[0] == 0)) {
      com.truecaller.util.a.a(this, getIntent());
    }
  }
  
  @SuppressLint({"InlinedApi"})
  public void onResume()
  {
    super.onResume();
    if (ForcedUpdate.a(this, false))
    {
      overridePendingTransition(0, 0);
      finish();
      return;
    }
    if ((v.o()) && (!v.p()))
    {
      v.a(false);
      com.truecaller.wizard.b.c.a(this, WizardActivity.class, null, true);
      return;
    }
    if (v.q())
    {
      L = new TruecallerInit.8(this);
      android.support.v4.content.d.a(this).a(L, new IntentFilter("com.truecaller.wizard.ACTION_AUTO_LOGIN"));
      localObject = v;
      com.truecaller.debug.log.a.a(new Object[] { "Wizard auto login" });
      new com.truecaller.wizard.utils.b((Context)localObject).h();
    }
    w();
    Object localObject = J;
    if (localObject != null) {
      ((PermissionPoller)localObject).a();
    }
    a.o().a(true);
    r();
    if ((e instanceof ab)) {
      ((ab)e).n();
    }
    s();
    D = true;
    a(j.getMenu());
    ((com.truecaller.presence.c)a.ae().a()).b();
    a.cb().b();
    a.ce().a(this);
    m.a();
  }
  
  public void onStart()
  {
    super.onStart();
    Object localObject = I;
    if (localObject != null) {
      ((com.truecaller.service.h)localObject).a();
    }
    z();
    if (!E)
    {
      ((com.truecaller.callhistory.a)a.ad().a()).a();
      E = true;
    }
    if (x)
    {
      x = false;
      SyncPhoneBookService.a(this);
      if (a.aF().A().a()) {
        a.bA();
      }
    }
    com.truecaller.utils.extensions.i.a(this, V, new String[] { "com.truecaller.notification.action.NOTIFICATIONS_UPDATED" });
    com.truecaller.utils.extensions.i.a(this, W, new String[] { "com.truecaller.action.UPDATE_CALL_BADGE" });
    com.truecaller.utils.extensions.i.a(this, X, new String[] { com.truecaller.common.network.g.c.a });
    U.a(this);
    U.b();
    a.o().a(false);
    x();
    localObject = N;
    if (localObject != null) {
      ((ReferralManager)localObject).b();
    }
    BadgeDrawerArrowDrawable localBadgeDrawerArrowDrawable = n;
    if ((a.ai().o()) && (!u.b("subscriptionPaymentFailedViewShownOnce"))) {
      localObject = BadgeDrawerArrowDrawable.BadgeTag.ERROR;
    } else {
      localObject = BadgeDrawerArrowDrawable.BadgeTag.NONE;
    }
    a = ((BadgeDrawerArrowDrawable.BadgeTag)localObject);
    if (com.truecaller.ui.view.BadgeDrawerArrowDrawable.1.a[localObject.ordinal()] != 1)
    {
      b.a(d);
      b.a = false;
      return;
    }
    b.a(com.truecaller.utils.ui.b.a(c, 2130969559));
    b.a = true;
    localBadgeDrawerArrowDrawable.invalidateSelf();
  }
  
  public void onStop()
  {
    super.onStop();
    com.truecaller.service.h localh = I;
    if (localh != null) {
      localh.b();
    }
    Y.removeMessages(1);
    android.support.v4.content.d.a(this).a(V);
    android.support.v4.content.d.a(this).a(W);
    android.support.v4.content.d.a(this).a(X);
    U.c();
    U.b(this);
  }
  
  public void replaceFragment(Fragment paramFragment)
  {
    if ((v.isTcPayEnabled()) && ((B.equals("banking")) || (B.equals("payments"))))
    {
      android.support.v4.app.o localo = M.a();
      localo.a(false);
      if (M.a(2131363156) == null) {
        localo.a(2131363156, paramFragment);
      } else {
        localo.b(2131363156, paramFragment);
      }
      localo.c(paramFragment);
      localo.d();
      M.b();
      a(paramFragment);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.TruecallerInit
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */