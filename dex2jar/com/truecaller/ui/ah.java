package com.truecaller.ui;

import android.content.ComponentName;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ac;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.b.e;
import com.truecaller.wizard.TruecallerWizard;
import com.truecaller.wizard.d.c;

public class ah
  extends TruecallerWizard
{
  public final void b()
  {
    super.b();
    e.b("wizard_OEMMode", false);
    getPackageManager().setComponentEnabledSetting(new ComponentName(this, ah.class), 2, 1);
    ac.a(this).a(null, 2131362836);
  }
  
  public final c c()
  {
    return new WizardActivity.a(((bk)getApplication()).a().ad());
  }
  
  public void onCreate(Bundle paramBundle)
  {
    e.b("wizard_OEMMode", true);
    e.b("signUpOrigin", "deviceSetup");
    super.onCreate(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.ah
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */