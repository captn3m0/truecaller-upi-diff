package com.truecaller.ui;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.g.a;
import com.truecaller.common.h.am;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.f;
import com.truecaller.notifications.y;
import com.truecaller.notifications.y.b;
import java.util.List;
import javax.inject.Inject;

public class QaOtpListActivity
  extends AppCompatActivity
{
  RecyclerView a;
  TextView b;
  QaOtpListActivity.b c;
  @Inject
  public y d;
  @Inject
  public a e;
  @Inject
  public e f;
  @Inject
  public b g;
  private AsyncTask<Void, Void, List<y.b>> h;
  private EditText i;
  private Button j;
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    ((bk)TrueApp.x()).a().a(this);
    setContentView(2131558462);
    a = ((RecyclerView)findViewById(2131364199));
    i = ((EditText)findViewById(2131364102));
    b = ((TextView)findViewById(2131364988));
    j = ((Button)findViewById(2131364227));
    a.setLayoutManager(new LinearLayoutManager(this, 1, false));
    a.addItemDecoration(new DividerItemDecoration(this, 1));
    paramBundle = f.aj().e();
    if (!am.b(paramBundle)) {
      i.setText(paramBundle.replaceAll("/", "\n"));
    }
    j.setOnClickListener(new -..Lambda.QaOtpListActivity.qhr2XREyJni6cA-KUtZqgZQoJ4M(this));
    h = new QaOtpListActivity.a(this, d).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    paramMenu.add("Report Errors");
    paramMenu.add("Edit regex");
    return true;
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    AsyncTask localAsyncTask = h;
    if ((localAsyncTask != null) && (!localAsyncTask.isCancelled())) {
      h.cancel(true);
    }
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    StringBuilder localStringBuilder = new StringBuilder("onOptionsItemSelected:: ");
    localStringBuilder.append(paramMenuItem.getTitle());
    localStringBuilder.toString();
    if (paramMenuItem.getTitle().equals("Report Errors"))
    {
      paramMenuItem = new Intent("android.intent.action.SEND");
      paramMenuItem.setType("text/html");
      paramMenuItem.putExtra("android.intent.extra.EMAIL", "supreeth.surendrababu@truecaller.com");
      paramMenuItem.putExtra("android.intent.extra.SUBJECT", "OTP parsing error");
      paramMenuItem.putExtra("android.intent.extra.TEXT", c.b.toString());
      startActivity(Intent.createChooser(paramMenuItem, "Send Email"));
      return true;
    }
    if (paramMenuItem.getTitle().equals("Edit regex"))
    {
      i.setVisibility(0);
      j.setVisibility(0);
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.QaOtpListActivity
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */