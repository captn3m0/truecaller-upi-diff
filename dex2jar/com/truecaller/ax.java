package com.truecaller;

import com.truecaller.featuretoggles.p;
import dagger.a.d;
import javax.inject.Provider;

public final class ax
  implements d<p>
{
  private final c a;
  private final Provider<com.truecaller.abtest.c> b;
  
  private ax(c paramc, Provider<com.truecaller.abtest.c> paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static ax a(c paramc, Provider<com.truecaller.abtest.c> paramProvider)
  {
    return new ax(paramc, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ax
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */