package com.truecaller.util.d;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ShortcutInfo.Builder;
import android.content.pm.ShortcutManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Icon;
import android.os.Build.VERSION;
import android.support.v4.content.a.a.a;
import android.support.v4.graphics.drawable.IconCompat;
import android.text.TextUtils;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.MessagesActivity;
import com.truecaller.payments.BankingActivity;
import com.truecaller.ui.ContactsActivity;
import com.truecaller.ui.TruecallerInit;

public final class b
  implements a
{
  private final Context a;
  
  public b(Context paramContext)
  {
    a = paramContext;
  }
  
  public final void a(int paramInt)
  {
    for (;;)
    {
      int j;
      int i;
      try
      {
        Object localObject3;
        Object localObject4;
        Object localObject1;
        if (android.support.v4.content.a.b.a(a))
        {
          localObject3 = a;
          localObject4 = a;
          if (paramInt == 1)
          {
            localObject1 = "home-shortcut-messages-id";
            localObject4 = new a.a((Context)localObject4, (String)localObject1);
            localObject1 = a;
            j = 2131886117;
            i = j;
          }
        }
        else
        {
          switch (paramInt)
          {
          case 0: 
            localObject1 = ((Context)localObject1).getString(i);
            a.e = ((CharSequence)localObject1);
            localObject1 = a;
            j = 2131689472;
            i = j;
            switch (paramInt)
            {
            case 0: 
              localObject1 = IconCompat.a((Context)localObject1, i);
              a.h = ((IconCompat)localObject1);
              localObject1 = TruecallerInit.class;
              if (paramInt != 1) {
                break label802;
              }
              localObject1 = MessagesActivity.class;
              localObject1 = new Intent(a, (Class)localObject1);
              ((Intent)localObject1).setAction("android.intent.action.MAIN");
              ((Intent)localObject1).addFlags(335544320);
              a.c = new Intent[] { localObject1 };
              ShortcutManager localShortcutManager;
              ShortcutInfo.Builder localBuilder;
              IconCompat localIconCompat;
              if (!TextUtils.isEmpty(a.e)) {
                if ((a.c != null) && (a.c.length != 0))
                {
                  localObject4 = a;
                  if (Build.VERSION.SDK_INT >= 26)
                  {
                    localShortcutManager = (ShortcutManager)((Context)localObject3).getSystemService(ShortcutManager.class);
                    localBuilder = new ShortcutInfo.Builder(a, b).setShortLabel(e).setIntents(c);
                    if (h != null)
                    {
                      localIconCompat = h;
                      paramInt = a;
                      if (paramInt == -1) {}
                    }
                  }
                }
              }
              switch (paramInt)
              {
              case 5: 
                throw new IllegalArgumentException("Unknown type");
                if (Build.VERSION.SDK_INT >= 26) {
                  localObject1 = Icon.createWithAdaptiveBitmap((Bitmap)b);
                } else {
                  localObject1 = Icon.createWithBitmap(IconCompat.a((Bitmap)b, false));
                }
                break;
              case 4: 
                localObject1 = Icon.createWithContentUri((String)b);
                break;
              case 3: 
                localObject1 = Icon.createWithData((byte[])b, e, f);
                break;
              case 2: 
                localObject1 = Icon.createWithResource(localIconCompat.a(), e);
                break;
              case 1: 
                localObject1 = Icon.createWithBitmap((Bitmap)b);
                if (g != null) {
                  ((Icon)localObject1).setTintList(g);
                }
                localObject3 = localObject1;
                if (i != IconCompat.h)
                {
                  ((Icon)localObject1).setTintMode(i);
                  localObject3 = localObject1;
                  continue;
                  localObject3 = (Icon)b;
                }
                localBuilder.setIcon((Icon)localObject3);
                if (!TextUtils.isEmpty(f)) {
                  localBuilder.setLongLabel(f);
                }
                if (!TextUtils.isEmpty(g)) {
                  localBuilder.setDisabledMessage(g);
                }
                if (d != null) {
                  localBuilder.setActivity(d);
                }
                localShortcutManager.requestPinShortcut(localBuilder.build(), null);
                return;
                if (android.support.v4.content.a.b.a((Context)localObject3))
                {
                  ((Context)localObject3).sendBroadcast(((android.support.v4.content.a.a)localObject4).a(new Intent("com.android.launcher.action.INSTALL_SHORTCUT")));
                  return;
                  throw new IllegalArgumentException("Shortcut must have an intent");
                  throw new IllegalArgumentException("Shortcut must have a non-empty label");
                }
                return;
              }
            }
            break;
          }
        }
      }
      catch (RuntimeException localRuntimeException)
      {
        AssertionUtil.reportThrowableButNeverCrash(localRuntimeException);
        return;
      }
      Object localObject2;
      if (paramInt == 2)
      {
        localObject2 = "home-shortcut-contacts-id";
      }
      else if (paramInt == 3)
      {
        localObject2 = "home-shortcut-banking-id";
      }
      else
      {
        localObject2 = "home-shortcut-dialer-id";
        continue;
        i = j;
        continue;
        i = 2131888830;
        continue;
        i = 2131888872;
        continue;
        i = 2131887268;
        continue;
        i = j;
        continue;
        i = 2131234543;
        continue;
        i = 2131689473;
        continue;
        i = 2131689476;
        continue;
        label802:
        if (paramInt == 2) {
          localObject2 = ContactsActivity.class;
        } else if (paramInt == 3) {
          localObject2 = BankingActivity.class;
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.d.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */