package com.truecaller.util;

import android.content.Context;
import com.truecaller.analytics.ae;
import com.truecaller.androidactors.f;
import com.truecaller.common.g.a;
import com.truecaller.i.c;
import javax.inject.Provider;

public final class m
  implements dagger.a.d<l>
{
  private final Provider<Context> a;
  private final Provider<com.truecaller.utils.d> b;
  private final Provider<a> c;
  private final Provider<c> d;
  private final Provider<f<ae>> e;
  
  private m(Provider<Context> paramProvider, Provider<com.truecaller.utils.d> paramProvider1, Provider<a> paramProvider2, Provider<c> paramProvider3, Provider<f<ae>> paramProvider4)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static m a(Provider<Context> paramProvider, Provider<com.truecaller.utils.d> paramProvider1, Provider<a> paramProvider2, Provider<c> paramProvider3, Provider<f<ae>> paramProvider4)
  {
    return new m(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */