package com.truecaller.util.e;

import android.app.Application;
import com.truecaller.ui.o;

public abstract class g
{
  protected final Application d;
  protected final int e;
  
  g(Application paramApplication, int paramInt)
  {
    d = paramApplication;
    e = paramInt;
  }
  
  public static g a(Application paramApplication, int paramInt)
  {
    if (paramInt != 1)
    {
      if (paramInt != 4) {
        return null;
      }
      return new c(paramApplication);
    }
    return new b(paramApplication);
  }
  
  static <T> void a(d<T> paramd, T paramT)
  {
    if (paramd != null) {
      paramd.a(paramT);
    }
  }
  
  public abstract e a(o paramo, f paramf);
  
  final void a(f paramf)
  {
    if (paramf != null) {
      paramf.a(e);
    }
  }
  
  public abstract boolean a();
  
  public abstract void b();
}

/* Location:
 * Qualified Name:     com.truecaller.util.e.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */