package com.truecaller.util;

import com.truecaller.i.c;
import com.truecaller.multisim.h;
import javax.inject.Provider;

public final class ak
  implements dagger.a.d<aj>
{
  private final Provider<al> a;
  private final Provider<com.truecaller.utils.d> b;
  private final Provider<c> c;
  private final Provider<h> d;
  
  private ak(Provider<al> paramProvider, Provider<com.truecaller.utils.d> paramProvider1, Provider<c> paramProvider2, Provider<h> paramProvider3)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static ak a(Provider<al> paramProvider, Provider<com.truecaller.utils.d> paramProvider1, Provider<c> paramProvider2, Provider<h> paramProvider3)
  {
    return new ak(paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ak
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */