package com.truecaller.util;

import android.content.ContentResolver;
import com.truecaller.androidactors.f;
import com.truecaller.callhistory.a;
import com.truecaller.data.access.c;
import com.truecaller.data.access.m;
import com.truecaller.utils.l;
import javax.inject.Provider;

public final class cr
  implements dagger.a.d<aa>
{
  private final Provider<bn> a;
  private final Provider<f<a>> b;
  private final Provider<c> c;
  private final Provider<ContentResolver> d;
  private final Provider<m> e;
  private final Provider<com.truecaller.truepay.d> f;
  private final Provider<l> g;
  
  private cr(Provider<bn> paramProvider, Provider<f<a>> paramProvider1, Provider<c> paramProvider2, Provider<ContentResolver> paramProvider3, Provider<m> paramProvider4, Provider<com.truecaller.truepay.d> paramProvider5, Provider<l> paramProvider6)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
  }
  
  public static cr a(Provider<bn> paramProvider, Provider<f<a>> paramProvider1, Provider<c> paramProvider2, Provider<ContentResolver> paramProvider3, Provider<m> paramProvider4, Provider<com.truecaller.truepay.d> paramProvider5, Provider<l> paramProvider6)
  {
    return new cr(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.cr
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */