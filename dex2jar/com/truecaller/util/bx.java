package com.truecaller.util;

import android.content.Context;
import com.truecaller.messaging.h;
import javax.inject.Provider;

public final class bx
  implements dagger.a.d<bw>
{
  private final Provider<Context> a;
  private final Provider<h> b;
  private final Provider<com.truecaller.utils.d> c;
  
  private bx(Provider<Context> paramProvider, Provider<h> paramProvider1, Provider<com.truecaller.utils.d> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static bx a(Provider<Context> paramProvider, Provider<h> paramProvider1, Provider<com.truecaller.utils.d> paramProvider2)
  {
    return new bx(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bx
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */