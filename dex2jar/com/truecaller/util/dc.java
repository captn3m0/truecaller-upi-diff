package com.truecaller.util;

import android.content.Context;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class dc
  implements d<bd>
{
  private final Provider<Context> a;
  
  private dc(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static bd a(Context paramContext)
  {
    return (bd)g.a(new be(paramContext), "Cannot return null from a non-@Nullable @Provides method");
  }
  
  public static dc a(Provider<Context> paramProvider)
  {
    return new dc(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.dc
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */