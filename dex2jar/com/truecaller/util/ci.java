package com.truecaller.util;

import android.content.ContentResolver;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.utils.extensions.Scheme;
import java.io.File;
import javax.inject.Inject;

public final class ci
  implements ch
{
  private final ContentResolver a;
  
  @Inject
  public ci(ContentResolver paramContentResolver)
  {
    a = paramContentResolver;
  }
  
  public final void a(Uri paramUri)
  {
    k.b(paramUri, "uri");
    String str = paramUri.getScheme();
    if (k.a(str, Scheme.CONTENT.getValue()))
    {
      a.delete(paramUri, null, null);
      return;
    }
    if (k.a(str, Scheme.FILE.getValue()))
    {
      new File(paramUri.getPath()).delete();
      return;
    }
    "URI scheme is not supported for deletion: ".concat(String.valueOf(paramUri));
  }
  
  public final void a(BinaryEntity paramBinaryEntity)
  {
    if (paramBinaryEntity == null) {
      return;
    }
    if (!c) {
      return;
    }
    paramBinaryEntity = b;
    k.a(paramBinaryEntity, "entity.content");
    a(paramBinaryEntity);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ci
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */