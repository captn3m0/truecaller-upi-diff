package com.truecaller.util;

import android.content.Context;
import com.truecaller.util.f.b;
import dagger.a.d;
import javax.inject.Provider;

public final class db
  implements d<b>
{
  private final Provider<Context> a;
  private final Provider<bd> b;
  
  private db(Provider<Context> paramProvider, Provider<bd> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static db a(Provider<Context> paramProvider, Provider<bd> paramProvider1)
  {
    return new db(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.db
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */