package com.truecaller.util;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import com.truecaller.utils.l;
import java.util.Iterator;
import java.util.List;

public final class cg
  implements cf
{
  private final Context b;
  private final l c;
  private final com.truecaller.utils.d d;
  
  public cg(Context paramContext, l paraml, com.truecaller.utils.d paramd)
  {
    b = paramContext;
    c = paraml;
    d = paramd;
  }
  
  @TargetApi(23)
  private final int a(Context paramContext)
  {
    if (!c.a(new String[] { "android.permission.READ_PHONE_STATE" })) {
      return 0;
    }
    Object localObject = paramContext.getSystemService("telecom");
    paramContext = (Context)localObject;
    if (!(localObject instanceof TelecomManager)) {
      paramContext = null;
    }
    paramContext = (TelecomManager)paramContext;
    if (paramContext == null) {
      return 0;
    }
    try
    {
      localObject = paramContext.getCallCapablePhoneAccounts();
      c.g.b.k.a(localObject, "telecomManager.callCapablePhoneAccounts");
      localObject = ((List)localObject).iterator();
      while (((Iterator)localObject).hasNext())
      {
        PhoneAccount localPhoneAccount = paramContext.getPhoneAccount((PhoneAccountHandle)((Iterator)localObject).next());
        if ((localPhoneAccount != null) && (localPhoneAccount.hasCapabilities(8)))
        {
          boolean bool = com.truecaller.common.h.k.g();
          int i = 1;
          if (!bool) {
            return 1;
          }
          if (localPhoneAccount.hasCapabilities(256)) {
            i = 3;
          }
          return i;
        }
      }
      return 0;
    }
    catch (SecurityException paramContext)
    {
      com.truecaller.log.d.a((Throwable)paramContext, "Couldn't get video calling availability");
    }
    return 0;
  }
  
  public final boolean a()
  {
    return (d.h() >= 23) && ((a(b) & 0x1) != 0);
  }
  
  @SuppressLint({"InlinedApi"})
  public final boolean a(int paramInt)
  {
    return (paramInt & 0x1) != 0;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.cg
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */