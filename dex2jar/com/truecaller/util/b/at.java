package com.truecaller.util.b;

import android.content.Context;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.old.data.access.Settings.BuildName;

public final class at
{
  public static j a;
  
  public static j a(Context paramContext)
  {
    paramContext = ((bk)paramContext.getApplicationContext()).a();
    j localj = a;
    if (localj != null) {
      return localj;
    }
    paramContext = Settings.BuildName.toBuildName(paramContext.aI().f());
    if (paramContext == null)
    {
      paramContext = new as();
      a = paramContext;
      return paramContext;
    }
    switch (at.1.a[paramContext.ordinal()])
    {
    default: 
      paramContext = e.a();
      a = paramContext;
      if (paramContext == null) {
        a = new as();
      }
      break;
    case 49: 
      a = new a();
      break;
    case 48: 
      a = new d();
      break;
    case 47: 
      a = new c();
      break;
    case 46: 
      a = new f();
      break;
    case 45: 
      a = new b();
      break;
    case 44: 
      a = new p();
      break;
    case 43: 
      a = new u();
      break;
    case 42: 
      a = new ah();
      break;
    case 41: 
      a = new o();
      break;
    case 40: 
      a = new i();
      break;
    case 39: 
      a = new ak();
      break;
    case 38: 
      a = new aw();
      break;
    case 37: 
      a = new aa();
      break;
    case 36: 
      a = new m();
      break;
    case 35: 
      a = new w();
      break;
    case 34: 
      a = new n();
      break;
    case 33: 
      a = new aq();
      break;
    case 32: 
      a = new q();
      break;
    case 31: 
      a = new y();
      break;
    case 30: 
      a = new x();
      break;
    case 29: 
      a = new an();
      break;
    case 28: 
      a = new ao();
      break;
    case 27: 
      a = new ap();
      break;
    case 26: 
      a = new am();
      break;
    case 25: 
      a = new v();
      break;
    case 24: 
      a = new ac();
      break;
    case 23: 
      a = new au();
      break;
    case 22: 
      a = new av();
      break;
    case 21: 
      a = new g();
      break;
    case 20: 
      a = new t();
      break;
    case 19: 
      a = new k();
      break;
    case 18: 
      a = new ai();
      break;
    case 17: 
      a = new h();
      break;
    case 16: 
      a = new ad();
      break;
    case 15: 
      a = new ae();
      break;
    case 14: 
      a = new al();
      break;
    case 13: 
      a = new ar();
      break;
    case 12: 
      a = new aj();
      break;
    case 11: 
      a = new z();
      break;
    case 9: 
    case 10: 
      a = new af();
      break;
    case 8: 
      a = new s();
      break;
    case 7: 
      a = new r();
      break;
    case 6: 
      a = new l();
      break;
    case 4: 
    case 5: 
      a = new ab();
      break;
    case 1: 
    case 2: 
    case 3: 
      a = new ag();
    }
    return a;
  }
  
  public static j.b b(Context paramContext)
  {
    return a(paramContext).a(paramContext);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.b.at
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */