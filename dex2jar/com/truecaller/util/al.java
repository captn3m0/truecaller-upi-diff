package com.truecaller.util;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.net.Uri;

public abstract interface al
{
  public abstract Uri a(long paramLong, String paramString, boolean paramBoolean);
  
  public abstract Uri a(Intent paramIntent, Integer paramInteger);
  
  public abstract void a(BroadcastReceiver paramBroadcastReceiver);
  
  public abstract void a(BroadcastReceiver paramBroadcastReceiver, String... paramVarArgs);
  
  public abstract void a(Intent paramIntent);
  
  public abstract void a(String paramString1, String paramString2);
  
  public abstract boolean a();
  
  public abstract boolean b();
  
  public abstract boolean b(Intent paramIntent);
  
  public abstract boolean c();
  
  public abstract String d();
  
  public abstract boolean e();
  
  public abstract long f();
  
  public abstract int g();
  
  public abstract boolean h();
  
  public abstract boolean i();
  
  public abstract void j();
  
  public abstract boolean k();
  
  public abstract boolean l();
}

/* Location:
 * Qualified Name:     com.truecaller.util.al
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */