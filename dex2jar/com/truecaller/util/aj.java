package com.truecaller.util;

import c.g.b.k;
import c.n.m;
import c.u;
import com.truecaller.i.c;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.h;
import com.truecaller.utils.d;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import javax.inject.Inject;

public final class aj
  implements bz
{
  private final al a;
  private final d b;
  private final c c;
  private final h d;
  
  @Inject
  public aj(al paramal, d paramd, c paramc, h paramh)
  {
    a = paramal;
    b = paramd;
    c = paramc;
    d = paramh;
  }
  
  public final ca a(String paramString)
  {
    k.b(paramString, "rawInput");
    Object localObject1 = (CharSequence)paramString;
    int i;
    if (((CharSequence)localObject1).length() == 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i == 0)
    {
      boolean bool;
      if (!c.b("hasNativeDialerCallerId")) {
        bool = false;
      } else {
        bool = a.l();
      }
      if (bool) {
        return (ca)new ca.a();
      }
      i = 0;
      while (i < ((CharSequence)localObject1).length())
      {
        int j = ((CharSequence)localObject1).charAt(i);
        if ((j != 35) && (j != 42)) {
          j = 1;
        } else {
          j = 0;
        }
        if (j != 0)
        {
          i = 0;
          break label139;
        }
        i += 1;
      }
      i = 1;
      label139:
      if (i == 0)
      {
        if ((m.b(paramString, "*#*#", false) & m.c(paramString, "#*#*", false)))
        {
          i = paramString.length();
          if (paramString != null)
          {
            paramString = paramString.substring(4, i - 4);
            k.a(paramString, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            return (ca)new ca.d(paramString);
          }
          throw new u("null cannot be cast to non-null type java.lang.String");
        }
        if (((m.b(paramString, "*", false) | m.b(paramString, "#", false)) & m.c(paramString, "#", false)))
        {
          i = paramString.hashCode();
          if (i != 39878404)
          {
            if (i != 39878435) {
              return null;
            }
            if (paramString.equals("*#07#"))
            {
              if (b.h() >= 21) {
                return (ca)new ca.c();
              }
              return null;
            }
          }
          else if (paramString.equals("*#06#"))
          {
            if (b.b()) {
              paramString = "MEID";
            } else {
              paramString = "IMEI";
            }
            localObject1 = d.h();
            k.a(localObject1, "multiSimManager.allSimInfos");
            Object localObject2 = (Iterable)localObject1;
            localObject1 = (Collection)new ArrayList();
            localObject2 = ((Iterable)localObject2).iterator();
            Object localObject3;
            while (((Iterator)localObject2).hasNext())
            {
              localObject3 = nextg;
              if (localObject3 != null) {
                ((Collection)localObject1).add(localObject3);
              }
            }
            localObject2 = (Iterable)localObject1;
            localObject1 = (Collection)new ArrayList();
            localObject2 = ((Iterable)localObject2).iterator();
            while (((Iterator)localObject2).hasNext())
            {
              localObject3 = ((Iterator)localObject2).next();
              if (((CharSequence)localObject3).length() > 0) {
                i = 1;
              } else {
                i = 0;
              }
              if (i != 0) {
                ((Collection)localObject1).add(localObject3);
              }
            }
            localObject1 = ((Collection)localObject1).toArray(new String[0]);
            if (localObject1 != null)
            {
              localObject1 = (String[])localObject1;
              return (ca)new ca.b(paramString, (String[])Arrays.copyOf((Object[])localObject1, localObject1.length));
            }
            throw new u("null cannot be cast to non-null type kotlin.Array<T>");
          }
          return null;
        }
      }
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.aj
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */