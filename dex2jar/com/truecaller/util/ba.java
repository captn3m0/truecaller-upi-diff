package com.truecaller.util;

import android.net.Uri;
import c.n;
import com.truecaller.androidactors.w;
import com.truecaller.messaging.d;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import java.util.Collection;
import java.util.List;

public abstract interface ba
{
  public abstract w<Boolean> a(Uri paramUri);
  
  public abstract w<n<BinaryEntity, az>> a(Uri paramUri, boolean paramBoolean);
  
  public abstract w<n<BinaryEntity, az>> a(Uri paramUri, boolean paramBoolean, long paramLong);
  
  public abstract w<List<n<BinaryEntity, az>>> a(Collection<d> paramCollection, long paramLong);
  
  public abstract w<Boolean> a(List<Uri> paramList);
  
  public abstract w<Boolean> a(Entity[] paramArrayOfEntity);
  
  public abstract w<n<BinaryEntity, az>> b(Uri paramUri, boolean paramBoolean);
}

/* Location:
 * Qualified Name:     com.truecaller.util.ba
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */