package com.truecaller.util;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.support.a.a;
import android.util.DisplayMetrics;
import c.a.j.b;
import c.a.y;
import c.f.e;
import c.g.b.k;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.ImageEntity;
import com.truecaller.utils.extensions.Scheme;
import com.truecaller.utils.extensions.b;
import com.truecaller.utils.extensions.l;
import com.truecaller.utils.extensions.r;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

public final class h
  implements g
{
  private final DisplayMetrics a;
  private final Context b;
  
  public h(Context paramContext)
  {
    b = paramContext;
    paramContext = b.getResources();
    k.a(paramContext, "context.resources");
    a = paramContext.getDisplayMetrics();
  }
  
  private static int a(BitmapFactory.Options paramOptions, int paramInt1, int paramInt2)
  {
    int k = outHeight;
    int m = outWidth;
    int j = 1;
    int i = 1;
    if ((k > paramInt2) || (m > paramInt1))
    {
      k /= 2;
      m /= 2;
      for (;;)
      {
        j = i;
        if (k / i <= paramInt2) {
          break;
        }
        j = i;
        if (m / i <= paramInt1) {
          break;
        }
        i *= 2;
      }
    }
    return j;
  }
  
  private static int a(String paramString)
  {
    int i = new a(paramString).a("Orientation");
    if (i != 3)
    {
      if (i != 6)
      {
        if (i != 8) {
          return 0;
        }
        return 270;
      }
      return 90;
    }
    return 180;
  }
  
  /* Error */
  private static Uri a(String paramString, BitmapFactory.Options paramOptions)
  {
    // Byte code:
    //   0: aload_1
    //   1: iconst_0
    //   2: putfield 81	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
    //   5: aload_1
    //   6: iconst_1
    //   7: putfield 84	android/graphics/BitmapFactory$Options:inSampleSize	I
    //   10: aload_0
    //   11: aload_1
    //   12: invokestatic 90	android/graphics/BitmapFactory:decodeFile	(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   15: astore_2
    //   16: new 92	java/io/FileOutputStream
    //   19: dup
    //   20: aload_0
    //   21: invokespecial 93	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
    //   24: astore_3
    //   25: aload_2
    //   26: aload_1
    //   27: invokestatic 96	com/truecaller/util/h:b	(Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap$CompressFormat;
    //   30: bipush 100
    //   32: aload_3
    //   33: checkcast 98	java/io/OutputStream
    //   36: invokevirtual 104	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   39: pop
    //   40: aload_3
    //   41: invokevirtual 107	java/io/FileOutputStream:flush	()V
    //   44: aload_3
    //   45: invokevirtual 110	java/io/FileOutputStream:close	()V
    //   48: new 112	java/io/File
    //   51: dup
    //   52: aload_0
    //   53: invokespecial 113	java/io/File:<init>	(Ljava/lang/String;)V
    //   56: invokestatic 119	android/net/Uri:fromFile	(Ljava/io/File;)Landroid/net/Uri;
    //   59: astore_0
    //   60: goto +16 -> 76
    //   63: astore_0
    //   64: aload_2
    //   65: invokevirtual 122	android/graphics/Bitmap:recycle	()V
    //   68: aload_3
    //   69: invokevirtual 110	java/io/FileOutputStream:close	()V
    //   72: aload_0
    //   73: athrow
    //   74: aconst_null
    //   75: astore_0
    //   76: aload_2
    //   77: invokevirtual 122	android/graphics/Bitmap:recycle	()V
    //   80: aload_3
    //   81: invokevirtual 110	java/io/FileOutputStream:close	()V
    //   84: aload_0
    //   85: areturn
    //   86: astore_0
    //   87: goto -13 -> 74
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	90	0	paramString	String
    //   0	90	1	paramOptions	BitmapFactory.Options
    //   15	62	2	localBitmap	Bitmap
    //   24	57	3	localFileOutputStream	java.io.FileOutputStream
    // Exception table:
    //   from	to	target	type
    //   25	60	63	finally
    //   25	60	86	java/io/IOException
  }
  
  /* Error */
  private static ImageEntity a(String paramString, BitmapFactory.Options paramOptions, int paramInt)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: iload_2
    //   3: invokestatic 127	com/truecaller/util/h:c	(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;I)Landroid/graphics/Bitmap;
    //   6: astore_3
    //   7: aload_3
    //   8: ifnull +138 -> 146
    //   11: aload_3
    //   12: aload_0
    //   13: invokestatic 128	com/truecaller/util/h:a	(Ljava/lang/String;)I
    //   16: invokestatic 133	com/truecaller/utils/extensions/b:a	(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    //   19: astore_3
    //   20: aload_3
    //   21: ifnonnull +5 -> 26
    //   24: aconst_null
    //   25: areturn
    //   26: new 92	java/io/FileOutputStream
    //   29: dup
    //   30: aload_0
    //   31: invokespecial 93	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
    //   34: astore 4
    //   36: aload_3
    //   37: aload_1
    //   38: invokestatic 96	com/truecaller/util/h:b	(Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap$CompressFormat;
    //   41: bipush 80
    //   43: aload 4
    //   45: checkcast 98	java/io/OutputStream
    //   48: invokevirtual 104	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   51: pop
    //   52: aload 4
    //   54: invokevirtual 107	java/io/FileOutputStream:flush	()V
    //   57: aload 4
    //   59: invokevirtual 110	java/io/FileOutputStream:close	()V
    //   62: new 112	java/io/File
    //   65: dup
    //   66: aload_0
    //   67: invokespecial 113	java/io/File:<init>	(Ljava/lang/String;)V
    //   70: astore_0
    //   71: aload_1
    //   72: getfield 137	android/graphics/BitmapFactory$Options:outMimeType	Ljava/lang/String;
    //   75: aload_0
    //   76: invokestatic 119	android/net/Uri:fromFile	(Ljava/io/File;)Landroid/net/Uri;
    //   79: aload_3
    //   80: invokevirtual 141	android/graphics/Bitmap:getWidth	()I
    //   83: aload_3
    //   84: invokevirtual 144	android/graphics/Bitmap:getHeight	()I
    //   87: iconst_1
    //   88: aload_0
    //   89: invokestatic 149	com/truecaller/utils/extensions/l:a	(Ljava/io/File;)J
    //   92: invokestatic 154	com/truecaller/messaging/data/types/Entity:a	(Ljava/lang/String;Landroid/net/Uri;IIZJ)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   95: astore_1
    //   96: aload_1
    //   97: astore_0
    //   98: aload_1
    //   99: instanceof 156
    //   102: ifne +5 -> 107
    //   105: aconst_null
    //   106: astore_0
    //   107: aload_0
    //   108: checkcast 156	com/truecaller/messaging/data/types/ImageEntity
    //   111: astore_0
    //   112: aload_3
    //   113: invokevirtual 122	android/graphics/Bitmap:recycle	()V
    //   116: aload 4
    //   118: invokevirtual 110	java/io/FileOutputStream:close	()V
    //   121: aload_0
    //   122: areturn
    //   123: astore_0
    //   124: aload_3
    //   125: invokevirtual 122	android/graphics/Bitmap:recycle	()V
    //   128: aload 4
    //   130: invokevirtual 110	java/io/FileOutputStream:close	()V
    //   133: aload_0
    //   134: athrow
    //   135: aload_3
    //   136: invokevirtual 122	android/graphics/Bitmap:recycle	()V
    //   139: aload 4
    //   141: invokevirtual 110	java/io/FileOutputStream:close	()V
    //   144: aconst_null
    //   145: areturn
    //   146: aconst_null
    //   147: areturn
    //   148: astore_0
    //   149: goto -14 -> 135
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	152	0	paramString	String
    //   0	152	1	paramOptions	BitmapFactory.Options
    //   0	152	2	paramInt	int
    //   6	130	3	localBitmap	Bitmap
    //   34	106	4	localFileOutputStream	java.io.FileOutputStream
    // Exception table:
    //   from	to	target	type
    //   36	96	123	finally
    //   98	105	123	finally
    //   107	112	123	finally
    //   36	96	148	java/io/IOException
    //   98	105	148	java/io/IOException
    //   107	112	148	java/io/IOException
  }
  
  private final File a(Bitmap paramBitmap)
  {
    File localFile = e.a("image", ".jpg", b.getCacheDir());
    if (a(paramBitmap, localFile, Bitmap.CompressFormat.PNG)) {
      return localFile;
    }
    return null;
  }
  
  /* Error */
  private static boolean a(Bitmap paramBitmap, File paramFile, Bitmap.CompressFormat paramCompressFormat)
  {
    // Byte code:
    //   0: aload_0
    //   1: ldc -75
    //   3: invokestatic 19	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_1
    //   7: ldc -73
    //   9: invokestatic 19	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   12: aload_2
    //   13: ldc -71
    //   15: invokestatic 19	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   18: new 92	java/io/FileOutputStream
    //   21: dup
    //   22: aload_1
    //   23: invokespecial 188	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   26: astore_1
    //   27: aload_0
    //   28: aload_2
    //   29: bipush 100
    //   31: aload_1
    //   32: checkcast 98	java/io/OutputStream
    //   35: invokevirtual 104	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   38: pop
    //   39: aload_1
    //   40: invokevirtual 107	java/io/FileOutputStream:flush	()V
    //   43: aload_1
    //   44: invokevirtual 110	java/io/FileOutputStream:close	()V
    //   47: iconst_1
    //   48: ireturn
    //   49: astore_0
    //   50: goto +17 -> 67
    //   53: astore_0
    //   54: aload_0
    //   55: checkcast 190	java/lang/Throwable
    //   58: invokestatic 196	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   61: aload_1
    //   62: invokevirtual 110	java/io/FileOutputStream:close	()V
    //   65: iconst_0
    //   66: ireturn
    //   67: aload_1
    //   68: invokevirtual 110	java/io/FileOutputStream:close	()V
    //   71: aload_0
    //   72: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	73	0	paramBitmap	Bitmap
    //   0	73	1	paramFile	File
    //   0	73	2	paramCompressFormat	Bitmap.CompressFormat
    // Exception table:
    //   from	to	target	type
    //   27	43	49	finally
    //   54	61	49	finally
    //   27	43	53	java/io/IOException
  }
  
  private static boolean a(BitmapFactory.Options paramOptions)
  {
    return (outWidth > 1280) || (outHeight > 1280);
  }
  
  private static int b(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    return (int)(((paramInt1 + paramInt2) / 2.0F + (paramInt3 + paramInt4) / 2.0F) / 2.0F) & 0xFF;
  }
  
  private static Bitmap.CompressFormat b(BitmapFactory.Options paramOptions)
  {
    if (outMimeType == null) {
      return Bitmap.CompressFormat.JPEG;
    }
    paramOptions = outMimeType;
    k.a(paramOptions, "outMimeType");
    if (c.n.m.c(paramOptions, "png", false)) {
      return Bitmap.CompressFormat.PNG;
    }
    return Bitmap.CompressFormat.JPEG;
  }
  
  private final Uri b(Bitmap paramBitmap)
  {
    int i;
    if ((paramBitmap.getWidth() <= 1280) && (paramBitmap.getHeight() <= 1280)) {
      i = 0;
    } else {
      i = 1;
    }
    if (i == 0)
    {
      paramBitmap = a(paramBitmap);
      if (paramBitmap == null) {
        return null;
      }
      return Uri.fromFile(paramBitmap);
    }
    paramBitmap = a(paramBitmap);
    if (paramBitmap == null) {
      return null;
    }
    try
    {
      Object localObject = Uri.fromFile(paramBitmap);
      k.a(localObject, "Uri.fromFile(file)");
      localObject = g((Uri)localObject);
      paramBitmap = paramBitmap.getPath();
      k.a(paramBitmap, "file.path");
      return b(paramBitmap, (BitmapFactory.Options)localObject, 1280);
    }
    catch (IOException paramBitmap) {}
    return null;
  }
  
  /* Error */
  private static Uri b(String paramString, BitmapFactory.Options paramOptions, int paramInt)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: iload_2
    //   3: invokestatic 127	com/truecaller/util/h:c	(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;I)Landroid/graphics/Bitmap;
    //   6: astore 4
    //   8: aconst_null
    //   9: astore_3
    //   10: aload 4
    //   12: ifnull +98 -> 110
    //   15: aload 4
    //   17: aload_0
    //   18: invokestatic 128	com/truecaller/util/h:a	(Ljava/lang/String;)I
    //   21: invokestatic 133	com/truecaller/utils/extensions/b:a	(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    //   24: astore 4
    //   26: aload 4
    //   28: ifnonnull +5 -> 33
    //   31: aconst_null
    //   32: areturn
    //   33: new 92	java/io/FileOutputStream
    //   36: dup
    //   37: aload_0
    //   38: invokespecial 93	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
    //   41: astore 5
    //   43: aload 4
    //   45: aload_1
    //   46: invokestatic 96	com/truecaller/util/h:b	(Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap$CompressFormat;
    //   49: bipush 80
    //   51: aload 5
    //   53: checkcast 98	java/io/OutputStream
    //   56: invokevirtual 104	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   59: pop
    //   60: aload 5
    //   62: invokevirtual 107	java/io/FileOutputStream:flush	()V
    //   65: aload 5
    //   67: invokevirtual 110	java/io/FileOutputStream:close	()V
    //   70: new 112	java/io/File
    //   73: dup
    //   74: aload_0
    //   75: invokespecial 113	java/io/File:<init>	(Ljava/lang/String;)V
    //   78: invokestatic 119	android/net/Uri:fromFile	(Ljava/io/File;)Landroid/net/Uri;
    //   81: astore_0
    //   82: goto +16 -> 98
    //   85: astore_0
    //   86: aload 4
    //   88: invokevirtual 122	android/graphics/Bitmap:recycle	()V
    //   91: aload 5
    //   93: invokevirtual 110	java/io/FileOutputStream:close	()V
    //   96: aload_0
    //   97: athrow
    //   98: aload 4
    //   100: invokevirtual 122	android/graphics/Bitmap:recycle	()V
    //   103: aload 5
    //   105: invokevirtual 110	java/io/FileOutputStream:close	()V
    //   108: aload_0
    //   109: areturn
    //   110: aconst_null
    //   111: areturn
    //   112: astore_0
    //   113: aload_3
    //   114: astore_0
    //   115: goto -17 -> 98
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	118	0	paramString	String
    //   0	118	1	paramOptions	BitmapFactory.Options
    //   0	118	2	paramInt	int
    //   9	105	3	localObject	Object
    //   6	93	4	localBitmap	Bitmap
    //   41	63	5	localFileOutputStream	java.io.FileOutputStream
    // Exception table:
    //   from	to	target	type
    //   43	82	85	finally
    //   43	82	112	java/io/IOException
  }
  
  private static Bitmap c(String paramString, BitmapFactory.Options paramOptions, int paramInt)
  {
    inJustDecodeBounds = true;
    BitmapFactory.decodeFile(paramString, paramOptions);
    int i = outWidth;
    int j = outHeight;
    float f1 = i;
    float f3 = paramInt;
    float f4 = f1 / f3;
    float f2 = j;
    f3 = Math.max(f4, f2 / f3);
    paramInt = Math.max(1, (int)(f1 / f3));
    i = Math.max(1, (int)(f2 / f3));
    inJustDecodeBounds = false;
    inSampleSize = a(paramOptions, paramInt, i);
    paramString = BitmapFactory.decodeFile(paramString, paramOptions);
    k.a(paramString, "BitmapFactory.decodeFile(path, options)");
    if ((paramString.getWidth() == paramInt) && (paramString.getHeight() == i)) {
      return paramString;
    }
    paramOptions = Bitmap.createScaledBitmap(paramString, paramInt, i, true);
    k.a(paramOptions, "Bitmap.createScaledBitma…idth, targetHeight, true)");
    paramString.recycle();
    return paramOptions;
  }
  
  /* Error */
  private final BitmapFactory.Options g(Uri paramUri)
  {
    // Byte code:
    //   0: new 57	android/graphics/BitmapFactory$Options
    //   3: dup
    //   4: invokespecial 246	android/graphics/BitmapFactory$Options:<init>	()V
    //   7: astore_2
    //   8: aload_2
    //   9: iconst_1
    //   10: putfield 81	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
    //   13: aload_1
    //   14: invokestatic 251	com/truecaller/utils/extensions/r:a	(Landroid/net/Uri;)Lcom/truecaller/utils/extensions/Scheme;
    //   17: astore_3
    //   18: aload_3
    //   19: ifnull +97 -> 116
    //   22: getstatic 256	com/truecaller/util/i:a	[I
    //   25: aload_3
    //   26: invokevirtual 261	com/truecaller/utils/extensions/Scheme:ordinal	()I
    //   29: iaload
    //   30: tableswitch	default:+22->52, 1:+75->105, 2:+25->55
    //   52: goto +64 -> 116
    //   55: aload_0
    //   56: getfield 24	com/truecaller/util/h:b	Landroid/content/Context;
    //   59: invokevirtual 265	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   62: aload_1
    //   63: invokevirtual 271	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   66: checkcast 273	java/io/Closeable
    //   69: astore_3
    //   70: aconst_null
    //   71: astore_1
    //   72: aload_3
    //   73: checkcast 275	java/io/InputStream
    //   76: aconst_null
    //   77: aload_2
    //   78: invokestatic 279	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   81: pop
    //   82: aload_3
    //   83: aconst_null
    //   84: invokestatic 284	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   87: aload_2
    //   88: areturn
    //   89: astore_2
    //   90: goto +8 -> 98
    //   93: astore_2
    //   94: aload_2
    //   95: astore_1
    //   96: aload_2
    //   97: athrow
    //   98: aload_3
    //   99: aload_1
    //   100: invokestatic 284	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   103: aload_2
    //   104: athrow
    //   105: aload_1
    //   106: invokevirtual 285	android/net/Uri:getPath	()Ljava/lang/String;
    //   109: aload_2
    //   110: invokestatic 90	android/graphics/BitmapFactory:decodeFile	(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   113: pop
    //   114: aload_2
    //   115: areturn
    //   116: new 287	java/lang/StringBuilder
    //   119: dup
    //   120: ldc_w 289
    //   123: invokespecial 290	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   126: astore_2
    //   127: aload_2
    //   128: aload_1
    //   129: invokevirtual 293	android/net/Uri:getScheme	()Ljava/lang/String;
    //   132: invokevirtual 297	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   135: pop
    //   136: aload_2
    //   137: ldc_w 299
    //   140: invokevirtual 297	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   143: pop
    //   144: new 301	java/lang/IllegalArgumentException
    //   147: dup
    //   148: aload_2
    //   149: invokevirtual 304	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   152: invokespecial 305	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   155: checkcast 190	java/lang/Throwable
    //   158: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	159	0	this	h
    //   0	159	1	paramUri	Uri
    //   7	81	2	localOptions	BitmapFactory.Options
    //   89	1	2	localObject1	Object
    //   93	22	2	localThrowable	Throwable
    //   126	23	2	localStringBuilder	StringBuilder
    //   17	82	3	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   72	82	89	finally
    //   96	98	89	finally
    //   72	82	93	java/lang/Throwable
  }
  
  public final int a()
  {
    return (int)(Math.min(a.widthPixels, a.heightPixels) * 0.667F);
  }
  
  public final Bitmap a(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3)
  {
    int i = paramInt1;
    k.b(paramArrayOfByte, "thumbnail");
    int j = paramArrayOfByte.length;
    int k = 0;
    if (j == 0) {
      j = 1;
    } else {
      j = 0;
    }
    if (j != 0) {
      return null;
    }
    float f1 = Math.max(paramInt1, paramInt2) / paramArrayOfByte[0];
    float f2 = i;
    j = (int)(f2 / f1);
    float f3 = paramInt2;
    paramInt1 = (int)(f3 / f1);
    if (paramArrayOfByte.length != j * paramInt1 * 2 + 1) {
      return null;
    }
    Object localObject = new cl(j, paramInt1);
    k.b(paramArrayOfByte, "receiver$0");
    if (paramArrayOfByte.length == 0) {
      paramInt1 = 1;
    } else {
      paramInt1 = 0;
    }
    if (paramInt1 != 0) {
      paramArrayOfByte = (Iterable)y.a;
    } else {
      paramArrayOfByte = (Iterable)new j.b(paramArrayOfByte);
    }
    paramArrayOfByte = (Iterable)c.a.m.c(paramArrayOfByte, 1);
    k.b(paramArrayOfByte, "src");
    paramArrayOfByte = paramArrayOfByte.iterator();
    int m = a;
    int n = b;
    paramInt1 = k;
    while (paramInt1 < m * n)
    {
      k = (((Number)paramArrayOfByte.next()).byteValue() & 0xFF) << 8 | ((Number)paramArrayOfByte.next()).byteValue() & 0xFF;
      c[paramInt1] = ((k & 0x1F) * 527 + 23 >> 6 | (k >> 11 & 0x1F) * 527 + 23 >> 6 << 16 | 0xFF000000 | (k >> 5 & 0x3F) * 259 + 23 >> 6 << 8);
      paramInt1 += 1;
    }
    paramInt1 = a();
    if ((i < paramInt1) && (paramInt2 < paramInt1))
    {
      paramInt1 = paramInt2;
      paramInt2 = i;
    }
    else
    {
      f1 = f2 / f3;
      if (f1 > 1.0F)
      {
        i = (int)(paramInt1 / f1);
        paramInt2 = paramInt1;
        paramInt1 = i;
      }
      else
      {
        paramInt2 = (int)(paramInt1 * f1);
      }
    }
    paramArrayOfByte = Bitmap.createBitmap(paramInt2, paramInt1, Bitmap.Config.ARGB_8888);
    cl localcl = new cl(paramInt2, paramInt1);
    paramInt1 = Math.max(3, paramInt3 / 2 * 2 + 1);
    f1 = paramInt2 / j;
    k.b(localObject, "src");
    bq localbq = (bq)new cl.c((cl)localObject, f1);
    localObject = (bp)localcl;
    localcl.a(paramInt1, localbq, (bp)localObject);
    localbq = (bq)localcl;
    localcl.b(paramInt1, localbq, (bp)localObject);
    k.a(paramArrayOfByte, "img");
    k.b(paramArrayOfByte, "out");
    localcl.a(paramInt1, localbq, (bp)localObject);
    localcl.b(paramInt1, localbq, (bp)new cl.b(paramArrayOfByte));
    return paramArrayOfByte;
  }
  
  public final ImageEntity a(Uri paramUri)
  {
    k.b(paramUri, "uri");
    return a(paramUri, 1280);
  }
  
  public final ImageEntity a(Uri paramUri, int paramInt)
  {
    k.b(paramUri, "uri");
    try
    {
      Object localObject1 = g(paramUri);
      paramUri = r.a(paramUri, b, null);
      if (paramUri == null) {
        return null;
      }
      Object localObject2 = paramUri.getPath();
      k.a(localObject2, "file.path");
      if (!a((BitmapFactory.Options)localObject1))
      {
        paramInt = a((String)localObject2);
        if (paramInt != 0)
        {
          localObject2 = BitmapFactory.decodeFile((String)localObject2);
          if (localObject2 != null)
          {
            localObject2 = b.a((Bitmap)localObject2, paramInt);
            if (localObject2 == null) {
              return null;
            }
            if (!a((Bitmap)localObject2, paramUri, b((BitmapFactory.Options)localObject1))) {
              return null;
            }
          }
          else
          {
            return null;
          }
        }
        localObject1 = Entity.a(outMimeType, Uri.fromFile(paramUri), outWidth, outHeight, true, l.a(paramUri));
        paramUri = (Uri)localObject1;
        if (!(localObject1 instanceof ImageEntity)) {
          paramUri = null;
        }
        return (ImageEntity)paramUri;
      }
      return a((String)localObject2, (BitmapFactory.Options)localObject1, paramInt);
    }
    catch (Exception paramUri) {}
    return null;
  }
  
  /* Error */
  public final byte[] b(Uri paramUri)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 408
    //   4: invokestatic 19	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   7: aload_0
    //   8: getfield 24	com/truecaller/util/h:b	Landroid/content/Context;
    //   11: invokevirtual 265	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   14: aload_1
    //   15: invokevirtual 271	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   18: checkcast 273	java/io/Closeable
    //   21: astore_3
    //   22: aload_3
    //   23: checkcast 275	java/io/InputStream
    //   26: invokestatic 427	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    //   29: astore_1
    //   30: aload_3
    //   31: aconst_null
    //   32: invokestatic 284	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   35: aload_1
    //   36: new 429	com/truecaller/util/h$a
    //   39: dup
    //   40: aload_0
    //   41: invokespecial 432	com/truecaller/util/h$a:<init>	(Lcom/truecaller/util/h;)V
    //   44: checkcast 434	c/g/a/b
    //   47: invokestatic 437	com/truecaller/utils/extensions/b:a	(Landroid/graphics/Bitmap;Lc/g/a/b;)Ljava/lang/Object;
    //   50: checkcast 439	[B
    //   53: astore_1
    //   54: aload_1
    //   55: areturn
    //   56: astore_2
    //   57: aconst_null
    //   58: astore_1
    //   59: goto +7 -> 66
    //   62: astore_1
    //   63: aload_1
    //   64: athrow
    //   65: astore_2
    //   66: aload_3
    //   67: aload_1
    //   68: invokestatic 284	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   71: aload_2
    //   72: athrow
    //   73: astore_1
    //   74: aload_1
    //   75: checkcast 190	java/lang/Throwable
    //   78: invokestatic 196	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   81: aconst_null
    //   82: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	83	0	this	h
    //   0	83	1	paramUri	Uri
    //   56	1	2	localObject1	Object
    //   65	7	2	localObject2	Object
    //   21	46	3	localCloseable	java.io.Closeable
    // Exception table:
    //   from	to	target	type
    //   22	30	56	finally
    //   22	30	62	java/lang/Throwable
    //   63	65	65	finally
    //   7	22	73	java/io/IOException
    //   30	54	73	java/io/IOException
    //   66	73	73	java/io/IOException
  }
  
  /* Error */
  public final Uri c(Uri paramUri)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 408
    //   4: invokestatic 19	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   7: new 444	android/media/MediaMetadataRetriever
    //   10: dup
    //   11: invokespecial 445	android/media/MediaMetadataRetriever:<init>	()V
    //   14: astore 4
    //   16: aconst_null
    //   17: astore_3
    //   18: aload 4
    //   20: aload_0
    //   21: getfield 24	com/truecaller/util/h:b	Landroid/content/Context;
    //   24: aload_1
    //   25: invokevirtual 449	android/media/MediaMetadataRetriever:setDataSource	(Landroid/content/Context;Landroid/net/Uri;)V
    //   28: aload 4
    //   30: invokevirtual 453	android/media/MediaMetadataRetriever:getFrameAtTime	()Landroid/graphics/Bitmap;
    //   33: astore_1
    //   34: aload_1
    //   35: ldc -75
    //   37: invokestatic 34	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   40: aload_0
    //   41: aload_1
    //   42: invokespecial 455	com/truecaller/util/h:b	(Landroid/graphics/Bitmap;)Landroid/net/Uri;
    //   45: astore_2
    //   46: aload 4
    //   48: invokevirtual 458	android/media/MediaMetadataRetriever:release	()V
    //   51: aload_1
    //   52: invokevirtual 122	android/graphics/Bitmap:recycle	()V
    //   55: aload_2
    //   56: areturn
    //   57: astore_2
    //   58: goto +6 -> 64
    //   61: astore_2
    //   62: aload_3
    //   63: astore_1
    //   64: aload 4
    //   66: invokevirtual 458	android/media/MediaMetadataRetriever:release	()V
    //   69: aload_1
    //   70: ifnull +7 -> 77
    //   73: aload_1
    //   74: invokevirtual 122	android/graphics/Bitmap:recycle	()V
    //   77: aload_2
    //   78: athrow
    //   79: aconst_null
    //   80: astore_1
    //   81: aload 4
    //   83: invokevirtual 458	android/media/MediaMetadataRetriever:release	()V
    //   86: aload_1
    //   87: ifnull +21 -> 108
    //   90: goto +14 -> 104
    //   93: aconst_null
    //   94: astore_1
    //   95: aload 4
    //   97: invokevirtual 458	android/media/MediaMetadataRetriever:release	()V
    //   100: aload_1
    //   101: ifnull +7 -> 108
    //   104: aload_1
    //   105: invokevirtual 122	android/graphics/Bitmap:recycle	()V
    //   108: aconst_null
    //   109: areturn
    //   110: astore_1
    //   111: goto -18 -> 93
    //   114: astore_1
    //   115: goto -36 -> 79
    //   118: astore_2
    //   119: goto -24 -> 95
    //   122: astore_2
    //   123: goto -42 -> 81
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	126	0	this	h
    //   0	126	1	paramUri	Uri
    //   45	11	2	localUri	Uri
    //   57	1	2	localObject1	Object
    //   61	17	2	localObject2	Object
    //   118	1	2	localIllegalArgumentException	IllegalArgumentException
    //   122	1	2	localSecurityException	SecurityException
    //   17	46	3	localObject3	Object
    //   14	82	4	localMediaMetadataRetriever	android.media.MediaMetadataRetriever
    // Exception table:
    //   from	to	target	type
    //   34	46	57	finally
    //   18	34	61	finally
    //   18	34	110	java/lang/IllegalArgumentException
    //   18	34	114	java/lang/SecurityException
    //   34	46	118	java/lang/IllegalArgumentException
    //   34	46	122	java/lang/SecurityException
  }
  
  public final Uri d(Uri paramUri)
  {
    k.b(paramUri, "uri");
    try
    {
      BitmapFactory.Options localOptions = g(paramUri);
      paramUri = r.a(paramUri, b, ".jpg");
      if (paramUri == null) {
        return null;
      }
      paramUri = paramUri.getPath();
      k.a(paramUri, "file.path");
      if (!a(localOptions)) {
        return a(paramUri, localOptions);
      }
      return b(paramUri, localOptions, 640);
    }
    catch (Exception paramUri) {}
    return null;
  }
  
  public final boolean e(Uri paramUri)
  {
    if (paramUri != null)
    {
      Context localContext = b;
      k.b(paramUri, "receiver$0");
      k.b(localContext, "context");
      Scheme localScheme = r.a(paramUri);
      if (localScheme != null) {
        switch (com.truecaller.utils.extensions.s.d[localScheme.ordinal()])
        {
        default: 
          break;
        case 2: 
          bool = new File(paramUri.getPath()).delete();
          break;
        case 1: 
          if (localContext.getContentResolver().delete(paramUri, null, null) == 1) {
            bool = true;
          } else {
            bool = false;
          }
          break;
        }
      }
      boolean bool = false;
      if (bool == true) {
        return true;
      }
    }
    return false;
  }
  
  /* Error */
  public final Uri f(Uri paramUri)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 408
    //   4: invokestatic 19	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   7: aload_1
    //   8: invokevirtual 285	android/net/Uri:getPath	()Ljava/lang/String;
    //   11: invokestatic 421	android/graphics/BitmapFactory:decodeFile	(Ljava/lang/String;)Landroid/graphics/Bitmap;
    //   14: astore 4
    //   16: aload 4
    //   18: ifnull +29 -> 47
    //   21: aload 4
    //   23: sipush 640
    //   26: sipush 640
    //   29: invokestatic 478	com/truecaller/utils/extensions/b:b	(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    //   32: astore_3
    //   33: goto +16 -> 49
    //   36: astore_1
    //   37: aconst_null
    //   38: astore_3
    //   39: goto +85 -> 124
    //   42: aconst_null
    //   43: astore_3
    //   44: goto +106 -> 150
    //   47: aconst_null
    //   48: astore_3
    //   49: aload_3
    //   50: ifnull +139 -> 189
    //   53: aload_3
    //   54: new 112	java/io/File
    //   57: dup
    //   58: aload_1
    //   59: invokevirtual 285	android/net/Uri:getPath	()Ljava/lang/String;
    //   62: invokespecial 113	java/io/File:<init>	(Ljava/lang/String;)V
    //   65: getstatic 200	android/graphics/Bitmap$CompressFormat:JPEG	Landroid/graphics/Bitmap$CompressFormat;
    //   68: invokestatic 179	com/truecaller/util/h:a	(Landroid/graphics/Bitmap;Ljava/io/File;Landroid/graphics/Bitmap$CompressFormat;)Z
    //   71: invokestatic 484	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   74: astore 5
    //   76: goto +3 -> 79
    //   79: aload 5
    //   81: getstatic 488	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   84: invokestatic 491	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/Object;)Z
    //   87: istore_2
    //   88: iload_2
    //   89: ifeq +6 -> 95
    //   92: goto +5 -> 97
    //   95: aconst_null
    //   96: astore_1
    //   97: aload 4
    //   99: ifnull +8 -> 107
    //   102: aload 4
    //   104: invokevirtual 122	android/graphics/Bitmap:recycle	()V
    //   107: aload_3
    //   108: ifnull +7 -> 115
    //   111: aload_3
    //   112: invokevirtual 122	android/graphics/Bitmap:recycle	()V
    //   115: aload_1
    //   116: areturn
    //   117: astore_1
    //   118: aconst_null
    //   119: astore 4
    //   121: aload 4
    //   123: astore_3
    //   124: aload 4
    //   126: ifnull +8 -> 134
    //   129: aload 4
    //   131: invokevirtual 122	android/graphics/Bitmap:recycle	()V
    //   134: aload_3
    //   135: ifnull +7 -> 142
    //   138: aload_3
    //   139: invokevirtual 122	android/graphics/Bitmap:recycle	()V
    //   142: aload_1
    //   143: athrow
    //   144: aconst_null
    //   145: astore 4
    //   147: aload 4
    //   149: astore_3
    //   150: aload 4
    //   152: ifnull +8 -> 160
    //   155: aload 4
    //   157: invokevirtual 122	android/graphics/Bitmap:recycle	()V
    //   160: aload_3
    //   161: ifnull +7 -> 168
    //   164: aload_3
    //   165: invokevirtual 122	android/graphics/Bitmap:recycle	()V
    //   168: aconst_null
    //   169: areturn
    //   170: astore_1
    //   171: goto -27 -> 144
    //   174: astore_1
    //   175: goto -133 -> 42
    //   178: astore_1
    //   179: goto +7 -> 186
    //   182: astore_1
    //   183: goto -59 -> 124
    //   186: goto -36 -> 150
    //   189: aconst_null
    //   190: astore 5
    //   192: goto -113 -> 79
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	195	0	this	h
    //   0	195	1	paramUri	Uri
    //   87	2	2	bool	boolean
    //   32	133	3	localBitmap1	Bitmap
    //   14	142	4	localBitmap2	Bitmap
    //   74	117	5	localBoolean	Boolean
    // Exception table:
    //   from	to	target	type
    //   21	33	36	finally
    //   7	16	117	finally
    //   7	16	170	java/lang/IllegalArgumentException
    //   21	33	174	java/lang/IllegalArgumentException
    //   53	76	178	java/lang/IllegalArgumentException
    //   79	88	178	java/lang/IllegalArgumentException
    //   53	76	182	finally
    //   79	88	182	finally
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */