package com.truecaller.util;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.net.Uri.Builder;
import android.provider.ContactsContract.Contacts;
import com.android.b.a.b;
import com.android.b.j;
import com.android.b.n;
import com.android.b.o;
import com.truecaller.androidactors.w;
import com.truecaller.common.h.am;
import com.truecaller.content.TruecallerContract.ah;
import com.truecaller.data.access.m;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.truepay.SenderInfo;
import com.truecaller.truepay.d;
import com.truecaller.utils.l;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.c.a.a.a.k;

final class ac
  implements aa
{
  private final bn a;
  private final com.truecaller.androidactors.f<com.truecaller.callhistory.a> b;
  private final com.truecaller.data.access.c c;
  private final m d;
  private final ContentResolver e;
  private final d f;
  private final l g;
  
  ac(bn parambn, com.truecaller.androidactors.f<com.truecaller.callhistory.a> paramf, com.truecaller.data.access.c paramc, ContentResolver paramContentResolver, m paramm, d paramd, l paraml)
  {
    a = parambn;
    b = paramf;
    c = paramc;
    e = paramContentResolver;
    d = paramm;
    f = paramd;
    g = paraml;
  }
  
  /* Error */
  private void a(Uri paramUri, com.android.b.f paramf)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: aconst_null
    //   4: astore 5
    //   6: aconst_null
    //   7: astore_3
    //   8: aload_0
    //   9: getfield 34	com/truecaller/util/ac:e	Landroid/content/ContentResolver;
    //   12: aload_1
    //   13: invokevirtual 54	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   16: astore 6
    //   18: aload 6
    //   20: astore_3
    //   21: aload 6
    //   23: astore 4
    //   25: aload 6
    //   27: astore 5
    //   29: aload_2
    //   30: aload 6
    //   32: invokevirtual 59	com/android/b/f:a	(Ljava/io/InputStream;)V
    //   35: aload 6
    //   37: invokestatic 64	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   40: return
    //   41: astore_1
    //   42: goto +83 -> 125
    //   45: astore_2
    //   46: goto +8 -> 54
    //   49: astore_2
    //   50: aload 5
    //   52: astore 4
    //   54: aload 4
    //   56: astore_3
    //   57: new 66	java/lang/StringBuilder
    //   60: dup
    //   61: ldc 68
    //   63: invokespecial 71	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   66: astore 5
    //   68: aload 4
    //   70: astore_3
    //   71: aload 5
    //   73: aload_1
    //   74: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   77: pop
    //   78: aload 4
    //   80: astore_3
    //   81: aload 5
    //   83: ldc 77
    //   85: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   88: pop
    //   89: aload 4
    //   91: astore_3
    //   92: aload 5
    //   94: aload_2
    //   95: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   98: pop
    //   99: aload 4
    //   101: astore_3
    //   102: aload 5
    //   104: ldc 82
    //   106: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   109: pop
    //   110: aload 4
    //   112: astore_3
    //   113: aload 5
    //   115: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   118: pop
    //   119: aload 4
    //   121: invokestatic 64	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   124: return
    //   125: aload_3
    //   126: invokestatic 64	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   129: aload_1
    //   130: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	131	0	this	ac
    //   0	131	1	paramUri	Uri
    //   0	131	2	paramf	com.android.b.f
    //   7	119	3	localObject1	Object
    //   1	119	4	localObject2	Object
    //   4	110	5	localObject3	Object
    //   16	20	6	localInputStream	java.io.InputStream
    // Exception table:
    //   from	to	target	type
    //   8	18	41	finally
    //   29	35	41	finally
    //   57	68	41	finally
    //   71	78	41	finally
    //   81	89	41	finally
    //   92	99	41	finally
    //   102	110	41	finally
    //   113	119	41	finally
    //   8	18	45	java/io/IOException
    //   29	35	45	java/io/IOException
    //   8	18	49	com/android/b/a/b
    //   29	35	49	com/android/b/a/b
  }
  
  public final w<Boolean> a()
  {
    a.a();
    return w.b(Boolean.TRUE);
  }
  
  public final w<Contact> a(long paramLong)
  {
    return w.b(c.a(paramLong));
  }
  
  public final w<Uri> a(Uri paramUri)
  {
    return w.b(a.a(paramUri));
  }
  
  public final w<Contact> a(String paramString)
  {
    return w.b(c.b(paramString));
  }
  
  public final w<Map<Uri, x>> a(List<Uri> paramList)
  {
    if (paramList.isEmpty()) {
      return w.b(null);
    }
    dd localdd = new dd();
    com.android.b.c localc = new com.android.b.c();
    a.add(localdd);
    n localn = new n();
    j localj = new j();
    localj.a(localn);
    android.support.v4.f.a locala = new android.support.v4.f.a(paramList.size());
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      Uri localUri = (Uri)paramList.next();
      if (localUri != null)
      {
        a(localUri, localj);
        int i;
        switch (a)
        {
        default: 
          if (b == 0) {
            i = -1073741824;
          }
          break;
        case 3: 
          i = 939524104;
          break;
        case 2: 
          i = 402653192;
          break;
        }
        if (b == 1) {
          i = -1073741823;
        } else if (b == 2) {
          i = -1073741822;
        } else {
          i = 0;
        }
        try
        {
          Object localObject = o.b(i);
          ((com.android.b.f)localObject).a(localc);
          a(localUri, (com.android.b.f)localObject);
          localObject = a;
          if (localObject != null)
          {
            a = localUri;
            if (((x)localObject).a()) {
              locala.put(localUri, localObject);
            }
          }
        }
        catch (b localb)
        {
          StringBuilder localStringBuilder = new StringBuilder("Unable to read vcard from ");
          localStringBuilder.append(localUri);
          localStringBuilder.append(" (");
          localStringBuilder.append(localb);
          localStringBuilder.append(")");
          localStringBuilder.toString();
        }
      }
    }
    return w.b(locala);
  }
  
  public final void a(HistoryEvent paramHistoryEvent)
  {
    if ((f != null) && (f.S()))
    {
      ((com.truecaller.callhistory.a)b.a()).a(paramHistoryEvent, f).c();
      return;
    }
    ((com.truecaller.callhistory.a)b.a()).a(paramHistoryEvent);
  }
  
  public final w<String> b(Uri paramUri)
  {
    String str = null;
    if (paramUri == null) {
      return w.b(null);
    }
    if (!g.a(new String[] { "android.permission.READ_CONTACTS" })) {
      return w.b(null);
    }
    Cursor localCursor = e.query(paramUri, new String[] { "display_name", "data1" }, null, null, null);
    paramUri = str;
    if (localCursor != null)
    {
      paramUri = str;
      try
      {
        if (localCursor.moveToFirst())
        {
          paramUri = localCursor.getString(0);
          str = localCursor.getString(1);
          StringBuilder localStringBuilder = new StringBuilder();
          boolean bool2 = am.a(new CharSequence[] { paramUri });
          boolean bool1 = k.b(new CharSequence[] { str }) ^ true;
          if (bool2)
          {
            localStringBuilder.append(paramUri);
            if (bool1) {
              localStringBuilder.append(" (");
            }
          }
          if (bool1)
          {
            localStringBuilder.append(str);
            if (bool2) {
              localStringBuilder.append(")");
            }
          }
          paramUri = localStringBuilder.toString();
        }
      }
      finally
      {
        q.a(localCursor);
      }
    }
    q.a(localCursor);
    return w.b(paramUri);
  }
  
  public final w<Contact> b(String paramString)
  {
    com.truecaller.data.access.c localc = c;
    return w.b(localc.a(localc.a(TruecallerContract.ah.a().buildUpon().appendQueryParameter("limit", "1").build(), "aggregated_contact_id", "contact_im_id=?", new String[] { paramString })));
  }
  
  public final w<x> c(Uri paramUri)
  {
    Object localObject = null;
    if (paramUri == null) {
      return w.b(null);
    }
    if (!g.a(new String[] { "android.permission.READ_CONTACTS" })) {
      return w.b(null);
    }
    Cursor localCursor = e.query(paramUri, new String[] { "lookup", "display_name" }, null, null, null);
    paramUri = (Uri)localObject;
    if (localCursor != null)
    {
      paramUri = (Uri)localObject;
      try
      {
        if (localCursor.moveToFirst())
        {
          String str = localCursor.getString(0);
          paramUri = (Uri)localObject;
          if (am.a(new CharSequence[] { str }))
          {
            paramUri = new x();
            a = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_VCARD_URI, str);
            b = localCursor.getString(1);
            paramUri.a(1);
          }
        }
      }
      finally
      {
        q.a(localCursor);
      }
    }
    q.a(localCursor);
    return w.b(paramUri);
  }
  
  public final w<Boolean> c(String paramString)
  {
    return w.b(Boolean.valueOf(d.a(paramString, new int[] { 1, 4, 8 })));
  }
  
  public final w<SenderInfo> d(String paramString)
  {
    return w.b(f.a(paramString));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ac
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */