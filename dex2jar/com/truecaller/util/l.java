package com.truecaller.util;

import android.content.Context;
import android.content.pm.PackageManager;
import c.a.ag;
import c.g.a.b;
import c.g.b.k;
import c.m.i;
import c.t;
import com.truecaller.analytics.ae;
import com.truecaller.common.g.a;
import com.truecaller.common.h.au;
import com.truecaller.log.AssertionUtil;
import com.truecaller.old.data.access.Settings.BuildName;
import com.truecaller.tracking.events.ao;
import com.truecaller.tracking.events.ao.a;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;

public final class l
  implements com.truecaller.common.h.c
{
  private final String a;
  private final boolean b;
  private final Context c;
  private final com.truecaller.utils.d d;
  private final a e;
  private final com.truecaller.i.c f;
  private final com.truecaller.androidactors.f<ae> g;
  
  @Inject
  public l(Context paramContext, com.truecaller.utils.d paramd, a parama, com.truecaller.i.c paramc, com.truecaller.androidactors.f<ae> paramf)
  {
    c = paramContext;
    d = paramd;
    e = parama;
    f = paramc;
    g = paramf;
    try
    {
      paramd = c.getPackageManager().getInstallerPackageName(c.getPackageName());
      paramContext = paramd;
      if (paramd == null) {
        paramContext = "";
      }
    }
    catch (Exception paramContext)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramContext);
      paramContext = null;
    }
    a = paramContext;
    boolean bool2 = k.a(a, "com.android.vending");
    boolean bool1 = false;
    if (!bool2)
    {
      paramContext = (CharSequence)a;
      int i;
      if ((paramContext != null) && (paramContext.length() != 0)) {
        i = 0;
      } else {
        i = 1;
      }
      if (i == 0) {}
    }
    else
    {
      bool1 = true;
    }
    b = bool1;
  }
  
  private final String a(String paramString)
  {
    PackageManager localPackageManager = c.getPackageManager();
    k.a(localPackageManager, "context.packageManager");
    Object localObject1 = i();
    Object localObject2 = (CharSequence)localObject1;
    int k = 0;
    int i;
    if ((localObject2 != null) && (!c.n.m.a((CharSequence)localObject2))) {
      i = 0;
    } else {
      i = 1;
    }
    if (i != 0)
    {
      localObject2 = "GOOGLE_PLAY";
      localObject1 = (CharSequence)a;
      if ((localObject1 != null) && (!c.n.m.a((CharSequence)localObject1))) {
        i = 0;
      } else {
        i = 1;
      }
      localObject1 = localObject2;
      if (i != 0)
      {
        localObject1 = localObject2;
        if (c.n.m.a("GOOGLE_PLAY", Settings.BuildName.GOOGLE_PLAY.name(), true)) {
          localObject1 = Settings.BuildName.TC_SHARED.name();
        }
      }
    }
    else
    {
      e.b("IS_PREALOAD_BUILD", true);
    }
    if (localPackageManager.hasSystemFeature("com.truecaller.dialer.integration"))
    {
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append("_NATIVE");
      localObject1 = ((StringBuilder)localObject2).toString();
      f.b("hasNativeDialerCallerId", true);
      i = 1;
    }
    else
    {
      i = 0;
    }
    localObject2 = (CharSequence)paramString;
    int j;
    if ((localObject2 != null) && (!c.n.m.a((CharSequence)localObject2))) {
      j = 0;
    } else {
      j = 1;
    }
    if ((j == 0) && (i == 0)) {
      return paramString;
    }
    e.a("BUILD_KEY", (String)localObject1);
    localObject2 = (CharSequence)localObject1;
    if (localObject2 != null)
    {
      i = k;
      if (!c.n.m.a((CharSequence)localObject2)) {}
    }
    else
    {
      i = 1;
    }
    if (i != 0) {
      a((String)localObject1, paramString);
    }
    return (String)localObject1;
  }
  
  private final void a(String paramString1, String paramString2)
  {
    paramString1 = ao.b().a((CharSequence)"AppMissingBuildName").a(ag.a(t.a("BuildName", b(paramString1)))).a(ag.a(t.a("OldBuildName", b(paramString2)))).a();
    ((ae)g.a()).a((org.apache.a.d.d)paramString1);
  }
  
  private static String b(String paramString)
  {
    if (paramString == null) {
      return "<null>";
    }
    int i;
    if (((CharSequence)paramString).length() == 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      return "<empty>";
    }
    return paramString;
  }
  
  public final boolean a()
  {
    return e.b("IS_PREALOAD_BUILD");
  }
  
  public final boolean b()
  {
    return b;
  }
  
  public final boolean c()
  {
    return (!b) && (!c.n.m.b(f(), "CAFEBAZAAR", false));
  }
  
  public final boolean d()
  {
    return (b) || (c.n.m.b(f(), Settings.BuildName.SAMSUNG.name(), false)) || (c.n.m.b(f(), Settings.BuildName.AMAZON.name(), false));
  }
  
  public final au e()
  {
    Object localObject3 = c.n.m.a((CharSequence)"10.41.6", new char[] { '.' }, 0, 6);
    Object localObject1 = (String)c.a.m.a((List)localObject3, 0);
    Integer localInteger = null;
    if (localObject1 != null) {
      localObject1 = Integer.valueOf(Integer.parseInt((String)localObject1));
    } else {
      localObject1 = null;
    }
    Object localObject2 = (String)c.a.m.a((List)localObject3, 1);
    if (localObject2 != null) {
      localObject2 = Integer.valueOf(Integer.parseInt((String)localObject2));
    } else {
      localObject2 = null;
    }
    localObject3 = (String)c.a.m.a((List)localObject3, 2);
    if (localObject3 != null) {
      localInteger = Integer.valueOf(Integer.parseInt((String)localObject3));
    }
    return new au((Integer)localObject1, (Integer)localObject2, localInteger);
  }
  
  public final String f()
  {
    String str2 = e.a("BUILD_KEY");
    String str1 = str2;
    if (str2 == null) {
      str1 = a(null);
    }
    k.a(str1, "coreSettings.getString(C…teBuildNameInternal(null)");
    return str1;
  }
  
  public final String g()
  {
    return a;
  }
  
  public final void h()
  {
    a(e.a("BUILD_KEY"));
  }
  
  public final String i()
  {
    Iterator localIterator = c.m.l.a(c.a.f.j(Settings.BuildName.values()), (b)l.a.a).a();
    while (localIterator.hasNext())
    {
      Object localObject2 = localIterator.next();
      Object localObject3 = (Settings.BuildName)localObject2;
      localObject1 = c.getPackageManager();
      if (localObject1 != null) {
        localObject1 = Boolean.valueOf(((PackageManager)localObject1).hasSystemFeature(packageName));
      } else {
        localObject1 = null;
      }
      if (!com.truecaller.utils.extensions.c.a((Boolean)localObject1))
      {
        localObject1 = d;
        String str = packageName;
        k.a(str, "it.packageName");
        if (((com.truecaller.utils.d)localObject1).c(str))
        {
          localObject1 = d;
          localObject3 = packageName;
          k.a(localObject3, "it.packageName");
          if (((com.truecaller.utils.d)localObject1).d((String)localObject3)) {}
        }
        else
        {
          i = 0;
          break label155;
        }
      }
      int i = 1;
      label155:
      if (i != 0)
      {
        localObject1 = localObject2;
        break label166;
      }
    }
    Object localObject1 = null;
    label166:
    localObject1 = (Settings.BuildName)localObject1;
    if (localObject1 != null) {
      return ((Settings.BuildName)localObject1).name();
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */