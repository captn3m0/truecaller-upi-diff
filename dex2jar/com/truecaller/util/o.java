package com.truecaller.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.provider.CallLog.Calls;
import android.support.v4.content.b;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.f;
import com.truecaller.bp;
import com.truecaller.callhistory.a;

public final class o
  extends ContentObserver
{
  private static boolean a = false;
  private static final Uri b = CallLog.Calls.CONTENT_URI;
  private final f<a> c;
  
  private o(Handler paramHandler, f<a> paramf)
  {
    super(paramHandler);
    c = paramf;
  }
  
  public static void a(Context paramContext)
  {
    if (!a)
    {
      if (b.a(paramContext, "android.permission.READ_CALL_LOG") != 0) {
        return;
      }
      Object localObject = (TrueApp)paramContext.getApplicationContext();
      localObject = new o(new Handler(), ((TrueApp)localObject).a().ad());
      paramContext.getContentResolver().registerContentObserver(b, true, (ContentObserver)localObject);
      a = true;
      return;
    }
  }
  
  public final void onChange(boolean paramBoolean)
  {
    ((a)c.a()).a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */