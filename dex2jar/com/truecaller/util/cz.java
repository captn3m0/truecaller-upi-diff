package com.truecaller.util;

import android.content.Context;
import com.truecaller.network.search.l;
import dagger.a.d;
import javax.inject.Provider;

public final class cz
  implements d<l>
{
  private final Provider<Context> a;
  
  private cz(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static cz a(Provider<Context> paramProvider)
  {
    return new cz(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.cz
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */