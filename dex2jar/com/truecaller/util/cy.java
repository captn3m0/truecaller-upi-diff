package com.truecaller.util;

import android.content.Context;
import com.truecaller.analytics.b;
import com.truecaller.messaging.transport.im.j;
import com.truecaller.utils.l;
import dagger.a.d;
import javax.inject.Provider;

public final class cy
  implements d<bn>
{
  private final Provider<Context> a;
  private final Provider<b> b;
  private final Provider<l> c;
  private final Provider<j> d;
  
  private cy(Provider<Context> paramProvider, Provider<b> paramProvider1, Provider<l> paramProvider2, Provider<j> paramProvider3)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static cy a(Provider<Context> paramProvider, Provider<b> paramProvider1, Provider<l> paramProvider2, Provider<j> paramProvider3)
  {
    return new cy(paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.cy
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */