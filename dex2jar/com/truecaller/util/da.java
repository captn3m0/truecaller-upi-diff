package com.truecaller.util;

import android.content.Context;
import com.truecaller.utils.l;
import dagger.a.g;
import javax.inject.Provider;

public final class da
  implements dagger.a.d<cf>
{
  private final Provider<Context> a;
  private final Provider<l> b;
  private final Provider<com.truecaller.utils.d> c;
  
  private da(Provider<Context> paramProvider, Provider<l> paramProvider1, Provider<com.truecaller.utils.d> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static cf a(Context paramContext, l paraml, com.truecaller.utils.d paramd)
  {
    return (cf)g.a(new cg(paramContext, paraml, paramd), "Cannot return null from a non-@Nullable @Provides method");
  }
  
  public static da a(Provider<Context> paramProvider, Provider<l> paramProvider1, Provider<com.truecaller.utils.d> paramProvider2)
  {
    return new da(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.da
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */