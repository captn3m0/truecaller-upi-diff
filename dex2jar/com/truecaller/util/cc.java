package com.truecaller.util;

import android.net.Uri;
import android.net.Uri.Builder;
import c.g.b.k;
import java.util.Locale;
import javax.inject.Inject;

public final class cc
  implements cb
{
  private final com.truecaller.common.h.u a;
  
  @Inject
  public cc(com.truecaller.common.h.u paramu)
  {
    a = paramu;
  }
  
  public final String a(String paramString)
  {
    if (paramString != null)
    {
      String str = a.e(paramString);
      if (str != null)
      {
        Uri.Builder localBuilder = Uri.parse("https://truecaller.com").buildUpon().appendPath("search");
        Locale localLocale = Locale.ENGLISH;
        k.a(localLocale, "Locale.ENGLISH");
        if (str != null)
        {
          str = str.toLowerCase(localLocale);
          k.a(str, "(this as java.lang.String).toLowerCase(locale)");
          paramString = localBuilder.appendPath(str).appendEncodedPath(paramString).build().toString();
          k.a(paramString, "Uri.parse(TRUECALLER_BAS…              .toString()");
          return paramString;
        }
        throw new c.u("null cannot be cast to non-null type java.lang.String");
      }
    }
    return "https://truecaller.com";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.cc
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */