package com.truecaller.util;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class ct
  implements d<ad>
{
  private final Provider<Context> a;
  
  private ct(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static ct a(Provider<Context> paramProvider)
  {
    return new ct(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ct
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */