package com.truecaller.util;

import android.content.Context;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.net.Uri;
import c.n.m;

public final class be
  implements bd
{
  private final Context a;
  
  public be(Context paramContext)
  {
    a = paramContext;
  }
  
  private final String a(Uri paramUri, String paramString)
  {
    MediaExtractor localMediaExtractor = new MediaExtractor();
    localMediaExtractor.setDataSource(a, paramUri, null);
    int j = localMediaExtractor.getTrackCount();
    int i = 0;
    while (i < j)
    {
      String str2 = localMediaExtractor.getTrackFormat(i).getString("mime");
      String str1 = str2;
      if (str2 == null) {
        str1 = "";
      }
      int k = paramString.hashCode();
      if (k != 93166550)
      {
        if ((k == 112202875) && (paramString.equals("video")) && (m.b(str1, "video", false)))
        {
          k = str1.hashCode();
          if (k != -1664118616)
          {
            if (k != -1662541442)
            {
              if (k != 1187890754) {
                if ((k != 1331836730) || (!str1.equals("video/avc"))) {
                  break label194;
                }
              } else {
                if (!str1.equals("video/mp4v-es")) {
                  break label194;
                }
              }
            }
            else {
              if (!str1.equals("video/hevc")) {
                break label194;
              }
            }
            return "video/mp4";
          }
          else if (str1.equals("video/3gpp"))
          {
            return "video/3gpp";
          }
          label194:
          "Unknown track video type ".concat(String.valueOf(str1));
        }
      }
      else if ((paramString.equals("audio")) && (m.b(str1, "audio", false)))
      {
        if ((str1.hashCode() == -53558318) && (str1.equals("audio/mp4a-latm"))) {
          return "audio/aac";
        }
        "Unknown track video type ".concat(String.valueOf(str1));
      }
      i += 1;
    }
    "No suitable media track found for ".concat(String.valueOf(paramUri));
    return null;
  }
  
  /* Error */
  public final de a(Uri paramUri)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 111
    //   3: invokestatic 18	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: new 113	android/media/MediaMetadataRetriever
    //   9: dup
    //   10: invokespecial 114	android/media/MediaMetadataRetriever:<init>	()V
    //   13: astore 6
    //   15: aload 6
    //   17: astore 7
    //   19: aload 6
    //   21: aload_0
    //   22: getfield 23	com/truecaller/util/be:a	Landroid/content/Context;
    //   25: aload_1
    //   26: invokevirtual 117	android/media/MediaMetadataRetriever:setDataSource	(Landroid/content/Context;Landroid/net/Uri;)V
    //   29: aload 6
    //   31: astore 7
    //   33: aload_0
    //   34: aload_1
    //   35: ldc 59
    //   37: invokespecial 119	com/truecaller/util/be:a	(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    //   40: astore 8
    //   42: aload 6
    //   44: astore 7
    //   46: aload 6
    //   48: bipush 9
    //   50: invokevirtual 123	android/media/MediaMetadataRetriever:extractMetadata	(I)Ljava/lang/String;
    //   53: astore_1
    //   54: aload_1
    //   55: ifnull +335 -> 390
    //   58: aload 6
    //   60: astore 7
    //   62: aload_1
    //   63: invokestatic 129	java/lang/Long:parseLong	(Ljava/lang/String;)J
    //   66: invokestatic 132	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   69: astore_1
    //   70: goto +3 -> 73
    //   73: iconst_m1
    //   74: istore_3
    //   75: aload_1
    //   76: ifnull +319 -> 395
    //   79: aload 6
    //   81: astore 7
    //   83: aload_1
    //   84: checkcast 134	java/lang/Number
    //   87: invokevirtual 138	java/lang/Number:longValue	()J
    //   90: pop2
    //   91: aload 6
    //   93: astore 7
    //   95: getstatic 144	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   98: aload_1
    //   99: invokevirtual 145	java/lang/Long:longValue	()J
    //   102: invokevirtual 149	java/util/concurrent/TimeUnit:toSeconds	(J)J
    //   105: l2i
    //   106: istore 4
    //   108: goto +3 -> 111
    //   111: aload 6
    //   113: astore 7
    //   115: aload 6
    //   117: bipush 24
    //   119: invokevirtual 123	android/media/MediaMetadataRetriever:extractMetadata	(I)Ljava/lang/String;
    //   122: astore_1
    //   123: aload_1
    //   124: ifnull +277 -> 401
    //   127: aload 6
    //   129: astore 7
    //   131: aload_1
    //   132: invokestatic 155	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   135: istore_2
    //   136: goto +267 -> 403
    //   139: aload 6
    //   141: astore 7
    //   143: aload 6
    //   145: bipush 18
    //   147: invokevirtual 123	android/media/MediaMetadataRetriever:extractMetadata	(I)Ljava/lang/String;
    //   150: astore_1
    //   151: aload_1
    //   152: ifnull +267 -> 419
    //   155: aload 6
    //   157: astore 7
    //   159: aload_1
    //   160: invokestatic 155	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   163: istore_2
    //   164: goto +3 -> 167
    //   167: aload 6
    //   169: astore 7
    //   171: aload 6
    //   173: bipush 19
    //   175: invokevirtual 123	android/media/MediaMetadataRetriever:extractMetadata	(I)Ljava/lang/String;
    //   178: astore_1
    //   179: iload_2
    //   180: istore 5
    //   182: aload_1
    //   183: ifnull +77 -> 260
    //   186: aload 6
    //   188: astore 7
    //   190: aload_1
    //   191: invokestatic 155	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   194: istore_3
    //   195: iload_2
    //   196: istore 5
    //   198: goto +62 -> 260
    //   201: aload 6
    //   203: astore 7
    //   205: aload 6
    //   207: bipush 19
    //   209: invokevirtual 123	android/media/MediaMetadataRetriever:extractMetadata	(I)Ljava/lang/String;
    //   212: astore_1
    //   213: aload_1
    //   214: ifnull +210 -> 424
    //   217: aload 6
    //   219: astore 7
    //   221: aload_1
    //   222: invokestatic 155	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   225: istore_2
    //   226: goto +3 -> 229
    //   229: aload 6
    //   231: astore 7
    //   233: aload 6
    //   235: bipush 18
    //   237: invokevirtual 123	android/media/MediaMetadataRetriever:extractMetadata	(I)Ljava/lang/String;
    //   240: astore_1
    //   241: iload_2
    //   242: istore 5
    //   244: aload_1
    //   245: ifnull +15 -> 260
    //   248: aload 6
    //   250: astore 7
    //   252: aload_1
    //   253: invokestatic 155	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   256: istore_3
    //   257: iload_2
    //   258: istore 5
    //   260: aload 6
    //   262: astore 7
    //   264: new 157	com/truecaller/util/de
    //   267: dup
    //   268: iload 5
    //   270: iload_3
    //   271: iload 4
    //   273: aload 8
    //   275: invokespecial 160	com/truecaller/util/de:<init>	(IIILjava/lang/String;)V
    //   278: astore_1
    //   279: aload 6
    //   281: invokevirtual 163	android/media/MediaMetadataRetriever:release	()V
    //   284: aload_1
    //   285: areturn
    //   286: astore_1
    //   287: goto +29 -> 316
    //   290: astore_1
    //   291: aconst_null
    //   292: astore 7
    //   294: goto +42 -> 336
    //   297: aconst_null
    //   298: astore 6
    //   300: aload 6
    //   302: ifnull +57 -> 359
    //   305: aload 6
    //   307: invokevirtual 163	android/media/MediaMetadataRetriever:release	()V
    //   310: aconst_null
    //   311: areturn
    //   312: astore_1
    //   313: aconst_null
    //   314: astore 6
    //   316: aload 6
    //   318: astore 7
    //   320: aload_1
    //   321: checkcast 165	java/lang/Throwable
    //   324: invokestatic 171	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   327: aload 6
    //   329: ifnull +30 -> 359
    //   332: goto -27 -> 305
    //   335: astore_1
    //   336: aload 7
    //   338: ifnull +8 -> 346
    //   341: aload 7
    //   343: invokevirtual 163	android/media/MediaMetadataRetriever:release	()V
    //   346: aload_1
    //   347: athrow
    //   348: aconst_null
    //   349: astore 6
    //   351: aload 6
    //   353: ifnull +6 -> 359
    //   356: goto -51 -> 305
    //   359: aconst_null
    //   360: areturn
    //   361: astore_1
    //   362: goto -14 -> 348
    //   365: astore_1
    //   366: goto -69 -> 297
    //   369: astore_1
    //   370: goto -19 -> 351
    //   373: astore_1
    //   374: goto -74 -> 300
    //   377: astore 6
    //   379: goto -95 -> 284
    //   382: astore_1
    //   383: aconst_null
    //   384: areturn
    //   385: astore 6
    //   387: goto -41 -> 346
    //   390: aconst_null
    //   391: astore_1
    //   392: goto -319 -> 73
    //   395: iconst_m1
    //   396: istore 4
    //   398: goto -287 -> 111
    //   401: iconst_m1
    //   402: istore_2
    //   403: iload_2
    //   404: bipush 90
    //   406: if_icmpeq -205 -> 201
    //   409: iload_2
    //   410: sipush 270
    //   413: if_icmpne -274 -> 139
    //   416: goto -215 -> 201
    //   419: iconst_m1
    //   420: istore_2
    //   421: goto -254 -> 167
    //   424: iconst_m1
    //   425: istore_2
    //   426: goto -197 -> 229
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	429	0	this	be
    //   0	429	1	paramUri	Uri
    //   135	291	2	i	int
    //   74	197	3	j	int
    //   106	291	4	k	int
    //   180	89	5	m	int
    //   13	339	6	localMediaMetadataRetriever1	android.media.MediaMetadataRetriever
    //   377	1	6	localException1	Exception
    //   385	1	6	localException2	Exception
    //   17	325	7	localMediaMetadataRetriever2	android.media.MediaMetadataRetriever
    //   40	234	8	str	String
    // Exception table:
    //   from	to	target	type
    //   19	29	286	java/io/FileNotFoundException
    //   33	42	286	java/io/FileNotFoundException
    //   46	54	286	java/io/FileNotFoundException
    //   62	70	286	java/io/FileNotFoundException
    //   83	91	286	java/io/FileNotFoundException
    //   95	108	286	java/io/FileNotFoundException
    //   115	123	286	java/io/FileNotFoundException
    //   131	136	286	java/io/FileNotFoundException
    //   143	151	286	java/io/FileNotFoundException
    //   159	164	286	java/io/FileNotFoundException
    //   171	179	286	java/io/FileNotFoundException
    //   190	195	286	java/io/FileNotFoundException
    //   205	213	286	java/io/FileNotFoundException
    //   221	226	286	java/io/FileNotFoundException
    //   233	241	286	java/io/FileNotFoundException
    //   252	257	286	java/io/FileNotFoundException
    //   264	279	286	java/io/FileNotFoundException
    //   6	15	290	finally
    //   6	15	312	java/io/FileNotFoundException
    //   19	29	335	finally
    //   33	42	335	finally
    //   46	54	335	finally
    //   62	70	335	finally
    //   83	91	335	finally
    //   95	108	335	finally
    //   115	123	335	finally
    //   131	136	335	finally
    //   143	151	335	finally
    //   159	164	335	finally
    //   171	179	335	finally
    //   190	195	335	finally
    //   205	213	335	finally
    //   221	226	335	finally
    //   233	241	335	finally
    //   252	257	335	finally
    //   264	279	335	finally
    //   320	327	335	finally
    //   6	15	361	java/lang/RuntimeException
    //   6	15	365	java/io/IOException
    //   19	29	369	java/lang/RuntimeException
    //   33	42	369	java/lang/RuntimeException
    //   46	54	369	java/lang/RuntimeException
    //   62	70	369	java/lang/RuntimeException
    //   83	91	369	java/lang/RuntimeException
    //   95	108	369	java/lang/RuntimeException
    //   115	123	369	java/lang/RuntimeException
    //   131	136	369	java/lang/RuntimeException
    //   143	151	369	java/lang/RuntimeException
    //   159	164	369	java/lang/RuntimeException
    //   171	179	369	java/lang/RuntimeException
    //   190	195	369	java/lang/RuntimeException
    //   205	213	369	java/lang/RuntimeException
    //   221	226	369	java/lang/RuntimeException
    //   233	241	369	java/lang/RuntimeException
    //   252	257	369	java/lang/RuntimeException
    //   264	279	369	java/lang/RuntimeException
    //   19	29	373	java/io/IOException
    //   33	42	373	java/io/IOException
    //   46	54	373	java/io/IOException
    //   62	70	373	java/io/IOException
    //   83	91	373	java/io/IOException
    //   95	108	373	java/io/IOException
    //   115	123	373	java/io/IOException
    //   131	136	373	java/io/IOException
    //   143	151	373	java/io/IOException
    //   159	164	373	java/io/IOException
    //   171	179	373	java/io/IOException
    //   190	195	373	java/io/IOException
    //   205	213	373	java/io/IOException
    //   221	226	373	java/io/IOException
    //   233	241	373	java/io/IOException
    //   252	257	373	java/io/IOException
    //   264	279	373	java/io/IOException
    //   279	284	377	java/lang/Exception
    //   305	310	382	java/lang/Exception
    //   341	346	385	java/lang/Exception
  }
  
  /* Error */
  public final e b(Uri paramUri)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 111
    //   3: invokestatic 18	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: new 113	android/media/MediaMetadataRetriever
    //   9: dup
    //   10: invokespecial 114	android/media/MediaMetadataRetriever:<init>	()V
    //   13: astore_3
    //   14: aload_3
    //   15: astore 4
    //   17: aload_3
    //   18: aload_0
    //   19: getfield 23	com/truecaller/util/be:a	Landroid/content/Context;
    //   22: aload_1
    //   23: invokevirtual 117	android/media/MediaMetadataRetriever:setDataSource	(Landroid/content/Context;Landroid/net/Uri;)V
    //   26: aload_3
    //   27: astore 4
    //   29: aload_0
    //   30: aload_1
    //   31: ldc 93
    //   33: invokespecial 119	com/truecaller/util/be:a	(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    //   36: astore 5
    //   38: aload_3
    //   39: astore 4
    //   41: aload_3
    //   42: bipush 9
    //   44: invokevirtual 123	android/media/MediaMetadataRetriever:extractMetadata	(I)Ljava/lang/String;
    //   47: astore_1
    //   48: aload_1
    //   49: ifnull +164 -> 213
    //   52: aload_3
    //   53: astore 4
    //   55: aload_1
    //   56: invokestatic 129	java/lang/Long:parseLong	(Ljava/lang/String;)J
    //   59: invokestatic 132	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   62: astore_1
    //   63: goto +3 -> 66
    //   66: aload_1
    //   67: ifnull +151 -> 218
    //   70: aload_3
    //   71: astore 4
    //   73: aload_1
    //   74: checkcast 134	java/lang/Number
    //   77: invokevirtual 138	java/lang/Number:longValue	()J
    //   80: pop2
    //   81: aload_3
    //   82: astore 4
    //   84: getstatic 144	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   87: aload_1
    //   88: invokevirtual 145	java/lang/Long:longValue	()J
    //   91: invokevirtual 149	java/util/concurrent/TimeUnit:toSeconds	(J)J
    //   94: l2i
    //   95: istore_2
    //   96: goto +3 -> 99
    //   99: aload_3
    //   100: astore 4
    //   102: new 174	com/truecaller/util/e
    //   105: dup
    //   106: iload_2
    //   107: aload 5
    //   109: invokespecial 177	com/truecaller/util/e:<init>	(ILjava/lang/String;)V
    //   112: astore_1
    //   113: aload_3
    //   114: invokevirtual 163	android/media/MediaMetadataRetriever:release	()V
    //   117: aload_1
    //   118: areturn
    //   119: astore_1
    //   120: goto +25 -> 145
    //   123: astore_1
    //   124: aconst_null
    //   125: astore 4
    //   127: goto +36 -> 163
    //   130: aconst_null
    //   131: astore_3
    //   132: aload_3
    //   133: ifnull +51 -> 184
    //   136: aload_3
    //   137: invokevirtual 163	android/media/MediaMetadataRetriever:release	()V
    //   140: aconst_null
    //   141: areturn
    //   142: astore_1
    //   143: aconst_null
    //   144: astore_3
    //   145: aload_3
    //   146: astore 4
    //   148: aload_1
    //   149: checkcast 165	java/lang/Throwable
    //   152: invokestatic 171	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   155: aload_3
    //   156: ifnull +28 -> 184
    //   159: goto -23 -> 136
    //   162: astore_1
    //   163: aload 4
    //   165: ifnull +8 -> 173
    //   168: aload 4
    //   170: invokevirtual 163	android/media/MediaMetadataRetriever:release	()V
    //   173: aload_1
    //   174: athrow
    //   175: aconst_null
    //   176: astore_3
    //   177: aload_3
    //   178: ifnull +6 -> 184
    //   181: goto -45 -> 136
    //   184: aconst_null
    //   185: areturn
    //   186: astore_1
    //   187: goto -12 -> 175
    //   190: astore_1
    //   191: goto -61 -> 130
    //   194: astore_1
    //   195: goto -18 -> 177
    //   198: astore_1
    //   199: goto -67 -> 132
    //   202: astore_3
    //   203: goto -86 -> 117
    //   206: astore_1
    //   207: aconst_null
    //   208: areturn
    //   209: astore_3
    //   210: goto -37 -> 173
    //   213: aconst_null
    //   214: astore_1
    //   215: goto -149 -> 66
    //   218: iconst_m1
    //   219: istore_2
    //   220: goto -121 -> 99
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	223	0	this	be
    //   0	223	1	paramUri	Uri
    //   95	125	2	i	int
    //   13	165	3	localMediaMetadataRetriever1	android.media.MediaMetadataRetriever
    //   202	1	3	localException1	Exception
    //   209	1	3	localException2	Exception
    //   15	154	4	localMediaMetadataRetriever2	android.media.MediaMetadataRetriever
    //   36	72	5	str	String
    // Exception table:
    //   from	to	target	type
    //   17	26	119	java/io/FileNotFoundException
    //   29	38	119	java/io/FileNotFoundException
    //   41	48	119	java/io/FileNotFoundException
    //   55	63	119	java/io/FileNotFoundException
    //   73	81	119	java/io/FileNotFoundException
    //   84	96	119	java/io/FileNotFoundException
    //   102	113	119	java/io/FileNotFoundException
    //   6	14	123	finally
    //   6	14	142	java/io/FileNotFoundException
    //   17	26	162	finally
    //   29	38	162	finally
    //   41	48	162	finally
    //   55	63	162	finally
    //   73	81	162	finally
    //   84	96	162	finally
    //   102	113	162	finally
    //   148	155	162	finally
    //   6	14	186	java/lang/RuntimeException
    //   6	14	190	java/io/IOException
    //   17	26	194	java/lang/RuntimeException
    //   29	38	194	java/lang/RuntimeException
    //   41	48	194	java/lang/RuntimeException
    //   55	63	194	java/lang/RuntimeException
    //   73	81	194	java/lang/RuntimeException
    //   84	96	194	java/lang/RuntimeException
    //   102	113	194	java/lang/RuntimeException
    //   17	26	198	java/io/IOException
    //   29	38	198	java/io/IOException
    //   41	48	198	java/io/IOException
    //   55	63	198	java/io/IOException
    //   73	81	198	java/io/IOException
    //   84	96	198	java/io/IOException
    //   102	113	198	java/io/IOException
    //   113	117	202	java/lang/Exception
    //   136	140	206	java/lang/Exception
    //   168	173	209	java/lang/Exception
  }
  
  /* Error */
  public final au c(Uri paramUri)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 111
    //   3: invokestatic 18	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: new 181	android/graphics/BitmapFactory$Options
    //   9: dup
    //   10: invokespecial 182	android/graphics/BitmapFactory$Options:<init>	()V
    //   13: astore_2
    //   14: aload_2
    //   15: iconst_1
    //   16: putfield 186	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
    //   19: aload_1
    //   20: invokestatic 191	com/truecaller/utils/extensions/r:a	(Landroid/net/Uri;)Lcom/truecaller/utils/extensions/Scheme;
    //   23: astore_3
    //   24: aload_3
    //   25: ifnull +114 -> 139
    //   28: getstatic 196	com/truecaller/util/bf:a	[I
    //   31: aload_3
    //   32: invokevirtual 201	com/truecaller/utils/extensions/Scheme:ordinal	()I
    //   35: iaload
    //   36: tableswitch	default:+146->182, 1:+74->110, 2:+24->60
    //   60: aload_0
    //   61: getfield 23	com/truecaller/util/be:a	Landroid/content/Context;
    //   64: invokevirtual 207	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   67: aload_1
    //   68: invokevirtual 213	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   71: checkcast 215	java/io/Closeable
    //   74: astore_3
    //   75: aload_3
    //   76: checkcast 217	java/io/InputStream
    //   79: aconst_null
    //   80: aload_2
    //   81: invokestatic 223	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   84: pop
    //   85: aload_3
    //   86: aconst_null
    //   87: invokestatic 228	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   90: goto +29 -> 119
    //   93: astore_2
    //   94: aconst_null
    //   95: astore_1
    //   96: goto +7 -> 103
    //   99: astore_1
    //   100: aload_1
    //   101: athrow
    //   102: astore_2
    //   103: aload_3
    //   104: aload_1
    //   105: invokestatic 228	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   108: aload_2
    //   109: athrow
    //   110: aload_1
    //   111: invokevirtual 234	android/net/Uri:getPath	()Ljava/lang/String;
    //   114: aload_2
    //   115: invokestatic 238	android/graphics/BitmapFactory:decodeFile	(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   118: pop
    //   119: new 240	com/truecaller/util/au
    //   122: dup
    //   123: aload_2
    //   124: getfield 244	android/graphics/BitmapFactory$Options:outWidth	I
    //   127: aload_2
    //   128: getfield 247	android/graphics/BitmapFactory$Options:outHeight	I
    //   131: aload_2
    //   132: getfield 251	android/graphics/BitmapFactory$Options:outMimeType	Ljava/lang/String;
    //   135: invokespecial 254	com/truecaller/util/au:<init>	(IILjava/lang/String;)V
    //   138: areturn
    //   139: new 256	java/lang/StringBuilder
    //   142: dup
    //   143: ldc_w 258
    //   146: invokespecial 261	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   149: astore_2
    //   150: aload_2
    //   151: aload_1
    //   152: invokevirtual 265	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   155: pop
    //   156: aload_2
    //   157: ldc_w 267
    //   160: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   163: pop
    //   164: new 272	java/lang/IllegalArgumentException
    //   167: dup
    //   168: aload_2
    //   169: invokevirtual 275	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   172: invokespecial 276	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   175: checkcast 165	java/lang/Throwable
    //   178: athrow
    //   179: astore_1
    //   180: aconst_null
    //   181: areturn
    //   182: goto -43 -> 139
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	185	0	this	be
    //   0	185	1	paramUri	Uri
    //   13	68	2	localOptions1	android.graphics.BitmapFactory.Options
    //   93	1	2	localObject1	Object
    //   102	30	2	localOptions2	android.graphics.BitmapFactory.Options
    //   149	20	2	localStringBuilder	StringBuilder
    //   23	81	3	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   75	85	93	finally
    //   75	85	99	java/lang/Throwable
    //   100	102	102	finally
    //   19	24	179	java/lang/RuntimeException
    //   28	60	179	java/lang/RuntimeException
    //   60	75	179	java/lang/RuntimeException
    //   85	90	179	java/lang/RuntimeException
    //   103	110	179	java/lang/RuntimeException
    //   110	119	179	java/lang/RuntimeException
    //   139	179	179	java/lang/RuntimeException
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.be
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */