package com.truecaller.util;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import dagger.a.d;
import javax.inject.Provider;

public final class cw
  implements d<f<ba>>
{
  private final Provider<i> a;
  private final Provider<ba> b;
  
  private cw(Provider<i> paramProvider, Provider<ba> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static cw a(Provider<i> paramProvider, Provider<ba> paramProvider1)
  {
    return new cw(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.cw
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */