package com.truecaller.util;

import android.content.Context;
import c.g.b.k;
import c.l;
import com.truecaller.common.h.j;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import org.a.a.a.g;
import org.a.a.c;
import org.a.a.i;
import org.a.a.p;
import org.a.a.x;

public final class ag
  implements af
{
  private final Context a;
  
  public ag(Context paramContext)
  {
    a = paramContext;
  }
  
  public final String a()
  {
    String str = j.c();
    k.a(str, "DateTimeUtils.getCurrentTimeZoneString()");
    return str;
  }
  
  public final String a(int paramInt)
  {
    String str = j.a(a, paramInt);
    k.a(str, "DateTimeUtils.getShortFo…atDuration(context, secs)");
    return str;
  }
  
  public final String a(long paramLong, DatePattern paramDatePattern)
  {
    k.b(paramDatePattern, "datePattern");
    switch (ah.a[paramDatePattern.ordinal()])
    {
    default: 
      throw new l();
    case 2: 
      paramDatePattern = org.a.a.d.a.a("EEEE, dd MMM YYYY");
      break;
    case 1: 
      paramDatePattern = org.a.a.d.a.a("EEEE, dd MMM");
    }
    paramDatePattern = paramDatePattern.a(paramLong);
    k.a(paramDatePattern, "when (datePattern) {\n   …       }.print(timestamp)");
    return paramDatePattern;
  }
  
  public final boolean a(long paramLong)
  {
    return p.a().a((org.a.a.z)new p(paramLong)) == 0;
  }
  
  public final boolean a(long paramLong1, long paramLong2)
  {
    return new p(paramLong1).a((org.a.a.z)new p(paramLong2)) == 0;
  }
  
  public final boolean a(org.a.a.b paramb1, org.a.a.b paramb2)
  {
    k.b(paramb1, "date");
    k.b(paramb2, "compareDate");
    return paramb1.c((x)paramb2);
  }
  
  public final String b(int paramInt)
  {
    int i = paramInt / 60;
    Object localObject = c.g.b.z.a;
    localObject = String.format("%02d:%02d", Arrays.copyOf(new Object[] { Integer.valueOf(i), Integer.valueOf(paramInt % 60) }, 2));
    k.a(localObject, "java.lang.String.format(format, *args)");
    return (String)localObject;
  }
  
  public final org.a.a.b b()
  {
    org.a.a.b localb = org.a.a.b.ay_();
    k.a(localb, "DateTime.now()");
    return localb;
  }
  
  public final boolean b(long paramLong)
  {
    p localp = p.a();
    long l = b.s().b(a, 1);
    l = b.u().d(l);
    if (l != a) {
      localp = new p(l, b);
    }
    return localp.a((org.a.a.z)new p(paramLong)) == 0;
  }
  
  public final boolean b(org.a.a.b paramb1, org.a.a.b paramb2)
  {
    k.b(paramb1, "date");
    k.b(paramb2, "compareDate");
    return paramb1.b((x)paramb2);
  }
  
  public final long c()
  {
    return TimeUnit.MILLISECONDS.toSeconds(ba);
  }
  
  public final boolean c(long paramLong)
  {
    p localp = p.a();
    k.a(localp, "LocalDate.now()");
    return localp.c() == new p(paramLong).c();
  }
  
  public final int d(long paramLong)
  {
    return new org.a.a.b(paramLong).g();
  }
  
  public final int e(long paramLong)
  {
    return new org.a.a.b(paramLong).h();
  }
  
  public final int f(long paramLong)
  {
    return new org.a.a.b(paramLong).i();
  }
  
  public final CharSequence g(long paramLong)
  {
    CharSequence localCharSequence = j.a(a, paramLong, false);
    k.a(localCharSequence, "DateTimeUtils.getRelativ…ate(context, date, false)");
    return localCharSequence;
  }
  
  public final String h(long paramLong)
  {
    String str = j.f(a, paramLong);
    k.a(str, "DateTimeUtils.getFormattedTime(context, millis)");
    return str;
  }
  
  public final String i(long paramLong)
  {
    String str = j.c(a, paramLong);
    k.a(str, "DateTimeUtils.getFormatt…Duration(context, millis)");
    return str;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ag
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */