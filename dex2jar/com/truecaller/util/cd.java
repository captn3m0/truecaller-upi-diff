package com.truecaller.util;

import com.truecaller.common.h.u;
import dagger.a.d;
import javax.inject.Provider;

public final class cd
  implements d<cc>
{
  private final Provider<u> a;
  
  private cd(Provider<u> paramProvider)
  {
    a = paramProvider;
  }
  
  public static cd a(Provider<u> paramProvider)
  {
    return new cd(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.cd
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */