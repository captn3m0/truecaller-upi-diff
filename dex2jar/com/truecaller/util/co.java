package com.truecaller.util;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.Context;
import android.content.OperationApplicationException;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.RemoteException;
import android.text.TextUtils;
import com.truecaller.TrueApp;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.h.c;
import com.truecaller.common.h.h;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.a;
import com.truecaller.content.TruecallerContract.aa;
import com.truecaller.content.TruecallerContract.ae;
import com.truecaller.content.TruecallerContract.ah;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.log.d;
import com.truecaller.log.f;
import com.truecaller.utils.l;
import java.util.ArrayList;

@Deprecated
public final class co
{
  public static boolean a(Context paramContext)
  {
    ArrayList localArrayList = new ArrayList(3);
    localArrayList.add(ContentProviderOperation.newDelete(TruecallerContract.ah.a()).build());
    localArrayList.add(ContentProviderOperation.newDelete(TruecallerContract.a.a()).build());
    localArrayList.add(ContentProviderOperation.newDelete(TruecallerContract.n.a()).build());
    localArrayList.add(ContentProviderOperation.newDelete(TruecallerContract.aa.a()).build());
    localArrayList.add(ContentProviderOperation.newDelete(Uri.withAppendedPath(TruecallerContract.b, "msg/msg_conversations")).build());
    localArrayList.add(ContentProviderOperation.newDelete(TruecallerContract.ae.a()).build());
    try
    {
      paramContext.getContentResolver().applyBatch(TruecallerContract.a, localArrayList);
      return true;
    }
    catch (RemoteException|OperationApplicationException paramContext)
    {
      for (;;) {}
    }
    return false;
  }
  
  public static Location b(Context paramContext)
  {
    if (((bk)paramContext.getApplicationContext()).a().bw().a(new String[] { "android.permission.ACCESS_COARSE_LOCATION" })) {}
    try
    {
      paramContext = ((LocationManager)paramContext.getSystemService("location")).getLastKnownLocation("network");
      return paramContext;
    }
    catch (RuntimeException paramContext)
    {
      d.a(3, "AdsUtil", f.a(paramContext));
      return null;
    }
    catch (SecurityException paramContext)
    {
      for (;;) {}
    }
  }
  
  public static boolean c(Context paramContext)
  {
    if (!((TrueApp)paramContext.getApplicationContext()).a().aI().b()) {
      return false;
    }
    paramContext = h.c(paramContext);
    return (paramContext != null) && (!TextUtils.isEmpty(c)) && (c.equalsIgnoreCase("gb"));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.co
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */