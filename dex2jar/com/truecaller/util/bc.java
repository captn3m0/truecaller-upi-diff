package com.truecaller.util;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import c.n;
import com.truecaller.androidactors.w;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.d;
import com.truecaller.messaging.data.types.AudioEntity;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.ImageEntity;
import com.truecaller.messaging.data.types.VideoEntity;
import com.truecaller.utils.extensions.r;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

final class bc
  implements ba
{
  private final Context a;
  private final g b;
  private final bd c;
  private final ch d;
  private final bg e;
  private final com.truecaller.featuretoggles.e f;
  
  bc(Context paramContext, g paramg, bd parambd, ch paramch, bg parambg, com.truecaller.featuretoggles.e parame)
  {
    a = paramContext;
    b = paramg;
    c = parambd;
    d = paramch;
    e = parambg;
    f = parame;
  }
  
  /* Error */
  private Uri b(Uri paramUri)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 25	com/truecaller/util/bc:a	Landroid/content/Context;
    //   4: astore_3
    //   5: aconst_null
    //   6: astore_2
    //   7: aload_3
    //   8: aconst_null
    //   9: aconst_null
    //   10: invokestatic 46	com/truecaller/util/TempContentProvider:a	(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Integer;)Landroid/net/Uri;
    //   13: astore 6
    //   15: aload 6
    //   17: ifnonnull +5 -> 22
    //   20: aconst_null
    //   21: areturn
    //   22: aload_0
    //   23: getfield 25	com/truecaller/util/bc:a	Landroid/content/Context;
    //   26: invokevirtual 52	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   29: aload_1
    //   30: invokevirtual 58	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   33: astore_3
    //   34: aload_0
    //   35: getfield 25	com/truecaller/util/bc:a	Landroid/content/Context;
    //   38: invokevirtual 52	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   41: aload 6
    //   43: invokevirtual 62	android/content/ContentResolver:openOutputStream	(Landroid/net/Uri;)Ljava/io/OutputStream;
    //   46: astore_1
    //   47: aload_3
    //   48: ifnull +41 -> 89
    //   51: aload_1
    //   52: ifnonnull +6 -> 58
    //   55: goto +34 -> 89
    //   58: aload_1
    //   59: astore 5
    //   61: aload_3
    //   62: astore 4
    //   64: aload_3
    //   65: aload_1
    //   66: invokestatic 67	com/truecaller/utils/extensions/m:a	(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    //   69: pop2
    //   70: aload_3
    //   71: invokestatic 72	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   74: aload_1
    //   75: invokestatic 72	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   78: aload 6
    //   80: areturn
    //   81: astore_2
    //   82: goto +46 -> 128
    //   85: astore_2
    //   86: goto +42 -> 128
    //   89: aload_3
    //   90: invokestatic 72	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   93: aload_1
    //   94: invokestatic 72	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   97: aconst_null
    //   98: areturn
    //   99: astore_1
    //   100: goto +72 -> 172
    //   103: astore_2
    //   104: goto +4 -> 108
    //   107: astore_2
    //   108: aconst_null
    //   109: astore_1
    //   110: goto +18 -> 128
    //   113: astore_1
    //   114: aconst_null
    //   115: astore_3
    //   116: goto +56 -> 172
    //   119: astore_2
    //   120: goto +4 -> 124
    //   123: astore_2
    //   124: aconst_null
    //   125: astore_3
    //   126: aload_3
    //   127: astore_1
    //   128: aload_1
    //   129: astore 5
    //   131: aload_3
    //   132: astore 4
    //   134: aload_2
    //   135: invokestatic 78	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   138: aload_1
    //   139: astore 5
    //   141: aload_3
    //   142: astore 4
    //   144: aload_0
    //   145: getfield 31	com/truecaller/util/bc:d	Lcom/truecaller/util/ch;
    //   148: aload 6
    //   150: invokeinterface 83 2 0
    //   155: aload_3
    //   156: invokestatic 72	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   159: aload_1
    //   160: invokestatic 72	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   163: aconst_null
    //   164: areturn
    //   165: astore_1
    //   166: aload 5
    //   168: astore_2
    //   169: aload 4
    //   171: astore_3
    //   172: aload_3
    //   173: invokestatic 72	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   176: aload_2
    //   177: invokestatic 72	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   180: aload_1
    //   181: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	182	0	this	bc
    //   0	182	1	paramUri	Uri
    //   6	1	2	localObject1	Object
    //   81	1	2	localSecurityException1	SecurityException
    //   85	1	2	localIOException1	java.io.IOException
    //   103	1	2	localSecurityException2	SecurityException
    //   107	1	2	localIOException2	java.io.IOException
    //   119	1	2	localSecurityException3	SecurityException
    //   123	12	2	localIOException3	java.io.IOException
    //   168	9	2	localObject2	Object
    //   4	169	3	localObject3	Object
    //   62	108	4	localObject4	Object
    //   59	108	5	localUri1	Uri
    //   13	136	6	localUri2	Uri
    // Exception table:
    //   from	to	target	type
    //   64	70	81	java/lang/SecurityException
    //   64	70	85	java/io/IOException
    //   34	47	99	finally
    //   34	47	103	java/lang/SecurityException
    //   34	47	107	java/io/IOException
    //   22	34	113	finally
    //   22	34	119	java/lang/SecurityException
    //   22	34	123	java/io/IOException
    //   64	70	165	finally
    //   134	138	165	finally
    //   144	155	165	finally
  }
  
  private n<BinaryEntity, az> b(Uri paramUri, boolean paramBoolean, long paramLong)
  {
    Long localLong = r.a(paramUri, a);
    if (localLong == null) {
      return new n(null, az.b.a);
    }
    de localde = c.a(paramUri);
    if ((localde != null) && (localde.a()))
    {
      long l;
      if (f.d().a()) {
        l = e.b(c);
      } else {
        l = localLong.longValue();
      }
      if (l > paramLong) {
        return new n(null, new az.a(paramLong));
      }
      Uri localUri = b(paramUri);
      if (localUri == null) {
        return new n(null, az.b.a);
      }
      if (paramBoolean) {
        d.a(paramUri);
      }
      paramUri = Entity.a(-1L, d, 0, localUri, a, b, c, true, localLong.longValue(), Uri.EMPTY);
      if ((paramUri instanceof VideoEntity)) {
        return new n(paramUri, null);
      }
      d.a(paramUri);
      return new n(null, az.b.a);
    }
    return new n(null, az.b.a);
  }
  
  private n<BinaryEntity, az> c(Uri paramUri, boolean paramBoolean)
  {
    try
    {
      Object localObject = b.a(paramUri);
      if (paramBoolean) {}
      try
      {
        d.a(paramUri);
        paramUri = new n(localObject, null);
        return paramUri;
      }
      catch (SecurityException localSecurityException2)
      {
        paramUri = (Uri)localObject;
        localObject = localSecurityException2;
      }
      AssertionUtil.reportThrowableButNeverCrash(localSecurityException1);
    }
    catch (SecurityException localSecurityException1)
    {
      paramUri = null;
    }
    d.a(paramUri);
    return new n(null, az.b.a);
  }
  
  /* Error */
  private boolean c(Uri paramUri)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 25	com/truecaller/util/bc:a	Landroid/content/Context;
    //   4: invokevirtual 52	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   7: aload_1
    //   8: invokevirtual 58	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   11: astore_1
    //   12: aload_1
    //   13: invokestatic 72	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   16: iconst_1
    //   17: ireturn
    //   18: astore_1
    //   19: aconst_null
    //   20: invokestatic 72	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   23: aload_1
    //   24: athrow
    //   25: aconst_null
    //   26: invokestatic 72	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   29: iconst_0
    //   30: ireturn
    //   31: astore_1
    //   32: goto -7 -> 25
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	35	0	this	bc
    //   0	35	1	paramUri	Uri
    // Exception table:
    //   from	to	target	type
    //   0	12	18	finally
    //   0	12	31	java/io/FileNotFoundException
  }
  
  private n<BinaryEntity, az> d(Uri paramUri, boolean paramBoolean)
  {
    Long localLong = r.a(paramUri, a);
    if (localLong == null) {
      return new n(null, az.b.a);
    }
    e locale = c.b(paramUri);
    if ((locale != null) && (locale.a()))
    {
      Uri localUri = b(paramUri);
      if (localUri == null) {
        return new n(null, az.b.a);
      }
      if (paramBoolean) {
        d.a(paramUri);
      }
      paramUri = Entity.a(-1L, b, 0, localUri, -1, -1, a, true, localLong.longValue(), Uri.EMPTY);
      if ((paramUri instanceof AudioEntity)) {
        return new n(paramUri, null);
      }
      d.a(paramUri);
      return new n(null, az.b.a);
    }
    return new n(null, az.b.a);
  }
  
  /* Error */
  private File d(Uri paramUri)
  {
    // Byte code:
    //   0: invokestatic 193	android/webkit/MimeTypeMap:getSingleton	()Landroid/webkit/MimeTypeMap;
    //   3: aload_0
    //   4: getfield 25	com/truecaller/util/bc:a	Landroid/content/Context;
    //   7: invokevirtual 52	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   10: aload_1
    //   11: invokevirtual 197	android/content/ContentResolver:getType	(Landroid/net/Uri;)Ljava/lang/String;
    //   14: invokevirtual 201	android/webkit/MimeTypeMap:getExtensionFromMimeType	(Ljava/lang/String;)Ljava/lang/String;
    //   17: astore 4
    //   19: getstatic 206	android/os/Environment:DIRECTORY_DOWNLOADS	Ljava/lang/String;
    //   22: invokestatic 210	android/os/Environment:getExternalStoragePublicDirectory	(Ljava/lang/String;)Ljava/io/File;
    //   25: astore 5
    //   27: new 212	org/a/a/b
    //   30: dup
    //   31: invokestatic 217	java/lang/System:currentTimeMillis	()J
    //   34: invokespecial 218	org/a/a/b:<init>	(J)V
    //   37: astore_3
    //   38: ldc -36
    //   40: invokestatic 225	org/a/a/d/a:a	(Ljava/lang/String;)Lorg/a/a/d/b;
    //   43: astore 6
    //   45: aload 6
    //   47: ifnonnull +11 -> 58
    //   50: aload_3
    //   51: invokevirtual 231	org/a/a/a/c:toString	()Ljava/lang/String;
    //   54: astore_3
    //   55: goto +10 -> 65
    //   58: aload 6
    //   60: aload_3
    //   61: invokevirtual 236	org/a/a/d/b:a	(Lorg/a/a/x;)Ljava/lang/String;
    //   64: astore_3
    //   65: iconst_0
    //   66: istore_2
    //   67: new 238	java/lang/StringBuilder
    //   70: dup
    //   71: ldc -16
    //   73: invokespecial 243	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   76: astore 6
    //   78: aload 6
    //   80: ldc -11
    //   82: invokevirtual 249	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   85: pop
    //   86: aload 6
    //   88: aload_3
    //   89: invokevirtual 249	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   92: pop
    //   93: iload_2
    //   94: ifle +27 -> 121
    //   97: aload 6
    //   99: ldc -5
    //   101: invokevirtual 249	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   104: pop
    //   105: aload 6
    //   107: iload_2
    //   108: invokevirtual 254	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   111: pop
    //   112: aload 6
    //   114: ldc_w 256
    //   117: invokevirtual 249	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   120: pop
    //   121: aload 4
    //   123: invokestatic 261	org/c/a/a/a/k:b	(Ljava/lang/CharSequence;)Z
    //   126: ifne +20 -> 146
    //   129: aload 6
    //   131: ldc_w 263
    //   134: invokevirtual 249	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   137: pop
    //   138: aload 6
    //   140: aload 4
    //   142: invokevirtual 249	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   145: pop
    //   146: new 265	java/io/File
    //   149: dup
    //   150: aload 5
    //   152: aload 6
    //   154: invokevirtual 266	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   157: invokespecial 269	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   160: astore 6
    //   162: iload_2
    //   163: iconst_1
    //   164: iadd
    //   165: istore_2
    //   166: aload 6
    //   168: invokevirtual 272	java/io/File:exists	()Z
    //   171: ifne -104 -> 67
    //   174: aconst_null
    //   175: astore 4
    //   177: aconst_null
    //   178: astore_3
    //   179: aload_0
    //   180: getfield 25	com/truecaller/util/bc:a	Landroid/content/Context;
    //   183: invokevirtual 52	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   186: aload_1
    //   187: invokevirtual 58	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   190: astore_1
    //   191: new 274	java/io/FileOutputStream
    //   194: dup
    //   195: aload 6
    //   197: invokespecial 277	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   200: astore 4
    //   202: aload_1
    //   203: ifnonnull +14 -> 217
    //   206: aload_1
    //   207: invokestatic 72	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   210: aload 4
    //   212: invokestatic 72	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   215: aconst_null
    //   216: areturn
    //   217: aload_1
    //   218: aload 4
    //   220: invokestatic 67	com/truecaller/utils/extensions/m:a	(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    //   223: pop2
    //   224: aload_1
    //   225: invokestatic 72	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   228: aload 4
    //   230: astore_1
    //   231: goto +101 -> 332
    //   234: astore 5
    //   236: aload_1
    //   237: astore_3
    //   238: aload 4
    //   240: astore_1
    //   241: aload 5
    //   243: astore 4
    //   245: goto +96 -> 341
    //   248: astore_3
    //   249: goto +4 -> 253
    //   252: astore_3
    //   253: aload_3
    //   254: astore 5
    //   256: aload_1
    //   257: astore_3
    //   258: aload 4
    //   260: astore_1
    //   261: aload 5
    //   263: astore 4
    //   265: goto +58 -> 323
    //   268: astore 4
    //   270: aconst_null
    //   271: astore 5
    //   273: aload_1
    //   274: astore_3
    //   275: aload 5
    //   277: astore_1
    //   278: goto +63 -> 341
    //   281: astore_3
    //   282: goto +4 -> 286
    //   285: astore_3
    //   286: aload_3
    //   287: astore 4
    //   289: aconst_null
    //   290: astore 5
    //   292: aload_1
    //   293: astore_3
    //   294: aload 5
    //   296: astore_1
    //   297: goto +26 -> 323
    //   300: astore 5
    //   302: aconst_null
    //   303: astore_1
    //   304: aload 4
    //   306: astore_3
    //   307: aload 5
    //   309: astore 4
    //   311: goto +30 -> 341
    //   314: astore 4
    //   316: goto +5 -> 321
    //   319: astore 4
    //   321: aconst_null
    //   322: astore_1
    //   323: aload 4
    //   325: invokestatic 78	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   328: aload_3
    //   329: invokestatic 72	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   332: aload_1
    //   333: invokestatic 72	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   336: aload 6
    //   338: areturn
    //   339: astore 4
    //   341: aload_3
    //   342: invokestatic 72	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   345: aload_1
    //   346: invokestatic 72	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   349: aload 4
    //   351: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	352	0	this	bc
    //   0	352	1	paramUri	Uri
    //   66	100	2	i	int
    //   37	201	3	localObject1	Object
    //   248	1	3	localSecurityException1	SecurityException
    //   252	2	3	localIOException1	java.io.IOException
    //   257	18	3	localUri	Uri
    //   281	1	3	localSecurityException2	SecurityException
    //   285	2	3	localIOException2	java.io.IOException
    //   293	49	3	localObject2	Object
    //   17	247	4	localObject3	Object
    //   268	1	4	localObject4	Object
    //   287	23	4	localObject5	Object
    //   314	1	4	localSecurityException3	SecurityException
    //   319	5	4	localIOException3	java.io.IOException
    //   339	11	4	localObject6	Object
    //   25	126	5	localFile	File
    //   234	8	5	localObject7	Object
    //   254	41	5	localObject8	Object
    //   300	8	5	localObject9	Object
    //   43	294	6	localObject10	Object
    // Exception table:
    //   from	to	target	type
    //   217	224	234	finally
    //   217	224	248	java/lang/SecurityException
    //   217	224	252	java/io/IOException
    //   191	202	268	finally
    //   191	202	281	java/lang/SecurityException
    //   191	202	285	java/io/IOException
    //   179	191	300	finally
    //   179	191	314	java/lang/SecurityException
    //   179	191	319	java/io/IOException
    //   323	328	339	finally
  }
  
  public final w<Boolean> a(Uri paramUri)
  {
    return w.b(Boolean.valueOf(c(paramUri)));
  }
  
  public final w<n<BinaryEntity, az>> a(Uri paramUri, boolean paramBoolean)
  {
    return w.b(c(paramUri, paramBoolean));
  }
  
  public final w<n<BinaryEntity, az>> a(Uri paramUri, boolean paramBoolean, long paramLong)
  {
    return w.b(b(paramUri, paramBoolean, paramLong));
  }
  
  public final w<List<n<BinaryEntity, az>>> a(Collection<d> paramCollection, long paramLong)
  {
    n localn = new n(null, az.b.a);
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = paramCollection.iterator();
    while (localIterator.hasNext())
    {
      paramCollection = (d)localIterator.next();
      if (paramCollection == null)
      {
        localArrayList.add(localn);
      }
      else
      {
        Uri localUri = a;
        if (org.c.a.a.a.k.c(b)) {
          paramCollection = b;
        } else {
          paramCollection = r.c(localUri, a);
        }
        if (paramCollection != null)
        {
          if ("image/gif".equalsIgnoreCase(paramCollection))
          {
            Object localObject = f;
            if (T.a((com.truecaller.featuretoggles.e)localObject, com.truecaller.featuretoggles.e.a[120]).a())
            {
              paramCollection = r.a(localUri, a);
              if (paramCollection == null)
              {
                paramCollection = new n(null, az.b.a);
              }
              else
              {
                localObject = c.c(localUri);
                if ((localObject != null) && (c.g.b.k.a("image/gif", c)))
                {
                  localUri = b(localUri);
                  if (localUri == null)
                  {
                    paramCollection = new n(null, az.b.a);
                  }
                  else
                  {
                    paramCollection = Entity.a(-1L, c, 0, localUri, a, b, -1, true, paramCollection.longValue(), Uri.EMPTY);
                    if ((paramCollection instanceof ImageEntity))
                    {
                      paramCollection = new n(paramCollection, null);
                    }
                    else
                    {
                      d.a(paramCollection);
                      paramCollection = new n(null, az.b.a);
                    }
                  }
                }
                else
                {
                  paramCollection = new n(null, az.b.a);
                }
              }
              break label452;
            }
          }
          if ((Entity.c(paramCollection)) && (!"image/gif".equalsIgnoreCase(paramCollection))) {
            paramCollection = c(localUri, false);
          } else if (Entity.d(paramCollection)) {
            paramCollection = b(localUri, false, paramLong);
          } else if (Entity.e(paramCollection)) {
            paramCollection = d(localUri, false);
          } else if (Entity.f(paramCollection)) {
            paramCollection = new n((BinaryEntity)Entity.a("text/x-vcard", 0, localUri.toString(), -1L), null);
          } else {
            paramCollection = new n(null, az.c.a);
          }
        }
        else
        {
          paramCollection = localn;
        }
        label452:
        localArrayList.add(paramCollection);
      }
    }
    return w.b(localArrayList);
  }
  
  public final w<Boolean> a(List<Uri> paramList)
  {
    paramList = paramList.iterator();
    while (paramList.hasNext()) {
      if (!c((Uri)paramList.next())) {
        return w.b(Boolean.FALSE);
      }
    }
    return w.b(Boolean.TRUE);
  }
  
  public final w<Boolean> a(Entity[] paramArrayOfEntity)
  {
    int j = paramArrayOfEntity.length;
    int i = 0;
    boolean bool2;
    for (boolean bool1 = false; i < j; bool1 = bool2)
    {
      Object localObject1 = paramArrayOfEntity[i];
      if (!((Entity)localObject1).b())
      {
        bool2 = bool1;
        if (!((Entity)localObject1).d()) {}
      }
      else
      {
        Object localObject2 = (BinaryEntity)localObject1;
        localObject1 = d(b);
        bool2 = bool1;
        if (localObject1 != null)
        {
          localObject2 = j;
          DownloadManager localDownloadManager = (DownloadManager)a.getSystemService("download");
          if (localDownloadManager != null) {
            localDownloadManager.addCompletedDownload(((File)localObject1).getName(), ((File)localObject1).getName(), true, (String)localObject2, ((File)localObject1).getAbsolutePath(), ((File)localObject1).length(), true);
          }
          bool2 = true;
        }
      }
      i += 1;
    }
    return w.b(Boolean.valueOf(bool1));
  }
  
  public final w<n<BinaryEntity, az>> b(Uri paramUri, boolean paramBoolean)
  {
    return w.b(d(paramUri, paramBoolean));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bc
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */