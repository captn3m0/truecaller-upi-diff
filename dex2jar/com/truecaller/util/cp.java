package com.truecaller.util;

import com.truecaller.androidactors.i;
import com.truecaller.androidactors.k;
import dagger.a.d;
import javax.inject.Provider;

public final class cp
  implements d<i>
{
  private final Provider<k> a;
  
  private cp(Provider<k> paramProvider)
  {
    a = paramProvider;
  }
  
  public static cp a(Provider<k> paramProvider)
  {
    return new cp(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.cp
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */