package com.truecaller.util.background;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.bp;
import com.truecaller.common.background.TrackedWorker;
import com.truecaller.util.ao;
import javax.inject.Inject;

public final class CleanUpBackgroundWorker
  extends TrackedWorker
{
  public static final CleanUpBackgroundWorker.a c = new CleanUpBackgroundWorker.a((byte)0);
  @Inject
  public b b;
  private final Context d;
  
  public CleanUpBackgroundWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    d = paramContext;
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public final b b()
  {
    b localb = b;
    if (localb == null) {
      k.a("analytics");
    }
    return localb;
  }
  
  public final boolean c()
  {
    return true;
  }
  
  public final ListenableWorker.a d()
  {
    ao.a(d);
    ListenableWorker.a locala = ListenableWorker.a.a();
    k.a(locala, "Result.success()");
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.background.CleanUpBackgroundWorker
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */