package com.truecaller.util.background;

import androidx.work.p;
import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d<b>
{
  private final Provider<p> a;
  
  private c(Provider<p> paramProvider)
  {
    a = paramProvider;
  }
  
  public static c a(Provider<p> paramProvider)
  {
    return new c(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.background.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */