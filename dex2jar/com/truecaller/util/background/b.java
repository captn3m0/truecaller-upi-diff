package com.truecaller.util.background;

import androidx.work.f;
import androidx.work.m;
import androidx.work.p;
import com.truecaller.common.background.g;
import com.truecaller.common.background.h;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.inject.Inject;

public final class b
  implements a
{
  private final p a;
  
  @Inject
  public b(p paramp)
  {
    a = paramp;
  }
  
  public final void a(boolean paramBoolean)
  {
    Iterator localIterator = d.a().entrySet().iterator();
    while (localIterator.hasNext())
    {
      Object localObject1 = (Map.Entry)localIterator.next();
      String str = (String)((Map.Entry)localObject1).getKey();
      Object localObject2 = (h)((Map.Entry)localObject1).getValue();
      if (paramBoolean) {
        localObject1 = f.a;
      } else {
        localObject1 = f.b;
      }
      localObject2 = ((h)localObject2).a().a();
      a.a(str, (f)localObject1, (m)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.background.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */