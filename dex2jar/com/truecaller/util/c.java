package com.truecaller.util;

import android.app.Activity;
import android.content.ComponentName;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.webkit.WebView;
import android.widget.Toast;
import com.truecaller.debug.log.a;
import com.truecaller.log.AssertionUtil;
import com.truecaller.old.data.access.Settings;
import java.util.ArrayList;
import java.util.List;

public final class c
  extends b
{
  private final List<ComponentName> a = new ArrayList();
  private boolean b;
  private final Handler c = new Handler(Looper.getMainLooper());
  private final Runnable d = new -..Lambda.c.PskNldMJvJeD-ugFkCEm0s646mg(this);
  
  public final boolean a()
  {
    return !a.isEmpty();
  }
  
  public final void onActivityCreated(Activity paramActivity, Bundle paramBundle) {}
  
  public final void onActivityDestroyed(Activity paramActivity) {}
  
  public final void onActivityPaused(Activity paramActivity)
  {
    a.a(new Object[] { "Activity paused: ", paramActivity.getLocalClassName() });
    c.postDelayed(d, 1000L);
  }
  
  public final void onActivityResumed(Activity paramActivity)
  {
    a.a(new Object[] { "Activity resumed: ", paramActivity.getLocalClassName() });
    c.removeCallbacks(d);
    if (b) {
      try
      {
        new WebView(paramActivity).resumeTimers();
        return;
      }
      catch (Exception paramActivity)
      {
        AssertionUtil.reportThrowableButNeverCrash(paramActivity);
      }
    }
  }
  
  public final void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle) {}
  
  public final void onActivityStarted(Activity paramActivity)
  {
    a.add(paramActivity.getComponentName());
    a.a(new Object[] { "Activity started: ", paramActivity.getLocalClassName() });
  }
  
  public final void onActivityStopped(Activity paramActivity)
  {
    a.remove(paramActivity.getComponentName());
    if ((a.isEmpty()) && (!Settings.e("onboardingDragToDockShown")) && (Settings.e("hasShownWelcome")))
    {
      Toast.makeText(paramActivity, 2131886784, 0).show();
      Settings.a("onboardingDragToDockShown", true);
    }
    a.a(new Object[] { "Activity stopped: ", paramActivity.getLocalClassName() });
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */