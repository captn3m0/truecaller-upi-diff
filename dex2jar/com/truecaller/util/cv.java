package com.truecaller.util;

import android.content.Context;
import com.truecaller.featuretoggles.e;
import dagger.a.d;
import javax.inject.Provider;

public final class cv
  implements d<ba>
{
  private final Provider<Context> a;
  private final Provider<g> b;
  private final Provider<bd> c;
  private final Provider<ch> d;
  private final Provider<bg> e;
  private final Provider<e> f;
  
  private cv(Provider<Context> paramProvider, Provider<g> paramProvider1, Provider<bd> paramProvider2, Provider<ch> paramProvider3, Provider<bg> paramProvider4, Provider<e> paramProvider5)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
  }
  
  public static cv a(Provider<Context> paramProvider, Provider<g> paramProvider1, Provider<bd> paramProvider2, Provider<ch> paramProvider3, Provider<bg> paramProvider4, Provider<e> paramProvider5)
  {
    return new cv(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.cv
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */