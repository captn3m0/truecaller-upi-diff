package com.truecaller.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Handler;
import android.os.Looper;
import android.os.StatFs;
import android.provider.ContactsContract.Contacts;
import android.support.v4.f.g;
import android.text.TextUtils;
import android.widget.ImageView;
import com.truecaller.common.h.am;
import com.truecaller.data.entity.Contact;
import com.truecaller.old.a.b;
import com.truecaller.old.data.access.Settings;
import com.truecaller.ui.components.n;
import com.truecaller.ui.w;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import okhttp3.c;
import okhttp3.y;
import okhttp3.y.a;

@Deprecated
public final class aw
{
  private static final Object b = new Object();
  private static volatile g<String, Bitmap> c;
  private static final Handler d = new Handler(Looper.getMainLooper());
  private static final Bitmap e = Bitmap.createBitmap(1, 1, Bitmap.Config.ALPHA_8);
  public int a = 2131230837;
  private int f = 2131230837;
  private final Context g;
  private final Map<ImageView, n> h = Collections.synchronizedMap(new WeakHashMap());
  
  @Deprecated
  private aw(Context paramContext)
  {
    g = paramContext.getApplicationContext();
    d(g);
  }
  
  @Deprecated
  public static Bitmap a(String paramString)
  {
    if (c == null) {
      return null;
    }
    if (TextUtils.isEmpty(paramString)) {
      return null;
    }
    paramString = (Bitmap)c.get(paramString);
    if (paramString == e) {
      return null;
    }
    return paramString;
  }
  
  public static Uri a(Uri paramUri)
  {
    if ((org.c.a.a.a.k.f(paramUri.getHost(), "truecaller.com")) && ("1".equals(paramUri.getLastPathSegment())))
    {
      ArrayList localArrayList = new ArrayList(paramUri.getPathSegments());
      localArrayList.set(localArrayList.size() - 1, "3");
      return paramUri.buildUpon().path(TextUtils.join("/", localArrayList)).build();
    }
    return paramUri;
  }
  
  public static y a(Context paramContext)
  {
    paramContext = new File(paramContext.getCacheDir(), "picasso-cache");
    long l1 = 5242880L;
    try
    {
      StatFs localStatFs = new StatFs(paramContext.getAbsolutePath());
      long l2 = Math.max(5242880L, localStatFs.getBlockCount() * localStatFs.getBlockSize() / 50L);
      l1 = l2;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      for (;;) {}
    }
    return new y.a().a(15L, TimeUnit.SECONDS).b(20L, TimeUnit.SECONDS).a(new c(paramContext, Math.min(l1, 52428800L))).d();
  }
  
  @Deprecated
  public static void a()
  {
    if (c != null) {
      c.evictAll();
    }
  }
  
  @Deprecated
  public static void a(Context paramContext, String paramString)
  {
    if ((c != null) && (paramString != null)) {
      c.remove(paramString);
    }
    ap.b(paramContext, paramString);
  }
  
  private void a(ImageView paramImageView, Bitmap paramBitmap)
  {
    if (paramImageView != null)
    {
      if ((paramBitmap != null) && (paramBitmap != e))
      {
        paramImageView.setImageBitmap(paramBitmap);
        return;
      }
      int i = f;
      if (i > 0) {
        paramImageView.setImageResource(i);
      }
    }
  }
  
  @Deprecated
  public static void a(String paramString, Bitmap paramBitmap)
  {
    if (paramString == null) {
      return;
    }
    if (paramBitmap == null)
    {
      c.remove(paramString);
      return;
    }
    c.put(paramString, paramBitmap);
  }
  
  @Deprecated
  public static Bitmap b(String paramString)
  {
    return BitmapFactory.decodeFile(paramString);
  }
  
  @Deprecated
  public static aw b(Context paramContext)
  {
    return new aw(paramContext);
  }
  
  @Deprecated
  private static List<Uri> b(Context paramContext, Contact paramContact)
  {
    if ((paramContact != null) && (paramContext != null))
    {
      paramContext = new ArrayList(2);
      Long localLong = paramContact.E();
      if ((localLong != null) && (localLong.longValue() > 0L)) {
        paramContext.add(c(String.valueOf(localLong)));
      }
      if (paramContact.O()) {
        paramContext.add(c(paramContact.p()));
      }
      return paramContext;
    }
    return Collections.emptyList();
  }
  
  @Deprecated
  private void b(n paramn, ImageView paramImageView)
  {
    paramn = new aw.k(this, new aw.j(paramn, paramImageView));
    b.a.execute(paramn);
  }
  
  @Deprecated
  private static Uri c(String paramString)
  {
    return ContactsContract.Contacts.CONTENT_URI.buildUpon().appendPath(paramString).appendEncodedPath("photo_uri").build();
  }
  
  @Deprecated
  public static void c(Context paramContext)
  {
    c.evictAll();
    if (paramContext != null) {
      ap.a(paramContext);
    }
  }
  
  @Deprecated
  private static g<String, Bitmap> d(Context paramContext)
  {
    if (c == null) {
      synchronized (b)
      {
        if (c == null) {
          c = new aw.i(com.truecaller.common.h.k.h(paramContext) / 7);
        }
      }
    }
    return c;
  }
  
  @Deprecated
  public final aw a(int paramInt1, int paramInt2)
  {
    f = paramInt1;
    a = paramInt2;
    return this;
  }
  
  @Deprecated
  public final void a(ImageView paramImageView)
  {
    h.put(paramImageView, null);
  }
  
  @Deprecated
  public final void a(Contact paramContact)
  {
    Object localObject = new ArrayList();
    if (am.a(paramContact.v())) {
      ((ArrayList)localObject).add(Uri.parse(paramContact.v()));
    }
    ((ArrayList)localObject).addAll(b(g, paramContact));
    paramContact = ((ArrayList)localObject).iterator();
    while (paramContact.hasNext())
    {
      localObject = ((Uri)paramContact.next()).toString();
      c.remove(localObject);
      c.remove(w.f((String)localObject));
    }
  }
  
  @Deprecated
  public final void a(Contact paramContact, ImageView paramImageView, aw.e parame)
  {
    aw.c localc = new aw.c(paramContact.v(), true, Settings.d() ^ true, (byte)0);
    h.put(paramImageView, localc);
    String str = paramContact.v();
    Bitmap localBitmap;
    if (am.a(str)) {
      localBitmap = (Bitmap)c.get(str);
    } else {
      localBitmap = null;
    }
    if (localBitmap == e) {
      return;
    }
    if (localBitmap != null)
    {
      a(paramImageView, localBitmap);
      parame.a(paramImageView, localBitmap, str);
      if ((am.a(paramContact.v())) && (paramContact.v().equals(str))) {
        return;
      }
    }
    paramContact = new aw.b(this, paramContact, new aw.j(localc, paramImageView), parame, (byte)0);
    b.a.execute(paramContact);
  }
  
  @Deprecated
  public final void a(n paramn, ImageView paramImageView)
  {
    if (paramImageView != null)
    {
      if (paramn == null) {
        return;
      }
      h.put(paramImageView, paramn);
      Object localObject = am.a(paramn.q());
      if (am.a((CharSequence)localObject)) {
        localObject = (Bitmap)c.get(localObject);
      } else {
        localObject = null;
      }
      if (localObject != null)
      {
        if (localObject != e) {
          a(paramImageView, (Bitmap)localObject);
        }
      }
      else
      {
        b(paramn, paramImageView);
        int i = a;
        if (i > 0) {
          paramImageView.setImageResource(i);
        }
      }
      return;
    }
  }
  
  @Deprecated
  public final void a(String paramString, ImageView paramImageView)
  {
    a(new aw.c(paramString, false, false, (byte)0), paramImageView);
  }
  
  @Deprecated
  final boolean a(aw.j paramj)
  {
    if (b != null)
    {
      n localn = (n)h.get(b);
      if ((localn == null) || (!localn.equals(a))) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.aw
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */