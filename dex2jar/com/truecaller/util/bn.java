package com.truecaller.util;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.provider.ContactsContract.Contacts;
import android.text.TextUtils;
import com.truecaller.analytics.b;
import com.truecaller.common.h.am;
import com.truecaller.common.h.h;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.data.access.m;
import com.truecaller.data.entity.Address;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Link;
import com.truecaller.data.entity.Note;
import com.truecaller.data.entity.StructuredName;
import com.truecaller.messaging.transport.im.j;
import com.truecaller.utils.extensions.g;
import com.truecaller.utils.l;
import java.util.Locale;

public final class bn
{
  final m a;
  private final Context b;
  private final b c;
  private final l d;
  private final j e;
  
  public bn(Context paramContext, b paramb, l paraml, j paramj)
  {
    b = paramContext;
    a = new m(paramContext);
    c = paramb;
    d = paraml;
    e = paramj;
  }
  
  private static long a(long paramLong1, long paramLong2)
  {
    return paramLong1 >> 57 ^ paramLong1 << 7 ^ paramLong2;
  }
  
  private static Contact a(bm parambm)
  {
    long l1 = parambm.b();
    Contact localContact = new Contact();
    localContact.l(parambm.c());
    localContact.c(Long.valueOf(l1));
    localContact.setSource(2);
    boolean bool;
    if (parambm.d() == 1) {
      bool = true;
    } else {
      bool = false;
    }
    localContact.b(bool);
    do
    {
      Object localObject2 = d;
      Object localObject1 = (Cursor)parambm;
      localObject2 = (String)((g)localObject2).a((Cursor)localObject1, bm.a[4]);
      if (((Number)e.a((Cursor)localObject1, bm.a[7])).intValue() == 1) {
        bool = true;
      } else {
        bool = false;
      }
      long l2 = parambm.a();
      switch (((String)localObject2).hashCode())
      {
      default: 
        break;
      case 689862072: 
        if (((String)localObject2).equals("vnd.android.cursor.item/organization")) {
          i = 4;
        }
        break;
      case 684173810: 
        if (((String)localObject2).equals("vnd.android.cursor.item/phone_v2")) {
          i = 0;
        }
        break;
      case 456415478: 
        if (((String)localObject2).equals("vnd.android.cursor.item/website")) {
          i = 2;
        }
        break;
      case -601229436: 
        if (((String)localObject2).equals("vnd.android.cursor.item/postal-address_v2")) {
          i = 3;
        }
        break;
      case -1079210633: 
        if (((String)localObject2).equals("vnd.android.cursor.item/note")) {
          i = 6;
        }
        break;
      case -1079224304: 
        if (((String)localObject2).equals("vnd.android.cursor.item/name")) {
          i = 5;
        }
        break;
      case -1569536764: 
        if (((String)localObject2).equals("vnd.android.cursor.item/email_v2")) {
          i = 1;
        }
        break;
      }
      int i = -1;
      Object localObject3;
      switch (i)
      {
      default: 
        break;
      case 6: 
        localObject1 = (String)r.a((Cursor)localObject1, bm.a[21]);
        if (localObject1 != null)
        {
          localObject2 = new Note();
          ((Note)localObject2).setValue((String)localObject1);
          h = ((Note)localObject2);
        }
        break;
      case 5: 
        localObject2 = new StructuredName();
        ((StructuredName)localObject2).setFamilyName((String)o.a((Cursor)localObject1, bm.a[18]));
        ((StructuredName)localObject2).setGivenName((String)p.a((Cursor)localObject1, bm.a[19]));
        ((StructuredName)localObject2).setMiddleName((String)q.a((Cursor)localObject1, bm.a[20]));
        g = ((StructuredName)localObject2);
        break;
      case 4: 
        localContact.k((String)n.a((Cursor)localObject1, bm.a[17]));
        localContact.f((String)m.a((Cursor)localObject1, bm.a[16]));
        break;
      case 3: 
        localObject3 = new Address();
        ((Address)localObject3).setIsPrimary(bool);
        ((Address)localObject3).setDataPhonebookId(Long.valueOf(l2));
        ((Address)localObject3).setStreet((String)parambm.i.a((Cursor)localObject1, bm.a[12]));
        ((Address)localObject3).setZipCode((String)j.a((Cursor)localObject1, bm.a[13]));
        ((Address)localObject3).setCity((String)k.a((Cursor)localObject1, bm.a[14]));
        String str = am.a((String)l.a((Cursor)localObject1, bm.a[15])).trim();
        localObject2 = h.b(str);
        localObject1 = localObject2;
        if (localObject2 == null) {
          localObject1 = h.c(str);
        }
        if (localObject1 != null) {
          ((Address)localObject3).setCountryCode(am.a(c, Locale.ENGLISH));
        }
        localContact.a((Address)localObject3);
        break;
      case 2: 
        localObject1 = new Link();
        ((Link)localObject1).setIsPrimary(bool);
        ((Link)localObject1).setDataPhonebookId(Long.valueOf(l2));
        ((Link)localObject1).setService("link");
        ((Link)localObject1).setInfo(parambm.e());
        localContact.a((Link)localObject1);
        break;
      case 1: 
        localObject1 = new Link();
        ((Link)localObject1).setIsPrimary(bool);
        ((Link)localObject1).setDataPhonebookId(Long.valueOf(l2));
        ((Link)localObject1).setService("email");
        ((Link)localObject1).setInfo(parambm.e());
        localContact.a((Link)localObject1);
        break;
      case 0: 
        localObject2 = (String)f.a((Cursor)localObject1, bm.a[8]);
        localObject3 = am.d((String)localObject2);
        if (!TextUtils.isEmpty((CharSequence)localObject3))
        {
          localObject3 = new com.truecaller.data.entity.Number((String)localObject3);
          ((com.truecaller.data.entity.Number)localObject3).setIsPrimary(bool);
          ((com.truecaller.data.entity.Number)localObject3).setDataPhonebookId(Long.valueOf(l2));
          ((com.truecaller.data.entity.Number)localObject3).b(((Number)g.a((Cursor)localObject1, bm.a[9])).intValue());
          ((com.truecaller.data.entity.Number)localObject3).e((String)h.a((Cursor)localObject1, bm.a[10]));
          ((com.truecaller.data.entity.Number)localObject3).c((String)localObject2);
          localContact.a((com.truecaller.data.entity.Number)localObject3);
          if (localContact.p() == null) {
            localContact.g(((com.truecaller.data.entity.Number)localObject3).a());
          }
        }
        break;
      }
    } while ((parambm.moveToNext()) && (l1 == parambm.b()));
    return localContact;
  }
  
  /* Error */
  private bn.b a(Cursor paramCursor, Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 25	com/truecaller/util/bn:b	Landroid/content/Context;
    //   4: invokevirtual 301	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   7: aload_2
    //   8: bipush 17
    //   10: anewarray 90	java/lang/String
    //   13: dup
    //   14: iconst_0
    //   15: ldc_w 303
    //   18: aastore
    //   19: dup
    //   20: iconst_1
    //   21: ldc_w 305
    //   24: aastore
    //   25: dup
    //   26: iconst_2
    //   27: ldc_w 307
    //   30: aastore
    //   31: dup
    //   32: iconst_3
    //   33: ldc_w 309
    //   36: aastore
    //   37: dup
    //   38: iconst_4
    //   39: ldc_w 311
    //   42: aastore
    //   43: dup
    //   44: iconst_5
    //   45: ldc_w 313
    //   48: aastore
    //   49: dup
    //   50: bipush 6
    //   52: ldc_w 315
    //   55: aastore
    //   56: dup
    //   57: bipush 7
    //   59: ldc_w 317
    //   62: aastore
    //   63: dup
    //   64: bipush 8
    //   66: ldc_w 319
    //   69: aastore
    //   70: dup
    //   71: bipush 9
    //   73: ldc_w 321
    //   76: aastore
    //   77: dup
    //   78: bipush 10
    //   80: ldc_w 323
    //   83: aastore
    //   84: dup
    //   85: bipush 11
    //   87: ldc_w 325
    //   90: aastore
    //   91: dup
    //   92: bipush 12
    //   94: ldc_w 327
    //   97: aastore
    //   98: dup
    //   99: bipush 13
    //   101: ldc_w 329
    //   104: aastore
    //   105: dup
    //   106: bipush 14
    //   108: ldc_w 331
    //   111: aastore
    //   112: dup
    //   113: bipush 15
    //   115: ldc_w 333
    //   118: aastore
    //   119: dup
    //   120: bipush 16
    //   122: ldc_w 317
    //   125: aastore
    //   126: aload_3
    //   127: aload 4
    //   129: ldc_w 335
    //   132: invokevirtual 341	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   135: astore_3
    //   136: aload_3
    //   137: ifnonnull +21 -> 158
    //   140: new 8	com/truecaller/util/bn$b
    //   143: dup
    //   144: new 343	java/util/ArrayList
    //   147: dup
    //   148: invokespecial 344	java/util/ArrayList:<init>	()V
    //   151: iconst_0
    //   152: iconst_0
    //   153: iconst_0
    //   154: invokespecial 347	com/truecaller/util/bn$b:<init>	(Ljava/util/List;IIB)V
    //   157: areturn
    //   158: new 6	com/truecaller/util/bn$a
    //   161: dup
    //   162: aload_0
    //   163: iconst_0
    //   164: invokespecial 350	com/truecaller/util/bn$a:<init>	(Lcom/truecaller/util/bn;B)V
    //   167: astore_2
    //   168: new 43	com/truecaller/util/bm
    //   171: dup
    //   172: aload_3
    //   173: invokespecial 353	com/truecaller/util/bm:<init>	(Landroid/database/Cursor;)V
    //   176: astore 4
    //   178: aload 4
    //   180: invokevirtual 356	com/truecaller/util/bm:moveToFirst	()Z
    //   183: pop
    //   184: aload_1
    //   185: invokeinterface 357 1 0
    //   190: ifeq +557 -> 747
    //   193: aload_1
    //   194: ldc_w 359
    //   197: invokeinterface 363 2 0
    //   202: istore 6
    //   204: aload_1
    //   205: ldc_w 365
    //   208: invokeinterface 363 2 0
    //   213: istore 8
    //   215: aload_1
    //   216: ldc_w 367
    //   219: invokeinterface 363 2 0
    //   224: istore 7
    //   226: iconst_0
    //   227: istore 5
    //   229: goto +3 -> 232
    //   232: aload 4
    //   234: invokevirtual 370	com/truecaller/util/bm:isAfterLast	()Z
    //   237: ifeq +56 -> 293
    //   240: aload_1
    //   241: invokeinterface 371 1 0
    //   246: istore 12
    //   248: iload 12
    //   250: ifne +6 -> 256
    //   253: goto +40 -> 293
    //   256: aload 4
    //   258: invokevirtual 374	com/truecaller/util/bm:close	()V
    //   261: aload_2
    //   262: invokevirtual 376	com/truecaller/util/bn$a:a	()V
    //   265: aload_2
    //   266: getfield 379	com/truecaller/util/bn$a:b	Ljava/util/List;
    //   269: astore_1
    //   270: new 8	com/truecaller/util/bn$b
    //   273: dup
    //   274: aload_1
    //   275: aload_1
    //   276: invokeinterface 384 1 0
    //   281: aload_2
    //   282: getfield 387	com/truecaller/util/bn$a:c	I
    //   285: iadd
    //   286: iload 5
    //   288: iconst_0
    //   289: invokespecial 347	com/truecaller/util/bn$b:<init>	(Ljava/util/List;IIB)V
    //   292: areturn
    //   293: aload 4
    //   295: invokevirtual 370	com/truecaller/util/bm:isAfterLast	()Z
    //   298: ifeq +11 -> 309
    //   301: ldc2_w 388
    //   304: lstore 13
    //   306: goto +10 -> 316
    //   309: aload 4
    //   311: invokevirtual 46	com/truecaller/util/bm:b	()J
    //   314: lstore 13
    //   316: aload_1
    //   317: invokeinterface 371 1 0
    //   322: ifeq +11 -> 333
    //   325: ldc2_w 390
    //   328: lstore 15
    //   330: goto +13 -> 343
    //   333: aload_1
    //   334: iload 6
    //   336: invokeinterface 395 2 0
    //   341: lstore 15
    //   343: aload 4
    //   345: invokevirtual 370	com/truecaller/util/bm:isAfterLast	()Z
    //   348: ifne +339 -> 687
    //   351: lload 15
    //   353: lload 13
    //   355: lcmp
    //   356: ifge +6 -> 362
    //   359: goto +328 -> 687
    //   362: aload_1
    //   363: invokeinterface 371 1 0
    //   368: ifne +400 -> 768
    //   371: lload 15
    //   373: lload 13
    //   375: lcmp
    //   376: ifle +386 -> 762
    //   379: goto +389 -> 768
    //   382: aload 4
    //   384: invokevirtual 398	com/truecaller/util/bm:getPosition	()I
    //   387: istore 10
    //   389: aload 4
    //   391: getfield 400	com/truecaller/util/bm:b	Lcom/truecaller/utils/extensions/g;
    //   394: aload 4
    //   396: checkcast 80	android/database/Cursor
    //   399: getstatic 83	com/truecaller/util/bm:a	[Lc/l/g;
    //   402: iconst_2
    //   403: aaload
    //   404: invokevirtual 88	com/truecaller/utils/extensions/g:a	(Landroid/database/Cursor;Lc/l/g;)Ljava/lang/Object;
    //   407: checkcast 90	java/lang/String
    //   410: astore_3
    //   411: aload 4
    //   413: invokevirtual 72	com/truecaller/util/bm:d	()I
    //   416: istore 11
    //   418: lconst_0
    //   419: aload 4
    //   421: invokevirtual 52	com/truecaller/util/bm:c	()Ljava/lang/String;
    //   424: invokestatic 203	com/truecaller/common/h/am:a	(Ljava/lang/String;)Ljava/lang/String;
    //   427: invokevirtual 102	java/lang/String:hashCode	()I
    //   430: i2l
    //   431: invokestatic 402	com/truecaller/util/bn:a	(JJ)J
    //   434: aload_3
    //   435: invokevirtual 102	java/lang/String:hashCode	()I
    //   438: i2l
    //   439: invokestatic 402	com/truecaller/util/bn:a	(JJ)J
    //   442: iload 11
    //   444: i2l
    //   445: invokestatic 402	com/truecaller/util/bn:a	(JJ)J
    //   448: lstore 15
    //   450: lload 15
    //   452: aload 4
    //   454: invokevirtual 99	com/truecaller/util/bm:a	()J
    //   457: invokestatic 402	com/truecaller/util/bn:a	(JJ)J
    //   460: aload 4
    //   462: getfield 404	com/truecaller/util/bm:c	Lcom/truecaller/utils/extensions/g;
    //   465: aload 4
    //   467: checkcast 80	android/database/Cursor
    //   470: getstatic 83	com/truecaller/util/bm:a	[Lc/l/g;
    //   473: iconst_3
    //   474: aaload
    //   475: invokevirtual 88	com/truecaller/utils/extensions/g:a	(Landroid/database/Cursor;Lc/l/g;)Ljava/lang/Object;
    //   478: checkcast 94	java/lang/Number
    //   481: invokevirtual 97	java/lang/Number:intValue	()I
    //   484: i2l
    //   485: invokestatic 402	com/truecaller/util/bn:a	(JJ)J
    //   488: lstore 17
    //   490: aload 4
    //   492: invokevirtual 292	com/truecaller/util/bm:moveToNext	()Z
    //   495: ifeq +18 -> 513
    //   498: lload 17
    //   500: lstore 15
    //   502: lload 13
    //   504: aload 4
    //   506: invokevirtual 46	com/truecaller/util/bm:b	()J
    //   509: lcmp
    //   510: ifeq -60 -> 450
    //   513: iload 9
    //   515: ifne +20 -> 535
    //   518: lload 17
    //   520: aload_1
    //   521: iload 8
    //   523: invokeinterface 395 2 0
    //   528: lcmp
    //   529: ifeq +245 -> 774
    //   532: goto +3 -> 535
    //   535: aload 4
    //   537: iload 10
    //   539: invokevirtual 408	com/truecaller/util/bm:moveToPosition	(I)Z
    //   542: pop
    //   543: aload 4
    //   545: invokestatic 410	com/truecaller/util/bn:a	(Lcom/truecaller/util/bm;)Lcom/truecaller/data/entity/Contact;
    //   548: astore 19
    //   550: aload 19
    //   552: lload 17
    //   554: invokestatic 62	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   557: invokevirtual 412	com/truecaller/data/entity/Contact:b	(Ljava/lang/Long;)V
    //   560: aload 19
    //   562: aload_3
    //   563: invokevirtual 414	com/truecaller/data/entity/Contact:m	(Ljava/lang/String;)V
    //   566: iload 9
    //   568: ifne +73 -> 641
    //   571: aload_1
    //   572: iload 7
    //   574: invokeinterface 418 2 0
    //   579: astore 20
    //   581: aload 19
    //   583: aload 20
    //   585: invokevirtual 421	com/truecaller/data/entity/Contact:setTcId	(Ljava/lang/String;)V
    //   588: aload 19
    //   590: invokevirtual 424	com/truecaller/data/entity/Contact:O	()Z
    //   593: ifeq +28 -> 621
    //   596: aload_2
    //   597: astore_3
    //   598: aload_3
    //   599: getfield 427	com/truecaller/util/bn$a:a	Ljava/util/ArrayList;
    //   602: aload 20
    //   604: invokevirtual 430	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   607: pop
    //   608: aload_3
    //   609: invokevirtual 432	com/truecaller/util/bn$a:b	()V
    //   612: aload_3
    //   613: aload 19
    //   615: invokevirtual 435	com/truecaller/util/bn$a:a	(Lcom/truecaller/data/entity/Contact;)V
    //   618: goto +37 -> 655
    //   621: aload 19
    //   623: invokevirtual 438	com/truecaller/data/entity/Contact:getTcId	()Ljava/lang/String;
    //   626: ifnull +29 -> 655
    //   629: aload_2
    //   630: aload 19
    //   632: invokevirtual 438	com/truecaller/data/entity/Contact:getTcId	()Ljava/lang/String;
    //   635: invokevirtual 440	com/truecaller/util/bn$a:a	(Ljava/lang/String;)V
    //   638: goto +17 -> 655
    //   641: aload 19
    //   643: invokevirtual 424	com/truecaller/data/entity/Contact:O	()Z
    //   646: ifeq +9 -> 655
    //   649: aload_2
    //   650: aload 19
    //   652: invokevirtual 435	com/truecaller/util/bn$a:a	(Lcom/truecaller/data/entity/Contact;)V
    //   655: aload 19
    //   657: invokevirtual 424	com/truecaller/data/entity/Contact:O	()Z
    //   660: ifne +114 -> 774
    //   663: iload 5
    //   665: iconst_1
    //   666: iadd
    //   667: istore 5
    //   669: goto +3 -> 672
    //   672: iload 9
    //   674: ifne +103 -> 777
    //   677: aload_1
    //   678: invokeinterface 441 1 0
    //   683: pop
    //   684: goto +93 -> 777
    //   687: aload_2
    //   688: aload_1
    //   689: iload 7
    //   691: invokeinterface 418 2 0
    //   696: invokevirtual 440	com/truecaller/util/bn$a:a	(Ljava/lang/String;)V
    //   699: aload_1
    //   700: invokeinterface 441 1 0
    //   705: pop
    //   706: goto -474 -> 232
    //   709: astore_1
    //   710: aconst_null
    //   711: astore_2
    //   712: goto +7 -> 719
    //   715: astore_2
    //   716: aload_2
    //   717: athrow
    //   718: astore_1
    //   719: aload_2
    //   720: ifnull +20 -> 740
    //   723: aload 4
    //   725: invokevirtual 374	com/truecaller/util/bm:close	()V
    //   728: goto +17 -> 745
    //   731: astore_3
    //   732: aload_2
    //   733: aload_3
    //   734: invokevirtual 445	java/lang/Throwable:addSuppressed	(Ljava/lang/Throwable;)V
    //   737: goto +8 -> 745
    //   740: aload 4
    //   742: invokevirtual 374	com/truecaller/util/bm:close	()V
    //   745: aload_1
    //   746: athrow
    //   747: iconst_0
    //   748: istore 5
    //   750: iconst_0
    //   751: istore 6
    //   753: iconst_0
    //   754: istore 7
    //   756: iconst_0
    //   757: istore 8
    //   759: goto -527 -> 232
    //   762: iconst_0
    //   763: istore 9
    //   765: goto -383 -> 382
    //   768: iconst_1
    //   769: istore 9
    //   771: goto -389 -> 382
    //   774: goto -102 -> 672
    //   777: goto -545 -> 232
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	780	0	this	bn
    //   0	780	1	paramCursor	Cursor
    //   0	780	2	paramUri	Uri
    //   0	780	3	paramString	String
    //   0	780	4	paramArrayOfString	String[]
    //   227	522	5	i	int
    //   202	550	6	j	int
    //   224	531	7	k	int
    //   213	545	8	m	int
    //   513	257	9	n	int
    //   387	151	10	i1	int
    //   416	27	11	i2	int
    //   246	3	12	bool	boolean
    //   304	199	13	l1	long
    //   328	173	15	l2	long
    //   488	65	17	l3	long
    //   548	108	19	localContact	Contact
    //   579	24	20	str	String
    // Exception table:
    //   from	to	target	type
    //   178	226	709	finally
    //   232	248	709	finally
    //   293	301	709	finally
    //   309	316	709	finally
    //   316	325	709	finally
    //   333	343	709	finally
    //   343	351	709	finally
    //   362	371	709	finally
    //   382	450	709	finally
    //   450	498	709	finally
    //   502	513	709	finally
    //   518	532	709	finally
    //   535	566	709	finally
    //   571	596	709	finally
    //   598	618	709	finally
    //   621	638	709	finally
    //   641	655	709	finally
    //   655	663	709	finally
    //   677	684	709	finally
    //   687	706	709	finally
    //   178	226	715	java/lang/Throwable
    //   232	248	715	java/lang/Throwable
    //   293	301	715	java/lang/Throwable
    //   309	316	715	java/lang/Throwable
    //   316	325	715	java/lang/Throwable
    //   333	343	715	java/lang/Throwable
    //   343	351	715	java/lang/Throwable
    //   362	371	715	java/lang/Throwable
    //   382	450	715	java/lang/Throwable
    //   450	498	715	java/lang/Throwable
    //   502	513	715	java/lang/Throwable
    //   518	532	715	java/lang/Throwable
    //   535	566	715	java/lang/Throwable
    //   571	596	715	java/lang/Throwable
    //   598	618	715	java/lang/Throwable
    //   621	638	715	java/lang/Throwable
    //   641	655	715	java/lang/Throwable
    //   655	663	715	java/lang/Throwable
    //   677	684	715	java/lang/Throwable
    //   687	706	715	java/lang/Throwable
    //   716	718	718	finally
    //   723	728	731	java/lang/Throwable
  }
  
  public final Uri a(Uri paramUri)
  {
    if (paramUri != null) {
      if (!b()) {
        return null;
      }
    }
    try
    {
      long l = ContentUris.parseId(ContactsContract.Contacts.lookupContact(b.getContentResolver(), paramUri));
      paramUri = a(Uri.withAppendedPath(paramUri, "data"), l, null, null);
      return paramUri;
    }
    catch (IllegalArgumentException|SQLiteException|SecurityException paramUri) {}
    return null;
    return null;
  }
  
  /* Error */
  final Uri a(Uri paramUri, long paramLong, String paramString, String[] paramArrayOfString)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 7
    //   3: lload_2
    //   4: lconst_1
    //   5: lcmp
    //   6: ifge +21 -> 27
    //   9: getstatic 483	java/lang/System:out	Ljava/io/PrintStream;
    //   12: ldc_w 485
    //   15: lload_2
    //   16: invokestatic 488	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   19: invokevirtual 491	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   22: invokevirtual 496	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   25: aconst_null
    //   26: areturn
    //   27: aload_0
    //   28: getfield 32	com/truecaller/util/bn:a	Lcom/truecaller/data/access/m;
    //   31: getfield 499	com/truecaller/data/access/m:b	Landroid/content/ContentResolver;
    //   34: invokestatic 504	com/truecaller/content/TruecallerContract$ah:a	()Landroid/net/Uri;
    //   37: iconst_3
    //   38: anewarray 90	java/lang/String
    //   41: dup
    //   42: iconst_0
    //   43: ldc_w 367
    //   46: aastore
    //   47: dup
    //   48: iconst_1
    //   49: ldc_w 359
    //   52: aastore
    //   53: dup
    //   54: iconst_2
    //   55: ldc_w 365
    //   58: aastore
    //   59: ldc_w 506
    //   62: iconst_1
    //   63: anewarray 90	java/lang/String
    //   66: dup
    //   67: iconst_0
    //   68: lload_2
    //   69: invokestatic 488	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   72: aastore
    //   73: aconst_null
    //   74: invokevirtual 341	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   77: astore 8
    //   79: aload 7
    //   81: astore 6
    //   83: aload_0
    //   84: aload 8
    //   86: aload_1
    //   87: aload 4
    //   89: aload 5
    //   91: invokespecial 508	com/truecaller/util/bn:a	(Landroid/database/Cursor;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/truecaller/util/bn$b;
    //   94: getfield 510	com/truecaller/util/bn$b:a	Ljava/util/List;
    //   97: astore_1
    //   98: aload_1
    //   99: ifnull +58 -> 157
    //   102: aload 7
    //   104: astore 6
    //   106: aload_1
    //   107: invokeinterface 384 1 0
    //   112: iconst_1
    //   113: if_icmpeq +6 -> 119
    //   116: goto +41 -> 157
    //   119: aload 7
    //   121: astore 6
    //   123: invokestatic 504	com/truecaller/content/TruecallerContract$ah:a	()Landroid/net/Uri;
    //   126: aload_1
    //   127: iconst_0
    //   128: invokeinterface 514 2 0
    //   133: checkcast 58	java/lang/Long
    //   136: invokevirtual 517	java/lang/Long:longValue	()J
    //   139: invokestatic 521	android/content/ContentUris:withAppendedId	(Landroid/net/Uri;J)Landroid/net/Uri;
    //   142: astore_1
    //   143: aload 8
    //   145: ifnull +10 -> 155
    //   148: aload 8
    //   150: invokeinterface 522 1 0
    //   155: aload_1
    //   156: areturn
    //   157: aload 8
    //   159: ifnull +10 -> 169
    //   162: aload 8
    //   164: invokeinterface 522 1 0
    //   169: aconst_null
    //   170: areturn
    //   171: astore_1
    //   172: goto +9 -> 181
    //   175: astore_1
    //   176: aload_1
    //   177: astore 6
    //   179: aload_1
    //   180: athrow
    //   181: aload 8
    //   183: ifnull +37 -> 220
    //   186: aload 6
    //   188: ifnull +25 -> 213
    //   191: aload 8
    //   193: invokeinterface 522 1 0
    //   198: goto +22 -> 220
    //   201: astore 4
    //   203: aload 6
    //   205: aload 4
    //   207: invokevirtual 445	java/lang/Throwable:addSuppressed	(Ljava/lang/Throwable;)V
    //   210: goto +10 -> 220
    //   213: aload 8
    //   215: invokeinterface 522 1 0
    //   220: aload_1
    //   221: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	222	0	this	bn
    //   0	222	1	paramUri	Uri
    //   0	222	2	paramLong	long
    //   0	222	4	paramString	String
    //   0	222	5	paramArrayOfString	String[]
    //   81	123	6	localObject1	Object
    //   1	119	7	localObject2	Object
    //   77	137	8	localCursor	Cursor
    // Exception table:
    //   from	to	target	type
    //   83	98	171	finally
    //   106	116	171	finally
    //   123	143	171	finally
    //   179	181	171	finally
    //   83	98	175	java/lang/Throwable
    //   106	116	175	java/lang/Throwable
    //   123	143	175	java/lang/Throwable
    //   191	198	201	java/lang/Throwable
  }
  
  /* Error */
  public final void a()
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 454	com/truecaller/util/bn:b	()Z
    //   4: ifne +4 -> 8
    //   7: return
    //   8: aload_0
    //   9: getfield 25	com/truecaller/util/bn:b	Landroid/content/Context;
    //   12: invokevirtual 528	android/content/Context:getApplicationContext	()Landroid/content/Context;
    //   15: checkcast 530	com/truecaller/common/b/a
    //   18: astore_3
    //   19: aload_3
    //   20: invokevirtual 532	com/truecaller/common/b/a:o	()Z
    //   23: ifeq +14 -> 37
    //   26: aload_3
    //   27: invokevirtual 535	com/truecaller/common/b/a:H	()Ljava/lang/String;
    //   30: invokestatic 263	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   33: ifeq +4 -> 37
    //   36: return
    //   37: new 537	com/truecaller/analytics/e$a
    //   40: dup
    //   41: ldc_w 539
    //   44: invokespecial 540	com/truecaller/analytics/e$a:<init>	(Ljava/lang/String;)V
    //   47: astore 5
    //   49: aload 5
    //   51: ldc_w 542
    //   54: ldc_w 544
    //   57: invokestatic 549	com/truecaller/old/data/access/Settings:e	(Ljava/lang/String;)Z
    //   60: iconst_1
    //   61: ixor
    //   62: invokevirtual 552	com/truecaller/analytics/e$a:a	(Ljava/lang/String;Z)Lcom/truecaller/analytics/e$a;
    //   65: pop
    //   66: invokestatic 555	java/lang/System:currentTimeMillis	()J
    //   69: lstore_1
    //   70: aload_0
    //   71: getfield 32	com/truecaller/util/bn:a	Lcom/truecaller/data/access/m;
    //   74: invokevirtual 558	com/truecaller/data/access/m:a	()Landroid/database/Cursor;
    //   77: astore 6
    //   79: aconst_null
    //   80: astore 4
    //   82: aload 4
    //   84: astore_3
    //   85: aload_0
    //   86: aload 6
    //   88: getstatic 564	android/provider/ContactsContract$Data:CONTENT_URI	Landroid/net/Uri;
    //   91: ldc_w 566
    //   94: iconst_1
    //   95: anewarray 90	java/lang/String
    //   98: dup
    //   99: iconst_0
    //   100: ldc_w 568
    //   103: aastore
    //   104: invokespecial 508	com/truecaller/util/bn:a	(Landroid/database/Cursor;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/truecaller/util/bn$b;
    //   107: astore 7
    //   109: aload 4
    //   111: astore_3
    //   112: ldc_w 544
    //   115: invokestatic 570	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;)Z
    //   118: ifne +40 -> 158
    //   121: aload 4
    //   123: astore_3
    //   124: ldc_w 544
    //   127: iconst_1
    //   128: invokestatic 573	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   131: aload 4
    //   133: astore_3
    //   134: aload_0
    //   135: getfield 38	com/truecaller/util/bn:e	Lcom/truecaller/messaging/transport/im/j;
    //   138: invokeinterface 577 1 0
    //   143: ifeq +15 -> 158
    //   146: aload 4
    //   148: astore_3
    //   149: aload_0
    //   150: getfield 38	com/truecaller/util/bn:e	Lcom/truecaller/messaging/transport/im/j;
    //   153: invokeinterface 579 1 0
    //   158: aload 4
    //   160: astore_3
    //   161: aload 5
    //   163: ldc_w 581
    //   166: ldc_w 583
    //   169: invokevirtual 586	com/truecaller/analytics/e$a:a	(Ljava/lang/String;Ljava/lang/String;)Lcom/truecaller/analytics/e$a;
    //   172: pop
    //   173: aload 4
    //   175: astore_3
    //   176: aload 5
    //   178: ldc_w 588
    //   181: aload 7
    //   183: getfield 589	com/truecaller/util/bn$b:c	I
    //   186: invokevirtual 592	com/truecaller/analytics/e$a:a	(Ljava/lang/String;I)Lcom/truecaller/analytics/e$a;
    //   189: pop
    //   190: aload 4
    //   192: astore_3
    //   193: aload 5
    //   195: ldc_w 594
    //   198: aload 7
    //   200: getfield 596	com/truecaller/util/bn$b:b	I
    //   203: invokevirtual 592	com/truecaller/analytics/e$a:a	(Ljava/lang/String;I)Lcom/truecaller/analytics/e$a;
    //   206: pop
    //   207: aload 6
    //   209: ifnull +10 -> 219
    //   212: aload 6
    //   214: invokeinterface 522 1 0
    //   219: aload 5
    //   221: invokestatic 555	java/lang/System:currentTimeMillis	()J
    //   224: lload_1
    //   225: lsub
    //   226: l2d
    //   227: invokestatic 601	java/lang/Double:valueOf	(D)Ljava/lang/Double;
    //   230: putfield 604	com/truecaller/analytics/e$a:a	Ljava/lang/Double;
    //   233: aload_0
    //   234: getfield 34	com/truecaller/util/bn:c	Lcom/truecaller/analytics/b;
    //   237: aload 5
    //   239: invokevirtual 607	com/truecaller/analytics/e$a:a	()Lcom/truecaller/analytics/e;
    //   242: invokeinterface 612 2 0
    //   247: return
    //   248: astore 4
    //   250: goto +11 -> 261
    //   253: astore 4
    //   255: aload 4
    //   257: astore_3
    //   258: aload 4
    //   260: athrow
    //   261: aload 6
    //   263: ifnull +35 -> 298
    //   266: aload_3
    //   267: ifnull +24 -> 291
    //   270: aload 6
    //   272: invokeinterface 522 1 0
    //   277: goto +21 -> 298
    //   280: astore 6
    //   282: aload_3
    //   283: aload 6
    //   285: invokevirtual 445	java/lang/Throwable:addSuppressed	(Ljava/lang/Throwable;)V
    //   288: goto +10 -> 298
    //   291: aload 6
    //   293: invokeinterface 522 1 0
    //   298: aload 4
    //   300: athrow
    //   301: astore_3
    //   302: goto +40 -> 342
    //   305: astore_3
    //   306: aload_3
    //   307: ldc_w 614
    //   310: invokestatic 619	com/truecaller/log/d:a	(Ljava/lang/Throwable;Ljava/lang/String;)V
    //   313: aload 5
    //   315: ldc_w 581
    //   318: ldc_w 621
    //   321: invokevirtual 586	com/truecaller/analytics/e$a:a	(Ljava/lang/String;Ljava/lang/String;)Lcom/truecaller/analytics/e$a;
    //   324: pop
    //   325: aload 5
    //   327: invokestatic 555	java/lang/System:currentTimeMillis	()J
    //   330: lload_1
    //   331: lsub
    //   332: l2d
    //   333: invokestatic 601	java/lang/Double:valueOf	(D)Ljava/lang/Double;
    //   336: putfield 604	com/truecaller/analytics/e$a:a	Ljava/lang/Double;
    //   339: goto -106 -> 233
    //   342: aload 5
    //   344: invokestatic 555	java/lang/System:currentTimeMillis	()J
    //   347: lload_1
    //   348: lsub
    //   349: l2d
    //   350: invokestatic 601	java/lang/Double:valueOf	(D)Ljava/lang/Double;
    //   353: putfield 604	com/truecaller/analytics/e$a:a	Ljava/lang/Double;
    //   356: aload_0
    //   357: getfield 34	com/truecaller/util/bn:c	Lcom/truecaller/analytics/b;
    //   360: aload 5
    //   362: invokevirtual 607	com/truecaller/analytics/e$a:a	()Lcom/truecaller/analytics/e;
    //   365: invokeinterface 612 2 0
    //   370: aload_3
    //   371: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	372	0	this	bn
    //   69	279	1	l	long
    //   18	265	3	localObject1	Object
    //   301	1	3	localObject2	Object
    //   305	66	3	localRuntimeException	RuntimeException
    //   80	111	4	localObject3	Object
    //   248	1	4	localObject4	Object
    //   253	46	4	localThrowable1	Throwable
    //   47	314	5	locala	com.truecaller.analytics.e.a
    //   77	194	6	localCursor	Cursor
    //   280	12	6	localThrowable2	Throwable
    //   107	92	7	localb	bn.b
    // Exception table:
    //   from	to	target	type
    //   85	109	248	finally
    //   112	121	248	finally
    //   124	131	248	finally
    //   134	146	248	finally
    //   149	158	248	finally
    //   161	173	248	finally
    //   176	190	248	finally
    //   193	207	248	finally
    //   258	261	248	finally
    //   85	109	253	java/lang/Throwable
    //   112	121	253	java/lang/Throwable
    //   124	131	253	java/lang/Throwable
    //   134	146	253	java/lang/Throwable
    //   149	158	253	java/lang/Throwable
    //   161	173	253	java/lang/Throwable
    //   176	190	253	java/lang/Throwable
    //   193	207	253	java/lang/Throwable
    //   270	277	280	java/lang/Throwable
    //   70	79	301	finally
    //   212	219	301	finally
    //   270	277	301	finally
    //   282	288	301	finally
    //   291	298	301	finally
    //   298	301	301	finally
    //   306	325	301	finally
    //   70	79	305	java/lang/RuntimeException
    //   212	219	305	java/lang/RuntimeException
    //   270	277	305	java/lang/RuntimeException
    //   282	288	305	java/lang/RuntimeException
    //   291	298	305	java/lang/RuntimeException
    //   298	301	305	java/lang/RuntimeException
  }
  
  final boolean b()
  {
    return d.a(new String[] { "android.permission.READ_CONTACTS" });
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bn
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */