package com.truecaller.util;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipData.Item;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.v4.content.d;
import android.telephony.TelephonyManager;
import com.truecaller.common.b.a;
import com.truecaller.common.h.k;
import com.truecaller.e.b;
import com.truecaller.notifications.NotificationHandlerService;
import com.truecaller.old.data.access.Settings;
import com.truecaller.utils.extensions.i;

final class am
  implements al
{
  private static final String[] a = { "SM-G900", "SM-A500" };
  private final Context b;
  
  am(Context paramContext)
  {
    b = paramContext;
  }
  
  public final Uri a(long paramLong, String paramString, boolean paramBoolean)
  {
    return y.a(paramLong, paramString, paramBoolean);
  }
  
  public final Uri a(Intent paramIntent, Integer paramInteger)
  {
    return TempContentProvider.a(b, paramIntent, paramInteger);
  }
  
  public final void a(BroadcastReceiver paramBroadcastReceiver)
  {
    d.a(b).a(paramBroadcastReceiver);
  }
  
  public final void a(BroadcastReceiver paramBroadcastReceiver, String... paramVarArgs)
  {
    i.a(b, paramBroadcastReceiver, paramVarArgs);
  }
  
  public final void a(Intent paramIntent)
  {
    d.a(b).a(paramIntent);
  }
  
  public final void a(String paramString1, String paramString2)
  {
    r.a(b, paramString2, paramString1);
  }
  
  public final boolean a()
  {
    return ((a)b.getApplicationContext()).p();
  }
  
  public final boolean b()
  {
    return ((a)b.getApplicationContext()).o();
  }
  
  public final boolean b(Intent paramIntent)
  {
    return paramIntent.resolveActivity(b.getPackageManager()) != null;
  }
  
  public final boolean c()
  {
    return Settings.e("initialContactsSyncComplete");
  }
  
  public final String d()
  {
    Object localObject = (ClipboardManager)b.getSystemService("clipboard");
    if (localObject != null)
    {
      localObject = ((ClipboardManager)localObject).getPrimaryClip();
      if (localObject != null) {
        return ((ClipData)localObject).getItemAt(0).getText().toString();
      }
    }
    return null;
  }
  
  public final boolean e()
  {
    return k.a(b);
  }
  
  public final long f()
  {
    return k.b(b);
  }
  
  public final int g()
  {
    return ((AudioManager)b.getSystemService("audio")).getRingerMode();
  }
  
  public final boolean h()
  {
    return !CallMonitoringReceiver.a().equals(TelephonyManager.EXTRA_STATE_IDLE);
  }
  
  @TargetApi(21)
  public final boolean i()
  {
    if (Build.VERSION.SDK_INT < 21) {
      return false;
    }
    int i = NotificationHandlerService.a();
    if (i == 0) {
      return false;
    }
    return i != 1;
  }
  
  public final void j()
  {
    a.F().a(new int[0]);
  }
  
  public final boolean k()
  {
    return !k.f();
  }
  
  public final boolean l()
  {
    TelephonyManager localTelephonyManager = (TelephonyManager)b.getSystemService("phone");
    return ((localTelephonyManager instanceof b)) && (((b)localTelephonyManager).a());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.am
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */