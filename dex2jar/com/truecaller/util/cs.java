package com.truecaller.util;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import dagger.a.d;
import javax.inject.Provider;

public final class cs
  implements d<f<aa>>
{
  private final Provider<i> a;
  private final Provider<aa> b;
  
  private cs(Provider<i> paramProvider, Provider<aa> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static cs a(Provider<i> paramProvider, Provider<aa> paramProvider1)
  {
    return new cs(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.cs
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */