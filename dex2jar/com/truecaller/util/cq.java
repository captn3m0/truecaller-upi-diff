package com.truecaller.util;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class cq
  implements d<u>
{
  private final Provider<Context> a;
  
  private cq(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static cq a(Provider<Context> paramProvider)
  {
    return new cq(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.cq
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */