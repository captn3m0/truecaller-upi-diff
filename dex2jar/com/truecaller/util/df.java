package com.truecaller.util;

import android.content.Context;
import android.net.Uri;
import com.d.b.aa;
import com.d.b.ac;

public final class df
  extends ac
{
  private final Context a;
  
  public df(Context paramContext)
  {
    a = paramContext.getApplicationContext();
  }
  
  /* Error */
  public final com.d.b.ac.a a(aa paramaa, int paramInt)
    throws java.io.IOException
  {
    // Byte code:
    //   0: new 27	android/media/MediaMetadataRetriever
    //   3: dup
    //   4: invokespecial 28	android/media/MediaMetadataRetriever:<init>	()V
    //   7: astore_3
    //   8: aload_3
    //   9: aload_0
    //   10: getfield 19	com/truecaller/util/df:a	Landroid/content/Context;
    //   13: aload_1
    //   14: getfield 34	com/d/b/aa:d	Landroid/net/Uri;
    //   17: invokevirtual 38	android/media/MediaMetadataRetriever:setDataSource	(Landroid/content/Context;Landroid/net/Uri;)V
    //   20: aload_3
    //   21: invokevirtual 42	android/media/MediaMetadataRetriever:getFrameAtTime	()Landroid/graphics/Bitmap;
    //   24: astore_1
    //   25: aload_3
    //   26: invokevirtual 45	android/media/MediaMetadataRetriever:release	()V
    //   29: goto +16 -> 45
    //   32: astore_1
    //   33: aload_3
    //   34: invokevirtual 45	android/media/MediaMetadataRetriever:release	()V
    //   37: aload_1
    //   38: athrow
    //   39: aload_3
    //   40: invokevirtual 45	android/media/MediaMetadataRetriever:release	()V
    //   43: aconst_null
    //   44: astore_1
    //   45: aload_1
    //   46: ifnonnull +5 -> 51
    //   49: aconst_null
    //   50: areturn
    //   51: new 47	com/d/b/ac$a
    //   54: dup
    //   55: aload_1
    //   56: getstatic 53	com/d/b/w$d:b	Lcom/d/b/w$d;
    //   59: invokespecial 56	com/d/b/ac$a:<init>	(Landroid/graphics/Bitmap;Lcom/d/b/w$d;)V
    //   62: areturn
    //   63: astore_1
    //   64: goto -25 -> 39
    //   67: astore_3
    //   68: goto -23 -> 45
    //   71: astore_3
    //   72: goto -35 -> 37
    //   75: astore_1
    //   76: goto -33 -> 43
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	79	0	this	df
    //   0	79	1	paramaa	aa
    //   0	79	2	paramInt	int
    //   7	33	3	localMediaMetadataRetriever	android.media.MediaMetadataRetriever
    //   67	1	3	localRuntimeException1	RuntimeException
    //   71	1	3	localRuntimeException2	RuntimeException
    // Exception table:
    //   from	to	target	type
    //   8	25	32	finally
    //   8	25	63	java/lang/RuntimeException
    //   25	29	67	java/lang/RuntimeException
    //   33	37	71	java/lang/RuntimeException
    //   39	43	75	java/lang/RuntimeException
  }
  
  public final boolean a(aa paramaa)
  {
    paramaa = d;
    return (paramaa.isHierarchical()) && (paramaa.getBooleanQueryParameter("com.truecaller.util.VideoRequestHandler.IS_VIDEO", false));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.df
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */