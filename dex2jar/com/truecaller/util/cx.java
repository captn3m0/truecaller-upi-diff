package com.truecaller.util;

import android.content.Context;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class cx
  implements d<bg>
{
  private final Provider<com.truecaller.multisim.h> a;
  private final Provider<com.truecaller.messaging.h> b;
  private final Provider<Context> c;
  
  private cx(Provider<com.truecaller.multisim.h> paramProvider, Provider<com.truecaller.messaging.h> paramProvider1, Provider<Context> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static bg a(com.truecaller.multisim.h paramh, com.truecaller.messaging.h paramh1, Context paramContext)
  {
    return (bg)g.a(new bh(paramh, paramh1, paramContext), "Cannot return null from a non-@Nullable @Provides method");
  }
  
  public static cx a(Provider<com.truecaller.multisim.h> paramProvider, Provider<com.truecaller.messaging.h> paramProvider1, Provider<Context> paramProvider2)
  {
    return new cx(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.cx
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */