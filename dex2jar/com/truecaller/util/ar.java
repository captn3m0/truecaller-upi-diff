package com.truecaller.util;

import c.g.b.k;
import java.io.File;
import javax.inject.Inject;

public final class ar
  implements aq
{
  public final boolean a(String paramString)
    throws SecurityException
  {
    k.b(paramString, "absolutePath");
    return new File(paramString).isDirectory();
  }
  
  public final boolean b(String paramString)
    throws SecurityException
  {
    k.b(paramString, "absolutePath");
    return new File(paramString).mkdirs();
  }
  
  public final boolean c(String paramString)
    throws SecurityException
  {
    k.b(paramString, "absolutePath");
    return new File(paramString).delete();
  }
  
  public final boolean d(String paramString)
    throws SecurityException
  {
    k.b(paramString, "absolutePath");
    return new File(paramString).exists();
  }
  
  public final boolean e(String paramString)
    throws SecurityException
  {
    k.b(paramString, "absolutePath");
    return new File(paramString).createNewFile();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ar
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */