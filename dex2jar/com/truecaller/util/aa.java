package com.truecaller.util;

import android.net.Uri;
import com.truecaller.androidactors.w;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.truepay.SenderInfo;
import java.util.List;
import java.util.Map;

public abstract interface aa
{
  public abstract w<Boolean> a();
  
  public abstract w<Contact> a(long paramLong);
  
  public abstract w<Uri> a(Uri paramUri);
  
  public abstract w<Contact> a(String paramString);
  
  public abstract w<Map<Uri, x>> a(List<Uri> paramList);
  
  public abstract void a(HistoryEvent paramHistoryEvent);
  
  public abstract w<String> b(Uri paramUri);
  
  public abstract w<Contact> b(String paramString);
  
  public abstract w<x> c(Uri paramUri);
  
  public abstract w<Boolean> c(String paramString);
  
  public abstract w<SenderInfo> d(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.util.aa
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */