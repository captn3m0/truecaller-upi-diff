package com.truecaller.util;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class cu
  implements d<al>
{
  private final Provider<Context> a;
  
  private cu(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static cu a(Provider<Context> paramProvider)
  {
    return new cu(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.cu
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */