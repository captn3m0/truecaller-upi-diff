package com.truecaller.util;

import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.messaging.h;
import com.truecaller.utils.d;
import javax.inject.Inject;

public final class bw
  implements bv
{
  private final String a;
  private final String b;
  private final String c;
  private final Context d;
  private final h e;
  private final d f;
  
  @Inject
  public bw(Context paramContext, h paramh, d paramd)
  {
    d = paramContext;
    e = paramh;
    f = paramd;
    a = "/raw/tc_message_tone";
    b = "/raw/tc_receive_money";
    c = "/2131821031";
  }
  
  public final Uri a()
  {
    if (e.q())
    {
      Object localObject = e.r();
      if (localObject == null) {
        return null;
      }
      k.a(localObject, "settings.messagingRingtone ?: return null");
      localObject = Uri.parse((String)localObject);
      RingtoneManager localRingtoneManager = new RingtoneManager(d);
      localRingtoneManager.setType(2);
      if (localRingtoneManager.getRingtonePosition((Uri)localObject) != -1) {
        return (Uri)localObject;
      }
    }
    return c();
  }
  
  public final boolean b()
  {
    return e.s();
  }
  
  public final Uri c()
  {
    Object localObject = new StringBuilder("android.resource://");
    ((StringBuilder)localObject).append(f.n());
    ((StringBuilder)localObject).append(a);
    localObject = Uri.parse(((StringBuilder)localObject).toString());
    k.a(localObject, "Uri.parse(\"android.resou…ame()}$MESSAGE_TONE_URI\")");
    return (Uri)localObject;
  }
  
  public final Uri d()
  {
    Object localObject = new StringBuilder("android.resource://");
    ((StringBuilder)localObject).append(f.n());
    ((StringBuilder)localObject).append(b);
    localObject = Uri.parse(((StringBuilder)localObject).toString());
    k.a(localObject, "Uri.parse(\"android.resou…Name()}$TC_PAY_TONE_URI\")");
    return (Uri)localObject;
  }
  
  public final Uri e()
  {
    Object localObject = new StringBuilder("android.resource://");
    ((StringBuilder)localObject).append(f.n());
    ((StringBuilder)localObject).append(c);
    localObject = Uri.parse(((StringBuilder)localObject).toString());
    k.a(localObject, "Uri.parse(\"android.resou…eName()}$FLASH_TONE_URI\")");
    return (Uri)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bw
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */