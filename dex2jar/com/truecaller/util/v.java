package com.truecaller.util;

import android.content.Context;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.data.entity.Number;
import com.truecaller.search.e;
import com.truecaller.search.f;
import com.truecaller.utils.l;

final class v
  implements u
{
  private final Context a;
  
  v(Context paramContext)
  {
    a = paramContext;
  }
  
  public final boolean a(Number paramNumber)
  {
    Context localContext = a;
    return (((bk)localContext.getApplicationContext()).a().bw().a(new String[] { "android.permission.READ_CONTACTS" })) && (e.a().a(localContext, paramNumber));
  }
  
  public final boolean a(String paramString)
  {
    return f.a(a, paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.v
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */