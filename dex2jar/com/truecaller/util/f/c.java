package com.truecaller.util.f;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import c.f.e;
import c.g.b.k;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.VideoEntity;
import com.truecaller.util.bd;
import com.truecaller.util.de;
import com.truecaller.utils.extensions.r;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import net.ypresto.androidtranscoder.a.a;

public final class c
  implements b
{
  private final ConcurrentMap<Long, Future<Void>> a;
  private final Context b;
  private final bd c;
  
  public c(Context paramContext, bd parambd)
  {
    b = paramContext;
    c = parambd;
    a = ((ConcurrentMap)new ConcurrentHashMap());
  }
  
  private final VideoEntity a(Uri paramUri, long paramLong)
  {
    Object localObject = c.a(paramUri);
    Long localLong = r.a(paramUri, b);
    long l;
    if (localLong != null) {
      l = localLong.longValue();
    } else {
      l = -1L;
    }
    "Compressed video Size: ".concat(String.valueOf(l));
    if ((localObject != null) && (d != null))
    {
      localObject = Entity.a(paramLong, d, paramUri, a, b, c, l, Uri.EMPTY);
      paramUri = (Uri)localObject;
      if (!(localObject instanceof VideoEntity)) {
        paramUri = null;
      }
      return (VideoEntity)paramUri;
    }
    return null;
  }
  
  private final VideoEntity a(String paramString, long paramLong)
  {
    paramString = Uri.fromFile(new File(paramString));
    k.a(paramString, "uri");
    return a(paramString, paramLong);
  }
  
  public final VideoEntity a(BinaryEntity paramBinaryEntity)
  {
    k.b(paramBinaryEntity, "binaryEntity");
    Uri localUri = b;
    k.a(localUri, "binaryEntity.content");
    File localFile = e.a("Video", ".mp4", b.getCacheDir());
    for (;;)
    {
      try
      {
        localObject1 = b.getContentResolver().openFileDescriptor(localUri, "r");
        if (localObject1 != null) {
          localObject1 = ((ParcelFileDescriptor)localObject1).getFileDescriptor();
        } else {
          localObject1 = null;
        }
        locala = new c.a();
      }
      catch (FileNotFoundException paramBinaryEntity)
      {
        Object localObject1;
        c.a locala;
        Object localObject3;
        AssertionUtil.reportThrowableButNeverCrash((Throwable)paramBinaryEntity);
        return null;
      }
      try
      {
        try
        {
          localObject1 = net.ypresto.androidtranscoder.a.a().a((FileDescriptor)localObject1, localFile.getAbsolutePath(), (net.ypresto.androidtranscoder.b.a)new a(), (a.a)locala);
          ((Map)a).put(Long.valueOf(h), localObject1);
          ((Future)localObject1).get();
          localObject1 = localFile.getPath();
          k.a(localObject1, "outputFile.path");
          localObject1 = a((String)localObject1, h);
        }
        finally {}
      }
      catch (ExecutionException localExecutionException) {}catch (CancellationException localCancellationException) {}
    }
    localFile.delete();
    throw ((Throwable)new CancellationException());
    localObject3 = r.a(localUri, b, null);
    if (localObject3 == null)
    {
      a.remove(Long.valueOf(h));
      return null;
    }
    localObject3 = ((File)localObject3).getPath();
    k.a(localObject3, "tempFile.path");
    localObject3 = a((String)localObject3, h);
    a.remove(Long.valueOf(h));
    return (VideoEntity)localObject3;
    a.remove(Long.valueOf(h));
    throw ((Throwable)localObject3);
  }
  
  public final void b(BinaryEntity paramBinaryEntity)
  {
    k.b(paramBinaryEntity, "binaryEntity");
    paramBinaryEntity = (Future)a.get(Long.valueOf(h));
    if (paramBinaryEntity != null)
    {
      paramBinaryEntity.cancel(true);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.f.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */