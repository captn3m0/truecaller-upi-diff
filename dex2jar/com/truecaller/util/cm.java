package com.truecaller.util;

import android.content.res.AssetManager;
import android.net.Uri;
import com.google.c.a.c;
import com.google.common.base.Strings;
import com.truecaller.log.AssertionUtil;
import java.io.IOException;
import java.io.InputStream;

public final class cm
  implements c
{
  private final AssetManager a;
  
  public cm(AssetManager paramAssetManager)
  {
    a = paramAssetManager;
  }
  
  public final InputStream a(String paramString)
  {
    try
    {
      if (!Strings.isNullOrEmpty(paramString))
      {
        paramString = Uri.parse(paramString).getLastPathSegment();
        try
        {
          paramString = a.open("libphonenumber/".concat(String.valueOf(paramString)));
          return paramString;
        }
        catch (IOException paramString)
        {
          AssertionUtil.reportThrowableButNeverCrash(paramString);
        }
      }
      return null;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.cm
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */