package com.truecaller.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.a;
import com.truecaller.utils.extensions.r;

public final class bh
  implements bg
{
  private final com.truecaller.multisim.h a;
  private final com.truecaller.messaging.h b;
  private final Context c;
  
  public bh(com.truecaller.multisim.h paramh, com.truecaller.messaging.h paramh1, Context paramContext)
  {
    a = paramh;
    b = paramh1;
    c = paramContext;
  }
  
  private final Long c(int paramInt)
  {
    Object localObject = a.a(paramInt);
    if (localObject == null) {
      return null;
    }
    k.a(localObject, "multiSimManager.getSimIn…SlotIndex) ?: return null");
    localObject = a.c(b);
    k.a(localObject, "multiSimManager.getCarri…uration(simInfo.simToken)");
    return Long.valueOf(((a)localObject).j());
  }
  
  @SuppressLint({"SwitchIntDef"})
  public final long a(int paramInt)
  {
    if (paramInt != 2)
    {
      if (!a.j())
      {
        localObject = a;
        localObject = ((com.truecaller.multisim.h)localObject).c(((com.truecaller.multisim.h)localObject).f());
        k.a(localObject, "multiSimManager.getCarri…mManager.defaultSimToken)");
        return ((a)localObject).j();
      }
      Long localLong1 = c(0);
      Long localLong2 = c(1);
      if ((localLong1 != null) && (localLong2 != null)) {
        return Math.min(localLong1.longValue(), localLong2.longValue());
      }
      Object localObject = localLong1;
      if (localLong1 == null) {
        localObject = localLong2;
      }
      if (localObject != null) {
        return ((Long)localObject).longValue();
      }
      return 307200L;
    }
    return b.L();
  }
  
  public final long a(long paramLong)
  {
    return paramLong / 250000L;
  }
  
  public final String a(Uri paramUri)
  {
    k.b(paramUri, "uri");
    return r.c(paramUri, c);
  }
  
  public final long b(int paramInt)
  {
    return paramInt * 2000000 / 8L;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bh
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */