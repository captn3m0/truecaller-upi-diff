package com.truecaller.util;

import android.content.ContentResolver;
import dagger.a.d;
import javax.inject.Provider;

public final class cj
  implements d<ci>
{
  private final Provider<ContentResolver> a;
  
  private cj(Provider<ContentResolver> paramProvider)
  {
    a = paramProvider;
  }
  
  public static cj a(Provider<ContentResolver> paramProvider)
  {
    return new cj(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.cj
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */