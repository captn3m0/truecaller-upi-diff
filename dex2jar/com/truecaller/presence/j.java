package com.truecaller.presence;

import com.truecaller.common.g.a;
import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d<r>
{
  private final g a;
  private final Provider<a> b;
  
  private j(g paramg, Provider<a> paramProvider)
  {
    a = paramg;
    b = paramProvider;
  }
  
  public static j a(g paramg, Provider<a> paramProvider)
  {
    return new j(paramg, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */