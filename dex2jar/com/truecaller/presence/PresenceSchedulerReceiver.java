package com.truecaller.presence;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import com.truecaller.androidactors.f;
import com.truecaller.bk;
import com.truecaller.bp;
import javax.inject.Inject;

public class PresenceSchedulerReceiver
  extends BroadcastReceiver
  implements l
{
  private Context a;
  
  public PresenceSchedulerReceiver() {}
  
  @Inject
  public PresenceSchedulerReceiver(Context paramContext)
  {
    a = paramContext;
  }
  
  private Intent a(String paramString)
  {
    return new Intent(paramString).setClass(a, PresenceSchedulerReceiver.class);
  }
  
  public final void a()
  {
    AlarmManager localAlarmManager = (AlarmManager)a.getSystemService("alarm");
    PendingIntent localPendingIntent = PendingIntent.getBroadcast(a, 2131364157, a("com.truecaller.action.ACTION_SET_LAST_SEEN"), 0);
    localAlarmManager.set(2, SystemClock.elapsedRealtime() + 180000L, localPendingIntent);
  }
  
  public final void a(long paramLong)
  {
    AlarmManager localAlarmManager = (AlarmManager)a.getSystemService("alarm");
    PendingIntent localPendingIntent = PendingIntent.getBroadcast(a, 2131364154, a("com.truecaller.action.ACTION_UPDATE_PRESENCE_FOR_CURRENT_USER"), 0);
    localAlarmManager.set(2, SystemClock.elapsedRealtime() + paramLong, localPendingIntent);
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramIntent == null) {
      return;
    }
    paramContext = ((bk)paramContext.getApplicationContext()).a();
    paramIntent = paramIntent.getAction();
    int i = -1;
    int j = paramIntent.hashCode();
    if (j != -135745394)
    {
      if (j != 190343278)
      {
        if ((j == 798292259) && (paramIntent.equals("android.intent.action.BOOT_COMPLETED"))) {
          i = 0;
        }
      }
      else if (paramIntent.equals("com.truecaller.action.ACTION_SET_LAST_SEEN")) {
        i = 1;
      }
    }
    else if (paramIntent.equals("com.truecaller.action.ACTION_UPDATE_PRESENCE_FOR_CURRENT_USER")) {
      i = 2;
    }
    switch (i)
    {
    default: 
      return;
    case 2: 
      ((c)paramContext.ae().a()).a(AvailabilityTrigger.RECURRING_TASK, false);
      return;
    case 1: 
      ((c)paramContext.ae().a()).b();
      return;
    }
    ((c)paramContext.ae().a()).a(AvailabilityTrigger.RECURRING_TASK, true);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.PresenceSchedulerReceiver
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */