package com.truecaller.presence;

import android.support.annotation.Keep;

@Keep
public enum AvailabilityTrigger
{
  RECURRING_TASK,  USER_ACTION;
  
  private AvailabilityTrigger() {}
}

/* Location:
 * Qualified Name:     com.truecaller.presence.AvailabilityTrigger
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */