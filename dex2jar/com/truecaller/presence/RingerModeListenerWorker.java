package com.truecaller.presence;

import android.annotation.TargetApi;
import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import c.g.b.k;
import c.u;
import com.truecaller.bk;
import com.truecaller.bp;
import javax.inject.Inject;

@TargetApi(26)
public final class RingerModeListenerWorker
  extends Worker
{
  public static final RingerModeListenerWorker.a c = new RingerModeListenerWorker.a((byte)0);
  @Inject
  public c b;
  
  public RingerModeListenerWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
  }
  
  public static final void b() {}
  
  public final ListenableWorker.a a()
  {
    Object localObject = getApplicationContext();
    if (localObject != null)
    {
      ((bk)localObject).a().a(this);
      localObject = b;
      if (localObject == null) {
        k.a("presenceManager");
      }
      ((c)localObject).a(AvailabilityTrigger.USER_ACTION, true);
      localObject = ListenableWorker.a.a();
      k.a(localObject, "Result.success()");
      return (ListenableWorker.a)localObject;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.GraphHolder");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.RingerModeListenerWorker
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */