package com.truecaller.presence;

import com.truecaller.common.h.i;
import com.truecaller.featuretoggles.e;
import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d<i>
{
  private final g a;
  private final Provider<e> b;
  
  private h(g paramg, Provider<e> paramProvider)
  {
    a = paramg;
    b = paramProvider;
  }
  
  public static h a(g paramg, Provider<e> paramProvider)
  {
    return new h(paramg, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */