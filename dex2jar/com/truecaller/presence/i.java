package com.truecaller.presence;

import com.truecaller.androidactors.f;
import dagger.a.d;
import javax.inject.Provider;

public final class i
  implements d<f<c>>
{
  private final g a;
  private final Provider<com.truecaller.androidactors.i> b;
  private final Provider<c> c;
  
  private i(g paramg, Provider<com.truecaller.androidactors.i> paramProvider, Provider<c> paramProvider1)
  {
    a = paramg;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static i a(g paramg, Provider<com.truecaller.androidactors.i> paramProvider, Provider<c> paramProvider1)
  {
    return new i(paramg, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */