package com.truecaller.presence;

import com.truecaller.common.g.a;
import javax.inject.Inject;

public final class s
  implements r
{
  private final a a;
  
  @Inject
  public s(a parama)
  {
    a = parama;
  }
  
  public final long a()
  {
    return Math.max(a.a("presence_interval", t.b()), t.c());
  }
  
  public final long b()
  {
    return a.a("presence_initial_delay", t.a());
  }
  
  public final long c()
  {
    return a.a("presence_stop_time", t.d());
  }
  
  public final long d()
  {
    return a.a("presence_recheck_time", t.e());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.s
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */