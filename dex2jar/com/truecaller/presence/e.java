package com.truecaller.presence;

import android.annotation.SuppressLint;
import com.google.f.am;
import com.google.f.am.a;
import com.google.f.r.a;
import com.google.f.t;
import com.google.f.t.a;
import com.truecaller.androidactors.w;
import com.truecaller.api.services.presence.v1.h.a;
import com.truecaller.api.services.presence.v1.models.Availability;
import com.truecaller.api.services.presence.v1.models.Availability.Context;
import com.truecaller.api.services.presence.v1.models.Availability.Status;
import com.truecaller.api.services.presence.v1.models.Availability.a;
import com.truecaller.api.services.presence.v1.models.b.a;
import com.truecaller.api.services.presence.v1.models.d.a;
import com.truecaller.api.services.presence.v1.models.i.a;
import com.truecaller.common.h.u;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.transport.im.bp;
import com.truecaller.search.local.model.g;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.utils.PaymentPresence;
import com.truecaller.util.af;
import com.truecaller.util.al;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.inject.Inject;

final class e
  implements c
{
  private final com.truecaller.common.account.r a;
  private final n b;
  private final com.truecaller.common.g.a c;
  private final l d;
  private final al e;
  private final af f;
  private final com.truecaller.utils.i g;
  private final dagger.a<g> h;
  private final dagger.a<u> i;
  private final dagger.a<com.truecaller.common.h.r> j;
  private final r k;
  private final com.truecaller.androidactors.f<bp> l;
  private final bw m;
  private final com.truecaller.messaging.h n;
  private final com.truecaller.voip.d o;
  private final dagger.a<com.truecaller.featuretoggles.e> p;
  
  @Inject
  e(com.truecaller.common.account.r paramr, n paramn, com.truecaller.common.g.a parama, l paraml, al paramal, com.truecaller.utils.i parami, dagger.a<g> parama1, dagger.a<u> parama2, dagger.a<com.truecaller.common.h.r> parama3, af paramaf, r paramr1, com.truecaller.androidactors.f<bp> paramf, bw parambw, com.truecaller.messaging.h paramh, com.truecaller.voip.d paramd, dagger.a<com.truecaller.featuretoggles.e> parama4)
  {
    a = paramr;
    b = paramn;
    c = parama;
    d = paraml;
    e = paramal;
    g = parami;
    h = parama1;
    i = parama2;
    j = parama3;
    f = paramaf;
    k = paramr1;
    l = paramf;
    m = parambw;
    n = paramh;
    o = paramd;
    p = parama4;
  }
  
  private long a(String paramString)
  {
    long l1 = c.a(paramString, 0L);
    if (l1 > System.currentTimeMillis()) {
      return 0L;
    }
    return l1;
  }
  
  private com.truecaller.api.services.presence.v1.h a(AvailabilityTrigger paramAvailabilityTrigger, Availability paramAvailability, boolean paramBoolean)
  {
    return (com.truecaller.api.services.presence.v1.h)com.truecaller.api.services.presence.v1.h.a().a(paramAvailability).a(e()).a(f()).a(am.newBuilder().setValue(paramAvailabilityTrigger.name())).a(g()).a(h()).a(paramBoolean).build();
  }
  
  private void a(Availability paramAvailability)
  {
    c.a("last_availability_update_success", q.a(paramAvailability));
    c.b("last_successful_availability_update_time", System.currentTimeMillis());
  }
  
  private static void a(u paramu, Collection<String> paramCollection)
  {
    paramCollection = paramCollection.iterator();
    while (paramCollection.hasNext()) {
      if (paramu.d((String)paramCollection.next()) != 2) {
        paramCollection.remove();
      }
    }
  }
  
  /* Error */
  private void a(com.truecaller.common.network.e parame, Collection<String> paramCollection)
  {
    // Byte code:
    //   0: aload_2
    //   1: ifnonnull +4 -> 5
    //   4: return
    //   5: aload_0
    //   6: getfield 62	com/truecaller/presence/e:i	Ldagger/a;
    //   9: invokeinterface 209 1 0
    //   14: checkcast 194	com/truecaller/common/h/u
    //   17: aload_2
    //   18: invokestatic 211	com/truecaller/presence/e:a	(Lcom/truecaller/common/h/u;Ljava/util/Collection;)V
    //   21: aload_2
    //   22: invokeinterface 214 1 0
    //   27: ifeq +4 -> 31
    //   30: return
    //   31: aload_0
    //   32: aload_2
    //   33: invokespecial 217	com/truecaller/presence/e:c	(Ljava/util/Collection;)V
    //   36: aload_2
    //   37: invokeinterface 214 1 0
    //   42: ifeq +4 -> 46
    //   45: return
    //   46: aload_2
    //   47: invokeinterface 180 1 0
    //   52: astore 5
    //   54: new 219	java/util/ArrayList
    //   57: dup
    //   58: bipush 50
    //   60: invokespecial 222	java/util/ArrayList:<init>	(I)V
    //   63: astore_2
    //   64: aload 5
    //   66: invokeinterface 186 1 0
    //   71: ifeq +186 -> 257
    //   74: iconst_0
    //   75: istore_3
    //   76: iload_3
    //   77: bipush 50
    //   79: if_icmpge +34 -> 113
    //   82: aload 5
    //   84: invokeinterface 186 1 0
    //   89: ifeq +24 -> 113
    //   92: aload_2
    //   93: aload 5
    //   95: invokeinterface 190 1 0
    //   100: invokeinterface 228 2 0
    //   105: pop
    //   106: iload_3
    //   107: iconst_1
    //   108: iadd
    //   109: istore_3
    //   110: goto -34 -> 76
    //   113: aload_2
    //   114: invokeinterface 229 1 0
    //   119: ifne +138 -> 257
    //   122: invokestatic 234	com/truecaller/api/services/presence/v1/a:a	()Lcom/truecaller/api/services/presence/v1/a$a;
    //   125: aload_2
    //   126: invokevirtual 239	com/truecaller/api/services/presence/v1/a$a:a	(Ljava/lang/Iterable;)Lcom/truecaller/api/services/presence/v1/a$a;
    //   129: invokevirtual 240	com/truecaller/api/services/presence/v1/a$a:build	()Lcom/google/f/q;
    //   132: checkcast 231	com/truecaller/api/services/presence/v1/a
    //   135: astore 6
    //   137: aload_0
    //   138: getfield 50	com/truecaller/presence/e:b	Lcom/truecaller/presence/n;
    //   141: aload_1
    //   142: invokeinterface 245 2 0
    //   147: checkcast 247	com/truecaller/api/services/presence/v1/e$a
    //   150: astore 7
    //   152: aload 7
    //   154: ifnonnull +12 -> 166
    //   157: aload_2
    //   158: invokeinterface 250 1 0
    //   163: goto -99 -> 64
    //   166: aload 7
    //   168: aload 6
    //   170: invokevirtual 253	com/truecaller/api/services/presence/v1/e$a:a	(Lcom/truecaller/api/services/presence/v1/a;)Lcom/truecaller/api/services/presence/v1/c;
    //   173: astore 6
    //   175: aload 6
    //   177: ifnull +49 -> 226
    //   180: aload_1
    //   181: instanceof 255
    //   184: istore 4
    //   186: aload 6
    //   188: iload 4
    //   190: invokestatic 260	com/truecaller/presence/a:a	(Lcom/truecaller/api/services/presence/v1/c;Z)Ljava/util/Collection;
    //   193: astore 6
    //   195: iload 4
    //   197: ifeq +9 -> 206
    //   200: aload_0
    //   201: aload 6
    //   203: invokespecial 262	com/truecaller/presence/e:b	(Ljava/util/Collection;)V
    //   206: aload_0
    //   207: getfield 60	com/truecaller/presence/e:h	Ldagger/a;
    //   210: invokeinterface 209 1 0
    //   215: checkcast 264	com/truecaller/search/local/model/g
    //   218: aload 6
    //   220: invokevirtual 266	com/truecaller/search/local/model/g:a	(Ljava/util/Collection;)V
    //   223: goto +17 -> 240
    //   226: goto +14 -> 240
    //   229: astore_1
    //   230: goto +19 -> 249
    //   233: astore 6
    //   235: aload 6
    //   237: invokestatic 272	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   240: aload_2
    //   241: invokeinterface 250 1 0
    //   246: goto -182 -> 64
    //   249: aload_2
    //   250: invokeinterface 250 1 0
    //   255: aload_1
    //   256: athrow
    //   257: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	258	0	this	e
    //   0	258	1	parame	com.truecaller.common.network.e
    //   0	258	2	paramCollection	Collection<String>
    //   75	35	3	i1	int
    //   184	12	4	bool	boolean
    //   52	42	5	localIterator	Iterator
    //   135	84	6	localObject	Object
    //   233	3	6	localRuntimeException	RuntimeException
    //   150	17	7	locala	com.truecaller.api.services.presence.v1.e.a
    // Exception table:
    //   from	to	target	type
    //   137	152	229	finally
    //   166	175	229	finally
    //   180	195	229	finally
    //   200	206	229	finally
    //   206	223	229	finally
    //   235	240	229	finally
    //   137	152	233	java/lang/RuntimeException
    //   166	175	233	java/lang/RuntimeException
    //   180	195	233	java/lang/RuntimeException
    //   200	206	233	java/lang/RuntimeException
    //   206	223	233	java/lang/RuntimeException
  }
  
  private boolean a(a parama)
  {
    return g.b(k.d()).d(org.a.a.e.a());
  }
  
  private static int b(Availability paramAvailability)
  {
    switch (e.1.b[paramAvailability.a().ordinal()])
    {
    default: 
      break;
    case 2: 
      switch (e.1.a[paramAvailability.b().ordinal()])
      {
      default: 
        break;
      case 2: 
        return 7200000;
      case 1: 
        return 600000;
      }
      break;
    case 1: 
      return 432000000;
    }
    return Integer.MIN_VALUE;
  }
  
  private void b(Collection<a> paramCollection)
  {
    if (!m.a()) {
      return;
    }
    ArrayList localArrayList = new ArrayList();
    paramCollection = paramCollection.iterator();
    while (paramCollection.hasNext())
    {
      a locala = (a)paramCollection.next();
      com.truecaller.api.services.presence.v1.models.d locald = e;
      if ((locald != null) && (!a)) {
        localArrayList.add(a);
      }
    }
    if (!localArrayList.isEmpty()) {
      ((bp)l.a()).a(localArrayList, false).c();
    }
  }
  
  private void c(Collection<String> paramCollection)
  {
    paramCollection = paramCollection.iterator();
    while (paramCollection.hasNext())
    {
      Object localObject = (String)paramCollection.next();
      localObject = ((g)h.get()).b((String)localObject);
      if ((localObject != null) && (!a((a)localObject))) {
        paramCollection.remove();
      }
    }
  }
  
  private boolean d()
  {
    Object localObject = f.a();
    try
    {
      com.truecaller.api.services.presence.v1.e.a locala = (com.truecaller.api.services.presence.v1.e.a)b.b(com.truecaller.common.network.e.a.a);
      if (locala != null)
      {
        localObject = (com.truecaller.api.services.presence.v1.f)com.truecaller.api.services.presence.v1.f.a().a((String)localObject).build();
        io.grpc.c.d.a(locala.getChannel(), com.truecaller.api.services.presence.v1.e.a(), locala.getCallOptions(), localObject);
        return true;
      }
    }
    catch (RuntimeException localRuntimeException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localRuntimeException);
    }
    return false;
  }
  
  private com.truecaller.api.services.presence.v1.models.b e()
  {
    return (com.truecaller.api.services.presence.v1.models.b)com.truecaller.api.services.presence.v1.models.b.c().a(i()).a(com.google.f.r.newBuilder().setValue(6)).build();
  }
  
  private com.truecaller.api.services.presence.v1.models.i f()
  {
    boolean bool = o.a();
    i.a locala = com.truecaller.api.services.presence.v1.models.i.c().a(bool ^ true);
    if (bool) {
      locala.a();
    }
    return (com.truecaller.api.services.presence.v1.models.i)locala.build();
  }
  
  private com.truecaller.api.services.presence.v1.models.d g()
  {
    int i1;
    if ((m.a()) && (n.G() != null)) {
      i1 = 1;
    } else {
      i1 = 0;
    }
    return (com.truecaller.api.services.presence.v1.models.d)com.truecaller.api.services.presence.v1.models.d.a().a(i1 ^ 0x1).build();
  }
  
  private com.truecaller.api.services.presence.v1.models.f h()
  {
    if (!((com.truecaller.featuretoggles.e)p.get()).n().a()) {
      return (com.truecaller.api.services.presence.v1.models.f)com.truecaller.api.services.presence.v1.models.f.d().a(false).build();
    }
    PaymentPresence localPaymentPresence = Truepay.getInstance().getPresenceInfo();
    return (com.truecaller.api.services.presence.v1.models.f)com.truecaller.api.services.presence.v1.models.f.d().a(localPaymentPresence.isEnabled()).b(localPaymentPresence.getLastTxnTs()).a(localPaymentPresence.getVersion()).build();
  }
  
  private boolean i()
  {
    boolean bool1 = c.b("flash_enabled");
    boolean bool2 = c.b("featureFlash");
    return (a.c()) && (bool1) && (bool2);
  }
  
  private boolean j()
  {
    boolean bool1 = c.b("availability_enabled");
    boolean bool2 = c.b("featureAvailability");
    return (a.c()) && (bool1) && (bool2);
  }
  
  private Availability k()
  {
    if (!j()) {
      return (Availability)Availability.c().a(Availability.Status.UNKNOWN).build();
    }
    boolean bool1 = e.h();
    int i1 = e.g();
    int i3 = 1;
    if (i1 == 0) {
      i1 = 1;
    } else {
      i1 = 0;
    }
    boolean bool2 = e.i();
    int i2 = i3;
    if (i1 == 0) {
      if (bool2) {
        i2 = i3;
      } else {
        i2 = 0;
      }
    }
    Availability.a locala = Availability.c();
    if ((!bool1) && (i2 == 0))
    {
      locala.a(Availability.Status.AVAILABLE);
    }
    else
    {
      locala.a(Availability.Status.BUSY);
      if (bool1) {
        localObject = Availability.Context.CALL;
      } else {
        localObject = Availability.Context.SLEEP;
      }
      locala.a((Availability.Context)localObject);
      Object localObject = t.newBuilder();
      long l1 = System.currentTimeMillis();
      if (bool1) {
        i1 = 600000;
      } else {
        i1 = 7200000;
      }
      locala.a(((t.a)localObject).setValue(l1 + i1));
    }
    return (Availability)locala.build();
  }
  
  public final w<Boolean> a()
  {
    if (!g.a()) {
      return w.b(Boolean.FALSE);
    }
    Availability localAvailability = k();
    com.truecaller.api.services.presence.v1.h localh = a(AvailabilityTrigger.USER_ACTION, localAvailability, true);
    try
    {
      com.truecaller.api.services.presence.v1.e.a locala = (com.truecaller.api.services.presence.v1.e.a)b.b(com.truecaller.common.network.e.a.a);
      if (locala == null) {
        return w.b(Boolean.FALSE);
      }
      locala.a(localh);
      a(localAvailability);
      return w.b(Boolean.TRUE);
    }
    catch (RuntimeException localRuntimeException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localRuntimeException);
    }
    return w.b(Boolean.FALSE);
  }
  
  public final void a(AvailabilityTrigger paramAvailabilityTrigger, boolean paramBoolean)
  {
    if (!j()) {
      return;
    }
    Availability localAvailability1 = k();
    Object localObject = q.a(localAvailability1);
    String str = c.a("last_availability_update_success");
    Availability localAvailability2 = q.a(str);
    int i3 = b(localAvailability1);
    if (localAvailability2 != null)
    {
      long l1 = a("last_successful_availability_update_time");
      long l2 = System.currentTimeMillis();
      int i2 = b(localAvailability2);
      switch (e.1.b[localAvailability2.a().ordinal()])
      {
      default: 
        break;
      case 2: 
        switch (e.1.a[localAvailability2.b().ordinal()])
        {
        default: 
          break;
        case 2: 
          i1 = 1200000;
          break;
        case 1: 
          i1 = 60000;
        }
        break;
      case 1: 
        i1 = 10800000;
        break;
      }
      int i1 = 0;
      if (l2 > l1 + i2 - i1) {
        i1 = 1;
      } else {
        i1 = 0;
      }
      if ((Availability.Status.AVAILABLE.equals(localAvailability1.a())) && (!Availability.Status.AVAILABLE.equals(localAvailability2.a()))) {
        i2 = 1;
      } else {
        i2 = 0;
      }
      if ((i2 != 0) && (i1 != 0))
      {
        paramAvailabilityTrigger = new StringBuilder("State (");
        paramAvailabilityTrigger.append((String)localObject);
        paramAvailabilityTrigger.append(") will expire on server soon (meaning it will go back to AVAILABLE), no need for a request. Scheduling for Available though, in ");
        paramAvailabilityTrigger.append(i3);
        paramAvailabilityTrigger.append("ms");
        paramAvailabilityTrigger.toString();
        d.a(i3);
        a(localAvailability1);
        return;
      }
      if ((((String)localObject).equals(str)) && (i1 == 0))
      {
        paramAvailabilityTrigger = new StringBuilder("State hasn't changed (");
        paramAvailabilityTrigger.append(str);
        paramAvailabilityTrigger.append("), and is not about to expire, no need for a request. Still rescheduling, to be sure that if for some reason the alarm was triggered too early, we still have a new one scheduled.");
        paramAvailabilityTrigger.toString();
        l1 = System.currentTimeMillis();
        l2 = a("last_successful_availability_update_time");
        long l3 = i3;
        d.a(l1 - l2 + l3);
        return;
      }
    }
    if (a("key_last_set_status_time") + 15000L > System.currentTimeMillis())
    {
      d.a(15000L);
      return;
    }
    c.b("key_last_set_status_time", System.currentTimeMillis());
    localObject = new StringBuilder("Scheduling next reportPresence in ");
    ((StringBuilder)localObject).append(i3);
    ((StringBuilder)localObject).append("ms");
    ((StringBuilder)localObject).toString();
    d.a(i3);
    if (!g.a()) {
      return;
    }
    paramAvailabilityTrigger = a(paramAvailabilityTrigger, localAvailability1, paramBoolean);
    try
    {
      localObject = (com.truecaller.api.services.presence.v1.e.a)b.b(com.truecaller.common.network.e.a.a);
      if (localObject != null)
      {
        ((com.truecaller.api.services.presence.v1.e.a)localObject).a(paramAvailabilityTrigger);
        a(localAvailability1);
      }
      return;
    }
    catch (RuntimeException paramAvailabilityTrigger)
    {
      AssertionUtil.reportThrowableButNeverCrash(paramAvailabilityTrigger);
    }
  }
  
  public final void a(Collection<String> paramCollection)
  {
    if ((!j()) && (!i()) && (!m.a()) && (!o.a())) {
      return;
    }
    if (paramCollection.isEmpty()) {
      return;
    }
    if (!g.a()) {
      return;
    }
    Object localObject = (u)i.get();
    paramCollection = ((com.truecaller.common.h.r)j.get()).a(((u)localObject).a(paramCollection));
    if (((com.truecaller.featuretoggles.e)p.get()).f().a())
    {
      paramCollection = paramCollection.entrySet().iterator();
      while (paramCollection.hasNext())
      {
        localObject = (Map.Entry)paramCollection.next();
        a((com.truecaller.common.network.e)((Map.Entry)localObject).getKey(), (Collection)((Map.Entry)localObject).getValue());
      }
      return;
    }
    a(com.truecaller.common.network.e.a.a, (Collection)paramCollection.get(com.truecaller.common.network.e.a.a));
  }
  
  public final void b()
  {
    if (!j()) {
      return;
    }
    if (!g.a()) {
      return;
    }
    if (a("key_last_set_last_seen_time") + 180000L > System.currentTimeMillis())
    {
      d.a();
      return;
    }
    c.b("key_last_set_last_seen_time", System.currentTimeMillis());
    d();
  }
  
  @SuppressLint({"CheckResult"})
  public final w<Boolean> c()
  {
    if (!g.a()) {
      return w.b(Boolean.FALSE);
    }
    try
    {
      com.truecaller.api.services.presence.v1.e.a locala = (com.truecaller.api.services.presence.v1.e.a)b.b(com.truecaller.common.network.e.a.a);
      if (locala == null) {
        return w.b(Boolean.FALSE);
      }
      locala.a((com.truecaller.api.services.presence.v1.h)com.truecaller.api.services.presence.v1.h.a().a((com.truecaller.api.services.presence.v1.models.i)com.truecaller.api.services.presence.v1.models.i.c().a(true).build()).build());
      return w.b(Boolean.TRUE);
    }
    catch (RuntimeException localRuntimeException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localRuntimeException);
    }
    return w.b(Boolean.FALSE);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */