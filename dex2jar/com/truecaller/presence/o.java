package com.truecaller.presence;

import c.a.y;
import com.truecaller.api.services.presence.v1.e.a;
import com.truecaller.api.services.presence.v1.e.b;
import com.truecaller.common.account.r;
import com.truecaller.common.edge.a;
import com.truecaller.common.network.c;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.network.d.e;
import com.truecaller.utils.d;
import java.util.Collection;
import javax.inject.Inject;
import javax.inject.Named;

public final class o
  extends com.truecaller.network.d.k<e.b, e.a>
  implements n
{
  @Inject
  public o(r paramr, com.truecaller.common.account.k paramk, d paramd, a parama, c paramc, e parame, @Named("ssl_patches_applied") boolean paramBoolean, @Named("grpc_user_agent") String paramString, com.truecaller.network.d.b paramb, com.truecaller.d.b paramb1, @Named("presence_cross_domain_support") com.truecaller.common.h.i parami)
  {
    super(KnownEndpoints.PRESENCE_GRPC, paramr, paramk, paramd, Integer.valueOf(10), paramb, parama, paramc, paramBoolean, parame, paramString, paramb1, parami);
  }
  
  public final Collection<io.grpc.i> a()
  {
    return (Collection)y.a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */