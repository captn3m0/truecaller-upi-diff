package com.truecaller.presence;

import com.truecaller.common.account.k;
import com.truecaller.common.account.r;
import com.truecaller.common.edge.a;
import com.truecaller.common.h.i;
import com.truecaller.common.network.c;
import com.truecaller.network.d.e;
import javax.inject.Provider;

public final class p
  implements dagger.a.d<o>
{
  private final Provider<r> a;
  private final Provider<k> b;
  private final Provider<com.truecaller.utils.d> c;
  private final Provider<a> d;
  private final Provider<c> e;
  private final Provider<e> f;
  private final Provider<Boolean> g;
  private final Provider<String> h;
  private final Provider<com.truecaller.network.d.b> i;
  private final Provider<com.truecaller.d.b> j;
  private final Provider<i> k;
  
  private p(Provider<r> paramProvider, Provider<k> paramProvider1, Provider<com.truecaller.utils.d> paramProvider2, Provider<a> paramProvider3, Provider<c> paramProvider4, Provider<e> paramProvider5, Provider<Boolean> paramProvider6, Provider<String> paramProvider7, Provider<com.truecaller.network.d.b> paramProvider8, Provider<com.truecaller.d.b> paramProvider9, Provider<i> paramProvider10)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
    k = paramProvider10;
  }
  
  public static p a(Provider<r> paramProvider, Provider<k> paramProvider1, Provider<com.truecaller.utils.d> paramProvider2, Provider<a> paramProvider3, Provider<c> paramProvider4, Provider<e> paramProvider5, Provider<Boolean> paramProvider6, Provider<String> paramProvider7, Provider<com.truecaller.network.d.b> paramProvider8, Provider<com.truecaller.d.b> paramProvider9, Provider<i> paramProvider10)
  {
    return new p(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */