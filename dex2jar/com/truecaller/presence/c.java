package com.truecaller.presence;

import com.truecaller.androidactors.w;
import java.util.Collection;

public abstract interface c
{
  public abstract w<Boolean> a();
  
  public abstract void a(AvailabilityTrigger paramAvailabilityTrigger, boolean paramBoolean);
  
  public abstract void a(Collection<String> paramCollection);
  
  public abstract void b();
  
  public abstract w<Boolean> c();
}

/* Location:
 * Qualified Name:     com.truecaller.presence.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */