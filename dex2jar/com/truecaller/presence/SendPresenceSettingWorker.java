package com.truecaller.presence;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.bp;
import com.truecaller.common.account.r;
import com.truecaller.common.background.TrackedWorker;
import com.truecaller.log.AssertionUtil;
import javax.inject.Inject;

public final class SendPresenceSettingWorker
  extends TrackedWorker
{
  public static final SendPresenceSettingWorker.a e = new SendPresenceSettingWorker.a((byte)0);
  @Inject
  public r b;
  @Inject
  public f<c> c;
  @Inject
  public b d;
  
  public SendPresenceSettingWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public final b b()
  {
    b localb = d;
    if (localb == null) {
      k.a("analytics");
    }
    return localb;
  }
  
  public final boolean c()
  {
    r localr = b;
    if (localr == null) {
      k.a("accountManager");
    }
    return localr.c();
  }
  
  public final ListenableWorker.a d()
  {
    try
    {
      Object localObject = c;
      if (localObject == null) {
        k.a("presenceManager");
      }
      if (k.a((Boolean)((c)((f)localObject).a()).a().d(), Boolean.TRUE))
      {
        localObject = ListenableWorker.a.a();
        k.a(localObject, "Result.success()");
        return (ListenableWorker.a)localObject;
      }
    }
    catch (InterruptedException localInterruptedException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localInterruptedException);
      ListenableWorker.a locala = ListenableWorker.a.b();
      k.a(locala, "Result.retry()");
      return locala;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.SendPresenceSettingWorker
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */