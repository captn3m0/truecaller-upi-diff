package com.truecaller.presence;

import android.content.Context;
import com.truecaller.androidactors.i;
import dagger.a.d;
import javax.inject.Provider;

public final class k
  implements d<i>
{
  private final g a;
  private final Provider<Context> b;
  private final Provider<com.truecaller.androidactors.k> c;
  
  private k(g paramg, Provider<Context> paramProvider, Provider<com.truecaller.androidactors.k> paramProvider1)
  {
    a = paramg;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static k a(g paramg, Provider<Context> paramProvider, Provider<com.truecaller.androidactors.k> paramProvider1)
  {
    return new k(paramg, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */