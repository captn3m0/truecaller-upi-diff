package com.truecaller.presence;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class m
  implements d<PresenceSchedulerReceiver>
{
  private final Provider<Context> a;
  
  private m(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static m a(Provider<Context> paramProvider)
  {
    return new m(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */