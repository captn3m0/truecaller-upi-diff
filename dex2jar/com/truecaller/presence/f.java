package com.truecaller.presence;

import com.truecaller.common.g.a;
import com.truecaller.common.h.u;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.h;
import com.truecaller.messaging.transport.im.bp;
import com.truecaller.search.local.model.g;
import com.truecaller.util.af;
import com.truecaller.util.al;
import com.truecaller.utils.i;
import javax.inject.Provider;

public final class f
  implements dagger.a.d<e>
{
  private final Provider<com.truecaller.common.account.r> a;
  private final Provider<n> b;
  private final Provider<a> c;
  private final Provider<l> d;
  private final Provider<al> e;
  private final Provider<i> f;
  private final Provider<g> g;
  private final Provider<u> h;
  private final Provider<com.truecaller.common.h.r> i;
  private final Provider<af> j;
  private final Provider<r> k;
  private final Provider<com.truecaller.androidactors.f<bp>> l;
  private final Provider<bw> m;
  private final Provider<h> n;
  private final Provider<com.truecaller.voip.d> o;
  private final Provider<com.truecaller.featuretoggles.e> p;
  
  private f(Provider<com.truecaller.common.account.r> paramProvider, Provider<n> paramProvider1, Provider<a> paramProvider2, Provider<l> paramProvider3, Provider<al> paramProvider4, Provider<i> paramProvider5, Provider<g> paramProvider6, Provider<u> paramProvider7, Provider<com.truecaller.common.h.r> paramProvider8, Provider<af> paramProvider9, Provider<r> paramProvider10, Provider<com.truecaller.androidactors.f<bp>> paramProvider11, Provider<bw> paramProvider12, Provider<h> paramProvider13, Provider<com.truecaller.voip.d> paramProvider14, Provider<com.truecaller.featuretoggles.e> paramProvider15)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
    k = paramProvider10;
    l = paramProvider11;
    m = paramProvider12;
    n = paramProvider13;
    o = paramProvider14;
    p = paramProvider15;
  }
  
  public static f a(Provider<com.truecaller.common.account.r> paramProvider, Provider<n> paramProvider1, Provider<a> paramProvider2, Provider<l> paramProvider3, Provider<al> paramProvider4, Provider<i> paramProvider5, Provider<g> paramProvider6, Provider<u> paramProvider7, Provider<com.truecaller.common.h.r> paramProvider8, Provider<af> paramProvider9, Provider<r> paramProvider10, Provider<com.truecaller.androidactors.f<bp>> paramProvider11, Provider<bw> paramProvider12, Provider<h> paramProvider13, Provider<com.truecaller.voip.d> paramProvider14, Provider<com.truecaller.featuretoggles.e> paramProvider15)
  {
    return new f(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10, paramProvider11, paramProvider12, paramProvider13, paramProvider14, paramProvider15);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */