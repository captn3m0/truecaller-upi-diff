package com.truecaller.presence;

import com.truecaller.androidactors.h;
import java.util.concurrent.TimeUnit;

public class PresenceService
  extends h
{
  public PresenceService()
  {
    super("presence", TimeUnit.SECONDS.toMillis(30L), true);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.PresenceService
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */