package com.truecaller.callerid;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import dagger.a.d;
import javax.inject.Provider;

public final class y
  implements d<f<ad>>
{
  private final q a;
  private final Provider<i> b;
  private final Provider<ad> c;
  
  private y(q paramq, Provider<i> paramProvider, Provider<ad> paramProvider1)
  {
    a = paramq;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static y a(q paramq, Provider<i> paramProvider, Provider<ad> paramProvider1)
  {
    return new y(paramq, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.y
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */