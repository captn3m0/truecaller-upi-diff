package com.truecaller.callerid;

import com.truecaller.androidactors.u;
import com.truecaller.androidactors.v;

public final class f
  implements e
{
  private final v a;
  
  public f(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return e.class.equals(paramClass);
  }
  
  public final void a()
  {
    a.a(new f(new com.truecaller.androidactors.e(), (byte)0));
  }
  
  public final void a(i parami)
  {
    a.a(new b(new com.truecaller.androidactors.e(), parami, (byte)0));
  }
  
  public final void b()
  {
    a.a(new e(new com.truecaller.androidactors.e(), (byte)0));
  }
  
  public final void c()
  {
    a.a(new d(new com.truecaller.androidactors.e(), (byte)0));
  }
  
  public final void d()
  {
    a.a(new a(new com.truecaller.androidactors.e(), (byte)0));
  }
  
  public final void e()
  {
    a.a(new c(new com.truecaller.androidactors.e(), (byte)0));
  }
  
  static final class a
    extends u<e, Void>
  {
    private a(com.truecaller.androidactors.e parame)
    {
      super();
    }
    
    public final String toString()
    {
      return ".dismissOnGoingCallRecordingNotification()";
    }
  }
  
  static final class b
    extends u<e, Void>
  {
    private final i b;
    
    private b(com.truecaller.androidactors.e parame, i parami)
    {
      super();
      b = parami;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".showBlockedCallNotification(");
      localStringBuilder.append(a(b, 1));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class c
    extends u<e, Void>
  {
    private c(com.truecaller.androidactors.e parame)
    {
      super();
    }
    
    public final String toString()
    {
      return ".showMissedCallsNotification()";
    }
  }
  
  static final class d
    extends u<e, Void>
  {
    private d(com.truecaller.androidactors.e parame)
    {
      super();
    }
    
    public final String toString()
    {
      return ".showOnGoingCallRecordingNotification()";
    }
  }
  
  static final class e
    extends u<e, Void>
  {
    private e(com.truecaller.androidactors.e parame)
    {
      super();
    }
    
    public final String toString()
    {
      return ".showRequestAllowDrawOverOtherAppsNotification()";
    }
  }
  
  static final class f
    extends u<e, Void>
  {
    private f(com.truecaller.androidactors.e parame)
    {
      super();
    }
    
    public final String toString()
    {
      return ".showUnableToRejectCallNotification()";
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */