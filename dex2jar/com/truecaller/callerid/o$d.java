package com.truecaller.callerid;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class o$d
  extends u<n, Void>
{
  private final int b;
  private final String c;
  private final int d;
  private final int e;
  
  private o$d(e parame, int paramInt1, String paramString, int paramInt2, int paramInt3)
  {
    super(parame);
    b = paramInt1;
    c = paramString;
    d = paramInt2;
    e = paramInt3;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".onStateChanged(");
    localStringBuilder.append(a(Integer.valueOf(b), 2));
    localStringBuilder.append(",");
    localStringBuilder.append(a(c, 1));
    localStringBuilder.append(",");
    localStringBuilder.append(a(Integer.valueOf(d), 2));
    localStringBuilder.append(",");
    localStringBuilder.append(a(Integer.valueOf(e), 2));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.o.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */