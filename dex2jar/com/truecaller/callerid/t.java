package com.truecaller.callerid;

import com.truecaller.analytics.ae;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.k;
import com.truecaller.calling.dialer.v;
import com.truecaller.calling.recorder.CallRecordingManager;
import com.truecaller.common.account.r;
import com.truecaller.data.entity.g;
import com.truecaller.filters.FilterManager;
import com.truecaller.util.aa;
import com.truecaller.util.u;
import javax.inject.Provider;

public final class t
  implements dagger.a.d<n>
{
  private final Provider<al> A;
  private final Provider<com.truecaller.ads.provider.f> B;
  private final Provider<com.truecaller.androidactors.f<com.truecaller.ads.provider.a.b>> C;
  private final Provider<k> D;
  private final Provider<CallRecordingManager> E;
  private final Provider<com.truecaller.androidactors.f<com.truecaller.callhistory.a>> F;
  private final Provider<com.truecaller.tcpermissions.l> G;
  private final Provider<com.truecaller.utils.l> H;
  private final Provider<com.truecaller.featuretoggles.e> I;
  private final Provider<com.truecaller.b.c> J;
  private final Provider<com.truecaller.utils.a> K;
  private final Provider<com.truecaller.search.b> L;
  private final Provider<v> M;
  private final Provider<com.truecaller.androidactors.f<com.truecaller.calling.recorder.floatingbutton.e>> N;
  private final q a;
  private final Provider<i> b;
  private final Provider<com.truecaller.utils.d> c;
  private final Provider<com.truecaller.util.al> d;
  private final Provider<a> e;
  private final Provider<c> f;
  private final Provider<com.truecaller.network.search.l> g;
  private final Provider<com.truecaller.common.h.aj> h;
  private final Provider<com.truecaller.androidactors.f<com.truecaller.h.c>> i;
  private final Provider<u> j;
  private final Provider<com.truecaller.data.access.c> k;
  private final Provider<com.truecaller.androidactors.f<aa>> l;
  private final Provider<com.truecaller.androidactors.f<e>> m;
  private final Provider<com.truecaller.androidactors.f<com.truecaller.network.util.c>> n;
  private final Provider<com.truecaller.androidactors.f<ad>> o;
  private final Provider<com.truecaller.androidactors.f<ae>> p;
  private final Provider<aj> q;
  private final Provider<FilterManager> r;
  private final Provider<com.truecaller.analytics.b> s;
  private final Provider<com.truecaller.common.g.a> t;
  private final Provider<com.truecaller.i.c> u;
  private final Provider<com.truecaller.aftercall.a> v;
  private final Provider<ag> w;
  private final Provider<r> x;
  private final Provider<com.truecaller.service.e> y;
  private final Provider<g> z;
  
  private t(q paramq, Provider<i> paramProvider, Provider<com.truecaller.utils.d> paramProvider1, Provider<com.truecaller.util.al> paramProvider2, Provider<a> paramProvider3, Provider<c> paramProvider4, Provider<com.truecaller.network.search.l> paramProvider5, Provider<com.truecaller.common.h.aj> paramProvider6, Provider<com.truecaller.androidactors.f<com.truecaller.h.c>> paramProvider7, Provider<u> paramProvider8, Provider<com.truecaller.data.access.c> paramProvider9, Provider<com.truecaller.androidactors.f<aa>> paramProvider10, Provider<com.truecaller.androidactors.f<e>> paramProvider11, Provider<com.truecaller.androidactors.f<com.truecaller.network.util.c>> paramProvider12, Provider<com.truecaller.androidactors.f<ad>> paramProvider13, Provider<com.truecaller.androidactors.f<ae>> paramProvider14, Provider<aj> paramProvider15, Provider<FilterManager> paramProvider16, Provider<com.truecaller.analytics.b> paramProvider17, Provider<com.truecaller.common.g.a> paramProvider18, Provider<com.truecaller.i.c> paramProvider19, Provider<com.truecaller.aftercall.a> paramProvider20, Provider<ag> paramProvider21, Provider<r> paramProvider22, Provider<com.truecaller.service.e> paramProvider23, Provider<g> paramProvider24, Provider<al> paramProvider25, Provider<com.truecaller.ads.provider.f> paramProvider26, Provider<com.truecaller.androidactors.f<com.truecaller.ads.provider.a.b>> paramProvider27, Provider<k> paramProvider28, Provider<CallRecordingManager> paramProvider29, Provider<com.truecaller.androidactors.f<com.truecaller.callhistory.a>> paramProvider30, Provider<com.truecaller.tcpermissions.l> paramProvider31, Provider<com.truecaller.utils.l> paramProvider32, Provider<com.truecaller.featuretoggles.e> paramProvider33, Provider<com.truecaller.b.c> paramProvider34, Provider<com.truecaller.utils.a> paramProvider35, Provider<com.truecaller.search.b> paramProvider36, Provider<v> paramProvider37, Provider<com.truecaller.androidactors.f<com.truecaller.calling.recorder.floatingbutton.e>> paramProvider38)
  {
    a = paramq;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
    f = paramProvider4;
    g = paramProvider5;
    h = paramProvider6;
    i = paramProvider7;
    j = paramProvider8;
    k = paramProvider9;
    l = paramProvider10;
    m = paramProvider11;
    n = paramProvider12;
    o = paramProvider13;
    p = paramProvider14;
    q = paramProvider15;
    r = paramProvider16;
    s = paramProvider17;
    t = paramProvider18;
    u = paramProvider19;
    v = paramProvider20;
    w = paramProvider21;
    x = paramProvider22;
    y = paramProvider23;
    z = paramProvider24;
    A = paramProvider25;
    B = paramProvider26;
    C = paramProvider27;
    D = paramProvider28;
    E = paramProvider29;
    F = paramProvider30;
    G = paramProvider31;
    H = paramProvider32;
    I = paramProvider33;
    J = paramProvider34;
    K = paramProvider35;
    L = paramProvider36;
    M = paramProvider37;
    N = paramProvider38;
  }
  
  public static t a(q paramq, Provider<i> paramProvider, Provider<com.truecaller.utils.d> paramProvider1, Provider<com.truecaller.util.al> paramProvider2, Provider<a> paramProvider3, Provider<c> paramProvider4, Provider<com.truecaller.network.search.l> paramProvider5, Provider<com.truecaller.common.h.aj> paramProvider6, Provider<com.truecaller.androidactors.f<com.truecaller.h.c>> paramProvider7, Provider<u> paramProvider8, Provider<com.truecaller.data.access.c> paramProvider9, Provider<com.truecaller.androidactors.f<aa>> paramProvider10, Provider<com.truecaller.androidactors.f<e>> paramProvider11, Provider<com.truecaller.androidactors.f<com.truecaller.network.util.c>> paramProvider12, Provider<com.truecaller.androidactors.f<ad>> paramProvider13, Provider<com.truecaller.androidactors.f<ae>> paramProvider14, Provider<aj> paramProvider15, Provider<FilterManager> paramProvider16, Provider<com.truecaller.analytics.b> paramProvider17, Provider<com.truecaller.common.g.a> paramProvider18, Provider<com.truecaller.i.c> paramProvider19, Provider<com.truecaller.aftercall.a> paramProvider20, Provider<ag> paramProvider21, Provider<r> paramProvider22, Provider<com.truecaller.service.e> paramProvider23, Provider<g> paramProvider24, Provider<al> paramProvider25, Provider<com.truecaller.ads.provider.f> paramProvider26, Provider<com.truecaller.androidactors.f<com.truecaller.ads.provider.a.b>> paramProvider27, Provider<k> paramProvider28, Provider<CallRecordingManager> paramProvider29, Provider<com.truecaller.androidactors.f<com.truecaller.callhistory.a>> paramProvider30, Provider<com.truecaller.tcpermissions.l> paramProvider31, Provider<com.truecaller.utils.l> paramProvider32, Provider<com.truecaller.featuretoggles.e> paramProvider33, Provider<com.truecaller.b.c> paramProvider34, Provider<com.truecaller.utils.a> paramProvider35, Provider<com.truecaller.search.b> paramProvider36, Provider<v> paramProvider37, Provider<com.truecaller.androidactors.f<com.truecaller.calling.recorder.floatingbutton.e>> paramProvider38)
  {
    return new t(paramq, paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10, paramProvider11, paramProvider12, paramProvider13, paramProvider14, paramProvider15, paramProvider16, paramProvider17, paramProvider18, paramProvider19, paramProvider20, paramProvider21, paramProvider22, paramProvider23, paramProvider24, paramProvider25, paramProvider26, paramProvider27, paramProvider28, paramProvider29, paramProvider30, paramProvider31, paramProvider32, paramProvider33, paramProvider34, paramProvider35, paramProvider36, paramProvider37, paramProvider38);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.t
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */