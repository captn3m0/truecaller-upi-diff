package com.truecaller.callerid;

import c.g.b.k;
import com.truecaller.androidactors.f;
import com.truecaller.utils.d;
import javax.inject.Inject;

public final class b
  implements a
{
  private final f<e> a;
  private final d b;
  
  @Inject
  public b(f<e> paramf, d paramd)
  {
    a = paramf;
    b = paramd;
  }
  
  public final void a(i parami)
  {
    k.b(parami, "callState");
    int i = h;
    int k = 0;
    if (i == 12785645) {
      i = 1;
    } else {
      i = 0;
    }
    int j;
    if (h == 3) {
      j = 1;
    } else {
      j = 0;
    }
    if (h == 1) {
      k = 1;
    }
    if ((i != 0) && (b.h() >= 24) && (!b.c())) {
      ((e)a.a()).a();
    }
    if ((j != 0) || (k != 0)) {
      ((e)a.a()).a(parami);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */