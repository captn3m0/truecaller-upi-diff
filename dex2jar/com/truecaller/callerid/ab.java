package com.truecaller.callerid;

import com.truecaller.androidactors.i;
import com.truecaller.androidactors.k;
import dagger.a.d;
import javax.inject.Provider;

public final class ab
  implements d<i>
{
  private final q a;
  private final Provider<k> b;
  
  private ab(q paramq, Provider<k> paramProvider)
  {
    a = paramq;
    b = paramProvider;
  }
  
  public static ab a(q paramq, Provider<k> paramProvider)
  {
    return new ab(paramq, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.ab
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */