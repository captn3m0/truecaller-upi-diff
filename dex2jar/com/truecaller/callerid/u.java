package com.truecaller.callerid;

import android.content.ContentResolver;
import com.truecaller.calling.dialer.v;
import dagger.a.d;
import javax.inject.Provider;

public final class u
  implements d<v>
{
  private final q a;
  private final Provider<ContentResolver> b;
  
  private u(q paramq, Provider<ContentResolver> paramProvider)
  {
    a = paramq;
    b = paramProvider;
  }
  
  public static u a(q paramq, Provider<ContentResolver> paramProvider)
  {
    return new u(paramq, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.u
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */