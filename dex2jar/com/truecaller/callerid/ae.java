package com.truecaller.callerid;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.network.search.j;

public final class ae
  implements ad
{
  private final v a;
  
  public ae(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return ad.class.equals(paramClass);
  }
  
  public final w<Contact> a(Number paramNumber, boolean paramBoolean, int paramInt, j paramj)
  {
    return w.a(a, new a(new e(), paramNumber, paramBoolean, paramInt, paramj, (byte)0));
  }
  
  static final class a
    extends u<ad, Contact>
  {
    private final Number b;
    private final boolean c;
    private final int d;
    private final j e;
    
    private a(e parame, Number paramNumber, boolean paramBoolean, int paramInt, j paramj)
    {
      super();
      b = paramNumber;
      c = paramBoolean;
      d = paramInt;
      e = paramj;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".performSearch(");
      localStringBuilder.append(a(b, 1));
      localStringBuilder.append(",");
      localStringBuilder.append(a(Boolean.valueOf(c), 2));
      localStringBuilder.append(",");
      localStringBuilder.append(a(Integer.valueOf(d), 2));
      localStringBuilder.append(",");
      localStringBuilder.append(a(e, 1));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.ae
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */