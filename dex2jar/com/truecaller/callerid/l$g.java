package com.truecaller.callerid;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class l$g
  extends u<k, Void>
{
  private final i b;
  private final boolean c;
  
  private l$g(e parame, i parami, boolean paramBoolean)
  {
    super(parame);
    b = parami;
    c = paramBoolean;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".updateCallerId(");
    localStringBuilder.append(a(b, 1));
    localStringBuilder.append(",");
    localStringBuilder.append(a(Boolean.valueOf(c), 2));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.l.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */