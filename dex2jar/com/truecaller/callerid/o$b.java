package com.truecaller.callerid;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class o$b
  extends u<n, Void>
{
  private final i b;
  
  private o$b(e parame, i parami)
  {
    super(parame);
    b = parami;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".onCallerIdWindowShown(");
    localStringBuilder.append(a(b, 2));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.o.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */