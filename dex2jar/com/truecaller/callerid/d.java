package com.truecaller.callerid;

import android.content.Context;
import c.g.b.k;
import com.truecaller.ui.FeedbackDialogActivity;
import com.truecaller.ui.components.FeedbackItemView;
import com.truecaller.ui.components.FeedbackItemView.DisplaySource;
import com.truecaller.ui.components.FeedbackItemView.FeedbackItem;
import javax.inject.Inject;

public final class d
  implements c
{
  private final Context a;
  
  @Inject
  public d(Context paramContext)
  {
    a = paramContext;
  }
  
  public final void a(i parami)
  {
    k.b(parami, "callState");
    parami = FeedbackItemView.a(FeedbackItemView.DisplaySource.BLOCKED_CALL, a);
    if (parami != null) {
      FeedbackDialogActivity.a(a, parami.b(), parami.d());
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */