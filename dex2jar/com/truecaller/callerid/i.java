package com.truecaller.callerid;

import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.filters.g;
import com.truecaller.util.w;
import java.util.Random;

public final class i
{
  public final Number a;
  public final int b;
  public final long c;
  public final long d;
  public final boolean e;
  public final boolean f;
  public final String g;
  public final int h;
  public int i;
  public boolean j;
  public boolean k;
  public Contact l;
  final g m;
  boolean n;
  String o = null;
  
  public i(int paramInt1, int paramInt2, Number paramNumber, int paramInt3, boolean paramBoolean, long paramLong, Contact paramContact, String paramString, g paramg)
  {
    a = paramNumber;
    b = paramInt3;
    f = paramBoolean;
    l = paramContact;
    c = new Random().nextLong();
    d = paramLong;
    if (paramInt1 != 0) {
      paramBoolean = true;
    } else {
      paramBoolean = false;
    }
    e = paramBoolean;
    g = paramString;
    h = paramInt2;
    i = paramInt1;
    m = paramg;
  }
  
  public final boolean a()
  {
    return w.a(l, m);
  }
  
  public final int b()
  {
    if (c()) {
      return 7;
    }
    if (h == 2) {
      return 12;
    }
    if (e)
    {
      if ((i == 3) && (!j)) {
        return 6;
      }
      return 2;
    }
    return 1;
  }
  
  public final boolean c()
  {
    int i1 = h;
    if (i1 != 1) {
      return i1 == 3;
    }
    return true;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("CallState{simSlotIndex=");
    localStringBuilder.append(b);
    localStringBuilder.append(", sessionId=");
    localStringBuilder.append(c);
    localStringBuilder.append(", startTime=");
    localStringBuilder.append(d);
    localStringBuilder.append(", isIncoming=");
    localStringBuilder.append(e);
    localStringBuilder.append(", isFromTrueCaller=");
    localStringBuilder.append(f);
    localStringBuilder.append(", callId='");
    localStringBuilder.append(g);
    localStringBuilder.append('\'');
    localStringBuilder.append(", action=");
    localStringBuilder.append(h);
    localStringBuilder.append(", state=");
    localStringBuilder.append(i);
    localStringBuilder.append(", wasConnected=");
    localStringBuilder.append(j);
    localStringBuilder.append(", isSearching=");
    localStringBuilder.append(k);
    localStringBuilder.append(", contact=");
    localStringBuilder.append(l);
    if (localStringBuilder.toString() == null) {
      return "null";
    }
    localStringBuilder = new StringBuilder("<non-null contact>, filter action=");
    localStringBuilder.append(m.h);
    localStringBuilder.append(", wasSearchPerformed=");
    localStringBuilder.append(n);
    localStringBuilder.append(", noSearchReason='");
    localStringBuilder.append(o);
    localStringBuilder.append('\'');
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */