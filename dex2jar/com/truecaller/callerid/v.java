package com.truecaller.callerid;

import dagger.a.d;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import javax.inject.Provider;

public final class v
  implements d<aj>
{
  private final q a;
  private final Provider<ScheduledThreadPoolExecutor> b;
  
  private v(q paramq, Provider<ScheduledThreadPoolExecutor> paramProvider)
  {
    a = paramq;
    b = paramProvider;
  }
  
  public static v a(q paramq, Provider<ScheduledThreadPoolExecutor> paramProvider)
  {
    return new v(paramq, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.v
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */