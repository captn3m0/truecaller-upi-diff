package com.truecaller.callerid;

import com.truecaller.analytics.ae;
import com.truecaller.androidactors.f;
import com.truecaller.calling.recorder.h;
import com.truecaller.filters.FilterManager;
import com.truecaller.i.c;
import com.truecaller.util.al;
import com.truecaller.util.cn;
import com.truecaller.util.u;
import dagger.a.d;
import javax.inject.Provider;

public final class r
  implements d<ag>
{
  private final q a;
  private final Provider<c> b;
  private final Provider<u> c;
  private final Provider<al> d;
  private final Provider<cn> e;
  private final Provider<FilterManager> f;
  private final Provider<f<ae>> g;
  private final Provider<h> h;
  
  private r(q paramq, Provider<c> paramProvider, Provider<u> paramProvider1, Provider<al> paramProvider2, Provider<cn> paramProvider3, Provider<FilterManager> paramProvider4, Provider<f<ae>> paramProvider5, Provider<h> paramProvider6)
  {
    a = paramq;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
    f = paramProvider4;
    g = paramProvider5;
    h = paramProvider6;
  }
  
  public static r a(q paramq, Provider<c> paramProvider, Provider<u> paramProvider1, Provider<al> paramProvider2, Provider<cn> paramProvider3, Provider<FilterManager> paramProvider4, Provider<f<ae>> paramProvider5, Provider<h> paramProvider6)
  {
    return new r(paramq, paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.r
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */