package com.truecaller.callerid;

import android.app.Notification.Builder;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.IBinder;
import com.truecaller.TrueApp;
import com.truecaller.aftercall.AfterCallPromotionActivity;
import com.truecaller.aftercall.PromotionType;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.callerid.b.c.a;
import com.truecaller.calling.after_call.AfterCallActivity;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.notificationchannels.e;
import com.truecaller.ui.o;
import javax.inject.Inject;

public class CallerIdService
  extends Service
  implements c.a, k
{
  @Inject
  public f<n> a;
  @Inject
  public e b;
  private com.truecaller.callerid.b.c c;
  
  public static void a(Context paramContext, Bundle paramBundle)
  {
    Intent localIntent = new Intent(paramContext, CallerIdService.class);
    localIntent.addFlags(32);
    localIntent.putExtras(paramBundle);
    if (Build.VERSION.SDK_INT >= 26)
    {
      paramContext.startForegroundService(localIntent);
      return;
    }
    try
    {
      paramContext.startService(localIntent);
      return;
    }
    catch (SecurityException paramContext)
    {
      AssertionUtil.reportThrowableButNeverCrash(paramContext);
    }
  }
  
  public final void a()
  {
    stopSelf();
  }
  
  public final void a(PromotionType paramPromotionType, HistoryEvent paramHistoryEvent, com.truecaller.i.c paramc)
  {
    AfterCallPromotionActivity.a(this, paramc, paramPromotionType, paramHistoryEvent);
  }
  
  public final void a(i parami, boolean paramBoolean)
  {
    if ((c == null) && (paramBoolean))
    {
      com.truecaller.callerid.b.b localb = new com.truecaller.callerid.b.b(this, this);
      if (localb.d())
      {
        c = localb;
        ((n)a.a()).a(parami);
      }
    }
    if (c != null)
    {
      ((n)a.a()).b(parami);
      c.a(parami);
    }
  }
  
  public final void a(HistoryEvent paramHistoryEvent, int paramInt)
  {
    AfterCallActivity.b(this, paramHistoryEvent, paramInt);
  }
  
  public final void b()
  {
    com.truecaller.callerid.b.c localc = c;
    if (localc != null) {
      localc.h();
    }
  }
  
  public final void c()
  {
    o.a(this, "com.truecaller.EVENT_AFTER_CALL_START");
  }
  
  public final w<Boolean> d()
  {
    com.truecaller.callerid.b.c localc = c;
    boolean bool;
    if ((localc != null) && (d)) {
      bool = true;
    } else {
      bool = false;
    }
    return w.b(Boolean.valueOf(bool));
  }
  
  public final void e()
  {
    c = null;
    ((n)a.a()).a();
  }
  
  public IBinder onBind(Intent paramIntent)
  {
    return null;
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    paramConfiguration = c;
    if (paramConfiguration != null) {
      paramConfiguration.i();
    }
  }
  
  public void onCreate()
  {
    super.onCreate();
    Object localObject = ((bk)getApplicationContext()).a();
    boolean bool = ((bp)localObject).D().b("hasNativeDialerCallerId");
    localObject = ((bp)localObject).m().a().a(k.class, this);
    ((TrueApp)getApplication()).a().a(new q(bool ^ true, (f)localObject)).a(this);
  }
  
  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    if (paramIntent != null)
    {
      paramInt1 = paramIntent.getIntExtra("CALL_STATE", -1);
      boolean bool;
      if (paramInt1 != -1) {
        bool = true;
      } else {
        bool = false;
      }
      AssertionUtil.AlwaysFatal.isTrue(bool, new String[0]);
      String str = paramIntent.getStringExtra("NUMBER");
      paramInt2 = paramIntent.getIntExtra("SIM_SLOT_INDEX", -1);
      int i = paramIntent.getIntExtra("ACTION", 0);
      ((n)a.a()).a(paramInt1, str, paramInt2, i);
      if (Build.VERSION.SDK_INT >= 26) {
        startForeground(2131362409, new Notification.Builder(this, b.d()).setSmallIcon(2131234787).setContentTitle(getString(2131886350)).setColor(android.support.v4.content.b.c(this, 2131100594)).build());
      }
    }
    return 1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.CallerIdService
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */