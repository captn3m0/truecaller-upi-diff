package com.truecaller.callerid;

import android.text.TextUtils;
import com.truecaller.analytics.b;
import com.truecaller.androidactors.w;
import com.truecaller.data.access.c;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.f;
import com.truecaller.log.AssertionUtil;
import com.truecaller.network.search.j;
import com.truecaller.network.search.n;
import com.truecaller.util.al;
import com.truecaller.utils.a;
import com.truecaller.utils.i;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

final class af
  implements ad
{
  private final c a;
  private final al b;
  private final a c;
  private final am d;
  private final b e;
  private final i f;
  private final e g;
  
  af(c paramc, al paramal, a parama, am paramam, b paramb, i parami, e parame)
  {
    a = paramc;
    b = paramal;
    c = parama;
    d = paramam;
    e = paramb;
    f = parami;
    g = parame;
  }
  
  public final w<Contact> a(Number paramNumber, boolean paramBoolean, int paramInt, j paramj)
  {
    com.truecaller.analytics.e.a locala = new com.truecaller.analytics.e.a("CallerIdSearch");
    if ((paramBoolean) && (!TextUtils.isEmpty(paramNumber.a())))
    {
      localObject1 = a.b(paramNumber.a());
      if ((localObject1 != null) && (!((Contact)localObject1).T()))
      {
        locala.a("Result", "Cache");
        e.a(locala.a());
        return w.b(localObject1);
      }
    }
    if (!b.a()) {
      return w.b(null);
    }
    Object localObject1 = g;
    int i = ((f)L.a((e)localObject1, e.a[103])).a(3000);
    i = paramNumber.d();
    localObject1 = TimeUnit.MILLISECONDS;
    l = i;
    m = ((TimeUnit)localObject1);
    n = true;
    localObject1 = paramj.b(paramNumber.l());
    h = paramInt;
    b = false;
    d = true;
    e = true;
    c = true;
    locala.a("Result", "Fail");
    locala.a("LastAttemptNetworkType", "no-connection");
    locala.a("ConnectTimeout", i);
    long l2 = c.b();
    localObject1 = null;
    long l1 = l2;
    paramInt = 0;
    Object localObject3;
    for (;;)
    {
      localObject3 = localObject1;
      if (paramInt >= 6) {
        break;
      }
      i = paramInt + 1;
      locala.a("Attempts", i);
      Object localObject4;
      if (f.a())
      {
        localObject3 = localObject1;
        localObject4 = localObject1;
        try
        {
          locala.a("LastAttemptNetworkType", f.b());
          localObject3 = localObject1;
          localObject4 = localObject1;
          localObject1 = paramj.f();
          localObject3 = localObject1;
          localObject4 = localObject1;
          locala.a("Result", "Success");
          localObject3 = localObject1;
        }
        catch (RuntimeException localRuntimeException) {}catch (IOException localIOException)
        {
          localObject3 = localObject4;
        }
        localObject4 = new StringBuilder("Search for ");
        ((StringBuilder)localObject4).append(paramNumber);
        ((StringBuilder)localObject4).append(" failed");
        ((StringBuilder)localObject4).toString();
        AssertionUtil.reportThrowableButNeverCrash(localIOException);
        localObject4 = localObject3;
        if (paramInt < 5)
        {
          d.a(500L);
          localObject4 = localObject3;
        }
      }
      else
      {
        localObject4 = localIOException;
        if (paramInt < 5)
        {
          d.a(1500L);
          localObject4 = localIOException;
        }
      }
      l1 = c.b();
      paramInt = i;
      Object localObject2 = localObject4;
    }
    long l3 = c.b();
    double d1 = l3;
    double d2 = l2;
    Double.isNaN(d1);
    Double.isNaN(d2);
    a = Double.valueOf(d1 - d2);
    l1 = TimeUnit.MILLISECONDS.toSeconds(l3 - l1);
    if (l1 < 1L) {
      paramNumber = "0-1";
    } else if (l1 < 2L) {
      paramNumber = "1-2";
    } else if (l1 < 3L) {
      paramNumber = "2-3";
    } else if (l1 < 4L) {
      paramNumber = "3-4";
    } else if (l1 < 5L) {
      paramNumber = "4-5";
    } else if (l1 < 6L) {
      paramNumber = "5-6";
    } else if (l1 < 7L) {
      paramNumber = "6-7";
    } else if (l1 < 8L) {
      paramNumber = "7-8";
    } else if (l1 < 9L) {
      paramNumber = "8-9";
    } else {
      paramNumber = ">9";
    }
    locala.a("LastAttemptDurationBucket", paramNumber);
    e.a(locala.a());
    if (localObject3 == null) {
      return w.b(null);
    }
    return w.b(((n)localObject3).a());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.af
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */