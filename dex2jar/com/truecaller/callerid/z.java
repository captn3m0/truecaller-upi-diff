package com.truecaller.callerid;

import com.truecaller.analytics.b;
import com.truecaller.data.access.c;
import com.truecaller.featuretoggles.e;
import com.truecaller.util.al;
import com.truecaller.utils.a;
import com.truecaller.utils.i;
import dagger.a.d;
import javax.inject.Provider;

public final class z
  implements d<ad>
{
  private final q a;
  private final Provider<c> b;
  private final Provider<al> c;
  private final Provider<a> d;
  private final Provider<am> e;
  private final Provider<b> f;
  private final Provider<i> g;
  private final Provider<e> h;
  
  private z(q paramq, Provider<c> paramProvider, Provider<al> paramProvider1, Provider<a> paramProvider2, Provider<am> paramProvider3, Provider<b> paramProvider4, Provider<i> paramProvider5, Provider<e> paramProvider6)
  {
    a = paramq;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
    f = paramProvider4;
    g = paramProvider5;
    h = paramProvider6;
  }
  
  public static z a(q paramq, Provider<c> paramProvider, Provider<al> paramProvider1, Provider<a> paramProvider2, Provider<am> paramProvider3, Provider<b> paramProvider4, Provider<i> paramProvider5, Provider<e> paramProvider6)
  {
    return new z(paramq, paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.z
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */