package com.truecaller.callerid.b;

import android.arch.lifecycle.e.b;
import android.arch.lifecycle.h;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.support.v4.widget.m;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.d.b.ab;
import com.d.b.t;
import com.truecaller.TrueApp;
import com.truecaller.ads.campaigns.AdCampaign;
import com.truecaller.ads.campaigns.AdCampaign.Style;
import com.truecaller.ads.campaigns.AdCampaigns;
import com.truecaller.ads.j.a;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.k;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.h.am;
import com.truecaller.common.h.aq.b;
import com.truecaller.common.tag.TagView;
import com.truecaller.data.entity.Address;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.g;
import com.truecaller.log.AssertionUtil;
import com.truecaller.old.data.access.Settings;
import com.truecaller.premium.ShineView;
import com.truecaller.ui.components.AvatarView;
import com.truecaller.ui.components.RippleView;
import com.truecaller.ui.view.TintedImageView;
import com.truecaller.util.b.ao;
import com.truecaller.util.b.j.b;
import com.truecaller.util.ce;
import com.truecaller.util.co;

public final class b
  extends c
  implements h
{
  private TintedImageView A;
  private TintedImageView B;
  private View C;
  private TintedImageView D;
  private View E;
  private ShineView F;
  private final com.truecaller.i.c G;
  private final g H;
  private final f<com.truecaller.callhistory.a> I;
  private final k J;
  private final f<com.truecaller.ads.provider.a.b> K;
  private final boolean L;
  private com.truecaller.androidactors.a M;
  private com.truecaller.utils.i N;
  private android.arch.lifecycle.i O;
  private final bp i;
  private RippleView j;
  private TextView k;
  private TextView l;
  private TextView m;
  private TextView n;
  private TextView o;
  private TextView p;
  private View q;
  private TextView r;
  private View s;
  private ImageView t;
  private AvatarView u;
  private TextView v;
  private TextView w;
  private ViewGroup x;
  private View y;
  private View z;
  
  public b(Context paramContext, c.a parama)
  {
    super(paramContext, parama);
    i = ((bk)paramContext.getApplicationContext()).a();
    G = i.D();
    H = i.aa();
    I = i.ad();
    J = i.m();
    K = i.au();
    N = i.v();
    L = i.aF().A().a();
    O = new android.arch.lifecycle.i(this);
  }
  
  public static int a(Context paramContext)
  {
    Resources localResources = paramContext.getResources();
    return getDisplayMetricsheightPixels / 2 - com.truecaller.util.at.a(paramContext, 180.0F) / 2 - com.truecaller.util.at.a(localResources);
  }
  
  private void a(AdCampaigns paramAdCampaigns)
  {
    if (paramAdCampaigns != null)
    {
      paramAdCampaigns = paramAdCampaigns.b();
      if (paramAdCampaigns != null)
      {
        paramAdCampaigns = b;
        break label23;
      }
    }
    paramAdCampaigns = null;
    label23:
    if (paramAdCampaigns == null) {
      return;
    }
    Object localObject = h.findViewById(2131362414);
    int i1 = a.getResources().getDimensionPixelSize(2131165186);
    GradientDrawable localGradientDrawable = new GradientDrawable();
    localGradientDrawable.setColor(a);
    float f = i1;
    localGradientDrawable.setCornerRadii(new float[] { 0.0F, 0.0F, 0.0F, 0.0F, f, f, f, f });
    com.truecaller.util.at.a((View)localObject, localGradientDrawable);
    ((TextView)h.findViewById(2131361975)).setTextColor(b);
    localObject = (ImageView)h.findViewById(2131361973);
    com.d.b.w.a(a).a(e).a((ImageView)localObject, new com.d.b.e()
    {
      public final void onError() {}
      
      public final void onSuccess()
      {
        if (h != null) {
          h.findViewById(2131362414).setVisibility(0);
        }
      }
    });
  }
  
  protected final a a()
  {
    return new a(this, ViewConfiguration.get(a).getScaledTouchSlop());
  }
  
  protected final void a(View paramView)
  {
    s = paramView.findViewById(2131363270);
    C = paramView.findViewById(2131362401);
    t = ((ImageView)paramView.findViewById(2131363990));
    v = ((TextView)paramView.findViewById(2131362413));
    w = ((TextView)paramView.findViewById(2131362411));
    x = ((ViewGroup)paramView.findViewById(2131364653));
    y = paramView.findViewById(2131364018);
    z = paramView.findViewById(2131362412);
    u = ((AvatarView)h.findViewById(2131362407));
    t.setColorFilter(android.support.v4.content.b.c(a, 2131100098));
    A = ((TintedImageView)paramView.findViewById(2131362399));
    A.setOnClickListener(new -..Lambda.b.GsnYxHuUuc6Gy_rihjDbuFZy3XA(this));
    D = ((TintedImageView)paramView.findViewById(2131362402));
    if (co.c(a)) {
      D.setImageResource(2131234510);
    }
    j = ((RippleView)paramView.findViewById(2131364180));
    k = ((TextView)paramView.findViewById(2131362398));
    l = ((TextView)paramView.findViewById(2131362408));
    m = ((TextView)paramView.findViewById(2131362403));
    n = ((TextView)paramView.findViewById(2131362405));
    o = ((TextView)paramView.findViewById(2131362406));
    p = ((TextView)paramView.findViewById(2131362404));
    q = paramView.findViewById(2131363941);
    B = ((TintedImageView)paramView.findViewById(2131362410));
    r = ((TextView)paramView.findViewById(2131362271));
    E = paramView.findViewById(2131363192);
    F = ((ShineView)paramView.findViewById(2131363196));
    F.setLifecycleOwner(this);
    u.addOnLayoutChangeListener(new -..Lambda.b.nIGAkseUnfoZb7dH7T3TAACRPos(this));
  }
  
  protected final void a(com.truecaller.callerid.i parami, boolean paramBoolean)
  {
    AssertionUtil.isNotNull(l, new String[0]);
    if (paramBoolean) {
      t.setVisibility(8);
    }
    Object localObject2 = "";
    w.setMaxLines(3);
    w.setTextSize(12.0F);
    int i1 = 117;
    if (L) {
      i1 = 245;
    }
    Object localObject1 = localObject2;
    if (!l.c(i1))
    {
      localObject1 = localObject2;
      if (!N.a()) {
        localObject1 = a.getString(2131886344);
      }
    }
    Object localObject3 = l.t();
    if (!am.a((CharSequence)localObject3)) {
      localObject3 = com.truecaller.util.at.a(a.n());
    }
    boolean bool2 = l.a(16);
    boolean bool3 = l.a(32);
    localObject2 = l;
    if (bool2) {
      i1 = 0;
    } else {
      i1 = 8;
    }
    ((TextView)localObject2).setVisibility(i1);
    Contact localContact = l;
    u.a(l);
    if ((localContact.a(32)) && (!parami.a()))
    {
      s.setBackground(null);
      com.truecaller.util.at.a(C, 2131230999);
      E.setVisibility(0);
      F.setVisibility(0);
      i3 = android.support.v4.content.b.c(a, 2131100383);
      int i4 = android.support.v4.content.b.c(a, 2131100384);
      int i5 = com.truecaller.util.at.a(a, 1.0F);
      int i6 = android.support.v4.content.b.c(a, 2131100381);
      A.setTint(android.support.v4.content.b.c(a, 2131100380));
      D.setTint(null);
      localObject2 = D;
      if (co.c(a)) {
        i1 = 2131234630;
      } else {
        i1 = 2131234629;
      }
      ((TintedImageView)localObject2).setImageResource(i1);
      v.setTextColor(i3);
      localObject2 = v;
      float f = i5;
      ((TextView)localObject2).setShadowLayer(1.0F, 0.0F, f, i6);
      v.setTypeface(Typeface.create("sans-serif-medium", 0));
      w.setTextColor(i4);
      w.setShadowLayer(1.0F, 0.0F, f, i6);
      k.setTextColor(i4);
      k.setShadowLayer(1.0F, 0.0F, f, i6);
      r.setTextColor(i4);
      r.setShadowLayer(1.0F, 0.0F, f, i6);
      m.setTextColor(i4);
      m.setShadowLayer(1.0F, 0.0F, f, i6);
      n.setTextColor(i3);
      n.setShadowLayer(1.0F, 0.0F, f, i6);
      o.setTextColor(i4);
      o.setShadowLayer(1.0F, 0.0F, f, i6);
      p.setTextColor(i4);
      p.setShadowLayer(1.0F, 0.0F, f, i6);
      localObject2 = new com.truecaller.ui.components.b(localContact.a(true), localContact.a(false), true, false);
      u.a((com.truecaller.ui.components.b)localObject2);
    }
    else if (bool2)
    {
      localObject2 = new com.truecaller.ui.components.b(localContact.a(true), localContact.a(false), localContact.a(32), localContact.a(4));
      u.a((com.truecaller.ui.components.b)localObject2);
      com.truecaller.utils.ui.b.a(s, 2130969576);
    }
    else if (parami.a())
    {
      u.a();
      com.truecaller.utils.ui.b.a(s, 2130969585);
    }
    else
    {
      localObject2 = new com.truecaller.ui.components.b(localContact.a(true), localContact.a(false), localContact.a(32), localContact.a(4));
      u.a((com.truecaller.ui.components.b)localObject2);
    }
    Object localObject4;
    if (parami.a())
    {
      i1 = com.truecaller.util.w.b(l, a);
      localObject2 = localObject1;
      if (i1 > 0) {
        localObject2 = g().getString(2131886146, new Object[] { Integer.valueOf(i1) });
      }
    }
    else
    {
      if (!am.a((CharSequence)localObject1))
      {
        localObject1 = l.g();
        if (localObject1 != null) {
          localObject1 = am.a(", ", new CharSequence[] { ((Address)localObject1).getCityOrArea(), ((Address)localObject1).getCountryName() });
        } else {
          localObject1 = "";
        }
        w.setMaxLines(1);
        w.setTextSize(14.0F);
      }
      localObject4 = l.a(true);
      localObject2 = localObject1;
      if (!bool2)
      {
        localObject2 = localObject1;
        if (!bool3)
        {
          com.truecaller.utils.ui.b.a(s, 2130968622);
          if (localObject4 != null)
          {
            localObject2 = com.d.b.w.a(a).a((Uri)localObject4);
            c = true;
            localObject2 = ((ab)localObject2).b().a().a(new aq.b());
            if (!Settings.d()) {
              ((ab)localObject2).a(t.c);
            }
            t.setVisibility(0);
            ((ab)localObject2).a(t, new com.d.b.e.a()
            {
              public final void onError()
              {
                b.a(b.this).setVisibility(8);
              }
            });
            localObject2 = localObject1;
          }
          else
          {
            t.setImageBitmap(null);
            localObject2 = localObject1;
          }
        }
      }
    }
    boolean bool1 = k ^ true;
    com.truecaller.util.at.a(y, k);
    com.truecaller.util.at.b(v, (CharSequence)localObject3);
    com.truecaller.util.at.b(w, (CharSequence)localObject2);
    if (l.a(2)) {
      localObject1 = com.truecaller.util.at.a(g(), 2131233985);
    } else {
      localObject1 = null;
    }
    localObject3 = v;
    localObject2 = null;
    m.a((TextView)localObject3, null, (Drawable)localObject1);
    if (bool1)
    {
      localObject1 = a;
      localObject3 = l;
      if ((localObject3 == null) || (!((Contact)localObject3).a(32)))
      {
        localObject1 = ((Number)localObject1).a();
        if ((Settings.h()) && (G.b("afterCall")) && (localObject1 != null))
        {
          localObject3 = (com.truecaller.ads.provider.a.b)K.a();
          localObject4 = new j.a("CALLERID");
          a = ((String)localObject1);
          M = ((com.truecaller.ads.provider.a.b)localObject3).a(((j.a)localObject4).a()).a(J.a(), new -..Lambda.b.9kbigglasJRfuEOsFnuvk8Ke7FA(this));
        }
      }
    }
    if (x != null)
    {
      localObject1 = localObject2;
      if (bool1) {
        localObject1 = ce.a(l.J());
      }
      if (localObject1 != null)
      {
        x.removeAllViews();
        localObject2 = new TagView(g(), true, true);
        ((TagView)localObject2).setTag((com.truecaller.common.tag.c)localObject1);
        x.addView((View)localObject2);
        x.setVisibility(0);
      }
      else
      {
        x.setVisibility(8);
      }
    }
    if (l != null)
    {
      if ((paramBoolean) && (!TextUtils.isEmpty(a.d())))
      {
        p.setVisibility(8);
        ((com.truecaller.callhistory.a)I.a()).c(a.a()).a(J.a(), new -..Lambda.b.-XX1C3u_MwwTadPF7h9-8zqgp74(this));
      }
      com.truecaller.util.at.b(k, am.e(l.K(), l.l()));
      com.truecaller.util.at.b(m, am.a(" @ ", new CharSequence[] { l.x(), l.o() }));
      localObject2 = com.truecaller.util.w.a(l, a);
      localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = a;
      }
      localObject3 = H.a((Number)localObject1);
      localObject2 = "";
      localObject4 = ((Number)localObject1).g();
      String str = ((Number)localObject1).n();
      localObject1 = localObject2;
      if (!TextUtils.isEmpty(str)) {
        localObject1 = android.support.v4.e.a.a().a(str, android.support.v4.e.e.a);
      }
      com.truecaller.util.at.b(n, (CharSequence)localObject1);
      com.truecaller.util.at.b(o, am.a(" - ", new CharSequence[] { localObject3, localObject4 }));
      localObject1 = j;
      if (e) {
        bool1 = false;
      } else {
        i2 = 8;
      }
      ((RippleView)localObject1).setVisibility(i2);
      switch (b)
      {
      default: 
        B.setVisibility(8);
        break;
      case 1: 
        B.setImageResource(2131234550);
        break;
      case 0: 
        B.setImageResource(2131234549);
        break;
      }
    }
    int i3 = 8;
    if (paramBoolean)
    {
      localObject2 = new com.truecaller.analytics.e.a("CALLERID_CallerIDWindow_Viewed");
      if ((l != null) && (l.Z()))
      {
        if (e) {
          localObject1 = "IncomingCallPhonebook";
        } else {
          localObject1 = "OutgoingCallPhonebook";
        }
      }
      else if (e) {
        localObject1 = "IncomingCall";
      } else {
        localObject1 = "OutgoingCall";
      }
      ((com.truecaller.analytics.e.a)localObject2).a("Call_Type", (String)localObject1);
      if ((l != null) && (l.a(32))) {
        paramBoolean = true;
      } else {
        paramBoolean = false;
      }
      ((com.truecaller.analytics.e.a)localObject2).a("IsGold", paramBoolean);
      if ((l != null) && (l.N())) {
        paramBoolean = true;
      } else {
        paramBoolean = false;
      }
      ((com.truecaller.analytics.e.a)localObject2).a("IsBusiness", paramBoolean);
      TrueApp.y().a().c().a(((com.truecaller.analytics.e.a)localObject2).a());
    }
    parami = k;
    if (localContact.N()) {
      i2 = 8;
    } else {
      i2 = 0;
    }
    parami.setVisibility(i2);
    parami = r;
    int i2 = i3;
    if (localContact.N()) {
      i2 = 0;
    }
    parami.setVisibility(i2);
  }
  
  protected final void b()
  {
    O.a(e.b.e);
    View localView = h.findViewById(2131363887);
    j.b localb = com.truecaller.util.b.at.b(a);
    boolean bool = localb.a();
    int i1 = 1;
    if (!bool)
    {
      localView.setVisibility(8);
    }
    else
    {
      int i2 = a.getResources().getDimensionPixelSize(2131165186);
      GradientDrawable localGradientDrawable = new GradientDrawable();
      localGradientDrawable.setColor(e);
      float f = i2;
      localGradientDrawable.setCornerRadii(new float[] { 0.0F, 0.0F, 0.0F, 0.0F, f, f, f, f });
      com.truecaller.util.at.a(localView, localGradientDrawable);
      ((ImageView)h.findViewById(2131363886)).setImageResource(d);
      ((TextView)localView.findViewById(2131363968)).setTextColor(f);
      localView.setVisibility(0);
    }
    if (((com.truecaller.util.b.at.a(a) instanceof ao)) || (G.a("callerIdHintCount", 0) > 0)) {
      i1 = 0;
    }
    if (i1 != 0) {
      z.setVisibility(0);
    } else {
      z.setVisibility(8);
    }
    h.postDelayed(new -..Lambda.b.w1_21gPSzMPLjOne--ld7p6B4GQ(this), 1000L);
  }
  
  public final void c()
  {
    O.a(e.b.c);
    com.truecaller.androidactors.a locala = M;
    if (locala != null)
    {
      locala.a();
      M = null;
    }
    super.c();
  }
  
  public final android.arch.lifecycle.e getLifecycle()
  {
    return O;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.b.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */