package com.truecaller.callerid.b;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.content.d;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.bk;
import com.truecaller.bp;

public final class a
  implements View.OnTouchListener
{
  private final c a;
  private final float b;
  private final float c;
  private float d;
  private float e;
  private int f;
  private boolean g;
  private boolean h;
  private boolean i;
  private final com.truecaller.i.c j;
  private final int k;
  private final VelocityTracker l = VelocityTracker.obtain();
  
  public a(c paramc, int paramInt)
  {
    a = paramc;
    k = paramInt;
    float f1 = a.a.getResources().getDisplayMetrics().density;
    c = (25.0F * f1);
    b = (f1 * 400.0F);
    j = ((bk)paramc.g().getApplicationContext()).a().D();
  }
  
  @SuppressLint({"NewApi"})
  public final boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    if (!a.d) {
      return true;
    }
    l.addMovement(paramMotionEvent);
    int i1 = paramMotionEvent.getAction();
    int n = 0;
    int m = 0;
    float f1;
    switch (i1)
    {
    default: 
      return false;
    case 2: 
      f1 = paramMotionEvent.getRawX();
      float f2 = paramMotionEvent.getRawY();
      f1 -= d;
      f2 -= e;
      if ((!g) && (!h)) {
        if (Math.abs(f2) > k)
        {
          h = true;
          if (!i)
          {
            i = true;
            j.a_("callerIdHintCount");
          }
        }
        else if (Math.abs(f1) > k)
        {
          g = true;
        }
      }
      if (h)
      {
        n = (int)(f + f2);
        if (n >= 0) {
          if (n > a.g - a.e()) {
            m = a.g - a.e();
          } else {
            m = n;
          }
        }
        paramView = a;
        if (e != null) {
          e.y = m;
        }
        paramView = a;
        c.updateViewLayout(b, e);
        d.a(TrueApp.x()).a(new Intent("BroadcastCallerIdPosY").putExtra("ExtraPosY", m));
      }
      if (g)
      {
        f2 = Math.max(0.0F, Math.min(1.0F, 1.0F - Math.abs(f1) / a.g));
        a.b(f2);
        a.a(f1);
      }
      return true;
    case 1: 
      if (g)
      {
        l.computeCurrentVelocity(1000);
        f1 = l.getXVelocity();
        if (((Math.abs(f1) > b) && (Math.abs(d - paramMotionEvent.getRawX()) > c)) || (Math.abs(a.f()) >= a.f / 2))
        {
          if (Math.abs(a.f()) >= a.f / 2) {
            f1 = a.f();
          }
          paramView = a;
          paramView.a((int)Math.copySign(f, f1), true);
          TrueApp.y().a().c().a(new e.a("CALLERID_CallerIDWindow_Dismissed").a("Dismiss_Type", "SwipeAway").a());
        }
        else
        {
          a.a(0.0F, false);
        }
        g = false;
      }
      else
      {
        TrueApp.y().a().c().a(new e.a("CALLERID_CallerIDWindow_Moved").a());
      }
      h = false;
      return true;
    }
    d = paramMotionEvent.getRawX();
    e = paramMotionEvent.getRawY();
    paramView = a;
    m = n;
    if (e != null) {
      m = e.y;
    }
    f = m;
    m = a.g - a.e();
    if (f > m) {
      f = m;
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.b.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */