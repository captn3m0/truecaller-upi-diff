package com.truecaller.callerid.b;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.support.v4.view.r;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.callerid.i;
import com.truecaller.common.h.k;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.log.d;
import com.truecaller.old.data.access.Settings;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.util.at;
import com.truecaller.utils.l;

public abstract class c
{
  protected final Context a;
  FrameLayout b;
  WindowManager c;
  public boolean d;
  WindowManager.LayoutParams e;
  int f;
  int g;
  protected View h;
  private final a i;
  private final int j;
  private i k;
  private l l;
  
  c(Context paramContext, a parama)
  {
    a = new ContextThemeWrapper(paramContext, aresId);
    i = parama;
    l = ((bk)paramContext.getApplicationContext()).a().bw();
    j = a.getResources().getInteger(17694720);
  }
  
  public static int a(l paraml)
  {
    if (k.h()) {
      return 2038;
    }
    if (paraml.a()) {
      return 2010;
    }
    return 2005;
  }
  
  @SuppressLint({"NewApi"})
  private void j()
  {
    d = true;
    b.setVisibility(0);
    h.clearAnimation();
    h.setAlpha(0.0F);
    h.setTranslationX(f);
    a(0.0F, false);
    b();
  }
  
  protected abstract a a();
  
  final void a(float paramFloat)
  {
    View localView = h;
    if (localView != null) {
      localView.setTranslationX(paramFloat);
    }
  }
  
  final void a(float paramFloat, final boolean paramBoolean)
  {
    float f1;
    Object localObject;
    if (!paramBoolean)
    {
      f1 = 1.0F;
      localObject = new AccelerateDecelerateInterpolator();
    }
    else
    {
      localObject = new LinearInterpolator();
      f1 = 0.0F;
    }
    d = (paramBoolean ^ true);
    h.animate().translationX(paramFloat).alpha(f1).setDuration(j).setInterpolator((TimeInterpolator)localObject).setListener(new AnimatorListenerAdapter()
    {
      public final void onAnimationEnd(Animator paramAnonymousAnimator)
      {
        if (paramBoolean) {
          c();
        }
      }
    });
  }
  
  protected abstract void a(View paramView);
  
  public final void a(i parami)
  {
    i locali = k;
    boolean bool;
    if ((locali != null) && (c == c)) {
      bool = false;
    } else {
      bool = true;
    }
    if (((com.truecaller.common.b.a)a.getApplicationContext()).p())
    {
      if (l == null) {
        return;
      }
      if (!d) {
        if (bool) {
          j();
        } else {
          return;
        }
      }
      k = parami;
      a(parami, bool);
      return;
    }
  }
  
  protected void a(i parami, boolean paramBoolean) {}
  
  protected abstract void b();
  
  final void b(float paramFloat)
  {
    View localView = h;
    if (localView != null) {
      localView.setAlpha(paramFloat);
    }
  }
  
  public void c()
  {
    WindowManager.LayoutParams localLayoutParams;
    if (!d)
    {
      boolean bool;
      if (e != null) {
        bool = true;
      } else {
        bool = false;
      }
      AssertionUtil.OnlyInDebug.isTrue(bool, new String[0]);
      localLayoutParams = e;
      if (localLayoutParams != null) {
        height = -2;
      }
    }
    try
    {
      c.updateViewLayout(b, localLayoutParams);
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      for (;;) {}
    }
    Settings.a("callerIdLastYPosition", e.y);
    if (r.H(b))
    {
      b.setVisibility(8);
      c.removeView(b);
    }
    i.e();
    return;
    j();
  }
  
  public final boolean d()
  {
    LayoutInflater localLayoutInflater = (LayoutInflater)a.getSystemService("layout_inflater");
    c = ((WindowManager)a.getSystemService("window"));
    Object localObject = a.getResources().getDisplayMetrics();
    f = widthPixels;
    g = (heightPixels - at.a(a.getResources()));
    e = new WindowManager.LayoutParams(-1, -2, a(l), 524296, -3);
    localObject = e;
    gravity = 49;
    dimAmount = 0.6F;
    y = Settings.c("callerIdLastYPosition");
    b = new FrameLayout(a);
    b.setVisibility(8);
    try
    {
      c.addView(b, e);
      h = localLayoutInflater.inflate(2131559156, null);
      b.addView(h);
      b.setOnTouchListener(a());
      a(h);
      return true;
    }
    catch (RuntimeException localRuntimeException)
    {
      d.a(localRuntimeException, "Cannot add caller id window");
    }
    return false;
  }
  
  final int e()
  {
    View localView = h;
    if (localView != null) {
      return localView.getHeight();
    }
    return 0;
  }
  
  final float f()
  {
    View localView = h;
    if (localView != null) {
      return localView.getTranslationX();
    }
    return 0.0F;
  }
  
  public final Context g()
  {
    return a;
  }
  
  @SuppressLint({"NewApi"})
  public final void h()
  {
    d = false;
    a(h.getTranslationX(), true);
  }
  
  public final void i()
  {
    DisplayMetrics localDisplayMetrics = a.getResources().getDisplayMetrics();
    f = widthPixels;
    g = (heightPixels - at.a(a.getResources()));
  }
  
  public static abstract interface a
  {
    public abstract void e();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.b.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */