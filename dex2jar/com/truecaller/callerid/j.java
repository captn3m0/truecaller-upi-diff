package com.truecaller.callerid;

import com.truecaller.common.h.am;
import com.truecaller.common.tag.c;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.Tag;
import com.truecaller.filters.FilterManager.ActionSource;
import com.truecaller.filters.g;
import com.truecaller.tracking.events.al;
import com.truecaller.tracking.events.al.a;
import com.truecaller.tracking.events.ay;
import com.truecaller.tracking.events.ay.a;
import com.truecaller.tracking.events.bc;
import com.truecaller.tracking.events.bc.a;
import com.truecaller.tracking.events.z;
import com.truecaller.tracking.events.z.a;
import com.truecaller.util.ce;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import org.c.a.a.a.k;

public final class j
{
  public static z a(i parami)
  {
    if (n) {
      return null;
    }
    z.a locala = z.b();
    locala.a(UUID.randomUUID().toString()).d("callerId").c(String.valueOf(parami.b()));
    locala.b(null);
    boolean bool2 = false;
    locala.a(false);
    locala.b(false);
    ArrayList localArrayList = new ArrayList();
    Object localObject1;
    if (l != null)
    {
      Contact localContact = l;
      localObject1 = m;
      Object localObject2 = al.b().b(am.b(localContact.z()) ^ true);
      if ((localContact.getSource() & 0x2) != 0) {
        bool1 = true;
      } else {
        bool1 = false;
      }
      localObject2 = ((al.a)localObject2).a(bool1).a(Integer.valueOf(Math.max(0, localContact.I()))).d(Boolean.valueOf(localContact.U()));
      if (j == FilterManager.ActionSource.CUSTOM_BLACKLIST) {
        bool1 = true;
      } else {
        bool1 = false;
      }
      localObject2 = ((al.a)localObject2).a(Boolean.valueOf(bool1));
      if (j == FilterManager.ActionSource.CUSTOM_WHITELIST) {
        bool1 = true;
      } else {
        bool1 = false;
      }
      localObject2 = ((al.a)localObject2).c(Boolean.valueOf(bool1));
      if (j == FilterManager.ActionSource.TOP_SPAMMER) {
        bool1 = true;
      } else {
        bool1 = false;
      }
      localObject1 = ((al.a)localObject2).b(Boolean.valueOf(bool1));
      boolean bool1 = bool2;
      if ((localContact.getSource() & 0x40) != 0) {
        bool1 = true;
      }
      al localal = ((al.a)localObject1).e(Boolean.valueOf(bool1)).a();
      Object localObject4 = new ArrayList();
      Object localObject3 = new ArrayList();
      localObject2 = new ArrayList();
      localObject1 = localContact.J().iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject5 = (Tag)((Iterator)localObject1).next();
        if (((Tag)localObject5).getSource() == 1) {
          ((List)localObject4).add(((Tag)localObject5).a());
        } else {
          ((List)localObject3).add(((Tag)localObject5).a());
        }
      }
      localObject1 = ce.a(localContact);
      if (localObject1 != null) {
        ((List)localObject2).add(String.valueOf(a));
      }
      Object localObject5 = bc.b();
      localObject1 = localObject4;
      if (((List)localObject4).isEmpty()) {
        localObject1 = null;
      }
      localObject4 = ((bc.a)localObject5).a((List)localObject1);
      localObject1 = localObject3;
      if (((List)localObject3).isEmpty()) {
        localObject1 = null;
      }
      localObject3 = ((bc.a)localObject4).b((List)localObject1);
      localObject1 = localObject2;
      if (((List)localObject2).isEmpty()) {
        localObject1 = null;
      }
      localObject2 = ((bc.a)localObject3).c((List)localObject1).a();
      localObject3 = localContact.A().iterator();
      localObject1 = null;
      while (((Iterator)localObject3).hasNext())
      {
        localObject4 = (Number)((Iterator)localObject3).next();
        if ((((Number)localObject4).getSource() & 0x1) != 0) {
          localObject1 = ((Number)localObject4).b();
        }
      }
      localArrayList.add(ay.b().a(k.n(a.d())).a((bc)localObject2).a(localal).b(o).c((CharSequence)localObject1).a());
    }
    else
    {
      localObject1 = al.b().b(false).a(false).a(Integer.valueOf(0)).d(Boolean.FALSE).a(Boolean.FALSE).c(Boolean.FALSE).b(Boolean.FALSE).e(Boolean.FALSE).a();
      localArrayList.add(ay.b().a(k.n(a.d())).a(null).a((al)localObject1).b(null).c(null).a());
    }
    locala.a(localArrayList);
    if (k.b(g))
    {
      locala.b(null);
    }
    else
    {
      localObject1 = new ArrayList();
      ((List)localObject1).add(g);
      locala.b((List)localObject1);
    }
    return locala.a();
  }
  
  public static HistoryEvent b(i parami)
  {
    HistoryEvent localHistoryEvent = new HistoryEvent(a);
    h = d;
    f = l;
    a = UUID.randomUUID().toString();
    if ((h == 0) && (parami.a())) {
      p = 2;
    } else if (h == 12785645) {
      p = 1;
    } else {
      p = h;
    }
    if (e)
    {
      if ((i == 3) && (!j))
      {
        o = 3;
        return localHistoryEvent;
      }
      o = 1;
      return localHistoryEvent;
    }
    o = 2;
    return localHistoryEvent;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */