package com.truecaller.callerid;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.app.z.d;
import android.support.v4.app.z.f;
import android.support.v4.app.z.g;
import android.text.format.DateUtils;
import c.g.b.k;
import com.truecaller.calling.notifications.CallingNotificationsBroadcastReceiver;
import com.truecaller.common.h.ab;
import com.truecaller.common.h.am;
import com.truecaller.common.h.j;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.i.c;
import com.truecaller.notifications.a;
import com.truecaller.service.MissedCallsNotificationService;
import com.truecaller.service.Receiver;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.util.bl;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public final class g
  implements e
{
  private final Context a;
  private final c b;
  private final com.truecaller.notificationchannels.e c;
  private final com.truecaller.notificationchannels.b d;
  private final a e;
  
  @Inject
  public g(Context paramContext, c paramc, com.truecaller.notificationchannels.e parame, com.truecaller.notificationchannels.b paramb, a parama)
  {
    a = paramContext;
    b = paramc;
    c = parame;
    d = paramb;
    e = parama;
  }
  
  private void a(List<com.truecaller.old.data.entity.e> paramList, int paramInt1, int paramInt2)
  {
    Object localObject2 = a.getResources();
    Object localObject1 = ((Resources)localObject2).getQuantityString(paramInt2, paramList.size(), new Object[] { Integer.valueOf(paramList.size()) });
    Object localObject3 = new z.d(a, c.a()).a(2131234787);
    C = android.support.v4.content.b.c(a, 2131100594);
    if (paramInt1 != 0) {
      localObject2 = ((Resources)localObject2).getString(paramInt1);
    } else {
      localObject2 = localObject1;
    }
    z.d locald = ((z.d)localObject3).a((CharSequence)localObject2);
    localObject2 = new z.f();
    ((z.f)localObject2).a((CharSequence)localObject1);
    paramInt2 = paramList.size() - 1;
    while (paramInt2 >= 0)
    {
      com.truecaller.old.data.entity.e locale = (com.truecaller.old.data.entity.e)paramList.get(paramInt2);
      Context localContext = a;
      if (DateUtils.isToday(a)) {
        localObject3 = j.f(a, a);
      } else {
        localObject3 = j.e(a, a);
      }
      String str = ab.a(a, b, ab.a(b));
      if (am.a(c)) {
        str = a.getString(2131886766, new Object[] { c, str });
      }
      localObject3 = localContext.getString(2131886768, new Object[] { localObject3, str });
      ((z.f)localObject2).c((CharSequence)localObject3);
      paramInt2 -= 1;
    }
    if (paramList.size() > 5) {
      ((z.f)localObject2).b(a.getString(2131888375, new Object[] { Integer.valueOf(paramList.size() - 5) }));
    }
    if (paramInt1 == 0) {
      localObject1 = a.getString(2131886774);
    }
    locald.b((CharSequence)localObject1);
    locald.a((z.g)localObject2);
    paramList = new Intent(a, Receiver.class);
    paramList.setAction("com.truecaller.intent.action.PHONE_NOTIFICATION_CANCELLED");
    paramList.putExtra("notificationType", 1);
    localObject1 = TruecallerInit.a(a, "calls", "notificationBlockedCall", "openApp");
    locald.b(PendingIntent.getBroadcast(a, 2131364140, paramList, 268435456));
    f = bl.a(a, (Intent)localObject1, 2131364141);
    locald.d(16);
    l = 0;
    locald.c(0);
    e.a("OsNotificationUtils", 222, locald.h(), "notificationBlockedCall");
  }
  
  public final void a()
  {
    Object localObject1 = new z.d(a, c.a()).a(2131234787);
    C = android.support.v4.content.b.c(a, 2131100594);
    localObject1 = ((z.d)localObject1).a(a.getString(2131886306));
    ((z.d)localObject1).d(16);
    ((z.d)localObject1).d(2);
    Object localObject2 = a;
    k.b(localObject2, "context");
    Intent localIntent = new Intent((Context)localObject2, CallingNotificationsBroadcastReceiver.class);
    localIntent.setAction("com.truecaller.request_set_as_default_phone_app");
    localObject2 = PendingIntent.getBroadcast((Context)localObject2, 0, localIntent, 268435456);
    k.a(localObject2, "PendingIntent.getBroadca…tent.FLAG_CANCEL_CURRENT)");
    f = ((PendingIntent)localObject2);
    localObject1 = ((z.d)localObject1).b(a.getString(2131886305)).h();
    e.a(2131365393, (Notification)localObject1, "notificationUnableToBlockCall");
  }
  
  public final void a(i parami)
  {
    ??? = l;
    if (??? != null)
    {
      if (!parami.c()) {
        return;
      }
      if (!b.a("blockCallNotification", true)) {
        return;
      }
      boolean bool;
      if (h == 1) {
        bool = true;
      } else {
        bool = false;
      }
      Object localObject1 = new com.truecaller.old.data.access.g(a);
      Object localObject3 = new com.truecaller.old.data.entity.e(d, a.d(), ((Contact)???).t(), String.valueOf(parami.b()), bool);
      synchronized (com.truecaller.old.data.access.b.a)
      {
        Object localObject4 = ((com.truecaller.old.data.access.b)localObject1).a();
        ((List)localObject4).remove(localObject3);
        ((List)localObject4).add(localObject3);
        ((com.truecaller.old.data.access.b)localObject1).c();
        localObject1 = new ArrayList(((com.truecaller.old.data.access.g)localObject1).f());
        int i;
        int j;
        if (((List)localObject1).size() == 1)
        {
          Contact localContact = l;
          if ((localContact != null) && (b.a("blockCallNotification", true)))
          {
            localObject1 = d.g();
            localObject1 = new z.d(a, (String)localObject1).a(2131234787);
            C = android.support.v4.content.b.c(a, 2131100594);
            localObject3 = ((z.d)localObject1).d(a.getString(2131886117));
            localObject1 = a.n();
            localObject4 = TruecallerInit.a(a, "calls", "notificationBlockedCall");
            ??? = ab.a(a, (String)localObject1, ab.a(a.d()));
            localObject1 = ???;
            if (am.a(localContact.t())) {
              localObject1 = a.getString(2131886766, new Object[] { localContact.t(), ??? });
            }
            i = 2131886779;
            j = 2131234317;
            if (h == 1) {
              i = 2131886777;
            } else {
              j = 2131234331;
            }
            ((z.d)localObject3).a(j);
            ((z.d)localObject3).a(BitmapFactory.decodeResource(a.getResources(), 2131234320));
            ??? = a.getString(i);
            parami = new Intent(a, Receiver.class);
            parami.setAction("com.truecaller.intent.action.PHONE_NOTIFICATION_CANCELLED");
            parami.putExtra("notificationType", 1);
            ((z.d)localObject3).b(PendingIntent.getBroadcast(a, 2131364140, parami, 268435456));
            parami = new StringBuilder("truecaller://");
            parami.append(System.currentTimeMillis());
            ((Intent)localObject4).setData(Uri.parse(parami.toString()));
            parami = (i)localObject1;
            if (!am.a((CharSequence)localObject1)) {
              parami = null;
            }
            parami = ((z.d)localObject3).a((CharSequence)???).b(parami).c(null);
            f = bl.a(a, (Intent)localObject4, 2131364141);
            parami.d(16);
            e.a("OsNotificationUtils", 222);
            e.a("OsNotificationUtils", 222, ((z.d)localObject3).h(), "notificationBlockedCall");
          }
          return;
        }
        if (bool) {
          i = 2131886773;
        } else {
          i = 2131886775;
        }
        if (bool) {
          j = 2131755030;
        } else {
          j = 2131755031;
        }
        a((List)localObject1, i, j);
        return;
      }
    }
  }
  
  public final void b()
  {
    Object localObject1 = new z.d(a, c.a()).a(2131234787);
    C = android.support.v4.content.b.c(a, 2131100594);
    localObject1 = ((z.d)localObject1).a(a.getString(2131886304));
    ((z.d)localObject1).d(16);
    ((z.d)localObject1).d(2);
    Object localObject2 = a;
    k.b(localObject2, "context");
    Intent localIntent = new Intent((Context)localObject2, CallingNotificationsBroadcastReceiver.class);
    localIntent.setAction("com.truecaller.request_allow_draw_over_other_apps");
    localObject2 = PendingIntent.getBroadcast((Context)localObject2, 0, localIntent, 268435456);
    k.a(localObject2, "PendingIntent.getBroadca…tent.FLAG_CANCEL_CURRENT)");
    f = ((PendingIntent)localObject2);
    localObject1 = ((z.d)localObject1).b(a.getString(2131886303)).h();
    e.a(2131362902, (Notification)localObject1, "notificationDrawOverOtherApps");
  }
  
  public final void c()
  {
    Object localObject = new z.d(a, c.a()).a(2131234787);
    C = android.support.v4.content.b.c(a, 2131100594);
    localObject = ((z.d)localObject).a("Call Recording");
    ((z.d)localObject).d(2);
    localObject = ((z.d)localObject).b("In Progress").h();
    e.a(2131362383, (Notification)localObject, "notificationCallRecording");
  }
  
  public final void d()
  {
    e.a(2131362383);
  }
  
  public final void e()
  {
    MissedCallsNotificationService.a(a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */