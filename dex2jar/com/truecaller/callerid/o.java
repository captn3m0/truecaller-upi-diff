package com.truecaller.callerid;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.androidactors.v;

public final class o
  implements n
{
  private final v a;
  
  public o(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return n.class.equals(paramClass);
  }
  
  public final void a()
  {
    a.a(new a(new e(), (byte)0));
  }
  
  public final void a(int paramInt1, String paramString, int paramInt2, int paramInt3)
  {
    a.a(new d(new e(), paramInt1, paramString, paramInt2, paramInt3, (byte)0));
  }
  
  public final void a(i parami)
  {
    a.a(new b(new e(), parami, (byte)0));
  }
  
  public final void b(i parami)
  {
    a.a(new c(new e(), parami, (byte)0));
  }
  
  static final class a
    extends u<n, Void>
  {
    private a(e parame)
    {
      super();
    }
    
    public final String toString()
    {
      return ".onCallerIdWindowClosed()";
    }
  }
  
  static final class b
    extends u<n, Void>
  {
    private final i b;
    
    private b(e parame, i parami)
    {
      super();
      b = parami;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".onCallerIdWindowShown(");
      localStringBuilder.append(a(b, 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class c
    extends u<n, Void>
  {
    private final i b;
    
    private c(e parame, i parami)
    {
      super();
      b = parami;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".onCallerIdWindowUpdated(");
      localStringBuilder.append(a(b, 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class d
    extends u<n, Void>
  {
    private final int b;
    private final String c;
    private final int d;
    private final int e;
    
    private d(e parame, int paramInt1, String paramString, int paramInt2, int paramInt3)
    {
      super();
      b = paramInt1;
      c = paramString;
      d = paramInt2;
      e = paramInt3;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".onStateChanged(");
      localStringBuilder.append(a(Integer.valueOf(b), 2));
      localStringBuilder.append(",");
      localStringBuilder.append(a(c, 1));
      localStringBuilder.append(",");
      localStringBuilder.append(a(Integer.valueOf(d), 2));
      localStringBuilder.append(",");
      localStringBuilder.append(a(Integer.valueOf(e), 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */