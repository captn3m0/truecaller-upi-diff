package com.truecaller.callerid.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.data.entity.Contact;
import com.truecaller.notifications.l.a;
import com.truecaller.util.al;
import com.truecaller.util.at;

final class d$a
  implements l.a
{
  d$a(d paramd, Contact paramContact) {}
  
  public final Bitmap create()
  {
    d locald = a;
    Contact localContact = b;
    Object localObject1 = localContact.E();
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = g;
      k.a(localObject1, "phonebookId");
      localObject1 = ((al)localObject2).a(((Long)localObject1).longValue(), localContact.v(), true);
      if (localObject1 != null) {
        localObject1 = ((Uri)localObject1).toString();
      } else {
        localObject1 = null;
      }
      localObject2 = localObject1;
      if (localObject1 != null) {}
    }
    else
    {
      localObject2 = localContact.v();
    }
    if (localObject2 != null)
    {
      localObject2 = at.a(e, (String)localObject2);
      localObject1 = localObject2;
      if (localObject2 != null) {}
    }
    else
    {
      localObject1 = BitmapFactory.decodeResource(e.getResources(), 2131234316);
      k.a(localObject1, "BitmapFactory.decodeReso…e.ic_notification_avatar)");
    }
    return (Bitmap)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.a.d.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */