package com.truecaller.callerid.a;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.z.d;
import c.o.b;
import c.x;
import com.truecaller.analytics.ae;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.g;
import com.truecaller.notifications.l;
import com.truecaller.notifications.l.a;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.utils.n;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

public final class b
  implements a
{
  final com.truecaller.data.access.m a;
  final com.truecaller.data.access.c b;
  final g c;
  final com.truecaller.utils.a d;
  final d e;
  final com.truecaller.i.c f;
  private final com.truecaller.androidactors.f<ae> g;
  private final com.truecaller.search.b h;
  private final c.d.f i;
  
  @Inject
  public b(com.truecaller.data.access.m paramm, com.truecaller.data.access.c paramc, g paramg, com.truecaller.androidactors.f<ae> paramf, com.truecaller.utils.a parama, com.truecaller.search.b paramb, @Named("IO") c.d.f paramf1, d paramd, com.truecaller.i.c paramc1)
  {
    a = paramm;
    b = paramc;
    c = paramg;
    g = paramf;
    d = parama;
    h = paramb;
    i = paramf1;
    e = paramd;
    f = paramc1;
  }
  
  public final void a(final com.truecaller.old.data.entity.Notification paramNotification)
  {
    c.g.b.k.b(paramNotification, "notification");
    final String str1 = paramNotification.a();
    if (str1 == null) {
      return;
    }
    final String str2 = paramNotification.e();
    if (str2 == null) {
      return;
    }
    final String str3 = paramNotification.f();
    if (str3 == null) {
      return;
    }
    Long localLong = paramNotification.g();
    if (localLong != null)
    {
      long l = localLong.longValue();
      kotlinx.coroutines.e.b((ag)bg.a, i, (c.g.a.m)new a(this, str1, str2, str3, paramNotification, l * 1000L, null), 2);
      return;
    }
  }
  
  @c.d.b.a.f(b="PushCallerIdNotificationHandler.kt", c={}, d="invokeSuspend", e="com.truecaller.callerid.push.PushCallerIdNotificationHandlerImpl$handleNotification$1")
  static final class a
    extends c.d.b.a.k
    implements c.g.a.m<ag, c.d.c<? super x>, Object>
  {
    int a;
    private ag h;
    
    a(b paramb, String paramString1, String paramString2, String paramString3, com.truecaller.old.data.entity.Notification paramNotification, long paramLong, c.d.c paramc)
    {
      super(paramc);
    }
    
    public final c.d.c<x> a(Object paramObject, c.d.c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new a(b, str1, str2, str3, paramNotification, g, paramc);
      h = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      Object localObject1 = c.d.a.a.a;
      if (a == 0)
      {
        if (!(paramObject instanceof o.b))
        {
          localObject1 = b.b.b(str1);
          paramObject = b;
          Object localObject3 = str2;
          Object localObject4 = str3;
          Object localObject5 = str1;
          Object localObject2 = paramNotification.d();
          c.g.b.k.a(localObject2, "notification.notificationId");
          long l = ((Long)localObject2).longValue();
          localObject2 = new Contact();
          Object localObject6 = new StringBuilder();
          ((StringBuilder)localObject6).append((String)localObject3);
          ((StringBuilder)localObject6).append(' ');
          ((StringBuilder)localObject6).append((String)localObject4);
          ((Contact)localObject2).l(((StringBuilder)localObject6).toString());
          ((Contact)localObject2).setSource(64);
          localObject3 = c.a(new String[] { localObject5 });
          if (localObject3 != null) {
            ((Contact)localObject2).a((Number)localObject3);
          }
          ((Contact)localObject2).n(String.valueOf(l));
          ((Contact)localObject2).a(d.a());
          paramObject = b;
          l = g;
          int i;
          if (Math.abs(d.a() - l) <= 20000L) {
            i = 1;
          } else {
            i = 0;
          }
          if ((i != 0) && (f.a("showIncomingCallNotifications", true))) {
            i = 1;
          } else {
            i = 0;
          }
          if (i != 0)
          {
            localObject4 = b.e;
            if (localObject1 == null) {
              paramObject = localObject2;
            } else {
              paramObject = localObject1;
            }
            localObject3 = str1;
            c.g.b.k.b(paramObject, "contact");
            c.g.b.k.b(localObject3, "normalizedNumber");
            if (f.V().a())
            {
              localObject5 = new z.d(e, a.i()).a(2131234787).f(android.support.v4.content.b.c(e, 2131100594)).a((CharSequence)c.a(2131887303, new Object[] { ((Contact)paramObject).t() })).b((CharSequence)c.a(2131887302, new Object[0])).e(2);
              localObject6 = DetailsFragment.a(e, (Contact)paramObject, DetailsFragment.SourceType.Notification, false, true).addFlags(268468224);
              localObject6 = PendingIntent.getActivity(e, 2131364155, (Intent)localObject6, 268435456);
              c.g.b.k.a(localObject6, "PendingIntent.getActivit…tent.FLAG_CANCEL_CURRENT)");
              localObject5 = ((z.d)localObject5).a((PendingIntent)localObject6);
              localObject5 = d.a((z.d)localObject5, (l.a)new d.a((d)localObject4, (Contact)paramObject));
              c.g.b.k.a(localObject5, "notificationIconHelper.c…) { contact.getAvatar() }");
              localObject4 = b;
              localObject6 = new Bundle();
              if (((Contact)paramObject).Z()) {
                paramObject = "pushNotificationPbContact";
              } else {
                paramObject = "pushNotificationNotPbContact";
              }
              ((Bundle)localObject6).putString("Subtype", (String)paramObject);
              ((com.truecaller.notifications.a)localObject4).a((String)localObject3, 2131365509, (android.app.Notification)localObject5, "notificationPushCallerId", (Bundle)localObject6);
            }
          }
          if (b.a(b, (Contact)localObject1))
          {
            b.a(b, String.valueOf(paramNotification.d().longValue()), true);
          }
          else
          {
            b.a(b, String.valueOf(paramNotification.d().longValue()), false);
            b.a.a(str1, new int[] { 64 });
            b.a.a((Contact)localObject2);
          }
          return x.a;
        }
        throw a;
      }
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((a)a(paramObject1, (c.d.c)paramObject2)).a(x.a);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */