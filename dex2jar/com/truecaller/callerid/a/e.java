package com.truecaller.callerid.a;

import android.content.Context;
import com.truecaller.notificationchannels.b;
import com.truecaller.notifications.a;
import com.truecaller.notifications.l;
import com.truecaller.util.al;
import com.truecaller.utils.n;
import javax.inject.Provider;

public final class e
  implements dagger.a.d<d>
{
  private final Provider<b> a;
  private final Provider<a> b;
  private final Provider<n> c;
  private final Provider<l> d;
  private final Provider<Context> e;
  private final Provider<com.truecaller.featuretoggles.e> f;
  private final Provider<al> g;
  
  private e(Provider<b> paramProvider, Provider<a> paramProvider1, Provider<n> paramProvider2, Provider<l> paramProvider3, Provider<Context> paramProvider4, Provider<com.truecaller.featuretoggles.e> paramProvider5, Provider<al> paramProvider6)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
  }
  
  public static e a(Provider<b> paramProvider, Provider<a> paramProvider1, Provider<n> paramProvider2, Provider<l> paramProvider3, Provider<Context> paramProvider4, Provider<com.truecaller.featuretoggles.e> paramProvider5, Provider<al> paramProvider6)
  {
    return new e(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.a.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */