package com.truecaller.callerid.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.data.entity.Contact;
import com.truecaller.featuretoggles.e;
import com.truecaller.notificationchannels.b;
import com.truecaller.notifications.a;
import com.truecaller.notifications.l;
import com.truecaller.notifications.l.a;
import com.truecaller.util.al;
import com.truecaller.util.at;
import com.truecaller.utils.n;
import javax.inject.Inject;

public final class d
{
  final b a;
  public final a b;
  final n c;
  final l d;
  final Context e;
  final e f;
  final al g;
  
  @Inject
  public d(b paramb, a parama, n paramn, l paraml, Context paramContext, e parame, al paramal)
  {
    a = paramb;
    b = parama;
    c = paramn;
    d = paraml;
    e = paramContext;
    f = parame;
    g = paramal;
  }
  
  static final class a
    implements l.a
  {
    a(d paramd, Contact paramContact) {}
    
    public final Bitmap create()
    {
      d locald = a;
      Contact localContact = b;
      Object localObject1 = localContact.E();
      Object localObject2;
      if (localObject1 != null)
      {
        localObject2 = g;
        k.a(localObject1, "phonebookId");
        localObject1 = ((al)localObject2).a(((Long)localObject1).longValue(), localContact.v(), true);
        if (localObject1 != null) {
          localObject1 = ((Uri)localObject1).toString();
        } else {
          localObject1 = null;
        }
        localObject2 = localObject1;
        if (localObject1 != null) {}
      }
      else
      {
        localObject2 = localContact.v();
      }
      if (localObject2 != null)
      {
        localObject2 = at.a(e, (String)localObject2);
        localObject1 = localObject2;
        if (localObject2 != null) {}
      }
      else
      {
        localObject1 = BitmapFactory.decodeResource(e.getResources(), 2131234316);
        k.a(localObject1, "BitmapFactory.decodeReso…e.ic_notification_avatar)");
      }
      return (Bitmap)localObject1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.a.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */