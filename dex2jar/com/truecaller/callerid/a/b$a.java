package com.truecaller.callerid.a;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.z.d;
import c.d.b.a.f;
import c.o.b;
import c.x;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.g;
import com.truecaller.featuretoggles.e;
import com.truecaller.notifications.l;
import com.truecaller.notifications.l.a;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.utils.n;
import kotlinx.coroutines.ag;

@f(b="PushCallerIdNotificationHandler.kt", c={}, d="invokeSuspend", e="com.truecaller.callerid.push.PushCallerIdNotificationHandlerImpl$handleNotification$1")
final class b$a
  extends c.d.b.a.k
  implements c.g.a.m<ag, c.d.c<? super x>, Object>
{
  int a;
  private ag h;
  
  b$a(b paramb, String paramString1, String paramString2, String paramString3, com.truecaller.old.data.entity.Notification paramNotification, long paramLong, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c<x> a(Object paramObject, c.d.c<?> paramc)
  {
    c.g.b.k.b(paramc, "completion");
    paramc = new a(b, c, d, e, f, g, paramc);
    h = ((ag)paramObject);
    return paramc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    if (a == 0)
    {
      if (!(paramObject instanceof o.b))
      {
        localObject1 = b.b.b(c);
        paramObject = b;
        Object localObject3 = d;
        Object localObject4 = e;
        Object localObject5 = c;
        Object localObject2 = f.d();
        c.g.b.k.a(localObject2, "notification.notificationId");
        long l = ((Long)localObject2).longValue();
        localObject2 = new Contact();
        Object localObject6 = new StringBuilder();
        ((StringBuilder)localObject6).append((String)localObject3);
        ((StringBuilder)localObject6).append(' ');
        ((StringBuilder)localObject6).append((String)localObject4);
        ((Contact)localObject2).l(((StringBuilder)localObject6).toString());
        ((Contact)localObject2).setSource(64);
        localObject3 = c.a(new String[] { localObject5 });
        if (localObject3 != null) {
          ((Contact)localObject2).a((Number)localObject3);
        }
        ((Contact)localObject2).n(String.valueOf(l));
        ((Contact)localObject2).a(d.a());
        paramObject = b;
        l = g;
        int i;
        if (Math.abs(d.a() - l) <= 20000L) {
          i = 1;
        } else {
          i = 0;
        }
        if ((i != 0) && (f.a("showIncomingCallNotifications", true))) {
          i = 1;
        } else {
          i = 0;
        }
        if (i != 0)
        {
          localObject4 = b.e;
          if (localObject1 == null) {
            paramObject = localObject2;
          } else {
            paramObject = localObject1;
          }
          localObject3 = c;
          c.g.b.k.b(paramObject, "contact");
          c.g.b.k.b(localObject3, "normalizedNumber");
          if (f.V().a())
          {
            localObject5 = new z.d(e, a.i()).a(2131234787).f(android.support.v4.content.b.c(e, 2131100594)).a((CharSequence)c.a(2131887303, new Object[] { ((Contact)paramObject).t() })).b((CharSequence)c.a(2131887302, new Object[0])).e(2);
            localObject6 = DetailsFragment.a(e, (Contact)paramObject, DetailsFragment.SourceType.Notification, false, true).addFlags(268468224);
            localObject6 = PendingIntent.getActivity(e, 2131364155, (Intent)localObject6, 268435456);
            c.g.b.k.a(localObject6, "PendingIntent.getActivit…tent.FLAG_CANCEL_CURRENT)");
            localObject5 = ((z.d)localObject5).a((PendingIntent)localObject6);
            localObject5 = d.a((z.d)localObject5, (l.a)new d.a((d)localObject4, (Contact)paramObject));
            c.g.b.k.a(localObject5, "notificationIconHelper.c…) { contact.getAvatar() }");
            localObject4 = b;
            localObject6 = new Bundle();
            if (((Contact)paramObject).Z()) {
              paramObject = "pushNotificationPbContact";
            } else {
              paramObject = "pushNotificationNotPbContact";
            }
            ((Bundle)localObject6).putString("Subtype", (String)paramObject);
            ((com.truecaller.notifications.a)localObject4).a((String)localObject3, 2131365509, (android.app.Notification)localObject5, "notificationPushCallerId", (Bundle)localObject6);
          }
        }
        if (b.a(b, (Contact)localObject1))
        {
          b.a(b, String.valueOf(f.d().longValue()), true);
        }
        else
        {
          b.a(b, String.valueOf(f.d().longValue()), false);
          b.a.a(c, new int[] { 64 });
          b.a.a((Contact)localObject2);
        }
        return x.a;
      }
      throw a;
    }
    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    return ((a)a(paramObject1, (c.d.c)paramObject2)).a(x.a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.a.b.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */