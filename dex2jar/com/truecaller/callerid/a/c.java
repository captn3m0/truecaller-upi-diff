package com.truecaller.callerid.a;

import com.truecaller.analytics.ae;
import com.truecaller.data.access.m;
import com.truecaller.data.entity.g;
import com.truecaller.utils.a;
import javax.inject.Provider;

public final class c
  implements dagger.a.d<b>
{
  private final Provider<m> a;
  private final Provider<com.truecaller.data.access.c> b;
  private final Provider<g> c;
  private final Provider<com.truecaller.androidactors.f<ae>> d;
  private final Provider<a> e;
  private final Provider<com.truecaller.search.b> f;
  private final Provider<c.d.f> g;
  private final Provider<d> h;
  private final Provider<com.truecaller.i.c> i;
  
  private c(Provider<m> paramProvider, Provider<com.truecaller.data.access.c> paramProvider1, Provider<g> paramProvider2, Provider<com.truecaller.androidactors.f<ae>> paramProvider3, Provider<a> paramProvider4, Provider<com.truecaller.search.b> paramProvider5, Provider<c.d.f> paramProvider6, Provider<d> paramProvider7, Provider<com.truecaller.i.c> paramProvider8)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
  }
  
  public static c a(Provider<m> paramProvider, Provider<com.truecaller.data.access.c> paramProvider1, Provider<g> paramProvider2, Provider<com.truecaller.androidactors.f<ae>> paramProvider3, Provider<a> paramProvider4, Provider<com.truecaller.search.b> paramProvider5, Provider<c.d.f> paramProvider6, Provider<d> paramProvider7, Provider<com.truecaller.i.c> paramProvider8)
  {
    return new c(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */