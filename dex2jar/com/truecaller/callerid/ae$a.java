package com.truecaller.callerid;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.network.search.j;

final class ae$a
  extends u<ad, Contact>
{
  private final Number b;
  private final boolean c;
  private final int d;
  private final j e;
  
  private ae$a(e parame, Number paramNumber, boolean paramBoolean, int paramInt, j paramj)
  {
    super(parame);
    b = paramNumber;
    c = paramBoolean;
    d = paramInt;
    e = paramj;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".performSearch(");
    localStringBuilder.append(a(b, 1));
    localStringBuilder.append(",");
    localStringBuilder.append(a(Boolean.valueOf(c), 2));
    localStringBuilder.append(",");
    localStringBuilder.append(a(Integer.valueOf(d), 2));
    localStringBuilder.append(",");
    localStringBuilder.append(a(e, 1));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.ae.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */