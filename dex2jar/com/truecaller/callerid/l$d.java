package com.truecaller.callerid;

import com.truecaller.aftercall.PromotionType;
import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.i.c;

final class l$d
  extends u<k, Void>
{
  private final PromotionType b;
  private final HistoryEvent c;
  private final c d;
  
  private l$d(e parame, PromotionType paramPromotionType, HistoryEvent paramHistoryEvent, c paramc)
  {
    super(parame);
    b = paramPromotionType;
    c = paramHistoryEvent;
    d = paramc;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".showAfterCallPromo(");
    localStringBuilder.append(a(b, 2));
    localStringBuilder.append(",");
    localStringBuilder.append(a(c, 1));
    localStringBuilder.append(",");
    localStringBuilder.append(a(d, 2));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.l.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */