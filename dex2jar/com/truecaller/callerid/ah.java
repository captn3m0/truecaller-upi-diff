package com.truecaller.callerid;

import com.truecaller.analytics.ae;
import com.truecaller.androidactors.f;
import com.truecaller.calling.recorder.h;
import com.truecaller.common.h.ab;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.filters.FilterManager;
import com.truecaller.filters.FilterManager.FilterAction;
import com.truecaller.filters.g;
import com.truecaller.i.c;
import com.truecaller.log.AssertionUtil;
import com.truecaller.tracking.events.k;
import com.truecaller.tracking.events.k.a;
import com.truecaller.util.al;
import com.truecaller.util.cn;
import com.truecaller.util.u;
import org.apache.a.a;

final class ah
  implements ag
{
  private final c a;
  private final u b;
  private final al c;
  private final cn d;
  private final FilterManager e;
  private final f<ae> f;
  private final h g;
  
  ah(c paramc, u paramu, al paramal, cn paramcn, FilterManager paramFilterManager, f<ae> paramf, h paramh)
  {
    a = paramc;
    b = paramu;
    c = paramal;
    d = paramcn;
    e = paramFilterManager;
    f = paramf;
    g = paramh;
  }
  
  private boolean c(HistoryEvent paramHistoryEvent)
  {
    int i;
    if (o == 3) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0)
    {
      if (!a.a("showAfterCallForPBContacts", false)) {
        try
        {
          ((ae)f.a()).a(k.b().a("SettingChanged").b("ACSPhonebookContacts").a());
          a.b("showAfterCallForPBContacts", true);
          return true;
        }
        catch (a paramHistoryEvent)
        {
          AssertionUtil.reportThrowableButNeverCrash(paramHistoryEvent);
        }
      }
      return true;
    }
    return false;
  }
  
  public final boolean a(HistoryEvent paramHistoryEvent)
  {
    return (f != null) && (a.b("afterCall")) && (ab.e(b)) && ((!b.a(c)) || (a.b("enabledCallerIDforPB")) || (c(paramHistoryEvent))) && (c.a()) && (!d.b()) && (e.a(c, b, d, true).h != FilterManager.FilterAction.FILTER_BLACKLISTED);
  }
  
  public final boolean b(HistoryEvent paramHistoryEvent)
  {
    return (f != null) && (!ab.a(f.p())) && (c.a()) && (!d.b()) && (g.a()) && (m != null);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.ah
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */