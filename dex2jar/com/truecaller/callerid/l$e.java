package com.truecaller.callerid;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.HistoryEvent;

final class l$e
  extends u<k, Void>
{
  private final HistoryEvent b;
  private final int c;
  
  private l$e(e parame, HistoryEvent paramHistoryEvent, int paramInt)
  {
    super(parame);
    b = paramHistoryEvent;
    c = paramInt;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".showRegularAfterCallScreen(");
    localStringBuilder.append(a(b, 1));
    localStringBuilder.append(",");
    localStringBuilder.append(a(Integer.valueOf(c), 2));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.l.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */