package com.truecaller.callerid;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import dagger.a.d;
import javax.inject.Provider;

public final class s
  implements d<f<n>>
{
  private final q a;
  private final Provider<i> b;
  private final Provider<n> c;
  
  private s(q paramq, Provider<i> paramProvider, Provider<n> paramProvider1)
  {
    a = paramq;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static s a(q paramq, Provider<i> paramProvider, Provider<n> paramProvider1)
  {
    return new s(paramq, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.s
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */