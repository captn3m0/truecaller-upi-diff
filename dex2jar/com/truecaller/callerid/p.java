package com.truecaller.callerid;

import android.text.TextUtils;
import com.google.common.base.Strings;
import com.truecaller.ads.j.a;
import com.truecaller.analytics.ae;
import com.truecaller.androidactors.w;
import com.truecaller.calling.dialer.v;
import com.truecaller.calling.dialer.v.a;
import com.truecaller.calling.recorder.CallRecordingManager;
import com.truecaller.common.account.r;
import com.truecaller.common.h.ab;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.Tag;
import com.truecaller.filters.FilterManager;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.UnmutedException.k;
import com.truecaller.tracking.events.bc;
import com.truecaller.tracking.events.bc.a;
import com.truecaller.tracking.events.g.a;
import com.truecaller.tracking.events.i.a;
import com.truecaller.util.ce;
import com.truecaller.util.u;
import com.truecaller.utils.d;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

final class p
  implements n
{
  private final com.truecaller.service.e A;
  private final com.truecaller.data.entity.g B;
  private al C;
  private final boolean D;
  private boolean E = true;
  private final com.truecaller.ads.provider.f F;
  private final com.truecaller.androidactors.f<com.truecaller.ads.provider.a.b> G;
  private final com.truecaller.androidactors.i H;
  private final CallRecordingManager I;
  private com.truecaller.androidactors.a J = null;
  private com.truecaller.androidactors.f<com.truecaller.callhistory.a> K;
  private com.truecaller.utils.l L;
  private com.truecaller.featuretoggles.e M;
  private com.truecaller.b.c N;
  private com.truecaller.androidactors.f<com.truecaller.calling.recorder.floatingbutton.e> O;
  private com.truecaller.utils.a P;
  private com.truecaller.search.b Q;
  private i.a R = com.truecaller.tracking.events.i.b();
  private v S;
  private long T;
  i a;
  private final com.truecaller.androidactors.f<k> b;
  private final com.truecaller.androidactors.i c;
  private final d d;
  private final com.truecaller.util.al e;
  private final a f;
  private final c g;
  private final com.truecaller.network.search.l h;
  private final com.truecaller.tcpermissions.l i;
  private final com.truecaller.common.h.aj j;
  private final com.truecaller.androidactors.f<com.truecaller.h.c> k;
  private final u l;
  private final com.truecaller.data.access.c m;
  private final com.truecaller.androidactors.f<com.truecaller.util.aa> n;
  private final com.truecaller.androidactors.f<e> o;
  private final com.truecaller.androidactors.f<com.truecaller.network.util.c> p;
  private final com.truecaller.androidactors.f<ae> q;
  private final com.truecaller.androidactors.f<ad> r;
  private final aj s;
  private final FilterManager t;
  private final com.truecaller.analytics.b u;
  private final com.truecaller.common.g.a v;
  private final com.truecaller.i.c w;
  private final com.truecaller.aftercall.a x;
  private final ag y;
  private final r z;
  
  p(boolean paramBoolean, com.truecaller.androidactors.f<k> paramf, com.truecaller.androidactors.i parami1, d paramd, com.truecaller.util.al paramal, a parama, c paramc, com.truecaller.network.search.l paraml, com.truecaller.common.h.aj paramaj, com.truecaller.androidactors.f<com.truecaller.h.c> paramf1, u paramu, com.truecaller.data.access.c paramc1, com.truecaller.androidactors.f<com.truecaller.util.aa> paramf2, com.truecaller.androidactors.f<e> paramf3, com.truecaller.androidactors.f<com.truecaller.network.util.c> paramf4, com.truecaller.androidactors.f<ae> paramf5, com.truecaller.androidactors.f<ad> paramf6, aj paramaj1, FilterManager paramFilterManager, com.truecaller.analytics.b paramb, com.truecaller.common.g.a parama1, com.truecaller.i.c paramc2, com.truecaller.aftercall.a parama2, ag paramag, r paramr, com.truecaller.service.e parame, com.truecaller.data.entity.g paramg, al paramal1, com.truecaller.ads.provider.f paramf7, com.truecaller.androidactors.f<com.truecaller.ads.provider.a.b> paramf8, com.truecaller.androidactors.i parami2, CallRecordingManager paramCallRecordingManager, com.truecaller.androidactors.f<com.truecaller.callhistory.a> paramf9, com.truecaller.tcpermissions.l paraml1, com.truecaller.utils.l paraml2, com.truecaller.featuretoggles.e parame1, com.truecaller.b.c paramc3, com.truecaller.utils.a parama3, com.truecaller.search.b paramb1, v paramv, com.truecaller.androidactors.f<com.truecaller.calling.recorder.floatingbutton.e> paramf10)
  {
    D = paramBoolean;
    b = paramf;
    c = parami1;
    d = paramd;
    e = paramal;
    f = parama;
    g = paramc;
    h = paraml;
    j = paramaj;
    k = paramf1;
    l = paramu;
    m = paramc1;
    o = paramf3;
    p = paramf4;
    n = paramf2;
    q = paramf5;
    s = paramaj1;
    t = paramFilterManager;
    u = paramb;
    v = parama1;
    w = paramc2;
    x = parama2;
    y = paramag;
    z = paramr;
    A = parame;
    B = paramg;
    C = paramal1;
    r = paramf6;
    F = paramf7;
    G = paramf8;
    H = parami2;
    I = paramCallRecordingManager;
    K = paramf9;
    i = paraml1;
    L = paraml2;
    M = parame1;
    N = paramc3;
    O = paramf10;
    P = parama3;
    Q = paramb1;
    S = paramv;
  }
  
  private Contact a(Number paramNumber, com.truecaller.filters.g paramg, long paramLong)
  {
    Contact localContact = m.b(paramNumber.a());
    if (localContact != null) {
      return localContact;
    }
    paramNumber.a(m);
    localContact = new Contact();
    localContact.l(k);
    localContact.a(paramNumber);
    localContact.a(paramLong);
    d = true;
    return localContact;
  }
  
  private static List<String> a(Contact paramContact)
  {
    paramContact = paramContact.J();
    if (!paramContact.isEmpty())
    {
      ArrayList localArrayList = new ArrayList(paramContact.size());
      Iterator localIterator = paramContact.iterator();
      for (;;)
      {
        paramContact = localArrayList;
        if (!localIterator.hasNext()) {
          break;
        }
        paramContact = ((Tag)localIterator.next()).a();
        if (!TextUtils.isEmpty(paramContact)) {
          localArrayList.add(paramContact);
        }
      }
    }
    paramContact = null;
    return paramContact;
  }
  
  private void a(android.support.v4.f.j<Boolean, String> paramj)
  {
    if ((paramj != null) && (Boolean.TRUE.equals(a)))
    {
      if ("initiated".equals((String)b)) {
        paramj = "CALLERID_Push_Sent";
      } else {
        paramj = "AFTERCALL_Push_Sent";
      }
      u.a(new com.truecaller.analytics.e.a(paramj).a());
    }
  }
  
  private void a(i parami, boolean paramBoolean)
  {
    if (D)
    {
      if (L.a())
      {
        ((k)b.a()).a(parami, paramBoolean);
        return;
      }
      ((e)o.a()).b();
    }
  }
  
  private void a(Contact paramContact, HistoryEvent paramHistoryEvent)
  {
    if ((y.a(paramHistoryEvent)) && (v.a("featureCacheAdAfterCall", false)) && (f())) {
      b(paramContact, paramHistoryEvent);
    }
  }
  
  private void a(Boolean paramBoolean)
  {
    com.truecaller.calling.recorder.floatingbutton.e locale = (com.truecaller.calling.recorder.floatingbutton.e)O.a();
    boolean bool;
    if ((paramBoolean != null) && (!paramBoolean.booleanValue())) {
      bool = true;
    } else {
      bool = false;
    }
    locale.a(bool);
  }
  
  private boolean a(HistoryEvent paramHistoryEvent)
  {
    return (!d.e()) || (o != 2);
  }
  
  private boolean a(Number paramNumber, boolean paramBoolean1, boolean paramBoolean2)
  {
    if (paramNumber != null)
    {
      int i1;
      if ((ab.a(paramNumber.d())) && (ab.a(paramNumber.a())) && (ab.a(paramNumber.c()))) {
        i1 = 1;
      } else {
        i1 = 0;
      }
      if (i1 != 0) {
        return false;
      }
      if (!w.b("hasNativeDialerCallerId"))
      {
        if (!D) {
          return false;
        }
        if (!paramBoolean1) {
          return true;
        }
        if (paramBoolean2)
        {
          paramNumber = M;
          if (o.a(paramNumber, com.truecaller.featuretoggles.e.a[42]).a()) {
            break label130;
          }
        }
        label130:
        return (w.b("enabledCallerIDforPB")) && (paramBoolean1);
      }
      return false;
    }
    return false;
  }
  
  private void b()
  {
    i locali = a;
    if ((locali != null) && (i == 0) && (!a.j))
    {
      a.i = 3;
      c();
    }
  }
  
  private void b(Contact paramContact, HistoryEvent paramHistoryEvent)
  {
    e();
    j.a locala = new j.a("AFTERCALL");
    a = b;
    b = paramContact.I();
    d = Integer.valueOf(paramHistoryEvent.g());
    e = paramContact.t();
    f = d;
    g = a(paramContact);
    paramContact = locala.a();
    J = ((com.truecaller.ads.provider.a.b)G.a()).a(paramContact).a(H, new -..Lambda.p.eNW91e7pFyz5PLvitLsXov_fQdI(this, paramContact));
  }
  
  private void c()
  {
    i locali = a;
    if ((locali == null) || (i == 3))
    {
      E = false;
      ((k)b.a()).a();
    }
  }
  
  private void c(i parami)
  {
    if (!e.a()) {
      return;
    }
    String str;
    if (i == 0) {
      str = "initiated";
    } else {
      str = "ended";
    }
    ((com.truecaller.network.util.c)p.a()).a(str, a).a(c, new -..Lambda.p.A5bhd-4eFfyF2qtl_1WWQgiz3TQ(this));
  }
  
  private void d()
  {
    ((k)b.a()).b();
    if (I.a()) {
      ((com.truecaller.calling.recorder.floatingbutton.e)O.a()).a(true);
    }
  }
  
  private void d(i parami)
  {
    Contact localContact = l;
    Object localObject = l;
    int i1 = 0;
    AssertionUtil.isNotNull(localObject, new String[0]);
    AssertionUtil.isNotNull(a, new String[0]);
    localObject = a;
    M.A().a();
    boolean bool1 = l.a((Number)localObject);
    if (!a((Number)localObject, bool1, l.a(32)))
    {
      if (bool1)
      {
        o = "inPhonebook";
        return;
      }
      o = "notNumber";
      return;
    }
    bool1 = localContact.p(((Number)localObject).a());
    if ((localContact.getSource() & 0x40) != 0) {
      i1 = 1;
    }
    boolean bool2 = Q.a(localContact.H(), localContact.getSource());
    if (((!bool1) && (i1 == 0)) || (bool2))
    {
      e(parami);
    }
    else
    {
      localObject = new com.truecaller.analytics.e.a("CallerIdSearch");
      ((com.truecaller.analytics.e.a)localObject).a("Result", "Cache");
      u.a(((com.truecaller.analytics.e.a)localObject).a());
      o = "validCacheResult";
      a(localContact, j.b(parami));
    }
    a(parami, true);
    if (D)
    {
      if (a.c != c)
      {
        l1 = 6L;
        break label277;
      }
      if (!e)
      {
        l1 = 10L;
        break label277;
      }
    }
    long l1 = 0L;
    label277:
    if (l1 != 0L) {
      s.a(l1, TimeUnit.SECONDS, new -..Lambda.p.BNVa7om4scb3DVa_276vzMAE7NU(this));
    }
  }
  
  private void e()
  {
    com.truecaller.androidactors.a locala = J;
    if (locala != null)
    {
      locala.a();
      J = null;
    }
  }
  
  private void e(i parami)
  {
    if ((E) && (D))
    {
      if (!e.a()) {
        return;
      }
      Number localNumber = a;
      int i1 = parami.b();
      o = null;
      k = true;
      n = true;
      ((ad)r.a()).a(localNumber, false, i1, h.a(UUID.randomUUID(), "callerId")).a(c, new -..Lambda.p.oMsaQ6Cu5SUCspybc1y_uPydpH4(this, parami));
      return;
    }
  }
  
  private boolean f()
  {
    return (!M.J().a()) || (!d.r());
  }
  
  public final void a()
  {
    E = false;
    e();
    c();
    if (I.a()) {
      ((com.truecaller.calling.recorder.floatingbutton.e)O.a()).a(true);
    }
    S.a(null);
    if (R.a())
    {
      if (!R.b()) {
        R.b(-1L);
      }
      ((ae)q.a()).a(R.c());
    }
  }
  
  public final void a(int paramInt1, String paramString, int paramInt2, int paramInt3)
  {
    boolean bool1 = e.a();
    boolean bool2 = false;
    int i1;
    if (((bool1) || (!e.b())) && (!j.a(paramString)) && (i.f())) {
      i1 = 1;
    } else {
      i1 = 0;
    }
    if (i1 == 0)
    {
      c();
      return;
    }
    switch (paramInt1)
    {
    default: 
      return;
    case 3: 
      paramString = a;
      if ((paramString != null) && (i != 3))
      {
        d();
        a.i = 3;
        ((com.truecaller.h.c)k.a()).b().c();
        localObject3 = j.b(a);
        if ((!I.j()) && (I.a()))
        {
          if (I.f()) {
            I.e();
          }
          ((com.truecaller.calling.recorder.floatingbutton.e)O.a()).a();
          if (a((HistoryEvent)localObject3))
          {
            paramString = I.b();
            if ((!org.c.a.a.a.k.b(paramString)) && ((a == null) || (!I.i())))
            {
              paramString = new CallRecording(-1L, a, paramString);
              m = paramString;
              ((com.truecaller.callhistory.a)K.a()).a(paramString);
              I.h();
              localObject1 = new com.truecaller.analytics.e.a("CallRecorded");
              switch (o)
              {
              default: 
                paramString = "Unknown";
                break;
              case 3: 
                paramString = "Missed";
                break;
              case 2: 
                paramString = "Outgoing";
                break;
              case 1: 
                paramString = "Incoming";
              }
              ((com.truecaller.analytics.e.a)localObject1).a("CallType", paramString);
              paramString = f;
              if (paramString != null)
              {
                if (paramString.U())
                {
                  paramString = "Spam";
                  break label449;
                }
                if (paramString.Z())
                {
                  paramString = "PhoneBook";
                  break label449;
                }
              }
              paramString = "Unknown";
              ((com.truecaller.analytics.e.a)localObject1).a("NumberType", paramString);
              I.a((com.truecaller.analytics.e.a)localObject1);
            }
          }
        }
        if (a((HistoryEvent)localObject3)) {
          ((com.truecaller.util.aa)n.a()).a((HistoryEvent)localObject3);
        }
        paramString = a;
        if (paramString.c())
        {
          w.a_("blockCallCounter");
          g.a(paramString);
        }
        f.a(a);
        if (!a.e) {
          c(a);
        }
        boolean bool3 = a.f;
        if (f())
        {
          if (a != null) {
            bool1 = true;
          } else {
            bool1 = false;
          }
          AssertionUtil.isTrue(bool1, new String[0]);
          paramString = x.a(a, (HistoryEvent)localObject3);
          if (paramString != null) {
            ((k)b.a()).a(paramString, (HistoryEvent)localObject3, w);
          } else if ((y.b((HistoryEvent)localObject3)) || ((y.a((HistoryEvent)localObject3)) && (!bool3))) {
            ((k)b.a()).a((HistoryEvent)localObject3, 0);
          }
        }
        localObject2 = a;
        bool3 = l.a(a);
        Object localObject5;
        if (D)
        {
          localObject4 = new com.truecaller.analytics.e.a("Call");
          if (e) {
            paramString = "Incoming";
          } else {
            paramString = "Outgoing";
          }
          ((com.truecaller.analytics.e.a)localObject4).a("Direction", paramString);
          if (e)
          {
            if (((i)localObject2).c()) {
              paramString = "Blocked";
            } else if (j) {
              paramString = "Answered";
            } else {
              paramString = "NotAnswered";
            }
            ((com.truecaller.analytics.e.a)localObject4).a("Action", paramString);
          }
          else
          {
            if (f) {
              paramString = w.b("key_last_call_origin", "");
            } else {
              paramString = "outsideTC";
            }
            AssertionUtil.isFalse(Strings.isNullOrEmpty(paramString), new String[] { "Call event analytics context shouldn't be empty" });
            ((com.truecaller.analytics.e.a)localObject4).a("Context", paramString);
          }
          paramString = t.b(a.d());
          switch (1.b[j.ordinal()])
          {
          default: 
            break;
          case 7: 
            switch (1.a[n.ordinal()])
            {
            default: 
              break;
            case 4: 
              ((com.truecaller.analytics.e.a)localObject4).a("BlockingSource", "FilterExplicit");
              break;
            case 3: 
              ((com.truecaller.analytics.e.a)localObject4).a("BlockingSource", "FilterEnd");
              break;
            case 2: 
              ((com.truecaller.analytics.e.a)localObject4).a("BlockingSource", "FilterStart");
              break;
            case 1: 
              ((com.truecaller.analytics.e.a)localObject4).a("BlockingSource", "FilterContains");
            }
            break;
          case 6: 
            ((com.truecaller.analytics.e.a)localObject4).a("BlockingSource", "IndianRegisteredTelemarketer");
            break;
          case 5: 
            ((com.truecaller.analytics.e.a)localObject4).a("BlockingSource", "NeighbourSpoofing");
            break;
          case 4: 
            ((com.truecaller.analytics.e.a)localObject4).a("BlockingSource", "HiddenNumber");
            break;
          case 3: 
            ((com.truecaller.analytics.e.a)localObject4).a("BlockingSource", "ForeignNumber");
            break;
          case 2: 
            ((com.truecaller.analytics.e.a)localObject4).a("BlockingSource", "NonPhonebookContact");
            break;
          case 1: 
            ((com.truecaller.analytics.e.a)localObject4).a("BlockingSource", "TopSpammer");
          }
          if (bool3) {
            paramString = "Phonebook";
          } else {
            paramString = "Unknown";
          }
          localObject5 = l;
          localObject1 = paramString;
          if (localObject5 != null)
          {
            localObject1 = paramString;
            if (paramString.equals("Unknown"))
            {
              localObject1 = ((Contact)localObject5).A().iterator();
              while (((Iterator)localObject1).hasNext()) {
                if (!Strings.isNullOrEmpty(((Number)((Iterator)localObject1).next()).a()))
                {
                  paramInt1 = 1;
                  break label1246;
                }
              }
              paramInt1 = 0;
              localObject1 = paramString;
              if (paramInt1 == 0) {
                localObject1 = "Hidden";
              }
            }
          }
          ((com.truecaller.analytics.e.a)localObject4).a("Participant", (String)localObject1);
          u.a(((com.truecaller.analytics.e.a)localObject4).a());
        }
        w.d("key_last_call_origin");
        boolean bool4 = y.a((HistoryEvent)localObject3);
        Object localObject4 = t;
        long l1 = T;
        paramString = a;
        if ((l != null) && (l.a(32))) {
          bool1 = true;
        } else {
          bool1 = false;
        }
        boolean bool5 = a(paramString, bool3, bool1);
        localObject3 = com.truecaller.tracking.events.e.b();
        ((com.truecaller.tracking.events.e.a)localObject3).a(com.truecaller.common.h.aa.c(org.c.a.a.a.k.n(a.a()), null));
        if (e) {
          paramString = "incoming";
        } else {
          paramString = "outgoing";
        }
        ((com.truecaller.tracking.events.e.a)localObject3).c(paramString);
        ((com.truecaller.tracking.events.e.a)localObject3).g(null);
        ((com.truecaller.tracking.events.e.a)localObject3).a(null);
        localObject1 = a.l();
        paramString = (String)localObject1;
        if (TextUtils.isEmpty((CharSequence)localObject1)) {
          paramString = "unknown";
        }
        ((com.truecaller.tracking.events.e.a)localObject3).b(paramString);
        if (e)
        {
          if (j) {
            paramString = "answered";
          } else {
            paramString = "notAnswered";
          }
          ((com.truecaller.tracking.events.e.a)localObject3).d(paramString);
        }
        else
        {
          ((com.truecaller.tracking.events.e.a)localObject3).d("unknown");
        }
        ((com.truecaller.tracking.events.e.a)localObject3).a(bool4);
        ((com.truecaller.tracking.events.e.a)localObject3).a(System.currentTimeMillis() - d);
        if (bool3) {
          paramString = "fromPhonebook";
        } else {
          paramString = "fromServer";
        }
        localObject1 = ((FilterManager)localObject4).a(a.d());
        switch (j.1.a[j.ordinal()])
        {
        default: 
          if ((l != null) && (!l.P()))
          {
            paramString = "noHit";
            paramInt1 = 0;
          }
          break;
        case 3: 
          paramString = "fromUserSpammerList";
          paramInt1 = 1;
          break;
        case 2: 
          paramString = "fromUserWhiteList";
          paramInt1 = 0;
          break;
        case 1: 
          paramString = "fromTopSpammerList";
          paramInt1 = 1;
          break;
        }
        paramInt1 = 0;
        ((com.truecaller.tracking.events.e.a)localObject3).e(paramString);
        if (paramInt1 == 0)
        {
          bool1 = bool2;
          if (!((i)localObject2).a()) {}
        }
        else
        {
          bool1 = true;
        }
        ((com.truecaller.tracking.events.e.a)localObject3).b(bool1);
        if (h == 1) {
          ((com.truecaller.tracking.events.e.a)localObject3).f("autoBlock");
        } else if (h == 3) {
          ((com.truecaller.tracking.events.e.a)localObject3).f("silentRing");
        } else {
          ((com.truecaller.tracking.events.e.a)localObject3).f("none");
        }
        ((com.truecaller.tracking.events.e.a)localObject3).b(l1);
        if (l != null)
        {
          paramString = bc.b();
          localObject1 = new ArrayList();
          localObject4 = new ArrayList();
          localObject5 = l.J();
          Iterator localIterator = ((List)localObject5).iterator();
          while (localIterator.hasNext())
          {
            Tag localTag = (Tag)localIterator.next();
            if ((localTag.getSource() & 0x10) != 0) {
              ((List)localObject1).add(localTag.a());
            } else {
              ((List)localObject4).add(localTag.a());
            }
          }
          paramString.a((List)localObject4);
          paramString.b((List)localObject1);
          if (bool5)
          {
            localObject1 = ce.a((List)localObject5);
            if (localObject1 != null)
            {
              localObject4 = new ArrayList();
              ((List)localObject4).add(String.valueOf(a));
              paramString.c((List)localObject4);
            }
          }
          ((com.truecaller.tracking.events.e.a)localObject3).a(paramString.a());
        }
        ((com.truecaller.tracking.events.e.a)localObject3).g(g);
        paramString = ((com.truecaller.tracking.events.e.a)localObject3).a();
        try
        {
          ((ae)q.a()).a(paramString);
        }
        catch (org.apache.a.a paramString)
        {
          AssertionUtil.reportThrowableButNeverCrash(paramString);
        }
        T = 0L;
        paramString = j.a((i)localObject2);
        if (paramString != null) {
          ((ae)q.a()).a(paramString);
        }
        c();
        ((e)o.a()).e();
        return;
      }
      c();
      return;
    case 2: 
      localObject1 = a;
      if (localObject1 != null)
      {
        if (i != 3) {
          a.j = true;
        }
        if (a.i == 1)
        {
          a.i = 2;
          if (d.e())
          {
            a = new i(a.i, a.h, a.a, a.b, a.f, P.a(), a.l, a.g, a.m);
            a.j = true;
          }
          d();
        }
        localObject1 = a.a.a();
        if ((I.a()) && (I.d())) {
          ((com.truecaller.calling.recorder.floatingbutton.e)O.a()).a((String)localObject1, new -..Lambda.p.rzcMCX1b61meQ6Afzht3FWjGYOM(this, (String)localObject1));
        }
      }
      if ((paramString != null) && (org.c.a.a.a.c.a(paramString, "#", 0) >= 0)) {
        paramInt1 = 1;
      } else {
        paramInt1 = 0;
      }
      if ((paramInt1 != 0) && (I.a()))
      {
        localObject1 = B.b(new String[] { paramString });
        ((com.truecaller.calling.recorder.floatingbutton.e)O.a()).a(paramString, new -..Lambda.p.04RAVmm4LF92OwVGrr8mIrWQ-sg(this, (Number)localObject1, paramString));
      }
      return;
    case 1: 
      label449:
      label1246:
      localObject1 = B.b(new String[] { paramString });
      paramString = t.a(paramString);
      localObject2 = a((Number)localObject1, paramString, P.a());
      paramString = new i(1, paramInt3, (Number)localObject1, paramInt2, false, P.a(), (Contact)localObject2, ai.b(), paramString);
      localObject2 = a;
      if ((localObject2 != null) && (i != 3))
      {
        if (paramInt3 != 1)
        {
          if (paramInt3 == 12785645) {
            return;
          }
          d(paramString);
          return;
        }
        return;
      }
      a = paramString;
      ((k)b.a()).c();
      if ((paramInt3 != 1) && (paramInt3 != 12785645))
      {
        w.b("key_temp_latest_call_made_with_tc", false);
        d(a);
        paramString = ((Number)localObject1).a();
        if ((I.a()) && (!I.d())) {
          ((com.truecaller.calling.recorder.floatingbutton.e)O.a()).a(paramString, new -..Lambda.p.Ej25DuGJuBIc1bBF5YDwp2vv3PA(this));
        }
        return;
      }
      a.o = "inSpammerList";
      return;
    }
    Object localObject1 = B.b(new String[] { paramString });
    paramString = t.a(paramString);
    Object localObject2 = a((Number)localObject1, paramString, P.a());
    localObject2 = new i(0, 0, (Number)localObject1, paramInt2, w.b("key_temp_latest_call_made_with_tc"), P.a(), (Contact)localObject2, ai.b(), paramString);
    paramString = a;
    if ((paramString == null) || (i == 3))
    {
      a = ((i)localObject2);
      c(a);
    }
    paramString = com.truecaller.tracking.events.g.b();
    paramString.d(g);
    localObject1 = z.a();
    paramString.c(org.c.a.a.a.k.n((String)localObject1));
    Object localObject3 = a;
    paramString.a(org.c.a.a.a.k.n(((Number)localObject3).d()));
    paramString.b(com.truecaller.common.h.aa.c(((Number)localObject3).a(), (String)localObject1));
    try
    {
      ((ae)q.a()).a(paramString.a());
    }
    catch (RuntimeException localRuntimeException)
    {
      paramString = localRuntimeException;
      if ((localRuntimeException instanceof IllegalArgumentException)) {
        paramString = new UnmutedException.k(localRuntimeException.getMessage());
      }
      AssertionUtil.reportThrowableButNeverCrash(paramString);
    }
    w.b("key_temp_latest_call_made_with_tc", false);
    paramString = a.a();
    A.a(paramString);
    d((i)localObject2);
    s.a(30L, TimeUnit.SECONDS, new -..Lambda.p.CbdsIqRFl5Z4pHLtFvwssd8vDKE(this));
    if ((I.a()) && (!I.d())) {
      ((com.truecaller.calling.recorder.floatingbutton.e)O.a()).a(paramString, new -..Lambda.p.WYR4MxjNWnyBxf8iO64KsOovIlg(this));
    }
  }
  
  public final void a(i parami)
  {
    S.a(new a(parami));
  }
  
  public final void b(i parami)
  {
    Contact localContact = l;
    AssertionUtil.isNotNull(l, new String[0]);
    long l1 = P.a() - d;
    parami = localContact.G();
    if ((localContact.T()) && (!R.a()) && (parami != null))
    {
      R.a(parami);
      R.a(l1);
    }
    if (((localContact.getSource() & 0x33) != 0) && (!R.b())) {
      R.b(l1);
    }
  }
  
  final class a
    implements v.a
  {
    private final i b;
    
    a(i parami)
    {
      b = parami;
    }
    
    public final void D_()
    {
      ((com.truecaller.util.aa)p.b(p.this).a()).a(b.a.a()).a(p.a(p.this), new -..Lambda.p.a.GN_PuPtabvNe03nYGLOGGoH_RLw(this));
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */