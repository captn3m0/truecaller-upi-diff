package com.truecaller.callerid;

import com.truecaller.aftercall.PromotionType;
import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.i.c;

public final class l
  implements k
{
  private final v a;
  
  public l(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return k.class.equals(paramClass);
  }
  
  public final void a()
  {
    a.a(new f(new e(), (byte)0));
  }
  
  public final void a(PromotionType paramPromotionType, HistoryEvent paramHistoryEvent, c paramc)
  {
    a.a(new d(new e(), paramPromotionType, paramHistoryEvent, paramc, (byte)0));
  }
  
  public final void a(i parami, boolean paramBoolean)
  {
    a.a(new g(new e(), parami, paramBoolean, (byte)0));
  }
  
  public final void a(HistoryEvent paramHistoryEvent, int paramInt)
  {
    a.a(new e(new e(), paramHistoryEvent, paramInt, (byte)0));
  }
  
  public final void b()
  {
    a.a(new b(new e(), (byte)0));
  }
  
  public final void c()
  {
    a.a(new a(new e(), (byte)0));
  }
  
  public final w<Boolean> d()
  {
    return w.a(a, new c(new e(), (byte)0));
  }
  
  static final class a
    extends u<k, Void>
  {
    private a(e parame)
    {
      super();
    }
    
    public final String toString()
    {
      return ".closeAfterCallScreen()";
    }
  }
  
  static final class b
    extends u<k, Void>
  {
    private b(e parame)
    {
      super();
    }
    
    public final String toString()
    {
      return ".dismissCallerIdUI()";
    }
  }
  
  static final class c
    extends u<k, Boolean>
  {
    private c(e parame)
    {
      super();
    }
    
    public final String toString()
    {
      return ".isCallerIdShown()";
    }
  }
  
  static final class d
    extends u<k, Void>
  {
    private final PromotionType b;
    private final HistoryEvent c;
    private final c d;
    
    private d(e parame, PromotionType paramPromotionType, HistoryEvent paramHistoryEvent, c paramc)
    {
      super();
      b = paramPromotionType;
      c = paramHistoryEvent;
      d = paramc;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".showAfterCallPromo(");
      localStringBuilder.append(a(b, 2));
      localStringBuilder.append(",");
      localStringBuilder.append(a(c, 1));
      localStringBuilder.append(",");
      localStringBuilder.append(a(d, 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class e
    extends u<k, Void>
  {
    private final HistoryEvent b;
    private final int c;
    
    private e(e parame, HistoryEvent paramHistoryEvent, int paramInt)
    {
      super();
      b = paramHistoryEvent;
      c = paramInt;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".showRegularAfterCallScreen(");
      localStringBuilder.append(a(b, 1));
      localStringBuilder.append(",");
      localStringBuilder.append(a(Integer.valueOf(c), 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class f
    extends u<k, Void>
  {
    private f(e parame)
    {
      super();
    }
    
    public final String toString()
    {
      return ".stopService()";
    }
  }
  
  static final class g
    extends u<k, Void>
  {
    private final i b;
    private final boolean c;
    
    private g(e parame, i parami, boolean paramBoolean)
    {
      super();
      b = parami;
      c = paramBoolean;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".updateCallerId(");
      localStringBuilder.append(a(b, 1));
      localStringBuilder.append(",");
      localStringBuilder.append(a(Boolean.valueOf(c), 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */