package com.truecaller.callerid;

import android.content.Context;
import com.truecaller.i.c;
import com.truecaller.notificationchannels.b;
import com.truecaller.notificationchannels.e;
import com.truecaller.notifications.a;
import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d<g>
{
  private final Provider<Context> a;
  private final Provider<c> b;
  private final Provider<e> c;
  private final Provider<b> d;
  private final Provider<a> e;
  
  private h(Provider<Context> paramProvider, Provider<c> paramProvider1, Provider<e> paramProvider2, Provider<b> paramProvider3, Provider<a> paramProvider4)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static h a(Provider<Context> paramProvider, Provider<c> paramProvider1, Provider<e> paramProvider2, Provider<b> paramProvider3, Provider<a> paramProvider4)
  {
    return new h(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callerid.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */