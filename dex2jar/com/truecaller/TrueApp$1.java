package com.truecaller;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.common.background.b;
import com.truecaller.push.e;
import com.truecaller.service.SyncPhoneBookService;
import com.truecaller.util.o;
import com.truecaller.wizard.utils.i.a;

final class TrueApp$1
  extends i.a
{
  TrueApp$1(TrueApp paramTrueApp) {}
  
  public final void a(Context paramContext, String paramString)
  {
    if (TextUtils.equals(paramString, "android.permission.READ_CONTACTS"))
    {
      SyncPhoneBookService.a(paramContext);
      a.c.b(10015);
    }
    if ("android.permission.READ_CALL_LOG".equals(paramString)) {
      o.a(paramContext);
    }
  }
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    if ("com.truecaller.wizard.verification.action.PHONE_VERIFIED".equals(paramIntent.getAction()))
    {
      SyncPhoneBookService.a(paramContext, true);
      a.c.b(10015);
      TrueApp.a(a).bZ().b(null);
      ((com.truecaller.config.a)TrueApp.a(a).aZ().a()).a().c();
      return;
    }
    super.onReceive(paramContext, paramIntent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.TrueApp.1
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */