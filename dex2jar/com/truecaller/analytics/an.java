package com.truecaller.analytics;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.content.b;
import javax.inject.Inject;

final class an
  implements am
{
  private final Context a;
  private final LocationManager b;
  
  @Inject
  an(Context paramContext, LocationManager paramLocationManager)
  {
    a = paramContext;
    b = paramLocationManager;
  }
  
  public final Location a()
  {
    for (;;)
    {
      try
      {
        i = b.a(a, "android.permission.ACCESS_FINE_LOCATION");
        if (i != 0) {}
      }
      catch (RuntimeException localRuntimeException)
      {
        try
        {
          int i;
          Location localLocation1;
          Location localLocation2 = b.getLastKnownLocation("network");
          if (localLocation1 == null) {
            return localLocation2;
          }
          Object localObject = localLocation1;
          if (localLocation2 != null)
          {
            long l1 = localLocation2.getElapsedRealtimeNanos();
            long l2 = localLocation1.getElapsedRealtimeNanos();
            localObject = localLocation1;
            if (l1 - l2 > 0L) {
              localObject = localLocation2;
            }
          }
          return (Location)localObject;
        }
        catch (SecurityException localSecurityException2) {}
        localRuntimeException = localRuntimeException;
        return null;
      }
      try
      {
        localLocation1 = b.getLastKnownLocation("gps");
      }
      catch (SecurityException localSecurityException1)
      {
        continue;
        return localSecurityException1;
      }
    }
    localLocation1 = null;
    i = b.a(a, "android.permission.ACCESS_COARSE_LOCATION");
    localObject = localLocation1;
    if (i != 0) {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.an
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */