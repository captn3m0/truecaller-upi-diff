package com.truecaller.analytics.a;

import c.g.b.k;
import com.google.firebase.analytics.FirebaseAnalytics;

public final class g
  implements f
{
  private final FirebaseAnalytics a;
  
  public g(FirebaseAnalytics paramFirebaseAnalytics)
  {
    a = paramFirebaseAnalytics;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "eventName");
    a.a(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.a.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */