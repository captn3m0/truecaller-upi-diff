package com.truecaller.analytics.a;

import c.g.b.k;
import c.n.m;
import java.util.UUID;
import javax.inject.Inject;

public final class b
  implements a
{
  private final com.truecaller.analytics.storage.a a;
  
  @Inject
  public b(com.truecaller.analytics.storage.a parama)
  {
    a = parama;
  }
  
  public final String a()
  {
    Object localObject2 = a.a("analyticsID");
    Object localObject1 = localObject2;
    if (localObject2 == null)
    {
      localObject1 = UUID.randomUUID().toString();
      k.a(localObject1, "UUID.randomUUID().toString()");
      localObject1 = (CharSequence)localObject1;
      localObject2 = (Appendable)new StringBuilder();
      int i = 0;
      int j = ((CharSequence)localObject1).length();
      while (i < j)
      {
        char c = ((CharSequence)localObject1).charAt(i);
        if (Character.isLetterOrDigit(c)) {
          ((Appendable)localObject2).append(c);
        }
        i += 1;
      }
      localObject1 = ((StringBuilder)localObject2).toString();
      k.a(localObject1, "filterTo(StringBuilder(), predicate).toString()");
      localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append(m.a((String)localObject1, 7));
      ((StringBuilder)localObject2).append('-');
      ((StringBuilder)localObject2).append(m.b((String)localObject1, 7));
      localObject1 = ((StringBuilder)localObject2).toString();
      a((String)localObject1);
    }
    return (String)localObject1;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "id");
    a.a("analyticsID", paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */