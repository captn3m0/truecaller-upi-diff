package com.truecaller.analytics.a;

import com.truecaller.analytics.storage.a;
import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d<b>
{
  private final Provider<a> a;
  
  private c(Provider<a> paramProvider)
  {
    a = paramProvider;
  }
  
  public static c a(Provider<a> paramProvider)
  {
    return new c(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */