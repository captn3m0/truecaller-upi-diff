package com.truecaller.analytics;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import c.g.b.k;
import c.u;
import com.truecaller.aftercall.AfterCallPromotionActivity;
import com.truecaller.androidactors.f;
import com.truecaller.calling.after_call.AfterCallActivity;
import com.truecaller.messaging.notifications.ClassZeroActivity;
import com.truecaller.notifications.SourcedContactListActivity;
import com.truecaller.tracking.events.j;
import com.truecaller.tracking.events.j.a;
import com.truecaller.ui.AfterClipboardSearchActivity;
import com.truecaller.ui.CallMeBackActivity;
import com.truecaller.ui.clicktocall.CallConfirmationActivity;
import com.truecaller.utils.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.UUID;
import org.apache.a.d.d;

public final class y
  implements w
{
  public static final y.a a = new y.a((byte)0);
  private final Class<?>[] b;
  private final String[] c;
  private int d;
  private long e;
  private boolean f;
  private String g;
  private final a h;
  private final f<ae> i;
  private final b j;
  
  public y(a parama, f<ae> paramf, b paramb)
  {
    h = parama;
    i = paramf;
    j = paramb;
    b = new Class[] { AfterCallActivity.class, AfterCallPromotionActivity.class, CallMeBackActivity.class, AfterClipboardSearchActivity.class, com.truecaller.tag.b.class, SourcedContactListActivity.class, CallConfirmationActivity.class, ClassZeroActivity.class };
    c = new String[] { "com.truecaller.flashsdk" };
    e = h.c();
    f = true;
  }
  
  private final void a(boolean paramBoolean)
  {
    e = h.c();
    if (paramBoolean) {
      f = false;
    }
  }
  
  private final boolean a()
  {
    return b() >= 300000000000L;
  }
  
  private final long b()
  {
    return h.c() - e;
  }
  
  private final boolean c()
  {
    int k;
    if ((b() < 5000000000L) && (!f)) {
      k = 0;
    } else {
      k = 1;
    }
    int m;
    if (d == 0) {
      m = 1;
    } else {
      m = 0;
    }
    return (k != 0) && (m != 0);
  }
  
  private final boolean c(Activity paramActivity)
  {
    Object localObject1 = paramActivity.getClass().getPackage();
    if (localObject1 != null)
    {
      localObject2 = ((Package)localObject1).getName();
      localObject1 = localObject2;
      if (localObject2 != null) {}
    }
    else
    {
      localObject1 = "";
    }
    Object localObject2 = c;
    Object localObject3 = (Collection)new ArrayList();
    int m = localObject2.length;
    int k = 0;
    String str;
    while (k < m)
    {
      str = localObject2[k];
      if (c.n.m.b((String)localObject1, str, false)) {
        ((Collection)localObject3).add(str);
      }
      k += 1;
    }
    localObject3 = (Iterable)localObject3;
    localObject2 = (Collection)new ArrayList(c.a.m.a((Iterable)localObject3, 10));
    localObject3 = ((Iterable)localObject3).iterator();
    while (((Iterator)localObject3).hasNext())
    {
      k = ((String)((Iterator)localObject3).next()).length();
      if (localObject1 != null)
      {
        str = ((String)localObject1).substring(k);
        k.a(str, "(this as java.lang.String).substring(startIndex)");
        ((Collection)localObject2).add(str);
      }
      else
      {
        throw new u("null cannot be cast to non-null type java.lang.String");
      }
    }
    localObject1 = (Iterable)localObject2;
    if (!((Collection)localObject1).isEmpty())
    {
      localObject1 = ((Iterable)localObject1).iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (String)((Iterator)localObject1).next();
        if ((!TextUtils.isEmpty((CharSequence)localObject2)) && (!c.n.m.b((String)localObject2, ".", false))) {
          k = 0;
        } else {
          k = 1;
        }
        if (k != 0)
        {
          k = 1;
          break label299;
        }
      }
    }
    k = 0;
    label299:
    if (k != 0) {
      return false;
    }
    localObject1 = b;
    m = localObject1.length;
    k = 0;
    while (k < m)
    {
      if (localObject1[k].isInstance(paramActivity)) {
        return false;
      }
      k += 1;
    }
    return true;
  }
  
  private final void d(Activity paramActivity)
  {
    Object localObject1 = paramActivity.getIntent();
    if (localObject1 != null) {
      localObject1 = ((Intent)localObject1).getStringExtra("AppUserInteraction.Context");
    } else {
      localObject1 = null;
    }
    String str = paramActivity.getClass().getSimpleName();
    Object localObject2 = UUID.randomUUID().toString();
    k.a(localObject2, "UUID.randomUUID().toString()");
    int m = 1;
    Object localObject3 = new StringBuilder("Logging appOpen for activity: ");
    ((StringBuilder)localObject3).append(paramActivity);
    ((StringBuilder)localObject3).append(" with context: ");
    ((StringBuilder)localObject3).append((String)localObject1);
    ((StringBuilder)localObject3).append(" and id: ");
    ((StringBuilder)localObject3).append((String)localObject2);
    ((StringBuilder)localObject3).toString();
    localObject3 = com.truecaller.tracking.events.y.b();
    paramActivity = (CharSequence)localObject1;
    localObject3 = ((com.truecaller.tracking.events.y.a)localObject3).a(paramActivity).b((CharSequence)str).c((CharSequence)localObject2).a();
    g = ((String)localObject2);
    ((ae)i.a()).a((d)localObject3);
    localObject2 = new e.a("AppVisited");
    ((e.a)localObject2).a("View", str);
    int k = m;
    if (paramActivity != null) {
      if (paramActivity.length() == 0) {
        k = m;
      } else {
        k = 0;
      }
    }
    if (k == 0) {
      ((e.a)localObject2).a("Context", (String)localObject1);
    }
    paramActivity = j;
    localObject1 = ((e.a)localObject2).a();
    k.a(localObject1, "eventBuilder.build()");
    paramActivity.b((e)localObject1);
  }
  
  public final void a(int paramInt)
  {
    if (paramInt < 20) {
      return;
    }
    f = true;
  }
  
  public final void a(Activity paramActivity)
  {
    k.b(paramActivity, "activity");
    if (!c(paramActivity)) {
      return;
    }
    if ((a()) && (c())) {
      d(paramActivity);
    }
    d += 1;
    a(true);
  }
  
  public final void a(Activity paramActivity, Bundle paramBundle)
  {
    k.b(paramActivity, "activity");
    if (!c(paramActivity)) {
      return;
    }
    if (((paramBundle == null) || (a())) && (c())) {
      d(paramActivity);
    }
    a(true);
  }
  
  public final void b(Activity paramActivity)
  {
    k.b(paramActivity, "activity");
    if (!c(paramActivity)) {
      return;
    }
    d -= 1;
    if (d == 0)
    {
      String str = g;
      if (str != null)
      {
        StringBuilder localStringBuilder = new StringBuilder("Logging appClose for activity: ");
        localStringBuilder.append(paramActivity);
        localStringBuilder.append(" with id: ");
        localStringBuilder.append(str);
        localStringBuilder.toString();
        paramActivity = j.b().a((CharSequence)str).a();
        g = null;
        ((ae)i.a()).a((d)paramActivity);
      }
    }
    a(false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.y
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */