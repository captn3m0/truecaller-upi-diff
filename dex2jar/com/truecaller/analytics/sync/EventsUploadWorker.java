package com.truecaller.analytics.sync;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.b;
import com.truecaller.androidactors.w;
import com.truecaller.bp;
import com.truecaller.common.background.TrackedWorker;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.extensions.c;
import javax.inject.Inject;
import javax.inject.Named;
import okhttp3.y;

public final class EventsUploadWorker
  extends TrackedWorker
{
  public static final EventsUploadWorker.a e = new EventsUploadWorker.a((byte)0);
  @Inject
  public b b;
  @Inject
  public ae c;
  @Inject
  @Named("andlytics-network-client")
  public y d;
  
  public EventsUploadWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public final b b()
  {
    b localb = b;
    if (localb == null) {
      k.a("analytics");
    }
    return localb;
  }
  
  public final boolean c()
  {
    return TrueApp.y().p();
  }
  
  public final ListenableWorker.a d()
  {
    try
    {
      Object localObject = c;
      if (localObject == null) {
        k.a("eventsTracker");
      }
      y localy = d;
      if (localy == null) {
        k.a("client");
      }
      if (c.a((Boolean)((ae)localObject).a(localy).d()))
      {
        localObject = ListenableWorker.a.a();
        k.a(localObject, "Result.success()");
        return (ListenableWorker.a)localObject;
      }
      localObject = ListenableWorker.a.b();
      k.a(localObject, "Result.retry()");
      return (ListenableWorker.a)localObject;
    }
    catch (InterruptedException localInterruptedException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localInterruptedException);
      ListenableWorker.a locala = ListenableWorker.a.c();
      k.a(locala, "Result.failure()");
      return locala;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.sync.EventsUploadWorker
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */