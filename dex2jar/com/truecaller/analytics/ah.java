package com.truecaller.analytics;

import android.telephony.TelephonyManager;
import com.truecaller.analytics.a.e;
import com.truecaller.analytics.storage.a;
import com.truecaller.utils.i;
import javax.inject.Provider;

public final class ah
  implements dagger.a.d<ag>
{
  private final Provider<s> a;
  private final Provider<a> b;
  private final Provider<com.truecaller.analytics.storage.d> c;
  private final Provider<ai> d;
  private final Provider<i> e;
  private final Provider<am> f;
  private final Provider<e> g;
  private final Provider<TelephonyManager> h;
  private final Provider<b> i;
  
  private ah(Provider<s> paramProvider, Provider<a> paramProvider1, Provider<com.truecaller.analytics.storage.d> paramProvider2, Provider<ai> paramProvider3, Provider<i> paramProvider4, Provider<am> paramProvider5, Provider<e> paramProvider6, Provider<TelephonyManager> paramProvider7, Provider<b> paramProvider8)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
  }
  
  public static ah a(Provider<s> paramProvider, Provider<a> paramProvider1, Provider<com.truecaller.analytics.storage.d> paramProvider2, Provider<ai> paramProvider3, Provider<i> paramProvider4, Provider<am> paramProvider5, Provider<e> paramProvider6, Provider<TelephonyManager> paramProvider7, Provider<b> paramProvider8)
  {
    return new ah(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.ah
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */