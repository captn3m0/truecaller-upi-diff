package com.truecaller.analytics;

import android.content.Context;
import com.truecaller.analytics.storage.AnalyticsDatabase;
import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d<AnalyticsDatabase>
{
  private final f a;
  private final Provider<Context> b;
  
  private j(f paramf, Provider<Context> paramProvider)
  {
    a = paramf;
    b = paramProvider;
  }
  
  public static j a(f paramf, Provider<Context> paramProvider)
  {
    return new j(paramf, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */