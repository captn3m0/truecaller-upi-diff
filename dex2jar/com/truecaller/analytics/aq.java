package com.truecaller.analytics;

import android.text.TextUtils;
import c.g.b.k;
import com.truecaller.androidactors.f;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.tracking.events.ae.a;
import com.truecaller.tracking.events.al;
import com.truecaller.tracking.events.al.a;
import com.truecaller.tracking.events.ax;
import com.truecaller.tracking.events.ax.a;
import com.truecaller.tracking.events.v;
import com.truecaller.tracking.events.v.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.a.d.d;

public final class aq
  implements ap
{
  private final f<ae> a;
  private final b b;
  
  public aq(f<ae> paramf, b paramb)
  {
    a = paramf;
    b = paramb;
  }
  
  public final void a(e parame)
  {
    k.b(parame, "event");
    b.a(parame);
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "transport");
    k.b(paramString2, "context");
    com.truecaller.tracking.events.ae localae = com.truecaller.tracking.events.ae.b().a((CharSequence)paramString1).b((CharSequence)paramString2).a();
    ((ae)a.a()).a((d)localae);
    paramString1 = new e.a("SwitchMessageTransport").a("Transport", paramString1).a("Context", paramString2).a();
    paramString2 = b;
    k.a(paramString1, "it");
    paramString2.b(paramString1);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, Participant[] paramArrayOfParticipant)
  {
    k.b(paramString1, "userInteraction");
    k.b(paramString2, "analyticsId");
    k.b(paramString3, "transportName");
    k.b(paramArrayOfParticipant, "participants");
    Collection localCollection = (Collection)new ArrayList(paramArrayOfParticipant.length);
    int j = paramArrayOfParticipant.length;
    int i = 0;
    while (i < j)
    {
      Object localObject1 = paramArrayOfParticipant[i];
      k.b(localObject1, "participant");
      Object localObject2 = al.b().a(((Participant)localObject1).d());
      boolean bool1 = TextUtils.isEmpty((CharSequence)m);
      boolean bool2 = true;
      localObject2 = ((al.a)localObject2).b(bool1 ^ true);
      if (j == 1) {
        bool1 = true;
      } else {
        bool1 = false;
      }
      localObject2 = ((al.a)localObject2).a(Boolean.valueOf(bool1)).b(Boolean.valueOf(k));
      if (j == 2) {
        bool1 = true;
      } else {
        bool1 = false;
      }
      localObject2 = ((al.a)localObject2).c(Boolean.valueOf(bool1));
      if (q >= 10) {
        bool1 = true;
      } else {
        bool1 = false;
      }
      localObject2 = ((al.a)localObject2).d(Boolean.valueOf(bool1));
      if ((o & 0x40) != 0) {
        bool1 = bool2;
      } else {
        bool1 = false;
      }
      localObject2 = ((al.a)localObject2).e(Boolean.valueOf(bool1)).a(Integer.valueOf(Math.max(0, q))).a();
      localObject1 = ax.b().a((CharSequence)((Participant)localObject1).j()).b((CharSequence)((Participant)localObject1).k()).a((al)localObject2).a();
      k.a(localObject1, "com.truecaller.tracking.…nfo)\n            .build()");
      localCollection.add(localObject1);
      i += 1;
    }
    paramArrayOfParticipant = (List)localCollection;
    paramString1 = v.b().a(paramArrayOfParticipant).a((CharSequence)paramString1).b((CharSequence)paramString3).c((CharSequence)paramString2).a();
    ((ae)a.a()).a((d)paramString1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.aq
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */