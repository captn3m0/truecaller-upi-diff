package com.truecaller.analytics;

import android.app.Application;
import dagger.a.d;
import javax.inject.Provider;

public final class i
  implements d<b>
{
  private final f a;
  private final Provider<Application> b;
  
  private i(f paramf, Provider<Application> paramProvider)
  {
    a = paramf;
    b = paramProvider;
  }
  
  public static i a(f paramf, Provider<Application> paramProvider)
  {
    return new i(paramf, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */