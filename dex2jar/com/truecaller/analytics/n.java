package com.truecaller.analytics;

import android.content.Context;
import android.location.LocationManager;
import dagger.a.d;
import javax.inject.Provider;

public final class n
  implements d<LocationManager>
{
  private final f a;
  private final Provider<Context> b;
  
  private n(f paramf, Provider<Context> paramProvider)
  {
    a = paramf;
    b = paramProvider;
  }
  
  public static n a(f paramf, Provider<Context> paramProvider)
  {
    return new n(paramf, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */