package com.truecaller.analytics;

import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Bundle;
import com.google.android.gms.common.GoogleApiAvailability;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.bp;
import com.truecaller.common.account.r;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.h.u;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.filters.p;
import com.truecaller.filters.s;
import com.truecaller.notifications.i;
import com.truecaller.old.data.access.Settings;
import com.truecaller.tracking.events.ab;
import com.truecaller.tracking.events.ab.a;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.util.b.at;
import com.truecaller.util.b.j;
import com.truecaller.util.b.j.a;
import com.truecaller.utils.l;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import org.a.a.n;

public class AppSettingsTask
  extends PersistentBackgroundTask
{
  @Inject
  public f<ae> a;
  @Inject
  public b b;
  @Inject
  public com.truecaller.utils.d c;
  @Inject
  public i d;
  @Inject
  public com.truecaller.common.g.a e;
  @Inject
  public p f;
  @Inject
  public com.truecaller.i.c g;
  @Inject
  public com.truecaller.i.e h;
  @Inject
  public com.truecaller.common.h.ac i;
  @Inject
  public com.truecaller.multisim.h j;
  @Inject
  public com.truecaller.d.b k;
  @Inject
  public com.truecaller.common.h.c l;
  @Inject
  public com.truecaller.push.e m;
  @Inject
  public com.truecaller.messaging.h n;
  @Inject
  public com.truecaller.featuretoggles.e o;
  @Inject
  public android.support.v4.app.ac p;
  @Inject
  public l q;
  @Inject
  public com.truecaller.calling.e.e r;
  @Inject
  public s s;
  @Inject
  public u t;
  @Inject
  public com.truecaller.abtest.c u;
  
  private static String a(boolean paramBoolean1, boolean paramBoolean2)
  {
    if (!paramBoolean1) {
      return "NotSupported";
    }
    if (paramBoolean2) {
      return "Enabled";
    }
    return "Disabled";
  }
  
  public static void a(com.truecaller.common.background.b paramb)
  {
    paramb.b(10001);
  }
  
  private void a(com.truecaller.filters.a.b paramb)
  {
    Object localObject1 = t.b();
    if (localObject1 == null) {
      return;
    }
    HashSet localHashSet = new HashSet();
    while (paramb.moveToNext())
    {
      Object localObject2 = paramb.a();
      if (f.equals("PHONE_NUMBER"))
      {
        localObject2 = t.e(e);
        if ((localObject2 != null) && (!((String)localObject1).equalsIgnoreCase((String)localObject2))) {
          localHashSet.add(localObject2);
        }
      }
    }
    double d1 = localHashSet.size();
    localObject1 = new e.a("CountOfForeignCountriesWithNumberBlocked");
    a = Double.valueOf(d1);
    localObject1 = ((e.a)localObject1).a();
    b.a((e)localObject1);
    paramb.close();
  }
  
  public static void b(com.truecaller.common.background.b paramb)
  {
    paramb.a(10001);
  }
  
  public final int a()
  {
    return 10001;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    TrueApp.y().a().a(this);
    paramBundle = new ArrayList();
    Object localObject1 = ((TrueApp)paramContext.getApplicationContext()).a().I();
    Object localObject2 = ab.b();
    ((ab.a)localObject2).a("notificationsAllowed");
    ((ab.a)localObject2).b(String.valueOf(q.d()));
    paramBundle.add(((ab.a)localObject2).a());
    paramBundle.add(ab.b().a("availabilityEnabled").b(String.valueOf(Settings.e("availability_enabled"))).a());
    paramBundle.add(ab.b().a("backupEnabled").b(String.valueOf(((com.truecaller.common.g.a)localObject1).a("backup_enabled", false))).a());
    paramBundle.add(ab.b().a("accountWasBackedUp").b(String.valueOf(((com.truecaller.common.g.a)localObject1).a("accountFileWasBackedUpByAutobackup", false))).a());
    long l1 = ((com.truecaller.common.g.a)localObject1).a("key_backup_frequency_hours", 0L);
    paramBundle.add(ab.b().a("backupFrequency").b(String.valueOf(l1)).a());
    paramBundle.add(ab.b().a("region1").b(String.valueOf(i.a())).a());
    paramBundle.add(ab.b().a("TruecallerX").b(String.valueOf(o.e().a())).a());
    paramBundle = paramBundle.iterator();
    while (paramBundle.hasNext())
    {
      localObject1 = (ab)paramBundle.next();
      ((ae)a.a()).a((org.apache.a.d.d)localObject1);
    }
    Object localObject4 = b;
    boolean bool2 = i.a();
    boolean bool1 = l.c();
    boolean bool3 = true;
    if ((bool1) && (!bool2)) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    boolean bool4 = q.d();
    if (g.a("merge_by", 3) == 3) {
      paramBundle = "Enabled";
    } else {
      paramBundle = "Disabled";
    }
    if (g.a("showFrequentlyCalledContacts", true)) {
      localObject1 = "Enabled";
    } else {
      localObject1 = "Disabled";
    }
    if (n.b(0)) {
      localObject2 = "Enabled";
    } else {
      localObject2 = "Disabled";
    }
    if (g.a("showIncomingCallNotifications", true)) {
      localObject3 = "Enabled";
    } else {
      localObject3 = "Disabled";
    }
    e.a locala = new e.a("SettingState").a("Theme", ThemeManager.a().name());
    if ((e.b("backup")) && (!bool2)) {
      bool2 = true;
    } else {
      bool2 = false;
    }
    ((b)localObject4).a(locala.a("EnhancedSearch", a(bool1, bool2)).a("BlockTopSpammers", a(true, f.g())).a("BlockHiddenCalls", a(true, f.a())).a("CallHistoryTap", Settings.b("callLogTapBehavior")).a("SearchClipBoard", a(true, g.b("clipboardSearchEnabled"))).a("SearchMessagingApps", a(bool4, Settings.j("enhancedNotificationsEnabled"))).a("MissedCallNotification", a(bool4, Settings.j("showMissedCallsNotifications"))).a("MissedCallReminder", a(true, Settings.j("showMissedCallReminders"))).a("CallerIdPhonebook", a(true, g.b("enabledCallerIDforPB"))).a("AfterCall", a(true, g.b("afterCall"))).a("GroupCalls", paramBundle).a("WhatsApp", a(true, r.b())).a("MostCalled", (String)localObject1).a("Backup", a(true, e.b("backup_enabled"))).a("AccountBackedUp", a(true, e.b("accountFileWasBackedUpByAutobackup"))).a("flashEnabled", Settings.g()).a("SmsDeliveryReport", (String)localObject2).a("IncomingCallNotification", (String)localObject3).a());
    paramBundle = new ArrayList();
    localObject1 = ab.b();
    ((ab.a)localObject1).a("defaultMessagingApp");
    localObject2 = c.k();
    if (localObject2 != null) {
      ((ab.a)localObject1).b((CharSequence)localObject2);
    } else {
      ((ab.a)localObject1).b("NotSupported");
    }
    paramBundle.add(((ab.a)localObject1).a());
    localObject1 = ab.b();
    ((ab.a)localObject1).a("messageReadPermission");
    ((ab.a)localObject1).b(String.valueOf(q.a(new String[] { "android.permission.READ_SMS" })));
    paramBundle.add(((ab.a)localObject1).a());
    localObject1 = ab.b();
    ((ab.a)localObject1).a("messageWritePermission");
    ((ab.a)localObject1).b(String.valueOf(c.a()));
    paramBundle.add(((ab.a)localObject1).a());
    if (o.b().a())
    {
      localObject1 = new n(org.a.a.h.a(1L), org.a.a.b.ay_());
      if ((!((n)localObject1).a(n.E())) && (!((n)localObject1).a(n.F()))) {
        bool1 = false;
      } else {
        bool1 = true;
      }
      paramBundle.add(ab.b().a("userIm").b(String.valueOf(bool1)).a());
    }
    paramBundle = paramBundle.iterator();
    while (paramBundle.hasNext())
    {
      localObject1 = (ab)paramBundle.next();
      ((ae)a.a()).a((org.apache.a.d.d)localObject1);
    }
    Object localObject3 = d;
    if (c.h() >= 23) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    localObject4 = new ArrayList();
    localObject1 = c.k();
    paramBundle = (Bundle)localObject1;
    if (localObject1 == null) {
      paramBundle = "NotSupported";
    }
    localObject2 = c.j();
    localObject1 = localObject2;
    if (localObject2 == null) {
      localObject1 = "NotSupported";
    }
    bool2 = android.support.v4.app.ac.a(paramContext).a();
    ((List)localObject4).add(new e.a("PermissionState").a("SMSApp", paramBundle).a("SMSRead", a(bool1, q.a(new String[] { "android.permission.READ_SMS" }))).a("DialerApp", (String)localObject1).a("NotificationsShow", a(true, bool2)).a("Camera", a(bool1, q.a(new String[] { "android.permission.CAMERA" }))).a("Contacts", a(bool1, q.a(new String[] { "android.permission.READ_CONTACTS" }))).a("Storage", a(bool1, q.a(new String[] { "android.permission.READ_EXTERNAL_STORAGE" }))).a("Phone", a(bool1, q.a(new String[] { "android.permission.READ_PHONE_STATE" }))).a("Location", a(bool1, q.a(new String[] { "android.permission.ACCESS_FINE_LOCATION" }))).a("DrawOnTop", a(bool1, q.a())).a("NotificationsAccess", a(true, ((i)localObject3).a())).a("BatteryOptimization", a(bool1, c.f() ^ true)).a("SettingsWrite", a(bool1, c.p())).a());
    paramBundle = ((List)localObject4).iterator();
    while (paramBundle.hasNext())
    {
      localObject1 = (e)paramBundle.next();
      b.a((e)localObject1);
    }
    paramBundle = ab.b();
    paramBundle.a("systemNotificationsAllowed");
    paramBundle.b(String.valueOf(p.a()));
    paramBundle = Collections.singletonList(paramBundle.a()).iterator();
    while (paramBundle.hasNext())
    {
      localObject1 = (ab)paramBundle.next();
      ((ae)a.a()).a((org.apache.a.d.d)localObject1);
    }
    localObject2 = b;
    paramBundle = at.a(paramContext).a();
    localObject3 = new e.a("AppState").a("DualSim", j.j());
    ((e.a)localObject3).a("DomainFronting", k.a());
    if (paramBundle != null) {
      ((e.a)localObject3).a("CarrierMenu", a);
    }
    localObject1 = m.c();
    localObject4 = m.a();
    paramBundle = "Empty";
    if (localObject4 != null) {
      if (((String)localObject4).equals(localObject1)) {
        paramBundle = "Uploaded";
      } else {
        paramBundle = "NotUploaded";
      }
    }
    ((e.a)localObject3).a("PushId", paramBundle);
    if (GoogleApiAvailability.a().a(paramContext.getApplicationContext()) == 0) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    ((e.a)localObject3).a("GoogleServices", bool1);
    if (i.a()) {
      paramBundle = "Enabled";
    } else {
      paramBundle = "Disabled";
    }
    ((e.a)localObject3).a("Region1", paramBundle);
    paramBundle = com.truecaller.common.h.h.c(paramContext);
    if ((paramBundle != null) && (b != null)) {
      ((e.a)localObject3).a("ProfileCountry", b);
    }
    paramBundle = e.a("networkDomain");
    if (paramBundle != null) {
      ((e.a)localObject3).a("Domain", paramBundle);
    }
    localObject1 = c.i();
    paramBundle = (Bundle)localObject1;
    if (localObject1 == null) {
      paramBundle = "Unknown";
    }
    ((e.a)localObject3).a("SecurityPatchVersion", paramBundle);
    if (c.h() >= 28)
    {
      paramContext = (UsageStatsManager)paramContext.getSystemService("usagestats");
      if (paramContext != null)
      {
        int i1 = paramContext.getAppStandbyBucket();
        if (i1 != 10)
        {
          if (i1 != 20)
          {
            if (i1 != 30)
            {
              if (i1 != 40) {
                paramContext = "Unknown";
              } else {
                paramContext = "Rare";
              }
            }
            else {
              paramContext = "Frequent";
            }
          }
          else {
            paramContext = "WorkingSet";
          }
        }
        else {
          paramContext = "Active";
        }
        ((e.a)localObject3).a("StandbyBucket", paramContext);
      }
      else
      {
        ((e.a)localObject3).a("StandbyBucket", "CouldntGetUsageStatsMgr");
      }
    }
    ((b)localObject2).a(((e.a)localObject3).a());
    paramContext = b;
    paramBundle = ar.a;
    double d1 = ar.b();
    Double.isNaN(d1);
    d1 /= 1000.0D;
    paramBundle = new e.a("DevicePerformanceMemory");
    a = Double.valueOf(d1);
    paramContext.a(paramBundle.a());
    if (Build.VERSION.SDK_INT >= 21)
    {
      paramContext = b;
      paramBundle = ar.a;
      paramBundle = ar.a();
      localObject1 = new e.a("DevicePerformanceCpu");
      if (paramBundle != null) {
        bool1 = bool3;
      } else {
        bool1 = false;
      }
      localObject1 = ((e.a)localObject1).a("Valid", bool1);
      a = Double.valueOf(((Double)org.c.a.a.a.h.a(paramBundle, Double.valueOf(0.0D))).doubleValue() * 1000.0D);
      paramContext.a(((e.a)localObject1).a());
    }
    s.a().a(new -..Lambda.AppSettingsTask.639E84OlVISHNe6dulXNN88C5eQ(this));
    localObject1 = new HashMap();
    paramContext = com.truecaller.utils.h.b();
    if (paramContext != null) {
      ((HashMap)localObject1).put("XiaomiMuiVersion", paramContext);
    }
    if (o.e().a()) {
      paramContext = "True";
    } else {
      paramContext = "False";
    }
    ((HashMap)localObject1).put("TruecallerX", paramContext);
    ((HashMap)localObject1).put("BuildName", l.f());
    paramBundle = l.g();
    if (paramBundle == null)
    {
      paramContext = "<null>";
    }
    else
    {
      paramContext = paramBundle;
      if (paramBundle.equals("")) {
        paramContext = "<empty>";
      }
    }
    ((HashMap)localObject1).put("InstallerPackage", paramContext);
    if ("true".equalsIgnoreCase(u.a("user_prop_churn"))) {
      paramContext = "Likely";
    } else {
      paramContext = "NotLikely";
    }
    ((HashMap)localObject1).put("LikelyToChurn", paramContext);
    if ("true".equalsIgnoreCase(u.a("user_prop_spend"))) {
      paramContext = "Likely";
    } else {
      paramContext = "NotLikely";
    }
    ((HashMap)localObject1).put("LikelyToSpend", paramContext);
    b.a((Map)localObject1);
    return PersistentBackgroundTask.RunResult.Success;
  }
  
  public final boolean a(Context paramContext)
  {
    return ((com.truecaller.common.b.a)paramContext).u().k().c();
  }
  
  public final com.truecaller.common.background.e b()
  {
    com.truecaller.common.background.e.a locala = new com.truecaller.common.background.e.a(1).a(12L, TimeUnit.HOURS);
    c = false;
    return locala.b(20L, TimeUnit.MINUTES).b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.AppSettingsTask
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */