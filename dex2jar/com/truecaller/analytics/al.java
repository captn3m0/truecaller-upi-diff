package com.truecaller.analytics;

import android.app.Application;
import c.g;
import c.g.a.a;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import java.util.Map;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ax;
import kotlinx.coroutines.bg;

public final class al
  implements b
{
  private final c.f b;
  private final String c;
  
  public al(Application paramApplication)
  {
    b = g.a((a)new al.d(paramApplication));
    c = "facebook";
  }
  
  public final void a(long paramLong, String paramString)
  {
    kotlinx.coroutines.e.b((ag)bg.a, (c.d.f)ax.b(), (m)new al.b(this, paramLong, paramString, null), 2);
  }
  
  public final void a(e parame)
  {
    k.b(parame, "event");
    b(parame);
  }
  
  public final void a(Map<String, String> paramMap)
  {
    k.b(paramMap, "properties");
    if (paramMap.isEmpty()) {
      return;
    }
    kotlinx.coroutines.e.b((ag)bg.a, (c.d.f)ax.b(), (m)new al.c(paramMap, null), 2);
  }
  
  public final void b(e parame)
  {
    k.b(parame, "event");
    int i = parame.a().length();
    boolean bool2 = true;
    boolean bool1;
    if (i <= 40) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    AssertionUtil.OnlyInDebug.isTrue(bool1, new String[] { "A Facebook app event name can not be longer than 40 characters" });
    Map localMap = parame.b();
    if (localMap != null) {
      i = localMap.size();
    } else {
      i = 0;
    }
    if (i <= 25) {
      bool1 = bool2;
    } else {
      bool1 = false;
    }
    AssertionUtil.OnlyInDebug.isTrue(bool1, new String[] { "A Facebook app event can't have more than 25 parameters" });
    kotlinx.coroutines.e.b((ag)bg.a, (c.d.f)ax.b(), (m)new al.a(this, parame, null), 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.al
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */