package com.truecaller.analytics;

import com.truecaller.tracking.events.EventRecordVersionedV2;
import java.io.IOException;
import java.util.ArrayList;
import okhttp3.y;

abstract interface ai
{
  public abstract boolean a(s params, y paramy, u paramu)
    throws IOException;
  
  public abstract boolean a(s params, y paramy, ArrayList<EventRecordVersionedV2> paramArrayList, u paramu)
    throws IOException;
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.ai
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */