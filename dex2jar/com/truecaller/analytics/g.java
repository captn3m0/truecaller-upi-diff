package com.truecaller.analytics;

import android.content.Context;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.k;
import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d<i>
{
  private final f a;
  private final Provider<Context> b;
  private final Provider<k> c;
  private final Provider<Integer> d;
  
  private g(f paramf, Provider<Context> paramProvider, Provider<k> paramProvider1, Provider<Integer> paramProvider2)
  {
    a = paramf;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
  }
  
  public static g a(f paramf, Provider<Context> paramProvider, Provider<k> paramProvider1, Provider<Integer> paramProvider2)
  {
    return new g(paramf, paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */