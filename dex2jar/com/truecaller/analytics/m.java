package com.truecaller.analytics;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class m
  implements d<com.truecaller.analytics.a.f>
{
  private final f a;
  private final Provider<Context> b;
  
  private m(f paramf, Provider<Context> paramProvider)
  {
    a = paramf;
    b = paramProvider;
  }
  
  public static m a(f paramf, Provider<Context> paramProvider)
  {
    return new m(paramf, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */