package com.truecaller.analytics;

import com.truecaller.androidactors.i;
import dagger.a.d;
import javax.inject.Provider;

public final class l
  implements d<com.truecaller.androidactors.f<ae>>
{
  private final f a;
  private final Provider<ae> b;
  private final Provider<i> c;
  
  private l(f paramf, Provider<ae> paramProvider, Provider<i> paramProvider1)
  {
    a = paramf;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static l a(f paramf, Provider<ae> paramProvider, Provider<i> paramProvider1)
  {
    return new l(paramf, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */