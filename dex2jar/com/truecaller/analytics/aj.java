package com.truecaller.analytics;

import com.truecaller.analytics.storage.a;
import com.truecaller.analytics.storage.c;
import com.truecaller.analytics.storage.d;
import com.truecaller.tracking.events.EventRecordVersionedV2;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import okhttp3.y;

final class aj
  implements ai
{
  private static final long a = TimeUnit.SECONDS.toMillis(5L);
  private static final long b = TimeUnit.SECONDS.toMillis(10L);
  private final int c;
  private final int d;
  private final long e;
  private final d f;
  private final a g;
  
  @Inject
  aj(a parama, d paramd)
  {
    g = parama;
    c = parama.a("uploadEventsMaxBatchSize", 100);
    d = parama.a("uploadEventsMinBatchSize", 100);
    e = parama.a("uploadEventsRetryJitter", 10000L);
    f = paramd;
  }
  
  private int a(int paramInt, long paramLong, boolean paramBoolean)
  {
    int i;
    if ((paramBoolean) && (paramLong <= b))
    {
      if (paramLong < a) {
        i = Math.min(paramInt * 133 / 100, c);
      } else {
        i = paramInt;
      }
    }
    else {
      i = Math.max(paramInt * 66 / 100, d);
    }
    if (paramInt != i) {
      g.b("analyticsUploadEnhancedBatchSize", i);
    }
    return i;
  }
  
  /* Error */
  private aj.b a(s params, y paramy, ArrayList<EventRecordVersionedV2> paramArrayList)
    throws IOException
  {
    // Byte code:
    //   0: new 10	com/truecaller/analytics/aj$b
    //   3: dup
    //   4: aload_0
    //   5: iconst_0
    //   6: invokespecial 97	com/truecaller/analytics/aj$b:<init>	(Lcom/truecaller/analytics/aj;B)V
    //   9: astore 6
    //   11: aload_3
    //   12: invokevirtual 103	java/util/ArrayList:isEmpty	()Z
    //   15: ifeq +6 -> 21
    //   18: aload 6
    //   20: areturn
    //   21: aload_3
    //   22: invokevirtual 107	java/util/ArrayList:listIterator	()Ljava/util/ListIterator;
    //   25: astore 7
    //   27: aload 7
    //   29: invokeinterface 112 1 0
    //   34: ifeq +23 -> 57
    //   37: aload 7
    //   39: invokeinterface 116 1 0
    //   44: ifnonnull -17 -> 27
    //   47: aload 7
    //   49: invokeinterface 119 1 0
    //   54: goto -27 -> 27
    //   57: aload_1
    //   58: invokeinterface 124 1 0
    //   63: astore 7
    //   65: aload_1
    //   66: invokeinterface 127 1 0
    //   71: astore_1
    //   72: aload_1
    //   73: ifnonnull +6 -> 79
    //   76: aload 6
    //   78: areturn
    //   79: invokestatic 133	java/lang/System:currentTimeMillis	()J
    //   82: lstore 4
    //   84: new 135	java/lang/StringBuilder
    //   87: dup
    //   88: ldc -119
    //   90: invokespecial 140	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   93: astore 8
    //   95: aload 8
    //   97: aload 7
    //   99: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   102: pop
    //   103: aload 8
    //   105: ldc -110
    //   107: invokevirtual 149	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   110: pop
    //   111: aload 8
    //   113: aload_3
    //   114: invokevirtual 153	java/util/ArrayList:size	()I
    //   117: invokevirtual 156	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   120: pop
    //   121: aload 8
    //   123: invokevirtual 160	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   126: pop
    //   127: new 8	com/truecaller/analytics/aj$a
    //   130: dup
    //   131: invokestatic 165	com/truecaller/tracking/events/PacketVersionedV2:b	()Lcom/truecaller/tracking/events/PacketVersionedV2$a;
    //   134: aload_3
    //   135: invokevirtual 170	com/truecaller/tracking/events/PacketVersionedV2$a:a	(Ljava/util/List;)Lcom/truecaller/tracking/events/PacketVersionedV2$a;
    //   138: invokevirtual 173	com/truecaller/tracking/events/PacketVersionedV2$a:a	()Lcom/truecaller/tracking/events/PacketVersionedV2;
    //   141: invokespecial 176	com/truecaller/analytics/aj$a:<init>	(Lorg/apache/a/d/d;)V
    //   144: astore_3
    //   145: new 178	okhttp3/ab$a
    //   148: dup
    //   149: invokespecial 179	okhttp3/ab$a:<init>	()V
    //   152: astore 8
    //   154: aload 8
    //   156: ldc -75
    //   158: aload_3
    //   159: invokevirtual 184	okhttp3/ab$a:a	(Ljava/lang/String;Lokhttp3/ac;)Lokhttp3/ab$a;
    //   162: aload_1
    //   163: invokevirtual 187	okhttp3/ab$a:a	(Lokhttp3/t;)Lokhttp3/ab$a;
    //   166: ldc -67
    //   168: ldc -65
    //   170: invokevirtual 194	okhttp3/ab$a:b	(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/ab$a;
    //   173: aload 7
    //   175: invokevirtual 197	okhttp3/ab$a:a	(Lokhttp3/u;)Lokhttp3/ab$a;
    //   178: pop
    //   179: aload_2
    //   180: aload 8
    //   182: invokevirtual 200	okhttp3/ab$a:a	()Lokhttp3/ab;
    //   185: iconst_0
    //   186: invokestatic 205	okhttp3/aa:a	(Lokhttp3/y;Lokhttp3/ab;Z)Lokhttp3/aa;
    //   189: invokestatic 211	com/google/firebase/perf/network/FirebasePerfOkHttpClient:execute	(Lokhttp3/e;)Lokhttp3/ad;
    //   192: astore_3
    //   193: aconst_null
    //   194: astore_2
    //   195: aload_2
    //   196: astore_1
    //   197: aload 6
    //   199: invokestatic 133	java/lang/System:currentTimeMillis	()J
    //   202: lload 4
    //   204: lsub
    //   205: putfield 212	com/truecaller/analytics/aj$b:b	J
    //   208: aload_3
    //   209: ifnull +28 -> 237
    //   212: aload_2
    //   213: astore_1
    //   214: aload 6
    //   216: aload_3
    //   217: invokevirtual 216	okhttp3/ad:c	()Z
    //   220: putfield 219	com/truecaller/analytics/aj$b:a	Z
    //   223: aload_2
    //   224: astore_1
    //   225: aload 6
    //   227: aload_3
    //   228: getfield 220	okhttp3/ad:c	I
    //   231: invokestatic 226	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   234: putfield 229	com/truecaller/analytics/aj$b:c	Ljava/lang/Integer;
    //   237: aload_3
    //   238: ifnull +7 -> 245
    //   241: aload_3
    //   242: invokevirtual 232	okhttp3/ad:close	()V
    //   245: aload 6
    //   247: areturn
    //   248: astore_2
    //   249: goto +8 -> 257
    //   252: astore_2
    //   253: aload_2
    //   254: astore_1
    //   255: aload_2
    //   256: athrow
    //   257: aload_3
    //   258: ifnull +27 -> 285
    //   261: aload_1
    //   262: ifnull +19 -> 281
    //   265: aload_3
    //   266: invokevirtual 232	okhttp3/ad:close	()V
    //   269: goto +16 -> 285
    //   272: astore_3
    //   273: aload_1
    //   274: aload_3
    //   275: invokevirtual 236	java/lang/Throwable:addSuppressed	(Ljava/lang/Throwable;)V
    //   278: goto +7 -> 285
    //   281: aload_3
    //   282: invokevirtual 232	okhttp3/ad:close	()V
    //   285: aload_2
    //   286: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	287	0	this	aj
    //   0	287	1	params	s
    //   0	287	2	paramy	y
    //   0	287	3	paramArrayList	ArrayList<EventRecordVersionedV2>
    //   82	121	4	l	long
    //   9	237	6	localb	aj.b
    //   25	149	7	localObject1	Object
    //   93	88	8	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   197	208	248	finally
    //   214	223	248	finally
    //   225	237	248	finally
    //   255	257	248	finally
    //   197	208	252	java/lang/Throwable
    //   214	223	252	java/lang/Throwable
    //   225	237	252	java/lang/Throwable
    //   265	269	272	java/lang/Throwable
  }
  
  private boolean a(int paramInt, aj.b paramb)
  {
    int i;
    if ((c != null) && (c.intValue() >= 500)) {
      i = 1;
    } else {
      i = 0;
    }
    double d1;
    if (i != 0) {
      d1 = paramInt;
    }
    try
    {
      d1 = Math.pow(2.0D, d1);
      double d2 = Math.random();
      long l = e;
      double d3 = l;
      Double.isNaN(d3);
      l = (d1 * 1000.0D + d2 * d3);
      "Upload postponed in ms: ".concat(String.valueOf(l));
      Thread.sleep(l);
      return true;
    }
    catch (InterruptedException paramb)
    {
      for (;;) {}
    }
    return false;
    return false;
  }
  
  public final boolean a(s params, y paramy, u paramu)
    throws IOException
  {
    for (;;)
    {
      try
      {
        k = g.a("analyticsUploadEnhancedBatchSize", c);
        ArrayList localArrayList = new ArrayList();
        if (params.f() == null)
        {
          paramu.a("MissingHeaders");
          return false;
        }
        int i = 0;
        int m = 0;
        List localList = f.a(k);
        j = i;
        if (!localList.isEmpty())
        {
          localArrayList.clear();
          Object localObject1 = localList.iterator();
          Object localObject2;
          if (((Iterator)localObject1).hasNext())
          {
            localObject2 = ad.a(nextb);
            if (localObject2 == null) {
              continue;
            }
            localArrayList.add(localObject2);
            continue;
          }
          if (localArrayList.isEmpty())
          {
            f.a(getsize1a);
            continue;
            int n;
            if (k < 3)
            {
              localObject2 = a(params, paramy, localArrayList);
              bool1 = a;
              if (bool1)
              {
                f.a(getsize1a);
                n = m + j;
                k = a(j, b, true);
                j = i + localArrayList.size();
                paramu.a(j);
              }
              else
              {
                StringBuilder localStringBuilder = new StringBuilder("Upload failed. Server response: ");
                if (c == null) {
                  localObject1 = "none";
                } else {
                  localObject1 = c;
                }
                localStringBuilder.append(localObject1);
                localStringBuilder.toString();
                j = a(j, b, false);
                paramu.a(c);
                boolean bool2 = a(k, (aj.b)localObject2);
                if (!bool2) {
                  return false;
                }
                k += 1;
              }
            }
            else
            {
              k = j;
              n = m;
              j = i;
            }
            if (!bool1) {
              return false;
            }
            i = j;
            m = n;
            if (n < 2000) {
              continue;
            }
          }
        }
        else
        {
          if (j == 0) {
            paramu.a("NoEvents");
          }
          return true;
        }
      }
      finally {}
      int j = k;
      int k = 0;
      boolean bool1 = false;
    }
  }
  
  public final boolean a(s params, y paramy, ArrayList<EventRecordVersionedV2> paramArrayList, u paramu)
    throws IOException
  {
    int i = 0;
    while (i < 3) {
      try
      {
        aj.b localb = a(params, paramy, paramArrayList);
        if (a)
        {
          paramu.a(paramArrayList.size());
          return true;
        }
        StringBuilder localStringBuilder = new StringBuilder("Upload failed. Server response: ");
        Object localObject;
        if (c == null) {
          localObject = "none";
        } else {
          localObject = c;
        }
        localStringBuilder.append(localObject);
        localStringBuilder.toString();
        paramu.a(c);
        boolean bool = a(i, localb);
        if (!bool) {
          return false;
        }
        i += 1;
      }
      finally {}
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.aj
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */