package com.truecaller.analytics;

import android.content.Context;
import android.telephony.TelephonyManager;
import dagger.a.d;
import javax.inject.Provider;

public final class q
  implements d<TelephonyManager>
{
  private final f a;
  private final Provider<Context> b;
  
  private q(f paramf, Provider<Context> paramProvider)
  {
    a = paramf;
    b = paramProvider;
  }
  
  public static q a(f paramf, Provider<Context> paramProvider)
  {
    return new q(paramf, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */