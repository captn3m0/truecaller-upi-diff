package com.truecaller.analytics.storage;

import android.content.Context;
import android.content.SharedPreferences;
import c.a.an;
import c.g.b.k;
import com.truecaller.log.UnmutedException.i;
import com.truecaller.log.d;
import com.truecaller.utils.a.c.a;

public final class b
  extends com.truecaller.utils.a.a
  implements a
{
  private final int b;
  private final String c;
  private final SharedPreferences d;
  
  public b(SharedPreferences paramSharedPreferences)
  {
    super(paramSharedPreferences);
    d = paramSharedPreferences;
    b = 1;
    c = "analytics";
  }
  
  public final int a()
  {
    return b;
  }
  
  public final void a(int paramInt, Context paramContext)
  {
    k.b(paramContext, "context");
    if (paramInt <= 0)
    {
      paramContext = paramContext.getSharedPreferences("tc.settings", 0);
      k.a(paramContext, "oldSharedPreferences");
      a(paramContext, an.a(new String[] { "uploadEventsRetryJitter", "analyticsID", "uploadEventsMaxBatchSize", "uploadEventsMinBatchSize" }));
      paramContext = d;
      k.b("analyticsUploadEnhancedBatchSize", "key");
      k.b(paramContext, "source");
      paramContext = c.a.a(this, "analyticsUploadEnhancedBatchSize", paramContext);
      if (paramContext != null)
      {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append(paramContext.getClass().getCanonicalName());
        localStringBuilder.append(": ");
        localStringBuilder.append(paramContext.getMessage());
        d.a((Throwable)new UnmutedException.i(localStringBuilder.toString()));
        d("analyticsUploadEnhancedBatchSize");
        return;
      }
    }
  }
  
  public final String b()
  {
    return c;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.storage.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */