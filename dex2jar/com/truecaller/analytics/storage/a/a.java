package com.truecaller.analytics.storage.a;

import android.arch.persistence.room.f.b;
import android.content.Context;

public final class a
  extends f.b
{
  private final Context a;
  
  public a(Context paramContext)
  {
    a = paramContext;
  }
  
  /* Error */
  public final void a(android.arch.persistence.db.b paramb)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 29
    //   3: invokestatic 16	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: new 31	java/io/File
    //   9: dup
    //   10: aload_0
    //   11: getfield 21	com/truecaller/analytics/storage/a/a:a	Landroid/content/Context;
    //   14: invokevirtual 37	android/content/Context:getFilesDir	()Ljava/io/File;
    //   17: ldc 39
    //   19: invokespecial 42	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   22: astore_2
    //   23: aload_2
    //   24: invokevirtual 46	java/io/File:exists	()Z
    //   27: ifne +4 -> 31
    //   30: return
    //   31: new 48	com/truecaller/analytics/storage/a/b
    //   34: dup
    //   35: aload_2
    //   36: invokespecial 51	com/truecaller/analytics/storage/a/b:<init>	(Ljava/io/File;)V
    //   39: invokevirtual 54	com/truecaller/analytics/storage/a/b:a	()Lcom/truecaller/analytics/storage/a/b$d;
    //   42: astore_3
    //   43: aload_3
    //   44: ifnonnull +9 -> 53
    //   47: aload_2
    //   48: invokevirtual 57	java/io/File:delete	()Z
    //   51: pop
    //   52: return
    //   53: aload_3
    //   54: invokeinterface 62 1 0
    //   59: astore 4
    //   61: aload 4
    //   63: ifnonnull +9 -> 72
    //   66: aload_2
    //   67: invokevirtual 57	java/io/File:delete	()Z
    //   70: pop
    //   71: return
    //   72: new 64	android/content/ContentValues
    //   75: dup
    //   76: invokespecial 65	android/content/ContentValues:<init>	()V
    //   79: astore 5
    //   81: aload 5
    //   83: ldc 67
    //   85: aload 4
    //   87: invokevirtual 71	android/content/ContentValues:put	(Ljava/lang/String;[B)V
    //   90: aload_1
    //   91: ldc 73
    //   93: iconst_0
    //   94: aload 5
    //   96: invokeinterface 78 4 0
    //   101: pop2
    //   102: goto -49 -> 53
    //   105: astore_1
    //   106: goto +31 -> 137
    //   109: astore_1
    //   110: aload_1
    //   111: checkcast 80	java/lang/Throwable
    //   114: invokestatic 86	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   117: aload_2
    //   118: invokevirtual 57	java/io/File:delete	()Z
    //   121: pop
    //   122: return
    //   123: astore_1
    //   124: aload_1
    //   125: checkcast 80	java/lang/Throwable
    //   128: invokestatic 86	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   131: aload_2
    //   132: invokevirtual 57	java/io/File:delete	()Z
    //   135: pop
    //   136: return
    //   137: aload_2
    //   138: invokevirtual 57	java/io/File:delete	()Z
    //   141: pop
    //   142: aload_1
    //   143: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	144	0	this	a
    //   0	144	1	paramb	android.arch.persistence.db.b
    //   22	116	2	localFile	java.io.File
    //   42	12	3	locald	b.d
    //   59	27	4	arrayOfByte	byte[]
    //   79	16	5	localContentValues	android.content.ContentValues
    // Exception table:
    //   from	to	target	type
    //   31	43	105	finally
    //   53	61	105	finally
    //   72	102	105	finally
    //   110	117	105	finally
    //   124	131	105	finally
    //   31	43	109	com/truecaller/analytics/storage/a/b$a
    //   53	61	109	com/truecaller/analytics/storage/a/b$a
    //   72	102	109	com/truecaller/analytics/storage/a/b$a
    //   31	43	123	java/io/IOException
    //   53	61	123	java/io/IOException
    //   72	102	123	java/io/IOException
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.storage.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */