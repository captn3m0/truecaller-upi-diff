package com.truecaller.analytics;

import android.app.Application;
import android.content.Context;
import android.location.LocationManager;
import android.telephony.TelephonyManager;
import com.truecaller.analytics.storage.AnalyticsDatabase;
import com.truecaller.analytics.storage.d;
import com.truecaller.utils.t;
import javax.inject.Provider;

public final class aa
  implements as
{
  private Provider<Context> a;
  private Provider<com.truecaller.analytics.a.f> b;
  private Provider<Application> c;
  private Provider<b> d;
  private Provider<com.truecaller.analytics.storage.a> e;
  private Provider<com.truecaller.analytics.a.b> f;
  private Provider<com.truecaller.analytics.a.a> g;
  private Provider<s> h;
  private Provider<AnalyticsDatabase> i;
  private Provider<d> j;
  private Provider<aj> k;
  private Provider<com.truecaller.utils.i> l;
  private Provider<LocationManager> m;
  private Provider<an> n;
  private Provider<com.truecaller.analytics.a.e> o;
  private Provider<TelephonyManager> p;
  private Provider<ag> q;
  private Provider<ae> r;
  private Provider<com.truecaller.androidactors.k> s;
  private Provider<Integer> t;
  private Provider<com.truecaller.androidactors.i> u;
  private Provider<com.truecaller.androidactors.f<ae>> v;
  
  private aa(f paramf, t paramt, Application paramApplication, Context paramContext, Integer paramInteger, s params)
  {
    a = dagger.a.e.a(paramContext);
    b = dagger.a.c.a(m.a(paramf, a));
    c = dagger.a.e.a(paramApplication);
    d = dagger.a.c.a(i.a(paramf, c));
    e = dagger.a.c.a(p.a(paramf, a));
    f = com.truecaller.analytics.a.c.a(e);
    g = dagger.a.c.a(f);
    h = dagger.a.e.a(params);
    i = dagger.a.c.a(j.a(paramf, a));
    j = dagger.a.c.a(o.a(paramf, i));
    k = ak.a(e, j);
    l = new b(paramt);
    m = n.a(paramf, a);
    n = ao.a(a, m);
    o = k.a(paramf, e, a);
    p = q.a(paramf, a);
    q = ah.a(h, e, j, k, l, n, o, p, d);
    r = dagger.a.c.a(q);
    s = dagger.a.c.a(h.a(paramf));
    t = dagger.a.e.a(paramInteger);
    u = g.a(paramf, a, s, t);
    v = dagger.a.c.a(l.a(paramf, r, u));
  }
  
  public static as.a a()
  {
    return new a((byte)0);
  }
  
  public final com.truecaller.analytics.a.f b()
  {
    return (com.truecaller.analytics.a.f)b.get();
  }
  
  public final b c()
  {
    return (b)d.get();
  }
  
  public final com.truecaller.analytics.a.a d()
  {
    return (com.truecaller.analytics.a.a)g.get();
  }
  
  public final com.truecaller.analytics.storage.a e()
  {
    return (com.truecaller.analytics.storage.a)e.get();
  }
  
  public final com.truecaller.androidactors.f<ae> f()
  {
    return (com.truecaller.androidactors.f)v.get();
  }
  
  public final ae g()
  {
    return (ae)r.get();
  }
  
  static final class a
    implements as.a
  {
    private f a;
    private t b;
    private Application c;
    private Context d;
    private Integer e;
    private s f;
  }
  
  static final class b
    implements Provider<com.truecaller.utils.i>
  {
    private final t a;
    
    b(t paramt)
    {
      a = paramt;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.aa
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */