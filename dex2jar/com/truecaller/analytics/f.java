package com.truecaller.analytics;

import android.app.Application;
import android.arch.persistence.room.f.b;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.telephony.TelephonyManager;
import c.u;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.truecaller.analytics.a.g;
import com.truecaller.analytics.storage.AnalyticsDatabase;
import com.truecaller.analytics.storage.d;
import com.truecaller.androidactors.i;
import javax.inject.Named;

public final class f
{
  public static final a a = new a((byte)0);
  
  public static LocationManager a(Context paramContext)
  {
    c.g.b.k.b(paramContext, "context");
    paramContext = paramContext.getSystemService("location");
    if (paramContext != null) {
      return (LocationManager)paramContext;
    }
    throw new u("null cannot be cast to non-null type android.location.LocationManager");
  }
  
  public static com.truecaller.analytics.a.e a(com.truecaller.analytics.storage.a parama, Context paramContext)
  {
    c.g.b.k.b(parama, "settings");
    c.g.b.k.b(paramContext, "context");
    paramContext = paramContext.getContentResolver();
    c.g.b.k.a(paramContext, "context.contentResolver");
    return new com.truecaller.analytics.a.e(parama, paramContext);
  }
  
  public static b a(Application paramApplication)
  {
    c.g.b.k.b(paramApplication, "application");
    return (b)new al(paramApplication);
  }
  
  public static d a(AnalyticsDatabase paramAnalyticsDatabase)
  {
    c.g.b.k.b(paramAnalyticsDatabase, "database");
    return paramAnalyticsDatabase.h();
  }
  
  public static com.truecaller.androidactors.f<ae> a(ae paramae, @Named("analytics") i parami)
  {
    c.g.b.k.b(paramae, "tracker");
    c.g.b.k.b(parami, "thread");
    paramae = parami.a(ae.class, paramae);
    c.g.b.k.a(paramae, "thread.bind(EventsTracker::class.java, tracker)");
    return paramae;
  }
  
  @Named("analytics")
  public static i a(Context paramContext, @Named("analytics") com.truecaller.androidactors.k paramk, @Named("analytics_job_id") int paramInt)
  {
    c.g.b.k.b(paramContext, "context");
    c.g.b.k.b(paramk, "actors");
    paramContext = paramk.a(paramContext, EventsTrackerService.class, paramInt);
    c.g.b.k.a(paramContext, "actors.createThread(cont…::class.java, actorJobId)");
    return paramContext;
  }
  
  @Named("analytics")
  public static com.truecaller.androidactors.k a()
  {
    com.truecaller.androidactors.k localk = new c().a();
    c.g.b.k.a(localk, "AnalyticsActorsBuilder().build()");
    return localk;
  }
  
  public static AnalyticsDatabase b(Context paramContext)
  {
    c.g.b.k.b(paramContext, "context");
    paramContext = android.arch.persistence.room.e.a(paramContext, AnalyticsDatabase.class, "analytics.db").a((f.b)new com.truecaller.analytics.storage.a.a(paramContext)).b();
    c.g.b.k.a(paramContext, "Room.databaseBuilder(con…xt))\n            .build()");
    return (AnalyticsDatabase)paramContext;
  }
  
  public static com.truecaller.analytics.a.f c(Context paramContext)
  {
    c.g.b.k.b(paramContext, "context");
    paramContext = FirebaseAnalytics.getInstance(paramContext);
    c.g.b.k.a(paramContext, "FirebaseAnalytics.getInstance(context)");
    return (com.truecaller.analytics.a.f)new g(paramContext);
  }
  
  public static com.truecaller.analytics.storage.a d(Context paramContext)
  {
    c.g.b.k.b(paramContext, "context");
    Object localObject = paramContext.getSharedPreferences("analytics", 0);
    c.g.b.k.a(localObject, "context.getSharedPrefere…ME, Context.MODE_PRIVATE)");
    localObject = new com.truecaller.analytics.storage.b((SharedPreferences)localObject);
    ((com.truecaller.analytics.storage.b)localObject).a(paramContext);
    return (com.truecaller.analytics.storage.a)localObject;
  }
  
  public static TelephonyManager e(Context paramContext)
  {
    c.g.b.k.b(paramContext, "context");
    paramContext = paramContext.getSystemService("phone");
    if (paramContext != null) {
      return (TelephonyManager)paramContext;
    }
    throw new u("null cannot be cast to non-null type android.telephony.TelephonyManager");
  }
  
  public static final class a {}
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */