package com.truecaller.analytics;

import android.content.Context;
import com.truecaller.analytics.a.e;
import com.truecaller.analytics.storage.a;
import dagger.a.d;
import javax.inject.Provider;

public final class k
  implements d<e>
{
  private final f a;
  private final Provider<a> b;
  private final Provider<Context> c;
  
  private k(f paramf, Provider<a> paramProvider, Provider<Context> paramProvider1)
  {
    a = paramf;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static k a(f paramf, Provider<a> paramProvider, Provider<Context> paramProvider1)
  {
    return new k(paramf, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */