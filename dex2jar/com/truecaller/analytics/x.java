package com.truecaller.analytics;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;

public final class x
  implements Application.ActivityLifecycleCallbacks
{
  private final w a;
  
  public x(w paramw)
  {
    a = paramw;
  }
  
  public final void onActivityCreated(Activity paramActivity, Bundle paramBundle)
  {
    a.a(paramActivity, paramBundle);
  }
  
  public final void onActivityDestroyed(Activity paramActivity) {}
  
  public final void onActivityPaused(Activity paramActivity) {}
  
  public final void onActivityResumed(Activity paramActivity) {}
  
  public final void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle) {}
  
  public final void onActivityStarted(Activity paramActivity)
  {
    a.a(paramActivity);
  }
  
  public final void onActivityStopped(Activity paramActivity)
  {
    a.b(paramActivity);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.x
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */