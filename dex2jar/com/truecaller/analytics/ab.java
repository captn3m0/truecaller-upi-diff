package com.truecaller.analytics;

import c.g.b.k;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.a;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.inject.Inject;

public final class ab
  implements au
{
  private final HashMap<String, ab.a> a;
  private final a b;
  private final b c;
  
  @Inject
  public ab(a parama, b paramb)
  {
    b = parama;
    c = paramb;
    a = new HashMap();
  }
  
  private static String a(double paramDouble)
  {
    return String.format("%.2f", new Object[] { Double.valueOf(paramDouble) });
  }
  
  private static String a(double paramDouble, long[] paramArrayOfLong)
  {
    int k = paramArrayOfLong.length;
    int i = 0;
    while (i < k)
    {
      long l = paramArrayOfLong[i];
      int j;
      if (paramDouble < l) {
        j = 1;
      } else {
        j = 0;
      }
      if (j != 0)
      {
        paramArrayOfLong = Long.valueOf(l);
        break label57;
      }
      i += 1;
    }
    paramArrayOfLong = null;
    label57:
    if (paramArrayOfLong != null)
    {
      String str = String.valueOf(paramArrayOfLong.longValue());
      paramArrayOfLong = str;
      if (str != null) {}
    }
    else
    {
      paramArrayOfLong = "MAX";
    }
    return paramArrayOfLong;
  }
  
  private final void a(e parame)
  {
    Object localObject2 = parame.b();
    String str6 = null;
    String str1;
    if (localObject2 != null) {
      str1 = (String)((Map)localObject2).get("Event");
    } else {
      str1 = null;
    }
    String str2;
    if (localObject2 != null) {
      str2 = (String)((Map)localObject2).get("Type");
    } else {
      str2 = null;
    }
    Object localObject1 = parame.c();
    if (localObject1 != null) {
      localObject1 = a(((Double)localObject1).doubleValue());
    } else {
      localObject1 = null;
    }
    String str3;
    if (localObject2 != null) {
      str3 = (String)((Map)localObject2).get("GranularValue");
    } else {
      str3 = null;
    }
    String str4;
    if (localObject2 != null) {
      str4 = (String)((Map)localObject2).get("Count");
    } else {
      str4 = null;
    }
    String str5;
    if (localObject2 != null) {
      str5 = (String)((Map)localObject2).get("State");
    } else {
      str5 = null;
    }
    if (localObject2 != null) {
      str6 = (String)((Map)localObject2).get("Parameters");
    }
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append(str1);
    ((StringBuilder)localObject2).append(' ');
    ((StringBuilder)localObject2).append(str2);
    ((StringBuilder)localObject2).append(' ');
    ((StringBuilder)localObject2).append((String)localObject1);
    ((StringBuilder)localObject2).append("ms [");
    ((StringBuilder)localObject2).append(str3);
    ((StringBuilder)localObject2).append("] Count:");
    ((StringBuilder)localObject2).append(str4);
    ((StringBuilder)localObject2).append(' ');
    ((StringBuilder)localObject2).append(str5);
    ((StringBuilder)localObject2).append(' ');
    ((StringBuilder)localObject2).append(str6);
    ((StringBuilder)localObject2).toString();
    c.a(parame);
  }
  
  public final String a(TimingEvent paramTimingEvent, String paramString1, String paramString2)
  {
    k.b(paramTimingEvent, "event");
    String str;
    if (paramTimingEvent.getUnique())
    {
      str = paramTimingEvent.getEvent();
    }
    else
    {
      str = UUID.randomUUID().toString();
      k.a(str, "UUID.randomUUID().toString()");
    }
    ((Map)a).put(str, new ab.a(paramTimingEvent, paramString1, paramString2, b.c()));
    return str;
  }
  
  public final void a(TimingEvent paramTimingEvent)
  {
    k.b(paramTimingEvent, "event");
    AssertionUtil.isTrue(paramTimingEvent.getUnique(), new String[] { "Only unique events can be finished without passing key" });
    a(paramTimingEvent.getEvent(), 0);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "key");
    a.remove(paramString);
  }
  
  public final void a(String paramString, int paramInt)
  {
    k.b(paramString, "key");
    long l = b.c();
    paramString = (ab.a)a.remove(paramString);
    if (paramString != null)
    {
      double d1 = l - d;
      Double.isNaN(d1);
      d1 /= 1000000.0D;
      Object localObject1 = new e.a("Timing");
      ((e.a)localObject1).a("Event", a.getEvent());
      ((e.a)localObject1).a("Type", "Full");
      Object localObject2 = b;
      if (localObject2 != null) {
        ((e.a)localObject1).a("State", (String)localObject2);
      }
      localObject2 = c;
      if (localObject2 != null) {
        ((e.a)localObject1).a("Parameters", (String)localObject2);
      }
      if (paramInt > 0) {
        ((e.a)localObject1).a("Count", paramInt);
      }
      localObject2 = a.getEventGranularity();
      if (localObject2 != null) {
        ((e.a)localObject1).a("GranularValue", a(d1, (long[])localObject2));
      }
      ((e.a)localObject1).a(Double.valueOf(d1));
      localObject1 = ((e.a)localObject1).a();
      k.a(localObject1, "with(AnalyticsEvent.Buil…    build()\n            }");
      a((e)localObject1);
      if (paramInt > 0)
      {
        double d2 = paramInt;
        Double.isNaN(d2);
        d1 /= d2;
        localObject1 = new e.a("Timing");
        ((e.a)localObject1).a("Event", a.getEvent());
        ((e.a)localObject1).a("Type", "PerItem");
        localObject2 = b;
        if (localObject2 != null) {
          ((e.a)localObject1).a("State", (String)localObject2);
        }
        localObject2 = c;
        if (localObject2 != null) {
          ((e.a)localObject1).a("Parameters", (String)localObject2);
        }
        paramString = a.getItemGranularity();
        if (paramString != null) {
          ((e.a)localObject1).a("GranularValue", a(d1, paramString));
        }
        ((e.a)localObject1).a(Double.valueOf(d1));
        paramString = ((e.a)localObject1).a();
        k.a(paramString, "with(AnalyticsEvent.Buil…build()\n                }");
        a(paramString);
      }
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.ab
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */