package com.truecaller.analytics;

import android.content.Context;
import android.location.LocationManager;
import dagger.a.d;
import javax.inject.Provider;

public final class ao
  implements d<an>
{
  private final Provider<Context> a;
  private final Provider<LocationManager> b;
  
  private ao(Provider<Context> paramProvider, Provider<LocationManager> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static ao a(Provider<Context> paramProvider, Provider<LocationManager> paramProvider1)
  {
    return new ao(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.ao
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */