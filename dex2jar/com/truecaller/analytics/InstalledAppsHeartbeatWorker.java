package com.truecaller.analytics;

import android.content.Context;
import android.content.pm.PackageManager;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import c.a.m;
import c.g.b.k;
import c.m.l;
import com.truecaller.TrueApp;
import com.truecaller.ads.installedapps.g;
import com.truecaller.ads.installedapps.h;
import com.truecaller.bp;
import com.truecaller.common.background.TrackedWorker;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.tracking.events.av;
import com.truecaller.tracking.events.t;
import com.truecaller.tracking.events.t.a;
import e.r;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;

public final class InstalledAppsHeartbeatWorker
  extends TrackedWorker
{
  public static final InstalledAppsHeartbeatWorker.a f = new InstalledAppsHeartbeatWorker.a((byte)0);
  @Inject
  public b b;
  @Inject
  public ae c;
  @Inject
  public com.truecaller.featuretoggles.e d;
  @Inject
  public com.truecaller.ads.installedapps.e e;
  private final Context g;
  
  public InstalledAppsHeartbeatWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    g = paramContext;
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  private final void a(List<com.truecaller.ads.installedapps.d> paramList, List<? extends CharSequence> paramList1)
  {
    Object localObject = (Iterable)paramList;
    paramList = (Collection)new ArrayList(m.a((Iterable)localObject, 10));
    localObject = ((Iterable)localObject).iterator();
    while (((Iterator)localObject).hasNext())
    {
      com.truecaller.ads.installedapps.d locald = (com.truecaller.ads.installedapps.d)((Iterator)localObject).next();
      paramList.add(new av((CharSequence)a, (CharSequence)b, (CharSequence)String.valueOf(c), (CharSequence)String.valueOf(d), (CharSequence)String.valueOf(e)));
    }
    paramList = (List)paramList;
    paramList = t.b().a(paramList).b(paramList1).a();
    paramList1 = c;
    if (paramList1 == null) {
      k.a("eventsTracker");
    }
    paramList1.a((org.apache.a.d.d)paramList);
  }
  
  public final b b()
  {
    b localb = b;
    if (localb == null) {
      k.a("analytics");
    }
    return localb;
  }
  
  public final boolean c()
  {
    if (TrueApp.y().p())
    {
      com.truecaller.featuretoggles.e locale = d;
      if (locale == null) {
        k.a("featuresRegistry");
      }
      if (c.a(locale, com.truecaller.featuretoggles.e.a[8]).a()) {
        return true;
      }
    }
    return false;
  }
  
  public final ListenableWorker.a d()
  {
    for (;;)
    {
      try
      {
        Object localObject1 = h.a;
        localObject1 = h.a().c();
        if (localObject1 == null)
        {
          localObject1 = ListenableWorker.a.c();
          k.a(localObject1, "Result.failure()");
          return (ListenableWorker.a)localObject1;
        }
        int i;
        if (!((r)localObject1).d())
        {
          if (((r)localObject1).b() == 404)
          {
            localObject1 = ListenableWorker.a.a();
            k.a(localObject1, "Result.success()");
            return (ListenableWorker.a)localObject1;
          }
          i = ((r)localObject1).b();
          if ((500 <= i) && (599 >= i))
          {
            localObject1 = ListenableWorker.a.b();
            k.a(localObject1, "Result.retry()");
            return (ListenableWorker.a)localObject1;
          }
          localObject1 = ListenableWorker.a.c();
          k.a(localObject1, "Result.failure()");
          return (ListenableWorker.a)localObject1;
        }
        Object localObject2 = (g)((r)localObject1).e();
        if (localObject2 == null)
        {
          localObject1 = ListenableWorker.a.a();
          k.a(localObject1, "Result.success()");
          return (ListenableWorker.a)localObject1;
        }
        if (a <= 0)
        {
          localObject1 = ListenableWorker.a.a();
          k.a(localObject1, "Result.success()");
          return (ListenableWorker.a)localObject1;
        }
        localObject1 = c;
        if (localObject1 == null)
        {
          localObject1 = ListenableWorker.a.a();
          k.a(localObject1, "Result.success()");
          return (ListenableWorker.a)localObject1;
        }
        Object localObject3 = g.getPackageManager();
        if (localObject3 != null)
        {
          i = 0;
          localObject3 = ((PackageManager)localObject3).getInstalledPackages(0);
          if (localObject3 != null)
          {
            localObject1 = l.d(l.c(l.b(m.n((Iterable)localObject3), (c.g.a.b)new InstalledAppsHeartbeatWorker.b((List)localObject1)), (c.g.a.b)InstalledAppsHeartbeatWorker.c.a));
            if (localObject1 != null)
            {
              if (((List)localObject1).isEmpty()) {
                break label767;
              }
              break label769;
              localObject3 = e;
              if (localObject3 == null) {
                k.a("installedPackagesDao");
              }
              Object localObject4 = ((com.truecaller.ads.installedapps.e)localObject3).a();
              Object localObject5 = (Iterable)localObject1;
              localObject3 = (Collection)new ArrayList();
              localObject5 = ((Iterable)localObject5).iterator();
              Object localObject6;
              if (((Iterator)localObject5).hasNext())
              {
                localObject6 = ((Iterator)localObject5).next();
                if (((List)localObject4).contains((com.truecaller.ads.installedapps.d)localObject6)) {
                  continue;
                }
                ((Collection)localObject3).add(localObject6);
                continue;
              }
              localObject3 = m.d((Iterable)localObject3, a);
              localObject5 = (Iterable)localObject1;
              localObject1 = (Collection)new ArrayList(m.a((Iterable)localObject5, 10));
              localObject5 = ((Iterable)localObject5).iterator();
              if (((Iterator)localObject5).hasNext())
              {
                ((Collection)localObject1).add(nexta);
                continue;
              }
              localObject1 = (List)localObject1;
              localObject5 = (Iterable)localObject4;
              localObject4 = (Collection)new ArrayList();
              localObject5 = ((Iterable)localObject5).iterator();
              if (((Iterator)localObject5).hasNext())
              {
                localObject6 = ((Iterator)localObject5).next();
                if (((List)localObject1).contains(a)) {
                  continue;
                }
                ((Collection)localObject4).add(localObject6);
                continue;
              }
              localObject1 = (List)localObject4;
              localObject4 = e;
              if (localObject4 == null) {
                k.a("installedPackagesDao");
              }
              ((com.truecaller.ads.installedapps.e)localObject4).a((List)localObject3);
              localObject4 = e;
              if (localObject4 == null) {
                k.a("installedPackagesDao");
              }
              ((com.truecaller.ads.installedapps.e)localObject4).b((List)localObject1);
              localObject2 = ((Iterable)m.e((Iterable)localObject3, b)).iterator();
              if (((Iterator)localObject2).hasNext())
              {
                localObject3 = (List)((Iterator)localObject2).next();
                if (i == 0)
                {
                  localObject5 = (Iterable)localObject1;
                  localObject4 = (Collection)new ArrayList(m.a((Iterable)localObject5, 10));
                  localObject5 = ((Iterable)localObject5).iterator();
                  if (((Iterator)localObject5).hasNext())
                  {
                    ((Collection)localObject4).add(nexta);
                    continue;
                  }
                  a((List)localObject3, (List)localObject4);
                  i = 1;
                  continue;
                }
                a((List)localObject3, null);
                continue;
              }
              localObject1 = ListenableWorker.a.a();
              k.a(localObject1, "Result.success()");
              return (ListenableWorker.a)localObject1;
            }
          }
        }
        localObject1 = ListenableWorker.a.a();
        k.a(localObject1, "Result.success()");
        return (ListenableWorker.a)localObject1;
      }
      catch (Exception localException)
      {
        if ((!(localException instanceof RuntimeException)) && (!(localException instanceof IOException))) {
          com.truecaller.log.d.a((Throwable)localException);
        }
        locala = ListenableWorker.a.c();
        k.a(locala, "Result.failure()");
        return locala;
      }
      label767:
      ListenableWorker.a locala = null;
      label769:
      if (locala != null) {}
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.InstalledAppsHeartbeatWorker
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */