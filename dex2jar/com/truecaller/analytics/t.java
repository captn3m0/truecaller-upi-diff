package com.truecaller.analytics;

import c.g.b.k;
import com.truecaller.common.account.r;
import com.truecaller.common.g.a;
import com.truecaller.common.network.util.KnownEndpoints;
import okhttp3.u;
import okhttp3.u.a;

public final class t
  implements s
{
  public CharSequence a;
  public r b;
  public a c;
  private final CharSequence d;
  private final CharSequence e;
  
  public t(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
  {
    d = paramCharSequence1;
    e = paramCharSequence2;
  }
  
  public final CharSequence a()
  {
    return d;
  }
  
  public final CharSequence b()
  {
    return e;
  }
  
  public final CharSequence c()
  {
    CharSequence localCharSequence = a;
    if (localCharSequence == null) {
      k.a("appBuild");
    }
    return localCharSequence;
  }
  
  public final long d()
  {
    a locala = c;
    if (locala == null) {
      k.a("coreSettings");
    }
    return locala.a("profileUserId", -1L);
  }
  
  public final u e()
  {
    u localu = KnownEndpoints.BATCHLOG.url().j().c("/v5/events").b();
    k.a(localu, "KnownEndpoints.BATCHLOG.…ts\")\n            .build()");
    return localu;
  }
  
  public final okhttp3.t f()
  {
    Object localObject = b;
    if (localObject == null) {
      k.a("accountManager");
    }
    localObject = ((r)localObject).e();
    if (localObject == null) {
      return null;
    }
    return okhttp3.t.a(new String[] { "Authorization", "Bearer ".concat(String.valueOf(localObject)) });
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.t
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */