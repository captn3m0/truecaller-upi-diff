package com.truecaller.analytics;

import java.util.HashMap;
import java.util.Map;

public abstract interface e
{
  public abstract String a();
  
  public abstract Map<String, String> b();
  
  public abstract Double c();
  
  public static final class a
  {
    public Double a;
    private final String b;
    private Map<String, String> c;
    
    public a(String paramString)
    {
      b = paramString;
    }
    
    public final a a(Double paramDouble)
    {
      a = paramDouble;
      return this;
    }
    
    public final a a(String paramString, int paramInt)
    {
      return a(paramString, String.valueOf(paramInt));
    }
    
    public final a a(String paramString, long paramLong)
    {
      return a(paramString, String.valueOf(paramLong));
    }
    
    public final a a(String paramString1, String paramString2)
    {
      if (c == null) {
        c = new HashMap();
      }
      c.put(paramString1, paramString2);
      return this;
    }
    
    public final a a(String paramString, boolean paramBoolean)
    {
      return a(paramString, String.valueOf(paramBoolean));
    }
    
    public final e a()
    {
      return new e.a.a(b, a, c, (byte)0);
    }
    
    public final void a(String paramString)
    {
      Map localMap = c;
      if (localMap != null) {
        localMap.remove(paramString);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */