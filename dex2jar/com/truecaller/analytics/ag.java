package com.truecaller.analytics;

import android.database.sqlite.SQLiteException;
import android.location.Location;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import c.f;
import com.truecaller.analytics.a.e;
import com.truecaller.analytics.storage.c;
import com.truecaller.log.AssertionUtil;
import com.truecaller.tracking.events.a.a;
import com.truecaller.tracking.events.ak;
import com.truecaller.tracking.events.ak.a;
import com.truecaller.tracking.events.am.a;
import com.truecaller.utils.i;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;

final class ag
  implements ae
{
  private final s a;
  private final com.truecaller.analytics.storage.a b;
  private final com.truecaller.analytics.storage.d c;
  private final ai d;
  private final i e;
  private final am f;
  private final e g;
  private final TelephonyManager h;
  private final b i;
  private long j = 0L;
  
  @Inject
  ag(s params, com.truecaller.analytics.storage.a parama, com.truecaller.analytics.storage.d paramd, ai paramai, i parami, am paramam, e parame, TelephonyManager paramTelephonyManager, b paramb)
  {
    a = params;
    b = parama;
    c = paramd;
    d = paramai;
    e = parami;
    f = paramam;
    h = paramTelephonyManager;
    g = parame;
    i = paramb;
  }
  
  private void a(u paramu, Exception paramException)
  {
    if (((paramException instanceof IOException)) && (!e.a())) {
      paramu.a("NoInternet");
    } else {
      paramu.a(paramException);
    }
    AssertionUtil.reportThrowableButNeverCrash(paramException);
  }
  
  private void a(byte[] paramArrayOfByte)
  {
    try
    {
      c.a(new c(0L, paramArrayOfByte));
      return;
    }
    catch (SQLiteException paramArrayOfByte)
    {
      AssertionUtil.reportThrowableButNeverCrash(paramArrayOfByte);
    }
  }
  
  private byte[] b(org.apache.a.d.d paramd)
  {
    long l1 = a.d();
    Object localObject1;
    if (l1 == -1L)
    {
      localObject1 = new StringBuilder("Event ");
      ((StringBuilder)localObject1).append(paramd.a().c());
      ((StringBuilder)localObject1).append(" skipped due empty user id");
      ((StringBuilder)localObject1).toString();
      return null;
    }
    try
    {
      localObject1 = com.truecaller.tracking.events.a.b().a(a.a()).c(a.c()).b(a.b()).a();
      localObject2 = ak.b();
      if (j == 0L) {
        j = b.a("analyticsLastEventId", System.currentTimeMillis());
      }
      com.truecaller.analytics.storage.a locala1 = b;
      long l2 = j + 1L;
      j = l2;
      locala1.b("analyticsLastEventId", l2);
      localObject2 = ((ak.a)localObject2).a(j).b(System.currentTimeMillis()).a(String.valueOf(l1)).b((String)g.b.b()).a((com.truecaller.tracking.events.a)localObject1).c(e.b()).d(h.getNetworkOperatorName());
      localObject1 = f.a();
      if (localObject1 == null) {
        localObject1 = null;
      } else {
        localObject1 = com.truecaller.tracking.events.am.b().a((float)((Location)localObject1).getLatitude()).b((float)((Location)localObject1).getLongitude()).a(TimeUnit.NANOSECONDS.toMillis(SystemClock.elapsedRealtimeNanos() - ((Location)localObject1).getElapsedRealtimeNanos())).a();
      }
      localObject1 = ad.a(((ak.a)localObject2).a((com.truecaller.tracking.events.am)localObject1).a(), paramd);
      return (byte[])localObject1;
    }
    catch (IOException localIOException) {}catch (org.apache.a.a locala) {}
    Object localObject2 = new StringBuilder("Event ");
    ((StringBuilder)localObject2).append(paramd.a().c());
    ((StringBuilder)localObject2).append(" skipped due encoding exception");
    ((StringBuilder)localObject2).toString();
    AssertionUtil.reportThrowableButNeverCrash(locala);
    return null;
  }
  
  /* Error */
  public final com.truecaller.androidactors.w<Boolean> a(okhttp3.y paramy)
  {
    // Byte code:
    //   0: new 66	com/truecaller/analytics/u
    //   3: dup
    //   4: ldc_w 281
    //   7: invokespecial 282	com/truecaller/analytics/u:<init>	(Ljava/lang/String;)V
    //   10: astore_2
    //   11: aload_0
    //   12: getfield 42	com/truecaller/analytics/ag:d	Lcom/truecaller/analytics/ai;
    //   15: aload_0
    //   16: getfield 36	com/truecaller/analytics/ag:a	Lcom/truecaller/analytics/s;
    //   19: aload_1
    //   20: aload_2
    //   21: invokeinterface 287 4 0
    //   26: invokestatic 292	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   29: invokestatic 297	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   32: astore_1
    //   33: aload_0
    //   34: getfield 52	com/truecaller/analytics/ag:i	Lcom/truecaller/analytics/b;
    //   37: aload_2
    //   38: invokevirtual 300	com/truecaller/analytics/u:a	()Lcom/truecaller/analytics/e;
    //   41: invokeinterface 305 2 0
    //   46: aload_1
    //   47: areturn
    //   48: astore_1
    //   49: goto +60 -> 109
    //   52: astore_1
    //   53: aload_2
    //   54: aload_1
    //   55: invokevirtual 72	com/truecaller/analytics/u:a	(Ljava/lang/Exception;)Lcom/truecaller/analytics/e$a;
    //   58: pop
    //   59: aload_1
    //   60: invokestatic 78	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   63: aload_1
    //   64: athrow
    //   65: astore_1
    //   66: aload_0
    //   67: aload_2
    //   68: aload_1
    //   69: invokespecial 307	com/truecaller/analytics/ag:a	(Lcom/truecaller/analytics/u;Ljava/lang/Exception;)V
    //   72: aload_0
    //   73: getfield 52	com/truecaller/analytics/ag:i	Lcom/truecaller/analytics/b;
    //   76: aload_2
    //   77: invokevirtual 300	com/truecaller/analytics/u:a	()Lcom/truecaller/analytics/e;
    //   80: invokeinterface 305 2 0
    //   85: goto +17 -> 102
    //   88: astore_1
    //   89: aload_1
    //   90: invokestatic 78	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   93: aload_2
    //   94: aload_1
    //   95: invokevirtual 72	com/truecaller/analytics/u:a	(Ljava/lang/Exception;)Lcom/truecaller/analytics/e$a;
    //   98: pop
    //   99: goto -27 -> 72
    //   102: getstatic 311	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   105: invokestatic 297	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   108: areturn
    //   109: aload_0
    //   110: getfield 52	com/truecaller/analytics/ag:i	Lcom/truecaller/analytics/b;
    //   113: aload_2
    //   114: invokevirtual 300	com/truecaller/analytics/u:a	()Lcom/truecaller/analytics/e;
    //   117: invokeinterface 305 2 0
    //   122: aload_1
    //   123: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	124	0	this	ag
    //   0	124	1	paramy	okhttp3.y
    //   10	104	2	localu	u
    // Exception table:
    //   from	to	target	type
    //   11	33	48	finally
    //   53	65	48	finally
    //   66	72	48	finally
    //   89	99	48	finally
    //   11	33	52	java/lang/Exception
    //   11	33	65	java/io/IOException
    //   11	33	88	android/database/sqlite/SQLiteException
  }
  
  /* Error */
  public final com.truecaller.androidactors.w<EventsUploadResult> a(org.apache.a.d.d paramd, okhttp3.y paramy)
  {
    // Byte code:
    //   0: new 66	com/truecaller/analytics/u
    //   3: dup
    //   4: ldc_w 316
    //   7: invokespecial 282	com/truecaller/analytics/u:<init>	(Ljava/lang/String;)V
    //   10: astore 5
    //   12: aload_0
    //   13: aload_1
    //   14: invokespecial 318	com/truecaller/analytics/ag:b	(Lorg/apache/a/d/d;)[B
    //   17: astore 6
    //   19: aload 6
    //   21: ifnonnull +35 -> 56
    //   24: aload 5
    //   26: ldc_w 320
    //   29: invokevirtual 69	com/truecaller/analytics/u:a	(Ljava/lang/String;)Lcom/truecaller/analytics/e$a;
    //   32: pop
    //   33: getstatic 326	com/truecaller/analytics/EventsUploadResult:FAILURE	Lcom/truecaller/analytics/EventsUploadResult;
    //   36: invokestatic 297	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   39: astore_1
    //   40: aload_0
    //   41: getfield 52	com/truecaller/analytics/ag:i	Lcom/truecaller/analytics/b;
    //   44: aload 5
    //   46: invokevirtual 300	com/truecaller/analytics/u:a	()Lcom/truecaller/analytics/e;
    //   49: invokeinterface 305 2 0
    //   54: aload_1
    //   55: areturn
    //   56: iconst_0
    //   57: istore_3
    //   58: aload 6
    //   60: invokestatic 329	com/truecaller/analytics/ad:a	([B)Lcom/truecaller/tracking/events/EventRecordVersionedV2;
    //   63: astore 7
    //   65: new 331	java/util/ArrayList
    //   68: dup
    //   69: invokespecial 332	java/util/ArrayList:<init>	()V
    //   72: astore 8
    //   74: aload 8
    //   76: aload 7
    //   78: invokevirtual 336	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   81: pop
    //   82: aload_0
    //   83: getfield 42	com/truecaller/analytics/ag:d	Lcom/truecaller/analytics/ai;
    //   86: aload_0
    //   87: getfield 36	com/truecaller/analytics/ag:a	Lcom/truecaller/analytics/s;
    //   90: aload_2
    //   91: aload 8
    //   93: aload 5
    //   95: invokeinterface 339 5 0
    //   100: istore 4
    //   102: iload 4
    //   104: istore_3
    //   105: goto +56 -> 161
    //   108: astore_2
    //   109: goto +4 -> 113
    //   112: astore_2
    //   113: new 103	java/lang/StringBuilder
    //   116: dup
    //   117: ldc 105
    //   119: invokespecial 108	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   122: astore 7
    //   124: aload 7
    //   126: aload_1
    //   127: invokeinterface 113 1 0
    //   132: invokevirtual 118	org/apache/a/d:c	()Ljava/lang/String;
    //   135: invokevirtual 122	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   138: pop
    //   139: aload 7
    //   141: ldc_w 341
    //   144: invokevirtual 122	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   147: pop
    //   148: aload 7
    //   150: invokevirtual 127	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   153: pop
    //   154: aload_0
    //   155: aload 5
    //   157: aload_2
    //   158: invokespecial 307	com/truecaller/analytics/ag:a	(Lcom/truecaller/analytics/u;Ljava/lang/Exception;)V
    //   161: iload_3
    //   162: ifne +32 -> 194
    //   165: aload_0
    //   166: aload 6
    //   168: invokespecial 343	com/truecaller/analytics/ag:a	([B)V
    //   171: getstatic 346	com/truecaller/analytics/EventsUploadResult:QUEUED	Lcom/truecaller/analytics/EventsUploadResult;
    //   174: invokestatic 297	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   177: astore_1
    //   178: aload_0
    //   179: getfield 52	com/truecaller/analytics/ag:i	Lcom/truecaller/analytics/b;
    //   182: aload 5
    //   184: invokevirtual 300	com/truecaller/analytics/u:a	()Lcom/truecaller/analytics/e;
    //   187: invokeinterface 305 2 0
    //   192: aload_1
    //   193: areturn
    //   194: getstatic 349	com/truecaller/analytics/EventsUploadResult:SUCCESS	Lcom/truecaller/analytics/EventsUploadResult;
    //   197: invokestatic 297	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   200: astore_1
    //   201: aload_0
    //   202: getfield 52	com/truecaller/analytics/ag:i	Lcom/truecaller/analytics/b;
    //   205: aload 5
    //   207: invokevirtual 300	com/truecaller/analytics/u:a	()Lcom/truecaller/analytics/e;
    //   210: invokeinterface 305 2 0
    //   215: aload_1
    //   216: areturn
    //   217: astore_1
    //   218: goto +17 -> 235
    //   221: astore_1
    //   222: aload 5
    //   224: aload_1
    //   225: invokevirtual 72	com/truecaller/analytics/u:a	(Ljava/lang/Exception;)Lcom/truecaller/analytics/e$a;
    //   228: pop
    //   229: aload_1
    //   230: invokestatic 78	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   233: aload_1
    //   234: athrow
    //   235: aload_0
    //   236: getfield 52	com/truecaller/analytics/ag:i	Lcom/truecaller/analytics/b;
    //   239: aload 5
    //   241: invokevirtual 300	com/truecaller/analytics/u:a	()Lcom/truecaller/analytics/e;
    //   244: invokeinterface 305 2 0
    //   249: aload_1
    //   250: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	251	0	this	ag
    //   0	251	1	paramd	org.apache.a.d.d
    //   0	251	2	paramy	okhttp3.y
    //   57	105	3	k	int
    //   100	3	4	bool	boolean
    //   10	230	5	localu	u
    //   17	150	6	arrayOfByte	byte[]
    //   63	86	7	localObject	Object
    //   72	20	8	localArrayList	java.util.ArrayList
    // Exception table:
    //   from	to	target	type
    //   58	102	108	java/io/IOException
    //   58	102	112	org/apache/a/a
    //   12	19	217	finally
    //   24	40	217	finally
    //   58	102	217	finally
    //   113	161	217	finally
    //   165	178	217	finally
    //   194	201	217	finally
    //   222	235	217	finally
    //   12	19	221	java/lang/Exception
    //   24	40	221	java/lang/Exception
    //   58	102	221	java/lang/Exception
    //   113	161	221	java/lang/Exception
    //   165	178	221	java/lang/Exception
    //   194	201	221	java/lang/Exception
  }
  
  public final void a(org.apache.a.d.d paramd)
  {
    byte[] arrayOfByte = b(paramd);
    if (arrayOfByte != null)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramd.a().c());
      localStringBuilder.append(" ");
      localStringBuilder.append(paramd.toString());
      localStringBuilder.toString();
      a(arrayOfByte);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.ag
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */