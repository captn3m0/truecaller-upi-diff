package com.truecaller.analytics;

import android.content.Context;
import com.truecaller.analytics.storage.a;
import dagger.a.d;
import javax.inject.Provider;

public final class p
  implements d<a>
{
  private final f a;
  private final Provider<Context> b;
  
  private p(f paramf, Provider<Context> paramProvider)
  {
    a = paramf;
    b = paramProvider;
  }
  
  public static p a(f paramf, Provider<Context> paramProvider)
  {
    return new p(paramf, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */