package com.truecaller.analytics;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import c.g.b.k;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.bp;
import com.truecaller.common.account.r;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.background.e;
import com.truecaller.common.h.aa;
import com.truecaller.common.h.ac;
import com.truecaller.common.h.am;
import com.truecaller.common.h.c;
import com.truecaller.log.AssertionUtil;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.h;
import com.truecaller.old.data.access.Settings;
import com.truecaller.tracking.events.aj;
import com.truecaller.tracking.events.aj.a;
import com.truecaller.tracking.events.an;
import com.truecaller.tracking.events.an.a;
import com.truecaller.tracking.events.ap;
import com.truecaller.tracking.events.ap.a;
import com.truecaller.tracking.events.aq;
import com.truecaller.tracking.events.aq.a;
import com.truecaller.tracking.events.ar.a;
import com.truecaller.tracking.events.as;
import com.truecaller.tracking.events.as.a;
import com.truecaller.tracking.events.au;
import com.truecaller.tracking.events.au.a;
import com.truecaller.tracking.events.aw;
import com.truecaller.tracking.events.aw.a;
import com.truecaller.tracking.events.az;
import com.truecaller.tracking.events.az.a;
import com.truecaller.tracking.events.o;
import com.truecaller.tracking.events.o.a;
import com.truecaller.utils.i;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import javax.inject.Named;
import okhttp3.y;
import org.apache.a.d.d;

public final class AppHeartBeatTask
  extends PersistentBackgroundTask
{
  public static final AppHeartBeatTask.a k = new AppHeartBeatTask.a((byte)0);
  @Inject
  public f<ae> a;
  @Inject
  public com.truecaller.calling.ar b;
  @Inject
  public i c;
  @Inject
  public h d;
  @Inject
  public com.truecaller.common.h.u e;
  @Inject
  public ac f;
  @Inject
  public r g;
  @Inject
  public b h;
  @Inject
  public c i;
  @Inject
  @Named("andlytics-network-client")
  public y j;
  
  public AppHeartBeatTask()
  {
    TrueApp localTrueApp = TrueApp.y();
    k.a(localTrueApp, "TrueApp.getApp()");
    localTrueApp.a().a(this);
  }
  
  private final o a(Context paramContext, String paramString)
  {
    o.a locala = o.b();
    Object localObject1 = paramContext.getSystemService("phone");
    Object localObject3;
    if (localObject1 != null)
    {
      localObject1 = (TelephonyManager)localObject1;
      localObject2 = locala.a((CharSequence)paramString);
      paramString = paramContext.getResources();
      k.a(paramString, "context.resources");
      paramString = paramString.getDisplayMetrics();
      localObject3 = an.b();
      ((an.a)localObject3).b((CharSequence)Build.MODEL).a((CharSequence)Build.MANUFACTURER).b(widthPixels).a(heightPixels).c(densityDpi);
      if (android.support.v4.content.b.a(paramContext, "android.permission.READ_PHONE_STATE") == 0)
      {
        paramString = paramContext.getSystemService("phone");
        if (paramString != null) {
          paramString = am.n(((TelephonyManager)paramString).getDeviceId());
        } else {
          throw new c.u("null cannot be cast to non-null type android.telephony.TelephonyManager");
        }
      }
      else
      {
        paramString = null;
      }
      k.a(localObject3, "deviceInfo");
      ((an.a)localObject3).c((CharSequence)paramString);
      paramString = ((an.a)localObject3).a();
      k.a(paramString, "deviceInfo.build()");
      ((o.a)localObject2).a(paramString).a(au.b().a((CharSequence)"Android").b((CharSequence)Build.VERSION.RELEASE).a()).a(f(paramContext)).a(g(paramContext));
      i1 = 0;
    }
    for (;;)
    {
      try
      {
        if (android.support.v4.content.b.a(paramContext, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
          k.a(locala, "heartBeat");
        }
      }
      catch (RuntimeException|SecurityException paramString)
      {
        int m;
        continue;
      }
      try
      {
        paramString = ((TelephonyManager)localObject1).getAllCellInfo();
      }
      catch (SecurityException paramString) {}
    }
    paramString = null;
    if (paramString == null)
    {
      paramString = ((TelephonyManager)localObject1).getCellLocation();
      if ((paramString instanceof GsmCellLocation))
      {
        localObject2 = ((TelephonyManager)localObject1).getNetworkOperator();
        if (localObject2 != null)
        {
          m = ((String)localObject2).length();
          if (m <= 3) {}
        }
      }
    }
    for (;;)
    {
      try
      {
        localObject3 = ((String)localObject2).substring(0, 3);
        k.a(localObject3, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        n = Integer.parseInt((String)localObject3);
      }
      catch (NumberFormatException localNumberFormatException1)
      {
        continue;
      }
      try
      {
        localObject2 = ((String)localObject2).substring(3);
        k.a(localObject2, "(this as java.lang.String).substring(startIndex)");
        m = Integer.parseInt((String)localObject2);
      }
      catch (NumberFormatException localNumberFormatException2) {}
    }
    n = 0;
    m = 0;
    break label353;
    m = 0;
    n = 0;
    label353:
    locala.a(ap.b().c(((GsmCellLocation)paramString).getCid()).d(((GsmCellLocation)paramString).getLac()).b(m).a(n).a());
    break label762;
    if ((paramString instanceof CdmaCellLocation))
    {
      locala.a(aj.b().a(((CdmaCellLocation)paramString).getBaseStationLatitude()).b(((CdmaCellLocation)paramString).getBaseStationLongitude()).a());
      break label762;
      paramString = paramString.iterator();
      do
      {
        do
        {
          do
          {
            do
            {
              do
              {
                do
                {
                  if (!paramString.hasNext()) {
                    break;
                  }
                  localObject2 = (CellInfo)paramString.next();
                } while (!((CellInfo)localObject2).isRegistered());
                if (!(localObject2 instanceof CellInfoGsm)) {
                  break;
                }
                localObject2 = ((CellInfoGsm)localObject2).getCellIdentity();
              } while (localObject2 == null);
              locala.a(ap.b().a(((CellIdentityGsm)localObject2).getMcc()).b(((CellIdentityGsm)localObject2).getMnc()).d(((CellIdentityGsm)localObject2).getLac()).c(((CellIdentityGsm)localObject2).getCid()).a());
              break label762;
              if (!(localObject2 instanceof CellInfoLte)) {
                break;
              }
              localObject2 = ((CellInfoLte)localObject2).getCellIdentity();
            } while (localObject2 == null);
            locala.a(com.truecaller.tracking.events.ar.b().a(((CellIdentityLte)localObject2).getMcc()).b(((CellIdentityLte)localObject2).getMnc()).c(((CellIdentityLte)localObject2).getCi()).d(((CellIdentityLte)localObject2).getTac()).a());
            break label762;
            if (!(localObject2 instanceof CellInfoCdma)) {
              break;
            }
            localObject2 = ((CellInfoCdma)localObject2).getCellIdentity();
          } while (localObject2 == null);
          locala.a(aj.b().a(((CellIdentityCdma)localObject2).getLongitude()).b(((CellIdentityCdma)localObject2).getLatitude()).a());
          break;
        } while (!(localObject2 instanceof CellInfoWcdma));
        localObject2 = ((CellInfoWcdma)localObject2).getCellIdentity();
        if (localObject2 == null)
        {
          m = 0;
        }
        else
        {
          locala.a(ap.b().a(((CellIdentityWcdma)localObject2).getMcc()).b(((CellIdentityWcdma)localObject2).getMnc()).d(((CellIdentityWcdma)localObject2).getLac()).c(((CellIdentityWcdma)localObject2).getCid()).a());
          m = 1;
        }
      } while (m == 0);
    }
    try
    {
      label762:
      paramContext = AdvertisingIdClient.getAdvertisingIdInfo(paramContext);
      if ((paramContext != null) && (!paramContext.isLimitAdTrackingEnabled())) {
        paramContext = am.n(paramContext.getId());
      }
    }
    catch (GooglePlayServicesNotAvailableException|GooglePlayServicesRepairableException|IOException|SecurityException paramContext)
    {
      for (;;) {}
    }
    paramContext = null;
    k.a(locala, "heartBeat");
    locala.b((CharSequence)paramContext);
    Object localObject2 = new ArrayList();
    paramContext = d;
    if (paramContext == null) {
      k.a("multiSimManager");
    }
    if (paramContext.j())
    {
      locala.a(2);
      paramContext = d;
      if (paramContext == null) {
        k.a("multiSimManager");
      }
      paramContext = paramContext.h();
      k.a(paramContext, "multiSimManager.allSimInfos");
      paramString = b;
      if (paramString == null) {
        k.a("simSelectionHelper");
      }
      localObject1 = paramString.e();
      localObject3 = paramContext.iterator();
      while (((Iterator)localObject3).hasNext())
      {
        paramContext = (SimInfo)((Iterator)localObject3).next();
        az.a locala1 = az.b();
        k.a(locala1, "simInfo");
        locala1.a((CharSequence)am.n(d));
        if (am.f((CharSequence)e) >= 4)
        {
          paramString = e;
          k.a(paramString, "info.mccMnc");
          if (paramString != null)
          {
            paramString = paramString.substring(0, 3);
            k.a(paramString, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            locala1.a(am.f(paramString));
            paramString = e;
            k.a(paramString, "info.mccMnc");
            if (paramString != null)
            {
              paramString = paramString.substring(3);
              k.a(paramString, "(this as java.lang.String).substring(startIndex)");
              locala1.b(am.f(paramString));
            }
            else
            {
              throw new c.u("null cannot be cast to non-null type java.lang.String");
            }
          }
          else
          {
            throw new c.u("null cannot be cast to non-null type java.lang.String");
          }
        }
        else
        {
          locala1.a(0);
          locala1.b(0);
        }
        locala1.a(k.a(b, localObject1));
        locala1.a();
        if (!TextUtils.isEmpty((CharSequence)c))
        {
          paramString = e;
          if (paramString == null) {
            k.a("phoneNumberHelper");
          }
          String str = c;
          if (str == null) {
            k.a();
          }
          k.a(str, "info.phoneNumber!!");
          paramContext = b;
          k.a(paramContext, "info.simToken");
          paramString = paramString.b(str, paramContext);
          paramContext = paramString;
          if (paramString != null)
          {
            paramContext = paramString;
            if (paramString.length() > 1)
            {
              paramContext = paramString;
              if (paramString.charAt(0) == '+')
              {
                paramContext = paramString.substring(1);
                k.a(paramContext, "(this as java.lang.String).substring(startIndex)");
              }
            }
          }
          locala1.b((CharSequence)paramContext);
        }
        else
        {
          locala1.b(null);
        }
        ((ArrayList)localObject2).add(locala1.b());
      }
    }
    locala.a(1);
    paramContext = az.b();
    k.a(paramContext, "simInfo");
    paramContext.a(true);
    paramContext.a((CharSequence)((TelephonyManager)localObject1).getSimOperatorName());
    paramString = ((TelephonyManager)localObject1).getSimOperator();
    if ((!TextUtils.isEmpty((CharSequence)paramString)) && (paramString.length() >= 5)) {}
    for (;;)
    {
      try
      {
        k.a(paramString, "mccmnc");
        if (paramString != null)
        {
          localObject3 = paramString.substring(0, 3);
          k.a(localObject3, "(this as java.lang.Strin…ing(startIndex, endIndex)");
          m = Integer.parseInt((String)localObject3);
        }
      }
      catch (NumberFormatException paramString)
      {
        continue;
      }
      try
      {
        paramString = paramString.substring(3);
        k.a(paramString, "(this as java.lang.String).substring(startIndex)");
        n = Integer.parseInt(paramString);
      }
      catch (NumberFormatException paramString)
      {
        n = i1;
      }
    }
    throw new c.u("null cannot be cast to non-null type java.lang.String");
    m = 0;
    n = i1;
    break label1398;
    m = 0;
    n = i1;
    label1398:
    paramContext.a(m);
    paramContext.b(n);
    paramContext.a();
    try
    {
      paramString = ((TelephonyManager)localObject1).getLine1Number();
      if (!TextUtils.isEmpty((CharSequence)paramString)) {
        paramContext.b((CharSequence)aa.c(paramString, null));
      } else {
        paramContext.b(null);
      }
    }
    catch (SecurityException paramString)
    {
      for (;;) {}
    }
    paramContext.b(null);
    ((ArrayList)localObject2).add(paramContext.b());
    locala.a((List)localObject2);
    paramString = aw.b();
    paramContext = i;
    if (paramContext == null) {
      k.a("buildHelper");
    }
    paramContext = paramContext.g();
    if (paramContext == null) {
      paramContext = "<null>";
    }
    paramString.a((CharSequence)paramContext);
    paramContext = i;
    if (paramContext == null) {
      k.a("buildHelper");
    }
    paramContext = paramContext.i();
    if (paramContext == null) {
      paramContext = "<null>";
    }
    paramString.b((CharSequence)paramContext);
    locala.a(paramString.a());
    paramContext = locala.a();
    k.a(paramContext, "heartBeat.build()");
    return paramContext;
    throw new c.u("null cannot be cast to non-null type android.telephony.TelephonyManager");
  }
  
  private final void a(EventsUploadResult paramEventsUploadResult, Context paramContext)
  {
    if (paramEventsUploadResult != null) {
      switch (v.a[paramEventsUploadResult.ordinal()])
      {
      default: 
        break;
      case 3: 
        paramEventsUploadResult = "InvalidParams";
        break;
      case 2: 
        paramEventsUploadResult = "Queued";
        break;
      case 1: 
        paramEventsUploadResult = "Success";
        break;
      }
    }
    paramEventsUploadResult = "Failure";
    long l2 = c(paramContext);
    long l1 = 0L;
    if (l2 > 0L) {
      l1 = TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - l2);
    }
    paramContext = h;
    if (paramContext == null) {
      k.a("analytics");
    }
    paramEventsUploadResult = new e.a("AppHeartBeatTask").a("Result", paramEventsUploadResult).a("TriggerPeriodMinutes", l1).a();
    k.a(paramEventsUploadResult, "AnalyticsEvent.Builder(A…\n                .build()");
    paramContext.b(paramEventsUploadResult);
  }
  
  public static final void a(com.truecaller.common.background.b paramb)
  {
    k.b(paramb, "scheduler");
    Bundle localBundle = new Bundle();
    localBundle.putString("beatType", "upgrade");
    paramb.b(10028, localBundle);
  }
  
  public static final void b(com.truecaller.common.background.b paramb)
  {
    k.b(paramb, "scheduler");
    Bundle localBundle = new Bundle();
    localBundle.putString("beatType", "firstactivation");
    paramb.b(10028, localBundle);
  }
  
  public static final void c(com.truecaller.common.background.b paramb)
  {
    k.b(paramb, "scheduler");
    Bundle localBundle = new Bundle();
    localBundle.putString("beatType", "active");
    paramb.b(10028, localBundle);
  }
  
  private final as f(Context paramContext)
  {
    as.a locala = as.b();
    k.a(locala, "builder");
    Object localObject = c;
    if (localObject == null) {
      k.a("networkUtil");
    }
    locala.a((CharSequence)((i)localObject).b());
    paramContext = paramContext.getSystemService("phone");
    ArrayList localArrayList;
    if (paramContext != null)
    {
      locala.b((CharSequence)((TelephonyManager)paramContext).getNetworkOperatorName());
      paramContext = f;
      if (paramContext == null) {
        k.a("regionUtils");
      }
      if (paramContext.a())
      {
        paramContext = locala.a();
        k.a(paramContext, "builder.build()");
        return paramContext;
      }
      localArrayList = new ArrayList();
    }
    try
    {
      Enumeration localEnumeration1 = NetworkInterface.getNetworkInterfaces();
      while (localEnumeration1.hasMoreElements())
      {
        paramContext = (NetworkInterface)localEnumeration1.nextElement();
        k.a(paramContext, "iface");
        if ((paramContext.isUp()) && (!paramContext.isLoopback()) && (!paramContext.isVirtual()))
        {
          Enumeration localEnumeration2 = paramContext.getInetAddresses();
          while (localEnumeration2.hasMoreElements())
          {
            localObject = localEnumeration2.nextElement();
            paramContext = (Context)localObject;
            if (!(localObject instanceof Inet4Address)) {
              paramContext = null;
            }
            paramContext = (Inet4Address)paramContext;
            if (paramContext != null) {
              localArrayList.add(paramContext.getHostAddress());
            }
          }
        }
      }
    }
    catch (SocketException paramContext)
    {
      for (;;) {}
    }
    catch (NullPointerException paramContext)
    {
      for (;;) {}
    }
    if (!localArrayList.isEmpty()) {
      locala.a((List)localArrayList);
    }
    paramContext = locala.a();
    k.a(paramContext, "builder.build()");
    return paramContext;
    throw new c.u("null cannot be cast to non-null type android.telephony.TelephonyManager");
  }
  
  private static aq g(Context paramContext)
  {
    aq.a locala = aq.b();
    try
    {
      paramContext = paramContext.getPackageManager().getResourcesForApplication("android");
      k.a(paramContext, "context.packageManager\n …ForApplication(\"android\")");
      paramContext = getConfigurationlocale;
      if (paramContext == null)
      {
        k.a(locala, "language");
        locala.c((CharSequence)"unknown");
      }
      else
      {
        k.a(locala, "language");
        locala.c((CharSequence)paramContext.getLanguage());
      }
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      int m;
      for (;;) {}
    }
    k.a(locala, "language");
    locala.c((CharSequence)"unknown");
    paramContext = Settings.b("t9_lang");
    if (paramContext != null)
    {
      k.a(paramContext, "it");
      if (((CharSequence)paramContext).length() == 0) {
        m = 1;
      } else {
        m = 0;
      }
      if (m != 0) {
        paramContext = null;
      }
      if (paramContext != null)
      {
        paramContext = (CharSequence)paramContext;
        break label160;
      }
    }
    paramContext = (CharSequence)"auto";
    label160:
    locala.b(paramContext);
    if (Settings.e("languageAuto")) {
      paramContext = "auto";
    } else {
      paramContext = Settings.b("language");
    }
    locala.a((CharSequence)paramContext);
    paramContext = locala.a();
    k.a(paramContext, "language.build()");
    return paramContext;
  }
  
  public final int a()
  {
    return 10028;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    k.b(paramContext, "serviceContext");
    if (paramBundle != null) {
      paramBundle = paramBundle.getString("beatType");
    } else {
      paramBundle = null;
    }
    Object localObject = (CharSequence)paramBundle;
    int m;
    if ((localObject != null) && (((CharSequence)localObject).length() != 0)) {
      m = 0;
    } else {
      m = 1;
    }
    if (m != 0)
    {
      a(EventsUploadResult.INVALID_PARAMS, paramContext);
      return PersistentBackgroundTask.RunResult.FailedSkip;
    }
    try
    {
      localObject = a(paramContext, paramBundle);
      paramBundle = a;
      if (paramBundle == null) {
        k.a("eventsTracker");
      }
      paramBundle = (ae)paramBundle.a();
      localObject = (d)localObject;
      y localy = j;
      if (localy == null) {
        k.a("analyticsHttpClient");
      }
      paramBundle = (EventsUploadResult)paramBundle.a((d)localObject, localy).d();
      a(paramBundle, paramContext);
      if (paramBundle == EventsUploadResult.SUCCESS) {
        return PersistentBackgroundTask.RunResult.Success;
      }
      return PersistentBackgroundTask.RunResult.FailedRetry;
    }
    catch (Exception paramContext)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramContext);
      paramContext = h;
      if (paramContext == null) {
        k.a("analytics");
      }
      paramBundle = new e.a("AppHeartBeatTask").a("Result", "FailedBuildingEvent").a();
      k.a(paramBundle, "AnalyticsEvent.Builder(A…\n                .build()");
      paramContext.b(paramBundle);
    }
    return PersistentBackgroundTask.RunResult.FailedRetry;
  }
  
  public final boolean a(Context paramContext)
  {
    k.b(paramContext, "serviceContext");
    paramContext = g;
    if (paramContext == null) {
      k.a("accountManager");
    }
    return paramContext.c();
  }
  
  public final e b()
  {
    e locale = new com.truecaller.common.background.e.a(1).a(6L, TimeUnit.HOURS).a().b(20L, TimeUnit.MINUTES).a("beatType", "active").b();
    k.a(locale, "TaskConfiguration.Builde…IVE)\n            .build()");
    return locale;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.AppHeartBeatTask
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */