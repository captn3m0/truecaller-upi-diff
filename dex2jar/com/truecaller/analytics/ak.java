package com.truecaller.analytics;

import com.truecaller.analytics.storage.a;
import javax.inject.Provider;

public final class ak
  implements dagger.a.d<aj>
{
  private final Provider<a> a;
  private final Provider<com.truecaller.analytics.storage.d> b;
  
  private ak(Provider<a> paramProvider, Provider<com.truecaller.analytics.storage.d> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static ak a(Provider<a> paramProvider, Provider<com.truecaller.analytics.storage.d> paramProvider1)
  {
    return new ak(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.ak
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */