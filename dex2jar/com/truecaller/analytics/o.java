package com.truecaller.analytics;

import com.truecaller.analytics.storage.AnalyticsDatabase;
import javax.inject.Provider;

public final class o
  implements dagger.a.d<com.truecaller.analytics.storage.d>
{
  private final f a;
  private final Provider<AnalyticsDatabase> b;
  
  private o(f paramf, Provider<AnalyticsDatabase> paramProvider)
  {
    a = paramf;
    b = paramProvider;
  }
  
  public static o a(f paramf, Provider<AnalyticsDatabase> paramProvider)
  {
    return new o(paramf, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.analytics.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */