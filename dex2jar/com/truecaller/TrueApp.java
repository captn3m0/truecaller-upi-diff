package com.truecaller;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import c.a.ag;
import c.a.an;
import com.d.b.ac;
import com.d.b.aj;
import com.d.b.w.a;
import com.truecaller.ads.provider.fetch.AdsConfigurationManager;
import com.truecaller.analytics.AppHeartBeatTask;
import com.truecaller.analytics.AppSettingsTask;
import com.truecaller.analytics.aa;
import com.truecaller.analytics.as.a;
import com.truecaller.analytics.e.a;
import com.truecaller.android.truemoji.Emoji;
import com.truecaller.android.truemoji.f.e;
import com.truecaller.backup.bb;
import com.truecaller.backup.bb.a;
import com.truecaller.common.account.r;
import com.truecaller.common.h.am;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.tag.TagService;
import com.truecaller.content.TruecallerContentProvider;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;
import com.truecaller.content.TruecallerContract.ah;
import com.truecaller.filters.FilterManager;
import com.truecaller.filters.FilterManager.FilterAction;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.FlashContact;
import com.truecaller.log.AssertionUtil;
import com.truecaller.notificationchannels.n.a;
import com.truecaller.old.data.access.Settings;
import com.truecaller.presence.AvailabilityTrigger;
import com.truecaller.presence.RingerModeListenerWorker;
import com.truecaller.profile.BusinessProfileOnboardingActivity;
import com.truecaller.sdk.ag.a;
import com.truecaller.service.AlarmReceiver;
import com.truecaller.service.CallStateService;
import com.truecaller.service.ClipboardService;
import com.truecaller.service.RefreshContactIndexingService.a;
import com.truecaller.service.RefreshT9MappingService;
import com.truecaller.service.SyncPhoneBookService;
import com.truecaller.tcpermissions.c.a;
import com.truecaller.tcpermissions.f.a;
import com.truecaller.tracking.events.ao;
import com.truecaller.tracking.events.ao.a;
import com.truecaller.truepay.PayTempTokenCallBack;
import com.truecaller.truepay.TcPaySDKListener;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.a.b.cx;
import com.truecaller.truepay.app.ui.transaction.views.activities.TransactionActivity;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.ui.WizardActivity;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.util.al;
import com.truecaller.util.bv;
import com.truecaller.util.cm;
import com.truecaller.util.co;
import com.truecaller.util.df;
import com.truecaller.utils.t.a;
import com.truecaller.voip.d.a;
import com.truecaller.voip.j.a;
import com.truecaller.voip.s;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;

public class TrueApp
  extends com.truecaller.common.b.a
  implements com.truecaller.analytics.a, bk, com.truecaller.flashsdk.core.i, com.truecaller.sdk.ae, ag.a, TcPaySDKListener
{
  public static final long a = ;
  public final com.truecaller.common.a b;
  private com.truecaller.analytics.b d;
  private com.truecaller.analytics.t e = new com.truecaller.analytics.t("Truecaller", "10.41.6");
  private final com.truecaller.utils.t f = com.truecaller.utils.c.a().a(this).a();
  private final com.truecaller.analytics.d g = aa.a().a(this).a(this).b().a(e).a(f).a();
  private final com.truecaller.backup.c h;
  private final com.truecaller.tcpermissions.f i;
  private final com.truecaller.voip.j j;
  private final com.truecaller.insights.a.a.b k;
  private final com.truecaller.incallui.a.i l;
  private final bp m;
  private final BroadcastReceiver n;
  
  public TrueApp()
  {
    Object localObject = com.truecaller.common.b.x();
    a = ((com.truecaller.common.b.a.a)dagger.a.g.a(new com.truecaller.common.b.a.a(this)));
    g = ((com.truecaller.utils.t)dagger.a.g.a(f));
    h = ((com.truecaller.analytics.d)dagger.a.g.a(g));
    b = ((com.truecaller.common.g.c)dagger.a.g.a(new com.truecaller.common.g.c(this, "tc.settings")));
    dagger.a.g.a(a, com.truecaller.common.b.a.a.class);
    dagger.a.g.a(b, com.truecaller.common.g.c.class);
    if (c == null) {
      c = new com.truecaller.common.edge.c();
    }
    if (d == null) {
      d = new com.truecaller.common.h.x();
    }
    if (e == null) {
      e = new com.truecaller.content.a();
    }
    if (f == null) {
      f = new com.truecaller.common.d.a();
    }
    dagger.a.g.a(g, com.truecaller.utils.t.class);
    dagger.a.g.a(h, com.truecaller.analytics.d.class);
    b = new com.truecaller.common.b(a, b, c, d, e, f, g, h, (byte)0);
    localObject = bb.e();
    b = ((com.truecaller.utils.t)dagger.a.g.a(f));
    a = ((com.truecaller.common.a)dagger.a.g.a(b));
    c = ((com.truecaller.analytics.d)dagger.a.g.a(g));
    dagger.a.g.a(a, com.truecaller.common.a.class);
    dagger.a.g.a(b, com.truecaller.utils.t.class);
    dagger.a.g.a(c, com.truecaller.analytics.d.class);
    h = new bb(a, b, c, (byte)0);
    i = com.truecaller.tcpermissions.c.a().a(f).a(b).a();
    localObject = com.truecaller.voip.a.a();
    a = ((com.truecaller.common.a)dagger.a.g.a(b));
    b = ((com.truecaller.utils.t)dagger.a.g.a(f));
    n.a locala = com.truecaller.notificationchannels.n.a;
    c = ((com.truecaller.notificationchannels.n)dagger.a.g.a(n.a.a(this)));
    d = ((com.truecaller.tcpermissions.e)dagger.a.g.a(i));
    e = ((com.truecaller.analytics.d)dagger.a.g.a(g));
    dagger.a.g.a(a, com.truecaller.common.a.class);
    dagger.a.g.a(b, com.truecaller.utils.t.class);
    dagger.a.g.a(c, com.truecaller.notificationchannels.n.class);
    dagger.a.g.a(d, com.truecaller.tcpermissions.e.class);
    dagger.a.g.a(e, com.truecaller.analytics.d.class);
    j = new com.truecaller.voip.a(a, b, c, d, e, (byte)0);
    k = com.truecaller.insights.a.a.a.a().a(g).a(b).a();
    localObject = com.truecaller.incallui.a.a.a();
    a = ((com.truecaller.common.a)dagger.a.g.a(b));
    locala = com.truecaller.notificationchannels.n.a;
    b = ((com.truecaller.notificationchannels.n)dagger.a.g.a(n.a.a(this)));
    c = ((com.truecaller.utils.t)dagger.a.g.a(f));
    dagger.a.g.a(a, com.truecaller.common.a.class);
    dagger.a.g.a(b, com.truecaller.notificationchannels.n.class);
    dagger.a.g.a(c, com.truecaller.utils.t.class);
    l = new com.truecaller.incallui.a.a(a, b, c, (byte)0);
    localObject = be.a();
    z = ((com.truecaller.utils.t)dagger.a.g.a(f));
    D = ((com.truecaller.analytics.d)dagger.a.g.a(g));
    y = ((com.truecaller.common.a)dagger.a.g.a(b));
    A = ((com.truecaller.backup.a)dagger.a.g.a(h));
    B = ((com.truecaller.tcpermissions.e)dagger.a.g.a(i));
    C = ((com.truecaller.voip.j)dagger.a.g.a(j));
    E = ((com.truecaller.insights.a.a.b)dagger.a.g.a(k));
    F = ((com.truecaller.incallui.a.i)dagger.a.g.a(l));
    d = ((c)dagger.a.g.a(new c(this)));
    if (a == null) {
      a = new com.truecaller.engagementrewards.m();
    }
    if (b == null) {
      b = new com.truecaller.messaging.l();
    }
    if (c == null) {
      c = new com.truecaller.messaging.data.e();
    }
    dagger.a.g.a(d, c.class);
    if (e == null) {
      e = new com.truecaller.messaging.transport.o();
    }
    if (f == null) {
      f = new com.truecaller.messaging.transport.sms.c();
    }
    if (g == null) {
      g = new com.truecaller.filters.h();
    }
    if (h == null) {
      h = new com.truecaller.network.util.k();
    }
    if (i == null) {
      i = new com.truecaller.smsparser.d();
    }
    if (j == null) {
      j = new cx();
    }
    if (k == null) {
      k = new com.truecaller.messaging.transport.mms.n();
    }
    if (l == null) {
      l = new com.truecaller.network.d.g();
    }
    if (m == null) {
      m = new com.truecaller.presence.g();
    }
    if (n == null) {
      n = new com.truecaller.callhistory.g();
    }
    if (o == null) {
      o = new com.truecaller.i.h();
    }
    if (p == null) {
      p = new com.truecaller.data.entity.c();
    }
    if (q == null) {
      q = new com.truecaller.tag.f();
    }
    if (r == null) {
      r = new com.truecaller.clevertap.g();
    }
    if (s == null) {
      s = new com.truecaller.ads.installedapps.a();
    }
    if (t == null) {
      t = new com.truecaller.config.e();
    }
    if (u == null) {
      u = new com.truecaller.flash.f();
    }
    if (v == null) {
      v = new com.truecaller.b.a();
    }
    if (w == null) {
      w = new com.truecaller.sdk.push.d();
    }
    if (x == null) {
      x = new com.truecaller.messaging.g.g();
    }
    dagger.a.g.a(y, com.truecaller.common.a.class);
    dagger.a.g.a(z, com.truecaller.utils.t.class);
    dagger.a.g.a(A, com.truecaller.backup.a.class);
    dagger.a.g.a(B, com.truecaller.tcpermissions.e.class);
    dagger.a.g.a(C, com.truecaller.voip.j.class);
    dagger.a.g.a(D, com.truecaller.analytics.d.class);
    dagger.a.g.a(E, com.truecaller.insights.a.a.b.class);
    dagger.a.g.a(F, com.truecaller.incallui.a.i.class);
    m = new be(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, A, B, C, D, E, F, (byte)0);
    n = new a((byte)0);
    localObject = com.truecaller.tcpermissions.f.a;
    f.a.a(i);
    localObject = com.truecaller.voip.d.a;
    d.a.a(new com.truecaller.voip.f(this));
    localObject = com.truecaller.voip.d.a;
    d.a.a(new s(this));
    localObject = com.truecaller.voip.d.a;
    d.a.a(new com.truecaller.voip.i(this));
    localObject = com.truecaller.voip.d.a;
    d.a.a(new com.truecaller.voip.h(this));
    localObject = com.truecaller.voip.d.a;
    d.a.a(new com.truecaller.voip.b(this));
    localObject = com.truecaller.voip.d.a;
    d.a.a(new com.truecaller.voip.ah(this));
    localObject = com.truecaller.voip.j.a;
    j.a.a(j);
    localObject = com.truecaller.incallui.a.a;
    com.truecaller.incallui.a.a.a(new com.truecaller.incallui.b(this));
    localObject = com.truecaller.incallui.a.a;
    com.truecaller.incallui.a.a.a(new com.truecaller.incallui.f(this));
    localObject = com.truecaller.incallui.a.a;
    com.truecaller.incallui.a.a.a(new com.truecaller.incallui.e(this));
    localObject = com.truecaller.incallui.a.i.a;
    com.truecaller.incallui.a.i.a.a(l);
  }
  
  public static Context x()
  {
    return F();
  }
  
  public static TrueApp y()
  {
    return (TrueApp)F();
  }
  
  public final String A()
  {
    return m.by().e().toString();
  }
  
  public final List<com.truecaller.flashsdk.models.a> B()
  {
    return m.bj().a();
  }
  
  public final FlashContact C()
  {
    com.truecaller.common.g.a locala = m.I();
    String str1 = locala.a("profileFirstName");
    String str2 = locala.a("profileNumber");
    if ((!TextUtils.isEmpty(str1)) && (!TextUtils.isEmpty(str2))) {
      return new FlashContact(str2, str1, locala.a("profileLastName"));
    }
    return null;
  }
  
  public final boolean D()
  {
    return m.ao().a("truecaller_sdk_sign_in_flow_17858").equalsIgnoreCase("Loader First");
  }
  
  public final boolean E()
  {
    return m.aF().r().a();
  }
  
  public final Intent a(Context paramContext)
  {
    return BusinessProfileOnboardingActivity.a(paramContext, true);
  }
  
  public final bp a()
  {
    AssertionUtil.isNotNull(m, new String[0]);
    return m;
  }
  
  public final void a(int paramInt, String paramString1, String paramString2)
  {
    if (paramInt != 0)
    {
      switch (paramInt)
      {
      default: 
        return;
      case 5: 
        String str = "+".concat(String.valueOf(paramString1));
        paramString2 = com.truecaller.common.h.h.c(str);
        if (paramString2 != null) {
          paramString2 = c;
        } else {
          paramString2 = null;
        }
        startActivity(DetailsFragment.a(this, null, null, str, paramString1, paramString2, DetailsFragment.SourceType.External, true, true, 10).addFlags(268435456).addFlags(536870912));
        return;
      case 4: 
        m.P().a(Collections.singletonList("+".concat(String.valueOf(paramString1))), "PHONE_NUMBER", paramString2, "quickReply", false, TruecallerContract.Filters.WildCardType.NONE, TruecallerContract.Filters.EntityType.PERSON);
        return;
      }
      getSharedPreferences("callMeBackNotifications", 0).edit().putLong(paramString1, System.currentTimeMillis()).apply();
      return;
    }
    m.D().a("key_last_call_origin", "callMeBackNotification");
  }
  
  public final void a(Activity paramActivity)
  {
    if (com.truecaller.wizard.b.c.g())
    {
      paramActivity.getPackageManager().setComponentEnabledSetting(new ComponentName(paramActivity, com.truecaller.ui.ah.class), 2, 1);
      com.truecaller.wizard.b.c.h();
    }
    else
    {
      paramActivity.startActivity(getPackageManager().getLaunchIntentForPackage(getPackageName()));
    }
    paramActivity.finish();
  }
  
  public final void a(Flash paramFlash)
  {
    m.aY().a(paramFlash);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    if (isTcPayEnabled()) {
      TransactionActivity.startForRequest(this, paramString2, paramString4, paramString1, paramString3, paramString5);
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    com.truecaller.common.g.a locala = m.I();
    String str1 = locala.a("profileNumber");
    String str2 = locala.a("profileCountryIso");
    String str3 = m.d().a();
    String str4 = locala.a("profileAvatar");
    if (str4 != null) {
      com.truecaller.util.aw.a(this, str4);
    }
    new com.truecaller.old.data.access.e(this).b();
    new com.truecaller.old.data.access.f(this).k();
    com.truecaller.common.b.e.a.edit().clear().apply();
    Settings.c();
    new com.truecaller.old.data.access.i(this).b();
    co.a(this);
    com.truecaller.ads.campaigns.f.a(this).b();
    Settings.b(this);
    m.be().a(ThemeManager.Theme.DEFAULT);
    m.d().a(str3);
    m.cb().b(str1);
    if (!paramBoolean)
    {
      locala.a("profileNumber", str1);
      locala.a("profileCountryIso", str2);
    }
    m.ce().b(this);
    super.a(paramBoolean);
  }
  
  public final boolean a(android.support.v4.app.j paramj)
  {
    if (Settings.a(this))
    {
      new com.truecaller.ui.dialogs.l().show(paramj, "QaDialog");
      return true;
    }
    return false;
  }
  
  public final boolean a(String paramString)
  {
    if (!com.truecaller.common.b.a.F().p()) {
      return true;
    }
    Object localObject1 = (TelephonyManager)getSystemService("phone");
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = am.c(((TelephonyManager)localObject1).getNetworkCountryIso(), Locale.ENGLISH);
      localObject1 = am.c(((TelephonyManager)localObject1).getSimCountryIso(), Locale.ENGLISH);
    }
    else
    {
      localObject1 = null;
      localObject2 = localObject1;
    }
    return m.P().a(paramString, null, (String)am.e((CharSequence)localObject2, (CharSequence)localObject1), true).h == FilterManager.FilterAction.FILTER_BLACKLISTED;
  }
  
  @SuppressLint({"MissingSuperCall"})
  public final boolean a(String paramString1, boolean paramBoolean, String paramString2)
    throws SecurityException
  {
    return a(paramString1, paramBoolean, false, paramString2);
  }
  
  public final boolean a(String paramString1, boolean paramBoolean1, boolean paramBoolean2, String paramString2)
    throws SecurityException
  {
    boolean bool = o();
    paramBoolean1 = super.a(paramString1, paramBoolean1, paramString2);
    m.aw().e();
    paramString1 = com.truecaller.old.data.a.d.a;
    int i2 = paramString1.length;
    int i1 = 0;
    while (i1 < i2)
    {
      com.truecaller.util.e.g.a(this, paramString1[i1]).b();
      i1 += 1;
    }
    if (m.ah().a())
    {
      if (!bool)
      {
        TruecallerInit.c(this, null);
        return paramBoolean1;
      }
      if (!paramBoolean2)
      {
        com.truecaller.wizard.b.c.a(this, WizardActivity.class);
        return paramBoolean1;
      }
      com.truecaller.wizard.b.c.b(this, WizardActivity.class);
    }
    return paramBoolean1;
  }
  
  protected void attachBaseContext(Context paramContext)
  {
    super.attachBaseContext(paramContext);
    android.support.multidex.a.a(this);
  }
  
  public final com.truecaller.common.profile.e b()
  {
    return m.bf();
  }
  
  @SuppressLint({"SwitchIntDef"})
  public final boolean b(String paramString)
  {
    if (org.c.a.a.a.k.b(paramString)) {
      return Settings.g();
    }
    return m.aV().h(paramString.replace("+", "")).c;
  }
  
  public final com.truecaller.common.f.c c()
  {
    return m.ai();
  }
  
  public void createShortcut(int paramInt)
  {
    a().aA().a(paramInt);
  }
  
  public final com.truecaller.common.f.b d()
  {
    return m.an();
  }
  
  public final com.truecaller.common.h.c e()
  {
    return m.aI();
  }
  
  public final com.truecaller.featuretoggles.e f()
  {
    return m.aF();
  }
  
  public void fetchTempToken(PayTempTokenCallBack paramPayTempTokenCallBack)
  {
    if (isTcPayEnabled())
    {
      com.truecaller.androidactors.i locali = m.m().a();
      ((com.truecaller.payments.network.b)m.aR().a()).a().a(locali, new -..Lambda.TrueApp.UvHmEQe10W-5wmy6u2xWbg5DgN8(paramPayTempTokenCallBack));
    }
  }
  
  public final com.truecaller.content.d.a g()
  {
    return m.cl();
  }
  
  public Uri getTcPayNotificationTone()
  {
    return m.by().d();
  }
  
  public final Boolean h()
  {
    return Boolean.valueOf(Truepay.getInstance().isRegistrationComplete());
  }
  
  public final void i()
  {
    try
    {
      Locale localLocale2 = Locale.getDefault();
      Object localObject = getResources();
      Locale localLocale1 = localLocale2;
      if (localObject != null)
      {
        localObject = ((Resources)localObject).getConfiguration();
        localLocale1 = localLocale2;
        if (localObject != null) {
          localLocale1 = locale;
        }
      }
      if (com.truecaller.old.data.access.d.d() == null) {
        com.truecaller.old.data.access.d.b(localLocale1);
      }
    }
    catch (RuntimeException localRuntimeException)
    {
      com.truecaller.log.d.a(localRuntimeException);
    }
    super.i();
    Settings.b(this);
    if (com.crashlytics.android.a.d() != null)
    {
      com.crashlytics.android.a.a("language", n());
      com.crashlytics.android.a.a("buildName", a().aI().f());
      com.crashlytics.android.a.a("googlePlayServicesVersion", m.bx().o());
      com.crashlytics.android.a.a(m.d().a());
    }
  }
  
  public boolean isTcPayEnabled()
  {
    return (m.aF().n().a()) && (p());
  }
  
  public void j()
  {
    super.j();
    m.ca().b();
    AlarmReceiver.a(this, false);
  }
  
  public final String k()
  {
    return "Truecaller";
  }
  
  public final String l()
  {
    return "10.41.6";
  }
  
  public void logTcPayCleverTapEvent(String paramString, Map<String, Object> paramMap)
  {
    m.bp().a(paramString, paramMap);
  }
  
  public void logTcPayEvent(String paramString1, String paramString2, Map<CharSequence, CharSequence> paramMap, Map<CharSequence, Double> paramMap1)
  {
    paramString1 = ao.b().a(paramMap).b(paramMap1).b(paramString2).a(paramString1).a();
    ((com.truecaller.analytics.ae)m.f().a()).a(paramString1);
  }
  
  public void logTcPayFacebookEvent(String paramString, Map<CharSequence, CharSequence> paramMap)
  {
    paramString = new e.a(paramString);
    paramMap = paramMap.entrySet().iterator();
    while (paramMap.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)paramMap.next();
      if ((localEntry.getKey() != null) && (localEntry.getValue() != null)) {
        paramString.a(((CharSequence)localEntry.getKey()).toString().toLowerCase(Locale.ENGLISH), ((CharSequence)localEntry.getValue()).toString());
      }
    }
    d.a(paramString.a());
  }
  
  public final boolean m()
  {
    return Settings.i();
  }
  
  public final String n()
  {
    try
    {
      String str = getResourcesgetConfigurationlocale.getLanguage();
      return str;
    }
    catch (NullPointerException localNullPointerException)
    {
      for (;;) {}
    }
    return Settings.b("language");
  }
  
  public final boolean o()
  {
    return !m.D().b("hasNativeDialerCallerId");
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    Settings.c(this);
    super.onConfigurationChanged(getResources().getConfiguration());
  }
  
  public void onCreate()
  {
    boolean bool2 = true;
    com.truecaller.log.d.a = true;
    try
    {
      io.fabric.sdk.android.c.a(this, new io.fabric.sdk.android.i[] { new com.crashlytics.android.a() });
      Thread.setDefaultUncaughtExceptionHandler(new com.truecaller.common.b.g(Thread.getDefaultUncaughtExceptionHandler()));
      com.google.firebase.b.a(this);
      localObject1 = e;
      localObject2 = a().T();
      Object localObject3 = b.c();
      Object localObject4 = a().aI().f();
      c.g.b.k.b(localObject2, "accountManager");
      c.g.b.k.b(localObject3, "coreSettings");
      c.g.b.k.b(localObject4, "buildName");
      b = ((r)localObject2);
      c = ((com.truecaller.common.g.a)localObject3);
      localObject2 = (CharSequence)localObject4;
      c.g.b.k.b(localObject2, "<set-?>");
      a = ((CharSequence)localObject2);
      localObject1 = m;
      localObject2 = com.truecaller.common.network.util.d.d;
      ((bp)localObject1).co();
      localObject1 = com.truecaller.android.truemoji.f.b;
      localObject2 = new com.truecaller.android.truemoji.b.i();
      c.g.b.k.b(localObject2, "provider");
      com.truecaller.android.truemoji.f.a(com.truecaller.android.truemoji.f.c()).clear();
      com.truecaller.android.truemoji.f.a(com.truecaller.android.truemoji.f.c(), ((com.truecaller.android.truemoji.i)localObject2).a());
      com.truecaller.android.truemoji.f.a(com.truecaller.android.truemoji.f.c(), (f.e)com.truecaller.android.truemoji.f.d());
      localObject1 = new ArrayList(3000);
      localObject2 = ((com.truecaller.android.truemoji.i)localObject2).a();
      localObject3 = (Collection)new ArrayList();
      int i2 = localObject2.length;
      i1 = 0;
      while (i1 < i2)
      {
        localObject4 = localObject2[i1].a();
        c.g.b.k.a(localObject4, "it.emojis");
        c.a.m.a((Collection)localObject3, (Iterable)c.a.f.f((Object[])localObject4));
        i1 += 1;
      }
      localObject3 = c.a.m.d((Collection)localObject3);
      localObject2 = (Iterable)localObject3;
      localObject4 = (Collection)new ArrayList();
      Object localObject5 = ((Iterable)localObject2).iterator();
      while (((Iterator)localObject5).hasNext())
      {
        Emoji localEmoji = (Emoji)((Iterator)localObject5).next();
        c.g.b.k.a(localEmoji, "it");
        c.a.m.a((Collection)localObject4, (Iterable)localEmoji.b());
      }
      ((List)localObject3).addAll((Collection)localObject4);
      localObject3 = com.truecaller.android.truemoji.f.c();
      localObject4 = (Map)new LinkedHashMap(c.k.i.c(ag.a(c.a.m.a((Iterable)localObject2, 10)), 16));
      localObject2 = ((Iterable)localObject2).iterator();
      while (((Iterator)localObject2).hasNext())
      {
        localObject5 = (Emoji)((Iterator)localObject2).next();
        c.g.b.k.a(localObject5, "it");
        ((Map)localObject4).put(a, localObject5);
      }
      com.truecaller.android.truemoji.f.a((com.truecaller.android.truemoji.f)localObject3, (HashMap)localObject4);
      ((ArrayList)localObject1).addAll((Collection)com.truecaller.android.truemoji.f.a(com.truecaller.android.truemoji.f.c()).keySet());
      if (!((ArrayList)localObject1).isEmpty())
      {
        localObject2 = c.a.m.a((Iterable)localObject1, com.truecaller.android.truemoji.f.e());
        localObject1 = new StringBuilder(12000);
        localObject2 = ((Iterable)localObject2).iterator();
        while (((Iterator)localObject2).hasNext())
        {
          ((StringBuilder)localObject1).append(Pattern.quote((String)((Iterator)localObject2).next()));
          ((StringBuilder)localObject1).append('|');
        }
        localObject1 = ((StringBuilder)localObject1).deleteCharAt(((StringBuilder)localObject1).length() - 1).toString();
        c.g.b.k.a(localObject1, "patternBuilder.deleteCha…er.length - 1).toString()");
        com.truecaller.android.truemoji.f.a(com.truecaller.android.truemoji.f.c(), Pattern.compile((String)localObject1));
        localObject2 = com.truecaller.android.truemoji.f.c();
        localObject3 = new StringBuilder("(");
        ((StringBuilder)localObject3).append((String)localObject1);
        ((StringBuilder)localObject3).append(")+");
        com.truecaller.android.truemoji.f.b((com.truecaller.android.truemoji.f)localObject2, Pattern.compile(((StringBuilder)localObject3).toString()));
        com.facebook.k.a(this);
        com.facebook.appevents.g.a(this, getString(2131886539));
        com.truecaller.debug.log.a.a(this, com.truecaller.common.h.ai.a(this));
        d = g.c();
        localObject1 = new androidx.work.b.a();
        d = 20000;
        e = 30000;
        f = Math.min(50, 50);
        androidx.work.impl.h.a(this, new androidx.work.b((androidx.work.b.a)localObject1));
        com.google.c.a.k.a(new cm(getAssets()));
        super.onCreate();
        localObject1 = m.t();
        m.bx();
        if (((al)localObject1).e()) {
          m.aB().b();
        } else {
          m.aB().a();
        }
        if (TruecallerContract.a == null) {
          TruecallerContract.a(com.truecaller.common.c.b.b.a(this, TruecallerContentProvider.class));
        }
        Settings.c(this);
        if (Settings.e("qaServer")) {
          KnownEndpoints.switchToStaging();
        }
        net.danlew.android.joda.a.a(this);
        localObject1 = new aj();
        localObject2 = new w.a(this).a((ac)localObject1).a(new df(this)).a(new com.c.a.a(com.truecaller.util.aw.a(this))).a();
        com.d.b.w.a((com.d.b.w)localObject2);
        ((aj)localObject1).a((com.d.b.w)localObject2);
        CallStateService.a(this);
        com.truecaller.util.o.a(this);
      }
    }
    catch (RuntimeException localRuntimeException)
    {
      try
      {
        int i1;
        startService(new Intent(this, ClipboardService.class));
        if (Build.VERSION.SDK_INT >= 26) {
          RingerModeListenerWorker.b();
        }
        registerActivityLifecycleCallbacks(m.ah());
        registerActivityLifecycleCallbacks(new com.truecaller.analytics.x(m.ag()));
        registerActivityLifecycleCallbacks(new com.truecaller.messaging.transport.im.aw());
        getContentResolver().registerContentObserver(TruecallerContract.ah.a(), true, new com.truecaller.data.b());
        if (p())
        {
          RefreshT9MappingService.b(this);
          new RefreshContactIndexingService.a(this).a();
        }
        TagService.a(this);
        Object localObject1 = new IntentFilter("com.truecaller.ACTION_PERMISSIONS_CHANGED");
        ((IntentFilter)localObject1).addAction("com.truecaller.wizard.verification.action.PHONE_VERIFIED");
        android.support.v4.content.d.a(this).a(new com.truecaller.wizard.utils.i.a()
        {
          public final void a(Context paramAnonymousContext, String paramAnonymousString)
          {
            if (TextUtils.equals(paramAnonymousString, "android.permission.READ_CONTACTS"))
            {
              SyncPhoneBookService.a(paramAnonymousContext);
              c.b(10015);
            }
            if ("android.permission.READ_CALL_LOG".equals(paramAnonymousString)) {
              com.truecaller.util.o.a(paramAnonymousContext);
            }
          }
          
          public final void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
          {
            if ("com.truecaller.wizard.verification.action.PHONE_VERIFIED".equals(paramAnonymousIntent.getAction()))
            {
              SyncPhoneBookService.a(paramAnonymousContext, true);
              c.b(10015);
              TrueApp.a(TrueApp.this).bZ().b(null);
              ((com.truecaller.config.a)TrueApp.a(TrueApp.this).aZ().a()).a().c();
              return;
            }
            super.onReceive(paramAnonymousContext, paramAnonymousIntent);
          }
        }, (IntentFilter)localObject1);
        boolean bool1 = Settings.e() ^ true;
        localObject1 = getPackageManager();
        Object localObject2 = new ComponentName(getPackageName(), "com.truecaller.TruecallerInitAlias");
        if (bool1) {
          i1 = 1;
        } else {
          i1 = 2;
        }
        ((PackageManager)localObject1).setComponentEnabledSetting((ComponentName)localObject2, i1, 1);
        localObject2 = new ComponentName(getPackageName(), "com.truecaller.DialerActivityAlias");
        if (bool1) {
          i1 = 1;
        } else {
          i1 = 2;
        }
        ((PackageManager)localObject1).setComponentEnabledSetting((ComponentName)localObject2, i1, 1);
        localObject1 = com.truecaller.flashsdk.core.c.b;
        com.truecaller.flashsdk.core.c.a(this);
        m.be().a(ThemeManager.a());
        m.aV().a(m.aW());
        m.aV().a(this);
        m.aV().b();
        com.truecaller.utils.extensions.i.a(this, n, new String[] { "com.truecaller.datamanager.STATUSES_CHANGED" });
        if ((1041006 == Settings.a("VERSION_CODE", 0)) && (org.c.a.a.a.k.a(Build.VERSION.RELEASE, Settings.b("osVersion"))))
        {
          bool2 = false;
        }
        else
        {
          a().aB().c();
          if (m.aF().n().a()) {
            a().aC().l();
          } else {
            a().aC().m();
          }
          if (m.aF().A().a()) {
            m.bA();
          }
          m.aC().n();
          if (m.aF().t().a()) {
            m.aC().p();
          } else {
            m.aC().q();
          }
          if (m.j().b()) {
            m.aC().r();
          } else {
            m.aC().s();
          }
          Settings.a("VERSION_CODE", 1041006L);
          Settings.b("osVersion", Build.VERSION.RELEASE);
          localObject1 = c;
          ((com.truecaller.common.background.b)localObject1).a();
          AppSettingsTask.a((com.truecaller.common.background.b)localObject1);
          AppHeartBeatTask.a((com.truecaller.common.background.b)localObject1);
          ((com.truecaller.config.a)m.aZ().a()).b().c();
          a().F().b("key_upgrade_timestamp", System.currentTimeMillis());
        }
        ((com.truecaller.presence.c)m.ae().a()).a(AvailabilityTrigger.USER_ACTION, false);
        if (m.aF().n().a())
        {
          m.aC().l();
          Truepay.initialize(this);
          Truepay.getInstance().handleAppUpdate(bool2);
        }
        else
        {
          m.aC().m();
        }
        if (m.aF().A().a()) {
          m.bA();
        }
        m.bp().a();
        m.bu().a(-..Lambda.TrueApp.lcucToFgVpCwHsQbhyCC-rdsEa4.INSTANCE);
        if (E())
        {
          m.bO().a(this);
          Truepay.getInstance().setCreditHelper(m.bO());
        }
        ((com.truecaller.messaging.data.t)a().p().a()).b(2);
        ((com.truecaller.messaging.data.t)a().p().a()).c(0);
        m.cb().b();
        if (m.cb().a()) {
          m.aD().k();
        } else {
          m.aD().l();
        }
        m.ce().a(this);
        m.cg().a(bool2);
        localObject1 = m.cq();
        if (c.e().a())
        {
          if ((b.a() != 0L) && (!b.aa())) {
            ((com.truecaller.messaging.data.t)a.a()).a(false, an.a(Integer.valueOf(5)));
          }
        }
        else if (b.aa()) {
          ((com.truecaller.messaging.data.t)a.a()).a(false, an.a(Integer.valueOf(5)));
        }
        return;
        throw ((Throwable)new IllegalArgumentException("Your EmojiProvider must at least have one category with at least one emoji."));
        localRuntimeException = localRuntimeException;
      }
      catch (IllegalStateException localIllegalStateException)
      {
        for (;;) {}
      }
    }
  }
  
  public void onLowMemory()
  {
    super.onLowMemory();
    com.truecaller.util.aw.a();
  }
  
  public void onTrimMemory(int paramInt)
  {
    super.onTrimMemory(paramInt);
    m.ag().a(paramInt);
    if (paramInt >= 60)
    {
      com.truecaller.util.aw.a();
      m.aq().d();
    }
    if (paramInt > 10)
    {
      if (com.truecaller.ads.i.a().a())
      {
        ((com.d.b.p)com.truecaller.ads.i.a().b()).a(-1);
        return;
      }
      AssertionUtil.reportThrowableButNeverCrash((Throwable)new AssertionError("adsImageCache is not initialized"));
    }
  }
  
  public final boolean p()
  {
    boolean bool2 = m.T().c();
    boolean bool3 = com.truecaller.wizard.b.c.e();
    boolean bool1;
    if ((bool2) && (bool3)) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    if ((!bool1) && (o()))
    {
      "        - hasValidAccount=".concat(String.valueOf(bool2));
      "        - isWizardCompleted=".concat(String.valueOf(bool3));
    }
    return bool1;
  }
  
  public final boolean q()
  {
    return (!o()) && (!p()) && (b.k().c());
  }
  
  public final com.truecaller.androidactors.f<com.truecaller.analytics.ae> r()
  {
    return m.f();
  }
  
  public final com.truecaller.analytics.b s()
  {
    return d;
  }
  
  public final String t()
  {
    CountryListDto.a locala = com.truecaller.common.h.h.c(this);
    if (locala == null) {
      return null;
    }
    return a;
  }
  
  public final com.truecaller.common.a u()
  {
    return b;
  }
  
  public void updateCleverTapProfile(Map<String, Object> paramMap)
  {
    m.bp().a(paramMap);
  }
  
  public final com.truecaller.analytics.d v()
  {
    return g;
  }
  
  public final String w()
  {
    return "tc.settings";
  }
  
  public final com.truecaller.analytics.b z()
  {
    return d;
  }
  
  final class a
    extends BroadcastReceiver
  {
    private a() {}
    
    public final void onReceive(Context paramContext, Intent paramIntent)
    {
      TrueApp.a(TrueApp.this).bd().a(paramIntent);
      TrueApp.a(TrueApp.this).cc().a(paramIntent);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.TrueApp
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */