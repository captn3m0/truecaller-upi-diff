package com.truecaller;

import android.content.Context;
import com.truecaller.util.b.j;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class ag
  implements d<j>
{
  private final c a;
  private final Provider<Context> b;
  
  private ag(c paramc, Provider<Context> paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static ag a(c paramc, Provider<Context> paramProvider)
  {
    return new ag(paramc, paramProvider);
  }
  
  public static j a(Context paramContext)
  {
    return (j)g.a(c.h(paramContext), "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ag
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */