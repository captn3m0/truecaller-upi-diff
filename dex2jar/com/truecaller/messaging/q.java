package com.truecaller.messaging;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class q
  implements d<h>
{
  private final l a;
  private final Provider<Context> b;
  
  private q(l paraml, Provider<Context> paramProvider)
  {
    a = paraml;
    b = paramProvider;
  }
  
  public static q a(l paraml, Provider<Context> paramProvider)
  {
    return new q(paraml, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */