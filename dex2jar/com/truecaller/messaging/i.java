package com.truecaller.messaging;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.google.common.collect.Lists;
import java.util.List;
import org.a.a.a.g;
import org.a.a.b;
import org.c.a.a.a.k;

@Deprecated
final class i
  implements h
{
  private final SharedPreferences a;
  
  i(SharedPreferences paramSharedPreferences)
  {
    a = paramSharedPreferences;
  }
  
  private void f(String paramString)
  {
    a.edit().putLong(paramString, a.getLong(paramString, 0L) + 1L).apply();
  }
  
  public final String A()
  {
    return a.getString("messageSignature", null);
  }
  
  public final String B()
  {
    return a.getString("autoDownloadMedia", "wifi");
  }
  
  public final void C()
  {
    a.edit().putBoolean("additionalPermissionsDialogShown", true).apply();
  }
  
  public final boolean D()
  {
    return a.getBoolean("additionalPermissionsDialogShown", false);
  }
  
  public final b E()
  {
    return new b(a.getLong("lastImReadTime", 0L));
  }
  
  public final b F()
  {
    return new b(a.getLong("lastImSendTime", 0L));
  }
  
  public final String G()
  {
    return a.getString("imPeerId", null);
  }
  
  public final void H()
  {
    a.edit().putBoolean("hasUnconsumedEvents", true).apply();
  }
  
  public final int I()
  {
    return a.getInt("imForceUpgradeVersion", 0);
  }
  
  public final long J()
  {
    return a.getLong("getImUserMissTtl", 0L);
  }
  
  public final int K()
  {
    return a.getInt("getUrlSpamScoreThreshold", 0);
  }
  
  public final long L()
  {
    return a.getLong("imMaxMediaSize", 104857600L);
  }
  
  public final boolean M()
  {
    return a.getBoolean("imTracingEnabled", false);
  }
  
  public final boolean N()
  {
    return a.getBoolean("imPromo", true);
  }
  
  public final void O()
  {
    a.edit().putBoolean("imPromo", false).apply();
  }
  
  public final int P()
  {
    return a.getInt("smsPermissionForBlockQuestionCount", 0);
  }
  
  public final boolean Q()
  {
    return a.getBoolean("isImPresenceReported", false);
  }
  
  public final long R()
  {
    return a.getLong("imInitialSyncTimestamp", -1L);
  }
  
  public final long S()
  {
    return a.getLong("spamTabVisitedTimestamp", 0L);
  }
  
  public final List<String> T()
  {
    return Lists.newArrayList(a.getString("reactions_emoji", "👍,🤣,😮,😍,😠,😢,👎").split(","));
  }
  
  public final boolean U()
  {
    return a.getBoolean("appUpdatePromo", false);
  }
  
  public final long V()
  {
    return a.getLong("lastTimeAppUpdatePromo", 0L);
  }
  
  public final int W()
  {
    return a.getInt("appUpdatePromoPeriod", 30);
  }
  
  public final int X()
  {
    return a.getInt("appUpdateToVersion", -1);
  }
  
  public final int Y()
  {
    return a.getInt("imNewJoinersPeriodDays", 7);
  }
  
  public final int Z()
  {
    return a.getInt("imGroupBatchParticipantCount", 20);
  }
  
  public final long a()
  {
    return a.getLong("MsgLastSyncTime", 0L);
  }
  
  public final long a(int paramInt)
  {
    return a.getLong("MsgLastTransportSyncTime_".concat(String.valueOf(paramInt)), 0L);
  }
  
  public final void a(int paramInt, long paramLong)
  {
    a.edit().putLong("MsgLastTransportSyncTime_".concat(String.valueOf(paramInt)), paramLong).apply();
  }
  
  public final void a(int paramInt, boolean paramBoolean)
  {
    String str;
    if (paramInt == 0)
    {
      str = "requestSmsDeliveryReport";
    }
    else
    {
      if (1 != paramInt) {
        return;
      }
      str = "requestSimTwoSmsDeliveryReport";
    }
    a.edit().putBoolean(str, paramBoolean).apply();
    return;
  }
  
  public final void a(long paramLong)
  {
    a.edit().putLong("MsgLastSyncTime", paramLong).apply();
  }
  
  public final void a(String paramString)
  {
    a.edit().putString("messagingRingtone", k.n(paramString)).apply();
  }
  
  public final void a(b paramb)
  {
    a.edit().putLong("LastMessagePromotionDate", a).apply();
  }
  
  public final void a(boolean paramBoolean)
  {
    a.edit().putBoolean("wasDefaultSmsApp", paramBoolean).apply();
  }
  
  public final boolean aa()
  {
    return a.getBoolean("historyMessagesInitialSyncCompleted", false);
  }
  
  public final int ab()
  {
    return a.getInt("imVoiceClipMaxDurationMins", 59);
  }
  
  public final boolean ac()
  {
    return a.getBoolean("hasDismissedReadReplyPromo", false);
  }
  
  public final void b(int paramInt, boolean paramBoolean)
  {
    String str;
    if (paramInt == 0)
    {
      str = "MmsAutoDownload";
    }
    else
    {
      if (1 != paramInt) {
        return;
      }
      str = "SimTwoMmsAutoDownload";
    }
    a.edit().putBoolean(str, paramBoolean).apply();
    return;
  }
  
  public final void b(long paramLong)
  {
    a.edit().putLong("defaultSmsAppTimestamp", paramLong).apply();
  }
  
  public final void b(String paramString)
  {
    a.edit().putString("messageSignature", paramString).apply();
  }
  
  public final void b(b paramb)
  {
    a.edit().putLong("JoinImUsersNotificationDate", a).apply();
  }
  
  public final void b(boolean paramBoolean)
  {
    a.edit().putBoolean("hadSmsReadAccess", paramBoolean).apply();
  }
  
  public final boolean b()
  {
    return a.getBoolean("wasDefaultSmsApp", false);
  }
  
  public final boolean b(int paramInt)
  {
    String str;
    if (paramInt == 0)
    {
      str = "requestSmsDeliveryReport";
    }
    else
    {
      if (1 != paramInt) {
        break label30;
      }
      str = "requestSimTwoSmsDeliveryReport";
    }
    return a.getBoolean(str, true);
    label30:
    return false;
  }
  
  public final long c()
  {
    return a.getLong("defaultSmsAppTimestamp", 0L);
  }
  
  public final void c(int paramInt, boolean paramBoolean)
  {
    String str;
    if (paramInt == 0)
    {
      str = "MmsAutoDownloadWhenRoaming";
    }
    else
    {
      if (1 != paramInt) {
        return;
      }
      str = "SimTwoMmsAutoDownloadWhenRoaming";
    }
    a.edit().putBoolean(str, paramBoolean).apply();
    return;
  }
  
  public final void c(long paramLong)
  {
    a.edit().putLong("latestSpamProtectionOffNotificationShowtime", paramLong).apply();
  }
  
  public final void c(String paramString)
  {
    SharedPreferences.Editor localEditor = a.edit();
    String str = paramString;
    if (paramString == null) {
      str = "wifi";
    }
    localEditor.putString("autoDownloadMedia", str).apply();
  }
  
  public final void c(b paramb)
  {
    a.edit().putLong("lastImReadTime", a).apply();
  }
  
  public final void c(boolean paramBoolean)
  {
    a.edit().putBoolean("BlockedMessagesNotification", paramBoolean).apply();
  }
  
  public final boolean c(int paramInt)
  {
    String str;
    if (paramInt == 0)
    {
      str = "MmsAutoDownload";
    }
    else
    {
      if (1 != paramInt) {
        break label30;
      }
      str = "SimTwoMmsAutoDownload";
    }
    return a.getBoolean(str, true);
    label30:
    return true;
  }
  
  public final void d(long paramLong)
  {
    a.edit().putLong("getImUserMissTtl", paramLong).apply();
  }
  
  public final void d(String paramString)
  {
    a.edit().putString("imPeerId", paramString).apply();
  }
  
  public final void d(b paramb)
  {
    a.edit().putLong("lastImSendTime", a).apply();
  }
  
  public final void d(boolean paramBoolean)
  {
    a.edit().putBoolean("messagingVibration", paramBoolean).apply();
  }
  
  public final boolean d()
  {
    return a.getBoolean("hadSmsReadAccess", false);
  }
  
  public final boolean d(int paramInt)
  {
    String str;
    if (paramInt == 0)
    {
      str = "MmsAutoDownloadWhenRoaming";
    }
    else
    {
      if (1 != paramInt) {
        break label30;
      }
      str = "SimTwoMmsAutoDownloadWhenRoaming";
    }
    return a.getBoolean(str, false);
    label30:
    return false;
  }
  
  public final void e()
  {
    a.edit().putBoolean("hasShownUndoTip", true).apply();
  }
  
  public final void e(int paramInt)
  {
    a.edit().putInt("spamSearchStatus", paramInt).apply();
  }
  
  public final void e(long paramLong)
  {
    a.edit().putLong("imMaxMediaSize", paramLong).apply();
  }
  
  public final void e(String paramString)
  {
    a.edit().putString("reactions_emoji", paramString).apply();
  }
  
  public final void e(boolean paramBoolean)
  {
    a.edit().putBoolean("showDefaultSmsScreen", paramBoolean).apply();
  }
  
  public final void f(int paramInt)
  {
    a.edit().putInt("pendingSpamProtectionOffNotificationsCount", paramInt).apply();
  }
  
  public final void f(long paramLong)
  {
    a.edit().putLong("imInitialSyncTimestamp", paramLong).apply();
  }
  
  public final void f(boolean paramBoolean)
  {
    a.edit().putBoolean("messagingSendGroupSms", paramBoolean).apply();
  }
  
  public final boolean f()
  {
    return a.getBoolean("hasShownUndoTip", false);
  }
  
  public final b g()
  {
    return new b(a.getLong("LastMessagePromotionDate", 0L));
  }
  
  public final void g(int paramInt)
  {
    a.edit().putInt("pendingIncomingMsgNotificationsCount", paramInt).apply();
  }
  
  public final void g(long paramLong)
  {
    a.edit().putLong("spamTabVisitedTimestamp", paramLong).apply();
  }
  
  public final void g(boolean paramBoolean)
  {
    a.edit().putBoolean("imTracingEnabled", paramBoolean).apply();
  }
  
  public final b h()
  {
    return new b(a.getLong("JoinImUsersNotificationDate", 0L));
  }
  
  public final void h(int paramInt)
  {
    a.edit().putInt("imForceUpgradeVersion", paramInt).apply();
  }
  
  public final void h(long paramLong)
  {
    a.edit().putLong("lastTimeAppUpdatePromo", paramLong).apply();
  }
  
  public final void h(boolean paramBoolean)
  {
    a.edit().putBoolean("isImPresenceReported", paramBoolean).apply();
  }
  
  public final void i(int paramInt)
  {
    a.edit().putInt("getUrlSpamScoreThreshold", paramInt).apply();
  }
  
  public final void i(boolean paramBoolean)
  {
    a.edit().putBoolean("appUpdatePromo", paramBoolean).apply();
  }
  
  public final boolean i()
  {
    return a.getBoolean("BlockedMessagesNotification", false);
  }
  
  public final void j(int paramInt)
  {
    a.edit().putInt("smsPermissionForBlockQuestionCount", paramInt).apply();
  }
  
  public final void j(boolean paramBoolean)
  {
    a.edit().putBoolean("historyMessagesInitialSyncCompleted", paramBoolean).apply();
  }
  
  public final boolean j()
  {
    return a.getBoolean("qaEnableAvailability", false);
  }
  
  public final void k(int paramInt)
  {
    a.edit().putInt("appUpdatePromoPeriod", paramInt).apply();
  }
  
  public final void k(boolean paramBoolean)
  {
    a.edit().putBoolean("hasDismissedReadReplyPromo", paramBoolean).apply();
  }
  
  public final boolean k()
  {
    return a.getBoolean("featureAvailability", false);
  }
  
  public final void l(int paramInt)
  {
    a.edit().putInt("appUpdateToVersion", paramInt).apply();
  }
  
  public final boolean l()
  {
    return a.getBoolean("availability_enabled", false);
  }
  
  public final void m(int paramInt)
  {
    a.edit().putInt("imNewJoinersPeriodDays", paramInt).apply();
  }
  
  public final boolean m()
  {
    return a.getBoolean("flash_enabled", false);
  }
  
  public final void n(int paramInt)
  {
    a.edit().putInt("imGroupMaxParticipantCount", paramInt).apply();
  }
  
  public final boolean n()
  {
    return a.getLong("addressFieldBlinkedCount", 0L) >= 3L;
  }
  
  public final void o()
  {
    f("addressFieldBlinkedCount");
  }
  
  public final void o(int paramInt)
  {
    a.edit().putInt("imGroupBatchParticipantCount", paramInt).apply();
  }
  
  public final void p()
  {
    f("counterFacebookInvite");
  }
  
  public final void p(int paramInt)
  {
    a.edit().putInt("imVoiceClipMaxDurationMins", paramInt).apply();
  }
  
  public final boolean q()
  {
    return a.contains("messagingRingtone");
  }
  
  public final String r()
  {
    String str = a.getString("messagingRingtone", "");
    if (k.b(str)) {
      return null;
    }
    return str;
  }
  
  public final boolean s()
  {
    return a.getBoolean("messagingVibration", false);
  }
  
  public final int t()
  {
    return a.getInt("spamSearchStatus", 0);
  }
  
  public final boolean u()
  {
    return a.getBoolean("showDefaultSmsScreen", false);
  }
  
  public final int v()
  {
    return a.getInt("pendingSpamProtectionOffNotificationsCount", 0);
  }
  
  public final int w()
  {
    return a.getInt("pendingIncomingMsgNotificationsCount", 0);
  }
  
  public final long x()
  {
    return a.getLong("latestSpamProtectionOffNotificationShowtime", 0L);
  }
  
  public final boolean y()
  {
    return a.getBoolean("messagingSendGroupSms", true);
  }
  
  public final boolean z()
  {
    return a.contains("messagingSendGroupSms");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */