package com.truecaller.messaging;

import android.os.Build.VERSION;
import com.truecaller.utils.d;

public final class b
  implements a
{
  private String a;
  private final d b;
  
  public b(d paramd)
  {
    b = paramd;
    a = b.k();
  }
  
  public final void a()
  {
    a = b.k();
  }
  
  public final String b()
  {
    if (Build.VERSION.SDK_INT >= 24) {
      return a;
    }
    return b.k();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */