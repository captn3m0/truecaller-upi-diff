package com.truecaller.messaging.transport;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class u
  implements d<l>
{
  private final o a;
  private final Provider<Context> b;
  
  private u(o paramo, Provider<Context> paramProvider)
  {
    a = paramo;
    b = paramProvider;
  }
  
  public static u a(o paramo, Provider<Context> paramProvider)
  {
    return new u(paramo, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.u
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */