package com.truecaller.messaging.transport;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.aa;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.utils.q;
import java.util.List;
import java.util.Set;
import org.a.a.b;

final class h
  implements l<ad>
{
  private final Context a;
  
  h(Context paramContext)
  {
    a = paramContext;
  }
  
  public final long a(long paramLong)
  {
    return paramLong;
  }
  
  public final long a(f paramf, i parami, r paramr, b paramb1, b paramb2, int paramInt, List<ContentProviderOperation> paramList, q paramq, boolean paramBoolean1, boolean paramBoolean2, Set<Long> paramSet)
  {
    return Long.MIN_VALUE;
  }
  
  public final l.a a(Message paramMessage, Participant[] paramArrayOfParticipant)
  {
    return new l.a(0);
  }
  
  public final String a()
  {
    return "unspecified";
  }
  
  public final String a(String paramString)
  {
    return paramString;
  }
  
  public final void a(Intent paramIntent, int paramInt) {}
  
  public final void a(BinaryEntity paramBinaryEntity) {}
  
  public final void a(Message paramMessage, String paramString1, String paramString2)
  {
    throw new IllegalStateException("Null transport does not support sending reactions");
  }
  
  public final void a(b paramb) {}
  
  public final boolean a(Message paramMessage)
  {
    return false;
  }
  
  public final boolean a(Message paramMessage, Entity paramEntity)
  {
    return false;
  }
  
  public final boolean a(Message paramMessage, ad paramad)
  {
    ad.a.a locala = paramad.a(TruecallerContract.aa.a(a));
    locala.a("status", Integer.valueOf(9));
    locala.a("status = ?", new String[] { String.valueOf(f) });
    paramad.a(locala.a());
    return true;
  }
  
  public final boolean a(Participant paramParticipant)
  {
    return false;
  }
  
  public final boolean a(TransportInfo paramTransportInfo, long paramLong1, long paramLong2, ad paramad)
  {
    return true;
  }
  
  public final boolean a(TransportInfo paramTransportInfo, ad paramad)
  {
    return true;
  }
  
  public final boolean a(TransportInfo paramTransportInfo, ad paramad, boolean paramBoolean)
  {
    paramad.a(paramad.b(TruecallerContract.aa.a(paramTransportInfo.c())).a());
    return true;
  }
  
  public final boolean a(ad paramad)
  {
    try
    {
      paramad = paramad.a(a.getContentResolver());
      if (paramad != null)
      {
        int i = paramad.length;
        if (i != 0) {
          return true;
        }
      }
      return false;
    }
    catch (RemoteException paramad) {}catch (OperationApplicationException paramad) {}
    AssertionUtil.reportThrowableButNeverCrash(paramad);
    return false;
  }
  
  public final boolean a(String paramString, a parama)
  {
    parama.a(0, 0, 0, 3);
    return false;
  }
  
  public final ad b()
  {
    return new ad(TruecallerContract.a);
  }
  
  public final void b(long paramLong)
  {
    throw new IllegalStateException("Null transport does not support retry");
  }
  
  public final boolean b(Message paramMessage)
  {
    return false;
  }
  
  public final boolean b(ad paramad)
  {
    return (!paramad.a()) && (b.equals(TruecallerContract.a));
  }
  
  public final boolean c()
  {
    return true;
  }
  
  public final boolean c(Message paramMessage)
  {
    return false;
  }
  
  public final k d(Message paramMessage)
  {
    return null;
  }
  
  public final b d()
  {
    return b.ay_();
  }
  
  public final int e(Message paramMessage)
  {
    return 0;
  }
  
  public final boolean e()
  {
    return false;
  }
  
  public final int f()
  {
    return 3;
  }
  
  public final boolean f(Message paramMessage)
  {
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */