package com.truecaller.messaging.transport.status;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import c.a.an;
import c.u;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.aa;
import com.truecaller.content.TruecallerContract.al;
import com.truecaller.content.TruecallerContract.z;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.transport.ad;
import com.truecaller.messaging.transport.ad.b;
import com.truecaller.messaging.transport.i;
import com.truecaller.messaging.transport.l;
import com.truecaller.messaging.transport.l.a;
import com.truecaller.utils.q;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import org.a.a.a.g;

public final class a
  implements l<b>
{
  private final ContentResolver a;
  private final ad.b b;
  
  @Inject
  public a(ContentResolver paramContentResolver, ad.b paramb)
  {
    a = paramContentResolver;
    b = paramb;
  }
  
  private final ContentProviderResult[] a(ArrayList<ContentProviderOperation> paramArrayList)
  {
    try
    {
      paramArrayList = a.applyBatch(TruecallerContract.a(), paramArrayList);
      return paramArrayList;
    }
    catch (RemoteException|OperationApplicationException paramArrayList)
    {
      for (;;) {}
    }
    return null;
  }
  
  public final long a(long paramLong)
  {
    return paramLong;
  }
  
  public final long a(com.truecaller.messaging.transport.f paramf, i parami, r paramr, org.a.a.b paramb1, org.a.a.b paramb2, int paramInt, List<ContentProviderOperation> paramList, q paramq, boolean paramBoolean1, boolean paramBoolean2, Set<Long> paramSet)
  {
    c.g.b.k.b(paramf, "threadInfoCache");
    c.g.b.k.b(parami, "participantCache");
    c.g.b.k.b(paramr, "cursor");
    c.g.b.k.b(paramb1, "timeTo");
    c.g.b.k.b(paramb2, "timeFrom");
    c.g.b.k.b(paramList, "operations");
    c.g.b.k.b(paramq, "trace");
    c.g.b.k.b(paramSet, "messagesToClassify");
    return Long.MIN_VALUE;
  }
  
  public final l.a a(Message paramMessage, Participant[] paramArrayOfParticipant)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramArrayOfParticipant, "recipients");
    throw ((Throwable)new UnsupportedOperationException());
  }
  
  public final String a()
  {
    return "status";
  }
  
  public final String a(String paramString)
  {
    c.g.b.k.b(paramString, "simToken");
    return paramString;
  }
  
  public final void a(Intent paramIntent, int paramInt)
  {
    c.g.b.k.b(paramIntent, "intent");
    throw ((Throwable)new UnsupportedOperationException());
  }
  
  public final void a(BinaryEntity paramBinaryEntity)
  {
    c.g.b.k.b(paramBinaryEntity, "entity");
    throw ((Throwable)new UnsupportedOperationException());
  }
  
  public final void a(Message paramMessage, String paramString1, String paramString2)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramString2, "initiatedVia");
    throw ((Throwable)new UnsupportedOperationException());
  }
  
  public final void a(org.a.a.b paramb)
  {
    c.g.b.k.b(paramb, "time");
  }
  
  public final boolean a(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    ArrayList localArrayList = new ArrayList();
    Object localObject1 = m;
    if (localObject1 != null)
    {
      localObject1 = (StatusTransportInfo)localObject1;
      localArrayList.add(ContentProviderOperation.newAssertQuery(TruecallerContract.al.a()).withSelection("raw_id = ?", new String[] { a }).withExpectedCount(0).build());
      Object localObject2 = (List)localArrayList;
      com.truecaller.messaging.data.b.a((List)localObject2, c);
      int i = com.truecaller.messaging.data.b.a((List)localObject2, an.a(c));
      int j = localArrayList.size();
      ContentProviderOperation.Builder localBuilder = ContentProviderOperation.newInsert(TruecallerContract.aa.a()).withValueBackReference("conversation_id", i).withValue("participant_id", Integer.valueOf(-1));
      ContentValues localContentValues = new ContentValues();
      org.a.a.b localb = e;
      c.g.b.k.a(localb, "date");
      localContentValues.put("date", Long.valueOf(a));
      localb = d;
      c.g.b.k.a(localb, "dateSent");
      localContentValues.put("date_sent", Long.valueOf(a));
      localContentValues.put("status", Integer.valueOf(f));
      localContentValues.put("seen", Integer.valueOf(1));
      localContentValues.put("read", Boolean.valueOf(h));
      localContentValues.put("transport", Integer.valueOf(j));
      localContentValues.put("category", Integer.valueOf(2));
      localContentValues.put("classification", Integer.valueOf(2));
      localArrayList.add(localBuilder.withValues(localContentValues).build());
      com.truecaller.messaging.data.b.a((List)localObject2, j, (StatusTransportInfo)localObject1);
      boolean bool;
      if (n.length == 1) {
        bool = true;
      } else {
        bool = false;
      }
      AssertionUtil.AlwaysFatal.isTrue(bool, new String[0]);
      paramMessage = n;
      c.g.b.k.a(paramMessage, "message.entities");
      paramMessage = c.a.f.b(paramMessage);
      c.g.b.k.a(paramMessage, "message.entities.first()");
      paramMessage = (Entity)paramMessage;
      AssertionUtil.AlwaysFatal.isTrue(paramMessage.a(), new String[0]);
      localObject1 = ContentProviderOperation.newInsert(TruecallerContract.z.a());
      localObject2 = new ContentValues();
      paramMessage.a((ContentValues)localObject2);
      localArrayList.add(((ContentProviderOperation.Builder)localObject1).withValues((ContentValues)localObject2).withValueBackReference("message_id", j).build());
      paramMessage = a(localArrayList);
      if (paramMessage != null) {
        i = paramMessage.length;
      } else {
        i = 0;
      }
      return i > 0;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.messaging.transport.status.StatusTransportInfo");
  }
  
  public final boolean a(Message paramMessage, Entity paramEntity)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramEntity, "entity");
    throw ((Throwable)new UnsupportedOperationException());
  }
  
  public final boolean a(Participant paramParticipant)
  {
    c.g.b.k.b(paramParticipant, "participant");
    return false;
  }
  
  public final boolean a(String paramString, com.truecaller.messaging.transport.a parama)
  {
    c.g.b.k.b(paramString, "text");
    c.g.b.k.b(parama, "result");
    return false;
  }
  
  public final void b(long paramLong)
  {
    throw ((Throwable)new UnsupportedOperationException());
  }
  
  public final boolean b(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return false;
  }
  
  public final boolean b(ad paramad)
  {
    c.g.b.k.b(paramad, "transaction");
    return (!paramad.a()) && (c.g.b.k.a(paramad.c(), TruecallerContract.a()));
  }
  
  public final boolean c()
  {
    return false;
  }
  
  public final boolean c(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return false;
  }
  
  public final com.truecaller.messaging.transport.k d(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    throw ((Throwable)new UnsupportedOperationException());
  }
  
  public final org.a.a.b d()
  {
    org.a.a.b localb = org.a.a.b.ay_();
    c.g.b.k.a(localb, "DateTime.now()");
    return localb;
  }
  
  public final int e(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return 0;
  }
  
  public final boolean e()
  {
    return false;
  }
  
  public final int f()
  {
    return 6;
  }
  
  public final boolean f(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    throw ((Throwable)new UnsupportedOperationException());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.status.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */