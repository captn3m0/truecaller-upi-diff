package com.truecaller.messaging.transport.status;

import android.content.ContentResolver;
import com.truecaller.messaging.transport.ad.b;
import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d<a>
{
  private final Provider<ContentResolver> a;
  private final Provider<ad.b> b;
  
  private c(Provider<ContentResolver> paramProvider, Provider<ad.b> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static c a(Provider<ContentResolver> paramProvider, Provider<ad.b> paramProvider1)
  {
    return new c(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.status.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */