package com.truecaller.messaging.transport;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import javax.inject.Provider;

public final class t
  implements dagger.a.d<f<d>>
{
  private final o a;
  private final Provider<i> b;
  private final Provider<d> c;
  
  private t(o paramo, Provider<i> paramProvider, Provider<d> paramProvider1)
  {
    a = paramo;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static t a(o paramo, Provider<i> paramProvider, Provider<d> paramProvider1)
  {
    return new t(paramo, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.t
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */