package com.truecaller.messaging.transport;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.net.Uri.Builder;
import android.provider.Telephony.Threads;
import com.android.a.a.c;
import java.util.Collections;
import java.util.List;

final class g
  implements f
{
  @SuppressLint({"NewApi"})
  private static final Uri a = Telephony.Threads.CONTENT_URI.buildUpon().appendQueryParameter("simple", "true").build();
  @SuppressLint({"InlinedApi"})
  private static final String[] b = { "_id", "recipient_ids" };
  private static final Uri c = Uri.parse("content://mms-sms/canonical-addresses");
  private final Context d;
  private android.support.v4.f.f<List<String>> e = null;
  
  g(Context paramContext)
  {
    d = paramContext;
  }
  
  /* Error */
  private android.support.v4.f.f<String> a()
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: aconst_null
    //   3: astore 4
    //   5: aload_0
    //   6: getfield 74	com/truecaller/messaging/transport/g:d	Landroid/content/Context;
    //   9: invokevirtual 83	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   12: getstatic 65	com/truecaller/messaging/transport/g:c	Landroid/net/Uri;
    //   15: aconst_null
    //   16: aconst_null
    //   17: aconst_null
    //   18: aconst_null
    //   19: invokevirtual 89	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   22: astore 5
    //   24: aload 5
    //   26: ifnull +123 -> 149
    //   29: aload 5
    //   31: astore 4
    //   33: aload 5
    //   35: astore_3
    //   36: aload 5
    //   38: ldc 91
    //   40: invokeinterface 97 2 0
    //   45: istore_1
    //   46: aload 5
    //   48: astore 4
    //   50: aload 5
    //   52: astore_3
    //   53: aload 5
    //   55: ldc 53
    //   57: invokeinterface 97 2 0
    //   62: istore_2
    //   63: aload 5
    //   65: astore 4
    //   67: aload 5
    //   69: astore_3
    //   70: new 99	android/support/v4/f/f
    //   73: dup
    //   74: aload 5
    //   76: invokeinterface 103 1 0
    //   81: invokespecial 106	android/support/v4/f/f:<init>	(I)V
    //   84: astore 6
    //   86: aload 5
    //   88: astore 4
    //   90: aload 5
    //   92: astore_3
    //   93: aload 5
    //   95: invokeinterface 110 1 0
    //   100: ifeq +34 -> 134
    //   103: aload 5
    //   105: astore 4
    //   107: aload 5
    //   109: astore_3
    //   110: aload 6
    //   112: aload 5
    //   114: iload_2
    //   115: invokeinterface 114 2 0
    //   120: aload 5
    //   122: iload_1
    //   123: invokeinterface 118 2 0
    //   128: invokevirtual 121	android/support/v4/f/f:c	(JLjava/lang/Object;)V
    //   131: goto -45 -> 86
    //   134: aload 5
    //   136: ifnull +10 -> 146
    //   139: aload 5
    //   141: invokeinterface 124 1 0
    //   146: aload 6
    //   148: areturn
    //   149: aload 5
    //   151: ifnull +37 -> 188
    //   154: aload 5
    //   156: astore_3
    //   157: goto +25 -> 182
    //   160: astore_3
    //   161: goto +36 -> 197
    //   164: astore 5
    //   166: aload_3
    //   167: astore 4
    //   169: aload 5
    //   171: iconst_0
    //   172: anewarray 51	java/lang/String
    //   175: invokestatic 130	com/truecaller/log/AssertionUtil:shouldNeverHappen	(Ljava/lang/Throwable;[Ljava/lang/String;)V
    //   178: aload_3
    //   179: ifnull +9 -> 188
    //   182: aload_3
    //   183: invokeinterface 124 1 0
    //   188: new 99	android/support/v4/f/f
    //   191: dup
    //   192: iconst_0
    //   193: invokespecial 106	android/support/v4/f/f:<init>	(I)V
    //   196: areturn
    //   197: aload 4
    //   199: ifnull +10 -> 209
    //   202: aload 4
    //   204: invokeinterface 124 1 0
    //   209: aload_3
    //   210: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	211	0	this	g
    //   45	78	1	i	int
    //   62	53	2	j	int
    //   1	156	3	localObject1	Object
    //   160	50	3	localObject2	Object
    //   3	200	4	localObject3	Object
    //   22	133	5	localCursor	Cursor
    //   164	6	5	localRuntimeException	RuntimeException
    //   84	63	6	localf	android.support.v4.f.f
    // Exception table:
    //   from	to	target	type
    //   5	24	160	finally
    //   36	46	160	finally
    //   53	63	160	finally
    //   70	86	160	finally
    //   93	103	160	finally
    //   110	131	160	finally
    //   169	178	160	finally
    //   5	24	164	java/lang/RuntimeException
    //   36	46	164	java/lang/RuntimeException
    //   53	63	164	java/lang/RuntimeException
    //   70	86	164	java/lang/RuntimeException
    //   93	103	164	java/lang/RuntimeException
    //   110	131	164	java/lang/RuntimeException
  }
  
  /* Error */
  private android.support.v4.f.f<List<String>> b()
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 6
    //   3: aconst_null
    //   4: astore 4
    //   6: aload 4
    //   8: astore_3
    //   9: aload_0
    //   10: getfield 72	com/truecaller/messaging/transport/g:e	Landroid/support/v4/f/f;
    //   13: ifnull +11 -> 24
    //   16: aload 4
    //   18: astore_3
    //   19: aload_0
    //   20: getfield 72	com/truecaller/messaging/transport/g:e	Landroid/support/v4/f/f;
    //   23: areturn
    //   24: aload 4
    //   26: astore_3
    //   27: aload_0
    //   28: invokespecial 134	com/truecaller/messaging/transport/g:a	()Landroid/support/v4/f/f;
    //   31: astore 5
    //   33: aload 4
    //   35: astore_3
    //   36: aload 5
    //   38: invokevirtual 136	android/support/v4/f/f:b	()I
    //   41: ifne +26 -> 67
    //   44: aload 4
    //   46: astore_3
    //   47: aload_0
    //   48: new 99	android/support/v4/f/f
    //   51: dup
    //   52: iconst_0
    //   53: invokespecial 106	android/support/v4/f/f:<init>	(I)V
    //   56: putfield 72	com/truecaller/messaging/transport/g:e	Landroid/support/v4/f/f;
    //   59: aload 4
    //   61: astore_3
    //   62: aload_0
    //   63: getfield 72	com/truecaller/messaging/transport/g:e	Landroid/support/v4/f/f;
    //   66: areturn
    //   67: aload 4
    //   69: astore_3
    //   70: aload_0
    //   71: getfield 74	com/truecaller/messaging/transport/g:d	Landroid/content/Context;
    //   74: invokevirtual 83	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   77: getstatic 49	com/truecaller/messaging/transport/g:a	Landroid/net/Uri;
    //   80: getstatic 57	com/truecaller/messaging/transport/g:b	[Ljava/lang/String;
    //   83: aconst_null
    //   84: aconst_null
    //   85: aconst_null
    //   86: invokevirtual 89	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   89: astore 4
    //   91: aload 4
    //   93: ifnull +156 -> 249
    //   96: aload_0
    //   97: new 99	android/support/v4/f/f
    //   100: dup
    //   101: aload 4
    //   103: invokeinterface 103 1 0
    //   108: invokespecial 106	android/support/v4/f/f:<init>	(I)V
    //   111: putfield 72	com/truecaller/messaging/transport/g:e	Landroid/support/v4/f/f;
    //   114: aload 4
    //   116: invokeinterface 110 1 0
    //   121: ifeq +100 -> 221
    //   124: aload 4
    //   126: iconst_1
    //   127: invokeinterface 118 2 0
    //   132: bipush 32
    //   134: invokestatic 141	org/c/a/a/a/k:a	(Ljava/lang/String;C)[Ljava/lang/String;
    //   137: astore_3
    //   138: new 143	java/util/ArrayList
    //   141: dup
    //   142: aload_3
    //   143: arraylength
    //   144: invokespecial 144	java/util/ArrayList:<init>	(I)V
    //   147: astore 6
    //   149: aload_3
    //   150: arraylength
    //   151: istore_2
    //   152: iconst_0
    //   153: istore_1
    //   154: iload_1
    //   155: iload_2
    //   156: if_icmpge -42 -> 114
    //   159: aload 5
    //   161: aload_3
    //   162: iload_1
    //   163: aaload
    //   164: invokestatic 150	java/lang/Long:parseLong	(Ljava/lang/String;)J
    //   167: aconst_null
    //   168: invokevirtual 153	android/support/v4/f/f:a	(JLjava/lang/Object;)Ljava/lang/Object;
    //   171: checkcast 51	java/lang/String
    //   174: astore 7
    //   176: aload 7
    //   178: invokestatic 159	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   181: ifne +13 -> 194
    //   184: aload 6
    //   186: aload 7
    //   188: invokeinterface 165 2 0
    //   193: pop
    //   194: aload_0
    //   195: getfield 72	com/truecaller/messaging/transport/g:e	Landroid/support/v4/f/f;
    //   198: aload 4
    //   200: iconst_0
    //   201: invokeinterface 114 2 0
    //   206: aload 6
    //   208: invokestatic 171	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
    //   211: invokevirtual 173	android/support/v4/f/f:b	(JLjava/lang/Object;)V
    //   214: iload_1
    //   215: iconst_1
    //   216: iadd
    //   217: istore_1
    //   218: goto -64 -> 154
    //   221: aload_0
    //   222: getfield 72	com/truecaller/messaging/transport/g:e	Landroid/support/v4/f/f;
    //   225: astore_3
    //   226: aload 4
    //   228: ifnull +10 -> 238
    //   231: aload 4
    //   233: invokeinterface 124 1 0
    //   238: aload_3
    //   239: areturn
    //   240: astore_3
    //   241: goto +81 -> 322
    //   244: astore 5
    //   246: goto +35 -> 281
    //   249: aload 4
    //   251: ifnull +54 -> 305
    //   254: aload 4
    //   256: invokeinterface 124 1 0
    //   261: goto +44 -> 305
    //   264: astore 5
    //   266: aload_3
    //   267: astore 4
    //   269: aload 5
    //   271: astore_3
    //   272: goto +50 -> 322
    //   275: astore 5
    //   277: aload 6
    //   279: astore 4
    //   281: aload 4
    //   283: astore_3
    //   284: aload 5
    //   286: iconst_0
    //   287: anewarray 51	java/lang/String
    //   290: invokestatic 130	com/truecaller/log/AssertionUtil:shouldNeverHappen	(Ljava/lang/Throwable;[Ljava/lang/String;)V
    //   293: aload 4
    //   295: ifnull +10 -> 305
    //   298: aload 4
    //   300: invokeinterface 124 1 0
    //   305: aload_0
    //   306: new 99	android/support/v4/f/f
    //   309: dup
    //   310: iconst_0
    //   311: invokespecial 106	android/support/v4/f/f:<init>	(I)V
    //   314: putfield 72	com/truecaller/messaging/transport/g:e	Landroid/support/v4/f/f;
    //   317: aload_0
    //   318: getfield 72	com/truecaller/messaging/transport/g:e	Landroid/support/v4/f/f;
    //   321: areturn
    //   322: aload 4
    //   324: ifnull +10 -> 334
    //   327: aload 4
    //   329: invokeinterface 124 1 0
    //   334: aload_3
    //   335: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	336	0	this	g
    //   153	65	1	i	int
    //   151	6	2	j	int
    //   8	231	3	localObject1	Object
    //   240	27	3	localObject2	Object
    //   271	64	3	localObject3	Object
    //   4	324	4	localObject4	Object
    //   31	129	5	localf	android.support.v4.f.f
    //   244	1	5	localRuntimeException1	RuntimeException
    //   264	6	5	localObject5	Object
    //   275	10	5	localRuntimeException2	RuntimeException
    //   1	277	6	localArrayList	java.util.ArrayList
    //   174	13	7	str	String
    // Exception table:
    //   from	to	target	type
    //   96	114	240	finally
    //   114	152	240	finally
    //   159	194	240	finally
    //   194	214	240	finally
    //   221	226	240	finally
    //   96	114	244	java/lang/RuntimeException
    //   114	152	244	java/lang/RuntimeException
    //   159	194	244	java/lang/RuntimeException
    //   194	214	244	java/lang/RuntimeException
    //   221	226	244	java/lang/RuntimeException
    //   9	16	264	finally
    //   19	24	264	finally
    //   27	33	264	finally
    //   36	44	264	finally
    //   47	59	264	finally
    //   62	67	264	finally
    //   70	91	264	finally
    //   284	293	264	finally
    //   9	16	275	java/lang/RuntimeException
    //   19	24	275	java/lang/RuntimeException
    //   27	33	275	java/lang/RuntimeException
    //   36	44	275	java/lang/RuntimeException
    //   47	59	275	java/lang/RuntimeException
    //   62	67	275	java/lang/RuntimeException
    //   70	91	275	java/lang/RuntimeException
  }
  
  @SuppressLint({"InlinedApi"})
  public final String a(long paramLong, Uri paramUri)
  {
    Object localObject = a(paramLong);
    if (((List)localObject).isEmpty()) {
      return "";
    }
    if (((List)localObject).size() == 1) {
      return (String)((List)localObject).get(0);
    }
    localObject = null;
    try
    {
      paramUri = d.getContentResolver().query(Uri.withAppendedPath(paramUri, "addr"), new String[] { "address", "charset" }, "type=137", null, null);
      if (paramUri != null)
      {
        localObject = paramUri;
        if (paramUri.moveToFirst())
        {
          localObject = paramUri;
          String str = c.a(c.a(paramUri.getString(0), paramUri.getInt(1)), paramUri.getInt(1));
          if (paramUri != null) {
            paramUri.close();
          }
          return str;
        }
      }
      if (paramUri != null) {
        paramUri.close();
      }
      return "";
    }
    finally
    {
      if (localObject != null) {
        ((Cursor)localObject).close();
      }
    }
  }
  
  public final List<String> a(long paramLong)
  {
    List localList2 = (List)b().a(paramLong, null);
    List localList1 = localList2;
    if (localList2 == null) {
      localList1 = Collections.emptyList();
    }
    return localList1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */