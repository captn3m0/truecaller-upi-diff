package com.truecaller.messaging.transport;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class y
  implements d<f>
{
  private final o a;
  private final Provider<Context> b;
  
  private y(o paramo, Provider<Context> paramProvider)
  {
    a = paramo;
    b = paramProvider;
  }
  
  public static y a(o paramo, Provider<Context> paramProvider)
  {
    return new y(paramo, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.y
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */