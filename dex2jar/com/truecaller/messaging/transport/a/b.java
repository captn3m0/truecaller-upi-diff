package com.truecaller.messaging.transport.a;

import com.truecaller.androidactors.f;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.transport.l;
import com.truecaller.messaging.transport.m;

public final class b
  extends a
{
  private final dagger.a<m> a;
  
  public b(dagger.a<m> parama, f<t> paramf, com.truecaller.messaging.c.a parama1)
  {
    super(paramf, parama1);
    a = parama;
  }
  
  public final void a(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    int i = j;
    boolean bool2 = true;
    boolean bool1;
    if (i == 2) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    AssertionUtil.AlwaysFatal.isTrue(bool1, new String[0]);
    if ((f & 0x4) != 0) {
      bool1 = bool2;
    } else {
      bool1 = false;
    }
    AssertionUtil.AlwaysFatal.isTrue(bool1, new String[0]);
    l locall = ((m)a.get()).a(2);
    c.g.b.k.a(locall, "transportManager.get().g…nsport(Transport.TYPE_IM)");
    com.truecaller.messaging.transport.k localk = locall.d(paramMessage);
    c.g.b.k.a(localk, "transport.sendMessage(message)");
    a(localk, paramMessage, locall);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */