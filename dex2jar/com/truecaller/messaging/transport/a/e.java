package com.truecaller.messaging.transport.a;

import com.truecaller.androidactors.f;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.transport.k.b;
import com.truecaller.messaging.transport.k.e;
import com.truecaller.messaging.transport.l;
import com.truecaller.messaging.transport.m;
import java.util.concurrent.TimeUnit;

public final class e
  extends a
{
  private final dagger.a<m> a;
  
  public e(dagger.a<m> parama, f<t> paramf, com.truecaller.messaging.c.a parama1)
  {
    super(paramf, parama1);
    a = parama;
  }
  
  public final void a(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    boolean bool;
    if ((f & 0x4) != 0) {
      bool = true;
    } else {
      bool = false;
    }
    AssertionUtil.AlwaysFatal.isTrue(bool, new String[0]);
    l locall = ((m)a.get()).b(j);
    Object localObject1;
    if (locall != null)
    {
      localObject2 = locall.d(paramMessage);
      localObject1 = localObject2;
      if (localObject2 != null) {}
    }
    else
    {
      localObject1 = (com.truecaller.messaging.transport.k)k.b.a;
    }
    c.g.b.k.a(localObject1, "transport?.sendMessage(m…age) ?: SendResult.Failed");
    Object localObject2 = localObject1;
    if ((localObject1 instanceof k.e)) {
      localObject2 = ((k.e)localObject1).a(TimeUnit.MINUTES);
    }
    a((com.truecaller.messaging.transport.k)localObject2, paramMessage, locall);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.a.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */