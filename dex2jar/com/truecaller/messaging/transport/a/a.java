package com.truecaller.messaging.transport.a;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.transport.k.a;
import com.truecaller.messaging.transport.k.b;
import com.truecaller.messaging.transport.k.c;
import com.truecaller.messaging.transport.k.d;
import com.truecaller.messaging.transport.l;
import org.a.a.a.g;

public abstract class a
  implements c
{
  private final f<t> a;
  private final com.truecaller.messaging.c.a b;
  
  public a(f<t> paramf, com.truecaller.messaging.c.a parama)
  {
    a = paramf;
    b = parama;
  }
  
  public final void a(com.truecaller.messaging.transport.k paramk, Message paramMessage, l<?> paraml)
  {
    c.g.b.k.b(paramk, "result");
    c.g.b.k.b(paramMessage, "message");
    boolean bool = paramk instanceof k.d;
    if (bool)
    {
      if (paraml != null) {
        ((t)a.a()).a(paraml.f(), e, false);
      }
    }
    else
    {
      if ((!(paramk instanceof k.b)) && (!(paramk instanceof k.a)))
      {
        if ((paramk instanceof k.c))
        {
          paraml = (t)a.a();
          paramk = (k.c)paramk;
          paraml.a(paramMessage, a.a, b).c();
          return;
        }
        throw ((Throwable)new IllegalStateException("Unexpected result ".concat(String.valueOf(paramk))));
      }
      ((t)a.a()).e(paramMessage).c();
    }
    if (bool)
    {
      paramk = "Success";
    }
    else if ((paramk instanceof k.a))
    {
      paramk = "Cancel";
    }
    else
    {
      if (!(paramk instanceof k.b)) {
        break label233;
      }
      paramk = "Failure";
    }
    com.truecaller.messaging.c.a locala = b;
    int i;
    if (paraml != null) {
      i = paraml.f();
    } else {
      i = 3;
    }
    locala.a(paramk, paramMessage, i);
    return;
    label233:
    throw ((Throwable)new IllegalStateException("Unexpected result ".concat(String.valueOf(paramk))));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */