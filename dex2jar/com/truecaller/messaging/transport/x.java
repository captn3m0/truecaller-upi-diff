package com.truecaller.messaging.transport;

import android.content.Context;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.k;
import com.truecaller.messaging.transport.a.c;
import dagger.a.d;
import javax.inject.Provider;

public final class x
  implements d<f<c>>
{
  private final o a;
  private final Provider<Context> b;
  private final Provider<k> c;
  private final Provider<c> d;
  
  private x(o paramo, Provider<Context> paramProvider, Provider<k> paramProvider1, Provider<c> paramProvider2)
  {
    a = paramo;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
  }
  
  public static x a(o paramo, Provider<Context> paramProvider, Provider<k> paramProvider1, Provider<c> paramProvider2)
  {
    return new x(paramo, paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.x
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */