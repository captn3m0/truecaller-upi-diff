package com.truecaller.messaging.transport;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class z
  implements d<ad.b>
{
  private final o a;
  private final Provider<Context> b;
  
  private z(o paramo, Provider<Context> paramProvider)
  {
    a = paramo;
    b = paramProvider;
  }
  
  public static z a(o paramo, Provider<Context> paramProvider)
  {
    return new z(paramo, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.z
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */