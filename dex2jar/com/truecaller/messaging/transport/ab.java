package com.truecaller.messaging.transport;

import com.truecaller.androidactors.f;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.h;
import com.truecaller.util.al;
import javax.inject.Provider;

public final class ab
  implements dagger.a.d<m>
{
  private final o a;
  private final Provider<h> b;
  private final Provider<al> c;
  private final Provider<com.truecaller.utils.d> d;
  private final Provider<f<t>> e;
  private final Provider<f<d>> f;
  private final Provider<l> g;
  private final Provider<l> h;
  private final Provider<l> i;
  private final Provider<l> j;
  private final Provider<l> k;
  private final Provider<l> l;
  private final Provider<l> m;
  private final Provider<l> n;
  private final Provider<l> o;
  private final Provider<bw> p;
  private final Provider<com.truecaller.utils.l> q;
  private final Provider<com.truecaller.messaging.a> r;
  private final Provider<com.truecaller.messaging.c.a> s;
  
  private ab(o paramo, Provider<h> paramProvider, Provider<al> paramProvider1, Provider<com.truecaller.utils.d> paramProvider2, Provider<f<t>> paramProvider3, Provider<f<d>> paramProvider4, Provider<l> paramProvider5, Provider<l> paramProvider6, Provider<l> paramProvider7, Provider<l> paramProvider8, Provider<l> paramProvider9, Provider<l> paramProvider10, Provider<l> paramProvider11, Provider<l> paramProvider12, Provider<l> paramProvider13, Provider<bw> paramProvider14, Provider<com.truecaller.utils.l> paramProvider15, Provider<com.truecaller.messaging.a> paramProvider16, Provider<com.truecaller.messaging.c.a> paramProvider17)
  {
    a = paramo;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
    f = paramProvider4;
    g = paramProvider5;
    h = paramProvider6;
    i = paramProvider7;
    j = paramProvider8;
    k = paramProvider9;
    l = paramProvider10;
    m = paramProvider11;
    n = paramProvider12;
    o = paramProvider13;
    p = paramProvider14;
    q = paramProvider15;
    r = paramProvider16;
    s = paramProvider17;
  }
  
  public static ab a(o paramo, Provider<h> paramProvider, Provider<al> paramProvider1, Provider<com.truecaller.utils.d> paramProvider2, Provider<f<t>> paramProvider3, Provider<f<d>> paramProvider4, Provider<l> paramProvider5, Provider<l> paramProvider6, Provider<l> paramProvider7, Provider<l> paramProvider8, Provider<l> paramProvider9, Provider<l> paramProvider10, Provider<l> paramProvider11, Provider<l> paramProvider12, Provider<l> paramProvider13, Provider<bw> paramProvider14, Provider<com.truecaller.utils.l> paramProvider15, Provider<com.truecaller.messaging.a> paramProvider16, Provider<com.truecaller.messaging.c.a> paramProvider17)
  {
    return new ab(paramo, paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10, paramProvider11, paramProvider12, paramProvider13, paramProvider14, paramProvider15, paramProvider16, paramProvider17);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.ab
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */