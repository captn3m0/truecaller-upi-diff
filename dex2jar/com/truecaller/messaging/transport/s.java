package com.truecaller.messaging.transport;

import android.content.Context;
import com.truecaller.androidactors.f;
import com.truecaller.messaging.c.a;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.transport.a.c;
import javax.inject.Provider;

public final class s
  implements dagger.a.d<d>
{
  private final o a;
  private final Provider<Context> b;
  private final Provider<f<t>> c;
  private final Provider<f<c>> d;
  private final Provider<f<c>> e;
  private final Provider<a> f;
  
  private s(o paramo, Provider<Context> paramProvider, Provider<f<t>> paramProvider1, Provider<f<c>> paramProvider2, Provider<f<c>> paramProvider3, Provider<a> paramProvider4)
  {
    a = paramo;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
    f = paramProvider4;
  }
  
  public static s a(o paramo, Provider<Context> paramProvider, Provider<f<t>> paramProvider1, Provider<f<c>> paramProvider2, Provider<f<c>> paramProvider3, Provider<a> paramProvider4)
  {
    return new s(paramo, paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.s
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */