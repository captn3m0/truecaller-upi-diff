package com.truecaller.messaging.transport;

import com.truecaller.common.h.u;
import dagger.a.d;
import javax.inject.Provider;

public final class v
  implements d<i>
{
  private final o a;
  private final Provider<u> b;
  
  private v(o paramo, Provider<u> paramProvider)
  {
    a = paramo;
    b = paramProvider;
  }
  
  public static v a(o paramo, Provider<u> paramProvider)
  {
    return new v(paramo, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.v
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */