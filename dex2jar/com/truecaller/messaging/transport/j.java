package com.truecaller.messaging.transport;

import c.g.b.k;
import com.truecaller.common.h.u;
import com.truecaller.messaging.data.types.Participant;
import java.util.LinkedHashMap;
import java.util.Map;

public final class j
  implements i
{
  private final Map<String, Participant> a;
  private final u b;
  
  public j(u paramu)
  {
    b = paramu;
    a = ((Map)new LinkedHashMap());
  }
  
  public final Participant a(String paramString)
  {
    k.b(paramString, "address");
    Object localObject2 = (Participant)a.get(paramString);
    Object localObject1 = localObject2;
    if (localObject2 == null)
    {
      localObject1 = b;
      localObject1 = Participant.a(paramString, (u)localObject1, ((u)localObject1).a());
      localObject2 = a;
      k.a(localObject1, "this");
      ((Map)localObject2).put(paramString, localObject1);
      k.a(localObject1, "Participant.buildFromAdd…cipants[address] = this }");
    }
    return (Participant)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */