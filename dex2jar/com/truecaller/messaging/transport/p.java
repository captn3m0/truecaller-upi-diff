package com.truecaller.messaging.transport;

import com.truecaller.backup.ca;
import dagger.a.d;
import javax.inject.Provider;

public final class p
  implements d<l>
{
  private final o a;
  private final Provider<ca> b;
  
  private p(o paramo, Provider<ca> paramProvider)
  {
    a = paramo;
    b = paramProvider;
  }
  
  public static p a(o paramo, Provider<ca> paramProvider)
  {
    return new p(paramo, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */