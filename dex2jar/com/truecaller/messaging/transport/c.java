package com.truecaller.messaging.transport;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import com.truecaller.content.TruecallerContract.aa;
import com.truecaller.content.TruecallerContract.z;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.multisim.h;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.a.a.a.g;

public abstract class c<T extends TransportInfo, RC extends c.a>
{
  protected final Context a;
  protected final h b;
  protected final e c;
  
  protected c(Context paramContext, h paramh, e parame)
  {
    a = paramContext.getApplicationContext();
    b = paramh;
    c = parame;
  }
  
  private void a(f paramf, i parami, List<ContentProviderOperation> paramList, Message paramMessage, boolean paramBoolean, Set<Long> paramSet)
  {
    ContentProviderOperation.Builder localBuilder = ContentProviderOperation.newInsert(TruecallerContract.aa.a());
    Object localObject = m;
    int j = paramList.size();
    parami = a(((TransportInfo)localObject).e(), paramf, parami, c, paramBoolean);
    a(parami);
    localObject = parami.iterator();
    int i = -1;
    while (((Iterator)localObject).hasNext())
    {
      Participant localParticipant = (Participant)((Iterator)localObject).next();
      if (c.equals(localParticipant)) {
        i = com.truecaller.messaging.data.b.a(paramList, localParticipant);
      } else {
        com.truecaller.messaging.data.b.a(paramList, localParticipant);
      }
    }
    if (parami.isEmpty())
    {
      paramList.subList(j, paramList.size()).clear();
      return;
    }
    j = i;
    if (i == -1) {
      j = com.truecaller.messaging.data.b.a(paramList, c);
    }
    i = com.truecaller.messaging.data.b.a(paramList, parami);
    localBuilder.withValueBackReference("participant_id", j);
    localBuilder.withValueBackReference("conversation_id", i);
    localBuilder.withValue("date_sent", Long.valueOf(d.a));
    localBuilder.withValue("date", Long.valueOf(e.a));
    localBuilder.withValue("status", Integer.valueOf(f));
    localBuilder.withValue("seen", Boolean.valueOf(g));
    localBuilder.withValue("read", Boolean.valueOf(h));
    localBuilder.withValue("locked", Boolean.valueOf(i));
    localBuilder.withValue("transport", Integer.valueOf(j));
    localBuilder.withValue("sim_token", l);
    localBuilder.withValue("analytics_id", o);
    localBuilder.withValue("raw_address", p);
    localBuilder.withValue("category", Integer.valueOf(2));
    paramBoolean = a(f);
    i = 0;
    if (paramBoolean)
    {
      localBuilder.withValue("classification", Integer.valueOf(0));
      paramSet.add(Long.valueOf(-1L));
    }
    else
    {
      localBuilder.withValue("classification", Integer.valueOf(2));
    }
    localBuilder.withValue("sync_status", Integer.valueOf(r));
    j = paramList.size();
    paramList.add(localBuilder.build());
    a(paramf, paramList, m, j);
    paramf = new ContentValues();
    parami = n;
    int k = parami.length;
    while (i < k)
    {
      paramMessage = parami[i];
      paramSet = ContentProviderOperation.newInsert(TruecallerContract.z.a());
      paramSet.withValueBackReference("message_id", j);
      paramf.clear();
      paramMessage.a(paramf);
      paramSet.withValues(paramf);
      paramList.add(paramSet.build());
      i += 1;
    }
  }
  
  private static void a(Set<Participant> paramSet)
  {
    paramSet = paramSet.iterator();
    while (paramSet.hasNext()) {
      if (((Participant)paramSet.next()).c()) {
        paramSet.remove();
      }
    }
  }
  
  /* Error */
  public long a(f paramf, i parami, r paramr, org.a.a.b paramb1, org.a.a.b paramb2, int paramInt, List<ContentProviderOperation> paramList, com.truecaller.utils.q paramq, boolean paramBoolean1, boolean paramBoolean2, Set<Long> paramSet)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 25
    //   3: aconst_null
    //   4: astore 24
    //   6: aload_0
    //   7: aload_0
    //   8: getfield 26	com/truecaller/messaging/transport/c:a	Landroid/content/Context;
    //   11: invokevirtual 260	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   14: aload_1
    //   15: aload_2
    //   16: aload 4
    //   18: aload 5
    //   20: iload 9
    //   22: iload 10
    //   24: invokevirtual 263	com/truecaller/messaging/transport/c:a	(Landroid/content/ContentResolver;Lcom/truecaller/messaging/transport/f;Lcom/truecaller/messaging/transport/i;Lorg/a/a/b;Lorg/a/a/b;ZZ)Lcom/truecaller/messaging/transport/c$a;
    //   27: astore 4
    //   29: aload 4
    //   31: ifnull +1141 -> 1172
    //   34: aload_3
    //   35: invokeinterface 268 1 0
    //   40: istore 10
    //   42: aload 4
    //   44: invokeinterface 269 1 0
    //   49: istore 17
    //   51: iconst_0
    //   52: istore 15
    //   54: iconst_0
    //   55: istore 13
    //   57: iconst_0
    //   58: istore 14
    //   60: iload 6
    //   62: istore 12
    //   64: iload 15
    //   66: istore 6
    //   68: iload 10
    //   70: ifeq +1208 -> 1278
    //   73: iload 17
    //   75: ifeq +1203 -> 1278
    //   78: aload_3
    //   79: invokeinterface 270 1 0
    //   84: aload 4
    //   86: invokeinterface 272 1 0
    //   91: lcmp
    //   92: ifle +45 -> 137
    //   95: aload_3
    //   96: invokeinterface 270 1 0
    //   101: lstore 18
    //   103: aload 7
    //   105: aload_3
    //   106: invokeinterface 274 1 0
    //   111: invokestatic 277	com/truecaller/messaging/data/b:a	(Ljava/util/List;J)V
    //   114: iload 14
    //   116: iconst_1
    //   117: iadd
    //   118: istore 14
    //   120: iload 12
    //   122: iconst_1
    //   123: isub
    //   124: istore 12
    //   126: aload_3
    //   127: invokeinterface 268 1 0
    //   132: istore 10
    //   134: goto +667 -> 801
    //   137: aload_3
    //   138: invokeinterface 270 1 0
    //   143: aload 4
    //   145: invokeinterface 272 1 0
    //   150: lcmp
    //   151: ifge +55 -> 206
    //   154: aload 4
    //   156: invokeinterface 272 1 0
    //   161: lstore 18
    //   163: aload_0
    //   164: aload_1
    //   165: aload_2
    //   166: aload 7
    //   168: aload 4
    //   170: invokeinterface 280 1 0
    //   175: iload 9
    //   177: aload 11
    //   179: invokespecial 282	com/truecaller/messaging/transport/c:a	(Lcom/truecaller/messaging/transport/f;Lcom/truecaller/messaging/transport/i;Ljava/util/List;Lcom/truecaller/messaging/data/types/Message;ZLjava/util/Set;)V
    //   182: iload 13
    //   184: iconst_1
    //   185: iadd
    //   186: istore 13
    //   188: iload 12
    //   190: iconst_1
    //   191: isub
    //   192: istore 12
    //   194: aload 4
    //   196: invokeinterface 269 1 0
    //   201: istore 17
    //   203: goto +598 -> 801
    //   206: aload_3
    //   207: invokeinterface 284 1 0
    //   212: aload 4
    //   214: invokeinterface 285 1 0
    //   219: lcmp
    //   220: ifle +45 -> 265
    //   223: aload_3
    //   224: invokeinterface 270 1 0
    //   229: lstore 18
    //   231: aload 7
    //   233: aload_3
    //   234: invokeinterface 274 1 0
    //   239: invokestatic 277	com/truecaller/messaging/data/b:a	(Ljava/util/List;J)V
    //   242: iload 14
    //   244: iconst_1
    //   245: iadd
    //   246: istore 14
    //   248: iload 12
    //   250: iconst_1
    //   251: isub
    //   252: istore 12
    //   254: aload_3
    //   255: invokeinterface 268 1 0
    //   260: istore 10
    //   262: goto +539 -> 801
    //   265: aload_3
    //   266: invokeinterface 284 1 0
    //   271: aload 4
    //   273: invokeinterface 285 1 0
    //   278: lcmp
    //   279: ifge +55 -> 334
    //   282: aload 4
    //   284: invokeinterface 272 1 0
    //   289: lstore 18
    //   291: aload_0
    //   292: aload_1
    //   293: aload_2
    //   294: aload 7
    //   296: aload 4
    //   298: invokeinterface 280 1 0
    //   303: iload 9
    //   305: aload 11
    //   307: invokespecial 282	com/truecaller/messaging/transport/c:a	(Lcom/truecaller/messaging/transport/f;Lcom/truecaller/messaging/transport/i;Ljava/util/List;Lcom/truecaller/messaging/data/types/Message;ZLjava/util/Set;)V
    //   310: iload 13
    //   312: iconst_1
    //   313: iadd
    //   314: istore 13
    //   316: iload 12
    //   318: iconst_1
    //   319: isub
    //   320: istore 12
    //   322: aload 4
    //   324: invokeinterface 269 1 0
    //   329: istore 17
    //   331: goto +470 -> 801
    //   334: aload_0
    //   335: aload_3
    //   336: aload 4
    //   338: invokevirtual 288	com/truecaller/messaging/transport/c:b	(Lcom/truecaller/messaging/data/a/r;Lcom/truecaller/messaging/transport/c$a;)Z
    //   341: ifeq +107 -> 448
    //   344: aload_3
    //   345: invokeinterface 290 1 0
    //   350: istore 15
    //   352: aload 7
    //   354: aload_3
    //   355: invokeinterface 274 1 0
    //   360: invokestatic 277	com/truecaller/messaging/data/b:a	(Ljava/util/List;J)V
    //   363: iload 14
    //   365: iconst_1
    //   366: iadd
    //   367: istore 14
    //   369: aload_3
    //   370: invokeinterface 268 1 0
    //   375: istore 10
    //   377: aload 4
    //   379: invokeinterface 272 1 0
    //   384: lstore 18
    //   386: aload 4
    //   388: invokeinterface 280 1 0
    //   393: invokevirtual 293	com/truecaller/messaging/data/types/Message:m	()Lcom/truecaller/messaging/data/types/Message$a;
    //   396: astore 5
    //   398: aload 5
    //   400: iload 15
    //   402: putfield 298	com/truecaller/messaging/data/types/Message$a:s	I
    //   405: aload_0
    //   406: aload_1
    //   407: aload_2
    //   408: aload 7
    //   410: aload 5
    //   412: invokevirtual 300	com/truecaller/messaging/data/types/Message$a:b	()Lcom/truecaller/messaging/data/types/Message;
    //   415: iload 9
    //   417: aload 11
    //   419: invokespecial 282	com/truecaller/messaging/transport/c:a	(Lcom/truecaller/messaging/transport/f;Lcom/truecaller/messaging/transport/i;Ljava/util/List;Lcom/truecaller/messaging/data/types/Message;ZLjava/util/Set;)V
    //   422: iload 13
    //   424: iconst_1
    //   425: iadd
    //   426: istore 13
    //   428: iload 12
    //   430: iconst_1
    //   431: isub
    //   432: iconst_1
    //   433: isub
    //   434: istore 12
    //   436: aload 4
    //   438: invokeinterface 269 1 0
    //   443: istore 17
    //   445: goto +356 -> 801
    //   448: aload 4
    //   450: invokeinterface 272 1 0
    //   455: lstore 18
    //   457: aload_0
    //   458: aload_3
    //   459: aload 4
    //   461: invokevirtual 302	com/truecaller/messaging/transport/c:a	(Lcom/truecaller/messaging/data/a/r;Lcom/truecaller/messaging/transport/c$a;)Z
    //   464: ifeq +781 -> 1245
    //   467: aload_3
    //   468: invokeinterface 305 1 0
    //   473: i2l
    //   474: lstore 20
    //   476: aload 4
    //   478: invokeinterface 306 1 0
    //   483: i2l
    //   484: lstore 22
    //   486: aload_0
    //   487: aload_1
    //   488: aload_2
    //   489: aload 7
    //   491: aload_3
    //   492: aload 4
    //   494: iload 9
    //   496: invokevirtual 309	com/truecaller/messaging/transport/c:a	(Lcom/truecaller/messaging/transport/f;Lcom/truecaller/messaging/transport/i;Ljava/util/List;Lcom/truecaller/messaging/data/a/r;Lcom/truecaller/messaging/transport/c$a;Z)Z
    //   499: ifeq +733 -> 1232
    //   502: iload 6
    //   504: iconst_1
    //   505: iadd
    //   506: istore 15
    //   508: iload 12
    //   510: iconst_1
    //   511: isub
    //   512: istore 12
    //   514: goto +3 -> 517
    //   517: lload 20
    //   519: aload_3
    //   520: invokeinterface 305 1 0
    //   525: i2l
    //   526: lcmp
    //   527: ifne +712 -> 1239
    //   530: lload 22
    //   532: aload 4
    //   534: invokeinterface 306 1 0
    //   539: i2l
    //   540: lcmp
    //   541: ifne +698 -> 1239
    //   544: iconst_1
    //   545: istore 10
    //   547: goto +3 -> 550
    //   550: iload 10
    //   552: iconst_1
    //   553: anewarray 311	java/lang/String
    //   556: dup
    //   557: iconst_0
    //   558: ldc_w 313
    //   561: aastore
    //   562: invokestatic 319	com/truecaller/log/AssertionUtil$AlwaysFatal:isTrue	(Z[Ljava/lang/String;)V
    //   565: goto +3 -> 568
    //   568: iload 15
    //   570: istore 6
    //   572: iload 12
    //   574: istore 16
    //   576: aload_3
    //   577: invokeinterface 290 1 0
    //   582: iconst_1
    //   583: if_icmpne +102 -> 685
    //   586: iload 15
    //   588: istore 6
    //   590: iload 12
    //   592: istore 16
    //   594: aload_3
    //   595: invokeinterface 321 1 0
    //   600: aload 4
    //   602: invokeinterface 323 1 0
    //   607: if_icmpne +78 -> 685
    //   610: iload 15
    //   612: istore 6
    //   614: iload 12
    //   616: istore 16
    //   618: aload_3
    //   619: invokeinterface 325 1 0
    //   624: aload 4
    //   626: invokeinterface 327 1 0
    //   631: if_icmpne +54 -> 685
    //   634: aload_3
    //   635: invokeinterface 274 1 0
    //   640: invokestatic 330	com/truecaller/content/TruecallerContract$aa:a	(J)Landroid/net/Uri;
    //   643: invokestatic 333	android/content/ContentProviderOperation:newUpdate	(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;
    //   646: astore 5
    //   648: aload 5
    //   650: ldc -44
    //   652: iconst_0
    //   653: invokestatic 156	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   656: invokevirtual 141	android/content/ContentProviderOperation$Builder:withValue	(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;
    //   659: pop
    //   660: aload 7
    //   662: aload 5
    //   664: invokevirtual 219	android/content/ContentProviderOperation$Builder:build	()Landroid/content/ContentProviderOperation;
    //   667: invokeinterface 220 2 0
    //   672: pop
    //   673: iload 15
    //   675: iconst_1
    //   676: iadd
    //   677: istore 6
    //   679: iload 12
    //   681: iconst_1
    //   682: isub
    //   683: istore 16
    //   685: aload_0
    //   686: getfield 30	com/truecaller/messaging/transport/c:c	Lcom/truecaller/featuretoggles/e;
    //   689: invokevirtual 339	com/truecaller/featuretoggles/e:w	()Lcom/truecaller/featuretoggles/b;
    //   692: invokeinterface 343 1 0
    //   697: ifeq +571 -> 1268
    //   700: aload_3
    //   701: invokeinterface 345 1 0
    //   706: ifne +555 -> 1261
    //   709: aload_0
    //   710: aload_3
    //   711: invokeinterface 347 1 0
    //   716: invokevirtual 203	com/truecaller/messaging/transport/c:a	(I)Z
    //   719: ifeq +23 -> 742
    //   722: aload 11
    //   724: aload_3
    //   725: invokeinterface 274 1 0
    //   730: invokestatic 137	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   733: invokeinterface 210 2 0
    //   738: pop
    //   739: goto +513 -> 1252
    //   742: aload_3
    //   743: invokeinterface 274 1 0
    //   748: invokestatic 330	com/truecaller/content/TruecallerContract$aa:a	(J)Landroid/net/Uri;
    //   751: invokestatic 333	android/content/ContentProviderOperation:newUpdate	(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;
    //   754: astore 5
    //   756: aload 5
    //   758: ldc -51
    //   760: iconst_2
    //   761: invokestatic 156	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   764: invokevirtual 141	android/content/ContentProviderOperation$Builder:withValue	(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;
    //   767: pop
    //   768: aload 7
    //   770: aload 5
    //   772: invokevirtual 219	android/content/ContentProviderOperation$Builder:build	()Landroid/content/ContentProviderOperation;
    //   775: invokeinterface 220 2 0
    //   780: pop
    //   781: goto +471 -> 1252
    //   784: aload_3
    //   785: invokeinterface 268 1 0
    //   790: istore 10
    //   792: aload 4
    //   794: invokeinterface 269 1 0
    //   799: istore 17
    //   801: iload 12
    //   803: ifgt +472 -> 1275
    //   806: new 349	java/lang/StringBuilder
    //   809: dup
    //   810: ldc_w 351
    //   813: invokespecial 354	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   816: astore_1
    //   817: aload_1
    //   818: iload 13
    //   820: invokevirtual 358	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   823: pop
    //   824: aload_1
    //   825: ldc_w 360
    //   828: invokevirtual 363	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   831: pop
    //   832: aload_1
    //   833: iload 6
    //   835: invokevirtual 358	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   838: pop
    //   839: aload_1
    //   840: ldc_w 365
    //   843: invokevirtual 363	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   846: pop
    //   847: aload_1
    //   848: iload 14
    //   850: invokevirtual 358	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   853: pop
    //   854: aload_1
    //   855: ldc_w 367
    //   858: invokevirtual 363	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   861: pop
    //   862: aload_1
    //   863: invokevirtual 371	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   866: pop
    //   867: lload 18
    //   869: invokestatic 376	com/truecaller/messaging/transport/l$b:a	(J)J
    //   872: lstore 18
    //   874: goto +272 -> 1146
    //   877: iload 17
    //   879: ifeq +402 -> 1281
    //   882: aload 4
    //   884: invokeinterface 280 1 0
    //   889: astore 5
    //   891: aload_0
    //   892: aload_1
    //   893: aload_2
    //   894: aload 7
    //   896: aload 5
    //   898: iload 9
    //   900: aload 11
    //   902: invokespecial 282	com/truecaller/messaging/transport/c:a	(Lcom/truecaller/messaging/transport/f;Lcom/truecaller/messaging/transport/i;Ljava/util/List;Lcom/truecaller/messaging/data/types/Message;ZLjava/util/Set;)V
    //   905: iload 13
    //   907: iconst_1
    //   908: iadd
    //   909: istore 13
    //   911: iload 12
    //   913: iconst_1
    //   914: isub
    //   915: istore 12
    //   917: iload 12
    //   919: ifgt +80 -> 999
    //   922: new 349	java/lang/StringBuilder
    //   925: dup
    //   926: ldc_w 351
    //   929: invokespecial 354	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   932: astore_1
    //   933: aload_1
    //   934: iload 13
    //   936: invokevirtual 358	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   939: pop
    //   940: aload_1
    //   941: ldc_w 360
    //   944: invokevirtual 363	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   947: pop
    //   948: aload_1
    //   949: iload 6
    //   951: invokevirtual 358	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   954: pop
    //   955: aload_1
    //   956: ldc_w 365
    //   959: invokevirtual 363	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   962: pop
    //   963: aload_1
    //   964: iload 14
    //   966: invokevirtual 358	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   969: pop
    //   970: aload_1
    //   971: ldc_w 367
    //   974: invokevirtual 363	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   977: pop
    //   978: aload_1
    //   979: invokevirtual 371	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   982: pop
    //   983: aload 5
    //   985: getfield 145	com/truecaller/messaging/data/types/Message:e	Lorg/a/a/b;
    //   988: getfield 131	org/a/a/a/g:a	J
    //   991: invokestatic 376	com/truecaller/messaging/transport/l$b:a	(J)J
    //   994: lstore 18
    //   996: goto +150 -> 1146
    //   999: aload 4
    //   1001: invokeinterface 269 1 0
    //   1006: istore 17
    //   1008: goto -131 -> 877
    //   1011: iload 9
    //   1013: ifeq +31 -> 1044
    //   1016: aload 7
    //   1018: aload_3
    //   1019: invokeinterface 274 1 0
    //   1024: invokestatic 277	com/truecaller/messaging/data/b:a	(Ljava/util/List;J)V
    //   1027: iload 14
    //   1029: iconst_1
    //   1030: iadd
    //   1031: istore 14
    //   1033: aload_3
    //   1034: invokeinterface 268 1 0
    //   1039: istore 9
    //   1041: goto -30 -> 1011
    //   1044: aload 8
    //   1046: ldc_w 378
    //   1049: iload 13
    //   1051: invokeinterface 383 3 0
    //   1056: aload 8
    //   1058: ldc_w 385
    //   1061: iload 6
    //   1063: invokeinterface 383 3 0
    //   1068: aload 8
    //   1070: ldc_w 387
    //   1073: iload 14
    //   1075: invokeinterface 383 3 0
    //   1080: new 349	java/lang/StringBuilder
    //   1083: dup
    //   1084: ldc_w 389
    //   1087: invokespecial 354	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1090: astore_1
    //   1091: aload_1
    //   1092: iload 13
    //   1094: invokevirtual 358	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1097: pop
    //   1098: aload_1
    //   1099: ldc_w 360
    //   1102: invokevirtual 363	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1105: pop
    //   1106: aload_1
    //   1107: iload 6
    //   1109: invokevirtual 358	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1112: pop
    //   1113: aload_1
    //   1114: ldc_w 365
    //   1117: invokevirtual 363	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1120: pop
    //   1121: aload_1
    //   1122: iload 14
    //   1124: invokevirtual 358	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1127: pop
    //   1128: aload_1
    //   1129: ldc_w 367
    //   1132: invokevirtual 363	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1135: pop
    //   1136: aload_1
    //   1137: invokevirtual 371	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1140: pop
    //   1141: ldc2_w 390
    //   1144: lstore 18
    //   1146: aload 4
    //   1148: ifnull +10 -> 1158
    //   1151: aload 4
    //   1153: invokeinterface 394 1 0
    //   1158: lload 18
    //   1160: lreturn
    //   1161: astore_1
    //   1162: goto +56 -> 1218
    //   1165: astore_2
    //   1166: aload 4
    //   1168: astore_1
    //   1169: goto +30 -> 1199
    //   1172: aload 4
    //   1174: ifnull +42 -> 1216
    //   1177: aload 4
    //   1179: invokeinterface 394 1 0
    //   1184: goto +32 -> 1216
    //   1187: astore_1
    //   1188: aload 24
    //   1190: astore 4
    //   1192: goto +26 -> 1218
    //   1195: astore_2
    //   1196: aload 25
    //   1198: astore_1
    //   1199: aload_1
    //   1200: astore 24
    //   1202: aload_2
    //   1203: invokestatic 400	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   1206: aload_1
    //   1207: ifnull +9 -> 1216
    //   1210: aload_1
    //   1211: invokeinterface 394 1 0
    //   1216: lconst_0
    //   1217: lreturn
    //   1218: aload 4
    //   1220: ifnull +10 -> 1230
    //   1223: aload 4
    //   1225: invokeinterface 394 1 0
    //   1230: aload_1
    //   1231: athrow
    //   1232: iload 6
    //   1234: istore 15
    //   1236: goto -719 -> 517
    //   1239: iconst_0
    //   1240: istore 10
    //   1242: goto -692 -> 550
    //   1245: iload 6
    //   1247: istore 15
    //   1249: goto -681 -> 568
    //   1252: iload 16
    //   1254: iconst_1
    //   1255: isub
    //   1256: istore 12
    //   1258: goto -474 -> 784
    //   1261: iload 16
    //   1263: istore 12
    //   1265: goto -481 -> 784
    //   1268: iload 16
    //   1270: istore 12
    //   1272: goto -488 -> 784
    //   1275: goto -1207 -> 68
    //   1278: goto -401 -> 877
    //   1281: iload 10
    //   1283: istore 9
    //   1285: goto -274 -> 1011
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1288	0	this	c
    //   0	1288	1	paramf	f
    //   0	1288	2	parami	i
    //   0	1288	3	paramr	r
    //   0	1288	4	paramb1	org.a.a.b
    //   0	1288	5	paramb2	org.a.a.b
    //   0	1288	6	paramInt	int
    //   0	1288	7	paramList	List<ContentProviderOperation>
    //   0	1288	8	paramq	com.truecaller.utils.q
    //   0	1288	9	paramBoolean1	boolean
    //   0	1288	10	paramBoolean2	boolean
    //   0	1288	11	paramSet	Set<Long>
    //   62	1209	12	i	int
    //   55	1038	13	j	int
    //   58	1065	14	k	int
    //   52	1196	15	m	int
    //   574	695	16	n	int
    //   49	958	17	bool	boolean
    //   101	1058	18	l1	long
    //   474	44	20	l2	long
    //   484	47	22	l3	long
    //   4	1197	24	localf	f
    //   1	1196	25	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   34	51	1161	finally
    //   78	114	1161	finally
    //   126	134	1161	finally
    //   137	182	1161	finally
    //   194	203	1161	finally
    //   206	242	1161	finally
    //   254	262	1161	finally
    //   265	310	1161	finally
    //   322	331	1161	finally
    //   334	363	1161	finally
    //   369	422	1161	finally
    //   436	445	1161	finally
    //   448	502	1161	finally
    //   517	544	1161	finally
    //   550	565	1161	finally
    //   576	586	1161	finally
    //   594	610	1161	finally
    //   618	673	1161	finally
    //   685	739	1161	finally
    //   742	781	1161	finally
    //   784	801	1161	finally
    //   806	874	1161	finally
    //   882	905	1161	finally
    //   922	996	1161	finally
    //   999	1008	1161	finally
    //   1016	1027	1161	finally
    //   1033	1041	1161	finally
    //   1044	1141	1161	finally
    //   34	51	1165	java/lang/RuntimeException
    //   78	114	1165	java/lang/RuntimeException
    //   126	134	1165	java/lang/RuntimeException
    //   137	182	1165	java/lang/RuntimeException
    //   194	203	1165	java/lang/RuntimeException
    //   206	242	1165	java/lang/RuntimeException
    //   254	262	1165	java/lang/RuntimeException
    //   265	310	1165	java/lang/RuntimeException
    //   322	331	1165	java/lang/RuntimeException
    //   334	363	1165	java/lang/RuntimeException
    //   369	422	1165	java/lang/RuntimeException
    //   436	445	1165	java/lang/RuntimeException
    //   448	502	1165	java/lang/RuntimeException
    //   517	544	1165	java/lang/RuntimeException
    //   550	565	1165	java/lang/RuntimeException
    //   576	586	1165	java/lang/RuntimeException
    //   594	610	1165	java/lang/RuntimeException
    //   618	673	1165	java/lang/RuntimeException
    //   685	739	1165	java/lang/RuntimeException
    //   742	781	1165	java/lang/RuntimeException
    //   784	801	1165	java/lang/RuntimeException
    //   806	874	1165	java/lang/RuntimeException
    //   882	905	1165	java/lang/RuntimeException
    //   922	996	1165	java/lang/RuntimeException
    //   999	1008	1165	java/lang/RuntimeException
    //   1016	1027	1165	java/lang/RuntimeException
    //   1033	1041	1165	java/lang/RuntimeException
    //   1044	1141	1165	java/lang/RuntimeException
    //   6	29	1187	finally
    //   1202	1206	1187	finally
    //   6	29	1195	java/lang/RuntimeException
  }
  
  protected abstract RC a(ContentResolver paramContentResolver, f paramf, i parami, org.a.a.b paramb1, org.a.a.b paramb2, boolean paramBoolean1, boolean paramBoolean2);
  
  protected abstract Set<Participant> a(long paramLong, f paramf, i parami, Participant paramParticipant, boolean paramBoolean);
  
  protected abstract void a(f paramf, List<ContentProviderOperation> paramList, T paramT, int paramInt);
  
  protected abstract boolean a(int paramInt);
  
  protected abstract boolean a(r paramr, RC paramRC);
  
  protected abstract boolean a(f paramf, i parami, List<ContentProviderOperation> paramList, r paramr, RC paramRC, boolean paramBoolean);
  
  protected abstract boolean b(r paramr, RC paramRC);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */