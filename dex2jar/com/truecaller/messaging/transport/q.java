package com.truecaller.messaging.transport;

import com.truecaller.androidactors.f;
import com.truecaller.messaging.c.a;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.transport.a.c;
import dagger.a.d;
import javax.inject.Provider;

public final class q
  implements d<c>
{
  private final o a;
  private final Provider<m> b;
  private final Provider<f<t>> c;
  private final Provider<a> d;
  
  private q(o paramo, Provider<m> paramProvider, Provider<f<t>> paramProvider1, Provider<a> paramProvider2)
  {
    a = paramo;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
  }
  
  public static q a(o paramo, Provider<m> paramProvider, Provider<f<t>> paramProvider1, Provider<a> paramProvider2)
  {
    return new q(paramo, paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */