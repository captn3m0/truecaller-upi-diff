package com.truecaller.messaging.transport;

import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import java.util.List;

final class ad$c
  implements ad.b
{
  private final ContentResolver a;
  private boolean b = true;
  
  ad$c(ContentResolver paramContentResolver)
  {
    a = paramContentResolver;
  }
  
  public final ContentProviderResult[] a(ad paramad)
    throws OperationApplicationException, RemoteException, SecurityException
  {
    boolean bool = b;
    int i = 0;
    if (bool) {}
    try
    {
      try
      {
        localObject = paramad.a(a);
        if (localObject != null) {
          return (ContentProviderResult[])localObject;
        }
        b = false;
      }
      catch (RemoteException paramad)
      {
        AssertionUtil.reportThrowableButNeverCrash(paramad);
        return ad.d();
      }
    }
    catch (SecurityException|NullPointerException|OperationApplicationException localSecurityException)
    {
      Object localObject;
      ContentProviderResult[] arrayOfContentProviderResult;
      int j;
      for (;;) {}
    }
    b = false;
    localObject = a;
    if ((c != null) && (!c.isEmpty()))
    {
      arrayOfContentProviderResult = new ContentProviderResult[c.size()];
      j = c.size();
      while (i < j)
      {
        ad.a locala = (ad.a)c.get(i);
        switch (a)
        {
        default: 
          AssertionUtil.AlwaysFatal.fail(new String[] { "Unsupported operation" });
          return ad.a;
        case 2: 
          arrayOfContentProviderResult[i] = new ContentProviderResult(((ContentResolver)localObject).delete(b, d, e));
          break;
        case 1: 
          arrayOfContentProviderResult[i] = new ContentProviderResult(((ContentResolver)localObject).update(b, c, d, e));
          break;
        case 0: 
          arrayOfContentProviderResult[i] = new ContentProviderResult(((ContentResolver)localObject).insert(b, c));
        }
        i += 1;
      }
      return arrayOfContentProviderResult;
    }
    return ad.a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.ad.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */