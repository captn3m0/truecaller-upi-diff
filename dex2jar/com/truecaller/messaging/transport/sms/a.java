package com.truecaller.messaging.transport.sms;

import android.content.ContentProviderOperation;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.aa;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.transport.ad;
import com.truecaller.messaging.transport.ad.a.a;
import com.truecaller.messaging.transport.ad.b;
import com.truecaller.messaging.transport.f;
import com.truecaller.messaging.transport.i;
import com.truecaller.messaging.transport.l;
import com.truecaller.messaging.transport.l.a;
import com.truecaller.utils.q;
import java.util.List;
import java.util.Set;
import org.a.a.b;

public final class a
  implements l<ad>
{
  private final l<ad> a;
  private final ad.b b;
  
  public a(l<ad> paraml, ad.b paramb)
  {
    a = paraml;
    b = paramb;
  }
  
  public final long a(long paramLong)
  {
    return a.a(paramLong);
  }
  
  public final long a(f paramf, i parami, r paramr, b paramb1, b paramb2, int paramInt, List<? extends ContentProviderOperation> paramList, q paramq, boolean paramBoolean1, boolean paramBoolean2, Set<Long> paramSet)
  {
    c.g.b.k.b(paramf, "threadInfoCache");
    c.g.b.k.b(parami, "participantCache");
    c.g.b.k.b(paramr, "cursor");
    c.g.b.k.b(paramb1, "timeTo");
    c.g.b.k.b(paramb2, "timeFrom");
    c.g.b.k.b(paramList, "operations");
    c.g.b.k.b(paramq, "trace");
    c.g.b.k.b(paramSet, "messagesToClassify");
    return a.a(paramf, parami, paramr, paramb1, paramb2, paramInt, paramList, paramq, paramBoolean1, paramBoolean2, paramSet);
  }
  
  public final l.a a(Message paramMessage, Participant[] paramArrayOfParticipant)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramArrayOfParticipant, "recipients");
    paramMessage = a.a(paramMessage, paramArrayOfParticipant);
    c.g.b.k.a(paramMessage, "transport.enqueueMessage(message, recipients)");
    return paramMessage;
  }
  
  public final String a()
  {
    String str = a.a();
    c.g.b.k.a(str, "transport.name");
    return str;
  }
  
  public final String a(String paramString)
  {
    c.g.b.k.b(paramString, "simToken");
    paramString = a.a(paramString);
    c.g.b.k.a(paramString, "transport.prepareSimTokenToStore(simToken)");
    return paramString;
  }
  
  public final void a(Intent paramIntent, int paramInt)
  {
    c.g.b.k.b(paramIntent, "intent");
    a.a(paramIntent, paramInt);
  }
  
  public final void a(BinaryEntity paramBinaryEntity)
  {
    c.g.b.k.b(paramBinaryEntity, "entity");
    a.a(paramBinaryEntity);
  }
  
  public final void a(Message paramMessage, String paramString1, String paramString2)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramString2, "initiatedVia");
    throw ((Throwable)new IllegalStateException("No SMS Permission transport does not support sending reactions"));
  }
  
  public final void a(b paramb)
  {
    c.g.b.k.b(paramb, "time");
    a.a(paramb);
  }
  
  public final boolean a(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return a.a(paramMessage);
  }
  
  public final boolean a(Message paramMessage, Entity paramEntity)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramEntity, "entity");
    return a.a(paramMessage, paramEntity);
  }
  
  public final boolean a(Message paramMessage, ad paramad)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramad, "transaction");
    return false;
  }
  
  public final boolean a(Participant paramParticipant)
  {
    c.g.b.k.b(paramParticipant, "participant");
    return a.a(paramParticipant);
  }
  
  public final boolean a(TransportInfo paramTransportInfo, long paramLong1, long paramLong2, ad paramad)
  {
    c.g.b.k.b(paramTransportInfo, "info");
    c.g.b.k.b(paramad, "transaction");
    paramTransportInfo = paramad.a(TruecallerContract.aa.a(paramTransportInfo.c()));
    paramTransportInfo.a("read", Integer.valueOf(1));
    paramTransportInfo.a("seen", Integer.valueOf(1));
    paramTransportInfo.a("sync_status", Integer.valueOf(1));
    paramad.a(paramTransportInfo.a());
    return true;
  }
  
  public final boolean a(TransportInfo paramTransportInfo, ad paramad)
  {
    c.g.b.k.b(paramTransportInfo, "info");
    c.g.b.k.b(paramad, "transaction");
    paramTransportInfo = paramad.a(TruecallerContract.aa.a(paramTransportInfo.c()));
    paramTransportInfo.a("seen", Integer.valueOf(1));
    paramTransportInfo.a("sync_status", Integer.valueOf(1));
    paramad.a(paramTransportInfo.a());
    return true;
  }
  
  public final boolean a(TransportInfo paramTransportInfo, ad paramad, boolean paramBoolean)
  {
    c.g.b.k.b(paramTransportInfo, "info");
    c.g.b.k.b(paramad, "transaction");
    return false;
  }
  
  public final boolean a(ad paramad)
  {
    c.g.b.k.b(paramad, "transaction");
    int i = 0;
    try
    {
      paramad = b.a(paramad);
      c.g.b.k.a(paramad, "transactionExecutor.execute(transaction)");
      int j = paramad.length;
      if (j == 0) {
        i = 1;
      }
      return i ^ 0x1;
    }
    catch (OperationApplicationException paramad)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramad);
      return false;
    }
    catch (SecurityException paramad)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramad);
      return false;
    }
    catch (RemoteException paramad)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramad);
    }
    return false;
  }
  
  public final boolean a(String paramString, com.truecaller.messaging.transport.a parama)
  {
    c.g.b.k.b(paramString, "text");
    c.g.b.k.b(parama, "result");
    return a.a(paramString, parama);
  }
  
  public final ad b()
  {
    return new ad(TruecallerContract.a());
  }
  
  public final void b(long paramLong)
  {
    a.b(paramLong);
  }
  
  public final boolean b(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return a.b(paramMessage);
  }
  
  public final boolean b(ad paramad)
  {
    c.g.b.k.b(paramad, "transaction");
    return (!paramad.a()) && (c.g.b.k.a(paramad.c(), TruecallerContract.a()));
  }
  
  public final boolean c()
  {
    return a.c();
  }
  
  public final boolean c(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return a.c(paramMessage);
  }
  
  public final com.truecaller.messaging.transport.k d(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    paramMessage = a.d(paramMessage);
    c.g.b.k.a(paramMessage, "transport.sendMessage(message)");
    return paramMessage;
  }
  
  public final b d()
  {
    b localb = a.d();
    c.g.b.k.a(localb, "transport.lastSyncTime");
    return localb;
  }
  
  public final int e(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return a.e(paramMessage);
  }
  
  public final boolean e()
  {
    return a.e();
  }
  
  public final int f()
  {
    return a.f();
  }
  
  public final boolean f(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return a.f(paramMessage);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.sms.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */