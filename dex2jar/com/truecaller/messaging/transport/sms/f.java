package com.truecaller.messaging.transport.sms;

import com.truecaller.messaging.transport.ad.b;
import com.truecaller.messaging.transport.l;
import dagger.a.d;
import javax.inject.Provider;

public final class f
  implements d<l>
{
  private final c a;
  private final Provider<l> b;
  private final Provider<ad.b> c;
  
  private f(c paramc, Provider<l> paramProvider, Provider<ad.b> paramProvider1)
  {
    a = paramc;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static f a(c paramc, Provider<l> paramProvider, Provider<ad.b> paramProvider1)
  {
    return new f(paramc, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.sms.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */