package com.truecaller.messaging.transport.sms;

import android.content.Context;
import com.truecaller.featuretoggles.e;
import com.truecaller.multisim.h;
import javax.inject.Provider;

public final class d
  implements dagger.a.d<g>
{
  private final c a;
  private final Provider<Context> b;
  private final Provider<h> c;
  private final Provider<e> d;
  
  private d(c paramc, Provider<Context> paramProvider, Provider<h> paramProvider1, Provider<e> paramProvider2)
  {
    a = paramc;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
  }
  
  public static d a(c paramc, Provider<Context> paramProvider, Provider<h> paramProvider1, Provider<e> paramProvider2)
  {
    return new d(paramc, paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.sms.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */