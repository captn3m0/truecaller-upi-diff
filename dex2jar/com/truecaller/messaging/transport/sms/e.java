package com.truecaller.messaging.transport.sms;

import android.content.Context;
import android.os.HandlerThread;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.b;
import com.truecaller.androidactors.f;
import com.truecaller.common.h.u;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.transport.ad.b;
import com.truecaller.payments.j;
import javax.inject.Provider;

public final class e
  implements dagger.a.d<com.truecaller.messaging.transport.l>
{
  private final c a;
  private final Provider<Context> b;
  private final Provider<f<ae>> c;
  private final Provider<HandlerThread> d;
  private final Provider<com.truecaller.utils.d> e;
  private final Provider<f<t>> f;
  private final Provider<g> g;
  private final Provider<com.truecaller.messaging.h> h;
  private final Provider<f<com.truecaller.messaging.notifications.a>> i;
  private final Provider<com.truecaller.multisim.h> j;
  private final Provider<u> k;
  private final Provider<ad.b> l;
  private final Provider<b> m;
  private final Provider<com.truecaller.utils.l> n;
  private final Provider<j> o;
  private final Provider<com.truecaller.smsparser.a> p;
  private final Provider<com.truecaller.featuretoggles.e> q;
  private final Provider<com.truecaller.messaging.a> r;
  
  private e(c paramc, Provider<Context> paramProvider, Provider<f<ae>> paramProvider1, Provider<HandlerThread> paramProvider2, Provider<com.truecaller.utils.d> paramProvider3, Provider<f<t>> paramProvider4, Provider<g> paramProvider5, Provider<com.truecaller.messaging.h> paramProvider6, Provider<f<com.truecaller.messaging.notifications.a>> paramProvider7, Provider<com.truecaller.multisim.h> paramProvider8, Provider<u> paramProvider9, Provider<ad.b> paramProvider10, Provider<b> paramProvider11, Provider<com.truecaller.utils.l> paramProvider12, Provider<j> paramProvider13, Provider<com.truecaller.smsparser.a> paramProvider14, Provider<com.truecaller.featuretoggles.e> paramProvider15, Provider<com.truecaller.messaging.a> paramProvider16)
  {
    a = paramc;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
    f = paramProvider4;
    g = paramProvider5;
    h = paramProvider6;
    i = paramProvider7;
    j = paramProvider8;
    k = paramProvider9;
    l = paramProvider10;
    m = paramProvider11;
    n = paramProvider12;
    o = paramProvider13;
    p = paramProvider14;
    q = paramProvider15;
    r = paramProvider16;
  }
  
  public static e a(c paramc, Provider<Context> paramProvider, Provider<f<ae>> paramProvider1, Provider<HandlerThread> paramProvider2, Provider<com.truecaller.utils.d> paramProvider3, Provider<f<t>> paramProvider4, Provider<g> paramProvider5, Provider<com.truecaller.messaging.h> paramProvider6, Provider<f<com.truecaller.messaging.notifications.a>> paramProvider7, Provider<com.truecaller.multisim.h> paramProvider8, Provider<u> paramProvider9, Provider<ad.b> paramProvider10, Provider<b> paramProvider11, Provider<com.truecaller.utils.l> paramProvider12, Provider<j> paramProvider13, Provider<com.truecaller.smsparser.a> paramProvider14, Provider<com.truecaller.featuretoggles.e> paramProvider15, Provider<com.truecaller.messaging.a> paramProvider16)
  {
    return new e(paramc, paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10, paramProvider11, paramProvider12, paramProvider13, paramProvider14, paramProvider15, paramProvider16);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.sms.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */