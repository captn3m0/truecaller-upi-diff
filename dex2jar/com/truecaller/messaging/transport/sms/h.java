package com.truecaller.messaging.transport.sms;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.RemoteException;
import android.provider.Telephony.Sms;
import android.provider.Telephony.Sms.Intents;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.telephony.SmsMessage.MessageClass;
import android.text.TextUtils;
import c.a.m;
import c.x;
import com.truecaller.TrueApp;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.w;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.transport.ad;
import com.truecaller.messaging.transport.ad.a.a;
import com.truecaller.messaging.transport.ad.b;
import com.truecaller.messaging.transport.k.b;
import com.truecaller.messaging.transport.l.a;
import com.truecaller.multisim.SimInfo;
import com.truecaller.payments.j;
import com.truecaller.tracking.events.u.a;
import com.truecaller.utils.d;
import com.truecaller.utils.q;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

final class h
  implements com.truecaller.messaging.transport.l<i>
{
  public static final Uri a;
  public static final String b;
  private static final SmsMessage[] c = new SmsMessage[0];
  private final Context d;
  private final com.truecaller.androidactors.f<ae> e;
  private final d f;
  private final HandlerThread g;
  private final com.truecaller.androidactors.f<t> h;
  private final g i;
  private final com.truecaller.messaging.h j;
  private final ContentObserver k;
  private final com.truecaller.androidactors.f<com.truecaller.messaging.notifications.a> l;
  private final com.truecaller.multisim.h m;
  private final com.truecaller.common.h.u n;
  private final ad.b o;
  private final com.truecaller.analytics.b p;
  private final com.truecaller.utils.l q;
  private final j r;
  private final com.truecaller.smsparser.a s;
  private final com.truecaller.featuretoggles.e t;
  private final com.truecaller.messaging.a u;
  private h.a v = null;
  private boolean w = false;
  
  static
  {
    Uri localUri = Telephony.Sms.CONTENT_URI;
    a = localUri;
    b = localUri.getAuthority();
  }
  
  h(Context paramContext, com.truecaller.androidactors.f<ae> paramf, HandlerThread paramHandlerThread, d paramd, com.truecaller.androidactors.f<t> paramf1, g paramg, com.truecaller.messaging.h paramh, com.truecaller.androidactors.f<com.truecaller.messaging.notifications.a> paramf2, com.truecaller.multisim.h paramh1, com.truecaller.common.h.u paramu, ad.b paramb, com.truecaller.analytics.b paramb1, com.truecaller.utils.l paraml, j paramj, com.truecaller.smsparser.a parama, com.truecaller.featuretoggles.e parame, com.truecaller.messaging.a parama1)
  {
    d = paramContext;
    g = paramHandlerThread;
    e = paramf;
    f = paramd;
    h = paramf1;
    k = new h.b(paramHandlerThread.getLooper(), paramContext.getContentResolver(), paramf1);
    i = paramg;
    j = paramh;
    l = paramf2;
    m = paramh1;
    n = paramu;
    o = paramb;
    p = paramb1;
    q = paraml;
    r = paramj;
    s = parama;
    t = parame;
    u = parama1;
  }
  
  @SuppressLint({"InlinedApi"})
  private Uri a(Context paramContext, Message paramMessage)
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.provideAs(TypeTransformer.java:780)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e1expr(TypeTransformer.java:496)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:713)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.enexpr(TypeTransformer.java:698)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:719)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.enexpr(TypeTransformer.java:698)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:719)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.s1stmt(TypeTransformer.java:810)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.sxStmt(TypeTransformer.java:840)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:206)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
  }
  
  private Message a(SmsMessage[] paramArrayOfSmsMessage, String paramString, int paramInt)
  {
    SmsMessage localSmsMessage = paramArrayOfSmsMessage[0];
    SmsTransportInfo.a locala = new SmsTransportInfo.a();
    h = localSmsMessage.getServiceCenterAddress();
    i = paramInt;
    f = localSmsMessage.getProtocolIdentifier();
    j = localSmsMessage.isReplyPathPresent();
    c = localSmsMessage.getStatus();
    Object localObject = localSmsMessage.getPseudoSubject();
    if (!TextUtils.isEmpty((CharSequence)localObject)) {
      k = ((String)localObject);
    }
    localObject = new Message.a();
    c = Participant.a((String)org.c.a.a.a.k.e(localSmsMessage.getDisplayOriginatingAddress(), "Unknown sender"), n, paramString);
    ((Message.a)localObject).c(System.currentTimeMillis()).d(localSmsMessage.getTimestampMillis()).a(Entity.a("text/plain", 0, a(paramArrayOfSmsMessage), -1L)).a(paramString).a(0, locala.a());
    return ((Message.a)localObject).b();
  }
  
  /* Error */
  private static String a(Context paramContext, long paramLong, String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 110	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   4: astore_0
    //   5: aconst_null
    //   6: astore 4
    //   8: lload_1
    //   9: invokestatic 265	com/truecaller/content/TruecallerContract$ab:a	(J)Landroid/net/Uri;
    //   12: astore 5
    //   14: aload_3
    //   15: invokestatic 269	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   18: astore_3
    //   19: aload_0
    //   20: aload 5
    //   22: iconst_2
    //   23: anewarray 210	java/lang/String
    //   26: dup
    //   27: iconst_0
    //   28: ldc_w 271
    //   31: aastore
    //   32: dup
    //   33: iconst_1
    //   34: ldc_w 273
    //   37: aastore
    //   38: ldc_w 275
    //   41: iconst_1
    //   42: anewarray 210	java/lang/String
    //   45: dup
    //   46: iconst_0
    //   47: aload_3
    //   48: aastore
    //   49: ldc_w 277
    //   52: invokevirtual 283	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   55: astore_0
    //   56: aload_0
    //   57: ifnull +67 -> 124
    //   60: aload_0
    //   61: astore_3
    //   62: aload_0
    //   63: invokeinterface 288 1 0
    //   68: ifeq +56 -> 124
    //   71: aload_0
    //   72: astore_3
    //   73: aload_0
    //   74: iconst_0
    //   75: invokeinterface 292 2 0
    //   80: invokestatic 295	org/c/a/a/a/k:n	(Ljava/lang/String;)Ljava/lang/String;
    //   83: astore 4
    //   85: aload_0
    //   86: astore_3
    //   87: aload_0
    //   88: iconst_1
    //   89: invokeinterface 299 2 0
    //   94: ifeq +9 -> 103
    //   97: aload 4
    //   99: astore_3
    //   100: goto +7 -> 107
    //   103: ldc_w 301
    //   106: astore_3
    //   107: aload_0
    //   108: ifnull +9 -> 117
    //   111: aload_0
    //   112: invokeinterface 304 1 0
    //   117: aload_3
    //   118: areturn
    //   119: astore 4
    //   121: goto +21 -> 142
    //   124: aload_0
    //   125: ifnull +34 -> 159
    //   128: goto +25 -> 153
    //   131: astore_0
    //   132: aload 4
    //   134: astore_3
    //   135: goto +27 -> 162
    //   138: astore 4
    //   140: aconst_null
    //   141: astore_0
    //   142: aload_0
    //   143: astore_3
    //   144: aload 4
    //   146: invokestatic 310	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   149: aload_0
    //   150: ifnull +9 -> 159
    //   153: aload_0
    //   154: invokeinterface 304 1 0
    //   159: aconst_null
    //   160: areturn
    //   161: astore_0
    //   162: aload_3
    //   163: ifnull +9 -> 172
    //   166: aload_3
    //   167: invokeinterface 304 1 0
    //   172: aload_0
    //   173: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	174	0	paramContext	Context
    //   0	174	1	paramLong	long
    //   0	174	3	paramString	String
    //   6	92	4	str	String
    //   119	14	4	localRuntimeException1	RuntimeException
    //   138	7	4	localRuntimeException2	RuntimeException
    //   12	9	5	localUri	Uri
    // Exception table:
    //   from	to	target	type
    //   62	71	119	java/lang/RuntimeException
    //   73	85	119	java/lang/RuntimeException
    //   87	97	119	java/lang/RuntimeException
    //   8	56	131	finally
    //   8	56	138	java/lang/RuntimeException
    //   62	71	161	finally
    //   73	85	161	finally
    //   87	97	161	finally
    //   144	149	161	finally
  }
  
  private static String a(SmsMessage[] paramArrayOfSmsMessage)
  {
    int i2 = paramArrayOfSmsMessage.length;
    int i1 = 0;
    if (i2 == 1) {
      return b(paramArrayOfSmsMessage[0].getDisplayMessageBody());
    }
    StringBuilder localStringBuilder = new StringBuilder();
    i2 = paramArrayOfSmsMessage.length;
    for (;;)
    {
      SmsMessage localSmsMessage;
      if (i1 < i2) {
        localSmsMessage = paramArrayOfSmsMessage[i1];
      }
      try
      {
        localStringBuilder.append(localSmsMessage.getDisplayMessageBody());
        i1 += 1;
        continue;
        return b(localStringBuilder.toString());
      }
      catch (NullPointerException localNullPointerException)
      {
        for (;;) {}
      }
    }
  }
  
  private boolean a(i parami)
  {
    if (!e) {
      return false;
    }
    Object localObject1 = ((Iterable)m.e((Iterable)d, 499)).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      Object localObject3 = (List)((Iterator)localObject1).next();
      ad.a.a locala = parami.b(Telephony.Sms.CONTENT_URI);
      Object localObject2 = new StringBuilder("_id IN (");
      localObject3 = (Iterable)localObject3;
      ((StringBuilder)localObject2).append(m.a((Iterable)localObject3, (CharSequence)",", null, null, 0, null, (c.g.a.b)i.a.a, 30));
      ((StringBuilder)localObject2).append(')');
      localObject2 = ((StringBuilder)localObject2).toString();
      Collection localCollection = (Collection)new ArrayList(m.a((Iterable)localObject3, 10));
      localObject3 = ((Iterable)localObject3).iterator();
      while (((Iterator)localObject3).hasNext()) {
        localCollection.add(String.valueOf(((Number)((Iterator)localObject3).next()).longValue()));
      }
      localObject3 = ((Collection)localCollection).toArray(new String[0]);
      if (localObject3 != null)
      {
        locala.a((String)localObject2, (String[])localObject3);
        parami.a(locala.a());
      }
      else
      {
        throw new c.u("null cannot be cast to non-null type kotlin.Array<T>");
      }
    }
    localObject1 = x.a;
    d.clear();
    try
    {
      int i1 = o.a(parami).length;
      return i1 != 0;
    }
    catch (OperationApplicationException parami) {}catch (SecurityException parami) {}catch (RemoteException parami) {}
    AssertionUtil.reportThrowableButNeverCrash(parami);
    return false;
  }
  
  private static SmsMessage[] a(Intent paramIntent)
  {
    Bundle localBundle = paramIntent.getExtras();
    if (localBundle == null) {
      return c;
    }
    if (localBundle.get("pdus") == null)
    {
      paramIntent = new StringBuilder("Intent from Telephony.Sms.Intents.SMS_RECEIVED_ACTION does not have pdus extra, but has: [");
      paramIntent.append(org.c.a.a.a.k.a(localBundle.keySet(), ','));
      paramIntent.append("]");
      AssertionUtil.reportWeirdnessButNeverCrash(paramIntent.toString());
      return c;
    }
    paramIntent = Telephony.Sms.Intents.getMessagesFromIntent(paramIntent);
    if ((paramIntent != null) && (paramIntent.length > 0)) {
      return paramIntent;
    }
    return c;
  }
  
  private static String b(String paramString)
  {
    if (paramString == null) {
      return "";
    }
    return paramString.replace('\f', '\n');
  }
  
  private boolean c(String paramString)
  {
    SimInfo localSimInfo = m.b(paramString);
    paramString = m.c(paramString);
    if (localSimInfo == null) {
      return false;
    }
    return (paramString.b()) && (j.b(a));
  }
  
  private boolean g()
  {
    return f.a(u.b());
  }
  
  private boolean h()
  {
    return f.b(u.b());
  }
  
  public final long a(long paramLong)
  {
    return paramLong;
  }
  
  public final long a(com.truecaller.messaging.transport.f paramf, com.truecaller.messaging.transport.i parami, r paramr, org.a.a.b paramb1, org.a.a.b paramb2, int paramInt, List<ContentProviderOperation> paramList, q paramq, boolean paramBoolean1, boolean paramBoolean2, Set<Long> paramSet)
  {
    if (!q.a(new String[] { "android.permission.READ_SMS" })) {
      return 0L;
    }
    return i.a(paramf, parami, paramr, paramb1, paramb2, paramInt, paramList, paramq, paramBoolean1, paramBoolean2, paramSet);
  }
  
  public final l.a a(Message paramMessage, Participant[] paramArrayOfParticipant)
  {
    boolean bool;
    if (paramArrayOfParticipant.length > 0) {
      bool = true;
    } else {
      bool = false;
    }
    AssertionUtil.AlwaysFatal.isTrue(bool, new String[0]);
    if (!g()) {
      return new l.a(0);
    }
    String str = n.a(l);
    int i2 = paramArrayOfParticipant.length;
    Object localObject2 = null;
    int i1 = 0;
    while (i1 < i2)
    {
      Object localObject1 = paramArrayOfParticipant[i1];
      if ((paramArrayOfParticipant.length <= 1) || (!org.c.a.a.a.k.a(str, f)))
      {
        localObject2 = paramMessage.m();
        c = ((Participant)localObject1);
        localObject2 = ((Message.a)localObject2).b();
        if (j == 3)
        {
          localObject1 = new SmsTransportInfo.a();
          a = a;
        }
        else
        {
          localObject1 = ((SmsTransportInfo)m).g();
        }
        g = 6;
        Object localObject3 = a(d, b, l);
        if (org.c.a.a.a.k.e((CharSequence)localObject3)) {
          h = ((String)localObject3);
        }
        if (c(l)) {
          c = 32;
        } else {
          c = -1;
        }
        localObject2 = ((Message)localObject2).m().a(0, ((SmsTransportInfo.a)localObject1).a()).b();
        localObject2 = a(d, (Message)localObject2);
        if (localObject2 == null)
        {
          localObject1 = null;
        }
        else
        {
          long l1 = ContentUris.parseId((Uri)localObject2);
          if (l1 <= 0L)
          {
            localObject1 = null;
          }
          else
          {
            localObject3 = new ContentValues();
            ((ContentValues)localObject3).put("type", Integer.valueOf(6));
            if (d.getContentResolver().update((Uri)localObject2, (ContentValues)localObject3, null, null) == 0)
            {
              localObject1 = null;
            }
            else
            {
              e = ((Uri)localObject2);
              b = l1;
              localObject1 = ((SmsTransportInfo.a)localObject1).a();
            }
          }
        }
        localObject2 = localObject1;
        if (localObject1 == null) {
          return new l.a(0);
        }
      }
      i1 += 1;
    }
    AssertionUtil.AlwaysFatal.isNotNull(localObject2, new String[0]);
    if (paramArrayOfParticipant.length > 1) {
      return new l.a(2);
    }
    return new l.a((TransportInfo)localObject2);
  }
  
  public final String a()
  {
    return "sms";
  }
  
  public final String a(String paramString)
  {
    return paramString;
  }
  
  public final void a(Intent paramIntent, int paramInt)
  {
    Object localObject1 = paramIntent.getAction();
    boolean bool2 = "com.truecaller.messaging.SmsStatusReceived.SMS_SENT".equals(localObject1);
    boolean bool1 = true;
    if (bool2)
    {
      int i1 = paramIntent.getIntExtra("message_part", -1);
      if (i1 == -1)
      {
        AssertionUtil.OnlyInDebug.fail(new String[] { "Invalid message part" });
        return;
      }
      int i2 = paramIntent.getIntExtra("errorCode", 0);
      paramIntent = paramIntent.getData();
      AssertionUtil.onSameThread(g, new String[0]);
      localObject1 = v;
      if ((localObject1 == null) || (((h.a)localObject1).a(paramIntent, paramInt, i1)))
      {
        localObject1 = new ContentValues();
        if (paramInt != -1)
        {
          if (paramInt != 4)
          {
            switch (paramInt)
            {
            default: 
              break;
            case 1: 
              ((ContentValues)localObject1).put("error_code", Integer.valueOf(i2));
            }
          }
          else
          {
            ((ContentValues)localObject1).put("type", Integer.valueOf(5));
            ((ContentValues)localObject1).put("seen", Integer.valueOf(0));
            break label216;
          }
        }
        else
        {
          ((ContentValues)localObject1).put("type", Integer.valueOf(2));
          ((ContentValues)localObject1).put("date_sent", Long.valueOf(System.currentTimeMillis()));
        }
        bool1 = false;
        try
        {
          label216:
          if (d.getContentResolver().update(paramIntent, (ContentValues)localObject1, null, null) > 0) {
            ((t)h.a()).a(bool1, Collections.singleton(Integer.valueOf(0)));
          }
        }
        catch (RuntimeException paramIntent)
        {
          AssertionUtil.reportThrowableButNeverCrash(paramIntent);
        }
        v = null;
      }
      return;
    }
    Object localObject2;
    Object localObject3;
    if ("com.truecaller.messaging.SmsStatusReceived.SMS_STATUS".equals(localObject1))
    {
      long l1 = paramIntent.getLongExtra("date", -1L);
      if (l1 == -1L)
      {
        AssertionUtil.OnlyInDebug.fail(new String[] { "Invalid message date" });
        return;
      }
      localObject1 = paramIntent.getData();
      localObject2 = new org.a.a.b(l1);
      AssertionUtil.onSameThread(g, new String[0]);
      if (g())
      {
        paramIntent = paramIntent.getByteArrayExtra("pdu");
        if (paramIntent == null)
        {
          AssertionUtil.reportWeirdnessButNeverCrash("PDU is null in delivery report");
          return;
        }
        paramIntent = SmsMessage.createFromPdu(paramIntent);
        if (paramIntent == null)
        {
          AssertionUtil.reportWeirdnessButNeverCrash("Can not decode message");
          return;
        }
        localObject3 = new ContentValues();
        ((ContentValues)localObject3).put("date_sent", Long.valueOf(System.currentTimeMillis()));
        ((ContentValues)localObject3).put("status", Integer.valueOf(paramIntent.getStatus()));
        try
        {
          d.getContentResolver().update((Uri)localObject1, (ContentValues)localObject3, null, null);
          ((t)h.a()).a(0, (org.a.a.b)localObject2, false);
          return;
        }
        catch (RuntimeException paramIntent)
        {
          AssertionUtil.reportThrowableButNeverCrash(paramIntent);
          return;
        }
      }
      return;
    }
    if (org.c.a.a.a.k.a((CharSequence)localObject1, "android.provider.Telephony.SMS_RECEIVED"))
    {
      AssertionUtil.onSameThread(g, new String[0]);
      localObject3 = new e.a("MessageReceived").a("Type", "sms");
      localObject2 = u.b();
      localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = "NotSupported";
      }
      localObject1 = ((e.a)localObject3).a("AppId", (String)localObject1).a();
      p.a((com.truecaller.analytics.e)localObject1);
      if (!h())
      {
        "Received intent: ".concat(String.valueOf(paramIntent));
        localObject1 = a(paramIntent);
        if (localObject1.length != 0)
        {
          paramInt = paramIntent.getIntExtra("errorCode", 0);
          paramIntent = a((SmsMessage[])localObject1, m.a(paramIntent), paramInt);
          ((com.truecaller.messaging.notifications.a)l.a()).a(paramIntent);
          if (!t.w().a()) {
            r.a(paramIntent);
          }
          if (q.a(new String[] { "android.permission.READ_SMS" })) {
            ((t)h.a()).a(0, d, false);
          }
        }
      }
      return;
    }
    if ("android.provider.Telephony.SMS_DELIVER".equals(localObject1))
    {
      AssertionUtil.onSameThread(g, new String[0]);
      "Intent received: ".concat(String.valueOf(paramIntent));
      localObject1 = a(paramIntent);
      if (localObject1.length != 0)
      {
        paramInt = paramIntent.getIntExtra("errorCode", 0);
        paramIntent = a((SmsMessage[])localObject1, m.a(paramIntent), paramInt);
        "New sms: ".concat(String.valueOf(paramIntent));
        if (!t.w().a()) {
          r.a(paramIntent);
        }
        if (localObject1[0].getMessageClass() == SmsMessage.MessageClass.CLASS_0)
        {
          ((com.truecaller.messaging.notifications.a)l.a()).c(paramIntent);
          return;
        }
        ((t)h.a()).a(paramIntent);
      }
      return;
    }
    if (("android.provider.action.DEFAULT_SMS_PACKAGE_CHANGED".equals(localObject1)) && (Build.VERSION.SDK_INT >= 24))
    {
      u.a();
      AssertionUtil.onSameThread(g, new String[0]);
      localObject2 = u.b();
      localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = "NotSupported";
      }
      bool1 = paramIntent.getBooleanExtra("android.provider.extra.IS_DEFAULT_SMS_APP", false);
      if (!bool1) {
        d.getApplicationContext()).c.b(10012);
      }
      paramIntent = (t)h.a();
      localObject2 = org.a.a.b.ay_();
      paramIntent.b(((org.a.a.b)localObject2).a_(b.i().b(a, 3))).a(new -..Lambda.h.DdmLLhy8FkDXD_fwy12AnP2a4Ks(this, bool1, (String)localObject1));
      return;
    }
    AssertionUtil.OnlyInDebug.fail(new String[] { "Unknown intent action: ".concat(String.valueOf(localObject1)) });
  }
  
  public final void a(BinaryEntity paramBinaryEntity)
  {
    throw new IllegalStateException("Sms transport can not be used to cancel attachments.");
  }
  
  public final void a(Message paramMessage, String paramString1, String paramString2)
  {
    throw new IllegalStateException("SMS transport does not support sending reactions");
  }
  
  public final void a(org.a.a.b paramb)
  {
    j.a(0, a);
  }
  
  public final boolean a(Message paramMessage)
  {
    Object localObject = a(d, paramMessage);
    if (localObject != null) {}
    try
    {
      l1 = ContentUris.parseId((Uri)localObject);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      long l1;
      String str2;
      String str1;
      for (;;) {}
    }
    l1 = -1L;
    break label36;
    l1 = -1L;
    label36:
    if (l1 == -1L) {
      return false;
    }
    if (!g)
    {
      str2 = Message.a(l1, e);
      str1 = c.f;
      localObject = str1;
      if (str1.startsWith("+")) {
        localObject = str1.substring(1);
      }
      try
      {
        if ((f & 0x1) == 0)
        {
          paramMessage = com.truecaller.tracking.events.u.b().d("sms").a(str2).b((CharSequence)localObject).c(c.j()).a();
          ((ae)e.a()).a(paramMessage);
          return true;
        }
      }
      catch (org.apache.a.a paramMessage)
      {
        AssertionUtil.shouldNeverHappen(paramMessage, new String[0]);
      }
    }
    return true;
  }
  
  public final boolean a(Message paramMessage, Entity paramEntity)
  {
    return false;
  }
  
  public final boolean a(Participant paramParticipant)
  {
    if (c != 0) {
      return c == 1;
    }
    return true;
  }
  
  public final boolean a(String paramString, com.truecaller.messaging.transport.a parama)
  {
    if (paramString.isEmpty())
    {
      parama.a(0, 0, 0, 0);
      return false;
    }
    paramString = SmsMessage.calculateLength(org.c.a.a.a.k.n(paramString), false);
    parama.a(paramString[1], paramString[2], paramString[0], 0);
    return true;
  }
  
  public final void b(long paramLong)
  {
    throw new IllegalStateException("SMS transport does not support retry");
  }
  
  public final boolean b(Message paramMessage)
  {
    if (g()) {
      if ((q.a(new String[] { "android.permission.SEND_SMS" })) && (c(paramMessage)) && (a(c))) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean b(ad paramad)
  {
    return (!paramad.a()) && (b.equals(b));
  }
  
  public final boolean c()
  {
    return (q.a(new String[] { "android.permission.READ_SMS" })) && (g());
  }
  
  public final boolean c(Message paramMessage)
  {
    return (paramMessage.c()) && (!paramMessage.d());
  }
  
  public final com.truecaller.messaging.transport.k d(Message paramMessage)
  {
    AssertionUtil.notOnMainThread(new String[0]);
    boolean bool;
    if (j == 0) {
      bool = true;
    } else {
      bool = false;
    }
    AssertionUtil.isTrue(bool, new String[0]);
    SmsTransportInfo localSmsTransportInfo = (SmsTransportInfo)m;
    AssertionUtil.isNotNull(e, new String[] { "Save message to system database before actual sending" });
    ArrayList localArrayList1 = SmsManager.getDefault().divideMessage(paramMessage.j());
    ArrayList localArrayList2 = new ArrayList();
    ArrayList localArrayList3 = new ArrayList();
    int i2 = localArrayList1.size();
    int i1 = 0;
    Object localObject;
    while (i1 < i2)
    {
      localObject = new Intent(d, SmsReceiver.class);
      ((Intent)localObject).setAction("com.truecaller.messaging.SmsStatusReceived.SMS_SENT");
      ((Intent)localObject).setData(e);
      ((Intent)localObject).putExtra("message_part", i1);
      ((Intent)localObject).setFlags(268435456);
      localArrayList2.add(PendingIntent.getBroadcast(d, i1, (Intent)localObject, 0));
      i1 += 1;
    }
    if (c(l))
    {
      localObject = new Intent(d, SmsReceiver.class);
      ((Intent)localObject).setAction("com.truecaller.messaging.SmsStatusReceived.SMS_STATUS");
      ((Intent)localObject).setData(e);
      ((Intent)localObject).putExtra("date", e.a);
      ((Intent)localObject).setFlags(268435456);
      localArrayList3.add(PendingIntent.getBroadcast(d, 0, (Intent)localObject, 0));
    }
    String str = c.f;
    AssertionUtil.isFalse(TextUtils.isEmpty(str), new String[] { "Destination can not be empty" });
    for (;;)
    {
      try
      {
        if (m.c(l).a())
        {
          i2 = localArrayList1.size();
          i1 = 0;
          if (i1 < i2)
          {
            if ((localArrayList3.isEmpty()) || (i1 != i2 - 1)) {
              break label486;
            }
            localObject = (PendingIntent)localArrayList3.get(0);
            if (m.a(str, i, (String)localArrayList1.get(i1), (PendingIntent)localArrayList2.get(i1), (PendingIntent)localObject, l)) {
              break label492;
            }
            return k.b.a;
          }
        }
        else if (!m.a(str, i, localArrayList1, localArrayList2, localArrayList3, l))
        {
          return k.b.a;
        }
        v = new h.a(e, localArrayList1.size(), (byte)0);
        paramMessage = v;
        return paramMessage;
      }
      catch (RuntimeException paramMessage)
      {
        AssertionUtil.OnlyInDebug.shouldNeverHappen(paramMessage, new String[0]);
        AssertionUtil.reportThrowableButNeverCrash(paramMessage);
        return k.b.a;
      }
      label486:
      localObject = null;
      continue;
      label492:
      i1 += 1;
    }
  }
  
  /* Error */
  @SuppressLint({"NewApi"})
  public final org.a.a.b d()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 119	com/truecaller/messaging/transport/sms/h:j	Lcom/truecaller/messaging/h;
    //   4: iconst_0
    //   5: invokeinterface 1131 2 0
    //   10: lstore_1
    //   11: lload_1
    //   12: lstore_3
    //   13: aload_0
    //   14: getfield 88	com/truecaller/messaging/transport/sms/h:w	Z
    //   17: ifne +280 -> 297
    //   20: lload_1
    //   21: lstore_3
    //   22: aload_0
    //   23: getfield 131	com/truecaller/messaging/transport/sms/h:q	Lcom/truecaller/utils/l;
    //   26: iconst_1
    //   27: anewarray 210	java/lang/String
    //   30: dup
    //   31: iconst_0
    //   32: ldc_w 1035
    //   35: aastore
    //   36: invokeinterface 612 2 0
    //   41: ifeq +256 -> 297
    //   44: lload_1
    //   45: lstore_3
    //   46: aload_0
    //   47: getfield 131	com/truecaller/messaging/transport/sms/h:q	Lcom/truecaller/utils/l;
    //   50: iconst_1
    //   51: anewarray 210	java/lang/String
    //   54: dup
    //   55: iconst_0
    //   56: ldc_w 607
    //   59: aastore
    //   60: invokeinterface 612 2 0
    //   65: ifeq +232 -> 297
    //   68: aload_0
    //   69: getfield 90	com/truecaller/messaging/transport/sms/h:d	Landroid/content/Context;
    //   72: invokevirtual 110	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   75: astore 12
    //   77: aconst_null
    //   78: astore 11
    //   80: aconst_null
    //   81: astore 9
    //   83: aload 12
    //   85: getstatic 71	com/truecaller/messaging/transport/sms/h:a	Landroid/net/Uri;
    //   88: iconst_1
    //   89: anewarray 210	java/lang/String
    //   92: dup
    //   93: iconst_0
    //   94: ldc_w 774
    //   97: aastore
    //   98: ldc_w 1133
    //   101: aconst_null
    //   102: ldc_w 277
    //   105: invokevirtual 283	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   108: astore 10
    //   110: lload_1
    //   111: lstore_3
    //   112: aload 10
    //   114: ifnull +118 -> 232
    //   117: lload_1
    //   118: lstore 5
    //   120: lload_1
    //   121: lstore_3
    //   122: aload 10
    //   124: invokeinterface 1136 1 0
    //   129: ifeq +103 -> 232
    //   132: lload_1
    //   133: lstore 5
    //   135: aload 10
    //   137: iconst_0
    //   138: invokeinterface 1139 2 0
    //   143: lstore 7
    //   145: lload_1
    //   146: lstore 5
    //   148: new 677	android/content/ContentValues
    //   151: dup
    //   152: invokespecial 678	android/content/ContentValues:<init>	()V
    //   155: astore 9
    //   157: lload_1
    //   158: lstore 5
    //   160: aload 9
    //   162: ldc_w 680
    //   165: iconst_5
    //   166: invokestatic 685	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   169: invokevirtual 689	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   172: lload_1
    //   173: lstore 5
    //   175: aload 12
    //   177: getstatic 71	com/truecaller/messaging/transport/sms/h:a	Landroid/net/Uri;
    //   180: aload 9
    //   182: ldc_w 1133
    //   185: aconst_null
    //   186: invokevirtual 693	android/content/ContentResolver:update	(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   189: pop
    //   190: lload_1
    //   191: lstore_3
    //   192: lload 7
    //   194: lload_1
    //   195: lcmp
    //   196: ifge +6 -> 202
    //   199: lload 7
    //   201: lstore_3
    //   202: lload_3
    //   203: lstore 5
    //   205: aload_0
    //   206: getfield 119	com/truecaller/messaging/transport/sms/h:j	Lcom/truecaller/messaging/h;
    //   209: iconst_0
    //   210: lload_3
    //   211: invokeinterface 937 4 0
    //   216: goto +16 -> 232
    //   219: astore 9
    //   221: goto +68 -> 289
    //   224: astore 11
    //   226: lload 5
    //   228: lstore_1
    //   229: goto +36 -> 265
    //   232: aload 10
    //   234: invokestatic 1144	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   237: lload_3
    //   238: lstore_1
    //   239: goto +40 -> 279
    //   242: astore 11
    //   244: aload 9
    //   246: astore 10
    //   248: aload 11
    //   250: astore 9
    //   252: goto +37 -> 289
    //   255: astore 9
    //   257: aload 11
    //   259: astore 10
    //   261: aload 9
    //   263: astore 11
    //   265: aload 10
    //   267: astore 9
    //   269: aload 11
    //   271: invokestatic 310	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   274: aload 10
    //   276: invokestatic 1144	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   279: aload_0
    //   280: iconst_1
    //   281: putfield 88	com/truecaller/messaging/transport/sms/h:w	Z
    //   284: lload_1
    //   285: lstore_3
    //   286: goto +11 -> 297
    //   289: aload 10
    //   291: invokestatic 1144	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   294: aload 9
    //   296: athrow
    //   297: new 399	org/a/a/b
    //   300: dup
    //   301: lload_3
    //   302: invokespecial 782	org/a/a/b:<init>	(J)V
    //   305: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	306	0	this	h
    //   10	275	1	l1	long
    //   12	290	3	l2	long
    //   118	109	5	l3	long
    //   143	57	7	l4	long
    //   81	100	9	localContentValues	ContentValues
    //   219	26	9	localObject1	Object
    //   250	1	9	localObject2	Object
    //   255	7	9	localRuntimeException1	RuntimeException
    //   267	28	9	localObject3	Object
    //   108	182	10	localObject4	Object
    //   78	1	11	localObject5	Object
    //   224	1	11	localRuntimeException2	RuntimeException
    //   242	16	11	localObject6	Object
    //   263	7	11	localObject7	Object
    //   75	101	12	localContentResolver	ContentResolver
    // Exception table:
    //   from	to	target	type
    //   122	132	219	finally
    //   135	145	219	finally
    //   148	157	219	finally
    //   160	172	219	finally
    //   175	190	219	finally
    //   205	216	219	finally
    //   122	132	224	java/lang/RuntimeException
    //   135	145	224	java/lang/RuntimeException
    //   148	157	224	java/lang/RuntimeException
    //   160	172	224	java/lang/RuntimeException
    //   175	190	224	java/lang/RuntimeException
    //   205	216	224	java/lang/RuntimeException
    //   83	110	242	finally
    //   269	274	242	finally
    //   83	110	255	java/lang/RuntimeException
  }
  
  public final int e(Message paramMessage)
  {
    return 0;
  }
  
  public final boolean e()
  {
    return true;
  }
  
  public final int f()
  {
    return 0;
  }
  
  public final boolean f(Message paramMessage)
  {
    return false;
  }
  
  public final String toString()
  {
    return "SMS transport";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.sms.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */