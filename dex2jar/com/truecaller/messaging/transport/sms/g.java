package com.truecaller.messaging.transport.sms;

import android.annotation.SuppressLint;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.provider.Telephony.Sms;
import com.truecaller.common.h.ab;
import com.truecaller.common.h.am;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.transport.c;
import com.truecaller.messaging.transport.c.a;
import com.truecaller.messaging.transport.f;
import com.truecaller.messaging.transport.i;
import com.truecaller.multisim.h;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.c.a.a.a.a;

final class g
  extends c<SmsTransportInfo, c.a>
{
  @SuppressLint({"InlinedApi"})
  private static final String[] d = { "_id", "address", "body", "date", "error_code", "locked", "person", "protocol", "read", "reply_path_present", "seen", "service_center", "status", "subject", "thread_id", "type" };
  @SuppressLint({"InlinedApi"})
  private static final String[] e = { "date_sent" };
  private static volatile int f = -1;
  private static volatile String[] g = null;
  
  g(Context paramContext, h paramh, e parame)
  {
    super(paramContext, paramh, parame);
  }
  
  @SuppressLint({"InlinedApi"})
  private String[] b(ContentResolver paramContentResolver)
  {
    Object localObject1 = g;
    if (localObject1 == null) {
      try
      {
        Object localObject2 = g;
        localObject1 = localObject2;
        if (localObject2 == null)
        {
          localObject2 = d;
          localObject1 = localObject2;
          if (a(paramContentResolver)) {
            localObject1 = (String[])a.b((Object[])localObject2, "date_sent");
          }
          localObject2 = b.b();
          paramContentResolver = (ContentResolver)localObject1;
          if (localObject2 != null) {
            paramContentResolver = (String[])a.b((Object[])localObject1, localObject2);
          }
          g = paramContentResolver;
          localObject1 = paramContentResolver;
        }
        return (String[])localObject1;
      }
      finally {}
    }
    return (String[])localObject1;
  }
  
  @SuppressLint({"NewApi"})
  public final c.a a(ContentResolver paramContentResolver, f paramf, i parami, org.a.a.b paramb1, org.a.a.b paramb2, boolean paramBoolean1, boolean paramBoolean2)
  {
    long l1 = a;
    long l2 = a;
    paramContentResolver = paramContentResolver.query(Telephony.Sms.CONTENT_URI, b(paramContentResolver), "date>=? AND date<=? AND type != 3", new String[] { String.valueOf(l1), String.valueOf(l2) }, "date DESC, _id DESC");
    if (paramContentResolver == null) {
      return null;
    }
    return new b(paramf, parami, b, paramContentResolver, paramBoolean1);
  }
  
  public final Set<Participant> a(long paramLong, f paramf, i parami, Participant paramParticipant, boolean paramBoolean)
  {
    HashSet localHashSet = new HashSet();
    localHashSet.add(paramParticipant);
    Iterator localIterator = paramf.a(paramLong).iterator();
    while (localIterator.hasNext())
    {
      paramParticipant = (String)localIterator.next();
      paramf = paramParticipant;
      if (paramBoolean) {
        paramf = ab.c(paramParticipant);
      }
      localHashSet.add(parami.a(paramf));
    }
    return localHashSet;
  }
  
  public final boolean a(int paramInt)
  {
    return (paramInt & 0x1) == 0;
  }
  
  /* Error */
  @SuppressLint({"NewApi"})
  final boolean a(ContentResolver paramContentResolver)
  {
    // Byte code:
    //   0: getstatic 58	com/truecaller/messaging/transport/sms/g:f	I
    //   3: istore_3
    //   4: iload_3
    //   5: istore_2
    //   6: iload_3
    //   7: iconst_m1
    //   8: if_icmpne +61 -> 69
    //   11: aload_0
    //   12: monitorenter
    //   13: getstatic 58	com/truecaller/messaging/transport/sms/g:f	I
    //   16: istore_3
    //   17: iload_3
    //   18: istore_2
    //   19: iload_3
    //   20: iconst_m1
    //   21: if_icmpne +38 -> 59
    //   24: aload_1
    //   25: getstatic 99	android/provider/Telephony$Sms:CONTENT_URI	Landroid/net/Uri;
    //   28: getstatic 56	com/truecaller/messaging/transport/sms/g:e	[Ljava/lang/String;
    //   31: aconst_null
    //   32: aconst_null
    //   33: ldc -29
    //   35: invokevirtual 115	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   38: astore_1
    //   39: aload_1
    //   40: ifnull +41 -> 81
    //   43: aload_1
    //   44: invokeinterface 232 1 0
    //   49: goto +32 -> 81
    //   52: astore_1
    //   53: aload_1
    //   54: athrow
    //   55: iload_2
    //   56: putstatic 58	com/truecaller/messaging/transport/sms/g:f	I
    //   59: aload_0
    //   60: monitorexit
    //   61: goto +8 -> 69
    //   64: astore_1
    //   65: aload_0
    //   66: monitorexit
    //   67: aload_1
    //   68: athrow
    //   69: iload_2
    //   70: ifeq +5 -> 75
    //   73: iconst_1
    //   74: ireturn
    //   75: iconst_0
    //   76: ireturn
    //   77: astore_1
    //   78: goto +8 -> 86
    //   81: iconst_1
    //   82: istore_2
    //   83: goto -28 -> 55
    //   86: iconst_0
    //   87: istore_2
    //   88: goto -33 -> 55
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	91	0	this	g
    //   0	91	1	paramContentResolver	ContentResolver
    //   5	83	2	i	int
    //   3	19	3	j	int
    // Exception table:
    //   from	to	target	type
    //   24	39	52	finally
    //   13	17	64	finally
    //   43	49	64	finally
    //   53	55	64	finally
    //   55	59	64	finally
    //   59	61	64	finally
    //   65	67	64	finally
    //   24	39	77	android/database/SQLException
  }
  
  public final boolean a(r paramr, c.a parama)
  {
    int i = paramr.i();
    int j = parama.g();
    if (((i & 0x20) != 0) && ((j & 0x4) != 0)) {
      return false;
    }
    if ((i == j) && (paramr.h() == parama.f()) && ((paramr.n() == 1) || (paramr.g() == parama.e())) && ((paramr.n() == 1) || (paramr.f() == parama.d()))) {
      return paramr.c() != parama.h();
    }
    return true;
  }
  
  public final boolean a(f paramf, i parami, List<ContentProviderOperation> paramList, r paramr, c.a parama, boolean paramBoolean)
  {
    parami = parama.i();
    paramf = (SmsTransportInfo)m;
    long l = paramr.a();
    parami = parami.m();
    paramf = paramf.g();
    a = l;
    paramf = parami.a(0, paramf.a());
    a = l;
    parami = paramf.b();
    paramf = parami;
    if (paramr.n() == 1)
    {
      paramf = parami.m();
      g = paramr.f();
      h = paramr.g();
      paramf = paramf.b();
    }
    com.truecaller.messaging.data.b.a(paramList, paramf, -1);
    return true;
  }
  
  public final boolean b(r paramr, c.a parama)
  {
    return ((paramr.i() & 0x1) == 0) && (!am.a(paramr.m(), parama.j()));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.sms.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */