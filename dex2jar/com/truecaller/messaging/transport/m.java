package com.truecaller.messaging.transport;

import android.content.Intent;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.messaging.data.types.Draft;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import java.util.List;

public abstract interface m
{
  public abstract int a(Message paramMessage, Participant[] paramArrayOfParticipant);
  
  public abstract int a(boolean paramBoolean1, Participant[] paramArrayOfParticipant, boolean paramBoolean2);
  
  public abstract com.truecaller.androidactors.a a(Message paramMessage, i parami, ac<Draft> paramac);
  
  public abstract com.truecaller.androidactors.a a(Message paramMessage, Participant[] paramArrayOfParticipant, i parami, ac<Draft> paramac);
  
  public abstract w<Message> a(Message paramMessage);
  
  public abstract w<Message> a(Message paramMessage, Participant[] paramArrayOfParticipant, boolean paramBoolean1, boolean paramBoolean2);
  
  public abstract l a(int paramInt);
  
  public abstract Iterable<l> a();
  
  public abstract void a(int paramInt1, Intent paramIntent, int paramInt2);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract boolean a(Message paramMessage, Entity paramEntity);
  
  public abstract boolean a(String paramString, boolean paramBoolean1, Participant[] paramArrayOfParticipant, boolean paramBoolean2, a parama);
  
  public abstract boolean a(Participant[] paramArrayOfParticipant);
  
  public abstract l b(int paramInt);
  
  public abstract List<Integer> b();
  
  public abstract List<Integer> b(Message paramMessage, Participant[] paramArrayOfParticipant);
  
  public abstract boolean b(Message paramMessage);
  
  public abstract int c(Message paramMessage);
  
  public abstract boolean d(Message paramMessage);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */