package com.truecaller.messaging.transport.mms;

import android.content.Context;
import com.truecaller.featuretoggles.e;
import com.truecaller.multisim.h;
import com.truecaller.util.bd;
import dagger.a.d;
import javax.inject.Provider;

public final class y
  implements d<an>
{
  private final n a;
  private final Provider<Context> b;
  private final Provider<com.truecaller.utils.n> c;
  private final Provider<h> d;
  private final Provider<bd> e;
  private final Provider<e> f;
  
  private y(n paramn, Provider<Context> paramProvider, Provider<com.truecaller.utils.n> paramProvider1, Provider<h> paramProvider2, Provider<bd> paramProvider3, Provider<e> paramProvider4)
  {
    a = paramn;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
    f = paramProvider4;
  }
  
  public static y a(n paramn, Provider<Context> paramProvider, Provider<com.truecaller.utils.n> paramProvider1, Provider<h> paramProvider2, Provider<bd> paramProvider3, Provider<e> paramProvider4)
  {
    return new y(paramn, paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.y
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */