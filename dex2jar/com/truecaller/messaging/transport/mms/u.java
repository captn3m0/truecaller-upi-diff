package com.truecaller.messaging.transport.mms;

import android.content.Context;
import com.truecaller.androidactors.i;
import dagger.a.d;
import javax.inject.Provider;

public final class u
  implements d<ak>
{
  private final n a;
  private final Provider<Context> b;
  private final Provider<i> c;
  
  private u(n paramn, Provider<Context> paramProvider, Provider<i> paramProvider1)
  {
    a = paramn;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static u a(n paramn, Provider<Context> paramProvider, Provider<i> paramProvider1)
  {
    return new u(paramn, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.u
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */