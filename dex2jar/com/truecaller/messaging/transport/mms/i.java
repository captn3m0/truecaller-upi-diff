package com.truecaller.messaging.transport.mms;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import android.util.SparseArray;
import com.android.a.a.a.e;
import com.android.a.a.a.f;
import com.android.a.a.a.g;
import com.android.a.a.a.j;
import com.android.a.a.a.m;
import com.android.a.a.a.o;
import com.android.a.a.c;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.ImageEntity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Participant.a;
import com.truecaller.messaging.data.types.TextEntity;
import com.truecaller.messaging.data.types.TransportInfo;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.a.a.b;
import org.c.a.a.a.k;

final class i
  implements h
{
  private final ContentResolver a;
  private final com.truecaller.common.h.u b;
  private final File c;
  
  i(ContentResolver paramContentResolver, com.truecaller.common.h.u paramu, File paramFile)
  {
    a = paramContentResolver;
    b = paramu;
    c = new File(paramFile, "pdu_parts");
  }
  
  private j a(h.a parama)
  {
    j localj = new j();
    int j = 0;
    while (parama.moveToNext())
    {
      String str2 = parama.a();
      boolean bool = Entity.a(str2);
      Object localObject3 = null;
      Object localObject2 = null;
      String str1 = null;
      Object localObject1 = null;
      int i;
      if ((!bool) && (!Entity.b(str2)))
      {
        if (Entity.c(str2))
        {
          localObject2 = parama.c();
          localObject3 = parama.d();
          i = b((Uri)localObject2);
          if (i >= 0)
          {
            localObject1 = new o();
            ((o)localObject1).e(str2.getBytes());
            ((o)localObject1).a((Uri)localObject2, i);
            ((o)localObject1).b(String.format(Locale.US, "image.%06d", new Object[] { Integer.valueOf(j) }).getBytes());
            ((o)localObject1).c(((String)localObject3).getBytes());
          }
          i = j + 1;
        }
        else if (Entity.f(str2))
        {
          localObject2 = parama.c();
          str1 = parama.d();
          i = b((Uri)localObject2);
          if (i < 0)
          {
            localObject1 = localObject3;
          }
          else
          {
            localObject1 = new o();
            ((o)localObject1).a(106);
            ((o)localObject1).e(str2.getBytes());
            ((o)localObject1).b(String.format(Locale.US, "vcard.%06d", new Object[] { Integer.valueOf(j) }).getBytes());
            ((o)localObject1).c(str1.getBytes());
            ((o)localObject1).a((Uri)localObject2, i);
          }
          i = j + 1;
        }
        else
        {
          i = j;
          localObject1 = str1;
          if (Entity.d(str2))
          {
            localObject3 = parama.c();
            str1 = parama.d();
            i = j + 1;
            int k = b((Uri)localObject3);
            if (k < 0)
            {
              localObject1 = localObject2;
            }
            else
            {
              localObject1 = new o();
              ((o)localObject1).e(str2.getBytes());
              ((o)localObject1).a((Uri)localObject3, k);
              ((o)localObject1).b(String.format(Locale.US, "video.%06d", new Object[] { Integer.valueOf(j) }).getBytes());
              ((o)localObject1).c(str1.getBytes());
            }
          }
        }
      }
      else
      {
        localObject2 = parama.b();
        localObject3 = parama.d();
        i = j + 1;
        localObject1 = new o();
        ((o)localObject1).a(106);
        ((o)localObject1).e(str2.getBytes());
        ((o)localObject1).b(String.format(Locale.US, "text.%06d", new Object[] { Integer.valueOf(j) }).getBytes());
        ((o)localObject1).c(((String)localObject3).getBytes());
        ((o)localObject1).a(((String)localObject2).getBytes());
      }
      j = i;
      if (localObject1 != null)
      {
        localj.a((o)localObject1);
        j = i;
      }
    }
    return localj;
  }
  
  /* Error */
  private PduEntity a(ImageEntity paramImageEntity, int paramInt1, int paramInt2, int paramInt3, StringBuilder paramStringBuilder, int paramInt4)
  {
    // Byte code:
    //   0: new 128	android/graphics/BitmapFactory$Options
    //   3: dup
    //   4: invokespecial 129	android/graphics/BitmapFactory$Options:<init>	()V
    //   7: astore 19
    //   9: aload_0
    //   10: getfield 19	com/truecaller/messaging/transport/mms/i:a	Landroid/content/ContentResolver;
    //   13: aload_1
    //   14: getfield 134	com/truecaller/messaging/data/types/ImageEntity:b	Landroid/net/Uri;
    //   17: invokevirtual 140	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   20: astore 20
    //   22: aload 20
    //   24: ifnonnull +10 -> 34
    //   27: aload 20
    //   29: invokestatic 145	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   32: aconst_null
    //   33: areturn
    //   34: aload 20
    //   36: invokevirtual 151	java/io/InputStream:available	()I
    //   39: istore 16
    //   41: aload 19
    //   43: iconst_1
    //   44: putfield 155	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
    //   47: aload 20
    //   49: aconst_null
    //   50: aload 19
    //   52: invokestatic 161	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   55: pop
    //   56: aload 20
    //   58: invokestatic 145	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   61: aload_1
    //   62: getfield 165	com/truecaller/messaging/data/types/ImageEntity:j	Ljava/lang/String;
    //   65: astore 21
    //   67: aload 19
    //   69: getfield 169	android/graphics/BitmapFactory$Options:outWidth	I
    //   72: istore 15
    //   74: aload 19
    //   76: getfield 172	android/graphics/BitmapFactory$Options:outHeight	I
    //   79: istore 14
    //   81: iload 15
    //   83: iload_2
    //   84: if_icmpgt +46 -> 130
    //   87: iload 14
    //   89: iload_3
    //   90: if_icmple +6 -> 96
    //   93: goto +37 -> 130
    //   96: iload 6
    //   98: istore_3
    //   99: aload 19
    //   101: astore 23
    //   103: aconst_null
    //   104: astore 20
    //   106: aload_0
    //   107: astore 22
    //   109: aload 21
    //   111: astore 19
    //   113: iload 16
    //   115: istore_2
    //   116: iload 4
    //   118: istore 6
    //   120: aload_1
    //   121: astore 21
    //   123: iload 15
    //   125: istore 4
    //   127: goto +321 -> 448
    //   130: iload 15
    //   132: iload 14
    //   134: if_icmple +40 -> 174
    //   137: iload 4
    //   139: istore 17
    //   141: aload 5
    //   143: astore 20
    //   145: iload 6
    //   147: istore 18
    //   149: aload_1
    //   150: astore 5
    //   152: aload_0
    //   153: astore_1
    //   154: iconst_1
    //   155: istore 16
    //   157: iload 15
    //   159: istore 4
    //   161: iload_3
    //   162: istore 15
    //   164: iload 17
    //   166: istore 6
    //   168: iload 18
    //   170: istore_3
    //   171: goto +61 -> 232
    //   174: aload 19
    //   176: astore 20
    //   178: aload_1
    //   179: astore 22
    //   181: aload_0
    //   182: astore_1
    //   183: aload 5
    //   185: astore 19
    //   187: iload 4
    //   189: istore 16
    //   191: iload_3
    //   192: istore 17
    //   194: iload 15
    //   196: istore 4
    //   198: aload 22
    //   200: astore 5
    //   202: aload 20
    //   204: astore 22
    //   206: iload 6
    //   208: istore_3
    //   209: iconst_0
    //   210: istore 18
    //   212: aload 19
    //   214: astore 20
    //   216: iload 16
    //   218: istore 6
    //   220: aload 22
    //   222: astore 19
    //   224: iload 17
    //   226: istore 15
    //   228: iload 18
    //   230: istore 16
    //   232: iload 4
    //   234: iload_2
    //   235: if_icmpgt +781 -> 1016
    //   238: iload 14
    //   240: iload 15
    //   242: if_icmple +6 -> 248
    //   245: goto +771 -> 1016
    //   248: aload 21
    //   250: invokevirtual 175	java/lang/String:hashCode	()I
    //   253: istore_2
    //   254: iload_2
    //   255: ldc -80
    //   257: if_icmpeq +48 -> 305
    //   260: iload_2
    //   261: ldc -79
    //   263: if_icmpeq +27 -> 290
    //   266: iload_2
    //   267: ldc -78
    //   269: if_icmpeq +6 -> 275
    //   272: goto +48 -> 320
    //   275: aload 21
    //   277: ldc -76
    //   279: invokevirtual 184	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   282: ifeq +38 -> 320
    //   285: iconst_0
    //   286: istore_2
    //   287: goto +35 -> 322
    //   290: aload 21
    //   292: ldc -70
    //   294: invokevirtual 184	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   297: ifeq +23 -> 320
    //   300: iconst_1
    //   301: istore_2
    //   302: goto +20 -> 322
    //   305: aload 21
    //   307: ldc -68
    //   309: invokevirtual 184	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   312: ifeq +8 -> 320
    //   315: iconst_2
    //   316: istore_2
    //   317: goto +5 -> 322
    //   320: iconst_m1
    //   321: istore_2
    //   322: iload_2
    //   323: tableswitch	default:+25->348, 0:+45->368, 1:+37->360, 2:+37->360
    //   348: getstatic 194	android/graphics/Bitmap$CompressFormat:JPEG	Landroid/graphics/Bitmap$CompressFormat;
    //   351: astore 22
    //   353: ldc -70
    //   355: astore 21
    //   357: goto +16 -> 373
    //   360: getstatic 194	android/graphics/Bitmap$CompressFormat:JPEG	Landroid/graphics/Bitmap$CompressFormat;
    //   363: astore 22
    //   365: goto +8 -> 373
    //   368: getstatic 197	android/graphics/Bitmap$CompressFormat:PNG	Landroid/graphics/Bitmap$CompressFormat;
    //   371: astore 22
    //   373: aload_1
    //   374: getfield 19	com/truecaller/messaging/transport/mms/i:a	Landroid/content/ContentResolver;
    //   377: astore 23
    //   379: aload 5
    //   381: getfield 134	com/truecaller/messaging/data/types/ImageEntity:b	Landroid/net/Uri;
    //   384: astore 24
    //   386: aload 19
    //   388: getfield 169	android/graphics/BitmapFactory$Options:outWidth	I
    //   391: istore_2
    //   392: aload 19
    //   394: getfield 172	android/graphics/BitmapFactory$Options:outHeight	I
    //   397: istore 15
    //   399: aload 23
    //   401: aload 24
    //   403: iload_2
    //   404: iload 4
    //   406: iload 14
    //   408: aload 22
    //   410: bipush 95
    //   412: invokestatic 202	com/truecaller/common/h/n:a	(Landroid/content/ContentResolver;Landroid/net/Uri;IIILandroid/graphics/Bitmap$CompressFormat;I)[B
    //   415: astore 23
    //   417: aload 23
    //   419: arraylength
    //   420: istore_2
    //   421: aload 20
    //   423: astore 22
    //   425: aload 23
    //   427: astore 20
    //   429: aload 19
    //   431: astore 23
    //   433: aload 21
    //   435: astore 19
    //   437: aload 5
    //   439: astore 21
    //   441: aload 22
    //   443: astore 5
    //   445: aload_1
    //   446: astore 22
    //   448: iload_2
    //   449: iload 6
    //   451: if_icmple +205 -> 656
    //   454: getstatic 194	android/graphics/Bitmap$CompressFormat:JPEG	Landroid/graphics/Bitmap$CompressFormat;
    //   457: astore 19
    //   459: ldc -70
    //   461: astore_1
    //   462: iload 4
    //   464: istore 16
    //   466: bipush 95
    //   468: istore 17
    //   470: iload_2
    //   471: istore 15
    //   473: iload 14
    //   475: istore 4
    //   477: iload 16
    //   479: istore 14
    //   481: iload 17
    //   483: istore_2
    //   484: iload_2
    //   485: bipush 50
    //   487: if_icmple +75 -> 562
    //   490: iload_2
    //   491: i2d
    //   492: dstore 7
    //   494: iload 6
    //   496: i2d
    //   497: dstore 9
    //   499: dload 9
    //   501: invokestatic 208	java/lang/Double:isNaN	(D)Z
    //   504: pop
    //   505: iload 15
    //   507: i2d
    //   508: dstore 11
    //   510: dload 11
    //   512: invokestatic 208	java/lang/Double:isNaN	(D)Z
    //   515: pop
    //   516: dload 9
    //   518: dconst_1
    //   519: dmul
    //   520: dload 11
    //   522: ddiv
    //   523: invokestatic 214	java/lang/Math:sqrt	(D)D
    //   526: dstore 9
    //   528: dload 7
    //   530: invokestatic 208	java/lang/Double:isNaN	(D)Z
    //   533: pop
    //   534: dload 9
    //   536: dload 7
    //   538: dmul
    //   539: d2i
    //   540: istore_2
    //   541: dload 7
    //   543: invokestatic 208	java/lang/Double:isNaN	(D)Z
    //   546: pop
    //   547: iload_2
    //   548: dload 7
    //   550: ldc2_w 215
    //   553: dmul
    //   554: d2i
    //   555: invokestatic 220	java/lang/Math:min	(II)I
    //   558: istore_2
    //   559: goto +24 -> 583
    //   562: iload 14
    //   564: i2f
    //   565: ldc -35
    //   567: fmul
    //   568: f2i
    //   569: istore 14
    //   571: iload 4
    //   573: i2f
    //   574: ldc -35
    //   576: fmul
    //   577: f2i
    //   578: istore 4
    //   580: bipush 95
    //   582: istore_2
    //   583: aload 22
    //   585: getfield 19	com/truecaller/messaging/transport/mms/i:a	Landroid/content/ContentResolver;
    //   588: astore 20
    //   590: aload 21
    //   592: getfield 134	com/truecaller/messaging/data/types/ImageEntity:b	Landroid/net/Uri;
    //   595: astore 24
    //   597: aload 23
    //   599: getfield 169	android/graphics/BitmapFactory$Options:outWidth	I
    //   602: istore 15
    //   604: aload 23
    //   606: getfield 172	android/graphics/BitmapFactory$Options:outHeight	I
    //   609: istore 16
    //   611: aload 20
    //   613: aload 24
    //   615: iload 15
    //   617: iload 14
    //   619: iload 4
    //   621: aload 19
    //   623: iload_2
    //   624: invokestatic 202	com/truecaller/common/h/n:a	(Landroid/content/ContentResolver;Landroid/net/Uri;IIILandroid/graphics/Bitmap$CompressFormat;I)[B
    //   627: astore 20
    //   629: aload 20
    //   631: arraylength
    //   632: istore 15
    //   634: iload 15
    //   636: iload 6
    //   638: if_icmpgt +13 -> 651
    //   641: iload 15
    //   643: istore 4
    //   645: aload_1
    //   646: astore 19
    //   648: goto +11 -> 659
    //   651: goto -167 -> 484
    //   654: aconst_null
    //   655: areturn
    //   656: iload_2
    //   657: istore 4
    //   659: getstatic 84	java/util/Locale:US	Ljava/util/Locale;
    //   662: ldc 86
    //   664: iconst_1
    //   665: anewarray 4	java/lang/Object
    //   668: dup
    //   669: iconst_0
    //   670: iload_3
    //   671: invokestatic 92	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   674: aastore
    //   675: invokestatic 96	java/lang/String:format	(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   678: astore 23
    //   680: aload 19
    //   682: invokevirtual 175	java/lang/String:hashCode	()I
    //   685: istore_2
    //   686: iload_2
    //   687: ldc -80
    //   689: if_icmpeq +69 -> 758
    //   692: iload_2
    //   693: ldc -34
    //   695: if_icmpeq +48 -> 743
    //   698: iload_2
    //   699: ldc -79
    //   701: if_icmpeq +27 -> 728
    //   704: iload_2
    //   705: ldc -78
    //   707: if_icmpeq +6 -> 713
    //   710: goto +63 -> 773
    //   713: aload 19
    //   715: ldc -76
    //   717: invokevirtual 184	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   720: ifeq +53 -> 773
    //   723: iconst_2
    //   724: istore_2
    //   725: goto +50 -> 775
    //   728: aload 19
    //   730: ldc -70
    //   732: invokevirtual 184	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   735: ifeq +38 -> 773
    //   738: iconst_0
    //   739: istore_2
    //   740: goto +35 -> 775
    //   743: aload 19
    //   745: ldc -32
    //   747: invokevirtual 184	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   750: ifeq +23 -> 773
    //   753: iconst_3
    //   754: istore_2
    //   755: goto +20 -> 775
    //   758: aload 19
    //   760: ldc -68
    //   762: invokevirtual 184	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   765: ifeq +8 -> 773
    //   768: iconst_1
    //   769: istore_2
    //   770: goto +5 -> 775
    //   773: iconst_m1
    //   774: istore_2
    //   775: iload_2
    //   776: tableswitch	default:+32->808, 0:+94->870, 1:+94->870, 2:+64->840, 3:+34->810
    //   808: aconst_null
    //   809: areturn
    //   810: new 226	java/lang/StringBuilder
    //   813: dup
    //   814: invokespecial 227	java/lang/StringBuilder:<init>	()V
    //   817: astore_1
    //   818: aload_1
    //   819: aload 23
    //   821: invokevirtual 231	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   824: pop
    //   825: aload_1
    //   826: ldc -23
    //   828: invokevirtual 231	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   831: pop
    //   832: aload_1
    //   833: invokevirtual 236	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   836: astore_1
    //   837: goto +60 -> 897
    //   840: new 226	java/lang/StringBuilder
    //   843: dup
    //   844: invokespecial 227	java/lang/StringBuilder:<init>	()V
    //   847: astore_1
    //   848: aload_1
    //   849: aload 23
    //   851: invokevirtual 231	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   854: pop
    //   855: aload_1
    //   856: ldc -18
    //   858: invokevirtual 231	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   861: pop
    //   862: aload_1
    //   863: invokevirtual 236	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   866: astore_1
    //   867: goto +30 -> 897
    //   870: new 226	java/lang/StringBuilder
    //   873: dup
    //   874: invokespecial 227	java/lang/StringBuilder:<init>	()V
    //   877: astore_1
    //   878: aload_1
    //   879: aload 23
    //   881: invokevirtual 231	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   884: pop
    //   885: aload_1
    //   886: ldc -16
    //   888: invokevirtual 231	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   891: pop
    //   892: aload_1
    //   893: invokevirtual 236	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   896: astore_1
    //   897: aload 20
    //   899: ifnull +75 -> 974
    //   902: aload 22
    //   904: getfield 19	com/truecaller/messaging/transport/mms/i:a	Landroid/content/ContentResolver;
    //   907: aload 21
    //   909: getfield 134	com/truecaller/messaging/data/types/ImageEntity:b	Landroid/net/Uri;
    //   912: invokevirtual 244	android/content/ContentResolver:openOutputStream	(Landroid/net/Uri;)Ljava/io/OutputStream;
    //   915: astore 22
    //   917: aload 22
    //   919: ifnonnull +10 -> 929
    //   922: aload 22
    //   924: invokestatic 145	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   927: aconst_null
    //   928: areturn
    //   929: aload 22
    //   931: aload 20
    //   933: invokevirtual 249	java/io/OutputStream:write	([B)V
    //   936: aload 22
    //   938: invokevirtual 252	java/io/OutputStream:flush	()V
    //   941: aload 22
    //   943: invokestatic 145	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   946: goto +28 -> 974
    //   949: astore_1
    //   950: goto +7 -> 957
    //   953: astore_1
    //   954: aconst_null
    //   955: astore 22
    //   957: aload 22
    //   959: invokestatic 145	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   962: aload_1
    //   963: athrow
    //   964: aconst_null
    //   965: astore 22
    //   967: aload 22
    //   969: invokestatic 145	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   972: aconst_null
    //   973: areturn
    //   974: aload 5
    //   976: ldc -2
    //   978: iconst_1
    //   979: anewarray 4	java/lang/Object
    //   982: dup
    //   983: iconst_0
    //   984: aload_1
    //   985: aastore
    //   986: invokestatic 257	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   989: invokevirtual 231	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   992: pop
    //   993: new 259	com/truecaller/messaging/transport/mms/PduEntity
    //   996: dup
    //   997: aload 19
    //   999: aload 21
    //   1001: getfield 134	com/truecaller/messaging/data/types/ImageEntity:b	Landroid/net/Uri;
    //   1004: iload 4
    //   1006: iconst_m1
    //   1007: aload 23
    //   1009: aload_1
    //   1010: invokespecial 262	com/truecaller/messaging/transport/mms/PduEntity:<init>	(Ljava/lang/String;Landroid/net/Uri;IILjava/lang/String;Ljava/lang/String;)V
    //   1013: areturn
    //   1014: aconst_null
    //   1015: areturn
    //   1016: iload 16
    //   1018: ifeq +49 -> 1067
    //   1021: iload 4
    //   1023: i2f
    //   1024: iload 14
    //   1026: i2f
    //   1027: fdiv
    //   1028: fstore 13
    //   1030: iload_2
    //   1031: i2f
    //   1032: fload 13
    //   1034: fdiv
    //   1035: f2i
    //   1036: istore 14
    //   1038: iload_2
    //   1039: istore 4
    //   1041: aload 20
    //   1043: astore 22
    //   1045: aload 19
    //   1047: astore 20
    //   1049: iload 15
    //   1051: istore 17
    //   1053: iload 6
    //   1055: istore 16
    //   1057: aload 22
    //   1059: astore 19
    //   1061: iload_3
    //   1062: istore 6
    //   1064: goto -862 -> 202
    //   1067: iload 14
    //   1069: i2f
    //   1070: iload 4
    //   1072: i2f
    //   1073: fdiv
    //   1074: fstore 13
    //   1076: iload 15
    //   1078: i2f
    //   1079: fload 13
    //   1081: fdiv
    //   1082: f2i
    //   1083: istore 4
    //   1085: iload 15
    //   1087: istore 14
    //   1089: iconst_1
    //   1090: istore 16
    //   1092: goto -860 -> 232
    //   1095: astore_1
    //   1096: goto +7 -> 1103
    //   1099: astore_1
    //   1100: aconst_null
    //   1101: astore 20
    //   1103: aload 20
    //   1105: invokestatic 145	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   1108: aload_1
    //   1109: athrow
    //   1110: aconst_null
    //   1111: astore 20
    //   1113: aload 20
    //   1115: invokestatic 145	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   1118: aconst_null
    //   1119: areturn
    //   1120: astore_1
    //   1121: goto -11 -> 1110
    //   1124: astore_1
    //   1125: goto -12 -> 1113
    //   1128: astore_1
    //   1129: goto -115 -> 1014
    //   1132: astore_1
    //   1133: goto -479 -> 654
    //   1136: astore_1
    //   1137: goto -173 -> 964
    //   1140: astore_1
    //   1141: goto -174 -> 967
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1144	0	this	i
    //   0	1144	1	paramImageEntity	ImageEntity
    //   0	1144	2	paramInt1	int
    //   0	1144	3	paramInt2	int
    //   0	1144	4	paramInt3	int
    //   0	1144	5	paramStringBuilder	StringBuilder
    //   0	1144	6	paramInt4	int
    //   492	57	7	d1	double
    //   497	38	9	d2	double
    //   508	13	11	d3	double
    //   1028	52	13	f	float
    //   79	1009	14	i	int
    //   72	1014	15	j	int
    //   39	1052	16	k	int
    //   139	913	17	m	int
    //   147	82	18	n	int
    //   7	1053	19	localObject1	Object
    //   20	1094	20	localObject2	Object
    //   65	935	21	localObject3	Object
    //   107	951	22	localObject4	Object
    //   101	907	23	localObject5	Object
    //   384	230	24	localUri	Uri
    // Exception table:
    //   from	to	target	type
    //   929	941	949	finally
    //   902	917	953	finally
    //   34	56	1095	finally
    //   9	22	1099	finally
    //   9	22	1120	java/io/IOException
    //   9	22	1120	java/lang/SecurityException
    //   34	56	1124	java/io/IOException
    //   34	56	1124	java/lang/SecurityException
    //   373	421	1128	java/io/IOException
    //   373	421	1128	java/lang/SecurityException
    //   583	634	1132	java/io/IOException
    //   583	634	1132	java/lang/SecurityException
    //   902	917	1136	java/io/IOException
    //   902	917	1136	java/lang/SecurityException
    //   929	941	1140	java/io/IOException
    //   929	941	1140	java/lang/SecurityException
  }
  
  /* Error */
  private File a(byte[] paramArrayOfByte)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 30	com/truecaller/messaging/transport/mms/i:c	Ljava/io/File;
    //   4: invokevirtual 266	java/io/File:exists	()Z
    //   7: istore_2
    //   8: aconst_null
    //   9: astore_3
    //   10: iload_2
    //   11: ifne +15 -> 26
    //   14: aload_0
    //   15: getfield 30	com/truecaller/messaging/transport/mms/i:c	Ljava/io/File;
    //   18: invokevirtual 269	java/io/File:mkdirs	()Z
    //   21: ifne +5 -> 26
    //   24: aconst_null
    //   25: areturn
    //   26: new 23	java/io/File
    //   29: dup
    //   30: aload_0
    //   31: getfield 30	com/truecaller/messaging/transport/mms/i:c	Ljava/io/File;
    //   34: bipush 45
    //   36: invokestatic 274	org/c/a/a/a/i:b	(I)Ljava/lang/String;
    //   39: invokespecial 28	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   42: astore 5
    //   44: new 276	java/io/FileOutputStream
    //   47: dup
    //   48: aload 5
    //   50: invokespecial 279	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   53: astore 4
    //   55: aload 4
    //   57: astore_3
    //   58: aload 4
    //   60: aload_1
    //   61: invokevirtual 280	java/io/FileOutputStream:write	([B)V
    //   64: aload 4
    //   66: invokestatic 145	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   69: aload 5
    //   71: areturn
    //   72: astore_3
    //   73: aload 4
    //   75: astore_1
    //   76: aload_3
    //   77: astore 4
    //   79: goto +11 -> 90
    //   82: astore_1
    //   83: goto +35 -> 118
    //   86: astore 4
    //   88: aconst_null
    //   89: astore_1
    //   90: aload_1
    //   91: astore_3
    //   92: aload 4
    //   94: invokestatic 286	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   97: aload_1
    //   98: invokestatic 145	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   101: aload 5
    //   103: invokevirtual 266	java/io/File:exists	()Z
    //   106: ifeq +9 -> 115
    //   109: aload 5
    //   111: invokevirtual 289	java/io/File:delete	()Z
    //   114: pop
    //   115: aconst_null
    //   116: areturn
    //   117: astore_1
    //   118: aload_3
    //   119: invokestatic 145	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   122: aload 5
    //   124: invokevirtual 266	java/io/File:exists	()Z
    //   127: ifeq +9 -> 136
    //   130: aload 5
    //   132: invokevirtual 289	java/io/File:delete	()Z
    //   135: pop
    //   136: aload_1
    //   137: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	138	0	this	i
    //   0	138	1	paramArrayOfByte	byte[]
    //   7	4	2	bool	boolean
    //   9	49	3	localObject1	Object
    //   72	5	3	localIOException1	java.io.IOException
    //   91	28	3	arrayOfByte	byte[]
    //   53	25	4	localObject2	Object
    //   86	7	4	localIOException2	java.io.IOException
    //   42	89	5	localFile	File
    // Exception table:
    //   from	to	target	type
    //   58	64	72	java/io/IOException
    //   44	55	82	finally
    //   44	55	86	java/io/IOException
    //   58	64	117	finally
    //   92	97	117	finally
  }
  
  /* Error */
  private List<Entity> a(j paramj)
  {
    // Byte code:
    //   0: new 292	java/util/ArrayList
    //   3: dup
    //   4: invokespecial 293	java/util/ArrayList:<init>	()V
    //   7: astore 10
    //   9: aload_0
    //   10: getfield 30	com/truecaller/messaging/transport/mms/i:c	Ljava/io/File;
    //   13: invokevirtual 266	java/io/File:exists	()Z
    //   16: ifne +16 -> 32
    //   19: aload_0
    //   20: getfield 30	com/truecaller/messaging/transport/mms/i:c	Ljava/io/File;
    //   23: invokevirtual 269	java/io/File:mkdirs	()Z
    //   26: ifne +6 -> 32
    //   29: aload 10
    //   31: areturn
    //   32: aload_1
    //   33: getfield 296	com/android/a/a/a/j:a	Ljava/util/Vector;
    //   36: invokevirtual 301	java/util/Vector:size	()I
    //   39: istore 5
    //   41: iconst_0
    //   42: istore_2
    //   43: iload_2
    //   44: iload 5
    //   46: if_icmpge +212 -> 258
    //   49: aload_1
    //   50: iload_2
    //   51: invokevirtual 304	com/android/a/a/a/j:a	(I)Lcom/android/a/a/a/o;
    //   54: astore 11
    //   56: new 23	java/io/File
    //   59: dup
    //   60: aload_0
    //   61: getfield 30	com/truecaller/messaging/transport/mms/i:c	Ljava/io/File;
    //   64: bipush 45
    //   66: invokestatic 274	org/c/a/a/a/i:b	(I)Ljava/lang/String;
    //   69: invokespecial 28	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   72: astore 9
    //   74: aconst_null
    //   75: astore 8
    //   77: aconst_null
    //   78: astore 6
    //   80: new 276	java/io/FileOutputStream
    //   83: dup
    //   84: aload 9
    //   86: invokespecial 279	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   89: astore 7
    //   91: aload 11
    //   93: getfield 307	com/android/a/a/a/o:f	[B
    //   96: astore 6
    //   98: aload 6
    //   100: ifnull +20 -> 120
    //   103: aload 7
    //   105: aload 6
    //   107: invokevirtual 280	java/io/FileOutputStream:write	([B)V
    //   110: aload 6
    //   112: arraylength
    //   113: istore_3
    //   114: iconst_0
    //   115: istore 4
    //   117: goto +8 -> 125
    //   120: iconst_1
    //   121: istore 4
    //   123: iconst_0
    //   124: istore_3
    //   125: aload 7
    //   127: invokestatic 145	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   130: iload 4
    //   132: ifeq +17 -> 149
    //   135: aload 9
    //   137: invokevirtual 266	java/io/File:exists	()Z
    //   140: ifeq +9 -> 149
    //   143: aload 9
    //   145: invokevirtual 289	java/io/File:delete	()Z
    //   148: pop
    //   149: aload 10
    //   151: new 259	com/truecaller/messaging/transport/mms/PduEntity
    //   154: dup
    //   155: aload 11
    //   157: aload 9
    //   159: invokestatic 313	android/net/Uri:fromFile	(Ljava/io/File;)Landroid/net/Uri;
    //   162: iload_3
    //   163: invokespecial 316	com/truecaller/messaging/transport/mms/PduEntity:<init>	(Lcom/android/a/a/a/o;Landroid/net/Uri;I)V
    //   166: invokeinterface 321 2 0
    //   171: pop
    //   172: goto +58 -> 230
    //   175: astore_1
    //   176: aload 7
    //   178: astore 6
    //   180: goto +57 -> 237
    //   183: astore 8
    //   185: goto +17 -> 202
    //   188: astore_1
    //   189: goto +48 -> 237
    //   192: astore 6
    //   194: aload 8
    //   196: astore 7
    //   198: aload 6
    //   200: astore 8
    //   202: aload 7
    //   204: astore 6
    //   206: aload 8
    //   208: invokestatic 286	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   211: aload 7
    //   213: invokestatic 145	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   216: aload 9
    //   218: invokevirtual 266	java/io/File:exists	()Z
    //   221: ifeq +9 -> 230
    //   224: aload 9
    //   226: invokevirtual 289	java/io/File:delete	()Z
    //   229: pop
    //   230: iload_2
    //   231: iconst_1
    //   232: iadd
    //   233: istore_2
    //   234: goto -191 -> 43
    //   237: aload 6
    //   239: invokestatic 145	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   242: aload 9
    //   244: invokevirtual 266	java/io/File:exists	()Z
    //   247: ifeq +9 -> 256
    //   250: aload 9
    //   252: invokevirtual 289	java/io/File:delete	()Z
    //   255: pop
    //   256: aload_1
    //   257: athrow
    //   258: aload 10
    //   260: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	261	0	this	i
    //   0	261	1	paramj	j
    //   42	192	2	i	int
    //   113	50	3	j	int
    //   115	16	4	k	int
    //   39	8	5	m	int
    //   78	101	6	localObject1	Object
    //   192	7	6	localIOException1	java.io.IOException
    //   204	34	6	localObject2	Object
    //   89	123	7	localObject3	Object
    //   75	1	8	localObject4	Object
    //   183	12	8	localIOException2	java.io.IOException
    //   200	7	8	localObject5	Object
    //   72	179	9	localFile	File
    //   7	252	10	localArrayList	ArrayList
    //   54	102	11	localo	o
    // Exception table:
    //   from	to	target	type
    //   91	98	175	finally
    //   103	114	175	finally
    //   91	98	183	java/io/IOException
    //   103	114	183	java/io/IOException
    //   80	91	188	finally
    //   206	211	188	finally
    //   80	91	192	java/io/IOException
  }
  
  private void a(Uri paramUri)
  {
    if (!"file".equals(paramUri.getScheme())) {
      return;
    }
    paramUri = new File(paramUri.getPath());
    if (!paramUri.exists()) {
      return;
    }
    if (!paramUri.getParentFile().getAbsolutePath().equals(c.getAbsolutePath())) {
      return;
    }
    paramUri.delete();
  }
  
  /* Error */
  private byte[] a(PduEntity paramPduEntity)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: aload_0
    //   3: getfield 19	com/truecaller/messaging/transport/mms/i:a	Landroid/content/ContentResolver;
    //   6: aload_1
    //   7: getfield 344	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   10: invokevirtual 140	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   13: astore_2
    //   14: aload_2
    //   15: ifnonnull +9 -> 24
    //   18: aload_2
    //   19: invokestatic 145	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   22: aconst_null
    //   23: areturn
    //   24: aload_2
    //   25: astore_1
    //   26: aload_2
    //   27: invokestatic 349	com/truecaller/utils/extensions/m:a	(Ljava/io/InputStream;)[B
    //   30: astore_3
    //   31: aload_2
    //   32: invokestatic 145	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   35: aload_3
    //   36: areturn
    //   37: astore_3
    //   38: goto +12 -> 50
    //   41: astore_2
    //   42: aload_3
    //   43: astore_1
    //   44: goto +23 -> 67
    //   47: astore_3
    //   48: aconst_null
    //   49: astore_2
    //   50: aload_2
    //   51: astore_1
    //   52: aload_3
    //   53: iconst_0
    //   54: anewarray 67	java/lang/String
    //   57: invokestatic 355	com/truecaller/log/AssertionUtil$OnlyInDebug:shouldNeverHappen	(Ljava/lang/Throwable;[Ljava/lang/String;)V
    //   60: aload_2
    //   61: invokestatic 145	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   64: aconst_null
    //   65: areturn
    //   66: astore_2
    //   67: aload_1
    //   68: invokestatic 145	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   71: aload_2
    //   72: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	73	0	this	i
    //   0	73	1	paramPduEntity	PduEntity
    //   13	19	2	localInputStream	java.io.InputStream
    //   41	1	2	localObject1	Object
    //   49	12	2	localCloseable	java.io.Closeable
    //   66	6	2	localObject2	Object
    //   1	35	3	arrayOfByte	byte[]
    //   37	6	3	localIOException1	java.io.IOException
    //   47	6	3	localIOException2	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   26	31	37	java/io/IOException
    //   2	14	41	finally
    //   2	14	47	java/io/IOException
    //   26	31	66	finally
    //   52	60	66	finally
  }
  
  /* Error */
  private int b(Uri paramUri)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: aconst_null
    //   3: astore 4
    //   5: aload_0
    //   6: getfield 19	com/truecaller/messaging/transport/mms/i:a	Landroid/content/ContentResolver;
    //   9: aload_1
    //   10: invokevirtual 140	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   13: astore_1
    //   14: aload_1
    //   15: astore_3
    //   16: aload_1
    //   17: ifnull +27 -> 44
    //   20: aload_1
    //   21: astore 4
    //   23: aload_1
    //   24: astore_3
    //   25: aload_1
    //   26: invokevirtual 151	java/io/InputStream:available	()I
    //   29: istore_2
    //   30: aload_1
    //   31: invokestatic 145	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   34: iload_2
    //   35: ireturn
    //   36: astore_1
    //   37: aload 4
    //   39: invokestatic 145	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   42: aload_1
    //   43: athrow
    //   44: aload_3
    //   45: invokestatic 145	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   48: iconst_m1
    //   49: ireturn
    //   50: astore_1
    //   51: goto -7 -> 44
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	54	0	this	i
    //   0	54	1	paramUri	Uri
    //   29	6	2	i	int
    //   1	44	3	localUri1	Uri
    //   3	35	4	localUri2	Uri
    // Exception table:
    //   from	to	target	type
    //   5	14	36	finally
    //   25	30	36	finally
    //   5	14	50	java/io/IOException
    //   5	14	50	java/lang/SecurityException
    //   25	30	50	java/io/IOException
    //   25	30	50	java/lang/SecurityException
  }
  
  public final f a(String paramString1, long paramLong, int paramInt1, int paramInt2, int paramInt3, List<String> paramList, h.a parama, String paramString2)
  {
    com.android.a.a.a.u localu = new com.android.a.a.a.u();
    paramString2 = b.a(paramString2);
    ArrayList localArrayList = new ArrayList(paramList.size());
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      String str = (String)paramList.next();
      if ((!k.d(str)) && (!k.a(paramString2, str))) {
        localArrayList.add(new e(str));
      }
    }
    paramList = (e[])localArrayList.toArray(new e[localArrayList.size()]);
    paramString2 = a;
    AssertionUtil.AlwaysFatal.isNotNull(paramList, new String[0]);
    localArrayList = new ArrayList();
    Collections.addAll(localArrayList, paramList);
    a.put(151, localArrayList);
    localu.a(paramLong);
    if (paramString1 != null) {
      localu.a(paramString1.getBytes());
    }
    try
    {
      localu.b(paramInt1);
      localu.c(paramInt2);
      localu.d(paramInt3);
      paramString1 = a(parama);
      if (paramString1.a() == 0) {
        return null;
      }
      b = paramString1;
      localu.b(paramString1.a());
      return localu;
    }
    catch (com.android.a.a.a paramString1) {}
    return null;
  }
  
  public final Message a(f paramf, boolean paramBoolean, String paramString, long paramLong)
  {
    Message.a locala = new Message.a();
    m localm = a;
    MmsTransportInfo.a locala1 = new MmsTransportInfo.a();
    Object localObject3 = a;
    Object localObject1 = ((m)localObject3).c(150);
    if (localObject1 != null) {
      locala1.a(c.a(((e)localObject1).a()), a);
    }
    localObject1 = ((m)localObject3).c(154);
    if (localObject1 != null) {
      locala1.b(c.a(((e)localObject1).a()), a);
    }
    localObject1 = ((m)localObject3).b(131);
    if (localObject1 != null)
    {
      localObject1 = c.a((byte[])localObject1);
      if (((String)localObject1).length() != 0) {
        k = Uri.parse((String)localObject1);
      }
    }
    localObject1 = ((m)localObject3).b(132);
    if (localObject1 != null) {
      l = c.a((byte[])localObject1);
    }
    localObject1 = ((m)localObject3).b(152);
    if (localObject1 != null) {
      p = c.a((byte[])localObject1);
    }
    localObject1 = ((m)localObject3).c(147);
    if (localObject1 != null) {
      n = ((e)localObject1).b();
    }
    localObject1 = ((m)localObject3).b(139);
    if (localObject1 != null) {
      u = c.a((byte[])localObject1);
    }
    localObject1 = ((m)localObject3).b(138);
    if (localObject1 != null) {
      o = c.a((byte[])localObject1);
    }
    int i = ((m)localObject3).a(149);
    if (i != 0) {
      c = i;
    }
    i = ((m)localObject3).a(153);
    if (i != 0) {
      s = i;
    }
    i = ((m)localObject3).a(146);
    if (i != 0) {
      t = i;
    }
    i = ((m)localObject3).a(140);
    if (i != 0) {
      w = i;
    }
    i = ((m)localObject3).a(143);
    if (i != 0) {
      r = i;
    }
    i = ((m)localObject3).a(141);
    if (i != 0) {
      f = i;
    }
    i = ((m)localObject3).a(186);
    if (i != 0) {
      m = i;
    }
    i = ((m)localObject3).a(134);
    if (i != 0) {
      y = i;
    }
    i = ((m)localObject3).a(144);
    if (i != 0) {
      A = i;
    }
    i = ((m)localObject3).a(155);
    if (i != 0) {
      B = i;
    }
    if (((m)localObject3).a(145) != 0) {
      C = true;
    }
    long l = ((m)localObject3).e(135);
    if (l != -1L) {
      z = l;
    }
    l = ((m)localObject3).e(136);
    if (l != -1L) {
      locala1.e(l);
    }
    l = ((m)localObject3).e(142);
    if (l != -1L) {
      x = ((int)l);
    }
    localObject1 = ((m)localObject3).c(137);
    if (localObject1 != null) {
      localObject1 = ((e)localObject1).b();
    } else {
      localObject1 = null;
    }
    Object localObject2 = localObject1;
    if (localObject1 == null) {
      localObject2 = "Unknown sender";
    }
    locala1.a(137, (String)localObject2);
    localObject2 = ao.a;
    int k = localObject2.length;
    i = 0;
    localObject1 = localObject3;
    while (i < k)
    {
      int m = localObject2[i];
      localObject3 = ((m)localObject1).d(m);
      if (localObject3 != null)
      {
        int n = localObject3.length;
        int j = 0;
        while (j < n)
        {
          String str = localObject3[j].b();
          if (!k.d(str)) {
            locala1.a(m, str);
          }
          j += 1;
        }
      }
      i += 1;
    }
    if (paramLong != -1L)
    {
      b = paramLong;
      locala1.c(paramLong);
    }
    localObject1 = locala1.a();
    g = false;
    h = false;
    i = false;
    k = 3;
    paramLong = localm.e(133);
    if (paramLong != -1L) {
      locala.c(paramLong * 1000L);
    } else {
      e = b.ay_().f();
    }
    f = ((MmsTransportInfo)localObject1).h();
    locala.a(1, (TransportInfo)localObject1);
    locala.a(paramString);
    i = paramf.a();
    if (i != 128)
    {
      if (i != 130)
      {
        if (i != 132)
        {
          paramString = null;
          break label1034;
        }
      }
      else {
        g = paramBoolean;
      }
      AssertionUtil.AlwaysFatal.isNotNull(E, new String[0]);
      localObject1 = (Set)E.get(137);
      AssertionUtil.AlwaysFatal.isNotNull(localObject1, new String[0]);
      paramString = Participant.a((String)((Set)localObject1).iterator().next(), b, paramString);
    }
    else
    {
      AssertionUtil.AlwaysFatal.isNotNull(E, new String[0]);
      localObject1 = (Set)E.get(151);
      AssertionUtil.AlwaysFatal.isNotNull(localObject1, new String[0]);
      paramString = Participant.a((String)((Set)localObject1).iterator().next(), b, paramString);
    }
    label1034:
    localObject1 = paramString;
    if (paramString == null)
    {
      paramString = new Participant.a(1);
      d = "Unknown sender";
      e = "Unknown sender";
      localObject1 = paramString.a();
    }
    c = ((Participant)localObject1);
    if ((paramf instanceof g))
    {
      paramf = b;
      if (paramf != null) {
        locala.a(a(paramf));
      }
    }
    return locala.b();
  }
  
  public final List<PduEntity> a(Entity[] paramArrayOfEntity, com.truecaller.multisim.a parama)
  {
    ArrayList localArrayList = new ArrayList();
    StringBuilder localStringBuilder1 = new StringBuilder();
    int i1 = parama.j();
    int k = 0;
    int i = 0;
    int m = 0;
    boolean bool1 = false;
    int n;
    for (;;)
    {
      Object localObject1 = paramArrayOfEntity;
      j = localObject1.length;
      n = 1;
      if (k >= j) {
        break;
      }
      Object localObject2 = localObject1[k];
      if (((Entity)localObject2).b())
      {
        i += 1;
      }
      else
      {
        if ((localObject2 instanceof PduEntity))
        {
          localObject1 = (PduEntity)localObject2;
        }
        else
        {
          AssertionUtil.AlwaysFatal.isFalse(((Entity)localObject2).b(), new String[0]);
          Object localObject3;
          Object localObject4;
          if (((Entity)localObject2).a())
          {
            Object localObject5 = (TextEntity)localObject2;
            localObject3 = a.getBytes();
            localObject4 = a((byte[])localObject3);
            if (localObject4 != null)
            {
              localObject1 = String.format(Locale.US, "text.%06d", new Object[] { Integer.valueOf(k) });
              StringBuilder localStringBuilder2 = new StringBuilder();
              localStringBuilder2.append((String)localObject1);
              localStringBuilder2.append(".txt");
              localStringBuilder1.append(String.format("<par dur=\"5000ms\"><text src=\"%s\" region=\"Text\" /></par>", new Object[] { localStringBuilder2.toString() }));
              localObject5 = j;
              localObject4 = Uri.fromFile((File)localObject4);
              j = localObject3.length;
              localObject3 = new StringBuilder();
              ((StringBuilder)localObject3).append((String)localObject1);
              ((StringBuilder)localObject3).append(".txt");
              localObject1 = new PduEntity((String)localObject5, (Uri)localObject4, j, 106, (String)localObject1, ((StringBuilder)localObject3).toString());
              break label709;
            }
          }
          else if (((Entity)localObject2).d())
          {
            localObject1 = (BinaryEntity)localObject2;
            j = b(b);
            if (j >= 0)
            {
              localObject3 = String.format(Locale.US, "vcard.%06d", new Object[] { Integer.valueOf(k) });
              localObject4 = new StringBuilder();
              ((StringBuilder)localObject4).append((String)localObject3);
              ((StringBuilder)localObject4).append(".vcf");
              localObject4 = ((StringBuilder)localObject4).toString();
              localStringBuilder1.append(String.format("<par dur=\"5000ms\"><text src=\"%s\" region=\"Text\" /></par>", new Object[] { localObject4 }));
              localObject1 = new PduEntity(j, b, j, 106, (String)localObject3, (String)localObject4);
              break label709;
            }
          }
          else if (((Entity)localObject2).c())
          {
            localObject3 = (BinaryEntity)localObject2;
            n = b(b);
            if (n >= 0)
            {
              localObject4 = String.format(Locale.US, "video.%06d", new Object[] { Integer.valueOf(k) });
              localObject1 = j;
              j = -1;
              int i2 = ((String)localObject1).hashCode();
              if (i2 != -1664118616)
              {
                if ((i2 == 1331848029) && (((String)localObject1).equals("video/mp4"))) {
                  j = 1;
                }
              }
              else if (((String)localObject1).equals("video/3gpp")) {
                j = 0;
              }
              switch (j)
              {
              default: 
                break;
              case 1: 
                localObject1 = new StringBuilder();
                ((StringBuilder)localObject1).append((String)localObject4);
                ((StringBuilder)localObject1).append(".mp4");
                localObject1 = ((StringBuilder)localObject1).toString();
                break;
              case 0: 
                localObject1 = new StringBuilder();
                ((StringBuilder)localObject1).append((String)localObject4);
                ((StringBuilder)localObject1).append(".3gp");
                localObject1 = ((StringBuilder)localObject1).toString();
              }
              localStringBuilder1.append(String.format(Locale.US, "<par dur=\"%2$dms\"><video src=\"%1$s\" dur=\"%2$dms\" /></par>", new Object[] { localObject1, Integer.valueOf(5000) }));
              localObject1 = new PduEntity(j, b, n, -1, (String)localObject4, (String)localObject1);
              break label709;
            }
          }
          localObject1 = null;
        }
        label709:
        if (localObject1 != null)
        {
          boolean bool2 = ((Entity)localObject2).a();
          localArrayList.add(localObject1);
          m = (int)(m + d);
          if (m > i1)
          {
            paramArrayOfEntity = localArrayList.iterator();
            while (paramArrayOfEntity.hasNext()) {
              a(nextb);
            }
            return null;
          }
          bool1 = bool2 | bool1;
        }
      }
      k += 1;
    }
    if (i > 0)
    {
      k = (i1 - m) / i;
      if (k < 2048)
      {
        paramArrayOfEntity = localArrayList.iterator();
        while (paramArrayOfEntity.hasNext()) {
          a(nextb);
        }
        return null;
      }
      m = parama.h();
      i1 = parama.i();
      i = 0;
      for (;;)
      {
        j = n;
        if (i >= paramArrayOfEntity.length) {
          break;
        }
        parama = paramArrayOfEntity[i];
        if (parama.b())
        {
          parama = a((ImageEntity)parama, m, i1, k, localStringBuilder1, i);
          if (parama != null) {
            localArrayList.add(parama);
          }
        }
        i += 1;
      }
    }
    int j = 0;
    if (j != 0)
    {
      if (bool1) {
        localStringBuilder1.insert(0, "<smil><head><layout><root-layout/><region id=\"Image\" fit=\"meet\" top=\"0\" left=\"0\" height=\"80%%\" width=\"100%%\"/><region id=\"Text\" top=\"80%%\" left=\"0\" height=\"20%%\" width=\"100%%\"/></layout></head><body>");
      } else {
        localStringBuilder1.insert(0, "<smil><head><layout><root-layout/><region id=\"Image\" fit=\"meet\" top=\"0\" left=\"0\" height=\"100%%\" width=\"100%%\"/></layout></head><body>");
      }
    }
    else {
      localStringBuilder1.append("<smil><head><layout><root-layout/><region id=\"Text\" top=\"0\" left=\"0\" height=\"100%%\" width=\"100%%\"/></layout></head><body>");
    }
    localStringBuilder1.append("</body></smil>");
    paramArrayOfEntity = localStringBuilder1.toString().getBytes();
    parama = a(paramArrayOfEntity);
    if (parama == null) {
      paramArrayOfEntity = null;
    } else {
      paramArrayOfEntity = new PduEntity("application/smil", Uri.fromFile(parama), paramArrayOfEntity.length, 106, "smil", "smil.xml");
    }
    if (paramArrayOfEntity != null) {
      localArrayList.add(0, paramArrayOfEntity);
    }
    return localArrayList;
  }
  
  @SuppressLint({"InlinedApi"})
  public final void a(PduEntity paramPduEntity, ContentValues paramContentValues)
  {
    paramContentValues.put("chset", Integer.valueOf(a));
    paramContentValues.put("ct", j);
    Object localObject;
    if ("text/plain".equals(j))
    {
      localObject = a(paramPduEntity);
      if (localObject != null) {
        localObject = new e(a, (byte[])localObject).b();
      } else {
        localObject = "";
      }
      if (((String)localObject).startsWith("BEGIN:VCARD")) {
        paramContentValues.put("ct", j);
      } else {
        paramContentValues.put("text", (String)localObject);
      }
    }
    if ("application/smil".equals(j))
    {
      paramContentValues.put("chset", Integer.valueOf(-1));
      localObject = a(paramPduEntity);
      if (localObject != null) {
        paramContentValues.put("text", new e(a, (byte[])localObject).b());
      } else {
        paramContentValues.put("text", "");
      }
    }
    if (Entity.f(j)) {
      paramContentValues.put("ct", "text/x-vcard");
    }
    if (k != null) {
      paramContentValues.put("fn", k);
    }
    if (l != null) {
      paramContentValues.put("name", l);
    }
    if (m != null) {
      paramContentValues.put("cd", m);
    }
    if (n != null) {
      paramContentValues.put("cid", n);
    }
    if (o != null) {
      paramContentValues.put("cl", o);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */