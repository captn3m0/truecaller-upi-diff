package com.truecaller.messaging.transport.mms;

import com.truecaller.messaging.transport.ad.b;
import com.truecaller.messaging.transport.l;
import dagger.a.d;
import javax.inject.Provider;

public final class x
  implements d<l>
{
  private final n a;
  private final Provider<l> b;
  private final Provider<ad.b> c;
  
  private x(n paramn, Provider<l> paramProvider, Provider<ad.b> paramProvider1)
  {
    a = paramn;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static x a(n paramn, Provider<l> paramProvider, Provider<ad.b> paramProvider1)
  {
    return new x(paramn, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.x
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */