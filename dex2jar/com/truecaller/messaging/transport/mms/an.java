package com.truecaller.messaging.transport.mms;

import android.annotation.SuppressLint;
import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.net.Uri.Builder;
import android.text.TextUtils;
import com.truecaller.common.h.am;
import com.truecaller.content.TruecallerContract.z;
import com.truecaller.featuretoggles.e;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.transport.i;
import com.truecaller.messaging.transport.l.b;
import com.truecaller.multisim.h;
import com.truecaller.util.bd;
import com.truecaller.util.de;
import com.truecaller.utils.n;
import com.truecaller.utils.q;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.a.a.b;

final class an
  extends com.truecaller.messaging.transport.c<MmsTransportInfo, an.a>
{
  private static final Uri d = Uri.parse("content://mms/part");
  @SuppressLint({"InlinedApi"})
  private static final String[] e = { "_id", "mid", "ct", "chset", "text" };
  private final n f;
  private final bd g;
  private android.support.v4.f.f<Integer> h;
  private android.support.v4.f.f<Long> i;
  private StringBuilder j;
  
  an(Context paramContext, h paramh, n paramn, bd parambd, e parame)
  {
    super(paramContext, paramh, parame);
    f = paramn;
    g = parambd;
  }
  
  /* Error */
  private static BitmapFactory.Options a(Context paramContext, Uri paramUri)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 71	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   4: astore_0
    //   5: aload_0
    //   6: aload_1
    //   7: invokevirtual 77	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   10: astore_1
    //   11: aload_1
    //   12: astore_0
    //   13: new 79	android/graphics/BitmapFactory$Options
    //   16: dup
    //   17: invokespecial 81	android/graphics/BitmapFactory$Options:<init>	()V
    //   20: astore_3
    //   21: aload_1
    //   22: astore_0
    //   23: aload_3
    //   24: iconst_1
    //   25: putfield 85	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
    //   28: aload_1
    //   29: astore_0
    //   30: aload_1
    //   31: aconst_null
    //   32: aload_3
    //   33: invokestatic 91	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   36: pop
    //   37: aload_1
    //   38: astore_0
    //   39: aload_3
    //   40: getfield 95	android/graphics/BitmapFactory$Options:outMimeType	Ljava/lang/String;
    //   43: invokestatic 101	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   46: istore_2
    //   47: iload_2
    //   48: ifeq +9 -> 57
    //   51: aload_1
    //   52: invokestatic 106	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   55: aconst_null
    //   56: areturn
    //   57: aload_1
    //   58: invokestatic 106	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   61: aload_3
    //   62: areturn
    //   63: astore_1
    //   64: goto +28 -> 92
    //   67: astore_3
    //   68: goto +12 -> 80
    //   71: astore_1
    //   72: aconst_null
    //   73: astore_0
    //   74: goto +18 -> 92
    //   77: astore_3
    //   78: aconst_null
    //   79: astore_1
    //   80: aload_1
    //   81: astore_0
    //   82: aload_3
    //   83: invokestatic 112	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   86: aload_1
    //   87: invokestatic 106	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   90: aconst_null
    //   91: areturn
    //   92: aload_0
    //   93: invokestatic 106	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   96: aload_1
    //   97: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	98	0	paramContext	Context
    //   0	98	1	paramUri	Uri
    //   46	2	2	bool	boolean
    //   20	42	3	localOptions	BitmapFactory.Options
    //   67	1	3	localFileNotFoundException1	java.io.FileNotFoundException
    //   77	6	3	localFileNotFoundException2	java.io.FileNotFoundException
    // Exception table:
    //   from	to	target	type
    //   13	21	63	finally
    //   23	28	63	finally
    //   30	37	63	finally
    //   39	47	63	finally
    //   82	86	63	finally
    //   13	21	67	java/io/FileNotFoundException
    //   23	28	67	java/io/FileNotFoundException
    //   30	37	67	java/io/FileNotFoundException
    //   39	47	67	java/io/FileNotFoundException
    //   5	11	71	finally
    //   5	11	77	java/io/FileNotFoundException
  }
  
  private void a(String paramString, android.support.v4.f.f<Integer> paramf, android.support.v4.f.f<Long> paramf1, List<ContentProviderOperation> paramList)
  {
    if ((paramf1.b() == 0) && (paramf.b() == 0)) {
      return;
    }
    Object localObject1 = a.getContentResolver();
    for (;;)
    {
      try
      {
        localObject1 = ((ContentResolver)localObject1).query(d, e, paramString, null, null);
        if (localObject1 != null) {
          try
          {
            if (((Cursor)localObject1).moveToNext())
            {
              str = ((Cursor)localObject1).getString(2);
              if ("application/smil".equals(str)) {
                continue;
              }
              k = 0;
              paramString = ((Cursor)localObject1).getString(0);
              long l = ((Cursor)localObject1).getLong(1);
              if (!((Cursor)localObject1).isNull(3)) {
                k = ((Cursor)localObject1).getInt(3);
              }
              localObject2 = ((Cursor)localObject1).getString(4);
              paramString = d.buildUpon().appendPath(paramString).build();
              localInteger = (Integer)paramf.a(l, null);
              localLong = (Long)paramf1.a(l, null);
              if (Entity.a(str))
              {
                localObject2 = com.android.a.a.c.a(am.n((String)localObject2), k);
                if (k == 0) {
                  paramString = new String((byte[])localObject2);
                }
              }
            }
          }
          finally
          {
            String str;
            Object localObject2;
            Integer localInteger;
            Long localLong;
            paramString = (String)localObject1;
          }
        }
      }
      finally
      {
        paramString = null;
        if (paramString != null) {
          paramString.close();
        }
      }
      try
      {
        paramString = new String((byte[])localObject2, com.android.a.a.a.c.a(k));
      }
      catch (UnsupportedEncodingException paramString)
      {
        continue;
      }
      try
      {
        paramString = new String((byte[])localObject2, "iso-8859-1");
      }
      catch (UnsupportedEncodingException paramString)
      {
        continue;
        m = -1;
        k = -1;
        n = -1;
        continue;
        k = -1;
        m = -1;
        n = -1;
        continue;
      }
      paramString = new String((byte[])localObject2);
      if (!TextUtils.isEmpty(paramString))
      {
        a(paramList, localInteger, localLong, str, paramString, -1, -1, -1, "");
        continue;
        if (Entity.c(str))
        {
          localObject2 = a(a, paramString);
          if (localObject2 == null) {
            break label436;
          }
          k = outWidth;
          m = outHeight;
          break label442;
        }
        if (!Entity.d(str)) {
          break label448;
        }
        localObject2 = g.a(paramString);
        if (localObject2 == null) {
          break label448;
        }
        k = a;
        m = b;
        n = c;
        a(paramList, localInteger, localLong, str, paramString.toString(), k, m, n, "");
      }
    }
    if (localObject1 != null)
    {
      ((Cursor)localObject1).close();
      return;
    }
  }
  
  private static void a(List<ContentProviderOperation> paramList, Integer paramInteger, Long paramLong, String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, String paramString3)
  {
    ContentProviderOperation.Builder localBuilder = ContentProviderOperation.newInsert(TruecallerContract.z.a());
    localBuilder.withValue("type", paramString1);
    localBuilder.withValue("content", paramString2);
    localBuilder.withValue("width", Integer.valueOf(paramInt1));
    localBuilder.withValue("height", Integer.valueOf(paramInt2));
    localBuilder.withValue("duration", Integer.valueOf(paramInt3));
    localBuilder.withValue("thumbnail", paramString3);
    if (paramLong == null)
    {
      AssertionUtil.AlwaysFatal.isNotNull(paramInteger, new String[0]);
      localBuilder.withValueBackReference("message_id", paramInteger.intValue());
    }
    else
    {
      localBuilder.withValue("message_id", paramLong);
    }
    paramList.add(localBuilder.build());
  }
  
  @SuppressLint({"InlinedApi"})
  public final long a(com.truecaller.messaging.transport.f paramf, i parami, r paramr, b paramb1, b paramb2, int paramInt, List<ContentProviderOperation> paramList, q paramq, boolean paramBoolean1, boolean paramBoolean2, Set<Long> paramSet)
  {
    j = new StringBuilder("ct != 'application/smil' AND mid IN (");
    h = new android.support.v4.f.f();
    i = new android.support.v4.f.f();
    long l = super.a(paramf, parami, paramr, paramb1, paramb2, paramInt, paramList, paramq, paramBoolean1, paramBoolean2, paramSet);
    if (!l.b.b(l))
    {
      j.append(")");
      a(j.toString(), h, i, paramList);
    }
    h = null;
    i = null;
    j = null;
    return l;
  }
  
  public final Set<Participant> a(long paramLong, com.truecaller.messaging.transport.f paramf, i parami, Participant paramParticipant, boolean paramBoolean)
  {
    HashSet localHashSet = new HashSet();
    localHashSet.add(paramParticipant);
    paramf = paramf.a(paramLong).iterator();
    while (paramf.hasNext()) {
      localHashSet.add(parami.a((String)paramf.next()));
    }
    return localHashSet;
  }
  
  public final boolean a(int paramInt)
  {
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.an
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */