package com.truecaller.messaging.transport.mms;

import android.content.ContentResolver;
import android.content.Context;
import com.truecaller.common.h.u;
import dagger.a.d;
import javax.inject.Provider;

public final class w
  implements d<h>
{
  private final n a;
  private final Provider<Context> b;
  private final Provider<ContentResolver> c;
  private final Provider<u> d;
  
  private w(n paramn, Provider<Context> paramProvider, Provider<ContentResolver> paramProvider1, Provider<u> paramProvider2)
  {
    a = paramn;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
  }
  
  public static w a(n paramn, Provider<Context> paramProvider, Provider<ContentResolver> paramProvider1, Provider<u> paramProvider2)
  {
    return new w(paramn, paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.w
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */