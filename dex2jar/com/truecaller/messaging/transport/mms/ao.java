package com.truecaller.messaging.transport.mms;

import android.annotation.SuppressLint;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.Telephony.Mms;
import android.provider.Telephony.Mms.Inbox;
import android.provider.Telephony.Threads;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.SparseArray;
import com.android.a.a.a.n;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.e.a;
import com.truecaller.common.h.am;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.transport.ad;
import com.truecaller.messaging.transport.ad.b;
import com.truecaller.messaging.transport.i;
import com.truecaller.messaging.transport.l.a;
import com.truecaller.multisim.SimInfo;
import com.truecaller.tracking.events.u;
import com.truecaller.tracking.events.u.a;
import com.truecaller.utils.extensions.m;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.a.a.a.g;
import org.a.a.e;
import org.c.a.a.a.k;

final class ao
  implements com.truecaller.messaging.transport.l<ap>
{
  static final int[] a;
  public static final Uri b;
  public static final String c;
  private static final long d = TimeUnit.DAYS.toSeconds(7L);
  private static final String[] e;
  private static final String[] f;
  private static final String g;
  private final Context h;
  private final com.truecaller.messaging.h i;
  private final com.truecaller.androidactors.f<t> j;
  private final com.truecaller.multisim.h k;
  private final ak l;
  private final com.truecaller.utils.d m;
  private final an n;
  private final h o;
  private final TelephonyManager p;
  private final Set<Long> q = new HashSet();
  private final com.truecaller.androidactors.f<com.truecaller.messaging.notifications.a> r;
  private final com.truecaller.androidactors.f<ae> s;
  private final ad.b t;
  private final com.truecaller.analytics.b u;
  private final com.truecaller.utils.l v;
  private final com.truecaller.messaging.a w;
  private ao.a x = null;
  private boolean y = false;
  
  static
  {
    a = new int[] { 129, 130, 151 };
    e = new String[] { "date", "m_cls", "pri", "d_rpt", "rr" };
    f = new String[] { "charset", "address" };
    StringBuilder localStringBuilder = new StringBuilder("type IN (");
    Object localObject = a;
    if (localObject == null) {
      localObject = null;
    } else {
      localObject = k.a((int[])localObject, localObject.length);
    }
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    g = localStringBuilder.toString();
    localObject = Telephony.Mms.CONTENT_URI;
    b = (Uri)localObject;
    c = ((Uri)localObject).getAuthority();
  }
  
  ao(Context paramContext, com.truecaller.androidactors.f<t> paramf, com.truecaller.messaging.h paramh, com.truecaller.utils.d paramd, com.truecaller.multisim.h paramh1, ak paramak, TelephonyManager paramTelephonyManager, an paraman, com.truecaller.androidactors.f<com.truecaller.messaging.notifications.a> paramf1, h paramh2, com.truecaller.androidactors.f<ae> paramf2, ad.b paramb, com.truecaller.analytics.b paramb1, com.truecaller.utils.l paraml, com.truecaller.messaging.a parama)
  {
    h = paramContext;
    i = paramh;
    j = paramf;
    m = paramd;
    n = paraman;
    k = paramh1;
    l = paramak;
    p = paramTelephonyManager;
    r = paramf1;
    s = paramf2;
    o = paramh2;
    t = paramb;
    u = paramb1;
    v = paraml;
    w = parama;
  }
  
  @SuppressLint({"NewApi"})
  private long a(Message paramMessage, MmsTransportInfo paramMmsTransportInfo)
  {
    Object localObject = E;
    int i1 = 0;
    AssertionUtil.AlwaysFatal.isNotNull(localObject, new String[0]);
    boolean bool;
    if (((SparseArray)localObject).size() > 0) {
      bool = true;
    } else {
      bool = false;
    }
    AssertionUtil.AlwaysFatal.isTrue(bool, new String[0]);
    paramMmsTransportInfo = new HashSet();
    int[] arrayOfInt = a;
    int i2 = arrayOfInt.length;
    while (i1 < i2)
    {
      Set localSet = (Set)((SparseArray)localObject).get(arrayOfInt[i1]);
      if (localSet != null) {
        paramMmsTransportInfo.addAll(localSet);
      }
      i1 += 1;
    }
    if ((f & 0x1) == 0)
    {
      if (paramMmsTransportInfo.size() == 1) {
        paramMmsTransportInfo.clear();
      }
      paramMmsTransportInfo.add(c.f);
    }
    else if (paramMmsTransportInfo.size() > 1)
    {
      paramMessage = (Set)((SparseArray)localObject).get(137);
      if (paramMessage != null) {
        paramMmsTransportInfo.addAll(paramMessage);
      }
    }
    try
    {
      long l1 = Telephony.Threads.getOrCreateThreadId(h, paramMmsTransportInfo);
      return l1;
    }
    catch (IllegalArgumentException paramMessage)
    {
      for (;;) {}
    }
    paramMessage = new StringBuilder("For some reasons we can not create thread for addresses: [");
    paramMmsTransportInfo = paramMmsTransportInfo.iterator();
    while (paramMmsTransportInfo.hasNext())
    {
      localObject = (String)paramMmsTransportInfo.next();
      paramMessage.append("{isEmpty:");
      paramMessage.append(am.a("insert-address-token", (CharSequence)localObject));
      paramMessage.append(", length:");
      if (localObject == null) {
        i1 = -1;
      } else {
        i1 = ((String)localObject).length();
      }
      paramMessage.append(i1);
      paramMessage.append("}, ");
    }
    paramMessage.append("]");
    AssertionUtil.reportWeirdnessButNeverCrash(paramMessage.toString());
    return -1L;
  }
  
  private com.android.a.a.a.f a(Uri paramUri, boolean paramBoolean)
    throws IOException
  {
    Object localObject = null;
    try
    {
      InputStream localInputStream = h.getContentResolver().openInputStream(paramUri);
      if (localInputStream != null)
      {
        localObject = localInputStream;
        paramUri = new n(m.a(localInputStream), paramBoolean).a();
        com.truecaller.utils.extensions.d.a(localInputStream);
        return paramUri;
      }
      localObject = localInputStream;
      throw new IOException("Can't open stream with PDU content from ".concat(String.valueOf(paramUri)));
    }
    finally
    {
      com.truecaller.utils.extensions.d.a((Closeable)localObject);
    }
  }
  
  /* Error */
  @SuppressLint({"InlinedApi"})
  private Entity a(long paramLong, PduEntity paramPduEntity)
  {
    // Byte code:
    //   0: new 344	android/content/ContentValues
    //   3: dup
    //   4: invokespecial 345	android/content/ContentValues:<init>	()V
    //   7: astore 8
    //   9: aload_0
    //   10: getfield 171	com/truecaller/messaging/transport/mms/ao:o	Lcom/truecaller/messaging/transport/mms/h;
    //   13: aload_3
    //   14: aload 8
    //   16: invokeinterface 350 3 0
    //   21: aload_0
    //   22: getfield 151	com/truecaller/messaging/transport/mms/ao:h	Landroid/content/Context;
    //   25: invokevirtual 302	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   28: astore 4
    //   30: new 99	java/lang/StringBuilder
    //   33: dup
    //   34: ldc_w 352
    //   37: invokespecial 105	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   40: astore 5
    //   42: aload 5
    //   44: lload_1
    //   45: invokevirtual 355	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   48: pop
    //   49: aload 5
    //   51: ldc_w 357
    //   54: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   57: pop
    //   58: aload 5
    //   60: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   63: invokestatic 361	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   66: astore 5
    //   68: new 99	java/lang/StringBuilder
    //   71: dup
    //   72: ldc_w 363
    //   75: invokespecial 105	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   78: astore 6
    //   80: aload 6
    //   82: aload 5
    //   84: invokevirtual 364	android/net/Uri:toString	()Ljava/lang/String;
    //   87: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   90: pop
    //   91: aload 6
    //   93: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   96: pop
    //   97: aconst_null
    //   98: astore 6
    //   100: aload 4
    //   102: aload 5
    //   104: aload 8
    //   106: invokevirtual 368	android/content/ContentResolver:insert	(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    //   109: astore 9
    //   111: aload 9
    //   113: ifnonnull +48 -> 161
    //   116: ldc_w 370
    //   119: aload_3
    //   120: getfield 373	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   123: invokevirtual 376	android/net/Uri:getScheme	()Ljava/lang/String;
    //   126: invokevirtual 379	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   129: ifeq +30 -> 159
    //   132: new 381	java/io/File
    //   135: dup
    //   136: aload_3
    //   137: getfield 373	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   140: invokevirtual 384	android/net/Uri:getPath	()Ljava/lang/String;
    //   143: invokespecial 385	java/io/File:<init>	(Ljava/lang/String;)V
    //   146: astore_3
    //   147: aload_3
    //   148: invokevirtual 388	java/io/File:exists	()Z
    //   151: ifeq +8 -> 159
    //   154: aload_3
    //   155: invokevirtual 391	java/io/File:delete	()Z
    //   158: pop
    //   159: aconst_null
    //   160: areturn
    //   161: new 99	java/lang/StringBuilder
    //   164: dup
    //   165: ldc_w 393
    //   168: invokespecial 105	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   171: astore 7
    //   173: aload 7
    //   175: aload 5
    //   177: invokevirtual 364	android/net/Uri:toString	()Ljava/lang/String;
    //   180: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   183: pop
    //   184: aload 7
    //   186: ldc_w 395
    //   189: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   192: pop
    //   193: aload 7
    //   195: aload_3
    //   196: getfield 397	com/truecaller/messaging/transport/mms/PduEntity:j	Ljava/lang/String;
    //   199: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   202: pop
    //   203: aload 7
    //   205: ldc_w 399
    //   208: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   211: pop
    //   212: aload 7
    //   214: aload 9
    //   216: invokevirtual 364	android/net/Uri:toString	()Ljava/lang/String;
    //   219: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   222: pop
    //   223: aload 7
    //   225: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   228: pop
    //   229: aload 8
    //   231: ldc_w 401
    //   234: invokevirtual 405	android/content/ContentValues:containsKey	(Ljava/lang/String;)Z
    //   237: ifeq +70 -> 307
    //   240: aload_3
    //   241: getfield 397	com/truecaller/messaging/transport/mms/PduEntity:j	Ljava/lang/String;
    //   244: aload 8
    //   246: ldc_w 401
    //   249: invokevirtual 408	android/content/ContentValues:getAsString	(Ljava/lang/String;)Ljava/lang/String;
    //   252: aload_3
    //   253: getfield 409	com/truecaller/messaging/transport/mms/PduEntity:d	J
    //   256: invokestatic 414	com/truecaller/messaging/data/types/Entity:a	(Ljava/lang/String;Ljava/lang/String;J)Lcom/truecaller/messaging/data/types/Entity;
    //   259: astore 4
    //   261: ldc_w 370
    //   264: aload_3
    //   265: getfield 373	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   268: invokevirtual 376	android/net/Uri:getScheme	()Ljava/lang/String;
    //   271: invokevirtual 379	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   274: ifeq +30 -> 304
    //   277: new 381	java/io/File
    //   280: dup
    //   281: aload_3
    //   282: getfield 373	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   285: invokevirtual 384	android/net/Uri:getPath	()Ljava/lang/String;
    //   288: invokespecial 385	java/io/File:<init>	(Ljava/lang/String;)V
    //   291: astore_3
    //   292: aload_3
    //   293: invokevirtual 388	java/io/File:exists	()Z
    //   296: ifeq +8 -> 304
    //   299: aload_3
    //   300: invokevirtual 391	java/io/File:delete	()Z
    //   303: pop
    //   304: aload 4
    //   306: areturn
    //   307: aload 4
    //   309: aload_3
    //   310: getfield 373	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   313: invokevirtual 308	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   316: astore 5
    //   318: aload 5
    //   320: ifnonnull +108 -> 428
    //   323: new 99	java/lang/StringBuilder
    //   326: dup
    //   327: ldc_w 416
    //   330: invokespecial 105	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   333: astore 4
    //   335: aload 4
    //   337: aload_3
    //   338: getfield 373	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   341: invokevirtual 419	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   344: pop
    //   345: aload 4
    //   347: ldc_w 421
    //   350: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   353: pop
    //   354: aload 4
    //   356: aload 8
    //   358: ldc_w 423
    //   361: invokevirtual 426	android/content/ContentValues:get	(Ljava/lang/String;)Ljava/lang/Object;
    //   364: invokevirtual 419	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   367: pop
    //   368: aload 4
    //   370: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   373: pop
    //   374: aload 5
    //   376: invokestatic 429	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   379: aconst_null
    //   380: invokestatic 429	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   383: ldc_w 370
    //   386: aload_3
    //   387: getfield 373	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   390: invokevirtual 376	android/net/Uri:getScheme	()Ljava/lang/String;
    //   393: invokevirtual 379	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   396: ifeq +30 -> 426
    //   399: new 381	java/io/File
    //   402: dup
    //   403: aload_3
    //   404: getfield 373	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   407: invokevirtual 384	android/net/Uri:getPath	()Ljava/lang/String;
    //   410: invokespecial 385	java/io/File:<init>	(Ljava/lang/String;)V
    //   413: astore_3
    //   414: aload_3
    //   415: invokevirtual 388	java/io/File:exists	()Z
    //   418: ifeq +8 -> 426
    //   421: aload_3
    //   422: invokevirtual 391	java/io/File:delete	()Z
    //   425: pop
    //   426: aconst_null
    //   427: areturn
    //   428: aload 4
    //   430: aload 9
    //   432: invokevirtual 433	android/content/ContentResolver:openOutputStream	(Landroid/net/Uri;)Ljava/io/OutputStream;
    //   435: astore 4
    //   437: aload 4
    //   439: ifnonnull +147 -> 586
    //   442: aload 4
    //   444: astore 7
    //   446: aload 5
    //   448: astore 6
    //   450: new 99	java/lang/StringBuilder
    //   453: dup
    //   454: ldc_w 435
    //   457: invokespecial 105	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   460: astore 10
    //   462: aload 4
    //   464: astore 7
    //   466: aload 5
    //   468: astore 6
    //   470: aload 10
    //   472: aload 9
    //   474: invokevirtual 419	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   477: pop
    //   478: aload 4
    //   480: astore 7
    //   482: aload 5
    //   484: astore 6
    //   486: aload 10
    //   488: ldc_w 421
    //   491: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   494: pop
    //   495: aload 4
    //   497: astore 7
    //   499: aload 5
    //   501: astore 6
    //   503: aload 10
    //   505: aload 8
    //   507: ldc_w 423
    //   510: invokevirtual 426	android/content/ContentValues:get	(Ljava/lang/String;)Ljava/lang/Object;
    //   513: invokevirtual 419	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   516: pop
    //   517: aload 4
    //   519: astore 7
    //   521: aload 5
    //   523: astore 6
    //   525: aload 10
    //   527: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   530: pop
    //   531: aload 5
    //   533: invokestatic 429	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   536: aload 4
    //   538: invokestatic 429	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   541: ldc_w 370
    //   544: aload_3
    //   545: getfield 373	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   548: invokevirtual 376	android/net/Uri:getScheme	()Ljava/lang/String;
    //   551: invokevirtual 379	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   554: ifeq +30 -> 584
    //   557: new 381	java/io/File
    //   560: dup
    //   561: aload_3
    //   562: getfield 373	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   565: invokevirtual 384	android/net/Uri:getPath	()Ljava/lang/String;
    //   568: invokespecial 385	java/io/File:<init>	(Ljava/lang/String;)V
    //   571: astore_3
    //   572: aload_3
    //   573: invokevirtual 388	java/io/File:exists	()Z
    //   576: ifeq +8 -> 584
    //   579: aload_3
    //   580: invokevirtual 391	java/io/File:delete	()Z
    //   583: pop
    //   584: aconst_null
    //   585: areturn
    //   586: aload 4
    //   588: astore 7
    //   590: aload 5
    //   592: astore 6
    //   594: aload 5
    //   596: aload 4
    //   598: invokestatic 438	com/truecaller/utils/extensions/m:a	(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    //   601: pop2
    //   602: aload 4
    //   604: astore 7
    //   606: aload 5
    //   608: astore 6
    //   610: aload 4
    //   612: invokevirtual 443	java/io/OutputStream:flush	()V
    //   615: aload 5
    //   617: invokestatic 429	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   620: aload 4
    //   622: invokestatic 429	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   625: aload_3
    //   626: getfield 397	com/truecaller/messaging/transport/mms/PduEntity:j	Ljava/lang/String;
    //   629: aload 9
    //   631: iconst_m1
    //   632: iconst_m1
    //   633: iconst_0
    //   634: aload_3
    //   635: getfield 409	com/truecaller/messaging/transport/mms/PduEntity:d	J
    //   638: invokestatic 446	com/truecaller/messaging/data/types/Entity:a	(Ljava/lang/String;Landroid/net/Uri;IIZJ)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   641: astore 4
    //   643: ldc_w 370
    //   646: aload_3
    //   647: getfield 373	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   650: invokevirtual 376	android/net/Uri:getScheme	()Ljava/lang/String;
    //   653: invokevirtual 379	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   656: ifeq +30 -> 686
    //   659: new 381	java/io/File
    //   662: dup
    //   663: aload_3
    //   664: getfield 373	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   667: invokevirtual 384	android/net/Uri:getPath	()Ljava/lang/String;
    //   670: invokespecial 385	java/io/File:<init>	(Ljava/lang/String;)V
    //   673: astore_3
    //   674: aload_3
    //   675: invokevirtual 388	java/io/File:exists	()Z
    //   678: ifeq +8 -> 686
    //   681: aload_3
    //   682: invokevirtual 391	java/io/File:delete	()Z
    //   685: pop
    //   686: aload 4
    //   688: areturn
    //   689: astore 8
    //   691: goto +33 -> 724
    //   694: astore 4
    //   696: goto +106 -> 802
    //   699: astore 8
    //   701: aconst_null
    //   702: astore 4
    //   704: goto +20 -> 724
    //   707: astore 4
    //   709: aconst_null
    //   710: astore 5
    //   712: goto +90 -> 802
    //   715: astore 8
    //   717: aconst_null
    //   718: astore 5
    //   720: aload 5
    //   722: astore 4
    //   724: aload 4
    //   726: astore 7
    //   728: aload 5
    //   730: astore 6
    //   732: aload 8
    //   734: invokestatic 450	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   737: aload 5
    //   739: invokestatic 429	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   742: aload 4
    //   744: invokestatic 429	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   747: ldc_w 370
    //   750: aload_3
    //   751: getfield 373	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   754: invokevirtual 376	android/net/Uri:getScheme	()Ljava/lang/String;
    //   757: invokevirtual 379	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   760: ifeq +30 -> 790
    //   763: new 381	java/io/File
    //   766: dup
    //   767: aload_3
    //   768: getfield 373	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   771: invokevirtual 384	android/net/Uri:getPath	()Ljava/lang/String;
    //   774: invokespecial 385	java/io/File:<init>	(Ljava/lang/String;)V
    //   777: astore_3
    //   778: aload_3
    //   779: invokevirtual 388	java/io/File:exists	()Z
    //   782: ifeq +8 -> 790
    //   785: aload_3
    //   786: invokevirtual 391	java/io/File:delete	()Z
    //   789: pop
    //   790: aconst_null
    //   791: areturn
    //   792: astore 4
    //   794: aload 6
    //   796: astore 5
    //   798: aload 7
    //   800: astore 6
    //   802: aload 5
    //   804: invokestatic 429	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   807: aload 6
    //   809: invokestatic 429	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   812: aload 4
    //   814: athrow
    //   815: astore 4
    //   817: aload 4
    //   819: invokestatic 450	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   822: ldc_w 370
    //   825: aload_3
    //   826: getfield 373	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   829: invokevirtual 376	android/net/Uri:getScheme	()Ljava/lang/String;
    //   832: invokevirtual 379	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   835: ifeq +30 -> 865
    //   838: new 381	java/io/File
    //   841: dup
    //   842: aload_3
    //   843: getfield 373	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   846: invokevirtual 384	android/net/Uri:getPath	()Ljava/lang/String;
    //   849: invokespecial 385	java/io/File:<init>	(Ljava/lang/String;)V
    //   852: astore_3
    //   853: aload_3
    //   854: invokevirtual 388	java/io/File:exists	()Z
    //   857: ifeq +8 -> 865
    //   860: aload_3
    //   861: invokevirtual 391	java/io/File:delete	()Z
    //   864: pop
    //   865: aconst_null
    //   866: areturn
    //   867: astore 4
    //   869: ldc_w 370
    //   872: aload_3
    //   873: getfield 373	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   876: invokevirtual 376	android/net/Uri:getScheme	()Ljava/lang/String;
    //   879: invokevirtual 379	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   882: ifeq +30 -> 912
    //   885: new 381	java/io/File
    //   888: dup
    //   889: aload_3
    //   890: getfield 373	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   893: invokevirtual 384	android/net/Uri:getPath	()Ljava/lang/String;
    //   896: invokespecial 385	java/io/File:<init>	(Ljava/lang/String;)V
    //   899: astore_3
    //   900: aload_3
    //   901: invokevirtual 388	java/io/File:exists	()Z
    //   904: ifeq +8 -> 912
    //   907: aload_3
    //   908: invokevirtual 391	java/io/File:delete	()Z
    //   911: pop
    //   912: aload 4
    //   914: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	915	0	this	ao
    //   0	915	1	paramLong	long
    //   0	915	3	paramPduEntity	PduEntity
    //   28	659	4	localObject1	Object
    //   694	1	4	localObject2	Object
    //   702	1	4	localObject3	Object
    //   707	1	4	localObject4	Object
    //   722	21	4	localObject5	Object
    //   792	21	4	localObject6	Object
    //   815	3	4	localRuntimeException	RuntimeException
    //   867	46	4	localObject7	Object
    //   40	763	5	localObject8	Object
    //   78	730	6	localObject9	Object
    //   171	628	7	localObject10	Object
    //   7	499	8	localContentValues	ContentValues
    //   689	1	8	localIOException1	IOException
    //   699	1	8	localIOException2	IOException
    //   715	18	8	localIOException3	IOException
    //   109	521	9	localUri	Uri
    //   460	66	10	localStringBuilder	StringBuilder
    // Exception table:
    //   from	to	target	type
    //   450	462	689	java/io/IOException
    //   470	478	689	java/io/IOException
    //   486	495	689	java/io/IOException
    //   503	517	689	java/io/IOException
    //   525	531	689	java/io/IOException
    //   594	602	689	java/io/IOException
    //   610	615	689	java/io/IOException
    //   323	374	694	finally
    //   428	437	694	finally
    //   323	374	699	java/io/IOException
    //   428	437	699	java/io/IOException
    //   307	318	707	finally
    //   307	318	715	java/io/IOException
    //   450	462	792	finally
    //   470	478	792	finally
    //   486	495	792	finally
    //   503	517	792	finally
    //   525	531	792	finally
    //   594	602	792	finally
    //   610	615	792	finally
    //   732	737	792	finally
    //   100	111	815	java/lang/RuntimeException
    //   0	97	867	finally
    //   100	111	867	finally
    //   161	261	867	finally
    //   374	383	867	finally
    //   531	541	867	finally
    //   615	643	867	finally
    //   737	747	867	finally
    //   802	815	867	finally
    //   817	822	867	finally
  }
  
  private Message a(Intent paramIntent)
  {
    byte[] arrayOfByte = paramIntent.getByteArrayExtra("data");
    if (arrayOfByte == null)
    {
      AssertionUtil.reportWeirdnessButNeverCrash("Got new MMS intent without PDU");
      return null;
    }
    Object localObject = k.a(paramIntent);
    paramIntent = (Intent)localObject;
    if ("-1".equals(localObject)) {
      paramIntent = k.f();
    }
    localObject = new n(arrayOfByte, k.c(paramIntent).c()).a();
    if (localObject == null) {
      return null;
    }
    return o.a((com.android.a.a.a.f)localObject, c(paramIntent), paramIntent, -1L);
  }
  
  @SuppressLint({"InlinedApi"})
  private Message a(Message paramMessage, MmsTransportInfo paramMmsTransportInfo, boolean paramBoolean)
  {
    Object localObject;
    switch (g)
    {
    case 131: 
    default: 
      return null;
    case 132: 
      paramMmsTransportInfo = c(paramMessage, paramMmsTransportInfo);
      if (paramMmsTransportInfo == null) {
        return null;
      }
      localObject = (MmsTransportInfo)m;
      if (e == null) {
        return null;
      }
      paramMessage = b(e);
      if (paramMessage != null) {
        b(paramMessage);
      }
      paramMessage = paramMmsTransportInfo;
      if (paramBoolean)
      {
        paramMessage = paramMmsTransportInfo;
        if (l != null)
        {
          paramMessage = l.a(l, k);
          if (D)
          {
            ((ai)paramMessage.a()).a(o.getBytes(), l, 129);
            return paramMmsTransportInfo;
          }
          ((ai)paramMessage.a()).a(o.getBytes(), l);
          return paramMmsTransportInfo;
        }
      }
      break;
    case 130: 
      paramMmsTransportInfo = b(paramMessage, paramMmsTransportInfo);
      paramMessage = paramMmsTransportInfo;
      if (paramMmsTransportInfo != null)
      {
        localObject = (MmsTransportInfo)m;
        b(e);
        if (l == null)
        {
          AssertionUtil.reportWeirdnessButNeverCrash("Received Notification pdu without location uri.Have no idea what to do with it.");
          return paramMmsTransportInfo;
        }
        boolean bool = paramBoolean;
        if (c(l))
        {
          bool = paramBoolean;
          if (e != null)
          {
            if (!a(paramMmsTransportInfo, false))
            {
              paramMessage = new ContentValues();
              paramMessage.put("retr_st", Integer.valueOf(194));
              paramMessage.put("seen", Integer.valueOf(0));
              h.getContentResolver().update(e, paramMessage, null, null);
              ((com.truecaller.messaging.notifications.a)r.a()).d();
            }
            bool = false;
          }
        }
        paramMessage = paramMmsTransportInfo;
        if (bool)
        {
          ((ai)l.a(l, k).a()).a(o.getBytes(), l, 131);
          return paramMmsTransportInfo;
        }
      }
      break;
    case 129: 
      paramMmsTransportInfo = e(paramMessage, paramMmsTransportInfo);
      if (paramMmsTransportInfo == null) {
        return null;
      }
      paramMessage = (MmsTransportInfo)m;
      if (e == null) {
        return null;
      }
      localObject = b(e);
      paramMessage = paramMmsTransportInfo;
      if (localObject != null)
      {
        b((org.a.a.b)localObject);
        return paramMmsTransportInfo;
      }
      break;
    case 128: 
      paramMessage = d(paramMessage, paramMmsTransportInfo);
      if (paramMessage != null) {
        b(e);
      }
      break;
    }
    return paramMessage;
  }
  
  private List<String> a(Uri paramUri)
  {
    long l1 = ContentUris.parseId(paramUri);
    paramUri = null;
    if (l1 == -1L) {
      return null;
    }
    ArrayList localArrayList = new ArrayList();
    Object localObject1 = new StringBuilder("content://mms/");
    ((StringBuilder)localObject1).append(l1);
    ((StringBuilder)localObject1).append("/addr");
    localObject1 = Uri.parse(((StringBuilder)localObject1).toString());
    try
    {
      Cursor localCursor = h.getContentResolver().query((Uri)localObject1, f, g, null, null);
      if (localCursor != null) {
        for (;;)
        {
          paramUri = localCursor;
          if (!localCursor.moveToNext()) {
            break;
          }
          paramUri = localCursor;
          int i1 = localCursor.getInt(0);
          paramUri = localCursor;
          String str = localCursor.getString(1);
          localObject1 = str;
          if (str != null)
          {
            paramUri = localCursor;
            localObject1 = com.android.a.a.c.a(str.getBytes(), i1);
          }
          paramUri = localCursor;
          localArrayList.add(localObject1);
        }
      }
      return localArrayList;
    }
    finally
    {
      com.truecaller.util.q.a(paramUri);
    }
  }
  
  @SuppressLint({"InlinedApi"})
  private void a(Uri paramUri, SparseArray<Set<String>> paramSparseArray)
  {
    long l1 = ContentUris.parseId(paramUri);
    if (l1 != -1L)
    {
      if (paramSparseArray.size() == 0) {
        return;
      }
      paramUri = new StringBuilder("content://mms/");
      paramUri.append(l1);
      paramUri.append("/addr");
      paramUri = Uri.parse(paramUri.toString());
      ContentResolver localContentResolver = h.getContentResolver();
      localContentResolver.delete(paramUri, null, null);
      ContentValues localContentValues = new ContentValues();
      int i1 = 0;
      while (i1 < paramSparseArray.size())
      {
        localContentValues.put("type", Integer.valueOf(paramSparseArray.keyAt(i1)));
        Iterator localIterator = ((Set)paramSparseArray.valueAt(i1)).iterator();
        while (localIterator.hasNext())
        {
          localContentValues.put("address", (String)localIterator.next());
          localContentValues.put("charset", Integer.valueOf(106));
          localContentResolver.insert(paramUri, localContentValues);
        }
        i1 += 1;
      }
      return;
    }
  }
  
  private boolean a(Message paramMessage, boolean paramBoolean)
  {
    MmsTransportInfo localMmsTransportInfo = (MmsTransportInfo)m;
    AssertionUtil.AlwaysFatal.isNotNull(localMmsTransportInfo, new String[0]);
    boolean bool;
    if (a != -1L) {
      bool = true;
    } else {
      bool = false;
    }
    AssertionUtil.AlwaysFatal.isTrue(bool, new String[0]);
    AssertionUtil.AlwaysFatal.isFalse(o.isEmpty(), new String[0]);
    if (l == null)
    {
      AssertionUtil.reportWeirdnessButNeverCrash("Should never try to download MMS content without content uri");
      return false;
    }
    if (k.g(l) == 2) {
      return false;
    }
    synchronized (q)
    {
      if (q.contains(Long.valueOf(b))) {
        return true;
      }
      q.add(Long.valueOf(b));
      ((ai)l.a(l, k).a()).a(b, o.getBytes(), l, paramBoolean ^ true);
      return true;
    }
  }
  
  @SuppressLint({"NewApi"})
  private boolean a(MmsTransportInfo paramMmsTransportInfo)
  {
    if (g == 130)
    {
      if (l == null) {
        return false;
      }
      ContentResolver localContentResolver = h.getContentResolver();
      Object localObject2 = null;
      Object localObject1 = localObject2;
      try
      {
        org.a.a.b localb1 = org.a.a.b.ay_();
        localObject1 = localObject2;
        org.a.a.b localb2 = localb1.c(7);
        localObject1 = localObject2;
        org.a.a.b localb3 = localb1.a(7);
        localObject1 = localObject2;
        Uri localUri = b;
        localObject1 = localObject2;
        long l1 = a / 1000L;
        localObject1 = localObject2;
        long l2 = a / 1000L;
        localObject1 = localObject2;
        long l3 = a / 1000L;
        localObject1 = localObject2;
        paramMmsTransportInfo = l.toString();
        localObject1 = localObject2;
        paramMmsTransportInfo = localContentResolver.query(localUri, new String[] { "_id" }, "((m_type<>130) OR (exp>?)) AND (date>?) AND (date<?) AND (ct_l=?)", new String[] { String.valueOf(l1), String.valueOf(l2), String.valueOf(l3), paramMmsTransportInfo }, null, null);
        if (paramMmsTransportInfo != null)
        {
          localObject1 = paramMmsTransportInfo;
          boolean bool = paramMmsTransportInfo.moveToFirst();
          if (paramMmsTransportInfo != null) {
            paramMmsTransportInfo.close();
          }
          return bool;
        }
        if (paramMmsTransportInfo != null) {
          paramMmsTransportInfo.close();
        }
        return false;
      }
      finally
      {
        if (localObject1 != null) {
          ((Cursor)localObject1).close();
        }
      }
    }
    return false;
  }
  
  private boolean a(ap paramap)
  {
    if (!d) {
      return false;
    }
    try
    {
      int i1 = t.a(paramap).length;
      return i1 != 0;
    }
    catch (OperationApplicationException paramap) {}catch (SecurityException paramap) {}catch (RemoteException paramap) {}
    AssertionUtil.reportThrowableButNeverCrash(paramap);
    return false;
  }
  
  @SuppressLint({"InlinedApi"})
  private Message b(Message paramMessage, MmsTransportInfo paramMmsTransportInfo)
  {
    Object localObject = new ContentValues();
    long l1 = a(paramMessage, paramMmsTransportInfo);
    if (l1 == -1L)
    {
      paramMmsTransportInfo = new StringBuilder("For some reasons we can not create thread for address ");
      paramMmsTransportInfo.append(c.f);
      AssertionUtil.OnlyInDebug.fail(new String[] { paramMmsTransportInfo.toString() });
      return null;
    }
    ((ContentValues)localObject).put("read", Integer.valueOf(0));
    ((ContentValues)localObject).put("locked", Integer.valueOf(0));
    ((ContentValues)localObject).put("seen", Boolean.valueOf(g));
    ((ContentValues)localObject).put("thread_id", Long.valueOf(l1));
    ((ContentValues)localObject).put("date", Long.valueOf(e.a / 1000L));
    ((ContentValues)localObject).put("date_sent", Long.valueOf(d.a / 1000L));
    ((ContentValues)localObject).put("m_id", v);
    ((ContentValues)localObject).put("sub", h);
    ((ContentValues)localObject).put("sub_cs", Integer.valueOf(i));
    ((ContentValues)localObject).put("ct_t", m);
    if (l != null) {
      ((ContentValues)localObject).put("ct_l", l.toString());
    }
    long l2 = p.a;
    if (l2 > 0L) {
      ((ContentValues)localObject).put("exp", Long.valueOf(l2 / 1000L));
    }
    ((ContentValues)localObject).put("m_cls", u);
    ((ContentValues)localObject).put("m_type", Integer.valueOf(g));
    ((ContentValues)localObject).put("v", Integer.valueOf(f));
    ((ContentValues)localObject).put("m_size", Integer.valueOf(x));
    ((ContentValues)localObject).put("pri", Integer.valueOf(q));
    ((ContentValues)localObject).put("rr", Integer.valueOf(A));
    if (C) {
      ((ContentValues)localObject).put("rpt_a", Integer.valueOf(1));
    }
    if (s > 0) {
      ((ContentValues)localObject).put("resp_st", Integer.valueOf(s));
    }
    if (c > 0) {
      ((ContentValues)localObject).put("st", Integer.valueOf(c));
    }
    ((ContentValues)localObject).put("tr_id", o);
    if (r > 0) {
      ((ContentValues)localObject).put("retr_st", Integer.valueOf(r));
    }
    if (!TextUtils.isEmpty(j))
    {
      ((ContentValues)localObject).put("retr_txt", j);
      ((ContentValues)localObject).put("retr_txt_cs", Integer.valueOf(k));
    }
    if (B > 0) {
      ((ContentValues)localObject).put("read_status", Integer.valueOf(B));
    }
    ((ContentValues)localObject).put("ct_cls", Integer.valueOf(n));
    ((ContentValues)localObject).put("resp_txt", t);
    if (z > 0L) {
      ((ContentValues)localObject).put("d_tm", Long.valueOf(z));
    }
    ((ContentValues)localObject).put("d_rpt", Integer.valueOf(y));
    ((ContentValues)localObject).put("text_only", Boolean.valueOf(paramMessage.d() ^ true));
    String str = k.c();
    if (str != null) {
      ((ContentValues)localObject).put(str, l);
    }
    try
    {
      localObject = h.getContentResolver().insert(Telephony.Mms.Inbox.CONTENT_URI, (ContentValues)localObject);
      if (localObject == null) {
        return null;
      }
      l2 = ContentUris.parseId((Uri)localObject);
      paramMmsTransportInfo = paramMmsTransportInfo.g();
      d = l1;
      b = l2;
      e = ((Uri)localObject);
      paramMmsTransportInfo = paramMmsTransportInfo.a();
      paramMessage = paramMessage.m().a(1, paramMmsTransportInfo).b();
      AssertionUtil.AlwaysFatal.isNotNull(E, new String[0]);
      a((Uri)localObject, E);
      return paramMessage;
    }
    catch (RuntimeException paramMessage)
    {
      AssertionUtil.reportThrowableButNeverCrash(paramMessage);
    }
    return null;
  }
  
  @SuppressLint({"InlinedApi"})
  private org.a.a.b b(Uri paramUri)
  {
    Object localObject3 = null;
    try
    {
      paramUri = h.getContentResolver().query(paramUri, new String[] { "date" }, null, null, null);
      if (paramUri != null) {
        try
        {
          if (paramUri.moveToFirst())
          {
            org.a.a.b localb = new org.a.a.b(paramUri.getLong(0) * 1000L);
            if (paramUri != null) {
              paramUri.close();
            }
            return localb;
          }
        }
        finally
        {
          break label89;
        }
      }
      if (paramUri != null) {
        paramUri.close();
      }
      return null;
    }
    finally
    {
      paramUri = (Uri)localObject3;
      label89:
      if (paramUri != null) {
        paramUri.close();
      }
    }
  }
  
  private void b(String paramString)
  {
    e.a locala = new e.a("MessageDownload").a("Type", "mms").a("Status", paramString);
    if (k.j()) {
      paramString = "Multi";
    } else {
      paramString = "Single";
    }
    paramString = locala.a("Sim", paramString).a();
    u.a(paramString);
  }
  
  private void b(org.a.a.b paramb)
  {
    long l1 = i.a(1);
    paramb = paramb.f();
    if (paramb.d(l1)) {
      i.a(1, a);
    }
  }
  
  private static boolean b(MmsTransportInfo paramMmsTransportInfo)
  {
    return (r >= 192) && (r < 255);
  }
  
  @SuppressLint({"InlinedApi"})
  private Message c(Message paramMessage, MmsTransportInfo paramMmsTransportInfo)
  {
    ContentResolver localContentResolver = h.getContentResolver();
    Object localObject = e;
    int i1 = 0;
    AssertionUtil.AlwaysFatal.isNotNull(localObject, new String[0]);
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("read", Integer.valueOf(0));
    localContentValues.put("seen", Integer.valueOf(0));
    localContentValues.put("retr_st", Integer.valueOf(r));
    localContentValues.put("retr_txt", j);
    Message.a locala = paramMessage.m();
    localObject = paramMmsTransportInfo;
    if (!b(paramMmsTransportInfo))
    {
      localContentValues.put("m_id", v);
      localContentValues.put("m_type", Integer.valueOf(g));
      localContentValues.put("tr_id", o);
      localContentValues.put("sub", h);
      localContentValues.put("sub_cs", Integer.valueOf(i));
      localContentValues.put("exp", Integer.valueOf(0));
      localContentValues.put("m_size", Integer.valueOf(x));
      localContentValues.put("pri", Integer.valueOf(q));
      localContentValues.putNull("ct_l");
      localContentValues.put("ct_t", m);
      localContentValues.put("m_cls", u);
      localContentValues.put("resp_st", Integer.valueOf(s));
      localContentValues.put("resp_txt", t);
      localContentValues.put("d_rpt", Integer.valueOf(y));
      localContentValues.put("d_tm", Long.valueOf(z));
      localObject = k.c();
      if (localObject != null) {
        localContentValues.put((String)localObject, l);
      }
      localObject = paramMmsTransportInfo;
      if (E != null)
      {
        localObject = paramMmsTransportInfo;
        if (E.size() > 0)
        {
          long l1 = a(paramMessage, paramMmsTransportInfo);
          localObject = paramMmsTransportInfo;
          if (l1 != -1L)
          {
            localContentValues.put("thread_id", Long.valueOf(l1));
            paramMmsTransportInfo = paramMmsTransportInfo.g();
            d = l1;
            localObject = paramMmsTransportInfo.a();
          }
        }
      }
    }
    try
    {
      if (localContentResolver.update(e, localContentValues, null, null) == 0) {
        return null;
      }
      locala.a(1, (TransportInfo)localObject);
      if ((e != null) && (E != null) && (E.size() > 0)) {
        a(e, E);
      }
      if (!paramMessage.c()) {
        return locala.b();
      }
      locala.a();
      paramMessage = n;
      int i2 = paramMessage.length;
      while (i1 < i2)
      {
        paramMmsTransportInfo = paramMessage[i1];
        AssertionUtil.AlwaysFatal.isTrue(paramMmsTransportInfo instanceof PduEntity, new String[] { "Only new received messages should be processed by this method" });
        paramMmsTransportInfo = a(b, (PduEntity)paramMmsTransportInfo);
        if (paramMmsTransportInfo != null) {
          locala.a(paramMmsTransportInfo);
        }
        i1 += 1;
      }
      return locala.b();
    }
    catch (RuntimeException paramMessage)
    {
      AssertionUtil.reportThrowableButNeverCrash(paramMessage);
    }
    return null;
  }
  
  @SuppressLint({"NewApi"})
  private boolean c(long paramLong)
  {
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("msg_box", Integer.valueOf(5));
    return h.getContentResolver().update(b, localContentValues, "_id=?", new String[] { String.valueOf(paramLong) }) > 0;
  }
  
  private boolean c(String paramString)
  {
    paramString = k.b(paramString);
    if (paramString == null) {
      return false;
    }
    if (!i.c(a)) {
      return false;
    }
    return (!p.isNetworkRoaming()) || (i.d(a));
  }
  
  @SuppressLint({"InlinedApi"})
  private Message d(Message paramMessage, MmsTransportInfo paramMmsTransportInfo)
  {
    Object localObject1 = new ContentValues();
    Object localObject2 = k.c(l);
    localObject2 = o.a(n, (com.truecaller.multisim.a)localObject2);
    if (localObject2 == null) {
      return null;
    }
    Object localObject3 = ((List)localObject2).iterator();
    PduEntity localPduEntity;
    for (int i1 = 0; ((Iterator)localObject3).hasNext(); i1 = (int)(i1 + d)) {
      localPduEntity = (PduEntity)((Iterator)localObject3).next();
    }
    long l1 = a(paramMessage, paramMmsTransportInfo);
    if (l1 == -1L)
    {
      paramMmsTransportInfo = new StringBuilder("For some reasons we can not create thread for address ");
      paramMmsTransportInfo.append(c.f);
      AssertionUtil.OnlyInDebug.fail(new String[] { paramMmsTransportInfo.toString() });
      return null;
    }
    ((ContentValues)localObject1).put("m_type", Integer.valueOf(g));
    ((ContentValues)localObject1).put("thread_id", Long.valueOf(l1));
    ((ContentValues)localObject1).put("ct_t", m);
    ((ContentValues)localObject1).put("sub", h);
    ((ContentValues)localObject1).put("sub_cs", Integer.valueOf(i));
    ((ContentValues)localObject1).put("pri", Integer.valueOf(q));
    ((ContentValues)localObject1).put("rr", Integer.valueOf(A));
    ((ContentValues)localObject1).put("d_rpt", Integer.valueOf(y));
    ((ContentValues)localObject1).put("m_cls", u);
    ((ContentValues)localObject1).put("exp", Long.valueOf(p.a / 1000L));
    ((ContentValues)localObject1).put("date", Long.valueOf(e.a / 1000L));
    ((ContentValues)localObject1).put("date_sent", Long.valueOf(d.a / 1000L));
    ((ContentValues)localObject1).put("m_size", Integer.valueOf(i1));
    ((ContentValues)localObject1).put("msg_box", Integer.valueOf(4));
    localObject3 = k.c();
    if (localObject3 != null) {
      ((ContentValues)localObject1).put((String)localObject3, l);
    }
    localObject3 = h.getContentResolver();
    for (;;)
    {
      try
      {
        if (e == null)
        {
          localObject1 = ((ContentResolver)localObject3).insert(b, (ContentValues)localObject1);
        }
        else
        {
          if (((ContentResolver)localObject3).update(e, (ContentValues)localObject1, null, null) <= 0) {
            break label607;
          }
          localObject1 = e;
        }
        long l2 = ContentUris.parseId((Uri)localObject1);
        if (l2 == -1L) {
          return null;
        }
        paramMmsTransportInfo = paramMmsTransportInfo.g();
        b = l2;
        d = l1;
        paramMmsTransportInfo = paramMmsTransportInfo.c(l2).a();
        localObject1 = paramMessage.m();
        ((Message.a)localObject1).a(1, paramMmsTransportInfo);
        AssertionUtil.AlwaysFatal.isNotNull(E, new String[0]);
        a(e, E);
        if (((List)localObject2).isEmpty()) {
          return paramMessage;
        }
        ((Message.a)localObject1).a();
        paramMessage = ((List)localObject2).iterator();
        if (paramMessage.hasNext())
        {
          localObject2 = (PduEntity)paramMessage.next();
          localObject2 = a(b, (PduEntity)localObject2);
          if (localObject2 == null) {
            continue;
          }
          ((Message.a)localObject1).a((Entity)localObject2);
          continue;
        }
        return ((Message.a)localObject1).b();
      }
      catch (RuntimeException paramMessage)
      {
        return null;
      }
      label607:
      localObject1 = null;
    }
  }
  
  private void d(String paramString)
  {
    paramString = new e.a("DeliverMmsError").a("Type", paramString).a();
    u.a(paramString);
  }
  
  @SuppressLint({"InlinedApi"})
  private Message e(Message paramMessage, MmsTransportInfo paramMmsTransportInfo)
  {
    AssertionUtil.AlwaysFatal.isNotNull(e, new String[0]);
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("m_id", v);
    localContentValues.put("tr_id", o);
    localContentValues.put("resp_st", Integer.valueOf(s));
    localContentValues.put("resp_txt", t);
    localContentValues.put("msg_box", Integer.valueOf(2));
    ContentResolver localContentResolver = h.getContentResolver();
    try
    {
      if (localContentResolver.update(e, localContentValues, null, null) <= 0) {
        return null;
      }
      paramMmsTransportInfo = paramMmsTransportInfo.g();
      v = 2;
      paramMmsTransportInfo = paramMmsTransportInfo.a();
      paramMessage = paramMessage.m().a(1, paramMmsTransportInfo).b();
      return paramMessage;
    }
    catch (RuntimeException paramMessage) {}
    return null;
  }
  
  private boolean g()
  {
    return m.a(w.b());
  }
  
  public final long a(long paramLong)
  {
    return paramLong / 1000L * 1000L;
  }
  
  public final long a(com.truecaller.messaging.transport.f paramf, i parami, r paramr, org.a.a.b paramb1, org.a.a.b paramb2, int paramInt, List<ContentProviderOperation> paramList, com.truecaller.utils.q paramq, boolean paramBoolean1, boolean paramBoolean2, Set<Long> paramSet)
  {
    if (!v.a(new String[] { "android.permission.READ_SMS" })) {
      return 0L;
    }
    return n.a(paramf, parami, paramr, paramb1, paramb2, paramInt, paramList, paramq, paramBoolean1, paramBoolean2, paramSet);
  }
  
  @SuppressLint({"NewApi"})
  public final l.a a(Message paramMessage, Participant[] paramArrayOfParticipant)
  {
    if (!g()) {
      return new l.a(0);
    }
    int i1;
    if (j == 3)
    {
      localObject = new MmsTransportInfo.a();
      a = a;
      localObject = ((MmsTransportInfo.a)localObject).a("No title", 106);
      w = 128;
      MmsTransportInfo.a locala = ((MmsTransportInfo.a)localObject).e(d);
      o = "personal";
      y = 129;
      A = 129;
      r = 129;
      l = "application/vnd.wap.multipart.related";
      if (E == null) {
        E = new SparseArray();
      }
      Set localSet = (Set)E.get(151);
      localObject = localSet;
      if (localSet == null)
      {
        localObject = new HashSet();
        E.put(151, localObject);
      }
      int i2 = paramArrayOfParticipant.length;
      i1 = 0;
      while (i1 < i2)
      {
        ((Set)localObject).add(f);
        i1 += 1;
      }
      paramMessage = a(paramMessage, locala.a(), false);
      if (paramMessage == null) {
        return new l.a(0);
      }
      return new l.a(m);
    }
    paramArrayOfParticipant = ((MmsTransportInfo)m).g();
    v = 4;
    w = 128;
    paramArrayOfParticipant = paramArrayOfParticipant.e(d);
    o = "personal";
    y = 129;
    A = 129;
    r = 129;
    l = "application/vnd.wap.multipart.related";
    paramArrayOfParticipant = paramArrayOfParticipant.a();
    Object localObject = new ContentValues();
    ((ContentValues)localObject).put("date", Long.valueOf(e.a / 1000L));
    ((ContentValues)localObject).put("date_sent", Long.valueOf(d.a / 1000L));
    ((ContentValues)localObject).put("msg_box", Integer.valueOf(4));
    paramMessage = h.getContentResolver();
    try
    {
      i1 = paramMessage.update(b, (ContentValues)localObject, "_id=?", new String[] { String.valueOf(b) });
    }
    catch (RuntimeException paramMessage)
    {
      AssertionUtil.reportThrowableButNeverCrash(paramMessage);
      i1 = 0;
    }
    if (i1 == 0) {
      return new l.a(0);
    }
    return new l.a(paramArrayOfParticipant);
  }
  
  public final String a()
  {
    return "mms";
  }
  
  public final String a(String paramString)
  {
    return paramString;
  }
  
  /* Error */
  @SuppressLint({"InlinedApi"})
  public final void a(Intent paramIntent, int paramInt)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual 1014	android/content/Intent:getAction	()Ljava/lang/String;
    //   4: astore 8
    //   6: ldc_w 1016
    //   9: aload 8
    //   11: invokevirtual 379	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   14: ifeq +45 -> 59
    //   17: ldc_w 1018
    //   20: aload_1
    //   21: invokevirtual 1021	android/content/Intent:getType	()Ljava/lang/String;
    //   24: invokevirtual 379	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   27: ifeq +32 -> 59
    //   30: aload_0
    //   31: aload_1
    //   32: invokespecial 1023	com/truecaller/messaging/transport/mms/ao:a	(Landroid/content/Intent;)Lcom/truecaller/messaging/data/types/Message;
    //   35: astore_1
    //   36: aload_1
    //   37: ifnull +21 -> 58
    //   40: aload_0
    //   41: getfield 155	com/truecaller/messaging/transport/mms/ao:j	Lcom/truecaller/androidactors/f;
    //   44: invokeinterface 515 1 0
    //   49: checkcast 1025	com/truecaller/messaging/data/t
    //   52: aload_1
    //   53: invokeinterface 1028 2 0
    //   58: return
    //   59: ldc_w 1030
    //   62: aload 8
    //   64: invokevirtual 379	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   67: ifeq +117 -> 184
    //   70: ldc_w 1018
    //   73: aload_1
    //   74: invokevirtual 1021	android/content/Intent:getType	()Ljava/lang/String;
    //   77: invokevirtual 379	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   80: ifeq +104 -> 184
    //   83: new 857	com/truecaller/analytics/e$a
    //   86: dup
    //   87: ldc_w 1032
    //   90: invokespecial 860	com/truecaller/analytics/e$a:<init>	(Ljava/lang/String;)V
    //   93: ldc_w 862
    //   96: ldc_w 864
    //   99: invokevirtual 867	com/truecaller/analytics/e$a:a	(Ljava/lang/String;Ljava/lang/String;)Lcom/truecaller/analytics/e$a;
    //   102: astore 10
    //   104: aload_0
    //   105: getfield 179	com/truecaller/messaging/transport/mms/ao:w	Lcom/truecaller/messaging/a;
    //   108: invokeinterface 952 1 0
    //   113: astore 9
    //   115: aload 9
    //   117: astore 8
    //   119: aload 9
    //   121: ifnonnull +8 -> 129
    //   124: ldc_w 1034
    //   127: astore 8
    //   129: aload 10
    //   131: ldc_w 1036
    //   134: aload 8
    //   136: invokevirtual 867	com/truecaller/analytics/e$a:a	(Ljava/lang/String;Ljava/lang/String;)Lcom/truecaller/analytics/e$a;
    //   139: invokevirtual 880	com/truecaller/analytics/e$a:a	()Lcom/truecaller/analytics/e;
    //   142: astore 8
    //   144: aload_0
    //   145: getfield 175	com/truecaller/messaging/transport/mms/ao:u	Lcom/truecaller/analytics/b;
    //   148: aload 8
    //   150: invokeinterface 885 2 0
    //   155: aload_0
    //   156: aload_1
    //   157: invokespecial 1023	com/truecaller/messaging/transport/mms/ao:a	(Landroid/content/Intent;)Lcom/truecaller/messaging/data/types/Message;
    //   160: astore_1
    //   161: aload_1
    //   162: ifnull +21 -> 183
    //   165: aload_0
    //   166: getfield 167	com/truecaller/messaging/transport/mms/ao:r	Lcom/truecaller/androidactors/f;
    //   169: invokeinterface 515 1 0
    //   174: checkcast 558	com/truecaller/messaging/notifications/a
    //   177: aload_1
    //   178: invokeinterface 1037 2 0
    //   183: return
    //   184: ldc_w 1039
    //   187: aload 8
    //   189: invokevirtual 379	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   192: ifeq +456 -> 648
    //   195: aload_1
    //   196: ldc_w 1041
    //   199: invokevirtual 1044	android/content/Intent:getStringExtra	(Ljava/lang/String;)Ljava/lang/String;
    //   202: astore 8
    //   204: aload 8
    //   206: invokestatic 789	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   209: ifne +21 -> 230
    //   212: aload_0
    //   213: getfield 151	com/truecaller/messaging/transport/mms/ao:h	Landroid/content/Context;
    //   216: invokevirtual 302	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   219: aload 8
    //   221: invokestatic 361	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   224: aconst_null
    //   225: aconst_null
    //   226: invokevirtual 609	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   229: pop
    //   230: aload_1
    //   231: ldc_w 1046
    //   234: ldc2_w 291
    //   237: invokevirtual 1050	android/content/Intent:getLongExtra	(Ljava/lang/String;J)J
    //   240: lstore_3
    //   241: lload_3
    //   242: ldc2_w 291
    //   245: lcmp
    //   246: ifne +4 -> 250
    //   249: return
    //   250: aload_1
    //   251: ldc_w 1052
    //   254: ldc2_w 291
    //   257: invokevirtual 1050	android/content/Intent:getLongExtra	(Ljava/lang/String;J)J
    //   260: lstore 5
    //   262: lload 5
    //   264: ldc2_w 291
    //   267: lcmp
    //   268: ifne +4 -> 272
    //   271: return
    //   272: aload_1
    //   273: ldc_w 1054
    //   276: invokevirtual 1044	android/content/Intent:getStringExtra	(Ljava/lang/String;)Ljava/lang/String;
    //   279: astore 8
    //   281: aload 8
    //   283: ifnonnull +11 -> 294
    //   286: ldc_w 468
    //   289: astore 8
    //   291: goto +3 -> 294
    //   294: aload_1
    //   295: ldc_w 1056
    //   298: invokevirtual 459	android/content/Intent:getByteArrayExtra	(Ljava/lang/String;)[B
    //   301: astore 9
    //   303: new 650	org/a/a/b
    //   306: dup
    //   307: lload 5
    //   309: invokespecial 855	org/a/a/b:<init>	(J)V
    //   312: astore_1
    //   313: aload_0
    //   314: getfield 161	com/truecaller/messaging/transport/mms/ao:k	Lcom/truecaller/multisim/h;
    //   317: aload 8
    //   319: invokeinterface 473 2 0
    //   324: astore 10
    //   326: aload 9
    //   328: ifnonnull +39 -> 367
    //   331: aload_0
    //   332: lload_3
    //   333: invokespecial 1058	com/truecaller/messaging/transport/mms/ao:c	(J)Z
    //   336: ifeq +23 -> 359
    //   339: aload_0
    //   340: getfield 155	com/truecaller/messaging/transport/mms/ao:j	Lcom/truecaller/androidactors/f;
    //   343: invokeinterface 515 1 0
    //   348: checkcast 1025	com/truecaller/messaging/data/t
    //   351: iconst_1
    //   352: aload_1
    //   353: iconst_1
    //   354: invokeinterface 1061 4 0
    //   359: aload_0
    //   360: ldc_w 1063
    //   363: invokespecial 1065	com/truecaller/messaging/transport/mms/ao:d	(Ljava/lang/String;)V
    //   366: return
    //   367: new 310	com/android/a/a/a/n
    //   370: dup
    //   371: aload 9
    //   373: aload 10
    //   375: invokeinterface 477 1 0
    //   380: invokespecial 318	com/android/a/a/a/n:<init>	([BZ)V
    //   383: invokevirtual 321	com/android/a/a/a/n:a	()Lcom/android/a/a/a/f;
    //   386: astore 9
    //   388: aload 9
    //   390: ifnonnull +11 -> 401
    //   393: aload_0
    //   394: ldc_w 1067
    //   397: invokespecial 1065	com/truecaller/messaging/transport/mms/ao:d	(Ljava/lang/String;)V
    //   400: return
    //   401: aload 9
    //   403: invokevirtual 1071	com/android/a/a/a/f:a	()I
    //   406: sipush 129
    //   409: if_icmpeq +11 -> 420
    //   412: aload_0
    //   413: ldc_w 1073
    //   416: invokespecial 1065	com/truecaller/messaging/transport/mms/ao:d	(Ljava/lang/String;)V
    //   419: return
    //   420: aload_0
    //   421: getfield 171	com/truecaller/messaging/transport/mms/ao:o	Lcom/truecaller/messaging/transport/mms/h;
    //   424: aload 9
    //   426: aload_0
    //   427: aload 8
    //   429: invokespecial 479	com/truecaller/messaging/transport/mms/ao:c	(Ljava/lang/String;)Z
    //   432: aload 8
    //   434: lload_3
    //   435: invokeinterface 482 6 0
    //   440: astore 8
    //   442: aload 8
    //   444: getfield 491	com/truecaller/messaging/data/types/Message:m	Lcom/truecaller/messaging/data/types/TransportInfo;
    //   447: checkcast 189	com/truecaller/messaging/transport/mms/MmsTransportInfo
    //   450: astore 9
    //   452: iconst_2
    //   453: aload 9
    //   455: getfield 485	com/truecaller/messaging/transport/mms/MmsTransportInfo:g	I
    //   458: aload 9
    //   460: getfield 773	com/truecaller/messaging/transport/mms/MmsTransportInfo:s	I
    //   463: invokestatic 1076	com/truecaller/messaging/transport/mms/MmsTransportInfo:a	(III)I
    //   466: bipush 8
    //   468: iand
    //   469: ifeq +58 -> 527
    //   472: new 857	com/truecaller/analytics/e$a
    //   475: dup
    //   476: ldc_w 1078
    //   479: invokespecial 860	com/truecaller/analytics/e$a:<init>	(Ljava/lang/String;)V
    //   482: ldc_w 1080
    //   485: iload_2
    //   486: invokevirtual 1083	com/truecaller/analytics/e$a:a	(Ljava/lang/String;I)Lcom/truecaller/analytics/e$a;
    //   489: ldc_w 1085
    //   492: aload 9
    //   494: getfield 773	com/truecaller/messaging/transport/mms/MmsTransportInfo:s	I
    //   497: invokevirtual 1083	com/truecaller/analytics/e$a:a	(Ljava/lang/String;I)Lcom/truecaller/analytics/e$a;
    //   500: ldc_w 1087
    //   503: aload 9
    //   505: getfield 783	com/truecaller/messaging/transport/mms/MmsTransportInfo:r	I
    //   508: invokevirtual 1083	com/truecaller/analytics/e$a:a	(Ljava/lang/String;I)Lcom/truecaller/analytics/e$a;
    //   511: astore 9
    //   513: aload_0
    //   514: getfield 175	com/truecaller/messaging/transport/mms/ao:u	Lcom/truecaller/analytics/b;
    //   517: aload 9
    //   519: invokevirtual 880	com/truecaller/analytics/e$a:a	()Lcom/truecaller/analytics/e;
    //   522: invokeinterface 885 2 0
    //   527: iload_2
    //   528: iconst_m1
    //   529: if_icmpeq +32 -> 561
    //   532: aload_0
    //   533: lload_3
    //   534: invokespecial 1058	com/truecaller/messaging/transport/mms/ao:c	(J)Z
    //   537: ifeq +23 -> 560
    //   540: aload_0
    //   541: getfield 155	com/truecaller/messaging/transport/mms/ao:j	Lcom/truecaller/androidactors/f;
    //   544: invokeinterface 515 1 0
    //   549: checkcast 1025	com/truecaller/messaging/data/t
    //   552: iconst_1
    //   553: aload_1
    //   554: iconst_1
    //   555: invokeinterface 1061 4 0
    //   560: return
    //   561: aload_0
    //   562: getfield 155	com/truecaller/messaging/transport/mms/ao:j	Lcom/truecaller/androidactors/f;
    //   565: invokeinterface 515 1 0
    //   570: checkcast 1025	com/truecaller/messaging/data/t
    //   573: aload 8
    //   575: invokeinterface 1028 2 0
    //   580: aload_0
    //   581: getfield 147	com/truecaller/messaging/transport/mms/ao:x	Lcom/truecaller/messaging/transport/mms/ao$a;
    //   584: astore_1
    //   585: aload_1
    //   586: ifnull +61 -> 647
    //   589: lload_3
    //   590: aload_1
    //   591: getfield 1088	com/truecaller/messaging/transport/mms/ao$a:a	J
    //   594: lcmp
    //   595: ifne +47 -> 642
    //   598: aload_1
    //   599: getfield 1091	com/truecaller/messaging/transport/mms/ao$a:b	Ljava/util/concurrent/locks/Lock;
    //   602: invokeinterface 1096 1 0
    //   607: aload_1
    //   608: getfield 1099	com/truecaller/messaging/transport/mms/ao$a:c	Ljava/util/concurrent/locks/Condition;
    //   611: invokeinterface 1104 1 0
    //   616: aload_1
    //   617: getfield 1091	com/truecaller/messaging/transport/mms/ao$a:b	Ljava/util/concurrent/locks/Lock;
    //   620: invokeinterface 1107 1 0
    //   625: goto +17 -> 642
    //   628: astore 8
    //   630: aload_1
    //   631: getfield 1091	com/truecaller/messaging/transport/mms/ao$a:b	Ljava/util/concurrent/locks/Lock;
    //   634: invokeinterface 1107 1 0
    //   639: aload 8
    //   641: athrow
    //   642: aload_0
    //   643: aconst_null
    //   644: putfield 147	com/truecaller/messaging/transport/mms/ao:x	Lcom/truecaller/messaging/transport/mms/ao$a;
    //   647: return
    //   648: ldc_w 1109
    //   651: aload 8
    //   653: invokevirtual 379	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   656: ifeq +617 -> 1273
    //   659: aload_1
    //   660: invokevirtual 1113	android/content/Intent:getData	()Landroid/net/Uri;
    //   663: astore 10
    //   665: aload 10
    //   667: ifnonnull +11 -> 678
    //   670: aload_0
    //   671: ldc_w 1115
    //   674: invokespecial 1117	com/truecaller/messaging/transport/mms/ao:b	(Ljava/lang/String;)V
    //   677: return
    //   678: aload_1
    //   679: ldc_w 1041
    //   682: invokevirtual 1044	android/content/Intent:getStringExtra	(Ljava/lang/String;)Ljava/lang/String;
    //   685: astore 12
    //   687: aload_1
    //   688: ldc_w 1046
    //   691: ldc2_w 291
    //   694: invokevirtual 1050	android/content/Intent:getLongExtra	(Ljava/lang/String;J)J
    //   697: lstore_3
    //   698: lload_3
    //   699: ldc2_w 291
    //   702: lcmp
    //   703: ifne +29 -> 732
    //   706: aload_0
    //   707: getfield 151	com/truecaller/messaging/transport/mms/ao:h	Landroid/content/Context;
    //   710: invokevirtual 302	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   713: aload 12
    //   715: invokestatic 361	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   718: aconst_null
    //   719: aconst_null
    //   720: invokevirtual 609	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   723: pop
    //   724: aload_0
    //   725: ldc_w 1115
    //   728: invokespecial 1117	com/truecaller/messaging/transport/mms/ao:b	(Ljava/lang/String;)V
    //   731: return
    //   732: aload_1
    //   733: ldc_w 1119
    //   736: invokevirtual 459	android/content/Intent:getByteArrayExtra	(Ljava/lang/String;)[B
    //   739: astore 11
    //   741: aload 11
    //   743: ifnonnull +29 -> 772
    //   746: aload_0
    //   747: getfield 151	com/truecaller/messaging/transport/mms/ao:h	Landroid/content/Context;
    //   750: invokevirtual 302	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   753: aload 12
    //   755: invokestatic 361	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   758: aconst_null
    //   759: aconst_null
    //   760: invokevirtual 609	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   763: pop
    //   764: aload_0
    //   765: ldc_w 1115
    //   768: invokespecial 1117	com/truecaller/messaging/transport/mms/ao:b	(Ljava/lang/String;)V
    //   771: return
    //   772: aload_1
    //   773: ldc_w 1054
    //   776: invokevirtual 1044	android/content/Intent:getStringExtra	(Ljava/lang/String;)Ljava/lang/String;
    //   779: astore 9
    //   781: aload 9
    //   783: astore 8
    //   785: aload 9
    //   787: ifnonnull +8 -> 795
    //   790: ldc_w 468
    //   793: astore 8
    //   795: aload 12
    //   797: invokestatic 361	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   800: astore 9
    //   802: aload_1
    //   803: ldc_w 1121
    //   806: iconst_0
    //   807: invokevirtual 1125	android/content/Intent:getBooleanExtra	(Ljava/lang/String;Z)Z
    //   810: istore 7
    //   812: aload_0
    //   813: getfield 145	com/truecaller/messaging/transport/mms/ao:q	Ljava/util/Set;
    //   816: astore_1
    //   817: aload_1
    //   818: monitorenter
    //   819: aload_0
    //   820: getfield 145	com/truecaller/messaging/transport/mms/ao:q	Ljava/util/Set;
    //   823: lload_3
    //   824: invokestatic 641	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   827: invokeinterface 1128 2 0
    //   832: pop
    //   833: aload_1
    //   834: monitorexit
    //   835: aload_0
    //   836: getfield 161	com/truecaller/messaging/transport/mms/ao:k	Lcom/truecaller/multisim/h;
    //   839: aload 8
    //   841: invokeinterface 473 2 0
    //   846: astore_1
    //   847: iload_2
    //   848: iconst_m1
    //   849: if_icmpne +236 -> 1085
    //   852: aload 9
    //   854: ifnonnull +6 -> 860
    //   857: goto +228 -> 1085
    //   860: aload_0
    //   861: aload 9
    //   863: aload_1
    //   864: invokeinterface 477 1 0
    //   869: invokespecial 1130	com/truecaller/messaging/transport/mms/ao:a	(Landroid/net/Uri;Z)Lcom/android/a/a/a/f;
    //   872: astore_1
    //   873: aload_0
    //   874: getfield 151	com/truecaller/messaging/transport/mms/ao:h	Landroid/content/Context;
    //   877: invokevirtual 302	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   880: aload 9
    //   882: aconst_null
    //   883: aconst_null
    //   884: invokevirtual 609	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   887: pop
    //   888: aload_1
    //   889: ifnonnull +23 -> 912
    //   892: ldc_w 1132
    //   895: aload 9
    //   897: invokestatic 332	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   900: invokevirtual 336	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   903: pop
    //   904: aload_0
    //   905: ldc_w 1115
    //   908: invokespecial 1117	com/truecaller/messaging/transport/mms/ao:b	(Ljava/lang/String;)V
    //   911: return
    //   912: aload_1
    //   913: instanceof 1134
    //   916: ifeq +33 -> 949
    //   919: aload_1
    //   920: checkcast 1134	com/android/a/a/a/s
    //   923: astore 9
    //   925: aload 9
    //   927: getfield 1137	com/android/a/a/a/s:a	Lcom/android/a/a/a/m;
    //   930: sipush 152
    //   933: invokevirtual 1142	com/android/a/a/a/m:b	(I)[B
    //   936: invokestatic 1147	org/c/a/a/a/a:a	([B)Z
    //   939: ifeq +10 -> 949
    //   942: aload 9
    //   944: aload 11
    //   946: invokevirtual 1150	com/android/a/a/a/s:a	([B)V
    //   949: aload_0
    //   950: getfield 171	com/truecaller/messaging/transport/mms/ao:o	Lcom/truecaller/messaging/transport/mms/h;
    //   953: aload_1
    //   954: iconst_0
    //   955: aload 8
    //   957: lload_3
    //   958: invokeinterface 482 6 0
    //   963: astore_1
    //   964: aload_1
    //   965: getfield 491	com/truecaller/messaging/data/types/Message:m	Lcom/truecaller/messaging/data/types/TransportInfo;
    //   968: checkcast 189	com/truecaller/messaging/transport/mms/MmsTransportInfo
    //   971: invokevirtual 827	com/truecaller/messaging/transport/mms/MmsTransportInfo:g	()Lcom/truecaller/messaging/transport/mms/MmsTransportInfo$a;
    //   974: astore 8
    //   976: aload 8
    //   978: aload 10
    //   980: putfield 1152	com/truecaller/messaging/transport/mms/MmsTransportInfo$a:k	Landroid/net/Uri;
    //   983: aload 8
    //   985: iload 7
    //   987: putfield 1153	com/truecaller/messaging/transport/mms/MmsTransportInfo$a:D	Z
    //   990: aload 8
    //   992: invokevirtual 835	com/truecaller/messaging/transport/mms/MmsTransportInfo$a:a	()Lcom/truecaller/messaging/transport/mms/MmsTransportInfo;
    //   995: astore 8
    //   997: aload_1
    //   998: invokevirtual 838	com/truecaller/messaging/data/types/Message:m	()Lcom/truecaller/messaging/data/types/Message$a;
    //   1001: iconst_1
    //   1002: aload 8
    //   1004: invokevirtual 843	com/truecaller/messaging/data/types/Message$a:a	(ILcom/truecaller/messaging/data/types/TransportInfo;)Lcom/truecaller/messaging/data/types/Message$a;
    //   1007: invokevirtual 846	com/truecaller/messaging/data/types/Message$a:b	()Lcom/truecaller/messaging/data/types/Message;
    //   1010: astore_1
    //   1011: aload_1
    //   1012: invokevirtual 903	com/truecaller/messaging/data/types/Message:c	()Z
    //   1015: ifne +11 -> 1026
    //   1018: aload_0
    //   1019: ldc_w 1115
    //   1022: invokespecial 1117	com/truecaller/messaging/transport/mms/ao:b	(Ljava/lang/String;)V
    //   1025: return
    //   1026: aload_0
    //   1027: ldc_w 1155
    //   1030: invokespecial 1117	com/truecaller/messaging/transport/mms/ao:b	(Ljava/lang/String;)V
    //   1033: goto +214 -> 1247
    //   1036: astore_1
    //   1037: goto +31 -> 1068
    //   1040: astore_1
    //   1041: aload_1
    //   1042: invokestatic 450	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   1045: aload_0
    //   1046: ldc_w 1115
    //   1049: invokespecial 1117	com/truecaller/messaging/transport/mms/ao:b	(Ljava/lang/String;)V
    //   1052: aload_0
    //   1053: getfield 151	com/truecaller/messaging/transport/mms/ao:h	Landroid/content/Context;
    //   1056: invokevirtual 302	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   1059: aload 9
    //   1061: aconst_null
    //   1062: aconst_null
    //   1063: invokevirtual 609	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   1066: pop
    //   1067: return
    //   1068: aload_0
    //   1069: getfield 151	com/truecaller/messaging/transport/mms/ao:h	Landroid/content/Context;
    //   1072: invokevirtual 302	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   1075: aload 9
    //   1077: aconst_null
    //   1078: aconst_null
    //   1079: invokevirtual 609	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   1082: pop
    //   1083: aload_1
    //   1084: athrow
    //   1085: iload 7
    //   1087: ifeq +36 -> 1123
    //   1090: aload_0
    //   1091: getfield 163	com/truecaller/messaging/transport/mms/ao:l	Lcom/truecaller/messaging/transport/mms/ak;
    //   1094: aload 8
    //   1096: aload_0
    //   1097: getfield 161	com/truecaller/messaging/transport/mms/ao:k	Lcom/truecaller/multisim/h;
    //   1100: invokevirtual 508	com/truecaller/messaging/transport/mms/ak:a	(Ljava/lang/String;Lcom/truecaller/multisim/h;)Lcom/truecaller/androidactors/f;
    //   1103: invokeinterface 515 1 0
    //   1108: checkcast 517	com/truecaller/messaging/transport/mms/ai
    //   1111: aload 11
    //   1113: aload 10
    //   1115: sipush 131
    //   1118: invokeinterface 526 4 0
    //   1123: new 829	com/truecaller/messaging/transport/mms/MmsTransportInfo$a
    //   1126: dup
    //   1127: invokespecial 980	com/truecaller/messaging/transport/mms/MmsTransportInfo$a:<init>	()V
    //   1130: astore_1
    //   1131: aload_1
    //   1132: lload_3
    //   1133: putfield 831	com/truecaller/messaging/transport/mms/MmsTransportInfo$a:b	J
    //   1136: aload_1
    //   1137: lload_3
    //   1138: invokevirtual 943	com/truecaller/messaging/transport/mms/MmsTransportInfo$a:c	(J)Lcom/truecaller/messaging/transport/mms/MmsTransportInfo$a;
    //   1141: astore_1
    //   1142: aload_1
    //   1143: sipush 132
    //   1146: putfield 989	com/truecaller/messaging/transport/mms/MmsTransportInfo$a:w	I
    //   1149: aload_1
    //   1150: sipush 194
    //   1153: putfield 1156	com/truecaller/messaging/transport/mms/MmsTransportInfo$a:s	I
    //   1156: aload_1
    //   1157: invokevirtual 835	com/truecaller/messaging/transport/mms/MmsTransportInfo$a:a	()Lcom/truecaller/messaging/transport/mms/MmsTransportInfo;
    //   1160: astore_1
    //   1161: new 840	com/truecaller/messaging/data/types/Message$a
    //   1164: dup
    //   1165: invokespecial 1157	com/truecaller/messaging/data/types/Message$a:<init>	()V
    //   1168: astore 10
    //   1170: aload 10
    //   1172: getstatic 1159	com/truecaller/messaging/data/types/Participant:a	Lcom/truecaller/messaging/data/types/Participant;
    //   1175: putfield 1160	com/truecaller/messaging/data/types/Message$a:c	Lcom/truecaller/messaging/data/types/Participant;
    //   1178: aload 10
    //   1180: iload 7
    //   1182: iconst_1
    //   1183: ixor
    //   1184: putfield 1161	com/truecaller/messaging/data/types/Message$a:g	Z
    //   1187: aload 10
    //   1189: iconst_1
    //   1190: aload_1
    //   1191: invokevirtual 843	com/truecaller/messaging/data/types/Message$a:a	(ILcom/truecaller/messaging/data/types/TransportInfo;)Lcom/truecaller/messaging/data/types/Message$a;
    //   1194: aload 8
    //   1196: invokevirtual 1164	com/truecaller/messaging/data/types/Message$a:a	(Ljava/lang/String;)Lcom/truecaller/messaging/data/types/Message$a;
    //   1199: invokevirtual 846	com/truecaller/messaging/data/types/Message$a:b	()Lcom/truecaller/messaging/data/types/Message;
    //   1202: astore_1
    //   1203: aload 9
    //   1205: ifnull +18 -> 1223
    //   1208: aload_0
    //   1209: getfield 151	com/truecaller/messaging/transport/mms/ao:h	Landroid/content/Context;
    //   1212: invokevirtual 302	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   1215: aload 9
    //   1217: aconst_null
    //   1218: aconst_null
    //   1219: invokevirtual 609	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   1222: pop
    //   1223: aload_0
    //   1224: getfield 167	com/truecaller/messaging/transport/mms/ao:r	Lcom/truecaller/androidactors/f;
    //   1227: invokeinterface 515 1 0
    //   1232: checkcast 558	com/truecaller/messaging/notifications/a
    //   1235: invokeinterface 560 1 0
    //   1240: aload_0
    //   1241: ldc_w 1115
    //   1244: invokespecial 1117	com/truecaller/messaging/transport/mms/ao:b	(Ljava/lang/String;)V
    //   1247: aload_0
    //   1248: getfield 155	com/truecaller/messaging/transport/mms/ao:j	Lcom/truecaller/androidactors/f;
    //   1251: invokeinterface 515 1 0
    //   1256: checkcast 1025	com/truecaller/messaging/data/t
    //   1259: aload_1
    //   1260: invokeinterface 1028 2 0
    //   1265: return
    //   1266: astore 8
    //   1268: aload_1
    //   1269: monitorexit
    //   1270: aload 8
    //   1272: athrow
    //   1273: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1274	0	this	ao
    //   0	1274	1	paramIntent	Intent
    //   0	1274	2	paramInt	int
    //   240	898	3	l1	long
    //   260	48	5	l2	long
    //   810	374	7	bool	boolean
    //   4	570	8	localObject1	Object
    //   628	24	8	localObject2	Object
    //   783	412	8	localObject3	Object
    //   1266	5	8	localObject4	Object
    //   113	1103	9	localObject5	Object
    //   102	1086	10	localObject6	Object
    //   739	373	11	arrayOfByte	byte[]
    //   685	111	12	str	String
    // Exception table:
    //   from	to	target	type
    //   607	616	628	finally
    //   860	873	1036	finally
    //   1041	1052	1036	finally
    //   860	873	1040	java/io/IOException
    //   819	835	1266	finally
    //   1268	1270	1266	finally
  }
  
  public final void a(BinaryEntity paramBinaryEntity)
  {
    throw new IllegalStateException("Mms transport can not be used to cancel attachments.");
  }
  
  public final void a(Message paramMessage, String paramString1, String paramString2)
  {
    throw new IllegalStateException("MMS transport does not support sending reactions");
  }
  
  public final void a(org.a.a.b paramb)
  {
    i.a(1, fa);
  }
  
  public final boolean a(Message paramMessage)
  {
    if (a((MmsTransportInfo)m)) {
      return false;
    }
    Message localMessage = a(paramMessage, (MmsTransportInfo)m, true);
    if (localMessage == null) {
      return false;
    }
    if (m).e == null) {
      return false;
    }
    MmsTransportInfo localMmsTransportInfo = (MmsTransportInfo)m;
    if (g == 130)
    {
      String str = c.f;
      paramMessage = str;
      if (str.startsWith("+")) {
        paramMessage = str.substring(1);
      }
      try
      {
        if ((f & 0x1) == 0)
        {
          paramMessage = u.b().d("mms").a(localMmsTransportInfo.a(e)).b(paramMessage).c(c.j()).a();
          ((ae)s.a()).a(paramMessage);
          return true;
        }
      }
      catch (org.apache.a.a paramMessage)
      {
        AssertionUtil.shouldNeverHappen(paramMessage, new String[0]);
      }
    }
    return true;
  }
  
  public final boolean a(Message paramMessage, Entity paramEntity)
  {
    return false;
  }
  
  public final boolean a(Participant paramParticipant)
  {
    return c != 3;
  }
  
  public final boolean a(String paramString, com.truecaller.messaging.transport.a parama)
  {
    parama.a(0, 0, 0, 1);
    return false;
  }
  
  public final void b(long paramLong)
  {
    throw new IllegalStateException("MMS transport does not support retry");
  }
  
  public final boolean b(Message paramMessage)
  {
    return g();
  }
  
  public final boolean b(ad paramad)
  {
    return (!paramad.a()) && (b.equals(c));
  }
  
  public final boolean c()
  {
    return (v.a(new String[] { "android.permission.READ_SMS" })) && (g());
  }
  
  public final boolean c(Message paramMessage)
  {
    return true;
  }
  
  /* Error */
  public final com.truecaller.messaging.transport.k d(Message paramMessage)
  {
    // Byte code:
    //   0: aload_1
    //   1: getfield 503	com/truecaller/messaging/data/types/Message:l	Ljava/lang/String;
    //   4: astore 7
    //   6: aload_0
    //   7: getfield 161	com/truecaller/messaging/transport/mms/ao:k	Lcom/truecaller/multisim/h;
    //   10: aload 7
    //   12: invokeinterface 634 2 0
    //   17: istore_2
    //   18: ldc_w 1275
    //   21: astore 7
    //   23: iload_2
    //   24: iconst_1
    //   25: if_icmpne +11 -> 36
    //   28: ldc_w 1277
    //   31: astore 7
    //   33: goto +13 -> 46
    //   36: iload_2
    //   37: iconst_2
    //   38: if_icmpne +8 -> 46
    //   41: ldc_w 1279
    //   44: astore 7
    //   46: new 857	com/truecaller/analytics/e$a
    //   49: dup
    //   50: ldc_w 1281
    //   53: invokespecial 860	com/truecaller/analytics/e$a:<init>	(Ljava/lang/String;)V
    //   56: ldc_w 1283
    //   59: aload 7
    //   61: invokevirtual 867	com/truecaller/analytics/e$a:a	(Ljava/lang/String;Ljava/lang/String;)Lcom/truecaller/analytics/e$a;
    //   64: invokevirtual 880	com/truecaller/analytics/e$a:a	()Lcom/truecaller/analytics/e;
    //   67: astore 7
    //   69: aload_0
    //   70: getfield 175	com/truecaller/messaging/transport/mms/ao:u	Lcom/truecaller/analytics/b;
    //   73: aload 7
    //   75: invokeinterface 885 2 0
    //   80: aload_0
    //   81: getfield 157	com/truecaller/messaging/transport/mms/ao:m	Lcom/truecaller/utils/d;
    //   84: invokeinterface 1285 1 0
    //   89: bipush 21
    //   91: if_icmpge +24 -> 115
    //   94: aload_0
    //   95: getfield 161	com/truecaller/messaging/transport/mms/ao:k	Lcom/truecaller/multisim/h;
    //   98: aload_1
    //   99: getfield 503	com/truecaller/messaging/data/types/Message:l	Ljava/lang/String;
    //   102: invokeinterface 634 2 0
    //   107: iconst_2
    //   108: if_icmpne +7 -> 115
    //   111: getstatic 1290	com/truecaller/messaging/transport/k$b:a	Lcom/truecaller/messaging/transport/k$b;
    //   114: areturn
    //   115: aload_1
    //   116: getfield 491	com/truecaller/messaging/data/types/Message:m	Lcom/truecaller/messaging/data/types/TransportInfo;
    //   119: checkcast 189	com/truecaller/messaging/transport/mms/MmsTransportInfo
    //   122: astore 11
    //   124: aload 11
    //   126: iconst_0
    //   127: anewarray 79	java/lang/String
    //   130: invokestatic 199	com/truecaller/log/AssertionUtil$AlwaysFatal:isNotNull	(Ljava/lang/Object;[Ljava/lang/String;)V
    //   133: aload 11
    //   135: getfield 493	com/truecaller/messaging/transport/mms/MmsTransportInfo:e	Landroid/net/Uri;
    //   138: iconst_0
    //   139: anewarray 79	java/lang/String
    //   142: invokestatic 199	com/truecaller/log/AssertionUtil$AlwaysFatal:isNotNull	(Ljava/lang/Object;[Ljava/lang/String;)V
    //   145: aload_0
    //   146: aload 11
    //   148: getfield 493	com/truecaller/messaging/transport/mms/MmsTransportInfo:e	Landroid/net/Uri;
    //   151: invokespecial 1292	com/truecaller/messaging/transport/mms/ao:a	(Landroid/net/Uri;)Ljava/util/List;
    //   154: astore 12
    //   156: aload 12
    //   158: ifnonnull +7 -> 165
    //   161: getstatic 1290	com/truecaller/messaging/transport/k$b:a	Lcom/truecaller/messaging/transport/k$b;
    //   164: areturn
    //   165: aconst_null
    //   166: astore 10
    //   168: aconst_null
    //   169: astore 8
    //   171: aconst_null
    //   172: astore 9
    //   174: aload_0
    //   175: getfield 151	com/truecaller/messaging/transport/mms/ao:h	Landroid/content/Context;
    //   178: invokevirtual 302	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   181: astore 13
    //   183: aload 13
    //   185: aload 11
    //   187: getfield 493	com/truecaller/messaging/transport/mms/MmsTransportInfo:e	Landroid/net/Uri;
    //   190: getstatic 91	com/truecaller/messaging/transport/mms/ao:e	[Ljava/lang/String;
    //   193: aconst_null
    //   194: aconst_null
    //   195: aconst_null
    //   196: invokevirtual 580	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   199: astore 7
    //   201: aload 7
    //   203: ifnull +358 -> 561
    //   206: aload 9
    //   208: astore 8
    //   210: aload 7
    //   212: invokeinterface 677 1 0
    //   217: ifne +6 -> 223
    //   220: goto +341 -> 561
    //   223: aload 9
    //   225: astore 8
    //   227: new 99	java/lang/StringBuilder
    //   230: dup
    //   231: ldc_w 352
    //   234: invokespecial 105	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   237: astore 10
    //   239: aload 9
    //   241: astore 8
    //   243: aload 10
    //   245: aload 11
    //   247: getfield 636	com/truecaller/messaging/transport/mms/MmsTransportInfo:b	J
    //   250: invokevirtual 355	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   253: pop
    //   254: aload 9
    //   256: astore 8
    //   258: aload 10
    //   260: ldc_w 357
    //   263: invokevirtual 114	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   266: pop
    //   267: aload 9
    //   269: astore 8
    //   271: aload 13
    //   273: aload 10
    //   275: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   278: invokestatic 361	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   281: getstatic 1296	com/truecaller/messaging/transport/mms/ag:a	[Ljava/lang/String;
    //   284: aconst_null
    //   285: aconst_null
    //   286: aconst_null
    //   287: invokevirtual 580	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   290: astore 9
    //   292: aload 9
    //   294: ifnonnull +23 -> 317
    //   297: aload 9
    //   299: astore 8
    //   301: getstatic 1290	com/truecaller/messaging/transport/k$b:a	Lcom/truecaller/messaging/transport/k$b;
    //   304: astore_1
    //   305: aload 7
    //   307: invokestatic 604	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   310: aload 9
    //   312: invokestatic 604	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   315: aload_1
    //   316: areturn
    //   317: aload 9
    //   319: astore 8
    //   321: aload_0
    //   322: getfield 171	com/truecaller/messaging/transport/mms/ao:o	Lcom/truecaller/messaging/transport/mms/h;
    //   325: astore 13
    //   327: aload 9
    //   329: astore 8
    //   331: aload 7
    //   333: iconst_1
    //   334: invokeinterface 593 2 0
    //   339: astore 14
    //   341: aload 9
    //   343: astore 8
    //   345: aload 7
    //   347: iconst_0
    //   348: invokeinterface 852 2 0
    //   353: lstore 5
    //   355: aload 9
    //   357: astore 8
    //   359: aload 7
    //   361: iconst_2
    //   362: invokeinterface 589 2 0
    //   367: istore_2
    //   368: aload 9
    //   370: astore 8
    //   372: aload 7
    //   374: iconst_3
    //   375: invokeinterface 589 2 0
    //   380: istore_3
    //   381: aload 9
    //   383: astore 8
    //   385: aload 7
    //   387: iconst_4
    //   388: invokeinterface 589 2 0
    //   393: istore 4
    //   395: aload 9
    //   397: astore 8
    //   399: new 1294	com/truecaller/messaging/transport/mms/ag
    //   402: dup
    //   403: aload 9
    //   405: invokespecial 1298	com/truecaller/messaging/transport/mms/ag:<init>	(Landroid/database/Cursor;)V
    //   408: astore 15
    //   410: aload 9
    //   412: astore 8
    //   414: aload_1
    //   415: getfield 503	com/truecaller/messaging/data/types/Message:l	Ljava/lang/String;
    //   418: astore 16
    //   420: aload 7
    //   422: astore 10
    //   424: aload 9
    //   426: astore 8
    //   428: aload 13
    //   430: aload 14
    //   432: lload 5
    //   434: iload_2
    //   435: iload_3
    //   436: iload 4
    //   438: aload 12
    //   440: aload 15
    //   442: aload 16
    //   444: invokeinterface 1301 10 0
    //   449: checkcast 1303	com/android/a/a/a/u
    //   452: astore 12
    //   454: aload 12
    //   456: ifnonnull +23 -> 479
    //   459: aload 9
    //   461: astore 8
    //   463: getstatic 1290	com/truecaller/messaging/transport/k$b:a	Lcom/truecaller/messaging/transport/k$b;
    //   466: astore_1
    //   467: aload 10
    //   469: invokestatic 604	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   472: aload 9
    //   474: invokestatic 604	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   477: aload_1
    //   478: areturn
    //   479: aload 10
    //   481: invokestatic 604	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   484: aload 9
    //   486: invokestatic 604	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   489: aload_0
    //   490: getfield 163	com/truecaller/messaging/transport/mms/ao:l	Lcom/truecaller/messaging/transport/mms/ak;
    //   493: aload_1
    //   494: getfield 503	com/truecaller/messaging/data/types/Message:l	Ljava/lang/String;
    //   497: aload_0
    //   498: getfield 161	com/truecaller/messaging/transport/mms/ao:k	Lcom/truecaller/multisim/h;
    //   501: invokevirtual 508	com/truecaller/messaging/transport/mms/ak:a	(Ljava/lang/String;Lcom/truecaller/multisim/h;)Lcom/truecaller/androidactors/f;
    //   504: invokeinterface 515 1 0
    //   509: checkcast 517	com/truecaller/messaging/transport/mms/ai
    //   512: aload 11
    //   514: getfield 636	com/truecaller/messaging/transport/mms/MmsTransportInfo:b	J
    //   517: aload_1
    //   518: getfield 534	com/truecaller/messaging/data/types/Message:e	Lorg/a/a/b;
    //   521: getfield 662	org/a/a/a/g:a	J
    //   524: aload 12
    //   526: aload 11
    //   528: getfield 493	com/truecaller/messaging/transport/mms/MmsTransportInfo:e	Landroid/net/Uri;
    //   531: invokeinterface 1306 7 0
    //   536: aload_0
    //   537: new 9	com/truecaller/messaging/transport/mms/ao$a
    //   540: dup
    //   541: aload 11
    //   543: getfield 636	com/truecaller/messaging/transport/mms/MmsTransportInfo:b	J
    //   546: invokespecial 1307	com/truecaller/messaging/transport/mms/ao$a:<init>	(J)V
    //   549: putfield 147	com/truecaller/messaging/transport/mms/ao:x	Lcom/truecaller/messaging/transport/mms/ao$a;
    //   552: aload_0
    //   553: getfield 147	com/truecaller/messaging/transport/mms/ao:x	Lcom/truecaller/messaging/transport/mms/ao$a;
    //   556: areturn
    //   557: astore_1
    //   558: goto +30 -> 588
    //   561: aload 10
    //   563: astore 8
    //   565: getstatic 1290	com/truecaller/messaging/transport/k$b:a	Lcom/truecaller/messaging/transport/k$b;
    //   568: astore_1
    //   569: aload 7
    //   571: invokestatic 604	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   574: aconst_null
    //   575: invokestatic 604	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   578: aload_1
    //   579: areturn
    //   580: astore_1
    //   581: goto +7 -> 588
    //   584: astore_1
    //   585: aconst_null
    //   586: astore 7
    //   588: aload 7
    //   590: invokestatic 604	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   593: aload 8
    //   595: invokestatic 604	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   598: aload_1
    //   599: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	600	0	this	ao
    //   0	600	1	paramMessage	Message
    //   17	418	2	i1	int
    //   380	56	3	i2	int
    //   393	44	4	i3	int
    //   353	80	5	l1	long
    //   4	585	7	localObject1	Object
    //   169	425	8	localObject2	Object
    //   172	313	9	localCursor	Cursor
    //   166	396	10	localObject3	Object
    //   122	420	11	localMmsTransportInfo	MmsTransportInfo
    //   154	371	12	localObject4	Object
    //   181	248	13	localObject5	Object
    //   339	92	14	str1	String
    //   408	33	15	localag	ag
    //   418	25	16	str2	String
    // Exception table:
    //   from	to	target	type
    //   210	220	557	finally
    //   227	239	557	finally
    //   243	254	557	finally
    //   258	267	557	finally
    //   271	292	557	finally
    //   301	305	557	finally
    //   321	327	557	finally
    //   331	341	557	finally
    //   345	355	557	finally
    //   359	368	557	finally
    //   372	381	557	finally
    //   385	395	557	finally
    //   399	410	557	finally
    //   414	420	557	finally
    //   428	454	580	finally
    //   463	467	580	finally
    //   565	569	580	finally
    //   174	201	584	finally
  }
  
  /* Error */
  @SuppressLint({"NewApi"})
  public final org.a.a.b d()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 153	com/truecaller/messaging/transport/mms/ao:i	Lcom/truecaller/messaging/h;
    //   4: iconst_1
    //   5: invokeinterface 889 2 0
    //   10: lstore_1
    //   11: lload_1
    //   12: lstore_3
    //   13: aload_0
    //   14: getfield 149	com/truecaller/messaging/transport/mms/ao:y	Z
    //   17: ifne +283 -> 300
    //   20: lload_1
    //   21: lstore_3
    //   22: aload_0
    //   23: getfield 177	com/truecaller/messaging/transport/mms/ao:v	Lcom/truecaller/utils/l;
    //   26: iconst_1
    //   27: anewarray 79	java/lang/String
    //   30: dup
    //   31: iconst_0
    //   32: ldc_w 1309
    //   35: aastore
    //   36: invokeinterface 964 2 0
    //   41: ifeq +259 -> 300
    //   44: lload_1
    //   45: lstore_3
    //   46: aload_0
    //   47: getfield 177	com/truecaller/messaging/transport/mms/ao:v	Lcom/truecaller/utils/l;
    //   50: iconst_1
    //   51: anewarray 79	java/lang/String
    //   54: dup
    //   55: iconst_0
    //   56: ldc_w 959
    //   59: aastore
    //   60: invokeinterface 964 2 0
    //   65: ifeq +235 -> 300
    //   68: aload_0
    //   69: getfield 151	com/truecaller/messaging/transport/mms/ao:h	Landroid/content/Context;
    //   72: invokevirtual 302	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   75: astore 12
    //   77: aconst_null
    //   78: astore 11
    //   80: aconst_null
    //   81: astore 9
    //   83: aload 12
    //   85: getstatic 129	com/truecaller/messaging/transport/mms/ao:b	Landroid/net/Uri;
    //   88: iconst_1
    //   89: anewarray 79	java/lang/String
    //   92: dup
    //   93: iconst_0
    //   94: ldc 81
    //   96: aastore
    //   97: ldc_w 1311
    //   100: aconst_null
    //   101: ldc_w 1313
    //   104: invokevirtual 580	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   107: astore 10
    //   109: lload_1
    //   110: lstore_3
    //   111: aload 10
    //   113: ifnull +122 -> 235
    //   116: lload_1
    //   117: lstore 5
    //   119: lload_1
    //   120: lstore_3
    //   121: aload 10
    //   123: invokeinterface 585 1 0
    //   128: ifeq +107 -> 235
    //   131: lload_1
    //   132: lstore 5
    //   134: aload 10
    //   136: iconst_0
    //   137: invokeinterface 852 2 0
    //   142: ldc2_w 663
    //   145: lmul
    //   146: lstore 7
    //   148: lload_1
    //   149: lstore 5
    //   151: new 344	android/content/ContentValues
    //   154: dup
    //   155: invokespecial 345	android/content/ContentValues:<init>	()V
    //   158: astore 9
    //   160: lload_1
    //   161: lstore 5
    //   163: aload 9
    //   165: ldc_w 917
    //   168: iconst_5
    //   169: invokestatic 546	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   172: invokevirtual 550	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   175: lload_1
    //   176: lstore 5
    //   178: aload 12
    //   180: getstatic 129	com/truecaller/messaging/transport/mms/ao:b	Landroid/net/Uri;
    //   183: aload 9
    //   185: ldc_w 1311
    //   188: aconst_null
    //   189: invokevirtual 556	android/content/ContentResolver:update	(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   192: pop
    //   193: lload_1
    //   194: lstore_3
    //   195: lload 7
    //   197: lload_1
    //   198: lcmp
    //   199: ifge +6 -> 205
    //   202: lload 7
    //   204: lstore_3
    //   205: lload_3
    //   206: lstore 5
    //   208: aload_0
    //   209: getfield 153	com/truecaller/messaging/transport/mms/ao:i	Lcom/truecaller/messaging/h;
    //   212: iconst_1
    //   213: lload_3
    //   214: invokeinterface 897 4 0
    //   219: goto +16 -> 235
    //   222: astore 9
    //   224: goto +68 -> 292
    //   227: astore 11
    //   229: lload 5
    //   231: lstore_1
    //   232: goto +36 -> 268
    //   235: aload 10
    //   237: invokestatic 604	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   240: lload_3
    //   241: lstore_1
    //   242: goto +40 -> 282
    //   245: astore 11
    //   247: aload 9
    //   249: astore 10
    //   251: aload 11
    //   253: astore 9
    //   255: goto +37 -> 292
    //   258: astore 9
    //   260: aload 11
    //   262: astore 10
    //   264: aload 9
    //   266: astore 11
    //   268: aload 10
    //   270: astore 9
    //   272: aload 11
    //   274: invokestatic 450	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   277: aload 10
    //   279: invokestatic 604	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   282: aload_0
    //   283: iconst_1
    //   284: putfield 149	com/truecaller/messaging/transport/mms/ao:y	Z
    //   287: lload_1
    //   288: lstore_3
    //   289: goto +11 -> 300
    //   292: aload 10
    //   294: invokestatic 604	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   297: aload 9
    //   299: athrow
    //   300: new 650	org/a/a/b
    //   303: dup
    //   304: lload_3
    //   305: invokespecial 855	org/a/a/b:<init>	(J)V
    //   308: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	309	0	this	ao
    //   10	278	1	l1	long
    //   12	293	3	l2	long
    //   117	113	5	l3	long
    //   146	57	7	l4	long
    //   81	103	9	localContentValues	ContentValues
    //   222	26	9	localObject1	Object
    //   253	1	9	localObject2	Object
    //   258	7	9	localRuntimeException1	RuntimeException
    //   270	28	9	localObject3	Object
    //   107	186	10	localObject4	Object
    //   78	1	11	localObject5	Object
    //   227	1	11	localRuntimeException2	RuntimeException
    //   245	16	11	localObject6	Object
    //   266	7	11	localObject7	Object
    //   75	104	12	localContentResolver	ContentResolver
    // Exception table:
    //   from	to	target	type
    //   121	131	222	finally
    //   134	148	222	finally
    //   151	160	222	finally
    //   163	175	222	finally
    //   178	193	222	finally
    //   208	219	222	finally
    //   121	131	227	java/lang/RuntimeException
    //   134	148	227	java/lang/RuntimeException
    //   151	160	227	java/lang/RuntimeException
    //   163	175	227	java/lang/RuntimeException
    //   178	193	227	java/lang/RuntimeException
    //   208	219	227	java/lang/RuntimeException
    //   83	109	245	finally
    //   272	277	245	finally
    //   83	109	258	java/lang/RuntimeException
  }
  
  public final int e(Message arg1)
  {
    MmsTransportInfo localMmsTransportInfo = (MmsTransportInfo)m;
    synchronized (q)
    {
      if (q.contains(Long.valueOf(b))) {
        return 2;
      }
      if ((g == 130) && (!p.d(e.a())))
      {
        if (b(localMmsTransportInfo)) {
          return 3;
        }
        return 1;
      }
      return 0;
    }
  }
  
  public final boolean e()
  {
    return true;
  }
  
  public final int f()
  {
    return 1;
  }
  
  public final boolean f(Message paramMessage)
  {
    boolean bool;
    if (j == 1) {
      bool = true;
    } else {
      bool = false;
    }
    AssertionUtil.AlwaysFatal.isTrue(bool, new String[0]);
    return a(paramMessage, true);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.ao
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */