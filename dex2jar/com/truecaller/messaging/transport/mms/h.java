package com.truecaller.messaging.transport.mms;

import android.content.ContentValues;
import com.android.a.a.a.f;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.multisim.a;
import java.util.List;

abstract interface h
{
  public abstract f a(String paramString1, long paramLong, int paramInt1, int paramInt2, int paramInt3, List<String> paramList, h.a parama, String paramString2);
  
  public abstract Message a(f paramf, boolean paramBoolean, String paramString, long paramLong);
  
  public abstract List<PduEntity> a(Entity[] paramArrayOfEntity, a parama);
  
  public abstract void a(PduEntity paramPduEntity, ContentValues paramContentValues);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */