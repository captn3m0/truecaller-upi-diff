package com.truecaller.messaging.transport.mms;

import android.content.Context;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.b;
import com.truecaller.androidactors.f;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.transport.ad.b;
import javax.inject.Provider;

public final class v
  implements dagger.a.d<com.truecaller.messaging.transport.l>
{
  private final n a;
  private final Provider<Context> b;
  private final Provider<com.truecaller.messaging.h> c;
  private final Provider<f<t>> d;
  private final Provider<com.truecaller.multisim.h> e;
  private final Provider<ak> f;
  private final Provider<an> g;
  private final Provider<com.truecaller.utils.d> h;
  private final Provider<f<com.truecaller.messaging.notifications.a>> i;
  private final Provider<h> j;
  private final Provider<f<ae>> k;
  private final Provider<ad.b> l;
  private final Provider<b> m;
  private final Provider<com.truecaller.utils.l> n;
  private final Provider<com.truecaller.messaging.a> o;
  
  private v(n paramn, Provider<Context> paramProvider, Provider<com.truecaller.messaging.h> paramProvider1, Provider<f<t>> paramProvider2, Provider<com.truecaller.multisim.h> paramProvider3, Provider<ak> paramProvider4, Provider<an> paramProvider5, Provider<com.truecaller.utils.d> paramProvider6, Provider<f<com.truecaller.messaging.notifications.a>> paramProvider7, Provider<h> paramProvider8, Provider<f<ae>> paramProvider9, Provider<ad.b> paramProvider10, Provider<b> paramProvider11, Provider<com.truecaller.utils.l> paramProvider12, Provider<com.truecaller.messaging.a> paramProvider13)
  {
    a = paramn;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
    f = paramProvider4;
    g = paramProvider5;
    h = paramProvider6;
    i = paramProvider7;
    j = paramProvider8;
    k = paramProvider9;
    l = paramProvider10;
    m = paramProvider11;
    n = paramProvider12;
    o = paramProvider13;
  }
  
  public static v a(n paramn, Provider<Context> paramProvider, Provider<com.truecaller.messaging.h> paramProvider1, Provider<f<t>> paramProvider2, Provider<com.truecaller.multisim.h> paramProvider3, Provider<ak> paramProvider4, Provider<an> paramProvider5, Provider<com.truecaller.utils.d> paramProvider6, Provider<f<com.truecaller.messaging.notifications.a>> paramProvider7, Provider<h> paramProvider8, Provider<f<ae>> paramProvider9, Provider<ad.b> paramProvider10, Provider<b> paramProvider11, Provider<com.truecaller.utils.l> paramProvider12, Provider<com.truecaller.messaging.a> paramProvider13)
  {
    return new v(paramn, paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10, paramProvider11, paramProvider12, paramProvider13);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.v
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */