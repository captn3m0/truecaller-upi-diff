package com.truecaller.messaging.transport.mms;

import android.annotation.SuppressLint;
import android.content.Context;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.a;
import com.truecaller.multisim.ac;
import com.truecaller.multisim.h;
import com.truecaller.multisim.o;

public final class ak
{
  private final Context a;
  private final i b;
  
  public ak(Context paramContext, i parami)
  {
    a = paramContext;
    b = parami;
  }
  
  @SuppressLint({"NewApi"})
  public final f<ai> a(String paramString, h paramh)
  {
    c.g.b.k.b(paramString, "simToken");
    c.g.b.k.b(paramh, "multiSimManager");
    boolean bool = paramh instanceof com.truecaller.multisim.l;
    if ((!bool) && (!(paramh instanceof o)) && (!(paramh instanceof ac)))
    {
      localSimInfo = paramh.b(paramString);
      paramString = paramh.c(paramString);
      c.g.b.k.a(paramString, "multiSimManager.getCarrierConfiguration(simToken)");
      paramString = new al(a, localSimInfo, paramString);
      paramString = b.a(ai.class, paramString);
      c.g.b.k.a(paramString, "mThread.bind(MmsSender::class.java, sender)");
      return paramString;
    }
    SimInfo localSimInfo = paramh.b(paramString);
    a locala = paramh.c(paramString);
    c.g.b.k.a(locala, "multiSimManager.getCarrierConfiguration(simToken)");
    Object localObject = l.a;
    localObject = a;
    c.g.b.k.b(localObject, "context");
    c.g.b.k.b(paramh, "multiSimManager");
    c.g.b.k.b(paramString, "simToken");
    if ((!bool) && (!(paramh instanceof o)))
    {
      if ((paramh instanceof ac))
      {
        paramh = paramh.f(paramString);
        c.g.b.k.a(paramh, "multiSimManager.getSmsManager(simToken)");
        paramString = (k)new aq((Context)localObject, paramString, paramh);
      }
      else
      {
        paramString = new StringBuilder();
        paramString.append(paramh.getClass().getCanonicalName());
        paramString.append(" is not supported");
        throw ((Throwable)new IllegalArgumentException(paramString.toString()));
      }
    }
    else
    {
      paramString = paramh.f(paramString);
      c.g.b.k.a(paramString, "multiSimManager.getSmsManager(simToken)");
      paramString = (k)new g((Context)localObject, paramString);
    }
    paramString = new am(a, localSimInfo, locala, paramString);
    paramString = b.a(ai.class, paramString);
    c.g.b.k.a(paramString, "mThread.bind(MmsSender::class.java, sender)");
    return paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.ak
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */