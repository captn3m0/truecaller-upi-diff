package com.truecaller.messaging.transport;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.SystemClock;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.messaging.c.a;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.transport.a.c;

final class b
  implements d
{
  private final Context a;
  private final f<t> b;
  private final f<c> c;
  private final f<c> d;
  private final a e;
  
  b(Context paramContext, f<t> paramf, f<c> paramf1, f<c> paramf2, a parama)
  {
    a = paramContext;
    b = paramf;
    c = paramf1;
    d = paramf2;
    e = parama;
  }
  
  @SuppressLint({"NewApi"})
  public final w<Message> a(Message paramMessage, Participant[] paramArrayOfParticipant, int paramInt1, int paramInt2)
  {
    try
    {
      paramMessage = (Message)((t)b.a()).a(paramMessage, paramArrayOfParticipant, paramInt1).d();
      if (paramMessage == null) {
        return w.b(null);
      }
      AssertionUtil.AlwaysFatal.isTrue(paramMessage.b(), new String[0]);
      paramInt1 = f;
      bool2 = true;
      if ((paramInt1 & 0x10) == 0) {
        break label304;
      }
      bool1 = true;
    }
    catch (InterruptedException paramMessage)
    {
      for (;;)
      {
        boolean bool2;
        long l;
        AlarmManager localAlarmManager;
        continue;
        label304:
        boolean bool1 = false;
        continue;
        bool1 = false;
        continue;
        label316:
        bool1 = false;
        continue;
        label322:
        bool1 = false;
      }
    }
    AssertionUtil.AlwaysFatal.isTrue(bool1, new String[0]);
    if (k != 3)
    {
      bool1 = true;
      AssertionUtil.AlwaysFatal.isTrue(bool1, new String[0]);
      if (j != 3) {
        break label316;
      }
      bool1 = true;
      AssertionUtil.AlwaysFatal.isTrue(bool1, new String[0]);
      if (m.c() == -1L) {
        break label322;
      }
      bool1 = bool2;
      AssertionUtil.AlwaysFatal.isTrue(bool1, new String[0]);
      if (paramInt2 == 0)
      {
        if (((t)b.a()).a(null).d() == Boolean.FALSE) {
          return w.b(null);
        }
        return w.b(paramMessage);
      }
      paramArrayOfParticipant = ScheduledMessageReceiver.a(a, e);
      a.sendBroadcast(paramArrayOfParticipant);
      paramArrayOfParticipant = PendingIntent.getBroadcast(a, 0, ScheduledMessageReceiver.a(a, null), 268435456);
      l = SystemClock.elapsedRealtime() + paramInt2;
      localAlarmManager = (AlarmManager)a.getSystemService("alarm");
      if (Build.VERSION.SDK_INT >= 23) {
        localAlarmManager.setExactAndAllowWhileIdle(2, l, paramArrayOfParticipant);
      } else {
        android.support.v4.app.b.a(localAlarmManager, 2, l, paramArrayOfParticipant);
      }
      paramMessage = w.b(paramMessage);
      return paramMessage;
      return w.b(null);
    }
  }
  
  public final void a(Message paramMessage)
  {
    boolean bool;
    if ((f & 0x9) == 9) {
      bool = true;
    } else {
      bool = false;
    }
    AssertionUtil.AlwaysFatal.isTrue(bool, new String[0]);
    ((t)b.a()).c(paramMessage).c();
  }
  
  public final void a(l paraml, Intent paramIntent, int paramInt)
  {
    paraml.a(paramIntent, paramInt);
  }
  
  public final void b(Message paramMessage)
  {
    if (j == 2) {
      ((c)d.a()).a(paramMessage);
    } else {
      ((c)c.a()).a(paramMessage);
    }
    e.c(o);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */