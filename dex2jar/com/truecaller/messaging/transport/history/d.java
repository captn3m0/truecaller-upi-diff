package com.truecaller.messaging.transport.history;

import android.content.Context;
import com.truecaller.featuretoggles.e;
import com.truecaller.multisim.h;
import javax.inject.Provider;

public final class d
  implements dagger.a.d<b>
{
  private final Provider<Context> a;
  private final Provider<h> b;
  private final Provider<e> c;
  
  private d(Provider<Context> paramProvider, Provider<h> paramProvider1, Provider<e> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static d a(Provider<Context> paramProvider, Provider<h> paramProvider1, Provider<e> paramProvider2)
  {
    return new d(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.history.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */