package com.truecaller.messaging.transport.history;

import com.truecaller.androidactors.f;
import com.truecaller.callhistory.a;
import com.truecaller.messaging.h;
import com.truecaller.messaging.transport.ad.b;
import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d<e>
{
  private final Provider<f<a>> a;
  private final Provider<b> b;
  private final Provider<h> c;
  private final Provider<ad.b> d;
  
  private g(Provider<f<a>> paramProvider, Provider<b> paramProvider1, Provider<h> paramProvider2, Provider<ad.b> paramProvider3)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static g a(Provider<f<a>> paramProvider, Provider<b> paramProvider1, Provider<h> paramProvider2, Provider<ad.b> paramProvider3)
  {
    return new g(paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.history.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */