package com.truecaller.messaging.transport.history;

import android.content.ContentProviderOperation;
import android.content.Intent;
import com.truecaller.content.TruecallerContract.aa;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.h;
import com.truecaller.messaging.transport.ad;
import com.truecaller.messaging.transport.ad.a.a;
import com.truecaller.messaging.transport.ad.b;
import com.truecaller.messaging.transport.i;
import com.truecaller.messaging.transport.l;
import com.truecaller.messaging.transport.l.a;
import com.truecaller.utils.q;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import org.a.a.a.g;

public final class e
  implements l<f>
{
  private final com.truecaller.androidactors.f<com.truecaller.callhistory.a> a;
  private final b b;
  private final h c;
  private final ad.b d;
  
  @Inject
  public e(com.truecaller.androidactors.f<com.truecaller.callhistory.a> paramf, b paramb, h paramh, ad.b paramb1)
  {
    a = paramf;
    b = paramb;
    c = paramh;
    d = paramb1;
  }
  
  private static void a(HistoryTransportInfo paramHistoryTransportInfo, f paramf)
  {
    paramf.a(paramf.a(TruecallerContract.aa.a(a)).a("read", Integer.valueOf(1)).a("seen", Integer.valueOf(1)).a("sync_status", Integer.valueOf(1)).a());
    if (c != 0)
    {
      paramf.a(c);
      return;
    }
    paramf.b(b);
  }
  
  public final long a(long paramLong)
  {
    return paramLong;
  }
  
  public final long a(com.truecaller.messaging.transport.f paramf, i parami, r paramr, org.a.a.b paramb1, org.a.a.b paramb2, int paramInt, List<ContentProviderOperation> paramList, q paramq, boolean paramBoolean1, boolean paramBoolean2, Set<Long> paramSet)
  {
    c.g.b.k.b(paramf, "threadInfoCache");
    c.g.b.k.b(parami, "participantCache");
    c.g.b.k.b(paramr, "cursor");
    c.g.b.k.b(paramb1, "timeTo");
    c.g.b.k.b(paramb2, "timeFrom");
    c.g.b.k.b(paramList, "operations");
    c.g.b.k.b(paramq, "trace");
    c.g.b.k.b(paramSet, "messagesToClassify");
    return b.a(paramf, parami, paramr, paramb1, paramb2, paramInt, paramList, paramq, paramBoolean1, paramBoolean2, paramSet);
  }
  
  public final l.a a(Message paramMessage, Participant[] paramArrayOfParticipant)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramArrayOfParticipant, "recipients");
    throw ((Throwable)new IllegalStateException("History transport cannot enqueue any message"));
  }
  
  public final String a()
  {
    return "history";
  }
  
  public final String a(String paramString)
  {
    c.g.b.k.b(paramString, "simToken");
    return paramString;
  }
  
  public final void a(Intent paramIntent, int paramInt)
  {
    c.g.b.k.b(paramIntent, "intent");
    throw ((Throwable)new IllegalStateException("History transport does not expect any intent"));
  }
  
  public final void a(BinaryEntity paramBinaryEntity)
  {
    c.g.b.k.b(paramBinaryEntity, "entity");
    throw ((Throwable)new IllegalStateException("History transport doesn't support attachments"));
  }
  
  public final void a(Message paramMessage, String paramString1, String paramString2)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramString2, "initiatedVia");
    throw ((Throwable)new IllegalStateException("History transport does not support reactions"));
  }
  
  public final void a(org.a.a.b paramb)
  {
    c.g.b.k.b(paramb, "time");
    c.a(5, a);
  }
  
  public final boolean a(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    throw ((Throwable)new IllegalStateException("History transport should only sync up with already existing events"));
  }
  
  public final boolean a(Message paramMessage, Entity paramEntity)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramEntity, "entity");
    throw ((Throwable)new IllegalStateException("History transport doesn't support entity download"));
  }
  
  public final boolean a(Participant paramParticipant)
  {
    c.g.b.k.b(paramParticipant, "participant");
    return true;
  }
  
  public final boolean a(String paramString, com.truecaller.messaging.transport.a parama)
  {
    c.g.b.k.b(paramString, "text");
    c.g.b.k.b(parama, "result");
    parama.a(0, 0, 0, 5);
    return false;
  }
  
  public final void b(long paramLong)
  {
    throw ((Throwable)new IllegalStateException("History transport does not support retry"));
  }
  
  public final boolean b(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return false;
  }
  
  public final boolean b(ad paramad)
  {
    c.g.b.k.b(paramad, "transaction");
    paramad = (f)paramad;
    if ((!(((Collection)e).isEmpty() ^ true)) && (!(((Collection)g).isEmpty() ^ true)) && (!(((Collection)d).isEmpty() ^ true)) && (!(((Collection)f).isEmpty() ^ true))) {
      return !paramad.a();
    }
    return true;
  }
  
  public final boolean c()
  {
    return false;
  }
  
  public final boolean c(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return false;
  }
  
  public final com.truecaller.messaging.transport.k d(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    throw ((Throwable)new IllegalStateException("History transport cannot send messages"));
  }
  
  public final org.a.a.b d()
  {
    return new org.a.a.b(c.a(5));
  }
  
  public final int e(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return 0;
  }
  
  public final boolean e()
  {
    return false;
  }
  
  public final int f()
  {
    return 5;
  }
  
  public final boolean f(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    throw ((Throwable)new IllegalStateException("History transport doesn't support content download"));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.history.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */