package com.truecaller.messaging.transport.im;

import android.content.Context;
import com.truecaller.androidactors.f;
import com.truecaller.common.h.an;
import com.truecaller.common.h.u;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.h;
import com.truecaller.notificationchannels.e;
import com.truecaller.notifications.a;
import com.truecaller.notifications.l;
import com.truecaller.util.af;
import com.truecaller.util.al;
import dagger.a.d;
import javax.inject.Provider;

public final class ca
  implements d<bz>
{
  private final Provider<bw> a;
  private final Provider<f<bp>> b;
  private final Provider<al> c;
  private final Provider<h> d;
  private final Provider<af> e;
  private final Provider<Context> f;
  private final Provider<a> g;
  private final Provider<l> h;
  private final Provider<e> i;
  private final Provider<u> j;
  private final Provider<an> k;
  
  private ca(Provider<bw> paramProvider, Provider<f<bp>> paramProvider1, Provider<al> paramProvider2, Provider<h> paramProvider3, Provider<af> paramProvider4, Provider<Context> paramProvider5, Provider<a> paramProvider6, Provider<l> paramProvider7, Provider<e> paramProvider8, Provider<u> paramProvider9, Provider<an> paramProvider10)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
    k = paramProvider10;
  }
  
  public static ca a(Provider<bw> paramProvider, Provider<f<bp>> paramProvider1, Provider<al> paramProvider2, Provider<h> paramProvider3, Provider<af> paramProvider4, Provider<Context> paramProvider5, Provider<a> paramProvider6, Provider<l> paramProvider7, Provider<e> paramProvider8, Provider<u> paramProvider9, Provider<an> paramProvider10)
  {
    return new ca(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.ca
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */