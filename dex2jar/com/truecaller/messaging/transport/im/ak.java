package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.i;
import com.truecaller.androidactors.k;
import dagger.a.d;
import javax.inject.Provider;

public final class ak
  implements d<i>
{
  private final Provider<k> a;
  
  private ak(Provider<k> paramProvider)
  {
    a = paramProvider;
  }
  
  public static ak a(Provider<k> paramProvider)
  {
    return new ak(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.ak
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */