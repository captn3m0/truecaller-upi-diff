package com.truecaller.messaging.transport.im.a;

import com.truecaller.androidactors.w;
import com.truecaller.api.services.messenger.v1.events.Event.d;
import com.truecaller.api.services.messenger.v1.events.Event.f;
import com.truecaller.api.services.messenger.v1.events.Event.n;
import com.truecaller.api.services.messenger.v1.events.Event.p;
import com.truecaller.api.services.messenger.v1.events.Event.x;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.data.types.Participant;
import java.util.Collection;
import java.util.List;

public abstract interface a
{
  public abstract w<Collection<String>> a();
  
  public abstract w<g> a(String paramString);
  
  public abstract w<Boolean> a(String paramString, int paramInt);
  
  public abstract w<Boolean> a(String paramString, Participant paramParticipant);
  
  public abstract w<Boolean> a(String paramString1, String paramString2, int paramInt);
  
  public abstract w<Boolean> a(String paramString1, String paramString2, String paramString3);
  
  public abstract w<Boolean> a(String paramString, List<? extends Participant> paramList);
  
  public abstract w<Boolean> a(String paramString, boolean paramBoolean);
  
  public abstract w<Participant> a(List<? extends Participant> paramList, String paramString1, String paramString2);
  
  public abstract void a(Event.d paramd);
  
  public abstract void a(Event.f paramf);
  
  public abstract void a(Event.n paramn);
  
  public abstract void a(Event.p paramp);
  
  public abstract void a(Event.x paramx);
  
  public abstract w<List<Participant>> b(String paramString);
  
  public abstract w<Boolean> b(String paramString, boolean paramBoolean);
  
  public abstract void b();
  
  public abstract w<Integer> c(String paramString);
  
  public abstract w<Boolean> d(String paramString);
  
  public abstract w<ImGroupInfo> e(String paramString);
  
  public abstract void f(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */