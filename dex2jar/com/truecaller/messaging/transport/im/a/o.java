package com.truecaller.messaging.transport.im.a;

import android.content.ContentResolver;
import com.truecaller.androidactors.f;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.h;
import com.truecaller.utils.a;
import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d<n>
{
  private final Provider<f<t>> a;
  private final Provider<ContentResolver> b;
  private final Provider<a> c;
  private final Provider<e> d;
  private final Provider<com.truecaller.utils.n> e;
  private final Provider<i> f;
  private final Provider<h> g;
  
  private o(Provider<f<t>> paramProvider, Provider<ContentResolver> paramProvider1, Provider<a> paramProvider2, Provider<e> paramProvider3, Provider<com.truecaller.utils.n> paramProvider4, Provider<i> paramProvider5, Provider<h> paramProvider6)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
  }
  
  public static o a(Provider<f<t>> paramProvider, Provider<ContentResolver> paramProvider1, Provider<a> paramProvider2, Provider<e> paramProvider3, Provider<com.truecaller.utils.n> paramProvider4, Provider<i> paramProvider5, Provider<h> paramProvider6)
  {
    return new o(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */