package com.truecaller.messaging.transport.im.a;

import android.annotation.SuppressLint;
import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.RemoteException;
import c.a.ag;
import c.j.c.b;
import c.n;
import c.u;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.w;
import com.truecaller.api.services.messenger.v1.a.b;
import com.truecaller.api.services.messenger.v1.a.b.a;
import com.truecaller.api.services.messenger.v1.a.d;
import com.truecaller.api.services.messenger.v1.d.b;
import com.truecaller.api.services.messenger.v1.d.b.a;
import com.truecaller.api.services.messenger.v1.d.d;
import com.truecaller.api.services.messenger.v1.events.Event.d;
import com.truecaller.api.services.messenger.v1.events.Event.f;
import com.truecaller.api.services.messenger.v1.events.Event.n;
import com.truecaller.api.services.messenger.v1.events.Event.p;
import com.truecaller.api.services.messenger.v1.events.Event.x;
import com.truecaller.api.services.messenger.v1.k.a;
import com.truecaller.api.services.messenger.v1.models.Peer;
import com.truecaller.api.services.messenger.v1.models.Peer.TypeCase;
import com.truecaller.api.services.messenger.v1.models.Peer.b;
import com.truecaller.api.services.messenger.v1.models.Peer.d;
import com.truecaller.api.services.messenger.v1.models.input.InputNotificationScope;
import com.truecaller.api.services.messenger.v1.models.input.InputNotificationScope.a;
import com.truecaller.api.services.messenger.v1.models.input.InputNotificationSettings;
import com.truecaller.api.services.messenger.v1.models.input.InputNotificationSettings.a;
import com.truecaller.api.services.messenger.v1.models.input.InputNotificationSettings.b;
import com.truecaller.api.services.messenger.v1.models.input.InputNotificationSettings.d;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer.a;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer.d;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer.d.a;
import com.truecaller.api.services.messenger.v1.models.input.a.a;
import com.truecaller.api.services.messenger.v1.n.b;
import com.truecaller.api.services.messenger.v1.n.b.a;
import com.truecaller.api.services.messenger.v1.n.d;
import com.truecaller.api.services.messenger.v1.v.b;
import com.truecaller.api.services.messenger.v1.v.b.a;
import com.truecaller.api.services.messenger.v1.v.d;
import com.truecaller.api.services.messenger.v1.x.b;
import com.truecaller.api.services.messenger.v1.x.b.a;
import com.truecaller.api.services.messenger.v1.z.b;
import com.truecaller.api.services.messenger.v1.z.b.a;
import com.truecaller.api.services.messenger.v1.z.d;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.r;
import com.truecaller.content.TruecallerContract.s;
import com.truecaller.content.TruecallerContract.t;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.data.types.ImGroupPermissions;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Participant.a;
import com.truecaller.messaging.transport.im.bp;
import com.truecaller.messaging.transport.im.cb;
import com.truecaller.network.d.j.a;
import com.truecaller.tracking.events.q;
import com.truecaller.tracking.events.q.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import org.apache.a.d.d;

public final class c
  implements a
{
  private final cb a;
  private final ContentResolver b;
  private final com.truecaller.messaging.data.c c;
  private final com.truecaller.messaging.h d;
  private final com.truecaller.utils.a e;
  private final m f;
  private final com.truecaller.data.access.m g;
  private final com.truecaller.messaging.notifications.a h;
  private final com.truecaller.featuretoggles.e i;
  private final com.truecaller.androidactors.f<com.truecaller.messaging.data.t> j;
  private final com.truecaller.androidactors.f<bp> k;
  private final com.truecaller.androidactors.f<ae> l;
  private final com.truecaller.analytics.b m;
  
  @Inject
  public c(cb paramcb, ContentResolver paramContentResolver, com.truecaller.messaging.data.c paramc, com.truecaller.messaging.h paramh, com.truecaller.utils.a parama, m paramm, com.truecaller.data.access.m paramm1, com.truecaller.messaging.notifications.a parama1, com.truecaller.featuretoggles.e parame, com.truecaller.androidactors.f<com.truecaller.messaging.data.t> paramf, com.truecaller.androidactors.f<bp> paramf1, com.truecaller.androidactors.f<ae> paramf2, com.truecaller.analytics.b paramb)
  {
    a = paramcb;
    b = paramContentResolver;
    c = paramc;
    d = paramh;
    e = parama;
    f = paramm;
    g = paramm1;
    h = parama1;
    i = parame;
    j = paramf;
    k = paramf1;
    l = paramf2;
    m = paramb;
  }
  
  private final Participant a(Map<Participant, Integer> paramMap, ImGroupInfo paramImGroupInfo)
  {
    Participant localParticipant = new Participant.a(4).b(a).a();
    c.g.b.k.a(localParticipant, "Builder(ParticipantTable…pId)\n            .build()");
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(ContentProviderOperation.newInsert(TruecallerContract.r.a()).withValues(e.a(paramImGroupInfo)).build());
    localArrayList.add(ContentProviderOperation.newDelete(TruecallerContract.s.a()).withSelection("im_group_id = ?", new String[] { a }).build());
    a(localArrayList, paramMap, a);
    a(localArrayList);
    return localParticipant;
  }
  
  /* Error */
  @SuppressLint({"Recycle"})
  private final List<Participant> a(List<? extends Participant> paramList)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokeinterface 187 1 0
    //   6: ifeq +7 -> 13
    //   9: invokestatic 193	java/util/Collections:emptyList	()Ljava/util/List;
    //   12: areturn
    //   13: aload_1
    //   14: checkcast 195	java/lang/Iterable
    //   17: astore_3
    //   18: new 123	java/util/ArrayList
    //   21: dup
    //   22: aload_3
    //   23: bipush 10
    //   25: invokestatic 200	c/a/m:a	(Ljava/lang/Iterable;I)I
    //   28: invokespecial 201	java/util/ArrayList:<init>	(I)V
    //   31: checkcast 203	java/util/Collection
    //   34: astore_1
    //   35: aload_3
    //   36: invokeinterface 207 1 0
    //   41: astore_3
    //   42: aload_3
    //   43: invokeinterface 212 1 0
    //   48: ifeq +25 -> 73
    //   51: aload_1
    //   52: aload_3
    //   53: invokeinterface 216 1 0
    //   58: checkcast 218	com/truecaller/messaging/data/types/Participant
    //   61: getfield 220	com/truecaller/messaging/data/types/Participant:f	Ljava/lang/String;
    //   64: invokeinterface 221 2 0
    //   69: pop
    //   70: goto -28 -> 42
    //   73: aload_1
    //   74: checkcast 183	java/util/List
    //   77: astore 6
    //   79: aload_0
    //   80: getfield 92	com/truecaller/messaging/transport/im/a/c:k	Lcom/truecaller/androidactors/f;
    //   83: invokeinterface 225 1 0
    //   88: checkcast 227	com/truecaller/messaging/transport/im/bp
    //   91: astore_1
    //   92: aload 6
    //   94: checkcast 203	java/util/Collection
    //   97: astore 5
    //   99: aload_1
    //   100: aload 5
    //   102: iconst_1
    //   103: invokeinterface 230 3 0
    //   108: invokevirtual 234	com/truecaller/androidactors/w:d	()Ljava/lang/Object;
    //   111: checkcast 236	java/lang/Boolean
    //   114: getstatic 240	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   117: invokestatic 243	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/Object;)Z
    //   120: istore_2
    //   121: aconst_null
    //   122: astore_3
    //   123: iload_2
    //   124: ifeq +276 -> 400
    //   127: aload_0
    //   128: getfield 74	com/truecaller/messaging/transport/im/a/c:b	Landroid/content/ContentResolver;
    //   131: astore_1
    //   132: invokestatic 246	com/truecaller/content/TruecallerContract$y:a	()Landroid/net/Uri;
    //   135: astore 4
    //   137: new 248	java/lang/StringBuilder
    //   140: dup
    //   141: ldc -6
    //   143: invokespecial 253	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   146: astore 7
    //   148: aload 7
    //   150: aload 6
    //   152: checkcast 195	java/lang/Iterable
    //   155: aconst_null
    //   156: aconst_null
    //   157: aconst_null
    //   158: iconst_0
    //   159: aconst_null
    //   160: getstatic 258	com/truecaller/messaging/transport/im/a/c$b:a	Lcom/truecaller/messaging/transport/im/a/c$b;
    //   163: checkcast 260	c/g/a/b
    //   166: bipush 31
    //   168: invokestatic 263	c/a/m:a	(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lc/g/a/b;I)Ljava/lang/String;
    //   171: invokevirtual 267	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   174: pop
    //   175: aload 7
    //   177: ldc_w 269
    //   180: invokevirtual 267	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   183: pop
    //   184: aload 7
    //   186: invokevirtual 273	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   189: astore 6
    //   191: aload 5
    //   193: iconst_0
    //   194: anewarray 164	java/lang/String
    //   197: invokeinterface 277 2 0
    //   202: astore 5
    //   204: aload 5
    //   206: ifnull +183 -> 389
    //   209: aload 5
    //   211: checkcast 279	[Ljava/lang/String;
    //   214: astore 5
    //   216: aload 5
    //   218: aload 5
    //   220: arraylength
    //   221: invokestatic 285	java/util/Arrays:copyOf	([Ljava/lang/Object;I)[Ljava/lang/Object;
    //   224: checkcast 279	[Ljava/lang/String;
    //   227: astore 5
    //   229: aload_1
    //   230: aload 4
    //   232: iconst_2
    //   233: anewarray 164	java/lang/String
    //   236: dup
    //   237: iconst_0
    //   238: ldc_w 287
    //   241: aastore
    //   242: dup
    //   243: iconst_1
    //   244: ldc_w 289
    //   247: aastore
    //   248: aload 6
    //   250: aload 5
    //   252: aconst_null
    //   253: invokevirtual 295	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   256: astore 5
    //   258: aload 5
    //   260: ifnull +127 -> 387
    //   263: aload 5
    //   265: checkcast 297	java/io/Closeable
    //   268: astore 4
    //   270: aload_3
    //   271: astore_1
    //   272: new 123	java/util/ArrayList
    //   275: dup
    //   276: invokespecial 124	java/util/ArrayList:<init>	()V
    //   279: checkcast 203	java/util/Collection
    //   282: astore 6
    //   284: aload_3
    //   285: astore_1
    //   286: aload 5
    //   288: invokeinterface 302 1 0
    //   293: ifeq +61 -> 354
    //   296: aload_3
    //   297: astore_1
    //   298: aload 5
    //   300: iconst_0
    //   301: invokeinterface 306 2 0
    //   306: astore 7
    //   308: aload_3
    //   309: astore_1
    //   310: aload 5
    //   312: iconst_1
    //   313: invokeinterface 306 2 0
    //   318: astore 8
    //   320: aload_3
    //   321: astore_1
    //   322: aload 6
    //   324: new 103	com/truecaller/messaging/data/types/Participant$a
    //   327: dup
    //   328: iconst_0
    //   329: invokespecial 106	com/truecaller/messaging/data/types/Participant$a:<init>	(I)V
    //   332: aload 7
    //   334: invokevirtual 114	com/truecaller/messaging/data/types/Participant$a:b	(Ljava/lang/String;)Lcom/truecaller/messaging/data/types/Participant$a;
    //   337: aload 8
    //   339: invokevirtual 308	com/truecaller/messaging/data/types/Participant$a:d	(Ljava/lang/String;)Lcom/truecaller/messaging/data/types/Participant$a;
    //   342: invokevirtual 117	com/truecaller/messaging/data/types/Participant$a:a	()Lcom/truecaller/messaging/data/types/Participant;
    //   345: invokeinterface 221 2 0
    //   350: pop
    //   351: goto -67 -> 284
    //   354: aload_3
    //   355: astore_1
    //   356: aload 6
    //   358: checkcast 183	java/util/List
    //   361: astore_3
    //   362: aload 4
    //   364: aconst_null
    //   365: invokestatic 313	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   368: aload_3
    //   369: areturn
    //   370: astore_3
    //   371: goto +8 -> 379
    //   374: astore_3
    //   375: aload_3
    //   376: astore_1
    //   377: aload_3
    //   378: athrow
    //   379: aload 4
    //   381: aload_1
    //   382: invokestatic 313	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   385: aload_3
    //   386: athrow
    //   387: aconst_null
    //   388: areturn
    //   389: new 315	c/u
    //   392: dup
    //   393: ldc_w 317
    //   396: invokespecial 318	c/u:<init>	(Ljava/lang/String;)V
    //   399: athrow
    //   400: aconst_null
    //   401: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	402	0	this	c
    //   0	402	1	paramList	List<? extends Participant>
    //   120	4	2	bool	boolean
    //   17	352	3	localObject1	Object
    //   370	1	3	localObject2	Object
    //   374	12	3	localThrowable	Throwable
    //   135	245	4	localObject3	Object
    //   97	214	5	localObject4	Object
    //   77	280	6	localObject5	Object
    //   146	187	7	localObject6	Object
    //   318	20	8	str	String
    // Exception table:
    //   from	to	target	type
    //   272	284	370	finally
    //   286	296	370	finally
    //   298	308	370	finally
    //   310	320	370	finally
    //   322	351	370	finally
    //   356	362	370	finally
    //   377	379	370	finally
    //   272	284	374	java/lang/Throwable
    //   286	296	374	java/lang/Throwable
    //   298	308	374	java/lang/Throwable
    //   310	320	374	java/lang/Throwable
    //   322	351	374	java/lang/Throwable
    //   356	362	374	java/lang/Throwable
  }
  
  private final void a(ArrayList<ContentProviderOperation> paramArrayList, String paramString1, String paramString2, int paramInt, ImGroupPermissions paramImGroupPermissions)
  {
    paramArrayList.add(ContentProviderOperation.newUpdate(TruecallerContract.s.a()).withValue("roles", Integer.valueOf(paramInt)).withSelection("im_group_id=? AND im_peer_id=?", new String[] { paramString1, paramString2 }).build());
    if (c.g.b.k.a(paramString2, d.G())) {
      paramArrayList.add(ContentProviderOperation.newUpdate(TruecallerContract.r.a()).withValue("roles", Integer.valueOf(paramInt)).withValues(e.a(paramImGroupPermissions)).withSelection("im_group_id = ?", new String[] { paramString1 }).build());
    }
  }
  
  private final void a(ArrayList<ContentProviderOperation> paramArrayList, List<? extends Participant> paramList, String paramString)
  {
    String str = d.G();
    Object localObject1 = (Iterable)paramList;
    paramList = (Collection)new ArrayList();
    localObject1 = ((Iterable)localObject1).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = nextd;
      if (localObject2 != null) {
        paramList.add(localObject2);
      }
    }
    Object localObject2 = (List)paramList;
    paramList = ContentProviderOperation.newDelete(TruecallerContract.s.a());
    Object localObject3 = new StringBuilder("im_group_id=? AND im_peer_id IN (");
    localObject1 = (Iterable)localObject2;
    ((StringBuilder)localObject3).append(c.a.m.a((Iterable)localObject1, null, null, null, 0, null, (c.g.a.b)c.a.a, 31));
    ((StringBuilder)localObject3).append(')');
    localObject3 = ((StringBuilder)localObject3).toString();
    localObject2 = ((Collection)localObject2).toArray(new String[0]);
    if (localObject2 != null)
    {
      paramArrayList.add(paramList.withSelection((String)localObject3, (String[])c.a.f.a(new String[] { paramString }, (Object[])localObject2)).build());
      if (c.a.m.a((Iterable)localObject1, str)) {
        paramArrayList.add(ContentProviderOperation.newUpdate(TruecallerContract.r.a()).withValue("roles", Integer.valueOf(0)).withValues(e.a(e.a())).withSelection("im_group_id = ?", new String[] { paramString }).build());
      }
      return;
    }
    throw new u("null cannot be cast to non-null type kotlin.Array<T>");
  }
  
  private static void a(ArrayList<ContentProviderOperation> paramArrayList, Map<Participant, Integer> paramMap, String paramString)
  {
    paramMap = paramMap.entrySet().iterator();
    while (paramMap.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)paramMap.next();
      Participant localParticipant = (Participant)localEntry.getKey();
      int n = ((Number)localEntry.getValue()).intValue();
      paramArrayList.add(ContentProviderOperation.newInsert(TruecallerContract.s.a()).withValue("im_group_id", paramString).withValue("im_peer_id", d).withValue("roles", Integer.valueOf(n)).build());
    }
  }
  
  private final void a(Map<String, com.truecaller.api.services.messenger.v1.models.k> paramMap)
  {
    Iterator localIterator = paramMap.entrySet().iterator();
    while (localIterator.hasNext())
    {
      paramMap = (Map.Entry)localIterator.next();
      String str = (String)paramMap.getKey();
      com.truecaller.api.services.messenger.v1.models.k localk = (com.truecaller.api.services.messenger.v1.models.k)paramMap.getValue();
      Contact localContact = g.a(localk.c());
      paramMap = localContact;
      if (localContact == null)
      {
        paramMap = new Contact();
        paramMap.setTcId(localk.c());
        paramMap.setSource(1);
        paramMap.a(0L);
        paramMap.b("private");
      }
      paramMap.l(localk.a());
      paramMap.j(localk.b());
      paramMap.c(str);
      g.a(paramMap);
    }
  }
  
  private final boolean a(String paramString, List<? extends Participant> paramList, boolean paramBoolean)
  {
    if (paramList.isEmpty()) {
      return true;
    }
    Object localObject1 = (k.a)j.a.a(a);
    if (localObject1 == null) {
      return false;
    }
    try
    {
      Object localObject2 = a.b.a();
      Object localObject4 = (Iterable)paramList;
      Object localObject3 = (Collection)new ArrayList(c.a.m.a((Iterable)localObject4, 10));
      localObject4 = ((Iterable)localObject4).iterator();
      while (((Iterator)localObject4).hasNext()) {
        ((Collection)localObject3).add(e.a((Participant)((Iterator)localObject4).next()));
      }
      localObject2 = ((a.b.a)localObject2).a((Iterable)localObject3).a(e.b(paramString));
      localObject3 = c.j.c.c;
      localObject2 = ((k.a)localObject1).a((a.b)((a.b.a)localObject2).a(c.j.c.d().c()).build());
      c.g.b.k.a(localObject2, "stub.addParticipants(it)");
      c.g.b.k.a(localObject2, "AddParticipants.Request.…tub.addParticipants(it) }");
      localObject1 = ((a.d)localObject2).b();
      c.g.b.k.a(localObject1, "response.invalidPeersList");
      localObject3 = (Iterable)localObject1;
      localObject1 = (Collection)new ArrayList(c.a.m.a((Iterable)localObject3, 10));
      localObject3 = ((Iterable)localObject3).iterator();
      while (((Iterator)localObject3).hasNext())
      {
        localObject4 = (Peer)((Iterator)localObject3).next();
        c.g.b.k.a(localObject4, "it");
        localObject4 = ((Peer)localObject4).b();
        c.g.b.k.a(localObject4, "it.user");
        ((Collection)localObject1).add(((Peer.d)localObject4).a());
      }
      localObject1 = c.a.m.i((Iterable)localObject1);
      paramList = (Iterable)paramList;
      localObject3 = (Collection)new ArrayList();
      localObject4 = paramList.iterator();
      while (((Iterator)localObject4).hasNext())
      {
        localObject5 = ((Iterator)localObject4).next();
        if ((((Set)localObject1).contains(d) ^ true)) {
          ((Collection)localObject3).add(localObject5);
        }
      }
      localObject4 = (Iterable)localObject3;
      localObject3 = new LinkedHashMap(c.k.i.c(ag.a(c.a.m.a((Iterable)localObject4, 10)), 16));
      localObject4 = ((Iterable)localObject4).iterator();
      while (((Iterator)localObject4).hasNext())
      {
        localObject5 = ((Iterator)localObject4).next();
        ((Map)localObject3).put(localObject5, Integer.valueOf(((a.d)localObject2).c()));
      }
      localObject3 = (Map)localObject3;
      localObject4 = new ArrayList();
      a((ArrayList)localObject4, (Map)localObject3, paramString);
      a((ArrayList)localObject4);
      localObject3 = f;
      localObject2 = ((a.d)localObject2).a();
      c.g.b.k.a(localObject2, "response.messageId");
      localObject4 = (Collection)new ArrayList();
      Object localObject5 = paramList.iterator();
      while (((Iterator)localObject5).hasNext())
      {
        String str = nextd;
        if (str != null) {
          ((Collection)localObject4).add(str);
        }
      }
      ((m)localObject3).a(paramString, (String)localObject2, (List)localObject4);
      if (paramBoolean)
      {
        localObject2 = (Collection)new ArrayList();
        paramList = paramList.iterator();
        while (paramList.hasNext())
        {
          localObject3 = paramList.next();
          if (((Set)localObject1).contains(d)) {
            ((Collection)localObject2).add(localObject3);
          }
        }
        paramList = a((List)localObject2);
        if (paramList != null) {
          a(paramString, paramList, false);
        }
      }
      return true;
    }
    catch (RuntimeException paramString) {}
    return false;
  }
  
  private final ContentProviderResult[] a(ArrayList<ContentProviderOperation> paramArrayList)
  {
    try
    {
      paramArrayList = b.applyBatch(TruecallerContract.a(), paramArrayList);
      c.g.b.k.a(paramArrayList, "contentResolver.applyBat…tAuthority(), operations)");
      return paramArrayList;
    }
    catch (OperationApplicationException paramArrayList)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramArrayList);
    }
    catch (RemoteException paramArrayList)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramArrayList);
    }
    return new ContentProviderResult[0];
  }
  
  private final n.d b(String paramString, Participant paramParticipant)
  {
    Object localObject1 = (k.a)j.a.a(a);
    if (localObject1 == null) {
      return null;
    }
    try
    {
      Object localObject2 = n.b.a().b(e.a(paramParticipant)).a(e.b(paramString));
      c.b localb = c.j.c.c;
      localObject1 = ((k.a)localObject1).a((n.b)((n.b.a)localObject2).a(c.j.c.d().c()).build());
      c.g.b.k.a(localObject1, "stub.removeParticipants(it)");
      c.g.b.k.a(localObject1, "RemoveParticipants.Reque….removeParticipants(it) }");
      localObject2 = new ArrayList();
      a((ArrayList)localObject2, c.a.m.a(paramParticipant), paramString);
      a((ArrayList)localObject2);
      return (n.d)localObject1;
    }
    catch (RuntimeException paramString) {}
    return null;
  }
  
  private final z.d b(String paramString1, String paramString2, int paramInt)
  {
    Object localObject1 = (k.a)j.a.a(a);
    if (localObject1 == null) {
      return null;
    }
    try
    {
      Object localObject2 = z.b.a().a(e.b(paramString1)).a(InputPeer.a().a(InputPeer.d.a().a(paramString2))).a(paramInt);
      Object localObject3 = c.j.c.c;
      localObject1 = ((k.a)localObject1).a((z.b)((z.b.a)localObject2).a(c.j.c.d().c()).build());
      c.g.b.k.a(localObject1, "stub.updateRoles(it)");
      c.g.b.k.a(localObject1, "UpdateRoles.Request.newB… { stub.updateRoles(it) }");
      localObject2 = new ArrayList();
      localObject3 = ((z.d)localObject1).b();
      c.g.b.k.a(localObject3, "response.permissions");
      a((ArrayList)localObject2, paramString1, paramString2, paramInt, e.a((com.truecaller.api.services.messenger.v1.models.a)localObject3));
      a((ArrayList)localObject2);
      return (z.d)localObject1;
    }
    catch (RuntimeException paramString1) {}
    return null;
  }
  
  private final boolean b(String paramString1, String paramString2, String paramString3)
  {
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("title", paramString2);
    localContentValues.put("avatar", paramString3);
    return b.update(TruecallerContract.r.a(), localContentValues, "im_group_id=?", new String[] { paramString1 }) > 0;
  }
  
  private final void c(String paramString1, String paramString2, String paramString3)
  {
    paramString1 = q.b().a((CharSequence)paramString1).b((CharSequence)paramString2).c((CharSequence)paramString3).d((CharSequence)"Receive");
    ((ae)l.a()).a((d)paramString1.a());
    paramString2 = new e.a("IMGroupInvite").a("action", "Receive");
    paramString1 = m;
    paramString2 = paramString2.a();
    c.g.b.k.a(paramString2, "it.build()");
    paramString1.b(paramString2);
  }
  
  private final void c(String paramString, boolean paramBoolean)
  {
    paramString = g(paramString);
    if (paramString != null)
    {
      long l1 = ((Number)paramString).longValue();
      ((com.truecaller.messaging.data.t)j.a()).a(l1, 1, 0, paramBoolean).d();
      return;
    }
  }
  
  /* Error */
  private final Long g(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 74	com/truecaller/messaging/transport/im/a/c:b	Landroid/content/ContentResolver;
    //   4: invokestatic 741	com/truecaller/content/TruecallerContract$i:a	()Landroid/net/Uri;
    //   7: iconst_1
    //   8: anewarray 164	java/lang/String
    //   11: dup
    //   12: iconst_0
    //   13: ldc_w 743
    //   16: aastore
    //   17: ldc_w 745
    //   20: iconst_1
    //   21: anewarray 164	java/lang/String
    //   24: dup
    //   25: iconst_0
    //   26: aload_1
    //   27: aastore
    //   28: aconst_null
    //   29: invokevirtual 295	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   32: astore_1
    //   33: aconst_null
    //   34: astore_2
    //   35: aload_1
    //   36: ifnull +72 -> 108
    //   39: aload_1
    //   40: checkcast 297	java/io/Closeable
    //   43: astore_3
    //   44: aload_2
    //   45: astore_1
    //   46: aload_3
    //   47: checkcast 299	android/database/Cursor
    //   50: astore 4
    //   52: aload_2
    //   53: astore_1
    //   54: aload 4
    //   56: invokeinterface 748 1 0
    //   61: ifeq +22 -> 83
    //   64: aload_2
    //   65: astore_1
    //   66: aload 4
    //   68: iconst_0
    //   69: invokeinterface 752 2 0
    //   74: invokestatic 757	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   77: astore_2
    //   78: aload_2
    //   79: astore_1
    //   80: goto +5 -> 85
    //   83: aconst_null
    //   84: astore_1
    //   85: aload_3
    //   86: aconst_null
    //   87: invokestatic 313	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   90: aload_1
    //   91: areturn
    //   92: astore_2
    //   93: goto +8 -> 101
    //   96: astore_2
    //   97: aload_2
    //   98: astore_1
    //   99: aload_2
    //   100: athrow
    //   101: aload_3
    //   102: aload_1
    //   103: invokestatic 313	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   106: aload_2
    //   107: athrow
    //   108: aconst_null
    //   109: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	110	0	this	c
    //   0	110	1	paramString	String
    //   34	45	2	localLong	Long
    //   92	1	2	localObject	Object
    //   96	11	2	localThrowable	Throwable
    //   43	59	3	localCloseable	java.io.Closeable
    //   50	17	4	localCursor	android.database.Cursor
    // Exception table:
    //   from	to	target	type
    //   46	52	92	finally
    //   54	64	92	finally
    //   66	78	92	finally
    //   99	101	92	finally
    //   46	52	96	java/lang/Throwable
    //   54	64	96	java/lang/Throwable
    //   66	78	96	java/lang/Throwable
  }
  
  private final void h(String paramString)
  {
    paramString = (ImGroupInfo)e(paramString).d();
    if (paramString != null)
    {
      h.a(paramString);
      return;
    }
  }
  
  private final boolean i(String paramString)
  {
    if (!i.i().a()) {
      return false;
    }
    ContentResolver localContentResolver = b;
    Uri localUri = TruecallerContract.r.a();
    c.g.b.k.a(localUri, "ImGroupInfoTable.getContentUri()");
    paramString = com.truecaller.utils.extensions.h.a(localContentResolver, localUri, "roles", "im_group_id = ?", new String[] { paramString });
    if (paramString != null) {
      return paramString.intValue() == 0;
    }
    return true;
  }
  
  /* Error */
  @SuppressLint({"Recycle"})
  public final w<Collection<String>> a()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 74	com/truecaller/messaging/transport/im/a/c:b	Landroid/content/ContentResolver;
    //   4: invokestatic 129	com/truecaller/content/TruecallerContract$r:a	()Landroid/net/Uri;
    //   7: iconst_1
    //   8: anewarray 164	java/lang/String
    //   11: dup
    //   12: iconst_0
    //   13: ldc_w 664
    //   16: aastore
    //   17: aconst_null
    //   18: aconst_null
    //   19: ldc_w 786
    //   22: invokevirtual 295	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   25: astore 4
    //   27: aconst_null
    //   28: astore_2
    //   29: aload 4
    //   31: ifnull +90 -> 121
    //   34: aload 4
    //   36: checkcast 297	java/io/Closeable
    //   39: astore_3
    //   40: aload_2
    //   41: astore_1
    //   42: new 123	java/util/ArrayList
    //   45: dup
    //   46: invokespecial 124	java/util/ArrayList:<init>	()V
    //   49: checkcast 203	java/util/Collection
    //   52: astore 5
    //   54: aload_2
    //   55: astore_1
    //   56: aload 4
    //   58: invokeinterface 302 1 0
    //   63: ifeq +24 -> 87
    //   66: aload_2
    //   67: astore_1
    //   68: aload 5
    //   70: aload 4
    //   72: iconst_0
    //   73: invokeinterface 306 2 0
    //   78: invokeinterface 221 2 0
    //   83: pop
    //   84: goto -30 -> 54
    //   87: aload_2
    //   88: astore_1
    //   89: aload 5
    //   91: checkcast 183	java/util/List
    //   94: astore_2
    //   95: aload_3
    //   96: aconst_null
    //   97: invokestatic 313	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   100: aload_2
    //   101: astore_1
    //   102: goto +21 -> 123
    //   105: astore_2
    //   106: goto +8 -> 114
    //   109: astore_2
    //   110: aload_2
    //   111: astore_1
    //   112: aload_2
    //   113: athrow
    //   114: aload_3
    //   115: aload_1
    //   116: invokestatic 313	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   119: aload_2
    //   120: athrow
    //   121: aconst_null
    //   122: astore_1
    //   123: aload_1
    //   124: ifnull +6 -> 130
    //   127: goto +7 -> 134
    //   130: invokestatic 193	java/util/Collections:emptyList	()Ljava/util/List;
    //   133: astore_1
    //   134: aload_1
    //   135: checkcast 203	java/util/Collection
    //   138: invokestatic 789	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   141: astore_1
    //   142: aload_1
    //   143: ldc_w 791
    //   146: invokestatic 121	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   149: aload_1
    //   150: ldc_w 793
    //   153: invokestatic 121	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   156: aload_1
    //   157: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	158	0	this	c
    //   41	116	1	localObject1	Object
    //   28	73	2	localList	List
    //   105	1	2	localObject2	Object
    //   109	11	2	localThrowable	Throwable
    //   39	76	3	localCloseable	java.io.Closeable
    //   25	46	4	localCursor	android.database.Cursor
    //   52	38	5	localCollection	Collection
    // Exception table:
    //   from	to	target	type
    //   42	54	105	finally
    //   56	66	105	finally
    //   68	84	105	finally
    //   89	95	105	finally
    //   112	114	105	finally
    //   42	54	109	java/lang/Throwable
    //   56	66	109	java/lang/Throwable
    //   68	84	109	java/lang/Throwable
    //   89	95	109	java/lang/Throwable
  }
  
  @SuppressLint({"Recycle"})
  public final w<g> a(String paramString)
  {
    c.g.b.k.b(paramString, "groupId");
    paramString = b.query(TruecallerContract.t.a(paramString, d.G()), null, null, null, "is_self DESC, roles DESC, name IS NULL ASC, name COLLATE NOCASE ASC");
    paramString = w.b(c.i(paramString));
    c.g.b.k.a(paramString, "Promise.wrap(it)");
    c.g.b.k.a(paramString, "contentResolver.query(\n ….let { Promise.wrap(it) }");
    return paramString;
  }
  
  public final w<Boolean> a(String paramString, int paramInt)
  {
    c.g.b.k.b(paramString, "groupId");
    Object localObject1 = (k.a)j.a.a(a);
    if (localObject1 == null)
    {
      paramString = w.b(Boolean.FALSE);
      c.g.b.k.a(paramString, "Promise.wrap(false)");
      return paramString;
    }
    Object localObject2 = InputNotificationSettings.a();
    switch (paramInt)
    {
    default: 
      throw ((Throwable)new IllegalArgumentException("Unknown notification settings: ".concat(String.valueOf(paramInt))));
    case 1: 
      ((InputNotificationSettings.a)localObject2).a(InputNotificationSettings.d.a());
      break;
    case 0: 
      ((InputNotificationSettings.a)localObject2).a(InputNotificationSettings.b.a());
    }
    try
    {
      ((k.a)localObject1).a((x.b)x.b.a().a((InputNotificationScope)InputNotificationScope.a().a(paramString).build()).a((InputNotificationSettings.a)localObject2).build());
      localObject1 = b;
      localObject2 = TruecallerContract.r.a();
      ContentValues localContentValues = new ContentValues();
      localContentValues.put("notification_settings", Integer.valueOf(paramInt));
      ((ContentResolver)localObject1).update((Uri)localObject2, localContentValues, "im_group_id = ?", new String[] { paramString });
      paramString = w.b(Boolean.TRUE);
      c.g.b.k.a(paramString, "Promise.wrap(true)");
      return paramString;
    }
    catch (RuntimeException paramString)
    {
      for (;;) {}
    }
    paramString = w.b(Boolean.FALSE);
    c.g.b.k.a(paramString, "Promise.wrap(false)");
    return paramString;
  }
  
  public final w<Boolean> a(String paramString, Participant paramParticipant)
  {
    c.g.b.k.b(paramString, "groupId");
    c.g.b.k.b(paramParticipant, "participant");
    Object localObject = b(paramString, paramParticipant);
    if (localObject == null)
    {
      paramString = w.b(Boolean.FALSE);
      c.g.b.k.a(paramString, "Promise.wrap(false)");
      return paramString;
    }
    paramParticipant = d;
    if (paramParticipant != null)
    {
      m localm = f;
      localObject = ((n.d)localObject).a();
      c.g.b.k.a(localObject, "response.messageId");
      c.g.b.k.a(paramParticipant, "imPeerId");
      localm.c(paramString, (String)localObject, paramParticipant);
    }
    paramString = w.b(Boolean.TRUE);
    c.g.b.k.a(paramString, "Promise.wrap(true)");
    return paramString;
  }
  
  public final w<Boolean> a(String paramString1, String paramString2, int paramInt)
  {
    c.g.b.k.b(paramString1, "groupId");
    c.g.b.k.b(paramString2, "imPeerId");
    Object localObject = b(paramString1, paramString2, paramInt);
    if (localObject == null)
    {
      paramString1 = w.b(Boolean.FALSE);
      c.g.b.k.a(paramString1, "Promise.wrap(false)");
      return paramString1;
    }
    m localm = f;
    localObject = ((z.d)localObject).a();
    c.g.b.k.a(localObject, "response.messageId");
    localm.a(paramString1, (String)localObject, paramInt, paramString2);
    paramString1 = w.b(Boolean.TRUE);
    c.g.b.k.a(paramString1, "Promise.wrap(true)");
    return paramString1;
  }
  
  @SuppressLint({"CheckResult"})
  public final w<Boolean> a(String paramString1, String paramString2, String paramString3)
  {
    c.g.b.k.b(paramString1, "groupId");
    c.g.b.k.b(paramString2, "title");
    Object localObject1 = (k.a)j.a.a(a);
    if (localObject1 == null)
    {
      paramString1 = w.b(Boolean.FALSE);
      c.g.b.k.a(paramString1, "Promise.wrap(false)");
      return paramString1;
    }
    Object localObject2 = v.b.a().a(paramString1);
    Object localObject3 = com.truecaller.api.services.messenger.v1.models.input.a.a().a(paramString2);
    if (paramString3 != null) {
      ((a.a)localObject3).b(paramString3);
    }
    localObject2 = ((v.b.a)localObject2).a((a.a)localObject3);
    localObject3 = c.j.c.c;
    localObject2 = (v.b)((v.b.a)localObject2).a(c.j.c.d().c()).build();
    try
    {
      localObject3 = ((k.a)localObject1).a((v.b)localObject2);
      ImGroupInfo localImGroupInfo = (ImGroupInfo)e(paramString1).d();
      c.g.b.k.a(localObject3, "response");
      localObject1 = ((v.d)localObject3).b();
      c.g.b.k.a(localObject1, "response.groupInfo");
      localObject1 = ((com.truecaller.api.services.messenger.v1.models.c)localObject1).a();
      c.g.b.k.a(localObject1, "response.groupInfo.title");
      localObject2 = ((v.d)localObject3).b();
      c.g.b.k.a(localObject2, "response.groupInfo");
      if (!b(paramString1, (String)localObject1, ((com.truecaller.api.services.messenger.v1.models.c)localObject2).b()))
      {
        paramString1 = w.b(Boolean.FALSE);
        c.g.b.k.a(paramString1, "Promise.wrap(false)");
        return paramString1;
      }
      localObject2 = null;
      if (localImGroupInfo != null) {
        localObject1 = b;
      } else {
        localObject1 = null;
      }
      if ((c.g.b.k.a(localObject1, paramString2) ^ true))
      {
        localObject1 = f;
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append(((v.d)localObject3).a());
        localStringBuilder.append("-title");
        ((m)localObject1).e(paramString1, localStringBuilder.toString(), paramString2);
      }
      paramString2 = (String)localObject2;
      if (localImGroupInfo != null) {
        paramString2 = c;
      }
      if ((c.g.b.k.a(paramString2, paramString3) ^ true))
      {
        paramString2 = f;
        paramString3 = new StringBuilder();
        paramString3.append(((v.d)localObject3).a());
        paramString3.append("-avatar");
        paramString2.c(paramString1, paramString3.toString());
      }
      paramString1 = w.b(Boolean.TRUE);
      c.g.b.k.a(paramString1, "Promise.wrap(true)");
      return paramString1;
    }
    catch (RuntimeException paramString1)
    {
      for (;;) {}
    }
    paramString1 = w.b(Boolean.FALSE);
    c.g.b.k.a(paramString1, "Promise.wrap(false)");
    return paramString1;
  }
  
  public final w<Boolean> a(String paramString, List<? extends Participant> paramList)
  {
    c.g.b.k.b(paramString, "groupId");
    c.g.b.k.b(paramList, "participants");
    paramString = w.b(Boolean.valueOf(a(paramString, paramList, true)));
    c.g.b.k.a(paramString, "Promise.wrap(addParticip…pId, participants, true))");
    return paramString;
  }
  
  public final w<Boolean> a(String paramString, boolean paramBoolean)
  {
    c.g.b.k.b(paramString, "groupId");
    Object localObject1 = d.G();
    if (localObject1 == null)
    {
      paramString = w.b(Boolean.FALSE);
      c.g.b.k.a(paramString, "Promise.wrap(false)");
      return paramString;
    }
    Object localObject2 = b(paramString, e.a((String)localObject1));
    if (localObject2 == null)
    {
      paramString = w.b(Boolean.FALSE);
      c.g.b.k.a(paramString, "Promise.wrap(false)");
      return paramString;
    }
    if (paramBoolean)
    {
      c(paramString, false);
    }
    else
    {
      localObject1 = f;
      localObject2 = ((n.d)localObject2).a();
      c.g.b.k.a(localObject2, "response.messageId");
      ((m)localObject1).a(paramString, (String)localObject2);
    }
    paramString = w.b(Boolean.TRUE);
    c.g.b.k.a(paramString, "Promise.wrap(true)");
    return paramString;
  }
  
  public final w<Participant> a(List<? extends Participant> paramList, String paramString1, String paramString2)
  {
    c.g.b.k.b(paramList, "participants");
    c.g.b.k.b(paramString1, "title");
    Object localObject2 = (k.a)j.a.a(a);
    if (localObject2 == null)
    {
      paramList = w.b(null);
      c.g.b.k.a(paramList, "Promise.wrap(null)");
      return paramList;
    }
    Object localObject1 = d.G();
    if (localObject1 == null)
    {
      paramList = w.b(null);
      c.g.b.k.a(paramList, "Promise.wrap(null)");
      return paramList;
    }
    Object localObject3 = com.truecaller.api.services.messenger.v1.models.input.a.a().a(paramString1);
    if (paramString2 != null) {
      ((a.a)localObject3).b(paramString2);
    }
    paramString2 = d.b.a();
    Object localObject4 = c.j.c.c;
    paramString2 = paramString2.a(c.j.c.d().c()).a((a.a)localObject3);
    paramList = (Iterable)paramList;
    localObject3 = (Collection)new ArrayList(c.a.m.a(paramList, 10));
    localObject4 = paramList.iterator();
    while (((Iterator)localObject4).hasNext()) {
      ((Collection)localObject3).add(e.a((Participant)((Iterator)localObject4).next()));
    }
    paramString2 = paramString2.a((Iterable)localObject3).build();
    c.g.b.k.a(paramString2, "Request.newBuilder()\n   …) })\n            .build()");
    paramString2 = (d.b)paramString2;
    try
    {
      paramString2 = ((k.a)localObject2).a(paramString2);
      c.g.b.k.a(paramString2, "stub.createGroup(request)");
      localObject2 = paramString2.c();
      c.g.b.k.a(localObject2, "response.invalidPeersList");
      localObject3 = (Iterable)localObject2;
      localObject2 = (Collection)new ArrayList(c.a.m.a((Iterable)localObject3, 10));
      localObject3 = ((Iterable)localObject3).iterator();
      while (((Iterator)localObject3).hasNext())
      {
        localObject4 = (Peer)((Iterator)localObject3).next();
        c.g.b.k.a(localObject4, "it");
        localObject4 = ((Peer)localObject4).b();
        c.g.b.k.a(localObject4, "it.user");
        ((Collection)localObject2).add(((Peer.d)localObject4).a());
      }
      localObject2 = c.a.m.i((Iterable)localObject2);
      localObject3 = (Collection)new ArrayList();
      localObject4 = paramList.iterator();
      while (((Iterator)localObject4).hasNext())
      {
        localObject5 = ((Iterator)localObject4).next();
        if ((((Set)localObject2).contains(d) ^ true)) {
          ((Collection)localObject3).add(localObject5);
        }
      }
      localObject4 = (Iterable)localObject3;
      localObject3 = new LinkedHashMap(c.k.i.c(ag.a(c.a.m.a((Iterable)localObject4, 10)), 16));
      localObject4 = ((Iterable)localObject4).iterator();
      while (((Iterator)localObject4).hasNext())
      {
        localObject5 = ((Iterator)localObject4).next();
        ((Map)localObject3).put(localObject5, Integer.valueOf(paramString2.e()));
      }
      localObject3 = ag.a((Map)localObject3, c.t.a(e.a((String)localObject1), Integer.valueOf(paramString2.d())));
      localObject4 = f;
      Object localObject5 = paramString2.a();
      c.g.b.k.a(localObject5, "response.groupId");
      Object localObject6 = paramString2.b();
      c.g.b.k.a(localObject6, "response.messageId");
      ((m)localObject4).b((String)localObject5, (String)localObject6, paramString1);
      paramString1 = paramString2.a();
      c.g.b.k.a(paramString1, "response.groupId");
      localObject4 = paramString2.g();
      c.g.b.k.a(localObject4, "response.groupInfo");
      localObject4 = ((com.truecaller.api.services.messenger.v1.models.c)localObject4).a();
      localObject5 = paramString2.g();
      c.g.b.k.a(localObject5, "response.groupInfo");
      localObject5 = ((com.truecaller.api.services.messenger.v1.models.c)localObject5).b();
      long l1 = e.a();
      int n = paramString2.d();
      localObject6 = paramString2.f();
      c.g.b.k.a(localObject6, "response.permissions");
      paramString1 = a((Map)localObject3, new ImGroupInfo(paramString1, (String)localObject4, (String)localObject5, l1, (String)localObject1, n, e.a((com.truecaller.api.services.messenger.v1.models.a)localObject6), 0));
      localObject1 = (Collection)new ArrayList();
      paramList = paramList.iterator();
      while (paramList.hasNext())
      {
        localObject3 = paramList.next();
        if (((Set)localObject2).contains(d)) {
          ((Collection)localObject1).add(localObject3);
        }
      }
      paramList = a((List)localObject1);
      if (paramList != null)
      {
        paramString2 = paramString2.a();
        c.g.b.k.a(paramString2, "response.groupId");
        a(paramString2, paramList, false);
      }
      paramList = w.b(paramString1);
      c.g.b.k.a(paramList, "Promise.wrap(groupParticipant)");
      return paramList;
    }
    catch (RuntimeException paramList)
    {
      for (;;) {}
    }
    paramList = w.b(null);
    c.g.b.k.a(paramList, "Promise.wrap(null)");
    return paramList;
  }
  
  public final void a(Event.d paramd)
  {
    c.g.b.k.b(paramd, "event");
    Object localObject1 = paramd.f();
    c.g.b.k.a(localObject1, "event.userInfoMap");
    a((Map)localObject1);
    localObject1 = paramd.h();
    c.g.b.k.a(localObject1, "event.participantsList");
    Object localObject2 = (Iterable)localObject1;
    localObject1 = (Map)new LinkedHashMap(c.k.i.c(ag.a(c.a.m.a((Iterable)localObject2, 10)), 16));
    localObject2 = ((Iterable)localObject2).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject3 = (com.truecaller.api.services.messenger.v1.models.f)((Iterator)localObject2).next();
      c.g.b.k.a(localObject3, "it");
      localObject4 = ((com.truecaller.api.services.messenger.v1.models.f)localObject3).a();
      c.g.b.k.a(localObject4, "it.peer");
      localObject3 = c.t.a(com.truecaller.messaging.i.i.a((Peer)localObject4), Integer.valueOf(((com.truecaller.api.services.messenger.v1.models.f)localObject3).b()));
      ((Map)localObject1).put(a, b);
    }
    localObject2 = paramd.a();
    c.g.b.k.a(localObject2, "event.sender");
    Object localObject3 = ag.a((Map)localObject1, c.t.a(com.truecaller.messaging.i.i.a((Peer)localObject2), Integer.valueOf(paramd.g())));
    localObject1 = paramd.b();
    c.g.b.k.a(localObject1, "event.groupId");
    boolean bool = i((String)localObject1);
    localObject2 = d.G();
    localObject1 = paramd.h();
    c.g.b.k.a(localObject1, "event.participantsList");
    Object localObject4 = ((Iterable)localObject1).iterator();
    while (((Iterator)localObject4).hasNext())
    {
      localObject1 = ((Iterator)localObject4).next();
      localObject5 = (com.truecaller.api.services.messenger.v1.models.f)localObject1;
      c.g.b.k.a(localObject5, "it");
      localObject5 = ((com.truecaller.api.services.messenger.v1.models.f)localObject5).a();
      c.g.b.k.a(localObject5, "it.peer");
      localObject5 = ((Peer)localObject5).b();
      c.g.b.k.a(localObject5, "it.peer.user");
      if (c.g.b.k.a(((Peer.d)localObject5).a(), localObject2)) {
        break label345;
      }
    }
    localObject1 = null;
    label345:
    localObject4 = (com.truecaller.api.services.messenger.v1.models.f)localObject1;
    localObject1 = paramd.b();
    c.g.b.k.a(localObject1, "event.groupId");
    Object localObject5 = paramd.e();
    c.g.b.k.a(localObject5, "event.groupInfo");
    localObject5 = ((com.truecaller.api.services.messenger.v1.models.c)localObject5).a();
    Object localObject6 = paramd.e();
    c.g.b.k.a(localObject6, "event.groupInfo");
    localObject6 = ((com.truecaller.api.services.messenger.v1.models.c)localObject6).b();
    long l1 = TimeUnit.SECONDS.toMillis(paramd.d());
    Object localObject7 = paramd.a();
    c.g.b.k.a(localObject7, "event.sender");
    localObject7 = ((Peer)localObject7).b();
    c.g.b.k.a(localObject7, "event.sender.user");
    localObject7 = ((Peer.d)localObject7).a();
    int n;
    if (localObject4 != null) {
      n = ((com.truecaller.api.services.messenger.v1.models.f)localObject4).b();
    } else {
      n = 0;
    }
    localObject4 = paramd.i();
    c.g.b.k.a(localObject4, "event.permissions");
    a((Map)localObject3, new ImGroupInfo((String)localObject1, (String)localObject5, (String)localObject6, l1, (String)localObject7, n, e.a((com.truecaller.api.services.messenger.v1.models.a)localObject4), 0));
    localObject1 = f;
    localObject3 = paramd.b();
    c.g.b.k.a(localObject3, "event.groupId");
    localObject4 = paramd.c();
    c.g.b.k.a(localObject4, "event.messageId");
    localObject5 = paramd.a();
    c.g.b.k.a(localObject5, "event.sender");
    localObject5 = ((Peer)localObject5).b();
    c.g.b.k.a(localObject5, "event.sender.user");
    localObject5 = ((Peer.d)localObject5).a();
    c.g.b.k.a(localObject5, "event.sender.user.id");
    localObject6 = paramd.e();
    c.g.b.k.a(localObject6, "event.groupInfo");
    localObject6 = ((com.truecaller.api.services.messenger.v1.models.c)localObject6).a();
    c.g.b.k.a(localObject6, "event.groupInfo.title");
    ((m)localObject1).a((String)localObject3, (String)localObject4, (String)localObject5, (String)localObject6);
    if (bool)
    {
      localObject1 = paramd.b();
      c.g.b.k.a(localObject1, "event.groupId");
      h((String)localObject1);
      localObject1 = paramd.b();
      c.g.b.k.a(localObject1, "event.groupId");
      paramd = paramd.a();
      c.g.b.k.a(paramd, "event.sender");
      paramd = paramd.b();
      c.g.b.k.a(paramd, "event.sender.user");
      localObject3 = paramd.a();
      c.g.b.k.a(localObject3, "event.sender.user.id");
      paramd = (Event.d)localObject2;
      if (localObject2 == null) {
        paramd = "";
      }
      c((String)localObject1, (String)localObject3, paramd);
    }
  }
  
  public final void a(Event.f paramf)
  {
    c.g.b.k.b(paramf, "event");
    Object localObject1 = paramf.b();
    c.g.b.k.a(localObject1, "event.groupId");
    Object localObject3 = (ImGroupInfo)e((String)localObject1).d();
    localObject1 = paramf.b();
    c.g.b.k.a(localObject1, "event.groupId");
    Object localObject2 = paramf.d();
    c.g.b.k.a(localObject2, "event.groupInfo");
    localObject2 = ((com.truecaller.api.services.messenger.v1.models.c)localObject2).a();
    c.g.b.k.a(localObject2, "event.groupInfo.title");
    Object localObject4 = paramf.d();
    c.g.b.k.a(localObject4, "event.groupInfo");
    b((String)localObject1, (String)localObject2, ((com.truecaller.api.services.messenger.v1.models.c)localObject4).b());
    localObject2 = null;
    if (localObject3 != null) {
      localObject1 = b;
    } else {
      localObject1 = null;
    }
    localObject4 = paramf.d();
    c.g.b.k.a(localObject4, "event.groupInfo");
    if ((c.g.b.k.a(localObject1, ((com.truecaller.api.services.messenger.v1.models.c)localObject4).a()) ^ true))
    {
      localObject1 = f;
      localObject4 = paramf.b();
      c.g.b.k.a(localObject4, "event.groupId");
      Object localObject5 = new StringBuilder();
      ((StringBuilder)localObject5).append(paramf.c());
      ((StringBuilder)localObject5).append("-title");
      localObject5 = ((StringBuilder)localObject5).toString();
      Object localObject6 = paramf.a();
      c.g.b.k.a(localObject6, "event.sender");
      localObject6 = ((Peer)localObject6).b();
      c.g.b.k.a(localObject6, "event.sender.user");
      localObject6 = ((Peer.d)localObject6).a();
      c.g.b.k.a(localObject6, "event.sender.user.id");
      Object localObject7 = paramf.d();
      c.g.b.k.a(localObject7, "event.groupInfo");
      localObject7 = ((com.truecaller.api.services.messenger.v1.models.c)localObject7).a();
      c.g.b.k.a(localObject7, "event.groupInfo.title");
      ((m)localObject1).b((String)localObject4, (String)localObject5, (String)localObject6, (String)localObject7);
    }
    localObject1 = localObject2;
    if (localObject3 != null) {
      localObject1 = c;
    }
    localObject2 = paramf.d();
    c.g.b.k.a(localObject2, "event.groupInfo");
    if ((c.g.b.k.a(localObject1, ((com.truecaller.api.services.messenger.v1.models.c)localObject2).b()) ^ true))
    {
      localObject1 = f;
      localObject2 = paramf.b();
      c.g.b.k.a(localObject2, "event.groupId");
      localObject3 = new StringBuilder();
      ((StringBuilder)localObject3).append(paramf.c());
      ((StringBuilder)localObject3).append("-avatar");
      localObject3 = ((StringBuilder)localObject3).toString();
      paramf = paramf.a();
      c.g.b.k.a(paramf, "event.sender");
      paramf = paramf.b();
      c.g.b.k.a(paramf, "event.sender.user");
      paramf = paramf.a();
      c.g.b.k.a(paramf, "event.sender.user.id");
      ((m)localObject1).d((String)localObject2, (String)localObject3, paramf);
    }
  }
  
  public final void a(Event.n paramn)
  {
    c.g.b.k.b(paramn, "event");
    Object localObject1 = paramn.f();
    c.g.b.k.a(localObject1, "event.userInfoMap");
    a((Map)localObject1);
    Object localObject2 = d.G();
    localObject1 = paramn.g();
    c.g.b.k.a(localObject1, "event.participantsList");
    Object localObject3 = ((Iterable)localObject1).iterator();
    while (((Iterator)localObject3).hasNext())
    {
      localObject1 = ((Iterator)localObject3).next();
      localObject4 = (com.truecaller.api.services.messenger.v1.models.f)localObject1;
      c.g.b.k.a(localObject4, "it");
      localObject4 = ((com.truecaller.api.services.messenger.v1.models.f)localObject4).a();
      c.g.b.k.a(localObject4, "it.peer");
      localObject4 = ((Peer)localObject4).b();
      c.g.b.k.a(localObject4, "it.peer.user");
      if (c.g.b.k.a(((Peer.d)localObject4).a(), localObject2)) {
        break label147;
      }
    }
    localObject1 = null;
    label147:
    localObject1 = (com.truecaller.api.services.messenger.v1.models.f)localObject1;
    Object localObject6;
    if (localObject1 != null)
    {
      localObject3 = paramn.h();
      c.g.b.k.a(localObject3, "event.existingParticipantsList");
      localObject4 = (Iterable)localObject3;
      localObject3 = (Map)new LinkedHashMap(c.k.i.c(ag.a(c.a.m.a((Iterable)localObject4, 10)), 16));
      localObject4 = ((Iterable)localObject4).iterator();
      while (((Iterator)localObject4).hasNext())
      {
        localObject5 = (com.truecaller.api.services.messenger.v1.models.f)((Iterator)localObject4).next();
        c.g.b.k.a(localObject5, "it");
        localObject6 = ((com.truecaller.api.services.messenger.v1.models.f)localObject5).a();
        c.g.b.k.a(localObject6, "it.peer");
        localObject5 = c.t.a(com.truecaller.messaging.i.i.a((Peer)localObject6), Integer.valueOf(((com.truecaller.api.services.messenger.v1.models.f)localObject5).b()));
        ((Map)localObject3).put(a, b);
      }
      localObject4 = paramn.b();
      c.g.b.k.a(localObject4, "event.context");
      localObject4 = ((Peer)localObject4).c();
      c.g.b.k.a(localObject4, "event.context.group");
      localObject4 = ((Peer.b)localObject4).a();
      c.g.b.k.a(localObject4, "event.context.group.id");
      boolean bool = i((String)localObject4);
      localObject4 = paramn.b();
      c.g.b.k.a(localObject4, "event.context");
      localObject4 = ((Peer)localObject4).c();
      c.g.b.k.a(localObject4, "event.context.group");
      localObject4 = ((Peer.b)localObject4).a();
      c.g.b.k.a(localObject4, "event.context.group.id");
      localObject5 = paramn.e();
      c.g.b.k.a(localObject5, "event.groupInfo");
      localObject5 = ((com.truecaller.api.services.messenger.v1.models.c)localObject5).a();
      localObject6 = paramn.e();
      c.g.b.k.a(localObject6, "event.groupInfo");
      localObject6 = ((com.truecaller.api.services.messenger.v1.models.c)localObject6).b();
      long l1 = TimeUnit.SECONDS.toMillis(paramn.d());
      Object localObject7 = paramn.a();
      c.g.b.k.a(localObject7, "event.sender");
      localObject7 = ((Peer)localObject7).b();
      c.g.b.k.a(localObject7, "event.sender.user");
      localObject7 = ((Peer.d)localObject7).a();
      int n = ((com.truecaller.api.services.messenger.v1.models.f)localObject1).b();
      localObject1 = paramn.i();
      c.g.b.k.a(localObject1, "event.permissions");
      a((Map)localObject3, new ImGroupInfo((String)localObject4, (String)localObject5, (String)localObject6, l1, (String)localObject7, n, e.a((com.truecaller.api.services.messenger.v1.models.a)localObject1), 0));
      if (bool)
      {
        localObject1 = paramn.b();
        c.g.b.k.a(localObject1, "event.context");
        localObject1 = ((Peer)localObject1).c();
        c.g.b.k.a(localObject1, "event.context.group");
        localObject1 = ((Peer.b)localObject1).a();
        c.g.b.k.a(localObject1, "event.context.group.id");
        h((String)localObject1);
        localObject1 = paramn.b();
        c.g.b.k.a(localObject1, "event.context");
        localObject1 = ((Peer)localObject1).c();
        c.g.b.k.a(localObject1, "event.context.group");
        localObject3 = ((Peer.b)localObject1).a();
        c.g.b.k.a(localObject3, "event.context.group.id");
        localObject1 = paramn.a();
        c.g.b.k.a(localObject1, "event.sender");
        localObject1 = ((Peer)localObject1).b();
        c.g.b.k.a(localObject1, "event.sender.user");
        localObject4 = ((Peer.d)localObject1).a();
        c.g.b.k.a(localObject4, "event.sender.user.id");
        localObject1 = localObject2;
        if (localObject2 == null) {
          localObject1 = "";
        }
        c((String)localObject3, (String)localObject4, (String)localObject1);
      }
    }
    localObject1 = new ArrayList();
    localObject2 = ContentProviderOperation.newAssertQuery(TruecallerContract.r.a());
    localObject3 = paramn.b();
    c.g.b.k.a(localObject3, "event.context");
    localObject3 = ((Peer)localObject3).c();
    c.g.b.k.a(localObject3, "event.context.group");
    ((ArrayList)localObject1).add(((ContentProviderOperation.Builder)localObject2).withSelection("im_group_id=?", new String[] { ((Peer.b)localObject3).a() }).withExpectedCount(1).build());
    localObject2 = paramn.g();
    c.g.b.k.a(localObject2, "event.participantsList");
    localObject3 = (Iterable)localObject2;
    localObject2 = (Map)new LinkedHashMap(c.k.i.c(ag.a(c.a.m.a((Iterable)localObject3, 10)), 16));
    localObject3 = ((Iterable)localObject3).iterator();
    while (((Iterator)localObject3).hasNext())
    {
      localObject4 = (com.truecaller.api.services.messenger.v1.models.f)((Iterator)localObject3).next();
      c.g.b.k.a(localObject4, "it");
      localObject5 = ((com.truecaller.api.services.messenger.v1.models.f)localObject4).a();
      c.g.b.k.a(localObject5, "it.peer");
      localObject4 = c.t.a(com.truecaller.messaging.i.i.a((Peer)localObject5), Integer.valueOf(((com.truecaller.api.services.messenger.v1.models.f)localObject4).b()));
      ((Map)localObject2).put(a, b);
    }
    localObject3 = paramn.b();
    c.g.b.k.a(localObject3, "event.context");
    localObject3 = ((Peer)localObject3).c();
    c.g.b.k.a(localObject3, "event.context.group");
    localObject3 = ((Peer.b)localObject3).a();
    c.g.b.k.a(localObject3, "event.context.group.id");
    a((ArrayList)localObject1, (Map)localObject2, (String)localObject3);
    a((ArrayList)localObject1);
    localObject1 = f;
    localObject2 = paramn.b();
    c.g.b.k.a(localObject2, "event.context");
    localObject2 = ((Peer)localObject2).c();
    c.g.b.k.a(localObject2, "event.context.group");
    localObject2 = ((Peer.b)localObject2).a();
    c.g.b.k.a(localObject2, "event.context.group.id");
    localObject3 = paramn.c();
    c.g.b.k.a(localObject3, "event.messageId");
    Object localObject4 = paramn.a();
    c.g.b.k.a(localObject4, "event.sender");
    localObject4 = ((Peer)localObject4).b();
    c.g.b.k.a(localObject4, "event.sender.user");
    localObject4 = ((Peer.d)localObject4).a();
    c.g.b.k.a(localObject4, "event.sender.user.id");
    paramn = paramn.g();
    c.g.b.k.a(paramn, "event.participantsList");
    Object localObject5 = (Iterable)paramn;
    paramn = (Collection)new ArrayList(c.a.m.a((Iterable)localObject5, 10));
    localObject5 = ((Iterable)localObject5).iterator();
    while (((Iterator)localObject5).hasNext())
    {
      localObject6 = (com.truecaller.api.services.messenger.v1.models.f)((Iterator)localObject5).next();
      c.g.b.k.a(localObject6, "it");
      localObject6 = ((com.truecaller.api.services.messenger.v1.models.f)localObject6).a();
      c.g.b.k.a(localObject6, "it.peer");
      localObject6 = ((Peer)localObject6).b();
      c.g.b.k.a(localObject6, "it.peer.user");
      paramn.add(((Peer.d)localObject6).a());
    }
    ((m)localObject1).a((String)localObject2, (String)localObject3, (String)localObject4, (List)paramn);
  }
  
  public final void a(Event.p paramp)
  {
    c.g.b.k.b(paramp, "event");
    Object localObject1 = new ArrayList();
    Object localObject2 = paramp.d();
    c.g.b.k.a(localObject2, "event.participantsList");
    Object localObject3 = (Iterable)localObject2;
    localObject2 = (Collection)new ArrayList(c.a.m.a((Iterable)localObject3, 10));
    localObject3 = ((Iterable)localObject3).iterator();
    while (((Iterator)localObject3).hasNext())
    {
      localObject4 = (Peer)((Iterator)localObject3).next();
      c.g.b.k.a(localObject4, "it");
      ((Collection)localObject2).add(com.truecaller.messaging.i.i.a((Peer)localObject4));
    }
    localObject2 = (List)localObject2;
    localObject3 = paramp.b();
    c.g.b.k.a(localObject3, "event.context");
    localObject3 = ((Peer)localObject3).c();
    c.g.b.k.a(localObject3, "event.context.group");
    localObject3 = ((Peer.b)localObject3).a();
    c.g.b.k.a(localObject3, "event.context.group.id");
    a((ArrayList)localObject1, (List)localObject2, (String)localObject3);
    a((ArrayList)localObject1);
    Object localObject4 = f;
    localObject1 = paramp.b();
    c.g.b.k.a(localObject1, "event.context");
    localObject1 = ((Peer)localObject1).c();
    c.g.b.k.a(localObject1, "event.context.group");
    String str1 = ((Peer.b)localObject1).a();
    c.g.b.k.a(str1, "event.context.group.id");
    String str2 = paramp.c();
    c.g.b.k.a(str2, "event.messageId");
    localObject2 = paramp.a();
    c.g.b.k.a(localObject2, "it");
    int n;
    if (((Peer)localObject2).a() == Peer.TypeCase.USER) {
      n = 1;
    } else {
      n = 0;
    }
    localObject3 = null;
    if (n == 0) {
      localObject2 = null;
    }
    localObject1 = localObject3;
    if (localObject2 != null)
    {
      localObject2 = ((Peer)localObject2).b();
      localObject1 = localObject3;
      if (localObject2 != null) {
        localObject1 = ((Peer.d)localObject2).a();
      }
    }
    paramp = paramp.d();
    c.g.b.k.a(paramp, "event.participantsList");
    localObject2 = (Iterable)paramp;
    paramp = (Collection)new ArrayList(c.a.m.a((Iterable)localObject2, 10));
    localObject2 = ((Iterable)localObject2).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject3 = (Peer)((Iterator)localObject2).next();
      c.g.b.k.a(localObject3, "it");
      localObject3 = ((Peer)localObject3).b();
      c.g.b.k.a(localObject3, "it.user");
      paramp.add(((Peer.d)localObject3).a());
    }
    ((m)localObject4).b(str1, str2, (String)localObject1, (List)paramp);
  }
  
  public final void a(Event.x paramx)
  {
    c.g.b.k.b(paramx, "event");
    Object localObject1 = new ArrayList();
    Object localObject2 = paramx.b();
    c.g.b.k.a(localObject2, "event.context");
    localObject2 = ((Peer)localObject2).c();
    c.g.b.k.a(localObject2, "event.context.group");
    localObject2 = ((Peer.b)localObject2).a();
    c.g.b.k.a(localObject2, "event.context.group.id");
    Object localObject3 = paramx.d();
    c.g.b.k.a(localObject3, "event.participant");
    localObject3 = ((Peer)localObject3).b();
    c.g.b.k.a(localObject3, "event.participant.user");
    localObject3 = ((Peer.d)localObject3).a();
    c.g.b.k.a(localObject3, "event.participant.user.id");
    int n = paramx.e();
    Object localObject4 = paramx.f();
    c.g.b.k.a(localObject4, "event.permissions");
    a((ArrayList)localObject1, (String)localObject2, (String)localObject3, n, e.a((com.truecaller.api.services.messenger.v1.models.a)localObject4));
    a((ArrayList)localObject1);
    localObject2 = f;
    localObject1 = paramx.b();
    c.g.b.k.a(localObject1, "event.context");
    localObject1 = ((Peer)localObject1).c();
    c.g.b.k.a(localObject1, "event.context.group");
    localObject3 = ((Peer.b)localObject1).a();
    c.g.b.k.a(localObject3, "event.context.group.id");
    localObject4 = paramx.c();
    c.g.b.k.a(localObject4, "event.messageId");
    int i1 = paramx.e();
    localObject1 = paramx.a();
    c.g.b.k.a(localObject1, "it");
    if (((Peer)localObject1).a() == Peer.TypeCase.USER) {
      n = 1;
    } else {
      n = 0;
    }
    if (n == 0) {
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      localObject1 = ((Peer)localObject1).b();
      if (localObject1 != null)
      {
        localObject1 = ((Peer.d)localObject1).a();
        break label287;
      }
    }
    localObject1 = null;
    label287:
    paramx = paramx.d();
    c.g.b.k.a(paramx, "event.participant");
    paramx = paramx.b();
    c.g.b.k.a(paramx, "event.participant.user");
    paramx = paramx.a();
    c.g.b.k.a(paramx, "event.participant.user.id");
    ((m)localObject2).a((String)localObject3, (String)localObject4, i1, (String)localObject1, paramx);
  }
  
  /* Error */
  @SuppressLint({"Recycle"})
  public final w<List<Participant>> b(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 796
    //   4: invokestatic 43	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   7: aload_0
    //   8: getfield 74	com/truecaller/messaging/transport/im/a/c:b	Landroid/content/ContentResolver;
    //   11: astore 4
    //   13: aconst_null
    //   14: astore_3
    //   15: aconst_null
    //   16: astore_2
    //   17: aload 4
    //   19: aload_1
    //   20: aconst_null
    //   21: invokestatic 801	com/truecaller/content/TruecallerContract$t:a	(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    //   24: aconst_null
    //   25: aconst_null
    //   26: aconst_null
    //   27: aconst_null
    //   28: invokevirtual 295	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   31: astore_1
    //   32: aload_0
    //   33: getfield 76	com/truecaller/messaging/transport/im/a/c:c	Lcom/truecaller/messaging/data/c;
    //   36: aload_1
    //   37: invokeinterface 808 2 0
    //   42: astore 4
    //   44: aload_3
    //   45: astore_1
    //   46: aload 4
    //   48: ifnull +227 -> 275
    //   51: aload 4
    //   53: checkcast 299	android/database/Cursor
    //   56: astore 4
    //   58: aload 4
    //   60: checkcast 297	java/io/Closeable
    //   63: astore_3
    //   64: aload_2
    //   65: astore_1
    //   66: new 123	java/util/ArrayList
    //   69: dup
    //   70: invokespecial 124	java/util/ArrayList:<init>	()V
    //   73: checkcast 203	java/util/Collection
    //   76: astore 5
    //   78: aload_2
    //   79: astore_1
    //   80: aload 4
    //   82: invokeinterface 302 1 0
    //   87: ifeq +26 -> 113
    //   90: aload_2
    //   91: astore_1
    //   92: aload 5
    //   94: aload 4
    //   96: checkcast 1234	com/truecaller/messaging/transport/im/a/g
    //   99: invokeinterface 1237 1 0
    //   104: invokeinterface 221 2 0
    //   109: pop
    //   110: goto -32 -> 78
    //   113: aload_2
    //   114: astore_1
    //   115: aload 5
    //   117: checkcast 183	java/util/List
    //   120: astore_2
    //   121: aload_3
    //   122: aconst_null
    //   123: invokestatic 313	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   126: aload_2
    //   127: checkcast 195	java/lang/Iterable
    //   130: astore_1
    //   131: new 123	java/util/ArrayList
    //   134: dup
    //   135: aload_1
    //   136: bipush 10
    //   138: invokestatic 200	c/a/m:a	(Ljava/lang/Iterable;I)I
    //   141: invokespecial 201	java/util/ArrayList:<init>	(I)V
    //   144: checkcast 203	java/util/Collection
    //   147: astore_3
    //   148: aload_1
    //   149: invokeinterface 207 1 0
    //   154: astore 4
    //   156: aload 4
    //   158: invokeinterface 212 1 0
    //   163: ifeq +88 -> 251
    //   166: aload 4
    //   168: invokeinterface 216 1 0
    //   173: checkcast 1239	com/truecaller/messaging/transport/im/a/f
    //   176: astore_1
    //   177: new 103	com/truecaller/messaging/data/types/Participant$a
    //   180: dup
    //   181: iconst_3
    //   182: invokespecial 106	com/truecaller/messaging/data/types/Participant$a:<init>	(I)V
    //   185: aload_1
    //   186: getfield 1240	com/truecaller/messaging/transport/im/a/f:a	Ljava/lang/String;
    //   189: invokevirtual 114	com/truecaller/messaging/data/types/Participant$a:b	(Ljava/lang/String;)Lcom/truecaller/messaging/data/types/Participant$a;
    //   192: aload_1
    //   193: getfield 1242	com/truecaller/messaging/transport/im/a/f:e	Ljava/lang/String;
    //   196: invokevirtual 1244	com/truecaller/messaging/data/types/Participant$a:f	(Ljava/lang/String;)Lcom/truecaller/messaging/data/types/Participant$a;
    //   199: aload_1
    //   200: getfield 1247	com/truecaller/messaging/transport/im/a/f:g	J
    //   203: invokevirtual 1250	com/truecaller/messaging/data/types/Participant$a:c	(J)Lcom/truecaller/messaging/data/types/Participant$a;
    //   206: astore 5
    //   208: aload_1
    //   209: getfield 1251	com/truecaller/messaging/transport/im/a/f:f	Ljava/lang/String;
    //   212: astore_2
    //   213: aload_2
    //   214: astore_1
    //   215: aload_2
    //   216: ifnonnull +7 -> 223
    //   219: ldc_w 1143
    //   222: astore_1
    //   223: aload 5
    //   225: aload_1
    //   226: invokevirtual 1253	com/truecaller/messaging/data/types/Participant$a:g	(Ljava/lang/String;)Lcom/truecaller/messaging/data/types/Participant$a;
    //   229: invokevirtual 117	com/truecaller/messaging/data/types/Participant$a:a	()Lcom/truecaller/messaging/data/types/Participant;
    //   232: astore_1
    //   233: aload_1
    //   234: ldc_w 1255
    //   237: invokestatic 121	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   240: aload_3
    //   241: aload_1
    //   242: invokeinterface 221 2 0
    //   247: pop
    //   248: goto -92 -> 156
    //   251: aload_3
    //   252: checkcast 183	java/util/List
    //   255: astore_1
    //   256: goto +19 -> 275
    //   259: astore_2
    //   260: goto +8 -> 268
    //   263: astore_2
    //   264: aload_2
    //   265: astore_1
    //   266: aload_2
    //   267: athrow
    //   268: aload_3
    //   269: aload_1
    //   270: invokestatic 313	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   273: aload_2
    //   274: athrow
    //   275: aload_1
    //   276: invokestatic 789	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   279: astore_1
    //   280: aload_1
    //   281: ldc_w 810
    //   284: invokestatic 121	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   287: aload_1
    //   288: ldc_w 1257
    //   291: invokestatic 121	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   294: aload_1
    //   295: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	296	0	this	c
    //   0	296	1	paramString	String
    //   16	200	2	localObject1	Object
    //   259	1	2	localObject2	Object
    //   263	11	2	localThrowable	Throwable
    //   14	255	3	localObject3	Object
    //   11	156	4	localObject4	Object
    //   76	148	5	localObject5	Object
    // Exception table:
    //   from	to	target	type
    //   66	78	259	finally
    //   80	90	259	finally
    //   92	110	259	finally
    //   115	121	259	finally
    //   266	268	259	finally
    //   66	78	263	java/lang/Throwable
    //   80	90	263	java/lang/Throwable
    //   92	110	263	java/lang/Throwable
    //   115	121	263	java/lang/Throwable
  }
  
  public final w<Boolean> b(String paramString, boolean paramBoolean)
  {
    c.g.b.k.b(paramString, "groupId");
    c(paramString, paramBoolean);
    f.a(paramString);
    paramString = w.b(Boolean.TRUE);
    c.g.b.k.a(paramString, "Promise.wrap(true)");
    return paramString;
  }
  
  /* Error */
  @SuppressLint({"Recycle"})
  public final void b()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 74	com/truecaller/messaging/transport/im/a/c:b	Landroid/content/ContentResolver;
    //   4: invokestatic 129	com/truecaller/content/TruecallerContract$r:a	()Landroid/net/Uri;
    //   7: aconst_null
    //   8: ldc_w 1262
    //   11: aconst_null
    //   12: aconst_null
    //   13: invokevirtual 295	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   16: astore_1
    //   17: aload_1
    //   18: ifnull +185 -> 203
    //   21: aload_0
    //   22: getfield 76	com/truecaller/messaging/transport/im/a/c:c	Lcom/truecaller/messaging/data/c;
    //   25: aload_1
    //   26: invokeinterface 1265 2 0
    //   31: astore_1
    //   32: aload_1
    //   33: ifnull +170 -> 203
    //   36: aload_1
    //   37: checkcast 299	android/database/Cursor
    //   40: astore 4
    //   42: aload 4
    //   44: checkcast 297	java/io/Closeable
    //   47: astore_3
    //   48: aconst_null
    //   49: astore_2
    //   50: aload_2
    //   51: astore_1
    //   52: new 123	java/util/ArrayList
    //   55: dup
    //   56: invokespecial 124	java/util/ArrayList:<init>	()V
    //   59: checkcast 203	java/util/Collection
    //   62: astore 5
    //   64: aload_2
    //   65: astore_1
    //   66: aload 4
    //   68: invokeinterface 302 1 0
    //   73: ifeq +108 -> 181
    //   76: aload_2
    //   77: astore_1
    //   78: aload 4
    //   80: checkcast 1267	com/truecaller/messaging/data/a/h
    //   83: invokeinterface 1270 1 0
    //   88: astore 6
    //   90: aload_2
    //   91: astore_1
    //   92: aload 6
    //   94: getfield 1271	com/truecaller/messaging/data/types/ImGroupInfo:e	Ljava/lang/String;
    //   97: ifnull +68 -> 165
    //   100: aload_2
    //   101: astore_1
    //   102: aload_0
    //   103: getfield 82	com/truecaller/messaging/transport/im/a/c:f	Lcom/truecaller/messaging/transport/im/a/m;
    //   106: astore 7
    //   108: aload_2
    //   109: astore_1
    //   110: aload 6
    //   112: getfield 111	com/truecaller/messaging/data/types/ImGroupInfo:a	Ljava/lang/String;
    //   115: astore 8
    //   117: aload_2
    //   118: astore_1
    //   119: new 248	java/lang/StringBuilder
    //   122: dup
    //   123: ldc_w 1273
    //   126: invokespecial 253	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   129: astore 9
    //   131: aload_2
    //   132: astore_1
    //   133: aload 9
    //   135: aload 6
    //   137: getfield 111	com/truecaller/messaging/data/types/ImGroupInfo:a	Ljava/lang/String;
    //   140: invokevirtual 267	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   143: pop
    //   144: aload_2
    //   145: astore_1
    //   146: aload 7
    //   148: aload 8
    //   150: aload 9
    //   152: invokevirtual 273	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   155: aload 6
    //   157: getfield 1271	com/truecaller/messaging/data/types/ImGroupInfo:e	Ljava/lang/String;
    //   160: invokeinterface 1275 4 0
    //   165: aload_2
    //   166: astore_1
    //   167: aload 5
    //   169: getstatic 1280	c/x:a	Lc/x;
    //   172: invokeinterface 221 2 0
    //   177: pop
    //   178: goto -114 -> 64
    //   181: aload_3
    //   182: aconst_null
    //   183: invokestatic 313	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   186: return
    //   187: astore_2
    //   188: goto +8 -> 196
    //   191: astore_2
    //   192: aload_2
    //   193: astore_1
    //   194: aload_2
    //   195: athrow
    //   196: aload_3
    //   197: aload_1
    //   198: invokestatic 313	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   201: aload_2
    //   202: athrow
    //   203: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	204	0	this	c
    //   16	182	1	localObject1	Object
    //   49	117	2	localObject2	Object
    //   187	1	2	localObject3	Object
    //   191	11	2	localThrowable	Throwable
    //   47	150	3	localCloseable	java.io.Closeable
    //   40	39	4	localCursor	android.database.Cursor
    //   62	106	5	localCollection	Collection
    //   88	68	6	localImGroupInfo	ImGroupInfo
    //   106	41	7	localm	m
    //   115	34	8	str	String
    //   129	22	9	localStringBuilder	StringBuilder
    // Exception table:
    //   from	to	target	type
    //   52	64	187	finally
    //   66	76	187	finally
    //   78	90	187	finally
    //   92	100	187	finally
    //   102	108	187	finally
    //   110	117	187	finally
    //   119	131	187	finally
    //   133	144	187	finally
    //   146	165	187	finally
    //   167	178	187	finally
    //   194	196	187	finally
    //   52	64	191	java/lang/Throwable
    //   66	76	191	java/lang/Throwable
    //   78	90	191	java/lang/Throwable
    //   92	100	191	java/lang/Throwable
    //   102	108	191	java/lang/Throwable
    //   110	117	191	java/lang/Throwable
    //   119	131	191	java/lang/Throwable
    //   133	144	191	java/lang/Throwable
    //   146	165	191	java/lang/Throwable
    //   167	178	191	java/lang/Throwable
  }
  
  @SuppressLint({"Recycle"})
  public final w<Integer> c(String paramString)
  {
    c.g.b.k.b(paramString, "groupId");
    Object localObject = b;
    Uri localUri = TruecallerContract.s.a();
    c.g.b.k.a(localUri, "ImGroupParticipantsTable.getContentUri()");
    localObject = com.truecaller.utils.extensions.h.a((ContentResolver)localObject, localUri, "COUNT()", "im_group_id = ?", new String[] { paramString });
    paramString = (String)localObject;
    if (localObject == null) {
      paramString = Integer.valueOf(0);
    }
    paramString = w.b(paramString);
    c.g.b.k.a(paramString, "Promise.wrap(it ?: 0)");
    c.g.b.k.a(paramString, "contentResolver.queryInt…{ Promise.wrap(it ?: 0) }");
    return paramString;
  }
  
  public final w<Boolean> d(String paramString)
  {
    c.g.b.k.b(paramString, "groupId");
    Object localObject1 = d.G();
    if (localObject1 == null)
    {
      paramString = w.b(Boolean.FALSE);
      c.g.b.k.a(paramString, "Promise.wrap(false)");
      return paramString;
    }
    Object localObject2 = b(paramString, (String)localObject1, 8);
    if (localObject2 == null)
    {
      paramString = w.b(Boolean.FALSE);
      c.g.b.k.a(paramString, "Promise.wrap(false)");
      return paramString;
    }
    localObject1 = f;
    localObject2 = ((z.d)localObject2).a();
    c.g.b.k.a(localObject2, "response.messageId");
    ((m)localObject1).b(paramString, (String)localObject2);
    paramString = w.b(Boolean.TRUE);
    c.g.b.k.a(paramString, "Promise.wrap(true)");
    return paramString;
  }
  
  /* Error */
  public final w<ImGroupInfo> e(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 796
    //   4: invokestatic 43	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   7: aload_0
    //   8: getfield 74	com/truecaller/messaging/transport/im/a/c:b	Landroid/content/ContentResolver;
    //   11: invokestatic 129	com/truecaller/content/TruecallerContract$r:a	()Landroid/net/Uri;
    //   14: aconst_null
    //   15: ldc -94
    //   17: iconst_1
    //   18: anewarray 164	java/lang/String
    //   21: dup
    //   22: iconst_0
    //   23: aload_1
    //   24: aastore
    //   25: aconst_null
    //   26: invokevirtual 295	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   29: astore_3
    //   30: aconst_null
    //   31: astore_1
    //   32: aconst_null
    //   33: astore_2
    //   34: aload_3
    //   35: ifnull +89 -> 124
    //   38: aload_3
    //   39: checkcast 297	java/io/Closeable
    //   42: astore_3
    //   43: aload_2
    //   44: astore_1
    //   45: aload_3
    //   46: checkcast 299	android/database/Cursor
    //   49: astore 4
    //   51: aload_2
    //   52: astore_1
    //   53: aload_0
    //   54: getfield 76	com/truecaller/messaging/transport/im/a/c:c	Lcom/truecaller/messaging/data/c;
    //   57: aload 4
    //   59: invokeinterface 1265 2 0
    //   64: astore 4
    //   66: aload 4
    //   68: ifnull +30 -> 98
    //   71: aload_2
    //   72: astore_1
    //   73: aload 4
    //   75: invokeinterface 1293 1 0
    //   80: ifeq +18 -> 98
    //   83: aload_2
    //   84: astore_1
    //   85: aload 4
    //   87: invokeinterface 1270 1 0
    //   92: astore_2
    //   93: aload_2
    //   94: astore_1
    //   95: goto +5 -> 100
    //   98: aconst_null
    //   99: astore_1
    //   100: aload_3
    //   101: aconst_null
    //   102: invokestatic 313	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   105: goto +19 -> 124
    //   108: astore_2
    //   109: goto +8 -> 117
    //   112: astore_2
    //   113: aload_2
    //   114: astore_1
    //   115: aload_2
    //   116: athrow
    //   117: aload_3
    //   118: aload_1
    //   119: invokestatic 313	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   122: aload_2
    //   123: athrow
    //   124: aload_1
    //   125: invokestatic 789	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   128: astore_1
    //   129: aload_1
    //   130: ldc_w 810
    //   133: invokestatic 121	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   136: aload_1
    //   137: ldc_w 812
    //   140: invokestatic 121	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   143: aload_1
    //   144: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	145	0	this	c
    //   0	145	1	paramString	String
    //   33	61	2	localImGroupInfo	ImGroupInfo
    //   108	1	2	localObject1	Object
    //   112	11	2	localThrowable	Throwable
    //   29	89	3	localObject2	Object
    //   49	37	4	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   45	51	108	finally
    //   53	66	108	finally
    //   73	83	108	finally
    //   85	93	108	finally
    //   115	117	108	finally
    //   45	51	112	java/lang/Throwable
    //   53	66	112	java/lang/Throwable
    //   73	83	112	java/lang/Throwable
    //   85	93	112	java/lang/Throwable
  }
  
  public final void f(String paramString)
  {
    c.g.b.k.b(paramString, "groupId");
    paramString = g(paramString);
    if (paramString != null)
    {
      long l1 = ((Number)paramString).longValue();
      ((com.truecaller.messaging.data.t)j.a()).a(l1, 1, 0);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */