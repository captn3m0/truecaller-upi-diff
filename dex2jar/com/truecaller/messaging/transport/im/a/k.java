package com.truecaller.messaging.transport.im.a;

import com.truecaller.utils.n;
import dagger.a.d;
import javax.inject.Provider;

public final class k
  implements d<j>
{
  private final Provider<n> a;
  
  private k(Provider<n> paramProvider)
  {
    a = paramProvider;
  }
  
  public static k a(Provider<n> paramProvider)
  {
    return new k(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */