package com.truecaller.messaging.transport.im.a;

import android.content.ContentResolver;
import c.g.b.k;
import com.truecaller.androidactors.f;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant.a;
import com.truecaller.messaging.data.types.TextEntity;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.h;
import com.truecaller.messaging.transport.status.StatusTransportInfo;
import com.truecaller.utils.a;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import javax.inject.Inject;

public final class n
  implements m
{
  private final f<t> a;
  private final ContentResolver b;
  private final a c;
  private final e d;
  private final com.truecaller.utils.n e;
  private final i f;
  private final h g;
  
  @Inject
  public n(f<t> paramf, ContentResolver paramContentResolver, a parama, e parame, com.truecaller.utils.n paramn, i parami, h paramh)
  {
    a = paramf;
    b = paramContentResolver;
    c = parama;
    d = parame;
    e = paramn;
    f = parami;
    g = paramh;
  }
  
  private final void a(String paramString1, String paramString2, int paramInt1, int paramInt2, Object... paramVarArgs)
  {
    paramVarArgs = e.a(paramInt1, paramInt2, Arrays.copyOf(paramVarArgs, paramVarArgs.length));
    k.a(paramVarArgs, "resourceProvider.getQuan…s, quantity, *formatArgs)");
    a(this, paramString1, paramString2, paramVarArgs);
  }
  
  private final void a(String paramString1, String paramString2, int paramInt, Object... paramVarArgs)
  {
    paramVarArgs = e.a(paramInt, Arrays.copyOf(paramVarArgs, paramVarArgs.length));
    k.a(paramVarArgs, "resourceProvider.getString(res, *formatArgs)");
    a(this, paramString1, paramString2, paramVarArgs);
  }
  
  private final void a(String paramString1, String paramString2, String paramString3, boolean paramBoolean1, boolean paramBoolean2)
  {
    if (!d.i().a()) {
      return;
    }
    if ((paramBoolean2) && ((c(paramString1) & 0x2) != 0)) {
      return;
    }
    paramString1 = new Participant.a(4).b(paramString1).a();
    k.a(paramString1, "Participant.Builder(Part…pId)\n            .build()");
    paramString1 = new Message.a().a(paramString1).a(6, (TransportInfo)new StatusTransportInfo(-1L, paramString2)).a(TextEntity.a("text/plain", paramString3)).b(paramBoolean1).b();
    k.a(paramString1, "Message.Builder()\n      …ead)\n            .build()");
    ((t)a.a()).a(paramString1);
  }
  
  /* Error */
  private final String b(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 50	com/truecaller/messaging/transport/im/a/n:b	Landroid/content/ContentResolver;
    //   4: aload_1
    //   5: invokestatic 173	com/truecaller/content/TruecallerContract$t:a	(Ljava/lang/String;)Landroid/net/Uri;
    //   8: iconst_1
    //   9: anewarray 175	java/lang/String
    //   12: dup
    //   13: iconst_0
    //   14: ldc -79
    //   16: aastore
    //   17: aconst_null
    //   18: aconst_null
    //   19: aconst_null
    //   20: invokevirtual 183	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   23: astore 4
    //   25: aconst_null
    //   26: astore_2
    //   27: aconst_null
    //   28: astore_3
    //   29: aload 4
    //   31: ifnull +75 -> 106
    //   34: aload 4
    //   36: checkcast 185	java/io/Closeable
    //   39: astore 4
    //   41: aload_3
    //   42: astore_2
    //   43: aload 4
    //   45: checkcast 187	android/database/Cursor
    //   48: astore 5
    //   50: aload_3
    //   51: astore_2
    //   52: aload 5
    //   54: invokeinterface 190 1 0
    //   59: ifeq +19 -> 78
    //   62: aload_3
    //   63: astore_2
    //   64: aload 5
    //   66: iconst_0
    //   67: invokeinterface 194 2 0
    //   72: astore_3
    //   73: aload_3
    //   74: astore_2
    //   75: goto +5 -> 80
    //   78: aconst_null
    //   79: astore_2
    //   80: aload 4
    //   82: aconst_null
    //   83: invokestatic 199	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   86: goto +20 -> 106
    //   89: astore_1
    //   90: goto +8 -> 98
    //   93: astore_1
    //   94: aload_1
    //   95: astore_2
    //   96: aload_1
    //   97: athrow
    //   98: aload 4
    //   100: aload_2
    //   101: invokestatic 199	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   104: aload_1
    //   105: athrow
    //   106: aload_2
    //   107: astore_3
    //   108: aload_2
    //   109: ifnonnull +14 -> 123
    //   112: aload_0
    //   113: getfield 58	com/truecaller/messaging/transport/im/a/n:f	Lcom/truecaller/messaging/transport/im/a/i;
    //   116: aload_1
    //   117: invokeinterface 203 2 0
    //   122: astore_3
    //   123: aload_3
    //   124: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	125	0	this	n
    //   0	125	1	paramString	String
    //   26	83	2	localObject1	Object
    //   28	96	3	localObject2	Object
    //   23	76	4	localObject3	Object
    //   48	17	5	localCursor	android.database.Cursor
    // Exception table:
    //   from	to	target	type
    //   43	50	89	finally
    //   52	62	89	finally
    //   64	73	89	finally
    //   96	98	89	finally
    //   43	50	93	java/lang/Throwable
    //   52	62	93	java/lang/Throwable
    //   64	73	93	java/lang/Throwable
  }
  
  private final void b(String paramString1, String paramString2, int paramInt, Object... paramVarArgs)
  {
    paramVarArgs = e.a(paramInt, Arrays.copyOf(paramVarArgs, paramVarArgs.length));
    k.a(paramVarArgs, "resourceProvider.getString(res, *formatArgs)");
    a(paramString1, paramString2, paramVarArgs, false, false);
  }
  
  /* Error */
  private final int c(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 50	com/truecaller/messaging/transport/im/a/n:b	Landroid/content/ContentResolver;
    //   4: invokestatic 208	com/truecaller/content/TruecallerContract$r:a	()Landroid/net/Uri;
    //   7: iconst_1
    //   8: anewarray 175	java/lang/String
    //   11: dup
    //   12: iconst_0
    //   13: ldc -46
    //   15: aastore
    //   16: ldc -44
    //   18: iconst_1
    //   19: anewarray 175	java/lang/String
    //   22: dup
    //   23: iconst_0
    //   24: aload_1
    //   25: aastore
    //   26: aconst_null
    //   27: invokevirtual 183	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   30: astore_1
    //   31: aload_1
    //   32: ifnull +81 -> 113
    //   35: aload_1
    //   36: checkcast 185	java/io/Closeable
    //   39: astore_3
    //   40: aconst_null
    //   41: astore_2
    //   42: aload_2
    //   43: astore_1
    //   44: aload_3
    //   45: checkcast 187	android/database/Cursor
    //   48: astore 4
    //   50: aload_2
    //   51: astore_1
    //   52: aload 4
    //   54: invokeinterface 190 1 0
    //   59: ifeq +22 -> 81
    //   62: aload_2
    //   63: astore_1
    //   64: aload 4
    //   66: iconst_0
    //   67: invokeinterface 216 2 0
    //   72: invokestatic 222	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   75: astore_2
    //   76: aload_2
    //   77: astore_1
    //   78: goto +5 -> 83
    //   81: aconst_null
    //   82: astore_1
    //   83: aload_3
    //   84: aconst_null
    //   85: invokestatic 199	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   88: aload_1
    //   89: ifnull +24 -> 113
    //   92: aload_1
    //   93: invokevirtual 226	java/lang/Integer:intValue	()I
    //   96: ireturn
    //   97: astore_2
    //   98: goto +8 -> 106
    //   101: astore_2
    //   102: aload_2
    //   103: astore_1
    //   104: aload_2
    //   105: athrow
    //   106: aload_3
    //   107: aload_1
    //   108: invokestatic 199	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   111: aload_2
    //   112: athrow
    //   113: iconst_0
    //   114: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	115	0	this	n
    //   0	115	1	paramString	String
    //   41	36	2	localInteger	Integer
    //   97	1	2	localObject	Object
    //   101	11	2	localThrowable	Throwable
    //   39	68	3	localCloseable	java.io.Closeable
    //   48	17	4	localCursor	android.database.Cursor
    // Exception table:
    //   from	to	target	type
    //   44	50	97	finally
    //   52	62	97	finally
    //   64	76	97	finally
    //   104	106	97	finally
    //   44	50	101	java/lang/Throwable
    //   52	62	101	java/lang/Throwable
    //   64	76	101	java/lang/Throwable
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "groupId");
    String str = UUID.randomUUID().toString();
    k.a(str, "UUID.randomUUID().toString()");
    a(paramString, str, 2131887177, new Object[0]);
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    a(paramString1, paramString2, 2131887189, new Object[0]);
  }
  
  public final void a(String paramString1, String paramString2, int paramInt, String paramString3)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    k.b(paramString3, "imPeerId");
    String str = f.a(paramInt);
    if (str == null) {
      return;
    }
    a(paramString1, paramString2, 2131887187, new Object[] { b(paramString3), str });
  }
  
  public final void a(String paramString1, String paramString2, int paramInt, String paramString3, String paramString4)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    k.b(paramString4, "imPeerId");
    String str = f.a(paramInt);
    if (str == null) {
      return;
    }
    if (paramString3 == null)
    {
      a(paramString1, paramString2, 2131887185, new Object[] { b(paramString4), str });
      return;
    }
    if (((paramInt & 0x8) != 0) && (k.a(paramString3, paramString4)))
    {
      a(paramString1, paramString2, 2131887181, new Object[] { b(paramString4) });
      return;
    }
    a(paramString1, paramString2, 2131887186, new Object[] { b(paramString3), b(paramString4), str });
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    k.b(paramString3, "senderPeerId");
    b(paramString1, paramString2, 2131887180, new Object[] { b(paramString3) });
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    k.b(paramString3, "senderPeerId");
    k.b(paramString4, "title");
    b(paramString1, paramString2, 2131887173, new Object[] { paramString4, b(paramString3) });
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, List<String> paramList)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    k.b(paramString3, "senderPeerId");
    k.b(paramList, "imPeerIds");
    if (c.a.m.a((Iterable)paramList, g.G()))
    {
      b(paramString1, paramString2, 2131887180, new Object[] { b(paramString3) });
      return;
    }
    if (paramList.size() == 1)
    {
      a(paramString1, paramString2, 2131887178, new Object[] { b((String)c.a.m.d(paramList)), b(paramString3) });
      return;
    }
    if (paramList.size() > 1)
    {
      int i = paramList.size() - 1;
      a(paramString1, paramString2, 2131755034, i, new Object[] { b((String)c.a.m.d(paramList)), Integer.valueOf(i), b(paramString3) });
    }
  }
  
  public final void a(String paramString1, String paramString2, List<String> paramList)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    k.b(paramList, "imPeerIds");
    if (paramList.size() == 1)
    {
      a(paramString1, paramString2, 2131887179, new Object[] { b((String)c.a.m.d(paramList)) });
      return;
    }
    if (paramList.size() > 1)
    {
      int i = paramList.size() - 1;
      a(paramString1, paramString2, 2131755035, i, new Object[] { b((String)c.a.m.d(paramList)), Integer.valueOf(i) });
    }
  }
  
  public final void b(String paramString1, String paramString2)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    a(paramString1, paramString2, 2131887188, new Object[0]);
  }
  
  public final void b(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    k.b(paramString3, "title");
    a(paramString1, paramString2, 2131887174, new Object[] { paramString3 });
  }
  
  public final void b(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    k.b(paramString3, "senderPeerId");
    k.b(paramString4, "title");
    a(paramString1, paramString2, 2131887175, new Object[] { b(paramString3), paramString4 });
  }
  
  public final void b(String paramString1, String paramString2, String paramString3, List<String> paramList)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    k.b(paramList, "imPeerIds");
    paramList = ((Iterable)paramList).iterator();
    while (paramList.hasNext())
    {
      String str = (String)paramList.next();
      StringBuilder localStringBuilder;
      if (k.a(str, g.G()))
      {
        if (paramString3 == null)
        {
          localStringBuilder = new StringBuilder();
          localStringBuilder.append(paramString2);
          localStringBuilder.append('-');
          localStringBuilder.append(str);
          a(paramString1, localStringBuilder.toString(), 2131887189, new Object[0]);
        }
        else
        {
          localStringBuilder = new StringBuilder();
          localStringBuilder.append(paramString2);
          localStringBuilder.append('-');
          localStringBuilder.append(str);
          a(paramString1, localStringBuilder.toString(), 2131887190, new Object[] { b(paramString3) });
        }
      }
      else if ((paramString3 != null) && (!k.a(paramString3, str)))
      {
        localStringBuilder = new StringBuilder();
        localStringBuilder.append(paramString2);
        localStringBuilder.append('-');
        localStringBuilder.append(str);
        a(paramString1, localStringBuilder.toString(), 2131887183, new Object[] { b(str), b(paramString3) });
      }
      else
      {
        localStringBuilder = new StringBuilder();
        localStringBuilder.append(paramString2);
        localStringBuilder.append('-');
        localStringBuilder.append(str);
        a(paramString1, localStringBuilder.toString(), 2131887182, new Object[] { b(str) });
      }
    }
  }
  
  public final void c(String paramString1, String paramString2)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    a(paramString1, paramString2, 2131887172, new Object[0]);
  }
  
  public final void c(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    k.b(paramString3, "imPeerId");
    a(paramString1, paramString2, 2131887184, new Object[] { b(paramString3) });
  }
  
  public final void d(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    k.b(paramString3, "senderPeerId");
    a(paramString1, paramString2, 2131887171, new Object[] { b(paramString3) });
  }
  
  public final void e(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    k.b(paramString3, "title");
    a(paramString1, paramString2, 2131887176, new Object[] { paramString3 });
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */