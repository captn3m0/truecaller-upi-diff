package com.truecaller.messaging.transport.im.a;

import c.g.b.k;
import com.truecaller.api.services.messenger.v1.models.GroupAction;
import com.truecaller.messaging.data.types.ImGroupPermissions;
import com.truecaller.utils.n;
import javax.inject.Inject;

public final class j
  implements i
{
  private final n a;
  
  @Inject
  public j(n paramn)
  {
    a = paramn;
  }
  
  private static long b(String paramString)
  {
    paramString = (CharSequence)paramString;
    long l = 5381L;
    int i = 0;
    while (i < paramString.length())
    {
      l = paramString.charAt(i) + ((l << 5) + l);
      i += 1;
    }
    return l;
  }
  
  public final String a(int paramInt)
  {
    Integer localInteger;
    if ((0x40000000 & paramInt) != 0) {
      localInteger = Integer.valueOf(2131886614);
    } else if ((0x20000000 & paramInt) != 0) {
      localInteger = Integer.valueOf(2131886612);
    } else if ((paramInt & 0x8) != 0) {
      localInteger = Integer.valueOf(2131886615);
    } else if ((paramInt & 0x2) != 0) {
      localInteger = Integer.valueOf(2131886613);
    } else {
      localInteger = null;
    }
    if (localInteger != null)
    {
      paramInt = ((Number)localInteger).intValue();
      return a.a(paramInt, new Object[0]);
    }
    return null;
  }
  
  public final String a(String paramString)
  {
    k.b(paramString, "imPeerId");
    StringBuilder localStringBuilder = new StringBuilder("User");
    localStringBuilder.append(Math.abs(b(paramString) % 1000000L));
    return localStringBuilder.toString();
  }
  
  public final boolean a(ImGroupPermissions paramImGroupPermissions, int paramInt1, int paramInt2)
  {
    k.b(paramImGroupPermissions, "permissions");
    paramInt2 ^= paramInt1;
    return ((paramInt1 & b) == 0) && ((c & paramInt2) == paramInt2);
  }
  
  public final boolean a(ImGroupPermissions paramImGroupPermissions, GroupAction paramGroupAction)
  {
    k.b(paramImGroupPermissions, "permissions");
    k.b(paramGroupAction, "action");
    return (a & paramGroupAction.getNumber()) != 0;
  }
  
  public final boolean a(ImGroupPermissions paramImGroupPermissions, GroupAction paramGroupAction, f paramf)
  {
    k.b(paramImGroupPermissions, "permissions");
    k.b(paramGroupAction, "action");
    k.b(paramf, "participant");
    if (a(paramImGroupPermissions, paramGroupAction))
    {
      int i = b;
      if ((b & i) == 0) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */