package com.truecaller.messaging.transport.im.a;

import android.content.ContentResolver;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.b;
import com.truecaller.androidactors.f;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.h;
import com.truecaller.messaging.transport.im.bp;
import com.truecaller.messaging.transport.im.cb;
import javax.inject.Provider;

public final class d
  implements dagger.a.d<c>
{
  private final Provider<cb> a;
  private final Provider<ContentResolver> b;
  private final Provider<com.truecaller.messaging.data.c> c;
  private final Provider<h> d;
  private final Provider<com.truecaller.utils.a> e;
  private final Provider<m> f;
  private final Provider<com.truecaller.data.access.m> g;
  private final Provider<com.truecaller.messaging.notifications.a> h;
  private final Provider<e> i;
  private final Provider<f<t>> j;
  private final Provider<f<bp>> k;
  private final Provider<f<ae>> l;
  private final Provider<b> m;
  
  private d(Provider<cb> paramProvider, Provider<ContentResolver> paramProvider1, Provider<com.truecaller.messaging.data.c> paramProvider2, Provider<h> paramProvider3, Provider<com.truecaller.utils.a> paramProvider4, Provider<m> paramProvider5, Provider<com.truecaller.data.access.m> paramProvider6, Provider<com.truecaller.messaging.notifications.a> paramProvider7, Provider<e> paramProvider8, Provider<f<t>> paramProvider9, Provider<f<bp>> paramProvider10, Provider<f<ae>> paramProvider11, Provider<b> paramProvider12)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
    k = paramProvider10;
    l = paramProvider11;
    m = paramProvider12;
  }
  
  public static d a(Provider<cb> paramProvider, Provider<ContentResolver> paramProvider1, Provider<com.truecaller.messaging.data.c> paramProvider2, Provider<h> paramProvider3, Provider<com.truecaller.utils.a> paramProvider4, Provider<m> paramProvider5, Provider<com.truecaller.data.access.m> paramProvider6, Provider<com.truecaller.messaging.notifications.a> paramProvider7, Provider<e> paramProvider8, Provider<f<t>> paramProvider9, Provider<f<bp>> paramProvider10, Provider<f<ae>> paramProvider11, Provider<b> paramProvider12)
  {
    return new d(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10, paramProvider11, paramProvider12);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */