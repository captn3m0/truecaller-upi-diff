package com.truecaller.messaging.transport.im;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.log.AssertionUtil;
import javax.inject.Inject;

public final class ImSubscriptionService
  extends Service
{
  @Inject
  public ay a;
  private final Binder b = (Binder)new ImSubscriptionService.a();
  private final Handler c = new Handler(Looper.getMainLooper());
  private final Runnable d = (Runnable)new ImSubscriptionService.b(this);
  
  public ImSubscriptionService()
  {
    TrueApp localTrueApp = TrueApp.y();
    k.a(localTrueApp, "TrueApp.getApp()");
    localTrueApp.a().a(this);
  }
  
  private final void a()
  {
    c.removeCallbacks(d);
    try
    {
      startService(new Intent((Context)this, ImSubscriptionService.class));
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localIllegalStateException);
    }
  }
  
  public final IBinder onBind(Intent paramIntent)
  {
    a();
    return (IBinder)b;
  }
  
  public final void onCreate()
  {
    super.onCreate();
    ay localay = a;
    if (localay == null) {
      k.a("subscriptionManager");
    }
    localay.a();
  }
  
  public final void onDestroy()
  {
    ay localay = a;
    if (localay == null) {
      k.a("subscriptionManager");
    }
    localay.b();
    super.onDestroy();
  }
  
  public final void onRebind(Intent paramIntent)
  {
    a();
  }
  
  public final boolean onUnbind(Intent paramIntent)
  {
    c.postDelayed(d, 10000L);
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.ImSubscriptionService
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */