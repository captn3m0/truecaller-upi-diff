package com.truecaller.messaging.transport.im;

import android.content.ContentResolver;
import com.truecaller.common.background.b;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.h;
import com.truecaller.util.af;
import com.truecaller.util.al;
import dagger.a.d;
import javax.inject.Provider;

public final class l
  implements d<k>
{
  private final Provider<af> a;
  private final Provider<ContentResolver> b;
  private final Provider<h> c;
  private final Provider<bp> d;
  private final Provider<bw> e;
  private final Provider<al> f;
  private final Provider<b> g;
  
  private l(Provider<af> paramProvider, Provider<ContentResolver> paramProvider1, Provider<h> paramProvider2, Provider<bp> paramProvider3, Provider<bw> paramProvider4, Provider<al> paramProvider5, Provider<b> paramProvider6)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
  }
  
  public static l a(Provider<af> paramProvider, Provider<ContentResolver> paramProvider1, Provider<h> paramProvider2, Provider<bp> paramProvider3, Provider<bw> paramProvider4, Provider<al> paramProvider5, Provider<b> paramProvider6)
  {
    return new l(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */