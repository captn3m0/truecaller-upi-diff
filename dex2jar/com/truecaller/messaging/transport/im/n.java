package com.truecaller.messaging.transport.im;

import android.content.Intent;
import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.api.services.messenger.v1.events.Event;
import com.truecaller.api.services.messenger.v1.events.Event.PayloadCase;
import com.truecaller.api.services.messenger.v1.events.Event.h;
import com.truecaller.api.services.messenger.v1.events.Event.l;
import com.truecaller.messaging.h;
import dagger.a;
import javax.inject.Inject;

public final class n
  implements m
{
  private final bu a;
  private final a<com.truecaller.messaging.transport.m> b;
  private final h c;
  private final b d;
  
  @Inject
  public n(bu parambu, a<com.truecaller.messaging.transport.m> parama, h paramh, b paramb)
  {
    a = parambu;
    b = parama;
    c = paramh;
    d = paramb;
  }
  
  private final void b(Event paramEvent, boolean paramBoolean)
  {
    Intent localIntent = new Intent("event");
    localIntent.putExtra("event", paramEvent.toByteArray());
    localIntent.putExtra("from_push", paramBoolean);
    ((com.truecaller.messaging.transport.m)b.get()).a(2, localIntent, 0);
  }
  
  public final ProcessResult a(Event paramEvent, boolean paramBoolean)
  {
    k.b(paramEvent, "event");
    if (paramEvent.a() == Event.PayloadCase.INCOMPATIBLE_EVENT)
    {
      Object localObject1 = paramEvent.l();
      k.a(localObject1, "event.original");
      int i = ((Event.l)localObject1).a();
      if (a.b(i))
      {
        paramEvent = paramEvent.l();
        k.a(paramEvent, "event.original");
        paramEvent = Event.a(paramEvent.b());
        k.a(paramEvent, "Event.parseFrom(event.original.event)");
        b(paramEvent, paramBoolean);
      }
      else
      {
        localObject1 = paramEvent.l();
        k.a(localObject1, "event.original");
        localObject1 = Event.a(((Event.l)localObject1).b());
        k.a(localObject1, "Event.parseFrom(event.original.event)");
        Object localObject2 = paramEvent.l();
        k.a(localObject2, "event.original");
        int j = ((Event.l)localObject2).a();
        localObject2 = new Intent("unsupported_event");
        ((Intent)localObject2).putExtra("unsupported_event", ((Event)localObject1).toByteArray());
        ((Intent)localObject2).putExtra("api_version", j);
        ((com.truecaller.messaging.transport.m)b.get()).a(2, (Intent)localObject2, 0);
        paramEvent = paramEvent.k();
        k.a(paramEvent, "event.incompatibleEvent");
        if (!paramEvent.a())
        {
          c.h(i);
          paramEvent = d;
          localObject1 = new e.a("ImForceUpgradeEvent").a("ClientVersion", a.a()).a("ApiVersion", i).a();
          k.a(localObject1, "AnalyticsEvent.Builder(I…\n                .build()");
          paramEvent.a((e)localObject1);
          return ProcessResult.FORCE_UPGRADE_ENCOUNTERED;
        }
        a.a(i);
        c.i(true);
      }
    }
    else
    {
      b(paramEvent, paramBoolean);
    }
    return ProcessResult.SUCCESS;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */