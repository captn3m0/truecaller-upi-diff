package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import dagger.a.d;
import javax.inject.Provider;

public final class ag
  implements d<f<as>>
{
  private final Provider<as> a;
  private final Provider<i> b;
  
  private ag(Provider<as> paramProvider, Provider<i> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static ag a(Provider<as> paramProvider, Provider<i> paramProvider1)
  {
    return new ag(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.ag
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */