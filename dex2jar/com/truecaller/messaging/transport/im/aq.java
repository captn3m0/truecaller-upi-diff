package com.truecaller.messaging.transport.im;

import android.util.Base64;
import c.g.b.k;
import com.google.firebase.messaging.RemoteMessage;
import com.truecaller.api.services.messenger.v1.events.Event;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.h;
import com.truecaller.wizard.b.c;
import java.util.Map;
import javax.inject.Inject;

public final class aq
  implements ap
{
  private final h a;
  private final bw b;
  private final m c;
  private final bu d;
  
  @Inject
  public aq(h paramh, bw parambw, m paramm, bu parambu)
  {
    a = paramh;
    b = parambw;
    c = paramm;
    d = parambu;
  }
  
  public final void a(RemoteMessage paramRemoteMessage)
  {
    k.b(paramRemoteMessage, "remoteMessage");
    if ((b.a()) && (c.e()))
    {
      if (d.b()) {
        return;
      }
      a.H();
      paramRemoteMessage = Event.a(Base64.decode((String)paramRemoteMessage.a().get("payload"), 0));
      m localm = c;
      k.a(paramRemoteMessage, "event");
      localm.a(paramRemoteMessage, true);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.aq
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */