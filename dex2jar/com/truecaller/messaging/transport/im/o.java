package com.truecaller.messaging.transport.im;

import com.truecaller.analytics.b;
import com.truecaller.messaging.h;
import com.truecaller.messaging.transport.m;
import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d<n>
{
  private final Provider<bu> a;
  private final Provider<m> b;
  private final Provider<h> c;
  private final Provider<b> d;
  
  private o(Provider<bu> paramProvider, Provider<m> paramProvider1, Provider<h> paramProvider2, Provider<b> paramProvider3)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static o a(Provider<bu> paramProvider, Provider<m> paramProvider1, Provider<h> paramProvider2, Provider<b> paramProvider3)
  {
    return new o(paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */