package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.f;
import com.truecaller.common.account.k;
import com.truecaller.common.account.r;
import com.truecaller.common.edge.a;
import com.truecaller.messaging.h;
import com.truecaller.network.d.e;
import com.truecaller.util.bt;
import javax.inject.Provider;

public final class cd
  implements dagger.a.d<cc>
{
  private final Provider<r> a;
  private final Provider<k> b;
  private final Provider<com.truecaller.utils.d> c;
  private final Provider<e> d;
  private final Provider<Boolean> e;
  private final Provider<String> f;
  private final Provider<com.truecaller.network.d.b> g;
  private final Provider<a> h;
  private final Provider<com.truecaller.common.network.c> i;
  private final Provider<com.truecaller.d.b> j;
  private final Provider<com.truecaller.common.h.i> k;
  private final Provider<com.truecaller.network.a.d> l;
  private final Provider<h> m;
  private final Provider<com.truecaller.analytics.b> n;
  private final Provider<com.truecaller.utils.i> o;
  private final Provider<f<com.truecaller.presence.c>> p;
  private final Provider<bu> q;
  private final Provider<bt> r;
  
  private cd(Provider<r> paramProvider, Provider<k> paramProvider1, Provider<com.truecaller.utils.d> paramProvider2, Provider<e> paramProvider3, Provider<Boolean> paramProvider4, Provider<String> paramProvider5, Provider<com.truecaller.network.d.b> paramProvider6, Provider<a> paramProvider7, Provider<com.truecaller.common.network.c> paramProvider8, Provider<com.truecaller.d.b> paramProvider9, Provider<com.truecaller.common.h.i> paramProvider10, Provider<com.truecaller.network.a.d> paramProvider11, Provider<h> paramProvider12, Provider<com.truecaller.analytics.b> paramProvider13, Provider<com.truecaller.utils.i> paramProvider14, Provider<f<com.truecaller.presence.c>> paramProvider15, Provider<bu> paramProvider16, Provider<bt> paramProvider17)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
    k = paramProvider10;
    l = paramProvider11;
    m = paramProvider12;
    n = paramProvider13;
    o = paramProvider14;
    p = paramProvider15;
    q = paramProvider16;
    r = paramProvider17;
  }
  
  public static cd a(Provider<r> paramProvider, Provider<k> paramProvider1, Provider<com.truecaller.utils.d> paramProvider2, Provider<e> paramProvider3, Provider<Boolean> paramProvider4, Provider<String> paramProvider5, Provider<com.truecaller.network.d.b> paramProvider6, Provider<a> paramProvider7, Provider<com.truecaller.common.network.c> paramProvider8, Provider<com.truecaller.d.b> paramProvider9, Provider<com.truecaller.common.h.i> paramProvider10, Provider<com.truecaller.network.a.d> paramProvider11, Provider<h> paramProvider12, Provider<com.truecaller.analytics.b> paramProvider13, Provider<com.truecaller.utils.i> paramProvider14, Provider<f<com.truecaller.presence.c>> paramProvider15, Provider<bu> paramProvider16, Provider<bt> paramProvider17)
  {
    return new cd(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10, paramProvider11, paramProvider12, paramProvider13, paramProvider14, paramProvider15, paramProvider16, paramProvider17);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.cd
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */