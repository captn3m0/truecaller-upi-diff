package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.i;
import com.truecaller.androidactors.k;
import dagger.a.d;
import javax.inject.Provider;

public final class ah
  implements d<i>
{
  private final Provider<k> a;
  
  private ah(Provider<k> paramProvider)
  {
    a = paramProvider;
  }
  
  public static ah a(Provider<k> paramProvider)
  {
    return new ah(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.ah
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */