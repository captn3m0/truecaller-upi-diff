package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import dagger.a.d;
import javax.inject.Provider;

public final class ae
  implements d<f<q>>
{
  private final Provider<q> a;
  private final Provider<i> b;
  
  private ae(Provider<q> paramProvider, Provider<i> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static ae a(Provider<q> paramProvider, Provider<i> paramProvider1)
  {
    return new ae(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.ae
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */