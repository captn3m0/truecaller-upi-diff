package com.truecaller.messaging.transport.im;

import android.content.Context;
import android.os.Bundle;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.featuretoggles.b;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;

public final class JoinedImUsersNotificationTask
  extends PersistentBackgroundTask
{
  @Inject
  public by a;
  @Inject
  public com.truecaller.featuretoggles.e b;
  
  public JoinedImUsersNotificationTask()
  {
    TrueApp localTrueApp = TrueApp.y();
    k.a(localTrueApp, "TrueApp.getApp()");
    localTrueApp.a().a(this);
  }
  
  public final int a()
  {
    return 10033;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    k.b(paramContext, "serviceContext");
    paramContext = a;
    if (paramContext == null) {
      k.a("joinedImUsersManager");
    }
    paramContext.b();
    return PersistentBackgroundTask.RunResult.Success;
  }
  
  public final boolean a(Context paramContext)
  {
    k.b(paramContext, "serviceContext");
    paramContext = b;
    if (paramContext == null) {
      k.a("featureRegistry");
    }
    if (p.a(paramContext, com.truecaller.featuretoggles.e.a[44]).a())
    {
      paramContext = a;
      if (paramContext == null) {
        k.a("joinedImUsersManager");
      }
      if (paramContext.a()) {
        return true;
      }
    }
    return false;
  }
  
  public final com.truecaller.common.background.e b()
  {
    com.truecaller.common.background.e locale = new com.truecaller.common.background.e.a(1).a(2L, TimeUnit.HOURS).b(30L, TimeUnit.MINUTES).a(1).b();
    k.a(locale, "TaskConfiguration.Builde…YPE_ANY)\n        .build()");
    return locale;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.JoinedImUsersNotificationTask
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */