package com.truecaller.messaging.transport.im;

import android.content.ContentResolver;
import android.content.Context;
import com.truecaller.analytics.b;
import com.truecaller.messaging.transport.m;
import dagger.a.d;
import javax.inject.Provider;
import okhttp3.y;

public final class af
  implements d<q>
{
  private final Provider<Context> a;
  private final Provider<m> b;
  private final Provider<cb> c;
  private final Provider<com.truecaller.messaging.data.providers.c> d;
  private final Provider<b> e;
  private final Provider<y> f;
  private final Provider<com.truecaller.messaging.data.c> g;
  private final Provider<ContentResolver> h;
  
  private af(Provider<Context> paramProvider, Provider<m> paramProvider1, Provider<cb> paramProvider2, Provider<com.truecaller.messaging.data.providers.c> paramProvider3, Provider<b> paramProvider4, Provider<y> paramProvider5, Provider<com.truecaller.messaging.data.c> paramProvider6, Provider<ContentResolver> paramProvider7)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
  }
  
  public static af a(Provider<Context> paramProvider, Provider<m> paramProvider1, Provider<cb> paramProvider2, Provider<com.truecaller.messaging.data.providers.c> paramProvider3, Provider<b> paramProvider4, Provider<y> paramProvider5, Provider<com.truecaller.messaging.data.c> paramProvider6, Provider<ContentResolver> paramProvider7)
  {
    return new af(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.af
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */