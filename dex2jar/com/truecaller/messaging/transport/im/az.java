package com.truecaller.messaging.transport.im;

import android.os.HandlerThread;
import android.os.Looper;
import c.g.b.k;
import com.truecaller.api.services.messenger.v1.events.Event.a;
import com.truecaller.messaging.conversation.bw;
import javax.inject.Inject;

public final class az
  implements ay
{
  io.grpc.c.f<Event.a> a;
  ch b;
  boolean c;
  final cb d;
  final m e;
  final bu f;
  final bw g;
  final com.truecaller.androidactors.f<bj> h;
  private final Runnable i;
  private HandlerThread j;
  private final com.truecaller.utils.a k;
  private final a l;
  
  @Inject
  public az(com.truecaller.utils.a parama, a parama1, cb paramcb, m paramm, bu parambu, bw parambw, com.truecaller.androidactors.f<bj> paramf)
  {
    k = parama;
    l = parama1;
    d = paramcb;
    e = paramm;
    f = parambu;
    g = parambw;
    h = paramf;
    i = ((Runnable)new az.b(this));
  }
  
  private final void d()
  {
    HandlerThread localHandlerThread = j;
    if (localHandlerThread == null) {
      k.a("thread");
    }
    localHandlerThread.quitSafely();
  }
  
  public final void a()
  {
    j = new HandlerThread("im_subscription");
    Object localObject = j;
    if (localObject == null) {
      k.a("thread");
    }
    ((HandlerThread)localObject).start();
    localObject = j;
    if (localObject == null) {
      k.a("thread");
    }
    localObject = ((HandlerThread)localObject).getLooper();
    k.a(localObject, "thread.looper");
    b = new ch(this, (Looper)localObject);
    localObject = b;
    if (localObject == null) {
      k.a("handler");
    }
    ((ch)localObject).post(i);
  }
  
  final void a(boolean paramBoolean)
  {
    a = null;
    if (!c)
    {
      long l1 = l.a(k.b(), paramBoolean);
      Object localObject = new StringBuilder("Retrying subscription in ");
      ((StringBuilder)localObject).append(l1);
      ((StringBuilder)localObject).append(" ms");
      ((StringBuilder)localObject).toString();
      localObject = b;
      if (localObject == null) {
        k.a("handler");
      }
      ((ch)localObject).postDelayed(i, l1);
      return;
    }
    d();
  }
  
  public final void b()
  {
    ch localch = b;
    if (localch == null) {
      k.a("handler");
    }
    localch.post((Runnable)new az.a(this));
  }
  
  final void c()
  {
    c = true;
    Object localObject = b;
    if (localObject == null) {
      k.a("handler");
    }
    ((ch)localObject).removeCallbacks(i);
    localObject = a;
    if (localObject != null)
    {
      if (localObject != null) {
        ((io.grpc.c.f)localObject).a();
      }
      return;
    }
    d();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.az
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */