package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.k;
import com.truecaller.messaging.transport.im.a.a;
import dagger.a.d;
import javax.inject.Provider;

public final class ad
  implements d<f<a>>
{
  private final Provider<k> a;
  private final Provider<a> b;
  
  private ad(Provider<k> paramProvider, Provider<a> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static ad a(Provider<k> paramProvider, Provider<a> paramProvider1)
  {
    return new ad(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.ad
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */