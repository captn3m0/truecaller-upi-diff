package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.w;
import java.util.Collection;
import java.util.List;

public abstract interface bp
{
  public abstract w<Boolean> a();
  
  public abstract w<List<bx>> a(long paramLong);
  
  public abstract w<Boolean> a(String paramString);
  
  public abstract w<Boolean> a(Collection<String> paramCollection, boolean paramBoolean);
  
  public abstract void a(String paramString1, String paramString2);
  
  public abstract void a(Collection<String> paramCollection);
  
  public abstract void a(List<String> paramList);
  
  public abstract w<Boolean> b(String paramString);
  
  public abstract w<String> c(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bp
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */