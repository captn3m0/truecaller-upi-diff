package com.truecaller.messaging.transport.im;

import c.a.m;
import com.truecaller.androidactors.w;
import com.truecaller.api.services.messenger.v1.k.a;
import com.truecaller.api.services.messenger.v1.k.b;
import com.truecaller.api.services.messenger.v1.l.b;
import com.truecaller.api.services.messenger.v1.l.d;
import com.truecaller.common.account.r;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.messaging.h;
import com.truecaller.util.bt;
import io.grpc.ao;
import io.grpc.ao.e;
import io.grpc.ba;
import io.grpc.ba.a;
import io.grpc.bc;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import javax.inject.Named;

public final class cc
  extends com.truecaller.network.d.k<k.b, k.a>
  implements cb
{
  private final com.truecaller.network.a.d a;
  private final h b;
  private final com.truecaller.analytics.b c;
  private final com.truecaller.utils.i d;
  private final dagger.a<com.truecaller.androidactors.f<com.truecaller.presence.c>> e;
  private final bu f;
  private final bt g;
  
  @Inject
  public cc(r paramr, com.truecaller.common.account.k paramk, com.truecaller.utils.d paramd, com.truecaller.network.d.e parame, @Named("ssl_patches_applied") boolean paramBoolean, @Named("grpc_user_agent") String paramString, com.truecaller.network.d.b paramb, com.truecaller.common.edge.a parama, com.truecaller.common.network.c paramc, com.truecaller.d.b paramb1, @Named("im_cross_domain_support") com.truecaller.common.h.i parami, com.truecaller.network.a.d paramd1, h paramh, com.truecaller.analytics.b paramb2, com.truecaller.utils.i parami1, dagger.a<com.truecaller.androidactors.f<com.truecaller.presence.c>> parama1, bu parambu, bt parambt)
  {
    super(KnownEndpoints.MESSENGER, paramr, paramk, paramd, Integer.valueOf(20), paramb, parama, paramc, paramBoolean, parame, paramString, paramb1, parami);
    a = paramd1;
    b = paramh;
    c = paramb2;
    d = parami1;
    e = parama1;
    f = parambu;
    g = parambt;
  }
  
  private final <S extends io.grpc.c.a<S>> S a(S paramS)
  {
    ao localao = new ao();
    localao.a(ao.e.a("Version", ao.b), String.valueOf(f.a()));
    Object localObject = (List)new ArrayList();
    if (g.c()) {
      ((List)localObject).add("versioning");
    }
    if (g.d()) {
      ((List)localObject).add("commands");
    }
    if (g.e()) {
      ((List)localObject).add("no-user-info");
    }
    if (b.M()) {
      ((List)localObject).add("tracing");
    }
    if ((((Collection)localObject).isEmpty() ^ true))
    {
      localObject = m.a((Iterable)localObject, (CharSequence)",", null, null, 0, null, null, 62);
      localao.a(ao.e.a("Debug", ao.b), localObject);
    }
    paramS = io.grpc.c.e.a(paramS, localao);
    c.g.b.k.a(paramS, "MetadataUtils.attachHeaders(this, metadata)");
    return paramS;
  }
  
  private final String b()
  {
    if (d.a()) {
      return d.b();
    }
    return "no-connection";
  }
  
  private final boolean c()
  {
    for (;;)
    {
      try
      {
        CharSequence localCharSequence = (CharSequence)b.G();
        if (localCharSequence == null) {
          break label120;
        }
        if (localCharSequence.length() != 0) {
          break label115;
        }
      }
      finally {}
      if (i != 0)
      {
        boolean bool = d();
        if (!bool) {
          return false;
        }
      }
      if (!b.Q()) {
        b.h(c.g.b.k.a((Boolean)((com.truecaller.presence.c)((com.truecaller.androidactors.f)e.get()).a()).a().d(), Boolean.TRUE));
      }
      return true;
      label115:
      int i = 0;
      continue;
      label120:
      i = 1;
    }
  }
  
  private final boolean d()
  {
    Object localObject1 = (k.a)super.b((com.truecaller.common.network.e)com.truecaller.common.network.e.a.a);
    if (localObject1 == null) {
      return false;
    }
    Object localObject3 = c;
    Object localObject4 = new com.truecaller.analytics.e.a("IMRequest").a("Type", "GetPeerImId").a("ConnectionType", b()).a("Status", "Attempt").a();
    c.g.b.k.a(localObject4, "AnalyticsEvent.Builder(A…\n                .build()");
    ((com.truecaller.analytics.b)localObject3).a((com.truecaller.analytics.e)localObject4);
    for (;;)
    {
      try
      {
        localObject1 = ((k.a)localObject1).a(l.b.a());
        localObject3 = b;
        if (localObject1 != null)
        {
          localObject1 = ((l.d)localObject1).a();
          ((h)localObject3).d((String)localObject1);
          b.h(false);
          return true;
        }
      }
      catch (RuntimeException localRuntimeException)
      {
        Object localObject5 = (Throwable)localRuntimeException;
        if ((localObject5 instanceof bc))
        {
          localObject3 = c;
          localObject4 = new com.truecaller.analytics.e.a("IMRequest").a("Type", "GetPeerImId").a("ConnectionType", b()).a("Status", "Failure");
          localObject5 = ((bc)localObject5).a();
          c.g.b.k.a(localObject5, "error.status");
          localObject4 = ((com.truecaller.analytics.e.a)localObject4).a("ErrorCode", ((ba)localObject5).a().name()).a();
          c.g.b.k.a(localObject4, "AnalyticsEvent.Builder(A…status.code.name).build()");
          ((com.truecaller.analytics.b)localObject3).a((com.truecaller.analytics.e)localObject4);
        }
        "Failed to register to IM: ".concat(String.valueOf(localRuntimeException));
        return false;
      }
      Object localObject2 = null;
    }
  }
  
  public final Collection<io.grpc.i> a()
  {
    return (Collection)m.a(new com.truecaller.network.d.f(a, b));
  }
  
  public final void a(io.grpc.okhttp.e parame)
  {
    c.g.b.k.b(parame, "builder");
    parame.b(TimeUnit.SECONDS);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.cc
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */