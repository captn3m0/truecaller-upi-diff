package com.truecaller.messaging.transport.im;

import android.content.Context;
import com.truecaller.network.d.b;
import com.truecaller.utils.i;
import dagger.a.d;
import javax.inject.Provider;

public final class z
  implements d<b>
{
  private final Provider<Context> a;
  private final Provider<i> b;
  
  private z(Provider<Context> paramProvider, Provider<i> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static z a(Provider<Context> paramProvider, Provider<i> paramProvider1)
  {
    return new z(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.z
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */