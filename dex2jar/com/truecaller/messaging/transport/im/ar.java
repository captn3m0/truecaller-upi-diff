package com.truecaller.messaging.transport.im;

import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.h;
import dagger.a.d;
import javax.inject.Provider;

public final class ar
  implements d<aq>
{
  private final Provider<h> a;
  private final Provider<bw> b;
  private final Provider<m> c;
  private final Provider<bu> d;
  
  private ar(Provider<h> paramProvider, Provider<bw> paramProvider1, Provider<m> paramProvider2, Provider<bu> paramProvider3)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static ar a(Provider<h> paramProvider, Provider<bw> paramProvider1, Provider<m> paramProvider2, Provider<bu> paramProvider3)
  {
    return new ar(paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.ar
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */