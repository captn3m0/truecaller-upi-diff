package com.truecaller.messaging.transport.im;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Parcelable;
import android.support.v4.app.z.c;
import android.support.v4.app.z.d;
import android.support.v4.app.z.g;
import c.g.b.k;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.common.h.an;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Participant.a;
import com.truecaller.messaging.h;
import com.truecaller.notificationchannels.e;
import com.truecaller.notifications.a;
import com.truecaller.notifications.l;
import com.truecaller.notifications.l.a;
import com.truecaller.util.af;
import com.truecaller.util.al;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import org.a.a.a.g;

public final class bz
  implements by
{
  final al a;
  final Context b;
  private final bw c;
  private final f<bp> d;
  private final h e;
  private final af f;
  private final a g;
  private final l h;
  private final e i;
  private final com.truecaller.common.h.u j;
  private final an k;
  
  @Inject
  public bz(bw parambw, f<bp> paramf, al paramal, h paramh, af paramaf, Context paramContext, a parama, l paraml, e parame, com.truecaller.common.h.u paramu, an paraman)
  {
    c = parambw;
    d = paramf;
    a = paramal;
    e = paramh;
    f = paramaf;
    b = paramContext;
    g = parama;
    h = paraml;
    i = parame;
    j = paramu;
    k = paraman;
  }
  
  private final Participant a(bx parambx)
  {
    Object localObject1 = b;
    Object localObject2 = j;
    localObject1 = Participant.a((String)localObject1, (com.truecaller.common.h.u)localObject2, ((com.truecaller.common.h.u)localObject2).a()).l();
    localObject2 = d;
    if (localObject2 != null) {
      ((Participant.a)localObject1).c(((Number)localObject2).longValue());
    }
    localObject2 = c;
    if (localObject2 != null) {
      ((Participant.a)localObject1).g((String)localObject2);
    }
    parambx = a;
    if (parambx != null) {
      ((Participant.a)localObject1).f(parambx);
    }
    return ((Participant.a)localObject1).a();
  }
  
  public final boolean a()
  {
    org.a.a.b localb1 = new org.a.a.b().az_();
    k.a(localb1, "DateTime().withTimeAtStartOfDay()");
    af localaf = f;
    org.a.a.b localb2 = localaf.b();
    org.a.a.b localb3 = localb1.b(22);
    k.a(localb3, "startOfDay.plusHours(22)");
    if (localaf.a(localb2, localb3))
    {
      localaf = f;
      localb2 = localaf.b();
      localb1 = localb1.b(18);
      k.a(localb1, "startOfDay.plusHours(18)");
      if (localaf.b(localb2, localb1))
      {
        m = 1;
        break label109;
      }
    }
    int m = 0;
    label109:
    localb1 = e.h();
    k.a(localb1, "settings.lastJoinUserNotificationDate");
    int n;
    if ((a != 0L) && (!k.a(a, 7L, TimeUnit.DAYS))) {
      n = 0;
    } else {
      n = 1;
    }
    return (c.a()) && (a.c()) && (e.R() > 0L) && (m != 0) && (n != 0);
  }
  
  public final void b()
  {
    long l2 = e.R();
    long l1 = l2;
    if (l2 > f.c())
    {
      l1 = 0L;
      e.f(f.c());
    }
    Object localObject4 = (Collection)((bp)d.a()).a(l1).d();
    if (localObject4 != null)
    {
      if ((((Collection)localObject4).isEmpty() ^ true) == true)
      {
        e.b(f.b());
        Object localObject6 = b.getResources();
        Object localObject2;
        Object localObject1;
        Object localObject3;
        if (localObject6 != null)
        {
          Object localObject5 = (Iterable)localObject4;
          localObject2 = ((Iterable)localObject5).iterator();
          int m;
          while (((Iterator)localObject2).hasNext())
          {
            localObject1 = ((Iterator)localObject2).next();
            localObject3 = (bx)localObject1;
            localObject7 = (CharSequence)c;
            if ((localObject7 != null) && (((CharSequence)localObject7).length() != 0)) {
              m = 0;
            } else {
              m = 1;
            }
            if (m == 0)
            {
              localObject3 = (CharSequence)a;
              if ((localObject3 != null) && (((CharSequence)localObject3).length() != 0)) {
                m = 0;
              } else {
                m = 1;
              }
              if (m == 0)
              {
                m = 1;
                break label255;
              }
            }
            m = 0;
            label255:
            if (m != 0) {
              break label265;
            }
          }
          localObject1 = null;
          label265:
          localObject2 = (bx)localObject1;
          localObject1 = localObject2;
          if (localObject2 == null)
          {
            localObject2 = ((Iterable)localObject5).iterator();
            while (((Iterator)localObject2).hasNext())
            {
              localObject1 = ((Iterator)localObject2).next();
              localObject3 = (CharSequence)a;
              if ((localObject3 != null) && (((CharSequence)localObject3).length() != 0)) {
                m = 0;
              } else {
                m = 1;
              }
              if ((m ^ 0x1) != 0) {
                break label359;
              }
            }
            localObject1 = null;
            label359:
            localObject1 = (bx)localObject1;
          }
          localObject3 = localObject1;
          if (localObject1 == null) {
            localObject3 = (bx)c.a.m.b((Iterable)localObject5);
          }
          localObject1 = a;
          if (localObject1 != null)
          {
            localObject1 = c.n.m.c((CharSequence)localObject1, new String[] { " " }, false, 6);
            if (localObject1 != null)
            {
              localObject1 = (String)((List)localObject1).get(0);
              localObject2 = localObject1;
              if (localObject1 != null) {
                break label454;
              }
            }
          }
          localObject2 = a;
          label454:
          localObject1 = localObject2;
          if (localObject2 == null) {
            localObject1 = b;
          }
          if (((Collection)localObject4).size() == 1)
          {
            localObject2 = localObject1;
          }
          else
          {
            localObject2 = ((Resources)localObject6).getString(2131888306, new Object[] { localObject1, Integer.valueOf(((Collection)localObject4).size() - 1) });
            k.a(localObject2, "resources.getString(R.st…, joinedImUsers.size - 1)");
          }
          Object localObject7 = ((Resources)localObject6).getString(2131888307, new Object[] { localObject1 });
          localObject2 = ((Resources)localObject6).getString(2131888305, new Object[] { "👋", localObject2 });
          if (((Collection)localObject4).size() == 1)
          {
            localObject1 = b;
            localObject5 = a((bx)c.a.m.b((Iterable)localObject5));
            k.b(localObject1, "context");
            localObject6 = new Intent((Context)localObject1, JoinedImUsersBroadcastReceiver.class);
            ((Intent)localObject6).setAction("com.truecaller.open_conversation");
            ((Intent)localObject6).putExtra("participant", (Parcelable)localObject5);
            localObject1 = PendingIntent.getBroadcast((Context)localObject1, 0, (Intent)localObject6, 268435456);
            k.a(localObject1, "PendingIntent.getBroadca…tent.FLAG_CANCEL_CURRENT)");
          }
          else
          {
            localObject1 = b;
            localObject6 = (Collection)new ArrayList();
            localObject5 = ((Iterable)localObject5).iterator();
            while (((Iterator)localObject5).hasNext())
            {
              Participant localParticipant = a((bx)((Iterator)localObject5).next());
              if (localParticipant != null) {
                ((Collection)localObject6).add(localParticipant);
              }
            }
            localObject5 = ((Collection)localObject6).toArray(new Participant[0]);
            if (localObject5 == null) {
              break label1138;
            }
            localObject5 = (Participant[])localObject5;
            k.b(localObject1, "context");
            k.b(localObject5, "participantList");
            localObject6 = new Intent((Context)localObject1, JoinedImUsersBroadcastReceiver.class);
            ((Intent)localObject6).setAction("com.truecaller.open_new_conversation");
            ((Intent)localObject6).putExtra("participant_list", (Parcelable[])localObject5);
            localObject1 = PendingIntent.getBroadcast((Context)localObject1, 0, (Intent)localObject6, 268435456);
            k.a(localObject1, "PendingIntent.getBroadca…tent.FLAG_CANCEL_CURRENT)");
          }
          localObject5 = g.a((PendingIntent)localObject1, "notificationJoinedImUsers", "Opened");
          localObject6 = i.a();
          localObject6 = new z.d(b, (String)localObject6).a((CharSequence)localObject7);
          localObject2 = (CharSequence)localObject2;
          localObject2 = ((z.d)localObject6).b((CharSequence)localObject2).a((z.g)new z.c().b((CharSequence)localObject2)).f(android.support.v4.content.b.c(b, 2131100594)).a(2131234787).c(-1).e().a((PendingIntent)localObject1).a(0, (CharSequence)b.getString(2131888304), (PendingIntent)localObject5);
          localObject1 = g;
          localObject2 = h.a((z.d)localObject2, (l.a)new bz.a(this, (bx)localObject3));
          k.a(localObject2, "notificationIconHelper.c…lder) { getAvatar(user) }");
          ((a)localObject1).a(2131363574, (Notification)localObject2, "notificationJoinedImUsers");
        }
        else
        {
          localObject1 = (bp)d.a();
          localObject3 = (Iterable)localObject4;
          localObject2 = (Collection)new ArrayList();
          localObject3 = ((Iterable)localObject3).iterator();
          while (((Iterator)localObject3).hasNext())
          {
            localObject4 = nextb;
            if (localObject4 != null) {
              ((Collection)localObject2).add(localObject4);
            }
          }
          ((bp)localObject1).a((List)localObject2);
          return;
        }
        label1138:
        throw new c.u("null cannot be cast to non-null type kotlin.Array<T>");
      }
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bz
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */