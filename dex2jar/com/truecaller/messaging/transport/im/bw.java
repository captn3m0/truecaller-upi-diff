package com.truecaller.messaging.transport.im;

import c.d.f;
import com.truecaller.common.g.a;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.h;
import dagger.a.d;
import javax.inject.Provider;

public final class bw
  implements d<bv>
{
  private final Provider<h> a;
  private final Provider<a> b;
  private final Provider<e> c;
  private final Provider<cb> d;
  private final Provider<f> e;
  private final Provider<f> f;
  
  private bw(Provider<h> paramProvider, Provider<a> paramProvider1, Provider<e> paramProvider2, Provider<cb> paramProvider3, Provider<f> paramProvider4, Provider<f> paramProvider5)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
  }
  
  public static bw a(Provider<h> paramProvider, Provider<a> paramProvider1, Provider<e> paramProvider2, Provider<cb> paramProvider3, Provider<f> paramProvider4, Provider<f> paramProvider5)
  {
    return new bw(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bw
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */