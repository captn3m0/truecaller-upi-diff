package com.truecaller.messaging.transport.im;

import android.content.ContentResolver;
import com.truecaller.analytics.b;
import com.truecaller.messaging.data.providers.c;
import dagger.a.d;
import javax.inject.Provider;
import okhttp3.y;

public final class i
  implements d<g>
{
  private final Provider<ContentResolver> a;
  private final Provider<com.truecaller.util.g> b;
  private final Provider<y> c;
  private final Provider<b> d;
  private final Provider<cb> e;
  private final Provider<cl> f;
  private final Provider<c> g;
  
  private i(Provider<ContentResolver> paramProvider, Provider<com.truecaller.util.g> paramProvider1, Provider<y> paramProvider2, Provider<b> paramProvider3, Provider<cb> paramProvider4, Provider<cl> paramProvider5, Provider<c> paramProvider6)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
  }
  
  public static i a(Provider<ContentResolver> paramProvider, Provider<com.truecaller.util.g> paramProvider1, Provider<y> paramProvider2, Provider<b> paramProvider3, Provider<cb> paramProvider4, Provider<cl> paramProvider5, Provider<c> paramProvider6)
  {
    return new i(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */