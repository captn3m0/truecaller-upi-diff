package com.truecaller.messaging.transport.im;

import android.net.Uri;
import com.truecaller.api.services.messenger.v1.models.MessageContent;
import com.truecaller.api.services.messenger.v1.models.input.InputMessageContent.c;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;

public abstract interface f
{
  public abstract Uri a(long paramLong, byte[] paramArrayOfByte, int paramInt1, int paramInt2, String paramString);
  
  public abstract Entity a(MessageContent paramMessageContent, int paramInt);
  
  public abstract void a(InputMessageContent.c paramc, BinaryEntity paramBinaryEntity, Message paramMessage);
  
  public abstract void a(BinaryEntity paramBinaryEntity);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */