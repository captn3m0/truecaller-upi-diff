package com.truecaller.messaging.transport.im;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import androidx.work.e;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.f;
import com.truecaller.bp;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.data.t;
import javax.inject.Inject;
import org.a.a.b;

public final class RetryImMessageWorker
  extends Worker
{
  public static final RetryImMessageWorker.a d = new RetryImMessageWorker.a((byte)0);
  @Inject
  public bw b;
  @Inject
  public f<t> c;
  
  public RetryImMessageWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public final ListenableWorker.a a()
  {
    Object localObject = b;
    if (localObject == null) {
      k.a("imStatusProvider");
    }
    if (!((bw)localObject).a())
    {
      localObject = ListenableWorker.a.a();
      k.a(localObject, "Result.success()");
      return (ListenableWorker.a)localObject;
    }
    long l = getInputData().a("to_date", 0L);
    if (l == 0L)
    {
      localObject = ListenableWorker.a.c();
      k.a(localObject, "Result.failure()");
      return (ListenableWorker.a)localObject;
    }
    localObject = c;
    if (localObject == null) {
      k.a("messagesStorage");
    }
    ((t)((f)localObject).a()).a(2, new b(l));
    localObject = ListenableWorker.a.a();
    k.a(localObject, "Result.success()");
    return (ListenableWorker.a)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.RetryImMessageWorker
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */