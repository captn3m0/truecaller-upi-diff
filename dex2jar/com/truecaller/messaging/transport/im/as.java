package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.w;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Reaction;
import java.util.Map;

public abstract interface as
{
  public abstract w<Boolean> a(Reaction[] paramArrayOfReaction);
  
  public abstract void a();
  
  public abstract void a(long paramLong);
  
  public abstract void a(long[] paramArrayOfLong);
  
  public abstract void b(long paramLong);
  
  public abstract void c(long paramLong);
  
  public abstract w<String> d(long paramLong);
  
  public abstract w<Map<Reaction, Participant>> e(long paramLong);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.as
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */