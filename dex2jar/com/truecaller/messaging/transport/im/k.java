package com.truecaller.messaging.transport.im;

import android.content.ContentResolver;
import com.truecaller.common.background.b;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.h;
import com.truecaller.util.af;
import com.truecaller.util.al;
import javax.inject.Inject;

public final class k
  implements j
{
  private final af a;
  private final ContentResolver b;
  private final h c;
  private final bp d;
  private final bw e;
  private final al f;
  private final b g;
  
  @Inject
  public k(af paramaf, ContentResolver paramContentResolver, h paramh, bp parambp, bw parambw, al paramal, b paramb)
  {
    a = paramaf;
    b = paramContentResolver;
    c = paramh;
    d = parambp;
    e = parambw;
    f = paramal;
    g = paramb;
  }
  
  public final boolean a()
  {
    return (e.a()) && (f.c());
  }
  
  /* Error */
  @android.annotation.SuppressLint({"Recycle"})
  public final void b()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 49	com/truecaller/messaging/transport/im/k:b	Landroid/content/ContentResolver;
    //   4: astore_2
    //   5: invokestatic 80	com/truecaller/content/TruecallerContract$k:a	()Landroid/net/Uri;
    //   8: astore_3
    //   9: iconst_0
    //   10: istore_1
    //   11: getstatic 85	com/google/c/a/k$d:b	Lcom/google/c/a/k$d;
    //   14: invokevirtual 89	com/google/c/a/k$d:name	()Ljava/lang/String;
    //   17: astore 4
    //   19: aload_2
    //   20: aload_3
    //   21: iconst_1
    //   22: anewarray 91	java/lang/String
    //   25: dup
    //   26: iconst_0
    //   27: ldc 93
    //   29: aastore
    //   30: ldc 95
    //   32: iconst_2
    //   33: anewarray 91	java/lang/String
    //   36: dup
    //   37: iconst_0
    //   38: ldc 97
    //   40: aastore
    //   41: dup
    //   42: iconst_1
    //   43: aload 4
    //   45: aastore
    //   46: aconst_null
    //   47: invokevirtual 103	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   50: astore 5
    //   52: aload 5
    //   54: ifnull +181 -> 235
    //   57: aload 5
    //   59: checkcast 105	java/io/Closeable
    //   62: astore 4
    //   64: aconst_null
    //   65: astore_3
    //   66: aload_3
    //   67: astore_2
    //   68: new 107	java/util/ArrayList
    //   71: dup
    //   72: invokespecial 108	java/util/ArrayList:<init>	()V
    //   75: checkcast 110	java/util/Collection
    //   78: astore 6
    //   80: aload_3
    //   81: astore_2
    //   82: aload 5
    //   84: invokeinterface 115 1 0
    //   89: ifeq +24 -> 113
    //   92: aload_3
    //   93: astore_2
    //   94: aload 6
    //   96: aload 5
    //   98: iconst_0
    //   99: invokeinterface 119 2 0
    //   104: invokeinterface 123 2 0
    //   109: pop
    //   110: goto -30 -> 80
    //   113: aload_3
    //   114: astore_2
    //   115: aload 6
    //   117: checkcast 125	java/util/List
    //   120: astore_3
    //   121: aload 4
    //   123: aconst_null
    //   124: invokestatic 130	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   127: aload_3
    //   128: invokeinterface 133 1 0
    //   133: ifeq +4 -> 137
    //   136: return
    //   137: aload_0
    //   138: getfield 51	com/truecaller/messaging/transport/im/k:c	Lcom/truecaller/messaging/h;
    //   141: invokeinterface 139 1 0
    //   146: lconst_0
    //   147: lcmp
    //   148: ifle +17 -> 165
    //   151: aload_0
    //   152: getfield 53	com/truecaller/messaging/transport/im/k:d	Lcom/truecaller/messaging/transport/im/bp;
    //   155: aload_3
    //   156: checkcast 110	java/util/Collection
    //   159: invokeinterface 144 2 0
    //   164: return
    //   165: aload_0
    //   166: getfield 53	com/truecaller/messaging/transport/im/k:d	Lcom/truecaller/messaging/transport/im/bp;
    //   169: aload_3
    //   170: checkcast 110	java/util/Collection
    //   173: iconst_0
    //   174: invokeinterface 147 3 0
    //   179: invokevirtual 152	com/truecaller/androidactors/w:d	()Ljava/lang/Object;
    //   182: checkcast 154	java/lang/Boolean
    //   185: astore_2
    //   186: aload_2
    //   187: ifnull +8 -> 195
    //   190: aload_2
    //   191: invokevirtual 157	java/lang/Boolean:booleanValue	()Z
    //   194: istore_1
    //   195: iload_1
    //   196: ifeq +21 -> 217
    //   199: aload_0
    //   200: getfield 51	com/truecaller/messaging/transport/im/k:c	Lcom/truecaller/messaging/h;
    //   203: aload_0
    //   204: getfield 47	com/truecaller/messaging/transport/im/k:a	Lcom/truecaller/util/af;
    //   207: invokeinterface 161 1 0
    //   212: invokeinterface 164 3 0
    //   217: return
    //   218: astore_3
    //   219: goto +8 -> 227
    //   222: astore_3
    //   223: aload_3
    //   224: astore_2
    //   225: aload_3
    //   226: athrow
    //   227: aload 4
    //   229: aload_2
    //   230: invokestatic 130	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   233: aload_3
    //   234: athrow
    //   235: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	236	0	this	k
    //   10	186	1	bool	boolean
    //   4	226	2	localObject1	Object
    //   8	162	3	localObject2	Object
    //   218	1	3	localObject3	Object
    //   222	12	3	localThrowable	Throwable
    //   17	211	4	localObject4	Object
    //   50	47	5	localCursor	android.database.Cursor
    //   78	38	6	localCollection	java.util.Collection
    // Exception table:
    //   from	to	target	type
    //   68	80	218	finally
    //   82	92	218	finally
    //   94	110	218	finally
    //   115	121	218	finally
    //   225	227	218	finally
    //   68	80	222	java/lang/Throwable
    //   82	92	222	java/lang/Throwable
    //   94	110	222	java/lang/Throwable
    //   115	121	222	java/lang/Throwable
  }
  
  public final void c()
  {
    g.b(10031);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */