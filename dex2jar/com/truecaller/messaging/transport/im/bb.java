package com.truecaller.messaging.transport.im;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.Context;
import androidx.work.g;
import androidx.work.p;
import c.a.an;
import c.g.b.k;
import com.truecaller.api.services.messenger.v1.models.input.InputReportType;
import com.truecaller.content.TruecallerContract.w;
import com.truecaller.featuretoggles.e;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.transport.c;
import com.truecaller.messaging.transport.f;
import com.truecaller.messaging.transport.i;
import com.truecaller.multisim.h;
import java.util.List;
import java.util.Set;

public final class bb
  extends c<ImTransportInfo, v>
{
  private final p d;
  
  public bb(Context paramContext, h paramh, e parame, p paramp)
  {
    super(paramContext, paramh, parame);
    d = paramp;
  }
  
  private final void a(List<ContentProviderOperation> paramList, r paramr, v paramv, InputReportType paramInputReportType)
  {
    int i;
    if (paramv.l() == 1) {
      i = 1;
    } else {
      i = 0;
    }
    paramr = ContentProviderOperation.newUpdate(TruecallerContract.w.a()).withSelection("message_id=?", new String[] { String.valueOf(paramr.a()) });
    int j;
    if (i != 0) {
      j = 5;
    } else {
      j = 2;
    }
    switch (bc.a[paramInputReportType.ordinal()])
    {
    default: 
      AssertionUtil.AlwaysFatal.fail(new String[] { "Unknown report type ".concat(String.valueOf(paramInputReportType)) });
      break;
    case 2: 
      paramr.withValue("read_sync_status", Integer.valueOf(j));
      break;
    case 1: 
      paramr.withValue("delivery_sync_status", Integer.valueOf(j));
    }
    paramr = paramr.build();
    k.a(paramr, "update.build()");
    paramList.add(paramr);
    if (i != 0) {
      return;
    }
    paramList = SendImReportWorker.c;
    paramList = SendImReportWorker.a.a(paramInputReportType, paramv.m(), paramv.k(), paramv.p());
    d.a("SendImReportV2", g.c, paramList);
  }
  
  private static boolean a(r paramr, v paramv)
  {
    int i;
    if (((CharSequence)paramv.m()).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    return (i != 0) && ((paramr.i() & 0x20) != 0);
  }
  
  private static boolean a(v paramv)
  {
    return paramv.n() == 1;
  }
  
  private static boolean b(v paramv)
  {
    return paramv.o() == 1;
  }
  
  public final Set<Participant> a(long paramLong, f paramf, i parami, Participant paramParticipant, boolean paramBoolean)
  {
    k.b(paramf, "threadInfoCache");
    k.b(parami, "participantCache");
    k.b(paramParticipant, "participant");
    return an.b(new Participant[] { paramParticipant });
  }
  
  public final boolean a(int paramInt)
  {
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bb
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */