package com.truecaller.messaging.transport.im;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import c.g.b.k;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public final class aw
  implements Application.ActivityLifecycleCallbacks
{
  private final Map<Activity, ServiceConnection> a = (Map)new LinkedHashMap();
  
  public final void onActivityCreated(Activity paramActivity, Bundle paramBundle) {}
  
  public final void onActivityDestroyed(Activity paramActivity) {}
  
  public final void onActivityPaused(Activity paramActivity) {}
  
  public final void onActivityResumed(Activity paramActivity) {}
  
  public final void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle) {}
  
  public final void onActivityStarted(Activity paramActivity)
  {
    k.b(paramActivity, "activity");
    if (!ax.a().contains(paramActivity.getClass())) {
      return;
    }
    ci localci = new ci();
    a.put(paramActivity, localci);
    paramActivity.bindService(new Intent((Context)paramActivity, ImSubscriptionService.class), (ServiceConnection)localci, 1);
  }
  
  public final void onActivityStopped(Activity paramActivity)
  {
    k.b(paramActivity, "activity");
    ServiceConnection localServiceConnection = (ServiceConnection)a.remove(paramActivity);
    if (localServiceConnection != null)
    {
      paramActivity.unbindService(localServiceConnection);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.aw
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */