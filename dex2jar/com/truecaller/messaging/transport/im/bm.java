package com.truecaller.messaging.transport.im;

import android.content.ContentResolver;
import dagger.a.d;
import javax.inject.Provider;

public final class bm
  implements d<bl>
{
  private final Provider<ContentResolver> a;
  private final Provider<bu> b;
  private final Provider<m> c;
  
  private bm(Provider<ContentResolver> paramProvider, Provider<bu> paramProvider1, Provider<m> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static bm a(Provider<ContentResolver> paramProvider, Provider<bu> paramProvider1, Provider<m> paramProvider2)
  {
    return new bm(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bm
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */