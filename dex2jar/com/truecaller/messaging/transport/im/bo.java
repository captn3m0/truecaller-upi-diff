package com.truecaller.messaging.transport.im;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.api.services.messenger.v1.MediaHandles.Request;
import com.truecaller.api.services.messenger.v1.MediaHandles.Request.a;
import com.truecaller.api.services.messenger.v1.MediaHandles.c;
import com.truecaller.api.services.messenger.v1.k.a;
import com.truecaller.log.AssertionUtil;
import com.truecaller.network.d.j.a;
import com.truecaller.utils.extensions.r;
import java.io.IOException;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Named;
import okhttp3.y;

public final class bo
  implements bn
{
  private final cb a;
  private final ContentResolver b;
  private final y c;
  private final Context d;
  
  @Inject
  public bo(cb paramcb, ContentResolver paramContentResolver, @Named("ImClient") y paramy, Context paramContext)
  {
    a = paramcb;
    b = paramContentResolver;
    c = paramy;
    d = paramContext;
  }
  
  /* Error */
  private final boolean a(Map<String, String> paramMap, String paramString1, String paramString2, Uri paramUri)
  {
    // Byte code:
    //   0: aload 4
    //   2: invokevirtual 60	android/net/Uri:getPathSegments	()Ljava/util/List;
    //   5: astore 7
    //   7: aload 7
    //   9: ldc 62
    //   11: invokestatic 64	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   14: aload 7
    //   16: invokestatic 70	c/a/m:f	(Ljava/util/List;)Ljava/lang/Object;
    //   19: checkcast 72	java/lang/String
    //   22: astore 7
    //   24: new 74	okhttp3/x$a
    //   27: dup
    //   28: invokespecial 75	okhttp3/x$a:<init>	()V
    //   31: getstatic 81	okhttp3/x:e	Lokhttp3/w;
    //   34: invokevirtual 84	okhttp3/x$a:a	(Lokhttp3/w;)Lokhttp3/x$a;
    //   37: astore 8
    //   39: aload_1
    //   40: invokeinterface 90 1 0
    //   45: invokeinterface 96 1 0
    //   50: astore_1
    //   51: aload_1
    //   52: invokeinterface 102 1 0
    //   57: ifeq +43 -> 100
    //   60: aload_1
    //   61: invokeinterface 106 1 0
    //   66: checkcast 108	java/util/Map$Entry
    //   69: astore 9
    //   71: aload 8
    //   73: aload 9
    //   75: invokeinterface 111 1 0
    //   80: checkcast 72	java/lang/String
    //   83: aload 9
    //   85: invokeinterface 114 1 0
    //   90: checkcast 72	java/lang/String
    //   93: invokevirtual 117	okhttp3/x$a:a	(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/x$a;
    //   96: pop
    //   97: goto -46 -> 51
    //   100: aload 8
    //   102: ldc 119
    //   104: aload 7
    //   106: new 8	com/truecaller/messaging/transport/im/bo$a
    //   109: dup
    //   110: aload_0
    //   111: getfield 42	com/truecaller/messaging/transport/im/bo:b	Landroid/content/ContentResolver;
    //   114: aload_3
    //   115: aload 4
    //   117: invokespecial 122	com/truecaller/messaging/transport/im/bo$a:<init>	(Landroid/content/ContentResolver;Ljava/lang/String;Landroid/net/Uri;)V
    //   120: checkcast 124	okhttp3/ac
    //   123: invokevirtual 127	okhttp3/x$a:a	(Ljava/lang/String;Ljava/lang/String;Lokhttp3/ac;)Lokhttp3/x$a;
    //   126: invokevirtual 130	okhttp3/x$a:a	()Lokhttp3/x;
    //   129: astore_1
    //   130: aload_1
    //   131: ldc -124
    //   133: invokestatic 64	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   136: aload_1
    //   137: checkcast 124	okhttp3/ac
    //   140: astore_1
    //   141: new 134	okhttp3/ab$a
    //   144: dup
    //   145: invokespecial 135	okhttp3/ab$a:<init>	()V
    //   148: aload_2
    //   149: invokevirtual 138	okhttp3/ab$a:a	(Ljava/lang/String;)Lokhttp3/ab$a;
    //   152: aload 7
    //   154: invokevirtual 141	okhttp3/ab$a:a	(Ljava/lang/Object;)Lokhttp3/ab$a;
    //   157: aload_1
    //   158: invokevirtual 144	okhttp3/ab$a:a	(Lokhttp3/ac;)Lokhttp3/ab$a;
    //   161: invokevirtual 147	okhttp3/ab$a:a	()Lokhttp3/ab;
    //   164: astore_1
    //   165: aload_0
    //   166: getfield 44	com/truecaller/messaging/transport/im/bo:c	Lokhttp3/y;
    //   169: aload_1
    //   170: invokevirtual 152	okhttp3/y:a	(Lokhttp3/ab;)Lokhttp3/e;
    //   173: astore_1
    //   174: aload_1
    //   175: ldc -102
    //   177: invokestatic 64	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   180: aload_1
    //   181: invokestatic 160	com/google/firebase/perf/network/FirebasePerfOkHttpClient:execute	(Lokhttp3/e;)Lokhttp3/ad;
    //   184: checkcast 162	java/io/Closeable
    //   187: astore_3
    //   188: aconst_null
    //   189: astore_2
    //   190: aload_2
    //   191: astore_1
    //   192: aload_3
    //   193: checkcast 164	okhttp3/ad
    //   196: astore 4
    //   198: iconst_1
    //   199: istore 5
    //   201: aload 4
    //   203: ifnull +21 -> 224
    //   206: aload_2
    //   207: astore_1
    //   208: aload 4
    //   210: invokevirtual 166	okhttp3/ad:c	()Z
    //   213: istore 6
    //   215: iload 6
    //   217: iconst_1
    //   218: if_icmpne +6 -> 224
    //   221: goto +6 -> 227
    //   224: iconst_0
    //   225: istore 5
    //   227: aload_3
    //   228: aconst_null
    //   229: invokestatic 171	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   232: iload 5
    //   234: ireturn
    //   235: astore_2
    //   236: goto +8 -> 244
    //   239: astore_2
    //   240: aload_2
    //   241: astore_1
    //   242: aload_2
    //   243: athrow
    //   244: aload_3
    //   245: aload_1
    //   246: invokestatic 171	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   249: aload_2
    //   250: athrow
    //   251: astore_1
    //   252: aload_1
    //   253: checkcast 54	java/lang/Throwable
    //   256: invokestatic 176	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   259: iconst_0
    //   260: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	261	0	this	bo
    //   0	261	1	paramMap	Map<String, String>
    //   0	261	2	paramString1	String
    //   0	261	3	paramString2	String
    //   0	261	4	paramUri	Uri
    //   199	34	5	bool1	boolean
    //   213	6	6	bool2	boolean
    //   5	148	7	localObject	Object
    //   37	64	8	locala	okhttp3.x.a
    //   69	15	9	localEntry	java.util.Map.Entry
    // Exception table:
    //   from	to	target	type
    //   192	198	235	finally
    //   208	215	235	finally
    //   242	244	235	finally
    //   192	198	239	java/lang/Throwable
    //   208	215	239	java/lang/Throwable
    //   180	188	251	java/io/IOException
    //   227	232	251	java/io/IOException
    //   244	251	251	java/io/IOException
  }
  
  public final ck a(Uri paramUri)
  {
    if (paramUri == null) {
      return new ck(false, null, Integer.valueOf(2131886744), 2);
    }
    Object localObject2 = (k.a)j.a.a(a);
    if (localObject2 == null) {
      return new ck(false, null, Integer.valueOf(2131886744), 2);
    }
    Object localObject1 = TrueApp.x();
    k.a(localObject1, "getAppContext()");
    localObject1 = r.a(paramUri, (Context)localObject1);
    long l;
    if (localObject1 != null)
    {
      l = ((Long)localObject1).longValue();
      localObject1 = r.c(paramUri, d);
      if (localObject1 == null) {
        return new ck(false, null, Integer.valueOf(2131886747), 2);
      }
    }
    try
    {
      localObject2 = ((k.a)localObject2).a((MediaHandles.Request)MediaHandles.Request.a().a(l).a((String)localObject1).build());
      k.a(localObject2, "stub.getMediaHandles(request)");
      Map localMap = ((MediaHandles.c)localObject2).c();
      k.a(localMap, "result.formFieldsMap");
      String str = ((MediaHandles.c)localObject2).a();
      k.a(str, "result.uploadUrl");
      if (a(localMap, str, (String)localObject1, paramUri)) {
        return new ck(true, ((MediaHandles.c)localObject2).b(), null, 4);
      }
      paramUri = new ck(false, null, Integer.valueOf(2131886744), 2);
      return paramUri;
    }
    catch (IOException paramUri)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramUri);
      return new ck(false, null, Integer.valueOf(2131886744), 2);
      return new ck(false, null, Integer.valueOf(2131886745), 2);
    }
    catch (RuntimeException paramUri)
    {
      for (;;) {}
    }
    return new ck(false, null, Integer.valueOf(2131886744), 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bo
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */