package com.truecaller.messaging.transport.im;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import androidx.work.e;
import c.g.b.k;
import c.l;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.w;
import com.truecaller.bp;
import javax.inject.Inject;

public final class SendReactionWorker
  extends Worker
{
  public static final SendReactionWorker.a c = new SendReactionWorker.a((byte)0);
  @Inject
  public q b;
  
  public SendReactionWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public final ListenableWorker.a a()
  {
    Object localObject = getInputData().b("raw_id");
    if (localObject == null)
    {
      localObject = ListenableWorker.a.a();
      k.a(localObject, "Result.success()");
      return (ListenableWorker.a)localObject;
    }
    long l = getInputData().a("message_id", -1L);
    String str1 = getInputData().b("from_peer_id");
    if (str1 == null)
    {
      localObject = ListenableWorker.a.a();
      k.a(localObject, "Result.success()");
      return (ListenableWorker.a)localObject;
    }
    String str2 = getInputData().b("to_peer_id");
    String str3 = getInputData().b("to_group_id");
    String str4 = getInputData().b("emoji");
    q localq = b;
    if (localq == null) {
      k.a("imManager");
    }
    localObject = (SendResult)localq.a((String)localObject, l, str1, str2, str3, str4).d();
    if (localObject != null) {}
    switch (cg.a[localObject.ordinal()])
    {
    default: 
      throw new l();
    case 3: 
      localObject = ListenableWorker.a.a();
      k.a(localObject, "Result.success()");
      return (ListenableWorker.a)localObject;
    case 2: 
      if (getRunAttemptCount() < 3) {
        localObject = ListenableWorker.a.b();
      } else {
        localObject = ListenableWorker.a.a();
      }
      k.a(localObject, "if (runAttemptCount < MA…y() else Result.success()");
      return (ListenableWorker.a)localObject;
    }
    localObject = ListenableWorker.a.a();
    k.a(localObject, "Result.success()");
    return (ListenableWorker.a)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.SendReactionWorker
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */