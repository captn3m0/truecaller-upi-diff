package com.truecaller.messaging.transport.im;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.androidactors.w;
import com.truecaller.api.services.messenger.v1.k.a;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer.a;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer.b;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer.b.a;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer.d;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer.d.a;
import com.truecaller.api.services.messenger.v1.models.input.InputReactionContent;
import com.truecaller.api.services.messenger.v1.models.input.InputReactionContent.a;
import com.truecaller.api.services.messenger.v1.models.input.InputReactionContent.b;
import com.truecaller.api.services.messenger.v1.models.input.InputReactionContent.b.a;
import com.truecaller.api.services.messenger.v1.models.input.InputReportType;
import com.truecaller.api.services.messenger.v1.r.b;
import com.truecaller.api.services.messenger.v1.r.b.a;
import com.truecaller.api.services.messenger.v1.r.d;
import com.truecaller.api.services.messenger.v1.t.b;
import com.truecaller.api.services.messenger.v1.t.b.a;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Reaction;
import com.truecaller.messaging.transport.m;
import com.truecaller.network.d.j.a;
import dagger.a;
import io.grpc.bc;
import java.io.File;
import java.util.concurrent.TimeUnit;
import okhttp3.y;

public final class s
  implements q
{
  private final Context a;
  private final a<m> b;
  private final cb c;
  private final com.truecaller.messaging.data.providers.c d;
  private final y e;
  private final b f;
  private final com.truecaller.messaging.data.c g;
  private final ContentResolver h;
  
  public s(Context paramContext, a<m> parama, cb paramcb, com.truecaller.messaging.data.providers.c paramc, y paramy, b paramb, com.truecaller.messaging.data.c paramc1, ContentResolver paramContentResolver)
  {
    a = paramContext;
    b = parama;
    c = paramcb;
    d = paramc;
    e = paramy;
    f = paramb;
    g = paramc1;
    h = paramContentResolver;
  }
  
  private static BinaryEntity a(d paramd, int paramInt)
  {
    paramd = Entity.a(b, "application/octet-stream", paramInt, d, e);
    k.a(paramd, "Entity.create(item.entit…ntUri, -1, -1, item.size)");
    return paramd;
  }
  
  /* Error */
  private final BinaryEntity a(File paramFile, long paramLong, Uri paramUri)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 6
    //   3: aconst_null
    //   4: astore 9
    //   6: aload_0
    //   7: getfield 63	com/truecaller/messaging/transport/im/s:g	Lcom/truecaller/messaging/data/c;
    //   10: aload_0
    //   11: getfield 65	com/truecaller/messaging/transport/im/s:h	Landroid/content/ContentResolver;
    //   14: invokestatic 98	com/truecaller/content/TruecallerContract$z:a	()Landroid/net/Uri;
    //   17: aconst_null
    //   18: ldc 100
    //   20: lload_2
    //   21: invokestatic 106	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   24: invokevirtual 110	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   27: aconst_null
    //   28: aconst_null
    //   29: invokevirtual 116	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   32: invokeinterface 121 2 0
    //   37: astore 8
    //   39: aload 8
    //   41: ifnull +246 -> 287
    //   44: aload 8
    //   46: checkcast 123	java/io/Closeable
    //   49: astore 11
    //   51: aload 9
    //   53: astore 6
    //   55: aload 11
    //   57: checkcast 125	com/truecaller/messaging/data/a/g
    //   60: astore 7
    //   62: aload 9
    //   64: astore 6
    //   66: aload 7
    //   68: invokeinterface 129 1 0
    //   73: istore 5
    //   75: iload 5
    //   77: iconst_1
    //   78: if_icmpne +154 -> 232
    //   81: aload 9
    //   83: astore 6
    //   85: aload 7
    //   87: invokeinterface 132 1 0
    //   92: astore 10
    //   94: aload 10
    //   96: astore 7
    //   98: aload 9
    //   100: astore 6
    //   102: aload 10
    //   104: instanceof 134
    //   107: ifne +6 -> 113
    //   110: aconst_null
    //   111: astore 7
    //   113: aload 9
    //   115: astore 6
    //   117: aload 7
    //   119: checkcast 134	com/truecaller/messaging/data/types/VideoEntity
    //   122: astore 7
    //   124: aload 7
    //   126: ifnull +101 -> 227
    //   129: aload 9
    //   131: astore 6
    //   133: aload 7
    //   135: getfield 137	com/truecaller/messaging/data/types/VideoEntity:m	Landroid/net/Uri;
    //   138: getstatic 142	android/net/Uri:EMPTY	Landroid/net/Uri;
    //   141: invokestatic 145	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/Object;)Z
    //   144: iconst_1
    //   145: ixor
    //   146: ifeq +38 -> 184
    //   149: aload 9
    //   151: astore 6
    //   153: aload_0
    //   154: getfield 57	com/truecaller/messaging/transport/im/s:d	Lcom/truecaller/messaging/data/providers/c;
    //   157: aload 7
    //   159: getfield 137	com/truecaller/messaging/data/types/VideoEntity:m	Landroid/net/Uri;
    //   162: invokeinterface 150 2 0
    //   167: astore 10
    //   169: aload 10
    //   171: ifnull +13 -> 184
    //   174: aload 9
    //   176: astore 6
    //   178: aload 10
    //   180: invokevirtual 155	java/io/File:delete	()Z
    //   183: pop
    //   184: aload 9
    //   186: astore 6
    //   188: lload_2
    //   189: aload 7
    //   191: getfield 159	com/truecaller/messaging/data/types/VideoEntity:j	Ljava/lang/String;
    //   194: iconst_0
    //   195: aload 4
    //   197: aload 7
    //   199: getfield 162	com/truecaller/messaging/data/types/VideoEntity:a	I
    //   202: aload 7
    //   204: getfield 165	com/truecaller/messaging/data/types/VideoEntity:k	I
    //   207: aload 7
    //   209: getfield 168	com/truecaller/messaging/data/types/VideoEntity:l	I
    //   212: iconst_0
    //   213: aload_1
    //   214: invokestatic 173	com/truecaller/utils/extensions/l:a	(Ljava/io/File;)J
    //   217: getstatic 142	android/net/Uri:EMPTY	Landroid/net/Uri;
    //   220: invokestatic 176	com/truecaller/messaging/data/types/Entity:a	(JLjava/lang/String;ILandroid/net/Uri;IIIZJLandroid/net/Uri;)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   223: astore_1
    //   224: goto +15 -> 239
    //   227: aconst_null
    //   228: astore_1
    //   229: goto +10 -> 239
    //   232: iload 5
    //   234: ifne +14 -> 248
    //   237: aconst_null
    //   238: astore_1
    //   239: aload 11
    //   241: aconst_null
    //   242: invokestatic 181	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   245: goto +44 -> 289
    //   248: aload 9
    //   250: astore 6
    //   252: new 183	c/l
    //   255: dup
    //   256: invokespecial 184	c/l:<init>	()V
    //   259: athrow
    //   260: astore_1
    //   261: goto +9 -> 270
    //   264: astore_1
    //   265: aload_1
    //   266: astore 6
    //   268: aload_1
    //   269: athrow
    //   270: aload 11
    //   272: aload 6
    //   274: invokestatic 181	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   277: aload_1
    //   278: athrow
    //   279: astore 4
    //   281: aload 8
    //   283: astore_1
    //   284: goto +20 -> 304
    //   287: aconst_null
    //   288: astore_1
    //   289: aload 8
    //   291: checkcast 186	android/database/Cursor
    //   294: invokestatic 191	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   297: aload_1
    //   298: areturn
    //   299: astore 4
    //   301: aload 6
    //   303: astore_1
    //   304: aload_1
    //   305: checkcast 186	android/database/Cursor
    //   308: invokestatic 191	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   311: aload 4
    //   313: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	314	0	this	s
    //   0	314	1	paramFile	File
    //   0	314	2	paramLong	long
    //   0	314	4	paramUri	Uri
    //   73	160	5	bool	boolean
    //   1	301	6	localObject1	Object
    //   60	148	7	localObject2	Object
    //   37	253	8	localg	com.truecaller.messaging.data.a.g
    //   4	245	9	localObject3	Object
    //   92	87	10	localObject4	Object
    //   49	222	11	localCloseable	java.io.Closeable
    // Exception table:
    //   from	to	target	type
    //   55	62	260	finally
    //   66	75	260	finally
    //   85	94	260	finally
    //   102	110	260	finally
    //   117	124	260	finally
    //   133	149	260	finally
    //   153	169	260	finally
    //   178	184	260	finally
    //   188	224	260	finally
    //   252	260	260	finally
    //   268	270	260	finally
    //   55	62	264	java/lang/Throwable
    //   66	75	264	java/lang/Throwable
    //   85	94	264	java/lang/Throwable
    //   102	110	264	java/lang/Throwable
    //   117	124	264	java/lang/Throwable
    //   133	149	264	java/lang/Throwable
    //   153	169	264	java/lang/Throwable
    //   178	184	264	java/lang/Throwable
    //   188	224	264	java/lang/Throwable
    //   252	260	264	java/lang/Throwable
    //   44	51	279	finally
    //   239	245	279	finally
    //   270	279	279	finally
    //   6	39	299	finally
  }
  
  private static BinaryEntity a(String paramString, File paramFile, long paramLong, Uri paramUri)
  {
    paramString = Entity.a(paramLong, paramString, 0, paramUri, -1, -1, false, com.truecaller.utils.extensions.l.a(paramFile));
    k.a(paramString, "Entity.create(id, type, …false, file.safeLength())");
    return paramString;
  }
  
  /* Error */
  private final Entity a(d paramd)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 51	com/truecaller/messaging/transport/im/s:a	Landroid/content/Context;
    //   4: invokevirtual 206	android/content/Context:getCacheDir	()Ljava/io/File;
    //   7: astore 5
    //   9: aconst_null
    //   10: astore 8
    //   12: ldc -48
    //   14: aconst_null
    //   15: aload 5
    //   17: invokestatic 212	java/io/File:createTempFile	(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
    //   20: astore 11
    //   22: ldc 76
    //   24: invokestatic 217	okhttp3/w:b	(Ljava/lang/String;)Lokhttp3/w;
    //   27: astore 6
    //   29: new 219	java/io/FileOutputStream
    //   32: dup
    //   33: aload 11
    //   35: invokespecial 222	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   38: checkcast 123	java/io/Closeable
    //   41: astore 9
    //   43: aload 8
    //   45: astore 5
    //   47: aload 9
    //   49: checkcast 219	java/io/FileOutputStream
    //   52: astore 12
    //   54: aload 8
    //   56: astore 5
    //   58: new 224	okhttp3/ab$a
    //   61: dup
    //   62: invokespecial 225	okhttp3/ab$a:<init>	()V
    //   65: aload_1
    //   66: getfield 228	com/truecaller/messaging/transport/im/d:c	Lokhttp3/u;
    //   69: invokevirtual 231	okhttp3/ab$a:a	(Lokhttp3/u;)Lokhttp3/ab$a;
    //   72: invokevirtual 234	okhttp3/ab$a:a	()Lokhttp3/ab;
    //   75: astore 7
    //   77: aload 8
    //   79: astore 5
    //   81: aload_0
    //   82: getfield 59	com/truecaller/messaging/transport/im/s:e	Lokhttp3/y;
    //   85: aload 7
    //   87: invokevirtual 239	okhttp3/y:a	(Lokhttp3/ab;)Lokhttp3/e;
    //   90: invokestatic 245	com/google/firebase/perf/network/FirebasePerfOkHttpClient:execute	(Lokhttp3/e;)Lokhttp3/ad;
    //   93: checkcast 123	java/io/Closeable
    //   96: astore 10
    //   98: aload 10
    //   100: checkcast 247	okhttp3/ad
    //   103: astore 5
    //   105: aload 5
    //   107: invokevirtual 250	okhttp3/ad:d	()Lokhttp3/ae;
    //   110: astore 13
    //   112: aload 5
    //   114: ldc -4
    //   116: invokestatic 90	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   119: aload 5
    //   121: invokevirtual 254	okhttp3/ad:c	()Z
    //   124: ifeq +509 -> 633
    //   127: aload 13
    //   129: ifnonnull +6 -> 135
    //   132: goto +501 -> 633
    //   135: aload 5
    //   137: ldc_w 256
    //   140: invokevirtual 258	okhttp3/ad:a	(Ljava/lang/String;)Ljava/lang/String;
    //   143: astore 7
    //   145: aload 7
    //   147: astore 5
    //   149: aload 7
    //   151: ifnonnull +8 -> 159
    //   154: ldc_w 260
    //   157: astore 5
    //   159: aload 5
    //   161: invokestatic 217	okhttp3/w:b	(Ljava/lang/String;)Lokhttp3/w;
    //   164: astore 5
    //   166: aload 5
    //   168: ifnonnull +594 -> 762
    //   171: goto +3 -> 174
    //   174: aload 13
    //   176: invokevirtual 265	okhttp3/ae:d	()Ljava/io/InputStream;
    //   179: astore 5
    //   181: aload 5
    //   183: ldc_w 267
    //   186: invokestatic 90	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   189: aload 5
    //   191: aload 12
    //   193: checkcast 269	java/io/OutputStream
    //   196: invokestatic 274	com/truecaller/utils/extensions/m:a	(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    //   199: pop2
    //   200: aload 8
    //   202: astore 5
    //   204: aload 10
    //   206: aconst_null
    //   207: invokestatic 181	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   210: aload 9
    //   212: aconst_null
    //   213: invokestatic 181	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   216: aload 6
    //   218: invokestatic 277	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   221: astore 5
    //   223: aload_0
    //   224: getfield 57	com/truecaller/messaging/transport/im/s:d	Lcom/truecaller/messaging/data/providers/c;
    //   227: aload_1
    //   228: getfield 74	com/truecaller/messaging/transport/im/d:b	J
    //   231: aload 5
    //   233: invokeinterface 280 4 0
    //   238: astore 7
    //   240: aload 7
    //   242: ifnonnull +12 -> 254
    //   245: aload_1
    //   246: iconst_2
    //   247: invokestatic 282	com/truecaller/messaging/transport/im/s:a	(Lcom/truecaller/messaging/transport/im/d;I)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   250: checkcast 83	com/truecaller/messaging/data/types/Entity
    //   253: areturn
    //   254: aload 11
    //   256: ldc_w 284
    //   259: invokestatic 90	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   262: aload 11
    //   264: aload 7
    //   266: invokestatic 289	c/f/e:a	(Ljava/io/File;Ljava/io/File;)Ljava/io/File;
    //   269: pop
    //   270: aload 11
    //   272: invokevirtual 155	java/io/File:delete	()Z
    //   275: pop
    //   276: aload_0
    //   277: getfield 57	com/truecaller/messaging/transport/im/s:d	Lcom/truecaller/messaging/data/providers/c;
    //   280: aload 7
    //   282: invokevirtual 293	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   285: invokestatic 298	c/a/m:a	(Ljava/lang/Object;)Ljava/util/List;
    //   288: invokeinterface 301 2 0
    //   293: aload_1
    //   294: getfield 79	com/truecaller/messaging/transport/im/d:d	Landroid/net/Uri;
    //   297: getstatic 142	android/net/Uri:EMPTY	Landroid/net/Uri;
    //   300: invokestatic 145	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/Object;)Z
    //   303: iconst_1
    //   304: ixor
    //   305: ifeq +29 -> 334
    //   308: aload_0
    //   309: getfield 57	com/truecaller/messaging/transport/im/s:d	Lcom/truecaller/messaging/data/providers/c;
    //   312: aload_1
    //   313: getfield 79	com/truecaller/messaging/transport/im/d:d	Landroid/net/Uri;
    //   316: invokeinterface 150 2 0
    //   321: astore 6
    //   323: aload 6
    //   325: ifnull +9 -> 334
    //   328: aload 6
    //   330: invokevirtual 155	java/io/File:delete	()Z
    //   333: pop
    //   334: aload_0
    //   335: getfield 57	com/truecaller/messaging/transport/im/s:d	Lcom/truecaller/messaging/data/providers/c;
    //   338: aload 7
    //   340: aload 5
    //   342: iconst_1
    //   343: invokeinterface 304 4 0
    //   348: astore 6
    //   350: aload 6
    //   352: ifnonnull +12 -> 364
    //   355: aload_1
    //   356: iconst_2
    //   357: invokestatic 282	com/truecaller/messaging/transport/im/s:a	(Lcom/truecaller/messaging/transport/im/d;I)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   360: checkcast 83	com/truecaller/messaging/data/types/Entity
    //   363: areturn
    //   364: aload 5
    //   366: checkcast 306	java/lang/CharSequence
    //   369: invokeinterface 310 1 0
    //   374: ifne +395 -> 769
    //   377: iconst_1
    //   378: istore_2
    //   379: goto +3 -> 382
    //   382: iload_2
    //   383: ifeq +20 -> 403
    //   386: ldc 76
    //   388: aload 7
    //   390: aload_1
    //   391: getfield 74	com/truecaller/messaging/transport/im/d:b	J
    //   394: aload 6
    //   396: invokestatic 312	com/truecaller/messaging/transport/im/s:a	(Ljava/lang/String;Ljava/io/File;JLandroid/net/Uri;)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   399: checkcast 83	com/truecaller/messaging/data/types/Entity
    //   402: areturn
    //   403: aload 5
    //   405: invokestatic 315	com/truecaller/messaging/data/types/Entity:c	(Ljava/lang/String;)Z
    //   408: ifeq +78 -> 486
    //   411: aload_1
    //   412: getfield 74	com/truecaller/messaging/transport/im/d:b	J
    //   415: lstore_3
    //   416: new 317	android/graphics/BitmapFactory$Options
    //   419: dup
    //   420: invokespecial 318	android/graphics/BitmapFactory$Options:<init>	()V
    //   423: astore 5
    //   425: aload 5
    //   427: iconst_1
    //   428: putfield 322	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
    //   431: aload 7
    //   433: invokevirtual 293	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   436: aload 5
    //   438: invokestatic 328	android/graphics/BitmapFactory:decodeFile	(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   441: pop
    //   442: lload_3
    //   443: aload 5
    //   445: getfield 331	android/graphics/BitmapFactory$Options:outMimeType	Ljava/lang/String;
    //   448: iconst_0
    //   449: aload 6
    //   451: aload 5
    //   453: getfield 334	android/graphics/BitmapFactory$Options:outWidth	I
    //   456: aload 5
    //   458: getfield 337	android/graphics/BitmapFactory$Options:outHeight	I
    //   461: iconst_0
    //   462: aload 7
    //   464: invokestatic 173	com/truecaller/utils/extensions/l:a	(Ljava/io/File;)J
    //   467: invokestatic 195	com/truecaller/messaging/data/types/Entity:a	(JLjava/lang/String;ILandroid/net/Uri;IIZJ)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   470: astore 5
    //   472: aload 5
    //   474: ldc_w 339
    //   477: invokestatic 90	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   480: aload 5
    //   482: checkcast 83	com/truecaller/messaging/data/types/Entity
    //   485: areturn
    //   486: aload 5
    //   488: invokestatic 341	com/truecaller/messaging/data/types/Entity:d	(Ljava/lang/String;)Z
    //   491: ifeq +45 -> 536
    //   494: aload_0
    //   495: aload 7
    //   497: aload_1
    //   498: getfield 74	com/truecaller/messaging/transport/im/d:b	J
    //   501: aload 6
    //   503: invokespecial 343	com/truecaller/messaging/transport/im/s:a	(Ljava/io/File;JLandroid/net/Uri;)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   506: astore 7
    //   508: aload 7
    //   510: ifnull +9 -> 519
    //   513: aload 7
    //   515: checkcast 83	com/truecaller/messaging/data/types/Entity
    //   518: areturn
    //   519: aload 5
    //   521: aload 11
    //   523: aload_1
    //   524: getfield 74	com/truecaller/messaging/transport/im/d:b	J
    //   527: aload 6
    //   529: invokestatic 312	com/truecaller/messaging/transport/im/s:a	(Ljava/lang/String;Ljava/io/File;JLandroid/net/Uri;)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   532: checkcast 83	com/truecaller/messaging/data/types/Entity
    //   535: areturn
    //   536: aload 5
    //   538: invokestatic 345	com/truecaller/messaging/data/types/Entity:e	(Ljava/lang/String;)Z
    //   541: ifeq +45 -> 586
    //   544: aload_0
    //   545: aload 7
    //   547: aload_1
    //   548: getfield 74	com/truecaller/messaging/transport/im/d:b	J
    //   551: aload 6
    //   553: invokespecial 347	com/truecaller/messaging/transport/im/s:b	(Ljava/io/File;JLandroid/net/Uri;)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   556: astore 7
    //   558: aload 7
    //   560: ifnull +9 -> 569
    //   563: aload 7
    //   565: checkcast 83	com/truecaller/messaging/data/types/Entity
    //   568: areturn
    //   569: aload 5
    //   571: aload 11
    //   573: aload_1
    //   574: getfield 74	com/truecaller/messaging/transport/im/d:b	J
    //   577: aload 6
    //   579: invokestatic 312	com/truecaller/messaging/transport/im/s:a	(Ljava/lang/String;Ljava/io/File;JLandroid/net/Uri;)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   582: checkcast 83	com/truecaller/messaging/data/types/Entity
    //   585: areturn
    //   586: aload 5
    //   588: invokestatic 349	com/truecaller/messaging/data/types/Entity:f	(Ljava/lang/String;)Z
    //   591: ifeq +21 -> 612
    //   594: ldc_w 351
    //   597: aload 7
    //   599: aload_1
    //   600: getfield 74	com/truecaller/messaging/transport/im/d:b	J
    //   603: aload 6
    //   605: invokestatic 312	com/truecaller/messaging/transport/im/s:a	(Ljava/lang/String;Ljava/io/File;JLandroid/net/Uri;)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   608: checkcast 83	com/truecaller/messaging/data/types/Entity
    //   611: areturn
    //   612: aload 5
    //   614: aload 7
    //   616: aload_1
    //   617: getfield 74	com/truecaller/messaging/transport/im/d:b	J
    //   620: aload 6
    //   622: invokestatic 312	com/truecaller/messaging/transport/im/s:a	(Ljava/lang/String;Ljava/io/File;JLandroid/net/Uri;)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   625: checkcast 83	com/truecaller/messaging/data/types/Entity
    //   628: astore 5
    //   630: aload 5
    //   632: areturn
    //   633: aload 5
    //   635: invokevirtual 353	okhttp3/ad:b	()I
    //   638: sipush 404
    //   641: if_icmpeq +16 -> 657
    //   644: aload_1
    //   645: iconst_2
    //   646: invokestatic 282	com/truecaller/messaging/transport/im/s:a	(Lcom/truecaller/messaging/transport/im/d;I)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   649: checkcast 83	com/truecaller/messaging/data/types/Entity
    //   652: astore 6
    //   654: goto +13 -> 667
    //   657: aload_1
    //   658: iconst_3
    //   659: invokestatic 282	com/truecaller/messaging/transport/im/s:a	(Lcom/truecaller/messaging/transport/im/d;I)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   662: checkcast 83	com/truecaller/messaging/data/types/Entity
    //   665: astore 6
    //   667: aload 8
    //   669: astore 5
    //   671: aload 10
    //   673: aconst_null
    //   674: invokestatic 181	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   677: aload 9
    //   679: aconst_null
    //   680: invokestatic 181	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   683: aload 6
    //   685: areturn
    //   686: astore 6
    //   688: aconst_null
    //   689: astore 7
    //   691: goto +10 -> 701
    //   694: astore 7
    //   696: aload 7
    //   698: athrow
    //   699: astore 6
    //   701: aload 8
    //   703: astore 5
    //   705: aload 10
    //   707: aload 7
    //   709: invokestatic 181	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   712: aload 8
    //   714: astore 5
    //   716: aload 6
    //   718: athrow
    //   719: astore 6
    //   721: goto +12 -> 733
    //   724: astore 6
    //   726: aload 6
    //   728: astore 5
    //   730: aload 6
    //   732: athrow
    //   733: aload 9
    //   735: aload 5
    //   737: invokestatic 181	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   740: aload 6
    //   742: athrow
    //   743: astore 5
    //   745: aload 5
    //   747: checkcast 93	java/lang/Throwable
    //   750: invokestatic 359	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   753: aload_1
    //   754: iconst_2
    //   755: invokestatic 282	com/truecaller/messaging/transport/im/s:a	(Lcom/truecaller/messaging/transport/im/d;I)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   758: checkcast 83	com/truecaller/messaging/data/types/Entity
    //   761: areturn
    //   762: aload 5
    //   764: astore 6
    //   766: goto -592 -> 174
    //   769: iconst_0
    //   770: istore_2
    //   771: goto -389 -> 382
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	774	0	this	s
    //   0	774	1	paramd	d
    //   378	393	2	i	int
    //   415	28	3	l	long
    //   7	729	5	localObject1	Object
    //   743	20	5	localIOException1	java.io.IOException
    //   27	657	6	localObject2	Object
    //   686	1	6	localObject3	Object
    //   699	18	6	localObject4	Object
    //   719	1	6	localObject5	Object
    //   724	17	6	localThrowable1	Throwable
    //   764	1	6	localIOException2	java.io.IOException
    //   75	615	7	localObject6	Object
    //   694	14	7	localThrowable2	Throwable
    //   10	703	8	localObject7	Object
    //   41	693	9	localCloseable1	java.io.Closeable
    //   96	610	10	localCloseable2	java.io.Closeable
    //   20	552	11	localFile	File
    //   52	140	12	localFileOutputStream	java.io.FileOutputStream
    //   110	65	13	localae	okhttp3.ae
    // Exception table:
    //   from	to	target	type
    //   98	127	686	finally
    //   135	145	686	finally
    //   159	166	686	finally
    //   174	200	686	finally
    //   633	654	686	finally
    //   657	667	686	finally
    //   98	127	694	java/lang/Throwable
    //   135	145	694	java/lang/Throwable
    //   159	166	694	java/lang/Throwable
    //   174	200	694	java/lang/Throwable
    //   633	654	694	java/lang/Throwable
    //   657	667	694	java/lang/Throwable
    //   696	699	699	finally
    //   47	54	719	finally
    //   58	77	719	finally
    //   81	98	719	finally
    //   204	210	719	finally
    //   671	677	719	finally
    //   705	712	719	finally
    //   716	719	719	finally
    //   730	733	719	finally
    //   47	54	724	java/lang/Throwable
    //   58	77	724	java/lang/Throwable
    //   81	98	724	java/lang/Throwable
    //   204	210	724	java/lang/Throwable
    //   671	677	724	java/lang/Throwable
    //   705	712	724	java/lang/Throwable
    //   716	719	724	java/lang/Throwable
    //   0	9	743	java/io/IOException
    //   12	43	743	java/io/IOException
    //   210	240	743	java/io/IOException
    //   245	254	743	java/io/IOException
    //   254	323	743	java/io/IOException
    //   328	334	743	java/io/IOException
    //   334	350	743	java/io/IOException
    //   355	364	743	java/io/IOException
    //   364	377	743	java/io/IOException
    //   386	403	743	java/io/IOException
    //   403	486	743	java/io/IOException
    //   486	508	743	java/io/IOException
    //   513	519	743	java/io/IOException
    //   519	536	743	java/io/IOException
    //   536	558	743	java/io/IOException
    //   563	569	743	java/io/IOException
    //   569	586	743	java/io/IOException
    //   586	612	743	java/io/IOException
    //   612	630	743	java/io/IOException
    //   677	683	743	java/io/IOException
    //   733	743	743	java/io/IOException
  }
  
  private final SendResult a(InputReportType paramInputReportType, String paramString, InputPeer paramInputPeer)
  {
    k.a locala = (k.a)j.a.a(c);
    if (locala == null) {
      return SendResult.FAILURE_PERMANENT;
    }
    try
    {
      locala.a((t.b)t.b.a().a(paramString).a(paramInputPeer).a(paramInputReportType).build());
      paramInputReportType = SendResult.SUCCESS;
      return paramInputReportType;
    }
    catch (bc paramInputReportType)
    {
      paramInputReportType = paramInputReportType.a();
      k.a(paramInputReportType, "e.status");
      if (!com.truecaller.messaging.i.l.a(paramInputReportType)) {
        break label82;
      }
      return SendResult.FAILURE_TRANSIENT;
      return SendResult.FAILURE_PERMANENT;
    }
    catch (RuntimeException paramInputReportType)
    {
      for (;;) {}
    }
    return SendResult.FAILURE_PERMANENT;
  }
  
  private final void a(String paramString, Reaction paramReaction)
  {
    paramString = new Intent("update_reaction").putExtra("reaction", (Parcelable)paramReaction).putExtra("rawId", paramString);
    ((m)b.get()).a(2, paramString, 0);
  }
  
  /* Error */
  private final BinaryEntity b(File paramFile, long paramLong, Uri paramUri)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 6
    //   3: aconst_null
    //   4: astore 9
    //   6: aload_0
    //   7: getfield 63	com/truecaller/messaging/transport/im/s:g	Lcom/truecaller/messaging/data/c;
    //   10: aload_0
    //   11: getfield 65	com/truecaller/messaging/transport/im/s:h	Landroid/content/ContentResolver;
    //   14: invokestatic 98	com/truecaller/content/TruecallerContract$z:a	()Landroid/net/Uri;
    //   17: aconst_null
    //   18: ldc 100
    //   20: lload_2
    //   21: invokestatic 106	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   24: invokevirtual 110	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   27: aconst_null
    //   28: aconst_null
    //   29: invokevirtual 116	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   32: invokeinterface 121 2 0
    //   37: astore 8
    //   39: aload 8
    //   41: ifnull +183 -> 224
    //   44: aload 8
    //   46: checkcast 123	java/io/Closeable
    //   49: astore 11
    //   51: aload 9
    //   53: astore 6
    //   55: aload 11
    //   57: checkcast 125	com/truecaller/messaging/data/a/g
    //   60: astore 7
    //   62: aload 9
    //   64: astore 6
    //   66: aload 7
    //   68: invokeinterface 129 1 0
    //   73: istore 5
    //   75: iload 5
    //   77: iconst_1
    //   78: if_icmpne +91 -> 169
    //   81: aload 9
    //   83: astore 6
    //   85: aload 7
    //   87: invokeinterface 132 1 0
    //   92: astore 10
    //   94: aload 10
    //   96: astore 7
    //   98: aload 9
    //   100: astore 6
    //   102: aload 10
    //   104: instanceof 450
    //   107: ifne +6 -> 113
    //   110: aconst_null
    //   111: astore 7
    //   113: aload 9
    //   115: astore 6
    //   117: aload 7
    //   119: checkcast 450	com/truecaller/messaging/data/types/AudioEntity
    //   122: astore 7
    //   124: aload 7
    //   126: ifnull +38 -> 164
    //   129: aload 9
    //   131: astore 6
    //   133: lload_2
    //   134: aload 7
    //   136: getfield 451	com/truecaller/messaging/data/types/AudioEntity:j	Ljava/lang/String;
    //   139: iconst_0
    //   140: aload 4
    //   142: iconst_m1
    //   143: iconst_m1
    //   144: aload 7
    //   146: getfield 452	com/truecaller/messaging/data/types/AudioEntity:a	I
    //   149: iconst_0
    //   150: aload_1
    //   151: invokestatic 173	com/truecaller/utils/extensions/l:a	(Ljava/io/File;)J
    //   154: getstatic 142	android/net/Uri:EMPTY	Landroid/net/Uri;
    //   157: invokestatic 176	com/truecaller/messaging/data/types/Entity:a	(JLjava/lang/String;ILandroid/net/Uri;IIIZJLandroid/net/Uri;)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   160: astore_1
    //   161: goto +15 -> 176
    //   164: aconst_null
    //   165: astore_1
    //   166: goto +10 -> 176
    //   169: iload 5
    //   171: ifne +14 -> 185
    //   174: aconst_null
    //   175: astore_1
    //   176: aload 11
    //   178: aconst_null
    //   179: invokestatic 181	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   182: goto +44 -> 226
    //   185: aload 9
    //   187: astore 6
    //   189: new 183	c/l
    //   192: dup
    //   193: invokespecial 184	c/l:<init>	()V
    //   196: athrow
    //   197: astore_1
    //   198: goto +9 -> 207
    //   201: astore_1
    //   202: aload_1
    //   203: astore 6
    //   205: aload_1
    //   206: athrow
    //   207: aload 11
    //   209: aload 6
    //   211: invokestatic 181	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   214: aload_1
    //   215: athrow
    //   216: astore 4
    //   218: aload 8
    //   220: astore_1
    //   221: goto +20 -> 241
    //   224: aconst_null
    //   225: astore_1
    //   226: aload 8
    //   228: checkcast 186	android/database/Cursor
    //   231: invokestatic 191	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   234: aload_1
    //   235: areturn
    //   236: astore 4
    //   238: aload 6
    //   240: astore_1
    //   241: aload_1
    //   242: checkcast 186	android/database/Cursor
    //   245: invokestatic 191	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   248: aload 4
    //   250: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	251	0	this	s
    //   0	251	1	paramFile	File
    //   0	251	2	paramLong	long
    //   0	251	4	paramUri	Uri
    //   73	97	5	bool	boolean
    //   1	238	6	localObject1	Object
    //   60	85	7	localObject2	Object
    //   37	190	8	localg	com.truecaller.messaging.data.a.g
    //   4	182	9	localObject3	Object
    //   92	11	10	localEntity	Entity
    //   49	159	11	localCloseable	java.io.Closeable
    // Exception table:
    //   from	to	target	type
    //   55	62	197	finally
    //   66	75	197	finally
    //   85	94	197	finally
    //   102	110	197	finally
    //   117	124	197	finally
    //   133	161	197	finally
    //   189	197	197	finally
    //   205	207	197	finally
    //   55	62	201	java/lang/Throwable
    //   66	75	201	java/lang/Throwable
    //   85	94	201	java/lang/Throwable
    //   102	110	201	java/lang/Throwable
    //   117	124	201	java/lang/Throwable
    //   133	161	201	java/lang/Throwable
    //   189	197	201	java/lang/Throwable
    //   44	51	216	finally
    //   176	182	216	finally
    //   207	216	216	finally
    //   6	39	236	finally
  }
  
  public final w<SendResult> a(InputReportType paramInputReportType, String paramString1, String paramString2, String paramString3)
  {
    k.b(paramInputReportType, "type");
    k.b(paramString1, "rawId");
    k.b(paramString2, "imPeerId");
    InputPeer.a locala = InputPeer.a();
    CharSequence localCharSequence = (CharSequence)paramString3;
    int i;
    if ((localCharSequence != null) && (localCharSequence.length() != 0)) {
      i = 0;
    } else {
      i = 1;
    }
    if (i == 1) {
      locala.a(InputPeer.d.a().a(paramString2));
    } else {
      locala.a(InputPeer.b.a().a(paramString3));
    }
    paramString2 = locala.build();
    k.a(paramString2, "InputPeer.newBuilder().a…      }\n        }.build()");
    paramString2 = a(paramInputReportType, paramString1, (InputPeer)paramString2);
    if (paramString2 == SendResult.SUCCESS) {
      i = 4;
    } else {
      i = 3;
    }
    paramString1 = new ImTransportInfo.a().a(paramString1);
    switch (t.a[paramInputReportType.ordinal()])
    {
    default: 
      throw ((Throwable)new IllegalArgumentException("Unknown report type ".concat(String.valueOf(paramInputReportType))));
    case 2: 
      f = i;
      paramInputReportType = new Intent("update_read_sync_status");
      break;
    case 1: 
      e = i;
      paramInputReportType = new Intent("update_read_sync_status");
    }
    paramInputReportType.putExtra("transport_info", (Parcelable)paramString1.a());
    ((m)b.get()).a(2, paramInputReportType, 0);
    paramInputReportType = w.b(paramString2);
    k.a(paramInputReportType, "Promise.wrap(result)");
    return paramInputReportType;
  }
  
  public final w<SendResult> a(String paramString1, long paramLong, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    k.b(paramString1, "rawId");
    k.b(paramString2, "fromPeerId");
    k.a locala = (k.a)j.a.a(c);
    if (locala == null)
    {
      paramString1 = w.b(SendResult.FAILURE_PERMANENT);
      k.a(paramString1, "Promise.wrap(FAILURE_PERMANENT)");
      return paramString1;
    }
    try
    {
      locala1 = InputReactionContent.a().a(paramString1);
      CharSequence localCharSequence = (CharSequence)paramString5;
      if (localCharSequence == null) {
        break label359;
      }
      if (localCharSequence.length() != 0) {
        break label353;
      }
    }
    catch (bc paramString3)
    {
      InputReactionContent.a locala1;
      a(paramString1, new Reaction(0L, paramLong, paramString2, paramString5, System.currentTimeMillis(), 2, 1));
      paramString1 = paramString3.a();
      k.a(paramString1, "e.status");
      if (!com.truecaller.messaging.i.l.a(paramString1)) {
        break label328;
      }
      paramString1 = w.b(SendResult.FAILURE_TRANSIENT);
      paramString2 = "Promise.wrap(FAILURE_TRANSIENT)";
      break label340;
      paramString1 = w.b(SendResult.FAILURE_PERMANENT);
      paramString2 = "Promise.wrap(FAILURE_PERMANENT)";
      k.a(paramString1, paramString2);
      return paramString1;
    }
    catch (RuntimeException paramString3)
    {
      for (;;)
      {
        continue;
        int i = 0;
        continue;
        i = 1;
      }
    }
    if (i == 0) {
      locala1.a(InputReactionContent.b.a().a(paramString5));
    }
    if (paramString4 != null) {
      paramString3 = InputPeer.a().a(InputPeer.b.a().a(paramString4));
    } else {
      paramString3 = InputPeer.a().a(InputPeer.d.a().a(paramString3));
    }
    paramString3 = locala.a((r.b)r.b.a().a(paramString3).a(locala1).build());
    paramString4 = TimeUnit.SECONDS;
    k.a(paramString3, "response");
    a(paramString1, new Reaction(0L, paramLong, paramString2, paramString5, paramString4.toMillis(paramString3.a()), 0, 1));
    paramString3 = w.b(SendResult.SUCCESS);
    k.a(paramString3, "Promise.wrap(SUCCESS)");
    return paramString3;
    a(paramString1, new Reaction(0L, paramLong, paramString2, paramString5, System.currentTimeMillis(), 2, 1));
    paramString1 = w.b(SendResult.FAILURE_PERMANENT);
    k.a(paramString1, "Promise.wrap(FAILURE_PERMANENT)");
    return paramString1;
  }
  
  /* Error */
  public final void a()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 65	com/truecaller/messaging/transport/im/s:h	Landroid/content/ContentResolver;
    //   4: invokestatic 615	com/truecaller/content/TruecallerContract$q:a	()Landroid/net/Uri;
    //   7: iconst_5
    //   8: anewarray 102	java/lang/String
    //   11: dup
    //   12: iconst_0
    //   13: ldc_w 617
    //   16: aastore
    //   17: dup
    //   18: iconst_1
    //   19: ldc_w 619
    //   22: aastore
    //   23: dup
    //   24: iconst_2
    //   25: ldc_w 621
    //   28: aastore
    //   29: dup
    //   30: iconst_3
    //   31: ldc_w 623
    //   34: aastore
    //   35: dup
    //   36: iconst_4
    //   37: ldc_w 625
    //   40: aastore
    //   41: ldc_w 627
    //   44: aconst_null
    //   45: ldc_w 629
    //   48: invokevirtual 116	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   51: astore 6
    //   53: aload 6
    //   55: ifnull +579 -> 634
    //   58: aload 6
    //   60: checkcast 123	java/io/Closeable
    //   63: astore 8
    //   65: aconst_null
    //   66: astore 7
    //   68: aload 7
    //   70: astore 6
    //   72: aload 8
    //   74: checkcast 186	android/database/Cursor
    //   77: astore 9
    //   79: aload 7
    //   81: astore 6
    //   83: new 631	com/truecaller/messaging/transport/im/s$a
    //   86: dup
    //   87: aload 9
    //   89: invokespecial 633	com/truecaller/messaging/transport/im/s$a:<init>	(Landroid/database/Cursor;)V
    //   92: checkcast 635	c/g/a/a
    //   95: astore 10
    //   97: aload 7
    //   99: astore 6
    //   101: aload 10
    //   103: ldc_w 637
    //   106: invokestatic 32	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   109: aload 7
    //   111: astore 6
    //   113: new 639	c/m/h
    //   116: dup
    //   117: aload 10
    //   119: new 641	c/m/o$d
    //   122: dup
    //   123: aload 10
    //   125: invokespecial 644	c/m/o$d:<init>	(Lc/g/a/a;)V
    //   128: checkcast 646	c/g/a/b
    //   131: invokespecial 649	c/m/h:<init>	(Lc/g/a/a;Lc/g/a/b;)V
    //   134: checkcast 651	c/m/i
    //   137: invokestatic 656	c/m/l:b	(Lc/m/i;)Lc/m/i;
    //   140: new 658	com/truecaller/messaging/transport/im/s$b
    //   143: dup
    //   144: aload 9
    //   146: invokespecial 659	com/truecaller/messaging/transport/im/s$b:<init>	(Landroid/database/Cursor;)V
    //   149: checkcast 646	c/g/a/b
    //   152: invokestatic 662	c/m/l:d	(Lc/m/i;Lc/g/a/b;)Lc/m/i;
    //   155: invokestatic 665	c/m/l:d	(Lc/m/i;)Ljava/util/List;
    //   158: astore 7
    //   160: aload 8
    //   162: aconst_null
    //   163: invokestatic 181	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   166: aload 7
    //   168: ifnonnull +4 -> 172
    //   171: return
    //   172: aload 7
    //   174: checkcast 667	java/lang/Iterable
    //   177: invokeinterface 671 1 0
    //   182: astore 8
    //   184: aload 8
    //   186: invokeinterface 676 1 0
    //   191: ifeq +418 -> 609
    //   194: aload 8
    //   196: invokeinterface 679 1 0
    //   201: checkcast 71	com/truecaller/messaging/transport/im/d
    //   204: astore 9
    //   206: invokestatic 609	java/lang/System:currentTimeMillis	()J
    //   209: lstore_2
    //   210: aload_0
    //   211: aload 9
    //   213: invokespecial 681	com/truecaller/messaging/transport/im/s:a	(Lcom/truecaller/messaging/transport/im/d;)Lcom/truecaller/messaging/data/types/Entity;
    //   216: astore 7
    //   218: invokestatic 609	java/lang/System:currentTimeMillis	()J
    //   221: lload_2
    //   222: lsub
    //   223: lstore 4
    //   225: aload 7
    //   227: instanceof 683
    //   230: ifne +9 -> 239
    //   233: aconst_null
    //   234: astore 6
    //   236: goto +7 -> 243
    //   239: aload 7
    //   241: astore 6
    //   243: aload 6
    //   245: checkcast 683	com/truecaller/messaging/data/types/BinaryEntity
    //   248: astore 6
    //   250: aload 6
    //   252: ifnull +12 -> 264
    //   255: aload 6
    //   257: getfield 685	com/truecaller/messaging/data/types/BinaryEntity:d	J
    //   260: lstore_2
    //   261: goto +7 -> 268
    //   264: ldc2_w 686
    //   267: lstore_2
    //   268: lload_2
    //   269: l2f
    //   270: ldc_w 688
    //   273: fdiv
    //   274: getstatic 691	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   277: lload 4
    //   279: invokevirtual 694	java/util/concurrent/TimeUnit:toSeconds	(J)J
    //   282: l2f
    //   283: fdiv
    //   284: fstore_1
    //   285: new 696	com/truecaller/analytics/e$a
    //   288: dup
    //   289: ldc_w 698
    //   292: invokespecial 699	com/truecaller/analytics/e$a:<init>	(Ljava/lang/String;)V
    //   295: ldc_w 701
    //   298: aload 7
    //   300: getfield 702	com/truecaller/messaging/data/types/Entity:j	Ljava/lang/String;
    //   303: invokevirtual 705	com/truecaller/analytics/e$a:a	(Ljava/lang/String;Ljava/lang/String;)Lcom/truecaller/analytics/e$a;
    //   306: astore 10
    //   308: aload 7
    //   310: getfield 708	com/truecaller/messaging/data/types/Entity:i	I
    //   313: tableswitch	default:+35->348, 0:+75->388, 1:+67->380, 2:+59->372, 3:+51->364, 4:+43->356
    //   348: ldc_w 710
    //   351: astore 6
    //   353: goto +40 -> 393
    //   356: ldc_w 712
    //   359: astore 6
    //   361: goto +32 -> 393
    //   364: ldc_w 714
    //   367: astore 6
    //   369: goto +24 -> 393
    //   372: ldc_w 716
    //   375: astore 6
    //   377: goto +16 -> 393
    //   380: ldc_w 718
    //   383: astore 6
    //   385: goto +8 -> 393
    //   388: ldc_w 720
    //   391: astore 6
    //   393: aload 10
    //   395: ldc_w 722
    //   398: aload 6
    //   400: invokevirtual 705	com/truecaller/analytics/e$a:a	(Ljava/lang/String;Ljava/lang/String;)Lcom/truecaller/analytics/e$a;
    //   403: ldc_w 724
    //   406: lload_2
    //   407: invokevirtual 727	com/truecaller/analytics/e$a:a	(Ljava/lang/String;J)Lcom/truecaller/analytics/e$a;
    //   410: ldc_w 729
    //   413: lload_2
    //   414: invokestatic 733	com/truecaller/messaging/transport/im/u:b	(J)Ljava/lang/String;
    //   417: invokevirtual 705	com/truecaller/analytics/e$a:a	(Ljava/lang/String;Ljava/lang/String;)Lcom/truecaller/analytics/e$a;
    //   420: fload_1
    //   421: f2d
    //   422: invokestatic 738	java/lang/Double:valueOf	(D)Ljava/lang/Double;
    //   425: invokevirtual 741	com/truecaller/analytics/e$a:a	(Ljava/lang/Double;)Lcom/truecaller/analytics/e$a;
    //   428: ldc_w 743
    //   431: lload 4
    //   433: invokestatic 745	com/truecaller/messaging/transport/im/u:a	(J)Ljava/lang/String;
    //   436: invokevirtual 705	com/truecaller/analytics/e$a:a	(Ljava/lang/String;Ljava/lang/String;)Lcom/truecaller/analytics/e$a;
    //   439: invokevirtual 748	com/truecaller/analytics/e$a:a	()Lcom/truecaller/analytics/e;
    //   442: astore 6
    //   444: aload_0
    //   445: getfield 61	com/truecaller/messaging/transport/im/s:f	Lcom/truecaller/analytics/b;
    //   448: astore 10
    //   450: aload 6
    //   452: ldc_w 750
    //   455: invokestatic 90	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   458: aload 10
    //   460: aload 6
    //   462: invokeinterface 755 2 0
    //   467: aload 7
    //   469: getfield 708	com/truecaller/messaging/data/types/Entity:i	I
    //   472: ifeq +61 -> 533
    //   475: new 419	android/content/Intent
    //   478: dup
    //   479: ldc_w 757
    //   482: invokespecial 424	android/content/Intent:<init>	(Ljava/lang/String;)V
    //   485: ldc_w 619
    //   488: aload 7
    //   490: getfield 759	com/truecaller/messaging/data/types/Entity:h	J
    //   493: invokevirtual 762	android/content/Intent:putExtra	(Ljava/lang/String;J)Landroid/content/Intent;
    //   496: ldc_w 764
    //   499: aload 7
    //   501: getfield 708	com/truecaller/messaging/data/types/Entity:i	I
    //   504: invokevirtual 767	android/content/Intent:putExtra	(Ljava/lang/String;I)Landroid/content/Intent;
    //   507: astore 6
    //   509: aload_0
    //   510: getfield 53	com/truecaller/messaging/transport/im/s:b	Ldagger/a;
    //   513: invokeinterface 443 1 0
    //   518: checkcast 445	com/truecaller/messaging/transport/m
    //   521: iconst_2
    //   522: aload 6
    //   524: iconst_0
    //   525: invokeinterface 448 4 0
    //   530: goto -346 -> 184
    //   533: new 419	android/content/Intent
    //   536: dup
    //   537: ldc_w 769
    //   540: invokespecial 424	android/content/Intent:<init>	(Ljava/lang/String;)V
    //   543: ldc_w 771
    //   546: aload 7
    //   548: checkcast 428	android/os/Parcelable
    //   551: invokevirtual 432	android/content/Intent:putExtra	(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
    //   554: astore 6
    //   556: aload_0
    //   557: getfield 53	com/truecaller/messaging/transport/im/s:b	Ldagger/a;
    //   560: invokeinterface 443 1 0
    //   565: checkcast 445	com/truecaller/messaging/transport/m
    //   568: iconst_2
    //   569: aload 6
    //   571: iconst_0
    //   572: invokeinterface 448 4 0
    //   577: aload_0
    //   578: getfield 65	com/truecaller/messaging/transport/im/s:h	Landroid/content/ContentResolver;
    //   581: invokestatic 774	com/truecaller/content/TruecallerContract$p:a	()Landroid/net/Uri;
    //   584: ldc_w 776
    //   587: iconst_1
    //   588: anewarray 102	java/lang/String
    //   591: dup
    //   592: iconst_0
    //   593: aload 9
    //   595: getfield 778	com/truecaller/messaging/transport/im/d:a	J
    //   598: invokestatic 106	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   601: aastore
    //   602: invokevirtual 781	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   605: pop
    //   606: goto -422 -> 184
    //   609: return
    //   610: astore 7
    //   612: goto +12 -> 624
    //   615: astore 7
    //   617: aload 7
    //   619: astore 6
    //   621: aload 7
    //   623: athrow
    //   624: aload 8
    //   626: aload 6
    //   628: invokestatic 181	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   631: aload 7
    //   633: athrow
    //   634: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	635	0	this	s
    //   284	137	1	f1	float
    //   209	205	2	l1	long
    //   223	209	4	l2	long
    //   51	576	6	localObject1	Object
    //   66	481	7	localObject2	Object
    //   610	1	7	localObject3	Object
    //   615	17	7	localThrowable	Throwable
    //   63	562	8	localObject4	Object
    //   77	517	9	localObject5	Object
    //   95	364	10	localObject6	Object
    // Exception table:
    //   from	to	target	type
    //   72	79	610	finally
    //   83	97	610	finally
    //   101	109	610	finally
    //   113	160	610	finally
    //   621	624	610	finally
    //   72	79	615	java/lang/Throwable
    //   83	97	615	java/lang/Throwable
    //   101	109	615	java/lang/Throwable
    //   113	160	615	java/lang/Throwable
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.s
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */