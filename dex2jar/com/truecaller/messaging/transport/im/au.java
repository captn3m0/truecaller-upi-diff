package com.truecaller.messaging.transport.im;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import c.a.m;
import c.g.b.k;
import com.truecaller.androidactors.w;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.u;
import com.truecaller.messaging.data.c;
import com.truecaller.messaging.data.types.Reaction;
import com.truecaller.messaging.h;
import com.truecaller.messaging.notifications.g;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.inject.Inject;

public final class au
  implements as
{
  private long a;
  private final ContentResolver b;
  private final c c;
  private final h d;
  private final g e;
  
  @Inject
  public au(ContentResolver paramContentResolver, c paramc, h paramh, g paramg)
  {
    b = paramContentResolver;
    c = paramc;
    d = paramh;
    e = paramg;
    a = -1L;
  }
  
  private final ContentProviderResult[] a(ArrayList<ContentProviderOperation> paramArrayList)
  {
    try
    {
      paramArrayList = b.applyBatch(TruecallerContract.a(), paramArrayList);
      return paramArrayList;
    }
    catch (RemoteException|OperationApplicationException paramArrayList)
    {
      for (;;) {}
    }
    return null;
  }
  
  public final w<Boolean> a(Reaction[] paramArrayOfReaction)
  {
    k.b(paramArrayOfReaction, "reactions");
    ArrayList localArrayList = new ArrayList();
    Object localObject1 = (Collection)new ArrayList(paramArrayOfReaction.length);
    int j = paramArrayOfReaction.length;
    boolean bool2 = false;
    int i = 0;
    Reaction localReaction;
    while (i < j)
    {
      localReaction = paramArrayOfReaction[i];
      ((Collection)localObject1).add(ContentProviderOperation.newDelete(TruecallerContract.u.a()).withSelection("message_id=? AND from_peer_id=?", new String[] { String.valueOf(b), c }).build());
      i += 1;
    }
    localArrayList.addAll((Collection)localObject1);
    localObject1 = (Collection)new ArrayList();
    int k = paramArrayOfReaction.length;
    i = 0;
    Object localObject2;
    while (i < k)
    {
      localReaction = paramArrayOfReaction[i];
      localObject2 = (CharSequence)d;
      if ((localObject2 != null) && (((CharSequence)localObject2).length() != 0)) {
        j = 0;
      } else {
        j = 1;
      }
      if ((j ^ 0x1) != 0) {
        ((Collection)localObject1).add(localReaction);
      }
      i += 1;
    }
    localObject1 = (Iterable)localObject1;
    paramArrayOfReaction = (Collection)new ArrayList(m.a((Iterable)localObject1, 10));
    localObject1 = ((Iterable)localObject1).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localReaction = (Reaction)((Iterator)localObject1).next();
      localObject2 = ContentProviderOperation.newInsert(TruecallerContract.u.a());
      ((ContentProviderOperation.Builder)localObject2).withValue("message_id", Long.valueOf(b));
      ((ContentProviderOperation.Builder)localObject2).withValue("from_peer_id", c);
      ((ContentProviderOperation.Builder)localObject2).withValue("emoji", d);
      ((ContentProviderOperation.Builder)localObject2).withValue("send_date", Long.valueOf(e));
      paramArrayOfReaction.add(((ContentProviderOperation.Builder)localObject2).withValue("status", Integer.valueOf(f)).build());
    }
    localArrayList.addAll((Collection)paramArrayOfReaction);
    paramArrayOfReaction = a(localArrayList);
    boolean bool1 = bool2;
    if (paramArrayOfReaction != null)
    {
      if (paramArrayOfReaction.length == 0) {
        i = 1;
      } else {
        i = 0;
      }
      bool1 = bool2;
      if (i == 0) {
        bool1 = true;
      }
    }
    paramArrayOfReaction = w.b(Boolean.valueOf(bool1));
    k.a(paramArrayOfReaction, "Promise.wrap(isSuccess)");
    return paramArrayOfReaction;
  }
  
  /* Error */
  @android.annotation.SuppressLint({"Recycle"})
  public final void a()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 37	com/truecaller/messaging/transport/im/au:b	Landroid/content/ContentResolver;
    //   4: invokestatic 211	com/truecaller/content/TruecallerContract$ai:a	()Landroid/net/Uri;
    //   7: aconst_null
    //   8: ldc -43
    //   10: iconst_3
    //   11: anewarray 93	java/lang/String
    //   14: dup
    //   15: iconst_0
    //   16: ldc -41
    //   18: aastore
    //   19: dup
    //   20: iconst_1
    //   21: ldc -41
    //   23: aastore
    //   24: dup
    //   25: iconst_2
    //   26: aload_0
    //   27: getfield 47	com/truecaller/messaging/transport/im/au:a	J
    //   30: invokestatic 101	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   33: aastore
    //   34: ldc -39
    //   36: invokevirtual 221	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   39: astore_1
    //   40: aload_0
    //   41: getfield 39	com/truecaller/messaging/transport/im/au:c	Lcom/truecaller/messaging/data/c;
    //   44: aload_1
    //   45: invokeinterface 227 2 0
    //   50: astore_3
    //   51: aconst_null
    //   52: astore_1
    //   53: aconst_null
    //   54: astore_2
    //   55: aload_3
    //   56: ifnull +104 -> 160
    //   59: aload_3
    //   60: checkcast 229	android/database/Cursor
    //   63: astore 4
    //   65: aload 4
    //   67: checkcast 231	java/io/Closeable
    //   70: astore_3
    //   71: aload_2
    //   72: astore_1
    //   73: new 72	java/util/ArrayList
    //   76: dup
    //   77: invokespecial 73	java/util/ArrayList:<init>	()V
    //   80: checkcast 78	java/util/Collection
    //   83: astore 5
    //   85: aload_2
    //   86: astore_1
    //   87: aload 4
    //   89: invokeinterface 234 1 0
    //   94: ifeq +26 -> 120
    //   97: aload_2
    //   98: astore_1
    //   99: aload 5
    //   101: aload 4
    //   103: checkcast 236	com/truecaller/messaging/data/a/n
    //   106: invokeinterface 239 1 0
    //   111: invokeinterface 118 2 0
    //   116: pop
    //   117: goto -32 -> 85
    //   120: aload_2
    //   121: astore_1
    //   122: aload 5
    //   124: checkcast 120	java/util/List
    //   127: astore_2
    //   128: aload_3
    //   129: aconst_null
    //   130: invokestatic 244	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   133: aload_2
    //   134: checkcast 134	java/lang/Iterable
    //   137: invokestatic 249	c/a/ag:a	(Ljava/lang/Iterable;)Ljava/util/Map;
    //   140: astore_1
    //   141: goto +19 -> 160
    //   144: astore_2
    //   145: goto +8 -> 153
    //   148: astore_2
    //   149: aload_2
    //   150: astore_1
    //   151: aload_2
    //   152: athrow
    //   153: aload_3
    //   154: aload_1
    //   155: invokestatic 244	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   158: aload_2
    //   159: athrow
    //   160: aload_0
    //   161: getfield 43	com/truecaller/messaging/transport/im/au:e	Lcom/truecaller/messaging/notifications/g;
    //   164: aload_1
    //   165: invokeinterface 254 2 0
    //   170: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	171	0	this	au
    //   39	126	1	localObject1	Object
    //   54	80	2	localList	java.util.List
    //   144	1	2	localObject2	Object
    //   148	11	2	localThrowable	Throwable
    //   50	104	3	localObject3	Object
    //   63	39	4	localCursor	android.database.Cursor
    //   83	40	5	localCollection	Collection
    // Exception table:
    //   from	to	target	type
    //   73	85	144	finally
    //   87	97	144	finally
    //   99	117	144	finally
    //   122	128	144	finally
    //   151	153	144	finally
    //   73	85	148	java/lang/Throwable
    //   87	97	148	java/lang/Throwable
    //   99	117	148	java/lang/Throwable
    //   122	128	148	java/lang/Throwable
  }
  
  /* Error */
  @android.annotation.SuppressLint({"Recycle"})
  public final void a(long paramLong)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 37	com/truecaller/messaging/transport/im/au:b	Landroid/content/ContentResolver;
    //   4: astore 4
    //   6: invokestatic 259	com/truecaller/content/TruecallerContract$aa:a	()Landroid/net/Uri;
    //   9: astore 5
    //   11: ldc_w 261
    //   14: ldc_w 263
    //   17: invokestatic 266	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   20: invokevirtual 270	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   23: astore 6
    //   25: iconst_0
    //   26: istore_3
    //   27: aload 4
    //   29: aload 5
    //   31: iconst_1
    //   32: anewarray 93	java/lang/String
    //   35: dup
    //   36: iconst_0
    //   37: ldc_w 272
    //   40: aastore
    //   41: aload 6
    //   43: iconst_1
    //   44: anewarray 93	java/lang/String
    //   47: dup
    //   48: iconst_0
    //   49: lload_1
    //   50: invokestatic 101	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   53: aastore
    //   54: aconst_null
    //   55: invokevirtual 221	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   58: astore 7
    //   60: aload 7
    //   62: ifnull +141 -> 203
    //   65: aload 7
    //   67: checkcast 231	java/io/Closeable
    //   70: astore 6
    //   72: aconst_null
    //   73: astore 5
    //   75: aload 5
    //   77: astore 4
    //   79: new 72	java/util/ArrayList
    //   82: dup
    //   83: invokespecial 73	java/util/ArrayList:<init>	()V
    //   86: checkcast 78	java/util/Collection
    //   89: astore 8
    //   91: aload 5
    //   93: astore 4
    //   95: aload 7
    //   97: invokeinterface 234 1 0
    //   102: ifeq +29 -> 131
    //   105: aload 5
    //   107: astore 4
    //   109: aload 8
    //   111: aload 7
    //   113: iconst_0
    //   114: invokeinterface 276 2 0
    //   119: invokestatic 163	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   122: invokeinterface 118 2 0
    //   127: pop
    //   128: goto -37 -> 91
    //   131: aload 5
    //   133: astore 4
    //   135: aload 8
    //   137: checkcast 120	java/util/List
    //   140: astore 5
    //   142: aload 6
    //   144: aconst_null
    //   145: invokestatic 244	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   148: aload 5
    //   150: checkcast 78	java/util/Collection
    //   153: invokestatic 279	c/a/m:c	(Ljava/util/Collection;)[J
    //   156: astore 4
    //   158: aload 4
    //   160: arraylength
    //   161: ifne +5 -> 166
    //   164: iconst_1
    //   165: istore_3
    //   166: iload_3
    //   167: iconst_1
    //   168: ixor
    //   169: ifeq +9 -> 178
    //   172: aload_0
    //   173: aload 4
    //   175: invokevirtual 282	com/truecaller/messaging/transport/im/au:a	([J)V
    //   178: return
    //   179: astore 5
    //   181: goto +12 -> 193
    //   184: astore 5
    //   186: aload 5
    //   188: astore 4
    //   190: aload 5
    //   192: athrow
    //   193: aload 6
    //   195: aload 4
    //   197: invokestatic 244	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   200: aload 5
    //   202: athrow
    //   203: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	204	0	this	au
    //   0	204	1	paramLong	long
    //   26	143	3	i	int
    //   4	192	4	localObject1	Object
    //   9	140	5	localObject2	Object
    //   179	1	5	localObject3	Object
    //   184	17	5	localThrowable	Throwable
    //   23	171	6	localObject4	Object
    //   58	54	7	localCursor	android.database.Cursor
    //   89	47	8	localCollection	Collection
    // Exception table:
    //   from	to	target	type
    //   79	91	179	finally
    //   95	105	179	finally
    //   109	128	179	finally
    //   135	142	179	finally
    //   190	193	179	finally
    //   79	91	184	java/lang/Throwable
    //   95	105	184	java/lang/Throwable
    //   109	128	184	java/lang/Throwable
    //   135	142	184	java/lang/Throwable
  }
  
  public final void a(long[] paramArrayOfLong)
  {
    k.b(paramArrayOfLong, "messageIds");
    ArrayList localArrayList = new ArrayList();
    int k = paramArrayOfLong.length;
    int j = 0;
    int i = 0;
    while (i < k)
    {
      long l = paramArrayOfLong[i];
      localArrayList.add(ContentProviderOperation.newUpdate(TruecallerContract.u.a()).withSelection("message_id=?", new String[] { String.valueOf(l) }).withValue("status", Integer.valueOf(0)).build());
      i += 1;
    }
    paramArrayOfLong = a(localArrayList);
    if (paramArrayOfLong != null)
    {
      if (paramArrayOfLong.length == 0) {
        i = 1;
      } else {
        i = 0;
      }
      if (i == 0) {}
    }
    else
    {
      j = 1;
    }
    if (j == 0) {
      a();
    }
  }
  
  public final void b(long paramLong)
  {
    a = paramLong;
  }
  
  public final void c(long paramLong)
  {
    if (a == paramLong) {
      a = -1L;
    }
  }
  
  /* Error */
  public final w<String> d(long paramLong)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 37	com/truecaller/messaging/transport/im/au:b	Landroid/content/ContentResolver;
    //   4: astore_3
    //   5: invokestatic 83	com/truecaller/content/TruecallerContract$u:a	()Landroid/net/Uri;
    //   8: astore 4
    //   10: aload_0
    //   11: getfield 41	com/truecaller/messaging/transport/im/au:d	Lcom/truecaller/messaging/h;
    //   14: invokeinterface 298 1 0
    //   19: astore 5
    //   21: aload_3
    //   22: aload 4
    //   24: iconst_1
    //   25: anewarray 93	java/lang/String
    //   28: dup
    //   29: iconst_0
    //   30: ldc -85
    //   32: aastore
    //   33: ldc_w 300
    //   36: iconst_2
    //   37: anewarray 93	java/lang/String
    //   40: dup
    //   41: iconst_0
    //   42: aload 5
    //   44: aastore
    //   45: dup
    //   46: iconst_1
    //   47: lload_1
    //   48: invokestatic 101	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   51: aastore
    //   52: aconst_null
    //   53: invokevirtual 221	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   56: astore 5
    //   58: aconst_null
    //   59: astore_3
    //   60: aconst_null
    //   61: astore 4
    //   63: aload 5
    //   65: ifnull +85 -> 150
    //   68: aload 5
    //   70: checkcast 231	java/io/Closeable
    //   73: astore 5
    //   75: aload 4
    //   77: astore_3
    //   78: aload 5
    //   80: checkcast 229	android/database/Cursor
    //   83: astore 6
    //   85: aload 4
    //   87: astore_3
    //   88: aload 6
    //   90: invokeinterface 303 1 0
    //   95: ifeq +22 -> 117
    //   98: aload 4
    //   100: astore_3
    //   101: aload 6
    //   103: iconst_0
    //   104: invokeinterface 307 2 0
    //   109: astore 4
    //   111: aload 4
    //   113: astore_3
    //   114: goto +5 -> 119
    //   117: aconst_null
    //   118: astore_3
    //   119: aload 5
    //   121: aconst_null
    //   122: invokestatic 244	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   125: goto +25 -> 150
    //   128: astore 4
    //   130: goto +11 -> 141
    //   133: astore 4
    //   135: aload 4
    //   137: astore_3
    //   138: aload 4
    //   140: athrow
    //   141: aload 5
    //   143: aload_3
    //   144: invokestatic 244	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   147: aload 4
    //   149: athrow
    //   150: aload_3
    //   151: invokestatic 198	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   154: astore_3
    //   155: aload_3
    //   156: ldc_w 309
    //   159: invokestatic 202	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   162: aload_3
    //   163: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	164	0	this	au
    //   0	164	1	paramLong	long
    //   4	159	3	localObject1	Object
    //   8	104	4	localObject2	Object
    //   128	1	4	localObject3	Object
    //   133	15	4	localThrowable	Throwable
    //   19	123	5	localObject4	Object
    //   83	19	6	localCursor	android.database.Cursor
    // Exception table:
    //   from	to	target	type
    //   78	85	128	finally
    //   88	98	128	finally
    //   101	111	128	finally
    //   138	141	128	finally
    //   78	85	133	java/lang/Throwable
    //   88	98	133	java/lang/Throwable
    //   101	111	133	java/lang/Throwable
  }
  
  /* Error */
  @android.annotation.SuppressLint({"Recycle"})
  public final w<java.util.Map<Reaction, com.truecaller.messaging.data.types.Participant>> e(long paramLong)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 37	com/truecaller/messaging/transport/im/au:b	Landroid/content/ContentResolver;
    //   4: invokestatic 211	com/truecaller/content/TruecallerContract$ai:a	()Landroid/net/Uri;
    //   7: aconst_null
    //   8: ldc_w 312
    //   11: iconst_1
    //   12: anewarray 93	java/lang/String
    //   15: dup
    //   16: iconst_0
    //   17: lload_1
    //   18: invokestatic 101	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   21: aastore
    //   22: aconst_null
    //   23: invokevirtual 221	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   26: astore_3
    //   27: aload_0
    //   28: getfield 39	com/truecaller/messaging/transport/im/au:c	Lcom/truecaller/messaging/data/c;
    //   31: aload_3
    //   32: invokeinterface 227 2 0
    //   37: astore 5
    //   39: aconst_null
    //   40: astore_3
    //   41: aconst_null
    //   42: astore 4
    //   44: aload 5
    //   46: ifnull +119 -> 165
    //   49: aload 5
    //   51: checkcast 229	android/database/Cursor
    //   54: astore 6
    //   56: aload 6
    //   58: checkcast 231	java/io/Closeable
    //   61: astore 5
    //   63: aload 4
    //   65: astore_3
    //   66: new 72	java/util/ArrayList
    //   69: dup
    //   70: invokespecial 73	java/util/ArrayList:<init>	()V
    //   73: checkcast 78	java/util/Collection
    //   76: astore 7
    //   78: aload 4
    //   80: astore_3
    //   81: aload 6
    //   83: invokeinterface 234 1 0
    //   88: ifeq +27 -> 115
    //   91: aload 4
    //   93: astore_3
    //   94: aload 7
    //   96: aload 6
    //   98: checkcast 236	com/truecaller/messaging/data/a/n
    //   101: invokeinterface 239 1 0
    //   106: invokeinterface 118 2 0
    //   111: pop
    //   112: goto -34 -> 78
    //   115: aload 4
    //   117: astore_3
    //   118: aload 7
    //   120: checkcast 120	java/util/List
    //   123: astore 4
    //   125: aload 5
    //   127: aconst_null
    //   128: invokestatic 244	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   131: aload 4
    //   133: checkcast 134	java/lang/Iterable
    //   136: invokestatic 249	c/a/ag:a	(Ljava/lang/Iterable;)Ljava/util/Map;
    //   139: astore_3
    //   140: goto +25 -> 165
    //   143: astore 4
    //   145: goto +11 -> 156
    //   148: astore 4
    //   150: aload 4
    //   152: astore_3
    //   153: aload 4
    //   155: athrow
    //   156: aload 5
    //   158: aload_3
    //   159: invokestatic 244	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   162: aload 4
    //   164: athrow
    //   165: aload_3
    //   166: invokestatic 198	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   169: astore_3
    //   170: aload_3
    //   171: ldc_w 314
    //   174: invokestatic 202	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   177: aload_3
    //   178: ldc_w 316
    //   181: invokestatic 202	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   184: aload_3
    //   185: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	186	0	this	au
    //   0	186	1	paramLong	long
    //   26	159	3	localObject1	Object
    //   42	90	4	localList	java.util.List
    //   143	1	4	localObject2	Object
    //   148	15	4	localThrowable	Throwable
    //   37	120	5	localObject3	Object
    //   54	43	6	localCursor	android.database.Cursor
    //   76	43	7	localCollection	Collection
    // Exception table:
    //   from	to	target	type
    //   66	78	143	finally
    //   81	91	143	finally
    //   94	112	143	finally
    //   118	125	143	finally
    //   153	156	143	finally
    //   66	78	148	java/lang/Throwable
    //   81	91	148	java/lang/Throwable
    //   94	112	148	java/lang/Throwable
    //   118	125	148	java/lang/Throwable
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.au
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */