package com.truecaller.messaging.transport.im;

import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.featuretoggles.b;
import com.truecaller.messaging.h;
import java.lang.ref.WeakReference;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

public final class bv
  implements bu
{
  final com.truecaller.common.g.a a;
  final dagger.a<cb> b;
  final f c;
  private final h d;
  private final com.truecaller.featuretoggles.e e;
  private final f f;
  
  @Inject
  public bv(h paramh, com.truecaller.common.g.a parama, com.truecaller.featuretoggles.e parame, dagger.a<cb> parama1, @Named("IO") f paramf1, @Named("UI") f paramf2)
  {
    d = paramh;
    a = parama;
    e = parame;
    b = parama1;
    f = paramf1;
    c = paramf2;
  }
  
  public final int a()
  {
    if (e.i().a()) {
      return 5;
    }
    return 4;
  }
  
  public final void a(int paramInt)
  {
    d.l(paramInt);
  }
  
  public final void a(WeakReference<ce> paramWeakReference)
  {
    k.b(paramWeakReference, "listenerRef");
    if (c() < a()) {
      return;
    }
    kotlinx.coroutines.e.b((ag)bg.a, f, (m)new bv.a(this, paramWeakReference, null), 2);
  }
  
  public final boolean b()
  {
    return !b(d.I());
  }
  
  public final boolean b(int paramInt)
  {
    return a() >= paramInt;
  }
  
  public final int c()
  {
    return d.X();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bv
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */