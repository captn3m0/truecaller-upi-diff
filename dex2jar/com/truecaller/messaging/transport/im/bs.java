package com.truecaller.messaging.transport.im;

import android.content.ContentResolver;
import com.truecaller.androidactors.f;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.h;
import com.truecaller.presence.c;
import com.truecaller.util.al;
import com.truecaller.utils.a;
import com.truecaller.utils.i;
import dagger.a.d;
import javax.inject.Provider;

public final class bs
  implements d<br>
{
  private final Provider<Integer> a;
  private final Provider<Integer> b;
  private final Provider<a> c;
  private final Provider<cb> d;
  private final Provider<ContentResolver> e;
  private final Provider<bw> f;
  private final Provider<al> g;
  private final Provider<h> h;
  private final Provider<i> i;
  private final Provider<f<c>> j;
  
  private bs(Provider<Integer> paramProvider1, Provider<Integer> paramProvider2, Provider<a> paramProvider, Provider<cb> paramProvider3, Provider<ContentResolver> paramProvider4, Provider<bw> paramProvider5, Provider<al> paramProvider6, Provider<h> paramProvider7, Provider<i> paramProvider8, Provider<f<c>> paramProvider9)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
  }
  
  public static bs a(Provider<Integer> paramProvider1, Provider<Integer> paramProvider2, Provider<a> paramProvider, Provider<cb> paramProvider3, Provider<ContentResolver> paramProvider4, Provider<bw> paramProvider5, Provider<al> paramProvider6, Provider<h> paramProvider7, Provider<i> paramProvider8, Provider<f<c>> paramProvider9)
  {
    return new bs(paramProvider1, paramProvider2, paramProvider, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bs
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */