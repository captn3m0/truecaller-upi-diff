package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.i;
import com.truecaller.androidactors.k;
import dagger.a.d;
import javax.inject.Provider;

public final class ab
  implements d<i>
{
  private final Provider<k> a;
  
  private ab(Provider<k> paramProvider)
  {
    a = paramProvider;
  }
  
  public static ab a(Provider<k> paramProvider)
  {
    return new ab(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.ab
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */