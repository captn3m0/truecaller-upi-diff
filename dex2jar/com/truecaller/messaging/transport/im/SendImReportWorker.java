package com.truecaller.messaging.transport.im;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import androidx.work.e;
import c.a.an;
import c.g.b.k;
import c.l;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.w;
import com.truecaller.api.services.messenger.v1.models.input.InputReportType;
import com.truecaller.bp;
import java.util.Set;
import javax.inject.Inject;

public final class SendImReportWorker
  extends Worker
{
  public static final SendImReportWorker.a c = new SendImReportWorker.a((byte)0);
  @Inject
  public q b;
  
  public SendImReportWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public final ListenableWorker.a a()
  {
    Object localObject = InputReportType.forNumber(getInputData().a("report_type", 0));
    String str1 = getInputData().b("raw_message_id");
    String str2 = getInputData().b("im_peer_id");
    String str3 = getInputData().b("im_group_id");
    if (localObject != null) {
      if ((an.a(new InputReportType[] { InputReportType.RECEIVED, InputReportType.READ }).contains(localObject)) && (str1 != null) && (str2 != null))
      {
        q localq = b;
        if (localq == null) {
          k.a("imManager");
        }
        localObject = (SendResult)localq.a((InputReportType)localObject, str1, str2, str3).d();
        if (localObject != null) {}
        switch (cf.a[localObject.ordinal()])
        {
        default: 
          throw new l();
        case 3: 
          localObject = ListenableWorker.a.a();
          k.a(localObject, "Result.success()");
          return (ListenableWorker.a)localObject;
        case 2: 
          if (getRunAttemptCount() < 3) {
            localObject = ListenableWorker.a.b();
          } else {
            localObject = ListenableWorker.a.a();
          }
          k.a(localObject, "if (runAttemptCount < MA…y() else Result.success()");
          return (ListenableWorker.a)localObject;
        }
        localObject = ListenableWorker.a.a();
        k.a(localObject, "Result.success()");
        return (ListenableWorker.a)localObject;
      }
    }
    localObject = ListenableWorker.a.a();
    k.a(localObject, "Result.success()");
    return (ListenableWorker.a)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.SendImReportWorker
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */