package com.truecaller.messaging.transport.im;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.bp;
import com.truecaller.common.background.TrackedWorker;
import javax.inject.Inject;

public final class FetchImContactsWorker
  extends TrackedWorker
{
  public static final FetchImContactsWorker.a d = new FetchImContactsWorker.a((byte)0);
  @Inject
  public j b;
  @Inject
  public b c;
  
  public FetchImContactsWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public final b b()
  {
    b localb = c;
    if (localb == null) {
      k.a("analytics");
    }
    return localb;
  }
  
  public final boolean c()
  {
    j localj = b;
    if (localj == null) {
      k.a("imContactFetcher");
    }
    return localj.a();
  }
  
  public final ListenableWorker.a d()
  {
    Object localObject = b;
    if (localObject == null) {
      k.a("imContactFetcher");
    }
    ((j)localObject).b();
    localObject = ListenableWorker.a.a();
    k.a(localObject, "Result.success()");
    return (ListenableWorker.a)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.FetchImContactsWorker
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */