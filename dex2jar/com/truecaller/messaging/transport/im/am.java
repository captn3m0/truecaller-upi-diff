package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.i;
import com.truecaller.androidactors.k;
import dagger.a.d;
import javax.inject.Provider;

public final class am
  implements d<i>
{
  private final Provider<k> a;
  
  private am(Provider<k> paramProvider)
  {
    a = paramProvider;
  }
  
  public static am a(Provider<k> paramProvider)
  {
    return new am(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.am
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */