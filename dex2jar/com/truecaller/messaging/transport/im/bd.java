package com.truecaller.messaging.transport.im;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.Parcelable;
import android.os.RemoteException;
import androidx.work.c.a;
import androidx.work.j;
import androidx.work.k.a;
import androidx.work.p;
import c.a.an;
import c.a.m;
import c.n;
import com.google.f.x;
import com.truecaller.analytics.ae;
import com.truecaller.androidactors.w;
import com.truecaller.api.services.messenger.v1.events.Event;
import com.truecaller.api.services.messenger.v1.events.Event.d;
import com.truecaller.api.services.messenger.v1.events.Event.f;
import com.truecaller.api.services.messenger.v1.events.Event.j;
import com.truecaller.api.services.messenger.v1.events.Event.l;
import com.truecaller.api.services.messenger.v1.events.Event.n;
import com.truecaller.api.services.messenger.v1.events.Event.p;
import com.truecaller.api.services.messenger.v1.events.Event.t;
import com.truecaller.api.services.messenger.v1.events.Event.v;
import com.truecaller.api.services.messenger.v1.events.Event.x;
import com.truecaller.api.services.messenger.v1.models.MessageContent;
import com.truecaller.api.services.messenger.v1.models.Peer;
import com.truecaller.api.services.messenger.v1.models.Peer.TypeCase;
import com.truecaller.api.services.messenger.v1.models.Peer.b;
import com.truecaller.api.services.messenger.v1.models.Peer.d;
import com.truecaller.api.services.messenger.v1.models.ReactionContent;
import com.truecaller.api.services.messenger.v1.models.ReactionContent.b;
import com.truecaller.common.h.am;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.aa;
import com.truecaller.content.TruecallerContract.p;
import com.truecaller.content.TruecallerContract.r;
import com.truecaller.content.TruecallerContract.w;
import com.truecaller.content.TruecallerContract.z;
import com.truecaller.insights.models.d.a;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Reaction;
import com.truecaller.messaging.data.types.TextEntity;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.transport.ad;
import com.truecaller.messaging.transport.ad.b;
import com.truecaller.messaging.transport.l;
import com.truecaller.messaging.transport.l.a;
import com.truecaller.tracking.events.s;
import com.truecaller.tracking.events.s.a;
import com.truecaller.tracking.events.u.a;
import com.truecaller.util.ch;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;

public final class bd
  implements l<bh>
{
  private final ContentResolver a;
  private final com.truecaller.messaging.data.c b;
  private final cb c;
  private final com.truecaller.androidactors.f<t> d;
  private final ad.b e;
  private final com.truecaller.messaging.h f;
  private final bb g;
  private final com.truecaller.androidactors.f<q> h;
  private final com.truecaller.messaging.data.providers.c i;
  private final com.truecaller.utils.i j;
  private final com.truecaller.androidactors.f<ae> k;
  private final com.truecaller.analytics.b l;
  private final f m;
  private final bu n;
  private final com.truecaller.androidactors.f<bp> o;
  private final com.truecaller.androidactors.f<as> p;
  private final com.truecaller.androidactors.f<bj> q;
  private final com.truecaller.featuretoggles.e r;
  private final com.truecaller.messaging.categorizer.b s;
  private final com.truecaller.util.f.b t;
  private final ch u;
  private final com.truecaller.search.local.model.g v;
  private final p w;
  private final com.truecaller.androidactors.f<com.truecaller.messaging.transport.im.a.a> x;
  
  @Inject
  public bd(ContentResolver paramContentResolver, com.truecaller.messaging.data.c paramc, cb paramcb, com.truecaller.androidactors.f<t> paramf, ad.b paramb, com.truecaller.messaging.h paramh, bb parambb, com.truecaller.androidactors.f<q> paramf1, com.truecaller.messaging.data.providers.c paramc1, com.truecaller.utils.i parami, com.truecaller.androidactors.f<ae> paramf2, com.truecaller.analytics.b paramb1, f paramf3, bu parambu, com.truecaller.androidactors.f<bp> paramf4, com.truecaller.androidactors.f<as> paramf5, com.truecaller.androidactors.f<bj> paramf6, com.truecaller.featuretoggles.e parame, com.truecaller.messaging.categorizer.b paramb2, com.truecaller.util.f.b paramb3, ch paramch, com.truecaller.search.local.model.g paramg, p paramp, com.truecaller.androidactors.f<com.truecaller.messaging.transport.im.a.a> paramf7)
  {
    a = paramContentResolver;
    b = paramc;
    c = paramcb;
    d = paramf;
    e = paramb;
    f = paramh;
    g = parambb;
    h = paramf1;
    i = paramc1;
    j = parami;
    k = paramf2;
    l = paramb1;
    m = paramf3;
    n = parambu;
    o = paramf4;
    p = paramf5;
    q = paramf6;
    r = parame;
    s = paramb2;
    t = paramb3;
    u = paramch;
    v = paramg;
    w = paramp;
    x = paramf7;
  }
  
  private final ImTransportInfo a(ImTransportInfo paramImTransportInfo, int paramInt)
  {
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("error_code", Integer.valueOf(paramInt));
    a.update(TruecallerContract.w.a(), localContentValues, "message_id=?", new String[] { String.valueOf(a) });
    paramImTransportInfo = paramImTransportInfo.g();
    h = paramInt;
    return paramImTransportInfo.a();
  }
  
  private final void a(int paramInt, Intent paramIntent)
  {
    paramIntent = ((ImTransportInfo)paramIntent.getParcelableExtra("transport_info")).g();
    g = paramInt;
    paramIntent = paramIntent.a();
    paramIntent = new Message.a().a(Participant.a).a(2, (TransportInfo)paramIntent).b();
    c.g.b.k.a(paramIntent, "Message.Builder()\n      …nfo)\n            .build()");
    ((t)d.a()).b(paramIntent);
  }
  
  private final void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6)
  {
    paramString3 = (CharSequence)paramString3;
    int i1;
    if ((paramString3 != null) && (paramString3.length() != 0)) {
      i1 = 0;
    } else {
      i1 = 1;
    }
    if (i1 != 0) {
      paramString3 = "Personal";
    } else {
      paramString3 = "Group";
    }
    com.truecaller.analytics.b localb = l;
    paramString5 = new com.truecaller.analytics.e.a("ImReaction").a("Context", paramString3).a("Action", paramString5);
    if (paramString4 == null) {
      paramString3 = "";
    } else {
      paramString3 = paramString4;
    }
    paramString3 = paramString5.a("SubAction", paramString3).a();
    c.g.b.k.a(paramString3, "AnalyticsEvent.Builder(A…\n                .build()");
    localb.b(paramString3);
    paramString3 = (ae)k.a();
    paramString5 = s.b();
    if (paramString2 == null) {
      paramString2 = "";
    }
    paramString2 = paramString5.b((CharSequence)paramString2).a((CharSequence)paramString1).c((CharSequence)paramString6);
    if (paramString4 != null) {
      paramString1 = (CharSequence)paramString4;
    } else {
      paramString1 = (CharSequence)"";
    }
    paramString3.a((org.apache.a.d.d)paramString2.d(paramString1).a());
  }
  
  private final void a(ArrayList<ContentProviderOperation> paramArrayList, long paramLong, Entity[] paramArrayOfEntity)
  {
    Object localObject2 = (Collection)new ArrayList(paramArrayOfEntity.length);
    int i2 = paramArrayOfEntity.length;
    int i1 = 0;
    Object localObject3;
    while (i1 < i2)
    {
      localObject3 = paramArrayOfEntity[i1];
      ContentValues localContentValues = new ContentValues();
      ((Entity)localObject3).a(localContentValues);
      if (!((Entity)localObject3).a())
      {
        if ((localObject3 instanceof ImageEntityWithThumbnail))
        {
          localObject1 = m;
          Object localObject4 = (ImageEntityWithThumbnail)localObject3;
          byte[] arrayOfByte = l;
          i3 = a;
          int i4 = k;
          localObject4 = j;
          c.g.b.k.a(localObject4, "entity.type");
          localObject1 = ((f)localObject1).a(paramLong, arrayOfByte, i3, i4, (String)localObject4);
        }
        else
        {
          localObject1 = Uri.EMPTY;
        }
        localContentValues.put("content", ((Uri)localObject1).toString());
      }
      int i3 = paramArrayList.size();
      paramArrayList.add(ContentProviderOperation.newInsert(TruecallerContract.z.a()).withValues(localContentValues).withValue("message_id", Long.valueOf(paramLong)).build());
      ((Collection)localObject2).add(new n(localObject3, Integer.valueOf(i3)));
      i1 += 1;
    }
    Object localObject1 = (Iterable)localObject2;
    paramArrayOfEntity = (Collection)new ArrayList();
    localObject1 = ((Iterable)localObject1).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = ((Iterator)localObject1).next();
      if (((Entity)a instanceof BinaryEntity)) {
        paramArrayOfEntity.add(localObject2);
      }
    }
    localObject1 = (Iterable)paramArrayOfEntity;
    paramArrayOfEntity = (Collection)new ArrayList(m.a((Iterable)localObject1, 10));
    localObject1 = ((Iterable)localObject1).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject3 = (n)((Iterator)localObject1).next();
      localObject2 = (Entity)a;
      i1 = ((Number)b).intValue();
      localObject3 = ContentProviderOperation.newInsert(TruecallerContract.p.a());
      if (localObject2 != null) {
        paramArrayOfEntity.add(((ContentProviderOperation.Builder)localObject3).withValue("uri", b.toString()).withValueBackReference("entity_id", i1).build());
      } else {
        throw new c.u("null cannot be cast to non-null type com.truecaller.messaging.data.types.BinaryEntity");
      }
    }
    m.b((Iterable)paramArrayOfEntity, (Collection)paramArrayList);
  }
  
  /* Error */
  private final boolean a(Message paramMessage, String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 117	com/truecaller/messaging/transport/im/bd:a	Landroid/content/ContentResolver;
    //   4: astore 7
    //   6: invokestatic 188	com/truecaller/content/TruecallerContract$w:a	()Landroid/net/Uri;
    //   9: astore 8
    //   11: iconst_1
    //   12: istore 4
    //   14: aload 7
    //   16: aload 8
    //   18: iconst_1
    //   19: anewarray 192	java/lang/String
    //   22: dup
    //   23: iconst_0
    //   24: ldc_w 396
    //   27: aastore
    //   28: ldc_w 480
    //   31: iconst_1
    //   32: anewarray 192	java/lang/String
    //   35: dup
    //   36: iconst_0
    //   37: aload_2
    //   38: aastore
    //   39: aconst_null
    //   40: invokevirtual 484	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   43: astore_2
    //   44: aload_2
    //   45: ifnull +176 -> 221
    //   48: aload_2
    //   49: checkcast 486	java/io/Closeable
    //   52: astore 8
    //   54: aconst_null
    //   55: astore 7
    //   57: aload 7
    //   59: astore_2
    //   60: aload 8
    //   62: checkcast 488	android/database/Cursor
    //   65: astore 9
    //   67: aload 7
    //   69: astore_2
    //   70: aload 9
    //   72: invokeinterface 491 1 0
    //   77: ifeq +112 -> 189
    //   80: aload 7
    //   82: astore_2
    //   83: aload 9
    //   85: aload 9
    //   87: ldc_w 396
    //   90: invokeinterface 495 2 0
    //   95: invokeinterface 499 2 0
    //   100: lstore 5
    //   102: aload 7
    //   104: astore_2
    //   105: new 170	android/content/ContentValues
    //   108: dup
    //   109: invokespecial 171	android/content/ContentValues:<init>	()V
    //   112: astore 9
    //   114: aload 7
    //   116: astore_2
    //   117: aload_1
    //   118: getfield 504	com/truecaller/messaging/data/types/Message:d	Lorg/a/a/b;
    //   121: astore_1
    //   122: aload 7
    //   124: astore_2
    //   125: aload_1
    //   126: ldc_w 506
    //   129: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   132: aload 7
    //   134: astore_2
    //   135: aload 9
    //   137: ldc_w 508
    //   140: aload_1
    //   141: getfield 511	org/a/a/a/g:a	J
    //   144: invokestatic 401	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   147: invokevirtual 514	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   150: aload 7
    //   152: astore_2
    //   153: aload_0
    //   154: getfield 117	com/truecaller/messaging/transport/im/bd:a	Landroid/content/ContentResolver;
    //   157: lload 5
    //   159: invokestatic 519	com/truecaller/content/TruecallerContract$aa:a	(J)Landroid/net/Uri;
    //   162: aload 9
    //   164: aconst_null
    //   165: aconst_null
    //   166: invokevirtual 206	android/content/ContentResolver:update	(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   169: istore_3
    //   170: iload_3
    //   171: ifle +6 -> 177
    //   174: goto +6 -> 180
    //   177: iconst_0
    //   178: istore 4
    //   180: aload 8
    //   182: aconst_null
    //   183: invokestatic 524	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   186: iload 4
    //   188: ireturn
    //   189: aload 7
    //   191: astore_2
    //   192: getstatic 529	c/x:a	Lc/x;
    //   195: astore_1
    //   196: aload 8
    //   198: aconst_null
    //   199: invokestatic 524	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   202: iconst_0
    //   203: ireturn
    //   204: astore_1
    //   205: goto +8 -> 213
    //   208: astore_1
    //   209: aload_1
    //   210: astore_2
    //   211: aload_1
    //   212: athrow
    //   213: aload 8
    //   215: aload_2
    //   216: invokestatic 524	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   219: aload_1
    //   220: athrow
    //   221: iconst_0
    //   222: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	223	0	this	bd
    //   0	223	1	paramMessage	Message
    //   0	223	2	paramString	String
    //   169	2	3	i1	int
    //   12	175	4	bool	boolean
    //   100	58	5	l1	long
    //   4	186	7	localContentResolver	ContentResolver
    //   9	205	8	localObject1	Object
    //   65	98	9	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   60	67	204	finally
    //   70	80	204	finally
    //   83	102	204	finally
    //   105	114	204	finally
    //   117	122	204	finally
    //   125	132	204	finally
    //   135	150	204	finally
    //   153	170	204	finally
    //   192	196	204	finally
    //   211	213	204	finally
    //   60	67	208	java/lang/Throwable
    //   70	80	208	java/lang/Throwable
    //   83	102	208	java/lang/Throwable
    //   105	114	208	java/lang/Throwable
    //   117	122	208	java/lang/Throwable
    //   125	132	208	java/lang/Throwable
    //   135	150	208	java/lang/Throwable
    //   153	170	208	java/lang/Throwable
    //   192	196	208	java/lang/Throwable
  }
  
  /* Error */
  private boolean a(TransportInfo paramTransportInfo, bh parambh, boolean paramBoolean)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 532
    //   4: invokestatic 66	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   7: aload_2
    //   8: ldc_w 534
    //   11: invokestatic 66	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   14: aload_2
    //   15: aload_2
    //   16: invokestatic 188	com/truecaller/content/TruecallerContract$w:a	()Landroid/net/Uri;
    //   19: invokevirtual 539	com/truecaller/messaging/transport/im/bh:b	(Landroid/net/Uri;)Lcom/truecaller/messaging/transport/ad$a$a;
    //   22: ldc -66
    //   24: iconst_1
    //   25: anewarray 192	java/lang/String
    //   28: dup
    //   29: iconst_0
    //   30: aload_1
    //   31: invokeinterface 542 1 0
    //   36: invokestatic 200	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   39: aastore
    //   40: invokevirtual 547	com/truecaller/messaging/transport/ad$a$a:a	(Ljava/lang/String;[Ljava/lang/String;)Lcom/truecaller/messaging/transport/ad$a$a;
    //   43: invokevirtual 550	com/truecaller/messaging/transport/ad$a$a:a	()Lcom/truecaller/messaging/transport/ad$a;
    //   46: invokevirtual 553	com/truecaller/messaging/transport/im/bh:a	(Lcom/truecaller/messaging/transport/ad$a;)V
    //   49: aload_2
    //   50: aload_2
    //   51: invokestatic 382	com/truecaller/content/TruecallerContract$z:a	()Landroid/net/Uri;
    //   54: invokevirtual 539	com/truecaller/messaging/transport/im/bh:b	(Landroid/net/Uri;)Lcom/truecaller/messaging/transport/ad$a$a;
    //   57: ldc -66
    //   59: iconst_1
    //   60: anewarray 192	java/lang/String
    //   63: dup
    //   64: iconst_0
    //   65: aload_1
    //   66: invokeinterface 542 1 0
    //   71: invokestatic 200	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   74: aastore
    //   75: invokevirtual 547	com/truecaller/messaging/transport/ad$a$a:a	(Ljava/lang/String;[Ljava/lang/String;)Lcom/truecaller/messaging/transport/ad$a$a;
    //   78: invokevirtual 550	com/truecaller/messaging/transport/ad$a$a:a	()Lcom/truecaller/messaging/transport/ad$a;
    //   81: invokevirtual 553	com/truecaller/messaging/transport/im/bh:a	(Lcom/truecaller/messaging/transport/ad$a;)V
    //   84: aload_2
    //   85: aload_2
    //   86: invokestatic 556	com/truecaller/content/TruecallerContract$u:a	()Landroid/net/Uri;
    //   89: invokevirtual 539	com/truecaller/messaging/transport/im/bh:b	(Landroid/net/Uri;)Lcom/truecaller/messaging/transport/ad$a$a;
    //   92: ldc -66
    //   94: iconst_1
    //   95: anewarray 192	java/lang/String
    //   98: dup
    //   99: iconst_0
    //   100: aload_1
    //   101: invokeinterface 542 1 0
    //   106: invokestatic 200	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   109: aastore
    //   110: invokevirtual 547	com/truecaller/messaging/transport/ad$a$a:a	(Ljava/lang/String;[Ljava/lang/String;)Lcom/truecaller/messaging/transport/ad$a$a;
    //   113: invokevirtual 550	com/truecaller/messaging/transport/ad$a$a:a	()Lcom/truecaller/messaging/transport/ad$a;
    //   116: invokevirtual 553	com/truecaller/messaging/transport/im/bh:a	(Lcom/truecaller/messaging/transport/ad$a;)V
    //   119: aload_2
    //   120: aload_0
    //   121: getfield 133	com/truecaller/messaging/transport/im/bd:i	Lcom/truecaller/messaging/data/providers/c;
    //   124: aload_1
    //   125: invokeinterface 542 1 0
    //   130: invokeinterface 561 3 0
    //   135: invokevirtual 564	com/truecaller/messaging/transport/im/bh:a	(Ljava/lang/Iterable;)V
    //   138: iload_3
    //   139: ifeq +222 -> 361
    //   142: aload_0
    //   143: getfield 119	com/truecaller/messaging/transport/im/bd:b	Lcom/truecaller/messaging/data/c;
    //   146: aload_0
    //   147: getfield 117	com/truecaller/messaging/transport/im/bd:a	Landroid/content/ContentResolver;
    //   150: invokestatic 382	com/truecaller/content/TruecallerContract$z:a	()Landroid/net/Uri;
    //   153: aconst_null
    //   154: ldc -66
    //   156: iconst_1
    //   157: anewarray 192	java/lang/String
    //   160: dup
    //   161: iconst_0
    //   162: aload_1
    //   163: invokeinterface 542 1 0
    //   168: invokestatic 200	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   171: aastore
    //   172: aconst_null
    //   173: invokevirtual 484	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   176: invokeinterface 569 2 0
    //   181: astore_1
    //   182: aload_1
    //   183: ifnull +178 -> 361
    //   186: aload_1
    //   187: checkcast 486	java/io/Closeable
    //   190: astore 5
    //   192: aconst_null
    //   193: astore 4
    //   195: aload 4
    //   197: astore_1
    //   198: aload 5
    //   200: checkcast 571	com/truecaller/messaging/data/a/g
    //   203: astore 6
    //   205: aload 4
    //   207: astore_1
    //   208: aload 6
    //   210: invokeinterface 574 1 0
    //   215: ifeq +114 -> 329
    //   218: aload 4
    //   220: astore_1
    //   221: aload 6
    //   223: ldc_w 576
    //   226: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   229: aload 4
    //   231: astore_1
    //   232: aload 6
    //   234: invokeinterface 579 1 0
    //   239: astore 8
    //   241: aload 4
    //   243: astore_1
    //   244: aload 8
    //   246: instanceof 441
    //   249: ifeq -44 -> 205
    //   252: aload 4
    //   254: astore_1
    //   255: aload_0
    //   256: getfield 133	com/truecaller/messaging/transport/im/bd:i	Lcom/truecaller/messaging/data/providers/c;
    //   259: astore 7
    //   261: aload 4
    //   263: astore_1
    //   264: aload 8
    //   266: checkcast 441	com/truecaller/messaging/data/types/BinaryEntity
    //   269: getfield 460	com/truecaller/messaging/data/types/BinaryEntity:b	Landroid/net/Uri;
    //   272: astore 8
    //   274: aload 4
    //   276: astore_1
    //   277: aload 8
    //   279: ldc_w 581
    //   282: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   285: aload 4
    //   287: astore_1
    //   288: aload 7
    //   290: aload 8
    //   292: invokeinterface 584 2 0
    //   297: astore 7
    //   299: aload 7
    //   301: ifnull -96 -> 205
    //   304: aload 4
    //   306: astore_1
    //   307: aload_2
    //   308: iconst_1
    //   309: anewarray 586	java/io/File
    //   312: dup
    //   313: iconst_0
    //   314: aload 7
    //   316: aastore
    //   317: invokestatic 589	c/a/m:d	([Ljava/lang/Object;)Ljava/util/ArrayList;
    //   320: checkcast 423	java/lang/Iterable
    //   323: invokevirtual 564	com/truecaller/messaging/transport/im/bh:a	(Ljava/lang/Iterable;)V
    //   326: goto -121 -> 205
    //   329: aload 4
    //   331: astore_1
    //   332: getstatic 529	c/x:a	Lc/x;
    //   335: astore_2
    //   336: aload 5
    //   338: aconst_null
    //   339: invokestatic 524	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   342: iconst_1
    //   343: ireturn
    //   344: astore_2
    //   345: goto +8 -> 353
    //   348: astore_2
    //   349: aload_2
    //   350: astore_1
    //   351: aload_2
    //   352: athrow
    //   353: aload 5
    //   355: aload_1
    //   356: invokestatic 524	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   359: aload_2
    //   360: athrow
    //   361: iconst_1
    //   362: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	363	0	this	bd
    //   0	363	1	paramTransportInfo	TransportInfo
    //   0	363	2	parambh	bh
    //   0	363	3	paramBoolean	boolean
    //   193	137	4	localObject1	Object
    //   190	164	5	localCloseable	java.io.Closeable
    //   203	30	6	localg	com.truecaller.messaging.data.a.g
    //   259	56	7	localObject2	Object
    //   239	52	8	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   198	205	344	finally
    //   208	218	344	finally
    //   221	229	344	finally
    //   232	241	344	finally
    //   244	252	344	finally
    //   255	261	344	finally
    //   264	274	344	finally
    //   277	285	344	finally
    //   288	299	344	finally
    //   307	326	344	finally
    //   332	336	344	finally
    //   351	353	344	finally
    //   198	205	348	java/lang/Throwable
    //   208	218	348	java/lang/Throwable
    //   221	229	348	java/lang/Throwable
    //   232	241	348	java/lang/Throwable
    //   244	252	348	java/lang/Throwable
    //   255	261	348	java/lang/Throwable
    //   264	274	348	java/lang/Throwable
    //   277	285	348	java/lang/Throwable
    //   288	299	348	java/lang/Throwable
    //   307	326	348	java/lang/Throwable
    //   332	336	348	java/lang/Throwable
  }
  
  private final boolean a(ImTransportInfo paramImTransportInfo)
  {
    Object localObject = b(b);
    if (localObject != null)
    {
      long l1 = ((Long)localObject).longValue();
      localObject = k;
      if (localObject != null)
      {
        Collection localCollection = (Collection)new ArrayList(localObject.length);
        int i1 = localObject.length;
        int i2 = 0;
        while (i2 < i1)
        {
          paramImTransportInfo = localObject[i2];
          if (b == -1L) {
            paramImTransportInfo = new Reaction(a, l1, c, d, e, f);
          }
          localCollection.add(paramImTransportInfo);
          i2 += 1;
        }
        paramImTransportInfo = ((Collection)localCollection).toArray(new Reaction[0]);
        if (paramImTransportInfo != null)
        {
          paramImTransportInfo = (Reaction[])paramImTransportInfo;
          if (paramImTransportInfo == null) {
            return false;
          }
          paramImTransportInfo = (Boolean)((as)p.a()).a(paramImTransportInfo).d();
          if (paramImTransportInfo != null) {
            return paramImTransportInfo.booleanValue();
          }
          return false;
        }
        throw new c.u("null cannot be cast to non-null type kotlin.Array<T>");
      }
      return false;
    }
    return false;
  }
  
  private final boolean a(ImTransportInfo paramImTransportInfo, ContentValues paramContentValues)
  {
    AssertionUtil.AlwaysFatal.isNotNull(b, new String[0]);
    return a.update(TruecallerContract.w.a(), paramContentValues, "raw_id=?", new String[] { b }) > 0;
  }
  
  /* Error */
  private final boolean a(ImTransportInfo paramImTransportInfo, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 117	com/truecaller/messaging/transport/im/bd:a	Landroid/content/ContentResolver;
    //   4: astore 9
    //   6: invokestatic 649	com/truecaller/content/TruecallerContract$aa:a	()Landroid/net/Uri;
    //   9: astore 10
    //   11: aload_1
    //   12: getfield 592	com/truecaller/messaging/transport/im/ImTransportInfo:b	Ljava/lang/String;
    //   15: astore_1
    //   16: aload 9
    //   18: aload 10
    //   20: iconst_2
    //   21: anewarray 192	java/lang/String
    //   24: dup
    //   25: iconst_0
    //   26: ldc_w 651
    //   29: aastore
    //   30: dup
    //   31: iconst_1
    //   32: ldc_w 653
    //   35: aastore
    //   36: ldc_w 655
    //   39: iconst_1
    //   40: anewarray 192	java/lang/String
    //   43: dup
    //   44: iconst_0
    //   45: aload_1
    //   46: aastore
    //   47: aconst_null
    //   48: invokevirtual 484	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   51: astore_1
    //   52: aload_1
    //   53: ifnull +215 -> 268
    //   56: aload_1
    //   57: checkcast 486	java/io/Closeable
    //   60: astore 10
    //   62: aconst_null
    //   63: astore 9
    //   65: aload 9
    //   67: astore_1
    //   68: aload 10
    //   70: checkcast 488	android/database/Cursor
    //   73: astore 11
    //   75: aload 9
    //   77: astore_1
    //   78: aload 11
    //   80: invokeinterface 491 1 0
    //   85: ifeq +43 -> 128
    //   88: aload 9
    //   90: astore_1
    //   91: new 415	c/n
    //   94: dup
    //   95: aload 11
    //   97: iconst_0
    //   98: invokeinterface 499 2 0
    //   103: invokestatic 401	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   106: aload 11
    //   108: iconst_1
    //   109: invokeinterface 499 2 0
    //   114: invokestatic 401	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   117: invokespecial 418	c/n:<init>	(Ljava/lang/Object;Ljava/lang/Object;)V
    //   120: astore 9
    //   122: aload 9
    //   124: astore_1
    //   125: goto +5 -> 130
    //   128: aconst_null
    //   129: astore_1
    //   130: aload 10
    //   132: aconst_null
    //   133: invokestatic 524	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   136: aload_1
    //   137: ifnonnull +5 -> 142
    //   140: iconst_0
    //   141: ireturn
    //   142: aload_1
    //   143: getfield 439	c/n:a	Ljava/lang/Object;
    //   146: checkcast 450	java/lang/Number
    //   149: invokevirtual 656	java/lang/Number:longValue	()J
    //   152: lstore 5
    //   154: aload_1
    //   155: getfield 448	c/n:b	Ljava/lang/Object;
    //   158: checkcast 450	java/lang/Number
    //   161: invokevirtual 656	java/lang/Number:longValue	()J
    //   164: lstore 7
    //   166: aload_0
    //   167: getfield 117	com/truecaller/messaging/transport/im/bd:a	Landroid/content/ContentResolver;
    //   170: astore_1
    //   171: invokestatic 188	com/truecaller/content/TruecallerContract$w:a	()Landroid/net/Uri;
    //   174: astore 9
    //   176: new 658	java/lang/StringBuilder
    //   179: dup
    //   180: ldc_w 660
    //   183: invokespecial 661	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   186: astore 10
    //   188: aload 10
    //   190: aload_3
    //   191: invokevirtual 665	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   194: pop
    //   195: aload 10
    //   197: ldc_w 667
    //   200: invokevirtual 665	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   203: pop
    //   204: aload_1
    //   205: aload 9
    //   207: aload_2
    //   208: aload 10
    //   210: invokevirtual 668	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   213: aload 4
    //   215: iconst_2
    //   216: anewarray 192	java/lang/String
    //   219: dup
    //   220: iconst_0
    //   221: lload 5
    //   223: invokestatic 200	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   226: aastore
    //   227: dup
    //   228: iconst_1
    //   229: lload 7
    //   231: invokestatic 200	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   234: aastore
    //   235: invokestatic 673	c/a/f:a	([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;
    //   238: checkcast 675	[Ljava/lang/String;
    //   241: invokevirtual 206	android/content/ContentResolver:update	(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   244: ifle +5 -> 249
    //   247: iconst_1
    //   248: ireturn
    //   249: iconst_0
    //   250: ireturn
    //   251: astore_2
    //   252: goto +8 -> 260
    //   255: astore_2
    //   256: aload_2
    //   257: astore_1
    //   258: aload_2
    //   259: athrow
    //   260: aload 10
    //   262: aload_1
    //   263: invokestatic 524	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   266: aload_2
    //   267: athrow
    //   268: iconst_0
    //   269: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	270	0	this	bd
    //   0	270	1	paramImTransportInfo	ImTransportInfo
    //   0	270	2	paramContentValues	ContentValues
    //   0	270	3	paramString	String
    //   0	270	4	paramArrayOfString	String[]
    //   152	70	5	l1	long
    //   164	66	7	l2	long
    //   4	202	9	localObject1	Object
    //   9	252	10	localObject2	Object
    //   73	34	11	localCursor	android.database.Cursor
    // Exception table:
    //   from	to	target	type
    //   68	75	251	finally
    //   78	88	251	finally
    //   91	122	251	finally
    //   258	260	251	finally
    //   68	75	255	java/lang/Throwable
    //   78	88	255	java/lang/Throwable
    //   91	122	255	java/lang/Throwable
  }
  
  private final boolean a(Entity[] paramArrayOfEntity)
  {
    ArrayList localArrayList = new ArrayList();
    Collection localCollection = (Collection)new ArrayList(paramArrayOfEntity.length);
    int i2 = paramArrayOfEntity.length;
    int i1 = 0;
    while (i1 < i2)
    {
      Entity localEntity = paramArrayOfEntity[i1];
      ContentProviderOperation.Builder localBuilder = ContentProviderOperation.newUpdate(TruecallerContract.z.a());
      ContentValues localContentValues = new ContentValues();
      localEntity.a(localContentValues);
      localBuilder.withValues(localContentValues);
      localCollection.add(localBuilder.withSelection("_id=?", new String[] { String.valueOf(h) }).build());
      i1 += 1;
    }
    localArrayList.addAll((Collection)localCollection);
    paramArrayOfEntity = a(localArrayList);
    if (paramArrayOfEntity != null)
    {
      if (paramArrayOfEntity.length == 0) {
        i1 = 1;
      } else {
        i1 = 0;
      }
      if (i1 == 0) {
        return true;
      }
    }
    return false;
  }
  
  private final ContentProviderResult[] a(ArrayList<ContentProviderOperation> paramArrayList)
  {
    try
    {
      paramArrayList = a.applyBatch(TruecallerContract.a(), paramArrayList);
      return paramArrayList;
    }
    catch (RemoteException|OperationApplicationException paramArrayList)
    {
      for (;;) {}
    }
    return null;
  }
  
  /* Error */
  private final Uri b(BinaryEntity paramBinaryEntity)
    throws IOException
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 133	com/truecaller/messaging/transport/im/bd:i	Lcom/truecaller/messaging/data/providers/c;
    //   4: astore 4
    //   6: aload_1
    //   7: getfield 460	com/truecaller/messaging/data/types/BinaryEntity:b	Landroid/net/Uri;
    //   10: astore 5
    //   12: aload 5
    //   14: ldc_w 581
    //   17: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   20: aload 4
    //   22: aload 5
    //   24: invokeinterface 713 2 0
    //   29: ifeq +17 -> 46
    //   32: aload_1
    //   33: getfield 460	com/truecaller/messaging/data/types/BinaryEntity:b	Landroid/net/Uri;
    //   36: astore_1
    //   37: aload_1
    //   38: ldc_w 581
    //   41: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   44: aload_1
    //   45: areturn
    //   46: aload_0
    //   47: getfield 133	com/truecaller/messaging/transport/im/bd:i	Lcom/truecaller/messaging/data/providers/c;
    //   50: astore 4
    //   52: aload_1
    //   53: getfield 714	com/truecaller/messaging/data/types/BinaryEntity:h	J
    //   56: lstore_2
    //   57: aload_1
    //   58: getfield 715	com/truecaller/messaging/data/types/BinaryEntity:j	Ljava/lang/String;
    //   61: astore 5
    //   63: aload 5
    //   65: ldc_w 356
    //   68: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   71: aload 4
    //   73: lload_2
    //   74: aload 5
    //   76: invokeinterface 718 4 0
    //   81: astore 6
    //   83: aload 6
    //   85: ifnull +171 -> 256
    //   88: aload_0
    //   89: getfield 117	com/truecaller/messaging/transport/im/bd:a	Landroid/content/ContentResolver;
    //   92: aload_1
    //   93: getfield 460	com/truecaller/messaging/data/types/BinaryEntity:b	Landroid/net/Uri;
    //   96: invokevirtual 722	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   99: astore 5
    //   101: new 724	java/io/FileOutputStream
    //   104: dup
    //   105: aload 6
    //   107: invokespecial 727	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   110: checkcast 729	java/io/OutputStream
    //   113: astore 4
    //   115: aload 5
    //   117: ldc_w 731
    //   120: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   123: aload 5
    //   125: aload 4
    //   127: invokestatic 736	com/truecaller/utils/extensions/m:a	(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    //   130: pop2
    //   131: aload_0
    //   132: getfield 133	com/truecaller/messaging/transport/im/bd:i	Lcom/truecaller/messaging/data/providers/c;
    //   135: aload 6
    //   137: invokevirtual 739	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   140: invokestatic 742	c/a/m:a	(Ljava/lang/Object;)Ljava/util/List;
    //   143: invokeinterface 745 2 0
    //   148: aload_0
    //   149: getfield 133	com/truecaller/messaging/transport/im/bd:i	Lcom/truecaller/messaging/data/providers/c;
    //   152: astore 7
    //   154: aload_1
    //   155: getfield 715	com/truecaller/messaging/data/types/BinaryEntity:j	Ljava/lang/String;
    //   158: astore_1
    //   159: aload_1
    //   160: ldc_w 356
    //   163: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   166: aload 7
    //   168: aload 6
    //   170: aload_1
    //   171: iconst_1
    //   172: invokeinterface 748 4 0
    //   177: astore_1
    //   178: aload_1
    //   179: ifnull +21 -> 200
    //   182: aload 5
    //   184: checkcast 486	java/io/Closeable
    //   187: invokestatic 753	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   190: aload 4
    //   192: checkcast 486	java/io/Closeable
    //   195: invokestatic 753	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   198: aload_1
    //   199: areturn
    //   200: new 710	java/io/IOException
    //   203: dup
    //   204: invokespecial 754	java/io/IOException:<init>	()V
    //   207: checkcast 478	java/lang/Throwable
    //   210: athrow
    //   211: astore 6
    //   213: aload 4
    //   215: astore_1
    //   216: aload 6
    //   218: astore 4
    //   220: goto +18 -> 238
    //   223: astore 4
    //   225: aconst_null
    //   226: astore_1
    //   227: goto +11 -> 238
    //   230: astore 4
    //   232: aconst_null
    //   233: astore 5
    //   235: aload 5
    //   237: astore_1
    //   238: aload 5
    //   240: checkcast 486	java/io/Closeable
    //   243: invokestatic 753	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   246: aload_1
    //   247: checkcast 486	java/io/Closeable
    //   250: invokestatic 753	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   253: aload 4
    //   255: athrow
    //   256: new 710	java/io/IOException
    //   259: dup
    //   260: ldc_w 756
    //   263: invokespecial 757	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   266: checkcast 478	java/lang/Throwable
    //   269: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	270	0	this	bd
    //   0	270	1	paramBinaryEntity	BinaryEntity
    //   56	18	2	l1	long
    //   4	215	4	localObject1	Object
    //   223	1	4	localObject2	Object
    //   230	24	4	localObject3	Object
    //   10	229	5	localObject4	Object
    //   81	88	6	localFile	File
    //   211	6	6	localObject5	Object
    //   152	15	7	localc	com.truecaller.messaging.data.providers.c
    // Exception table:
    //   from	to	target	type
    //   115	178	211	finally
    //   200	211	211	finally
    //   101	115	223	finally
    //   88	101	230	finally
  }
  
  /* Error */
  private final Long b(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 117	com/truecaller/messaging/transport/im/bd:a	Landroid/content/ContentResolver;
    //   4: invokestatic 188	com/truecaller/content/TruecallerContract$w:a	()Landroid/net/Uri;
    //   7: iconst_1
    //   8: anewarray 192	java/lang/String
    //   11: dup
    //   12: iconst_0
    //   13: ldc_w 396
    //   16: aastore
    //   17: ldc_w 480
    //   20: iconst_1
    //   21: anewarray 192	java/lang/String
    //   24: dup
    //   25: iconst_0
    //   26: aload_1
    //   27: aastore
    //   28: aconst_null
    //   29: invokevirtual 484	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   32: astore_1
    //   33: aconst_null
    //   34: astore_2
    //   35: aload_1
    //   36: ifnull +72 -> 108
    //   39: aload_1
    //   40: checkcast 486	java/io/Closeable
    //   43: astore_3
    //   44: aload_2
    //   45: astore_1
    //   46: aload_3
    //   47: checkcast 488	android/database/Cursor
    //   50: astore 4
    //   52: aload_2
    //   53: astore_1
    //   54: aload 4
    //   56: invokeinterface 491 1 0
    //   61: ifeq +22 -> 83
    //   64: aload_2
    //   65: astore_1
    //   66: aload 4
    //   68: iconst_0
    //   69: invokeinterface 499 2 0
    //   74: invokestatic 401	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   77: astore_2
    //   78: aload_2
    //   79: astore_1
    //   80: goto +5 -> 85
    //   83: aconst_null
    //   84: astore_1
    //   85: aload_3
    //   86: aconst_null
    //   87: invokestatic 524	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   90: aload_1
    //   91: areturn
    //   92: astore_2
    //   93: goto +8 -> 101
    //   96: astore_2
    //   97: aload_2
    //   98: astore_1
    //   99: aload_2
    //   100: athrow
    //   101: aload_3
    //   102: aload_1
    //   103: invokestatic 524	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   106: aload_2
    //   107: athrow
    //   108: aconst_null
    //   109: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	110	0	this	bd
    //   0	110	1	paramString	String
    //   34	45	2	localLong	Long
    //   92	1	2	localObject	Object
    //   96	11	2	localThrowable	Throwable
    //   43	59	3	localCloseable	java.io.Closeable
    //   50	17	4	localCursor	android.database.Cursor
    // Exception table:
    //   from	to	target	type
    //   46	52	92	finally
    //   54	64	92	finally
    //   66	78	92	finally
    //   99	101	92	finally
    //   46	52	96	java/lang/Throwable
    //   54	64	96	java/lang/Throwable
    //   66	78	96	java/lang/Throwable
  }
  
  private final boolean b(Entity[] paramArrayOfEntity)
  {
    ArrayList localArrayList = new ArrayList();
    Collection localCollection = (Collection)new ArrayList(paramArrayOfEntity.length);
    int i2 = paramArrayOfEntity.length;
    int i1 = 0;
    while (i1 < i2)
    {
      Entity localEntity = paramArrayOfEntity[i1];
      localCollection.add(ContentProviderOperation.newUpdate(TruecallerContract.z.a()).withValue("status", Integer.valueOf(i)).withSelection("_id=?", new String[] { String.valueOf(h) }).build());
      i1 += 1;
    }
    localArrayList.addAll((Collection)localCollection);
    paramArrayOfEntity = a(localArrayList);
    if (paramArrayOfEntity != null)
    {
      if (paramArrayOfEntity.length == 0) {
        i1 = 1;
      } else {
        i1 = 0;
      }
      if (i1 == 0) {
        return true;
      }
    }
    return false;
  }
  
  /* Error */
  private final String c(long paramLong)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 117	com/truecaller/messaging/transport/im/bd:a	Landroid/content/ContentResolver;
    //   4: invokestatic 188	com/truecaller/content/TruecallerContract$w:a	()Landroid/net/Uri;
    //   7: iconst_1
    //   8: anewarray 192	java/lang/String
    //   11: dup
    //   12: iconst_0
    //   13: ldc_w 764
    //   16: aastore
    //   17: ldc -66
    //   19: iconst_1
    //   20: anewarray 192	java/lang/String
    //   23: dup
    //   24: iconst_0
    //   25: lload_1
    //   26: invokestatic 200	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   29: aastore
    //   30: aconst_null
    //   31: invokevirtual 484	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   34: astore_3
    //   35: aconst_null
    //   36: astore 4
    //   38: aload_3
    //   39: ifnull +83 -> 122
    //   42: aload_3
    //   43: checkcast 486	java/io/Closeable
    //   46: astore 5
    //   48: aload 4
    //   50: astore_3
    //   51: aload 5
    //   53: checkcast 488	android/database/Cursor
    //   56: astore 6
    //   58: aload 4
    //   60: astore_3
    //   61: aload 6
    //   63: invokeinterface 491 1 0
    //   68: ifeq +22 -> 90
    //   71: aload 4
    //   73: astore_3
    //   74: aload 6
    //   76: iconst_0
    //   77: invokeinterface 768 2 0
    //   82: astore 4
    //   84: aload 4
    //   86: astore_3
    //   87: goto +5 -> 92
    //   90: aconst_null
    //   91: astore_3
    //   92: aload 5
    //   94: aconst_null
    //   95: invokestatic 524	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   98: aload_3
    //   99: areturn
    //   100: astore 4
    //   102: goto +11 -> 113
    //   105: astore 4
    //   107: aload 4
    //   109: astore_3
    //   110: aload 4
    //   112: athrow
    //   113: aload 5
    //   115: aload_3
    //   116: invokestatic 524	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   119: aload 4
    //   121: athrow
    //   122: aconst_null
    //   123: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	124	0	this	bd
    //   0	124	1	paramLong	long
    //   34	82	3	localObject1	Object
    //   36	49	4	str	String
    //   100	1	4	localObject2	Object
    //   105	15	4	localThrowable	Throwable
    //   46	68	5	localCloseable	java.io.Closeable
    //   56	19	6	localCursor	android.database.Cursor
    // Exception table:
    //   from	to	target	type
    //   51	58	100	finally
    //   61	71	100	finally
    //   74	84	100	finally
    //   110	113	100	finally
    //   51	58	105	java/lang/Throwable
    //   61	71	105	java/lang/Throwable
    //   74	84	105	java/lang/Throwable
  }
  
  /* Error */
  private final boolean c(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 117	com/truecaller/messaging/transport/im/bd:a	Landroid/content/ContentResolver;
    //   4: invokestatic 188	com/truecaller/content/TruecallerContract$w:a	()Landroid/net/Uri;
    //   7: iconst_1
    //   8: anewarray 192	java/lang/String
    //   11: dup
    //   12: iconst_0
    //   13: ldc_w 396
    //   16: aastore
    //   17: ldc_w 771
    //   20: iconst_1
    //   21: anewarray 192	java/lang/String
    //   24: dup
    //   25: iconst_0
    //   26: aload_1
    //   27: aastore
    //   28: aconst_null
    //   29: invokevirtual 484	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   32: astore_1
    //   33: aload_1
    //   34: ifnull +47 -> 81
    //   37: aload_1
    //   38: checkcast 486	java/io/Closeable
    //   41: astore 4
    //   43: aconst_null
    //   44: astore_1
    //   45: aload 4
    //   47: checkcast 488	android/database/Cursor
    //   50: invokeinterface 491 1 0
    //   55: istore_2
    //   56: aload 4
    //   58: aconst_null
    //   59: invokestatic 524	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   62: iload_2
    //   63: ireturn
    //   64: astore_3
    //   65: goto +8 -> 73
    //   68: astore_3
    //   69: aload_3
    //   70: astore_1
    //   71: aload_3
    //   72: athrow
    //   73: aload 4
    //   75: aload_1
    //   76: invokestatic 524	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   79: aload_3
    //   80: athrow
    //   81: iconst_0
    //   82: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	83	0	this	bd
    //   0	83	1	paramString	String
    //   55	8	2	bool	boolean
    //   64	1	3	localObject	Object
    //   68	12	3	localThrowable	Throwable
    //   41	33	4	localCloseable	java.io.Closeable
    // Exception table:
    //   from	to	target	type
    //   45	56	64	finally
    //   71	73	64	finally
    //   45	56	68	java/lang/Throwable
  }
  
  private final boolean c(Entity[] paramArrayOfEntity)
  {
    ArrayList localArrayList = new ArrayList();
    int i2 = paramArrayOfEntity.length;
    int i1 = 0;
    while (i1 < i2)
    {
      Entity localEntity = paramArrayOfEntity[i1];
      localArrayList.add(ContentProviderOperation.newUpdate(TruecallerContract.z.a()).withValue("status", Integer.valueOf(1)).withSelection("_id=? AND status IN (2, 4)", new String[] { String.valueOf(h) }).build());
      i1 += 1;
    }
    paramArrayOfEntity = a(localArrayList);
    if ((paramArrayOfEntity != null) && (paramArrayOfEntity.length == 0)) {
      return false;
    }
    ((q)h.a()).a();
    return true;
  }
  
  /* Error */
  private final String d(long paramLong)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 117	com/truecaller/messaging/transport/im/bd:a	Landroid/content/ContentResolver;
    //   4: invokestatic 780	com/truecaller/content/TruecallerContract$i:a	()Landroid/net/Uri;
    //   7: iconst_1
    //   8: anewarray 192	java/lang/String
    //   11: dup
    //   12: iconst_0
    //   13: ldc_w 782
    //   16: aastore
    //   17: ldc_w 681
    //   20: iconst_1
    //   21: anewarray 192	java/lang/String
    //   24: dup
    //   25: iconst_0
    //   26: lload_1
    //   27: invokestatic 200	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   30: aastore
    //   31: aconst_null
    //   32: invokevirtual 484	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   35: astore_3
    //   36: aconst_null
    //   37: astore 4
    //   39: aload_3
    //   40: ifnull +83 -> 123
    //   43: aload_3
    //   44: checkcast 486	java/io/Closeable
    //   47: astore 5
    //   49: aload 4
    //   51: astore_3
    //   52: aload 5
    //   54: checkcast 488	android/database/Cursor
    //   57: astore 6
    //   59: aload 4
    //   61: astore_3
    //   62: aload 6
    //   64: invokeinterface 491 1 0
    //   69: ifeq +22 -> 91
    //   72: aload 4
    //   74: astore_3
    //   75: aload 6
    //   77: iconst_0
    //   78: invokeinterface 768 2 0
    //   83: astore 4
    //   85: aload 4
    //   87: astore_3
    //   88: goto +5 -> 93
    //   91: aconst_null
    //   92: astore_3
    //   93: aload 5
    //   95: aconst_null
    //   96: invokestatic 524	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   99: aload_3
    //   100: areturn
    //   101: astore 4
    //   103: goto +11 -> 114
    //   106: astore 4
    //   108: aload 4
    //   110: astore_3
    //   111: aload 4
    //   113: athrow
    //   114: aload 5
    //   116: aload_3
    //   117: invokestatic 524	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   120: aload 4
    //   122: athrow
    //   123: aconst_null
    //   124: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	125	0	this	bd
    //   0	125	1	paramLong	long
    //   35	82	3	localObject1	Object
    //   37	49	4	str	String
    //   101	1	4	localObject2	Object
    //   106	15	4	localThrowable	Throwable
    //   47	68	5	localCloseable	java.io.Closeable
    //   57	19	6	localCursor	android.database.Cursor
    // Exception table:
    //   from	to	target	type
    //   52	59	101	finally
    //   62	72	101	finally
    //   75	85	101	finally
    //   111	114	101	finally
    //   52	59	106	java/lang/Throwable
    //   62	72	106	java/lang/Throwable
    //   75	85	106	java/lang/Throwable
  }
  
  private final boolean g()
  {
    String str = f.B();
    int i1 = str.hashCode();
    if (i1 != -244809062)
    {
      if (i1 != 3649301) {
        return false;
      }
      if (str.equals("wifi")) {
        return (j.a()) && (!j.c());
      }
    }
    else if (str.equals("wifiOrMobile"))
    {
      return j.a();
    }
    return false;
  }
  
  /* Error */
  private final boolean g(Message paramMessage)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual 810	com/truecaller/messaging/data/types/Message:k	()Lcom/truecaller/messaging/data/types/TransportInfo;
    //   4: astore 6
    //   6: aload 6
    //   8: ldc_w 812
    //   11: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   14: aload 6
    //   16: checkcast 194	com/truecaller/messaging/transport/im/ImTransportInfo
    //   19: astore 9
    //   21: aload_0
    //   22: getfield 117	com/truecaller/messaging/transport/im/bd:a	Landroid/content/ContentResolver;
    //   25: astore 6
    //   27: invokestatic 188	com/truecaller/content/TruecallerContract$w:a	()Landroid/net/Uri;
    //   30: astore 7
    //   32: aload 9
    //   34: getfield 592	com/truecaller/messaging/transport/im/ImTransportInfo:b	Ljava/lang/String;
    //   37: astore 8
    //   39: aload 6
    //   41: aload 7
    //   43: iconst_2
    //   44: anewarray 192	java/lang/String
    //   47: dup
    //   48: iconst_0
    //   49: ldc_w 396
    //   52: aastore
    //   53: dup
    //   54: iconst_1
    //   55: ldc_w 814
    //   58: aastore
    //   59: ldc_w 480
    //   62: iconst_1
    //   63: anewarray 192	java/lang/String
    //   66: dup
    //   67: iconst_0
    //   68: aload 8
    //   70: aastore
    //   71: aconst_null
    //   72: invokevirtual 484	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   75: astore 6
    //   77: aload 6
    //   79: ifnull +582 -> 661
    //   82: aload 6
    //   84: checkcast 486	java/io/Closeable
    //   87: astore 8
    //   89: aconst_null
    //   90: astore 7
    //   92: aload 7
    //   94: astore 6
    //   96: aload 8
    //   98: checkcast 488	android/database/Cursor
    //   101: astore 10
    //   103: aload 7
    //   105: astore 6
    //   107: aload 10
    //   109: invokeinterface 491 1 0
    //   114: ifeq +512 -> 626
    //   117: aload 7
    //   119: astore 6
    //   121: aload 10
    //   123: aload 10
    //   125: ldc_w 396
    //   128: invokeinterface 495 2 0
    //   133: invokeinterface 499 2 0
    //   138: lstore 4
    //   140: aload 7
    //   142: astore 6
    //   144: aload 10
    //   146: aload 10
    //   148: ldc_w 814
    //   151: invokeinterface 495 2 0
    //   156: invokeinterface 499 2 0
    //   161: lconst_0
    //   162: lcmp
    //   163: ifle +504 -> 667
    //   166: iconst_1
    //   167: istore_2
    //   168: goto +3 -> 171
    //   171: aload 7
    //   173: astore 6
    //   175: new 329	java/util/ArrayList
    //   178: dup
    //   179: invokespecial 424	java/util/ArrayList:<init>	()V
    //   182: astore 10
    //   184: iload_2
    //   185: ifeq +130 -> 315
    //   188: aload 7
    //   190: astore 6
    //   192: aload 10
    //   194: invokestatic 382	com/truecaller/content/TruecallerContract$z:a	()Landroid/net/Uri;
    //   197: invokestatic 817	android/content/ContentProviderOperation:newDelete	(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;
    //   200: ldc -66
    //   202: iconst_1
    //   203: anewarray 192	java/lang/String
    //   206: dup
    //   207: iconst_0
    //   208: lload 4
    //   210: invokestatic 200	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   213: aastore
    //   214: invokevirtual 687	android/content/ContentProviderOperation$Builder:withSelection	(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;
    //   217: invokevirtual 409	android/content/ContentProviderOperation$Builder:build	()Landroid/content/ContentProviderOperation;
    //   220: invokevirtual 413	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   223: pop
    //   224: aload 7
    //   226: astore 6
    //   228: aload_1
    //   229: getfield 820	com/truecaller/messaging/data/types/Message:n	[Lcom/truecaller/messaging/data/types/Entity;
    //   232: astore 11
    //   234: aload 7
    //   236: astore 6
    //   238: aload 11
    //   240: ldc_w 822
    //   243: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   246: aload 7
    //   248: astore 6
    //   250: aload_0
    //   251: aload 10
    //   253: lload 4
    //   255: aload 11
    //   257: invokespecial 824	com/truecaller/messaging/transport/im/bd:a	(Ljava/util/ArrayList;J[Lcom/truecaller/messaging/data/types/Entity;)V
    //   260: aload 7
    //   262: astore 6
    //   264: aload 10
    //   266: invokestatic 188	com/truecaller/content/TruecallerContract$w:a	()Landroid/net/Uri;
    //   269: invokestatic 679	android/content/ContentProviderOperation:newUpdate	(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;
    //   272: ldc -66
    //   274: iconst_1
    //   275: anewarray 192	java/lang/String
    //   278: dup
    //   279: iconst_0
    //   280: aload_1
    //   281: invokevirtual 826	com/truecaller/messaging/data/types/Message:a	()J
    //   284: invokestatic 200	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   287: aastore
    //   288: invokevirtual 687	android/content/ContentProviderOperation$Builder:withSelection	(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;
    //   291: ldc_w 814
    //   294: aload 9
    //   296: getfield 827	com/truecaller/messaging/transport/im/ImTransportInfo:i	I
    //   299: invokestatic 179	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   302: invokevirtual 405	android/content/ContentProviderOperation$Builder:withValue	(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;
    //   305: invokevirtual 409	android/content/ContentProviderOperation$Builder:build	()Landroid/content/ContentProviderOperation;
    //   308: invokevirtual 413	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   311: pop
    //   312: goto +88 -> 400
    //   315: aload 7
    //   317: astore 6
    //   319: aload_0
    //   320: invokespecial 829	com/truecaller/messaging/transport/im/bd:g	()Z
    //   323: ifeq +349 -> 672
    //   326: aload 7
    //   328: astore 6
    //   330: aload_0
    //   331: getfield 133	com/truecaller/messaging/transport/im/bd:i	Lcom/truecaller/messaging/data/providers/c;
    //   334: invokeinterface 830 1 0
    //   339: ifeq +333 -> 672
    //   342: iconst_1
    //   343: istore_3
    //   344: goto +3 -> 347
    //   347: aload 7
    //   349: astore 6
    //   351: aload 10
    //   353: invokestatic 382	com/truecaller/content/TruecallerContract$z:a	()Landroid/net/Uri;
    //   356: invokestatic 679	android/content/ContentProviderOperation:newUpdate	(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;
    //   359: ldc_w 832
    //   362: iconst_2
    //   363: anewarray 192	java/lang/String
    //   366: dup
    //   367: iconst_0
    //   368: lload 4
    //   370: invokestatic 200	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   373: aastore
    //   374: dup
    //   375: iconst_1
    //   376: ldc_w 834
    //   379: aastore
    //   380: invokevirtual 687	android/content/ContentProviderOperation$Builder:withSelection	(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;
    //   383: ldc_w 760
    //   386: iload_3
    //   387: invokestatic 179	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   390: invokevirtual 405	android/content/ContentProviderOperation$Builder:withValue	(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;
    //   393: invokevirtual 409	android/content/ContentProviderOperation$Builder:build	()Landroid/content/ContentProviderOperation;
    //   396: invokevirtual 413	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   399: pop
    //   400: aload 7
    //   402: astore 6
    //   404: aload_0
    //   405: aload 10
    //   407: invokespecial 694	com/truecaller/messaging/transport/im/bd:a	(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    //   410: astore_1
    //   411: aload_1
    //   412: ifnull +214 -> 626
    //   415: aload 7
    //   417: astore 6
    //   419: aload_1
    //   420: arraylength
    //   421: istore_3
    //   422: iload_3
    //   423: ifne +8 -> 431
    //   426: iconst_1
    //   427: istore_3
    //   428: goto +5 -> 433
    //   431: iconst_0
    //   432: istore_3
    //   433: iload_3
    //   434: iconst_1
    //   435: ixor
    //   436: iconst_1
    //   437: if_icmpne +189 -> 626
    //   440: iload_2
    //   441: ifeq +156 -> 597
    //   444: aload 7
    //   446: astore 6
    //   448: aload_0
    //   449: getfield 133	com/truecaller/messaging/transport/im/bd:i	Lcom/truecaller/messaging/data/providers/c;
    //   452: lload 4
    //   454: invokeinterface 561 3 0
    //   459: astore 9
    //   461: aload 7
    //   463: astore 6
    //   465: new 329	java/util/ArrayList
    //   468: dup
    //   469: invokespecial 424	java/util/ArrayList:<init>	()V
    //   472: checkcast 334	java/util/Collection
    //   475: astore_1
    //   476: aload 7
    //   478: astore 6
    //   480: aload 9
    //   482: invokeinterface 428 1 0
    //   487: astore 9
    //   489: aload 7
    //   491: astore 6
    //   493: aload 9
    //   495: invokeinterface 433 1 0
    //   500: ifeq +47 -> 547
    //   503: aload 7
    //   505: astore 6
    //   507: aload 9
    //   509: invokeinterface 436 1 0
    //   514: astore 10
    //   516: aload 7
    //   518: astore 6
    //   520: aload 10
    //   522: checkcast 586	java/io/File
    //   525: invokevirtual 837	java/io/File:exists	()Z
    //   528: ifeq -39 -> 489
    //   531: aload 7
    //   533: astore 6
    //   535: aload_1
    //   536: aload 10
    //   538: invokeinterface 419 2 0
    //   543: pop
    //   544: goto -55 -> 489
    //   547: aload 7
    //   549: astore 6
    //   551: aload_1
    //   552: checkcast 421	java/util/List
    //   555: checkcast 423	java/lang/Iterable
    //   558: invokeinterface 428 1 0
    //   563: astore_1
    //   564: aload 7
    //   566: astore 6
    //   568: aload_1
    //   569: invokeinterface 433 1 0
    //   574: ifeq +23 -> 597
    //   577: aload 7
    //   579: astore 6
    //   581: aload_1
    //   582: invokeinterface 436 1 0
    //   587: checkcast 586	java/io/File
    //   590: invokevirtual 840	java/io/File:delete	()Z
    //   593: pop
    //   594: goto -30 -> 564
    //   597: aload 7
    //   599: astore 6
    //   601: aload_0
    //   602: getfield 131	com/truecaller/messaging/transport/im/bd:h	Lcom/truecaller/androidactors/f;
    //   605: invokeinterface 256 1 0
    //   610: checkcast 775	com/truecaller/messaging/transport/im/q
    //   613: invokeinterface 777 1 0
    //   618: aload 8
    //   620: aconst_null
    //   621: invokestatic 524	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   624: iconst_1
    //   625: ireturn
    //   626: aload 7
    //   628: astore 6
    //   630: getstatic 529	c/x:a	Lc/x;
    //   633: astore_1
    //   634: aload 8
    //   636: aconst_null
    //   637: invokestatic 524	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   640: iconst_0
    //   641: ireturn
    //   642: astore_1
    //   643: goto +9 -> 652
    //   646: astore_1
    //   647: aload_1
    //   648: astore 6
    //   650: aload_1
    //   651: athrow
    //   652: aload 8
    //   654: aload 6
    //   656: invokestatic 524	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   659: aload_1
    //   660: athrow
    //   661: iconst_0
    //   662: ireturn
    //   663: astore_1
    //   664: goto -67 -> 597
    //   667: iconst_0
    //   668: istore_2
    //   669: goto -498 -> 171
    //   672: iconst_4
    //   673: istore_3
    //   674: goto -327 -> 347
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	677	0	this	bd
    //   0	677	1	paramMessage	Message
    //   167	502	2	i1	int
    //   343	331	3	i2	int
    //   138	315	4	l1	long
    //   4	651	6	localObject1	Object
    //   30	597	7	localUri	Uri
    //   37	616	8	localObject2	Object
    //   19	489	9	localObject3	Object
    //   101	436	10	localObject4	Object
    //   232	24	11	arrayOfEntity	Entity[]
    // Exception table:
    //   from	to	target	type
    //   96	103	642	finally
    //   107	117	642	finally
    //   121	140	642	finally
    //   144	166	642	finally
    //   175	184	642	finally
    //   192	224	642	finally
    //   228	234	642	finally
    //   238	246	642	finally
    //   250	260	642	finally
    //   264	312	642	finally
    //   319	326	642	finally
    //   330	342	642	finally
    //   351	400	642	finally
    //   404	411	642	finally
    //   419	422	642	finally
    //   448	461	642	finally
    //   465	476	642	finally
    //   480	489	642	finally
    //   493	503	642	finally
    //   507	516	642	finally
    //   520	531	642	finally
    //   535	544	642	finally
    //   551	564	642	finally
    //   568	577	642	finally
    //   581	594	642	finally
    //   601	618	642	finally
    //   630	634	642	finally
    //   650	652	642	finally
    //   96	103	646	java/lang/Throwable
    //   107	117	646	java/lang/Throwable
    //   121	140	646	java/lang/Throwable
    //   144	166	646	java/lang/Throwable
    //   175	184	646	java/lang/Throwable
    //   192	224	646	java/lang/Throwable
    //   228	234	646	java/lang/Throwable
    //   238	246	646	java/lang/Throwable
    //   250	260	646	java/lang/Throwable
    //   264	312	646	java/lang/Throwable
    //   319	326	646	java/lang/Throwable
    //   330	342	646	java/lang/Throwable
    //   351	400	646	java/lang/Throwable
    //   404	411	646	java/lang/Throwable
    //   419	422	646	java/lang/Throwable
    //   448	461	646	java/lang/Throwable
    //   465	476	646	java/lang/Throwable
    //   480	489	646	java/lang/Throwable
    //   493	503	646	java/lang/Throwable
    //   507	516	646	java/lang/Throwable
    //   520	531	646	java/lang/Throwable
    //   535	544	646	java/lang/Throwable
    //   551	564	646	java/lang/Throwable
    //   568	577	646	java/lang/Throwable
    //   581	594	646	java/lang/Throwable
    //   601	618	646	java/lang/Throwable
    //   630	634	646	java/lang/Throwable
    //   448	461	663	java/lang/SecurityException
    //   465	476	663	java/lang/SecurityException
    //   480	489	663	java/lang/SecurityException
    //   493	503	663	java/lang/SecurityException
    //   507	516	663	java/lang/SecurityException
    //   520	531	663	java/lang/SecurityException
    //   535	544	663	java/lang/SecurityException
    //   551	564	663	java/lang/SecurityException
    //   568	577	663	java/lang/SecurityException
    //   581	594	663	java/lang/SecurityException
  }
  
  public final long a(long paramLong)
  {
    return paramLong;
  }
  
  public final long a(com.truecaller.messaging.transport.f paramf, com.truecaller.messaging.transport.i parami, r paramr, org.a.a.b paramb1, org.a.a.b paramb2, int paramInt, List<ContentProviderOperation> paramList, com.truecaller.utils.q paramq, boolean paramBoolean1, boolean paramBoolean2, Set<Long> paramSet)
  {
    c.g.b.k.b(paramf, "threadInfoCache");
    c.g.b.k.b(parami, "participantCache");
    c.g.b.k.b(paramr, "cursor");
    c.g.b.k.b(paramb1, "timeTo");
    c.g.b.k.b(paramb2, "timeFrom");
    c.g.b.k.b(paramList, "operations");
    c.g.b.k.b(paramq, "trace");
    c.g.b.k.b(paramSet, "messagesToClassify");
    return g.a(paramf, parami, paramr, paramb1, paramb2, paramInt, paramList, paramq, paramBoolean1, paramBoolean2, paramSet);
  }
  
  public final l.a a(Message paramMessage, Participant[] paramArrayOfParticipant)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramArrayOfParticipant, "recipients");
    paramArrayOfParticipant = new ArrayList();
    for (;;)
    {
      int i1;
      try
      {
        Object localObject1 = n;
        c.g.b.k.a(localObject1, "message.entities");
        Object localObject2 = (Collection)new ArrayList();
        int i2 = localObject1.length;
        i1 = 0;
        Object localObject3;
        if (i1 < i2)
        {
          localObject3 = localObject1[i1];
          if ((true ^ Entity.a(j))) {
            ((Collection)localObject2).add(localObject3);
          }
        }
        else
        {
          localObject2 = (Iterable)localObject2;
          localObject1 = (Collection)new ArrayList(m.a((Iterable)localObject2, 10));
          localObject2 = ((Iterable)localObject2).iterator();
          if (((Iterator)localObject2).hasNext())
          {
            localObject3 = (Entity)((Iterator)localObject2).next();
            ContentProviderOperation.Builder localBuilder = ContentProviderOperation.newUpdate(TruecallerContract.z.a());
            if (localObject3 != null)
            {
              localBuilder.withValue("content", b((BinaryEntity)localObject3).toString());
              localBuilder.withValue("status", Integer.valueOf(0));
              localBuilder.withSelection("_id=?", new String[] { String.valueOf(h) });
              ((Collection)localObject1).add(localBuilder.build());
              continue;
            }
            throw new c.u("null cannot be cast to non-null type com.truecaller.messaging.data.types.BinaryEntity");
          }
          paramArrayOfParticipant.addAll((Collection)localObject1);
          if ((((Collection)paramArrayOfParticipant).isEmpty() ^ true))
          {
            paramArrayOfParticipant = a(paramArrayOfParticipant);
            if (paramArrayOfParticipant != null)
            {
              if (paramArrayOfParticipant.length == 0) {
                i1 = 1;
              } else {
                i1 = 0;
              }
              if ((i1 ^ 0x1) == 1) {}
            }
            else
            {
              return new l.a(0);
            }
          }
          paramArrayOfParticipant = n;
          c.g.b.k.a(paramArrayOfParticipant, "message.entities");
          localObject1 = (Collection)new ArrayList();
          int i3 = paramArrayOfParticipant.length;
          i1 = 0;
          if (i1 < i3)
          {
            localObject2 = paramArrayOfParticipant[i1];
            if (!Entity.a(j)) {
              if (localObject2 != null)
              {
                localObject3 = b;
                c.g.b.k.a(localObject3, "(it as BinaryEntity).content");
                if (c.g.b.k.a(((Uri)localObject3).getScheme(), "file"))
                {
                  i2 = 1;
                  continue;
                }
              }
              else
              {
                throw new c.u("null cannot be cast to non-null type com.truecaller.messaging.data.types.BinaryEntity");
              }
            }
            i2 = 0;
            if (i2 != 0) {
              ((Collection)localObject1).add(localObject2);
            }
            i1 += 1;
            continue;
          }
          localObject1 = (Iterable)localObject1;
          paramArrayOfParticipant = (Collection)new ArrayList(m.a((Iterable)localObject1, 10));
          localObject1 = ((Iterable)localObject1).iterator();
          if (((Iterator)localObject1).hasNext())
          {
            localObject2 = (Entity)((Iterator)localObject1).next();
            if (localObject2 != null)
            {
              localObject2 = b;
              c.g.b.k.a(localObject2, "(it as BinaryEntity).content");
              paramArrayOfParticipant.add(new File(((Uri)localObject2).getPath()));
              continue;
            }
            throw new c.u("null cannot be cast to non-null type com.truecaller.messaging.data.types.BinaryEntity");
          }
          localObject1 = (Iterable)paramArrayOfParticipant;
          paramArrayOfParticipant = (Collection)new ArrayList();
          localObject1 = ((Iterable)localObject1).iterator();
          if (((Iterator)localObject1).hasNext())
          {
            localObject2 = ((Iterator)localObject1).next();
            if (!((File)localObject2).exists()) {
              continue;
            }
            paramArrayOfParticipant.add(localObject2);
            continue;
          }
          paramArrayOfParticipant = ((Iterable)paramArrayOfParticipant).iterator();
          if (paramArrayOfParticipant.hasNext())
          {
            ((File)paramArrayOfParticipant.next()).delete();
            continue;
          }
          if (j == 3)
          {
            paramArrayOfParticipant = new ImTransportInfo.a();
            a = paramMessage.a();
            j = new Random().nextLong();
            b = bf.a(paramMessage);
            paramMessage = paramArrayOfParticipant.a();
          }
          else
          {
            paramMessage = paramMessage.k();
            c.g.b.k.a(paramMessage, "message.getTransportInfo()");
            paramMessage = (ImTransportInfo)paramMessage;
          }
          return new l.a((TransportInfo)paramMessage);
        }
      }
      catch (IOException paramMessage)
      {
        AssertionUtil.reportThrowableButNeverCrash((Throwable)paramMessage);
        return new l.a(0);
      }
      i1 += 1;
    }
  }
  
  public final String a()
  {
    return "im";
  }
  
  public final String a(String paramString)
  {
    c.g.b.k.b(paramString, "simToken");
    return "-1";
  }
  
  public final void a(Intent paramIntent, int paramInt)
  {
    c.g.b.k.b(paramIntent, "intent");
    Object localObject1 = paramIntent.getAction();
    if (localObject1 == null) {
      return;
    }
    paramInt = ((String)localObject1).hashCode();
    int i2 = 4;
    boolean bool2 = true;
    int i3 = 0;
    long l1;
    boolean bool1;
    switch (paramInt)
    {
    default: 
      return;
    case 1917307608: 
      if (((String)localObject1).equals("update_entity_status"))
      {
        l1 = paramIntent.getLongExtra("entity_id", -1L);
        paramInt = paramIntent.getIntExtra("entity_status", -1);
        if (l1 == -1L) {
          bool1 = true;
        } else {
          bool1 = false;
        }
        AssertionUtil.AlwaysFatal.isFalse(bool1, new String[0]);
        if (paramInt == -1) {
          bool1 = bool2;
        } else {
          bool1 = false;
        }
        AssertionUtil.AlwaysFatal.isFalse(bool1, new String[0]);
        paramIntent = new Message.a().a(Participant.a).a(Entity.a(l1, "image/png", paramInt, "", -1, -1, -1, -1L, ""));
        localObject1 = new ImTransportInfo.a();
        g = 7;
        paramIntent = paramIntent.a(2, (TransportInfo)((ImTransportInfo.a)localObject1).a()).b();
        c.g.b.k.a(paramIntent, "Message.Builder()\n      …   )\n            .build()");
        ((t)d.a()).b(paramIntent);
        return;
      }
      break;
    case 1624585217: 
      if (((String)localObject1).equals("update_delivery_sync_status"))
      {
        a(4, paramIntent);
        return;
      }
      break;
    case 1239505315: 
      if (((String)localObject1).equals("update_read_sync_status"))
      {
        a(5, paramIntent);
        return;
      }
      break;
    case 96891546: 
      if (((String)localObject1).equals("event"))
      {
        localObject1 = paramIntent.getByteArrayExtra("event");
        if (localObject1 == null) {
          return;
        }
      }
      break;
    }
    for (;;)
    {
      Object localObject2;
      Object localObject3;
      try
      {
        localObject1 = Event.a((byte[])localObject1);
        bool2 = paramIntent.getBooleanExtra("from_push", false);
        c.g.b.k.a(localObject1, "event");
        paramIntent = ((Event)localObject1).a();
        if (paramIntent == null) {
          return;
        }
        switch (be.a[paramIntent.ordinal()])
        {
        default: 
          return;
        case 8: 
          paramIntent = (com.truecaller.messaging.transport.im.a.a)x.a();
          localObject1 = ((Event)localObject1).j();
          c.g.b.k.a(localObject1, "event.groupInfoUpdated");
          paramIntent.a((Event.f)localObject1);
          return;
        case 7: 
          paramIntent = (com.truecaller.messaging.transport.im.a.a)x.a();
          localObject1 = ((Event)localObject1).i();
          c.g.b.k.a(localObject1, "event.rolesUpdated");
          paramIntent.a((Event.x)localObject1);
          return;
        case 6: 
          paramIntent = (com.truecaller.messaging.transport.im.a.a)x.a();
          localObject1 = ((Event)localObject1).h();
          c.g.b.k.a(localObject1, "event.participantRemoved");
          paramIntent.a((Event.p)localObject1);
          return;
        case 5: 
          paramIntent = (com.truecaller.messaging.transport.im.a.a)x.a();
          localObject1 = ((Event)localObject1).g();
          c.g.b.k.a(localObject1, "event.participantAdded");
          paramIntent.a((Event.n)localObject1);
          return;
        case 4: 
          paramIntent = (com.truecaller.messaging.transport.im.a.a)x.a();
          localObject1 = ((Event)localObject1).f();
          c.g.b.k.a(localObject1, "event.groupCreated");
          paramIntent.a((Event.d)localObject1);
          return;
        case 3: 
          localObject2 = ((Event)localObject1).e();
          c.g.b.k.a(localObject2, "event.reactionSent");
          if (!r.F().a()) {
            return;
          }
          paramIntent = ((Event.t)localObject2).d();
          c.g.b.k.a(paramIntent, "reactionSent.content");
          paramIntent = paramIntent.b();
          c.g.b.k.a(paramIntent, "reactionSent.content.replyTo");
          localObject1 = ((Event.t)localObject2).d();
          c.g.b.k.a(localObject1, "reactionSent.content");
          localObject1 = ((ReactionContent)localObject1).a();
          c.g.b.k.a(localObject1, "reactionSent.content.emoji");
          localObject1 = ((ReactionContent.b)localObject1).a();
          c.g.b.k.a(localObject1, "reactionSent.content.emoji.value");
          localObject3 = ((Event.t)localObject2).a();
          c.g.b.k.a(localObject3, "reactionSent.sender");
          localObject3 = ((Peer)localObject3).b();
          c.g.b.k.a(localObject3, "reactionSent.sender.user");
          localObject3 = ((Peer.d)localObject3).a();
          c.g.b.k.a(localObject3, "reactionSent.sender.user.id");
          l1 = TimeUnit.SECONDS.toMillis(((Event.t)localObject2).c());
          localObject2 = ((Event.t)localObject2).b();
          c.g.b.k.a(localObject2, "reactionSent.recipient");
          localObject2 = ((Peer)localObject2).c();
          c.g.b.k.a(localObject2, "reactionSent.recipient.group");
          localObject2 = ((Peer.b)localObject2).a();
          c.g.b.k.a(localObject2, "reactionSent.recipient.group.id");
          localObject4 = new Reaction(0L, 0L, (String)localObject3, (String)localObject1, l1, 1, 3);
          paramIntent = new Intent("update_reaction").putExtra("reaction", (Parcelable)localObject4).putExtra("rawId", paramIntent);
          c.g.b.k.a(paramIntent, "intent");
          a(paramIntent, 0);
          a((String)localObject3, (String)localObject1, (String)localObject2, null, "Received", "incoming");
          return;
        case 2: 
          paramIntent = ((Event)localObject1).d();
          c.g.b.k.a(paramIntent, "event.reportSent");
          switch (paramIntent.e())
          {
          default: 
            return;
          case 1: 
            paramInt = 3;
            break;
          case 0: 
          case 2: 
            paramInt = 2;
          }
          localObject1 = new ImTransportInfo.a();
          localObject2 = paramIntent.c();
          c.g.b.k.a(localObject2, "reportSent.messageId");
          localObject1 = ((ImTransportInfo.a)localObject1).a((String)localObject2);
          c = 3;
          d = 3;
          g = paramInt;
          localObject1 = ((ImTransportInfo.a)localObject1).a();
          localObject2 = new Message.a();
          localObject3 = paramIntent.b();
          c.g.b.k.a(localObject3, "reportSent.recipient");
          localObject1 = ((Message.a)localObject2).a(com.truecaller.messaging.i.i.a((Peer)localObject3)).a(2, (TransportInfo)localObject1).a(new org.a.a.b(TimeUnit.SECONDS.toMillis(paramIntent.d()))).b();
          c.g.b.k.a(localObject1, "Message.Builder()\n      …))))\n            .build()");
          ((t)d.a()).b((Message)localObject1);
          if (paramIntent.e() == 1)
          {
            localObject1 = (bp)o.a();
            localObject2 = paramIntent.a();
            c.g.b.k.a(localObject2, "reportSent.senderId");
            localObject1 = (String)((bp)localObject1).c((String)localObject2).d();
            v.a((String)localObject1, new org.a.a.b(TimeUnit.SECONDS.toMillis(paramIntent.d())));
          }
          return;
        }
        paramIntent = ((Event)localObject1).c();
        c.g.b.k.a(paramIntent, "event.messageSent");
        localObject2 = paramIntent.c();
        c.g.b.k.a(localObject2, "messageSent.messageId");
        if (c((String)localObject2))
        {
          if (bool2) {
            continue;
          }
          paramInt = 9;
        }
        else
        {
          paramInt = 0;
        }
        localObject2 = new ImTransportInfo.a();
        localObject3 = paramIntent.c();
        c.g.b.k.a(localObject3, "messageSent.messageId");
        localObject2 = ((ImTransportInfo.a)localObject2).a((String)localObject3);
        localObject3 = paramIntent.b();
        c.g.b.k.a(localObject3, "recipient");
        if (((Peer)localObject3).a() == Peer.TypeCase.GROUP) {
          i1 = 2;
        } else {
          i1 = 0;
        }
        b = i1;
        g = paramInt;
        e = 1;
        localObject1 = ((Event)localObject1).l();
        if (localObject1 != null) {
          paramInt = ((Event.l)localObject1).a();
        } else {
          paramInt = 0;
        }
        i = paramInt;
        localObject1 = paramIntent.b();
        c.g.b.k.a(localObject1, "messageSent.recipient");
        l = com.truecaller.messaging.i.i.a((Peer)localObject1);
        localObject2 = ((ImTransportInfo.a)localObject2).a();
        localObject1 = paramIntent.a();
        c.g.b.k.a(localObject1, "messageSent.sender");
        localObject1 = com.truecaller.messaging.i.i.a((Peer.d)localObject1);
        localObject2 = new Message.a().a((Participant)localObject1).a(new org.a.a.b(TimeUnit.SECONDS.toMillis(paramIntent.d()))).a(2, (TransportInfo)localObject2);
        localObject3 = paramIntent.a();
        c.g.b.k.a(localObject3, "messageSent.sender");
        localObject2 = ((Message.a)localObject2).c(((Peer.d)localObject3).a());
        localObject3 = paramIntent.b();
        c.g.b.k.a(localObject3, "recipient");
        if (((Peer)localObject3).a() == Peer.TypeCase.GROUP)
        {
          localObject3 = a;
          localObject4 = TruecallerContract.r.a();
          c.g.b.k.a(localObject4, "ImGroupInfoTable.getContentUri()");
          Object localObject5 = paramIntent.b();
          c.g.b.k.a(localObject5, "recipient");
          localObject5 = ((Peer)localObject5).c();
          c.g.b.k.a(localObject5, "recipient.group");
          localObject5 = ((Peer.b)localObject5).a();
          c.g.b.k.a(localObject5, "recipient.group.id");
          localObject3 = com.truecaller.utils.extensions.h.a((ContentResolver)localObject3, (Uri)localObject4, "notification_settings", "im_group_id = ?", new String[] { localObject5 });
          if ((localObject3 != null) && (((Integer)localObject3).intValue() == 1))
          {
            bool1 = true;
            continue;
          }
        }
        bool1 = false;
        localObject2 = ((Message.a)localObject2).a(bool1);
        c.g.b.k.a(localObject2, "ImMessageBuilder()\n     …en(messageSent.isMuted())");
        localObject3 = paramIntent.e();
        c.g.b.k.a(localObject3, "messageSent.content");
        if (am.c((CharSequence)((MessageContent)localObject3).g()))
        {
          if ((r.h().a()) && (r.b().a())) {
            paramInt = 1;
          } else {
            paramInt = 0;
          }
          if (paramInt == 0) {}
        }
      }
      catch (x paramIntent)
      {
        Object localObject4;
        int i1;
        AssertionUtil.reportThrowableButNeverCrash((Throwable)paramIntent);
        return;
      }
      try
      {
        localObject3 = (t)d.a();
        localObject4 = paramIntent.e();
        c.g.b.k.a(localObject4, "messageSent.content");
        localObject3 = (Long)((t)localObject3).a(((MessageContent)localObject4).g()).d();
        if (localObject3 != null)
        {
          c.g.b.k.a(localObject3, "it");
          ((Message.a)localObject2).f(((Long)localObject3).longValue());
        }
      }
      catch (InterruptedException localInterruptedException) {}
    }
    localObject3 = paramIntent.e();
    c.g.b.k.a(localObject3, "messageSent.content");
    if (am.c((CharSequence)((MessageContent)localObject3).b()))
    {
      localObject3 = paramIntent.e();
      c.g.b.k.a(localObject3, "messageSent.content");
      ((Message.a)localObject2).a(TextEntity.a("text/plain", ((MessageContent)localObject3).b()));
    }
    i1 = i3;
    if (g())
    {
      i1 = i3;
      if (i.a()) {
        i1 = 1;
      }
    }
    if ((i1 != 0) && (!bool2))
    {
      paramInt = 1;
    }
    else
    {
      paramInt = i2;
      if (i1 != 0)
      {
        paramInt = i2;
        if (bool2) {
          paramInt = 5;
        }
      }
    }
    localObject3 = m;
    localObject4 = paramIntent.e();
    c.g.b.k.a(localObject4, "messageSent.content");
    localObject3 = ((f)localObject3).a((MessageContent)localObject4, paramInt);
    if (localObject3 != null) {
      ((Message.a)localObject2).a((Entity)localObject3);
    }
    localObject2 = ((Message.a)localObject2).b();
    c.g.b.k.a(localObject2, "builder.build()");
    if (((Message)localObject2).c()) {
      ((t)d.a()).a((Message)localObject2);
    }
    if ((c == 0) && (d != null))
    {
      localObject2 = (bp)o.a();
      localObject3 = f;
      c.g.b.k.a(localObject3, "participant.normalizedAddress");
      ((bp)localObject2).a((String)localObject3, d);
    }
    v.a(f, new org.a.a.b(TimeUnit.SECONDS.toMillis(paramIntent.d())));
    return;
    if (((String)localObject1).equals("unsupported_event"))
    {
      localObject1 = paramIntent.getByteArrayExtra("unsupported_event");
      if (localObject1 == null) {
        return;
      }
      try
      {
        localObject1 = Event.a((byte[])localObject1);
        paramInt = paramIntent.getIntExtra("api_version", 0);
        paramIntent = (bj)q.a();
        c.g.b.k.a(localObject1, "event");
        paramIntent.a((Event)localObject1, paramInt);
        return;
      }
      catch (x paramIntent)
      {
        AssertionUtil.reportThrowableButNeverCrash((Throwable)paramIntent);
        return;
      }
      if (((String)localObject1).equals("update_reaction"))
      {
        localObject1 = paramIntent.getParcelableExtra("reaction");
        c.g.b.k.a(localObject1, "intent.getParcelableExtra(EXTRA_REACTION)");
        localObject1 = (Reaction)localObject1;
        localObject2 = paramIntent.getStringExtra("rawId");
        c.g.b.k.a(localObject2, "intent.getStringExtra(EXTRA_RAW_ID)");
        paramIntent = new Message.a().a(Participant.a);
        localObject3 = new ImTransportInfo.a();
        g = 10;
        localObject2 = ((ImTransportInfo.a)localObject3).a((String)localObject2);
        c.g.b.k.b(localObject1, "reaction");
        k = m.c(new Reaction[] { localObject1 });
        paramIntent = paramIntent.a(2, (TransportInfo)((ImTransportInfo.a)localObject2).a()).b();
        c.g.b.k.a(paramIntent, "Message.Builder()\n      …   )\n            .build()");
        ((t)d.a()).b(paramIntent);
        return;
        if (((String)localObject1).equals("update_entity"))
        {
          paramIntent = (Entity)paramIntent.getParcelableExtra("entity");
          if (paramIntent == null) {
            return;
          }
          paramIntent = new Message.a().a(Participant.a).a(paramIntent);
          localObject1 = new ImTransportInfo.a();
          g = 6;
          paramIntent = paramIntent.a(2, (TransportInfo)((ImTransportInfo.a)localObject1).a()).b();
          c.g.b.k.a(paramIntent, "Message.Builder()\n      …   )\n            .build()");
          ((t)d.a()).b(paramIntent);
          return;
        }
      }
    }
  }
  
  public final void a(BinaryEntity paramBinaryEntity)
  {
    c.g.b.k.b(paramBinaryEntity, "entity");
    if ((r.d().a()) && (paramBinaryEntity.c())) {
      t.b(paramBinaryEntity);
    }
    m.a(paramBinaryEntity);
  }
  
  public final void a(Message paramMessage, String paramString1, String paramString2)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramString2, "initiatedVia");
    String str1 = f.G();
    if (str1 == null) {
      return;
    }
    String str2 = c(paramMessage.a());
    if (str2 == null) {
      return;
    }
    String str3 = c.d;
    String str4 = d(b);
    boolean bool;
    if ((str3 == null) && (str4 == null)) {
      bool = false;
    } else {
      bool = true;
    }
    AssertionUtil.isTrue(bool, new String[] { "imPeerId or imGroupId must be set for sending reaction" });
    Object localObject = new Reaction(0L, paramMessage.a(), str1, paramString1, System.currentTimeMillis(), 2, 1);
    localObject = new Intent("update_reaction").putExtra("reaction", (Parcelable)localObject).putExtra("rawId", str2);
    c.g.b.k.a(localObject, "intent");
    a((Intent)localObject, 0);
    a(f.G(), paramString1, str4, paramString2, "Sent", "outgoing");
    paramString2 = w;
    localObject = androidx.work.g.c;
    SendReactionWorker.a locala = SendReactionWorker.c;
    long l1 = paramMessage.a();
    c.g.b.k.b(str2, "rawId");
    c.g.b.k.b(str1, "fromPeerId");
    paramMessage = ((k.a)((k.a)((k.a)((k.a)new k.a(SendReactionWorker.class).a(androidx.work.a.a, 30L, TimeUnit.SECONDS)).a(new androidx.work.e.a().a("raw_id", str2).a("message_id", l1).a("from_peer_id", str1).a("to_peer_id", str3).a("to_group_id", str4).a("emoji", paramString1).a())).a("send_im_reaction")).a(new c.a().a(j.b).a())).c();
    c.g.b.k.a(paramMessage, "OneTimeWorkRequest.Build…\n                .build()");
    paramString2.a("SendReaction", (androidx.work.g)localObject, (androidx.work.k)paramMessage);
  }
  
  public final void a(org.a.a.b paramb)
  {
    c.g.b.k.b(paramb, "time");
    f.a(2, a);
  }
  
  public final boolean a(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    Object localObject1 = paramMessage.k();
    c.g.b.k.a(localObject1, "message.getTransportInfo<ImTransportInfo>()");
    localObject1 = (ImTransportInfo)localObject1;
    int i1;
    switch (l)
    {
    default: 
      return false;
    case 10: 
      return a((ImTransportInfo)localObject1);
    case 9: 
      return g(paramMessage);
    case 8: 
      paramMessage = n;
      c.g.b.k.a(paramMessage, "message.entities");
      return c(paramMessage);
    case 7: 
      paramMessage = n;
      c.g.b.k.a(paramMessage, "message.entities");
      return b(paramMessage);
    case 6: 
      paramMessage = n;
      c.g.b.k.a(paramMessage, "message.entities");
      return a(paramMessage);
    case 5: 
      paramMessage = new ContentValues();
      paramMessage.put("read_sync_status", Integer.valueOf(g));
      return a((ImTransportInfo)localObject1, paramMessage);
    case 4: 
      paramMessage = new ContentValues();
      paramMessage.put("delivery_sync_status", Integer.valueOf(f));
      return a((ImTransportInfo)localObject1, paramMessage);
    case 3: 
      localObject1 = paramMessage.k();
      c.g.b.k.a(localObject1, "message.getTransportInfo()");
      localObject1 = (ImTransportInfo)localObject1;
      localObject2 = new ContentValues();
      ((ContentValues)localObject2).put("read_status", Integer.valueOf(e));
      if (c.c == 4) {
        return a((ImTransportInfo)localObject1, (ContentValues)localObject2, "read_status != ?", new String[] { String.valueOf(e) });
      }
      return a((ImTransportInfo)localObject1, (ContentValues)localObject2);
    case 2: 
      localObject1 = paramMessage.k();
      c.g.b.k.a(localObject1, "message.getTransportInfo<ImTransportInfo>()");
      localObject1 = (ImTransportInfo)localObject1;
      localObject2 = new ContentValues();
      ((ContentValues)localObject2).put("delivery_status", Integer.valueOf(d));
      boolean bool;
      if (c.c == 4) {
        bool = a((ImTransportInfo)localObject1, (ContentValues)localObject2, "delivery_status != ?", new String[] { String.valueOf(d) });
      } else {
        bool = a((ImTransportInfo)localObject1, (ContentValues)localObject2);
      }
      return (bool) && (a(paramMessage, b));
    case 1: 
      AssertionUtil.AlwaysFatal.isTrue(paramMessage.b(), new String[0]);
      localObject1 = new ArrayList();
      com.truecaller.messaging.data.b.a((List)localObject1, (ImTransportInfo)paramMessage.k());
      paramMessage = a((ArrayList)localObject1);
      if (paramMessage != null)
      {
        if (paramMessage.length == 0) {
          i1 = 1;
        } else {
          i1 = 0;
        }
        return i1 == 0;
      }
      return false;
    }
    ArrayList localArrayList = new ArrayList();
    localObject1 = ContentProviderOperation.newAssertQuery(TruecallerContract.w.a());
    Object localObject2 = m;
    if (localObject2 != null)
    {
      localArrayList.add(((ContentProviderOperation.Builder)localObject1).withSelection("raw_id=? AND (im_status & 1)=0", new String[] { b }).withExpectedCount(0).build());
      List localList = (List)localArrayList;
      int i2 = com.truecaller.messaging.data.b.a(localList, c);
      Object localObject3 = s.a(paramMessage, (com.truecaller.insights.models.d)d.a.a);
      localObject1 = m).m;
      if (localObject1 != null)
      {
        if (c == 4) {
          i1 = 1;
        } else {
          i1 = 0;
        }
        if (i1 == 0) {
          localObject1 = null;
        }
        if (localObject1 != null)
        {
          com.truecaller.messaging.data.b.a(localList, (Participant)localObject1);
          break label666;
        }
      }
      localObject1 = null;
      label666:
      localObject2 = localObject1;
      if (localObject1 == null)
      {
        localObject2 = c;
        c.g.b.k.a(localObject2, "message.participant");
      }
      int i3 = com.truecaller.messaging.data.b.a(localList, an.a(localObject2));
      i1 = localArrayList.size();
      localObject1 = ContentProviderOperation.newInsert(TruecallerContract.aa.a()).withValueBackReference("participant_id", i2).withValueBackReference("conversation_id", i3);
      i2 = a;
      i3 = b;
      localObject2 = new ContentValues();
      localObject3 = e;
      c.g.b.k.a(localObject3, "message.date");
      ((ContentValues)localObject2).put("date", Long.valueOf(a));
      localObject3 = d;
      c.g.b.k.a(localObject3, "message.dateSent");
      ((ContentValues)localObject2).put("date_sent", Long.valueOf(a));
      ((ContentValues)localObject2).put("status", Integer.valueOf(f));
      ((ContentValues)localObject2).put("seen", Boolean.valueOf(g));
      ((ContentValues)localObject2).put("read", Boolean.valueOf(h));
      ((ContentValues)localObject2).put("locked", Boolean.valueOf(i));
      ((ContentValues)localObject2).put("transport", Integer.valueOf(j));
      ((ContentValues)localObject2).put("analytics_id", o);
      ((ContentValues)localObject2).put("raw_address", p);
      ((ContentValues)localObject2).put("category", Integer.valueOf(i2));
      ((ContentValues)localObject2).put("classification", Integer.valueOf(i3));
      ((ContentValues)localObject2).put("reply_to_msg_id", Long.valueOf(x));
      localArrayList.add(((ContentProviderOperation.Builder)localObject1).withValues((ContentValues)localObject2).build());
      com.truecaller.messaging.data.b.a(localList, i1, (ImTransportInfo)paramMessage.k());
      localObject1 = a(localArrayList);
      if (localObject1 == null) {
        return false;
      }
      if (localObject1.length < localArrayList.size()) {
        return false;
      }
      long l1 = ContentUris.parseId(uri);
      localArrayList.clear();
      localObject1 = n;
      c.g.b.k.a(localObject1, "message.entities");
      a(localArrayList, l1, (Entity[])localObject1);
      localObject1 = a(localArrayList);
      if (localObject1 != null)
      {
        if (localObject1.length == 0) {
          i1 = 1;
        } else {
          i1 = 0;
        }
        if (i1 == 1) {
          return false;
        }
      }
      ((q)h.a()).a();
      AssertionUtil.AlwaysFatal.isTrue(c.f(), new String[0]);
      if ((f & 0x1) == 0)
      {
        try
        {
          paramMessage = com.truecaller.tracking.events.u.b().d((CharSequence)"im").a((CharSequence)m.a(e)).b((CharSequence)c.d).c((CharSequence)"tc").a();
          ((ae)k.a()).a((org.apache.a.d.d)paramMessage);
        }
        catch (org.apache.a.a paramMessage)
        {
          AssertionUtil.shouldNeverHappen((Throwable)paramMessage, new String[0]);
        }
        paramMessage = l;
        localObject1 = new com.truecaller.analytics.e.a("MessageReceived").a("Type", "im").a();
        c.g.b.k.a(localObject1, "AnalyticsEvent.Builder(A…\n                .build()");
        paramMessage.a((com.truecaller.analytics.e)localObject1);
        paramMessage = l;
        localObject1 = new com.truecaller.analytics.e.a("IMMessage").a("Action", "Received").a();
        c.g.b.k.a(localObject1, "AnalyticsEvent.Builder(A…\n                .build()");
        paramMessage.a((com.truecaller.analytics.e)localObject1);
      }
      return true;
    }
    throw new c.u("null cannot be cast to non-null type com.truecaller.messaging.transport.im.ImTransportInfo");
  }
  
  public final boolean a(Message paramMessage, Entity paramEntity)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramEntity, "entity");
    t localt = (t)d.a();
    paramMessage = paramMessage.m().a().a(paramEntity);
    paramEntity = new ImTransportInfo.a();
    g = 8;
    localt.b(paramMessage.a(2, (TransportInfo)paramEntity.a()).b());
    return true;
  }
  
  public final boolean a(Participant paramParticipant)
  {
    c.g.b.k.b(paramParticipant, "participant");
    if (n.b()) {
      return false;
    }
    return (paramParticipant.f()) || ((r.i().a()) && (c == 4));
  }
  
  public final boolean a(String paramString, com.truecaller.messaging.transport.a parama)
  {
    c.g.b.k.b(paramString, "text");
    c.g.b.k.b(parama, "result");
    parama.a(0, 0, 0, 2);
    return false;
  }
  
  public final void b(long paramLong)
  {
    Object localObject = RetryImMessageWorker.d;
    localObject = ((k.a)((k.a)new k.a(RetryImMessageWorker.class).a(Math.max(paramLong - System.currentTimeMillis(), 0L), TimeUnit.MILLISECONDS).a(new androidx.work.e.a().a("to_date", paramLong).a())).a(new c.a().a(j.b).a())).c();
    c.g.b.k.a(localObject, "OneTimeWorkRequest.Build…\n                .build()");
    localObject = (androidx.work.k)localObject;
    w.a("RetryImMessage", androidx.work.g.a, (androidx.work.k)localObject);
  }
  
  public final boolean b(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    if (c(paramMessage))
    {
      paramMessage = c;
      c.g.b.k.a(paramMessage, "message.participant");
      if (a(paramMessage)) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean b(ad paramad)
  {
    c.g.b.k.b(paramad, "transaction");
    return (!paramad.a()) && (c.g.b.k.a(paramad.c(), TruecallerContract.a()));
  }
  
  public final boolean c()
  {
    return true;
  }
  
  public final boolean c(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    if (n.b()) {
      return false;
    }
    return paramMessage.c();
  }
  
  /* Error */
  public final com.truecaller.messaging.transport.k d(Message paramMessage)
  {
    // Byte code:
    //   0: aload_0
    //   1: astore 13
    //   3: aload_1
    //   4: astore 14
    //   6: aload 14
    //   8: ldc_w 864
    //   11: invokestatic 66	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   14: aload_1
    //   15: invokevirtual 1515	com/truecaller/messaging/data/types/Message:b	()Z
    //   18: iconst_0
    //   19: anewarray 192	java/lang/String
    //   22: invokestatic 1374	com/truecaller/log/AssertionUtil:isTrue	(Z[Ljava/lang/String;)V
    //   25: aload 14
    //   27: getfield 1366	com/truecaller/messaging/data/types/Message:c	Lcom/truecaller/messaging/data/types/Participant;
    //   30: invokevirtual 1624	com/truecaller/messaging/data/types/Participant:f	()Z
    //   33: ifne +24 -> 57
    //   36: aload 14
    //   38: getfield 1366	com/truecaller/messaging/data/types/Message:c	Lcom/truecaller/messaging/data/types/Participant;
    //   41: getfield 1315	com/truecaller/messaging/data/types/Participant:c	I
    //   44: iconst_4
    //   45: if_icmpne +6 -> 51
    //   48: goto +9 -> 57
    //   51: iconst_0
    //   52: istore 8
    //   54: goto +6 -> 60
    //   57: iconst_1
    //   58: istore 8
    //   60: iload 8
    //   62: iconst_0
    //   63: anewarray 192	java/lang/String
    //   66: invokestatic 1374	com/truecaller/log/AssertionUtil:isTrue	(Z[Ljava/lang/String;)V
    //   69: aload 13
    //   71: getfield 127	com/truecaller/messaging/transport/im/bd:f	Lcom/truecaller/messaging/h;
    //   74: invokestatic 1702	org/a/a/b:ay_	()Lorg/a/a/b;
    //   77: invokeinterface 1817 2 0
    //   82: aload 13
    //   84: getfield 135	com/truecaller/messaging/transport/im/bd:j	Lcom/truecaller/utils/i;
    //   87: invokeinterface 800 1 0
    //   92: ifne +30 -> 122
    //   95: invokestatic 1702	org/a/a/b:ay_	()Lorg/a/a/b;
    //   98: invokevirtual 1819	org/a/a/b:c	()Lorg/a/a/b;
    //   101: astore_1
    //   102: aload_1
    //   103: ldc_w 1821
    //   106: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   109: new 1823	com/truecaller/messaging/transport/k$c
    //   112: dup
    //   113: aload_1
    //   114: iconst_0
    //   115: invokespecial 1826	com/truecaller/messaging/transport/k$c:<init>	(Lorg/a/a/b;Z)V
    //   118: checkcast 1828	com/truecaller/messaging/transport/k
    //   121: areturn
    //   122: aload 13
    //   124: getfield 121	com/truecaller/messaging/transport/im/bd:c	Lcom/truecaller/messaging/transport/im/cb;
    //   127: getstatic 1833	com/truecaller/common/network/e$a:a	Lcom/truecaller/common/network/e$a;
    //   130: checkcast 1835	com/truecaller/common/network/e
    //   133: invokeinterface 1840 2 0
    //   138: ifne +10 -> 148
    //   141: getstatic 1845	com/truecaller/messaging/transport/k$b:a	Lcom/truecaller/messaging/transport/k$b;
    //   144: checkcast 1828	com/truecaller/messaging/transport/k
    //   147: areturn
    //   148: aload 13
    //   150: astore 15
    //   152: aload 14
    //   154: astore 16
    //   156: aload 13
    //   158: astore 16
    //   160: aload 14
    //   162: astore 17
    //   164: aload 13
    //   166: astore 17
    //   168: aload 14
    //   170: astore 20
    //   172: invokestatic 1850	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent:a	()Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c;
    //   175: aload_1
    //   176: invokevirtual 1852	com/truecaller/messaging/data/types/Message:j	()Ljava/lang/String;
    //   179: invokevirtual 1857	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c:a	(Ljava/lang/String;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c;
    //   182: astore 18
    //   184: aload 13
    //   186: astore 15
    //   188: aload 14
    //   190: astore 16
    //   192: aload 13
    //   194: astore 16
    //   196: aload 14
    //   198: astore 17
    //   200: aload 13
    //   202: astore 17
    //   204: aload 14
    //   206: astore 20
    //   208: aload_1
    //   209: invokevirtual 1858	com/truecaller/messaging/data/types/Message:g	()Z
    //   212: istore 8
    //   214: iload 8
    //   216: ifeq +127 -> 343
    //   219: aload 13
    //   221: astore 15
    //   223: aload 14
    //   225: astore 16
    //   227: aload 13
    //   229: astore 16
    //   231: aload 14
    //   233: astore 17
    //   235: aload 13
    //   237: astore 17
    //   239: aload 14
    //   241: astore 20
    //   243: aload 13
    //   245: getfield 123	com/truecaller/messaging/transport/im/bd:d	Lcom/truecaller/androidactors/f;
    //   248: invokeinterface 256 1 0
    //   253: checkcast 258	com/truecaller/messaging/data/t
    //   256: aload 14
    //   258: getfield 1606	com/truecaller/messaging/data/types/Message:x	J
    //   261: invokeinterface 1861 3 0
    //   266: invokevirtual 633	com/truecaller/androidactors/w:d	()Ljava/lang/Object;
    //   269: checkcast 192	java/lang/String
    //   272: astore 19
    //   274: aload 19
    //   276: ifnull +67 -> 343
    //   279: aload 13
    //   281: astore 15
    //   283: aload 14
    //   285: astore 16
    //   287: aload 13
    //   289: astore 16
    //   291: aload 14
    //   293: astore 17
    //   295: aload 13
    //   297: astore 17
    //   299: aload 14
    //   301: astore 20
    //   303: aload 18
    //   305: ldc_w 1863
    //   308: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   311: aload 13
    //   313: astore 15
    //   315: aload 14
    //   317: astore 16
    //   319: aload 13
    //   321: astore 16
    //   323: aload 14
    //   325: astore 17
    //   327: aload 13
    //   329: astore 17
    //   331: aload 14
    //   333: astore 20
    //   335: aload 18
    //   337: aload 19
    //   339: invokevirtual 1865	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c:b	(Ljava/lang/String;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c;
    //   342: pop
    //   343: aload 13
    //   345: astore 15
    //   347: aload 14
    //   349: astore 16
    //   351: aload 13
    //   353: astore 16
    //   355: aload 14
    //   357: astore 17
    //   359: aload 13
    //   361: astore 17
    //   363: aload 14
    //   365: astore 20
    //   367: new 329	java/util/ArrayList
    //   370: dup
    //   371: invokespecial 424	java/util/ArrayList:<init>	()V
    //   374: checkcast 421	java/util/List
    //   377: astore 21
    //   379: aload 13
    //   381: astore 15
    //   383: aload 14
    //   385: astore 16
    //   387: aload 13
    //   389: astore 16
    //   391: aload 14
    //   393: astore 17
    //   395: aload 13
    //   397: astore 17
    //   399: aload 14
    //   401: astore 20
    //   403: aload 14
    //   405: getfield 820	com/truecaller/messaging/data/types/Message:n	[Lcom/truecaller/messaging/data/types/Entity;
    //   408: astore 19
    //   410: aload 13
    //   412: astore 15
    //   414: aload 14
    //   416: astore 16
    //   418: aload 13
    //   420: astore 16
    //   422: aload 14
    //   424: astore 17
    //   426: aload 13
    //   428: astore 17
    //   430: aload 14
    //   432: astore 20
    //   434: aload 19
    //   436: ldc_w 822
    //   439: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   442: aload 13
    //   444: astore 15
    //   446: aload 14
    //   448: astore 16
    //   450: aload 13
    //   452: astore 16
    //   454: aload 14
    //   456: astore 17
    //   458: aload 13
    //   460: astore 17
    //   462: aload 14
    //   464: astore 20
    //   466: new 329	java/util/ArrayList
    //   469: dup
    //   470: invokespecial 424	java/util/ArrayList:<init>	()V
    //   473: checkcast 334	java/util/Collection
    //   476: astore 22
    //   478: aload 13
    //   480: astore 15
    //   482: aload 14
    //   484: astore 16
    //   486: aload 13
    //   488: astore 16
    //   490: aload 14
    //   492: astore 17
    //   494: aload 13
    //   496: astore 17
    //   498: aload 14
    //   500: astore 20
    //   502: aload 19
    //   504: arraylength
    //   505: istore_3
    //   506: iconst_0
    //   507: istore_2
    //   508: iload_2
    //   509: iload_3
    //   510: if_icmpge +78 -> 588
    //   513: aload 19
    //   515: iload_2
    //   516: aaload
    //   517: astore 23
    //   519: aload 13
    //   521: astore 15
    //   523: aload 14
    //   525: astore 16
    //   527: aload 13
    //   529: astore 16
    //   531: aload 14
    //   533: astore 17
    //   535: aload 13
    //   537: astore 17
    //   539: aload 14
    //   541: astore 20
    //   543: aload 23
    //   545: instanceof 441
    //   548: ifeq +2495 -> 3043
    //   551: aload 13
    //   553: astore 15
    //   555: aload 14
    //   557: astore 16
    //   559: aload 13
    //   561: astore 16
    //   563: aload 14
    //   565: astore 17
    //   567: aload 13
    //   569: astore 17
    //   571: aload 14
    //   573: astore 20
    //   575: aload 22
    //   577: aload 23
    //   579: invokeinterface 419 2 0
    //   584: pop
    //   585: goto +2458 -> 3043
    //   588: aload 13
    //   590: astore 15
    //   592: aload 14
    //   594: astore 16
    //   596: aload 13
    //   598: astore 16
    //   600: aload 14
    //   602: astore 17
    //   604: aload 13
    //   606: astore 17
    //   608: aload 14
    //   610: astore 20
    //   612: aload 22
    //   614: checkcast 421	java/util/List
    //   617: checkcast 423	java/lang/Iterable
    //   620: invokeinterface 428 1 0
    //   625: astore 19
    //   627: iconst_0
    //   628: istore_2
    //   629: iconst_0
    //   630: istore_3
    //   631: aload 13
    //   633: astore 15
    //   635: aload 14
    //   637: astore 16
    //   639: aload 13
    //   641: astore 16
    //   643: aload 14
    //   645: astore 17
    //   647: aload 13
    //   649: astore 17
    //   651: aload 14
    //   653: astore 20
    //   655: aload 19
    //   657: invokeinterface 433 1 0
    //   662: istore 8
    //   664: iload 8
    //   666: ifeq +1165 -> 1831
    //   669: aload 13
    //   671: astore 15
    //   673: aload 14
    //   675: astore 16
    //   677: aload 13
    //   679: astore 16
    //   681: aload 14
    //   683: astore 17
    //   685: aload 19
    //   687: invokeinterface 436 1 0
    //   692: checkcast 441	com/truecaller/messaging/data/types/BinaryEntity
    //   695: astore 22
    //   697: aload 13
    //   699: astore 15
    //   701: aload 14
    //   703: astore 16
    //   705: aload 13
    //   707: astore 16
    //   709: aload 14
    //   711: astore 17
    //   713: aload 13
    //   715: getfield 151	com/truecaller/messaging/transport/im/bd:r	Lcom/truecaller/featuretoggles/e;
    //   718: invokevirtual 1349	com/truecaller/featuretoggles/e:d	()Lcom/truecaller/featuretoggles/b;
    //   721: invokeinterface 1055 1 0
    //   726: istore 8
    //   728: iload 8
    //   730: ifeq +1007 -> 1737
    //   733: aload 13
    //   735: astore 16
    //   737: aload 13
    //   739: astore 17
    //   741: aload 13
    //   743: astore 15
    //   745: aload 22
    //   747: invokevirtual 1350	com/truecaller/messaging/data/types/BinaryEntity:c	()Z
    //   750: ifeq +987 -> 1737
    //   753: aload 13
    //   755: astore 16
    //   757: aload 13
    //   759: astore 17
    //   761: aload 13
    //   763: astore 15
    //   765: aload 13
    //   767: getfield 155	com/truecaller/messaging/transport/im/bd:t	Lcom/truecaller/util/f/b;
    //   770: aload 22
    //   772: invokeinterface 1868 2 0
    //   777: astore 20
    //   779: aload 20
    //   781: ifnonnull +10 -> 791
    //   784: aload 18
    //   786: astore 15
    //   788: goto +988 -> 1776
    //   791: aload 13
    //   793: astore 15
    //   795: aload 13
    //   797: getfield 141	com/truecaller/messaging/transport/im/bd:m	Lcom/truecaller/messaging/transport/im/f;
    //   800: astore 16
    //   802: aload 13
    //   804: astore 15
    //   806: aload 18
    //   808: ldc_w 1863
    //   811: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   814: aload 13
    //   816: astore 15
    //   818: aload 16
    //   820: aload 18
    //   822: aload 20
    //   824: checkcast 441	com/truecaller/messaging/data/types/BinaryEntity
    //   827: aload 14
    //   829: invokeinterface 1871 4 0
    //   834: aload 20
    //   836: ifnull +418 -> 1254
    //   839: aload 13
    //   841: astore 15
    //   843: aload 13
    //   845: aload 20
    //   847: checkcast 441	com/truecaller/messaging/data/types/BinaryEntity
    //   850: invokespecial 870	com/truecaller/messaging/transport/im/bd:b	(Lcom/truecaller/messaging/data/types/BinaryEntity;)Landroid/net/Uri;
    //   853: astore 23
    //   855: aload 13
    //   857: astore 15
    //   859: aload 23
    //   861: ldc_w 1873
    //   864: invokestatic 66	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   867: aload 13
    //   869: astore 15
    //   871: aload 20
    //   873: getfield 1876	com/truecaller/messaging/data/types/VideoEntity:h	J
    //   876: lstore 9
    //   878: aload 13
    //   880: astore 15
    //   882: aload 20
    //   884: getfield 1877	com/truecaller/messaging/data/types/VideoEntity:j	Ljava/lang/String;
    //   887: astore 24
    //   889: aload 13
    //   891: astore 15
    //   893: aload 24
    //   895: ldc_w 1879
    //   898: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   901: aload 13
    //   903: astore 15
    //   905: aload 20
    //   907: getfield 1880	com/truecaller/messaging/data/types/VideoEntity:i	I
    //   910: istore 4
    //   912: aload 13
    //   914: astore 15
    //   916: aload 20
    //   918: getfield 1882	com/truecaller/messaging/data/types/VideoEntity:c	Z
    //   921: istore 8
    //   923: aload 13
    //   925: astore 15
    //   927: aload 20
    //   929: getfield 1884	com/truecaller/messaging/data/types/VideoEntity:d	J
    //   932: lstore 11
    //   934: aload 13
    //   936: astore 15
    //   938: aload 20
    //   940: getfield 1885	com/truecaller/messaging/data/types/VideoEntity:a	I
    //   943: istore 5
    //   945: aload 13
    //   947: astore 16
    //   949: aload 13
    //   951: astore 17
    //   953: aload 13
    //   955: astore 15
    //   957: aload 13
    //   959: astore 14
    //   961: aload 20
    //   963: getfield 1886	com/truecaller/messaging/data/types/VideoEntity:k	I
    //   966: istore 6
    //   968: aload 13
    //   970: astore 16
    //   972: aload 13
    //   974: astore 17
    //   976: aload 13
    //   978: astore 15
    //   980: aload 13
    //   982: astore 14
    //   984: aload 20
    //   986: getfield 1887	com/truecaller/messaging/data/types/VideoEntity:l	I
    //   989: istore 7
    //   991: aload 21
    //   993: new 1875	com/truecaller/messaging/data/types/VideoEntity
    //   996: dup
    //   997: lload 9
    //   999: aload 24
    //   1001: iload 4
    //   1003: aload 23
    //   1005: iload 8
    //   1007: lload 11
    //   1009: iload 5
    //   1011: iload 6
    //   1013: iload 7
    //   1015: aload 20
    //   1017: getfield 1889	com/truecaller/messaging/data/types/VideoEntity:m	Landroid/net/Uri;
    //   1020: invokespecial 1892	com/truecaller/messaging/data/types/VideoEntity:<init>	(JLjava/lang/String;ILandroid/net/Uri;ZJIIILandroid/net/Uri;)V
    //   1023: checkcast 441	com/truecaller/messaging/data/types/BinaryEntity
    //   1026: invokeinterface 1698 2 0
    //   1031: pop
    //   1032: aload_0
    //   1033: astore 13
    //   1035: aload 13
    //   1037: astore 16
    //   1039: aload 13
    //   1041: astore 17
    //   1043: aload 13
    //   1045: astore 15
    //   1047: aload 13
    //   1049: astore 14
    //   1051: aload 13
    //   1053: getfield 157	com/truecaller/messaging/transport/im/bd:u	Lcom/truecaller/util/ch;
    //   1056: astore 23
    //   1058: aload 13
    //   1060: astore 16
    //   1062: aload 13
    //   1064: astore 17
    //   1066: aload 13
    //   1068: astore 15
    //   1070: aload 13
    //   1072: astore 14
    //   1074: aload 22
    //   1076: getfield 460	com/truecaller/messaging/data/types/BinaryEntity:b	Landroid/net/Uri;
    //   1079: astore 22
    //   1081: aload 13
    //   1083: astore 16
    //   1085: aload 13
    //   1087: astore 17
    //   1089: aload 13
    //   1091: astore 15
    //   1093: aload 13
    //   1095: astore 14
    //   1097: aload 22
    //   1099: ldc_w 1894
    //   1102: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1105: aload 13
    //   1107: astore 16
    //   1109: aload 13
    //   1111: astore 17
    //   1113: aload 13
    //   1115: astore 15
    //   1117: aload 13
    //   1119: astore 14
    //   1121: aload 23
    //   1123: aload 22
    //   1125: invokeinterface 1899 2 0
    //   1130: aload 13
    //   1132: astore 16
    //   1134: aload 13
    //   1136: astore 17
    //   1138: aload 13
    //   1140: astore 15
    //   1142: aload 20
    //   1144: getfield 1900	com/truecaller/messaging/data/types/VideoEntity:b	Landroid/net/Uri;
    //   1147: astore 14
    //   1149: aload 13
    //   1151: astore 16
    //   1153: aload 13
    //   1155: astore 17
    //   1157: aload 13
    //   1159: astore 15
    //   1161: aload 14
    //   1163: ldc_w 1902
    //   1166: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1169: aload 13
    //   1171: astore 16
    //   1173: aload 13
    //   1175: astore 17
    //   1177: aload 13
    //   1179: astore 15
    //   1181: new 586	java/io/File
    //   1184: dup
    //   1185: aload 14
    //   1187: invokevirtual 889	android/net/Uri:getPath	()Ljava/lang/String;
    //   1190: invokespecial 890	java/io/File:<init>	(Ljava/lang/String;)V
    //   1193: astore 14
    //   1195: goto +398 -> 1593
    //   1198: astore 14
    //   1200: aload_0
    //   1201: astore 13
    //   1203: goto +102 -> 1305
    //   1206: astore 14
    //   1208: aload_0
    //   1209: astore 13
    //   1211: goto +99 -> 1310
    //   1214: aload_0
    //   1215: astore 13
    //   1217: goto +222 -> 1439
    //   1220: astore 14
    //   1222: aload_0
    //   1223: astore 13
    //   1225: goto +291 -> 1516
    //   1228: astore 14
    //   1230: goto +80 -> 1310
    //   1233: goto +206 -> 1439
    //   1236: astore 14
    //   1238: goto +278 -> 1516
    //   1241: astore 14
    //   1243: goto +67 -> 1310
    //   1246: goto +1804 -> 3050
    //   1249: astore 14
    //   1251: goto +265 -> 1516
    //   1254: aload 13
    //   1256: astore 16
    //   1258: aload 13
    //   1260: astore 17
    //   1262: aload 13
    //   1264: astore 15
    //   1266: aload 13
    //   1268: astore 14
    //   1270: new 468	c/u
    //   1273: dup
    //   1274: ldc_w 470
    //   1277: invokespecial 471	c/u:<init>	(Ljava/lang/String;)V
    //   1280: athrow
    //   1281: astore 14
    //   1283: aload 16
    //   1285: astore 13
    //   1287: goto +23 -> 1310
    //   1290: astore 14
    //   1292: aload 17
    //   1294: astore 13
    //   1296: goto +220 -> 1516
    //   1299: astore 14
    //   1301: aload 15
    //   1303: astore 13
    //   1305: goto +316 -> 1621
    //   1308: astore 14
    //   1310: aload 13
    //   1312: astore 15
    //   1314: aload 14
    //   1316: checkcast 478	java/lang/Throwable
    //   1319: invokestatic 920	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   1322: aload 13
    //   1324: astore 15
    //   1326: aload_1
    //   1327: invokevirtual 810	com/truecaller/messaging/data/types/Message:k	()Lcom/truecaller/messaging/data/types/TransportInfo;
    //   1330: astore 16
    //   1332: aload 13
    //   1334: astore 15
    //   1336: aload 16
    //   1338: ldc_w 911
    //   1341: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1344: aload 13
    //   1346: astore 15
    //   1348: aload 13
    //   1350: aload 16
    //   1352: checkcast 194	com/truecaller/messaging/transport/im/ImTransportInfo
    //   1355: aload 14
    //   1357: getfield 1903	com/truecaller/messaging/transport/im/cj:a	I
    //   1360: invokespecial 1905	com/truecaller/messaging/transport/im/bd:a	(Lcom/truecaller/messaging/transport/im/ImTransportInfo;I)Lcom/truecaller/messaging/transport/im/ImTransportInfo;
    //   1363: pop
    //   1364: aload 13
    //   1366: astore 16
    //   1368: aload 13
    //   1370: astore 17
    //   1372: aload 13
    //   1374: astore 15
    //   1376: aload 20
    //   1378: getfield 1900	com/truecaller/messaging/data/types/VideoEntity:b	Landroid/net/Uri;
    //   1381: astore 14
    //   1383: aload 13
    //   1385: astore 16
    //   1387: aload 13
    //   1389: astore 17
    //   1391: aload 13
    //   1393: astore 15
    //   1395: aload 14
    //   1397: ldc_w 1902
    //   1400: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1403: aload 13
    //   1405: astore 16
    //   1407: aload 13
    //   1409: astore 17
    //   1411: aload 13
    //   1413: astore 15
    //   1415: new 586	java/io/File
    //   1418: dup
    //   1419: aload 14
    //   1421: invokevirtual 889	android/net/Uri:getPath	()Ljava/lang/String;
    //   1424: invokespecial 890	java/io/File:<init>	(Ljava/lang/String;)V
    //   1427: invokevirtual 840	java/io/File:delete	()Z
    //   1430: pop
    //   1431: aload_1
    //   1432: astore 14
    //   1434: iconst_1
    //   1435: istore_2
    //   1436: goto -805 -> 631
    //   1439: aload 13
    //   1441: astore 16
    //   1443: aload 13
    //   1445: astore 17
    //   1447: aload 13
    //   1449: astore 15
    //   1451: aload 20
    //   1453: getfield 1900	com/truecaller/messaging/data/types/VideoEntity:b	Landroid/net/Uri;
    //   1456: astore 14
    //   1458: aload 13
    //   1460: astore 16
    //   1462: aload 13
    //   1464: astore 17
    //   1466: aload 13
    //   1468: astore 15
    //   1470: aload 14
    //   1472: ldc_w 1902
    //   1475: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1478: aload 13
    //   1480: astore 16
    //   1482: aload 13
    //   1484: astore 17
    //   1486: aload 13
    //   1488: astore 15
    //   1490: new 586	java/io/File
    //   1493: dup
    //   1494: aload 14
    //   1496: invokevirtual 889	android/net/Uri:getPath	()Ljava/lang/String;
    //   1499: invokespecial 890	java/io/File:<init>	(Ljava/lang/String;)V
    //   1502: invokevirtual 840	java/io/File:delete	()Z
    //   1505: pop
    //   1506: aload_1
    //   1507: astore 14
    //   1509: iconst_1
    //   1510: istore_3
    //   1511: goto -880 -> 631
    //   1514: astore 14
    //   1516: aload 13
    //   1518: astore 15
    //   1520: aload 14
    //   1522: checkcast 478	java/lang/Throwable
    //   1525: invokestatic 920	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   1528: aload 13
    //   1530: astore 16
    //   1532: aload 13
    //   1534: astore 17
    //   1536: aload 13
    //   1538: astore 15
    //   1540: aload 20
    //   1542: getfield 1900	com/truecaller/messaging/data/types/VideoEntity:b	Landroid/net/Uri;
    //   1545: astore 14
    //   1547: aload 13
    //   1549: astore 16
    //   1551: aload 13
    //   1553: astore 17
    //   1555: aload 13
    //   1557: astore 15
    //   1559: aload 14
    //   1561: ldc_w 1902
    //   1564: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1567: aload 13
    //   1569: astore 16
    //   1571: aload 13
    //   1573: astore 17
    //   1575: aload 13
    //   1577: astore 15
    //   1579: new 586	java/io/File
    //   1582: dup
    //   1583: aload 14
    //   1585: invokevirtual 889	android/net/Uri:getPath	()Ljava/lang/String;
    //   1588: invokespecial 890	java/io/File:<init>	(Ljava/lang/String;)V
    //   1591: astore 14
    //   1593: aload 13
    //   1595: astore 16
    //   1597: aload 13
    //   1599: astore 17
    //   1601: aload 13
    //   1603: astore 15
    //   1605: aload 14
    //   1607: invokevirtual 840	java/io/File:delete	()Z
    //   1610: pop
    //   1611: aload 18
    //   1613: astore 15
    //   1615: aload_1
    //   1616: astore 14
    //   1618: goto +158 -> 1776
    //   1621: aload 13
    //   1623: astore 16
    //   1625: aload 13
    //   1627: astore 17
    //   1629: aload 13
    //   1631: astore 15
    //   1633: aload 20
    //   1635: getfield 1900	com/truecaller/messaging/data/types/VideoEntity:b	Landroid/net/Uri;
    //   1638: astore 18
    //   1640: aload 13
    //   1642: astore 16
    //   1644: aload 13
    //   1646: astore 17
    //   1648: aload 13
    //   1650: astore 15
    //   1652: aload 18
    //   1654: ldc_w 1902
    //   1657: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1660: aload 13
    //   1662: astore 16
    //   1664: aload 13
    //   1666: astore 17
    //   1668: aload 13
    //   1670: astore 15
    //   1672: new 586	java/io/File
    //   1675: dup
    //   1676: aload 18
    //   1678: invokevirtual 889	android/net/Uri:getPath	()Ljava/lang/String;
    //   1681: invokespecial 890	java/io/File:<init>	(Ljava/lang/String;)V
    //   1684: invokevirtual 840	java/io/File:delete	()Z
    //   1687: pop
    //   1688: aload 13
    //   1690: astore 16
    //   1692: aload 13
    //   1694: astore 17
    //   1696: aload 13
    //   1698: astore 15
    //   1700: aload 14
    //   1702: athrow
    //   1703: astore 14
    //   1705: aload 16
    //   1707: astore 13
    //   1709: goto +762 -> 2471
    //   1712: astore 14
    //   1714: aload 17
    //   1716: astore 13
    //   1718: goto +797 -> 2515
    //   1721: astore 16
    //   1723: aload_1
    //   1724: astore 14
    //   1726: aload 15
    //   1728: astore 13
    //   1730: aload 16
    //   1732: astore 15
    //   1734: goto +94 -> 1828
    //   1737: aload 13
    //   1739: getfield 141	com/truecaller/messaging/transport/im/bd:m	Lcom/truecaller/messaging/transport/im/f;
    //   1742: astore 16
    //   1744: aload 18
    //   1746: ldc_w 1863
    //   1749: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1752: aload_1
    //   1753: astore 14
    //   1755: aload 14
    //   1757: astore 15
    //   1759: aload 16
    //   1761: aload 18
    //   1763: aload 22
    //   1765: aload 14
    //   1767: invokeinterface 1871 4 0
    //   1772: aload 18
    //   1774: astore 15
    //   1776: aload 15
    //   1778: astore 18
    //   1780: goto -1149 -> 631
    //   1783: astore 14
    //   1785: goto +31 -> 1816
    //   1788: astore 14
    //   1790: goto +681 -> 2471
    //   1793: astore 14
    //   1795: goto +720 -> 2515
    //   1798: astore 14
    //   1800: aload_1
    //   1801: astore 15
    //   1803: goto +13 -> 1816
    //   1806: astore 16
    //   1808: aload 14
    //   1810: astore 15
    //   1812: aload 16
    //   1814: astore 14
    //   1816: aload 14
    //   1818: astore 16
    //   1820: aload 15
    //   1822: astore 14
    //   1824: aload 16
    //   1826: astore 15
    //   1828: goto +757 -> 2585
    //   1831: aload 21
    //   1833: checkcast 334	java/util/Collection
    //   1836: invokeinterface 873 1 0
    //   1841: istore 8
    //   1843: iload 8
    //   1845: iconst_1
    //   1846: ixor
    //   1847: ifeq +1211 -> 3058
    //   1850: aload 14
    //   1852: getfield 820	com/truecaller/messaging/data/types/Message:n	[Lcom/truecaller/messaging/data/types/Entity;
    //   1855: astore 16
    //   1857: aload 16
    //   1859: ldc_w 822
    //   1862: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1865: new 329	java/util/ArrayList
    //   1868: dup
    //   1869: invokespecial 424	java/util/ArrayList:<init>	()V
    //   1872: checkcast 334	java/util/Collection
    //   1875: astore 17
    //   1877: aload 16
    //   1879: arraylength
    //   1880: istore 5
    //   1882: iconst_0
    //   1883: istore 4
    //   1885: iload 4
    //   1887: iload 5
    //   1889: if_icmpge +57 -> 1946
    //   1892: aload 16
    //   1894: iload 4
    //   1896: aaload
    //   1897: astore 19
    //   1899: aload 14
    //   1901: astore 15
    //   1903: aload 19
    //   1905: ldc_w 1346
    //   1908: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1911: aload 14
    //   1913: astore 15
    //   1915: aload 19
    //   1917: invokevirtual 1906	com/truecaller/messaging/data/types/Entity:c	()Z
    //   1920: ifne +17 -> 1937
    //   1923: aload 14
    //   1925: astore 15
    //   1927: aload 17
    //   1929: aload 19
    //   1931: invokeinterface 419 2 0
    //   1936: pop
    //   1937: iload 4
    //   1939: iconst_1
    //   1940: iadd
    //   1941: istore 4
    //   1943: goto -58 -> 1885
    //   1946: aload 21
    //   1948: aload 17
    //   1950: checkcast 421	java/util/List
    //   1953: checkcast 334	java/util/Collection
    //   1956: invokeinterface 1907 2 0
    //   1961: pop
    //   1962: aload_1
    //   1963: invokevirtual 1666	com/truecaller/messaging/data/types/Message:m	()Lcom/truecaller/messaging/data/types/Message$a;
    //   1966: invokevirtual 1668	com/truecaller/messaging/data/types/Message$a:a	()Lcom/truecaller/messaging/data/types/Message$a;
    //   1969: aload 21
    //   1971: checkcast 334	java/util/Collection
    //   1974: invokevirtual 1910	com/truecaller/messaging/data/types/Message$a:a	(Ljava/util/Collection;)Lcom/truecaller/messaging/data/types/Message$a;
    //   1977: invokevirtual 247	com/truecaller/messaging/data/types/Message$a:b	()Lcom/truecaller/messaging/data/types/Message;
    //   1980: astore 15
    //   1982: aload 15
    //   1984: ldc_w 1912
    //   1987: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1990: new 211	com/truecaller/messaging/transport/im/ImTransportInfo$a
    //   1993: dup
    //   1994: invokespecial 893	com/truecaller/messaging/transport/im/ImTransportInfo$a:<init>	()V
    //   1997: astore 16
    //   1999: aload 16
    //   2001: bipush 6
    //   2003: putfield 228	com/truecaller/messaging/transport/im/ImTransportInfo$a:g	I
    //   2006: aload 16
    //   2008: invokevirtual 217	com/truecaller/messaging/transport/im/ImTransportInfo$a:a	()Lcom/truecaller/messaging/transport/im/ImTransportInfo;
    //   2011: astore 16
    //   2013: aload 13
    //   2015: getfield 123	com/truecaller/messaging/transport/im/bd:d	Lcom/truecaller/androidactors/f;
    //   2018: invokeinterface 256 1 0
    //   2023: checkcast 258	com/truecaller/messaging/data/t
    //   2026: aload 15
    //   2028: invokevirtual 1666	com/truecaller/messaging/data/types/Message:m	()Lcom/truecaller/messaging/data/types/Message$a;
    //   2031: iconst_2
    //   2032: aload 16
    //   2034: checkcast 241	com/truecaller/messaging/data/types/TransportInfo
    //   2037: invokevirtual 244	com/truecaller/messaging/data/types/Message$a:a	(ILcom/truecaller/messaging/data/types/TransportInfo;)Lcom/truecaller/messaging/data/types/Message$a;
    //   2040: invokevirtual 247	com/truecaller/messaging/data/types/Message$a:b	()Lcom/truecaller/messaging/data/types/Message;
    //   2043: invokeinterface 261 2 0
    //   2048: goto +3 -> 2051
    //   2051: iload_2
    //   2052: ifeq +10 -> 2062
    //   2055: getstatic 1845	com/truecaller/messaging/transport/k$b:a	Lcom/truecaller/messaging/transport/k$b;
    //   2058: checkcast 1828	com/truecaller/messaging/transport/k
    //   2061: areturn
    //   2062: iload_3
    //   2063: ifeq +14 -> 2077
    //   2066: getstatic 1917	com/truecaller/messaging/transport/k$a:a	Lcom/truecaller/messaging/transport/k$a;
    //   2069: checkcast 1828	com/truecaller/messaging/transport/k
    //   2072: astore 15
    //   2074: aload 15
    //   2076: areturn
    //   2077: invokestatic 1922	com/truecaller/api/services/messenger/v1/p$b:a	()Lcom/truecaller/api/services/messenger/v1/p$b$a;
    //   2080: aload 18
    //   2082: invokevirtual 1925	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c:build	()Lcom/google/f/q;
    //   2085: checkcast 1847	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent
    //   2088: invokevirtual 1930	com/truecaller/api/services/messenger/v1/p$b$a:a	(Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent;)Lcom/truecaller/api/services/messenger/v1/p$b$a;
    //   2091: astore 16
    //   2093: aload 14
    //   2095: getfield 1366	com/truecaller/messaging/data/types/Message:c	Lcom/truecaller/messaging/data/types/Participant;
    //   2098: astore 15
    //   2100: aload 15
    //   2102: ldc_w 1550
    //   2105: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   2108: aload 15
    //   2110: getfield 1315	com/truecaller/messaging/data/types/Participant:c	I
    //   2113: istore_2
    //   2114: iload_2
    //   2115: iconst_4
    //   2116: if_icmpne +43 -> 2159
    //   2119: invokestatic 1935	com/truecaller/api/services/messenger/v1/models/input/InputPeer:a	()Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer$a;
    //   2122: invokestatic 1940	com/truecaller/api/services/messenger/v1/models/input/InputPeer$b:a	()Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer$b$a;
    //   2125: aload 15
    //   2127: getfield 1318	com/truecaller/messaging/data/types/Participant:f	Ljava/lang/String;
    //   2130: invokevirtual 1945	com/truecaller/api/services/messenger/v1/models/input/InputPeer$b$a:a	(Ljava/lang/String;)Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer$b$a;
    //   2133: invokevirtual 1950	com/truecaller/api/services/messenger/v1/models/input/InputPeer$a:a	(Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer$b$a;)Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer$a;
    //   2136: invokevirtual 1951	com/truecaller/api/services/messenger/v1/models/input/InputPeer$a:build	()Lcom/google/f/q;
    //   2139: astore 15
    //   2141: aload 15
    //   2143: ldc_w 1953
    //   2146: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   2149: aload 15
    //   2151: checkcast 1932	com/truecaller/api/services/messenger/v1/models/input/InputPeer
    //   2154: astore 15
    //   2156: goto +40 -> 2196
    //   2159: invokestatic 1935	com/truecaller/api/services/messenger/v1/models/input/InputPeer:a	()Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer$a;
    //   2162: invokestatic 1958	com/truecaller/api/services/messenger/v1/models/input/InputPeer$d:a	()Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer$d$a;
    //   2165: aload 15
    //   2167: getfield 1316	com/truecaller/messaging/data/types/Participant:d	Ljava/lang/String;
    //   2170: invokevirtual 1963	com/truecaller/api/services/messenger/v1/models/input/InputPeer$d$a:a	(Ljava/lang/String;)Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer$d$a;
    //   2173: invokevirtual 1966	com/truecaller/api/services/messenger/v1/models/input/InputPeer$a:a	(Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer$d$a;)Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer$a;
    //   2176: invokevirtual 1951	com/truecaller/api/services/messenger/v1/models/input/InputPeer$a:build	()Lcom/google/f/q;
    //   2179: astore 15
    //   2181: aload 15
    //   2183: ldc_w 1968
    //   2186: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   2189: aload 15
    //   2191: checkcast 1932	com/truecaller/api/services/messenger/v1/models/input/InputPeer
    //   2194: astore 15
    //   2196: aload 16
    //   2198: aload 15
    //   2200: invokevirtual 1971	com/truecaller/api/services/messenger/v1/p$b$a:a	(Lcom/truecaller/api/services/messenger/v1/models/input/InputPeer;)Lcom/truecaller/api/services/messenger/v1/p$b$a;
    //   2203: aload_1
    //   2204: invokevirtual 810	com/truecaller/messaging/data/types/Message:k	()Lcom/truecaller/messaging/data/types/TransportInfo;
    //   2207: checkcast 194	com/truecaller/messaging/transport/im/ImTransportInfo
    //   2210: getfield 1972	com/truecaller/messaging/transport/im/ImTransportInfo:j	J
    //   2213: invokevirtual 1975	com/truecaller/api/services/messenger/v1/p$b$a:a	(J)Lcom/truecaller/api/services/messenger/v1/p$b$a;
    //   2216: invokevirtual 1976	com/truecaller/api/services/messenger/v1/p$b$a:build	()Lcom/google/f/q;
    //   2219: checkcast 1919	com/truecaller/api/services/messenger/v1/p$b
    //   2222: astore 15
    //   2224: aload 13
    //   2226: getfield 121	com/truecaller/messaging/transport/im/bd:c	Lcom/truecaller/messaging/transport/im/cb;
    //   2229: invokestatic 1981	com/truecaller/network/d/j$a:a	(Lcom/truecaller/network/d/j;)Lio/grpc/c/a;
    //   2232: checkcast 1983	com/truecaller/api/services/messenger/v1/k$a
    //   2235: astore 16
    //   2237: aload 16
    //   2239: ifnull +190 -> 2429
    //   2242: aload 16
    //   2244: aload 15
    //   2246: invokevirtual 1986	com/truecaller/api/services/messenger/v1/k$a:a	(Lcom/truecaller/api/services/messenger/v1/p$b;)Lcom/truecaller/api/services/messenger/v1/p$d;
    //   2249: astore 15
    //   2251: aload 15
    //   2253: ifnonnull +6 -> 2259
    //   2256: goto +173 -> 2429
    //   2259: invokestatic 1991	com/truecaller/tracking/events/x:b	()Lcom/truecaller/tracking/events/x$a;
    //   2262: aload 15
    //   2264: invokevirtual 1994	com/truecaller/api/services/messenger/v1/p$d:a	()Ljava/lang/String;
    //   2267: checkcast 264	java/lang/CharSequence
    //   2270: invokevirtual 1999	com/truecaller/tracking/events/x$a:a	(Ljava/lang/CharSequence;)Lcom/truecaller/tracking/events/x$a;
    //   2273: aload 14
    //   2275: getfield 1594	com/truecaller/messaging/data/types/Message:o	Ljava/lang/String;
    //   2278: checkcast 264	java/lang/CharSequence
    //   2281: invokevirtual 2001	com/truecaller/tracking/events/x$a:b	(Ljava/lang/CharSequence;)Lcom/truecaller/tracking/events/x$a;
    //   2284: invokevirtual 2004	com/truecaller/tracking/events/x$a:a	()Lcom/truecaller/tracking/events/x;
    //   2287: astore 16
    //   2289: aload 13
    //   2291: getfield 137	com/truecaller/messaging/transport/im/bd:k	Lcom/truecaller/androidactors/f;
    //   2294: invokeinterface 256 1 0
    //   2299: checkcast 302	com/truecaller/analytics/ae
    //   2302: aload 16
    //   2304: checkcast 323	org/apache/a/d/d
    //   2307: invokeinterface 326 2 0
    //   2312: new 211	com/truecaller/messaging/transport/im/ImTransportInfo$a
    //   2315: dup
    //   2316: invokespecial 893	com/truecaller/messaging/transport/im/ImTransportInfo$a:<init>	()V
    //   2319: astore 16
    //   2321: aload 15
    //   2323: invokevirtual 1994	com/truecaller/api/services/messenger/v1/p$d:a	()Ljava/lang/String;
    //   2326: astore 15
    //   2328: aload 15
    //   2330: ldc_w 2006
    //   2333: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   2336: aload 16
    //   2338: aload 15
    //   2340: invokevirtual 1163	com/truecaller/messaging/transport/im/ImTransportInfo$a:a	(Ljava/lang/String;)Lcom/truecaller/messaging/transport/im/ImTransportInfo$a;
    //   2343: astore 15
    //   2345: aload 15
    //   2347: aload_1
    //   2348: invokevirtual 826	com/truecaller/messaging/data/types/Message:a	()J
    //   2351: putfield 894	com/truecaller/messaging/transport/im/ImTransportInfo$a:a	J
    //   2354: aload 15
    //   2356: aload_1
    //   2357: invokestatic 907	com/truecaller/messaging/transport/im/bf:a	(Lcom/truecaller/messaging/data/types/Message;)I
    //   2360: putfield 909	com/truecaller/messaging/transport/im/ImTransportInfo$a:b	I
    //   2363: aload 15
    //   2365: iconst_2
    //   2366: putfield 1165	com/truecaller/messaging/transport/im/ImTransportInfo$a:c	I
    //   2369: aload 15
    //   2371: iconst_2
    //   2372: putfield 1167	com/truecaller/messaging/transport/im/ImTransportInfo$a:d	I
    //   2375: aload 15
    //   2377: iconst_1
    //   2378: putfield 228	com/truecaller/messaging/transport/im/ImTransportInfo$a:g	I
    //   2381: aload 15
    //   2383: invokevirtual 217	com/truecaller/messaging/transport/im/ImTransportInfo$a:a	()Lcom/truecaller/messaging/transport/im/ImTransportInfo;
    //   2386: astore 15
    //   2388: aload 13
    //   2390: getfield 123	com/truecaller/messaging/transport/im/bd:d	Lcom/truecaller/androidactors/f;
    //   2393: invokeinterface 256 1 0
    //   2398: checkcast 258	com/truecaller/messaging/data/t
    //   2401: aload_1
    //   2402: invokevirtual 1666	com/truecaller/messaging/data/types/Message:m	()Lcom/truecaller/messaging/data/types/Message$a;
    //   2405: iconst_2
    //   2406: aload 15
    //   2408: checkcast 241	com/truecaller/messaging/data/types/TransportInfo
    //   2411: invokevirtual 244	com/truecaller/messaging/data/types/Message$a:a	(ILcom/truecaller/messaging/data/types/TransportInfo;)Lcom/truecaller/messaging/data/types/Message$a;
    //   2414: invokevirtual 247	com/truecaller/messaging/data/types/Message$a:b	()Lcom/truecaller/messaging/data/types/Message;
    //   2417: invokeinterface 261 2 0
    //   2422: getstatic 2011	com/truecaller/messaging/transport/k$d:a	Lcom/truecaller/messaging/transport/k$d;
    //   2425: checkcast 1828	com/truecaller/messaging/transport/k
    //   2428: areturn
    //   2429: getstatic 1845	com/truecaller/messaging/transport/k$b:a	Lcom/truecaller/messaging/transport/k$b;
    //   2432: checkcast 1828	com/truecaller/messaging/transport/k
    //   2435: astore 15
    //   2437: aload 15
    //   2439: areturn
    //   2440: astore 15
    //   2442: goto +143 -> 2585
    //   2445: astore 15
    //   2447: goto +138 -> 2585
    //   2450: astore 14
    //   2452: goto +19 -> 2471
    //   2455: astore 14
    //   2457: goto +58 -> 2515
    //   2460: astore 15
    //   2462: goto +123 -> 2585
    //   2465: astore 14
    //   2467: aload 15
    //   2469: astore 13
    //   2471: aload 14
    //   2473: checkcast 478	java/lang/Throwable
    //   2476: invokestatic 920	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   2479: aload_1
    //   2480: invokevirtual 810	com/truecaller/messaging/data/types/Message:k	()Lcom/truecaller/messaging/data/types/TransportInfo;
    //   2483: astore_1
    //   2484: aload_1
    //   2485: ldc_w 911
    //   2488: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   2491: aload 13
    //   2493: aload_1
    //   2494: checkcast 194	com/truecaller/messaging/transport/im/ImTransportInfo
    //   2497: aload 14
    //   2499: getfield 1903	com/truecaller/messaging/transport/im/cj:a	I
    //   2502: invokespecial 1905	com/truecaller/messaging/transport/im/bd:a	(Lcom/truecaller/messaging/transport/im/ImTransportInfo;I)Lcom/truecaller/messaging/transport/im/ImTransportInfo;
    //   2505: pop
    //   2506: goto +485 -> 2991
    //   2509: astore 14
    //   2511: aload 16
    //   2513: astore 13
    //   2515: aload 14
    //   2517: checkcast 478	java/lang/Throwable
    //   2520: invokestatic 920	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   2523: aload_1
    //   2524: invokevirtual 810	com/truecaller/messaging/data/types/Message:k	()Lcom/truecaller/messaging/data/types/TransportInfo;
    //   2527: astore_1
    //   2528: aload_1
    //   2529: ldc_w 911
    //   2532: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   2535: aload_1
    //   2536: checkcast 194	com/truecaller/messaging/transport/im/ImTransportInfo
    //   2539: astore_1
    //   2540: getstatic 2016	io/grpc/ba:c	Lio/grpc/ba;
    //   2543: astore 14
    //   2545: aload 14
    //   2547: ldc_w 2018
    //   2550: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   2553: aload 13
    //   2555: aload_1
    //   2556: aload 14
    //   2558: invokevirtual 2021	io/grpc/ba:a	()Lio/grpc/ba$a;
    //   2561: invokevirtual 2024	io/grpc/ba$a:a	()I
    //   2564: sipush 10000
    //   2567: iadd
    //   2568: invokespecial 1905	com/truecaller/messaging/transport/im/bd:a	(Lcom/truecaller/messaging/transport/im/ImTransportInfo;I)Lcom/truecaller/messaging/transport/im/ImTransportInfo;
    //   2571: pop
    //   2572: goto +419 -> 2991
    //   2575: astore 15
    //   2577: aload 20
    //   2579: astore 14
    //   2581: aload 17
    //   2583: astore 13
    //   2585: aload 15
    //   2587: checkcast 478	java/lang/Throwable
    //   2590: invokestatic 920	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   2593: aload 15
    //   2595: invokevirtual 2027	io/grpc/bc:a	()Lio/grpc/ba;
    //   2598: getstatic 2029	io/grpc/ba:a	Lio/grpc/ba;
    //   2601: invokestatic 886	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/Object;)Z
    //   2604: iconst_1
    //   2605: ixor
    //   2606: ifeq +385 -> 2991
    //   2609: aload 15
    //   2611: invokevirtual 2027	io/grpc/bc:a	()Lio/grpc/ba;
    //   2614: astore 16
    //   2616: aload 16
    //   2618: ldc_w 2031
    //   2621: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   2624: aload 16
    //   2626: invokevirtual 2021	io/grpc/ba:a	()Lio/grpc/ba$a;
    //   2629: invokevirtual 2024	io/grpc/ba$a:a	()I
    //   2632: sipush 10000
    //   2635: iadd
    //   2636: istore_2
    //   2637: aload 16
    //   2639: invokevirtual 2021	io/grpc/ba:a	()Lio/grpc/ba$a;
    //   2642: astore 17
    //   2644: aload 17
    //   2646: ifnonnull +6 -> 2652
    //   2649: goto +161 -> 2810
    //   2652: getstatic 2034	com/truecaller/messaging/transport/im/bg:a	[I
    //   2655: aload 17
    //   2657: invokevirtual 2035	io/grpc/ba$a:ordinal	()I
    //   2660: iaload
    //   2661: tableswitch	default:+27->2688, 1:+82->2743, 2:+36->2697, 3:+30->2691
    //   2688: goto +122 -> 2810
    //   2691: bipush 7
    //   2693: istore_2
    //   2694: goto +116 -> 2810
    //   2697: aload 16
    //   2699: invokevirtual 2036	io/grpc/ba:b	()Ljava/lang/String;
    //   2702: astore 16
    //   2704: aload 16
    //   2706: ifnonnull +6 -> 2712
    //   2709: goto +101 -> 2810
    //   2712: aload 16
    //   2714: invokevirtual 790	java/lang/String:hashCode	()I
    //   2717: ldc_w 2037
    //   2720: if_icmpeq +6 -> 2726
    //   2723: goto +87 -> 2810
    //   2726: aload 16
    //   2728: ldc_w 2039
    //   2731: invokevirtual 797	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   2734: ifeq +76 -> 2810
    //   2737: bipush 6
    //   2739: istore_2
    //   2740: goto +70 -> 2810
    //   2743: aload 16
    //   2745: invokevirtual 2036	io/grpc/ba:b	()Ljava/lang/String;
    //   2748: astore 16
    //   2750: aload 16
    //   2752: ifnonnull +6 -> 2758
    //   2755: goto +55 -> 2810
    //   2758: aload 16
    //   2760: invokevirtual 790	java/lang/String:hashCode	()I
    //   2763: istore_3
    //   2764: iload_3
    //   2765: ldc_w 2040
    //   2768: if_icmpeq +29 -> 2797
    //   2771: iload_3
    //   2772: ldc_w 2041
    //   2775: if_icmpeq +6 -> 2781
    //   2778: goto +32 -> 2810
    //   2781: aload 16
    //   2783: ldc_w 2043
    //   2786: invokevirtual 797	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   2789: ifeq +21 -> 2810
    //   2792: iconst_5
    //   2793: istore_2
    //   2794: goto +16 -> 2810
    //   2797: aload 16
    //   2799: ldc_w 2045
    //   2802: invokevirtual 797	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   2805: ifeq +5 -> 2810
    //   2808: iconst_4
    //   2809: istore_2
    //   2810: aload_1
    //   2811: invokevirtual 810	com/truecaller/messaging/data/types/Message:k	()Lcom/truecaller/messaging/data/types/TransportInfo;
    //   2814: astore_1
    //   2815: aload_1
    //   2816: ldc_w 911
    //   2819: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   2822: aload 13
    //   2824: aload_1
    //   2825: checkcast 194	com/truecaller/messaging/transport/im/ImTransportInfo
    //   2828: iload_2
    //   2829: invokespecial 1905	com/truecaller/messaging/transport/im/bd:a	(Lcom/truecaller/messaging/transport/im/ImTransportInfo;I)Lcom/truecaller/messaging/transport/im/ImTransportInfo;
    //   2832: pop
    //   2833: iload_2
    //   2834: bipush 7
    //   2836: if_icmpne +92 -> 2928
    //   2839: aload 13
    //   2841: getfield 145	com/truecaller/messaging/transport/im/bd:o	Lcom/truecaller/androidactors/f;
    //   2844: invokeinterface 256 1 0
    //   2849: checkcast 1189	com/truecaller/messaging/transport/im/bp
    //   2852: astore_1
    //   2853: aload 14
    //   2855: getfield 1366	com/truecaller/messaging/data/types/Message:c	Lcom/truecaller/messaging/data/types/Participant;
    //   2858: getfield 1318	com/truecaller/messaging/data/types/Participant:f	Ljava/lang/String;
    //   2861: astore 13
    //   2863: aload 13
    //   2865: ldc_w 2047
    //   2868: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   2871: aload_1
    //   2872: aload 13
    //   2874: invokeinterface 2048 2 0
    //   2879: invokevirtual 633	com/truecaller/androidactors/w:d	()Ljava/lang/Object;
    //   2882: checkcast 635	java/lang/Boolean
    //   2885: invokestatic 2053	com/truecaller/utils/extensions/c:a	(Ljava/lang/Boolean;)Z
    //   2888: istore 8
    //   2890: aload 14
    //   2892: getfield 2055	com/truecaller/messaging/data/types/Message:t	I
    //   2895: iconst_2
    //   2896: if_icmpge +32 -> 2928
    //   2899: iload 8
    //   2901: ifeq +27 -> 2928
    //   2904: invokestatic 1702	org/a/a/b:ay_	()Lorg/a/a/b;
    //   2907: astore_1
    //   2908: aload_1
    //   2909: ldc_w 2057
    //   2912: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   2915: new 1823	com/truecaller/messaging/transport/k$c
    //   2918: dup
    //   2919: aload_1
    //   2920: iconst_1
    //   2921: invokespecial 1826	com/truecaller/messaging/transport/k$c:<init>	(Lorg/a/a/b;Z)V
    //   2924: checkcast 1828	com/truecaller/messaging/transport/k
    //   2927: areturn
    //   2928: aload 15
    //   2930: invokevirtual 2027	io/grpc/bc:a	()Lio/grpc/ba;
    //   2933: astore_1
    //   2934: aload_1
    //   2935: ldc_w 2031
    //   2938: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   2941: aload_1
    //   2942: invokestatic 2062	com/truecaller/messaging/i/l:a	(Lio/grpc/ba;)Z
    //   2945: ifeq +46 -> 2991
    //   2948: aload 14
    //   2950: getfield 2055	com/truecaller/messaging/data/types/Message:t	I
    //   2953: iconst_2
    //   2954: if_icmplt +10 -> 2964
    //   2957: getstatic 1845	com/truecaller/messaging/transport/k$b:a	Lcom/truecaller/messaging/transport/k$b;
    //   2960: checkcast 1828	com/truecaller/messaging/transport/k
    //   2963: areturn
    //   2964: invokestatic 1702	org/a/a/b:ay_	()Lorg/a/a/b;
    //   2967: invokevirtual 1819	org/a/a/b:c	()Lorg/a/a/b;
    //   2970: astore_1
    //   2971: aload_1
    //   2972: ldc_w 1821
    //   2975: invokestatic 251	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   2978: new 1823	com/truecaller/messaging/transport/k$c
    //   2981: dup
    //   2982: aload_1
    //   2983: iconst_1
    //   2984: invokespecial 1826	com/truecaller/messaging/transport/k$c:<init>	(Lorg/a/a/b;Z)V
    //   2987: checkcast 1828	com/truecaller/messaging/transport/k
    //   2990: areturn
    //   2991: getstatic 1845	com/truecaller/messaging/transport/k$b:a	Lcom/truecaller/messaging/transport/k$b;
    //   2994: checkcast 1828	com/truecaller/messaging/transport/k
    //   2997: areturn
    //   2998: getstatic 1917	com/truecaller/messaging/transport/k$a:a	Lcom/truecaller/messaging/transport/k$a;
    //   3001: checkcast 1828	com/truecaller/messaging/transport/k
    //   3004: areturn
    //   3005: astore_1
    //   3006: goto -8 -> 2998
    //   3009: astore 15
    //   3011: goto -2668 -> 343
    //   3014: astore 14
    //   3016: goto +34 -> 3050
    //   3019: astore 14
    //   3021: goto -1775 -> 1246
    //   3024: astore 14
    //   3026: goto -1793 -> 1233
    //   3029: astore 13
    //   3031: aload 14
    //   3033: astore 13
    //   3035: goto -1596 -> 1439
    //   3038: astore 13
    //   3040: goto -1826 -> 1214
    //   3043: iload_2
    //   3044: iconst_1
    //   3045: iadd
    //   3046: istore_2
    //   3047: goto -2539 -> 508
    //   3050: goto -1611 -> 1439
    //   3053: astore 15
    //   3055: goto -1227 -> 1828
    //   3058: goto -1007 -> 2051
    //   3061: astore 15
    //   3063: goto -8 -> 3055
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	3066	0	this	bd
    //   0	3066	1	paramMessage	Message
    //   507	2540	2	i1	int
    //   505	2271	3	i2	int
    //   910	1032	4	i3	int
    //   943	947	5	i4	int
    //   966	46	6	i5	int
    //   989	25	7	i6	int
    //   52	2848	8	bool	boolean
    //   876	122	9	l1	long
    //   932	76	11	l2	long
    //   1	2872	13	localObject1	Object
    //   3029	1	13	localCancellationException1	java.util.concurrent.CancellationException
    //   3033	1	13	localObject2	Object
    //   3038	1	13	localCancellationException2	java.util.concurrent.CancellationException
    //   4	1190	14	localObject3	Object
    //   1198	1	14	localObject4	Object
    //   1206	1	14	localcj1	cj
    //   1220	1	14	localIOException1	IOException
    //   1228	1	14	localcj2	cj
    //   1236	1	14	localIOException2	IOException
    //   1241	1	14	localcj3	cj
    //   1249	1	14	localIOException3	IOException
    //   1268	1	14	localObject5	Object
    //   1281	1	14	localcj4	cj
    //   1290	1	14	localIOException4	IOException
    //   1299	1	14	localObject6	Object
    //   1308	48	14	localcj5	cj
    //   1381	127	14	localObject7	Object
    //   1514	7	14	localIOException5	IOException
    //   1545	156	14	localObject8	Object
    //   1703	1	14	localcj6	cj
    //   1712	1	14	localRuntimeException1	RuntimeException
    //   1724	42	14	localMessage	Message
    //   1783	1	14	localbc1	io.grpc.bc
    //   1788	1	14	localcj7	cj
    //   1793	1	14	localRuntimeException2	RuntimeException
    //   1798	11	14	localbc2	io.grpc.bc
    //   1814	460	14	localObject9	Object
    //   2450	1	14	localcj8	cj
    //   2455	1	14	localRuntimeException3	RuntimeException
    //   2465	33	14	localcj9	cj
    //   2509	7	14	localRuntimeException4	RuntimeException
    //   2543	406	14	localObject10	Object
    //   3014	1	14	localCancellationException3	java.util.concurrent.CancellationException
    //   3019	1	14	localCancellationException4	java.util.concurrent.CancellationException
    //   3024	8	14	localCancellationException5	java.util.concurrent.CancellationException
    //   150	2288	15	localObject11	Object
    //   2440	1	15	localbc3	io.grpc.bc
    //   2445	1	15	localbc4	io.grpc.bc
    //   2460	8	15	localbc5	io.grpc.bc
    //   2575	354	15	localbc6	io.grpc.bc
    //   3009	1	15	localInterruptedException	InterruptedException
    //   3053	1	15	localbc7	io.grpc.bc
    //   3061	1	15	localbc8	io.grpc.bc
    //   154	1552	16	localObject12	Object
    //   1721	10	16	localbc9	io.grpc.bc
    //   1742	18	16	localf	f
    //   1806	7	16	localbc10	io.grpc.bc
    //   1818	980	16	localObject13	Object
    //   162	2494	17	localObject14	Object
    //   182	1899	18	localObject15	Object
    //   272	1658	19	localObject16	Object
    //   170	2408	20	localObject17	Object
    //   377	1593	21	localList	List
    //   476	1288	22	localObject18	Object
    //   517	605	23	localObject19	Object
    //   887	113	24	str	String
    // Exception table:
    //   from	to	target	type
    //   991	1032	1198	finally
    //   991	1032	1206	com/truecaller/messaging/transport/im/cj
    //   991	1032	1220	java/io/IOException
    //   927	934	1228	com/truecaller/messaging/transport/im/cj
    //   938	945	1228	com/truecaller/messaging/transport/im/cj
    //   927	934	1236	java/io/IOException
    //   938	945	1236	java/io/IOException
    //   916	923	1241	com/truecaller/messaging/transport/im/cj
    //   916	923	1249	java/io/IOException
    //   961	968	1281	com/truecaller/messaging/transport/im/cj
    //   984	991	1281	com/truecaller/messaging/transport/im/cj
    //   1051	1058	1281	com/truecaller/messaging/transport/im/cj
    //   1074	1081	1281	com/truecaller/messaging/transport/im/cj
    //   1097	1105	1281	com/truecaller/messaging/transport/im/cj
    //   1121	1130	1281	com/truecaller/messaging/transport/im/cj
    //   1270	1281	1281	com/truecaller/messaging/transport/im/cj
    //   961	968	1290	java/io/IOException
    //   984	991	1290	java/io/IOException
    //   1051	1058	1290	java/io/IOException
    //   1074	1081	1290	java/io/IOException
    //   1097	1105	1290	java/io/IOException
    //   1121	1130	1290	java/io/IOException
    //   1270	1281	1290	java/io/IOException
    //   795	802	1299	finally
    //   806	814	1299	finally
    //   818	834	1299	finally
    //   843	855	1299	finally
    //   859	867	1299	finally
    //   871	878	1299	finally
    //   882	889	1299	finally
    //   893	901	1299	finally
    //   905	912	1299	finally
    //   916	923	1299	finally
    //   927	934	1299	finally
    //   938	945	1299	finally
    //   961	968	1299	finally
    //   984	991	1299	finally
    //   1051	1058	1299	finally
    //   1074	1081	1299	finally
    //   1097	1105	1299	finally
    //   1121	1130	1299	finally
    //   1270	1281	1299	finally
    //   1314	1322	1299	finally
    //   1326	1332	1299	finally
    //   1336	1344	1299	finally
    //   1348	1364	1299	finally
    //   1520	1528	1299	finally
    //   795	802	1308	com/truecaller/messaging/transport/im/cj
    //   806	814	1308	com/truecaller/messaging/transport/im/cj
    //   818	834	1308	com/truecaller/messaging/transport/im/cj
    //   843	855	1308	com/truecaller/messaging/transport/im/cj
    //   859	867	1308	com/truecaller/messaging/transport/im/cj
    //   871	878	1308	com/truecaller/messaging/transport/im/cj
    //   882	889	1308	com/truecaller/messaging/transport/im/cj
    //   893	901	1308	com/truecaller/messaging/transport/im/cj
    //   905	912	1308	com/truecaller/messaging/transport/im/cj
    //   795	802	1514	java/io/IOException
    //   806	814	1514	java/io/IOException
    //   818	834	1514	java/io/IOException
    //   843	855	1514	java/io/IOException
    //   859	867	1514	java/io/IOException
    //   871	878	1514	java/io/IOException
    //   882	889	1514	java/io/IOException
    //   893	901	1514	java/io/IOException
    //   905	912	1514	java/io/IOException
    //   745	753	1703	com/truecaller/messaging/transport/im/cj
    //   765	779	1703	com/truecaller/messaging/transport/im/cj
    //   1142	1149	1703	com/truecaller/messaging/transport/im/cj
    //   1161	1169	1703	com/truecaller/messaging/transport/im/cj
    //   1181	1195	1703	com/truecaller/messaging/transport/im/cj
    //   1376	1383	1703	com/truecaller/messaging/transport/im/cj
    //   1395	1403	1703	com/truecaller/messaging/transport/im/cj
    //   1415	1431	1703	com/truecaller/messaging/transport/im/cj
    //   1451	1458	1703	com/truecaller/messaging/transport/im/cj
    //   1470	1478	1703	com/truecaller/messaging/transport/im/cj
    //   1490	1506	1703	com/truecaller/messaging/transport/im/cj
    //   1540	1547	1703	com/truecaller/messaging/transport/im/cj
    //   1559	1567	1703	com/truecaller/messaging/transport/im/cj
    //   1579	1593	1703	com/truecaller/messaging/transport/im/cj
    //   1605	1611	1703	com/truecaller/messaging/transport/im/cj
    //   1633	1640	1703	com/truecaller/messaging/transport/im/cj
    //   1652	1660	1703	com/truecaller/messaging/transport/im/cj
    //   1672	1688	1703	com/truecaller/messaging/transport/im/cj
    //   1700	1703	1703	com/truecaller/messaging/transport/im/cj
    //   745	753	1712	java/lang/RuntimeException
    //   765	779	1712	java/lang/RuntimeException
    //   1142	1149	1712	java/lang/RuntimeException
    //   1161	1169	1712	java/lang/RuntimeException
    //   1181	1195	1712	java/lang/RuntimeException
    //   1376	1383	1712	java/lang/RuntimeException
    //   1395	1403	1712	java/lang/RuntimeException
    //   1415	1431	1712	java/lang/RuntimeException
    //   1451	1458	1712	java/lang/RuntimeException
    //   1470	1478	1712	java/lang/RuntimeException
    //   1490	1506	1712	java/lang/RuntimeException
    //   1540	1547	1712	java/lang/RuntimeException
    //   1559	1567	1712	java/lang/RuntimeException
    //   1579	1593	1712	java/lang/RuntimeException
    //   1605	1611	1712	java/lang/RuntimeException
    //   1633	1640	1712	java/lang/RuntimeException
    //   1652	1660	1712	java/lang/RuntimeException
    //   1672	1688	1712	java/lang/RuntimeException
    //   1700	1703	1712	java/lang/RuntimeException
    //   745	753	1721	io/grpc/bc
    //   765	779	1721	io/grpc/bc
    //   1142	1149	1721	io/grpc/bc
    //   1161	1169	1721	io/grpc/bc
    //   1181	1195	1721	io/grpc/bc
    //   1376	1383	1721	io/grpc/bc
    //   1395	1403	1721	io/grpc/bc
    //   1415	1431	1721	io/grpc/bc
    //   1451	1458	1721	io/grpc/bc
    //   1470	1478	1721	io/grpc/bc
    //   1490	1506	1721	io/grpc/bc
    //   1540	1547	1721	io/grpc/bc
    //   1559	1567	1721	io/grpc/bc
    //   1579	1593	1721	io/grpc/bc
    //   1605	1611	1721	io/grpc/bc
    //   1633	1640	1721	io/grpc/bc
    //   1652	1660	1721	io/grpc/bc
    //   1672	1688	1721	io/grpc/bc
    //   1700	1703	1721	io/grpc/bc
    //   1759	1772	1783	io/grpc/bc
    //   1903	1911	1783	io/grpc/bc
    //   1915	1923	1783	io/grpc/bc
    //   1927	1937	1783	io/grpc/bc
    //   1737	1744	1788	com/truecaller/messaging/transport/im/cj
    //   1744	1752	1788	com/truecaller/messaging/transport/im/cj
    //   1737	1744	1793	java/lang/RuntimeException
    //   1744	1752	1793	java/lang/RuntimeException
    //   1737	1744	1798	io/grpc/bc
    //   1744	1752	1798	io/grpc/bc
    //   685	697	1806	io/grpc/bc
    //   713	728	1806	io/grpc/bc
    //   2119	2156	2440	io/grpc/bc
    //   2159	2196	2440	io/grpc/bc
    //   2196	2237	2440	io/grpc/bc
    //   2242	2251	2440	io/grpc/bc
    //   2259	2429	2440	io/grpc/bc
    //   2429	2437	2440	io/grpc/bc
    //   2077	2114	2445	io/grpc/bc
    //   1759	1772	2450	com/truecaller/messaging/transport/im/cj
    //   1831	1843	2450	com/truecaller/messaging/transport/im/cj
    //   1850	1882	2450	com/truecaller/messaging/transport/im/cj
    //   1903	1911	2450	com/truecaller/messaging/transport/im/cj
    //   1915	1923	2450	com/truecaller/messaging/transport/im/cj
    //   1927	1937	2450	com/truecaller/messaging/transport/im/cj
    //   1946	1999	2450	com/truecaller/messaging/transport/im/cj
    //   1999	2048	2450	com/truecaller/messaging/transport/im/cj
    //   2055	2062	2450	com/truecaller/messaging/transport/im/cj
    //   2066	2074	2450	com/truecaller/messaging/transport/im/cj
    //   2077	2114	2450	com/truecaller/messaging/transport/im/cj
    //   2119	2156	2450	com/truecaller/messaging/transport/im/cj
    //   2159	2196	2450	com/truecaller/messaging/transport/im/cj
    //   2196	2237	2450	com/truecaller/messaging/transport/im/cj
    //   2242	2251	2450	com/truecaller/messaging/transport/im/cj
    //   2259	2429	2450	com/truecaller/messaging/transport/im/cj
    //   2429	2437	2450	com/truecaller/messaging/transport/im/cj
    //   1759	1772	2455	java/lang/RuntimeException
    //   1831	1843	2455	java/lang/RuntimeException
    //   1850	1882	2455	java/lang/RuntimeException
    //   1903	1911	2455	java/lang/RuntimeException
    //   1915	1923	2455	java/lang/RuntimeException
    //   1927	1937	2455	java/lang/RuntimeException
    //   1946	1999	2455	java/lang/RuntimeException
    //   1999	2048	2455	java/lang/RuntimeException
    //   2055	2062	2455	java/lang/RuntimeException
    //   2066	2074	2455	java/lang/RuntimeException
    //   2077	2114	2455	java/lang/RuntimeException
    //   2119	2156	2455	java/lang/RuntimeException
    //   2159	2196	2455	java/lang/RuntimeException
    //   2196	2237	2455	java/lang/RuntimeException
    //   2242	2251	2455	java/lang/RuntimeException
    //   2259	2429	2455	java/lang/RuntimeException
    //   2429	2437	2455	java/lang/RuntimeException
    //   1831	1843	2460	io/grpc/bc
    //   172	184	2465	com/truecaller/messaging/transport/im/cj
    //   208	214	2465	com/truecaller/messaging/transport/im/cj
    //   243	274	2465	com/truecaller/messaging/transport/im/cj
    //   303	311	2465	com/truecaller/messaging/transport/im/cj
    //   335	343	2465	com/truecaller/messaging/transport/im/cj
    //   367	379	2465	com/truecaller/messaging/transport/im/cj
    //   403	410	2465	com/truecaller/messaging/transport/im/cj
    //   434	442	2465	com/truecaller/messaging/transport/im/cj
    //   466	478	2465	com/truecaller/messaging/transport/im/cj
    //   502	506	2465	com/truecaller/messaging/transport/im/cj
    //   543	551	2465	com/truecaller/messaging/transport/im/cj
    //   575	585	2465	com/truecaller/messaging/transport/im/cj
    //   612	627	2465	com/truecaller/messaging/transport/im/cj
    //   655	664	2465	com/truecaller/messaging/transport/im/cj
    //   685	697	2465	com/truecaller/messaging/transport/im/cj
    //   713	728	2465	com/truecaller/messaging/transport/im/cj
    //   172	184	2509	java/lang/RuntimeException
    //   208	214	2509	java/lang/RuntimeException
    //   243	274	2509	java/lang/RuntimeException
    //   303	311	2509	java/lang/RuntimeException
    //   335	343	2509	java/lang/RuntimeException
    //   367	379	2509	java/lang/RuntimeException
    //   403	410	2509	java/lang/RuntimeException
    //   434	442	2509	java/lang/RuntimeException
    //   466	478	2509	java/lang/RuntimeException
    //   502	506	2509	java/lang/RuntimeException
    //   543	551	2509	java/lang/RuntimeException
    //   575	585	2509	java/lang/RuntimeException
    //   612	627	2509	java/lang/RuntimeException
    //   655	664	2509	java/lang/RuntimeException
    //   685	697	2509	java/lang/RuntimeException
    //   713	728	2509	java/lang/RuntimeException
    //   172	184	2575	io/grpc/bc
    //   208	214	2575	io/grpc/bc
    //   243	274	2575	io/grpc/bc
    //   303	311	2575	io/grpc/bc
    //   335	343	2575	io/grpc/bc
    //   367	379	2575	io/grpc/bc
    //   403	410	2575	io/grpc/bc
    //   434	442	2575	io/grpc/bc
    //   466	478	2575	io/grpc/bc
    //   502	506	2575	io/grpc/bc
    //   543	551	2575	io/grpc/bc
    //   575	585	2575	io/grpc/bc
    //   612	627	2575	io/grpc/bc
    //   655	664	2575	io/grpc/bc
    //   172	184	3005	java/util/concurrent/CancellationException
    //   208	214	3005	java/util/concurrent/CancellationException
    //   243	274	3005	java/util/concurrent/CancellationException
    //   303	311	3005	java/util/concurrent/CancellationException
    //   335	343	3005	java/util/concurrent/CancellationException
    //   367	379	3005	java/util/concurrent/CancellationException
    //   403	410	3005	java/util/concurrent/CancellationException
    //   434	442	3005	java/util/concurrent/CancellationException
    //   466	478	3005	java/util/concurrent/CancellationException
    //   502	506	3005	java/util/concurrent/CancellationException
    //   543	551	3005	java/util/concurrent/CancellationException
    //   575	585	3005	java/util/concurrent/CancellationException
    //   612	627	3005	java/util/concurrent/CancellationException
    //   655	664	3005	java/util/concurrent/CancellationException
    //   685	697	3005	java/util/concurrent/CancellationException
    //   713	728	3005	java/util/concurrent/CancellationException
    //   745	753	3005	java/util/concurrent/CancellationException
    //   765	779	3005	java/util/concurrent/CancellationException
    //   1142	1149	3005	java/util/concurrent/CancellationException
    //   1161	1169	3005	java/util/concurrent/CancellationException
    //   1181	1195	3005	java/util/concurrent/CancellationException
    //   1376	1383	3005	java/util/concurrent/CancellationException
    //   1395	1403	3005	java/util/concurrent/CancellationException
    //   1415	1431	3005	java/util/concurrent/CancellationException
    //   1451	1458	3005	java/util/concurrent/CancellationException
    //   1470	1478	3005	java/util/concurrent/CancellationException
    //   1490	1506	3005	java/util/concurrent/CancellationException
    //   1540	1547	3005	java/util/concurrent/CancellationException
    //   1559	1567	3005	java/util/concurrent/CancellationException
    //   1579	1593	3005	java/util/concurrent/CancellationException
    //   1605	1611	3005	java/util/concurrent/CancellationException
    //   1633	1640	3005	java/util/concurrent/CancellationException
    //   1652	1660	3005	java/util/concurrent/CancellationException
    //   1672	1688	3005	java/util/concurrent/CancellationException
    //   1700	1703	3005	java/util/concurrent/CancellationException
    //   1737	1744	3005	java/util/concurrent/CancellationException
    //   1744	1752	3005	java/util/concurrent/CancellationException
    //   1759	1772	3005	java/util/concurrent/CancellationException
    //   1831	1843	3005	java/util/concurrent/CancellationException
    //   1850	1882	3005	java/util/concurrent/CancellationException
    //   1903	1911	3005	java/util/concurrent/CancellationException
    //   1915	1923	3005	java/util/concurrent/CancellationException
    //   1927	1937	3005	java/util/concurrent/CancellationException
    //   1946	1999	3005	java/util/concurrent/CancellationException
    //   1999	2048	3005	java/util/concurrent/CancellationException
    //   2055	2062	3005	java/util/concurrent/CancellationException
    //   2066	2074	3005	java/util/concurrent/CancellationException
    //   2077	2114	3005	java/util/concurrent/CancellationException
    //   2119	2156	3005	java/util/concurrent/CancellationException
    //   2159	2196	3005	java/util/concurrent/CancellationException
    //   2196	2237	3005	java/util/concurrent/CancellationException
    //   2242	2251	3005	java/util/concurrent/CancellationException
    //   2259	2429	3005	java/util/concurrent/CancellationException
    //   2429	2437	3005	java/util/concurrent/CancellationException
    //   243	274	3009	java/lang/InterruptedException
    //   303	311	3009	java/lang/InterruptedException
    //   335	343	3009	java/lang/InterruptedException
    //   795	802	3014	java/util/concurrent/CancellationException
    //   806	814	3014	java/util/concurrent/CancellationException
    //   818	834	3014	java/util/concurrent/CancellationException
    //   843	855	3014	java/util/concurrent/CancellationException
    //   859	867	3014	java/util/concurrent/CancellationException
    //   871	878	3014	java/util/concurrent/CancellationException
    //   882	889	3014	java/util/concurrent/CancellationException
    //   893	901	3014	java/util/concurrent/CancellationException
    //   905	912	3014	java/util/concurrent/CancellationException
    //   916	923	3019	java/util/concurrent/CancellationException
    //   927	934	3024	java/util/concurrent/CancellationException
    //   938	945	3024	java/util/concurrent/CancellationException
    //   961	968	3029	java/util/concurrent/CancellationException
    //   984	991	3029	java/util/concurrent/CancellationException
    //   1051	1058	3029	java/util/concurrent/CancellationException
    //   1074	1081	3029	java/util/concurrent/CancellationException
    //   1097	1105	3029	java/util/concurrent/CancellationException
    //   1121	1130	3029	java/util/concurrent/CancellationException
    //   1270	1281	3029	java/util/concurrent/CancellationException
    //   991	1032	3038	java/util/concurrent/CancellationException
    //   1850	1882	3053	io/grpc/bc
    //   1946	1999	3053	io/grpc/bc
    //   1999	2048	3061	io/grpc/bc
    //   2055	2062	3061	io/grpc/bc
    //   2066	2074	3061	io/grpc/bc
  }
  
  public final org.a.a.b d()
  {
    return new org.a.a.b(f.a(2));
  }
  
  public final int e(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return 0;
  }
  
  public final boolean e()
  {
    return true;
  }
  
  public final int f()
  {
    return 2;
  }
  
  public final boolean f(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bd
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */