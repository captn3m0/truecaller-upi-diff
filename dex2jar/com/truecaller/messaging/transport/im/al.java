package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import dagger.a.d;
import javax.inject.Provider;

public final class al
  implements d<f<bp>>
{
  private final Provider<bp> a;
  private final Provider<i> b;
  
  private al(Provider<bp> paramProvider, Provider<i> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static al a(Provider<bp> paramProvider, Provider<i> paramProvider1)
  {
    return new al(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.al
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */