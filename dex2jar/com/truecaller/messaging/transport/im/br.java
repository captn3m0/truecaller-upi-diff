package com.truecaller.messaging.transport.im;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import c.a.ag;
import c.g.a.b;
import c.g.b.k;
import c.m.l;
import c.u;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.api.services.messenger.v1.h.b;
import com.truecaller.api.services.messenger.v1.h.b.a;
import com.truecaller.api.services.messenger.v1.h.d;
import com.truecaller.api.services.messenger.v1.k.a;
import com.truecaller.api.services.messenger.v1.models.j;
import com.truecaller.content.TruecallerContract.y;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.h;
import com.truecaller.network.d.j.a;
import com.truecaller.util.al;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.inject.Inject;
import javax.inject.Named;

public final class br
  implements bp
{
  private final int a;
  private final int b;
  private final com.truecaller.utils.a c;
  private final cb d;
  private final ContentResolver e;
  private final bw f;
  private final al g;
  private final h h;
  private final com.truecaller.utils.i i;
  private final dagger.a<f<com.truecaller.presence.c>> j;
  
  @Inject
  public br(@Named("max_get_users_batch_size") int paramInt1, @Named("max_db_argument_count") int paramInt2, com.truecaller.utils.a parama, cb paramcb, ContentResolver paramContentResolver, bw parambw, al paramal, h paramh, com.truecaller.utils.i parami, dagger.a<f<com.truecaller.presence.c>> parama1)
  {
    a = paramInt1;
    b = paramInt2;
    c = parama;
    d = paramcb;
    e = paramContentResolver;
    f = parambw;
    g = paramal;
    h = paramh;
    i = parami;
    j = parama1;
  }
  
  private final void a(String paramString1, String paramString2, boolean paramBoolean)
  {
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("normalized_number", paramString1);
    localContentValues.put("im_peer_id", paramString2);
    localContentValues.put("date", Long.valueOf(c.a()));
    e.insert(TruecallerContract.y.a(), localContentValues);
    if (!e(paramString2))
    {
      b((Collection)c.a.m.d(new String[] { paramString1 }), paramBoolean);
      return;
    }
    if (paramBoolean) {
      c((Collection)c.a.m.a(paramString1));
    }
  }
  
  /* Error */
  @android.annotation.SuppressLint({"Recycle"})
  private final Collection<String> b(Collection<String> paramCollection)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 73	com/truecaller/messaging/transport/im/br:h	Lcom/truecaller/messaging/h;
    //   4: invokeinterface 155 1 0
    //   9: invokestatic 111	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   12: astore 7
    //   14: aload 7
    //   16: checkcast 157	java/lang/Number
    //   19: invokevirtual 160	java/lang/Number:longValue	()J
    //   22: lconst_0
    //   23: lcmp
    //   24: ifle +8 -> 32
    //   27: iconst_1
    //   28: istore_2
    //   29: goto +5 -> 34
    //   32: iconst_0
    //   33: istore_2
    //   34: aconst_null
    //   35: astore 8
    //   37: iload_2
    //   38: ifeq +6 -> 44
    //   41: goto +6 -> 47
    //   44: aconst_null
    //   45: astore 7
    //   47: aload 7
    //   49: ifnull +12 -> 61
    //   52: aload 7
    //   54: invokevirtual 161	java/lang/Long:longValue	()J
    //   57: lstore_3
    //   58: goto +7 -> 65
    //   61: invokestatic 164	com/truecaller/messaging/transport/im/bt:a	()J
    //   64: lstore_3
    //   65: aload_0
    //   66: getfield 63	com/truecaller/messaging/transport/im/br:c	Lcom/truecaller/utils/a;
    //   69: invokeinterface 105 1 0
    //   74: lstore 5
    //   76: aload_0
    //   77: getfield 67	com/truecaller/messaging/transport/im/br:e	Landroid/content/ContentResolver;
    //   80: astore 7
    //   82: invokestatic 119	com/truecaller/content/TruecallerContract$y:a	()Landroid/net/Uri;
    //   85: astore 10
    //   87: new 166	java/lang/StringBuilder
    //   90: dup
    //   91: ldc -88
    //   93: invokespecial 171	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   96: astore 11
    //   98: aload_1
    //   99: checkcast 173	java/lang/Iterable
    //   102: astore 9
    //   104: aload 11
    //   106: aload 9
    //   108: aconst_null
    //   109: aconst_null
    //   110: aconst_null
    //   111: iconst_0
    //   112: aconst_null
    //   113: getstatic 178	com/truecaller/messaging/transport/im/br$b:a	Lcom/truecaller/messaging/transport/im/br$b;
    //   116: checkcast 180	c/g/a/b
    //   119: bipush 31
    //   121: invokestatic 183	c/a/m:a	(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lc/g/a/b;I)Ljava/lang/String;
    //   124: invokevirtual 187	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   127: pop
    //   128: aload 11
    //   130: ldc -67
    //   132: invokevirtual 187	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   135: pop
    //   136: aload 11
    //   138: invokevirtual 193	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   141: astore 11
    //   143: aload_1
    //   144: lload 5
    //   146: lload_3
    //   147: lsub
    //   148: invokestatic 196	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   151: invokestatic 199	c/a/m:a	(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;
    //   154: checkcast 137	java/util/Collection
    //   157: lload 5
    //   159: invokestatic 196	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   162: invokestatic 199	c/a/m:a	(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;
    //   165: checkcast 137	java/util/Collection
    //   168: iconst_0
    //   169: anewarray 130	java/lang/String
    //   172: invokeinterface 203 2 0
    //   177: astore_1
    //   178: aload_1
    //   179: ifnull +167 -> 346
    //   182: aload_1
    //   183: checkcast 205	[Ljava/lang/String;
    //   186: astore_1
    //   187: aload 7
    //   189: aload 10
    //   191: iconst_1
    //   192: anewarray 130	java/lang/String
    //   195: dup
    //   196: iconst_0
    //   197: ldc 92
    //   199: aastore
    //   200: aload 11
    //   202: aload_1
    //   203: aconst_null
    //   204: invokevirtual 209	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   207: astore 7
    //   209: aload 7
    //   211: ifnull +104 -> 315
    //   214: aload 7
    //   216: checkcast 211	java/io/Closeable
    //   219: astore 10
    //   221: aload 8
    //   223: astore_1
    //   224: new 213	java/util/ArrayList
    //   227: dup
    //   228: invokespecial 214	java/util/ArrayList:<init>	()V
    //   231: checkcast 137	java/util/Collection
    //   234: astore 11
    //   236: aload 8
    //   238: astore_1
    //   239: aload 7
    //   241: invokeinterface 220 1 0
    //   246: ifeq +25 -> 271
    //   249: aload 8
    //   251: astore_1
    //   252: aload 11
    //   254: aload 7
    //   256: iconst_0
    //   257: invokeinterface 224 2 0
    //   262: invokeinterface 228 2 0
    //   267: pop
    //   268: goto -32 -> 236
    //   271: aload 8
    //   273: astore_1
    //   274: aload 11
    //   276: checkcast 230	java/util/List
    //   279: astore 7
    //   281: aload 10
    //   283: aconst_null
    //   284: invokestatic 235	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   287: aload 7
    //   289: astore_1
    //   290: goto +27 -> 317
    //   293: astore 7
    //   295: goto +11 -> 306
    //   298: astore 7
    //   300: aload 7
    //   302: astore_1
    //   303: aload 7
    //   305: athrow
    //   306: aload 10
    //   308: aload_1
    //   309: invokestatic 235	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   312: aload 7
    //   314: athrow
    //   315: aconst_null
    //   316: astore_1
    //   317: aload_1
    //   318: astore 7
    //   320: aload_1
    //   321: ifnonnull +11 -> 332
    //   324: getstatic 240	c/a/y:a	Lc/a/y;
    //   327: checkcast 230	java/util/List
    //   330: astore 7
    //   332: aload 9
    //   334: aload 7
    //   336: checkcast 173	java/lang/Iterable
    //   339: invokestatic 243	c/a/m:c	(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/util/List;
    //   342: checkcast 137	java/util/Collection
    //   345: areturn
    //   346: new 245	c/u
    //   349: dup
    //   350: ldc -9
    //   352: invokespecial 248	c/u:<init>	(Ljava/lang/String;)V
    //   355: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	356	0	this	br
    //   0	356	1	paramCollection	Collection<String>
    //   28	10	2	k	int
    //   57	90	3	l1	long
    //   74	84	5	l2	long
    //   12	276	7	localObject1	Object
    //   293	1	7	localObject2	Object
    //   298	15	7	localThrowable	Throwable
    //   318	17	7	localObject3	Object
    //   35	237	8	localObject4	Object
    //   102	231	9	localIterable	Iterable
    //   85	222	10	localObject5	Object
    //   96	179	11	localObject6	Object
    // Exception table:
    //   from	to	target	type
    //   224	236	293	finally
    //   239	249	293	finally
    //   252	268	293	finally
    //   274	281	293	finally
    //   303	306	293	finally
    //   224	236	298	java/lang/Throwable
    //   239	249	298	java/lang/Throwable
    //   252	268	298	java/lang/Throwable
    //   274	281	298	java/lang/Throwable
  }
  
  private final boolean b(Collection<String> paramCollection, boolean paramBoolean)
  {
    if (f.a())
    {
      if (!i.a()) {
        return false;
      }
      Map localMap = d(paramCollection);
      Object localObject1 = (Iterable)paramCollection;
      Collection localCollection = (Collection)new ArrayList(c.a.m.a((Iterable)localObject1, 10));
      Iterator localIterator = ((Iterable)localObject1).iterator();
      for (;;)
      {
        boolean bool = localIterator.hasNext();
        int m = 1;
        if (!bool) {
          break;
        }
        localObject1 = (String)localIterator.next();
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("normalized_number", (String)localObject1);
        localContentValues.put("date", Long.valueOf(c.a()));
        if (localMap != null)
        {
          j localj = (j)localMap.get(localObject1);
          Object localObject2 = null;
          if (localj != null) {
            localObject1 = localj.a();
          } else {
            localObject1 = null;
          }
          localContentValues.put("im_peer_id", (String)localObject1);
          if (localj != null) {
            localObject1 = Integer.valueOf(localj.b());
          } else {
            localObject1 = Integer.valueOf(0);
          }
          localContentValues.put("registration_timestamp", (Integer)localObject1);
          localObject1 = localObject2;
          if (localj != null) {
            localObject1 = localj.a();
          }
          localObject1 = (CharSequence)localObject1;
          int k = m;
          if (localObject1 != null) {
            if (((CharSequence)localObject1).length() == 0) {
              k = m;
            } else {
              k = 0;
            }
          }
          if (k != 0) {
            localContentValues.put("join_im_notification", Integer.valueOf(0));
          }
        }
        localCollection.add(localContentValues);
      }
      localObject1 = ((Collection)localCollection).toArray(new ContentValues[0]);
      if (localObject1 != null)
      {
        localObject1 = (ContentValues[])localObject1;
        e.bulkInsert(TruecallerContract.y.a(), (ContentValues[])localObject1);
        if (paramBoolean) {
          c(paramCollection);
        }
        return localMap != null;
      }
      throw new u("null cannot be cast to non-null type kotlin.Array<T>");
    }
    return false;
  }
  
  private final void c(Collection<String> paramCollection)
  {
    Intent localIntent = new Intent("com.truecaller.messaging.transport.im.ACTION_IM_USED_ADDED");
    localIntent.putStringArrayListExtra("phone_numbers", new ArrayList(paramCollection));
    g.a(localIntent);
  }
  
  private final Map<String, j> d(Collection<String> paramCollection)
  {
    Object localObject1 = (k.a)j.a.a(d);
    if (localObject1 == null) {
      return null;
    }
    paramCollection = (Iterable)paramCollection;
    Map localMap = (Map)new LinkedHashMap(c.k.i.c(ag.a(c.a.m.a(paramCollection, 10)), 16));
    Object localObject2 = paramCollection.iterator();
    while (((Iterator)localObject2).hasNext())
    {
      Object localObject3 = ((Iterator)localObject2).next();
      paramCollection = (String)localObject3;
      CharSequence localCharSequence = (CharSequence)paramCollection;
      k.b(localCharSequence, "receiver$0");
      int n = localCharSequence.length();
      int m = 0;
      int k = m;
      if (n > 0)
      {
        k = m;
        if (c.n.a.a(localCharSequence.charAt(0), '+', false)) {
          k = 1;
        }
      }
      if (k == 0)
      {
        paramCollection = null;
      }
      else
      {
        if (paramCollection == null) {
          break label181;
        }
        paramCollection = paramCollection.substring(1);
        k.a(paramCollection, "(this as java.lang.String).substring(startIndex)");
        paramCollection = c.n.m.d(paramCollection);
      }
      localMap.put(paramCollection, localObject3);
      continue;
      label181:
      throw new u("null cannot be cast to non-null type java.lang.String");
    }
    paramCollection = c.a.m.e((Iterable)localMap.keySet());
    if (paramCollection.isEmpty()) {
      return ag.a();
    }
    paramCollection = (h.b)h.b.a().a((Iterable)paramCollection).build();
    try
    {
      paramCollection = ((k.a)localObject1).a(paramCollection);
      k.a(paramCollection, "response");
      localObject1 = paramCollection.a();
      k.a(localObject1, "response.usersMap");
      paramCollection = (Map)new LinkedHashMap(ag.a(((Map)localObject1).size()));
      localObject1 = ((Iterable)((Map)localObject1).entrySet()).iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = ((Iterator)localObject1).next();
        paramCollection.put((String)localMap.get(((Map.Entry)localObject2).getKey()), ((Map.Entry)localObject2).getValue());
      }
      return paramCollection;
    }
    catch (RuntimeException paramCollection)
    {
      for (;;) {}
    }
    com.truecaller.multisim.b.c.a();
    return null;
  }
  
  /* Error */
  private final boolean d(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 67	com/truecaller/messaging/transport/im/br:e	Landroid/content/ContentResolver;
    //   4: astore 4
    //   6: invokestatic 119	com/truecaller/content/TruecallerContract$y:a	()Landroid/net/Uri;
    //   9: astore 5
    //   11: iconst_1
    //   12: istore_3
    //   13: aload 4
    //   15: aload 5
    //   17: iconst_1
    //   18: anewarray 130	java/lang/String
    //   21: dup
    //   22: iconst_0
    //   23: ldc_w 443
    //   26: aastore
    //   27: ldc_w 445
    //   30: iconst_1
    //   31: anewarray 130	java/lang/String
    //   34: dup
    //   35: iconst_0
    //   36: aload_1
    //   37: aastore
    //   38: aconst_null
    //   39: invokevirtual 209	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   42: astore_1
    //   43: aload_1
    //   44: ifnull +61 -> 105
    //   47: aload_1
    //   48: checkcast 211	java/io/Closeable
    //   51: astore 5
    //   53: aconst_null
    //   54: astore_1
    //   55: aload 5
    //   57: checkcast 216	android/database/Cursor
    //   60: invokeinterface 448 1 0
    //   65: istore_2
    //   66: iload_2
    //   67: ifle +6 -> 73
    //   70: goto +5 -> 75
    //   73: iconst_0
    //   74: istore_3
    //   75: aload 5
    //   77: aconst_null
    //   78: invokestatic 235	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   81: iload_3
    //   82: ireturn
    //   83: astore 4
    //   85: goto +11 -> 96
    //   88: astore 4
    //   90: aload 4
    //   92: astore_1
    //   93: aload 4
    //   95: athrow
    //   96: aload 5
    //   98: aload_1
    //   99: invokestatic 235	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   102: aload 4
    //   104: athrow
    //   105: iconst_0
    //   106: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	107	0	this	br
    //   0	107	1	paramString	String
    //   65	2	2	k	int
    //   12	70	3	bool	boolean
    //   4	10	4	localContentResolver	ContentResolver
    //   83	1	4	localObject1	Object
    //   88	15	4	localThrowable	Throwable
    //   9	88	5	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   55	66	83	finally
    //   93	96	83	finally
    //   55	66	88	java/lang/Throwable
  }
  
  /* Error */
  @android.annotation.SuppressLint({"Recycle"})
  private final boolean e(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: ifnonnull +5 -> 6
    //   4: iconst_0
    //   5: ireturn
    //   6: aload_0
    //   7: getfield 67	com/truecaller/messaging/transport/im/br:e	Landroid/content/ContentResolver;
    //   10: invokestatic 119	com/truecaller/content/TruecallerContract$y:a	()Landroid/net/Uri;
    //   13: iconst_1
    //   14: anewarray 130	java/lang/String
    //   17: dup
    //   18: iconst_0
    //   19: ldc_w 299
    //   22: aastore
    //   23: ldc_w 450
    //   26: iconst_1
    //   27: anewarray 130	java/lang/String
    //   30: dup
    //   31: iconst_0
    //   32: aload_1
    //   33: aastore
    //   34: aconst_null
    //   35: invokevirtual 209	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   38: astore 4
    //   40: aconst_null
    //   41: astore_1
    //   42: aconst_null
    //   43: astore_2
    //   44: aload 4
    //   46: ifnull +99 -> 145
    //   49: aload 4
    //   51: checkcast 211	java/io/Closeable
    //   54: astore_3
    //   55: aload_2
    //   56: astore_1
    //   57: new 213	java/util/ArrayList
    //   60: dup
    //   61: invokespecial 214	java/util/ArrayList:<init>	()V
    //   64: checkcast 137	java/util/Collection
    //   67: astore 5
    //   69: aload_2
    //   70: astore_1
    //   71: aload 4
    //   73: invokeinterface 220 1 0
    //   78: ifeq +27 -> 105
    //   81: aload_2
    //   82: astore_1
    //   83: aload 5
    //   85: aload 4
    //   87: iconst_0
    //   88: invokeinterface 453 2 0
    //   93: invokestatic 297	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   96: invokeinterface 228 2 0
    //   101: pop
    //   102: goto -33 -> 69
    //   105: aload_2
    //   106: astore_1
    //   107: aload 5
    //   109: checkcast 230	java/util/List
    //   112: astore_2
    //   113: aload_3
    //   114: aconst_null
    //   115: invokestatic 235	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   118: aload_2
    //   119: invokestatic 456	c/a/m:e	(Ljava/util/List;)Ljava/lang/Object;
    //   122: checkcast 294	java/lang/Integer
    //   125: astore_1
    //   126: goto +19 -> 145
    //   129: astore_2
    //   130: goto +8 -> 138
    //   133: astore_2
    //   134: aload_2
    //   135: astore_1
    //   136: aload_2
    //   137: athrow
    //   138: aload_3
    //   139: aload_1
    //   140: invokestatic 235	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   143: aload_2
    //   144: athrow
    //   145: aload_1
    //   146: ifnonnull +5 -> 151
    //   149: iconst_1
    //   150: ireturn
    //   151: aload_1
    //   152: invokevirtual 459	java/lang/Integer:intValue	()I
    //   155: ifeq +5 -> 160
    //   158: iconst_1
    //   159: ireturn
    //   160: iconst_0
    //   161: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	162	0	this	br
    //   0	162	1	paramString	String
    //   43	76	2	localList	List
    //   129	1	2	localObject	Object
    //   133	11	2	localThrowable	Throwable
    //   54	85	3	localCloseable	java.io.Closeable
    //   38	48	4	localCursor	android.database.Cursor
    //   67	41	5	localCollection	Collection
    // Exception table:
    //   from	to	target	type
    //   57	69	129	finally
    //   71	81	129	finally
    //   83	102	129	finally
    //   107	113	129	finally
    //   136	138	129	finally
    //   57	69	133	java/lang/Throwable
    //   71	81	133	java/lang/Throwable
    //   83	102	133	java/lang/Throwable
    //   107	113	133	java/lang/Throwable
  }
  
  /* Error */
  public final w<Boolean> a()
  {
    // Byte code:
    //   0: iconst_0
    //   1: istore_1
    //   2: getstatic 465	com/google/c/a/k$d:b	Lcom/google/c/a/k$d;
    //   5: invokevirtual 468	com/google/c/a/k$d:name	()Ljava/lang/String;
    //   8: astore_2
    //   9: aload_0
    //   10: getfield 67	com/truecaller/messaging/transport/im/br:e	Landroid/content/ContentResolver;
    //   13: astore_3
    //   14: invokestatic 471	com/truecaller/content/TruecallerContract$k:a	()Landroid/net/Uri;
    //   17: astore 4
    //   19: new 166	java/lang/StringBuilder
    //   22: dup
    //   23: invokespecial 472	java/lang/StringBuilder:<init>	()V
    //   26: astore 5
    //   28: aload 5
    //   30: ldc_w 474
    //   33: invokevirtual 187	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   36: pop
    //   37: aload 5
    //   39: ldc_w 476
    //   42: invokevirtual 187	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   45: pop
    //   46: aload 5
    //   48: ldc_w 478
    //   51: invokevirtual 187	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   54: pop
    //   55: aload 5
    //   57: invokevirtual 193	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   60: astore 5
    //   62: aload_3
    //   63: aload 4
    //   65: iconst_1
    //   66: anewarray 130	java/lang/String
    //   69: dup
    //   70: iconst_0
    //   71: ldc_w 480
    //   74: aastore
    //   75: aload 5
    //   77: iconst_2
    //   78: anewarray 130	java/lang/String
    //   81: dup
    //   82: iconst_0
    //   83: ldc_w 482
    //   86: aastore
    //   87: dup
    //   88: iconst_1
    //   89: aload_2
    //   90: aastore
    //   91: ldc_w 484
    //   94: invokevirtual 209	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   97: astore_2
    //   98: aload_2
    //   99: ifnull +63 -> 162
    //   102: aload_2
    //   103: checkcast 211	java/io/Closeable
    //   106: astore 4
    //   108: aconst_null
    //   109: astore_3
    //   110: aload_3
    //   111: astore_2
    //   112: aload 4
    //   114: checkcast 216	android/database/Cursor
    //   117: invokeinterface 448 1 0
    //   122: ifle +5 -> 127
    //   125: iconst_1
    //   126: istore_1
    //   127: aload_3
    //   128: astore_2
    //   129: iload_1
    //   130: invokestatic 489	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   133: invokestatic 494	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   136: astore_3
    //   137: aload 4
    //   139: aconst_null
    //   140: invokestatic 235	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   143: aload_3
    //   144: areturn
    //   145: astore_3
    //   146: goto +8 -> 154
    //   149: astore_3
    //   150: aload_3
    //   151: astore_2
    //   152: aload_3
    //   153: athrow
    //   154: aload 4
    //   156: aload_2
    //   157: invokestatic 235	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   160: aload_3
    //   161: athrow
    //   162: getstatic 498	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   165: invokestatic 494	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   168: astore_2
    //   169: aload_2
    //   170: ldc_w 500
    //   173: invokestatic 375	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   176: aload_2
    //   177: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	178	0	this	br
    //   1	129	1	bool	boolean
    //   8	169	2	localObject1	Object
    //   13	131	3	localObject2	Object
    //   145	1	3	localObject3	Object
    //   149	12	3	localThrowable	Throwable
    //   17	138	4	localObject4	Object
    //   26	50	5	localObject5	Object
    // Exception table:
    //   from	to	target	type
    //   112	125	145	finally
    //   129	137	145	finally
    //   152	154	145	finally
    //   112	125	149	java/lang/Throwable
    //   129	137	149	java/lang/Throwable
  }
  
  /* Error */
  @android.annotation.SuppressLint({"Recycle"})
  public final w<List<bx>> a(long paramLong)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 73	com/truecaller/messaging/transport/im/br:h	Lcom/truecaller/messaging/h;
    //   4: invokeinterface 505 1 0
    //   9: astore 4
    //   11: aload 4
    //   13: ifnull +42 -> 55
    //   16: new 166	java/lang/StringBuilder
    //   19: dup
    //   20: ldc_w 507
    //   23: invokespecial 171	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   26: astore 5
    //   28: aload 5
    //   30: aload 4
    //   32: invokestatic 513	android/database/DatabaseUtils:sqlEscapeString	(Ljava/lang/String;)Ljava/lang/String;
    //   35: invokevirtual 187	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   38: pop
    //   39: aload 5
    //   41: invokevirtual 193	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   44: astore 5
    //   46: aload 5
    //   48: astore 4
    //   50: aload 5
    //   52: ifnonnull +8 -> 60
    //   55: ldc_w 515
    //   58: astore 4
    //   60: new 166	java/lang/StringBuilder
    //   63: dup
    //   64: ldc_w 517
    //   67: invokespecial 171	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   70: astore 5
    //   72: aload 5
    //   74: aload 4
    //   76: invokevirtual 187	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   79: pop
    //   80: aload 5
    //   82: bipush 41
    //   84: invokevirtual 520	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   87: pop
    //   88: aload 5
    //   90: invokevirtual 193	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   93: astore 6
    //   95: iconst_3
    //   96: anewarray 130	java/lang/String
    //   99: dup
    //   100: iconst_0
    //   101: ldc_w 482
    //   104: aastore
    //   105: dup
    //   106: iconst_1
    //   107: getstatic 465	com/google/c/a/k$d:b	Lcom/google/c/a/k$d;
    //   110: invokevirtual 468	com/google/c/a/k$d:name	()Ljava/lang/String;
    //   113: aastore
    //   114: dup
    //   115: iconst_2
    //   116: lload_1
    //   117: invokestatic 196	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   120: aastore
    //   121: invokestatic 523	c/a/m:b	([Ljava/lang/Object;)Ljava/util/List;
    //   124: astore 7
    //   126: aload_0
    //   127: getfield 67	com/truecaller/messaging/transport/im/br:e	Landroid/content/ContentResolver;
    //   130: astore 4
    //   132: invokestatic 471	com/truecaller/content/TruecallerContract$k:a	()Landroid/net/Uri;
    //   135: astore 5
    //   137: new 166	java/lang/StringBuilder
    //   140: dup
    //   141: invokespecial 472	java/lang/StringBuilder:<init>	()V
    //   144: astore 8
    //   146: aload 8
    //   148: ldc_w 474
    //   151: invokevirtual 187	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   154: pop
    //   155: aload 8
    //   157: ldc_w 476
    //   160: invokevirtual 187	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   163: pop
    //   164: aload 8
    //   166: aload 6
    //   168: invokevirtual 187	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   171: pop
    //   172: aload 8
    //   174: invokevirtual 193	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   177: astore 6
    //   179: aload 7
    //   181: checkcast 137	java/util/Collection
    //   184: astore 7
    //   186: aload 7
    //   188: ifnull +766 -> 954
    //   191: aload 7
    //   193: iconst_0
    //   194: anewarray 130	java/lang/String
    //   197: invokeinterface 203 2 0
    //   202: astore 7
    //   204: aload 7
    //   206: ifnull +738 -> 944
    //   209: aload 7
    //   211: checkcast 205	[Ljava/lang/String;
    //   214: astore 7
    //   216: aload 4
    //   218: aload 5
    //   220: iconst_1
    //   221: anewarray 130	java/lang/String
    //   224: dup
    //   225: iconst_0
    //   226: ldc_w 525
    //   229: aastore
    //   230: aload 6
    //   232: aload 7
    //   234: aconst_null
    //   235: invokevirtual 209	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   238: astore 10
    //   240: aconst_null
    //   241: astore 6
    //   243: aconst_null
    //   244: astore 7
    //   246: aconst_null
    //   247: astore 8
    //   249: aconst_null
    //   250: astore 5
    //   252: aload 10
    //   254: ifnull +673 -> 927
    //   257: aload 10
    //   259: checkcast 211	java/io/Closeable
    //   262: astore 9
    //   264: aload 8
    //   266: astore 4
    //   268: new 213	java/util/ArrayList
    //   271: dup
    //   272: invokespecial 214	java/util/ArrayList:<init>	()V
    //   275: checkcast 137	java/util/Collection
    //   278: astore 11
    //   280: aload 8
    //   282: astore 4
    //   284: aload 10
    //   286: invokeinterface 220 1 0
    //   291: ifeq +26 -> 317
    //   294: aload 8
    //   296: astore 4
    //   298: aload 11
    //   300: aload 10
    //   302: iconst_0
    //   303: invokeinterface 224 2 0
    //   308: invokeinterface 228 2 0
    //   313: pop
    //   314: goto -34 -> 280
    //   317: aload 8
    //   319: astore 4
    //   321: aload 11
    //   323: checkcast 230	java/util/List
    //   326: astore 8
    //   328: aload 9
    //   330: aconst_null
    //   331: invokestatic 235	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   334: aload 8
    //   336: checkcast 173	java/lang/Iterable
    //   339: invokestatic 529	c/a/m:l	(Ljava/lang/Iterable;)Ljava/util/Set;
    //   342: astore 8
    //   344: aload 8
    //   346: ifnonnull +6 -> 352
    //   349: goto +578 -> 927
    //   352: aload_0
    //   353: getfield 67	com/truecaller/messaging/transport/im/br:e	Landroid/content/ContentResolver;
    //   356: invokestatic 532	com/truecaller/content/TruecallerContract$ae:a	()Landroid/net/Uri;
    //   359: iconst_1
    //   360: anewarray 130	java/lang/String
    //   363: dup
    //   364: iconst_0
    //   365: ldc_w 534
    //   368: aastore
    //   369: ldc_w 536
    //   372: aconst_null
    //   373: aconst_null
    //   374: invokevirtual 209	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   377: astore 10
    //   379: aload 10
    //   381: ifnull +505 -> 886
    //   384: aload 10
    //   386: checkcast 211	java/io/Closeable
    //   389: astore 9
    //   391: aload 7
    //   393: astore 4
    //   395: new 213	java/util/ArrayList
    //   398: dup
    //   399: invokespecial 214	java/util/ArrayList:<init>	()V
    //   402: checkcast 137	java/util/Collection
    //   405: astore 11
    //   407: aload 7
    //   409: astore 4
    //   411: aload 10
    //   413: invokeinterface 220 1 0
    //   418: ifeq +26 -> 444
    //   421: aload 7
    //   423: astore 4
    //   425: aload 11
    //   427: aload 10
    //   429: iconst_0
    //   430: invokeinterface 224 2 0
    //   435: invokeinterface 228 2 0
    //   440: pop
    //   441: goto -34 -> 407
    //   444: aload 7
    //   446: astore 4
    //   448: aload 11
    //   450: checkcast 230	java/util/List
    //   453: astore 7
    //   455: aload 9
    //   457: aconst_null
    //   458: invokestatic 235	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   461: aload 8
    //   463: aload 7
    //   465: checkcast 137	java/util/Collection
    //   468: invokeinterface 542 2 0
    //   473: pop
    //   474: aload_0
    //   475: getfield 67	com/truecaller/messaging/transport/im/br:e	Landroid/content/ContentResolver;
    //   478: astore 4
    //   480: invokestatic 546	com/truecaller/content/TruecallerContract$a:b	()Landroid/net/Uri;
    //   483: astore 7
    //   485: new 166	java/lang/StringBuilder
    //   488: dup
    //   489: ldc_w 548
    //   492: invokespecial 171	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   495: astore 9
    //   497: aload 9
    //   499: aload 8
    //   501: checkcast 173	java/lang/Iterable
    //   504: aconst_null
    //   505: aconst_null
    //   506: aconst_null
    //   507: iconst_0
    //   508: aconst_null
    //   509: getstatic 553	com/truecaller/messaging/transport/im/br$c:a	Lcom/truecaller/messaging/transport/im/br$c;
    //   512: checkcast 180	c/g/a/b
    //   515: bipush 31
    //   517: invokestatic 183	c/a/m:a	(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lc/g/a/b;I)Ljava/lang/String;
    //   520: invokevirtual 187	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   523: pop
    //   524: aload 9
    //   526: bipush 41
    //   528: invokevirtual 520	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   531: pop
    //   532: aload 9
    //   534: invokevirtual 193	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   537: astore 9
    //   539: aload 8
    //   541: checkcast 137	java/util/Collection
    //   544: astore 8
    //   546: aload 8
    //   548: ifnull +303 -> 851
    //   551: aload 8
    //   553: iconst_0
    //   554: anewarray 130	java/lang/String
    //   557: invokeinterface 203 2 0
    //   562: astore 8
    //   564: aload 8
    //   566: ifnull +275 -> 841
    //   569: aload 8
    //   571: checkcast 205	[Ljava/lang/String;
    //   574: astore 8
    //   576: aload 4
    //   578: aload 7
    //   580: iconst_5
    //   581: anewarray 130	java/lang/String
    //   584: dup
    //   585: iconst_0
    //   586: ldc_w 555
    //   589: aastore
    //   590: dup
    //   591: iconst_1
    //   592: ldc_w 557
    //   595: aastore
    //   596: dup
    //   597: iconst_2
    //   598: ldc_w 559
    //   601: aastore
    //   602: dup
    //   603: iconst_3
    //   604: ldc_w 561
    //   607: aastore
    //   608: dup
    //   609: iconst_4
    //   610: ldc_w 563
    //   613: aastore
    //   614: aload 9
    //   616: aload 8
    //   618: aconst_null
    //   619: invokevirtual 209	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   622: astore 7
    //   624: aload 6
    //   626: astore 4
    //   628: aload 7
    //   630: ifnull +193 -> 823
    //   633: aload 7
    //   635: checkcast 211	java/io/Closeable
    //   638: astore 6
    //   640: aload 5
    //   642: astore 4
    //   644: new 213	java/util/ArrayList
    //   647: dup
    //   648: invokespecial 214	java/util/ArrayList:<init>	()V
    //   651: checkcast 137	java/util/Collection
    //   654: astore 8
    //   656: aload 5
    //   658: astore 4
    //   660: aload 7
    //   662: invokeinterface 220 1 0
    //   667: ifeq +102 -> 769
    //   670: aload 5
    //   672: astore 4
    //   674: aload 7
    //   676: iconst_0
    //   677: invokeinterface 453 2 0
    //   682: istore_3
    //   683: aload 5
    //   685: astore 4
    //   687: aload 7
    //   689: iconst_1
    //   690: invokeinterface 224 2 0
    //   695: astore 9
    //   697: aload 5
    //   699: astore 4
    //   701: aload 7
    //   703: iconst_2
    //   704: invokeinterface 224 2 0
    //   709: astore 10
    //   711: aload 5
    //   713: astore 4
    //   715: aload 10
    //   717: ldc_w 565
    //   720: invokestatic 375	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   723: aload 5
    //   725: astore 4
    //   727: aload 8
    //   729: new 567	com/truecaller/messaging/transport/im/bx
    //   732: dup
    //   733: iload_3
    //   734: aload 9
    //   736: aload 10
    //   738: aload 7
    //   740: iconst_3
    //   741: invokeinterface 224 2 0
    //   746: aload 7
    //   748: iconst_4
    //   749: invokeinterface 571 2 0
    //   754: invokestatic 111	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   757: invokespecial 574	com/truecaller/messaging/transport/im/bx:<init>	(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    //   760: invokeinterface 228 2 0
    //   765: pop
    //   766: goto -110 -> 656
    //   769: aload 5
    //   771: astore 4
    //   773: aload 8
    //   775: checkcast 230	java/util/List
    //   778: astore 5
    //   780: aload 6
    //   782: aconst_null
    //   783: invokestatic 235	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   786: aload 5
    //   788: checkcast 173	java/lang/Iterable
    //   791: invokestatic 576	c/a/m:g	(Ljava/lang/Iterable;)Ljava/util/List;
    //   794: astore 4
    //   796: goto +27 -> 823
    //   799: astore 5
    //   801: goto +12 -> 813
    //   804: astore 5
    //   806: aload 5
    //   808: astore 4
    //   810: aload 5
    //   812: athrow
    //   813: aload 6
    //   815: aload 4
    //   817: invokestatic 235	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   820: aload 5
    //   822: athrow
    //   823: aload 4
    //   825: invokestatic 494	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   828: astore 4
    //   830: aload 4
    //   832: ldc_w 578
    //   835: invokestatic 375	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   838: aload 4
    //   840: areturn
    //   841: new 245	c/u
    //   844: dup
    //   845: ldc -9
    //   847: invokespecial 248	c/u:<init>	(Ljava/lang/String;)V
    //   850: athrow
    //   851: new 245	c/u
    //   854: dup
    //   855: ldc_w 580
    //   858: invokespecial 248	c/u:<init>	(Ljava/lang/String;)V
    //   861: athrow
    //   862: astore 5
    //   864: goto +12 -> 876
    //   867: astore 5
    //   869: aload 5
    //   871: astore 4
    //   873: aload 5
    //   875: athrow
    //   876: aload 9
    //   878: aload 4
    //   880: invokestatic 235	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   883: aload 5
    //   885: athrow
    //   886: aconst_null
    //   887: invokestatic 494	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   890: astore 4
    //   892: aload 4
    //   894: ldc_w 582
    //   897: invokestatic 375	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   900: aload 4
    //   902: areturn
    //   903: astore 5
    //   905: goto +12 -> 917
    //   908: astore 5
    //   910: aload 5
    //   912: astore 4
    //   914: aload 5
    //   916: athrow
    //   917: aload 9
    //   919: aload 4
    //   921: invokestatic 235	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   924: aload 5
    //   926: athrow
    //   927: aconst_null
    //   928: invokestatic 494	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   931: astore 4
    //   933: aload 4
    //   935: ldc_w 582
    //   938: invokestatic 375	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   941: aload 4
    //   943: areturn
    //   944: new 245	c/u
    //   947: dup
    //   948: ldc -9
    //   950: invokespecial 248	c/u:<init>	(Ljava/lang/String;)V
    //   953: athrow
    //   954: new 245	c/u
    //   957: dup
    //   958: ldc_w 580
    //   961: invokespecial 248	c/u:<init>	(Ljava/lang/String;)V
    //   964: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	965	0	this	br
    //   0	965	1	paramLong	long
    //   682	52	3	k	int
    //   9	933	4	localObject1	Object
    //   26	761	5	localObject2	Object
    //   799	1	5	localObject3	Object
    //   804	17	5	localThrowable1	Throwable
    //   862	1	5	localObject4	Object
    //   867	17	5	localThrowable2	Throwable
    //   903	1	5	localObject5	Object
    //   908	17	5	localThrowable3	Throwable
    //   93	721	6	localObject6	Object
    //   124	623	7	localObject7	Object
    //   144	630	8	localObject8	Object
    //   262	656	9	localObject9	Object
    //   238	499	10	localObject10	Object
    //   278	171	11	localCollection	Collection
    // Exception table:
    //   from	to	target	type
    //   644	656	799	finally
    //   660	670	799	finally
    //   674	683	799	finally
    //   687	697	799	finally
    //   701	711	799	finally
    //   715	723	799	finally
    //   727	766	799	finally
    //   773	780	799	finally
    //   810	813	799	finally
    //   644	656	804	java/lang/Throwable
    //   660	670	804	java/lang/Throwable
    //   674	683	804	java/lang/Throwable
    //   687	697	804	java/lang/Throwable
    //   701	711	804	java/lang/Throwable
    //   715	723	804	java/lang/Throwable
    //   727	766	804	java/lang/Throwable
    //   773	780	804	java/lang/Throwable
    //   395	407	862	finally
    //   411	421	862	finally
    //   425	441	862	finally
    //   448	455	862	finally
    //   873	876	862	finally
    //   395	407	867	java/lang/Throwable
    //   411	421	867	java/lang/Throwable
    //   425	441	867	java/lang/Throwable
    //   448	455	867	java/lang/Throwable
    //   268	280	903	finally
    //   284	294	903	finally
    //   298	314	903	finally
    //   321	328	903	finally
    //   914	917	903	finally
    //   268	280	908	java/lang/Throwable
    //   284	294	908	java/lang/Throwable
    //   298	314	908	java/lang/Throwable
    //   321	328	908	java/lang/Throwable
  }
  
  public final w<Boolean> a(String paramString)
  {
    k.b(paramString, "normalizedNumber");
    a(paramString, null, true);
    paramString = w.b(Boolean.valueOf(d(paramString)));
    k.a(paramString, "Promise.wrap(hasImId(normalizedNumber))");
    return paramString;
  }
  
  public final w<Boolean> a(Collection<String> paramCollection, boolean paramBoolean)
  {
    k.b(paramCollection, "normalizedNumbers");
    if ((f.a()) && (i.a()))
    {
      paramCollection = l.a(l.a(l.c(l.a(c.a.m.n((Iterable)paramCollection), b), (b)new br.a(this, paramBoolean))), a).a();
      paramBoolean = true;
      while (paramCollection.hasNext()) {
        paramBoolean &= b((Collection)paramCollection.next(), true);
      }
      paramCollection = w.b(Boolean.valueOf(paramBoolean));
      k.a(paramCollection, "Promise.wrap(it)");
      k.a(paramCollection, "normalizedNumbers.asSequ….let { Promise.wrap(it) }");
      return paramCollection;
    }
    paramCollection = w.b(Boolean.FALSE);
    k.a(paramCollection, "Promise.wrap(false)");
    return paramCollection;
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "normalizedNumber");
    k.b(paramString2, "imPeerId");
    a(paramString1, paramString2, false);
  }
  
  public final void a(Collection<String> paramCollection)
  {
    k.b(paramCollection, "normalizedNumbers");
    if (f.a())
    {
      if (!i.a()) {
        return;
      }
      paramCollection = (Collection)l.d(l.a(l.c(l.a(c.a.m.n((Iterable)paramCollection), b), (b)new br.d(this))));
      if (paramCollection.isEmpty()) {
        return;
      }
      paramCollection = (List)paramCollection;
      ((com.truecaller.presence.c)((f)j.get()).a()).a((Collection)paramCollection);
      return;
    }
  }
  
  public final void a(List<String> paramList)
  {
    k.b(paramList, "numbers");
    Object localObject = (Iterable)paramList;
    paramList = (Collection)new ArrayList(c.a.m.a((Iterable)localObject, 10));
    localObject = ((Iterable)localObject).iterator();
    while (((Iterator)localObject).hasNext())
    {
      String str = (String)((Iterator)localObject).next();
      ContentValues localContentValues = new ContentValues();
      localContentValues.put("normalized_number", str);
      localContentValues.put("join_im_notification", Integer.valueOf(1));
      paramList.add(localContentValues);
    }
    paramList = ((Collection)paramList).toArray(new ContentValues[0]);
    if (paramList != null)
    {
      paramList = (ContentValues[])paramList;
      com.truecaller.multisim.b.c.c(new String[] { "mark Im User As Notified successfully: ".concat(String.valueOf(e.bulkInsert(TruecallerContract.y.a(), paramList))) });
      return;
    }
    throw new u("null cannot be cast to non-null type kotlin.Array<T>");
  }
  
  public final w<Boolean> b(String paramString)
  {
    k.b(paramString, "normalizedNumber");
    a((Collection)c.a.m.a(paramString));
    paramString = w.b(Boolean.valueOf(d(paramString)));
    k.a(paramString, "Promise.wrap(hasImId(normalizedNumber))");
    return paramString;
  }
  
  /* Error */
  public final w<String> c(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 627
    //   4: invokestatic 40	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   7: aload_0
    //   8: getfield 67	com/truecaller/messaging/transport/im/br:e	Landroid/content/ContentResolver;
    //   11: invokestatic 119	com/truecaller/content/TruecallerContract$y:a	()Landroid/net/Uri;
    //   14: iconst_1
    //   15: anewarray 130	java/lang/String
    //   18: dup
    //   19: iconst_0
    //   20: ldc 92
    //   22: aastore
    //   23: ldc_w 665
    //   26: iconst_1
    //   27: anewarray 130	java/lang/String
    //   30: dup
    //   31: iconst_0
    //   32: aload_1
    //   33: aastore
    //   34: aconst_null
    //   35: invokevirtual 209	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   38: astore_1
    //   39: aconst_null
    //   40: astore_2
    //   41: aload_1
    //   42: ifnull +70 -> 112
    //   45: aload_1
    //   46: checkcast 211	java/io/Closeable
    //   49: astore_3
    //   50: aload_2
    //   51: astore_1
    //   52: aload_3
    //   53: checkcast 216	android/database/Cursor
    //   56: astore 4
    //   58: aload_2
    //   59: astore_1
    //   60: aload 4
    //   62: invokeinterface 668 1 0
    //   67: ifeq +19 -> 86
    //   70: aload_2
    //   71: astore_1
    //   72: aload 4
    //   74: iconst_0
    //   75: invokeinterface 224 2 0
    //   80: astore_2
    //   81: aload_2
    //   82: astore_1
    //   83: goto +5 -> 88
    //   86: aconst_null
    //   87: astore_1
    //   88: aload_3
    //   89: aconst_null
    //   90: invokestatic 235	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   93: goto +21 -> 114
    //   96: astore_2
    //   97: goto +8 -> 105
    //   100: astore_2
    //   101: aload_2
    //   102: astore_1
    //   103: aload_2
    //   104: athrow
    //   105: aload_3
    //   106: aload_1
    //   107: invokestatic 235	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   110: aload_2
    //   111: athrow
    //   112: aconst_null
    //   113: astore_1
    //   114: aload_1
    //   115: invokestatic 494	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   118: astore_1
    //   119: aload_1
    //   120: ldc_w 670
    //   123: invokestatic 375	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   126: aload_1
    //   127: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	128	0	this	br
    //   0	128	1	paramString	String
    //   40	42	2	str	String
    //   96	1	2	localObject	Object
    //   100	11	2	localThrowable	Throwable
    //   49	57	3	localCloseable	java.io.Closeable
    //   56	17	4	localCursor	android.database.Cursor
    // Exception table:
    //   from	to	target	type
    //   52	58	96	finally
    //   60	70	96	finally
    //   72	81	96	finally
    //   103	105	96	finally
    //   52	58	100	java/lang/Throwable
    //   60	70	100	java/lang/Throwable
    //   72	81	100	java/lang/Throwable
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.br
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */