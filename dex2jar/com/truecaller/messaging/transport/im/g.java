package com.truecaller.messaging.transport.im;

import android.content.ContentResolver;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.analytics.e.a;
import com.truecaller.api.services.messenger.v1.models.MessageContent;
import com.truecaller.api.services.messenger.v1.models.MessageContent.a;
import com.truecaller.api.services.messenger.v1.models.MessageContent.d;
import com.truecaller.api.services.messenger.v1.models.MessageContent.h;
import com.truecaller.api.services.messenger.v1.models.MessageContent.j;
import com.truecaller.messaging.data.providers.c;
import com.truecaller.messaging.data.types.AudioEntity;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.VideoEntity;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import javax.inject.Named;
import okhttp3.ab;
import okhttp3.e;
import okhttp3.o;
import okhttp3.y;

public final class g
  implements f
{
  private final ContentResolver a;
  private final com.truecaller.util.g b;
  private final y c;
  private final com.truecaller.analytics.b d;
  private final cb e;
  private final cl f;
  private final c g;
  
  @Inject
  public g(ContentResolver paramContentResolver, com.truecaller.util.g paramg, @Named("ImClient") y paramy, com.truecaller.analytics.b paramb, cb paramcb, cl paramcl, c paramc)
  {
    a = paramContentResolver;
    b = paramg;
    c = paramy;
    d = paramb;
    e = paramcb;
    f = paramcl;
    g = paramc;
  }
  
  private final void a(Entity paramEntity, long paramLong, String paramString)
  {
    if (!(paramEntity instanceof BinaryEntity)) {
      localObject = null;
    } else {
      localObject = paramEntity;
    }
    Object localObject = (BinaryEntity)localObject;
    long l;
    if (localObject != null) {
      l = d;
    } else {
      l = -1L;
    }
    float f1 = (float)l / 1024.0F / (float)TimeUnit.MILLISECONDS.toSeconds(paramLong);
    paramEntity = new e.a("ImAttachmentUpload").a("Type", j).a("Status", paramString).a("SizeAbsolute", l).a("SizeBatch", u.b(l)).a(Double.valueOf(f1)).a("TimeBatch", u.a(paramLong)).a();
    paramString = d;
    k.a(paramEntity, "event");
    paramString.a(paramEntity);
  }
  
  private static void a(y paramy, Object paramObject)
  {
    Object localObject = paramy.b().b();
    k.a(localObject, "dispatcher().queuedCalls()");
    localObject = ((Iterable)localObject).iterator();
    while (((Iterator)localObject).hasNext())
    {
      e locale = (e)((Iterator)localObject).next();
      if (k.a(locale.a().d(), paramObject)) {
        locale.c();
      }
    }
    paramy = paramy.b().c();
    k.a(paramy, "dispatcher().runningCalls()");
    paramy = ((Iterable)paramy).iterator();
    while (paramy.hasNext())
    {
      localObject = (e)paramy.next();
      if (k.a(((e)localObject).a().d(), paramObject)) {
        ((e)localObject).c();
      }
    }
  }
  
  /* Error */
  private final byte[] a(Uri paramUri)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 52	com/truecaller/messaging/transport/im/g:b	Lcom/truecaller/util/g;
    //   4: aload_1
    //   5: invokeinterface 194 2 0
    //   10: astore_1
    //   11: aload_1
    //   12: ifnonnull +5 -> 17
    //   15: aconst_null
    //   16: areturn
    //   17: aload_0
    //   18: getfield 52	com/truecaller/messaging/transport/im/g:b	Lcom/truecaller/util/g;
    //   21: aload_1
    //   22: invokeinterface 196 2 0
    //   27: astore_2
    //   28: aload_1
    //   29: getstatic 202	android/net/Uri:EMPTY	Landroid/net/Uri;
    //   32: invokestatic 182	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/Object;)Z
    //   35: iconst_1
    //   36: ixor
    //   37: ifeq +23 -> 60
    //   40: aload_0
    //   41: getfield 62	com/truecaller/messaging/transport/im/g:g	Lcom/truecaller/messaging/data/providers/c;
    //   44: aload_1
    //   45: invokeinterface 207 2 0
    //   50: astore_1
    //   51: aload_1
    //   52: ifnull +8 -> 60
    //   55: aload_1
    //   56: invokevirtual 212	java/io/File:delete	()Z
    //   59: pop
    //   60: aload_2
    //   61: areturn
    //   62: astore_2
    //   63: aload_1
    //   64: getstatic 202	android/net/Uri:EMPTY	Landroid/net/Uri;
    //   67: invokestatic 182	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/Object;)Z
    //   70: iconst_1
    //   71: ixor
    //   72: ifeq +23 -> 95
    //   75: aload_0
    //   76: getfield 62	com/truecaller/messaging/transport/im/g:g	Lcom/truecaller/messaging/data/providers/c;
    //   79: aload_1
    //   80: invokeinterface 207 2 0
    //   85: astore_1
    //   86: aload_1
    //   87: ifnull +8 -> 95
    //   90: aload_1
    //   91: invokevirtual 212	java/io/File:delete	()Z
    //   94: pop
    //   95: aload_2
    //   96: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	97	0	this	g
    //   0	97	1	paramUri	Uri
    //   27	34	2	arrayOfByte	byte[]
    //   62	34	2	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   17	28	62	finally
  }
  
  public final Uri a(long paramLong, byte[] paramArrayOfByte, int paramInt1, int paramInt2, String paramString)
  {
    k.b(paramArrayOfByte, "thumbnail");
    k.b(paramString, "type");
    File localFile = g.a(paramLong);
    if (localFile != null)
    {
      com.truecaller.util.g localg = b;
      double d1 = paramInt1;
      Double.isNaN(d1);
      paramArrayOfByte = localg.a(paramArrayOfByte, paramInt1, paramInt2, (int)(d1 * 0.08D));
      if (paramArrayOfByte != null) {
        com.truecaller.utils.extensions.b.a(paramArrayOfByte, (c.g.a.b)new g.a(localFile));
      }
      paramArrayOfByte = g.a(localFile, paramString, false);
      if (paramArrayOfByte != null) {
        return paramArrayOfByte;
      }
      throw ((Throwable)new IOException("Invalid attachment's file URI"));
    }
    throw ((Throwable)new IOException("Can't create file for attachment"));
  }
  
  public final Entity a(MessageContent paramMessageContent, int paramInt)
  {
    k.b(paramMessageContent, "content");
    Object localObject1 = paramMessageContent.a();
    if (localObject1 != null)
    {
      Object localObject2;
      int i;
      int j;
      long l;
      switch (h.a[localObject1.ordinal()])
      {
      default: 
        break;
      case 5: 
        return null;
      case 4: 
        paramMessageContent = paramMessageContent.d();
        k.a(paramMessageContent, "content.vcard");
        paramMessageContent = Entity.a("text/vcard", paramInt, paramMessageContent.a(), paramMessageContent.b());
        k.a(paramMessageContent, "Entity.create(Entity.CON…atus, uri, size.toLong())");
        return paramMessageContent;
      case 3: 
        paramMessageContent = paramMessageContent.f();
        k.a(paramMessageContent, "content.audio");
        localObject1 = paramMessageContent.a();
        k.a(localObject1, "mimeType");
        localObject2 = Uri.parse(paramMessageContent.b());
        k.a(localObject2, "Uri.parse(uri)");
        return (Entity)new AudioEntity(-1L, (String)localObject1, paramInt, (Uri)localObject2, paramMessageContent.c(), paramMessageContent.d());
      case 2: 
        paramMessageContent = paramMessageContent.e();
        k.a(paramMessageContent, "content.video");
        localObject1 = paramMessageContent.g().toByteArray();
        k.a(localObject1, "thumbnail.toByteArray()");
        i = paramMessageContent.d();
        j = paramMessageContent.e();
        localObject2 = paramMessageContent.a();
        k.a(localObject2, "mimeType");
        Uri localUri = a(-1L, (byte[])localObject1, i, j, (String)localObject2);
        localObject1 = paramMessageContent.a();
        k.a(localObject1, "mimeType");
        localObject2 = paramMessageContent.b();
        k.a(localObject2, "uri");
        l = paramMessageContent.c();
        i = paramMessageContent.d();
        j = paramMessageContent.e();
        int k = paramMessageContent.f();
        paramMessageContent = localUri.toString();
        k.a(paramMessageContent, "thumbnailUrl.toString()");
        return (Entity)new VideoEntity(-1L, (String)localObject1, paramInt, (String)localObject2, l, i, j, k, paramMessageContent);
      case 1: 
        localObject2 = paramMessageContent.c();
        k.a(localObject2, "content.image");
        paramMessageContent = ((MessageContent.d)localObject2).a();
        k.a(paramMessageContent, "mimeType");
        localObject1 = ((MessageContent.d)localObject2).b();
        k.a(localObject1, "uri");
        l = ((MessageContent.d)localObject2).c();
        i = ((MessageContent.d)localObject2).d();
        j = ((MessageContent.d)localObject2).e();
        localObject2 = ((MessageContent.d)localObject2).f().toByteArray();
        k.a(localObject2, "thumbnail.toByteArray()");
        return (Entity)new ImageEntityWithThumbnail(paramMessageContent, paramInt, (String)localObject1, l, i, j, (byte[])localObject2);
      }
    }
    localObject1 = new StringBuilder("Found unsupported attachment ");
    ((StringBuilder)localObject1).append(paramMessageContent.a());
    ((StringBuilder)localObject1).toString();
    return null;
  }
  
  /* Error */
  public final void a(com.truecaller.api.services.messenger.v1.models.input.InputMessageContent.c paramc, BinaryEntity paramBinaryEntity, com.truecaller.messaging.data.types.Message paramMessage)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 390
    //   4: invokestatic 33	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   7: aload_2
    //   8: ldc_w 392
    //   11: invokestatic 33	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   14: aload_3
    //   15: ldc_w 394
    //   18: invokestatic 33	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   21: aload_2
    //   22: checkcast 95	com/truecaller/messaging/data/types/Entity
    //   25: astore 9
    //   27: aload 9
    //   29: invokevirtual 396	com/truecaller/messaging/data/types/Entity:b	()Z
    //   32: ifne +36 -> 68
    //   35: aload 9
    //   37: invokevirtual 398	com/truecaller/messaging/data/types/Entity:c	()Z
    //   40: ifne +28 -> 68
    //   43: aload 9
    //   45: invokevirtual 400	com/truecaller/messaging/data/types/Entity:d	()Z
    //   48: ifne +20 -> 68
    //   51: aload 9
    //   53: invokevirtual 402	com/truecaller/messaging/data/types/Entity:e	()Z
    //   56: ifeq +6 -> 62
    //   59: goto +9 -> 68
    //   62: iconst_0
    //   63: istore 4
    //   65: goto +6 -> 71
    //   68: iconst_1
    //   69: istore 4
    //   71: iload 4
    //   73: ifne +4 -> 77
    //   76: return
    //   77: aload_0
    //   78: getfield 58	com/truecaller/messaging/transport/im/g:e	Lcom/truecaller/messaging/transport/im/cb;
    //   81: invokestatic 407	com/truecaller/network/d/j$a:a	(Lcom/truecaller/network/d/j;)Lio/grpc/c/a;
    //   84: checkcast 409	com/truecaller/api/services/messenger/v1/k$a
    //   87: astore_3
    //   88: aload_3
    //   89: ifnonnull +4 -> 93
    //   92: return
    //   93: aload_3
    //   94: invokestatic 414	com/truecaller/api/services/messenger/v1/MediaHandles$Request:a	()Lcom/truecaller/api/services/messenger/v1/MediaHandles$Request$a;
    //   97: aload_2
    //   98: getfield 71	com/truecaller/messaging/data/types/BinaryEntity:d	J
    //   101: invokevirtual 419	com/truecaller/api/services/messenger/v1/MediaHandles$Request$a:a	(J)Lcom/truecaller/api/services/messenger/v1/MediaHandles$Request$a;
    //   104: aload_2
    //   105: getfield 420	com/truecaller/messaging/data/types/BinaryEntity:j	Ljava/lang/String;
    //   108: invokevirtual 423	com/truecaller/api/services/messenger/v1/MediaHandles$Request$a:a	(Ljava/lang/String;)Lcom/truecaller/api/services/messenger/v1/MediaHandles$Request$a;
    //   111: invokevirtual 427	com/truecaller/api/services/messenger/v1/MediaHandles$Request$a:build	()Lcom/google/f/q;
    //   114: checkcast 411	com/truecaller/api/services/messenger/v1/MediaHandles$Request
    //   117: invokevirtual 430	com/truecaller/api/services/messenger/v1/k$a:a	(Lcom/truecaller/api/services/messenger/v1/MediaHandles$Request;)Lcom/truecaller/api/services/messenger/v1/MediaHandles$c;
    //   120: astore 8
    //   122: aload 8
    //   124: ldc_w 432
    //   127: invokestatic 136	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   130: aload 8
    //   132: invokevirtual 437	com/truecaller/api/services/messenger/v1/MediaHandles$c:c	()Ljava/util/Map;
    //   135: astore 10
    //   137: aload 10
    //   139: ldc_w 439
    //   142: invokestatic 136	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   145: aload 8
    //   147: invokevirtual 440	com/truecaller/api/services/messenger/v1/MediaHandles$c:a	()Ljava/lang/String;
    //   150: astore_3
    //   151: aload_3
    //   152: ldc_w 442
    //   155: invokestatic 136	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   158: new 444	okhttp3/x$a
    //   161: dup
    //   162: invokespecial 445	okhttp3/x$a:<init>	()V
    //   165: getstatic 450	okhttp3/x:e	Lokhttp3/w;
    //   168: invokevirtual 453	okhttp3/x$a:a	(Lokhttp3/w;)Lokhttp3/x$a;
    //   171: astore 7
    //   173: aload 10
    //   175: invokeinterface 459 1 0
    //   180: invokeinterface 462 1 0
    //   185: astore 10
    //   187: aload 10
    //   189: invokeinterface 166 1 0
    //   194: ifeq +44 -> 238
    //   197: aload 10
    //   199: invokeinterface 170 1 0
    //   204: checkcast 464	java/util/Map$Entry
    //   207: astore 11
    //   209: aload 7
    //   211: aload 11
    //   213: invokeinterface 467 1 0
    //   218: checkcast 469	java/lang/String
    //   221: aload 11
    //   223: invokeinterface 472 1 0
    //   228: checkcast 469	java/lang/String
    //   231: invokevirtual 475	okhttp3/x$a:a	(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/x$a;
    //   234: pop
    //   235: goto -48 -> 187
    //   238: aload_2
    //   239: getfield 477	com/truecaller/messaging/data/types/BinaryEntity:b	Landroid/net/Uri;
    //   242: astore 10
    //   244: aload 10
    //   246: ldc_w 479
    //   249: invokestatic 136	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   252: aload 10
    //   254: invokevirtual 482	android/net/Uri:getPathSegments	()Ljava/util/List;
    //   257: astore 10
    //   259: aload 10
    //   261: ldc_w 484
    //   264: invokestatic 136	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   267: aload 7
    //   269: ldc_w 486
    //   272: aload 10
    //   274: invokestatic 491	c/a/m:f	(Ljava/util/List;)Ljava/lang/Object;
    //   277: checkcast 469	java/lang/String
    //   280: new 493	com/truecaller/messaging/transport/im/c
    //   283: dup
    //   284: aload_0
    //   285: getfield 50	com/truecaller/messaging/transport/im/g:a	Landroid/content/ContentResolver;
    //   288: aload_2
    //   289: invokespecial 496	com/truecaller/messaging/transport/im/c:<init>	(Landroid/content/ContentResolver;Lcom/truecaller/messaging/data/types/BinaryEntity;)V
    //   292: checkcast 498	okhttp3/ac
    //   295: invokevirtual 501	okhttp3/x$a:a	(Ljava/lang/String;Ljava/lang/String;Lokhttp3/ac;)Lokhttp3/x$a;
    //   298: invokevirtual 504	okhttp3/x$a:a	()Lokhttp3/x;
    //   301: astore 7
    //   303: aload 7
    //   305: ldc_w 506
    //   308: invokestatic 136	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   311: aload 7
    //   313: checkcast 498	okhttp3/ac
    //   316: astore 7
    //   318: new 508	okhttp3/ab$a
    //   321: dup
    //   322: invokespecial 509	okhttp3/ab$a:<init>	()V
    //   325: aload_3
    //   326: invokevirtual 512	okhttp3/ab$a:a	(Ljava/lang/String;)Lokhttp3/ab$a;
    //   329: aload_2
    //   330: getfield 515	com/truecaller/messaging/data/types/BinaryEntity:h	J
    //   333: invokestatic 520	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   336: invokevirtual 523	okhttp3/ab$a:a	(Ljava/lang/Object;)Lokhttp3/ab$a;
    //   339: aload 7
    //   341: invokevirtual 526	okhttp3/ab$a:a	(Lokhttp3/ac;)Lokhttp3/ab$a;
    //   344: invokevirtual 527	okhttp3/ab$a:a	()Lokhttp3/ab;
    //   347: astore_3
    //   348: invokestatic 533	java/lang/System:currentTimeMillis	()J
    //   351: lstore 5
    //   353: aload_0
    //   354: getfield 54	com/truecaller/messaging/transport/im/g:c	Lokhttp3/y;
    //   357: aload_3
    //   358: invokevirtual 536	okhttp3/y:a	(Lokhttp3/ab;)Lokhttp3/e;
    //   361: astore 10
    //   363: aload 10
    //   365: ldc_w 538
    //   368: invokestatic 136	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   371: aload 10
    //   373: invokestatic 544	com/google/firebase/perf/network/FirebasePerfOkHttpClient:execute	(Lokhttp3/e;)Lokhttp3/ad;
    //   376: checkcast 546	java/io/Closeable
    //   379: astore 11
    //   381: aconst_null
    //   382: astore 7
    //   384: aload 7
    //   386: astore_3
    //   387: aload 11
    //   389: checkcast 548	okhttp3/ad
    //   392: astore 12
    //   394: aload 7
    //   396: astore_3
    //   397: aload 12
    //   399: ldc_w 550
    //   402: invokestatic 136	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   405: aload 7
    //   407: astore_3
    //   408: aload 12
    //   410: invokevirtual 551	okhttp3/ad:c	()Z
    //   413: ifne +107 -> 520
    //   416: aload 7
    //   418: astore_3
    //   419: aload_0
    //   420: aload_2
    //   421: checkcast 95	com/truecaller/messaging/data/types/Entity
    //   424: invokestatic 533	java/lang/System:currentTimeMillis	()J
    //   427: lload 5
    //   429: lsub
    //   430: ldc_w 553
    //   433: invokespecial 555	com/truecaller/messaging/transport/im/g:a	(Lcom/truecaller/messaging/data/types/Entity;JLjava/lang/String;)V
    //   436: aload 7
    //   438: astore_3
    //   439: aload 12
    //   441: ldc_w 550
    //   444: invokestatic 33	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   447: aload 7
    //   449: astore_3
    //   450: aload 12
    //   452: invokevirtual 556	okhttp3/ad:b	()I
    //   455: sipush 400
    //   458: if_icmpne +26 -> 484
    //   461: aload 7
    //   463: astore_3
    //   464: aload 12
    //   466: invokevirtual 559	okhttp3/ad:d	()Lokhttp3/ae;
    //   469: invokestatic 564	com/truecaller/messaging/transport/im/cl:a	(Lokhttp3/ae;)Ljava/lang/String;
    //   472: ldc_w 566
    //   475: invokestatic 182	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/Object;)Z
    //   478: ifne +557 -> 1035
    //   481: goto +3 -> 484
    //   484: aload 7
    //   486: astore_3
    //   487: aload 12
    //   489: invokevirtual 556	okhttp3/ad:b	()I
    //   492: sipush 403
    //   495: if_icmpne +546 -> 1041
    //   498: iconst_2
    //   499: istore 4
    //   501: goto +3 -> 504
    //   504: aload 7
    //   506: astore_3
    //   507: new 568	com/truecaller/messaging/transport/im/cj
    //   510: dup
    //   511: iload 4
    //   513: invokespecial 571	com/truecaller/messaging/transport/im/cj:<init>	(I)V
    //   516: checkcast 251	java/lang/Throwable
    //   519: athrow
    //   520: aload 7
    //   522: astore_3
    //   523: getstatic 576	c/x:a	Lc/x;
    //   526: astore 7
    //   528: aload 11
    //   530: aconst_null
    //   531: invokestatic 581	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   534: aload_0
    //   535: aload 9
    //   537: invokestatic 533	java/lang/System:currentTimeMillis	()J
    //   540: lload 5
    //   542: lsub
    //   543: ldc_w 583
    //   546: invokespecial 555	com/truecaller/messaging/transport/im/g:a	(Lcom/truecaller/messaging/data/types/Entity;JLjava/lang/String;)V
    //   549: aload_2
    //   550: invokevirtual 584	com/truecaller/messaging/data/types/BinaryEntity:b	()Z
    //   553: ifeq +130 -> 683
    //   556: aload_2
    //   557: checkcast 586	com/truecaller/messaging/data/types/ImageEntity
    //   560: astore_3
    //   561: aload 8
    //   563: invokevirtual 587	com/truecaller/api/services/messenger/v1/MediaHandles$c:b	()Ljava/lang/String;
    //   566: astore 7
    //   568: aload 7
    //   570: ldc_w 589
    //   573: invokestatic 136	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   576: aload_0
    //   577: getfield 52	com/truecaller/messaging/transport/im/g:b	Lcom/truecaller/util/g;
    //   580: astore 8
    //   582: aload_2
    //   583: getfield 477	com/truecaller/messaging/data/types/BinaryEntity:b	Landroid/net/Uri;
    //   586: astore_2
    //   587: aload_2
    //   588: ldc_w 479
    //   591: invokestatic 136	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   594: aload 8
    //   596: aload_2
    //   597: invokeinterface 196 2 0
    //   602: astore_2
    //   603: invokestatic 594	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d:a	()Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a;
    //   606: aload 7
    //   608: invokevirtual 599	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a:b	(Ljava/lang/String;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a;
    //   611: aload_3
    //   612: getfield 600	com/truecaller/messaging/data/types/ImageEntity:j	Ljava/lang/String;
    //   615: invokevirtual 602	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a:a	(Ljava/lang/String;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a;
    //   618: aload_3
    //   619: getfield 605	com/truecaller/messaging/data/types/ImageEntity:a	I
    //   622: invokevirtual 608	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a:b	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a;
    //   625: aload_3
    //   626: getfield 611	com/truecaller/messaging/data/types/ImageEntity:k	I
    //   629: invokevirtual 613	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a:c	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a;
    //   632: aload_3
    //   633: getfield 614	com/truecaller/messaging/data/types/ImageEntity:d	J
    //   636: l2i
    //   637: invokevirtual 616	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a:a	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a;
    //   640: astore_3
    //   641: aload_2
    //   642: ifnull +19 -> 661
    //   645: aload_3
    //   646: ldc_w 390
    //   649: invokestatic 136	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   652: aload_3
    //   653: aload_2
    //   654: invokestatic 620	com/google/f/f:copyFrom	([B)Lcom/google/f/f;
    //   657: invokevirtual 623	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a:a	(Lcom/google/f/f;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a;
    //   660: pop
    //   661: aload_3
    //   662: invokevirtual 624	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a:build	()Lcom/google/f/q;
    //   665: astore_2
    //   666: aload_2
    //   667: ldc_w 626
    //   670: invokestatic 136	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   673: aload_1
    //   674: aload_2
    //   675: checkcast 591	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d
    //   678: invokevirtual 631	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c:a	(Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c;
    //   681: pop
    //   682: return
    //   683: aload_2
    //   684: invokevirtual 632	com/truecaller/messaging/data/types/BinaryEntity:c	()Z
    //   687: ifeq +128 -> 815
    //   690: aload_2
    //   691: checkcast 355	com/truecaller/messaging/data/types/VideoEntity
    //   694: astore_3
    //   695: aload 8
    //   697: invokevirtual 587	com/truecaller/api/services/messenger/v1/MediaHandles$c:b	()Ljava/lang/String;
    //   700: astore 7
    //   702: aload 7
    //   704: ldc_w 589
    //   707: invokestatic 136	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   710: aload_2
    //   711: getfield 477	com/truecaller/messaging/data/types/BinaryEntity:b	Landroid/net/Uri;
    //   714: astore_2
    //   715: aload_2
    //   716: ldc_w 479
    //   719: invokestatic 136	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   722: aload_0
    //   723: aload_2
    //   724: invokespecial 634	com/truecaller/messaging/transport/im/g:a	(Landroid/net/Uri;)[B
    //   727: astore_2
    //   728: invokestatic 639	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j:a	()Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a;
    //   731: aload 7
    //   733: invokevirtual 644	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a:b	(Ljava/lang/String;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a;
    //   736: aload_3
    //   737: getfield 645	com/truecaller/messaging/data/types/VideoEntity:j	Ljava/lang/String;
    //   740: invokevirtual 647	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a:a	(Ljava/lang/String;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a;
    //   743: aload_3
    //   744: getfield 648	com/truecaller/messaging/data/types/VideoEntity:d	J
    //   747: l2i
    //   748: invokevirtual 651	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a:a	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a;
    //   751: aload_3
    //   752: getfield 652	com/truecaller/messaging/data/types/VideoEntity:a	I
    //   755: invokevirtual 654	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a:b	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a;
    //   758: aload_3
    //   759: getfield 655	com/truecaller/messaging/data/types/VideoEntity:k	I
    //   762: invokevirtual 657	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a:c	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a;
    //   765: aload_3
    //   766: getfield 660	com/truecaller/messaging/data/types/VideoEntity:l	I
    //   769: invokevirtual 662	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a:d	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a;
    //   772: astore_3
    //   773: aload_2
    //   774: ifnull +19 -> 793
    //   777: aload_3
    //   778: ldc_w 390
    //   781: invokestatic 136	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   784: aload_3
    //   785: aload_2
    //   786: invokestatic 620	com/google/f/f:copyFrom	([B)Lcom/google/f/f;
    //   789: invokevirtual 665	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a:a	(Lcom/google/f/f;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a;
    //   792: pop
    //   793: aload_3
    //   794: invokevirtual 666	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a:build	()Lcom/google/f/q;
    //   797: astore_2
    //   798: aload_2
    //   799: ldc_w 626
    //   802: invokestatic 136	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   805: aload_1
    //   806: aload_2
    //   807: checkcast 636	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j
    //   810: invokevirtual 669	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c:a	(Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c;
    //   813: pop
    //   814: return
    //   815: aload_2
    //   816: invokevirtual 670	com/truecaller/messaging/data/types/BinaryEntity:e	()Z
    //   819: ifeq +71 -> 890
    //   822: aload_2
    //   823: checkcast 311	com/truecaller/messaging/data/types/AudioEntity
    //   826: astore_2
    //   827: aload 8
    //   829: invokevirtual 587	com/truecaller/api/services/messenger/v1/MediaHandles$c:b	()Ljava/lang/String;
    //   832: astore_3
    //   833: aload_3
    //   834: ldc_w 589
    //   837: invokestatic 136	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   840: invokestatic 675	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:a	()Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a;
    //   843: aload_3
    //   844: invokevirtual 680	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a:b	(Ljava/lang/String;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a;
    //   847: aload_2
    //   848: getfield 681	com/truecaller/messaging/data/types/AudioEntity:j	Ljava/lang/String;
    //   851: invokevirtual 683	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a:a	(Ljava/lang/String;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a;
    //   854: aload_2
    //   855: getfield 684	com/truecaller/messaging/data/types/AudioEntity:d	J
    //   858: l2i
    //   859: invokevirtual 687	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a:a	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a;
    //   862: aload_2
    //   863: getfield 688	com/truecaller/messaging/data/types/AudioEntity:a	I
    //   866: invokevirtual 690	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a:b	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a;
    //   869: invokevirtual 691	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a:build	()Lcom/google/f/q;
    //   872: astore_2
    //   873: aload_2
    //   874: ldc_w 626
    //   877: invokestatic 136	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   880: aload_1
    //   881: aload_2
    //   882: checkcast 672	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a
    //   885: invokevirtual 694	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c:a	(Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c;
    //   888: pop
    //   889: return
    //   890: aload_2
    //   891: invokevirtual 695	com/truecaller/messaging/data/types/BinaryEntity:d	()Z
    //   894: ifeq +52 -> 946
    //   897: aload 8
    //   899: invokevirtual 587	com/truecaller/api/services/messenger/v1/MediaHandles$c:b	()Ljava/lang/String;
    //   902: astore_3
    //   903: aload_3
    //   904: ldc_w 589
    //   907: invokestatic 136	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   910: invokestatic 700	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h:a	()Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h$a;
    //   913: aload_3
    //   914: invokevirtual 705	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h$a:a	(Ljava/lang/String;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h$a;
    //   917: aload_2
    //   918: getfield 71	com/truecaller/messaging/data/types/BinaryEntity:d	J
    //   921: l2i
    //   922: invokevirtual 708	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h$a:a	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h$a;
    //   925: invokevirtual 709	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h$a:build	()Lcom/google/f/q;
    //   928: astore_2
    //   929: aload_2
    //   930: ldc_w 711
    //   933: invokestatic 136	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   936: aload_1
    //   937: aload_2
    //   938: checkcast 697	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h
    //   941: invokevirtual 714	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c:a	(Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c;
    //   944: pop
    //   945: return
    //   946: new 379	java/lang/StringBuilder
    //   949: dup
    //   950: ldc_w 716
    //   953: invokespecial 382	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   956: astore_1
    //   957: aload_1
    //   958: aload_2
    //   959: getfield 420	com/truecaller/messaging/data/types/BinaryEntity:j	Ljava/lang/String;
    //   962: invokevirtual 719	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   965: pop
    //   966: iconst_1
    //   967: anewarray 469	java/lang/String
    //   970: dup
    //   971: iconst_0
    //   972: aload_1
    //   973: invokevirtual 387	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   976: aastore
    //   977: invokestatic 725	com/truecaller/log/AssertionUtil$OnlyInDebug:fail	([Ljava/lang/String;)V
    //   980: return
    //   981: astore_1
    //   982: goto +8 -> 990
    //   985: astore_1
    //   986: aload_1
    //   987: astore_3
    //   988: aload_1
    //   989: athrow
    //   990: aload 11
    //   992: aload_3
    //   993: invokestatic 581	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   996: aload_1
    //   997: athrow
    //   998: aload 10
    //   1000: invokeinterface 726 1 0
    //   1005: ifeq +14 -> 1019
    //   1008: new 728	java/util/concurrent/CancellationException
    //   1011: dup
    //   1012: invokespecial 729	java/util/concurrent/CancellationException:<init>	()V
    //   1015: checkcast 251	java/lang/Throwable
    //   1018: athrow
    //   1019: new 568	com/truecaller/messaging/transport/im/cj
    //   1022: dup
    //   1023: iconst_1
    //   1024: invokespecial 571	com/truecaller/messaging/transport/im/cj:<init>	(I)V
    //   1027: checkcast 251	java/lang/Throwable
    //   1030: athrow
    //   1031: astore_1
    //   1032: goto -34 -> 998
    //   1035: iconst_3
    //   1036: istore 4
    //   1038: goto -534 -> 504
    //   1041: iconst_1
    //   1042: istore 4
    //   1044: goto -540 -> 504
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1047	0	this	g
    //   0	1047	1	paramc	com.truecaller.api.services.messenger.v1.models.input.InputMessageContent.c
    //   0	1047	2	paramBinaryEntity	BinaryEntity
    //   0	1047	3	paramMessage	com.truecaller.messaging.data.types.Message
    //   63	980	4	i	int
    //   351	190	5	l	long
    //   171	561	7	localObject1	Object
    //   120	778	8	localObject2	Object
    //   25	511	9	localEntity	Entity
    //   135	864	10	localObject3	Object
    //   207	784	11	localObject4	Object
    //   392	96	12	localad	okhttp3.ad
    // Exception table:
    //   from	to	target	type
    //   387	394	981	finally
    //   397	405	981	finally
    //   408	416	981	finally
    //   419	436	981	finally
    //   439	447	981	finally
    //   450	461	981	finally
    //   464	481	981	finally
    //   487	498	981	finally
    //   507	520	981	finally
    //   523	528	981	finally
    //   988	990	981	finally
    //   387	394	985	java/lang/Throwable
    //   397	405	985	java/lang/Throwable
    //   408	416	985	java/lang/Throwable
    //   419	436	985	java/lang/Throwable
    //   439	447	985	java/lang/Throwable
    //   450	461	985	java/lang/Throwable
    //   464	481	985	java/lang/Throwable
    //   487	498	985	java/lang/Throwable
    //   507	520	985	java/lang/Throwable
    //   523	528	985	java/lang/Throwable
    //   371	381	1031	java/io/IOException
    //   528	534	1031	java/io/IOException
    //   990	998	1031	java/io/IOException
  }
  
  public final void a(BinaryEntity paramBinaryEntity)
  {
    k.b(paramBinaryEntity, "entity");
    StringBuilder localStringBuilder = new StringBuilder("Cancel uploading entity: ");
    localStringBuilder.append(h);
    localStringBuilder.toString();
    a(c, Long.valueOf(h));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */