package com.truecaller.messaging.transport.im;

import android.content.ContentResolver;
import androidx.work.p;
import com.truecaller.analytics.ae;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.h;
import com.truecaller.messaging.transport.ad.b;
import com.truecaller.messaging.transport.im.a.a;
import com.truecaller.search.local.model.g;
import com.truecaller.util.ch;
import com.truecaller.utils.i;
import dagger.a.d;
import javax.inject.Provider;

public final class bi
  implements d<bd>
{
  private final Provider<ContentResolver> a;
  private final Provider<com.truecaller.messaging.data.c> b;
  private final Provider<cb> c;
  private final Provider<com.truecaller.androidactors.f<t>> d;
  private final Provider<ad.b> e;
  private final Provider<h> f;
  private final Provider<bb> g;
  private final Provider<com.truecaller.androidactors.f<q>> h;
  private final Provider<com.truecaller.messaging.data.providers.c> i;
  private final Provider<i> j;
  private final Provider<com.truecaller.androidactors.f<ae>> k;
  private final Provider<com.truecaller.analytics.b> l;
  private final Provider<f> m;
  private final Provider<bu> n;
  private final Provider<com.truecaller.androidactors.f<bp>> o;
  private final Provider<com.truecaller.androidactors.f<as>> p;
  private final Provider<com.truecaller.androidactors.f<bj>> q;
  private final Provider<e> r;
  private final Provider<com.truecaller.messaging.categorizer.b> s;
  private final Provider<com.truecaller.util.f.b> t;
  private final Provider<ch> u;
  private final Provider<g> v;
  private final Provider<p> w;
  private final Provider<com.truecaller.androidactors.f<a>> x;
  
  private bi(Provider<ContentResolver> paramProvider, Provider<com.truecaller.messaging.data.c> paramProvider1, Provider<cb> paramProvider2, Provider<com.truecaller.androidactors.f<t>> paramProvider3, Provider<ad.b> paramProvider4, Provider<h> paramProvider5, Provider<bb> paramProvider6, Provider<com.truecaller.androidactors.f<q>> paramProvider7, Provider<com.truecaller.messaging.data.providers.c> paramProvider8, Provider<i> paramProvider9, Provider<com.truecaller.androidactors.f<ae>> paramProvider10, Provider<com.truecaller.analytics.b> paramProvider11, Provider<f> paramProvider12, Provider<bu> paramProvider13, Provider<com.truecaller.androidactors.f<bp>> paramProvider14, Provider<com.truecaller.androidactors.f<as>> paramProvider15, Provider<com.truecaller.androidactors.f<bj>> paramProvider16, Provider<e> paramProvider17, Provider<com.truecaller.messaging.categorizer.b> paramProvider18, Provider<com.truecaller.util.f.b> paramProvider19, Provider<ch> paramProvider20, Provider<g> paramProvider21, Provider<p> paramProvider22, Provider<com.truecaller.androidactors.f<a>> paramProvider23)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
    k = paramProvider10;
    l = paramProvider11;
    m = paramProvider12;
    n = paramProvider13;
    o = paramProvider14;
    p = paramProvider15;
    q = paramProvider16;
    r = paramProvider17;
    s = paramProvider18;
    t = paramProvider19;
    u = paramProvider20;
    v = paramProvider21;
    w = paramProvider22;
    x = paramProvider23;
  }
  
  public static bi a(Provider<ContentResolver> paramProvider, Provider<com.truecaller.messaging.data.c> paramProvider1, Provider<cb> paramProvider2, Provider<com.truecaller.androidactors.f<t>> paramProvider3, Provider<ad.b> paramProvider4, Provider<h> paramProvider5, Provider<bb> paramProvider6, Provider<com.truecaller.androidactors.f<q>> paramProvider7, Provider<com.truecaller.messaging.data.providers.c> paramProvider8, Provider<i> paramProvider9, Provider<com.truecaller.androidactors.f<ae>> paramProvider10, Provider<com.truecaller.analytics.b> paramProvider11, Provider<f> paramProvider12, Provider<bu> paramProvider13, Provider<com.truecaller.androidactors.f<bp>> paramProvider14, Provider<com.truecaller.androidactors.f<as>> paramProvider15, Provider<com.truecaller.androidactors.f<bj>> paramProvider16, Provider<e> paramProvider17, Provider<com.truecaller.messaging.categorizer.b> paramProvider18, Provider<com.truecaller.util.f.b> paramProvider19, Provider<ch> paramProvider20, Provider<g> paramProvider21, Provider<p> paramProvider22, Provider<com.truecaller.androidactors.f<a>> paramProvider23)
  {
    return new bi(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10, paramProvider11, paramProvider12, paramProvider13, paramProvider14, paramProvider15, paramProvider16, paramProvider17, paramProvider18, paramProvider19, paramProvider20, paramProvider21, paramProvider22, paramProvider23);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bi
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */