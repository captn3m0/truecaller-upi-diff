package com.truecaller.messaging.transport.im;

import android.content.Context;
import androidx.work.p;
import com.truecaller.featuretoggles.e;
import com.truecaller.multisim.h;
import dagger.a.d;
import javax.inject.Provider;

public final class ai
  implements d<bb>
{
  private final Provider<Context> a;
  private final Provider<h> b;
  private final Provider<e> c;
  private final Provider<p> d;
  
  private ai(Provider<Context> paramProvider, Provider<h> paramProvider1, Provider<e> paramProvider2, Provider<p> paramProvider3)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static ai a(Provider<Context> paramProvider, Provider<h> paramProvider1, Provider<e> paramProvider2, Provider<p> paramProvider3)
  {
    return new ai(paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.ai
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */