package com.truecaller.messaging.transport.im;

import android.content.ContentResolver;
import com.truecaller.messaging.data.c;
import com.truecaller.messaging.h;
import com.truecaller.messaging.notifications.g;
import dagger.a.d;
import javax.inject.Provider;

public final class av
  implements d<au>
{
  private final Provider<ContentResolver> a;
  private final Provider<c> b;
  private final Provider<h> c;
  private final Provider<g> d;
  
  private av(Provider<ContentResolver> paramProvider, Provider<c> paramProvider1, Provider<h> paramProvider2, Provider<g> paramProvider3)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static av a(Provider<ContentResolver> paramProvider, Provider<c> paramProvider1, Provider<h> paramProvider2, Provider<g> paramProvider3)
  {
    return new av(paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.av
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */