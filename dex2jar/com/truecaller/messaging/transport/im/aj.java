package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import dagger.a.d;
import javax.inject.Provider;

public final class aj
  implements d<f<bj>>
{
  private final Provider<bj> a;
  private final Provider<i> b;
  
  private aj(Provider<bj> paramProvider, Provider<i> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static aj a(Provider<bj> paramProvider, Provider<i> paramProvider1)
  {
    return new aj(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.aj
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */