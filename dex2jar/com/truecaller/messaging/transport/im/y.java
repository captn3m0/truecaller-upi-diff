package com.truecaller.messaging.transport.im;

import android.content.Context;
import com.truecaller.util.h;
import dagger.a.d;
import javax.inject.Provider;

public final class y
  implements d<com.truecaller.util.g>
{
  private final Provider<Context> a;
  
  private y(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static y a(Provider<Context> paramProvider)
  {
    return new y(paramProvider);
  }
  
  public static com.truecaller.util.g a(Context paramContext)
  {
    return (com.truecaller.util.g)dagger.a.g.a(new h(paramContext), "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.y
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */