package com.truecaller.messaging.transport.im;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import c.g.b.k;
import com.google.f.x;
import com.truecaller.androidactors.w;
import com.truecaller.api.services.messenger.v1.events.Event;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.x;
import com.truecaller.log.AssertionUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;

public final class bl
  implements bj
{
  private final ContentResolver a;
  private final bu b;
  private final m c;
  
  @Inject
  public bl(ContentResolver paramContentResolver, bu parambu, m paramm)
  {
    a = paramContentResolver;
    b = parambu;
    c = paramm;
  }
  
  /* Error */
  @android.annotation.SuppressLint({"Recycle"})
  private final List<bl.a> a(int paramInt)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 33	com/truecaller/messaging/transport/im/bl:a	Landroid/content/ContentResolver;
    //   4: invokestatic 50	com/truecaller/content/TruecallerContract$x:a	()Landroid/net/Uri;
    //   7: iconst_2
    //   8: anewarray 52	java/lang/String
    //   11: dup
    //   12: iconst_0
    //   13: ldc 54
    //   15: aastore
    //   16: dup
    //   17: iconst_1
    //   18: ldc 56
    //   20: aastore
    //   21: ldc 58
    //   23: iconst_1
    //   24: anewarray 52	java/lang/String
    //   27: dup
    //   28: iconst_0
    //   29: iload_1
    //   30: invokestatic 62	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   33: aastore
    //   34: ldc 64
    //   36: invokevirtual 70	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   39: astore 7
    //   41: aload 7
    //   43: ifnull +170 -> 213
    //   46: aload 7
    //   48: checkcast 72	java/io/Closeable
    //   51: astore 6
    //   53: aconst_null
    //   54: astore 5
    //   56: aload 5
    //   58: astore 4
    //   60: new 74	java/util/ArrayList
    //   63: dup
    //   64: invokespecial 75	java/util/ArrayList:<init>	()V
    //   67: checkcast 77	java/util/Collection
    //   70: astore 8
    //   72: aload 5
    //   74: astore 4
    //   76: aload 7
    //   78: invokeinterface 83 1 0
    //   83: ifeq +67 -> 150
    //   86: aload 5
    //   88: astore 4
    //   90: aload 7
    //   92: iconst_0
    //   93: invokeinterface 87 2 0
    //   98: lstore_2
    //   99: aload 5
    //   101: astore 4
    //   103: aload 7
    //   105: iconst_1
    //   106: invokeinterface 91 2 0
    //   111: astore 9
    //   113: aload 5
    //   115: astore 4
    //   117: aload 9
    //   119: ldc 93
    //   121: invokestatic 95	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   124: aload 5
    //   126: astore 4
    //   128: aload 8
    //   130: new 8	com/truecaller/messaging/transport/im/bl$a
    //   133: dup
    //   134: lload_2
    //   135: aload 9
    //   137: iload_1
    //   138: invokespecial 98	com/truecaller/messaging/transport/im/bl$a:<init>	(J[BI)V
    //   141: invokeinterface 102 2 0
    //   146: pop
    //   147: goto -75 -> 72
    //   150: aload 5
    //   152: astore 4
    //   154: aload 8
    //   156: checkcast 104	java/util/List
    //   159: astore 5
    //   161: aload 6
    //   163: aconst_null
    //   164: invokestatic 109	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   167: aload 5
    //   169: checkcast 111	java/lang/Iterable
    //   172: invokestatic 117	c/a/m:g	(Ljava/lang/Iterable;)Ljava/util/List;
    //   175: astore 5
    //   177: aload 5
    //   179: astore 4
    //   181: aload 5
    //   183: ifnonnull +38 -> 221
    //   186: goto +27 -> 213
    //   189: astore 5
    //   191: goto +12 -> 203
    //   194: astore 5
    //   196: aload 5
    //   198: astore 4
    //   200: aload 5
    //   202: athrow
    //   203: aload 6
    //   205: aload 4
    //   207: invokestatic 109	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   210: aload 5
    //   212: athrow
    //   213: getstatic 122	c/a/y:a	Lc/a/y;
    //   216: checkcast 104	java/util/List
    //   219: astore 4
    //   221: aload 4
    //   223: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	224	0	this	bl
    //   0	224	1	paramInt	int
    //   98	37	2	l	long
    //   58	164	4	localObject1	Object
    //   54	128	5	localList	List
    //   189	1	5	localObject2	Object
    //   194	17	5	localThrowable	Throwable
    //   51	153	6	localCloseable	java.io.Closeable
    //   39	65	7	localCursor	android.database.Cursor
    //   70	85	8	localCollection	Collection
    //   111	25	9	arrayOfByte	byte[]
    // Exception table:
    //   from	to	target	type
    //   60	72	189	finally
    //   76	86	189	finally
    //   90	99	189	finally
    //   103	113	189	finally
    //   117	124	189	finally
    //   128	147	189	finally
    //   154	161	189	finally
    //   200	203	189	finally
    //   60	72	194	java/lang/Throwable
    //   76	86	194	java/lang/Throwable
    //   90	99	194	java/lang/Throwable
    //   103	113	194	java/lang/Throwable
    //   117	124	194	java/lang/Throwable
    //   128	147	194	java/lang/Throwable
    //   154	161	194	java/lang/Throwable
  }
  
  private final boolean a(List<Long> paramList)
  {
    if (paramList.isEmpty()) {
      return false;
    }
    ArrayList localArrayList = new ArrayList();
    Object localObject = (Iterable)paramList;
    paramList = (Collection)new ArrayList(c.a.m.a((Iterable)localObject, 10));
    localObject = ((Iterable)localObject).iterator();
    while (((Iterator)localObject).hasNext())
    {
      long l = ((Number)((Iterator)localObject).next()).longValue();
      paramList.add(ContentProviderOperation.newDelete(TruecallerContract.x.a()).withSelection("_id=?", new String[] { String.valueOf(l) }).build());
    }
    localArrayList.addAll((Collection)paramList);
    paramList = a(localArrayList);
    if (paramList != null)
    {
      int i;
      if (paramList.length == 0) {
        i = 1;
      } else {
        i = 0;
      }
      if (i == 0) {
        return true;
      }
    }
    return false;
  }
  
  private final ContentProviderResult[] a(ArrayList<ContentProviderOperation> paramArrayList)
  {
    try
    {
      paramArrayList = a.applyBatch(TruecallerContract.a(), paramArrayList);
      return paramArrayList;
    }
    catch (RemoteException|OperationApplicationException paramArrayList)
    {
      for (;;) {}
    }
    return null;
  }
  
  public final w<Boolean> a()
  {
    Object localObject = a(b.a());
    for (boolean bool = false; !((List)localObject).isEmpty(); bool = true)
    {
      List localList = (List)new ArrayList();
      localObject = ((Iterable)localObject).iterator();
      while (((Iterator)localObject).hasNext())
      {
        bl.a locala = (bl.a)((Iterator)localObject).next();
        try
        {
          Event localEvent = Event.a(b);
          m localm = c;
          k.a(localEvent, "event");
          if (localm.a(localEvent, false) == ProcessResult.SUCCESS) {
            localList.add(Long.valueOf(a));
          }
        }
        catch (x localx)
        {
          AssertionUtil.reportThrowableButNeverCrash((Throwable)localx);
        }
      }
      if (!a(localList)) {
        break;
      }
      localObject = a(b.a());
      StringBuilder localStringBuilder = new StringBuilder("Unsupported events are processed successfully, processed count ");
      localStringBuilder.append(localList.size());
      localStringBuilder.toString();
    }
    localObject = w.b(Boolean.valueOf(bool));
    k.a(localObject, "Promise.wrap(isSuccess)");
    return (w<Boolean>)localObject;
  }
  
  public final void a(Event paramEvent, int paramInt)
  {
    k.b(paramEvent, "event");
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("event", paramEvent.toByteArray());
    localContentValues.put("api_version", Integer.valueOf(paramInt));
    a.insert(TruecallerContract.x.a(), localContentValues);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bl
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */