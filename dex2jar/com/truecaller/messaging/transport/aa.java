package com.truecaller.messaging.transport;

import android.os.HandlerThread;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.k;
import dagger.a.d;
import javax.inject.Provider;

public final class aa
  implements d<i>
{
  private final o a;
  private final Provider<k> b;
  private final Provider<HandlerThread> c;
  
  private aa(o paramo, Provider<k> paramProvider, Provider<HandlerThread> paramProvider1)
  {
    a = paramo;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static aa a(o paramo, Provider<k> paramProvider, Provider<HandlerThread> paramProvider1)
  {
    return new aa(paramo, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.aa
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */