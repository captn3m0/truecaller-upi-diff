package com.truecaller.messaging.transport;

import android.content.ContentProviderResult;
import android.content.OperationApplicationException;
import android.os.RemoteException;

public abstract interface ad$b
{
  public abstract ContentProviderResult[] a(ad paramad)
    throws OperationApplicationException, RemoteException, SecurityException;
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.ad.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */