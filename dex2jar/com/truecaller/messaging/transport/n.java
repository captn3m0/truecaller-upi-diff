package com.truecaller.messaging.transport;

import android.content.Intent;
import com.google.common.collect.Iterables;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Draft;
import com.truecaller.messaging.data.types.Draft.a;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.h;
import com.truecaller.util.al;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.a.a.a.g;
import org.a.a.b;

final class n
  implements m, Iterable<l>
{
  private static final long a = TimeUnit.SECONDS.toMillis(3L);
  private static final int[] t = { 3, 0, 1, 2, 4, 5, 6 };
  private final h b;
  private final al c;
  private final com.truecaller.utils.d d;
  private final f<t> e;
  private final f<d> f;
  private final l g;
  private final l h;
  private final l i;
  private final l j;
  private final l k;
  private final l l;
  private final l m;
  private final l n;
  private final l o;
  private final bw p;
  private final com.truecaller.utils.l q;
  private final com.truecaller.messaging.a r;
  private final com.truecaller.messaging.c.a s;
  
  n(h paramh, al paramal, com.truecaller.utils.d paramd, f<t> paramf, f<d> paramf1, l paraml1, l paraml2, l paraml3, l paraml4, l paraml5, l paraml6, l paraml7, l paraml8, l paraml9, bw parambw, com.truecaller.utils.l paraml, com.truecaller.messaging.a parama, com.truecaller.messaging.c.a parama1)
  {
    b = paramh;
    c = paramal;
    d = paramd;
    g = paraml1;
    f = paramf1;
    h = paraml2;
    i = paraml3;
    j = paraml4;
    k = paraml5;
    l = paraml6;
    m = paraml7;
    n = paraml8;
    o = paraml9;
    e = paramf;
    p = parambw;
    q = paraml;
    r = parama;
    s = parama1;
  }
  
  private boolean b(Participant[] paramArrayOfParticipant)
  {
    l locall = a(0);
    int i2 = paramArrayOfParticipant.length;
    int i1 = 0;
    while (i1 < i2)
    {
      if (!locall.a(paramArrayOfParticipant[i1])) {
        return false;
      }
      i1 += 1;
    }
    return true;
  }
  
  public final int a(Message paramMessage, Participant[] paramArrayOfParticipant)
  {
    int i1 = j;
    int i2 = 1;
    boolean bool;
    if (i1 == 2) {
      bool = true;
    } else {
      bool = false;
    }
    int i3 = a(paramMessage.d(), paramArrayOfParticipant, bool);
    l locall = a(i3);
    int i4 = paramArrayOfParticipant.length;
    i1 = 0;
    while (i1 < i4)
    {
      if (!locall.a(paramArrayOfParticipant[i1])) {
        break label106;
      }
      i1 += 1;
    }
    if ((locall.f() != j) && (locall.c(paramMessage))) {
      i1 = i2;
    } else {
      label106:
      i1 = 0;
    }
    if (i1 != 0) {
      return i3;
    }
    return 3;
  }
  
  public final int a(boolean paramBoolean1, Participant[] paramArrayOfParticipant, boolean paramBoolean2)
  {
    if ((!paramBoolean2) && (a(paramArrayOfParticipant))) {
      return 2;
    }
    if (paramBoolean1) {
      return 1;
    }
    if ((paramArrayOfParticipant.length > 1) && ((!b.y()) || (!b(paramArrayOfParticipant)))) {
      return 1;
    }
    return 0;
  }
  
  public final com.truecaller.androidactors.a a(Message paramMessage, i parami, ac<Draft> paramac)
  {
    AssertionUtil.AlwaysFatal.isTrue(paramMessage.b(), new String[0]);
    boolean bool;
    if (m.c() != -1L) {
      bool = true;
    } else {
      bool = false;
    }
    AssertionUtil.AlwaysFatal.isTrue(bool, new String[0]);
    paramMessage = paramMessage.m();
    f = 17;
    paramMessage = paramMessage.b();
    return ((t)e.a()).d(paramMessage).a(parami, paramac);
  }
  
  public final com.truecaller.androidactors.a a(Message paramMessage, Participant[] paramArrayOfParticipant, i parami, ac<Draft> paramac)
  {
    boolean bool1 = paramMessage.b();
    int i1 = 0;
    AssertionUtil.AlwaysFatal.isTrue(bool1, new String[0]);
    long l1 = b;
    boolean bool2 = true;
    if (l1 != -1L) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    AssertionUtil.AlwaysFatal.isTrue(bool1, new String[0]);
    if (m.c() != -1L) {
      bool1 = bool2;
    } else {
      bool1 = false;
    }
    AssertionUtil.AlwaysFatal.isTrue(bool1, new String[0]);
    paramArrayOfParticipant = new Draft.a().a(paramArrayOfParticipant);
    d = paramMessage.j();
    Entity[] arrayOfEntity = n;
    int i2 = arrayOfEntity.length;
    while (i1 < i2)
    {
      Entity localEntity = arrayOfEntity[i1];
      if (!localEntity.a()) {
        paramArrayOfParticipant.a((BinaryEntity)localEntity);
      }
      i1 += 1;
    }
    return ((t)e.a()).a(paramArrayOfParticipant.d()).a(parami, new -..Lambda.n.gmP3R2w0gNT7GznwW9YjY0tW0KM(this, paramMessage, paramac));
  }
  
  public final w<Message> a(Message paramMessage)
  {
    return a(paramMessage, new Participant[] { c }, false, false);
  }
  
  public final w<Message> a(Message paramMessage, Participant[] paramArrayOfParticipant, boolean paramBoolean1, boolean paramBoolean2)
  {
    if (!paramMessage.c()) {
      return w.b(null);
    }
    int i3 = a(paramMessage.d(), paramArrayOfParticipant, paramBoolean2);
    Object localObject = a(i3);
    int i2 = 0;
    AssertionUtil.isNotNull(localObject, new String[0]);
    if (!((l)localObject).b(paramMessage)) {
      return w.b(null);
    }
    int i1 = i2;
    if (paramBoolean1)
    {
      i1 = i2;
      if (i3 != 2) {
        i1 = 1;
      }
    }
    localObject = (d)f.a();
    long l1;
    if (i1 != 0) {
      l1 = a;
    } else {
      l1 = 0L;
    }
    paramArrayOfParticipant = ((d)localObject).a(paramMessage, paramArrayOfParticipant, i3, (int)l1);
    s.a(o);
    return paramArrayOfParticipant;
  }
  
  public final l a(int paramInt)
  {
    l locall = b(paramInt);
    if (locall != null) {
      return locall;
    }
    throw new IllegalArgumentException("Unsupported transport type");
  }
  
  public final Iterable<l> a()
  {
    return this;
  }
  
  public final void a(int paramInt1, Intent paramIntent, int paramInt2)
  {
    l locall = b(paramInt1);
    if (locall == null) {
      return;
    }
    ((d)f.a()).a(locall, paramIntent, paramInt2);
  }
  
  public final void a(boolean paramBoolean)
  {
    boolean bool1 = d.d();
    boolean bool2 = b.b();
    boolean bool3 = q.a(new String[] { "android.permission.READ_SMS" });
    boolean bool4 = b.d();
    if (bool2 != bool1)
    {
      b.a(bool1);
      if (bool1)
      {
        ((t)e.a()).b();
        i1 = 1;
        break label97;
      }
    }
    int i1 = 0;
    label97:
    if (bool3 != bool4)
    {
      b.b(bool3);
      if (bool3) {
        c.j();
      }
      i1 = 1;
    }
    if (!bool3) {
      return;
    }
    if (i1 != 0)
    {
      ((t)e.a()).a(true);
      return;
    }
    if (paramBoolean) {
      return;
    }
    ((t)e.a()).b(bool1);
  }
  
  public final boolean a(Message paramMessage, Entity paramEntity)
  {
    boolean bool;
    if (h != -1L) {
      bool = true;
    } else {
      bool = false;
    }
    AssertionUtil.AlwaysFatal.isTrue(bool, new String[0]);
    return a(j).a(paramMessage, paramEntity);
  }
  
  public final boolean a(String paramString, boolean paramBoolean1, Participant[] paramArrayOfParticipant, boolean paramBoolean2, a parama)
  {
    return a(a(paramBoolean1, paramArrayOfParticipant, paramBoolean2)).a(paramString, parama);
  }
  
  public final boolean a(Participant[] paramArrayOfParticipant)
  {
    return (p.a()) && (paramArrayOfParticipant.length == 1) && (j.a(paramArrayOfParticipant[0]));
  }
  
  public final l b(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return null;
    case 6: 
      return o;
    case 5: 
      return n;
    case 4: 
      return k;
    case 3: 
      return g;
    case 2: 
      return j;
    case 1: 
      if (d.a(r.b())) {
        return i;
      }
      return m;
    }
    if (d.a(r.b())) {
      return h;
    }
    return l;
  }
  
  public final List<Integer> b()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = iterator();
    while (localIterator.hasNext())
    {
      l locall = (l)localIterator.next();
      if (locall.c()) {
        localArrayList.add(Integer.valueOf(locall.f()));
      }
    }
    return localArrayList;
  }
  
  public final List<Integer> b(Message paramMessage, Participant[] paramArrayOfParticipant)
  {
    ArrayList localArrayList = new ArrayList();
    int i1 = a(paramMessage.d(), paramArrayOfParticipant, false);
    localArrayList.add(Integer.valueOf(i1));
    Iterables.addAll(localArrayList, Iterables.filter(Arrays.asList(org.c.a.a.a.a.a(t)), new -..Lambda.n.KExfGeCK65xvT_ib5ITs_AGzYIU(this, i1, paramMessage, paramArrayOfParticipant)));
    return localArrayList;
  }
  
  public final boolean b(Message paramMessage)
  {
    if ((f & 0x9) != 9) {
      return false;
    }
    ((t)e.a()).a(paramMessage, ay_a).a(new -..Lambda.n.ZbCl5pW11Hb04ZqUbbYxfPZHQSw(this, paramMessage));
    return true;
  }
  
  public final int c(Message paramMessage)
  {
    l locall = b(j);
    AssertionUtil.AlwaysFatal.isNotNull(locall, new String[0]);
    return locall.e(paramMessage);
  }
  
  public final boolean d(Message paramMessage)
  {
    l locall = b(j);
    AssertionUtil.AlwaysFatal.isNotNull(locall, new String[0]);
    return locall.f(paramMessage);
  }
  
  public final Iterator<l> iterator()
  {
    return new n.a(this, (byte)0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */