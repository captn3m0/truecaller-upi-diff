package com.truecaller.messaging;

import javax.inject.Provider;

public final class n
  implements dagger.a.d<a>
{
  private final l a;
  private final Provider<com.truecaller.utils.d> b;
  
  private n(l paraml, Provider<com.truecaller.utils.d> paramProvider)
  {
    a = paraml;
    b = paramProvider;
  }
  
  public static n a(l paraml, Provider<com.truecaller.utils.d> paramProvider)
  {
    return new n(paraml, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */