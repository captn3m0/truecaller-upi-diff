package com.truecaller.messaging;

import com.truecaller.utils.d;

public final class k
  implements j
{
  private final d a;
  private final h b;
  
  public k(d paramd, h paramh)
  {
    a = paramd;
    b = paramh;
  }
  
  public final boolean a()
  {
    if (!a.a())
    {
      int i = b.P();
      b.j((i + 1) % 5);
      return i == 0;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */