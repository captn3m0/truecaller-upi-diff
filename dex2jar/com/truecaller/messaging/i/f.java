package com.truecaller.messaging.i;

import com.truecaller.utils.n;
import dagger.a.d;
import javax.inject.Provider;

public final class f
  implements d<e>
{
  private final Provider<n> a;
  
  private f(Provider<n> paramProvider)
  {
    a = paramProvider;
  }
  
  public static f a(Provider<n> paramProvider)
  {
    return new f(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.i.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */