package com.truecaller.messaging.i;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import c.g.a.b;
import c.g.b.k;
import c.g.b.z;
import c.u;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.utils.n;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import javax.inject.Inject;

public final class e
  implements d
{
  private final n a;
  
  @Inject
  public e(n paramn)
  {
    a = paramn;
  }
  
  private static boolean b(String paramString)
  {
    return (k.a("text/html", paramString)) || (k.a("text/plain", paramString));
  }
  
  private static List<String> c(Message paramMessage)
  {
    Entity[] arrayOfEntity = n;
    k.a(arrayOfEntity, "entities");
    Collection localCollection = (Collection)new ArrayList();
    int k = arrayOfEntity.length;
    int i = 0;
    while (i < k)
    {
      paramMessage = j;
      CharSequence localCharSequence = (CharSequence)paramMessage;
      int j;
      if ((localCharSequence != null) && (localCharSequence.length() != 0)) {
        j = 0;
      } else {
        j = 1;
      }
      if (j != 0) {
        paramMessage = null;
      }
      if (paramMessage != null) {
        localCollection.add(paramMessage);
      }
      i += 1;
    }
    return (List)localCollection;
  }
  
  public final int a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if ((paramInt1 & 0x1) == 0) {
      return 0;
    }
    int i;
    if ((paramInt1 & 0x74) != 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (paramInt3 == 3)
    {
      if (paramInt4 == 0) {
        return 2131234199;
      }
      return 2131234435;
    }
    if (paramInt2 == 3)
    {
      if (paramInt4 == 0) {
        return 2131234197;
      }
      return 2131234041;
    }
    if (i != 0)
    {
      if (paramInt4 == 0) {
        return 2131234200;
      }
      return 2131234536;
    }
    if (paramInt1 == 1)
    {
      if (paramInt4 == 0) {
        return 2131234201;
      }
      return 2131234537;
    }
    return 0;
  }
  
  public final int a(Message paramMessage, b<? super Entity, Boolean> paramb)
  {
    k.b(paramb, "negativePredicate");
    int i = 0;
    if (paramMessage != null)
    {
      if (!paramMessage.c()) {
        return 0;
      }
      Entity[] arrayOfEntity = n;
      k.a(arrayOfEntity, "message.entities");
      int m = arrayOfEntity.length;
      int k;
      for (int j = 0; i < m; j = k)
      {
        Entity localEntity = arrayOfEntity[i];
        k.a(localEntity, "it");
        k = j;
        if (((Boolean)paramb.invoke(localEntity)).booleanValue()) {
          k = j + 1;
        }
        i += 1;
      }
      return n.length - j;
    }
    return 0;
  }
  
  public final Drawable a(int paramInt1, int paramInt2, int paramInt3)
  {
    int i = 0;
    Integer localInteger = Integer.valueOf(a(paramInt1, paramInt2, paramInt3, 0));
    paramInt1 = i;
    if (((Number)localInteger).intValue() > 0) {
      paramInt1 = 1;
    }
    if (paramInt1 == 0) {
      localInteger = null;
    }
    if (localInteger != null)
    {
      paramInt1 = ((Number)localInteger).intValue();
      return a.c(paramInt1);
    }
    return null;
  }
  
  public final Drawable a(int paramInt, String paramString)
  {
    if (paramInt == 1)
    {
      if (paramString == null) {
        return null;
      }
      if (Entity.c(paramString)) {
        return a.c(2131234198);
      }
      if (Entity.d(paramString)) {
        return a.c(2131234202);
      }
      if (Entity.f(paramString)) {
        return a.c(2131234196);
      }
      if (Entity.e(paramString)) {
        return a.c(2131234203);
      }
      return null;
    }
    return null;
  }
  
  public final String a(Message paramMessage)
  {
    if (paramMessage == null) {
      return "";
    }
    Object localObject2 = (Iterable)c(paramMessage);
    Object localObject1 = (Collection)new ArrayList();
    localObject2 = ((Iterable)localObject2).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      Object localObject3 = ((Iterator)localObject2).next();
      if (!b((String)localObject3)) {
        ((Collection)localObject1).add(localObject3);
      }
    }
    localObject1 = (List)localObject1;
    return a(paramMessage.j(), ((List)localObject1).size(), (String)c.a.m.e((List)localObject1));
  }
  
  public final String a(com.truecaller.util.c.a parama)
  {
    k.b(parama, "place");
    double d1 = c;
    double d2 = d;
    Object localObject1 = z.a;
    localObject1 = Locale.US;
    k.a(localObject1, "Locale.US");
    int j = 1;
    localObject1 = String.format((Locale)localObject1, "%s%.7f,%.7f", Arrays.copyOf(new Object[] { "https://maps.google.com/maps?q=", Double.valueOf(d1), Double.valueOf(d2) }, 3));
    k.a(localObject1, "java.lang.String.format(locale, format, *args)");
    Object localObject2 = Uri.parse((String)localObject1);
    k.a(localObject2, "Uri.parse(uriStr)");
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append(((Uri)localObject2).toString());
    localObject2 = a;
    parama = b;
    int i;
    if ((localObject2 != null) && (((CharSequence)localObject2).length() != 0)) {
      i = 0;
    } else {
      i = 1;
    }
    if (i == 0)
    {
      if ((parama != null) && (parama.length() != 0)) {
        i = 0;
      } else {
        i = 1;
      }
      if (i == 0)
      {
        if (parama == null) {
          k.a();
        }
        if (localObject2 == null) {
          k.a();
        }
        if (!c.n.m.a(parama, (CharSequence)localObject2, true))
        {
          ((StringBuilder)localObject1).append(" ");
          ((StringBuilder)localObject1).append((CharSequence)localObject2);
          ((StringBuilder)localObject1).append(", ");
          ((StringBuilder)localObject1).append(parama);
          break label346;
        }
      }
    }
    if ((parama != null) && (parama.length() != 0)) {
      i = 0;
    } else {
      i = 1;
    }
    if (i == 0)
    {
      ((StringBuilder)localObject1).append(" ");
      ((StringBuilder)localObject1).append(parama);
    }
    else
    {
      i = j;
      if (localObject2 != null) {
        if (((CharSequence)localObject2).length() == 0) {
          i = j;
        } else {
          i = 0;
        }
      }
      if (i == 0)
      {
        ((StringBuilder)localObject1).append(" ");
        ((StringBuilder)localObject1).append((CharSequence)localObject2);
      }
    }
    label346:
    parama = ((StringBuilder)localObject1).toString();
    k.a(parama, "it.toString()");
    k.a(parama, "StringBuilder().let {\n  …  it.toString()\n        }");
    return parama;
  }
  
  public final String a(String paramString1, int paramInt, String paramString2)
  {
    if (paramString1 != null) {
      if (paramString1 != null)
      {
        paramString1 = c.n.m.b((CharSequence)paramString1).toString();
        if (paramString1 != null)
        {
          localObject = c.n.m.a(paramString1, '\n', ' ');
          paramString1 = (String)localObject;
          if (localObject != null) {
            break label58;
          }
        }
      }
      else
      {
        throw new u("null cannot be cast to non-null type kotlin.CharSequence");
      }
    }
    paramString1 = "";
    label58:
    Object localObject = new StringBuilder(paramString1);
    int i = 1;
    if (paramInt > 1)
    {
      paramString1 = a.a(2131755025, paramInt, new Object[] { Integer.valueOf(paramInt) });
      if (((CharSequence)localObject).length() > 0) {
        paramInt = i;
      } else {
        paramInt = 0;
      }
      if (paramInt != 0) {
        ((StringBuilder)localObject).append(", ");
      }
      ((StringBuilder)localObject).append(paramString1);
    }
    else if ((paramInt == 1) && (paramString2 != null))
    {
      if (((CharSequence)localObject).length() == 0) {
        paramInt = 1;
      } else {
        paramInt = 0;
      }
      if (paramInt != 0)
      {
        if (Entity.g(paramString2)) {
          paramString1 = a.a(2131886684, new Object[0]);
        } else if (Entity.c(paramString2)) {
          paramString1 = a.a(2131886685, new Object[0]);
        } else if (Entity.d(paramString2)) {
          paramString1 = a.a(2131886699, new Object[0]);
        } else if (Entity.f(paramString2)) {
          paramString1 = a.a(2131886658, new Object[0]);
        } else if (Entity.e(paramString2)) {
          paramString1 = a.a(2131886700, new Object[0]);
        } else {
          paramString1 = a.a(2131755025, 1, new Object[] { Integer.valueOf(1) });
        }
        ((StringBuilder)localObject).append(paramString1);
      }
    }
    paramString1 = ((StringBuilder)localObject).toString();
    k.a(paramString1, "fullText.toString()");
    return paramString1;
  }
  
  public final boolean a(String paramString)
  {
    k.b(paramString, "text");
    return com.truecaller.android.truemoji.c.a.a((CharSequence)paramString);
  }
  
  public final String b(Message paramMessage)
  {
    k.b(paramMessage, "message");
    Object localObject1 = (Iterable)c(paramMessage);
    paramMessage = (Collection)new ArrayList();
    localObject1 = ((Iterable)localObject1).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      Object localObject2 = ((Iterator)localObject1).next();
      if (!b((String)localObject2)) {
        paramMessage.add(localObject2);
      }
    }
    paramMessage = (List)paramMessage;
    if (paramMessage.size() != 1) {
      return null;
    }
    paramMessage = (String)c.a.m.d(paramMessage);
    if (Entity.c(paramMessage)) {
      return "🌄";
    }
    if (Entity.d(paramMessage)) {
      return "🎥";
    }
    if (Entity.f(paramMessage)) {
      return "👤";
    }
    if (Entity.e(paramMessage)) {
      return "🎙";
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.i.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */