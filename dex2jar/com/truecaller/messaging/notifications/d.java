package com.truecaller.messaging.notifications;

import android.content.Context;
import com.truecaller.androidactors.f;
import com.truecaller.filters.p;
import com.truecaller.filters.v;
import com.truecaller.messaging.h;
import com.truecaller.notificationchannels.j;
import com.truecaller.notifications.y;
import com.truecaller.search.b;
import com.truecaller.util.al;
import com.truecaller.util.bv;
import com.truecaller.utils.i;
import com.truecaller.utils.n;
import javax.inject.Provider;

public final class d
  implements dagger.a.d<c>
{
  private final Provider<Context> a;
  private final Provider<com.truecaller.notifications.a> b;
  private final Provider<al> c;
  private final Provider<com.truecaller.utils.d> d;
  private final Provider<bv> e;
  private final Provider<com.truecaller.network.search.l> f;
  private final Provider<p> g;
  private final Provider<n> h;
  private final Provider<h> i;
  private final Provider<com.truecaller.notifications.ae> j;
  private final Provider<f<com.truecaller.analytics.ae>> k;
  private final Provider<com.truecaller.notifications.l> l;
  private final Provider<e> m;
  private final Provider<com.truecaller.notificationchannels.e> n;
  private final Provider<j> o;
  private final Provider<com.truecaller.common.g.a> p;
  private final Provider<y> q;
  private final Provider<com.truecaller.abtest.c> r;
  private final Provider<v> s;
  private final Provider<i> t;
  private final Provider<com.truecaller.messaging.i.d> u;
  private final Provider<com.truecaller.featuretoggles.e> v;
  private final Provider<com.truecaller.smsparser.a> w;
  private final Provider<com.truecaller.truepay.d> x;
  private final Provider<b> y;
  
  private d(Provider<Context> paramProvider, Provider<com.truecaller.notifications.a> paramProvider1, Provider<al> paramProvider2, Provider<com.truecaller.utils.d> paramProvider3, Provider<bv> paramProvider4, Provider<com.truecaller.network.search.l> paramProvider5, Provider<p> paramProvider6, Provider<n> paramProvider7, Provider<h> paramProvider8, Provider<com.truecaller.notifications.ae> paramProvider9, Provider<f<com.truecaller.analytics.ae>> paramProvider10, Provider<com.truecaller.notifications.l> paramProvider11, Provider<e> paramProvider12, Provider<com.truecaller.notificationchannels.e> paramProvider13, Provider<j> paramProvider14, Provider<com.truecaller.common.g.a> paramProvider15, Provider<y> paramProvider16, Provider<com.truecaller.abtest.c> paramProvider17, Provider<v> paramProvider18, Provider<i> paramProvider19, Provider<com.truecaller.messaging.i.d> paramProvider20, Provider<com.truecaller.featuretoggles.e> paramProvider21, Provider<com.truecaller.smsparser.a> paramProvider22, Provider<com.truecaller.truepay.d> paramProvider23, Provider<b> paramProvider24)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
    k = paramProvider10;
    l = paramProvider11;
    m = paramProvider12;
    n = paramProvider13;
    o = paramProvider14;
    p = paramProvider15;
    q = paramProvider16;
    r = paramProvider17;
    s = paramProvider18;
    t = paramProvider19;
    u = paramProvider20;
    v = paramProvider21;
    w = paramProvider22;
    x = paramProvider23;
    y = paramProvider24;
  }
  
  public static d a(Provider<Context> paramProvider, Provider<com.truecaller.notifications.a> paramProvider1, Provider<al> paramProvider2, Provider<com.truecaller.utils.d> paramProvider3, Provider<bv> paramProvider4, Provider<com.truecaller.network.search.l> paramProvider5, Provider<p> paramProvider6, Provider<n> paramProvider7, Provider<h> paramProvider8, Provider<com.truecaller.notifications.ae> paramProvider9, Provider<f<com.truecaller.analytics.ae>> paramProvider10, Provider<com.truecaller.notifications.l> paramProvider11, Provider<e> paramProvider12, Provider<com.truecaller.notificationchannels.e> paramProvider13, Provider<j> paramProvider14, Provider<com.truecaller.common.g.a> paramProvider15, Provider<y> paramProvider16, Provider<com.truecaller.abtest.c> paramProvider17, Provider<v> paramProvider18, Provider<i> paramProvider19, Provider<com.truecaller.messaging.i.d> paramProvider20, Provider<com.truecaller.featuretoggles.e> paramProvider21, Provider<com.truecaller.smsparser.a> paramProvider22, Provider<com.truecaller.truepay.d> paramProvider23, Provider<b> paramProvider24)
  {
    return new d(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10, paramProvider11, paramProvider12, paramProvider13, paramProvider14, paramProvider15, paramProvider16, paramProvider17, paramProvider18, paramProvider19, paramProvider20, paramProvider21, paramProvider22, paramProvider23, paramProvider24);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */