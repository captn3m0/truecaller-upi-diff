package com.truecaller.messaging.notifications;

import android.app.Notification;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.v4.app.z.d;
import android.support.v4.app.z.f;
import android.support.v4.app.z.g;
import android.support.v4.content.b;
import c.a.aa;
import c.a.m;
import c.g.b.k;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Reaction;
import com.truecaller.notificationchannels.e;
import com.truecaller.notificationchannels.j;
import com.truecaller.notifications.a;
import com.truecaller.notifications.l;
import com.truecaller.notifications.l.a;
import com.truecaller.util.al;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.inject.Inject;

public final class h
  implements g
{
  private Set<Long> a;
  private final Context b;
  private final l c;
  private final a d;
  private final e e;
  private final j f;
  private final al g;
  
  @Inject
  public h(Context paramContext, l paraml, a parama, e parame, j paramj, al paramal)
  {
    b = paramContext;
    c = paraml;
    d = parama;
    e = parame;
    f = paramj;
    g = paramal;
    a = ((Set)aa.a);
  }
  
  private final z.d a(Reaction paramReaction, Participant paramParticipant, long[] paramArrayOfLong, boolean paramBoolean)
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
  }
  
  private final void b(Map<Reaction, ? extends Participant> paramMap)
  {
    Object localObject2 = m.a((Iterable)paramMap.keySet(), (Comparator)new h.c());
    Object localObject1 = new z.f();
    Object localObject4 = (Iterable)localObject2;
    Object localObject3 = ((Iterable)localObject4).iterator();
    boolean bool1;
    boolean bool2;
    Object localObject6;
    for (;;)
    {
      bool1 = ((Iterator)localObject3).hasNext();
      bool2 = false;
      if (!bool1) {
        break;
      }
      localObject5 = (Reaction)((Iterator)localObject3).next();
      localObject6 = (Participant)paramMap.get(localObject5);
      if (localObject6 != null) {
        ((z.f)localObject1).c((CharSequence)b.getString(2131888601, new Object[] { m, d }));
      }
    }
    ((z.f)localObject1).b((CharSequence)b.getString(2131888602, new Object[] { Integer.valueOf(paramMap.size()) }));
    localObject3 = (Reaction)m.d((List)localObject2);
    localObject2 = (Participant)paramMap.get(localObject3);
    if (localObject2 == null) {
      return;
    }
    Object localObject5 = (Collection)new ArrayList(m.a((Iterable)localObject4, 10));
    localObject4 = ((Iterable)localObject4).iterator();
    while (((Iterator)localObject4).hasNext()) {
      ((Collection)localObject5).add(Long.valueOf(nextb));
    }
    localObject4 = m.c((Collection)localObject5);
    localObject5 = (Iterable)paramMap.keySet();
    if ((localObject5 instanceof Collection))
    {
      bool1 = bool2;
      if (((Collection)localObject5).isEmpty()) {}
    }
    else
    {
      localObject5 = ((Iterable)localObject5).iterator();
      do
      {
        bool1 = bool2;
        if (!((Iterator)localObject5).hasNext()) {
          break;
        }
        localObject6 = (Reaction)((Iterator)localObject5).next();
      } while (!(a.contains(Long.valueOf(a)) ^ true));
      bool1 = true;
    }
    localObject1 = a((Reaction)localObject3, (Participant)localObject2, (long[])localObject4, bool1).a((z.g)localObject1);
    k.a(localObject1, "buildNotification(lastRe…ons).setStyle(inboxStyle)");
    paramMap = m.i((Iterable)paramMap.values());
    if (paramMap.size() > 1) {
      ((z.d)localObject1).a((CharSequence)com.truecaller.messaging.i.g.a((Collection)paramMap));
    }
    paramMap = d;
    localObject1 = c.a((z.d)localObject1, (l.a)new h.d(this, (Participant)localObject2));
    k.a(localObject1, "notificationIconHelper.c…Avatar(lastParticipant) }");
    paramMap.a(2131363320, (Notification)localObject1, "notificationIncomingReaction");
  }
  
  public final void a(Map<Reaction, ? extends Participant> paramMap)
  {
    int i;
    if ((paramMap != null) && (!paramMap.isEmpty())) {
      i = 0;
    } else {
      i = 1;
    }
    if (i != 0)
    {
      d.a(2131363320);
      a = ((Set)aa.a);
      return;
    }
    if (Build.VERSION.SDK_INT >= 24)
    {
      localObject1 = m.a((Iterable)paramMap.keySet(), (Comparator)new h.b());
      Object localObject2 = (Iterable)localObject1;
      Object localObject3 = ((Iterable)localObject2).iterator();
      Object localObject5;
      while (((Iterator)localObject3).hasNext())
      {
        localObject5 = (Reaction)((Iterator)localObject3).next();
        Participant localParticipant = (Participant)paramMap.get(localObject5);
        if (localParticipant != null)
        {
          boolean bool = a.contains(Long.valueOf(a));
          z.d locald = a((Reaction)localObject5, localParticipant, new long[] { b }, bool ^ true).c("com.truecaller.messaging.notifications.REACTIONS");
          k.a(locald, "buildNotification(reacti…roup(GROUP_KEY_REACTIONS)");
          localObject4 = d;
          long l = b;
          localObject5 = c.a(locald, (l.a)new h.a(localParticipant, this, paramMap));
          k.a(localObject5, "notificationIconHelper.c… getAvatar(participant) }");
          ((a)localObject4).a(String.valueOf(l), 2131363320, (Notification)localObject5, "notificationIncomingReaction");
        }
      }
      localObject3 = (Reaction)m.d((List)localObject1);
      Object localObject4 = (Participant)paramMap.get(localObject3);
      if (localObject4 != null)
      {
        localObject5 = (Collection)new ArrayList(m.a((Iterable)localObject2, 10));
        localObject2 = ((Iterable)localObject2).iterator();
        while (((Iterator)localObject2).hasNext()) {
          ((Collection)localObject5).add(Long.valueOf(nextb));
        }
        localObject2 = m.c((Collection)localObject5);
        localObject1 = new z.d(b, e.a()).a(2131234327).f(b.c(b, 2131099685)).c("com.truecaller.messaging.notifications.REACTIONS").f().b(((List)localObject1).size()).c(-1).a(f.a(b, (Participant)localObject4, b, 0)).b(f.a(b, (long[])localObject2, 0)).e().h();
        localObject2 = d;
        k.a(localObject1, "summaryNotification");
        ((a)localObject2).a(2131363320, (Notification)localObject1, "notificationIncomingReaction");
      }
    }
    else
    {
      b(paramMap);
    }
    Object localObject1 = (Iterable)paramMap.keySet();
    paramMap = (Collection)new ArrayList(m.a((Iterable)localObject1, 10));
    localObject1 = ((Iterable)localObject1).iterator();
    while (((Iterator)localObject1).hasNext()) {
      paramMap.add(Long.valueOf(nexta));
    }
    a = m.i((Iterable)paramMap);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */