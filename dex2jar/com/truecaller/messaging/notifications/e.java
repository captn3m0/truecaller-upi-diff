package com.truecaller.messaging.notifications;

import android.app.Notification;
import android.content.Context;
import android.support.v4.app.z.d;
import android.support.v4.content.b;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.notifications.l;

public final class e
{
  private final Context a;
  private final l b;
  
  public e(Context paramContext, l paraml)
  {
    a = paramContext;
    b = paraml;
  }
  
  private static boolean a(Participant paramParticipant)
  {
    return (paramParticipant.g()) && (q > 0);
  }
  
  final Notification a(Participant paramParticipant, String paramString)
  {
    if ((!paramParticipant.g()) && (c == 1)) {
      return null;
    }
    z.d locald1 = new z.d(a, paramString);
    z.d locald2 = locald1.a(2131234327);
    C = b.c(a, 2131099685);
    int i;
    if (a(paramParticipant))
    {
      paramString = a;
      i = 2131887528;
    }
    else
    {
      paramString = a;
      i = 2131887526;
    }
    locald2 = locald2.a(String.format(paramString.getString(i), new Object[] { paramParticipant.b() }));
    if (a(paramParticipant))
    {
      paramString = a;
      i = 2131887527;
    }
    else
    {
      paramString = a;
      i = 2131887525;
    }
    bgetStringf = NotificationBroadcastReceiver.a(a, "notificationIncomingMessagePromo");
    return b.a(locald1, new -..Lambda.e.3ddttDFwYAAU-9Kk0Iy_MdYHjqs(this, paramParticipant));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */