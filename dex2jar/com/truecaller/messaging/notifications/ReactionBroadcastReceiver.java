package com.truecaller.messaging.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.f;
import com.truecaller.bp;
import com.truecaller.messaging.conversation.ConversationActivity;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.transport.im.as;
import javax.inject.Inject;

public final class ReactionBroadcastReceiver
  extends BroadcastReceiver
{
  @Inject
  public f<as> a;
  
  public ReactionBroadcastReceiver()
  {
    TrueApp localTrueApp = TrueApp.y();
    k.a(localTrueApp, "TrueApp.getApp()");
    localTrueApp.a().a(this);
  }
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramContext != null)
    {
      if (paramIntent == null) {
        return;
      }
      Object localObject = paramIntent.getAction();
      if (localObject != null)
      {
        int i = ((String)localObject).hashCode();
        if (i != -603694704)
        {
          if ((i == 545703614) && (((String)localObject).equals("com.truecaller.mark_as_seen")))
          {
            paramContext = paramIntent.getLongArrayExtra("message_ids");
            k.a(paramContext, "intent.getLongArrayExtra(EXTRA_MESSAGE_IDS)");
            paramIntent = a;
            if (paramIntent == null) {
              k.a("imReactionManagerRef");
            }
            ((as)paramIntent.a()).a(paramContext);
          }
        }
        else if (((String)localObject).equals("com.truecaller.open_conversation"))
        {
          Parcelable localParcelable = paramIntent.getParcelableExtra("participant");
          localObject = localParcelable;
          if (!(localParcelable instanceof Participant)) {
            localObject = null;
          }
          localObject = (Participant)localObject;
          long l = paramIntent.getLongExtra("message_id", -1L);
          if (localObject != null)
          {
            paramIntent = new Intent(paramContext, ConversationActivity.class);
            paramIntent.setFlags(268435456);
            paramIntent.putExtra("participants", (Parcelable[])new Participant[] { localObject });
            paramIntent.putExtra("message_id", l);
            paramContext.startActivity(paramIntent);
          }
          return;
        }
      }
      paramContext = new StringBuilder("Unknown action ");
      paramContext.append(paramIntent.getAction());
      paramContext.append(" in onReceive");
      throw ((Throwable)new RuntimeException(paramContext.toString()));
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.ReactionBroadcastReceiver
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */