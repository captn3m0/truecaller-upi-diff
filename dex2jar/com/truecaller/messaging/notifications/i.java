package com.truecaller.messaging.notifications;

import android.content.Context;
import com.truecaller.notificationchannels.e;
import com.truecaller.notificationchannels.j;
import com.truecaller.notifications.a;
import com.truecaller.notifications.l;
import com.truecaller.util.al;
import dagger.a.d;
import javax.inject.Provider;

public final class i
  implements d<h>
{
  private final Provider<Context> a;
  private final Provider<l> b;
  private final Provider<a> c;
  private final Provider<e> d;
  private final Provider<j> e;
  private final Provider<al> f;
  
  private i(Provider<Context> paramProvider, Provider<l> paramProvider1, Provider<a> paramProvider2, Provider<e> paramProvider3, Provider<j> paramProvider4, Provider<al> paramProvider5)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
  }
  
  public static i a(Provider<Context> paramProvider, Provider<l> paramProvider1, Provider<a> paramProvider2, Provider<e> paramProvider3, Provider<j> paramProvider4, Provider<al> paramProvider5)
  {
    return new i(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */