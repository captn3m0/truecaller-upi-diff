package com.truecaller.messaging.notifications;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.content.Context;
import android.support.v4.app.z.d;
import com.truecaller.notifications.l;
import com.truecaller.notifications.l.a;
import java.lang.reflect.Field;

public final class j
  implements l
{
  private final Context a;
  
  public j(Context paramContext)
  {
    a = paramContext;
  }
  
  @SuppressLint({"PrivateApi"})
  private static Object a()
    throws Exception
  {
    Object localObject = Class.forName("android.app.MiuiNotification").newInstance();
    Field localField = localObject.getClass().getDeclaredField("customizedIcon");
    localField.setAccessible(true);
    localField.set(localObject, Boolean.TRUE);
    return localObject;
  }
  
  private static void a(Notification paramNotification)
  {
    try
    {
      a(paramNotification, a());
      return;
    }
    catch (Exception paramNotification) {}
  }
  
  private static void a(Notification paramNotification, Object paramObject)
    throws Exception
  {
    Field localField = paramNotification.getClass().getField("extraNotification");
    localField.setAccessible(true);
    localField.set(paramNotification, paramObject);
  }
  
  public final Notification a(z.d paramd, l.a parama)
  {
    return a(paramd, parama, new -..Lambda.j.05bFIID5ZvXkmyqZ_bN-KOANijg(this));
  }
  
  public final Notification a(z.d paramd, l.a parama1, l.a parama2)
  {
    paramd.a(parama2.create());
    paramd = paramd.h();
    a(paramd);
    return paramd;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */