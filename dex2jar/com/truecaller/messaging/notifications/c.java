package com.truecaller.messaging.notifications;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.v4.app.ae.a;
import android.support.v4.app.z.a.a;
import android.support.v4.app.z.c;
import android.support.v4.app.z.d;
import android.support.v4.app.z.e;
import android.support.v4.app.z.f;
import android.support.v4.app.z.g;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.util.Pair;
import android.util.Patterns;
import android.widget.RemoteViews;
import android.widget.Toast;
import c.a.m;
import com.d.b.w;
import com.truecaller.TrueApp;
import com.truecaller.common.h.ah;
import com.truecaller.common.h.am;
import com.truecaller.common.h.aq.d;
import com.truecaller.data.entity.Contact;
import com.truecaller.filters.p;
import com.truecaller.filters.sync.TopSpammer;
import com.truecaller.filters.v;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.UnmutedException.g;
import com.truecaller.messaging.MessagesActivity;
import com.truecaller.messaging.data.types.Conversation;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Participant.a;
import com.truecaller.messaging.data.types.TextEntity;
import com.truecaller.messaging.h;
import com.truecaller.notifications.x;
import com.truecaller.notifications.y;
import com.truecaller.notifications.y.b;
import com.truecaller.tracking.events.al.a;
import com.truecaller.tracking.events.ay;
import com.truecaller.tracking.events.ay.a;
import com.truecaller.tracking.events.bc;
import com.truecaller.tracking.events.bc.a;
import com.truecaller.tracking.events.z;
import com.truecaller.truepay.SenderInfo;
import com.truecaller.util.at;
import com.truecaller.util.bv;
import com.truecaller.utils.i;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Inject;
import org.a.a.a.g;
import org.a.a.aa;
import org.a.a.r;

public final class c
  implements a
{
  private final com.truecaller.search.b A;
  private long B = -100L;
  private boolean C;
  private boolean D;
  private long E = -1L;
  private final Context a;
  private final com.truecaller.notifications.l b;
  private final com.truecaller.notifications.a c;
  private final com.truecaller.util.al d;
  private final com.truecaller.utils.d e;
  private final bv f;
  private final com.truecaller.network.search.l g;
  private final p h;
  private final com.truecaller.utils.n i;
  private final h j;
  private final com.truecaller.notifications.ae k;
  private final com.truecaller.androidactors.f<com.truecaller.analytics.ae> l;
  private final e m;
  private final com.truecaller.notificationchannels.j n;
  private final com.truecaller.notificationchannels.e o;
  private final com.truecaller.abtest.c p;
  private Set<Long> q;
  private final com.truecaller.common.g.a r;
  private final y s;
  private final v t;
  private final i u;
  private final com.truecaller.messaging.i.d v;
  private final com.truecaller.featuretoggles.e w;
  private final com.truecaller.smsparser.a x;
  private final com.truecaller.smsparser.b.d y;
  private final com.truecaller.truepay.d z;
  
  @Inject
  c(Context paramContext, com.truecaller.notifications.a parama, com.truecaller.util.al paramal, com.truecaller.utils.d paramd, bv parambv, com.truecaller.network.search.l paraml, p paramp, com.truecaller.utils.n paramn, h paramh, com.truecaller.notifications.ae paramae, com.truecaller.androidactors.f<com.truecaller.analytics.ae> paramf, com.truecaller.notifications.l paraml1, e parame, com.truecaller.notificationchannels.e parame1, com.truecaller.notificationchannels.j paramj, com.truecaller.common.g.a parama1, y paramy, com.truecaller.abtest.c paramc, v paramv, i parami, com.truecaller.messaging.i.d paramd1, com.truecaller.featuretoggles.e parame2, com.truecaller.smsparser.a parama2, com.truecaller.truepay.d paramd2, com.truecaller.search.b paramb)
  {
    a = paramContext;
    c = parama;
    d = paramal;
    e = paramd;
    f = parambv;
    g = paraml;
    h = paramp;
    i = paramn;
    j = paramh;
    k = paramae;
    l = paramf;
    b = paraml1;
    m = parame;
    o = parame1;
    n = paramj;
    r = parama1;
    s = paramy;
    p = paramc;
    t = paramv;
    u = parami;
    v = paramd1;
    w = parame2;
    x = parama2;
    y = new com.truecaller.smsparser.k(((TrueApp)a.getApplicationContext()).a());
    z = paramd2;
    A = paramb;
  }
  
  private Bitmap a(Uri paramUri, int paramInt)
  {
    if (paramUri != null) {}
    try
    {
      try
      {
        paramUri = w.a(a).a(paramUri).a(aq.d.b()).d();
      }
      catch (IllegalArgumentException paramUri)
      {
        AssertionUtil.reportThrowableButNeverCrash(new UnmutedException.g(am.n(paramUri.getMessage())));
      }
    }
    catch (IOException|SecurityException paramUri)
    {
      Object localObject;
      for (;;) {}
    }
    paramUri = null;
    localObject = paramUri;
    if (paramUri == null)
    {
      localObject = paramUri;
      if (paramInt != 0) {
        localObject = at.a(android.support.v4.content.b.a(a, paramInt));
      }
    }
    return (Bitmap)localObject;
  }
  
  private Bitmap a(Uri paramUri, boolean paramBoolean)
  {
    int i1;
    if (paramBoolean) {
      i1 = 2131233857;
    } else {
      i1 = 2131233849;
    }
    return a(paramUri, i1);
  }
  
  private Uri a(Conversation paramConversation, Map<String, Participant> paramMap)
  {
    if (x != null)
    {
      paramConversation = x.c;
      if (paramConversation != null)
      {
        if (paramConversation.isEmpty()) {
          return null;
        }
        return Uri.parse(paramConversation);
      }
      return null;
    }
    if (c == 1) {
      return null;
    }
    paramConversation = a(l[0], paramMap);
    return d.a(p, n, true);
  }
  
  private android.support.v4.app.z.a a(String paramString, NotificationIdentifier paramNotificationIdentifier, List<Message> paramList)
  {
    return new android.support.v4.app.z.a(2131234474, a.getString(2131886388), NotificationBroadcastReceiver.a(a, paramNotificationIdentifier, paramString, false, paramList));
  }
  
  private android.support.v4.app.z.a a(List<Message> paramList, Uri paramUri)
  {
    paramList = NotificationBroadcastReceiver.a(a, paramList, paramUri);
    return new android.support.v4.app.z.a(2131234474, a.getString(2131888548), paramList);
  }
  
  private android.support.v4.app.z.a a(List<Message> paramList, NotificationIdentifier paramNotificationIdentifier)
  {
    Context localContext;
    int i1;
    if (paramList.size() == 1)
    {
      localContext = a;
      i1 = 2131886651;
    }
    else
    {
      localContext = a;
      i1 = 2131886650;
    }
    return new android.support.v4.app.z.a(2131234063, localContext.getString(i1), NotificationBroadcastReceiver.c(a, paramList, paramNotificationIdentifier));
  }
  
  private android.support.v4.app.z.a a(List<Message> paramList, NotificationIdentifier paramNotificationIdentifier, String paramString)
  {
    if (Build.VERSION.SDK_INT >= 24) {
      paramList = NotificationBroadcastReceiver.a(a, paramList, paramNotificationIdentifier, paramString);
    } else {
      paramList = NotificationBroadcastReceiver.b(a, paramList);
    }
    paramNotificationIdentifier = a.getString(2131888676);
    paramString = new ae.a("KEY_REPLY_TEXT");
    a = paramNotificationIdentifier;
    paramString = paramString.a();
    return new z.a.a(2131234528, paramNotificationIdentifier, paramList).a(paramString).a();
  }
  
  private static Spannable a(String paramString)
  {
    SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder(paramString);
    localSpannableStringBuilder.setSpan(new StyleSpan(1), 0, paramString.length(), 33);
    return localSpannableStringBuilder;
  }
  
  private RemoteViews a(int paramInt, String paramString1, NotificationIdentifier paramNotificationIdentifier, long paramLong, String paramString2)
  {
    RemoteViews localRemoteViews = new RemoteViews(a.getPackageName(), paramInt);
    paramNotificationIdentifier = x.a(a, paramString1, paramLong, paramNotificationIdentifier);
    paramNotificationIdentifier = PendingIntent.getService(a, 0, paramNotificationIdentifier, 134217728);
    localRemoteViews.setTextViewText(2131363847, paramString1.replaceAll("", " ").trim());
    localRemoteViews.setTextViewText(2131364297, i.a(2131888473, new Object[] { paramString2 }));
    localRemoteViews.setOnClickPendingIntent(2131362606, paramNotificationIdentifier);
    return localRemoteViews;
  }
  
  private static Participant a(Message paramMessage, Map<String, Participant> paramMap)
  {
    return a(c, paramMap);
  }
  
  private Participant a(Participant paramParticipant, List<CharSequence> paramList)
  {
    Object localObject;
    if (e.d()) {
      localObject = "notification";
    } else {
      localObject = "notificationNotDefault";
    }
    if (!u.a())
    {
      a(paramParticipant, "noConnection", (String)localObject, paramList);
      return paramParticipant;
    }
    if (!d.a())
    {
      a(paramParticipant, "noAccount", (String)localObject, paramList);
      return paramParticipant;
    }
    if (!paramParticipant.h())
    {
      a(paramParticipant, "notNumber", (String)localObject, paramList);
      return paramParticipant;
    }
    if (A.a(t, o))
    {
      a(paramParticipant, "validCacheResult", (String)localObject, paramList);
      return paramParticipant;
    }
    try
    {
      localObject = g.a(UUID.randomUUID(), (String)localObject).a();
      i = f;
      h = 20;
      a = paramList;
      paramList = ((com.truecaller.network.search.j)localObject).f();
      if (paramList == null) {
        break label282;
      }
      localObject = paramList.a();
      if (localObject == null) {
        break label282;
      }
      int i2 = c;
      i1 = 1;
      if (i2 == 1) {
        break label386;
      }
    }
    catch (IOException paramList)
    {
      for (;;)
      {
        Participant.a locala;
        continue;
        int i1 = 0;
      }
    }
    locala = paramParticipant.l();
    if (i1 != 0) {
      paramList = ((Contact)localObject).t();
    } else {
      paramList = paramParticipant.a();
    }
    l = paramList;
    n = (o & ((Contact)localObject).getSource());
    m = ((Contact)localObject).v();
    p = Math.max(0, ((Contact)localObject).I());
    paramList = locala.a();
    return paramList;
    label282:
    if (k)
    {
      localObject = t.a(f);
      if (localObject != null)
      {
        locala = paramParticipant.l();
        if (((TopSpammer)localObject).getLabel() != null) {
          paramList = ((TopSpammer)localObject).getLabel();
        } else {
          paramList = m;
        }
        l = paramList;
        if (((TopSpammer)localObject).getReports() != null) {
          i1 = ((TopSpammer)localObject).getReports().intValue();
        } else {
          i1 = q;
        }
        p = i1;
        return locala.a();
      }
    }
    return paramParticipant;
  }
  
  private static Participant a(Participant paramParticipant, Map<String, Participant> paramMap)
  {
    paramMap = (Participant)paramMap.get(f);
    if (paramMap != null) {
      return paramMap;
    }
    return paramParticipant;
  }
  
  private CharSequence a(Message paramMessage, Conversation paramConversation, Map<String, Participant> paramMap, boolean paramBoolean)
  {
    SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder();
    if ((c == 1) && (paramBoolean)) {
      localSpannableStringBuilder.append(a(b(paramConversation, paramMap))).append(" ").append(a(paramMessage, paramMap).a()).append(": ");
    } else if ((c == 1) || (paramBoolean)) {
      localSpannableStringBuilder.append(a(a(paramMessage, paramMap).a())).append(": ");
    }
    paramConversation = new com.truecaller.messaging.i.c(ah.a(v.a(paramMessage))).a(a.getResources());
    g = v.b(paramMessage);
    localSpannableStringBuilder.append(paramConversation.a());
    return localSpannableStringBuilder;
  }
  
  private Map<String, Participant> a(List<Message> paramList)
  {
    HashMap localHashMap1 = new HashMap();
    HashMap localHashMap2 = new HashMap();
    Iterator localIterator = paramList.iterator();
    Object localObject;
    while (localIterator.hasNext())
    {
      Message localMessage = (Message)localIterator.next();
      localHashMap1.put(c.f, c);
      localObject = (List)localHashMap2.get(c.f);
      paramList = (List<Message>)localObject;
      if (localObject == null)
      {
        paramList = new ArrayList();
        localHashMap2.put(c.f, paramList);
      }
      paramList.add(localMessage.l());
    }
    paramList = localHashMap1.keySet().iterator();
    while (paramList.hasNext())
    {
      localObject = (String)paramList.next();
      localHashMap1.put(localObject, a((Participant)localHashMap1.get(localObject), (List)localHashMap2.get(localObject)));
    }
    return localHashMap1;
  }
  
  private void a(z.d paramd)
  {
    a2131234327C = android.support.v4.content.b.c(a, 2131099685);
  }
  
  private void a(Participant paramParticipant, String paramString1, String paramString2, List<CharSequence> paramList)
  {
    com.truecaller.tracking.events.z.a locala = z.b();
    locala.a(UUID.randomUUID().toString()).d(paramString2).c("20");
    paramString2 = null;
    locala.b(null);
    locala.a(false);
    locala.b(false);
    ArrayList localArrayList = new ArrayList();
    Object localObject = com.truecaller.tracking.events.al.b().b(TextUtils.isEmpty(m) ^ true).a(paramParticipant.d()).a(Integer.valueOf(Math.max(0, q)));
    boolean bool;
    if (q >= 10) {
      bool = true;
    } else {
      bool = false;
    }
    localObject = ((al.a)localObject).d(Boolean.valueOf(bool));
    if (j == 1) {
      bool = true;
    } else {
      bool = false;
    }
    localObject = ((al.a)localObject).a(Boolean.valueOf(bool));
    if (j == 2) {
      bool = true;
    } else {
      bool = false;
    }
    localObject = ((al.a)localObject).c(Boolean.valueOf(bool)).b(Boolean.valueOf(k));
    if ((o & 0x40) != 0) {
      bool = true;
    } else {
      bool = false;
    }
    com.truecaller.tracking.events.al localal = ((al.a)localObject).e(Boolean.valueOf(bool)).a();
    bc localbc = bc.b().a(null).b(null).c(null).a();
    if ((o & 0x1) != 0)
    {
      localObject = f;
      paramString2 = (String)localObject;
      if (org.c.a.a.a.k.a((CharSequence)localObject, "+", false)) {
        paramString2 = ((String)localObject).substring(1);
      }
    }
    localArrayList.add(ay.b().a(f).a(localbc).a(localal).b(paramString1).c(paramString2).a());
    locala.a(localArrayList);
    locala.b(paramList);
    try
    {
      ((com.truecaller.analytics.ae)l.a()).a(locala.a());
      return;
    }
    catch (org.apache.a.a paramParticipant)
    {
      AssertionUtil.reportThrowableButNeverCrash(paramParticipant);
    }
  }
  
  private void a(Collection<Long> paramCollection)
  {
    try
    {
      FileOutputStream localFileOutputStream = a.openFileOutput("notifications.state", 0);
      try
      {
        DataOutputStream localDataOutputStream = new DataOutputStream(localFileOutputStream);
        paramCollection = paramCollection.iterator();
        while (paramCollection.hasNext()) {
          localDataOutputStream.writeLong(((Long)paramCollection.next()).longValue());
        }
        return;
      }
      finally
      {
        com.truecaller.utils.extensions.d.a(localFileOutputStream);
      }
      return;
    }
    catch (IOException paramCollection)
    {
      AssertionUtil.reportThrowableButNeverCrash(paramCollection);
    }
  }
  
  private void a(List<Message> paramList, String paramString1, String paramString2)
  {
    HashMap localHashMap2 = new HashMap();
    HashMap localHashMap1 = new HashMap();
    Iterator localIterator = paramList.iterator();
    Object localObject;
    while (localIterator.hasNext())
    {
      Message localMessage = (Message)localIterator.next();
      localHashMap2.put(c.f, c);
      localObject = (List)localHashMap1.get(c.f);
      paramList = (List<Message>)localObject;
      if (localObject == null)
      {
        paramList = new ArrayList();
        localHashMap1.put(c.f, paramList);
      }
      paramList.add(localMessage.l());
    }
    paramList = localHashMap2.entrySet().iterator();
    while (paramList.hasNext())
    {
      localObject = (Map.Entry)paramList.next();
      a((Participant)((Map.Entry)localObject).getValue(), paramString1, paramString2, (List)localHashMap1.get(((Map.Entry)localObject).getKey()));
    }
  }
  
  @TargetApi(24)
  private void a(Map<Conversation, List<Message>> paramMap, Map<String, Participant> paramMap1)
  {
    int i1 = Math.min(paramMap.size(), 8);
    boolean bool1;
    if (paramMap.size() != 1) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    Iterator localIterator = paramMap.entrySet().iterator();
    int i2 = 0;
    while (localIterator.hasNext())
    {
      paramMap = (Map.Entry)localIterator.next();
      Conversation localConversation = (Conversation)paramMap.getKey();
      List localList = (List)paramMap.getValue();
      AssertionUtil.isFalse(localList.isEmpty(), new String[] { "Messages cannot be empty" });
      long l1 = get0b;
      Object localObject6 = String.valueOf(l1);
      boolean bool2;
      if ((!w.w().a()) && (localConversation.c())) {
        bool2 = true;
      } else {
        bool2 = false;
      }
      Object localObject7 = a;
      Participant[] arrayOfParticipant = l;
      Object localObject2 = -..Lambda.c.qHL7POPSRzeX6ovKs-OstRwBrbI.INSTANCE;
      c.g.b.k.b(arrayOfParticipant, "receiver$0");
      c.g.b.k.b(localObject2, "selector");
      int i3;
      if (arrayOfParticipant.length == 0) {
        i3 = 1;
      } else {
        i3 = 0;
      }
      int i4;
      if (i3 != 0)
      {
        localObject1 = null;
      }
      else
      {
        paramMap = arrayOfParticipant[0];
        localObject1 = (Comparable)((c.g.a.b)localObject2).invoke(paramMap);
        i4 = c.a.f.e(arrayOfParticipant);
        if (i4 > 0)
        {
          i3 = 1;
          for (;;)
          {
            localObject4 = arrayOfParticipant[i3];
            localObject5 = (Comparable)((c.g.a.b)localObject2).invoke(localObject4);
            localObject3 = localObject1;
            if (((Comparable)localObject1).compareTo(localObject5) < 0)
            {
              localObject3 = localObject5;
              paramMap = (Map<Conversation, List<Message>>)localObject4;
            }
            localObject1 = paramMap;
            if (i3 == i4) {
              break;
            }
            i3 += 1;
            localObject1 = localObject3;
          }
        }
        localObject1 = paramMap;
      }
      paramMap = (Participant)localObject1;
      if (paramMap == null) {
        i3 = 0;
      } else {
        i3 = q;
      }
      Object localObject4 = ((Context)localObject7).getString(2131886146, new Object[] { Integer.valueOf(i3) });
      if (localList.size() > 1)
      {
        paramMap = new z.f();
        if (bool2) {
          paramMap.c((CharSequence)localObject4);
        }
        i4 = localList.size();
        if (bool2) {
          i3 = 2;
        } else {
          i3 = 1;
        }
        i4 = Math.min(i4, 5 - i3);
        i3 = 0;
        while (i3 < i4)
        {
          paramMap.c(a((Message)localList.get(i3), localConversation, paramMap1, bool1));
          i3 += 1;
        }
        if (localList.size() == 5)
        {
          paramMap.c(a((Message)localList.get(4), localConversation, paramMap1, bool1));
        }
        else if (localList.size() > 5)
        {
          i3 = localList.size() - 5 + 1;
          paramMap.c(i.a(2131755022, i3, new Object[] { Integer.valueOf(i3) }));
        }
      }
      else
      {
        paramMap = null;
      }
      Object localObject3 = (Message)localList.get(0);
      localObject2 = o.a();
      Object localObject1 = new z.d(a, (String)localObject2);
      a((z.d)localObject1);
      Object localObject5 = a((Message)localObject3, localConversation, paramMap1, false);
      if (bool2) {
        ((z.d)localObject1).b((CharSequence)localObject4);
      } else {
        ((z.d)localObject1).b((CharSequence)localObject5);
      }
      localObject7 = new StringBuilder();
      if (bool2)
      {
        ((StringBuilder)localObject7).append((String)localObject4);
        ((StringBuilder)localObject7).append("\n");
      }
      ((StringBuilder)localObject7).append((CharSequence)localObject5);
      ((z.d)localObject1).a(new z.c().b(((StringBuilder)localObject7).toString()));
      localObject4 = new StringBuilder();
      ((StringBuilder)localObject4).append(b(localConversation, paramMap1));
      if (localList.size() > 1)
      {
        ((StringBuilder)localObject4).append(' ');
        ((StringBuilder)localObject4).append('(');
        ((StringBuilder)localObject4).append(localList.size());
        ((StringBuilder)localObject4).append(')');
      }
      ((z.d)localObject1).a(((StringBuilder)localObject4).toString());
      ((z.d)localObject1).a(e.a);
      u = "notificationGroupNewMessages";
      if (paramMap != null) {
        ((z.d)localObject1).a(paramMap);
      }
      paramMap = new NotificationIdentifier(2131363802, (String)localObject6, (int)l1);
      ((z.d)localObject1).b(NotificationBroadcastReceiver.a(a, localList, paramMap));
      f = NotificationBroadcastReceiver.b(a, localList, paramMap);
      localObject4 = l[0];
      localObject3 = b(Collections.singletonList(localObject3));
      if (localObject3 != null) {
        i3 = 1;
      } else {
        i3 = 0;
      }
      localObject5 = z.a(com.truecaller.common.h.ab.c(e));
      if ((w.n().a()) && (localObject5 != null) && (((SenderInfo)localObject5).getCategory().equals("bank"))) {
        i4 = 1;
      } else {
        i4 = 0;
      }
      ((z.d)localObject1).a(a(localList, paramMap));
      localObject6 = b(localList, paramMap);
      if (i4 != 0) {
        ((z.d)localObject1).a(a(((SenderInfo)localObject5).getSymbol(), paramMap, localList));
      } else if (bool2) {
        ((z.d)localObject1).a((android.support.v4.app.z.a)localObject6);
      } else if (c == 1) {
        ((z.d)localObject1).a((android.support.v4.app.z.a)localObject6);
      } else if (i3 != 0) {
        ((z.d)localObject1).a(a(localList, (Uri)localObject3));
      } else {
        ((z.d)localObject1).a(a(localList, paramMap, (String)localObject2));
      }
      localObject2 = a(localConversation, paramMap1);
      localObject1 = b.a((z.d)localObject1, new -..Lambda.c.uuy8UBqvqWb_vIBDcJQNeYN0GL4(this, (Uri)localObject2, bool2));
      c.a(b, a, (Notification)localObject1, "notificationIncomingMessage");
      i2 += 1;
      if (i2 >= i1) {
        return;
      }
    }
  }
  
  private boolean a(Message paramMessage, boolean paramBoolean1, boolean paramBoolean2)
  {
    return (r.b("featureSmartNotifications")) && (x.a(com.truecaller.common.h.ab.c(c.e), paramMessage.j(), new com.truecaller.smsparser.j(paramMessage, n, z), paramBoolean1, paramBoolean2, e.a));
  }
  
  private boolean a(List<Message> paramList, boolean paramBoolean)
  {
    paramList = new ArrayList(paramList);
    if ((r.b("featureOTPNotificationEnabled")) && (!paramList.isEmpty()))
    {
      Collections.sort(paramList, -..Lambda.c.eYWY_gW3CluitrPtrGN0Ld2bYRo.INSTANCE);
      Message localMessage = (Message)paramList.get(paramList.size() - 1);
      if (d(localMessage)) {
        return false;
      }
      paramList = n;
      int i3 = paramList.length;
      int i1 = 0;
      int i2 = 0;
      while (i1 < i3)
      {
        if (paramList[i1].a()) {
          i2 = 1;
        }
        i1 += 1;
      }
      if (i2 == 0) {
        return false;
      }
      Object localObject1 = (Participant)a(Collections.singletonList(localMessage)).get(c.f);
      paramList = localMessage.j();
      paramList = s.a(paramList);
      if (paramList == null) {
        return false;
      }
      Object localObject2 = c;
      NotificationIdentifier localNotificationIdentifier = new NotificationIdentifier(2131363804);
      paramList = a;
      PendingIntent localPendingIntent2 = PendingIntent.getService(paramList, 0, x.b(paramList, a, localNotificationIdentifier), 134217728);
      PendingIntent localPendingIntent1;
      if (paramBoolean)
      {
        localPendingIntent1 = NotificationBroadcastReceiver.b(a, Collections.singletonList(localMessage), localNotificationIdentifier);
        paramList = a;
        paramList = PendingIntent.getService(paramList, 0, x.a(paramList, a, localNotificationIdentifier), 134217728);
      }
      else
      {
        localPendingIntent1 = NotificationBroadcastReceiver.a(a, localNotificationIdentifier, false);
        paramList = NotificationBroadcastReceiver.a(a, localNotificationIdentifier, false);
      }
      Object localObject3 = n.d();
      z.d locald = new z.d(a, (String)localObject3);
      RemoteViews localRemoteViews = a(2131559087, (String)localObject2, localNotificationIdentifier, a, ((Participant)localObject1).a());
      localObject2 = a(2131559088, (String)localObject2, localNotificationIdentifier, a, ((Participant)localObject1).a());
      localObject1 = new z.d(a, (String)localObject3).a(i.a(2131888473, new Object[] { ((Participant)localObject1).a() })).b(i.a(2131888472, new Object[0])).h();
      a(locald);
      localObject3 = locald.a(new z.e());
      G = localRemoteViews;
      F = ((RemoteViews)localObject2);
      H = ((RemoteViews)localObject2);
      f = localPendingIntent1;
      D = 0;
      E = ((Notification)localObject1);
      l = 2;
      ((z.d)localObject3).a(2131234063, a.getString(2131886651), paramList).a(2131234000, a.getString(2131887927), localPendingIntent2).d(16);
      b(locald);
      paramList = locald.h();
      c.a(a, paramList, "notificationOtpMessage");
      y.a(a);
      return true;
    }
    return false;
  }
  
  private Uri b(List<Message> paramList)
  {
    String str = a.getString(2131888543);
    Object localObject1 = paramList.iterator();
    paramList = "";
    int i1 = 0;
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (Message)((Iterator)localObject1).next();
      paramList = ((Message)localObject2).j();
      localObject2 = n;
      int i4 = localObject2.length;
      int i2 = 0;
      while (i2 < i4)
      {
        TextEntity localTextEntity = localObject2[i2];
        int i3 = i1;
        if (localTextEntity.a())
        {
          localTextEntity = (TextEntity)localTextEntity;
          i3 = i1;
          if (am.c(a))
          {
            i3 = i1;
            if (a.toLowerCase(Locale.getDefault()).contains(str)) {
              i3 = 1;
            }
          }
        }
        i2 += 1;
        i1 = i3;
      }
    }
    Object localObject2 = null;
    localObject1 = localObject2;
    if (i1 != 0)
    {
      paramList = Patterns.WEB_URL.matcher(paramList);
      do
      {
        localObject1 = localObject2;
        if (!paramList.find()) {
          break;
        }
        localObject1 = Uri.parse(paramList.group());
      } while (!paramList.group().contains(str));
    }
    return (Uri)localObject1;
  }
  
  private android.support.v4.app.z.a b(List<Message> paramList, NotificationIdentifier paramNotificationIdentifier)
  {
    Participant localParticipant = get0c;
    String str;
    if (c == 0) {
      str = "PHONE_NUMBER";
    } else {
      str = "OTHER";
    }
    return new android.support.v4.app.z.a(2131233887, a.getString(2131886772), NotificationBroadcastReceiver.a(a, paramList, f, str, m, paramNotificationIdentifier));
  }
  
  private z.g b(Map<Conversation, List<Message>> paramMap, Map<String, Participant> paramMap1)
  {
    ArrayList localArrayList = new ArrayList();
    Object localObject1 = paramMap.entrySet().iterator();
    Object localObject2;
    Object localObject3;
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (Map.Entry)((Iterator)localObject1).next();
      localObject3 = ((List)((Map.Entry)localObject2).getValue()).iterator();
      while (((Iterator)localObject3).hasNext()) {
        localArrayList.add(Pair.create((Message)((Iterator)localObject3).next(), ((Map.Entry)localObject2).getKey()));
      }
    }
    Collections.sort(localArrayList, -..Lambda.c.v84qnbk_rL0LBijXJw1V2-y66pQ.INSTANCE);
    localObject1 = new z.f();
    int i2 = Math.min(localArrayList.size(), 5);
    int i1 = 0;
    for (;;)
    {
      boolean bool = true;
      if (i1 >= i2) {
        break;
      }
      localObject2 = (Message)getfirst;
      localObject3 = (Conversation)getsecond;
      if (paramMap.size() == 1) {
        bool = false;
      }
      ((z.f)localObject1).c(a((Message)localObject2, (Conversation)localObject3, paramMap1, bool));
      i1 += 1;
    }
    if (localArrayList.size() > 1) {
      ((z.f)localObject1).b(i.a(2131886767, new Object[] { Integer.valueOf(localArrayList.size()) }));
    }
    return (z.g)localObject1;
  }
  
  private String b(Conversation paramConversation, Map<String, Participant> paramMap)
  {
    if (x != null) {
      return am.n(x.b);
    }
    return c.a.f.a(l, ", ", "", "", -1, "", new -..Lambda.c.glVnJ6DS4ziZUaEaR1mHPR3zBlo(this, paramMap));
  }
  
  private void b(z.d paramd)
  {
    int i1 = d.g();
    if ((j.s()) && (i1 != 0)) {
      i1 = 1;
    } else {
      i1 = 0;
    }
    if (i1 != 0) {
      i1 = 6;
    } else {
      i1 = 4;
    }
    paramd.c(i1);
    paramd.a(f.a());
  }
  
  private void b(Map<Conversation, List<Message>> paramMap)
  {
    Object localObject4 = c(paramMap);
    Collections.sort((List)localObject4, -..Lambda.c.63PzRZJp3i8IUw1QKmo2Xj6v_TM.INSTANCE);
    Map localMap = a((List)localObject4);
    Object localObject2 = q;
    Object localObject1 = localObject2;
    if (localObject2 == null) {
      localObject1 = g();
    }
    localObject2 = ((List)localObject4).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject3 = (Message)((Iterator)localObject2).next();
      if ((!((Set)localObject1).contains(Long.valueOf(a))) && ((w.w().a()) || (!a((Message)localObject3, localMap).g())))
      {
        i1 = 1;
        break label127;
      }
    }
    int i1 = 0;
    label127:
    Object localObject6 = (Message)((List)localObject4).get(0);
    Object localObject5 = (Conversation)paramMap.keySet().iterator().next();
    Participant localParticipant = a((Message)localObject6, localMap);
    boolean bool;
    if ((!w.w().a()) && (localParticipant.g())) {
      bool = true;
    } else {
      bool = false;
    }
    if (bool) {
      localObject2 = n.a();
    } else {
      localObject2 = n.d();
    }
    z.d locald = new z.d(a, (String)localObject2);
    a(locald);
    Object localObject7 = new StringBuilder();
    ((StringBuilder)localObject7).append(b((Conversation)localObject5, localMap));
    if (bool)
    {
      ((StringBuilder)localObject7).append(' ');
      ((StringBuilder)localObject7).append('(');
      ((StringBuilder)localObject7).append(a.getString(2131886146, new Object[] { Integer.valueOf(q) }));
      ((StringBuilder)localObject7).append(")");
    }
    locald.a(e.a);
    Object localObject3 = a((Conversation)localObject5, localMap);
    if ((((List)localObject4).size() > 1) && (paramMap.size() > 1)) {
      locald.a(m.a(paramMap.keySet(), ", ", "", "", -1, "", new -..Lambda.c.QP2mlYWLu6w_cfuttbUkvW7midY(this, localMap))).b(a.getString(2131886767, new Object[] { Integer.valueOf(((List)localObject4).size()) })).a(at.a(android.support.v4.content.b.a(a, 2131233849)));
    } else {
      locald.a(((StringBuilder)localObject7).toString()).b(a((Message)localObject6, (Conversation)localObject5, localMap, false));
    }
    locald.a(b(paramMap, localMap));
    localObject5 = new NotificationIdentifier(2131363802, String.valueOf(b), (int)b);
    localObject6 = b(Collections.singletonList(localObject6));
    int i2;
    if (localObject6 != null) {
      i2 = 1;
    } else {
      i2 = 0;
    }
    localObject7 = z.a(com.truecaller.common.h.ab.c(e));
    int i3;
    if ((w.n().a()) && (localObject7 != null) && (((SenderInfo)localObject7).getCategory().equals("bank"))) {
      i3 = 1;
    } else {
      i3 = 0;
    }
    locald.a(a((List)localObject4, (NotificationIdentifier)localObject5));
    android.support.v4.app.z.a locala = b((List)localObject4, (NotificationIdentifier)localObject5);
    if ((bool) && (localMap.size() == 1))
    {
      if (i3 != 0) {
        locald.a(a(((SenderInfo)localObject7).getSymbol(), (NotificationIdentifier)localObject5, (List)localObject4));
      } else {
        locald.a(locala);
      }
    }
    else if (paramMap.size() == 1) {
      if (i3 != 0) {
        locald.a(a(((SenderInfo)localObject7).getSymbol(), (NotificationIdentifier)localObject5, (List)localObject4));
      } else if (c == 1) {
        locald.a(locala);
      } else if (i2 != 0) {
        locald.a(a((List)localObject4, (Uri)localObject6));
      } else {
        locald.a(a((List)localObject4, (NotificationIdentifier)localObject5, (String)localObject2));
      }
    }
    localObject2 = null;
    if (i1 != 0)
    {
      b(locald);
      localObject2 = "notificationIncomingMessage";
    }
    locald.b(NotificationBroadcastReceiver.a(a, (List)localObject4, (NotificationIdentifier)localObject5));
    f = NotificationBroadcastReceiver.b(a, (List)localObject4, (NotificationIdentifier)localObject5);
    if ((Build.VERSION.SDK_INT >= 21) && (i1 != 0) && (!C)) {
      l = 2;
    }
    if (Build.VERSION.SDK_INT >= 24)
    {
      localObject4 = ((List)localObject4).iterator();
      while (((Iterator)localObject4).hasNext()) {
        if (!((Set)localObject1).contains(Long.valueOf(nexta)))
        {
          i1 = 1;
          break label925;
        }
      }
      i1 = 0;
      label925:
      if (i1 != 0)
      {
        if (paramMap.entrySet().size() > 8) {
          c.a(2131363802);
        }
        a(paramMap, localMap);
        u = "notificationGroupNewMessages";
        k = paramMap.size();
        v = true;
        paramMap = b.a(locald, new -..Lambda.c.icf079ZWEysZuRMYGCZeOinH_-k(this, (Uri)localObject3, bool));
        c.a(2131363802, paramMap, (String)localObject2);
      }
      return;
    }
    paramMap = b.a(locald, new -..Lambda.c.nyo57IeR80zUM2yjMOTrkeujZ2M(this, (Uri)localObject3, bool));
    c.a(2131363802, paramMap, (String)localObject2);
  }
  
  private static List<Message> c(Map<Conversation, List<Message>> paramMap)
  {
    ArrayList localArrayList = new ArrayList();
    paramMap = paramMap.values().iterator();
    while (paramMap.hasNext()) {
      localArrayList.addAll((List)paramMap.next());
    }
    return localArrayList;
  }
  
  private boolean d(Message paramMessage)
  {
    return com.truecaller.search.f.a(a, c.f);
  }
  
  /* Error */
  private Set<Long> g()
  {
    // Byte code:
    //   0: new 1349	java/util/HashSet
    //   3: dup
    //   4: invokespecial 1350	java/util/HashSet:<init>	()V
    //   7: astore_1
    //   8: aload_0
    //   9: getfield 84	com/truecaller/messaging/notifications/c:a	Landroid/content/Context;
    //   12: ldc_w 805
    //   15: invokevirtual 1354	android/content/Context:openFileInput	(Ljava/lang/String;)Ljava/io/FileInputStream;
    //   18: astore_2
    //   19: new 1356	java/io/DataInputStream
    //   22: dup
    //   23: aload_2
    //   24: invokespecial 1359	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
    //   27: astore_3
    //   28: aload_1
    //   29: aload_3
    //   30: invokevirtual 1362	java/io/DataInputStream:readLong	()J
    //   33: invokestatic 1293	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   36: invokeinterface 1363 2 0
    //   41: pop
    //   42: goto -14 -> 28
    //   45: astore_3
    //   46: aload_2
    //   47: invokestatic 832	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   50: aload_3
    //   51: athrow
    //   52: aload_2
    //   53: invokestatic 832	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   56: aload_1
    //   57: invokestatic 1367	java/util/Collections:unmodifiableSet	(Ljava/util/Set;)Ljava/util/Set;
    //   60: areturn
    //   61: astore_2
    //   62: goto -6 -> 56
    //   65: astore_3
    //   66: goto -14 -> 52
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	69	0	this	c
    //   7	50	1	localHashSet	HashSet
    //   18	35	2	localFileInputStream	java.io.FileInputStream
    //   61	1	2	localIOException	IOException
    //   27	3	3	localDataInputStream	java.io.DataInputStream
    //   45	6	3	localObject	Object
    //   65	1	3	localEOFException	java.io.EOFException
    // Exception table:
    //   from	to	target	type
    //   19	28	45	finally
    //   28	42	45	finally
    //   8	19	61	java/io/IOException
    //   46	52	61	java/io/IOException
    //   52	56	61	java/io/IOException
    //   19	28	65	java/io/EOFException
    //   28	42	65	java/io/EOFException
  }
  
  public final void a()
  {
    C = true;
  }
  
  public final void a(int paramInt)
  {
    k.a(a, MessagesActivity.class, paramInt);
  }
  
  public final void a(long paramLong)
  {
    B = paramLong;
  }
  
  public final void a(ImGroupInfo paramImGroupInfo)
  {
    Object localObject = o.a();
    z.d locald = new z.d(a, (String)localObject).a(b).b(a.getString(2131886606));
    NotificationIdentifier localNotificationIdentifier = new NotificationIdentifier(2131363316, a, a.hashCode());
    f = NotificationBroadcastReceiver.a(a, localNotificationIdentifier, paramImGroupInfo);
    a(locald);
    if (am.b(c)) {
      localObject = null;
    } else {
      localObject = Uri.parse(c);
    }
    localObject = b.a(locald, new -..Lambda.c.CLu4ddU0EYy_HjyAEGZ4PxO378w(this, (Uri)localObject));
    c.a(a, a, (Notification)localObject, "notificationImGroupInvitation");
  }
  
  public final void a(Message paramMessage)
  {
    if (!d.a()) {
      return;
    }
    boolean bool = r.a("smart_notifications", true);
    int i2 = 0;
    if ((bool) && (a(Collections.singletonList(paramMessage), false))) {
      return;
    }
    Object localObject2 = q;
    Object localObject1 = localObject2;
    if (localObject2 == null) {
      localObject1 = g();
    }
    if (bool) {
      a(paramMessage, false, r.a("smart_notifications_clicked", false));
    }
    if (((Set)localObject1).contains(Long.valueOf(a))) {
      return;
    }
    long l1 = TimeUnit.DAYS.toMillis(2L);
    localObject1 = new org.a.a.b().az_();
    localObject2 = r.a();
    if ((((org.a.a.b)localObject1).a_(b.b((aa)localObject2, a)).d(org.a.a.e.a())) && (!d(paramMessage)))
    {
      int i1 = com.truecaller.common.b.e.a("featurePromoIncomingMsgCount", 0);
      if (j.w() < i1) {
        i1 = 1;
      } else {
        i1 = 0;
      }
      if ((i1 != 0) && (j.g().b(l1).d(org.a.a.e.a())))
      {
        localObject1 = a(Collections.singletonList(paramMessage));
        paramMessage = m.a(a(paramMessage, (Map)localObject1), o.a());
        if (paramMessage == null)
        {
          i1 = i2;
        }
        else
        {
          c.a(2131363759, paramMessage, "notificationIncomingMessagePromo");
          i1 = 1;
        }
        if (i1 != 0)
        {
          i1 = j.w();
          j.g(i1 + 1);
          j.a(org.a.a.b.ay_());
        }
      }
    }
  }
  
  public final void a(Map<Conversation, List<Message>> paramMap)
  {
    if (a(c(paramMap), true)) {
      return;
    }
    HashSet localHashSet = new HashSet();
    HashMap localHashMap = new HashMap();
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    boolean bool2 = r.a("smart_notifications", true);
    Iterator localIterator1 = paramMap.entrySet().iterator();
    Object localObject;
    while (localIterator1.hasNext())
    {
      paramMap = (Map.Entry)localIterator1.next();
      Conversation localConversation = (Conversation)paramMap.getKey();
      Iterator localIterator2 = ((List)paramMap.getValue()).iterator();
      while (localIterator2.hasNext())
      {
        Message localMessage = (Message)localIterator2.next();
        if (B == b)
        {
          localHashSet.add(Long.valueOf(a));
          localArrayList2.add(localMessage);
        }
        else if ((bool2) && (a(localMessage, true, r.a("smart_notifications_clicked", false))))
        {
          y.a(a);
        }
        else
        {
          paramMap = c;
          boolean bool1;
          if ((h.g()) && (!w.w().a())) {
            bool1 = true;
          } else {
            bool1 = false;
          }
          if (paramMap.a(bool1))
          {
            localArrayList1.add(localMessage);
          }
          else
          {
            localObject = (List)localHashMap.get(localConversation);
            paramMap = (Map<Conversation, List<Message>>)localObject;
            if (localObject == null)
            {
              paramMap = new ArrayList();
              localHashMap.put(localConversation, paramMap);
            }
            paramMap.add(localMessage);
          }
          localHashSet.add(Long.valueOf(a));
        }
      }
    }
    if (localHashMap.isEmpty()) {
      c.a(2131363802);
    } else {
      b(localHashMap);
    }
    if (localArrayList1.isEmpty())
    {
      c.a(2131363798);
    }
    else
    {
      a(localArrayList1, "inSpammerList", "spamNotification");
      if ((!D) && (j.i()))
      {
        paramMap = n.g();
        paramMap = new z.d(a, paramMap);
        localObject = paramMap.a(2131234318);
        C = android.support.v4.content.b.c(a, 2131099685);
        aa.getResources().getQuantityString(2131755021, localArrayList1.size(), new Object[] { Integer.valueOf(localArrayList1.size()) })).b(a.getString(2131886687)).a(BitmapFactory.decodeResource(a.getResources(), 2131234328)).b(NotificationBroadcastReceiver.a(a, localArrayList1, new NotificationIdentifier(2131363798))).f = NotificationBroadcastReceiver.a(a, localArrayList1);
        c.a(2131363798, paramMap.h(), "notificationBlockedMessage");
      }
    }
    q = Collections.unmodifiableSet(localHashSet);
    a(localHashSet);
    if (!localArrayList2.isEmpty()) {
      a(localArrayList2, "inConversationView", "conversation");
    }
  }
  
  public final void b()
  {
    C = false;
  }
  
  public final void b(long paramLong)
  {
    if (B == paramLong) {
      B = -100L;
    }
  }
  
  public final void b(ImGroupInfo paramImGroupInfo)
  {
    c.a(a, 2131363316);
  }
  
  public final void b(Message paramMessage)
  {
    if (B == b) {
      return;
    }
    E = b;
    z.d locald = new z.d(a, o.a());
    a(locald);
    Object localObject = c;
    Resources localResources = a.getResources();
    String str = localResources.getString(2131886692);
    localObject = localResources.getString(2131886689, new Object[] { ((Participant)localObject).a() });
    locald.a(str);
    locald.b((CharSequence)localObject);
    locald.a(BitmapFactory.decodeResource(a.getResources(), 2131234329));
    str = localResources.getString(2131886690);
    localObject = NotificationBroadcastReceiver.a(a, paramMessage);
    locald.a(2131234000, str, (PendingIntent)localObject);
    locald.a(2131234528, localResources.getString(2131886691), NotificationBroadcastReceiver.b(a, paramMessage));
    f = NotificationBroadcastReceiver.b(a, Collections.singletonList(paramMessage), new NotificationIdentifier(2131363030));
    locald.a(e.a);
    locald.d(16);
    b(locald);
    locald.b((PendingIntent)localObject);
    c.a(2131363030, locald.h(), "notificationFailedMessage");
  }
  
  public final void c()
  {
    c.a(2131363759);
  }
  
  public final void c(long paramLong)
  {
    if (paramLong == E)
    {
      c.a(2131363030);
      E = -1L;
    }
  }
  
  public final void c(Message paramMessage)
  {
    Context localContext = a;
    localContext.startActivity(ClassZeroActivity.a(localContext, paramMessage));
  }
  
  public final void d()
  {
    Toast.makeText(a, 2131886688, 0).show();
  }
  
  public final void e()
  {
    f();
    p.a();
    Object localObject = o.a();
    localObject = new z.d(a, (String)localObject).a(a.getString(2131886694)).b(a.getString(2131886693));
    f = NotificationBroadcastReceiver.a(a, "notificationSMSPromoSpamOff");
    a((z.d)localObject);
    localObject = b.a((z.d)localObject, new -..Lambda.c.aFsKICY9ztiDpK5ZgyNNnMdemVo(this), new -..Lambda.c.onXxWD0OSK_vvJxZHrVLE-EQ_tI(this));
    c.a(2131364562, (Notification)localObject, "notificationSmsPromoSpamOff");
  }
  
  public final void f()
  {
    c.a(2131364562);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */