package com.truecaller.messaging.notifications;

import com.truecaller.messaging.data.types.Conversation;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.data.types.Message;
import java.util.List;
import java.util.Map;

public abstract interface a
{
  public abstract void a();
  
  public abstract void a(int paramInt);
  
  public abstract void a(long paramLong);
  
  public abstract void a(ImGroupInfo paramImGroupInfo);
  
  public abstract void a(Message paramMessage);
  
  public abstract void a(Map<Conversation, List<Message>> paramMap);
  
  public abstract void b();
  
  public abstract void b(long paramLong);
  
  public abstract void b(ImGroupInfo paramImGroupInfo);
  
  public abstract void b(Message paramMessage);
  
  public abstract void c();
  
  public abstract void c(long paramLong);
  
  public abstract void c(Message paramMessage);
  
  public abstract void d();
  
  public abstract void e();
  
  public abstract void f();
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */