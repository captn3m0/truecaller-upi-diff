package com.truecaller.messaging.data;

import android.content.ContentResolver;
import dagger.a.d;
import javax.inject.Provider;

public final class z
  implements d<y>
{
  private final Provider<ContentResolver> a;
  private final Provider<c> b;
  
  private z(Provider<ContentResolver> paramProvider, Provider<c> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static z a(Provider<ContentResolver> paramProvider, Provider<c> paramProvider1)
  {
    return new z(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.z
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */