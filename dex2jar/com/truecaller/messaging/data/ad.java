package com.truecaller.messaging.data;

import com.truecaller.featuretoggles.e;
import dagger.a.d;
import javax.inject.Provider;

public final class ad
  implements d<ac>
{
  private final Provider<e> a;
  
  private ad(Provider<e> paramProvider)
  {
    a = paramProvider;
  }
  
  public static ad a(Provider<e> paramProvider)
  {
    return new ad(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.ad
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */