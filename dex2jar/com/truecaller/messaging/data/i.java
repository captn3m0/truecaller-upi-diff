package com.truecaller.messaging.data;

import android.content.Context;
import dagger.a.d;
import java.io.File;
import javax.inject.Provider;

public final class i
  implements d<File[]>
{
  private final e a;
  private final Provider<Context> b;
  
  private i(e parame, Provider<Context> paramProvider)
  {
    a = parame;
    b = paramProvider;
  }
  
  public static i a(e parame, Provider<Context> paramProvider)
  {
    return new i(parame, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */