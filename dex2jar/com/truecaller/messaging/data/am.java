package com.truecaller.messaging.data;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.database.ContentObserver;
import android.os.Handler;
import android.os.Looper;
import c.g.a.b;
import c.g.b.k;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.content.TruecallerContract.g;
import com.truecaller.network.search.e;
import com.truecaller.network.search.e.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public final class am
  implements al, e.a
{
  final List<al.a> a;
  ak b;
  boolean c;
  final f<com.truecaller.messaging.notifications.a> d;
  private List<String> e;
  private com.truecaller.androidactors.a f;
  private boolean g;
  private final ContentObserver h;
  private final am.c i;
  private final ContentResolver j;
  private final f<o> k;
  private final i l;
  private final com.truecaller.util.al m;
  private final e n;
  
  public am(ContentResolver paramContentResolver, f<o> paramf, i parami, com.truecaller.util.al paramal, e parame, f<com.truecaller.messaging.notifications.a> paramf1)
  {
    j = paramContentResolver;
    k = paramf;
    l = parami;
    m = paramal;
    n = parame;
    d = paramf1;
    e = ((List)new ArrayList());
    a = ((List)new ArrayList());
    b = new ak(0, 0, 0, 0L);
    h = ((ContentObserver)new am.a(this, new Handler(Looper.getMainLooper())));
    i = new am.c(this);
  }
  
  private final void f()
  {
    com.truecaller.androidactors.a locala = f;
    if (locala != null) {
      locala.a();
    }
    f = null;
    c = false;
  }
  
  public final List<String> a()
  {
    return e;
  }
  
  public final void a(al.a parama)
  {
    k.b(parama, "observer");
    int i1;
    if ((g) && (!c)) {
      i1 = 1;
    } else {
      i1 = 0;
    }
    if (i1 != 0) {
      c(parama);
    }
    a.add(parama);
  }
  
  public final void a(Collection<String> paramCollection)
  {
    k.b(paramCollection, "normalizedNumbers");
    d();
  }
  
  public final void a(List<String> paramList)
  {
    k.b(paramList, "<set-?>");
    e = paramList;
  }
  
  public final void a(Set<String> paramSet)
  {
    k.b(paramSet, "normalizedNumbers");
  }
  
  public final void b()
  {
    if (g) {
      return;
    }
    j.registerContentObserver(TruecallerContract.g.a(), true, h);
    m.a((BroadcastReceiver)i, new String[] { "com.truecaller.messaging.spam.SEARCH_COMPLETED" });
    n.a((e.a)this);
    g = true;
    d();
  }
  
  public final void b(al.a parama)
  {
    k.b(parama, "observer");
    a.remove(parama);
  }
  
  public final void c()
  {
    j.unregisterContentObserver(h);
    m.a((BroadcastReceiver)i);
    n.b((e.a)this);
    g = false;
  }
  
  final void c(al.a parama)
  {
    parama.a(b);
  }
  
  public final void d()
  {
    f();
    c = true;
    f = ((o)k.a()).b().a(l, (ac)new an((b)new am.b((am)this)));
  }
  
  public final boolean e()
  {
    return g;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.am
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */