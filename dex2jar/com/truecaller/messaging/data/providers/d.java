package com.truecaller.messaging.data.providers;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Environment;
import android.webkit.MimeTypeMap;
import c.a.an;
import c.a.f;
import c.a.m;
import c.a.y;
import c.g.b.k;
import c.u;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.utils.l;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import javax.inject.Inject;
import org.c.a.a.a.i;

public final class d
  implements c
{
  private final SimpleDateFormat a;
  private final Context b;
  private final l c;
  
  @Inject
  public d(Context paramContext, l paraml)
  {
    b = paramContext;
    c = paraml;
    a = new SimpleDateFormat("yyMMdd-HHmmss", Locale.US);
  }
  
  private static File b()
  {
    return new File(Environment.getExternalStorageDirectory(), "Truecaller");
  }
  
  private final File c()
  {
    return new File(b.getFilesDir(), "media");
  }
  
  public final Uri a(BinaryEntity paramBinaryEntity)
  {
    k.b(paramBinaryEntity, "entity");
    Object localObject1 = b;
    k.a(localObject1, "entity.content");
    if (k.a(((Uri)localObject1).getScheme(), "content"))
    {
      localObject1 = b;
      k.a(localObject1, "entity.content");
      if (k.a(((Uri)localObject1).getAuthority(), "com.truecaller.attachmentprovider")) {
        return b;
      }
    }
    localObject1 = b.b();
    k.a(localObject1, "MMS_PART_URI");
    localObject1 = ((Uri)localObject1).getScheme();
    Object localObject2 = b;
    k.a(localObject2, "entity.content");
    if (!(k.a(localObject1, ((Uri)localObject2).getScheme()) ^ true))
    {
      localObject1 = b.b();
      k.a(localObject1, "MMS_PART_URI");
      localObject1 = ((Uri)localObject1).getAuthority();
      localObject2 = b;
      k.a(localObject2, "entity.content");
      if ((k.a(localObject1, ((Uri)localObject2).getAuthority()) ^ true)) {
        return null;
      }
      localObject1 = b.b();
      k.a(localObject1, "MMS_PART_URI");
      localObject2 = ((Uri)localObject1).getPathSegments().iterator();
      localObject1 = b;
      k.a(localObject1, "entity.content");
      localObject1 = ((Uri)localObject1).getPathSegments();
      k.a(localObject1, "entity.content.pathSegments");
      Object localObject3 = (Iterable)localObject1;
      localObject1 = (Collection)new ArrayList();
      localObject3 = ((Iterable)localObject3).iterator();
      while (((Iterator)localObject3).hasNext())
      {
        localObject4 = ((Iterator)localObject3).next();
        String str = (String)localObject4;
        int i;
        if ((((Iterator)localObject2).hasNext()) && (k.a(str, (String)((Iterator)localObject2).next()))) {
          i = 1;
        } else {
          i = 0;
        }
        if (i == 0) {
          ((Collection)localObject1).add(localObject4);
        }
      }
      localObject3 = ((Iterable)localObject1).iterator();
      while (((Iterator)localObject3).hasNext())
      {
        localObject1 = ((Iterator)localObject3).next();
        if ((((Iterator)localObject2).hasNext() ^ true)) {
          break label338;
        }
      }
      localObject1 = null;
      label338:
      localObject3 = (String)localObject1;
      if (localObject3 == null) {
        return null;
      }
      localObject2 = MimeTypeMap.getSingleton().getExtensionFromMimeType(j);
      localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = "bin";
      }
      localObject2 = new Uri.Builder();
      ((Uri.Builder)localObject2).scheme("content");
      ((Uri.Builder)localObject2).authority("com.truecaller.attachmentprovider");
      ((Uri.Builder)localObject2).appendPath("mms");
      Object localObject4 = new StringBuilder();
      ((StringBuilder)localObject4).append((String)localObject3);
      ((StringBuilder)localObject4).append('.');
      ((StringBuilder)localObject4).append((String)localObject1);
      ((Uri.Builder)localObject2).appendEncodedPath(((StringBuilder)localObject4).toString());
      ((Uri.Builder)localObject2).appendQueryParameter("mime", j);
      return ((Uri.Builder)localObject2).build();
    }
    return null;
  }
  
  public final Uri a(File paramFile, String paramString, boolean paramBoolean)
  {
    k.b(paramFile, "file");
    k.b(paramString, "contentType");
    Uri.Builder localBuilder = new Uri.Builder();
    localBuilder.scheme("content");
    localBuilder.authority("com.truecaller.attachmentprovider");
    if (paramBoolean) {
      localObject = "public_media";
    } else {
      localObject = "private_media";
    }
    localBuilder.appendPath((String)localObject);
    Object localObject = paramFile.getParentFile();
    k.a(localObject, "file.parentFile");
    localBuilder.appendPath(((File)localObject).getName());
    localBuilder.appendPath(paramFile.getName());
    localBuilder.appendQueryParameter("mime", paramString);
    localBuilder.appendQueryParameter("tmp", String.valueOf(System.currentTimeMillis()));
    return localBuilder.build();
  }
  
  public final File a(long paramLong)
  {
    File localFile = new File(b.getFilesDir(), "media/".concat(String.valueOf(paramLong)));
    if ((!localFile.exists()) && (!localFile.mkdirs())) {
      return null;
    }
    return new File(localFile, i.b(28));
  }
  
  public final File a(long paramLong, String paramString)
  {
    k.b(paramString, "contentType");
    File localFile = b();
    StringBuilder localStringBuilder = new StringBuilder("Truecaller ");
    String str;
    if (Entity.c(paramString)) {
      str = "Images";
    } else if (Entity.d(paramString)) {
      str = "Video";
    } else if (Entity.e(paramString)) {
      str = "Audio";
    } else {
      str = "Documents";
    }
    localStringBuilder.append(str);
    localFile = new File(localFile, localStringBuilder.toString());
    if ((!localFile.exists()) && (!localFile.mkdirs())) {
      return null;
    }
    if (Entity.c(paramString)) {
      str = "IMG";
    } else if (Entity.d(paramString)) {
      str = "VID";
    } else if (Entity.e(paramString)) {
      str = "AUD";
    } else {
      str = "DOC";
    }
    paramString = MimeTypeMap.getSingleton().getExtensionFromMimeType(paramString);
    if (paramString == null) {
      paramString = "";
    } else {
      paramString = ".".concat(String.valueOf(paramString));
    }
    localStringBuilder = new StringBuilder();
    localStringBuilder.append(str);
    localStringBuilder.append('-');
    localStringBuilder.append(a.format(new Date()));
    localStringBuilder.append('-');
    localStringBuilder.append(paramLong);
    localStringBuilder.append(paramString);
    return new File(localFile, localStringBuilder.toString());
  }
  
  public final File a(Uri paramUri)
  {
    k.b(paramUri, "uri");
    if (!(k.a(paramUri.getScheme(), "content") ^ true))
    {
      if ((k.a(paramUri.getAuthority(), "com.truecaller.attachmentprovider") ^ true)) {
        return null;
      }
      Object localObject = paramUri.getPathSegments();
      k.a(localObject, "uri.pathSegments");
      localObject = (String)m.e((List)localObject);
      if (localObject == null) {
        return null;
      }
      int i = ((String)localObject).hashCode();
      if (i != 870994574)
      {
        if (i != 959285800) {
          return null;
        }
        if (((String)localObject).equals("private_media")) {
          return b.a(paramUri, c());
        }
      }
      else if (((String)localObject).equals("public_media"))
      {
        return b.a(paramUri, b());
      }
      return null;
    }
    return null;
  }
  
  public final void a(List<String> paramList)
  {
    k.b(paramList, "filePaths");
    Object localObject = (Collection)paramList;
    if ((((Collection)localObject).isEmpty() ^ true))
    {
      paramList = b;
      localObject = ((Collection)localObject).toArray(new String[0]);
      if (localObject != null)
      {
        MediaScannerConnection.scanFile(paramList, (String[])localObject, null, (MediaScannerConnection.OnScanCompletedListener)d.a.a);
        return;
      }
      throw new u("null cannot be cast to non-null type kotlin.Array<T>");
    }
  }
  
  public final boolean a()
  {
    return (k.a(Environment.getExternalStorageState(), "mounted")) && (c.c());
  }
  
  public final Iterable<File> b(long paramLong)
  {
    Object localObject = new File(c(), String.valueOf(paramLong));
    if (!((File)localObject).exists()) {
      return (Iterable)y.a;
    }
    localObject = ((File)localObject).listFiles();
    if (localObject != null)
    {
      Iterable localIterable = f.i((Object[])localObject);
      localObject = localIterable;
      if (localIterable != null) {}
    }
    else
    {
      localObject = (Iterable)y.a;
    }
    return (Iterable<File>)localObject;
  }
  
  public final boolean b(Uri paramUri)
  {
    k.b(paramUri, "uri");
    if ((k.a(paramUri.getScheme(), "content")) && (k.a(paramUri.getAuthority(), "com.truecaller.attachmentprovider")))
    {
      Iterable localIterable = (Iterable)an.a(new String[] { "private_media", "public_media" });
      paramUri = paramUri.getPathSegments();
      k.a(paramUri, "uri.pathSegments");
      if (m.a(localIterable, m.e(paramUri))) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean c(Uri paramUri)
  {
    k.b(paramUri, "uri");
    paramUri = paramUri.getPathSegments();
    k.a(paramUri, "uri.pathSegments");
    return k.a("public_media", (String)m.e(paramUri));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.providers.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */