package com.truecaller.messaging.data.providers;

import android.content.Context;
import com.truecaller.utils.l;
import javax.inject.Provider;

public final class e
  implements dagger.a.d<d>
{
  private final Provider<Context> a;
  private final Provider<l> b;
  
  private e(Provider<Context> paramProvider, Provider<l> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static e a(Provider<Context> paramProvider, Provider<l> paramProvider1)
  {
    return new e(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.providers.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */