package com.truecaller.messaging.data;

import android.database.Cursor;
import com.truecaller.messaging.data.a.a;
import com.truecaller.messaging.data.a.b;
import com.truecaller.messaging.data.a.e;
import com.truecaller.messaging.data.a.i;
import com.truecaller.messaging.data.a.j;
import com.truecaller.messaging.data.a.k;
import com.truecaller.messaging.data.a.l;
import com.truecaller.messaging.data.a.m;
import com.truecaller.messaging.data.a.o;
import com.truecaller.messaging.data.a.p;
import com.truecaller.messaging.data.a.q;
import com.truecaller.messaging.data.a.r;

final class d
  implements c
{
  public final a a(Cursor paramCursor)
  {
    if (paramCursor != null) {
      return new b(paramCursor);
    }
    return null;
  }
  
  public final com.truecaller.messaging.data.a.c a(Cursor paramCursor, boolean paramBoolean)
  {
    if (paramCursor == null) {
      return null;
    }
    if (paramBoolean) {
      return new e(paramCursor);
    }
    return new com.truecaller.messaging.data.a.d(paramCursor);
  }
  
  public final j b(Cursor paramCursor)
  {
    if (paramCursor != null) {
      return new s(paramCursor);
    }
    return null;
  }
  
  public final m c(Cursor paramCursor)
  {
    if (paramCursor != null) {
      return new aa(paramCursor);
    }
    return null;
  }
  
  public final r d(Cursor paramCursor)
  {
    if (paramCursor != null) {
      return new ai(paramCursor);
    }
    return null;
  }
  
  public final com.truecaller.messaging.data.a.s e(Cursor paramCursor)
  {
    if (paramCursor != null) {
      return new aj(paramCursor);
    }
    return null;
  }
  
  public final com.truecaller.messaging.data.a.g f(Cursor paramCursor)
  {
    if (paramCursor != null) {
      return new n(paramCursor);
    }
    return null;
  }
  
  public final p g(Cursor paramCursor)
  {
    if (paramCursor != null) {
      return new q(paramCursor);
    }
    return null;
  }
  
  public final k h(Cursor paramCursor)
  {
    if (paramCursor != null) {
      return new l(paramCursor);
    }
    return null;
  }
  
  public final com.truecaller.messaging.transport.im.a.g i(Cursor paramCursor)
  {
    if (paramCursor != null) {
      return new com.truecaller.messaging.transport.im.a.h(paramCursor);
    }
    return null;
  }
  
  public final com.truecaller.messaging.data.a.h j(Cursor paramCursor)
  {
    if (paramCursor != null) {
      return new i(paramCursor);
    }
    return null;
  }
  
  public final com.truecaller.messaging.data.a.n k(Cursor paramCursor)
  {
    if (paramCursor != null) {
      return new o(paramCursor);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */