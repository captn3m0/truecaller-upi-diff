package com.truecaller.messaging.data;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import dagger.a.d;
import javax.inject.Provider;

public final class l
  implements d<f<t>>
{
  private final e a;
  private final Provider<t> b;
  private final Provider<i> c;
  
  private l(e parame, Provider<t> paramProvider, Provider<i> paramProvider1)
  {
    a = parame;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static l a(e parame, Provider<t> paramProvider, Provider<i> paramProvider1)
  {
    return new l(parame, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */