package com.truecaller.messaging.data;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.database.sqlite.SQLiteFullException;
import android.net.Uri;
import android.os.Environment;
import android.os.RemoteException;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import com.google.common.collect.Sets;
import com.truecaller.backup.bk;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.aa;
import com.truecaller.content.TruecallerContract.ab;
import com.truecaller.content.TruecallerContract.an;
import com.truecaller.content.TruecallerContract.g;
import com.truecaller.content.TruecallerContract.h;
import com.truecaller.content.TruecallerContract.i;
import com.truecaller.content.TruecallerContract.z;
import com.truecaller.filters.p;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.messaging.data.a.j;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.types.AudioEntity;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Conversation;
import com.truecaller.messaging.data.types.Conversation.a;
import com.truecaller.messaging.data.types.Draft;
import com.truecaller.messaging.data.types.Draft.a;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.ImageEntity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.TextEntity;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.data.types.VideoEntity;
import com.truecaller.messaging.h;
import com.truecaller.messaging.transport.NullTransportInfo;
import com.truecaller.messaging.transport.ad;
import com.truecaller.messaging.transport.i;
import com.truecaller.messaging.transport.im.as;
import com.truecaller.messaging.transport.l;
import com.truecaller.messaging.transport.l.a;
import com.truecaller.messaging.transport.l.b;
import com.truecaller.messaging.transport.m;
import com.truecaller.tracking.events.w.a;
import com.truecaller.util.ch;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import org.c.a.a.a.k;

final class v
  implements t
{
  static final org.a.a.b a = new org.a.a.b(org.a.a.f.a);
  private static final ContentProviderResult[] b = new ContentProviderResult[0];
  private final com.truecaller.messaging.categorizer.d A;
  private final al B;
  private final dagger.a<com.truecaller.messaging.h.c> C;
  private final x D;
  private final ContentResolver c;
  private final File d;
  private File[] e;
  private final dagger.a<m> f;
  private final dagger.a<com.truecaller.messaging.transport.d> g;
  private final Provider<i> h;
  private final Provider<com.truecaller.messaging.transport.f> i;
  private final com.truecaller.androidactors.f<com.truecaller.messaging.notifications.a> j;
  private final h k;
  private final c l;
  private final p m;
  private final ae n;
  private final com.truecaller.util.al o;
  private final ch p;
  private final com.truecaller.androidactors.f<com.truecaller.analytics.ae> q;
  private final com.truecaller.messaging.c.a r;
  private v.d s = null;
  private final com.truecaller.utils.s t;
  private final o u;
  private final com.truecaller.androidactors.f<as> v;
  private final ab w;
  private final com.truecaller.featuretoggles.e x;
  private final bk y;
  private final com.truecaller.messaging.categorizer.b z;
  
  @Inject
  v(ContentResolver paramContentResolver, @Named("cache_dir") File paramFile, @Named("storage_dirs") File[] paramArrayOfFile, h paramh, c paramc, o paramo, com.truecaller.androidactors.f<as> paramf, dagger.a<m> parama, dagger.a<com.truecaller.messaging.transport.d> parama1, com.truecaller.androidactors.f<com.truecaller.messaging.notifications.a> paramf1, Provider<com.truecaller.messaging.transport.f> paramProvider, Provider<i> paramProvider1, ae paramae, p paramp, com.truecaller.util.al paramal, ch paramch, com.truecaller.androidactors.f<com.truecaller.analytics.ae> paramf2, com.truecaller.utils.s params, com.truecaller.messaging.c.a parama2, ab paramab, com.truecaller.featuretoggles.e parame, bk parambk, com.truecaller.messaging.categorizer.b paramb, com.truecaller.messaging.categorizer.d paramd, al paramal1, dagger.a<com.truecaller.messaging.h.c> parama3, x paramx)
  {
    c = paramContentResolver;
    d = new File(paramFile, "msg_media");
    e = paramArrayOfFile;
    k = paramh;
    f = parama;
    g = parama1;
    l = paramc;
    u = paramo;
    v = paramf;
    j = paramf1;
    i = paramProvider;
    h = paramProvider1;
    n = paramae;
    m = paramp;
    o = paramal;
    p = paramch;
    q = paramf2;
    t = params;
    r = parama2;
    w = paramab;
    x = parame;
    y = parambk;
    z = paramb;
    A = paramd;
    B = paramal1;
    C = parama3;
    D = paramx;
  }
  
  private int a(t.a parama, com.truecaller.utils.q paramq, Iterable<l> paramIterable)
  {
    ArrayList localArrayList = new ArrayList();
    HashSet localHashSet = new HashSet();
    Object localObject1 = new StringBuilder("Staring sync batch ");
    ((StringBuilder)localObject1).append(parama);
    ((StringBuilder)localObject1).append(" with messages limit 40");
    ((StringBuilder)localObject1).toString();
    localObject1 = new SparseBooleanArray();
    Iterator localIterator = paramIterable.iterator();
    int i1 = 0;
    int i2 = 0;
    Object localObject2 = this;
    l locall;
    int i3;
    org.a.a.b localb2;
    org.a.a.b localb1;
    if (localIterator.hasNext())
    {
      locall = (l)localIterator.next();
      i3 = locall.f();
      localb2 = parama.c();
      localb1 = parama.a(i3);
      if (localb1 == null) {
        localb1 = a;
      }
      if (localb1.d(localb2)) {}
    }
    for (;;)
    {
      long l1;
      try
      {
        l1 = a;
        long l2 = a;
        Cursor localCursor = c.query(Uri.withAppendedPath(TruecallerContract.b, "msg/msg_messages_by_transport_view"), ai.a, "transport=? AND date>=? AND date <=?", new String[] { String.valueOf(i3), String.valueOf(l1), String.valueOf(l2) }, "date DESC, raw_id DESC");
        localObject2 = l.d(localCursor);
        if (localObject2 == null) {}
        try
        {
          parama.a(i3, localb2);
          com.truecaller.util.q.a((Cursor)localObject2);
        }
        finally
        {
          int i4;
          parama = (t.a)localObject2;
          continue;
        }
        i4 = localArrayList.size();
        l1 = locall.a(parama.a(), parama.b(), (r)localObject2, localb2, localb1, 40, localArrayList, paramq, parama.e(), parama.f(), localHashSet);
        if (!l.b.b(l1)) {
          break label615;
        }
        parama.a(i3, localb2);
        localArrayList.subList(i4, localArrayList.size()).clear();
        continue;
        if (i1 != 0)
        {
          parama.a(i3, localb2);
          ((SparseBooleanArray)localObject1).put(i3, true);
          i1 = 1;
        }
        else
        {
          parama.a(i3, new org.a.a.b(l1 & 0x3FFFFFFFFFFFFFFF));
          ((SparseBooleanArray)localObject1).put(i3, true);
          i1 = 1;
          i2 = 1;
        }
        com.truecaller.util.q.a((Cursor)localObject2);
      }
      finally
      {
        parama = null;
        com.truecaller.util.q.a(parama);
      }
      return 2;
      paramq = a(localArrayList);
      if ((!localArrayList.isEmpty()) && (paramq.length == 0)) {
        return 2;
      }
      if (x.w().a())
      {
        if (localHashSet.remove(Long.valueOf(-1L))) {
          localHashSet.addAll(a(paramq));
        }
        if (!localHashSet.isEmpty()) {
          a(A.a(localHashSet));
        }
      }
      paramq = paramIterable.iterator();
      while (paramq.hasNext())
      {
        paramIterable = (l)paramq.next();
        if ((((SparseBooleanArray)localObject1).get(paramIterable.f(), false)) && (paramIterable.d().c(parama.d()))) {
          paramIterable.a(parama.d());
        }
      }
      if (i2 != 0) {
        return 0;
      }
      return 1;
      label615:
      if ((0x8000000000000000 & l1) != 0L) {
        i1 = 1;
      } else {
        i1 = 0;
      }
    }
  }
  
  private SparseBooleanArray a(String paramString, boolean paramBoolean)
  {
    SparseArray localSparseArray1 = new SparseArray(5);
    SparseArray localSparseArray2 = new SparseArray(5);
    SparseArray localSparseArray3 = new SparseArray(5);
    SparseBooleanArray localSparseBooleanArray = new SparseBooleanArray(5);
    Object localObject1;
    if (!paramBoolean)
    {
      localObject1 = new StringBuilder("(");
      ((StringBuilder)localObject1).append(paramString);
      ((StringBuilder)localObject1).append(") AND (read = 0)");
      localObject1 = ((StringBuilder)localObject1).toString();
    }
    else
    {
      localObject1 = paramString;
    }
    TransportInfo localTransportInfo = null;
    Object localObject3 = null;
    paramString = localTransportInfo;
    label635:
    for (;;)
    {
      try
      {
        localObject1 = c.query(TruecallerContract.h.a(), null, (String)localObject1, null, null);
        if (localObject1 != null)
        {
          paramString = localTransportInfo;
          localObject1 = l.e((Cursor)localObject1);
          localObject3 = localObject1;
          paramString = (String)localObject1;
          if (((com.truecaller.messaging.data.a.s)localObject1).moveToNext())
          {
            paramString = (String)localObject1;
            i1 = ((com.truecaller.messaging.data.a.s)localObject1).d();
            paramString = (String)localObject1;
            localTransportInfo = ((com.truecaller.messaging.data.a.s)localObject1).e();
            paramString = (String)localObject1;
            l locall = ((m)f.get()).b(i1);
            if (locall == null)
            {
              paramString = (String)localObject1;
              AssertionUtil.OnlyInDebug.fail(new String[] { "Unsupported transport type: ".concat(String.valueOf(i1)) });
              com.truecaller.util.q.a((Cursor)localObject1);
              return localSparseBooleanArray;
            }
            paramString = (String)localObject1;
            localObject3 = (ad)localSparseArray1.get(i1);
            if (localObject3 != null) {
              break label635;
            }
            paramString = (String)localObject1;
            localObject3 = locall.b();
            paramString = (String)localObject1;
            localSparseArray2.put(i1, org.a.a.b.ay_());
            paramString = (String)localObject1;
            localSparseArray3.put(i1, new ArrayList());
            paramString = (String)localObject1;
            localSparseArray1.put(i1, localObject3);
            paramString = (String)localObject1;
            if (locall.a(localTransportInfo, ((com.truecaller.messaging.data.a.s)localObject1).a(), ((com.truecaller.messaging.data.a.s)localObject1).b(), (ad)localObject3))
            {
              paramString = (String)localObject1;
              localObject3 = new org.a.a.b(((com.truecaller.messaging.data.a.s)localObject1).b());
              paramString = (String)localObject1;
              if (((org.a.a.b)localSparseArray2.get(i1)).b((org.a.a.x)localObject3))
              {
                paramString = (String)localObject1;
                localSparseArray2.put(i1, localObject3);
              }
              paramString = (String)localObject1;
              if ((((com.truecaller.messaging.data.a.s)localObject1).c() & 0x1) == 0)
              {
                paramString = (String)localObject1;
                ((List)localSparseArray3.get(i1)).add(localTransportInfo.a((org.a.a.b)localObject3));
              }
            }
            continue;
          }
        }
        com.truecaller.util.q.a((Cursor)localObject3);
        paramString = new HashSet();
        int i2 = localSparseArray1.size();
        int i1 = 0;
        if (i1 < i2)
        {
          localObject1 = ((m)f.get()).b(localSparseArray1.keyAt(i1));
          AssertionUtil.AlwaysFatal.isNotNull(localObject1, new String[] { "Only known transports should by part of transactions" });
          localObject3 = (ad)localSparseArray1.valueAt(i1);
          if ((((l)localObject1).b((ad)localObject3)) && (((l)localObject1).a((ad)localObject3))) {
            paramBoolean = true;
          } else {
            paramBoolean = false;
          }
          if (paramBoolean)
          {
            ((l)localObject1).a((org.a.a.b)localSparseArray2.get(((l)localObject1).f()));
            a((List)localSparseArray3.valueAt(i1));
            paramString.add(localObject1);
          }
          localSparseBooleanArray.put(((l)localObject1).f(), paramBoolean);
          i1 += 1;
        }
        else
        {
          if (!paramString.isEmpty()) {
            b(true, paramString);
          }
          return localSparseBooleanArray;
        }
      }
      finally
      {
        com.truecaller.util.q.a(paramString);
      }
    }
  }
  
  private Message a(Message paramMessage, Participant[] paramArrayOfParticipant)
  {
    Object localObject3 = new ArrayList();
    Object localObject1;
    if (paramMessage.b()) {
      localObject1 = f(a);
    } else {
      localObject1 = null;
    }
    Object localObject2 = n;
    int i1 = 0;
    localObject2 = a((Entity[])localObject2, false);
    paramArrayOfParticipant = a(a, paramArrayOfParticipant, (List)localObject2, (ArrayList)localObject3, q, x);
    localObject3 = a((ArrayList)localObject3);
    if (localObject3.length == 0)
    {
      b((List)localObject2);
      return null;
    }
    Object localObject4 = n;
    int i2 = localObject4.length;
    while (i1 < i2)
    {
      BinaryEntity localBinaryEntity = localObject4[i1];
      if (!localBinaryEntity.a())
      {
        localBinaryEntity = (BinaryEntity)localBinaryEntity;
        if (c) {
          p.a(localBinaryEntity);
        } else if ((a(b)) && (localObject1 != null)) {
          ((Set)localObject1).remove(new File(b.getPath()));
        }
      }
      i1 += 1;
    }
    if (localObject1 != null)
    {
      localObject1 = ((Set)localObject1).iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject4 = (File)((Iterator)localObject1).next();
        if (((File)localObject4).exists()) {
          ((File)localObject4).delete();
        }
      }
    }
    long l1 = ContentUris.parseId(b].uri);
    long l2 = ContentUris.parseId(c].uri);
    paramMessage = paramMessage.m();
    a = l2;
    b = l1;
    return paramMessage.a().a((Collection)localObject2).b();
  }
  
  private static v.a a(long paramLong1, Participant[] paramArrayOfParticipant, List<Entity> paramList, ArrayList<ContentProviderOperation> paramArrayList, int paramInt, long paramLong2)
  {
    int i2 = b.a(paramArrayList, paramArrayOfParticipant[0]);
    int i1 = 1;
    while (i1 < paramArrayOfParticipant.length)
    {
      b.a(paramArrayList, paramArrayOfParticipant[i1]);
      i1 += 1;
    }
    i1 = b.a(paramArrayList, Sets.newHashSet(paramArrayOfParticipant));
    paramArrayOfParticipant = ContentProviderOperation.newInsert(TruecallerContract.aa.a());
    if (paramLong1 != -1L) {
      paramArrayOfParticipant.withValue("_id", Long.valueOf(paramLong1));
    }
    paramArrayOfParticipant.withValueBackReference("participant_id", i2);
    paramArrayOfParticipant.withValueBackReference("conversation_id", i1);
    paramArrayOfParticipant.withValue("date", Long.valueOf(System.currentTimeMillis()));
    paramArrayOfParticipant.withValue("status", Integer.valueOf(3));
    paramArrayOfParticipant.withValue("seen", Integer.valueOf(1));
    paramArrayOfParticipant.withValue("read", Integer.valueOf(1));
    paramArrayOfParticipant.withValue("locked", Integer.valueOf(0));
    paramArrayOfParticipant.withValue("transport", Integer.valueOf(3));
    paramArrayOfParticipant.withValue("category", Integer.valueOf(paramInt));
    paramArrayOfParticipant.withValue("reply_to_msg_id", Long.valueOf(paramLong2));
    paramInt = paramArrayList.size();
    paramArrayList.add(paramArrayOfParticipant.build());
    paramArrayOfParticipant = ContentProviderOperation.newDelete(TruecallerContract.z.a());
    paramArrayOfParticipant.withSelection("message_id=?", new String[1]).withSelectionBackReference(0, paramInt);
    paramArrayList.add(paramArrayOfParticipant.build());
    paramArrayOfParticipant = paramList.iterator();
    while (paramArrayOfParticipant.hasNext())
    {
      Object localObject = (Entity)paramArrayOfParticipant.next();
      paramList = ContentProviderOperation.newInsert(TruecallerContract.z.a());
      paramList.withValue("type", j);
      if (((Entity)localObject).a())
      {
        paramList.withValue("content", a);
      }
      else
      {
        BinaryEntity localBinaryEntity = (BinaryEntity)localObject;
        paramList.withValue("content", b.toString());
        paramList.withValue("size", Long.valueOf(d));
        if (((Entity)localObject).b())
        {
          localObject = (ImageEntity)localObject;
          paramList.withValue("width", Integer.valueOf(a));
          paramList.withValue("height", Integer.valueOf(k));
        }
        else if (((Entity)localObject).c())
        {
          localObject = (VideoEntity)localObject;
          paramList.withValue("width", Integer.valueOf(a));
          paramList.withValue("height", Integer.valueOf(k));
          paramList.withValue("duration", Integer.valueOf(l));
          paramList.withValue("thumbnail", m.toString());
        }
        else if (((Entity)localObject).e())
        {
          paramList.withValue("duration", Integer.valueOf(a));
        }
      }
      paramList.withValueBackReference("message_id", paramInt);
      paramArrayList.add(paramList.build());
    }
    return new v.a(i2, i1, paramInt, (byte)0);
  }
  
  private v.d a(org.a.a.b paramb1, org.a.a.b paramb2, int paramInt)
  {
    v.d locald = s;
    if (locald != null)
    {
      s = locald.a(paramb1, paramb2, paramInt);
      return null;
    }
    s = new v.d(paramb1, paramb2, paramInt);
    return s;
  }
  
  /* Error */
  private List<Entity> a(Entity[] paramArrayOfEntity, boolean paramBoolean)
  {
    // Byte code:
    //   0: new 175	java/util/ArrayList
    //   3: dup
    //   4: invokespecial 176	java/util/ArrayList:<init>	()V
    //   7: astore 20
    //   9: aload_0
    //   10: getfield 118	com/truecaller/messaging/data/v:d	Ljava/io/File;
    //   13: invokevirtual 546	java/io/File:exists	()Z
    //   16: ifne +16 -> 32
    //   19: aload_0
    //   20: getfield 118	com/truecaller/messaging/data/v:d	Ljava/io/File;
    //   23: invokevirtual 728	java/io/File:mkdirs	()Z
    //   26: ifne +6 -> 32
    //   29: aload 20
    //   31: areturn
    //   32: aload_1
    //   33: arraylength
    //   34: istore 8
    //   36: iconst_0
    //   37: istore 6
    //   39: iload 6
    //   41: iload 8
    //   43: if_icmpge +739 -> 782
    //   46: aload_1
    //   47: iload 6
    //   49: aaload
    //   50: astore 17
    //   52: aload 17
    //   54: astore 11
    //   56: aload 17
    //   58: invokevirtual 524	com/truecaller/messaging/data/types/Entity:a	()Z
    //   61: ifne +702 -> 763
    //   64: aload 17
    //   66: checkcast 526	com/truecaller/messaging/data/types/BinaryEntity
    //   69: astore 21
    //   71: aload 21
    //   73: getfield 529	com/truecaller/messaging/data/types/BinaryEntity:c	Z
    //   76: ifne +23 -> 99
    //   79: aload_0
    //   80: aload 21
    //   82: getfield 535	com/truecaller/messaging/data/types/BinaryEntity:b	Landroid/net/Uri;
    //   85: invokespecial 538	com/truecaller/messaging/data/v:a	(Landroid/net/Uri;)Z
    //   88: ifeq +11 -> 99
    //   91: aload 17
    //   93: astore 11
    //   95: iload_2
    //   96: ifeq +667 -> 763
    //   99: aload 21
    //   101: getfield 680	com/truecaller/messaging/data/types/BinaryEntity:d	J
    //   104: invokestatic 734	android/os/Environment:getDataDirectory	()Ljava/io/File;
    //   107: invokevirtual 735	java/io/File:getPath	()Ljava/lang/String;
    //   110: invokestatic 740	com/truecaller/util/ao:a	(Ljava/lang/String;)J
    //   113: lcmp
    //   114: ifge +8 -> 122
    //   117: iconst_1
    //   118: istore_3
    //   119: goto +5 -> 124
    //   122: iconst_0
    //   123: istore_3
    //   124: aconst_null
    //   125: astore 15
    //   127: aconst_null
    //   128: astore 14
    //   130: aconst_null
    //   131: astore 12
    //   133: aconst_null
    //   134: astore 13
    //   136: aconst_null
    //   137: astore 18
    //   139: iload_3
    //   140: ifne +41 -> 181
    //   143: aload_0
    //   144: invokespecial 742	com/truecaller/messaging/data/v:c	()Ljava/io/File;
    //   147: astore 16
    //   149: aload 16
    //   151: ifnull +27 -> 178
    //   154: aload 16
    //   156: astore 11
    //   158: aload 16
    //   160: invokevirtual 546	java/io/File:exists	()Z
    //   163: ifne +21 -> 184
    //   166: aload 16
    //   168: astore 11
    //   170: aload 16
    //   172: invokevirtual 728	java/io/File:mkdirs	()Z
    //   175: ifne +9 -> 184
    //   178: goto +595 -> 773
    //   181: aconst_null
    //   182: astore 11
    //   184: iload_3
    //   185: ifeq +9 -> 194
    //   188: aload_0
    //   189: getfield 118	com/truecaller/messaging/data/v:d	Ljava/io/File;
    //   192: astore 11
    //   194: new 111	java/io/File
    //   197: dup
    //   198: aload 11
    //   200: bipush 45
    //   202: invokestatic 746	org/c/a/a/a/i:b	(I)Ljava/lang/String;
    //   205: invokespecial 116	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   208: astore 19
    //   210: aload 19
    //   212: invokevirtual 749	java/io/File:createNewFile	()Z
    //   215: ifne +22 -> 237
    //   218: aconst_null
    //   219: invokestatic 752	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   222: aconst_null
    //   223: invokestatic 752	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   226: aload 19
    //   228: invokevirtual 546	java/io/File:exists	()Z
    //   231: ifeq +542 -> 773
    //   234: goto +493 -> 727
    //   237: new 754	java/io/FileOutputStream
    //   240: dup
    //   241: aload 19
    //   243: invokespecial 757	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   246: astore 16
    //   248: aload 18
    //   250: astore 11
    //   252: aload 15
    //   254: astore 12
    //   256: aload_0
    //   257: getfield 109	com/truecaller/messaging/data/v:c	Landroid/content/ContentResolver;
    //   260: aload 21
    //   262: getfield 535	com/truecaller/messaging/data/types/BinaryEntity:b	Landroid/net/Uri;
    //   265: invokevirtual 761	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   268: astore 13
    //   270: aload 13
    //   272: ifnonnull +24 -> 296
    //   275: aload 16
    //   277: invokestatic 752	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   280: aload 13
    //   282: invokestatic 752	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   285: aload 19
    //   287: invokevirtual 546	java/io/File:exists	()Z
    //   290: ifeq +483 -> 773
    //   293: goto +434 -> 727
    //   296: aload 13
    //   298: astore 11
    //   300: aload 13
    //   302: astore 12
    //   304: aload 13
    //   306: astore 14
    //   308: aload 13
    //   310: aload 16
    //   312: invokestatic 766	com/truecaller/utils/extensions/m:a	(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    //   315: lstore 9
    //   317: aload 13
    //   319: astore 11
    //   321: aload 13
    //   323: astore 12
    //   325: aload 13
    //   327: astore 14
    //   329: aload 19
    //   331: invokestatic 770	android/net/Uri:fromFile	(Ljava/io/File;)Landroid/net/Uri;
    //   334: astore 18
    //   336: aload 13
    //   338: astore 11
    //   340: aload 13
    //   342: astore 12
    //   344: aload 13
    //   346: astore 14
    //   348: aload 17
    //   350: invokevirtual 681	com/truecaller/messaging/data/types/Entity:b	()Z
    //   353: ifeq +432 -> 785
    //   356: aload 13
    //   358: astore 11
    //   360: aload 13
    //   362: astore 12
    //   364: aload 13
    //   366: astore 14
    //   368: aload 17
    //   370: checkcast 683	com/truecaller/messaging/data/types/ImageEntity
    //   373: getfield 687	com/truecaller/messaging/data/types/ImageEntity:a	I
    //   376: istore_3
    //   377: aload 13
    //   379: astore 11
    //   381: aload 13
    //   383: astore 12
    //   385: aload 13
    //   387: astore 14
    //   389: aload 17
    //   391: checkcast 683	com/truecaller/messaging/data/types/ImageEntity
    //   394: getfield 691	com/truecaller/messaging/data/types/ImageEntity:k	I
    //   397: istore 4
    //   399: goto +3 -> 402
    //   402: aload 13
    //   404: astore 11
    //   406: aload 13
    //   408: astore 12
    //   410: aload 13
    //   412: astore 14
    //   414: getstatic 773	android/net/Uri:EMPTY	Landroid/net/Uri;
    //   417: astore 15
    //   419: aload 13
    //   421: astore 11
    //   423: aload 13
    //   425: astore 12
    //   427: aload 13
    //   429: astore 14
    //   431: aload 17
    //   433: invokevirtual 693	com/truecaller/messaging/data/types/Entity:c	()Z
    //   436: ifeq +357 -> 793
    //   439: aload 13
    //   441: astore 11
    //   443: aload 13
    //   445: astore 12
    //   447: aload 13
    //   449: astore 14
    //   451: aload 17
    //   453: checkcast 695	com/truecaller/messaging/data/types/VideoEntity
    //   456: getfield 696	com/truecaller/messaging/data/types/VideoEntity:a	I
    //   459: istore_3
    //   460: aload 13
    //   462: astore 11
    //   464: aload 13
    //   466: astore 12
    //   468: aload 13
    //   470: astore 14
    //   472: aload 17
    //   474: checkcast 695	com/truecaller/messaging/data/types/VideoEntity
    //   477: getfield 697	com/truecaller/messaging/data/types/VideoEntity:k	I
    //   480: istore 4
    //   482: aload 13
    //   484: astore 11
    //   486: aload 13
    //   488: astore 12
    //   490: aload 13
    //   492: astore 14
    //   494: aload 17
    //   496: checkcast 695	com/truecaller/messaging/data/types/VideoEntity
    //   499: getfield 701	com/truecaller/messaging/data/types/VideoEntity:l	I
    //   502: istore 5
    //   504: aload 13
    //   506: astore 11
    //   508: aload 13
    //   510: astore 12
    //   512: aload 13
    //   514: astore 14
    //   516: aload 17
    //   518: checkcast 695	com/truecaller/messaging/data/types/VideoEntity
    //   521: getfield 705	com/truecaller/messaging/data/types/VideoEntity:m	Landroid/net/Uri;
    //   524: astore 15
    //   526: goto +3 -> 529
    //   529: aload 13
    //   531: astore 11
    //   533: aload 13
    //   535: astore 12
    //   537: aload 13
    //   539: astore 14
    //   541: aload 17
    //   543: invokevirtual 706	com/truecaller/messaging/data/types/Entity:e	()Z
    //   546: ifeq +253 -> 799
    //   549: aload 13
    //   551: astore 11
    //   553: aload 13
    //   555: astore 12
    //   557: aload 13
    //   559: astore 14
    //   561: aload 17
    //   563: checkcast 708	com/truecaller/messaging/data/types/AudioEntity
    //   566: getfield 709	com/truecaller/messaging/data/types/AudioEntity:a	I
    //   569: istore 4
    //   571: aload 13
    //   573: astore 11
    //   575: aload 13
    //   577: astore 12
    //   579: aload 13
    //   581: astore 14
    //   583: getstatic 773	android/net/Uri:EMPTY	Landroid/net/Uri;
    //   586: astore 15
    //   588: iconst_m1
    //   589: istore_3
    //   590: iconst_m1
    //   591: istore 5
    //   593: goto +3 -> 596
    //   596: aload 13
    //   598: astore 11
    //   600: aload 13
    //   602: astore 12
    //   604: aload 13
    //   606: astore 14
    //   608: ldc2_w 344
    //   611: aload 17
    //   613: getfield 670	com/truecaller/messaging/data/types/Entity:j	Ljava/lang/String;
    //   616: aload 18
    //   618: iload_3
    //   619: iload 5
    //   621: iload 4
    //   623: lload 9
    //   625: aload 15
    //   627: invokestatic 776	com/truecaller/messaging/data/types/Entity:a	(JLjava/lang/String;Landroid/net/Uri;IIIJLandroid/net/Uri;)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   630: astore 15
    //   632: aload 16
    //   634: invokestatic 752	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   637: aload 13
    //   639: invokestatic 752	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   642: aload 15
    //   644: astore 11
    //   646: goto +117 -> 763
    //   649: astore_1
    //   650: aload 16
    //   652: astore 12
    //   654: goto +83 -> 737
    //   657: astore 11
    //   659: goto +9 -> 668
    //   662: astore 11
    //   664: aload 14
    //   666: astore 12
    //   668: aload 11
    //   670: astore 13
    //   672: aload 12
    //   674: astore 11
    //   676: aload 16
    //   678: astore 12
    //   680: goto +24 -> 704
    //   683: astore_1
    //   684: aconst_null
    //   685: astore 12
    //   687: aload 13
    //   689: astore 11
    //   691: goto +46 -> 737
    //   694: astore 13
    //   696: goto +5 -> 701
    //   699: astore 13
    //   701: aconst_null
    //   702: astore 11
    //   704: aload 13
    //   706: invokestatic 782	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   709: aload 12
    //   711: invokestatic 752	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   714: aload 11
    //   716: invokestatic 752	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   719: aload 19
    //   721: invokevirtual 546	java/io/File:exists	()Z
    //   724: ifeq +49 -> 773
    //   727: aload 19
    //   729: invokevirtual 549	java/io/File:delete	()Z
    //   732: pop
    //   733: goto +40 -> 773
    //   736: astore_1
    //   737: aload 12
    //   739: invokestatic 752	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   742: aload 11
    //   744: invokestatic 752	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   747: aload 19
    //   749: invokevirtual 546	java/io/File:exists	()Z
    //   752: ifeq +9 -> 761
    //   755: aload 19
    //   757: invokevirtual 549	java/io/File:delete	()Z
    //   760: pop
    //   761: aload_1
    //   762: athrow
    //   763: aload 20
    //   765: aload 11
    //   767: invokeinterface 468 2 0
    //   772: pop
    //   773: iload 6
    //   775: iconst_1
    //   776: iadd
    //   777: istore 6
    //   779: goto -740 -> 39
    //   782: aload 20
    //   784: areturn
    //   785: iconst_m1
    //   786: istore_3
    //   787: iconst_m1
    //   788: istore 4
    //   790: goto -388 -> 402
    //   793: iconst_m1
    //   794: istore 5
    //   796: goto -267 -> 529
    //   799: iload 5
    //   801: istore 7
    //   803: iload 4
    //   805: istore 5
    //   807: iload 7
    //   809: istore 4
    //   811: goto -215 -> 596
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	814	0	this	v
    //   0	814	1	paramArrayOfEntity	Entity[]
    //   0	814	2	paramBoolean	boolean
    //   118	669	3	i1	int
    //   397	413	4	i2	int
    //   502	304	5	i3	int
    //   37	741	6	i4	int
    //   801	7	7	i5	int
    //   34	10	8	i6	int
    //   315	309	9	l1	long
    //   54	591	11	localObject1	Object
    //   657	1	11	localSecurityException1	SecurityException
    //   662	7	11	localIOException1	java.io.IOException
    //   674	92	11	localObject2	Object
    //   131	607	12	localObject3	Object
    //   134	554	13	localObject4	Object
    //   694	1	13	localSecurityException2	SecurityException
    //   699	6	13	localIOException2	java.io.IOException
    //   128	537	14	localObject5	Object
    //   125	518	15	localObject6	Object
    //   147	530	16	localObject7	Object
    //   50	562	17	localEntity	Entity
    //   137	480	18	localUri	Uri
    //   208	548	19	localFile	File
    //   7	776	20	localArrayList	ArrayList
    //   69	192	21	localBinaryEntity	BinaryEntity
    // Exception table:
    //   from	to	target	type
    //   256	270	649	finally
    //   308	317	649	finally
    //   329	336	649	finally
    //   348	356	649	finally
    //   368	377	649	finally
    //   389	399	649	finally
    //   414	419	649	finally
    //   431	439	649	finally
    //   451	460	649	finally
    //   472	482	649	finally
    //   494	504	649	finally
    //   516	526	649	finally
    //   541	549	649	finally
    //   561	571	649	finally
    //   583	588	649	finally
    //   608	632	649	finally
    //   256	270	657	java/lang/SecurityException
    //   308	317	657	java/lang/SecurityException
    //   329	336	657	java/lang/SecurityException
    //   348	356	657	java/lang/SecurityException
    //   368	377	657	java/lang/SecurityException
    //   389	399	657	java/lang/SecurityException
    //   414	419	657	java/lang/SecurityException
    //   431	439	657	java/lang/SecurityException
    //   451	460	657	java/lang/SecurityException
    //   472	482	657	java/lang/SecurityException
    //   494	504	657	java/lang/SecurityException
    //   516	526	657	java/lang/SecurityException
    //   541	549	657	java/lang/SecurityException
    //   561	571	657	java/lang/SecurityException
    //   583	588	657	java/lang/SecurityException
    //   608	632	657	java/lang/SecurityException
    //   256	270	662	java/io/IOException
    //   308	317	662	java/io/IOException
    //   329	336	662	java/io/IOException
    //   348	356	662	java/io/IOException
    //   368	377	662	java/io/IOException
    //   389	399	662	java/io/IOException
    //   414	419	662	java/io/IOException
    //   431	439	662	java/io/IOException
    //   451	460	662	java/io/IOException
    //   472	482	662	java/io/IOException
    //   494	504	662	java/io/IOException
    //   516	526	662	java/io/IOException
    //   541	549	662	java/io/IOException
    //   561	571	662	java/io/IOException
    //   583	588	662	java/io/IOException
    //   608	632	662	java/io/IOException
    //   210	218	683	finally
    //   237	248	683	finally
    //   210	218	694	java/lang/SecurityException
    //   237	248	694	java/lang/SecurityException
    //   210	218	699	java/io/IOException
    //   237	248	699	java/io/IOException
    //   704	709	736	finally
  }
  
  private Set<l> a(Collection<Integer> paramCollection)
  {
    HashSet localHashSet = new HashSet();
    paramCollection = paramCollection.iterator();
    while (paramCollection.hasNext())
    {
      Integer localInteger = (Integer)paramCollection.next();
      localHashSet.add(((m)f.get()).a(localInteger.intValue()));
    }
    return localHashSet;
  }
  
  private static Set<Long> a(ContentProviderResult[] paramArrayOfContentProviderResult)
  {
    HashSet localHashSet = new HashSet();
    int i2 = paramArrayOfContentProviderResult.length;
    int i1 = 0;
    while (i1 < i2)
    {
      Uri localUri = uri;
      if ((localUri != null) && (localUri.getPath() != null) && (localUri.getPath().contains("msg_messages"))) {
        localHashSet.add(Long.valueOf(ContentUris.parseId(localUri)));
      }
      i1 += 1;
    }
    return localHashSet;
  }
  
  private org.a.a.b a(Message paramMessage, int paramInt, List<ContentProviderOperation> paramList)
  {
    Object localObject = new StringBuilder("Enqueue for sending message: ");
    ((StringBuilder)localObject).append(paramMessage);
    ((StringBuilder)localObject).append(" by transport: ");
    ((StringBuilder)localObject).append(paramInt);
    ((StringBuilder)localObject).toString();
    org.a.a.b localb = e;
    l locall = ((m)f.get()).b(paramInt);
    if (locall != null) {
      localObject = null;
    }
    try
    {
      Conversation localConversation = (Conversation)u.a(b).d();
      localObject = localConversation;
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;) {}
    }
    if (localObject == null)
    {
      localObject = paramMessage.m();
      f = 9;
      g = true;
      b.a(paramList, ((Message.a)localObject).b(), -1);
      "Can't fetch conversation for message: ".concat(String.valueOf(paramMessage));
      AssertionUtil.reportWeirdnessButNeverCrash("Trying to enqueue a message but conversation doesn't exist");
      return localb;
    }
    localObject = locall.a(paramMessage, l);
    switch (a)
    {
    default: 
      paramList = localb;
      break;
    case 2: 
      b.a(paramList, a);
      paramList = d;
      break;
    case 1: 
      AssertionUtil.AlwaysFatal.isNotNull(b, new String[0]);
      paramMessage = paramMessage.m();
      f = 5;
      paramMessage = paramMessage.a(locall.f(), b).b();
      b.a(paramList, paramMessage, -1);
      paramList = d;
      break;
    case 0: 
      paramMessage = paramMessage.m();
      f = 9;
      paramMessage = paramMessage.b();
      b.a(paramList, paramMessage, -1);
      r.a("Failure", paramMessage, locall.f());
      paramList = localb;
    }
    r.b(o);
    return paramList;
    localObject = new StringBuilder("Unknown transport: ");
    ((StringBuilder)localObject).append(k);
    ((StringBuilder)localObject).toString();
    paramMessage = paramMessage.m();
    f = 9;
    paramMessage = paramMessage.b();
    b.a(paramList, paramMessage, -1);
    r.a("Failure", paramMessage, 3);
    return localb;
  }
  
  private static void a(SparseBooleanArray paramSparseBooleanArray1, SparseBooleanArray paramSparseBooleanArray2)
  {
    int i1 = 0;
    while (i1 < paramSparseBooleanArray2.size())
    {
      int i2 = paramSparseBooleanArray2.keyAt(i1);
      boolean bool2 = paramSparseBooleanArray2.valueAt(i1);
      boolean bool1 = true;
      if ((!bool2) || (!paramSparseBooleanArray1.get(i2, true))) {
        bool1 = false;
      }
      paramSparseBooleanArray1.put(i2, bool1);
      i1 += 1;
    }
  }
  
  private void a(List<String> paramList)
  {
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      Object localObject = (String)paramList.next();
      try
      {
        localObject = com.truecaller.tracking.events.w.b().a((CharSequence)localObject).a();
        ((com.truecaller.analytics.ae)q.a()).a((org.apache.a.d.d)localObject);
      }
      catch (org.apache.a.a locala)
      {
        for (;;) {}
      }
    }
  }
  
  private void a(org.a.a.b paramb1, org.a.a.b paramb2, int paramInt, Iterable<l> paramIterable)
  {
    paramb1 = a(paramb1, paramb2, paramInt);
    if (paramb1 == null) {
      return;
    }
    boolean bool1 = x.v().a();
    boolean bool2 = x.e().a();
    a(new v.c((com.truecaller.messaging.transport.f)i.get(), (i)h.get(), paramb1, bool1, bool2), a, paramIterable);
  }
  
  private void a(boolean paramBoolean, Iterable<l> paramIterable)
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
  }
  
  private void a(Conversation[] paramArrayOfConversation, v.b paramb)
  {
    int i1 = 0;
    while (i1 < paramArrayOfConversation.length)
    {
      int i2 = Math.min(i1 + 200, paramArrayOfConversation.length);
      StringBuilder localStringBuilder = new StringBuilder();
      while (i1 < i2)
      {
        if (localStringBuilder.length() > 0) {
          localStringBuilder.append(" OR ");
        }
        localStringBuilder.append("conversation_id = ");
        localStringBuilder.append(a);
        localStringBuilder.append(" ");
        localStringBuilder.append(com.truecaller.content.c.a(x, p, q));
        i1 += 1;
      }
      paramb.onSelection(localStringBuilder.toString());
    }
  }
  
  private boolean a(Uri paramUri)
  {
    if (!"file".equals(paramUri.getScheme())) {
      return false;
    }
    return new File(paramUri.getPath()).getParentFile().equals(d);
  }
  
  private ContentProviderResult[] a(ArrayList<ContentProviderOperation> paramArrayList)
  {
    try
    {
      paramArrayList = c.applyBatch(TruecallerContract.a, paramArrayList);
      return paramArrayList;
    }
    catch (SQLiteFullException paramArrayList) {}catch (OperationApplicationException paramArrayList) {}catch (RemoteException paramArrayList) {}
    AssertionUtil.reportThrowableButNeverCrash(paramArrayList);
    return b;
  }
  
  private SparseBooleanArray b(String paramString, boolean paramBoolean)
  {
    SparseArray localSparseArray1 = new SparseArray(5);
    SparseArray localSparseArray2 = new SparseArray(5);
    SparseBooleanArray localSparseBooleanArray = new SparseBooleanArray(5);
    ad localad = null;
    Object localObject2 = null;
    Object localObject1 = localad;
    try
    {
      paramString = c.query(TruecallerContract.h.a(), null, paramString, null, null);
      if (paramString != null)
      {
        localObject1 = localad;
        paramString = l.e(paramString);
        for (;;)
        {
          localObject2 = paramString;
          localObject1 = paramString;
          if (!paramString.moveToNext()) {
            break;
          }
          localObject1 = paramString;
          i1 = paramString.d();
          localObject1 = paramString;
          TransportInfo localTransportInfo = paramString.e();
          localObject1 = paramString;
          l locall = ((m)f.get()).b(i1);
          if (locall == null)
          {
            localObject1 = paramString;
            AssertionUtil.OnlyInDebug.fail(new String[] { "Unsupported transport type: ".concat(String.valueOf(i1)) });
            com.truecaller.util.q.a(paramString);
            return localSparseBooleanArray;
          }
          localObject1 = paramString;
          localad = (ad)localSparseArray1.get(i1);
          localObject2 = localad;
          if (localad == null)
          {
            localObject1 = paramString;
            localObject2 = locall.b();
            localObject1 = paramString;
            localSparseArray1.put(i1, localObject2);
            localObject1 = paramString;
            localSparseArray2.put(i1, org.a.a.b.ay_());
          }
          localObject1 = paramString;
          if (locall.a(localTransportInfo, (ad)localObject2, paramBoolean))
          {
            localObject1 = paramString;
            localObject2 = new org.a.a.b(paramString.b());
            localObject1 = paramString;
            if (((org.a.a.b)localSparseArray2.get(i1)).b((org.a.a.x)localObject2))
            {
              localObject1 = paramString;
              localSparseArray2.put(i1, localObject2);
            }
          }
        }
      }
      com.truecaller.util.q.a((Cursor)localObject2);
      paramString = new HashSet();
      int i2 = localSparseArray1.size();
      int i1 = 0;
      while (i1 < i2)
      {
        localObject1 = ((m)f.get()).b(localSparseArray1.keyAt(i1));
        AssertionUtil.AlwaysFatal.isNotNull(localObject1, new String[] { "Only known transports should by part of transactions" });
        localObject2 = (ad)localSparseArray1.valueAt(i1);
        if ((((l)localObject1).b((ad)localObject2)) && (((l)localObject1).a((ad)localObject2))) {
          paramBoolean = true;
        } else {
          paramBoolean = false;
        }
        if (paramBoolean)
        {
          ((l)localObject1).a((org.a.a.b)localSparseArray2.get(((l)localObject1).f()));
          paramString.add(localObject1);
        }
        localSparseBooleanArray.put(((l)localObject1).f(), paramBoolean);
        i1 += 1;
      }
      if (!paramString.isEmpty()) {
        b(true, paramString);
      }
      return localSparseBooleanArray;
    }
    finally
    {
      com.truecaller.util.q.a((Cursor)localObject1);
    }
  }
  
  private void b(String paramString)
  {
    SparseArray localSparseArray1 = new SparseArray(5);
    SparseArray localSparseArray2 = new SparseArray(5);
    Object localObject2 = null;
    Object localObject1 = localObject2;
    try
    {
      paramString = c.query(TruecallerContract.h.a(), null, paramString, null, null);
      if (paramString == null)
      {
        com.truecaller.util.q.a(null);
        return;
      }
      localObject1 = localObject2;
      localObject2 = l.e(paramString);
      int i1;
      for (;;)
      {
        localObject1 = localObject2;
        boolean bool = ((com.truecaller.messaging.data.a.s)localObject2).moveToNext();
        i1 = 0;
        if (!bool) {
          break;
        }
        localObject1 = localObject2;
        i1 = ((com.truecaller.messaging.data.a.s)localObject2).d();
        localObject1 = localObject2;
        TransportInfo localTransportInfo = ((com.truecaller.messaging.data.a.s)localObject2).e();
        localObject1 = localObject2;
        l locall = ((m)f.get()).b(i1);
        if (locall == null)
        {
          localObject1 = localObject2;
          AssertionUtil.OnlyInDebug.fail(new String[] { "Unsupported transport type: ".concat(String.valueOf(i1)) });
        }
        else
        {
          localObject1 = localObject2;
          ad localad = (ad)localSparseArray1.get(i1);
          paramString = localad;
          if (localad == null)
          {
            localObject1 = localObject2;
            paramString = locall.b();
            localObject1 = localObject2;
            localSparseArray1.put(i1, paramString);
            localObject1 = localObject2;
            localSparseArray2.put(i1, org.a.a.b.ay_());
          }
          localObject1 = localObject2;
          i2 = paramString.b();
          localObject1 = localObject2;
          if (!locall.a(localTransportInfo, paramString))
          {
            localObject1 = localObject2;
            paramString.a(i2);
          }
          else
          {
            localObject1 = localObject2;
            paramString = new org.a.a.b(((com.truecaller.messaging.data.a.s)localObject2).b());
            localObject1 = localObject2;
            if (((org.a.a.b)localSparseArray2.get(i1)).b(paramString))
            {
              localObject1 = localObject2;
              localSparseArray2.put(i1, paramString);
            }
          }
        }
      }
      com.truecaller.util.q.a((Cursor)localObject2);
      paramString = new HashSet();
      int i2 = localSparseArray1.size();
      while (i1 < i2)
      {
        localObject1 = ((m)f.get()).b(localSparseArray1.keyAt(i1));
        AssertionUtil.AlwaysFatal.isNotNull(localObject1, new String[] { "Only known transports should by part of transactions" });
        localObject2 = (ad)localSparseArray1.valueAt(i1);
        if ((((l)localObject1).b((ad)localObject2)) && (((l)localObject1).a((ad)localObject2)))
        {
          ((l)localObject1).a((org.a.a.b)localSparseArray2.get(((l)localObject1).f()));
          paramString.add(localObject1);
        }
        i1 += 1;
      }
      if (!paramString.isEmpty()) {
        b(true, paramString);
      }
      return;
    }
    finally
    {
      com.truecaller.util.q.a((Cursor)localObject1);
    }
  }
  
  private void b(List<Entity> paramList)
  {
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      Object localObject = (Entity)paramList.next();
      if (!((Entity)localObject).a())
      {
        localObject = (BinaryEntity)localObject;
        if (c)
        {
          p.a(b);
        }
        else if (a(b))
        {
          localObject = new File(b.getPath());
          if (((File)localObject).exists()) {
            ((File)localObject).delete();
          }
        }
      }
    }
  }
  
  private void b(boolean paramBoolean, Iterable<l> paramIterable)
  {
    for (;;)
    {
      try
      {
        if (!d())
        {
          a(paramBoolean);
          return;
        }
        Object localObject = org.a.a.b.ay_();
        Iterator localIterator = paramIterable.iterator();
        if (localIterator.hasNext())
        {
          localb = c(((l)localIterator.next()).d());
          if (!localb.c((org.a.a.x)localObject)) {
            continue;
          }
          localObject = localb;
          continue;
        }
        org.a.a.b localb = org.a.a.b.ay_();
        if (paramBoolean)
        {
          i1 = 1;
          a(localb, (org.a.a.b)localObject, i1, paramIterable);
          return;
        }
      }
      finally {}
      int i1 = 0;
    }
  }
  
  private File c()
  {
    if ((e != null) && (Environment.getExternalStorageState().equals("mounted")))
    {
      File[] arrayOfFile = e;
      int i2 = arrayOfFile.length;
      int i1 = 0;
      while (i1 < i2)
      {
        File localFile = arrayOfFile[i1];
        if (!localFile.getAbsolutePath().contains("/emulated/")) {
          return new File(localFile, "msg_media");
        }
        i1 += 1;
      }
    }
    return null;
  }
  
  private static org.a.a.b c(org.a.a.b paramb)
  {
    if (paramb.c(org.a.a.e.a())) {
      return new org.a.a.b(0L);
    }
    return paramb;
  }
  
  private static long[] c(List<Long> paramList)
  {
    return org.c.a.a.a.a.a((Long[])paramList.toArray(new Long[paramList.size()]));
  }
  
  private static String d(long[] paramArrayOfLong)
  {
    StringBuilder localStringBuilder = new StringBuilder("message_id");
    localStringBuilder.append(" IN (");
    localStringBuilder.append(paramArrayOfLong[0]);
    int i1 = 1;
    while (i1 < paramArrayOfLong.length)
    {
      localStringBuilder.append(',');
      localStringBuilder.append(paramArrayOfLong[i1]);
      i1 += 1;
    }
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  private boolean d()
  {
    return k.a() != 0L;
  }
  
  private Set<File> f(long paramLong)
  {
    HashSet localHashSet = new HashSet();
    Object localObject1 = null;
    try
    {
      com.truecaller.messaging.data.a.g localg = l.f(c.query(TruecallerContract.z.a(), null, "message_id=?", new String[] { String.valueOf(paramLong) }, null));
      if (localg != null) {
        for (;;)
        {
          localObject1 = localg;
          if (!localg.moveToNext()) {
            break;
          }
          localObject1 = localg;
          Object localObject3 = localg.a();
          localObject1 = localg;
          if (!((Entity)localObject3).a())
          {
            localObject1 = localg;
            localObject3 = b;
            localObject1 = localg;
            if (a((Uri)localObject3))
            {
              localObject1 = localg;
              localHashSet.add(new File(((Uri)localObject3).getPath()));
            }
          }
        }
      }
      com.truecaller.util.q.a(localg);
      return localHashSet;
    }
    finally
    {
      com.truecaller.util.q.a((Cursor)localObject1);
    }
  }
  
  private void f(Message paramMessage)
  {
    l locall = ((m)f.get()).b(j);
    if (locall == null)
    {
      AssertionUtil.OnlyInDebug.fail(new String[] { "Can not save message to system table: ".concat(String.valueOf(paramMessage)) });
      return;
    }
    if (locall.a(paramMessage))
    {
      b(true, Collections.singleton(Integer.valueOf(locall.f())));
      return;
    }
  }
  
  public final com.truecaller.androidactors.w<SparseBooleanArray> a(int paramInt)
  {
    Object localObject1 = w.a(paramInt);
    Cursor localCursor = c.query(TruecallerContract.g.a(), new String[] { "_id", "split_criteria" }, (String)localObject1, null, null);
    com.truecaller.androidactors.w localw = com.truecaller.androidactors.w.b(new SparseBooleanArray());
    if (localCursor != null) {
      try
      {
        if (localCursor.getCount() > 0)
        {
          Conversation[] arrayOfConversation = new Conversation[localCursor.getCount()];
          int i1 = 0;
          for (;;)
          {
            localObject1 = arrayOfConversation;
            if (!localCursor.moveToNext()) {
              break;
            }
            localObject1 = new Conversation.a();
            a = localCursor.getLong(0);
            q = localCursor.getInt(1);
            p = paramInt;
            arrayOfConversation[i1] = ((Conversation.a)localObject1).a();
            i1 += 1;
          }
        }
        localObject1 = null;
        localCursor.close();
        if (localObject1 != null) {
          return a((Conversation[])localObject1);
        }
      }
      finally
      {
        localCursor.close();
      }
    }
    return localw;
  }
  
  public final com.truecaller.androidactors.w<Message> a(long paramLong)
  {
    return u.b(paramLong);
  }
  
  public final com.truecaller.androidactors.w<SparseBooleanArray> a(long paramLong, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    StringBuilder localStringBuilder = new StringBuilder("conversation_id=");
    localStringBuilder.append(paramLong);
    localStringBuilder.append(" ");
    localStringBuilder.append(com.truecaller.content.c.a(x, paramInt1, paramInt2));
    return com.truecaller.androidactors.w.b(b(localStringBuilder.toString(), paramBoolean));
  }
  
  /* Error */
  public final com.truecaller.androidactors.w<Draft> a(Draft paramDraft)
  {
    // Byte code:
    //   0: aload_1
    //   1: getfield 1155	com/truecaller/messaging/data/types/Draft:d	[Lcom/truecaller/messaging/data/types/Participant;
    //   4: iconst_0
    //   5: anewarray 261	java/lang/String
    //   8: invokestatic 481	com/truecaller/log/AssertionUtil$AlwaysFatal:isNotNull	(Ljava/lang/Object;[Ljava/lang/String;)V
    //   11: aload_1
    //   12: getfield 1155	com/truecaller/messaging/data/types/Draft:d	[Lcom/truecaller/messaging/data/types/Participant;
    //   15: arraylength
    //   16: ifle +9 -> 25
    //   19: iconst_1
    //   20: istore 4
    //   22: goto +6 -> 28
    //   25: iconst_0
    //   26: istore 4
    //   28: iload 4
    //   30: iconst_0
    //   31: anewarray 261	java/lang/String
    //   34: invokestatic 1159	com/truecaller/log/AssertionUtil$AlwaysFatal:isTrue	(Z[Ljava/lang/String;)V
    //   37: ldc_w 1161
    //   40: aload_1
    //   41: invokestatic 836	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   44: invokevirtual 428	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   47: pop
    //   48: aload_1
    //   49: invokevirtual 1162	com/truecaller/messaging/data/types/Draft:a	()Z
    //   52: ifeq +203 -> 255
    //   55: aload_1
    //   56: getfield 1163	com/truecaller/messaging/data/types/Draft:a	J
    //   59: ldc2_w 344
    //   62: lcmp
    //   63: ifeq +455 -> 518
    //   66: iconst_1
    //   67: istore_2
    //   68: goto +3 -> 71
    //   71: aload_1
    //   72: astore 7
    //   74: iload_2
    //   75: ifeq +174 -> 249
    //   78: aload_1
    //   79: getfield 1163	com/truecaller/messaging/data/types/Draft:a	J
    //   82: lstore 5
    //   84: new 175	java/util/ArrayList
    //   87: dup
    //   88: invokespecial 176	java/util/ArrayList:<init>	()V
    //   91: astore 8
    //   93: aload_0
    //   94: lload 5
    //   96: invokespecial 505	com/truecaller/messaging/data/v:f	(J)Ljava/util/Set;
    //   99: astore 7
    //   101: invokestatic 595	com/truecaller/content/TruecallerContract$aa:a	()Landroid/net/Uri;
    //   104: invokestatic 654	android/content/ContentProviderOperation:newDelete	(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;
    //   107: astore 9
    //   109: aload 9
    //   111: ldc_w 1165
    //   114: iconst_1
    //   115: anewarray 261	java/lang/String
    //   118: dup
    //   119: iconst_0
    //   120: lload 5
    //   122: invokestatic 268	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   125: aastore
    //   126: invokevirtual 660	android/content/ContentProviderOperation$Builder:withSelection	(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;
    //   129: pop
    //   130: aload 8
    //   132: aload 9
    //   134: invokevirtual 647	android/content/ContentProviderOperation$Builder:build	()Landroid/content/ContentProviderOperation;
    //   137: invokevirtual 648	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   140: pop
    //   141: aload_0
    //   142: aload 8
    //   144: invokespecial 331	com/truecaller/messaging/data/v:a	(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    //   147: astore 8
    //   149: aload 8
    //   151: arraylength
    //   152: ifeq +67 -> 219
    //   155: aload 8
    //   157: iconst_0
    //   158: aaload
    //   159: getfield 1169	android/content/ContentProviderResult:count	Ljava/lang/Integer;
    //   162: invokevirtual 790	java/lang/Integer:intValue	()I
    //   165: ifne +6 -> 171
    //   168: goto +51 -> 219
    //   171: aload 7
    //   173: invokeinterface 543 1 0
    //   178: astore 7
    //   180: aload 7
    //   182: invokeinterface 214 1 0
    //   187: ifeq +44 -> 231
    //   190: aload 7
    //   192: invokeinterface 218 1 0
    //   197: checkcast 111	java/io/File
    //   200: astore 8
    //   202: aload 8
    //   204: invokevirtual 546	java/io/File:exists	()Z
    //   207: ifeq -27 -> 180
    //   210: aload 8
    //   212: invokevirtual 549	java/io/File:delete	()Z
    //   215: pop
    //   216: goto -36 -> 180
    //   219: ldc_w 1171
    //   222: lload 5
    //   224: invokestatic 268	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   227: invokevirtual 428	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   230: pop
    //   231: aload_1
    //   232: invokevirtual 1174	com/truecaller/messaging/data/types/Draft:c	()Lcom/truecaller/messaging/data/types/Draft$a;
    //   235: astore_1
    //   236: aload_1
    //   237: ldc2_w 344
    //   240: putfield 1177	com/truecaller/messaging/data/types/Draft$a:a	J
    //   243: aload_1
    //   244: invokevirtual 1180	com/truecaller/messaging/data/types/Draft$a:d	()Lcom/truecaller/messaging/data/types/Draft;
    //   247: astore 7
    //   249: aload 7
    //   251: invokestatic 1114	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   254: areturn
    //   255: aload_0
    //   256: aload_1
    //   257: ldc_w 1182
    //   260: invokevirtual 1185	com/truecaller/messaging/data/types/Draft:a	(Ljava/lang/String;)Lcom/truecaller/messaging/data/types/Message;
    //   263: aload_1
    //   264: getfield 1155	com/truecaller/messaging/data/types/Draft:d	[Lcom/truecaller/messaging/data/types/Participant;
    //   267: invokespecial 1187	com/truecaller/messaging/data/v:a	(Lcom/truecaller/messaging/data/types/Message;[Lcom/truecaller/messaging/data/types/Participant;)Lcom/truecaller/messaging/data/types/Message;
    //   270: astore 10
    //   272: aload 10
    //   274: ifnonnull +10 -> 284
    //   277: aload_1
    //   278: invokestatic 1114	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   281: astore_1
    //   282: aload_1
    //   283: areturn
    //   284: aconst_null
    //   285: astore 7
    //   287: aconst_null
    //   288: astore 9
    //   290: aload_0
    //   291: getfield 128	com/truecaller/messaging/data/v:l	Lcom/truecaller/messaging/data/c;
    //   294: aload_0
    //   295: getfield 109	com/truecaller/messaging/data/v:c	Landroid/content/ContentResolver;
    //   298: aload 10
    //   300: getfield 813	com/truecaller/messaging/data/types/Message:b	J
    //   303: invokestatic 1190	com/truecaller/content/TruecallerContract$g:a	(J)Landroid/net/Uri;
    //   306: aconst_null
    //   307: aconst_null
    //   308: aconst_null
    //   309: aconst_null
    //   310: invokevirtual 276	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   313: invokeinterface 1193 2 0
    //   318: astore 8
    //   320: aload 9
    //   322: astore 7
    //   324: aload 8
    //   326: ifnull +37 -> 363
    //   329: aload 9
    //   331: astore 7
    //   333: aload 8
    //   335: invokeinterface 1198 1 0
    //   340: ifeq +23 -> 363
    //   343: aload 8
    //   345: invokeinterface 1200 1 0
    //   350: astore 7
    //   352: goto +11 -> 363
    //   355: astore 7
    //   357: aload 8
    //   359: astore_1
    //   360: goto +148 -> 508
    //   363: aload 8
    //   365: invokestatic 289	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   368: new 1176	com/truecaller/messaging/data/types/Draft$a
    //   371: dup
    //   372: invokespecial 1201	com/truecaller/messaging/data/types/Draft$a:<init>	()V
    //   375: astore 8
    //   377: aload 8
    //   379: aload 10
    //   381: getfield 502	com/truecaller/messaging/data/types/Message:a	J
    //   384: putfield 1177	com/truecaller/messaging/data/types/Draft$a:a	J
    //   387: aload 8
    //   389: aload_1
    //   390: getfield 1155	com/truecaller/messaging/data/types/Draft:d	[Lcom/truecaller/messaging/data/types/Participant;
    //   393: invokevirtual 1204	com/truecaller/messaging/data/types/Draft$a:a	([Lcom/truecaller/messaging/data/types/Participant;)Lcom/truecaller/messaging/data/types/Draft$a;
    //   396: pop
    //   397: aload 8
    //   399: aload_1
    //   400: getfield 1206	com/truecaller/messaging/data/types/Draft:h	J
    //   403: putfield 1208	com/truecaller/messaging/data/types/Draft$a:g	J
    //   406: aload 8
    //   408: aload_1
    //   409: getfield 1211	com/truecaller/messaging/data/types/Draft:i	Lcom/truecaller/messaging/data/types/ReplySnippet;
    //   412: putfield 1213	com/truecaller/messaging/data/types/Draft$a:h	Lcom/truecaller/messaging/data/types/ReplySnippet;
    //   415: aload 10
    //   417: getfield 508	com/truecaller/messaging/data/types/Message:n	[Lcom/truecaller/messaging/data/types/Entity;
    //   420: astore_1
    //   421: aload_1
    //   422: arraylength
    //   423: istore_3
    //   424: iconst_0
    //   425: istore_2
    //   426: iload_2
    //   427: iload_3
    //   428: if_icmpge +46 -> 474
    //   431: aload_1
    //   432: iload_2
    //   433: aaload
    //   434: astore 9
    //   436: aload 9
    //   438: invokevirtual 524	com/truecaller/messaging/data/types/Entity:a	()Z
    //   441: ifeq +19 -> 460
    //   444: aload 8
    //   446: aload 9
    //   448: checkcast 674	com/truecaller/messaging/data/types/TextEntity
    //   451: getfield 676	com/truecaller/messaging/data/types/TextEntity:a	Ljava/lang/String;
    //   454: putfield 1215	com/truecaller/messaging/data/types/Draft$a:d	Ljava/lang/String;
    //   457: goto +66 -> 523
    //   460: aload 8
    //   462: aload 9
    //   464: checkcast 526	com/truecaller/messaging/data/types/BinaryEntity
    //   467: invokevirtual 1218	com/truecaller/messaging/data/types/Draft$a:a	(Lcom/truecaller/messaging/data/types/BinaryEntity;)Lcom/truecaller/messaging/data/types/Draft$a;
    //   470: pop
    //   471: goto +52 -> 523
    //   474: aload 7
    //   476: ifnull +10 -> 486
    //   479: aload 8
    //   481: aload 7
    //   483: putfield 1221	com/truecaller/messaging/data/types/Draft$a:b	Lcom/truecaller/messaging/data/types/Conversation;
    //   486: aload 8
    //   488: invokevirtual 1180	com/truecaller/messaging/data/types/Draft$a:d	()Lcom/truecaller/messaging/data/types/Draft;
    //   491: astore_1
    //   492: ldc_w 1223
    //   495: aload_1
    //   496: invokestatic 836	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   499: invokevirtual 428	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   502: pop
    //   503: aload_1
    //   504: invokestatic 1114	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   507: areturn
    //   508: aload_1
    //   509: invokestatic 289	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   512: aload 7
    //   514: athrow
    //   515: astore_1
    //   516: aload_1
    //   517: athrow
    //   518: iconst_0
    //   519: istore_2
    //   520: goto -449 -> 71
    //   523: iload_2
    //   524: iconst_1
    //   525: iadd
    //   526: istore_2
    //   527: goto -101 -> 426
    //   530: astore 8
    //   532: aload 7
    //   534: astore_1
    //   535: aload 8
    //   537: astore 7
    //   539: goto -31 -> 508
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	542	0	this	v
    //   0	542	1	paramDraft	Draft
    //   67	460	2	i1	int
    //   423	6	3	i2	int
    //   20	9	4	bool	boolean
    //   82	141	5	l1	long
    //   72	279	7	localObject1	Object
    //   355	178	7	localConversation	Conversation
    //   537	1	7	localObject2	Object
    //   91	396	8	localObject3	Object
    //   530	6	8	localObject4	Object
    //   107	356	9	localBuilder	ContentProviderOperation.Builder
    //   270	146	10	localMessage	Message
    // Exception table:
    //   from	to	target	type
    //   333	352	355	finally
    //   48	66	515	finally
    //   78	168	515	finally
    //   171	180	515	finally
    //   180	216	515	finally
    //   219	231	515	finally
    //   231	249	515	finally
    //   249	255	515	finally
    //   255	272	515	finally
    //   277	282	515	finally
    //   363	424	515	finally
    //   436	457	515	finally
    //   460	471	515	finally
    //   479	486	515	finally
    //   486	508	515	finally
    //   508	515	515	finally
    //   290	320	530	finally
  }
  
  public final com.truecaller.androidactors.w<Message> a(Message paramMessage, int paramInt, String paramString)
  {
    List localList = a(n, true);
    paramMessage = paramMessage.m();
    a = -1L;
    paramMessage = paramMessage.a().a(localList);
    f = 3;
    k = paramInt;
    paramMessage = paramMessage.a(3, NullTransportInfo.b);
    q = UUID.randomUUID().toString();
    paramMessage = paramMessage.a(paramString);
    e = org.a.a.b.ay_();
    d = org.a.a.b.ay_();
    return com.truecaller.androidactors.w.b(paramMessage.b());
  }
  
  public final com.truecaller.androidactors.w<Boolean> a(Message paramMessage, long paramLong)
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(ContentProviderOperation.newUpdate(TruecallerContract.aa.a(a)).withValue("date_sent", Long.valueOf(paramLong)).withValue("date", Long.valueOf(paramLong)).build());
    boolean bool;
    if (a(localArrayList).length > 0) {
      bool = true;
    } else {
      bool = false;
    }
    return com.truecaller.androidactors.w.b(Boolean.valueOf(bool));
  }
  
  public final com.truecaller.androidactors.w<Message> a(Message paramMessage, long paramLong, boolean paramBoolean)
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
  }
  
  public final com.truecaller.androidactors.w<Message> a(Message paramMessage, Participant[] paramArrayOfParticipant, int paramInt)
  {
    int i1 = j;
    boolean bool2 = true;
    boolean bool1;
    if (i1 == 3) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    AssertionUtil.isTrue(bool1, new String[0]);
    if (f == 3) {
      bool1 = bool2;
    } else {
      bool1 = false;
    }
    AssertionUtil.isTrue(bool1, new String[0]);
    if (!paramMessage.c())
    {
      AssertionUtil.OnlyInDebug.fail(new String[] { "Can not schedule empty message for sending" });
      return com.truecaller.androidactors.w.b(null);
    }
    paramArrayOfParticipant = a(paramMessage, paramArrayOfParticipant);
    if (paramArrayOfParticipant == null) {
      return com.truecaller.androidactors.w.b(null);
    }
    l locall = ((m)f.get()).a(paramInt);
    ArrayList localArrayList = new ArrayList();
    ContentProviderOperation.Builder localBuilder = ContentProviderOperation.newAssertQuery(TruecallerContract.aa.a(a));
    localBuilder.withValue("status", Integer.valueOf(3));
    localArrayList.add(localBuilder.build());
    localBuilder = ContentProviderOperation.newUpdate(TruecallerContract.aa.a(a));
    localBuilder.withValue("status", Integer.valueOf(17));
    localBuilder.withValue("scheduled_transport", Integer.valueOf(paramInt));
    localBuilder.withValue("date_sent", Long.valueOf(locall.a(d.a)));
    localBuilder.withValue("date", Long.valueOf(locall.a(e.a)));
    localBuilder.withValue("sim_token", locall.a(l));
    localBuilder.withValue("category", Integer.valueOf(q));
    localBuilder.withValue("seen", Boolean.FALSE);
    localBuilder.withValue("read", Boolean.FALSE);
    localBuilder.withValue("analytics_id", o);
    localArrayList.add(localBuilder.build());
    if (a(localArrayList).length == 0) {
      return com.truecaller.androidactors.w.b(null);
    }
    return u.b(a);
  }
  
  public final com.truecaller.androidactors.w<Long> a(String paramString)
  {
    return u.b(paramString);
  }
  
  /* Error */
  public final com.truecaller.androidactors.w<Boolean> a(org.a.a.b paramb)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 8
    //   3: aconst_null
    //   4: astore 7
    //   6: aload_1
    //   7: ifnonnull +13 -> 20
    //   10: ldc_w 1295
    //   13: astore_1
    //   14: aconst_null
    //   15: astore 6
    //   17: goto +41 -> 58
    //   20: aload 7
    //   22: astore 4
    //   24: aload 8
    //   26: astore 5
    //   28: aload_1
    //   29: getfield 239	org/a/a/a/g:a	J
    //   32: lstore_2
    //   33: ldc_w 1297
    //   36: astore_1
    //   37: aload 7
    //   39: astore 4
    //   41: aload 8
    //   43: astore 5
    //   45: iconst_1
    //   46: anewarray 261	java/lang/String
    //   49: dup
    //   50: iconst_0
    //   51: lload_2
    //   52: invokestatic 268	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   55: aastore
    //   56: astore 6
    //   58: aload 7
    //   60: astore 4
    //   62: aload 8
    //   64: astore 5
    //   66: aload_0
    //   67: getfield 128	com/truecaller/messaging/data/v:l	Lcom/truecaller/messaging/data/c;
    //   70: aload_0
    //   71: getfield 109	com/truecaller/messaging/data/v:c	Landroid/content/ContentResolver;
    //   74: invokestatic 1300	com/truecaller/content/TruecallerContract$ab:a	()Landroid/net/Uri;
    //   77: aconst_null
    //   78: aload_1
    //   79: aload 6
    //   81: ldc_w 1302
    //   84: invokevirtual 276	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   87: invokeinterface 1305 2 0
    //   92: astore_1
    //   93: aload_1
    //   94: ifnonnull +24 -> 118
    //   97: aload_1
    //   98: astore 4
    //   100: aload_1
    //   101: astore 5
    //   103: getstatic 1285	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   106: invokestatic 1114	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   109: astore 6
    //   111: aload_1
    //   112: invokestatic 289	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   115: aload 6
    //   117: areturn
    //   118: aload_1
    //   119: astore 4
    //   121: aload_1
    //   122: astore 5
    //   124: invokestatic 445	org/a/a/b:ay_	()Lorg/a/a/b;
    //   127: astore 6
    //   129: aload_1
    //   130: astore 4
    //   132: aload_1
    //   133: astore 5
    //   135: new 175	java/util/ArrayList
    //   138: dup
    //   139: invokespecial 176	java/util/ArrayList:<init>	()V
    //   142: astore 8
    //   144: aload_1
    //   145: astore 4
    //   147: aload_1
    //   148: astore 5
    //   150: new 178	java/util/HashSet
    //   153: dup
    //   154: invokespecial 179	java/util/HashSet:<init>	()V
    //   157: astore 9
    //   159: aload_1
    //   160: astore 4
    //   162: aload_1
    //   163: astore 5
    //   165: aload_1
    //   166: invokeinterface 1308 1 0
    //   171: ifeq +97 -> 268
    //   174: aload_1
    //   175: astore 4
    //   177: aload_1
    //   178: astore 5
    //   180: aload_1
    //   181: invokeinterface 1309 1 0
    //   186: astore 10
    //   188: aload_1
    //   189: astore 4
    //   191: aload_1
    //   192: astore 5
    //   194: aload_0
    //   195: aload 10
    //   197: aload 10
    //   199: getfield 875	com/truecaller/messaging/data/types/Message:k	I
    //   202: aload 8
    //   204: invokespecial 1311	com/truecaller/messaging/data/v:a	(Lcom/truecaller/messaging/data/types/Message;ILjava/util/List;)Lorg/a/a/b;
    //   207: astore 7
    //   209: aload_1
    //   210: astore 4
    //   212: aload_1
    //   213: astore 5
    //   215: aload 9
    //   217: aload_0
    //   218: getfield 124	com/truecaller/messaging/data/v:f	Ldagger/a;
    //   221: invokeinterface 417 1 0
    //   226: checkcast 419	com/truecaller/messaging/transport/m
    //   229: aload 10
    //   231: getfield 875	com/truecaller/messaging/data/types/Message:k	I
    //   234: invokeinterface 792 2 0
    //   239: invokeinterface 493 2 0
    //   244: pop
    //   245: aload_1
    //   246: astore 4
    //   248: aload_1
    //   249: astore 5
    //   251: aload 7
    //   253: aload 6
    //   255: invokevirtual 378	org/a/a/b:c	(Lorg/a/a/x;)Z
    //   258: ifeq -99 -> 159
    //   261: aload 7
    //   263: astore 6
    //   265: goto -106 -> 159
    //   268: aload_1
    //   269: astore 4
    //   271: aload_1
    //   272: astore 5
    //   274: aload 8
    //   276: invokevirtual 334	java/util/ArrayList:isEmpty	()Z
    //   279: ifeq +24 -> 303
    //   282: aload_1
    //   283: astore 4
    //   285: aload_1
    //   286: astore 5
    //   288: getstatic 1314	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   291: invokestatic 1114	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   294: astore 6
    //   296: aload_1
    //   297: invokestatic 289	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   300: aload 6
    //   302: areturn
    //   303: aload_1
    //   304: astore 4
    //   306: aload_1
    //   307: astore 5
    //   309: aload_0
    //   310: aload 8
    //   312: invokespecial 331	com/truecaller/messaging/data/v:a	(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    //   315: arraylength
    //   316: ifne +24 -> 340
    //   319: aload_1
    //   320: astore 4
    //   322: aload_1
    //   323: astore 5
    //   325: getstatic 1285	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   328: invokestatic 1114	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   331: astore 6
    //   333: aload_1
    //   334: invokestatic 289	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   337: aload 6
    //   339: areturn
    //   340: aload_1
    //   341: astore 4
    //   343: aload_1
    //   344: astore 5
    //   346: aload_0
    //   347: invokestatic 445	org/a/a/b:ay_	()Lorg/a/a/b;
    //   350: aload 6
    //   352: iconst_2
    //   353: aload 9
    //   355: invokespecial 1024	com/truecaller/messaging/data/v:a	(Lorg/a/a/b;Lorg/a/a/b;ILjava/lang/Iterable;)V
    //   358: aload_1
    //   359: astore 4
    //   361: aload_1
    //   362: astore 5
    //   364: getstatic 1314	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   367: invokestatic 1114	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   370: astore 6
    //   372: aload_1
    //   373: invokestatic 289	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   376: aload 6
    //   378: areturn
    //   379: astore_1
    //   380: goto +24 -> 404
    //   383: astore_1
    //   384: aload 5
    //   386: astore 4
    //   388: aload_1
    //   389: invokestatic 782	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   392: aload 5
    //   394: invokestatic 289	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   397: getstatic 1285	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   400: invokestatic 1114	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   403: areturn
    //   404: aload 4
    //   406: invokestatic 289	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   409: aload_1
    //   410: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	411	0	this	v
    //   0	411	1	paramb	org.a.a.b
    //   32	20	2	l1	long
    //   22	383	4	localObject1	Object
    //   26	367	5	localObject2	Object
    //   15	362	6	localObject3	Object
    //   4	258	7	localb	org.a.a.b
    //   1	310	8	localArrayList	ArrayList
    //   157	197	9	localHashSet	HashSet
    //   186	44	10	localMessage	Message
    // Exception table:
    //   from	to	target	type
    //   28	33	379	finally
    //   45	58	379	finally
    //   66	93	379	finally
    //   103	111	379	finally
    //   124	129	379	finally
    //   135	144	379	finally
    //   150	159	379	finally
    //   165	174	379	finally
    //   180	188	379	finally
    //   194	209	379	finally
    //   215	245	379	finally
    //   251	261	379	finally
    //   274	282	379	finally
    //   288	296	379	finally
    //   309	319	379	finally
    //   325	333	379	finally
    //   346	358	379	finally
    //   364	372	379	finally
    //   388	392	379	finally
    //   28	33	383	java/lang/RuntimeException
    //   45	58	383	java/lang/RuntimeException
    //   66	93	383	java/lang/RuntimeException
    //   103	111	383	java/lang/RuntimeException
    //   124	129	383	java/lang/RuntimeException
    //   135	144	383	java/lang/RuntimeException
    //   150	159	383	java/lang/RuntimeException
    //   165	174	383	java/lang/RuntimeException
    //   180	188	383	java/lang/RuntimeException
    //   194	209	383	java/lang/RuntimeException
    //   215	245	383	java/lang/RuntimeException
    //   251	261	383	java/lang/RuntimeException
    //   274	282	383	java/lang/RuntimeException
    //   288	296	383	java/lang/RuntimeException
    //   309	319	383	java/lang/RuntimeException
    //   325	333	383	java/lang/RuntimeException
    //   346	358	383	java/lang/RuntimeException
    //   364	372	383	java/lang/RuntimeException
  }
  
  public final com.truecaller.androidactors.w<SparseBooleanArray> a(boolean paramBoolean, long... paramVarArgs)
  {
    return com.truecaller.androidactors.w.b(b(d(paramVarArgs), paramBoolean));
  }
  
  public final com.truecaller.androidactors.w<SparseBooleanArray> a(long[] paramArrayOfLong)
  {
    StringBuilder localStringBuilder = new StringBuilder("conversation_id");
    localStringBuilder.append(" IN (");
    localStringBuilder.append(paramArrayOfLong[0]);
    int i1 = 1;
    while (i1 < paramArrayOfLong.length)
    {
      localStringBuilder.append(',');
      localStringBuilder.append(paramArrayOfLong[i1]);
      i1 += 1;
    }
    localStringBuilder.append(')');
    return com.truecaller.androidactors.w.b(a(localStringBuilder.toString(), false));
  }
  
  public final com.truecaller.androidactors.w<SparseBooleanArray> a(Conversation[] paramArrayOfConversation)
  {
    SparseBooleanArray localSparseBooleanArray = new SparseBooleanArray();
    a(paramArrayOfConversation, new -..Lambda.v.bXMuDjRgAMAvY_MYIfd8FzIwVZw(this, localSparseBooleanArray));
    return com.truecaller.androidactors.w.b(localSparseBooleanArray);
  }
  
  public final com.truecaller.androidactors.w<SparseBooleanArray> a(Conversation[] paramArrayOfConversation, boolean paramBoolean)
  {
    SparseBooleanArray localSparseBooleanArray = new SparseBooleanArray();
    a(paramArrayOfConversation, new -..Lambda.v.WCdhtL09afn-oOKkG6hWsnBLqKw(this, localSparseBooleanArray, paramBoolean));
    return com.truecaller.androidactors.w.b(localSparseBooleanArray);
  }
  
  public final void a()
  {
    Cursor localCursor = c.query(TruecallerContract.aa.a(), new String[] { "_id" }, "classification = 0 AND (status & 1) = 0", null, "_id LIMIT 1");
    if (localCursor != null) {
      try
      {
        if (localCursor.getCount() > 0)
        {
          HashSet localHashSet = new HashSet();
          localHashSet.add(Integer.valueOf(0));
          localHashSet.add(Integer.valueOf(4));
          a(false, localHashSet);
        }
        return;
      }
      finally
      {
        localCursor.close();
      }
    }
  }
  
  public final void a(int paramInt, org.a.a.b paramb)
  {
    j localj = l.b(c.query(TruecallerContract.ab.a(), null, "(transport = ? OR scheduled_transport = ?) AND (status & 1) != 0 AND (status & 36) != 0", new String[] { String.valueOf(paramInt), String.valueOf(paramInt) }, "date_sent LIMIT 1"));
    if (localj == null) {
      return;
    }
    try
    {
      boolean bool = localj.moveToFirst();
      if (!bool) {
        return;
      }
      Message localMessage = localj.b();
      paramInt = f;
      if ((paramInt & 0x20) != 0) {
        return;
      }
      if ((f & 0x40) != 0)
      {
        bool = u.b(paramb);
        if (bool) {
          return;
        }
      }
      paramb = new ContentValues();
      paramb.put("status", Integer.valueOf(33));
      paramInt = c.update(TruecallerContract.aa.a(a), paramb, null, null);
      if (paramInt == 0) {
        return;
      }
      ((com.truecaller.messaging.transport.d)g.get()).b(localMessage);
      return;
    }
    finally
    {
      localj.close();
    }
  }
  
  public final void a(int paramInt, org.a.a.b paramb, boolean paramBoolean)
  {
    ((m)f.get()).a(paramInt).a(paramb);
    b(paramBoolean, Collections.singleton(Integer.valueOf(paramInt)));
  }
  
  public final void a(long paramLong, int paramInt)
  {
    String str = "_id = ".concat(String.valueOf(paramLong));
    ContentValues localContentValues = new ContentValues(1);
    localContentValues.put("preferred_transport", Integer.valueOf(paramInt));
    c.update(TruecallerContract.i.a(), localContentValues, str, null);
  }
  
  public final void a(long paramLong, int paramInt1, int paramInt2)
  {
    StringBuilder localStringBuilder = new StringBuilder("conversation_id = ");
    localStringBuilder.append(paramLong);
    localStringBuilder.append(" ");
    localStringBuilder.append(com.truecaller.content.c.a(x, paramInt1, paramInt2));
    a(localStringBuilder.toString(), false);
  }
  
  public final void a(t.a parama, int paramInt, Iterable<l> paramIterable)
  {
    AssertionUtil.AlwaysFatal.isNotNull(s, new String[] { "You can't trigger sync without setting its bounds" });
    com.truecaller.utils.q localq = t.a("MessagesStorage.performBatch");
    int i1;
    int i2;
    try
    {
      if (!parama.a(s.b, s.c)) {
        break label1161;
      }
      paramInt |= s.a;
      i1 = a(parama, localq, paramIterable);
      i2 = 0;
      localMessage = null;
      localObject2 = null;
      switch (i1)
      {
      case 2: 
        s = null;
        return;
      }
    }
    finally
    {
      Message localMessage;
      Object localObject2;
      long l1;
      Object localObject1;
      Object localObject3;
      boolean bool;
      localq.a();
    }
    l1 = k.a();
    if (parama.d().c(l1))
    {
      if (d()) {
        break label1167;
      }
      i1 = 1;
      label167:
      k.a(da);
      if (i1 != 0) {
        o.j();
      }
    }
    if (parama.c().e(0L)) {
      y.b();
    }
    s = null;
    localObject1 = ((m)f.get()).a().iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject3 = (l)((Iterator)localObject1).next();
      if (((l)localObject3).e()) {
        a(((l)localObject3).f(), org.a.a.b.ay_());
      }
    }
    paramIterable = paramIterable.iterator();
    while (paramIterable.hasNext()) {
      if (((l)paramIterable.next()).f() == 5)
      {
        localObject1 = (com.truecaller.messaging.h.c)C.get();
        bool = parama.f();
        b.j(bool);
      }
    }
    break label1173;
    n.a(parama, paramInt, paramIterable);
    "Schedule next batch: ".concat(String.valueOf(parama));
    for (;;)
    {
      if (i1 != 0)
      {
        m localm = (m)f.get();
        localObject1 = localm.b();
        bool = ((List)localObject1).isEmpty();
        if (!bool)
        {
          parama = localMessage;
          try
          {
            ContentResolver localContentResolver = c;
            parama = localMessage;
            Uri localUri = TruecallerContract.ab.a();
            paramIterable = "seen=0";
            parama = localMessage;
            if (x.w().a())
            {
              parama = localMessage;
              paramIterable = new StringBuilder();
              parama = localMessage;
              paramIterable.append("seen=0");
              parama = localMessage;
              paramIterable.append(" AND category!=3");
              parama = localMessage;
              paramIterable = paramIterable.toString();
            }
            parama = localMessage;
            l1 = k.c();
            if (l1 > 0L)
            {
              parama = localMessage;
              ArrayList localArrayList = new ArrayList();
              parama = localMessage;
              Iterator localIterator = ((List)localObject1).iterator();
              for (;;)
              {
                parama = localMessage;
                if (!localIterator.hasNext()) {
                  break;
                }
                parama = localMessage;
                Integer localInteger = (Integer)localIterator.next();
                parama = localMessage;
                localObject3 = " (transport=".concat(String.valueOf(localInteger));
                localObject1 = localObject3;
                parama = localMessage;
                if (localInteger.intValue() != 2)
                {
                  parama = localMessage;
                  localObject1 = new StringBuilder();
                  parama = localMessage;
                  ((StringBuilder)localObject1).append((String)localObject3);
                  parama = localMessage;
                  ((StringBuilder)localObject1).append(" AND date> ");
                  parama = localMessage;
                  ((StringBuilder)localObject1).append(l1);
                  parama = localMessage;
                  localObject1 = ((StringBuilder)localObject1).toString();
                }
                parama = localMessage;
                localObject3 = new StringBuilder();
                parama = localMessage;
                ((StringBuilder)localObject3).append((String)localObject1);
                parama = localMessage;
                ((StringBuilder)localObject3).append(" )");
                parama = localMessage;
                localArrayList.add(((StringBuilder)localObject3).toString());
              }
              parama = localMessage;
              localObject1 = new StringBuilder();
              parama = localMessage;
              ((StringBuilder)localObject1).append(paramIterable);
              parama = localMessage;
              ((StringBuilder)localObject1).append(" AND (");
              parama = localMessage;
              ((StringBuilder)localObject1).append(k.a(localArrayList, " OR "));
              parama = localMessage;
              ((StringBuilder)localObject1).append(")");
              parama = localMessage;
              paramIterable = ((StringBuilder)localObject1).toString();
            }
            else
            {
              parama = localMessage;
              localObject3 = new StringBuilder();
              parama = localMessage;
              ((StringBuilder)localObject3).append(paramIterable);
              parama = localMessage;
              ((StringBuilder)localObject3).append(" AND transport IN (");
              parama = localMessage;
              ((StringBuilder)localObject3).append(k.a((Iterable)localObject1, ","));
              parama = localMessage;
              ((StringBuilder)localObject3).append(")");
              parama = localMessage;
              paramIterable = ((StringBuilder)localObject3).toString();
            }
            parama = localMessage;
            paramIterable = localContentResolver.query(localUri, null, paramIterable, null, "date DESC LIMIT 25");
            if (paramIterable == null) {
              localObject1 = localObject2;
            }
            for (;;)
            {
              com.truecaller.util.q.a((Cursor)localObject1);
              break;
              parama = localMessage;
              localObject1 = new ArrayList();
              parama = localMessage;
              paramIterable = l.b(paramIterable);
              for (;;)
              {
                parama = paramIterable;
                if (!paramIterable.moveToNext()) {
                  break;
                }
                parama = paramIterable;
                localMessage = paramIterable.b();
                parama = paramIterable;
                if ((f & 0x1) == 0)
                {
                  parama = paramIterable;
                  if (localm.c(localMessage) != 2)
                  {
                    parama = paramIterable;
                    ((List)localObject1).add(localMessage);
                  }
                }
                else
                {
                  parama = paramIterable;
                  if ((f & 0x8) == 8)
                  {
                    parama = paramIterable;
                    b(a);
                    parama = paramIterable;
                    ((com.truecaller.messaging.notifications.a)j.a()).b(localMessage);
                  }
                }
              }
              parama = paramIterable;
              ((com.truecaller.messaging.notifications.a)j.a()).a(D.a((List)localObject1));
              localObject1 = paramIterable;
              parama = paramIterable;
              if (!B.e())
              {
                parama = paramIterable;
                B.d();
                localObject1 = paramIterable;
              }
            }
            ((as)v.a()).a();
          }
          finally
          {
            com.truecaller.util.q.a(parama);
          }
        }
      }
      localq.a();
      return;
      label1161:
      break;
      break label1173;
      label1167:
      i1 = 0;
      break label167;
      label1173:
      i1 = i2;
      if ((paramInt & 0x1) == 1) {
        i1 = 1;
      }
    }
  }
  
  public final void a(Message paramMessage)
  {
    f(paramMessage.m().c(System.currentTimeMillis()).b());
  }
  
  public final void a(Message paramMessage, String paramString1, String paramString2)
  {
    l locall = ((m)f.get()).b(j);
    if (locall == null)
    {
      paramString1 = new StringBuilder("Unknown transport: ");
      paramString1.append(j);
      paramString1.toString();
      return;
    }
    locall.a(paramMessage, paramString1, paramString2);
  }
  
  public final void a(boolean paramBoolean)
  {
    a(paramBoolean, ((m)f.get()).a());
  }
  
  public final void a(boolean paramBoolean, Set<Integer> paramSet)
  {
    a(paramBoolean, a(paramSet));
  }
  
  public final void a(Message[] paramArrayOfMessage, int paramInt)
  {
    ArrayList localArrayList = new ArrayList();
    int i1;
    if (paramInt == 1) {
      i1 = 3;
    } else {
      i1 = 2;
    }
    int i3 = paramArrayOfMessage.length;
    int i2 = 0;
    while (i2 < i3)
    {
      Message localMessage = paramArrayOfMessage[i2];
      ContentProviderOperation.Builder localBuilder = ContentProviderOperation.newUpdate(TruecallerContract.aa.a(a));
      localBuilder.withValue("category", Integer.valueOf(i1));
      localBuilder.withValue("classification", Integer.valueOf(1));
      localArrayList.add(localBuilder.build());
      z.a(localMessage, q, paramInt);
      i2 += 1;
    }
    if (a(localArrayList).length == 0) {}
  }
  
  public final com.truecaller.androidactors.w<Conversation> b(org.a.a.b paramb)
  {
    return u.a(paramb);
  }
  
  public final void b()
  {
    Cursor localCursor = c.query(TruecallerContract.aa.a(), new String[] { "_id", "read", "seen" }, "sync_status = 1", null, null);
    if (localCursor != null) {}
    for (;;)
    {
      try
      {
        ArrayList localArrayList1 = new ArrayList();
        ArrayList localArrayList2 = new ArrayList();
        if (localCursor.moveToNext())
        {
          int i2 = 0;
          long l1 = localCursor.getLong(0);
          if (localCursor.getInt(1) != 1) {
            break label219;
          }
          i1 = 1;
          if (localCursor.getInt(2) == 1) {
            i2 = 1;
          }
          if (i1 != 0)
          {
            localArrayList1.add(Long.valueOf(l1));
            continue;
          }
          if (i2 == 0) {
            continue;
          }
          localArrayList2.add(Long.valueOf(l1));
          continue;
        }
        if (!localArrayList1.isEmpty()) {
          a(d(c(localArrayList1)), true);
        }
        if (!localArrayList2.isEmpty()) {
          b(d(c(localArrayList2)));
        }
        return;
      }
      finally
      {
        localCursor.close();
      }
      return;
      label219:
      int i1 = 0;
    }
  }
  
  public final void b(int paramInt)
  {
    l locall = ((m)f.get()).b(paramInt);
    if (locall == null) {
      return;
    }
    j localj = l.b(c.query(TruecallerContract.ab.a(), null, "transport = ? AND (status & 33) = 33", new String[] { String.valueOf(paramInt) }, null));
    if (localj == null) {
      return;
    }
    ad localad = locall.b();
    org.a.a.b localb = a;
    try
    {
      while (localj.moveToNext())
      {
        Message localMessage = localj.b();
        locall.a(localMessage, localad);
        if (e.c(localb)) {
          localb = e;
        }
      }
      localj.close();
      if (locall.b(localad))
      {
        if (!locall.a(localad)) {
          return;
        }
        a(paramInt, localb, false);
        return;
      }
      return;
    }
    finally
    {
      localj.close();
    }
  }
  
  public final void b(long paramLong)
  {
    b("message_id=".concat(String.valueOf(paramLong)));
  }
  
  public final void b(Message paramMessage)
  {
    f(paramMessage);
  }
  
  public final void b(boolean paramBoolean)
  {
    b(paramBoolean, ((m)f.get()).a());
  }
  
  public final void b(boolean paramBoolean, Set<Integer> paramSet)
  {
    b(paramBoolean, a(paramSet));
  }
  
  public final void b(long[] paramArrayOfLong)
  {
    b(d(paramArrayOfLong));
  }
  
  public final com.truecaller.androidactors.w<SparseBooleanArray> c(long paramLong)
  {
    return com.truecaller.androidactors.w.b(b("message_id=".concat(String.valueOf(paramLong)), false));
  }
  
  public final com.truecaller.androidactors.w<Boolean> c(Message paramMessage)
  {
    AssertionUtil.AlwaysFatal.isTrue(paramMessage.b(), new String[] { "You can re-schedule only saved messages" });
    boolean bool;
    if ((f & 0x8) != 0) {
      bool = true;
    } else {
      bool = false;
    }
    AssertionUtil.AlwaysFatal.isTrue(bool, new String[] { "You can re-schedule only failed messages" });
    if ((f & 0x1) != 0) {
      bool = true;
    } else {
      bool = false;
    }
    AssertionUtil.AlwaysFatal.isTrue(bool, new String[] { "You can re-schedule only outgoing messages" });
    Object localObject1 = null;
    try
    {
      paramMessage = l.b(c.query(TruecallerContract.ab.a(), null, "_id=?", new String[] { String.valueOf(a) }, null));
      if (paramMessage != null)
      {
        localObject1 = paramMessage;
        if (paramMessage.moveToNext())
        {
          localObject1 = paramMessage;
          localObject2 = paramMessage.b();
          com.truecaller.util.q.a(paramMessage);
          if ((f & 0x9) != 9) {
            return com.truecaller.androidactors.w.b(Boolean.FALSE);
          }
          int i2 = j;
          int i1 = i2;
          if (i2 == 3) {
            i1 = k;
          }
          paramMessage = new ArrayList();
          localObject1 = a((Message)localObject2, i1, paramMessage);
          if (paramMessage.isEmpty()) {
            return com.truecaller.androidactors.w.b(Boolean.TRUE);
          }
          if (a(paramMessage).length == 0) {
            return com.truecaller.androidactors.w.b(Boolean.FALSE);
          }
          a(org.a.a.b.ay_(), (org.a.a.b)localObject1, 2, Collections.singleton(((m)f.get()).a(i1)));
          return com.truecaller.androidactors.w.b(Boolean.TRUE);
        }
      }
      localObject1 = paramMessage;
      Object localObject2 = com.truecaller.androidactors.w.b(Boolean.FALSE);
      com.truecaller.util.q.a(paramMessage);
      return (com.truecaller.androidactors.w<Boolean>)localObject2;
    }
    finally
    {
      com.truecaller.util.q.a((Cursor)localObject1);
    }
  }
  
  public final void c(int paramInt)
  {
    ArrayList localArrayList = new ArrayList();
    Cursor localCursor = c.query(TruecallerContract.aa.a(), new String[] { "_id", "date" }, "scheduled_transport =? AND (status & 17) = 17", new String[] { String.valueOf(paramInt) }, null);
    if (localCursor == null) {
      return;
    }
    Object localObject1 = a;
    try
    {
      while (localCursor.moveToNext())
      {
        long l1 = localCursor.getLong(0);
        org.a.a.b localb = new org.a.a.b(localCursor.getLong(1));
        ContentProviderOperation.Builder localBuilder = ContentProviderOperation.newUpdate(TruecallerContract.aa.a());
        localBuilder.withValue("status", Integer.valueOf(9));
        localBuilder.withValue("seen", Integer.valueOf(1));
        localBuilder.withSelection("_id=? ", new String[] { String.valueOf(l1) });
        localArrayList.add(localBuilder.build());
        boolean bool = localb.c((org.a.a.x)localObject1);
        if (bool) {
          localObject1 = localb;
        }
      }
      localCursor.close();
      if (a(localArrayList).length != 0) {
        a(paramInt, (org.a.a.b)localObject1, false);
      }
      return;
    }
    finally
    {
      localCursor.close();
    }
  }
  
  public final void c(long... paramVarArgs)
  {
    a(d(paramVarArgs), false);
  }
  
  public final com.truecaller.androidactors.w<Draft> d(Message paramMessage)
  {
    AssertionUtil.AlwaysFatal.isTrue(paramMessage.b(), new String[] { "You can update status only for stored messages" });
    int i2 = j;
    int i1 = 0;
    boolean bool;
    if (i2 == 3) {
      bool = true;
    } else {
      bool = false;
    }
    AssertionUtil.AlwaysFatal.isTrue(bool, new String[] { "You can't cancel failed message" });
    Object localObject1 = new ArrayList();
    Object localObject2 = ContentProviderOperation.newDelete(TruecallerContract.aa.a());
    ((ContentProviderOperation.Builder)localObject2).withSelection("conversation_id=? AND (status & 2) == 2", new String[] { String.valueOf(b) });
    ((ArrayList)localObject1).add(((ContentProviderOperation.Builder)localObject2).build());
    localObject2 = ContentProviderOperation.newUpdate(TruecallerContract.aa.a());
    ((ContentProviderOperation.Builder)localObject2).withValue("status", Integer.valueOf(3));
    ((ContentProviderOperation.Builder)localObject2).withSelection("_id=? AND status = 17", new String[] { String.valueOf(a) });
    ((ArrayList)localObject1).add(((ContentProviderOperation.Builder)localObject2).build());
    localObject1 = a((ArrayList)localObject1);
    if ((localObject1.length != 0) && (1count.intValue() != 0)) {}
    try
    {
      localObject1 = (Conversation)u.a(b).d();
    }
    catch (InterruptedException localInterruptedException)
    {
      Draft.a locala;
      for (;;) {}
    }
    localObject1 = null;
    if (localObject1 == null) {
      return com.truecaller.androidactors.w.b(null);
    }
    localObject2 = new Draft.a();
    locala = ((Draft.a)localObject2).a(l);
    b = ((Conversation)localObject1);
    a = a;
    localObject1 = n;
    i2 = localObject1.length;
    while (i1 < i2)
    {
      locala = localObject1[i1];
      if (locala.a()) {
        d = a;
      } else {
        ((Draft.a)localObject2).a((BinaryEntity)locala);
      }
      i1 += 1;
    }
    r.a("Cancel", paramMessage, k);
    return com.truecaller.androidactors.w.b(((Draft.a)localObject2).d());
    return com.truecaller.androidactors.w.b(null);
  }
  
  public final void d(long paramLong)
  {
    ContentValues localContentValues = new ContentValues(1);
    localContentValues.put("actions_dismissed", Integer.valueOf(1));
    c.update(TruecallerContract.an.a(paramLong), localContentValues, null, null);
  }
  
  public final com.truecaller.androidactors.w<String> e(long paramLong)
  {
    return u.c(paramLong);
  }
  
  public final com.truecaller.androidactors.w<Message> e(Message paramMessage)
  {
    AssertionUtil.AlwaysFatal.isTrue(paramMessage.b(), new String[] { "You can update status only for stored messages" });
    boolean bool;
    if (m.c() != -1L) {
      bool = true;
    } else {
      bool = false;
    }
    AssertionUtil.AlwaysFatal.isTrue(bool, new String[] { "You can update status only for stored messages" });
    if ((f & 0x1) != 0) {
      bool = true;
    } else {
      bool = false;
    }
    AssertionUtil.AlwaysFatal.isTrue(bool, new String[0]);
    Object localObject = new StringBuilder("Update message status for ");
    ((StringBuilder)localObject).append(paramMessage);
    ((StringBuilder)localObject).append(" from: ");
    ((StringBuilder)localObject).append(f);
    ((StringBuilder)localObject).append(" to: 9 by transport: ");
    ((StringBuilder)localObject).append(j);
    ((StringBuilder)localObject).toString();
    try
    {
      localObject = ((m)f.get()).b(j);
      if (localObject == null)
      {
        localObject = new StringBuilder("Unknown transport: ");
        ((StringBuilder)localObject).append(k);
        ((StringBuilder)localObject).toString();
        return com.truecaller.androidactors.w.b(null);
      }
      ad localad = ((l)localObject).b();
      if (!((l)localObject).a(paramMessage, localad)) {
        return com.truecaller.androidactors.w.b(null);
      }
      if (!((l)localObject).a(localad)) {
        return com.truecaller.androidactors.w.b(null);
      }
      ((l)localObject).a(e);
      b(true, Collections.singleton(Integer.valueOf(((l)localObject).f())));
      localObject = new ContentValues();
      ((ContentValues)localObject).put("retry_count", Integer.valueOf(0));
      c.update(TruecallerContract.aa.a(a), (ContentValues)localObject, null, null);
      paramMessage = paramMessage.m();
      f = 9;
      u = 0;
      paramMessage = com.truecaller.androidactors.w.b(paramMessage.b());
      return paramMessage;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.v
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */