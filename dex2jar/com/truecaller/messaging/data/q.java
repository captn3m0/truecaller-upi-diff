package com.truecaller.messaging.data;

import android.content.ContentResolver;
import android.database.Cursor;
import android.database.MergeCursor;
import android.net.Uri;
import c.a.m;
import c.g.b.k;
import c.n;
import c.u;
import com.truecaller.androidactors.w;
import com.truecaller.content.TruecallerContract.aa;
import com.truecaller.content.TruecallerContract.ab;
import com.truecaller.content.TruecallerContract.g;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.data.a.a;
import com.truecaller.messaging.data.a.j;
import com.truecaller.messaging.data.types.Conversation;
import java.util.Collection;
import javax.inject.Inject;
import org.a.a.a.g;

public final class q
  implements o
{
  private final ContentResolver a;
  private final c b;
  private final e c;
  private final ab d;
  
  @Inject
  public q(ContentResolver paramContentResolver, c paramc, e parame, ab paramab)
  {
    a = paramContentResolver;
    b = paramc;
    c = parame;
    d = paramab;
  }
  
  private final Cursor b(int paramInt)
  {
    ContentResolver localContentResolver = a;
    Uri localUri = TruecallerContract.g.a(paramInt);
    String str = c(paramInt);
    return localContentResolver.query(localUri, new String[] { "COUNT()" }, str, null, null);
  }
  
  /* Error */
  private final long c()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 35	com/truecaller/messaging/data/q:a	Landroid/content/ContentResolver;
    //   4: astore_3
    //   5: iconst_3
    //   6: invokestatic 49	com/truecaller/content/TruecallerContract$g:a	(I)Landroid/net/Uri;
    //   9: astore 4
    //   11: aload_0
    //   12: iconst_3
    //   13: invokespecial 52	com/truecaller/messaging/data/q:c	(I)Ljava/lang/String;
    //   16: astore 5
    //   18: aload_3
    //   19: aload 4
    //   21: iconst_1
    //   22: anewarray 54	java/lang/String
    //   25: dup
    //   26: iconst_0
    //   27: ldc 67
    //   29: aastore
    //   30: aload 5
    //   32: aconst_null
    //   33: ldc 69
    //   35: invokevirtual 62	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   38: astore_3
    //   39: aload_3
    //   40: ifnull +105 -> 145
    //   43: aload_3
    //   44: checkcast 71	java/io/Closeable
    //   47: astore 5
    //   49: aconst_null
    //   50: astore 4
    //   52: aload 4
    //   54: astore_3
    //   55: aload 5
    //   57: checkcast 73	android/database/Cursor
    //   60: astore 6
    //   62: aload 4
    //   64: astore_3
    //   65: aload 6
    //   67: invokeinterface 77 1 0
    //   72: ifle +34 -> 106
    //   75: aload 4
    //   77: astore_3
    //   78: aload 6
    //   80: invokeinterface 81 1 0
    //   85: pop
    //   86: aload 4
    //   88: astore_3
    //   89: aload 6
    //   91: iconst_0
    //   92: invokeinterface 85 2 0
    //   97: lstore_1
    //   98: aload 5
    //   100: aconst_null
    //   101: invokestatic 90	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   104: lload_1
    //   105: lreturn
    //   106: aload 4
    //   108: astore_3
    //   109: getstatic 95	c/x:a	Lc/x;
    //   112: astore 4
    //   114: aload 5
    //   116: aconst_null
    //   117: invokestatic 90	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   120: goto +25 -> 145
    //   123: astore 4
    //   125: goto +11 -> 136
    //   128: astore 4
    //   130: aload 4
    //   132: astore_3
    //   133: aload 4
    //   135: athrow
    //   136: aload 5
    //   138: aload_3
    //   139: invokestatic 90	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   142: aload 4
    //   144: athrow
    //   145: lconst_0
    //   146: lreturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	147	0	this	q
    //   97	8	1	l	long
    //   4	135	3	localObject1	Object
    //   9	104	4	localObject2	Object
    //   123	1	4	localObject3	Object
    //   128	15	4	localThrowable	Throwable
    //   16	121	5	localObject4	Object
    //   60	30	6	localCursor	Cursor
    // Exception table:
    //   from	to	target	type
    //   55	62	123	finally
    //   65	75	123	finally
    //   78	86	123	finally
    //   89	98	123	finally
    //   109	114	123	finally
    //   133	136	123	finally
    //   55	62	128	java/lang/Throwable
    //   65	75	128	java/lang/Throwable
    //   78	86	128	java/lang/Throwable
    //   89	98	128	java/lang/Throwable
    //   109	114	128	java/lang/Throwable
  }
  
  private final String c(int paramInt)
  {
    StringBuilder localStringBuilder = new StringBuilder("(");
    localStringBuilder.append(d.a(paramInt));
    localStringBuilder.append(") AND (unread_messages_count > 0)");
    return localStringBuilder.toString();
  }
  
  public final w<Integer> a()
  {
    Object localObject1 = null;
    try
    {
      Cursor localCursor = a.query(TruecallerContract.aa.a(), new String[] { "COUNT(*)" }, null, null, null);
      if (localCursor != null)
      {
        localObject1 = localCursor;
        localCursor.moveToFirst();
        localObject1 = localCursor;
        localw = w.b(Integer.valueOf(localCursor.getInt(0)));
        localObject1 = localCursor;
        k.a(localw, "Promise.wrap(cursor.getInt(0))");
        com.truecaller.util.q.a(localCursor);
        return localw;
      }
      localObject1 = localCursor;
      w localw = w.b(Integer.valueOf(0));
      localObject1 = localCursor;
      k.a(localw, "Promise.wrap(0)");
      com.truecaller.util.q.a(localCursor);
      return localw;
    }
    finally
    {
      com.truecaller.util.q.a((Cursor)localObject1);
    }
  }
  
  public final w<a> a(int paramInt)
  {
    Object localObject = d.a(paramInt);
    localObject = a.query(TruecallerContract.g.a(paramInt), null, (String)localObject, null, "date DESC");
    if (localObject == null)
    {
      localObject = w.b(null);
      k.a(localObject, "Promise.wrap(null)");
      return (w<a>)localObject;
    }
    localObject = w.a(b.a((Cursor)localObject), (com.truecaller.androidactors.ab)q.f.a);
    k.a(localObject, "Promise.wrap<Conversatio…(cursor), { it.close() })");
    return (w<a>)localObject;
  }
  
  public final w<Conversation> a(long paramLong)
  {
    Object localObject1 = null;
    Object localObject4 = null;
    try
    {
      Object localObject2 = a.query(TruecallerContract.g.a(paramLong), null, null, null, null);
      if (localObject2 == null)
      {
        localObject2 = w.b(null);
        k.a(localObject2, "Promise.wrap(null)");
        com.truecaller.util.q.a(null);
        return (w<Conversation>)localObject2;
      }
      localObject2 = b.a((Cursor)localObject2);
      if (localObject2 == null) {}
      try
      {
        k.a();
        if (!((a)localObject2).moveToNext()) {
          localObject1 = localObject4;
        } else {
          localObject1 = ((a)localObject2).b();
        }
        localObject1 = w.b(localObject1);
        k.a(localObject1, "Promise.wrap(conversation)");
        com.truecaller.util.q.a((Cursor)localObject2);
        return (w<Conversation>)localObject1;
      }
      finally
      {
        localObject1 = localObject2;
        localObject2 = localObject5;
      }
      com.truecaller.util.q.a((Cursor)localObject1);
    }
    finally {}
    throw ((Throwable)localObject3);
  }
  
  public final w<j> a(long paramLong, int paramInt1, int paramInt2, Integer paramInteger)
  {
    ContentResolver localContentResolver = a;
    Uri localUri = TruecallerContract.ab.a(paramLong);
    Object localObject = new StringBuilder("(status & 2) = 0 ");
    ((StringBuilder)localObject).append(com.truecaller.content.c.a(c, paramInt1, paramInt2));
    String str = ((StringBuilder)localObject).toString();
    StringBuilder localStringBuilder = new StringBuilder("date DESC");
    if (paramInteger != null)
    {
      localObject = " LIMIT ".concat(String.valueOf(((Number)paramInteger).intValue()));
      paramInteger = (Integer)localObject;
      if (localObject != null) {}
    }
    else
    {
      paramInteger = "";
    }
    localStringBuilder.append(paramInteger);
    paramInteger = localContentResolver.query(localUri, null, str, null, localStringBuilder.toString());
    if (paramInteger == null)
    {
      paramInteger = w.b(null);
      k.a(paramInteger, "Promise.wrap(null)");
      return paramInteger;
    }
    paramInteger = w.a(b.b(paramInteger), (com.truecaller.androidactors.ab)q.d.a);
    k.a(paramInteger, "Promise.wrap<MessageCurs…r(cursor)) { it.close() }");
    return paramInteger;
  }
  
  public final w<a> a(Integer paramInteger)
  {
    if (paramInteger != null)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("date DESC");
      localStringBuilder.append("  LIMIT ");
      localStringBuilder.append(paramInteger);
      paramInteger = localStringBuilder.toString();
    }
    else
    {
      paramInteger = "date DESC";
    }
    paramInteger = a.query(TruecallerContract.g.a(), null, null, null, paramInteger);
    if (paramInteger == null)
    {
      paramInteger = w.b(null);
      k.a(paramInteger, "Promise.wrap(null)");
      return paramInteger;
    }
    paramInteger = w.a(b.a(paramInteger), (com.truecaller.androidactors.ab)q.a.a);
    k.a(paramInteger, "Promise.wrap<Conversatio…r(cursor)) { it.close() }");
    return paramInteger;
  }
  
  public final w<j> a(String paramString)
  {
    k.b(paramString, "uriFilter");
    paramString = a.query(TruecallerContract.ab.a(paramString), null, null, null, null);
    if (paramString == null)
    {
      paramString = w.b(null);
      k.a(paramString, "Promise.wrap(null)");
      return paramString;
    }
    paramString = w.a(b.b(paramString), (com.truecaller.androidactors.ab)q.e.a);
    k.a(paramString, "Promise.wrap<MessageCurs…r(cursor)) { it.close() }");
    return paramString;
  }
  
  /* Error */
  public final w<Boolean> a(java.util.List<Long> paramList)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 260
    //   4: invokestatic 24	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   7: new 97	java/lang/StringBuilder
    //   10: dup
    //   11: ldc_w 262
    //   14: invokespecial 102	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   17: astore_2
    //   18: aload_2
    //   19: aload_1
    //   20: checkcast 264	java/lang/Iterable
    //   23: ldc_w 266
    //   26: checkcast 268	java/lang/CharSequence
    //   29: aconst_null
    //   30: aconst_null
    //   31: iconst_0
    //   32: aconst_null
    //   33: aconst_null
    //   34: bipush 62
    //   36: invokestatic 273	c/a/m:a	(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lc/g/a/b;I)Ljava/lang/String;
    //   39: invokevirtual 110	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   42: pop
    //   43: aload_2
    //   44: ldc_w 275
    //   47: invokevirtual 110	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   50: pop
    //   51: aload_2
    //   52: invokevirtual 116	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   55: astore_1
    //   56: aload_0
    //   57: getfield 35	com/truecaller/messaging/data/q:a	Landroid/content/ContentResolver;
    //   60: invokestatic 122	com/truecaller/content/TruecallerContract$aa:a	()Landroid/net/Uri;
    //   63: aconst_null
    //   64: aload_1
    //   65: iconst_1
    //   66: anewarray 54	java/lang/String
    //   69: dup
    //   70: iconst_0
    //   71: ldc_w 277
    //   74: aastore
    //   75: ldc 69
    //   77: invokevirtual 62	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   80: astore_1
    //   81: aload_1
    //   82: ifnull +82 -> 164
    //   85: aload_1
    //   86: checkcast 71	java/io/Closeable
    //   89: astore_3
    //   90: aconst_null
    //   91: astore_2
    //   92: aload_2
    //   93: astore_1
    //   94: aload_3
    //   95: checkcast 73	android/database/Cursor
    //   98: invokeinterface 81 1 0
    //   103: ifeq +31 -> 134
    //   106: aload_2
    //   107: astore_1
    //   108: getstatic 283	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   111: invokestatic 139	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   114: astore 4
    //   116: aload_2
    //   117: astore_1
    //   118: aload 4
    //   120: ldc_w 285
    //   123: invokestatic 143	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   126: aload_3
    //   127: aconst_null
    //   128: invokestatic 90	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   131: aload 4
    //   133: areturn
    //   134: aload_2
    //   135: astore_1
    //   136: getstatic 95	c/x:a	Lc/x;
    //   139: astore_2
    //   140: aload_3
    //   141: aconst_null
    //   142: invokestatic 90	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   145: goto +19 -> 164
    //   148: astore_2
    //   149: goto +8 -> 157
    //   152: astore_2
    //   153: aload_2
    //   154: astore_1
    //   155: aload_2
    //   156: athrow
    //   157: aload_3
    //   158: aload_1
    //   159: invokestatic 90	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   162: aload_2
    //   163: athrow
    //   164: getstatic 288	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   167: invokestatic 139	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   170: astore_1
    //   171: aload_1
    //   172: ldc_w 290
    //   175: invokestatic 143	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   178: aload_1
    //   179: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	180	0	this	q
    //   0	180	1	paramList	java.util.List<Long>
    //   17	123	2	localObject1	Object
    //   148	1	2	localObject2	Object
    //   152	11	2	localThrowable	Throwable
    //   89	69	3	localCloseable	java.io.Closeable
    //   114	18	4	localw	w
    // Exception table:
    //   from	to	target	type
    //   94	106	148	finally
    //   108	116	148	finally
    //   118	126	148	finally
    //   136	140	148	finally
    //   155	157	148	finally
    //   94	106	152	java/lang/Throwable
    //   108	116	152	java/lang/Throwable
    //   118	126	152	java/lang/Throwable
    //   136	140	152	java/lang/Throwable
  }
  
  public final w<Conversation> a(org.a.a.b paramb)
  {
    k.b(paramb, "afterDate");
    Object localObject3 = null;
    Object localObject5 = null;
    try
    {
      paramb = a.query(TruecallerContract.g.a(), null, "date>?", new String[] { String.valueOf(a) }, "date DESC LIMIT 1");
      if (paramb == null)
      {
        paramb = w.b(null);
        k.a(paramb, "Promise.wrap(null)");
        com.truecaller.util.q.a(null);
        return paramb;
      }
      Object localObject1 = b.a(paramb);
      if (localObject1 == null) {}
      try
      {
        k.a();
        if (!((a)localObject1).moveToNext()) {
          paramb = (org.a.a.b)localObject5;
        } else {
          paramb = ((a)localObject1).b();
        }
        paramb = w.b(paramb);
        k.a(paramb, "Promise.wrap(conversation)");
        com.truecaller.util.q.a((Cursor)localObject1);
        return paramb;
      }
      finally
      {
        paramb = (org.a.a.b)localObject1;
        localObject1 = localObject4;
      }
      com.truecaller.util.q.a((Cursor)paramb);
    }
    finally
    {
      paramb = (org.a.a.b)localObject4;
    }
    throw ((Throwable)localObject2);
  }
  
  /* Error */
  public final w<com.truecaller.messaging.data.types.Draft> a(com.truecaller.messaging.data.types.Participant[] paramArrayOfParticipant, int paramInt)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 310
    //   4: invokestatic 24	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   7: aload_1
    //   8: arraylength
    //   9: istore_3
    //   10: iconst_0
    //   11: istore 4
    //   13: iload_3
    //   14: ifne +8 -> 22
    //   17: iconst_1
    //   18: istore_3
    //   19: goto +5 -> 24
    //   22: iconst_0
    //   23: istore_3
    //   24: iload_3
    //   25: iconst_1
    //   26: ixor
    //   27: iconst_1
    //   28: anewarray 54	java/lang/String
    //   31: dup
    //   32: iconst_0
    //   33: ldc_w 312
    //   36: aastore
    //   37: invokestatic 318	com/truecaller/log/AssertionUtil:isTrue	(Z[Ljava/lang/String;)V
    //   40: new 320	com/truecaller/messaging/data/types/Draft$a
    //   43: dup
    //   44: invokespecial 321	com/truecaller/messaging/data/types/Draft$a:<init>	()V
    //   47: astore 12
    //   49: getstatic 326	com/truecaller/messaging/data/q$c:a	Lcom/truecaller/messaging/data/q$c;
    //   52: checkcast 328	java/util/Comparator
    //   55: astore 8
    //   57: aload_1
    //   58: aload_1
    //   59: arraylength
    //   60: invokestatic 334	java/util/Arrays:copyOf	([Ljava/lang/Object;I)[Ljava/lang/Object;
    //   63: checkcast 336	[Lcom/truecaller/messaging/data/types/Participant;
    //   66: astore 9
    //   68: aload 8
    //   70: ldc_w 338
    //   73: invokestatic 24	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   76: aload 9
    //   78: ldc_w 340
    //   81: invokestatic 24	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   84: aload 9
    //   86: new 342	java/util/TreeSet
    //   89: dup
    //   90: aload 8
    //   92: invokespecial 345	java/util/TreeSet:<init>	(Ljava/util/Comparator;)V
    //   95: checkcast 347	java/util/Collection
    //   98: invokestatic 352	c/a/f:b	([Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;
    //   101: checkcast 342	java/util/TreeSet
    //   104: checkcast 264	java/lang/Iterable
    //   107: invokestatic 356	c/a/m:l	(Ljava/lang/Iterable;)Ljava/util/Set;
    //   110: astore 11
    //   112: new 342	java/util/TreeSet
    //   115: dup
    //   116: getstatic 361	com/truecaller/messaging/data/q$b:a	Lcom/truecaller/messaging/data/q$b;
    //   119: checkcast 328	java/util/Comparator
    //   122: invokespecial 345	java/util/TreeSet:<init>	(Ljava/util/Comparator;)V
    //   125: astore 13
    //   127: aconst_null
    //   128: astore 8
    //   130: aconst_null
    //   131: astore 10
    //   133: aconst_null
    //   134: astore 9
    //   136: new 363	java/util/ArrayList
    //   139: dup
    //   140: aload_1
    //   141: arraylength
    //   142: invokespecial 366	java/util/ArrayList:<init>	(I)V
    //   145: checkcast 347	java/util/Collection
    //   148: astore 14
    //   150: aload_1
    //   151: arraylength
    //   152: istore 5
    //   154: iconst_0
    //   155: istore_3
    //   156: iload_3
    //   157: iload 5
    //   159: if_icmpge +24 -> 183
    //   162: aload 14
    //   164: aload_1
    //   165: iload_3
    //   166: aaload
    //   167: getfield 372	com/truecaller/messaging/data/types/Participant:f	Ljava/lang/String;
    //   170: invokeinterface 376 2 0
    //   175: pop
    //   176: iload_3
    //   177: iconst_1
    //   178: iadd
    //   179: istore_3
    //   180: goto -24 -> 156
    //   183: aload 14
    //   185: checkcast 378	java/util/List
    //   188: checkcast 347	java/util/Collection
    //   191: iconst_0
    //   192: anewarray 54	java/lang/String
    //   195: invokeinterface 382 2 0
    //   200: astore_1
    //   201: aload_1
    //   202: ifnull +940 -> 1142
    //   205: aload_1
    //   206: checkcast 384	[Ljava/lang/String;
    //   209: astore_1
    //   210: aload_0
    //   211: getfield 37	com/truecaller/messaging/data/q:b	Lcom/truecaller/messaging/data/c;
    //   214: aload_0
    //   215: getfield 35	com/truecaller/messaging/data/q:a	Landroid/content/ContentResolver;
    //   218: aload_1
    //   219: invokestatic 389	com/truecaller/content/TruecallerContract$af:a	([Ljava/lang/String;)Landroid/net/Uri;
    //   222: aconst_null
    //   223: aconst_null
    //   224: aconst_null
    //   225: ldc_w 391
    //   228: invokevirtual 62	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   231: invokeinterface 394 2 0
    //   236: astore_1
    //   237: aload_1
    //   238: ifnull +217 -> 455
    //   241: aload_1
    //   242: invokeinterface 397 1 0
    //   247: ifeq +208 -> 455
    //   250: aload_1
    //   251: invokeinterface 400 1 0
    //   256: astore 14
    //   258: aload 14
    //   260: ldc_w 402
    //   263: invokestatic 143	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   266: aload 11
    //   268: aload 14
    //   270: invokeinterface 407 2 0
    //   275: istore 7
    //   277: iload 7
    //   279: istore 6
    //   281: iload 7
    //   283: ifne +40 -> 323
    //   286: aload 14
    //   288: invokevirtual 410	com/truecaller/messaging/data/types/Participant:l	()Lcom/truecaller/messaging/data/types/Participant$a;
    //   291: aload 14
    //   293: getfield 413	com/truecaller/messaging/data/types/Participant:e	Ljava/lang/String;
    //   296: invokevirtual 418	com/truecaller/messaging/data/types/Participant$a:b	(Ljava/lang/String;)Lcom/truecaller/messaging/data/types/Participant$a;
    //   299: invokevirtual 419	com/truecaller/messaging/data/types/Participant$a:a	()Lcom/truecaller/messaging/data/types/Participant;
    //   302: astore 15
    //   304: aload 15
    //   306: ldc_w 421
    //   309: invokestatic 143	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   312: aload 11
    //   314: aload 15
    //   316: invokeinterface 407 2 0
    //   321: istore 6
    //   323: iload 6
    //   325: istore 7
    //   327: iload 6
    //   329: ifne +72 -> 401
    //   332: aload_1
    //   333: invokeinterface 423 1 0
    //   338: astore 15
    //   340: iload 6
    //   342: istore 7
    //   344: aload 15
    //   346: checkcast 268	java/lang/CharSequence
    //   349: invokestatic 429	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   352: ifne +49 -> 401
    //   355: aload 14
    //   357: invokevirtual 410	com/truecaller/messaging/data/types/Participant:l	()Lcom/truecaller/messaging/data/types/Participant$a;
    //   360: astore 16
    //   362: aload 15
    //   364: ifnonnull +6 -> 370
    //   367: invokestatic 181	c/g/b/k:a	()V
    //   370: aload 16
    //   372: aload 15
    //   374: invokevirtual 418	com/truecaller/messaging/data/types/Participant$a:b	(Ljava/lang/String;)Lcom/truecaller/messaging/data/types/Participant$a;
    //   377: invokevirtual 419	com/truecaller/messaging/data/types/Participant$a:a	()Lcom/truecaller/messaging/data/types/Participant;
    //   380: astore 15
    //   382: aload 15
    //   384: ldc_w 431
    //   387: invokestatic 143	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   390: aload 11
    //   392: aload 15
    //   394: invokeinterface 407 2 0
    //   399: istore 7
    //   401: iload 7
    //   403: ifeq +14 -> 417
    //   406: aload 13
    //   408: aload 14
    //   410: invokevirtual 432	java/util/TreeSet:add	(Ljava/lang/Object;)Z
    //   413: pop
    //   414: goto -173 -> 241
    //   417: aload 14
    //   419: getfield 434	com/truecaller/messaging/data/types/Participant:b	J
    //   422: ldc2_w 435
    //   425: lcmp
    //   426: ifeq -185 -> 241
    //   429: aload 13
    //   431: aload 14
    //   433: invokevirtual 437	java/util/TreeSet:remove	(Ljava/lang/Object;)Z
    //   436: ifeq -195 -> 241
    //   439: aload 13
    //   441: aload 14
    //   443: invokevirtual 432	java/util/TreeSet:add	(Ljava/lang/Object;)Z
    //   446: pop
    //   447: goto -206 -> 241
    //   450: astore 8
    //   452: goto +705 -> 1157
    //   455: aload_1
    //   456: checkcast 73	android/database/Cursor
    //   459: invokestatic 148	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   462: aload 11
    //   464: invokeinterface 439 1 0
    //   469: ifeq +199 -> 668
    //   472: aload 13
    //   474: checkcast 264	java/lang/Iterable
    //   477: astore 11
    //   479: new 363	java/util/ArrayList
    //   482: dup
    //   483: aload 11
    //   485: bipush 10
    //   487: invokestatic 442	c/a/m:a	(Ljava/lang/Iterable;I)I
    //   490: invokespecial 366	java/util/ArrayList:<init>	(I)V
    //   493: checkcast 347	java/util/Collection
    //   496: astore_1
    //   497: aload 11
    //   499: invokeinterface 446 1 0
    //   504: astore 11
    //   506: aload 11
    //   508: invokeinterface 451 1 0
    //   513: ifeq +26 -> 539
    //   516: aload_1
    //   517: aload 11
    //   519: invokeinterface 455 1 0
    //   524: checkcast 368	com/truecaller/messaging/data/types/Participant
    //   527: getfield 372	com/truecaller/messaging/data/types/Participant:f	Ljava/lang/String;
    //   530: invokeinterface 376 2 0
    //   535: pop
    //   536: goto -30 -> 506
    //   539: aload_1
    //   540: checkcast 378	java/util/List
    //   543: checkcast 347	java/util/Collection
    //   546: iconst_0
    //   547: anewarray 54	java/lang/String
    //   550: invokeinterface 382 2 0
    //   555: astore_1
    //   556: aload_1
    //   557: ifnull +100 -> 657
    //   560: aload_1
    //   561: checkcast 384	[Ljava/lang/String;
    //   564: astore_1
    //   565: aload_0
    //   566: getfield 37	com/truecaller/messaging/data/q:b	Lcom/truecaller/messaging/data/c;
    //   569: aload_0
    //   570: getfield 35	com/truecaller/messaging/data/q:a	Landroid/content/ContentResolver;
    //   573: aload_1
    //   574: iload_2
    //   575: invokestatic 458	com/truecaller/content/TruecallerContract$g:a	([Ljava/lang/String;I)Landroid/net/Uri;
    //   578: aconst_null
    //   579: aconst_null
    //   580: aconst_null
    //   581: aconst_null
    //   582: invokevirtual 62	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   585: invokeinterface 162 2 0
    //   590: astore 11
    //   592: aload 11
    //   594: ifnull +32 -> 626
    //   597: aload 11
    //   599: invokeinterface 459 1 0
    //   604: ifeq +22 -> 626
    //   607: aload 11
    //   609: invokeinterface 189 1 0
    //   614: astore_1
    //   615: goto +13 -> 628
    //   618: astore 8
    //   620: aload 11
    //   622: astore_1
    //   623: goto +24 -> 647
    //   626: aconst_null
    //   627: astore_1
    //   628: aload 11
    //   630: checkcast 73	android/database/Cursor
    //   633: invokestatic 148	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   636: aload_1
    //   637: astore 9
    //   639: goto +43 -> 682
    //   642: astore 8
    //   644: aload 9
    //   646: astore_1
    //   647: aload_1
    //   648: checkcast 73	android/database/Cursor
    //   651: invokestatic 148	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   654: aload 8
    //   656: athrow
    //   657: new 461	c/u
    //   660: dup
    //   661: ldc_w 463
    //   664: invokespecial 464	c/u:<init>	(Ljava/lang/String;)V
    //   667: athrow
    //   668: aload 13
    //   670: aload 11
    //   672: checkcast 347	java/util/Collection
    //   675: invokevirtual 468	java/util/TreeSet:addAll	(Ljava/util/Collection;)Z
    //   678: pop
    //   679: aconst_null
    //   680: astore 9
    //   682: aload 13
    //   684: checkcast 347	java/util/Collection
    //   687: iconst_0
    //   688: anewarray 368	com/truecaller/messaging/data/types/Participant
    //   691: invokeinterface 382 2 0
    //   696: astore_1
    //   697: aload_1
    //   698: ifnull +433 -> 1131
    //   701: aload 12
    //   703: aload_1
    //   704: checkcast 336	[Lcom/truecaller/messaging/data/types/Participant;
    //   707: invokevirtual 471	com/truecaller/messaging/data/types/Draft$a:a	([Lcom/truecaller/messaging/data/types/Participant;)Lcom/truecaller/messaging/data/types/Draft$a;
    //   710: pop
    //   711: aload 9
    //   713: ifnull +400 -> 1113
    //   716: aload 12
    //   718: aload 9
    //   720: invokevirtual 474	com/truecaller/messaging/data/types/Draft$a:a	(Lcom/truecaller/messaging/data/types/Conversation;)Lcom/truecaller/messaging/data/types/Draft$a;
    //   723: pop
    //   724: aload 10
    //   726: astore_1
    //   727: aload_0
    //   728: getfield 35	com/truecaller/messaging/data/q:a	Landroid/content/ContentResolver;
    //   731: aload 9
    //   733: getfield 477	com/truecaller/messaging/data/types/Conversation:a	J
    //   736: invokestatic 196	com/truecaller/content/TruecallerContract$ab:a	(J)Landroid/net/Uri;
    //   739: aconst_null
    //   740: ldc_w 479
    //   743: aconst_null
    //   744: aconst_null
    //   745: invokevirtual 62	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   748: astore 9
    //   750: aload 9
    //   752: ifnull +290 -> 1042
    //   755: aload 9
    //   757: astore 8
    //   759: aload 9
    //   761: astore_1
    //   762: aload 9
    //   764: invokeinterface 81 1 0
    //   769: ifne +6 -> 775
    //   772: goto +270 -> 1042
    //   775: aload 9
    //   777: astore 8
    //   779: aload 9
    //   781: astore_1
    //   782: aload_0
    //   783: getfield 37	com/truecaller/messaging/data/q:b	Lcom/truecaller/messaging/data/c;
    //   786: aload 9
    //   788: invokeinterface 224 2 0
    //   793: astore 10
    //   795: aload 10
    //   797: ifnonnull +13 -> 810
    //   800: aload 9
    //   802: astore 8
    //   804: aload 9
    //   806: astore_1
    //   807: invokestatic 181	c/g/b/k:a	()V
    //   810: aload 9
    //   812: astore 8
    //   814: aload 9
    //   816: astore_1
    //   817: aload 10
    //   819: invokeinterface 484 1 0
    //   824: astore 10
    //   826: aload 9
    //   828: astore 8
    //   830: aload 9
    //   832: astore_1
    //   833: aload 10
    //   835: ldc_w 486
    //   838: invokestatic 143	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   841: aload 9
    //   843: astore 8
    //   845: aload 9
    //   847: astore_1
    //   848: aload 12
    //   850: aload 10
    //   852: invokevirtual 490	com/truecaller/messaging/data/types/Message:a	()J
    //   855: invokevirtual 493	com/truecaller/messaging/data/types/Draft$a:a	(J)Lcom/truecaller/messaging/data/types/Draft$a;
    //   858: pop
    //   859: aload 9
    //   861: astore 8
    //   863: aload 9
    //   865: astore_1
    //   866: aload 12
    //   868: aload 10
    //   870: getfield 496	com/truecaller/messaging/data/types/Message:x	J
    //   873: invokevirtual 498	com/truecaller/messaging/data/types/Draft$a:b	(J)Lcom/truecaller/messaging/data/types/Draft$a;
    //   876: pop
    //   877: aload 9
    //   879: astore 8
    //   881: aload 9
    //   883: astore_1
    //   884: aload 10
    //   886: invokevirtual 501	com/truecaller/messaging/data/types/Message:g	()Z
    //   889: ifeq +21 -> 910
    //   892: aload 9
    //   894: astore 8
    //   896: aload 9
    //   898: astore_1
    //   899: aload 12
    //   901: aload 10
    //   903: getfield 505	com/truecaller/messaging/data/types/Message:v	Lcom/truecaller/messaging/data/types/ReplySnippet;
    //   906: invokevirtual 508	com/truecaller/messaging/data/types/Draft$a:a	(Lcom/truecaller/messaging/data/types/ReplySnippet;)Lcom/truecaller/messaging/data/types/Draft$a;
    //   909: pop
    //   910: aload 9
    //   912: astore 8
    //   914: aload 9
    //   916: astore_1
    //   917: aload 10
    //   919: getfield 512	com/truecaller/messaging/data/types/Message:n	[Lcom/truecaller/messaging/data/types/Entity;
    //   922: astore 10
    //   924: aload 9
    //   926: astore 8
    //   928: aload 9
    //   930: astore_1
    //   931: aload 10
    //   933: ldc_w 514
    //   936: invokestatic 143	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   939: aload 9
    //   941: astore 8
    //   943: aload 9
    //   945: astore_1
    //   946: aload 10
    //   948: arraylength
    //   949: istore_3
    //   950: iload 4
    //   952: istore_2
    //   953: aload 9
    //   955: astore_1
    //   956: iload_2
    //   957: iload_3
    //   958: if_icmpge +141 -> 1099
    //   961: aload 10
    //   963: iload_2
    //   964: aaload
    //   965: astore 11
    //   967: aload 9
    //   969: astore 8
    //   971: aload 9
    //   973: astore_1
    //   974: aload 11
    //   976: ldc_w 516
    //   979: invokestatic 143	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   982: aload 9
    //   984: astore 8
    //   986: aload 9
    //   988: astore_1
    //   989: aload 11
    //   991: invokevirtual 520	com/truecaller/messaging/data/types/Entity:a	()Z
    //   994: ifeq +27 -> 1021
    //   997: aload 9
    //   999: astore 8
    //   1001: aload 9
    //   1003: astore_1
    //   1004: aload 12
    //   1006: aload 11
    //   1008: checkcast 522	com/truecaller/messaging/data/types/TextEntity
    //   1011: getfield 524	com/truecaller/messaging/data/types/TextEntity:a	Ljava/lang/String;
    //   1014: invokevirtual 527	com/truecaller/messaging/data/types/Draft$a:a	(Ljava/lang/String;)Lcom/truecaller/messaging/data/types/Draft$a;
    //   1017: pop
    //   1018: goto +149 -> 1167
    //   1021: aload 9
    //   1023: astore 8
    //   1025: aload 9
    //   1027: astore_1
    //   1028: aload 12
    //   1030: aload 11
    //   1032: checkcast 529	com/truecaller/messaging/data/types/BinaryEntity
    //   1035: invokevirtual 532	com/truecaller/messaging/data/types/Draft$a:a	(Lcom/truecaller/messaging/data/types/BinaryEntity;)Lcom/truecaller/messaging/data/types/Draft$a;
    //   1038: pop
    //   1039: goto +128 -> 1167
    //   1042: aload 9
    //   1044: astore 8
    //   1046: aload 9
    //   1048: astore_1
    //   1049: aload 12
    //   1051: invokevirtual 535	com/truecaller/messaging/data/types/Draft$a:d	()Lcom/truecaller/messaging/data/types/Draft;
    //   1054: invokestatic 139	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   1057: astore 10
    //   1059: aload 9
    //   1061: astore 8
    //   1063: aload 9
    //   1065: astore_1
    //   1066: aload 10
    //   1068: ldc_w 537
    //   1071: invokestatic 143	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1074: aload 9
    //   1076: invokestatic 148	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   1079: aload 10
    //   1081: areturn
    //   1082: astore_1
    //   1083: goto +23 -> 1106
    //   1086: astore 9
    //   1088: aload_1
    //   1089: astore 8
    //   1091: aload 9
    //   1093: checkcast 65	java/lang/Throwable
    //   1096: invokestatic 541	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   1099: aload_1
    //   1100: invokestatic 148	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   1103: goto +10 -> 1113
    //   1106: aload 8
    //   1108: invokestatic 148	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   1111: aload_1
    //   1112: athrow
    //   1113: aload 12
    //   1115: invokevirtual 535	com/truecaller/messaging/data/types/Draft$a:d	()Lcom/truecaller/messaging/data/types/Draft;
    //   1118: invokestatic 139	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   1121: astore_1
    //   1122: aload_1
    //   1123: ldc_w 537
    //   1126: invokestatic 143	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1129: aload_1
    //   1130: areturn
    //   1131: new 461	c/u
    //   1134: dup
    //   1135: ldc_w 463
    //   1138: invokespecial 464	c/u:<init>	(Ljava/lang/String;)V
    //   1141: athrow
    //   1142: new 461	c/u
    //   1145: dup
    //   1146: ldc_w 463
    //   1149: invokespecial 464	c/u:<init>	(Ljava/lang/String;)V
    //   1152: athrow
    //   1153: astore 8
    //   1155: aconst_null
    //   1156: astore_1
    //   1157: aload_1
    //   1158: checkcast 73	android/database/Cursor
    //   1161: invokestatic 148	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   1164: aload 8
    //   1166: athrow
    //   1167: iload_2
    //   1168: iconst_1
    //   1169: iadd
    //   1170: istore_2
    //   1171: goto -218 -> 953
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1174	0	this	q
    //   0	1174	1	paramArrayOfParticipant	com.truecaller.messaging.data.types.Participant[]
    //   0	1174	2	paramInt	int
    //   9	950	3	i	int
    //   11	940	4	j	int
    //   152	8	5	k	int
    //   279	62	6	bool1	boolean
    //   275	127	7	bool2	boolean
    //   55	74	8	localComparator	java.util.Comparator
    //   450	1	8	localObject1	Object
    //   618	1	8	localObject2	Object
    //   642	13	8	localObject3	Object
    //   757	350	8	localObject4	Object
    //   1153	12	8	localObject5	Object
    //   66	1009	9	localObject6	Object
    //   1086	6	9	localSQLException	android.database.SQLException
    //   131	949	10	localObject7	Object
    //   110	921	11	localObject8	Object
    //   47	1067	12	locala	com.truecaller.messaging.data.types.Draft.a
    //   125	558	13	localTreeSet	java.util.TreeSet
    //   148	294	14	localObject9	Object
    //   302	91	15	localObject10	Object
    //   360	11	16	locala1	com.truecaller.messaging.data.types.Participant.a
    // Exception table:
    //   from	to	target	type
    //   241	277	450	finally
    //   286	323	450	finally
    //   332	340	450	finally
    //   344	362	450	finally
    //   367	370	450	finally
    //   370	401	450	finally
    //   406	414	450	finally
    //   417	447	450	finally
    //   597	615	618	finally
    //   565	592	642	finally
    //   727	750	1082	finally
    //   762	772	1082	finally
    //   782	795	1082	finally
    //   807	810	1082	finally
    //   817	826	1082	finally
    //   833	841	1082	finally
    //   848	859	1082	finally
    //   866	877	1082	finally
    //   884	892	1082	finally
    //   899	910	1082	finally
    //   917	924	1082	finally
    //   931	939	1082	finally
    //   946	950	1082	finally
    //   974	982	1082	finally
    //   989	997	1082	finally
    //   1004	1018	1082	finally
    //   1028	1039	1082	finally
    //   1049	1059	1082	finally
    //   1066	1074	1082	finally
    //   1091	1099	1082	finally
    //   727	750	1086	android/database/SQLException
    //   762	772	1086	android/database/SQLException
    //   782	795	1086	android/database/SQLException
    //   807	810	1086	android/database/SQLException
    //   817	826	1086	android/database/SQLException
    //   833	841	1086	android/database/SQLException
    //   848	859	1086	android/database/SQLException
    //   866	877	1086	android/database/SQLException
    //   884	892	1086	android/database/SQLException
    //   899	910	1086	android/database/SQLException
    //   917	924	1086	android/database/SQLException
    //   931	939	1086	android/database/SQLException
    //   946	950	1086	android/database/SQLException
    //   974	982	1086	android/database/SQLException
    //   989	997	1086	android/database/SQLException
    //   1004	1018	1086	android/database/SQLException
    //   1028	1039	1086	android/database/SQLException
    //   1049	1059	1086	android/database/SQLException
    //   1066	1074	1086	android/database/SQLException
    //   136	154	1153	finally
    //   162	176	1153	finally
    //   183	201	1153	finally
    //   205	237	1153	finally
    //   1142	1153	1153	finally
  }
  
  public final w<n<com.truecaller.messaging.data.a.c, Long>> b()
  {
    try
    {
      if (c.w().a())
      {
        localObject1 = new MergeCursor(new Cursor[] { b(4), b(3), b(2) });
        localObject1 = w.b(new n(b.a((Cursor)localObject1, true), Long.valueOf(c())));
        k.a(localObject1, "Promise.wrap(\n          …      )\n                )");
        return (w<n<com.truecaller.messaging.data.a.c, Long>>)localObject1;
      }
      Object localObject1 = new StringBuilder("SUM(CASE WHEN ");
      ((StringBuilder)localObject1).append(c(4));
      ((StringBuilder)localObject1).append(' ');
      ((StringBuilder)localObject1).append("THEN 1 ELSE 0 END) AS 'INBOX' ");
      localObject1 = ((StringBuilder)localObject1).toString();
      Object localObject3 = new StringBuilder("SUM(CASE WHEN ");
      ((StringBuilder)localObject3).append(c(3));
      ((StringBuilder)localObject3).append(' ');
      ((StringBuilder)localObject3).append("THEN 1 ELSE 0 END) AS 'SPAM' ");
      localObject3 = ((StringBuilder)localObject3).toString();
      Object localObject4 = new StringBuilder("SUM(CASE WHEN ");
      ((StringBuilder)localObject4).append(c(2));
      ((StringBuilder)localObject4).append(' ');
      ((StringBuilder)localObject4).append("THEN 1 ELSE 0 END) AS 'NON_SPAM' ");
      localObject4 = m.b(new String[] { localObject1, localObject3, ((StringBuilder)localObject4).toString() });
      localObject1 = a;
      localObject3 = TruecallerContract.g.a();
      localObject4 = (Collection)localObject4;
      if (localObject4 != null)
      {
        localObject4 = ((Collection)localObject4).toArray(new String[0]);
        if (localObject4 != null)
        {
          localObject1 = ((ContentResolver)localObject1).query((Uri)localObject3, (String[])localObject4, null, null, null);
          localObject1 = w.b(new n(b.a((Cursor)localObject1, false), Long.valueOf(c())));
          k.a(localObject1, "Promise.wrap(\n          …      )\n                )");
          return (w<n<com.truecaller.messaging.data.a.c, Long>>)localObject1;
        }
        throw new u("null cannot be cast to non-null type kotlin.Array<T>");
      }
      throw new u("null cannot be cast to non-null type java.util.Collection<T>");
    }
    finally {}
  }
  
  /* Error */
  public final w<com.truecaller.messaging.data.types.Message> b(long paramLong)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: aload_0
    //   4: getfield 35	com/truecaller/messaging/data/q:a	Landroid/content/ContentResolver;
    //   7: invokestatic 593	com/truecaller/content/TruecallerContract$ab:a	()Landroid/net/Uri;
    //   10: aconst_null
    //   11: ldc_w 595
    //   14: iconst_1
    //   15: anewarray 54	java/lang/String
    //   18: dup
    //   19: iconst_0
    //   20: lload_1
    //   21: invokestatic 304	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   24: aastore
    //   25: aconst_null
    //   26: invokevirtual 62	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   29: astore_3
    //   30: aload_0
    //   31: getfield 37	com/truecaller/messaging/data/q:b	Lcom/truecaller/messaging/data/c;
    //   34: aload_3
    //   35: invokeinterface 224 2 0
    //   40: astore_3
    //   41: aload_3
    //   42: astore 5
    //   44: aload_3
    //   45: ifnull +80 -> 125
    //   48: aload_3
    //   49: astore 5
    //   51: aload_3
    //   52: astore 4
    //   54: aload_3
    //   55: invokeinterface 596 1 0
    //   60: ifeq +65 -> 125
    //   63: aload_3
    //   64: astore 4
    //   66: aload_3
    //   67: invokeinterface 484 1 0
    //   72: invokestatic 139	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   75: astore 5
    //   77: aload_3
    //   78: astore 4
    //   80: aload 5
    //   82: ldc_w 598
    //   85: invokestatic 143	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   88: aload_3
    //   89: checkcast 73	android/database/Cursor
    //   92: invokestatic 148	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   95: aload 5
    //   97: areturn
    //   98: astore 5
    //   100: goto +11 -> 111
    //   103: astore_3
    //   104: goto +43 -> 147
    //   107: astore 5
    //   109: aconst_null
    //   110: astore_3
    //   111: aload_3
    //   112: astore 4
    //   114: aload 5
    //   116: checkcast 65	java/lang/Throwable
    //   119: invokestatic 541	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   122: aload_3
    //   123: astore 5
    //   125: aload 5
    //   127: checkcast 73	android/database/Cursor
    //   130: invokestatic 148	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   133: aconst_null
    //   134: invokestatic 139	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   137: astore_3
    //   138: aload_3
    //   139: ldc -99
    //   141: invokestatic 143	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   144: aload_3
    //   145: areturn
    //   146: astore_3
    //   147: aload 4
    //   149: checkcast 73	android/database/Cursor
    //   152: invokestatic 148	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   155: aload_3
    //   156: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	157	0	this	q
    //   0	157	1	paramLong	long
    //   29	60	3	localObject1	Object
    //   103	1	3	localObject2	Object
    //   110	35	3	localw1	w
    //   146	10	3	localObject3	Object
    //   1	147	4	localObject4	Object
    //   42	54	5	localObject5	Object
    //   98	1	5	localSQLException1	android.database.SQLException
    //   107	8	5	localSQLException2	android.database.SQLException
    //   123	3	5	localw2	w
    // Exception table:
    //   from	to	target	type
    //   54	63	98	android/database/SQLException
    //   66	77	98	android/database/SQLException
    //   80	88	98	android/database/SQLException
    //   3	41	103	finally
    //   3	41	107	android/database/SQLException
    //   54	63	146	finally
    //   66	77	146	finally
    //   80	88	146	finally
    //   114	122	146	finally
  }
  
  /* Error */
  public final w<Long> b(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 601
    //   4: invokestatic 24	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   7: aload_0
    //   8: getfield 35	com/truecaller/messaging/data/q:a	Landroid/content/ContentResolver;
    //   11: invokestatic 604	com/truecaller/content/TruecallerContract$w:a	()Landroid/net/Uri;
    //   14: iconst_1
    //   15: anewarray 54	java/lang/String
    //   18: dup
    //   19: iconst_0
    //   20: ldc_w 606
    //   23: aastore
    //   24: ldc_w 608
    //   27: iconst_1
    //   28: anewarray 54	java/lang/String
    //   31: dup
    //   32: iconst_0
    //   33: aload_1
    //   34: aastore
    //   35: aconst_null
    //   36: invokevirtual 62	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   39: astore_1
    //   40: aload_1
    //   41: ifnull +96 -> 137
    //   44: aload_1
    //   45: checkcast 71	java/io/Closeable
    //   48: astore_3
    //   49: aconst_null
    //   50: astore_2
    //   51: aload_2
    //   52: astore_1
    //   53: aload_3
    //   54: checkcast 73	android/database/Cursor
    //   57: astore 4
    //   59: aload_2
    //   60: astore_1
    //   61: aload 4
    //   63: invokeinterface 81 1 0
    //   68: ifeq +39 -> 107
    //   71: aload_2
    //   72: astore_1
    //   73: aload 4
    //   75: iconst_0
    //   76: invokeinterface 85 2 0
    //   81: invokestatic 570	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   84: invokestatic 139	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   87: astore 4
    //   89: aload_2
    //   90: astore_1
    //   91: aload 4
    //   93: ldc_w 610
    //   96: invokestatic 143	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   99: aload_3
    //   100: aconst_null
    //   101: invokestatic 90	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   104: aload 4
    //   106: areturn
    //   107: aload_2
    //   108: astore_1
    //   109: getstatic 95	c/x:a	Lc/x;
    //   112: astore_2
    //   113: aload_3
    //   114: aconst_null
    //   115: invokestatic 90	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   118: goto +19 -> 137
    //   121: astore_2
    //   122: goto +8 -> 130
    //   125: astore_2
    //   126: aload_2
    //   127: astore_1
    //   128: aload_2
    //   129: athrow
    //   130: aload_3
    //   131: aload_1
    //   132: invokestatic 90	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   135: aload_2
    //   136: athrow
    //   137: ldc2_w 435
    //   140: invokestatic 570	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   143: invokestatic 139	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   146: astore_1
    //   147: aload_1
    //   148: ldc_w 612
    //   151: invokestatic 143	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   154: aload_1
    //   155: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	156	0	this	q
    //   0	156	1	paramString	String
    //   50	63	2	localx	c.x
    //   121	1	2	localObject1	Object
    //   125	11	2	localThrowable	Throwable
    //   48	83	3	localCloseable	java.io.Closeable
    //   57	48	4	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   53	59	121	finally
    //   61	71	121	finally
    //   73	89	121	finally
    //   91	99	121	finally
    //   109	113	121	finally
    //   128	130	121	finally
    //   53	59	125	java/lang/Throwable
    //   61	71	125	java/lang/Throwable
    //   73	89	125	java/lang/Throwable
    //   91	99	125	java/lang/Throwable
    //   109	113	125	java/lang/Throwable
  }
  
  /* Error */
  public final w<String> c(long paramLong)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 35	com/truecaller/messaging/data/q:a	Landroid/content/ContentResolver;
    //   4: invokestatic 604	com/truecaller/content/TruecallerContract$w:a	()Landroid/net/Uri;
    //   7: iconst_1
    //   8: anewarray 54	java/lang/String
    //   11: dup
    //   12: iconst_0
    //   13: ldc_w 615
    //   16: aastore
    //   17: ldc_w 617
    //   20: iconst_1
    //   21: anewarray 54	java/lang/String
    //   24: dup
    //   25: iconst_0
    //   26: lload_1
    //   27: invokestatic 304	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   30: aastore
    //   31: aconst_null
    //   32: invokevirtual 62	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   35: astore_3
    //   36: aconst_null
    //   37: astore 4
    //   39: aload_3
    //   40: ifnull +107 -> 147
    //   43: aload_3
    //   44: checkcast 71	java/io/Closeable
    //   47: astore 5
    //   49: aload 4
    //   51: astore_3
    //   52: aload 5
    //   54: checkcast 73	android/database/Cursor
    //   57: astore 6
    //   59: aload 4
    //   61: astore_3
    //   62: aload 6
    //   64: invokeinterface 81 1 0
    //   69: ifeq +39 -> 108
    //   72: aload 4
    //   74: astore_3
    //   75: aload 6
    //   77: iconst_0
    //   78: invokeinterface 620 2 0
    //   83: invokestatic 139	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   86: astore 6
    //   88: aload 4
    //   90: astore_3
    //   91: aload 6
    //   93: ldc_w 622
    //   96: invokestatic 143	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   99: aload 5
    //   101: aconst_null
    //   102: invokestatic 90	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   105: aload 6
    //   107: areturn
    //   108: aload 4
    //   110: astore_3
    //   111: getstatic 95	c/x:a	Lc/x;
    //   114: astore 4
    //   116: aload 5
    //   118: aconst_null
    //   119: invokestatic 90	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   122: goto +25 -> 147
    //   125: astore 4
    //   127: goto +11 -> 138
    //   130: astore 4
    //   132: aload 4
    //   134: astore_3
    //   135: aload 4
    //   137: athrow
    //   138: aload 5
    //   140: aload_3
    //   141: invokestatic 90	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   144: aload 4
    //   146: athrow
    //   147: aconst_null
    //   148: invokestatic 139	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   151: astore_3
    //   152: aload_3
    //   153: ldc -99
    //   155: invokestatic 143	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   158: aload_3
    //   159: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	160	0	this	q
    //   0	160	1	paramLong	long
    //   35	124	3	localObject1	Object
    //   37	78	4	localx	c.x
    //   125	1	4	localObject2	Object
    //   130	15	4	localThrowable	Throwable
    //   47	92	5	localCloseable	java.io.Closeable
    //   57	49	6	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   52	59	125	finally
    //   62	72	125	finally
    //   75	88	125	finally
    //   91	99	125	finally
    //   111	116	125	finally
    //   135	138	125	finally
    //   52	59	130	java/lang/Throwable
    //   62	72	130	java/lang/Throwable
    //   75	88	130	java/lang/Throwable
    //   91	99	130	java/lang/Throwable
    //   111	116	130	java/lang/Throwable
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */