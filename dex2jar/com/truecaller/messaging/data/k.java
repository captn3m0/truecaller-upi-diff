package com.truecaller.messaging.data;

import com.truecaller.androidactors.i;
import dagger.a.d;
import javax.inject.Provider;

public final class k
  implements d<i>
{
  private final e a;
  private final Provider<com.truecaller.androidactors.k> b;
  
  private k(e parame, Provider<com.truecaller.androidactors.k> paramProvider)
  {
    a = parame;
    b = paramProvider;
  }
  
  public static k a(e parame, Provider<com.truecaller.androidactors.k> paramProvider)
  {
    return new k(parame, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */