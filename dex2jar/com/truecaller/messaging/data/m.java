package com.truecaller.messaging.data;

import android.content.ContentResolver;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.k;
import com.truecaller.messaging.notifications.a;
import dagger.a.d;
import javax.inject.Provider;

public final class m
  implements d<al>
{
  private final e a;
  private final Provider<ContentResolver> b;
  private final Provider<f<o>> c;
  private final Provider<k> d;
  private final Provider<com.truecaller.util.al> e;
  private final Provider<com.truecaller.network.search.e> f;
  private final Provider<f<a>> g;
  
  private m(e parame, Provider<ContentResolver> paramProvider, Provider<f<o>> paramProvider1, Provider<k> paramProvider2, Provider<com.truecaller.util.al> paramProvider3, Provider<com.truecaller.network.search.e> paramProvider4, Provider<f<a>> paramProvider5)
  {
    a = parame;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
    f = paramProvider4;
    g = paramProvider5;
  }
  
  public static m a(e parame, Provider<ContentResolver> paramProvider, Provider<f<o>> paramProvider1, Provider<k> paramProvider2, Provider<com.truecaller.util.al> paramProvider3, Provider<com.truecaller.network.search.e> paramProvider4, Provider<f<a>> paramProvider5)
  {
    return new m(parame, paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */