package com.truecaller.messaging.data;

import android.content.ContentResolver;
import com.truecaller.featuretoggles.e;
import dagger.a.d;
import javax.inject.Provider;

public final class r
  implements d<q>
{
  private final Provider<ContentResolver> a;
  private final Provider<c> b;
  private final Provider<e> c;
  private final Provider<ab> d;
  
  private r(Provider<ContentResolver> paramProvider, Provider<c> paramProvider1, Provider<e> paramProvider2, Provider<ab> paramProvider3)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static r a(Provider<ContentResolver> paramProvider, Provider<c> paramProvider1, Provider<e> paramProvider2, Provider<ab> paramProvider3)
  {
    return new r(paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.r
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */