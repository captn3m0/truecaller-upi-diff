package com.truecaller.messaging.data;

import c.n;
import com.truecaller.androidactors.w;
import com.truecaller.messaging.data.a.a;
import com.truecaller.messaging.data.a.c;
import com.truecaller.messaging.data.a.j;
import com.truecaller.messaging.data.types.Conversation;
import com.truecaller.messaging.data.types.Draft;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import java.util.List;
import org.a.a.b;

public abstract interface o
{
  public abstract w<Integer> a();
  
  public abstract w<a> a(int paramInt);
  
  public abstract w<Conversation> a(long paramLong);
  
  public abstract w<j> a(long paramLong, int paramInt1, int paramInt2, Integer paramInteger);
  
  public abstract w<a> a(Integer paramInteger);
  
  public abstract w<j> a(String paramString);
  
  public abstract w<Boolean> a(List<Long> paramList);
  
  public abstract w<Conversation> a(b paramb);
  
  public abstract w<Draft> a(Participant[] paramArrayOfParticipant, int paramInt);
  
  public abstract w<n<c, Long>> b();
  
  public abstract w<Message> b(long paramLong);
  
  public abstract w<Long> b(String paramString);
  
  public abstract w<String> c(long paramLong);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */