package com.truecaller.messaging.data;

import android.content.ContentResolver;
import javax.inject.Inject;

public final class y
  implements x
{
  private final ContentResolver a;
  private final c b;
  
  @Inject
  public y(ContentResolver paramContentResolver, c paramc)
  {
    a = paramContentResolver;
    b = paramc;
  }
  
  /* Error */
  @android.annotation.SuppressLint({"Recycle"})
  public final java.util.Map<com.truecaller.messaging.data.types.Conversation, java.util.List<com.truecaller.messaging.data.types.Message>> a(java.util.List<com.truecaller.messaging.data.types.Message> paramList)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 39
    //   3: invokestatic 20	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_1
    //   7: checkcast 41	java/lang/Iterable
    //   10: astore_1
    //   11: new 43	java/util/LinkedHashMap
    //   14: dup
    //   15: invokespecial 44	java/util/LinkedHashMap:<init>	()V
    //   18: checkcast 46	java/util/Map
    //   21: astore_3
    //   22: aload_1
    //   23: invokeinterface 50 1 0
    //   28: astore 4
    //   30: aload 4
    //   32: invokeinterface 56 1 0
    //   37: ifeq +73 -> 110
    //   40: aload 4
    //   42: invokeinterface 60 1 0
    //   47: astore 5
    //   49: aload 5
    //   51: checkcast 62	com/truecaller/messaging/data/types/Message
    //   54: getfield 65	com/truecaller/messaging/data/types/Message:b	J
    //   57: invokestatic 71	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   60: astore 6
    //   62: aload_3
    //   63: aload 6
    //   65: invokeinterface 75 2 0
    //   70: astore_2
    //   71: aload_2
    //   72: astore_1
    //   73: aload_2
    //   74: ifnonnull +21 -> 95
    //   77: new 77	java/util/ArrayList
    //   80: dup
    //   81: invokespecial 78	java/util/ArrayList:<init>	()V
    //   84: astore_1
    //   85: aload_3
    //   86: aload 6
    //   88: aload_1
    //   89: invokeinterface 82 3 0
    //   94: pop
    //   95: aload_1
    //   96: checkcast 84	java/util/List
    //   99: aload 5
    //   101: invokeinterface 88 2 0
    //   106: pop
    //   107: goto -77 -> 30
    //   110: aload_0
    //   111: getfield 27	com/truecaller/messaging/data/y:a	Landroid/content/ContentResolver;
    //   114: astore_1
    //   115: invokestatic 93	com/truecaller/content/TruecallerContract$g:a	()Landroid/net/Uri;
    //   118: astore_2
    //   119: new 95	java/lang/StringBuilder
    //   122: dup
    //   123: ldc 97
    //   125: invokespecial 100	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   128: astore 4
    //   130: aload 4
    //   132: aload_3
    //   133: invokeinterface 104 1 0
    //   138: checkcast 41	java/lang/Iterable
    //   141: aconst_null
    //   142: aconst_null
    //   143: aconst_null
    //   144: iconst_0
    //   145: aconst_null
    //   146: aconst_null
    //   147: bipush 63
    //   149: invokestatic 109	c/a/m:a	(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lc/g/a/b;I)Ljava/lang/String;
    //   152: invokevirtual 113	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   155: pop
    //   156: aload 4
    //   158: bipush 41
    //   160: invokevirtual 116	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   163: pop
    //   164: aload_1
    //   165: aload_2
    //   166: aconst_null
    //   167: aload 4
    //   169: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   172: aconst_null
    //   173: aconst_null
    //   174: invokevirtual 126	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   177: astore_1
    //   178: aload_0
    //   179: getfield 29	com/truecaller/messaging/data/y:b	Lcom/truecaller/messaging/data/c;
    //   182: aload_1
    //   183: invokeinterface 131 2 0
    //   188: astore_1
    //   189: aload_1
    //   190: ifnull +177 -> 367
    //   193: aload_1
    //   194: checkcast 133	android/database/Cursor
    //   197: astore 5
    //   199: aload 5
    //   201: checkcast 135	java/io/Closeable
    //   204: astore 4
    //   206: aconst_null
    //   207: astore_2
    //   208: aload_2
    //   209: astore_1
    //   210: new 77	java/util/ArrayList
    //   213: dup
    //   214: invokespecial 78	java/util/ArrayList:<init>	()V
    //   217: checkcast 137	java/util/Collection
    //   220: astore 6
    //   222: aload_2
    //   223: astore_1
    //   224: aload 5
    //   226: invokeinterface 140 1 0
    //   231: ifeq +26 -> 257
    //   234: aload_2
    //   235: astore_1
    //   236: aload 6
    //   238: aload 5
    //   240: checkcast 142	com/truecaller/messaging/data/a/a
    //   243: invokeinterface 145 1 0
    //   248: invokeinterface 146 2 0
    //   253: pop
    //   254: goto -32 -> 222
    //   257: aload_2
    //   258: astore_1
    //   259: aload 6
    //   261: checkcast 84	java/util/List
    //   264: astore_2
    //   265: aload 4
    //   267: aconst_null
    //   268: invokestatic 151	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   271: aload_2
    //   272: checkcast 41	java/lang/Iterable
    //   275: astore_1
    //   276: new 43	java/util/LinkedHashMap
    //   279: dup
    //   280: aload_1
    //   281: bipush 10
    //   283: invokestatic 154	c/a/m:a	(Ljava/lang/Iterable;I)I
    //   286: invokestatic 159	c/a/ag:a	(I)I
    //   289: bipush 16
    //   291: invokestatic 165	c/k/i:c	(II)I
    //   294: invokespecial 168	java/util/LinkedHashMap:<init>	(I)V
    //   297: checkcast 46	java/util/Map
    //   300: astore_2
    //   301: aload_1
    //   302: invokeinterface 50 1 0
    //   307: astore 4
    //   309: aload_2
    //   310: astore_1
    //   311: aload 4
    //   313: invokeinterface 56 1 0
    //   318: ifeq +53 -> 371
    //   321: aload 4
    //   323: invokeinterface 60 1 0
    //   328: astore_1
    //   329: aload_2
    //   330: aload_1
    //   331: checkcast 170	com/truecaller/messaging/data/types/Conversation
    //   334: getfield 172	com/truecaller/messaging/data/types/Conversation:a	J
    //   337: invokestatic 71	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   340: aload_1
    //   341: invokeinterface 82 3 0
    //   346: pop
    //   347: goto -38 -> 309
    //   350: astore_2
    //   351: goto +8 -> 359
    //   354: astore_2
    //   355: aload_2
    //   356: astore_1
    //   357: aload_2
    //   358: athrow
    //   359: aload 4
    //   361: aload_1
    //   362: invokestatic 151	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   365: aload_2
    //   366: athrow
    //   367: invokestatic 175	c/a/ag:a	()Ljava/util/Map;
    //   370: astore_1
    //   371: new 43	java/util/LinkedHashMap
    //   374: dup
    //   375: invokespecial 44	java/util/LinkedHashMap:<init>	()V
    //   378: astore_2
    //   379: aload_3
    //   380: invokeinterface 178 1 0
    //   385: invokeinterface 181 1 0
    //   390: astore_3
    //   391: aload_3
    //   392: invokeinterface 56 1 0
    //   397: ifeq +61 -> 458
    //   400: aload_3
    //   401: invokeinterface 60 1 0
    //   406: checkcast 183	java/util/Map$Entry
    //   409: astore 4
    //   411: aload_1
    //   412: aload 4
    //   414: invokeinterface 186 1 0
    //   419: checkcast 188	java/lang/Number
    //   422: invokevirtual 192	java/lang/Number:longValue	()J
    //   425: invokestatic 71	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   428: invokeinterface 195 2 0
    //   433: ifeq -42 -> 391
    //   436: aload_2
    //   437: aload 4
    //   439: invokeinterface 186 1 0
    //   444: aload 4
    //   446: invokeinterface 198 1 0
    //   451: invokevirtual 199	java/util/LinkedHashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   454: pop
    //   455: goto -64 -> 391
    //   458: aload_2
    //   459: checkcast 46	java/util/Map
    //   462: astore_3
    //   463: new 43	java/util/LinkedHashMap
    //   466: dup
    //   467: aload_3
    //   468: invokeinterface 203 1 0
    //   473: invokestatic 159	c/a/ag:a	(I)I
    //   476: invokespecial 168	java/util/LinkedHashMap:<init>	(I)V
    //   479: checkcast 46	java/util/Map
    //   482: astore_2
    //   483: aload_3
    //   484: invokeinterface 178 1 0
    //   489: checkcast 41	java/lang/Iterable
    //   492: invokeinterface 50 1 0
    //   497: astore_3
    //   498: aload_3
    //   499: invokeinterface 56 1 0
    //   504: ifeq +54 -> 558
    //   507: aload_3
    //   508: invokeinterface 60 1 0
    //   513: checkcast 183	java/util/Map$Entry
    //   516: astore 4
    //   518: aload_2
    //   519: aload_1
    //   520: aload 4
    //   522: invokeinterface 186 1 0
    //   527: checkcast 188	java/lang/Number
    //   530: invokevirtual 192	java/lang/Number:longValue	()J
    //   533: invokestatic 71	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   536: invokestatic 206	c/a/ag:b	(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
    //   539: checkcast 170	com/truecaller/messaging/data/types/Conversation
    //   542: aload 4
    //   544: invokeinterface 198 1 0
    //   549: invokeinterface 82 3 0
    //   554: pop
    //   555: goto -57 -> 498
    //   558: aload_2
    //   559: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	560	0	this	y
    //   0	560	1	paramList	java.util.List<com.truecaller.messaging.data.types.Message>
    //   70	260	2	localObject1	Object
    //   350	1	2	localObject2	Object
    //   354	12	2	localThrowable	Throwable
    //   378	181	2	localObject3	Object
    //   21	487	3	localObject4	Object
    //   28	515	4	localObject5	Object
    //   47	192	5	localObject6	Object
    //   60	200	6	localObject7	Object
    // Exception table:
    //   from	to	target	type
    //   210	222	350	finally
    //   224	234	350	finally
    //   236	254	350	finally
    //   259	265	350	finally
    //   357	359	350	finally
    //   210	222	354	java/lang/Throwable
    //   224	234	354	java/lang/Throwable
    //   236	254	354	java/lang/Throwable
    //   259	265	354	java/lang/Throwable
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.y
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */