package com.truecaller.messaging.data;

import android.content.Context;
import dagger.a.d;
import java.io.File;
import javax.inject.Provider;

public final class f
  implements d<File>
{
  private final e a;
  private final Provider<Context> b;
  
  private f(e parame, Provider<Context> paramProvider)
  {
    a = parame;
    b = paramProvider;
  }
  
  public static f a(e parame, Provider<Context> paramProvider)
  {
    return new f(parame, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */