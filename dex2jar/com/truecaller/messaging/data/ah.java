package com.truecaller.messaging.data;

import com.truecaller.androidactors.f;
import dagger.a.d;
import javax.inject.Provider;

public final class ah
  implements d<ag>
{
  private final Provider<f<t>> a;
  
  private ah(Provider<f<t>> paramProvider)
  {
    a = paramProvider;
  }
  
  public static ah a(Provider<f<t>> paramProvider)
  {
    return new ah(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.ah
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */