package com.truecaller.messaging.data;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d<f<o>>
{
  private final e a;
  private final Provider<o> b;
  private final Provider<i> c;
  
  private j(e parame, Provider<o> paramProvider, Provider<i> paramProvider1)
  {
    a = parame;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static j a(e parame, Provider<o> paramProvider, Provider<i> paramProvider1)
  {
    return new j(parame, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */