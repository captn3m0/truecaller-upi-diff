package com.truecaller.messaging.data;

import android.content.ContentResolver;
import com.truecaller.backup.bk;
import com.truecaller.featuretoggles.e;
import com.truecaller.filters.p;
import com.truecaller.messaging.categorizer.b;
import com.truecaller.messaging.h;
import com.truecaller.messaging.transport.i;
import com.truecaller.messaging.transport.im.as;
import com.truecaller.messaging.transport.m;
import com.truecaller.util.ch;
import com.truecaller.utils.s;
import java.io.File;
import javax.inject.Provider;

public final class w
  implements dagger.a.d<v>
{
  private final Provider<x> A;
  private final Provider<ContentResolver> a;
  private final Provider<File> b;
  private final Provider<File[]> c;
  private final Provider<h> d;
  private final Provider<c> e;
  private final Provider<o> f;
  private final Provider<com.truecaller.androidactors.f<as>> g;
  private final Provider<m> h;
  private final Provider<com.truecaller.messaging.transport.d> i;
  private final Provider<com.truecaller.androidactors.f<com.truecaller.messaging.notifications.a>> j;
  private final Provider<com.truecaller.messaging.transport.f> k;
  private final Provider<i> l;
  private final Provider<ae> m;
  private final Provider<p> n;
  private final Provider<com.truecaller.util.al> o;
  private final Provider<ch> p;
  private final Provider<com.truecaller.androidactors.f<com.truecaller.analytics.ae>> q;
  private final Provider<s> r;
  private final Provider<com.truecaller.messaging.c.a> s;
  private final Provider<ab> t;
  private final Provider<e> u;
  private final Provider<bk> v;
  private final Provider<b> w;
  private final Provider<com.truecaller.messaging.categorizer.d> x;
  private final Provider<al> y;
  private final Provider<com.truecaller.messaging.h.c> z;
  
  private w(Provider<ContentResolver> paramProvider, Provider<File> paramProvider1, Provider<File[]> paramProvider2, Provider<h> paramProvider3, Provider<c> paramProvider4, Provider<o> paramProvider5, Provider<com.truecaller.androidactors.f<as>> paramProvider6, Provider<m> paramProvider7, Provider<com.truecaller.messaging.transport.d> paramProvider8, Provider<com.truecaller.androidactors.f<com.truecaller.messaging.notifications.a>> paramProvider9, Provider<com.truecaller.messaging.transport.f> paramProvider10, Provider<i> paramProvider11, Provider<ae> paramProvider12, Provider<p> paramProvider13, Provider<com.truecaller.util.al> paramProvider14, Provider<ch> paramProvider15, Provider<com.truecaller.androidactors.f<com.truecaller.analytics.ae>> paramProvider16, Provider<s> paramProvider17, Provider<com.truecaller.messaging.c.a> paramProvider18, Provider<ab> paramProvider19, Provider<e> paramProvider20, Provider<bk> paramProvider21, Provider<b> paramProvider22, Provider<com.truecaller.messaging.categorizer.d> paramProvider23, Provider<al> paramProvider24, Provider<com.truecaller.messaging.h.c> paramProvider25, Provider<x> paramProvider26)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
    k = paramProvider10;
    l = paramProvider11;
    m = paramProvider12;
    n = paramProvider13;
    o = paramProvider14;
    p = paramProvider15;
    q = paramProvider16;
    r = paramProvider17;
    s = paramProvider18;
    t = paramProvider19;
    u = paramProvider20;
    v = paramProvider21;
    w = paramProvider22;
    x = paramProvider23;
    y = paramProvider24;
    z = paramProvider25;
    A = paramProvider26;
  }
  
  public static w a(Provider<ContentResolver> paramProvider, Provider<File> paramProvider1, Provider<File[]> paramProvider2, Provider<h> paramProvider3, Provider<c> paramProvider4, Provider<o> paramProvider5, Provider<com.truecaller.androidactors.f<as>> paramProvider6, Provider<m> paramProvider7, Provider<com.truecaller.messaging.transport.d> paramProvider8, Provider<com.truecaller.androidactors.f<com.truecaller.messaging.notifications.a>> paramProvider9, Provider<com.truecaller.messaging.transport.f> paramProvider10, Provider<i> paramProvider11, Provider<ae> paramProvider12, Provider<p> paramProvider13, Provider<com.truecaller.util.al> paramProvider14, Provider<ch> paramProvider15, Provider<com.truecaller.androidactors.f<com.truecaller.analytics.ae>> paramProvider16, Provider<s> paramProvider17, Provider<com.truecaller.messaging.c.a> paramProvider18, Provider<ab> paramProvider19, Provider<e> paramProvider20, Provider<bk> paramProvider21, Provider<b> paramProvider22, Provider<com.truecaller.messaging.categorizer.d> paramProvider23, Provider<al> paramProvider24, Provider<com.truecaller.messaging.h.c> paramProvider25, Provider<x> paramProvider26)
  {
    return new w(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10, paramProvider11, paramProvider12, paramProvider13, paramProvider14, paramProvider15, paramProvider16, paramProvider17, paramProvider18, paramProvider19, paramProvider20, paramProvider21, paramProvider22, paramProvider23, paramProvider24, paramProvider25, paramProvider26);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.w
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */