package com.truecaller.messaging.data;

import com.truecaller.androidactors.h;
import java.util.concurrent.TimeUnit;

public class MessagesDataService
  extends h
{
  private static final long b = TimeUnit.SECONDS.toMillis(30L);
  
  public MessagesDataService()
  {
    super("messages-storage", b, true);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.MessagesDataService
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */