package com.truecaller.messaging.data.types;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.Patterns;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.h.u;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.messaging.transport.im.a.i;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.c.a.a.a.k;

public class Participant
  implements Parcelable
{
  public static final Parcelable.Creator<Participant> CREATOR = new Participant.1();
  public static final Participant a;
  public final long b;
  public final int c;
  public final String d;
  public final String e;
  public final String f;
  public final String g;
  public final String h;
  public final long i;
  public final int j;
  public final boolean k;
  public final int l;
  public final String m;
  public final String n;
  public final int o;
  public final long p;
  public final int q;
  public final int r;
  public final String s;
  public final long t;
  private final int u;
  
  static
  {
    Participant.a locala = new Participant.a(3);
    e = "";
    a = locala.a();
  }
  
  private Participant(Parcel paramParcel)
  {
    b = paramParcel.readLong();
    c = paramParcel.readInt();
    d = paramParcel.readString();
    e = paramParcel.readString();
    f = paramParcel.readString();
    g = paramParcel.readString();
    i = paramParcel.readLong();
    h = paramParcel.readString();
    j = paramParcel.readInt();
    int i1 = paramParcel.readInt();
    boolean bool = true;
    if (i1 != 1) {
      bool = false;
    }
    k = bool;
    l = paramParcel.readInt();
    m = paramParcel.readString();
    n = paramParcel.readString();
    o = paramParcel.readInt();
    p = paramParcel.readLong();
    q = paramParcel.readInt();
    r = paramParcel.readInt();
    s = paramParcel.readString();
    t = paramParcel.readLong();
    u = org.c.a.a.a.a.aaf).a(c).a;
  }
  
  private Participant(Participant.a parama)
  {
    b = b;
    c = a;
    d = c;
    e = k.n(d);
    f = k.n(e);
    g = k.n(f);
    i = h;
    h = g;
    j = i;
    k = j;
    l = k;
    m = l;
    n = m;
    o = n;
    p = o;
    q = p;
    r = s;
    s = q;
    t = r;
    u = org.c.a.a.a.a.aaf).a(c).a;
  }
  
  public static Participant a(Contact paramContact, String paramString, u paramu)
  {
    Participant.a locala = new Participant.a(0);
    if (paramString != null)
    {
      e = paramString;
    }
    else
    {
      paramString = paramContact.r();
      if (paramString != null)
      {
        e = paramString.a();
        f = paramString.l();
      }
      else
      {
        AssertionUtil.reportThrowableButNeverCrash(new IllegalArgumentException("Normalized number cannot be null"));
      }
    }
    if ((paramu != null) && (k.b(f)) && (!k.d(e)))
    {
      paramString = paramu.e(e);
      if (!k.d(paramString)) {
        f = paramString;
      }
    }
    if (paramContact.k() != null) {
      h = paramContact.k().longValue();
    }
    if (!k.b(paramContact.s())) {
      l = paramContact.s();
    }
    paramContact = paramContact.a(true);
    if (paramContact != null) {
      m = paramContact.toString();
    }
    return locala.a();
  }
  
  public static Participant a(String paramString1, u paramu, String paramString2)
  {
    if (paramString1.indexOf('@') >= 0)
    {
      if (Patterns.EMAIL_ADDRESS.matcher(paramString1).matches())
      {
        paramu = new Participant.a(2);
        d = paramString1;
        e = paramString1;
        return paramu.a();
      }
      paramu = new Participant.a(1);
      d = paramString1;
      e = paramString1;
      return paramu.a();
    }
    return b(paramString1, paramu, paramString2);
  }
  
  private boolean a(int paramInt)
  {
    return (paramInt & r) != 0;
  }
  
  public static Participant[] a(Uri paramUri, u paramu, String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    Object localObject = paramUri.getScheme();
    if (("sms".equals(localObject)) || ("smsto".equals(localObject)))
    {
      localObject = paramUri.getSchemeSpecificPart();
      String str = paramUri.getQuery();
      boolean bool = TextUtils.isEmpty(str);
      int i1 = 0;
      paramUri = (Uri)localObject;
      if (!bool) {
        paramUri = ((String)localObject).substring(0, ((String)localObject).length() - str.length() - 1);
      }
      paramUri = k.a(paramUri, ",;");
      int i2 = paramUri.length;
      while (i1 < i2)
      {
        localObject = a(paramUri[i1], paramu, paramString);
        int i3 = c;
        if ((i3 == 0) || (i3 == 1)) {
          localArrayList.add(localObject);
        }
        i1 += 1;
      }
    }
    return (Participant[])localArrayList.toArray(new Participant[localArrayList.size()]);
  }
  
  public static Participant b(String paramString1, u paramu, String paramString2)
  {
    String str = paramu.c(paramString1, paramString2);
    if (str == null)
    {
      paramu = new Participant.a(1);
      e = paramString1;
    }
    else
    {
      paramString2 = new Participant.a(0);
      e = str;
      paramu = paramu.e(str);
      if (!k.d(paramu)) {
        f = paramu;
      }
      paramu = paramString2;
    }
    d = paramString1;
    return paramu.a();
  }
  
  private boolean m()
  {
    return (o & 0x20) == 32;
  }
  
  public final String a()
  {
    if ((com.truecaller.content.a.a.a(r, 64)) && (k.e(s)) && (TrueApp.y().a().aF().y().a()))
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(s);
      Object localObject;
      if (k.e(m))
      {
        localObject = new StringBuilder(" (");
        ((StringBuilder)localObject).append(m);
        ((StringBuilder)localObject).append(")");
        localObject = ((StringBuilder)localObject).toString();
      }
      else
      {
        localObject = "";
      }
      localStringBuilder.append((String)localObject);
      return localStringBuilder.toString();
    }
    if (k.e(m)) {
      return m;
    }
    return b();
  }
  
  public final boolean a(boolean paramBoolean)
  {
    return (j != 2) && (((k) && (paramBoolean)) || (j == 1));
  }
  
  @SuppressLint({"SwitchIntDef"})
  public final String b()
  {
    int i1 = c;
    if (i1 != 0) {
      switch (i1)
      {
      default: 
        return f;
      case 3: 
        return TrueApp.y().a().cp().a(f);
      }
    }
    return android.support.v4.e.a.a().a(f);
  }
  
  public final boolean c()
  {
    return (k.d(f)) || (k.a("insert-address-token", f));
  }
  
  public final boolean d()
  {
    return (o & 0x2) == 2;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public final boolean e()
  {
    return (c == 0) && ((o & 0x2) == 0) && (!TextUtils.isEmpty(m)) && (!a(1));
  }
  
  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof Participant)) {
      return false;
    }
    paramObject = (Participant)paramObject;
    return (c == c) && (f.equals(f));
  }
  
  public final boolean f()
  {
    return k.c(d);
  }
  
  public final boolean g()
  {
    int i1 = j;
    return (i1 != 2) && ((k) || (q >= 10) || (i1 == 1));
  }
  
  public final boolean h()
  {
    int i1 = c;
    if (i1 != 0) {
      return i1 == 1;
    }
    return true;
  }
  
  public int hashCode()
  {
    return u;
  }
  
  public final boolean i()
  {
    return (!d()) && (!a(2)) && (!m());
  }
  
  public final String j()
  {
    switch (c)
    {
    default: 
      AssertionUtil.OnlyInDebug.fail(new String[] { "Should never happen" });
      return "unknwon";
    case 4: 
      return "im_group";
    case 3: 
      return "tc";
    case 2: 
      return "email";
    case 1: 
      return "alphanum";
    }
    return "phone_number";
  }
  
  public final String k()
  {
    if ((c == 0) && (f.startsWith("+"))) {
      return f.substring(1);
    }
    return f;
  }
  
  public final Participant.a l()
  {
    return new Participant.a(this, (byte)0);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("{id : ");
    localStringBuilder.append(b);
    localStringBuilder.append(", type: ");
    localStringBuilder.append(j());
    localStringBuilder.append(", source : \"");
    localStringBuilder.append(o);
    localStringBuilder.append("\"}");
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.provideAs(TypeTransformer.java:780)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e1expr(TypeTransformer.java:496)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:713)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.enexpr(TypeTransformer.java:698)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:719)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.s1stmt(TypeTransformer.java:810)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.sxStmt(TypeTransformer.java:840)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:206)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.Participant
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */