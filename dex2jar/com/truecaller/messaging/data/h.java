package com.truecaller.messaging.data;

import android.content.Context;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.k;
import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d<i>
{
  private final e a;
  private final Provider<Context> b;
  private final Provider<k> c;
  
  private h(e parame, Provider<Context> paramProvider, Provider<k> paramProvider1)
  {
    a = parame;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static h a(e parame, Provider<Context> paramProvider, Provider<k> paramProvider1)
  {
    return new h(parame, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */