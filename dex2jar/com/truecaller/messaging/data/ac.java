package com.truecaller.messaging.data;

import c.g.b.k;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import javax.inject.Inject;

public final class ac
  implements ab
{
  private final e a;
  
  @Inject
  public ac(e parame)
  {
    a = parame;
  }
  
  private static StringBuilder a()
  {
    StringBuilder localStringBuilder = new StringBuilder("( ( spam_score<10 AND top_spammer_count = 0  AND blacklist_count = 0) OR white_list_count > 0)");
    k.a(localStringBuilder, "StringBuilder().append(\"…ST_COUNT).append(\" > 0)\")");
    return localStringBuilder;
  }
  
  public final String a(int paramInt)
  {
    Object localObject;
    if (a.w().a())
    {
      switch (paramInt)
      {
      default: 
        throw ((Throwable)new IllegalArgumentException("Unknown filter type ".concat(String.valueOf(paramInt))));
      case 4: 
        localObject = new StringBuilder("\n                (outgoing_message_count > 0\n                OR phonebook_count > 0\n                OR tc_group_id IS NOT NULL)\n                AND ");
        ((StringBuilder)localObject).append(a());
        ((StringBuilder)localObject).append("\n            ");
        localObject = (CharSequence)((StringBuilder)localObject).toString();
        break;
      case 3: 
        localObject = (CharSequence)"\n            split_criteria = 1\n            OR blacklist_count > 0\n            AND white_list_count = 0\n    ";
        break;
      case 2: 
        localObject = new StringBuilder("\n                outgoing_message_count = 0\n                AND phonebook_count = 0\n                AND tc_group_id IS NULL\n                AND latest_message_id IS NOT NULL\n                AND ");
        StringBuilder localStringBuilder = new StringBuilder("( blacklist_count= 0 OR white_list_count > 0 )");
        k.a(localStringBuilder, "StringBuilder().append(\"…T_COUNT).append(\" > 0 )\")");
        ((StringBuilder)localObject).append(localStringBuilder);
        ((StringBuilder)localObject).append("\n            ");
        localObject = (CharSequence)((StringBuilder)localObject).toString();
        break;
      case 1: 
        localObject = (CharSequence)"";
      }
      return localObject.toString();
    }
    switch (paramInt)
    {
    default: 
      throw ((Throwable)new IllegalArgumentException("Unknown filter type ".concat(String.valueOf(paramInt))));
    case 4: 
      localObject = new StringBuilder("\n                (outgoing_message_count > 0\n                OR phonebook_count > 0\n                OR tc_group_id IS NOT NULL)\n                AND ");
      ((StringBuilder)localObject).append(a());
      ((StringBuilder)localObject).append("\n            ");
      localObject = (CharSequence)((StringBuilder)localObject).toString();
      break;
    case 3: 
      localObject = new StringBuilder(" ( ( spam_score>=10 OR top_spammer_count > 0  OR blacklist_count > 0) AND white_list_count = 0 )");
      k.a(localObject, "StringBuilder(\" ( ( \")\n …T_COUNT).append(\" = 0 )\")");
      localObject = (CharSequence)localObject;
      break;
    case 2: 
      localObject = new StringBuilder("\n                outgoing_message_count = 0\n                AND phonebook_count = 0\n                AND tc_group_id IS NULL\n                AND ");
      ((StringBuilder)localObject).append(a());
      ((StringBuilder)localObject).append("\n            ");
      localObject = (CharSequence)((StringBuilder)localObject).toString();
      break;
    case 1: 
      localObject = (CharSequence)"";
    }
    return localObject.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.ac
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */