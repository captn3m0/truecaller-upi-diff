package com.truecaller.messaging.h;

import c.a.an;
import com.truecaller.androidactors.f;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.h;
import javax.inject.Inject;

public final class c
{
  public final f<t> a;
  public final h b;
  public final e c;
  
  @Inject
  public c(f<t> paramf, h paramh, e parame)
  {
    a = paramf;
    b = paramh;
    c = parame;
  }
  
  public final void a()
  {
    ((t)a.a()).a(false, an.a(Integer.valueOf(5)));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.h.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */