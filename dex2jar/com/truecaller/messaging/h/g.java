package com.truecaller.messaging.h;

import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d<f>
{
  private final Provider<c> a;
  
  private g(Provider<c> paramProvider)
  {
    a = paramProvider;
  }
  
  public static g a(Provider<c> paramProvider)
  {
    return new g(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.h.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */