package com.truecaller.messaging.h;

import com.truecaller.androidactors.f;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.h;
import javax.inject.Provider;

public final class d
  implements dagger.a.d<c>
{
  private final Provider<f<t>> a;
  private final Provider<h> b;
  private final Provider<e> c;
  
  private d(Provider<f<t>> paramProvider, Provider<h> paramProvider1, Provider<e> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static d a(Provider<f<t>> paramProvider, Provider<h> paramProvider1, Provider<e> paramProvider2)
  {
    return new d(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.h.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */