package com.truecaller.messaging.h;

import com.truecaller.androidactors.f;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.data.t;
import dagger.a.d;
import javax.inject.Provider;

public final class b
  implements d<a>
{
  private final Provider<e> a;
  private final Provider<f<t>> b;
  
  private b(Provider<e> paramProvider, Provider<f<t>> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static b a(Provider<e> paramProvider, Provider<f<t>> paramProvider1)
  {
    return new b(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.h.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */