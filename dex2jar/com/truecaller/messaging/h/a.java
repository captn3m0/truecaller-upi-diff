package com.truecaller.messaging.h;

import android.content.ContentValues;
import android.os.Handler;
import android.os.Looper;
import com.truecaller.androidactors.f;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.data.t;
import javax.inject.Inject;

public final class a
  implements com.truecaller.content.d.a
{
  final f<t> a;
  private final Handler b;
  private final Runnable c;
  private final e d;
  
  @Inject
  public a(e parame, f<t> paramf)
  {
    d = parame;
    a = paramf;
    b = new Handler(Looper.getMainLooper());
    c = ((Runnable)new a.a(this));
  }
  
  public final void a(ContentValues paramContentValues)
  {
    if (d.e().a())
    {
      if (paramContentValues != null)
      {
        paramContentValues = paramContentValues.getAsLong("timestamp");
        if (paramContentValues != null)
        {
          l = paramContentValues.longValue();
          break label40;
        }
      }
      long l = 0L;
      label40:
      if (l > 0L)
      {
        ((t)a.a()).a(5, new org.a.a.b(l), false);
        return;
      }
      b.removeCallbacks(c);
      b.postDelayed(c, 300L);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.h.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */