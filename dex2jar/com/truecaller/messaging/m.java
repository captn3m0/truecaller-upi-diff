package com.truecaller.messaging;

import com.truecaller.analytics.ae;
import com.truecaller.androidactors.f;
import com.truecaller.featuretoggles.e;
import com.truecaller.insights.core.d.a;
import com.truecaller.messaging.categorizer.b;
import com.truecaller.payments.j;
import javax.inject.Provider;

public final class m
  implements dagger.a.d<b>
{
  private final l a;
  private final Provider<e> b;
  private final Provider<com.truecaller.insights.core.smscategorizer.d> c;
  private final Provider<j> d;
  private final Provider<h> e;
  private final Provider<a> f;
  private final Provider<f<ae>> g;
  
  private m(l paraml, Provider<e> paramProvider, Provider<com.truecaller.insights.core.smscategorizer.d> paramProvider1, Provider<j> paramProvider2, Provider<h> paramProvider3, Provider<a> paramProvider4, Provider<f<ae>> paramProvider5)
  {
    a = paraml;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
    f = paramProvider4;
    g = paramProvider5;
  }
  
  public static m a(l paraml, Provider<e> paramProvider, Provider<com.truecaller.insights.core.smscategorizer.d> paramProvider1, Provider<j> paramProvider2, Provider<h> paramProvider3, Provider<a> paramProvider4, Provider<f<ae>> paramProvider5)
  {
    return new m(paraml, paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */