package com.truecaller.messaging.conversation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.j;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import com.truecaller.bp;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.a.a;
import com.truecaller.messaging.data.types.Conversation;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import java.util.ArrayList;

public class ConversationActivity
  extends AppCompatActivity
  implements f
{
  private k a;
  private g b;
  
  public final g a()
  {
    return b;
  }
  
  public void onBackPressed()
  {
    k localk = a;
    if ((localk == null) || (!b.k())) {
      super.onBackPressed();
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    setTheme(aresId);
    Intent localIntent = getIntent();
    int k = 0;
    int i = localIntent.getIntExtra("filter", 0);
    boolean bool = localIntent.hasExtra("conversation_id");
    bk.a locala = null;
    Object localObject3;
    if (bool)
    {
      long l = localIntent.getLongExtra("conversation_id", -1L);
      localObject2 = Long.valueOf(l);
      localObject1 = null;
      localObject3 = localObject1;
    }
    else if (localIntent.hasExtra("participants"))
    {
      localObject2 = localIntent.getParcelableArrayExtra("participants");
      if ((localObject2 != null) && (localObject2.length > 0))
      {
        localObject1 = new Participant[localObject2.length];
        System.arraycopy(localObject2, 0, localObject1, 0, localObject2.length);
      }
      else
      {
        localObject1 = null;
      }
      localObject2 = null;
      localObject3 = localObject1;
      localObject4 = localObject2;
      localObject1 = localObject2;
      localObject2 = localObject4;
    }
    else
    {
      localObject1 = (Conversation)localIntent.getParcelableExtra("conversation");
      int j = i;
      if (i == 0) {
        j = p;
      }
      AssertionUtil.isNotNull(localObject1, new String[0]);
      i = j;
      localObject2 = null;
      localObject3 = localObject2;
    }
    Object localObject4 = locala;
    if (localIntent.hasExtra("message_id")) {
      localObject4 = Long.valueOf(localIntent.getLongExtra("message_id", -1L));
    }
    locala = new bk.a((byte)0);
    c = ((bp)dagger.a.g.a(((com.truecaller.bk)getApplication()).a()));
    a = ((s)dagger.a.g.a(new s((Conversation)localObject1, (Participant[])localObject3, (Long)localObject2, (Long)localObject4, this, i)));
    b = ((a)dagger.a.g.a(new a(this)));
    dagger.a.g.a(a, s.class);
    dagger.a.g.a(b, a.class);
    dagger.a.g.a(c, bp.class);
    b = new bk(a, b, c, (byte)0);
    super.onCreate(paramBundle);
    if (paramBundle != null) {
      return;
    }
    Object localObject1 = new Bundle();
    paramBundle = localIntent;
    if (localIntent.hasExtra("send_intent")) {
      paramBundle = (Intent)localIntent.getParcelableExtra("send_intent");
    }
    Object localObject2 = com.truecaller.common.h.o.b(paramBundle);
    if (localObject2 != null) {
      ((Bundle)localObject1).putString("initial_content", (String)localObject2);
    }
    if (paramBundle.getType() != null)
    {
      i = k;
      if (Entity.a(paramBundle.getType())) {}
    }
    else
    {
      i = 1;
    }
    if (i != 0)
    {
      paramBundle = com.truecaller.common.h.o.a(paramBundle);
      if ((paramBundle != null) && (!paramBundle.isEmpty())) {
        ((Bundle)localObject1).putParcelableArrayList("initial_attachments", paramBundle);
      }
    }
    a = new k();
    a.setArguments((Bundle)localObject1);
    getSupportFragmentManager().a().b(16908290, a).c();
  }
  
  public void startActivity(Intent paramIntent)
  {
    if (com.truecaller.common.h.o.b(getApplicationContext(), paramIntent))
    {
      com.truecaller.common.h.o.a(getApplicationContext(), paramIntent, "android.intent.action.VIEW");
      super.startActivity(paramIntent);
      return;
    }
    Toast.makeText(getApplicationContext(), 2131887195, 1).show();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.ConversationActivity
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */