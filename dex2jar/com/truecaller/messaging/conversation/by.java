package com.truecaller.messaging.conversation;

import com.truecaller.common.account.r;
import com.truecaller.featuretoggles.e;
import dagger.a.d;
import javax.inject.Provider;

public final class by
  implements d<bx>
{
  private final Provider<e> a;
  private final Provider<r> b;
  
  private by(Provider<e> paramProvider, Provider<r> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static by a(Provider<e> paramProvider, Provider<r> paramProvider1)
  {
    return new by(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.by
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */