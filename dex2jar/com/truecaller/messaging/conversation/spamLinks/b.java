package com.truecaller.messaging.conversation.spamLinks;

import android.net.Uri;
import c.n.m;
import c.u;
import com.truecaller.messaging.conversation.a.b.f;
import com.truecaller.network.spamUrls.d;
import java.util.Iterator;
import java.util.Set;
import javax.inject.Inject;

public final class b
  implements a
{
  private final d a;
  
  @Inject
  public b(d paramd)
  {
    a = paramd;
  }
  
  private final boolean a(String paramString)
  {
    if (a.b().contains(paramString)) {
      return true;
    }
    Iterator localIterator = ((Iterable)a.c()).iterator();
    while (localIterator.hasNext()) {
      if (new c.n.k((String)localIterator.next()).a((CharSequence)paramString)) {
        return true;
      }
    }
    paramString = m.d(paramString, ".", "");
    int i;
    if (((CharSequence)paramString).length() == 0) {
      i = 1;
    } else {
      i = 0;
    }
    return (i == 0) && (a(paramString));
  }
  
  public final boolean a(f paramf)
  {
    c.g.b.k.b(paramf, "matchResult");
    paramf = Uri.parse(d);
    c.g.b.k.a(paramf, "uri");
    String str = paramf.getHost();
    if (str != null)
    {
      if (str != null)
      {
        str = str.toLowerCase();
        c.g.b.k.a(str, "(this as java.lang.String).toLowerCase()");
        if (str == null) {
          return false;
        }
        paramf = paramf.getScheme();
        int i;
        if ((paramf == null) || ((!c.g.b.k.a(paramf, "http")) && (!c.g.b.k.a(paramf, "https")))) {
          i = 0;
        } else {
          i = 1;
        }
        return (i != 0) && (!a(str));
      }
      throw new u("null cannot be cast to non-null type java.lang.String");
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.spamLinks.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */