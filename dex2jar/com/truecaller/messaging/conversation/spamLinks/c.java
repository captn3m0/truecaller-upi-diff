package com.truecaller.messaging.conversation.spamLinks;

import javax.inject.Provider;

public final class c
  implements dagger.a.d<b>
{
  private final Provider<com.truecaller.network.spamUrls.d> a;
  
  private c(Provider<com.truecaller.network.spamUrls.d> paramProvider)
  {
    a = paramProvider;
  }
  
  public static c a(Provider<com.truecaller.network.spamUrls.d> paramProvider)
  {
    return new c(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.spamLinks.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */