package com.truecaller.messaging.conversation.a.b;

import android.content.ContentResolver;
import android.text.SpannableString;
import android.text.style.URLSpan;
import c.g.a.m;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.e.a;
import com.truecaller.messaging.data.c;
import com.truecaller.messaging.h;
import com.truecaller.tracking.events.r.a;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.bn;
import org.apache.a.d.d;

public final class l
  implements k
{
  k.a a;
  Map<String, n> b;
  boolean c;
  final ContentResolver d;
  final c e;
  final c.d.f f;
  final com.truecaller.network.spamUrls.a g;
  private bn h;
  private final c.d.f i;
  private final h j;
  private final com.truecaller.messaging.conversation.spamLinks.a k;
  private final com.truecaller.analytics.b l;
  private final com.truecaller.androidactors.f<ae> m;
  
  @Inject
  public l(ContentResolver paramContentResolver, c paramc, @Named("UI") c.d.f paramf1, @Named("Async") c.d.f paramf2, com.truecaller.network.spamUrls.a parama, h paramh, com.truecaller.messaging.conversation.spamLinks.a parama1, com.truecaller.analytics.b paramb, com.truecaller.androidactors.f<ae> paramf)
  {
    d = paramContentResolver;
    e = paramc;
    i = paramf1;
    f = paramf2;
    g = parama;
    j = paramh;
    k = parama1;
    l = paramb;
    m = paramf;
    b = ((Map)new LinkedHashMap());
  }
  
  static <T> e.r<T> a(e.b<T> paramb)
  {
    try
    {
      paramb = paramb.c();
      return paramb;
    }
    catch (IOException|RuntimeException paramb) {}
    return null;
  }
  
  private final String b(n paramn)
  {
    int n = j.K();
    if (c > n) {
      return "spam";
    }
    return "notSpam";
  }
  
  public final int a(n paramn)
  {
    c.g.b.k.b(paramn, "spamReport");
    int n = j.K();
    if (c > n) {
      return a;
    }
    if (d == 1) {
      return 1;
    }
    return 0;
  }
  
  public final List<o> a(CharSequence paramCharSequence)
  {
    c.g.b.k.b(paramCharSequence, "text");
    SpannableString localSpannableString = SpannableString.valueOf(paramCharSequence);
    ArrayList localArrayList = new ArrayList();
    int i1 = paramCharSequence.length();
    int n = 0;
    URLSpan[] arrayOfURLSpan = (URLSpan[])localSpannableString.getSpans(0, i1, URLSpan.class);
    if (arrayOfURLSpan == null) {
      return (List)localArrayList;
    }
    i1 = arrayOfURLSpan.length;
    while (n < i1)
    {
      Object localObject1 = arrayOfURLSpan[n];
      int i2 = localSpannableString.getSpanStart(localObject1);
      int i3 = localSpannableString.getSpanEnd(localObject1);
      String str = paramCharSequence.subSequence(i2, i3).toString();
      c.g.b.k.a(localObject1, "urlSpan");
      localObject1 = ((URLSpan)localObject1).getURL();
      c.g.b.k.a(localObject1, "urlSpan.url");
      f localf = new f(i2, i3, str, (String)localObject1);
      if (k.a(localf))
      {
        if (c)
        {
          Map localMap = b;
          Object localObject2 = localMap.get(str);
          localObject1 = localObject2;
          if (localObject2 == null)
          {
            localObject1 = new n();
            localMap.put(str, localObject1);
          }
          localObject2 = (n)localObject1;
          localObject1 = localObject2;
          if (f + b <= System.currentTimeMillis())
          {
            f = System.currentTimeMillis();
            kotlinx.coroutines.e.b((ag)bg.a, i, (m)new l.g(this, str, (n)localObject2, null), 2);
            localObject1 = localObject2;
          }
        }
        else
        {
          localObject1 = new n();
        }
        localArrayList.add(new o(localf, (n)localObject1));
      }
      n += 1;
    }
    return (List)localArrayList;
  }
  
  public final void a()
  {
    if (!c) {
      kotlinx.coroutines.e.b((ag)bg.a, i, (m)new l.c(this, null), 2);
    }
  }
  
  public final void a(k.a parama)
  {
    a = parama;
  }
  
  public final void a(n paramn, String paramString1, String paramString2)
  {
    c.g.b.k.b(paramn, "spamReport");
    c.g.b.k.b(paramString1, "action");
    c.g.b.k.b(paramString2, "url");
    paramn = b(paramn);
    com.truecaller.analytics.e locale = new e.a("SpamLink").a("Action", paramString1).a("Type", paramn).a();
    com.truecaller.analytics.b localb = l;
    c.g.b.k.a(locale, "event");
    localb.b(locale);
    ((ae)m.a()).a((d)com.truecaller.tracking.events.r.b().a((CharSequence)paramString1).b((CharSequence)paramn).c((CharSequence)paramString2).a());
  }
  
  public final void a(o paramo)
  {
    c.g.b.k.b(paramo, "spamResult");
    String str = a.c;
    Object localObject = h;
    if ((localObject != null) && (((bn)localObject).c(null) == true))
    {
      localObject = (n)b.get(str);
      if (localObject != null)
      {
        if (d == 1) {
          a -= 1;
        }
        d = 0;
      }
      localObject = a;
      if (localObject != null) {
        ((k.a)localObject).onLinksLoaded();
      }
      localObject = (n)b.get(str);
      if (localObject == null) {
        return;
      }
      kotlinx.coroutines.e.b((ag)bg.a, f, (m)new l.f(this, str, (n)localObject, null), 2);
    }
    a(b, "undo", str);
  }
  
  public final void a(String paramString, boolean paramBoolean)
  {
    c.g.b.k.b(paramString, "url");
    n localn = (n)b.get(paramString);
    if (localn == null) {
      return;
    }
    int n;
    if (paramBoolean) {
      n = 1;
    } else {
      n = 2;
    }
    d = n;
    if (paramBoolean) {
      a += 1;
    }
    h = kotlinx.coroutines.e.b((ag)bg.a, f, (m)new l.e(this, paramString, localn, null), 2);
  }
  
  /* Error */
  public final boolean b()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 68	com/truecaller/messaging/conversation/a/b/l:d	Landroid/content/ContentResolver;
    //   4: invokestatic 170	com/truecaller/content/TruecallerContract$ak:a	()Landroid/net/Uri;
    //   7: aconst_null
    //   8: ldc_w 401
    //   11: iconst_2
    //   12: anewarray 403	java/lang/String
    //   15: dup
    //   16: iconst_0
    //   17: ldc_w 405
    //   20: aastore
    //   21: dup
    //   22: iconst_1
    //   23: ldc_w 405
    //   26: aastore
    //   27: aconst_null
    //   28: invokevirtual 409	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   31: astore 4
    //   33: new 200	java/util/ArrayList
    //   36: dup
    //   37: invokespecial 201	java/util/ArrayList:<init>	()V
    //   40: astore 6
    //   42: aload_0
    //   43: getfield 70	com/truecaller/messaging/conversation/a/b/l:e	Lcom/truecaller/messaging/data/c;
    //   46: aload 4
    //   48: invokeinterface 414 2 0
    //   53: astore 4
    //   55: aconst_null
    //   56: astore 5
    //   58: aload 4
    //   60: ifnull +110 -> 170
    //   63: aload 4
    //   65: checkcast 416	java/io/Closeable
    //   68: astore 7
    //   70: aload 5
    //   72: astore 4
    //   74: aload 7
    //   76: checkcast 418	com/truecaller/messaging/data/a/p
    //   79: astore 8
    //   81: aload 5
    //   83: astore 4
    //   85: aload 8
    //   87: invokeinterface 421 1 0
    //   92: ifeq +36 -> 128
    //   95: aload 5
    //   97: astore 4
    //   99: aload 8
    //   101: invokeinterface 424 1 0
    //   106: astore 9
    //   108: aload 9
    //   110: ifnull -29 -> 81
    //   113: aload 5
    //   115: astore 4
    //   117: aload 6
    //   119: aload 9
    //   121: invokevirtual 294	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   124: pop
    //   125: goto -44 -> 81
    //   128: aload 5
    //   130: astore 4
    //   132: getstatic 429	c/x:a	Lc/x;
    //   135: astore 5
    //   137: aload 7
    //   139: aconst_null
    //   140: invokestatic 434	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   143: goto +27 -> 170
    //   146: astore 5
    //   148: goto +12 -> 160
    //   151: astore 5
    //   153: aload 5
    //   155: astore 4
    //   157: aload 5
    //   159: athrow
    //   160: aload 7
    //   162: aload 4
    //   164: invokestatic 434	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   167: aload 5
    //   169: athrow
    //   170: aload 6
    //   172: checkcast 436	java/util/Collection
    //   175: invokeinterface 439 1 0
    //   180: iconst_1
    //   181: ixor
    //   182: ifeq +129 -> 311
    //   185: bipush 10
    //   187: aload 6
    //   189: invokevirtual 442	java/util/ArrayList:size	()I
    //   192: invokestatic 448	java/lang/Math:min	(II)I
    //   195: istore_1
    //   196: iconst_0
    //   197: istore_2
    //   198: iload_2
    //   199: iload_1
    //   200: if_icmpge +111 -> 311
    //   203: iload_1
    //   204: aload 6
    //   206: invokevirtual 442	java/util/ArrayList:size	()I
    //   209: if_icmpgt +102 -> 311
    //   212: aload 6
    //   214: iload_2
    //   215: iload_1
    //   216: invokevirtual 452	java/util/ArrayList:subList	(II)Ljava/util/List;
    //   219: astore 4
    //   221: aload 4
    //   223: ldc_w 454
    //   226: invokestatic 235	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   229: aload_0
    //   230: getfield 76	com/truecaller/messaging/conversation/a/b/l:g	Lcom/truecaller/network/spamUrls/a;
    //   233: aload 4
    //   235: invokeinterface 459 2 0
    //   240: invokestatic 461	com/truecaller/messaging/conversation/a/b/l:a	(Le/b;)Le/r;
    //   243: astore 5
    //   245: aload 5
    //   247: ifnonnull +5 -> 252
    //   250: iconst_0
    //   251: ireturn
    //   252: aload 5
    //   254: invokevirtual 465	e/r:d	()Z
    //   257: ifeq +52 -> 309
    //   260: getstatic 271	kotlinx/coroutines/bg:a	Lkotlinx/coroutines/bg;
    //   263: checkcast 273	kotlinx/coroutines/ag
    //   266: aload_0
    //   267: getfield 74	com/truecaller/messaging/conversation/a/b/l:f	Lc/d/f;
    //   270: new 467	com/truecaller/messaging/conversation/a/b/l$h
    //   273: dup
    //   274: aload_0
    //   275: aload 4
    //   277: aconst_null
    //   278: invokespecial 470	com/truecaller/messaging/conversation/a/b/l$h:<init>	(Lcom/truecaller/messaging/conversation/a/b/l;Ljava/util/List;Lc/d/c;)V
    //   281: checkcast 280	c/g/a/m
    //   284: iconst_2
    //   285: invokestatic 285	kotlinx/coroutines/e:b	(Lkotlinx/coroutines/ag;Lc/d/f;Lc/g/a/m;I)Lkotlinx/coroutines/bn;
    //   288: pop
    //   289: iload_1
    //   290: bipush 10
    //   292: iadd
    //   293: aload 6
    //   295: invokevirtual 442	java/util/ArrayList:size	()I
    //   298: invokestatic 448	java/lang/Math:min	(II)I
    //   301: istore_3
    //   302: iload_1
    //   303: istore_2
    //   304: iload_3
    //   305: istore_1
    //   306: goto -108 -> 198
    //   309: iconst_0
    //   310: ireturn
    //   311: iconst_1
    //   312: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	313	0	this	l
    //   195	111	1	n	int
    //   197	107	2	i1	int
    //   301	4	3	i2	int
    //   31	245	4	localObject1	Object
    //   56	80	5	localx	c.x
    //   146	1	5	localObject2	Object
    //   151	17	5	localThrowable	Throwable
    //   243	10	5	localr	e.r
    //   40	254	6	localArrayList	ArrayList
    //   68	93	7	localCloseable	java.io.Closeable
    //   79	21	8	localp	com.truecaller.messaging.data.a.p
    //   106	14	9	localUrlReportDto	com.truecaller.messaging.conversation.spamLinks.UrlReportDto
    // Exception table:
    //   from	to	target	type
    //   74	81	146	finally
    //   85	95	146	finally
    //   99	108	146	finally
    //   117	125	146	finally
    //   132	137	146	finally
    //   157	160	146	finally
    //   74	81	151	java/lang/Throwable
    //   85	95	151	java/lang/Throwable
    //   99	108	151	java/lang/Throwable
    //   117	125	151	java/lang/Throwable
    //   132	137	151	java/lang/Throwable
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.a.b.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */