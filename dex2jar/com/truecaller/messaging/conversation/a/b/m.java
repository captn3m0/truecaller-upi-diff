package com.truecaller.messaging.conversation.a.b;

import android.content.ContentResolver;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.b;
import com.truecaller.messaging.data.c;
import com.truecaller.messaging.h;
import dagger.a.d;
import javax.inject.Provider;

public final class m
  implements d<l>
{
  private final Provider<ContentResolver> a;
  private final Provider<c> b;
  private final Provider<c.d.f> c;
  private final Provider<c.d.f> d;
  private final Provider<com.truecaller.network.spamUrls.a> e;
  private final Provider<h> f;
  private final Provider<com.truecaller.messaging.conversation.spamLinks.a> g;
  private final Provider<b> h;
  private final Provider<com.truecaller.androidactors.f<ae>> i;
  
  private m(Provider<ContentResolver> paramProvider, Provider<c> paramProvider1, Provider<c.d.f> paramProvider2, Provider<c.d.f> paramProvider3, Provider<com.truecaller.network.spamUrls.a> paramProvider4, Provider<h> paramProvider5, Provider<com.truecaller.messaging.conversation.spamLinks.a> paramProvider6, Provider<b> paramProvider7, Provider<com.truecaller.androidactors.f<ae>> paramProvider8)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
  }
  
  public static m a(Provider<ContentResolver> paramProvider, Provider<c> paramProvider1, Provider<c.d.f> paramProvider2, Provider<c.d.f> paramProvider3, Provider<com.truecaller.network.spamUrls.a> paramProvider4, Provider<h> paramProvider5, Provider<com.truecaller.messaging.conversation.spamLinks.a> paramProvider6, Provider<b> paramProvider7, Provider<com.truecaller.androidactors.f<ae>> paramProvider8)
  {
    return new m(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.a.b.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */