package com.truecaller.messaging.conversation;

import com.truecaller.common.account.r;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import javax.inject.Inject;

public final class bx
  implements bw
{
  private final e a;
  private final r b;
  
  @Inject
  public bx(e parame, r paramr)
  {
    a = parame;
    b = paramr;
  }
  
  public final boolean a()
  {
    return (a.b().a()) && (b.c());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.bx
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */