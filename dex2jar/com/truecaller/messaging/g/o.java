package com.truecaller.messaging.g;

import com.truecaller.analytics.ap;
import com.truecaller.messaging.c.a;
import com.truecaller.messaging.transport.m;
import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d<n>
{
  private final Provider<m> a;
  private final Provider<a> b;
  private final Provider<ap> c;
  
  private o(Provider<m> paramProvider, Provider<a> paramProvider1, Provider<ap> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static o a(Provider<m> paramProvider, Provider<a> paramProvider1, Provider<ap> paramProvider2)
  {
    return new o(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.g.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */