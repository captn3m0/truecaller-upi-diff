package com.truecaller.messaging.g;

import com.truecaller.analytics.ae;
import com.truecaller.analytics.ap;
import com.truecaller.analytics.b;
import com.truecaller.androidactors.f;
import dagger.a.d;
import javax.inject.Provider;

public final class i
  implements d<ap>
{
  private final g a;
  private final Provider<f<ae>> b;
  private final Provider<b> c;
  
  private i(g paramg, Provider<f<ae>> paramProvider, Provider<b> paramProvider1)
  {
    a = paramg;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static i a(g paramg, Provider<f<ae>> paramProvider, Provider<b> paramProvider1)
  {
    return new i(paramg, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.g.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */