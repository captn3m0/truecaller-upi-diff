package com.truecaller.messaging.g;

import c.g.b.k;
import com.truecaller.analytics.ap;
import com.truecaller.androidactors.w;
import com.truecaller.messaging.c.a;
import com.truecaller.messaging.data.types.Draft;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;

public final class n
  implements l
{
  private final com.truecaller.messaging.transport.m a;
  private final a b;
  private final ap c;
  
  @Inject
  public n(com.truecaller.messaging.transport.m paramm, a parama, ap paramap)
  {
    a = paramm;
    b = parama;
    c = paramap;
  }
  
  public final w<c> a(List<Draft> paramList, String paramString1, boolean paramBoolean1, boolean paramBoolean2, String paramString2)
  {
    k.b(paramList, "draftsList");
    k.b(paramString1, "simToken");
    k.b(paramString2, "analyticsContext");
    if (paramList.isEmpty())
    {
      paramList = w.b(null);
      k.a(paramList, "Promise.wrap(null)");
      return paramList;
    }
    List localList = (List)new ArrayList();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      Draft localDraft = (Draft)localIterator.next();
      Object localObject1 = localDraft.a(paramString1);
      k.a(localObject1, "draft.buildMessage(simToken)");
      paramList = a;
      Object localObject2 = e;
      k.a(localObject2, "draft.media");
      if (localObject2.length == 0) {
        i = 1;
      } else {
        i = 0;
      }
      int i = paramList.a(i ^ 0x1, d, paramBoolean1 ^ true);
      int j = paramString2.hashCode();
      if (j != -1930796239)
      {
        if (j != 3625376)
        {
          if (j != 79840983)
          {
            if ((j == 108401386) && (paramString2.equals("reply")))
            {
              paramList = "Reply";
              break label257;
            }
          }
          else if (paramString2.equals("incallui"))
          {
            paramList = "InCallUI";
            break label257;
          }
        }
        else if (paramString2.equals("voip"))
        {
          paramList = "Voip";
          break label257;
        }
      }
      else if (paramString2.equals("forwardMessages"))
      {
        paramList = "Forward";
        break label257;
      }
      paramList = "UserInput";
      label257:
      localObject2 = b;
      String str = g;
      Participant[] arrayOfParticipant = d;
      k.a(arrayOfParticipant, "draft.participants");
      ((a)localObject2).a(str, paramString2, i, arrayOfParticipant, paramList);
      paramList = (Message)a.a((Message)localObject1, d, paramBoolean2, paramBoolean1 ^ true).d();
      localObject1 = a.a(i);
      k.a(localObject1, "transportManager.getTransport(transportType)");
      localObject1 = ((com.truecaller.messaging.transport.l)localObject1).a();
      k.a(localObject1, "transportManager.getTransport(transportType).name");
      localObject2 = c;
      str = g;
      k.a(str, "draft.analyticsId");
      arrayOfParticipant = d;
      k.a(arrayOfParticipant, "draft.participants");
      ((ap)localObject2).a(paramString2, str, (String)localObject1, arrayOfParticipant);
      if (paramList != null)
      {
        localList.add(new c.n(localDraft, paramList));
      }
      else
      {
        paramString1 = (Iterable)localList;
        paramList = (Collection)new ArrayList(c.a.m.a(paramString1, 10));
        paramString1 = paramString1.iterator();
        while (paramString1.hasNext()) {
          paramList.add((Message)nextb);
        }
        paramList = w.b(new c.a((List)paramList, localDraft));
        k.a(paramList, "Promise.wrap(DraftSchedu…ap { it.second }, draft))");
        return paramList;
      }
    }
    paramList = w.b(new c.b(localList));
    k.a(paramList, "Promise.wrap(DraftSchedu…heduledDraftMessageList))");
    return paramList;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.g.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */