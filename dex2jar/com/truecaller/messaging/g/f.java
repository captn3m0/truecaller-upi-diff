package com.truecaller.messaging.g;

import com.truecaller.messaging.transport.m;
import com.truecaller.util.bg;
import javax.inject.Provider;

public final class f
  implements dagger.a.d<e>
{
  private final Provider<com.truecaller.utils.d> a;
  private final Provider<com.truecaller.tcpermissions.l> b;
  private final Provider<bg> c;
  private final Provider<m> d;
  private final Provider<com.truecaller.androidactors.f<l>> e;
  private final Provider<com.truecaller.featuretoggles.e> f;
  
  private f(Provider<com.truecaller.utils.d> paramProvider, Provider<com.truecaller.tcpermissions.l> paramProvider1, Provider<bg> paramProvider2, Provider<m> paramProvider3, Provider<com.truecaller.androidactors.f<l>> paramProvider4, Provider<com.truecaller.featuretoggles.e> paramProvider5)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
  }
  
  public static f a(Provider<com.truecaller.utils.d> paramProvider, Provider<com.truecaller.tcpermissions.l> paramProvider1, Provider<bg> paramProvider2, Provider<m> paramProvider3, Provider<com.truecaller.androidactors.f<l>> paramProvider4, Provider<com.truecaller.featuretoggles.e> paramProvider5)
  {
    return new f(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.g.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */