package com.truecaller.messaging.g;

import c.g.b.k;
import c.m.i;
import c.n;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Draft;
import com.truecaller.messaging.data.types.Draft.a;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.util.bg;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;

public final class e
  implements d
{
  final bg a;
  final com.truecaller.featuretoggles.e b;
  private final com.truecaller.utils.d c;
  private final com.truecaller.tcpermissions.l d;
  private final com.truecaller.messaging.transport.m e;
  private final f<l> f;
  
  @Inject
  public e(com.truecaller.utils.d paramd, com.truecaller.tcpermissions.l paraml, bg parambg, com.truecaller.messaging.transport.m paramm, f<l> paramf, com.truecaller.featuretoggles.e parame)
  {
    c = paramd;
    d = paraml;
    a = parambg;
    e = paramm;
    f = paramf;
    b = parame;
  }
  
  private static b a(List<? extends n<Draft, ? extends Collection<? extends BinaryEntity>>> paramList)
  {
    Iterator localIterator = c.m.l.c(c.a.m.n((Iterable)paramList), (c.g.a.b)e.d.a).a();
    while (localIterator.hasNext())
    {
      paramList = localIterator.next();
      int j = ((Collection)paramList).size();
      int i = 1;
      if (j <= 1) {
        i = 0;
      }
      if (i != 0) {
        break label69;
      }
    }
    paramList = null;
    label69:
    if ((Collection)paramList != null) {
      return (b)b.g.a;
    }
    return null;
  }
  
  private static Collection<Draft> a(Collection<? extends BinaryEntity> paramCollection, Draft paramDraft)
  {
    Object localObject1 = (Iterable)paramCollection;
    paramCollection = (Collection)new ArrayList(c.a.m.a((Iterable)localObject1, 10));
    localObject1 = ((Iterable)localObject1).iterator();
    int i = 0;
    while (((Iterator)localObject1).hasNext())
    {
      Object localObject2 = ((Iterator)localObject1).next();
      if (i < 0) {
        c.a.m.a();
      }
      localObject2 = (BinaryEntity)localObject2;
      Draft.a locala = paramDraft.c();
      locala.a(-1L);
      locala.b();
      locala.a((Collection)c.a.m.a(localObject2));
      if (i != 0) {
        locala.a();
      }
      paramCollection.add(locala.d());
      i += 1;
    }
    return (Collection)paramCollection;
  }
  
  private static List<Draft> b(List<? extends n<Draft, ? extends Collection<? extends BinaryEntity>>> paramList)
  {
    List localList = (List)new ArrayList();
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      Object localObject2 = (n)paramList.next();
      Object localObject1 = (Draft)a;
      localObject2 = (Collection)b;
      if (((Collection)localObject2).size() > 1)
      {
        localList.addAll(a((Collection)localObject2, (Draft)localObject1));
      }
      else
      {
        localObject1 = ((Draft)localObject1).c().b().a((Collection)localObject2);
        localObject2 = ((Draft.a)localObject1).d();
        k.a(localObject2, "build()");
        localList.add(localObject2);
        k.a(localObject1, "draft.buildUpon()\n      …())\n                    }");
      }
    }
    return localList;
  }
  
  private final b c(List<n<Draft, Message>> paramList)
  {
    Iterator localIterator = c.m.l.a(c.a.m.n((Iterable)paramList), (c.g.a.b)e.a.a).a();
    while (localIterator.hasNext())
    {
      paramList = localIterator.next();
      Object localObject2 = (n)paramList;
      Object localObject1 = (Draft)a;
      localObject2 = (Message)b;
      localObject1 = e.b((Message)localObject2, d);
      k.a(localObject1, "transportManager.getSupp…sage, draft.participants)");
      int j = 0;
      int i = j;
      if (((List)localObject1).contains(Integer.valueOf(0)))
      {
        i = j;
        if (((List)localObject1).contains(Integer.valueOf(1))) {
          i = 1;
        }
      }
      if (i != 0) {
        break label137;
      }
    }
    paramList = null;
    label137:
    if ((n)paramList != null) {
      return (b)b.b.a;
    }
    return null;
  }
  
  public final w<c> a(b.e parame, boolean paramBoolean, String paramString)
  {
    k.b(parame, "preparationResult");
    k.b(paramString, "analyticsContext");
    return ((l)f.a()).a(a, b, c, paramBoolean, paramString);
  }
  
  public final b a(List<? extends n<Draft, ? extends Collection<? extends BinaryEntity>>> paramList, String paramString, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    k.b(paramList, "draftContent");
    k.b(paramString, "simToken");
    k.b(paramList, "draftsAndMedia");
    k.b(paramString, "simToken");
    AssertionUtil.OnlyInDebug.isTrue(((Collection)paramList).isEmpty() ^ true, new String[0]);
    Collection localCollection = null;
    if (!paramBoolean1)
    {
      if (!c.d()) {
        localObject1 = (b)b.d.a;
      } else if (!d.g()) {
        localObject1 = (b)b.c.a;
      } else {
        localObject1 = null;
      }
      if (localObject1 != null) {
        return (b)localObject1;
      }
    }
    if (!paramBoolean1)
    {
      localObject2 = c.a.m.n((Iterable)paramList).a();
      while (((Iterator)localObject2).hasNext())
      {
        localObject1 = ((Iterator)localObject2).next();
        if (((Draft)a).b()) {
          break label166;
        }
      }
      localObject1 = null;
      label166:
      if ((n)localObject1 != null)
      {
        localObject1 = (b)b.f.a;
        break label188;
      }
    }
    Object localObject1 = null;
    label188:
    if (localObject1 != null) {
      return (b)localObject1;
    }
    int i;
    if (!paramBoolean1) {
      i = 1;
    } else {
      i = 2;
    }
    long l = a.a(i);
    Object localObject2 = (Iterable)paramList;
    localObject1 = (Collection)new ArrayList();
    localObject2 = ((Iterable)localObject2).iterator();
    while (((Iterator)localObject2).hasNext()) {
      c.a.m.a((Collection)localObject1, (Iterable)nextb);
    }
    localObject2 = c.m.l.d(c.m.l.a(c.m.l.a(c.a.m.n((Iterable)localObject1), (c.g.a.b)e.b.a), (c.g.a.b)new e.c(this, l)));
    localObject1 = localCollection;
    if ((true ^ ((Collection)localObject2).isEmpty())) {
      localObject1 = (b)new b.a(l, (List)localObject2, i);
    }
    if (localObject1 != null) {
      return (b)localObject1;
    }
    if ((!paramBoolean1) && (!paramBoolean2))
    {
      localObject1 = a(paramList);
      if (localObject1 != null) {
        return (b)localObject1;
      }
    }
    paramList = b(paramList);
    localObject1 = (Iterable)paramList;
    localCollection = (Collection)new ArrayList(c.a.m.a((Iterable)localObject1, 10));
    localObject2 = ((Iterable)localObject1).iterator();
    while (((Iterator)localObject2).hasNext()) {
      localCollection.add(((Draft)((Iterator)localObject2).next()).a(paramString));
    }
    localObject1 = c.a.m.d((Iterable)localObject1, (Iterable)localCollection);
    if ((!paramBoolean1) && (!paramBoolean3))
    {
      localObject1 = c((List)localObject1);
      if (localObject1 != null) {
        return (b)localObject1;
      }
    }
    return (b)new b.e(paramList, paramString, paramBoolean1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.g.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */