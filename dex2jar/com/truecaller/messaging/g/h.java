package com.truecaller.messaging.g;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.k;
import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d<f<l>>
{
  private final g a;
  private final Provider<k> b;
  private final Provider<n> c;
  
  private h(g paramg, Provider<k> paramProvider, Provider<n> paramProvider1)
  {
    a = paramg;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static h a(g paramg, Provider<k> paramProvider, Provider<n> paramProvider1)
  {
    return new h(paramg, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.g.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */