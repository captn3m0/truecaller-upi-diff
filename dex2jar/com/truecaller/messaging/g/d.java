package com.truecaller.messaging.g;

import c.n;
import com.truecaller.androidactors.w;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Draft;
import java.util.Collection;
import java.util.List;

public abstract interface d
{
  public abstract w<c> a(b.e parame, boolean paramBoolean, String paramString);
  
  public abstract b a(List<? extends n<Draft, ? extends Collection<? extends BinaryEntity>>> paramList, String paramString, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.g.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */