package com.truecaller.messaging.categorizer;

import android.content.Context;
import android.os.Bundle;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.f;
import com.truecaller.bp;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.background.e.a;
import com.truecaller.featuretoggles.b;
import com.truecaller.messaging.data.t;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;

public final class UnclassifiedMessagesTask
  extends PersistentBackgroundTask
{
  @Inject
  public com.truecaller.featuretoggles.e a;
  
  public UnclassifiedMessagesTask()
  {
    TrueApp localTrueApp = TrueApp.y();
    k.a(localTrueApp, "TrueApp.getApp()");
    localTrueApp.a().a(this);
  }
  
  public final int a()
  {
    return 10032;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    k.b(paramContext, "context");
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    ((t)paramContext.a().p().a()).a();
    return PersistentBackgroundTask.RunResult.Success;
  }
  
  public final boolean a(Context paramContext)
  {
    k.b(paramContext, "context");
    paramContext = a;
    if (paramContext == null) {
      k.a("featuresRegistry");
    }
    return paramContext.w().a();
  }
  
  public final com.truecaller.common.background.e b()
  {
    com.truecaller.common.background.e locale = new e.a(1).a(12L, TimeUnit.HOURS).b(2L, TimeUnit.HOURS).b();
    k.a(locale, "TaskConfiguration.Builde…URS)\n            .build()");
    return locale;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.categorizer.UnclassifiedMessagesTask
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */