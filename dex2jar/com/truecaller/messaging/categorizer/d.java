package com.truecaller.messaging.categorizer;

import android.content.ContentResolver;
import c.d.f;
import com.truecaller.featuretoggles.e;
import com.truecaller.insights.core.b.c;
import com.truecaller.messaging.transport.i;
import javax.inject.Inject;
import javax.inject.Named;

public final class d
{
  final e a;
  final b b;
  final i c;
  final com.truecaller.insights.core.d.a d;
  final com.truecaller.insights.core.c.a e;
  private final ContentResolver f;
  private final c g;
  private final f h;
  
  @Inject
  public d(e parame, ContentResolver paramContentResolver, b paramb, i parami, com.truecaller.insights.core.d.a parama, c paramc, com.truecaller.insights.core.c.a parama1, @Named("CPU") f paramf)
  {
    a = parame;
    f = paramContentResolver;
    b = paramb;
    c = parami;
    d = parama;
    g = paramc;
    e = parama1;
    h = paramf;
  }
  
  /* Error */
  public final java.util.ArrayList<android.content.ContentProviderOperation> a(java.util.Set<Long> paramSet)
  {
    // Byte code:
    //   0: aload_0
    //   1: astore 11
    //   3: aload_1
    //   4: ldc 74
    //   6: invokestatic 33	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   9: new 76	java/util/ArrayList
    //   12: dup
    //   13: invokespecial 77	java/util/ArrayList:<init>	()V
    //   16: astore 10
    //   18: new 76	java/util/ArrayList
    //   21: dup
    //   22: invokespecial 77	java/util/ArrayList:<init>	()V
    //   25: astore 9
    //   27: aload 11
    //   29: getfield 62	com/truecaller/messaging/categorizer/d:g	Lcom/truecaller/insights/core/b/c;
    //   32: invokeinterface 82 1 0
    //   37: astore 16
    //   39: aload 11
    //   41: getfield 54	com/truecaller/messaging/categorizer/d:f	Landroid/content/ContentResolver;
    //   44: aload_1
    //   45: invokestatic 87	com/truecaller/content/TruecallerContract$ap:a	(Ljava/util/Set;)Landroid/net/Uri;
    //   48: aconst_null
    //   49: aconst_null
    //   50: aconst_null
    //   51: aconst_null
    //   52: invokevirtual 93	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   55: astore_1
    //   56: aload_1
    //   57: ifnull +315 -> 372
    //   60: aload_1
    //   61: checkcast 95	java/io/Closeable
    //   64: astore_1
    //   65: aload_1
    //   66: astore 11
    //   68: aload_1
    //   69: astore 12
    //   71: aload_1
    //   72: checkcast 97	android/database/Cursor
    //   75: astore 13
    //   77: aload_1
    //   78: astore 11
    //   80: aload_1
    //   81: astore 12
    //   83: aload 13
    //   85: invokeinterface 101 1 0
    //   90: istore 4
    //   92: iload 4
    //   94: ifeq +192 -> 286
    //   97: aload 13
    //   99: ldc 103
    //   101: invokestatic 108	com/truecaller/utils/extensions/k:b	(Landroid/database/Cursor;Ljava/lang/String;)I
    //   104: istore_2
    //   105: aload 13
    //   107: ldc 110
    //   109: invokestatic 113	com/truecaller/utils/extensions/k:c	(Landroid/database/Cursor;Ljava/lang/String;)J
    //   112: lstore 5
    //   114: aload 13
    //   116: ldc 115
    //   118: invokestatic 108	com/truecaller/utils/extensions/k:b	(Landroid/database/Cursor;Ljava/lang/String;)I
    //   121: istore_3
    //   122: aload 13
    //   124: ldc 117
    //   126: invokestatic 120	com/truecaller/utils/extensions/k:a	(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    //   129: astore 11
    //   131: aload 11
    //   133: astore 12
    //   135: aload 11
    //   137: ifnonnull +7 -> 144
    //   140: ldc 122
    //   142: astore 12
    //   144: aload 13
    //   146: ldc 124
    //   148: invokestatic 120	com/truecaller/utils/extensions/k:a	(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    //   151: astore 11
    //   153: aload 11
    //   155: astore 14
    //   157: aload 11
    //   159: ifnonnull +7 -> 166
    //   162: ldc 122
    //   164: astore 14
    //   166: aload 13
    //   168: ldc 126
    //   170: invokestatic 120	com/truecaller/utils/extensions/k:a	(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    //   173: astore 11
    //   175: aload 11
    //   177: astore 15
    //   179: aload 11
    //   181: ifnonnull +7 -> 188
    //   184: ldc 122
    //   186: astore 15
    //   188: aload 13
    //   190: ldc -128
    //   192: invokestatic 113	com/truecaller/utils/extensions/k:c	(Landroid/database/Cursor;Ljava/lang/String;)J
    //   195: lstore 7
    //   197: getstatic 133	kotlinx/coroutines/bg:a	Lkotlinx/coroutines/bg;
    //   200: checkcast 135	kotlinx/coroutines/ag
    //   203: astore 17
    //   205: aload_0
    //   206: getfield 66	com/truecaller/messaging/categorizer/d:h	Lc/d/f;
    //   209: astore 18
    //   211: aload_1
    //   212: astore 11
    //   214: aload 9
    //   216: aload 17
    //   218: aload 18
    //   220: new 137	com/truecaller/messaging/categorizer/d$a
    //   223: dup
    //   224: iload_2
    //   225: aload 12
    //   227: aload 15
    //   229: aload 14
    //   231: lload 5
    //   233: lload 7
    //   235: iload_3
    //   236: aconst_null
    //   237: aload_0
    //   238: aload 9
    //   240: aload 16
    //   242: invokespecial 140	com/truecaller/messaging/categorizer/d$a:<init>	(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJILc/d/c;Lcom/truecaller/messaging/categorizer/d;Ljava/util/ArrayList;Lcom/truecaller/insights/database/c;)V
    //   245: checkcast 142	c/g/a/m
    //   248: iconst_2
    //   249: invokestatic 147	kotlinx/coroutines/e:a	(Lkotlinx/coroutines/ag;Lc/d/f;Lc/g/a/m;I)Lkotlinx/coroutines/ao;
    //   252: invokevirtual 151	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   255: pop
    //   256: aload 11
    //   258: astore_1
    //   259: goto -182 -> 77
    //   262: astore 9
    //   264: aload 11
    //   266: astore_1
    //   267: goto +81 -> 348
    //   270: astore 10
    //   272: aload_1
    //   273: astore 9
    //   275: aload 10
    //   277: astore_1
    //   278: goto +40 -> 318
    //   281: astore 9
    //   283: goto +65 -> 348
    //   286: aload_1
    //   287: astore 11
    //   289: aload 13
    //   291: invokeinterface 154 1 0
    //   296: getstatic 159	c/x:a	Lc/x;
    //   299: astore 12
    //   301: aload 11
    //   303: aconst_null
    //   304: invokestatic 164	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   307: goto +65 -> 372
    //   310: astore 10
    //   312: aload_1
    //   313: astore 9
    //   315: aload 10
    //   317: astore_1
    //   318: aconst_null
    //   319: astore 10
    //   321: goto +42 -> 363
    //   324: astore 9
    //   326: aload 11
    //   328: astore_1
    //   329: goto +19 -> 348
    //   332: astore_1
    //   333: aconst_null
    //   334: astore 10
    //   336: aload 11
    //   338: astore 9
    //   340: goto +23 -> 363
    //   343: astore 9
    //   345: aload 12
    //   347: astore_1
    //   348: aload 9
    //   350: athrow
    //   351: astore 11
    //   353: aload 9
    //   355: astore 10
    //   357: aload_1
    //   358: astore 9
    //   360: aload 11
    //   362: astore_1
    //   363: aload 9
    //   365: aload 10
    //   367: invokestatic 164	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   370: aload_1
    //   371: athrow
    //   372: aload_0
    //   373: getfield 66	com/truecaller/messaging/categorizer/d:h	Lc/d/f;
    //   376: new 166	com/truecaller/messaging/categorizer/d$b
    //   379: dup
    //   380: aload_0
    //   381: aload 9
    //   383: aload 10
    //   385: aload 16
    //   387: aconst_null
    //   388: invokespecial 169	com/truecaller/messaging/categorizer/d$b:<init>	(Lcom/truecaller/messaging/categorizer/d;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/truecaller/insights/database/c;Lc/d/c;)V
    //   391: checkcast 142	c/g/a/m
    //   394: invokestatic 174	kotlinx/coroutines/f:a	(Lc/d/f;Lc/g/a/m;)Ljava/lang/Object;
    //   397: pop
    //   398: aload 10
    //   400: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	401	0	this	d
    //   0	401	1	paramSet	java.util.Set<Long>
    //   104	121	2	i	int
    //   121	115	3	j	int
    //   90	3	4	bool	boolean
    //   112	120	5	l1	long
    //   195	39	7	l2	long
    //   25	214	9	localArrayList1	java.util.ArrayList
    //   262	1	9	localThrowable1	Throwable
    //   273	1	9	localSet1	java.util.Set<Long>
    //   281	1	9	localThrowable2	Throwable
    //   313	1	9	localSet2	java.util.Set<Long>
    //   324	1	9	localThrowable3	Throwable
    //   338	1	9	localObject1	Object
    //   343	11	9	localThrowable4	Throwable
    //   358	24	9	localSet3	java.util.Set<Long>
    //   16	1	10	localArrayList2	java.util.ArrayList
    //   270	6	10	localObject2	Object
    //   310	6	10	localObject3	Object
    //   319	80	10	localObject4	Object
    //   1	336	11	localObject5	Object
    //   351	10	11	localObject6	Object
    //   69	277	12	localObject7	Object
    //   75	215	13	localCursor	android.database.Cursor
    //   155	75	14	localObject8	Object
    //   177	51	15	localObject9	Object
    //   37	349	16	localc	com.truecaller.insights.database.c
    //   203	14	17	localag	kotlinx.coroutines.ag
    //   209	10	18	localf	f
    // Exception table:
    //   from	to	target	type
    //   214	256	262	java/lang/Throwable
    //   97	131	270	finally
    //   144	153	270	finally
    //   166	175	270	finally
    //   188	211	270	finally
    //   97	131	281	java/lang/Throwable
    //   144	153	281	java/lang/Throwable
    //   166	175	281	java/lang/Throwable
    //   188	211	281	java/lang/Throwable
    //   214	256	310	finally
    //   289	301	310	finally
    //   289	301	324	java/lang/Throwable
    //   71	77	332	finally
    //   83	92	332	finally
    //   71	77	343	java/lang/Throwable
    //   83	92	343	java/lang/Throwable
    //   348	351	351	finally
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.categorizer.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */