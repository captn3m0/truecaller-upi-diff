package com.truecaller.messaging.categorizer;

import android.text.TextUtils;
import c.a.m;
import c.g.b.k;
import com.truecaller.analytics.ae;
import com.truecaller.androidactors.f;
import com.truecaller.common.h.ab;
import com.truecaller.insights.models.d.a;
import com.truecaller.insights.models.d.b;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.h;
import com.truecaller.payments.j;
import com.truecaller.tracking.events.ac;
import com.truecaller.tracking.events.ac.a;
import com.truecaller.tracking.events.al;
import com.truecaller.tracking.events.al.a;
import com.truecaller.tracking.events.ax;
import com.truecaller.tracking.events.ax.a;
import java.util.List;

public final class c
  implements b
{
  private final com.truecaller.featuretoggles.e a;
  private final com.truecaller.insights.core.smscategorizer.d b;
  private final j c;
  private final com.truecaller.insights.core.d.a d;
  private final h e;
  private final f<ae> f;
  
  public c(com.truecaller.featuretoggles.e parame, com.truecaller.insights.core.smscategorizer.d paramd, j paramj, com.truecaller.insights.core.d.a parama, h paramh, f<ae> paramf)
  {
    a = parame;
    b = paramd;
    c = paramj;
    d = parama;
    e = paramh;
    f = paramf;
  }
  
  private static ax a(Participant paramParticipant)
  {
    Object localObject = al.b().a(paramParticipant.d());
    boolean bool1 = TextUtils.isEmpty((CharSequence)m);
    boolean bool2 = true;
    localObject = ((al.a)localObject).b(bool1 ^ true);
    if (j == 1) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    localObject = ((al.a)localObject).a(Boolean.valueOf(bool1)).b(Boolean.valueOf(k));
    if (j == 2) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    localObject = ((al.a)localObject).c(Boolean.valueOf(bool1));
    if (q >= 10) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    localObject = ((al.a)localObject).d(Boolean.valueOf(bool1));
    if ((o & 0x40) != 0) {
      bool1 = bool2;
    } else {
      bool1 = false;
    }
    localObject = ((al.a)localObject).e(Boolean.valueOf(bool1)).a(Integer.valueOf(Math.max(0, q))).a();
    paramParticipant = ax.b().a((CharSequence)paramParticipant.j()).b((CharSequence)paramParticipant.k()).a((al)localObject).a();
    k.a(paramParticipant, "com.truecaller.tracking.…nfo)\n            .build()");
    return paramParticipant;
  }
  
  public final a a(Message paramMessage, com.truecaller.insights.models.d paramd)
  {
    k.b(paramMessage, "message");
    k.b(paramd, "insightsParseResponse");
    boolean bool1 = a.w().a();
    boolean bool3 = false;
    if (!bool1) {
      return new a(2, 0);
    }
    Object localObject1 = paramMessage.j();
    k.a(localObject1, "message.buildMessageText()");
    int i;
    if ((f & 0x1) != 0) {
      i = 1;
    } else {
      i = 0;
    }
    int j;
    if ((j != 0) && (j != 4)) {
      j = 0;
    } else {
      j = 1;
    }
    int k;
    if (((CharSequence)localObject1).length() == 0) {
      k = 1;
    } else {
      k = 0;
    }
    if ((k == 0) && (j != 0) && (i == 0))
    {
      if (e.a() != 0L) {
        bool1 = true;
      } else {
        bool1 = false;
      }
      ac.a locala = ac.b();
      Object localObject2 = b;
      String str = ab.c(c.e);
      k.a(str, "PhoneNumberUtils.stripAl…e.participant.rawAddress)");
      Object localObject3 = c;
      k.a(localObject3, "message.participant");
      localObject3 = m.a(a((Participant)localObject3));
      k.a(locala, "smsCategorizerCompareEvent");
      i = ((com.truecaller.insights.core.smscategorizer.d)localObject2).a(str, (String)localObject1, bool1, (List)localObject3, locala);
      if (i == 0) {
        c.a(paramMessage);
      }
      if (i == 1) {
        paramMessage = new a(3, 2);
      } else {
        paramMessage = new a(2, 2);
      }
      localObject1 = paramMessage;
      if (!(paramd instanceof d.a))
      {
        localObject2 = (d.b)paramd;
        boolean bool2 = bool3;
        paramd = paramMessage;
        if (a == 3)
        {
          bool2 = bool3;
          paramd = paramMessage;
          if (d.a((com.truecaller.insights.models.d)localObject2))
          {
            paramd = new a(2, 2);
            bool2 = true;
          }
        }
        localObject1 = paramd;
        if (bool1)
        {
          d.a(b);
          locala.b((CharSequence)b.a());
          locala.b();
          locala.a(bool2);
          ((ae)f.a()).a((org.apache.a.d.d)locala.c());
          localObject1 = paramd;
        }
      }
      return (a)localObject1;
    }
    return new a(2, 2);
  }
  
  public final void a(Message paramMessage, int paramInt1, int paramInt2)
  {
    k.b(paramMessage, "message");
    com.truecaller.insights.core.smscategorizer.d locald = b;
    String str = paramMessage.j();
    k.a(str, "message.buildMessageText()");
    if (paramInt1 == 3) {
      paramInt1 = 1;
    } else {
      paramInt1 = 2;
    }
    paramMessage = c;
    k.a(paramMessage, "message.participant");
    locald.a(str, paramInt1, paramInt2, m.a(a(paramMessage)));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.categorizer.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */