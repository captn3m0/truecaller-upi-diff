package com.truecaller.messaging.categorizer;

import android.content.ContentResolver;
import c.d.f;
import com.truecaller.insights.core.b.c;
import com.truecaller.messaging.transport.i;
import javax.inject.Provider;

public final class e
  implements dagger.a.d<d>
{
  private final Provider<com.truecaller.featuretoggles.e> a;
  private final Provider<ContentResolver> b;
  private final Provider<b> c;
  private final Provider<i> d;
  private final Provider<com.truecaller.insights.core.d.a> e;
  private final Provider<c> f;
  private final Provider<com.truecaller.insights.core.c.a> g;
  private final Provider<f> h;
  
  private e(Provider<com.truecaller.featuretoggles.e> paramProvider, Provider<ContentResolver> paramProvider1, Provider<b> paramProvider2, Provider<i> paramProvider3, Provider<com.truecaller.insights.core.d.a> paramProvider4, Provider<c> paramProvider5, Provider<com.truecaller.insights.core.c.a> paramProvider6, Provider<f> paramProvider7)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
  }
  
  public static e a(Provider<com.truecaller.featuretoggles.e> paramProvider, Provider<ContentResolver> paramProvider1, Provider<b> paramProvider2, Provider<i> paramProvider3, Provider<com.truecaller.insights.core.d.a> paramProvider4, Provider<c> paramProvider5, Provider<com.truecaller.insights.core.c.a> paramProvider6, Provider<f> paramProvider7)
  {
    return new e(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.categorizer.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */