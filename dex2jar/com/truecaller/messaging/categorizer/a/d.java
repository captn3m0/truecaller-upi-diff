package com.truecaller.messaging.categorizer.a;

import com.truecaller.featuretoggles.d.b;
import javax.inject.Inject;

public final class d
  implements d.b
{
  private final dagger.a<a> a;
  
  @Inject
  public d(dagger.a<a> parama)
  {
    a = parama;
  }
  
  public final String a()
  {
    return "featureSmsCategorizer";
  }
  
  public final void b()
  {
    ((a)a.get()).a(true);
  }
  
  public final void c()
  {
    ((a)a.get()).a(false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.categorizer.a.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */