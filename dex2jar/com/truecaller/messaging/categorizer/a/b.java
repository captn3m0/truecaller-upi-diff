package com.truecaller.messaging.categorizer.a;

import com.truecaller.androidactors.f;
import com.truecaller.messaging.data.t;
import dagger.a.d;
import javax.inject.Provider;

public final class b
  implements d<a>
{
  private final Provider<f<t>> a;
  
  private b(Provider<f<t>> paramProvider)
  {
    a = paramProvider;
  }
  
  public static b a(Provider<f<t>> paramProvider)
  {
    return new b(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.categorizer.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */