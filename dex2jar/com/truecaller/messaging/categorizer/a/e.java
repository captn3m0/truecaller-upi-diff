package com.truecaller.messaging.categorizer.a;

import javax.inject.Provider;

public final class e
  implements dagger.a.d<d>
{
  private final Provider<a> a;
  
  private e(Provider<a> paramProvider)
  {
    a = paramProvider;
  }
  
  public static e a(Provider<a> paramProvider)
  {
    return new e(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.categorizer.a.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */