package com.truecaller.messaging.c;

import com.truecaller.common.h.an;
import com.truecaller.multisim.h;
import javax.inject.Provider;

public final class c
  implements dagger.a.d<b>
{
  private final Provider<d> a;
  private final Provider<com.truecaller.analytics.b> b;
  private final Provider<h> c;
  private final Provider<an> d;
  
  private c(Provider<d> paramProvider, Provider<com.truecaller.analytics.b> paramProvider1, Provider<h> paramProvider2, Provider<an> paramProvider3)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static c a(Provider<d> paramProvider, Provider<com.truecaller.analytics.b> paramProvider1, Provider<h> paramProvider2, Provider<an> paramProvider3)
  {
    return new c(paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.c.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */