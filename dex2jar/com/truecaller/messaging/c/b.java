package com.truecaller.messaging.c;

import c.g.b.k;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.common.h.an;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.multisim.h;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;

public final class b
  implements a
{
  private final d a;
  private final com.truecaller.analytics.b b;
  private final h c;
  private final an d;
  
  @Inject
  public b(d paramd, com.truecaller.analytics.b paramb, h paramh, an paraman)
  {
    a = paramd;
    b = paramb;
    c = paramh;
    d = paraman;
  }
  
  private static String a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return "Unknown";
    case 2: 
      return "im";
    case 1: 
      return "mms";
    }
    return "sms";
  }
  
  private static String a(long paramLong)
  {
    int i = (int)TimeUnit.MILLISECONDS.toSeconds(paramLong);
    if ((i >= 0) && (2 >= i)) {
      return "0-2";
    }
    if ((2 <= i) && (5 >= i)) {
      return "2-5";
    }
    if ((5 <= i) && (10 >= i)) {
      return "5-10";
    }
    if ((10 <= i) && (20 >= i)) {
      return "10-20";
    }
    if ((20 <= i) && (30 >= i)) {
      return "20-30";
    }
    if ((30 <= i) && (40 >= i)) {
      return "30-40";
    }
    if ((40 <= i) && (50 >= i)) {
      return "40-50";
    }
    if ((50 <= i) && (60 >= i)) {
      return "50-60";
    }
    if ((60 <= i) && (90 >= i)) {
      return "60-90";
    }
    if ((90 <= i) && (120 >= i)) {
      return "90-120";
    }
    if ((120 <= i) && (180 >= i)) {
      return "120-180";
    }
    if ((180 <= i) && (240 >= i)) {
      return "180-240";
    }
    if ((240 <= i) && (480 >= i)) {
      return "240-480";
    }
    return ">480";
  }
  
  public final void a(String paramString)
  {
    if (paramString != null)
    {
      a.b(paramString);
      return;
    }
  }
  
  public final void a(String paramString, Message paramMessage, int paramInt)
  {
    k.b(paramString, "status");
    k.b(paramMessage, "message");
    Object localObject2 = new e.a("MessageSendResult").a("Type", a(paramInt)).a("Status", paramString);
    if (c.j()) {
      localObject1 = "Multi";
    } else {
      localObject1 = "Single";
    }
    localObject2 = ((e.a)localObject2).a("Sim", (String)localObject1).a("SimToken", l).a("MultiSimConfig", c.a()).a("RetryCount", t);
    k.a(localObject2, "AnalyticsEvent.Builder(A…OUNT, message.retryCount)");
    String str = o;
    if (str != null)
    {
      localObject1 = a;
      long l = d.a();
      k.a(str, "id");
      localObject1 = Long.valueOf(((d)localObject1).b(l, str));
      l = ((Number)localObject1).longValue();
      int j = 1;
      int i;
      if (l > 0L) {
        i = 1;
      } else {
        i = 0;
      }
      if (i == 0) {
        localObject1 = null;
      }
      if (localObject1 != null) {
        ((e.a)localObject2).a("FullTimeInterval", a(((Number)localObject1).longValue()));
      }
      localObject1 = Long.valueOf(a.e(str));
      if (((Number)localObject1).longValue() > 0L) {
        i = 1;
      } else {
        i = 0;
      }
      if (i == 0) {
        localObject1 = null;
      }
      if (localObject1 != null) {
        ((e.a)localObject2).a("ScheduleTimeInterval", a(((Number)localObject1).longValue()));
      }
      localObject1 = Long.valueOf(a.f(str));
      if (((Number)localObject1).longValue() > 0L) {
        i = 1;
      } else {
        i = 0;
      }
      if (i == 0) {
        localObject1 = null;
      }
      if (localObject1 != null) {
        ((e.a)localObject2).a("EnqueueTimeInterval", a(((Number)localObject1).longValue()));
      }
      localObject1 = Long.valueOf(a.a(d.a(), str));
      if (((Number)localObject1).longValue() > 0L) {
        i = j;
      } else {
        i = 0;
      }
      if (i == 0) {
        localObject1 = null;
      }
      if (localObject1 != null)
      {
        l = ((Number)localObject1).longValue();
        ((e.a)localObject2).a("SendTimeInterval", a(l));
        ((e.a)localObject2).a(Double.valueOf(l));
      }
      a.g(str);
    }
    Object localObject1 = b;
    localObject2 = ((e.a)localObject2).a();
    k.a(localObject2, "event.build()");
    ((com.truecaller.analytics.b)localObject1).a((e)localObject2);
    if (paramInt == 2)
    {
      paramMessage = new e.a("IMMessage").a("Status", paramString).a("Action", "Sent").a("RetryCount", t);
      k.a(paramMessage, "AnalyticsEvent.Builder(A…OUNT, message.retryCount)");
      paramString = b;
      paramMessage = paramMessage.a();
      k.a(paramMessage, "imEvent.build()");
      paramString.a(paramMessage);
    }
  }
  
  public final void a(String paramString1, String paramString2, int paramInt, Participant[] paramArrayOfParticipant, String paramString3)
  {
    k.b(paramString2, "userInteraction");
    k.b(paramArrayOfParticipant, "participants");
    if (paramString1 != null) {
      a.a(paramString1);
    }
    int i;
    if (paramArrayOfParticipant.length == 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i == 0)
    {
      if (paramArrayOfParticipant.length > 1)
      {
        paramString1 = "Group";
        break label79;
      }
      if (paramArrayOfParticipant[0].d())
      {
        paramString1 = "Phonebook";
        break label79;
      }
    }
    paramString1 = "Unknown";
    label79:
    paramString1 = new e.a("MessageInitiated").a("Context", paramString2).a("Type", a(paramInt)).a("Participant", paramString1).a();
    paramString2 = b;
    k.a(paramString1, "event");
    paramString2.a(paramString1);
    if (paramInt == 2)
    {
      paramString1 = new e.a("IMMessage").a("Action", "Initiated");
      if (paramString3 != null) {
        paramString1.a("InitiatedVia", paramString3);
      }
      paramString2 = b;
      paramString1 = paramString1.a();
      k.a(paramString1, "imEvent.build()");
      paramString2.a(paramString1);
    }
  }
  
  public final void b(String paramString)
  {
    if (paramString != null)
    {
      a.c(paramString);
      return;
    }
  }
  
  public final void c(String paramString)
  {
    if (paramString != null)
    {
      a.d(paramString);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.c.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */