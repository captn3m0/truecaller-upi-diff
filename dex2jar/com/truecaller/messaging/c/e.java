package com.truecaller.messaging.c;

import c.g.b.k;
import com.truecaller.common.h.an;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.inject.Inject;

public final class e
  implements d
{
  private final Map<String, Long> a;
  private final Map<String, Long> b;
  private final Map<String, Long> c;
  private final Map<String, Long> d;
  private final an e;
  
  @Inject
  public e(an paraman)
  {
    e = paraman;
    a = ((Map)new LinkedHashMap());
    b = ((Map)new LinkedHashMap());
    c = ((Map)new LinkedHashMap());
    d = ((Map)new LinkedHashMap());
  }
  
  public final long a(long paramLong, String paramString)
  {
    k.b(paramString, "id");
    paramString = (Long)d.get(paramString);
    if (paramString != null) {
      return paramLong - paramString.longValue();
    }
    return 0L;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "id");
    a.put(paramString, Long.valueOf(e.a()));
  }
  
  public final long b(long paramLong, String paramString)
  {
    k.b(paramString, "id");
    paramString = (Long)a.get(paramString);
    if (paramString != null) {
      return paramLong - paramString.longValue();
    }
    return 0L;
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "id");
    b.put(paramString, Long.valueOf(e.a()));
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "id");
    c.put(paramString, Long.valueOf(e.a()));
  }
  
  public final void d(String paramString)
  {
    k.b(paramString, "id");
    d.put(paramString, Long.valueOf(e.a()));
  }
  
  public final long e(String paramString)
  {
    k.b(paramString, "id");
    Long localLong = (Long)a.get(paramString);
    if (localLong != null)
    {
      long l = localLong.longValue();
      paramString = (Long)b.get(paramString);
      if (paramString != null) {
        return paramString.longValue() - l;
      }
      return 0L;
    }
    return 0L;
  }
  
  public final long f(String paramString)
  {
    k.b(paramString, "id");
    Long localLong = (Long)b.get(paramString);
    if (localLong != null)
    {
      long l = localLong.longValue();
      paramString = (Long)c.get(paramString);
      if (paramString != null) {
        return paramString.longValue() - l;
      }
      return 0L;
    }
    return 0L;
  }
  
  public final void g(String paramString)
  {
    k.b(paramString, "id");
    a.remove(paramString);
    d.remove(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.c.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */