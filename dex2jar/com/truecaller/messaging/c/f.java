package com.truecaller.messaging.c;

import com.truecaller.common.h.an;
import dagger.a.d;
import javax.inject.Provider;

public final class f
  implements d<e>
{
  private final Provider<an> a;
  
  private f(Provider<an> paramProvider)
  {
    a = paramProvider;
  }
  
  public static f a(Provider<an> paramProvider)
  {
    return new f(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.c.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */