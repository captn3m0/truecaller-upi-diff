package com.truecaller.d;

import android.annotation.SuppressLint;
import c.g.b.k;
import c.m.l;
import c.u;
import com.truecaller.common.g.a;
import com.truecaller.common.h.i;
import com.truecaller.common.network.e.b;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.featuretoggles.f;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public final class c
  implements b
{
  private final com.truecaller.featuretoggles.e a;
  private final List<String> b;
  private final a c;
  
  public c(com.truecaller.featuretoggles.e parame, List<String> paramList, a parama)
  {
    a = parame;
    b = paramList;
    c = parama;
  }
  
  public final String a(com.truecaller.common.network.e parame, i parami)
  {
    k.b(parami, "crossDomainSupport");
    com.truecaller.featuretoggles.e locale = a;
    parame = parami.b(parame);
    if (parame != null) {
      parame = a;
    } else {
      parame = null;
    }
    if ((parame == null) || (d.a[parame.ordinal()] != 1)) {
      parame = ((f)D.a(locale, com.truecaller.featuretoggles.e.a[89])).e();
    } else {
      parame = ((f)E.a(locale, com.truecaller.featuretoggles.e.a[90])).e();
    }
    parame = l.d(l.b(l.c(c.a.m.n((Iterable)c.n.m.c((CharSequence)parame, new String[] { "," }, false, 6)), (c.g.a.b)c.a.a), (c.g.a.b)c.b.a));
    if (parame.isEmpty()) {
      parame = null;
    }
    if (parame != null)
    {
      parami = c.j.c.c;
      int i = parame.size();
      return (String)parame.get(c.j.c.d().b(i));
    }
    return null;
  }
  
  @SuppressLint({"WrongConstant"})
  public final boolean a()
  {
    if (c.a("qaEnableDomainFronting", false)) {
      return true;
    }
    Object localObject1 = a;
    localObject1 = ((f)F.a((com.truecaller.featuretoggles.e)localObject1, com.truecaller.featuretoggles.e.a[91])).e();
    Object localObject2 = Locale.ENGLISH;
    k.a(localObject2, "Locale.ENGLISH");
    if (localObject1 != null)
    {
      localObject1 = ((String)localObject1).toLowerCase((Locale)localObject2);
      k.a(localObject1, "(this as java.lang.String).toLowerCase(locale)");
      localObject2 = (Collection)c.n.m.c((CharSequence)localObject1, new String[] { "," }, false, 6);
      localObject1 = c.a("profileCountryIso");
      Object localObject3 = (Iterable)c.a.m.a((Collection)b, localObject1);
      localObject1 = (Collection)new ArrayList();
      localObject3 = ((Iterable)localObject3).iterator();
      Object localObject5;
      while (((Iterator)localObject3).hasNext())
      {
        localObject4 = ((Iterator)localObject3).next();
        localObject5 = (CharSequence)localObject4;
        int i;
        if ((localObject5 != null) && (!c.n.m.a((CharSequence)localObject5))) {
          i = 0;
        } else {
          i = 1;
        }
        if (i == 0) {
          ((Collection)localObject1).add(localObject4);
        }
      }
      localObject1 = (Iterable)localObject1;
      localObject3 = (Collection)new ArrayList(c.a.m.a((Iterable)localObject1, 10));
      Object localObject4 = ((Iterable)localObject1).iterator();
      while (((Iterator)localObject4).hasNext())
      {
        localObject1 = (String)((Iterator)localObject4).next();
        if (localObject1 != null)
        {
          localObject5 = Locale.ENGLISH;
          k.a(localObject5, "Locale.ENGLISH");
          if (localObject1 != null)
          {
            localObject1 = ((String)localObject1).toLowerCase((Locale)localObject5);
            k.a(localObject1, "(this as java.lang.String).toLowerCase(locale)");
          }
          else
          {
            throw new u("null cannot be cast to non-null type java.lang.String");
          }
        }
        else
        {
          localObject1 = null;
        }
        ((Collection)localObject3).add(localObject1);
      }
      return !((Collection)c.a.m.b((Iterable)localObject3, (Iterable)localObject2)).isEmpty();
    }
    throw new u("null cannot be cast to non-null type java.lang.String");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.d.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */