package com.truecaller;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.backup.BackupAgent;
import android.app.backup.BackupDataInput;
import android.app.backup.BackupDataOutput;
import android.app.backup.FullBackupDataOutput;
import android.content.Context;
import android.os.ParcelFileDescriptor;
import c.a.f;
import c.g.b.k;
import c.u;
import com.truecaller.backup.d;
import com.truecaller.common.g.a;
import com.truecaller.common.g.c;
import java.io.File;
import javax.inject.Inject;

public final class TruecallerBackupAgent
  extends BackupAgent
{
  @Inject
  public a a;
  
  private final void a(String paramString)
  {
    boolean bool3 = false;
    try
    {
      localObject1 = getFileStreamPath("account.v2.bak");
      k.a(localObject1, "getFileStreamPath(\"account.v2.bak\")");
      bool1 = ((File)localObject1).exists();
    }
    catch (SecurityException localSecurityException)
    {
      Object localObject1;
      boolean bool1;
      Object localObject2;
      Account localAccount;
      boolean bool2;
      Object localObject3;
      int i;
      for (;;) {}
    }
    bool1 = false;
    localObject1 = getSystemService("account");
    if (localObject1 != null)
    {
      localObject2 = (AccountManager)localObject1;
      localObject1 = ((AccountManager)localObject2).getAccountsByType(getString(2131887458));
      k.a(localObject1, "accountManager.getAccoun…henticator_account_type))");
      localAccount = (Account)f.c((Object[])localObject1);
      bool2 = bool3;
      if (localAccount != null)
      {
        localObject3 = ((AccountManager)localObject2).peekAuthToken(localAccount, "installation_id_backup");
        localObject1 = ((AccountManager)localObject2).getUserData(localAccount, "normalized_number_backup");
        localObject2 = ((AccountManager)localObject2).getUserData(localAccount, "country_code_backup");
        localObject3 = (CharSequence)localObject3;
        if ((localObject3 != null) && (((CharSequence)localObject3).length() != 0)) {
          i = 0;
        } else {
          i = 1;
        }
        bool2 = bool3;
        if (i == 0)
        {
          localObject1 = (CharSequence)localObject1;
          if ((localObject1 != null) && (((CharSequence)localObject1).length() != 0)) {
            i = 0;
          } else {
            i = 1;
          }
          bool2 = bool3;
          if (i == 0)
          {
            localObject1 = (CharSequence)localObject2;
            if ((localObject1 != null) && (((CharSequence)localObject1).length() != 0)) {
              i = 0;
            } else {
              i = 1;
            }
            bool2 = bool3;
            if (i == 0) {
              bool2 = true;
            }
          }
        }
      }
      localObject1 = a;
      if (localObject1 == null) {
        k.a("settings");
      }
      new d((a)localObject1).a(paramString, bool1, bool2);
      return;
    }
    throw new u("null cannot be cast to non-null type android.accounts.AccountManager");
  }
  
  public final void onBackup(ParcelFileDescriptor paramParcelFileDescriptor1, BackupDataOutput paramBackupDataOutput, ParcelFileDescriptor paramParcelFileDescriptor2)
  {
    paramParcelFileDescriptor1 = a;
    if (paramParcelFileDescriptor1 == null) {
      k.a("settings");
    }
    paramParcelFileDescriptor1.b("accountFileWasBackedUpByAutobackup", true);
    a("onBackup");
  }
  
  public final void onCreate()
  {
    super.onCreate();
    bf.a().a(new c((Context)this, "tc.settings")).a().a(this);
  }
  
  public final void onFullBackup(FullBackupDataOutput paramFullBackupDataOutput)
  {
    super.onFullBackup(paramFullBackupDataOutput);
    paramFullBackupDataOutput = a;
    if (paramFullBackupDataOutput == null) {
      k.a("settings");
    }
    paramFullBackupDataOutput.b("accountFileWasBackedUpByAutobackup", true);
    a("onFullBackup");
  }
  
  public final void onRestore(BackupDataInput paramBackupDataInput, int paramInt, ParcelFileDescriptor paramParcelFileDescriptor) {}
  
  public final void onRestoreFinished()
  {
    super.onRestoreFinished();
    a locala = a;
    if (locala == null) {
      k.a("settings");
    }
    locala.b("accountFileWasRestoredByAutobackup", true);
    a("onRestore");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.TruecallerBackupAgent
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */