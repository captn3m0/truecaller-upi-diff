package com.truecaller.engagementrewards;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import com.google.android.libraries.nbu.engagementrewards.api.EngagementRewardsClient;
import com.google.android.libraries.nbu.engagementrewards.external.EngagementRewardsHelper;
import com.google.android.libraries.nbu.engagementrewards.models.ClientInfo;
import com.google.android.libraries.nbu.engagementrewards.models.PhoneNumber;
import com.google.android.libraries.nbu.engagementrewards.models.PhoneNumber.Builder;
import com.google.android.libraries.nbu.engagementrewards.models.Promotion;
import com.google.android.libraries.nbu.engagementrewards.models.Reward;
import com.google.android.libraries.nbu.engagementrewards.models.UserInfo;
import com.google.android.libraries.nbu.engagementrewards.models.UserInfo.Builder;
import com.google.c.a.g;
import com.google.c.a.m.a;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.log.AssertionUtil;
import java.util.List;
import java.util.UUID;
import javax.inject.Inject;

public final class k
{
  private dagger.a<EngagementRewardsClient> a;
  private final com.truecaller.common.g.a b;
  private final com.google.c.a.k c;
  private final Context d;
  private final dagger.a<ClientInfo> e;
  private final e f;
  
  @Inject
  public k(dagger.a<EngagementRewardsClient> parama, com.truecaller.common.g.a parama1, com.google.c.a.k paramk, Context paramContext, dagger.a<ClientInfo> parama2, e parame)
  {
    a = parama;
    b = parama1;
    c = paramk;
    d = paramContext;
    e = parama2;
    f = parame;
  }
  
  public static String c()
  {
    return UUID.randomUUID().toString();
  }
  
  private ListenableFuture<List<Promotion>> d()
  {
    Object localObject1 = b.a("profileNumber");
    String str = b.a("profileCountryIso");
    try
    {
      localObject1 = c.a((CharSequence)localObject1, str);
      int i = b;
      long l = d;
      localObject1 = PhoneNumber.builder().setNationalNumber(l).setCountryCode(i).build();
    }
    catch (g localg)
    {
      AssertionUtil.reportThrowableButNeverCrash(localg);
      localObject2 = null;
    }
    Object localObject2 = UserInfo.builder().setPhoneNumber((PhoneNumber)localObject2).build();
    return Futures.transformAsync(((EngagementRewardsClient)a.get()).isUserPreviouslyAuthenticated(), new -..Lambda.k.UGImljRilGhGVzbf-QuS1LZMn0M(this, (UserInfo)localObject2), u.a());
  }
  
  public final ListenableFuture<Boolean> a()
  {
    return ((EngagementRewardsClient)a.get()).isUserPreviouslyAuthenticated();
  }
  
  public final ListenableFuture<Void> a(Account paramAccount)
  {
    return ((EngagementRewardsClient)a.get()).register(EngagementRewardsHelper.getInstance().getRegisterConfig(paramAccount, UserInfo.builder().build(), (ClientInfo)e.get()), f.as().a() ^ true);
  }
  
  public final ListenableFuture<Reward> a(String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder = new StringBuilder("getRegisteredPromotionsAndRedeem() called with: actionType = [");
    localStringBuilder.append(paramString1);
    localStringBuilder.append("], requestId = [");
    localStringBuilder.append(paramString2);
    localStringBuilder.append("]");
    localStringBuilder.toString();
    return Futures.transformAsync(Futures.transform(d(), new -..Lambda.k.xwaR9wFm1_MRp90jKyy77kxLpyY(this, paramString1), u.a()), new -..Lambda.k.YQJ-pEi7ECgrU8HB0Cd7BwMhjfY(this, paramString2), u.a());
  }
  
  public final void a(Activity paramActivity)
  {
    ((EngagementRewardsClient)a.get()).launchAuthenticationFlow(paramActivity, 333);
  }
  
  public final void a(FutureCallback<List<Promotion>> paramFutureCallback)
  {
    Futures.addCallback(d(), paramFutureCallback, u.b());
  }
  
  public final void b()
  {
    ((EngagementRewardsClient)a.get()).unregister();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */