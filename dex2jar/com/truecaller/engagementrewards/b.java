package com.truecaller.engagementrewards;

import com.truecaller.notificationchannels.e;
import dagger.a.d;
import javax.inject.Provider;

public final class b
  implements d<a>
{
  private final Provider<e> a;
  
  private b(Provider<e> paramProvider)
  {
    a = paramProvider;
  }
  
  public static b a(Provider<e> paramProvider)
  {
    return new b(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */