package com.truecaller.engagementrewards;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class q
  implements d<s>
{
  private final m a;
  private final Provider<Context> b;
  
  private q(m paramm, Provider<Context> paramProvider)
  {
    a = paramm;
    b = paramProvider;
  }
  
  public static q a(m paramm, Provider<Context> paramProvider)
  {
    return new q(paramm, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */