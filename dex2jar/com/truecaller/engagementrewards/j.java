package com.truecaller.engagementrewards;

import android.content.Context;
import com.google.android.libraries.nbu.engagementrewards.api.EngagementRewardsClient;
import com.google.android.libraries.nbu.engagementrewards.api.EngagementRewardsConstants.RewardsEnvironment;
import com.google.android.libraries.nbu.engagementrewards.api.impl.EngagementRewardsClientFactory;
import com.google.android.libraries.nbu.engagementrewards.external.EngagementRewardsHelper;
import com.google.android.libraries.nbu.engagementrewards.models.AndroidClient;
import com.google.android.libraries.nbu.engagementrewards.models.AndroidClient.Builder;
import com.google.android.libraries.nbu.engagementrewards.models.ClientInfo;
import com.google.android.libraries.nbu.engagementrewards.models.ClientInfo.Builder;
import com.google.android.libraries.nbu.engagementrewards.models.EngagementRewardsConfig;
import com.google.android.libraries.nbu.engagementrewards.models.EngagementRewardsConfig.Builder;
import com.truecaller.common.g.a;
import com.truecaller.old.data.access.Settings;
import com.truecaller.util.al;
import java.util.Locale;
import java.util.UUID;

public final class j
{
  private static EngagementRewardsClient a;
  private static final EngagementRewardsConstants.RewardsEnvironment b = EngagementRewardsConstants.RewardsEnvironment.FAKE_ENVIRONMENT;
  
  public static EngagementRewardsClient a(Context paramContext, al paramal, a parama)
  {
    if (a == null)
    {
      if ((paramal.e()) && (Settings.a("qaEngagementRewardEnv", "").equalsIgnoreCase(EngagementRewardsConstants.RewardsEnvironment.STAGING_ENVIRONMENT.name()))) {
        paramal = EngagementRewardsConstants.RewardsEnvironment.STAGING_ENVIRONMENT;
      } else {
        paramal = EngagementRewardsConstants.RewardsEnvironment.PROD_ENVIRONMENT;
      }
      "getClient:: Env: ".concat(String.valueOf(paramal));
      paramContext = paramContext.getApplicationContext();
      paramContext = EngagementRewardsConfig.builder().setApiKey("AIzaSyCd6tpLEKJi-5w6SDpTpzj6UTZpS47j7fw").setBackgroundExecutors(u.a()).setApplicationContext(paramContext).setRewardsEnvironment(paramal).build();
      a = EngagementRewardsHelper.getInstance().getEngagementRewardsFactory(paramContext).getInstance(a(parama));
    }
    return a;
  }
  
  public static ClientInfo a(a parama)
  {
    long l = parama.a("profileUserId", UUID.randomUUID().hashCode());
    "getClientInfo:: Client id: ".concat(String.valueOf(l));
    parama = AndroidClient.builder().setClientVersionCode(1L).setClientId(l).build();
    return ClientInfo.builder().setSponsorId("TRUECALLER").setLocale(Locale.getDefault().toString()).setAndroidClient(parama).build();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */