package com.truecaller.engagementrewards.ui;

import android.accounts.Account;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.nbu.engagementrewards.models.Promotion;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.truecaller.bb;
import com.truecaller.engagementrewards.EngagementRewardActionType;
import com.truecaller.engagementrewards.g;
import com.truecaller.engagementrewards.u;
import com.truecaller.utils.n;
import javax.inject.Inject;

public final class a
  extends bb<c>
{
  String a;
  Promotion c;
  final com.truecaller.engagementrewards.k d;
  final com.truecaller.engagementrewards.c e;
  final n f;
  final g g;
  
  @Inject
  public a(com.truecaller.engagementrewards.k paramk, com.truecaller.engagementrewards.c paramc, n paramn, g paramg)
  {
    d = paramk;
    e = paramc;
    f = paramn;
    g = paramg;
  }
  
  static Account a(Task<GoogleSignInAccount> paramTask)
  {
    try
    {
      paramTask = (GoogleSignInAccount)paramTask.a(ApiException.class);
      if (paramTask != null)
      {
        paramTask = paramTask.a();
        return paramTask;
      }
    }
    catch (ApiException paramTask)
    {
      StringBuilder localStringBuilder = new StringBuilder("signInResult:failed code=");
      localStringBuilder.append(paramTask.a());
      localStringBuilder.toString();
    }
    return null;
  }
  
  public final void a()
  {
    Futures.addCallback(d.a(), (FutureCallback)new a.f(this), u.b());
  }
  
  public final void a(c paramc)
  {
    c.g.b.k.b(paramc, "presenterView");
    super.a(paramc);
    if (!e.b())
    {
      paramc.finish();
      return;
    }
    d.a((FutureCallback)new a.a(this));
  }
  
  public final void b()
  {
    c localc = (c)b;
    if (localc != null) {
      localc.a(false);
    }
    Futures.addCallback(d.a(EngagementRewardActionType.BUY_PREMIUM_ANNUAL.name(), com.truecaller.engagementrewards.k.c()), (FutureCallback)new a.e(this), u.b());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.ui.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */