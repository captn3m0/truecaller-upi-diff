package com.truecaller.engagementrewards.ui;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Resources.Theme;
import android.os.Bundle;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import c.x;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.libraries.nbu.engagementrewards.api.Event;
import com.google.android.libraries.nbu.engagementrewards.models.Promotion;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.Futures.FutureCombiner;
import com.google.common.util.concurrent.ListenableFuture;
import com.truecaller.R.id;
import com.truecaller.TrueApp;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.engagementrewards.g;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import javax.inject.Inject;

public final class ClaimEngagementRewardsActivity
  extends AppCompatActivity
  implements DialogInterface.OnDismissListener, c
{
  public static final ClaimEngagementRewardsActivity.a c = new ClaimEngagementRewardsActivity.a((byte)0);
  @Inject
  public d a;
  @Inject
  public a b;
  private HashMap d;
  
  private View a(int paramInt)
  {
    if (d == null) {
      d = new HashMap();
    }
    View localView2 = (View)d.get(Integer.valueOf(paramInt));
    View localView1 = localView2;
    if (localView2 == null)
    {
      localView1 = findViewById(paramInt);
      d.put(Integer.valueOf(paramInt), localView1);
    }
    return localView1;
  }
  
  public final a a()
  {
    a locala = b;
    if (locala == null) {
      c.g.b.k.a("presenter");
    }
    return locala;
  }
  
  public final void a(c.g.a.a<x> parama)
  {
    c.g.b.k.b(parama, "onRetry");
    if (a == null) {
      c.g.b.k.a("engagementRewardPrompter");
    }
    d.a((Context)this, parama);
  }
  
  public final void a(com.truecaller.engagementrewards.k paramk)
  {
    c.g.b.k.b(paramk, "engagementRewardsManager");
    paramk.a((Activity)this);
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "text");
    TextView localTextView = (TextView)a(R.id.rewardText);
    c.g.b.k.a(localTextView, "rewardText");
    t.a((View)localTextView);
    localTextView = (TextView)a(R.id.rewardText);
    c.g.b.k.a(localTextView, "rewardText");
    localTextView.setText((CharSequence)paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject = (Button)a(R.id.ctaGetReward);
    c.g.b.k.a(localObject, "ctaGetReward");
    ((Button)localObject).setEnabled(paramBoolean);
    localObject = (Button)a(R.id.gotItButton);
    c.g.b.k.a(localObject, "gotItButton");
    ((Button)localObject).setEnabled(paramBoolean);
    localObject = (ProgressBar)a(R.id.progressBar);
    c.g.b.k.a(localObject, "progressBar");
    t.a((View)localObject, paramBoolean ^ true);
  }
  
  public final void b()
  {
    Object localObject = (TextView)a(R.id.rewardText);
    c.g.b.k.a(localObject, "rewardText");
    t.b((View)localObject);
    localObject = (Button)a(R.id.ctaGetReward);
    c.g.b.k.a(localObject, "ctaGetReward");
    t.b((View)localObject);
    localObject = (TextView)a(R.id.dialogText);
    c.g.b.k.a(localObject, "dialogText");
    ((TextView)localObject).setText((CharSequence)getString(2131887991));
    localObject = (TextView)a(R.id.dialogTitle);
    c.g.b.k.a(localObject, "dialogTitle");
    ((TextView)localObject).setText((CharSequence)getString(2131887992));
  }
  
  public final void b(c.g.a.a<x> parama)
  {
    c.g.b.k.b(parama, "onRetry");
    if (a == null) {
      c.g.b.k.a("engagementRewardPrompter");
    }
    d.b((Context)this, parama);
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "message");
    if (a == null) {
      c.g.b.k.a("engagementRewardPrompter");
    }
    d.a((Context)this, paramString);
  }
  
  public final void c()
  {
    if (a == null) {
      c.g.b.k.a("engagementRewardPrompter");
    }
    j localj = getSupportFragmentManager();
    c.g.b.k.a(localj, "supportFragmentManager");
    d.a(localj);
  }
  
  public final void d()
  {
    if (a == null) {
      c.g.b.k.a("engagementRewardPrompter");
    }
    d.a((Context)this);
  }
  
  public final void e()
  {
    if (a == null) {
      c.g.b.k.a("engagementRewardPrompter");
    }
    d.b((Context)this);
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    Object localObject1 = b;
    if (localObject1 == null) {
      c.g.b.k.a("presenter");
    }
    if (paramInt1 != 333) {
      return;
    }
    Object localObject2 = (c)b;
    if (localObject2 != null) {
      ((c)localObject2).a(true);
    }
    if (paramInt2 == 0)
    {
      d.b();
      paramIntent = g;
      localObject2 = c;
      localObject1 = a;
      paramIntent.a(Event.GOOGLE_ACCOUNT_AUTHENTICATION_FLOW_FAILED, (Promotion)localObject2, (String)localObject1);
      localObject3 = x.a;
      paramIntent.a("ErGoogleAuthFailed", (Promotion)localObject2, (String)localObject1);
      return;
    }
    paramIntent = GoogleSignIn.a(paramIntent);
    c.g.b.k.a(paramIntent, "GoogleSignIn.getSignedInAccountFromIntent(data)");
    paramIntent = a.a(paramIntent);
    if (paramIntent == null) {
      return;
    }
    localObject2 = new StringBuilder("onActivityResult:: Selected account: ");
    ((StringBuilder)localObject2).append(name);
    ((StringBuilder)localObject2).toString();
    localObject2 = g;
    Object localObject3 = c;
    String str = a;
    ((g)localObject2).a(Event.GOOGLE_ACCOUNT_AUTHENTICATION_FLOW_COMPLETED, (Promotion)localObject3, str);
    x localx = x.a;
    ((g)localObject2).a("ErGoogleAuthCompleted", (Promotion)localObject3, str);
    Futures.whenAllComplete(new ListenableFuture[] { d.a(paramIntent) }).run((Runnable)new a.d((a)localObject1), com.truecaller.engagementrewards.u.b());
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    com.truecaller.utils.extensions.a.a(this);
    getTheme().applyStyle(2131952145, false);
    paramBundle = TrueApp.x();
    if (paramBundle != null)
    {
      ((bk)paramBundle).a().a(this);
      setContentView(2131558582);
      paramBundle = b;
      if (paramBundle == null) {
        c.g.b.k.a("presenter");
      }
      a = getIntent().getStringExtra("ARG_SOURCE");
      paramBundle = b;
      if (paramBundle == null) {
        c.g.b.k.a("presenter");
      }
      paramBundle.a((c)this);
      ((Button)a(R.id.ctaGetReward)).setOnClickListener((View.OnClickListener)new ClaimEngagementRewardsActivity.b(this));
      a(false);
      ((Button)a(R.id.gotItButton)).setOnClickListener((View.OnClickListener)new ClaimEngagementRewardsActivity.c(this));
      return;
    }
    throw new c.u("null cannot be cast to non-null type com.truecaller.GraphHolder");
  }
  
  public final void onDismiss(DialogInterface paramDialogInterface)
  {
    paramDialogInterface = b;
    if (paramDialogInterface == null) {
      c.g.b.k.a("presenter");
    }
    paramDialogInterface = (c)b;
    if (paramDialogInterface != null)
    {
      paramDialogInterface.finish();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.ui.ClaimEngagementRewardsActivity
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */