package com.truecaller.engagementrewards.ui;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.graphics.BitmapFactory;
import android.support.v4.app.j;
import android.support.v4.app.z.d;
import android.support.v4.content.b;
import android.support.v7.app.AlertDialog.Builder;
import c.g.b.k;
import c.x;
import com.truecaller.notificationchannels.e;
import javax.inject.Inject;

public final class d
{
  private final Context a;
  private final e b;
  private final com.truecaller.notifications.a c;
  
  @Inject
  public d(Context paramContext, e parame, com.truecaller.notifications.a parama)
  {
    a = paramContext;
    b = parame;
    c = parama;
  }
  
  public static void a(Context paramContext)
  {
    k.b(paramContext, "context");
    new AlertDialog.Builder(paramContext).setTitle(2131887988).setMessage(2131887987).setNegativeButton(2131887218, (DialogInterface.OnClickListener)d.h.a).show();
  }
  
  public static void a(Context paramContext, c.g.a.a<x> parama)
  {
    k.b(paramContext, "context");
    k.b(parama, "onRetry");
    new AlertDialog.Builder(paramContext).setTitle(2131887994).setMessage(2131887993).setNegativeButton(2131887205, (DialogInterface.OnClickListener)d.f.a).setPositiveButton(2131887224, (DialogInterface.OnClickListener)new d.g(parama)).show();
  }
  
  public static void a(Context paramContext, String paramString)
  {
    k.b(paramContext, "context");
    new AlertDialog.Builder(paramContext).setTitle(2131887990).setMessage((CharSequence)paramContext.getString(2131887989, new Object[] { paramString })).setNegativeButton(2131887205, (DialogInterface.OnClickListener)d.e.a).show();
  }
  
  public static void a(j paramj)
  {
    k.b(paramj, "fragmentManager");
    new h().show(paramj, "RewardsRedeemCelebrationDialog");
  }
  
  public static void b(Context paramContext)
  {
    k.b(paramContext, "context");
    new AlertDialog.Builder(paramContext).setTitle(2131887992).setMessage(2131887991).setNegativeButton(2131887205, (DialogInterface.OnClickListener)d.i.a).show();
  }
  
  public static void b(Context paramContext, c.g.a.a<x> parama)
  {
    k.b(paramContext, "context");
    k.b(parama, "onRetry");
    new AlertDialog.Builder(paramContext).setTitle(2131887986).setMessage(2131887985).setNegativeButton(2131887205, (DialogInterface.OnClickListener)d.c.a).setPositiveButton(2131887224, (DialogInterface.OnClickListener)new d.d(parama)).show();
  }
  
  public static void c(Context paramContext, c.g.a.a<x> parama)
  {
    k.b(paramContext, "context");
    k.b(parama, "onAgree");
    new AlertDialog.Builder(paramContext).setTitle(2131887977).setMessage(2131887976).setPositiveButton(2131887193, (DialogInterface.OnClickListener)new d.a(parama)).setNegativeButton(2131887216, (DialogInterface.OnClickListener)d.b.a).show();
  }
  
  public final void a()
  {
    Object localObject2 = a;
    Object localObject1 = ClaimEngagementRewardsActivity.c;
    PendingIntent localPendingIntent = PendingIntent.getActivity((Context)localObject2, 0, ClaimEngagementRewardsActivity.a.a(a, "NOTIFICATION"), 134217728);
    String str1 = ((Context)localObject2).getString(2131887996);
    String str2 = ((Context)localObject2).getString(2131887995);
    localObject1 = b.k();
    if (localObject1 != null) {
      localObject1 = new z.d((Context)localObject2, (String)localObject1);
    } else {
      localObject1 = new z.d((Context)localObject2);
    }
    localObject1 = ((z.d)localObject1).a((CharSequence)str1).b((CharSequence)str2).f(b.c((Context)localObject2, 2131100594)).c(-1).a(BitmapFactory.decodeResource(((Context)localObject2).getResources(), 2131234704)).a(2131234787).a(localPendingIntent).e().h();
    localObject2 = c;
    k.a(localObject1, "notification");
    ((com.truecaller.notifications.a)localObject2).a(null, 2131362982, (Notification)localObject1, "notificationEngagementRewards", null);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.ui.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */