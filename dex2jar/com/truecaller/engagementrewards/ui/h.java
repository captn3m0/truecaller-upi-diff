package com.truecaller.engagementrewards.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.R.id;
import com.truecaller.TrueApp;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.engagementrewards.EngagementRewardActionType;
import com.truecaller.engagementrewards.c;
import com.truecaller.engagementrewards.e;
import com.truecaller.engagementrewards.s;
import java.util.HashMap;
import javax.inject.Inject;

public final class h
  extends AppCompatDialogFragment
{
  @Inject
  public ClipboardManager a;
  @Inject
  public c b;
  private DialogInterface.OnDismissListener c;
  private HashMap d;
  
  private View a(int paramInt)
  {
    if (d == null) {
      d = new HashMap();
    }
    View localView2 = (View)d.get(Integer.valueOf(paramInt));
    View localView1 = localView2;
    if (localView2 == null)
    {
      localView1 = getView();
      if (localView1 == null) {
        return null;
      }
      localView1 = localView1.findViewById(paramInt);
      d.put(Integer.valueOf(paramInt), localView1);
    }
    return localView1;
  }
  
  public final c a()
  {
    c localc = b;
    if (localc == null) {
      k.a("engagementRewardUtil");
    }
    return localc;
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof DialogInterface.OnDismissListener)) {
      c = ((DialogInterface.OnDismissListener)paramContext);
    }
  }
  
  public final void onCancel(DialogInterface paramDialogInterface)
  {
    super.onCancel(paramDialogInterface);
    DialogInterface.OnDismissListener localOnDismissListener = c;
    if (localOnDismissListener != null)
    {
      localOnDismissListener.onDismiss(paramDialogInterface);
      return;
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getDialog();
    if (paramBundle != null)
    {
      paramBundle = paramBundle.getWindow();
      if (paramBundle != null) {
        paramBundle.setBackgroundDrawableResource(17170445);
      }
    }
    setStyle(0, 2131952158);
    paramBundle = TrueApp.x();
    if (paramBundle != null)
    {
      ((bk)paramBundle).a().a(this);
      return;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.GraphHolder");
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558590, paramViewGroup, false);
  }
  
  public final void onDetach()
  {
    super.onDetach();
    c = null;
  }
  
  public final void onDismiss(DialogInterface paramDialogInterface)
  {
    super.onDismiss(paramDialogInterface);
    DialogInterface.OnDismissListener localOnDismissListener = c;
    if (localOnDismissListener != null)
    {
      localOnDismissListener.onDismiss(paramDialogInterface);
      return;
    }
  }
  
  public final void onStart()
  {
    super.onStart();
    Object localObject = getDialog();
    if (localObject != null)
    {
      localObject = ((Dialog)localObject).getWindow();
      if (localObject != null)
      {
        ((Window)localObject).setLayout(-1, -2);
        return;
      }
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    paramView = new DisplayMetrics();
    paramBundle = getContext();
    if (paramBundle != null)
    {
      paramBundle = ((Activity)paramBundle).getWindowManager();
      k.a(paramBundle, "(context as Activity).windowManager");
      paramBundle.getDefaultDisplay().getRealMetrics(paramView);
      double d1 = widthPixels;
      Double.isNaN(d1);
      paramView = (LinearLayout)a(R.id.container);
      k.a(paramView, "container");
      paramView.setMinimumWidth((int)(d1 * 0.6D));
      paramView = b;
      if (paramView == null) {
        k.a("engagementRewardUtil");
      }
      paramBundle = EngagementRewardActionType.BUY_PREMIUM_ANNUAL;
      k.b(paramBundle, "actionType");
      paramView = b.a(e.b(paramBundle));
      if (paramView != null)
      {
        k.a(paramView, "it");
        paramView = new f(paramView);
      }
      else
      {
        paramView = null;
      }
      if (paramView != null)
      {
        paramView = a;
        paramBundle = (TextView)a(R.id.couponCode);
        k.a(paramBundle, "couponCode");
        paramBundle.setText((CharSequence)paramView);
        paramBundle = (TextView)a(R.id.title);
        k.a(paramBundle, "title");
        paramBundle.setText((CharSequence)getString(2131887971));
        paramBundle = (TextView)a(R.id.subtitle);
        k.a(paramBundle, "subtitle");
        paramBundle.setText((CharSequence)getString(2131887970));
        ((Button)a(R.id.ctaButton)).setOnClickListener((View.OnClickListener)new h.a(paramView, this));
        return;
      }
      return;
    }
    throw new u("null cannot be cast to non-null type android.app.Activity");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.ui.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */