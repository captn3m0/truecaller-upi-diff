package com.truecaller.engagementrewards.ui;

import android.content.Context;
import com.truecaller.notifications.a;
import javax.inject.Provider;

public final class e
  implements dagger.a.d<d>
{
  private final Provider<Context> a;
  private final Provider<com.truecaller.notificationchannels.e> b;
  private final Provider<a> c;
  
  private e(Provider<Context> paramProvider, Provider<com.truecaller.notificationchannels.e> paramProvider1, Provider<a> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static e a(Provider<Context> paramProvider, Provider<com.truecaller.notificationchannels.e> paramProvider1, Provider<a> paramProvider2)
  {
    return new e(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.ui.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */