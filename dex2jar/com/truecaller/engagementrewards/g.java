package com.truecaller.engagementrewards;

import android.annotation.SuppressLint;
import android.os.Bundle;
import c.x;
import com.google.android.libraries.nbu.engagementrewards.api.EngagementRewardsClient;
import com.google.android.libraries.nbu.engagementrewards.api.Event;
import com.google.android.libraries.nbu.engagementrewards.api.Parameter;
import com.google.android.libraries.nbu.engagementrewards.models.Promotion;
import com.google.common.util.concurrent.FutureCallback;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import dagger.a;
import javax.inject.Inject;

public final class g
{
  private final a<EngagementRewardsClient> a;
  private final k b;
  private final b c;
  
  @Inject
  public g(a<EngagementRewardsClient> parama, k paramk, b paramb)
  {
    a = parama;
    b = paramk;
    c = paramb;
  }
  
  public final void a(Event paramEvent, Promotion paramPromotion, String paramString)
  {
    Bundle localBundle = new Bundle();
    String str3 = Parameter.PROMOTION_NAME.name();
    if (paramPromotion != null)
    {
      String str2 = paramPromotion.promotionCode();
      str1 = str2;
      if (str2 != null) {}
    }
    else
    {
      str1 = "NO_AVAILABLE_PROMOTIONS";
    }
    localBundle.putString(str3, str1);
    String str1 = Parameter.ACTION_NAME.name();
    if (paramPromotion != null) {
      paramPromotion = paramPromotion.actionType();
    } else {
      paramPromotion = null;
    }
    localBundle.putString(str1, paramPromotion);
    localBundle.putString(Parameter.ACTION_SOURCE.name(), paramString);
    ((EngagementRewardsClient)a.get()).logEvent(paramEvent, localBundle);
  }
  
  public final void a(Promotion paramPromotion, String paramString)
  {
    c.g.b.k.b(paramPromotion, "promotion");
    a(Event.PROMOTION_SEEN, paramPromotion, paramString);
    x localx = x.a;
    a("ErPromotionShown", paramPromotion, paramString);
  }
  
  @SuppressLint({"SyntheticAccessor"})
  public final void a(String paramString)
  {
    b.a((FutureCallback)new g.a(this, paramString));
  }
  
  public final void a(String paramString1, Promotion paramPromotion, String paramString2)
  {
    b localb = c;
    e.a locala = new e.a(paramString1);
    if (paramPromotion != null)
    {
      localObject = paramPromotion.promotionCode();
      paramString1 = (String)localObject;
      if (localObject != null) {}
    }
    else
    {
      paramString1 = "NOT_PRESENT";
    }
    Object localObject = locala.a("PromoId", paramString1);
    paramString1 = paramString2;
    if (paramString2 == null) {
      paramString1 = "NOT_PRESENT";
    }
    paramString2 = ((e.a)localObject).a("PromoSource", paramString1);
    if (paramPromotion != null)
    {
      paramPromotion = paramPromotion.actionType();
      paramString1 = paramPromotion;
      if (paramPromotion != null) {}
    }
    else
    {
      paramString1 = "NOT_PRESENT";
    }
    paramString1 = paramString2.a("ActionName", paramString1).a();
    c.g.b.k.a(paramString1, "AnalyticsEvent.Builder(e…\n                .build()");
    localb.b(paramString1);
  }
  
  public final void b(Promotion paramPromotion, String paramString)
  {
    a("ErPurchaseDone", paramPromotion, paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */