package com.truecaller.engagementrewards;

import com.google.android.libraries.nbu.engagementrewards.models.ClientInfo;
import com.truecaller.common.g.a;
import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d<ClientInfo>
{
  private final m a;
  private final Provider<a> b;
  
  private o(m paramm, Provider<a> paramProvider)
  {
    a = paramm;
    b = paramProvider;
  }
  
  public static o a(m paramm, Provider<a> paramProvider)
  {
    return new o(paramm, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */