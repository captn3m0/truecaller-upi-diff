package com.truecaller.engagementrewards;

import c.d.f;
import c.g.b.k;
import c.l;
import com.truecaller.featuretoggles.b;
import com.truecaller.old.data.access.Settings;
import com.truecaller.util.al;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

public final class c
{
  final com.truecaller.premium.a.d a;
  public final s b;
  private final com.truecaller.utils.d c;
  private final com.truecaller.featuretoggles.e d;
  private final com.truecaller.abtest.c e;
  private final al f;
  private final f g;
  
  @Inject
  public c(com.truecaller.premium.a.d paramd, s params, com.truecaller.utils.d paramd1, com.truecaller.featuretoggles.e parame, com.truecaller.abtest.c paramc, al paramal, @Named("IO") f paramf)
  {
    a = paramd;
    b = params;
    c = paramd1;
    d = parame;
    e = paramc;
    f = paramal;
    g = paramf;
  }
  
  private final boolean d()
  {
    return f.e();
  }
  
  public final EngagementRewardState a(EngagementRewardActionType paramEngagementRewardActionType)
  {
    k.b(paramEngagementRewardActionType, "actionType");
    Object localObject = b.c();
    int j = 0;
    if (localObject != null)
    {
      k.a(localObject, "it");
      if (((CharSequence)localObject).length() > 0) {
        i = 1;
      } else {
        i = 0;
      }
      if (i == 0) {
        localObject = null;
      }
      if (localObject != null)
      {
        k.a(localObject, "it");
        localEngagementRewardState = EngagementRewardState.valueOf((String)localObject);
        break label86;
      }
    }
    EngagementRewardState localEngagementRewardState = null;
    label86:
    int i = j;
    if (localEngagementRewardState != null)
    {
      i = j;
      if (localEngagementRewardState != EngagementRewardState.NONE)
      {
        i = j;
        if (d()) {
          i = 1;
        }
      }
    }
    if (i == 0) {
      localEngagementRewardState = null;
    }
    localObject = localEngagementRewardState;
    if (localEngagementRewardState == null)
    {
      paramEngagementRewardActionType = b.a(e.a(paramEngagementRewardActionType));
      if (paramEngagementRewardActionType != null)
      {
        k.a(paramEngagementRewardActionType, "it");
        if (!(c.n.m.a((CharSequence)paramEngagementRewardActionType) ^ true)) {
          paramEngagementRewardActionType = null;
        }
        if (paramEngagementRewardActionType != null)
        {
          k.a(paramEngagementRewardActionType, "it");
          paramEngagementRewardActionType = EngagementRewardState.valueOf(paramEngagementRewardActionType);
          localObject = paramEngagementRewardActionType;
          if (paramEngagementRewardActionType != null) {
            break label201;
          }
        }
      }
      localObject = EngagementRewardState.NONE;
    }
    label201:
    return (EngagementRewardState)localObject;
  }
  
  public final void a()
  {
    if (!b()) {
      return;
    }
    kotlinx.coroutines.e.b((ag)bg.a, g, (c.g.a.m)new c.a(this, null), 2);
  }
  
  public final void a(EngagementRewardActionType paramEngagementRewardActionType, EngagementRewardState paramEngagementRewardState)
  {
    k.b(paramEngagementRewardActionType, "actionType");
    k.b(paramEngagementRewardState, "state");
    "setRewardState:: State: ".concat(String.valueOf(paramEngagementRewardState));
    EngagementRewardState localEngagementRewardState = a(paramEngagementRewardActionType);
    switch (d.a[localEngagementRewardState.ordinal()])
    {
    default: 
      throw new l();
    case 6: 
      localEngagementRewardState = EngagementRewardState.NONE;
      break;
    case 5: 
      localEngagementRewardState = EngagementRewardState.COUPON_COPIED;
      break;
    case 4: 
      localEngagementRewardState = EngagementRewardState.REDEEMED;
      break;
    case 3: 
      localEngagementRewardState = EngagementRewardState.COMPLETED;
      break;
    case 2: 
      localEngagementRewardState = EngagementRewardState.PENDING;
      break;
    case 1: 
      localEngagementRewardState = EngagementRewardState.SHOWN;
    }
    if ((paramEngagementRewardState == EngagementRewardState.NONE) || (paramEngagementRewardState == localEngagementRewardState)) {
      b.a(e.a(paramEngagementRewardActionType), paramEngagementRewardState.name());
    }
  }
  
  public final boolean a(String paramString)
  {
    if (!c.n.m.a("truecaller_yearly_subscription", paramString, true)) {
      return (d()) && (Settings.b("qaEnableRewardForMonthlySubs", false));
    }
    return true;
  }
  
  public final boolean b()
  {
    return (f.a()) && (c.h() > 16) && (d.aq().a());
  }
  
  public final String c()
  {
    return e.a("erPromoImage_19751");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */