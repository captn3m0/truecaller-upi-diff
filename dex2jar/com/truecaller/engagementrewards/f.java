package com.truecaller.engagementrewards;

import com.truecaller.featuretoggles.e;
import com.truecaller.util.al;
import javax.inject.Provider;

public final class f
  implements dagger.a.d<c>
{
  private final Provider<com.truecaller.premium.a.d> a;
  private final Provider<s> b;
  private final Provider<com.truecaller.utils.d> c;
  private final Provider<e> d;
  private final Provider<com.truecaller.abtest.c> e;
  private final Provider<al> f;
  private final Provider<c.d.f> g;
  
  private f(Provider<com.truecaller.premium.a.d> paramProvider, Provider<s> paramProvider1, Provider<com.truecaller.utils.d> paramProvider2, Provider<e> paramProvider3, Provider<com.truecaller.abtest.c> paramProvider4, Provider<al> paramProvider5, Provider<c.d.f> paramProvider6)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
  }
  
  public static f a(Provider<com.truecaller.premium.a.d> paramProvider, Provider<s> paramProvider1, Provider<com.truecaller.utils.d> paramProvider2, Provider<e> paramProvider3, Provider<com.truecaller.abtest.c> paramProvider4, Provider<al> paramProvider5, Provider<c.d.f> paramProvider6)
  {
    return new f(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */