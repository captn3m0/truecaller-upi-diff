package com.truecaller.engagementrewards;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public final class u
{
  private static ListeningExecutorService a;
  private static Executor b;
  
  public static ListeningExecutorService a()
  {
    if (a == null) {
      a = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(4));
    }
    return a;
  }
  
  public static Executor b()
  {
    if (b == null) {
      b = new u.a();
    }
    return b;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.u
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */