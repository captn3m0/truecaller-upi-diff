package com.truecaller.engagementrewards;

import android.content.Context;
import com.google.android.libraries.nbu.engagementrewards.api.EngagementRewardsClient;
import com.google.android.libraries.nbu.engagementrewards.models.ClientInfo;
import com.truecaller.common.g.a;
import com.truecaller.featuretoggles.e;
import dagger.a.d;
import javax.inject.Provider;

public final class l
  implements d<k>
{
  private final Provider<EngagementRewardsClient> a;
  private final Provider<a> b;
  private final Provider<com.google.c.a.k> c;
  private final Provider<Context> d;
  private final Provider<ClientInfo> e;
  private final Provider<e> f;
  
  private l(Provider<EngagementRewardsClient> paramProvider, Provider<a> paramProvider1, Provider<com.google.c.a.k> paramProvider2, Provider<Context> paramProvider3, Provider<ClientInfo> paramProvider4, Provider<e> paramProvider5)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
  }
  
  public static l a(Provider<EngagementRewardsClient> paramProvider, Provider<a> paramProvider1, Provider<com.google.c.a.k> paramProvider2, Provider<Context> paramProvider3, Provider<ClientInfo> paramProvider4, Provider<e> paramProvider5)
  {
    return new l(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */