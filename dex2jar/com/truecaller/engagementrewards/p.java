package com.truecaller.engagementrewards;

import android.content.Context;
import com.google.android.libraries.nbu.engagementrewards.api.EngagementRewardsClient;
import com.truecaller.common.g.a;
import com.truecaller.util.al;
import javax.inject.Provider;

public final class p
  implements dagger.a.d<EngagementRewardsClient>
{
  private final m a;
  private final Provider<Context> b;
  private final Provider<com.truecaller.utils.d> c;
  private final Provider<al> d;
  private final Provider<a> e;
  
  private p(m paramm, Provider<Context> paramProvider, Provider<com.truecaller.utils.d> paramProvider1, Provider<al> paramProvider2, Provider<a> paramProvider3)
  {
    a = paramm;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
  }
  
  public static p a(m paramm, Provider<Context> paramProvider, Provider<com.truecaller.utils.d> paramProvider1, Provider<al> paramProvider2, Provider<a> paramProvider3)
  {
    return new p(paramm, paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */