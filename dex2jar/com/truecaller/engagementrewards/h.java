package com.truecaller.engagementrewards;

import com.google.android.libraries.nbu.engagementrewards.api.EngagementRewardsClient;
import com.truecaller.analytics.b;
import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d<g>
{
  private final Provider<EngagementRewardsClient> a;
  private final Provider<k> b;
  private final Provider<b> c;
  
  private h(Provider<EngagementRewardsClient> paramProvider, Provider<k> paramProvider1, Provider<b> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static h a(Provider<EngagementRewardsClient> paramProvider, Provider<k> paramProvider1, Provider<b> paramProvider2)
  {
    return new h(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */