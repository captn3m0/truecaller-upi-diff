package com.truecaller;

import com.truecaller.util.al;
import com.truecaller.util.cn;
import dagger.a.d;
import javax.inject.Provider;

public final class i
  implements d<com.truecaller.aftercall.a>
{
  private final c a;
  private final Provider<cn> b;
  private final Provider<com.truecaller.i.c> c;
  private final Provider<al> d;
  private final Provider<com.truecaller.utils.a> e;
  
  private i(c paramc, Provider<cn> paramProvider, Provider<com.truecaller.i.c> paramProvider1, Provider<al> paramProvider2, Provider<com.truecaller.utils.a> paramProvider3)
  {
    a = paramc;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
  }
  
  public static i a(c paramc, Provider<cn> paramProvider, Provider<com.truecaller.i.c> paramProvider1, Provider<al> paramProvider2, Provider<com.truecaller.utils.a> paramProvider3)
  {
    return new i(paramc, paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */