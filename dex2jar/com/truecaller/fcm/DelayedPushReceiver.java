package com.truecaller.fcm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.push.b;
import javax.inject.Inject;

public final class DelayedPushReceiver
  extends BroadcastReceiver
{
  public static final DelayedPushReceiver.a b = new DelayedPushReceiver.a((byte)0);
  @Inject
  public b a;
  
  public DelayedPushReceiver()
  {
    TrueApp localTrueApp = TrueApp.y();
    k.a(localTrueApp, "TrueApp.getApp()");
    localTrueApp.a().a(this);
  }
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramIntent != null) {
      paramContext = paramIntent.getAction();
    } else {
      paramContext = null;
    }
    if (k.a(paramContext, "com.truecaller.fcm.delayed_push"))
    {
      paramContext = a;
      if (paramContext == null) {
        k.a("pushHandler");
      }
      paramContext.a(paramIntent.getExtras(), paramIntent.getLongExtra("com.truecaller.fcm.delayed_push.EXTRA_SENT_TIME", 0L));
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.fcm.DelayedPushReceiver
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */