package com.truecaller.c;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.os.RemoteException;
import android.provider.CallLog.Calls;
import android.util.Pair;
import android.util.SparseBooleanArray;
import android.widget.ListView;
import com.truecaller.TrueApp;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.log.d;
import com.truecaller.old.a.c;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class a
  extends com.truecaller.old.a.a
{
  private final List<Pair<Long, Long>> a;
  
  public a(c paramc, List<Pair<Long, Long>> paramList)
  {
    super(paramc, false, new Object[0]);
    a = paramList;
  }
  
  public static List<Pair<Long, Long>> a(ListView paramListView)
  {
    if (paramListView != null)
    {
      int j = paramListView.getCount();
      ArrayList localArrayList = new ArrayList(j);
      int i = 0;
      while (i < j)
      {
        Object localObject = paramListView.getItemAtPosition(i);
        if ((localObject instanceof Cursor))
        {
          localObject = (Cursor)localObject;
          long l1 = paramListView.getItemIdAtPosition(i);
          long l2 = ((Cursor)localObject).getLong(((Cursor)localObject).getColumnIndex("call_log_id"));
          if (l1 != 0L) {
            localArrayList.add(new Pair(Long.valueOf(l1), Long.valueOf(l2)));
          }
        }
        i += 1;
      }
      return localArrayList;
    }
    return null;
  }
  
  public static List<Pair<Long, Long>> b(ListView paramListView)
  {
    SparseBooleanArray localSparseBooleanArray;
    if (paramListView == null) {
      localSparseBooleanArray = null;
    } else {
      localSparseBooleanArray = paramListView.getCheckedItemPositions();
    }
    if (localSparseBooleanArray != null)
    {
      int n = localSparseBooleanArray.size();
      if (n > 0)
      {
        ArrayList localArrayList = new ArrayList(n);
        int k = 0;
        int i = -1;
        while (k < n)
        {
          int m = i;
          try
          {
            if (localSparseBooleanArray.valueAt(k))
            {
              int i1 = localSparseBooleanArray.keyAt(k);
              Object localObject = paramListView.getItemAtPosition(i1);
              m = i;
              if ((localObject instanceof Cursor))
              {
                localObject = (Cursor)localObject;
                int j = i;
                if (i == -1) {
                  j = ((Cursor)localObject).getColumnIndexOrThrow("call_log_id");
                }
                long l1 = paramListView.getItemIdAtPosition(i1);
                long l2 = ((Cursor)localObject).getLong(j);
                m = j;
                if (l1 != 0L)
                {
                  localArrayList.add(new Pair(Long.valueOf(l1), Long.valueOf(l2)));
                  m = j;
                }
              }
            }
            k += 1;
            i = m;
          }
          catch (IllegalArgumentException paramListView)
          {
            d.a(paramListView);
            return null;
          }
        }
        return localArrayList;
      }
    }
    return null;
  }
  
  protected Object doInBackground(Object... paramVarArgs)
  {
    paramVarArgs = TrueApp.x();
    int k = 0;
    int m = 0;
    int n = 0;
    int i1 = 0;
    int j = 0;
    int i = i1;
    if (paramVarArgs != null)
    {
      ArrayList localArrayList1 = new ArrayList();
      ArrayList localArrayList2 = new ArrayList();
      Iterator localIterator = a.iterator();
      while (localIterator.hasNext())
      {
        Pair localPair = (Pair)localIterator.next();
        localArrayList1.add(ContentProviderOperation.newDelete(ContentUris.withAppendedId(TruecallerContract.n.a(), ((Long)first).longValue())).build());
        if (((Long)second).longValue() != 0L) {
          localArrayList2.add(ContentProviderOperation.newDelete(CallLog.Calls.CONTENT_URI).withSelection("_id=?", new String[] { String.valueOf(second) }).build());
        }
      }
      try
      {
        if (!localArrayList2.isEmpty()) {
          paramVarArgs.getContentResolver().applyBatch("call_log", localArrayList2);
        }
        i = i1;
        if (!localArrayList1.isEmpty())
        {
          paramVarArgs = paramVarArgs.getContentResolver().applyBatch(TruecallerContract.a, localArrayList1);
          i1 = paramVarArgs.length;
          i = 0;
          for (;;)
          {
            if (j < i1) {
              try
              {
                k = count.intValue();
                i += k;
                j += 1;
              }
              catch (OperationApplicationException paramVarArgs) {}catch (RemoteException paramVarArgs) {}catch (RuntimeException paramVarArgs) {}
            }
          }
        }
      }
      catch (OperationApplicationException paramVarArgs)
      {
        i = k;
        d.a(paramVarArgs);
      }
      catch (RemoteException paramVarArgs)
      {
        i = m;
        d.a(paramVarArgs);
      }
      catch (RuntimeException paramVarArgs)
      {
        i = n;
        d.a(paramVarArgs);
      }
    }
    return Integer.valueOf(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.c.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */