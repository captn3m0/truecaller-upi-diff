package com.truecaller.incallui;

import android.content.Context;
import android.net.Uri;
import c.d.a.a;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.filters.FilterManager;
import com.truecaller.incallui.a.h;
import com.truecaller.log.AssertionUtil;
import com.truecaller.network.search.j;
import com.truecaller.network.search.l;
import com.truecaller.network.search.n;
import com.truecaller.util.ce;
import com.truecaller.util.w;
import java.io.IOException;
import java.util.UUID;
import javax.inject.Inject;
import kotlinx.coroutines.ag;

public final class b
  implements com.truecaller.incallui.a.g
{
  private final Context a;
  
  @Inject
  public b(Context paramContext)
  {
    a = paramContext;
  }
  
  private final FilterManager b()
  {
    FilterManager localFilterManager = a().P();
    c.g.b.k.a(localFilterManager, "graph.filterManager()");
    return localFilterManager;
  }
  
  final bp a()
  {
    Object localObject = a.getApplicationContext();
    if (localObject != null)
    {
      localObject = ((bk)localObject).a();
      c.g.b.k.a(localObject, "(context.applicationCont…GraphHolder).objectsGraph");
      return (bp)localObject;
    }
    throw new c.u("null cannot be cast to non-null type com.truecaller.GraphHolder");
  }
  
  public final Object a(final String paramString, c.d.c<? super h> paramc)
  {
    c.d.f localf = a().bm();
    c.g.b.k.a(localf, "graph.asyncCoroutineContext()");
    return kotlinx.coroutines.g.a(localf, (m)new a(this, paramString, null), paramc);
  }
  
  @c.d.b.a.f(b="InCallUICallerInfoProviderImpl.kt", c={}, d="invokeSuspend", e="com.truecaller.incallui.InCallUICallerInfoProviderImpl$searchCaller$2")
  static final class a
    extends c.d.b.a.k
    implements m<ag, c.d.c<? super h>, Object>
  {
    int a;
    private ag d;
    
    a(b paramb, String paramString, c.d.c paramc)
    {
      super(paramc);
    }
    
    public final c.d.c<x> a(Object paramObject, c.d.c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new a(b, paramString, paramc);
      d = ((ag)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      Object localObject1 = a.a;
      if (a == 0) {
        if ((paramObject instanceof o.b)) {}
      }
      for (;;)
      {
        try
        {
          paramObject = b.a().V();
          c.g.b.k.a(paramObject, "graph.phoneNumberHelper()");
          localObject1 = ((com.truecaller.common.h.u)paramObject).b(paramString);
          paramObject = localObject1;
          if (localObject1 == null) {
            paramObject = paramString;
          }
          localObject1 = b.a().u();
          c.g.b.k.a(localObject1, "graph.searchManager()");
          localObject2 = UUID.randomUUID();
          c.g.b.k.a(localObject2, "UUID.randomUUID()");
          localObject1 = ((l)localObject1).a((UUID)localObject2, "voip").b().a((String)paramObject).a().a(4).f();
          if (localObject1 != null)
          {
            Contact localContact = ((n)localObject1).a();
            if (localContact != null)
            {
              Number localNumber = w.a(localContact, (String)paramObject);
              com.truecaller.common.tag.c localc = ce.a(localContact);
              c.g.b.k.a(localContact, "contact");
              String str1 = localContact.s();
              c.g.b.k.a(str1, "contact.displayNameOrNumber");
              paramObject = localContact.a(false);
              if (paramObject == null) {
                break label300;
              }
              paramObject = ((Uri)paramObject).toString();
              localObject2 = localContact.q();
              localObject1 = localObject2;
              if (localObject2 == null) {
                localObject1 = paramString;
              }
              String str2 = localContact.c();
              c.g.b.k.a(str2, "contact.shortFormattedAddress");
              if (localNumber == null) {
                break label305;
              }
              localObject2 = localNumber.g();
              paramObject = new h(str1, (String)paramObject, (String)localObject1, str2, (String)localObject2, localc, b.a(b, localContact), b.b(b, localContact), localContact.Z());
              return paramObject;
            }
          }
        }
        catch (IOException paramObject)
        {
          AssertionUtil.reportThrowableButNeverCrash((Throwable)paramObject);
        }
        return null;
        throw a;
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        label300:
        paramObject = null;
        continue;
        label305:
        Object localObject2 = null;
      }
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((a)a(paramObject1, (c.d.c)paramObject2)).a(x.a);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */