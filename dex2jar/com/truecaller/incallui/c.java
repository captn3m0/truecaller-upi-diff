package com.truecaller.incallui;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build.VERSION;
import c.g.b.k;
import com.truecaller.common.account.r;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.incallui.service.InCallUIService;
import javax.inject.Inject;
import javax.inject.Named;

public final class c
  implements a
{
  private final r b;
  private final e c;
  
  @Inject
  public c(r paramr, @Named("features_registry") e parame)
  {
    b = paramr;
    c = parame;
  }
  
  private static boolean a()
  {
    return Build.VERSION.SDK_INT >= 23;
  }
  
  private static void c(Context paramContext)
  {
    ComponentName localComponentName = new ComponentName(paramContext, InCallUIService.class);
    paramContext.getPackageManager().setComponentEnabledSetting(localComponentName, 2, 1);
  }
  
  public final void a(Context paramContext)
  {
    k.b(paramContext, "context");
    if (!a()) {
      return;
    }
    Object localObject = c;
    if ((B.a((e)localObject, e.a[86]).a()) && (b.c()))
    {
      localObject = new ComponentName(paramContext, InCallUIService.class);
      paramContext.getPackageManager().setComponentEnabledSetting((ComponentName)localObject, 1, 1);
      return;
    }
    c(paramContext);
  }
  
  public final void b(Context paramContext)
  {
    k.b(paramContext, "context");
    if (!a()) {
      return;
    }
    c(paramContext);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */