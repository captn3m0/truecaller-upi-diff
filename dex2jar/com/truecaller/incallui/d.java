package com.truecaller.incallui;

import com.truecaller.common.account.r;
import com.truecaller.featuretoggles.e;
import javax.inject.Provider;

public final class d
  implements dagger.a.d<c>
{
  private final Provider<r> a;
  private final Provider<e> b;
  
  private d(Provider<r> paramProvider, Provider<e> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static d a(Provider<r> paramProvider, Provider<e> paramProvider1)
  {
    return new d(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */