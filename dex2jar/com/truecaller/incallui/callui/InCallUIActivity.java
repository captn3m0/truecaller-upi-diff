package com.truecaller.incallui.callui;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v4.content.b;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import c.g.b.k;
import c.u;
import com.truecaller.incallui.R.color;
import com.truecaller.incallui.R.id;
import com.truecaller.incallui.R.layout;
import com.truecaller.incallui.a.i.a;
import javax.inject.Inject;

public final class InCallUIActivity
  extends AppCompatActivity
  implements b.b
{
  public static final InCallUIActivity.a b = new InCallUIActivity.a((byte)0);
  @Inject
  public b.a a;
  
  private final void a(Intent paramIntent)
  {
    if (paramIntent != null) {
      paramIntent = paramIntent.getAction();
    } else {
      paramIntent = null;
    }
    if (paramIntent == null) {
      return;
    }
    int i = paramIntent.hashCode();
    if (i != 361822499)
    {
      if (i != 1285183569)
      {
        if (i != 1965691843) {
          return;
        }
        if (paramIntent.equals("com.truecaller.incallui.callui.ACTION_DECLINE_CALL"))
        {
          paramIntent = a;
          if (paramIntent == null) {
            k.a("presenter");
          }
          paramIntent.O_();
        }
      }
      else if (paramIntent.equals("com.truecaller.incallui.callui.ACTION_HANG_UP_CALL"))
      {
        paramIntent = a;
        if (paramIntent == null) {
          k.a("presenter");
        }
        paramIntent.P_();
      }
    }
    else if (paramIntent.equals("com.truecaller.incallui.callui.ACTION_ANSWER_CALL"))
    {
      paramIntent = a;
      if (paramIntent == null) {
        k.a("presenter");
      }
      paramIntent.a();
      return;
    }
  }
  
  public final void a()
  {
    com.truecaller.utils.extensions.i.f(this).cancel(R.id.incallui_service_incoming_call_notification);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "number");
    o localo = getSupportFragmentManager().a();
    Object localObject = com.truecaller.incallui.callui.a.a.b;
    localObject = new com.truecaller.incallui.callui.a.a();
    Bundle localBundle = new Bundle();
    localBundle.putString("ARGUMENT_CALLER_NUMBER", paramString);
    ((com.truecaller.incallui.callui.a.a)localObject).setArguments(localBundle);
    localo.b(16908290, (Fragment)localObject).d();
  }
  
  public final void b()
  {
    getSupportFragmentManager().c();
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "number");
    if (getSupportFragmentManager().a("OUTGOING_CALL_FRAGMENT_TAG") == null)
    {
      localObject1 = getSupportFragmentManager().a();
      Object localObject2 = com.truecaller.incallui.callui.b.a.b;
      localObject2 = new com.truecaller.incallui.callui.b.a();
      Bundle localBundle = new Bundle();
      localBundle.putString("ARGUMENT_CALLER_NUMBER", paramString);
      ((com.truecaller.incallui.callui.b.a)localObject2).setArguments(localBundle);
      ((o)localObject1).a(16908290, (Fragment)localObject2, "OUTGOING_CALL_FRAGMENT_TAG").d();
      return;
    }
    paramString = getSupportFragmentManager().a();
    Object localObject1 = getSupportFragmentManager().a("OUTGOING_CALL_FRAGMENT_TAG");
    if (localObject1 != null)
    {
      paramString.e((Fragment)localObject1).d();
      return;
    }
    throw new u("null cannot be cast to non-null type android.support.v4.app.Fragment");
  }
  
  public final void c()
  {
    finish();
  }
  
  public final void onBackPressed()
  {
    b.a locala = a;
    if (locala == null) {
      k.a("presenter");
    }
    j localj = getSupportFragmentManager();
    k.a(localj, "supportFragmentManager");
    locala.a(localj.e());
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.activity_incallui);
    com.truecaller.utils.extensions.a.a(this, true);
    paramBundle = com.truecaller.incallui.a.i.a;
    i.a.a().a(this);
    if (Build.VERSION.SDK_INT >= 21)
    {
      paramBundle = getWindow();
      k.a(paramBundle, "window");
      paramBundle.setStatusBarColor(b.c((Context)this, R.color.incallui_status_bar_color));
    }
    paramBundle = a;
    if (paramBundle == null) {
      k.a("presenter");
    }
    paramBundle.a(this);
    paramBundle = a;
    if (paramBundle == null) {
      k.a("presenter");
    }
    Object localObject = getIntent();
    if (localObject != null)
    {
      localObject = ((Intent)localObject).getData();
      if (localObject != null)
      {
        localObject = ((Uri)localObject).getSchemeSpecificPart();
        if (localObject == null) {
          return;
        }
        paramBundle.a((String)localObject);
        a(getIntent());
        return;
      }
    }
  }
  
  public final void onDestroy()
  {
    b.a locala = a;
    if (locala == null) {
      k.a("presenter");
    }
    locala.y_();
    super.onDestroy();
  }
  
  public final void onNewIntent(Intent paramIntent)
  {
    super.onNewIntent(paramIntent);
    a(paramIntent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.callui.InCallUIActivity
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */