package com.truecaller.incallui.callui.b.a;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.incallui.R.anim;
import com.truecaller.incallui.R.id;
import com.truecaller.incallui.R.layout;
import com.truecaller.incallui.a.i;
import com.truecaller.incallui.a.i.a;
import java.util.HashMap;
import javax.inject.Inject;

public final class a
  extends Fragment
  implements c.b
{
  public static final a.a b = new a.a((byte)0);
  @Inject
  public c.a a;
  private ImageButton c;
  private FrameLayout d;
  private GridLayout e;
  private TextView f;
  private HashMap g;
  
  public final c.a a()
  {
    c.a locala = a;
    if (locala == null) {
      k.a("presenter");
    }
    return locala;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "input");
    TextView localTextView = f;
    if (localTextView == null) {
      k.a("keypadInputText");
    }
    localTextView.append((CharSequence)paramString);
  }
  
  public final void b()
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = ((f)localObject).getSupportFragmentManager();
      if (localObject == null) {
        return;
      }
      if (((j)localObject).e() == 0) {
        return;
      }
      ((j)localObject).b("KEYPAD_FRAGMENT_TAG");
      return;
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    k.b(paramContext, "context");
    super.onAttach(paramContext);
    paramContext = i.a;
    i.a.a().a(this);
  }
  
  public final Animation onCreateAnimation(int paramInt1, boolean paramBoolean, int paramInt2)
  {
    if (paramBoolean)
    {
      localAnimation = AnimationUtils.loadAnimation((Context)getActivity(), R.anim.fast_slide_in_up);
      k.a(localAnimation, "AnimationUtils.loadAnima… R.anim.fast_slide_in_up)");
      return localAnimation;
    }
    Animation localAnimation = AnimationUtils.loadAnimation((Context)getActivity(), R.anim.fast_slide_out_down);
    k.a(localAnimation, "AnimationUtils.loadAnima…anim.fast_slide_out_down)");
    return localAnimation;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    paramLayoutInflater = paramLayoutInflater.inflate(R.layout.fragment_keypad, paramViewGroup, false);
    k.a(paramLayoutInflater, "inflater.inflate(R.layou…keypad, container, false)");
    return paramLayoutInflater;
  }
  
  public final void onDestroyView()
  {
    Object localObject = a;
    if (localObject == null) {
      k.a("presenter");
    }
    ((c.a)localObject).y_();
    super.onDestroyView();
    localObject = g;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    paramBundle = paramView.findViewById(R.id.button_close);
    k.a(paramBundle, "view.findViewById(R.id.button_close)");
    c = ((ImageButton)paramBundle);
    paramBundle = paramView.findViewById(R.id.view_outside_area);
    k.a(paramBundle, "view.findViewById(R.id.view_outside_area)");
    d = ((FrameLayout)paramBundle);
    paramBundle = paramView.findViewById(R.id.grid_keypad);
    k.a(paramBundle, "view.findViewById(R.id.grid_keypad)");
    e = ((GridLayout)paramBundle);
    paramView = paramView.findViewById(R.id.text_keypad_input);
    k.a(paramView, "view.findViewById(R.id.text_keypad_input)");
    f = ((TextView)paramView);
    paramView = a;
    if (paramView == null) {
      k.a("presenter");
    }
    paramView.a(this);
    paramView = c;
    if (paramView == null) {
      k.a("closeButton");
    }
    paramView.setOnClickListener((View.OnClickListener)new a.b(this));
    paramView = d;
    if (paramView == null) {
      k.a("outsideAreaView");
    }
    paramView.setOnClickListener((View.OnClickListener)new a.c(this));
    int i = 0;
    paramView = e;
    if (paramView == null) {
      k.a("keypadView");
    }
    int j = paramView.getChildCount();
    while (i < j)
    {
      paramView = e;
      if (paramView == null) {
        k.a("keypadView");
      }
      paramView.getChildAt(i).setOnClickListener((View.OnClickListener)new a.d(this, i));
      i += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.callui.b.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */