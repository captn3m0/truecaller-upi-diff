package com.truecaller.incallui.callui.b.a;

import com.truecaller.bb;
import com.truecaller.incallui.a.p;
import com.truecaller.incallui.service.c;
import javax.inject.Inject;

public final class d
  extends bb<c.b>
  implements c.a
{
  private final c a;
  private final p c;
  
  @Inject
  public d(c paramc, p paramp)
  {
    a = paramc;
    c = paramp;
  }
  
  public final void a()
  {
    c.b localb = (c.b)b;
    if (localb != null)
    {
      localb.b();
      return;
    }
  }
  
  public final void a(char paramChar)
  {
    c.b localb = (c.b)b;
    if (localb != null) {
      localb.a(String.valueOf(paramChar));
    }
    c.a(paramChar);
    a.a(paramChar);
  }
  
  public final void y_()
  {
    c.a();
    super.y_();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.callui.b.a.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */