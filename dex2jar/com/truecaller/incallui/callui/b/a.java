package com.truecaller.incallui.callui.b;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.constraint.motion.MotionLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v4.content.b;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;
import c.g.b.k;
import c.u;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.truecaller.common.h.aq.d;
import com.truecaller.incallui.R.color;
import com.truecaller.incallui.R.drawable;
import com.truecaller.incallui.R.id;
import com.truecaller.incallui.R.layout;
import com.truecaller.incallui.a.i;
import com.truecaller.incallui.a.i.a;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import javax.inject.Inject;

public final class a
  extends Fragment
  implements b.b
{
  public static final a.a b = new a.a((byte)0);
  @Inject
  public b.a a;
  private MotionLayout c;
  private FloatingActionButton d;
  private ToggleButton e;
  private Button f;
  private ToggleButton g;
  private ToggleButton h;
  private ImageView i;
  private ImageButton j;
  private TextView k;
  private ImageView l;
  private View m;
  private View n;
  private Chronometer o;
  private TextView p;
  private HashMap q;
  
  public final b.a a()
  {
    b.a locala = a;
    if (locala == null) {
      k.a("presenter");
    }
    return locala;
  }
  
  public final void a(int paramInt)
  {
    ImageView localImageView = l;
    if (localImageView == null) {
      k.a("truecallerLogoView");
    }
    localImageView.setImageResource(paramInt);
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    Object localObject = getActivity();
    if (localObject == null) {
      return;
    }
    k.a(localObject, "activity ?: return");
    paramInt2 = b.c((Context)localObject, paramInt2);
    localObject = p;
    if (localObject == null) {
      k.a("statusTextView");
    }
    ((TextView)localObject).setText(paramInt1);
    localObject = p;
    if (localObject == null) {
      k.a("statusTextView");
    }
    ((TextView)localObject).setTextColor(paramInt2);
    localObject = p;
    if (localObject == null) {
      k.a("statusTextView");
    }
    t.a((View)localObject);
  }
  
  public final void a(long paramLong)
  {
    Chronometer localChronometer = o;
    if (localChronometer == null) {
      k.a("chronometerView");
    }
    t.a((View)localChronometer);
    localChronometer = o;
    if (localChronometer == null) {
      k.a("chronometerView");
    }
    localChronometer.setBase(paramLong);
    localChronometer = o;
    if (localChronometer == null) {
      k.a("chronometerView");
    }
    localChronometer.start();
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "profileName");
    TextView localTextView = k;
    if (localTextView == null) {
      k.a("profileNameTextView");
    }
    localTextView.setText((CharSequence)paramString);
    paramString = k;
    if (paramString == null) {
      k.a("profileNameTextView");
    }
    paramString.setSelected(true);
  }
  
  public final void b()
  {
    Object localObject1 = getActivity();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "activity ?: return");
    if (Build.VERSION.SDK_INT >= 21)
    {
      localObject2 = ((f)localObject1).getWindow();
      k.a(localObject2, "context.window");
      ((Window)localObject2).setStatusBarColor(b.c((Context)localObject1, R.color.incallui_status_bar_spam_color));
    }
    Object localObject2 = m;
    if (localObject2 == null) {
      k.a("headerCoverView");
    }
    ((View)localObject2).setBackgroundColor(b.c((Context)localObject1, R.color.incallui_spam_color));
    localObject1 = n;
    if (localObject1 == null) {
      k.a("headerArcView");
    }
    ((View)localObject1).setBackgroundResource(R.drawable.background_incallui_spam_header_view);
    localObject1 = i;
    if (localObject1 == null) {
      k.a("profilePictureImageView");
    }
    ((ImageView)localObject1).setImageDrawable(null);
    localObject1 = i;
    if (localObject1 == null) {
      k.a("profilePictureImageView");
    }
    ((ImageView)localObject1).setImageResource(R.drawable.ic_avatar_incallui_spam);
    localObject1 = j;
    if (localObject1 == null) {
      k.a("minimiseButton");
    }
    ((ImageButton)localObject1).setBackgroundResource(R.drawable.background_incallui_minimise_spam_call);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "profilePictureUrl");
    Object localObject = getActivity();
    if (localObject == null) {
      return;
    }
    k.a(localObject, "activity ?: return");
    paramString = w.a((Context)localObject).a(paramString).a(R.drawable.ic_avatar_incallui_default).a((ai)aq.d.b());
    c = true;
    paramString = paramString.b();
    localObject = i;
    if (localObject == null) {
      k.a("profilePictureImageView");
    }
    paramString.a((ImageView)localObject, null);
  }
  
  public final void c()
  {
    TextView localTextView = p;
    if (localTextView == null) {
      k.a("statusTextView");
    }
    t.c((View)localTextView);
  }
  
  public final void d()
  {
    ToggleButton localToggleButton = e;
    if (localToggleButton == null) {
      k.a("muteToggleButton");
    }
    localToggleButton.setChecked(true);
  }
  
  public final void e()
  {
    ToggleButton localToggleButton = e;
    if (localToggleButton == null) {
      k.a("muteToggleButton");
    }
    localToggleButton.setChecked(false);
  }
  
  public final void f()
  {
    ToggleButton localToggleButton = g;
    if (localToggleButton == null) {
      k.a("speakerToggleButton");
    }
    localToggleButton.setChecked(true);
  }
  
  public final void g()
  {
    ToggleButton localToggleButton = g;
    if (localToggleButton == null) {
      k.a("speakerToggleButton");
    }
    localToggleButton.setChecked(false);
  }
  
  public final void h()
  {
    ToggleButton localToggleButton = h;
    if (localToggleButton == null) {
      k.a("holdToggleButton");
    }
    localToggleButton.setEnabled(true);
  }
  
  public final void i()
  {
    Object localObject1 = getActivity();
    if (localObject1 != null)
    {
      Object localObject2 = ((f)localObject1).getSupportFragmentManager();
      if (localObject2 == null) {
        return;
      }
      if (((j)localObject2).a("KEYPAD_FRAGMENT_TAG") == null)
      {
        localObject1 = ((j)localObject2).a();
        int i1 = R.id.view_keypad;
        localObject2 = com.truecaller.incallui.callui.b.a.a.b;
        ((o)localObject1).a(i1, (Fragment)new com.truecaller.incallui.callui.b.a.a(), "KEYPAD_FRAGMENT_TAG").a("KEYPAD_FRAGMENT_TAG").d();
        return;
      }
      localObject1 = ((j)localObject2).a();
      localObject2 = ((j)localObject2).a("KEYPAD_FRAGMENT_TAG");
      if (localObject2 != null)
      {
        ((o)localObject1).e((Fragment)localObject2).a("KEYPAD_FRAGMENT_TAG").d();
        return;
      }
      throw new u("null cannot be cast to non-null type android.support.v4.app.Fragment");
    }
  }
  
  public final void j()
  {
    ToggleButton localToggleButton = h;
    if (localToggleButton == null) {
      k.a("holdToggleButton");
    }
    localToggleButton.setChecked(true);
  }
  
  public final void k()
  {
    ToggleButton localToggleButton = h;
    if (localToggleButton == null) {
      k.a("holdToggleButton");
    }
    localToggleButton.setChecked(false);
  }
  
  public final void l()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final void m()
  {
    Chronometer localChronometer = o;
    if (localChronometer == null) {
      k.a("chronometerView");
    }
    t.c((View)localChronometer);
    localChronometer = o;
    if (localChronometer == null) {
      k.a("chronometerView");
    }
    localChronometer.stop();
  }
  
  public final void n()
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = ((f)localObject).getSupportFragmentManager();
      if (localObject == null) {
        return;
      }
      if (((j)localObject).e() == 0) {
        return;
      }
      ((j)localObject).b("KEYPAD_FRAGMENT_TAG");
      return;
    }
  }
  
  public final void o()
  {
    MotionLayout localMotionLayout = c;
    if (localMotionLayout == null) {
      k.a("motionLayoutView");
    }
    localMotionLayout.a(R.id.outgoing_incallui_ended_start_set, R.id.outgoing_incallui_ended_end_set);
    localMotionLayout = c;
    if (localMotionLayout == null) {
      k.a("motionLayoutView");
    }
    localMotionLayout.c();
  }
  
  public final void onAttach(Context paramContext)
  {
    k.b(paramContext, "context");
    super.onAttach(paramContext);
    paramContext = i.a;
    i.a.a().a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    paramLayoutInflater = paramLayoutInflater.inflate(R.layout.fragment_incallui_ongoing, paramViewGroup, false);
    k.a(paramLayoutInflater, "inflater.inflate(R.layou…ngoing, container, false)");
    return paramLayoutInflater;
  }
  
  public final void onDestroyView()
  {
    Object localObject = a;
    if (localObject == null) {
      k.a("presenter");
    }
    ((b.a)localObject).y_();
    super.onDestroyView();
    localObject = q;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    paramBundle = paramView.findViewById(R.id.motion_layout);
    k.a(paramBundle, "view.findViewById(R.id.motion_layout)");
    c = ((MotionLayout)paramBundle);
    paramBundle = paramView.findViewById(R.id.button_end_call);
    k.a(paramBundle, "view.findViewById(R.id.button_end_call)");
    d = ((FloatingActionButton)paramBundle);
    paramBundle = paramView.findViewById(R.id.toggle_mute);
    k.a(paramBundle, "view.findViewById(R.id.toggle_mute)");
    e = ((ToggleButton)paramBundle);
    paramBundle = paramView.findViewById(R.id.button_keypad);
    k.a(paramBundle, "view.findViewById(R.id.button_keypad)");
    f = ((Button)paramBundle);
    paramBundle = paramView.findViewById(R.id.toggle_speaker);
    k.a(paramBundle, "view.findViewById(R.id.toggle_speaker)");
    g = ((ToggleButton)paramBundle);
    paramBundle = paramView.findViewById(R.id.toggle_hold);
    k.a(paramBundle, "view.findViewById(R.id.toggle_hold)");
    h = ((ToggleButton)paramBundle);
    paramBundle = paramView.findViewById(R.id.text_profile_name);
    k.a(paramBundle, "view.findViewById(R.id.text_profile_name)");
    k = ((TextView)paramBundle);
    paramBundle = paramView.findViewById(R.id.image_profile_picture);
    k.a(paramBundle, "view.findViewById(R.id.image_profile_picture)");
    i = ((ImageView)paramBundle);
    paramBundle = paramView.findViewById(R.id.button_minimise);
    k.a(paramBundle, "view.findViewById(R.id.button_minimise)");
    j = ((ImageButton)paramBundle);
    paramBundle = paramView.findViewById(R.id.image_logo);
    k.a(paramBundle, "view.findViewById(R.id.image_logo)");
    l = ((ImageView)paramBundle);
    paramBundle = paramView.findViewById(R.id.view_header_cover);
    k.a(paramBundle, "view.findViewById(R.id.view_header_cover)");
    m = paramBundle;
    paramBundle = paramView.findViewById(R.id.view_header);
    k.a(paramBundle, "view.findViewById(R.id.view_header)");
    n = paramBundle;
    paramBundle = paramView.findViewById(R.id.chronometer);
    k.a(paramBundle, "view.findViewById(R.id.chronometer)");
    o = ((Chronometer)paramBundle);
    paramView = paramView.findViewById(R.id.text_status);
    k.a(paramView, "view.findViewById(R.id.text_status)");
    p = ((TextView)paramView);
    paramView = a;
    if (paramView == null) {
      k.a("presenter");
    }
    paramView.a(this);
    paramBundle = a;
    if (paramBundle == null) {
      k.a("presenter");
    }
    Bundle localBundle = getArguments();
    paramView = null;
    if (localBundle != null) {
      paramView = localBundle.getString("ARGUMENT_CALLER_NUMBER", null);
    }
    paramBundle.a(paramView);
    paramView = d;
    if (paramView == null) {
      k.a("endCallButton");
    }
    paramView.setOnClickListener((View.OnClickListener)new a.b(this));
    paramView = e;
    if (paramView == null) {
      k.a("muteToggleButton");
    }
    paramView.setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)new a.c(this));
    paramView = f;
    if (paramView == null) {
      k.a("keypadButton");
    }
    paramView.setOnClickListener((View.OnClickListener)new a.d(this));
    paramView = g;
    if (paramView == null) {
      k.a("speakerToggleButton");
    }
    paramView.setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)new a.e(this));
    paramView = h;
    if (paramView == null) {
      k.a("holdToggleButton");
    }
    paramView.setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)new a.f(this));
    paramView = j;
    if (paramView == null) {
      k.a("minimiseButton");
    }
    paramView.setOnClickListener((View.OnClickListener)new a.g(this));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.callui.b.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */