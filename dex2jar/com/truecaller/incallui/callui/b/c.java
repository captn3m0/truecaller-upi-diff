package com.truecaller.incallui.callui.b;

import c.d.f;
import c.g.a.m;
import com.truecaller.ba;
import com.truecaller.incallui.a.b;
import com.truecaller.incallui.a.o;
import com.truecaller.utils.a;
import com.truecaller.utils.extensions.j;
import javax.inject.Inject;
import javax.inject.Named;

public final class c
  extends ba<b.b>
  implements b.a
{
  private final com.truecaller.incallui.service.c c;
  private final o d;
  private final a e;
  private final b f;
  private final f g;
  
  @Inject
  public c(com.truecaller.incallui.service.c paramc, o paramo, a parama, b paramb, @Named("UI") f paramf)
  {
    super(paramf);
    c = paramc;
    d = paramo;
    e = parama;
    f = paramb;
    g = paramf;
  }
  
  private final void a(int paramInt1, int paramInt2)
  {
    b.b localb = (b.b)b;
    if (localb != null) {
      localb.m();
    }
    localb = (b.b)b;
    if (localb != null)
    {
      localb.a(paramInt1, paramInt2);
      return;
    }
  }
  
  public final void S_()
  {
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.i();
      return;
    }
  }
  
  public final void T_()
  {
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.l();
      return;
    }
  }
  
  public final void a()
  {
    c.h();
  }
  
  public final void a(String paramString)
  {
    if (paramString != null)
    {
      b.b localb = (b.b)b;
      if (localb != null) {
        localb.a(paramString);
      }
    }
    paramString = c.b();
    if (paramString == null) {
      return;
    }
    j.a(this, paramString, (m)new c.b(this, null));
  }
  
  public final void a(boolean paramBoolean)
  {
    c.a(paramBoolean);
  }
  
  public final void b(boolean paramBoolean)
  {
    c.b(paramBoolean);
  }
  
  public final void c(boolean paramBoolean)
  {
    c.c(paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.callui.b.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */