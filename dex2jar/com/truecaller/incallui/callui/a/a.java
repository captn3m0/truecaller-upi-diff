package com.truecaller.incallui.callui.a;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.res.ColorStateList;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.constraint.motion.MotionLayout;
import android.support.constraint.motion.MotionLayout.d;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.content.b;
import android.support.v4.view.r;
import android.support.v7.app.AlertDialog.Builder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.truecaller.common.h.aq.d;
import com.truecaller.common.tag.TagView;
import com.truecaller.common.tag.c;
import com.truecaller.incallui.R.anim;
import com.truecaller.incallui.R.array;
import com.truecaller.incallui.R.color;
import com.truecaller.incallui.R.drawable;
import com.truecaller.incallui.R.id;
import com.truecaller.incallui.R.layout;
import com.truecaller.incallui.R.string;
import com.truecaller.incallui.a.i;
import com.truecaller.incallui.a.i.a;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import java.util.List;
import javax.inject.Inject;

public final class a
  extends Fragment
  implements b.b
{
  public static final a.a b = new a.a((byte)0);
  @Inject
  public b.a a;
  private MotionLayout c;
  private FloatingActionButton d;
  private FloatingActionButton e;
  private FloatingActionButton f;
  private TextView g;
  private ImageView h;
  private TextView i;
  private TagView j;
  private ImageButton k;
  private View l;
  private ImageView m;
  private View n;
  private View o;
  private TextView p;
  private TextView q;
  private String r;
  private HashMap s;
  
  public final b.a a()
  {
    b.a locala = a;
    if (locala == null) {
      k.a("presenter");
    }
    return locala;
  }
  
  public final void a(int paramInt)
  {
    ImageView localImageView = m;
    if (localImageView == null) {
      k.a("truecallerLogoView");
    }
    localImageView.setImageResource(paramInt);
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    Object localObject = getActivity();
    if (localObject == null) {
      return;
    }
    k.a(localObject, "activity ?: return");
    paramInt2 = b.c((Context)localObject, paramInt2);
    localObject = q;
    if (localObject == null) {
      k.a("statusTextView");
    }
    ((TextView)localObject).setText(paramInt1);
    localObject = q;
    if (localObject == null) {
      k.a("statusTextView");
    }
    ((TextView)localObject).setTextColor(paramInt2);
    localObject = q;
    if (localObject == null) {
      k.a("statusTextView");
    }
    t.a((View)localObject);
  }
  
  public final void a(c paramc)
  {
    k.b(paramc, "tag");
    f localf = getActivity();
    if (localf == null) {
      return;
    }
    k.a(localf, "activity ?: return");
    TagView localTagView = j;
    if (localTagView == null) {
      k.a("tagView");
    }
    localTagView.setTag(paramc);
    paramc = j;
    if (paramc == null) {
      k.a("tagView");
    }
    paramc.setBackgroundColor(b.c((Context)localf, R.color.incallui_action_background_color));
    paramc = j;
    if (paramc == null) {
      k.a("tagView");
    }
    t.a((View)paramc);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "profileName");
    TextView localTextView = g;
    if (localTextView == null) {
      k.a("profileNameTextView");
    }
    localTextView.setText((CharSequence)paramString);
    paramString = g;
    if (paramString == null) {
      k.a("profileNameTextView");
    }
    paramString.setSelected(true);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "phoneNumberForDisplay");
    k.b(paramString3, "shortFormattedAddress");
    TextView localTextView = i;
    if (localTextView == null) {
      k.a("aboutTextView");
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString1);
    if (paramString2 != null)
    {
      localStringBuilder.append(" · ");
      localStringBuilder.append(paramString2);
    }
    if ((c.n.m.a((CharSequence)paramString3) ^ true))
    {
      localStringBuilder.append("\n");
      localStringBuilder.append(paramString3);
    }
    localTextView.setText((CharSequence)localStringBuilder.toString());
  }
  
  public final void b()
  {
    TagView localTagView = j;
    if (localTagView == null) {
      k.a("tagView");
    }
    t.b((View)localTagView);
  }
  
  public final void b(int paramInt)
  {
    Object localObject1 = getActivity();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "activity ?: return");
    if (Build.VERSION.SDK_INT >= 21)
    {
      localObject2 = ((f)localObject1).getWindow();
      k.a(localObject2, "context.window");
      ((Window)localObject2).setStatusBarColor(b.c((Context)localObject1, R.color.incallui_status_bar_spam_color));
    }
    Object localObject2 = n;
    if (localObject2 == null) {
      k.a("headerCoverView");
    }
    ((View)localObject2).setBackgroundColor(b.c((Context)localObject1, R.color.incallui_spam_color));
    localObject1 = o;
    if (localObject1 == null) {
      k.a("headerArcView");
    }
    ((View)localObject1).setBackgroundResource(R.drawable.background_incallui_spam_header_view);
    localObject1 = h;
    if (localObject1 == null) {
      k.a("profilePictureImageView");
    }
    ((ImageView)localObject1).setImageDrawable(null);
    localObject1 = h;
    if (localObject1 == null) {
      k.a("profilePictureImageView");
    }
    ((ImageView)localObject1).setImageResource(R.drawable.ic_avatar_incallui_spam);
    localObject1 = k;
    if (localObject1 == null) {
      k.a("minimiseButton");
    }
    ((ImageButton)localObject1).setBackgroundResource(R.drawable.background_incallui_minimise_spam_call);
    localObject1 = p;
    if (localObject1 == null) {
      k.a("spamScoreTextView");
    }
    ((TextView)localObject1).setText((CharSequence)getString(R.string.incallui_spam_reports_score, new Object[] { Integer.valueOf(paramInt) }));
    localObject1 = p;
    if (localObject1 == null) {
      k.a("spamScoreTextView");
    }
    t.a((View)localObject1);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "profilePictureUrl");
    Object localObject = getActivity();
    if (localObject == null) {
      return;
    }
    k.a(localObject, "activity ?: return");
    paramString = w.a((Context)localObject).a(paramString).a(R.drawable.ic_avatar_incallui_default).a((ai)aq.d.b());
    c = true;
    paramString = paramString.b();
    localObject = h;
    if (localObject == null) {
      k.a("profilePictureImageView");
    }
    paramString.a((ImageView)localObject, null);
  }
  
  public final void c()
  {
    Object localObject = getActivity();
    if (localObject == null) {
      return;
    }
    k.a(localObject, "activity ?: return");
    FloatingActionButton localFloatingActionButton = d;
    if (localFloatingActionButton == null) {
      k.a("acceptCallButton");
    }
    r.a((View)localFloatingActionButton, ColorStateList.valueOf(b.c((Context)localObject, R.color.incallui_action_end_call_background_color)));
    localObject = d;
    if (localObject == null) {
      k.a("acceptCallButton");
    }
    ((FloatingActionButton)localObject).setImageResource(R.drawable.ic_button_incallui_hangup);
    localObject = c;
    if (localObject == null) {
      k.a("motionLayoutView");
    }
    ((MotionLayout)localObject).c();
    ((MotionLayout)localObject).setTransitionListener(null);
    ((MotionLayout)localObject).a(R.id.incoming_incallui_accepted_start_set, R.id.incoming_incallui_accepted_end_set);
    localObject = c;
    if (localObject == null) {
      k.a("motionLayoutView");
    }
    ((MotionLayout)localObject).c();
  }
  
  public final void d()
  {
    f localf = getActivity();
    if (localf == null) {
      return;
    }
    k.a(localf, "activity ?: return");
    List localList = c.a.m.b(new Integer[] { Integer.valueOf(R.string.incallui_reject_message_first_option), Integer.valueOf(R.string.incallui_reject_message_second_option), Integer.valueOf(R.string.incallui_reject_message_third_option), Integer.valueOf(R.string.incallui_reject_message_custom_option) });
    new AlertDialog.Builder((Context)localf).setItems(R.array.incallui_button_message_options, (DialogInterface.OnClickListener)new a.f(this, localList)).show();
  }
  
  public final void e()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final void f()
  {
    MotionLayout localMotionLayout = c;
    if (localMotionLayout == null) {
      k.a("motionLayoutView");
    }
    localMotionLayout.a(R.id.incoming_incallui_ended_start_set, R.id.incoming_incallui_ended_end_set);
    localMotionLayout = c;
    if (localMotionLayout == null) {
      k.a("motionLayoutView");
    }
    localMotionLayout.c();
  }
  
  public final void onAttach(Context paramContext)
  {
    k.b(paramContext, "context");
    super.onAttach(paramContext);
    paramContext = i.a;
    i.a.a().a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    paramLayoutInflater = paramLayoutInflater.inflate(R.layout.fragment_incallui_incoming, paramViewGroup, false);
    k.a(paramLayoutInflater, "inflater.inflate(R.layou…coming, container, false)");
    return paramLayoutInflater;
  }
  
  public final void onDestroyView()
  {
    Object localObject = a;
    if (localObject == null) {
      k.a("presenter");
    }
    ((b.a)localObject).y_();
    super.onDestroyView();
    localObject = s;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    paramBundle = paramView.findViewById(R.id.motion_layout);
    k.a(paramBundle, "view.findViewById(R.id.motion_layout)");
    c = ((MotionLayout)paramBundle);
    paramBundle = paramView.findViewById(R.id.button_accept_call);
    k.a(paramBundle, "view.findViewById(R.id.button_accept_call)");
    d = ((FloatingActionButton)paramBundle);
    paramBundle = paramView.findViewById(R.id.button_reject_call);
    k.a(paramBundle, "view.findViewById(R.id.button_reject_call)");
    e = ((FloatingActionButton)paramBundle);
    paramBundle = paramView.findViewById(R.id.button_message);
    k.a(paramBundle, "view.findViewById(R.id.button_message)");
    f = ((FloatingActionButton)paramBundle);
    paramBundle = paramView.findViewById(R.id.text_profile_name);
    k.a(paramBundle, "view.findViewById(R.id.text_profile_name)");
    g = ((TextView)paramBundle);
    paramBundle = paramView.findViewById(R.id.image_profile_picture);
    k.a(paramBundle, "view.findViewById(R.id.image_profile_picture)");
    h = ((ImageView)paramBundle);
    paramBundle = paramView.findViewById(R.id.text_about);
    k.a(paramBundle, "view.findViewById(R.id.text_about)");
    i = ((TextView)paramBundle);
    paramBundle = paramView.findViewById(R.id.tag_view);
    k.a(paramBundle, "view.findViewById(R.id.tag_view)");
    j = ((TagView)paramBundle);
    paramBundle = paramView.findViewById(R.id.button_minimise);
    k.a(paramBundle, "view.findViewById(R.id.button_minimise)");
    k = ((ImageButton)paramBundle);
    paramBundle = paramView.findViewById(R.id.view_answer_arrows);
    k.a(paramBundle, "view.findViewById(R.id.view_answer_arrows)");
    l = paramBundle;
    paramBundle = paramView.findViewById(R.id.image_logo);
    k.a(paramBundle, "view.findViewById(R.id.image_logo)");
    m = ((ImageView)paramBundle);
    paramBundle = paramView.findViewById(R.id.view_header_cover);
    k.a(paramBundle, "view.findViewById(R.id.view_header_cover)");
    n = paramBundle;
    paramBundle = paramView.findViewById(R.id.view_header);
    k.a(paramBundle, "view.findViewById(R.id.view_header)");
    o = paramBundle;
    paramBundle = paramView.findViewById(R.id.text_spam_score);
    k.a(paramBundle, "view.findViewById(R.id.text_spam_score)");
    p = ((TextView)paramBundle);
    paramView = paramView.findViewById(R.id.text_status);
    k.a(paramView, "view.findViewById(R.id.text_status)");
    q = ((TextView)paramView);
    paramView = a;
    if (paramView == null) {
      k.a("presenter");
    }
    paramView.a(this);
    paramBundle = getArguments();
    paramView = null;
    if (paramBundle != null) {
      paramView = paramBundle.getString("ARGUMENT_CALLER_NUMBER", null);
    }
    r = paramView;
    paramView = a;
    if (paramView == null) {
      k.a("presenter");
    }
    paramView.a(r);
    paramView = l;
    if (paramView == null) {
      k.a("answerArrowsView");
    }
    paramView.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.anim_incallui_answer_arrows));
    paramView = e;
    if (paramView == null) {
      k.a("rejectCallButton");
    }
    paramView.setOnClickListener((View.OnClickListener)new a.b(this));
    paramView = f;
    if (paramView == null) {
      k.a("rejectMessageButton");
    }
    paramView.setOnClickListener((View.OnClickListener)new a.c(this));
    paramView = k;
    if (paramView == null) {
      k.a("minimiseButton");
    }
    paramView.setOnClickListener((View.OnClickListener)new a.d(this));
    paramView = c;
    if (paramView == null) {
      k.a("motionLayoutView");
    }
    paramView.setTransitionListener((MotionLayout.d)new a.e(this));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.callui.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */