package com.truecaller.incallui.callui.a;

import c.d.f;
import c.g.a.m;
import com.truecaller.ba;
import com.truecaller.incallui.R.id;
import com.truecaller.incallui.R.string;
import com.truecaller.incallui.a.b;
import com.truecaller.incallui.a.n;
import com.truecaller.incallui.a.o;
import com.truecaller.utils.extensions.j;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.e;

public final class c
  extends ba<b.b>
  implements b.a
{
  private final com.truecaller.incallui.service.c c;
  private final o d;
  private final n e;
  private final b f;
  private final f g;
  
  @Inject
  public c(com.truecaller.incallui.service.c paramc, o paramo, n paramn, b paramb, @Named("UI") f paramf)
  {
    super(paramf);
    c = paramc;
    d = paramo;
    e = paramn;
    f = paramb;
    g = paramf;
  }
  
  public final void Q_()
  {
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.d();
      return;
    }
  }
  
  public final void R_()
  {
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.e();
      return;
    }
  }
  
  public final void a()
  {
    c.h();
  }
  
  public final void a(float paramFloat, int paramInt1, int paramInt2, int paramInt3)
  {
    if ((paramFloat < 0.95D) && (paramInt1 != R.id.incoming_incallui_answer_end_set)) {
      paramInt1 = 0;
    } else {
      paramInt1 = 1;
    }
    if ((paramInt2 == R.id.incoming_incallui_answer_start_set) && (paramInt3 == R.id.incoming_incallui_answer_end_set) && (paramInt1 != 0)) {
      e.b(this, null, (m)new c.a(this, null), 3);
    }
  }
  
  public final void a(String paramString)
  {
    if (paramString != null)
    {
      b.b localb = (b.b)b;
      if (localb != null) {
        localb.a(paramString);
      }
    }
    paramString = c.b();
    if (paramString == null) {
      return;
    }
    j.a(this, paramString, (m)new c.c(this, null));
  }
  
  public final void a(String paramString, int paramInt)
  {
    if (paramString == null) {
      return;
    }
    Integer localInteger;
    if (paramInt != R.string.incallui_reject_message_custom_option) {
      localInteger = Integer.valueOf(paramInt);
    } else {
      localInteger = null;
    }
    e.a(paramString, localInteger);
    c.h();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.callui.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */