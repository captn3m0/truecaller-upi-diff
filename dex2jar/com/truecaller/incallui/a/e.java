package com.truecaller.incallui.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.b;
import c.g.b.k;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.truecaller.common.h.aq.d;
import com.truecaller.incallui.R.dimen;
import java.io.IOException;
import javax.inject.Inject;

public final class e
  implements d
{
  private final Context a;
  
  @Inject
  public e(Context paramContext)
  {
    a = paramContext;
  }
  
  public final Bitmap a(int paramInt)
  {
    Drawable localDrawable = b.a(a, paramInt);
    if (localDrawable == null) {
      return null;
    }
    k.a(localDrawable, "ContextCompat.getDrawabl…awableRes) ?: return null");
    Bitmap localBitmap = Bitmap.createBitmap(localDrawable.getIntrinsicWidth(), localDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
    Canvas localCanvas = new Canvas(localBitmap);
    localDrawable.setBounds(0, 0, localCanvas.getWidth(), localCanvas.getHeight());
    localDrawable.draw(localCanvas);
    return localBitmap;
  }
  
  public final Bitmap a(String paramString)
  {
    k.b(paramString, "url");
    int i;
    if (((CharSequence)paramString).length() == 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      return null;
    }
    try
    {
      paramString = w.a(a).a(paramString);
      i = R.dimen.incallui_notification_avatar_image_size;
      paramString = paramString.a(i, i).a((ai)aq.d.b()).d();
      return paramString;
    }
    catch (IOException paramString) {}
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.a.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */