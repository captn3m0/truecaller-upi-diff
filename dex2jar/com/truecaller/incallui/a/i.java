package com.truecaller.incallui.a;

import c.g.b.k;
import com.truecaller.incallui.callui.InCallUIActivity;
import com.truecaller.incallui.service.InCallUIService;

public abstract interface i
{
  public static final a a = a.b;
  
  public abstract void a(InCallUIActivity paramInCallUIActivity);
  
  public abstract void a(com.truecaller.incallui.callui.a.a parama);
  
  public abstract void a(com.truecaller.incallui.callui.b.a.a parama);
  
  public abstract void a(com.truecaller.incallui.callui.b.a parama);
  
  public abstract void a(InCallUIService paramInCallUIService);
  
  public abstract com.truecaller.incallui.a b();
  
  public static final class a
  {
    public static i a;
    
    public static i a()
    {
      i locali = a;
      if (locali == null) {
        k.a("instance");
      }
      return locali;
    }
    
    public static void a(i parami)
    {
      k.b(parami, "<set-?>");
      a = parami;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.a.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */