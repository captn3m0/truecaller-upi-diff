package com.truecaller.incallui.a;

import android.content.Context;
import com.truecaller.incallui.callui.InCallUIActivity;
import com.truecaller.incallui.service.InCallUIService;
import com.truecaller.notificationchannels.n;
import com.truecaller.utils.t;
import dagger.a.g;
import javax.inject.Provider;

public final class a
  implements r
{
  private final t b;
  private final com.truecaller.common.a c;
  private final n d;
  private Provider<com.truecaller.incallui.service.c> e;
  private Provider<Context> f;
  private Provider<e> g;
  private Provider<d> h;
  private Provider<com.truecaller.common.account.r> i;
  private Provider<com.truecaller.featuretoggles.e> j;
  private Provider<com.truecaller.incallui.c> k;
  private Provider<com.truecaller.incallui.a> l;
  
  private a(com.truecaller.common.a parama, n paramn, t paramt)
  {
    b = paramt;
    c = parama;
    d = paramn;
    e = dagger.a.c.a(com.truecaller.incallui.service.e.a());
    f = new b(parama);
    g = f.a(f);
    h = dagger.a.c.a(g);
    i = new d(parama);
    j = new c(parama);
    k = com.truecaller.incallui.d.a(i, j);
    l = dagger.a.c.a(k);
  }
  
  public static a a()
  {
    return new a((byte)0);
  }
  
  private c c()
  {
    return new c((Context)g.a(c.b(), "Cannot return null from a non-@Nullable component method"));
  }
  
  public final void a(InCallUIActivity paramInCallUIActivity)
  {
    a = new com.truecaller.incallui.callui.c((com.truecaller.incallui.service.c)e.get(), (c.d.f)g.a(c.r(), "Cannot return null from a non-@Nullable component method"));
  }
  
  public final void a(com.truecaller.incallui.callui.a.a parama)
  {
    a = new com.truecaller.incallui.callui.a.c((com.truecaller.incallui.service.c)e.get(), m.a(), l.a(), c(), (c.d.f)g.a(c.r(), "Cannot return null from a non-@Nullable component method"));
  }
  
  public final void a(com.truecaller.incallui.callui.b.a.a parama)
  {
    a = new com.truecaller.incallui.callui.b.a.d((com.truecaller.incallui.service.c)e.get(), new q());
  }
  
  public final void a(com.truecaller.incallui.callui.b.a parama)
  {
    a = new com.truecaller.incallui.callui.b.c((com.truecaller.incallui.service.c)e.get(), m.a(), (com.truecaller.utils.a)g.a(b.d(), "Cannot return null from a non-@Nullable component method"), c(), (c.d.f)g.a(c.r(), "Cannot return null from a non-@Nullable component method"));
  }
  
  public final void a(InCallUIService paramInCallUIService)
  {
    a = new com.truecaller.incallui.service.b((com.truecaller.incallui.service.c)e.get(), k.a(), (d)h.get(), (com.truecaller.utils.a)g.a(b.d(), "Cannot return null from a non-@Nullable component method"), (c.d.f)g.a(c.r(), "Cannot return null from a non-@Nullable component method"), (c.d.f)g.a(c.t(), "Cannot return null from a non-@Nullable component method"));
    b = ((com.truecaller.notificationchannels.b)g.a(d.d(), "Cannot return null from a non-@Nullable component method"));
  }
  
  public final com.truecaller.incallui.a b()
  {
    return (com.truecaller.incallui.a)l.get();
  }
  
  public static final class a
  {
    public com.truecaller.common.a a;
    public n b;
    public t c;
  }
  
  static final class b
    implements Provider<Context>
  {
    private final com.truecaller.common.a a;
    
    b(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
  
  static final class c
    implements Provider<com.truecaller.featuretoggles.e>
  {
    private final com.truecaller.common.a a;
    
    c(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
  
  static final class d
    implements Provider<com.truecaller.common.account.r>
  {
    private final com.truecaller.common.a a;
    
    d(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */