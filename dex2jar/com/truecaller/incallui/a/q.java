package com.truecaller.incallui.a;

import android.media.ToneGenerator;
import com.truecaller.log.AssertionUtil;
import javax.inject.Inject;

public final class q
  implements p
{
  private final ToneGenerator a;
  
  @Inject
  public q()
  {
    ToneGenerator localToneGenerator2;
    try
    {
      ToneGenerator localToneGenerator1 = new ToneGenerator(0, 70);
    }
    catch (RuntimeException localRuntimeException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localRuntimeException);
      localToneGenerator2 = null;
    }
    a = localToneGenerator2;
  }
  
  public final void a()
  {
    ToneGenerator localToneGenerator = a;
    if (localToneGenerator != null) {
      localToneGenerator.stopTone();
    }
    localToneGenerator = a;
    if (localToneGenerator != null)
    {
      localToneGenerator.release();
      return;
    }
  }
  
  public final void a(char paramChar)
  {
    if (paramChar != '#')
    {
      if (paramChar != '*') {
        switch (paramChar)
        {
        default: 
          return;
        case '9': 
          paramChar = '\t';
          break;
        case '8': 
          paramChar = '\b';
          break;
        case '7': 
          paramChar = '\007';
          break;
        case '6': 
          paramChar = '\006';
          break;
        case '5': 
          paramChar = '\005';
          break;
        case '4': 
          paramChar = '\004';
          break;
        case '3': 
          paramChar = '\003';
          break;
        case '2': 
          paramChar = '\002';
          break;
        case '1': 
          paramChar = '\001';
          break;
        case '0': 
          paramChar = '\000';
          break;
        }
      } else {
        paramChar = '\n';
      }
    }
    else {
      paramChar = '\013';
    }
    ToneGenerator localToneGenerator = a;
    if (localToneGenerator != null)
    {
      localToneGenerator.startTone(paramChar, 200);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.a.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */