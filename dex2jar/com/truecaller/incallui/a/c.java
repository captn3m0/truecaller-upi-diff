package com.truecaller.incallui.a;

import android.content.Context;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Build.VERSION;
import android.os.VibrationEffect;
import android.os.Vibrator;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.extensions.i;
import javax.inject.Inject;

public final class c
  implements b
{
  private final ToneGenerator a;
  private final Vibrator b;
  private final AudioManager c;
  
  @Inject
  public c(Context paramContext)
  {
    b = i.h(paramContext);
    c = i.i(paramContext);
    try
    {
      paramContext = new ToneGenerator(0, 100);
    }
    catch (RuntimeException paramContext)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramContext);
      paramContext = null;
    }
    a = paramContext;
  }
  
  public final void a()
  {
    if (!b.hasVibrator()) {
      return;
    }
    if (c.getRingerMode() == 0) {
      return;
    }
    if (Build.VERSION.SDK_INT >= 26)
    {
      b.vibrate(VibrationEffect.createOneShot(400L, -1));
      return;
    }
    b.vibrate(400L);
  }
  
  public final void a(int paramInt)
  {
    Object localObject;
    if (paramInt != 7)
    {
      if (paramInt != 9) {
        localObject = null;
      } else {
        localObject = Integer.valueOf(35);
      }
    }
    else {
      localObject = Integer.valueOf(96);
    }
    if (localObject == null)
    {
      localObject = a;
      if (localObject != null) {
        ((ToneGenerator)localObject).stopTone();
      }
      return;
    }
    ToneGenerator localToneGenerator = a;
    if (localToneGenerator != null)
    {
      localToneGenerator.startTone(((Integer)localObject).intValue(), 200);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */