package com.truecaller.incallui.service;

import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.incallui.a.d;
import com.truecaller.incallui.a.g;
import com.truecaller.utils.extensions.j;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.a.h;
import kotlinx.coroutines.a.i;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.e;

public final class b
  extends ba<a.c>
  implements a.a
{
  private final h<com.truecaller.incallui.callui.a> c;
  private com.truecaller.incallui.callui.a d;
  private long e;
  private String f;
  private final c g;
  private final g h;
  private final d i;
  private final com.truecaller.utils.a j;
  private final f k;
  private final f l;
  
  @Inject
  public b(c paramc, g paramg, d paramd, com.truecaller.utils.a parama, @Named("UI") f paramf1, @Named("IO") f paramf2)
  {
    super(paramf1);
    g = paramc;
    h = paramg;
    i = paramd;
    j = parama;
    k = paramf1;
    l = paramf2;
    c = i.a(-1);
    f = "";
  }
  
  private final void b(String paramString)
  {
    a.c localc = (a.c)b;
    if (localc != null)
    {
      com.truecaller.incallui.callui.a locala = d;
      String str = paramString;
      if (locala != null)
      {
        str = a;
        if (str == null) {
          str = paramString;
        }
      }
      localc.a(str);
    }
    j();
  }
  
  private final void j()
  {
    Object localObject1 = d;
    Object localObject2 = null;
    if (localObject1 != null) {
      localObject1 = g;
    } else {
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      k();
      return;
    }
    com.truecaller.incallui.callui.a locala = d;
    localObject1 = localObject2;
    if (locala != null) {
      localObject1 = b;
    }
    if (localObject1 != null)
    {
      l();
      return;
    }
  }
  
  private final bn k()
  {
    return e.b(this, null, (m)new b.d(this, null), 3);
  }
  
  private final bn l()
  {
    return e.b(this, null, (m)new b.c(this, null), 3);
  }
  
  public final void a()
  {
    a.c localc = (a.c)b;
    if (localc != null) {
      localc.q();
    }
    localc = (a.c)b;
    if (localc != null)
    {
      localc.r();
      return;
    }
  }
  
  public final void a(char paramChar)
  {
    Object localObject = f;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(paramChar);
    f = localStringBuilder.toString();
    localObject = (a.c)b;
    if (localObject != null)
    {
      ((a.c)localObject).a(paramChar);
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "number");
    h localh = b();
    if (localh != null) {
      j.a(this, localh, (m)new b.a(this, paramString, null));
    }
    e.b(this, null, (m)new b.b(this, paramString, null), 3);
  }
  
  public final void a(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      localc = (a.c)b;
      if (localc != null) {
        localc.i();
      }
      return;
    }
    a.c localc = (a.c)b;
    if (localc != null)
    {
      localc.j();
      return;
    }
  }
  
  public final h<Integer> b()
  {
    a.c localc = (a.c)b;
    if (localc != null) {
      return localc.a();
    }
    return null;
  }
  
  public final void b(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      localc = (a.c)b;
      if (localc != null) {
        localc.l();
      }
      return;
    }
    a.c localc = (a.c)b;
    if (localc != null)
    {
      localc.m();
      return;
    }
  }
  
  public final h<com.truecaller.incallui.callui.a> c()
  {
    return c;
  }
  
  public final void c(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      localc = (a.c)b;
      if (localc != null) {
        localc.n();
      }
      return;
    }
    a.c localc = (a.c)b;
    if (localc != null)
    {
      localc.o();
      return;
    }
  }
  
  public final long d()
  {
    return e;
  }
  
  public final Boolean e()
  {
    a.c localc = (a.c)b;
    if (localc != null) {
      return Boolean.valueOf(localc.h());
    }
    return null;
  }
  
  public final Boolean f()
  {
    a.c localc = (a.c)b;
    if (localc != null) {
      return Boolean.valueOf(localc.k());
    }
    return null;
  }
  
  public final String g()
  {
    return f;
  }
  
  public final void h()
  {
    a.c localc = (a.c)b;
    if (localc != null)
    {
      localc.d();
      return;
    }
  }
  
  public final void i()
  {
    a.c localc = (a.c)b;
    if (localc != null)
    {
      localc.p();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.service.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */