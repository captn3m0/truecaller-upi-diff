package com.truecaller.incallui.service;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.z.d;
import android.support.v4.app.z.e;
import android.support.v4.app.z.g;
import android.telecom.Call;
import android.telecom.Call.Callback;
import android.telecom.Call.Details;
import android.telecom.CallAudioState;
import android.telecom.InCallService;
import android.widget.Toast;
import c.g.b.k;
import c.u;
import com.truecaller.incallui.R.color;
import com.truecaller.incallui.R.drawable;
import com.truecaller.incallui.R.id;
import com.truecaller.incallui.R.string;
import com.truecaller.incallui.a.i.a;
import com.truecaller.incallui.callui.InCallUIActivity;
import com.truecaller.incallui.callui.InCallUIActivity.a;
import com.truecaller.utils.extensions.n;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import kotlinx.coroutines.a.h;

public final class InCallUIService
  extends InCallService
  implements a.c
{
  @Inject
  public a.a a;
  @Inject
  public com.truecaller.notificationchannels.b b;
  final h<Integer> c = kotlinx.coroutines.a.i.a(-1);
  private Call d;
  private AudioManager e;
  private PowerManager.WakeLock f;
  private z.d g;
  private int h = R.id.incallui_service_ongoing_call_notification;
  private final InCallUIService.a i = new InCallUIService.a(this);
  
  private final z.d b(String paramString)
  {
    Context localContext = (Context)this;
    paramString = new z.d(localContext, paramString).a(R.drawable.ic_button_incallui_answer).b().d().f(android.support.v4.content.b.c(localContext, R.color.incallui_header_color)).c("INCALLUI_NOTIFICATION_GROUP").e(2);
    k.a(paramString, "NotificationCompat.Build…ationCompat.PRIORITY_MAX)");
    return paramString;
  }
  
  private final void s()
  {
    z.d locald = g;
    if (locald != null)
    {
      startForeground(h, locald.h());
      return;
    }
  }
  
  public final h<Integer> a()
  {
    return c;
  }
  
  public final void a(char paramChar)
  {
    Call localCall = d;
    if (localCall != null)
    {
      localCall.playDtmfTone(paramChar);
      return;
    }
  }
  
  public final void a(int paramInt, Long paramLong)
  {
    Object localObject1 = d;
    if (localObject1 == null) {
      return;
    }
    Context localContext = (Context)this;
    Object localObject2 = InCallUIActivity.b;
    localObject2 = PendingIntent.getActivity(localContext, 0, InCallUIActivity.a.a(localContext, (Call)localObject1), 0);
    Object localObject3 = b;
    if (localObject3 == null) {
      k.a("notificationChannelProvider");
    }
    localObject2 = b(((com.truecaller.notificationchannels.b)localObject3).j()).b((CharSequence)getString(paramInt)).a((PendingIntent)localObject2);
    paramInt = R.drawable.ic_button_incallui_hangup;
    localObject3 = (CharSequence)getString(R.string.incallui_notification_button_hang_up);
    int j = R.id.incallui_incoming_notification_action_hang_up;
    Object localObject4 = InCallUIActivity.b;
    k.b(localContext, "context");
    k.b(localObject1, "call");
    if (Build.VERSION.SDK_INT < 23)
    {
      localObject1 = null;
    }
    else
    {
      localObject4 = new Intent(localContext, InCallUIActivity.class).setAction("com.truecaller.incallui.callui.ACTION_HANG_UP_CALL").setFlags(268435456);
      localObject1 = ((Call)localObject1).getDetails();
      k.a(localObject1, "call.details");
      localObject1 = ((Intent)localObject4).setData(((Call.Details)localObject1).getHandle());
    }
    localObject1 = ((z.d)localObject2).a(paramInt, (CharSequence)localObject3, PendingIntent.getActivity(localContext, j, (Intent)localObject1, 0));
    if (paramLong != null) {
      ((z.d)localObject1).a(true).a(paramLong.longValue());
    }
    g = ((z.d)localObject1);
    h = R.id.incallui_service_ongoing_call_notification;
    s();
  }
  
  public final void a(Bitmap paramBitmap)
  {
    k.b(paramBitmap, "icon");
    z.d locald = g;
    if (locald != null) {
      locald.a(paramBitmap);
    }
    s();
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "title");
    z.d locald = g;
    if (locald != null) {
      locald.a((CharSequence)paramString);
    }
    s();
  }
  
  public final void b()
  {
    Call localCall = d;
    if (localCall == null) {
      return;
    }
    Context localContext = (Context)this;
    Object localObject = InCallUIActivity.b;
    PendingIntent localPendingIntent = PendingIntent.getActivity(localContext, 0, InCallUIActivity.a.a(localContext, localCall), 0);
    localObject = b;
    if (localObject == null) {
      k.a("notificationChannelProvider");
    }
    z.d locald = b(((com.truecaller.notificationchannels.b)localObject).i()).b((CharSequence)getString(R.string.incallui_notification_incoming_content)).a(localPendingIntent).a("call").a((z.g)new z.e());
    int j = R.drawable.ic_button_incallui_close;
    CharSequence localCharSequence = (CharSequence)getString(R.string.incallui_notification_button_decline);
    int k = R.id.incallui_incoming_notification_action_decline;
    localObject = InCallUIActivity.b;
    k.b(localContext, "context");
    k.b(localCall, "call");
    int m = Build.VERSION.SDK_INT;
    Call.Details localDetails1 = null;
    if (m < 23)
    {
      localObject = null;
    }
    else
    {
      localObject = new Intent(localContext, InCallUIActivity.class).setAction("com.truecaller.incallui.callui.ACTION_DECLINE_CALL").setFlags(268435456);
      Call.Details localDetails2 = localCall.getDetails();
      k.a(localDetails2, "call.details");
      localObject = ((Intent)localObject).setData(localDetails2.getHandle());
    }
    locald = locald.a(j, localCharSequence, PendingIntent.getActivity(localContext, k, (Intent)localObject, 0));
    j = R.drawable.ic_button_incallui_answer;
    localCharSequence = (CharSequence)getString(R.string.incallui_notification_button_answer);
    k = R.id.incallui_incoming_notification_action_answer;
    localObject = InCallUIActivity.b;
    k.b(localContext, "context");
    k.b(localCall, "call");
    if (Build.VERSION.SDK_INT < 23)
    {
      localObject = localDetails1;
    }
    else
    {
      localObject = new Intent(localContext, InCallUIActivity.class).setAction("com.truecaller.incallui.callui.ACTION_ANSWER_CALL").setFlags(268435456);
      localDetails1 = localCall.getDetails();
      k.a(localDetails1, "call.details");
      localObject = ((Intent)localObject).setData(localDetails1.getHandle());
    }
    g = locald.a(j, localCharSequence, PendingIntent.getActivity(localContext, k, (Intent)localObject, 0)).c().c(localPendingIntent);
    h = R.id.incallui_service_incoming_call_notification;
    s();
  }
  
  public final void c()
  {
    Call localCall = d;
    if (localCall == null) {
      return;
    }
    InCallUIActivity.a locala = InCallUIActivity.b;
    startActivity(InCallUIActivity.a.a((Context)this, localCall));
  }
  
  public final void d()
  {
    Call localCall = d;
    if (localCall != null)
    {
      localCall.answer(0);
      return;
    }
  }
  
  public final void e()
  {
    PowerManager.WakeLock localWakeLock = f;
    if (localWakeLock != null)
    {
      if (!localWakeLock.isHeld()) {
        localWakeLock.acquire(TimeUnit.HOURS.toMillis(5L));
      }
      return;
    }
  }
  
  public final boolean f()
  {
    ActivityManager.RunningAppProcessInfo localRunningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
    ActivityManager.getMyMemoryState(localRunningAppProcessInfo);
    return importance != 100;
  }
  
  public final void g()
  {
    Toast.makeText((Context)this, R.string.incallui_status_call_connected, 0).show();
  }
  
  public final boolean h()
  {
    CallAudioState localCallAudioState = getCallAudioState();
    k.a(localCallAudioState, "callAudioState");
    return localCallAudioState.isMuted();
  }
  
  public final void i()
  {
    setMuted(true);
  }
  
  public final void j()
  {
    setMuted(false);
  }
  
  public final boolean k()
  {
    AudioManager localAudioManager = e;
    if (localAudioManager != null) {
      return localAudioManager.isSpeakerphoneOn();
    }
    return false;
  }
  
  public final void l()
  {
    AudioManager localAudioManager = e;
    if (localAudioManager != null)
    {
      localAudioManager.setSpeakerphoneOn(true);
      return;
    }
  }
  
  public final void m()
  {
    AudioManager localAudioManager = e;
    if (localAudioManager != null)
    {
      localAudioManager.setSpeakerphoneOn(false);
      return;
    }
  }
  
  public final void n()
  {
    Call localCall = d;
    if (localCall != null)
    {
      localCall.hold();
      return;
    }
  }
  
  public final void o()
  {
    Call localCall = d;
    if (localCall != null)
    {
      localCall.unhold();
      return;
    }
  }
  
  public final void onCallAdded(Call paramCall)
  {
    k.b(paramCall, "call");
    Object localObject = d;
    if (localObject != null) {
      ((Call)localObject).unregisterCallback((Call.Callback)i);
    }
    if (paramCall != null)
    {
      paramCall.registerCallback((Call.Callback)i);
      c.d_(Integer.valueOf(paramCall.getState()));
    }
    d = paramCall;
    localObject = a;
    if (localObject == null) {
      k.a("presenter");
    }
    paramCall = paramCall.getDetails();
    k.a(paramCall, "call.details");
    paramCall = paramCall.getHandle();
    k.a(paramCall, "call.details.handle");
    paramCall = paramCall.getSchemeSpecificPart();
    k.a(paramCall, "call.details.handle.schemeSpecificPart");
    ((a.a)localObject).a(paramCall);
  }
  
  public final void onCallRemoved(Call paramCall)
  {
    k.b(paramCall, "call");
    paramCall = a;
    if (paramCall == null) {
      k.a("presenter");
    }
    paramCall.a();
  }
  
  public final void onCreate()
  {
    super.onCreate();
    Object localObject = com.truecaller.incallui.a.i.a;
    i.a.a().a(this);
    localObject = getSystemService("audio");
    if (localObject != null)
    {
      e = ((AudioManager)localObject);
      localObject = e;
      if (localObject != null) {
        ((AudioManager)localObject).setMode(2);
      }
      f = n.a(com.truecaller.utils.extensions.i.g(this));
      localObject = a;
      if (localObject == null) {
        k.a("presenter");
      }
      ((a.a)localObject).a(this);
      return;
    }
    throw new u("null cannot be cast to non-null type android.media.AudioManager");
  }
  
  public final void onDestroy()
  {
    a.a locala = a;
    if (locala == null) {
      k.a("presenter");
    }
    locala.y_();
    super.onDestroy();
  }
  
  public final void p()
  {
    Call localCall = d;
    if (localCall != null)
    {
      localCall.disconnect();
      return;
    }
  }
  
  public final void q()
  {
    PowerManager.WakeLock localWakeLock = f;
    if (localWakeLock != null)
    {
      if (localWakeLock.isHeld()) {
        localWakeLock.release();
      }
      return;
    }
  }
  
  public final void r()
  {
    stopForeground(true);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.service.InCallUIService
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */