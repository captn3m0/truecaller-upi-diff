package com.truecaller.incallui;

import android.content.Context;
import com.truecaller.incallui.a.g;
import com.truecaller.incallui.a.n;
import com.truecaller.incallui.a.o;

public abstract interface a
{
  public static final a a = a.a;
  
  public abstract void a(Context paramContext);
  
  public abstract void b(Context paramContext);
  
  public static final class a
  {
    private static g b;
    private static o c;
    private static n d;
    
    public static g a()
    {
      return b;
    }
    
    public static void a(g paramg)
    {
      b = paramg;
    }
    
    public static void a(n paramn)
    {
      d = paramn;
    }
    
    public static void a(o paramo)
    {
      c = paramo;
    }
    
    public static o b()
    {
      return c;
    }
    
    public static n c()
    {
      return d;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.incallui.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */