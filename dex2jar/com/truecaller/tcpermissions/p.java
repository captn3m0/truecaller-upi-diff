package com.truecaller.tcpermissions;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import c.d.f;
import c.g.b.k;
import c.x;
import com.truecaller.common.g.a;
import com.truecaller.common.h.ac;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.l;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;

public final class p
  implements o
{
  c.g.a.b<? super Boolean, x> a;
  final l b;
  private c.g.a.b<? super d, x> c;
  private final kotlinx.coroutines.e.b d;
  private final f e;
  private final Context f;
  private final ac g;
  private final com.truecaller.common.h.c h;
  private final a i;
  
  @Inject
  public p(@Named("UI") f paramf, Context paramContext, l paraml, ac paramac, com.truecaller.common.h.c paramc, a parama)
  {
    e = paramf;
    f = paramContext;
    b = paraml;
    g = paramac;
    h = paramc;
    i = parama;
    d = kotlinx.coroutines.e.d.a();
  }
  
  private final boolean a(List<String> paramList)
  {
    Iterator localIterator = ((Iterable)paramList).iterator();
    while (localIterator.hasNext())
    {
      paramList = localIterator.next();
      if (k.a((String)paramList, "android.permission.READ_CONTACTS")) {
        break label46;
      }
    }
    paramList = null;
    label46:
    paramList = (String)paramList;
    boolean bool2 = false;
    if (paramList == null) {
      return false;
    }
    boolean bool1 = bool2;
    if (h.c())
    {
      bool1 = bool2;
      if (!g.a())
      {
        bool1 = bool2;
        if (!i.a("backup", false)) {
          bool1 = true;
        }
      }
    }
    return bool1;
  }
  
  /* Error */
  public final Object a(PermissionRequestOptions paramPermissionRequestOptions, String[] paramArrayOfString, c.d.c<? super d> paramc)
  {
    // Byte code:
    //   0: aload_3
    //   1: instanceof 259
    //   4: ifeq +36 -> 40
    //   7: aload_3
    //   8: checkcast 259	com/truecaller/tcpermissions/p$e
    //   11: astore 12
    //   13: aload 12
    //   15: getfield 260	com/truecaller/tcpermissions/p$e:b	I
    //   18: ldc -43
    //   20: iand
    //   21: ifeq +19 -> 40
    //   24: aload 12
    //   26: aload 12
    //   28: getfield 260	com/truecaller/tcpermissions/p$e:b	I
    //   31: ldc -43
    //   33: iadd
    //   34: putfield 260	com/truecaller/tcpermissions/p$e:b	I
    //   37: goto +14 -> 51
    //   40: new 259	com/truecaller/tcpermissions/p$e
    //   43: dup
    //   44: aload_0
    //   45: aload_3
    //   46: invokespecial 261	com/truecaller/tcpermissions/p$e:<init>	(Lcom/truecaller/tcpermissions/p;Lc/d/c;)V
    //   49: astore 12
    //   51: aload 12
    //   53: getfield 262	com/truecaller/tcpermissions/p$e:a	Ljava/lang/Object;
    //   56: astore 9
    //   58: getstatic 136	c/d/a/a:a	Lc/d/a/a;
    //   61: astore 15
    //   63: aload 12
    //   65: getfield 260	com/truecaller/tcpermissions/p$e:b	I
    //   68: tableswitch	default:+36->104, 0:+401->469, 1:+282->350, 2:+189->257, 3:+112->180, 4:+46->114
    //   104: new 221	java/lang/IllegalStateException
    //   107: dup
    //   108: ldc -33
    //   110: invokespecial 224	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   113: athrow
    //   114: aload 12
    //   116: getfield 264	com/truecaller/tcpermissions/p$e:h	Ljava/lang/Object;
    //   119: checkcast 8	com/truecaller/tcpermissions/p$a
    //   122: astore 7
    //   124: aload 12
    //   126: getfield 265	com/truecaller/tcpermissions/p$e:d	Ljava/lang/Object;
    //   129: checkcast 2	com/truecaller/tcpermissions/p
    //   132: astore 8
    //   134: aload 7
    //   136: astore_1
    //   137: aload 8
    //   139: astore_3
    //   140: aload 7
    //   142: astore_2
    //   143: aload 8
    //   145: astore 6
    //   147: aload 9
    //   149: instanceof 226
    //   152: ifne +6 -> 158
    //   155: goto +1388 -> 1543
    //   158: aload 7
    //   160: astore_1
    //   161: aload 8
    //   163: astore_3
    //   164: aload 7
    //   166: astore_2
    //   167: aload 8
    //   169: astore 6
    //   171: aload 9
    //   173: checkcast 226	c/o$b
    //   176: getfield 229	c/o$b:a	Ljava/lang/Throwable;
    //   179: athrow
    //   180: aload 12
    //   182: getfield 264	com/truecaller/tcpermissions/p$e:h	Ljava/lang/Object;
    //   185: checkcast 8	com/truecaller/tcpermissions/p$a
    //   188: astore 8
    //   190: aload 12
    //   192: getfield 265	com/truecaller/tcpermissions/p$e:d	Ljava/lang/Object;
    //   195: checkcast 2	com/truecaller/tcpermissions/p
    //   198: astore 7
    //   200: aload 8
    //   202: astore_1
    //   203: aload 7
    //   205: astore_3
    //   206: aload 8
    //   208: astore_2
    //   209: aload 7
    //   211: astore 6
    //   213: aload 9
    //   215: instanceof 226
    //   218: ifne +6 -> 224
    //   221: goto +1140 -> 1361
    //   224: aload 8
    //   226: astore_1
    //   227: aload 7
    //   229: astore_3
    //   230: aload 8
    //   232: astore_2
    //   233: aload 7
    //   235: astore 6
    //   237: aload 9
    //   239: checkcast 226	c/o$b
    //   242: getfield 229	c/o$b:a	Ljava/lang/Throwable;
    //   245: athrow
    //   246: astore_2
    //   247: goto +1422 -> 1669
    //   250: astore_3
    //   251: aload 6
    //   253: astore_1
    //   254: goto +1410 -> 1664
    //   257: aload 12
    //   259: getfield 264	com/truecaller/tcpermissions/p$e:h	Ljava/lang/Object;
    //   262: checkcast 8	com/truecaller/tcpermissions/p$a
    //   265: astore_3
    //   266: aload 12
    //   268: getfield 266	com/truecaller/tcpermissions/p$e:g	Ljava/lang/Object;
    //   271: checkcast 268	java/util/List
    //   274: astore 13
    //   276: aload 12
    //   278: getfield 269	com/truecaller/tcpermissions/p$e:f	Ljava/lang/Object;
    //   281: checkcast 271	[Ljava/lang/String;
    //   284: astore 11
    //   286: aload 12
    //   288: getfield 272	com/truecaller/tcpermissions/p$e:e	Ljava/lang/Object;
    //   291: checkcast 274	com/truecaller/tcpermissions/PermissionRequestOptions
    //   294: astore 10
    //   296: aload 12
    //   298: getfield 265	com/truecaller/tcpermissions/p$e:d	Ljava/lang/Object;
    //   301: checkcast 2	com/truecaller/tcpermissions/p
    //   304: astore 6
    //   306: aload 6
    //   308: astore_1
    //   309: aload_3
    //   310: astore_2
    //   311: aload 6
    //   313: astore 8
    //   315: aload_3
    //   316: astore 7
    //   318: aload 9
    //   320: instanceof 226
    //   323: ifne +6 -> 329
    //   326: goto +760 -> 1086
    //   329: aload 6
    //   331: astore_1
    //   332: aload_3
    //   333: astore_2
    //   334: aload 6
    //   336: astore 8
    //   338: aload_3
    //   339: astore 7
    //   341: aload 9
    //   343: checkcast 226	c/o$b
    //   346: getfield 229	c/o$b:a	Ljava/lang/Throwable;
    //   349: athrow
    //   350: aload 12
    //   352: getfield 264	com/truecaller/tcpermissions/p$e:h	Ljava/lang/Object;
    //   355: checkcast 8	com/truecaller/tcpermissions/p$a
    //   358: astore_3
    //   359: aload 12
    //   361: getfield 266	com/truecaller/tcpermissions/p$e:g	Ljava/lang/Object;
    //   364: checkcast 268	java/util/List
    //   367: astore 13
    //   369: aload 12
    //   371: getfield 269	com/truecaller/tcpermissions/p$e:f	Ljava/lang/Object;
    //   374: checkcast 271	[Ljava/lang/String;
    //   377: astore 11
    //   379: aload 12
    //   381: getfield 272	com/truecaller/tcpermissions/p$e:e	Ljava/lang/Object;
    //   384: checkcast 274	com/truecaller/tcpermissions/PermissionRequestOptions
    //   387: astore 10
    //   389: aload 12
    //   391: getfield 265	com/truecaller/tcpermissions/p$e:d	Ljava/lang/Object;
    //   394: checkcast 2	com/truecaller/tcpermissions/p
    //   397: astore 6
    //   399: aload 6
    //   401: astore_1
    //   402: aload_3
    //   403: astore_2
    //   404: aload 6
    //   406: astore 8
    //   408: aload_3
    //   409: astore 7
    //   411: aload 9
    //   413: instanceof 226
    //   416: ifne +10 -> 426
    //   419: aload 13
    //   421: astore 9
    //   423: goto +315 -> 738
    //   426: aload 6
    //   428: astore_1
    //   429: aload_3
    //   430: astore_2
    //   431: aload 6
    //   433: astore 8
    //   435: aload_3
    //   436: astore 7
    //   438: aload 9
    //   440: checkcast 226	c/o$b
    //   443: getfield 229	c/o$b:a	Ljava/lang/Throwable;
    //   446: athrow
    //   447: astore 6
    //   449: aload_1
    //   450: astore_3
    //   451: aload_2
    //   452: astore_1
    //   453: aload 6
    //   455: astore_2
    //   456: goto +1213 -> 1669
    //   459: astore_3
    //   460: aload 8
    //   462: astore_1
    //   463: aload 7
    //   465: astore_2
    //   466: goto +1198 -> 1664
    //   469: aload 9
    //   471: instanceof 226
    //   474: ifne +1259 -> 1733
    //   477: getstatic 279	android/os/Build$VERSION:SDK_INT	I
    //   480: bipush 23
    //   482: if_icmpge +9 -> 491
    //   485: iconst_1
    //   486: istore 4
    //   488: goto +6 -> 494
    //   491: iconst_0
    //   492: istore 4
    //   494: iload 4
    //   496: ifeq +12 -> 508
    //   499: new 231	com/truecaller/tcpermissions/d
    //   502: dup
    //   503: iconst_1
    //   504: invokespecial 250	com/truecaller/tcpermissions/d:<init>	(Z)V
    //   507: areturn
    //   508: new 150	java/util/ArrayList
    //   511: dup
    //   512: invokespecial 280	java/util/ArrayList:<init>	()V
    //   515: checkcast 152	java/util/Collection
    //   518: astore_3
    //   519: aload_2
    //   520: arraylength
    //   521: istore 5
    //   523: iconst_0
    //   524: istore 4
    //   526: iload 4
    //   528: iload 5
    //   530: if_icmpge +50 -> 580
    //   533: aload_2
    //   534: iload 4
    //   536: aaload
    //   537: astore 6
    //   539: aload_0
    //   540: getfield 59	com/truecaller/tcpermissions/p:b	Lcom/truecaller/utils/l;
    //   543: iconst_1
    //   544: anewarray 185	java/lang/String
    //   547: dup
    //   548: iconst_0
    //   549: aload 6
    //   551: aastore
    //   552: invokeinterface 285 2 0
    //   557: iconst_1
    //   558: ixor
    //   559: ifeq +12 -> 571
    //   562: aload_3
    //   563: aload 6
    //   565: invokeinterface 289 2 0
    //   570: pop
    //   571: iload 4
    //   573: iconst_1
    //   574: iadd
    //   575: istore 4
    //   577: goto -51 -> 526
    //   580: aload_3
    //   581: checkcast 268	java/util/List
    //   584: astore 9
    //   586: aload 9
    //   588: invokeinterface 292 1 0
    //   593: ifeq +12 -> 605
    //   596: new 231	com/truecaller/tcpermissions/d
    //   599: dup
    //   600: iconst_1
    //   601: invokespecial 250	com/truecaller/tcpermissions/d:<init>	(Z)V
    //   604: areturn
    //   605: new 99	java/lang/StringBuilder
    //   608: dup
    //   609: ldc_w 294
    //   612: invokespecial 104	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   615: astore_3
    //   616: aload_3
    //   617: aload 9
    //   619: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   622: pop
    //   623: aload_3
    //   624: ldc_w 296
    //   627: invokevirtual 299	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   630: pop
    //   631: aload_3
    //   632: aload_1
    //   633: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   636: pop
    //   637: aload_3
    //   638: invokevirtual 118	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   641: pop
    //   642: new 8	com/truecaller/tcpermissions/p$a
    //   645: dup
    //   646: aload_0
    //   647: aload 9
    //   649: invokespecial 237	com/truecaller/tcpermissions/p$a:<init>	(Lcom/truecaller/tcpermissions/p;Ljava/util/List;)V
    //   652: astore_3
    //   653: aload_0
    //   654: getfield 72	com/truecaller/tcpermissions/p:d	Lkotlinx/coroutines/e/b;
    //   657: invokeinterface 302 1 0
    //   662: ifeq +3 -> 665
    //   665: aload_0
    //   666: getfield 72	com/truecaller/tcpermissions/p:d	Lkotlinx/coroutines/e/b;
    //   669: astore 6
    //   671: aload 12
    //   673: aload_0
    //   674: putfield 265	com/truecaller/tcpermissions/p$e:d	Ljava/lang/Object;
    //   677: aload 12
    //   679: aload_1
    //   680: putfield 272	com/truecaller/tcpermissions/p$e:e	Ljava/lang/Object;
    //   683: aload 12
    //   685: aload_2
    //   686: putfield 269	com/truecaller/tcpermissions/p$e:f	Ljava/lang/Object;
    //   689: aload 12
    //   691: aload 9
    //   693: putfield 266	com/truecaller/tcpermissions/p$e:g	Ljava/lang/Object;
    //   696: aload 12
    //   698: aload_3
    //   699: putfield 264	com/truecaller/tcpermissions/p$e:h	Ljava/lang/Object;
    //   702: aload 12
    //   704: iconst_1
    //   705: putfield 260	com/truecaller/tcpermissions/p$e:b	I
    //   708: aload 6
    //   710: aload 12
    //   712: invokeinterface 305 2 0
    //   717: astore 6
    //   719: aload 6
    //   721: aload 15
    //   723: if_acmpne +6 -> 729
    //   726: aload 15
    //   728: areturn
    //   729: aload_0
    //   730: astore 6
    //   732: aload_1
    //   733: astore 10
    //   735: aload_2
    //   736: astore 11
    //   738: aload 6
    //   740: astore_1
    //   741: aload_3
    //   742: astore_2
    //   743: aload 6
    //   745: astore 8
    //   747: aload_3
    //   748: astore 7
    //   750: aload 10
    //   752: getfield 307	com/truecaller/tcpermissions/PermissionRequestOptions:b	Z
    //   755: ifeq +635 -> 1390
    //   758: aload 6
    //   760: astore_1
    //   761: aload_3
    //   762: astore_2
    //   763: aload 6
    //   765: astore 8
    //   767: aload_3
    //   768: astore 7
    //   770: aload 6
    //   772: aload 9
    //   774: invokespecial 309	com/truecaller/tcpermissions/p:a	(Ljava/util/List;)Z
    //   777: ifeq +613 -> 1390
    //   780: aload 6
    //   782: astore_1
    //   783: aload_3
    //   784: astore_2
    //   785: aload 6
    //   787: astore 8
    //   789: aload_3
    //   790: astore 7
    //   792: aload 12
    //   794: aload 6
    //   796: putfield 265	com/truecaller/tcpermissions/p$e:d	Ljava/lang/Object;
    //   799: aload 6
    //   801: astore_1
    //   802: aload_3
    //   803: astore_2
    //   804: aload 6
    //   806: astore 8
    //   808: aload_3
    //   809: astore 7
    //   811: aload 12
    //   813: aload 10
    //   815: putfield 272	com/truecaller/tcpermissions/p$e:e	Ljava/lang/Object;
    //   818: aload 6
    //   820: astore_1
    //   821: aload_3
    //   822: astore_2
    //   823: aload 6
    //   825: astore 8
    //   827: aload_3
    //   828: astore 7
    //   830: aload 12
    //   832: aload 11
    //   834: putfield 269	com/truecaller/tcpermissions/p$e:f	Ljava/lang/Object;
    //   837: aload 6
    //   839: astore_1
    //   840: aload_3
    //   841: astore_2
    //   842: aload 6
    //   844: astore 8
    //   846: aload_3
    //   847: astore 7
    //   849: aload 12
    //   851: aload 9
    //   853: putfield 266	com/truecaller/tcpermissions/p$e:g	Ljava/lang/Object;
    //   856: aload 6
    //   858: astore_1
    //   859: aload_3
    //   860: astore_2
    //   861: aload 6
    //   863: astore 8
    //   865: aload_3
    //   866: astore 7
    //   868: aload 12
    //   870: aload_3
    //   871: putfield 264	com/truecaller/tcpermissions/p$e:h	Ljava/lang/Object;
    //   874: aload 6
    //   876: astore_1
    //   877: aload_3
    //   878: astore_2
    //   879: aload 6
    //   881: astore 8
    //   883: aload_3
    //   884: astore 7
    //   886: aload 12
    //   888: iconst_2
    //   889: putfield 260	com/truecaller/tcpermissions/p$e:b	I
    //   892: aload 6
    //   894: astore_1
    //   895: aload_3
    //   896: astore_2
    //   897: aload 6
    //   899: astore 8
    //   901: aload_3
    //   902: astore 7
    //   904: new 78	kotlinx/coroutines/k
    //   907: dup
    //   908: aload 12
    //   910: invokestatic 83	c/d/a/b:a	(Lc/d/c;)Lc/d/c;
    //   913: iconst_1
    //   914: invokespecial 86	kotlinx/coroutines/k:<init>	(Lc/d/c;I)V
    //   917: astore 13
    //   919: aload 6
    //   921: astore_1
    //   922: aload_3
    //   923: astore_2
    //   924: aload 6
    //   926: astore 8
    //   928: aload_3
    //   929: astore 7
    //   931: aload 6
    //   933: new 311	com/truecaller/tcpermissions/p$c
    //   936: dup
    //   937: aload 13
    //   939: checkcast 90	kotlinx/coroutines/j
    //   942: aload 6
    //   944: invokespecial 314	com/truecaller/tcpermissions/p$c:<init>	(Lkotlinx/coroutines/j;Lcom/truecaller/tcpermissions/p;)V
    //   947: checkcast 95	c/g/a/b
    //   950: putfield 97	com/truecaller/tcpermissions/p:c	Lc/g/a/b;
    //   953: aload 6
    //   955: astore_1
    //   956: aload_3
    //   957: astore_2
    //   958: aload 6
    //   960: astore 8
    //   962: aload_3
    //   963: astore 7
    //   965: new 143	android/content/Intent
    //   968: dup
    //   969: aload 6
    //   971: getfield 57	com/truecaller/tcpermissions/p:f	Landroid/content/Context;
    //   974: ldc_w 316
    //   977: invokespecial 319	android/content/Intent:<init>	(Landroid/content/Context;Ljava/lang/Class;)V
    //   980: astore 14
    //   982: aload 6
    //   984: astore_1
    //   985: aload_3
    //   986: astore_2
    //   987: aload 6
    //   989: astore 8
    //   991: aload_3
    //   992: astore 7
    //   994: aload 14
    //   996: ldc_w 320
    //   999: invokevirtual 324	android/content/Intent:setFlags	(I)Landroid/content/Intent;
    //   1002: pop
    //   1003: aload 6
    //   1005: astore_1
    //   1006: aload_3
    //   1007: astore_2
    //   1008: aload 6
    //   1010: astore 8
    //   1012: aload_3
    //   1013: astore 7
    //   1015: aload 6
    //   1017: getfield 57	com/truecaller/tcpermissions/p:f	Landroid/content/Context;
    //   1020: aload 14
    //   1022: invokevirtual 330	android/content/Context:startActivity	(Landroid/content/Intent;)V
    //   1025: aload 6
    //   1027: astore_1
    //   1028: aload_3
    //   1029: astore_2
    //   1030: aload 6
    //   1032: astore 8
    //   1034: aload_3
    //   1035: astore 7
    //   1037: aload 13
    //   1039: invokevirtual 131	kotlinx/coroutines/k:h	()Ljava/lang/Object;
    //   1042: astore 14
    //   1044: aload 6
    //   1046: astore_1
    //   1047: aload_3
    //   1048: astore_2
    //   1049: aload 6
    //   1051: astore 8
    //   1053: aload_3
    //   1054: astore 7
    //   1056: aload 14
    //   1058: getstatic 136	c/d/a/a:a	Lc/d/a/a;
    //   1061: if_acmpne +681 -> 1742
    //   1064: aload 6
    //   1066: astore_1
    //   1067: aload_3
    //   1068: astore_2
    //   1069: aload 6
    //   1071: astore 8
    //   1073: aload_3
    //   1074: astore 7
    //   1076: aload 12
    //   1078: ldc -118
    //   1080: invokestatic 40	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1083: goto +659 -> 1742
    //   1086: aload 6
    //   1088: astore_1
    //   1089: aload_3
    //   1090: astore_2
    //   1091: aload 6
    //   1093: astore 8
    //   1095: aload_3
    //   1096: astore 7
    //   1098: aload 9
    //   1100: checkcast 231	com/truecaller/tcpermissions/d
    //   1103: astore 9
    //   1105: aload 6
    //   1107: astore_1
    //   1108: aload_3
    //   1109: astore_2
    //   1110: aload 6
    //   1112: astore 8
    //   1114: aload_3
    //   1115: astore 7
    //   1117: new 99	java/lang/StringBuilder
    //   1120: dup
    //   1121: ldc_w 332
    //   1124: invokespecial 104	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1127: astore 14
    //   1129: aload 6
    //   1131: astore_1
    //   1132: aload_3
    //   1133: astore_2
    //   1134: aload 6
    //   1136: astore 8
    //   1138: aload_3
    //   1139: astore 7
    //   1141: aload 14
    //   1143: aload 9
    //   1145: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1148: pop
    //   1149: aload 6
    //   1151: astore_1
    //   1152: aload_3
    //   1153: astore_2
    //   1154: aload 6
    //   1156: astore 8
    //   1158: aload_3
    //   1159: astore 7
    //   1161: aload 14
    //   1163: bipush 46
    //   1165: invokevirtual 114	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   1168: pop
    //   1169: aload 6
    //   1171: astore_1
    //   1172: aload_3
    //   1173: astore_2
    //   1174: aload 6
    //   1176: astore 8
    //   1178: aload_3
    //   1179: astore 7
    //   1181: aload 14
    //   1183: invokevirtual 118	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1186: pop
    //   1187: aload 6
    //   1189: astore_1
    //   1190: aload_3
    //   1191: astore_2
    //   1192: aload 6
    //   1194: astore 8
    //   1196: aload_3
    //   1197: astore 7
    //   1199: aload 12
    //   1201: aload 6
    //   1203: putfield 265	com/truecaller/tcpermissions/p$e:d	Ljava/lang/Object;
    //   1206: aload 6
    //   1208: astore_1
    //   1209: aload_3
    //   1210: astore_2
    //   1211: aload 6
    //   1213: astore 8
    //   1215: aload_3
    //   1216: astore 7
    //   1218: aload 12
    //   1220: aload 10
    //   1222: putfield 272	com/truecaller/tcpermissions/p$e:e	Ljava/lang/Object;
    //   1225: aload 6
    //   1227: astore_1
    //   1228: aload_3
    //   1229: astore_2
    //   1230: aload 6
    //   1232: astore 8
    //   1234: aload_3
    //   1235: astore 7
    //   1237: aload 12
    //   1239: aload 11
    //   1241: putfield 269	com/truecaller/tcpermissions/p$e:f	Ljava/lang/Object;
    //   1244: aload 6
    //   1246: astore_1
    //   1247: aload_3
    //   1248: astore_2
    //   1249: aload 6
    //   1251: astore 8
    //   1253: aload_3
    //   1254: astore 7
    //   1256: aload 12
    //   1258: aload 13
    //   1260: putfield 266	com/truecaller/tcpermissions/p$e:g	Ljava/lang/Object;
    //   1263: aload 6
    //   1265: astore_1
    //   1266: aload_3
    //   1267: astore_2
    //   1268: aload 6
    //   1270: astore 8
    //   1272: aload_3
    //   1273: astore 7
    //   1275: aload 12
    //   1277: aload_3
    //   1278: putfield 264	com/truecaller/tcpermissions/p$e:h	Ljava/lang/Object;
    //   1281: aload 6
    //   1283: astore_1
    //   1284: aload_3
    //   1285: astore_2
    //   1286: aload 6
    //   1288: astore 8
    //   1290: aload_3
    //   1291: astore 7
    //   1293: aload 12
    //   1295: aload 9
    //   1297: putfield 334	com/truecaller/tcpermissions/p$e:i	Ljava/lang/Object;
    //   1300: aload 6
    //   1302: astore_1
    //   1303: aload_3
    //   1304: astore_2
    //   1305: aload 6
    //   1307: astore 8
    //   1309: aload_3
    //   1310: astore 7
    //   1312: aload 12
    //   1314: iconst_3
    //   1315: putfield 260	com/truecaller/tcpermissions/p$e:b	I
    //   1318: aload 6
    //   1320: astore_1
    //   1321: aload_3
    //   1322: astore_2
    //   1323: aload 6
    //   1325: astore 8
    //   1327: aload_3
    //   1328: astore 7
    //   1330: aload 6
    //   1332: aload 10
    //   1334: aload 9
    //   1336: aload_3
    //   1337: aload 12
    //   1339: invokevirtual 336	com/truecaller/tcpermissions/p:a	(Lcom/truecaller/tcpermissions/PermissionRequestOptions;Lcom/truecaller/tcpermissions/d;Lcom/truecaller/tcpermissions/p$a;Lc/d/c;)Ljava/lang/Object;
    //   1342: astore 9
    //   1344: aload 9
    //   1346: aload 15
    //   1348: if_acmpne +6 -> 1354
    //   1351: aload 15
    //   1353: areturn
    //   1354: aload 6
    //   1356: astore 7
    //   1358: aload_3
    //   1359: astore 8
    //   1361: aload 8
    //   1363: astore_1
    //   1364: aload 7
    //   1366: astore_3
    //   1367: aload 8
    //   1369: astore_2
    //   1370: aload 7
    //   1372: astore 6
    //   1374: aload 9
    //   1376: checkcast 231	com/truecaller/tcpermissions/d
    //   1379: astore 9
    //   1381: aload 7
    //   1383: astore_2
    //   1384: aload 9
    //   1386: astore_1
    //   1387: goto +186 -> 1573
    //   1390: aload 6
    //   1392: astore_1
    //   1393: aload_3
    //   1394: astore_2
    //   1395: aload 6
    //   1397: astore 8
    //   1399: aload_3
    //   1400: astore 7
    //   1402: aload 12
    //   1404: aload 6
    //   1406: putfield 265	com/truecaller/tcpermissions/p$e:d	Ljava/lang/Object;
    //   1409: aload 6
    //   1411: astore_1
    //   1412: aload_3
    //   1413: astore_2
    //   1414: aload 6
    //   1416: astore 8
    //   1418: aload_3
    //   1419: astore 7
    //   1421: aload 12
    //   1423: aload 10
    //   1425: putfield 272	com/truecaller/tcpermissions/p$e:e	Ljava/lang/Object;
    //   1428: aload 6
    //   1430: astore_1
    //   1431: aload_3
    //   1432: astore_2
    //   1433: aload 6
    //   1435: astore 8
    //   1437: aload_3
    //   1438: astore 7
    //   1440: aload 12
    //   1442: aload 11
    //   1444: putfield 269	com/truecaller/tcpermissions/p$e:f	Ljava/lang/Object;
    //   1447: aload 6
    //   1449: astore_1
    //   1450: aload_3
    //   1451: astore_2
    //   1452: aload 6
    //   1454: astore 8
    //   1456: aload_3
    //   1457: astore 7
    //   1459: aload 12
    //   1461: aload 9
    //   1463: putfield 266	com/truecaller/tcpermissions/p$e:g	Ljava/lang/Object;
    //   1466: aload 6
    //   1468: astore_1
    //   1469: aload_3
    //   1470: astore_2
    //   1471: aload 6
    //   1473: astore 8
    //   1475: aload_3
    //   1476: astore 7
    //   1478: aload 12
    //   1480: aload_3
    //   1481: putfield 264	com/truecaller/tcpermissions/p$e:h	Ljava/lang/Object;
    //   1484: aload 6
    //   1486: astore_1
    //   1487: aload_3
    //   1488: astore_2
    //   1489: aload 6
    //   1491: astore 8
    //   1493: aload_3
    //   1494: astore 7
    //   1496: aload 12
    //   1498: iconst_4
    //   1499: putfield 260	com/truecaller/tcpermissions/p$e:b	I
    //   1502: aload 6
    //   1504: astore_1
    //   1505: aload_3
    //   1506: astore_2
    //   1507: aload 6
    //   1509: astore 8
    //   1511: aload_3
    //   1512: astore 7
    //   1514: aload 6
    //   1516: aload 10
    //   1518: aload_3
    //   1519: aload 12
    //   1521: invokespecial 247	com/truecaller/tcpermissions/p:a	(Lcom/truecaller/tcpermissions/PermissionRequestOptions;Lcom/truecaller/tcpermissions/p$a;Lc/d/c;)Ljava/lang/Object;
    //   1524: astore 9
    //   1526: aload 9
    //   1528: aload 15
    //   1530: if_acmpne +6 -> 1536
    //   1533: aload 15
    //   1535: areturn
    //   1536: aload 6
    //   1538: astore 8
    //   1540: aload_3
    //   1541: astore 7
    //   1543: aload 7
    //   1545: astore_1
    //   1546: aload 8
    //   1548: astore_3
    //   1549: aload 7
    //   1551: astore_2
    //   1552: aload 8
    //   1554: astore 6
    //   1556: aload 9
    //   1558: checkcast 231	com/truecaller/tcpermissions/d
    //   1561: astore 9
    //   1563: aload 9
    //   1565: astore_1
    //   1566: aload 8
    //   1568: astore_2
    //   1569: aload 7
    //   1571: astore 8
    //   1573: aload_2
    //   1574: aconst_null
    //   1575: putfield 97	com/truecaller/tcpermissions/p:c	Lc/g/a/b;
    //   1578: aload_2
    //   1579: getfield 72	com/truecaller/tcpermissions/p:d	Lkotlinx/coroutines/e/b;
    //   1582: invokeinterface 302 1 0
    //   1587: ifeq +12 -> 1599
    //   1590: aload_2
    //   1591: getfield 72	com/truecaller/tcpermissions/p:d	Lkotlinx/coroutines/e/b;
    //   1594: invokeinterface 338 1 0
    //   1599: new 99	java/lang/StringBuilder
    //   1602: dup
    //   1603: ldc_w 340
    //   1606: invokespecial 104	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1609: astore_2
    //   1610: aload_2
    //   1611: aload 8
    //   1613: invokevirtual 235	com/truecaller/tcpermissions/p$a:a	()Z
    //   1616: invokevirtual 343	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
    //   1619: pop
    //   1620: aload_2
    //   1621: bipush 46
    //   1623: invokevirtual 114	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   1626: pop
    //   1627: aload_2
    //   1628: invokevirtual 118	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1631: pop
    //   1632: aload 8
    //   1634: invokevirtual 344	com/truecaller/tcpermissions/p$a:b	()V
    //   1637: aload_1
    //   1638: areturn
    //   1639: astore 6
    //   1641: aload_0
    //   1642: astore_1
    //   1643: aload_3
    //   1644: astore_2
    //   1645: aload_1
    //   1646: astore_3
    //   1647: aload_2
    //   1648: astore_1
    //   1649: aload 6
    //   1651: astore_2
    //   1652: goto +17 -> 1669
    //   1655: astore 6
    //   1657: aload_0
    //   1658: astore_1
    //   1659: aload_3
    //   1660: astore_2
    //   1661: aload 6
    //   1663: astore_3
    //   1664: aload_3
    //   1665: checkcast 346	java/lang/Throwable
    //   1668: athrow
    //   1669: aload_3
    //   1670: aconst_null
    //   1671: putfield 97	com/truecaller/tcpermissions/p:c	Lc/g/a/b;
    //   1674: aload_3
    //   1675: getfield 72	com/truecaller/tcpermissions/p:d	Lkotlinx/coroutines/e/b;
    //   1678: invokeinterface 302 1 0
    //   1683: ifeq +12 -> 1695
    //   1686: aload_3
    //   1687: getfield 72	com/truecaller/tcpermissions/p:d	Lkotlinx/coroutines/e/b;
    //   1690: invokeinterface 338 1 0
    //   1695: new 99	java/lang/StringBuilder
    //   1698: dup
    //   1699: ldc_w 340
    //   1702: invokespecial 104	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1705: astore_3
    //   1706: aload_3
    //   1707: aload_1
    //   1708: invokevirtual 235	com/truecaller/tcpermissions/p$a:a	()Z
    //   1711: invokevirtual 343	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
    //   1714: pop
    //   1715: aload_3
    //   1716: bipush 46
    //   1718: invokevirtual 114	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   1721: pop
    //   1722: aload_3
    //   1723: invokevirtual 118	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1726: pop
    //   1727: aload_1
    //   1728: invokevirtual 344	com/truecaller/tcpermissions/p$a:b	()V
    //   1731: aload_2
    //   1732: athrow
    //   1733: aload 9
    //   1735: checkcast 226	c/o$b
    //   1738: getfield 229	c/o$b:a	Ljava/lang/Throwable;
    //   1741: athrow
    //   1742: aload 9
    //   1744: astore 13
    //   1746: aload 14
    //   1748: astore 9
    //   1750: aload 14
    //   1752: aload 15
    //   1754: if_acmpne -668 -> 1086
    //   1757: aload 15
    //   1759: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1760	0	this	p
    //   0	1760	1	paramPermissionRequestOptions	PermissionRequestOptions
    //   0	1760	2	paramArrayOfString	String[]
    //   0	1760	3	paramc	c.d.c<? super d>
    //   486	90	4	j	int
    //   521	10	5	k	int
    //   145	287	6	localObject1	Object
    //   447	7	6	localObject2	Object
    //   537	1018	6	localObject3	Object
    //   1639	11	6	localObject4	Object
    //   1655	7	6	localCancellationException	java.util.concurrent.CancellationException
    //   122	1448	7	localObject5	Object
    //   132	1501	8	localObject6	Object
    //   56	1693	9	localObject7	Object
    //   294	1223	10	localPermissionRequestOptions	PermissionRequestOptions
    //   284	1159	11	arrayOfString	String[]
    //   11	1509	12	locale	p.e
    //   274	1471	13	localObject8	Object
    //   980	771	14	localObject9	Object
    //   61	1697	15	locala	c.d.a.a
    // Exception table:
    //   from	to	target	type
    //   147	155	246	finally
    //   171	180	246	finally
    //   213	221	246	finally
    //   237	246	246	finally
    //   1374	1381	246	finally
    //   1556	1563	246	finally
    //   147	155	250	java/util/concurrent/CancellationException
    //   171	180	250	java/util/concurrent/CancellationException
    //   213	221	250	java/util/concurrent/CancellationException
    //   237	246	250	java/util/concurrent/CancellationException
    //   1374	1381	250	java/util/concurrent/CancellationException
    //   1556	1563	250	java/util/concurrent/CancellationException
    //   318	326	447	finally
    //   341	350	447	finally
    //   411	419	447	finally
    //   438	447	447	finally
    //   750	758	447	finally
    //   770	780	447	finally
    //   792	799	447	finally
    //   811	818	447	finally
    //   830	837	447	finally
    //   849	856	447	finally
    //   868	874	447	finally
    //   886	892	447	finally
    //   904	919	447	finally
    //   931	953	447	finally
    //   965	982	447	finally
    //   994	1003	447	finally
    //   1015	1025	447	finally
    //   1037	1044	447	finally
    //   1056	1064	447	finally
    //   1076	1083	447	finally
    //   1098	1105	447	finally
    //   1117	1129	447	finally
    //   1141	1149	447	finally
    //   1161	1169	447	finally
    //   1181	1187	447	finally
    //   1199	1206	447	finally
    //   1218	1225	447	finally
    //   1237	1244	447	finally
    //   1256	1263	447	finally
    //   1275	1281	447	finally
    //   1293	1300	447	finally
    //   1312	1318	447	finally
    //   1330	1344	447	finally
    //   1402	1409	447	finally
    //   1421	1428	447	finally
    //   1440	1447	447	finally
    //   1459	1466	447	finally
    //   1478	1484	447	finally
    //   1496	1502	447	finally
    //   1514	1526	447	finally
    //   1664	1669	447	finally
    //   318	326	459	java/util/concurrent/CancellationException
    //   341	350	459	java/util/concurrent/CancellationException
    //   411	419	459	java/util/concurrent/CancellationException
    //   438	447	459	java/util/concurrent/CancellationException
    //   750	758	459	java/util/concurrent/CancellationException
    //   770	780	459	java/util/concurrent/CancellationException
    //   792	799	459	java/util/concurrent/CancellationException
    //   811	818	459	java/util/concurrent/CancellationException
    //   830	837	459	java/util/concurrent/CancellationException
    //   849	856	459	java/util/concurrent/CancellationException
    //   868	874	459	java/util/concurrent/CancellationException
    //   886	892	459	java/util/concurrent/CancellationException
    //   904	919	459	java/util/concurrent/CancellationException
    //   931	953	459	java/util/concurrent/CancellationException
    //   965	982	459	java/util/concurrent/CancellationException
    //   994	1003	459	java/util/concurrent/CancellationException
    //   1015	1025	459	java/util/concurrent/CancellationException
    //   1037	1044	459	java/util/concurrent/CancellationException
    //   1056	1064	459	java/util/concurrent/CancellationException
    //   1076	1083	459	java/util/concurrent/CancellationException
    //   1098	1105	459	java/util/concurrent/CancellationException
    //   1117	1129	459	java/util/concurrent/CancellationException
    //   1141	1149	459	java/util/concurrent/CancellationException
    //   1161	1169	459	java/util/concurrent/CancellationException
    //   1181	1187	459	java/util/concurrent/CancellationException
    //   1199	1206	459	java/util/concurrent/CancellationException
    //   1218	1225	459	java/util/concurrent/CancellationException
    //   1237	1244	459	java/util/concurrent/CancellationException
    //   1256	1263	459	java/util/concurrent/CancellationException
    //   1275	1281	459	java/util/concurrent/CancellationException
    //   1293	1300	459	java/util/concurrent/CancellationException
    //   1312	1318	459	java/util/concurrent/CancellationException
    //   1330	1344	459	java/util/concurrent/CancellationException
    //   1402	1409	459	java/util/concurrent/CancellationException
    //   1421	1428	459	java/util/concurrent/CancellationException
    //   1440	1447	459	java/util/concurrent/CancellationException
    //   1459	1466	459	java/util/concurrent/CancellationException
    //   1478	1484	459	java/util/concurrent/CancellationException
    //   1496	1502	459	java/util/concurrent/CancellationException
    //   1514	1526	459	java/util/concurrent/CancellationException
    //   653	665	1639	finally
    //   665	719	1639	finally
    //   653	665	1655	java/util/concurrent/CancellationException
    //   665	719	1655	java/util/concurrent/CancellationException
  }
  
  public final Object a(String[] paramArrayOfString, c.d.c<? super d> paramc)
  {
    return a(new PermissionRequestOptions(false, false, null, 7), (String[])Arrays.copyOf(paramArrayOfString, paramArrayOfString.length), paramc);
  }
  
  public final void a(c.g.a.b<? super Boolean, x> paramb)
  {
    k.b(paramb, "callback");
    a = paramb;
  }
  
  public final void a(d paramd)
  {
    k.b(paramd, "result");
    c.g.a.b localb = c;
    if (localb == null) {
      return;
    }
    c = null;
    localb.invoke(paramd);
  }
  
  public final boolean a()
  {
    Intent localIntent = new Intent("miui.intent.action.APP_PERM_EDITOR");
    for (;;)
    {
      try
      {
        localIntent.setClassName("com.miui.securitycenter", "com.miui.permcenter.permissions.PermissionsEditorActivity");
        localIntent.setFlags(268435456);
        localIntent.putExtra("extra_pkgname", f.getPackageName());
        f.startActivity(localIntent);
        return true;
      }
      catch (RuntimeException localRuntimeException3)
      {
        continue;
      }
      try
      {
        localIntent.setClassName("com.miui.securitycenter", "com.miui.permcenter.permissions.AppPermissionsEditorActivity");
        f.startActivity(localIntent);
        return true;
      }
      catch (RuntimeException localRuntimeException2)
      {
        continue;
      }
      try
      {
        localIntent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS").setFlags(268435456).setData(Uri.fromParts("package", f.getPackageName(), null));
        k.a(localIntent, "appSettingsIntent()");
        f.startActivity(localIntent);
        return true;
      }
      catch (RuntimeException localRuntimeException1)
      {
        AssertionUtil.reportThrowableButNeverCrash((Throwable)localRuntimeException1);
        return false;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tcpermissions.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */