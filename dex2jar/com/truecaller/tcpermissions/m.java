package com.truecaller.tcpermissions;

import android.os.Build.VERSION;
import c.a.f;
import c.u;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;

public final class m
  implements l
{
  private final com.truecaller.utils.l a;
  
  @Inject
  public m(com.truecaller.utils.l paraml)
  {
    a = paraml;
  }
  
  private static boolean i()
  {
    return Build.VERSION.SDK_INT < 23;
  }
  
  public final String[] a()
  {
    if (i()) {
      return new String[0];
    }
    Object localObject = (List)new ArrayList();
    ((List)localObject).add("android.permission.READ_PHONE_STATE");
    ((List)localObject).add("android.permission.READ_CALL_LOG");
    ((List)localObject).add("android.permission.WRITE_CALL_LOG");
    ((List)localObject).add("android.permission.CALL_PHONE");
    ((List)localObject).add("android.permission.PROCESS_OUTGOING_CALLS");
    if (Build.VERSION.SDK_INT >= 28) {
      ((List)localObject).add("android.permission.ANSWER_PHONE_CALLS");
    }
    localObject = ((Collection)localObject).toArray(new String[0]);
    if (localObject != null) {
      return (String[])localObject;
    }
    throw new u("null cannot be cast to non-null type kotlin.Array<T>");
  }
  
  public final String[] b()
  {
    if (i()) {
      return new String[0];
    }
    return new String[] { "android.permission.READ_CONTACTS", "android.permission.WRITE_CONTACTS" };
  }
  
  public final String[] c()
  {
    if (i()) {
      return new String[0];
    }
    return (String[])f.a(a(), b());
  }
  
  public final String[] d()
  {
    return new String[] { "android.permission.READ_SMS", "android.permission.SEND_SMS", "android.permission.RECEIVE_SMS" };
  }
  
  public final String[] e()
  {
    if (i()) {
      return new String[0];
    }
    return new String[] { "android.permission.RECORD_AUDIO" };
  }
  
  public final boolean f()
  {
    com.truecaller.utils.l locall = a;
    String[] arrayOfString = a();
    if (locall.a((String[])Arrays.copyOf(arrayOfString, arrayOfString.length)))
    {
      locall = a;
      arrayOfString = b();
      if (locall.a((String[])Arrays.copyOf(arrayOfString, arrayOfString.length))) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean g()
  {
    return a.a((String[])Arrays.copyOf(d(), 3));
  }
  
  public final boolean h()
  {
    com.truecaller.utils.l locall = a;
    String[] arrayOfString = e();
    return locall.a((String[])Arrays.copyOf(arrayOfString, arrayOfString.length));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tcpermissions.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */