package com.truecaller.tcpermissions;

import com.truecaller.utils.l;
import dagger.a.d;
import javax.inject.Provider;

public final class n
  implements d<m>
{
  private final Provider<l> a;
  
  private n(Provider<l> paramProvider)
  {
    a = paramProvider;
  }
  
  public static n a(Provider<l> paramProvider)
  {
    return new n(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tcpermissions.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */