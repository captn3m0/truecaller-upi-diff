package com.truecaller.tcpermissions;

import android.content.Context;
import com.truecaller.common.h.ac;
import com.truecaller.utils.t;
import dagger.a.g;
import javax.inject.Provider;

public final class c
  implements f
{
  private final t b;
  private final com.truecaller.common.a c;
  private Provider<c.d.f> d;
  private Provider<Context> e;
  private Provider<com.truecaller.utils.l> f;
  private Provider<ac> g;
  private Provider<com.truecaller.common.h.c> h;
  private Provider<com.truecaller.common.g.a> i;
  private Provider<p> j;
  private Provider<o> k;
  private Provider<m> l;
  private Provider<l> m;
  
  private c(t paramt, com.truecaller.common.a parama)
  {
    b = paramt;
    c = parama;
    d = new f(parama);
    e = new c(parama);
    f = new g(paramt);
    g = new e(parama);
    h = j.a(e);
    i = new d(parama);
    j = q.a(d, e, f, g, h, i);
    k = dagger.a.c.a(j);
    l = n.a(f);
    m = dagger.a.c.a(l);
  }
  
  public static a a()
  {
    return new a((byte)0);
  }
  
  public final o b()
  {
    return (o)k.get();
  }
  
  public final l c()
  {
    return (l)m.get();
  }
  
  public final k d()
  {
    return new b((byte)0);
  }
  
  public static final class a
  {
    private t a;
    private com.truecaller.common.a b;
    
    public final a a(com.truecaller.common.a parama)
    {
      b = ((com.truecaller.common.a)g.a(parama));
      return this;
    }
    
    public final a a(t paramt)
    {
      a = ((t)g.a(paramt));
      return this;
    }
    
    public final f a()
    {
      g.a(a, t.class);
      g.a(b, com.truecaller.common.a.class);
      return new c(a, b, (byte)0);
    }
  }
  
  final class b
    implements k
  {
    private b() {}
    
    public final void a(AccessContactsActivity paramAccessContactsActivity)
    {
      a = new b((c.d.f)g.a(c.c(c.this).r(), "Cannot return null from a non-@Nullable component method"), (o)c.a(c.this).get());
    }
    
    public final void a(TcPermissionsHandlerActivity paramTcPermissionsHandlerActivity)
    {
      a = new h((o)c.a(c.this).get(), (com.truecaller.utils.l)g.a(c.b(c.this).b(), "Cannot return null from a non-@Nullable component method"));
    }
  }
  
  static final class c
    implements Provider<Context>
  {
    private final com.truecaller.common.a a;
    
    c(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
  
  static final class d
    implements Provider<com.truecaller.common.g.a>
  {
    private final com.truecaller.common.a a;
    
    d(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
  
  static final class e
    implements Provider<ac>
  {
    private final com.truecaller.common.a a;
    
    e(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
  
  static final class f
    implements Provider<c.d.f>
  {
    private final com.truecaller.common.a a;
    
    f(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
  
  static final class g
    implements Provider<com.truecaller.utils.l>
  {
    private final t a;
    
    g(t paramt)
    {
      a = paramt;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tcpermissions.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */