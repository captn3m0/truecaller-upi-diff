package com.truecaller.tcpermissions;

import android.content.Context;
import c.d.f;
import com.truecaller.common.g.a;
import com.truecaller.common.h.ac;
import com.truecaller.common.h.c;
import com.truecaller.utils.l;
import dagger.a.d;
import javax.inject.Provider;

public final class q
  implements d<p>
{
  private final Provider<f> a;
  private final Provider<Context> b;
  private final Provider<l> c;
  private final Provider<ac> d;
  private final Provider<c> e;
  private final Provider<a> f;
  
  private q(Provider<f> paramProvider, Provider<Context> paramProvider1, Provider<l> paramProvider2, Provider<ac> paramProvider3, Provider<c> paramProvider4, Provider<a> paramProvider5)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
  }
  
  public static q a(Provider<f> paramProvider, Provider<Context> paramProvider1, Provider<l> paramProvider2, Provider<ac> paramProvider3, Provider<c> paramProvider4, Provider<a> paramProvider5)
  {
    return new q(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tcpermissions.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */