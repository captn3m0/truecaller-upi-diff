package com.truecaller.tcpermissions;

import android.content.Context;
import com.truecaller.common.h.c;
import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d<c>
{
  private final Provider<Context> a;
  
  private j(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static j a(Provider<Context> paramProvider)
  {
    return new j(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tcpermissions.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */