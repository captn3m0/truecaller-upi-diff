package com.truecaller;

import android.content.Context;
import com.truecaller.calling.dialer.ax;
import dagger.a.d;
import javax.inject.Provider;

public final class af
  implements d<ax>
{
  private final c a;
  private final Provider<Context> b;
  
  private af(c paramc, Provider<Context> paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static af a(c paramc, Provider<Context> paramProvider)
  {
    return new af(paramc, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.af
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */