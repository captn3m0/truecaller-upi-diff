package com.truecaller.profile.data;

import c.g.b.k;
import c.o.b;
import com.truecaller.common.account.r;
import com.truecaller.common.profile.f.f;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.UnmutedException.h;
import com.truecaller.profile.data.dto.Address;
import com.truecaller.profile.data.dto.BusinessData;
import com.truecaller.profile.data.dto.BusinessDataResponse;
import com.truecaller.profile.data.dto.Company;
import com.truecaller.profile.data.dto.OnlineIds;
import com.truecaller.profile.data.dto.PersonalData;
import com.truecaller.profile.data.dto.PersonalDataResponse;
import com.truecaller.profile.data.dto.PhoneNumber;
import com.truecaller.profile.data.dto.Profile;
import com.truecaller.profile.data.dto.ProfileResponse;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;
import kotlinx.coroutines.bg;
import okhttp3.ac;

public final class f
  implements com.truecaller.common.profile.e
{
  final c.d.f a;
  private final c b;
  private final com.truecaller.common.g.a c;
  private final r d;
  private final com.truecaller.common.profile.b e;
  private final com.truecaller.common.h.u f;
  private final c.d.f g;
  
  @Inject
  public f(c paramc, com.truecaller.common.g.a parama, r paramr, com.truecaller.common.profile.b paramb, com.truecaller.common.h.u paramu, @Named("UI") c.d.f paramf1, @Named("IO") c.d.f paramf2)
  {
    b = paramc;
    c = parama;
    d = paramr;
    e = paramb;
    f = paramu;
    a = paramf1;
    g = paramf2;
  }
  
  private final Long a(Long paramLong, boolean paramBoolean)
  {
    if (paramBoolean) {
      return paramLong;
    }
    paramLong = c.a("profileTag");
    if (paramLong != null) {
      return c.n.m.d(paramLong);
    }
    return null;
  }
  
  private static String a(long paramLong)
  {
    return "+".concat(String.valueOf(paramLong));
  }
  
  private final String a(PhoneNumber paramPhoneNumber)
  {
    return f.a(String.valueOf(paramPhoneNumber.getNumber()), paramPhoneNumber.getCountryCode());
  }
  
  private static String a(String paramString)
  {
    if (k.a(paramString, "Private")) {
      return "0";
    }
    return "1";
  }
  
  public final Object a(c.d.c<? super Boolean> paramc)
  {
    if ((paramc instanceof f.a))
    {
      localObject1 = (f.a)paramc;
      if ((b & 0x80000000) != 0)
      {
        b += Integer.MIN_VALUE;
        paramc = (c.d.c<? super Boolean>)localObject1;
        break label53;
      }
    }
    paramc = new f.a(this, paramc);
    label53:
    Object localObject1 = a;
    Object localObject2 = c.d.a.a.a;
    switch (b)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 1: 
      paramc = (f)d;
      if ((localObject1 instanceof o.b)) {
        throw a;
      }
      break;
    case 0: 
      if ((localObject1 instanceof o.b)) {
        break label805;
      }
      localObject1 = b.a();
      d = this;
      b = 1;
      localObject1 = ((ao)localObject1).a(paramc);
      if (localObject1 == localObject2) {
        return localObject2;
      }
      paramc = this;
    }
    h localh = (h)localObject1;
    Object localObject5 = b;
    boolean bool = false;
    if (localObject5 != null)
    {
      localObject1 = c;
      long l = ((ProfileResponse)localObject5).getUserId();
      k.b(localObject1, "receiver$0");
      ((com.truecaller.common.g.a)localObject1).b("profileUserId", l);
      Object localObject4 = c;
      localObject2 = ((ProfileResponse)localObject5).getFirstName();
      localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = "";
      }
      Object localObject3 = ((ProfileResponse)localObject5).getLastName();
      localObject2 = localObject3;
      if (localObject3 == null) {
        localObject2 = "";
      }
      i.a((com.truecaller.common.g.a)localObject4, (String)localObject1, (String)localObject2);
      localObject2 = ((ProfileResponse)localObject5).getPersonalData();
      localObject4 = null;
      localObject1 = null;
      Object localObject6;
      String str1;
      Object localObject7;
      Object localObject8;
      String str2;
      Object localObject9;
      if (localObject2 != null)
      {
        localObject2 = ((ProfileResponse)localObject5).getPersonalData();
        i.a(c);
        localObject3 = (PhoneNumber)((PersonalDataResponse)localObject2).getPhoneNumbers().get(0);
        i.a(c, a(((PhoneNumber)localObject3).getNumber()), ((PhoneNumber)localObject3).getCountryCode(), paramc.a((PhoneNumber)localObject3));
        localObject3 = c;
        localObject4 = ((PersonalDataResponse)localObject2).getGender();
        localObject5 = ((PersonalDataResponse)localObject2).getAddress().getStreet();
        localObject6 = ((PersonalDataResponse)localObject2).getAddress().getCity();
        str1 = ((PersonalDataResponse)localObject2).getAddress().getZipCode();
        localObject7 = ((PersonalDataResponse)localObject2).getCompanyName();
        localObject8 = ((PersonalDataResponse)localObject2).getOnlineIds();
        str2 = ((PersonalDataResponse)localObject2).getAvatarUrl();
        localObject9 = ((PersonalDataResponse)localObject2).getJobTitle();
        Long localLong = (Long)c.a.m.e(((PersonalDataResponse)localObject2).getTags());
        if (localLong != null) {
          localObject1 = String.valueOf(localLong.longValue());
        }
        i.a((com.truecaller.common.g.a)localObject3, (String)localObject4, (String)localObject5, (String)localObject6, str1, (String)localObject7, (OnlineIds)localObject8, str2, (String)localObject9, (String)localObject1, ((PersonalDataResponse)localObject2).getAbout(), a(((PersonalDataResponse)localObject2).getPrivacy()), Boolean.FALSE);
      }
      else if (((ProfileResponse)localObject5).getBusinessData() != null)
      {
        localObject5 = ((ProfileResponse)localObject5).getBusinessData();
        localObject1 = (PhoneNumber)((BusinessDataResponse)localObject5).getPhoneNumbers().get(0);
        i.a(c, a(((PhoneNumber)localObject1).getNumber()), ((PhoneNumber)localObject1).getCountryCode(), paramc.a((PhoneNumber)localObject1));
        localObject6 = c;
        localObject1 = ((BusinessDataResponse)localObject5).getCompany().getAddress();
        if (localObject1 != null) {
          localObject1 = ((Address)localObject1).getStreet();
        } else {
          localObject1 = null;
        }
        localObject2 = ((BusinessDataResponse)localObject5).getCompany().getAddress();
        if (localObject2 != null) {
          localObject2 = ((Address)localObject2).getCity();
        } else {
          localObject2 = null;
        }
        localObject3 = ((BusinessDataResponse)localObject5).getCompany().getAddress();
        if (localObject3 != null) {
          localObject3 = ((Address)localObject3).getZipCode();
        } else {
          localObject3 = null;
        }
        str1 = ((BusinessDataResponse)localObject5).getCompany().getName();
        localObject7 = ((BusinessDataResponse)localObject5).getOnlineIds();
        localObject8 = ((BusinessDataResponse)localObject5).getAvatarUrl();
        str2 = ((BusinessDataResponse)localObject5).getJobTitle();
        localObject9 = (Long)c.a.m.e(((BusinessDataResponse)localObject5).getTags());
        if (localObject9 != null) {
          localObject4 = String.valueOf(((Long)localObject9).longValue());
        }
        i.a((com.truecaller.common.g.a)localObject6, "N", (String)localObject1, (String)localObject2, (String)localObject3, str1, (OnlineIds)localObject7, (String)localObject8, str2, (String)localObject4, ((BusinessDataResponse)localObject5).getAbout(), "0", Boolean.TRUE);
      }
      else
      {
        throw ((Throwable)new IllegalStateException("Either personal or business data should not be empty"));
      }
    }
    e.a();
    int i = ((Number)Integer.valueOf(a)).intValue();
    if ((i == 200) || (i == 404)) {
      bool = true;
    }
    return Boolean.valueOf(bool);
    label805:
    throw a;
  }
  
  public final Object a(boolean paramBoolean1, ac paramac, boolean paramBoolean2, Long paramLong, Map<String, String> paramMap, boolean paramBoolean3, c.d.c<? super com.truecaller.common.profile.f> paramc)
  {
    if ((paramc instanceof f.c))
    {
      localObject1 = (f.c)paramc;
      if ((b & 0x80000000) != 0)
      {
        b += Integer.MIN_VALUE;
        break label54;
      }
    }
    Object localObject1 = new f.c(this, paramc);
    label54:
    paramc = a;
    Object localObject5 = c.d.a.a.a;
    int i = b;
    String str1 = null;
    OnlineIds localOnlineIds = null;
    switch (i)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 2: 
      paramLong = (Profile)i;
      paramac = (f)d;
      if (!(paramc instanceof o.b)) {
        break label760;
      }
      throw a;
    case 1: 
      paramBoolean3 = l;
      paramac = (Map)g;
      paramMap = (Long)f;
      paramBoolean2 = k;
      paramLong = (ac)e;
      paramBoolean1 = j;
      localObject2 = (f)d;
      if (!(paramc instanceof o.b))
      {
        localObject3 = paramc;
        paramc = (c.d.c<? super com.truecaller.common.profile.f>)localObject2;
      }
      else
      {
        throw a;
      }
      break;
    case 0: 
      if ((paramc instanceof o.b)) {
        break label1193;
      }
      if (!paramBoolean1) {
        break label398;
      }
      d = this;
      j = paramBoolean1;
      e = paramac;
      k = paramBoolean2;
      localObject2 = paramLong;
      f = localObject2;
      g = paramMap;
      l = paramBoolean3;
      b = 1;
      localObject3 = a(paramac, (c.d.c)localObject1);
      if (localObject3 == localObject5) {
        return localObject5;
      }
      paramc = this;
      paramLong = paramac;
      paramac = paramMap;
      paramMap = (Map<String, String>)localObject2;
    }
    Object localObject2 = (String)localObject3;
    if (localObject2 != null)
    {
      c.a("profileAvatar", (String)localObject2);
      if (localObject2 != null)
      {
        localObject3 = paramac;
        paramac = paramc;
        paramc = paramLong;
        break label446;
      }
    }
    return new f.f();
    label398:
    Object localObject4 = paramLong;
    paramc = c.a("profileAvatar");
    paramLong = paramc;
    if (paramc == null) {
      paramLong = "";
    }
    paramc = paramac;
    paramac = this;
    Object localObject3 = paramMap;
    localObject2 = paramLong;
    paramMap = (Map<String, String>)localObject4;
    label446:
    localObject4 = d.b();
    if (localObject4 != null)
    {
      if (((CharSequence)localObject4).length() == 0) {
        i = 1;
      } else {
        i = 0;
      }
      if (i == 0)
      {
        paramLong = (Long)localObject4;
        if (c.n.m.b((String)localObject4, "+", false)) {
          if (localObject4 != null)
          {
            paramLong = ((String)localObject4).substring(1);
            k.a(paramLong, "(this as java.lang.String).substring(startIndex)");
          }
          else
          {
            throw new c.u("null cannot be cast to non-null type java.lang.String");
          }
        }
        paramLong = c.n.m.d(paramLong);
        break label555;
      }
    }
    paramLong = null;
    label555:
    if (paramLong != null)
    {
      long l = ((Number)paramLong).longValue();
      if (paramBoolean3)
      {
        paramLong = new b((Map)localObject3, c, l).a((String)localObject2, c.a.m.b(paramac.a(paramMap, paramBoolean2)), false);
      }
      else
      {
        paramLong = new b((Map)localObject3, c, l);
        paramLong = paramLong.a((String)localObject2, c.a.m.b(paramac.a(paramMap, paramBoolean2)), a.b("profileBusiness"));
      }
      localObject4 = b.a(paramLong);
      d = paramac;
      j = paramBoolean1;
      e = paramc;
      k = paramBoolean2;
      f = paramMap;
      g = localObject3;
      l = paramBoolean3;
      h = localObject2;
      i = paramLong;
      b = 2;
      paramc = ((ao)localObject4).a((c.d.c)localObject1);
      if (paramc == localObject5) {
        return localObject5;
      }
      label760:
      localObject1 = (com.truecaller.common.profile.f)paramc;
      if (a)
      {
        i.a(c, paramLong.getFirstName(), paramLong.getLastName());
        String str2;
        Object localObject6;
        if (paramLong.getPersonalData() != null)
        {
          paramLong = paramLong.getPersonalData();
          i.a(c);
          paramMap = c;
          paramc = paramLong.getGender();
          localObject2 = paramLong.getAddress().getStreet();
          localObject3 = paramLong.getAddress().getCity();
          localObject4 = paramLong.getAddress().getZipCode();
          str1 = paramLong.getCompanyName();
          localObject5 = paramLong.getOnlineIds();
          str2 = paramLong.getAvatarUrl();
          localObject6 = paramLong.getJobTitle();
          Long localLong = (Long)c.a.m.e(paramLong.getTags());
          paramac = localOnlineIds;
          if (localLong != null) {
            paramac = String.valueOf(localLong.longValue());
          }
          i.a(paramMap, paramc, (String)localObject2, (String)localObject3, (String)localObject4, str1, (OnlineIds)localObject5, str2, (String)localObject6, paramac, paramLong.getAbout(), a(paramLong.getPrivacy()), Boolean.FALSE);
          return localObject1;
        }
        if (paramLong.getBusinessData() != null)
        {
          localObject2 = paramLong.getBusinessData();
          localObject3 = c;
          paramac = ((BusinessData)localObject2).getCompany().getAddress();
          if (paramac != null) {
            paramac = paramac.getStreet();
          } else {
            paramac = null;
          }
          paramLong = ((BusinessData)localObject2).getCompany().getAddress();
          if (paramLong != null) {
            paramLong = paramLong.getCity();
          } else {
            paramLong = null;
          }
          paramMap = ((BusinessData)localObject2).getCompany().getAddress();
          if (paramMap != null) {
            paramMap = paramMap.getZipCode();
          } else {
            paramMap = null;
          }
          localObject4 = ((BusinessData)localObject2).getCompany().getName();
          localOnlineIds = ((BusinessData)localObject2).getOnlineIds();
          localObject5 = ((BusinessData)localObject2).getAvatarUrl();
          str2 = ((BusinessData)localObject2).getJobTitle();
          localObject6 = (Long)c.a.m.e(((BusinessData)localObject2).getTags());
          paramc = str1;
          if (localObject6 != null) {
            paramc = String.valueOf(((Long)localObject6).longValue());
          }
          i.a((com.truecaller.common.g.a)localObject3, "N", paramac, paramLong, paramMap, (String)localObject4, localOnlineIds, (String)localObject5, str2, paramc, ((BusinessData)localObject2).getAbout(), "0", Boolean.TRUE);
          return localObject1;
        }
        throw ((Throwable)new IllegalStateException("Either personal or business data should not be empty"));
      }
      return localObject1;
    }
    AssertionUtil.reportThrowableButNeverCrash((Throwable)new UnmutedException.h());
    return new f.f();
    label1193:
    throw a;
  }
  
  public final void a(com.truecaller.common.profile.a parama)
  {
    kotlinx.coroutines.e.b((ag)bg.a, g, (c.g.a.m)new f.b(this, parama, null), 2);
  }
  
  public final void a(boolean paramBoolean1, ac paramac, boolean paramBoolean2, Long paramLong, Map<String, String> paramMap, boolean paramBoolean3, com.truecaller.common.profile.h paramh)
  {
    kotlinx.coroutines.e.b((ag)bg.a, g, (c.g.a.m)new f.d(this, paramBoolean1, paramac, paramBoolean2, paramLong, paramMap, paramBoolean3, paramh, null), 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.data.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */