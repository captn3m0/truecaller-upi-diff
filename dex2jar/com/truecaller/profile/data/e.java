package com.truecaller.profile.data;

import c.d.f;
import com.truecaller.network.e.a;
import javax.inject.Provider;

public final class e
  implements dagger.a.d<d>
{
  private final Provider<a> a;
  private final Provider<f> b;
  
  private e(Provider<a> paramProvider, Provider<f> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static e a(Provider<a> paramProvider, Provider<f> paramProvider1)
  {
    return new e(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.data.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */