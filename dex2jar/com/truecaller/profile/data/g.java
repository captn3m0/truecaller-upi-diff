package com.truecaller.profile.data;

import com.truecaller.common.account.r;
import com.truecaller.common.g.a;
import com.truecaller.common.h.u;
import com.truecaller.common.profile.b;
import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d<f>
{
  private final Provider<c> a;
  private final Provider<a> b;
  private final Provider<r> c;
  private final Provider<b> d;
  private final Provider<u> e;
  private final Provider<c.d.f> f;
  private final Provider<c.d.f> g;
  
  private g(Provider<c> paramProvider, Provider<a> paramProvider1, Provider<r> paramProvider2, Provider<b> paramProvider3, Provider<u> paramProvider4, Provider<c.d.f> paramProvider5, Provider<c.d.f> paramProvider6)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
  }
  
  public static g a(Provider<c> paramProvider, Provider<a> paramProvider1, Provider<r> paramProvider2, Provider<b> paramProvider3, Provider<u> paramProvider4, Provider<c.d.f> paramProvider5, Provider<c.d.f> paramProvider6)
  {
    return new g(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.data.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */