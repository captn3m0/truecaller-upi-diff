package com.truecaller.profile.data;

import c.g.a.m;
import c.g.b.k;
import com.truecaller.network.e.a;
import com.truecaller.profile.data.dto.Profile;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;
import kotlinx.coroutines.e;
import okhttp3.ac;

public final class d
  implements c, ag
{
  final a a;
  private final c.d.f b;
  
  @Inject
  public d(a parama, @Named("IO") c.d.f paramf)
  {
    a = parama;
    b = paramf;
  }
  
  public final c.d.f V_()
  {
    return b;
  }
  
  public final ao<h> a()
  {
    return e.a(this, null, (m)new d.b(this, null), 3);
  }
  
  public final ao<com.truecaller.common.profile.f> a(Profile paramProfile)
  {
    k.b(paramProfile, "profile");
    return e.a(this, null, (m)new d.c(this, paramProfile, null), 3);
  }
  
  public final ao<String> a(ac paramac)
  {
    k.b(paramac, "avatar");
    return e.a(this, null, (m)new d.d(paramac, null), 3);
  }
  
  public final ao<Boolean> b()
  {
    return e.a(this, null, (m)new d.a(null), 3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.data.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */