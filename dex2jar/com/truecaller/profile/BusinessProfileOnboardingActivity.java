package com.truecaller.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.f;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import c.g.b.k;
import com.truecaller.R.id;
import com.truecaller.analytics.e.a;
import com.truecaller.profile.business.i;
import com.truecaller.profile.business.j;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import java.util.HashMap;
import javax.inject.Inject;

public final class BusinessProfileOnboardingActivity
  extends AppCompatActivity
{
  public static final BusinessProfileOnboardingActivity.a c = new BusinessProfileOnboardingActivity.a((byte)0);
  @Inject
  public com.truecaller.featuretoggles.e a;
  @Inject
  public com.truecaller.analytics.b b;
  private HashMap d;
  
  public static final Intent a(Context paramContext)
  {
    return BusinessProfileOnboardingActivity.a.a(paramContext, false, true);
  }
  
  public static final Intent a(Context paramContext, boolean paramBoolean)
  {
    return BusinessProfileOnboardingActivity.a.a(paramContext, paramBoolean, false);
  }
  
  private View a(int paramInt)
  {
    if (d == null) {
      d = new HashMap();
    }
    View localView2 = (View)d.get(Integer.valueOf(paramInt));
    View localView1 = localView2;
    if (localView2 == null)
    {
      localView1 = findViewById(paramInt);
      d.put(Integer.valueOf(paramInt), localView1);
    }
    return localView1;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setTheme(aresId);
    setContentView(2131558512);
    j.a(this).a(this);
    paramBundle = a;
    if (paramBundle == null) {
      k.a("featuresRegistry");
    }
    if (!paramBundle.z().a())
    {
      finish();
      return;
    }
    if (getIntent().getBooleanExtra("arg_from_wizard", false))
    {
      paramBundle = b;
      if (paramBundle == null) {
        k.a("analytics");
      }
      com.truecaller.analytics.e locale = new e.a("WizardAction").a("Action", "BusinessProfileOnboarding").a();
      k.a(locale, "AnalyticsEvent.Builder(W…                 .build()");
      paramBundle.b(locale);
    }
    setSupportActionBar((Toolbar)a(R.id.toolbar));
    paramBundle = getSupportActionBar();
    if (paramBundle != null) {
      paramBundle.setDisplayHomeAsUpEnabled(true);
    }
    paramBundle = getSupportActionBar();
    if (paramBundle != null) {
      paramBundle.setDisplayShowHomeEnabled(true);
    }
    paramBundle = (Toolbar)a(R.id.toolbar);
    if (paramBundle != null) {
      paramBundle.setNavigationOnClickListener((View.OnClickListener)new BusinessProfileOnboardingActivity.c(this));
    }
    ((Button)a(R.id.continueButton)).setOnClickListener((View.OnClickListener)new BusinessProfileOnboardingActivity.b(this));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.profile.BusinessProfileOnboardingActivity
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */