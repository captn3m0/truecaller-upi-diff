package com.truecaller;

import com.truecaller.androidactors.aa;
import com.truecaller.androidactors.q;
import com.truecaller.androidactors.v;
import com.truecaller.callerid.ae;
import com.truecaller.callhistory.s;
import com.truecaller.filters.t;
import com.truecaller.messaging.data.af;
import com.truecaller.messaging.data.p;
import com.truecaller.messaging.data.u;
import com.truecaller.messaging.g.m;
import com.truecaller.messaging.transport.e;
import com.truecaller.messaging.transport.im.at;
import com.truecaller.messaging.transport.im.bk;
import com.truecaller.messaging.transport.im.bq;
import com.truecaller.messaging.transport.mms.aj;
import com.truecaller.network.util.g;
import com.truecaller.premium.data.h;
import com.truecaller.util.ab;
import com.truecaller.util.bb;

final class a
{
  q a;
  aa b;
  
  static final class a
    extends com.truecaller.androidactors.l
  {
    a(aa paramaa, q paramq)
    {
      super(paramq);
    }
  }
  
  static final class b
    extends aa
  {
    public final <T> T a(Class<T> paramClass, v paramv)
    {
      if (af.a(paramClass)) {
        return new af(paramv);
      }
      if (u.a(paramClass)) {
        return new u(paramv);
      }
      if (com.truecaller.messaging.notifications.b.a(paramClass)) {
        return new com.truecaller.messaging.notifications.b(paramv);
      }
      if (e.a(paramClass)) {
        return new e(paramv);
      }
      if (aj.a(paramClass)) {
        return new aj(paramv);
      }
      if (bb.a(paramClass)) {
        return new bb(paramv);
      }
      if (ab.a(paramClass)) {
        return new ab(paramv);
      }
      if (com.truecaller.callhistory.b.a(paramClass)) {
        return new com.truecaller.callhistory.b(paramv);
      }
      if (com.truecaller.network.util.d.a(paramClass)) {
        return new com.truecaller.network.util.d(paramv);
      }
      if (g.a(paramClass)) {
        return new g(paramv);
      }
      if (ae.a(paramClass)) {
        return new ae(paramv);
      }
      if (com.truecaller.callerid.f.a(paramClass)) {
        return new com.truecaller.callerid.f(paramv);
      }
      if (com.truecaller.callerid.l.a(paramClass)) {
        return new com.truecaller.callerid.l(paramv);
      }
      if (com.truecaller.callerid.o.a(paramClass)) {
        return new com.truecaller.callerid.o(paramv);
      }
      if (com.truecaller.presence.d.a(paramClass)) {
        return new com.truecaller.presence.d(paramv);
      }
      if (h.a(paramClass)) {
        return new h(paramv);
      }
      if (com.truecaller.referral.r.a(paramClass)) {
        return new com.truecaller.referral.r(paramv);
      }
      if (com.truecaller.calling.recorder.d.a(paramClass)) {
        return new com.truecaller.calling.recorder.d(paramv);
      }
      if (com.truecaller.tag.d.a(paramClass)) {
        return new com.truecaller.tag.d(paramv);
      }
      if (t.a(paramClass)) {
        return new t(paramv);
      }
      if (com.truecaller.a.c.a(paramClass)) {
        return new com.truecaller.a.c(paramv);
      }
      if (p.a(paramClass)) {
        return new p(paramv);
      }
      if (com.truecaller.messaging.transport.a.d.a(paramClass)) {
        return new com.truecaller.messaging.transport.a.d(paramv);
      }
      if (bk.a(paramClass)) {
        return new bk(paramv);
      }
      if (com.truecaller.messaging.transport.im.r.a(paramClass)) {
        return new com.truecaller.messaging.transport.im.r(paramv);
      }
      if (at.a(paramClass)) {
        return new at(paramv);
      }
      if (com.truecaller.messaging.transport.im.a.b.a(paramClass)) {
        return new com.truecaller.messaging.transport.im.a.b(paramv);
      }
      if (bq.a(paramClass)) {
        return new bq(paramv);
      }
      if (m.a(paramClass)) {
        return new m(paramv);
      }
      if (s.a(paramClass)) {
        return new s(paramv);
      }
      if (com.truecaller.h.d.a(paramClass)) {
        return new com.truecaller.h.d(paramv);
      }
      if (com.truecaller.network.a.b.a(paramClass)) {
        return new com.truecaller.network.a.b(paramv);
      }
      if (com.truecaller.config.b.a(paramClass)) {
        return new com.truecaller.config.b(paramv);
      }
      if (com.truecaller.calling.recorder.floatingbutton.f.a(paramClass)) {
        return new com.truecaller.calling.recorder.floatingbutton.f(paramv);
      }
      if (com.truecaller.ads.leadgen.o.a(paramClass)) {
        return new com.truecaller.ads.leadgen.o(paramv);
      }
      if (com.truecaller.ads.provider.a.c.a(paramClass)) {
        return new com.truecaller.ads.provider.a.c(paramv);
      }
      if (com.truecaller.payments.network.c.a(paramClass)) {
        return new com.truecaller.payments.network.c(paramv);
      }
      return (T)b(paramClass, paramv);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */