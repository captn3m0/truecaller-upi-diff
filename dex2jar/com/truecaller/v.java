package com.truecaller;

import com.truecaller.common.account.r;
import com.truecaller.common.g.a;
import javax.inject.Provider;

public final class v
  implements dagger.a.d<com.truecaller.network.a.d>
{
  private final c a;
  private final Provider<r> b;
  private final Provider<a> c;
  private final Provider<com.truecaller.common.network.c> d;
  
  private v(c paramc, Provider<r> paramProvider, Provider<a> paramProvider1, Provider<com.truecaller.common.network.c> paramProvider2)
  {
    a = paramc;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
  }
  
  public static v a(c paramc, Provider<r> paramProvider, Provider<a> paramProvider1, Provider<com.truecaller.common.network.c> paramProvider2)
  {
    return new v(paramc, paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.v
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */