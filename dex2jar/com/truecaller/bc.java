package com.truecaller;

import c.g;
import c.g.a.a;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bn;

public abstract class bc<Router, PV>
  extends bd<Router, PV>
  implements ag
{
  private final c.f d;
  private final c.d.f e;
  
  public bc(c.d.f paramf)
  {
    e = paramf;
    d = g.a((a)bc.a.a);
  }
  
  private bn b()
  {
    return (bn)d.b();
  }
  
  public final c.d.f V_()
  {
    return e.plus((c.d.f)b());
  }
  
  public void y_()
  {
    super.y_();
    b().n();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.bc
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */