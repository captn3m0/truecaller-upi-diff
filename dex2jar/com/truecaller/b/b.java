package com.truecaller.b;

import android.content.Context;
import com.truecaller.data.access.m;
import com.truecaller.data.entity.g;
import dagger.a.d;
import javax.inject.Provider;

public final class b
  implements d<c>
{
  private final a a;
  private final Provider<Context> b;
  private final Provider<com.truecaller.analytics.b> c;
  private final Provider<m> d;
  private final Provider<g> e;
  private final Provider<com.truecaller.utils.a> f;
  
  private b(a parama, Provider<Context> paramProvider, Provider<com.truecaller.analytics.b> paramProvider1, Provider<m> paramProvider2, Provider<g> paramProvider3, Provider<com.truecaller.utils.a> paramProvider4)
  {
    a = parama;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
    f = paramProvider4;
  }
  
  public static b a(a parama, Provider<Context> paramProvider, Provider<com.truecaller.analytics.b> paramProvider1, Provider<m> paramProvider2, Provider<g> paramProvider3, Provider<com.truecaller.utils.a> paramProvider4)
  {
    return new b(parama, paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.b.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */