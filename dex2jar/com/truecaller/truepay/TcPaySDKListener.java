package com.truecaller.truepay;

import android.net.Uri;
import java.util.Map;

public abstract interface TcPaySDKListener
{
  public abstract void createShortcut(int paramInt);
  
  public abstract void fetchTempToken(PayTempTokenCallBack paramPayTempTokenCallBack);
  
  public abstract Uri getTcPayNotificationTone();
  
  public abstract boolean isTcPayEnabled();
  
  public abstract void logTcPayCleverTapEvent(String paramString, Map<String, Object> paramMap);
  
  public abstract void logTcPayEvent(String paramString1, String paramString2, Map<CharSequence, CharSequence> paramMap, Map<CharSequence, Double> paramMap1);
  
  public abstract void logTcPayFacebookEvent(String paramString, Map<CharSequence, CharSequence> paramMap);
  
  public abstract void updateCleverTapProfile(Map<String, Object> paramMap);
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.TcPaySDKListener
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */