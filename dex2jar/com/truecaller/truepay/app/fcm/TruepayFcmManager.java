package com.truecaller.truepay.app.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.z.a;
import android.support.v4.app.z.c;
import android.support.v4.app.z.d;
import android.support.v4.content.b;
import android.support.v4.content.d;
import android.text.TextUtils;
import com.d.b.ab;
import com.d.b.w;
import com.truecaller.common.h.aq.d;
import com.truecaller.notificationchannels.e;
import com.truecaller.notificationchannels.n;
import com.truecaller.notificationchannels.n.a;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.TcPaySDKListener;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.history.views.activities.TransactionHistoryActivity;
import com.truecaller.truepay.app.ui.transaction.views.activities.TransactionActivity;
import com.truecaller.truepay.data.f.r;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TruepayFcmManager
{
  private final String REGEX_GENERIC_NAME_RESOLVER = "\\d{10}";
  private final Context context;
  private final e coreNotificationChannelProvider;
  private int notificationId;
  
  public TruepayFcmManager(Context paramContext)
  {
    context = paramContext;
    n.a locala = n.a;
    coreNotificationChannelProvider = n.a.a(paramContext, null).c();
  }
  
  private Bitmap fetchAvatar(String paramString)
  {
    Bitmap localBitmap = getBitmap(paramString);
    paramString = localBitmap;
    if (localBitmap == null) {
      paramString = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_stat_avatar);
    }
    return paramString;
  }
  
  private void fetchAvatarAndNotify(NotificationModel paramNotificationModel, z.d paramd)
  {
    if (isNotEmpty(d)) {
      paramNotificationModel = d;
    } else if (isNotEmpty(c)) {
      paramNotificationModel = c;
    } else {
      paramNotificationModel = "0000000000";
    }
    new r(context.getContentResolver()).c(paramNotificationModel).b(io.reactivex.g.a.b()).a(new -..Lambda.TruepayFcmManager.-TdriVIrHjAL7hOp-hh87s-yG3Y(this, paramd)).a(io.reactivex.android.b.a.a()).a(new TruepayFcmManager.3(this, paramd));
  }
  
  private void fetchContactAndNotify(z.d paramd, NotificationModel paramNotificationModel, String paramString)
  {
    new r(context.getContentResolver()).d(paramString).b(io.reactivex.g.a.b()).a(new -..Lambda.TruepayFcmManager.DeG7ufTJqz1D8CpM2plFGbaimVA(this, paramNotificationModel, paramString, paramd)).a(io.reactivex.android.b.a.a()).a(new TruepayFcmManager.2(this, paramd));
  }
  
  private Bitmap fetchOperator(String paramString)
  {
    Bitmap localBitmap = getBitmap(paramString);
    paramString = localBitmap;
    if (localBitmap == null) {
      paramString = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_place_holder_circle);
    }
    return paramString;
  }
  
  private void fetchOperatorAndNotify(NotificationModel paramNotificationModel, z.d paramd)
  {
    io.reactivex.o.a(new -..Lambda.TruepayFcmManager.DLTfxaLA_jsQFp754xB44zxY2Lw(this, paramNotificationModel)).b(io.reactivex.g.a.b()).a(new -..Lambda.TruepayFcmManager.3s1OpNtz84-dLWrsf2IULEOVqvc(this, paramd)).a(io.reactivex.android.b.a.a()).a(new TruepayFcmManager.1(this, paramd));
  }
  
  private Intent getBankingTabIntent()
  {
    Intent localIntent = new Intent();
    localIntent.setAction("android.intent.action.VIEW");
    localIntent.setData(Uri.parse("truecaller://home/tabs/banking"));
    return localIntent;
  }
  
  private Bitmap getBitmap(String paramString)
  {
    if (paramString != null) {}
    try
    {
      paramString = Uri.parse(paramString);
      paramString = w.a(context).a(paramString).a(aq.d.b()).d();
      return paramString;
    }
    catch (IOException|SecurityException paramString)
    {
      for (;;) {}
    }
    return null;
  }
  
  private Intent getInstantRewardsIntent(NotificationModel paramNotificationModel)
  {
    Intent localIntent = getBankingTabIntent();
    localIntent.putExtra("instant_reward_content", a);
    localIntent.putExtra("show_instant_reward", true);
    return localIntent;
  }
  
  private Matcher getMatcher(String paramString)
  {
    return Pattern.compile("\\d{10}").matcher(paramString);
  }
  
  private boolean isNotEmpty(String paramString)
  {
    return (!TextUtils.isEmpty(paramString)) && (!TextUtils.equals(paramString, "0000000000"));
  }
  
  private void notifyUser(Notification paramNotification)
  {
    NotificationManager localNotificationManager = (NotificationManager)context.getSystemService("notification");
    if (localNotificationManager != null) {
      localNotificationManager.notify(notificationId, paramNotification);
    }
  }
  
  private void notifyUser(NotificationModel paramNotificationModel, Intent paramIntent)
  {
    notifyUser(paramNotificationModel, paramIntent, null);
  }
  
  private void notifyUser(NotificationModel paramNotificationModel, Intent paramIntent, List<z.a> paramList)
  {
    Object localObject = PendingIntent.getActivity(context, 0, paramIntent, 268435456);
    paramIntent = coreNotificationChannelProvider.j();
    if (paramIntent == null) {
      paramIntent = new z.d(context);
    } else {
      paramIntent = new z.d(context, paramIntent);
    }
    z.c localc = new z.c();
    if (!TextUtils.isEmpty(a))
    {
      paramIntent.a(a);
      localc.a(a);
    }
    if (!TextUtils.isEmpty(b))
    {
      paramIntent.b(b);
      localc.b(b);
    }
    paramIntent.a(R.drawable.ic_stat_notification);
    C = b.c(context, R.color.accent_default);
    paramIntent.a(localc);
    f = ((PendingIntent)localObject);
    if (NotificationType.PAYMENT_INCOMING.equals(h)) {
      paramIntent.a(Truepay.getInstance().getListener().getTcPayNotificationTone());
    } else {
      paramIntent.c(1);
    }
    l = 2;
    paramIntent.d(16);
    if ((!NotificationType.PAYMENT_INCOMING.equals(h)) && (!NotificationType.PAYMENT_REQUEST.equals(h)))
    {
      if (NotificationType.PAYMENT_CUSTOM.equals(h))
      {
        if ("action.instant_reward_show".equals(g))
        {
          notifyUser(paramIntent.h());
          sendInstantRewardBroadcast(paramNotificationModel);
          return;
        }
        if (paramList != null)
        {
          paramList = paramList.iterator();
          while (paramList.hasNext())
          {
            localObject = (z.a)paramList.next();
            if (localObject != null) {
              paramIntent.a((z.a)localObject);
            }
          }
        }
        paramList = getMatcher(a);
        if (paramList.find())
        {
          fetchContactAndNotify(paramIntent, paramNotificationModel, paramList.group().replaceAll("[^0-9]", ""));
          return;
        }
        fetchOperatorAndNotify(paramNotificationModel, paramIntent);
        return;
      }
      notifyUser(paramIntent.h());
      return;
    }
    fetchAvatarAndNotify(paramNotificationModel, paramIntent);
  }
  
  private String resolveName(String paramString1, String paramString2)
  {
    return paramString1.replaceAll("\\d{10}", paramString2);
  }
  
  private void sendInstantRewardBroadcast(NotificationModel paramNotificationModel)
  {
    Intent localIntent = new Intent("INSTANT_REWARD_RECEIVED");
    localIntent.putExtra("instant_reward_content", a);
    localIntent.putExtra("instant_reward_notification_id", notificationId);
    d.a(context).a(localIntent);
  }
  
  private void showCustomNotification(NotificationModel paramNotificationModel)
  {
    Object localObject = g;
    Intent localIntent = getBankingTabIntent();
    if (!TextUtils.isEmpty((CharSequence)localObject))
    {
      String[] arrayOfString = ((String)localObject).split(",");
      ArrayList localArrayList = new ArrayList();
      localIntent = new Intent(context, TransactionHistoryActivity.class);
      localObject = new Bundle();
      ((Bundle)localObject).putString("id", f);
      ((Bundle)localObject).putInt("EXTRA_NOTIFICATION_ID", notificationId);
      localIntent.putExtras((Bundle)localObject);
      int k = arrayOfString.length;
      int j = 0;
      while (j < k)
      {
        String str = arrayOfString[j];
        localObject = null;
        switch (str.hashCode())
        {
        default: 
          break;
        case 1327427831: 
          if (str.equals("action.instant_reward_show")) {
            i = 5;
          }
          break;
        case 970112032: 
          if (str.equals("action.share_receipt")) {
            i = 0;
          }
          break;
        case 440937684: 
          if (str.equals("action.later")) {
            i = 4;
          }
          break;
        case -399989823: 
          if (str.equals("action.check_status")) {
            i = 2;
          }
          break;
        case -1488693062: 
          if (str.equals("action.page.transaction_details")) {
            i = 1;
          }
          break;
        case -1840980623: 
          if (str.equals("action.registration")) {
            i = 3;
          }
          break;
        }
        int i = -1;
        switch (i)
        {
        default: 
          break;
        case 5: 
          localIntent = getInstantRewardsIntent(paramNotificationModel);
          break;
        case 4: 
          localIntent = new Intent(context, NotificationBroadcastReceiver.class);
          localIntent.setAction("com.truecaller.tcpay.notifications.LATER");
          localObject = new z.a(0, "LATER", PendingIntent.getBroadcast(context, notificationId, localIntent, 268435456));
          break;
        case 3: 
          localIntent = getBankingTabIntent();
          localObject = new z.a(0, "REGISTER", PendingIntent.getActivity(context, (int)System.currentTimeMillis(), localIntent, 268435456));
          break;
        case 2: 
          localObject = new z.a(0, "CHECK STATUS", PendingIntent.getActivity(context, (int)System.currentTimeMillis(), localIntent, 268435456));
          break;
        case 1: 
          localObject = new z.a(0, "VIEW DETAILS", PendingIntent.getActivity(context, (int)System.currentTimeMillis(), localIntent, 268435456));
          break;
        case 0: 
          localObject = new z.a(0, "SHARE RECEIPT", PendingIntent.getActivity(context, (int)System.currentTimeMillis(), localIntent, 268435456));
        }
        if (localObject != null) {
          localArrayList.add(localObject);
        }
        j += 1;
      }
      notifyUser(paramNotificationModel, localIntent, localArrayList);
      return;
    }
    notifyUser(paramNotificationModel, localIntent);
  }
  
  private void showPaymentConfirmation(NotificationModel paramNotificationModel)
  {
    notifyUser(paramNotificationModel, new Intent(context, TransactionHistoryActivity.class));
  }
  
  private void showPaymentIncoming(NotificationModel paramNotificationModel)
  {
    notifyUser(paramNotificationModel, new Intent(context, TransactionHistoryActivity.class));
  }
  
  private void showPaymentRequest(NotificationModel paramNotificationModel)
  {
    Intent localIntent = new Intent(context, TransactionActivity.class);
    localIntent.putExtra("route", "route_pending_request");
    notifyUser(paramNotificationModel, localIntent);
  }
  
  public void handleNotification(int paramInt, com.google.gson.o paramo)
  {
    notificationId = new Random().nextInt();
    NotificationType localNotificationType = NotificationType.valueOf(paramInt);
    paramo = NotificationModel.a(paramo);
    h = localNotificationType;
    switch (TruepayFcmManager.4.a[localNotificationType.ordinal()])
    {
    default: 
      break;
    case 4: 
      showPaymentConfirmation(paramo);
      break;
    case 3: 
      showPaymentIncoming(paramo);
      break;
    case 2: 
      showPaymentRequest(paramo);
      break;
    case 1: 
      showCustomNotification(paramo);
    }
    paramo = new Intent("notification_received");
    d.a(context).a(paramo);
    com.truecaller.truepay.app.ui.dashboard.a.b = true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.fcm.TruepayFcmManager
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */