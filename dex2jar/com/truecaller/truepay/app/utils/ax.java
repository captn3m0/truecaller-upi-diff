package com.truecaller.truepay.app.utils;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.res.AssetManager;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.widget.EditText;
import android.widget.Toast;
import com.google.c.a.g;
import com.google.c.a.k;
import com.google.c.a.k.c;
import com.google.gson.f;
import com.truecaller.truepay.SenderInfo;
import com.truecaller.truepay.data.c.b;
import io.reactivex.o;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class ax
{
  private final Context a;
  
  public ax(Context paramContext)
  {
    a = paramContext;
  }
  
  public static TextWatcher a(EditText paramEditText, String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString);
    localStringBuilder.append(paramEditText.getText());
    paramEditText.setText(localStringBuilder.toString());
    Selection.setSelection(paramEditText.getText(), paramEditText.getText().length());
    paramString = new ax.1(paramString, paramEditText);
    paramEditText.addTextChangedListener(paramString);
    return paramString;
  }
  
  /* Error */
  private static String a(java.io.InputStream paramInputStream)
  {
    // Byte code:
    //   0: new 17	java/lang/StringBuilder
    //   3: dup
    //   4: invokespecial 18	java/lang/StringBuilder:<init>	()V
    //   7: astore 5
    //   9: aconst_null
    //   10: astore 4
    //   12: aconst_null
    //   13: astore_3
    //   14: new 65	java/util/zip/GZIPInputStream
    //   17: dup
    //   18: aload_0
    //   19: invokespecial 68	java/util/zip/GZIPInputStream:<init>	(Ljava/io/InputStream;)V
    //   22: astore_1
    //   23: new 70	java/io/InputStreamReader
    //   26: dup
    //   27: aload_1
    //   28: invokespecial 71	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   31: astore_2
    //   32: new 73	java/io/BufferedReader
    //   35: dup
    //   36: aload_2
    //   37: invokespecial 76	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   40: astore_0
    //   41: aload_0
    //   42: invokevirtual 79	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   45: astore_3
    //   46: aload_3
    //   47: ifnull +21 -> 68
    //   50: aload 5
    //   52: aload_3
    //   53: invokevirtual 22	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   56: pop
    //   57: aload 5
    //   59: ldc 81
    //   61: invokevirtual 22	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   64: pop
    //   65: goto -24 -> 41
    //   68: aload_1
    //   69: invokestatic 86	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   72: aload_2
    //   73: invokestatic 86	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   76: aload_0
    //   77: invokestatic 86	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   80: goto +93 -> 173
    //   83: astore 4
    //   85: aload_0
    //   86: astore_3
    //   87: aload 4
    //   89: astore_0
    //   90: aload_1
    //   91: astore 4
    //   93: aload_2
    //   94: astore_1
    //   95: goto +44 -> 139
    //   98: aload_0
    //   99: astore_3
    //   100: aload_2
    //   101: astore_0
    //   102: goto +59 -> 161
    //   105: astore_0
    //   106: aload_1
    //   107: astore 4
    //   109: aload_2
    //   110: astore_1
    //   111: goto +28 -> 139
    //   114: astore_0
    //   115: aconst_null
    //   116: astore_2
    //   117: aload_1
    //   118: astore 4
    //   120: aload_2
    //   121: astore_1
    //   122: goto +17 -> 139
    //   125: aconst_null
    //   126: astore_0
    //   127: aload 4
    //   129: astore_3
    //   130: goto +31 -> 161
    //   133: astore_0
    //   134: aconst_null
    //   135: astore_1
    //   136: aload_1
    //   137: astore 4
    //   139: aload 4
    //   141: invokestatic 86	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   144: aload_1
    //   145: invokestatic 86	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   148: aload_3
    //   149: invokestatic 86	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   152: aload_0
    //   153: athrow
    //   154: aconst_null
    //   155: astore_0
    //   156: aload_0
    //   157: astore_1
    //   158: aload 4
    //   160: astore_3
    //   161: aload_1
    //   162: invokestatic 86	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   165: aload_0
    //   166: invokestatic 86	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   169: aload_3
    //   170: invokestatic 86	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   173: aload 5
    //   175: invokevirtual 35	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   178: areturn
    //   179: astore_0
    //   180: goto -26 -> 154
    //   183: astore_0
    //   184: goto -59 -> 125
    //   187: astore_0
    //   188: aload 4
    //   190: astore_3
    //   191: aload_2
    //   192: astore_0
    //   193: goto -32 -> 161
    //   196: astore_3
    //   197: goto -99 -> 98
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	200	0	paramInputStream	java.io.InputStream
    //   22	140	1	localObject1	Object
    //   31	161	2	localInputStreamReader	java.io.InputStreamReader
    //   13	178	3	localObject2	Object
    //   196	1	3	localIOException	IOException
    //   10	1	4	localObject3	Object
    //   83	5	4	localObject4	Object
    //   91	98	4	localObject5	Object
    //   7	167	5	localStringBuilder	StringBuilder
    // Exception table:
    //   from	to	target	type
    //   41	46	83	finally
    //   50	65	83	finally
    //   32	41	105	finally
    //   23	32	114	finally
    //   14	23	133	finally
    //   14	23	179	java/io/IOException
    //   23	32	183	java/io/IOException
    //   32	41	187	java/io/IOException
    //   41	46	196	java/io/IOException
    //   50	65	196	java/io/IOException
  }
  
  public static String a(Date paramDate)
  {
    return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(paramDate);
  }
  
  public static String b(String paramString)
  {
    Object localObject = k.a();
    try
    {
      localObject = ((k)localObject).a(((k)localObject).a(paramString, "IN"), k.c.a);
      return (String)localObject;
    }
    catch (g localg) {}
    return paramString;
  }
  
  public static boolean b(String paramString1, String paramString2)
  {
    if (TextUtils.isEmpty(paramString1)) {
      return true;
    }
    try
    {
      boolean bool = Pattern.compile(paramString1).matcher(paramString2).matches();
      return bool;
    }
    catch (PatternSyntaxException paramString1)
    {
      for (;;) {}
    }
    return false;
  }
  
  public static String d(String paramString)
  {
    return a(new ByteArrayInputStream(Base64.decode(paramString, 2)));
  }
  
  public static HashMap<String, String> g(String paramString)
  {
    HashMap localHashMap = new HashMap();
    try
    {
      paramString = new JSONObject(paramString);
      Iterator localIterator = paramString.keys();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        localHashMap.put(str, paramString.get(str).toString());
      }
      return localHashMap;
    }
    catch (JSONException paramString) {}
    return localHashMap;
  }
  
  private String i(String paramString)
  {
    Object localObject = new StringBuilder("/data/data/");
    ((StringBuilder)localObject).append(a.getPackageName());
    ((StringBuilder)localObject).append("/");
    ((StringBuilder)localObject).append(paramString);
    localObject = new File(((StringBuilder)localObject).toString());
    paramString = new StringBuilder();
    try
    {
      localObject = new BufferedReader(new FileReader((File)localObject));
      for (;;)
      {
        String str = ((BufferedReader)localObject).readLine();
        if (str == null) {
          break;
        }
        paramString.append(str);
        paramString.append('\n');
      }
      ((BufferedReader)localObject).close();
      return paramString.toString();
    }
    catch (IOException paramString)
    {
      for (;;) {}
    }
    return null;
  }
  
  public final o<String> a()
  {
    return o.a(new -..Lambda.ax.xZe3FvVbUeUCfbgzRSWupwJmSuI(this));
  }
  
  /* Error */
  public final String a(String paramString)
  {
    // Byte code:
    //   0: new 73	java/io/BufferedReader
    //   3: dup
    //   4: new 70	java/io/InputStreamReader
    //   7: dup
    //   8: aload_0
    //   9: getfield 13	com/truecaller/truepay/app/utils/ax:a	Landroid/content/Context;
    //   12: invokevirtual 106	android/content/Context:getAssets	()Landroid/content/res/AssetManager;
    //   15: aload_1
    //   16: invokevirtual 114	android/content/res/AssetManager:open	(Ljava/lang/String;)Ljava/io/InputStream;
    //   19: ldc_w 268
    //   22: invokespecial 271	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;Ljava/lang/String;)V
    //   25: invokespecial 76	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   28: astore_2
    //   29: aload_2
    //   30: astore_1
    //   31: new 17	java/lang/StringBuilder
    //   34: dup
    //   35: invokespecial 18	java/lang/StringBuilder:<init>	()V
    //   38: astore_3
    //   39: aload_2
    //   40: astore_1
    //   41: aload_2
    //   42: invokevirtual 79	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   45: astore 4
    //   47: aload 4
    //   49: ifnull +24 -> 73
    //   52: aload_2
    //   53: astore_1
    //   54: aload_3
    //   55: aload 4
    //   57: invokevirtual 22	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   60: pop
    //   61: aload_2
    //   62: astore_1
    //   63: aload_3
    //   64: ldc 81
    //   66: invokevirtual 22	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   69: pop
    //   70: goto -31 -> 39
    //   73: aload_2
    //   74: astore_1
    //   75: aload_3
    //   76: invokevirtual 35	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   79: astore_3
    //   80: aload_2
    //   81: invokevirtual 243	java/io/BufferedReader:close	()V
    //   84: aload_3
    //   85: areturn
    //   86: astore_1
    //   87: aload_1
    //   88: invokevirtual 272	java/io/IOException:toString	()Ljava/lang/String;
    //   91: pop
    //   92: aload_3
    //   93: areturn
    //   94: astore_3
    //   95: goto +12 -> 107
    //   98: astore_2
    //   99: aconst_null
    //   100: astore_1
    //   101: goto +32 -> 133
    //   104: astore_3
    //   105: aconst_null
    //   106: astore_2
    //   107: aload_2
    //   108: astore_1
    //   109: aload_3
    //   110: invokevirtual 272	java/io/IOException:toString	()Ljava/lang/String;
    //   113: pop
    //   114: aload_2
    //   115: ifnull +15 -> 130
    //   118: aload_2
    //   119: invokevirtual 243	java/io/BufferedReader:close	()V
    //   122: aconst_null
    //   123: areturn
    //   124: astore_1
    //   125: aload_1
    //   126: invokevirtual 272	java/io/IOException:toString	()Ljava/lang/String;
    //   129: pop
    //   130: aconst_null
    //   131: areturn
    //   132: astore_2
    //   133: aload_1
    //   134: ifnull +16 -> 150
    //   137: aload_1
    //   138: invokevirtual 243	java/io/BufferedReader:close	()V
    //   141: goto +9 -> 150
    //   144: astore_1
    //   145: aload_1
    //   146: invokevirtual 272	java/io/IOException:toString	()Ljava/lang/String;
    //   149: pop
    //   150: aload_2
    //   151: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	152	0	this	ax
    //   0	152	1	paramString	String
    //   28	53	2	localBufferedReader	BufferedReader
    //   98	1	2	localObject1	Object
    //   106	13	2	localObject2	Object
    //   132	19	2	localObject3	Object
    //   38	55	3	localObject4	Object
    //   94	1	3	localIOException1	IOException
    //   104	6	3	localIOException2	IOException
    //   45	11	4	str	String
    // Exception table:
    //   from	to	target	type
    //   80	84	86	java/io/IOException
    //   31	39	94	java/io/IOException
    //   41	47	94	java/io/IOException
    //   54	61	94	java/io/IOException
    //   63	70	94	java/io/IOException
    //   75	80	94	java/io/IOException
    //   0	29	98	finally
    //   0	29	104	java/io/IOException
    //   118	122	124	java/io/IOException
    //   31	39	132	finally
    //   41	47	132	finally
    //   54	61	132	finally
    //   63	70	132	finally
    //   75	80	132	finally
    //   109	114	132	finally
    //   137	141	144	java/io/IOException
  }
  
  public final void a(String paramString1, String paramString2)
  {
    ((ClipboardManager)a.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("text", paramString1));
    Toast.makeText(a, paramString2, 0).show();
  }
  
  public final String b()
  {
    try
    {
      String str = a(a.getAssets().open("sender_id.json"));
      return str;
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
    return "";
  }
  
  public final com.truecaller.truepay.app.ui.registration.c.d c()
  {
    try
    {
      Object localObject = a(a.getAssets().open("bank_list.json"));
      Type localType = ax.3b;
      localObject = (com.truecaller.truepay.data.api.model.d)new f().a((String)localObject, localType);
      localObject = new b().a((com.truecaller.truepay.data.api.model.d)localObject);
      return (com.truecaller.truepay.app.ui.registration.c.d)localObject;
    }
    catch (Exception localException)
    {
      com.truecaller.log.d.a(localException);
    }
    return null;
  }
  
  public final HashMap<String, SenderInfo> c(String paramString)
  {
    localHashMap = new HashMap();
    try
    {
      Type localType = ax.2b;
      f localf = new f();
      paramString = new JSONObject(paramString);
      Iterator localIterator = paramString.keys();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        Object localObject = paramString.getJSONObject(str).toString();
        JSONArray localJSONArray = (JSONArray)paramString.getJSONObject(str).get("sender_ids");
        localObject = (SenderInfo)localf.a((String)localObject, localType);
        ((SenderInfo)localObject).setSymbol(str);
        int i = 0;
        while (i < localJSONArray.length())
        {
          localHashMap.put(localJSONArray.getString(i), localObject);
          i += 1;
        }
      }
      return localHashMap;
    }
    catch (JSONException paramString)
    {
      com.truecaller.log.d.a(paramString);
    }
  }
  
  public final List<String> d()
  {
    try
    {
      Object localObject = new String(Base64.decode(a("clevertap_app_tags_sheet.json"), 2));
      ArrayList localArrayList = new ArrayList();
      localObject = new JSONObject((String)localObject).getJSONArray("app_ids_list");
      int i = 0;
      while (i < ((JSONArray)localObject).length())
      {
        localArrayList.add(((JSONArray)localObject).getString(i));
        i += 1;
      }
      return localArrayList;
    }
    catch (Exception localException)
    {
      com.truecaller.log.d.a(localException);
    }
    return null;
  }
  
  public final o<String> e(String paramString)
  {
    return o.a(new -..Lambda.ax.kml1FQnqpxSw4BaWoHejWY_7TD8(this, paramString));
  }
  
  public final o<String> f(String paramString)
  {
    return o.a(new -..Lambda.ax.NoebNKrFMtsF9YQEzKGZafaGMPk(this, paramString));
  }
  
  public final com.truecaller.truepay.app.ui.payments.models.a h(String paramString)
  {
    paramString = i(paramString);
    if (paramString != null)
    {
      paramString = d(paramString);
      return (com.truecaller.truepay.app.ui.payments.models.a)new f().a(paramString, com.truecaller.truepay.app.ui.payments.models.a.class);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.utils.ax
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */