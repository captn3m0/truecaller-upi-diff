package com.truecaller.truepay.app.ui.transaction;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
public @interface TransactionType {}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.TransactionType
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */