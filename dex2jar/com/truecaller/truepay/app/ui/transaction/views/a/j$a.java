package com.truecaller.truepay.app.ui.transaction.views.a;

import com.truecaller.truepay.app.ui.transaction.b.b;
import com.truecaller.truepay.data.api.model.a;

public abstract interface j$a
{
  public abstract void onAddBeneficiaryClicked(String paramString);
  
  public abstract void onBeneficiarySelected(b paramb);
  
  public abstract void onFetchingOwnAccountVPA(a parama);
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.a.j.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */