package com.truecaller.truepay.app.ui.transaction.views.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.text.TextUtils;
import android.view.Window;
import android.widget.FrameLayout;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.accounts.views.activities.ManageAccountsActivity;
import com.truecaller.truepay.app.ui.history.views.activities.TruePayWebViewActivity;
import com.truecaller.truepay.app.ui.transaction.a.a.a;
import com.truecaller.truepay.app.ui.transaction.b.f;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.app.ui.transaction.views.a.b.a;
import com.truecaller.truepay.app.ui.transaction.views.a.e;
import com.truecaller.truepay.app.ui.transaction.views.a.f.a;
import com.truecaller.truepay.app.ui.transaction.views.a.h;
import com.truecaller.truepay.app.ui.transaction.views.a.h.b;
import com.truecaller.truepay.app.ui.transaction.views.a.i;
import com.truecaller.truepay.app.ui.transaction.views.a.k;
import com.truecaller.truepay.app.ui.transaction.views.a.m;
import com.truecaller.truepay.app.ui.transaction.views.a.o.a;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.data.d.c;
import com.truecaller.utils.extensions.t;

public class TransactionActivity
  extends com.truecaller.truepay.app.ui.base.views.a.b
  implements b.a, f.a, h.b, com.truecaller.truepay.app.ui.transaction.views.a.j.a, o.a, com.truecaller.truepay.app.ui.transaction.views.c.l
{
  public static boolean isMerchantTxn;
  public static boolean isShowingProgress;
  FrameLayout progressFrameLayout;
  
  private String getDeeplinkHost()
  {
    if ((getIntent() != null) && (getIntent().getData() != null) && (getIntent().getData().getHost() != null) && (getIntent().getData().getHost().length() != 0)) {
      return getIntent().getData().getHost();
    }
    return "";
  }
  
  private String getInvitationMessage()
  {
    return getString(R.string.invitation_message, new Object[] { "truecaller.com/download" });
  }
  
  private String getRoute()
  {
    if (!TextUtils.isEmpty(getDeeplinkHost())) {
      return getDeeplinkHost();
    }
    if ((getIntent().getExtras() != null) && (getIntent().getExtras().containsKey("tranx_type"))) {
      return getIntent().getExtras().getString("tranx_type", "");
    }
    return "";
  }
  
  private void handleDeeplinkToAddBenfy()
  {
    Bundle localBundle = getIntent().getExtras();
    String str2 = "account";
    String str1 = str2;
    if (localBundle != null)
    {
      str1 = str2;
      if (localBundle.getString("benfy_type") != null) {
        str1 = localBundle.getString("benfy_type");
      }
    }
    getIntent().putExtra("tranx_is_beneficiary", true);
    getIntent().putExtra("tranx_type", 1004);
    showContactSelection();
    onAddBeneficiaryClicked(str1);
  }
  
  private void handleDeeplinkToRequest()
  {
    if ((getIntent() != null) && (getIntent().getExtras() != null))
    {
      Object localObject = getIntent().getExtras().getString("payer_vpa");
      String str1 = getIntent().getExtras().getString("amount");
      String str3 = getIntent().getExtras().getString("payer_mobile");
      String str2 = getIntent().getExtras().getString("trnx_comment");
      if ((TextUtils.isEmpty((CharSequence)localObject)) && (TextUtils.isEmpty(str1)) && (TextUtils.isEmpty(str3)))
      {
        showCollectOptionContacts("deeplink");
        return;
      }
      com.truecaller.truepay.app.ui.transaction.b.n localn = new com.truecaller.truepay.app.ui.transaction.b.n();
      p localp = new p();
      e = ((String)localObject);
      f = str3;
      if (localObject != null) {
        localObject = au.b(localObject.split("@")[0]);
      } else {
        localObject = "";
      }
      h = ((String)localObject);
      h = localn;
      e = str1;
      e = str1;
      i = str2;
      m = false;
      localObject = new Bundle();
      ((Bundle)localObject).putSerializable("payable_object", localp);
      ((Bundle)localObject).putInt("tranx_type", 1003);
      showFragment(m.a((Bundle)localObject), true);
      return;
    }
    showCollectOptionContacts("deeplink");
  }
  
  private void handleDeeplinkToSend()
  {
    if ((getIntent() != null) && (getIntent().getExtras() != null))
    {
      Object localObject = getIntent().getExtras().getString("receiver_vpa");
      String str1 = getIntent().getExtras().getString("amount");
      String str2 = getIntent().getExtras().getString("trnx_comment");
      String str3 = getIntent().getExtras().getString("receiver_mobile");
      String str4 = getIntent().getExtras().getString("receiver_acc_num");
      String str5 = getIntent().getExtras().getString("receiver_ifsc");
      String str6 = getIntent().getExtras().getString("receiver_aadhaar_num");
      String str7 = getIntent().getExtras().getString("receiver_iin");
      String str8 = getIntent().getExtras().getString("receiver_name");
      if ((TextUtils.isEmpty((CharSequence)localObject)) && (TextUtils.isEmpty(str3)) && (TextUtils.isEmpty(str4)) && (TextUtils.isEmpty(str5)) && (TextUtils.isEmpty(str6)) && (TextUtils.isEmpty(str7)) && (TextUtils.isEmpty(str8)))
      {
        showContactSelection();
        return;
      }
      com.truecaller.truepay.app.ui.transaction.b.n localn = new com.truecaller.truepay.app.ui.transaction.b.n();
      p localp = new p();
      e = ((String)localObject);
      if (localObject != null) {
        localObject = au.b(localObject.split("@")[0]);
      } else {
        localObject = "";
      }
      h = ((String)localObject);
      a = str4;
      b = str5;
      c = str6;
      d = str7;
      f = str3;
      h = str8;
      h = localn;
      c = "Deeplink";
      d = "pay_other";
      e = str1;
      i = str2;
      m = false;
      localObject = new Bundle();
      ((Bundle)localObject).putSerializable("payable_object", localp);
      ((Bundle)localObject).putInt("tranx_type", 1004);
      showFragment(m.a((Bundle)localObject), true);
      return;
    }
    showContactSelection();
  }
  
  private void handleDeeplinkToShowBeneficiaries()
  {
    getIntent().putExtra("tranx_is_beneficiary", true);
    getIntent().putExtra("tranx_type", 1004);
    showContactSelection();
  }
  
  private void initUI()
  {
    Object localObject = getRoute();
    int i = ((String)localObject).hashCode();
    int j = -1;
    switch (i)
    {
    default: 
      break;
    case 1855357622: 
      if (((String)localObject).equals("trnx_type_send")) {
        i = 3;
      }
      break;
    case 1095692943: 
      if (((String)localObject).equals("request")) {
        i = 5;
      }
      break;
    case 724706375: 
      if (((String)localObject).equals("add_beneficiary")) {
        i = 0;
      }
      break;
    case 140658849: 
      if (((String)localObject).equals("trnx_type_request")) {
        i = 4;
      }
      break;
    case 3526536: 
      if (((String)localObject).equals("send")) {
        i = 2;
      }
      break;
    case -1897995709: 
      if (((String)localObject).equals("beneficiaries")) {
        i = 1;
      }
      break;
    }
    i = -1;
    switch (i)
    {
    default: 
      localObject = new StringBuilder("Host not handled");
      ((StringBuilder)localObject).append(getDeeplinkHost());
      ((StringBuilder)localObject).toString();
      localObject = getIntent().getStringExtra("route");
      if (localObject == null) {
        break label299;
      }
      if (((String)localObject).hashCode() != 224129521) {
        i = j;
      }
      break;
    case 4: 
    case 5: 
      handleDeeplinkToRequest();
      return;
    case 2: 
    case 3: 
      handleDeeplinkToSend();
      return;
    case 1: 
      handleDeeplinkToShowBeneficiaries();
      return;
    case 0: 
      handleDeeplinkToAddBenfy();
      return;
    }
    i = j;
    if (((String)localObject).equals("route_pending_request")) {
      i = 0;
    }
    if (i == 0)
    {
      showPendingRequests();
      return;
    }
    label299:
    if (getIntent().getStringExtra("type") == null)
    {
      showContactSelection();
      return;
    }
    isMerchantTxn = getIntent().getBooleanExtra("is_merchant_txn", false);
    proceedToMerchantPayment();
  }
  
  private void initViews()
  {
    progressFrameLayout = ((FrameLayout)findViewById(R.id.overlay_progress_frame));
  }
  
  private void showAddBeneficiary(String paramString, com.truecaller.truepay.app.ui.transaction.b.a parama)
  {
    int i = getIntent().getIntExtra("tranx_type", 0);
    getSupportFragmentManager().a().b(R.id.payment_container, com.truecaller.truepay.app.ui.transaction.views.a.b.a(paramString, i, parama), com.truecaller.truepay.app.ui.transaction.views.a.b.class.getSimpleName()).a(null).c();
  }
  
  private void showCollectOptionContacts(String paramString)
  {
    getSupportFragmentManager().a().b(R.id.payment_container, e.a(paramString)).a(null).c();
  }
  
  private void showPendingRequests()
  {
    showFragment(com.truecaller.truepay.app.ui.transaction.views.a.o.b(), true);
  }
  
  private void showSendOptionContacts(String paramString, boolean paramBoolean)
  {
    getSupportFragmentManager().a().b(R.id.payment_container, com.truecaller.truepay.app.ui.transaction.views.a.n.a(paramBoolean, paramString), com.truecaller.truepay.app.ui.transaction.views.a.n.class.getSimpleName()).a(null).c();
  }
  
  public static void startForRequest(Context paramContext, String paramString1, String paramString2)
  {
    Intent localIntent = new Intent(paramContext, TransactionActivity.class);
    localIntent.setFlags(536870912);
    Bundle localBundle = new Bundle();
    localBundle.putString("tranx_type", "trnx_type_request");
    localBundle.putString("payer_mobile", paramString1);
    localBundle.putString("payer_name", paramString2);
    localIntent.putExtras(localBundle);
    paramContext.startActivity(localIntent);
  }
  
  public static void startForRequest(Context paramContext, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    Intent localIntent = new Intent(paramContext, TransactionActivity.class);
    localIntent.setFlags(536870912);
    Bundle localBundle = new Bundle();
    localBundle.putString("tranx_type", "trnx_type_request");
    localBundle.putString("amount", paramString1);
    localBundle.putString("trnx_comment", paramString2);
    localBundle.putString("payer_vpa", paramString3);
    localBundle.putString("payer_mobile", paramString4);
    localBundle.putString("payer_name", paramString5);
    localIntent.putExtras(localBundle);
    paramContext.startActivity(localIntent);
  }
  
  public static void startForSend(Context paramContext, String paramString1, String paramString2)
  {
    Intent localIntent = new Intent(paramContext, TransactionActivity.class);
    localIntent.setFlags(536870912);
    Bundle localBundle = new Bundle();
    localBundle.putString("tranx_type", "trnx_type_send");
    localBundle.putString("receiver_mobile", paramString1);
    localBundle.putString("receiver_name", paramString2);
    localIntent.putExtras(localBundle);
    paramContext.startActivity(localIntent);
  }
  
  public static void startForSend(Context paramContext, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    Intent localIntent = new Intent(paramContext, TransactionActivity.class);
    localIntent.setFlags(536870912);
    Bundle localBundle = new Bundle();
    localBundle.putString("tranx_type", "trnx_type_send");
    localBundle.putString("receiver_vpa", paramString2);
    localBundle.putString("amount", paramString1);
    localBundle.putString("trnx_comment", paramString4);
    localBundle.putString("receiver_mobile", paramString5);
    localBundle.putString("receiver_name", paramString3);
    localIntent.putExtras(localBundle);
    paramContext.startActivity(localIntent);
  }
  
  protected String getCurrentFragmentName()
  {
    if (getFragmentCount() > 0) {
      return getSupportFragmentManager().c(getFragmentCount() - 1).h();
    }
    return "";
  }
  
  protected int getFragmentCount()
  {
    return getSupportFragmentManager().e();
  }
  
  public int getLayoutId()
  {
    return R.layout.activity_transaction;
  }
  
  public void hideProgress()
  {
    if (!isFinishing())
    {
      isShowingProgress = false;
      progressFrameLayout.setVisibility(8);
    }
  }
  
  public void initDagger(com.truecaller.truepay.app.a.a.a parama)
  {
    com.truecaller.truepay.app.ui.transaction.a.a.a().a(Truepay.getApplicationComponent()).a().a(this);
  }
  
  public void inviteContact(com.truecaller.truepay.app.ui.transaction.b.a parama)
  {
    onContactInvite(parama);
  }
  
  public void onAcceptClicked(c paramc)
  {
    showFragment(m.a(null, null, paramc, null, 1004), true);
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    if (paramInt1 != 999) {
      return;
    }
    if (paramInt2 == -1)
    {
      initUI();
      return;
    }
  }
  
  public void onAddBeneficiaryClicked(String paramString)
  {
    showAddBeneficiary(paramString, null);
  }
  
  public void onBackPressed()
  {
    t.a(progressFrameLayout, false, 2);
    if (isShowingProgress) {
      return;
    }
    progressFrameLayout.setVisibility(8);
    if ((isMerchantTxn) && ((m.class.getName().equalsIgnoreCase(getCurrentFragmentName())) || (k.class.getName().equalsIgnoreCase(getCurrentFragmentName()))))
    {
      Intent localIntent = new Intent("MERCHANT_INTENT");
      android.support.v4.content.d.a(this).a(localIntent);
      finish();
      return;
    }
    if (getFragmentCount() != 1)
    {
      if (k.class.getName().equalsIgnoreCase(getCurrentFragmentName()))
      {
        if (com.truecaller.truepay.app.ui.transaction.a.a)
        {
          setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
          com.truecaller.truepay.app.ui.transaction.a.a = false;
          super.onBackPressed();
          return;
        }
        finish();
        return;
      }
      super.onBackPressed();
      return;
    }
    finish();
  }
  
  public void onBeneficiaryClicked(com.truecaller.truepay.app.ui.transaction.b.b paramb)
  {
    showFragment(m.a(null, paramb, null, null, 1003), true);
  }
  
  public void onBeneficiarySelected(com.truecaller.truepay.app.ui.transaction.b.b paramb)
  {
    showFragment(m.a(null, paramb, null, null, 1004), true);
  }
  
  public void onBlockedVpaListClicked()
  {
    showFragment(com.truecaller.truepay.app.ui.transaction.views.a.d.f(), true);
  }
  
  public void onContactInvite(com.truecaller.truepay.app.ui.transaction.b.a parama)
  {
    StringBuilder localStringBuilder = new StringBuilder("https://api.whatsapp.com/send?phone=91");
    localStringBuilder.append(e);
    localStringBuilder.append("&text=");
    localStringBuilder.append(getInvitationMessage());
    startActivity(new Intent("android.intent.action.VIEW", Uri.parse(localStringBuilder.toString())));
  }
  
  public void onContactSelected(com.truecaller.truepay.app.ui.transaction.b.a parama, int paramInt)
  {
    showFragment(m.a(parama, null, null, null, paramInt), true);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    initViews();
    if (isUserOnboarded())
    {
      initUI();
      return;
    }
  }
  
  public void onFetchingOwnAccountVPA(com.truecaller.truepay.data.api.model.a parama)
  {
    showFragment(m.a(null, null, null, parama, 1004), true);
  }
  
  public void onPaySavedBeneficiary(com.truecaller.truepay.app.ui.transaction.b.b paramb)
  {
    showFragment(m.a(null, paramb, null, null, 1004), true);
  }
  
  public void onRequestSavedBeneficiary(com.truecaller.truepay.app.ui.transaction.b.b paramb)
  {
    onBeneficiaryClicked(paramb);
  }
  
  public void onResume()
  {
    super.onResume();
  }
  
  public void onSaveBeneficiarySucessful()
  {
    getSupportFragmentManager().c();
  }
  
  public void proceedToMerchantPayment()
  {
    showFragment(m.a(getIntent().getExtras()), true);
  }
  
  public void returnToCallingIntent(p paramp)
  {
    Bundle localBundle = new Bundle();
    com.truecaller.truepay.app.ui.transaction.b.l locall = j;
    localBundle.putString("txnId", o);
    localBundle.putString("responseCode", e);
    localBundle.putString("status", b);
    localBundle.putString("txnRef", n);
    localBundle.putString("message", d);
    paramp = new Intent("MERCHANT_INTENT");
    paramp.putExtras(localBundle);
    android.support.v4.content.d.a(this).a(paramp);
    finish();
  }
  
  public void setStatusBarColor(int paramInt)
  {
    if (Build.VERSION.SDK_INT >= 21) {
      getWindow().setStatusBarColor(paramInt);
    }
  }
  
  public void showAccountChooser(i parami)
  {
    showFragment(parami, true);
  }
  
  public void showAddBeneficiaryUsingAccNumberIfsc(com.truecaller.truepay.app.ui.transaction.b.a parama)
  {
    showAddBeneficiary("account", parama);
  }
  
  public void showContactSelection()
  {
    int i = getIntent().getIntExtra("tranx_type", 1004);
    String str = getIntent().getStringExtra("from");
    if (i == 1003)
    {
      showCollectOptionContacts(str);
      return;
    }
    if (i == 1004)
    {
      showSendOptionContacts(str, getIntent().getBooleanExtra("tranx_is_beneficiary", false));
      return;
    }
  }
  
  public void showFragment(Fragment paramFragment, boolean paramBoolean)
  {
    try
    {
      android.support.v4.app.o localo = getSupportFragmentManager().a();
      if (paramBoolean) {
        localo.a(paramFragment.getClass().getName());
      }
      localo.a(R.id.payment_container, paramFragment, paramFragment.getClass().getSimpleName()).d();
      return;
    }
    catch (Exception paramFragment) {}
  }
  
  public void showInviteOptionPopup(com.truecaller.truepay.app.ui.transaction.b.a parama)
  {
    parama = h.c(parama);
    getSupportFragmentManager().a().a(parama, h.class.getSimpleName()).d();
  }
  
  public void showNeedHelp(String paramString)
  {
    Intent localIntent = new Intent(this, TruePayWebViewActivity.class);
    localIntent.putExtra("url", paramString);
    startActivity(localIntent);
    finish();
  }
  
  public void showPayConfirmation(p paramp)
  {
    showFragment(k.a(paramp), true);
    com.truecaller.truepay.app.ui.dashboard.a.b = true;
  }
  
  public void showProgress()
  {
    if (!isFinishing())
    {
      isShowingProgress = true;
      progressFrameLayout.setVisibility(0);
      progressFrameLayout.setClickable(true);
      j localj = getSupportFragmentManager();
      com.truecaller.truepay.app.ui.registration.views.a locala = new com.truecaller.truepay.app.ui.registration.views.a();
      a = "";
      localj.a().b(R.id.overlay_progress_frame, locala).d();
    }
  }
  
  public void showResetPin(com.truecaller.truepay.data.api.model.a parama, String paramString)
  {
    ManageAccountsActivity.a(this, "action.page.reset_upi_pin", parama, paramString);
    finish();
  }
  
  public void showSetPin(com.truecaller.truepay.data.api.model.a parama, String paramString)
  {
    ManageAccountsActivity.a(this, "action.page.forgot_upi_pin", parama, paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.transaction.views.activities.TransactionActivity
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */