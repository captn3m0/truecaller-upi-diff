package com.truecaller.truepay.app.ui.base.views.a;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public abstract class a
  extends AppCompatActivity
{
  protected abstract int getLayoutId();
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(getLayoutId());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.views.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */