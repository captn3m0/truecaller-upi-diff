package com.truecaller.truepay.app.ui.base.views.a;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Toast;
import com.truecaller.featuretoggles.e;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.payments.views.activities.PaymentsActivity;
import com.truecaller.truepay.app.ui.scan.views.activities.MerchantActivity;
import com.truecaller.truepay.app.utils.u;
import com.truecaller.truepay.app.utils.v.a;
import javax.inject.Inject;

public abstract class b
  extends a
  implements v.a
{
  @Inject
  public e featuresRegistry;
  @Inject
  public u instantRewardHandler;
  
  private void setStatusBar()
  {
    if (Build.VERSION.SDK_INT >= 21) {
      getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
    }
  }
  
  protected abstract void initDagger(com.truecaller.truepay.app.a.a.a parama);
  
  protected boolean isUserOnboarded()
  {
    return Truepay.getInstance().isRegistrationComplete();
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    if ((paramInt1 == 999) && (paramInt2 != -1))
    {
      Toast.makeText(this, R.string.pre_registration_failure, 0).show();
      finish();
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    initDagger(Truepay.getApplicationComponent());
    setStatusBar();
    if ((Truepay.getApplicationComponent() != null) && (!isUserOnboarded()) && (!(this instanceof PaymentsActivity)) && (!(this instanceof MerchantActivity)))
    {
      Truepay.getInstance().openBankingTab(this);
      finish();
    }
  }
  
  public void onPause()
  {
    super.onPause();
    if ((instantRewardHandler != null) && (isUserOnboarded()))
    {
      e locale = featuresRegistry;
      if ((locale != null) && (locale.H().a())) {
        instantRewardHandler.a();
      }
    }
  }
  
  public void onResume()
  {
    super.onResume();
    if ((instantRewardHandler != null) && (isUserOnboarded()))
    {
      e locale = featuresRegistry;
      if ((locale != null) && (locale.H().a())) {
        instantRewardHandler.a(this);
      }
    }
  }
  
  public void onRewardReceived(Intent paramIntent)
  {
    startActivity(paramIntent);
  }
  
  protected void setVisibility(int paramInt, View... paramVarArgs)
  {
    int j = paramVarArgs.length;
    int i = 0;
    while (i < j)
    {
      View localView = paramVarArgs[i];
      if (localView.getVisibility() != paramInt) {
        localView.setVisibility(paramInt);
      }
      i += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.base.views.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */