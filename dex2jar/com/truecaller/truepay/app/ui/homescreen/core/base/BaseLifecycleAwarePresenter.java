package com.truecaller.truepay.app.ui.homescreen.core.base;

import android.arch.lifecycle.e;
import android.arch.lifecycle.e.a;
import android.arch.lifecycle.g;
import android.arch.lifecycle.q;
import com.truecaller.bb;

public abstract class BaseLifecycleAwarePresenter<PV>
  extends bb<PV>
  implements LifecycleAwarePresenter<PV>
{
  protected e a;
  
  @q(a=e.a.ON_DESTROY)
  public void onViewDestroyed()
  {
    e locale = a;
    if (locale != null) {
      locale.b((g)this);
    }
    a = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.core.base.BaseLifecycleAwarePresenter
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */