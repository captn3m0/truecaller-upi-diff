package com.truecaller.truepay.app.ui.homescreen.core.base;

import android.arch.lifecycle.e;
import android.arch.lifecycle.e.a;
import android.arch.lifecycle.g;
import android.arch.lifecycle.q;
import com.truecaller.ba;

public abstract class BaseCoroutineLifecycleAwarePresenter<PV>
  extends ba<PV>
  implements LifecycleAwarePresenter<PV>
{
  protected e c;
  
  @q(a=e.a.ON_DESTROY)
  public void onViewDestroyed()
  {
    e locale = c;
    if (locale != null) {
      locale.b((g)this);
    }
    c = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.core.base.BaseCoroutineLifecycleAwarePresenter
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */