package com.truecaller.truepay.app.ui.homescreen.core.base;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.e;
import android.arch.lifecycle.e.a;
import android.arch.lifecycle.g;
import android.arch.lifecycle.h;
import android.arch.lifecycle.p;
import android.arch.lifecycle.q;
import c.d.f;
import c.g.a.b;
import c.g.b.k;
import c.x;
import com.truecaller.bc;

public abstract class BaseRoutedCoroutineLifecycleAwarePresenter<Router, PV>
  extends bc<Router, PV>
  implements RoutedLifecycleAwarePresenter<Router, PV>
{
  protected e d;
  
  public BaseRoutedCoroutineLifecycleAwarePresenter(f paramf)
  {
    super(paramf);
  }
  
  protected static <T> void a(e parame, LiveData<T> paramLiveData, b<? super T, x> paramb)
  {
    k.b(parame, "receiver$0");
    k.b(paramLiveData, "liveData");
    k.b(paramb, "observer");
    paramLiveData.a((h)new BaseRoutedCoroutineLifecycleAwarePresenter.a(parame), (p)new a(paramb));
  }
  
  public final void a(PV paramPV)
  {
    super.a(paramPV);
    throw ((Throwable)new IllegalStateException("Use #onAttachView(presenterView: PV, lifecycle: Lifecycle) when using BaseRoutedCoroutineLifecycleAwarePresenter"));
  }
  
  public void a(PV paramPV, e parame)
  {
    k.b(parame, "lifecycle");
    super.a(paramPV);
    d = parame;
    parame.a((g)this);
  }
  
  @q(a=e.a.ON_DESTROY)
  public void onViewDestroyed()
  {
    e locale = d;
    if (locale != null) {
      locale.b((g)this);
    }
    d = null;
  }
  
  public final void y_()
  {
    super.y_();
    e locale = d;
    if (locale != null) {
      locale.b((g)this);
    }
    d = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.core.base.BaseRoutedCoroutineLifecycleAwarePresenter
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */