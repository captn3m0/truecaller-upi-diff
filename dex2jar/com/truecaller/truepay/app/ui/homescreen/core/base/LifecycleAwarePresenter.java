package com.truecaller.truepay.app.ui.homescreen.core.base;

import android.arch.lifecycle.e;
import android.arch.lifecycle.e.a;
import android.arch.lifecycle.g;
import android.arch.lifecycle.q;
import com.truecaller.bm;

public abstract interface LifecycleAwarePresenter<PV>
  extends g, bm<PV>
{
  public abstract void a(PV paramPV, e parame);
  
  @q(a=e.a.ON_DESTROY)
  public abstract void onViewDestroyed();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.core.base.LifecycleAwarePresenter
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */