package com.truecaller.truepay.app.ui.homescreen.core.base;

import android.arch.lifecycle.g;
import com.truecaller.bn;

public abstract interface RoutedLifecycleAwarePresenter<Router, PV>
  extends g, bn<Router, PV>, LifecycleAwarePresenter<PV>
{}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.homescreen.core.base.RoutedLifecycleAwarePresenter
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */