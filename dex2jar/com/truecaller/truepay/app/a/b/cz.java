package com.truecaller.truepay.app.a.b;

import com.truecaller.truepay.data.f.ag;
import com.truecaller.truepay.data.g.a.g;
import dagger.a.d;
import javax.inject.Provider;

public final class cz
  implements d<ag>
{
  private final cx a;
  private final Provider<g> b;
  
  private cz(cx paramcx, Provider<g> paramProvider)
  {
    a = paramcx;
    b = paramProvider;
  }
  
  public static cz a(cx paramcx, Provider<g> paramProvider)
  {
    return new cz(paramcx, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.cz
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */