package com.truecaller.truepay.app.a.b;

import android.content.Context;
import com.truecaller.truepay.app.utils.ax;
import dagger.a.d;
import javax.inject.Provider;

public final class da
  implements d<ax>
{
  private final cx a;
  private final Provider<Context> b;
  
  private da(cx paramcx, Provider<Context> paramProvider)
  {
    a = paramcx;
    b = paramProvider;
  }
  
  public static da a(cx paramcx, Provider<Context> paramProvider)
  {
    return new da(paramcx, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.da
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */