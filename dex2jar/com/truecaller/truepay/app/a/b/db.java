package com.truecaller.truepay.app.a.b;

import com.truecaller.truepay.app.utils.ax;
import com.truecaller.truepay.data.f.ag;
import javax.inject.Provider;

public final class db
  implements dagger.a.d<com.truecaller.truepay.d>
{
  private final cx a;
  private final Provider<ag> b;
  private final Provider<ax> c;
  
  private db(cx paramcx, Provider<ag> paramProvider, Provider<ax> paramProvider1)
  {
    a = paramcx;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static db a(cx paramcx, Provider<ag> paramProvider, Provider<ax> paramProvider1)
  {
    return new db(paramcx, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.db
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */