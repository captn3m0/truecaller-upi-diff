package com.truecaller.truepay.app.a.b;

import android.content.Context;
import com.truecaller.truepay.data.g.a.g;
import dagger.a.d;
import javax.inject.Provider;

public final class cy
  implements d<g>
{
  private final cx a;
  private final Provider<Context> b;
  
  private cy(cx paramcx, Provider<Context> paramProvider)
  {
    a = paramcx;
    b = paramProvider;
  }
  
  public static cy a(cx paramcx, Provider<Context> paramProvider)
  {
    return new cy(paramcx, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.a.b.cy
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */