package com.truecaller.truepay;

import android.arch.lifecycle.LiveData;
import com.truecaller.common.payments.a.a;
import java.util.List;

public abstract interface c
{
  public abstract Object a(c.d.c<? super List<TcPayCreditLoanItem>> paramc);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void a(boolean paramBoolean, String paramString1, String paramString2);
  
  public abstract Object c();
  
  public abstract LiveData<a> d();
  
  public abstract void e();
  
  public abstract void f();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */