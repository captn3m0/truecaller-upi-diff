package com.truecaller.truepay.data.f;

import com.truecaller.truepay.SmsBankData;
import io.reactivex.d;
import io.reactivex.o;
import java.util.HashMap;
import java.util.List;

public abstract interface ah
{
  public abstract d<SmsBankData> a(String paramString);
  
  public abstract o<SmsBankData> a(SmsBankData paramSmsBankData);
  
  public abstract o<SmsBankData> a(SmsBankData paramSmsBankData, String paramString, int paramInt);
  
  public abstract HashMap<String, Object> a();
  
  public abstract HashMap<String, Object> b();
  
  public abstract List<SmsBankData> c();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.f.ah
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */