package com.truecaller.truepay.data.f;

import com.truecaller.truepay.SmsBankData;
import io.reactivex.d;
import io.reactivex.o;
import java.util.HashMap;
import java.util.List;

public final class ag
  implements ah
{
  private ah a;
  
  public ag(ah paramah)
  {
    a = paramah;
  }
  
  public final d<SmsBankData> a(String paramString)
  {
    return a.a(paramString);
  }
  
  public final o<SmsBankData> a(SmsBankData paramSmsBankData)
  {
    return a.a(paramSmsBankData);
  }
  
  public final o<SmsBankData> a(SmsBankData paramSmsBankData, String paramString, int paramInt)
  {
    return a.a(paramSmsBankData, paramString, paramInt);
  }
  
  public final HashMap<String, Object> a()
  {
    return a.a();
  }
  
  public final HashMap<String, Object> b()
  {
    return a.b();
  }
  
  public final List<SmsBankData> c()
  {
    return a.c();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.f.ag
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */