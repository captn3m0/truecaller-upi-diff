package com.truecaller.truepay.data.g.a;

import android.content.ContentResolver;
import com.truecaller.truepay.SmsBankData;
import com.truecaller.truepay.data.f.ah;
import com.truecaller.truepay.data.provider.a.b;
import com.truecaller.truepay.data.provider.b.c;
import io.reactivex.a;
import io.reactivex.o;

public final class g
  implements ah
{
  private final ContentResolver a;
  
  public g(ContentResolver paramContentResolver)
  {
    a = paramContentResolver;
  }
  
  public final io.reactivex.d<SmsBankData> a(String paramString)
  {
    return io.reactivex.d.a(new -..Lambda.g.us6UBCYAYfq9y0UhZ-mOGpnRP7g(this, paramString), a.c);
  }
  
  public final o<SmsBankData> a(SmsBankData paramSmsBankData)
  {
    b localb = new b();
    localb.b(paramSmsBankData.getBankSymbol()).a(paramSmsBankData.getBankName()).a(paramSmsBankData.getSimSlotIndex()).a(paramSmsBankData.getSmsCount());
    localb.a(a);
    return o.a(paramSmsBankData);
  }
  
  public final o<SmsBankData> a(SmsBankData paramSmsBankData, String paramString, int paramInt)
  {
    b localb = new b();
    localb.b(paramString).a(paramSmsBankData.getBankName()).a(paramSmsBankData.getSimSlotIndex()).a(Integer.valueOf(paramInt));
    paramString = new com.truecaller.truepay.data.provider.a.d();
    ((com.truecaller.truepay.data.provider.a.d)paramString.a(new String[] { paramSmsBankData.getBankName() }).d()).c(new String[] { paramSmsBankData.getBankSymbol() });
    a.update(localb.b(), localb.c(), a.toString(), paramString.e());
    return o.a(paramSmsBankData);
  }
  
  /* Error */
  public final java.util.HashMap<String, Object> a()
  {
    // Byte code:
    //   0: new 207	java/util/HashMap
    //   3: dup
    //   4: invokespecial 208	java/util/HashMap:<init>	()V
    //   7: astore 5
    //   9: iconst_0
    //   10: istore_1
    //   11: iload_1
    //   12: iconst_2
    //   13: if_icmpge +225 -> 238
    //   16: iconst_2
    //   17: newarray <illegal type>
    //   19: dup
    //   20: iconst_0
    //   21: iconst_0
    //   22: iastore
    //   23: dup
    //   24: iconst_1
    //   25: iconst_1
    //   26: iastore
    //   27: iload_1
    //   28: iaload
    //   29: istore_2
    //   30: new 23	com/truecaller/truepay/data/provider/a/d
    //   33: dup
    //   34: invokespecial 24	com/truecaller/truepay/data/provider/a/d:<init>	()V
    //   37: astore_3
    //   38: aload_3
    //   39: iconst_1
    //   40: newarray <illegal type>
    //   42: dup
    //   43: iconst_0
    //   44: iload_2
    //   45: iastore
    //   46: invokevirtual 211	com/truecaller/truepay/data/provider/a/d:a	([I)Lcom/truecaller/truepay/data/provider/a/d;
    //   49: pop
    //   50: aload_3
    //   51: aload_0
    //   52: getfield 15	com/truecaller/truepay/data/g/a/g:a	Landroid/content/ContentResolver;
    //   55: invokevirtual 39	com/truecaller/truepay/data/provider/a/d:a	(Landroid/content/ContentResolver;)Lcom/truecaller/truepay/data/provider/a/c;
    //   58: astore 6
    //   60: aconst_null
    //   61: astore 4
    //   63: aload 4
    //   65: astore_3
    //   66: aload 6
    //   68: invokevirtual 45	com/truecaller/truepay/data/provider/a/c:getCount	()I
    //   71: ifle +93 -> 164
    //   74: aload 4
    //   76: astore_3
    //   77: new 213	java/util/HashSet
    //   80: dup
    //   81: invokespecial 214	java/util/HashSet:<init>	()V
    //   84: astore 7
    //   86: aload 4
    //   88: astore_3
    //   89: aload 6
    //   91: invokevirtual 49	com/truecaller/truepay/data/provider/a/c:moveToFirst	()Z
    //   94: pop
    //   95: aload 4
    //   97: astore_3
    //   98: aload 6
    //   100: invokevirtual 52	com/truecaller/truepay/data/provider/a/c:isAfterLast	()Z
    //   103: ifne +31 -> 134
    //   106: aload 4
    //   108: astore_3
    //   109: aload 7
    //   111: aload 6
    //   113: ldc 65
    //   115: invokevirtual 60	com/truecaller/truepay/data/provider/a/c:b	(Ljava/lang/String;)Ljava/lang/String;
    //   118: invokevirtual 218	java/util/HashSet:add	(Ljava/lang/Object;)Z
    //   121: pop
    //   122: aload 4
    //   124: astore_3
    //   125: aload 6
    //   127: invokevirtual 92	com/truecaller/truepay/data/provider/a/c:moveToNext	()Z
    //   130: pop
    //   131: goto -36 -> 95
    //   134: aload 4
    //   136: astore_3
    //   137: aload 5
    //   139: getstatic 224	java/util/Locale:ENGLISH	Ljava/util/Locale;
    //   142: ldc -30
    //   144: iconst_1
    //   145: anewarray 4	java/lang/Object
    //   148: dup
    //   149: iconst_0
    //   150: iload_2
    //   151: invokestatic 173	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   154: aastore
    //   155: invokestatic 230	java/lang/String:format	(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   158: aload 7
    //   160: invokevirtual 234	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   163: pop
    //   164: aload 6
    //   166: ifnull +65 -> 231
    //   169: aload 6
    //   171: invokevirtual 97	com/truecaller/truepay/data/provider/a/c:close	()V
    //   174: goto +57 -> 231
    //   177: astore 4
    //   179: goto +11 -> 190
    //   182: astore 4
    //   184: aload 4
    //   186: astore_3
    //   187: aload 4
    //   189: athrow
    //   190: aload 6
    //   192: ifnull +31 -> 223
    //   195: aload_3
    //   196: ifnull +22 -> 218
    //   199: aload 6
    //   201: invokevirtual 97	com/truecaller/truepay/data/provider/a/c:close	()V
    //   204: goto +19 -> 223
    //   207: astore 6
    //   209: aload_3
    //   210: aload 6
    //   212: invokevirtual 101	java/lang/Throwable:addSuppressed	(Ljava/lang/Throwable;)V
    //   215: goto +8 -> 223
    //   218: aload 6
    //   220: invokevirtual 97	com/truecaller/truepay/data/provider/a/c:close	()V
    //   223: aload 4
    //   225: athrow
    //   226: astore_3
    //   227: aload_3
    //   228: invokestatic 237	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   231: iload_1
    //   232: iconst_1
    //   233: iadd
    //   234: istore_1
    //   235: goto -224 -> 11
    //   238: aload 5
    //   240: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	241	0	this	g
    //   10	225	1	i	int
    //   29	122	2	j	int
    //   37	173	3	localObject1	Object
    //   226	2	3	localException	Exception
    //   61	74	4	localObject2	Object
    //   177	1	4	localObject3	Object
    //   182	42	4	localThrowable1	Throwable
    //   7	232	5	localHashMap	java.util.HashMap
    //   58	142	6	localc	com.truecaller.truepay.data.provider.a.c
    //   207	12	6	localThrowable2	Throwable
    //   84	75	7	localHashSet	java.util.HashSet
    // Exception table:
    //   from	to	target	type
    //   66	74	177	finally
    //   77	86	177	finally
    //   89	95	177	finally
    //   98	106	177	finally
    //   109	122	177	finally
    //   125	131	177	finally
    //   137	164	177	finally
    //   187	190	177	finally
    //   66	74	182	java/lang/Throwable
    //   77	86	182	java/lang/Throwable
    //   89	95	182	java/lang/Throwable
    //   98	106	182	java/lang/Throwable
    //   109	122	182	java/lang/Throwable
    //   125	131	182	java/lang/Throwable
    //   137	164	182	java/lang/Throwable
    //   199	204	207	java/lang/Throwable
    //   50	60	226	java/lang/Exception
    //   169	174	226	java/lang/Exception
    //   199	204	226	java/lang/Exception
    //   209	215	226	java/lang/Exception
    //   218	223	226	java/lang/Exception
    //   223	226	226	java/lang/Exception
  }
  
  /* Error */
  public final java.util.HashMap<String, Object> b()
  {
    // Byte code:
    //   0: new 207	java/util/HashMap
    //   3: dup
    //   4: invokespecial 208	java/util/HashMap:<init>	()V
    //   7: astore_3
    //   8: new 23	com/truecaller/truepay/data/provider/a/d
    //   11: dup
    //   12: invokespecial 24	com/truecaller/truepay/data/provider/a/d:<init>	()V
    //   15: astore_1
    //   16: aload_1
    //   17: invokevirtual 241	com/truecaller/truepay/data/provider/a/d:b	()Lcom/truecaller/truepay/data/provider/a/d;
    //   20: pop
    //   21: aload_1
    //   22: aload_0
    //   23: getfield 15	com/truecaller/truepay/data/g/a/g:a	Landroid/content/ContentResolver;
    //   26: invokevirtual 39	com/truecaller/truepay/data/provider/a/d:a	(Landroid/content/ContentResolver;)Lcom/truecaller/truepay/data/provider/a/c;
    //   29: astore 4
    //   31: aconst_null
    //   32: astore_2
    //   33: aload_2
    //   34: astore_1
    //   35: aload 4
    //   37: invokevirtual 45	com/truecaller/truepay/data/provider/a/c:getCount	()I
    //   40: ifle +62 -> 102
    //   43: aload_2
    //   44: astore_1
    //   45: aload 4
    //   47: invokevirtual 49	com/truecaller/truepay/data/provider/a/c:moveToFirst	()Z
    //   50: ifeq +52 -> 102
    //   53: aload_2
    //   54: astore_1
    //   55: aload_3
    //   56: ldc 65
    //   58: aload 4
    //   60: ldc 65
    //   62: invokevirtual 60	com/truecaller/truepay/data/provider/a/c:b	(Ljava/lang/String;)Ljava/lang/String;
    //   65: invokevirtual 234	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   68: pop
    //   69: aload_2
    //   70: astore_1
    //   71: aload_3
    //   72: ldc 57
    //   74: aload 4
    //   76: ldc 57
    //   78: invokevirtual 60	com/truecaller/truepay/data/provider/a/c:b	(Ljava/lang/String;)Ljava/lang/String;
    //   81: invokevirtual 234	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   84: pop
    //   85: aload_2
    //   86: astore_1
    //   87: aload_3
    //   88: ldc -13
    //   90: aload 4
    //   92: invokevirtual 70	com/truecaller/truepay/data/provider/a/c:a	()I
    //   95: invokestatic 173	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   98: invokevirtual 234	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   101: pop
    //   102: aload 4
    //   104: ifnull +59 -> 163
    //   107: aload 4
    //   109: invokevirtual 97	com/truecaller/truepay/data/provider/a/c:close	()V
    //   112: aload_3
    //   113: areturn
    //   114: astore_2
    //   115: goto +8 -> 123
    //   118: astore_2
    //   119: aload_2
    //   120: astore_1
    //   121: aload_2
    //   122: athrow
    //   123: aload 4
    //   125: ifnull +31 -> 156
    //   128: aload_1
    //   129: ifnull +22 -> 151
    //   132: aload 4
    //   134: invokevirtual 97	com/truecaller/truepay/data/provider/a/c:close	()V
    //   137: goto +19 -> 156
    //   140: astore 4
    //   142: aload_1
    //   143: aload 4
    //   145: invokevirtual 101	java/lang/Throwable:addSuppressed	(Ljava/lang/Throwable;)V
    //   148: goto +8 -> 156
    //   151: aload 4
    //   153: invokevirtual 97	com/truecaller/truepay/data/provider/a/c:close	()V
    //   156: aload_2
    //   157: athrow
    //   158: astore_1
    //   159: aload_1
    //   160: invokestatic 237	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   163: aload_3
    //   164: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	165	0	this	g
    //   15	128	1	localObject1	Object
    //   158	2	1	localException	Exception
    //   32	54	2	localObject2	Object
    //   114	1	2	localObject3	Object
    //   118	39	2	localThrowable1	Throwable
    //   7	157	3	localHashMap	java.util.HashMap
    //   29	104	4	localc	com.truecaller.truepay.data.provider.a.c
    //   140	12	4	localThrowable2	Throwable
    // Exception table:
    //   from	to	target	type
    //   35	43	114	finally
    //   45	53	114	finally
    //   55	69	114	finally
    //   71	85	114	finally
    //   87	102	114	finally
    //   121	123	114	finally
    //   35	43	118	java/lang/Throwable
    //   45	53	118	java/lang/Throwable
    //   55	69	118	java/lang/Throwable
    //   71	85	118	java/lang/Throwable
    //   87	102	118	java/lang/Throwable
    //   132	137	140	java/lang/Throwable
    //   21	31	158	java/lang/Exception
    //   107	112	158	java/lang/Exception
    //   132	137	158	java/lang/Exception
    //   142	148	158	java/lang/Exception
    //   151	156	158	java/lang/Exception
    //   156	158	158	java/lang/Exception
  }
  
  /* Error */
  public final java.util.List<SmsBankData> c()
  {
    // Byte code:
    //   0: new 246	java/util/ArrayList
    //   3: dup
    //   4: invokespecial 247	java/util/ArrayList:<init>	()V
    //   7: astore 4
    //   9: new 23	com/truecaller/truepay/data/provider/a/d
    //   12: dup
    //   13: invokespecial 24	com/truecaller/truepay/data/provider/a/d:<init>	()V
    //   16: astore_2
    //   17: aload_2
    //   18: invokevirtual 241	com/truecaller/truepay/data/provider/a/d:b	()Lcom/truecaller/truepay/data/provider/a/d;
    //   21: pop
    //   22: aload_2
    //   23: aload_0
    //   24: getfield 15	com/truecaller/truepay/data/g/a/g:a	Landroid/content/ContentResolver;
    //   27: invokevirtual 39	com/truecaller/truepay/data/provider/a/d:a	(Landroid/content/ContentResolver;)Lcom/truecaller/truepay/data/provider/a/c;
    //   30: astore 5
    //   32: aconst_null
    //   33: astore_3
    //   34: aload_3
    //   35: astore_2
    //   36: aload 5
    //   38: invokevirtual 45	com/truecaller/truepay/data/provider/a/c:getCount	()I
    //   41: ifle +91 -> 132
    //   44: aload_3
    //   45: astore_2
    //   46: aload 5
    //   48: invokevirtual 49	com/truecaller/truepay/data/provider/a/c:moveToFirst	()Z
    //   51: ifeq +81 -> 132
    //   54: aload_3
    //   55: astore_2
    //   56: new 54	com/truecaller/truepay/SmsBankData$a
    //   59: dup
    //   60: invokespecial 55	com/truecaller/truepay/SmsBankData$a:<init>	()V
    //   63: astore 6
    //   65: aload_3
    //   66: astore_2
    //   67: aload 6
    //   69: aload 5
    //   71: ldc 57
    //   73: invokevirtual 60	com/truecaller/truepay/data/provider/a/c:b	(Ljava/lang/String;)Ljava/lang/String;
    //   76: putfield 63	com/truecaller/truepay/SmsBankData$a:b	Ljava/lang/String;
    //   79: aload_3
    //   80: astore_2
    //   81: aload 6
    //   83: aload 5
    //   85: ldc 65
    //   87: invokevirtual 60	com/truecaller/truepay/data/provider/a/c:b	(Ljava/lang/String;)Ljava/lang/String;
    //   90: putfield 68	com/truecaller/truepay/SmsBankData$a:c	Ljava/lang/String;
    //   93: aload_3
    //   94: astore_2
    //   95: aload 6
    //   97: aload 5
    //   99: invokevirtual 70	com/truecaller/truepay/data/provider/a/c:a	()I
    //   102: putfield 73	com/truecaller/truepay/SmsBankData$a:a	I
    //   105: aload_3
    //   106: astore_2
    //   107: aload 4
    //   109: aload 6
    //   111: invokevirtual 84	com/truecaller/truepay/SmsBankData$a:a	()Lcom/truecaller/truepay/SmsBankData;
    //   114: invokeinterface 250 2 0
    //   119: pop
    //   120: aload_3
    //   121: astore_2
    //   122: aload 5
    //   124: invokevirtual 92	com/truecaller/truepay/data/provider/a/c:moveToNext	()Z
    //   127: istore_1
    //   128: iload_1
    //   129: ifne -75 -> 54
    //   132: aload 5
    //   134: ifnull +60 -> 194
    //   137: aload 5
    //   139: invokevirtual 97	com/truecaller/truepay/data/provider/a/c:close	()V
    //   142: aload 4
    //   144: areturn
    //   145: astore_3
    //   146: goto +8 -> 154
    //   149: astore_3
    //   150: aload_3
    //   151: astore_2
    //   152: aload_3
    //   153: athrow
    //   154: aload 5
    //   156: ifnull +31 -> 187
    //   159: aload_2
    //   160: ifnull +22 -> 182
    //   163: aload 5
    //   165: invokevirtual 97	com/truecaller/truepay/data/provider/a/c:close	()V
    //   168: goto +19 -> 187
    //   171: astore 5
    //   173: aload_2
    //   174: aload 5
    //   176: invokevirtual 101	java/lang/Throwable:addSuppressed	(Ljava/lang/Throwable;)V
    //   179: goto +8 -> 187
    //   182: aload 5
    //   184: invokevirtual 97	com/truecaller/truepay/data/provider/a/c:close	()V
    //   187: aload_3
    //   188: athrow
    //   189: astore_2
    //   190: aload_2
    //   191: invokestatic 237	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   194: aload 4
    //   196: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	197	0	this	g
    //   127	2	1	bool	boolean
    //   16	158	2	localObject1	Object
    //   189	2	2	localException	Exception
    //   33	88	3	localObject2	Object
    //   145	1	3	localObject3	Object
    //   149	39	3	localThrowable1	Throwable
    //   7	188	4	localArrayList	java.util.ArrayList
    //   30	134	5	localc	com.truecaller.truepay.data.provider.a.c
    //   171	12	5	localThrowable2	Throwable
    //   63	47	6	locala	com.truecaller.truepay.SmsBankData.a
    // Exception table:
    //   from	to	target	type
    //   36	44	145	finally
    //   46	54	145	finally
    //   56	65	145	finally
    //   67	79	145	finally
    //   81	93	145	finally
    //   95	105	145	finally
    //   107	120	145	finally
    //   122	128	145	finally
    //   152	154	145	finally
    //   36	44	149	java/lang/Throwable
    //   46	54	149	java/lang/Throwable
    //   56	65	149	java/lang/Throwable
    //   67	79	149	java/lang/Throwable
    //   81	93	149	java/lang/Throwable
    //   95	105	149	java/lang/Throwable
    //   107	120	149	java/lang/Throwable
    //   122	128	149	java/lang/Throwable
    //   163	168	171	java/lang/Throwable
    //   22	32	189	java/lang/Exception
    //   137	142	189	java/lang/Exception
    //   163	168	189	java/lang/Exception
    //   173	179	189	java/lang/Exception
    //   182	187	189	java/lang/Exception
    //   187	189	189	java/lang/Exception
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.data.g.a.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */