package com.truecaller.truepay;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.Uri.Builder;
import com.truecaller.truepay.app.a.a.b.a;
import com.truecaller.truepay.app.a.b.br;
import com.truecaller.truepay.app.a.b.ct;
import com.truecaller.truepay.app.a.b.dc;
import com.truecaller.truepay.app.a.b.dj;
import com.truecaller.truepay.app.a.b.x;
import com.truecaller.truepay.app.ui.dashboard.views.b.d;
import com.truecaller.truepay.app.ui.payments.views.b.j;
import com.truecaller.truepay.app.ui.registration.d.u;
import com.truecaller.truepay.app.ui.registration.views.b.h;
import com.truecaller.truepay.app.utils.PaymentPresence;
import com.truecaller.truepay.app.utils.ag;
import com.truecaller.truepay.app.utils.n;
import com.truecaller.utils.t;
import com.truecaller.utils.t.a;
import dagger.a.g;
import javax.inject.Inject;

public class Truepay
{
  private static com.truecaller.truepay.app.a.a.a applicationComponent;
  @Inject
  public com.truecaller.truepay.data.b.a analyticLoggerHelper;
  @Inject
  public Application application;
  public c creditHelper;
  @Inject
  public n dynamicShortcutHelper;
  @Inject
  public com.truecaller.featuretoggles.e featuresRegistry;
  private TcPaySDKListener listener;
  @Inject
  public com.truecaller.truepay.app.utils.ab payAppUpdateManager;
  @Inject
  public ag paymentPresenceHelper;
  @Inject
  public com.truecaller.truepay.data.e.e prefSecretToken;
  @Inject
  public com.truecaller.truepay.data.e.e prefTempToken;
  @Inject
  public com.truecaller.truepay.data.e.e prefUserUuid;
  @Inject
  public com.truecaller.common.background.b scheduler;
  @Inject
  public com.truecaller.truepay.data.e.c securePreferences;
  @Inject
  public f userRegisteredListener;
  
  public static com.truecaller.truepay.app.a.a.a getApplicationComponent()
  {
    return applicationComponent;
  }
  
  private com.truecaller.truepay.app.ui.base.views.fragments.b getBankingHomeFragment()
  {
    if (featuresRegistry.ad().a()) {
      return com.truecaller.truepay.app.ui.homescreen.views.d.a.k();
    }
    return d.b();
  }
  
  private com.truecaller.truepay.app.ui.base.views.fragments.b getBankingIntroFragmentV2()
  {
    return h.d("banking");
  }
  
  public static Truepay getInstance()
  {
    return Truepay.a.a;
  }
  
  private com.truecaller.truepay.app.ui.base.views.fragments.b getPaymentsIntroFragmentV2()
  {
    return h.d("payments");
  }
  
  private void initDagger(Application paramApplication)
  {
    b.a locala = com.truecaller.truepay.app.a.a.b.aw();
    a = ((com.truecaller.truepay.app.a.b.c)g.a(new com.truecaller.truepay.app.a.b.c(paramApplication)));
    l = ((t)g.a(com.truecaller.utils.c.a().a(paramApplication).a()));
    m = ((com.truecaller.common.a)g.a(((com.truecaller.common.b.a)paramApplication).u()));
    g.a(a, com.truecaller.truepay.app.a.b.c.class);
    if (b == null) {
      b = new dj();
    }
    if (c == null) {
      c = new com.truecaller.truepay.app.a.b.ab();
    }
    if (d == null) {
      d = new br();
    }
    if (e == null) {
      e = new ct();
    }
    if (f == null) {
      f = new com.truecaller.truepay.app.a.b.a();
    }
    if (g == null) {
      g = new u();
    }
    if (h == null) {
      h = new com.truecaller.truepay.app.ui.npci.a.a();
    }
    if (i == null) {
      i = new x();
    }
    if (j == null) {
      j = new dc();
    }
    if (k == null) {
      k = new com.truecaller.truepay.data.a.ab();
    }
    g.a(l, t.class);
    g.a(m, com.truecaller.common.a.class);
    paramApplication = new com.truecaller.truepay.app.a.a.b(a, b, c, d, e, f, g, h, i, j, k, l, m, (byte)0);
    applicationComponent = paramApplication;
    paramApplication.a(this);
  }
  
  private void initScheduler()
  {
    scheduler.a(20001, new int[] { 20003, 20004 });
  }
  
  private void initShortcuts()
  {
    dynamicShortcutHelper.a();
    dynamicShortcutHelper.a(application.getString(R.string.shortcut_banking_short_label), R.drawable.ic_shortcut_banking, 4, new Intent[] { new Intent("android.intent.action.VIEW", Uri.parse("truecaller://home/tabs/banking").buildUpon().appendQueryParameter("deeplink_source", "app_shortcut").build()) });
    dynamicShortcutHelper.a(application.getString(R.string.shortcut_payments_short_label), R.drawable.ic_shortcut_payments, 5, new Intent[] { new Intent("android.intent.action.VIEW", Uri.parse("truecaller://home/tabs/payments").buildUpon().appendQueryParameter("deeplink_source", "app_shortcut").build()) });
    dynamicShortcutHelper.b();
  }
  
  private void initStrictMode() {}
  
  private void initTLog()
  {
    com.truecaller.log.f.a = false;
  }
  
  private void initTcPaySDKListener(Application paramApplication)
  {
    if ((paramApplication != null) && ((paramApplication instanceof TcPaySDKListener)))
    {
      listener = ((TcPaySDKListener)paramApplication);
      return;
    }
    paramApplication = new StringBuilder("Application must implement ");
    paramApplication.append(TcPaySDKListener.class.getSimpleName());
    throw new IllegalStateException(paramApplication.toString());
  }
  
  private void initUserRegisteredListener()
  {
    if (isRegistrationComplete())
    {
      userRegisteredListener.b();
      return;
    }
    userRegisteredListener.a();
  }
  
  public static void initialize(Application paramApplication)
  {
    getInstance().initDagger(paramApplication);
    getInstance().initTLog();
    getInstance().initStrictMode();
    getInstance().initScheduler();
    getInstance().initTcPaySDKListener(paramApplication);
    getInstance().initShortcuts();
    getInstance().initUserRegisteredListener();
  }
  
  private boolean isRegistrationSuccess()
  {
    return 104 == securePreferences.d("j<@$f)qntd=?5e!y").intValue();
  }
  
  public com.truecaller.truepay.data.b.a getAnalyticLoggerHelper()
  {
    return analyticLoggerHelper;
  }
  
  public com.truecaller.truepay.app.ui.base.views.fragments.b getBankingFragment()
  {
    if (isRegistrationComplete()) {
      return getBankingHomeFragment();
    }
    return getBankingIntroFragmentV2();
  }
  
  public TcPaySDKListener getListener()
  {
    return listener;
  }
  
  public com.truecaller.truepay.app.ui.base.views.fragments.b getPaymentsFragment()
  {
    if (featuresRegistry.o().a())
    {
      if (isRegistrationComplete()) {
        return getPaymentsHomeFragment();
      }
      return getPaymentsIntroFragmentV2();
    }
    return com.truecaller.truepay.app.ui.dashboard.views.b.e.b();
  }
  
  public com.truecaller.truepay.app.ui.base.views.fragments.b getPaymentsHomeFragment()
  {
    return j.b();
  }
  
  public PaymentPresence getPresenceInfo()
  {
    if (applicationComponent != null)
    {
      ag localag = paymentPresenceHelper;
      if (localag != null) {
        return localag.a();
      }
    }
    return new PaymentPresence(false, 1, 0);
  }
  
  public void handleAppUpdate(boolean paramBoolean)
  {
    payAppUpdateManager.a(paramBoolean);
  }
  
  public void incrementBankingClicked()
  {
    if (applicationComponent != null)
    {
      int i = securePreferences.d("P)7`@F]CxES%Xuf!").intValue();
      securePreferences.a("P)7`@F]CxES%Xuf!", Integer.valueOf(i + 1));
    }
  }
  
  public void incrementPaymentsClicked()
  {
    if (applicationComponent != null)
    {
      int i = securePreferences.d("DR!~Tm9k7N>2jv`,").intValue();
      securePreferences.a("DR!~Tm9k7N>2jv`,", Integer.valueOf(i + 1));
    }
  }
  
  public boolean isRegistrationComplete()
  {
    if (applicationComponent != null) {
      return (prefUserUuid.b()) && (prefSecretToken.b()) && (isRegistrationSuccess());
    }
    return false;
  }
  
  public void openBankingTab(Context paramContext)
  {
    Intent localIntent = new Intent();
    localIntent.setAction("android.intent.action.VIEW");
    localIntent.setData(Uri.parse("truecaller://home/tabs/banking"));
    paramContext.startActivity(localIntent);
  }
  
  public void openPaymentsTab(Context paramContext)
  {
    Intent localIntent = new Intent();
    localIntent.setAction("android.intent.action.VIEW");
    localIntent.setData(Uri.parse("truecaller://home/tabs/payments"));
    paramContext.startActivity(localIntent);
  }
  
  public void setCreditHelper(c paramc)
  {
    creditHelper = paramc;
  }
  
  public boolean shouldShowBankingAsNew()
  {
    if (applicationComponent != null) {
      return securePreferences.d("P)7`@F]CxES%Xuf!").intValue() < 3;
    }
    return false;
  }
  
  public boolean shouldShowPaymentsAsNew()
  {
    if (applicationComponent != null) {
      return securePreferences.d("DR!~Tm9k7N>2jv`,").intValue() < 3;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.Truepay
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */