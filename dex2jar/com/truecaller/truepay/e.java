package com.truecaller.truepay;

import com.truecaller.common.h.am;
import com.truecaller.truepay.app.utils.ax;
import com.truecaller.truepay.data.f.ag;
import io.reactivex.o;
import java.util.HashMap;

public final class e
  implements d
{
  HashMap<String, SenderInfo> a;
  ag b;
  private ax c;
  
  public e(ag paramag, ax paramax)
  {
    b = paramag;
    c = paramax;
    c.a().b(io.reactivex.g.a.b()).a(new -..Lambda.e.iafG-XFz_g0xU2gOc8aM1kRvYK4(this)).a(io.reactivex.android.b.a.a()).a(new e.1(this));
  }
  
  public final SenderInfo a(String paramString)
  {
    if (!am.a(paramString)) {
      return null;
    }
    HashMap localHashMap = a;
    Object localObject = localHashMap;
    if (localHashMap == null)
    {
      localObject = c;
      localObject = ((ax)localObject).c(((ax)localObject).b());
      a = ((HashMap)localObject);
    }
    return (SenderInfo)((HashMap)localObject).get(paramString);
  }
  
  public final SmsBankData a()
  {
    Object localObject = b.b();
    String str1 = (String)((HashMap)localObject).get("bank_symbol");
    String str2 = (String)((HashMap)localObject).get("bank_name");
    localObject = (Integer)((HashMap)localObject).get("sim_slot");
    if ((str2 != null) && (str1 != null) && (localObject != null))
    {
      SmsBankData.a locala = new SmsBankData.a();
      b = str2;
      c = str1;
      a = ((Integer)localObject).intValue();
      return locala.a();
    }
    return null;
  }
  
  public final void a(String paramString, int paramInt)
  {
    paramString = a(paramString);
    if ((paramString != null) && ("bank".equalsIgnoreCase(paramString.getCategory())))
    {
      Object localObject = new SmsBankData.a();
      b = paramString.getName();
      a = paramInt;
      c = paramString.getSymbol();
      d = Integer.valueOf(1);
      localObject = ((SmsBankData.a)localObject).a();
      b.a(paramString.getName()).b(io.reactivex.g.a.b()).a(new e.2(this, (SmsBankData)localObject));
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */