package com.truecaller.multisim;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Telephony.Mms;
import android.provider.Telephony.Sms;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.truecaller.callhistory.k;
import com.truecaller.multisim.a.b;
import com.truecaller.multisim.a.e;
import com.truecaller.multisim.b.a;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class i
  implements h
{
  final Context a;
  final a b;
  final b c;
  private final k d;
  private String e;
  private String f;
  private String g;
  private volatile boolean h = false;
  private volatile boolean i = false;
  private volatile boolean j = false;
  
  i(Context paramContext)
  {
    a = paramContext.getApplicationContext();
    d = k.a(paramContext);
    b = new a(a);
    int k = Build.VERSION.SDK_INT;
    if (k >= 26) {
      paramContext = new e(paramContext);
    } else if (k >= 23) {
      paramContext = new com.truecaller.multisim.a.d(paramContext);
    } else {
      paramContext = new com.truecaller.multisim.a.c(paramContext);
    }
    c = paramContext;
  }
  
  public static h a(Context paramContext, TelephonyManager paramTelephonyManager)
  {
    String str = Build.MANUFACTURER.toLowerCase();
    i.a[] arrayOfa = i.a.values();
    int m = arrayOfa.length;
    int k = 0;
    while (k < m)
    {
      Object localObject = arrayOfa[k];
      if ((Build.VERSION.SDK_INT >= s) && ((t == null) || (str.contains(t))))
      {
        localObject = r.create(paramContext, paramTelephonyManager);
        if (localObject != null)
        {
          paramContext = new StringBuilder("Creating MultiSimManager ");
          paramContext.append(localObject.getClass().getSimpleName());
          com.truecaller.multisim.b.c.a(new String[] { paramContext.toString() });
          return (h)localObject;
        }
      }
      k += 1;
    }
    com.truecaller.multisim.b.c.a(new String[] { "Creating MultiSimManager SingleSimManager" });
    return new ag(paramContext, paramTelephonyManager);
  }
  
  private boolean a(Uri paramUri, String paramString)
  {
    if (!TextUtils.isEmpty(paramString)) {}
    try
    {
      paramUri = a.getContentResolver().query(paramUri, new String[] { paramString }, null, null, "_id ASC LIMIT 1");
      if (paramUri != null) {
        paramUri.close();
      }
      return true;
    }
    catch (Throwable paramUri)
    {
      for (;;) {}
    }
    com.truecaller.multisim.b.c.a();
    return false;
  }
  
  public d a(Cursor paramCursor)
  {
    return new i.b(this, paramCursor);
  }
  
  public void a(String paramString) {}
  
  @SuppressLint({"NewApi"})
  public final String b()
  {
    if (h) {
      return e;
    }
    try
    {
      if (h)
      {
        str = e;
        return str;
      }
      if (!b.a(new String[] { "android.permission.READ_SMS" })) {
        return null;
      }
      String str = m();
      if (a(Telephony.Sms.CONTENT_URI, str)) {
        e = str;
      }
      h = true;
      return e;
    }
    finally {}
  }
  
  @SuppressLint({"NewApi"})
  public final String c()
  {
    if (i) {
      return f;
    }
    try
    {
      if (i)
      {
        str = f;
        return str;
      }
      if (!b.a(new String[] { "android.permission.READ_SMS" })) {
        return null;
      }
      String str = n();
      if (a(Telephony.Mms.CONTENT_URI, str)) {
        f = str;
      }
      i = true;
      return f;
    }
    finally {}
  }
  
  public final String d()
  {
    if (j) {
      return g;
    }
    try
    {
      if (j)
      {
        str = g;
        return str;
      }
      if (!b.a(new String[] { "android.permission.READ_CALL_LOG" })) {
        return null;
      }
      String str = o();
      if (a(d.a(), str)) {
        g = str;
      }
      j = true;
      return g;
    }
    finally {}
  }
  
  public SmsManager f(String paramString)
  {
    return SmsManager.getDefault();
  }
  
  public final int g(String paramString)
  {
    return c.b(paramString);
  }
  
  public String g()
  {
    return "-1";
  }
  
  public final List<String> i()
  {
    Object localObject = h();
    ArrayList localArrayList = new ArrayList();
    localObject = ((List)localObject).iterator();
    while (((Iterator)localObject).hasNext())
    {
      SimInfo localSimInfo = (SimInfo)((Iterator)localObject).next();
      if (!TextUtils.isEmpty(h)) {
        localArrayList.add(h);
      } else {
        localArrayList.add("");
      }
    }
    return localArrayList;
  }
  
  public boolean l()
  {
    return false;
  }
  
  protected abstract String m();
  
  protected abstract String n();
  
  protected abstract String o();
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */