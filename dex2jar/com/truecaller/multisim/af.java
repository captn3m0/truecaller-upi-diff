package com.truecaller.multisim;

import android.os.SystemClock;
import java.util.HashMap;
import java.util.Map;

public final class af
  implements ae
{
  private long a;
  private final h b;
  private Map<String, SimInfo> c = new HashMap();
  
  public af(h paramh)
  {
    b = paramh;
  }
  
  public final SimInfo a(String paramString)
  {
    int i;
    if (a + 3000L < SystemClock.elapsedRealtime()) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      c.clear();
    }
    a = SystemClock.elapsedRealtime();
    if (c.containsKey(paramString)) {
      return (SimInfo)c.get(paramString);
    }
    SimInfo localSimInfo = b.b(paramString);
    c.put(paramString, localSimInfo);
    return localSimInfo;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.af
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */