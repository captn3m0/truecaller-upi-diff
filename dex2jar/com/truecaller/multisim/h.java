package com.truecaller.multisim;

import android.app.PendingIntent;
import android.content.Intent;
import android.database.Cursor;
import android.telephony.SmsManager;
import java.util.ArrayList;
import java.util.List;

public abstract interface h
{
  public abstract SimInfo a(int paramInt);
  
  public abstract d a(Cursor paramCursor);
  
  public abstract String a();
  
  public abstract String a(Intent paramIntent);
  
  public abstract void a(Intent paramIntent, String paramString);
  
  public abstract void a(String paramString);
  
  public abstract boolean a(String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, String paramString4);
  
  public abstract boolean a(String paramString1, String paramString2, ArrayList<String> paramArrayList, ArrayList<PendingIntent> paramArrayList1, ArrayList<PendingIntent> paramArrayList2, String paramString3);
  
  public abstract SimInfo b(String paramString);
  
  public abstract String b();
  
  public abstract String b(Intent paramIntent);
  
  public abstract a c(String paramString);
  
  public abstract String c();
  
  public abstract String d();
  
  public abstract String d(String paramString);
  
  public abstract String e(String paramString);
  
  public abstract boolean e();
  
  public abstract SmsManager f(String paramString);
  
  public abstract String f();
  
  public abstract int g(String paramString);
  
  public abstract String g();
  
  public abstract List<SimInfo> h();
  
  public abstract List<String> i();
  
  public abstract boolean j();
  
  public abstract boolean k();
  
  public abstract boolean l();
}

/* Location:
 * Qualified Name:     com.truecaller.multisim.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */