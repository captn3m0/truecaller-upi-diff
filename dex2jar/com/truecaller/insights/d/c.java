package com.truecaller.insights.d;

import android.content.Context;
import android.content.res.AssetManager;
import c.g.b.k;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public final class c
  implements b
{
  private final Context a;
  
  public c(Context paramContext)
  {
    a = paramContext;
  }
  
  public final BufferedReader a(String paramString)
  {
    k.b(paramString, "fileName");
    Object localObject = null;
    try
    {
      InputStream localInputStream = a.getAssets().open(paramString);
      paramString = (String)localObject;
      if (localInputStream != null) {
        paramString = new BufferedReader((Reader)new InputStreamReader(localInputStream));
      }
      return paramString;
    }
    catch (IOException paramString) {}
    return null;
  }
  
  public final String a(InputStream paramInputStream)
  {
    k.b(paramInputStream, "inputStream");
    StringBuilder localStringBuilder = new StringBuilder(16384);
    String str = null;
    try
    {
      try
      {
        paramInputStream = new InputStreamReader(paramInputStream, "UTF8");
        BufferedReader localBufferedReader;
        label96:
        a((Closeable)paramInputStream);
      }
      finally
      {
        try
        {
          try
          {
            localBufferedReader = new BufferedReader((Reader)paramInputStream);
            for (str = localBufferedReader.readLine(); str != null; str = localBufferedReader.readLine())
            {
              localStringBuilder.append(str);
              localStringBuilder.append('\n');
            }
            a((Closeable)paramInputStream);
          }
          finally
          {
            break label96;
          }
        }
        catch (IOException localIOException)
        {
          for (;;) {}
        }
        localObject2 = finally;
        paramInputStream = null;
        a((Closeable)paramInputStream);
        throw ((Throwable)localObject2);
      }
    }
    catch (IOException paramInputStream)
    {
      for (;;)
      {
        paramInputStream = (InputStream)localObject2;
      }
    }
    paramInputStream = localStringBuilder.toString();
    k.a(paramInputStream, "outPut.toString()");
    return paramInputStream;
  }
  
  public final void a(Closeable paramCloseable)
  {
    if (paramCloseable != null) {}
    try
    {
      paramCloseable.close();
      return;
    }
    catch (IOException paramCloseable) {}
    return;
    return;
  }
  
  public final InputStream b(String paramString)
  {
    k.b(paramString, "fileName");
    try
    {
      paramString = a.getAssets().open(paramString);
      return paramString;
    }
    catch (IOException paramString)
    {
      for (;;) {}
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.d.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */