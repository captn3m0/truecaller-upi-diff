package com.truecaller.insights.d;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.InputStream;

public abstract interface b
{
  public abstract BufferedReader a(String paramString);
  
  public abstract String a(InputStream paramInputStream);
  
  public abstract void a(Closeable paramCloseable);
  
  public abstract InputStream b(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.insights.d.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */