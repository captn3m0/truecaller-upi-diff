package com.truecaller.insights.core.a;

import c.g.a.m;
import c.n;
import com.truecaller.insights.d.b;
import com.twelfthmile.malana.compiler.types.MalanaSeed;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ao;

public final class a
  implements MalanaSeed
{
  private String a;
  private String b;
  private String c;
  private String d;
  private String e;
  private String f;
  private String g;
  private String h;
  private final List<ao<n<String, String>>> i;
  private final b j;
  private final c.d.f k;
  
  @Inject
  public a(b paramb, @Named("IO") c.d.f paramf)
  {
    j = paramb;
    k = paramf;
    i = ((List)new ArrayList());
    kotlinx.coroutines.f.a(k, (m)new a.1(this, null));
  }
  
  public final String getMalanaAddr()
  {
    return f;
  }
  
  public final String getMalanaAirport()
  {
    return d;
  }
  
  public final String getMalanaBank()
  {
    return e;
  }
  
  public final String getMalanaLocation()
  {
    return c;
  }
  
  public final String getMalanaOffers()
  {
    return b;
  }
  
  public final String getMalanaSemantic()
  {
    return g;
  }
  
  public final String getMalanaSyntax()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */