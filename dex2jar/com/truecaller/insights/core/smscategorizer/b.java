package com.truecaller.insights.core.smscategorizer;

import android.util.Patterns;
import c.a.m;
import c.a.y;
import c.u;
import com.truecaller.insights.core.smscategorizer.db.KeywordCounts;
import com.truecaller.insights.core.smscategorizer.db.MetaData;
import com.truecaller.insights.core.smscategorizer.db.g;
import com.truecaller.tracking.events.bb.a;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class b
  implements a
{
  private final List<String> a;
  private final g b;
  
  public b(g paramg)
  {
    b = paramg;
    a = b.a();
  }
  
  private static String b(String paramString, bb.a parama)
  {
    paramString = Patterns.EMAIL_ADDRESS.matcher((CharSequence)paramString).replaceAll(" EMAIL ");
    c.g.b.k.a(paramString, "emailMatcher.replaceAll(EMAIL_STRING)");
    paramString = c.b().matcher((CharSequence)paramString).replaceAll(" DATE ");
    c.g.b.k.a(paramString, "dateMatcher.replaceAll(DATE_STRING)");
    paramString = Patterns.WEB_URL.matcher((CharSequence)paramString).replaceAll(" URL ");
    c.g.b.k.a(paramString, "urlMatcher.replaceAll(URL_STRING)");
    CharSequence localCharSequence = (CharSequence)paramString;
    paramString = new c.n.k(" URL ").a(localCharSequence, 0);
    ListIterator localListIterator;
    int i;
    if (!paramString.isEmpty())
    {
      localListIterator = paramString.listIterator(paramString.size());
      while (localListIterator.hasPrevious())
      {
        if (((CharSequence)localListIterator.previous()).length() == 0) {
          i = 1;
        } else {
          i = 0;
        }
        if (i == 0)
        {
          paramString = m.d((Iterable)paramString, localListIterator.nextIndex() + 1);
          break label178;
        }
      }
    }
    paramString = (List)y.a;
    label178:
    paramString = (Collection)paramString;
    if (paramString != null)
    {
      paramString = paramString.toArray(new String[0]);
      if (paramString != null)
      {
        parama.b(paramString.length - 1);
        paramString = new c.n.k(c.c()).a(localCharSequence, 0);
        if (!paramString.isEmpty())
        {
          localListIterator = paramString.listIterator(paramString.size());
          while (localListIterator.hasPrevious())
          {
            if (((CharSequence)localListIterator.previous()).length() == 0) {
              i = 1;
            } else {
              i = 0;
            }
            if (i == 0)
            {
              paramString = m.d((Iterable)paramString, localListIterator.nextIndex() + 1);
              break label319;
            }
          }
        }
        paramString = (List)y.a;
        label319:
        paramString = (Collection)paramString;
        if (paramString != null)
        {
          paramString = paramString.toArray(new String[0]);
          if (paramString != null)
          {
            parama.a(paramString.length - 1);
            paramString = new c.n.k(c.c()).a(localCharSequence, " NUMBER ");
            paramString = c.d().matcher((CharSequence)paramString).replaceAll(" CURRENCY ");
            c.g.b.k.a(paramString, "currencyMatcher.replaceAll(CURRENCY_STRING)");
            if (paramString != null)
            {
              paramString = paramString.toLowerCase();
              c.g.b.k.a(paramString, "(this as java.lang.String).toLowerCase()");
              parama = new StringBuilder();
              parama.append(paramString);
              parama.append(" z");
              paramString = (CharSequence)parama.toString();
              return new c.n.k("[0-9]").a(paramString, "");
            }
            throw new u("null cannot be cast to non-null type java.lang.String");
          }
          throw new u("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new u("null cannot be cast to non-null type java.util.Collection<T>");
      }
      throw new u("null cannot be cast to non-null type kotlin.Array<T>");
    }
    throw new u("null cannot be cast to non-null type java.util.Collection<T>");
  }
  
  public final int a(String paramString, bb.a parama)
  {
    c.g.b.k.b(paramString, "message");
    c.g.b.k.b(parama, "metadataBuilder");
    Object localObject1 = new StringBuilder("getCalculatedScore start time ");
    ((StringBuilder)localObject1).append(System.currentTimeMillis());
    ((StringBuilder)localObject1).toString();
    localObject1 = b(paramString, parama);
    paramString = (CharSequence)localObject1;
    paramString = new c.n.k("[.,;:'\"()?!@+#]?\\s+").a(paramString, 0);
    Object localObject2;
    int i;
    if (!paramString.isEmpty())
    {
      localObject2 = paramString.listIterator(paramString.size());
      while (((ListIterator)localObject2).hasPrevious())
      {
        if (((CharSequence)((ListIterator)localObject2).previous()).length() == 0) {
          i = 1;
        } else {
          i = 0;
        }
        if (i == 0)
        {
          paramString = m.d((Iterable)paramString, ((ListIterator)localObject2).nextIndex() + 1);
          break label161;
        }
      }
    }
    paramString = (List)y.a;
    label161:
    paramString = (Collection)paramString;
    if (paramString != null)
    {
      paramString = paramString.toArray(new String[0]);
      if (paramString != null)
      {
        localObject2 = (String[])paramString;
        Double[] arrayOfDouble = new Double[4];
        arrayOfDouble[0] = Double.valueOf(0.0D);
        arrayOfDouble[1] = Double.valueOf(0.0D);
        arrayOfDouble[2] = Double.valueOf(0.0D);
        arrayOfDouble[3] = Double.valueOf(0.0D);
        List localList = b.c();
        double d1 = b.b();
        double d2 = ((MetaData)localList.get(0)).getInstances();
        Double.isNaN(d2);
        double d3 = d2 / d1;
        d2 = ((MetaData)localList.get(1)).getInstances();
        Double.isNaN(d2);
        double d4 = d2 / d1;
        d2 = ((MetaData)localList.get(2)).getInstances();
        Double.isNaN(d2);
        d2 /= d1;
        double d5 = ((MetaData)localList.get(3)).getInstances();
        Double.isNaN(d5);
        d1 = d5 / d1;
        b localb = (b)this;
        int m = 0;
        i = 0;
        int j = 0;
        int k = 0;
        while (i < localObject2.length - 1)
        {
          paramString = new StringBuilder();
          paramString.append(localObject2[i]);
          paramString.append(" ");
          int n = i + 1;
          paramString.append(localObject2[n]);
          paramString = paramString.toString();
          if (b.a(paramString) != null)
          {
            i = n;
          }
          else
          {
            paramString = localObject2[i];
            if (a.contains(paramString))
            {
              i = n;
              continue;
            }
          }
          KeywordCounts localKeywordCounts = b.a(paramString);
          if (localKeywordCounts != null)
          {
            if (c.a().matcher((CharSequence)paramString).find()) {
              j += 1;
            } else {
              k += 1;
            }
            m += paramString.length();
            d5 = arrayOfDouble[0].doubleValue();
            double d6 = Double.parseDouble(Integer.toString(localKeywordCounts.getRanValue()));
            double d7 = ((MetaData)localList.get(0)).getWordsCount();
            Double.isNaN(d7);
            arrayOfDouble[0] = Double.valueOf(d5 + Math.log10(d6 / d7));
            d5 = arrayOfDouble[1].doubleValue();
            d6 = Double.parseDouble(Integer.toString(localKeywordCounts.getPamValue()));
            d7 = ((MetaData)localList.get(1)).getWordsCount();
            Double.isNaN(d7);
            arrayOfDouble[1] = Double.valueOf(d5 + Math.log10(d6 / d7));
            d5 = arrayOfDouble[2].doubleValue();
            d6 = Double.parseDouble(Integer.toString(localKeywordCounts.getHamValue()));
            d7 = ((MetaData)localList.get(2)).getWordsCount();
            Double.isNaN(d7);
            arrayOfDouble[2] = Double.valueOf(d5 + Math.log10(d6 / d7));
            d5 = arrayOfDouble[3].doubleValue();
            d6 = Double.parseDouble(Integer.toString(localKeywordCounts.getOtpValue()));
            d7 = ((MetaData)localList.get(3)).getWordsCount();
            Double.isNaN(d7);
            arrayOfDouble[3] = Double.valueOf(d5 + Math.log10(d6 / d7));
          }
          i += 1;
        }
        paramString = parama.b(Integer.valueOf(j)).a(Integer.valueOf(k));
        c.g.b.k.a(paramString, "metadataBuilder.setNumBi…am(numIdentifiedUnigrams)");
        paramString.c(localObject2.length - 1);
        if ((arrayOfDouble[1].doubleValue() != 0.0D) && (m >= ((String)localObject1).length() / 5) && (m >= 20))
        {
          arrayOfDouble[0] = Double.valueOf(arrayOfDouble[0].doubleValue() + Math.log10(d3));
          arrayOfDouble[1] = Double.valueOf(arrayOfDouble[1].doubleValue() + Math.log10(d4));
          arrayOfDouble[2] = Double.valueOf(arrayOfDouble[2].doubleValue() + Math.log10(d2));
          arrayOfDouble[3] = Double.valueOf(arrayOfDouble[3].doubleValue() + Math.log10(d1));
          d1 = -1000000.0D;
          i = 0;
          j = 0;
          while (i <= 3)
          {
            d2 = d1;
            if (arrayOfDouble[i].doubleValue() > d1)
            {
              d2 = arrayOfDouble[i].doubleValue();
              j = i;
            }
            i += 1;
            d1 = d2;
          }
          if (arrayOfDouble[j].doubleValue() != 0.0D)
          {
            arrayOfDouble[0] = Double.valueOf(arrayOfDouble[0].doubleValue() / d1);
            arrayOfDouble[1] = Double.valueOf(arrayOfDouble[1].doubleValue() / d1);
            arrayOfDouble[2] = Double.valueOf(arrayOfDouble[2].doubleValue() / d1);
            arrayOfDouble[3] = Double.valueOf(arrayOfDouble[3].doubleValue() / d1);
          }
          arrayOfDouble[1] = Double.valueOf(arrayOfDouble[1].doubleValue() + 0.016D);
          d1 = 10.0D;
          i = 0;
          while (i <= 3)
          {
            d2 = d1;
            if (arrayOfDouble[i].doubleValue() <= d1)
            {
              d2 = arrayOfDouble[i].doubleValue();
              j = i;
            }
            i += 1;
            d1 = d2;
          }
          paramString = new StringBuilder("getCalculatedScore end ");
          paramString.append(System.currentTimeMillis());
          paramString.toString();
          return j;
        }
        return 2;
      }
      throw new u("null cannot be cast to non-null type kotlin.Array<T>");
    }
    throw new u("null cannot be cast to non-null type java.util.Collection<T>");
  }
  
  public final void a(String paramString, int paramInt1, int paramInt2, bb.a parama)
  {
    c.g.b.k.b(paramString, "message");
    c.g.b.k.b(parama, "metadataBuilder");
    if (paramInt1 != paramInt2)
    {
      b.d();
      b.a(paramInt2);
      c.g.b.k.b(paramString, "message");
      c.g.b.k.b(parama, "metadataBuilder");
      paramString = (CharSequence)b(paramString, parama);
      Object localObject = new c.n.k("[.,;:'\"()?!@+#]?\\s+");
      int i = 0;
      paramString = ((c.n.k)localObject).a(paramString, 0);
      if (!paramString.isEmpty())
      {
        localObject = paramString.listIterator(paramString.size());
        while (((ListIterator)localObject).hasPrevious())
        {
          if (((CharSequence)((ListIterator)localObject).previous()).length() == 0) {
            paramInt1 = 1;
          } else {
            paramInt1 = 0;
          }
          if (paramInt1 == 0)
          {
            paramString = m.d((Iterable)paramString, ((ListIterator)localObject).nextIndex() + 1);
            break label174;
          }
        }
      }
      paramString = (List)y.a;
      label174:
      paramString = (Collection)paramString;
      if (paramString != null)
      {
        paramString = paramString.toArray(new String[0]);
        if (paramString != null)
        {
          paramString = (String[])paramString;
          int j = 0;
          int k = 0;
          paramInt1 = i;
          int m;
          for (i = k; paramInt1 < paramString.length - 1; i = m)
          {
            localObject = new StringBuilder();
            ((StringBuilder)localObject).append(paramString[paramInt1]);
            ((StringBuilder)localObject).append(" ");
            int n = paramInt1 + 1;
            ((StringBuilder)localObject).append(paramString[n]);
            localObject = ((StringBuilder)localObject).toString();
            if (b.a(paramInt2, (String)localObject) == 0)
            {
              localObject = paramString[paramInt1];
              k = j;
              m = i;
              n = paramInt1;
              if (b.a(paramInt2, (String)localObject) != 0)
              {
                m = i + 1;
                k = j;
                n = paramInt1;
              }
            }
            else
            {
              k = j + 1;
              m = i;
            }
            paramInt1 = n + 1;
            j = k;
          }
          parama = parama.b(Integer.valueOf(j)).a(Integer.valueOf(i));
          c.g.b.k.a(parama, "metadataBuilder.setNumBi…am(numIdentifiedUnigrams)");
          parama.c(paramString.length - 1);
          return;
        }
        throw new u("null cannot be cast to non-null type kotlin.Array<T>");
      }
      throw new u("null cannot be cast to non-null type java.util.Collection<T>");
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.smscategorizer.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */