package com.truecaller.insights.core.smscategorizer;

import c.a.ag;
import c.g.b.k;
import c.n;
import c.t;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.f;
import com.truecaller.tracking.events.ac.a;
import com.truecaller.tracking.events.ad;
import com.truecaller.tracking.events.ad.a;
import com.truecaller.tracking.events.ax;
import com.truecaller.tracking.events.ba;
import com.truecaller.tracking.events.ba.a;
import com.truecaller.tracking.events.bb;
import com.truecaller.tracking.events.bb.a;
import java.util.List;
import javax.inject.Inject;

public final class e
  implements d
{
  @Inject
  public dagger.a<a> a;
  private final b b;
  private final f<ae> c;
  
  @Inject
  public e(b paramb, f<ae> paramf)
  {
    b = paramb;
    c = paramf;
  }
  
  private static String a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return "";
    case 3: 
      return "OTP";
    case 2: 
      return "Other";
    case 1: 
      return "Spam";
    }
    return "Transaction";
  }
  
  public final int a(String paramString1, String paramString2, boolean paramBoolean, List<? extends ax> paramList, ac.a parama)
  {
    k.b(paramString1, "senderId");
    k.b(paramString2, "message");
    k.b(paramList, "eventParticipants");
    k.b(parama, "smsCategorizerCompareEvent");
    paramString1 = bb.g();
    Object localObject = a;
    if (localObject == null) {
      k.a("scoreCalculatorLazy");
    }
    localObject = (a)((dagger.a)localObject).get();
    k.a(paramString1, "metadataBuilder");
    int i = ((a)localObject).a(paramString2, paramString1);
    if (paramBoolean)
    {
      paramString2 = a(i);
      paramString1 = paramString1.a();
      k.a(paramString1, "metadataBuilder.build()");
      paramString1 = ag.a(new n[] { t.a("numUnigram", String.valueOf(paramString1.e().intValue())), t.a("numBigram", String.valueOf(paramString1.f().intValue())), t.a("numNumbers", String.valueOf(paramString1.b().intValue())), t.a("numUrls", String.valueOf(paramString1.c().intValue())), t.a("numWords", String.valueOf(paramString1.d().intValue())) });
      parama.a(paramList);
      parama.a((CharSequence)paramString2);
      parama.a();
      parama.a(paramString1);
      paramString2 = new e.a("SmsCategorizer");
      switch (i)
      {
      default: 
        break;
      case 3: 
        paramString2.a("Type", "OTP");
        break;
      case 2: 
        paramString2.a("Type", "Other");
        break;
      case 1: 
        paramString2.a("Type", "Spam");
        break;
      case 0: 
        paramString2.a("Type", "Transaction");
      }
      paramString1 = b;
      paramString2 = paramString2.a();
      k.a(paramString2, "event.build()");
      paramString1.b(paramString2);
    }
    return i;
  }
  
  public final void a(String paramString, int paramInt1, int paramInt2, List<? extends ax> paramList)
  {
    k.b(paramString, "message");
    k.b(paramList, "eventParticipants");
    Object localObject1 = bb.g();
    Object localObject2 = a;
    if (localObject2 == null) {
      k.a("scoreCalculatorLazy");
    }
    localObject2 = (a)((dagger.a)localObject2).get();
    k.a(localObject1, "metadataBuilder");
    ((a)localObject2).a(paramString, paramInt1, paramInt2, (bb.a)localObject1);
    localObject2 = new e.a("SmsCategorizerReclassification");
    switch (paramInt2)
    {
    default: 
      break;
    case 3: 
      ((e.a)localObject2).a("ReclassificationType", "OTP");
      break;
    case 2: 
      ((e.a)localObject2).a("ReclassificationType", "Other");
      break;
    case 1: 
      ((e.a)localObject2).a("ReclassificationType", "Spam");
      break;
    case 0: 
      ((e.a)localObject2).a("ReclassificationType", "Transaction");
    }
    paramString = b;
    localObject2 = ((e.a)localObject2).a();
    k.a(localObject2, "event.build()");
    paramString.b((com.truecaller.analytics.e)localObject2);
    localObject2 = a(paramInt1);
    String str = a(paramInt2);
    paramString = ((bb.a)localObject1).a();
    k.a(paramString, "metadataBuilder.build()");
    localObject1 = ad.b().a((CharSequence)localObject2).b((CharSequence)str);
    localObject2 = ba.b().a((CharSequence)"RawOccurrences").a().b();
    k.a(localObject2, "SmsCategorizerModel.newB…VERSION)\n        .build()");
    paramString = ((ad.a)localObject1).a((ba)localObject2).a(paramString).a(paramList).a();
    ((ae)c.a()).a((org.apache.a.d.d)paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.smscategorizer.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */