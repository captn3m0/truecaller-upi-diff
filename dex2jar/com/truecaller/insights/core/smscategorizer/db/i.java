package com.truecaller.insights.core.smscategorizer.db;

import java.util.List;

public abstract interface i
{
  public abstract List<MetaData> a();
  
  public abstract void a(String paramString);
  
  public abstract int b(String paramString);
  
  public abstract List<KeywordCounts> b();
  
  public abstract int c();
  
  public abstract int c(String paramString);
  
  public abstract int d(String paramString);
  
  public abstract List<String> d();
  
  public abstract int e(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.smscategorizer.db.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */