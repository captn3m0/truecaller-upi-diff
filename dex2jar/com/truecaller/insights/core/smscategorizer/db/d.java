package com.truecaller.insights.core.smscategorizer.db;

import android.arch.persistence.db.b;
import android.arch.persistence.room.f.b;
import c.g.b.k;
import javax.inject.Inject;

public final class d
  extends f.b
{
  private final a a;
  
  @Inject
  public d(a parama)
  {
    a = parama;
  }
  
  public final void a(b paramb)
  {
    k.b(paramb, "db");
    a.c(paramb);
    a.a(paramb);
    a.b(paramb);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.smscategorizer.db.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */