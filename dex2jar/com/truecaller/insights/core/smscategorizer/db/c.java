package com.truecaller.insights.core.smscategorizer.db;

import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d<b>
{
  private final Provider<com.truecaller.insights.d.b> a;
  
  private c(Provider<com.truecaller.insights.d.b> paramProvider)
  {
    a = paramProvider;
  }
  
  public static c a(Provider<com.truecaller.insights.d.b> paramProvider)
  {
    return new c(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.smscategorizer.db.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */