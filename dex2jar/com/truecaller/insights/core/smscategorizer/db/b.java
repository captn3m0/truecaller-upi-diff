package com.truecaller.insights.core.smscategorizer.db;

import c.g.b.k;
import c.n.d;
import com.google.gson.f;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Iterator;
import javax.inject.Inject;

public final class b
  implements a
{
  private final f a;
  private final com.truecaller.insights.d.b b;
  
  @Inject
  public b(com.truecaller.insights.d.b paramb)
  {
    b = paramb;
    a = new f();
  }
  
  /* Error */
  public final com.truecaller.insights.models.PdoBinderType a()
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aload_0
    //   3: getfield 25	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   6: ldc 37
    //   8: invokeinterface 42 2 0
    //   13: astore_1
    //   14: aload_0
    //   15: getfield 30	com/truecaller/insights/core/smscategorizer/db/b:a	Lcom/google/gson/f;
    //   18: aload_1
    //   19: checkcast 44	java/io/Reader
    //   22: ldc 46
    //   24: invokevirtual 49	com/google/gson/f:a	(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;
    //   27: checkcast 46	com/truecaller/insights/models/PdoBinderType$PdoBinder
    //   30: astore_3
    //   31: aload_0
    //   32: getfield 25	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   35: aload_1
    //   36: checkcast 51	java/io/Closeable
    //   39: invokeinterface 54 2 0
    //   44: aload_3
    //   45: astore_1
    //   46: goto +46 -> 92
    //   49: astore_2
    //   50: aload_1
    //   51: astore_3
    //   52: goto +8 -> 60
    //   55: astore_1
    //   56: aconst_null
    //   57: astore_3
    //   58: aload_1
    //   59: astore_2
    //   60: aload_0
    //   61: getfield 25	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   64: aload_3
    //   65: checkcast 51	java/io/Closeable
    //   68: invokeinterface 54 2 0
    //   73: aload_2
    //   74: athrow
    //   75: aconst_null
    //   76: astore_1
    //   77: aload_0
    //   78: getfield 25	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   81: aload_1
    //   82: checkcast 51	java/io/Closeable
    //   85: invokeinterface 54 2 0
    //   90: aload_2
    //   91: astore_1
    //   92: aload_1
    //   93: ifnull +8 -> 101
    //   96: aload_1
    //   97: checkcast 56	com/truecaller/insights/models/PdoBinderType
    //   100: areturn
    //   101: getstatic 61	com/truecaller/insights/models/PdoBinderType$a:a	Lcom/truecaller/insights/models/PdoBinderType$a;
    //   104: checkcast 56	com/truecaller/insights/models/PdoBinderType
    //   107: areturn
    //   108: astore_1
    //   109: goto -34 -> 75
    //   112: astore_3
    //   113: goto -36 -> 77
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	116	0	this	b
    //   13	38	1	localObject1	Object
    //   55	4	1	localObject2	Object
    //   76	21	1	localObject3	Object
    //   108	1	1	localException1	Exception
    //   1	1	2	localObject4	Object
    //   49	1	2	localObject5	Object
    //   59	32	2	localObject6	Object
    //   30	35	3	localObject7	Object
    //   112	1	3	localException2	Exception
    // Exception table:
    //   from	to	target	type
    //   14	31	49	finally
    //   2	14	55	finally
    //   2	14	108	java/lang/Exception
    //   14	31	112	java/lang/Exception
  }
  
  /* Error */
  public final void a(android.arch.persistence.db.b paramb)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 66
    //   3: invokestatic 20	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aconst_null
    //   7: astore 7
    //   9: aconst_null
    //   10: astore 8
    //   12: aload_0
    //   13: getfield 25	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   16: ldc 68
    //   18: invokeinterface 42 2 0
    //   23: astore 6
    //   25: aload 6
    //   27: ifnonnull +18 -> 45
    //   30: aload_0
    //   31: getfield 25	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   34: aload 6
    //   36: checkcast 51	java/io/Closeable
    //   39: invokeinterface 54 2 0
    //   44: return
    //   45: aload 6
    //   47: astore 8
    //   49: aload 6
    //   51: astore 7
    //   53: aload 6
    //   55: invokevirtual 74	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   58: astore 9
    //   60: aload 6
    //   62: astore 7
    //   64: aload 9
    //   66: ifnull +495 -> 561
    //   69: aload 6
    //   71: astore 8
    //   73: aload 6
    //   75: astore 7
    //   77: aload 9
    //   79: checkcast 76	java/lang/CharSequence
    //   82: astore 9
    //   84: aload 6
    //   86: astore 8
    //   88: aload 6
    //   90: astore 7
    //   92: aload 9
    //   94: invokeinterface 80 1 0
    //   99: iconst_1
    //   100: isub
    //   101: istore_3
    //   102: iconst_0
    //   103: istore_2
    //   104: iconst_0
    //   105: istore 4
    //   107: goto +490 -> 597
    //   110: aload 6
    //   112: astore 8
    //   114: aload 6
    //   116: astore 7
    //   118: aload 9
    //   120: iload 5
    //   122: invokeinterface 84 2 0
    //   127: bipush 32
    //   129: if_icmpgt +490 -> 619
    //   132: iconst_1
    //   133: istore 5
    //   135: goto +487 -> 622
    //   138: aload 6
    //   140: astore 8
    //   142: aload 6
    //   144: astore 7
    //   146: aload 9
    //   148: iload_2
    //   149: iload_3
    //   150: iconst_1
    //   151: iadd
    //   152: invokeinterface 88 3 0
    //   157: invokevirtual 91	java/lang/Object:toString	()Ljava/lang/String;
    //   160: checkcast 76	java/lang/CharSequence
    //   163: astore 9
    //   165: aload 6
    //   167: astore 8
    //   169: aload 6
    //   171: astore 7
    //   173: new 93	c/n/k
    //   176: dup
    //   177: ldc 95
    //   179: invokespecial 98	c/n/k:<init>	(Ljava/lang/String;)V
    //   182: aload 9
    //   184: iconst_0
    //   185: invokevirtual 101	c/n/k:a	(Ljava/lang/CharSequence;I)Ljava/util/List;
    //   188: astore 9
    //   190: aload 6
    //   192: astore 8
    //   194: aload 6
    //   196: astore 7
    //   198: aload 9
    //   200: invokeinterface 107 1 0
    //   205: ifne +113 -> 318
    //   208: aload 6
    //   210: astore 8
    //   212: aload 6
    //   214: astore 7
    //   216: aload 9
    //   218: aload 9
    //   220: invokeinterface 110 1 0
    //   225: invokeinterface 114 2 0
    //   230: astore 10
    //   232: aload 6
    //   234: astore 8
    //   236: aload 6
    //   238: astore 7
    //   240: aload 10
    //   242: invokeinterface 119 1 0
    //   247: ifeq +71 -> 318
    //   250: aload 6
    //   252: astore 8
    //   254: aload 6
    //   256: astore 7
    //   258: aload 10
    //   260: invokeinterface 123 1 0
    //   265: checkcast 125	java/lang/String
    //   268: checkcast 76	java/lang/CharSequence
    //   271: invokeinterface 80 1 0
    //   276: ifne +381 -> 657
    //   279: iconst_1
    //   280: istore_2
    //   281: goto +3 -> 284
    //   284: iload_2
    //   285: ifne -53 -> 232
    //   288: aload 6
    //   290: astore 8
    //   292: aload 6
    //   294: astore 7
    //   296: aload 9
    //   298: checkcast 127	java/lang/Iterable
    //   301: aload 10
    //   303: invokeinterface 130 1 0
    //   308: iconst_1
    //   309: iadd
    //   310: invokestatic 136	c/a/m:d	(Ljava/lang/Iterable;I)Ljava/util/List;
    //   313: astore 9
    //   315: goto +19 -> 334
    //   318: aload 6
    //   320: astore 8
    //   322: aload 6
    //   324: astore 7
    //   326: getstatic 141	c/a/y:a	Lc/a/y;
    //   329: checkcast 103	java/util/List
    //   332: astore 9
    //   334: aload 6
    //   336: astore 8
    //   338: aload 6
    //   340: astore 7
    //   342: aload 9
    //   344: checkcast 143	java/util/Collection
    //   347: astore 9
    //   349: aload 9
    //   351: ifnull +192 -> 543
    //   354: aload 6
    //   356: astore 8
    //   358: aload 6
    //   360: astore 7
    //   362: aload 9
    //   364: iconst_0
    //   365: anewarray 125	java/lang/String
    //   368: invokeinterface 147 2 0
    //   373: astore 9
    //   375: aload 9
    //   377: ifnull +148 -> 525
    //   380: aload 6
    //   382: astore 8
    //   384: aload 6
    //   386: astore 7
    //   388: aload 9
    //   390: checkcast 149	[Ljava/lang/String;
    //   393: astore 9
    //   395: aload 6
    //   397: astore 8
    //   399: aload 6
    //   401: astore 7
    //   403: aload 9
    //   405: arraylength
    //   406: iconst_3
    //   407: if_icmpeq +6 -> 413
    //   410: goto -380 -> 30
    //   413: aload 6
    //   415: astore 8
    //   417: aload 6
    //   419: astore 7
    //   421: new 151	android/content/ContentValues
    //   424: dup
    //   425: invokespecial 152	android/content/ContentValues:<init>	()V
    //   428: astore 10
    //   430: aload 6
    //   432: astore 8
    //   434: aload 6
    //   436: astore 7
    //   438: aload 10
    //   440: ldc -102
    //   442: aload 9
    //   444: iconst_0
    //   445: aaload
    //   446: invokevirtual 158	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   449: aload 6
    //   451: astore 8
    //   453: aload 6
    //   455: astore 7
    //   457: aload 10
    //   459: ldc -96
    //   461: aload 9
    //   463: iconst_1
    //   464: aaload
    //   465: invokevirtual 158	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   468: aload 6
    //   470: astore 8
    //   472: aload 6
    //   474: astore 7
    //   476: aload 10
    //   478: ldc -94
    //   480: aload 9
    //   482: iconst_2
    //   483: aaload
    //   484: invokevirtual 158	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   487: aload 6
    //   489: astore 8
    //   491: aload 6
    //   493: astore 7
    //   495: aload_1
    //   496: ldc -92
    //   498: iconst_1
    //   499: aload 10
    //   501: invokeinterface 169 4 0
    //   506: pop2
    //   507: aload 6
    //   509: astore 8
    //   511: aload 6
    //   513: astore 7
    //   515: aload 6
    //   517: invokevirtual 74	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   520: astore 9
    //   522: goto -462 -> 60
    //   525: aload 6
    //   527: astore 8
    //   529: aload 6
    //   531: astore 7
    //   533: new 171	c/u
    //   536: dup
    //   537: ldc -83
    //   539: invokespecial 174	c/u:<init>	(Ljava/lang/String;)V
    //   542: athrow
    //   543: aload 6
    //   545: astore 8
    //   547: aload 6
    //   549: astore 7
    //   551: new 171	c/u
    //   554: dup
    //   555: ldc -80
    //   557: invokespecial 174	c/u:<init>	(Ljava/lang/String;)V
    //   560: athrow
    //   561: aload_0
    //   562: getfield 25	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   565: aload 7
    //   567: checkcast 51	java/io/Closeable
    //   570: invokeinterface 54 2 0
    //   575: return
    //   576: astore_1
    //   577: aload_0
    //   578: getfield 25	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   581: aload 8
    //   583: checkcast 51	java/io/Closeable
    //   586: invokeinterface 54 2 0
    //   591: aload_1
    //   592: athrow
    //   593: astore_1
    //   594: goto -33 -> 561
    //   597: iload_2
    //   598: iload_3
    //   599: if_icmpgt -461 -> 138
    //   602: iload 4
    //   604: ifne +9 -> 613
    //   607: iload_2
    //   608: istore 5
    //   610: goto -500 -> 110
    //   613: iload_3
    //   614: istore 5
    //   616: goto -506 -> 110
    //   619: iconst_0
    //   620: istore 5
    //   622: iload 4
    //   624: ifne +21 -> 645
    //   627: iload 5
    //   629: ifne +9 -> 638
    //   632: iconst_1
    //   633: istore 4
    //   635: goto -38 -> 597
    //   638: iload_2
    //   639: iconst_1
    //   640: iadd
    //   641: istore_2
    //   642: goto -45 -> 597
    //   645: iload 5
    //   647: ifeq -509 -> 138
    //   650: iload_3
    //   651: iconst_1
    //   652: isub
    //   653: istore_3
    //   654: goto -57 -> 597
    //   657: iconst_0
    //   658: istore_2
    //   659: goto -375 -> 284
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	662	0	this	b
    //   0	662	1	paramb	android.arch.persistence.db.b
    //   103	556	2	i	int
    //   101	553	3	j	int
    //   105	529	4	k	int
    //   120	526	5	m	int
    //   23	525	6	localBufferedReader	BufferedReader
    //   7	559	7	localObject1	Object
    //   10	572	8	localObject2	Object
    //   58	463	9	localObject3	Object
    //   230	270	10	localObject4	Object
    // Exception table:
    //   from	to	target	type
    //   12	25	576	finally
    //   53	60	576	finally
    //   77	84	576	finally
    //   92	102	576	finally
    //   118	132	576	finally
    //   146	165	576	finally
    //   173	190	576	finally
    //   198	208	576	finally
    //   216	232	576	finally
    //   240	250	576	finally
    //   258	279	576	finally
    //   296	315	576	finally
    //   326	334	576	finally
    //   342	349	576	finally
    //   362	375	576	finally
    //   388	395	576	finally
    //   403	410	576	finally
    //   421	430	576	finally
    //   438	449	576	finally
    //   457	468	576	finally
    //   476	487	576	finally
    //   495	507	576	finally
    //   515	522	576	finally
    //   533	543	576	finally
    //   551	561	576	finally
    //   12	25	593	java/io/IOException
    //   53	60	593	java/io/IOException
    //   77	84	593	java/io/IOException
    //   92	102	593	java/io/IOException
    //   118	132	593	java/io/IOException
    //   146	165	593	java/io/IOException
    //   173	190	593	java/io/IOException
    //   198	208	593	java/io/IOException
    //   216	232	593	java/io/IOException
    //   240	250	593	java/io/IOException
    //   258	279	593	java/io/IOException
    //   296	315	593	java/io/IOException
    //   326	334	593	java/io/IOException
    //   342	349	593	java/io/IOException
    //   362	375	593	java/io/IOException
    //   388	395	593	java/io/IOException
    //   403	410	593	java/io/IOException
    //   421	430	593	java/io/IOException
    //   438	449	593	java/io/IOException
    //   457	468	593	java/io/IOException
    //   476	487	593	java/io/IOException
    //   495	507	593	java/io/IOException
    //   515	522	593	java/io/IOException
    //   533	543	593	java/io/IOException
    //   551	561	593	java/io/IOException
  }
  
  public final void b(android.arch.persistence.db.b paramb)
  {
    k.b(paramb, "db");
    Object localObject1 = null;
    try
    {
      InputStream localInputStream = b.b("countData.json");
      if (localInputStream == null) {
        return;
      }
      localObject1 = localInputStream;
      Object localObject2 = (KeyWordsList)a.a((Reader)new BufferedReader((Reader)new InputStreamReader(localInputStream, d.a), 8192), KeyWordsList.class);
      if (localObject2 != null)
      {
        localObject1 = localInputStream;
        localObject2 = ((KeyWordsList)localObject2).getKeyWordsList$insights_release();
        if (localObject2 != null)
        {
          localObject1 = localInputStream;
          localObject2 = ((Iterable)localObject2).iterator();
          for (;;)
          {
            localObject1 = localInputStream;
            if (!((Iterator)localObject2).hasNext()) {
              break;
            }
            localObject1 = localInputStream;
            paramb.a("keywordCounts", 1, ((KeywordCounts)((Iterator)localObject2).next()).toContentValue());
          }
        }
      }
      return;
    }
    finally
    {
      b.a((Closeable)localObject1);
    }
  }
  
  /* Error */
  public final void c(android.arch.persistence.db.b paramb)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 66
    //   3: invokestatic 20	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aconst_null
    //   7: astore 6
    //   9: aconst_null
    //   10: astore 7
    //   12: aload_0
    //   13: getfield 25	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   16: ldc -33
    //   18: invokeinterface 181 2 0
    //   23: astore 5
    //   25: aload 5
    //   27: ifnonnull +18 -> 45
    //   30: aload_0
    //   31: getfield 25	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   34: aload 5
    //   36: checkcast 51	java/io/Closeable
    //   39: invokeinterface 54 2 0
    //   44: return
    //   45: aload 5
    //   47: astore 7
    //   49: aload 5
    //   51: astore 6
    //   53: aload_0
    //   54: getfield 25	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   57: aload 5
    //   59: invokeinterface 226 2 0
    //   64: ldc -28
    //   66: ldc -26
    //   68: invokestatic 235	c/n/m:a	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   71: ldc -19
    //   73: ldc -26
    //   75: invokestatic 235	c/n/m:a	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   78: checkcast 76	java/lang/CharSequence
    //   81: astore 8
    //   83: aload 5
    //   85: astore 7
    //   87: aload 5
    //   89: astore 6
    //   91: new 93	c/n/k
    //   94: dup
    //   95: ldc 95
    //   97: invokespecial 98	c/n/k:<init>	(Ljava/lang/String;)V
    //   100: aload 8
    //   102: iconst_0
    //   103: invokevirtual 101	c/n/k:a	(Ljava/lang/CharSequence;I)Ljava/util/List;
    //   106: astore 8
    //   108: aload 5
    //   110: astore 7
    //   112: aload 5
    //   114: astore 6
    //   116: aload 8
    //   118: invokeinterface 107 1 0
    //   123: ifne +113 -> 236
    //   126: aload 5
    //   128: astore 7
    //   130: aload 5
    //   132: astore 6
    //   134: aload 8
    //   136: aload 8
    //   138: invokeinterface 110 1 0
    //   143: invokeinterface 114 2 0
    //   148: astore 9
    //   150: aload 5
    //   152: astore 7
    //   154: aload 5
    //   156: astore 6
    //   158: aload 9
    //   160: invokeinterface 119 1 0
    //   165: ifeq +71 -> 236
    //   168: aload 5
    //   170: astore 7
    //   172: aload 5
    //   174: astore 6
    //   176: aload 9
    //   178: invokeinterface 123 1 0
    //   183: checkcast 125	java/lang/String
    //   186: checkcast 76	java/lang/CharSequence
    //   189: invokeinterface 80 1 0
    //   194: ifne +309 -> 503
    //   197: iconst_1
    //   198: istore_2
    //   199: goto +3 -> 202
    //   202: iload_2
    //   203: ifne -53 -> 150
    //   206: aload 5
    //   208: astore 7
    //   210: aload 5
    //   212: astore 6
    //   214: aload 8
    //   216: checkcast 127	java/lang/Iterable
    //   219: aload 9
    //   221: invokeinterface 130 1 0
    //   226: iconst_1
    //   227: iadd
    //   228: invokestatic 136	c/a/m:d	(Ljava/lang/Iterable;I)Ljava/util/List;
    //   231: astore 8
    //   233: goto +19 -> 252
    //   236: aload 5
    //   238: astore 7
    //   240: aload 5
    //   242: astore 6
    //   244: getstatic 141	c/a/y:a	Lc/a/y;
    //   247: checkcast 103	java/util/List
    //   250: astore 8
    //   252: aload 5
    //   254: astore 7
    //   256: aload 5
    //   258: astore 6
    //   260: aload 8
    //   262: checkcast 143	java/util/Collection
    //   265: astore 8
    //   267: aload 8
    //   269: ifnull +195 -> 464
    //   272: aload 5
    //   274: astore 7
    //   276: aload 5
    //   278: astore 6
    //   280: aload 8
    //   282: iconst_0
    //   283: anewarray 125	java/lang/String
    //   286: invokeinterface 147 2 0
    //   291: astore 8
    //   293: aload 8
    //   295: ifnull +151 -> 446
    //   298: aload 5
    //   300: astore 7
    //   302: aload 5
    //   304: astore 6
    //   306: aload 8
    //   308: arraylength
    //   309: istore 4
    //   311: iconst_0
    //   312: istore_2
    //   313: aload 5
    //   315: astore 6
    //   317: iload_2
    //   318: iload 4
    //   320: if_icmpge +111 -> 431
    //   323: aload 5
    //   325: astore 7
    //   327: aload 5
    //   329: astore 6
    //   331: aload 8
    //   333: iload_2
    //   334: aaload
    //   335: checkcast 125	java/lang/String
    //   338: astore 9
    //   340: aload 5
    //   342: astore 7
    //   344: aload 5
    //   346: astore 6
    //   348: aload 9
    //   350: checkcast 76	java/lang/CharSequence
    //   353: invokeinterface 80 1 0
    //   358: ifle +150 -> 508
    //   361: iconst_1
    //   362: istore_3
    //   363: goto +3 -> 366
    //   366: iload_3
    //   367: ifeq +57 -> 424
    //   370: aload 5
    //   372: astore 7
    //   374: aload 5
    //   376: astore 6
    //   378: new 151	android/content/ContentValues
    //   381: dup
    //   382: invokespecial 152	android/content/ContentValues:<init>	()V
    //   385: astore 10
    //   387: aload 5
    //   389: astore 7
    //   391: aload 5
    //   393: astore 6
    //   395: aload 10
    //   397: ldc -17
    //   399: aload 9
    //   401: invokevirtual 158	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   404: aload 5
    //   406: astore 7
    //   408: aload 5
    //   410: astore 6
    //   412: aload_1
    //   413: ldc -15
    //   415: iconst_1
    //   416: aload 10
    //   418: invokeinterface 169 4 0
    //   423: pop2
    //   424: iload_2
    //   425: iconst_1
    //   426: iadd
    //   427: istore_2
    //   428: goto -115 -> 313
    //   431: aload_0
    //   432: getfield 25	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   435: aload 6
    //   437: checkcast 51	java/io/Closeable
    //   440: invokeinterface 54 2 0
    //   445: return
    //   446: aload 5
    //   448: astore 7
    //   450: aload 5
    //   452: astore 6
    //   454: new 171	c/u
    //   457: dup
    //   458: ldc -83
    //   460: invokespecial 174	c/u:<init>	(Ljava/lang/String;)V
    //   463: athrow
    //   464: aload 5
    //   466: astore 7
    //   468: aload 5
    //   470: astore 6
    //   472: new 171	c/u
    //   475: dup
    //   476: ldc -80
    //   478: invokespecial 174	c/u:<init>	(Ljava/lang/String;)V
    //   481: athrow
    //   482: astore_1
    //   483: aload_0
    //   484: getfield 25	com/truecaller/insights/core/smscategorizer/db/b:b	Lcom/truecaller/insights/d/b;
    //   487: aload 7
    //   489: checkcast 51	java/io/Closeable
    //   492: invokeinterface 54 2 0
    //   497: aload_1
    //   498: athrow
    //   499: astore_1
    //   500: goto -69 -> 431
    //   503: iconst_0
    //   504: istore_2
    //   505: goto -303 -> 202
    //   508: iconst_0
    //   509: istore_3
    //   510: goto -144 -> 366
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	513	0	this	b
    //   0	513	1	paramb	android.arch.persistence.db.b
    //   198	307	2	i	int
    //   362	148	3	j	int
    //   309	12	4	k	int
    //   23	446	5	localInputStream	InputStream
    //   7	464	6	localObject1	Object
    //   10	478	7	localObject2	Object
    //   81	251	8	localObject3	Object
    //   148	252	9	localObject4	Object
    //   385	32	10	localContentValues	android.content.ContentValues
    // Exception table:
    //   from	to	target	type
    //   12	25	482	finally
    //   53	83	482	finally
    //   91	108	482	finally
    //   116	126	482	finally
    //   134	150	482	finally
    //   158	168	482	finally
    //   176	197	482	finally
    //   214	233	482	finally
    //   244	252	482	finally
    //   260	267	482	finally
    //   280	293	482	finally
    //   306	311	482	finally
    //   331	340	482	finally
    //   348	361	482	finally
    //   378	387	482	finally
    //   395	404	482	finally
    //   412	424	482	finally
    //   454	464	482	finally
    //   472	482	482	finally
    //   12	25	499	java/lang/Exception
    //   53	83	499	java/lang/Exception
    //   91	108	499	java/lang/Exception
    //   116	126	499	java/lang/Exception
    //   134	150	499	java/lang/Exception
    //   158	168	499	java/lang/Exception
    //   176	197	499	java/lang/Exception
    //   214	233	499	java/lang/Exception
    //   244	252	499	java/lang/Exception
    //   260	267	499	java/lang/Exception
    //   280	293	499	java/lang/Exception
    //   306	311	499	java/lang/Exception
    //   331	340	499	java/lang/Exception
    //   348	361	499	java/lang/Exception
    //   378	387	499	java/lang/Exception
    //   395	404	499	java/lang/Exception
    //   412	424	499	java/lang/Exception
    //   454	464	499	java/lang/Exception
    //   472	482	499	java/lang/Exception
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.smscategorizer.db.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */