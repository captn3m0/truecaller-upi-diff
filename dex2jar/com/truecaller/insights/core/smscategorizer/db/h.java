package com.truecaller.insights.core.smscategorizer.db;

import c.g.b.k;
import java.util.List;

public final class h
  implements g
{
  private f a;
  private List<MetaData> b;
  private boolean c;
  private final i d;
  
  public h(i parami)
  {
    d = parami;
    a = new f(d.b());
    b = d.a();
    c = true;
  }
  
  private final void e()
  {
    a = new f(d.b());
    b = d.a();
    c = true;
  }
  
  public final int a(int paramInt, String paramString)
  {
    k.b(paramString, "word");
    switch (paramInt)
    {
    default: 
      return 0;
    case 3: 
      return d.e(paramString);
    case 2: 
      return d.b(paramString);
    case 1: 
      return d.c(paramString);
    }
    return d.d(paramString);
  }
  
  public final KeywordCounts a(String paramString)
  {
    k.b(paramString, "word");
    if (!c) {
      e();
    }
    return (KeywordCounts)a.get(paramString);
  }
  
  public final List<String> a()
  {
    return d.d();
  }
  
  public final void a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return;
    case 3: 
      d.a("OTP");
      return;
    case 2: 
      d.a("HAM");
      return;
    case 1: 
      d.a("PAM");
      return;
    }
    d.a("RAN");
  }
  
  public final double b()
  {
    return Double.parseDouble(Integer.toString(d.c()));
  }
  
  public final List<MetaData> c()
  {
    if (!c) {
      e();
    }
    return b;
  }
  
  public final void d()
  {
    c = false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.smscategorizer.db.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */