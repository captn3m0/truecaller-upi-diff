package com.truecaller.insights.core.d;

import android.os.Build.VERSION;
import c.n.m;
import c.u;
import com.truecaller.insights.models.d.b;
import com.truecaller.insights.models.e.b;
import com.truecaller.insights.models.e.c;
import com.truecaller.insights.models.e.d;
import com.truecaller.insights.models.e.e;
import com.truecaller.insights.models.e.f;
import com.truecaller.insights.models.e.g;
import com.truecaller.insights.models.e.h;
import com.truecaller.insights.models.e.i;
import com.truecaller.insights.models.f;
import com.truecaller.insights.models.f.a;
import com.truecaller.insights.models.f.b;
import com.truecaller.insights.models.g.a;
import com.truecaller.log.UnmutedException.InsightsExceptions;
import com.truecaller.log.UnmutedException.InsightsExceptions.Cause;
import com.twelfthmile.malana.compiler.types.MalanaSeed;
import com.twelfthmile.malana.compiler.types.Response;
import com.twelfthmile.malana.controller.Controller;
import java.util.List;
import javax.inject.Inject;

public final class b
  implements a
{
  private final com.truecaller.analytics.b a;
  private final com.truecaller.featuretoggles.e b;
  
  @Inject
  public b(com.truecaller.analytics.b paramb, com.truecaller.featuretoggles.e parame, MalanaSeed paramMalanaSeed)
  {
    a = paramb;
    b = parame;
    if (Build.VERSION.SDK_INT > 19)
    {
      Controller.init(paramMalanaSeed);
      return;
    }
    Controller.init(paramMalanaSeed, true);
  }
  
  private static d.b a(com.truecaller.insights.models.c paramc)
  {
    c.g.b.k.b(paramc, "smsMessage");
    Object localObject1 = com.truecaller.insights.d.d.a;
    localObject1 = a;
    c.g.b.k.b(localObject1, "address");
    Object localObject2 = m.a(m.a((String)localObject1, "-", ""), " ", "");
    localObject1 = com.truecaller.insights.d.d.a;
    localObject1 = (CharSequence)localObject2;
    int i;
    if ((new c.n.k("[A-Za-z0-9]*").a((CharSequence)localObject1)) && (new c.n.k(".*[A-Za-z].*").a((CharSequence)localObject1))) {
      i = 1;
    } else {
      i = 0;
    }
    localObject1 = localObject2;
    if (i != 0)
    {
      localObject1 = localObject2;
      if (((String)localObject2).length() == 8)
      {
        i = ((String)localObject2).length();
        if (localObject2 != null)
        {
          localObject1 = ((String)localObject2).substring(2, i);
          c.g.b.k.a(localObject1, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        }
        else
        {
          throw new u("null cannot be cast to non-null type java.lang.String");
        }
      }
    }
    for (;;)
    {
      try
      {
        localObject2 = Controller.parse(c, (String)localObject1, d, Controller.getGRMs());
        if (localObject2 == null) {
          return new d.b(paramc, (com.truecaller.insights.models.e)e.h.a, (String)localObject1, (f)new f.a((Throwable)new UnmutedException.InsightsExceptions(UnmutedException.InsightsExceptions.Cause.PARSE_FAILURE)));
        }
        String str = ((Response)localObject2).getCategory();
        if (str != null) {}
        switch (str.hashCode())
        {
        case 1240557924: 
          if (str.equals("GRM_BILL")) {
            return new d.b(paramc, (com.truecaller.insights.models.e)e.b.a, (String)localObject1, (f)new f.b((Response)localObject2));
          }
        case 1240550297: 
          if (str.equals("GRM_BANK")) {
            return new d.b(paramc, (com.truecaller.insights.models.e)com.truecaller.insights.models.e.a.a, (String)localObject1, (f)new f.b((Response)localObject2));
          }
        case 1009862158: 
          if (str.equals("GRM_OTP")) {
            return new d.b(paramc, (com.truecaller.insights.models.e)e.g.a, (String)localObject1, (f)new f.b((Response)localObject2));
          }
        case -186141357: 
          if (str.equals("GRM_NOTIF")) {
            return new d.b(paramc, (com.truecaller.insights.models.e)e.e.a, (String)localObject1, (f)new f.b((Response)localObject2));
          }
        case -194258755: 
          if (str.equals("GRM_EVENT")) {
            return new d.b(paramc, (com.truecaller.insights.models.e)e.d.a, (String)localObject1, (f)new f.b((Response)localObject2));
          }
        case -1296211247: 
          if (str.equals("GRM_DELIVERY")) {
            return new d.b(paramc, (com.truecaller.insights.models.e)e.c.a, (String)localObject1, (f)new f.b((Response)localObject2));
          }
        case -1301422793: 
          if (str.equals("GRM_TRAVEL")) {
            return new d.b(paramc, (com.truecaller.insights.models.e)e.i.a, (String)localObject1, (f)new f.b((Response)localObject2));
          }
        case -1455517772: 
          if (str.equals("GRM_OFFERS")) {
            return new d.b(paramc, (com.truecaller.insights.models.e)e.f.a, (String)localObject1, (f)new f.b((Response)localObject2));
          }
          localObject2 = a(paramc, (String)localObject1, (Throwable)new UnmutedException.InsightsExceptions(UnmutedException.InsightsExceptions.Cause.PARSER_UNKNOWN_GRM_EXCEPTION));
          return (d.b)localObject2;
        }
      }
      catch (Exception localException)
      {
        return a(paramc, (String)localObject1, (Throwable)localException);
      }
    }
  }
  
  private static d.b a(com.truecaller.insights.models.c paramc, String paramString, Throwable paramThrowable)
  {
    com.truecaller.insights.b.a.a(paramThrowable.getLocalizedMessage());
    return new d.b(paramc, (com.truecaller.insights.models.e)e.h.a, paramString, (f)new f.a(paramThrowable));
  }
  
  public final d.b a(com.truecaller.insights.database.c paramc, com.truecaller.insights.models.c paramc1)
  {
    c.g.b.k.b(paramc, "receiver$0");
    c.g.b.k.b(paramc1, "smsMessage");
    paramc1 = a(paramc1);
    if (b.Z().a()) {
      a.add(new g.a(paramc1));
    }
    return paramc1;
  }
  
  public final void a(com.truecaller.insights.models.e parame)
  {
    c.g.b.k.b(parame, "response");
    Object localObject = new com.truecaller.analytics.e.a("InsightsParser");
    ((com.truecaller.analytics.e.a)localObject).a("Category", parame.a());
    parame = a;
    localObject = ((com.truecaller.analytics.e.a)localObject).a();
    c.g.b.k.a(localObject, "event.build()");
    parame.b((com.truecaller.analytics.e)localObject);
  }
  
  public final boolean a(com.truecaller.insights.models.d paramd)
  {
    c.g.b.k.b(paramd, "parseResponse");
    paramd = b;
    return (!c.g.b.k.a(paramd, e.h.a)) && (!c.g.b.k.a(paramd, e.f.a));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.core.d.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */