package com.truecaller.insights.models;

import c.a.ag;
import c.g;
import c.l;
import c.u;
import com.truecaller.log.UnmutedException.InsightsExceptions;
import com.truecaller.log.UnmutedException.InsightsExceptions.Cause;
import com.truecaller.log.d;
import com.twelfthmile.malana.compiler.types.Response;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class b
  implements a
{
  private final c.f b;
  private final com.truecaller.insights.core.smscategorizer.db.a c;
  private final com.google.gson.f d;
  
  public b(com.truecaller.insights.core.smscategorizer.db.a parama, com.google.gson.f paramf)
  {
    c = parama;
    d = paramf;
    b = g.a((c.g.a.a)new b.a(this));
  }
  
  public final ParsedDataObject a(d.b paramb)
  {
    c.g.b.k.b(paramb, "smsResponse");
    Object localObject1 = d;
    if (localObject1 != null)
    {
      localObject1 = a.getValMap();
      c.g.b.k.a(localObject1, "(smsResponse.detailedRes…Response).response.valMap");
      localObject1 = ((com.twelfthmile.b.a.a)localObject1).getAll();
      c.g.b.k.a(localObject1, "(smsResponse.detailedRes…onse).response.valMap.all");
      Object localObject3 = (Map)new LinkedHashMap(ag.a(((Map)localObject1).size()));
      Object localObject4 = ((Iterable)((Map)localObject1).entrySet()).iterator();
      while (((Iterator)localObject4).hasNext())
      {
        Map.Entry localEntry = (Map.Entry)((Iterator)localObject4).next();
        localObject1 = a();
        if (localObject1 != null)
        {
          localObject2 = ((PdoBinderType.PdoBinder)localObject1).getBinderByCategory(b).get(localEntry.getKey());
          localObject1 = localObject2;
          if (localObject2 == null) {
            localObject1 = null;
          }
          ((Map)localObject3).put((String)localObject1, localEntry.getValue());
        }
        else
        {
          throw new u("null cannot be cast to non-null type com.truecaller.insights.models.PdoBinderType.PdoBinder");
        }
      }
      localObject1 = new LinkedHashMap();
      Object localObject2 = ((Map)localObject3).entrySet().iterator();
      while (((Iterator)localObject2).hasNext())
      {
        localObject3 = (Map.Entry)((Iterator)localObject2).next();
        localObject4 = (String)((Map.Entry)localObject3).getKey();
        int i;
        if ((localObject4 != null) && (!c.g.b.k.a(localObject4, ""))) {
          i = 1;
        } else {
          i = 0;
        }
        if (i != 0) {
          ((LinkedHashMap)localObject1).put(((Map.Entry)localObject3).getKey(), ((Map.Entry)localObject3).getValue());
        }
      }
      localObject1 = (Map)localObject1;
      localObject2 = d;
      localObject1 = ((com.google.gson.f)localObject2).a(((com.google.gson.f)localObject2).a(localObject1), ParsedDataObject.class);
      localObject1 = com.google.gson.b.k.a(ParsedDataObject.class).cast(localObject1);
      c.g.b.k.a(localObject1, "gson.fromJson(gson.toJso…edDataObject::class.java)");
      localObject1 = (ParsedDataObject)localObject1;
      localObject2 = a;
      ((ParsedDataObject)localObject1).setMessageID(b);
      ((ParsedDataObject)localObject1).setAddress(c);
      ((ParsedDataObject)localObject1).setD(b.a());
      paramb = com.twelfthmile.e.b.a.a().format(d);
      c.g.b.k.a(paramb, "Constants.dateTimeFormat…).format(smsMessage.date)");
      ((ParsedDataObject)localObject1).setMsgdatetime(paramb);
      paramb = com.twelfthmile.e.b.a.b().format(d);
      c.g.b.k.a(paramb, "Constants.dateFormatter().format(smsMessage.date)");
      ((ParsedDataObject)localObject1).setDate(paramb);
      return (ParsedDataObject)localObject1;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.insights.models.SmsDetailedResponseType.SmsDetailedResponse");
  }
  
  public final PdoBinderType a()
  {
    return (PdoBinderType)b.b();
  }
  
  public final e a(ParsedDataObject paramParsedDataObject)
  {
    c.g.b.k.b(paramParsedDataObject, "pdo");
    paramParsedDataObject = paramParsedDataObject.getD();
    switch (paramParsedDataObject.hashCode())
    {
    default: 
      break;
    case 888111124: 
      if (paramParsedDataObject.equals("Delivery")) {
        return (e)e.c.a;
      }
      break;
    case 75456272: 
      if (paramParsedDataObject.equals("Notif")) {
        return (e)e.e.a;
      }
      break;
    case 67338874: 
      if (paramParsedDataObject.equals("Event")) {
        return (e)e.d.a;
      }
      break;
    case 2070567: 
      if (paramParsedDataObject.equals("Bill")) {
        return (e)e.b.a;
      }
      break;
    case 2062940: 
      if (paramParsedDataObject.equals("Bank")) {
        return (e)e.a.a;
      }
      break;
    case 78603: 
      if (paramParsedDataObject.equals("OTP")) {
        return (e)e.g.a;
      }
      break;
    case -1781830854: 
      if (paramParsedDataObject.equals("Travel")) {
        return (e)e.i.a;
      }
      break;
    case -1935925833: 
      if (paramParsedDataObject.equals("Offers")) {
        return (e)e.f.a;
      }
      break;
    }
    return (e)e.h.a;
  }
  
  public final String a(ParsedDataObject paramParsedDataObject, String paramString)
  {
    c.g.b.k.b(paramParsedDataObject, "pdo");
    c.g.b.k.b(paramString, "key");
    Object localObject = a();
    if (localObject != null)
    {
      localObject = (String)((PdoBinderType.PdoBinder)localObject).getBinderByCategoryString(paramParsedDataObject.getD()).get(paramString);
      if (localObject != null) {
        switch (((String)localObject).hashCode())
        {
        default: 
          break;
        case 1793702779: 
          if (((String)localObject).equals("datetime")) {
            return paramParsedDataObject.getDatetime();
          }
          break;
        case 1529956463: 
          if (((String)localObject).equals("dff_val5")) {
            return paramParsedDataObject.getDiffVal5();
          }
          break;
        case 1529956462: 
          if (((String)localObject).equals("dff_val4")) {
            return paramParsedDataObject.getDiffVal4();
          }
          break;
        case 1529956461: 
          if (((String)localObject).equals("dff_val3")) {
            return paramParsedDataObject.getDiffVal3();
          }
          break;
        case 1529956460: 
          if (((String)localObject).equals("dff_val2")) {
            return paramParsedDataObject.getDiffVal2();
          }
          break;
        case 1529956459: 
          if (((String)localObject).equals("dff_val1")) {
            return paramParsedDataObject.getDiffVal1();
          }
          break;
        case 1344204463: 
          if (((String)localObject).equals("msgdate")) {
            return paramParsedDataObject.getMsgdate();
          }
          break;
        case 886067708: 
          if (((String)localObject).equals("msgdatetime")) {
            return paramParsedDataObject.getMsgdatetime();
          }
          break;
        case 3611956: 
          if (((String)localObject).equals("val5")) {
            return paramParsedDataObject.getVal5();
          }
          break;
        case 3611955: 
          if (((String)localObject).equals("val4")) {
            return paramParsedDataObject.getVal4();
          }
          break;
        case 3611954: 
          if (((String)localObject).equals("val3")) {
            return paramParsedDataObject.getVal3();
          }
          break;
        case 3611953: 
          if (((String)localObject).equals("val2")) {
            return paramParsedDataObject.getVal2();
          }
          break;
        case 3611952: 
          if (((String)localObject).equals("val1")) {
            return paramParsedDataObject.getVal1();
          }
          break;
        case 3076014: 
          if (((String)localObject).equals("date")) {
            return paramParsedDataObject.getDate();
          }
          break;
        case 115: 
          if (((String)localObject).equals("s")) {
            return paramParsedDataObject.getS();
          }
          break;
        case 112: 
          if (((String)localObject).equals("p")) {
            return paramParsedDataObject.getP();
          }
          break;
        case 111: 
          if (((String)localObject).equals("o")) {
            return paramParsedDataObject.getO();
          }
          break;
        case 107: 
          if (((String)localObject).equals("k")) {
            return paramParsedDataObject.getK();
          }
          break;
        case 103: 
          if (((String)localObject).equals("g")) {
            return paramParsedDataObject.getG();
          }
          break;
        case 102: 
          if (((String)localObject).equals("f")) {
            return paramParsedDataObject.getF();
          }
          break;
        case 99: 
          if (((String)localObject).equals("c")) {
            return paramParsedDataObject.getC();
          }
          break;
        }
      }
      paramParsedDataObject = "Attempt to unBind an unknown key: ".concat(String.valueOf(paramString));
      d.a((Throwable)new UnmutedException.InsightsExceptions(UnmutedException.InsightsExceptions.Cause.BINDER_EXCEPTION), paramParsedDataObject);
      return "";
    }
    throw new u("null cannot be cast to non-null type com.truecaller.insights.models.PdoBinderType.PdoBinder");
  }
  
  public final SmsBackup b(d.b paramb)
  {
    c.g.b.k.b(paramb, "smsResponse");
    f localf = d;
    if ((localf instanceof f.a)) {
      return new SmsBackup(a, d).a);
    }
    if ((localf instanceof f.b)) {
      return new SmsBackup(a);
    }
    throw new l();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.models.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */