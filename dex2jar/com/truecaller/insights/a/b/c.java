package com.truecaller.insights.a.b;

import com.truecaller.insights.database.InsightsDb;
import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d<com.truecaller.insights.database.a.a>
{
  private final a a;
  private final Provider<InsightsDb> b;
  
  private c(a parama, Provider<InsightsDb> paramProvider)
  {
    a = parama;
    b = paramProvider;
  }
  
  public static c a(a parama, Provider<InsightsDb> paramProvider)
  {
    return new c(parama, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */