package com.truecaller.insights.a.b;

import com.truecaller.insights.c.b;
import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d<com.truecaller.insights.core.c.a>
{
  private final h a;
  private final Provider<b> b;
  private final Provider<com.truecaller.insights.c.a> c;
  private final Provider<com.truecaller.insights.database.c.a> d;
  private final Provider<com.truecaller.insights.models.a> e;
  
  private j(h paramh, Provider<b> paramProvider, Provider<com.truecaller.insights.c.a> paramProvider1, Provider<com.truecaller.insights.database.c.a> paramProvider2, Provider<com.truecaller.insights.models.a> paramProvider3)
  {
    a = paramh;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
  }
  
  public static j a(h paramh, Provider<b> paramProvider, Provider<com.truecaller.insights.c.a> paramProvider1, Provider<com.truecaller.insights.database.c.a> paramProvider2, Provider<com.truecaller.insights.models.a> paramProvider3)
  {
    return new j(paramh, paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */