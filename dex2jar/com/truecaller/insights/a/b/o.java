package com.truecaller.insights.a.b;

import com.google.gson.f;
import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d<com.truecaller.insights.models.a>
{
  private final m a;
  private final Provider<com.truecaller.insights.core.smscategorizer.db.a> b;
  private final Provider<f> c;
  
  private o(m paramm, Provider<com.truecaller.insights.core.smscategorizer.db.a> paramProvider, Provider<f> paramProvider1)
  {
    a = paramm;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static o a(m paramm, Provider<com.truecaller.insights.core.smscategorizer.db.a> paramProvider, Provider<f> paramProvider1)
  {
    return new o(paramm, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */