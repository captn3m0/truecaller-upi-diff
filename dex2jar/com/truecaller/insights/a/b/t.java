package com.truecaller.insights.a.b;

import android.content.Context;
import com.truecaller.insights.d.b;
import dagger.a.d;
import javax.inject.Provider;

public final class t
  implements d<b>
{
  private final r a;
  private final Provider<Context> b;
  
  private t(r paramr, Provider<Context> paramProvider)
  {
    a = paramr;
    b = paramProvider;
  }
  
  public static t a(r paramr, Provider<Context> paramProvider)
  {
    return new t(paramr, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.t
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */