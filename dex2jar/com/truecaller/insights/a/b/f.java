package com.truecaller.insights.a.b;

import com.truecaller.insights.database.InsightsDb;
import com.truecaller.insights.database.a.e;
import dagger.a.d;
import javax.inject.Provider;

public final class f
  implements d<e>
{
  private final a a;
  private final Provider<InsightsDb> b;
  
  private f(a parama, Provider<InsightsDb> paramProvider)
  {
    a = parama;
    b = paramProvider;
  }
  
  public static f a(a parama, Provider<InsightsDb> paramProvider)
  {
    return new f(parama, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */