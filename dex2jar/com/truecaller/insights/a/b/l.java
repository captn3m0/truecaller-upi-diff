package com.truecaller.insights.a.b;

import c.d.f;
import com.truecaller.insights.c.b;
import dagger.a.d;
import javax.inject.Provider;

public final class l
  implements d<b>
{
  private final h a;
  private final Provider<com.truecaller.insights.database.c.a> b;
  private final Provider<com.truecaller.insights.models.a> c;
  private final Provider<f> d;
  
  private l(h paramh, Provider<com.truecaller.insights.database.c.a> paramProvider, Provider<com.truecaller.insights.models.a> paramProvider1, Provider<f> paramProvider2)
  {
    a = paramh;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
  }
  
  public static l a(h paramh, Provider<com.truecaller.insights.database.c.a> paramProvider, Provider<com.truecaller.insights.models.a> paramProvider1, Provider<f> paramProvider2)
  {
    return new l(paramh, paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */