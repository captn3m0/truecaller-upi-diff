package com.truecaller.insights.a.b;

import android.arch.persistence.room.e;
import android.arch.persistence.room.f.a;
import android.arch.persistence.room.f.b;
import android.content.Context;
import c.g.b.k;
import com.truecaller.insights.database.InsightsDb;
import dagger.a.g;
import javax.inject.Provider;

public final class d
  implements dagger.a.d<InsightsDb>
{
  private final a a;
  private final Provider<Context> b;
  private final Provider<com.truecaller.insights.database.a> c;
  
  private d(a parama, Provider<Context> paramProvider, Provider<com.truecaller.insights.database.a> paramProvider1)
  {
    a = parama;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static d a(a parama, Provider<Context> paramProvider, Provider<com.truecaller.insights.database.a> paramProvider1)
  {
    return new d(parama, paramProvider, paramProvider1);
  }
  
  public static InsightsDb a(Context paramContext, com.truecaller.insights.database.a parama)
  {
    k.b(paramContext, "applicationContext");
    k.b(parama, "categorizerDBCallback");
    paramContext = e.a(paramContext, InsightsDb.class, "insights.db").a(new android.arch.persistence.room.a.a[] { (android.arch.persistence.room.a.a)new com.truecaller.insights.database.b.a() }).a((f.b)parama).b();
    k.a(paramContext, "Room\n            .databa…ack)\n            .build()");
    return (InsightsDb)g.a((InsightsDb)paramContext, "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */