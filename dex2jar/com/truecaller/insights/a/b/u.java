package com.truecaller.insights.a.b;

import com.truecaller.insights.core.smscategorizer.db.g;
import com.truecaller.insights.core.smscategorizer.db.i;
import dagger.a.d;
import javax.inject.Provider;

public final class u
  implements d<g>
{
  private final r a;
  private final Provider<i> b;
  
  private u(r paramr, Provider<i> paramProvider)
  {
    a = paramr;
    b = paramProvider;
  }
  
  public static u a(r paramr, Provider<i> paramProvider)
  {
    return new u(paramr, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.u
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */