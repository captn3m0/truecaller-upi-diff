package com.truecaller.insights.a.b;

import com.truecaller.analytics.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.insights.core.d.a;
import com.twelfthmile.malana.compiler.types.MalanaSeed;
import dagger.a.d;
import javax.inject.Provider;

public final class q
  implements d<a>
{
  private final m a;
  private final Provider<e> b;
  private final Provider<b> c;
  private final Provider<MalanaSeed> d;
  
  private q(m paramm, Provider<e> paramProvider, Provider<b> paramProvider1, Provider<MalanaSeed> paramProvider2)
  {
    a = paramm;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
  }
  
  public static q a(m paramm, Provider<e> paramProvider, Provider<b> paramProvider1, Provider<MalanaSeed> paramProvider2)
  {
    return new q(paramm, paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */