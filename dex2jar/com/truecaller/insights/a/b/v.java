package com.truecaller.insights.a.b;

import com.truecaller.insights.core.smscategorizer.a;
import com.truecaller.insights.core.smscategorizer.db.g;
import dagger.a.d;
import javax.inject.Provider;

public final class v
  implements d<a>
{
  private final r a;
  private final Provider<g> b;
  
  private v(r paramr, Provider<g> paramProvider)
  {
    a = paramr;
    b = paramProvider;
  }
  
  public static v a(r paramr, Provider<g> paramProvider)
  {
    return new v(paramr, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.v
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */