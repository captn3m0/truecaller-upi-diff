package com.truecaller.insights.a.b;

import com.truecaller.insights.database.InsightsDb;
import com.truecaller.insights.database.a.c;
import dagger.a.d;
import javax.inject.Provider;

public final class e
  implements d<c>
{
  private final a a;
  private final Provider<InsightsDb> b;
  
  private e(a parama, Provider<InsightsDb> paramProvider)
  {
    a = parama;
    b = paramProvider;
  }
  
  public static e a(a parama, Provider<InsightsDb> paramProvider)
  {
    return new e(parama, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */