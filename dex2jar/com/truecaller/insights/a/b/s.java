package com.truecaller.insights.a.b;

import com.truecaller.insights.core.smscategorizer.db.a;
import javax.inject.Provider;

public final class s
  implements dagger.a.d<com.truecaller.insights.core.smscategorizer.db.d>
{
  private final r a;
  private final Provider<a> b;
  
  private s(r paramr, Provider<a> paramProvider)
  {
    a = paramr;
    b = paramProvider;
  }
  
  public static s a(r paramr, Provider<a> paramProvider)
  {
    return new s(paramr, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.s
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */