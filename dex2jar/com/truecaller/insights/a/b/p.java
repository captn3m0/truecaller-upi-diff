package com.truecaller.insights.a.b;

import c.d.f;
import com.truecaller.insights.d.b;
import com.twelfthmile.malana.compiler.types.MalanaSeed;
import dagger.a.d;
import javax.inject.Provider;

public final class p
  implements d<MalanaSeed>
{
  private final m a;
  private final Provider<b> b;
  private final Provider<f> c;
  
  private p(m paramm, Provider<b> paramProvider, Provider<f> paramProvider1)
  {
    a = paramm;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static p a(m paramm, Provider<b> paramProvider, Provider<f> paramProvider1)
  {
    return new p(paramm, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */