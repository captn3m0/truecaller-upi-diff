package com.truecaller.insights.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class i
  implements d<com.truecaller.insights.c.a>
{
  private final h a;
  private final Provider<com.truecaller.insights.database.c.a> b;
  
  private i(h paramh, Provider<com.truecaller.insights.database.c.a> paramProvider)
  {
    a = paramh;
    b = paramProvider;
  }
  
  public static i a(h paramh, Provider<com.truecaller.insights.database.c.a> paramProvider)
  {
    return new i(paramh, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */