package com.truecaller.insights.a.b;

import android.content.Context;
import com.truecaller.insights.core.smscategorizer.db.b;
import com.truecaller.insights.core.smscategorizer.db.i;
import javax.inject.Provider;

public final class w
  implements dagger.a.d<i>
{
  private final r a;
  private final Provider<Context> b;
  private final Provider<com.truecaller.insights.core.smscategorizer.db.d> c;
  private final Provider<b> d;
  
  private w(r paramr, Provider<Context> paramProvider, Provider<com.truecaller.insights.core.smscategorizer.db.d> paramProvider1, Provider<b> paramProvider2)
  {
    a = paramr;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
  }
  
  public static w a(r paramr, Provider<Context> paramProvider, Provider<com.truecaller.insights.core.smscategorizer.db.d> paramProvider1, Provider<b> paramProvider2)
  {
    return new w(paramr, paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.w
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */