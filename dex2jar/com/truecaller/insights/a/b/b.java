package com.truecaller.insights.a.b;

import c.d.f;
import c.g.b.k;
import com.truecaller.insights.database.InsightsDb;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class b
  implements d<com.truecaller.insights.core.b.a>
{
  private final Provider<InsightsDb> a;
  private final Provider<com.truecaller.insights.models.a> b;
  private final Provider<f> c;
  
  public static com.truecaller.insights.core.b.a a(InsightsDb paramInsightsDb, com.truecaller.insights.models.a parama, f paramf)
  {
    k.b(paramInsightsDb, "db");
    k.b(parama, "insightsBinder");
    k.b(paramf, "coroutineContext");
    return (com.truecaller.insights.core.b.a)g.a((com.truecaller.insights.core.b.a)new com.truecaller.insights.core.b.b(paramInsightsDb, parama, paramf), "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */