package com.truecaller.insights.a.b;

import c.d.f;
import com.truecaller.insights.database.a.c;
import com.truecaller.insights.database.a.e;
import dagger.a.d;
import javax.inject.Provider;

public final class k
  implements d<com.truecaller.insights.database.c.a>
{
  private final h a;
  private final Provider<c> b;
  private final Provider<e> c;
  private final Provider<com.truecaller.insights.database.a.a> d;
  private final Provider<f> e;
  
  private k(h paramh, Provider<c> paramProvider, Provider<e> paramProvider1, Provider<com.truecaller.insights.database.a.a> paramProvider2, Provider<f> paramProvider3)
  {
    a = paramh;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
  }
  
  public static k a(h paramh, Provider<c> paramProvider, Provider<e> paramProvider1, Provider<com.truecaller.insights.database.a.a> paramProvider2, Provider<f> paramProvider3)
  {
    return new k(paramh, paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.b.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */