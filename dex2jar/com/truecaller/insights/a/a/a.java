package com.truecaller.insights.a.a;

import android.content.Context;
import com.truecaller.insights.a.b.h;
import com.truecaller.insights.a.b.j;
import com.truecaller.insights.a.b.k;
import com.truecaller.insights.a.b.l;
import com.truecaller.insights.a.b.m;
import com.truecaller.insights.a.b.n;
import com.truecaller.insights.a.b.o;
import com.truecaller.insights.a.b.p;
import com.truecaller.insights.a.b.q;
import com.truecaller.insights.a.b.r;
import com.truecaller.insights.a.b.s;
import com.truecaller.insights.a.b.t;
import com.truecaller.insights.a.b.u;
import com.truecaller.insights.a.b.v;
import com.truecaller.insights.a.b.w;
import com.truecaller.insights.database.InsightsDb;
import com.twelfthmile.malana.compiler.types.MalanaSeed;
import javax.inject.Provider;

public final class a
  implements b
{
  private final com.truecaller.analytics.d a;
  private final com.truecaller.common.a b;
  private final com.truecaller.insights.a.b.a c;
  private Provider<com.truecaller.featuretoggles.e> d;
  private Provider<com.truecaller.analytics.b> e;
  private Provider<Context> f;
  private Provider<com.truecaller.insights.d.b> g;
  private Provider<c.d.f> h;
  private Provider<MalanaSeed> i;
  private Provider<com.truecaller.insights.core.d.a> j;
  private Provider<com.truecaller.insights.core.smscategorizer.db.b> k;
  private Provider<com.truecaller.insights.core.smscategorizer.db.d> l;
  private Provider<com.truecaller.insights.core.smscategorizer.db.i> m;
  private Provider<com.truecaller.insights.core.smscategorizer.db.g> n;
  private Provider<com.truecaller.insights.core.smscategorizer.a> o;
  private Provider<com.google.gson.f> p;
  private Provider<com.truecaller.insights.models.a> q;
  private Provider<InsightsDb> r;
  private Provider<com.truecaller.insights.database.a.c> s;
  private Provider<com.truecaller.insights.database.a.e> t;
  private Provider<com.truecaller.insights.database.a.a> u;
  private Provider<com.truecaller.insights.database.c.a> v;
  private Provider<com.truecaller.insights.c.b> w;
  private Provider<com.truecaller.insights.c.a> x;
  private Provider<com.truecaller.insights.core.c.a> y;
  
  private a(m paramm, r paramr, com.truecaller.insights.a.b.a parama, h paramh, com.truecaller.analytics.d paramd, com.truecaller.common.a parama1)
  {
    a = paramd;
    b = parama1;
    c = parama;
    d = new d(parama1);
    e = new b(paramd);
    f = new c(parama1);
    g = t.a(paramr, f);
    h = new e(parama1);
    i = dagger.a.c.a(p.a(paramm, g, h));
    j = dagger.a.c.a(q.a(paramm, d, e, i));
    k = com.truecaller.insights.core.smscategorizer.db.c.a(g);
    l = s.a(paramr, k);
    m = w.a(paramr, f, l, k);
    n = u.a(paramr, m);
    o = v.a(paramr, n);
    p = dagger.a.c.a(n.a(paramm));
    q = dagger.a.c.a(o.a(paramm, k, p));
    r = com.truecaller.insights.a.b.d.a(parama, f, com.truecaller.insights.database.b.a());
    s = com.truecaller.insights.a.b.e.a(parama, r);
    t = com.truecaller.insights.a.b.f.a(parama, r);
    u = com.truecaller.insights.a.b.c.a(parama, r);
    v = k.a(paramh, s, t, u, h);
    w = l.a(paramh, v, q, h);
    x = com.truecaller.insights.a.b.i.a(paramh, v);
    y = dagger.a.c.a(j.a(paramh, w, x, v, q));
  }
  
  public static b.a a()
  {
    return new a((byte)0);
  }
  
  public final com.truecaller.insights.core.d.a b()
  {
    return (com.truecaller.insights.core.d.a)j.get();
  }
  
  public final com.truecaller.insights.core.smscategorizer.d c()
  {
    com.truecaller.insights.core.smscategorizer.e locale = com.truecaller.insights.core.smscategorizer.f.a((com.truecaller.analytics.b)dagger.a.g.a(a.c(), "Cannot return null from a non-@Nullable component method"), (com.truecaller.androidactors.f)dagger.a.g.a(a.f(), "Cannot return null from a non-@Nullable component method"));
    a = dagger.a.c.b(o);
    return locale;
  }
  
  public final com.truecaller.insights.core.b.c d()
  {
    return com.truecaller.insights.a.b.g.a(com.truecaller.insights.a.b.b.a(com.truecaller.insights.a.b.d.a((Context)dagger.a.g.a(b.b(), "Cannot return null from a non-@Nullable component method"), new com.truecaller.insights.database.a()), (com.truecaller.insights.models.a)q.get(), (c.d.f)dagger.a.g.a(b.t(), "Cannot return null from a non-@Nullable component method")));
  }
  
  public final com.truecaller.insights.core.c.a e()
  {
    return (com.truecaller.insights.core.c.a)y.get();
  }
  
  static final class a
    implements b.a
  {
    private m a;
    private r b;
    private com.truecaller.insights.a.b.a c;
    private h d;
    private com.truecaller.analytics.d e;
    private com.truecaller.common.a f;
    
    public final b a()
    {
      if (a == null) {
        a = new m();
      }
      if (b == null) {
        b = new r();
      }
      if (c == null) {
        c = new com.truecaller.insights.a.b.a();
      }
      if (d == null) {
        d = new h();
      }
      dagger.a.g.a(e, com.truecaller.analytics.d.class);
      dagger.a.g.a(f, com.truecaller.common.a.class);
      return new a(a, b, c, d, e, f, (byte)0);
    }
  }
  
  static final class b
    implements Provider<com.truecaller.analytics.b>
  {
    private final com.truecaller.analytics.d a;
    
    b(com.truecaller.analytics.d paramd)
    {
      a = paramd;
    }
  }
  
  static final class c
    implements Provider<Context>
  {
    private final com.truecaller.common.a a;
    
    c(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
  
  static final class d
    implements Provider<com.truecaller.featuretoggles.e>
  {
    private final com.truecaller.common.a a;
    
    d(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
  
  static final class e
    implements Provider<c.d.f>
  {
    private final com.truecaller.common.a a;
    
    e(com.truecaller.common.a parama)
    {
      a = parama;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.a.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */