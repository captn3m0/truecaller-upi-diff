package com.truecaller.insights.c;

import c.g.a.m;
import c.g.b.k;
import com.truecaller.insights.models.ParsedDataObject;
import com.twelfthmile.c.a.c;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.inject.Named;

public final class b
  extends c
{
  final com.truecaller.insights.database.c.a a;
  final com.truecaller.insights.models.a b;
  final c.d.f c;
  
  public b(com.truecaller.insights.database.c.a parama, com.truecaller.insights.models.a parama1, @Named("IO") c.d.f paramf)
  {
    a = parama;
    b = parama1;
    c = paramf;
  }
  
  static ParsedDataObject a(ParsedDataObject paramParsedDataObject, List<ParsedDataObject> paramList)
  {
    ParsedDataObject localParsedDataObject1 = new ParsedDataObject();
    a(localParsedDataObject1, paramParsedDataObject);
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      ParsedDataObject localParsedDataObject2 = (ParsedDataObject)localIterator.next();
      a(localParsedDataObject1, localParsedDataObject2);
      if (k.a(paramParsedDataObject.getState(), "SELF_TRANSFER")) {
        paramList = "MERGED_SELF_TRANSFER";
      } else {
        paramList = "MERGED";
      }
      paramParsedDataObject.setState(paramList);
      if (k.a(localParsedDataObject2.getState(), "SELF_TRANSFER")) {
        paramList = "MERGED_SELF_TRANSFER";
      } else {
        paramList = "MERGED";
      }
      localParsedDataObject2.setState(paramList);
      if ((!k.a(paramParsedDataObject.getState(), "SELF_TRANSFER")) && (!k.a(paramParsedDataObject.getState(), "MERGED_SELF_TRANSFER"))) {
        paramList = "SYNTHETIC";
      } else {
        paramList = "SYNTHETIC_SELF_TRANSFER";
      }
      localParsedDataObject1.setState(paramList);
    }
    return localParsedDataObject1;
  }
  
  private static void a(ParsedDataObject paramParsedDataObject1, ParsedDataObject paramParsedDataObject2)
  {
    int i = ((CharSequence)paramParsedDataObject2.getD()).length();
    int j = 1;
    if (i > 0) {
      i = 1;
    } else {
      i = 0;
    }
    String str;
    if (i != 0) {
      str = paramParsedDataObject2.getD();
    } else {
      str = paramParsedDataObject1.getD();
    }
    paramParsedDataObject1.setD(str);
    if (((CharSequence)paramParsedDataObject2.getK()).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      str = paramParsedDataObject2.getK();
    } else {
      str = paramParsedDataObject1.getK();
    }
    paramParsedDataObject1.setK(str);
    if (((CharSequence)paramParsedDataObject2.getP()).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      str = paramParsedDataObject2.getP();
    } else {
      str = paramParsedDataObject1.getP();
    }
    paramParsedDataObject1.setP(str);
    if (((CharSequence)paramParsedDataObject2.getC()).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      str = paramParsedDataObject2.getC();
    } else {
      str = paramParsedDataObject1.getD();
    }
    paramParsedDataObject1.setC(str);
    if (((CharSequence)paramParsedDataObject2.getO()).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      str = paramParsedDataObject2.getO();
    } else {
      str = paramParsedDataObject1.getO();
    }
    paramParsedDataObject1.setO(str);
    if (((CharSequence)paramParsedDataObject2.getF()).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      str = paramParsedDataObject2.getF();
    } else {
      str = paramParsedDataObject1.getF();
    }
    paramParsedDataObject1.setF(str);
    if (((CharSequence)paramParsedDataObject2.getG()).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      str = paramParsedDataObject2.getG();
    } else {
      str = paramParsedDataObject1.getG();
    }
    paramParsedDataObject1.setG(str);
    if (((CharSequence)paramParsedDataObject2.getS()).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      str = paramParsedDataObject2.getS();
    } else {
      str = paramParsedDataObject1.getS();
    }
    paramParsedDataObject1.setS(str);
    if (((CharSequence)paramParsedDataObject2.getVal1()).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      str = paramParsedDataObject2.getVal1();
    } else {
      str = paramParsedDataObject1.getVal1();
    }
    paramParsedDataObject1.setVal1(str);
    if (((CharSequence)paramParsedDataObject2.getVal2()).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      str = paramParsedDataObject2.getVal2();
    } else {
      str = paramParsedDataObject1.getVal2();
    }
    paramParsedDataObject1.setVal2(str);
    if (((CharSequence)paramParsedDataObject2.getVal3()).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      str = paramParsedDataObject2.getVal3();
    } else {
      str = paramParsedDataObject1.getVal3();
    }
    paramParsedDataObject1.setVal3(str);
    if (((CharSequence)paramParsedDataObject2.getVal4()).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      str = paramParsedDataObject2.getVal4();
    } else {
      str = paramParsedDataObject1.getVal4();
    }
    paramParsedDataObject1.setVal4(str);
    if (((CharSequence)paramParsedDataObject2.getVal5()).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      str = paramParsedDataObject2.getVal5();
    } else {
      str = paramParsedDataObject1.getVal5();
    }
    paramParsedDataObject1.setVal5(str);
    if (((CharSequence)paramParsedDataObject2.getDatetime()).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      str = paramParsedDataObject2.getDatetime();
    } else {
      str = paramParsedDataObject1.getDatetime();
    }
    paramParsedDataObject1.setDatetime(str);
    if (((CharSequence)paramParsedDataObject2.getAddress()).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      str = paramParsedDataObject2.getAddress();
    } else {
      str = paramParsedDataObject1.getAddress();
    }
    paramParsedDataObject1.setAddress(str);
    if (((CharSequence)paramParsedDataObject2.getMsgdatetime()).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      str = paramParsedDataObject2.getMsgdatetime();
    } else {
      str = paramParsedDataObject1.getMsgdatetime();
    }
    paramParsedDataObject1.setMsgdatetime(str);
    if (((CharSequence)paramParsedDataObject2.getDate()).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      str = paramParsedDataObject2.getDate();
    } else {
      str = paramParsedDataObject1.getDate();
    }
    paramParsedDataObject1.setDate(str);
    if (((CharSequence)paramParsedDataObject2.getMsgdate()).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      str = paramParsedDataObject2.getMsgdate();
    } else {
      str = paramParsedDataObject1.getMsgdate();
    }
    paramParsedDataObject1.setMsgdate(str);
    if (((CharSequence)paramParsedDataObject2.getDiffVal1()).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      str = paramParsedDataObject2.getDiffVal1();
    } else {
      str = paramParsedDataObject1.getDiffVal1();
    }
    paramParsedDataObject1.setDiffVal1(str);
    if (((CharSequence)paramParsedDataObject2.getDiffVal2()).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      str = paramParsedDataObject2.getDiffVal2();
    } else {
      str = paramParsedDataObject1.getDiffVal2();
    }
    paramParsedDataObject1.setDiffVal2(str);
    if (((CharSequence)paramParsedDataObject2.getDiffVal3()).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      str = paramParsedDataObject2.getDiffVal3();
    } else {
      str = paramParsedDataObject1.getDiffVal3();
    }
    paramParsedDataObject1.setDiffVal3(str);
    if (((CharSequence)paramParsedDataObject2.getDiffVal4()).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      str = paramParsedDataObject2.getDiffVal4();
    } else {
      str = paramParsedDataObject1.getDiffVal4();
    }
    paramParsedDataObject1.setDiffVal4(str);
    if (((CharSequence)paramParsedDataObject2.getDiffVal5()).length() > 0) {
      i = j;
    } else {
      i = 0;
    }
    if (i != 0) {
      paramParsedDataObject2 = paramParsedDataObject2.getDiffVal5();
    } else {
      paramParsedDataObject2 = paramParsedDataObject1.getDiffVal5();
    }
    paramParsedDataObject1.setDiffVal5(paramParsedDataObject2);
  }
  
  static void b(ParsedDataObject paramParsedDataObject, List<ParsedDataObject> paramList)
  {
    paramParsedDataObject.setState("SELF_TRANSFER");
    paramParsedDataObject = ((Iterable)paramList).iterator();
    while (paramParsedDataObject.hasNext()) {
      ((ParsedDataObject)paramParsedDataObject.next()).setState("SELF_TRANSFER");
    }
  }
  
  public final void a(HashMap<Long, com.twelfthmile.c.b.b.b> paramHashMap)
  {
    if (paramHashMap != null)
    {
      List localList = (List)new ArrayList();
      paramHashMap = ((Map)paramHashMap).entrySet().iterator();
      while (paramHashMap.hasNext())
      {
        Object localObject1 = (Map.Entry)paramHashMap.next();
        long l = ((Number)((Map.Entry)localObject1).getKey()).longValue();
        localObject1 = (com.twelfthmile.c.b.b.b)((Map.Entry)localObject1).getValue();
        Object localObject2 = ((com.twelfthmile.c.b.b.b)localObject1).a();
        k.a(localObject2, "childrenIds.linkingIds");
        if ((((Collection)localObject2).isEmpty() ^ true))
        {
          localObject2 = (Long)((com.twelfthmile.c.b.b.b)localObject1).a().get(0);
          if ((localObject2 != null) && (l == ((Long)localObject2).longValue())) {
            ((com.twelfthmile.c.b.b.b)localObject1).a().remove(0);
          }
        }
        kotlinx.coroutines.f.a(c, (m)new b.a(this, l, (com.twelfthmile.c.b.b.b)localObject1, localList, null));
      }
      kotlinx.coroutines.f.a(c, (m)new b.b(this, localList, null));
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.c.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */