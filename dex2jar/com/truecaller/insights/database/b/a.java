package com.truecaller.insights.database.b;

import android.arch.persistence.db.b;
import c.g.b.k;

public final class a
  extends android.arch.persistence.room.a.a
{
  public a()
  {
    super(1, 2);
  }
  
  public final void a(b paramb)
  {
    k.b(paramb, "database");
    paramb.c("ALTER TABLE parsed_data_object_table ADD COLUMN state TEXT NOT NULL DEFAULT '' ");
    paramb.c("CREATE TABLE link_prune_table (parent_id INTEGER NOT NULL, child_id INTEGER NOT NULL, link_type TEXT NOT NULL, created_at INTEGER NOT NULL, PRIMARY KEY (parent_id, child_id), FOREIGN KEY (parent_id) REFERENCES parsed_data_object_table (id), FOREIGN KEY (child_id) REFERENCES parsed_data_object_table (id))");
    paramb.c("CREATE INDEX IF NOT EXISTS index_link_prune_table_parent_id ON link_prune_table (parent_id)");
    paramb.c("CREATE INDEX IF NOT EXISTS index_link_prune_table_child_id ON link_prune_table (child_id)");
    paramb.c("CREATE TABLE states_table (owner TEXT NOT NULL, last_updated_at INTEGER NOT NULL, last_updated_data TEXT, created_at INTEGER NOT NULL, PRIMARY KEY (owner))");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.database.b.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */