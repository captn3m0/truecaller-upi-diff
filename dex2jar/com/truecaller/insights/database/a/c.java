package com.truecaller.insights.database.a;

import com.truecaller.insights.models.LinkPruneMap;
import com.truecaller.insights.models.ParsedDataObject;
import com.truecaller.insights.models.SmsBackup;
import java.util.Date;
import java.util.List;
import java.util.Set;

public abstract interface c
{
  public abstract ParsedDataObject a(long paramLong);
  
  public abstract List<ParsedDataObject> a(Date paramDate);
  
  public abstract List<ParsedDataObject> a(Date paramDate, List<String> paramList);
  
  public abstract void a(List<ParsedDataObject> paramList);
  
  public abstract void a(Set<Long> paramSet);
  
  public abstract List<ParsedDataObject> b(Set<Long> paramSet);
  
  public abstract void b(List<LinkPruneMap> paramList);
  
  public abstract void c(List<SmsBackup> paramList);
}

/* Location:
 * Qualified Name:     com.truecaller.insights.database.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */