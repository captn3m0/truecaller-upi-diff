package com.truecaller.insights.database.c;

import c.d.c;
import c.x;
import com.truecaller.insights.models.InsightState;
import com.truecaller.insights.models.ParsedDataObject;
import java.util.Date;
import java.util.List;

public abstract interface a
{
  public abstract Object a(long paramLong, c<? super ParsedDataObject> paramc);
  
  public abstract Object a(InsightState paramInsightState, Date paramDate, String paramString, c<? super x> paramc);
  
  public abstract Object a(ParsedDataObject paramParsedDataObject, List<ParsedDataObject> paramList, c<? super x> paramc);
  
  public abstract Object a(String paramString, c<? super InsightState> paramc);
  
  public abstract Object a(Date paramDate, c<? super List<ParsedDataObject>> paramc);
  
  public abstract Object a(List<Long> paramList, c<? super List<ParsedDataObject>> paramc);
  
  public abstract Object b(Date paramDate, c<? super List<ParsedDataObject>> paramc);
  
  public abstract Object b(List<ParsedDataObject> paramList, c<? super x> paramc);
  
  public abstract Object c(List<Long> paramList, c<? super x> paramc);
}

/* Location:
 * Qualified Name:     com.truecaller.insights.database.c.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */