package com.truecaller.insights.database.c;

import c.d.f;
import c.g.a.m;
import c.x;
import com.truecaller.insights.database.a.e;
import com.truecaller.insights.models.InsightState;
import com.truecaller.insights.models.ParsedDataObject;
import java.util.Date;
import java.util.List;
import javax.inject.Named;
import kotlinx.coroutines.g;

public final class b
  implements a
{
  final com.truecaller.insights.database.a.c a;
  final e b;
  private final com.truecaller.insights.database.a.a c;
  private final f d;
  
  public b(com.truecaller.insights.database.a.c paramc, e parame, com.truecaller.insights.database.a.a parama, @Named("IO") f paramf)
  {
    a = paramc;
    b = parame;
    c = parama;
    d = paramf;
  }
  
  public final Object a(long paramLong, c.d.c<? super ParsedDataObject> paramc)
  {
    return g.a(d, (m)new b.c(this, paramLong, null), paramc);
  }
  
  public final Object a(InsightState paramInsightState, Date paramDate, String paramString, c.d.c<? super x> paramc)
  {
    return g.a(d, (m)new b.i(this, paramInsightState, paramString, paramDate, null), paramc);
  }
  
  public final Object a(ParsedDataObject paramParsedDataObject, List<ParsedDataObject> paramList, c.d.c<? super x> paramc)
  {
    return g.a(d, (m)new b.h(this, paramList, paramParsedDataObject, null), paramc);
  }
  
  public final Object a(String paramString, c.d.c<? super InsightState> paramc)
  {
    return g.a(d, (m)new b.b(this, paramString, null), paramc);
  }
  
  public final Object a(Date paramDate, c.d.c<? super List<ParsedDataObject>> paramc)
  {
    return g.a(d, (m)new b.e(this, paramDate, null), paramc);
  }
  
  public final Object a(List<Long> paramList, c.d.c<? super List<ParsedDataObject>> paramc)
  {
    return g.a(d, (m)new b.d(this, paramList, null), paramc);
  }
  
  public final Object b(Date paramDate, c.d.c<? super List<ParsedDataObject>> paramc)
  {
    return g.a(d, (m)new b.f(this, paramDate, null), paramc);
  }
  
  public final Object b(List<ParsedDataObject> paramList, c.d.c<? super x> paramc)
  {
    return g.a(d, (m)new b.g(this, paramList, null), paramc);
  }
  
  public final Object c(List<Long> paramList, c.d.c<? super x> paramc)
  {
    return g.a(d, (m)new b.a(this, paramList, null), paramc);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.insights.database.c.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */