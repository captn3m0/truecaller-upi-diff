package com.truecaller.debug.log;

import android.content.Context;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import c.x;
import com.truecaller.utils.extensions.j;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;

public final class a
{
  public static final a a = new a();
  private static final SimpleDateFormat b = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US);
  private static String c;
  private static b d;
  
  public static final ao<x> a(Context paramContext)
  {
    k.b(paramContext, "context");
    return e.a((ag)bg.a, (f)j.a(), (m)new a.a(paramContext, null), 2);
  }
  
  public static final void a(Context paramContext, String paramString)
  {
    k.b(paramContext, "context");
    k.b(paramString, "fileProviderAuthority");
    c = paramString;
    paramString = new b();
    paramString.a(paramContext.getApplicationContext(), "debug.log");
    d = paramString;
  }
  
  public static final void a(Object... paramVarArgs)
  {
    k.b(paramVarArgs, "events");
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(b.format(new Date()));
    localStringBuilder.append(": ");
    localStringBuilder = new StringBuilder(localStringBuilder.toString());
    int j = paramVarArgs.length;
    int i = 0;
    while (i < j)
    {
      localStringBuilder.append(paramVarArgs[i]);
      i += 1;
    }
    paramVarArgs = d;
    if (paramVarArgs == null) {
      k.a("logger");
    }
    paramVarArgs.a(localStringBuilder.toString());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.debug.log.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */