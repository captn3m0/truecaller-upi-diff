package com.truecaller.feature_toggles;

import android.content.Context;
import com.truecaller.featuretoggles.a;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.p;
import com.truecaller.util.al;
import javax.inject.Provider;

public final class b
  implements dagger.a.d<e>
{
  private final Provider<a> a;
  private final Provider<Context> b;
  private final Provider<al> c;
  private final Provider<com.truecaller.featuretoggles.d> d;
  private final Provider<p> e;
  
  private b(Provider<a> paramProvider, Provider<Context> paramProvider1, Provider<al> paramProvider2, Provider<com.truecaller.featuretoggles.d> paramProvider3, Provider<p> paramProvider4)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static b a(Provider<a> paramProvider, Provider<Context> paramProvider1, Provider<al> paramProvider2, Provider<com.truecaller.featuretoggles.d> paramProvider3, Provider<p> paramProvider4)
  {
    return new b(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */