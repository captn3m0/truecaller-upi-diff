package com.truecaller.feature_toggles;

import com.truecaller.credit.c;
import com.truecaller.messaging.h.f;
import javax.inject.Provider;

public final class a
  implements dagger.a.d<com.truecaller.featuretoggles.d>
{
  private final Provider<com.truecaller.payments.h> a;
  private final Provider<com.truecaller.whoviewedme.h> b;
  private final Provider<com.truecaller.messaging.categorizer.a.d> c;
  private final Provider<f> d;
  private final Provider<c> e;
  private final Provider<com.truecaller.engagementrewards.a> f;
  
  private a(Provider<com.truecaller.payments.h> paramProvider, Provider<com.truecaller.whoviewedme.h> paramProvider1, Provider<com.truecaller.messaging.categorizer.a.d> paramProvider2, Provider<f> paramProvider3, Provider<c> paramProvider4, Provider<com.truecaller.engagementrewards.a> paramProvider5)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
  }
  
  public static a a(Provider<com.truecaller.payments.h> paramProvider, Provider<com.truecaller.whoviewedme.h> paramProvider1, Provider<com.truecaller.messaging.categorizer.a.d> paramProvider2, Provider<f> paramProvider3, Provider<c> paramProvider4, Provider<com.truecaller.engagementrewards.a> paramProvider5)
  {
    return new a(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */