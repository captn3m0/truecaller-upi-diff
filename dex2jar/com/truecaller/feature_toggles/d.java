package com.truecaller.feature_toggles;

import javax.inject.Provider;

public final class d
  implements dagger.a.d<c>
{
  private final Provider<com.truecaller.utils.d> a;
  
  private d(Provider<com.truecaller.utils.d> paramProvider)
  {
    a = paramProvider;
  }
  
  public static d a(Provider<com.truecaller.utils.d> paramProvider)
  {
    return new d(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.feature_toggles.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */