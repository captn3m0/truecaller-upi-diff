package com.truecaller;

import android.content.Context;
import com.truecaller.util.d.a;
import dagger.a.d;
import javax.inject.Provider;

public final class aq
  implements d<a>
{
  private final c a;
  private final Provider<Context> b;
  
  private aq(c paramc, Provider<Context> paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static aq a(c paramc, Provider<Context> paramProvider)
  {
    return new aq(paramc, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.aq
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */