package com.truecaller.old.data.access;

import android.text.TextUtils;
import com.truecaller.common.e.c;
import com.truecaller.common.e.e;
import com.truecaller.old.data.entity.b;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class d
{
  private static volatile b a;
  private static volatile List<b> b;
  private static volatile List<b> c;
  private static volatile Locale d;
  private static final e e = new e();
  private static final com.truecaller.common.e.d f = com.truecaller.common.e.d.a;
  
  public static b a(String paramString)
  {
    Iterator localIterator = a(false).iterator();
    while (localIterator.hasNext())
    {
      b localb = (b)localIterator.next();
      if (a.b.equalsIgnoreCase(paramString)) {
        return localb;
      }
    }
    return a;
  }
  
  public static b a(Locale paramLocale)
  {
    Object localObject2 = a(false);
    if (paramLocale != null)
    {
      String str = paramLocale.getLanguage();
      Object localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append(str);
      ((StringBuilder)localObject1).append("_");
      ((StringBuilder)localObject1).append(paramLocale.getCountry());
      localObject1 = ((StringBuilder)localObject1).toString();
      Object localObject3 = new StringBuilder();
      ((StringBuilder)localObject3).append(str);
      ((StringBuilder)localObject3).append("_");
      ((StringBuilder)localObject3).append(paramLocale.getVariant());
      localObject3 = ((StringBuilder)localObject3).toString();
      paramLocale = paramLocale.toString();
      localObject2 = ((List)localObject2).iterator();
      while (((Iterator)localObject2).hasNext())
      {
        b localb = (b)((Iterator)localObject2).next();
        if ((a.b.equalsIgnoreCase(str)) || (a.b.equalsIgnoreCase((String)localObject1)) || (a.b.equalsIgnoreCase((String)localObject3)) || (a.b.equalsIgnoreCase(paramLocale))) {
          return localb;
        }
      }
    }
    return a;
  }
  
  public static List<b> a()
  {
    return a(false);
  }
  
  /* Error */
  private static List<b> a(boolean paramBoolean)
  {
    // Byte code:
    //   0: iload_0
    //   1: ifeq +10 -> 11
    //   4: getstatic 104	com/truecaller/old/data/access/d:c	Ljava/util/List;
    //   7: astore_1
    //   8: goto +7 -> 15
    //   11: getstatic 106	com/truecaller/old/data/access/d:b	Ljava/util/List;
    //   14: astore_1
    //   15: aload_1
    //   16: ifnull +5 -> 21
    //   19: aload_1
    //   20: areturn
    //   21: ldc 2
    //   23: monitorenter
    //   24: aload_1
    //   25: ifnull +8 -> 33
    //   28: ldc 2
    //   30: monitorexit
    //   31: aload_1
    //   32: areturn
    //   33: new 57	com/truecaller/old/data/entity/b
    //   36: dup
    //   37: invokestatic 109	com/truecaller/common/e/d:a	()Lcom/truecaller/common/e/c;
    //   40: invokespecial 112	com/truecaller/old/data/entity/b:<init>	(Lcom/truecaller/common/e/c;)V
    //   43: putstatic 73	com/truecaller/old/data/access/d:a	Lcom/truecaller/old/data/entity/b;
    //   46: new 114	java/util/ArrayList
    //   49: dup
    //   50: invokespecial 115	java/util/ArrayList:<init>	()V
    //   53: astore_2
    //   54: iload_0
    //   55: ifeq +10 -> 65
    //   58: invokestatic 117	com/truecaller/common/e/d:f	()Ljava/util/List;
    //   61: astore_1
    //   62: goto +7 -> 69
    //   65: invokestatic 120	com/truecaller/common/e/d:g	()Ljava/util/List;
    //   68: astore_1
    //   69: aload_1
    //   70: invokeinterface 45 1 0
    //   75: astore_1
    //   76: aload_1
    //   77: invokeinterface 51 1 0
    //   82: ifeq +29 -> 111
    //   85: aload_2
    //   86: new 57	com/truecaller/old/data/entity/b
    //   89: dup
    //   90: aload_1
    //   91: invokeinterface 55 1 0
    //   96: checkcast 62	com/truecaller/common/e/c
    //   99: invokespecial 112	com/truecaller/old/data/entity/b:<init>	(Lcom/truecaller/common/e/c;)V
    //   102: invokeinterface 124 2 0
    //   107: pop
    //   108: goto -32 -> 76
    //   111: invokestatic 127	com/truecaller/common/e/e:a	()Ljava/util/Set;
    //   114: astore_1
    //   115: getstatic 129	com/truecaller/old/data/access/d:d	Ljava/util/Locale;
    //   118: astore 5
    //   120: aload 5
    //   122: ifnull +142 -> 264
    //   125: aload 5
    //   127: invokevirtual 80	java/util/Locale:getLanguage	()Ljava/lang/String;
    //   130: invokevirtual 132	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   133: astore_3
    //   134: aload_1
    //   135: aload_3
    //   136: invokeinterface 135 2 0
    //   141: ifeq +123 -> 264
    //   144: aload 5
    //   146: invokevirtual 92	java/util/Locale:getCountry	()Ljava/lang/String;
    //   149: invokevirtual 132	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   152: astore 4
    //   154: aload 5
    //   156: invokevirtual 98	java/util/Locale:getVariant	()Ljava/lang/String;
    //   159: invokevirtual 132	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   162: astore 5
    //   164: aload 4
    //   166: invokevirtual 139	java/lang/String:length	()I
    //   169: iconst_2
    //   170: if_icmple +50 -> 220
    //   173: new 82	java/lang/StringBuilder
    //   176: dup
    //   177: invokespecial 83	java/lang/StringBuilder:<init>	()V
    //   180: astore 4
    //   182: aload 4
    //   184: aload_3
    //   185: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   188: pop
    //   189: aload 4
    //   191: ldc 89
    //   193: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   196: pop
    //   197: aload 4
    //   199: aload 5
    //   201: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   204: pop
    //   205: aload_1
    //   206: aload 4
    //   208: invokevirtual 95	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   211: invokeinterface 135 2 0
    //   216: pop
    //   217: goto +47 -> 264
    //   220: new 82	java/lang/StringBuilder
    //   223: dup
    //   224: invokespecial 83	java/lang/StringBuilder:<init>	()V
    //   227: astore 5
    //   229: aload 5
    //   231: aload_3
    //   232: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   235: pop
    //   236: aload 5
    //   238: ldc 89
    //   240: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   243: pop
    //   244: aload 5
    //   246: aload 4
    //   248: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   251: pop
    //   252: aload_1
    //   253: aload 5
    //   255: invokevirtual 95	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   258: invokeinterface 135 2 0
    //   263: pop
    //   264: ldc -115
    //   266: invokestatic 146	com/truecaller/old/data/access/Settings:b	(Ljava/lang/String;)Ljava/lang/String;
    //   269: astore_3
    //   270: aload_3
    //   271: invokestatic 152	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   274: ifne +21 -> 295
    //   277: aload_1
    //   278: aload_3
    //   279: invokeinterface 155 2 0
    //   284: ifne +11 -> 295
    //   287: aload_1
    //   288: aload_3
    //   289: invokeinterface 135 2 0
    //   294: pop
    //   295: new 114	java/util/ArrayList
    //   298: dup
    //   299: invokespecial 115	java/util/ArrayList:<init>	()V
    //   302: astore_3
    //   303: aload_2
    //   304: invokeinterface 45 1 0
    //   309: astore_2
    //   310: aload_2
    //   311: invokeinterface 51 1 0
    //   316: ifeq +58 -> 374
    //   319: aload_2
    //   320: invokeinterface 55 1 0
    //   325: checkcast 57	com/truecaller/old/data/entity/b
    //   328: astore 4
    //   330: aload 4
    //   332: getfield 60	com/truecaller/old/data/entity/b:a	Lcom/truecaller/common/e/c;
    //   335: getfield 65	com/truecaller/common/e/c:b	Ljava/lang/String;
    //   338: invokevirtual 132	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   341: astore 5
    //   343: aload_1
    //   344: aload 5
    //   346: invokeinterface 155 2 0
    //   351: ifeq -41 -> 310
    //   354: aload 5
    //   356: invokestatic 157	com/truecaller/common/e/e:a	(Ljava/lang/String;)Z
    //   359: ifeq -49 -> 310
    //   362: aload_3
    //   363: aload 4
    //   365: invokeinterface 124 2 0
    //   370: pop
    //   371: goto -61 -> 310
    //   374: iload_0
    //   375: ifeq +13 -> 388
    //   378: aload_3
    //   379: invokestatic 163	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
    //   382: putstatic 104	com/truecaller/old/data/access/d:c	Ljava/util/List;
    //   385: goto +10 -> 395
    //   388: aload_3
    //   389: invokestatic 163	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
    //   392: putstatic 106	com/truecaller/old/data/access/d:b	Ljava/util/List;
    //   395: ldc 2
    //   397: monitorexit
    //   398: aload_3
    //   399: areturn
    //   400: astore_1
    //   401: ldc 2
    //   403: monitorexit
    //   404: aload_1
    //   405: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	406	0	paramBoolean	boolean
    //   7	337	1	localObject1	Object
    //   400	5	1	localObject2	Object
    //   53	267	2	localObject3	Object
    //   133	266	3	localObject4	Object
    //   152	212	4	localObject5	Object
    //   118	237	5	localObject6	Object
    // Exception table:
    //   from	to	target	type
    //   28	31	400	finally
    //   33	54	400	finally
    //   58	62	400	finally
    //   65	69	400	finally
    //   69	76	400	finally
    //   76	108	400	finally
    //   111	120	400	finally
    //   125	217	400	finally
    //   220	264	400	finally
    //   264	295	400	finally
    //   295	310	400	finally
    //   310	371	400	finally
    //   378	385	400	finally
    //   388	395	400	finally
    //   395	398	400	finally
    //   401	404	400	finally
  }
  
  public static List<b> b()
  {
    return a(true);
  }
  
  public static void b(Locale paramLocale)
  {
    d = paramLocale;
  }
  
  public static boolean c()
  {
    Object localObject = Locale.getDefault();
    if (localObject != null)
    {
      localObject = ((Locale)localObject).getDisplayName((Locale)localObject);
      if (!TextUtils.isEmpty((CharSequence)localObject))
      {
        if (Character.getDirectionality(((String)localObject).charAt(0)) != 1) {
          return Character.getDirectionality(((String)localObject).charAt(0)) == 2;
        }
        return true;
      }
    }
    return false;
  }
  
  public static Locale d()
  {
    return d;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.access.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */