package com.truecaller.old.data.access;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.log.d;
import com.truecaller.old.data.entity.c;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

@Deprecated
public final class e
  extends a<c>
{
  private HashMap<Long, Integer> b;
  
  public e(Context paramContext)
  {
    super(paramContext);
  }
  
  private void a(c paramc)
  {
    super.a(paramc);
    boolean bool2 = true;
    AssertionUtil.OnlyInDebug.isTrue(true, new String[0]);
    for (;;)
    {
      try
      {
        if (aea != a) {
          break label139;
        }
        bool1 = true;
        AssertionUtil.OnlyInDebug.isTrue(bool1, new String[0]);
        if (aea != a) {
          break label144;
        }
        bool1 = bool2;
        AssertionUtil.OnlyInDebug.isTrue(bool1, new String[0]);
      }
      catch (Exception localException)
      {
        d.a(localException);
        AssertionUtil.OnlyInDebug.isTrue(false, new String[0]);
      }
      HashMap localHashMap = b;
      if (localHashMap != null) {
        localHashMap.put(Long.valueOf(a), Integer.valueOf(e()));
      }
      return;
      label139:
      boolean bool1 = false;
      continue;
      label144:
      bool1 = false;
    }
  }
  
  private HashMap<Long, Integer> f()
  {
    if (b == null)
    {
      int j = e();
      b = new HashMap();
      int i = 1;
      while (i < j + 1)
      {
        c localc = (c)a(c.class, i);
        if (localc != null) {
          b.put(Long.valueOf(a), Integer.valueOf(i));
        }
        i += 1;
      }
    }
    return b;
  }
  
  public final c a(long paramLong)
  {
    Integer localInteger = (Integer)f().get(Long.valueOf(paramLong));
    c localc;
    if (localInteger != null) {
      localc = (c)a(c.class, localInteger.intValue());
    } else {
      localc = null;
    }
    if (localc != null) {
      p = localInteger.intValue();
    }
    return localc;
  }
  
  public final c a(long paramLong, String paramString, int paramInt)
  {
    Object localObject = d();
    try
    {
      Integer localInteger = (Integer)f().get(Long.valueOf(paramLong));
      c localc1 = null;
      if (localInteger != null) {
        localc1 = (c)a(c.class, localInteger.intValue());
      }
      if (localc1 != null)
      {
        n = paramString;
        o = paramInt;
        localObject = ((SharedPreferences)localObject).edit();
        ((SharedPreferences.Editor)localObject).putString(Integer.toString(localInteger.intValue()), localc1.a());
        a((SharedPreferences.Editor)localObject);
        return localc1;
      }
    }
    catch (Exception localException)
    {
      d.a(localException);
      c localc2 = new c();
      a = paramLong;
      b = "";
      c = "";
      j = "";
      k = "";
      l = "";
      n = paramString;
      o = paramInt;
      m = "";
      a(localc2);
      return localc2;
    }
  }
  
  protected final String a()
  {
    return "TC.meta.2.90";
  }
  
  public final void a(List<c> paramList)
  {
    int i = e();
    super.a(paramList);
    if (b != null)
    {
      paramList = paramList.iterator();
      while (paramList.hasNext())
      {
        c localc = (c)paramList.next();
        i += 1;
        b.put(Long.valueOf(a), Integer.valueOf(i));
      }
      boolean bool;
      if (e() == i) {
        bool = true;
      } else {
        bool = false;
      }
      AssertionUtil.OnlyInDebug.isTrue(bool, new String[0]);
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    super.a(paramBoolean);
    b = null;
  }
  
  public final void b()
  {
    super.b();
    b = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.access.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */