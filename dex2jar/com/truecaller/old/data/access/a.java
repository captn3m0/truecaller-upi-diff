package com.truecaller.old.data.access;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.truecaller.content.c.v;
import com.truecaller.content.c.w;
import com.truecaller.log.AssertionUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class a<T extends com.truecaller.old.data.entity.d>
{
  protected final Context a;
  private boolean b = Settings.b();
  
  public a(Context paramContext)
  {
    a = paramContext;
  }
  
  public final T a(Class<T> paramClass, int paramInt)
  {
    try
    {
      paramClass = (com.truecaller.old.data.entity.d)paramClass.newInstance();
    }
    catch (IllegalAccessException paramClass)
    {
      AssertionUtil.shouldNeverHappen(paramClass, new String[0]);
    }
    catch (InstantiationException paramClass)
    {
      AssertionUtil.shouldNeverHappen(paramClass, new String[0]);
    }
    paramClass = null;
    paramClass.a(d().getString(Integer.toString(paramInt % Integer.MAX_VALUE), ""));
    return paramClass;
  }
  
  protected abstract String a();
  
  public final List<T> a(Class<T> paramClass)
  {
    ArrayList localArrayList = new ArrayList();
    int j = c();
    int k = e();
    int i = 0;
    for (;;)
    {
      if (i < j) {
        try
        {
          localArrayList.add(a(paramClass, k - i));
          i += 1;
        }
        catch (Exception paramClass)
        {
          com.truecaller.log.d.a(paramClass, "In Dao - getAllForSuggestions exception");
          localArrayList.clear();
        }
      }
    }
    return localArrayList;
  }
  
  @SuppressLint({"ApplySharedPref"})
  final void a(SharedPreferences.Editor paramEditor)
  {
    if (b)
    {
      paramEditor.commit();
      return;
    }
    paramEditor.apply();
  }
  
  public void a(T paramT)
  {
    Object localObject = d();
    int i = ((SharedPreferences)localObject).getInt("size", 0);
    localObject = ((SharedPreferences)localObject).edit();
    i += 1;
    ((SharedPreferences.Editor)localObject).putString(Integer.toString(i % Integer.MAX_VALUE), paramT.a());
    ((SharedPreferences.Editor)localObject).putInt("size", i);
    a((SharedPreferences.Editor)localObject);
  }
  
  public void a(List<T> paramList)
  {
    Object localObject = d();
    int i = ((SharedPreferences)localObject).getInt("size", 0);
    localObject = ((SharedPreferences)localObject).edit();
    int j = i;
    if (paramList != null)
    {
      paramList = paramList.iterator();
      for (;;)
      {
        j = i;
        if (!paramList.hasNext()) {
          break;
        }
        com.truecaller.old.data.entity.d locald = (com.truecaller.old.data.entity.d)paramList.next();
        i += 1;
        ((SharedPreferences.Editor)localObject).putString(Integer.toString(i % Integer.MAX_VALUE), locald.a());
      }
    }
    ((SharedPreferences.Editor)localObject).putInt("size", j);
    a((SharedPreferences.Editor)localObject);
  }
  
  public void a(boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = d().edit();
    localEditor.clear();
    a(localEditor);
  }
  
  public void b()
  {
    a(true);
  }
  
  public int c()
  {
    return Math.min(e(), Integer.MAX_VALUE);
  }
  
  @SuppressLint({"ApplySharedPref"})
  protected final SharedPreferences d()
  {
    Object localObject = a();
    SharedPreferences localSharedPreferences = v.a(a, (String)localObject);
    if (w.a(a, "TC.settings.3.0.beta5"))
    {
      localObject = a.getSharedPreferences((String)localObject, 0);
      w.a((SharedPreferences)localObject, localSharedPreferences);
      ((SharedPreferences)localObject).edit().clear().commit();
    }
    return localSharedPreferences;
  }
  
  protected final int e()
  {
    return d().getInt("size", 0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.access.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */