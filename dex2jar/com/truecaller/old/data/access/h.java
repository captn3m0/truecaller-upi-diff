package com.truecaller.old.data.access;

import android.content.Context;
import android.text.TextUtils;
import com.google.gson.i;
import com.google.gson.l;
import com.google.gson.o;
import com.google.gson.q;
import com.truecaller.log.d;
import com.truecaller.old.data.entity.f;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

@Deprecated
public abstract class h<T extends f>
  extends c
{
  private static final Object a = new Object();
  private static final Object c = new Object();
  private static Map<String, TreeSet<? extends f>> d = new HashMap();
  
  public h(Context arg1)
  {
    super(???);
    synchronized (a)
    {
      if (!d.containsKey(d())) {
        d.put(d(), f());
      }
      return;
    }
  }
  
  private static Collection<T> a(Collection<T> paramCollection, Collection<h.a<T>> paramCollection1)
  {
    TreeSet localTreeSet = new TreeSet();
    HashMap localHashMap = new HashMap();
    Object localObject = paramCollection1.iterator();
    while (((Iterator)localObject).hasNext()) {
      localHashMap.put((h.a)((Iterator)localObject).next(), new ArrayList());
    }
    paramCollection = paramCollection.iterator();
    while (paramCollection.hasNext())
    {
      localObject = (f)paramCollection.next();
      int j = 0;
      Iterator localIterator = paramCollection1.iterator();
      h.a locala;
      do
      {
        i = j;
        if (!localIterator.hasNext()) {
          break;
        }
        locala = (h.a)localIterator.next();
      } while (!locala.a(localObject));
      ((ArrayList)localHashMap.get(locala)).add(localObject);
      int i = 1;
      if (i == 0) {
        localTreeSet.add(localObject);
      }
    }
    paramCollection = paramCollection1.iterator();
    while (paramCollection.hasNext())
    {
      paramCollection1 = (h.a)paramCollection.next();
      localObject = (ArrayList)localHashMap.get(paramCollection1);
      if (!((ArrayList)localObject).isEmpty()) {
        localTreeSet.add((f)paramCollection1.a((Collection)localObject));
      }
    }
    return localTreeSet;
  }
  
  private TreeSet<T> a(i parami)
    throws Exception
  {
    ArrayList localArrayList = new ArrayList();
    if (parami != null)
    {
      int i = 0;
      int j = parami.a();
      while (i < j)
      {
        f localf = a(parami.a(i).i());
        if (localf != null) {
          localArrayList.add(localf);
        }
        i += 1;
      }
    }
    return new TreeSet(localArrayList);
  }
  
  private TreeSet<T> f()
  {
    TreeSet localTreeSet = new TreeSet();
    try
    {
      Object localObject = a("LIST");
      if (!TextUtils.isEmpty((CharSequence)localObject))
      {
        localObject = a(q.a((String)localObject).j());
        return (TreeSet<T>)localObject;
      }
    }
    catch (Exception localException)
    {
      d.a(localException);
    }
    return localTreeSet;
  }
  
  private TreeSet<T> g()
  {
    synchronized (c)
    {
      TreeSet localTreeSet = (TreeSet)d.get(d());
      return localTreeSet;
    }
  }
  
  protected int a()
  {
    return 0;
  }
  
  protected abstract T a(o paramo);
  
  protected Collection<h.a<T>> b()
  {
    return new ArrayList();
  }
  
  protected h.b<T> c()
  {
    return new h.1(this);
  }
  
  protected final Collection<T> d(Collection<T> paramCollection)
  {
    h.b localb = c();
    ArrayList localArrayList = new ArrayList();
    paramCollection = paramCollection.iterator();
    while (paramCollection.hasNext())
    {
      f localf = (f)paramCollection.next();
      if (localb.apply(localf)) {
        localArrayList.add(localf);
      }
    }
    return a(localArrayList, b());
  }
  
  public final void e(Collection<T> paramCollection)
  {
    synchronized (c)
    {
      g().removeAll(paramCollection);
      f(paramCollection);
      return;
    }
  }
  
  public final int f(Collection<T> arg1)
  {
    Object localObject1 = c;
    int i = 0;
    try
    {
      Object localObject2 = g();
      Object localObject4 = ???.iterator();
      while (((Iterator)localObject4).hasNext()) {
        if (!((TreeSet)localObject2).contains(((Iterator)localObject4).next())) {
          i += 1;
        }
      }
      if (i > 0)
      {
        ((TreeSet)localObject2).addAll(???);
        localObject2 = new TreeSet(d(g()));
        while ((a() > 0) && (((TreeSet)localObject2).size() > a())) {
          ((TreeSet)localObject2).pollLast();
        }
        synchronized (c)
        {
          localObject4 = g();
          ((TreeSet)localObject4).clear();
          ((TreeSet)localObject4).addAll((Collection)localObject2);
          ??? = new i();
          localObject2 = ((TreeSet)localObject2).iterator();
          while (((Iterator)localObject2).hasNext())
          {
            localObject4 = (f)((Iterator)localObject2).next();
            try
            {
              ???.a(((f)localObject4).s());
            }
            catch (Throwable localThrowable)
            {
              d.a(localThrowable);
            }
          }
          a("LIST", ???.toString());
          j();
        }
      }
      return i;
    }
    finally {}
  }
  
  protected void j() {}
  
  public final void k()
  {
    synchronized (c)
    {
      g().clear();
      e();
      j();
      return;
    }
  }
  
  public final List<T> l()
  {
    return new ArrayList(g());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.access.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */