package com.truecaller.old.data.access;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.truecaller.old.data.entity.d;
import java.util.Map;

public final class i
  extends a<d>
{
  public i(Context paramContext)
  {
    super(paramContext);
  }
  
  protected final String a()
  {
    return "TC.ugc.2.00";
  }
  
  public final boolean a(String paramString)
  {
    return d().contains(paramString);
  }
  
  public final void b(String paramString)
  {
    SharedPreferences.Editor localEditor = d().edit();
    localEditor.putBoolean(paramString, true);
    a(localEditor);
  }
  
  public final int c()
  {
    return d().getAll().size();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.access.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */