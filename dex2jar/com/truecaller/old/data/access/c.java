package com.truecaller.old.data.access;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.truecaller.content.c.v;
import com.truecaller.content.c.w;

public abstract class c
{
  private boolean a = Settings.b();
  protected final Context b;
  
  public c(Context paramContext)
  {
    b = paramContext.getApplicationContext();
  }
  
  @SuppressLint({"ApplySharedPref"})
  private SharedPreferences a()
  {
    Object localObject1 = new StringBuilder("truecaller.data.");
    ((StringBuilder)localObject1).append(d());
    Object localObject2 = ((StringBuilder)localObject1).toString();
    localObject1 = v.a(b, (String)localObject2);
    if (w.a(b, "TC.settings.3.0.beta5"))
    {
      localObject2 = b.getSharedPreferences((String)localObject2, 0);
      w.a((SharedPreferences)localObject2, (SharedPreferences)localObject1);
      ((SharedPreferences)localObject2).edit().clear().commit();
    }
    return (SharedPreferences)localObject1;
  }
  
  @SuppressLint({"ApplySharedPref"})
  private void a(SharedPreferences.Editor paramEditor)
  {
    if (a)
    {
      paramEditor.commit();
      return;
    }
    paramEditor.apply();
  }
  
  protected final String a(String paramString)
  {
    return a().getString(paramString, "");
  }
  
  protected final void a(String paramString, long paramLong)
  {
    SharedPreferences.Editor localEditor = a().edit();
    localEditor.putLong(paramString, paramLong);
    a(localEditor);
  }
  
  protected final void a(String paramString1, String paramString2)
  {
    SharedPreferences.Editor localEditor = a().edit();
    localEditor.putString(paramString1, paramString2);
    a(localEditor);
  }
  
  protected final Long b(String paramString)
  {
    return Long.valueOf(a().getLong(paramString, 0L));
  }
  
  protected abstract String d();
  
  protected final void e()
  {
    SharedPreferences.Editor localEditor = a().edit();
    localEditor.clear();
    a(localEditor);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.access.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */