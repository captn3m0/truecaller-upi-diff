package com.truecaller.old.data.access;

import android.content.Context;
import com.google.gson.o;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.log.d;
import com.truecaller.network.notification.NotificationScope;
import com.truecaller.network.notification.NotificationType;
import com.truecaller.network.notification.c.a;
import com.truecaller.network.notification.c.a.b;
import com.truecaller.notifications.ah;
import com.truecaller.old.data.entity.Notification;
import com.truecaller.old.data.entity.Notification.NotificationState;
import com.truecaller.service.WidgetListProvider;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

public final class f
  extends h<Notification>
{
  public f(Context paramContext)
  {
    super(paramContext);
  }
  
  private static String a(NotificationScope paramNotificationScope)
  {
    StringBuilder localStringBuilder = new StringBuilder("LAST_ID_");
    localStringBuilder.append(value);
    return localStringBuilder.toString();
  }
  
  private void a(Map<NotificationScope, Long> paramMap)
  {
    paramMap = paramMap.entrySet().iterator();
    while (paramMap.hasNext())
    {
      Object localObject = (Map.Entry)paramMap.next();
      NotificationScope localNotificationScope = (NotificationScope)((Map.Entry)localObject).getKey();
      localObject = (Long)((Map.Entry)localObject).getValue();
      a(a(localNotificationScope), ((Long)localObject).longValue());
    }
  }
  
  private static Notification b(o paramo)
  {
    try
    {
      paramo = new Notification(paramo);
      return paramo;
    }
    catch (Throwable paramo)
    {
      d.a(paramo);
    }
    return null;
  }
  
  public static Collection<Notification> b(Collection<Notification> paramCollection)
  {
    TreeSet localTreeSet = new TreeSet();
    paramCollection = paramCollection.iterator();
    while (paramCollection.hasNext())
    {
      Notification localNotification = (Notification)paramCollection.next();
      if (localNotification.r()) {
        localTreeSet.add(localNotification);
      }
    }
    return localTreeSet;
  }
  
  protected final int a()
  {
    return 100;
  }
  
  public final int a(Collection<Notification> paramCollection, Boolean paramBoolean)
  {
    if (paramBoolean.booleanValue())
    {
      paramBoolean = new HashMap();
      Iterator localIterator = paramCollection.iterator();
      while (localIterator.hasNext())
      {
        Notification localNotification = (Notification)localIterator.next();
        Long localLong = (Long)paramBoolean.get(a.a.c);
        if ((localLong == null) || (localLong.longValue() < a.a.a)) {
          paramBoolean.put(a.a.c, Long.valueOf(a.a.a));
        }
      }
      a(paramBoolean);
    }
    return super.f(paramCollection);
  }
  
  public final Collection<Notification> a(Collection<c.a> paramCollection)
  {
    ArrayList localArrayList = new ArrayList(paramCollection.size());
    ah localah = TrueApp.y().a().aK();
    paramCollection = paramCollection.iterator();
    while (paramCollection.hasNext())
    {
      Notification localNotification = new Notification((c.a)paramCollection.next());
      if (localNotification.b() == NotificationType.GENERIC_WEBVIEW)
      {
        String str = localah.b(localNotification.a("u"));
        if (str != null)
        {
          d = localah.a(str);
          localArrayList.add(localNotification);
        }
      }
      else
      {
        localArrayList.add(localNotification);
      }
    }
    return d(localArrayList);
  }
  
  public final void a(Collection<Notification> paramCollection, Notification.NotificationState paramNotificationState)
  {
    Iterator localIterator = paramCollection.iterator();
    while (localIterator.hasNext()) {
      nextb = paramNotificationState;
    }
    e(paramCollection);
  }
  
  protected final Collection<h.a<Notification>> b()
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(new f.1(this));
    return localArrayList;
  }
  
  protected final h.b<Notification> c()
  {
    return -..Lambda.f.J9ZUvgGuOH0ZHSidmjIaJfX-tzU.INSTANCE;
  }
  
  public final void c(Collection<Notification> paramCollection)
  {
    a(paramCollection, Notification.NotificationState.VIEWED);
  }
  
  protected final String d()
  {
    return "Notifications";
  }
  
  public final int f()
  {
    Iterator localIterator = b(l()).iterator();
    int i = 0;
    while (localIterator.hasNext()) {
      if (nextb == Notification.NotificationState.NEW) {
        i += 1;
      }
    }
    return i;
  }
  
  public final Map<NotificationScope, Long> g()
  {
    HashMap localHashMap = new HashMap();
    NotificationScope[] arrayOfNotificationScope = NotificationScope.values();
    int j = arrayOfNotificationScope.length;
    int i = 0;
    while (i < j)
    {
      NotificationScope localNotificationScope = arrayOfNotificationScope[i];
      localHashMap.put(localNotificationScope, b(a(localNotificationScope)));
      i += 1;
    }
    return localHashMap;
  }
  
  public final Collection<Notification> h()
  {
    return b(l());
  }
  
  public final Notification i()
  {
    NotificationType localNotificationType = NotificationType.SOFTWARE_UPDATE;
    List localList = l();
    Iterator localIterator = localList.iterator();
    Object localObject1 = null;
    while (localIterator.hasNext())
    {
      Notification localNotification = (Notification)localIterator.next();
      if (localNotification.b() == localNotificationType)
      {
        Object localObject2 = localObject1;
        if (localObject1 == null) {
          localObject2 = new ArrayList(localList.size());
        }
        ((ArrayList)localObject2).add(localNotification);
        localObject1 = localObject2;
      }
    }
    if ((localObject1 != null) && (((List)localObject1).size() > 0)) {
      return (Notification)((List)localObject1).get(0);
    }
    return null;
  }
  
  protected final void j()
  {
    WidgetListProvider.a(b);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.access.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */