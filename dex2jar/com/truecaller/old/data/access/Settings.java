package com.truecaller.old.data.access;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.provider.Settings.System;
import android.text.TextUtils;
import com.truecaller.TrueApp;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.callhistory.ae;
import com.truecaller.common.enhancedsearch.EnhancedSearchStateWorker;
import com.truecaller.common.h.am;
import com.truecaller.common.h.h;
import com.truecaller.common.tag.sync.AvailableTagsDownloadWorker;
import com.truecaller.filters.p;
import com.truecaller.filters.sync.FilterUploadWorker;
import com.truecaller.filters.v;
import com.truecaller.service.RefreshT9MappingService;
import com.truecaller.service.SyncPhoneBookService;
import com.truecaller.utils.l;
import java.io.File;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class Settings
{
  private static volatile boolean a = false;
  private static volatile SharedPreferences b;
  private static SharedPreferences.Editor c;
  
  public static int a(String paramString, int paramInt)
  {
    return (int)b.getLong(paramString, paramInt);
  }
  
  public static String a(String paramString1, String paramString2)
  {
    return b.getString(paramString1, paramString2);
  }
  
  public static void a(long paramLong)
  {
    "disable ads until ".concat(String.valueOf(paramLong));
    a("adsDisabledUntil", paramLong);
  }
  
  public static void a(Context paramContext, com.truecaller.old.data.entity.b paramb)
  {
    b("language", a.b);
    c(paramContext);
  }
  
  public static void a(String paramString, long paramLong)
  {
    c.putLong(paramString, paramLong);
    if (a) {
      return;
    }
    n();
  }
  
  public static void a(String paramString, boolean paramBoolean)
  {
    c.putBoolean(paramString, paramBoolean);
    if (a) {
      return;
    }
    n();
  }
  
  public static boolean a()
  {
    return false;
  }
  
  public static boolean a(int paramInt)
  {
    return (paramInt & 0x8) == 0;
  }
  
  public static boolean a(Context paramContext)
  {
    return com.truecaller.common.h.k.a(paramContext);
  }
  
  public static boolean a(String paramString)
  {
    return b.contains(paramString);
  }
  
  public static String b(String paramString)
  {
    return a(paramString, "");
  }
  
  /* Error */
  @android.annotation.SuppressLint({"CommitPrefEdits"})
  public static void b(Context paramContext)
  {
    // Byte code:
    //   0: aload_0
    //   1: ldc 106
    //   3: iconst_0
    //   4: invokevirtual 112	android/content/Context:getSharedPreferences	(Ljava/lang/String;I)Landroid/content/SharedPreferences;
    //   7: astore_2
    //   8: aload_2
    //   9: putstatic 19	com/truecaller/old/data/access/Settings:b	Landroid/content/SharedPreferences;
    //   12: aload_2
    //   13: invokeinterface 116 1 0
    //   18: putstatic 68	com/truecaller/old/data/access/Settings:c	Landroid/content/SharedPreferences$Editor;
    //   21: invokestatic 122	com/truecaller/TrueApp:y	()Lcom/truecaller/TrueApp;
    //   24: invokevirtual 125	com/truecaller/TrueApp:a	()Lcom/truecaller/bp;
    //   27: astore_2
    //   28: aload_2
    //   29: invokeinterface 131 1 0
    //   34: astore_3
    //   35: aload_2
    //   36: invokeinterface 135 1 0
    //   41: astore 4
    //   43: aload_2
    //   44: invokeinterface 139 1 0
    //   49: astore 5
    //   51: ldc -115
    //   53: invokestatic 144	com/truecaller/old/data/access/Settings:e	(Ljava/lang/String;)Z
    //   56: ifeq +8 -> 64
    //   59: aload_0
    //   60: invokestatic 147	com/truecaller/old/data/access/Settings:f	(Landroid/content/Context;)V
    //   63: return
    //   64: aload_2
    //   65: invokeinterface 151 1 0
    //   70: invokeinterface 156 1 0
    //   75: ldc -98
    //   77: ldc2_w 159
    //   80: invokestatic 47	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;J)V
    //   83: ldc -94
    //   85: getstatic 167	android/os/Build$VERSION:RELEASE	Ljava/lang/String;
    //   88: invokestatic 63	com/truecaller/old/data/access/Settings:b	(Ljava/lang/String;Ljava/lang/String;)V
    //   91: ldc -87
    //   93: ldc2_w 170
    //   96: invokestatic 47	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;J)V
    //   99: aload 4
    //   101: ldc -83
    //   103: invokestatic 179	java/lang/System:currentTimeMillis	()J
    //   106: invokeinterface 183 4 0
    //   111: aload_0
    //   112: invokestatic 186	com/truecaller/old/data/access/Settings:g	(Landroid/content/Context;)Z
    //   115: ifeq +9 -> 124
    //   118: ldc -68
    //   120: iconst_1
    //   121: invokestatic 190	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   124: invokestatic 193	com/truecaller/common/h/k:d	()Z
    //   127: ifeq +13 -> 140
    //   130: ldc -61
    //   132: aload_0
    //   133: invokestatic 200	com/truecaller/callerid/b/b:a	(Landroid/content/Context;)I
    //   136: i2l
    //   137: invokestatic 47	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;J)V
    //   140: aload_3
    //   141: ldc -54
    //   143: iconst_1
    //   144: invokeinterface 206 3 0
    //   149: aload_3
    //   150: ldc -48
    //   152: iconst_1
    //   153: invokeinterface 206 3 0
    //   158: ldc -46
    //   160: iconst_1
    //   161: invokestatic 190	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   164: aload_0
    //   165: invokestatic 215	com/truecaller/old/data/access/d:d	()Ljava/util/Locale;
    //   168: invokestatic 218	com/truecaller/old/data/access/d:a	(Ljava/util/Locale;)Lcom/truecaller/old/data/entity/b;
    //   171: invokestatic 220	com/truecaller/old/data/access/Settings:a	(Landroid/content/Context;Lcom/truecaller/old/data/entity/b;)V
    //   174: ldc -34
    //   176: iconst_1
    //   177: invokestatic 190	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   180: aload 4
    //   182: ldc -32
    //   184: bipush 100
    //   186: invokeinterface 227 3 0
    //   191: ldc -27
    //   193: ldc -25
    //   195: invokestatic 63	com/truecaller/old/data/access/Settings:b	(Ljava/lang/String;Ljava/lang/String;)V
    //   198: ldc -23
    //   200: invokestatic 179	java/lang/System:currentTimeMillis	()J
    //   203: invokestatic 47	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;J)V
    //   206: aload 4
    //   208: ldc -21
    //   210: bipush 21
    //   212: invokeinterface 227 3 0
    //   217: ldc -19
    //   219: invokestatic 179	java/lang/System:currentTimeMillis	()J
    //   222: ldc2_w 238
    //   225: ladd
    //   226: invokestatic 47	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;J)V
    //   229: ldc -15
    //   231: iconst_1
    //   232: invokestatic 190	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   235: ldc -13
    //   237: iconst_1
    //   238: invokestatic 190	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   241: ldc -11
    //   243: iconst_1
    //   244: invokestatic 190	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   247: ldc -9
    //   249: iconst_1
    //   250: invokestatic 190	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   253: ldc -7
    //   255: iconst_1
    //   256: invokestatic 190	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   259: aload 5
    //   261: ldc -5
    //   263: iconst_1
    //   264: invokeinterface 254 3 0
    //   269: ldc_w 256
    //   272: iconst_1
    //   273: invokestatic 190	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   276: ldc_w 258
    //   279: ldc_w 260
    //   282: invokestatic 63	com/truecaller/old/data/access/Settings:b	(Ljava/lang/String;Ljava/lang/String;)V
    //   285: ldc_w 262
    //   288: iconst_0
    //   289: invokestatic 190	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   292: getstatic 19	com/truecaller/old/data/access/Settings:b	Landroid/content/SharedPreferences;
    //   295: astore_2
    //   296: aload_0
    //   297: ldc_w 264
    //   300: invokevirtual 268	android/content/Context:getDatabasePath	(Ljava/lang/String;)Ljava/io/File;
    //   303: invokevirtual 273	java/io/File:exists	()Z
    //   306: ifeq +393 -> 699
    //   309: aload_2
    //   310: invokeinterface 116 1 0
    //   315: astore 5
    //   317: aconst_null
    //   318: astore_2
    //   319: aconst_null
    //   320: astore_3
    //   321: aload_0
    //   322: ldc_w 264
    //   325: iconst_0
    //   326: aconst_null
    //   327: invokevirtual 277	android/content/Context:openOrCreateDatabase	(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;
    //   330: astore 4
    //   332: aload_3
    //   333: astore_2
    //   334: aload 4
    //   336: ldc_w 279
    //   339: iconst_3
    //   340: anewarray 34	java/lang/String
    //   343: dup
    //   344: iconst_0
    //   345: ldc_w 281
    //   348: aastore
    //   349: dup
    //   350: iconst_1
    //   351: ldc_w 282
    //   354: aastore
    //   355: dup
    //   356: iconst_2
    //   357: ldc_w 284
    //   360: aastore
    //   361: aconst_null
    //   362: aconst_null
    //   363: aconst_null
    //   364: aconst_null
    //   365: aconst_null
    //   366: invokevirtual 290	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   369: astore_3
    //   370: aload_3
    //   371: astore_2
    //   372: aload_3
    //   373: invokeinterface 295 1 0
    //   378: ifeq +250 -> 628
    //   381: aload_3
    //   382: astore_2
    //   383: aload_3
    //   384: iconst_0
    //   385: invokeinterface 298 2 0
    //   390: astore 6
    //   392: aload_3
    //   393: astore_2
    //   394: aload_3
    //   395: iconst_1
    //   396: invokeinterface 302 2 0
    //   401: astore 7
    //   403: aload_3
    //   404: astore_2
    //   405: aload_3
    //   406: iconst_2
    //   407: invokeinterface 306 2 0
    //   412: istore_1
    //   413: aload_3
    //   414: astore_2
    //   415: new 308	java/io/DataInputStream
    //   418: dup
    //   419: new 310	java/io/ByteArrayInputStream
    //   422: dup
    //   423: aload 7
    //   425: invokespecial 314	java/io/ByteArrayInputStream:<init>	([B)V
    //   428: invokespecial 317	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
    //   431: astore 7
    //   433: iload_1
    //   434: iconst_2
    //   435: if_icmpeq +101 -> 536
    //   438: iload_1
    //   439: iconst_4
    //   440: if_icmpeq +78 -> 518
    //   443: iload_1
    //   444: bipush 8
    //   446: if_icmpeq +54 -> 500
    //   449: iload_1
    //   450: bipush 16
    //   452: if_icmpeq +30 -> 482
    //   455: iload_1
    //   456: bipush 32
    //   458: if_icmpeq +6 -> 464
    //   461: goto +91 -> 552
    //   464: aload 5
    //   466: aload 6
    //   468: aload 7
    //   470: invokevirtual 321	java/io/DataInputStream:readUTF	()Ljava/lang/String;
    //   473: invokeinterface 325 3 0
    //   478: pop
    //   479: goto +73 -> 552
    //   482: aload 5
    //   484: aload 6
    //   486: aload 7
    //   488: invokevirtual 328	java/io/DataInputStream:readBoolean	()Z
    //   491: invokeinterface 84 3 0
    //   496: pop
    //   497: goto +55 -> 552
    //   500: aload 5
    //   502: aload 6
    //   504: aload 7
    //   506: invokevirtual 332	java/io/DataInputStream:readFloat	()F
    //   509: invokeinterface 336 3 0
    //   514: pop
    //   515: goto +37 -> 552
    //   518: aload 5
    //   520: aload 6
    //   522: aload 7
    //   524: invokevirtual 339	java/io/DataInputStream:readLong	()J
    //   527: invokeinterface 74 4 0
    //   532: pop
    //   533: goto +19 -> 552
    //   536: aload 5
    //   538: aload 6
    //   540: aload 7
    //   542: invokevirtual 343	java/io/DataInputStream:readInt	()I
    //   545: i2l
    //   546: invokeinterface 74 4 0
    //   551: pop
    //   552: aload_3
    //   553: astore_2
    //   554: aload 7
    //   556: invokestatic 348	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   559: goto -189 -> 370
    //   562: astore 6
    //   564: goto +52 -> 616
    //   567: astore_2
    //   568: new 350	java/lang/StringBuilder
    //   571: dup
    //   572: ldc_w 352
    //   575: invokespecial 355	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   578: astore 8
    //   580: aload 8
    //   582: aload 6
    //   584: invokevirtual 359	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   587: pop
    //   588: aload 8
    //   590: ldc_w 361
    //   593: invokevirtual 359	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   596: pop
    //   597: new 104	java/io/IOException
    //   600: dup
    //   601: aload 8
    //   603: invokevirtual 364	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   606: aload_2
    //   607: invokespecial 367	java/io/IOException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   610: invokestatic 372	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   613: goto -61 -> 552
    //   616: aload_3
    //   617: astore_2
    //   618: aload 7
    //   620: invokestatic 348	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   623: aload_3
    //   624: astore_2
    //   625: aload 6
    //   627: athrow
    //   628: aload 5
    //   630: invokeinterface 375 1 0
    //   635: pop
    //   636: aload_3
    //   637: invokestatic 378	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   640: aload 4
    //   642: invokestatic 381	com/truecaller/util/q:a	(Landroid/database/sqlite/SQLiteDatabase;)V
    //   645: aload_0
    //   646: ldc_w 264
    //   649: invokevirtual 384	android/content/Context:deleteDatabase	(Ljava/lang/String;)Z
    //   652: pop
    //   653: aload_0
    //   654: invokestatic 147	com/truecaller/old/data/access/Settings:f	(Landroid/content/Context;)V
    //   657: goto +42 -> 699
    //   660: astore_3
    //   661: goto +7 -> 668
    //   664: astore_3
    //   665: aconst_null
    //   666: astore 4
    //   668: aload 5
    //   670: invokeinterface 375 1 0
    //   675: pop
    //   676: aload_2
    //   677: invokestatic 378	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   680: aload 4
    //   682: invokestatic 381	com/truecaller/util/q:a	(Landroid/database/sqlite/SQLiteDatabase;)V
    //   685: aload_0
    //   686: ldc_w 264
    //   689: invokevirtual 384	android/content/Context:deleteDatabase	(Ljava/lang/String;)Z
    //   692: pop
    //   693: aload_0
    //   694: invokestatic 147	com/truecaller/old/data/access/Settings:f	(Landroid/content/Context;)V
    //   697: aload_3
    //   698: athrow
    //   699: ldc -115
    //   701: iconst_1
    //   702: invokestatic 190	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   705: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	706	0	paramContext	Context
    //   412	47	1	i	int
    //   7	547	2	localObject1	Object
    //   567	40	2	localIOException	java.io.IOException
    //   617	60	2	localObject2	Object
    //   34	603	3	localObject3	Object
    //   660	1	3	localObject4	Object
    //   664	34	3	localObject5	Object
    //   41	640	4	localObject6	Object
    //   49	620	5	localObject7	Object
    //   390	149	6	str1	String
    //   562	64	6	str2	String
    //   401	218	7	localObject8	Object
    //   578	24	8	localStringBuilder	StringBuilder
    // Exception table:
    //   from	to	target	type
    //   464	479	562	finally
    //   482	497	562	finally
    //   500	515	562	finally
    //   518	533	562	finally
    //   536	552	562	finally
    //   568	613	562	finally
    //   464	479	567	java/io/IOException
    //   482	497	567	java/io/IOException
    //   500	515	567	java/io/IOException
    //   518	533	567	java/io/IOException
    //   536	552	567	java/io/IOException
    //   334	370	660	finally
    //   372	381	660	finally
    //   383	392	660	finally
    //   394	403	660	finally
    //   405	413	660	finally
    //   415	433	660	finally
    //   554	559	660	finally
    //   618	623	660	finally
    //   625	628	660	finally
    //   321	332	664	finally
  }
  
  public static void b(String paramString1, String paramString2)
  {
    c.putString(paramString1, paramString2);
    if (a) {
      return;
    }
    n();
  }
  
  static boolean b()
  {
    return false;
  }
  
  public static boolean b(int paramInt)
  {
    return paramInt == 8;
  }
  
  public static boolean b(String paramString, long paramLong)
  {
    return System.currentTimeMillis() - d(paramString).longValue() > paramLong;
  }
  
  public static boolean b(String paramString, boolean paramBoolean)
  {
    return b.getBoolean(paramString, paramBoolean);
  }
  
  public static int c(String paramString)
  {
    return (int)b.getLong(paramString, 0L);
  }
  
  public static void c()
  {
    c.clear();
    if (a) {
      return;
    }
    n();
  }
  
  public static void c(int paramInt)
  {
    a("contact_count", paramInt);
  }
  
  public static void c(Context paramContext)
  {
    if (b("languageAuto", true))
    {
      paramContext = Locale.getDefault();
      if (paramContext != null) {
        b("language", aa.b);
      }
      return;
    }
    Object localObject = a("language", "");
    String[] arrayOfString = ((String)localObject).split("_");
    if (arrayOfString.length == 2) {
      localObject = new Locale(arrayOfString[0], arrayOfString[1]);
    } else {
      localObject = new Locale((String)localObject);
    }
    com.truecaller.common.e.f.a(paramContext, (Locale)localObject);
  }
  
  public static boolean c(String paramString, long paramLong)
  {
    return d(paramString).longValue() >= paramLong;
  }
  
  public static Long d(String paramString)
  {
    return Long.valueOf(b.getLong(paramString, 0L));
  }
  
  public static void d(String paramString, long paramLong)
  {
    a(o(paramString), paramLong);
    a(p(paramString), false);
  }
  
  public static boolean d()
  {
    return (e("alwaysDownloadImages")) || (TrueApp.y().a().v().d());
  }
  
  public static boolean d(Context paramContext)
  {
    return (h(paramContext) & 0x1) != 0;
  }
  
  public static boolean e()
  {
    return e("hasTruedialerIntegration");
  }
  
  public static boolean e(Context paramContext)
  {
    if (((bk)paramContext.getApplicationContext()).a().D().b("hasNativeDialerCallerId")) {
      return Settings.System.getInt(paramContext.getContentResolver(), "dtmf_tone", 1) == 1;
    }
    return (h(paramContext) & 0x2) != 0;
  }
  
  public static boolean e(String paramString)
  {
    return b.getBoolean(paramString, false);
  }
  
  private static void f(Context paramContext)
  {
    int j = a("global_settings_ver", 0);
    if (j < 31) {
      i = 1;
    } else {
      i = 0;
    }
    if (i == 0) {
      return;
    }
    Object localObject3 = (com.truecaller.common.b.a)paramContext.getApplicationContext();
    Object localObject1 = ((bk)localObject3).a();
    Object localObject2 = a("version", "7.60");
    int k = a("VERSION_CODE", 0);
    com.truecaller.i.c localc = ((bp)localObject1).D();
    Object localObject4 = ((bp)localObject1).F();
    com.truecaller.common.g.a locala = ((bp)localObject1).I();
    if (((String)localObject2).compareTo("2.99") < 0) {
      f("GOOGLE_REVIEW_ASK_TIMESTAMP");
    }
    if (((String)localObject2).compareTo("3.0") < 0) {
      a("clearTCHistory", true);
    }
    if (((String)localObject2).compareTo("3.32") < 0) {
      ((com.truecaller.i.e)localObject4).b("backupBatchSize", 100);
    }
    if (((String)localObject2).compareTo("4.0") < 0) {
      a("notificationPush", true);
    }
    boolean bool1;
    if (((String)localObject2).compareTo("4.04") < 0)
    {
      a("collaborativeUserTimestamp", System.currentTimeMillis());
      localObject4 = ((bp)localObject1).R();
      if ((!e("CALL_FILTER_TOP")) && (!e("SMS_FILTER_TOP"))) {
        bool1 = false;
      } else {
        bool1 = true;
      }
      boolean bool2;
      if ((!e("CALL_FILTER_UNKNOWN")) && (!e("SMS_FILTER_UNKNOWN"))) {
        bool2 = false;
      } else {
        bool2 = true;
      }
      ((p)localObject4).f(bool1);
      ((p)localObject4).a(bool2);
    }
    if (((String)localObject2).compareTo("4.10") < 0) {
      if ((e("profileVerified")) && (h.c(paramContext) != null)) {
        com.truecaller.wizard.b.c.a(true);
      } else {
        a("wizardStep", 0L);
      }
    }
    if ((((String)localObject2).compareTo("4.34") < 0) && (!am.a(a("language", "")))) {
      a(paramContext, d.a(d.d()));
    }
    if (((String)localObject2).compareTo("4.40") < 0)
    {
      a("hasShownWelcome", true);
      b("countryHash", "37e8d09fd4a669e5d4b3337e926b76ce");
    }
    if (((String)localObject2).compareTo("5.10") < 0)
    {
      localObject4 = new f(paramContext);
      a("notificationsSeenCount", f.b(((f)localObject4).l()).size() - ((f)localObject4).f());
      f("certValidationError");
      f("toast");
      f("theme_name");
      f("toastDuration");
      a("FEEDBACK_DISMISSED_COUNT", 0L);
      if (e("GOOGLE_REVIEW_DONE")) {
        a("FEEDBACK_LIKES_TRUECALLER", true);
      } else {
        a("FEEDBACK_LIKES_TRUECALLER", false);
      }
      a("HAS_SHARED", false);
    }
    if (((String)localObject2).compareTo("5.30") < 0) {
      localc.b("clipboardSearchEnabled", true);
    }
    if (((String)localObject2).compareTo("5.40") < 0)
    {
      paramContext.deleteDatabase("truecaller.data.History.s3db");
      paramContext.deleteDatabase("truecaller.data.CallersPb.s3db");
    }
    if (((String)localObject2).compareTo("5.81") < 0) {
      paramContext.deleteDatabase("TC.logview.3.11.s3db");
    }
    if (((String)localObject2).compareTo("6.03") < 0)
    {
      a("alwaysDownloadImages", true);
      paramContext.deleteDatabase("BlockedSms.s3db");
      localObject4 = ((bp)localObject1).R();
      if (c("TOP_SPAMMERS_SETTINGS") > 0) {
        bool1 = true;
      } else {
        bool1 = false;
      }
      ((p)localObject4).f(bool1);
      if (c("UNKNOWN_SETTINGS") > 0) {
        bool1 = true;
      } else {
        bool1 = false;
      }
      ((p)localObject4).a(bool1);
      if (com.truecaller.common.b.a.F().p()) {
        AvailableTagsDownloadWorker.e();
      }
    }
    if (((String)localObject2).compareTo("6.09") < 0)
    {
      localObject4 = paramContext.getSharedPreferences("TC.settings.3.0.beta5", 0).getAll().entrySet().iterator();
      while (((Iterator)localObject4).hasNext())
      {
        Object localObject5 = (Map.Entry)((Iterator)localObject4).next();
        String str = (String)((Map.Entry)localObject5).getKey();
        localObject5 = ((Map.Entry)localObject5).getValue();
        if ((localObject5 instanceof String)) {
          b(str, (String)localObject5);
        } else if ((localObject5 instanceof Boolean)) {
          a(str, ((Boolean)localObject5).booleanValue());
        } else if ((localObject5 instanceof Long)) {
          a(str, ((Long)localObject5).longValue());
        } else if ((localObject5 instanceof Integer)) {
          a(str, ((Integer)localObject5).intValue());
        }
      }
    }
    if (((String)localObject2).compareTo("6.17") < 0)
    {
      if (c("wizardStep") >= 3) {
        com.truecaller.wizard.b.c.a(true);
      }
      paramContext.deleteDatabase("truecaller.data.NameSuggestion.s3db");
      ((com.truecaller.common.b.a)localObject3).u().w();
      localObject3 = com.truecaller.common.e.e.c();
      if ((localObject3 != null) && (am.a(aa.b, a("language", "")))) {
        bool1 = true;
      } else {
        bool1 = false;
      }
      a("languageAuto", bool1);
      if (com.truecaller.common.b.a.F().p()) {
        ((bp)localObject1).Q().b();
      }
    }
    if (((String)localObject2).compareTo("6.21") < 0) {
      paramContext.deleteDatabase("truecaller.data.cms.s3db");
    }
    if (((String)localObject2).compareTo("6.24") < 0) {
      AvailableTagsDownloadWorker.e();
    }
    if (((String)localObject2).compareTo("6.40") < 0)
    {
      if (localc.b("hasNativeDialerCallerId")) {
        ((bp)localObject1).aI().h();
      }
      f("clipboardSearchTimeout");
    }
    if (((String)localObject2).compareTo("6.50") < 0)
    {
      if (TextUtils.equals(locala.a("profileAcceptAuto"), "1")) {
        locala.a("profileAcceptAuto", "0");
      }
      localc.b("blockCallMethod", am.f(a("blockCallMode", "")));
      f("blockCallMode");
    }
    if (((String)localObject2).compareTo("7.00") < 0)
    {
      f("DISPLAY_CALL_TAB");
      a("availability_enabled", true);
    }
    if (((String)localObject2).compareTo("7.01") < 0) {
      f("CHECK_DEVICE_ID");
    }
    if (((String)localObject2).compareTo("7.10") <= 0)
    {
      if (((bp)localObject1).aI().i() != null) {
        bool1 = true;
      } else {
        bool1 = false;
      }
      locala.b("IS_PREALOAD_BUILD", bool1);
    }
    if ((((String)localObject2).compareTo("7.20") < 0) && (g(paramContext))) {
      a("hasTruedialerIntegration", true);
    }
    if (((String)localObject2).compareTo("7.28") < 0)
    {
      localObject3 = locala.a("profileCountryIso");
      localObject4 = a("codeName", "");
      if ((TextUtils.isEmpty((CharSequence)localObject3)) && (!TextUtils.isEmpty((CharSequence)localObject4))) {
        locala.a("profileCountryIso", (String)localObject4);
      }
      f("codeName");
    }
    if (((String)localObject2).compareTo("7.30") < 0)
    {
      paramContext.deleteDatabase("adKeywords.db");
      a("hasShownWelcome", false);
    }
    int i = 0;
    if (((String)localObject2).compareTo("7.50") < 0)
    {
      f("INMOBI_ID");
      f("INVITE_PEOPLE_LAST_DISMISSED");
      f("INVITE_PEOPLE_DISMISSED");
      f("clearNativeCallLog");
      f("nudgeEnableTopSpammersCounter");
      f("blockHintCounter");
      f("updatePhonebookJobLastRun");
      f("linkedinLoggedIn");
      f("firstSearchDone");
      f("counterLoyalUser");
      f("dualSimSlotId");
      f("dualSimProviderField");
      f("dualSimProviderIndexing");
      f("ui_lang");
      f("counterLoyalUser");
      f("click_item_action_dialer");
      f("multi_sim_call_log_sim_field");
      f("multi_sim_call_log_sim_indexing");
      f("selected_theme");
      f("has_cleared_using_backspace_count");
      f("hasShownRatingDialog");
      f("ratingDialogDate");
      f("hasShownInviteDialog");
      f("inviteDialogDate");
      f("hasPlusOned");
      f("plusOneDialogDate");
      f("force_show_rate");
      f("force_show_invite");
      f("force_show_google_plus");
      paramContext.deleteDatabase("truecaller.data.CommonConnectionsListDao.s3db");
      paramContext.deleteDatabase("truecaller.data.Whitelist.s3db");
    }
    if (k <= 435)
    {
      b("version", "10.41.6");
      paramContext.deleteDatabase("truecaller.data.LogCounterEvent.s3db");
      paramContext.deleteDatabase("truecaller.data.LogEvent.s3db");
      n("batchLoggingBatchId");
      n("batchLoggingBatchSize");
      n("checkIfLogEventCountersLastRun");
      n("key_show_ringtone_onboarding");
      a("enhancedNotificationsEnabled", true);
      b("callLogTapBehavior", "call");
      localc.d("lastCallMadeWithTcTime");
      localc.d("lastDialerPromotionTime");
      n("dialerTipsShownCount");
    }
    if (k < 450)
    {
      a("showMissedCallsNotifications", TrueApp.y().a().bw().d());
      n("showAlternativeMissedCallNotification");
      n("removeDoubleMissedCallNotifications");
    }
    if (k < 454) {
      a("showMissedCallReminders", true);
    }
    if (k < 1300)
    {
      f("blockUpdateLastPressed");
      f("blockUpdateLastPerformed");
      f("blockUpdateCount");
      f("blockUpdateCountLastIncremented");
      f("regionCode");
      f("TC_SEARCH_TIMESTAMP");
      f("callerIdTheme");
      if (ae.b(paramContext)) {
        ((com.truecaller.callhistory.a)((bp)localObject1).ad().a()).g();
      }
    }
    if (k < 1314)
    {
      f("last_successful_availability_update");
      f("last_successful_time_zone_update");
      f("key_busy_reason");
    }
    if (k < 1318) {
      SyncPhoneBookService.a(paramContext, true);
    }
    if (k < 1335)
    {
      a("flash_enabled", j("availability_enabled"));
      localObject2 = paramContext.getDatabasePath("missed_calls.db");
      if (((File)localObject2).exists()) {
        ((File)localObject2).delete();
      }
      localObject2 = paramContext.getDatabasePath("missed_calls.db-journal");
      if (((File)localObject2).exists()) {
        ((File)localObject2).delete();
      }
    }
    if ((k == 1335) || (k == 1336)) {
      while (i < 2)
      {
        localObject2 = new String[] { "afterCallWarnFriends", "afterCallPromoteTcCounter" }[i];
        try
        {
          int m = b.getInt((String)localObject2, Integer.MIN_VALUE);
          if (m != Integer.MIN_VALUE) {
            b.edit().putLong((String)localObject2, m).apply();
          }
        }
        catch (ClassCastException localClassCastException)
        {
          for (;;) {}
        }
        i += 1;
      }
    }
    if (k <= 1340) {
      f("blockCount");
    }
    if (k <= 1358)
    {
      f("FEEDBACK_PLUS_ONE_FIRST_CHECKED");
      f("FEEDBACK_PLUS_ONE_DONE");
      f("FEEDBACK_PLUS_ONE_DISMISS_COUNT");
    }
    if (j <= 0)
    {
      new i(paramContext).b();
      f("PROFILE_MANUALLY_DEACTIVATED");
      f("updatePhonebookTimestamp");
      f("updatePhonebookEnabled");
      f("syncPictures");
      f("syncPicturesOverwrite");
      f("facebookFriendsTimestamp");
      f("linkedinFriendsTimestamp");
      f("googleFriendsTimestamp");
      f("twitterFriendsTimestamp");
      f("whatsNewDialogShownTimestamp");
      f("whatsNewDialogShownTimes");
      f("key_has_shown_default_dialer_sticky");
      f("showDefaultDialerPopupAfterDial");
      f("forceDefaultDialerPopup");
      f("key_has_shown_truecaller_notification");
      f("key_force_show_truecaller_notification");
      f("key_truecaller_notification_click_count");
      f("key_has_shown_identify_unknown_senders");
      f("last_banner_dismiss_timestamp");
      paramContext.deleteDatabase("TC.friend.2.90.s3db");
      paramContext.deleteDatabase("truecaller.data.automataStorage.s3db");
      f("featureDisableOnboarding");
      f("dialerPromotionStartTime");
      f("callerIdHintCount");
    }
    if (j < 2)
    {
      f("suppressAftercall");
      f("callerIdDialerPromoFirstShow");
      f("callerIdDialerPromoLastShow");
    }
    if (j < 3) {
      f("referralsDisabledUntil");
    }
    if ((j < 4) && (c.g.b.k.a("alphaRelease", "release"))) {
      com.truecaller.common.b.e.b("android.permission.WRITE_EXTERNAL_STORAGE");
    }
    if (j < 5)
    {
      com.truecaller.common.b.e.b("shortcutsInboxShownTimes");
      com.truecaller.common.b.e.b("general_requestPinMessagesShortcutShown");
    }
    if (j < 6) {
      com.truecaller.common.b.e.b("HAS_INVITED");
    }
    if (j < 7)
    {
      com.truecaller.common.b.e.b("PromoReferralDismissCount", c("Promo{Referral}DismissCount"));
      com.truecaller.common.b.e.b("PromoDefaultsmsDismissCount", c("Promo{Defaultsms}DismissCount"));
      com.truecaller.common.b.e.b("PromoBuyproDismissCount", c("Promo{Buypro}DismissCount"));
      com.truecaller.common.b.e.b("Promo{Referral}DismissCount");
      com.truecaller.common.b.e.b("Promo{Defaultsms}DismissCount");
      com.truecaller.common.b.e.b("Promo{Buypro}DismissCount");
      com.truecaller.common.b.e.b("home_screen_banner_close_count");
    }
    if (j < 8)
    {
      n("lastDialerPromotionInteractionTime_onboarding");
      n("lastDialerPromotionInteractionTime_frequentlyCalled");
      n("lastDialerPromotionInteractionTime_missed");
      n("lastDialerPromotionInteractionTime_outgoingUnanswered");
      n("lastDialerPromotionInteractionTime_incoming");
    }
    if (j < 9) {
      ((bp)localObject1).cv().a().d();
    }
    if (j < 10)
    {
      locala.d("edgeLocationsLastRequestTime");
      locala.d("edgeLocationsExpiration");
      n("profileNumberBackEnd");
    }
    if (j < 11)
    {
      localObject1 = com.truecaller.common.b.e.a("wizard_StartPage");
      if ((((String)localObject1).equals("Page_CallVerification")) || (((String)localObject1).equals("Page_SmsVerification"))) {
        com.truecaller.common.b.e.b("wizard_StartPage");
      }
    }
    if (j < 12)
    {
      locala.d("featureAdUnifiedSearchHistory");
      locala.d("featureAdUnifiedBlock");
      locala.d("featureAdUnifiedCallLog");
      locala.d("featureAdUnifiedInbox");
    }
    if (j < 13) {
      locala.d("presenceSettingNeedSync");
    }
    if (j < 15) {
      RefreshT9MappingService.a(paramContext);
    }
    if (j < 16) {
      locala.d("featureBusinessSuggestion");
    }
    if (j < 17) {
      n("HeartBeatLastTime");
    }
    if (j < 18)
    {
      locala.d("filter_scheduledFilterSyncingEnabled");
      locala.d("filter_settingsLastVisitTimestamp");
    }
    if (j < 19) {
      localc.b("whatsAppCallsEnabled", true);
    }
    if (j < 20) {
      locala.d("filter_filterJustActivated");
    }
    if (j < 21)
    {
      n("call_counter");
      n("lastCallMeBackTime");
      n("MsgMastSyncTime");
    }
    if (j < 22) {
      locala.b("smart_notifications", true);
    }
    if (j < 23) {
      n("backupWhatsNewShown");
    }
    if (j < 24) {
      n("featureShowOptInReadMore");
    }
    if (j < 25)
    {
      n("debugLoggingUploadTriggered");
      n("lastTracingFeatureTime");
    }
    if (j < 26) {
      n("initializeJobLastRun");
    }
    if (j < 27)
    {
      n("EmojiBarTipWasShown");
      n("EmojiBarEverUsed");
      n("ConversationScreenOpenCount");
    }
    if (j < 28)
    {
      if (j("backupNeedsSync")) {
        EnhancedSearchStateWorker.a(j("backupSyncValue"));
      }
      n("backupNeedsSync");
      n("backupSyncValue");
    }
    if (j < 29) {
      n("UNUSED_DIRECTORIES_DELETED_ON_UPGRADE");
    }
    if (j < 30)
    {
      FilterUploadWorker.b();
      n("filter_filtersRestored");
    }
    if (j < 31) {
      n("imTooltipShown");
    }
    a("global_settings_ver", 31L);
  }
  
  public static void f(String paramString)
  {
    c.remove(paramString);
    if (a) {
      return;
    }
    n();
  }
  
  public static boolean f()
  {
    return (((bk)com.truecaller.common.b.a.F()).a().I().a("featureAvailability", false)) && (e("availability_enabled"));
  }
  
  public static void g(String paramString)
  {
    a(paramString, System.currentTimeMillis());
  }
  
  public static boolean g()
  {
    return (((bk)com.truecaller.common.b.a.F()).a().I().a("featureFlash", false)) && (e("flash_enabled"));
  }
  
  private static boolean g(Context paramContext)
  {
    Object localObject = Settings.BuildName.toBuildName(((bk)paramContext.getApplicationContext()).a().aI().f());
    if ((localObject != null) && (!TextUtils.isEmpty(((Settings.BuildName)localObject).getPackageName())))
    {
      localObject = ((Settings.BuildName)localObject).getPackageName().replace("truecaller", "truedialer");
      return paramContext.getPackageManager().hasSystemFeature((String)localObject);
    }
    return false;
  }
  
  private static int h(Context paramContext)
  {
    String str2 = a("dialpad_feedback_index_str", "");
    String str1 = str2;
    if (TextUtils.isEmpty(str2)) {
      str1 = "-1";
    }
    int j = Integer.valueOf(str1).intValue();
    int i = j;
    if (j == -1)
    {
      i = Settings.System.getInt(paramContext.getContentResolver(), "haptic_feedback_enabled", 1);
      b("dialpad_feedback_index_str", String.valueOf(i));
    }
    return i;
  }
  
  public static void h(String paramString)
  {
    a(paramString, 0L);
  }
  
  public static boolean h()
  {
    com.truecaller.common.f.c localc = ((bk)com.truecaller.common.b.a.F()).a().ai();
    if (!e("qaForceAds"))
    {
      if (!localc.d())
      {
        long l = d("adsDisabledUntil").longValue();
        int i;
        if (l > 0L)
        {
          if (System.currentTimeMillis() - l < 0L) {
            i = 1;
          } else {
            i = 0;
          }
        }
        else {
          i = 0;
        }
        if ((i == 0) && (com.truecaller.common.b.a.F().p()) && (!i())) {
          return true;
        }
      }
      return false;
    }
    return true;
  }
  
  public static void i(String paramString)
  {
    a(paramString, d(paramString).longValue() + 1L);
  }
  
  public static boolean i()
  {
    return Settings.BuildName.toBuildName(com.truecaller.common.b.a.F().e().f()) == Settings.BuildName.WILEYFOX;
  }
  
  public static long j()
  {
    return d("blockCallCounter").longValue();
  }
  
  public static boolean j(String paramString)
  {
    return b.getBoolean(paramString, false);
  }
  
  @Deprecated
  public static String k()
  {
    String str = l();
    if (TextUtils.equals(str, "auto")) {
      return com.truecaller.common.e.f.a().getLanguage();
    }
    return str;
  }
  
  public static boolean k(String paramString)
  {
    return e(p(paramString));
  }
  
  public static long l(String paramString)
  {
    return d(o(paramString)).longValue();
  }
  
  public static String l()
  {
    return b.getString("t9_lang", "auto");
  }
  
  public static int m()
  {
    return a("contact_count", -1);
  }
  
  public static void m(String paramString)
  {
    a(p(paramString), true);
  }
  
  private static void n()
  {
    a = false;
    c.apply();
  }
  
  public static void n(String paramString)
  {
    if (!b.contains(paramString)) {
      return;
    }
    c.remove(paramString);
    if (a) {
      return;
    }
    n();
  }
  
  private static String o(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder("truecaller.alarm.notification.");
    localStringBuilder.append(paramString);
    localStringBuilder.append(".set");
    return localStringBuilder.toString();
  }
  
  private static String p(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder("truecaller.alarm.notification.");
    localStringBuilder.append(paramString);
    localStringBuilder.append(".fired");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.access.Settings
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */