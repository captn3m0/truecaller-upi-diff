package com.truecaller;

import android.content.Context;
import com.truecaller.premium.br;
import com.truecaller.premium.bt;
import com.truecaller.startup_dialogs.a.aa;
import com.truecaller.startup_dialogs.a.ab;
import com.truecaller.startup_dialogs.a.ac;
import com.truecaller.startup_dialogs.a.ad;
import com.truecaller.startup_dialogs.a.ae;
import com.truecaller.startup_dialogs.a.ah;
import com.truecaller.startup_dialogs.a.ak;
import com.truecaller.startup_dialogs.a.am;
import com.truecaller.startup_dialogs.a.an;
import com.truecaller.startup_dialogs.a.ao;
import com.truecaller.startup_dialogs.a.ap;
import com.truecaller.startup_dialogs.a.aq;
import com.truecaller.startup_dialogs.a.at;
import com.truecaller.startup_dialogs.a.av;
import com.truecaller.startup_dialogs.a.aw;
import com.truecaller.startup_dialogs.a.ax;
import com.truecaller.startup_dialogs.a.ay;
import com.truecaller.startup_dialogs.a.az;
import com.truecaller.startup_dialogs.a.b;
import com.truecaller.startup_dialogs.a.ba;
import com.truecaller.startup_dialogs.a.i;
import com.truecaller.startup_dialogs.a.j;
import com.truecaller.startup_dialogs.a.k;
import com.truecaller.startup_dialogs.a.l;
import com.truecaller.startup_dialogs.a.n;
import com.truecaller.startup_dialogs.a.o;
import com.truecaller.startup_dialogs.a.p;
import com.truecaller.startup_dialogs.a.q;
import com.truecaller.startup_dialogs.a.r;
import com.truecaller.startup_dialogs.a.s;
import com.truecaller.startup_dialogs.a.t;
import com.truecaller.startup_dialogs.a.u;
import com.truecaller.startup_dialogs.a.v;
import com.truecaller.startup_dialogs.a.x;
import com.truecaller.startup_dialogs.a.y;
import com.truecaller.startup_dialogs.a.z;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.whoviewedme.w;
import javax.inject.Provider;

final class be$t
  implements com.truecaller.ui.af
{
  private Provider<com.truecaller.startup_dialogs.d> A = com.truecaller.startup_dialogs.e.a(be.G(a), c, d, f, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, be.s(a));
  private Provider<com.truecaller.startup_dialogs.c> B = dagger.a.c.a(A);
  private Provider<com.truecaller.g.a> C = dagger.a.c.a(com.truecaller.g.d.a(b));
  private final com.truecaller.g.c b = new com.truecaller.g.c();
  private Provider<com.truecaller.startup_dialogs.a.a> c = b.a(be.h(a), be.i(a), be.j(a), be.k(a), be.l(a));
  private Provider<av> d = aw.a(be.m(a), be.i(a), be.j(a));
  private Provider<at> e = com.truecaller.startup_dialogs.a.au.a(be.n(a));
  private Provider<az> f = ba.a(be.i(a), e, com.truecaller.startup_dialogs.h.a(), be.j(a));
  private Provider<com.truecaller.old.data.access.f> g = com.truecaller.startup_dialogs.g.a(be.o(a));
  private Provider<ak> h = am.a(be.i(a), g, be.j(a), com.truecaller.startup_dialogs.f.a());
  private Provider<com.truecaller.startup_dialogs.a.e> i = com.truecaller.startup_dialogs.a.f.a(be.p(a), be.q(a), be.r(a), be.h(a), be.s(a), be.t(a), be.u(a));
  private Provider<v> j = x.a(be.v(a), be.w(a), be.i(a), be.j(a));
  private Provider<n> k = o.a(be.h(a), be.r(a), be.i(a), be.j(a));
  private Provider<y> l = z.a(be.w(a), be.x(a), be.i(a), be.j(a));
  private Provider<i> m = j.a(be.y(a), be.w(a), be.i(a), be.j(a));
  private Provider<p> n = q.a(be.w(a), be.x(a), be.i(a), be.j(a));
  private Provider<com.truecaller.startup_dialogs.a.g> o = com.truecaller.startup_dialogs.a.h.a(be.y(a), be.w(a), be.i(a), be.j(a));
  private Provider<t> p = u.a(be.h(a), be.y(a), be.i(a), be.j(a));
  private Provider<com.truecaller.startup_dialogs.a.ag> q = ah.a(be.i(a), be.a(a), be.j(a), be.z(a), be.p(a), bt.a(), be.A(a), be.B(a));
  private Provider<aa> r = ab.a(be.w(a), be.i(a), be.r(a));
  private Provider<com.truecaller.startup_dialogs.a.c> s = com.truecaller.startup_dialogs.a.d.a(be.a(a), be.C(a), be.v(a), be.r(a), be.i(a), be.D(a));
  private Provider<ap> t = aq.a(be.a(a), be.C(a), be.i(a));
  private Provider<ae> u = com.truecaller.startup_dialogs.a.af.a(be.i(a), be.E(a), be.v(a));
  private Provider<an> v = ao.a(be.i(a), be.a(a), be.j(a), be.z(a));
  private Provider<ax> w = ay.a(be.m(a), be.x(a), be.i(a), be.j(a));
  private Provider<r> x = s.a(be.i(a));
  private Provider<ac> y = ad.a(be.A(a), be.i(a), be.j(a), bt.a());
  private Provider<k> z = l.a(be.j(a), be.p(a), be.F(a), bt.a());
  
  private be$t(be parambe) {}
  
  public final void a(TruecallerInit paramTruecallerInit)
  {
    o = ((com.truecaller.startup_dialogs.c)B.get());
    p = ((com.truecaller.g.a)C.get());
    q = ((com.truecaller.analytics.au)be.H(a).get());
    r = new br();
    s = ((com.truecaller.analytics.a.f)dagger.a.g.a(be.I(a).b(), "Cannot return null from a non-@Nullable component method"));
    t = new com.truecaller.ui.ag((w)be.g(a).get(), com.truecaller.startup_dialogs.g.a((Context)dagger.a.g.a(be.f(a).b(), "Cannot return null from a non-@Nullable component method")), (c.d.f)dagger.a.g.a(be.f(a).t(), "Cannot return null from a non-@Nullable component method"), (c.d.f)dagger.a.g.a(be.f(a).r(), "Cannot return null from a non-@Nullable component method"));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.be.t
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */