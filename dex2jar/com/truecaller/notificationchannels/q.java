package com.truecaller.notificationchannels;

import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build.VERSION;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;

public final class q
  implements p
{
  private final NotificationManager a;
  private final List<m> b;
  
  @Inject
  public q(Context paramContext, f paramf, c paramc, k paramk)
  {
    a = ((NotificationManager)paramContext.getSystemService("notification"));
    b = c.a.m.b(new m[] { (m)paramf, (m)paramc, (m)paramk });
  }
  
  public final void a()
  {
    if (Build.VERSION.SDK_INT < 26) {
      return;
    }
    Object localObject = a;
    int i = 0;
    if (localObject != null)
    {
      localObject = ((NotificationManager)localObject).getNotificationChannels();
      if (!((List)localObject).isEmpty()) {
        if (((List)localObject).size() == 1)
        {
          localObject = (NotificationChannel)((List)localObject).get(0);
          c.g.b.k.a(localObject, "channel");
          if (c.g.b.k.a(((NotificationChannel)localObject).getId(), "miscellaneous")) {}
        }
        else
        {
          i = 1;
        }
      }
    }
    if (i != 0) {
      return;
    }
    b();
  }
  
  public final void b()
  {
    Iterator localIterator = ((Iterable)b).iterator();
    while (localIterator.hasNext()) {
      ((m)localIterator.next()).b();
    }
    localIterator = ((Iterable)b).iterator();
    while (localIterator.hasNext()) {
      ((m)localIterator.next()).c();
    }
  }
  
  public final void c()
  {
    NotificationManager localNotificationManager = a;
    if (localNotificationManager == null) {
      return;
    }
    if (Build.VERSION.SDK_INT < 26) {
      return;
    }
    Object localObject1 = localNotificationManager.getNotificationChannelGroups();
    c.g.b.k.a(localObject1, "notificationManager.notificationChannelGroups");
    localObject1 = ((Iterable)localObject1).iterator();
    Object localObject2;
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (NotificationChannelGroup)((Iterator)localObject1).next();
      c.g.b.k.a(localObject2, "it");
      localNotificationManager.deleteNotificationChannelGroup(((NotificationChannelGroup)localObject2).getId());
    }
    localObject1 = localNotificationManager.getNotificationChannels();
    c.g.b.k.a(localObject1, "notificationManager.notificationChannels");
    localObject1 = ((Iterable)localObject1).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (NotificationChannel)((Iterator)localObject1).next();
      c.g.b.k.a(localObject2, "it");
      if (!c.g.b.k.a(((NotificationChannel)localObject2).getId(), "miscellaneous")) {
        localNotificationManager.deleteNotificationChannel(((NotificationChannel)localObject2).getId());
      }
    }
    b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notificationchannels.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */