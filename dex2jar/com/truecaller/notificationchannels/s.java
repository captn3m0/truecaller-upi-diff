package com.truecaller.notificationchannels;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class s
  implements d<q>
{
  private final Provider<Context> a;
  private final Provider<f> b;
  private final Provider<c> c;
  private final Provider<k> d;
  
  private s(Provider<Context> paramProvider, Provider<f> paramProvider1, Provider<c> paramProvider2, Provider<k> paramProvider3)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static s a(Provider<Context> paramProvider, Provider<f> paramProvider1, Provider<c> paramProvider2, Provider<k> paramProvider3)
  {
    return new s(paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notificationchannels.s
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */