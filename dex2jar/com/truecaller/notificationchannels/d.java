package com.truecaller.notificationchannels;

import android.content.Context;
import javax.inject.Provider;

public final class d
  implements dagger.a.d<c>
{
  private final Provider<Context> a;
  private final Provider<e> b;
  
  private d(Provider<Context> paramProvider, Provider<e> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static d a(Provider<Context> paramProvider, Provider<e> paramProvider1)
  {
    return new d(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notificationchannels.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */