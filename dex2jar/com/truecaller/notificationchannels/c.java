package com.truecaller.notificationchannels;

import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.content.Context;
import c.a.m;
import c.g.b.k;
import java.util.List;
import javax.inject.Inject;

public final class c
  extends a
  implements b
{
  private final String c;
  private final String d;
  private final String e;
  private final String f;
  private final String g;
  private final Context h;
  private final e i;
  
  @Inject
  public c(Context paramContext, e parame)
  {
    super(paramContext);
    h = paramContext;
    i = parame;
    c = "missed_calls";
    d = "missed_calls_reminder";
    e = "blocked_calls";
    f = "push_caller_id";
    g = "phone_calls";
  }
  
  public final String X_()
  {
    return c;
  }
  
  public final String d()
  {
    return d;
  }
  
  public final List<NotificationChannel> e()
  {
    NotificationChannel localNotificationChannel1 = new NotificationChannel(c, (CharSequence)h.getString(R.string.notification_channels_channel_missed_calls), 2);
    localNotificationChannel1.setDescription(h.getString(R.string.notification_channels_channel_description_missed_calls));
    localNotificationChannel1.enableLights(true);
    localNotificationChannel1.setLightColor(Z_());
    localNotificationChannel1.setGroup("calls");
    NotificationChannel localNotificationChannel2 = new NotificationChannel(d, (CharSequence)h.getString(R.string.notification_channels_channel_missed_calls_reminder), 3);
    localNotificationChannel2.setDescription(h.getString(R.string.notification_channels_channel_description_missed_calls_reminder));
    localNotificationChannel2.enableLights(true);
    localNotificationChannel2.setLightColor(Z_());
    localNotificationChannel2.setGroup("calls");
    NotificationChannel localNotificationChannel3 = new NotificationChannel(e, (CharSequence)h.getString(R.string.notification_channels_channel_blocked_calls), 2);
    localNotificationChannel3.setDescription(h.getString(R.string.notification_channels_channel_description_blocked_calls));
    localNotificationChannel3.setGroup("calls");
    NotificationChannel localNotificationChannel4 = new NotificationChannel(f, (CharSequence)h.getString(R.string.notification_channels_channel_push_caller_id), 4);
    localNotificationChannel4.setDescription(h.getString(R.string.notification_channels_channel_description_push_caller_id));
    localNotificationChannel4.setGroup("calls");
    NotificationChannel localNotificationChannel5 = new NotificationChannel(g, (CharSequence)h.getString(R.string.notification_channels_channel_phone_calls), 2);
    localNotificationChannel5.setDescription(h.getString(R.string.notification_channels_channel_description_phone_calls));
    localNotificationChannel5.enableLights(true);
    localNotificationChannel5.setLightColor(Z_());
    localNotificationChannel5.setGroup("calls");
    return m.b(new NotificationChannel[] { localNotificationChannel1, localNotificationChannel2, localNotificationChannel3, localNotificationChannel4, localNotificationChannel5 });
  }
  
  public final List<NotificationChannelGroup> f()
  {
    return m.a(new NotificationChannelGroup("calls", (CharSequence)h.getString(R.string.notification_channels_group_calls)));
  }
  
  public final String g()
  {
    return e;
  }
  
  public final String h()
  {
    String str2 = i.a();
    if (!Y_()) {
      return str2;
    }
    Object localObject = b;
    String str1 = str2;
    if (localObject != null)
    {
      localObject = ((NotificationManager)localObject).getNotificationChannel("voip");
      str1 = str2;
      if (localObject != null)
      {
        str1 = ((NotificationChannel)localObject).getId();
        if (str1 == null) {
          return str2;
        }
      }
    }
    return str1;
  }
  
  public final String i()
  {
    return f;
  }
  
  public final String j()
  {
    return g;
  }
  
  public final void k()
  {
    if (!Y_()) {
      return;
    }
    if ((k.a(h(), "voip") ^ true))
    {
      NotificationManager localNotificationManager = b;
      if (localNotificationManager != null)
      {
        NotificationChannel localNotificationChannel = new NotificationChannel("voip", (CharSequence)h.getString(R.string.notification_channels_channel_voip), 2);
        localNotificationChannel.setDescription(h.getString(R.string.notification_channels_channel_description_voip, new Object[] { h.getString(R.string.voip_text) }));
        localNotificationChannel.setGroup("calls");
        localNotificationManager.createNotificationChannel(localNotificationChannel);
        return;
      }
    }
  }
  
  public final void l()
  {
    if (!Y_()) {
      return;
    }
    if (k.a(h(), "voip"))
    {
      NotificationManager localNotificationManager = b;
      if (localNotificationManager != null)
      {
        localNotificationManager.deleteNotificationChannel("voip");
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notificationchannels.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */