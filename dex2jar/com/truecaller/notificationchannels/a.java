package com.truecaller.notificationchannels;

import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build.VERSION;
import c.f;
import c.g;
import java.util.List;

public abstract class a
  implements m
{
  final NotificationManager b;
  private final f c;
  
  public a(Context paramContext)
  {
    b = ((NotificationManager)paramContext.getSystemService("notification"));
    c = g.a((c.g.a.a)new a.a(paramContext));
  }
  
  protected static boolean Y_()
  {
    return Build.VERSION.SDK_INT >= 26;
  }
  
  protected final int Z_()
  {
    return ((Number)c.b()).intValue();
  }
  
  public final void b()
  {
    if (!Y_()) {
      return;
    }
    NotificationManager localNotificationManager = b;
    if (localNotificationManager == null) {
      return;
    }
    localNotificationManager.createNotificationChannelGroups(f());
  }
  
  public final void c()
  {
    if (!Y_()) {
      return;
    }
    NotificationManager localNotificationManager = b;
    if (localNotificationManager == null) {
      return;
    }
    localNotificationManager.createNotificationChannels(e());
  }
  
  public abstract List<NotificationChannel> e();
  
  public abstract List<NotificationChannelGroup> f();
}

/* Location:
 * Qualified Name:     com.truecaller.notificationchannels.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */