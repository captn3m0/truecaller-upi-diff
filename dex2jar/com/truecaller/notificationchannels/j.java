package com.truecaller.notificationchannels;

import android.annotation.TargetApi;
import android.net.Uri;

public abstract interface j
  extends m
{
  public abstract String a();
  
  public abstract String d();
  
  public abstract String g();
  
  public abstract Uri h();
  
  public abstract boolean i();
  
  @TargetApi(26)
  public abstract void j();
}

/* Location:
 * Qualified Name:     com.truecaller.notificationchannels.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */