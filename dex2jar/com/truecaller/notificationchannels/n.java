package com.truecaller.notificationchannels;

import android.content.Context;
import c.g.b.k;

public abstract interface n
{
  public static final a a = a.a;
  
  public abstract p b();
  
  public abstract e c();
  
  public abstract b d();
  
  public abstract j e();
  
  public static final class a
  {
    public static n a(Context paramContext, h paramh)
    {
      k.b(paramContext, "context");
      paramContext = i.a().a(paramh).a(paramContext).a();
      k.a(paramContext, "DaggerNotificationChanne…\n                .build()");
      return (n)paramContext;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notificationchannels.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */