package com.truecaller.notificationchannels;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.AudioAttributes.Builder;
import android.net.Uri;
import android.os.Build.VERSION;
import c.a.m;
import c.j.c;
import java.util.List;
import javax.inject.Inject;

public final class k
  extends a
  implements j
{
  private final SharedPreferences c;
  private final String d;
  private final String e;
  private final Context f;
  private final h g;
  
  @Inject
  public k(Context paramContext, h paramh)
  {
    super(paramContext);
    f = paramContext;
    g = paramh;
    paramContext = f.getSharedPreferences("notifications.settings", 0);
    c.g.b.k.a(paramContext, "context.getSharedPrefere…E, Activity.MODE_PRIVATE)");
    c = paramContext;
    d = "spam_sms";
    e = "blocked_sms";
  }
  
  @TargetApi(26)
  private final NotificationChannel a(String paramString1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean1, boolean paramBoolean2, String paramString2, Uri paramUri)
  {
    paramString1 = new NotificationChannel(paramString1, (CharSequence)f.getString(paramInt1), paramInt3);
    paramString1.setDescription(f.getString(paramInt2));
    paramString1.enableLights(paramBoolean1);
    paramString1.enableVibration(paramBoolean2);
    paramString1.setLightColor(paramInt4);
    paramString1.setGroup(paramString2);
    paramString1.setSound(paramUri, new AudioAttributes.Builder().setContentType(4).setUsage(5).build());
    return paramString1;
  }
  
  @TargetApi(26)
  private final String a(int paramInt, Uri paramUri, boolean paramBoolean)
  {
    String str = m();
    if (paramInt != c.getInt("non_spam_sms_settings_hash_key", -1))
    {
      Object localObject1 = new StringBuilder("non_spam_sms_v2");
      Object localObject2 = c.c;
      ((StringBuilder)localObject1).append(c.d().b());
      localObject1 = ((StringBuilder)localObject1).toString();
      paramUri = a(this, (String)localObject1, R.string.notification_channels_channel_non_spam_sms, R.string.notification_channels_channel_description_non_spam_sms, 5, 0, false, paramBoolean, null, paramUri, 176);
      localObject2 = b;
      if (localObject2 != null)
      {
        ((NotificationManager)localObject2).deleteNotificationChannel(str);
        ((NotificationManager)localObject2).createNotificationChannel(paramUri);
      }
      c.edit().putString("non_spam_sms_channel_id_key", (String)localObject1).putInt("non_spam_sms_settings_hash_key", paramInt).apply();
      return (String)localObject1;
    }
    return str;
  }
  
  private Uri k()
  {
    h localh = g;
    if (localh != null) {
      return localh.a();
    }
    return null;
  }
  
  private boolean l()
  {
    h localh = g;
    if (localh != null) {
      return localh.b();
    }
    return true;
  }
  
  private final String m()
  {
    String str2 = c.getString("non_spam_sms_channel_id_key", "non_spam_sms_v2");
    String str1 = str2;
    if (str2 == null) {
      str1 = "non_spam_sms_v2";
    }
    return str1;
  }
  
  public final String a()
  {
    return d;
  }
  
  public final String d()
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.provideAs(TypeTransformer.java:780)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.enexpr(TypeTransformer.java:659)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:719)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e2expr(TypeTransformer.java:632)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:716)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.s2stmt(TypeTransformer.java:820)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.sxStmt(TypeTransformer.java:843)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:206)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
  }
  
  @TargetApi(24)
  public final List<NotificationChannel> e()
  {
    return m.b(new NotificationChannel[] { a(this, d(), R.string.notification_channels_channel_non_spam_sms, R.string.notification_channels_channel_description_non_spam_sms, 5, 0, false, false, null, null, 496), a(this, d, R.string.notification_channels_channel_spam_sms, R.string.notification_channels_channel_description_spam_sms, 0, 0, false, false, null, null, 504), a(this, e, R.string.notification_channels_channel_blocked_sms, R.string.notification_channels_channel_description_blocked_sms, 0, 0, false, false, null, null, 472) });
  }
  
  public final List<NotificationChannelGroup> f()
  {
    return m.a(new NotificationChannelGroup("sms", (CharSequence)f.getString(R.string.notification_channels_group_sms)));
  }
  
  public final String g()
  {
    return e;
  }
  
  public final Uri h()
  {
    Uri localUri = k();
    if (Build.VERSION.SDK_INT >= 26)
    {
      Object localObject = m();
      NotificationManager localNotificationManager = b;
      if (localNotificationManager != null)
      {
        localObject = localNotificationManager.getNotificationChannel((String)localObject);
        if (localObject != null)
        {
          localObject = ((NotificationChannel)localObject).getSound();
          break label47;
        }
      }
      localObject = null;
      label47:
      if ((localObject != null) && ((c.g.b.k.a(localObject, localUri) ^ true))) {
        return (Uri)localObject;
      }
    }
    return localUri;
  }
  
  public final boolean i()
  {
    if (Build.VERSION.SDK_INT >= 26)
    {
      Object localObject = m();
      NotificationManager localNotificationManager = b;
      if (localNotificationManager != null)
      {
        localObject = localNotificationManager.getNotificationChannel((String)localObject);
        if (localObject != null)
        {
          localObject = Boolean.valueOf(((NotificationChannel)localObject).shouldVibrate());
          break label45;
        }
      }
      localObject = null;
      label45:
      if ((localObject != null) && ((c.g.b.k.a(localObject, Boolean.valueOf(l())) ^ true))) {
        return ((Boolean)localObject).booleanValue();
      }
    }
    return l();
  }
  
  @TargetApi(26)
  public final void j()
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.provideAs(TypeTransformer.java:780)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.enexpr(TypeTransformer.java:659)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:719)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e2expr(TypeTransformer.java:632)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:716)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.enexpr(TypeTransformer.java:698)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:719)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.s1stmt(TypeTransformer.java:810)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.sxStmt(TypeTransformer.java:840)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:206)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notificationchannels.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */