package com.truecaller.notificationchannels;

import android.content.Context;
import javax.inject.Provider;

public final class i
  implements o
{
  private Provider<Context> b;
  private Provider<f> c;
  private Provider<e> d;
  private Provider<c> e;
  private Provider<h> f;
  private Provider<k> g;
  private Provider<q> h;
  private Provider<p> i;
  private Provider<b> j;
  private Provider<j> k;
  
  private i(Context paramContext, h paramh)
  {
    b = dagger.a.e.a(paramContext);
    c = g.a(b);
    d = dagger.a.c.a(c);
    e = d.a(b, d);
    f = dagger.a.e.b(paramh);
    g = l.a(b, f);
    h = s.a(b, c, e, g);
    i = dagger.a.c.a(h);
    j = dagger.a.c.a(e);
    k = dagger.a.c.a(g);
  }
  
  public static o.a a()
  {
    return new a((byte)0);
  }
  
  public final p b()
  {
    return (p)i.get();
  }
  
  public final e c()
  {
    return (e)d.get();
  }
  
  public final b d()
  {
    return (b)j.get();
  }
  
  public final j e()
  {
    return (j)k.get();
  }
  
  static final class a
    implements o.a
  {
    private Context a;
    private h b;
    
    public final o a()
    {
      dagger.a.g.a(a, Context.class);
      return new i(a, b, (byte)0);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notificationchannels.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */