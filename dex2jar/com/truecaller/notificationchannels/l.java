package com.truecaller.notificationchannels;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class l
  implements d<k>
{
  private final Provider<Context> a;
  private final Provider<h> b;
  
  private l(Provider<Context> paramProvider, Provider<h> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static l a(Provider<Context> paramProvider, Provider<h> paramProvider1)
  {
    return new l(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notificationchannels.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */