package com.truecaller.notificationchannels;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d<f>
{
  private final Provider<Context> a;
  
  private g(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static g a(Provider<Context> paramProvider)
  {
    return new g(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notificationchannels.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */