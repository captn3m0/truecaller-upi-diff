package com.truecaller.notificationchannels;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.content.Context;
import c.a.m;
import c.a.y;
import java.util.List;
import javax.inject.Inject;

public final class f
  extends a
  implements e
{
  private final String c;
  private final String d;
  private final String e;
  private final String f;
  private final String g;
  private final Context h;
  
  @Inject
  public f(Context paramContext)
  {
    super(paramContext);
    h = paramContext;
    c = "miscellaneous_channel";
    d = "caller_id";
    e = "backup";
    f = "flash";
    g = "profile_share";
  }
  
  @TargetApi(26)
  private final NotificationChannel t()
  {
    NotificationChannel localNotificationChannel = new NotificationChannel("profile_views", (CharSequence)h.getString(R.string.notification_channels_channel_profile_views), 4);
    localNotificationChannel.setDescription(h.getString(R.string.notification_channels_channel_description_profile_views));
    localNotificationChannel.enableLights(true);
    localNotificationChannel.setLightColor(Z_());
    localNotificationChannel.enableVibration(true);
    localNotificationChannel.setVibrationPattern(new long[] { 500L, 100L, 500L });
    return localNotificationChannel;
  }
  
  @TargetApi(26)
  private final NotificationChannel u()
  {
    NotificationChannel localNotificationChannel = new NotificationChannel("engagement_rewards", (CharSequence)h.getString(R.string.notification_channels_channel_engagement_rewards), 4);
    localNotificationChannel.setDescription(h.getString(R.string.notification_channels_channel_engagement_rewards));
    return localNotificationChannel;
  }
  
  public final String a()
  {
    return c;
  }
  
  public final String d()
  {
    return d;
  }
  
  public final List<NotificationChannel> e()
  {
    NotificationChannel localNotificationChannel1 = new NotificationChannel("miscellaneous_channel", (CharSequence)h.getString(R.string.notification_channels_channel_miscellaneous), 2);
    localNotificationChannel1.setDescription(h.getString(R.string.notification_channels_channel_description_miscellaneous));
    localNotificationChannel1.enableLights(true);
    localNotificationChannel1.setLightColor(Z_());
    NotificationChannel localNotificationChannel2 = new NotificationChannel("caller_id", (CharSequence)h.getString(R.string.notification_channels_channel_caller_id), 1);
    localNotificationChannel2.setDescription(h.getString(R.string.notification_channels_channel_description_caller_id));
    NotificationChannel localNotificationChannel3 = new NotificationChannel("backup", (CharSequence)h.getString(R.string.notification_channels_channel_backup), 2);
    localNotificationChannel3.setDescription(h.getString(R.string.notification_channels_channel_description_backup));
    localNotificationChannel3.enableLights(true);
    localNotificationChannel3.setLightColor(Z_());
    NotificationChannel localNotificationChannel4 = new NotificationChannel("flash", (CharSequence)h.getString(R.string.notification_channels_channel_flash), 2);
    localNotificationChannel4.setDescription(h.getString(R.string.notification_channels_channel_description_flash));
    localNotificationChannel4.enableLights(true);
    localNotificationChannel4.setLightColor(Z_());
    NotificationChannel localNotificationChannel5 = new NotificationChannel("profile_share", (CharSequence)h.getString(R.string.notification_channels_channel_profile_share), 4);
    localNotificationChannel5.setDescription(h.getString(R.string.notification_channels_channel_description_profile_share));
    localNotificationChannel5.enableLights(true);
    localNotificationChannel5.setLightColor(Z_());
    localNotificationChannel5.enableVibration(true);
    localNotificationChannel5.setVibrationPattern(new long[] { 500L, 100L, 500L });
    return m.b(new NotificationChannel[] { localNotificationChannel1, localNotificationChannel2, localNotificationChannel3, localNotificationChannel4, localNotificationChannel5, t(), u() });
  }
  
  public final List<NotificationChannelGroup> f()
  {
    return (List)y.a;
  }
  
  public final String g()
  {
    return e;
  }
  
  public final String h()
  {
    return f;
  }
  
  public final String i()
  {
    return g;
  }
  
  public final String j()
  {
    if (!Y_()) {
      return null;
    }
    Object localObject = b;
    if (localObject == null) {
      return null;
    }
    localObject = ((NotificationManager)localObject).getNotificationChannel("truecaller_pay_v2");
    if (localObject != null) {
      return ((NotificationChannel)localObject).getId();
    }
    return null;
  }
  
  public final String k()
  {
    if (!Y_()) {
      return null;
    }
    Object localObject = b;
    if (localObject == null) {
      return null;
    }
    localObject = ((NotificationManager)localObject).getNotificationChannel("engagement_rewards");
    if (localObject != null) {
      return ((NotificationChannel)localObject).getId();
    }
    return null;
  }
  
  public final void l()
  {
    if (!Y_()) {
      return;
    }
    NotificationManager localNotificationManager = b;
    if (localNotificationManager != null)
    {
      NotificationChannel localNotificationChannel = new NotificationChannel("truecaller_pay_v2", (CharSequence)h.getString(R.string.notification_channels_channel_truecaller_pay), 4);
      localNotificationChannel.setDescription(h.getString(R.string.notification_channels_channel_description_truecaller_pay));
      localNotificationChannel.enableLights(true);
      localNotificationChannel.setLightColor(Z_());
      localNotificationManager.createNotificationChannel(localNotificationChannel);
      return;
    }
  }
  
  public final void m()
  {
    if (!Y_()) {
      return;
    }
    NotificationManager localNotificationManager = b;
    if (localNotificationManager != null)
    {
      localNotificationManager.deleteNotificationChannel("truecaller_pay_v2");
      return;
    }
  }
  
  public final void n()
  {
    if (!Y_()) {
      return;
    }
    NotificationManager localNotificationManager = b;
    if (localNotificationManager != null)
    {
      localNotificationManager.deleteNotificationChannel("dapp_search");
      return;
    }
  }
  
  public final String o()
  {
    if (!Y_()) {
      return null;
    }
    Object localObject = b;
    if (localObject == null) {
      return null;
    }
    localObject = ((NotificationManager)localObject).getNotificationChannel("profile_views");
    if (localObject != null) {
      return ((NotificationChannel)localObject).getId();
    }
    return null;
  }
  
  public final void p()
  {
    if (!Y_()) {
      return;
    }
    NotificationManager localNotificationManager = b;
    if (localNotificationManager != null)
    {
      localNotificationManager.createNotificationChannel(t());
      return;
    }
  }
  
  public final void q()
  {
    if (!Y_()) {
      return;
    }
    NotificationManager localNotificationManager = b;
    if (localNotificationManager != null)
    {
      localNotificationManager.deleteNotificationChannel("profile_views");
      return;
    }
  }
  
  public final void r()
  {
    if (!Y_()) {
      return;
    }
    NotificationManager localNotificationManager = b;
    if (localNotificationManager != null)
    {
      localNotificationManager.createNotificationChannel(u());
      return;
    }
  }
  
  public final void s()
  {
    if (!Y_()) {
      return;
    }
    NotificationManager localNotificationManager = b;
    if (localNotificationManager != null)
    {
      localNotificationManager.deleteNotificationChannel("engagement_rewards");
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notificationchannels.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */