package com.truecaller.data.entity;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.c.a.b.d;
import com.google.c.a.g;
import com.google.c.a.j;
import com.google.c.a.k.c;
import com.google.c.a.k.d;
import com.google.c.a.l.b;
import com.google.c.a.m.a;
import com.google.c.a.n;
import com.truecaller.common.h.aa;
import com.truecaller.common.h.ab;
import com.truecaller.common.h.am;
import com.truecaller.common.h.h;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.log.AssertionUtil;
import com.truecaller.search.ContactDto.Contact.PhoneNumber;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Number
  extends RowEntity<ContactDto.Contact.PhoneNumber>
  implements f
{
  public static final Parcelable.Creator<Number> CREATOR = new Number.1();
  static final Comparator<Number> a;
  private static final int[] b = { 2, 17, 1, 3, 7 };
  private static String d;
  private int c;
  
  static
  {
    a = -..Lambda.Number.vmwOABLbFcqzJfnAtqXybcO4iLE.INSTANCE;
  }
  
  public Number()
  {
    this(new ContactDto.Contact.PhoneNumber());
  }
  
  protected Number(Parcel paramParcel)
  {
    super(paramParcel);
    c = paramParcel.readInt();
  }
  
  public Number(Number paramNumber)
  {
    this(new ContactDto.Contact.PhoneNumber((ContactDto.Contact.PhoneNumber)paramNumber.row()));
    setSource(paramNumber.getSource());
  }
  
  Number(ContactDto.Contact.PhoneNumber paramPhoneNumber)
  {
    super(paramPhoneNumber);
  }
  
  public Number(String paramString)
  {
    this(paramString, null);
  }
  
  Number(String paramString1, String paramString2)
  {
    this();
    c(paramString1);
    Object localObject = g(paramString2);
    n localn = n.a();
    String str = am.b(new String[] { paramString2, d });
    if ((localObject != null) && (!TextUtils.isEmpty(str)))
    {
      if (!Collections.unmodifiableSet(g).contains(str))
      {
        AssertionUtil.reportWeirdnessButNeverCrash("Invalid country iso: ".concat(String.valueOf(str)));
        a(paramString1);
      }
      try
      {
        if (localn.a(paramString1, str))
        {
          a(paramString1);
          b(paramString1);
          a(k.d.c);
        }
        else
        {
          paramString2 = ((com.google.c.a.k)localObject).a(paramString1, str);
          if ((!localn.a(paramString2)) && (((com.google.c.a.k)localObject).e(paramString2)))
          {
            a(((com.google.c.a.k)localObject).a(paramString2, k.c.a));
            b(((com.google.c.a.k)localObject).a(paramString2, k.c.c));
          }
          else
          {
            a(paramString1);
            b(paramString1);
          }
          c(b);
          a(((com.google.c.a.k)localObject).b(paramString2));
        }
        paramString2 = h.c(a());
        if (paramString2 == null)
        {
          f(str);
          return;
        }
        f(am.c(c, Locale.ENGLISH));
        return;
      }
      catch (g paramString2)
      {
        a(paramString1);
        localObject = new StringBuilder("Invalid number, cannot parse \"");
        ((StringBuilder)localObject).append(paramString1);
        ((StringBuilder)localObject).append("\" using ");
        ((StringBuilder)localObject).append(str);
        ((StringBuilder)localObject).append(", ");
        ((StringBuilder)localObject).append(paramString2.getMessage());
        ((StringBuilder)localObject).toString();
        return;
      }
    }
    a(paramString1);
    f(paramString2);
  }
  
  public static Number a(String paramString1, String paramString2, String paramString3)
  {
    if ((TextUtils.isEmpty(paramString1)) && (TextUtils.isEmpty(paramString2))) {
      return null;
    }
    Number localNumber;
    if (!TextUtils.isEmpty(paramString1)) {
      localNumber = new Number(paramString1);
    } else {
      localNumber = new Number(paramString2, paramString3);
    }
    localNumber.a((String)am.e(paramString1, localNumber.a()));
    localNumber.c((String)am.e(paramString2, localNumber.d()));
    localNumber.f((String)am.e(paramString3, localNumber.l()));
    return localNumber;
  }
  
  private static com.google.c.a.k g(String paramString)
  {
    if (d == null)
    {
      if (TextUtils.isEmpty(paramString)) {
        paramString = com.truecaller.common.b.a.F().H();
      }
      if (am.b(paramString)) {
        return null;
      }
      d = paramString.toUpperCase();
    }
    return com.google.c.a.k.a();
  }
  
  public final String a()
  {
    return mRow).e164Format;
  }
  
  public final void a(int paramInt)
  {
    mRow).spamScore = String.valueOf(paramInt);
  }
  
  public final void a(k.d paramd)
  {
    ContactDto.Contact.PhoneNumber localPhoneNumber = (ContactDto.Contact.PhoneNumber)mRow;
    if (paramd == null) {
      paramd = null;
    } else {
      paramd = paramd.toString();
    }
    numberType = paramd;
  }
  
  public final void a(String paramString)
  {
    mRow).e164Format = paramString;
  }
  
  public final String b()
  {
    if (org.c.a.a.a.k.a(mRow).e164Format, "+", false)) {
      return mRow).e164Format.substring(1);
    }
    return mRow).e164Format;
  }
  
  public final void b(int paramInt)
  {
    mRow).telType = String.valueOf(paramInt);
  }
  
  public final void b(String paramString)
  {
    mRow).nationalFormat = paramString;
  }
  
  public final String c()
  {
    return mRow).nationalFormat;
  }
  
  public final void c(int paramInt)
  {
    mRow).dialingCode = String.valueOf(paramInt);
  }
  
  public final void c(String paramString)
  {
    mRow).rawNumberFormat = paramString;
  }
  
  public final String d()
  {
    return mRow).rawNumberFormat;
  }
  
  public final void d(String paramString)
  {
    mRow).carrier = paramString;
  }
  
  public final String e()
  {
    if ((m() == k.d.d) && (c() != null)) {
      return c();
    }
    return am.b(new String[] { d(), a(), c() });
  }
  
  public final void e(String paramString)
  {
    mRow).telTypeLabel = paramString;
  }
  
  public final String f()
  {
    return mRow).carrier;
  }
  
  public final void f(String paramString)
  {
    mRow).countryCode = paramString;
  }
  
  public final String g()
  {
    if (!TextUtils.isEmpty(f())) {
      return f();
    }
    Object localObject1 = g(null);
    if ((localObject1 != null) && (!TextUtils.isEmpty(a()))) {}
    Object localObject3;
    boolean bool;
    try
    {
      locala = ((com.google.c.a.k)localObject1).a(a(), l());
      localObject1 = j.a();
      localObject3 = Locale.getDefault();
      localObject5 = b;
      localObject4 = b.d(locala);
      localObject5 = ((com.google.c.a.k)localObject5).b((String)localObject4);
      if (localObject5 == null)
      {
        com.google.c.a.k.a.log(Level.WARNING, "Invalid or unknown region code provided: ".concat(String.valueOf(localObject4)));
        bool = false;
      }
      else
      {
        bool = B;
      }
    }
    catch (Exception localException)
    {
      m.a locala;
      Object localObject5;
      Object localObject4;
      AssertionUtil.shouldNeverHappen(localException, new String[0]);
      return null;
    }
    catch (g localg)
    {
      label160:
      return null;
    }
    localObject4 = b.b(locala);
    int i;
    if ((localObject4 != k.d.b) && (localObject4 != k.d.c)) {
      if (localObject4 == k.d.i)
      {
        break label328;
        if (i != 0)
        {
          localObject4 = ((Locale)localObject3).getLanguage();
          localObject3 = ((Locale)localObject3).getCountry();
          localObject5 = a;
          i = b;
          if (i == 1) {
            i = (int)(d / 10000000L) + 1000;
          }
          localObject1 = ((com.google.c.a.b.f)localObject5).a(i, (String)localObject4, "", (String)localObject3);
          if (localObject1 == null) {
            break label333;
          }
          localObject1 = ((d)localObject1).a(locala);
        }
      }
    }
    for (;;)
    {
      if (localObject1 != null)
      {
        localObject3 = localObject1;
        if (((String)localObject1).length() != 0) {
          break label338;
        }
      }
      localObject3 = localObject1;
      if (!com.google.c.a.b.f.a((String)localObject4)) {
        break label338;
      }
      localObject1 = ((com.google.c.a.b.f)localObject5).a(i, "en", "", "");
      if (localObject1 == null) {
        break label346;
      }
      localObject3 = ((d)localObject1).a(locala);
      break label338;
      return "";
      if (!bool) {
        break;
      }
      return "";
      i = 0;
      break label160;
      label328:
      i = 1;
      break label160;
      label333:
      Object localObject2 = null;
    }
    label338:
    if (localObject3 != null) {
      return (String)localObject3;
    }
    label346:
    return "";
  }
  
  public int getSource()
  {
    return c;
  }
  
  public String getTcId()
  {
    return mRow).id;
  }
  
  public final int h()
  {
    return org.c.a.a.a.b.a.a(mRow).spamScore);
  }
  
  public final int i()
  {
    return org.c.a.a.a.b.a.a(mRow).telType);
  }
  
  public final int j()
  {
    return org.c.a.a.a.b.a.a(mRow).telType, ContactDto.Contact.PhoneNumber.EMPTY_TEL_TYPE);
  }
  
  public final String k()
  {
    return mRow).telTypeLabel;
  }
  
  public final String l()
  {
    return mRow).countryCode;
  }
  
  public final k.d m()
  {
    return ab.a(mRow).numberType, k.d.l);
  }
  
  public boolean mergeEquals(f paramf)
  {
    if (this == paramf) {
      return true;
    }
    if (!(paramf instanceof Number)) {
      return false;
    }
    paramf = (Number)paramf;
    return TextUtils.equals(a(), paramf.a());
  }
  
  public final String n()
  {
    String str1 = d();
    if ((str1 != null) && (ab.b(str1))) {
      return str1;
    }
    if ((d != null) && (l() != null))
    {
      if ((!TextUtils.isEmpty(c())) && (d.contains(l()))) {
        return c();
      }
      if ((!TextUtils.isEmpty(a())) && (!d.contains(l()))) {
        return aa.d(a());
      }
    }
    String str2 = a();
    if (str1 == null) {
      return str2;
    }
    return str1;
  }
  
  public final String o()
  {
    if (ab.e(d())) {
      return d();
    }
    if (ab.e(a())) {
      return a();
    }
    if (ab.e(c())) {
      return c();
    }
    return null;
  }
  
  public void setSource(int paramInt)
  {
    c = paramInt;
  }
  
  public void setTcId(String paramString)
  {
    super.setTcId(paramString);
    mRow).id = paramString;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    paramParcel.writeInt(c);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.Number
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */