package com.truecaller.data.entity;

import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.google.c.a.k;
import com.google.common.base.Strings;
import com.truecaller.calling.ah;
import com.truecaller.calling.dialer.ax;
import com.truecaller.common.h.am;
import com.truecaller.utils.n;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

final class h
  implements g
{
  private static final long a = TimeUnit.MINUTES.toMillis(2L);
  private final TelephonyManager b;
  private final n c;
  private final ax d;
  private String e;
  private volatile long f;
  private String g;
  private volatile long h;
  
  h(TelephonyManager paramTelephonyManager, n paramn, ax paramax)
  {
    b = paramTelephonyManager;
    c = paramn;
    d = paramax;
  }
  
  private String a()
  {
    if (b == null) {
      return null;
    }
    long l1 = SystemClock.elapsedRealtime();
    long l2 = f;
    String str = e;
    if (l1 - l2 < a) {
      return str;
    }
    try
    {
      l2 = f;
      str = e;
      if (l1 - l2 < a) {
        return str;
      }
      str = am.a(b.getNetworkCountryIso(), Locale.ENGLISH);
      e = str;
      f = SystemClock.elapsedRealtime();
      return str;
    }
    finally {}
  }
  
  private String b()
  {
    if (b == null) {
      return null;
    }
    long l1 = SystemClock.elapsedRealtime();
    long l2 = h;
    String str = g;
    if (l1 - l2 < a) {
      return str;
    }
    try
    {
      l2 = h;
      str = g;
      if (l1 - l2 < a) {
        return str;
      }
      str = am.a(b.getSimCountryIso(), Locale.ENGLISH);
      g = str;
      h = SystemClock.elapsedRealtime();
      return str;
    }
    finally {}
  }
  
  public final Number a(String... paramVarArgs)
  {
    if (paramVarArgs == null) {
      return null;
    }
    k localk = k.a();
    int j = paramVarArgs.length;
    int i = 0;
    String str;
    Object localObject2;
    for (Object localObject1 = null; i < j; localObject1 = localObject2)
    {
      str = paramVarArgs[i];
      localObject2 = localObject1;
      if (!Strings.isNullOrEmpty(str))
      {
        localObject2 = localObject1;
        if (localObject1 == null) {
          localObject2 = str;
        }
      }
      try
      {
        localk.a(str, null);
        localObject1 = new Number(str);
        ((Number)localObject1).c(str);
        return (Number)localObject1;
      }
      catch (com.google.c.a.g localg)
      {
        for (;;) {}
      }
      i += 1;
    }
    if (localObject1 != null)
    {
      if (localObject1 == null) {
        return null;
      }
      localObject2 = a();
      str = b();
      paramVarArgs = (String[])localObject2;
      if (TextUtils.isEmpty((CharSequence)localObject2)) {
        paramVarArgs = str;
      }
      paramVarArgs = new Number((String)localObject1, paramVarArgs);
      paramVarArgs.c((String)localObject1);
      return paramVarArgs;
    }
    return null;
  }
  
  public final String a(Number paramNumber)
  {
    return ah.a(paramNumber, c, d);
  }
  
  public final Number b(String... paramVarArgs)
  {
    return (Number)org.c.a.a.a.h.a(new Number[] { a(paramVarArgs), new Number() });
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */