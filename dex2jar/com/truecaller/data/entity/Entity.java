package com.truecaller.data.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.truecaller.common.h.am;

public abstract class Entity
  implements Parcelable
{
  private Long mId;
  private Object mTag;
  private String mTcId;
  
  protected Entity() {}
  
  protected Entity(Parcel paramParcel)
  {
    mId = ((Long)paramParcel.readValue(Long.class.getClassLoader()));
    mTcId = paramParcel.readString();
  }
  
  static int presentationCompare(String... paramVarArgs)
  {
    int i = 0;
    int j = 0;
    while ((i == 0) && (j < paramVarArgs.length))
    {
      String str1 = am.a(paramVarArgs[j]);
      String str2 = am.a(paramVarArgs[(j + 1)]);
      if ((!TextUtils.isEmpty(str1)) && (!TextUtils.isEmpty(str2))) {
        i = am.a(str1, str2, true);
      } else if (TextUtils.equals(str1, str2)) {
        i = 0;
      } else if (TextUtils.isEmpty(str1)) {
        i = 1;
      } else {
        i = -1;
      }
      j += 2;
    }
    return i;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Long getId()
  {
    return mId;
  }
  
  public Object getTag()
  {
    return mTag;
  }
  
  public String getTcId()
  {
    return mTcId;
  }
  
  public void setId(Long paramLong)
  {
    mId = paramLong;
  }
  
  public void setTag(Object paramObject)
  {
    mTag = paramObject;
  }
  
  public void setTcId(String paramString)
  {
    mTcId = paramString;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeValue(mId);
    paramParcel.writeString(mTcId);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.Entity
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */