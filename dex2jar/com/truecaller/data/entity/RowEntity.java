package com.truecaller.data.entity;

import android.os.Parcel;
import com.truecaller.search.ContactDto.Row;

public abstract class RowEntity<RT extends ContactDto.Row>
  extends Entity
{
  public final RT mRow;
  
  protected RowEntity(Parcel paramParcel)
  {
    super(paramParcel);
    mRow = ((ContactDto.Row)paramParcel.readParcelable(ContactDto.Row.class.getClassLoader()));
  }
  
  RowEntity(RT paramRT)
  {
    mRow = paramRT;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Long getDataPhonebookId()
  {
    if (mRow.phonebookId == 0L) {
      return null;
    }
    return Long.valueOf(mRow.phonebookId);
  }
  
  public Long getId()
  {
    if (mRow.rowId == 0L) {
      return null;
    }
    return Long.valueOf(mRow.rowId);
  }
  
  public int getSource()
  {
    return mRow.source;
  }
  
  public String getTcId()
  {
    return mRow.tcId;
  }
  
  public boolean isPrimary()
  {
    return mRow.isPrimary;
  }
  
  final RT row()
  {
    return mRow;
  }
  
  public void setDataPhonebookId(Long paramLong)
  {
    ContactDto.Row localRow = mRow;
    long l;
    if (paramLong == null) {
      l = 0L;
    } else {
      l = paramLong.longValue();
    }
    phonebookId = l;
  }
  
  public void setId(Long paramLong)
  {
    ContactDto.Row localRow = mRow;
    long l;
    if (paramLong == null) {
      l = 0L;
    } else {
      l = paramLong.longValue();
    }
    rowId = l;
  }
  
  public void setIsPrimary(boolean paramBoolean)
  {
    mRow.isPrimary = paramBoolean;
  }
  
  public void setSource(int paramInt)
  {
    mRow.source = paramInt;
  }
  
  public void setTcId(String paramString)
  {
    mRow.tcId = paramString;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(getClass().getSimpleName());
    localStringBuilder.append("{row=");
    localStringBuilder.append(mRow);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    paramParcel.writeParcelable(mRow, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.RowEntity
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */