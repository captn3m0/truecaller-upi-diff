package com.truecaller.data.entity;

import android.content.Context;
import com.truecaller.calling.dialer.ax;
import com.truecaller.utils.n;
import javax.inject.Provider;

public final class d
  implements dagger.a.d<g>
{
  private final c a;
  private final Provider<Context> b;
  private final Provider<n> c;
  private final Provider<ax> d;
  
  private d(c paramc, Provider<Context> paramProvider, Provider<n> paramProvider1, Provider<ax> paramProvider2)
  {
    a = paramc;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
  }
  
  public static d a(c paramc, Provider<Context> paramProvider, Provider<n> paramProvider1, Provider<ax> paramProvider2)
  {
    return new d(paramc, paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */