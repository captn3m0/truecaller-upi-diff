package com.truecaller.data.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.c.a.g;
import com.google.c.a.k;
import com.google.c.a.k.c;
import com.google.c.a.k.d;
import com.truecaller.backup.CallLogBackupItem;
import com.truecaller.common.b.a;
import com.truecaller.common.h.ab;
import com.truecaller.common.h.h;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.flash.CallLogFlashItem;

public class HistoryEvent
  extends Entity
  implements Parcelable
{
  public static final Parcelable.Creator<HistoryEvent> CREATOR = new HistoryEvent.1();
  private static volatile k s;
  private static volatile String t;
  public String a = "";
  public String b;
  public String c;
  public String d;
  public String e;
  public Contact f;
  Long g;
  public long h;
  public long i;
  public String j = "-1";
  int k;
  int l;
  public CallRecording m;
  int n = 1;
  public int o;
  public int p = 4;
  public String q;
  public int r;
  private k.d u;
  
  private HistoryEvent() {}
  
  private HistoryEvent(Parcel paramParcel)
  {
    setTcId(paramParcel.readString());
    b = paramParcel.readString();
    c = paramParcel.readString();
    d = paramParcel.readString();
    int i1 = paramParcel.readInt();
    Object localObject2 = null;
    if (i1 == -1) {
      u = null;
    } else {
      u = k.d.values()[i1];
    }
    o = paramParcel.readInt();
    p = paramParcel.readInt();
    h = paramParcel.readLong();
    i = paramParcel.readLong();
    k = paramParcel.readInt();
    n = paramParcel.readInt();
    l = paramParcel.readInt();
    q = paramParcel.readString();
    r = paramParcel.readInt();
    if (paramParcel.readByte() == 1) {
      localObject1 = Long.valueOf(paramParcel.readLong());
    } else {
      localObject1 = null;
    }
    setId((Long)localObject1);
    Object localObject1 = localObject2;
    if (paramParcel.readByte() == 1) {
      localObject1 = Long.valueOf(paramParcel.readLong());
    }
    g = ((Long)localObject1);
    if (paramParcel.readByte() == 1) {
      f = ((Contact)paramParcel.readParcelable(Contact.class.getClassLoader()));
    }
    j = paramParcel.readString();
    a = paramParcel.readString();
    if (paramParcel.readByte() == 1) {
      m = ((CallRecording)paramParcel.readParcelable(CallRecording.class.getClassLoader()));
    }
  }
  
  public HistoryEvent(CallLogBackupItem paramCallLogBackupItem)
  {
    this(paramCallLogBackupItem.getNumber());
    h = paramCallLogBackupItem.getTimestamp();
    i = paramCallLogBackupItem.getDuration();
    o = paramCallLogBackupItem.getType();
    p = paramCallLogBackupItem.getAction();
    k = paramCallLogBackupItem.getFeatures();
    q = paramCallLogBackupItem.getComponentName();
    n = 0;
    l = 1;
    r = 2;
  }
  
  public HistoryEvent(Contact paramContact, int paramInt)
  {
    v();
    setTcId(paramContact.getTcId());
    b = paramContact.p();
    Number localNumber = paramContact.r();
    if (localNumber != null)
    {
      c = localNumber.d();
      b = localNumber.a();
      u = localNumber.m();
      d = localNumber.l();
    }
    o = paramInt;
    g = null;
    h = System.currentTimeMillis();
    i = 0L;
    if (paramContact.U()) {
      p = 2;
    }
  }
  
  public HistoryEvent(Number paramNumber)
  {
    c = paramNumber.d();
    b = paramNumber.a();
    u = paramNumber.m();
    d = paramNumber.l();
  }
  
  public HistoryEvent(CallLogFlashItem paramCallLogFlashItem)
  {
    this(paramCallLogFlashItem.getNumber());
    h = paramCallLogFlashItem.getTimestamp();
    i = paramCallLogFlashItem.getDuration();
    o = paramCallLogFlashItem.getType();
    p = paramCallLogFlashItem.getAction();
    k = paramCallLogFlashItem.getFeatures();
    q = paramCallLogFlashItem.getComponentName();
    n = 0;
    l = 1;
    r = 3;
  }
  
  public HistoryEvent(String paramString)
  {
    if (ab.a(paramString)) {
      return;
    }
    v();
    c = paramString;
    try
    {
      paramString = s.a(paramString, t);
      b = s.a(paramString, k.c.a);
      u = s.b(paramString);
      paramString = h.c(b);
      if ((paramString != null) && (!TextUtils.isEmpty(c)))
      {
        d = c.toUpperCase();
        return;
      }
      d = t;
      return;
    }
    catch (g paramString)
    {
      StringBuilder localStringBuilder = new StringBuilder("Cannot parse number, ");
      localStringBuilder.append(paramString.getMessage());
      localStringBuilder.toString();
    }
  }
  
  private void v()
  {
    if (s == null) {
      try
      {
        if (s == null)
        {
          t = a.F().H();
          s = k.a();
        }
        return;
      }
      finally {}
    }
  }
  
  public final String a()
  {
    return b;
  }
  
  public final void a(int paramInt)
  {
    o = paramInt;
  }
  
  public final void a(long paramLong)
  {
    h = paramLong;
  }
  
  public final void a(CallRecording paramCallRecording)
  {
    m = paramCallRecording;
  }
  
  public final void a(Long paramLong)
  {
    g = paramLong;
  }
  
  public final void a(String paramString)
  {
    c = paramString;
  }
  
  public final String b()
  {
    return c;
  }
  
  public final void b(int paramInt)
  {
    p = paramInt;
  }
  
  public final void b(long paramLong)
  {
    i = paramLong;
  }
  
  public final void b(String paramString)
  {
    q = paramString;
  }
  
  public final k.d c()
  {
    return u;
  }
  
  public final String d()
  {
    return d;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public final String e()
  {
    return e;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (paramObject != null)
    {
      if (getClass() != paramObject.getClass()) {
        return false;
      }
      paramObject = (HistoryEvent)paramObject;
      if (!a.equals(a)) {
        return false;
      }
      if (o != o) {
        return false;
      }
      if (p != p) {
        return false;
      }
      if (h != h) {
        return false;
      }
      if (i != i) {
        return false;
      }
      if (k != k) {
        return false;
      }
      Object localObject = b;
      if (localObject != null)
      {
        if (!((String)localObject).equals(b)) {
          return false;
        }
      }
      else if (b != null) {
        return false;
      }
      localObject = c;
      if (localObject != null)
      {
        if (!((String)localObject).equals(c)) {
          return false;
        }
      }
      else if (c != null) {
        return false;
      }
      localObject = d;
      if (localObject != null)
      {
        if (!((String)localObject).equals(d)) {
          return false;
        }
      }
      else if (d != null) {
        return false;
      }
      localObject = e;
      if (localObject != null)
      {
        if (!((String)localObject).equals(e)) {
          return false;
        }
      }
      else if (e != null) {
        return false;
      }
      if (u != u) {
        return false;
      }
      localObject = g;
      if (localObject != null)
      {
        if (!((Long)localObject).equals(g)) {
          return false;
        }
      }
      else if (g != null) {
        return false;
      }
      localObject = m;
      if (localObject != null)
      {
        if (((CallRecording)localObject).equals(m)) {
          return false;
        }
      }
      else if (m != null) {
        return false;
      }
      return j.equals(j);
    }
    return false;
  }
  
  public final int f()
  {
    return o;
  }
  
  public final int g()
  {
    switch (o)
    {
    case 4: 
    default: 
      return 0;
    case 6: 
      return 21;
    case 5: 
      return 4;
    case 3: 
      return 6;
    case 2: 
      return 1;
    }
    return 2;
  }
  
  public final int h()
  {
    return p;
  }
  
  public int hashCode()
  {
    Object localObject = b;
    int i7 = 0;
    int i1;
    if (localObject != null) {
      i1 = ((String)localObject).hashCode();
    } else {
      i1 = 0;
    }
    localObject = c;
    int i2;
    if (localObject != null) {
      i2 = ((String)localObject).hashCode();
    } else {
      i2 = 0;
    }
    localObject = d;
    int i3;
    if (localObject != null) {
      i3 = ((String)localObject).hashCode();
    } else {
      i3 = 0;
    }
    localObject = e;
    int i4;
    if (localObject != null) {
      i4 = ((String)localObject).hashCode();
    } else {
      i4 = 0;
    }
    localObject = u;
    int i5;
    if (localObject != null) {
      i5 = ((k.d)localObject).hashCode();
    } else {
      i5 = 0;
    }
    int i8 = o;
    int i9 = p;
    localObject = g;
    int i6;
    if (localObject != null) {
      i6 = ((Long)localObject).hashCode();
    } else {
      i6 = 0;
    }
    long l1 = h;
    int i10 = (int)(l1 ^ l1 >>> 32);
    l1 = i;
    int i11 = (int)(l1 ^ l1 >>> 32);
    int i12 = j.hashCode();
    int i13 = k;
    int i14 = a.hashCode();
    localObject = m;
    if (localObject != null) {
      i7 = ((CallRecording)localObject).hashCode();
    }
    return ((((((((((((i1 * 31 + i2) * 31 + i3) * 31 + i4) * 31 + i5) * 31 + i8) * 31 + i9) * 31 + i6) * 31 + i10) * 31 + i11) * 31 + i12) * 31 + i13) * 31 + i14) * 31 + i7;
  }
  
  public final Long i()
  {
    return g;
  }
  
  public final long j()
  {
    return h;
  }
  
  public final long k()
  {
    return i;
  }
  
  public final String l()
  {
    return j;
  }
  
  public final int m()
  {
    return k;
  }
  
  public final void n()
  {
    k = 1;
  }
  
  public final int o()
  {
    return n;
  }
  
  public final int p()
  {
    return l;
  }
  
  public final String q()
  {
    return q;
  }
  
  public final int r()
  {
    return r;
  }
  
  public final Contact s()
  {
    return f;
  }
  
  public final CallRecording t()
  {
    return m;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("HistoryEvent:{id=");
    localStringBuilder.append(getId());
    localStringBuilder.append(", tcId=");
    localStringBuilder.append(getTcId());
    localStringBuilder.append(", normalizedNumber=");
    localStringBuilder.append(b);
    if (localStringBuilder.toString() == null) {
      return "null";
    }
    localStringBuilder = new StringBuilder("<non-null normalized number>, rawNumber=");
    localStringBuilder.append(c);
    if (localStringBuilder.toString() == null) {
      return "null";
    }
    localStringBuilder = new StringBuilder("<non-null raw number>, cachedName=");
    localStringBuilder.append(e);
    if (localStringBuilder.toString() == null) {
      return "null";
    }
    localStringBuilder = new StringBuilder("<non-null cached name>, numberType=");
    localStringBuilder.append(u);
    localStringBuilder.append(", type=");
    localStringBuilder.append(o);
    localStringBuilder.append(", action=");
    localStringBuilder.append(p);
    localStringBuilder.append(", callLogId=");
    localStringBuilder.append(g);
    localStringBuilder.append(", timestamp=");
    localStringBuilder.append(h);
    localStringBuilder.append(", duration=");
    localStringBuilder.append(i);
    localStringBuilder.append(", features=");
    localStringBuilder.append(k);
    localStringBuilder.append(", isNew=");
    localStringBuilder.append(k);
    localStringBuilder.append(", isRead=");
    localStringBuilder.append(k);
    localStringBuilder.append(", phoneAccountComponentName=");
    localStringBuilder.append(q);
    localStringBuilder.append(", contact=");
    localStringBuilder.append(f);
    localStringBuilder.append(", eventId=");
    localStringBuilder.append(a);
    localStringBuilder.append(", callRecording=");
    localStringBuilder.append(m);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
  
  public final String u()
  {
    return a;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeString(getTcId());
    paramParcel.writeString(b);
    paramParcel.writeString(c);
    paramParcel.writeString(d);
    k.d locald = u;
    int i1;
    if (locald == null) {
      i1 = -1;
    } else {
      i1 = locald.ordinal();
    }
    paramParcel.writeInt(i1);
    paramParcel.writeInt(o);
    paramParcel.writeInt(p);
    paramParcel.writeLong(h);
    paramParcel.writeLong(i);
    paramParcel.writeInt(k);
    paramParcel.writeInt(n);
    paramParcel.writeInt(l);
    paramParcel.writeString(q);
    paramParcel.writeInt(r);
    if (getId() == null)
    {
      paramParcel.writeByte((byte)0);
    }
    else
    {
      paramParcel.writeByte((byte)1);
      paramParcel.writeLong(getId().longValue());
    }
    if (g == null)
    {
      paramParcel.writeByte((byte)0);
    }
    else
    {
      paramParcel.writeByte((byte)1);
      paramParcel.writeLong(g.longValue());
    }
    if (f == null)
    {
      paramParcel.writeByte((byte)0);
    }
    else
    {
      paramParcel.writeByte((byte)1);
      paramParcel.writeParcelable(f, paramInt);
    }
    paramParcel.writeString(j);
    paramParcel.writeString(a);
    if (m == null)
    {
      paramParcel.writeByte((byte)0);
      return;
    }
    paramParcel.writeByte((byte)1);
    paramParcel.writeParcelable(m, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.HistoryEvent
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */