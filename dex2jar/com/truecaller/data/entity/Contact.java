package com.truecaller.data.entity;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.h.aa;
import com.truecaller.common.h.ab;
import com.truecaller.common.h.am;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.search.ContactDto.Contact;
import com.truecaller.search.ContactDto.Contact.Address;
import com.truecaller.search.ContactDto.Contact.InternetAddress;
import com.truecaller.search.ContactDto.Contact.PhoneNumber;
import com.truecaller.search.ContactDto.Contact.Source;
import com.truecaller.search.ContactDto.Contact.Tag;
import com.truecaller.search.ContactDto.Row;
import com.truecaller.search.local.model.a.g;
import com.truecaller.util.y;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Contact
  extends RowEntity<ContactDto.Contact>
{
  public static final Parcelable.Creator<Contact> CREATOR = new Contact.1();
  public final List<Tag> a = new ArrayList();
  public final List<Source> b = new ArrayList();
  public transient Uri c;
  public transient boolean d;
  public List<Source> e;
  public int f;
  public StructuredName g;
  public Note h;
  public Business i;
  public Style j;
  public int k;
  public Integer l;
  private final List<Address> m = new ArrayList();
  private final List<Number> n = new ArrayList();
  private final List<Link> o = new ArrayList();
  private transient List<Number> p;
  private List<Address> q;
  private List<Number> r;
  private List<Tag> s;
  private List<Link> t;
  
  public Contact()
  {
    this(new ContactDto.Contact());
  }
  
  protected Contact(Parcel paramParcel)
  {
    super(paramParcel);
    boolean bool = false;
    f = 0;
    m.addAll(paramParcel.createTypedArrayList(Address.CREATOR));
    n.addAll(paramParcel.createTypedArrayList(Number.CREATOR));
    a.addAll(paramParcel.createTypedArrayList(Tag.CREATOR));
    b.addAll(paramParcel.createTypedArrayList(Source.CREATOR));
    o.addAll(paramParcel.createTypedArrayList(Link.CREATOR));
    f = paramParcel.readInt();
    l = ((Integer)paramParcel.readValue(Integer.class.getClassLoader()));
    c = ((Uri)paramParcel.readParcelable(Uri.class.getClassLoader()));
    if (paramParcel.readByte() != 0) {
      bool = true;
    }
    d = bool;
    g = ((StructuredName)paramParcel.readParcelable(StructuredName.class.getClassLoader()));
    h = ((Note)paramParcel.readParcelable(Note.class.getClassLoader()));
    i = ((Business)paramParcel.readParcelable(Business.class.getClassLoader()));
    j = ((Style)paramParcel.readParcelable(Style.class.getClassLoader()));
  }
  
  public Contact(Contact paramContact)
  {
    this(new ContactDto.Contact((ContactDto.Contact)mRow));
    c = c;
    d = d;
    l = l;
  }
  
  public Contact(ContactDto.Contact paramContact)
  {
    super(paramContact);
    f = 0;
    Object localObject;
    if (mRow).addresses != null)
    {
      paramContact = mRow).addresses.iterator();
      while (paramContact.hasNext())
      {
        localObject = (ContactDto.Contact.Address)paramContact.next();
        m.add(a(new Address((ContactDto.Contact.Address)localObject), getSource()));
      }
    }
    if (mRow).phones != null)
    {
      paramContact = mRow).phones.iterator();
      while (paramContact.hasNext())
      {
        localObject = (ContactDto.Contact.PhoneNumber)paramContact.next();
        n.add(a(new Number((ContactDto.Contact.PhoneNumber)localObject), getSource()));
      }
    }
    if (mRow).internetAddresses != null)
    {
      paramContact = mRow).internetAddresses.iterator();
      while (paramContact.hasNext())
      {
        localObject = (ContactDto.Contact.InternetAddress)paramContact.next();
        o.add(a(new Link((ContactDto.Contact.InternetAddress)localObject), getSource()));
      }
    }
    if (mRow).tags != null)
    {
      paramContact = mRow).tags.iterator();
      while (paramContact.hasNext())
      {
        localObject = (ContactDto.Contact.Tag)paramContact.next();
        a.add(a(new Tag((ContactDto.Contact.Tag)localObject), getSource()));
      }
    }
    if (mRow).sources != null)
    {
      paramContact = mRow).sources.iterator();
      while (paramContact.hasNext())
      {
        localObject = (ContactDto.Contact.Source)paramContact.next();
        b.add(a(new Source((ContactDto.Contact.Source)localObject), getSource()));
      }
    }
    if (mRow).business != null) {
      i = ((Business)a(new Business(mRow).business), getSource()));
    }
    if (mRow).style != null) {
      j = ((Style)a(new Style(mRow).style), getSource()));
    }
    f = com.truecaller.content.a.a.a(mRow).badges);
  }
  
  private <T extends Entity> T a(T paramT)
  {
    paramT.setTcId(getTcId());
    return paramT;
  }
  
  private <T extends RowEntity> T a(T paramT, int paramInt)
  {
    paramT.setTcId(getTcId());
    paramT.setSource(paramInt);
    return paramT;
  }
  
  private <RT extends ContactDto.Row, ET extends Entity> List<RT> a(List<ET> paramList, List<RT> paramList1, ET paramET, RT paramRT)
  {
    Object localObject = paramList1;
    if (paramList1 == null) {
      localObject = new ArrayList();
    }
    paramList.add(a(paramET));
    ((List)localObject).add(paramRT);
    return (List<RT>)localObject;
  }
  
  private static <T extends f> void a(T paramT1, T paramT2)
  {
    if ((paramT1 instanceof Number))
    {
      paramT1 = (Number)paramT1;
      paramT2 = (Number)paramT2;
      if (paramT2.h() > paramT1.h()) {
        paramT1.a(paramT2.h());
      }
      if (am.b(paramT1.f())) {
        paramT1.d(paramT2.f());
      }
      if (paramT1.getDataPhonebookId() == null) {
        paramT1.setDataPhonebookId(paramT2.getDataPhonebookId());
      }
      paramT1.setSource(paramT1.getSource() | paramT2.getSource());
      if (paramT2.i() != ContactDto.Contact.PhoneNumber.EMPTY_TEL_TYPE)
      {
        paramT1.b(paramT2.i());
        paramT1.e(paramT2.k());
        paramT1.a(paramT2.m());
      }
    }
  }
  
  private void a(List<Number> paramList)
  {
    if (paramList != null)
    {
      if (paramList.isEmpty()) {
        return;
      }
      p = new ArrayList(paramList.size());
      paramList = paramList.iterator();
      while (paramList.hasNext())
      {
        Number localNumber = (Number)paramList.next();
        if ((localNumber.getSource() & 0xD) != 0) {
          p.add(localNumber);
        }
      }
      return;
    }
  }
  
  private String af()
  {
    Object localObject = o();
    if ((N()) && (!am.b((CharSequence)localObject)))
    {
      String str = z();
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append((String)localObject);
      if (str != null)
      {
        localObject = new StringBuilder(" (");
        ((StringBuilder)localObject).append(str);
        ((StringBuilder)localObject).append(")");
        str = ((StringBuilder)localObject).toString();
      }
      else
      {
        str = "";
      }
      localStringBuilder.append(str);
      return localStringBuilder.toString();
    }
    return null;
  }
  
  private static <T extends f> void b(List<T> paramList)
  {
    if (paramList == null) {
      return;
    }
    ListIterator localListIterator1 = paramList.listIterator();
    while (localListIterator1.hasNext())
    {
      f localf1 = (f)localListIterator1.next();
      int i1 = localListIterator1.previousIndex();
      if (i1 != -1)
      {
        boolean bool = false;
        ListIterator localListIterator2 = paramList.listIterator();
        while ((!bool) && (localListIterator2.hasNext()) && (localListIterator2.nextIndex() != i1))
        {
          f localf2 = (f)localListIterator2.next();
          bool = localf2.mergeEquals(localf1);
          if (bool)
          {
            a(localf2, localf1);
            localListIterator1.remove();
          }
        }
      }
    }
  }
  
  public final List<Number> A()
  {
    if (r == null) {
      r = Collections.unmodifiableList(n);
    }
    return r;
  }
  
  public final String B()
  {
    if (b.isEmpty()) {
      return "";
    }
    return ((Source)b.get(0)).c();
  }
  
  public final String C()
  {
    if (b.isEmpty()) {
      return "";
    }
    return ((Source)b.get(0)).d();
  }
  
  public final String D()
  {
    if (b.isEmpty()) {
      return "";
    }
    return ((Source)b.get(0)).b();
  }
  
  public final Long E()
  {
    if (mRow).phonebookId == 0L) {
      return null;
    }
    return Long.valueOf(mRow).phonebookId);
  }
  
  public final String F()
  {
    return mRow).phonebookLookupKey;
  }
  
  public final String G()
  {
    return mRow).searchQuery;
  }
  
  public final long H()
  {
    return mRow).searchTime;
  }
  
  public final int I()
  {
    Object localObject = l;
    if (localObject != null)
    {
      if (((Integer)localObject).intValue() >= 10) {
        return l.intValue();
      }
      return -1;
    }
    localObject = n.iterator();
    while (((Iterator)localObject).hasNext())
    {
      Number localNumber = (Number)((Iterator)localObject).next();
      if (localNumber.h() >= 10) {
        return localNumber.h();
      }
    }
    return -1;
  }
  
  public final List<Tag> J()
  {
    if (s == null) {
      s = Collections.unmodifiableList(a);
    }
    return s;
  }
  
  public final String K()
  {
    return mRow).transliteratedName;
  }
  
  public final boolean L()
  {
    return a(1);
  }
  
  public final boolean M()
  {
    return a(32);
  }
  
  public final boolean N()
  {
    return (a(64)) && (TrueApp.y().a().aF().y().a());
  }
  
  public final boolean O()
  {
    return n.size() > 0;
  }
  
  public final boolean P()
  {
    return ((getSource() & 0x4) == 0) && (!am.b(z()));
  }
  
  public final boolean Q()
  {
    return ab.a(p());
  }
  
  public final boolean R()
  {
    return ("private".equalsIgnoreCase(i())) && (!O());
  }
  
  public final boolean S()
  {
    return (getSource() & 0xD) != 0;
  }
  
  public final boolean T()
  {
    return ((getSource() & 0x33) == 0) && (c(64));
  }
  
  public final boolean U()
  {
    Object localObject = l;
    if (localObject != null) {
      return ((Integer)localObject).intValue() >= 10;
    }
    localObject = n.iterator();
    while (((Iterator)localObject).hasNext()) {
      if (((Number)((Iterator)localObject).next()).h() >= 10) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean V()
  {
    com.truecaller.common.g.a locala = TrueApp.y().a().I();
    return ((getSource() & 0x8) != 0) && (locala.b("backup"));
  }
  
  public final boolean W()
  {
    return am.b(z());
  }
  
  public final boolean X()
  {
    return mRow).isFavorite;
  }
  
  public final boolean Y()
  {
    return (getSource() & 0x20) == 32;
  }
  
  public final boolean Z()
  {
    return E() != null;
  }
  
  public final Uri a(boolean paramBoolean)
  {
    return y.a(mRow).phonebookId, mRow).image, paramBoolean);
  }
  
  public final String a()
  {
    Address localAddress = g();
    if (localAddress == null) {
      return null;
    }
    if ((!R()) && ((am.a(localAddress.getStreet())) || (am.a(localAddress.getZipCode())) || (am.a(localAddress.getCity())))) {
      return am.a(new String[] { localAddress.getStreet(), am.a(" ", new CharSequence[] { localAddress.getZipCode(), localAddress.getCity() }) });
    }
    return localAddress.getCity();
  }
  
  public final void a(long paramLong)
  {
    mRow).searchTime = paramLong;
  }
  
  public final void a(Address paramAddress)
  {
    mRow).addresses = a(m, mRow).addresses, paramAddress, paramAddress.row());
  }
  
  public final void a(Link paramLink)
  {
    mRow).internetAddresses = a(o, mRow).internetAddresses, paramLink, paramLink.row());
  }
  
  public final void a(Number paramNumber)
  {
    mRow).phones = a(n, mRow).phones, paramNumber, paramNumber.row());
  }
  
  public final void a(Source paramSource)
  {
    mRow).sources = a(b, mRow).sources, paramSource, paramSource.row());
  }
  
  public final void a(Tag paramTag)
  {
    mRow).tags = a(a, mRow).tags, paramTag, paramTag.row());
  }
  
  public final void a(Integer paramInteger)
  {
    ContactDto.Contact localContact = (ContactDto.Contact)mRow;
    int i1;
    if (paramInteger != null) {
      i1 = paramInteger.intValue();
    } else {
      i1 = -1;
    }
    favoritePosition = i1;
  }
  
  public final void a(Long paramLong)
  {
    ContactDto.Contact localContact = (ContactDto.Contact)mRow;
    long l1;
    if (paramLong == null) {
      l1 = 0L;
    } else {
      l1 = paramLong.longValue();
    }
    aggregatedRowId = l1;
  }
  
  public final void a(String paramString)
  {
    mRow).about = paramString;
  }
  
  public final boolean a(int paramInt)
  {
    return (paramInt & f) != 0;
  }
  
  public final boolean aa()
  {
    return (!c(2)) && (!am.b(z())) && (!a(1));
  }
  
  public final void ab()
  {
    a.clear();
    mRow).tags = null;
  }
  
  public final boolean ac()
  {
    return (!Z()) && (!a(2)) && (!R()) && (!Y());
  }
  
  public final void ad()
  {
    Collections.sort(m, Address.PRESENTATION_COMPARATOR);
    b(m);
    Collections.sort(n, Number.a);
    a(n);
    b(n);
    b(b);
    b(o);
    b(a);
  }
  
  public final void ae()
  {
    k = 2;
  }
  
  public final String b()
  {
    Address localAddress = g();
    if (localAddress == null) {
      return "";
    }
    return localAddress.getDisplayableAddress();
  }
  
  public final void b(int paramInt)
  {
    mRow).commonConnections = paramInt;
  }
  
  public final void b(Long paramLong)
  {
    ContactDto.Contact localContact = (ContactDto.Contact)mRow;
    long l1;
    if (paramLong == null) {
      l1 = 0L;
    } else {
      l1 = paramLong.longValue();
    }
    phonebookHash = l1;
  }
  
  public final void b(String paramString)
  {
    mRow).access = paramString;
  }
  
  public final void b(boolean paramBoolean)
  {
    mRow).isFavorite = paramBoolean;
  }
  
  public final String c()
  {
    Address localAddress = g();
    if (localAddress == null) {
      return "";
    }
    return localAddress.getShortDisplayableAddress();
  }
  
  public final void c(Long paramLong)
  {
    ContactDto.Contact localContact = (ContactDto.Contact)mRow;
    long l1;
    if (paramLong == null) {
      l1 = 0L;
    } else {
      l1 = paramLong.longValue();
    }
    phonebookId = l1;
  }
  
  public final void c(String paramString)
  {
    mRow).imId = paramString;
  }
  
  public final boolean c(int paramInt)
  {
    return (paramInt & getSource()) != 0;
  }
  
  public final List<Address> d()
  {
    if (q == null) {
      q = Collections.unmodifiableList(m);
    }
    return q;
  }
  
  public final void d(String paramString)
  {
    mRow).altName = paramString;
  }
  
  public final StructuredName e()
  {
    return g;
  }
  
  public final void e(String paramString)
  {
    mRow).cacheControl = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof Contact)) {
      return false;
    }
    paramObject = (Contact)paramObject;
    if (!am.a(mRow).defaultNumber, mRow).defaultNumber)) {
      return false;
    }
    if (O() == ((Contact)paramObject).O())
    {
      if (n.size() != n.size()) {
        return false;
      }
      Iterator localIterator1 = n.iterator();
      while (localIterator1.hasNext())
      {
        Number localNumber1 = (Number)localIterator1.next();
        Iterator localIterator2 = n.iterator();
        while (localIterator2.hasNext())
        {
          Number localNumber2 = (Number)localIterator2.next();
          if (localNumber1.a().equals(localNumber2.a()))
          {
            i1 = 1;
            break label169;
          }
        }
        int i1 = 0;
        label169:
        if (i1 == 0) {
          return false;
        }
      }
      return am.a(z(), ((Contact)paramObject).z(), true) == 0;
    }
    return false;
  }
  
  public final Style f()
  {
    return j;
  }
  
  public final void f(String paramString)
  {
    mRow).companyName = paramString;
  }
  
  public final Address g()
  {
    Iterator localIterator = m.iterator();
    Object localObject = null;
    while (localIterator.hasNext())
    {
      Address localAddress = (Address)localIterator.next();
      localObject = localAddress;
      if (localAddress.getDataPhonebookId() != null) {
        return localAddress;
      }
    }
    return (Address)localObject;
  }
  
  @Deprecated
  public final void g(String paramString)
  {
    mRow).defaultNumber = paramString;
  }
  
  public int getSource()
  {
    return mRow).source;
  }
  
  public String getTcId()
  {
    return mRow).id;
  }
  
  public final String h()
  {
    return mRow).about;
  }
  
  public final void h(String paramString)
  {
    mRow).gender = paramString;
  }
  
  public final String i()
  {
    return mRow).access;
  }
  
  public final void i(String paramString)
  {
    mRow).handle = paramString;
  }
  
  public final String j()
  {
    return mRow).imId;
  }
  
  public final void j(String paramString)
  {
    mRow).image = paramString;
  }
  
  public final Long k()
  {
    if (mRow).aggregatedRowId == 0L) {
      return null;
    }
    return Long.valueOf(mRow).aggregatedRowId);
  }
  
  public final void k(String paramString)
  {
    mRow).jobTitle = paramString;
  }
  
  public final String l()
  {
    return mRow).altName;
  }
  
  public final void l(String paramString)
  {
    mRow).name = paramString;
  }
  
  public final int m()
  {
    return f;
  }
  
  public final void m(String paramString)
  {
    mRow).phonebookLookupKey = paramString;
  }
  
  public final String n()
  {
    return mRow).cacheControl;
  }
  
  public final void n(String paramString)
  {
    mRow).searchQuery = paramString;
  }
  
  public final String o()
  {
    return mRow).companyName;
  }
  
  public final void o(String paramString)
  {
    mRow).transliteratedName = paramString;
  }
  
  @Deprecated
  public final String p()
  {
    if (am.a(mRow).defaultNumber)) {
      return mRow).defaultNumber;
    }
    Iterator localIterator = n.iterator();
    do
    {
      if (!localIterator.hasNext()) {
        break;
      }
      Number localNumber = (Number)localIterator.next();
      mRow).defaultNumber = am.b(new String[] { localNumber.a(), localNumber.d(), localNumber.c() });
    } while (am.b(mRow).defaultNumber));
    return mRow).defaultNumber;
  }
  
  public final boolean p(String paramString)
  {
    if ((S()) && (p != null) && (ab.e(paramString)))
    {
      Iterator localIterator = p.iterator();
      while (localIterator.hasNext())
      {
        Number localNumber = (Number)localIterator.next();
        if ((paramString.equals(localNumber.a())) && ((localNumber.getSource() & 0xD) != 0)) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final String q()
  {
    Number localNumber = r();
    if (localNumber != null) {
      return localNumber.n();
    }
    if (!n.isEmpty()) {
      return ((Number)n.get(0)).n();
    }
    if (!am.b(mRow).defaultNumber)) {
      return aa.e(mRow).defaultNumber);
    }
    return mRow).defaultNumber;
  }
  
  @Deprecated
  public final Number r()
  {
    String str = p();
    if (!am.b(str))
    {
      Iterator localIterator = n.iterator();
      while (localIterator.hasNext())
      {
        Number localNumber = (Number)localIterator.next();
        if (str.equals(localNumber.a())) {
          return localNumber;
        }
      }
    }
    return null;
  }
  
  public final String s()
  {
    String str2 = u();
    String str1 = str2;
    if (am.b(str2))
    {
      str2 = q();
      str1 = str2;
      if (am.b(str2)) {
        str1 = g.a;
      }
    }
    return str1;
  }
  
  public void setSource(int paramInt)
  {
    mRow).source = paramInt;
  }
  
  public void setTcId(String paramString)
  {
    super.setTcId(paramString);
    mRow).id = paramString;
  }
  
  public final String t()
  {
    String str = af();
    if (str != null) {
      return str;
    }
    return z();
  }
  
  public final String u()
  {
    Object localObject = af();
    if (localObject != null) {
      return (String)localObject;
    }
    String str = z();
    if (Z()) {
      return str;
    }
    if (!am.b(K()))
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append(str);
      ((StringBuilder)localObject).append(" (");
      ((StringBuilder)localObject).append(K());
      ((StringBuilder)localObject).append(")");
      return ((StringBuilder)localObject).toString();
    }
    localObject = str;
    if (!am.b(l()))
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append(str);
      ((StringBuilder)localObject).append(" (");
      ((StringBuilder)localObject).append(l());
      ((StringBuilder)localObject).append(")");
      localObject = ((StringBuilder)localObject).toString();
    }
    return (String)localObject;
  }
  
  public final String v()
  {
    return mRow).image;
  }
  
  public final String w()
  {
    return am.a(" @ ", new CharSequence[] { x(), o() });
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.provideAs(TypeTransformer.java:780)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e1expr(TypeTransformer.java:496)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:713)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.enexpr(TypeTransformer.java:698)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:719)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.s1stmt(TypeTransformer.java:810)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.sxStmt(TypeTransformer.java:840)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:206)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
  }
  
  public final String x()
  {
    return mRow).jobTitle;
  }
  
  public final List<Link> y()
  {
    if (t == null) {
      t = Collections.unmodifiableList(o);
    }
    return t;
  }
  
  public final String z()
  {
    return mRow).name;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.entity.Contact
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */