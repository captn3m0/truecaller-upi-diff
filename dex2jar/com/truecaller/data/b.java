package com.truecaller.data;

import android.content.Context;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.service.RefreshContactIndexingService.a;
import com.truecaller.service.RefreshT9MappingService;
import com.truecaller.service.RefreshT9MappingService.a;

public final class b
  extends com.truecaller.common.c.b
{
  private final Context a;
  
  public b()
  {
    super(null, 2000L);
    Object localObject = TrueApp.y();
    k.a(localObject, "TrueApp.getApp()");
    localObject = ((TrueApp)localObject).a().l();
    k.a(localObject, "TrueApp.getApp().objectsGraph.applicationContext()");
    a = ((Context)localObject);
  }
  
  public final void a()
  {
    RefreshT9MappingService.a locala = RefreshT9MappingService.j;
    RefreshT9MappingService.a.a(a);
    new RefreshContactIndexingService.a(a).a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */