package com.truecaller.data.access;

import android.os.CancellationSignal;
import c.n;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import java.util.List;

public abstract interface i
{
  public abstract CancellationSignal a(String paramString, Integer paramInteger, i.a<List<n<Contact, String>>> parama);
  
  public abstract n<Contact, Number> a(String paramString);
  
  public abstract List<n<Contact, String>> a(String paramString, Integer paramInteger);
  
  public abstract void a(i.a<Integer> parama);
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */