package com.truecaller.data.access;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.net.Uri.Builder;
import android.provider.ContactsContract.Data;
import android.text.TextUtils;
import com.google.c.a.k.d;
import com.google.gson.f;
import com.truecaller.content.TruecallerContract.ae;
import com.truecaller.content.TruecallerContract.ah;
import com.truecaller.content.TruecallerContract.k;
import com.truecaller.data.entity.Address;
import com.truecaller.data.entity.Business;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Entity;
import com.truecaller.data.entity.Link;
import com.truecaller.data.entity.Note;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.RowEntity;
import com.truecaller.data.entity.Source;
import com.truecaller.data.entity.StructuredName;
import com.truecaller.data.entity.Style;
import com.truecaller.data.entity.Tag;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.search.ContactDto.Contact;
import com.truecaller.search.ContactDto.Contact.PhoneNumber;
import com.truecaller.search.ContactDto.Contact.Source;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.c.a.a.a.b.a;
import org.c.a.a.a.k;

public final class m
  extends h
{
  private boolean c = true;
  private boolean d = false;
  
  public m(Context paramContext)
  {
    super(paramContext);
  }
  
  private static void a(List<ContentProviderOperation> paramList, String paramString)
  {
    String[] arrayOfString = new String[1];
    arrayOfString[0] = paramString;
    paramList.add(ContentProviderOperation.newDelete(TruecallerContract.k.a()).withSelection("tc_id=?", arrayOfString).build());
    paramList.add(ContentProviderOperation.newDelete(TruecallerContract.ah.a()).withSelection("tc_id=?", arrayOfString).build());
  }
  
  private void a(List<ContentProviderOperation> paramList, List<Entity> paramList1, Contact paramContact, boolean paramBoolean)
  {
    if (paramContact != null)
    {
      if (d) {
        return;
      }
      paramList1.add(paramContact);
      Object localObject2 = TruecallerContract.ah.a();
      Object localObject1 = localObject2;
      if (!c) {
        localObject1 = ((Uri)localObject2).buildUpon().appendQueryParameter("aggregation", "false").build();
      }
      if ((!d) && (paramContact.R()) && (!TextUtils.isEmpty(paramContact.getTcId()))) {
        paramList.add(ContentProviderOperation.newAssertQuery(TruecallerContract.ah.a()).withSelection("tc_id=? AND contact_access LIKE ?", new String[] { paramContact.getTcId(), "public" }).withExpectedCount(0).build());
      }
      int i = paramList.size();
      Object localObject3 = ContentProviderOperation.newInsert((Uri)localObject1);
      boolean bool = c;
      Object localObject4 = new ContentValues();
      if (paramContact.getTcId() == null) {
        paramContact.setTcId(UUID.randomUUID().toString());
      }
      if (!bool) {
        ((ContentValues)localObject4).put("aggregated_contact_id", paramContact.k());
      }
      ((ContentValues)localObject4).put("tc_id", paramContact.getTcId());
      ((ContentValues)localObject4).put("contact_name", paramContact.z());
      ((ContentValues)localObject4).put("contact_transliterated_name", paramContact.K());
      ((ContentValues)localObject4).put("contact_is_favorite", Boolean.valueOf(paramContact.X()));
      int j = mRow).favoritePosition;
      localObject2 = null;
      if (j >= 0) {
        localObject1 = Integer.valueOf(mRow).favoritePosition);
      } else {
        localObject1 = null;
      }
      ((ContentValues)localObject4).put("contact_favorite_position", (Integer)localObject1);
      ((ContentValues)localObject4).put("contact_handle", mRow).handle);
      ((ContentValues)localObject4).put("contact_alt_name", paramContact.l());
      ((ContentValues)localObject4).put("contact_gender", mRow).gender);
      ((ContentValues)localObject4).put("contact_about", paramContact.h());
      ((ContentValues)localObject4).put("contact_image_url", paramContact.v());
      ((ContentValues)localObject4).put("contact_job_title", paramContact.x());
      ((ContentValues)localObject4).put("contact_company", paramContact.o());
      ((ContentValues)localObject4).put("contact_access", paramContact.i());
      ((ContentValues)localObject4).put("contact_common_connections", Integer.valueOf(mRow).commonConnections));
      ((ContentValues)localObject4).put("contact_search_time", Long.valueOf(paramContact.H()));
      ((ContentValues)localObject4).put("contact_source", Integer.valueOf(paramContact.getSource()));
      ((ContentValues)localObject4).put("contact_default_number", paramContact.p());
      ((ContentValues)localObject4).put("contact_phonebook_id", paramContact.E());
      if (mRow).phonebookHash == 0L) {
        localObject1 = localObject2;
      } else {
        localObject1 = Long.valueOf(mRow).phonebookHash);
      }
      ((ContentValues)localObject4).put("contact_phonebook_hash", (Long)localObject1);
      ((ContentValues)localObject4).put("contact_phonebook_lookup", paramContact.F());
      ((ContentValues)localObject4).put("search_query", paramContact.G());
      ((ContentValues)localObject4).put("cache_control", paramContact.n());
      ((ContentValues)localObject4).put("contact_badges", Integer.valueOf(f));
      ((ContentValues)localObject4).put("tc_flag", Integer.valueOf(k));
      ((ContentValues)localObject4).put("contact_im_id", paramContact.j());
      paramList.add(((ContentProviderOperation.Builder)localObject3).withValues((ContentValues)localObject4).withYieldAllowed(paramBoolean).build());
      localObject2 = paramContact.A().iterator();
      while (((Iterator)localObject2).hasNext())
      {
        localObject3 = (Number)((Iterator)localObject2).next();
        paramList1.add(localObject3);
        localObject4 = ContentProviderOperation.newInsert(TruecallerContract.k.a());
        ContentValues localContentValues = a((RowEntity)localObject3, paramContact);
        localContentValues.put("data1", ((Number)localObject3).a());
        localContentValues.put("data2", ((Number)localObject3).c());
        localContentValues.put("data9", ((Number)localObject3).d());
        localContentValues.put("data3", Integer.valueOf(((Number)localObject3).h()));
        localContentValues.put("data4", Integer.valueOf(((Number)localObject3).i()));
        localContentValues.put("data5", ((Number)localObject3).k());
        localContentValues.put("data6", Integer.valueOf(a.a(mRow).dialingCode)));
        localContentValues.put("data7", ((Number)localObject3).l());
        if (((Number)localObject3).m() != null) {
          localObject1 = ((Number)localObject3).m();
        } else {
          localObject1 = k.d.l;
        }
        localContentValues.put("data8", ((k.d)localObject1).name());
        localContentValues.put("data10", ((Number)localObject3).f());
        localContentValues.put("data_type", Integer.valueOf(4));
        paramList.add(((ContentProviderOperation.Builder)localObject4).withValues(localContentValues).withValueBackReference("data_raw_contact_id", i).build());
        if ((!c) && (!k.b(((Number)localObject3).a()))) {
          paramList.add(ContentProviderOperation.newUpdate(TruecallerContract.ae.a()).withSelection("normalized_destination=?", new String[] { ((Number)localObject3).a() }).withValue("aggregated_contact_id", paramContact.k()).build());
        }
      }
      localObject1 = paramContact.d().iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (Address)((Iterator)localObject1).next();
        paramList1.add(localObject2);
        localObject3 = ContentProviderOperation.newInsert(TruecallerContract.k.a());
        localObject4 = a((RowEntity)localObject2, paramContact);
        ((ContentValues)localObject4).put("data1", ((Address)localObject2).getStreet());
        ((ContentValues)localObject4).put("data2", ((Address)localObject2).getZipCode());
        ((ContentValues)localObject4).put("data3", ((Address)localObject2).getCity());
        ((ContentValues)localObject4).put("data4", ((Address)localObject2).getCountryCode());
        ((ContentValues)localObject4).put("data5", Integer.valueOf(((Address)localObject2).getType()));
        ((ContentValues)localObject4).put("data6", ((Address)localObject2).getTypeLabel());
        ((ContentValues)localObject4).put("data7", ((Address)localObject2).getTimeZone());
        ((ContentValues)localObject4).put("data8", ((Address)localObject2).getArea());
        ((ContentValues)localObject4).put("data_type", Integer.valueOf(1));
        paramList.add(((ContentProviderOperation.Builder)localObject3).withValues((ContentValues)localObject4).withValueBackReference("data_raw_contact_id", i).build());
      }
      localObject1 = paramContact.J().iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (Tag)((Iterator)localObject1).next();
        paramList1.add(localObject2);
        localObject3 = ContentProviderOperation.newInsert(TruecallerContract.k.a());
        localObject4 = a((RowEntity)localObject2, paramContact);
        ((ContentValues)localObject4).put("data1", ((Tag)localObject2).a());
        ((ContentValues)localObject4).put("data_type", Integer.valueOf(6));
        paramList.add(((ContentProviderOperation.Builder)localObject3).withValues((ContentValues)localObject4).withValueBackReference("data_raw_contact_id", i).build());
      }
      localObject1 = paramContact.y().iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (Link)((Iterator)localObject1).next();
        paramList1.add(localObject2);
        localObject3 = ContentProviderOperation.newInsert(TruecallerContract.k.a());
        localObject4 = a((RowEntity)localObject2, paramContact);
        ((ContentValues)localObject4).put("data1", ((Link)localObject2).getInfo());
        ((ContentValues)localObject4).put("data2", ((Link)localObject2).getService());
        ((ContentValues)localObject4).put("data3", ((Link)localObject2).getCaption());
        ((ContentValues)localObject4).put("data_type", Integer.valueOf(3));
        paramList.add(((ContentProviderOperation.Builder)localObject3).withValues((ContentValues)localObject4).withValueBackReference("data_raw_contact_id", i).build());
      }
      if (e == null) {
        e = Collections.unmodifiableList(b);
      }
      localObject1 = e.iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject4 = (Source)((Iterator)localObject1).next();
        paramList1.add(localObject4);
        localObject2 = ContentProviderOperation.newInsert(TruecallerContract.k.a());
        localObject3 = a((RowEntity)localObject4, paramContact);
        ((ContentValues)localObject3).put("data1", ((Source)localObject4).a());
        ((ContentValues)localObject3).put("data2", ((Source)localObject4).b());
        ((ContentValues)localObject3).put("data3", ((Source)localObject4).c());
        ((ContentValues)localObject3).put("data4", ((Source)localObject4).d());
        ((ContentValues)localObject3).put("data_type", Integer.valueOf(5));
        localObject4 = mRow).extra;
        if ((localObject4 != null) && (!((Map)localObject4).isEmpty())) {
          ((ContentValues)localObject3).put("data5", new f().b(localObject4));
        }
        paramList.add(((ContentProviderOperation.Builder)localObject2).withValues((ContentValues)localObject3).withValueBackReference("data_raw_contact_id", i).build());
      }
      localObject1 = g;
      if (localObject1 != null)
      {
        paramList1.add(localObject1);
        localObject2 = ContentProviderOperation.newInsert(TruecallerContract.k.a());
        localObject3 = a((RowEntity)localObject1, paramContact);
        ((ContentValues)localObject3).put("data1", ((StructuredName)localObject1).getGivenName());
        ((ContentValues)localObject3).put("data2", ((StructuredName)localObject1).getFamilyName());
        ((ContentValues)localObject3).put("data3", ((StructuredName)localObject1).getMiddleName());
        ((ContentValues)localObject3).put("data_type", Integer.valueOf(7));
        paramList.add(((ContentProviderOperation.Builder)localObject2).withValues((ContentValues)localObject3).withValueBackReference("data_raw_contact_id", i).build());
      }
      localObject1 = h;
      if (localObject1 != null)
      {
        paramList1.add(localObject1);
        localObject2 = ContentProviderOperation.newInsert(TruecallerContract.k.a());
        localObject3 = a((RowEntity)localObject1, paramContact);
        ((ContentValues)localObject3).put("data1", ((Note)localObject1).getValue());
        ((ContentValues)localObject3).put("data_type", Integer.valueOf(8));
        paramList.add(((ContentProviderOperation.Builder)localObject2).withValues((ContentValues)localObject3).withValueBackReference("data_raw_contact_id", i).build());
      }
      localObject1 = i;
      if (localObject1 != null)
      {
        paramList1.add(localObject1);
        localObject2 = ContentProviderOperation.newInsert(TruecallerContract.k.a());
        localObject3 = a((RowEntity)localObject1, paramContact);
        ((ContentValues)localObject3).put("data1", ((Business)localObject1).getBranch());
        ((ContentValues)localObject3).put("data2", ((Business)localObject1).getDepartment());
        ((ContentValues)localObject3).put("data3", ((Business)localObject1).getCompanySize());
        ((ContentValues)localObject3).put("data4", ((Business)localObject1).getOpeningHours());
        ((ContentValues)localObject3).put("data5", ((Business)localObject1).getLandline());
        ((ContentValues)localObject3).put("data6", ((Business)localObject1).getScore());
        ((ContentValues)localObject3).put("data7", ((Business)localObject1).getSwishNumber());
        ((ContentValues)localObject3).put("data_type", Integer.valueOf(9));
        paramList.add(((ContentProviderOperation.Builder)localObject2).withValues((ContentValues)localObject3).withValueBackReference("data_raw_contact_id", i).build());
      }
      localObject1 = j;
      if (localObject1 != null)
      {
        paramList1.add(localObject1);
        paramList1 = ContentProviderOperation.newInsert(TruecallerContract.k.a());
        paramContact = a((RowEntity)localObject1, paramContact);
        paramContact.put("data1", ((Style)localObject1).getBackgroundColor());
        paramContact.put("data2", ((Style)localObject1).getImageUrls());
        paramContact.put("data_type", Integer.valueOf(10));
        paramList.add(paramList1.withValues(paramContact).withValueBackReference("data_raw_contact_id", i).build());
      }
      return;
    }
  }
  
  public final Cursor a()
  {
    return b.query(TruecallerContract.ah.a(), new String[] { "tc_id", "contact_phonebook_id", "contact_phonebook_hash" }, "contact_phonebook_hash IS NOT NULL", null, "contact_phonebook_id ASC");
  }
  
  public final Contact a(Uri paramUri, String paramString, String... paramVarArgs)
  {
    ContentResolver localContentResolver = b;
    int i = paramVarArgs.length;
    Object localObject = null;
    if (i == 0) {
      paramVarArgs = null;
    }
    paramString = localContentResolver.query(paramUri, null, paramString, paramVarArgs, null);
    if (paramString != null)
    {
      paramUri = (Uri)localObject;
      try
      {
        if (paramString.moveToFirst())
        {
          paramVarArgs = new e(paramString);
          paramVarArgs.a(false);
          paramUri = paramVarArgs.a(paramString);
          boolean bool;
          do
          {
            paramVarArgs.a(paramString, paramUri);
            bool = paramString.moveToNext();
          } while (bool);
        }
        return paramUri;
      }
      finally
      {
        paramString.close();
      }
    }
    return null;
  }
  
  public final Contact a(String paramString)
  {
    return a(TruecallerContract.ah.b(), "tc_id=?", new String[] { paramString });
  }
  
  public final List<Contact> a(long paramLong)
  {
    ArrayList localArrayList = new ArrayList();
    Cursor localCursor;
    if (paramLong > 0L)
    {
      localCursor = b.query(TruecallerContract.ah.b(), null, "aggregated_contact_id=?", new String[] { String.valueOf(paramLong) }, null);
      if (localCursor == null) {}
    }
    for (;;)
    {
      int i;
      try
      {
        if (localCursor.moveToFirst())
        {
          e locale = new e(localCursor);
          locale.a(false);
          Contact localContact = null;
          i = 0;
          break label233;
          paramLong = localContact.getId().longValue();
          long l = locale.c(localCursor);
          if (localContact != null)
          {
            j = i;
            if (paramLong == l) {}
          }
          else
          {
            localContact = locale.a(localCursor);
            localArrayList.add(localContact);
            j = i;
          }
          locale.a(localCursor, localContact);
          boolean bool = localCursor.moveToNext();
          if (bool)
          {
            paramLong = locale.c(localCursor);
            if (paramLong == l)
            {
              bool = true;
              i = j;
            }
            else
            {
              bool = false;
              i = j;
            }
          }
          else
          {
            i = 1;
          }
          int j = i;
          if (bool) {
            continue;
          }
          break label233;
        }
        return localArrayList;
      }
      finally
      {
        localCursor.close();
      }
      return localArrayList;
      label233:
      if (i == 0) {
        if (localObject == null) {
          paramLong = 0L;
        }
      }
    }
  }
  
  public final void a(String paramString, Long paramLong)
  {
    if ((paramLong != null) && (paramLong.longValue() > 0L))
    {
      localObject = new ContentValues();
      ((ContentValues)localObject).put("is_super_primary", Integer.valueOf(1));
      paramLong = ContentUris.withAppendedId(ContactsContract.Data.CONTENT_URI, paramLong.longValue());
      b.update(paramLong, (ContentValues)localObject, null, null);
    }
    paramLong = new ContentValues();
    paramLong.put("data_is_primary", Integer.valueOf(1));
    Object localObject = TruecallerContract.k.a();
    b.update((Uri)localObject, paramLong, "_id=?", new String[] { paramString });
  }
  
  public final void a(List<String> paramList)
  {
    ArrayList localArrayList = new ArrayList();
    paramList = paramList.iterator();
    while (paramList.hasNext()) {
      a(localArrayList, (String)paramList.next());
    }
    a(localArrayList, Collections.emptyList());
  }
  
  public final void a(List<String> paramList, List<Contact> paramList1)
  {
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    paramList = paramList.iterator();
    while (paramList.hasNext()) {
      a(localArrayList1, (String)paramList.next());
    }
    paramList = paramList1.iterator();
    int i = 0;
    while (paramList.hasNext())
    {
      paramList1 = (Contact)paramList.next();
      boolean bool = true;
      i += 1;
      if (i % 5 != 0) {
        bool = false;
      }
      a(localArrayList1, localArrayList2, paramList1, bool);
    }
    a(localArrayList1, localArrayList2);
  }
  
  public final boolean a(Contact paramContact)
  {
    return b(Collections.singletonList(paramContact));
  }
  
  public final boolean a(String paramString, int... paramVarArgs)
  {
    boolean bool;
    if (paramVarArgs.length == 0) {
      bool = true;
    } else {
      bool = false;
    }
    AssertionUtil.AlwaysFatal.isFalse(bool, new String[] { "At least one source is required" });
    String[] arrayOfString = new String[paramVarArgs.length + 1];
    arrayOfString[0] = paramString;
    paramString = new StringBuilder();
    paramString.append("data1=? AND data_type=4 AND contact_source IN (?");
    arrayOfString[1] = String.valueOf(paramVarArgs[0]);
    int j;
    for (int i = 1; i < paramVarArgs.length; i = j)
    {
      paramString.append(",?");
      j = i + 1;
      arrayOfString[j] = String.valueOf(paramVarArgs[i]);
    }
    paramString.append(")");
    paramVarArgs = b;
    Uri localUri = TruecallerContract.ah.b();
    paramString = paramString.toString();
    paramString = paramVarArgs.query(localUri, new String[] { "tc_id" }, paramString, arrayOfString, null);
    paramVarArgs = new ArrayList();
    if (paramString != null) {}
    try
    {
      while (paramString.moveToNext()) {
        paramVarArgs.add(paramString.getString(0));
      }
      paramString.close();
    }
    finally
    {
      paramString.close();
    }
    return !paramVarArgs.isEmpty();
  }
  
  public final boolean b(List<Contact> paramList)
  {
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    paramList = paramList.iterator();
    int i = 0;
    while (paramList.hasNext())
    {
      Contact localContact = (Contact)paramList.next();
      boolean bool = true;
      i += 1;
      if (i % 5 != 0) {
        bool = false;
      }
      a(localArrayList1, localArrayList2, localContact, bool);
    }
    return a(localArrayList1, localArrayList2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */