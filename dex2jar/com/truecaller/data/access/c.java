package com.truecaller.data.access;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.net.Uri.Builder;
import android.text.TextUtils;
import com.truecaller.common.c.b.b;
import com.truecaller.content.TruecallerContract.a;
import com.truecaller.content.TruecallerContract.ah;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.data.entity.Contact;

public class c
  extends h
{
  private static UriMatcher c;
  
  public c(Context paramContext)
  {
    super(paramContext);
  }
  
  private Contact a(Uri paramUri, String paramString, String... paramVarArgs)
  {
    paramString = b.query(paramUri, null, paramString, paramVarArgs, null);
    paramUri = null;
    if (paramString != null) {
      try
      {
        if (paramString.moveToFirst())
        {
          paramVarArgs = new e(paramString);
          paramVarArgs.a(true);
          paramUri = paramVarArgs.a(paramString);
          do
          {
            paramVarArgs.a(paramString, paramUri);
          } while (paramString.moveToNext());
          paramUri.ad();
        }
        return paramUri;
      }
      finally
      {
        paramString.close();
      }
    }
    return null;
  }
  
  private static void a(UriMatcher paramUriMatcher, Uri paramUri, int paramInt)
  {
    String str = paramUri.getAuthority();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(TextUtils.join("/", paramUri.getPathSegments()));
    localStringBuilder.append("/#");
    paramUriMatcher.addURI(str, localStringBuilder.toString(), paramInt);
  }
  
  private static UriMatcher b()
  {
    if (c == null) {
      try
      {
        if (c == null)
        {
          UriMatcher localUriMatcher = new UriMatcher(-1);
          c = localUriMatcher;
          a(localUriMatcher, TruecallerContract.ah.a(), 1);
          a(c, TruecallerContract.a.a(), 2);
          a(c, TruecallerContract.ah.b(), 1);
          a(c, TruecallerContract.a.b(), 2);
          a(c, TruecallerContract.n.a(), 3);
          a(c, TruecallerContract.n.c(), 3);
          a(c, TruecallerContract.n.d(), 3);
        }
      }
      finally {}
    }
    return c;
  }
  
  public static boolean b(Contact paramContact)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (paramContact != null)
    {
      bool1 = bool2;
      if (c != null)
      {
        bool1 = bool2;
        if (d)
        {
          int i = b().match(c);
          if (i != 2)
          {
            bool1 = bool2;
            if (i != 3) {}
          }
          else
          {
            bool1 = true;
          }
        }
      }
    }
    return bool1;
  }
  
  private Contact c(long paramLong)
  {
    return a(a(TruecallerContract.a.b(), "_id", "aggregated_raw_contact_id=?", new String[] { String.valueOf(paramLong) }));
  }
  
  public final int a()
  {
    return b.a(b, TruecallerContract.a.a(), null, null);
  }
  
  public final long a(Uri paramUri, String paramString1, String paramString2, String... paramVarArgs)
  {
    ContentResolver localContentResolver = b;
    String[] arrayOfString = paramVarArgs;
    if (paramVarArgs.length == 0) {
      arrayOfString = null;
    }
    paramUri = localContentResolver.query(paramUri, new String[] { paramString1 }, paramString2, arrayOfString, null);
    long l = -1L;
    if (paramUri != null) {
      try
      {
        if (paramUri.moveToNext()) {
          l = paramUri.getLong(0);
        }
        return l;
      }
      finally
      {
        paramUri.close();
      }
    }
    return -1L;
  }
  
  public final Contact a(long paramLong)
  {
    if (paramLong < 1L) {
      return null;
    }
    return a(TruecallerContract.a.b(), "_id=?", new String[] { String.valueOf(paramLong) });
  }
  
  public final Contact a(Uri paramUri)
  {
    if (paramUri == null) {
      return null;
    }
    int i = b().match(paramUri);
    if (i == -1) {
      return null;
    }
    long l = ContentUris.parseId(paramUri);
    switch (i)
    {
    default: 
      return null;
    case 3: 
      return b(l);
    case 2: 
      return a(l);
    }
    return c(l);
  }
  
  public final Contact a(Contact paramContact)
  {
    if (paramContact == null) {
      return null;
    }
    Object localObject = paramContact.getTcId();
    if (localObject != null)
    {
      localObject = a((String)localObject);
      if (localObject != null) {
        return (Contact)localObject;
      }
    }
    localObject = paramContact.k();
    if (localObject != null)
    {
      localObject = a(((Long)localObject).longValue());
      if (localObject != null) {
        return (Contact)localObject;
      }
    }
    localObject = c;
    if (localObject != null)
    {
      localObject = a((Uri)localObject);
      if (localObject != null) {
        return (Contact)localObject;
      }
    }
    paramContact = paramContact.E();
    if (paramContact != null)
    {
      long l = paramContact.longValue();
      paramContact = a(a(TruecallerContract.ah.a().buildUpon().appendQueryParameter("limit", "1").build(), "aggregated_contact_id", "contact_phonebook_id=".concat(String.valueOf(l)), new String[0]));
      if (paramContact != null) {
        return paramContact;
      }
    }
    return null;
  }
  
  public final Contact a(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return a(a(TruecallerContract.ah.a(), "aggregated_contact_id", "tc_id=?", new String[] { paramString }));
  }
  
  public final Contact b(long paramLong)
  {
    return a(a(TruecallerContract.n.d(), "history_aggregated_contact_id", "_id=?", new String[] { String.valueOf(paramLong) }));
  }
  
  public final Contact b(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return a(a(TruecallerContract.ah.b().buildUpon().appendQueryParameter("limit", "1").build(), "aggregated_contact_id", "data1=? AND data_type=4", new String[] { paramString }));
  }
  
  public final Contact c(Contact paramContact)
  {
    Contact localContact = a(paramContact);
    if (localContact != null) {
      return localContact;
    }
    if ((!d) && (Integer.bitCount(paramContact.getSource()) == 1) && (!TextUtils.isEmpty(paramContact.getTcId())))
    {
      new m(a).a(paramContact);
      return a(paramContact);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */