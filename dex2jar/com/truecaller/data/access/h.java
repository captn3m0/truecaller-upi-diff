package com.truecaller.data.access;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.sqlite.SQLiteException;
import android.os.RemoteException;
import com.truecaller.content.TruecallerContract;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Entity;
import com.truecaller.data.entity.RowEntity;
import com.truecaller.log.d;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class h
{
  final Context a;
  public final ContentResolver b;
  
  protected h(Context paramContext)
  {
    a = paramContext.getApplicationContext();
    b = a.getContentResolver();
  }
  
  static ContentValues a(RowEntity paramRowEntity, Contact paramContact)
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.provideAs(TypeTransformer.java:780)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.enexpr(TypeTransformer.java:659)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:719)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.enexpr(TypeTransformer.java:698)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:719)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.enexpr(TypeTransformer.java:698)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:719)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.s1stmt(TypeTransformer.java:810)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.sxStmt(TypeTransformer.java:840)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:206)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
  }
  
  final boolean a(ArrayList<ContentProviderOperation> paramArrayList, List<Entity> paramList)
  {
    if (paramArrayList.isEmpty()) {
      return true;
    }
    for (;;)
    {
      int i;
      try
      {
        try
        {
          paramArrayList = b.applyBatch(TruecallerContract.a, paramArrayList);
          paramList = paramList.iterator();
          int j = paramArrayList.length;
          i = 0;
          if (i < j)
          {
            Object localObject = paramArrayList[i];
            if (uri == null) {
              break label114;
            }
            ((Entity)paramList.next()).setId(Long.valueOf(ContentUris.parseId(uri)));
            break label114;
          }
          boolean bool = paramList.hasNext();
          return !bool;
        }
        catch (SQLiteException paramArrayList) {}catch (IllegalArgumentException paramArrayList) {}catch (RemoteException paramArrayList) {}
        d.a(paramArrayList);
        return false;
      }
      catch (OperationApplicationException paramArrayList)
      {
        return false;
      }
      label114:
      i += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */