package com.truecaller.data.access;

import android.content.ContentResolver;
import android.os.CancellationSignal;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import c.n;
import c.t;
import com.truecaller.common.h.u;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;

public final class j
  implements i
{
  final ContentResolver a;
  final c b;
  final f c;
  private final u d;
  private final f e;
  
  public j(ContentResolver paramContentResolver, u paramu, c paramc, f paramf1, f paramf2)
  {
    a = paramContentResolver;
    d = paramu;
    b = paramc;
    c = paramf1;
    e = paramf2;
  }
  
  public final CancellationSignal a(String paramString, Integer paramInteger, i.a<List<n<Contact, String>>> parama)
  {
    k.b(paramString, "filter");
    k.b(parama, "callback");
    CancellationSignal localCancellationSignal = new CancellationSignal();
    e.b((ag)bg.a, e, (m)new j.c(this, paramString, paramInteger, localCancellationSignal, parama, null), 2);
    return localCancellationSignal;
  }
  
  public final n<Contact, Number> a(String paramString)
  {
    k.b(paramString, "numberString");
    Object localObject1 = d.b(paramString);
    if (localObject1 != null) {
      paramString = (String)localObject1;
    }
    Contact localContact = b.b(paramString);
    Iterator localIterator = null;
    Object localObject2 = null;
    localObject1 = localIterator;
    if (localContact != null)
    {
      Object localObject3 = localContact.A();
      localObject1 = localIterator;
      if (localObject3 != null)
      {
        localIterator = ((Iterable)localObject3).iterator();
        do
        {
          localObject1 = localObject2;
          if (!localIterator.hasNext()) {
            break;
          }
          localObject1 = localIterator.next();
          localObject3 = (Number)localObject1;
          k.a(localObject3, "it");
        } while (!k.a(((Number)localObject3).a(), paramString));
        localObject1 = (Number)localObject1;
      }
    }
    return t.a(localContact, localObject1);
  }
  
  /* Error */
  public final List<n<Contact, String>> a(String paramString, Integer paramInteger)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 49
    //   3: invokestatic 24	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_1
    //   7: invokestatic 148	com/truecaller/content/TruecallerContract$a:b	(Ljava/lang/String;)Landroid/net/Uri;
    //   10: astore_1
    //   11: aload_2
    //   12: ifnull +237 -> 249
    //   15: aload_2
    //   16: checkcast 150	java/lang/Number
    //   19: invokevirtual 154	java/lang/Number:intValue	()I
    //   22: istore_3
    //   23: aload_1
    //   24: invokevirtual 160	android/net/Uri:buildUpon	()Landroid/net/Uri$Builder;
    //   27: ldc -94
    //   29: ldc -92
    //   31: iload_3
    //   32: invokestatic 170	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   35: invokevirtual 173	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   38: invokevirtual 179	android/net/Uri$Builder:appendQueryParameter	(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;
    //   41: invokevirtual 183	android/net/Uri$Builder:build	()Landroid/net/Uri;
    //   44: astore_2
    //   45: aload_2
    //   46: ifnonnull +198 -> 244
    //   49: goto +200 -> 249
    //   52: aload_0
    //   53: getfield 37	com/truecaller/data/access/j:a	Landroid/content/ContentResolver;
    //   56: aload_1
    //   57: aconst_null
    //   58: aconst_null
    //   59: aconst_null
    //   60: aconst_null
    //   61: invokevirtual 189	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   64: astore_1
    //   65: aload_1
    //   66: ifnull +157 -> 223
    //   69: new 191	com/truecaller/data/a
    //   72: dup
    //   73: aload_1
    //   74: invokespecial 194	com/truecaller/data/a:<init>	(Landroid/database/Cursor;)V
    //   77: checkcast 196	android/database/Cursor
    //   80: astore 6
    //   82: aload 6
    //   84: checkcast 198	java/io/Closeable
    //   87: astore 5
    //   89: aconst_null
    //   90: astore 4
    //   92: aload 4
    //   94: astore_1
    //   95: new 200	java/util/ArrayList
    //   98: dup
    //   99: invokespecial 201	java/util/ArrayList:<init>	()V
    //   102: checkcast 203	java/util/Collection
    //   105: astore 7
    //   107: aload 4
    //   109: astore_1
    //   110: aload 6
    //   112: invokeinterface 206 1 0
    //   117: ifeq +57 -> 174
    //   120: aload 4
    //   122: astore_1
    //   123: aload 6
    //   125: checkcast 191	com/truecaller/data/a
    //   128: astore_2
    //   129: aload 4
    //   131: astore_1
    //   132: aload_2
    //   133: invokevirtual 209	com/truecaller/data/a:a	()Lcom/truecaller/data/entity/Contact;
    //   136: astore 8
    //   138: aload 8
    //   140: ifnull +112 -> 252
    //   143: aload 4
    //   145: astore_1
    //   146: aload 8
    //   148: aload_2
    //   149: invokevirtual 211	com/truecaller/data/a:b	()Ljava/lang/String;
    //   152: invokestatic 127	c/t:a	(Ljava/lang/Object;Ljava/lang/Object;)Lc/n;
    //   155: astore_2
    //   156: goto +3 -> 159
    //   159: aload 4
    //   161: astore_1
    //   162: aload 7
    //   164: aload_2
    //   165: invokeinterface 215 2 0
    //   170: pop
    //   171: goto -64 -> 107
    //   174: aload 4
    //   176: astore_1
    //   177: aload 7
    //   179: checkcast 217	java/util/List
    //   182: astore_2
    //   183: aload 5
    //   185: aconst_null
    //   186: invokestatic 222	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   189: aload_2
    //   190: checkcast 96	java/lang/Iterable
    //   193: invokestatic 227	c/a/m:e	(Ljava/lang/Iterable;)Ljava/util/List;
    //   196: astore_2
    //   197: aload_2
    //   198: astore_1
    //   199: aload_2
    //   200: ifnonnull +30 -> 230
    //   203: goto +20 -> 223
    //   206: astore_2
    //   207: goto +8 -> 215
    //   210: astore_2
    //   211: aload_2
    //   212: astore_1
    //   213: aload_2
    //   214: athrow
    //   215: aload 5
    //   217: aload_1
    //   218: invokestatic 222	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   221: aload_2
    //   222: athrow
    //   223: getstatic 232	c/a/y:a	Lc/a/y;
    //   226: checkcast 217	java/util/List
    //   229: astore_1
    //   230: aload_1
    //   231: areturn
    //   232: astore_1
    //   233: aload_1
    //   234: invokestatic 238	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   237: getstatic 232	c/a/y:a	Lc/a/y;
    //   240: checkcast 217	java/util/List
    //   243: areturn
    //   244: aload_2
    //   245: astore_1
    //   246: goto -194 -> 52
    //   249: goto -197 -> 52
    //   252: aconst_null
    //   253: astore_2
    //   254: goto -95 -> 159
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	257	0	this	j
    //   0	257	1	paramString	String
    //   0	257	2	paramInteger	Integer
    //   22	10	3	i	int
    //   90	85	4	localObject	Object
    //   87	129	5	localCloseable	java.io.Closeable
    //   80	44	6	localCursor	android.database.Cursor
    //   105	73	7	localCollection	java.util.Collection
    //   136	11	8	localContact	Contact
    // Exception table:
    //   from	to	target	type
    //   95	107	206	finally
    //   110	120	206	finally
    //   123	129	206	finally
    //   132	138	206	finally
    //   146	156	206	finally
    //   162	171	206	finally
    //   177	183	206	finally
    //   213	215	206	finally
    //   95	107	210	java/lang/Throwable
    //   110	120	210	java/lang/Throwable
    //   123	129	210	java/lang/Throwable
    //   132	138	210	java/lang/Throwable
    //   146	156	210	java/lang/Throwable
    //   162	171	210	java/lang/Throwable
    //   177	183	210	java/lang/Throwable
    //   6	11	232	java/lang/Throwable
    //   15	45	232	java/lang/Throwable
    //   52	65	232	java/lang/Throwable
    //   69	89	232	java/lang/Throwable
    //   183	197	232	java/lang/Throwable
    //   215	223	232	java/lang/Throwable
    //   223	230	232	java/lang/Throwable
  }
  
  public final void a(i.a<Integer> parama)
  {
    k.b(parama, "callback");
    e.b((ag)bg.a, e, (m)new j.a(this, parama, null), 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.data.access.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */