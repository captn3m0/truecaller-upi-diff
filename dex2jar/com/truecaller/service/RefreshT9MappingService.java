package com.truecaller.service;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.af;
import android.support.v4.app.v;
import c.a.m;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.e.c;
import com.truecaller.common.e.d;
import com.truecaller.common.e.f;
import com.truecaller.data.access.s;
import com.truecaller.i.e;
import com.truecaller.i.g;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public final class RefreshT9MappingService
  extends af
{
  public static final RefreshT9MappingService.a j = new RefreshT9MappingService.a((byte)0);
  
  public static final void a(Context paramContext)
  {
    k.b(paramContext, "context");
    Intent localIntent = new Intent(paramContext, RefreshT9MappingService.class).setAction("RefreshT9MappingService.action.sync").putExtra("RefreshT9MappingService.extra.rebuild_all", true);
    v.a(paramContext.getApplicationContext(), RefreshT9MappingService.class, 2131364101, localIntent);
  }
  
  public static final void b(Context paramContext)
  {
    RefreshT9MappingService.a.a(paramContext);
  }
  
  public final void a(Intent paramIntent)
  {
    k.b(paramIntent, "intent");
    Object localObject1 = TrueApp.y();
    k.a(localObject1, "TrueApp.getApp()");
    localObject1 = aHa.a("t9_lang");
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = d.a;
      Object localObject3 = (Iterable)d.f();
      localObject2 = (Collection)new ArrayList(m.a((Iterable)localObject3, 10));
      localObject3 = ((Iterable)localObject3).iterator();
      while (((Iterator)localObject3).hasNext()) {
        ((Collection)localObject2).add(nextb);
      }
      if (!((List)localObject2).contains(localObject1)) {
        localObject1 = null;
      }
      localObject2 = localObject1;
      if (localObject1 != null) {}
    }
    else
    {
      localObject1 = f.a();
      k.a(localObject1, "LocaleManager.getAppLocale()");
      localObject2 = ((Locale)localObject1).getLanguage();
      k.a(localObject2, "LocaleManager.getAppLocale().language");
    }
    localObject1 = paramIntent.getAction();
    if (localObject1 == null) {
      return;
    }
    int i = ((String)localObject1).hashCode();
    if (i != 1164272748)
    {
      if (i != 1517992106) {
        return;
      }
      if (((String)localObject1).equals("RefreshT9MappingService.action.rebuild"))
      {
        paramIntent = paramIntent.getLongArrayExtra("RefreshT9MappingService.extra.scopes");
        if (paramIntent != null)
        {
          localObject1 = getContentResolver();
          k.a(localObject1, "contentResolver");
          new s((ContentResolver)localObject1).a(paramIntent, (String)localObject2);
        }
      }
    }
    else if (((String)localObject1).equals("RefreshT9MappingService.action.sync"))
    {
      localObject1 = getContentResolver();
      k.a(localObject1, "contentResolver");
      localObject1 = new s((ContentResolver)localObject1);
      localObject2 = getApplicationContext();
      k.a(localObject2, "applicationContext");
      ((s)localObject1).a((Context)localObject2, paramIntent.getBooleanExtra("RefreshT9MappingService.extra.rebuild_all", false));
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.RefreshT9MappingService
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */