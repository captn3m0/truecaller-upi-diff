package com.truecaller.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.google.gson.c.a;
import com.truecaller.old.data.access.Settings;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public final class f
  implements e
{
  private Context a;
  private final Map<String, MissedCallReminder> b;
  
  public f(Context arg1)
  {
    a = ???.getApplicationContext();
    ??? = c();
    if (??? == null) {
      ??? = new HashMap();
    }
    b = Collections.synchronizedMap(???);
    synchronized (b)
    {
      Iterator localIterator = b.values().iterator();
      while (localIterator.hasNext()) {
        a((MissedCallReminder)localIterator.next());
      }
      return;
    }
  }
  
  private PendingIntent a(MissedCallReminder paramMissedCallReminder, int paramInt)
  {
    Intent localIntent = MissedCallReminderService.a(a, paramMissedCallReminder);
    return PendingIntent.getService(a, notificationId, localIntent, paramInt);
  }
  
  private void a(MissedCallReminder paramMissedCallReminder)
  {
    PendingIntent localPendingIntent = a(paramMissedCallReminder, 134217728);
    long l1 = (System.currentTimeMillis() - timestamp) / 3600000L;
    long l2 = timestamp;
    ((AlarmManager)a.getSystemService("alarm")).setRepeating(1, l2 + (l1 + 1L) * 3600000L, 3600000L, localPendingIntent);
  }
  
  private void b()
  {
    if (!b.isEmpty()) {
      synchronized (b)
      {
        String str = new com.google.gson.f().b(b);
        Settings.b("missedCallReminders", str);
        return;
      }
    }
    Settings.n("missedCallReminders");
  }
  
  private void b(MissedCallReminder paramMissedCallReminder)
  {
    paramMissedCallReminder = a(paramMissedCallReminder, 536870912);
    if (paramMissedCallReminder != null) {
      ((AlarmManager)a.getSystemService("alarm")).cancel(paramMissedCallReminder);
    }
  }
  
  private Map<String, MissedCallReminder> c()
  {
    Object localObject = Settings.b("missedCallReminders");
    if (!TextUtils.isEmpty((CharSequence)localObject)) {}
    try
    {
      localObject = (Map)new com.google.gson.f().a((String)localObject, f.1b);
      return (Map<String, MissedCallReminder>)localObject;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    return null;
  }
  
  public final void a()
  {
    synchronized (b)
    {
      Iterator localIterator = b.values().iterator();
      while (localIterator.hasNext()) {
        b((MissedCallReminder)localIterator.next());
      }
      b.clear();
      b();
      return;
    }
  }
  
  /* Error */
  public final void a(com.truecaller.data.entity.HistoryEvent paramHistoryEvent)
  {
    // Byte code:
    //   0: ldc -86
    //   2: invokestatic 174	com/truecaller/old/data/access/Settings:e	(Ljava/lang/String;)Z
    //   5: ifeq +21 -> 26
    //   8: aload_0
    //   9: getfield 24	com/truecaller/service/f:a	Landroid/content/Context;
    //   12: checkcast 176	com/truecaller/TrueApp
    //   15: invokevirtual 179	com/truecaller/TrueApp:p	()Z
    //   18: ifeq +8 -> 26
    //   21: iconst_1
    //   22: istore_2
    //   23: goto +5 -> 28
    //   26: iconst_0
    //   27: istore_2
    //   28: iload_2
    //   29: ifeq +261 -> 290
    //   32: aload_1
    //   33: getfield 184	com/truecaller/data/entity/HistoryEvent:b	Ljava/lang/String;
    //   36: astore 9
    //   38: aload_0
    //   39: getfield 24	com/truecaller/service/f:a	Landroid/content/Context;
    //   42: aload 9
    //   44: invokestatic 189	com/truecaller/search/f:a	(Landroid/content/Context;Ljava/lang/String;)Z
    //   47: ifeq +243 -> 290
    //   50: aload_0
    //   51: getfield 39	com/truecaller/service/f:b	Ljava/util/Map;
    //   54: aload 9
    //   56: invokeinterface 193 2 0
    //   61: ifne +229 -> 290
    //   64: aconst_null
    //   65: astore 8
    //   67: aconst_null
    //   68: astore 7
    //   70: aload 7
    //   72: astore 6
    //   74: aload 8
    //   76: astore 5
    //   78: aload_0
    //   79: getfield 24	com/truecaller/service/f:a	Landroid/content/Context;
    //   82: invokevirtual 197	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   85: astore 10
    //   87: aload 7
    //   89: astore 6
    //   91: aload 8
    //   93: astore 5
    //   95: invokestatic 202	com/truecaller/content/TruecallerContract$n:a	()Landroid/net/Uri;
    //   98: astore 11
    //   100: aload 7
    //   102: astore 6
    //   104: aload 8
    //   106: astore 5
    //   108: aload_1
    //   109: getfield 205	com/truecaller/data/entity/HistoryEvent:h	J
    //   112: lstore_3
    //   113: aload 7
    //   115: astore 6
    //   117: aload 8
    //   119: astore 5
    //   121: aload 10
    //   123: aload 11
    //   125: iconst_1
    //   126: anewarray 207	java/lang/String
    //   129: dup
    //   130: iconst_0
    //   131: ldc -48
    //   133: aastore
    //   134: ldc -46
    //   136: iconst_2
    //   137: anewarray 207	java/lang/String
    //   140: dup
    //   141: iconst_0
    //   142: lload_3
    //   143: invokestatic 214	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   146: aastore
    //   147: dup
    //   148: iconst_1
    //   149: aload 9
    //   151: aastore
    //   152: aconst_null
    //   153: invokevirtual 220	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   156: astore 7
    //   158: aload 7
    //   160: ifnull +36 -> 196
    //   163: aload 7
    //   165: astore 6
    //   167: aload 7
    //   169: astore 5
    //   171: aload 7
    //   173: invokeinterface 226 1 0
    //   178: istore_2
    //   179: iload_2
    //   180: ifle +16 -> 196
    //   183: aload 7
    //   185: ifnull +10 -> 195
    //   188: aload 7
    //   190: invokeinterface 229 1 0
    //   195: return
    //   196: aload 7
    //   198: ifnull +37 -> 235
    //   201: aload 7
    //   203: astore 5
    //   205: goto +23 -> 228
    //   208: astore_1
    //   209: goto +67 -> 276
    //   212: astore 7
    //   214: aload 5
    //   216: astore 6
    //   218: aload 7
    //   220: invokestatic 234	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   223: aload 5
    //   225: ifnull +10 -> 235
    //   228: aload 5
    //   230: invokeinterface 229 1 0
    //   235: new 63	com/truecaller/service/MissedCallReminder
    //   238: dup
    //   239: aload_1
    //   240: getfield 236	com/truecaller/data/entity/HistoryEvent:c	Ljava/lang/String;
    //   243: aload 9
    //   245: aload_1
    //   246: getfield 205	com/truecaller/data/entity/HistoryEvent:h	J
    //   249: invokespecial 239	com/truecaller/service/MissedCallReminder:<init>	(Ljava/lang/String;Ljava/lang/String;J)V
    //   252: astore_1
    //   253: aload_0
    //   254: aload_1
    //   255: invokespecial 66	com/truecaller/service/f:a	(Lcom/truecaller/service/MissedCallReminder;)V
    //   258: aload_0
    //   259: getfield 39	com/truecaller/service/f:b	Ljava/util/Map;
    //   262: aload 9
    //   264: aload_1
    //   265: invokeinterface 243 3 0
    //   270: pop
    //   271: aload_0
    //   272: invokespecial 167	com/truecaller/service/f:b	()V
    //   275: return
    //   276: aload 6
    //   278: ifnull +10 -> 288
    //   281: aload 6
    //   283: invokeinterface 229 1 0
    //   288: aload_1
    //   289: athrow
    //   290: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	291	0	this	f
    //   0	291	1	paramHistoryEvent	com.truecaller.data.entity.HistoryEvent
    //   22	158	2	i	int
    //   112	31	3	l	long
    //   76	153	5	localObject1	Object
    //   72	210	6	localObject2	Object
    //   68	134	7	localCursor	android.database.Cursor
    //   212	7	7	localException	Exception
    //   65	53	8	localObject3	Object
    //   36	227	9	str	String
    //   85	37	10	localContentResolver	android.content.ContentResolver
    //   98	26	11	localUri	android.net.Uri
    // Exception table:
    //   from	to	target	type
    //   78	87	208	finally
    //   95	100	208	finally
    //   108	113	208	finally
    //   121	158	208	finally
    //   171	179	208	finally
    //   218	223	208	finally
    //   78	87	212	java/lang/Exception
    //   95	100	212	java/lang/Exception
    //   108	113	212	java/lang/Exception
    //   121	158	212	java/lang/Exception
    //   171	179	212	java/lang/Exception
  }
  
  public final void a(String paramString)
  {
    MissedCallReminder localMissedCallReminder = (MissedCallReminder)b.get(paramString);
    if (localMissedCallReminder != null)
    {
      b(localMissedCallReminder);
      b.remove(paramString);
      b();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */