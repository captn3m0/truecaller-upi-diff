package com.truecaller.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.utils.l;

public class CallStateService
  extends Service
{
  private static boolean b = false;
  private final CallStateService.a a = new CallStateService.a(this, (byte)0);
  private l c;
  private boolean d;
  
  private void a(int paramInt)
  {
    b().listen(a, paramInt);
  }
  
  public static void a(Context paramContext)
  {
    if (Build.VERSION.SDK_INT < 26) {
      paramContext.startService(new Intent(paramContext, CallStateService.class));
    }
  }
  
  public static boolean a()
  {
    return b;
  }
  
  private TelephonyManager b()
  {
    return (TelephonyManager)getSystemService("phone");
  }
  
  private boolean c()
  {
    return c.a(new String[] { "android.permission.READ_PHONE_STATE", "android.permission.READ_CALL_LOG" });
  }
  
  public IBinder onBind(Intent paramIntent)
  {
    return null;
  }
  
  public void onCreate()
  {
    super.onCreate();
    c = ((TrueApp)getApplicationContext()).a().bw();
    d = c();
    a(32);
    b = true;
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    a(0);
    b = false;
  }
  
  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    return 1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.CallStateService
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */