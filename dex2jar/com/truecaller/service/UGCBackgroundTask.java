package com.truecaller.service;

import android.content.Context;
import android.os.Bundle;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.h.ac;
import com.truecaller.old.data.access.i;
import com.truecaller.util.t;
import com.truecaller.utils.l;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;

@Deprecated
public class UGCBackgroundTask
  extends PersistentBackgroundTask
{
  @Inject
  public com.truecaller.featuretoggles.e a;
  @Inject
  public com.truecaller.common.h.c b;
  
  public UGCBackgroundTask()
  {
    TrueApp.y().a().a(this);
  }
  
  /* Error */
  private static int a(Context paramContext, ArrayList<com.truecaller.old.data.a.e> paramArrayList)
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore 4
    //   3: new 36	java/lang/StringBuilder
    //   6: dup
    //   7: ldc 38
    //   9: invokespecial 41	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   12: astore 13
    //   14: aload 13
    //   16: aload_1
    //   17: invokevirtual 47	java/util/ArrayList:size	()I
    //   20: invokevirtual 51	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   23: pop
    //   24: aload 13
    //   26: invokevirtual 55	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   29: pop
    //   30: iconst_0
    //   31: istore_2
    //   32: ldc2_w 56
    //   35: lstore 11
    //   37: lload 11
    //   39: lstore 5
    //   41: lload 11
    //   43: lstore 7
    //   45: lload 11
    //   47: lstore 9
    //   49: new 59	com/google/gson/i
    //   52: dup
    //   53: invokespecial 60	com/google/gson/i:<init>	()V
    //   56: astore 13
    //   58: lload 11
    //   60: lstore 5
    //   62: lload 11
    //   64: lstore 7
    //   66: lload 11
    //   68: lstore 9
    //   70: aload_1
    //   71: invokevirtual 64	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   74: astore 14
    //   76: lload 11
    //   78: lstore 5
    //   80: lload 11
    //   82: lstore 7
    //   84: lload 11
    //   86: lstore 9
    //   88: aload 14
    //   90: invokeinterface 70 1 0
    //   95: ifeq +309 -> 404
    //   98: lload 11
    //   100: lstore 5
    //   102: lload 11
    //   104: lstore 7
    //   106: lload 11
    //   108: lstore 9
    //   110: aload 14
    //   112: invokeinterface 74 1 0
    //   117: checkcast 76	com/truecaller/old/data/a/e
    //   120: astore 15
    //   122: lload 11
    //   124: lstore 5
    //   126: lload 11
    //   128: lstore 7
    //   130: lload 11
    //   132: lstore 9
    //   134: new 78	com/google/gson/o
    //   137: dup
    //   138: invokespecial 79	com/google/gson/o:<init>	()V
    //   141: astore 16
    //   143: lload 11
    //   145: lstore 5
    //   147: lload 11
    //   149: lstore 7
    //   151: lload 11
    //   153: lstore 9
    //   155: aload 16
    //   157: ldc 81
    //   159: aload 15
    //   161: getfield 85	com/truecaller/old/data/a/e:c	Ljava/lang/String;
    //   164: invokestatic 88	com/truecaller/old/data/a/e:a	(Lcom/google/gson/o;Ljava/lang/String;Ljava/lang/String;)V
    //   167: lload 11
    //   169: lstore 5
    //   171: lload 11
    //   173: lstore 7
    //   175: lload 11
    //   177: lstore 9
    //   179: aload 16
    //   181: ldc 90
    //   183: aload 15
    //   185: getfield 93	com/truecaller/old/data/a/e:d	Ljava/lang/String;
    //   188: invokestatic 88	com/truecaller/old/data/a/e:a	(Lcom/google/gson/o;Ljava/lang/String;Ljava/lang/String;)V
    //   191: lload 11
    //   193: lstore 5
    //   195: lload 11
    //   197: lstore 7
    //   199: lload 11
    //   201: lstore 9
    //   203: aload 16
    //   205: ldc 95
    //   207: aload 15
    //   209: getfield 98	com/truecaller/old/data/a/e:e	Ljava/lang/String;
    //   212: invokestatic 88	com/truecaller/old/data/a/e:a	(Lcom/google/gson/o;Ljava/lang/String;Ljava/lang/String;)V
    //   215: lload 11
    //   217: lstore 5
    //   219: lload 11
    //   221: lstore 7
    //   223: lload 11
    //   225: lstore 9
    //   227: aload 16
    //   229: ldc 100
    //   231: new 43	java/util/ArrayList
    //   234: dup
    //   235: aload 15
    //   237: getfield 104	com/truecaller/old/data/a/e:j	Ljava/util/Set;
    //   240: invokespecial 107	java/util/ArrayList:<init>	(Ljava/util/Collection;)V
    //   243: invokestatic 110	com/truecaller/old/data/a/e:a	(Lcom/google/gson/o;Ljava/lang/String;Ljava/util/ArrayList;)V
    //   246: lload 11
    //   248: lstore 5
    //   250: lload 11
    //   252: lstore 7
    //   254: lload 11
    //   256: lstore 9
    //   258: aload 16
    //   260: ldc 112
    //   262: new 43	java/util/ArrayList
    //   265: dup
    //   266: aload 15
    //   268: getfield 115	com/truecaller/old/data/a/e:i	Ljava/util/Set;
    //   271: invokespecial 107	java/util/ArrayList:<init>	(Ljava/util/Collection;)V
    //   274: invokestatic 110	com/truecaller/old/data/a/e:a	(Lcom/google/gson/o;Ljava/lang/String;Ljava/util/ArrayList;)V
    //   277: lload 11
    //   279: lstore 5
    //   281: lload 11
    //   283: lstore 7
    //   285: lload 11
    //   287: lstore 9
    //   289: aload 16
    //   291: ldc 117
    //   293: new 43	java/util/ArrayList
    //   296: dup
    //   297: aload 15
    //   299: getfield 120	com/truecaller/old/data/a/e:h	Ljava/util/Set;
    //   302: invokespecial 107	java/util/ArrayList:<init>	(Ljava/util/Collection;)V
    //   305: invokestatic 110	com/truecaller/old/data/a/e:a	(Lcom/google/gson/o;Ljava/lang/String;Ljava/util/ArrayList;)V
    //   308: lload 11
    //   310: lstore 5
    //   312: lload 11
    //   314: lstore 7
    //   316: lload 11
    //   318: lstore 9
    //   320: aload 16
    //   322: ldc 122
    //   324: new 43	java/util/ArrayList
    //   327: dup
    //   328: aload 15
    //   330: getfield 125	com/truecaller/old/data/a/e:k	Ljava/util/Set;
    //   333: invokespecial 107	java/util/ArrayList:<init>	(Ljava/util/Collection;)V
    //   336: invokestatic 110	com/truecaller/old/data/a/e:a	(Lcom/google/gson/o;Ljava/lang/String;Ljava/util/ArrayList;)V
    //   339: lload 11
    //   341: lstore 5
    //   343: lload 11
    //   345: lstore 7
    //   347: lload 11
    //   349: lstore 9
    //   351: aload 15
    //   353: aload 16
    //   355: invokevirtual 128	com/truecaller/old/data/a/e:a	(Lcom/google/gson/o;)V
    //   358: lload 11
    //   360: lstore 5
    //   362: lload 11
    //   364: lstore 7
    //   366: lload 11
    //   368: lstore 9
    //   370: aload 16
    //   372: ldc -126
    //   374: aload 15
    //   376: getfield 133	com/truecaller/old/data/a/e:f	Ljava/lang/String;
    //   379: invokestatic 88	com/truecaller/old/data/a/e:a	(Lcom/google/gson/o;Ljava/lang/String;Ljava/lang/String;)V
    //   382: lload 11
    //   384: lstore 5
    //   386: lload 11
    //   388: lstore 7
    //   390: lload 11
    //   392: lstore 9
    //   394: aload 13
    //   396: aload 16
    //   398: invokevirtual 136	com/google/gson/i:a	(Lcom/google/gson/l;)V
    //   401: goto -325 -> 76
    //   404: lload 11
    //   406: lstore 5
    //   408: lload 11
    //   410: lstore 7
    //   412: lload 11
    //   414: lstore 9
    //   416: aload 13
    //   418: invokevirtual 137	com/google/gson/i:toString	()Ljava/lang/String;
    //   421: astore 14
    //   423: lload 11
    //   425: lstore 5
    //   427: lload 11
    //   429: lstore 7
    //   431: lload 11
    //   433: lstore 9
    //   435: aload 13
    //   437: invokevirtual 139	com/google/gson/i:a	()I
    //   440: ifeq +964 -> 1404
    //   443: iconst_1
    //   444: istore_3
    //   445: goto +3 -> 448
    //   448: lload 11
    //   450: lstore 5
    //   452: lload 11
    //   454: lstore 7
    //   456: lload 11
    //   458: lstore 9
    //   460: iload_3
    //   461: iconst_1
    //   462: anewarray 141	java/lang/String
    //   465: dup
    //   466: iconst_0
    //   467: ldc -113
    //   469: aastore
    //   470: invokestatic 149	com/truecaller/log/AssertionUtil$OnlyInDebug:isTrue	(Z[Ljava/lang/String;)V
    //   473: lload 11
    //   475: lstore 5
    //   477: lload 11
    //   479: lstore 7
    //   481: lload 11
    //   483: lstore 9
    //   485: aload_1
    //   486: invokevirtual 152	java/util/ArrayList:isEmpty	()Z
    //   489: ifne +920 -> 1409
    //   492: iconst_1
    //   493: istore_3
    //   494: goto +3 -> 497
    //   497: lload 11
    //   499: lstore 5
    //   501: lload 11
    //   503: lstore 7
    //   505: lload 11
    //   507: lstore 9
    //   509: iload_3
    //   510: iconst_1
    //   511: anewarray 141	java/lang/String
    //   514: dup
    //   515: iconst_0
    //   516: ldc -102
    //   518: aastore
    //   519: invokestatic 149	com/truecaller/log/AssertionUtil$OnlyInDebug:isTrue	(Z[Ljava/lang/String;)V
    //   522: lload 11
    //   524: lstore 5
    //   526: lload 11
    //   528: lstore 7
    //   530: lload 11
    //   532: lstore 9
    //   534: aload 14
    //   536: invokestatic 159	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   539: ifne +875 -> 1414
    //   542: iconst_1
    //   543: istore_3
    //   544: goto +3 -> 547
    //   547: lload 11
    //   549: lstore 5
    //   551: lload 11
    //   553: lstore 7
    //   555: lload 11
    //   557: lstore 9
    //   559: iload_3
    //   560: iconst_1
    //   561: anewarray 141	java/lang/String
    //   564: dup
    //   565: iconst_0
    //   566: ldc -95
    //   568: aastore
    //   569: invokestatic 149	com/truecaller/log/AssertionUtil$OnlyInDebug:isTrue	(Z[Ljava/lang/String;)V
    //   572: lload 11
    //   574: lstore 5
    //   576: lload 11
    //   578: lstore 7
    //   580: lload 11
    //   582: lstore 9
    //   584: aload 13
    //   586: invokevirtual 139	com/google/gson/i:a	()I
    //   589: ifeq +748 -> 1337
    //   592: lload 11
    //   594: lstore 5
    //   596: lload 11
    //   598: lstore 7
    //   600: lload 11
    //   602: lstore 9
    //   604: aload_1
    //   605: invokevirtual 152	java/util/ArrayList:isEmpty	()Z
    //   608: ifne +729 -> 1337
    //   611: lload 11
    //   613: lstore 5
    //   615: lload 11
    //   617: lstore 7
    //   619: lload 11
    //   621: lstore 9
    //   623: aload 14
    //   625: invokestatic 159	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   628: ifeq +6 -> 634
    //   631: goto +706 -> 1337
    //   634: lload 11
    //   636: lstore 5
    //   638: lload 11
    //   640: lstore 7
    //   642: lload 11
    //   644: lstore 9
    //   646: invokestatic 167	java/lang/System:currentTimeMillis	()J
    //   649: lstore 11
    //   651: aconst_null
    //   652: astore 13
    //   654: aload_0
    //   655: ldc -87
    //   657: invokestatic 174	android/support/v4/content/b:a	(Landroid/content/Context;Ljava/lang/String;)I
    //   660: ifne +9 -> 669
    //   663: aload_0
    //   664: invokestatic 179	com/truecaller/common/h/k:e	(Landroid/content/Context;)Ljava/lang/String;
    //   667: astore 13
    //   669: new 181	com/truecaller/common/network/util/a
    //   672: dup
    //   673: invokespecial 182	com/truecaller/common/network/util/a:<init>	()V
    //   676: getstatic 188	com/truecaller/common/network/util/KnownEndpoints:PHONEBOOK	Lcom/truecaller/common/network/util/KnownEndpoints;
    //   679: invokevirtual 191	com/truecaller/common/network/util/a:a	(Lcom/truecaller/common/network/util/KnownEndpoints;)Lcom/truecaller/common/network/util/a;
    //   682: astore 15
    //   684: new 193	com/truecaller/common/network/d/b
    //   687: dup
    //   688: invokespecial 194	com/truecaller/common/network/d/b:<init>	()V
    //   691: astore 16
    //   693: aload 16
    //   695: ldc -60
    //   697: invokestatic 201	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   700: aload 15
    //   702: getfield 204	com/truecaller/common/network/util/a:a	Ljava/util/List;
    //   705: ifnonnull +18 -> 723
    //   708: aload 15
    //   710: new 43	java/util/ArrayList
    //   713: dup
    //   714: invokespecial 205	java/util/ArrayList:<init>	()V
    //   717: checkcast 207	java/util/List
    //   720: putfield 204	com/truecaller/common/network/util/a:a	Ljava/util/List;
    //   723: aload 15
    //   725: getfield 204	com/truecaller/common/network/util/a:a	Ljava/util/List;
    //   728: astore 17
    //   730: aload 17
    //   732: ifnull +13 -> 745
    //   735: aload 17
    //   737: aload 16
    //   739: invokeinterface 211 2 0
    //   744: pop
    //   745: aload 15
    //   747: ldc -43
    //   749: invokevirtual 216	com/truecaller/common/network/util/a:b	(Ljava/lang/Class;)Ljava/lang/Object;
    //   752: checkcast 213	com/truecaller/network/f/b$a
    //   755: aload 13
    //   757: getstatic 221	com/truecaller/common/network/util/g:b	Lokhttp3/w;
    //   760: aload 14
    //   762: invokestatic 226	okhttp3/ac:a	(Lokhttp3/w;Ljava/lang/String;)Lokhttp3/ac;
    //   765: invokeinterface 229 3 0
    //   770: invokeinterface 234 1 0
    //   775: getfield 239	e/r:b	Ljava/lang/Object;
    //   778: checkcast 241	com/truecaller/network/f/a
    //   781: astore 14
    //   783: lload 11
    //   785: lstore 5
    //   787: lload 11
    //   789: lstore 7
    //   791: lload 11
    //   793: lstore 9
    //   795: invokestatic 167	java/lang/System:currentTimeMillis	()J
    //   798: lload 11
    //   800: lsub
    //   801: lstore 11
    //   803: aload 14
    //   805: ifnull +483 -> 1288
    //   808: lload 11
    //   810: lstore 5
    //   812: lload 11
    //   814: lstore 7
    //   816: lload 11
    //   818: lstore 9
    //   820: aload 14
    //   822: getfield 244	com/truecaller/network/f/a:a	Lcom/truecaller/network/f/a$a;
    //   825: ifnull +463 -> 1288
    //   828: lload 11
    //   830: lstore 5
    //   832: lload 11
    //   834: lstore 7
    //   836: lload 11
    //   838: lstore 9
    //   840: aload 14
    //   842: getfield 244	com/truecaller/network/f/a:a	Lcom/truecaller/network/f/a$a;
    //   845: getfield 247	com/truecaller/network/f/a$a:a	Ljava/util/List;
    //   848: ifnull +440 -> 1288
    //   851: lload 11
    //   853: lstore 5
    //   855: lload 11
    //   857: lstore 7
    //   859: lload 11
    //   861: lstore 9
    //   863: aload 14
    //   865: getfield 244	com/truecaller/network/f/a:a	Lcom/truecaller/network/f/a$a;
    //   868: getfield 247	com/truecaller/network/f/a$a:a	Ljava/util/List;
    //   871: invokeinterface 248 1 0
    //   876: ifne +412 -> 1288
    //   879: lload 11
    //   881: lstore 5
    //   883: lload 11
    //   885: lstore 7
    //   887: lload 11
    //   889: lstore 9
    //   891: new 250	java/util/HashMap
    //   894: dup
    //   895: invokespecial 251	java/util/HashMap:<init>	()V
    //   898: astore 13
    //   900: lload 11
    //   902: lstore 5
    //   904: lload 11
    //   906: lstore 7
    //   908: lload 11
    //   910: lstore 9
    //   912: aload 14
    //   914: getfield 244	com/truecaller/network/f/a:a	Lcom/truecaller/network/f/a$a;
    //   917: getfield 247	com/truecaller/network/f/a$a:a	Ljava/util/List;
    //   920: invokeinterface 252 1 0
    //   925: astore 14
    //   927: lload 11
    //   929: lstore 5
    //   931: lload 11
    //   933: lstore 7
    //   935: lload 11
    //   937: lstore 9
    //   939: aload 14
    //   941: invokeinterface 70 1 0
    //   946: ifeq +100 -> 1046
    //   949: lload 11
    //   951: lstore 5
    //   953: lload 11
    //   955: lstore 7
    //   957: lload 11
    //   959: lstore 9
    //   961: aload 14
    //   963: invokeinterface 74 1 0
    //   968: checkcast 254	com/truecaller/network/f/a$b
    //   971: astore 15
    //   973: lload 11
    //   975: lstore 5
    //   977: lload 11
    //   979: lstore 7
    //   981: lload 11
    //   983: lstore 9
    //   985: aload 15
    //   987: getfield 257	com/truecaller/network/f/a$b:c	Z
    //   990: ifeq -63 -> 927
    //   993: lload 11
    //   995: lstore 5
    //   997: lload 11
    //   999: lstore 7
    //   1001: lload 11
    //   1003: lstore 9
    //   1005: aload 15
    //   1007: getfield 259	com/truecaller/network/f/a$b:a	Ljava/lang/String;
    //   1010: ifnull -83 -> 927
    //   1013: lload 11
    //   1015: lstore 5
    //   1017: lload 11
    //   1019: lstore 7
    //   1021: lload 11
    //   1023: lstore 9
    //   1025: aload 13
    //   1027: aload 15
    //   1029: getfield 259	com/truecaller/network/f/a$b:a	Ljava/lang/String;
    //   1032: aload 15
    //   1034: getfield 261	com/truecaller/network/f/a$b:b	Ljava/lang/String;
    //   1037: invokeinterface 267 3 0
    //   1042: pop
    //   1043: goto -116 -> 927
    //   1046: lload 11
    //   1048: lstore 5
    //   1050: lload 11
    //   1052: lstore 7
    //   1054: lload 11
    //   1056: lstore 9
    //   1058: new 36	java/lang/StringBuilder
    //   1061: dup
    //   1062: ldc_w 269
    //   1065: invokespecial 41	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1068: astore 14
    //   1070: lload 11
    //   1072: lstore 5
    //   1074: lload 11
    //   1076: lstore 7
    //   1078: lload 11
    //   1080: lstore 9
    //   1082: aload 14
    //   1084: aload 13
    //   1086: invokeinterface 270 1 0
    //   1091: invokevirtual 51	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1094: pop
    //   1095: lload 11
    //   1097: lstore 5
    //   1099: lload 11
    //   1101: lstore 7
    //   1103: lload 11
    //   1105: lstore 9
    //   1107: aload 14
    //   1109: invokevirtual 55	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1112: pop
    //   1113: lload 11
    //   1115: lstore 5
    //   1117: lload 11
    //   1119: lstore 7
    //   1121: lload 11
    //   1123: lstore 9
    //   1125: new 272	com/truecaller/old/data/access/e
    //   1128: dup
    //   1129: aload_0
    //   1130: invokespecial 275	com/truecaller/old/data/access/e:<init>	(Landroid/content/Context;)V
    //   1133: astore 14
    //   1135: lload 11
    //   1137: lstore 5
    //   1139: lload 11
    //   1141: lstore 7
    //   1143: lload 11
    //   1145: lstore 9
    //   1147: new 277	com/truecaller/old/data/access/i
    //   1150: dup
    //   1151: aload_0
    //   1152: invokespecial 278	com/truecaller/old/data/access/i:<init>	(Landroid/content/Context;)V
    //   1155: astore_0
    //   1156: lload 11
    //   1158: lstore 5
    //   1160: lload 11
    //   1162: lstore 7
    //   1164: lload 11
    //   1166: lstore 9
    //   1168: aload_1
    //   1169: invokevirtual 64	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   1172: astore 15
    //   1174: iconst_0
    //   1175: istore_2
    //   1176: lload 11
    //   1178: lstore 5
    //   1180: aload 15
    //   1182: invokeinterface 70 1 0
    //   1187: ifeq +87 -> 1274
    //   1190: lload 11
    //   1192: lstore 5
    //   1194: aload 15
    //   1196: invokeinterface 74 1 0
    //   1201: checkcast 76	com/truecaller/old/data/a/e
    //   1204: astore 16
    //   1206: lload 11
    //   1208: lstore 5
    //   1210: aload 13
    //   1212: aload 16
    //   1214: getfield 133	com/truecaller/old/data/a/e:f	Ljava/lang/String;
    //   1217: invokeinterface 282 2 0
    //   1222: checkcast 141	java/lang/String
    //   1225: astore 17
    //   1227: aload 17
    //   1229: ifnull -53 -> 1176
    //   1232: lload 11
    //   1234: lstore 5
    //   1236: aload 14
    //   1238: aload 16
    //   1240: getfield 286	com/truecaller/old/data/a/e:g	J
    //   1243: aload 17
    //   1245: aload 16
    //   1247: getfield 289	com/truecaller/old/data/a/e:a	I
    //   1250: invokevirtual 292	com/truecaller/old/data/access/e:a	(JLjava/lang/String;I)Lcom/truecaller/old/data/entity/c;
    //   1253: pop
    //   1254: lload 11
    //   1256: lstore 5
    //   1258: aload_0
    //   1259: aload 16
    //   1261: getfield 133	com/truecaller/old/data/a/e:f	Ljava/lang/String;
    //   1264: invokevirtual 294	com/truecaller/old/data/access/i:b	(Ljava/lang/String;)V
    //   1267: iload_2
    //   1268: iconst_1
    //   1269: iadd
    //   1270: istore_2
    //   1271: goto -95 -> 1176
    //   1274: iload 4
    //   1276: istore_3
    //   1277: goto +13 -> 1290
    //   1280: astore_0
    //   1281: goto +88 -> 1369
    //   1284: astore_0
    //   1285: goto +84 -> 1369
    //   1288: iconst_0
    //   1289: istore_3
    //   1290: aload_1
    //   1291: invokevirtual 297	java/util/ArrayList:clear	()V
    //   1294: lload 11
    //   1296: iload_3
    //   1297: invokestatic 300	com/truecaller/service/UGCBackgroundTask:a	(JZ)V
    //   1300: iload_2
    //   1301: ireturn
    //   1302: astore_0
    //   1303: lload 11
    //   1305: lstore 5
    //   1307: lload 11
    //   1309: lstore 7
    //   1311: lload 11
    //   1313: lstore 9
    //   1315: invokestatic 167	java/lang/System:currentTimeMillis	()J
    //   1318: lload 11
    //   1320: lsub
    //   1321: lstore 11
    //   1323: lload 11
    //   1325: lstore 5
    //   1327: lload 11
    //   1329: lstore 7
    //   1331: lload 11
    //   1333: lstore 9
    //   1335: aload_0
    //   1336: athrow
    //   1337: aload_1
    //   1338: invokevirtual 297	java/util/ArrayList:clear	()V
    //   1341: ldc2_w 56
    //   1344: iconst_0
    //   1345: invokestatic 300	com/truecaller/service/UGCBackgroundTask:a	(JZ)V
    //   1348: iconst_0
    //   1349: ireturn
    //   1350: astore_0
    //   1351: goto +41 -> 1392
    //   1354: astore_0
    //   1355: goto +8 -> 1363
    //   1358: astore_0
    //   1359: lload 9
    //   1361: lstore 7
    //   1363: iconst_0
    //   1364: istore_2
    //   1365: lload 7
    //   1367: lstore 11
    //   1369: lload 11
    //   1371: lstore 5
    //   1373: aload_0
    //   1374: ldc_w 302
    //   1377: invokestatic 307	com/truecaller/log/d:a	(Ljava/lang/Throwable;Ljava/lang/String;)V
    //   1380: aload_1
    //   1381: invokevirtual 297	java/util/ArrayList:clear	()V
    //   1384: lload 11
    //   1386: iconst_0
    //   1387: invokestatic 300	com/truecaller/service/UGCBackgroundTask:a	(JZ)V
    //   1390: iload_2
    //   1391: ireturn
    //   1392: aload_1
    //   1393: invokevirtual 297	java/util/ArrayList:clear	()V
    //   1396: lload 5
    //   1398: iconst_0
    //   1399: invokestatic 300	com/truecaller/service/UGCBackgroundTask:a	(JZ)V
    //   1402: aload_0
    //   1403: athrow
    //   1404: iconst_0
    //   1405: istore_3
    //   1406: goto -958 -> 448
    //   1409: iconst_0
    //   1410: istore_3
    //   1411: goto -914 -> 497
    //   1414: iconst_0
    //   1415: istore_3
    //   1416: goto -869 -> 547
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1419	0	paramContext	Context
    //   0	1419	1	paramArrayList	ArrayList<com.truecaller.old.data.a.e>
    //   31	1360	2	i	int
    //   444	972	3	bool1	boolean
    //   1	1274	4	bool2	boolean
    //   39	1358	5	l1	long
    //   43	1323	7	l2	long
    //   47	1313	9	l3	long
    //   35	1350	11	l4	long
    //   12	1199	13	localObject1	Object
    //   74	1163	14	localObject2	Object
    //   120	1075	15	localObject3	Object
    //   141	1119	16	localObject4	Object
    //   728	516	17	localObject5	Object
    // Exception table:
    //   from	to	target	type
    //   1180	1190	1280	java/io/IOException
    //   1194	1206	1280	java/io/IOException
    //   1210	1227	1280	java/io/IOException
    //   1236	1254	1280	java/io/IOException
    //   1258	1267	1280	java/io/IOException
    //   1180	1190	1284	java/lang/RuntimeException
    //   1194	1206	1284	java/lang/RuntimeException
    //   1210	1227	1284	java/lang/RuntimeException
    //   1236	1254	1284	java/lang/RuntimeException
    //   1258	1267	1284	java/lang/RuntimeException
    //   654	669	1302	finally
    //   669	723	1302	finally
    //   723	730	1302	finally
    //   735	745	1302	finally
    //   745	783	1302	finally
    //   49	58	1350	finally
    //   70	76	1350	finally
    //   88	98	1350	finally
    //   110	122	1350	finally
    //   134	143	1350	finally
    //   155	167	1350	finally
    //   179	191	1350	finally
    //   203	215	1350	finally
    //   227	246	1350	finally
    //   258	277	1350	finally
    //   289	308	1350	finally
    //   320	339	1350	finally
    //   351	358	1350	finally
    //   370	382	1350	finally
    //   394	401	1350	finally
    //   416	423	1350	finally
    //   435	443	1350	finally
    //   460	473	1350	finally
    //   485	492	1350	finally
    //   509	522	1350	finally
    //   534	542	1350	finally
    //   559	572	1350	finally
    //   584	592	1350	finally
    //   604	611	1350	finally
    //   623	631	1350	finally
    //   646	651	1350	finally
    //   795	803	1350	finally
    //   820	828	1350	finally
    //   840	851	1350	finally
    //   863	879	1350	finally
    //   891	900	1350	finally
    //   912	927	1350	finally
    //   939	949	1350	finally
    //   961	973	1350	finally
    //   985	993	1350	finally
    //   1005	1013	1350	finally
    //   1025	1043	1350	finally
    //   1058	1070	1350	finally
    //   1082	1095	1350	finally
    //   1107	1113	1350	finally
    //   1125	1135	1350	finally
    //   1147	1156	1350	finally
    //   1168	1174	1350	finally
    //   1180	1190	1350	finally
    //   1194	1206	1350	finally
    //   1210	1227	1350	finally
    //   1236	1254	1350	finally
    //   1258	1267	1350	finally
    //   1315	1323	1350	finally
    //   1335	1337	1350	finally
    //   1373	1380	1350	finally
    //   49	58	1354	java/io/IOException
    //   70	76	1354	java/io/IOException
    //   88	98	1354	java/io/IOException
    //   110	122	1354	java/io/IOException
    //   134	143	1354	java/io/IOException
    //   155	167	1354	java/io/IOException
    //   179	191	1354	java/io/IOException
    //   203	215	1354	java/io/IOException
    //   227	246	1354	java/io/IOException
    //   258	277	1354	java/io/IOException
    //   289	308	1354	java/io/IOException
    //   320	339	1354	java/io/IOException
    //   351	358	1354	java/io/IOException
    //   370	382	1354	java/io/IOException
    //   394	401	1354	java/io/IOException
    //   416	423	1354	java/io/IOException
    //   435	443	1354	java/io/IOException
    //   460	473	1354	java/io/IOException
    //   485	492	1354	java/io/IOException
    //   509	522	1354	java/io/IOException
    //   534	542	1354	java/io/IOException
    //   559	572	1354	java/io/IOException
    //   584	592	1354	java/io/IOException
    //   604	611	1354	java/io/IOException
    //   623	631	1354	java/io/IOException
    //   646	651	1354	java/io/IOException
    //   795	803	1354	java/io/IOException
    //   820	828	1354	java/io/IOException
    //   840	851	1354	java/io/IOException
    //   863	879	1354	java/io/IOException
    //   891	900	1354	java/io/IOException
    //   912	927	1354	java/io/IOException
    //   939	949	1354	java/io/IOException
    //   961	973	1354	java/io/IOException
    //   985	993	1354	java/io/IOException
    //   1005	1013	1354	java/io/IOException
    //   1025	1043	1354	java/io/IOException
    //   1058	1070	1354	java/io/IOException
    //   1082	1095	1354	java/io/IOException
    //   1107	1113	1354	java/io/IOException
    //   1125	1135	1354	java/io/IOException
    //   1147	1156	1354	java/io/IOException
    //   1168	1174	1354	java/io/IOException
    //   1315	1323	1354	java/io/IOException
    //   1335	1337	1354	java/io/IOException
    //   49	58	1358	java/lang/RuntimeException
    //   70	76	1358	java/lang/RuntimeException
    //   88	98	1358	java/lang/RuntimeException
    //   110	122	1358	java/lang/RuntimeException
    //   134	143	1358	java/lang/RuntimeException
    //   155	167	1358	java/lang/RuntimeException
    //   179	191	1358	java/lang/RuntimeException
    //   203	215	1358	java/lang/RuntimeException
    //   227	246	1358	java/lang/RuntimeException
    //   258	277	1358	java/lang/RuntimeException
    //   289	308	1358	java/lang/RuntimeException
    //   320	339	1358	java/lang/RuntimeException
    //   351	358	1358	java/lang/RuntimeException
    //   370	382	1358	java/lang/RuntimeException
    //   394	401	1358	java/lang/RuntimeException
    //   416	423	1358	java/lang/RuntimeException
    //   435	443	1358	java/lang/RuntimeException
    //   460	473	1358	java/lang/RuntimeException
    //   485	492	1358	java/lang/RuntimeException
    //   509	522	1358	java/lang/RuntimeException
    //   534	542	1358	java/lang/RuntimeException
    //   559	572	1358	java/lang/RuntimeException
    //   584	592	1358	java/lang/RuntimeException
    //   604	611	1358	java/lang/RuntimeException
    //   623	631	1358	java/lang/RuntimeException
    //   646	651	1358	java/lang/RuntimeException
    //   795	803	1358	java/lang/RuntimeException
    //   820	828	1358	java/lang/RuntimeException
    //   840	851	1358	java/lang/RuntimeException
    //   863	879	1358	java/lang/RuntimeException
    //   891	900	1358	java/lang/RuntimeException
    //   912	927	1358	java/lang/RuntimeException
    //   939	949	1358	java/lang/RuntimeException
    //   961	973	1358	java/lang/RuntimeException
    //   985	993	1358	java/lang/RuntimeException
    //   1005	1013	1358	java/lang/RuntimeException
    //   1025	1043	1358	java/lang/RuntimeException
    //   1058	1070	1358	java/lang/RuntimeException
    //   1082	1095	1358	java/lang/RuntimeException
    //   1107	1113	1358	java/lang/RuntimeException
    //   1125	1135	1358	java/lang/RuntimeException
    //   1147	1156	1358	java/lang/RuntimeException
    //   1168	1174	1358	java/lang/RuntimeException
    //   1315	1323	1358	java/lang/RuntimeException
    //   1335	1337	1358	java/lang/RuntimeException
  }
  
  private static void a(long paramLong, boolean paramBoolean)
  {
    com.truecaller.i.e locale = TrueApp.y().a().F();
    int i = locale.a("backupBatchSize", 0);
    if ((paramBoolean) && (paramLong <= 10000L))
    {
      if (paramLong < 5000L)
      {
        i = i * 133 / 100;
        j = 200;
        if (i > 200) {
          i = j;
        }
        locale.b("backupBatchSize", i);
      }
      return;
    }
    i = i * 66 / 100;
    int j = 50;
    if (i < 50) {
      i = j;
    }
    locale.b("backupBatchSize", i);
  }
  
  public final int a()
  {
    return 10015;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    if (a.T().a()) {
      return PersistentBackgroundTask.RunResult.Success;
    }
    TrueApp localTrueApp = (TrueApp)paramContext.getApplicationContext();
    paramBundle = b.c();
    Object localObject1 = b.n();
    Object localObject2 = localTrueApp.a().bw();
    com.truecaller.analytics.e.a locala = new com.truecaller.analytics.e.a("EnhancedSearch");
    paramContext = PersistentBackgroundTask.RunResult.FailedSkip;
    if (localTrueApp.p()) {
      if ((((l)localObject2).a(new String[] { "android.permission.READ_CONTACTS" })) && (!((ac)localObject1).a()))
      {
        if (paramBundle.b("featureUgcDisabled"))
        {
          locala.a("Result", "Disabled");
          break label1058;
        }
        locala.a("Result", "Proceeded");
        paramContext = TrueApp.y().a();
        localObject1 = paramContext.F();
        localObject2 = paramContext.I();
        i locali = new i(localTrueApp);
        com.truecaller.common.tag.b localb = new com.truecaller.common.tag.b(localTrueApp);
        locala.a("Type", "Normal");
        int i;
        if (!com.truecaller.common.b.e.a("tagsPhonebookForcedUpload", false))
        {
          i = 0;
        }
        else
        {
          paramContext = new com.truecaller.old.data.access.e(localTrueApp);
          paramBundle = paramContext.a(com.truecaller.old.data.entity.c.class);
          localObject3 = paramBundle.iterator();
          i = 0;
          while (((Iterator)localObject3).hasNext())
          {
            localObject4 = (com.truecaller.old.data.entity.c)((Iterator)localObject3).next();
            if (o != -1)
            {
              o = -1;
              i = 1;
            }
          }
          if (i != 0)
          {
            paramContext.a(false);
            paramContext.a(paramBundle);
          }
          com.truecaller.common.b.e.b("tagsPhonebookForcedUpload", false);
          i = 1;
        }
        if (i != 0) {
          locala.a("Type", "TagReset");
        }
        if (locali.c() == 0) {
          locala.a("Type", "Initial");
        }
        Object localObject4 = t.a(localTrueApp);
        int n = ((List)localObject4).size();
        Object localObject3 = new ArrayList();
        paramContext = new StringBuilder("UGC - enough time passed since last timestamp - processing ");
        paramContext.append(n);
        paramContext.append(" device contacts");
        paramContext.toString();
        locala.a("Total", n);
        boolean bool1;
        if ((b.c()) && (((com.truecaller.common.g.a)localObject2).b("backup"))) {
          bool1 = true;
        } else {
          bool1 = false;
        }
        boolean bool2 = com.truecaller.common.b.e.a("featureEmailSource", false);
        paramBundle = b.g();
        locala.a("Full", bool1);
        if (paramBundle == null)
        {
          paramContext = "<null>";
        }
        else
        {
          paramContext = paramBundle;
          if (paramBundle.equals("")) {
            paramContext = "<empty>";
          }
        }
        locala.a("InstallerPackage", paramContext);
        locala.a("BuildName", b.f());
        localObject4 = ((List)localObject4).iterator();
        int k = 0;
        int j = 0;
        label859:
        while (((Iterator)localObject4).hasNext())
        {
          paramContext = (com.truecaller.old.data.a.b)((Iterator)localObject4).next();
          if ((d != null) && (!d.isEmpty()))
          {
            if (!((com.truecaller.common.g.a)localObject2).a("featureUgcContactsWithoutIdentity", true))
            {
              if ((e != null) && (!e.isEmpty())) {
                i = 1;
              } else if ((c != null) && ((c.e != null) || (c.b != null))) {
                i = 1;
              } else if ((b != null) && (bool1)) {
                i = 1;
              } else {
                i = 0;
              }
              if (i == 0) {
                break label859;
              }
            }
            paramBundle = new com.truecaller.old.data.a.e(paramContext, localb, bool1, bool2);
            if (b != a) {
              i = 1;
            } else {
              i = 0;
            }
            paramContext = paramBundle;
            if (i == 0)
            {
              paramContext = paramBundle;
              if (locali.a(f)) {
                paramContext = null;
              }
            }
            int m;
            if (paramContext != null)
            {
              ((ArrayList)localObject3).add(paramContext);
              i = k;
              m = j;
              if (((ArrayList)localObject3).size() == ((com.truecaller.i.e)localObject1).a("backupBatchSize", 0))
              {
                paramContext = new StringBuilder("While iterating all contacts, UGC size: ");
                paramContext.append(((ArrayList)localObject3).size());
                paramContext.toString();
                i = k + ((ArrayList)localObject3).size();
                m = j + a(localTrueApp, (ArrayList)localObject3);
              }
            }
            else
            {
              m = j;
              i = k;
            }
            k = i;
            j = m;
          }
        }
        if (((ArrayList)localObject3).size() > 0)
        {
          k += ((ArrayList)localObject3).size();
          j += a(localTrueApp, (ArrayList)localObject3);
        }
        locala.a("Count", k);
        locala.a("Successful", j);
        if (n == 0)
        {
          paramContext = "0-1";
        }
        else
        {
          i = k * 100 / n;
          if (i <= 1) {
            paramContext = "0-1";
          } else if (i <= 5) {
            paramContext = "2-5";
          } else if (i <= 10) {
            paramContext = "6-10";
          } else if (i <= 20) {
            paramContext = "11-20";
          } else if (i <= 50) {
            paramContext = "21-50";
          } else if (i <= 75) {
            paramContext = "51-75";
          } else {
            paramContext = "76-100";
          }
        }
        locala.a("CountRatio", paramContext);
        a = Double.valueOf(j);
        paramContext = PersistentBackgroundTask.RunResult.Success;
        break label1058;
      }
    }
    locala.a("Result", "Skipped");
    label1058:
    TrueApp.y().a().c().a(locala.a());
    return paramContext;
  }
  
  public final boolean a(Context paramContext)
  {
    return e(paramContext);
  }
  
  public final com.truecaller.common.background.e b()
  {
    com.truecaller.common.background.e.a locala = new com.truecaller.common.background.e.a(1).a(12L, TimeUnit.HOURS).b(6L, TimeUnit.HOURS).c(2L, TimeUnit.HOURS);
    b = 1;
    c = false;
    return locala.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.UGCBackgroundTask
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */