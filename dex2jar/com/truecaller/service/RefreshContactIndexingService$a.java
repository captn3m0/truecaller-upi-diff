package com.truecaller.service;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.v;

public final class RefreshContactIndexingService$a
{
  public final Context a;
  
  public RefreshContactIndexingService$a(Context paramContext)
  {
    a = paramContext;
  }
  
  public final void a()
  {
    Intent localIntent = new Intent(a, RefreshContactIndexingService.class).setAction("RefreshContactIndexingService.action.sync");
    v.a(a, RefreshContactIndexingService.class, 2131364100, localIntent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.RefreshContactIndexingService.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */