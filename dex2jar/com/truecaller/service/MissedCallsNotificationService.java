package com.truecaller.service;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.af;
import android.support.v4.app.z.d;
import android.text.TextUtils;
import c.g.a.m;
import c.g.b.k;
import c.u;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.featuretoggles.e;
import com.truecaller.i.c;
import com.truecaller.network.search.j;
import com.truecaller.network.search.n;
import com.truecaller.notificationchannels.b;
import com.truecaller.utils.l;
import java.io.IOException;
import java.util.UUID;
import javax.inject.Inject;
import javax.inject.Named;

public final class MissedCallsNotificationService
  extends af
{
  public static final MissedCallsNotificationService.a q = new MissedCallsNotificationService.a((byte)0);
  @Inject
  @Named("UI")
  public c.d.f j;
  @Inject
  public b k;
  @Inject
  public com.truecaller.androidactors.f<com.truecaller.callhistory.a> l;
  @Inject
  public e m;
  @Inject
  public com.truecaller.notifications.a n;
  @Inject
  public c o;
  @Inject
  public l p;
  
  private final Contact a(Context paramContext, String paramString)
  {
    Application localApplication = getApplication();
    if (localApplication != null)
    {
      if (!((com.truecaller.common.b.a)localApplication).p()) {
        return null;
      }
      if (TextUtils.isEmpty((CharSequence)paramString)) {
        return null;
      }
    }
    try
    {
      paramContext = new j(paramContext, UUID.randomUUID(), "notification").a(6).a(paramString).a().f();
      if (paramContext != null)
      {
        paramContext = paramContext.a();
        return paramContext;
      }
    }
    catch (IOException paramContext)
    {
      for (;;) {}
    }
    return null;
    throw new u("null cannot be cast to non-null type com.truecaller.common.app.ApplicationBase");
  }
  
  public static final void a(Context paramContext)
  {
    MissedCallsNotificationService.a.a(paramContext);
  }
  
  private static boolean a(HistoryEvent paramHistoryEvent)
  {
    return paramHistoryEvent.r() == 3;
  }
  
  public final void a(Intent paramIntent)
  {
    k.b(paramIntent, "intent");
    kotlinx.coroutines.f.a((m)new MissedCallsNotificationService.e(this, null));
  }
  
  public final c.d.f c()
  {
    c.d.f localf = j;
    if (localf == null) {
      k.a("uiCoroutineContext");
    }
    return localf;
  }
  
  public final com.truecaller.androidactors.f<com.truecaller.callhistory.a> d()
  {
    com.truecaller.androidactors.f localf = l;
    if (localf == null) {
      k.a("historyManager");
    }
    return localf;
  }
  
  public final c e()
  {
    c localc = o;
    if (localc == null) {
      k.a("callingSettings");
    }
    return localc;
  }
  
  final z.d f()
  {
    Context localContext = (Context)this;
    b localb = k;
    if (localb == null) {
      k.a("callingNotificationChannelProvider");
    }
    return new z.d(localContext, localb.X_());
  }
  
  public final void onCreate()
  {
    super.onCreate();
    Application localApplication = getApplication();
    if (localApplication != null)
    {
      ((bk)localApplication).a().a(this);
      return;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.GraphHolder");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.MissedCallsNotificationService
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */