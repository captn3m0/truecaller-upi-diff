package com.truecaller.service;

import android.app.Service;
import android.content.ClipboardManager;
import android.content.ClipboardManager.OnPrimaryClipChangedListener;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.DisplayMetrics;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.bo;
import com.truecaller.bo.a;
import com.truecaller.bp;
import com.truecaller.data.entity.Contact;
import com.truecaller.filters.g;
import com.truecaller.ui.components.FloatingWindow;
import com.truecaller.ui.components.e;
import com.truecaller.util.at;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicInteger;
import javax.inject.Inject;
import kotlinx.coroutines.bn;

public class ClipboardService
  extends Service
  implements bo.a
{
  @Inject
  public bo a;
  e b;
  private final AtomicInteger c = new AtomicInteger(0);
  private Configuration d;
  private Handler e;
  
  final e a()
  {
    if (b == null) {
      b = new e(this);
    }
    return b;
  }
  
  public final Object a(String paramString)
  {
    Integer localInteger = Integer.valueOf(c.incrementAndGet());
    Message localMessage = e.obtainMessage(3, 0, 0, localInteger);
    localMessage.getData().putString("number", paramString);
    e.sendMessageDelayed(localMessage, 100L);
    return localInteger;
  }
  
  public final void a(Object paramObject)
  {
    b(paramObject);
  }
  
  public final void a(String paramString, Contact paramContact, g paramg)
  {
    e.removeMessages(1);
    e.sendEmptyMessage(0);
    Message localMessage = e.obtainMessage(1);
    obj = new ClipboardService.a.a(paramString, paramContact, paramg);
    e.sendMessage(localMessage);
  }
  
  final void b(Object paramObject)
  {
    e.removeCallbacksAndMessages(paramObject);
    Handler localHandler = e;
    localHandler.sendMessage(localHandler.obtainMessage(4, 0, 0, paramObject));
  }
  
  public IBinder onBind(Intent paramIntent)
  {
    return null;
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    int i = d.updateFrom(paramConfiguration);
    if (Configuration.needNewResources(i, 4))
    {
      e.removeMessages(2);
      e.sendEmptyMessage(2);
      return;
    }
    if ((i & 0x80) != 0)
    {
      paramConfiguration = b;
      if (paramConfiguration != null)
      {
        DisplayMetrics localDisplayMetrics = d.getResources().getDisplayMetrics();
        h = widthPixels;
        i = (heightPixels - at.a(d.getResources()));
      }
    }
  }
  
  public void onCreate()
  {
    super.onCreate();
    d = new Configuration(getResources().getConfiguration());
    e = new Handler(new ClipboardService.a(this));
    TrueApp.y().a().cB().a(this);
    bo localbo = a;
    k.b(this, "searchListener");
    a = this;
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    bo localbo = a;
    if (localbo != null)
    {
      bn localbn = b;
      if (localbn != null) {
        localbn.c((Throwable)new CancellationException("SearchOnCopyHelper destroyed"));
      }
      c.removePrimaryClipChangedListener((ClipboardManager.OnPrimaryClipChangedListener)localbo);
      a = null;
    }
  }
  
  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    return 1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.ClipboardService
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */