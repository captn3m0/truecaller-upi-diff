package com.truecaller.service;

import c.d.f;
import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d<b>
{
  private final Provider<f> a;
  private final Provider<com.truecaller.data.access.c> b;
  private final Provider<com.truecaller.search.local.model.c> c;
  
  private c(Provider<f> paramProvider, Provider<com.truecaller.data.access.c> paramProvider1, Provider<com.truecaller.search.local.model.c> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static c a(Provider<f> paramProvider, Provider<com.truecaller.data.access.c> paramProvider1, Provider<com.truecaller.search.local.model.c> paramProvider2)
  {
    return new c(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */