package com.truecaller.service;

import android.content.Context;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import c.o.b;
import com.truecaller.api.services.presence.v1.models.Availability;
import com.truecaller.api.services.presence.v1.models.Availability.Context;
import com.truecaller.data.entity.Contact;
import com.truecaller.ui.CallMeBackActivity;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.g;
import org.a.a.x;

public final class b
  implements a
{
  final com.truecaller.data.access.c a;
  private final int b;
  private final f c;
  private final com.truecaller.search.local.model.c d;
  
  @Inject
  public b(@Named("Async") f paramf, com.truecaller.data.access.c paramc, com.truecaller.search.local.model.c paramc1)
  {
    c = paramf;
    a = paramc;
    d = paramc1;
    b = 24;
  }
  
  public final Object a(Context paramContext, String paramString1, String paramString2, c.d.c<? super Boolean> paramc)
  {
    if ((paramc instanceof b.a))
    {
      localObject1 = (b.a)paramc;
      if ((b & 0x80000000) != 0)
      {
        b += Integer.MIN_VALUE;
        paramc = (c.d.c<? super Boolean>)localObject1;
        break label58;
      }
    }
    paramc = new b.a(this, paramc);
    label58:
    Object localObject1 = a;
    Object localObject2 = c.d.a.a.a;
    int j = b;
    int i = 1;
    switch (j)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 1: 
      paramString1 = (String)f;
      paramContext = (Context)e;
      paramString2 = (b)d;
      if (!(localObject1 instanceof o.b)) {
        paramc = (c.d.c<? super Boolean>)localObject1;
      } else {
        throw a;
      }
      break;
    case 0: 
      if ((localObject1 instanceof o.b)) {
        break label417;
      }
      if (paramString2 == null) {
        return Boolean.FALSE;
      }
      localObject1 = c;
      m localm = (m)new b.b(this, paramString2, null);
      d = this;
      e = paramContext;
      f = paramString1;
      g = paramString2;
      b = 1;
      paramc = g.a((f)localObject1, localm, paramc);
      if (paramc == localObject2) {
        return localObject2;
      }
      paramString2 = this;
    }
    paramc = (Contact)paramc;
    if (paramc == null) {
      return Boolean.FALSE;
    }
    k.a(paramc, "withContext(asyncContext…Number) } ?: return false");
    if (!paramc.Z()) {
      return Boolean.FALSE;
    }
    localObject1 = d.a(paramc);
    if (localObject1 == null) {
      return Boolean.FALSE;
    }
    localObject2 = b;
    if (localObject2 == null) {
      return Boolean.FALSE;
    }
    if ((d == null) || (((Availability)localObject2).b() != Availability.Context.CALL)) {
      i = 0;
    }
    if ((i != 0) && (org.a.a.hd, (x)org.a.a.b.ay_()).b / 3600000L <= b))
    {
      paramContext.startActivity(CallMeBackActivity.a(paramContext, paramc, paramString1, 0, "callMeBackPopupInApp"));
      return Boolean.TRUE;
    }
    return Boolean.FALSE;
    label417:
    throw a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */