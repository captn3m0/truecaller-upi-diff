package com.truecaller.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v4.app.ac;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.h.ai;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.log.d;
import com.truecaller.old.data.access.Settings;
import com.truecaller.ui.NotificationAccessActivity;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.util.NotificationUtil;
import java.util.Calendar;

public class AlarmReceiver
  extends BroadcastReceiver
{
  private static final AlarmReceiver.AlarmType[] a = { AlarmReceiver.AlarmType.TYPE_15DAYS, AlarmReceiver.AlarmType.TYPE_20DAYS, AlarmReceiver.AlarmType.TYPE_RESCHEDULE, AlarmReceiver.AlarmType.TYPE_UPDATE_SPAM, AlarmReceiver.AlarmType.TYPE_NOTIFICATION_ACCESS, AlarmReceiver.AlarmType.TYPE_DISMISS_NOTIFICATION };
  private static final AlarmReceiver.AlarmType[] b = { AlarmReceiver.AlarmType.TYPE_15DAYS, AlarmReceiver.AlarmType.TYPE_RESCHEDULE };
  private static final AlarmReceiver.AlarmType[] c = { AlarmReceiver.AlarmType.TYPE_2DAYS_UPGRADED, AlarmReceiver.AlarmType.TYPE_5DAYS, AlarmReceiver.AlarmType.TYPE_UPDATE_SPAM, AlarmReceiver.AlarmType.TYPE_NOTIFICATION_ACCESS, AlarmReceiver.AlarmType.TYPE_DISMISS_NOTIFICATION };
  private static final AlarmReceiver.AlarmType[] d = { AlarmReceiver.AlarmType.TYPE_DO_NOT_DISTURB_ACCESS };
  private static PackageInfo e;
  
  public static void a(Context paramContext)
  {
    AlarmReceiver.AlarmType[] arrayOfAlarmType = b;
    int j = arrayOfAlarmType.length;
    int i = 0;
    while (i < j)
    {
      Settings.d(arrayOfAlarmType[i].name(), 0L);
      i += 1;
    }
    a(paramContext, false);
  }
  
  private static void a(Context paramContext, AlarmReceiver.AlarmType paramAlarmType)
  {
    long l2 = Settings.l(paramAlarmType.name());
    long l1 = l2;
    if (l2 == 0L)
    {
      l1 = System.currentTimeMillis();
      if ((Settings.k(paramAlarmType.name())) && (paramAlarmType.getRecurringPeriod() > 0L)) {
        l1 += paramAlarmType.getRecurringPeriod();
      } else {
        l1 += paramAlarmType.getFirstDelay();
      }
    }
    AlarmManager localAlarmManager = (AlarmManager)paramContext.getSystemService("alarm");
    Intent localIntent = new Intent(paramContext, AlarmReceiver.class).putExtra("notification_type", paramAlarmType.name());
    localAlarmManager.set(0, l1, PendingIntent.getBroadcast(paramContext, paramAlarmType.getNotificationId(), localIntent, 0));
    Settings.d(paramAlarmType.name(), l1);
    paramContext = new StringBuilder("Scheduled alarm ");
    paramContext.append(paramAlarmType.name());
    paramContext.append(" for ");
    paramContext.append((l1 - System.currentTimeMillis()) / 1000L);
    paramContext.append(" seconds from now");
    paramContext.toString();
  }
  
  public static void a(Context paramContext, boolean paramBoolean)
  {
    if (!((com.truecaller.common.b.a)paramContext.getApplicationContext()).p()) {
      return;
    }
    "wasBooted = ".concat(String.valueOf(paramBoolean));
    d(paramContext);
    int i;
    if (efirstInstallTime != elastUpdateTime) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0)
    {
      localObject2 = c;
      AlarmReceiver.AlarmType[] arrayOfAlarmType = a;
      int j = arrayOfAlarmType.length;
      i = 0;
      for (;;)
      {
        localObject1 = localObject2;
        if (i >= j) {
          break;
        }
        localObject1 = arrayOfAlarmType[i];
        if (Settings.l(((AlarmReceiver.AlarmType)localObject1).name()) > 0L)
        {
          Settings.d(((AlarmReceiver.AlarmType)localObject1).name(), 0L);
          ((AlarmManager)paramContext.getSystemService("alarm")).cancel(PendingIntent.getBroadcast(paramContext, ((AlarmReceiver.AlarmType)localObject1).getNotificationId(), new Intent(paramContext, AlarmReceiver.class), 0));
          StringBuilder localStringBuilder = new StringBuilder("Canceled alarm ");
          localStringBuilder.append(((AlarmReceiver.AlarmType)localObject1).name());
          localStringBuilder.toString();
        }
        i += 1;
      }
    }
    Object localObject1 = a;
    Object localObject2 = localObject1;
    if (paramBoolean) {
      localObject2 = (AlarmReceiver.AlarmType[])org.c.a.a.a.a.a((Object[])localObject1, d);
    }
    a(paramContext, paramBoolean, (AlarmReceiver.AlarmType[])localObject2);
  }
  
  private static void a(Context paramContext, boolean paramBoolean, AlarmReceiver.AlarmType[] paramArrayOfAlarmType)
  {
    int j = paramArrayOfAlarmType.length;
    int i = 0;
    while (i < j)
    {
      AlarmReceiver.AlarmType localAlarmType = paramArrayOfAlarmType[i];
      long l = Settings.l(localAlarmType.name());
      if (((!Settings.k(localAlarmType.name())) || (localAlarmType.getRecurringPeriod() > 0L)) && (((paramBoolean) && (l > 0L)) || (l == 0L))) {
        a(paramContext, localAlarmType);
      }
      i += 1;
    }
  }
  
  public static void a(Intent paramIntent)
  {
    if (paramIntent.hasExtra("notification_type")) {
      paramIntent.removeExtra("notification_type");
    }
  }
  
  private static long c(Context paramContext)
  {
    d(paramContext);
    return efirstInstallTime;
  }
  
  private static void d(Context paramContext)
  {
    if (e == null) {}
    try
    {
      e = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 0);
      return;
    }
    catch (PackageManager.NameNotFoundException paramContext) {}
    return;
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    "AlarmReceiver received intent ".concat(String.valueOf(paramIntent));
    Object localObject1;
    int i;
    if (paramIntent.getAction() != null)
    {
      localObject1 = paramIntent.getAction();
      i = ((String)localObject1).hashCode();
      if (i != 170717771)
      {
        if (i != 1370363729)
        {
          if ((i == 1763328585) && (((String)localObject1).equals("com.truecaller.intent.action.PROMO_CLICKED")))
          {
            i = 1;
            break label97;
          }
        }
        else if (((String)localObject1).equals("com.truecaller.intent.action.SHARE"))
        {
          i = 0;
          break label97;
        }
      }
      else if (((String)localObject1).equals("com.truecaller.intent.action.PROMO_DISMISSED"))
      {
        i = 2;
        break label97;
      }
      i = -1;
      switch (i)
      {
      default: 
        break;
      case 2: 
        ac.a(paramContext).a(null, AlarmReceiver.AlarmType.TYPE_NOTIFICATION_ACCESS.getNotificationId());
        return;
      case 1: 
        paramIntent.setAction("android.intent.action.MAIN");
        paramContext.startActivity(NotificationAccessActivity.a(paramContext, TruecallerInit.a(paramContext, "calls", "notification")));
        return;
      case 0: 
        label97:
        paramContext.startActivity(ai.a(paramContext.getString(2131886653), paramContext.getString(2131887144), paramContext.getString(2131887143)).addFlags(268435456));
        TrueApp.y().a().c().a(new e.a("ViewAction").a("Context", "notification").a("Action", "share").a());
        return;
      }
    }
    if (paramIntent.hasExtra("notification_type"))
    {
      localObject1 = paramIntent.getStringExtra("notification_type");
      try
      {
        localObject1 = AlarmReceiver.AlarmType.valueOf((String)localObject1);
        Object localObject2 = new StringBuilder("Alarm type: ");
        ((StringBuilder)localObject2).append(((AlarmReceiver.AlarmType)localObject1).name());
        ((StringBuilder)localObject2).toString();
        if (!NotificationUtil.a())
        {
          paramIntent = PendingIntent.getBroadcast(paramContext.getApplicationContext(), ((AlarmReceiver.AlarmType)localObject1).getNotificationId(), paramIntent, 134217728);
          paramContext = (AlarmManager)paramContext.getSystemService("alarm");
          localObject1 = Calendar.getInstance();
          i = ((Calendar)localObject1).get(11);
          long l;
          if ((i >= 9) && (i <= 21))
          {
            l = ((Calendar)localObject1).getTimeInMillis();
          }
          else
          {
            ((Calendar)localObject1).add(11, 12);
            l = ((Calendar)localObject1).getTimeInMillis();
          }
          paramContext.set(0, l, paramIntent);
          return;
        }
        Settings.d(((AlarmReceiver.AlarmType)localObject1).name(), 0L);
        Settings.m(((AlarmReceiver.AlarmType)localObject1).name());
        paramIntent = ((AlarmReceiver.AlarmType)localObject1).getNotification(paramContext);
        if ((paramIntent != null) && (((AlarmReceiver.AlarmType)localObject1).shouldShow(paramContext)))
        {
          Object localObject3 = ((AlarmReceiver.AlarmType)localObject1).getAnalyticsSubtype();
          if (localObject3 == null)
          {
            AssertionUtil.OnlyInDebug.fail(new String[] { "Notification must specify analytics subtype" });
            return;
          }
          localObject2 = new Bundle();
          ((Bundle)localObject2).putString("Subtype", (String)localObject3);
          localObject3 = ((bk)paramContext.getApplicationContext()).a().W();
          ((com.truecaller.notifications.a)localObject3).a(((AlarmReceiver.AlarmType)localObject1).getNotificationId());
          ((com.truecaller.notifications.a)localObject3).a(null, ((AlarmReceiver.AlarmType)localObject1).getNotificationId(), paramIntent, "notificationPeriodicPromo", (Bundle)localObject2);
        }
        if (((AlarmReceiver.AlarmType)localObject1).getRecurringPeriod() > 0L) {
          a(paramContext, (AlarmReceiver.AlarmType)localObject1);
        }
        return;
      }
      catch (IllegalArgumentException paramContext)
      {
        d.a(paramContext, "Unsupported alarm type");
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.service.AlarmReceiver
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */