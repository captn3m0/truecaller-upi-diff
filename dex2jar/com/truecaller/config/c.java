package com.truecaller.config;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import c.a.ag;
import c.g.b.k;
import c.u;
import c.x;
import com.truecaller.analytics.ae;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.common.network.account.InstallationDetailsDto;
import com.truecaller.common.network.b.a.b;
import com.truecaller.common.network.b.a.c;
import com.truecaller.filters.p;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.h;
import com.truecaller.old.data.access.i;
import com.truecaller.referral.am;
import com.truecaller.tracking.events.ao;
import com.truecaller.tracking.events.ao.a;
import com.truecaller.truepay.Truepay;
import com.truecaller.update.ForcedUpdate;
import com.truecaller.update.ForcedUpdate.UpdateType;
import e.r;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import javax.inject.Named;

public final class c
  implements a
{
  private final int a;
  private final Context b;
  private final com.truecaller.common.g.a c;
  private final com.truecaller.i.a d;
  private final com.truecaller.i.e e;
  private final am f;
  private final com.truecaller.analytics.storage.a g;
  private final p h;
  private final com.truecaller.featuretoggles.e i;
  private final com.truecaller.util.b j;
  private final h k;
  private final com.truecaller.common.network.account.b l;
  private final f<ae> m;
  
  @Inject
  public c(@Named("app_version_code") int paramInt, Context paramContext, com.truecaller.common.g.a parama, com.truecaller.i.a parama1, com.truecaller.i.e parame, am paramam, com.truecaller.analytics.storage.a parama2, p paramp, com.truecaller.featuretoggles.e parame1, com.truecaller.util.b paramb, h paramh, com.truecaller.common.network.account.b paramb1, f<ae> paramf)
  {
    a = paramInt;
    b = paramContext;
    c = parama;
    d = parama1;
    e = parame;
    f = paramam;
    g = parama2;
    h = paramp;
    i = parame1;
    j = paramb;
    k = paramh;
    l = paramb1;
    m = paramf;
  }
  
  private static int a(String paramString, int paramInt)
  {
    if (paramString != null)
    {
      paramString = c.n.m.b(paramString);
      if (paramString != null) {
        return paramString.intValue();
      }
    }
    return paramInt;
  }
  
  private static long a(String paramString, long paramLong)
  {
    if (paramString != null)
    {
      paramString = c.n.m.d(paramString);
      if (paramString != null) {
        return paramString.longValue();
      }
    }
    return paramLong;
  }
  
  private static Long a(String paramString)
  {
    for (;;)
    {
      try
      {
        if (TextUtils.isEmpty((CharSequence)paramString))
        {
          paramString = null;
        }
        else
        {
          if (paramString == null) {
            k.a();
          }
          paramString = Long.valueOf(Long.parseLong(paramString));
        }
        if (paramString == null) {}
      }
      catch (NumberFormatException paramString)
      {
        long l1;
        return null;
      }
      try
      {
        l1 = paramString.longValue();
        if (0L <= l1) {
          continue;
        }
        return null;
      }
      catch (NumberFormatException localNumberFormatException) {}
    }
    return paramString;
    return paramString;
  }
  
  private final void a(a.b paramb)
  {
    a("featureSmsSearch", g, "0");
    a("featureOutgoingSearch", f, "0");
    a("featureStatsSearch", h, "1");
    if ((b(a)) && (!com.truecaller.common.b.e.a("featureEmailSource", false))) {
      com.truecaller.common.b.e.b("featureEmailSource", true);
    }
    if (b(c)) {
      new i(b).b();
    }
    try
    {
      if (TextUtils.isEmpty((CharSequence)l))
      {
        l1 = 0L;
      }
      else
      {
        localObject1 = l;
        if (localObject1 == null) {
          k.a();
        }
        k.a(localObject1, "features.featureTagUpload!!");
        l1 = Long.parseLong((String)localObject1);
      }
    }
    catch (NumberFormatException localNumberFormatException)
    {
      long l1;
      Object localObject1;
      boolean bool;
      Long localLong;
      Object localObject2;
      for (;;) {}
    }
    l1 = 0L;
    com.truecaller.common.b.e.b("tagsKeywordsFeatureCurrentVersion", l1);
    if (l1 > 0L) {
      bool = true;
    } else {
      bool = false;
    }
    com.truecaller.common.b.e.b("featureAutoTagging", bool);
    c.b("featureAvailability", b(m));
    com.truecaller.common.b.e.b("featureLoggingEnabled", b(n));
    c.b("featureCacheAdAfterCall", b(q));
    c.b("featureAdCtpRotation", b(ac));
    d.b("adsFeatureHouseAdsTimeout", a(av, 0L));
    d.b("adsFeatureUnifiedAdsBlock", b(ax));
    d.b("adsFeatureUnifiedAdsDetails", b(aw));
    d.b("adFeatureRetentionTime", a(ay, 0L));
    c.b("featureOTPNotificationEnabled", b(S));
    c.a("otpParserRegex", T);
    c.b("featureUgcDisabled", b(O));
    c.b("featureUgcContactsWithoutIdentity", b(M));
    c.b("featureFlash", b(t));
    c.b("featureCleverTap", b(ab));
    c.b("featureOfflineDirectory", b(L));
    c.b("featureSmartNotifications", b(az));
    c.b("featureShareImageInFlash", b(aB));
    c.b("featureRegion1", true ^ b(ad));
    c.b("key_region_1_timestamp", System.currentTimeMillis());
    com.truecaller.common.b.e.b("featureDisableOutgoingOutside", b(r));
    com.truecaller.common.b.e.b("featureHideDialpad", b(s));
    com.truecaller.common.b.e.b("featureNumberScanner", b(N));
    com.truecaller.common.b.e.b("featurePromoSpamOffCount", a(G, 0));
    c.b("featureOfflineDirectory", b(L));
    com.truecaller.common.b.e.b("featurePromoIncomingMsgCount", a(H, 0));
    com.truecaller.common.b.e.b("featureOperatorCustomization", p);
    c.b("presence_interval", a(ah, com.truecaller.presence.t.b()));
    c.b("presence_initial_delay", a(ai, com.truecaller.presence.t.a()));
    c.b("presence_stop_time", a(aj, com.truecaller.presence.t.d()));
    c.b("presence_recheck_time", a(ak, com.truecaller.presence.t.e()));
    e.a("feature_global_unimportant_promo_period_days", a(P));
    e.a("feature_unimportant_promo_period_days", a(Q));
    e.a("feature_unimportant_promo_dismissed_delay_days", a(R));
    g.b("uploadEventsMaxBatchSize", a(aF, 100));
    g.b("uploadEventsMinBatchSize", a(aG, 100));
    g.b("uploadEventsRetryJitter", a(aH, 10000L));
    localObject1 = c;
    localLong = a(aq);
    if (localLong != null) {
      l1 = localLong.longValue();
    } else {
      l1 = 0L;
    }
    ((com.truecaller.common.g.a)localObject1).b("featureBusinessSuggestionMaxCount", l1);
    k.d(a(aA, 0L));
    k.i(a(aC, 0));
    k.e(a(aD, 104857600L));
    k.g(b(aE));
    k.e(aU);
    k.k(a(aK, 30));
    k.m(a(bu, 7));
    k.n(a(bG, 200));
    k.o(a(bH, 20));
    k.p(a(bK, 59));
    e.b("feature_im_promo_after_call_period_days", a(aJ, 5));
    localObject1 = a(bc);
    if (localObject1 != null)
    {
      l1 = ((Number)localObject1).longValue();
      c.b("searchHitTtl", l1);
    }
    localObject1 = a(bd);
    if (localObject1 != null)
    {
      l1 = ((Number)localObject1).longValue();
      c.b("searchMissTtl", l1);
    }
    e.b("feature_voip_promo_after_call_period_days", a(br, 5));
    e.a("httpAnalyitcsHosts", bJ);
    localObject2 = h;
    localObject1 = bM;
    localLong = null;
    if (localObject1 != null) {
      localObject1 = c.n.m.b((String)localObject1);
    } else {
      localObject1 = null;
    }
    ((p)localObject2).a((Integer)localObject1);
    localObject1 = h;
    localObject2 = bN;
    paramb = localLong;
    if (localObject2 != null) {
      paramb = c.n.m.b((String)localObject2);
    }
    ((p)localObject1).b(paramb);
  }
  
  private final void a(a.c paramc)
  {
    ForcedUpdate.a(paramc);
    if ((j.a()) && (ForcedUpdate.a() != ForcedUpdate.UpdateType.OPTIONAL)) {
      ForcedUpdate.a(b, false);
    }
    if (paramc != null)
    {
      paramc = a;
      if (paramc != null)
      {
        paramc = ao.b().a((CharSequence)"UpgradePathReceived").a(ag.a(c.t.a("UpgradePath", paramc))).a();
        ((ae)m.a()).a((org.apache.a.d.d)paramc);
        return;
      }
    }
  }
  
  private static void a(String paramString1, String paramString2, String paramString3)
  {
    if ((k.a(paramString3, paramString2)) && (com.truecaller.common.b.e.c(paramString1, TimeUnit.DAYS.toMillis(1L)))) {
      com.truecaller.common.b.e.d(paramString1);
    }
  }
  
  private final void b(a.b paramb)
  {
    f.a("featureSearchBarIcon", b(C));
    f.a("featureAftercall", b(u));
    f.a("featureAftercallSaveContact", b(v));
    f.a("featureContactDetail", b(B));
    f.a("featureContacts", b(x));
    f.a("featureInboxOverflow", b(z));
    f.a("featureReferralDeeplink", b(y));
    f.a("featureReferralNavigationDrawer", b(D));
    f.a("featureUserBusyPrompt", b(w));
    f.a("featureGoPro", b(F));
    f.a("featureReferralAfterCallPromo", b(K));
    am localam = f;
    String str = E;
    paramb = str;
    if (str == null) {
      paramb = "App Chooser";
    }
    localam.a("featureReferralShareApps", paramb);
    f.a("featurePushNotification", true);
    f.a("featureLaunchReferralFromDeeplink", true);
    f.a("featureSearchScreenPromo", true);
  }
  
  private static boolean b(String paramString)
  {
    return (k.a("1", paramString)) || ((paramString != null) && (Boolean.parseBoolean(paramString)));
  }
  
  private final void c(a.b paramb)
  {
    paramb = af;
    Object localObject = null;
    if (paramb != null) {
      paramb = c.n.m.c((CharSequence)paramb, new String[] { "," }, false, 6);
    } else {
      paramb = null;
    }
    com.truecaller.common.g.a locala = c;
    if (paramb != null) {
      localObject = (String)c.a.m.e(paramb);
    }
    locala.a("callRecordingLicense", (String)localObject);
    localObject = c;
    if (paramb != null)
    {
      paramb = (String)c.a.m.g(paramb);
      if (paramb != null)
      {
        l1 = Long.parseLong(paramb);
        break label101;
      }
    }
    long l1 = 0L;
    label101:
    ((com.truecaller.common.g.a)localObject).b("callRecordingSerial", l1);
  }
  
  private final boolean c()
  {
    try
    {
      Object localObject = l.a();
      com.truecaller.common.network.account.a locala = com.truecaller.common.network.account.a.a;
      localObject = com.truecaller.common.network.account.a.a((InstallationDetailsDto)localObject).c();
      k.a(localObject, "AccountRestAdapter.updat…nstallationDto).execute()");
      if (((r)localObject).d())
      {
        c.b("lastUpdateInstallationVersion", a);
        return true;
      }
    }
    catch (IOException localIOException)
    {
      com.truecaller.log.d.a((Throwable)localIOException);
    }
    return false;
  }
  
  private final void d(a.b paramb)
  {
    com.truecaller.common.g.a locala = c;
    String str = am;
    long l2 = 5L;
    if (str != null) {
      l1 = Long.parseLong(str);
    } else {
      l1 = 5L;
    }
    locala.b("featureWhoViewdMeNewViewIntervalInDays", l1);
    locala = c;
    str = ao;
    if (str != null) {
      l1 = Long.parseLong(str);
    } else {
      l1 = 5L;
    }
    locala.b("featureWhoViewdMeShowNotificationAfterXLookups", l1);
    locala = c;
    str = an;
    long l1 = l2;
    if (str != null) {
      l1 = Long.parseLong(str);
    }
    locala.b("featureWhoViewdMeShowNotificationAfterXDays", l1);
    c.a("whoViewedMeIncognitoBucket", ap);
    c.b("whoViewedMePBContactEnabled", b(ar));
    c.b("whoViewedMeACSEnabled", b(as));
    i.a("featureWhoViewedMe", b(al));
  }
  
  private final boolean d()
  {
    com.truecaller.common.network.b.a locala = e();
    if (locala != null)
    {
      Object localObject = a;
      if (localObject != null)
      {
        k.a(localObject, "features");
        a((a.b)localObject);
        e((a.b)localObject);
        b((a.b)localObject);
        c((a.b)localObject);
        d((a.b)localObject);
        if (localObject != null) {}
      }
      else
      {
        AssertionUtil.reportWeirdnessButNeverCrash("features object not present");
        localObject = x.a;
      }
      a(b);
    }
    return locala != null;
  }
  
  private static com.truecaller.common.network.b.a e()
  {
    try
    {
      Object localObject = com.truecaller.common.network.b.b.a;
      localObject = com.truecaller.common.network.b.b.a().c();
      k.a(localObject, "ConfigRestAdapter.getConfig().execute()");
      if ((((r)localObject).d()) && (((r)localObject).e() != null))
      {
        localObject = (com.truecaller.common.network.b.a)((r)localObject).e();
        return (com.truecaller.common.network.b.a)localObject;
      }
    }
    catch (IOException localIOException)
    {
      com.truecaller.log.d.a((Throwable)localIOException);
    }
    return null;
  }
  
  private final void e(a.b paramb)
  {
    i.a("featureIm", b(U));
    i.a("featureBackup", b(I));
    i.a("featureTcPayOnboarding", b(o));
    i.a("featureSwish", b(J));
    i.a("featureCallRecording", b(ae));
    i.a("featureEnableUtilities", b(W));
    i.a("featureSmsCategorizer", b(at));
    i.a("featureBlockByCountry", b(au));
    i.a("featureMultiplePsp", b(X));
    i.a("featureUseBankSmsData", b(Y));
    i.a("featureWhatsAppCalls", b(Z));
    i.a("featureTcCredit", b(aa));
    i.a("featureEnableGoldCallerIdForContacts", b(aI));
    i.a("featureBusinessProfiles", b(aL));
    i.a("featureCreateBusinessProfiles", b(aM));
    i.a("featureNormalizeShortCodes", b(aN));
    i.a("featureShowUserJoinedImNotification", b(aO));
    i.a("featureSmartNotificationPayAction", b(aP));
    i.a("featureImEmojiPoke", b(aQ));
    i.a("featureSdkScanner", b(aR));
    i.a("featureTcPaySmsBindingDeliveryCheck", b(aS));
    i.a("featureReactions", b(aT));
    i.a("featureContactsListV2", b(aV));
    i.a("featurePayHomeCarousel", b(aW));
    i.a("featurePayRewards", b(aX));
    i.a("featurePayInstantReward", b(ba));
    i.a("featurePayAppUpdatePopUp", b(bb));
    i.a("featureDisableAftercallIfLandscape", b(be));
    i.a("featureBlockHiddenNumbersAsPremium", b(bf));
    i.a("featureBlockTopSpammersAsPremium", b(bg));
    i.a("featureBlockNonPhonebook", b(bh));
    i.a("featureBlockNonPhonebookAsPremium", b(bi));
    i.a("featureBlockForeignNumbers", b(bj));
    i.a("featureBlockForeignNumbersAsPremium", b(bk));
    i.a("featureBlockNeighbourSpoofing", b(bl));
    i.a("featureBlockNeighbourSpoofingAsPremium", b(bm));
    i.a("featureBlockRegisteredTelemarketersAsPremium", b(bn));
    i.a("featureIMReply", b(bo));
    i.a("featureVoiceClip", b(bI));
    i.a("featureConvertBusinessProfileToPrivate", b(bp));
    i.a("featureVoIP", b(bq));
    i.a("featureTruecallerX", b(bs));
    i.a("featureLargeDetailsViewAd", b(bt));
    i.a("featureVisiblePushCallerId", b(bv));
    i.a("featureContactFieldsPremiumForUgc", b(bw));
    i.a("featureContactFieldsPremiumForProfile", b(bx));
    i.a("featureContactEmailAsPremium", b(by));
    i.a("featureContactAddressAsPremium", b(bz));
    i.a("featureContactJobAsPremium", b(bA));
    i.a("featureContactWebsiteAsPremium", b(bB));
    i.a("featureContactSocialAsPremium", b(bC));
    i.a("featureContactAboutAsPremium", b(bD));
    i.a("featureFiveBottomTabsWithBlockingPremium", b(bL));
    i.a("featureInsightsSMSParsing", b(bE));
    i.a("featureSdkScannerIgnoreFilter", b(bO));
    i.a("featurePayShortcutIcon", b(bP));
    i.a("featurePremiumTab", b(bQ));
    i.a("featurePayHomeRevamp", b(bR));
    i.a("featureInCallUI", b(bS));
    i.a("featureCrossDcSearch", b(bT));
    i.a("featurePayRegistrationV2", b(bU));
    i.a("featureInsightsSMSPersistence", b(bF));
    i.a("featureEngagementRewards", b(bV));
    i.a("featurePayAppIxigo", b(bW));
    i.a("featurePayGooglePlayRecharge", b(aY));
    i.a("featureAppsInstalledHeartbeat", b(bX));
    i.a("featurePayBbpsReminders", b(aZ));
    boolean bool1 = b(V);
    boolean bool2 = i.n().a();
    i.a("featureTcPay", bool1);
    if ((bool1 != bool2) && (bool1))
    {
      paramb = b.getApplicationContext();
      if (paramb != null)
      {
        Truepay.initialize((Application)paramb);
        return;
      }
      throw new u("null cannot be cast to non-null type android.app.Application");
    }
  }
  
  public final w<Boolean> a()
  {
    if ((c.a("lastUpdateInstallationVersion", 0) != a) && (!c()))
    {
      localw = w.b(Boolean.FALSE);
      k.a(localw, "Promise.wrap(false)");
      return localw;
    }
    w localw = w.b(Boolean.valueOf(d()));
    k.a(localw, "Promise.wrap(updateConfigInternal())");
    return localw;
  }
  
  public final w<Boolean> b()
  {
    if (c())
    {
      d();
      localw = w.b(Boolean.TRUE);
      k.a(localw, "Promise.wrap(true)");
      return localw;
    }
    w localw = w.b(Boolean.FALSE);
    k.a(localw, "Promise.wrap(false)");
    return localw;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.config.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */