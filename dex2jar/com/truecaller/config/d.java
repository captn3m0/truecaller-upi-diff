package com.truecaller.config;

import android.content.Context;
import com.truecaller.analytics.ae;
import com.truecaller.androidactors.f;
import com.truecaller.filters.p;
import com.truecaller.messaging.h;
import com.truecaller.referral.am;
import javax.inject.Provider;

public final class d
  implements dagger.a.d<c>
{
  private final Provider<Integer> a;
  private final Provider<Context> b;
  private final Provider<com.truecaller.common.g.a> c;
  private final Provider<com.truecaller.i.a> d;
  private final Provider<com.truecaller.i.e> e;
  private final Provider<am> f;
  private final Provider<com.truecaller.analytics.storage.a> g;
  private final Provider<p> h;
  private final Provider<com.truecaller.featuretoggles.e> i;
  private final Provider<com.truecaller.util.b> j;
  private final Provider<h> k;
  private final Provider<com.truecaller.common.network.account.b> l;
  private final Provider<f<ae>> m;
  
  private d(Provider<Integer> paramProvider, Provider<Context> paramProvider1, Provider<com.truecaller.common.g.a> paramProvider2, Provider<com.truecaller.i.a> paramProvider3, Provider<com.truecaller.i.e> paramProvider4, Provider<am> paramProvider5, Provider<com.truecaller.analytics.storage.a> paramProvider6, Provider<p> paramProvider7, Provider<com.truecaller.featuretoggles.e> paramProvider8, Provider<com.truecaller.util.b> paramProvider9, Provider<h> paramProvider10, Provider<com.truecaller.common.network.account.b> paramProvider11, Provider<f<ae>> paramProvider12)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
    k = paramProvider10;
    l = paramProvider11;
    m = paramProvider12;
  }
  
  public static d a(Provider<Integer> paramProvider, Provider<Context> paramProvider1, Provider<com.truecaller.common.g.a> paramProvider2, Provider<com.truecaller.i.a> paramProvider3, Provider<com.truecaller.i.e> paramProvider4, Provider<am> paramProvider5, Provider<com.truecaller.analytics.storage.a> paramProvider6, Provider<p> paramProvider7, Provider<com.truecaller.featuretoggles.e> paramProvider8, Provider<com.truecaller.util.b> paramProvider9, Provider<h> paramProvider10, Provider<com.truecaller.common.network.account.b> paramProvider11, Provider<f<ae>> paramProvider12)
  {
    return new d(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10, paramProvider11, paramProvider12);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.config.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */