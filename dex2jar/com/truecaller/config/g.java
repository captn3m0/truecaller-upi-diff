package com.truecaller.config;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.k;
import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d<f<a>>
{
  private final e a;
  private final Provider<k> b;
  private final Provider<a> c;
  
  private g(e parame, Provider<k> paramProvider, Provider<a> paramProvider1)
  {
    a = parame;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static g a(e parame, Provider<k> paramProvider, Provider<a> paramProvider1)
  {
    return new g(parame, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.config.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */