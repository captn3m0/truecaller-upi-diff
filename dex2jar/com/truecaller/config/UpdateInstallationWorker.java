package com.truecaller.config;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.androidactors.w;
import com.truecaller.bp;
import com.truecaller.common.background.TrackedWorker;
import javax.inject.Inject;

public final class UpdateInstallationWorker
  extends TrackedWorker
{
  public static final UpdateInstallationWorker.a d = new UpdateInstallationWorker.a((byte)0);
  @Inject
  public b b;
  @Inject
  public a c;
  
  public UpdateInstallationWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public final b b()
  {
    b localb = b;
    if (localb == null) {
      k.a("analytics");
    }
    return localb;
  }
  
  public final boolean c()
  {
    return TrueApp.y().p();
  }
  
  public final ListenableWorker.a d()
  {
    Object localObject = c;
    if (localObject == null) {
      k.a("configManager");
    }
    if (k.a((Boolean)((a)localObject).b().d(), Boolean.TRUE))
    {
      localObject = ListenableWorker.a.a();
      k.a(localObject, "Result.success()");
      return (ListenableWorker.a)localObject;
    }
    localObject = ListenableWorker.a.b();
    k.a(localObject, "Result.retry()");
    return (ListenableWorker.a)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.config.UpdateInstallationWorker
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */