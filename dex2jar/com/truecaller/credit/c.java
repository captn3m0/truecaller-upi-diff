package com.truecaller.credit;

import android.app.Application;
import android.content.Context;
import c.u;
import com.truecaller.featuretoggles.d.b;
import com.truecaller.truepay.Truepay;
import javax.inject.Inject;

public final class c
  implements d.b
{
  private final String a;
  private final Context b;
  private final e c;
  
  @Inject
  public c(Context paramContext, e parame)
  {
    b = paramContext;
    c = parame;
    a = "featureTcCredit";
  }
  
  public final String a()
  {
    return a;
  }
  
  public final void b()
  {
    e locale = c;
    Context localContext = b.getApplicationContext();
    if (localContext != null)
    {
      locale.a((Application)localContext);
      Truepay.getInstance().setCreditHelper((com.truecaller.truepay.c)c);
      return;
    }
    throw new u("null cannot be cast to non-null type android.app.Application");
  }
  
  public final void c() {}
}

/* Location:
 * Qualified Name:     com.truecaller.credit.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */