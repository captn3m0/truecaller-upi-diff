package com.truecaller.credit;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.content.Intent;
import c.a.m;
import c.d.c;
import c.g.b.k;
import c.o.b;
import com.google.gson.o;
import com.truecaller.common.payments.a.b;
import com.truecaller.credit.app.ui.onboarding.views.activities.InitialOfferActivity;
import com.truecaller.truepay.TcPayCreditLoanItem;
import com.truecaller.truepay.Truepay;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;

public final class f
  implements e, j
{
  private final h a = (h)new i();
  
  public final Object a(c<? super List<TcPayCreditLoanItem>> paramc)
  {
    if ((paramc instanceof f.a))
    {
      localObject1 = (f.a)paramc;
      if ((b & 0x80000000) != 0)
      {
        b += Integer.MIN_VALUE;
        paramc = (c<? super List<TcPayCreditLoanItem>>)localObject1;
        break label48;
      }
    }
    paramc = new f.a(this, paramc);
    label48:
    Object localObject1 = a;
    Object localObject2 = c.d.a.a.a;
    switch (b)
    {
    default: 
      throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    case 1: 
      if (!(localObject1 instanceof o.b)) {
        paramc = (c<? super List<TcPayCreditLoanItem>>)localObject1;
      } else {
        throw a;
      }
      break;
    case 0: 
      if ((localObject1 instanceof o.b)) {
        break label282;
      }
      localObject1 = a;
      d = this;
      b = 1;
      localObject1 = ((h)localObject1).b();
      paramc = (c<? super List<TcPayCreditLoanItem>>)localObject1;
      if (localObject1 == localObject2) {
        return localObject2;
      }
      break;
    }
    paramc = (Iterable)paramc;
    localObject1 = (Collection)new ArrayList(m.a(paramc, 10));
    paramc = paramc.iterator();
    while (paramc.hasNext())
    {
      localObject2 = (b)paramc.next();
      ((Collection)localObject1).add(new TcPayCreditLoanItem(k, b, c, d, e, f, g, h, m, l, n, i, j, o, p));
    }
    return (List)localObject1;
    label282:
    throw a;
  }
  
  public final void a()
  {
    a.c();
  }
  
  public final void a(Application paramApplication)
  {
    k.b(paramApplication, "application");
    a.a(paramApplication, (j)this);
  }
  
  public final void a(Context paramContext)
  {
    k.b(paramContext, "context");
    paramContext.startActivity(new Intent(paramContext, InitialOfferActivity.class));
  }
  
  public final void a(o paramo)
  {
    k.b(paramo, "notification");
    a.a(paramo);
  }
  
  public final void a(boolean paramBoolean)
  {
    a.a(paramBoolean);
  }
  
  public final void a(boolean paramBoolean, String paramString1, String paramString2)
  {
    a.a(paramBoolean, paramString1, paramString2);
  }
  
  public final void b()
  {
    a.f();
  }
  
  public final void b(Context paramContext)
  {
    k.b(paramContext, "context");
    a.a(paramContext);
  }
  
  public final Object c()
  {
    return a.a();
  }
  
  public final LiveData<com.truecaller.common.payments.a.a> d()
  {
    return a.g();
  }
  
  public final void e()
  {
    a.d();
  }
  
  public final void f()
  {
    a.e();
  }
  
  public final boolean g()
  {
    Truepay localTruepay = Truepay.getInstance();
    k.a(localTruepay, "Truepay.getInstance()");
    return localTruepay.isRegistrationComplete();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.credit.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */