package com.truecaller;

import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.support.v4.app.ac;
import android.telephony.TelephonyManager;
import com.google.android.gms.internal.firebase_remote_config.zzeh;
import com.google.android.gms.internal.firebase_remote_config.zzeo;
import com.google.android.gms.internal.firebase_remote_config.zzeq;
import com.google.android.gms.internal.firebase_remote_config.zzeu;
import com.google.android.gms.internal.firebase_remote_config.zzex;
import com.google.firebase.remoteconfig.g.a;
import com.truecaller.analytics.ab;
import com.truecaller.analytics.au;
import com.truecaller.analytics.w;
import com.truecaller.analytics.y;
import com.truecaller.calling.ak;
import com.truecaller.calling.ar;
import com.truecaller.calling.as;
import com.truecaller.calling.dialer.ax;
import com.truecaller.calling.recorder.CallRecordingManager;
import com.truecaller.common.h.an;
import com.truecaller.common.h.u;
import com.truecaller.data.access.i;
import com.truecaller.filters.FilterManager;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.transport.im.bp;
import com.truecaller.network.search.BulkSearcherImpl;
import com.truecaller.scanner.o;
import com.truecaller.util.ag;
import com.truecaller.util.b.at;
import com.truecaller.util.bt;
import com.truecaller.util.bu;
import com.truecaller.util.cn;
import java.util.Arrays;
import java.util.Map;
import javax.inject.Named;
import org.json.JSONException;

public class c
{
  final TrueApp a;
  
  public c(TrueApp paramTrueApp)
  {
    a = paramTrueApp;
  }
  
  static com.truecaller.abtest.c a(dagger.a<com.google.firebase.remoteconfig.a> parama)
  {
    if (com.truecaller.common.b.e.a("qaAbTestEnableLocalConfig", false)) {
      return new com.truecaller.abtest.a(parama);
    }
    return new com.truecaller.abtest.d(parama);
  }
  
  static com.truecaller.aftercall.a a(cn paramcn, com.truecaller.i.c paramc, com.truecaller.util.al paramal, com.truecaller.utils.a parama)
  {
    return new com.truecaller.aftercall.b(paramcn, paramc, paramal, parama);
  }
  
  static au a(com.truecaller.utils.a parama, com.truecaller.analytics.b paramb)
  {
    return new ab(parama, paramb);
  }
  
  static w a(com.truecaller.utils.a parama, com.truecaller.androidactors.f<com.truecaller.analytics.ae> paramf, com.truecaller.analytics.b paramb)
  {
    return new y(parama, paramf, paramb);
  }
  
  static com.truecaller.androidactors.k a()
  {
    a locala = new a();
    if (a == null) {
      a = new com.truecaller.androidactors.m();
    }
    if (b == null) {
      b = new a.b((byte)0);
    }
    return new a.a(b, a);
  }
  
  static com.truecaller.callerid.a a(com.truecaller.androidactors.f<com.truecaller.callerid.e> paramf, com.truecaller.utils.d paramd)
  {
    return new com.truecaller.callerid.b(paramf, paramd);
  }
  
  static com.truecaller.calling.after_call.a a(com.truecaller.androidactors.k paramk, com.truecaller.androidactors.f<bp> paramf, bw parambw, com.truecaller.analytics.b paramb, com.truecaller.i.e parame, an paraman)
  {
    return new com.truecaller.calling.after_call.a(paramk, paramf, parambw, paramb, parame, paraman);
  }
  
  static com.truecaller.calling.after_call.b a(Context paramContext, @Named("Async") c.d.f paramf, com.truecaller.analytics.b paramb)
  {
    return new com.truecaller.calling.after_call.b(paramContext, paramb, paramf);
  }
  
  static ak a(com.truecaller.calling.k paramk, com.truecaller.calling.f paramf, com.truecaller.utils.l paraml, com.truecaller.utils.a parama, com.truecaller.callerid.a.d paramd, u paramu)
  {
    return new com.truecaller.calling.al(paramk, paramf, paraml, parama, paramd, paramu);
  }
  
  static ar a(com.truecaller.i.c paramc, com.truecaller.multisim.h paramh, com.truecaller.utils.n paramn)
  {
    return new as(paramc, paramh, paramn);
  }
  
  static ax a(Context paramContext)
  {
    return new com.truecaller.calling.dialer.b(paramContext);
  }
  
  static com.truecaller.calling.f a(com.truecaller.androidactors.f<com.truecaller.h.c> paramf, com.truecaller.common.h.d paramd, com.truecaller.i.c paramc)
  {
    return new com.truecaller.calling.g(paramf, paramd, paramc);
  }
  
  static com.truecaller.calling.k a(com.truecaller.utils.d paramd, com.truecaller.i.c paramc, FilterManager paramFilterManager, com.truecaller.common.account.r paramr, an paraman, cn paramcn, u paramu, com.truecaller.multisim.h paramh, com.truecaller.utils.l paraml, com.truecaller.utils.a parama, CallRecordingManager paramCallRecordingManager, com.truecaller.featuretoggles.e parame, com.truecaller.androidactors.f<com.truecaller.callhistory.a> paramf, com.truecaller.voip.d paramd1)
  {
    return new com.truecaller.calling.l(paramd, paramc, paraman, paramFilterManager, paramh, paramr, paramu, paramcn, parama, paraml, paramCallRecordingManager, parame, paramf, paramd1);
  }
  
  static com.truecaller.common.f.c a(com.truecaller.androidactors.f<com.truecaller.premium.data.g> paramf, com.truecaller.androidactors.f<com.truecaller.network.util.f> paramf1, com.truecaller.androidactors.k paramk, com.truecaller.common.g.a parama, com.truecaller.filters.p paramp, com.truecaller.engagementrewards.c paramc, com.truecaller.engagementrewards.ui.d paramd, com.truecaller.engagementrewards.g paramg, com.truecaller.featuretoggles.e parame)
  {
    return new com.truecaller.premium.data.p(paramf, paramf1, paramk, parama, paramp, paramc, paramd, paramg, parame);
  }
  
  static com.truecaller.d.b a(com.truecaller.featuretoggles.e parame, com.truecaller.common.g.a parama, TelephonyManager paramTelephonyManager)
  {
    return new com.truecaller.d.c(parame, Arrays.asList(new String[] { paramTelephonyManager.getSimCountryIso(), paramTelephonyManager.getNetworkCountryIso() }), parama);
  }
  
  static i a(ContentResolver paramContentResolver, u paramu, com.truecaller.data.access.c paramc, @Named("UI") c.d.f paramf1, @Named("Async") c.d.f paramf2)
  {
    return new com.truecaller.data.access.j(paramContentResolver, paramu, paramc, paramf1, paramf2);
  }
  
  static com.truecaller.featuretoggles.p a(com.truecaller.abtest.c paramc)
  {
    return paramc;
  }
  
  static com.truecaller.messaging.j a(com.truecaller.utils.d paramd, com.truecaller.messaging.h paramh)
  {
    return new com.truecaller.messaging.k(paramd, paramh);
  }
  
  static com.truecaller.multisim.ae a(com.truecaller.multisim.h paramh)
  {
    return new com.truecaller.multisim.af(paramh);
  }
  
  static com.truecaller.network.a.d a(com.truecaller.common.account.r paramr, com.truecaller.common.g.a parama, com.truecaller.common.network.c paramc)
  {
    return new com.truecaller.network.a.e(paramr, parama, paramc);
  }
  
  static com.truecaller.scanner.l a(com.truecaller.utils.d paramd, com.truecaller.common.g.a parama)
  {
    return new com.truecaller.scanner.l(paramd, parama);
  }
  
  @Named("inbox_availability_manager")
  static com.truecaller.search.local.model.c a(com.truecaller.search.local.model.c paramc)
  {
    return paramc;
  }
  
  static com.truecaller.search.local.model.c a(com.truecaller.search.local.model.g paramg, com.truecaller.common.account.r paramr, com.truecaller.util.al paramal, com.truecaller.messaging.h paramh, com.truecaller.androidactors.f<com.truecaller.presence.c> paramf, com.truecaller.presence.r paramr1, bw parambw, com.truecaller.voip.d paramd)
  {
    return new com.truecaller.search.local.model.d(paramal, paramg, paramr, paramh, paramf, paramr1, parambw, paramd);
  }
  
  static cn a(com.truecaller.util.b paramb)
  {
    return new br(paramb);
  }
  
  @Named("inbox")
  static com.truecaller.network.search.e b(Context paramContext)
  {
    return new BulkSearcherImpl(paramContext, 20, "inbox", null);
  }
  
  static com.truecaller.util.b b()
  {
    return new com.truecaller.util.c();
  }
  
  static com.google.firebase.remoteconfig.a c()
  {
    com.google.firebase.remoteconfig.a locala = com.google.firebase.remoteconfig.a.a();
    Object localObject = new g.a();
    a = false;
    localObject = ((g.a)localObject).a();
    g.zzb(a);
    localObject = zzex.zza(b, 2132082700);
    try
    {
      localObject = zzeo.zzct().zzc((Map)localObject).zzcv();
      f.zzb((zzeo)localObject);
      return locala;
    }
    catch (JSONException localJSONException) {}
    return locala;
  }
  
  static com.truecaller.data.access.m c(Context paramContext)
  {
    return new com.truecaller.data.access.m(paramContext);
  }
  
  static com.truecaller.data.access.c d(Context paramContext)
  {
    return new com.truecaller.data.access.c(paramContext);
  }
  
  static com.truecaller.f.a d()
  {
    return new com.truecaller.f.b();
  }
  
  static ContentResolver e(Context paramContext)
  {
    return paramContext.getContentResolver();
  }
  
  static com.truecaller.scanner.r e()
  {
    return new com.truecaller.scanner.r();
  }
  
  static com.truecaller.scanner.n f()
  {
    return new o();
  }
  
  static com.truecaller.service.e f(Context paramContext)
  {
    return new com.truecaller.service.f(paramContext);
  }
  
  static androidx.work.p g()
  {
    return androidx.work.p.a();
  }
  
  static com.truecaller.callerid.c g(Context paramContext)
  {
    return new com.truecaller.callerid.d(paramContext);
  }
  
  static com.truecaller.util.b.j h(Context paramContext)
  {
    return at.a(paramContext);
  }
  
  @Named("app_name")
  static String h()
  {
    TrueApp.y();
    return "Truecaller";
  }
  
  static com.truecaller.util.d.a i(Context paramContext)
  {
    return new com.truecaller.util.d.b(paramContext);
  }
  
  @Named("app_ver")
  static String i()
  {
    TrueApp.y();
    return "10.41.6";
  }
  
  static com.truecaller.util.af j(Context paramContext)
  {
    return new ag(paramContext);
  }
  
  static ac k(Context paramContext)
  {
    return ac.a(paramContext);
  }
  
  static ClipboardManager l(Context paramContext)
  {
    return (ClipboardManager)paramContext.getSystemService("clipboard");
  }
  
  static bt m(Context paramContext)
  {
    return new bu(paramContext.getSharedPreferences("qa-menu", 0));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */