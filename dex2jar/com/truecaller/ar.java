package com.truecaller;

import com.truecaller.multisim.ae;
import com.truecaller.multisim.h;
import dagger.a.d;
import javax.inject.Provider;

public final class ar
  implements d<ae>
{
  private final c a;
  private final Provider<h> b;
  
  private ar(c paramc, Provider<h> paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static ar a(c paramc, Provider<h> paramProvider)
  {
    return new ar(paramc, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ar
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */