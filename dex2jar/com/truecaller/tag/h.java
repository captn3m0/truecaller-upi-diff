package com.truecaller.tag;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d<c>
{
  private final f a;
  private final Provider<Context> b;
  
  private h(f paramf, Provider<Context> paramProvider)
  {
    a = paramf;
    b = paramProvider;
  }
  
  public static h a(f paramf, Provider<Context> paramProvider)
  {
    return new h(paramf, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tag.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */