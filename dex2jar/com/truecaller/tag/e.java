package com.truecaller.tag;

import android.content.Context;
import android.text.TextUtils;
import com.truecaller.androidactors.w;
import com.truecaller.common.tag.d;
import com.truecaller.data.access.k;
import com.truecaller.data.access.u;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.Tag;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class e
  implements c
{
  private final Context a;
  
  e(Context paramContext)
  {
    a = paramContext;
  }
  
  public final w<Void> a(Contact paramContact, long paramLong1, long paramLong2, int paramInt1, int paramInt2)
  {
    Object localObject1 = new ArrayList();
    Object localObject2 = paramContact.A().iterator();
    while (((Iterator)localObject2).hasNext())
    {
      String str = ((Number)((Iterator)localObject2).next()).a();
      if (!TextUtils.isEmpty(str)) {}
      try
      {
        ((List)localObject1).add(Long.valueOf(str.replaceFirst("\\+", "")));
      }
      catch (NumberFormatException localNumberFormatException)
      {
        for (;;) {}
      }
    }
    d.a(a, (List)localObject1, paramLong1, paramLong2, paramInt1, paramInt2);
    localObject1 = paramContact;
    if (!com.truecaller.data.access.c.b(paramContact))
    {
      paramContact = new com.truecaller.data.access.c(a).c(paramContact);
      localObject1 = paramContact;
      if (paramContact == null) {
        return w.b(null);
      }
    }
    paramContact = new u(a);
    paramContact.a((Contact)localObject1);
    ((Contact)localObject1).ab();
    if ((paramLong1 > 0L) || (paramLong1 == -1L))
    {
      localObject2 = new Tag();
      ((Tag)localObject2).setSource(16);
      ((Tag)localObject2).a(String.valueOf(paramLong1));
      paramContact.a((Contact)localObject1, (Tag)localObject2);
      ((Contact)localObject1).a((Tag)localObject2);
    }
    if (paramLong2 > 0L)
    {
      localObject2 = new Tag();
      ((Tag)localObject2).setSource(16);
      ((Tag)localObject2).a(String.valueOf(paramLong2));
      paramContact.a((Contact)localObject1, (Tag)localObject2);
      ((Contact)localObject1).a((Tag)localObject2);
    }
    return w.b(null);
  }
  
  public final w<Contact> a(Contact paramContact, String paramString, int paramInt)
  {
    Object localObject1 = new ArrayList();
    Object localObject2 = paramContact.A().iterator();
    while (((Iterator)localObject2).hasNext())
    {
      String str = ((Number)((Iterator)localObject2).next()).a();
      if (!TextUtils.isEmpty(str)) {
        ((List)localObject1).add(str);
      }
    }
    d.a(a, (List)localObject1, paramString, paramInt);
    localObject1 = paramContact;
    if (!com.truecaller.data.access.c.b(paramContact))
    {
      localObject1 = new com.truecaller.data.access.c(a).c(paramContact);
      if (localObject1 == null) {
        return w.b(paramContact);
      }
    }
    localObject2 = new k(a);
    paramContact = paramString;
    if (TextUtils.isEmpty(paramString)) {
      paramContact = null;
    }
    return w.b(((k)localObject2).a((Contact)localObject1, paramContact));
  }
  
  public final void a(Contact paramContact, int paramInt)
  {
    Iterator localIterator = paramContact.A().iterator();
    int i = 0;
    while (localIterator.hasNext())
    {
      String str = ((Number)localIterator.next()).a();
      if (!TextUtils.isEmpty(str))
      {
        d.a(a, str, paramInt);
        i = 1;
      }
    }
    if (i != 0)
    {
      if (!com.truecaller.data.access.c.b(paramContact)) {
        new com.truecaller.data.access.c(a).c(paramContact);
      }
      new k(a).a(paramContact, null);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tag.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */