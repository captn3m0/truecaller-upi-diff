package com.truecaller.tag;

import com.truecaller.androidactors.i;
import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d<com.truecaller.androidactors.f<c>>
{
  private final f a;
  private final Provider<i> b;
  private final Provider<c> c;
  
  private g(f paramf, Provider<i> paramProvider, Provider<c> paramProvider1)
  {
    a = paramf;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static g a(f paramf, Provider<i> paramProvider, Provider<c> paramProvider1)
  {
    return new g(paramf, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tag.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */