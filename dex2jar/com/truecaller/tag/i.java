package com.truecaller.tag;

import com.truecaller.androidactors.k;
import dagger.a.d;
import javax.inject.Provider;

public final class i
  implements d<com.truecaller.androidactors.i>
{
  private final f a;
  private final Provider<k> b;
  
  private i(f paramf, Provider<k> paramProvider)
  {
    a = paramf;
    b = paramProvider;
  }
  
  public static i a(f paramf, Provider<k> paramProvider)
  {
    return new i(paramf, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tag.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */