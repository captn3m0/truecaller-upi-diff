package com.truecaller;

import android.content.Context;
import android.support.v4.app.ac;
import dagger.a.d;
import javax.inject.Provider;

public final class ae
  implements d<ac>
{
  private final c a;
  private final Provider<Context> b;
  
  private ae(c paramc, Provider<Context> paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static ae a(c paramc, Provider<Context> paramProvider)
  {
    return new ae(paramc, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ae
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */