package com.truecaller;

import com.truecaller.multisim.h;
import com.truecaller.utils.n;
import javax.inject.Provider;

public final class au
  implements dagger.a.d<com.truecaller.calling.b.c>
{
  private final c a;
  private final Provider<com.truecaller.utils.d> b;
  private final Provider<com.truecaller.i.c> c;
  private final Provider<h> d;
  private final Provider<n> e;
  
  private au(c paramc, Provider<com.truecaller.utils.d> paramProvider, Provider<com.truecaller.i.c> paramProvider1, Provider<h> paramProvider2, Provider<n> paramProvider3)
  {
    a = paramc;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
  }
  
  public static au a(c paramc, Provider<com.truecaller.utils.d> paramProvider, Provider<com.truecaller.i.c> paramProvider1, Provider<h> paramProvider2, Provider<n> paramProvider3)
  {
    return new au(paramc, paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.au
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */