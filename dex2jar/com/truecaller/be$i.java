package com.truecaller;

import android.support.v4.app.j;
import com.truecaller.ads.b.f.a.a;
import com.truecaller.ads.b.f.a.b;
import com.truecaller.ads.b.f.a.d;
import com.truecaller.ads.b.f.a.e;
import com.truecaller.ads.b.f.a.f;
import com.truecaller.ads.b.f.a.g;
import com.truecaller.ads.b.q;
import com.truecaller.ads.b.r;
import com.truecaller.ads.b.u;
import com.truecaller.ads.b.v;
import com.truecaller.ads.b.w;
import com.truecaller.ads.b.x;
import com.truecaller.calling.dialer.ab;
import com.truecaller.calling.dialer.ac;
import com.truecaller.calling.dialer.ad;
import com.truecaller.calling.dialer.ae.a;
import com.truecaller.calling.dialer.af;
import com.truecaller.calling.dialer.ah;
import com.truecaller.calling.dialer.aj.b;
import com.truecaller.calling.dialer.ak;
import com.truecaller.calling.dialer.an;
import com.truecaller.calling.dialer.ay.a;
import com.truecaller.calling.dialer.az;
import com.truecaller.calling.dialer.ba;
import com.truecaller.calling.dialer.be.a;
import com.truecaller.calling.dialer.bf;
import com.truecaller.calling.dialer.bg;
import com.truecaller.calling.dialer.bi;
import com.truecaller.calling.dialer.bj;
import com.truecaller.calling.dialer.bk;
import com.truecaller.calling.dialer.bl;
import com.truecaller.calling.dialer.bn.a;
import com.truecaller.calling.dialer.bo;
import com.truecaller.calling.dialer.bp;
import com.truecaller.calling.dialer.bs;
import com.truecaller.calling.dialer.bw.a;
import com.truecaller.calling.dialer.bx;
import com.truecaller.calling.dialer.by;
import com.truecaller.calling.dialer.cd;
import com.truecaller.calling.dialer.cf.a;
import com.truecaller.calling.dialer.ch;
import com.truecaller.calling.dialer.ck.b;
import com.truecaller.calling.dialer.cl;
import com.truecaller.calling.dialer.cm;
import com.truecaller.calling.dialer.cn.b;
import com.truecaller.calling.dialer.co;
import com.truecaller.calling.dialer.cp;
import com.truecaller.calling.dialer.n.a;
import com.truecaller.referral.ReferralManager;
import com.truecaller.truepay.TcPaySDKListener;
import dagger.a.a;
import dagger.a.h.a;
import java.util.Set;
import javax.inject.Provider;

final class be$i
  implements com.truecaller.calling.dialer.y
{
  private Provider<com.truecaller.search.local.b.e> A;
  private Provider<bx> B;
  private Provider<bw.a> C;
  private Provider<ReferralManager> D;
  private Provider<com.truecaller.f.a.k> E;
  private Provider<com.truecaller.f.a.i> F;
  private Provider<com.truecaller.f.a.g> G;
  private Provider<com.truecaller.f.a.i> H;
  private Provider<j> I;
  private Provider<com.truecaller.f.a.b> J;
  private Provider<com.truecaller.f.a.i> K;
  private Provider<TcPaySDKListener> L;
  private Provider<com.truecaller.f.a.m> M;
  private Provider<com.truecaller.f.a.i> N;
  private Provider<com.truecaller.f.a.o> O;
  private Provider<com.truecaller.f.a.i> P;
  private Provider<com.truecaller.f.a.d> Q;
  private Provider<com.truecaller.f.a.i> R;
  private Provider<Set<com.truecaller.f.a.i>> S;
  private Provider<bf> T;
  private Provider<be.a> U;
  private Provider<az> V;
  private Provider<ay.a> W;
  private Provider<u> X;
  private Provider<f.a.e> Y;
  private Provider<com.truecaller.ads.b.m> Z;
  private Provider<f.a.b> aa;
  private Provider<com.truecaller.ads.b.o> ab;
  private Provider<com.truecaller.ads.b.k> ac;
  private Provider<f.a.a> ad;
  private Provider<com.truecaller.ads.b.s> ae;
  private Provider<f.a.d> af;
  private Provider<com.truecaller.ads.b.z> ag;
  private Provider<f.a.g> ah;
  private Provider<x> ai;
  private Provider<f.a.f> aj;
  private Provider<q> ak;
  private Provider<w> al;
  private Provider<com.truecaller.calling.dialer.e> b = com.truecaller.calling.dialer.f.a(be.k(a), be.H(a));
  private Provider<com.truecaller.calling.dialer.g> c = dagger.a.c.a(b);
  private Provider<com.truecaller.data.access.s> d = com.truecaller.data.access.t.a(be.X(a), com.truecaller.data.access.p.a());
  private Provider<bs> e = com.truecaller.calling.dialer.bt.a(d, be.aa(a));
  private Provider<com.truecaller.calling.dialer.br> f = dagger.a.c.a(e);
  private Provider<com.truecaller.ads.provider.e> g = dagger.a.c.a(com.truecaller.calling.dialer.aa.a(be.aj(a), be.G(a)));
  private Provider<com.truecaller.search.local.model.c> h = dagger.a.c.a(be.aq(a));
  private Provider<com.truecaller.calling.dialer.a.c> i = com.truecaller.calling.dialer.a.d.a(be.h(a), be.p(a), be.i(a), be.j(a), be.z(a));
  private Provider<com.truecaller.calling.dialer.a.b> j = dagger.a.c.a(i);
  private Provider<af> k = ah.a(be.aw(a), be.m(a), be.at(a), be.h(a), be.w(a), c, f, be.R(a), be.V(a), cd.a(), be.al(a), be.H(a), be.S(a), be.ax(a), be.ay(a), be.az(a), be.G(a), be.l(a), g, h, be.aA(a), be.ap(a), be.aB(a), be.n(a), j, be.a(a));
  private Provider<ae.a> l = dagger.a.c.a(k);
  private Provider<com.truecaller.flashsdk.core.i> m = dagger.a.c.a(ac.a(be.ay(a)));
  private Provider<com.truecaller.network.search.e> n = dagger.a.c.a(ab.a(be.o(a), l));
  private Provider<com.truecaller.calling.dialer.o> o;
  private Provider<n.a> p;
  private Provider<cf.a> q;
  private Provider<cl> r;
  private Provider<ck.b> s;
  private Provider<co> t;
  private Provider<cn.b> u;
  private Provider<ak> v;
  private Provider<aj.b> w;
  private Provider<com.truecaller.calling.dialer.s> x;
  private Provider<bo> y;
  private Provider<bn.a> z;
  
  private be$i(be parambe, bi parambi)
  {
    parambe = l;
    o = com.truecaller.calling.dialer.p.a(parambe, h, parambe, parambe, be.S(a), m, be.ay(a), be.w(a), be.at(a), be.aC(a), be.ai(a), be.z(a), be.aD(a), be.ax(a), be.ab(a), be.q(a), n, be.aE(a), be.aF(a));
    p = dagger.a.c.a(o);
    q = dagger.a.c.a(ch.a());
    r = cm.a(l, h, be.at(a), n, l, be.aC(a), be.ai(a), be.ab(a));
    s = dagger.a.c.a(r);
    parambe = l;
    t = cp.a(parambe, parambe);
    u = dagger.a.c.a(t);
    v = an.a(l, be.C(a), be.aG(a), be.h(a), be.w(a), be.aH(a), l, be.S(a), be.ai(a), l);
    w = dagger.a.c.a(v);
    x = dagger.a.c.a(com.truecaller.calling.dialer.z.a(be.X(a)));
    y = bp.a(l, be.h(a), be.at(a), l);
    z = dagger.a.c.a(y);
    A = dagger.a.c.a(ad.a());
    parambe = h;
    Provider localProvider = l;
    B = by.a(parambe, localProvider, localProvider, A, be.w(a), m, be.S(a), be.ay(a), be.ai(a));
    C = dagger.a.c.a(B);
    D = bk.a(parambi);
    E = com.truecaller.f.a.l.a(D, be.aI(a), be.a(a), be.S(a), be.h(a), be.t(a));
    F = dagger.a.c.a(E);
    G = com.truecaller.f.a.h.a(be.aI(a), be.a(a), be.p(a), be.S(a), be.h(a), be.t(a), be.z(a), com.truecaller.premium.bt.a());
    H = dagger.a.c.a(G);
    I = bj.a(parambi);
    J = com.truecaller.f.a.c.a(I, be.aI(a), be.a(a), be.p(a), be.S(a), be.v(a), be.h(a), be.t(a));
    K = dagger.a.c.a(J);
    L = bl.a(parambi);
    M = com.truecaller.f.a.n.a(L, be.aI(a), be.a(a), be.S(a), be.h(a), be.t(a));
    N = dagger.a.c.a(M);
    O = com.truecaller.f.a.p.a(be.aI(a), be.a(a), be.S(a), be.h(a), be.t(a), be.p(a), be.g(a), be.z(a));
    P = dagger.a.c.a(O);
    Q = com.truecaller.f.a.f.a(I, be.aI(a), be.a(a), be.S(a), be.h(a), be.t(a), be.F(a), com.truecaller.premium.bt.a());
    R = dagger.a.c.a(Q);
    parambe = dagger.a.h.a().a(F).a(H).a(K).a(N).a(P).a(R);
    if ((!h.a.c) && (a.a(a))) {
      throw new AssertionError("Codegen error?  Duplicates in the provider list");
    }
    if ((!h.a.c) && (a.a(b))) {
      throw new AssertionError("Codegen error?  Duplicates in the provider list");
    }
    S = new dagger.a.h(a, b, (byte)0);
    T = bg.a(l, be.a(a), S);
    U = dagger.a.c.a(T);
    parambe = l;
    V = ba.a(parambe, parambe, be.S(a), l, m, be.ay(a), be.aE(a), be.aF(a));
    W = dagger.a.c.a(V);
    X = v.a(g);
    Y = dagger.a.c.a(X);
    Z = com.truecaller.ads.b.n.a(g);
    aa = dagger.a.c.a(Z);
    ab = com.truecaller.ads.b.p.a(g);
    ac = com.truecaller.ads.b.l.a(g);
    ad = dagger.a.c.a(ac);
    ae = com.truecaller.ads.b.t.a(g);
    af = dagger.a.c.a(ae);
    ag = com.truecaller.ads.b.aa.a(g);
    ah = dagger.a.c.a(ag);
    ai = com.truecaller.ads.b.y.a(g);
    aj = dagger.a.c.a(ai);
    ak = r.a(Y, aa, ab, ad, af, ah, aj);
    al = dagger.a.c.a(ak);
  }
  
  public final void a(com.truecaller.calling.dialer.l paraml)
  {
    a = ((ae.a)l.get());
    b = ((n.a)p.get());
    c = ((cf.a)q.get());
    d = ((ck.b)s.get());
    e = ((cn.b)u.get());
    f = ((aj.b)w.get());
    g = ((com.truecaller.calling.dialer.s)x.get());
    h = ((bn.a)z.get());
    i = ((bw.a)C.get());
    j = ((be.a)U.get());
    k = ((ay.a)W.get());
    l = ((w)al.get());
    m = ((com.truecaller.calling.initiate_call.b)be.aJ(a).get());
    n = new com.truecaller.premium.br();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.be.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */