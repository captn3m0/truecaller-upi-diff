package com.truecaller.a;

import c.g.b.k;
import com.google.gson.o;
import com.truecaller.androidactors.w;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.network.util.h;
import e.r;
import java.io.IOException;
import javax.inject.Inject;

public final class d
  implements b
{
  public final w<Integer> a(String paramString)
  {
    k.b(paramString, "webId");
    paramString = ((i)h.a(KnownEndpoints.CONTACTREQUEST, i.class)).a(paramString);
    try
    {
      paramString = w.b(Integer.valueOf(paramString.c().b()));
      k.a(paramString, "Promise.wrap<Int>(response.code())");
      return paramString;
    }
    catch (IOException paramString)
    {
      for (;;) {}
    }
    paramString = w.b(Integer.valueOf(-1));
    k.a(paramString, "Promise.wrap<Int>(UNKNOWN_ERROR)");
    return paramString;
  }
  
  public final w<Integer> a(String paramString1, String paramString2)
  {
    k.b(paramString1, "receiver");
    k.b(paramString2, "name");
    o localo = new o();
    localo.a("receiverName", paramString2);
    paramString1 = ((i)h.a(KnownEndpoints.CONTACTREQUEST, i.class)).a(paramString1, localo);
    try
    {
      paramString1 = w.b(Integer.valueOf(paramString1.c().b()));
      k.a(paramString1, "Promise.wrap<Int>(response.code())");
      return paramString1;
    }
    catch (IOException paramString1)
    {
      for (;;) {}
    }
    paramString1 = w.b(Integer.valueOf(-1));
    k.a(paramString1, "Promise.wrap<Int>(UNKNOWN_ERROR)");
    return paramString1;
  }
  
  public final w<Integer> b(String paramString)
  {
    k.b(paramString, "webId");
    paramString = ((i)h.a(KnownEndpoints.CONTACTREQUEST, i.class)).b(paramString);
    try
    {
      paramString = w.b(Integer.valueOf(paramString.c().b()));
      k.a(paramString, "Promise.wrap<Int>(response.code())");
      return paramString;
    }
    catch (IOException paramString)
    {
      for (;;) {}
    }
    paramString = w.b(Integer.valueOf(-1));
    k.a(paramString, "Promise.wrap<Int>(UNKNOWN_ERROR)");
    return paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.a.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */