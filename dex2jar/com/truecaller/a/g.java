package com.truecaller.a;

import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.w;
import com.truecaller.common.f.c;
import javax.inject.Inject;

public final class g
  implements f
{
  final c a;
  private final com.truecaller.androidactors.f<b> b;
  private final com.truecaller.androidactors.k c;
  
  @Inject
  public g(com.truecaller.androidactors.f<b> paramf, com.truecaller.androidactors.k paramk, c paramc)
  {
    b = paramf;
    c = paramk;
    a = paramc;
  }
  
  public final void a(String paramString, f.a parama)
  {
    c.g.b.k.b(paramString, "webId");
    c.g.b.k.b(parama, "callback");
    ((b)b.a()).a(paramString).a(c.a(), (ac)new g.a(parama));
  }
  
  public final void a(String paramString1, String paramString2, f.b paramb)
  {
    c.g.b.k.b(paramString1, "receiver");
    c.g.b.k.b(paramString2, "name");
    c.g.b.k.b(paramb, "callback");
    ((b)b.a()).a(paramString1, paramString2).a(c.a(), (ac)new g.b(this, paramb, paramString2));
  }
  
  public final void b(String paramString, f.a parama)
  {
    c.g.b.k.b(paramString, "webId");
    c.g.b.k.b(parama, "callback");
    ((b)b.a()).b(paramString).a(c.a(), (ac)new g.c(parama));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.a.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */