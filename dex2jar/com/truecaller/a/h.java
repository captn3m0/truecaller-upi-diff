package com.truecaller.a;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.k;
import com.truecaller.common.f.c;
import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d<g>
{
  private final Provider<f<b>> a;
  private final Provider<k> b;
  private final Provider<c> c;
  
  private h(Provider<f<b>> paramProvider, Provider<k> paramProvider1, Provider<c> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static h a(Provider<f<b>> paramProvider, Provider<k> paramProvider1, Provider<c> paramProvider2)
  {
    return new h(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.a.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */