package com.truecaller.a;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import dagger.a.d;
import javax.inject.Provider;

public final class a
  implements d<f<b>>
{
  private final Provider<i> a;
  private final Provider<b> b;
  
  private a(Provider<i> paramProvider, Provider<b> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static a a(Provider<i> paramProvider, Provider<b> paramProvider1)
  {
    return new a(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */