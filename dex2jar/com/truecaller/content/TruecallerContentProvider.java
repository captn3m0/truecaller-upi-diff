package com.truecaller.content;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import com.truecaller.common.c.a.a.a;
import com.truecaller.common.c.a.a.b;
import com.truecaller.common.c.a.a.c;
import com.truecaller.common.c.a.a.e;
import com.truecaller.common.c.a.a.f;
import com.truecaller.common.c.a.a.h;
import com.truecaller.common.c.a.c;
import com.truecaller.content.c.ag;
import com.truecaller.content.c.ag.a;
import com.truecaller.content.c.p;
import java.util.Collection;
import java.util.HashSet;

public class TruecallerContentProvider
  extends com.truecaller.common.c.a
  implements com.truecaller.common.c.a.e
{
  Handler b;
  private final ThreadLocal<TruecallerContentProvider.AggregationState> c = new ThreadLocal();
  private final t d = new t();
  
  private static Uri a(com.truecaller.common.c.a.d paramd, String paramString1, String paramString2)
  {
    com.truecaller.common.c.a.b localb = paramd.a(paramString1);
    c = true;
    a = paramString2;
    localb = localb.a().a(paramString1);
    c = true;
    a = paramString2;
    b = true;
    localb = localb.a().a(paramString1);
    c = true;
    a = paramString2;
    d = true;
    localb.a();
    return paramd.a(paramString1).b();
  }
  
  public static void c(Context paramContext)
  {
    Intent localIntent = new Intent("ACTION_RESTORE_AGGREGATION");
    localIntent.putExtra("ARG_DELAY", 100L);
    android.support.v4.content.d.a(paramContext).a(localIntent);
  }
  
  private TruecallerContentProvider.AggregationState e()
  {
    TruecallerContentProvider.AggregationState localAggregationState2 = (TruecallerContentProvider.AggregationState)c.get();
    TruecallerContentProvider.AggregationState localAggregationState1 = localAggregationState2;
    if (localAggregationState2 == null) {
      localAggregationState1 = TruecallerContentProvider.AggregationState.NONE;
    }
    return localAggregationState1;
  }
  
  final void a(long paramLong)
  {
    b.sendEmptyMessageDelayed(1, paramLong);
  }
  
  final void a(TruecallerContentProvider.AggregationState paramAggregationState)
  {
    if (e().ordinal() < paramAggregationState.ordinal()) {
      c.set(paramAggregationState);
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    super.a(paramBoolean);
    TruecallerContentProvider.AggregationState localAggregationState = e();
    if ((localAggregationState == TruecallerContentProvider.AggregationState.DELAYED) || (localAggregationState == TruecallerContentProvider.AggregationState.IMMEDIATE))
    {
      c.remove();
      a(100L);
    }
  }
  
  public final SQLiteDatabase a_(Context paramContext)
    throws SQLiteException
  {
    Object localObject = ag.a(paramContext, ag.b(), com.truecaller.common.b.a.F().v().c());
    try
    {
      localObject = ((ag)localObject).getWritableDatabase();
      return (SQLiteDatabase)localObject;
    }
    catch (ag.a locala)
    {
      paramContext.deleteDatabase("tc.db");
      com.truecaller.common.b.a.F().a(false);
      throw a;
    }
  }
  
  public final c b(Context paramContext)
  {
    Object localObject1 = (com.truecaller.common.b.a)paramContext.getApplicationContext();
    com.truecaller.featuretoggles.e locale = ((com.truecaller.common.b.a)localObject1).f();
    Object localObject4 = ((com.truecaller.common.b.a)localObject1).g();
    com.truecaller.common.g.a locala = ((com.truecaller.common.b.a)localObject1).u().c();
    Object localObject2 = com.truecaller.common.c.b.b.a(paramContext, getClass());
    localObject1 = new com.truecaller.common.c.a.d();
    d = ((String)localObject2);
    if ((localObject2 != null) && (e == null)) {
      e = Uri.parse("content://".concat(String.valueOf(localObject2)));
    }
    if (c == null)
    {
      c = this;
      Object localObject3 = new HashSet();
      ((HashSet)localObject3).add(a((com.truecaller.common.c.a.d)localObject1, "history_with_raw_contact", "history_with_raw_contact"));
      ((HashSet)localObject3).add(a((com.truecaller.common.c.a.d)localObject1, "history_with_aggregated_contact", "history_with_aggregated_contact"));
      ((HashSet)localObject3).add(a((com.truecaller.common.c.a.d)localObject1, "history_top_called_with_aggregated_contact", "history_top_called_with_aggregated_contact"));
      ((HashSet)localObject3).add(a((com.truecaller.common.c.a.d)localObject1, "history_with_aggregated_contact_number", "history_with_aggregated_contact_number"));
      ((HashSet)localObject3).add(a((com.truecaller.common.c.a.d)localObject1, "history_with_call_recording", "history_with_call_recording"));
      ((HashSet)localObject3).add(a((com.truecaller.common.c.a.d)localObject1, "call_recordings_with_history_event", "call_recordings_with_history_event"));
      Uri localUri = a((com.truecaller.common.c.a.d)localObject1, "sorted_contacts_with_data", "sorted_contacts_with_data");
      ((HashSet)localObject3).add(localUri);
      ((HashSet)localObject3).add(a((com.truecaller.common.c.a.d)localObject1, "sorted_contacts_shallow", "sorted_contacts_shallow"));
      localObject2 = new HashSet();
      ((HashSet)localObject2).add(TruecallerContract.n.c());
      ((HashSet)localObject2).add(TruecallerContract.n.b());
      ((HashSet)localObject2).add(TruecallerContract.n.e());
      ((HashSet)localObject2).add(TruecallerContract.n.d());
      ((HashSet)localObject2).add(TruecallerContract.c.b());
      Object localObject5 = ((com.truecaller.common.c.a.d)localObject1).a("aggregated_contact");
      e = 5;
      ((com.truecaller.common.c.a.b)localObject5).a((Collection)localObject3).a();
      localObject5 = ((com.truecaller.common.c.a.d)localObject1).a("aggregated_contact").a((Collection)localObject3);
      b = true;
      ((com.truecaller.common.c.a.b)localObject5).a();
      localObject5 = ((com.truecaller.common.c.a.d)localObject1).a("aggregated_contact");
      d = true;
      ((com.truecaller.common.c.a.b)localObject5).a();
      localObject5 = ((com.truecaller.common.c.a.d)localObject1).a("aggregated_contact_t9").a(false).b(true);
      f = new p(true);
      ((com.truecaller.common.c.a.b)localObject5).a();
      localObject5 = ((com.truecaller.common.c.a.d)localObject1).a("aggregated_contact_plain_text").a(false).b(true);
      f = new p(false);
      ((com.truecaller.common.c.a.b)localObject5).a();
      localObject5 = ((com.truecaller.common.c.a.d)localObject1).a("raw_contact");
      e = 5;
      Object localObject6 = d;
      g = ((a.f)localObject6);
      j = ((a.b)localObject6);
      i = ((a.e)localObject6);
      ((com.truecaller.common.c.a.b)localObject5).a((Collection)localObject3).a();
      localObject5 = ((com.truecaller.common.c.a.d)localObject1).a("raw_contact");
      i = d;
      localObject5 = ((com.truecaller.common.c.a.b)localObject5).a((Collection)localObject3);
      b = true;
      ((com.truecaller.common.c.a.b)localObject5).a();
      localObject5 = ((com.truecaller.common.c.a.d)localObject1).a("raw_contact");
      d = true;
      ((com.truecaller.common.c.a.b)localObject5).a();
      localObject5 = new j((com.truecaller.content.d.a)localObject4);
      localObject6 = new k((com.truecaller.content.d.a)localObject4);
      localObject4 = new i((com.truecaller.content.d.a)localObject4);
      com.truecaller.common.c.a.b localb = ((com.truecaller.common.c.a.d)localObject1).a("history");
      j = ((a.b)localObject5);
      k = ((a.c)localObject6);
      l = ((a.a)localObject4);
      localb.a((Collection)localObject3).a();
      localObject5 = ((com.truecaller.common.c.a.d)localObject1).a("history").a((Collection)localObject3);
      b = true;
      l = ((a.a)localObject4);
      ((com.truecaller.common.c.a.b)localObject5).a();
      localObject4 = ((com.truecaller.common.c.a.d)localObject1).a("history");
      d = true;
      ((com.truecaller.common.c.a.b)localObject4).a();
      ((HashSet)localObject3).add(((com.truecaller.common.c.a.d)localObject1).a("raw_contact").b());
      ((HashSet)localObject3).add(a((com.truecaller.common.c.a.d)localObject1, "raw_contact_data", "raw_contact/data"));
      ((HashSet)localObject3).add(a((com.truecaller.common.c.a.d)localObject1, "aggregated_contact_data", "aggregated_contact/data"));
      localObject4 = new q();
      localObject5 = ((com.truecaller.common.c.a.d)localObject1).a("data");
      g = ((a.f)localObject4);
      j = ((a.b)localObject4);
      ((com.truecaller.common.c.a.b)localObject5).a((Collection)localObject3).a();
      localObject5 = ((com.truecaller.common.c.a.d)localObject1).a("data");
      g = ((a.f)localObject4);
      j = ((a.b)localObject4);
      localObject3 = ((com.truecaller.common.c.a.b)localObject5).a((Collection)localObject3);
      b = true;
      ((com.truecaller.common.c.a.b)localObject3).a();
      localObject3 = ((com.truecaller.common.c.a.d)localObject1).a("data");
      d = true;
      ((com.truecaller.common.c.a.b)localObject3).a();
      localObject3 = new d(locale);
      localObject4 = ((com.truecaller.common.c.a.d)localObject1).a("msg_conversations");
      a = "msg/msg_conversations";
      localObject4 = ((com.truecaller.common.c.a.b)localObject4).a(true);
      g = ((a.f)localObject3);
      h = ((a.h)localObject3);
      i = ((a.e)localObject3);
      ((com.truecaller.common.c.a.b)localObject4).a();
      localObject3 = ((com.truecaller.common.c.a.d)localObject1).a("msg_thread_stats");
      a = "msg/msg_thread_stats";
      ((com.truecaller.common.c.a.b)localObject3).a();
      localObject3 = ((com.truecaller.common.c.a.d)localObject1).a("msg/msg_thread_stats_specific_update");
      h = new v();
      ((com.truecaller.common.c.a.b)localObject3).b(false).a(true).a();
      localObject3 = ((com.truecaller.common.c.a.d)localObject1).a("msg_conversations_list");
      a = "msg/msg_conversations_list";
      b = true;
      localObject3 = ((com.truecaller.common.c.a.b)localObject3).a(false);
      f = new e(locale);
      ((com.truecaller.common.c.a.b)localObject3).a();
      localObject3 = ((com.truecaller.common.c.a.d)localObject1).a("msg_conversations_list");
      a = "msg/msg_conversations_list";
      localObject3 = ((com.truecaller.common.c.a.b)localObject3).a(false);
      f = new e(locale);
      ((com.truecaller.common.c.a.b)localObject3).a();
      localObject3 = ((com.truecaller.common.c.a.d)localObject1).a("msg_conversation_transport_info");
      a = "msg/msg_conversation_transport_info";
      ((com.truecaller.common.c.a.b)localObject3).a(false).a();
      localObject3 = new r();
      localObject4 = ((com.truecaller.common.c.a.d)localObject1).a("msg_participants");
      a = "msg/msg_participants";
      g = ((a.f)localObject3);
      h = ((a.h)localObject3);
      ((com.truecaller.common.c.a.b)localObject4).b(true).a();
      localObject3 = ((com.truecaller.common.c.a.d)localObject1).a("msg_conversation_participants");
      a = "msg/msg_conversation_participants";
      ((com.truecaller.common.c.a.b)localObject3).a(false).b(true).a();
      localObject3 = ((com.truecaller.common.c.a.d)localObject1).a("msg_participants_with_contact_info");
      a = "msg/msg_participants_with_contact_info";
      f = new s(paramContext);
      ((com.truecaller.common.c.a.b)localObject3).a(false).a();
      localObject3 = new n();
      localObject4 = ((com.truecaller.common.c.a.d)localObject1).a("msg_messages");
      a = "msg/msg_messages";
      g = ((a.f)localObject3);
      ((com.truecaller.common.c.a.b)localObject4).a(new Uri[] { TruecallerContract.g.a() }).a();
      localObject4 = ((com.truecaller.common.c.a.d)localObject1).a("msg_messages");
      a = "msg/msg_messages";
      b = true;
      h = ((a.h)localObject3);
      i = ((a.e)localObject3);
      ((com.truecaller.common.c.a.b)localObject4).a(new Uri[] { TruecallerContract.g.a() }).a();
      localObject3 = ((com.truecaller.common.c.a.d)localObject1).a("msg_entities");
      a = "msg/msg_entities";
      ((com.truecaller.common.c.a.b)localObject3).a(new Uri[] { TruecallerContract.ab.a() }).a(new Uri[] { TruecallerContract.g.a() }).a();
      localObject3 = ((com.truecaller.common.c.a.d)localObject1).a("msg_sms_transport_info");
      a = "msg/msg_sms_transport_info";
      ((com.truecaller.common.c.a.b)localObject3).b(false).a();
      localObject3 = ((com.truecaller.common.c.a.d)localObject1).a("msg_mms_transport_info");
      a = "msg/msg_mms_transport_info";
      ((com.truecaller.common.c.a.b)localObject3).b(false).a();
      localObject3 = ((com.truecaller.common.c.a.d)localObject1).a("msg_im_transport_info");
      a = "msg/msg_im_transport_info";
      ((com.truecaller.common.c.a.b)localObject3).b(true).a(new Uri[] { TruecallerContract.ab.a() }).a(new Uri[] { TruecallerContract.g.a() }).a();
      localObject3 = ((com.truecaller.common.c.a.d)localObject1).a("msg_history_transport_info");
      a = "msg/msg_history_transport_info";
      ((com.truecaller.common.c.a.b)localObject3).b(true).a(new Uri[] { TruecallerContract.ab.a() }).a(new Uri[] { TruecallerContract.g.a() }).a();
      localObject3 = ((com.truecaller.common.c.a.d)localObject1).a("msg_status_transport_info");
      a = "msg/msg_status_transport_info";
      ((com.truecaller.common.c.a.b)localObject3).b(true).a(true).a();
      localObject3 = ((com.truecaller.common.c.a.d)localObject1).a("msg_im_reactions");
      a = "msg/msg_im_reactions";
      ((com.truecaller.common.c.a.b)localObject3).a(new Uri[] { TruecallerContract.ab.a() }).a();
      localObject3 = ((com.truecaller.common.c.a.d)localObject1).a("reaction_with_participants").a(false).b(true);
      f = new u();
      ((com.truecaller.common.c.a.b)localObject3).a();
      localObject3 = ((com.truecaller.common.c.a.d)localObject1).a("msg_messages_by_transport_view");
      a = "msg/msg_messages_by_transport_view";
      ((com.truecaller.common.c.a.b)localObject3).b(true).a(false).a();
      localObject3 = ((com.truecaller.common.c.a.d)localObject1).a("msg/msg_messages_with_entities").a(false).b(true);
      f = new com.truecaller.content.c.s(paramContext);
      b = true;
      ((com.truecaller.common.c.a.b)localObject3).a();
      localObject3 = ((com.truecaller.common.c.a.d)localObject1).a("msg_messages_with_entities");
      a = "msg/msg_messages_with_entities";
      localObject3 = ((com.truecaller.common.c.a.b)localObject3).a(false).b(true);
      f = new com.truecaller.content.c.s(paramContext);
      ((com.truecaller.common.c.a.b)localObject3).a();
      paramContext = ((com.truecaller.common.c.a.d)localObject1).a("msg_messages_with_entities");
      a = "msg/msg_messages_with_entities_filtered";
      paramContext = paramContext.a(false);
      f = new com.truecaller.content.c.r();
      paramContext.a();
      paramContext = ((com.truecaller.common.c.a.d)localObject1).a("msg_im_attachments");
      a = "msg/msg_im_attachments";
      paramContext.a();
      ((com.truecaller.common.c.a.d)localObject1).a("msg_im_attachments_entities").a(false).b(true).a();
      paramContext = ((com.truecaller.common.c.a.d)localObject1).a("msg_im_sync_view");
      a = "msg/msg_im_sync_view";
      paramContext.a(false).b(true).a();
      paramContext = new m();
      localObject3 = ((com.truecaller.common.c.a.d)localObject1).a("msg_im_users");
      a = "msg/msg_im_users";
      localObject3 = ((com.truecaller.common.c.a.b)localObject3).a(true).b(true);
      g = paramContext;
      h = paramContext;
      i = paramContext;
      e = 5;
      ((com.truecaller.common.c.a.b)localObject3).a();
      paramContext = ((com.truecaller.common.c.a.d)localObject1).a("msg_im_group_participants");
      a = "msg/msg_im_group_participants";
      paramContext = paramContext.a(true).b(true);
      e = 5;
      paramContext.a(new Uri[] { TruecallerContract.t.a() }).a();
      paramContext = ((com.truecaller.common.c.a.d)localObject1).a("msg_im_group_info");
      a = "msg/msg_im_group_info";
      paramContext = paramContext.a(true).b(true);
      e = 5;
      paramContext.a(new Uri[] { TruecallerContract.g.a() }).a();
      paramContext = ((com.truecaller.common.c.a.d)localObject1).a("msg_im_group_participants_view");
      a = "msg/msg_im_group_participants_view";
      paramContext = paramContext.a(false).b(true);
      f = new l();
      paramContext.a();
      paramContext = ((com.truecaller.common.c.a.d)localObject1).a("new_conversation_items").a(false).b(true);
      f = new o(locala, locale);
      paramContext.a();
      paramContext = ((com.truecaller.common.c.a.d)localObject1).a("conversation_messages").a(true).b(true);
      f = new z();
      paramContext.a();
      paramContext = ((com.truecaller.common.c.a.d)localObject1).a("filters");
      a = "filters";
      g = new g();
      h = new h();
      i = new f();
      paramContext = paramContext.a().a("filters");
      a = "filters";
      b = true;
      paramContext = paramContext.a().a("filters");
      a = "filters";
      d = true;
      paramContext.a();
      paramContext = ((com.truecaller.common.c.a.d)localObject1).a("topspammers");
      a = "topspammers";
      j = new x();
      h = new y();
      l = new w();
      paramContext = paramContext.a().a("topspammers");
      a = "topspammers";
      b = true;
      paramContext = paramContext.a().a("topspammers");
      a = "topspammers";
      d = true;
      paramContext.a();
      ((com.truecaller.common.c.a.d)localObject1).a("t9_mapping").a(true).b(true).a();
      ((com.truecaller.common.c.a.d)localObject1).a("contact_sorting_index").a(new Uri[] { localUri }).a(true).b(true).a();
      paramContext = ((com.truecaller.common.c.a.d)localObject1).a("contact_sorting_index");
      a = "contact_sorting_index/fast_scroll";
      paramContext = paramContext.a(false).b(true);
      f = new com.truecaller.content.c.o();
      paramContext.a();
      paramContext = ((com.truecaller.common.c.a.d)localObject1).a("call_recordings");
      a = "call_recordings";
      paramContext.a((Collection)localObject2).a(true).b(true).a();
      paramContext = ((com.truecaller.common.c.a.d)localObject1).a("profile_view_events");
      a = "profile_view_events";
      paramContext.a(true).b(true).a();
      paramContext = ((com.truecaller.common.c.a.d)localObject1).a("spam_url_reports");
      a = "spam_url_reports";
      e = 5;
      paramContext.a();
      paramContext = ((com.truecaller.common.c.a.d)localObject1).a("msg_im_unsupported_events");
      a = "msg/msg_im_unsupported_events";
      paramContext.a(true).b(true).a();
      paramContext = ((com.truecaller.common.c.a.d)localObject1).a("contact_settings");
      a = "contact_settings";
      paramContext = paramContext.a(true).b(true);
      e = 5;
      paramContext.a();
      return ((com.truecaller.common.c.a.d)localObject1).a();
    }
    throw new IllegalStateException("Database factory already set");
  }
  
  public final void d()
  {
    super.d();
    if (e() == TruecallerContentProvider.AggregationState.IMMEDIATE)
    {
      d.a(c());
      c.remove();
      a(TruecallerContract.a.a());
    }
  }
  
  public void onBegin()
  {
    super.onBegin();
    c.remove();
    if (b.hasMessages(1))
    {
      b.removeMessages(1);
      a(TruecallerContentProvider.AggregationState.DELAYED);
    }
  }
  
  public boolean onCreate()
  {
    android.support.v4.content.d.a(getContext()).a(new TruecallerContentProvider.1(this), new IntentFilter("ACTION_RESTORE_AGGREGATION"));
    HandlerThread localHandlerThread = new HandlerThread("Aggregation", 10);
    localHandlerThread.start();
    b = new Handler(localHandlerThread.getLooper(), new TruecallerContentProvider.a(this, (byte)0));
    TruecallerContract.a(com.truecaller.common.c.b.b.a(getContext(), getClass()));
    return super.onCreate();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.TruecallerContentProvider
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */