package com.truecaller.content;

import android.content.Context;
import c.d.f;
import com.truecaller.common.h.u;
import com.truecaller.utils.s;
import dagger.a.d;
import javax.inject.Provider;

public final class b
  implements d<aa>
{
  private final a a;
  private final Provider<Context> b;
  private final Provider<u> c;
  private final Provider<f> d;
  private final Provider<s> e;
  
  private b(a parama, Provider<Context> paramProvider, Provider<u> paramProvider1, Provider<f> paramProvider2, Provider<s> paramProvider3)
  {
    a = parama;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
  }
  
  public static b a(a parama, Provider<Context> paramProvider, Provider<u> paramProvider1, Provider<f> paramProvider2, Provider<s> paramProvider3)
  {
    return new b(parama, paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */