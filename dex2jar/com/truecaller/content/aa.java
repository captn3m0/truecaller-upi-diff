package com.truecaller.content;

import android.content.ContentResolver;
import c.d.f;
import com.truecaller.common.h.u;
import com.truecaller.utils.s;
import javax.inject.Inject;

public final class aa
{
  final ContentResolver a;
  final u b;
  public final f c;
  final s d;
  
  @Inject
  public aa(ContentResolver paramContentResolver, u paramu, f paramf, s params)
  {
    a = paramContentResolver;
    b = paramu;
    c = paramf;
    d = params;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.content.aa
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */