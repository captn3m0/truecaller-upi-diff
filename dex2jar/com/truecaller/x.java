package com.truecaller;

import android.content.Context;
import com.truecaller.util.af;
import dagger.a.d;
import javax.inject.Provider;

public final class x
  implements d<af>
{
  private final c a;
  private final Provider<Context> b;
  
  private x(c paramc, Provider<Context> paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static x a(c paramc, Provider<Context> paramProvider)
  {
    return new x(paramc, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.x
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */