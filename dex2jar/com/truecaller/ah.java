package com.truecaller;

import com.truecaller.calling.ak;
import com.truecaller.calling.f;
import com.truecaller.calling.k;
import com.truecaller.common.h.u;
import com.truecaller.utils.a;
import com.truecaller.utils.l;
import javax.inject.Provider;

public final class ah
  implements dagger.a.d<ak>
{
  private final c a;
  private final Provider<k> b;
  private final Provider<f> c;
  private final Provider<l> d;
  private final Provider<a> e;
  private final Provider<com.truecaller.callerid.a.d> f;
  private final Provider<u> g;
  
  private ah(c paramc, Provider<k> paramProvider, Provider<f> paramProvider1, Provider<l> paramProvider2, Provider<a> paramProvider3, Provider<com.truecaller.callerid.a.d> paramProvider4, Provider<u> paramProvider5)
  {
    a = paramc;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
    f = paramProvider4;
    g = paramProvider5;
  }
  
  public static ah a(c paramc, Provider<k> paramProvider, Provider<f> paramProvider1, Provider<l> paramProvider2, Provider<a> paramProvider3, Provider<com.truecaller.callerid.a.d> paramProvider4, Provider<u> paramProvider5)
  {
    return new ah(paramc, paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ah
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */