package com.truecaller;

import com.truecaller.androidactors.f;
import com.truecaller.callerid.a;
import com.truecaller.callerid.e;
import javax.inject.Provider;

public final class h
  implements dagger.a.d<a>
{
  private final c a;
  private final Provider<f<e>> b;
  private final Provider<com.truecaller.utils.d> c;
  
  private h(c paramc, Provider<f<e>> paramProvider, Provider<com.truecaller.utils.d> paramProvider1)
  {
    a = paramc;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static h a(c paramc, Provider<f<e>> paramProvider, Provider<com.truecaller.utils.d> paramProvider1)
  {
    return new h(paramc, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */