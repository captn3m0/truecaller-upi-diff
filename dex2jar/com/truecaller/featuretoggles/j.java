package com.truecaller.featuretoggles;

import android.content.SharedPreferences;
import java.util.Iterator;

public final class j
  extends e
{
  private final SharedPreferences U;
  
  public j(a parama, SharedPreferences paramSharedPreferences, d paramd, p paramp)
  {
    super(parama, paramSharedPreferences, paramd, paramp);
    U = paramSharedPreferences;
    parama = ((Iterable)a()).iterator();
    while (parama.hasNext())
    {
      paramSharedPreferences = (b)parama.next();
      if ((paramSharedPreferences instanceof n)) {
        a(paramSharedPreferences, (c.g.a.b)new j.a(paramSharedPreferences, this, paramp));
      } else if ((paramSharedPreferences instanceof g)) {
        a(paramSharedPreferences, (c.g.a.b)new j.b(paramSharedPreferences, this, paramp));
      } else {
        a(paramSharedPreferences, (c.g.a.b)new j.c(this, paramp));
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.featuretoggles.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */