package com.truecaller.featuretoggles;

import c.g.b.k;
import java.util.LinkedHashMap;
import java.util.Map;

public final class d
{
  private final Map<String, b> a;
  
  private d(Map<String, ? extends b> paramMap)
  {
    a = paramMap;
  }
  
  public final void a(m paramm)
  {
    k.b(paramm, "feature");
    b localb = (b)a.get(paramm.d());
    if (localb == null) {
      return;
    }
    if (paramm.a())
    {
      localb.b();
      return;
    }
    localb.c();
  }
  
  public static final class a
  {
    public final Map<String, d.b> a = (Map)new LinkedHashMap();
    
    public final a a(d.b paramb)
    {
      k.b(paramb, "observer");
      a.put(paramb.a(), paramb);
      return this;
    }
  }
  
  public static abstract interface b
  {
    public abstract String a();
    
    public abstract void b();
    
    public abstract void c();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.featuretoggles.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */