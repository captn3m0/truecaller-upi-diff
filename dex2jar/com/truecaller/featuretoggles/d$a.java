package com.truecaller.featuretoggles;

import c.g.b.k;
import java.util.LinkedHashMap;
import java.util.Map;

public final class d$a
{
  public final Map<String, d.b> a = (Map)new LinkedHashMap();
  
  public final a a(d.b paramb)
  {
    k.b(paramb, "observer");
    a.put(paramb.a(), paramb);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.featuretoggles.d.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */