package com.truecaller;

import android.content.ContentResolver;
import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class u
  implements d<ContentResolver>
{
  private final c a;
  private final Provider<Context> b;
  
  private u(c paramc, Provider<Context> paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static u a(c paramc, Provider<Context> paramProvider)
  {
    return new u(paramc, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.u
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */