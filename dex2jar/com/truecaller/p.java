package com.truecaller;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class p
  implements d<com.truecaller.callerid.c>
{
  private final c a;
  private final Provider<Context> b;
  
  private p(c paramc, Provider<Context> paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static p a(c paramc, Provider<Context> paramProvider)
  {
    return new p(paramc, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */