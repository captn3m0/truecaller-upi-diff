package com.truecaller;

import android.telephony.TelephonyManager;
import com.truecaller.common.g.a;
import com.truecaller.d.b;
import com.truecaller.featuretoggles.e;
import dagger.a.d;
import javax.inject.Provider;

public final class y
  implements d<b>
{
  private final c a;
  private final Provider<e> b;
  private final Provider<a> c;
  private final Provider<TelephonyManager> d;
  
  private y(c paramc, Provider<e> paramProvider, Provider<a> paramProvider1, Provider<TelephonyManager> paramProvider2)
  {
    a = paramc;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
  }
  
  public static y a(c paramc, Provider<e> paramProvider, Provider<a> paramProvider1, Provider<TelephonyManager> paramProvider2)
  {
    return new y(paramc, paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.y
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */