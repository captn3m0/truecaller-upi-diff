package com.truecaller.flash;

import android.content.Intent;
import c.d.f;
import c.g.b.k;
import com.truecaller.flashsdk.core.b;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;

public final class n
  implements m
{
  final b a;
  final f b;
  private final f c;
  
  @Inject
  public n(b paramb, @Named("Async") f paramf1, @Named("UI") f paramf2)
  {
    a = paramb;
    b = paramf1;
    c = paramf2;
  }
  
  public final void a(Intent paramIntent)
  {
    k.b(paramIntent, "intent");
    if (!paramIntent.hasExtra("com.truecaller.datamanager.EXTRA_PRESENCE")) {
      return;
    }
    e.b((ag)bg.a, c, (c.g.a.m)new n.a(this, paramIntent, null), 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flash.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */