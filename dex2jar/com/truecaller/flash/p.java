package com.truecaller.flash;

import c.g.b.k;
import com.truecaller.flashsdk.core.Theme;
import com.truecaller.flashsdk.core.b;
import com.truecaller.ui.ThemeManager.Theme;
import javax.inject.Inject;

public final class p
  implements o
{
  private final b a;
  
  @Inject
  public p(b paramb)
  {
    a = paramb;
  }
  
  public final void a(ThemeManager.Theme paramTheme)
  {
    k.b(paramTheme, "theme");
    switch (q.a[paramTheme.ordinal()])
    {
    default: 
      paramTheme = Theme.LIGHT;
      break;
    case 6: 
      paramTheme = Theme.LIGHT_GRAY;
      break;
    case 5: 
      paramTheme = Theme.PITCH_BLACK;
      break;
    case 4: 
      paramTheme = Theme.RAMADAN;
      break;
    case 3: 
      paramTheme = Theme.COFFEE;
      break;
    case 2: 
      paramTheme = Theme.LIGHT;
      break;
    case 1: 
      paramTheme = Theme.DARK;
    }
    a.a(paramTheme);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flash.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */