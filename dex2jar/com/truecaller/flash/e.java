package com.truecaller.flash;

import c.g.b.k;
import c.n.m;
import com.truecaller.callhistory.a;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.Sender;
import javax.inject.Inject;

public final class e
  implements d
{
  private final a a;
  
  @Inject
  public e(a parama)
  {
    a = parama;
  }
  
  public final void a(Flash paramFlash)
  {
    k.b(paramFlash, "flash");
    int i;
    if (paramFlash.k()) {
      i = 1;
    } else {
      i = 2;
    }
    Object localObject = paramFlash.e();
    k.a(localObject, "flash.history");
    if (!m.a((CharSequence)localObject, (CharSequence)" ", false))
    {
      localObject = new StringBuilder("should not be in db for ");
      ((StringBuilder)localObject).append(paramFlash.e());
      ((StringBuilder)localObject).toString();
      long l2;
      long l1;
      if (paramFlash.k())
      {
        localObject = paramFlash.a();
        k.a(localObject, "flash.sender");
        l2 = ((Sender)localObject).a().longValue();
        l1 = System.currentTimeMillis();
        localObject = String.valueOf(l2);
      }
      else
      {
        l2 = paramFlash.b();
        l1 = paramFlash.g();
        localObject = String.valueOf(l2);
      }
      a.a(new HistoryEvent(new CallLogFlashItem(paramFlash.g(), "+".concat(String.valueOf(localObject)), l1, 0L, i, 0, 0, null, null, 480, null)));
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flash.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */