package com.truecaller.flash;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d<b>
{
  private final f a;
  private final Provider<Context> b;
  private final Provider<com.truecaller.callhistory.a> c;
  private final Provider<com.truecaller.common.g.a> d;
  
  private h(f paramf, Provider<Context> paramProvider, Provider<com.truecaller.callhistory.a> paramProvider1, Provider<com.truecaller.common.g.a> paramProvider2)
  {
    a = paramf;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
  }
  
  public static h a(f paramf, Provider<Context> paramProvider, Provider<com.truecaller.callhistory.a> paramProvider1, Provider<com.truecaller.common.g.a> paramProvider2)
  {
    return new h(paramf, paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flash.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */