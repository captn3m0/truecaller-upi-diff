package com.truecaller.flash;

import android.os.Bundle;
import android.text.TextUtils;
import c.g.b.k;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.b;
import com.truecaller.analytics.bb;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.f;
import com.truecaller.common.h.am;
import com.truecaller.flashsdk.core.s;
import com.truecaller.tracking.events.l;
import com.truecaller.tracking.events.l.a;
import com.truecaller.tracking.events.m;
import com.truecaller.tracking.events.m.a;
import com.truecaller.tracking.events.n;
import com.truecaller.tracking.events.n.a;
import java.util.Iterator;
import java.util.Set;
import org.apache.a.d.d;

public final class a
  implements s
{
  private final f<ae> a;
  private final b b;
  
  public a(f<ae> paramf, b paramb)
  {
    a = paramf;
    b = paramb;
  }
  
  public final void a(String paramString, Bundle paramBundle)
  {
    k.b(paramString, "eventName");
    if (!TextUtils.isEmpty((CharSequence)paramString))
    {
      e.a locala2 = new e.a("FLASH_USED");
      locala2.a("FlashActionType", paramString);
      e.a locala1 = new e.a(paramString);
      if (paramBundle != null)
      {
        Object localObject2;
        switch (paramString.hashCode())
        {
        default: 
          break;
        case 1973848558: 
          if (!paramString.equals("ANDROID_FLASH_VIEW_PROFILE")) {
            break label996;
          }
          break;
        case 1929709946: 
          if (!paramString.equals("ANDROID_FLASH_REPLIED")) {
            break label996;
          }
          break;
        case 967132857: 
          if (!paramString.equals("ANDROID_FLASH_CLOSE")) {
            break label996;
          }
          break;
        case 392911307: 
          if ((!paramString.equals("ANDROID_FLASH_TAPPED")) || (paramBundle.getString("flash_context") == null)) {
            break label996;
          }
          localObject1 = paramBundle.getString("flash_context");
          if (localObject1 == null) {
            break label996;
          }
          switch (((String)localObject1).hashCode())
          {
          default: 
            break;
          case 1198114018: 
            if (!((String)localObject1).equals("callMeBackPopupOutApp")) {
              break;
            }
            bb.a(a, "callMeBackPopupOutApp", "flashButton");
            break;
          case 1018734170: 
            if (!((String)localObject1).equals("afterCall")) {
              break;
            }
            bb.a(a, "afterCall", "flashButton");
            break;
          case 740154499: 
            if (!((String)localObject1).equals("conversation")) {
              break;
            }
            bb.a(a, "conversation", "flashButton");
            break;
          case 628280070: 
            if (!((String)localObject1).equals("deepLink")) {
              break;
            }
            bb.a(a, "deepLink", "flashButton");
            break;
          case 595233003: 
            if (!((String)localObject1).equals("notification")) {
              break;
            }
            bb.a(a, "notification", "flashButton");
            break;
          case -674115797: 
            if (!((String)localObject1).equals("globalSearch")) {
              break;
            }
            bb.a(a, "globalSearch", "flashButton");
            break;
          case -845664406: 
            if (!((String)localObject1).equals("incomingFlash")) {
              break;
            }
            bb.a(a, "incomingFlash", "flashButton");
            break;
          case -1206196785: 
            if (!((String)localObject1).equals("flashShare")) {
              break;
            }
            bb.a(a, "flashShare", "flashButton");
            break;
          case -1522107352: 
            if (!((String)localObject1).equals("callMeBackNotification")) {
              break;
            }
            bb.a(a, "callMeBackNotification", "flashButton");
            break;
          case -1768263699: 
            if (!((String)localObject1).equals("callMeBackPopupInApp")) {
              break;
            }
            bb.a(a, "callMeBackPopupInApp", "flashButton");
            break;
          case -1880932050: 
            if (!((String)localObject1).equals("searchResults")) {
              break;
            }
            bb.a(a, "searchResults", "flashButton");
          }
          break;
        case 263288744: 
          if (!paramString.equals("ANDROID_FLASH_OPENED")) {
            break label996;
          }
          break;
        case 199987674: 
          if (!paramString.equals("ANDROID_FLASH_MISSED")) {
            break label996;
          }
          break;
        case 170215063: 
          if (!paramString.equals("ANDROID_FLASH_SENT")) {
            break label996;
          }
          localObject1 = n.b();
          localObject2 = ((n.a)localObject1).d((CharSequence)paramBundle.getString("type")).a((CharSequence)paramBundle.getString("flash_message_id")).a(paramBundle.getBoolean("flash_from_phonebook")).b((CharSequence)paramBundle.getString("flash_receiver_id")).c((CharSequence)paramBundle.getString("flash_context")).e((CharSequence)paramBundle.getString("flash_reply_id")).a(Integer.parseInt(paramBundle.getString("history_length")));
          k.a(localObject2, "flashInitiatedEvent.setC…alytics.HISTORY_LENGTH)))");
          ((n.a)localObject2).f((CharSequence)paramBundle.getString("flash_thread_id"));
          ((ae)a.a()).a((d)((n.a)localObject1).a());
          break;
        case -687160992: 
          if (!paramString.equals("ANDROID_FLASH_RECEIVED")) {
            break label996;
          }
          localObject1 = m.b();
          localObject2 = ((m.a)localObject1).d((CharSequence)paramBundle.getString("type")).a((CharSequence)paramBundle.getString("flash_message_id")).a(paramBundle.getBoolean("flash_from_phonebook")).b((CharSequence)paramBundle.getString("flash_sender_id"));
          k.a(localObject2, "flashIncomingEvent.setCo…alytics.FLASH_SENDER_ID))");
          ((m.a)localObject2).c((CharSequence)paramBundle.getString("flash_thread_id"));
          ((ae)a.a()).a((d)((m.a)localObject1).a());
          break;
        }
        if (paramString.equals("ANDROID_FLASH_BLOCK_USER"))
        {
          localObject1 = l.b();
          localObject2 = ((l.a)localObject1).d((CharSequence)paramBundle.getString("type")).a((CharSequence)paramBundle.getString("flash_message_id")).a(paramBundle.getBoolean("flash_from_phonebook")).b(paramBundle.getBoolean("flash_missed")).b((CharSequence)paramBundle.getString("flash_sender_id")).c((CharSequence)paramBundle.getString("flash_thread_id"));
          k.a(localObject2, "flashActionEvent.setCont…alytics.FLASH_THREAD_ID))");
          ((l.a)localObject2).e((CharSequence)paramBundle.getString("flash_action_name"));
          ((ae)a.a()).a((d)((l.a)localObject1).a());
        }
        label996:
        Object localObject1 = paramBundle.keySet().iterator();
        while (((Iterator)localObject1).hasNext())
        {
          localObject2 = (String)((Iterator)localObject1).next();
          if (paramBundle.get((String)localObject2) != null)
          {
            CharSequence localCharSequence = (CharSequence)localObject2;
            if ((am.a(localCharSequence, (CharSequence)"type")) || (am.a(localCharSequence, (CharSequence)"history_length")) || (am.a(localCharSequence, (CharSequence)"flash_context")) || (am.a(localCharSequence, (CharSequence)"CampaignDescription")) || (am.a(localCharSequence, (CharSequence)"FlashFromHistory")) || (am.a(localCharSequence, (CharSequence)"flash_waiting_timer")) || (am.a(localCharSequence, (CharSequence)"sentFailed")) || (am.a(localCharSequence, (CharSequence)"ANDROID_FLASH_CUSTOM_BUTTON_CLICKED")))
            {
              locala2.a((String)localObject2, paramBundle.getString((String)localObject2));
              locala1.a((String)localObject2, paramBundle.getString((String)localObject2));
            }
          }
        }
        if (k.a(paramString, "ANDROID_FLASH_SENT_FAILED"))
        {
          locala2.a("flash_thread_id", paramBundle.getString("flash_thread_id"));
          locala1.a("flash_thread_id", paramBundle.getString("flash_thread_id"));
        }
      }
      paramString = b;
      paramBundle = locala2.a();
      k.a(paramBundle, "event.build()");
      paramString.a(paramBundle);
      paramString = b;
      paramBundle = locala1.a();
      k.a(paramBundle, "eventOld.build()");
      paramString.a(paramBundle);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flash.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */