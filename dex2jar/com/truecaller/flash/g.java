package com.truecaller.flash;

import com.truecaller.analytics.ae;
import com.truecaller.analytics.b;
import com.truecaller.flashsdk.core.s;
import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d<s>
{
  private final f a;
  private final Provider<com.truecaller.androidactors.f<ae>> b;
  private final Provider<b> c;
  
  private g(f paramf, Provider<com.truecaller.androidactors.f<ae>> paramProvider, Provider<b> paramProvider1)
  {
    a = paramf;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static g a(f paramf, Provider<com.truecaller.androidactors.f<ae>> paramProvider, Provider<b> paramProvider1)
  {
    return new g(paramf, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flash.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */