package com.truecaller.flash;

import com.truecaller.flashsdk.core.b;
import dagger.a.d;
import javax.inject.Provider;

public final class l
  implements d<o>
{
  private final f a;
  private final Provider<b> b;
  
  private l(f paramf, Provider<b> paramProvider)
  {
    a = paramf;
    b = paramProvider;
  }
  
  public static l a(f paramf, Provider<b> paramProvider)
  {
    return new l(paramf, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flash.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */