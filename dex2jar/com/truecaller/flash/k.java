package com.truecaller.flash;

import com.truecaller.flashsdk.core.b;
import dagger.a.d;
import javax.inject.Provider;

public final class k
  implements d<m>
{
  private final f a;
  private final Provider<b> b;
  private final Provider<c.d.f> c;
  private final Provider<c.d.f> d;
  
  private k(f paramf, Provider<b> paramProvider, Provider<c.d.f> paramProvider1, Provider<c.d.f> paramProvider2)
  {
    a = paramf;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
  }
  
  public static k a(f paramf, Provider<b> paramProvider, Provider<c.d.f> paramProvider1, Provider<c.d.f> paramProvider2)
  {
    return new k(paramf, paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flash.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */