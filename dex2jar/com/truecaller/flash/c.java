package com.truecaller.flash;

import c.g.b.k;
import com.truecaller.common.h.aa;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.flashsdk.core.i;
import com.truecaller.log.AssertionUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;

public final class c
  implements b
{
  private final String a;
  private final i b;
  private final com.truecaller.callhistory.a c;
  private final com.truecaller.common.g.a d;
  
  @Inject
  public c(String paramString, i parami, com.truecaller.callhistory.a parama, com.truecaller.common.g.a parama1)
  {
    a = paramString;
    b = parami;
    c = parama;
    d = parama1;
  }
  
  private final List<com.truecaller.flashsdk.models.a> a(Collection<? extends Contact> paramCollection)
  {
    Object localObject1 = (Iterable)c.a.m.e((Iterable)paramCollection);
    paramCollection = (Collection)new ArrayList();
    localObject1 = ((Iterable)localObject1).iterator();
    int i;
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      i = 1;
      if (!bool) {
        break;
      }
      localObject2 = ((Iterator)localObject1).next();
      if (((Contact)localObject2).A().size() <= 0) {
        i = 0;
      }
      if (i != 0) {
        paramCollection.add(localObject2);
      }
    }
    localObject1 = (Iterable)paramCollection;
    paramCollection = (Collection)new ArrayList();
    localObject1 = ((Iterable)localObject1).iterator();
    Object localObject3;
    Object localObject4;
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = ((Iterator)localObject1).next();
      localObject3 = (Contact)localObject2;
      localObject4 = ((Contact)localObject3).A();
      k.a(localObject4, "contact.numbers");
      localObject4 = (Number)c.a.m.e((List)localObject4);
      if ((localObject4 != null) && (b.b(((Number)localObject4).a())))
      {
        localObject3 = ((Contact)localObject3).A();
        k.a(localObject3, "contact.numbers");
        localObject3 = c.a.m.d((List)localObject3);
        k.a(localObject3, "contact.numbers.first()");
        localObject3 = aa.c(((Number)localObject3).a(), a);
        k.a(localObject3, "PhoneNumberNormalizer.no…alizedNumber, countryIso)");
        if (c.n.m.d((String)localObject3) != null)
        {
          i = 1;
          break label260;
        }
      }
      i = 0;
      label260:
      if (i != 0) {
        paramCollection.add(localObject2);
      }
    }
    localObject1 = (Iterable)paramCollection;
    paramCollection = (Collection)new ArrayList(c.a.m.a((Iterable)localObject1, 10));
    localObject1 = ((Iterable)localObject1).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (Contact)((Iterator)localObject1).next();
      localObject3 = ((Contact)localObject2).s();
      localObject4 = ((Contact)localObject2).A();
      k.a(localObject4, "it.numbers");
      localObject4 = c.a.m.d((List)localObject4);
      k.a(localObject4, "it.numbers.first()");
      paramCollection.add(new com.truecaller.flashsdk.models.a((String)localObject3, aa.c(((Number)localObject4).a(), a), ((Contact)localObject2).v()));
    }
    Object localObject2 = (Iterable)paramCollection;
    paramCollection = new HashSet();
    localObject1 = new ArrayList();
    localObject2 = ((Iterable)localObject2).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject3 = ((Iterator)localObject2).next();
      if (paramCollection.add(((com.truecaller.flashsdk.models.a)localObject3).b())) {
        ((ArrayList)localObject1).add(localObject3);
      }
    }
    return (List)localObject1;
  }
  
  /* Error */
  private List<com.truecaller.flashsdk.models.a> b()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 37	com/truecaller/flash/c:c	Lcom/truecaller/callhistory/a;
    //   4: getstatic 156	com/truecaller/callhistory/FilterType:NONE	Lcom/truecaller/callhistory/FilterType;
    //   7: bipush 20
    //   9: invokestatic 162	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   12: invokeinterface 167 3 0
    //   17: invokevirtual 171	com/truecaller/androidactors/w:d	()Ljava/lang/Object;
    //   20: checkcast 77	java/util/List
    //   23: astore_2
    //   24: aload_2
    //   25: astore_1
    //   26: aload_2
    //   27: ifnonnull +10 -> 37
    //   30: getstatic 176	c/a/y:a	Lc/a/y;
    //   33: checkcast 77	java/util/List
    //   36: astore_1
    //   37: aload_1
    //   38: checkcast 44	java/lang/Iterable
    //   41: astore_2
    //   42: new 52	java/util/ArrayList
    //   45: dup
    //   46: aload_2
    //   47: bipush 10
    //   49: invokestatic 121	c/a/m:a	(Ljava/lang/Iterable;I)I
    //   52: invokespecial 124	java/util/ArrayList:<init>	(I)V
    //   55: checkcast 55	java/util/Collection
    //   58: astore_1
    //   59: aload_2
    //   60: invokeinterface 59 1 0
    //   65: astore_2
    //   66: aload_2
    //   67: invokeinterface 65 1 0
    //   72: ifeq +33 -> 105
    //   75: aload_2
    //   76: invokeinterface 69 1 0
    //   81: checkcast 178	com/truecaller/data/entity/HistoryEvent
    //   84: astore_3
    //   85: aload_3
    //   86: ldc -76
    //   88: invokestatic 89	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   91: aload_1
    //   92: aload_3
    //   93: invokevirtual 183	com/truecaller/data/entity/HistoryEvent:s	()Lcom/truecaller/data/entity/Contact;
    //   96: invokeinterface 85 2 0
    //   101: pop
    //   102: goto -36 -> 66
    //   105: aload_0
    //   106: aload_1
    //   107: checkcast 77	java/util/List
    //   110: checkcast 55	java/util/Collection
    //   113: invokespecial 185	com/truecaller/flash/c:a	(Ljava/util/Collection;)Ljava/util/List;
    //   116: astore 4
    //   118: aload_0
    //   119: getfield 37	com/truecaller/flash/c:c	Lcom/truecaller/callhistory/a;
    //   122: bipush 100
    //   124: invokeinterface 188 2 0
    //   129: invokevirtual 171	com/truecaller/androidactors/w:d	()Ljava/lang/Object;
    //   132: checkcast 190	com/truecaller/callhistory/z
    //   135: astore_1
    //   136: aconst_null
    //   137: astore_3
    //   138: aload_1
    //   139: ifnull +119 -> 258
    //   142: aload_1
    //   143: checkcast 192	android/database/Cursor
    //   146: astore 6
    //   148: aload 6
    //   150: checkcast 194	java/io/Closeable
    //   153: astore 5
    //   155: aload_3
    //   156: astore_1
    //   157: new 52	java/util/ArrayList
    //   160: dup
    //   161: invokespecial 53	java/util/ArrayList:<init>	()V
    //   164: checkcast 55	java/util/Collection
    //   167: astore 7
    //   169: aload_3
    //   170: astore_1
    //   171: aload 6
    //   173: invokeinterface 197 1 0
    //   178: ifeq +44 -> 222
    //   181: aload_3
    //   182: astore_1
    //   183: aload 6
    //   185: checkcast 190	com/truecaller/callhistory/z
    //   188: invokeinterface 200 1 0
    //   193: astore_2
    //   194: aload_2
    //   195: ifnull +303 -> 498
    //   198: aload_3
    //   199: astore_1
    //   200: aload_2
    //   201: invokevirtual 183	com/truecaller/data/entity/HistoryEvent:s	()Lcom/truecaller/data/entity/Contact;
    //   204: astore_2
    //   205: goto +3 -> 208
    //   208: aload_3
    //   209: astore_1
    //   210: aload 7
    //   212: aload_2
    //   213: invokeinterface 85 2 0
    //   218: pop
    //   219: goto -50 -> 169
    //   222: aload_3
    //   223: astore_1
    //   224: aload 7
    //   226: checkcast 77	java/util/List
    //   229: astore_2
    //   230: aload 5
    //   232: aconst_null
    //   233: invokestatic 205	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   236: aload_2
    //   237: astore_1
    //   238: goto +22 -> 260
    //   241: astore_2
    //   242: goto +8 -> 250
    //   245: astore_2
    //   246: aload_2
    //   247: astore_1
    //   248: aload_2
    //   249: athrow
    //   250: aload 5
    //   252: aload_1
    //   253: invokestatic 205	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   256: aload_2
    //   257: athrow
    //   258: aconst_null
    //   259: astore_1
    //   260: aload_1
    //   261: astore_2
    //   262: aload_1
    //   263: ifnonnull +10 -> 273
    //   266: getstatic 176	c/a/y:a	Lc/a/y;
    //   269: checkcast 77	java/util/List
    //   272: astore_2
    //   273: aload_0
    //   274: aload_2
    //   275: checkcast 55	java/util/Collection
    //   278: invokespecial 185	com/truecaller/flash/c:a	(Ljava/util/Collection;)Ljava/util/List;
    //   281: astore_1
    //   282: aload 4
    //   284: checkcast 44	java/lang/Iterable
    //   287: iconst_2
    //   288: invokestatic 208	c/a/m:d	(Ljava/lang/Iterable;I)Ljava/util/List;
    //   291: checkcast 55	java/util/Collection
    //   294: aload_1
    //   295: checkcast 44	java/lang/Iterable
    //   298: bipush 10
    //   300: invokestatic 208	c/a/m:d	(Ljava/lang/Iterable;I)Ljava/util/List;
    //   303: checkcast 44	java/lang/Iterable
    //   306: invokestatic 211	c/a/m:c	(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;
    //   309: checkcast 44	java/lang/Iterable
    //   312: astore_3
    //   313: new 141	java/util/HashSet
    //   316: dup
    //   317: invokespecial 142	java/util/HashSet:<init>	()V
    //   320: astore_1
    //   321: new 52	java/util/ArrayList
    //   324: dup
    //   325: invokespecial 53	java/util/ArrayList:<init>	()V
    //   328: astore_2
    //   329: aload_3
    //   330: invokeinterface 59 1 0
    //   335: astore_3
    //   336: aload_3
    //   337: invokeinterface 65 1 0
    //   342: ifeq +36 -> 378
    //   345: aload_3
    //   346: invokeinterface 69 1 0
    //   351: astore 4
    //   353: aload_1
    //   354: aload 4
    //   356: checkcast 133	com/truecaller/flashsdk/models/a
    //   359: invokevirtual 144	com/truecaller/flashsdk/models/a:b	()Ljava/lang/String;
    //   362: invokevirtual 145	java/util/HashSet:add	(Ljava/lang/Object;)Z
    //   365: ifeq -29 -> 336
    //   368: aload_2
    //   369: aload 4
    //   371: invokevirtual 146	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   374: pop
    //   375: goto -39 -> 336
    //   378: aload_2
    //   379: checkcast 77	java/util/List
    //   382: checkcast 44	java/lang/Iterable
    //   385: bipush 10
    //   387: invokestatic 208	c/a/m:d	(Ljava/lang/Iterable;I)Ljava/util/List;
    //   390: astore_1
    //   391: aload_1
    //   392: invokeinterface 214 1 0
    //   397: ifeq +99 -> 496
    //   400: aload_0
    //   401: getfield 39	com/truecaller/flash/c:d	Lcom/truecaller/common/g/a;
    //   404: ldc -40
    //   406: invokeinterface 221 2 0
    //   411: astore_2
    //   412: aload_2
    //   413: ifnonnull +10 -> 423
    //   416: getstatic 176	c/a/y:a	Lc/a/y;
    //   419: checkcast 77	java/util/List
    //   422: areturn
    //   423: aload_2
    //   424: ldc -33
    //   426: invokestatic 89	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   429: aload_2
    //   430: aload_0
    //   431: getfield 33	com/truecaller/flash/c:a	Ljava/lang/String;
    //   434: invokestatic 111	com/truecaller/common/h/aa:c	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   437: astore_2
    //   438: aload_2
    //   439: ldc 113
    //   441: invokestatic 89	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   444: aload_2
    //   445: invokestatic 118	c/n/m:d	(Ljava/lang/String;)Ljava/lang/Long;
    //   448: ifnull +48 -> 496
    //   451: new 133	com/truecaller/flashsdk/models/a
    //   454: dup
    //   455: ldc -31
    //   457: iconst_1
    //   458: anewarray 227	java/lang/CharSequence
    //   461: dup
    //   462: iconst_0
    //   463: aload_0
    //   464: getfield 39	com/truecaller/flash/c:d	Lcom/truecaller/common/g/a;
    //   467: invokestatic 232	com/truecaller/profile/c:b	(Lcom/truecaller/common/g/a;)Ljava/lang/String;
    //   470: checkcast 227	java/lang/CharSequence
    //   473: aastore
    //   474: invokestatic 237	com/truecaller/common/h/am:a	(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/String;
    //   477: aload_2
    //   478: aload_0
    //   479: getfield 39	com/truecaller/flash/c:d	Lcom/truecaller/common/g/a;
    //   482: ldc -17
    //   484: invokeinterface 221 2 0
    //   489: invokespecial 139	com/truecaller/flashsdk/models/a:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   492: invokestatic 242	c/a/m:a	(Ljava/lang/Object;)Ljava/util/List;
    //   495: areturn
    //   496: aload_1
    //   497: areturn
    //   498: aconst_null
    //   499: astore_2
    //   500: goto -292 -> 208
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	503	0	this	c
    //   25	472	1	localObject1	Object
    //   23	214	2	localObject2	Object
    //   241	1	2	localObject3	Object
    //   245	12	2	localThrowable	Throwable
    //   261	239	2	localObject4	Object
    //   84	262	3	localObject5	Object
    //   116	254	4	localObject6	Object
    //   153	98	5	localCloseable	java.io.Closeable
    //   146	38	6	localCursor	android.database.Cursor
    //   167	58	7	localCollection	Collection
    // Exception table:
    //   from	to	target	type
    //   157	169	241	finally
    //   171	181	241	finally
    //   183	194	241	finally
    //   200	205	241	finally
    //   210	219	241	finally
    //   224	230	241	finally
    //   248	250	241	finally
    //   157	169	245	java/lang/Throwable
    //   171	181	245	java/lang/Throwable
    //   183	194	245	java/lang/Throwable
    //   200	205	245	java/lang/Throwable
    //   210	219	245	java/lang/Throwable
    //   224	230	245	java/lang/Throwable
  }
  
  public final List<com.truecaller.flashsdk.models.a> a()
  {
    AssertionUtil.notOnMainThread(new String[] { "Please run off the main thread!" });
    return b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flash.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */