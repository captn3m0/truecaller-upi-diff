package com.truecaller.flash;

import com.truecaller.callhistory.a;
import javax.inject.Provider;

public final class i
  implements dagger.a.d<d>
{
  private final f a;
  private final Provider<a> b;
  
  private i(f paramf, Provider<a> paramProvider)
  {
    a = paramf;
    b = paramProvider;
  }
  
  public static i a(f paramf, Provider<a> paramProvider)
  {
    return new i(paramf, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flash.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */