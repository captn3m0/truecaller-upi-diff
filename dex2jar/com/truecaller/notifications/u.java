package com.truecaller.notifications;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class u
  implements d<l>
{
  private final Provider<Context> a;
  
  private u(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static u a(Provider<Context> paramProvider)
  {
    return new u(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.u
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */