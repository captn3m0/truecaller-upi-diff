package com.truecaller.notifications;

import android.content.Context;
import com.truecaller.messaging.notifications.e;
import dagger.a.d;
import javax.inject.Provider;

public final class w
  implements d<e>
{
  private final Provider<Context> a;
  private final Provider<l> b;
  
  private w(Provider<Context> paramProvider, Provider<l> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static w a(Provider<Context> paramProvider, Provider<l> paramProvider1)
  {
    return new w(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.w
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */