package com.truecaller.notifications;

import android.app.Notification;
import android.content.Context;
import android.service.notification.StatusBarNotification;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.common.h.t;
import com.truecaller.data.access.i;
import java.util.Stack;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.bn;

public final class al
  extends h
{
  final Stack<ak> a;
  final Context b;
  final i c;
  final com.truecaller.data.access.c d;
  final com.truecaller.calling.e.c e;
  final t f;
  private bn g;
  private final com.truecaller.calling.e.e h;
  private final f i;
  
  @Inject
  public al(Context paramContext, com.truecaller.calling.e.e parame, i parami, com.truecaller.data.access.c paramc, com.truecaller.calling.e.c paramc1, @Named("Async") f paramf, t paramt)
  {
    b = paramContext;
    h = parame;
    c = parami;
    d = paramc;
    e = paramc1;
    i = paramf;
    f = paramt;
    a = new Stack();
  }
  
  private final boolean c(StatusBarNotification paramStatusBarNotification)
  {
    if (!h.b()) {
      return false;
    }
    if (!paramStatusBarNotification.isClearable())
    {
      if ((k.a("com.whatsapp", paramStatusBarNotification.getPackageName()) ^ true)) {
        return false;
      }
      return !(k.a(getNotificationcategory, "call") ^ true);
    }
    return false;
  }
  
  public final void a(StatusBarNotification paramStatusBarNotification)
  {
    k.b(paramStatusBarNotification, "statusBarNotification");
    if (!c(paramStatusBarNotification)) {
      return;
    }
    bn localbn = g;
    if (localbn != null) {
      localbn.n();
    }
    paramStatusBarNotification = am.a(paramStatusBarNotification, b);
    a.push(paramStatusBarNotification);
  }
  
  public final void b(StatusBarNotification paramStatusBarNotification)
  {
    k.b(paramStatusBarNotification, "statusBarNotification");
    if (!c(paramStatusBarNotification)) {
      return;
    }
    bn localbn = g;
    if (localbn != null) {
      localbn.n();
    }
    g = kotlinx.coroutines.e.b((ag)bg.a, i, (m)new al.a(this, paramStatusBarNotification, null), 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.al
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */