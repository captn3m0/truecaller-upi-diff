package com.truecaller.notifications;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.z.c;
import android.support.v4.app.z.d;
import android.support.v4.app.z.g;
import android.support.v4.content.b;
import c.g.b.k;
import c.u;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.notificationchannels.e;
import com.truecaller.ui.TruecallerInit;
import javax.inject.Inject;

public final class ab
  implements aa
{
  private final a a;
  
  @Inject
  public ab(a parama)
  {
    a = parama;
  }
  
  public final void a(Context paramContext, int paramInt1, int paramInt2, String paramString)
  {
    k.b(paramContext, "context");
    k.b(paramString, "type");
    Intent localIntent = new Intent(paramContext, TruecallerInit.class);
    localIntent.addFlags(268468224);
    localIntent.putExtra("EXTRA_REG_NUDGE", paramString);
    paramString = paramContext.getApplicationContext();
    if (paramString != null)
    {
      paramString = new z.d(paramContext, ((bk)paramString).a().aC().a()).a((CharSequence)paramContext.getString(paramInt1)).b((CharSequence)paramContext.getString(paramInt2)).a((z.g)new z.c().b((CharSequence)paramContext.getString(paramInt2))).f(b.c(paramContext, 2131100594)).c(-1).a(2131234787).a(PendingIntent.getActivity(paramContext, 0, localIntent, 0)).e();
      paramContext = a;
      paramString = paramString.h();
      k.a(paramString, "builder.build()");
      paramContext.a(2131362836, paramString, "notificationRegistrationNudge");
      return;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.GraphHolder");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.ab
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */