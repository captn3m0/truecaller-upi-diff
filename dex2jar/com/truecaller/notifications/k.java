package com.truecaller.notifications;

import com.truecaller.utils.l;
import dagger.a.d;
import javax.inject.Provider;

public final class k
  implements d<j>
{
  private final Provider<l> a;
  
  private k(Provider<l> paramProvider)
  {
    a = paramProvider;
  }
  
  public static k a(Provider<l> paramProvider)
  {
    return new k(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */