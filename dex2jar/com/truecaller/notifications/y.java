package com.truecaller.notifications;

import c.a.al;
import c.u;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.f;
import com.truecaller.log.AssertionUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Inject;

public final class y
{
  public static final y.a a = new y.a((byte)0);
  private final e b;
  
  @Inject
  public y(e parame)
  {
    b = parame;
  }
  
  public final y.b a(String paramString)
  {
    c.g.b.k.b(paramString, "messageBody");
    Object localObject1 = c.n.m.c((CharSequence)paramString, new String[] { "." }, false, 6);
    int i;
    if (!((List)localObject1).isEmpty())
    {
      localObject2 = ((List)localObject1).listIterator(((List)localObject1).size());
      while (((ListIterator)localObject2).hasPrevious())
      {
        if (((CharSequence)((ListIterator)localObject2).previous()).length() == 0) {
          i = 1;
        } else {
          i = 0;
        }
        if (i == 0)
        {
          localObject1 = c.a.m.d((Iterable)localObject1, ((ListIterator)localObject2).nextIndex() + 1);
          break label125;
        }
      }
    }
    localObject1 = (List)c.a.y.a;
    label125:
    Object localObject3 = new ArrayList();
    Object localObject2 = ((Iterable)localObject1).iterator();
    Object localObject4;
    while (((Iterator)localObject2).hasNext())
    {
      localObject4 = (CharSequence)((Iterator)localObject2).next();
      localObject4 = new c.n.k("\\s").a((CharSequence)localObject4, 0);
      c.g.b.k.b(localObject4, "receiver$0");
      ((ArrayList)localObject3).add(c.a.m.a((Iterable)new al((List)localObject4), (CharSequence)" ", null, null, 0, null, null, 62));
    }
    localObject2 = new ArrayList();
    ((ArrayList)localObject2).addAll((Collection)localObject1);
    ((ArrayList)localObject2).addAll((Collection)localObject3);
    localObject1 = (String)org.c.a.a.a.k.d((CharSequence)b.aj().e(), (CharSequence)"(?:(from)?.*\\b(?:ending|PAYTMWALLETLOADING|PAYUZOMATOMEDIAPVTLT|one.time.password|your.code|slack|Billdk|netbanking).*\\b.*(is|code):?\\s+)\\b(\\d{3,8})/.*?(?:verification|security|confirmation|validation).(?:code|otp).*(?:is|:).*?(\\b\\d{3,8}\\d)/(?:.*otp.*?is\\s)(?=.*?\\btr?xn\\b.*?)\\b(\\d{3,8})\\b/(?:otp is\\s)\\b(\\d{3,8})\\b/(?=.*?\\botp\\b.*?)(?:[\\s.,:\\-\\[]|^)(\\d{3,8})(?:[\\s.,\\]]|$)/(\\b[\\d-]{3,6}\\d).*?(?:verification|security|validation|twitter.login|login|password.reset).code/.*?(?:verification|security|confirmation|validation).code.*?(\\b\\d{3,8}\\d)/(?=.*?\\b(?:one.time.*?password|password.time.one|password.one-time|facebook)\\b.*?)\\b\\d{3,8}\\b/(?=.*?\\b(?:code.telegram|code..?imo|code.auto-verify|verify.*?grofers|key.v-authentication|banking.*?mpin|to.proceed.with.your.transaction|account.*?verify)\\b.*?)\\b\\d{3,8}\\b/(?:whatsapp|snapchat|uber|eventshigh|truecaller|signal.verification|discord.verification).code.*?([\\d-\\s]{3,8})/(?:enter|use).*?(?:code)*.*?(\\d{3,8}).(?:(?:to|as).*?(?:activate|allow|verify|confirm|sign.in))/to.*?enter.pin..\\b(\\d{3,8}\\b)/(?:otp|code|tiptapp.pin|pin|transaction.password|URN)[-:]?\\s?(is)?\\s?<?(\\b\\d{4,8}\\b)>?(?: ShareChat.*)?$/([\\d-]{3,8})\\b\\n?.*code.*(?:Microsoft).(?:verification)");
    c.g.b.k.a(localObject1, "regexString");
    localObject3 = (CharSequence)localObject1;
    AssertionUtil.isFalse(c.n.m.a((CharSequence)localObject3), new String[] { "Regex string is empty or null" });
    if (c.n.m.a((CharSequence)localObject3)) {
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      localObject1 = (CharSequence)localObject1;
      localObject1 = new c.n.k("/").a((CharSequence)localObject1, 0);
      if (!((List)localObject1).isEmpty())
      {
        localObject3 = ((List)localObject1).listIterator(((List)localObject1).size());
        while (((ListIterator)localObject3).hasPrevious())
        {
          if (((CharSequence)((ListIterator)localObject3).previous()).length() == 0) {
            i = 1;
          } else {
            i = 0;
          }
          if (i == 0)
          {
            localObject1 = c.a.m.d((Iterable)localObject1, ((ListIterator)localObject3).nextIndex() + 1);
            break label465;
          }
        }
      }
      localObject1 = (List)c.a.y.a;
      label465:
      localObject1 = (Collection)localObject1;
      if (localObject1 != null)
      {
        localObject1 = ((Collection)localObject1).toArray(new String[0]);
        if (localObject1 != null)
        {
          localObject1 = (String[])localObject1;
          localObject2 = ((Iterable)localObject2).iterator();
          while (((Iterator)localObject2).hasNext())
          {
            localObject3 = (String)((Iterator)localObject2).next();
            int j = localObject1.length;
            i = 0;
            while (i < j)
            {
              localObject4 = localObject1[i];
              try
              {
                Object localObject5 = Pattern.compile("(?i)".concat(String.valueOf(localObject4))).matcher((CharSequence)localObject3);
                if (((Matcher)localObject5).find())
                {
                  localObject5 = ((Matcher)localObject5).group(((Matcher)localObject5).groupCount());
                  c.g.b.k.a(localObject5, "group");
                  localObject5 = c.n.m.b((String)localObject5, "-", "");
                  if (localObject5 != null)
                  {
                    localObject5 = c.n.m.b(c.n.m.b((CharSequence)localObject5).toString(), " ", "");
                    if (org.c.a.a.a.k.g((CharSequence)localObject5)) {
                      return new y.b(paramString, (String)localObject4, (String)localObject5);
                    }
                  }
                  else
                  {
                    throw new u("null cannot be cast to non-null type kotlin.CharSequence");
                  }
                }
              }
              catch (Exception localException)
              {
                AssertionUtil.reportThrowableButNeverCrash((Throwable)localException);
                i += 1;
              }
            }
          }
        }
        throw new u("null cannot be cast to non-null type kotlin.Array<T>");
      }
      throw new u("null cannot be cast to non-null type java.util.Collection<T>");
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.y
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */