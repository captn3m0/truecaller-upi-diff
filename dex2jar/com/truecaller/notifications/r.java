package com.truecaller.notifications;

import com.truecaller.notificationchannels.j;
import com.truecaller.notificationchannels.n;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class r
  implements d<j>
{
  private final Provider<n> a;
  
  private r(Provider<n> paramProvider)
  {
    a = paramProvider;
  }
  
  public static j a(n paramn)
  {
    return (j)g.a(paramn.e(), "Cannot return null from a non-@Nullable @Provides method");
  }
  
  public static r a(Provider<n> paramProvider)
  {
    return new r(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.r
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */