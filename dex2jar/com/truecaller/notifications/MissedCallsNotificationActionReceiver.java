package com.truecaller.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import c.n.m;
import com.truecaller.TrueApp;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.bb;
import com.truecaller.androidactors.w;
import com.truecaller.bp;
import com.truecaller.callerid.ai;
import com.truecaller.calling.ar;
import com.truecaller.common.h.aa;
import com.truecaller.flashsdk.core.b;
import com.truecaller.log.AssertionUtil;
import com.truecaller.multisim.h;
import com.truecaller.tracking.events.f.a;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.util.al;
import com.truecaller.util.bl;
import org.apache.a.d.d;

public final class MissedCallsNotificationActionReceiver
  extends BroadcastReceiver
{
  public static final MissedCallsNotificationActionReceiver.a a = new MissedCallsNotificationActionReceiver.a((byte)0);
  private Context b;
  private bp c;
  private Intent d;
  
  private final void a(boolean paramBoolean)
  {
    Object localObject = c;
    if (localObject == null) {
      c.g.b.k.a("graph");
    }
    localObject = ((bp)localObject).ad();
    c.g.b.k.a(localObject, "graph.callHistoryManager()");
    Intent localIntent = d;
    if (localIntent == null) {
      c.g.b.k.a("intent");
    }
    long l = localIntent.getLongExtra("lastTimestamp", Long.MAX_VALUE);
    ((com.truecaller.callhistory.a)((com.truecaller.androidactors.f)localObject).a()).a(l).a((com.truecaller.androidactors.ac)new MissedCallsNotificationActionReceiver.b(this));
    ((com.truecaller.callhistory.a)((com.truecaller.androidactors.f)localObject).a()).b(l);
    if (!paramBoolean)
    {
      localObject = b;
      if (localObject == null) {
        c.g.b.k.a("context");
      }
      bl.a((Context)localObject);
    }
  }
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramContext == null) {
      return;
    }
    b = paramContext;
    if (paramIntent == null) {
      return;
    }
    d = paramIntent;
    Object localObject = paramContext.getApplicationContext();
    boolean bool = localObject instanceof TrueApp;
    Context localContext = null;
    if (!bool) {
      localObject = null;
    }
    localObject = (TrueApp)localObject;
    if (localObject != null)
    {
      localObject = ((TrueApp)localObject).a();
      if (localObject == null) {
        return;
      }
      c = ((bp)localObject);
      paramIntent = paramIntent.getAction();
      if (paramIntent == null) {
        return;
      }
      localObject = c;
      if (localObject == null) {
        c.g.b.k.a("graph");
      }
      localObject = ((bp)localObject).t();
      c.g.b.k.a(localObject, "graph.deviceManager()");
      if (((al)localObject).b())
      {
        localObject = c;
        if (localObject == null) {
          c.g.b.k.a("graph");
        }
        if (!((bp)localObject).T().c()) {
          return;
        }
      }
      long l;
      switch (paramIntent.hashCode())
      {
      default: 
        break;
      case 2097706097: 
        if (paramIntent.equals("com.truecaller.SMS"))
        {
          paramIntent = c;
          if (paramIntent == null) {
            c.g.b.k.a("graph");
          }
          paramIntent = paramIntent.ad();
          c.g.b.k.a(paramIntent, "graph.callHistoryManager()");
          localObject = d;
          if (localObject == null) {
            c.g.b.k.a("intent");
          }
          l = ((Intent)localObject).getLongExtra("callLogId", -1L);
          if (l != -1L) {
            ((com.truecaller.callhistory.a)paramIntent.a()).c(l);
          }
          paramIntent = b;
          if (paramIntent == null) {
            c.g.b.k.a("context");
          }
          paramIntent.sendBroadcast(new Intent("android.intent.action.CLOSE_SYSTEM_DIALOGS"));
          paramIntent = d;
          if (paramIntent == null) {
            c.g.b.k.a("intent");
          }
          paramIntent = new Intent("android.intent.action.SENDTO", Uri.fromParts("smsto", paramIntent.getStringExtra("number"), null));
          paramIntent.setFlags(268435456);
          localObject = b;
          if (localObject == null) {
            c.g.b.k.a("context");
          }
          ((Context)localObject).startActivity(paramIntent);
        }
        break;
      case 1543847176: 
        if (paramIntent.equals("com.truecaller.FLASH"))
        {
          paramIntent = d;
          if (paramIntent == null) {
            c.g.b.k.a("intent");
          }
          paramIntent = paramIntent.getExtras();
          l = paramIntent.getLong("callLogId", -1L);
          if (l != -1L)
          {
            localObject = c;
            if (localObject == null) {
              c.g.b.k.a("graph");
            }
            ((com.truecaller.callhistory.a)((bp)localObject).ad().a()).c(l);
          }
          localObject = b;
          if (localObject == null) {
            c.g.b.k.a("context");
          }
          ((Context)localObject).sendBroadcast(new Intent("android.intent.action.CLOSE_SYSTEM_DIALOGS"));
          localObject = paramIntent.getString("number");
          paramIntent = paramIntent.getString("name", (String)localObject);
          if (!org.c.a.a.a.k.b((CharSequence)localObject))
          {
            if (localObject == null) {
              c.g.b.k.a();
            }
            l = Long.parseLong(m.a((String)localObject, "+", ""));
            localObject = com.truecaller.flashsdk.core.c.a();
            localContext = b;
            if (localContext == null) {
              c.g.b.k.a("context");
            }
            ((b)localObject).a(localContext, l, paramIntent, "notification");
          }
        }
        break;
      case 603891238: 
        if (paramIntent.equals("com.truecaller.CALL"))
        {
          paramIntent = c;
          if (paramIntent == null) {
            c.g.b.k.a("graph");
          }
          h localh = paramIntent.U();
          c.g.b.k.a(localh, "graph.multiSimManager()");
          paramIntent = d;
          if (paramIntent == null) {
            c.g.b.k.a("intent");
          }
          l = paramIntent.getLongExtra("callLogId", -1L);
          if (l != -1L)
          {
            paramIntent = c;
            if (paramIntent == null) {
              c.g.b.k.a("graph");
            }
            ((com.truecaller.callhistory.a)paramIntent.ad().a()).c(l);
          }
          paramIntent = b;
          if (paramIntent == null) {
            c.g.b.k.a("context");
          }
          paramIntent.sendBroadcast(new Intent("android.intent.action.CLOSE_SYSTEM_DIALOGS"));
          paramIntent = d;
          if (paramIntent == null) {
            c.g.b.k.a("intent");
          }
          String str = paramIntent.getStringExtra("number");
          if (localh.j())
          {
            paramIntent = c;
            if (paramIntent == null) {
              c.g.b.k.a("graph");
            }
            localObject = paramIntent.bv().e();
            paramIntent = localContext;
            if ((c.g.b.k.a(localObject, "-1") ^ true)) {
              paramIntent = (Intent)localObject;
            }
            if (paramIntent != null)
            {
              localObject = d;
              if (localObject == null) {
                c.g.b.k.a("intent");
              }
              localh.a((Intent)localObject, paramIntent);
            }
          }
          paramIntent = c;
          if (paramIntent == null) {
            c.g.b.k.a("graph");
          }
          paramIntent = paramIntent.D();
          c.g.b.k.a(paramIntent, "graph.callingSettings()");
          paramIntent.a("key_last_call_origin", "missedCallNotification");
          paramIntent.b("key_temp_latest_call_made_with_tc", true);
          paramIntent.b("lastCallMadeWithTcTime", System.currentTimeMillis());
          paramIntent = b;
          if (paramIntent == null) {
            c.g.b.k.a("context");
          }
          paramIntent = paramIntent.getPackageManager();
          localObject = b;
          if (localObject == null) {
            c.g.b.k.a("context");
          }
          if (paramIntent.checkPermission("android.permission.CALL_PRIVILEGED", ((Context)localObject).getPackageName()) == 0) {
            paramIntent = "android.intent.action.CALL_PRIVILEGED";
          } else {
            paramIntent = "android.intent.action.CALL";
          }
          c.g.b.k.a(str, "number");
          paramIntent = new Intent(paramIntent, com.truecaller.utils.extensions.r.a(str)).setFlags(268435456);
          localObject = b;
          if (localObject == null) {
            c.g.b.k.a("context");
          }
          ((Context)localObject).startActivity(paramIntent);
          paramIntent = b;
          if (paramIntent == null) {
            c.g.b.k.a("context");
          }
          paramIntent = aa.c(str, com.truecaller.common.h.k.f(paramIntent));
          c.g.b.k.a(paramIntent, "PhoneNumberNormalizer.no…tPlus(number, countryIso)");
          try
          {
            localObject = c;
            if (localObject == null) {
              c.g.b.k.a("graph");
            }
            ((ae)((bp)localObject).f().a()).a((d)com.truecaller.tracking.events.f.b().b((CharSequence)"notification").a((CharSequence)paramIntent).c((CharSequence)ai.a()).a());
          }
          catch (org.apache.a.a paramIntent)
          {
            AssertionUtil.reportThrowableButNeverCrash((Throwable)paramIntent);
          }
        }
        break;
      case 112535124: 
        if (paramIntent.equals("com.truecaller.OPEN_APP"))
        {
          paramIntent = b;
          if (paramIntent == null) {
            c.g.b.k.a("context");
          }
          paramIntent = TruecallerInit.a(paramIntent, "calls", "notificationCalls");
          c.g.b.k.a(paramIntent, "intent");
          paramIntent.setAction("android.intent.action.MAIN");
          bb.a(paramIntent, "notification", "openApp");
          localObject = paramIntent.getExtras();
          if (localObject != null) {
            paramIntent.putExtras((Bundle)localObject);
          }
          localObject = b;
          if (localObject == null) {
            c.g.b.k.a("context");
          }
          ((Context)localObject).startActivity(paramIntent);
        }
        break;
      case -152353365: 
        if (paramIntent.equals("com.truecaller.CLEAR_MISSED_CALLS")) {
          a(false);
        }
        break;
      case -502740451: 
        if (paramIntent.equals("com.truecaller.CLEAR_ALTERNATIVE_MISSED_CALLS")) {
          a(true);
        }
        break;
      }
      paramContext = android.support.v4.app.ac.a(paramContext);
      c.g.b.k.a(paramContext, "NotificationManagerCompat.from(context)");
      paramContext.a("missedCall", 12345);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.MissedCallsNotificationActionReceiver
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */