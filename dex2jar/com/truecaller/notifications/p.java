package com.truecaller.notifications;

import com.truecaller.notificationchannels.b;
import com.truecaller.notificationchannels.n;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class p
  implements d<b>
{
  private final Provider<n> a;
  
  private p(Provider<n> paramProvider)
  {
    a = paramProvider;
  }
  
  public static b a(n paramn)
  {
    return (b)g.a(paramn.d(), "Cannot return null from a non-@Nullable @Provides method");
  }
  
  public static p a(Provider<n> paramProvider)
  {
    return new p(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */