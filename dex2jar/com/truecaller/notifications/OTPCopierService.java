package com.truecaller.notifications;

import android.app.Application;
import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;
import c.g.b.k;
import c.u;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.f;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.notifications.NotificationIdentifier;
import com.truecaller.utils.n;
import javax.inject.Inject;

public final class OTPCopierService
  extends Service
{
  @Inject
  public n a;
  @Inject
  public a b;
  @Inject
  public b c;
  @Inject
  public f<t> d;
  @Inject
  public ClipboardManager e;
  
  private final void a(String paramString)
  {
    b localb = c;
    if (localb == null) {
      k.a("analytics");
    }
    paramString = new e.a("ViewAction").a("Action", paramString).a();
    k.a(paramString, "AnalyticsEvent.Builder(A…\n                .build()");
    localb.b(paramString);
  }
  
  public final IBinder onBind(Intent paramIntent)
  {
    k.b(paramIntent, "intent");
    return null;
  }
  
  public final void onCreate()
  {
    super.onCreate();
    Application localApplication = getApplication();
    if (localApplication != null)
    {
      ((bk)localApplication).a().a(this);
      return;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.GraphHolder");
  }
  
  public final int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    k.b(paramIntent, "intent");
    String str = paramIntent.getAction();
    AssertionUtil.isNotNull(str, new String[] { "OTPCopierService action should not be null" });
    paramInt1 = paramIntent.getIntExtra("OTP_NOTIFICATION_ID", -1);
    Object localObject1;
    if (paramInt1 != -1)
    {
      localObject1 = new NotificationIdentifier(paramInt1, 0, 4);
    }
    else
    {
      localObject1 = paramIntent.getParcelableExtra("OTP_NOTIFICATION_ID");
      k.a(localObject1, "intent.getParcelableExtra(KEY_OTP_NOTIFICATION_ID)");
      localObject1 = (NotificationIdentifier)localObject1;
    }
    long l = paramIntent.getLongExtra("MESSAGE_ID", -1L);
    Object localObject2;
    if (l != -1L)
    {
      localObject2 = d;
      if (localObject2 == null) {
        k.a("messageStorageRef");
      }
      ((t)((f)localObject2).a()).c(new long[] { l });
    }
    AssertionUtil.isNotNull(localObject1, new String[0]);
    if (b != null)
    {
      localObject2 = b;
      if (localObject2 == null) {
        k.a("analyticsNotificationManager");
      }
      ((a)localObject2).a(b, a);
    }
    else
    {
      localObject2 = b;
      if (localObject2 == null) {
        k.a("analyticsNotificationManager");
      }
      ((a)localObject2).a(a);
    }
    if (str != null)
    {
      paramInt1 = str.hashCode();
      if (paramInt1 != -648928470)
      {
        if (paramInt1 != 299469613)
        {
          if ((paramInt1 == 2031677783) && (str.equals("ACTION_MARK_MESSAGE_READ"))) {
            a("otpMarkedReadFromNotif");
          }
        }
        else if (str.equals("ACTION_DISMISS_OTP")) {
          a("otpDismissedFromNotif");
        }
      }
      else if (str.equals("ACTION_COPY_OTP"))
      {
        sendBroadcast(new Intent("android.intent.action.CLOSE_SYSTEM_DIALOGS"));
        paramIntent = paramIntent.getStringExtra("OTP");
        paramIntent = ClipData.newPlainText((CharSequence)"com.truecaller.OTP", (CharSequence)paramIntent);
        localObject1 = e;
        if (localObject1 != null)
        {
          if (localObject1 != null) {
            ((ClipboardManager)localObject1).setPrimaryClip(paramIntent);
          }
          paramIntent = (Context)this;
          localObject1 = a;
          if (localObject1 == null) {
            k.a("resourceProvider");
          }
          Toast.makeText(paramIntent, (CharSequence)((n)localObject1).a(2131888469, new Object[0]), 0).show();
          a("otpCopiedFromNotif");
        }
        else
        {
          AssertionUtil.report(new String[] { "Clipboard manager is null." });
        }
      }
    }
    return 2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.OTPCopierService
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */