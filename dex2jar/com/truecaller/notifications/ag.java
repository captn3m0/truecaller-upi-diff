package com.truecaller.notifications;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class ag
  implements d<af>
{
  private final Provider<Context> a;
  
  private ag(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static ag a(Provider<Context> paramProvider)
  {
    return new ag(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.ag
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */