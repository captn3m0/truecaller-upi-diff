package com.truecaller.notifications;

import android.net.Uri;
import c.g.b.k;
import com.google.firebase.perf.network.FirebasePerfOkHttpClient;
import com.truecaller.common.h.am;
import com.truecaller.old.data.access.Settings;
import com.truecaller.profile.c;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.inject.Inject;
import okhttp3.ab.a;
import okhttp3.ad;
import okhttp3.ae;
import okhttp3.y;

public final class ai
  implements ah
{
  private final String a;
  private final String b;
  private final String c;
  private final com.truecaller.common.g.a d;
  
  @Inject
  public ai(com.truecaller.common.g.a parama)
  {
    d = parama;
    a = "profile_name";
    b = "profile_country";
    c = "truecaller";
  }
  
  public final String a(String paramString)
  {
    k.b(paramString, "html");
    Object localObject = (Map)new LinkedHashMap();
    ((Map)localObject).put(a, c.b(d));
    ((Map)localObject).put(b, d.a("profileCountryIso"));
    localObject = new org.c.a.a.a.c.d((Map)localObject);
    if (paramString == null)
    {
      paramString = null;
    }
    else
    {
      org.c.a.a.a.c.a locala = new org.c.a.a.a.c.a(paramString);
      if (((org.c.a.a.a.c.d)localObject).a(locala, paramString.length())) {
        paramString = locala.toString();
      }
    }
    k.a(paramString, "htmlSubstitute.replace(html)");
    return paramString;
  }
  
  public final boolean a(Uri paramUri)
  {
    k.b(paramUri, "url");
    return k.a(c, paramUri.getScheme());
  }
  
  public final String b(String paramString)
  {
    k.b(paramString, "url");
    try
    {
      y localy = com.truecaller.common.network.util.d.a();
      String str = Settings.b("language");
      k.a(str, "Settings.get(Settings.LANGUAGE_ISO)");
      StringBuilder localStringBuilder = new StringBuilder("en;q=0.5");
      if (!am.b((CharSequence)str))
      {
        localStringBuilder.append(",");
        localStringBuilder.append(str);
      }
      paramString = FirebasePerfOkHttpClient.execute(localy.a(new ab.a().a(paramString).b("Accept-Language", localStringBuilder.toString()).a())).d();
      if (paramString != null)
      {
        paramString = paramString.g();
        return paramString;
      }
      return null;
    }
    catch (IOException paramString) {}
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.ai
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */