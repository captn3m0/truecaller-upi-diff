package com.truecaller.notifications;

import dagger.a.d;
import javax.inject.Provider;

public final class ac
  implements d<ab>
{
  private final Provider<a> a;
  
  private ac(Provider<a> paramProvider)
  {
    a = paramProvider;
  }
  
  public static ac a(Provider<a> paramProvider)
  {
    return new ac(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.ac
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */