package com.truecaller.notifications;

import android.content.Context;
import com.truecaller.notificationchannels.n;
import com.truecaller.util.bv;
import dagger.a.d;
import javax.inject.Provider;

public final class s
  implements d<n>
{
  private final Provider<Context> a;
  private final Provider<bv> b;
  
  private s(Provider<Context> paramProvider, Provider<bv> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static s a(Provider<Context> paramProvider, Provider<bv> paramProvider1)
  {
    return new s(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.s
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */