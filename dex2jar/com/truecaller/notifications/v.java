package com.truecaller.notifications;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import com.truecaller.messaging.notifications.a;
import dagger.a.d;
import javax.inject.Provider;

public final class v
  implements d<f<a>>
{
  private final Provider<i> a;
  private final Provider<a> b;
  
  private v(Provider<i> paramProvider, Provider<a> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static v a(Provider<i> paramProvider, Provider<a> paramProvider1)
  {
    return new v(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.v
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */