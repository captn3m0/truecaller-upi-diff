package com.truecaller.notifications;

import android.content.Context;
import com.truecaller.common.h.t;
import com.truecaller.util.al;
import dagger.a.d;
import javax.inject.Provider;

public final class f
  implements d<e>
{
  private final Provider<Context> a;
  private final Provider<t> b;
  private final Provider<al> c;
  private final Provider<com.truecaller.notificationchannels.e> d;
  private final Provider<a> e;
  
  public static e a(Context paramContext, t paramt, al paramal, com.truecaller.notificationchannels.e parame, a parama)
  {
    return new e(paramContext, paramt, paramal, parame, parama);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */