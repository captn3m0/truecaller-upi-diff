package com.truecaller.notifications;

import android.content.Context;
import android.support.v4.app.ac;
import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d<b>
{
  private final Provider<Context> a;
  private final Provider<ac> b;
  private final Provider<com.truecaller.analytics.b> c;
  
  private c(Provider<Context> paramProvider, Provider<ac> paramProvider1, Provider<com.truecaller.analytics.b> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static c a(Provider<Context> paramProvider, Provider<ac> paramProvider1, Provider<com.truecaller.analytics.b> paramProvider2)
  {
    return new c(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */