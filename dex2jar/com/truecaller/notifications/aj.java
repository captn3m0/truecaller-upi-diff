package com.truecaller.notifications;

import com.truecaller.common.g.a;
import dagger.a.d;
import javax.inject.Provider;

public final class aj
  implements d<ai>
{
  private final Provider<a> a;
  
  private aj(Provider<a> paramProvider)
  {
    a = paramProvider;
  }
  
  public static aj a(Provider<a> paramProvider)
  {
    return new aj(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.aj
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */