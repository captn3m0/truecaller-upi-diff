package com.truecaller.notifications;

import com.truecaller.androidactors.i;
import com.truecaller.androidactors.k;
import dagger.a.d;
import javax.inject.Provider;

public final class n
  implements d<i>
{
  private final Provider<k> a;
  
  private n(Provider<k> paramProvider)
  {
    a = paramProvider;
  }
  
  public static n a(Provider<k> paramProvider)
  {
    return new n(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */