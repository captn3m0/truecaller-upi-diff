package com.truecaller.notifications;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import javax.inject.Inject;

public final class g
{
  private final com.truecaller.utils.d a;
  private Handler b;
  
  @Inject
  public g(com.truecaller.utils.d paramd)
  {
    a = paramd;
    b = new Handler();
  }
  
  @SuppressLint({"InlinedApi"})
  public final boolean a(Context paramContext, int paramInt)
  {
    Intent localIntent;
    if (a.h() >= 22) {
      localIntent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
    } else {
      localIntent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
    }
    try
    {
      paramContext.startActivity(localIntent);
      b.postDelayed(new -..Lambda.g.fY73_hTVk42V30FUs7CHe_60z_Y(paramInt), 500L);
      return true;
    }
    catch (ActivityNotFoundException paramContext)
    {
      com.truecaller.log.d.a(paramContext, "Cannot start activity");
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */