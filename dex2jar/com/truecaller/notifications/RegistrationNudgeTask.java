package com.truecaller.notifications;

import android.content.Context;
import android.os.Bundle;
import c.u;
import com.truecaller.TrueApp;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.h.am;
import com.truecaller.util.b.af;
import com.truecaller.util.b.at;
import com.truecaller.util.f;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;

public final class RegistrationNudgeTask
  extends PersistentBackgroundTask
{
  public static final RegistrationNudgeTask.a e = new RegistrationNudgeTask.a((byte)0);
  @Inject
  public com.truecaller.abtest.c a;
  @Inject
  public com.truecaller.analytics.b b;
  @Inject
  public aa c;
  @Inject
  public com.truecaller.featuretoggles.e d;
  
  public RegistrationNudgeTask()
  {
    if (com.truecaller.common.b.e.a("regNudgeLastShown", 0L) == 0L)
    {
      com.truecaller.common.b.e.b("regNudgeLastShown", System.currentTimeMillis());
      com.truecaller.common.b.e.b("regNudgeBadgeStartTime", System.currentTimeMillis());
    }
    Object localObject = TrueApp.y();
    c.g.b.k.a(localObject, "TrueApp.getApp()");
    ((TrueApp)localObject).a().a(this);
    localObject = a;
    if (localObject == null) {
      c.g.b.k.a("mFirebaseRemoteConfig");
    }
    ((com.truecaller.abtest.c)localObject).a();
  }
  
  private static boolean a(RegistrationNudgeTask.TaskState paramTaskState)
  {
    return (b(paramTaskState) == RegistrationNudgeTask.TaskState.DONE) || (paramTaskState == RegistrationNudgeTask.TaskState.DONE);
  }
  
  private static RegistrationNudgeTask.TaskState b(RegistrationNudgeTask.TaskState paramTaskState)
  {
    switch (ad.a[paramTaskState.ordinal()])
    {
    default: 
      return RegistrationNudgeTask.TaskState.DONE;
    case 4: 
      return RegistrationNudgeTask.TaskState.DONE;
    case 3: 
      return RegistrationNudgeTask.TaskState.THIRD;
    case 2: 
      return RegistrationNudgeTask.TaskState.SECOND;
    }
    return RegistrationNudgeTask.TaskState.FIRST;
  }
  
  public final int a()
  {
    return 10010;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    c.g.b.k.b(paramContext, "serviceContext");
    if (e(paramContext)) {
      return PersistentBackgroundTask.RunResult.FailedSkip;
    }
    paramBundle = paramContext.getApplicationContext();
    if (paramBundle != null)
    {
      paramBundle = ((bk)paramBundle).a().D();
      c.g.b.k.a(paramBundle, "(context.applicationCont…tsGraph.callingSettings()");
      int i;
      if (paramBundle.b("hasNativeDialerCallerId")) {
        i = 1;
      } else {
        i = 0;
      }
      if (i == 0)
      {
        i = (int)TimeUnit.MILLISECONDS.toDays(System.currentTimeMillis() - com.truecaller.common.b.e.a("regNudgeBadgeStartTime", System.currentTimeMillis() - 60000L));
        if (i > 0)
        {
          f.a(com.facebook.k.f(), i);
          com.truecaller.common.b.e.b("regNudgeBadgeSet", true);
        }
      }
      paramBundle = com.truecaller.common.b.e.a("registrationNotificationState", RegistrationNudgeTask.TaskState.INIT.toString());
      c.g.b.k.a(paramBundle, "CommonSettings.getString…askState.INIT.toString())");
      paramBundle = RegistrationNudgeTask.TaskState.valueOf(paramBundle);
      Object localObject = new StringBuilder("Current State: ");
      ((StringBuilder)localObject).append(paramBundle.toString());
      ((StringBuilder)localObject).toString();
      if (a(paramBundle)) {
        return PersistentBackgroundTask.RunResult.FailedSkip;
      }
      long l1 = com.truecaller.common.b.e.a("regNudgeLastShown", 0L);
      long l2 = b(paramBundle).getInterval();
      if (new org.a.a.b(l1).b(1000L * l2).d(System.currentTimeMillis()))
      {
        localObject = new StringBuilder("RegistrationNudgeTask: Time past: ");
        ((StringBuilder)localObject).append(System.currentTimeMillis() - l1);
        ((StringBuilder)localObject).toString();
        i = 1;
      }
      else
      {
        "RegistrationNudgeTask: Wait for next iteration ".concat(String.valueOf(l2));
        i = 0;
      }
      if (i != 0)
      {
        paramBundle = b(paramBundle);
        localObject = c;
        if (localObject == null) {
          c.g.b.k.a("mRegistrationNudgeHelper");
        }
        ((aa)localObject).a(paramContext, paramBundle.getTitle(), paramBundle.getText(), paramBundle.toString());
        com.truecaller.common.b.e.b("registrationNotificationState", paramBundle.toString());
        com.truecaller.common.b.e.b("regNudgeLastShown", System.currentTimeMillis());
        paramContext = b;
        if (paramContext == null) {
          c.g.b.k.a("mAnalytics");
        }
        localObject = new com.truecaller.analytics.e.a("Notification").a("Type", "regNudge").a("Status", am.a(paramBundle.toString())).a();
        c.g.b.k.a(localObject, "AnalyticsEvent.Builder(A…tate.toString())).build()");
        paramContext.a((com.truecaller.analytics.e)localObject);
        paramContext = new StringBuilder("RegistrationNudgeTask: Moved to State: ");
        paramContext.append(paramBundle.toString());
        paramContext.toString();
      }
      return PersistentBackgroundTask.RunResult.Success;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.GraphHolder");
  }
  
  public final boolean a(Context paramContext)
  {
    c.g.b.k.b(paramContext, "serviceContext");
    Object localObject = com.truecaller.common.b.e.a("registrationNotificationState", RegistrationNudgeTask.TaskState.INIT.toString());
    c.g.b.k.a(localObject, "CommonSettings.getString…askState.INIT.toString())");
    localObject = RegistrationNudgeTask.TaskState.valueOf((String)localObject);
    return (!e(paramContext)) && (!a((RegistrationNudgeTask.TaskState)localObject)) && (!(at.a(paramContext) instanceof af));
  }
  
  public final com.truecaller.common.background.e b()
  {
    com.truecaller.common.background.e locale = new com.truecaller.common.background.e.a(1).a(60L, TimeUnit.MINUTES).c(10L, TimeUnit.MINUTES).a(1).b();
    c.g.b.k.a(locale, "TaskConfiguration.Builde…ANY)\n            .build()");
    return locale;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.RegistrationNudgeTask
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */