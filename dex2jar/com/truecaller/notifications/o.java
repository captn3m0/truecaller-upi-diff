package com.truecaller.notifications;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import com.truecaller.callerid.e;
import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d<f<e>>
{
  private final Provider<i> a;
  private final Provider<e> b;
  
  private o(Provider<i> paramProvider, Provider<e> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static o a(Provider<i> paramProvider, Provider<e> paramProvider1)
  {
    return new o(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */