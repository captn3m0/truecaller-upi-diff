package com.truecaller.notifications;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.z.d;
import android.support.v4.content.b;
import android.text.TextUtils;
import com.truecaller.common.h.t;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.d;
import com.truecaller.old.data.access.Settings;
import com.truecaller.search.f;
import com.truecaller.util.al;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;

final class e
  extends h
{
  final SharedPreferences a;
  final LinkedHashSet<SourcedContact> b = new LinkedHashSet();
  private final Context c;
  private final al d;
  private final Handler e;
  private final BroadcastReceiver f;
  private final t g;
  private final com.truecaller.notificationchannels.e h;
  private final a i;
  
  @Inject
  e(Context paramContext, t paramt, al paramal, com.truecaller.notificationchannels.e parame, a parama)
  {
    c = paramContext;
    d = paramal;
    h = parame;
    i = parama;
    e = new Handler(Looper.getMainLooper());
    g = paramt;
    a = paramContext.getSharedPreferences("enhancedNumbers", 0);
    f = new e.1(this);
    c.registerReceiver(f, new IntentFilter("com.truecaller.ACTION_ENHANCED_NOTIFICATION_DELETED"), "com.truecaller.permission.ENHANCED_NOTIFICATION", null);
  }
  
  private void a(StatusBarNotification paramStatusBarNotification, Collection<SourcedContact> paramCollection, String paramString)
  {
    if (TextUtils.isEmpty(paramString)) {
      return;
    }
    Object localObject = g.a(paramString);
    "input: ".concat(String.valueOf(paramString));
    paramString = new StringBuilder("resolved numbers: [");
    paramString.append(TextUtils.join(", ", (Iterable)localObject));
    paramString.append("]");
    paramString.toString();
    localObject = ((List)localObject).iterator();
    while (((Iterator)localObject).hasNext())
    {
      paramString = (String)((Iterator)localObject).next();
      long l = a.getLong(paramString, 0L);
      boolean bool1;
      if ((l != 0L) && (System.currentTimeMillis() - 86400000L <= l)) {
        bool1 = false;
      } else {
        bool1 = true;
      }
      boolean bool2 = f.a(c, paramString);
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramString);
      localStringBuilder.append(": isNewSearch = ");
      localStringBuilder.append(bool1);
      localStringBuilder.append(", isInPhoneBook = ");
      localStringBuilder.append(bool2);
      localStringBuilder.toString();
      if ((bool1) && (!bool2))
      {
        "going to add contact for number: ".concat(String.valueOf(paramString));
        Contact localContact = a(c, paramString);
        if ((localContact != null) && (!TextUtils.isEmpty(localContact.t())))
        {
          "Contact resolved ".concat(String.valueOf(paramString));
          paramCollection.add(new SourcedContact(paramStatusBarNotification.getPackageName(), d(paramStatusBarNotification), localContact.getId(), localContact.getTcId(), localContact.t(), paramString, localContact.a(false), localContact.a(true)));
        }
        else
        {
          localStringBuilder = new StringBuilder("Contact resolution failed: ");
          if (localContact != null) {
            paramString = localContact.t();
          } else {
            paramString = null;
          }
          localStringBuilder.append(paramString);
          localStringBuilder.toString();
        }
      }
    }
  }
  
  private static boolean a(String paramString)
  {
    return NotificationHandlerService.a.contains(paramString);
  }
  
  private void b()
  {
    Object localObject1 = new StringBuilder("displayContactsNotification, mDisplayedContacts.size()=");
    ((StringBuilder)localObject1).append(b.size());
    ((StringBuilder)localObject1).append(" on ");
    ((StringBuilder)localObject1).append(Thread.currentThread().getName());
    ((StringBuilder)localObject1).toString();
    if (b.isEmpty())
    {
      i.a(2131363819);
      return;
    }
    localObject1 = new long[b.size()];
    Object localObject2 = b.iterator();
    int j = 0;
    while (((Iterator)localObject2).hasNext())
    {
      localObject3 = nextc;
      long l;
      if (localObject3 == null) {
        l = 0L;
      } else {
        l = ((Long)localObject3).longValue();
      }
      localObject1[j] = l;
      j += 1;
    }
    Object localObject3 = new Intent("com.truecaller.ACTION_ENHANCED_NOTIFICATION_DELETED");
    ((Intent)localObject3).putExtra("ids", (long[])localObject1);
    Intent localIntent = SourcedContactListActivity.a(c, b);
    localObject1 = c.getResources();
    String str = ((Resources)localObject1).getQuantityString(2131755015, b.size(), new Object[] { Integer.valueOf(b.size()) });
    if (b.size() == 1)
    {
      localObject4 = (SourcedContact)b.iterator().next();
      localObject2 = e;
      localObject1 = ((Resources)localObject1).getString(2131886525, new Object[] { b });
    }
    else
    {
      localObject2 = ((Resources)localObject1).getQuantityString(2131755015, b.size(), new Object[] { Integer.valueOf(b.size()) });
      localObject1 = ((Resources)localObject1).getString(2131886526);
    }
    Object localObject4 = new z.d(c, h.a());
    ((z.d)localObject4).d(str);
    ((z.d)localObject4).a((CharSequence)localObject2);
    ((z.d)localObject4).b((CharSequence)localObject1);
    ((z.d)localObject4).b(PendingIntent.getBroadcast(c, 2131364143, (Intent)localObject3, 268435456));
    ((z.d)localObject4).a(2131234787);
    ((z.d)localObject4).d(16);
    f = PendingIntent.getActivity(c, 2131364144, localIntent, 268435456);
    C = b.c(c, 2131099685);
    i.a(2131363819, ((z.d)localObject4).h(), "enhanceNotification");
  }
  
  private void c(StatusBarNotification paramStatusBarNotification)
  {
    Object localObject = paramStatusBarNotification.getNotification();
    if (localObject == null) {
      return;
    }
    LinkedHashSet localLinkedHashSet = new LinkedHashSet();
    if (!TextUtils.isEmpty(tickerText)) {
      a(paramStatusBarNotification, localLinkedHashSet, tickerText.toString());
    }
    a(paramStatusBarNotification, localLinkedHashSet, extras.getString("android.title"));
    localObject = extras.getStringArray("android.people");
    if (localObject != null)
    {
      int k = localObject.length;
      int j = 0;
      while (j < k)
      {
        a(paramStatusBarNotification, localLinkedHashSet, localObject[j]);
        j += 1;
      }
    }
    if (!localLinkedHashSet.isEmpty()) {
      e.post(new -..Lambda.e.IyB5-ZLWOtA620VC1yGnJb-d2FI(this, localLinkedHashSet));
    }
  }
  
  private String d(StatusBarNotification paramStatusBarNotification)
  {
    String str = "";
    try
    {
      PackageManager localPackageManager = c.getPackageManager();
      paramStatusBarNotification = getPackageInfogetPackageName0applicationInfo.loadLabel(localPackageManager);
      return String.valueOf(paramStatusBarNotification);
    }
    catch (PackageManager.NameNotFoundException|RuntimeException paramStatusBarNotification)
    {
      for (;;)
      {
        paramStatusBarNotification = str;
      }
    }
  }
  
  public final void a()
  {
    c.unregisterReceiver(f);
  }
  
  public final void a(StatusBarNotification paramStatusBarNotification)
  {
    int j = 1;
    StringBuilder localStringBuilder = new StringBuilder("shouldHandle() enabled=");
    localStringBuilder.append(Settings.e("enhancedNotificationsEnabled"));
    localStringBuilder.append(", hasValidAccount=");
    localStringBuilder.append(d.a());
    localStringBuilder.append(", validPackage=");
    localStringBuilder.append(a(paramStatusBarNotification.getPackageName()));
    localStringBuilder.toString();
    if ((!Settings.e("enhancedNotificationsEnabled")) || (!d.a()) || (!a(paramStatusBarNotification.getPackageName()))) {
      j = 0;
    }
    if (j == 0) {
      return;
    }
    try
    {
      c(paramStatusBarNotification);
      return;
    }
    catch (RuntimeException paramStatusBarNotification)
    {
      d.a(paramStatusBarNotification, "Error handling notification");
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */