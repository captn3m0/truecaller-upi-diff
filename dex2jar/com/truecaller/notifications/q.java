package com.truecaller.notifications;

import com.truecaller.notificationchannels.e;
import com.truecaller.notificationchannels.n;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class q
  implements d<e>
{
  private final Provider<n> a;
  
  private q(Provider<n> paramProvider)
  {
    a = paramProvider;
  }
  
  public static e a(n paramn)
  {
    return (e)g.a(paramn.c(), "Cannot return null from a non-@Nullable @Provides method");
  }
  
  public static q a(Provider<n> paramProvider)
  {
    return new q(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */