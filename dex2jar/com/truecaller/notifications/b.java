package com.truecaller.notifications;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.ac;
import c.g.b.k;
import com.truecaller.analytics.e.a;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;
import javax.inject.Inject;

public final class b
  implements a
{
  private final Random a;
  private final Context b;
  private final ac c;
  private final com.truecaller.analytics.b d;
  
  @Inject
  public b(Context paramContext, ac paramac, com.truecaller.analytics.b paramb)
  {
    b = paramContext;
    c = paramac;
    d = paramb;
    a = new Random();
  }
  
  private final Intent a(String paramString1, PendingIntent paramPendingIntent, String paramString2, Bundle paramBundle)
  {
    Intent localIntent = new Intent(b, AnalyticsNotificationReceiver.class);
    localIntent.putExtra("notification_type", paramString1);
    localIntent.putExtra("original_pending_intent", (Parcelable)paramPendingIntent);
    localIntent.putExtra("notification_status", paramString2);
    localIntent.putExtra("additional_params", paramBundle);
    return localIntent;
  }
  
  private final void a(String paramString1, String paramString2, Bundle paramBundle)
  {
    e.a locala = new e.a("Notification").a("Type", paramString1).a("Status", paramString2);
    if (paramBundle != null)
    {
      Iterator localIterator = paramBundle.keySet().iterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        paramString1 = paramBundle.get(str);
        if (paramString1 != null) {
          paramString1 = paramString1.toString();
        } else {
          paramString1 = null;
        }
        paramString2 = paramString1;
        if (paramString1 == null) {
          paramString2 = "";
        }
        locala.a(str, paramString2);
      }
    }
    paramString1 = d;
    paramString2 = locala.a();
    k.a(paramString2, "event.build()");
    paramString1.b(paramString2);
  }
  
  public final PendingIntent a(PendingIntent paramPendingIntent, String paramString1, String paramString2)
  {
    k.b(paramString1, "type");
    k.b(paramString2, "notificationStatus");
    paramPendingIntent = a(paramString1, paramPendingIntent, paramString2, null);
    paramPendingIntent = PendingIntent.getBroadcast(b, a.nextInt(), paramPendingIntent, 268435456);
    k.a(paramPendingIntent, "PendingIntent.getBroadca…_CANCEL_CURRENT\n        )");
    return paramPendingIntent;
  }
  
  public final void a(int paramInt)
  {
    c.a(paramInt);
  }
  
  public final void a(int paramInt, Notification paramNotification, String paramString)
  {
    k.b(paramNotification, "notification");
    a(null, paramInt, paramNotification, paramString, null);
  }
  
  public final void a(Intent paramIntent)
  {
    k.b(paramIntent, "intent");
    try
    {
      localObject = (PendingIntent)paramIntent.getParcelableExtra("original_pending_intent");
      if (localObject != null) {
        ((PendingIntent)localObject).send();
      }
    }
    catch (PendingIntent.CanceledException localCanceledException)
    {
      Object localObject;
      String str;
      for (;;) {}
    }
    localObject = paramIntent.getStringExtra("notification_type");
    if (localObject == null) {
      return;
    }
    str = paramIntent.getStringExtra("notification_status");
    if (str == null) {
      return;
    }
    a((String)localObject, str, (Bundle)paramIntent.getParcelableExtra("additional_params"));
  }
  
  public final void a(String paramString, int paramInt)
  {
    k.b(paramString, "tag");
    c.a(paramString, paramInt);
  }
  
  public final void a(String paramString1, int paramInt, Notification paramNotification, String paramString2)
  {
    k.b(paramNotification, "notification");
    a(paramString1, paramInt, paramNotification, paramString2, null);
  }
  
  public final void a(String paramString1, int paramInt, Notification paramNotification, String paramString2, Bundle paramBundle)
  {
    k.b(paramNotification, "notification");
    if (paramString2 != null)
    {
      a(paramString2, "Shown", paramBundle);
      Intent localIntent = a(paramString2, contentIntent, "Opened", paramBundle);
      paramString2 = a(paramString2, deleteIntent, "Dismissed", paramBundle);
      contentIntent = PendingIntent.getBroadcast(b, a.nextInt(), localIntent, 268435456);
      deleteIntent = PendingIntent.getBroadcast(b, a.nextInt(), paramString2, 268435456);
    }
    c.a(paramString1, paramInt, paramNotification);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */