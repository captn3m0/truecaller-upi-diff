package com.truecaller.notifications;

import com.truecaller.featuretoggles.e;
import dagger.a.d;
import javax.inject.Provider;

public final class z
  implements d<y>
{
  private final Provider<e> a;
  
  private z(Provider<e> paramProvider)
  {
    a = paramProvider;
  }
  
  public static z a(Provider<e> paramProvider)
  {
    return new z(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.z
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */