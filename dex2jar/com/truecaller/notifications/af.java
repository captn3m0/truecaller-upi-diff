package com.truecaller.notifications;

import android.app.Activity;
import android.app.Notification;
import android.content.ComponentName;
import android.content.Context;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.utils.h;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.inject.Inject;
import me.leolin.shortcutbadger.a;
import me.leolin.shortcutbadger.b;
import me.leolin.shortcutbadger.c;

public final class af
  implements ae
{
  private final a a;
  
  @Inject
  public af(Context paramContext)
  {
    try
    {
      Field localField = c.class.getDeclaredField("a");
      localField.setAccessible(true);
      Object localObject = (a)localField.get(null);
      if (localObject == null) {
        try
        {
          Method localMethod = c.class.getDeclaredMethod("a", new Class[] { Context.class });
          localMethod.setAccessible(true);
          localMethod.invoke(null, new Object[] { paramContext });
          paramContext = (a)localField.get(null);
        }
        catch (InvocationTargetException localInvocationTargetException2)
        {
          paramContext = (Context)localObject;
          localObject = localInvocationTargetException2;
          break label121;
        }
        catch (NoSuchMethodException localNoSuchMethodException2)
        {
          paramContext = (Context)localObject;
          localObject = localNoSuchMethodException2;
          break label140;
        }
        catch (IllegalAccessException localIllegalAccessException2)
        {
          paramContext = (Context)localObject;
          localObject = localIllegalAccessException2;
          break label159;
        }
        catch (NoSuchFieldException localNoSuchFieldException2)
        {
          paramContext = (Context)localObject;
          localObject = localNoSuchFieldException2;
          break label178;
        }
      } else {
        paramContext = (Context)localObject;
      }
    }
    catch (InvocationTargetException localInvocationTargetException1)
    {
      paramContext = null;
      AssertionUtil.OnlyInDebug.shouldNeverHappen(localInvocationTargetException1, new String[] { "Could not access badger init method" });
    }
    catch (NoSuchMethodException localNoSuchMethodException1)
    {
      paramContext = null;
      AssertionUtil.OnlyInDebug.shouldNeverHappen(localNoSuchMethodException1, new String[] { "Could not access badger init method" });
    }
    catch (IllegalAccessException localIllegalAccessException1)
    {
      paramContext = null;
      AssertionUtil.OnlyInDebug.shouldNeverHappen(localIllegalAccessException1, new String[] { "Could not access badger" });
    }
    catch (NoSuchFieldException localNoSuchFieldException1)
    {
      label121:
      label140:
      label159:
      paramContext = null;
      label178:
      AssertionUtil.OnlyInDebug.shouldNeverHappen(localNoSuchFieldException1, new String[] { "Could not access badger" });
    }
    a = paramContext;
  }
  
  public static void a(Context paramContext, Notification paramNotification, String paramString, int paramInt)
  {
    if ((h.a()) && (paramNotification != null)) {
      c.a(paramString, paramContext, paramNotification, paramInt);
    }
  }
  
  public final void a(Context paramContext, Class<? extends Activity> paramClass, int paramInt)
  {
    a locala = a;
    if (locala != null) {}
    try
    {
      locala.a(paramContext, new ComponentName(paramContext, paramClass), paramInt);
      return;
    }
    catch (Exception paramContext)
    {
      AssertionUtil.reportThrowableButNeverCrash(paramContext);
      return;
      return;
    }
    catch (b paramContext) {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.af
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */