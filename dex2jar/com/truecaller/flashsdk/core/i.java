package com.truecaller.flashsdk.core;

import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.FlashContact;
import com.truecaller.flashsdk.models.a;
import java.util.List;

public abstract interface i
{
  public abstract String A();
  
  public abstract List<a> B();
  
  public abstract FlashContact C();
  
  public abstract void a(int paramInt, String paramString1, String paramString2);
  
  public abstract void a(Flash paramFlash);
  
  public abstract void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5);
  
  public abstract boolean a(String paramString);
  
  public abstract boolean b(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */