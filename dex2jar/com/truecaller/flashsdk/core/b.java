package com.truecaller.flashsdk.core;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.truecaller.flashsdk.db.g;
import com.truecaller.flashsdk.db.l;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.FlashContact;
import com.truecaller.flashsdk.models.e;
import java.util.ArrayList;
import java.util.List;

public abstract interface b
{
  public abstract Intent a(Context paramContext, String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean, String paramString5);
  
  public abstract void a(long paramLong);
  
  public abstract void a(long paramLong, String paramString);
  
  public abstract void a(long paramLong, List<String> paramList, String paramString);
  
  public abstract void a(Context paramContext, long paramLong, String paramString1, String paramString2);
  
  public abstract void a(Context paramContext, long paramLong1, String paramString1, String paramString2, long paramLong2);
  
  public abstract void a(Context paramContext, long paramLong, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, boolean paramBoolean, String paramString6);
  
  public abstract void a(Context paramContext, ArrayList<FlashContact> paramArrayList, String paramString);
  
  public abstract void a(Theme paramTheme);
  
  public abstract void a(i parami);
  
  public abstract void a(s params);
  
  public abstract void a(l paraml);
  
  public abstract void a(l paraml, String... paramVarArgs);
  
  public abstract void a(Flash paramFlash);
  
  public abstract void a(String paramString, long paramLong, Flash paramFlash);
  
  public abstract void a(String paramString, Bundle paramBundle);
  
  public abstract void a(String paramString1, String paramString2);
  
  public abstract void a(List<e> paramList);
  
  public abstract boolean a();
  
  public abstract boolean a(String paramString);
  
  public abstract void b();
  
  public abstract void b(long paramLong);
  
  public abstract void b(long paramLong, String paramString);
  
  public abstract void b(Context paramContext, long paramLong, String paramString1, String paramString2);
  
  public abstract void b(String paramString);
  
  public abstract boolean c();
  
  public abstract boolean c(String paramString);
  
  public abstract Uri d();
  
  public abstract void d(String paramString);
  
  public abstract long e(String paramString);
  
  public abstract i e();
  
  public abstract int f(String paramString);
  
  public abstract Theme f();
  
  public abstract int g();
  
  public abstract g g(String paramString);
  
  public abstract e h(String paramString);
  
  public abstract boolean h();
  
  public abstract void i();
  
  public abstract void j();
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */