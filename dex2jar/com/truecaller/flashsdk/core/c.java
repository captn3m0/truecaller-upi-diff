package com.truecaller.flashsdk.core;

import android.app.Application;
import android.content.Context;
import c.f;
import c.g;
import c.g.b.k;
import c.u;
import com.truecaller.flashsdk.core.a.a.b.a;
import com.truecaller.utils.t.a;

public final class c
{
  public static final c b = new c();
  private static final f c = g.a((c.g.a.a)c.a.a);
  
  public static final b a()
  {
    return (b)c.b();
  }
  
  public static void a(Application paramApplication)
  {
    k.b(paramApplication, "application");
    Object localObject = a();
    if (localObject != null)
    {
      localObject = (d)localObject;
      k.b(paramApplication, "application");
      com.truecaller.common.a locala = ((com.truecaller.common.b.a)paramApplication).u();
      k.a(locala, "(application as ApplicationBase).commonGraph");
      paramApplication = com.truecaller.flashsdk.core.a.a.b.s().a(com.truecaller.utils.c.a().a((Context)paramApplication).a()).a(locala).a(new com.truecaller.flashsdk.core.a.b.a()).a();
      k.a(paramApplication, "DaggerAppComponent.build…e())\n            .build()");
      e = paramApplication;
      paramApplication = e;
      if (paramApplication == null) {
        k.a("component");
      }
      paramApplication.a((d)localObject);
      return;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.flashsdk.core.FlashManagerImpl");
  }
  
  public static com.truecaller.flashsdk.core.a.a.a b()
  {
    Object localObject = a();
    if (localObject != null)
    {
      localObject = e;
      if (localObject == null) {
        k.a("component");
      }
      return (com.truecaller.flashsdk.core.a.a.a)localObject;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.flashsdk.core.FlashManagerImpl");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */