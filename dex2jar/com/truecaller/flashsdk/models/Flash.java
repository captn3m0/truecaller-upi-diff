package com.truecaller.flashsdk.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.SystemClock;
import android.text.TextUtils;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.a.a;
import com.google.gson.f;
import java.util.Map;
import java.util.Random;

public class Flash
  implements Parcelable
{
  public static final Parcelable.Creator<Flash> CREATOR = new Flash.1();
  @a
  @com.google.gson.a.c(a="sender")
  protected Sender a;
  @a
  @com.google.gson.a.c(a="to")
  protected long b;
  @a
  @com.google.gson.a.c(a="threadId")
  protected String c;
  @a
  @com.google.gson.a.c(a="state")
  protected String d;
  @a
  @com.google.gson.a.c(a="history")
  protected String e;
  @a
  @com.google.gson.a.c(a="payload")
  protected Payload f;
  @a
  @com.google.gson.a.c(a="timestamp")
  protected long g;
  @a
  @com.google.gson.a.c(a="instanceId")
  protected String h;
  
  public Flash() {}
  
  protected Flash(Parcel paramParcel)
  {
    a = ((Sender)paramParcel.readParcelable(Sender.class.getClassLoader()));
    b = paramParcel.readLong();
    c = paramParcel.readString();
    d = paramParcel.readString();
    e = paramParcel.readString();
    f = ((Payload)paramParcel.readParcelable(Payload.class.getClassLoader()));
    g = paramParcel.readLong();
    h = paramParcel.readString();
  }
  
  public static Flash a(RemoteMessage paramRemoteMessage, f paramf)
  {
    Map localMap = paramRemoteMessage.a();
    if ((localMap.containsKey("sender")) && (localMap.containsKey("payload")) && (localMap.containsKey("timestamp")))
    {
      Flash localFlash = new Flash();
      a = ((Sender)paramf.a((String)localMap.get("sender"), Sender.class));
      f = ((Payload)paramf.a((String)localMap.get("payload"), Payload.class));
      g = Long.parseLong((String)localMap.get("timestamp"));
      if (localMap.containsKey("instanceId")) {
        h = ((String)localMap.get("instanceId"));
      } else if (paramRemoteMessage.b() != null) {
        h = paramRemoteMessage.b();
      }
      if (localMap.containsKey("history")) {
        e = ((String)localMap.get("history"));
      } else {
        e = com.truecaller.flashsdk.assist.c.a("💬");
      }
      if (localMap.containsKey("state")) {
        d = ((String)localMap.get("state"));
      }
      if (localMap.containsKey("thread_id")) {
        c = ((String)localMap.get("thread_id"));
      }
      if (localMap.containsKey("threadId")) {
        c = ((String)localMap.get("threadId"));
      }
      return localFlash;
    }
    return null;
  }
  
  private static String d(String paramString)
  {
    return String.format("%s-%s", new Object[] { Long.toHexString(SystemClock.elapsedRealtime()), Integer.toHexString(new Random(paramString.hashCode()).nextInt()) });
  }
  
  public final Sender a()
  {
    return a;
  }
  
  public final void a(long paramLong)
  {
    b = paramLong;
  }
  
  public final void a(Payload paramPayload)
  {
    f = paramPayload;
  }
  
  public final void a(String paramString)
  {
    d = paramString;
  }
  
  public final long b()
  {
    return b;
  }
  
  public final void b(long paramLong)
  {
    g = paramLong;
  }
  
  public final void b(String paramString)
  {
    e = paramString;
  }
  
  public final FlashRequest c(String paramString)
  {
    return new FlashRequest(h, "6", new Data(b, c, d, e, paramString, f));
  }
  
  public final String c()
  {
    return c;
  }
  
  public final String d()
  {
    return d;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public final String e()
  {
    return e;
  }
  
  public final Payload f()
  {
    return f;
  }
  
  public final long g()
  {
    return g;
  }
  
  public final String h()
  {
    return h;
  }
  
  public final void i()
  {
    c = d(Long.toString(b));
  }
  
  public final void j()
  {
    long l = b;
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(c);
    localStringBuilder.append(System.currentTimeMillis());
    h = String.format("-%s-%s", new Object[] { Long.valueOf(l), d(String.valueOf(localStringBuilder.toString())) });
  }
  
  public final boolean k()
  {
    Object localObject = a;
    if (localObject == null) {
      return false;
    }
    if (Long.valueOf(a) == null) {
      return false;
    }
    localObject = f;
    if (localObject == null) {
      return false;
    }
    if (TextUtils.isEmpty(a)) {
      return false;
    }
    return !TextUtils.isEmpty(f.a);
  }
  
  public final boolean l()
  {
    if (b == 0L) {
      return false;
    }
    Payload localPayload = f;
    if (localPayload == null) {
      return false;
    }
    if (TextUtils.isEmpty(a)) {
      return false;
    }
    return !TextUtils.isEmpty(f.a);
  }
  
  public final boolean m()
  {
    return (!TextUtils.isEmpty(h)) && (!TextUtils.isEmpty(c));
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeParcelable(a, paramInt);
    paramParcel.writeLong(b);
    paramParcel.writeString(c);
    paramParcel.writeString(d);
    paramParcel.writeString(e);
    paramParcel.writeParcelable(f, paramInt);
    paramParcel.writeLong(g);
    paramParcel.writeString(h);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.Flash
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */