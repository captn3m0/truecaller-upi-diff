package com.truecaller;

import dagger.a.d;
import javax.inject.Provider;

public final class aa
  implements d<com.truecaller.search.local.model.c>
{
  private final c a;
  private final Provider<com.truecaller.search.local.model.c> b;
  
  private aa(c paramc, Provider<com.truecaller.search.local.model.c> paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static aa a(c paramc, Provider<com.truecaller.search.local.model.c> paramProvider)
  {
    return new aa(paramc, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.aa
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */