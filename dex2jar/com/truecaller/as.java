package com.truecaller;

import com.truecaller.calling.ar;
import com.truecaller.multisim.h;
import com.truecaller.utils.n;
import dagger.a.d;
import javax.inject.Provider;

public final class as
  implements d<ar>
{
  private final c a;
  private final Provider<com.truecaller.i.c> b;
  private final Provider<h> c;
  private final Provider<n> d;
  
  private as(c paramc, Provider<com.truecaller.i.c> paramProvider, Provider<h> paramProvider1, Provider<n> paramProvider2)
  {
    a = paramc;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
  }
  
  public static as a(c paramc, Provider<com.truecaller.i.c> paramProvider, Provider<h> paramProvider1, Provider<n> paramProvider2)
  {
    return new as(paramc, paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.as
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */