package com.truecaller;

import android.content.Context;
import c.d.f;
import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d<com.truecaller.calling.after_call.b>
{
  private final c a;
  private final Provider<Context> b;
  private final Provider<f> c;
  private final Provider<com.truecaller.analytics.b> d;
  
  private j(c paramc, Provider<Context> paramProvider, Provider<f> paramProvider1, Provider<com.truecaller.analytics.b> paramProvider2)
  {
    a = paramc;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
  }
  
  public static j a(c paramc, Provider<Context> paramProvider, Provider<f> paramProvider1, Provider<com.truecaller.analytics.b> paramProvider2)
  {
    return new j(paramc, paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */