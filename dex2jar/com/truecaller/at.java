package com.truecaller;

import com.truecaller.messaging.h;
import com.truecaller.messaging.j;
import javax.inject.Provider;

public final class at
  implements dagger.a.d<j>
{
  private final c a;
  private final Provider<com.truecaller.utils.d> b;
  private final Provider<h> c;
  
  private at(c paramc, Provider<com.truecaller.utils.d> paramProvider, Provider<h> paramProvider1)
  {
    a = paramc;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static at a(c paramc, Provider<com.truecaller.utils.d> paramProvider, Provider<h> paramProvider1)
  {
    return new at(paramc, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.at
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */