package com.truecaller.callrecording;

import android.media.MediaRecorder;
import android.os.Environment;
import java.io.File;
import java.io.IOException;

public final class b
  implements CallRecorder
{
  private final MediaRecorder a = new MediaRecorder();
  private String b;
  private CallRecorder.RecordingState c = CallRecorder.RecordingState.INITIALIZING;
  private CallRecorder.a d;
  
  public final String getOutputFile()
  {
    return b;
  }
  
  public final CallRecorder.RecordingState getRecordingState()
  {
    return c;
  }
  
  public final boolean isRecording()
  {
    return c == CallRecorder.RecordingState.RECORDING;
  }
  
  public final void prepare()
  {
    c = CallRecorder.RecordingState.READY;
  }
  
  public final void release() {}
  
  public final void reset() {}
  
  public final void setErrorListener(CallRecorder.a parama)
  {
    d = parama;
  }
  
  public final void setOutputFile(String paramString)
  {
    Object localObject = new StringBuilder("setOutputFile() called with: filePath = [");
    ((StringBuilder)localObject).append(paramString);
    ((StringBuilder)localObject).append("]");
    ((StringBuilder)localObject).toString();
    localObject = paramString;
    if (paramString != null) {
      localObject = paramString.replace(".m4a", ".3gp");
    }
    "setOutputFile:: New file: ".concat(String.valueOf(localObject));
    b = ((String)localObject);
  }
  
  public final void start()
    throws IOException
  {
    Object localObject = Environment.getExternalStorageState();
    if (((String)localObject).equals("mounted"))
    {
      localObject = new File(b).getParentFile();
      if ((!((File)localObject).exists()) && (!((File)localObject).mkdirs())) {
        throw new IOException("Path to file could not be created.");
      }
      try
      {
        a.setAudioSource(1);
        a.setOutputFormat(1);
        a.setAudioEncoder(1);
        a.setOutputFile(b);
        a.prepare();
        a.start();
        c = CallRecorder.RecordingState.RECORDING;
        return;
      }
      catch (Exception localException)
      {
        d.onError(localException);
        return;
      }
    }
    StringBuilder localStringBuilder = new StringBuilder("SD Card is not mounted.  It is ");
    localStringBuilder.append(localException);
    localStringBuilder.append(".");
    throw new IOException(localStringBuilder.toString());
  }
  
  public final void stop()
  {
    a.stop();
    a.release();
    c = CallRecorder.RecordingState.STOPPED;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callrecording.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */