package com.truecaller.callrecording;

import java.io.IOException;

public abstract interface CallRecorder
{
  public abstract String getOutputFile();
  
  public abstract RecordingState getRecordingState();
  
  public abstract boolean isRecording();
  
  public abstract void prepare();
  
  public abstract void release();
  
  public abstract void reset();
  
  public abstract void setErrorListener(a parama);
  
  public abstract void setOutputFile(String paramString);
  
  public abstract void start()
    throws IOException;
  
  public abstract void stop()
    throws IOException;
  
  public static enum RecordingState
  {
    static
    {
      RecordingState localRecordingState1 = new RecordingState("INITIALIZING", 0);
      INITIALIZING = localRecordingState1;
      RecordingState localRecordingState2 = new RecordingState("READY", 1);
      READY = localRecordingState2;
      RecordingState localRecordingState3 = new RecordingState("RECORDING", 2);
      RECORDING = localRecordingState3;
      RecordingState localRecordingState4 = new RecordingState("ERROR", 3);
      ERROR = localRecordingState4;
      RecordingState localRecordingState5 = new RecordingState("STOPPED", 4);
      STOPPED = localRecordingState5;
      RecordingState localRecordingState6 = new RecordingState("PAUSED", 5);
      PAUSED = localRecordingState6;
      $VALUES = new RecordingState[] { localRecordingState1, localRecordingState2, localRecordingState3, localRecordingState4, localRecordingState5, localRecordingState6 };
    }
    
    private RecordingState() {}
  }
  
  public static abstract interface a
  {
    public abstract void onError(Exception paramException);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callrecording.CallRecorder
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */