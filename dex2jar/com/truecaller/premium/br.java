package com.truecaller.premium;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import c.g.b.k;
import c.u;
import com.truecaller.common.b.a;
import com.truecaller.common.b.e;
import com.truecaller.engagementrewards.ui.ClaimEngagementRewardsActivity;
import com.truecaller.engagementrewards.ui.ClaimEngagementRewardsActivity.a;
import com.truecaller.premium.data.SubscriptionPromoEventMetaData;
import com.truecaller.ui.WizardActivity;
import com.truecaller.wizard.b.c;
import java.io.Serializable;
import javax.inject.Inject;

public final class br
{
  public static Intent a(Context paramContext, PremiumPresenterView.LaunchContext paramLaunchContext, String paramString, SubscriptionPromoEventMetaData paramSubscriptionPromoEventMetaData)
  {
    k.b(paramContext, "context");
    paramContext = new Intent(paramContext, a(paramLaunchContext)).putExtra("launchContext", (Serializable)paramLaunchContext).putExtra("selectedPage", paramString).putExtra("analyticsMetadata", (Parcelable)paramSubscriptionPromoEventMetaData).addFlags(268435456);
    k.a(paramContext, "Intent(context, activity…t.FLAG_ACTIVITY_NEW_TASK)");
    return paramContext;
  }
  
  private static Class<? extends PremiumActivity> a(PremiumPresenterView.LaunchContext paramLaunchContext)
  {
    if (paramLaunchContext != null) {
      switch (bs.a[paramLaunchContext.ordinal()])
      {
      default: 
        break;
      case 1: 
      case 2: 
        return PremiumDialogActivity.class;
      }
    }
    return PremiumActivity.class;
  }
  
  public static void a(Context paramContext, PremiumPresenterView.LaunchContext paramLaunchContext)
  {
    k.b(paramContext, "context");
    k.b(paramLaunchContext, "launchContext");
    paramContext.startActivity(a(paramContext, paramLaunchContext, null, 8));
  }
  
  public static void a(Context paramContext, PremiumPresenterView.LaunchContext paramLaunchContext, String paramString)
  {
    k.b(paramContext, "context");
    k.b(paramLaunchContext, "launchContext");
    k.b(paramString, "page");
    paramContext.startActivity(a(paramContext, paramLaunchContext, paramString, 8));
  }
  
  public static void a(Context paramContext, String paramString, PremiumPresenterView.LaunchContext paramLaunchContext, SubscriptionPromoEventMetaData paramSubscriptionPromoEventMetaData)
  {
    k.b(paramContext, "context");
    k.b(paramLaunchContext, "launchContext");
    k.b(paramSubscriptionPromoEventMetaData, "subscriptionPromoEventMetaData");
    paramContext.startActivity(a(paramContext, paramLaunchContext, paramString, paramSubscriptionPromoEventMetaData));
  }
  
  public static void b(Context paramContext, PremiumPresenterView.LaunchContext paramLaunchContext)
  {
    k.b(paramContext, "context");
    k.b(paramLaunchContext, "launchContext");
    paramContext.startActivity(c(paramContext, paramLaunchContext));
  }
  
  public static void b(Context paramContext, String paramString, PremiumPresenterView.LaunchContext paramLaunchContext, SubscriptionPromoEventMetaData paramSubscriptionPromoEventMetaData)
  {
    k.b(paramContext, "context");
    k.b(paramLaunchContext, "launchContext");
    k.b(paramSubscriptionPromoEventMetaData, "subscriptionPromoEventMetaData");
    Object localObject = paramContext.getApplicationContext();
    if (localObject != null)
    {
      localObject = (a)localObject;
      if ((((a)localObject).p()) && (c.f()))
      {
        a(paramContext, paramString, paramLaunchContext, paramSubscriptionPromoEventMetaData);
        return;
      }
      if (e.a("silentLoginFailed", false)) {
        ((a)localObject).a(false);
      }
      c.a(paramContext, WizardActivity.class);
      return;
    }
    throw new u("null cannot be cast to non-null type com.truecaller.common.app.ApplicationBase");
  }
  
  public static Intent c(Context paramContext, PremiumPresenterView.LaunchContext paramLaunchContext)
  {
    k.b(paramContext, "context");
    k.b(paramLaunchContext, "launchContext");
    ClaimEngagementRewardsActivity.a locala = ClaimEngagementRewardsActivity.c;
    return ClaimEngagementRewardsActivity.a.a(paramContext, paramLaunchContext.name());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.br
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */