package com.truecaller.premium.data;

import com.truecaller.featuretoggles.e;
import javax.inject.Provider;

public final class d
  implements dagger.a.d<c>
{
  private final Provider<e> a;
  
  private d(Provider<e> paramProvider)
  {
    a = paramProvider;
  }
  
  public static d a(Provider<e> paramProvider)
  {
    return new d(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */