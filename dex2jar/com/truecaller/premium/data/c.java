package com.truecaller.premium.data;

import c.a.m;
import com.truecaller.featuretoggles.e;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;

public final class c
{
  private final e a;
  
  @Inject
  public c(e parame)
  {
    a = parame;
  }
  
  public final List<b> a()
  {
    boolean bool2 = true;
    Object localObject1 = new b(true, 2131886829);
    Object localObject2 = new b(true, 2131886839);
    Object localObject3 = new b(a.L().a(), 2131886836);
    boolean bool1;
    if ((a.M().a()) && (a.N().a())) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    b localb1 = new b(bool1, 2131886835);
    if ((a.O().a()) && (a.P().a())) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    b localb2 = new b(bool1, 2131886831);
    b localb3 = new b(a.K().a(), 2131886832);
    if ((a.Q().a()) && (a.R().a())) {
      bool1 = bool2;
    } else {
      bool1 = false;
    }
    localObject2 = (Iterable)m.b(new b[] { localObject1, localObject2, localObject3, localb1, localb2, localb3, new b(bool1, 2131886834), new b(a.S().a(), 2131886833) });
    localObject1 = (Collection)new ArrayList();
    localObject2 = ((Iterable)localObject2).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject3 = ((Iterator)localObject2).next();
      if (a) {
        ((Collection)localObject1).add(localObject3);
      }
    }
    return (List)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */