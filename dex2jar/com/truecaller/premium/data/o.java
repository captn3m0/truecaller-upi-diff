package com.truecaller.premium.data;

import android.content.Context;
import com.truecaller.calling.recorder.h;
import com.truecaller.common.g.a;
import com.truecaller.whoviewedme.w;
import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d<n>
{
  private final Provider<Context> a;
  private final Provider<com.truecaller.androidactors.f<g>> b;
  private final Provider<com.truecaller.util.b.j> c;
  private final Provider<w> d;
  private final Provider<h> e;
  private final Provider<com.truecaller.common.f.c> f;
  private final Provider<a> g;
  private final Provider<com.truecaller.data.entity.g> h;
  private final Provider<j> i;
  private final Provider<c> j;
  private final Provider<c.d.f> k;
  
  private o(Provider<Context> paramProvider, Provider<com.truecaller.androidactors.f<g>> paramProvider1, Provider<com.truecaller.util.b.j> paramProvider2, Provider<w> paramProvider3, Provider<h> paramProvider4, Provider<com.truecaller.common.f.c> paramProvider5, Provider<a> paramProvider6, Provider<com.truecaller.data.entity.g> paramProvider7, Provider<j> paramProvider8, Provider<c> paramProvider9, Provider<c.d.f> paramProvider10)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
    k = paramProvider10;
  }
  
  public static o a(Provider<Context> paramProvider, Provider<com.truecaller.androidactors.f<g>> paramProvider1, Provider<com.truecaller.util.b.j> paramProvider2, Provider<w> paramProvider3, Provider<h> paramProvider4, Provider<com.truecaller.common.f.c> paramProvider5, Provider<a> paramProvider6, Provider<com.truecaller.data.entity.g> paramProvider7, Provider<j> paramProvider8, Provider<c> paramProvider9, Provider<c.d.f> paramProvider10)
  {
    return new o(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */