package com.truecaller.premium.data;

import android.support.v4.f.j;
import com.truecaller.androidactors.w;
import com.truecaller.common.f.a.a;
import com.truecaller.common.f.c.b;
import com.truecaller.common.f.c.c;
import com.truecaller.common.h.am;
import com.truecaller.engagementrewards.EngagementRewardActionType;
import com.truecaller.engagementrewards.EngagementRewardState;
import com.truecaller.engagementrewards.ui.d;
import com.truecaller.featuretoggles.e;
import com.truecaller.log.AssertionUtil;
import com.truecaller.old.data.access.Settings;
import org.a.a.d.i.a;

public final class p
  implements com.truecaller.common.f.c
{
  private final com.truecaller.androidactors.k a;
  private final com.truecaller.androidactors.f<g> b;
  private final com.truecaller.androidactors.f<com.truecaller.network.util.f> c;
  private final com.truecaller.common.g.a d;
  private final com.truecaller.filters.p e;
  private final com.truecaller.engagementrewards.c f;
  private final d g;
  private final com.truecaller.engagementrewards.g h;
  private final e i;
  
  public p(com.truecaller.androidactors.f<g> paramf, com.truecaller.androidactors.f<com.truecaller.network.util.f> paramf1, com.truecaller.androidactors.k paramk, com.truecaller.common.g.a parama, com.truecaller.filters.p paramp, com.truecaller.engagementrewards.c paramc, d paramd, com.truecaller.engagementrewards.g paramg, e parame)
  {
    b = paramf;
    c = paramf1;
    a = paramk;
    d = parama;
    e = paramp;
    f = paramc;
    g = paramd;
    h = paramg;
    i = parame;
  }
  
  private static int a(boolean paramBoolean, z paramz)
  {
    if (!paramBoolean) {
      return -1;
    }
    if (paramz == null) {
      return -2;
    }
    return 0;
  }
  
  private static long a(String paramString)
  {
    if (org.c.a.a.a.k.b(paramString)) {
      return 0L;
    }
    return aba;
  }
  
  private static com.truecaller.common.f.a a(z paramz)
  {
    if ((paramz != null) && (b != null))
    {
      int j = Integer.parseInt(b.a);
      long l1 = a(b.b);
      long l2 = a(b.c);
      int k = b(b.d);
      String str1;
      if (b.e != null) {
        str1 = b.e.a;
      } else {
        str1 = null;
      }
      String str2 = c(b.f);
      a.a locala = new a.a();
      a = j;
      b = l1;
      c = l2;
      d = k;
      f = str1;
      g = str2;
      e = b.g;
      return locala.a();
    }
    return new a.a().a();
  }
  
  private void a(j<Boolean, z> paramj, String paramString, c.c paramc)
  {
    boolean bool = ((Boolean)a).booleanValue();
    Object localObject = (z)b;
    paramj = a((z)localObject);
    if (bool) {
      a(paramj);
    }
    if (paramc == null) {
      return;
    }
    int j;
    if (!bool)
    {
      j = -1;
    }
    else if (localObject == null)
    {
      j = -2;
    }
    else
    {
      localObject = a;
      if (am.b((CharSequence)localObject, "Successful")) {
        j = 0;
      } else if (am.b((CharSequence)localObject, "ExistsAnotherUser")) {
        j = 2;
      } else if (am.b((CharSequence)localObject, "ExistsSameUser")) {
        j = 3;
      } else if (am.b((CharSequence)localObject, "NotPremiumOwnerDevice")) {
        j = 4;
      } else {
        j = 1;
      }
    }
    paramc.a(j, paramString, paramj);
  }
  
  private void a(com.truecaller.common.f.a parama)
  {
    boolean bool2 = a(b);
    boolean bool1 = a(c);
    if ((bool2) && (bool1))
    {
      d.d("premiumDuration");
      Settings.f("premiumGraceExpiration");
      a(0);
      d.d("subscriptionStatus");
      d.d("subscriptionErrorResolveUrl");
      d.d("subscriptionPaymentFailedViewShownOnce");
      d.d("subscriptionStatusChangedReason");
    }
    else
    {
      long l = (b - System.currentTimeMillis()) / 1000L;
      d.b("premiumDuration", l);
      Settings.a("premiumGraceExpiration", c);
      a(a);
    }
    if (d == 1) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    Settings.a("premiumRenewable", bool1);
    Settings.g("premiumTimestamp");
    d.b("premiumLastFetchDate", System.currentTimeMillis());
    Settings.b("premiumLevel", g);
    if (i.aq().a())
    {
      if ((e != null) && (e.booleanValue())) {
        bool1 = true;
      } else {
        bool1 = false;
      }
      "Engagement Rewards: updatePremium:: isFreeTrial: ".concat(String.valueOf(bool1));
      d.b("subscriptionStatusChangedIsFreeTrial", bool1);
      if ((d()) && (!bool2) && (!bool1) && (f.a(EngagementRewardActionType.BUY_PREMIUM_ANNUAL) == EngagementRewardState.PENDING) && (f.a(d.a("subscriptionPurchaseSku"))))
      {
        h.a(d.a("subscriptionPurchaseSource"));
        f.a(EngagementRewardActionType.BUY_PREMIUM_ANNUAL, EngagementRewardState.COMPLETED);
        g.a();
      }
    }
    if (!d())
    {
      e.a(null);
      return;
    }
    if (e.k() == null) {
      e.a(Boolean.TRUE);
    }
  }
  
  private static int b(String paramString)
  {
    if (org.c.a.a.a.k.b(paramString)) {
      return 0;
    }
    if (org.c.a.a.a.b.a(paramString)) {
      return 1;
    }
    return 2;
  }
  
  private static String c(String paramString)
  {
    if (am.b(paramString, "regular")) {
      return "regular";
    }
    if (am.b(paramString, "gold")) {
      return "gold";
    }
    return "none";
  }
  
  private boolean p()
  {
    long l = q();
    return (l <= 0L) || (Settings.b("premiumTimestamp", l));
  }
  
  private long q()
  {
    return d.a("premiumDuration", 0L) * 1000L;
  }
  
  public final int a()
  {
    if (!Settings.a("premiumRenewable")) {
      return 0;
    }
    if (Settings.b("premiumRenewable", false)) {
      return 1;
    }
    return 2;
  }
  
  public final void a(int paramInt)
  {
    Settings.a("premiumRequests", paramInt);
  }
  
  public final void a(String paramString1, String paramString2, c.b paramb)
  {
    String str = com.truecaller.profile.c.b(d);
    ((com.truecaller.network.util.f)c.a()).a(str, paramString2, "Unable to purchase Truecaller Professional", paramString1, k()).a(a.a(), new -..Lambda.p.-XlnCRH8s5Yax-cu3Xt0BtUrikM(paramb));
  }
  
  public final void a(String paramString1, String paramString2, c.c paramc)
  {
    ((g)b.a()).a(paramString1, paramString2).a(a.a(), new -..Lambda.p.mvg2nsCNeThCI5F-OpnlO_krlLI(this, paramString1, paramc));
  }
  
  public final boolean a(long paramLong)
  {
    return paramLong < System.currentTimeMillis();
  }
  
  public final j<Integer, com.truecaller.common.f.a> b()
  {
    try
    {
      Object localObject = (j)((g)b.a()).a().d();
      AssertionUtil.isNotNull(localObject, new String[0]);
      boolean bool = ((Boolean)a).booleanValue();
      localObject = (z)b;
      com.truecaller.common.f.a locala = a((z)localObject);
      int j = a(bool, (z)localObject);
      if (j == 0) {
        a(locala);
      }
      localObject = j.a(Integer.valueOf(j), locala);
      return (j<Integer, com.truecaller.common.f.a>)localObject;
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;) {}
    }
    return null;
  }
  
  public final void b(String paramString1, String paramString2, c.c paramc)
  {
    ((g)b.a()).b(paramString1, paramString2).a(a.a(), new -..Lambda.p.7VGc8q1eBIz3VblmELpeAQEjWaE(this, paramString1, paramc));
  }
  
  public final void c()
  {
    ((g)b.a()).a().a(a.a(), new -..Lambda.p.fhcQ1lmar-c5o4pncGesNRAI8_g(this, null));
  }
  
  public final boolean d()
  {
    return (!p()) || (g());
  }
  
  public final long e()
  {
    return Settings.d("premiumTimestamp").longValue() + q();
  }
  
  public final String f()
  {
    return d.a("profileEmail");
  }
  
  public final boolean g()
  {
    return (p()) && (!a(Settings.d("premiumGraceExpiration").longValue()));
  }
  
  public final long h()
  {
    return Settings.d("premiumGraceExpiration").longValue();
  }
  
  public final int i()
  {
    return Settings.d("premiumRequests").intValue();
  }
  
  public final boolean j()
  {
    return d.e("premiumLastFetchDate");
  }
  
  public final String k()
  {
    String str2 = Settings.a("premiumLevel", "none");
    String str1 = str2;
    if (d())
    {
      str1 = str2;
      if (str2.equals("none")) {
        str1 = "regular";
      }
    }
    return str1;
  }
  
  public final boolean l()
  {
    return !Settings.i();
  }
  
  public final boolean m()
  {
    return am.b(d.a("subscriptionStatus"), "hold");
  }
  
  public final boolean n()
  {
    return (am.b(d.a("subscriptionStatus"), "InActive")) && (am.b(d.a("subscriptionStatusChangedReason"), "SUBSCRIPTION_CANCELED"));
  }
  
  public final boolean o()
  {
    return (m()) || (n());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */