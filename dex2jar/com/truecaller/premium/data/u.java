package com.truecaller.premium.data;

import c.d.f;
import com.truecaller.common.f.c;
import javax.inject.Provider;

public final class u
  implements dagger.a.d<t>
{
  private final Provider<c> a;
  private final Provider<m> b;
  private final Provider<com.truecaller.premium.a.d> c;
  private final Provider<f> d;
  private final Provider<f> e;
  
  private u(Provider<c> paramProvider, Provider<m> paramProvider1, Provider<com.truecaller.premium.a.d> paramProvider2, Provider<f> paramProvider3, Provider<f> paramProvider4)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static u a(Provider<c> paramProvider, Provider<m> paramProvider1, Provider<com.truecaller.premium.a.d> paramProvider2, Provider<f> paramProvider3, Provider<f> paramProvider4)
  {
    return new u(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.u
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */