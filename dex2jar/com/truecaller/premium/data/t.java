package com.truecaller.premium.data;

import c.d.f;
import c.g.a.b;
import c.g.b.k;
import c.x;
import com.truecaller.premium.a.d;
import com.truecaller.premium.bz;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.g;

public final class t
  implements s
{
  public static final t.a b = new t.a((byte)0);
  final f a;
  private List<? extends bz> c;
  private String d;
  private af e;
  private final com.truecaller.common.f.c f;
  private final m g;
  private final d h;
  private final f i;
  
  @Inject
  public t(com.truecaller.common.f.c paramc, m paramm, d paramd, @Named("Async") f paramf1, @Named("UI") f paramf2)
  {
    f = paramc;
    g = paramm;
    h = paramd;
    i = paramf1;
    a = paramf2;
  }
  
  private static af b(Integer paramInteger, af paramaf)
  {
    if (paramInteger == null) {
      return (af)af.h.a;
    }
    if (paramInteger.intValue() == -1) {
      return (af)af.e.a;
    }
    if (paramInteger.intValue() == -2) {
      return (af)af.h.a;
    }
    paramInteger = paramaf;
    if (paramaf == null) {
      paramInteger = (af)af.h.a;
    }
    return paramInteger;
  }
  
  public final Object a(c.d.c<? super af> paramc)
  {
    return g.a(i, (c.g.a.m)new t.c(this, null), paramc);
  }
  
  public final Object a(b<? super com.truecaller.common.f.a, Boolean> paramb, c.d.c<? super x> paramc)
  {
    return g.a(a, (c.g.a.m)new t.d(this, paramb, null), paramc);
  }
  
  public final Object a(s.a parama, String paramString, c.g.a.a<x> parama1, c.d.c<? super aa> paramc)
  {
    return g.a(a, (c.g.a.m)new t.e(this, parama, paramString, parama1, null), paramc);
  }
  
  public final boolean a()
  {
    return e != null;
  }
  
  public final void b()
  {
    e = null;
    Object localObject = c;
    if (localObject != null)
    {
      localObject = ((Iterable)localObject).iterator();
      while (((Iterator)localObject).hasNext())
      {
        bz localbz = (bz)((Iterator)localObject).next();
        f.a(b, c, null);
        Date localDate = org.c.a.a.a.d.a.a(new Date(e));
        k.a(localDate, "DateUtils.addDays(Date(i…DURATION_PREMIUM_01_DAYS)");
        long l = localDate.getTime();
        if ((k.a(a, "premium_01")) && (f.a(l))) {
          h.a(localbz);
        }
      }
    }
    h.d();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.t
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */