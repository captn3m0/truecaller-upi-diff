package com.truecaller.premium.data;

import android.support.v4.f.j;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.network.util.h;
import e.b;
import e.r;
import java.io.IOException;
import java.net.UnknownHostException;
import okhttp3.ac;
import okhttp3.ad;

public final class i
  implements g
{
  public final com.truecaller.androidactors.w<j<Boolean, z>> a()
  {
    Object localObject = ((q)h.a(KnownEndpoints.PREMIUM, q.class)).a();
    try
    {
      localObject = ((b)localObject).c();
      if ((a.c()) && (b != null))
      {
        localObject = (z)b;
        localObject = com.truecaller.androidactors.w.b(j.a(Boolean.TRUE, localObject));
        return (com.truecaller.androidactors.w<j<Boolean, z>>)localObject;
      }
      return com.truecaller.androidactors.w.b(j.a(Boolean.TRUE, null));
    }
    catch (IOException localIOException)
    {
      boolean bool;
      if (!(localIOException instanceof UnknownHostException)) {
        bool = true;
      } else {
        bool = false;
      }
      return com.truecaller.androidactors.w.b(j.a(Boolean.valueOf(bool), null));
    }
  }
  
  public final com.truecaller.androidactors.w<j<Boolean, y>> a(String paramString)
  {
    paramString = ((q)h.a(KnownEndpoints.PREMIUM, q.class)).a(paramString);
    try
    {
      paramString = paramString.c();
      if ((a.c()) && (b != null))
      {
        paramString = (y)b;
        paramString = com.truecaller.androidactors.w.b(j.a(Boolean.TRUE, paramString));
        return paramString;
      }
      return com.truecaller.androidactors.w.b(j.a(Boolean.TRUE, null));
    }
    catch (IOException paramString)
    {
      boolean bool;
      if (!(paramString instanceof UnknownHostException)) {
        bool = true;
      } else {
        bool = false;
      }
      return com.truecaller.androidactors.w.b(j.a(Boolean.valueOf(bool), null));
    }
  }
  
  public final com.truecaller.androidactors.w<j<Boolean, z>> a(String paramString1, String paramString2)
  {
    paramString1 = ac.a(okhttp3.w.b("text/plain"), paramString1);
    paramString1 = ((q)h.a(KnownEndpoints.PREMIUM, q.class)).a(paramString1, paramString2);
    try
    {
      paramString1 = paramString1.c();
      if ((a.c()) && (b != null))
      {
        paramString1 = (z)b;
        paramString1 = com.truecaller.androidactors.w.b(j.a(Boolean.TRUE, paramString1));
        return paramString1;
      }
      return com.truecaller.androidactors.w.b(j.a(Boolean.TRUE, null));
    }
    catch (IOException paramString1)
    {
      boolean bool;
      if (!(paramString1 instanceof UnknownHostException)) {
        bool = true;
      } else {
        bool = false;
      }
      return com.truecaller.androidactors.w.b(j.a(Boolean.valueOf(bool), null));
    }
  }
  
  public final com.truecaller.androidactors.w<j<Boolean, z>> b(String paramString1, String paramString2)
  {
    paramString1 = ac.a(okhttp3.w.b("text/plain"), paramString1);
    paramString1 = ((q)h.a(KnownEndpoints.PREMIUM, q.class)).b(paramString1, paramString2);
    try
    {
      paramString1 = paramString1.c();
      if ((a.c()) && (b != null))
      {
        paramString1 = (z)b;
        paramString1 = com.truecaller.androidactors.w.b(j.a(Boolean.TRUE, paramString1));
        return paramString1;
      }
      return com.truecaller.androidactors.w.b(j.a(Boolean.TRUE, null));
    }
    catch (IOException paramString1)
    {
      boolean bool;
      if (!(paramString1 instanceof UnknownHostException)) {
        bool = true;
      } else {
        bool = false;
      }
      return com.truecaller.androidactors.w.b(j.a(Boolean.valueOf(bool), null));
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */