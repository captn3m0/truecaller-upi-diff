package com.truecaller.premium.data;

import android.content.Context;
import com.truecaller.common.h.an;
import dagger.a.d;
import javax.inject.Provider;

public final class l
  implements d<k>
{
  private final Provider<Context> a;
  private final Provider<an> b;
  
  private l(Provider<Context> paramProvider, Provider<an> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static l a(Provider<Context> paramProvider, Provider<an> paramProvider1)
  {
    return new l(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */