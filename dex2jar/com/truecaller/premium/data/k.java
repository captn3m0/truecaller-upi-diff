package com.truecaller.premium.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.google.gson.f;
import com.truecaller.common.h.an;
import javax.inject.Inject;

public final class k
  implements j
{
  private final SharedPreferences a;
  private final f b;
  private final an c;
  
  @Inject
  public k(Context paramContext, an paraman)
  {
    c = paraman;
    a = paramContext.getSharedPreferences("premium_products_cache", 0);
    b = new f();
  }
  
  public final y a()
  {
    boolean bool = a.contains("cache_ttl");
    int j = 0;
    int i = j;
    if (bool)
    {
      i = j;
      if (a.contains("last_timestamp")) {
        if (!a.contains("dto"))
        {
          i = j;
        }
        else
        {
          i = j;
          if (!c.a(a.getLong("last_timestamp", 0L), a.getLong("cache_ttl", 0L))) {
            i = 1;
          }
        }
      }
    }
    if (i == 0) {
      return null;
    }
    String str = a.getString("dto", null);
    if (str == null) {
      return null;
    }
    return (y)b.a(str, y.class);
  }
  
  public final void a(y paramy)
  {
    c.g.b.k.b(paramy, "dto");
    a.edit().putLong("last_timestamp", System.currentTimeMillis()).putLong("cache_ttl", c).putString("dto", b.b(paramy)).apply();
  }
  
  public final void b()
  {
    a.edit().remove("last_timestamp").remove("cache_ttl").remove("dto").apply();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */