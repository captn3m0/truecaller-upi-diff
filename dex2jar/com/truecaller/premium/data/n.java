package com.truecaller.premium.data;

import android.content.Context;
import android.net.Uri;
import c.u;
import com.google.common.collect.ImmutableList;
import com.truecaller.calling.recorder.h;
import com.truecaller.common.h.am;
import com.truecaller.data.entity.Number;
import com.truecaller.util.at;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import javax.inject.Inject;
import javax.inject.Named;
import org.json.JSONException;
import org.json.JSONObject;

public final class n
  implements m
{
  final com.truecaller.androidactors.f<g> a;
  final j b;
  private final Context c;
  private final com.truecaller.util.b.j d;
  private final com.truecaller.whoviewedme.w e;
  private final h f;
  private final com.truecaller.common.f.c g;
  private final com.truecaller.common.g.a h;
  private final com.truecaller.data.entity.g i;
  private final c j;
  private final c.d.f k;
  
  @Inject
  public n(Context paramContext, com.truecaller.androidactors.f<g> paramf, com.truecaller.util.b.j paramj, com.truecaller.whoviewedme.w paramw, h paramh, com.truecaller.common.f.c paramc, com.truecaller.common.g.a parama, com.truecaller.data.entity.g paramg, j paramj1, c paramc1, @Named("IO") c.d.f paramf1)
  {
    c = paramContext;
    a = paramf;
    d = paramj;
    e = paramw;
    f = paramh;
    g = paramc;
    h = parama;
    i = paramg;
    b = paramj1;
    j = paramc1;
    k = paramf1;
  }
  
  private static String a(long paramLong)
  {
    Object localObject = new Date(paramLong);
    localObject = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format((Date)localObject);
    c.g.b.k.a(localObject, "SimpleDateFormat(\"yyyy-M…etDefault()).format(date)");
    return (String)localObject;
  }
  
  private final String b()
  {
    if (g.g())
    {
      str = a(g.h());
      return c.getString(2131886819, new Object[] { str });
    }
    int m;
    if (g.a() == 1) {
      m = 1;
    } else {
      m = 0;
    }
    String str = a(g.e());
    Context localContext = c;
    if (m != 0) {
      m = 2131886820;
    } else {
      m = 2131886818;
    }
    return localContext.getString(m, new Object[] { str });
  }
  
  private final String c()
  {
    StringBuilder localStringBuilder = new StringBuilder("android.resource://");
    localStringBuilder.append(c.getPackageName());
    localStringBuilder.append("/2131234702");
    return localStringBuilder.toString();
  }
  
  private final String d()
  {
    StringBuilder localStringBuilder = new StringBuilder("android.resource://");
    localStringBuilder.append(c.getPackageName());
    localStringBuilder.append("/2131234701");
    return localStringBuilder.toString();
  }
  
  private final List<e> e()
  {
    Object localObject1 = new ArrayList();
    if (f.a()) {
      ((ArrayList)localObject1).add(new e("premiumCallRecording", 2131886827, 2131233933, 2131886848, c.a.m.a(Integer.valueOf(2131886837)), 2131234396));
    }
    if (e.a())
    {
      ((ArrayList)localObject1).add(new e("premiumWhoViewedMe", 2131886853, 2131234420, 2131886852, c.a.m.a(Integer.valueOf(2131886843)), 2131234401));
      ((ArrayList)localObject1).add(new e("premiumIncognitoMode", 2131886844, 2131234204, 2131886850, c.a.m.a(Integer.valueOf(2131886840)), 2131234398));
    }
    ((ArrayList)localObject1).add(new e("premiumNoAds", 2131886845, 2131234303, 2131886851, c.a.m.a(Integer.valueOf(2131886841)), 2131234399));
    ((ArrayList)localObject1).add(new e("premiumBadge", 2131886826, 2131234301, 2131886847, c.a.m.a(Integer.valueOf(2131886830)), 2131234400));
    if (d.b()) {
      ((ArrayList)localObject1).add(new e("premiumContactsRequests", 2131886828, 2131234009, 2131886849, c.a.m.a(Integer.valueOf(2131886838)), 2131234397));
    }
    Object localObject2 = (Iterable)j.a();
    c.g.b.k.b(localObject2, "receiver$0");
    if ((((Collection)localObject2).isEmpty() ^ true))
    {
      localObject2 = (Collection)localObject1;
      Object localObject3 = (Iterable)j.a();
      Collection localCollection = (Collection)new ArrayList(c.a.m.a((Iterable)localObject3, 10));
      localObject3 = ((Iterable)localObject3).iterator();
      while (((Iterator)localObject3).hasNext()) {
        localCollection.add(Integer.valueOf(nextb));
      }
      ((Collection)localObject2).add(new e("premiumAdvancedBlocking", 2131886825, 2131234405, 2131886846, (List)localCollection, 2131234395));
    }
    localObject1 = ImmutableList.copyOf((Collection)localObject1);
    c.g.b.k.a(localObject1, "ImmutableList.copyOf(features)");
    return (List)localObject1;
  }
  
  final v a(Boolean paramBoolean1, Boolean paramBoolean2, y paramy)
  {
    Object localObject1 = new ag();
    c.g.b.k.b(paramy, "dto");
    if (!org.c.a.a.a.k.b((CharSequence)b)) {}
    try
    {
      paramy = new JSONObject(b);
      a = paramy.optString("topImage");
      b = paramy.optString("goldTopImage");
    }
    catch (JSONException paramy)
    {
      boolean bool;
      int m;
      Object localObject2;
      String str1;
      Uri localUri;
      Collection localCollection;
      List localList;
      String str2;
      int n;
      String str3;
      String str4;
      String str5;
      CharSequence localCharSequence;
      label831:
      for (;;) {}
    }
    if (paramBoolean1 != null)
    {
      bool = paramBoolean1.booleanValue();
      if (bool) {
        m = 2131886908;
      } else {
        m = 2131886907;
      }
      if (bool) {
        paramBoolean1 = b();
      } else {
        paramBoolean1 = c.getString(2131886752);
      }
      paramy = new f(m, paramBoolean1);
      if (bool) {
        m = 2131886822;
      } else {
        m = 2131886821;
      }
      if (bool) {
        paramBoolean1 = d();
      } else {
        paramBoolean1 = (String)org.c.a.a.a.k.e((CharSequence)a, (CharSequence)d());
      }
      c.g.b.k.a(paramBoolean1, "topImageUri");
      localObject2 = c.getString(2131886909);
      c.g.b.k.a(localObject2, "context.getString(R.string.PremiumToolbarTitle)");
      paramBoolean1 = Uri.parse(paramBoolean1);
      c.g.b.k.a(paramBoolean1, "Uri.parse(topImage)");
      paramBoolean1 = new w(paramy, m, 2131234403, (String)localObject2, paramBoolean1, e(), android.support.v4.content.b.c(c, 2131100401));
    }
    else
    {
      paramBoolean1 = null;
    }
    if (paramBoolean2 != null)
    {
      bool = paramBoolean2.booleanValue();
      if (bool) {
        m = 2131886868;
      } else {
        m = 2131886867;
      }
      if (bool) {
        paramBoolean2 = b();
      } else {
        paramBoolean2 = c.getString(2131886752);
      }
      localObject2 = new f(m, paramBoolean2);
      if (bool) {
        m = 2131886856;
      } else {
        m = 2131886855;
      }
      if (bool) {
        paramBoolean2 = c();
      } else {
        paramBoolean2 = (String)org.c.a.a.a.k.e((CharSequence)b, (CharSequence)c());
      }
      c.g.b.k.a(paramBoolean2, "topImageUri");
      str1 = c.getString(2131886869);
      c.g.b.k.a(str1, "context.getString(R.stri….PremiumGoldToolbarTitle)");
      localUri = Uri.parse(paramBoolean2);
      c.g.b.k.a(localUri, "Uri.parse(topImage)");
      localCollection = (Collection)new ArrayList();
      localList = c.a.m.a(Integer.valueOf(2131886859));
      paramBoolean2 = c.getApplicationContext();
      if (paramBoolean2 != null)
      {
        if (!((com.truecaller.common.b.a)paramBoolean2).p())
        {
          paramBoolean2 = null;
        }
        else
        {
          paramy = h.a("profileAvatar");
          str2 = com.truecaller.profile.c.a(h);
          if (str2 != null)
          {
            localObject1 = new Number(str2);
            paramBoolean2 = ((Number)localObject1).g();
            localObject1 = i.a((Number)localObject1);
            c.g.b.k.a(localObject1, "numberProvider.getTypeForDisplay(number)");
            paramBoolean2 = am.a(" - ", new CharSequence[] { (CharSequence)localObject1, (CharSequence)paramBoolean2 });
          }
          else
          {
            paramBoolean2 = null;
          }
          localObject1 = (CharSequence)paramy;
          if ((localObject1 != null) && (((CharSequence)localObject1).length() != 0)) {
            n = 0;
          } else {
            n = 1;
          }
          if (n == 0) {
            paramy = Uri.parse(paramy);
          } else {
            paramy = null;
          }
          str3 = com.truecaller.profile.c.b(h);
          localObject1 = h.a("profileStreet");
          str4 = h.a("profileZip");
          str5 = h.a("profileCity");
          localCharSequence = (CharSequence)localObject1;
          if ((localCharSequence != null) && (localCharSequence.length() != 0)) {
            n = 0;
          } else {
            n = 1;
          }
          if (n != 0)
          {
            localCharSequence = (CharSequence)str4;
            if ((localCharSequence != null) && (localCharSequence.length() != 0)) {
              n = 0;
            } else {
              n = 1;
            }
            if (n != 0)
            {
              localCharSequence = (CharSequence)str5;
              if ((localCharSequence != null) && (localCharSequence.length() != 0)) {
                n = 0;
              } else {
                n = 1;
              }
              if (n != 0)
              {
                localObject1 = null;
                break label831;
              }
            }
          }
          localObject1 = am.a(new String[] { localObject1, am.a(" ", new CharSequence[] { (CharSequence)str4, (CharSequence)str5 }) });
          paramBoolean2 = new a(paramy, str3, (String)localObject1, at.a((CharSequence)str2), paramBoolean2);
        }
        localCollection.add(new e("goldCallerId", 2131886861, 2131234165, 2131886864, localList, 2131234048, paramBoolean2));
        localCollection.add(new e("goldAllPremium", 2131886857, 2131233824, 2131886863, c.a.m.a(Integer.valueOf(2131886858)), 2131234047));
        localCollection.add(new e("goldSupport", 2131886862, 2131234409, 2131886865, c.a.m.a(Integer.valueOf(2131886860)), 2131234049));
        paramBoolean2 = ImmutableList.copyOf(localCollection);
        c.g.b.k.a(paramBoolean2, "ImmutableList.copyOf(features)");
        paramBoolean2 = new w((f)localObject2, m, 2131234166, str1, localUri, (List)paramBoolean2, android.support.v4.content.b.c(c, 2131100399));
      }
      else
      {
        throw new u("null cannot be cast to non-null type com.truecaller.common.app.ApplicationBase");
      }
    }
    else
    {
      paramBoolean2 = null;
    }
    return new v(paramBoolean1, paramBoolean2);
  }
  
  public final Object a(Boolean paramBoolean1, Boolean paramBoolean2, c.d.c<? super m.b> paramc)
  {
    return kotlinx.coroutines.g.a(k, (c.g.a.m)new n.a(this, paramBoolean1, paramBoolean2, null), paramc);
  }
  
  public final void a()
  {
    b.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.data.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */