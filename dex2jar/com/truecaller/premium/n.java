package com.truecaller.premium;

import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import javax.inject.Inject;

public final class n
  implements m
{
  private final b a;
  
  @Inject
  public n(b paramb)
  {
    a = paramb;
  }
  
  public final void a(PremiumPresenterView.LaunchContext paramLaunchContext, cb paramcb)
  {
    k.b(paramLaunchContext, "launchContext");
    k.b(paramcb, "subscription");
    paramLaunchContext = new e.a("fb_mobile_initiated_checkout").a(Double.valueOf(by.a(paramcb)));
    if (c != null) {
      paramLaunchContext.a("fb_currency", c);
    }
    paramcb = a;
    if (paramcb != null)
    {
      paramLaunchContext.a("fb_content_id", paramcb);
      paramLaunchContext.a("fb_content_type", paramcb);
      paramLaunchContext.a("fb_content", paramcb);
    }
    paramcb = a;
    paramLaunchContext = paramLaunchContext.a();
    k.a(paramLaunchContext, "builder.build()");
    paramcb.b(paramLaunchContext);
  }
  
  public final void b(PremiumPresenterView.LaunchContext paramLaunchContext, cb paramcb)
  {
    k.b(paramLaunchContext, "launchContext");
    k.b(paramcb, "subscription");
    a.a(by.a(paramcb), c);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */