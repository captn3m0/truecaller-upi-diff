package com.truecaller.premium.b;

import com.truecaller.common.f.c;
import com.truecaller.common.h.am;
import com.truecaller.data.entity.Address;
import com.truecaller.data.entity.Contact;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.e.a;
import javax.inject.Inject;

public final class a
{
  public final e a;
  private final c b;
  
  @Inject
  public a(c paramc, e parame)
  {
    b = paramc;
    a = parame;
  }
  
  public final boolean a(Contact paramContact)
  {
    e locale = a;
    return (u.a(locale, e.a[72]).a()) && (b(paramContact, true));
  }
  
  public final boolean a(Contact paramContact, boolean paramBoolean)
  {
    Object localObject = a;
    if (v.a((e)localObject, e.a[73]).a())
    {
      if (paramContact != null)
      {
        localObject = paramContact.g();
        if (localObject != null)
        {
          localObject = ((Address)localObject).getStreet();
          break label50;
        }
      }
      localObject = null;
      label50:
      if ((!am.b((CharSequence)localObject)) && (b(paramContact, paramBoolean))) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean b(Contact paramContact)
  {
    e locale = a;
    return (w.a(locale, e.a[74]).a()) && (b(paramContact, true));
  }
  
  public final boolean b(Contact paramContact, boolean paramBoolean)
  {
    if (paramContact == null) {
      return false;
    }
    if ((paramBoolean) && (paramContact.Z())) {
      return false;
    }
    if (!paramContact.N())
    {
      if (b.d()) {
        return false;
      }
      if (paramContact.L()) {
        return a.X().a();
      }
      return a.W().a();
    }
    return false;
  }
  
  public final boolean c(Contact paramContact)
  {
    e locale = a;
    return (z.a(locale, e.a[77]).a()) && (b(paramContact, true));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.b.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */