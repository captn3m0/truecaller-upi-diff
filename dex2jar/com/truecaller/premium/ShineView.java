package com.truecaller.premium;

import android.arch.lifecycle.e;
import android.arch.lifecycle.e.a;
import android.arch.lifecycle.e.b;
import android.arch.lifecycle.g;
import android.arch.lifecycle.h;
import android.arch.lifecycle.q;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.AttributeSet;
import android.view.View;
import c.g.b.k;
import c.l.d;
import c.u;
import c.x;
import com.truecaller.utils.extensions.t;

public final class ShineView
  extends View
  implements g
{
  private float a;
  private float b;
  private final Paint c = new Paint();
  private final RectF d = new RectF();
  private final int e;
  private final int f;
  private final Matrix g;
  private Shader h;
  private boolean i;
  private final ca j;
  private final d<x> k;
  private h l;
  
  public ShineView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private ShineView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    e = android.support.v4.content.b.c(paramContext, 2131100390);
    f = android.support.v4.content.b.c(paramContext, 2131100391);
    g = new Matrix();
    paramContext = paramContext.getSystemService("sensor");
    if (paramContext != null)
    {
      j = new ca((SensorManager)paramContext);
      k = new ShineView.a((ShineView)this);
      setLayerType(2, null);
      return;
    }
    throw new u("null cannot be cast to non-null type android.hardware.SensorManager");
  }
  
  private final void setRotationData(ca.a parama)
  {
    i = true;
    a = (a + 0.5F);
    b = b;
    invalidate();
  }
  
  @q(a=e.a.ON_RESUME)
  private final void subscribeSensorData()
  {
    if (t.d(this))
    {
      Object localObject = l;
      if (localObject != null)
      {
        localObject = ((h)localObject).getLifecycle();
        if (localObject != null)
        {
          localObject = ((e)localObject).a();
          if (localObject != null)
          {
            if (((e.b)localObject).a(e.b.e) != true) {
              return;
            }
            localObject = j;
            c.g.a.b localb = (c.g.a.b)k;
            k.b(localb, "subscriber");
            if (a != null) {
              return;
            }
            Sensor localSensor1 = b.getDefaultSensor(9);
            Sensor localSensor2 = b.getDefaultSensor(1);
            Sensor localSensor3 = b.getDefaultSensor(2);
            int m;
            if ((localSensor3 != null) && ((localSensor1 != null) || (localSensor2 != null))) {
              m = 1;
            } else {
              m = 0;
            }
            if (m == 0) {
              return;
            }
            a = new ca.b(localb);
            if (localSensor1 != null) {
              b.registerListener((SensorEventListener)a, localSensor1, 1);
            }
            if ((localSensor1 == null) && (localSensor2 != null)) {
              b.registerListener((SensorEventListener)a, localSensor2, 1);
            }
            b.registerListener((SensorEventListener)a, localSensor3, 1);
            return;
          }
        }
      }
      return;
    }
  }
  
  @q(a=e.a.ON_PAUSE)
  private final void unsubscribeSensorData()
  {
    i = false;
    ca localca = j;
    b.unregisterListener((SensorEventListener)a);
    a = null;
  }
  
  public final h getLifecycleOwner()
  {
    return l;
  }
  
  protected final void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    subscribeSensorData();
  }
  
  protected final void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    unsubscribeSensorData();
  }
  
  protected final void onDraw(Canvas paramCanvas)
  {
    k.b(paramCanvas, "canvas");
    if (t.d(this))
    {
      if (!i) {
        return;
      }
      if (h == null)
      {
        float f1 = getMeasuredWidth() * 2.0F;
        float f2 = getMeasuredHeight();
        float f3 = -f1;
        double d1 = getMeasuredHeight();
        Double.isNaN(d1);
        float f4 = (float)(d1 * 1.5D);
        m = f;
        n = e;
        localObject = Shader.TileMode.CLAMP;
        h = ((Shader)new LinearGradient(f3, f4, f3 + f1, f4 - f2 * 2.0F, new int[] { m, n, m }, new float[] { 0.3F, 0.5F, 0.7F }, (Shader.TileMode)localObject));
      }
      int m = getMeasuredWidth();
      int n = getMeasuredHeight();
      g.setTranslate(m * 3 * a, n * 2 * b);
      Object localObject = h;
      if (localObject != null) {
        ((Shader)localObject).setLocalMatrix(g);
      }
      c.setShader(h);
      localObject = d;
      left = 0.0F;
      top = 0.0F;
      bottom = getMeasuredHeight();
      d.right = getMeasuredWidth();
      paramCanvas.drawRect(d, c);
      return;
    }
  }
  
  public final void setLifecycleOwner(h paramh)
  {
    if (l == null)
    {
      l = paramh;
      paramh = l;
      if (paramh != null)
      {
        paramh = paramh.getLifecycle();
        if (paramh != null)
        {
          paramh.a((g)this);
          return;
        }
      }
    }
  }
  
  public final void setVisibility(int paramInt)
  {
    super.setVisibility(paramInt);
    if (t.d(this))
    {
      subscribeSensorData();
      return;
    }
    unsubscribeSensorData();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.ShineView
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */