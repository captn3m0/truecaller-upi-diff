package com.truecaller.premium.a;

import android.app.Activity;
import android.content.Context;
import c.a.y;
import c.g.b.k;
import com.android.billingclient.api.e;
import com.android.billingclient.api.e.a;
import com.android.billingclient.api.h;
import com.android.billingclient.api.i;
import com.truecaller.premium.bz;
import com.truecaller.premium.bz.a;
import com.truecaller.premium.cb;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;

public final class b
  implements i, d
{
  private com.android.billingclient.api.b a;
  private d.b b;
  private final Context c;
  private final c.d.f d;
  
  @Inject
  public b(Context paramContext, @Named("UI") c.d.f paramf)
  {
    c = paramContext;
    d = paramf;
  }
  
  static bz a(h paramh)
  {
    if (paramh == null)
    {
      paramh = new bz.a().a();
      k.a(paramh, "Receipt.Builder().build()");
      return paramh;
    }
    paramh = new bz.a().a(paramh.a()).b(paramh.d()).c(paramh.e()).d("subs").a(paramh.b()).a();
    k.a(paramh, "Receipt.Builder()\n      …ime)\n            .build()");
    return paramh;
  }
  
  static boolean a(Integer paramInteger)
  {
    return (paramInteger != null) && (paramInteger.intValue() == 0);
  }
  
  public final List<cb> a(List<String> paramList)
  {
    k.b(paramList, "productIds");
    if (!a()) {
      return (List)y.a;
    }
    return (List)kotlinx.coroutines.f.a(d, (c.g.a.m)new b.a(this, paramList, null));
  }
  
  public final void a(int paramInt, List<h> paramList)
  {
    Object localObject = paramList;
    if (paramList == null) {
      localObject = (List)y.a;
    }
    paramList = (h)c.a.m.e((List)localObject);
    localObject = b;
    if (localObject != null)
    {
      switch (paramInt)
      {
      default: 
        paramInt = 1;
        break;
      case 1: 
        paramInt = 2;
        break;
      case 0: 
        paramInt = 0;
      }
      ((d.b)localObject).a(paramInt, a(paramList));
    }
    b = null;
  }
  
  public final void a(Activity paramActivity, String paramString1, String paramString2, d.b paramb)
  {
    k.b(paramActivity, "activity");
    k.b(paramString1, "sku");
    k.b(paramb, "listener");
    if (!a()) {
      return;
    }
    b = paramb;
    if (!a(Integer.valueOf(c().a(paramActivity, e.a().a(paramString1).c(paramString2).a().b("subs").b())))) {
      b = null;
    }
  }
  
  public final void a(bz parambz)
  {
    k.b(parambz, "receipt");
    if (!a()) {
      return;
    }
    com.android.billingclient.api.b localb = c();
    parambz = new h(b, c).c();
    k.a(parambz, "receipt.purchaseToken");
    k.b(localb, "receiver$0");
    k.b(parambz, "token");
    localb.a(parambz, (com.android.billingclient.api.f)a.b.a);
  }
  
  public final boolean a()
  {
    com.android.billingclient.api.b localb = a;
    if (localb != null) {
      return localb.a();
    }
    return false;
  }
  
  public final d.a b()
  {
    return (d.a)kotlinx.coroutines.f.a(d, (c.g.a.m)new b.b(this, null));
  }
  
  final com.android.billingclient.api.b c()
  {
    com.android.billingclient.api.b localb2 = a;
    com.android.billingclient.api.b localb1 = localb2;
    if (localb2 == null)
    {
      localb1 = com.android.billingclient.api.b.a(c).a((i)this).a();
      a = localb1;
      k.a(localb1, "BillingClient\n        .n… { billingClient = this }");
    }
    return localb1;
  }
  
  public final void d()
  {
    com.android.billingclient.api.b localb = a;
    if (localb != null) {
      localb.b();
    }
    a = null;
    b = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */