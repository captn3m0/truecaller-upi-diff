package com.truecaller.premium.a;

import android.app.Activity;
import com.truecaller.premium.bz;
import com.truecaller.premium.cb;
import java.util.List;

public abstract interface d
{
  public abstract List<cb> a(List<String> paramList);
  
  public abstract void a(Activity paramActivity, String paramString1, String paramString2, d.b paramb);
  
  public abstract void a(bz parambz);
  
  public abstract boolean a();
  
  public abstract d.a b();
  
  public abstract void d();
}

/* Location:
 * Qualified Name:     com.truecaller.premium.a.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */