package com.truecaller.premium.a;

import android.content.Context;
import c.d.f;
import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d<b>
{
  private final Provider<Context> a;
  private final Provider<f> b;
  
  private c(Provider<Context> paramProvider, Provider<f> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static c a(Provider<Context> paramProvider, Provider<f> paramProvider1)
  {
    return new c(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */