package com.truecaller.premium;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.f.j;
import c.g.b.k;
import c.u;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.b.a;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.background.e;
import com.truecaller.common.background.e.a;
import com.truecaller.common.f.c;
import com.truecaller.old.data.access.Settings;
import com.truecaller.premium.a.d;
import com.truecaller.premium.a.d.a;
import com.truecaller.premium.data.m.b;
import com.truecaller.premium.data.m.b.b;
import com.truecaller.premium.data.x;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import kotlinx.coroutines.f;

public final class PremiumStatusRecurringTask
  extends PersistentBackgroundTask
{
  @Inject
  public c a;
  @Inject
  public com.truecaller.premium.data.m b;
  @Inject
  public d c;
  
  public PremiumStatusRecurringTask()
  {
    TrueApp localTrueApp = TrueApp.y();
    k.a(localTrueApp, "TrueApp.getApp()");
    localTrueApp.a().a(this);
  }
  
  public final int a()
  {
    return 10009;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    k.b(paramContext, "serviceContext");
    paramContext = a;
    if (paramContext == null) {
      k.a("premiumRepository");
    }
    if (!paramContext.d())
    {
      paramContext = a;
      if (paramContext == null) {
        k.a("premiumRepository");
      }
      if ((paramContext.j()) && (Settings.a("premiumLevel")))
      {
        i = 0;
        break label125;
      }
    }
    paramContext = a;
    if (paramContext == null) {
      k.a("premiumRepository");
    }
    paramContext = paramContext.b();
    if (paramContext != null)
    {
      paramContext = (Integer)a;
      if ((paramContext != null) && (paramContext.intValue() == 0))
      {
        i = 0;
        break label125;
      }
    }
    int i = 1;
    label125:
    paramContext = a;
    if (paramContext == null) {
      k.a("premiumRepository");
    }
    int j = i;
    if (!paramContext.d())
    {
      paramContext = c;
      if (paramContext == null) {
        k.a("inAppBilling");
      }
      paramBundle = paramContext.b();
      if ((i == 0) && (a)) {
        i = 0;
      } else {
        i = 1;
      }
      paramContext = (m.b)f.a((c.g.a.m)new PremiumStatusRecurringTask.a(this, null));
      if ((i == 0) && ((paramContext instanceof m.b.b))) {
        i = 0;
      } else {
        i = 1;
      }
      if ((a) && ((paramContext instanceof m.b.b)))
      {
        paramBundle = c;
        if (paramBundle == null) {
          k.a("inAppBilling");
        }
        paramBundle.a(a.a());
      }
      paramContext = c;
      if (paramContext == null) {
        k.a("inAppBilling");
      }
      paramContext.d();
      j = i;
    }
    if (j != 0) {
      return PersistentBackgroundTask.RunResult.FailedRetry;
    }
    return PersistentBackgroundTask.RunResult.Success;
  }
  
  public final boolean a(Context paramContext)
  {
    k.b(paramContext, "serviceContext");
    paramContext = paramContext.getApplicationContext();
    if (paramContext != null) {
      return ((a)paramContext).p();
    }
    throw new u("null cannot be cast to non-null type com.truecaller.common.app.ApplicationBase");
  }
  
  public final e b()
  {
    e locale = new e.a(1).a(6L, TimeUnit.HOURS).b(2L, TimeUnit.HOURS).c(1L, TimeUnit.HOURS).a(1).b();
    k.a(locale, "TaskConfiguration.Builde…ANY)\n            .build()");
    return locale;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.PremiumStatusRecurringTask
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */