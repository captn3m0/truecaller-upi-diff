package com.truecaller.premium;

import com.truecaller.analytics.b;
import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d<n>
{
  private final Provider<b> a;
  
  private o(Provider<b> paramProvider)
  {
    a = paramProvider;
  }
  
  public static o a(Provider<b> paramProvider)
  {
    return new o(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.premium.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */