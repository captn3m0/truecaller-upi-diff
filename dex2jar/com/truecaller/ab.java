package com.truecaller;

import android.content.Context;
import com.truecaller.network.search.e;
import dagger.a.d;
import javax.inject.Provider;

public final class ab
  implements d<e>
{
  private final c a;
  private final Provider<Context> b;
  
  private ab(c paramc, Provider<Context> paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static ab a(c paramc, Provider<Context> paramProvider)
  {
    return new ab(paramc, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ab
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */