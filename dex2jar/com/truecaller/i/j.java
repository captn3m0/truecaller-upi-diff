package com.truecaller.i;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d<c>
{
  private final h a;
  private final Provider<Context> b;
  
  private j(h paramh, Provider<Context> paramProvider)
  {
    a = paramh;
    b = paramProvider;
  }
  
  public static j a(h paramh, Provider<Context> paramProvider)
  {
    return new j(paramh, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.i.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */