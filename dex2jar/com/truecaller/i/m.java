package com.truecaller.i;

import android.content.Context;
import android.content.SharedPreferences;
import c.g.b.k;
import com.truecaller.utils.a.a;

public abstract class m
  extends a
{
  public m(SharedPreferences paramSharedPreferences)
  {
    super(paramSharedPreferences);
  }
  
  public int a()
  {
    throw ((Throwable)new c.m("An operation is not implemented: ".concat(String.valueOf("To support settings migration implement [migrateFrom], [currentVersion] and [name]. Migrations should start with moving values to separate file"))));
  }
  
  public final int a(String paramString, int paramInt)
  {
    k.b(paramString, "key");
    return (int)a(paramString, paramInt);
  }
  
  public void a(int paramInt, Context paramContext)
  {
    k.b(paramContext, "context");
    throw ((Throwable)new c.m("An operation is not implemented: ".concat(String.valueOf("To support settings migration implement [migrateFrom], [currentVersion] and [name]. Migrations should start with moving values to separate file"))));
  }
  
  public String b()
  {
    throw ((Throwable)new c.m("An operation is not implemented: ".concat(String.valueOf("To support settings migration implement [migrateFrom], [currentVersion] and [name]. Migrations should start with moving values to separate file"))));
  }
  
  public final void b(String paramString, int paramInt)
  {
    k.b(paramString, "key");
    b(paramString, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.i.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */