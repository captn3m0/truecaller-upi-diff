package com.truecaller.i;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class k
  implements d<e>
{
  private final h a;
  private final Provider<Context> b;
  
  private k(h paramh, Provider<Context> paramProvider)
  {
    a = paramh;
    b = paramProvider;
  }
  
  public static k a(h paramh, Provider<Context> paramProvider)
  {
    return new k(paramh, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.i.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */