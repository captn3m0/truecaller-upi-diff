package com.truecaller.i;

import com.truecaller.calling.d.q;
import dagger.a.d;
import javax.inject.Provider;

public final class l
  implements d<q>
{
  private final h a;
  private final Provider<c> b;
  
  private l(h paramh, Provider<c> paramProvider)
  {
    a = paramh;
    b = paramProvider;
  }
  
  public static l a(h paramh, Provider<c> paramProvider)
  {
    return new l(paramh, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.i.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */