package com.truecaller.i;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class i
  implements d<a>
{
  private final h a;
  private final Provider<Context> b;
  
  private i(h paramh, Provider<Context> paramProvider)
  {
    a = paramh;
    b = paramProvider;
  }
  
  public static i a(h paramh, Provider<Context> paramProvider)
  {
    return new i(paramh, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.i.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */