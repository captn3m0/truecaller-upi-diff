package com.truecaller;

import com.truecaller.analytics.ae;
import com.truecaller.analytics.b;
import com.truecaller.analytics.w;
import com.truecaller.androidactors.f;
import com.truecaller.utils.a;
import dagger.a.d;
import javax.inject.Provider;

public final class m
  implements d<w>
{
  private final c a;
  private final Provider<a> b;
  private final Provider<f<ae>> c;
  private final Provider<b> d;
  
  private m(c paramc, Provider<a> paramProvider, Provider<f<ae>> paramProvider1, Provider<b> paramProvider2)
  {
    a = paramc;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
  }
  
  public static m a(c paramc, Provider<a> paramProvider, Provider<f<ae>> paramProvider1, Provider<b> paramProvider2)
  {
    return new m(paramc, paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */