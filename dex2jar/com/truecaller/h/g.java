package com.truecaller.h;

import com.truecaller.androidactors.i;
import com.truecaller.androidactors.k;
import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d<i>
{
  private final Provider<k> a;
  
  private g(Provider<k> paramProvider)
  {
    a = paramProvider;
  }
  
  public static g a(Provider<k> paramProvider)
  {
    return new g(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.h.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */