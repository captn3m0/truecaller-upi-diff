package com.truecaller.h;

import c.g.b.k;
import com.truecaller.androidactors.w;
import com.truecaller.h.a.b;
import com.truecaller.utils.l;
import javax.inject.Inject;

public final class e
  implements c
{
  private b a;
  private Boolean b;
  private final a c;
  private final l d;
  
  @Inject
  public e(a parama, l paraml)
  {
    c = parama;
    d = paraml;
  }
  
  private final boolean a(boolean paramBoolean)
  {
    return k.a(b, Boolean.valueOf(paramBoolean)) ^ true;
  }
  
  private b c()
  {
    boolean bool = d.e();
    b localb = a;
    if ((localb != null) && (!a(bool))) {
      return localb;
    }
    localb = c.a();
    a = localb;
    b = Boolean.valueOf(bool);
    return localb;
  }
  
  public final w<Boolean> a()
  {
    Object localObject = c();
    if (((b)localObject).a())
    {
      localObject = w.b(Boolean.FALSE);
      k.a(localObject, "Promise.wrap(false)");
      return (w<Boolean>)localObject;
    }
    ((b)localObject).b();
    localObject = w.b(Boolean.TRUE);
    k.a(localObject, "Promise.wrap(true)");
    return (w<Boolean>)localObject;
  }
  
  public final w<Boolean> b()
  {
    Object localObject = c();
    if (!((b)localObject).a())
    {
      localObject = w.b(Boolean.FALSE);
      k.a(localObject, "Promise.wrap(false)");
      return (w<Boolean>)localObject;
    }
    ((b)localObject).c();
    localObject = w.b(Boolean.TRUE);
    k.a(localObject, "Promise.wrap(true)");
    return (w<Boolean>)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.h.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */