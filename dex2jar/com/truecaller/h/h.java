package com.truecaller.h;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d<f<c>>
{
  private final Provider<c> a;
  private final Provider<i> b;
  
  private h(Provider<c> paramProvider, Provider<i> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static h a(Provider<c> paramProvider, Provider<i> paramProvider1)
  {
    return new h(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.h.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */