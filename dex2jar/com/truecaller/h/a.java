package com.truecaller.h;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.AudioManager;
import android.telecom.TelecomManager;
import c.g.b.k;
import c.u;
import com.truecaller.h.a.b;
import com.truecaller.utils.d;
import javax.inject.Inject;

public final class a
{
  private final dagger.a<AudioManager> a;
  private final Context b;
  private final com.truecaller.i.c c;
  private final d d;
  
  @Inject
  public a(dagger.a<AudioManager> parama, Context paramContext, com.truecaller.i.c paramc, d paramd)
  {
    a = parama;
    b = paramContext;
    c = paramc;
    d = paramd;
  }
  
  @SuppressLint({"NewApi"})
  public final b a()
  {
    if ((d.h() >= 23) && (c.b("hasNativeDialerCallerId")))
    {
      localObject = b.getSystemService("telecom");
      if (localObject != null) {
        return (b)new com.truecaller.h.a.c((TelecomManager)localObject);
      }
      throw new u("null cannot be cast to non-null type android.telecom.TelecomManager");
    }
    Object localObject = a.get();
    k.a(localObject, "audioManager.get()");
    return (b)new com.truecaller.h.a.a((AudioManager)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.h.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */