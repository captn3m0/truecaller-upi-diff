package com.truecaller.h;

import com.truecaller.utils.l;
import dagger.a.d;
import javax.inject.Provider;

public final class f
  implements d<e>
{
  private final Provider<a> a;
  private final Provider<l> b;
  
  private f(Provider<a> paramProvider, Provider<l> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static f a(Provider<a> paramProvider, Provider<l> paramProvider1)
  {
    return new f(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.h.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */