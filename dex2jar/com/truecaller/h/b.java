package com.truecaller.h;

import android.content.Context;
import android.media.AudioManager;
import com.truecaller.i.c;
import javax.inject.Provider;

public final class b
  implements dagger.a.d<a>
{
  private final Provider<AudioManager> a;
  private final Provider<Context> b;
  private final Provider<c> c;
  private final Provider<com.truecaller.utils.d> d;
  
  private b(Provider<AudioManager> paramProvider, Provider<Context> paramProvider1, Provider<c> paramProvider2, Provider<com.truecaller.utils.d> paramProvider3)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static b a(Provider<AudioManager> paramProvider, Provider<Context> paramProvider1, Provider<c> paramProvider2, Provider<com.truecaller.utils.d> paramProvider3)
  {
    return new b(paramProvider, paramProvider1, paramProvider2, paramProvider3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.h.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */