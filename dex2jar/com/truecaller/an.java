package com.truecaller;

import com.truecaller.common.g.a;
import com.truecaller.scanner.l;
import javax.inject.Provider;

public final class an
  implements dagger.a.d<l>
{
  private final c a;
  private final Provider<com.truecaller.utils.d> b;
  private final Provider<a> c;
  
  private an(c paramc, Provider<com.truecaller.utils.d> paramProvider, Provider<a> paramProvider1)
  {
    a = paramc;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static an a(c paramc, Provider<com.truecaller.utils.d> paramProvider, Provider<a> paramProvider1)
  {
    return new an(paramc, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.an
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */