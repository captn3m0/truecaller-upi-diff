package com.truecaller.abtest;

import com.truecaller.abtest.definitions.Constants.ActiveExperiments;
import com.truecaller.common.b.e;
import com.truecaller.common.h.am;
import java.lang.reflect.Field;

public final class a
  implements c
{
  private dagger.a<com.google.firebase.remoteconfig.a> a;
  
  public a(dagger.a<com.google.firebase.remoteconfig.a> parama)
  {
    a = parama;
  }
  
  public final String a(String paramString)
  {
    return am.n(e.a(paramString));
  }
  
  public final void a()
  {
    Class[] arrayOfClass = Constants.ActiveExperiments.class.getDeclaredClasses();
    int k = arrayOfClass.length;
    int i = 0;
    while (i < k)
    {
      Field[] arrayOfField = arrayOfClass[i].getFields();
      int m = arrayOfField.length;
      int j = 0;
      while (j < m)
      {
        Object localObject = arrayOfField[j];
        try
        {
          String str = ((Field)localObject).getName();
          localObject = (String)((Field)localObject).get(null);
          if (str.equals("VARIANT_KEY"))
          {
            str = ((com.google.firebase.remoteconfig.a)a.get()).a((String)localObject);
            if (e.c((String)localObject)) {
              break;
            }
            e.b((String)localObject, str);
            StringBuilder localStringBuilder = new StringBuilder("fetch:: ");
            localStringBuilder.append((String)localObject);
            localStringBuilder.append(" , ");
            localStringBuilder.append(str);
            localStringBuilder.toString();
          }
        }
        catch (IllegalAccessException localIllegalAccessException)
        {
          localIllegalAccessException.printStackTrace();
          j += 1;
        }
      }
      i += 1;
    }
  }
  
  public final boolean b()
  {
    return e.a("inviteMore_17575", "").equalsIgnoreCase("bulkInvite");
  }
  
  public final boolean b(String paramString)
  {
    return Boolean.valueOf(am.n(e.a(paramString))).booleanValue();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.abtest.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */