package com.truecaller.abtest;

import c.g.b.k;
import c.n.m;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.truecaller.common.h.am;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.UnmutedException.c;
import com.truecaller.old.data.access.Settings;
import java.util.concurrent.TimeUnit;

public final class d
  implements c
{
  final dagger.a<com.google.firebase.remoteconfig.a> a;
  
  public d(dagger.a<com.google.firebase.remoteconfig.a> parama)
  {
    a = parama;
  }
  
  public final String a(String paramString)
  {
    k.b(paramString, "key");
    paramString = am.n(((com.google.firebase.remoteconfig.a)a.get()).a(paramString));
    k.a(paramString, "StringUtils.defaultStrin…fig.get().getString(key))");
    return paramString;
  }
  
  public final void a()
  {
    long l;
    if (Settings.e("qaDisableFirebaseConfig")) {
      l = 0L;
    } else {
      l = TimeUnit.HOURS.toSeconds(6L);
    }
    "FirebaseRemoteConfig fetching remote values: with cache expiration: ".concat(String.valueOf(l));
    try
    {
      ((com.google.firebase.remoteconfig.a)a.get()).a(l).a((OnCompleteListener)new d.a(this));
      return;
    }
    catch (Exception localException)
    {
      String str = localException.getMessage();
      if (str != null)
      {
        AssertionUtil.reportThrowableButNeverCrash((Throwable)new UnmutedException.c(str));
        return;
      }
    }
  }
  
  public final boolean b()
  {
    return m.a(a("inviteMore_17575"), "bulkInvite", true);
  }
  
  public final boolean b(String paramString)
  {
    k.b(paramString, "key");
    paramString = ((com.google.firebase.remoteconfig.a)a.get()).a(paramString);
    CharSequence localCharSequence = (CharSequence)paramString;
    int i;
    if ((localCharSequence != null) && (localCharSequence.length() != 0)) {
      i = 0;
    } else {
      i = 1;
    }
    if (i == 0) {
      return Boolean.parseBoolean(paramString);
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.abtest.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */