package com.truecaller;

import com.truecaller.analytics.au;
import com.truecaller.analytics.b;
import com.truecaller.utils.a;
import dagger.a.d;
import javax.inject.Provider;

public final class az
  implements d<au>
{
  private final c a;
  private final Provider<a> b;
  private final Provider<b> c;
  
  private az(c paramc, Provider<a> paramProvider, Provider<b> paramProvider1)
  {
    a = paramc;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static az a(c paramc, Provider<a> paramProvider, Provider<b> paramProvider1)
  {
    return new az(paramc, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.az
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */