package com.truecaller;

import android.content.Context;
import com.truecaller.service.e;
import dagger.a.d;
import javax.inject.Provider;

public final class ad
  implements d<e>
{
  private final c a;
  private final Provider<Context> b;
  
  private ad(c paramc, Provider<Context> paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static ad a(c paramc, Provider<Context> paramProvider)
  {
    return new ad(paramc, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ad
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */