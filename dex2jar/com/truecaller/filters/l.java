package com.truecaller.filters;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class l
  implements d<p>
{
  private final h a;
  private final Provider<Context> b;
  private final Provider<r> c;
  
  private l(h paramh, Provider<Context> paramProvider, Provider<r> paramProvider1)
  {
    a = paramh;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static l a(h paramh, Provider<Context> paramProvider, Provider<r> paramProvider1)
  {
    return new l(paramh, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */