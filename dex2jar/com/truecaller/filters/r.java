package com.truecaller.filters;

import c.n.k;
import com.truecaller.common.h.u;

public final class r
{
  private final com.truecaller.common.account.r a;
  private final u b;
  
  public r(com.truecaller.common.account.r paramr, u paramu)
  {
    a = paramr;
    b = paramu;
  }
  
  private final String c()
  {
    String str = a.b();
    if (str != null) {
      return str;
    }
    throw ((Throwable)new IllegalArgumentException("Account normalized number should not be null (because block options require a valid account)".toString()));
  }
  
  public final String a()
  {
    String str = c();
    u localu = b;
    Object localObject = null;
    str = localu.a(str, null);
    if (str != null)
    {
      localObject = (CharSequence)str;
      localObject = new k("[^\\d]").a((CharSequence)localObject, "");
    }
    if (localObject != null) {
      return (String)localObject;
    }
    throw ((Throwable)new IllegalArgumentException("Parsing of normalized account number to national failed".toString()));
  }
  
  public final int b()
  {
    return c().length() - a().length();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.r
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */