package com.truecaller.filters;

import android.content.Context;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.k;
import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d<i>
{
  private final h a;
  private final Provider<Context> b;
  private final Provider<k> c;
  
  private j(h paramh, Provider<Context> paramProvider, Provider<k> paramProvider1)
  {
    a = paramh;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static j a(h paramh, Provider<Context> paramProvider, Provider<k> paramProvider1)
  {
    return new j(paramh, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */