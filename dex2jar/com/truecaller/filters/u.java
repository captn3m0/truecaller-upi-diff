package com.truecaller.filters;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import com.truecaller.androidactors.w;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.content.TruecallerContract.Filters;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;
import com.truecaller.filters.a.a;
import com.truecaller.filters.a.c;
import com.truecaller.filters.sync.FilterUploadWorker;
import com.truecaller.log.AssertionUtil;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

final class u
  implements s
{
  private final Context a;
  private final FilterManager b;
  
  u(Context paramContext, FilterManager paramFilterManager)
  {
    a = paramContext;
    b = paramFilterManager;
  }
  
  private static ContentValues a(String paramString1, String paramString2, String paramString3, String paramString4, int paramInt1, int paramInt2, int paramInt3, TruecallerContract.Filters.EntityType paramEntityType)
  {
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("value", paramString1);
    localContentValues.put("label", paramString3);
    localContentValues.put("rule", Integer.valueOf(paramInt1));
    localContentValues.put("wildcard_type", Integer.valueOf(paramInt2));
    localContentValues.put("sync_state", Integer.valueOf(paramInt3));
    localContentValues.put("tracking_type", paramString2);
    localContentValues.put("tracking_source", paramString4);
    localContentValues.put("entity_type", Integer.valueOf(value));
    return localContentValues;
  }
  
  private static ContentValues a(String paramString1, String paramString2, String paramString3, String paramString4, int paramInt, TruecallerContract.Filters.EntityType paramEntityType)
  {
    return a(paramString1, paramString2, paramString3, paramString4, paramInt, NONEtype, 1, paramEntityType);
  }
  
  private void a(List<String> paramList1, List<String> paramList2, List<String> paramList3, String paramString1, String paramString2, boolean paramBoolean, int paramInt, TruecallerContract.Filters.EntityType paramEntityType)
  {
    ContentValues[] arrayOfContentValues = new ContentValues[paramList1.size()];
    int i = 0;
    while (i < paramList1.size())
    {
      String str1 = (String)paramList1.get(i);
      String str2 = (String)paramList2.get(i);
      arrayOfContentValues[i] = a(str1, str2, (String)paramList3.get(i), paramString2, paramInt, paramEntityType);
      b.a(str1, str2, paramString1, paramString2, paramBoolean);
      i += 1;
    }
    a.getContentResolver().bulkInsert(TruecallerContract.Filters.a(), arrayOfContentValues);
  }
  
  public final w<com.truecaller.filters.a.b> a()
  {
    return w.a(new c(a.getContentResolver().query(TruecallerContract.Filters.a(), null, "rule=? AND sync_state!=?", new String[] { "0", "2" }, "_id DESC")), -..Lambda.l5vNz0Dnc5PqEuCdBo9UGFYxfYw.INSTANCE);
  }
  
  public final w<Boolean> a(CountryListDto.a parama, String paramString)
  {
    parama = c;
    ContentValues localContentValues = a(parama, "COUNTRY_CODE", null, paramString, 0, TruecallerContract.Filters.EntityType.UNKNOWN);
    localContentValues.put("sync_state", Integer.valueOf(0));
    a.getContentResolver().insert(TruecallerContract.Filters.a(), localContentValues);
    b.a(parama, "COUNTRY_CODE", "block", paramString, false);
    FilterUploadWorker.b();
    return w.b(Boolean.TRUE);
  }
  
  public final w<Boolean> a(a parama, String paramString, boolean paramBoolean)
  {
    ContentValues localContentValues = a(e, f, d, paramString, b, h.type, c, TruecallerContract.Filters.EntityType.UNKNOWN);
    if (parama.b())
    {
      a.getContentResolver().delete(TruecallerContract.Filters.a(), "_id = ?", new String[] { String.valueOf(a) });
      b.a(e, "COUNTRY_CODE", "unblock", paramString, paramBoolean);
      return w.b(Boolean.TRUE);
    }
    if (!parama.a())
    {
      localContentValues.put("rule", Integer.valueOf(1));
      localContentValues.put("sync_state", Integer.valueOf(1));
    }
    else
    {
      localContentValues.put("sync_state", Integer.valueOf(2));
    }
    a.getContentResolver().insert(TruecallerContract.Filters.a(), localContentValues);
    b.a(e, f, "unblock", paramString, paramBoolean);
    FilterUploadWorker.b();
    return w.b(Boolean.TRUE);
  }
  
  public final w<Boolean> a(String paramString1, String paramString2, TruecallerContract.Filters.WildCardType paramWildCardType, String paramString3)
  {
    paramString1 = paramWildCardType.formatPattern(paramString1);
    try
    {
      Pattern.compile(paramString1);
      paramString2 = a(paramString1, "REG_EXP", paramString2, paramString3, 0, TruecallerContract.Filters.EntityType.UNKNOWN);
      paramString2.put("wildcard_type", Integer.valueOf(type));
      a.getContentResolver().insert(TruecallerContract.Filters.a(), paramString2);
      b.a(paramString1, "REG_EXP", "block", paramString3, false);
      FilterUploadWorker.b();
      return w.b(Boolean.TRUE);
    }
    catch (PatternSyntaxException paramString1)
    {
      AssertionUtil.shouldNeverHappen(paramString1, new String[] { "Could not compile wildcard pattern" });
    }
    return w.b(Boolean.FALSE);
  }
  
  public final w<Boolean> a(String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean, TruecallerContract.Filters.EntityType paramEntityType)
  {
    paramString3 = a(paramString1, paramString2, paramString3, paramString4, 0, paramEntityType);
    a.getContentResolver().insert(TruecallerContract.Filters.a(), paramString3);
    FilterUploadWorker.b();
    b.a(paramString1, paramString2, "block", paramString4, paramBoolean);
    return w.b(Boolean.TRUE);
  }
  
  public final w<Boolean> a(List<String> paramList1, List<String> paramList2, List<String> paramList3, String paramString1, String paramString2, boolean paramBoolean)
  {
    a(paramList1, paramList2, paramList3, paramString1, paramString2, paramBoolean, 1, TruecallerContract.Filters.EntityType.UNKNOWN);
    FilterUploadWorker.b();
    return w.b(Boolean.TRUE);
  }
  
  public final w<Boolean> a(List<String> paramList1, List<String> paramList2, List<String> paramList3, String paramString, boolean paramBoolean, TruecallerContract.Filters.EntityType paramEntityType)
  {
    a(paramList1, paramList2, paramList3, "block", paramString, paramBoolean, 0, paramEntityType);
    FilterUploadWorker.b();
    return w.b(Boolean.TRUE);
  }
  
  public final w<Integer> b()
  {
    int j = com.truecaller.common.c.b.b.a(a.getContentResolver(), TruecallerContract.Filters.a(), "rule=? AND sync_state!=?", new String[] { "0", "2" });
    int i = j;
    if (j == -1) {
      i = 0;
    }
    return w.b(Integer.valueOf(i));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.u
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */