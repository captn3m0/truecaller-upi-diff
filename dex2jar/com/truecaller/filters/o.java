package com.truecaller.filters;

import android.content.Context;
import c.d.f;
import com.truecaller.common.background.b;
import com.truecaller.common.f.c;
import com.truecaller.common.h.an;
import com.truecaller.common.h.u;
import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d<v>
{
  private final h a;
  private final Provider<Context> b;
  private final Provider<p> c;
  private final Provider<f> d;
  private final Provider<b> e;
  private final Provider<u> f;
  private final Provider<an> g;
  private final Provider<c> h;
  
  private o(h paramh, Provider<Context> paramProvider, Provider<p> paramProvider1, Provider<f> paramProvider2, Provider<b> paramProvider3, Provider<u> paramProvider4, Provider<an> paramProvider5, Provider<c> paramProvider6)
  {
    a = paramh;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
    f = paramProvider4;
    g = paramProvider5;
    h = paramProvider6;
  }
  
  public static o a(h paramh, Provider<Context> paramProvider, Provider<p> paramProvider1, Provider<f> paramProvider2, Provider<b> paramProvider3, Provider<u> paramProvider4, Provider<an> paramProvider5, Provider<c> paramProvider6)
  {
    return new o(paramh, paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */