package com.truecaller.filters;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

final class q
  implements p
{
  private final SharedPreferences a;
  private final r b;
  
  q(SharedPreferences paramSharedPreferences, r paramr)
  {
    a = paramSharedPreferences;
    b = paramr;
  }
  
  public final void a(int paramInt)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putInt("filter_filteringNeighbourSpoofingMatchingDigits", paramInt);
    localEditor.apply();
  }
  
  public final void a(long paramLong)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putLong("filter_filterSyncLastUpdateTimestamp", paramLong);
    localEditor.apply();
  }
  
  public final void a(Boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = a.edit();
    if (paramBoolean == null)
    {
      localEditor.remove("filter_autoUpdateTopSpammers").apply();
      return;
    }
    localEditor.putBoolean("filter_autoUpdateTopSpammers", paramBoolean.booleanValue()).apply();
  }
  
  public final void a(Integer paramInteger)
  {
    SharedPreferences.Editor localEditor = a.edit();
    if (paramInteger == null) {
      localEditor.remove("filter_topSpammersMaxSize");
    } else {
      localEditor.putInt("filter_topSpammersMaxSize", paramInteger.intValue());
    }
    localEditor.apply();
  }
  
  public final void a(boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putBoolean("filter_filteringUnknown", paramBoolean);
    localEditor.apply();
  }
  
  public final boolean a()
  {
    return a.getBoolean("filter_filteringUnknown", false);
  }
  
  public final void b(long paramLong)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putLong("filter_topSpammersSyncLastUpdateTimestamp", paramLong);
    localEditor.apply();
  }
  
  public final void b(Integer paramInteger)
  {
    SharedPreferences.Editor localEditor = a.edit();
    if (paramInteger == null) {
      localEditor.remove("filter_topSpammersPremiumMaxSize");
    } else {
      localEditor.putInt("filter_topSpammersPremiumMaxSize", paramInteger.intValue());
    }
    localEditor.apply();
  }
  
  public final void b(boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putBoolean("filter_filteringNonPhonebook", paramBoolean);
    localEditor.apply();
  }
  
  public final boolean b()
  {
    return a.getBoolean("filter_filteringNonPhonebook", false);
  }
  
  public final void c(boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putBoolean("filter_filteringForeignNumbers", paramBoolean);
    localEditor.apply();
  }
  
  public final boolean c()
  {
    return a.getBoolean("filter_filteringForeignNumbers", false);
  }
  
  public final void d(boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putBoolean("filter_filteringNeighbourSpoofing", paramBoolean);
    localEditor.apply();
    if ((paramBoolean) && (f() == null)) {
      a(b.b() + 5);
    }
  }
  
  public final boolean d()
  {
    return a.getBoolean("filter_filteringNeighbourSpoofing", false);
  }
  
  public final void e(boolean paramBoolean)
  {
    a.edit().putBoolean("filter_filteringIndianRegisteredTelemarketers", paramBoolean).apply();
  }
  
  public final boolean e()
  {
    return a.getBoolean("filter_filteringIndianRegisteredTelemarketers", false);
  }
  
  public final Integer f()
  {
    if (a.contains("filter_filteringNeighbourSpoofingMatchingDigits")) {
      return Integer.valueOf(a.getInt("filter_filteringNeighbourSpoofingMatchingDigits", Integer.MIN_VALUE));
    }
    return null;
  }
  
  public final void f(boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putBoolean("filter_filteringTopSpammers", paramBoolean);
    localEditor.apply();
  }
  
  public final void g(boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putBoolean("filter_filterSettingsNeedsUpload", paramBoolean);
    localEditor.apply();
  }
  
  public final boolean g()
  {
    return a.getBoolean("filter_filteringTopSpammers", false);
  }
  
  public final void h(boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putBoolean("filter_updateNeeded", paramBoolean);
    localEditor.apply();
  }
  
  public final boolean h()
  {
    return a.getBoolean("filter_filterSettingsNeedsUpload", false);
  }
  
  public final long i()
  {
    return a.getLong("filter_topSpammersSyncLastUpdateTimestamp", 0L);
  }
  
  public final boolean j()
  {
    long l = i();
    int i;
    if (System.currentTimeMillis() - l < 1209600000L) {
      i = 1;
    } else {
      i = 0;
    }
    if (!a.getBoolean("filter_updateNeeded", false)) {
      return i == 0;
    }
    return true;
  }
  
  public final Boolean k()
  {
    if (!a.contains("filter_autoUpdateTopSpammers")) {
      return null;
    }
    return Boolean.valueOf(a.getBoolean("filter_autoUpdateTopSpammers", false));
  }
  
  public final int l()
  {
    return a.getInt("filter_topSpammersMaxSize", 2000);
  }
  
  public final int m()
  {
    return a.getInt("filter_topSpammersPremiumMaxSize", 2000);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */