package com.truecaller.filters;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import dagger.a.d;
import javax.inject.Provider;

public final class m
  implements d<f<s>>
{
  private final h a;
  private final Provider<i> b;
  private final Provider<s> c;
  
  private m(h paramh, Provider<i> paramProvider, Provider<s> paramProvider1)
  {
    a = paramh;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static m a(h paramh, Provider<i> paramProvider, Provider<s> paramProvider1)
  {
    return new m(paramh, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */