package com.truecaller.filters;

import android.text.TextUtils;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;
import org.c.a.a.a.a.a;
import org.c.a.a.a.a.c;
import org.c.a.a.a.a.d;

public final class g
{
  static final g a = new g(FilterManager.FilterAction.NONE_FOUND, FilterManager.ActionSource.NONE);
  static final g b = new g(FilterManager.FilterAction.FILTER_BLACKLISTED, FilterManager.ActionSource.UNKNOWN);
  static final g c = new g(FilterManager.FilterAction.FILTER_DISABLED, FilterManager.ActionSource.UNKNOWN);
  static final g d = new g(FilterManager.FilterAction.FILTER_BLACKLISTED, FilterManager.ActionSource.NON_PHONEBOOK);
  static final g e = new g(FilterManager.FilterAction.FILTER_BLACKLISTED, FilterManager.ActionSource.FOREIGN);
  static final g f = new g(FilterManager.FilterAction.FILTER_BLACKLISTED, FilterManager.ActionSource.NEIGHBOUR_SPOOFING);
  static final g g = new g(FilterManager.FilterAction.FILTER_BLACKLISTED, FilterManager.ActionSource.INDIAN_REGISTERED_TELEMARKETER);
  public final FilterManager.FilterAction h;
  public final long i;
  public final FilterManager.ActionSource j;
  public final String k;
  public final int l;
  public final int m;
  public final TruecallerContract.Filters.WildCardType n;
  
  public g(long paramLong, FilterManager.FilterAction paramFilterAction, FilterManager.ActionSource paramActionSource, String paramString, int paramInt1, int paramInt2, TruecallerContract.Filters.WildCardType paramWildCardType)
  {
    i = paramLong;
    h = paramFilterAction;
    j = paramActionSource;
    k = paramString;
    l = paramInt1;
    m = paramInt2;
    n = paramWildCardType;
  }
  
  private g(FilterManager.FilterAction paramFilterAction, FilterManager.ActionSource paramActionSource)
  {
    i = -1L;
    h = paramFilterAction;
    j = paramActionSource;
    k = null;
    l = 0;
    m = 0;
    n = TruecallerContract.Filters.WildCardType.NONE;
  }
  
  public final boolean equals(Object paramObject)
  {
    if ((paramObject instanceof g))
    {
      if (paramObject == this) {
        return true;
      }
      paramObject = (g)paramObject;
      return (i == i) && (l == l) && (m == m) && (h == h) && (j == j) && (n == n) && (TextUtils.equals(k, k));
    }
    return false;
  }
  
  public final int hashCode()
  {
    return aai).a(h).a(j).a(k).a(l).a(m).a(n).a;
  }
  
  public final String toString()
  {
    return c.b(this, d.g);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */