package com.truecaller.filters;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import c.d.f;
import c.g.b.k;
import com.truecaller.common.background.b;
import com.truecaller.common.f.c;
import com.truecaller.common.h.an;
import com.truecaller.common.h.q;
import com.truecaller.content.TruecallerContract.ao;
import com.truecaller.filters.sync.TopSpammer;
import com.truecaller.filters.sync.c.d;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.log.d;
import e.r;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

public final class w
  implements v
{
  private final p a;
  private final ContentResolver b;
  private final f c;
  private final b d;
  private final com.truecaller.filters.sync.e e;
  private final com.truecaller.common.h.u f;
  private final an g;
  private final c h;
  
  @Inject
  public w(p paramp, ContentResolver paramContentResolver, f paramf, b paramb, com.truecaller.filters.sync.e parame, com.truecaller.common.h.u paramu, an paraman, c paramc)
  {
    a = paramp;
    b = paramContentResolver;
    c = paramf;
    d = paramb;
    e = parame;
    f = paramu;
    g = paraman;
    h = paramc;
  }
  
  private static TopSpammer a(Cursor paramCursor)
  {
    try
    {
      String str = paramCursor.getString(paramCursor.getColumnIndexOrThrow("label"));
      paramCursor = new TopSpammer(paramCursor.getString(paramCursor.getColumnIndexOrThrow("value")), str, Integer.valueOf(paramCursor.getInt(paramCursor.getColumnIndexOrThrow("count"))));
      return paramCursor;
    }
    catch (IllegalAccessException paramCursor)
    {
      d.a((Throwable)paramCursor, "could not read top spammer from db");
    }
    return null;
  }
  
  private static Collection<ContentValues> a(Collection<TopSpammer> paramCollection)
  {
    Object localObject = (Iterable)paramCollection;
    paramCollection = (Collection)new ArrayList(c.a.m.a((Iterable)localObject, 10));
    localObject = ((Iterable)localObject).iterator();
    while (((Iterator)localObject).hasNext())
    {
      TopSpammer localTopSpammer = (TopSpammer)((Iterator)localObject).next();
      ContentValues localContentValues = new ContentValues();
      localContentValues.put("value", localTopSpammer.getValue());
      localContentValues.put("label", localTopSpammer.getLabel());
      localContentValues.put("count", localTopSpammer.getReports());
      boolean bool2 = TextUtils.isEmpty((CharSequence)localContentValues.getAsString("value"));
      boolean bool1 = true;
      AssertionUtil.isTrue(bool2 ^ true, new String[0]);
      if (localContentValues.getAsLong("count").longValue() < 0L) {
        bool1 = false;
      }
      AssertionUtil.isTrue(bool1, new String[0]);
      paramCollection.add(localContentValues);
    }
    return (Collection)paramCollection;
  }
  
  private final void b(Collection<ContentValues> paramCollection)
  {
    Object localObject = b;
    Uri localUri = TruecallerContract.ao.a();
    if (paramCollection != null)
    {
      Object[] arrayOfObject = paramCollection.toArray(new ContentValues[0]);
      if (arrayOfObject != null)
      {
        int i = ((ContentResolver)localObject).bulkInsert(localUri, (ContentValues[])arrayOfObject);
        boolean bool;
        if (i == paramCollection.size()) {
          bool = true;
        } else {
          bool = false;
        }
        localObject = new StringBuilder("Unexpected # of spammers added, got ");
        ((StringBuilder)localObject).append(paramCollection.size());
        ((StringBuilder)localObject).append(", added ");
        ((StringBuilder)localObject).append(i);
        AssertionUtil.OnlyInDebug.isTrue(bool, new String[] { ((StringBuilder)localObject).toString() });
        return;
      }
      throw new c.u("null cannot be cast to non-null type kotlin.Array<T>");
    }
    throw new c.u("null cannot be cast to non-null type java.util.Collection<T>");
  }
  
  /* Error */
  public final TopSpammer a(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc -3
    //   3: invokestatic 32	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_0
    //   7: getfield 53	com/truecaller/filters/w:b	Landroid/content/ContentResolver;
    //   10: invokestatic 204	com/truecaller/content/TruecallerContract$ao:a	()Landroid/net/Uri;
    //   13: aconst_null
    //   14: ldc -1
    //   16: iconst_1
    //   17: anewarray 174	java/lang/String
    //   20: dup
    //   21: iconst_0
    //   22: aload_1
    //   23: aastore
    //   24: aconst_null
    //   25: invokevirtual 259	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   28: astore_1
    //   29: aconst_null
    //   30: astore_2
    //   31: aload_1
    //   32: ifnull +72 -> 104
    //   35: aload_1
    //   36: checkcast 261	java/io/Closeable
    //   39: astore_3
    //   40: aload_2
    //   41: astore_1
    //   42: aload_3
    //   43: checkcast 74	android/database/Cursor
    //   46: astore 4
    //   48: aload_2
    //   49: astore_1
    //   50: aload 4
    //   52: invokeinterface 264 1 0
    //   57: ifeq +18 -> 75
    //   60: aload_2
    //   61: astore_1
    //   62: aload 4
    //   64: invokestatic 266	com/truecaller/filters/w:a	(Landroid/database/Cursor;)Lcom/truecaller/filters/sync/TopSpammer;
    //   67: astore_2
    //   68: aload_3
    //   69: aconst_null
    //   70: invokestatic 271	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   73: aload_2
    //   74: areturn
    //   75: aload_2
    //   76: astore_1
    //   77: getstatic 276	c/x:a	Lc/x;
    //   80: astore_2
    //   81: aload_3
    //   82: aconst_null
    //   83: invokestatic 271	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   86: aconst_null
    //   87: areturn
    //   88: astore_2
    //   89: goto +8 -> 97
    //   92: astore_2
    //   93: aload_2
    //   94: astore_1
    //   95: aload_2
    //   96: athrow
    //   97: aload_3
    //   98: aload_1
    //   99: invokestatic 271	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   102: aload_2
    //   103: athrow
    //   104: aconst_null
    //   105: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	106	0	this	w
    //   0	106	1	paramString	String
    //   30	51	2	localObject1	Object
    //   88	1	2	localObject2	Object
    //   92	11	2	localThrowable	Throwable
    //   39	59	3	localCloseable	java.io.Closeable
    //   46	17	4	localCursor	Cursor
    // Exception table:
    //   from	to	target	type
    //   42	48	88	finally
    //   50	60	88	finally
    //   62	68	88	finally
    //   77	81	88	finally
    //   95	97	88	finally
    //   42	48	92	java/lang/Throwable
    //   50	60	92	java/lang/Throwable
    //   62	68	92	java/lang/Throwable
    //   77	81	92	java/lang/Throwable
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString2, "number");
    b(a((Collection)c.a.m.a(new TopSpammer(f.b(paramString2), paramString1, Integer.valueOf(999)))));
  }
  
  public final boolean a()
  {
    int i;
    if (h.d()) {
      i = a.m();
    } else {
      i = a.l();
    }
    Object localObject = q.a(e.a(i, "caller"));
    int j = 0;
    if (localObject != null)
    {
      if (!((r)localObject).d()) {
        localObject = null;
      }
      if (localObject != null)
      {
        localObject = (c.d)((r)localObject).e();
        if (localObject != null)
        {
          List localList = a;
          if (localList == null) {
            return false;
          }
          localObject = q.a(e.a(i, "sms"));
          if (localObject != null)
          {
            if (!((r)localObject).d()) {
              localObject = null;
            }
            if (localObject != null)
            {
              localObject = (c.d)((r)localObject).e();
              if (localObject != null)
              {
                localObject = a;
                if (localObject == null) {
                  return false;
                }
                localObject = a((Collection)c.a.m.a((Iterable)c.a.m.c((Collection)localList, (Iterable)localObject)));
                b.delete(TruecallerContract.ao.a(), null, null);
                b((Collection)localObject);
                i = j;
                if (a.i() == 0L) {
                  i = 1;
                }
                a.b(g.a());
                if (i != 0) {
                  d.a(10004);
                }
                return true;
              }
            }
          }
          return false;
        }
      }
    }
    return false;
  }
  
  public final void b()
  {
    kotlinx.coroutines.e.b((ag)bg.a, c, (c.g.a.m)new w.a(this, null), 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.w
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */