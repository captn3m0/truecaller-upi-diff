package com.truecaller.filters;

import android.content.Context;
import android.telephony.TelephonyManager;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.b;
import com.truecaller.androidactors.f;
import com.truecaller.common.account.r;
import com.truecaller.common.f.c;
import com.truecaller.common.h.u;
import com.truecaller.featuretoggles.e;
import com.truecaller.utils.l;
import dagger.a.d;
import javax.inject.Provider;

public final class k
  implements d<FilterManager>
{
  private final h a;
  private final Provider<Context> b;
  private final Provider<p> c;
  private final Provider<f<ae>> d;
  private final Provider<b> e;
  private final Provider<u> f;
  private final Provider<e> g;
  private final Provider<r> h;
  private final Provider<TelephonyManager> i;
  private final Provider<l> j;
  private final Provider<c> k;
  
  private k(h paramh, Provider<Context> paramProvider, Provider<p> paramProvider1, Provider<f<ae>> paramProvider2, Provider<b> paramProvider3, Provider<u> paramProvider4, Provider<e> paramProvider5, Provider<r> paramProvider6, Provider<TelephonyManager> paramProvider7, Provider<l> paramProvider8, Provider<c> paramProvider9)
  {
    a = paramh;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
    f = paramProvider4;
    g = paramProvider5;
    h = paramProvider6;
    i = paramProvider7;
    j = paramProvider8;
    k = paramProvider9;
  }
  
  public static k a(h paramh, Provider<Context> paramProvider, Provider<p> paramProvider1, Provider<f<ae>> paramProvider2, Provider<b> paramProvider3, Provider<u> paramProvider4, Provider<e> paramProvider5, Provider<r> paramProvider6, Provider<TelephonyManager> paramProvider7, Provider<l> paramProvider8, Provider<c> paramProvider9)
  {
    return new k(paramh, paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */