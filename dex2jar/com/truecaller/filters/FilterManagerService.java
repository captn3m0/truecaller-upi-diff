package com.truecaller.filters;

import com.truecaller.androidactors.h;
import java.util.concurrent.TimeUnit;

public class FilterManagerService
  extends h
{
  public FilterManagerService()
  {
    super("filter-manager", TimeUnit.SECONDS.toMillis(30L), false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.FilterManagerService
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */