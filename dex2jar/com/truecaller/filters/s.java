package com.truecaller.filters;

import com.truecaller.androidactors.w;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;
import com.truecaller.filters.a.a;
import com.truecaller.filters.a.b;
import java.util.List;

public abstract interface s
{
  public abstract w<b> a();
  
  public abstract w<Boolean> a(CountryListDto.a parama, String paramString);
  
  public abstract w<Boolean> a(a parama, String paramString, boolean paramBoolean);
  
  public abstract w<Boolean> a(String paramString1, String paramString2, TruecallerContract.Filters.WildCardType paramWildCardType, String paramString3);
  
  public abstract w<Boolean> a(String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean, TruecallerContract.Filters.EntityType paramEntityType);
  
  public abstract w<Boolean> a(List<String> paramList1, List<String> paramList2, List<String> paramList3, String paramString1, String paramString2, boolean paramBoolean);
  
  public abstract w<Boolean> a(List<String> paramList1, List<String> paramList2, List<String> paramList3, String paramString, boolean paramBoolean, TruecallerContract.Filters.EntityType paramEntityType);
  
  public abstract w<Integer> b();
}

/* Location:
 * Qualified Name:     com.truecaller.filters.s
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */