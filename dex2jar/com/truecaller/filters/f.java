package com.truecaller.filters;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import c.n;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.e.a;
import com.truecaller.common.h.aa;
import com.truecaller.common.h.ab;
import com.truecaller.common.h.am;
import com.truecaller.common.h.u;
import com.truecaller.content.TruecallerContract.Filters;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;
import com.truecaller.content.TruecallerContract.ao;
import com.truecaller.data.entity.Contact;
import com.truecaller.featuretoggles.e;
import com.truecaller.filters.sync.FilterUploadWorker;
import com.truecaller.filters.sync.c.b;
import com.truecaller.filters.sync.c.c;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.tracking.events.at;
import com.truecaller.tracking.events.at.a;
import com.truecaller.utils.l;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import okhttp3.ad;
import org.c.a.a.a.k;

final class f
  implements FilterManager
{
  private static final Pattern a = Pattern.compile("91140\\d{7}$");
  private final Context b;
  private final p c;
  private final com.truecaller.androidactors.f<ae> d;
  private final com.truecaller.analytics.b e;
  private final u f;
  private final e g;
  private final com.truecaller.common.account.r h;
  private final TelephonyManager i;
  private final l j;
  private final com.truecaller.common.f.c k;
  
  f(Context paramContext, p paramp, com.truecaller.androidactors.f<ae> paramf, com.truecaller.analytics.b paramb, u paramu, e parame, com.truecaller.common.account.r paramr, TelephonyManager paramTelephonyManager, l paraml, com.truecaller.common.f.c paramc)
  {
    b = paramContext.getApplicationContext();
    c = paramp;
    d = paramf;
    e = paramb;
    f = paramu;
    g = parame;
    h = paramr;
    i = paramTelephonyManager;
    j = paraml;
    k = paramc;
  }
  
  private static ContentValues a(String paramString1, String paramString2, String paramString3, int paramInt, String paramString4, TruecallerContract.Filters.EntityType paramEntityType)
  {
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("value", paramString1);
    localContentValues.put("label", paramString3);
    localContentValues.put("rule", Integer.valueOf(paramInt));
    localContentValues.put("wildcard_type", Integer.valueOf(NONEtype));
    localContentValues.put("sync_state", Integer.valueOf(1));
    localContentValues.put("tracking_type", paramString2);
    localContentValues.put("tracking_source", paramString4);
    localContentValues.put("entity_type", Integer.valueOf(value));
    return localContentValues;
  }
  
  private g a(g paramg, boolean paramBoolean)
  {
    if ((paramg != null) && (paramBoolean) && (h == FilterManager.FilterAction.FILTER_BLACKLISTED) && (j == FilterManager.ActionSource.TOP_SPAMMER) && (!c.g())) {
      return new g(i, FilterManager.FilterAction.FILTER_DISABLED, j, k, l, m, n);
    }
    return paramg;
  }
  
  private g a(String paramString1, String paramString2)
  {
    paramString1 = c(paramString1, paramString2, 1);
    if (paramString1.isEmpty()) {
      return null;
    }
    return (g)paramString1.get(0);
  }
  
  private static com.truecaller.filters.sync.c.a a(ContentValues paramContentValues, List<com.truecaller.filters.sync.c.a> paramList)
  {
    paramContentValues = paramContentValues.getAsString("value");
    if (paramContentValues == null) {
      return null;
    }
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      com.truecaller.filters.sync.c.a locala = (com.truecaller.filters.sync.c.a)paramList.next();
      if (paramContentValues.equals(b)) {
        return locala;
      }
    }
    return null;
  }
  
  private String a(String... paramVarArgs)
  {
    int n = paramVarArgs.length;
    int m = 0;
    while (m < n)
    {
      String str = paramVarArgs[m];
      if (!TextUtils.isEmpty(str))
      {
        str = f.e(str);
        if (str != null) {
          return str;
        }
      }
      m += 1;
    }
    return null;
  }
  
  private List<g> a(String paramString1, String paramString2, int paramInt)
  {
    ArrayList localArrayList = new ArrayList();
    Object localObject = f.a.a(TruecallerContract.Filters.WildCardType.NONE, paramString1, paramString2, 1, true);
    if (localObject == null) {
      return localArrayList;
    }
    localObject = b.getContentResolver().query(TruecallerContract.Filters.a(), null, a, b, null);
    if (localObject != null) {
      try
      {
        int m = ((Cursor)localObject).getColumnIndexOrThrow("_id");
        int n = ((Cursor)localObject).getColumnIndex("value");
        int i1 = ((Cursor)localObject).getColumnIndex("label");
        int i2 = ((Cursor)localObject).getColumnIndex("sync_state");
        while ((((Cursor)localObject).moveToNext()) && (localArrayList.size() < paramInt))
        {
          long l = ((Cursor)localObject).getLong(m);
          String str1 = ((Cursor)localObject).getString(n);
          String str2 = ((Cursor)localObject).getString(i1);
          int i3 = ((Cursor)localObject).getInt(i2);
          if ((str1.equals(paramString1)) || (str1.equals(paramString2))) {
            localArrayList.add(new g(l, FilterManager.FilterAction.ALLOW_WHITELISTED, FilterManager.ActionSource.CUSTOM_WHITELIST, str2, i3, 0, TruecallerContract.Filters.WildCardType.NONE));
          }
        }
        return localArrayList;
      }
      finally
      {
        ((Cursor)localObject).close();
      }
    }
    return localArrayList;
  }
  
  private static void a(Collection<ContentValues> paramCollection)
  {
    a(paramCollection, true);
    FilterUploadWorker.b();
  }
  
  private static void a(Collection<ContentValues> paramCollection, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      localObject = paramCollection.iterator();
      while (((Iterator)localObject).hasNext()) {
        ((ContentValues)((Iterator)localObject).next()).put("sync_state", Integer.valueOf(1));
      }
    }
    int m = com.truecaller.common.b.a.F().getContentResolver().bulkInsert(TruecallerContract.Filters.a(), (ContentValues[])paramCollection.toArray(new ContentValues[paramCollection.size()]));
    if (m == paramCollection.size()) {
      paramBoolean = true;
    } else {
      paramBoolean = false;
    }
    Object localObject = new StringBuilder("Unexpected # of filters added, got ");
    ((StringBuilder)localObject).append(paramCollection.size());
    ((StringBuilder)localObject).append(", added ");
    ((StringBuilder)localObject).append(m);
    AssertionUtil.OnlyInDebug.isTrue(paramBoolean, new String[] { ((StringBuilder)localObject).toString() });
  }
  
  private static void a(List<Long> paramList)
  {
    if (paramList.isEmpty()) {
      return;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    ArrayList localArrayList = new ArrayList();
    localStringBuilder.append("_id IN (?");
    localArrayList.add(String.valueOf(paramList.get(0)));
    int m = 1;
    while (m < paramList.size())
    {
      localStringBuilder.append(",?");
      localArrayList.add(String.valueOf(paramList.get(m)));
      m += 1;
    }
    localStringBuilder.append(")");
    com.truecaller.common.b.a.F().getContentResolver().delete(TruecallerContract.Filters.a(), localStringBuilder.toString(), (String[])localArrayList.toArray(new String[localArrayList.size()]));
  }
  
  private boolean a(c.f<Boolean> paramf)
  {
    if ((g.M().a()) && (c.b())) {
      return !((Boolean)paramf.b()).booleanValue();
    }
    return false;
  }
  
  private boolean a(String paramString, c.f<Boolean> paramf)
  {
    String str = paramString;
    if (paramString != null)
    {
      str = paramString;
      if (paramString.startsWith("+")) {
        str = paramString.substring(1);
      }
    }
    return (str != null) && (g.S().a()) && (c.e()) && (a.matcher(str).matches()) && (!((Boolean)paramf.b()).booleanValue());
  }
  
  private static boolean a(String paramString, String... paramVarArgs)
  {
    int m = 0;
    while (m < 3)
    {
      String str = paramVarArgs[m];
      if ((str != null) && (str.equalsIgnoreCase(paramString))) {
        return true;
      }
      m += 1;
    }
    return false;
  }
  
  private g b(String paramString1, String paramString2)
  {
    paramString1 = a(paramString1, paramString2, 1);
    if (paramString1.isEmpty()) {
      return null;
    }
    return (g)paramString1.get(0);
  }
  
  private g b(String paramString1, String paramString2, boolean paramBoolean)
  {
    if ((paramBoolean) && (ab.a(paramString1)) && (ab.a(paramString2)))
    {
      if (c.a()) {
        return g.b;
      }
      return g.c;
    }
    if ((TextUtils.isEmpty(paramString1)) && (TextUtils.isEmpty(paramString2))) {
      return g.a;
    }
    return null;
  }
  
  private List<g> b(String paramString1, String paramString2, int paramInt)
  {
    ArrayList localArrayList = new ArrayList();
    Object localObject = f.a.a(TruecallerContract.Filters.WildCardType.NONE, paramString1, paramString2, 0, false);
    if (localObject == null) {
      return localArrayList;
    }
    localObject = b.getContentResolver().query(TruecallerContract.Filters.a(), null, a, b, null);
    int m;
    int n;
    int i1;
    int i2;
    long l;
    String str;
    int i3;
    if (localObject != null) {
      try
      {
        m = ((Cursor)localObject).getColumnIndexOrThrow("_id");
        n = ((Cursor)localObject).getColumnIndex("value");
        i1 = ((Cursor)localObject).getColumnIndexOrThrow("label");
        i2 = ((Cursor)localObject).getColumnIndexOrThrow("sync_state");
        while ((((Cursor)localObject).moveToNext()) && (localArrayList.size() < paramInt))
        {
          l = ((Cursor)localObject).getLong(m);
          str = ((Cursor)localObject).getString(n);
          if ((str.equals(paramString1)) || (str.equals(paramString2)))
          {
            str = ((Cursor)localObject).getString(i1);
            i3 = ((Cursor)localObject).getInt(i2);
            localArrayList.add(new g(l, FilterManager.FilterAction.FILTER_BLACKLISTED, FilterManager.ActionSource.CUSTOM_BLACKLIST, str, i3, 0, TruecallerContract.Filters.WildCardType.NONE));
          }
        }
      }
      finally
      {
        ((Cursor)localObject).close();
      }
    }
    if (localArrayList.size() >= paramInt) {
      return localArrayList;
    }
    localObject = f.a.a(null, paramString1, paramString2, -1, false);
    if (localObject == null) {
      return localArrayList;
    }
    localObject = b.getContentResolver().query(TruecallerContract.ao.a(), null, a, b, null);
    if (localObject != null) {
      try
      {
        m = ((Cursor)localObject).getColumnIndexOrThrow("_id");
        n = ((Cursor)localObject).getColumnIndex("value");
        i1 = ((Cursor)localObject).getColumnIndexOrThrow("label");
        i2 = ((Cursor)localObject).getColumnIndexOrThrow("count");
        while ((((Cursor)localObject).moveToNext()) && (localArrayList.size() < paramInt))
        {
          l = ((Cursor)localObject).getLong(m);
          str = ((Cursor)localObject).getString(n);
          if ((str.equalsIgnoreCase(paramString1)) || (str.equalsIgnoreCase(paramString2)))
          {
            str = ((Cursor)localObject).getString(i1);
            i3 = ((Cursor)localObject).getInt(i2);
            localArrayList.add(new g(l, FilterManager.FilterAction.FILTER_BLACKLISTED, FilterManager.ActionSource.TOP_SPAMMER, str, 0, i3, TruecallerContract.Filters.WildCardType.NONE));
          }
        }
        return localArrayList;
      }
      finally
      {
        ((Cursor)localObject).close();
      }
    }
    return localArrayList;
  }
  
  private boolean b(String paramString, c.f<Boolean> paramf)
  {
    String str = h.b();
    boolean bool1 = g.Q().a();
    boolean bool2 = c.d();
    Integer localInteger = c.f();
    if ((str != null) && (paramString != null) && (localInteger != null) && (bool1) && (bool2) && (!((Boolean)paramf.b()).booleanValue()))
    {
      paramf = str.substring(0, localInteger.intValue());
      if (paramf.length() < 3)
      {
        AssertionUtil.reportWeirdnessButNeverCrash("Invalid neighbour spoofing filter (too short)");
        return false;
      }
      return paramString.startsWith(paramf);
    }
    return false;
  }
  
  /* Error */
  private g c(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 60	com/truecaller/filters/f:b	Landroid/content/Context;
    //   4: invokevirtual 234	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   7: invokestatic 239	com/truecaller/content/TruecallerContract$Filters:a	()Landroid/net/Uri;
    //   10: aconst_null
    //   11: ldc_w 476
    //   14: iconst_3
    //   15: anewarray 209	java/lang/String
    //   18: dup
    //   19: iconst_0
    //   20: aload_1
    //   21: invokestatic 482	java/util/Locale:getDefault	()Ljava/util/Locale;
    //   24: invokevirtual 486	java/lang/String:toLowerCase	(Ljava/util/Locale;)Ljava/lang/String;
    //   27: aastore
    //   28: dup
    //   29: iconst_1
    //   30: ldc_w 488
    //   33: aastore
    //   34: dup
    //   35: iconst_2
    //   36: ldc_w 490
    //   39: aastore
    //   40: aconst_null
    //   41: invokevirtual 250	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   44: astore_1
    //   45: aload_1
    //   46: ifnull +120 -> 166
    //   49: aload_1
    //   50: ldc -4
    //   52: invokeinterface 258 2 0
    //   57: istore_2
    //   58: aload_1
    //   59: ldc 86
    //   61: invokeinterface 261 2 0
    //   66: istore_3
    //   67: aload_1
    //   68: ldc 117
    //   70: invokeinterface 258 2 0
    //   75: istore 4
    //   77: aload_1
    //   78: invokeinterface 493 1 0
    //   83: ifeq +63 -> 146
    //   86: aload_1
    //   87: iload_2
    //   88: invokeinterface 272 2 0
    //   93: lstore 5
    //   95: aload_1
    //   96: iload_3
    //   97: invokeinterface 276 2 0
    //   102: astore 7
    //   104: aload_1
    //   105: iload 4
    //   107: invokeinterface 280 2 0
    //   112: istore_2
    //   113: new 130	com/truecaller/filters/g
    //   116: dup
    //   117: lload 5
    //   119: getstatic 138	com/truecaller/filters/FilterManager$FilterAction:FILTER_BLACKLISTED	Lcom/truecaller/filters/FilterManager$FilterAction;
    //   122: getstatic 440	com/truecaller/filters/FilterManager$ActionSource:CUSTOM_BLACKLIST	Lcom/truecaller/filters/FilterManager$ActionSource;
    //   125: aload 7
    //   127: iload_2
    //   128: iconst_0
    //   129: getstatic 111	com/truecaller/content/TruecallerContract$Filters$WildCardType:NONE	Lcom/truecaller/content/TruecallerContract$Filters$WildCardType;
    //   132: invokespecial 172	com/truecaller/filters/g:<init>	(JLcom/truecaller/filters/FilterManager$FilterAction;Lcom/truecaller/filters/FilterManager$ActionSource;Ljava/lang/String;IILcom/truecaller/content/TruecallerContract$Filters$WildCardType;)V
    //   135: astore 7
    //   137: aload_1
    //   138: invokeinterface 292 1 0
    //   143: aload 7
    //   145: areturn
    //   146: aload_1
    //   147: invokeinterface 292 1 0
    //   152: goto +14 -> 166
    //   155: astore 7
    //   157: aload_1
    //   158: invokeinterface 292 1 0
    //   163: aload 7
    //   165: athrow
    //   166: aconst_null
    //   167: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	168	0	this	f
    //   0	168	1	paramString	String
    //   57	71	2	m	int
    //   66	31	3	n	int
    //   75	31	4	i1	int
    //   93	25	5	l	long
    //   102	42	7	localObject1	Object
    //   155	9	7	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   49	137	155	finally
  }
  
  private g c(String paramString1, String paramString2)
  {
    paramString1 = b(paramString1, paramString2, 1);
    if (paramString1.isEmpty()) {
      return null;
    }
    return (g)paramString1.get(0);
  }
  
  private g c(String paramString1, String paramString2, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      paramString1 = b(paramString1, paramString2, Integer.MAX_VALUE);
      if (paramString1.size() > 1)
      {
        paramString2 = paramString1.iterator();
        while (paramString2.hasNext())
        {
          g localg1 = (g)paramString2.next();
          g localg2 = a(localg1, true);
          if ((localg2 != null) && (h == FilterManager.FilterAction.FILTER_BLACKLISTED)) {
            return localg1;
          }
        }
      }
      if (paramString1.isEmpty()) {
        paramString1 = null;
      } else {
        paramString1 = (g)paramString1.get(0);
      }
      return a(paramString1, true);
    }
    return c(paramString1, paramString2);
  }
  
  private List<g> c(String paramString1, String paramString2, int paramInt)
  {
    int m = NONEtype;
    Object localObject1 = DatabaseUtils.concatenateWhere("wildcard_type !=?", "sync_state!=?");
    Object localObject2 = DatabaseUtils.appendSelectionArgs(new String[] { String.valueOf(m) }, new String[] { "2" });
    localObject1 = b.getContentResolver().query(TruecallerContract.Filters.a(), null, (String)localObject1, (String[])localObject2, null);
    localObject2 = new ArrayList();
    if (localObject1 != null) {}
    label376:
    label379:
    for (;;)
    {
      try
      {
        m = ((Cursor)localObject1).getColumnIndexOrThrow("_id");
        int n = ((Cursor)localObject1).getColumnIndex("value");
        int i1 = ((Cursor)localObject1).getColumnIndex("wildcard_type");
        int i2 = ((Cursor)localObject1).getColumnIndexOrThrow("label");
        int i3 = ((Cursor)localObject1).getColumnIndexOrThrow("sync_state");
        if ((((Cursor)localObject1).moveToNext()) && (((List)localObject2).size() < paramInt))
        {
          long l = ((Cursor)localObject1).getLong(m);
          String str1 = ((Cursor)localObject1).getString(n);
          int i4 = ((Cursor)localObject1).getInt(i1);
          String str2 = ((Cursor)localObject1).getString(i2);
          int i5 = ((Cursor)localObject1).getInt(i3);
          TruecallerContract.Filters.WildCardType localWildCardType = TruecallerContract.Filters.WildCardType.valueOfType(i4);
          if ((i4 == CONTAINtype) && (!TextUtils.isEmpty(null)) && (d(null, str1, i4)))
          {
            ((List)localObject2).add(new g(l, FilterManager.FilterAction.FILTER_BLACKLISTED, FilterManager.ActionSource.CUSTOM_BLACKLIST, str2, i5, 0, localWildCardType));
            continue;
          }
          if (TextUtils.isEmpty(paramString1)) {
            break label379;
          }
          if (d(paramString1, str1, i4)) {
            break label376;
          }
          if ((TextUtils.isEmpty(paramString2)) || (!d(paramString2, str1, i4))) {
            continue;
          }
          ((List)localObject2).add(new g(l, FilterManager.FilterAction.FILTER_BLACKLISTED, FilterManager.ActionSource.CUSTOM_BLACKLIST, str2, i5, 0, localWildCardType));
          continue;
        }
        return (List<g>)localObject2;
      }
      finally
      {
        ((Cursor)localObject1).close();
      }
      return (List<g>)localObject2;
    }
  }
  
  private boolean c(String paramString, c.f<Boolean> paramf)
  {
    if ((g.O().a()) && (c.c()) && (!((Boolean)paramf.b()).booleanValue())) {
      return !a(paramString, new String[] { h.a(), i.getNetworkCountryIso(), i.getSimCountryIso() });
    }
    return false;
  }
  
  private static boolean d(String paramString1, String paramString2, int paramInt)
  {
    return new f.c(paramString2, paramInt).a(paramString1);
  }
  
  public final int a(List<String> paramList, String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean)
  {
    ArrayList localArrayList = new ArrayList();
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      String str = (String)paramList.next();
      if (!TextUtils.isEmpty(str))
      {
        localArrayList.add(a(str, paramString1, am.n(paramString2), 1, paramString3, TruecallerContract.Filters.EntityType.UNKNOWN));
        a(str, paramString1, paramString4, paramString3, paramBoolean);
      }
    }
    if (!localArrayList.isEmpty()) {
      a(localArrayList);
    }
    return localArrayList.size();
  }
  
  public final int a(List<String> paramList, String paramString1, String paramString2, String paramString3, boolean paramBoolean, TruecallerContract.Filters.WildCardType paramWildCardType, TruecallerContract.Filters.EntityType paramEntityType)
  {
    ArrayList localArrayList = new ArrayList();
    paramList = paramList.iterator();
    while (paramList.hasNext()) {
      localArrayList.add(new n((String)paramList.next(), paramString2));
    }
    return a(localArrayList, paramString1, paramString3, paramBoolean, paramWildCardType, paramEntityType);
  }
  
  public final int a(List<n<String, String>> paramList, String paramString1, String paramString2, boolean paramBoolean, TruecallerContract.Filters.WildCardType paramWildCardType, TruecallerContract.Filters.EntityType paramEntityType)
  {
    ArrayList localArrayList = new ArrayList();
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      n localn = (n)paramList.next();
      if (!TextUtils.isEmpty((CharSequence)a))
      {
        if (paramWildCardType == TruecallerContract.Filters.WildCardType.NONE)
        {
          localArrayList.add(a((String)a, paramString1, am.n((String)b), 0, paramString2, paramEntityType));
        }
        else
        {
          String str = (String)a;
          Object localObject = am.n((String)b);
          str = paramWildCardType.formatPattern(str);
          Pattern.compile(str);
          localObject = a(str, "REG_EXP", (String)localObject, 0, paramString2, TruecallerContract.Filters.EntityType.UNKNOWN);
          ((ContentValues)localObject).put("wildcard_type", Integer.valueOf(type));
          localArrayList.add(localObject);
        }
        a((String)a, paramString1, "block", paramString2, paramBoolean);
      }
    }
    if (!localArrayList.isEmpty()) {
      a(localArrayList);
    }
    return localArrayList.size();
  }
  
  public final g a(String paramString)
  {
    return a(paramString, null, null, false);
  }
  
  public final g a(String paramString1, String paramString2, String paramString3, boolean paramBoolean)
  {
    Object localObject = b(paramString1, paramString2, paramBoolean);
    if (localObject != null) {
      return (g)localObject;
    }
    c.f localf = c.g.a(new -..Lambda.f.gwA-od1EsMZv1k1JzBjqllBVpOc(this, paramString1));
    if ((paramBoolean) && (a(localf))) {
      return g.d;
    }
    localObject = am.n(paramString1);
    String str = am.n(paramString2);
    paramString2 = aa.c(paramString3);
    if (paramString2 == null) {
      paramString1 = (String)localObject;
    } else {
      paramString1 = aa.b((String)localObject, paramString2);
    }
    if (paramString2 == null) {
      paramString2 = str;
    } else {
      paramString2 = aa.b(str, paramString2);
    }
    int m;
    if ((TextUtils.equals((CharSequence)localObject, paramString1)) && (TextUtils.equals(str, paramString2))) {
      m = 0;
    } else {
      m = 1;
    }
    paramString3 = b(paramString1, paramString2);
    if (paramString3 != null) {
      return paramString3;
    }
    if (m != 0)
    {
      paramString3 = b((String)localObject, str);
      if (paramString3 != null) {
        return paramString3;
      }
    }
    paramString3 = a(new String[] { paramString1, paramString2, localObject, str });
    if ((paramBoolean) && (c(paramString3, localf))) {
      return g.e;
    }
    if ((paramBoolean) && (b(paramString1, localf))) {
      return g.f;
    }
    if ((paramBoolean) && (a(paramString1, localf))) {
      return g.g;
    }
    if (paramString3 != null)
    {
      paramString3 = c(paramString3);
      if (paramString3 != null) {
        return paramString3;
      }
    }
    paramString1 = c(paramString1, paramString2, paramBoolean);
    if (paramString1 != null) {
      return paramString1;
    }
    if (m != 0)
    {
      paramString1 = c((String)localObject, str, paramBoolean);
      if (paramString1 != null) {
        return paramString1;
      }
    }
    paramString1 = a((String)localObject, str);
    if (paramString1 != null) {
      return paramString1;
    }
    return g.a;
  }
  
  public final Collection<g> a(String paramString1, String paramString2, boolean paramBoolean)
  {
    LinkedHashSet localLinkedHashSet = new LinkedHashSet();
    Object localObject = b(paramString1, paramString2, paramBoolean);
    if (localObject != null) {
      localLinkedHashSet.add(localObject);
    }
    localLinkedHashSet.addAll(a(paramString1, paramString2, Integer.MAX_VALUE));
    localObject = b(paramString1, paramString2, Integer.MAX_VALUE).iterator();
    while (((Iterator)localObject).hasNext()) {
      localLinkedHashSet.add(a((g)((Iterator)localObject).next(), paramBoolean));
    }
    localObject = a(new String[] { paramString2, paramString1 });
    if (localObject != null)
    {
      localObject = c((String)localObject);
      if (localObject != null) {
        localLinkedHashSet.add(localObject);
      }
    }
    localLinkedHashSet.addAll(c(paramString1, paramString2, Integer.MAX_VALUE));
    return localLinkedHashSet;
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean)
  {
    com.truecaller.tracking.events.c.a locala = com.truecaller.tracking.events.c.b().a(paramString1).d(paramString3).e(paramString4);
    paramString4 = new e.a("ViewAction").a("Action", paramString3).a("Context", paramString4);
    boolean bool = paramString2.equals("REG_EXP");
    paramString3 = null;
    at.a locala1;
    if ((!bool) && (!paramString2.equals("COUNTRY_CODE")))
    {
      locala1 = at.b();
      paramString2 = a(paramString1);
      if (h == FilterManager.FilterAction.ALLOW_WHITELISTED) {
        bool = true;
      } else {
        bool = false;
      }
      locala1.c(bool);
      if ((h != FilterManager.FilterAction.FILTER_BLACKLISTED) && (h != FilterManager.FilterAction.FILTER_DISABLED))
      {
        locala1.b(false);
        locala1.a(false);
      }
      else
      {
        if (j == FilterManager.ActionSource.CUSTOM_BLACKLIST) {
          bool = true;
        } else {
          bool = false;
        }
        locala1.b(bool);
        if (j == FilterManager.ActionSource.TOP_SPAMMER) {
          bool = true;
        } else {
          bool = false;
        }
        locala1.a(bool);
      }
    }
    try
    {
      paramString1 = aa.a(paramString1);
    }
    catch (com.google.c.a.g paramString1)
    {
      for (;;) {}
    }
    paramString1 = null;
    if (paramString1 == null) {
      paramString2 = null;
    } else {
      paramString2 = new com.truecaller.data.access.c(b).b(paramString1);
    }
    if ((paramString2 != null) && (paramString2.U())) {
      bool = true;
    } else {
      bool = false;
    }
    locala1.d(bool);
    if (k.a(paramString1, "+", false))
    {
      paramString1 = paramString1.substring(1);
      paramString4.a("SubAction", "numeric");
    }
    else
    {
      paramString4.a("SubAction", "alphanumeric");
    }
    locala.b("address");
    locala.c(paramString1);
    locala.a(locala1.a());
    paramString1 = paramString3;
    if (paramBoolean)
    {
      paramString1 = paramString3;
      if (paramString2 != null)
      {
        paramString1 = paramString3;
        if (paramString2.U()) {
          paramString1 = Integer.valueOf(paramString2.I());
        }
      }
    }
    locala.a(paramString1);
    break label447;
    if (paramString2.equals("COUNTRY_CODE"))
    {
      paramString1 = "country";
      paramString2 = "country";
    }
    else
    {
      paramString1 = "wildcard";
      paramString2 = "wildcard";
    }
    locala.b(paramString1);
    locala.c(null);
    locala.a(null);
    locala.a(null);
    paramString4.a("SubAction", paramString2);
    try
    {
      label447:
      ((ae)d.a()).a(locala.a());
    }
    catch (org.apache.a.a paramString1)
    {
      AssertionUtil.reportThrowableButNeverCrash(paramString1);
    }
    e.a(paramString4.a());
  }
  
  /* Error */
  public final boolean a()
  {
    // Byte code:
    //   0: iconst_1
    //   1: anewarray 209	java/lang/String
    //   4: dup
    //   5: iconst_0
    //   6: ldc_w 800
    //   9: aastore
    //   10: invokestatic 804	com/truecaller/log/AssertionUtil:notOnMainThread	([Ljava/lang/String;)V
    //   13: aload_0
    //   14: getfield 60	com/truecaller/filters/f:b	Landroid/content/Context;
    //   17: invokevirtual 234	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   20: invokestatic 239	com/truecaller/content/TruecallerContract$Filters:a	()Landroid/net/Uri;
    //   23: aconst_null
    //   24: ldc_w 806
    //   27: iconst_2
    //   28: anewarray 209	java/lang/String
    //   31: dup
    //   32: iconst_0
    //   33: ldc_w 808
    //   36: aastore
    //   37: dup
    //   38: iconst_1
    //   39: ldc_w 514
    //   42: aastore
    //   43: aconst_null
    //   44: invokevirtual 250	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   47: astore 9
    //   49: iconst_0
    //   50: istore_1
    //   51: aload 9
    //   53: ifnull +783 -> 836
    //   56: iconst_1
    //   57: istore_3
    //   58: iconst_1
    //   59: istore 4
    //   61: iconst_1
    //   62: istore_2
    //   63: aload 9
    //   65: invokeinterface 811 1 0
    //   70: ifne +6 -> 76
    //   73: goto +763 -> 836
    //   76: new 226	java/util/ArrayList
    //   79: dup
    //   80: invokespecial 227	java/util/ArrayList:<init>	()V
    //   83: astore 11
    //   85: aload 9
    //   87: aload 11
    //   89: invokestatic 816	com/truecaller/common/c/b/a:a	(Landroid/database/Cursor;Ljava/util/Collection;)V
    //   92: new 325	java/lang/StringBuilder
    //   95: dup
    //   96: ldc_w 818
    //   99: invokespecial 330	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   102: astore 7
    //   104: aload 7
    //   106: aload 11
    //   108: invokevirtual 821	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   111: pop
    //   112: aload 7
    //   114: ldc_w 823
    //   117: invokevirtual 339	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   120: pop
    //   121: aload 7
    //   123: invokevirtual 343	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   126: pop
    //   127: new 226	java/util/ArrayList
    //   130: dup
    //   131: invokespecial 227	java/util/ArrayList:<init>	()V
    //   134: astore 12
    //   136: new 226	java/util/ArrayList
    //   139: dup
    //   140: invokespecial 227	java/util/ArrayList:<init>	()V
    //   143: astore 10
    //   145: aload 11
    //   147: invokeinterface 305 1 0
    //   152: astore 13
    //   154: aload 13
    //   156: invokeinterface 199 1 0
    //   161: ifeq +337 -> 498
    //   164: aload 13
    //   166: invokeinterface 203 1 0
    //   171: checkcast 83	android/content/ContentValues
    //   174: astore 14
    //   176: aload 14
    //   178: ldc 86
    //   180: invokevirtual 190	android/content/ContentValues:getAsString	(Ljava/lang/String;)Ljava/lang/String;
    //   183: astore 15
    //   185: aload 14
    //   187: ldc 119
    //   189: invokevirtual 190	android/content/ContentValues:getAsString	(Ljava/lang/String;)Ljava/lang/String;
    //   192: astore 8
    //   194: aload 8
    //   196: astore 7
    //   198: aload 8
    //   200: invokestatic 220	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   203: ifeq +8 -> 211
    //   206: ldc_w 825
    //   209: astore 7
    //   211: aload 14
    //   213: ldc 123
    //   215: invokevirtual 829	android/content/ContentValues:getAsInteger	(Ljava/lang/String;)Ljava/lang/Integer;
    //   218: invokevirtual 460	java/lang/Integer:intValue	()I
    //   221: invokestatic 833	com/truecaller/content/TruecallerContract$Filters$EntityType:fromValue	(I)Lcom/truecaller/content/TruecallerContract$Filters$EntityType;
    //   224: astore 8
    //   226: aload 14
    //   228: ldc 117
    //   230: invokevirtual 829	android/content/ContentValues:getAsInteger	(Ljava/lang/String;)Ljava/lang/Integer;
    //   233: invokevirtual 460	java/lang/Integer:intValue	()I
    //   236: iconst_1
    //   237: if_icmpne +190 -> 427
    //   240: new 205	com/truecaller/filters/sync/c$a
    //   243: dup
    //   244: invokespecial 834	com/truecaller/filters/sync/c$a:<init>	()V
    //   247: astore 16
    //   249: aload 16
    //   251: aload 14
    //   253: ldc_w 836
    //   256: invokevirtual 190	android/content/ContentValues:getAsString	(Ljava/lang/String;)Ljava/lang/String;
    //   259: putfield 837	com/truecaller/filters/sync/c$a:a	Ljava/lang/String;
    //   262: aload 16
    //   264: aload 15
    //   266: putfield 207	com/truecaller/filters/sync/c$a:b	Ljava/lang/String;
    //   269: aload 16
    //   271: aload 14
    //   273: ldc 92
    //   275: invokevirtual 190	android/content/ContentValues:getAsString	(Ljava/lang/String;)Ljava/lang/String;
    //   278: putfield 839	com/truecaller/filters/sync/c$a:c	Ljava/lang/String;
    //   281: aload 14
    //   283: ldc 94
    //   285: invokevirtual 829	android/content/ContentValues:getAsInteger	(Ljava/lang/String;)Ljava/lang/Integer;
    //   288: iconst_m1
    //   289: invokestatic 100	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   292: invokestatic 844	org/c/a/a/a/h:a	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   295: checkcast 96	java/lang/Integer
    //   298: astore 15
    //   300: aload 15
    //   302: invokevirtual 460	java/lang/Integer:intValue	()I
    //   305: tableswitch	default:+545->850, 0:+37->342, 1:+26->331
    //   328: goto +75 -> 403
    //   331: aload 16
    //   333: ldc_w 846
    //   336: putfield 848	com/truecaller/filters/sync/c$a:d	Ljava/lang/String;
    //   339: goto +11 -> 350
    //   342: aload 16
    //   344: ldc_w 850
    //   347: putfield 848	com/truecaller/filters/sync/c$a:d	Ljava/lang/String;
    //   350: aload 16
    //   352: aload 7
    //   354: putfield 852	com/truecaller/filters/sync/c$a:e	Ljava/lang/String;
    //   357: aload 16
    //   359: aload 14
    //   361: ldc 121
    //   363: invokevirtual 190	android/content/ContentValues:getAsString	(Ljava/lang/String;)Ljava/lang/String;
    //   366: putfield 854	com/truecaller/filters/sync/c$a:f	Ljava/lang/String;
    //   369: aload 8
    //   371: getstatic 582	com/truecaller/content/TruecallerContract$Filters$EntityType:UNKNOWN	Lcom/truecaller/content/TruecallerContract$Filters$EntityType;
    //   374: if_acmpeq +16 -> 390
    //   377: aload 16
    //   379: aload 8
    //   381: getfield 127	com/truecaller/content/TruecallerContract$Filters$EntityType:value	I
    //   384: invokestatic 100	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   387: putfield 857	com/truecaller/filters/sync/c$a:g	Ljava/lang/Integer;
    //   390: aload 12
    //   392: aload 16
    //   394: invokeinterface 289 2 0
    //   399: pop
    //   400: goto -246 -> 154
    //   403: iconst_1
    //   404: anewarray 209	java/lang/String
    //   407: dup
    //   408: iconst_0
    //   409: ldc_w 859
    //   412: aload 15
    //   414: invokestatic 357	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   417: invokevirtual 862	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   420: aastore
    //   421: invokestatic 865	com/truecaller/log/AssertionUtil:report	([Ljava/lang/String;)V
    //   424: goto -270 -> 154
    //   427: aload 14
    //   429: ldc_w 836
    //   432: invokevirtual 190	android/content/ContentValues:getAsString	(Ljava/lang/String;)Ljava/lang/String;
    //   435: astore 7
    //   437: aload 14
    //   439: ldc -4
    //   441: invokevirtual 869	android/content/ContentValues:getAsLong	(Ljava/lang/String;)Ljava/lang/Long;
    //   444: invokevirtual 875	java/lang/Long:longValue	()J
    //   447: lstore 5
    //   449: aload 7
    //   451: invokestatic 220	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   454: ifne +30 -> 484
    //   457: aload 10
    //   459: lload 5
    //   461: invokestatic 878	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   464: aload 7
    //   466: ldc_w 880
    //   469: invokestatic 885	java/net/URLEncoder:encode	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   472: invokestatic 890	android/support/v4/f/j:a	(Ljava/lang/Object;Ljava/lang/Object;)Landroid/support/v4/f/j;
    //   475: invokeinterface 289 2 0
    //   480: pop
    //   481: goto -327 -> 154
    //   484: lload 5
    //   486: invokestatic 878	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   489: invokestatic 896	java/util/Collections:singletonList	(Ljava/lang/Object;)Ljava/util/List;
    //   492: invokestatic 898	com/truecaller/filters/f:a	(Ljava/util/List;)V
    //   495: goto -341 -> 154
    //   498: invokestatic 903	com/truecaller/filters/sync/a:a	()Lcom/truecaller/filters/sync/b;
    //   501: astore 7
    //   503: aload 12
    //   505: invokeinterface 181 1 0
    //   510: ifne +144 -> 654
    //   513: aload 7
    //   515: aload 12
    //   517: invokeinterface 908 2 0
    //   522: invokeinterface 913 1 0
    //   527: getfield 916	e/r:b	Ljava/lang/Object;
    //   530: checkcast 918	com/truecaller/filters/sync/c$b
    //   533: astore 8
    //   535: aload 8
    //   537: ifnull +324 -> 861
    //   540: aload 8
    //   542: getfield 921	com/truecaller/filters/sync/c$b:a	Ljava/util/List;
    //   545: ifnull +316 -> 861
    //   548: aload 8
    //   550: getfield 921	com/truecaller/filters/sync/c$b:a	Ljava/util/List;
    //   553: invokeinterface 181 1 0
    //   558: ifne +303 -> 861
    //   561: aload 11
    //   563: invokeinterface 305 1 0
    //   568: astore 11
    //   570: iconst_0
    //   571: istore_1
    //   572: aload 11
    //   574: invokeinterface 199 1 0
    //   579: ifeq +279 -> 858
    //   582: aload 11
    //   584: invokeinterface 203 1 0
    //   589: checkcast 83	android/content/ContentValues
    //   592: astore 12
    //   594: aload 12
    //   596: aload 8
    //   598: getfield 921	com/truecaller/filters/sync/c$b:a	Ljava/util/List;
    //   601: invokestatic 923	com/truecaller/filters/f:a	(Landroid/content/ContentValues;Ljava/util/List;)Lcom/truecaller/filters/sync/c$a;
    //   604: astore 13
    //   606: aload 13
    //   608: ifnull +245 -> 853
    //   611: aload 12
    //   613: ldc 117
    //   615: iconst_0
    //   616: invokestatic 100	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   619: invokevirtual 103	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   622: aload 12
    //   624: ldc_w 836
    //   627: aload 13
    //   629: getfield 837	com/truecaller/filters/sync/c$a:a	Ljava/lang/String;
    //   632: invokevirtual 90	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   635: aload_0
    //   636: getfield 60	com/truecaller/filters/f:b	Landroid/content/Context;
    //   639: invokevirtual 234	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   642: invokestatic 239	com/truecaller/content/TruecallerContract$Filters:a	()Landroid/net/Uri;
    //   645: aload 12
    //   647: invokevirtual 927	android/content/ContentResolver:insert	(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    //   650: pop
    //   651: goto -79 -> 572
    //   654: new 10	com/truecaller/filters/f$b
    //   657: dup
    //   658: ldc_w 929
    //   661: invokeinterface 930 1 0
    //   666: invokespecial 931	com/truecaller/filters/f$b:<init>	(Ljava/lang/String;)V
    //   669: aload 10
    //   671: invokevirtual 934	com/truecaller/filters/f$b:a	(Ljava/lang/Iterable;)Ljava/lang/String;
    //   674: astore 8
    //   676: aload 8
    //   678: invokestatic 936	com/truecaller/common/h/am:b	(Ljava/lang/CharSequence;)Z
    //   681: ifne +90 -> 771
    //   684: aload 7
    //   686: aload 8
    //   688: invokeinterface 939 2 0
    //   693: invokeinterface 913 1 0
    //   698: getfield 942	e/r:a	Lokhttp3/ad;
    //   701: invokevirtual 945	okhttp3/ad:c	()Z
    //   704: ifeq +69 -> 773
    //   707: new 226	java/util/ArrayList
    //   710: dup
    //   711: aload 10
    //   713: invokeinterface 268 1 0
    //   718: invokespecial 948	java/util/ArrayList:<init>	(I)V
    //   721: astore 7
    //   723: aload 10
    //   725: invokeinterface 194 1 0
    //   730: astore 8
    //   732: aload 8
    //   734: invokeinterface 199 1 0
    //   739: ifeq +27 -> 766
    //   742: aload 7
    //   744: aload 8
    //   746: invokeinterface 203 1 0
    //   751: checkcast 887	android/support/v4/f/j
    //   754: getfield 949	android/support/v4/f/j:a	Ljava/lang/Object;
    //   757: invokeinterface 289 2 0
    //   762: pop
    //   763: goto -31 -> 732
    //   766: aload 7
    //   768: invokestatic 898	com/truecaller/filters/f:a	(Ljava/util/List;)V
    //   771: iload_1
    //   772: istore_2
    //   773: iload_2
    //   774: istore_1
    //   775: aload 9
    //   777: ifnull +42 -> 819
    //   780: aload 9
    //   782: invokeinterface 292 1 0
    //   787: iload_2
    //   788: ireturn
    //   789: astore 7
    //   791: goto +30 -> 821
    //   794: astore 7
    //   796: goto +5 -> 801
    //   799: astore 7
    //   801: aload 7
    //   803: invokestatic 786	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   806: iload 4
    //   808: istore_1
    //   809: aload 9
    //   811: ifnull +8 -> 819
    //   814: iload_3
    //   815: istore_2
    //   816: goto -36 -> 780
    //   819: iload_1
    //   820: ireturn
    //   821: aload 9
    //   823: ifnull +10 -> 833
    //   826: aload 9
    //   828: invokeinterface 292 1 0
    //   833: aload 7
    //   835: athrow
    //   836: aload 9
    //   838: ifnull +10 -> 848
    //   841: aload 9
    //   843: invokeinterface 292 1 0
    //   848: iconst_0
    //   849: ireturn
    //   850: goto -522 -> 328
    //   853: iconst_1
    //   854: istore_1
    //   855: goto -283 -> 572
    //   858: goto -204 -> 654
    //   861: iconst_1
    //   862: istore_1
    //   863: goto -209 -> 654
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	866	0	this	f
    //   50	813	1	bool1	boolean
    //   62	754	2	bool2	boolean
    //   57	758	3	bool3	boolean
    //   59	748	4	bool4	boolean
    //   447	38	5	l	long
    //   102	665	7	localObject1	Object
    //   789	1	7	localObject2	Object
    //   794	1	7	localRuntimeException	RuntimeException
    //   799	35	7	localIOException	IOException
    //   192	553	8	localObject3	Object
    //   47	795	9	localCursor	Cursor
    //   143	581	10	localArrayList	ArrayList
    //   83	500	11	localObject4	Object
    //   134	512	12	localObject5	Object
    //   152	476	13	localObject6	Object
    //   174	264	14	localContentValues	ContentValues
    //   183	230	15	localObject7	Object
    //   247	146	16	locala	com.truecaller.filters.sync.c.a
    // Exception table:
    //   from	to	target	type
    //   63	73	789	finally
    //   76	154	789	finally
    //   154	194	789	finally
    //   198	206	789	finally
    //   211	328	789	finally
    //   331	339	789	finally
    //   342	350	789	finally
    //   350	390	789	finally
    //   390	400	789	finally
    //   403	424	789	finally
    //   427	481	789	finally
    //   484	495	789	finally
    //   498	503	789	finally
    //   503	535	789	finally
    //   540	570	789	finally
    //   572	606	789	finally
    //   611	651	789	finally
    //   654	684	789	finally
    //   684	732	789	finally
    //   732	763	789	finally
    //   766	771	789	finally
    //   801	806	789	finally
    //   63	73	794	java/lang/RuntimeException
    //   76	154	794	java/lang/RuntimeException
    //   154	194	794	java/lang/RuntimeException
    //   198	206	794	java/lang/RuntimeException
    //   211	328	794	java/lang/RuntimeException
    //   331	339	794	java/lang/RuntimeException
    //   342	350	794	java/lang/RuntimeException
    //   350	390	794	java/lang/RuntimeException
    //   390	400	794	java/lang/RuntimeException
    //   403	424	794	java/lang/RuntimeException
    //   427	481	794	java/lang/RuntimeException
    //   484	495	794	java/lang/RuntimeException
    //   498	503	794	java/lang/RuntimeException
    //   503	535	794	java/lang/RuntimeException
    //   540	570	794	java/lang/RuntimeException
    //   572	606	794	java/lang/RuntimeException
    //   611	651	794	java/lang/RuntimeException
    //   654	684	794	java/lang/RuntimeException
    //   684	732	794	java/lang/RuntimeException
    //   732	763	794	java/lang/RuntimeException
    //   766	771	794	java/lang/RuntimeException
    //   63	73	799	java/io/IOException
    //   76	154	799	java/io/IOException
    //   154	194	799	java/io/IOException
    //   198	206	799	java/io/IOException
    //   211	328	799	java/io/IOException
    //   331	339	799	java/io/IOException
    //   342	350	799	java/io/IOException
    //   350	390	799	java/io/IOException
    //   390	400	799	java/io/IOException
    //   403	424	799	java/io/IOException
    //   427	481	799	java/io/IOException
    //   484	495	799	java/io/IOException
    //   498	503	799	java/io/IOException
    //   503	535	799	java/io/IOException
    //   540	570	799	java/io/IOException
    //   572	606	799	java/io/IOException
    //   611	651	799	java/io/IOException
    //   654	684	799	java/io/IOException
    //   684	732	799	java/io/IOException
    //   732	763	799	java/io/IOException
    //   766	771	799	java/io/IOException
  }
  
  public final g b(String paramString)
  {
    return a(paramString, null, null, true);
  }
  
  public final boolean b()
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
  }
  
  public final boolean c()
    throws IOException
  {
    if (com.truecaller.common.c.b.b.a(b.getContentResolver(), TruecallerContract.Filters.a(), null, null) > 0)
    {
      AssertionUtil.report(new String[] { "Filters are already present" });
      return true;
    }
    Object localObject1 = com.truecaller.filters.sync.a.a();
    Object localObject2 = ((com.truecaller.filters.sync.b)localObject1).b().c();
    boolean bool1 = a.c();
    boolean bool2 = false;
    com.truecaller.filters.sync.c.a locala;
    if (bool1)
    {
      Object localObject3 = (c.b)b;
      if ((localObject3 != null) && (a != null) && (!a.isEmpty()))
      {
        localObject2 = new ArrayList();
        localObject3 = a.iterator();
        if (((Iterator)localObject3).hasNext()) {
          locala = (com.truecaller.filters.sync.c.a)((Iterator)localObject3).next();
        }
      }
    }
    for (;;)
    {
      try
      {
        if (!TextUtils.isEmpty(b))
        {
          Object localObject4 = new ContentValues();
          ((ContentValues)localObject4).put("server_id", a);
          ((ContentValues)localObject4).put("value", b);
          ((ContentValues)localObject4).put("label", c);
          int m;
          if ("BLACKLIST".equals(d))
          {
            m = 0;
          }
          else
          {
            if (!"WHITELIST".equals(d)) {
              continue;
            }
            m = 1;
          }
          ((ContentValues)localObject4).put("rule", Integer.valueOf(m));
          ((ContentValues)localObject4).put("wildcard_type", Integer.valueOf(TruecallerContract.Filters.WildCardType.valueOfPattern(b).getType()));
          ((ContentValues)localObject4).put("sync_state", Integer.valueOf(0));
          ((ContentValues)localObject4).put("tracking_source", f);
          ((ContentValues)localObject4).put("tracking_type", e);
          if (TextUtils.isEmpty(((ContentValues)localObject4).getAsString("server_id"))) {
            break label624;
          }
          bool1 = true;
          AssertionUtil.isTrue(bool1, new String[0]);
          if (TextUtils.isEmpty(((ContentValues)localObject4).getAsString("value"))) {
            break label629;
          }
          bool1 = true;
          AssertionUtil.isTrue(bool1, new String[0]);
          if (((ContentValues)localObject4).getAsLong("wildcard_type").longValue() < 0L) {
            break label634;
          }
          bool1 = true;
          AssertionUtil.isTrue(bool1, new String[0]);
          ((Collection)localObject2).add(localObject4);
          break;
          localObject4 = new StringBuilder("Unknown backend filter rule ");
          ((StringBuilder)localObject4).append(d);
          throw new IllegalArgumentException(((StringBuilder)localObject4).toString());
        }
        throw new IllegalArgumentException("Filter value is empty");
      }
      catch (IllegalArgumentException localIllegalArgumentException)
      {
        AssertionUtil.reportThrowableButNeverCrash(localIllegalArgumentException);
      }
      break;
      a((Collection)localObject2, false);
      localObject1 = (c.c)acb;
      if (localObject1 != null)
      {
        localObject2 = c;
        if (a > 0) {
          bool1 = true;
        } else {
          bool1 = false;
        }
        ((p)localObject2).f(bool1);
        localObject2 = c;
        if (b > 0) {
          bool1 = true;
        } else {
          bool1 = false;
        }
        ((p)localObject2).a(bool1);
        localObject2 = c;
        bool1 = bool2;
        if (g.S().a())
        {
          bool1 = bool2;
          if (k.d())
          {
            bool1 = bool2;
            if (g > 0) {
              bool1 = true;
            }
          }
        }
        ((p)localObject2).e(bool1);
      }
      c.a(System.currentTimeMillis());
      return true;
      AssertionUtil.reportWeirdnessButNeverCrash("Could not restore filters");
      return false;
      label624:
      bool1 = false;
      continue;
      label629:
      bool1 = false;
      continue;
      label634:
      bool1 = false;
    }
  }
  
  public final void d()
  {
    b.getContentResolver().delete(TruecallerContract.Filters.a(), "rule=1", null);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */