package com.truecaller.filters.sync;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import androidx.work.c.a;
import androidx.work.g;
import androidx.work.j;
import androidx.work.k.a;
import androidx.work.p;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.account.r;
import com.truecaller.filters.FilterManager;
import com.truecaller.log.AssertionUtil;
import java.io.IOException;
import javax.inject.Inject;

public final class FilterUploadWorker
  extends Worker
{
  public static final FilterUploadWorker.a d = new FilterUploadWorker.a((byte)0);
  @Inject
  public r b;
  @Inject
  public FilterManager c;
  
  public FilterUploadWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    c.g.b.k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public static final void b()
  {
    p.a().a("FilterUploadWorker", g.a, (androidx.work.k)((k.a)new k.a(FilterUploadWorker.class).a(new c.a().a(j.b).a())).c());
  }
  
  public final ListenableWorker.a a()
  {
    Object localObject = b;
    if (localObject == null) {
      c.g.b.k.a("accountManager");
    }
    if (!((r)localObject).c())
    {
      localObject = ListenableWorker.a.a();
      c.g.b.k.a(localObject, "Result.success()");
      return (ListenableWorker.a)localObject;
    }
    try
    {
      localObject = c;
      if (localObject == null) {
        c.g.b.k.a("filterManager");
      }
      if (!((FilterManager)localObject).a())
      {
        localObject = ListenableWorker.a.a();
        c.g.b.k.a(localObject, "Result.success()");
        return (ListenableWorker.a)localObject;
      }
    }
    catch (Exception localException)
    {
      AssertionUtil.shouldNeverHappen((Throwable)localException, new String[0]);
      break label96;
      ListenableWorker.a.b();
      break label96;
      ListenableWorker.a.b();
      ListenableWorker.a locala = ListenableWorker.a.c();
      c.g.b.k.a(locala, "Result.failure()");
      return locala;
    }
    catch (RuntimeException localRuntimeException)
    {
      for (;;) {}
    }
    catch (IOException localIOException)
    {
      label96:
      for (;;) {}
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.sync.FilterUploadWorker
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */