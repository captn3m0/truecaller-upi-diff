package com.truecaller.filters.sync;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.account.r;
import com.truecaller.filters.FilterManager;
import java.io.IOException;
import javax.inject.Inject;

public final class FilterSettingsUploadWorker
  extends Worker
{
  public static final FilterSettingsUploadWorker.a d = new FilterSettingsUploadWorker.a((byte)0);
  @Inject
  public r b;
  @Inject
  public FilterManager c;
  
  public FilterSettingsUploadWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public static final void b()
  {
    FilterSettingsUploadWorker.a.a(null);
  }
  
  public final ListenableWorker.a a()
  {
    Object localObject = b;
    if (localObject == null) {
      k.a("accountManager");
    }
    if (!((r)localObject).c())
    {
      localObject = ListenableWorker.a.a();
      k.a(localObject, "Result.success()");
      return (ListenableWorker.a)localObject;
    }
    try
    {
      localObject = c;
      if (localObject == null) {
        k.a("filterManager");
      }
      if (((FilterManager)localObject).b()) {
        break label81;
      }
      localObject = ListenableWorker.a.a();
      k.a(localObject, "Result.success()");
      return (ListenableWorker.a)localObject;
    }
    catch (RuntimeException localRuntimeException)
    {
      for (;;) {}
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
    ListenableWorker.a.b();
    break label81;
    ListenableWorker.a.b();
    label81:
    localObject = ListenableWorker.a.c();
    k.a(localObject, "Result.failure()");
    return (ListenableWorker.a)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.sync.FilterSettingsUploadWorker
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */