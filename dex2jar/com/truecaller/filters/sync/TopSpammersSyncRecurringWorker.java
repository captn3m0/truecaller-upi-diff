package com.truecaller.filters.sync;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.bp;
import com.truecaller.common.account.r;
import com.truecaller.common.background.TrackedWorker;
import com.truecaller.filters.v;
import com.truecaller.log.d;
import javax.inject.Inject;

public final class TopSpammersSyncRecurringWorker
  extends TrackedWorker
{
  public static final TopSpammersSyncRecurringWorker.a e = new TopSpammersSyncRecurringWorker.a((byte)0);
  @Inject
  public b b;
  @Inject
  public r c;
  @Inject
  public v d;
  private final Context f;
  
  public TopSpammersSyncRecurringWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    f = paramContext;
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public final b b()
  {
    b localb = b;
    if (localb == null) {
      k.a("analytics");
    }
    return localb;
  }
  
  public final boolean c()
  {
    r localr = c;
    if (localr == null) {
      k.a("accountManager");
    }
    return localr.c();
  }
  
  public final ListenableWorker.a d()
  {
    try
    {
      Object localObject = d;
      if (localObject == null) {
        k.a("topSpammerRepository");
      }
      if (((v)localObject).a())
      {
        localObject = ListenableWorker.a.a();
        k.a(localObject, "Result.success()");
        return (ListenableWorker.a)localObject;
      }
    }
    catch (Exception localException)
    {
      d.a((Throwable)localException);
      ListenableWorker.a locala = ListenableWorker.a.b();
      k.a(locala, "Result.retry()");
      return locala;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.sync.TopSpammersSyncRecurringWorker
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */