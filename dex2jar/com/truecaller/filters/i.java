package com.truecaller.filters;

import com.truecaller.common.h.u;
import dagger.a.d;
import javax.inject.Provider;

public final class i
  implements d<r>
{
  private final h a;
  private final Provider<com.truecaller.common.account.r> b;
  private final Provider<u> c;
  
  private i(h paramh, Provider<com.truecaller.common.account.r> paramProvider, Provider<u> paramProvider1)
  {
    a = paramh;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static i a(h paramh, Provider<com.truecaller.common.account.r> paramProvider, Provider<u> paramProvider1)
  {
    return new i(paramh, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.filters.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */