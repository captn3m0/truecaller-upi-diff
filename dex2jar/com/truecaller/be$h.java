package com.truecaller;

import com.truecaller.backup.l;
import com.truecaller.calling.a.b;
import com.truecaller.calling.contacts_list.ContactsHolder;
import com.truecaller.calling.contacts_list.aa;
import com.truecaller.calling.contacts_list.ab;
import com.truecaller.calling.contacts_list.ac;
import com.truecaller.calling.contacts_list.ad;
import com.truecaller.calling.contacts_list.b.b;
import com.truecaller.calling.contacts_list.data.SortedContactsDao;
import com.truecaller.calling.contacts_list.data.SortedContactsRepository;
import com.truecaller.calling.contacts_list.data.i;
import com.truecaller.calling.contacts_list.j.a;
import com.truecaller.calling.contacts_list.k;
import com.truecaller.calling.contacts_list.l.a;
import com.truecaller.calling.contacts_list.o;
import com.truecaller.calling.contacts_list.p.a;
import com.truecaller.calling.contacts_list.q;
import com.truecaller.calling.contacts_list.v;
import com.truecaller.calling.contacts_list.y;
import com.truecaller.calling.contacts_list.z;
import com.truecaller.calling.dialer.s;
import com.truecaller.ui.view.VoipTintedImageView;
import com.truecaller.utils.t;
import javax.inject.Provider;

final class be$h
  implements com.truecaller.calling.contacts_list.j
{
  private Provider<com.truecaller.calling.contacts_list.data.a> b = com.truecaller.calling.contacts_list.data.c.a(be.X(a));
  private Provider<SortedContactsDao> c = dagger.a.c.a(b);
  private Provider<com.truecaller.calling.contacts_list.data.d> d = com.truecaller.calling.contacts_list.data.f.a(c, be.l(a));
  private Provider<SortedContactsRepository> e = dagger.a.c.a(d);
  private Provider<com.truecaller.search.local.model.c> f = dagger.a.c.a(be.aq(a));
  private Provider<com.truecaller.calling.contacts_list.r> g = v.a(e, be.w(a), be.G(a), be.ar(a), be.C(a), f, be.S(a), be.t(a));
  private Provider<p.a> h = dagger.a.c.a(g);
  private Provider<s> i = dagger.a.c.a(o.a(be.X(a)));
  private Provider<s> j = dagger.a.c.a(com.truecaller.calling.contacts_list.n.a(be.X(a)));
  private Provider<com.truecaller.calling.contacts_list.j> k = dagger.a.e.a(this);
  private Provider<ac> l = ad.a(k);
  private Provider<ab> m = dagger.a.c.a(l);
  private Provider<com.truecaller.calling.contacts_list.data.g> n = dagger.a.c.a(i.a());
  private Provider<z> o;
  private Provider<y> p;
  private Provider<com.truecaller.backup.e> q;
  private Provider<com.truecaller.calling.contacts_list.c> r;
  private Provider<b.b> s;
  private Provider<b> t;
  private Provider<com.truecaller.calling.a.a> u;
  private Provider<com.truecaller.calling.a.f> v;
  private Provider<com.truecaller.calling.a.e> w;
  
  private be$h(be parambe)
  {
    parambe = h;
    o = aa.a(parambe, f, parambe, be.S(a), n);
    p = dagger.a.c.a(o);
    q = dagger.a.c.a(l.a(be.as(a)));
    parambe = h;
    r = com.truecaller.calling.contacts_list.d.a(parambe, parambe, be.w(a), q, be.S(a), be.at(a));
    s = dagger.a.c.a(r);
    t = com.truecaller.calling.a.c.a(be.X(a));
    u = dagger.a.c.a(t);
    v = com.truecaller.calling.a.g.a(u, be.B(a));
    w = dagger.a.c.a(v);
  }
  
  public final com.truecaller.calling.a.e a()
  {
    return (com.truecaller.calling.a.e)w.get();
  }
  
  public final j.a a(l.a parama)
  {
    dagger.a.g.a(parama);
    return new be.h.a(this, parama, (byte)0);
  }
  
  public final void a(k paramk)
  {
    a = ((p.a)h.get());
    b = ((s)i.get());
    c = ((s)j.get());
    d = new q((ContactsHolder)h.get(), (p.a)h.get(), (ab)m.get(), (com.truecaller.utils.n)dagger.a.g.a(be.L(a).g(), "Cannot return null from a non-@Nullable component method"), (y)p.get(), (com.truecaller.common.account.r)dagger.a.g.a(be.f(a).k(), "Cannot return null from a non-@Nullable component method"), (b.b)s.get(), (com.truecaller.i.a)be.ap(a).get());
    e = be.au(a);
  }
  
  public final void a(VoipTintedImageView paramVoipTintedImageView)
  {
    a = be.au(a);
    b = ((com.truecaller.voip.d)dagger.a.g.a(be.av(a).b(), "Cannot return null from a non-@Nullable component method"));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.be.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */