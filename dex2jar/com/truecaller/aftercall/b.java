package com.truecaller.aftercall;

import android.text.TextUtils;
import com.truecaller.callerid.i;
import com.truecaller.common.h.ab;
import com.truecaller.common.h.an;
import com.truecaller.common.h.j;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.Number;
import com.truecaller.i.c;
import com.truecaller.util.al;
import com.truecaller.util.cn;
import java.util.Calendar;

public final class b
  implements a
{
  private final cn a;
  private final c b;
  private final al c;
  private final an d;
  
  public b(cn paramcn, c paramc, al paramal, com.truecaller.utils.a parama)
  {
    a = paramcn;
    b = paramc;
    c = paramal;
    d = new an(parama);
  }
  
  private boolean a(PromotionCategory paramPromotionCategory)
  {
    if (paramPromotionCategory == PromotionCategory.DIALER)
    {
      if (b.b("hasNativeDialerCallerId")) {
        return false;
      }
      if (!j.a(b.a("lastCallMadeWithTcTime", 0L), 604800000L)) {
        return false;
      }
      if (!j.a(b.a("lastDialerPromotionTime", 0L), 86400000L)) {
        return false;
      }
    }
    return true;
  }
  
  public final PromotionType a(i parami, HistoryEvent paramHistoryEvent)
  {
    if (!a(PromotionCategory.DIALER)) {
      return null;
    }
    if ((paramHistoryEvent != null) && (f != null) && (!TextUtils.isEmpty(f.t())))
    {
      if (ab.a(a.m()) != 2) {
        return null;
      }
      if ((!e) && (!f))
      {
        parami = PromotionType.DIALER_OUTGOING_OUTSIDE;
        break label75;
      }
    }
    parami = null;
    label75:
    if ((parami != null) && (a(parami, paramHistoryEvent))) {
      return parami;
    }
    return null;
  }
  
  public final boolean a(PromotionType paramPromotionType, HistoryEvent paramHistoryEvent)
  {
    if (!a.a())
    {
      if (!paramPromotionType.isEnabled()) {
        return false;
      }
      long l1;
      if (paramPromotionType == PromotionType.SIGN_UP)
      {
        l1 = b.a("afterCallPromoteTcTimestamp", 0L);
        return (d.a(c.f(), 864000000L)) && (d.a(l1, 604800000L));
      }
      if (category == PromotionCategory.PERMISSION)
      {
        l1 = b.a("afterCallPromotePhonePermissionTimestamp", 0L);
        long l2 = b.a("afterCallPromoteContactsPermissionTimestamp", 0L);
        if (paramPromotionType != PromotionType.PHONE_PERMISSION) {
          l1 = l2;
        }
        return j.a(l1, 86400000L);
      }
      if (category == PromotionCategory.DIALER) {
        return (Calendar.getInstance().get(7) == 7) && (paramHistoryEvent != null) && (f != null) && (a(PromotionCategory.DIALER));
      }
      return false;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.aftercall.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */