package com.truecaller;

import com.truecaller.util.b;
import com.truecaller.util.cn;
import dagger.a.d;
import javax.inject.Provider;

public final class aw
  implements d<cn>
{
  private final c a;
  private final Provider<b> b;
  
  private aw(c paramc, Provider<b> paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static aw a(c paramc, Provider<b> paramProvider)
  {
    return new aw(paramc, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.aw
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */