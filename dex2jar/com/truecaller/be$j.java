package com.truecaller;

import com.truecaller.clevertap.j;
import com.truecaller.fcm.FcmMessageListenerService;
import com.truecaller.fcm.d;
import com.truecaller.fcm.g;
import com.truecaller.messaging.transport.im.ap;
import javax.inject.Provider;

final class be$j
  implements com.truecaller.fcm.a
{
  private Provider<com.truecaller.flashsdk.core.a.a.a> b;
  private Provider<com.google.gson.f> c;
  private Provider<com.truecaller.flashsdk.c.a> d;
  private Provider<com.truecaller.flashsdk.b.a> e;
  private Provider<j> f;
  
  private be$j(be parambe, com.truecaller.fcm.b paramb)
  {
    b = dagger.a.c.a(d.a(paramb));
    c = dagger.a.c.a(g.a(paramb, b));
    d = dagger.a.c.a(com.truecaller.fcm.f.a(paramb, b));
    e = dagger.a.c.a(com.truecaller.fcm.e.a(paramb, be.o(a), c, d));
    f = dagger.a.c.a(com.truecaller.fcm.c.a(paramb, be.o(a)));
  }
  
  public final void a(FcmMessageListenerService paramFcmMessageListenerService)
  {
    b = ((com.truecaller.flashsdk.b.a)e.get());
    c = ((j)f.get());
    d = ((ap)be.N(a).get());
    e = ((com.truecaller.b.c)be.O(a).get());
    f = ((com.truecaller.featuretoggles.e)be.a(a).get());
    g = ((com.truecaller.push.e)be.P(a).get());
    h = ((com.truecaller.push.b)be.Q(a).get());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.be.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */