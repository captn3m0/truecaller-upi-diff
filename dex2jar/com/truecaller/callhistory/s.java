package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import com.truecaller.data.entity.CallRecording;
import java.util.Collection;

public final class s
  implements r
{
  private final v a;
  
  public s(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return r.class.equals(paramClass);
  }
  
  public final w<z> a()
  {
    return w.a(a, new c(new e(), (byte)0));
  }
  
  public final w<Boolean> a(CallRecording paramCallRecording)
  {
    return w.a(a, new a(new e(), paramCallRecording, (byte)0));
  }
  
  public final w<Boolean> a(Collection<Long> paramCollection)
  {
    return w.a(a, new b(new e(), paramCollection, (byte)0));
  }
  
  static final class a
    extends u<r, Boolean>
  {
    private final CallRecording b;
    
    private a(e parame, CallRecording paramCallRecording)
    {
      super();
      b = paramCallRecording;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".delete(");
      localStringBuilder.append(a(b, 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class b
    extends u<r, Boolean>
  {
    private final Collection<Long> b;
    
    private b(e parame, Collection<Long> paramCollection)
    {
      super();
      b = paramCollection;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".delete(");
      localStringBuilder.append(a(b, 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class c
    extends u<r, z>
  {
    private c(e parame)
    {
      super();
    }
    
    public final String toString()
    {
      return ".getAllCallRecordings()";
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.s
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */