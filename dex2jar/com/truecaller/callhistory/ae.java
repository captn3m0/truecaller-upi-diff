package com.truecaller.callhistory;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.Uri;
import org.c.a.a.a.a;

@TargetApi(22)
public final class ae
  extends ab
{
  private static final Uri b = Uri.parse("content://logs/call");
  private static final String[] c = (String[])a.a(a, new String[] { "normalized_number", "features", "subscription_component_name", "logtype" });
  
  ae(Context paramContext)
  {
    super(paramContext);
  }
  
  /* Error */
  public static boolean b(Context paramContext)
  {
    // Byte code:
    //   0: ldc 56
    //   2: getstatic 62	android/os/Build:BRAND	Ljava/lang/String;
    //   5: invokevirtual 66	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
    //   8: ifne +5 -> 13
    //   11: iconst_0
    //   12: ireturn
    //   13: aload_0
    //   14: invokevirtual 72	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   17: getstatic 23	com/truecaller/callhistory/ae:b	Landroid/net/Uri;
    //   20: bipush 10
    //   22: anewarray 28	java/lang/String
    //   25: dup
    //   26: iconst_0
    //   27: ldc 74
    //   29: aastore
    //   30: dup
    //   31: iconst_1
    //   32: ldc 76
    //   34: aastore
    //   35: dup
    //   36: iconst_2
    //   37: ldc 78
    //   39: aastore
    //   40: dup
    //   41: iconst_3
    //   42: ldc 32
    //   44: aastore
    //   45: dup
    //   46: iconst_4
    //   47: ldc 80
    //   49: aastore
    //   50: dup
    //   51: iconst_5
    //   52: ldc 82
    //   54: aastore
    //   55: dup
    //   56: bipush 6
    //   58: ldc 34
    //   60: aastore
    //   61: dup
    //   62: bipush 7
    //   64: ldc 84
    //   66: aastore
    //   67: dup
    //   68: bipush 8
    //   70: ldc 86
    //   72: aastore
    //   73: dup
    //   74: bipush 9
    //   76: ldc 88
    //   78: aastore
    //   79: aconst_null
    //   80: aconst_null
    //   81: aconst_null
    //   82: invokevirtual 94	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   85: astore_2
    //   86: aload_2
    //   87: ifnull +9 -> 96
    //   90: aload_2
    //   91: invokeinterface 99 1 0
    //   96: aload_2
    //   97: ifnull +5 -> 102
    //   100: iconst_1
    //   101: ireturn
    //   102: iconst_0
    //   103: ireturn
    //   104: astore_0
    //   105: goto +27 -> 132
    //   108: getstatic 105	android/os/Build$VERSION:SDK_INT	I
    //   111: bipush 23
    //   113: if_icmplt +17 -> 130
    //   116: aload_0
    //   117: ldc 107
    //   119: invokevirtual 111	android/content/Context:checkSelfPermission	(Ljava/lang/String;)I
    //   122: istore_1
    //   123: iload_1
    //   124: iconst_m1
    //   125: if_icmpne +5 -> 130
    //   128: iconst_1
    //   129: ireturn
    //   130: iconst_0
    //   131: ireturn
    //   132: aload_0
    //   133: athrow
    //   134: astore_2
    //   135: goto -27 -> 108
    //   138: astore_0
    //   139: iconst_0
    //   140: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	141	0	paramContext	Context
    //   122	4	1	i	int
    //   85	12	2	localCursor	android.database.Cursor
    //   134	1	2	localSecurityException	SecurityException
    // Exception table:
    //   from	to	target	type
    //   13	86	104	finally
    //   108	123	104	finally
    //   13	86	134	java/lang/SecurityException
    //   13	86	138	java/lang/Exception
  }
  
  public final Uri a()
  {
    return b;
  }
  
  public final String[] b()
  {
    return c;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.ae
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */