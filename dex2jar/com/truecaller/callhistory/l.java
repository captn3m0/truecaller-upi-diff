package com.truecaller.callhistory;

import com.truecaller.androidactors.w;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import java.util.List;
import java.util.Set;

public abstract interface l
{
  public abstract w<z> a();
  
  public abstract w<z> a(int paramInt);
  
  public abstract w<z> a(long paramLong);
  
  public abstract w<List<HistoryEvent>> a(FilterType paramFilterType, Integer paramInteger);
  
  public abstract w<z> a(Contact paramContact);
  
  public abstract w<z> a(String paramString);
  
  public abstract w<Integer> a(List<? extends HistoryEvent> paramList);
  
  public abstract boolean a(HistoryEvent paramHistoryEvent);
  
  public abstract boolean a(Set<Long> paramSet);
  
  public abstract w<Integer> b();
  
  public abstract w<HistoryEvent> b(String paramString);
  
  public abstract void b(long paramLong);
  
  public abstract w<HistoryEvent> c(String paramString);
  
  public abstract void c();
  
  public abstract void c(long paramLong);
  
  public abstract void d();
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */