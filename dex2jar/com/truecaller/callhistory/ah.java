package com.truecaller.callhistory;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class ah
  implements d<ag>
{
  private final Provider<Context> a;
  
  private ah(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static ah a(Provider<Context> paramProvider)
  {
    return new ah(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.ah
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */