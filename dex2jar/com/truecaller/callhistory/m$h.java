package com.truecaller.callhistory;

import android.database.Cursor;
import c.g.a.b;
import c.g.b.j;
import c.g.b.w;
import c.l.c;
import c.x;

final class m$h
  extends j
  implements b<Cursor, x>
{
  public static final h a = new h();
  
  public final c a()
  {
    return w.a(z.class);
  }
  
  public final String b()
  {
    return "close";
  }
  
  public final String c()
  {
    return "close()V";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.m.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */