package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class b$i
  extends u<a, z>
{
  private final String b;
  
  private b$i(e parame, String paramString)
  {
    super(parame);
    b = paramString;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".getCallHistoryByNumber(");
    localStringBuilder.append(a(b, 1));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */