package com.truecaller.callhistory;

import android.content.Context;
import android.net.Uri;
import android.os.Build.VERSION;
import android.provider.CallLog.Calls;

public abstract class k
{
  static final String[] a = { "_id", "date", "number", "type", "duration", "name", "new", "is_read" };
  private static volatile k b;
  
  public static k a(Context paramContext)
  {
    k localk = b;
    if (localk != null) {
      return localk;
    }
    try
    {
      localk = b;
      if (localk != null) {
        return localk;
      }
      if (Build.VERSION.SDK_INT >= 22)
      {
        if (ae.b(paramContext)) {
          paramContext = new ae(paramContext);
        } else {
          paramContext = new ab(paramContext);
        }
      }
      else {
        paramContext = new y();
      }
      b = paramContext;
      return paramContext;
    }
    finally {}
  }
  
  public abstract int a(int paramInt);
  
  public Uri a()
  {
    return CallLog.Calls.CONTENT_URI;
  }
  
  public abstract String[] b();
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */