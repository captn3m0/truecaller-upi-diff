package com.truecaller.callhistory;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import c.g.a.b;
import c.g.b.j;
import c.u;
import c.x;
import com.google.android.gms.tasks.RuntimeExecutionException;
import com.truecaller.androidactors.ab;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.service.WidgetListProvider;
import com.truecaller.voip.d;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;

public final class m
  implements l
{
  final com.truecaller.calling.e.e a;
  final com.truecaller.common.g.a b;
  final d c;
  private final Context d;
  private final k e;
  private final com.truecaller.i.c f;
  
  @Inject
  public m(Context paramContext, com.truecaller.calling.e.e parame, com.truecaller.common.g.a parama, k paramk, com.truecaller.i.c paramc, d paramd)
  {
    d = paramContext;
    a = parame;
    b = parama;
    e = paramk;
    f = paramc;
    c = paramd;
  }
  
  private final String d(String paramString)
  {
    return p.a(p.a(p.a(paramString, "tc_flag!=3", (c.g.a.a)new d(this)), "(subscription_component_name!='com.whatsapp' OR subscription_component_name IS NULL)", (c.g.a.a)new e(this)), "(subscription_component_name!='com.truecaller.voip.manager.VOIP' OR subscription_component_name IS NULL)", (c.g.a.a)new f(this));
  }
  
  public final com.truecaller.androidactors.w<z> a()
  {
    try
    {
      localObject = d.getContentResolver().query(TruecallerContract.n.d(), null, d("type IN (1,2,3) "), null, "timestamp DESC");
      if (localObject == null) {
        break label68;
      }
      try
      {
        com.truecaller.androidactors.w localw = com.truecaller.androidactors.w.a(e.a((Cursor)localObject), (ab)a.a);
        c.g.b.k.a(localw, "wrap<HistoryEventCursor>…tCursor()) { it.close() }");
        return localw;
      }
      catch (SQLiteException localSQLiteException1) {}
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localSQLiteException2);
    }
    catch (SQLiteException localSQLiteException2)
    {
      localObject = null;
    }
    com.truecaller.util.q.a((Cursor)localObject);
    label68:
    Object localObject = com.truecaller.androidactors.w.b(null);
    c.g.b.k.a(localObject, "wrap(null)");
    return (com.truecaller.androidactors.w<z>)localObject;
  }
  
  public final com.truecaller.androidactors.w<z> a(int paramInt)
  {
    Cursor localCursor;
    try
    {
      localCursor = d.getContentResolver().query(TruecallerContract.n.a(paramInt), null, null, null, null);
      if (localCursor == null) {
        break label63;
      }
      try
      {
        com.truecaller.androidactors.w localw1 = com.truecaller.androidactors.w.a(e.a(localCursor), (ab)g.a);
        c.g.b.k.a(localw1, "wrap<HistoryEventCursor>…tCursor()) { it.close() }");
        return localw1;
      }
      catch (SQLiteException localSQLiteException1) {}
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localSQLiteException2);
    }
    catch (SQLiteException localSQLiteException2)
    {
      localCursor = null;
    }
    com.truecaller.util.q.a(localCursor);
    label63:
    com.truecaller.androidactors.w localw2 = com.truecaller.androidactors.w.b(null);
    c.g.b.k.a(localw2, "wrap(null)");
    return localw2;
  }
  
  public final com.truecaller.androidactors.w<z> a(long paramLong)
  {
    try
    {
      Object localObject2 = d.getContentResolver().query(TruecallerContract.n.d(), null, "(call_log_id NOT NULL OR tc_flag=2 OR tc_flag=3) AND new=1 AND type=3 AND action NOT IN (5,1,3,4) AND timestamp<=?", new String[] { String.valueOf(paramLong) }, "timestamp DESC");
      if (localObject2 == null) {
        break label115;
      }
      try
      {
        aa localaa = e.a((Cursor)localObject2);
        b localb = (b)h.a;
        localObject1 = localb;
        if (localb != null) {
          localObject1 = new q(localb);
        }
        localObject1 = com.truecaller.androidactors.w.a(localaa, (ab)localObject1);
        c.g.b.k.a(localObject1, "wrap<HistoryEventCursor>…istoryEventCursor::close)");
        return (com.truecaller.androidactors.w<z>)localObject1;
      }
      catch (SQLiteException localSQLiteException2)
      {
        localObject1 = localObject2;
        localObject2 = localSQLiteException2;
      }
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localSQLiteException1);
    }
    catch (SQLiteException localSQLiteException1)
    {
      localObject1 = null;
    }
    com.truecaller.util.q.a((Cursor)localObject1);
    label115:
    Object localObject1 = com.truecaller.androidactors.w.b(null);
    c.g.b.k.a(localObject1, "wrap(null)");
    return (com.truecaller.androidactors.w<z>)localObject1;
  }
  
  /* Error */
  public final com.truecaller.androidactors.w<List<HistoryEvent>> a(FilterType paramFilterType, Integer paramInteger)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc -68
    //   3: invokestatic 44	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aconst_null
    //   7: astore 4
    //   9: aload_0
    //   10: getfield 59	com/truecaller/callhistory/m:d	Landroid/content/Context;
    //   13: invokevirtual 99	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   16: astore 5
    //   18: invokestatic 190	com/truecaller/content/TruecallerContract$n:e	()Landroid/net/Uri;
    //   21: astore 6
    //   23: aload_2
    //   24: ifnull +346 -> 370
    //   27: new 192	java/lang/StringBuilder
    //   30: dup
    //   31: invokespecial 193	java/lang/StringBuilder:<init>	()V
    //   34: astore 7
    //   36: aload 7
    //   38: ldc 110
    //   40: invokevirtual 197	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   43: pop
    //   44: aload 7
    //   46: ldc -57
    //   48: invokevirtual 197	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   51: pop
    //   52: aload 7
    //   54: aload_2
    //   55: invokevirtual 202	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   58: pop
    //   59: aload 7
    //   61: invokevirtual 206	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   64: astore_2
    //   65: goto +3 -> 68
    //   68: getstatic 211	com/truecaller/callhistory/n:b	[I
    //   71: aload_1
    //   72: invokevirtual 217	com/truecaller/callhistory/FilterType:ordinal	()I
    //   75: iaload
    //   76: tableswitch	default:+300->376, 1:+76->152, 2:+56->132, 3:+36->112
    //   104: getstatic 219	com/truecaller/callhistory/n:a	[I
    //   107: astore 7
    //   109: goto +63 -> 172
    //   112: aload 5
    //   114: aload 6
    //   116: aconst_null
    //   117: aload_0
    //   118: ldc -35
    //   120: invokespecial 108	com/truecaller/callhistory/m:d	(Ljava/lang/String;)Ljava/lang/String;
    //   123: aconst_null
    //   124: aload_2
    //   125: invokevirtual 116	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   128: astore_1
    //   129: goto +98 -> 227
    //   132: aload 5
    //   134: aload 6
    //   136: aconst_null
    //   137: aload_0
    //   138: ldc -33
    //   140: invokespecial 108	com/truecaller/callhistory/m:d	(Ljava/lang/String;)Ljava/lang/String;
    //   143: aconst_null
    //   144: aload_2
    //   145: invokevirtual 116	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   148: astore_1
    //   149: goto +78 -> 227
    //   152: aload 5
    //   154: aload 6
    //   156: aconst_null
    //   157: aload_0
    //   158: ldc 106
    //   160: invokespecial 108	com/truecaller/callhistory/m:d	(Ljava/lang/String;)Ljava/lang/String;
    //   163: aconst_null
    //   164: aload_2
    //   165: invokevirtual 116	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   168: astore_1
    //   169: goto +58 -> 227
    //   172: aload 7
    //   174: aload_1
    //   175: invokevirtual 217	com/truecaller/callhistory/FilterType:ordinal	()I
    //   178: iaload
    //   179: tableswitch	default:+200->379, 1:+215->394, 2:+210->389, 3:+205->384
    //   204: aload 5
    //   206: aload 6
    //   208: aconst_null
    //   209: ldc -31
    //   211: iconst_1
    //   212: anewarray 168	java/lang/String
    //   215: dup
    //   216: iconst_0
    //   217: iload_3
    //   218: invokestatic 228	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   221: aastore
    //   222: aload_2
    //   223: invokevirtual 116	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   226: astore_1
    //   227: aload_1
    //   228: ifnull +94 -> 322
    //   231: new 230	com/truecaller/callhistory/aa
    //   234: dup
    //   235: aload_1
    //   236: new 232	com/truecaller/data/access/e
    //   239: dup
    //   240: aload_1
    //   241: invokespecial 234	com/truecaller/data/access/e:<init>	(Landroid/database/Cursor;)V
    //   244: new 236	com/truecaller/data/access/d
    //   247: dup
    //   248: aload_1
    //   249: invokespecial 237	com/truecaller/data/access/d:<init>	(Landroid/database/Cursor;)V
    //   252: iconst_1
    //   253: invokespecial 240	com/truecaller/callhistory/aa:<init>	(Landroid/database/Cursor;Lcom/truecaller/data/access/e;Lcom/truecaller/data/access/d;Z)V
    //   256: astore_2
    //   257: new 242	java/util/ArrayList
    //   260: dup
    //   261: invokespecial 243	java/util/ArrayList:<init>	()V
    //   264: astore 4
    //   266: aload_2
    //   267: invokevirtual 247	com/truecaller/callhistory/aa:moveToNext	()Z
    //   270: ifeq +25 -> 295
    //   273: aload_2
    //   274: invokevirtual 250	com/truecaller/callhistory/aa:d	()Lcom/truecaller/data/entity/HistoryEvent;
    //   277: astore 5
    //   279: aload 5
    //   281: ifnull -15 -> 266
    //   284: aload 4
    //   286: aload 5
    //   288: invokevirtual 254	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   291: pop
    //   292: goto -26 -> 266
    //   295: aload 4
    //   297: invokestatic 151	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   300: astore_2
    //   301: aload_2
    //   302: ldc_w 256
    //   305: invokestatic 135	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   308: aload_1
    //   309: invokestatic 148	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   312: aload_2
    //   313: areturn
    //   314: astore_2
    //   315: goto +49 -> 364
    //   318: astore_2
    //   319: goto +20 -> 339
    //   322: aload_1
    //   323: invokestatic 148	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   326: goto +24 -> 350
    //   329: astore_2
    //   330: aload 4
    //   332: astore_1
    //   333: goto +31 -> 364
    //   336: astore_2
    //   337: aconst_null
    //   338: astore_1
    //   339: aload_2
    //   340: checkcast 137	java/lang/Throwable
    //   343: invokestatic 143	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   346: aload_1
    //   347: invokestatic 148	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   350: aconst_null
    //   351: invokestatic 151	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   354: astore_1
    //   355: aload_1
    //   356: ldc -103
    //   358: invokestatic 135	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   361: aload_1
    //   362: areturn
    //   363: astore_2
    //   364: aload_1
    //   365: invokestatic 148	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   368: aload_2
    //   369: athrow
    //   370: ldc 110
    //   372: astore_2
    //   373: goto -305 -> 68
    //   376: goto -272 -> 104
    //   379: iconst_0
    //   380: istore_3
    //   381: goto -177 -> 204
    //   384: iconst_1
    //   385: istore_3
    //   386: goto -182 -> 204
    //   389: iconst_2
    //   390: istore_3
    //   391: goto -187 -> 204
    //   394: iconst_3
    //   395: istore_3
    //   396: goto -192 -> 204
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	399	0	this	m
    //   0	399	1	paramFilterType	FilterType
    //   0	399	2	paramInteger	Integer
    //   217	179	3	i	int
    //   7	324	4	localArrayList	ArrayList
    //   16	271	5	localObject1	Object
    //   21	186	6	localUri	Uri
    //   34	139	7	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   231	266	314	finally
    //   266	279	314	finally
    //   284	292	314	finally
    //   295	308	314	finally
    //   231	266	318	android/database/sqlite/SQLiteException
    //   266	279	318	android/database/sqlite/SQLiteException
    //   284	292	318	android/database/sqlite/SQLiteException
    //   295	308	318	android/database/sqlite/SQLiteException
    //   9	23	329	finally
    //   27	65	329	finally
    //   68	104	329	finally
    //   104	109	329	finally
    //   112	129	329	finally
    //   132	149	329	finally
    //   152	169	329	finally
    //   172	204	329	finally
    //   204	227	329	finally
    //   9	23	336	android/database/sqlite/SQLiteException
    //   27	65	336	android/database/sqlite/SQLiteException
    //   68	104	336	android/database/sqlite/SQLiteException
    //   104	109	336	android/database/sqlite/SQLiteException
    //   112	129	336	android/database/sqlite/SQLiteException
    //   132	149	336	android/database/sqlite/SQLiteException
    //   152	169	336	android/database/sqlite/SQLiteException
    //   172	204	336	android/database/sqlite/SQLiteException
    //   204	227	336	android/database/sqlite/SQLiteException
    //   339	346	363	finally
  }
  
  public final com.truecaller.androidactors.w<z> a(Contact paramContact)
  {
    c.g.b.k.b(paramContact, "contact");
    Object localObject;
    try
    {
      localObject = d.getContentResolver();
      paramContact = String.valueOf(paramContact.getId());
      localObject = ((ContentResolver)localObject).query(TruecallerContract.n.d(), null, d("type IN (1,2,3)  AND (history_aggregated_contact_id=? OR history_aggregated_contact_tc_id=?)"), new String[] { paramContact, paramContact }, "timestamp DESC");
      if (localObject == null) {
        break label97;
      }
      try
      {
        paramContact = com.truecaller.androidactors.w.a(e.a((Cursor)localObject), (ab)c.a);
        c.g.b.k.a(paramContact, "wrap<HistoryEventCursor>…tCursor()) { it.close() }");
        return paramContact;
      }
      catch (SQLiteException paramContact) {}
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramContact);
    }
    catch (SQLiteException paramContact)
    {
      localObject = null;
    }
    com.truecaller.util.q.a((Cursor)localObject);
    label97:
    paramContact = com.truecaller.androidactors.w.b(null);
    c.g.b.k.a(paramContact, "wrap(null)");
    return paramContact;
  }
  
  public final com.truecaller.androidactors.w<z> a(String paramString)
  {
    c.g.b.k.b(paramString, "normalizedNumber");
    AssertionUtil.AlwaysFatal.isFalse(org.c.a.a.a.k.b((CharSequence)paramString), new String[0]);
    Cursor localCursor;
    try
    {
      localCursor = d.getContentResolver().query(TruecallerContract.n.d(), null, d("type IN (1,2,3)  AND normalized_number=?"), new String[] { paramString }, "timestamp DESC");
      if (localCursor == null) {
        break label97;
      }
      try
      {
        paramString = com.truecaller.androidactors.w.a(e.a(localCursor), (ab)b.a);
        c.g.b.k.a(paramString, "wrap<HistoryEventCursor>…tCursor()) { it.close() }");
        return paramString;
      }
      catch (SQLiteException paramString) {}
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramString);
    }
    catch (SQLiteException paramString)
    {
      localCursor = null;
    }
    com.truecaller.util.q.a(localCursor);
    label97:
    paramString = com.truecaller.androidactors.w.b(null);
    c.g.b.k.a(paramString, "wrap(null)");
    return paramString;
  }
  
  public final com.truecaller.androidactors.w<Integer> a(List<? extends HistoryEvent> paramList)
  {
    c.g.b.k.b(paramList, "eventsToRestore");
    if (paramList.isEmpty())
    {
      paramList = com.truecaller.androidactors.w.b(Integer.valueOf(0));
      c.g.b.k.a(paramList, "Promise.wrap(0)");
      return paramList;
    }
    ContentResolver localContentResolver = d.getContentResolver();
    ContentValues[] arrayOfContentValues = new ContentValues[paramList.size()];
    int j = ((Collection)paramList).size();
    int i = 0;
    while (i < j)
    {
      arrayOfContentValues[i] = e.a((HistoryEvent)paramList.get(i));
      i += 1;
    }
    i = localContentResolver.bulkInsert(TruecallerContract.n.a(), arrayOfContentValues);
    paramList = new StringBuilder();
    paramList.append(i);
    paramList.append(" HistoryTable rows inserted from backup");
    paramList.toString();
    paramList = com.truecaller.androidactors.w.b(Integer.valueOf(i));
    c.g.b.k.a(paramList, "Promise.wrap(insertedRows)");
    return paramList;
  }
  
  public final boolean a(HistoryEvent paramHistoryEvent)
  {
    c.g.b.k.b(paramHistoryEvent, "event");
    Cursor localCursor = null;
    Object localObject1 = localCursor;
    for (;;)
    {
      try
      {
        long l1 = paramHistoryEvent.j();
        localObject1 = localCursor;
        long l2 = paramHistoryEvent.j();
        localObject1 = localCursor;
        Object localObject2 = p.a(paramHistoryEvent.a());
        localObject1 = localCursor;
        localCursor = d.getContentResolver().query(TruecallerContract.n.a(), null, "normalized_number=? AND action=4 AND timestamp>=? AND timestamp<=? AND (call_log_id NOT NULL OR tc_flag=2 OR tc_flag=3) AND tc_flag=0", new String[] { localObject2, String.valueOf(l1 - 10000L), String.valueOf(l2 + 10000L) }, "timestamp");
        if (localCursor != null)
        {
          localObject1 = localCursor;
          Object localObject3 = new aa(localCursor);
          localObject1 = localCursor;
          if (((aa)localObject3).moveToNext())
          {
            localObject1 = localCursor;
            localObject2 = ((aa)localObject3).d();
            if (localObject2 == null) {
              continue;
            }
            localObject1 = localCursor;
            if (!ao.a(paramHistoryEvent.f(), ((HistoryEvent)localObject2).f(), paramHistoryEvent.a(), ((HistoryEvent)localObject2).a(), paramHistoryEvent.j(), ((HistoryEvent)localObject2).j())) {
              continue;
            }
            localObject1 = localCursor;
            if (paramHistoryEvent.h() != 5) {
              break label445;
            }
            bool = true;
            localObject1 = localCursor;
            localObject3 = new ContentValues();
            localObject1 = localCursor;
            ((ContentValues)localObject3).put("action", Integer.valueOf(paramHistoryEvent.h()));
            localObject1 = localCursor;
            ((ContentValues)localObject3).put("event_id", paramHistoryEvent.u());
            if (bool)
            {
              localObject1 = localCursor;
              ((ContentValues)localObject3).putNull("call_log_id");
            }
            localObject1 = localCursor;
            if (d.getContentResolver().update(TruecallerContract.n.a(), (ContentValues)localObject3, "_id=?", new String[] { String.valueOf(((HistoryEvent)localObject2).getId()) }) > 0)
            {
              localObject1 = localCursor;
              paramHistoryEvent.setId(((HistoryEvent)localObject2).getId());
              if (!bool)
              {
                localObject1 = localCursor;
                paramHistoryEvent.a(((HistoryEvent)localObject2).i());
              }
              localObject1 = localCursor;
              paramHistoryEvent.a(((HistoryEvent)localObject2).j());
              localObject1 = localCursor;
              paramHistoryEvent.b(((HistoryEvent)localObject2).k());
              com.truecaller.util.q.a(localCursor);
              return bool;
            }
            com.truecaller.util.q.a(localCursor);
            return false;
          }
        }
        com.truecaller.util.q.a(localCursor);
        localObject1 = d.getContentResolver().insert(TruecallerContract.n.a(), e.a(paramHistoryEvent));
        if (localObject1 == null) {
          return false;
        }
        l1 = ContentUris.parseId((Uri)localObject1);
        if (l1 == -1L) {
          return false;
        }
        paramHistoryEvent.setId(Long.valueOf(l1));
        return true;
      }
      finally
      {
        com.truecaller.util.q.a((Cursor)localObject1);
      }
      label445:
      boolean bool = false;
    }
  }
  
  public final boolean a(Set<Long> paramSet)
  {
    c.g.b.k.b(paramSet, "callLogIds");
    try
    {
      Object localObject1 = new StringBuilder("IN (");
      ((StringBuilder)localObject1).append(org.c.a.a.a.k.a("?", ",", paramSet.size()));
      ((StringBuilder)localObject1).append(')');
      localObject1 = ((StringBuilder)localObject1).toString();
      Object localObject2 = (Iterable)paramSet;
      paramSet = (Collection)new ArrayList(c.a.m.a((Iterable)localObject2, 10));
      localObject2 = ((Iterable)localObject2).iterator();
      while (((Iterator)localObject2).hasNext()) {
        paramSet.add(String.valueOf(((Number)((Iterator)localObject2).next()).longValue()));
      }
      paramSet = ((Collection)paramSet).toArray(new String[0]);
      if (paramSet != null)
      {
        paramSet = (String[])paramSet;
        localObject2 = d.getContentResolver();
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("new", Integer.valueOf(0));
        localContentValues.put("is_read", Integer.valueOf(1));
        if (((ContentResolver)localObject2).update(e.a(), localContentValues, "_id ".concat(String.valueOf(localObject1)), paramSet) != 0)
        {
          localContentValues.clear();
          localContentValues.put("new", Integer.valueOf(0));
          localContentValues.put("is_read", Integer.valueOf(1));
          ((ContentResolver)localObject2).update(TruecallerContract.n.a(), localContentValues, "call_log_id ".concat(String.valueOf(localObject1)), paramSet);
          return true;
        }
      }
      else
      {
        throw new u("null cannot be cast to non-null type kotlin.Array<T>");
      }
    }
    catch (IllegalArgumentException paramSet)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramSet);
      return false;
    }
    catch (SecurityException paramSet)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramSet);
      return false;
    }
    catch (RuntimeExecutionException paramSet)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramSet);
      return false;
    }
    return true;
  }
  
  /* Error */
  public final com.truecaller.androidactors.w<Integer> b()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 59	com/truecaller/callhistory/m:d	Landroid/content/Context;
    //   4: invokevirtual 99	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   7: invokestatic 104	com/truecaller/content/TruecallerContract$n:d	()Landroid/net/Uri;
    //   10: iconst_1
    //   11: anewarray 168	java/lang/String
    //   14: dup
    //   15: iconst_0
    //   16: ldc_w 519
    //   19: aastore
    //   20: ldc_w 521
    //   23: aconst_null
    //   24: aconst_null
    //   25: invokevirtual 116	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   28: astore_1
    //   29: aload_1
    //   30: astore_3
    //   31: aload_1
    //   32: ifnull +71 -> 103
    //   35: aload_1
    //   36: astore_3
    //   37: aload_1
    //   38: astore_2
    //   39: aload_1
    //   40: invokeinterface 526 1 0
    //   45: ifeq +58 -> 103
    //   48: aload_1
    //   49: astore_2
    //   50: aload_1
    //   51: iconst_0
    //   52: invokeinterface 530 2 0
    //   57: invokestatic 310	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   60: invokestatic 151	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   63: astore_3
    //   64: aload_1
    //   65: astore_2
    //   66: aload_3
    //   67: ldc_w 532
    //   70: invokestatic 135	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   73: aload_1
    //   74: invokestatic 148	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   77: aload_3
    //   78: areturn
    //   79: astore_3
    //   80: goto +12 -> 92
    //   83: astore_1
    //   84: aconst_null
    //   85: astore_2
    //   86: goto +35 -> 121
    //   89: astore_3
    //   90: aconst_null
    //   91: astore_1
    //   92: aload_1
    //   93: astore_2
    //   94: aload_3
    //   95: checkcast 137	java/lang/Throwable
    //   98: invokestatic 143	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   101: aload_1
    //   102: astore_3
    //   103: aload_3
    //   104: invokestatic 148	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   107: aconst_null
    //   108: invokestatic 151	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   111: astore_1
    //   112: aload_1
    //   113: ldc -103
    //   115: invokestatic 135	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   118: aload_1
    //   119: areturn
    //   120: astore_1
    //   121: aload_2
    //   122: invokestatic 148	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   125: aload_1
    //   126: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	127	0	this	m
    //   28	46	1	localCursor	Cursor
    //   83	1	1	localObject1	Object
    //   91	28	1	localw1	com.truecaller.androidactors.w
    //   120	6	1	localObject2	Object
    //   38	84	2	localObject3	Object
    //   30	48	3	localObject4	Object
    //   79	1	3	localSQLiteException1	SQLiteException
    //   89	6	3	localSQLiteException2	SQLiteException
    //   102	2	3	localw2	com.truecaller.androidactors.w
    // Exception table:
    //   from	to	target	type
    //   39	48	79	android/database/sqlite/SQLiteException
    //   50	64	79	android/database/sqlite/SQLiteException
    //   66	73	79	android/database/sqlite/SQLiteException
    //   0	29	83	finally
    //   0	29	89	android/database/sqlite/SQLiteException
    //   39	48	120	finally
    //   50	64	120	finally
    //   66	73	120	finally
    //   94	101	120	finally
  }
  
  public final com.truecaller.androidactors.w<HistoryEvent> b(String paramString)
  {
    c.g.b.k.b(paramString, "normalizedNumber");
    Object localObject1 = d.getContentResolver();
    Object localObject4 = null;
    try
    {
      paramString = ((ContentResolver)localObject1).query(TruecallerContract.n.d(), null, "(call_log_id NOT NULL OR tc_flag=2 OR tc_flag=3) AND type IN (1,2,3) AND action!=1 AND normalized_number=?", new String[] { paramString }, "timestamp DESC LIMIT 1");
      if (paramString != null) {
        try
        {
          localObject1 = e.a(paramString);
          if (((aa)localObject1).moveToFirst())
          {
            localObject1 = com.truecaller.androidactors.w.b(((aa)localObject1).d());
            c.g.b.k.a(localObject1, "Promise.wrap(eventCursor.historyEvent)");
            com.truecaller.util.q.a(paramString);
            return (com.truecaller.androidactors.w<HistoryEvent>)localObject1;
          }
        }
        finally
        {
          break label102;
        }
      }
      com.truecaller.util.q.a(paramString);
      paramString = com.truecaller.androidactors.w.b(null);
      c.g.b.k.a(paramString, "Promise.wrap(null)");
      return paramString;
    }
    finally
    {
      paramString = (String)localObject4;
      label102:
      com.truecaller.util.q.a(paramString);
    }
  }
  
  public final void b(long paramLong)
  {
    try
    {
      ContentResolver localContentResolver = d.getContentResolver();
      ContentValues localContentValues = new ContentValues();
      localContentValues.put("new", Integer.valueOf(0));
      localContentResolver.update(TruecallerContract.n.a(), localContentValues, "timestamp<=".concat(String.valueOf(paramLong)), null);
      localContentValues.clear();
      localContentValues.put("new", Integer.valueOf(0));
      localContentResolver.update(e.a(), localContentValues, "date<=".concat(String.valueOf(paramLong)), null);
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localIllegalArgumentException);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localSecurityException);
      return;
    }
    catch (RuntimeExecutionException localRuntimeExecutionException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localRuntimeExecutionException);
    }
  }
  
  public final com.truecaller.androidactors.w<HistoryEvent> c(String paramString)
  {
    c.g.b.k.b(paramString, "tcId");
    Object localObject1 = d.getContentResolver();
    Object localObject4 = null;
    try
    {
      paramString = ((ContentResolver)localObject1).query(TruecallerContract.n.d(), null, "(call_log_id NOT NULL OR tc_flag=2 OR tc_flag=3) AND type IN (1,2,3)  AND action!=1 AND tc_id=?", new String[] { paramString }, "timestamp DESC LIMIT 1");
      if (paramString != null) {
        try
        {
          localObject1 = e.a(paramString);
          if (((aa)localObject1).moveToFirst())
          {
            localObject1 = com.truecaller.androidactors.w.b(((aa)localObject1).d());
            c.g.b.k.a(localObject1, "Promise.wrap(eventCursor.historyEvent)");
            com.truecaller.util.q.a(paramString);
            return (com.truecaller.androidactors.w<HistoryEvent>)localObject1;
          }
        }
        finally
        {
          break label102;
        }
      }
      com.truecaller.util.q.a(paramString);
      paramString = com.truecaller.androidactors.w.b(null);
      c.g.b.k.a(paramString, "Promise.wrap(null)");
      return paramString;
    }
    finally
    {
      paramString = (String)localObject4;
      label102:
      com.truecaller.util.q.a(paramString);
    }
  }
  
  public final void c()
  {
    try
    {
      ContentResolver localContentResolver = d.getContentResolver();
      ContentValues localContentValues = new ContentValues();
      localContentValues.put("new", Integer.valueOf(0));
      localContentValues.put("is_read", Integer.valueOf(1));
      localContentResolver.update(TruecallerContract.n.a(), localContentValues, "new=1 OR is_read=0", null);
      localContentValues.clear();
      localContentValues.put("new", Integer.valueOf(0));
      localContentValues.put("is_read", Integer.valueOf(1));
      localContentResolver.update(e.a(), localContentValues, null, null);
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localIllegalArgumentException);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localSecurityException);
      return;
    }
    catch (RuntimeExecutionException localRuntimeExecutionException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localRuntimeExecutionException);
    }
  }
  
  public final void c(long paramLong)
  {
    try
    {
      ContentResolver localContentResolver = d.getContentResolver();
      ContentValues localContentValues = new ContentValues();
      localContentValues.put("new", Integer.valueOf(0));
      localContentValues.put("is_read", Integer.valueOf(1));
      if (localContentResolver.update(e.a(), localContentValues, "_id=?", new String[] { String.valueOf(paramLong) }) != 0)
      {
        localContentValues.clear();
        localContentValues.put("new", Integer.valueOf(0));
        localContentValues.put("is_read", Integer.valueOf(1));
        localContentResolver.update(TruecallerContract.n.a(), localContentValues, null, null);
        return;
      }
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localIllegalArgumentException);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localSecurityException);
      return;
    }
    catch (RuntimeExecutionException localRuntimeExecutionException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localRuntimeExecutionException);
    }
  }
  
  public final void d()
  {
    ContentResolver localContentResolver = d.getContentResolver();
    try
    {
      localContentResolver.delete(TruecallerContract.n.a(), "type IN (1,2,3)  AND tc_flag!=3", null);
      f.b("initialCallLogSyncComplete", false);
      WidgetListProvider.a(d);
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localSQLiteException);
    }
  }
  
  static final class a<T>
    implements ab<R>
  {
    public static final a a = new a();
  }
  
  static final class b<T>
    implements ab<R>
  {
    public static final b a = new b();
  }
  
  static final class c<T>
    implements ab<R>
  {
    public static final c a = new c();
  }
  
  static final class d
    extends c.g.b.l
    implements c.g.a.a<Boolean>
  {
    d(m paramm)
    {
      super();
    }
  }
  
  static final class e
    extends c.g.b.l
    implements c.g.a.a<Boolean>
  {
    e(m paramm)
    {
      super();
    }
  }
  
  static final class f
    extends c.g.b.l
    implements c.g.a.a<Boolean>
  {
    f(m paramm)
    {
      super();
    }
  }
  
  static final class g<T>
    implements ab<R>
  {
    public static final g a = new g();
  }
  
  static final class h
    extends j
    implements b<Cursor, x>
  {
    public static final h a = new h();
    
    public final c.l.c a()
    {
      return c.g.b.w.a(z.class);
    }
    
    public final String b()
    {
      return "close";
    }
    
    public final String c()
    {
      return "close()V";
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */