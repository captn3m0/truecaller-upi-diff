package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class b$y
  extends u<a, Void>
{
  private final long b;
  
  private b$y(e parame, long paramLong)
  {
    super(parame);
    b = paramLong;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".markMissedCallsAsShown(");
    localStringBuilder.append(a(Long.valueOf(b), 2));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.y
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */