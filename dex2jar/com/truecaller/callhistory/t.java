package com.truecaller.callhistory;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.androidactors.ab;
import com.truecaller.androidactors.w;
import com.truecaller.content.TruecallerContract.c;
import com.truecaller.data.access.d;
import com.truecaller.data.access.e;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.util.aq;
import javax.inject.Inject;

public final class t
  implements r
{
  private final ContentResolver a;
  private final aq b;
  
  @Inject
  public t(ContentResolver paramContentResolver, aq paramaq)
  {
    a = paramContentResolver;
    b = paramaq;
  }
  
  public final w<z> a()
  {
    Object localObject = a.query(TruecallerContract.c.b(), null, null, null, "timestamp DESC");
    if (localObject == null)
    {
      localObject = w.b(null);
      k.a(localObject, "Promise.wrap(null)");
      return (w<z>)localObject;
    }
    localObject = w.a(new aa((Cursor)localObject, new e((Cursor)localObject), new d((Cursor)localObject)), (ab)new a((Cursor)localObject));
    k.a(localObject, "Promise.wrap(\n          …eaner { cursor.close() })");
    return (w<z>)localObject;
  }
  
  public final w<Boolean> a(CallRecording paramCallRecording)
  {
    k.b(paramCallRecording, "callRecording");
    try
    {
      b.c(c);
      ContentResolver localContentResolver = a;
      Uri localUri = TruecallerContract.c.a();
      bool = true;
      if (localContentResolver.delete(localUri, "history_event_id=?", new String[] { b }) <= 0) {
        break label92;
      }
    }
    catch (Exception paramCallRecording)
    {
      for (;;)
      {
        continue;
        boolean bool = false;
      }
    }
    paramCallRecording = w.b(Boolean.valueOf(bool));
    k.a(paramCallRecording, "Promise.wrap(count > 0)");
    return paramCallRecording;
    paramCallRecording = w.b(Boolean.FALSE);
    k.a(paramCallRecording, "Promise.wrap(false)");
    return paramCallRecording;
  }
  
  /* Error */
  public final w<Boolean> a(java.util.Collection<Long> paramCollection)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 127
    //   3: invokestatic 22	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_1
    //   7: checkcast 129	java/lang/Iterable
    //   10: ldc -125
    //   12: checkcast 133	java/lang/CharSequence
    //   15: aconst_null
    //   16: aconst_null
    //   17: iconst_0
    //   18: aconst_null
    //   19: aconst_null
    //   20: bipush 62
    //   22: invokestatic 138	c/a/m:a	(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lc/g/a/b;I)Ljava/lang/String;
    //   25: astore 4
    //   27: aload_0
    //   28: getfield 29	com/truecaller/callhistory/t:a	Landroid/content/ContentResolver;
    //   31: astore_1
    //   32: invokestatic 97	com/truecaller/content/TruecallerContract$c:a	()Landroid/net/Uri;
    //   35: astore_3
    //   36: new 140	java/lang/StringBuilder
    //   39: dup
    //   40: ldc -114
    //   42: invokespecial 145	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   45: astore 5
    //   47: aload 5
    //   49: aload 4
    //   51: invokevirtual 149	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   54: pop
    //   55: aload 5
    //   57: bipush 41
    //   59: invokevirtual 152	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   62: pop
    //   63: aload 5
    //   65: invokevirtual 156	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   68: astore 5
    //   70: aload_1
    //   71: aload_3
    //   72: iconst_1
    //   73: anewarray 101	java/lang/String
    //   76: dup
    //   77: iconst_0
    //   78: ldc -98
    //   80: aastore
    //   81: aload 5
    //   83: aconst_null
    //   84: aconst_null
    //   85: invokevirtual 47	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   88: astore 6
    //   90: aconst_null
    //   91: astore_3
    //   92: aload 6
    //   94: ifnull +134 -> 228
    //   97: aload 6
    //   99: checkcast 160	java/io/Closeable
    //   102: astore 5
    //   104: aload_3
    //   105: astore_1
    //   106: new 162	java/util/ArrayList
    //   109: dup
    //   110: invokespecial 163	java/util/ArrayList:<init>	()V
    //   113: checkcast 165	java/util/Collection
    //   116: astore 7
    //   118: aload_3
    //   119: astore_1
    //   120: aload 6
    //   122: invokeinterface 171 1 0
    //   127: ifeq +23 -> 150
    //   130: aload_3
    //   131: astore_1
    //   132: aload 7
    //   134: aload 6
    //   136: ldc -98
    //   138: invokestatic 176	com/truecaller/utils/extensions/k:a	(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    //   141: invokeinterface 180 2 0
    //   146: pop
    //   147: goto -29 -> 118
    //   150: aload_3
    //   151: astore_1
    //   152: aload 7
    //   154: checkcast 182	java/util/List
    //   157: astore_3
    //   158: aload 5
    //   160: aconst_null
    //   161: invokestatic 187	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   164: aload_3
    //   165: checkcast 129	java/lang/Iterable
    //   168: invokeinterface 191 1 0
    //   173: astore_1
    //   174: aload_1
    //   175: invokeinterface 196 1 0
    //   180: ifeq +48 -> 228
    //   183: aload_1
    //   184: invokeinterface 200 1 0
    //   189: checkcast 101	java/lang/String
    //   192: astore_3
    //   193: aload_3
    //   194: ifnull -20 -> 174
    //   197: aload_0
    //   198: getfield 31	com/truecaller/callhistory/t:b	Lcom/truecaller/util/aq;
    //   201: aload_3
    //   202: invokeinterface 95 2 0
    //   207: pop
    //   208: goto -34 -> 174
    //   211: astore_3
    //   212: goto +8 -> 220
    //   215: astore_3
    //   216: aload_3
    //   217: astore_1
    //   218: aload_3
    //   219: athrow
    //   220: aload 5
    //   222: aload_1
    //   223: invokestatic 187	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   226: aload_3
    //   227: athrow
    //   228: aload_0
    //   229: getfield 29	com/truecaller/callhistory/t:a	Landroid/content/ContentResolver;
    //   232: astore_1
    //   233: invokestatic 97	com/truecaller/content/TruecallerContract$c:a	()Landroid/net/Uri;
    //   236: astore_3
    //   237: new 140	java/lang/StringBuilder
    //   240: dup
    //   241: ldc -114
    //   243: invokespecial 145	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   246: astore 5
    //   248: aload 5
    //   250: aload 4
    //   252: invokevirtual 149	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   255: pop
    //   256: aload 5
    //   258: bipush 41
    //   260: invokevirtual 152	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   263: pop
    //   264: aload_1
    //   265: aload_3
    //   266: aload 5
    //   268: invokevirtual 156	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   271: aconst_null
    //   272: invokevirtual 107	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   275: ifle +43 -> 318
    //   278: iconst_1
    //   279: istore_2
    //   280: goto +3 -> 283
    //   283: iload_2
    //   284: invokestatic 113	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   287: invokestatic 52	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   290: astore_1
    //   291: aload_1
    //   292: ldc 115
    //   294: invokestatic 56	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   297: aload_1
    //   298: areturn
    //   299: getstatic 119	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   302: invokestatic 52	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   305: astore_1
    //   306: aload_1
    //   307: ldc 121
    //   309: invokestatic 56	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   312: aload_1
    //   313: areturn
    //   314: astore_1
    //   315: goto -16 -> 299
    //   318: iconst_0
    //   319: istore_2
    //   320: goto -37 -> 283
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	323	0	this	t
    //   0	323	1	paramCollection	java.util.Collection<Long>
    //   279	41	2	bool	boolean
    //   35	167	3	localObject1	Object
    //   211	1	3	localObject2	Object
    //   215	12	3	localThrowable	Throwable
    //   236	30	3	localUri	Uri
    //   25	226	4	str	String
    //   45	222	5	localObject3	Object
    //   88	47	6	localCursor	Cursor
    //   116	37	7	localCollection	java.util.Collection
    // Exception table:
    //   from	to	target	type
    //   106	118	211	finally
    //   120	130	211	finally
    //   132	147	211	finally
    //   152	158	211	finally
    //   218	220	211	finally
    //   106	118	215	java/lang/Throwable
    //   120	130	215	java/lang/Throwable
    //   132	147	215	java/lang/Throwable
    //   152	158	215	java/lang/Throwable
    //   6	90	314	java/lang/Exception
    //   97	104	314	java/lang/Exception
    //   158	174	314	java/lang/Exception
    //   174	193	314	java/lang/Exception
    //   197	208	314	java/lang/Exception
    //   220	228	314	java/lang/Exception
    //   228	278	314	java/lang/Exception
    //   283	297	314	java/lang/Exception
  }
  
  static final class a<T>
    implements ab<z>
  {
    a(Cursor paramCursor) {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.t
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */