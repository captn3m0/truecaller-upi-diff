package com.truecaller.callhistory;

import android.content.Context;
import com.truecaller.calling.recorder.CallRecordingManager;
import com.truecaller.data.entity.g;
import com.truecaller.i.c;
import com.truecaller.multisim.h;
import com.truecaller.utils.l;
import javax.inject.Provider;

public final class an
  implements dagger.a.d<am>
{
  private final Provider<l> a;
  private final Provider<h> b;
  private final Provider<v> c;
  private final Provider<k> d;
  private final Provider<g> e;
  private final Provider<ai> f;
  private final Provider<Context> g;
  private final Provider<c> h;
  private final Provider<com.truecaller.utils.d> i;
  private final Provider<CallRecordingManager> j;
  
  private an(Provider<l> paramProvider, Provider<h> paramProvider1, Provider<v> paramProvider2, Provider<k> paramProvider3, Provider<g> paramProvider4, Provider<ai> paramProvider5, Provider<Context> paramProvider6, Provider<c> paramProvider7, Provider<com.truecaller.utils.d> paramProvider8, Provider<CallRecordingManager> paramProvider9)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
  }
  
  public static an a(Provider<l> paramProvider, Provider<h> paramProvider1, Provider<v> paramProvider2, Provider<k> paramProvider3, Provider<g> paramProvider4, Provider<ai> paramProvider5, Provider<Context> paramProvider6, Provider<c> paramProvider7, Provider<com.truecaller.utils.d> paramProvider8, Provider<CallRecordingManager> paramProvider9)
  {
    return new an(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.an
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */