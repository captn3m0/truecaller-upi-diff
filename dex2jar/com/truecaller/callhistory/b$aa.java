package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class b$aa
  extends u<a, Void>
{
  private final ai.a b;
  
  private b$aa(e parame, ai.a parama)
  {
    super(parame);
    b = parama;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".performNextSyncBatch(");
    localStringBuilder.append(a(b, 2));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.aa
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */