package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class b$x
  extends u<a, Void>
{
  private final String b;
  
  private b$x(e parame, String paramString)
  {
    super(parame);
    b = paramString;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".markMissedCallsAsSeen(");
    localStringBuilder.append(a(b, 1));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.x
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */