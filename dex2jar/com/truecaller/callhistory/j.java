package com.truecaller.callhistory;

import android.content.Context;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.k;
import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d<i>
{
  private final g a;
  private final Provider<Context> b;
  private final Provider<k> c;
  
  private j(g paramg, Provider<Context> paramProvider, Provider<k> paramProvider1)
  {
    a = paramg;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static j a(g paramg, Provider<Context> paramProvider, Provider<k> paramProvider1)
  {
    return new j(paramg, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */