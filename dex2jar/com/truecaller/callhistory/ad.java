package com.truecaller.callhistory;

import android.annotation.SuppressLint;
import android.database.CursorWrapper;
import com.truecaller.common.h.ab;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.HistoryEvent.a;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.g;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.multisim.d;
import java.util.UUID;
import org.c.a.a.a.a;
import org.c.a.a.a.k;

final class ad
  extends CursorWrapper
  implements ac
{
  private int[] a = { 200, 300, 400, 500 };
  private final g b;
  private final d c;
  private final int d;
  private final int e;
  private final int f;
  private final int g;
  private final int h;
  private final int i;
  private final int j;
  private final int k;
  private final int l;
  private final int m;
  private final int n;
  private final int o;
  
  @SuppressLint({"InlinedApi"})
  ad(g paramg, d paramd)
  {
    super(paramd);
    b = paramg;
    c = paramd;
    d = paramd.getColumnIndexOrThrow("_id");
    e = paramd.getColumnIndexOrThrow("date");
    f = paramd.getColumnIndexOrThrow("number");
    g = paramd.getColumnIndex("normalized_number");
    h = paramd.getColumnIndex("type");
    j = paramd.getColumnIndexOrThrow("duration");
    k = paramd.getColumnIndexOrThrow("name");
    l = paramd.getColumnIndex("features");
    m = paramd.getColumnIndex("new");
    n = paramd.getColumnIndex("is_read");
    o = paramd.getColumnIndex("subscription_component_name");
    i = paramd.getColumnIndex("logtype");
  }
  
  public final boolean a()
  {
    int i1 = i;
    if (i1 != -1)
    {
      i1 = getInt(i1);
      if (a.a(a, i1)) {
        return true;
      }
    }
    return isNull(f);
  }
  
  public final long b()
  {
    return getLong(d);
  }
  
  public final long c()
  {
    return getLong(e);
  }
  
  public final HistoryEvent d()
  {
    boolean bool = a();
    Object localObject1 = null;
    if (bool) {
      return null;
    }
    HistoryEvent.a locala = new HistoryEvent.a();
    String str = getString(f);
    bool = ab.a(str);
    int i1 = 1;
    if (bool)
    {
      locala.b("");
      locala.a("");
    }
    else
    {
      int i2 = g;
      if (i2 != -1) {
        localObject1 = getString(i2);
      }
      Object localObject2 = localObject1;
      if (k.b((CharSequence)localObject1)) {
        localObject2 = k.n(str);
      }
      AssertionUtil.AlwaysFatal.isNotNull(localObject2, new String[0]);
      localObject1 = b.b(new String[] { localObject2, str });
      locala.b(k.n(((Number)localObject1).d()));
      locala.a(k.n(((Number)localObject1).a()));
      locala.a(((Number)localObject1).m());
      locala.c(((Number)localObject1).l());
    }
    switch (getInt(h))
    {
    case 4: 
    case 7: 
    case 8: 
    case 9: 
    default: 
      i1 = 0;
      break;
    case 3: 
    case 5: 
    case 6: 
    case 10: 
      i1 = 3;
      break;
    case 2: 
      i1 = 2;
    }
    locala.a(i1);
    locala.b(4);
    locala.a(getLong(e));
    locala.a(Long.valueOf(getLong(d)));
    locala.b(getLong(j));
    locala.d(getString(k));
    locala.e(c.e());
    locala.g(UUID.randomUUID().toString());
    i1 = l;
    if (i1 >= 0) {
      locala.c(getInt(i1));
    }
    i1 = m;
    if (i1 >= 0) {
      locala.d(getInt(i1));
    }
    i1 = n;
    if (i1 >= 0) {
      locala.e(getInt(i1));
    }
    i1 = o;
    if (i1 >= 0) {
      locala.f(getString(i1));
    }
    return a;
  }
  
  public final String e()
  {
    return c.e();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.ad
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */