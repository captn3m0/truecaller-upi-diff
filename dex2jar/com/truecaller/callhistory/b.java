package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import com.truecaller.calling.dialer.HistoryEventsScope;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public final class b
  implements a
{
  private final v a;
  
  public b(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return a.class.equals(paramClass);
  }
  
  public final w<Boolean> a(int paramInt, Collection<Long> paramCollection)
  {
    return w.a(a, new g(new e(), paramInt, paramCollection, (byte)0));
  }
  
  public final w<z> a(long paramLong)
  {
    return w.a(a, new r(new e(), paramLong, (byte)0));
  }
  
  public final w<List<HistoryEvent>> a(FilterType paramFilterType, Integer paramInteger)
  {
    return w.a(a, new k(new e(), paramFilterType, paramInteger, (byte)0));
  }
  
  public final w<z> a(Contact paramContact)
  {
    return w.a(a, new j(new e(), paramContact, (byte)0));
  }
  
  public final w<Boolean> a(HistoryEvent paramHistoryEvent, Contact paramContact)
  {
    return w.a(a, new d(new e(), paramHistoryEvent, paramContact, (byte)0));
  }
  
  public final w<z> a(String paramString)
  {
    return w.a(a, new i(new e(), paramString, (byte)0));
  }
  
  public final w<Integer> a(List<HistoryEvent> paramList)
  {
    return w.a(a, new b(new e(), paramList, (byte)0));
  }
  
  public final w<Boolean> a(List<Long> paramList1, List<Long> paramList2, HistoryEventsScope paramHistoryEventsScope)
  {
    return w.a(a, new f(new e(), paramList1, paramList2, paramHistoryEventsScope, (byte)0));
  }
  
  public final w<Boolean> a(Set<Long> paramSet)
  {
    return w.a(a, new w(new e(), paramSet, (byte)0));
  }
  
  public final void a()
  {
    a.a(new z(new e(), (byte)0));
  }
  
  public final void a(int paramInt)
  {
    a.a(new e(new e(), paramInt, (byte)0));
  }
  
  public final void a(ai.a parama)
  {
    a.a(new aa(new e(), parama, (byte)0));
  }
  
  public final void a(CallRecording paramCallRecording)
  {
    a.a(new a(new e(), paramCallRecording, (byte)0));
  }
  
  public final void a(HistoryEvent paramHistoryEvent)
  {
    a.a(new c(new e(), paramHistoryEvent, (byte)0));
  }
  
  public final w<z> b()
  {
    return w.a(a, new h(new e(), (byte)0));
  }
  
  public final w<z> b(int paramInt)
  {
    return w.a(a, new s(new e(), paramInt, (byte)0));
  }
  
  public final w<Boolean> b(Set<Long> paramSet)
  {
    return w.a(a, new u(new e(), paramSet, (byte)0));
  }
  
  public final void b(long paramLong)
  {
    a.a(new y(new e(), paramLong, (byte)0));
  }
  
  public final void b(String paramString)
  {
    a.a(new x(new e(), paramString, (byte)0));
  }
  
  public final w<z> c()
  {
    return w.a(a, new l(new e(), (byte)0));
  }
  
  public final w<z> c(int paramInt)
  {
    return w.a(a, new p(new e(), paramInt, (byte)0));
  }
  
  public final w<HistoryEvent> c(String paramString)
  {
    return w.a(a, new m(new e(), paramString, (byte)0));
  }
  
  public final void c(long paramLong)
  {
    a.a(new v(new e(), paramLong, (byte)0));
  }
  
  public final w<z> d()
  {
    return w.a(a, new q(new e(), (byte)0));
  }
  
  public final w<HistoryEvent> d(String paramString)
  {
    return w.a(a, new n(new e(), paramString, (byte)0));
  }
  
  public final w<Integer> e()
  {
    return w.a(a, new o(new e(), (byte)0));
  }
  
  public final void f()
  {
    a.a(new t(new e(), (byte)0));
  }
  
  public final void g()
  {
    a.a(new ab(new e(), (byte)0));
  }
  
  static final class a
    extends u<a, Void>
  {
    private final CallRecording b;
    
    private a(e parame, CallRecording paramCallRecording)
    {
      super();
      b = paramCallRecording;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".addCallRecording(");
      localStringBuilder.append(a(b, 1));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class aa
    extends u<a, Void>
  {
    private final ai.a b;
    
    private aa(e parame, ai.a parama)
    {
      super();
      b = parama;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".performNextSyncBatch(");
      localStringBuilder.append(a(b, 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class ab
    extends u<a, Void>
  {
    private ab(e parame)
    {
      super();
    }
    
    public final String toString()
    {
      return ".resetCallLogHistory()";
    }
  }
  
  static final class b
    extends u<a, Integer>
  {
    private final List<HistoryEvent> b;
    
    private b(e parame, List<HistoryEvent> paramList)
    {
      super();
      b = paramList;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".addFromBackup(");
      localStringBuilder.append(a(b, 1));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class c
    extends u<a, Void>
  {
    private final HistoryEvent b;
    
    private c(e parame, HistoryEvent paramHistoryEvent)
    {
      super();
      b = paramHistoryEvent;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".add(");
      localStringBuilder.append(a(b, 1));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class d
    extends u<a, Boolean>
  {
    private final HistoryEvent b;
    private final Contact c;
    
    private d(e parame, HistoryEvent paramHistoryEvent, Contact paramContact)
    {
      super();
      b = paramHistoryEvent;
      c = paramContact;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".addWithContact(");
      localStringBuilder.append(a(b, 1));
      localStringBuilder.append(",");
      localStringBuilder.append(a(c, 1));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class e
    extends u<a, Void>
  {
    private final int b;
    
    private e(e parame, int paramInt)
    {
      super();
      b = paramInt;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".clearSearchHistory(");
      localStringBuilder.append(a(Integer.valueOf(b), 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class f
    extends u<a, Boolean>
  {
    private final List<Long> b;
    private final List<Long> c;
    private final HistoryEventsScope d;
    
    private f(e parame, List<Long> paramList1, List<Long> paramList2, HistoryEventsScope paramHistoryEventsScope)
    {
      super();
      b = paramList1;
      c = paramList2;
      d = paramHistoryEventsScope;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".deleteHistory(");
      localStringBuilder.append(a(b, 2));
      localStringBuilder.append(",");
      localStringBuilder.append(a(c, 2));
      localStringBuilder.append(",");
      localStringBuilder.append(a(d, 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class g
    extends u<a, Boolean>
  {
    private final int b;
    private final Collection<Long> c;
    
    private g(e parame, int paramInt, Collection<Long> paramCollection)
    {
      super();
      b = paramInt;
      c = paramCollection;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".deleteSearchHistoryForIds(");
      localStringBuilder.append(a(Integer.valueOf(b), 2));
      localStringBuilder.append(",");
      localStringBuilder.append(a(c, 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class h
    extends u<a, z>
  {
    private h(e parame)
    {
      super();
    }
    
    public final String toString()
    {
      return ".getAllForWidget()";
    }
  }
  
  static final class i
    extends u<a, z>
  {
    private final String b;
    
    private i(e parame, String paramString)
    {
      super();
      b = paramString;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".getCallHistoryByNumber(");
      localStringBuilder.append(a(b, 1));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class j
    extends u<a, z>
  {
    private final Contact b;
    
    private j(e parame, Contact paramContact)
    {
      super();
      b = paramContact;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".getCallHistoryForContact(");
      localStringBuilder.append(a(b, 1));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class k
    extends u<a, List<HistoryEvent>>
  {
    private final FilterType b;
    private final Integer c;
    
    private k(e parame, FilterType paramFilterType, Integer paramInteger)
    {
      super();
      b = paramFilterType;
      c = paramInteger;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".getCallHistoryList(");
      localStringBuilder.append(a(b, 2));
      localStringBuilder.append(",");
      localStringBuilder.append(a(c, 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class l
    extends u<a, z>
  {
    private l(e parame)
    {
      super();
    }
    
    public final String toString()
    {
      return ".getCallHistory()";
    }
  }
  
  static final class m
    extends u<a, HistoryEvent>
  {
    private final String b;
    
    private m(e parame, String paramString)
    {
      super();
      b = paramString;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".getLastMappedCallByNumber(");
      localStringBuilder.append(a(b, 1));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class n
    extends u<a, HistoryEvent>
  {
    private final String b;
    
    private n(e parame, String paramString)
    {
      super();
      b = paramString;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".getLastMappedCallByTcId(");
      localStringBuilder.append(a(b, 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class o
    extends u<a, Integer>
  {
    private o(e parame)
    {
      super();
    }
    
    public final String toString()
    {
      return ".getMissedCallsCount()";
    }
  }
  
  static final class p
    extends u<a, z>
  {
    private final int b;
    
    private p(e parame, int paramInt)
    {
      super();
      b = paramInt;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".getMostCalledEvents(");
      localStringBuilder.append(a(Integer.valueOf(b), 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class q
    extends u<a, z>
  {
    private q(e parame)
    {
      super();
    }
    
    public final String toString()
    {
      return ".getNewMissedCalls()";
    }
  }
  
  static final class r
    extends u<a, z>
  {
    private final long b;
    
    private r(e parame, long paramLong)
    {
      super();
      b = paramLong;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".getNewMissedCalls(");
      localStringBuilder.append(a(Long.valueOf(b), 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class s
    extends u<a, z>
  {
    private final int b;
    
    private s(e parame, int paramInt)
    {
      super();
      b = paramInt;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".getSearchHistoryWithContact(");
      localStringBuilder.append(a(Integer.valueOf(b), 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class t
    extends u<a, Void>
  {
    private t(e parame)
    {
      super();
    }
    
    public final String toString()
    {
      return ".markAllAsSeen()";
    }
  }
  
  static final class u
    extends u<a, Boolean>
  {
    private final Set<Long> b;
    
    private u(e parame, Set<Long> paramSet)
    {
      super();
      b = paramSet;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".markAsSeenByHistoryIds(");
      localStringBuilder.append(a(b, 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class v
    extends u<a, Void>
  {
    private final long b;
    
    private v(e parame, long paramLong)
    {
      super();
      b = paramLong;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".markAsSeen(");
      localStringBuilder.append(a(Long.valueOf(b), 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class w
    extends u<a, Boolean>
  {
    private final Set<Long> b;
    
    private w(e parame, Set<Long> paramSet)
    {
      super();
      b = paramSet;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".markAsSeen(");
      localStringBuilder.append(a(b, 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class x
    extends u<a, Void>
  {
    private final String b;
    
    private x(e parame, String paramString)
    {
      super();
      b = paramString;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".markMissedCallsAsSeen(");
      localStringBuilder.append(a(b, 1));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class y
    extends u<a, Void>
  {
    private final long b;
    
    private y(e parame, long paramLong)
    {
      super();
      b = paramLong;
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(".markMissedCallsAsShown(");
      localStringBuilder.append(a(Long.valueOf(b), 2));
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
  }
  
  static final class z
    extends u<a, Void>
  {
    private z(e parame)
    {
      super();
    }
    
    public final String toString()
    {
      return ".performFullSync()";
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */