package com.truecaller.callhistory;

import android.content.ContentResolver;
import dagger.a.d;
import javax.inject.Provider;

public final class x
  implements d<w>
{
  private final Provider<ContentResolver> a;
  
  private x(Provider<ContentResolver> paramProvider)
  {
    a = paramProvider;
  }
  
  public static x a(Provider<ContentResolver> paramProvider)
  {
    return new x(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.x
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */