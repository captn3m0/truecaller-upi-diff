package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import java.util.Collection;

final class s$b
  extends u<r, Boolean>
{
  private final Collection<Long> b;
  
  private s$b(e parame, Collection<Long> paramCollection)
  {
    super(parame);
    b = paramCollection;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".delete(");
    localStringBuilder.append(a(b, 2));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.s.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */