package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class b$p
  extends u<a, z>
{
  private final int b;
  
  private b$p(e parame, int paramInt)
  {
    super(parame);
    b = paramInt;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".getMostCalledEvents(");
    localStringBuilder.append(a(Integer.valueOf(b), 2));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */