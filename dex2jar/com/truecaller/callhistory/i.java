package com.truecaller.callhistory;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class i
  implements d<k>
{
  private final g a;
  private final Provider<Context> b;
  
  private i(g paramg, Provider<Context> paramProvider)
  {
    a = paramg;
    b = paramProvider;
  }
  
  public static i a(g paramg, Provider<Context> paramProvider)
  {
    return new i(paramg, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */