package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import java.util.Set;

final class b$u
  extends u<a, Boolean>
{
  private final Set<Long> b;
  
  private b$u(e parame, Set<Long> paramSet)
  {
    super(parame);
    b = paramSet;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".markAsSeenByHistoryIds(");
    localStringBuilder.append(a(b, 2));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.u
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */