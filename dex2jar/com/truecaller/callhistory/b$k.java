package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.HistoryEvent;
import java.util.List;

final class b$k
  extends u<a, List<HistoryEvent>>
{
  private final FilterType b;
  private final Integer c;
  
  private b$k(e parame, FilterType paramFilterType, Integer paramInteger)
  {
    super(parame);
    b = paramFilterType;
    c = paramInteger;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".getCallHistoryList(");
    localStringBuilder.append(a(b, 2));
    localStringBuilder.append(",");
    localStringBuilder.append(a(c, 2));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */