package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.CallRecording;

final class s$a
  extends u<r, Boolean>
{
  private final CallRecording b;
  
  private s$a(e parame, CallRecording paramCallRecording)
  {
    super(parame);
    b = paramCallRecording;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".delete(");
    localStringBuilder.append(a(b, 2));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.s.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */