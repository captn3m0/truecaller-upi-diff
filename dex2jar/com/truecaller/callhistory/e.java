package com.truecaller.callhistory;

import android.content.ContentValues;
import android.database.Cursor;
import com.google.c.a.k.d;
import com.truecaller.data.access.d;
import com.truecaller.data.entity.HistoryEvent;
import java.util.UUID;

public final class e
{
  public static final ContentValues a(HistoryEvent paramHistoryEvent)
  {
    c.g.b.k.b(paramHistoryEvent, "receiver$0");
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("tc_id", paramHistoryEvent.getTcId());
    Object localObject1 = paramHistoryEvent.a();
    Object localObject2 = null;
    if (localObject1 != null) {
      localObject1 = p.a((String)localObject1);
    } else {
      localObject1 = null;
    }
    localContentValues.put("normalized_number", (String)localObject1);
    String str = paramHistoryEvent.b();
    localObject1 = localObject2;
    if (str != null) {
      localObject1 = p.a(str);
    }
    localContentValues.put("raw_number", (String)localObject1);
    if (paramHistoryEvent.c() != null) {
      localObject1 = paramHistoryEvent.c();
    } else {
      localObject1 = k.d.l;
    }
    localContentValues.put("number_type", ((k.d)localObject1).name());
    localContentValues.put("country_code", paramHistoryEvent.d());
    localContentValues.put("cached_name", paramHistoryEvent.e());
    localContentValues.put("type", Integer.valueOf(paramHistoryEvent.f()));
    localContentValues.put("action", Integer.valueOf(paramHistoryEvent.h()));
    localContentValues.put("call_log_id", paramHistoryEvent.i());
    long l2 = paramHistoryEvent.j();
    long l1 = l2;
    if (l2 < 1L) {
      l1 = System.currentTimeMillis();
    }
    localContentValues.put("timestamp", Long.valueOf(l1));
    localContentValues.put("duration", Long.valueOf(paramHistoryEvent.k()));
    localContentValues.put("subscription_id", paramHistoryEvent.l());
    localContentValues.put("feature", Integer.valueOf(paramHistoryEvent.m()));
    localContentValues.put("new", Integer.valueOf(paramHistoryEvent.o()));
    localContentValues.put("is_read", Integer.valueOf(paramHistoryEvent.p()));
    localContentValues.put("subscription_component_name", paramHistoryEvent.q());
    localContentValues.put("tc_flag", Integer.valueOf(paramHistoryEvent.r()));
    localContentValues.put("event_id", (String)org.c.a.a.a.k.e((CharSequence)paramHistoryEvent.u(), (CharSequence)UUID.randomUUID().toString()));
    return localContentValues;
  }
  
  public static final aa a(Cursor paramCursor)
  {
    c.g.b.k.b(paramCursor, "receiver$0");
    return new aa(paramCursor, new com.truecaller.data.access.e(paramCursor), new d(paramCursor));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */