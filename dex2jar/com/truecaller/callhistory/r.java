package com.truecaller.callhistory;

import com.truecaller.androidactors.w;
import com.truecaller.data.entity.CallRecording;
import java.util.Collection;

public abstract interface r
{
  public abstract w<z> a();
  
  public abstract w<Boolean> a(CallRecording paramCallRecording);
  
  public abstract w<Boolean> a(Collection<Long> paramCollection);
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.r
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */