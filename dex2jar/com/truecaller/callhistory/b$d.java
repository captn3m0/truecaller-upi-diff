package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;

final class b$d
  extends u<a, Boolean>
{
  private final HistoryEvent b;
  private final Contact c;
  
  private b$d(e parame, HistoryEvent paramHistoryEvent, Contact paramContact)
  {
    super(parame);
    b = paramHistoryEvent;
    c = paramContact;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".addWithContact(");
    localStringBuilder.append(a(b, 1));
    localStringBuilder.append(",");
    localStringBuilder.append(a(c, 1));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */