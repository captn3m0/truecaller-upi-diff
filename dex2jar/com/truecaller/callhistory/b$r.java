package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class b$r
  extends u<a, z>
{
  private final long b;
  
  private b$r(e parame, long paramLong)
  {
    super(parame);
    b = paramLong;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".getNewMissedCalls(");
    localStringBuilder.append(a(Long.valueOf(b), 2));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.r
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */