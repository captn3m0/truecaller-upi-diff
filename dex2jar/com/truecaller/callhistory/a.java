package com.truecaller.callhistory;

import com.truecaller.androidactors.w;
import com.truecaller.calling.dialer.HistoryEventsScope;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public abstract interface a
{
  public abstract w<Boolean> a(int paramInt, Collection<Long> paramCollection);
  
  public abstract w<z> a(long paramLong);
  
  public abstract w<List<HistoryEvent>> a(FilterType paramFilterType, Integer paramInteger);
  
  public abstract w<z> a(Contact paramContact);
  
  public abstract w<Boolean> a(HistoryEvent paramHistoryEvent, Contact paramContact);
  
  public abstract w<z> a(String paramString);
  
  public abstract w<Integer> a(List<HistoryEvent> paramList);
  
  public abstract w<Boolean> a(List<Long> paramList1, List<Long> paramList2, HistoryEventsScope paramHistoryEventsScope);
  
  public abstract w<Boolean> a(Set<Long> paramSet);
  
  public abstract void a();
  
  public abstract void a(int paramInt);
  
  public abstract void a(ai.a parama);
  
  public abstract void a(CallRecording paramCallRecording);
  
  public abstract void a(HistoryEvent paramHistoryEvent);
  
  public abstract w<z> b();
  
  public abstract w<z> b(int paramInt);
  
  public abstract w<Boolean> b(Set<Long> paramSet);
  
  public abstract void b(long paramLong);
  
  public abstract void b(String paramString);
  
  public abstract w<z> c();
  
  public abstract w<z> c(int paramInt);
  
  public abstract w<HistoryEvent> c(String paramString);
  
  public abstract void c(long paramLong);
  
  public abstract w<z> d();
  
  public abstract w<HistoryEvent> d(String paramString);
  
  public abstract w<Integer> e();
  
  public abstract void f();
  
  public abstract void g();
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */