package com.truecaller.callhistory;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import com.truecaller.androidactors.ab;
import com.truecaller.androidactors.w;
import com.truecaller.content.TruecallerContract.k;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.log.AssertionUtil;
import com.truecaller.service.WidgetListProvider;
import com.truecaller.util.q;
import java.util.Collection;
import javax.inject.Inject;

public final class ag
  implements af
{
  private final Context a;
  
  @Inject
  public ag(Context paramContext)
  {
    a = paramContext;
  }
  
  public final w<Boolean> a(int paramInt, Collection<Long> paramCollection)
  {
    c.g.b.k.b(paramCollection, "ids");
    boolean bool2 = true;
    boolean bool1;
    if ((paramInt != 5) && (paramInt != 6)) {
      bool1 = false;
    } else {
      bool1 = true;
    }
    for (;;)
    {
      try
      {
        AssertionUtil.isTrue(bool1, new String[0]);
        StringBuilder localStringBuilder = new StringBuilder("type=? AND _id IN (");
        localStringBuilder.append(org.c.a.a.a.k.a((Iterable)paramCollection, ','));
        localStringBuilder.append(")");
        paramCollection = localStringBuilder.toString();
        paramInt = a.getContentResolver().delete(TruecallerContract.n.a(), paramCollection, new String[] { String.valueOf(paramInt) });
        if (paramInt == 0) {
          break label157;
        }
        WidgetListProvider.a(a);
      }
      catch (SQLiteException paramCollection)
      {
        AssertionUtil.reportThrowableButNeverCrash((Throwable)paramCollection);
        paramCollection = w.b(Boolean.FALSE);
        c.g.b.k.a(paramCollection, "Promise.wrap(false)");
        return paramCollection;
      }
      paramCollection = w.b(Boolean.valueOf(bool1));
      c.g.b.k.a(paramCollection, "Promise.wrap(result > 0)");
      return paramCollection;
      label157:
      if (paramInt > 0) {
        bool1 = bool2;
      } else {
        bool1 = false;
      }
    }
  }
  
  public final void a(int paramInt)
  {
    boolean bool;
    if ((paramInt != 5) && (paramInt != 6)) {
      bool = false;
    } else {
      bool = true;
    }
    try
    {
      AssertionUtil.isTrue(bool, new String[0]);
      if (a.getContentResolver().delete(TruecallerContract.n.a(), "type=?", new String[] { String.valueOf(paramInt) }) != 0)
      {
        WidgetListProvider.a(a);
        return;
      }
    }
    catch (SQLiteException localSQLiteException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localSQLiteException);
    }
  }
  
  public final boolean a(HistoryEvent paramHistoryEvent)
  {
    c.g.b.k.b(paramHistoryEvent, "event");
    return (paramHistoryEvent.f() == 5) || (paramHistoryEvent.f() == 6);
  }
  
  public final w<z> b(int paramInt)
  {
    boolean bool;
    if ((paramInt != 5) && (paramInt != 6)) {
      bool = false;
    } else {
      bool = true;
    }
    AssertionUtil.isTrue(bool, new String[0]);
    try
    {
      localObject = a.getContentResolver().query(TruecallerContract.n.d(), null, "type=?", new String[] { String.valueOf(paramInt) }, "timestamp DESC");
      if (localObject == null) {
        break label109;
      }
      try
      {
        w localw = w.a(e.a((Cursor)localObject), (ab)a.a);
        c.g.b.k.a(localw, "Promise.wrap<HistoryEven…tCursor()) { it.close() }");
        return localw;
      }
      catch (SQLiteException localSQLiteException1) {}
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localSQLiteException2);
    }
    catch (SQLiteException localSQLiteException2)
    {
      localObject = null;
    }
    q.a((Cursor)localObject);
    label109:
    Object localObject = w.b(null);
    c.g.b.k.a(localObject, "Promise.wrap(null)");
    return (w<z>)localObject;
  }
  
  public final void b(HistoryEvent paramHistoryEvent)
  {
    c.g.b.k.b(paramHistoryEvent, "event");
    Object localObject2;
    Object localObject1;
    if ((org.c.a.a.a.k.b((CharSequence)paramHistoryEvent.getTcId())) && (!org.c.a.a.a.k.b((CharSequence)paramHistoryEvent.a())))
    {
      localObject2 = null;
      localObject1 = localObject2;
      try
      {
        ContentResolver localContentResolver = a.getContentResolver();
        localObject1 = localObject2;
        Uri localUri = TruecallerContract.k.a();
        localObject1 = localObject2;
        String str = paramHistoryEvent.a();
        localObject1 = localObject2;
        localObject2 = localContentResolver.query(localUri, new String[] { "tc_id" }, "data1=? AND data_type=4", new String[] { str }, null);
        if (localObject2 != null)
        {
          localObject1 = localObject2;
          if (((Cursor)localObject2).moveToFirst())
          {
            localObject1 = localObject2;
            paramHistoryEvent.setTcId(((Cursor)localObject2).getString(0));
          }
        }
      }
      finally
      {
        q.a((Cursor)localObject1);
      }
    }
    if ((org.c.a.a.a.k.c((CharSequence)paramHistoryEvent.getTcId())) && (paramHistoryEvent.f() != 6))
    {
      localObject1 = new ContentValues();
      ((ContentValues)localObject1).put("timestamp", Long.valueOf(paramHistoryEvent.j()));
      localObject2 = paramHistoryEvent.getTcId();
      if (localObject2 != null) {
        if (a.getContentResolver().update(TruecallerContract.n.a(), (ContentValues)localObject1, "tc_id=? AND type=5", new String[] { localObject2 }) != 0)
        {
          WidgetListProvider.a(a);
          return;
        }
      }
    }
    paramHistoryEvent.b(0);
    if (a.getContentResolver().insert(TruecallerContract.n.a(), e.a(paramHistoryEvent)) != null) {
      WidgetListProvider.a(a);
    }
  }
  
  static final class a<T>
    implements ab<R>
  {
    public static final a a = new a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.ag
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */