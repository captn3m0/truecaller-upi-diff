package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.CallRecording;

final class b$a
  extends u<a, Void>
{
  private final CallRecording b;
  
  private b$a(e parame, CallRecording paramCallRecording)
  {
    super(parame);
    b = paramCallRecording;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".addCallRecording(");
    localStringBuilder.append(a(b, 1));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */