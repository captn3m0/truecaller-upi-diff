package com.truecaller.callhistory;

import com.truecaller.androidactors.f;
import dagger.a.d;
import javax.inject.Provider;

public final class ak
  implements d<aj>
{
  private final Provider<f<a>> a;
  
  private ak(Provider<f<a>> paramProvider)
  {
    a = paramProvider;
  }
  
  public static ak a(Provider<f<a>> paramProvider)
  {
    return new ak(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.ak
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */