package com.truecaller.callhistory;

import com.truecaller.androidactors.w;
import com.truecaller.data.entity.HistoryEvent;
import java.util.Collection;

public abstract interface af
{
  public abstract w<Boolean> a(int paramInt, Collection<Long> paramCollection);
  
  public abstract void a(int paramInt);
  
  public abstract boolean a(HistoryEvent paramHistoryEvent);
  
  public abstract w<z> b(int paramInt);
  
  public abstract void b(HistoryEvent paramHistoryEvent);
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.af
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */