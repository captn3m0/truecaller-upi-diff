package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.calling.dialer.HistoryEventsScope;
import java.util.List;

final class b$f
  extends u<a, Boolean>
{
  private final List<Long> b;
  private final List<Long> c;
  private final HistoryEventsScope d;
  
  private b$f(e parame, List<Long> paramList1, List<Long> paramList2, HistoryEventsScope paramHistoryEventsScope)
  {
    super(parame);
    b = paramList1;
    c = paramList2;
    d = paramHistoryEventsScope;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".deleteHistory(");
    localStringBuilder.append(a(b, 2));
    localStringBuilder.append(",");
    localStringBuilder.append(a(c, 2));
    localStringBuilder.append(",");
    localStringBuilder.append(a(d, 2));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */