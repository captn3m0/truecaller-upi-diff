package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.HistoryEvent;
import java.util.List;

final class b$b
  extends u<a, Integer>
{
  private final List<HistoryEvent> b;
  
  private b$b(e parame, List<HistoryEvent> paramList)
  {
    super(parame);
    b = paramList;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".addFromBackup(");
    localStringBuilder.append(a(b, 1));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */