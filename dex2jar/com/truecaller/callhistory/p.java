package com.truecaller.callhistory;

import c.g.a.a;
import c.l;
import com.truecaller.common.h.ab;

public final class p
{
  public static final String a(String paramString)
  {
    if (ab.a(paramString)) {
      return "";
    }
    String str = paramString;
    if (paramString == null) {
      str = "";
    }
    return str;
  }
  
  static final String a(String paramString1, String paramString2, a<Boolean> parama)
  {
    boolean bool = ((Boolean)parama.invoke()).booleanValue();
    if (bool == true)
    {
      parama = new StringBuilder();
      parama.append(paramString1);
      parama.append(" AND ");
      parama.append(paramString2);
      return parama.toString();
    }
    if (!bool) {
      return paramString1;
    }
    throw new l();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */