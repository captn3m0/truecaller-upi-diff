package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.Contact;

final class b$j
  extends u<a, z>
{
  private final Contact b;
  
  private b$j(e parame, Contact paramContact)
  {
    super(parame);
    b = paramContact;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".getCallHistoryForContact(");
    localStringBuilder.append(a(b, 1));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */