package com.truecaller.callhistory;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import c.g.b.k;
import com.truecaller.androidactors.ab;
import com.truecaller.androidactors.w;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.log.AssertionUtil;
import com.truecaller.util.q;
import javax.inject.Inject;

public final class aq
  implements ap
{
  private final Context a;
  
  @Inject
  public aq(Context paramContext)
  {
    a = paramContext;
  }
  
  public final w<z> a()
  {
    try
    {
      localObject = a.getContentResolver().query(TruecallerContract.n.d(), null, "action NOT IN (5)  AND tc_flag!=3 AND type!=6", null, "timestamp DESC LIMIT 20");
      if (localObject == null) {
        break label64;
      }
      try
      {
        w localw = w.a(e.a((Cursor)localObject), (ab)a.a);
        k.a(localw, "Promise.wrap<HistoryEven…tCursor()) { it.close() }");
        return localw;
      }
      catch (SQLiteException localSQLiteException1) {}
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localSQLiteException2);
    }
    catch (SQLiteException localSQLiteException2)
    {
      localObject = null;
    }
    q.a((Cursor)localObject);
    label64:
    Object localObject = w.b(null);
    k.a(localObject, "Promise.wrap(null)");
    return (w<z>)localObject;
  }
  
  static final class a<T>
    implements ab<R>
  {
    public static final a a = new a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.aq
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */