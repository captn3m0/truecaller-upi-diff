package com.truecaller.callhistory;

import com.truecaller.androidactors.h;
import java.util.concurrent.TimeUnit;

public class CallHistoryService
  extends h
{
  public CallHistoryService()
  {
    super("call-history", TimeUnit.SECONDS.toMillis(30L), true);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.CallHistoryService
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */