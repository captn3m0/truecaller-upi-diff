package com.truecaller.callhistory;

import android.annotation.SuppressLint;
import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import com.truecaller.calling.recorder.CallRecordingManager;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.g;
import com.truecaller.i.c;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.multisim.h;
import com.truecaller.utils.d;
import com.truecaller.utils.l;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import javax.inject.Inject;

public final class am
  implements al
{
  private long a;
  private final l b;
  private final dagger.a<h> c;
  private final v d;
  private final k e;
  private final g f;
  private final ai g;
  private final Context h;
  private final c i;
  private final d j;
  private final CallRecordingManager k;
  
  @Inject
  public am(l paraml, dagger.a<h> parama, v paramv, k paramk, g paramg, ai paramai, Context paramContext, c paramc, d paramd, CallRecordingManager paramCallRecordingManager)
  {
    b = paraml;
    c = parama;
    d = paramv;
    e = paramk;
    f = paramg;
    g = paramai;
    h = paramContext;
    i = paramc;
    j = paramd;
    k = paramCallRecordingManager;
    a = -1L;
  }
  
  private static long a(long paramLong, ArrayList<ContentValues> paramArrayList1, ArrayList<ContentValues> paramArrayList2)
  {
    long l = paramLong;
    if (!paramArrayList2.isEmpty())
    {
      paramArrayList2 = ((ContentValues)paramArrayList2.get(paramArrayList2.size() - 1)).getAsLong("timestamp");
      if (paramArrayList2 == null) {
        c.g.b.k.a();
      }
      l = Math.max(paramLong, paramArrayList2.longValue());
    }
    paramLong = l;
    if (!paramArrayList1.isEmpty())
    {
      paramArrayList1 = ((ContentValues)paramArrayList1.get(paramArrayList1.size() - 1)).getAsLong("timestamp");
      if (paramArrayList1 == null) {
        c.g.b.k.a();
      }
      paramLong = Math.max(l, paramArrayList1.longValue());
    }
    return paramLong;
  }
  
  @SuppressLint({"Recycle"})
  private final ac a(ContentResolver paramContentResolver, long paramLong)
  {
    if (b.a(new String[] { "android.permission.READ_CALL_LOG" }))
    {
      if (!b.a(new String[] { "android.permission.READ_PHONE_STATE" })) {
        return null;
      }
      Object localObject2 = e.b();
      Object localObject1 = c.get();
      c.g.b.k.a(localObject1, "multiSimManager.get()");
      String str = ((h)localObject1).d();
      localObject1 = localObject2;
      if (str != null) {
        localObject1 = (String[])org.c.a.a.a.a.b((Object[])localObject2, str);
      }
      try
      {
        localObject1 = paramContentResolver.query(e.a(), (String[])localObject1, "date<=?", new String[] { String.valueOf(paramLong) }, "date DESC, _id ASC");
        if (localObject1 == null) {
          return null;
        }
        try
        {
          paramContentResolver = (ac)new ad(f, ((h)c.get()).a((Cursor)localObject1));
          return paramContentResolver;
        }
        catch (IllegalArgumentException paramContentResolver) {}
        if (localObject1 == null) {
          break label354;
        }
      }
      catch (SecurityException paramContentResolver)
      {
        AssertionUtil.reportThrowableButNeverCrash((Throwable)paramContentResolver);
        return null;
      }
      catch (SQLiteException paramContentResolver)
      {
        AssertionUtil.reportThrowableButNeverCrash((Throwable)paramContentResolver);
        return null;
      }
      catch (IllegalArgumentException paramContentResolver)
      {
        localObject1 = null;
      }
      paramContentResolver = ((Cursor)localObject1).getColumnNames();
      if (paramContentResolver == null)
      {
        paramContentResolver = null;
      }
      else
      {
        int n = paramContentResolver.length;
        if (paramContentResolver == null)
        {
          paramContentResolver = null;
        }
        else
        {
          int m = n + 0;
          if (m <= 0)
          {
            paramContentResolver = "";
          }
          else
          {
            localObject2 = new StringBuilder(m * 16);
            m = 0;
            while (m < n)
            {
              if (m > 0) {
                ((StringBuilder)localObject2).append(',');
              }
              if (paramContentResolver[m] != null) {
                ((StringBuilder)localObject2).append(paramContentResolver[m]);
              }
              m += 1;
            }
            paramContentResolver = ((StringBuilder)localObject2).toString();
          }
        }
      }
      AssertionUtil.report(new String[] { "Can't create remote calls cursor. Available columns: ".concat(String.valueOf(paramContentResolver)) });
      ((Cursor)localObject1).close();
      return null;
      label354:
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramContentResolver);
      return null;
    }
    return null;
  }
  
  private final ai.a a(ac paramac, z paramz, List<ContentProviderOperation> paramList1, List<ContentProviderOperation> paramList2)
  {
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    boolean bool2 = paramac.moveToFirst();
    boolean bool1 = paramz.moveToFirst();
    boolean bool3;
    long l1;
    long l2;
    Object localObject2;
    Object localObject1;
    do
    {
      for (;;)
      {
        bool3 = bool2;
        if (!bool2) {
          break label455;
        }
        bool3 = bool2;
        if (!bool1) {
          break label455;
        }
        if (!paramac.a()) {
          break;
        }
        bool2 = paramac.moveToNext();
      }
      l1 = paramz.c();
      l2 = paramac.c();
      long l5 = paramz.b();
      long l6 = paramac.b();
      if (paramz.b() == -1L)
      {
        a(paramz.d(), (List)localArrayList2, (List)localArrayList1, paramList1, paramList2);
        bool1 = paramz.moveToNext();
      }
      else
      {
        long l3 = l1;
        long l4 = l2;
        if (l3 > l4)
        {
          a(paramz.a(), paramList1);
          bool1 = paramz.moveToNext();
        }
        else if (l3 < l4)
        {
          a(paramac.d(), localArrayList2, localArrayList1, paramList1, paramList2);
          bool2 = paramac.moveToNext();
        }
        else if (l5 > l6)
        {
          a(paramz.a(), paramList1);
          bool1 = paramz.moveToNext();
        }
        else if (l5 < l6)
        {
          a(paramac.d(), localArrayList2, localArrayList1, paramList1, paramList2);
          bool2 = paramac.moveToNext();
        }
        else
        {
          localObject2 = paramz.e();
          c.g.b.k.a(localObject2, "local.simToken");
          localObject1 = paramac.e();
          c.g.b.k.a(localObject1, "remote.simToken");
          if (!org.c.a.a.a.k.a((CharSequence)localObject2, (CharSequence)localObject1))
          {
            localObject2 = ContentProviderOperation.newUpdate(TruecallerContract.n.a());
            ((ContentProviderOperation.Builder)localObject2).withValue("subscription_id", localObject1);
            ((ContentProviderOperation.Builder)localObject2).withSelection("_id=?", new String[] { String.valueOf(paramz.a()) });
            localObject1 = ((ContentProviderOperation.Builder)localObject2).build();
            c.g.b.k.a(localObject1, "operation.build()");
            paramList1.add(localObject1);
          }
          bool1 = paramz.moveToNext();
          bool2 = paramac.moveToNext();
        }
      }
    } while (paramList1.size() < 100);
    return new ai.a(0, a(Math.max(l1, l2), localArrayList2, localArrayList1));
    for (;;)
    {
      label455:
      bool2 = bool1;
      if (!bool3) {
        break;
      }
      localObject1 = paramac.d();
      if (localObject1 != null)
      {
        if (localArrayList1.isEmpty())
        {
          localObject2 = ContentProviderOperation.newInsert(TruecallerContract.n.a());
          ((ContentProviderOperation.Builder)localObject2).withValues(e.a((HistoryEvent)localObject1));
          localObject2 = ((ContentProviderOperation.Builder)localObject2).build();
          c.g.b.k.a(localObject2, "operation.build()");
          paramList1.add(localObject2);
        }
        else
        {
          a((HistoryEvent)localObject1, localArrayList2, localArrayList1, paramList1, paramList2);
        }
        if (paramList1.size() >= 100) {
          return new ai.a(0, a(((HistoryEvent)localObject1).j(), localArrayList2, localArrayList1));
        }
      }
      bool3 = paramac.moveToNext();
    }
    while (bool2)
    {
      if (paramz.b() != -1L) {
        a(paramz.a(), paramList1);
      } else if (!localArrayList2.isEmpty()) {
        a(paramz.d(), (List)localArrayList2, (List)localArrayList1, paramList1, paramList2);
      }
      bool2 = paramz.moveToNext();
    }
    paramac = localArrayList2.listIterator();
    c.g.b.k.a(paramac, "toBeAdded.listIterator()");
    while (paramac.hasNext())
    {
      paramz = ContentProviderOperation.newInsert(TruecallerContract.n.a());
      paramz.withValues((ContentValues)paramac.next());
      paramac.remove();
      paramz = paramz.build();
      c.g.b.k.a(paramz, "operation.build()");
      paramList1.add(paramz);
    }
    return new ai.a();
  }
  
  private static void a(long paramLong, List<ContentProviderOperation> paramList)
  {
    Object localObject = ContentProviderOperation.newDelete(TruecallerContract.n.a());
    ((ContentProviderOperation.Builder)localObject).withSelection("_id=?", new String[] { String.valueOf(paramLong) });
    localObject = ((ContentProviderOperation.Builder)localObject).build();
    c.g.b.k.a(localObject, "builder.build()");
    paramList.add(localObject);
  }
  
  private final void a(HistoryEvent paramHistoryEvent, ArrayList<ContentValues> paramArrayList1, ArrayList<ContentValues> paramArrayList2, List<ContentProviderOperation> paramList1, List<ContentProviderOperation> paramList2)
  {
    Object localObject1 = new StringBuilder("scheduleEventToAdd() called with: remote = [");
    ((StringBuilder)localObject1).append(paramHistoryEvent);
    ((StringBuilder)localObject1).append("], toBeAdded = [");
    ((StringBuilder)localObject1).append(paramArrayList1);
    ((StringBuilder)localObject1).append("], toBeUpdated = [");
    ((StringBuilder)localObject1).append(paramArrayList2);
    ((StringBuilder)localObject1).append("], localOperations = [");
    ((StringBuilder)localObject1).append(paramList1);
    ((StringBuilder)localObject1).append("], remoteOperations = [");
    ((StringBuilder)localObject1).append(paramList2);
    ((StringBuilder)localObject1).append("]");
    ((StringBuilder)localObject1).toString();
    if (paramHistoryEvent == null) {
      return;
    }
    long l = paramHistoryEvent.j() + 10000L;
    if (!paramArrayList2.isEmpty())
    {
      paramArrayList2 = paramArrayList2.iterator();
      c.g.b.k.a(paramArrayList2, "toBeUpdated.iterator()");
      while (paramArrayList2.hasNext())
      {
        localObject1 = paramArrayList2.next();
        c.g.b.k.a(localObject1, "update.next()");
        localObject1 = (ContentValues)localObject1;
        if (((ContentValues)localObject1).getAsLong("timestamp").longValue() > l)
        {
          paramArrayList2.remove();
        }
        else
        {
          Object localObject2 = ((ContentValues)localObject1).getAsInteger("type");
          if (localObject2 == null) {
            c.g.b.k.a();
          }
          int m = ((Integer)localObject2).intValue();
          int n = paramHistoryEvent.f();
          localObject2 = ((ContentValues)localObject1).getAsString("normalized_number");
          String str = paramHistoryEvent.a();
          Long localLong = ((ContentValues)localObject1).getAsLong("timestamp");
          if (localLong == null) {
            c.g.b.k.a();
          }
          if (ao.a(m, n, (String)localObject2, str, localLong.longValue(), paramHistoryEvent.j()))
          {
            paramArrayList1 = ((ContentValues)localObject1).getAsInteger("action");
            if ((paramArrayList1 != null) && (paramArrayList1.intValue() == 5))
            {
              paramArrayList1 = ((ContentValues)localObject1).getAsLong("_id");
              if (paramArrayList1 == null) {
                c.g.b.k.a();
              }
              a(paramArrayList1.longValue(), paramList1);
              paramArrayList1 = ContentProviderOperation.newDelete(e.a());
              paramArrayList1.withSelection("_id=?", new String[] { String.valueOf(paramHistoryEvent.i().longValue()) });
              paramHistoryEvent = paramArrayList1.build();
              c.g.b.k.a(paramHistoryEvent, "op.build()");
              paramList2.add(paramHistoryEvent);
            }
            else
            {
              paramArrayList1 = ContentProviderOperation.newUpdate(TruecallerContract.n.a());
              paramList2 = ((ContentValues)localObject1).getAsString("_id");
              ((ContentValues)localObject1).put("type", Integer.valueOf(paramHistoryEvent.f()));
              ((ContentValues)localObject1).put("call_log_id", paramHistoryEvent.i());
              ((ContentValues)localObject1).put("timestamp", Long.valueOf(paramHistoryEvent.j()));
              ((ContentValues)localObject1).put("duration", Long.valueOf(paramHistoryEvent.k()));
              ((ContentValues)localObject1).put("subscription_id", paramHistoryEvent.l());
              ((ContentValues)localObject1).put("feature", Integer.valueOf(paramHistoryEvent.m()));
              ((ContentValues)localObject1).put("subscription_component_name", paramHistoryEvent.q());
              ((ContentValues)localObject1).remove("normalized_number");
              ((ContentValues)localObject1).remove("action");
              paramArrayList1.withValues((ContentValues)localObject1);
              paramArrayList1.withSelection("_id=?", new String[] { paramList2 });
              paramHistoryEvent = paramArrayList1.build();
              c.g.b.k.a(paramHistoryEvent, "builder.build()");
              paramList1.add(paramHistoryEvent);
            }
            paramArrayList2.remove();
            return;
          }
        }
      }
    }
    paramArrayList1.add(0, e.a(paramHistoryEvent));
    if ((j.e()) && (paramHistoryEvent.f() == 2))
    {
      paramArrayList2 = k.b();
      if (!org.c.a.a.a.k.b((CharSequence)paramArrayList2))
      {
        if (k.i()) {
          return;
        }
        paramList2 = paramHistoryEvent.u();
        if (paramArrayList2 == null) {
          c.g.b.k.a();
        }
        paramArrayList2 = new CallRecording(-1L, paramList2, paramArrayList2);
        paramHistoryEvent.a(paramArrayList2);
        d.a(paramArrayList2);
        k.h();
      }
    }
    paramHistoryEvent = paramArrayList1.listIterator(paramArrayList1.size());
    c.g.b.k.a(paramHistoryEvent, "toBeAdded.listIterator(toBeAdded.size)");
    while (paramHistoryEvent.hasPrevious())
    {
      paramArrayList1 = paramHistoryEvent.previous();
      c.g.b.k.a(paramArrayList1, "it.previous()");
      paramArrayList1 = (ContentValues)paramArrayList1;
      if (paramArrayList1.getAsLong("timestamp").longValue() <= l) {
        break;
      }
      paramArrayList2 = ContentProviderOperation.newInsert(TruecallerContract.n.a());
      paramArrayList2.withValues(paramArrayList1);
      paramArrayList1 = paramArrayList2.build();
      c.g.b.k.a(paramArrayList1, "builder.build()");
      paramList1.add(paramArrayList1);
      paramHistoryEvent.remove();
    }
  }
  
  private final void a(HistoryEvent paramHistoryEvent, List<ContentValues> paramList1, List<ContentValues> paramList2, List<ContentProviderOperation> paramList3, List<ContentProviderOperation> paramList4)
  {
    if (paramHistoryEvent == null) {
      return;
    }
    AssertionUtil.AlwaysFatal.isNotNull(paramHistoryEvent.getId(), new String[] { "Event must have record in local database" });
    if (paramList1.isEmpty())
    {
      paramList1 = new ContentValues();
      paramList1.put("_id", paramHistoryEvent.getId());
      paramList1.put("timestamp", Long.valueOf(paramHistoryEvent.j()));
      paramList1.put("normalized_number", paramHistoryEvent.a());
      paramList1.put("action", Integer.valueOf(paramHistoryEvent.h()));
      paramList1.put("type", Integer.valueOf(paramHistoryEvent.f()));
      paramList2.add(paramList1);
      return;
    }
    paramList1 = paramList1.iterator();
    while (paramList1.hasNext())
    {
      ContentValues localContentValues = (ContentValues)paramList1.next();
      int m = paramHistoryEvent.f();
      Object localObject = localContentValues.getAsInteger("type");
      if (localObject == null) {
        c.g.b.k.a();
      }
      int n = ((Integer)localObject).intValue();
      localObject = paramHistoryEvent.a();
      String str = localContentValues.getAsString("normalized_number");
      long l = paramHistoryEvent.j();
      Long localLong = localContentValues.getAsLong("timestamp");
      if (localLong == null) {
        c.g.b.k.a();
      }
      if (ao.a(m, n, (String)localObject, str, l, localLong.longValue()))
      {
        if (paramHistoryEvent.h() == 5)
        {
          paramHistoryEvent = paramHistoryEvent.getId();
          if (paramHistoryEvent == null) {
            c.g.b.k.a();
          }
          c.g.b.k.a(paramHistoryEvent, "event.id!!");
          a(paramHistoryEvent.longValue(), paramList3);
          paramHistoryEvent = ContentProviderOperation.newDelete(e.a());
          paramHistoryEvent.withSelection("_id=?", new String[] { localContentValues.getAsString("call_log_id") });
          paramHistoryEvent = paramHistoryEvent.build();
          c.g.b.k.a(paramHistoryEvent, "op.build()");
          paramList4.add(paramHistoryEvent);
        }
        else
        {
          paramList2 = ContentProviderOperation.newUpdate(TruecallerContract.n.a());
          localContentValues.remove("tc_id");
          localContentValues.remove("normalized_number");
          localContentValues.remove("raw_number");
          localContentValues.remove("number_type");
          localContentValues.remove("country_code");
          localContentValues.remove("cached_name");
          localContentValues.remove("action");
          paramList2.withValues(localContentValues);
          paramList2.withSelection("_id=?", new String[] { String.valueOf(paramHistoryEvent.getId()) });
          paramHistoryEvent = paramList2.build();
          c.g.b.k.a(paramHistoryEvent, "builder.build()");
          paramList3.add(paramHistoryEvent);
        }
        paramList1.remove();
        return;
      }
    }
    paramList1 = new ContentValues();
    paramList1.put("_id", paramHistoryEvent.getId());
    paramList1.put("normalized_number", paramHistoryEvent.a());
    paramList1.put("timestamp", Long.valueOf(paramHistoryEvent.j()));
    paramList1.put("action", Integer.valueOf(paramHistoryEvent.h()));
    paramList1.put("type", Integer.valueOf(paramHistoryEvent.f()));
    paramList2.add(0, paramList1);
  }
  
  private static z b(ContentResolver paramContentResolver, long paramLong)
  {
    paramContentResolver = paramContentResolver.query(TruecallerContract.n.a(), null, "type IN (1,2,3)  AND timestamp<=? AND tc_flag!=3 AND (subscription_component_name!='com.whatsapp' OR subscription_component_name IS NULL) AND tc_flag!=2 AND (subscription_component_name!='com.truecaller.voip.manager.VOIP' OR subscription_component_name IS NULL)", new String[] { String.valueOf(paramLong) }, "timestamp DESC, call_log_id ASC");
    if (paramContentResolver == null)
    {
      AssertionUtil.reportWeirdnessButNeverCrash("Content resolver returned null cursor");
      return null;
    }
    return (z)new aa(paramContentResolver);
  }
  
  /* Error */
  public final void a(ai.a parama)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 584
    //   4: invokestatic 39	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   7: aload_0
    //   8: getfield 74	com/truecaller/callhistory/am:h	Landroid/content/Context;
    //   11: invokevirtual 590	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   14: astore 9
    //   16: new 91	java/util/ArrayList
    //   19: dup
    //   20: invokespecial 249	java/util/ArrayList:<init>	()V
    //   23: astore 11
    //   25: new 91	java/util/ArrayList
    //   28: dup
    //   29: invokespecial 249	java/util/ArrayList:<init>	()V
    //   32: astore 10
    //   34: aload_1
    //   35: astore 6
    //   37: aload_0
    //   38: getfield 84	com/truecaller/callhistory/am:a	J
    //   41: aload_1
    //   42: getfield 591	com/truecaller/callhistory/ai$a:a	J
    //   45: lcmp
    //   46: ifle +20 -> 66
    //   49: new 334	com/truecaller/callhistory/ai$a
    //   52: dup
    //   53: aload_1
    //   54: getfield 594	com/truecaller/callhistory/ai$a:b	I
    //   57: aload_0
    //   58: getfield 84	com/truecaller/callhistory/am:a	J
    //   61: invokespecial 339	com/truecaller/callhistory/ai$a:<init>	(IJ)V
    //   64: astore 6
    //   66: aconst_null
    //   67: astore 8
    //   69: aconst_null
    //   70: astore 4
    //   72: aconst_null
    //   73: astore 7
    //   75: aconst_null
    //   76: astore_1
    //   77: aload 9
    //   79: ldc_w 596
    //   82: invokestatic 161	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   85: aload_0
    //   86: aload 9
    //   88: aload 6
    //   90: getfield 591	com/truecaller/callhistory/ai$a:a	J
    //   93: invokespecial 598	com/truecaller/callhistory/am:a	(Landroid/content/ContentResolver;J)Lcom/truecaller/callhistory/ac;
    //   96: astore 5
    //   98: aload 5
    //   100: ifnonnull +16 -> 116
    //   103: aload 5
    //   105: checkcast 210	android/database/Cursor
    //   108: invokestatic 602	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   111: aconst_null
    //   112: invokestatic 602	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   115: return
    //   116: aload 8
    //   118: astore 4
    //   120: aload 9
    //   122: aload 6
    //   124: getfield 591	com/truecaller/callhistory/ai$a:a	J
    //   127: invokestatic 604	com/truecaller/callhistory/am:b	(Landroid/content/ContentResolver;J)Lcom/truecaller/callhistory/z;
    //   130: astore 6
    //   132: aload 6
    //   134: ifnonnull +20 -> 154
    //   137: aload 5
    //   139: checkcast 210	android/database/Cursor
    //   142: invokestatic 602	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   145: aload 6
    //   147: checkcast 210	android/database/Cursor
    //   150: invokestatic 602	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   153: return
    //   154: aload 6
    //   156: astore_1
    //   157: aload 6
    //   159: astore 4
    //   161: aload_0
    //   162: aload 5
    //   164: aload 6
    //   166: aload 11
    //   168: checkcast 271	java/util/List
    //   171: aload 10
    //   173: checkcast 271	java/util/List
    //   176: invokespecial 606	com/truecaller/callhistory/am:a	(Lcom/truecaller/callhistory/ac;Lcom/truecaller/callhistory/z;Ljava/util/List;Ljava/util/List;)Lcom/truecaller/callhistory/ai$a;
    //   179: astore 7
    //   181: aload 5
    //   183: checkcast 210	android/database/Cursor
    //   186: invokestatic 602	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   189: aload 6
    //   191: checkcast 210	android/database/Cursor
    //   194: invokestatic 602	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   197: aload 11
    //   199: invokevirtual 95	java/util/ArrayList:isEmpty	()Z
    //   202: ifeq +53 -> 255
    //   205: aload 10
    //   207: invokevirtual 95	java/util/ArrayList:isEmpty	()Z
    //   210: iconst_1
    //   211: anewarray 138	java/lang/String
    //   214: dup
    //   215: iconst_0
    //   216: ldc_w 608
    //   219: aastore
    //   220: invokestatic 612	com/truecaller/log/AssertionUtil$AlwaysFatal:isTrue	(Z[Ljava/lang/String;)V
    //   223: aload_0
    //   224: getfield 76	com/truecaller/callhistory/am:i	Lcom/truecaller/i/c;
    //   227: ldc_w 614
    //   230: iconst_1
    //   231: invokeinterface 619 3 0
    //   236: getstatic 624	com/truecaller/service/MissedCallsNotificationService:q	Lcom/truecaller/service/MissedCallsNotificationService$a;
    //   239: astore_1
    //   240: aload_0
    //   241: getfield 74	com/truecaller/callhistory/am:h	Landroid/content/Context;
    //   244: invokestatic 629	com/truecaller/service/MissedCallsNotificationService$a:a	(Landroid/content/Context;)V
    //   247: aload_0
    //   248: ldc2_w 81
    //   251: putfield 84	com/truecaller/callhistory/am:a	J
    //   254: return
    //   255: aload 7
    //   257: getfield 594	com/truecaller/callhistory/ai$a:b	I
    //   260: istore_2
    //   261: iconst_0
    //   262: istore_3
    //   263: iload_2
    //   264: iconst_1
    //   265: if_icmpne +8 -> 273
    //   268: iconst_1
    //   269: istore_2
    //   270: goto +5 -> 275
    //   273: iconst_0
    //   274: istore_2
    //   275: iload_2
    //   276: ifeq +61 -> 337
    //   279: invokestatic 301	com/truecaller/content/TruecallerContract$n:a	()Landroid/net/Uri;
    //   282: invokestatic 307	android/content/ContentProviderOperation:newUpdate	(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;
    //   285: astore_1
    //   286: aload_1
    //   287: ldc_w 440
    //   290: iconst_0
    //   291: invokestatic 450	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   294: invokevirtual 315	android/content/ContentProviderOperation$Builder:withValue	(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;
    //   297: pop
    //   298: aload_1
    //   299: ldc_w 631
    //   302: iconst_1
    //   303: anewarray 138	java/lang/String
    //   306: dup
    //   307: iconst_0
    //   308: invokestatic 636	java/lang/System:currentTimeMillis	()J
    //   311: getstatic 642	java/util/concurrent/TimeUnit:DAYS	Ljava/util/concurrent/TimeUnit;
    //   314: lconst_1
    //   315: invokevirtual 646	java/util/concurrent/TimeUnit:toMillis	(J)J
    //   318: lsub
    //   319: invokestatic 182	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   322: aastore
    //   323: invokevirtual 321	android/content/ContentProviderOperation$Builder:withSelection	(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;
    //   326: pop
    //   327: aload 11
    //   329: aload_1
    //   330: invokevirtual 325	android/content/ContentProviderOperation$Builder:build	()Landroid/content/ContentProviderOperation;
    //   333: invokevirtual 647	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   336: pop
    //   337: aload 9
    //   339: invokestatic 650	com/truecaller/content/TruecallerContract:a	()Ljava/lang/String;
    //   342: aload 11
    //   344: invokevirtual 654	android/content/ContentResolver:applyBatch	(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    //   347: astore_1
    //   348: aload_1
    //   349: ldc_w 656
    //   352: invokestatic 161	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   355: aload_1
    //   356: arraylength
    //   357: ifne +5 -> 362
    //   360: iconst_1
    //   361: istore_3
    //   362: iload_3
    //   363: iconst_1
    //   364: ixor
    //   365: ifeq +10 -> 375
    //   368: aload_0
    //   369: getfield 74	com/truecaller/callhistory/am:h	Landroid/content/Context;
    //   372: invokestatic 659	com/truecaller/service/WidgetListProvider:a	(Landroid/content/Context;)V
    //   375: aload 10
    //   377: invokevirtual 95	java/util/ArrayList:isEmpty	()Z
    //   380: ifne +36 -> 416
    //   383: aload 9
    //   385: ldc_w 661
    //   388: aload 10
    //   390: invokevirtual 654	android/content/ContentResolver:applyBatch	(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    //   393: pop
    //   394: goto +22 -> 416
    //   397: astore_1
    //   398: aload_1
    //   399: checkcast 202	java/lang/Throwable
    //   402: invokestatic 208	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   405: goto +11 -> 416
    //   408: astore_1
    //   409: aload_1
    //   410: checkcast 202	java/lang/Throwable
    //   413: invokestatic 208	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   416: iload_2
    //   417: ifeq +35 -> 452
    //   420: aload_0
    //   421: getfield 76	com/truecaller/callhistory/am:i	Lcom/truecaller/i/c;
    //   424: ldc_w 614
    //   427: iconst_1
    //   428: invokeinterface 619 3 0
    //   433: getstatic 624	com/truecaller/service/MissedCallsNotificationService:q	Lcom/truecaller/service/MissedCallsNotificationService$a;
    //   436: astore_1
    //   437: aload_0
    //   438: getfield 74	com/truecaller/callhistory/am:h	Landroid/content/Context;
    //   441: invokestatic 629	com/truecaller/service/MissedCallsNotificationService$a:a	(Landroid/content/Context;)V
    //   444: aload_0
    //   445: ldc2_w 81
    //   448: putfield 84	com/truecaller/callhistory/am:a	J
    //   451: return
    //   452: aload_0
    //   453: aload 7
    //   455: getfield 591	com/truecaller/callhistory/ai$a:a	J
    //   458: putfield 84	com/truecaller/callhistory/am:a	J
    //   461: aload_0
    //   462: getfield 72	com/truecaller/callhistory/am:g	Lcom/truecaller/callhistory/ai;
    //   465: aload 7
    //   467: invokeinterface 665 2 0
    //   472: return
    //   473: astore_1
    //   474: aload_1
    //   475: checkcast 202	java/lang/Throwable
    //   478: invokestatic 208	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   481: aload_0
    //   482: ldc2_w 81
    //   485: putfield 84	com/truecaller/callhistory/am:a	J
    //   488: return
    //   489: astore_1
    //   490: aload_1
    //   491: checkcast 202	java/lang/Throwable
    //   494: invokestatic 208	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   497: aload_0
    //   498: ldc2_w 81
    //   501: putfield 84	com/truecaller/callhistory/am:a	J
    //   504: return
    //   505: astore 6
    //   507: aload_1
    //   508: astore 4
    //   510: aload 6
    //   512: astore_1
    //   513: goto +70 -> 583
    //   516: astore 6
    //   518: aload 4
    //   520: astore_1
    //   521: aload 5
    //   523: astore 4
    //   525: aload 6
    //   527: astore 5
    //   529: goto +18 -> 547
    //   532: astore_1
    //   533: aconst_null
    //   534: astore 4
    //   536: aload 7
    //   538: astore 5
    //   540: goto +43 -> 583
    //   543: astore 5
    //   545: aconst_null
    //   546: astore_1
    //   547: aload 5
    //   549: checkcast 202	java/lang/Throwable
    //   552: invokestatic 208	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   555: aload 4
    //   557: checkcast 210	android/database/Cursor
    //   560: invokestatic 602	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   563: aload_1
    //   564: checkcast 210	android/database/Cursor
    //   567: invokestatic 602	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   570: return
    //   571: astore 6
    //   573: aload 4
    //   575: astore 5
    //   577: aload_1
    //   578: astore 4
    //   580: aload 6
    //   582: astore_1
    //   583: aload 5
    //   585: checkcast 210	android/database/Cursor
    //   588: invokestatic 602	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   591: aload 4
    //   593: checkcast 210	android/database/Cursor
    //   596: invokestatic 602	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   599: aload_1
    //   600: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	601	0	this	am
    //   0	601	1	parama	ai.a
    //   260	157	2	m	int
    //   262	103	3	n	int
    //   70	522	4	localObject1	Object
    //   96	443	5	localObject2	Object
    //   543	5	5	localRuntimeException1	RuntimeException
    //   575	9	5	localObject3	Object
    //   35	155	6	localObject4	Object
    //   505	6	6	localObject5	Object
    //   516	10	6	localRuntimeException2	RuntimeException
    //   571	10	6	localObject6	Object
    //   73	464	7	locala	ai.a
    //   67	50	8	localObject7	Object
    //   14	370	9	localContentResolver	ContentResolver
    //   32	357	10	localArrayList1	ArrayList
    //   23	320	11	localArrayList2	ArrayList
    // Exception table:
    //   from	to	target	type
    //   383	394	397	android/content/OperationApplicationException
    //   383	394	408	android/os/RemoteException
    //   337	355	473	android/content/OperationApplicationException
    //   355	360	473	android/content/OperationApplicationException
    //   368	375	473	android/content/OperationApplicationException
    //   337	355	489	android/os/RemoteException
    //   355	360	489	android/os/RemoteException
    //   368	375	489	android/os/RemoteException
    //   120	132	505	finally
    //   161	181	505	finally
    //   120	132	516	java/lang/RuntimeException
    //   161	181	516	java/lang/RuntimeException
    //   77	98	532	finally
    //   77	98	543	java/lang/RuntimeException
    //   547	555	571	finally
  }
  
  public final void a(boolean paramBoolean)
  {
    if (a != -1L)
    {
      a = System.currentTimeMillis();
      return;
    }
    ai.a locala = new ai.a((byte)0);
    a = a;
    if (paramBoolean)
    {
      a(locala);
      return;
    }
    g.a(locala);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.am
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */