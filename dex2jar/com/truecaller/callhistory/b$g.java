package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import java.util.Collection;

final class b$g
  extends u<a, Boolean>
{
  private final int b;
  private final Collection<Long> c;
  
  private b$g(e parame, int paramInt, Collection<Long> paramCollection)
  {
    super(parame);
    b = paramInt;
    c = paramCollection;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".deleteSearchHistoryForIds(");
    localStringBuilder.append(a(Integer.valueOf(b), 2));
    localStringBuilder.append(",");
    localStringBuilder.append(a(c, 2));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */