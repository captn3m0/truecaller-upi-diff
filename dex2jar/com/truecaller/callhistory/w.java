package com.truecaller.callhistory;

import android.content.ContentResolver;
import android.content.ContentValues;
import c.g.b.k;
import com.truecaller.content.TruecallerContract.c;
import com.truecaller.data.entity.CallRecording;
import javax.inject.Inject;

public final class w
  implements v
{
  private final ContentResolver a;
  
  @Inject
  public w(ContentResolver paramContentResolver)
  {
    a = paramContentResolver;
  }
  
  public final void a(CallRecording paramCallRecording)
  {
    k.b(paramCallRecording, "callRecording");
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("recording_path", c);
    localContentValues.put("history_event_id", b);
    a.insert(TruecallerContract.c.a(), localContentValues);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.w
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */