package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.HistoryEvent;

final class b$m
  extends u<a, HistoryEvent>
{
  private final String b;
  
  private b$m(e parame, String paramString)
  {
    super(parame);
    b = paramString;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".getLastMappedCallByNumber(");
    localStringBuilder.append(a(b, 1));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */