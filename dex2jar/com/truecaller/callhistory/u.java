package com.truecaller.callhistory;

import android.content.ContentResolver;
import com.truecaller.util.aq;
import dagger.a.d;
import javax.inject.Provider;

public final class u
  implements d<t>
{
  private final Provider<ContentResolver> a;
  private final Provider<aq> b;
  
  private u(Provider<ContentResolver> paramProvider, Provider<aq> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static u a(Provider<ContentResolver> paramProvider, Provider<aq> paramProvider1)
  {
    return new u(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.u
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */