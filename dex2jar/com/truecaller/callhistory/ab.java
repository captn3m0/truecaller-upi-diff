package com.truecaller.callhistory;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.SystemClock;
import android.provider.CallLog.Calls;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import com.truecaller.multisim.b.c;
import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.c.a.a.a.a;

@TargetApi(22)
class ab
  extends k
{
  private static final long b = TimeUnit.MINUTES.toMillis(2L);
  private String[] c;
  private final Object d;
  private final ContentResolver e;
  private final TelephonyManager f;
  private final SubscriptionManager g;
  private List<SubscriptionInfo> h;
  private volatile long i;
  private final Method j;
  
  ab(Context paramContext)
  {
    Object localObject = null;
    c = null;
    d = new Object();
    i = 0L;
    f = ((TelephonyManager)paramContext.getSystemService("phone"));
    g = SubscriptionManager.from(paramContext);
    try
    {
      Method localMethod = f.getClass().getDeclaredMethod("getCallState", new Class[] { Integer.TYPE });
      localObject = localMethod;
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
      for (;;) {}
    }
    c.a();
    e = paramContext.getContentResolver();
    j = ((Method)localObject);
  }
  
  private List<SubscriptionInfo> c()
  {
    long l1 = SystemClock.elapsedRealtime();
    long l2 = i;
    List localList = h;
    if (l1 - l2 < b) {
      return localList;
    }
    try
    {
      l2 = i;
      localList = h;
      if (l1 - l2 < b) {
        return localList;
      }
      localList = g.getActiveSubscriptionInfoList();
      h = localList;
      i = SystemClock.elapsedRealtime();
      return localList;
    }
    finally {}
  }
  
  private boolean d()
  {
    try
    {
      Cursor localCursor = e.query(CallLog.Calls.CONTENT_URI, new String[] { "subscription_component_name" }, null, null, "_id ASC LIMIT 1");
      if (localCursor == null) {
        break label39;
      }
      localCursor.close();
    }
    catch (SecurityException|IOException localSecurityException)
    {
      for (;;) {}
    }
    catch (Exception localException)
    {
      label39:
      for (;;) {}
    }
    return true;
    return false;
  }
  
  public int a(int paramInt)
  {
    if (j == null) {
      return -1;
    }
    Object localObject = c();
    if (localObject == null) {
      return -1;
    }
    localObject = ((List)localObject).iterator();
    while (((Iterator)localObject).hasNext())
    {
      SubscriptionInfo localSubscriptionInfo = (SubscriptionInfo)((Iterator)localObject).next();
      try
      {
        if (((Integer)j.invoke(f, new Object[] { Integer.valueOf(localSubscriptionInfo.getSubscriptionId()) })).intValue() == paramInt)
        {
          int k = localSubscriptionInfo.getSimSlotIndex();
          return k;
        }
      }
      catch (IllegalAccessException|InvocationTargetException localIllegalAccessException)
      {
        for (;;) {}
      }
    }
    return -1;
  }
  
  public String[] b()
  {
    if (c == null) {
      synchronized (d)
      {
        if (c == null)
        {
          String[] arrayOfString2 = (String[])a.a(a, new String[] { "normalized_number", "features" });
          String[] arrayOfString1 = arrayOfString2;
          if (!d()) {
            arrayOfString1 = (String[])a.b(arrayOfString2, "subscription_component_name");
          }
          c = arrayOfString1;
        }
      }
    }
    return c;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.ab
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */