package com.truecaller.callhistory;

public enum FilterType
{
  static
  {
    FilterType localFilterType1 = new FilterType("NONE", 0);
    NONE = localFilterType1;
    FilterType localFilterType2 = new FilterType("INCOMING", 1);
    INCOMING = localFilterType2;
    FilterType localFilterType3 = new FilterType("OUTGOING", 2);
    OUTGOING = localFilterType3;
    FilterType localFilterType4 = new FilterType("MISSED", 3);
    MISSED = localFilterType4;
    FilterType localFilterType5 = new FilterType("BLOCKED", 4);
    BLOCKED = localFilterType5;
    FilterType localFilterType6 = new FilterType("FLASH", 5);
    FLASH = localFilterType6;
    $VALUES = new FilterType[] { localFilterType1, localFilterType2, localFilterType3, localFilterType4, localFilterType5, localFilterType6 };
  }
  
  private FilterType() {}
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.FilterType
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */