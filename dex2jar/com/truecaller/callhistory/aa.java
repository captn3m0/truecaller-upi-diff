package com.truecaller.callhistory;

import android.content.ContentUris;
import android.database.Cursor;
import android.database.CursorWrapper;
import com.google.c.a.k.d;
import com.truecaller.common.h.ab;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.data.access.d;
import com.truecaller.data.access.e;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.HistoryEvent.a;
import com.truecaller.data.entity.Number;

public final class aa
  extends CursorWrapper
  implements z
{
  private final int a;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  private final int f;
  private final int g;
  private final int h;
  private final int i;
  private final int j;
  private final int k;
  private final int l;
  private final int m;
  private final int n;
  private final int o;
  private final int p;
  private final int q;
  private final int r;
  private final int s;
  private final e t;
  private final d u;
  private final boolean v;
  
  public aa(Cursor paramCursor)
  {
    this(paramCursor, null, null);
  }
  
  public aa(Cursor paramCursor, e parame, d paramd)
  {
    this(paramCursor, parame, paramd, false);
  }
  
  public aa(Cursor paramCursor, e parame, d paramd, boolean paramBoolean)
  {
    super(paramCursor);
    t = parame;
    v = paramBoolean;
    u = paramd;
    a = paramCursor.getColumnIndexOrThrow("_id");
    b = paramCursor.getColumnIndexOrThrow("tc_id");
    c = paramCursor.getColumnIndexOrThrow("normalized_number");
    d = paramCursor.getColumnIndexOrThrow("raw_number");
    e = paramCursor.getColumnIndexOrThrow("number_type");
    f = paramCursor.getColumnIndexOrThrow("country_code");
    g = paramCursor.getColumnIndexOrThrow("cached_name");
    h = paramCursor.getColumnIndexOrThrow("type");
    i = paramCursor.getColumnIndexOrThrow("action");
    j = paramCursor.getColumnIndexOrThrow("call_log_id");
    k = paramCursor.getColumnIndexOrThrow("timestamp");
    l = paramCursor.getColumnIndexOrThrow("duration");
    m = paramCursor.getColumnIndexOrThrow("subscription_id");
    n = paramCursor.getColumnIndexOrThrow("feature");
    o = paramCursor.getColumnIndexOrThrow("new");
    p = paramCursor.getColumnIndexOrThrow("is_read");
    q = paramCursor.getColumnIndexOrThrow("subscription_component_name");
    r = paramCursor.getColumnIndexOrThrow("tc_flag");
    s = paramCursor.getColumnIndexOrThrow("event_id");
  }
  
  private int a(int paramInt)
  {
    if (isNull(paramInt)) {
      return 0;
    }
    return getInt(paramInt);
  }
  
  private long a(int paramInt, long paramLong)
  {
    if (isNull(paramInt)) {
      return paramLong;
    }
    return getLong(paramInt);
  }
  
  public final long a()
  {
    return a(a, -1L);
  }
  
  public final long b()
  {
    return a(j, -1L);
  }
  
  public final long c()
  {
    return getLong(k);
  }
  
  public final HistoryEvent d()
  {
    boolean bool = isNull(a);
    Object localObject2 = null;
    if (!bool)
    {
      if (isNull(h)) {
        return null;
      }
      HistoryEvent.a locala = new HistoryEvent.a();
      long l1 = getLong(a);
      String str4 = getString(b);
      a.setId(Long.valueOf(l1));
      a.setTcId(str4);
      locala.g(getString(s));
      String str1 = getString(c);
      String str2 = getString(d);
      String str3 = getString(f);
      String str5 = getString(g);
      Object localObject4 = ab.a(getString(e), k.d.l);
      locala.a(str1);
      locala.b(str2);
      locala.a((k.d)localObject4);
      locala.c(str3);
      locala.d(str5);
      locala.a(getInt(h));
      locala.b(a(i));
      locala.a(Long.valueOf(a(j, -1L)));
      long l2 = getLong(k);
      locala.a(l2);
      locala.b(a(l, 0L));
      Object localObject1 = getString(m);
      if (!org.c.a.a.a.k.b((CharSequence)localObject1)) {
        locala.e((String)localObject1);
      }
      locala.c(a(n));
      locala.d(a(o));
      locala.e(a(p));
      locala.f(getString(q));
      int i1 = a(r);
      a.r = i1;
      localObject1 = t;
      if (localObject1 != null)
      {
        localObject3 = ((e)localObject1).a(this);
        if (localObject3 == null)
        {
          localObject1 = new Contact();
          ((Contact)localObject1).l(str5);
          ((Contact)localObject1).setTcId(str4);
          c = ContentUris.withAppendedId(TruecallerContract.n.a(), l1);
          ((Contact)localObject1).a(l2);
        }
        else
        {
          localObject1 = localObject3;
          if (v)
          {
            t.a(this, (Contact)localObject3);
            localObject1 = localObject3;
          }
        }
        if (!((Contact)localObject1).O())
        {
          localObject3 = Number.a(str1, str2, str3);
          if (localObject3 != null)
          {
            ((Number)localObject3).setTcId(((Contact)localObject1).getTcId());
            ((Number)localObject3).a((k.d)localObject4);
            if (!((Contact)localObject1).O()) {
              ((Contact)localObject1).g(((Number)localObject3).a());
            }
            ((Contact)localObject1).a((Number)localObject3);
          }
          d = true;
        }
        a.f = ((Contact)localObject1);
      }
      Object localObject3 = u;
      if (localObject3 != null)
      {
        c.g.b.k.b(this, "cursor");
        localObject1 = localObject2;
        if (a != -1)
        {
          localObject1 = localObject2;
          if (b != -1)
          {
            localObject4 = getString(a);
            str1 = getString(b);
            if (d != -1) {
              l1 = getLong(d);
            } else if (c != -1) {
              l1 = getLong(c);
            } else {
              l1 = -1L;
            }
            localObject1 = localObject2;
            if (str1 != null) {
              localObject1 = new CallRecording(l1, (String)localObject4, str1);
            }
          }
        }
        if (localObject1 != null) {
          a.m = ((CallRecording)localObject1);
        }
      }
      return a;
    }
    return null;
  }
  
  public final String e()
  {
    return (String)org.c.a.a.a.k.e(getString(m), "-1");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.aa
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */