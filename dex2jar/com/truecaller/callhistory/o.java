package com.truecaller.callhistory;

import android.content.Context;
import com.truecaller.calling.e.e;
import com.truecaller.common.g.a;
import com.truecaller.i.c;
import javax.inject.Provider;

public final class o
  implements dagger.a.d<m>
{
  private final Provider<Context> a;
  private final Provider<e> b;
  private final Provider<a> c;
  private final Provider<k> d;
  private final Provider<c> e;
  private final Provider<com.truecaller.voip.d> f;
  
  private o(Provider<Context> paramProvider, Provider<e> paramProvider1, Provider<a> paramProvider2, Provider<k> paramProvider3, Provider<c> paramProvider4, Provider<com.truecaller.voip.d> paramProvider5)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
  }
  
  public static o a(Provider<Context> paramProvider, Provider<e> paramProvider1, Provider<a> paramProvider2, Provider<k> paramProvider3, Provider<c> paramProvider4, Provider<com.truecaller.voip.d> paramProvider5)
  {
    return new o(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */