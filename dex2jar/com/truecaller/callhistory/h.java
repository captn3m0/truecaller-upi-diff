package com.truecaller.callhistory;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d<f<a>>
{
  private final g a;
  private final Provider<a> b;
  private final Provider<i> c;
  
  private h(g paramg, Provider<a> paramProvider, Provider<i> paramProvider1)
  {
    a = paramg;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static h a(g paramg, Provider<a> paramProvider, Provider<i> paramProvider1)
  {
    return new h(paramg, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */