package com.truecaller.callhistory;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import android.provider.CallLog.Calls;
import c.u;
import com.google.android.gms.tasks.RuntimeExecutionException;
import com.truecaller.androidactors.w;
import com.truecaller.calling.dialer.HistoryEventsScope;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.log.AssertionUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;

public final class c
  implements a
{
  private final l a;
  private final af b;
  private final al c;
  private final com.truecaller.data.access.m d;
  private final ap e;
  private final v f;
  private final ContentResolver g;
  
  @Inject
  public c(l paraml, af paramaf, al paramal, com.truecaller.data.access.m paramm, ap paramap, v paramv, ContentResolver paramContentResolver)
  {
    a = paraml;
    b = paramaf;
    c = paramal;
    d = paramm;
    e = paramap;
    f = paramv;
    g = paramContentResolver;
  }
  
  private final void a(Uri paramUri, String paramString1, List<Long> paramList, String paramString2)
  {
    if (paramList != null)
    {
      localObject = c.a.m.e((Iterable)paramList, 10000);
      paramList = (List<Long>)localObject;
      if (localObject != null) {}
    }
    else
    {
      paramList = c.a.m.a(null);
    }
    Object localObject = ((Iterable)paramList).iterator();
    while (((Iterator)localObject).hasNext())
    {
      paramList = (List)((Iterator)localObject).next();
      if (paramList != null)
      {
        paramList = c.a.m.a((Iterable)paramList, null, (CharSequence)"(", (CharSequence)")", 0, null, null, 57);
        if (paramList != null)
        {
          localStringBuilder = new StringBuilder();
          localStringBuilder.append(paramString1);
          localStringBuilder.append(" IN ");
          localStringBuilder.append(paramList);
          paramList = localStringBuilder.toString();
          break label135;
        }
      }
      paramList = null;
      label135:
      paramList = c.a.m.e(new String[] { paramString2, paramList });
      if (!(((Collection)paramList).isEmpty() ^ true)) {
        paramList = null;
      }
      if (paramList != null) {
        paramList = c.a.m.a((Iterable)paramList, (CharSequence)" AND ", null, null, 0, null, null, 62);
      } else {
        paramList = null;
      }
      long l1 = System.currentTimeMillis();
      int i = g.delete(paramUri, paramList, null);
      long l2 = System.currentTimeMillis();
      StringBuilder localStringBuilder = new StringBuilder("delete from ");
      localStringBuilder.append(paramUri);
      localStringBuilder.append(' ');
      localStringBuilder.append(i);
      localStringBuilder.append(" items, took: ");
      localStringBuilder.append(l2 - l1);
      localStringBuilder.append("ms\ndeleteWhere = ");
      localStringBuilder.append(paramList);
      localStringBuilder.toString();
    }
  }
  
  public final w<Boolean> a(int paramInt, Collection<Long> paramCollection)
  {
    c.g.b.k.b(paramCollection, "ids");
    return b.a(paramInt, paramCollection);
  }
  
  public final w<z> a(long paramLong)
  {
    return a.a(paramLong);
  }
  
  public final w<List<HistoryEvent>> a(FilterType paramFilterType, Integer paramInteger)
  {
    c.g.b.k.b(paramFilterType, "callTypeFilter");
    return a.a(paramFilterType, paramInteger);
  }
  
  public final w<z> a(Contact paramContact)
  {
    c.g.b.k.b(paramContact, "contact");
    return a.a(paramContact);
  }
  
  public final w<Boolean> a(HistoryEvent paramHistoryEvent, Contact paramContact)
  {
    c.g.b.k.b(paramHistoryEvent, "event");
    c.g.b.k.b(paramContact, "contact");
    d.a(paramContact);
    paramHistoryEvent.setTcId(paramContact.getTcId());
    a(paramHistoryEvent);
    paramHistoryEvent = w.b(Boolean.TRUE);
    c.g.b.k.a(paramHistoryEvent, "Promise.wrap(true)");
    return paramHistoryEvent;
  }
  
  public final w<z> a(String paramString)
  {
    c.g.b.k.b(paramString, "normalizedNumber");
    return a.a(paramString);
  }
  
  public final w<Integer> a(List<? extends HistoryEvent> paramList)
  {
    c.g.b.k.b(paramList, "eventsToRestore");
    return a.a(paramList);
  }
  
  public final w<Boolean> a(List<Long> paramList1, List<Long> paramList2, HistoryEventsScope paramHistoryEventsScope)
  {
    c.g.b.k.b(paramHistoryEventsScope, "scope");
    String str;
    switch (d.a[paramHistoryEventsScope.ordinal()])
    {
    default: 
      throw new c.l();
    case 3: 
      str = "type IN (1,2,3) ";
      break;
    case 2: 
      str = "type IN (1,2,3) \n     AND tc_flag != 3\n     ";
      break;
    case 1: 
      str = "tc_flag = 3";
    }
    try
    {
      if (paramHistoryEventsScope != HistoryEventsScope.ONLY_FLASH_EVENTS)
      {
        paramHistoryEventsScope = CallLog.Calls.CONTENT_URI;
        c.g.b.k.a(paramHistoryEventsScope, "Calls.CONTENT_URI");
        a(paramHistoryEventsScope, "_id", paramList2, null);
      }
      paramList2 = TruecallerContract.n.a();
      c.g.b.k.a(paramList2, "HistoryTable.getContentUri()");
      a(paramList2, "_id", paramList1, str);
      paramList1 = w.b(Boolean.TRUE);
      c.g.b.k.a(paramList1, "Promise.wrap(true)");
      return paramList1;
    }
    catch (Exception paramList1)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramList1);
      paramList1 = w.b(Boolean.FALSE);
      c.g.b.k.a(paramList1, "Promise.wrap(false)");
    }
    return paramList1;
  }
  
  public final w<Boolean> a(Set<Long> paramSet)
  {
    c.g.b.k.b(paramSet, "callLogIds");
    paramSet = w.b(Boolean.valueOf(a.a(paramSet)));
    c.g.b.k.a(paramSet, "Promise.wrap(callLogMana…r.markAsSeen(callLogIds))");
    return paramSet;
  }
  
  public final void a()
  {
    c.a(true);
  }
  
  public final void a(int paramInt)
  {
    b.a(paramInt);
  }
  
  public final void a(ai.a parama)
  {
    c.g.b.k.b(parama, "batch");
    c.a(parama);
  }
  
  public final void a(CallRecording paramCallRecording)
  {
    c.g.b.k.b(paramCallRecording, "callRecording");
    f.a(paramCallRecording);
  }
  
  public final void a(HistoryEvent paramHistoryEvent)
  {
    c.g.b.k.b(paramHistoryEvent, "event");
    if (b.a(paramHistoryEvent))
    {
      b.b(paramHistoryEvent);
      return;
    }
    if (a.a(paramHistoryEvent)) {
      c.a(false);
    }
  }
  
  public final w<z> b()
  {
    return e.a();
  }
  
  public final w<z> b(int paramInt)
  {
    return b.b(paramInt);
  }
  
  public final w<Boolean> b(Set<Long> paramSet)
  {
    c.g.b.k.b(paramSet, "historyIds");
    try
    {
      ContentValues localContentValues = new ContentValues();
      boolean bool = false;
      localContentValues.put("new", Integer.valueOf(0));
      localContentValues.put("is_read", Integer.valueOf(1));
      ContentResolver localContentResolver = g;
      Uri localUri = TruecallerContract.n.a();
      Object localObject1 = new StringBuilder("_id IN (");
      ((StringBuilder)localObject1).append(org.c.a.a.a.k.a("?", ",", paramSet.size()));
      ((StringBuilder)localObject1).append(')');
      localObject1 = ((StringBuilder)localObject1).toString();
      Object localObject2 = (Iterable)paramSet;
      paramSet = (Collection)new ArrayList(c.a.m.a((Iterable)localObject2, 10));
      localObject2 = ((Iterable)localObject2).iterator();
      while (((Iterator)localObject2).hasNext()) {
        paramSet.add(String.valueOf(((Number)((Iterator)localObject2).next()).longValue()));
      }
      paramSet = ((Collection)paramSet).toArray(new String[0]);
      if (paramSet != null)
      {
        if (localContentResolver.update(localUri, localContentValues, (String)localObject1, (String[])paramSet) != 0) {
          bool = true;
        }
        paramSet = w.b(Boolean.valueOf(bool));
        c.g.b.k.a(paramSet, "Promise.wrap(count != 0)");
        return paramSet;
      }
      throw new u("null cannot be cast to non-null type kotlin.Array<T>");
    }
    catch (IllegalArgumentException paramSet)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramSet);
      paramSet = w.b(Boolean.FALSE);
      c.g.b.k.a(paramSet, "Promise.wrap(false)");
      return paramSet;
    }
    catch (SecurityException paramSet)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramSet);
      paramSet = w.b(Boolean.FALSE);
      c.g.b.k.a(paramSet, "Promise.wrap(false)");
      return paramSet;
    }
    catch (RuntimeExecutionException paramSet)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramSet);
      paramSet = w.b(Boolean.FALSE);
      c.g.b.k.a(paramSet, "Promise.wrap(false)");
    }
    return paramSet;
  }
  
  public final void b(long paramLong)
  {
    a.b(paramLong);
  }
  
  /* Error */
  public final void b(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc -33
    //   3: invokestatic 30	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_0
    //   7: getfield 59	com/truecaller/callhistory/c:g	Landroid/content/ContentResolver;
    //   10: invokestatic 278	com/truecaller/content/TruecallerContract$n:a	()Landroid/net/Uri;
    //   13: iconst_2
    //   14: anewarray 112	java/lang/String
    //   17: dup
    //   18: iconst_0
    //   19: ldc_w 271
    //   22: aastore
    //   23: dup
    //   24: iconst_1
    //   25: ldc_w 426
    //   28: aastore
    //   29: ldc_w 428
    //   32: iconst_1
    //   33: anewarray 112	java/lang/String
    //   36: dup
    //   37: iconst_0
    //   38: aload_1
    //   39: aastore
    //   40: aconst_null
    //   41: invokevirtual 432	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   44: astore_1
    //   45: aload_1
    //   46: ifnull +226 -> 272
    //   49: aload_1
    //   50: checkcast 434	java/io/Closeable
    //   53: astore 7
    //   55: aconst_null
    //   56: astore 6
    //   58: aload 6
    //   60: astore_1
    //   61: aload 7
    //   63: checkcast 436	android/database/Cursor
    //   66: astore 8
    //   68: aload 6
    //   70: astore_1
    //   71: new 438	java/util/LinkedHashSet
    //   74: dup
    //   75: invokespecial 439	java/util/LinkedHashSet:<init>	()V
    //   78: checkcast 375	java/util/Set
    //   81: astore 9
    //   83: aload 6
    //   85: astore_1
    //   86: new 438	java/util/LinkedHashSet
    //   89: dup
    //   90: invokespecial 439	java/util/LinkedHashSet:<init>	()V
    //   93: checkcast 375	java/util/Set
    //   96: astore 10
    //   98: aload 6
    //   100: astore_1
    //   101: aload 8
    //   103: invokeinterface 442 1 0
    //   108: ifeq +71 -> 179
    //   111: aload 6
    //   113: astore_1
    //   114: aload 8
    //   116: ldc_w 426
    //   119: invokestatic 447	com/truecaller/utils/extensions/k:c	(Landroid/database/Cursor;Ljava/lang/String;)J
    //   122: lstore_2
    //   123: aload 6
    //   125: astore_1
    //   126: aload 8
    //   128: ldc_w 271
    //   131: invokestatic 447	com/truecaller/utils/extensions/k:c	(Landroid/database/Cursor;Ljava/lang/String;)J
    //   134: lstore 4
    //   136: lload_2
    //   137: lconst_0
    //   138: lcmp
    //   139: ifle +21 -> 160
    //   142: aload 6
    //   144: astore_1
    //   145: aload 9
    //   147: lload_2
    //   148: invokestatic 452	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   151: invokeinterface 453 2 0
    //   156: pop
    //   157: goto -59 -> 98
    //   160: aload 6
    //   162: astore_1
    //   163: aload 10
    //   165: lload 4
    //   167: invokestatic 452	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   170: invokeinterface 453 2 0
    //   175: pop
    //   176: goto -78 -> 98
    //   179: aload 6
    //   181: astore_1
    //   182: aload 9
    //   184: checkcast 117	java/util/Collection
    //   187: invokeinterface 120 1 0
    //   192: iconst_1
    //   193: ixor
    //   194: ifeq +13 -> 207
    //   197: aload 6
    //   199: astore_1
    //   200: aload_0
    //   201: aload 9
    //   203: invokevirtual 455	com/truecaller/callhistory/c:a	(Ljava/util/Set;)Lcom/truecaller/androidactors/w;
    //   206: pop
    //   207: aload 6
    //   209: astore_1
    //   210: iconst_1
    //   211: aload 10
    //   213: checkcast 117	java/util/Collection
    //   216: invokeinterface 120 1 0
    //   221: ixor
    //   222: ifeq +13 -> 235
    //   225: aload 6
    //   227: astore_1
    //   228: aload_0
    //   229: aload 10
    //   231: invokevirtual 457	com/truecaller/callhistory/c:b	(Ljava/util/Set;)Lcom/truecaller/androidactors/w;
    //   234: pop
    //   235: aload 6
    //   237: astore_1
    //   238: getstatic 462	c/x:a	Lc/x;
    //   241: astore 6
    //   243: aload 7
    //   245: aconst_null
    //   246: invokestatic 467	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   249: return
    //   250: astore 6
    //   252: goto +11 -> 263
    //   255: astore 6
    //   257: aload 6
    //   259: astore_1
    //   260: aload 6
    //   262: athrow
    //   263: aload 7
    //   265: aload_1
    //   266: invokestatic 467	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   269: aload 6
    //   271: athrow
    //   272: return
    //   273: astore_1
    //   274: aload_1
    //   275: checkcast 282	java/lang/Throwable
    //   278: invokestatic 288	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   281: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	282	0	this	c
    //   0	282	1	paramString	String
    //   122	26	2	l1	long
    //   134	32	4	l2	long
    //   56	186	6	localx	c.x
    //   250	1	6	localObject	Object
    //   255	15	6	localThrowable	Throwable
    //   53	211	7	localCloseable	java.io.Closeable
    //   66	61	8	localCursor	android.database.Cursor
    //   81	121	9	localSet1	Set
    //   96	134	10	localSet2	Set
    // Exception table:
    //   from	to	target	type
    //   61	68	250	finally
    //   71	83	250	finally
    //   86	98	250	finally
    //   101	111	250	finally
    //   114	123	250	finally
    //   126	136	250	finally
    //   145	157	250	finally
    //   163	176	250	finally
    //   182	197	250	finally
    //   200	207	250	finally
    //   210	225	250	finally
    //   228	235	250	finally
    //   238	243	250	finally
    //   260	263	250	finally
    //   61	68	255	java/lang/Throwable
    //   71	83	255	java/lang/Throwable
    //   86	98	255	java/lang/Throwable
    //   101	111	255	java/lang/Throwable
    //   114	123	255	java/lang/Throwable
    //   126	136	255	java/lang/Throwable
    //   145	157	255	java/lang/Throwable
    //   163	176	255	java/lang/Throwable
    //   182	197	255	java/lang/Throwable
    //   200	207	255	java/lang/Throwable
    //   210	225	255	java/lang/Throwable
    //   228	235	255	java/lang/Throwable
    //   238	243	255	java/lang/Throwable
    //   6	45	273	android/database/sqlite/SQLiteException
    //   49	55	273	android/database/sqlite/SQLiteException
    //   243	249	273	android/database/sqlite/SQLiteException
    //   263	272	273	android/database/sqlite/SQLiteException
  }
  
  public final w<z> c()
  {
    return a.a();
  }
  
  public final w<z> c(int paramInt)
  {
    return a.a(paramInt);
  }
  
  public final w<HistoryEvent> c(String paramString)
  {
    c.g.b.k.b(paramString, "normalizedNumber");
    return a.b(paramString);
  }
  
  public final void c(long paramLong)
  {
    a.c(paramLong);
  }
  
  public final w<z> d()
  {
    return a(Long.MAX_VALUE);
  }
  
  public final w<HistoryEvent> d(String paramString)
  {
    c.g.b.k.b(paramString, "tcId");
    return a.c(paramString);
  }
  
  public final w<Integer> e()
  {
    return a.b();
  }
  
  public final void f()
  {
    a.c();
  }
  
  public final void g()
  {
    a.d();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */