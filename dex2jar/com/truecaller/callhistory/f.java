package com.truecaller.callhistory;

import android.content.ContentResolver;
import com.truecaller.data.access.m;
import dagger.a.d;
import javax.inject.Provider;

public final class f
  implements d<c>
{
  private final Provider<l> a;
  private final Provider<af> b;
  private final Provider<al> c;
  private final Provider<m> d;
  private final Provider<ap> e;
  private final Provider<v> f;
  private final Provider<ContentResolver> g;
  
  private f(Provider<l> paramProvider, Provider<af> paramProvider1, Provider<al> paramProvider2, Provider<m> paramProvider3, Provider<ap> paramProvider4, Provider<v> paramProvider5, Provider<ContentResolver> paramProvider6)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
  }
  
  public static f a(Provider<l> paramProvider, Provider<af> paramProvider1, Provider<al> paramProvider2, Provider<m> paramProvider3, Provider<ap> paramProvider4, Provider<v> paramProvider5, Provider<ContentResolver> paramProvider6)
  {
    return new f(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */