package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class b$e
  extends u<a, Void>
{
  private final int b;
  
  private b$e(e parame, int paramInt)
  {
    super(parame);
    b = paramInt;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".clearSearchHistory(");
    localStringBuilder.append(a(Integer.valueOf(b), 2));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */