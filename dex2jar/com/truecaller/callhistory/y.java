package com.truecaller.callhistory;

import android.os.Build.VERSION;
import org.c.a.a.a.a;

final class y
  extends k
{
  private static final String[] b;
  
  static
  {
    String[] arrayOfString2 = a;
    String[] arrayOfString1 = arrayOfString2;
    if (Build.VERSION.SDK_INT >= 21) {
      arrayOfString1 = (String[])a.a(arrayOfString2, new String[] { "normalized_number", "features", "subscription_component_name" });
    }
    b = arrayOfString1;
  }
  
  public final int a(int paramInt)
  {
    return -1;
  }
  
  public final String[] b()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.y
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */