package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.HistoryEvent;

final class b$c
  extends u<a, Void>
{
  private final HistoryEvent b;
  
  private b$c(e parame, HistoryEvent paramHistoryEvent)
  {
    super(parame);
    b = paramHistoryEvent;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(".add(");
    localStringBuilder.append(a(b, 1));
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */