package com.truecaller.callhistory;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class ar
  implements d<aq>
{
  private final Provider<Context> a;
  
  private ar(Provider<Context> paramProvider)
  {
    a = paramProvider;
  }
  
  public static ar a(Provider<Context> paramProvider)
  {
    return new ar(paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.ar
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */