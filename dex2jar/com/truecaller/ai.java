package com.truecaller;

import com.truecaller.androidactors.k;
import com.truecaller.common.g.a;
import com.truecaller.featuretoggles.e;
import com.truecaller.filters.p;
import javax.inject.Provider;

public final class ai
  implements dagger.a.d<com.truecaller.common.f.c>
{
  private final c a;
  private final Provider<com.truecaller.androidactors.f<com.truecaller.premium.data.g>> b;
  private final Provider<com.truecaller.androidactors.f<com.truecaller.network.util.f>> c;
  private final Provider<k> d;
  private final Provider<a> e;
  private final Provider<p> f;
  private final Provider<com.truecaller.engagementrewards.c> g;
  private final Provider<com.truecaller.engagementrewards.ui.d> h;
  private final Provider<com.truecaller.engagementrewards.g> i;
  private final Provider<e> j;
  
  private ai(c paramc, Provider<com.truecaller.androidactors.f<com.truecaller.premium.data.g>> paramProvider, Provider<com.truecaller.androidactors.f<com.truecaller.network.util.f>> paramProvider1, Provider<k> paramProvider2, Provider<a> paramProvider3, Provider<p> paramProvider4, Provider<com.truecaller.engagementrewards.c> paramProvider5, Provider<com.truecaller.engagementrewards.ui.d> paramProvider6, Provider<com.truecaller.engagementrewards.g> paramProvider7, Provider<e> paramProvider8)
  {
    a = paramc;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
    f = paramProvider4;
    g = paramProvider5;
    h = paramProvider6;
    i = paramProvider7;
    j = paramProvider8;
  }
  
  public static ai a(c paramc, Provider<com.truecaller.androidactors.f<com.truecaller.premium.data.g>> paramProvider, Provider<com.truecaller.androidactors.f<com.truecaller.network.util.f>> paramProvider1, Provider<k> paramProvider2, Provider<a> paramProvider3, Provider<p> paramProvider4, Provider<com.truecaller.engagementrewards.c> paramProvider5, Provider<com.truecaller.engagementrewards.ui.d> paramProvider6, Provider<com.truecaller.engagementrewards.g> paramProvider7, Provider<e> paramProvider8)
  {
    return new ai(paramc, paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ai
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */