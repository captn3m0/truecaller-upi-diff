package com.truecaller;

import android.content.Context;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class k
  implements d<com.truecaller.data.access.c>
{
  private final c a;
  private final Provider<Context> b;
  
  private k(c paramc, Provider<Context> paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static com.truecaller.data.access.c a(Context paramContext)
  {
    return (com.truecaller.data.access.c)g.a(c.d(paramContext), "Cannot return null from a non-@Nullable @Provides method");
  }
  
  public static k a(c paramc, Provider<Context> paramProvider)
  {
    return new k(paramc, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */