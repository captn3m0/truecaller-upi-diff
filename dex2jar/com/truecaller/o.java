package com.truecaller;

import com.truecaller.androidactors.f;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.h;
import com.truecaller.util.al;
import javax.inject.Provider;

public final class o
  implements dagger.a.d<com.truecaller.search.local.model.c>
{
  private final c a;
  private final Provider<com.truecaller.search.local.model.g> b;
  private final Provider<com.truecaller.common.account.r> c;
  private final Provider<al> d;
  private final Provider<h> e;
  private final Provider<f<com.truecaller.presence.c>> f;
  private final Provider<com.truecaller.presence.r> g;
  private final Provider<bw> h;
  private final Provider<com.truecaller.voip.d> i;
  
  private o(c paramc, Provider<com.truecaller.search.local.model.g> paramProvider, Provider<com.truecaller.common.account.r> paramProvider1, Provider<al> paramProvider2, Provider<h> paramProvider3, Provider<f<com.truecaller.presence.c>> paramProvider4, Provider<com.truecaller.presence.r> paramProvider5, Provider<bw> paramProvider6, Provider<com.truecaller.voip.d> paramProvider7)
  {
    a = paramc;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
    f = paramProvider4;
    g = paramProvider5;
    h = paramProvider6;
    i = paramProvider7;
  }
  
  public static o a(c paramc, Provider<com.truecaller.search.local.model.g> paramProvider, Provider<com.truecaller.common.account.r> paramProvider1, Provider<al> paramProvider2, Provider<h> paramProvider3, Provider<f<com.truecaller.presence.c>> paramProvider4, Provider<com.truecaller.presence.r> paramProvider5, Provider<bw> paramProvider6, Provider<com.truecaller.voip.d> paramProvider7)
  {
    return new o(paramc, paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7);
  }
  
  public static com.truecaller.search.local.model.c a(com.truecaller.search.local.model.g paramg, com.truecaller.common.account.r paramr, al paramal, h paramh, f<com.truecaller.presence.c> paramf, com.truecaller.presence.r paramr1, bw parambw, com.truecaller.voip.d paramd)
  {
    return (com.truecaller.search.local.model.c)dagger.a.g.a(c.a(paramg, paramr, paramal, paramh, paramf, paramr1, parambw, paramd), "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */