package com.truecaller.smsparser;

import com.truecaller.androidactors.f;
import com.truecaller.bp;
import com.truecaller.messaging.data.t;
import com.truecaller.smsparser.b.d;
import dagger.a;
import javax.inject.Inject;

public final class k
  implements d
{
  @Inject
  public a<f<t>> a;
  
  public k(bp parambp)
  {
    parambp.a(this);
  }
  
  public final void a(long paramLong)
  {
    a locala = a;
    if (locala == null) {
      c.g.b.k.a("messageStorageRef");
    }
    ((t)((f)locala.get()).a()).b(paramLong);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */