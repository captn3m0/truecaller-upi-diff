package com.truecaller.smsparser.a;

import android.content.Context;
import android.content.res.AssetManager;
import c.g.b.k;
import com.truecaller.smsparser.models.c;
import com.truecaller.smsparser.models.e;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class b
  implements a
{
  private final com.truecaller.smsparser.models.f a;
  private final Map<String, e> b;
  private final Context c;
  private final com.google.gson.f d;
  
  private b(Context paramContext, com.google.gson.f paramf)
  {
    c = paramContext;
    d = paramf;
    b = ((Map)new HashMap());
    try
    {
      paramContext = new com.truecaller.smsparser.models.f(new JSONObject(c("senderTagsFileNameSchema.json")));
    }
    catch (IOException paramContext)
    {
      for (;;) {}
    }
    paramContext = null;
    a = paramContext;
  }
  
  private final e b(String paramString)
  {
    Object localObject2 = (e)b.get(paramString);
    Object localObject1 = localObject2;
    if (localObject2 == null) {
      localObject1 = null;
    }
    try
    {
      localObject2 = c(paramString);
      localObject1 = localObject2;
    }
    catch (IOException|JSONException localIOException)
    {
      for (;;) {}
    }
    localObject1 = (e)d.a((String)localObject1, e.class);
    localObject2 = b;
    k.a(localObject1, "senderList");
    ((Map)localObject2).put(paramString, localObject1);
    return (e)localObject1;
  }
  
  private final String c(String paramString)
    throws IOException
  {
    paramString = c.getAssets().open(paramString);
    byte[] arrayOfByte = new byte[paramString.available()];
    paramString.read(arrayOfByte);
    paramString.close();
    return new String(arrayOfByte, c.n.d.a);
  }
  
  public final c a(String paramString)
  {
    k.b(paramString, "tag");
    if (a == null) {
      return null;
    }
    paramString = paramString.toLowerCase();
    k.a(paramString, "(this as java.lang.String).toLowerCase()");
    String str = (String)a.get(paramString);
    if (str == null) {
      return null;
    }
    e locale = b(str);
    if (locale == null) {
      return null;
    }
    str = b;
    if (str == null) {
      k.a("iconLink");
    }
    List localList1 = (List)new ArrayList();
    Object localObject1 = a;
    if (localObject1 == null) {
      k.a("senders");
    }
    localObject1 = ((List)localObject1).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      com.truecaller.smsparser.models.d locald = (com.truecaller.smsparser.models.d)((Iterator)localObject1).next();
      Object localObject2 = a;
      if (localObject2 == null) {
        k.a("tags");
      }
      localObject2 = ((List)localObject2).iterator();
      while (((Iterator)localObject2).hasNext()) {
        if (k.a((String)((Iterator)localObject2).next(), paramString))
        {
          List localList2 = b;
          if (localList2 == null) {
            k.a("formats");
          }
          localList1.addAll((Collection)localList2);
        }
      }
    }
    paramString = c;
    if (paramString == null) {
      k.a("senderType");
    }
    return new c(localList1, str, paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */