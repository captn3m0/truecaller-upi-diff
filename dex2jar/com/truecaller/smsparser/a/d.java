package com.truecaller.smsparser.a;

import c.g.b.k;
import c.n.m;
import com.truecaller.log.UnmutedException.d;
import com.truecaller.smsparser.models.MatchedNotificationAttributes;
import com.truecaller.smsparser.models.NotificationAttribute;
import com.truecaller.smsparser.models.a;
import com.truecaller.smsparser.models.b;
import java.text.NumberFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class d
  implements c
{
  private final NumberFormat a = NumberFormat.getNumberInstance(new Locale("en", "in"));
  
  private final String a(String paramString)
  {
    try
    {
      String str = a.format(Double.parseDouble(m.a(paramString, ",", "")));
      k.a(str, "numberFormat.format(valu…lace(\",\", \"\").toDouble())");
      return str;
    }
    catch (NumberFormatException localNumberFormatException) {}
    return paramString;
  }
  
  private static String a(Matcher paramMatcher, a parama)
  {
    String str1 = "";
    Iterator localIterator = ((Iterable)parama.a()).iterator();
    while (localIterator.hasNext())
    {
      String str2 = paramMatcher.group(((Number)localIterator.next()).intValue());
      if (str2 != null)
      {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append(str1);
        localStringBuilder.append(str2);
        str1 = localStringBuilder.toString();
      }
    }
    if (k.a(parama.b(), "NUMBER")) {
      return b(str1);
    }
    return str1;
  }
  
  private final String a(Matcher paramMatcher, a parama, b paramb)
  {
    Object localObject1 = "";
    Object localObject2 = parama.b();
    if ((((String)localObject2).hashCode() == 2090926) && (((String)localObject2).equals("DATE"))) {
      localObject2 = "-";
    } else {
      localObject2 = "";
    }
    int i = 0;
    for (;;)
    {
      try
      {
        if (i < parama.a().size() - 1)
        {
          String str = paramMatcher.group(((Number)parama.a().get(i)).intValue());
          localObject3 = localObject1;
          if (str == null) {
            continue;
          }
          localObject3 = new StringBuilder();
          ((StringBuilder)localObject3).append((String)localObject1);
          ((StringBuilder)localObject3).append(str);
          ((StringBuilder)localObject3).append((String)localObject2);
          localObject3 = ((StringBuilder)localObject3).toString();
          continue;
        }
        localObject3 = paramMatcher.group(((Number)parama.a().get(i)).intValue());
        localObject2 = localObject1;
        if (localObject3 == null) {
          continue;
        }
        localObject2 = new StringBuilder();
        ((StringBuilder)localObject2).append((String)localObject1);
        ((StringBuilder)localObject2).append((String)localObject3);
        localObject2 = ((StringBuilder)localObject2).toString();
      }
      catch (Exception localException)
      {
        Object localObject3;
        continue;
        i += 1;
        localObject1 = localObject3;
      }
      localObject2 = new StringBuilder("groupIndexSearched: ");
      ((StringBuilder)localObject2).append(((Number)parama.a().get(i)).intValue());
      ((StringBuilder)localObject2).append('\n');
      ((StringBuilder)localObject2).append("totalGroupCount: ");
      ((StringBuilder)localObject2).append(paramMatcher.groupCount());
      ((StringBuilder)localObject2).append('\n');
      ((StringBuilder)localObject2).append("Bank name: ");
      ((StringBuilder)localObject2).append(paramb.b());
      ((StringBuilder)localObject2).append('\n');
      ((StringBuilder)localObject2).append("ReminderType: ");
      ((StringBuilder)localObject2).append(paramb.a());
      ((StringBuilder)localObject2).append('\n');
      ((StringBuilder)localObject2).append("MessageType: ");
      ((StringBuilder)localObject2).append(paramb.c());
      com.truecaller.log.d.a((Throwable)new UnmutedException.d(((StringBuilder)localObject2).toString()));
      localObject2 = localObject1;
      if ((!k.a(parama.b(), "NUMBER")) && (!k.a(parama.b(), "NUMBER_COMMA"))) {
        return (String)localObject2;
      }
      return a((String)localObject2);
    }
  }
  
  private static String a(Matcher paramMatcher, b paramb)
  {
    StringBuilder localStringBuilder = new StringBuilder("truecaller://utility/postpaid");
    String str = "?recharge_number=";
    Object localObject1 = "&amount=";
    Iterator localIterator = ((Iterable)paramb.d()).iterator();
    paramb = (b)localObject1;
    while (localIterator.hasNext())
    {
      localObject1 = (a)localIterator.next();
      Object localObject2 = a;
      if (localObject2 == null) {
        k.a("name");
      }
      int i = ((String)localObject2).hashCode();
      if (i != 107058794)
      {
        if ((i == 1901669035) && (((String)localObject2).equals("MobileNumber")))
        {
          localObject2 = new StringBuilder();
          ((StringBuilder)localObject2).append(str);
          ((StringBuilder)localObject2).append(a(paramMatcher, (a)localObject1));
          str = ((StringBuilder)localObject2).toString();
        }
      }
      else if (((String)localObject2).equals("AmountPayable"))
      {
        localObject2 = new StringBuilder();
        ((StringBuilder)localObject2).append(paramb);
        ((StringBuilder)localObject2).append(a(paramMatcher, (a)localObject1));
        paramb = ((StringBuilder)localObject2).toString();
      }
    }
    localStringBuilder.append(str);
    localStringBuilder.append(paramb);
    paramMatcher = localStringBuilder.toString();
    k.a(paramMatcher, "deepLinkBuilder.append(r…append(amount).toString()");
    return paramMatcher;
  }
  
  private static String b(String paramString)
  {
    try
    {
      int i = (int)Math.ceil(Double.parseDouble(m.a(paramString, ",", "")));
      return String.valueOf(i);
    }
    catch (NumberFormatException localNumberFormatException) {}
    return paramString;
  }
  
  public final MatchedNotificationAttributes a(String paramString, com.truecaller.smsparser.models.c paramc)
  {
    k.b(paramString, "message");
    k.b(paramc, "formatList");
    Object localObject2 = ((Iterable)a).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      b localb = (b)((Iterator)localObject2).next();
      Object localObject1 = a;
      if (localObject1 == null) {
        k.a("messageFormat");
      }
      localObject1 = Pattern.compile((String)localObject1, 2);
      k.a(localObject1, "Pattern.compile(format.m…Pattern.CASE_INSENSITIVE)");
      localObject1 = ((Pattern)localObject1).matcher((CharSequence)paramString);
      k.a(localObject1, "pattern.matcher(message)");
      if (((Matcher)localObject1).find())
      {
        paramString = localb.d();
        localObject2 = ((Iterable)localb.e()).iterator();
        while (((Iterator)localObject2).hasNext())
        {
          localObject3 = (NotificationAttribute)((Iterator)localObject2).next();
          int i = c;
          if (i != -1)
          {
            localObject4 = a((Matcher)localObject1, (a)paramString.get(i), localb);
            if ((m.a((CharSequence)localObject4) ^ true))
            {
              k.b(localObject4, "<set-?>");
              d = ((String)localObject4);
            }
          }
        }
        paramc = b;
        localObject2 = localb.a();
        Object localObject3 = localb.c();
        Object localObject4 = localb.e();
        paramString = localb.a();
        if ((paramString.hashCode() == 1540463468) && (paramString.equals("POSTPAID"))) {
          paramString = a((Matcher)localObject1, localb);
        } else {
          paramString = "";
        }
        return new MatchedNotificationAttributes(paramc, (String)localObject2, (String)localObject3, (List)localObject4, paramString);
      }
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.a.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */