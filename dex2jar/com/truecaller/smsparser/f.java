package com.truecaller.smsparser;

import android.content.Context;
import com.truecaller.smsparser.a.a;
import javax.inject.Provider;

public final class f
  implements dagger.a.d<a>
{
  private final d a;
  private final Provider<Context> b;
  
  private f(d paramd, Provider<Context> paramProvider)
  {
    a = paramd;
    b = paramProvider;
  }
  
  public static f a(d paramd, Provider<Context> paramProvider)
  {
    return new f(paramd, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */