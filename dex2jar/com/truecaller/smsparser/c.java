package com.truecaller.smsparser;

import com.truecaller.analytics.ae;
import com.truecaller.androidactors.f;
import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d<b>
{
  private final Provider<com.truecaller.analytics.b> a;
  private final Provider<com.truecaller.smsparser.b.a> b;
  private final Provider<com.truecaller.smsparser.a.a> c;
  private final Provider<com.truecaller.smsparser.a.c> d;
  private final Provider<f<ae>> e;
  
  private c(Provider<com.truecaller.analytics.b> paramProvider, Provider<com.truecaller.smsparser.b.a> paramProvider1, Provider<com.truecaller.smsparser.a.a> paramProvider2, Provider<com.truecaller.smsparser.a.c> paramProvider3, Provider<f<ae>> paramProvider4)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static c a(Provider<com.truecaller.analytics.b> paramProvider, Provider<com.truecaller.smsparser.b.a> paramProvider1, Provider<com.truecaller.smsparser.a.a> paramProvider2, Provider<com.truecaller.smsparser.a.c> paramProvider3, Provider<f<ae>> paramProvider4)
  {
    return new c(paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */