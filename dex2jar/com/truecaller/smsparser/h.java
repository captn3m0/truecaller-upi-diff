package com.truecaller.smsparser;

import android.content.Context;
import c.d.f;
import c.g.b.k;
import com.d.b.w;
import com.truecaller.featuretoggles.e;
import com.truecaller.smsparser.b.a;
import com.truecaller.smsparser.b.b;
import com.truecaller.utils.i;
import dagger.a.g;
import javax.inject.Provider;

public final class h
  implements dagger.a.d<a>
{
  private final d a;
  private final Provider<Context> b;
  private final Provider<i> c;
  private final Provider<e> d;
  private final Provider<w> e;
  private final Provider<f> f;
  
  private h(d paramd, Provider<Context> paramProvider, Provider<i> paramProvider1, Provider<e> paramProvider2, Provider<w> paramProvider3, Provider<f> paramProvider4)
  {
    a = paramd;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
    f = paramProvider4;
  }
  
  public static a a(Context paramContext, i parami, e parame, w paramw, f paramf)
  {
    k.b(paramContext, "context");
    k.b(parami, "networkUtil");
    k.b(parame, "featuresRegistry");
    k.b(paramw, "picasso");
    k.b(paramf, "asyncContext");
    return (a)g.a((a)new b(paramContext, parami, parame, paramw, paramf), "Cannot return null from a non-@Nullable @Provides method");
  }
  
  public static h a(d paramd, Provider<Context> paramProvider, Provider<i> paramProvider1, Provider<e> paramProvider2, Provider<w> paramProvider3, Provider<f> paramProvider4)
  {
    return new h(paramd, paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */