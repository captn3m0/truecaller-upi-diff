package com.truecaller.smsparser;

import android.content.Context;
import c.g.b.k;
import com.d.b.w;
import javax.inject.Provider;

public final class g
  implements dagger.a.d<w>
{
  private final d a;
  private final Provider<Context> b;
  
  private g(d paramd, Provider<Context> paramProvider)
  {
    a = paramd;
    b = paramProvider;
  }
  
  public static w a(Context paramContext)
  {
    k.b(paramContext, "context");
    paramContext = w.a(paramContext);
    k.a(paramContext, "Picasso.with(context)");
    return (w)dagger.a.g.a(paramContext, "Cannot return null from a non-@Nullable @Provides method");
  }
  
  public static g a(d paramd, Provider<Context> paramProvider)
  {
    return new g(paramd, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */