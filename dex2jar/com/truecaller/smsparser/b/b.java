package com.truecaller.smsparser.b;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.support.v4.app.z.a;
import android.support.v4.app.z.c;
import android.support.v4.app.z.d;
import android.support.v4.app.z.g;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import c.u;
import com.d.b.ab;
import com.d.b.w;
import com.truecaller.log.AssertionUtil;
import com.truecaller.smsparser.R.color;
import com.truecaller.smsparser.R.drawable;
import com.truecaller.smsparser.R.string;
import com.truecaller.smsparser.models.MatchedNotificationAttributes;
import com.truecaller.smsparser.models.NotificationAttribute;
import com.truecaller.utils.extensions.d;
import com.truecaller.utils.i;
import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

public final class b
  implements a
{
  HashSet<Integer> a;
  private final Context b;
  private final i c;
  private final com.truecaller.featuretoggles.e d;
  private final w e;
  private final f f;
  
  public b(Context paramContext, i parami, com.truecaller.featuretoggles.e parame, w paramw, @Named("Parser_Async") f paramf)
  {
    b = paramContext;
    c = parami;
    d = parame;
    e = paramw;
    f = paramf;
    a = new HashSet();
  }
  
  /* Error */
  private final z.a a(c paramc, MatchedNotificationAttributes paramMatchedNotificationAttributes, boolean paramBoolean, String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 48	com/truecaller/smsparser/b/b:d	Lcom/truecaller/featuretoggles/e;
    //   4: invokevirtual 66	com/truecaller/featuretoggles/e:x	()Lcom/truecaller/featuretoggles/b;
    //   7: invokeinterface 71 1 0
    //   12: ifeq +202 -> 214
    //   15: aload_2
    //   16: getfield 76	com/truecaller/smsparser/models/MatchedNotificationAttributes:b	Ljava/lang/String;
    //   19: astore 5
    //   21: aload 5
    //   23: invokevirtual 82	java/lang/String:hashCode	()I
    //   26: lookupswitch	default:+50->76, -459336179:+113->139, 65146:+100->126, 84238:+87->113, 1280945827:+74->100, 1540463468:+53->79
    //   76: goto +131 -> 207
    //   79: aload 5
    //   81: ldc 84
    //   83: invokevirtual 88	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   86: ifeq +121 -> 207
    //   89: aload_0
    //   90: aload_1
    //   91: aload_2
    //   92: getfield 90	com/truecaller/smsparser/models/MatchedNotificationAttributes:e	Ljava/lang/String;
    //   95: iload_3
    //   96: invokespecial 93	com/truecaller/smsparser/b/b:a	(Lcom/truecaller/smsparser/b/c;Ljava/lang/String;Z)Landroid/support/v4/app/z$a;
    //   99: areturn
    //   100: aload 5
    //   102: ldc 95
    //   104: invokevirtual 88	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   107: ifeq +100 -> 207
    //   110: goto +39 -> 149
    //   113: aload 5
    //   115: ldc 97
    //   117: invokevirtual 88	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   120: ifeq +87 -> 207
    //   123: goto +26 -> 149
    //   126: aload 5
    //   128: ldc 99
    //   130: invokevirtual 88	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   133: ifeq +74 -> 207
    //   136: goto +13 -> 149
    //   139: aload 5
    //   141: ldc 101
    //   143: invokevirtual 88	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   146: ifeq +61 -> 207
    //   149: aload_1
    //   150: aload 4
    //   152: invokeinterface 106 2 0
    //   157: astore_2
    //   158: aload_2
    //   159: ifnull +38 -> 197
    //   162: aload_1
    //   163: aload_0
    //   164: getfield 44	com/truecaller/smsparser/b/b:b	Landroid/content/Context;
    //   167: aload_2
    //   168: invokeinterface 109 3 0
    //   173: astore_1
    //   174: new 111	android/support/v4/app/z$a
    //   177: dup
    //   178: iconst_0
    //   179: aload_0
    //   180: getfield 44	com/truecaller/smsparser/b/b:b	Landroid/content/Context;
    //   183: getstatic 117	com/truecaller/smsparser/R$string:CheckBalance	I
    //   186: invokevirtual 123	android/content/Context:getString	(I)Ljava/lang/String;
    //   189: checkcast 125	java/lang/CharSequence
    //   192: aload_1
    //   193: invokespecial 128	android/support/v4/app/z$a:<init>	(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V
    //   196: areturn
    //   197: aload_0
    //   198: checkcast 2	com/truecaller/smsparser/b/b
    //   201: aload_1
    //   202: iload_3
    //   203: invokespecial 131	com/truecaller/smsparser/b/b:a	(Lcom/truecaller/smsparser/b/c;Z)Landroid/support/v4/app/z$a;
    //   206: areturn
    //   207: aload_0
    //   208: aload_1
    //   209: iload_3
    //   210: invokespecial 131	com/truecaller/smsparser/b/b:a	(Lcom/truecaller/smsparser/b/c;Z)Landroid/support/v4/app/z$a;
    //   213: areturn
    //   214: aload_0
    //   215: aload_1
    //   216: iload_3
    //   217: invokespecial 131	com/truecaller/smsparser/b/b:a	(Lcom/truecaller/smsparser/b/c;Z)Landroid/support/v4/app/z$a;
    //   220: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	221	0	this	b
    //   0	221	1	paramc	c
    //   0	221	2	paramMatchedNotificationAttributes	MatchedNotificationAttributes
    //   0	221	3	paramBoolean	boolean
    //   0	221	4	paramString	String
    //   19	121	5	str	String
  }
  
  private final z.a a(c paramc, String paramString, boolean paramBoolean)
  {
    if (d.o().a()) {
      return new z.a(0, (CharSequence)b.getString(R.string.PayBill), paramc.a(b, paramString));
    }
    return a(paramc, paramBoolean);
  }
  
  private final z.a a(c paramc, boolean paramBoolean)
  {
    if (paramBoolean) {
      paramc = paramc.a(b);
    } else {
      paramc = paramc.c(b);
    }
    return new z.a(0, (CharSequence)b.getString(R.string.MarkAsRead), paramc);
  }
  
  /* Error */
  final java.util.Set<Integer> a()
  {
    // Byte code:
    //   0: new 54	java/util/HashSet
    //   3: dup
    //   4: invokespecial 55	java/util/HashSet:<init>	()V
    //   7: astore_2
    //   8: aload_0
    //   9: getfield 44	com/truecaller/smsparser/b/b:b	Landroid/content/Context;
    //   12: ldc -102
    //   14: invokevirtual 158	android/content/Context:openFileInput	(Ljava/lang/String;)Ljava/io/FileInputStream;
    //   17: astore_3
    //   18: new 160	java/io/DataInputStream
    //   21: dup
    //   22: aload_3
    //   23: checkcast 162	java/io/InputStream
    //   26: invokespecial 165	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
    //   29: astore 4
    //   31: aload 4
    //   33: invokevirtual 168	java/io/DataInputStream:readInt	()I
    //   36: istore_1
    //   37: aload_2
    //   38: iload_1
    //   39: invokestatic 174	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   42: invokevirtual 177	java/util/HashSet:add	(Ljava/lang/Object;)Z
    //   45: pop
    //   46: aload 4
    //   48: invokevirtual 168	java/io/DataInputStream:readInt	()I
    //   51: istore_1
    //   52: goto -15 -> 37
    //   55: astore 4
    //   57: aload_3
    //   58: checkcast 179	java/io/Closeable
    //   61: invokestatic 184	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   64: aload 4
    //   66: athrow
    //   67: aload_3
    //   68: checkcast 179	java/io/Closeable
    //   71: invokestatic 184	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   74: aload_2
    //   75: checkcast 186	java/util/Set
    //   78: areturn
    //   79: astore_3
    //   80: goto -6 -> 74
    //   83: astore 4
    //   85: goto -18 -> 67
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	88	0	this	b
    //   36	16	1	i	int
    //   7	68	2	localHashSet	HashSet
    //   17	51	3	localFileInputStream	java.io.FileInputStream
    //   79	1	3	localIOException	IOException
    //   29	18	4	localDataInputStream	java.io.DataInputStream
    //   55	10	4	localObject	Object
    //   83	1	4	localEOFException	java.io.EOFException
    // Exception table:
    //   from	to	target	type
    //   18	37	55	finally
    //   37	52	55	finally
    //   8	18	79	java/io/IOException
    //   57	67	79	java/io/IOException
    //   67	74	79	java/io/IOException
    //   18	37	83	java/io/EOFException
    //   37	52	83	java/io/EOFException
  }
  
  public final void a(MatchedNotificationAttributes paramMatchedNotificationAttributes, c paramc, boolean paramBoolean1, boolean paramBoolean2, String paramString, int paramInt)
  {
    k.b(paramMatchedNotificationAttributes, "matchedNotificationAttributes");
    k.b(paramc, "smartNotificationsHelper");
    k.b(paramString, "senderId");
    Object localObject1 = "";
    Object localObject2 = "";
    Object localObject4 = paramc.a();
    Object localObject3 = paramc.e(b);
    localObject4 = new z.d(b, (String)localObject4).f(android.support.v4.content.b.c(b, R.color.notification_channels_notification_light_default));
    String str1 = b;
    String str2 = c;
    switch (str1.hashCode())
    {
    default: 
      break;
    case 1878720662: 
      if (str1.equals("CREDIT_CARD")) {
        i = R.drawable.ic_credit_card;
      }
      break;
    case 1540463468: 
      if (str1.equals("POSTPAID")) {
        i = R.drawable.ic_postpaid;
      }
      break;
    case 1280945827: 
      if (str1.equals("DEBIT_CARD")) {
        i = R.drawable.ic_credit_card;
      }
      break;
    case 1167688599: 
      if (str1.equals("BROADBAND")) {
        i = R.drawable.ic_broadband;
      }
      break;
    case 399611855: 
      if (str1.equals("PREPAID")) {
        i = R.drawable.ic_prepaid;
      }
      break;
    case 84238: 
      if (str1.equals("UPI")) {
        i = R.drawable.ic_rupee_mobile;
      }
      break;
    case 65146: 
      if (str1.equals("ATM")) {
        i = R.drawable.ic_money_bill_alt;
      }
      break;
    case -459336179: 
      if (str1.equals("ACCOUNT")) {
        if (k.a(str2, "CREDIT")) {
          i = R.drawable.ic_rupee_circle;
        } else {
          i = R.drawable.ic_wallet;
        }
      }
      break;
    }
    int i = R.drawable.ic_wallet;
    localObject3 = ((z.d)localObject4).a(i).b((PendingIntent)localObject3);
    if (!paramBoolean2)
    {
      localObject4 = paramc.d(b);
      ((z.d)localObject3).a(a(paramc, paramMatchedNotificationAttributes, paramBoolean1, paramString));
      ((z.d)localObject3).a(new z.a(0, (CharSequence)b.getString(R.string.WhatsThis), (PendingIntent)localObject4));
      ((z.d)localObject3).a((PendingIntent)localObject4);
    }
    else if (!paramBoolean1)
    {
      localObject4 = paramc.c(b);
      ((z.d)localObject3).a(new z.a(0, (CharSequence)b.getString(R.string.ShowSMS), (PendingIntent)localObject4));
      ((z.d)localObject3).a(a(paramc, paramMatchedNotificationAttributes, paramBoolean1, paramString));
      ((z.d)localObject3).a((PendingIntent)localObject4);
    }
    else
    {
      localObject4 = paramc.b(b);
      ((z.d)localObject3).a(new z.a(0, (CharSequence)b.getString(R.string.ShowSMS), (PendingIntent)localObject4));
      ((z.d)localObject3).a(a(paramc, paramMatchedNotificationAttributes, paramBoolean1, paramString));
      ((z.d)localObject3).a((PendingIntent)localObject4);
    }
    if (c.a()) {}
    try
    {
      ((z.d)localObject3).a(e.a(a).d());
      paramString = d.iterator();
      paramMatchedNotificationAttributes = (MatchedNotificationAttributes)localObject2;
      paramc = (c)localObject1;
      while (paramString.hasNext())
      {
        localObject1 = (NotificationAttribute)paramString.next();
        localObject2 = a;
        i = ((String)localObject2).hashCode();
        if (i != -1868540019)
        {
          if (i != -973978124)
          {
            if (i != -389150394)
            {
              if ((i == 821354847) && (((String)localObject2).equals("contentTitle")))
              {
                localObject2 = new StringBuilder();
                ((StringBuilder)localObject2).append(b);
                ((StringBuilder)localObject2).append(d);
                ((z.d)localObject3).a((CharSequence)((StringBuilder)localObject2).toString());
              }
            }
            else if (((String)localObject2).equals("contentText"))
            {
              localObject2 = new StringBuilder();
              ((StringBuilder)localObject2).append(paramc);
              ((StringBuilder)localObject2).append(b);
              ((StringBuilder)localObject2).append(d);
              paramc = ((StringBuilder)localObject2).toString();
              ((z.d)localObject3).b((CharSequence)paramc);
            }
          }
          else if (((String)localObject2).equals("additionalText"))
          {
            localObject2 = new StringBuilder();
            ((StringBuilder)localObject2).append(paramMatchedNotificationAttributes);
            ((StringBuilder)localObject2).append(b);
            ((StringBuilder)localObject2).append(d);
            paramMatchedNotificationAttributes = ((StringBuilder)localObject2).toString();
          }
        }
        else if (((String)localObject2).equals("subText"))
        {
          localObject2 = new StringBuilder();
          ((StringBuilder)localObject2).append(b);
          ((StringBuilder)localObject2).append(d);
          ((z.d)localObject3).c((CharSequence)((StringBuilder)localObject2).toString());
        }
      }
      i = ((CharSequence)paramMatchedNotificationAttributes).length();
      int j = 1;
      if (i > 0) {
        i = 1;
      } else {
        i = 0;
      }
      if (i != 0)
      {
        paramString = new z.c();
        localObject1 = new StringBuilder();
        ((StringBuilder)localObject1).append(paramc);
        ((StringBuilder)localObject1).append("\n");
        ((StringBuilder)localObject1).append(paramMatchedNotificationAttributes);
        ((z.d)localObject3).a((z.g)paramString.b((CharSequence)((StringBuilder)localObject1).toString()));
      }
      paramMatchedNotificationAttributes = b.getSystemService("notification");
      if (paramMatchedNotificationAttributes != null)
      {
        ((NotificationManager)paramMatchedNotificationAttributes).notify(paramInt, ((z.d)localObject3).h());
        paramMatchedNotificationAttributes = (Collection)a;
        i = j;
        if (paramMatchedNotificationAttributes != null) {
          if (paramMatchedNotificationAttributes.isEmpty()) {
            i = j;
          } else {
            i = 0;
          }
        }
        if (i != 0) {
          a = ((HashSet)a());
        }
        a.add(Integer.valueOf(paramInt));
        a((Collection)a);
        return;
      }
      throw new u("null cannot be cast to non-null type android.app.NotificationManager");
    }
    catch (IOException paramc)
    {
      for (;;) {}
    }
  }
  
  final void a(Collection<Integer> paramCollection)
  {
    try
    {
      FileOutputStream localFileOutputStream = b.openFileOutput("smartNotifications.state", 0);
      try
      {
        DataOutputStream localDataOutputStream = new DataOutputStream((OutputStream)localFileOutputStream);
        paramCollection = paramCollection.iterator();
        while (paramCollection.hasNext()) {
          localDataOutputStream.writeInt(((Number)paramCollection.next()).intValue());
        }
        return;
      }
      finally
      {
        d.a((Closeable)localFileOutputStream);
      }
      return;
    }
    catch (IOException paramCollection)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramCollection);
    }
  }
  
  public final boolean a(int paramInt)
  {
    Collection localCollection = (Collection)a;
    int i;
    if ((localCollection != null) && (!localCollection.isEmpty())) {
      i = 0;
    } else {
      i = 1;
    }
    if (i != 0) {
      a = ((HashSet)a());
    }
    return a.contains(Integer.valueOf(paramInt));
  }
  
  public final void b(int paramInt)
  {
    kotlinx.coroutines.e.b((ag)bg.a, f, (m)new b.a(this, paramInt, null), 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.b.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */