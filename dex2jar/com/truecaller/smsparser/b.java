package com.truecaller.smsparser;

import c.a.ag;
import c.g.b.k;
import c.n;
import c.t;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.f;
import com.truecaller.smsparser.models.MatchedNotificationAttributes;
import com.truecaller.tracking.events.ao;
import com.truecaller.tracking.events.ao.a;
import java.util.List;
import javax.inject.Inject;
import org.apache.a.d.d;

public final class b
  implements a
{
  private final com.truecaller.analytics.b a;
  private final com.truecaller.smsparser.b.a b;
  private final com.truecaller.smsparser.a.a c;
  private final com.truecaller.smsparser.a.c d;
  private final f<ae> e;
  
  @Inject
  public b(com.truecaller.analytics.b paramb, com.truecaller.smsparser.b.a parama, com.truecaller.smsparser.a.a parama1, com.truecaller.smsparser.a.c paramc, f<ae> paramf)
  {
    a = paramb;
    b = parama;
    c = parama1;
    d = paramc;
    e = paramf;
  }
  
  private final void a(boolean paramBoolean, String paramString1, String paramString2, String paramString3)
  {
    Object localObject = new e.a("SmartNotification");
    ((e.a)localObject).a("IsDefaultApp", paramBoolean);
    ((e.a)localObject).a("SenderId", paramString1);
    ((e.a)localObject).a("SenderName", paramString2);
    ((e.a)localObject).a("MessageType", paramString3);
    com.truecaller.analytics.b localb = a;
    localObject = ((e.a)localObject).a();
    k.a(localObject, "event.build()");
    localb.a((e)localObject);
    ((ae)e.a()).a((d)ao.b().a((CharSequence)"SmartNotification").a(ag.a(new n[] { t.a("IsDefaultApp", String.valueOf(paramBoolean)), t.a("SenderId", paramString1), t.a("SenderName", paramString2), t.a("MessageType", paramString3) })).a());
  }
  
  public final boolean a(String paramString1, String paramString2, com.truecaller.smsparser.b.c paramc, boolean paramBoolean1, boolean paramBoolean2, long paramLong)
  {
    k.b(paramString1, "senderId");
    k.b(paramString2, "message");
    k.b(paramc, "smartNotificationsHelper");
    Object localObject = c.a(paramString1);
    if (localObject == null) {
      return false;
    }
    paramString2 = d.a(paramString2, (com.truecaller.smsparser.models.c)localObject);
    if (paramString2 != null)
    {
      com.truecaller.smsparser.b.a locala = b;
      int i = (int)paramLong;
      if (!locala.a(i))
      {
        b.a(paramString2, paramc, paramBoolean1, paramBoolean2, paramString1, i);
        paramc = ((com.truecaller.smsparser.models.b)a.get(0)).b();
        localObject = b;
        paramString2 = c;
        localObject = new StringBuilder((String)localObject);
        ((StringBuilder)localObject).append("_");
        ((StringBuilder)localObject).append(paramString2);
        paramString2 = ((StringBuilder)localObject).toString();
        k.a(paramString2, "builder.append(\"_\").append(messageType).toString()");
        a(paramBoolean1, paramString1, paramc, paramString2);
      }
      return true;
    }
    a(paramBoolean1, paramString1, ((com.truecaller.smsparser.models.b)a.get(0)).b(), "NotParsed");
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.smsparser.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */