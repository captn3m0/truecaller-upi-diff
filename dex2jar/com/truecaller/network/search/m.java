package com.truecaller.network.search;

import android.content.Context;
import c.g.b.k;
import java.util.UUID;

public final class m
  implements l
{
  private final Context a;
  
  public m(Context paramContext)
  {
    a = paramContext;
  }
  
  public final j a(UUID paramUUID, String paramString)
  {
    k.b(paramUUID, "requestId");
    k.b(paramString, "searchSource");
    return new j(a, paramUUID, paramString);
  }
  
  public final d b(UUID paramUUID, String paramString)
  {
    k.b(paramUUID, "requestId");
    k.b(paramString, "searchSource");
    return new d(a, paramUUID, paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.search.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */