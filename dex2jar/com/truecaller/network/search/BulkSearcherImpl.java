package com.truecaller.network.search;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Keep;
import android.support.v4.content.d;
import android.support.v7.widget.RecyclerView.Adapter;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.b.a;
import com.truecaller.common.h.ab;
import com.truecaller.common.h.am;
import com.truecaller.utils.i;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BulkSearcherImpl
  implements e
{
  private static final Intent d = new Intent("com.truecaller.actions.BULK_SEARCH_COMPLETE");
  final Set<String> a = new HashSet();
  final Set<String> b = new HashSet();
  final LinkedHashMap<String, d.b> c = new BulkSearcherImpl.1(this);
  private final Context e;
  private final d f;
  private final int g;
  private final int h;
  private final int i;
  private final RecyclerView.Adapter j;
  private final Handler k;
  private final i l;
  private final int m;
  @Keep
  private j.b mListener;
  private final String n;
  private final Map<String, Integer> o = new HashMap();
  private List<e.a> p = new ArrayList();
  private final Runnable q = new BulkSearcherImpl.2(this);
  
  public BulkSearcherImpl(Context paramContext, int paramInt, String paramString, e.a parama)
  {
    this(paramContext, paramInt, paramString, parama, (byte)0);
  }
  
  private BulkSearcherImpl(Context paramContext, int paramInt, String paramString, e.a parama, byte paramByte)
  {
    e = paramContext.getApplicationContext();
    f = d.a(e);
    g = 10;
    h = 2;
    i = 500;
    j = null;
    k = new Handler(Looper.getMainLooper());
    m = paramInt;
    n = paramString;
    a(parama);
    l = ((bk)e).a().v();
  }
  
  public final void a(e.a parama)
  {
    if (parama != null) {
      p.add(parama);
    }
  }
  
  public final void a(String paramString1, String paramString2)
  {
    if (paramString1 == null) {
      return;
    }
    if ((!a.contains(paramString1)) && (!b.contains(paramString1)) && (!c.containsKey(paramString1)))
    {
      Integer localInteger = (Integer)o.get(paramString1);
      int i1;
      if ((localInteger != null) && (localInteger.intValue() > h)) {
        i1 = 1;
      } else {
        i1 = 0;
      }
      if ((i1 == 0) && (!am.d(paramString1)) && ((20 == m) || (ab.e(paramString1))) && (l.a()) && (((a)e).p())) {
        c.put(paramString1, new d.b(paramString1, paramString2, null));
      }
    }
    k.removeCallbacks(q);
    if (!c.isEmpty()) {
      k.postDelayed(q, i);
    }
  }
  
  final void a(Collection<String> paramCollection)
  {
    a.removeAll(paramCollection);
    b.removeAll(paramCollection);
    Object localObject = paramCollection.iterator();
    while (((Iterator)localObject).hasNext())
    {
      String str = (String)((Iterator)localObject).next();
      int i1 = 0;
      if (o.containsKey(str)) {
        i1 = ((Integer)o.get(str)).intValue() + 1;
      }
      o.put(str, Integer.valueOf(i1));
    }
    localObject = j;
    if (localObject != null) {
      ((RecyclerView.Adapter)localObject).notifyDataSetChanged();
    }
    localObject = p.iterator();
    while (((Iterator)localObject).hasNext()) {
      ((e.a)((Iterator)localObject).next()).a(new HashSet(paramCollection));
    }
  }
  
  public final boolean a(String paramString)
  {
    return (paramString != null) && ((c.containsKey(paramString)) || (b.contains(paramString)));
  }
  
  public final void b(e.a parama)
  {
    p.remove(parama);
  }
  
  final void b(Collection<String> paramCollection)
  {
    b.removeAll(paramCollection);
    f.a(d);
    Iterator localIterator = p.iterator();
    while (localIterator.hasNext()) {
      ((e.a)localIterator.next()).a(paramCollection);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.search.BulkSearcherImpl
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */