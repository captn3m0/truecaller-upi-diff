package com.truecaller.network.a;

import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.common.network.account.CheckCredentialsRequestDto;
import com.truecaller.common.network.account.CheckCredentialsResponseDto;
import com.truecaller.common.network.c;
import com.truecaller.log.AssertionUtil;
import e.b;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public final class e
  implements d
{
  private final com.truecaller.common.account.r a;
  private final com.truecaller.common.g.a b;
  private final c c;
  
  public e(com.truecaller.common.account.r paramr, com.truecaller.common.g.a parama, c paramc)
  {
    a = paramr;
    b = parama;
    c = paramc;
  }
  
  public final void a(String paramString)
    throws IOException
  {
    try
    {
      k.b(paramString, "requestUrl");
      boolean bool = a.c();
      if (!bool) {
        return;
      }
      Object localObject1 = b;
      long l1 = 0L;
      long l2 = ((com.truecaller.common.g.a)localObject1).a("checkCredentialsLastTime", 0L);
      long l3 = b.a("checkCredentialsTtl", 0L);
      long l4 = System.currentTimeMillis();
      if ((l3 + l2 > l4) && (l2 < l4)) {
        throw ((Throwable)new f("Token is valid by request TTL, but server returned UNAUTHORIZED to ".concat(String.valueOf(paramString))));
      }
      localObject1 = com.truecaller.common.network.account.a.a;
      Object localObject2 = com.truecaller.common.network.account.a.a(new CheckCredentialsRequestDto("received_unauthorized", paramString, null, 4, null)).c();
      k.a(localObject2, "AccountRestAdapter.check…D, requestUrl)).execute()");
      localObject1 = (CheckCredentialsResponseDto)((e.r)localObject2).e();
      if (((e.r)localObject2).d())
      {
        if (localObject1 != null)
        {
          b.b("checkCredentialsLastTime", System.currentTimeMillis());
          b.b("checkCredentialsTtl", TimeUnit.SECONDS.toMillis(((CheckCredentialsResponseDto)localObject1).getNextCallDuration()));
          localObject2 = ((CheckCredentialsResponseDto)localObject1).getInstallationId();
          if (localObject2 != null)
          {
            com.truecaller.common.account.r localr = a;
            TimeUnit localTimeUnit = TimeUnit.SECONDS;
            Long localLong = ((CheckCredentialsResponseDto)localObject1).getTtl();
            if (localLong != null) {
              l1 = localLong.longValue();
            }
            localr.a((String)localObject2, localTimeUnit.toMillis(l1));
          }
          c.a(((CheckCredentialsResponseDto)localObject1).getDomain());
          throw ((Throwable)new f("Token is valid by request, but server returned UNAUTHORIZED to ".concat(String.valueOf(paramString))));
        }
        return;
      }
      if (((e.r)localObject2).b() == 401)
      {
        paramString = a.e();
        if (paramString != null)
        {
          int i = ((CharSequence)paramString).length();
          if (i > 0) {
            i = 1;
          } else {
            i = 0;
          }
          if (i != 0) {
            try
            {
              TrueApp.y().a(paramString, true, "CheckCredentials");
              return;
            }
            catch (SecurityException paramString)
            {
              AssertionUtil.reportThrowableButNeverCrash((Throwable)paramString);
            }
          }
        }
      }
      return;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.a.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */