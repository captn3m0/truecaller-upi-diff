package com.truecaller.network.a;

import android.os.Bundle;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.analytics.AppHeartBeatTask;
import com.truecaller.analytics.AppSettingsTask;
import com.truecaller.analytics.ae;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.log.AssertionUtil;
import java.io.IOException;
import javax.inject.Inject;
import javax.inject.Named;
import okhttp3.y;

public final class c
  implements a
{
  private final com.truecaller.common.background.b a;
  private final ae b;
  private final y c;
  private final com.truecaller.common.account.r d;
  private final f<com.truecaller.presence.c> e;
  
  @Inject
  public c(com.truecaller.common.background.b paramb, ae paramae, @Named("andlytics-network-client") y paramy, com.truecaller.common.account.r paramr, f<com.truecaller.presence.c> paramf)
  {
    a = paramb;
    b = paramae;
    c = paramy;
    d = paramr;
    e = paramf;
  }
  
  private final boolean a(boolean paramBoolean1, boolean paramBoolean2)
  {
    if (!TrueApp.y().p()) {
      return false;
    }
    AppSettingsTask.b(a);
    Object localObject = AppHeartBeatTask.k;
    localObject = a;
    k.b(localObject, "scheduler");
    Bundle localBundle = new Bundle();
    localBundle.putString("beatType", "deactivation");
    ((com.truecaller.common.background.b)localObject).a(10028, localBundle);
    b.a(c);
    ((com.truecaller.presence.c)e.a()).c().d();
    if (paramBoolean1) {}
    try
    {
      localObject = com.truecaller.common.network.account.a.a;
      localObject = com.truecaller.common.network.account.a.b().c();
      k.a(localObject, "AccountRestAdapter.deactivateAndDelete().execute()");
      break label153;
      localObject = com.truecaller.common.network.account.a.a;
      localObject = com.truecaller.common.network.account.a.a().c();
      k.a(localObject, "AccountRestAdapter.deactivate().execute()");
      label153:
      if (((e.r)localObject).d())
      {
        localObject = d.e();
        if (localObject != null)
        {
          int i = ((CharSequence)localObject).length();
          if (i > 0) {
            i = 1;
          } else {
            i = 0;
          }
          if (i != 0) {
            try
            {
              TrueApp.y().a((String)localObject, true, paramBoolean2, "Deactivate");
              return true;
            }
            catch (SecurityException localSecurityException)
            {
              AssertionUtil.shouldNeverHappen((Throwable)localSecurityException, new String[0]);
            }
          }
        }
        return true;
      }
    }
    catch (RuntimeException localRuntimeException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localRuntimeException);
      return false;
    }
    catch (IOException localIOException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localIOException);
    }
    return false;
  }
  
  public final w<Boolean> a(boolean paramBoolean)
  {
    w localw = w.b(Boolean.valueOf(a(paramBoolean, false)));
    k.a(localw, "Promise.wrap(deactivateAccount(deleteData, false))");
    return localw;
  }
  
  public final w<Boolean> b(boolean paramBoolean)
  {
    w localw = w.b(Boolean.valueOf(a(paramBoolean, true)));
    k.a(localw, "Promise.wrap(deactivateAccount(deleteData, true))");
    return localw;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */