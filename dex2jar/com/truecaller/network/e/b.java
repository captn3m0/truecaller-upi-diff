package com.truecaller.network.e;

import c.g.b.k;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.network.util.h;
import com.truecaller.profile.data.dto.Profile;
import com.truecaller.profile.data.dto.ProfileResponse;
import javax.inject.Inject;
import okhttp3.ae;

public final class b
  implements a
{
  private final b.a a = (b.a)h.a(KnownEndpoints.PROFILE, b.a.class);
  
  public final e.b<ProfileResponse> a()
  {
    return a.a();
  }
  
  public final e.b<ae> a(@e.b.a Profile paramProfile)
  {
    k.b(paramProfile, "profile");
    return a.a(paramProfile);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.e.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */