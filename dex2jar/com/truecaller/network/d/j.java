package com.truecaller.network.d;

import com.truecaller.common.network.e;
import io.grpc.c.a;

public abstract interface j<NonBlocking extends a<NonBlocking>, Blocking extends a<Blocking>>
{
  public abstract NonBlocking a(e parame);
  
  public abstract Blocking b(e parame);
  
  public abstract boolean c(e parame);
}

/* Location:
 * Qualified Name:     com.truecaller.network.d.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */