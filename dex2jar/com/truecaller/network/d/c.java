package com.truecaller.network.d;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import c.g.b.k;
import com.truecaller.utils.i;
import io.grpc.al;
import java.util.LinkedHashMap;
import java.util.Map;

public final class c
  implements b
{
  final Map<Object, al> a;
  final i b;
  private boolean c;
  private final c.a d;
  private final Context e;
  
  public c(Context paramContext, i parami)
  {
    e = paramContext;
    b = parami;
    a = ((Map)new LinkedHashMap());
    d = new c.a(this);
  }
  
  public final void a(Object paramObject, al paramal)
  {
    k.b(paramObject, "tag");
    k.b(paramal, "channel");
    a.put(paramObject, paramal);
    if (!c)
    {
      paramObject = new IntentFilter();
      ((IntentFilter)paramObject).addAction("android.net.conn.CONNECTIVITY_CHANGE");
      e.registerReceiver((BroadcastReceiver)d, (IntentFilter)paramObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.d.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */