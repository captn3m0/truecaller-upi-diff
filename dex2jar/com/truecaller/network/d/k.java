package com.truecaller.network.d;

import c.a.m;
import c.u;
import com.truecaller.common.account.r;
import com.truecaller.common.network.KnownDomain;
import com.truecaller.common.network.e.b;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.utils.d;
import io.grpc.al;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public abstract class k<NonBlocking extends io.grpc.c.a<NonBlocking>, Blocking extends io.grpc.c.a<Blocking>>
  implements j<NonBlocking, Blocking>
{
  private final Map<com.truecaller.common.network.e, l<NonBlocking, Blocking>> a;
  private final KnownEndpoints b;
  private final r c;
  private final com.truecaller.common.account.k d;
  private final d e;
  private final Integer f;
  private final b g;
  private final com.truecaller.common.edge.a h;
  private final com.truecaller.common.network.c i;
  private final boolean j;
  private final e k;
  private final String l;
  private final com.truecaller.d.b m;
  private final com.truecaller.common.h.i n;
  
  public k(KnownEndpoints paramKnownEndpoints, r paramr, com.truecaller.common.account.k paramk, d paramd, Integer paramInteger, b paramb, com.truecaller.common.edge.a parama, com.truecaller.common.network.c paramc, boolean paramBoolean, e parame, String paramString, com.truecaller.d.b paramb1, com.truecaller.common.h.i parami)
  {
    b = paramKnownEndpoints;
    c = paramr;
    d = paramk;
    e = paramd;
    f = paramInteger;
    g = paramb;
    h = parama;
    i = paramc;
    j = paramBoolean;
    k = parame;
    l = paramString;
    m = paramb1;
    n = parami;
    a = ((Map)new LinkedHashMap());
  }
  
  private static <S extends io.grpc.c.a<S>> S a(S paramS, Integer paramInteger)
  {
    if (paramInteger == null) {
      return paramS;
    }
    paramS = paramS.withDeadlineAfter(paramInteger.intValue(), TimeUnit.SECONDS);
    c.g.b.k.a(paramS, "this.withDeadlineAfter(timeout.toLong(), SECONDS)");
    return paramS;
  }
  
  private final io.grpc.i[] b()
  {
    Object[] arrayOfObject = ((Collection)m.d(a())).toArray(new io.grpc.i[0]);
    if (arrayOfObject != null) {
      return (io.grpc.i[])arrayOfObject;
    }
    throw new u("null cannot be cast to non-null type kotlin.Array<T>");
  }
  
  private final boolean d(com.truecaller.common.network.e parame)
  {
    for (;;)
    {
      try
      {
        Object localObject1 = n.b(parame);
        Object localObject2;
        if (localObject1 != null)
        {
          localObject2 = h.a(a.getValue(), b.getKey());
          localObject1 = localObject2;
          if (localObject2 != null) {}
        }
        else
        {
          localObject1 = b;
          localObject2 = h;
          localObject3 = i;
          c.g.b.k.b(localObject1, "receiver$0");
          c.g.b.k.b(localObject2, "edgeLocationsManager");
          c.g.b.k.b(localObject3, "domainResolver");
          localObject1 = com.truecaller.common.edge.f.a(((KnownEndpoints)localObject1).getKey(), (com.truecaller.common.edge.a)localObject2, (com.truecaller.common.network.c)localObject3);
        }
        if (localObject1 == null) {
          return false;
        }
        if (m.a())
        {
          localObject3 = m.a(parame, n);
          if (localObject3 == null) {
            return false;
          }
          localObject2 = localObject1;
          localObject1 = localObject3;
        }
        else
        {
          localObject2 = null;
        }
        localObject3 = (l)a.get(parame);
        if (localObject3 != null)
        {
          localObject3 = d;
          boolean bool = c.g.b.k.a(localObject3, localObject1);
          if (bool) {
            return true;
          }
          localObject3 = io.grpc.okhttp.e.d((String)localObject1);
          if (!j)
          {
            com.d.a.k localk = k.a();
            if (localk != null) {
              ((io.grpc.okhttp.e)localObject3).a(localk);
            }
          }
          ((io.grpc.okhttp.e)localObject3).a(TimeUnit.SECONDS);
          ((io.grpc.okhttp.e)localObject3).a(l);
          c.g.b.k.a(localObject3, "this");
          a((io.grpc.okhttp.e)localObject3);
          if (localObject2 != null) {
            ((io.grpc.okhttp.e)localObject3).b((String)localObject2);
          }
          localObject2 = ((io.grpc.okhttp.e)localObject3).a();
          c.g.b.k.a(localObject2, "OkHttpChannelBuilder.for…y(it) }\n        }.build()");
          g.a(parame, (al)localObject2);
          a.put(parame, new l(a((io.grpc.f)localObject2), b((io.grpc.f)localObject2), null, (String)localObject1));
          return true;
        }
      }
      finally {}
      Object localObject3 = null;
    }
  }
  
  public NonBlocking a(com.truecaller.common.network.e parame)
  {
    c.g.b.k.b(parame, "targetDomain");
    if (c(parame))
    {
      parame = (l)a.get(parame);
      if (parame != null)
      {
        parame = a;
        if (parame != null) {
          return a(this, parame);
        }
      }
    }
    return null;
  }
  
  public abstract NonBlocking a(io.grpc.f paramf);
  
  public abstract Collection<io.grpc.i> a();
  
  public void a(io.grpc.okhttp.e parame)
  {
    c.g.b.k.b(parame, "builder");
  }
  
  public Blocking b(com.truecaller.common.network.e parame)
  {
    c.g.b.k.b(parame, "targetDomain");
    if (c(parame))
    {
      parame = (l)a.get(parame);
      if (parame != null)
      {
        parame = b;
        if (parame != null) {
          return a(this, parame);
        }
      }
    }
    return null;
  }
  
  public abstract Blocking b(io.grpc.f paramf);
  
  public final boolean c(com.truecaller.common.network.e parame)
  {
    try
    {
      c.g.b.k.b(parame, "targetDomain");
      boolean bool = d(parame);
      if (!bool) {
        return false;
      }
      Object localObject1 = (l)a.get(parame);
      if (localObject1 == null) {
        return false;
      }
      String str;
      if (n.a(parame)) {
        str = d.a();
      } else {
        str = c.e();
      }
      if (str == null) {
        return false;
      }
      bool = c.g.b.k.a(c, str);
      if (bool) {
        return true;
      }
      Object localObject2 = new a(str);
      Map localMap = a;
      io.grpc.c.a locala = a.withCallCredentials((io.grpc.c)localObject2);
      io.grpc.i[] arrayOfi = b();
      locala = locala.withInterceptors((io.grpc.i[])Arrays.copyOf(arrayOfi, arrayOfi.length));
      c.g.b.k.a(locala, "asyncStub.withCallCreden…ors(*buildInterceptors())");
      localObject2 = b.withCallCredentials((io.grpc.c)localObject2);
      arrayOfi = b();
      localObject2 = ((io.grpc.c.a)localObject2).withInterceptors((io.grpc.i[])Arrays.copyOf(arrayOfi, arrayOfi.length));
      c.g.b.k.a(localObject2, "syncStub.withCallCredent…ors(*buildInterceptors())");
      localObject1 = d;
      c.g.b.k.b(locala, "asyncStub");
      c.g.b.k.b(localObject2, "syncStub");
      c.g.b.k.b(localObject1, "host");
      localMap.put(parame, new l(locala, (io.grpc.c.a)localObject2, str, (String)localObject1));
      return true;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.d.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */