package com.truecaller.network.d;

import javax.inject.Provider;

public final class h
  implements dagger.a.d<e>
{
  private final g a;
  private final Provider<com.truecaller.utils.d> b;
  
  private h(g paramg, Provider<com.truecaller.utils.d> paramProvider)
  {
    a = paramg;
    b = paramProvider;
  }
  
  public static h a(g paramg, Provider<com.truecaller.utils.d> paramProvider)
  {
    return new h(paramg, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.d.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */