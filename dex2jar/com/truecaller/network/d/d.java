package com.truecaller.network.d;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.ConnectivityManager.NetworkCallback;
import android.net.Network;
import c.g.b.k;
import c.u;
import io.grpc.al;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public final class d
  extends ConnectivityManager.NetworkCallback
  implements b
{
  private final ConnectivityManager a;
  private boolean b;
  private boolean c;
  private final Map<Object, al> d;
  
  public d(Context paramContext)
  {
    paramContext = paramContext.getSystemService("connectivity");
    if (paramContext != null)
    {
      a = ((ConnectivityManager)paramContext);
      d = ((Map)new LinkedHashMap());
      return;
    }
    throw new u("null cannot be cast to non-null type android.net.ConnectivityManager");
  }
  
  public final void a(Object paramObject, al paramal)
  {
    k.b(paramObject, "tag");
    k.b(paramal, "channel");
    d.put(paramObject, paramal);
    if (!b)
    {
      paramObject = a;
      if (paramObject != null)
      {
        ((ConnectivityManager)paramObject).registerDefaultNetworkCallback((ConnectivityManager.NetworkCallback)this);
        b = true;
        return;
      }
    }
  }
  
  public final void onAvailable(Network paramNetwork)
  {
    k.b(paramNetwork, "network");
    if (c)
    {
      paramNetwork = ((Iterable)d.values()).iterator();
      while (paramNetwork.hasNext()) {
        ((al)paramNetwork.next()).c();
      }
    }
    c = false;
  }
  
  public final void onLost(Network paramNetwork)
  {
    k.b(paramNetwork, "network");
    c = true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.d.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */