package com.truecaller.network.b;

import c.g.b.k;
import c.n.m;
import com.truecaller.analytics.ae;
import com.truecaller.androidactors.f;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.inject.Inject;
import okhttp3.ab;
import okhttp3.q;
import okhttp3.u;

public final class a
  implements com.truecaller.common.network.f.a
{
  private final f<ae> a;
  private final com.truecaller.utils.a b;
  private final com.truecaller.i.e c;
  
  @Inject
  public a(f<ae> paramf, com.truecaller.utils.a parama, com.truecaller.i.e parame)
  {
    a = paramf;
    b = parama;
    c = parame;
  }
  
  public final q a(okhttp3.e parame)
  {
    k.b(parame, "call");
    parame = parame.a().a().f().toString();
    Object localObject1 = c.b("httpAnalyitcsHosts", "");
    k.a(localObject1, "generalSettings\n        …HTTP_ANALYTICS_HOSTS, \"\")");
    localObject1 = (CharSequence)localObject1;
    int j = 0;
    Object localObject2 = (Iterable)m.c((CharSequence)localObject1, new String[] { "," }, false, 6);
    localObject1 = (Collection)new ArrayList();
    localObject2 = ((Iterable)localObject2).iterator();
    while (((Iterator)localObject2).hasNext())
    {
      Object localObject3 = ((Iterator)localObject2).next();
      if (!m.a((CharSequence)localObject3)) {
        ((Collection)localObject1).add(localObject3);
      }
    }
    localObject1 = (Iterable)localObject1;
    int i = j;
    if (!((Collection)localObject1).isEmpty())
    {
      localObject1 = ((Iterable)localObject1).iterator();
      do
      {
        i = j;
        if (!((Iterator)localObject1).hasNext()) {
          break;
        }
      } while (!m.b(parame, (String)((Iterator)localObject1).next(), false));
      i = 1;
    }
    if (i != 0) {
      return (q)new d(a, b, parame);
    }
    return (q)e.a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.b.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */