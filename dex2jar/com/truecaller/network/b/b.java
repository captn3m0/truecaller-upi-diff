package com.truecaller.network.b;

import com.truecaller.analytics.ae;
import com.truecaller.androidactors.f;
import com.truecaller.i.e;
import dagger.a.d;
import javax.inject.Provider;

public final class b
  implements d<a>
{
  private final Provider<f<ae>> a;
  private final Provider<com.truecaller.utils.a> b;
  private final Provider<e> c;
  
  private b(Provider<f<ae>> paramProvider, Provider<com.truecaller.utils.a> paramProvider1, Provider<e> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static b a(Provider<f<ae>> paramProvider, Provider<com.truecaller.utils.a> paramProvider1, Provider<e> paramProvider2)
  {
    return new b(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.b.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */