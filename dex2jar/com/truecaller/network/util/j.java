package com.truecaller.network.util;

import com.truecaller.androidactors.f;
import com.truecaller.common.account.k;
import com.truecaller.common.account.r;
import com.truecaller.common.h.af;
import com.truecaller.common.network.c;
import javax.inject.Provider;

public final class j
  implements dagger.a.d<i>
{
  private final Provider<String> a;
  private final Provider<String> b;
  private final Provider<c> c;
  private final Provider<com.truecaller.analytics.b> d;
  private final Provider<r> e;
  private final Provider<com.truecaller.network.a.d> f;
  private final Provider<f<com.truecaller.config.a>> g;
  private final Provider<com.truecaller.common.edge.a> h;
  private final Provider<com.truecaller.d.b> i;
  private final Provider<k> j;
  private final Provider<af> k;
  
  private j(Provider<String> paramProvider1, Provider<String> paramProvider2, Provider<c> paramProvider, Provider<com.truecaller.analytics.b> paramProvider3, Provider<r> paramProvider4, Provider<com.truecaller.network.a.d> paramProvider5, Provider<f<com.truecaller.config.a>> paramProvider6, Provider<com.truecaller.common.edge.a> paramProvider7, Provider<com.truecaller.d.b> paramProvider8, Provider<k> paramProvider9, Provider<af> paramProvider10)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
    k = paramProvider10;
  }
  
  public static j a(Provider<String> paramProvider1, Provider<String> paramProvider2, Provider<c> paramProvider, Provider<com.truecaller.analytics.b> paramProvider3, Provider<r> paramProvider4, Provider<com.truecaller.network.a.d> paramProvider5, Provider<f<com.truecaller.config.a>> paramProvider6, Provider<com.truecaller.common.edge.a> paramProvider7, Provider<com.truecaller.d.b> paramProvider8, Provider<k> paramProvider9, Provider<af> paramProvider10)
  {
    return new j(paramProvider1, paramProvider2, paramProvider, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */