package com.truecaller.network.util;

import android.os.Build.VERSION;
import com.truecaller.androidactors.w;
import com.truecaller.common.b.a;
import com.truecaller.common.h.k;
import com.truecaller.common.network.feedback.Feedback;
import com.truecaller.common.network.feedback.a.a;
import com.truecaller.common.network.util.KnownEndpoints;
import e.b;
import e.r;
import java.io.IOException;
import okhttp3.ad;

final class h
  implements f
{
  public final w<Integer> a(CharSequence paramCharSequence1, CharSequence paramCharSequence2, CharSequence paramCharSequence3, CharSequence paramCharSequence4, String paramString)
  {
    a locala = a.F();
    if ("gold".equals(paramString)) {
      paramString = "(GOLD_USER)";
    } else if ("regular".equals(paramString)) {
      paramString = "(PREMIUM_USER)";
    } else {
      paramString = "";
    }
    paramCharSequence1 = String.format("FEEDBACK FORM ANDROID %s\r\nName: %s\r\nSubject: %s\r\nDevice Name: %s\r\nAndroid OS Version: %s\r\n%s Version: %s\r\nFeedback:\r\n\r\n%s", new Object[] { paramString, paramCharSequence1, paramCharSequence3, k.c(), Build.VERSION.RELEASE, locala.k(), locala.l(), paramCharSequence4 });
    paramCharSequence1 = new Feedback(paramCharSequence2.toString(), paramCharSequence1);
    paramCharSequence1 = ((a.a)com.truecaller.common.network.util.h.a(KnownEndpoints.FEEDBACK, a.a.class)).a(paramCharSequence1);
    try
    {
      paramCharSequence1 = paramCharSequence1.c();
      if (paramCharSequence1 != null)
      {
        paramCharSequence1 = w.b(Integer.valueOf(a.c));
        return paramCharSequence1;
      }
    }
    catch (IOException paramCharSequence1)
    {
      for (;;) {}
    }
    return w.b(null);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */