package com.truecaller.network.util;

import dagger.a.d;
import javax.inject.Provider;

public final class r
  implements d<c>
{
  private final k a;
  private final Provider<com.truecaller.network.util.calling_cache.c> b;
  
  private r(k paramk, Provider<com.truecaller.network.util.calling_cache.c> paramProvider)
  {
    a = paramk;
    b = paramProvider;
  }
  
  public static r a(k paramk, Provider<com.truecaller.network.util.calling_cache.c> paramProvider)
  {
    return new r(paramk, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.r
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */