package com.truecaller.network.util;

import com.truecaller.androidactors.i;
import dagger.a.d;
import javax.inject.Provider;

public final class u
  implements d<i>
{
  private final k a;
  private final Provider<com.truecaller.androidactors.k> b;
  
  private u(k paramk, Provider<com.truecaller.androidactors.k> paramProvider)
  {
    a = paramk;
    b = paramProvider;
  }
  
  public static u a(k paramk, Provider<com.truecaller.androidactors.k> paramProvider)
  {
    return new u(paramk, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.u
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */