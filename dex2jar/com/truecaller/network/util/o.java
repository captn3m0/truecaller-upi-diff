package com.truecaller.network.util;

import android.content.Context;
import com.truecaller.network.util.calling_cache.a;
import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d<a>
{
  private final k a;
  private final Provider<Context> b;
  
  private o(k paramk, Provider<Context> paramProvider)
  {
    a = paramk;
    b = paramProvider;
  }
  
  public static o a(k paramk, Provider<Context> paramProvider)
  {
    return new o(paramk, paramProvider);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */