package com.truecaller.network.util.calling_cache;

import c.g.b.k;
import java.util.concurrent.TimeUnit;
import okhttp3.ad;

public final class d
  implements c
{
  private final a a;
  private final com.truecaller.utils.a b;
  
  public d(a parama, com.truecaller.utils.a parama1)
  {
    a = parama;
    b = parama1;
  }
  
  private static String a(com.truecaller.data.entity.Number paramNumber)
  {
    String str2 = paramNumber.a();
    String str1 = str2;
    if (str2 == null) {
      str1 = paramNumber.d();
    }
    paramNumber = str1;
    if (str1 == null) {
      paramNumber = "";
    }
    return paramNumber;
  }
  
  public final void a(com.truecaller.data.entity.Number paramNumber, String paramString, ad paramad)
  {
    k.b(paramNumber, "number");
    k.b(paramString, "callState");
    k.b(paramad, "response");
    paramad = paramad.f();
    if (paramad != null) {
      paramad = Long.valueOf(paramad.a());
    } else {
      paramad = null;
    }
    if (paramad != null)
    {
      long l = ((Number)paramad).longValue();
      a.a(new CallCacheEntry(a(paramNumber), b.a(), paramString, l, null, 16, null));
      return;
    }
  }
  
  public final boolean a(com.truecaller.data.entity.Number paramNumber, String paramString)
  {
    k.b(paramNumber, "number");
    k.b(paramString, "callState");
    paramNumber = a.a(a(paramNumber), paramString);
    if (paramNumber != null) {
      return paramNumber.getTimestamp() + TimeUnit.SECONDS.toMillis(paramNumber.getMaxAgeSeconds()) > b.a();
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.calling_cache.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */