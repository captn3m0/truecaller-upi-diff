package com.truecaller.network.util.calling_cache;

import android.arch.persistence.room.a.a;
import android.arch.persistence.room.e;
import android.arch.persistence.room.f.a;
import android.content.Context;
import c.g.b.k;

public final class CallingCacheDatabase$a
{
  public final CallingCacheDatabase a(Context paramContext)
  {
    try
    {
      k.b(paramContext, "context");
      if (CallingCacheDatabase.i() == null) {
        CallingCacheDatabase.a((CallingCacheDatabase)e.a(paramContext.getApplicationContext(), CallingCacheDatabase.class, "calling-cache.db").a(new a[] { (a)CallingCacheDatabase.j() }).b());
      }
      paramContext = CallingCacheDatabase.i();
      return paramContext;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.calling_cache.CallingCacheDatabase.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */