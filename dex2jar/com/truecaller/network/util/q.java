package com.truecaller.network.util;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import dagger.a.d;
import javax.inject.Provider;

public final class q
  implements d<f<c>>
{
  private final k a;
  private final Provider<i> b;
  private final Provider<c> c;
  
  private q(k paramk, Provider<i> paramProvider, Provider<c> paramProvider1)
  {
    a = paramk;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static q a(k paramk, Provider<i> paramProvider, Provider<c> paramProvider1)
  {
    return new q(paramk, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */