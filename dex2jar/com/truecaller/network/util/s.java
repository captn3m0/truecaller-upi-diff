package com.truecaller.network.util;

import com.truecaller.androidactors.i;
import dagger.a.d;
import javax.inject.Provider;

public final class s
  implements d<com.truecaller.androidactors.f<f>>
{
  private final k a;
  private final Provider<i> b;
  private final Provider<f> c;
  
  private s(k paramk, Provider<i> paramProvider, Provider<f> paramProvider1)
  {
    a = paramk;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static s a(k paramk, Provider<i> paramProvider, Provider<f> paramProvider1)
  {
    return new s(paramk, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.s
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */