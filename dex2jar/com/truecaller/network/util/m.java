package com.truecaller.network.util;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import com.truecaller.network.a.a;
import dagger.a.d;
import javax.inject.Provider;

public final class m
  implements d<f<a>>
{
  private final k a;
  private final Provider<i> b;
  private final Provider<a> c;
  
  private m(k paramk, Provider<i> paramProvider, Provider<a> paramProvider1)
  {
    a = paramk;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static m a(k paramk, Provider<i> paramProvider, Provider<a> paramProvider1)
  {
    return new m(paramk, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */