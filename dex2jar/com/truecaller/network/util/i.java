package com.truecaller.network.util;

import c.l;
import com.truecaller.common.account.r;
import com.truecaller.common.h.af;
import com.truecaller.common.network.a.a.a;
import com.truecaller.common.network.a.a.b;
import com.truecaller.common.network.a.a.c;
import com.truecaller.common.network.a.a.d;
import com.truecaller.common.network.a.a.e;
import com.truecaller.common.network.a.a.f;
import com.truecaller.common.network.a.a.g;
import com.truecaller.common.network.util.AuthRequirement;
import com.truecaller.network.a.d;
import com.truecaller.network.a.g;
import com.truecaller.network.a.h;
import javax.inject.Inject;
import javax.inject.Named;
import okhttp3.v;

public final class i
  implements com.truecaller.common.network.d.c
{
  private final String a;
  private final String b;
  private final dagger.a<com.truecaller.common.network.c> c;
  private final dagger.a<com.truecaller.analytics.b> d;
  private final dagger.a<r> e;
  private final dagger.a<d> f;
  private final dagger.a<com.truecaller.androidactors.f<com.truecaller.config.a>> g;
  private final dagger.a<com.truecaller.common.edge.a> h;
  private final dagger.a<com.truecaller.d.b> i;
  private final dagger.a<com.truecaller.common.account.k> j;
  private final dagger.a<af> k;
  
  @Inject
  public i(@Named("app_name") String paramString1, @Named("app_ver") String paramString2, dagger.a<com.truecaller.common.network.c> parama, dagger.a<com.truecaller.analytics.b> parama1, dagger.a<r> parama2, dagger.a<d> parama3, dagger.a<com.truecaller.androidactors.f<com.truecaller.config.a>> parama4, dagger.a<com.truecaller.common.edge.a> parama5, dagger.a<com.truecaller.d.b> parama6, dagger.a<com.truecaller.common.account.k> parama7, dagger.a<af> parama8)
  {
    a = paramString1;
    b = paramString2;
    c = parama;
    d = parama1;
    e = parama2;
    f = parama3;
    g = parama4;
    h = parama5;
    i = parama6;
    j = parama7;
    k = parama8;
  }
  
  public final v a(com.truecaller.common.network.a.a parama)
  {
    c.g.b.k.b(parama, "attribute");
    if ((parama instanceof a.b))
    {
      bool = c;
      parama = f;
      localObject1 = j;
      localObject2 = k.get();
      c.g.b.k.a(localObject2, "restCrossDcSupport.get()");
      return (v)new g(bool, parama, (dagger.a)localObject1, (af)localObject2);
    }
    if ((parama instanceof a.g))
    {
      parama = c.get();
      c.g.b.k.a(parama, "domainResolver.get()");
      parama = (com.truecaller.common.network.c)parama;
      localObject1 = d.get();
      c.g.b.k.a(localObject1, "analytics.get()");
      localObject1 = (com.truecaller.analytics.b)localObject1;
      localObject2 = e.get();
      c.g.b.k.a(localObject2, "accountManager.get()");
      return (v)new com.truecaller.network.a.i(parama, (com.truecaller.analytics.b)localObject1, (r)localObject2);
    }
    boolean bool = parama instanceof a.a;
    Object localObject2 = null;
    Object localObject1 = null;
    Object localObject3;
    if (bool)
    {
      localObject2 = c;
      localObject3 = AuthRequirement.NONE;
      bool = true;
      int m;
      if (localObject2 != localObject3) {
        m = 1;
      } else {
        m = 0;
      }
      if (m == 0) {
        parama = null;
      }
      localObject2 = (a.a)parama;
      parama = (com.truecaller.common.network.a.a)localObject1;
      if (localObject2 != null)
      {
        if (c != AuthRequirement.REQUIRED) {
          bool = false;
        }
        parama = e.get();
        c.g.b.k.a(parama, "accountManager.get()");
        parama = (r)parama;
        localObject1 = j;
        localObject2 = k.get();
        c.g.b.k.a(localObject2, "restCrossDcSupport.get()");
        parama = new com.truecaller.common.network.a(bool, parama, (dagger.a)localObject1, (com.truecaller.common.h.i)localObject2);
      }
      return (v)parama;
    }
    if ((parama instanceof a.f))
    {
      parama = g.get();
      c.g.b.k.a(parama, "configManager.get()");
      return (v)new h((com.truecaller.androidactors.f)parama);
    }
    if ((parama instanceof a.e))
    {
      localObject1 = h.get();
      c.g.b.k.a(localObject1, "edgeLocationsManager.get()");
      localObject1 = (com.truecaller.common.edge.a)localObject1;
      localObject2 = d.get();
      c.g.b.k.a(localObject2, "analytics.get()");
      localObject2 = (com.truecaller.analytics.b)localObject2;
      localObject3 = c.get();
      c.g.b.k.a(localObject3, "domainResolver.get()");
      localObject3 = (com.truecaller.common.network.c)localObject3;
      Object localObject4 = k.get();
      c.g.b.k.a(localObject4, "restCrossDcSupport.get()");
      return (v)new com.truecaller.network.c.a((com.truecaller.common.edge.a)localObject1, (com.truecaller.analytics.b)localObject2, (com.truecaller.common.network.c)localObject3, (af)localObject4, c);
    }
    if ((parama instanceof a.d))
    {
      localObject1 = (com.truecaller.d.b)i.get();
      parama = (com.truecaller.common.network.a.a)localObject2;
      if (localObject1 != null)
      {
        parama = (com.truecaller.common.network.a.a)localObject2;
        if (((com.truecaller.d.b)localObject1).a())
        {
          parama = k.get();
          c.g.b.k.a(parama, "restCrossDcSupport.get()");
          parama = new com.truecaller.d.a((com.truecaller.d.b)localObject1, (af)parama);
        }
      }
      return (v)parama;
    }
    if ((parama instanceof a.c)) {
      return (v)new com.truecaller.common.network.d.f(a, b);
    }
    throw new l();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */