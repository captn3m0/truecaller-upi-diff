package com.truecaller.network.util;

import dagger.a.g;
import java.util.concurrent.TimeUnit;
import okhttp3.y;
import okhttp3.y.a;

public final class n
  implements dagger.a.d<y>
{
  private final k a;
  
  private n(k paramk)
  {
    a = paramk;
  }
  
  public static n a(k paramk)
  {
    return new n(paramk);
  }
  
  public static y a()
  {
    return (y)g.a(com.truecaller.common.network.util.d.d().a(30L, TimeUnit.SECONDS).d(), "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */