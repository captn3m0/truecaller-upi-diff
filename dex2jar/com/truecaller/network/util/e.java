package com.truecaller.network.util;

import android.support.v4.f.j;
import com.truecaller.androidactors.w;
import com.truecaller.data.entity.Number;
import e.b;
import e.b.o;
import e.b.s;
import e.b.t;
import e.r;
import java.io.IOException;
import okhttp3.ad;
import okhttp3.ae;

public final class e
  implements c
{
  private final com.truecaller.network.util.calling_cache.c a;
  private final a b;
  
  public e(com.truecaller.network.util.calling_cache.c paramc, a parama)
  {
    a = paramc;
    b = parama;
  }
  
  public final w<j<Boolean, String>> a(String paramString, Number paramNumber)
  {
    c.g.b.k.b(paramString, "callState");
    c.g.b.k.b(paramNumber, "number");
    Object localObject1 = paramNumber.o();
    if ((localObject1 != null) && (!org.c.a.a.a.k.b((CharSequence)localObject1)))
    {
      if (a.a(paramNumber, paramString))
      {
        paramString = w.b(j.a(Boolean.FALSE, paramString));
        c.g.b.k.a(paramString, "Promise.wrap(Pair.create(false, callState))");
        return paramString;
      }
      localObject1 = b.a(paramString, (String)localObject1, paramNumber.l());
    }
    try
    {
      Object localObject2 = ((b)localObject1).c();
      localObject1 = a;
      localObject2 = ((r)localObject2).a();
      c.g.b.k.a(localObject2, "response.raw()");
      ((com.truecaller.network.util.calling_cache.c)localObject1).a(paramNumber, paramString, (ad)localObject2);
      paramString = w.b(j.a(Boolean.TRUE, paramString));
      c.g.b.k.a(paramString, "Promise.wrap(Pair.create(true, callState))");
      return paramString;
    }
    catch (IOException paramNumber)
    {
      for (;;) {}
    }
    paramString = w.b(j.a(Boolean.FALSE, paramString));
    c.g.b.k.a(paramString, "Promise.wrap(Pair.create(false, callState))");
    return paramString;
    paramString = w.b(j.a(Boolean.FALSE, paramString));
    c.g.b.k.a(paramString, "Promise.wrap(Pair.create(false, callState))");
    return paramString;
  }
  
  public static abstract interface a
  {
    @o(a="/v1/callerId/{callState}")
    public abstract b<ae> a(@s(a="callState") String paramString1, @t(a="q") String paramString2, @t(a="countryCode") String paramString3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */