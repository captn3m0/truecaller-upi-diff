package com.truecaller.network.util;

import com.truecaller.network.util.calling_cache.c;
import dagger.a.d;
import javax.inject.Provider;

public final class p
  implements d<c>
{
  private final k a;
  private final Provider<com.truecaller.utils.a> b;
  private final Provider<com.truecaller.network.util.calling_cache.a> c;
  
  private p(k paramk, Provider<com.truecaller.utils.a> paramProvider, Provider<com.truecaller.network.util.calling_cache.a> paramProvider1)
  {
    a = paramk;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static p a(k paramk, Provider<com.truecaller.utils.a> paramProvider, Provider<com.truecaller.network.util.calling_cache.a> paramProvider1)
  {
    return new p(paramk, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */