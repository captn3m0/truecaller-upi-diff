package com.truecaller.network.util;

import com.truecaller.analytics.ae;
import com.truecaller.androidactors.f;
import com.truecaller.common.account.r;
import com.truecaller.common.background.b;
import com.truecaller.network.a.a;
import com.truecaller.presence.c;
import dagger.a.d;
import javax.inject.Provider;
import okhttp3.y;

public final class l
  implements d<a>
{
  private final k a;
  private final Provider<b> b;
  private final Provider<ae> c;
  private final Provider<y> d;
  private final Provider<r> e;
  private final Provider<f<c>> f;
  
  private l(k paramk, Provider<b> paramProvider, Provider<ae> paramProvider1, Provider<y> paramProvider2, Provider<r> paramProvider3, Provider<f<c>> paramProvider4)
  {
    a = paramk;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
    f = paramProvider4;
  }
  
  public static l a(k paramk, Provider<b> paramProvider, Provider<ae> paramProvider1, Provider<y> paramProvider2, Provider<r> paramProvider3, Provider<f<c>> paramProvider4)
  {
    return new l(paramk, paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */