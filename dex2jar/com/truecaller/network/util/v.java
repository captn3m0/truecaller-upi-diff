package com.truecaller.network.util;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import com.truecaller.premium.data.g;
import dagger.a.d;
import javax.inject.Provider;

public final class v
  implements d<f<g>>
{
  private final k a;
  private final Provider<i> b;
  private final Provider<g> c;
  
  private v(k paramk, Provider<i> paramProvider, Provider<g> paramProvider1)
  {
    a = paramk;
    b = paramProvider;
    c = paramProvider1;
  }
  
  public static v a(k paramk, Provider<i> paramProvider, Provider<g> paramProvider1)
  {
    return new v(paramk, paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.v
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */