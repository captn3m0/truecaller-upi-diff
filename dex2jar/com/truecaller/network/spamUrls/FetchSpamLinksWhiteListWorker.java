package com.truecaller.network.spamUrls;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.bp;
import com.truecaller.common.account.r;
import com.truecaller.common.background.TrackedWorker;
import javax.inject.Inject;

public final class FetchSpamLinksWhiteListWorker
  extends TrackedWorker
{
  public static final FetchSpamLinksWhiteListWorker.a e = new FetchSpamLinksWhiteListWorker.a((byte)0);
  @Inject
  public d b;
  @Inject
  public r c;
  @Inject
  public b d;
  
  public FetchSpamLinksWhiteListWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public final b b()
  {
    b localb = d;
    if (localb == null) {
      k.a("analytics");
    }
    return localb;
  }
  
  public final boolean c()
  {
    r localr = c;
    if (localr == null) {
      k.a("accountManager");
    }
    return localr.c();
  }
  
  public final ListenableWorker.a d()
  {
    Object localObject = b;
    if (localObject == null) {
      k.a("whiteListRepository");
    }
    if (((d)localObject).a()) {
      localObject = ListenableWorker.a.a();
    }
    for (String str = "Result.success()";; str = "Result.retry()")
    {
      k.a(localObject, str);
      return (ListenableWorker.a)localObject;
      localObject = ListenableWorker.a.b();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.spamUrls.FetchSpamLinksWhiteListWorker
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */