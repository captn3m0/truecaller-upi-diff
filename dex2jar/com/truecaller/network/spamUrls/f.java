package com.truecaller.network.spamUrls;

import android.content.Context;
import dagger.a.d;
import javax.inject.Provider;

public final class f
  implements d<e>
{
  private final Provider<a> a;
  private final Provider<c.d.f> b;
  private final Provider<Context> c;
  
  private f(Provider<a> paramProvider, Provider<c.d.f> paramProvider1, Provider<Context> paramProvider2)
  {
    a = paramProvider;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static f a(Provider<a> paramProvider, Provider<c.d.f> paramProvider1, Provider<Context> paramProvider2)
  {
    return new f(paramProvider, paramProvider1, paramProvider2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.spamUrls.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */