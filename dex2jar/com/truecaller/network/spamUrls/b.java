package com.truecaller.network.spamUrls;

import c.g.b.k;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.network.util.h;
import com.truecaller.messaging.conversation.spamLinks.GetUrlReportDto;
import com.truecaller.messaging.conversation.spamLinks.SpamReportDto;
import com.truecaller.messaging.conversation.spamLinks.UrlReportDto;
import java.util.List;
import javax.inject.Inject;
import okhttp3.ae;

public final class b
  implements a
{
  private final b.a a = (b.a)h.a(KnownEndpoints.SPAM_URL, b.a.class);
  
  public final b.a a()
  {
    return a;
  }
  
  public final e.b<SpamReportDto> a(String paramString)
  {
    k.b(paramString, "url");
    return a.a(new GetUrlReportDto(paramString));
  }
  
  public final e.b<ae> a(List<UrlReportDto> paramList)
  {
    k.b(paramList, "urlReports");
    return a.a(paramList);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.spamUrls.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */