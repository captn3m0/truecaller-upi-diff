package com.truecaller.network.spamUrls;

import android.content.Context;
import c.d.f;
import c.g.a.m;
import com.truecaller.messaging.conversation.spamLinks.UrlDto;
import com.truecaller.messaging.conversation.spamLinks.WhitelistDto;
import java.io.File;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.inject.Inject;
import javax.inject.Named;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

public final class e
  implements d
{
  final File a;
  private final Set<String> b;
  private final Set<String> c;
  private final a d;
  private final f e;
  
  @Inject
  public e(a parama, @Named("Async") f paramf, Context paramContext)
  {
    d = parama;
    e = paramf;
    a = new File(paramContext.getFilesDir(), "whitelisted_urls.json");
    b = ((Set)new LinkedHashSet());
    c = ((Set)new LinkedHashSet());
    kotlinx.coroutines.e.b((ag)bg.a, e, (m)new e.1(this, null), 2);
  }
  
  /* Error */
  static WhitelistDto a(File paramFile)
  {
    // Byte code:
    //   0: getstatic 98	c/n/d:a	Ljava/nio/charset/Charset;
    //   3: astore_1
    //   4: new 100	java/io/InputStreamReader
    //   7: dup
    //   8: new 102	java/io/FileInputStream
    //   11: dup
    //   12: aload_0
    //   13: invokespecial 105	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   16: checkcast 107	java/io/InputStream
    //   19: aload_1
    //   20: invokespecial 110	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V
    //   23: checkcast 112	java/io/Closeable
    //   26: astore_2
    //   27: aload_2
    //   28: checkcast 100	java/io/InputStreamReader
    //   31: astore_0
    //   32: new 114	com/google/gson/f
    //   35: dup
    //   36: invokespecial 115	com/google/gson/f:<init>	()V
    //   39: aload_0
    //   40: checkcast 117	java/io/Reader
    //   43: ldc 119
    //   45: invokevirtual 122	com/google/gson/f:a	(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;
    //   48: checkcast 119	com/truecaller/messaging/conversation/spamLinks/WhitelistDto
    //   51: astore_0
    //   52: aload_2
    //   53: aconst_null
    //   54: invokestatic 127	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   57: aload_0
    //   58: areturn
    //   59: astore_1
    //   60: aconst_null
    //   61: astore_0
    //   62: goto +7 -> 69
    //   65: astore_0
    //   66: aload_0
    //   67: athrow
    //   68: astore_1
    //   69: aload_2
    //   70: aload_0
    //   71: invokestatic 127	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   74: aload_1
    //   75: athrow
    //   76: astore_0
    //   77: aload_0
    //   78: checkcast 93	java/lang/Throwable
    //   81: invokestatic 132	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   84: aconst_null
    //   85: areturn
    //   86: astore_0
    //   87: aload_0
    //   88: checkcast 93	java/lang/Throwable
    //   91: invokestatic 132	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   94: aconst_null
    //   95: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	96	0	paramFile	File
    //   3	17	1	localCharset	java.nio.charset.Charset
    //   59	1	1	localObject1	Object
    //   68	7	1	localObject2	Object
    //   26	44	2	localCloseable	java.io.Closeable
    // Exception table:
    //   from	to	target	type
    //   27	52	59	finally
    //   27	52	65	java/lang/Throwable
    //   66	68	68	finally
    //   0	27	76	java/lang/Exception
    //   52	57	76	java/lang/Exception
    //   69	76	76	java/lang/Exception
    //   0	27	86	com/google/gson/p
    //   52	57	86	com/google/gson/p
    //   69	76	86	com/google/gson/p
  }
  
  final void a(WhitelistDto paramWhitelistDto)
  {
    try
    {
      paramWhitelistDto = ((Iterable)paramWhitelistDto.getUrls()).iterator();
      while (paramWhitelistDto.hasNext())
      {
        UrlDto localUrlDto = (UrlDto)paramWhitelistDto.next();
        String str = localUrlDto.getKind();
        int i = str.hashCode();
        if (i != 3210)
        {
          if ((i == 3654) && (str.equals("rx"))) {
            c.add(localUrlDto.getUrl());
          }
        }
        else if (str.equals("dn")) {
          b.add(localUrlDto.getUrl());
        }
      }
      return;
    }
    finally {}
  }
  
  /* Error */
  public final boolean a()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 38	com/truecaller/network/spamUrls/e:d	Lcom/truecaller/network/spamUrls/a;
    //   4: invokeinterface 184 1 0
    //   9: invokeinterface 189 1 0
    //   14: invokestatic 194	com/truecaller/common/h/q:a	(Le/b;)Le/r;
    //   17: astore_2
    //   18: aload_2
    //   19: ifnull +166 -> 185
    //   22: aload_2
    //   23: invokevirtual 198	e/r:d	()Z
    //   26: istore_1
    //   27: aconst_null
    //   28: astore_3
    //   29: iload_1
    //   30: ifeq +6 -> 36
    //   33: goto +5 -> 38
    //   36: aconst_null
    //   37: astore_2
    //   38: aload_2
    //   39: ifnull +146 -> 185
    //   42: aload_2
    //   43: invokevirtual 200	e/r:e	()Ljava/lang/Object;
    //   46: checkcast 119	com/truecaller/messaging/conversation/spamLinks/WhitelistDto
    //   49: astore 4
    //   51: aload 4
    //   53: ifnull +132 -> 185
    //   56: aload 4
    //   58: ldc -54
    //   60: invokestatic 204	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   63: aload_0
    //   64: getfield 55	com/truecaller/network/spamUrls/e:a	Ljava/io/File;
    //   67: astore_2
    //   68: getstatic 98	c/n/d:a	Ljava/nio/charset/Charset;
    //   71: astore 5
    //   73: new 206	java/io/OutputStreamWriter
    //   76: dup
    //   77: new 208	java/io/FileOutputStream
    //   80: dup
    //   81: aload_2
    //   82: invokespecial 209	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   85: checkcast 211	java/io/OutputStream
    //   88: aload 5
    //   90: invokespecial 214	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V
    //   93: checkcast 112	java/io/Closeable
    //   96: astore 5
    //   98: aload_3
    //   99: astore_2
    //   100: aload 5
    //   102: checkcast 206	java/io/OutputStreamWriter
    //   105: astore 6
    //   107: aload_3
    //   108: astore_2
    //   109: new 114	com/google/gson/f
    //   112: dup
    //   113: invokespecial 115	com/google/gson/f:<init>	()V
    //   116: aload 4
    //   118: aload 6
    //   120: checkcast 216	java/lang/Appendable
    //   123: invokevirtual 219	com/google/gson/f:a	(Ljava/lang/Object;Ljava/lang/Appendable;)V
    //   126: aload_3
    //   127: astore_2
    //   128: getstatic 224	c/x:a	Lc/x;
    //   131: astore_3
    //   132: aload 5
    //   134: aconst_null
    //   135: invokestatic 127	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   138: goto +39 -> 177
    //   141: astore_3
    //   142: goto +8 -> 150
    //   145: astore_3
    //   146: aload_3
    //   147: astore_2
    //   148: aload_3
    //   149: athrow
    //   150: aload 5
    //   152: aload_2
    //   153: invokestatic 127	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   156: aload_3
    //   157: athrow
    //   158: astore_2
    //   159: aload_2
    //   160: checkcast 93	java/lang/Throwable
    //   163: invokestatic 132	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   166: goto +11 -> 177
    //   169: astore_2
    //   170: aload_2
    //   171: checkcast 93	java/lang/Throwable
    //   174: invokestatic 132	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   177: aload_0
    //   178: aload 4
    //   180: invokevirtual 226	com/truecaller/network/spamUrls/e:a	(Lcom/truecaller/messaging/conversation/spamLinks/WhitelistDto;)V
    //   183: iconst_1
    //   184: ireturn
    //   185: iconst_0
    //   186: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	187	0	this	e
    //   26	4	1	bool	boolean
    //   17	136	2	localObject1	Object
    //   158	2	2	localException	Exception
    //   169	2	2	localp	com.google.gson.p
    //   28	104	3	localx	c.x
    //   141	1	3	localObject2	Object
    //   145	12	3	localThrowable	Throwable
    //   49	130	4	localWhitelistDto	WhitelistDto
    //   71	80	5	localObject3	Object
    //   105	14	6	localOutputStreamWriter	java.io.OutputStreamWriter
    // Exception table:
    //   from	to	target	type
    //   100	107	141	finally
    //   109	126	141	finally
    //   128	132	141	finally
    //   148	150	141	finally
    //   100	107	145	java/lang/Throwable
    //   109	126	145	java/lang/Throwable
    //   128	132	145	java/lang/Throwable
    //   63	98	158	java/lang/Exception
    //   132	138	158	java/lang/Exception
    //   150	158	158	java/lang/Exception
    //   63	98	169	com/google/gson/p
    //   132	138	169	com/google/gson/p
    //   150	158	169	com/google/gson/p
  }
  
  public final Set<String> b()
  {
    Set localSet1 = b;
    if (localSet1.isEmpty()) {
      localSet1 = null;
    }
    Set localSet2 = localSet1;
    if (localSet1 == null) {
      localSet2 = (Set)g.a();
    }
    return localSet2;
  }
  
  public final Set<String> c()
  {
    return c;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.spamUrls.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */