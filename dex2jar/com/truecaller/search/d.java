package com.truecaller.search;

import com.truecaller.common.g.a;
import com.truecaller.common.h.an;
import javax.inject.Provider;

public final class d
  implements dagger.a.d<b>
{
  private final Provider<an> a;
  private final Provider<a> b;
  
  private d(Provider<an> paramProvider, Provider<a> paramProvider1)
  {
    a = paramProvider;
    b = paramProvider1;
  }
  
  public static d a(Provider<an> paramProvider, Provider<a> paramProvider1)
  {
    return new d(paramProvider, paramProvider1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */