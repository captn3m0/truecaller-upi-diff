package com.truecaller.search;

import com.truecaller.common.g.a;
import com.truecaller.common.h.an;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;

public final class b
{
  private final an a;
  private final a b;
  
  @Inject
  public b(an paraman, a parama)
  {
    a = paraman;
    b = parama;
  }
  
  private static boolean a(int paramInt)
  {
    return ((paramInt & 0x1) != 0) || ((paramInt & 0x40) != 0) || ((paramInt & 0x8) != 0);
  }
  
  private static boolean b(int paramInt)
  {
    return (paramInt & 0x4) != 0;
  }
  
  public final boolean a(long paramLong, int paramInt)
  {
    if (a(paramInt)) {
      return a.a(paramLong, b.a("searchHitTtl", c.a()), TimeUnit.MILLISECONDS);
    }
    if (b(paramInt)) {
      return a.a(paramLong, b.a("searchMissTtl", c.b()), TimeUnit.MILLISECONDS);
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */