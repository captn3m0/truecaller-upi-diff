package com.truecaller.search.local.model;

import com.truecaller.data.entity.Contact;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.presence.a;

public abstract interface c
{
  public abstract a a(Contact paramContact);
  
  public abstract c.a a(Participant paramParticipant);
  
  public abstract c.a a(String... paramVarArgs);
  
  public abstract boolean a();
  
  public abstract c.a b(Contact paramContact);
  
  public abstract void b();
  
  public abstract void c();
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */