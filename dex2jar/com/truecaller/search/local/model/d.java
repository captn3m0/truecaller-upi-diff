package com.truecaller.search.local.model;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import c.a.m;
import c.g.b.k;
import c.u;
import com.truecaller.api.services.presence.v1.models.Availability;
import com.truecaller.api.services.presence.v1.models.Availability.Status;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.h;
import com.truecaller.presence.a;
import com.truecaller.presence.q;
import com.truecaller.util.al;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class d
  extends BroadcastReceiver
  implements c, Runnable
{
  private final Handler a;
  private final ArrayList<d.a> b;
  private boolean c;
  private final g d;
  private final com.truecaller.common.account.r e;
  private final h f;
  private final com.truecaller.androidactors.f<com.truecaller.presence.c> g;
  private final com.truecaller.presence.r h;
  private final bw i;
  private final com.truecaller.voip.d j;
  
  public d(al paramal, g paramg, com.truecaller.common.account.r paramr, h paramh, com.truecaller.androidactors.f<com.truecaller.presence.c> paramf, com.truecaller.presence.r paramr1, bw parambw, com.truecaller.voip.d paramd)
  {
    d = paramg;
    e = paramr;
    f = paramh;
    g = paramf;
    h = paramr1;
    i = parambw;
    j = paramd;
    a = new Handler(Looper.getMainLooper());
    b = new ArrayList();
    paramal.a((BroadcastReceiver)this, new String[] { "com.truecaller.datamanager.STATUSES_CHANGED" });
  }
  
  private a a(String paramString)
  {
    if ((a()) && (paramString != null)) {
      return d.b(paramString);
    }
    return null;
  }
  
  private final void a(long paramLong)
  {
    Handler localHandler = a;
    Runnable localRunnable = (Runnable)this;
    localHandler.removeCallbacks(localRunnable);
    a.postDelayed(localRunnable, paramLong);
  }
  
  private static List<String> c(Contact paramContact)
  {
    paramContact = paramContact.A();
    k.a(paramContact, "numbers");
    Object localObject1 = (Iterable)paramContact;
    paramContact = (Collection)new ArrayList();
    localObject1 = ((Iterable)localObject1).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      Object localObject2 = (Number)((Iterator)localObject1).next();
      k.a(localObject2, "it");
      localObject2 = ((Number)localObject2).a();
      if (localObject2 != null) {
        paramContact.add(localObject2);
      }
    }
    return (List)paramContact;
  }
  
  private final void d()
  {
    if (!e()) {
      return;
    }
    a(h.b());
  }
  
  private final boolean e()
  {
    return (c) && ((a()) || (f.m()) || (i.a()) || (j.a()));
  }
  
  public final a a(Contact paramContact)
  {
    k.b(paramContact, "contact");
    paramContact = ((Collection)c(paramContact)).toArray(new String[0]);
    if (paramContact != null)
    {
      paramContact = (String[])paramContact;
      return b((String[])Arrays.copyOf(paramContact, paramContact.length));
    }
    throw new u("null cannot be cast to non-null type kotlin.Array<T>");
  }
  
  public final c.a a(Participant paramParticipant)
  {
    if ((paramParticipant != null) && (c == 0))
    {
      paramParticipant = f;
      k.a(paramParticipant, "participant.normalizedAddress");
      return a(new String[] { paramParticipant });
    }
    return null;
  }
  
  public final c.a a(String... paramVarArgs)
  {
    k.b(paramVarArgs, "normalizedNumbers");
    int k;
    if (paramVarArgs.length == 0) {
      k = 1;
    } else {
      k = 0;
    }
    if (k != 0) {
      return null;
    }
    d();
    return (c.a)new d.a(this, (String[])Arrays.copyOf(paramVarArgs, paramVarArgs.length));
  }
  
  public final boolean a()
  {
    return (f.j()) || ((f.k()) && (f.l()));
  }
  
  public final a b(String... paramVarArgs)
  {
    k.b(paramVarArgs, "normalizedPhoneNumbers");
    if (!a()) {
      return null;
    }
    int m = paramVarArgs.length;
    int k = 0;
    if (m == 1) {
      return a(paramVarArgs[0]);
    }
    Object localObject1 = q.a();
    m = paramVarArgs.length;
    Object localObject2;
    for (;;)
    {
      localObject2 = localObject1;
      if (k >= m) {
        break;
      }
      Object localObject3 = a(paramVarArgs[k]);
      if (localObject3 == null)
      {
        localObject2 = localObject1;
      }
      else
      {
        localObject2 = localObject1;
        if (b != null)
        {
          localObject1 = q.a((a)localObject1, (a)localObject3);
          localObject2 = localObject1;
          if (b == null) {
            break;
          }
          localObject2 = b;
          if (localObject2 != null) {
            localObject3 = ((Availability)localObject2).a();
          } else {
            localObject3 = null;
          }
          localObject2 = localObject1;
          if (localObject3 == Availability.Status.BUSY) {
            break;
          }
          localObject2 = localObject1;
        }
      }
      k += 1;
      localObject1 = localObject2;
    }
    return (a)localObject2;
  }
  
  public final c.a b(Contact paramContact)
  {
    k.b(paramContact, "contact");
    paramContact = ((Collection)c(paramContact)).toArray(new String[0]);
    if (paramContact != null)
    {
      paramContact = (String[])paramContact;
      return a((String[])Arrays.copyOf(paramContact, paramContact.length));
    }
    throw new u("null cannot be cast to non-null type kotlin.Array<T>");
  }
  
  public final void b()
  {
    c = true;
    d();
  }
  
  public final void c()
  {
    c = false;
  }
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    paramContext = ((Iterable)m.f((Iterable)b)).iterator();
    while (paramContext.hasNext())
    {
      paramIntent = (d.a)paramContext.next();
      if (a())
      {
        String[] arrayOfString = a;
        paramIntent.a(b((String[])Arrays.copyOf(arrayOfString, arrayOfString.length)));
      }
      else
      {
        paramIntent.a(null);
      }
    }
  }
  
  public final void run()
  {
    if (e())
    {
      if (!e.c()) {
        return;
      }
      Object localObject1 = (Iterable)b;
      Collection localCollection = (Collection)new ArrayList();
      localObject1 = ((Iterable)localObject1).iterator();
      while (((Iterator)localObject1).hasNext())
      {
        Object localObject2 = ((Iterator)localObject1).next();
        if (((d.a)localObject2).a()) {
          localCollection.add(localObject2);
        }
      }
      localObject1 = (Iterable)localCollection;
      localCollection = (Collection)new ArrayList();
      localObject1 = ((Iterable)localObject1).iterator();
      while (((Iterator)localObject1).hasNext()) {
        m.a(localCollection, (Iterable)c.a.f.h(nexta));
      }
      localCollection = (Collection)localCollection;
      if ((localCollection.isEmpty() ^ true))
      {
        ((com.truecaller.presence.c)g.a()).a(localCollection);
        a(h.a());
        return;
      }
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */