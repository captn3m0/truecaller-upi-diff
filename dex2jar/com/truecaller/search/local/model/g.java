package com.truecaller.search.local.model;

import android.content.Context;
import com.truecaller.network.search.n;
import com.truecaller.presence.a;
import e.r;
import java.util.Collection;
import java.util.List;
import java.util.SortedSet;

@Deprecated
public abstract class g
  implements a.a
{
  private static volatile g a;
  
  public static g a(Context paramContext)
  {
    Object localObject = a;
    if (localObject == null) {
      try
      {
        g localg = a;
        localObject = localg;
        if (localg == null)
        {
          localObject = new h(paramContext.getApplicationContext());
          a = (g)localObject;
        }
        return (g)localObject;
      }
      finally {}
    }
    return (g)localObject;
  }
  
  @Deprecated
  public abstract j a(long paramLong);
  
  @Deprecated
  public abstract j a(String paramString);
  
  public abstract void a();
  
  public abstract void a(Runnable paramRunnable);
  
  public abstract void a(String paramString, r<n> paramr);
  
  public abstract void a(String paramString, org.a.a.b paramb);
  
  public abstract void a(Collection<a> paramCollection);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract boolean a(long paramLong, String paramString);
  
  public abstract a b(String paramString);
  
  @Deprecated
  public abstract SortedSet<j> b();
  
  @Deprecated
  public abstract com.truecaller.api.services.presence.v1.models.b c(String paramString);
  
  @Deprecated
  public abstract SortedSet<j> c();
  
  public abstract r<n> d(String paramString);
  
  @Deprecated
  public abstract List<j> d();
  
  abstract f e();
  
  @Deprecated
  public abstract SortedSet<j> f();
}

/* Location:
 * Qualified Name:     com.truecaller.search.local.model.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */