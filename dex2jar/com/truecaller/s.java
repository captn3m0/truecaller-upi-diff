package com.truecaller;

import com.truecaller.androidactors.f;
import com.truecaller.calling.k;
import com.truecaller.calling.recorder.CallRecordingManager;
import com.truecaller.common.account.r;
import com.truecaller.common.h.an;
import com.truecaller.common.h.u;
import com.truecaller.featuretoggles.e;
import com.truecaller.filters.FilterManager;
import com.truecaller.multisim.h;
import com.truecaller.util.cn;
import com.truecaller.utils.l;
import javax.inject.Provider;

public final class s
  implements dagger.a.d<k>
{
  private final c a;
  private final Provider<com.truecaller.utils.d> b;
  private final Provider<com.truecaller.i.c> c;
  private final Provider<FilterManager> d;
  private final Provider<r> e;
  private final Provider<an> f;
  private final Provider<cn> g;
  private final Provider<u> h;
  private final Provider<h> i;
  private final Provider<l> j;
  private final Provider<com.truecaller.utils.a> k;
  private final Provider<CallRecordingManager> l;
  private final Provider<e> m;
  private final Provider<f<com.truecaller.callhistory.a>> n;
  private final Provider<com.truecaller.voip.d> o;
  
  private s(c paramc, Provider<com.truecaller.utils.d> paramProvider, Provider<com.truecaller.i.c> paramProvider1, Provider<FilterManager> paramProvider2, Provider<r> paramProvider3, Provider<an> paramProvider4, Provider<cn> paramProvider5, Provider<u> paramProvider6, Provider<h> paramProvider7, Provider<l> paramProvider8, Provider<com.truecaller.utils.a> paramProvider9, Provider<CallRecordingManager> paramProvider10, Provider<e> paramProvider11, Provider<f<com.truecaller.callhistory.a>> paramProvider12, Provider<com.truecaller.voip.d> paramProvider13)
  {
    a = paramc;
    b = paramProvider;
    c = paramProvider1;
    d = paramProvider2;
    e = paramProvider3;
    f = paramProvider4;
    g = paramProvider5;
    h = paramProvider6;
    i = paramProvider7;
    j = paramProvider8;
    k = paramProvider9;
    l = paramProvider10;
    m = paramProvider11;
    n = paramProvider12;
    o = paramProvider13;
  }
  
  public static s a(c paramc, Provider<com.truecaller.utils.d> paramProvider, Provider<com.truecaller.i.c> paramProvider1, Provider<FilterManager> paramProvider2, Provider<r> paramProvider3, Provider<an> paramProvider4, Provider<cn> paramProvider5, Provider<u> paramProvider6, Provider<h> paramProvider7, Provider<l> paramProvider8, Provider<com.truecaller.utils.a> paramProvider9, Provider<CallRecordingManager> paramProvider10, Provider<e> paramProvider11, Provider<f<com.truecaller.callhistory.a>> paramProvider12, Provider<com.truecaller.voip.d> paramProvider13)
  {
    return new s(paramc, paramProvider, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10, paramProvider11, paramProvider12, paramProvider13);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.s
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */