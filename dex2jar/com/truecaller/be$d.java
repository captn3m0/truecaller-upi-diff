package com.truecaller;

import com.truecaller.analytics.d;
import com.truecaller.androidactors.k;
import com.truecaller.callhistory.r;
import com.truecaller.calling.dialer.ax;
import com.truecaller.calling.dialer.bu;
import com.truecaller.calling.dialer.v;
import com.truecaller.calling.recorder.CallRecordingFloatingButton;
import com.truecaller.calling.recorder.CallRecordingFloatingButtonPresenter;
import com.truecaller.calling.recorder.CallRecordingManager;
import com.truecaller.calling.recorder.CallRecordingOnBoardingActivity;
import com.truecaller.calling.recorder.ad;
import com.truecaller.calling.recorder.ag;
import com.truecaller.calling.recorder.ah;
import com.truecaller.calling.recorder.ai;
import com.truecaller.calling.recorder.al;
import com.truecaller.calling.recorder.ar;
import com.truecaller.calling.recorder.as;
import com.truecaller.calling.recorder.au;
import com.truecaller.calling.recorder.ba;
import com.truecaller.calling.recorder.bj;
import com.truecaller.calling.recorder.bk;
import com.truecaller.calling.recorder.bl;
import com.truecaller.calling.recorder.bm;
import com.truecaller.calling.recorder.bp;
import com.truecaller.calling.recorder.bt;
import com.truecaller.calling.recorder.bx;
import com.truecaller.calling.recorder.x;
import com.truecaller.calling.recorder.z;
import com.truecaller.network.search.e;
import com.truecaller.premium.br;
import com.truecaller.startup_dialogs.fragments.CallRecordingOnBoardingDialog;
import com.truecaller.util.af;
import com.truecaller.utils.l;
import com.truecaller.utils.n;
import javax.inject.Provider;

final class be$d
  implements com.truecaller.calling.recorder.b
{
  private Provider<com.truecaller.callhistory.t> b = com.truecaller.callhistory.u.a(be.X(a), be.bb(a));
  private Provider<r> c = dagger.a.c.a(b);
  private Provider<com.truecaller.androidactors.f<r>> d = dagger.a.c.a(ah.a(c, be.K(a)));
  private Provider<bu> e = dagger.a.c.a(com.truecaller.calling.dialer.cd.a());
  private Provider<v> f = dagger.a.c.a(ai.a(be.X(a)));
  private Provider<bm> g = bp.a(d, e, be.at(a), f, be.u(a), be.K(a));
  private Provider<bl> h = dagger.a.c.a(g);
  private Provider<x> i = dagger.a.c.a(z.a());
  private Provider<e> j = dagger.a.c.a(ag.a(be.o(a), h));
  private Provider<as> k;
  private Provider<ar> l;
  
  private be$d(be parambe)
  {
    parambe = be.u(a);
    Provider localProvider1 = be.at(a);
    Provider localProvider2 = h;
    k = au.a(parambe, localProvider1, localProvider2, localProvider2, be.p(a), h, be.u(a));
    l = dagger.a.c.a(k);
  }
  
  public final void a(CallRecordingFloatingButton paramCallRecordingFloatingButton)
  {
    a = new CallRecordingFloatingButtonPresenter((CallRecordingManager)be.u(a).get(), (com.truecaller.utils.a)dagger.a.g.a(be.L(a).d(), "Cannot return null from a non-@Nullable component method"), (c.d.f)dagger.a.g.a(be.f(a).r(), "Cannot return null from a non-@Nullable component method"), (c.d.f)dagger.a.g.a(be.f(a).s(), "Cannot return null from a non-@Nullable component method"), new ba((com.truecaller.callhistory.a)be.k(a).get()), (bx)be.ba(a).get());
  }
  
  public final void a(CallRecordingOnBoardingActivity paramCallRecordingOnBoardingActivity)
  {
    a = new al((com.truecaller.common.g.a)dagger.a.g.a(be.f(a).c(), "Cannot return null from a non-@Nullable component method"), (n)dagger.a.g.a(be.L(a).g(), "Cannot return null from a non-@Nullable component method"), (com.truecaller.utils.a)dagger.a.g.a(be.L(a).d(), "Cannot return null from a non-@Nullable component method"), (com.truecaller.common.f.c)be.p(a).get(), (com.truecaller.analytics.b)dagger.a.g.a(be.I(a).c(), "Cannot return null from a non-@Nullable component method"), (l)dagger.a.g.a(be.L(a).b(), "Cannot return null from a non-@Nullable component method"), (CallRecordingManager)be.u(a).get());
  }
  
  public final void a(bj parambj)
  {
    a = ((bl)h.get());
    b = new ad((bk)h.get(), (com.truecaller.common.h.aj)dagger.a.g.a(be.f(a).q(), "Cannot return null from a non-@Nullable component method"), (n)dagger.a.g.a(be.L(a).g(), "Cannot return null from a non-@Nullable component method"), (af)be.z(a).get(), (ax)be.aC(a).get(), (com.truecaller.calling.dialer.t)h.get(), (com.truecaller.calling.recorder.u)be.aY(a).get(), (x)i.get(), (com.truecaller.calling.recorder.cd)be.aZ(a).get(), (bt)h.get(), (com.truecaller.calling.dialer.a)h.get(), (com.truecaller.analytics.b)dagger.a.g.a(be.I(a).c(), "Cannot return null from a non-@Nullable component method"), (k)be.K(a).get(), (e)j.get(), (c.d.f)dagger.a.g.a(be.f(a).r(), "Cannot return null from a non-@Nullable component method"), (com.truecaller.calling.recorder.g)be.u(a).get(), (bx)be.ba(a).get(), (CallRecordingManager)be.u(a).get(), (c.d.f)dagger.a.g.a(be.f(a).s(), "Cannot return null from a non-@Nullable component method"));
    c = ((ar)l.get());
    d = new br();
    e = new com.truecaller.calling.recorder.aj();
  }
  
  public final void a(CallRecordingOnBoardingDialog paramCallRecordingOnBoardingDialog)
  {
    a = ((CallRecordingManager)be.u(a).get());
    b = ((n)dagger.a.g.a(be.L(a).g(), "Cannot return null from a non-@Nullable component method"));
    c = new br();
    d = new com.truecaller.calling.recorder.aj();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.be.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */