package com.truecaller;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import com.truecaller.callerid.CallerIdService;
import com.truecaller.callerid.aa;
import com.truecaller.callerid.ab;
import com.truecaller.callerid.ac;
import com.truecaller.callerid.ad;
import com.truecaller.callerid.aj;
import com.truecaller.callerid.al;
import com.truecaller.callerid.am;
import com.truecaller.callerid.m;
import com.truecaller.callerid.n;
import com.truecaller.callerid.q;
import com.truecaller.callerid.r;
import com.truecaller.callerid.s;
import com.truecaller.callerid.t;
import com.truecaller.callerid.u;
import com.truecaller.callerid.w;
import com.truecaller.callerid.x;
import com.truecaller.callerid.y;
import com.truecaller.callerid.z;
import dagger.a.c;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import javax.inject.Provider;

final class be$e
  implements m
{
  private Provider<i> b = ac.a(paramq, be.K(a));
  private Provider<i> c = ab.a(paramq, be.K(a));
  private Provider<am> d;
  private Provider<ad> e;
  private Provider<f<ad>> f;
  private Provider<ScheduledThreadPoolExecutor> g;
  private Provider<aj> h;
  private Provider i;
  private Provider<al> j;
  private Provider<com.truecaller.calling.dialer.v> k;
  private Provider<n> l;
  private Provider<f<n>> m;
  
  private be$e(be parambe, q paramq)
  {
    d = aa.a(paramq);
    e = z.a(paramq, be.R(a), be.h(a), be.t(a), d, be.S(a), be.T(a), be.a(a));
    f = c.a(y.a(paramq, c, e));
    g = w.a(paramq);
    h = com.truecaller.callerid.v.a(paramq, g);
    i = r.a(paramq, be.w(a), be.U(a), be.h(a), be.s(a), be.V(a), be.W(a), be.q(a));
    j = c.a(x.a(paramq));
    k = u.a(paramq, be.X(a));
    l = t.a(paramq, b, be.v(a), be.h(a), be.Y(a), be.Z(a), be.aa(a), be.ab(a), be.ac(a), be.U(a), be.R(a), be.ad(a), be.ae(a), be.af(a), f, be.W(a), h, be.V(a), be.S(a), be.r(a), be.w(a), be.ag(a), i, be.C(a), be.ah(a), be.ai(a), j, be.aj(a), be.ak(a), be.K(a), be.u(a), be.al(a), be.am(a), be.y(a), be.a(a), be.O(a), be.t(a), be.an(a), k, be.ao(a));
    m = c.a(s.a(paramq, b, l));
  }
  
  public final void a(CallerIdService paramCallerIdService)
  {
    a = ((f)m.get());
    b = a.aC();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.be.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */