package com.truecaller.tracking.events;

import java.util.Map;
import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;
import org.apache.a.d.f;

public final class ao
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a = new d.q().a("{\"type\":\"record\",\"name\":\"GenericAnalyticsEvent\",\"namespace\":\"com.truecaller.tracking.events\",\"fields\":[{\"name\":\"eventType\",\"type\":\"string\"},{\"name\":\"sessionId\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"properties\",\"type\":[\"null\",{\"type\":\"map\",\"values\":\"string\"}],\"default\":null},{\"name\":\"measures\",\"type\":[\"null\",{\"type\":\"map\",\"values\":\"double\"}],\"default\":null}]}");
  @Deprecated
  public CharSequence b;
  @Deprecated
  public CharSequence c;
  @Deprecated
  public Map<CharSequence, CharSequence> d;
  @Deprecated
  public Map<CharSequence, Double> e;
  
  public static a b()
  {
    return new a((byte)0);
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      throw new a("Bad index");
    case 3: 
      return e;
    case 2: 
      return d;
    case 1: 
      return c;
    }
    return b;
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      throw new a("Bad index");
    case 3: 
      e = ((Map)paramObject);
      return;
    case 2: 
      d = ((Map)paramObject);
      return;
    case 1: 
      c = ((CharSequence)paramObject);
      return;
    }
    b = ((CharSequence)paramObject);
  }
  
  public static final class a
    extends f<ao>
  {
    private CharSequence c;
    private CharSequence d;
    private Map<CharSequence, CharSequence> e;
    private Map<CharSequence, Double> f;
    
    private a()
    {
      super();
    }
    
    public final a a(CharSequence paramCharSequence)
    {
      a(a[0], paramCharSequence);
      c = paramCharSequence;
      b[0] = true;
      return this;
    }
    
    public final a a(Map<CharSequence, CharSequence> paramMap)
    {
      a(a[2], paramMap);
      e = paramMap;
      b[2] = true;
      return this;
    }
    
    public final ao a()
    {
      try
      {
        ao localao = new ao();
        Object localObject;
        if (b[0] != 0) {
          localObject = c;
        } else {
          localObject = (CharSequence)a(a[0]);
        }
        b = ((CharSequence)localObject);
        if (b[1] != 0) {
          localObject = d;
        } else {
          localObject = (CharSequence)a(a[1]);
        }
        c = ((CharSequence)localObject);
        if (b[2] != 0) {
          localObject = e;
        } else {
          localObject = (Map)a(a[2]);
        }
        d = ((Map)localObject);
        if (b[3] != 0) {
          localObject = f;
        } else {
          localObject = (Map)a(a[3]);
        }
        e = ((Map)localObject);
        return localao;
      }
      catch (Exception localException)
      {
        throw new a(localException);
      }
    }
    
    public final a b(CharSequence paramCharSequence)
    {
      a(a[1], paramCharSequence);
      d = paramCharSequence;
      b[1] = true;
      return this;
    }
    
    public final a b(Map<CharSequence, Double> paramMap)
    {
      a(a[3], paramMap);
      f = paramMap;
      b[3] = true;
      return this;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.ao
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */