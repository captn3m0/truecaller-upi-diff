package com.truecaller.tracking.events;

import java.util.Map;
import org.apache.a.a;
import org.apache.a.d.f;

public final class ao$a
  extends f<ao>
{
  private CharSequence c;
  private CharSequence d;
  private Map<CharSequence, CharSequence> e;
  private Map<CharSequence, Double> f;
  
  private ao$a()
  {
    super(ao.a);
  }
  
  public final a a(CharSequence paramCharSequence)
  {
    a(a[0], paramCharSequence);
    c = paramCharSequence;
    b[0] = true;
    return this;
  }
  
  public final a a(Map<CharSequence, CharSequence> paramMap)
  {
    a(a[2], paramMap);
    e = paramMap;
    b[2] = true;
    return this;
  }
  
  public final ao a()
  {
    try
    {
      ao localao = new ao();
      Object localObject;
      if (b[0] != 0) {
        localObject = c;
      } else {
        localObject = (CharSequence)a(a[0]);
      }
      b = ((CharSequence)localObject);
      if (b[1] != 0) {
        localObject = d;
      } else {
        localObject = (CharSequence)a(a[1]);
      }
      c = ((CharSequence)localObject);
      if (b[2] != 0) {
        localObject = e;
      } else {
        localObject = (Map)a(a[2]);
      }
      d = ((Map)localObject);
      if (b[3] != 0) {
        localObject = f;
      } else {
        localObject = (Map)a(a[3]);
      }
      e = ((Map)localObject);
      return localao;
    }
    catch (Exception localException)
    {
      throw new a(localException);
    }
  }
  
  public final a b(CharSequence paramCharSequence)
  {
    a(a[1], paramCharSequence);
    d = paramCharSequence;
    b[1] = true;
    return this;
  }
  
  public final a b(Map<CharSequence, Double> paramMap)
  {
    a(a[3], paramMap);
    f = paramMap;
    b[3] = true;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.ao.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */