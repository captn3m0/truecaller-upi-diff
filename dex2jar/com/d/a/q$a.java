package com.d.a;

import d.c;
import java.net.IDN;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public final class q$a
{
  String a;
  String b = "";
  String c = "";
  String d;
  int e = -1;
  final List<String> f = new ArrayList();
  List<String> g;
  String h;
  
  public q$a()
  {
    f.add("");
  }
  
  private static int a(String paramString, int paramInt)
  {
    int i = 0;
    while (i < paramInt)
    {
      switch (paramString.charAt(i))
      {
      default: 
        return i;
      }
      i += 1;
    }
    return paramInt;
  }
  
  private static String a(byte[] paramArrayOfByte)
  {
    int i1 = 0;
    int i = 0;
    int k = -1;
    int m;
    int n;
    for (int j = 0; i < paramArrayOfByte.length; j = n)
    {
      m = i;
      while ((m < 16) && (paramArrayOfByte[m] == 0) && (paramArrayOfByte[(m + 1)] == 0)) {
        m += 2;
      }
      int i2 = m - i;
      n = j;
      if (i2 > j)
      {
        n = i2;
        k = i;
      }
      i = m + 2;
    }
    c localc = new c();
    i = i1;
    while (i < paramArrayOfByte.length) {
      if (i == k)
      {
        localc.b(58);
        m = i + j;
        i = m;
        if (m == 16)
        {
          localc.b(58);
          i = m;
        }
      }
      else
      {
        if (i > 0) {
          localc.b(58);
        }
        localc.j((paramArrayOfByte[i] & 0xFF) << 8 | paramArrayOfByte[(i + 1)] & 0xFF);
        i += 2;
      }
    }
    return localc.p();
  }
  
  private void a(String paramString, int paramInt1, int paramInt2)
  {
    if (paramInt1 == paramInt2) {
      return;
    }
    int i = paramString.charAt(paramInt1);
    Object localObject;
    if ((i != 47) && (i != 92))
    {
      localObject = f;
      ((List)localObject).set(((List)localObject).size() - 1, "");
    }
    else
    {
      f.clear();
      f.add("");
      paramInt1 += 1;
    }
    while (paramInt1 < paramInt2)
    {
      int j = q.a(paramString, paramInt1, paramInt2, "/\\");
      int k = 0;
      if (j < paramInt2) {
        i = 1;
      } else {
        i = 0;
      }
      localObject = q.a(paramString, paramInt1, j, " \"<>^`{}|/\\?#", true, false, true);
      if ((!((String)localObject).equals(".")) && (!((String)localObject).equalsIgnoreCase("%2e"))) {
        paramInt1 = 0;
      } else {
        paramInt1 = 1;
      }
      if (paramInt1 == 0)
      {
        if ((!((String)localObject).equals("..")) && (!((String)localObject).equalsIgnoreCase("%2e.")) && (!((String)localObject).equalsIgnoreCase(".%2e")))
        {
          paramInt1 = k;
          if (!((String)localObject).equalsIgnoreCase("%2e%2e")) {}
        }
        else
        {
          paramInt1 = 1;
        }
        if (paramInt1 != 0)
        {
          localObject = f;
          if ((((String)((List)localObject).remove(((List)localObject).size() - 1)).isEmpty()) && (!f.isEmpty()))
          {
            localObject = f;
            ((List)localObject).set(((List)localObject).size() - 1, "");
          }
          else
          {
            f.add("");
          }
        }
        else
        {
          List localList = f;
          if (((String)localList.get(localList.size() - 1)).isEmpty())
          {
            localList = f;
            localList.set(localList.size() - 1, localObject);
          }
          else
          {
            f.add(localObject);
          }
          if (i != 0) {
            f.add("");
          }
        }
      }
      paramInt1 = j;
      if (i != 0) {
        paramInt1 = j + 1;
      }
    }
  }
  
  private static int b(String paramString, int paramInt1, int paramInt2)
  {
    paramInt2 -= 1;
    while (paramInt2 >= paramInt1)
    {
      switch (paramString.charAt(paramInt2))
      {
      default: 
        return paramInt2 + 1;
      }
      paramInt2 -= 1;
    }
    return paramInt1;
  }
  
  private static InetAddress b(String paramString, int paramInt)
  {
    byte[] arrayOfByte = new byte[16];
    int m = 1;
    int j = 0;
    int k = -1;
    int i = -1;
    while (m < paramInt)
    {
      if (j == 16) {
        return null;
      }
      int n = m + 2;
      int i2;
      if ((n <= paramInt) && (paramString.regionMatches(m, "::", 0, 2)))
      {
        if (k != -1) {
          return null;
        }
        i = j + 2;
        if (n == paramInt)
        {
          k = i;
          break label463;
        }
        k = i;
        j = n;
        m = i;
        i = j;
      }
      else if (j != 0)
      {
        if (paramString.regionMatches(m, ":", 0, 1))
        {
          i = m + 1;
          m = j;
        }
        else
        {
          if (paramString.regionMatches(m, ".", 0, 1))
          {
            i2 = j - 2;
            n = i2;
            m = i;
            for (;;)
            {
              i1 = 0;
              if (m >= paramInt) {
                break;
              }
              if (n == 16)
              {
                paramInt = i1;
                break label330;
              }
              i = m;
              if (n != i2)
              {
                if (paramString.charAt(m) != '.')
                {
                  paramInt = i1;
                  break label330;
                }
                i = m + 1;
              }
              m = i;
              i1 = 0;
              while (m < paramInt)
              {
                int i3 = paramString.charAt(m);
                if ((i3 < 48) || (i3 > 57)) {
                  break;
                }
                if ((i1 == 0) && (i != m))
                {
                  paramInt = 0;
                  break label330;
                }
                i1 = i1 * 10 + i3 - 48;
                if (i1 > 255)
                {
                  paramInt = 0;
                  break label330;
                }
                m += 1;
              }
              if (m - i == 0)
              {
                paramInt = 0;
                break label330;
              }
              arrayOfByte[n] = ((byte)i1);
              n += 1;
            }
            if (n != i2 + 4) {
              paramInt = 0;
            } else {
              paramInt = 1;
            }
            label330:
            if (paramInt == 0) {
              return null;
            }
            i = j + 2;
            break label463;
          }
          return null;
        }
      }
      else
      {
        i = m;
        m = j;
      }
      j = i;
      n = 0;
      while (j < paramInt)
      {
        i1 = q.a(paramString.charAt(j));
        if (i1 == -1) {
          break;
        }
        n = (n << 4) + i1;
        j += 1;
      }
      int i1 = j - i;
      if (i1 != 0)
      {
        if (i1 > 4) {
          return null;
        }
        i2 = m + 1;
        arrayOfByte[m] = ((byte)(n >>> 8 & 0xFF));
        i1 = i2 + 1;
        arrayOfByte[i2] = ((byte)(n & 0xFF));
        m = j;
        j = i1;
      }
      else
      {
        return null;
      }
    }
    i = j;
    label463:
    if (i != 16)
    {
      if (k == -1) {
        return null;
      }
      paramInt = i - k;
      System.arraycopy(arrayOfByte, k, arrayOfByte, 16 - paramInt, paramInt);
      Arrays.fill(arrayOfByte, k, 16 - i + k, (byte)0);
    }
    try
    {
      paramString = InetAddress.getByAddress(arrayOfByte);
      return paramString;
    }
    catch (UnknownHostException paramString)
    {
      for (;;) {}
    }
    throw new AssertionError();
  }
  
  private static int c(String paramString, int paramInt1, int paramInt2)
  {
    if (paramInt2 - paramInt1 < 2) {
      return -1;
    }
    int j = paramString.charAt(paramInt1);
    int i;
    if (j >= 97)
    {
      i = paramInt1;
      if (j <= 122) {}
    }
    else
    {
      if (j < 65) {
        break label154;
      }
      i = paramInt1;
      if (j > 90) {
        return -1;
      }
    }
    do
    {
      do
      {
        do
        {
          do
          {
            do
            {
              do
              {
                paramInt1 = i + 1;
                if (paramInt1 >= paramInt2) {
                  break label152;
                }
                j = paramString.charAt(paramInt1);
                if (j < 97) {
                  break;
                }
                i = paramInt1;
              } while (j <= 122);
              if (j < 65) {
                break;
              }
              i = paramInt1;
            } while (j <= 90);
            if (j < 48) {
              break;
            }
            i = paramInt1;
          } while (j <= 57);
          i = paramInt1;
        } while (j == 43);
        i = paramInt1;
      } while (j == 45);
      i = paramInt1;
    } while (j == 46);
    if (j == 58) {
      return paramInt1;
    }
    return -1;
    label152:
    return -1;
    label154:
    return -1;
  }
  
  private static int d(String paramString, int paramInt1, int paramInt2)
  {
    int i = 0;
    while (paramInt1 < paramInt2)
    {
      int j = paramString.charAt(paramInt1);
      if ((j != 92) && (j != 47)) {
        break;
      }
      i += 1;
      paramInt1 += 1;
    }
    return i;
  }
  
  private static String d(String paramString)
  {
    for (;;)
    {
      try
      {
        paramString = IDN.toASCII(paramString).toLowerCase(Locale.US);
        if (!paramString.isEmpty()) {
          break label98;
        }
        return null;
      }
      catch (IllegalArgumentException paramString)
      {
        int j;
        int k;
        int m;
        return null;
      }
      j = paramString.length();
      k = 1;
      if (i < j)
      {
        m = paramString.charAt(i);
        j = k;
        if (m > 31) {
          if (m >= 127)
          {
            j = k;
          }
          else
          {
            j = " #%/:?@[\\]".indexOf(m);
            if (j != -1) {
              j = k;
            } else {
              i += 1;
            }
          }
        }
      }
      else
      {
        j = 0;
      }
      if (j != 0) {
        return null;
      }
      return paramString;
      label98:
      int i = 0;
    }
  }
  
  private static int e(String paramString, int paramInt1, int paramInt2)
  {
    while (paramInt1 < paramInt2)
    {
      int j = paramString.charAt(paramInt1);
      if (j != 58)
      {
        int i = paramInt1;
        if (j != 91)
        {
          i = paramInt1;
        }
        else
        {
          do
          {
            paramInt1 = i + 1;
            i = paramInt1;
            if (paramInt1 >= paramInt2) {
              break;
            }
            i = paramInt1;
          } while (paramString.charAt(paramInt1) != ']');
          i = paramInt1;
        }
        paramInt1 = i + 1;
      }
      else
      {
        return paramInt1;
      }
    }
    return paramInt2;
  }
  
  private static String f(String paramString, int paramInt1, int paramInt2)
  {
    paramString = q.a(paramString, paramInt1, paramInt2, false);
    if ((paramString.startsWith("[")) && (paramString.endsWith("]")))
    {
      paramString = b(paramString, paramString.length() - 1);
      if (paramString == null) {
        return null;
      }
      paramString = paramString.getAddress();
      if (paramString.length == 16) {
        return a(paramString);
      }
      throw new AssertionError();
    }
    return d(paramString);
  }
  
  private static int g(String paramString, int paramInt1, int paramInt2)
  {
    try
    {
      paramInt1 = Integer.parseInt(q.a(paramString, paramInt1, paramInt2, "", false, false, true));
      if ((paramInt1 > 0) && (paramInt1 <= 65535)) {
        return paramInt1;
      }
      return -1;
    }
    catch (NumberFormatException paramString) {}
    return -1;
  }
  
  final int a()
  {
    int i = e;
    if (i != -1) {
      return i;
    }
    return q.a(a);
  }
  
  final a a(q paramq, String paramString)
  {
    int i = a(paramString, paramString.length());
    int i1 = b(paramString, i, paramString.length());
    if (c(paramString, i, i1) != -1)
    {
      if (paramString.regionMatches(true, i, "https:", 0, 6))
      {
        a = "https";
        i += 6;
      }
      else if (paramString.regionMatches(true, i, "http:", 0, 5))
      {
        a = "http";
        i += 5;
      }
      else
      {
        return a.c;
      }
    }
    else
    {
      if (paramq == null) {
        break label705;
      }
      a = q.a(paramq);
    }
    int j = d(paramString, i, i1);
    if ((j < 2) && (paramq != null) && (q.a(paramq).equals(a)))
    {
      b = paramq.d();
      c = paramq.e();
      d = q.b(paramq);
      e = q.c(paramq);
      f.clear();
      f.addAll(paramq.g());
      if ((i == i1) || (paramString.charAt(i) == '#')) {
        c(paramq.h());
      }
    }
    else
    {
      j = i + j;
      i = 0;
      int k = 0;
      int n;
      for (;;)
      {
        n = q.a(paramString, j, i1, "@/\\?#");
        int m;
        if (n != i1) {
          m = paramString.charAt(n);
        } else {
          m = -1;
        }
        if ((m == -1) || (m == 35) || (m == 47) || (m == 92)) {
          break;
        }
        switch (m)
        {
        default: 
          break;
        case 64: 
          if (i == 0)
          {
            m = q.a(paramString, j, n, ":");
            String str = q.a(paramString, j, m, " \"':;<=>@[]^`{}|/\\?#", true, false, true);
            paramq = str;
            if (k != 0)
            {
              paramq = new StringBuilder();
              paramq.append(b);
              paramq.append("%40");
              paramq.append(str);
              paramq = paramq.toString();
            }
            b = paramq;
            if (m != n)
            {
              c = q.a(paramString, m + 1, n, " \"':;<=>@[]^`{}|/\\?#", true, false, true);
              i = 1;
            }
            k = 1;
          }
          else
          {
            paramq = new StringBuilder();
            paramq.append(c);
            paramq.append("%40");
            paramq.append(q.a(paramString, j, n, " \"':;<=>@[]^`{}|/\\?#", true, false, true));
            c = paramq.toString();
          }
          j = n + 1;
        }
      }
      i = e(paramString, j, n);
      k = i + 1;
      if (k < n)
      {
        d = f(paramString, j, i);
        e = g(paramString, k, n);
        if (e == -1) {
          return a.d;
        }
      }
      else
      {
        d = f(paramString, j, i);
        e = q.a(a);
      }
      i = n;
      if (d == null) {
        return a.e;
      }
    }
    j = q.a(paramString, i, i1, "?#");
    a(paramString, i, j);
    i = j;
    if (j < i1)
    {
      i = j;
      if (paramString.charAt(j) == '?')
      {
        i = q.a(paramString, j, i1, "#");
        g = q.b(q.a(paramString, j + 1, i, " \"'<>#", true, true, true));
      }
    }
    if ((i < i1) && (paramString.charAt(i) == '#')) {
      h = q.a(paramString, 1 + i, i1, "", true, false, false);
    }
    return a.a;
    label705:
    return a.b;
  }
  
  public final a a(int paramInt)
  {
    if ((paramInt > 0) && (paramInt <= 65535))
    {
      e = paramInt;
      return this;
    }
    throw new IllegalArgumentException("unexpected port: ".concat(String.valueOf(paramInt)));
  }
  
  public final a a(String paramString)
  {
    if (paramString.equalsIgnoreCase("http"))
    {
      a = "http";
      return this;
    }
    if (paramString.equalsIgnoreCase("https"))
    {
      a = "https";
      return this;
    }
    throw new IllegalArgumentException("unexpected scheme: ".concat(String.valueOf(paramString)));
  }
  
  public final a b(String paramString)
  {
    if (paramString != null)
    {
      String str = f(paramString, 0, paramString.length());
      if (str != null)
      {
        d = str;
        return this;
      }
      throw new IllegalArgumentException("unexpected host: ".concat(String.valueOf(paramString)));
    }
    throw new IllegalArgumentException("host == null");
  }
  
  public final q b()
  {
    if (a != null)
    {
      if (d != null) {
        return new q(this, (byte)0);
      }
      throw new IllegalStateException("host == null");
    }
    throw new IllegalStateException("scheme == null");
  }
  
  public final a c(String paramString)
  {
    if (paramString != null) {
      paramString = q.b(q.a(paramString, " \"'<>#", true, true));
    } else {
      paramString = null;
    }
    g = paramString;
    return this;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(a);
    localStringBuilder.append("://");
    if ((!b.isEmpty()) || (!c.isEmpty()))
    {
      localStringBuilder.append(b);
      if (!c.isEmpty())
      {
        localStringBuilder.append(':');
        localStringBuilder.append(c);
      }
      localStringBuilder.append('@');
    }
    if (d.indexOf(':') != -1)
    {
      localStringBuilder.append('[');
      localStringBuilder.append(d);
      localStringBuilder.append(']');
    }
    else
    {
      localStringBuilder.append(d);
    }
    int i = a();
    if (i != q.a(a))
    {
      localStringBuilder.append(':');
      localStringBuilder.append(i);
    }
    q.a(localStringBuilder, f);
    if (g != null)
    {
      localStringBuilder.append('?');
      q.b(localStringBuilder, g);
    }
    if (h != null)
    {
      localStringBuilder.append('#');
      localStringBuilder.append(h);
    }
    return localStringBuilder.toString();
  }
  
  static enum a
  {
    private a() {}
  }
}

/* Location:
 * Qualified Name:     com.d.a.q.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */