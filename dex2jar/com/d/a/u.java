package com.d.a;

import java.io.IOException;

public enum u
{
  private final String e;
  
  private u(String paramString)
  {
    e = paramString;
  }
  
  public static u a(String paramString)
    throws IOException
  {
    if (paramString.equals(ae)) {
      return a;
    }
    if (paramString.equals(be)) {
      return b;
    }
    if (paramString.equals(de)) {
      return d;
    }
    if (paramString.equals(ce)) {
      return c;
    }
    throw new IOException("Unexpected protocol: ".concat(String.valueOf(paramString)));
  }
  
  public final String toString()
  {
    return e;
  }
}

/* Location:
 * Qualified Name:     com.d.a.u
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */