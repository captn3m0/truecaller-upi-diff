package com.d.a;

import com.d.a.a.j;
import java.security.cert.Certificate;
import java.util.Collections;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;

public final class o
{
  public final String a;
  public final List<Certificate> b;
  final List<Certificate> c;
  
  o(String paramString, List<Certificate> paramList1, List<Certificate> paramList2)
  {
    a = paramString;
    b = paramList1;
    c = paramList2;
  }
  
  public static o a(SSLSession paramSSLSession)
  {
    String str = paramSSLSession.getCipherSuite();
    if (str != null) {}
    try
    {
      localObject = paramSSLSession.getPeerCertificates();
    }
    catch (SSLPeerUnverifiedException localSSLPeerUnverifiedException)
    {
      Object localObject;
      for (;;) {}
    }
    localObject = null;
    if (localObject != null) {
      localObject = j.a((Object[])localObject);
    } else {
      localObject = Collections.emptyList();
    }
    paramSSLSession = paramSSLSession.getLocalCertificates();
    if (paramSSLSession != null) {
      paramSSLSession = j.a(paramSSLSession);
    } else {
      paramSSLSession = Collections.emptyList();
    }
    return new o(str, (List)localObject, paramSSLSession);
    throw new IllegalStateException("cipherSuite == null");
  }
  
  public final boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof o)) {
      return false;
    }
    paramObject = (o)paramObject;
    return (a.equals(a)) && (b.equals(b)) && (c.equals(c));
  }
  
  public final int hashCode()
  {
    return ((a.hashCode() + 527) * 31 + b.hashCode()) * 31 + c.hashCode();
  }
}

/* Location:
 * Qualified Name:     com.d.a.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */