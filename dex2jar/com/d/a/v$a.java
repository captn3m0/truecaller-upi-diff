package com.d.a;

import com.d.a.a.b.i;

public final class v$a
{
  q a;
  String b;
  p.a c;
  w d;
  Object e;
  
  public v$a()
  {
    b = "GET";
    c = new p.a();
  }
  
  private v$a(v paramv)
  {
    a = a;
    b = b;
    d = d;
    e = e;
    c = c.a();
  }
  
  public final a a(d paramd)
  {
    paramd = paramd.toString();
    if (paramd.isEmpty()) {
      return b("Cache-Control");
    }
    return a("Cache-Control", paramd);
  }
  
  public final a a(q paramq)
  {
    if (paramq != null)
    {
      a = paramq;
      return this;
    }
    throw new IllegalArgumentException("url == null");
  }
  
  public final a a(String paramString)
  {
    if (paramString != null)
    {
      Object localObject;
      if (paramString.regionMatches(true, 0, "ws:", 0, 3))
      {
        localObject = new StringBuilder("http:");
        ((StringBuilder)localObject).append(paramString.substring(3));
        localObject = ((StringBuilder)localObject).toString();
      }
      else
      {
        localObject = paramString;
        if (paramString.regionMatches(true, 0, "wss:", 0, 4))
        {
          localObject = new StringBuilder("https:");
          ((StringBuilder)localObject).append(paramString.substring(4));
          localObject = ((StringBuilder)localObject).toString();
        }
      }
      paramString = q.c((String)localObject);
      if (paramString != null) {
        return a(paramString);
      }
      throw new IllegalArgumentException("unexpected url: ".concat(String.valueOf(localObject)));
    }
    throw new IllegalArgumentException("url == null");
  }
  
  public final a a(String paramString1, String paramString2)
  {
    c.b(paramString1, paramString2);
    return this;
  }
  
  public final v a()
  {
    if (a != null) {
      return new v(this, (byte)0);
    }
    throw new IllegalStateException("url == null");
  }
  
  public final a b(String paramString)
  {
    c.b(paramString);
    return this;
  }
  
  public final a b(String paramString1, String paramString2)
  {
    c.a(paramString1, paramString2);
    return this;
  }
  
  public final a c(String paramString)
  {
    if ((paramString != null) && (paramString.length() != 0))
    {
      if (!i.b(paramString))
      {
        b = paramString;
        d = null;
        return this;
      }
      StringBuilder localStringBuilder = new StringBuilder("method ");
      localStringBuilder.append(paramString);
      localStringBuilder.append(" must have a request body.");
      throw new IllegalArgumentException(localStringBuilder.toString());
    }
    throw new IllegalArgumentException("method == null || method.length() == 0");
  }
}

/* Location:
 * Qualified Name:     com.d.a.v.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */