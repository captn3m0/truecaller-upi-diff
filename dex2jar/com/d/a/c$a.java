package com.d.a;

import com.d.a.a.b.a;
import com.d.a.a.b.b;
import com.d.a.a.j;
import d.h;
import d.t;
import java.io.IOException;

final class c$a
  implements b
{
  boolean a;
  private final b.a c;
  private t d;
  private t e;
  
  public c$a(final c paramc, final b.a parama)
    throws IOException
  {
    c = parama;
    d = parama.a(1);
    e = new h(d)
    {
      public final void close()
        throws IOException
      {
        synchronized (b)
        {
          if (a) {
            return;
          }
          a = true;
          c localc2 = b;
          b += 1;
          super.close();
          parama.a();
          return;
        }
      }
    };
  }
  
  public final void a()
  {
    synchronized (b)
    {
      if (a) {
        return;
      }
      a = true;
      c localc2 = b;
      c += 1;
      j.a(d);
    }
  }
  
  public final t b()
  {
    return e;
  }
}

/* Location:
 * Qualified Name:     com.d.a.c.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */