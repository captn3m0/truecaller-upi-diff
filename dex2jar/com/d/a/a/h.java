package com.d.a.a;

import com.d.a.a.d.a;
import com.d.a.a.d.e;
import com.d.a.a.d.f;
import com.d.a.u;
import d.c;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

public class h
{
  private static final h a = ;
  
  public static h a()
  {
    return a;
  }
  
  static <T> T a(Object paramObject, Class<T> paramClass, String paramString)
  {
    do
    {
      for (Class localClass = paramObject.getClass(); localClass != Object.class; localClass = localClass.getSuperclass())
      {
        try
        {
          Object localObject = localClass.getDeclaredField(paramString);
          ((Field)localObject).setAccessible(true);
          localObject = ((Field)localObject).get(paramObject);
          if (localObject != null)
          {
            if (!paramClass.isInstance(localObject)) {
              return null;
            }
            localObject = paramClass.cast(localObject);
            return (T)localObject;
          }
          return null;
        }
        catch (NoSuchFieldException localNoSuchFieldException)
        {
          continue;
        }
        catch (IllegalAccessException paramObject)
        {
          for (;;) {}
        }
        throw new AssertionError();
      }
      if (paramString.equals("delegate")) {
        break;
      }
      paramObject = a(paramObject, Object.class, "delegate");
    } while (paramObject != null);
    return null;
  }
  
  public static void a(String paramString)
  {
    System.out.println(paramString);
  }
  
  public static String b()
  {
    return "OkHttp";
  }
  
  private static h c()
  {
    for (;;)
    {
      try
      {
        localObject5 = Class.forName("com.android.org.conscrypt.SSLParametersImpl");
      }
      catch (ClassNotFoundException localClassNotFoundException1)
      {
        Object localObject5;
        g localg1;
        g localg2;
        Object localObject1;
        Object localObject3;
        Object localObject2;
        Object localObject4;
        Object localObject6;
        label186:
        label196:
        label386:
        continue;
      }
      try
      {
        localObject5 = Class.forName("org.apache.harmony.xnet.provider.jsse.SSLParametersImpl");
        localg1 = new g(null, "setUseSessionTickets", new Class[] { Boolean.TYPE });
        localg2 = new g(null, "setHostname", new Class[] { String.class });
      }
      catch (ClassNotFoundException localClassNotFoundException2)
      {
        continue;
      }
      try
      {
        localObject1 = Class.forName("android.net.TrafficStats");
        localObject3 = ((Class)localObject1).getMethod("tagSocket", new Class[] { Socket.class });
      }
      catch (ClassNotFoundException|NoSuchMethodException localClassNotFoundException3)
      {
        continue;
      }
      try
      {
        localObject2 = ((Class)localObject1).getMethod("untagSocket", new Class[] { Socket.class });
      }
      catch (ClassNotFoundException|NoSuchMethodException localClassNotFoundException4)
      {
        continue;
      }
      try
      {
        Class.forName("android.net.Network");
        localObject1 = new g(byte[].class, "getAlpnSelectedProtocol", new Class[0]);
      }
      catch (ClassNotFoundException localClassNotFoundException5)
      {
        continue;
      }
      catch (NoSuchMethodException localNoSuchMethodException1)
      {
        continue;
      }
      try
      {
        localObject4 = new g(null, "setAlpnProtocols", new Class[] { byte[].class });
        localObject6 = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject6;
      }
      catch (ClassNotFoundException localClassNotFoundException8) {}catch (NoSuchMethodException localNoSuchMethodException2) {}
    }
    localObject1 = null;
    break label186;
    localObject1 = null;
    localObject6 = null;
    localObject4 = localObject1;
    localObject1 = localObject2;
    localObject2 = localObject4;
    localObject4 = localObject6;
    break label196;
    localObject2 = null;
    localObject1 = localObject2;
    break label186;
    localObject2 = null;
    localObject3 = localObject2;
    localObject1 = localObject3;
    localObject4 = null;
    localObject6 = localObject2;
    localObject2 = localObject1;
    localObject1 = localObject6;
    localObject1 = new a((Class)localObject5, localg1, localg2, (Method)localObject3, (Method)localObject1, (g)localObject2, (g)localObject4);
    return (h)localObject1;
    try
    {
      localObject1 = Class.forName("sun.security.ssl.SSLContextImpl");
    }
    catch (ClassNotFoundException localClassNotFoundException6)
    {
      for (;;) {}
    }
    try
    {
      localObject2 = Class.forName("org.eclipse.jetty.alpn.ALPN");
      localObject3 = new StringBuilder();
      ((StringBuilder)localObject3).append("org.eclipse.jetty.alpn.ALPN");
      ((StringBuilder)localObject3).append("$Provider");
      localObject3 = Class.forName(((StringBuilder)localObject3).toString());
      localObject4 = new StringBuilder();
      ((StringBuilder)localObject4).append("org.eclipse.jetty.alpn.ALPN");
      ((StringBuilder)localObject4).append("$ClientProvider");
      localObject4 = Class.forName(((StringBuilder)localObject4).toString());
      localObject5 = new StringBuilder();
      ((StringBuilder)localObject5).append("org.eclipse.jetty.alpn.ALPN");
      ((StringBuilder)localObject5).append("$ServerProvider");
      localObject5 = Class.forName(((StringBuilder)localObject5).toString());
      localObject2 = new c((Class)localObject1, ((Class)localObject2).getMethod("put", new Class[] { SSLSocket.class, localObject3 }), ((Class)localObject2).getMethod("get", new Class[] { SSLSocket.class }), ((Class)localObject2).getMethod("remove", new Class[] { SSLSocket.class }), (Class)localObject4, (Class)localObject5);
      return (h)localObject2;
    }
    catch (ClassNotFoundException|NoSuchMethodException localClassNotFoundException7)
    {
      break label386;
    }
    localObject1 = new b((Class)localObject1);
    return (h)localObject1;
    return new h();
  }
  
  public f a(X509TrustManager paramX509TrustManager)
  {
    return new e(paramX509TrustManager.getAcceptedIssuers());
  }
  
  public X509TrustManager a(SSLSocketFactory paramSSLSocketFactory)
  {
    return null;
  }
  
  public void a(Socket paramSocket, InetSocketAddress paramInetSocketAddress, int paramInt)
    throws IOException
  {
    paramSocket.connect(paramInetSocketAddress, paramInt);
  }
  
  public void a(SSLSocket paramSSLSocket) {}
  
  public void a(SSLSocket paramSSLSocket, String paramString, List<u> paramList) {}
  
  public String b(SSLSocket paramSSLSocket)
  {
    return null;
  }
  
  static final class a
    extends h
  {
    private final Class<?> a;
    private final g<Socket> b;
    private final g<Socket> c;
    private final Method d;
    private final Method e;
    private final g<Socket> f;
    private final g<Socket> g;
    
    public a(Class<?> paramClass, g<Socket> paramg1, g<Socket> paramg2, Method paramMethod1, Method paramMethod2, g<Socket> paramg3, g<Socket> paramg4)
    {
      a = paramClass;
      b = paramg1;
      c = paramg2;
      d = paramMethod1;
      e = paramMethod2;
      f = paramg3;
      g = paramg4;
    }
    
    public final f a(X509TrustManager paramX509TrustManager)
    {
      f localf = a.a(paramX509TrustManager);
      if (localf != null) {
        return localf;
      }
      return super.a(paramX509TrustManager);
    }
    
    public final X509TrustManager a(SSLSocketFactory paramSSLSocketFactory)
    {
      Object localObject2 = a(paramSSLSocketFactory, a, "sslParameters");
      Object localObject1 = localObject2;
      if (localObject2 == null) {}
      try
      {
        localObject1 = a(paramSSLSocketFactory, Class.forName("com.google.android.gms.org.conscrypt.SSLParametersImpl", false, paramSSLSocketFactory.getClass().getClassLoader()), "sslParameters");
      }
      catch (ClassNotFoundException paramSSLSocketFactory)
      {
        for (;;) {}
      }
      return null;
      paramSSLSocketFactory = (X509TrustManager)a(localObject1, X509TrustManager.class, "x509TrustManager");
      if (paramSSLSocketFactory != null) {
        return paramSSLSocketFactory;
      }
      return (X509TrustManager)a(localObject1, X509TrustManager.class, "trustManager");
    }
    
    public final void a(Socket paramSocket, InetSocketAddress paramInetSocketAddress, int paramInt)
      throws IOException
    {
      try
      {
        paramSocket.connect(paramInetSocketAddress, paramInt);
        return;
      }
      catch (SecurityException paramSocket)
      {
        paramInetSocketAddress = new IOException("Exception in connect");
        paramInetSocketAddress.initCause(paramSocket);
        throw paramInetSocketAddress;
      }
      catch (AssertionError paramSocket)
      {
        if (j.a(paramSocket)) {
          throw new IOException(paramSocket);
        }
        throw paramSocket;
      }
    }
    
    public final void a(SSLSocket paramSSLSocket, String paramString, List<u> paramList)
    {
      if (paramString != null)
      {
        b.a(paramSSLSocket, new Object[] { Boolean.TRUE });
        c.a(paramSSLSocket, new Object[] { paramString });
      }
      paramString = g;
      if ((paramString != null) && (paramString.a(paramSSLSocket)))
      {
        paramString = new c();
        int j = paramList.size();
        int i = 0;
        while (i < j)
        {
          u localu = (u)paramList.get(i);
          if (localu != u.a)
          {
            paramString.b(localu.toString().length());
            paramString.a(localu.toString());
          }
          i += 1;
        }
        paramString = paramString.r();
        g.b(paramSSLSocket, new Object[] { paramString });
      }
    }
    
    public final String b(SSLSocket paramSSLSocket)
    {
      g localg = f;
      if (localg == null) {
        return null;
      }
      if (!localg.a(paramSSLSocket)) {
        return null;
      }
      paramSSLSocket = (byte[])f.b(paramSSLSocket, new Object[0]);
      if (paramSSLSocket != null) {
        return new String(paramSSLSocket, j.c);
      }
      return null;
    }
  }
  
  static class b
    extends h
  {
    private final Class<?> a;
    
    public b(Class<?> paramClass)
    {
      a = paramClass;
    }
    
    public final X509TrustManager a(SSLSocketFactory paramSSLSocketFactory)
    {
      paramSSLSocketFactory = a(paramSSLSocketFactory, a, "context");
      if (paramSSLSocketFactory == null) {
        return null;
      }
      return (X509TrustManager)a(paramSSLSocketFactory, X509TrustManager.class, "trustManager");
    }
  }
  
  static final class c
    extends h.b
  {
    private final Method a;
    private final Method b;
    private final Method c;
    private final Class<?> d;
    private final Class<?> e;
    
    public c(Class<?> paramClass1, Method paramMethod1, Method paramMethod2, Method paramMethod3, Class<?> paramClass2, Class<?> paramClass3)
    {
      super();
      a = paramMethod1;
      b = paramMethod2;
      c = paramMethod3;
      d = paramClass2;
      e = paramClass3;
    }
    
    public final void a(SSLSocket paramSSLSocket)
    {
      try
      {
        c.invoke(null, new Object[] { paramSSLSocket });
        return;
      }
      catch (IllegalAccessException|InvocationTargetException paramSSLSocket)
      {
        for (;;) {}
      }
      throw new AssertionError();
    }
    
    public final void a(SSLSocket paramSSLSocket, String paramString, List<u> paramList)
    {
      paramString = new ArrayList(paramList.size());
      int j = paramList.size();
      int i = 0;
      Object localObject;
      while (i < j)
      {
        localObject = (u)paramList.get(i);
        if (localObject != u.a) {
          paramString.add(((u)localObject).toString());
        }
        i += 1;
      }
      try
      {
        paramList = h.class.getClassLoader();
        localObject = d;
        Class localClass = e;
        paramString = new h.d(paramString);
        paramString = Proxy.newProxyInstance(paramList, new Class[] { localObject, localClass }, paramString);
        a.invoke(null, new Object[] { paramSSLSocket, paramString });
        return;
      }
      catch (IllegalAccessException paramSSLSocket) {}catch (InvocationTargetException paramSSLSocket) {}
      throw new AssertionError(paramSSLSocket);
    }
    
    public final String b(SSLSocket paramSSLSocket)
    {
      try
      {
        paramSSLSocket = (h.d)Proxy.getInvocationHandler(b.invoke(null, new Object[] { paramSSLSocket }));
        if ((!h.d.a(paramSSLSocket)) && (h.d.b(paramSSLSocket) == null))
        {
          d.a.log(Level.INFO, "ALPN callback dropped: SPDY and HTTP/2 are disabled. Is alpn-boot on the boot class path?");
          return null;
        }
        if (h.d.a(paramSSLSocket)) {
          return null;
        }
        paramSSLSocket = h.d.b(paramSSLSocket);
        return paramSSLSocket;
      }
      catch (InvocationTargetException|IllegalAccessException paramSSLSocket)
      {
        for (;;) {}
      }
      throw new AssertionError();
    }
  }
  
  static final class d
    implements InvocationHandler
  {
    private final List<String> a;
    private boolean b;
    private String c;
    
    public d(List<String> paramList)
    {
      a = paramList;
    }
    
    public final Object invoke(Object paramObject, Method paramMethod, Object[] paramArrayOfObject)
      throws Throwable
    {
      String str = paramMethod.getName();
      Class localClass = paramMethod.getReturnType();
      paramObject = paramArrayOfObject;
      if (paramArrayOfObject == null) {
        paramObject = j.b;
      }
      if ((str.equals("supports")) && (Boolean.TYPE == localClass)) {
        return Boolean.TRUE;
      }
      if ((str.equals("unsupported")) && (Void.TYPE == localClass))
      {
        b = true;
        return null;
      }
      if ((str.equals("protocols")) && (paramObject.length == 0)) {
        return a;
      }
      if (((str.equals("selectProtocol")) || (str.equals("select"))) && (String.class == localClass) && (paramObject.length == 1) && ((paramObject[0] instanceof List)))
      {
        paramObject = (List)paramObject[0];
        int j = ((List)paramObject).size();
        int i = 0;
        while (i < j)
        {
          if (a.contains(((List)paramObject).get(i)))
          {
            paramObject = (String)((List)paramObject).get(i);
            c = ((String)paramObject);
            return paramObject;
          }
          i += 1;
        }
        paramObject = (String)a.get(0);
        c = ((String)paramObject);
        return paramObject;
      }
      if (((str.equals("protocolSelected")) || (str.equals("selected"))) && (paramObject.length == 1))
      {
        c = ((String)paramObject[0]);
        return null;
      }
      return paramMethod.invoke(this, (Object[])paramObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */