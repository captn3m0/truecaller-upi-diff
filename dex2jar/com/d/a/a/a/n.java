package com.d.a.a.a;

public final class n
{
  int a;
  int b;
  int c;
  final int[] d = new int[10];
  
  private boolean c(int paramInt)
  {
    return (1 << paramInt & b) != 0;
  }
  
  private boolean d(int paramInt)
  {
    return (1 << paramInt & c) != 0;
  }
  
  final int a()
  {
    if ((a & 0x2) != 0) {
      return d[1];
    }
    return -1;
  }
  
  final n a(int paramInt1, int paramInt2, int paramInt3)
  {
    if (paramInt1 >= d.length) {
      return this;
    }
    int i = 1 << paramInt1;
    a |= i;
    if ((paramInt2 & 0x1) != 0) {
      b |= i;
    } else {
      b &= (i ^ 0xFFFFFFFF);
    }
    if ((paramInt2 & 0x2) != 0) {
      c |= i;
    } else {
      c &= (i ^ 0xFFFFFFFF);
    }
    d[paramInt1] = paramInt3;
    return this;
  }
  
  final boolean a(int paramInt)
  {
    return (1 << paramInt & a) != 0;
  }
  
  public final int b()
  {
    if ((a & 0x80) != 0) {
      return d[7];
    }
    return 65536;
  }
  
  final int b(int paramInt)
  {
    int i;
    if (d(paramInt)) {
      i = 2;
    } else {
      i = 0;
    }
    int j = i;
    if (c(paramInt)) {
      j = i | 0x1;
    }
    return j;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */