package com.d.a.a.a;

import d.e;
import d.f;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.List;

final class o$a
  implements b
{
  private final e a;
  private final boolean b;
  private final k c;
  
  o$a(e parame, boolean paramBoolean)
  {
    a = parame;
    c = new k(a);
    b = paramBoolean;
  }
  
  private static IOException a(String paramString, Object... paramVarArgs)
    throws IOException
  {
    throw new IOException(String.format(paramString, paramVarArgs));
  }
  
  private void a(b.a parama, int paramInt1, int paramInt2)
    throws IOException
  {
    int i = a.j();
    boolean bool = false;
    if (paramInt2 == i * 8 + 4)
    {
      n localn = new n();
      paramInt2 = 0;
      while (paramInt2 < i)
      {
        int j = a.j();
        localn.a(j & 0xFFFFFF, (0xFF000000 & j) >>> 24, a.j());
        paramInt2 += 1;
      }
      if ((paramInt1 & 0x1) != 0) {
        bool = true;
      }
      parama.a(bool, localn);
      return;
    }
    throw a("TYPE_SETTINGS length: %d != 4 + 8 * %d", new Object[] { Integer.valueOf(paramInt2), Integer.valueOf(i) });
  }
  
  public final void a() {}
  
  public final boolean a(b.a parama)
    throws IOException
  {
    boolean bool1 = false;
    try
    {
      int j = a.j();
      int m = a.j();
      int i;
      if ((0x80000000 & j) != 0) {
        i = 1;
      } else {
        i = 0;
      }
      int k = (0xFF000000 & m) >>> 24;
      m &= 0xFFFFFF;
      if (i != 0)
      {
        i = (0x7FFF0000 & j) >>> 16;
        if (i == 3)
        {
          boolean bool2;
          switch (j & 0xFFFF)
          {
          case 5: 
          default: 
            a.h(m);
            return true;
          case 9: 
            if (m == 8)
            {
              i = a.j();
              long l = a.j() & 0x7FFFFFFF;
              if (l != 0L)
              {
                parama.a(i & 0x7FFFFFFF, l);
                return true;
              }
              throw a("windowSizeIncrement was 0", new Object[] { Long.valueOf(l) });
            }
            throw a("TYPE_WINDOW_UPDATE length: %d != 8", new Object[] { Integer.valueOf(m) });
          case 8: 
            parama.a(false, false, a.j() & 0x7FFFFFFF, c.a(m - 4), g.c);
            return true;
          case 7: 
            if (m == 8)
            {
              i = a.j();
              j = a.j();
              if (a.c(j) != null)
              {
                localObject = f.b;
                parama.a(i & 0x7FFFFFFF);
                return true;
              }
              throw a("TYPE_GOAWAY unexpected error code: %d", new Object[] { Integer.valueOf(j) });
            }
            throw a("TYPE_GOAWAY length: %d != 8", new Object[] { Integer.valueOf(m) });
          case 6: 
            if (m == 4)
            {
              i = a.j();
              bool2 = b;
              if ((i & 0x1) == 1) {
                bool1 = true;
              } else {
                bool1 = false;
              }
              if (bool2 == bool1) {
                bool1 = true;
              } else {
                bool1 = false;
              }
              parama.a(bool1, i, 0);
              return true;
            }
            throw a("TYPE_PING length: %d != 4", new Object[] { Integer.valueOf(m) });
          case 4: 
            a(parama, k, m);
            return true;
          case 3: 
            if (m == 8)
            {
              i = a.j();
              j = a.j();
              localObject = a.a(j);
              if (localObject != null)
              {
                parama.a(i & 0x7FFFFFFF, (a)localObject);
                return true;
              }
              throw a("TYPE_RST_STREAM unexpected error code: %d", new Object[] { Integer.valueOf(j) });
            }
            throw a("TYPE_RST_STREAM length: %d != 8", new Object[] { Integer.valueOf(m) });
          case 2: 
            i = a.j();
            localObject = c.a(m - 4);
            if ((k & 0x1) != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            }
            parama.a(false, bool1, i & 0x7FFFFFFF, (List)localObject, g.b);
            return true;
          }
          i = a.j();
          a.j();
          a.i();
          Object localObject = c.a(m - 10);
          if ((k & 0x1) != 0) {
            bool1 = true;
          } else {
            bool1 = false;
          }
          if ((k & 0x2) != 0) {
            bool2 = true;
          } else {
            bool2 = false;
          }
          parama.a(bool2, bool1, i & 0x7FFFFFFF, (List)localObject, g.a);
          return true;
        }
        throw new ProtocolException("version != 3: ".concat(String.valueOf(i)));
      }
      if ((k & 0x1) != 0) {
        bool1 = true;
      }
      parama.a(bool1, j & 0x7FFFFFFF, a, m);
      return true;
    }
    catch (IOException parama) {}
    return false;
  }
  
  public final void close()
    throws IOException
  {
    c.b.close();
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.o.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */