package com.d.a.a.a;

import java.io.IOException;
import java.net.SocketTimeoutException;

final class e$c
  extends d.a
{
  e$c(e parame) {}
  
  public final IOException a(IOException paramIOException)
  {
    SocketTimeoutException localSocketTimeoutException = new SocketTimeoutException("timeout");
    if (paramIOException != null) {
      localSocketTimeoutException.initCause(paramIOException);
    }
    return localSocketTimeoutException;
  }
  
  public final void a()
  {
    a.b(a.l);
  }
  
  public final void b()
    throws IOException
  {
    if (!n_()) {
      return;
    }
    throw a(null);
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.e.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */