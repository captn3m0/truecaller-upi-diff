package com.d.a.a.a;

import d.c;
import d.t;
import d.u;
import d.v;
import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.util.List;

public final class e
{
  long a = 0L;
  long b;
  final int c;
  final d d;
  List<f> e;
  public final b f;
  final a g;
  public final c h = new c();
  public final c i = new c();
  private final List<f> k;
  private a l = null;
  
  e(int paramInt, d paramd, boolean paramBoolean1, boolean paramBoolean2, List<f> paramList)
  {
    if (paramd != null)
    {
      if (paramList != null)
      {
        c = paramInt;
        d = paramd;
        b = f.b();
        f = new b(e.b(), (byte)0);
        g = new a();
        b.a(f, paramBoolean2);
        a.a(g, paramBoolean1);
        k = paramList;
        return;
      }
      throw new NullPointerException("requestHeaders == null");
    }
    throw new NullPointerException("connection == null");
  }
  
  private boolean d(a parama)
  {
    if ((!j) && (Thread.holdsLock(this))) {
      throw new AssertionError();
    }
    try
    {
      if (l != null) {
        return false;
      }
      if ((b.a(f)) && (a.a(g))) {
        return false;
      }
      l = parama;
      notifyAll();
      d.b(c);
      return true;
    }
    finally {}
  }
  
  private void f()
    throws InterruptedIOException
  {
    try
    {
      wait();
      return;
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;) {}
    }
    throw new InterruptedIOException();
  }
  
  final void a(long paramLong)
  {
    b += paramLong;
    if (paramLong > 0L) {
      notifyAll();
    }
  }
  
  public final void a(a parama)
    throws IOException
  {
    if (!d(parama)) {
      return;
    }
    d.b(c, parama);
  }
  
  public final boolean a()
  {
    try
    {
      Object localObject1 = l;
      if (localObject1 != null) {
        return false;
      }
      if (((b.a(f)) || (b.b(f))) && ((a.a(g)) || (a.b(g))))
      {
        localObject1 = e;
        if (localObject1 != null) {
          return false;
        }
      }
      return true;
    }
    finally {}
  }
  
  public final void b(a parama)
  {
    if (!d(parama)) {
      return;
    }
    d.a(c, parama);
  }
  
  public final boolean b()
  {
    int m;
    if ((c & 0x1) == 1) {
      m = 1;
    } else {
      m = 0;
    }
    return d.b == m;
  }
  
  /* Error */
  public final List<f> c()
    throws IOException
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 54	com/d/a/a/a/e:h	Lcom/d/a/a/a/e$c;
    //   6: invokevirtual 188	com/d/a/a/a/e$c:m_	()V
    //   9: aload_0
    //   10: getfield 180	com/d/a/a/a/e:e	Ljava/util/List;
    //   13: ifnonnull +17 -> 30
    //   16: aload_0
    //   17: getfield 58	com/d/a/a/a/e:l	Lcom/d/a/a/a/a;
    //   20: ifnonnull +10 -> 30
    //   23: aload_0
    //   24: invokespecial 133	com/d/a/a/a/e:f	()V
    //   27: goto -18 -> 9
    //   30: aload_0
    //   31: getfield 54	com/d/a/a/a/e:h	Lcom/d/a/a/a/e$c;
    //   34: invokevirtual 190	com/d/a/a/a/e$c:b	()V
    //   37: aload_0
    //   38: getfield 180	com/d/a/a/a/e:e	Ljava/util/List;
    //   41: ifnull +12 -> 53
    //   44: aload_0
    //   45: getfield 180	com/d/a/a/a/e:e	Ljava/util/List;
    //   48: astore_1
    //   49: aload_0
    //   50: monitorexit
    //   51: aload_1
    //   52: areturn
    //   53: new 156	java/lang/StringBuilder
    //   56: dup
    //   57: ldc -98
    //   59: invokespecial 159	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   62: astore_1
    //   63: aload_1
    //   64: aload_0
    //   65: getfield 58	com/d/a/a/a/e:l	Lcom/d/a/a/a/a;
    //   68: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   71: pop
    //   72: new 142	java/io/IOException
    //   75: dup
    //   76: aload_1
    //   77: invokevirtual 167	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   80: invokespecial 168	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   83: athrow
    //   84: astore_1
    //   85: aload_0
    //   86: getfield 54	com/d/a/a/a/e:h	Lcom/d/a/a/a/e$c;
    //   89: invokevirtual 190	com/d/a/a/a/e$c:b	()V
    //   92: aload_1
    //   93: athrow
    //   94: astore_1
    //   95: aload_0
    //   96: monitorexit
    //   97: aload_1
    //   98: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	99	0	this	e
    //   48	29	1	localObject1	Object
    //   84	9	1	localObject2	Object
    //   94	4	1	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   9	27	84	finally
    //   2	9	94	finally
    //   30	49	94	finally
    //   53	84	94	finally
    //   85	94	94	finally
  }
  
  final void c(a parama)
  {
    try
    {
      if (l == null)
      {
        l = parama;
        notifyAll();
      }
      return;
    }
    finally
    {
      parama = finally;
      throw parama;
    }
  }
  
  public final t d()
  {
    try
    {
      if ((e == null) && (!b())) {
        throw new IllegalStateException("reply before requesting the sink");
      }
      return g;
    }
    finally {}
  }
  
  final void e()
  {
    if ((!j) && (Thread.holdsLock(this))) {
      throw new AssertionError();
    }
    try
    {
      b.a(f, true);
      boolean bool = a();
      notifyAll();
      if (!bool) {
        d.b(c);
      }
      return;
    }
    finally {}
  }
  
  final class a
    implements t
  {
    private final c c = new c();
    private boolean d;
    private boolean e;
    
    a() {}
    
    private void a(boolean paramBoolean)
      throws IOException
    {
      for (;;)
      {
        synchronized (e.this)
        {
          e.g(e.this).m_();
          try
          {
            if ((b <= 0L) && (!e) && (!d) && (e.d(e.this) == null))
            {
              e.e(e.this);
              continue;
            }
            e.g(e.this).b();
            e.h(e.this);
            long l = Math.min(b, c.b);
            e locale = e.this;
            b -= l;
            e.g(e.this).m_();
            try
            {
              ??? = e.a(e.this);
              int i = e.b(e.this);
              if ((!paramBoolean) || (l != c.b)) {
                break label230;
              }
              paramBoolean = true;
              ((d)???).a(i, paramBoolean, c, l);
              return;
            }
            finally
            {
              e.g(e.this).b();
            }
            localObject4 = finally;
          }
          finally
          {
            e.g(e.this).b();
          }
        }
        label230:
        paramBoolean = false;
      }
    }
    
    public final void a_(c paramc, long paramLong)
      throws IOException
    {
      if ((!a) && (Thread.holdsLock(e.this))) {
        throw new AssertionError();
      }
      c.a_(paramc, paramLong);
      while (c.b >= 16384L) {
        a(false);
      }
    }
    
    public final void close()
      throws IOException
    {
      if ((!a) && (Thread.holdsLock(e.this))) {
        throw new AssertionError();
      }
      synchronized (e.this)
      {
        if (d) {
          return;
        }
        if (!g.e)
        {
          if (c.b > 0L) {
            while (c.b > 0L) {
              a(true);
            }
          }
          e.a(e.this).a(e.b(e.this), true, null, 0L);
        }
        synchronized (e.this)
        {
          d = true;
          e.a(e.this).b();
          e.f(e.this);
          return;
        }
      }
    }
    
    public final void flush()
      throws IOException
    {
      if ((!a) && (Thread.holdsLock(e.this))) {
        throw new AssertionError();
      }
      synchronized (e.this)
      {
        e.h(e.this);
        while (c.b > 0L)
        {
          a(false);
          e.a(e.this).b();
        }
        return;
      }
    }
    
    public final v l_()
    {
      return e.g(e.this);
    }
  }
  
  final class b
    implements u
  {
    private final c c = new c();
    private final c d = new c();
    private final long e;
    private boolean f;
    private boolean g;
    
    private b(long paramLong)
    {
      e = paramLong;
    }
    
    private void b()
      throws IOException
    {
      e.c(e.this).m_();
      try
      {
        while ((d.b == 0L) && (!g) && (!f) && (e.d(e.this) == null)) {
          e.e(e.this);
        }
        return;
      }
      finally
      {
        e.c(e.this).b();
      }
    }
    
    public final long a(c arg1, long paramLong)
      throws IOException
    {
      if (paramLong >= 0L) {
        synchronized (e.this)
        {
          b();
          if (!f)
          {
            if (e.d(e.this) == null)
            {
              if (d.b == 0L) {
                return -1L;
              }
              paramLong = d.a(???, Math.min(paramLong, d.b));
              ??? = e.this;
              a += paramLong;
              if (a >= ae.b() / 2)
              {
                e.a(e.this).a(e.b(e.this), a);
                a = 0L;
              }
              synchronized (e.a(e.this))
              {
                ??? = e.a(e.this);
                c += paramLong;
                if (ac >= ae.b() / 2)
                {
                  e.a(e.this).a(0, ac);
                  ac = 0L;
                }
                return paramLong;
              }
            }
            ??? = new StringBuilder("stream was reset: ");
            ???.append(e.d(e.this));
            throw new IOException(???.toString());
          }
          throw new IOException("stream closed");
        }
      }
      throw new IllegalArgumentException("byteCount < 0: ".concat(String.valueOf(paramLong)));
    }
    
    final void a(d.e parame, long paramLong)
      throws IOException
    {
      long l1 = paramLong;
      if (!a) {
        if (!Thread.holdsLock(e.this)) {
          l1 = paramLong;
        } else {
          throw new AssertionError();
        }
      }
      if (l1 > 0L) {}
      for (;;)
      {
        synchronized (e.this)
        {
          boolean bool = g;
          paramLong = d.b;
          long l2 = e;
          int j = 1;
          if (paramLong + l1 <= l2) {
            break label235;
          }
          i = 1;
          if (i != 0)
          {
            parame.h(l1);
            b(a.h);
            return;
          }
          if (bool)
          {
            parame.h(l1);
            return;
          }
          paramLong = parame.a(c, l1);
          if (paramLong != -1L)
          {
            l1 -= paramLong;
            synchronized (e.this)
            {
              if (d.b != 0L) {
                break label241;
              }
              i = j;
              d.a(c);
              if (i != 0) {
                notifyAll();
              }
            }
          }
          throw new EOFException();
        }
        return;
        label235:
        int i = 0;
        continue;
        label241:
        i = 0;
      }
    }
    
    public final void close()
      throws IOException
    {
      synchronized (e.this)
      {
        f = true;
        d.s();
        notifyAll();
        e.f(e.this);
        return;
      }
    }
    
    public final v l_()
    {
      return e.c(e.this);
    }
  }
  
  final class c
    extends d.a
  {
    c() {}
    
    public final IOException a(IOException paramIOException)
    {
      SocketTimeoutException localSocketTimeoutException = new SocketTimeoutException("timeout");
      if (paramIOException != null) {
        localSocketTimeoutException.initCause(paramIOException);
      }
      return localSocketTimeoutException;
    }
    
    public final void a()
    {
      b(a.l);
    }
    
    public final void b()
      throws IOException
    {
      if (!n_()) {
        return;
      }
      throw a(null);
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */