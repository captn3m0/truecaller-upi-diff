package com.d.a.a.a;

import d.e;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

final class i$c
  implements b
{
  final h.a a;
  private final e b;
  private final i.a c;
  private final boolean d;
  
  i$c(e parame, boolean paramBoolean)
  {
    b = parame;
    d = paramBoolean;
    c = new i.a(b);
    a = new h.a(c);
  }
  
  private List<f> a(int paramInt1, short paramShort, byte paramByte, int paramInt2)
    throws IOException
  {
    i.a locala = c;
    d = paramInt1;
    a = paramInt1;
    e = paramShort;
    b = paramByte;
    c = paramInt2;
    a.b();
    return a.c();
  }
  
  private void b()
    throws IOException
  {
    b.j();
    b.h();
  }
  
  public final void a()
    throws IOException
  {
    if (d) {
      return;
    }
    d.f localf = b.d(i.a().h());
    if (i.b().isLoggable(Level.FINE)) {
      i.b().fine(String.format("<< CONNECTION %s", new Object[] { localf.f() }));
    }
    if (i.a().equals(localf)) {
      return;
    }
    throw i.a("Expected a connection header but was %s", new Object[] { localf.a() });
  }
  
  public final boolean a(b.a parama)
    throws IOException
  {
    int i = 0;
    int j = 0;
    int k = 0;
    boolean bool = false;
    try
    {
      b.a(9L);
      int i1 = i.a(b);
      if ((i1 >= 0) && (i1 <= 16384))
      {
        byte b1 = (byte)(b.h() & 0xFF);
        byte b2 = (byte)(b.h() & 0xFF);
        int n = b.j() & 0x7FFFFFFF;
        if (i.b().isLoggable(Level.FINE)) {
          i.b().fine(i.b.a(true, n, i1, b1, b2));
        }
        int m;
        Object localObject;
        switch (b1)
        {
        default: 
          b.h(i1);
          return true;
        case 8: 
          if (i1 == 4)
          {
            long l = b.j() & 0x7FFFFFFF;
            if (l != 0L)
            {
              parama.a(n, l);
              return true;
            }
            throw i.a("windowSizeIncrement was 0", new Object[] { Long.valueOf(l) });
          }
          throw i.a("TYPE_WINDOW_UPDATE length !=4: %s", new Object[] { Integer.valueOf(i1) });
        case 7: 
          if (i1 >= 8)
          {
            if (n == 0)
            {
              m = b.j();
              n = b.j();
              i1 -= 8;
              if (a.b(n) != null)
              {
                localObject = d.f.b;
                if (i1 > 0) {
                  b.d(i1);
                }
                parama.a(m);
                return true;
              }
              throw i.a("TYPE_GOAWAY unexpected error code: %d", new Object[] { Integer.valueOf(n) });
            }
            throw i.a("TYPE_GOAWAY streamId != 0", new Object[0]);
          }
          throw i.a("TYPE_GOAWAY length < 8: %s", new Object[] { Integer.valueOf(i1) });
        case 6: 
          if (i1 == 8)
          {
            if (n == 0)
            {
              m = b.j();
              n = b.j();
              if ((b2 & 0x1) != 0) {
                bool = true;
              }
              parama.a(bool, m, n);
              return true;
            }
            throw i.a("TYPE_PING streamId != 0", new Object[0]);
          }
          throw i.a("TYPE_PING length != 8: %s", new Object[] { Integer.valueOf(i1) });
        case 5: 
          if (n != 0)
          {
            if ((b2 & 0x8) != 0) {
              i = (short)(b.h() & 0xFF);
            }
            parama.a(b.j() & 0x7FFFFFFF, a(i.a(i1 - 4, b2, i), i, b2, n));
            return true;
          }
          throw i.a("PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0", new Object[0]);
        case 4: 
          if (n == 0)
          {
            if ((b2 & 0x1) != 0)
            {
              if (i1 == 0) {
                return true;
              }
              throw i.a("FRAME_SIZE_ERROR ack frame should be empty!", new Object[0]);
            }
            if (i1 % 6 == 0)
            {
              localObject = new n();
              m = 0;
              while (m < i1)
              {
                i = b.i();
                int i2 = b.j();
                n = i;
                switch (i)
                {
                default: 
                  throw i.a("PROTOCOL_ERROR invalid settings id: %s", new Object[] { Short.valueOf(i) });
                case 5: 
                  if ((i2 >= 16384) && (i2 <= 16777215)) {
                    n = i;
                  } else {
                    throw i.a("PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s", new Object[] { Integer.valueOf(i2) });
                  }
                  break;
                case 4: 
                  n = 7;
                  if (i2 < 0) {
                    throw i.a("PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1", new Object[0]);
                  }
                  break;
                case 3: 
                  n = 4;
                  break;
                case 2: 
                  n = i;
                  if (i2 != 0) {
                    if (i2 == 1) {
                      n = i;
                    } else {
                      throw i.a("PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1", new Object[0]);
                    }
                  }
                  break;
                }
                ((n)localObject).a(n, 0, i2);
                m += 6;
              }
              parama.a(false, (n)localObject);
              if (((n)localObject).a() >= 0)
              {
                parama = a;
                m = ((n)localObject).a();
                a = m;
                b = m;
                parama.a();
                return true;
              }
            }
            else
            {
              throw i.a("TYPE_SETTINGS length %% 6 != 0: %s", new Object[] { Integer.valueOf(i1) });
            }
          }
          else
          {
            throw i.a("TYPE_SETTINGS streamId != 0", new Object[0]);
          }
          break;
        case 3: 
          if (i1 == 4)
          {
            if (n != 0)
            {
              m = b.j();
              localObject = a.b(m);
              if (localObject != null)
              {
                parama.a(n, (a)localObject);
                return true;
              }
              throw i.a("TYPE_RST_STREAM unexpected error code: %d", new Object[] { Integer.valueOf(m) });
            }
            throw i.a("TYPE_RST_STREAM streamId == 0", new Object[0]);
          }
          throw i.a("TYPE_RST_STREAM length: %d != 4", new Object[] { Integer.valueOf(i1) });
        case 2: 
          if (i1 == 5)
          {
            if (n != 0)
            {
              b();
              return true;
            }
            throw i.a("TYPE_PRIORITY streamId == 0", new Object[0]);
          }
          throw i.a("TYPE_PRIORITY length: %d != 5", new Object[] { Integer.valueOf(i1) });
        case 1: 
          if (n != 0)
          {
            if ((b2 & 0x1) != 0) {
              bool = true;
            } else {
              bool = false;
            }
            i = j;
            if ((b2 & 0x8) != 0) {
              i = (short)(b.h() & 0xFF);
            }
            m = i1;
            if ((b2 & 0x20) != 0)
            {
              b();
              m = i1 - 5;
            }
            parama.a(false, bool, n, a(i.a(m, b2, i), i, b2, n), g.d);
            return true;
          }
          throw i.a("PROTOCOL_ERROR: TYPE_HEADERS streamId == 0", new Object[0]);
        case 0: 
          if ((b2 & 0x1) != 0) {
            bool = true;
          } else {
            bool = false;
          }
          if ((b2 & 0x20) != 0) {
            m = 1;
          } else {
            m = 0;
          }
          if (m == 0)
          {
            i = k;
            if ((b2 & 0x8) != 0) {
              i = (short)(b.h() & 0xFF);
            }
            m = i.a(i1, b2, i);
            parama.a(bool, n, b, m);
            b.h(i);
            return true;
          }
          throw i.a("PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA", new Object[0]);
        }
        return true;
      }
      throw i.a("FRAME_SIZE_ERROR: %s", new Object[] { Integer.valueOf(i1) });
    }
    catch (IOException parama) {}
    return false;
  }
  
  public final void close()
    throws IOException
  {
    b.close();
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.i.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */