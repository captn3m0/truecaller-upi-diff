package com.d.a.a.a;

import d.d;
import d.e;
import d.u;
import d.v;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class i
  implements p
{
  private static final Logger a = Logger.getLogger(b.class.getName());
  private static final d.f b = d.f.a("PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n");
  
  private static IOException c(String paramString, Object... paramVarArgs)
    throws IOException
  {
    throw new IOException(String.format(paramString, paramVarArgs));
  }
  
  public final b a(e parame, boolean paramBoolean)
  {
    return new c(parame, paramBoolean);
  }
  
  public final c a(d paramd, boolean paramBoolean)
  {
    return new d(paramd, paramBoolean);
  }
  
  static final class a
    implements u
  {
    int a;
    byte b;
    int c;
    int d;
    short e;
    private final e f;
    
    public a(e parame)
    {
      f = parame;
    }
    
    public final long a(d.c paramc, long paramLong)
      throws IOException
    {
      int i;
      byte b1;
      do
      {
        i = d;
        if (i != 0) {
          break label203;
        }
        f.h(e);
        e = 0;
        if ((b & 0x4) != 0) {
          return -1L;
        }
        i = c;
        int j = i.a(f);
        d = j;
        a = j;
        b1 = (byte)(f.h() & 0xFF);
        b = ((byte)(f.h() & 0xFF));
        if (i.b().isLoggable(Level.FINE)) {
          i.b().fine(i.b.a(true, c, a, b1, b));
        }
        c = (f.j() & 0x7FFFFFFF);
        if (b1 != 9) {
          break;
        }
      } while (c == i);
      throw i.a("TYPE_CONTINUATION streamId changed", new Object[0]);
      throw i.a("%s != TYPE_CONTINUATION", new Object[] { Byte.valueOf(b1) });
      label203:
      paramLong = f.a(paramc, Math.min(paramLong, i));
      if (paramLong == -1L) {
        return -1L;
      }
      d = ((int)(d - paramLong));
      return paramLong;
    }
    
    public final void close()
      throws IOException
    {}
    
    public final v l_()
    {
      return f.l_();
    }
  }
  
  static final class b
  {
    private static final String[] a = { "DATA", "HEADERS", "PRIORITY", "RST_STREAM", "SETTINGS", "PUSH_PROMISE", "PING", "GOAWAY", "WINDOW_UPDATE", "CONTINUATION" };
    private static final String[] b = new String[64];
    private static final String[] c = new String['Ā'];
    
    static
    {
      int k = 0;
      int i = 0;
      for (;;)
      {
        localObject = c;
        if (i >= localObject.length) {
          break;
        }
        localObject[i] = String.format("%8s", new Object[] { Integer.toBinaryString(i) }).replace(' ', '0');
        i += 1;
      }
      String[] arrayOfString = b;
      arrayOfString[0] = "";
      arrayOfString[1] = "END_STREAM";
      Object localObject = new int[1];
      localObject[0] = 1;
      arrayOfString[8] = "PADDED";
      i = 0;
      int j;
      StringBuilder localStringBuilder;
      while (i <= 0)
      {
        j = localObject[i];
        arrayOfString = b;
        localStringBuilder = new StringBuilder();
        localStringBuilder.append(b[j]);
        localStringBuilder.append("|PADDED");
        arrayOfString[(j | 0x8)] = localStringBuilder.toString();
        i += 1;
      }
      arrayOfString = b;
      arrayOfString[4] = "END_HEADERS";
      arrayOfString[32] = "PRIORITY";
      arrayOfString[36] = "END_HEADERS|PRIORITY";
      i = 0;
      for (;;)
      {
        j = k;
        if (i >= 3) {
          break;
        }
        int m = new int[] { 4, 32, 36 }[i];
        j = 0;
        while (j <= 0)
        {
          int n = localObject[j];
          arrayOfString = b;
          int i1 = n | m;
          localStringBuilder = new StringBuilder();
          localStringBuilder.append(b[n]);
          localStringBuilder.append('|');
          localStringBuilder.append(b[m]);
          arrayOfString[i1] = localStringBuilder.toString();
          arrayOfString = b;
          localStringBuilder = new StringBuilder();
          localStringBuilder.append(b[n]);
          localStringBuilder.append('|');
          localStringBuilder.append(b[m]);
          localStringBuilder.append("|PADDED");
          arrayOfString[(i1 | 0x8)] = localStringBuilder.toString();
          j += 1;
        }
        i += 1;
      }
      for (;;)
      {
        localObject = b;
        if (j >= localObject.length) {
          break;
        }
        if (localObject[j] == null) {
          localObject[j] = c[j];
        }
        j += 1;
      }
    }
    
    static String a(boolean paramBoolean, int paramInt1, int paramInt2, byte paramByte1, byte paramByte2)
    {
      Object localObject = a;
      String str1;
      if (paramByte1 < localObject.length) {
        str1 = localObject[paramByte1];
      } else {
        str1 = String.format("0x%02x", new Object[] { Byte.valueOf(paramByte1) });
      }
      if (paramByte2 == 0)
      {
        localObject = "";
      }
      else
      {
        switch (paramByte1)
        {
        case 5: 
        default: 
          localObject = b;
          if (paramByte2 < localObject.length) {
            localObject = localObject[paramByte2];
          }
          break;
        case 4: 
        case 6: 
          if (paramByte2 == 1) {
            localObject = "ACK";
          } else {
            localObject = c[paramByte2];
          }
          break;
        case 2: 
        case 3: 
        case 7: 
        case 8: 
          localObject = c[paramByte2];
          break;
        }
        localObject = c[paramByte2];
        if ((paramByte1 == 5) && ((paramByte2 & 0x4) != 0)) {
          localObject = ((String)localObject).replace("HEADERS", "PUSH_PROMISE");
        } else if ((paramByte1 == 0) && ((paramByte2 & 0x20) != 0)) {
          localObject = ((String)localObject).replace("PRIORITY", "COMPRESSED");
        }
      }
      String str2;
      if (paramBoolean) {
        str2 = "<<";
      } else {
        str2 = ">>";
      }
      return String.format("%s 0x%08x %5d %-13s %s", new Object[] { str2, Integer.valueOf(paramInt1), Integer.valueOf(paramInt2), str1, localObject });
    }
  }
  
  static final class c
    implements b
  {
    final h.a a;
    private final e b;
    private final i.a c;
    private final boolean d;
    
    c(e parame, boolean paramBoolean)
    {
      b = parame;
      d = paramBoolean;
      c = new i.a(b);
      a = new h.a(c);
    }
    
    private List<f> a(int paramInt1, short paramShort, byte paramByte, int paramInt2)
      throws IOException
    {
      i.a locala = c;
      d = paramInt1;
      a = paramInt1;
      e = paramShort;
      b = paramByte;
      c = paramInt2;
      a.b();
      return a.c();
    }
    
    private void b()
      throws IOException
    {
      b.j();
      b.h();
    }
    
    public final void a()
      throws IOException
    {
      if (d) {
        return;
      }
      d.f localf = b.d(i.a().h());
      if (i.b().isLoggable(Level.FINE)) {
        i.b().fine(String.format("<< CONNECTION %s", new Object[] { localf.f() }));
      }
      if (i.a().equals(localf)) {
        return;
      }
      throw i.a("Expected a connection header but was %s", new Object[] { localf.a() });
    }
    
    public final boolean a(b.a parama)
      throws IOException
    {
      int i = 0;
      int j = 0;
      int k = 0;
      boolean bool = false;
      try
      {
        b.a(9L);
        int i1 = i.a(b);
        if ((i1 >= 0) && (i1 <= 16384))
        {
          byte b1 = (byte)(b.h() & 0xFF);
          byte b2 = (byte)(b.h() & 0xFF);
          int n = b.j() & 0x7FFFFFFF;
          if (i.b().isLoggable(Level.FINE)) {
            i.b().fine(i.b.a(true, n, i1, b1, b2));
          }
          int m;
          Object localObject;
          switch (b1)
          {
          default: 
            b.h(i1);
            return true;
          case 8: 
            if (i1 == 4)
            {
              long l = b.j() & 0x7FFFFFFF;
              if (l != 0L)
              {
                parama.a(n, l);
                return true;
              }
              throw i.a("windowSizeIncrement was 0", new Object[] { Long.valueOf(l) });
            }
            throw i.a("TYPE_WINDOW_UPDATE length !=4: %s", new Object[] { Integer.valueOf(i1) });
          case 7: 
            if (i1 >= 8)
            {
              if (n == 0)
              {
                m = b.j();
                n = b.j();
                i1 -= 8;
                if (a.b(n) != null)
                {
                  localObject = d.f.b;
                  if (i1 > 0) {
                    b.d(i1);
                  }
                  parama.a(m);
                  return true;
                }
                throw i.a("TYPE_GOAWAY unexpected error code: %d", new Object[] { Integer.valueOf(n) });
              }
              throw i.a("TYPE_GOAWAY streamId != 0", new Object[0]);
            }
            throw i.a("TYPE_GOAWAY length < 8: %s", new Object[] { Integer.valueOf(i1) });
          case 6: 
            if (i1 == 8)
            {
              if (n == 0)
              {
                m = b.j();
                n = b.j();
                if ((b2 & 0x1) != 0) {
                  bool = true;
                }
                parama.a(bool, m, n);
                return true;
              }
              throw i.a("TYPE_PING streamId != 0", new Object[0]);
            }
            throw i.a("TYPE_PING length != 8: %s", new Object[] { Integer.valueOf(i1) });
          case 5: 
            if (n != 0)
            {
              if ((b2 & 0x8) != 0) {
                i = (short)(b.h() & 0xFF);
              }
              parama.a(b.j() & 0x7FFFFFFF, a(i.a(i1 - 4, b2, i), i, b2, n));
              return true;
            }
            throw i.a("PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0", new Object[0]);
          case 4: 
            if (n == 0)
            {
              if ((b2 & 0x1) != 0)
              {
                if (i1 == 0) {
                  return true;
                }
                throw i.a("FRAME_SIZE_ERROR ack frame should be empty!", new Object[0]);
              }
              if (i1 % 6 == 0)
              {
                localObject = new n();
                m = 0;
                while (m < i1)
                {
                  i = b.i();
                  int i2 = b.j();
                  n = i;
                  switch (i)
                  {
                  default: 
                    throw i.a("PROTOCOL_ERROR invalid settings id: %s", new Object[] { Short.valueOf(i) });
                  case 5: 
                    if ((i2 >= 16384) && (i2 <= 16777215)) {
                      n = i;
                    } else {
                      throw i.a("PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s", new Object[] { Integer.valueOf(i2) });
                    }
                    break;
                  case 4: 
                    n = 7;
                    if (i2 < 0) {
                      throw i.a("PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1", new Object[0]);
                    }
                    break;
                  case 3: 
                    n = 4;
                    break;
                  case 2: 
                    n = i;
                    if (i2 != 0) {
                      if (i2 == 1) {
                        n = i;
                      } else {
                        throw i.a("PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1", new Object[0]);
                      }
                    }
                    break;
                  }
                  ((n)localObject).a(n, 0, i2);
                  m += 6;
                }
                parama.a(false, (n)localObject);
                if (((n)localObject).a() >= 0)
                {
                  parama = a;
                  m = ((n)localObject).a();
                  a = m;
                  b = m;
                  parama.a();
                  return true;
                }
              }
              else
              {
                throw i.a("TYPE_SETTINGS length %% 6 != 0: %s", new Object[] { Integer.valueOf(i1) });
              }
            }
            else
            {
              throw i.a("TYPE_SETTINGS streamId != 0", new Object[0]);
            }
            break;
          case 3: 
            if (i1 == 4)
            {
              if (n != 0)
              {
                m = b.j();
                localObject = a.b(m);
                if (localObject != null)
                {
                  parama.a(n, (a)localObject);
                  return true;
                }
                throw i.a("TYPE_RST_STREAM unexpected error code: %d", new Object[] { Integer.valueOf(m) });
              }
              throw i.a("TYPE_RST_STREAM streamId == 0", new Object[0]);
            }
            throw i.a("TYPE_RST_STREAM length: %d != 4", new Object[] { Integer.valueOf(i1) });
          case 2: 
            if (i1 == 5)
            {
              if (n != 0)
              {
                b();
                return true;
              }
              throw i.a("TYPE_PRIORITY streamId == 0", new Object[0]);
            }
            throw i.a("TYPE_PRIORITY length: %d != 5", new Object[] { Integer.valueOf(i1) });
          case 1: 
            if (n != 0)
            {
              if ((b2 & 0x1) != 0) {
                bool = true;
              } else {
                bool = false;
              }
              i = j;
              if ((b2 & 0x8) != 0) {
                i = (short)(b.h() & 0xFF);
              }
              m = i1;
              if ((b2 & 0x20) != 0)
              {
                b();
                m = i1 - 5;
              }
              parama.a(false, bool, n, a(i.a(m, b2, i), i, b2, n), g.d);
              return true;
            }
            throw i.a("PROTOCOL_ERROR: TYPE_HEADERS streamId == 0", new Object[0]);
          case 0: 
            if ((b2 & 0x1) != 0) {
              bool = true;
            } else {
              bool = false;
            }
            if ((b2 & 0x20) != 0) {
              m = 1;
            } else {
              m = 0;
            }
            if (m == 0)
            {
              i = k;
              if ((b2 & 0x8) != 0) {
                i = (short)(b.h() & 0xFF);
              }
              m = i.a(i1, b2, i);
              parama.a(bool, n, b, m);
              b.h(i);
              return true;
            }
            throw i.a("PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA", new Object[0]);
          }
          return true;
        }
        throw i.a("FRAME_SIZE_ERROR: %s", new Object[] { Integer.valueOf(i1) });
      }
      catch (IOException parama) {}
      return false;
    }
    
    public final void close()
      throws IOException
    {
      b.close();
    }
  }
  
  static final class d
    implements c
  {
    private final d a;
    private final boolean b;
    private final d.c c;
    private final h.b d;
    private int e;
    private boolean f;
    
    d(d paramd, boolean paramBoolean)
    {
      a = paramd;
      b = paramBoolean;
      c = new d.c();
      d = new h.b(c);
      e = 16384;
    }
    
    private void a(int paramInt1, int paramInt2, byte paramByte1, byte paramByte2)
      throws IOException
    {
      if (i.b().isLoggable(Level.FINE)) {
        i.b().fine(i.b.a(false, paramInt1, paramInt2, paramByte1, paramByte2));
      }
      int i = e;
      if (paramInt2 <= i)
      {
        if ((0x80000000 & paramInt1) == 0)
        {
          i.a(a, paramInt2);
          a.j(paramByte1 & 0xFF);
          a.j(paramByte2 & 0xFF);
          a.h(paramInt1 & 0x7FFFFFFF);
          return;
        }
        throw i.b("reserved bit set: %s", new Object[] { Integer.valueOf(paramInt1) });
      }
      throw i.b("FRAME_SIZE_ERROR length > %d: %d", new Object[] { Integer.valueOf(i), Integer.valueOf(paramInt2) });
    }
    
    private void b(int paramInt, long paramLong)
      throws IOException
    {
      while (paramLong > 0L)
      {
        int i = (int)Math.min(e, paramLong);
        long l = i;
        paramLong -= l;
        byte b1;
        if (paramLong == 0L) {
          b1 = 4;
        } else {
          b1 = 0;
        }
        a(paramInt, i, (byte)9, b1);
        a.a_(c, l);
      }
    }
    
    public final void a()
      throws IOException
    {
      try
      {
        if (!f)
        {
          boolean bool = b;
          if (!bool) {
            return;
          }
          if (i.b().isLoggable(Level.FINE)) {
            i.b().fine(String.format(">> CONNECTION %s", new Object[] { i.a().f() }));
          }
          a.c(i.a().i());
          a.flush();
          return;
        }
        throw new IOException("closed");
      }
      finally {}
    }
    
    public final void a(int paramInt, long paramLong)
      throws IOException
    {
      try
      {
        if (!f)
        {
          if ((paramLong != 0L) && (paramLong <= 2147483647L))
          {
            a(paramInt, 4, (byte)8, (byte)0);
            a.h((int)paramLong);
            a.flush();
            return;
          }
          throw i.b("windowSizeIncrement == 0 || windowSizeIncrement > 0x7fffffffL: %s", new Object[] { Long.valueOf(paramLong) });
        }
        throw new IOException("closed");
      }
      finally {}
    }
    
    public final void a(int paramInt, a parama)
      throws IOException
    {
      try
      {
        if (!f)
        {
          if (s != -1)
          {
            a(paramInt, 4, (byte)3, (byte)0);
            a.h(s);
            a.flush();
            return;
          }
          throw new IllegalArgumentException();
        }
        throw new IOException("closed");
      }
      finally {}
    }
    
    public final void a(int paramInt, a parama, byte[] paramArrayOfByte)
      throws IOException
    {
      try
      {
        if (!f)
        {
          if (s != -1)
          {
            a(0, paramArrayOfByte.length + 8, (byte)7, (byte)0);
            a.h(paramInt);
            a.h(s);
            if (paramArrayOfByte.length > 0) {
              a.c(paramArrayOfByte);
            }
            a.flush();
            return;
          }
          throw i.b("errorCode.httpCode == -1", new Object[0]);
        }
        throw new IOException("closed");
      }
      finally {}
    }
    
    public final void a(n paramn)
      throws IOException
    {
      try
      {
        if (!f)
        {
          int i = e;
          if ((a & 0x20) != 0) {
            i = d[5];
          }
          e = i;
          a(0, 0, (byte)4, (byte)1);
          a.flush();
          return;
        }
        throw new IOException("closed");
      }
      finally {}
    }
    
    public final void a(boolean paramBoolean, int paramInt1, int paramInt2)
      throws IOException
    {
      for (;;)
      {
        try
        {
          if (!f)
          {
            if (paramBoolean)
            {
              b1 = 1;
              a(0, 8, (byte)6, b1);
              a.h(paramInt1);
              a.h(paramInt2);
              a.flush();
            }
          }
          else {
            throw new IOException("closed");
          }
        }
        finally {}
        byte b1 = 0;
      }
    }
    
    public final void a(boolean paramBoolean, int paramInt1, d.c paramc, int paramInt2)
      throws IOException
    {
      throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
    }
    
    public final void a(boolean paramBoolean, int paramInt, List<f> paramList)
      throws IOException
    {
      for (;;)
      {
        try
        {
          if (!f)
          {
            if (!f)
            {
              d.a(paramList);
              long l1 = c.b;
              int i = (int)Math.min(e, l1);
              long l2 = i;
              if (l1 == l2)
              {
                b1 = 4;
                break label139;
                a(paramInt, i, (byte)1, b2);
                a.a_(c, l2);
                if (l1 > l2) {
                  b(paramInt, l1 - l2);
                }
              }
            }
            else
            {
              throw new IOException("closed");
            }
          }
          else {
            throw new IOException("closed");
          }
        }
        finally {}
        byte b1 = 0;
        label139:
        byte b2 = b1;
        if (paramBoolean) {
          b2 = (byte)(b1 | 0x1);
        }
      }
    }
    
    public final void b()
      throws IOException
    {
      try
      {
        if (!f)
        {
          a.flush();
          return;
        }
        throw new IOException("closed");
      }
      finally {}
    }
    
    public final void b(n paramn)
      throws IOException
    {
      for (;;)
      {
        int j;
        int i;
        try
        {
          if (!f)
          {
            j = Integer.bitCount(a);
            i = 0;
            a(0, j * 6, (byte)4, (byte)0);
            if (i < 10)
            {
              if (!paramn.a(i)) {
                break label127;
              }
              if (i == 4)
              {
                j = 3;
                a.i(j);
                a.h(d[i]);
                break label127;
              }
            }
            else
            {
              a.flush();
            }
          }
          else
          {
            throw new IOException("closed");
          }
        }
        finally {}
        if (i == 7)
        {
          j = 4;
        }
        else
        {
          j = i;
          continue;
          label127:
          i += 1;
        }
      }
    }
    
    public final int c()
    {
      return e;
    }
    
    public final void close()
      throws IOException
    {
      try
      {
        f = true;
        a.close();
        return;
      }
      finally
      {
        localObject = finally;
        throw ((Throwable)localObject);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */