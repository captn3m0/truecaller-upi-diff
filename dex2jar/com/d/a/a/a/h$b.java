package com.d.a.a.a;

import d.c;
import java.io.IOException;
import java.util.List;
import java.util.Map;

final class h$b
{
  private final c a;
  
  h$b(c paramc)
  {
    a = paramc;
  }
  
  private void a(int paramInt1, int paramInt2)
    throws IOException
  {
    if (paramInt1 < paramInt2)
    {
      a.b(paramInt1 | 0x0);
      return;
    }
    a.b(paramInt2 | 0x0);
    paramInt1 -= paramInt2;
    while (paramInt1 >= 128)
    {
      a.b(0x80 | paramInt1 & 0x7F);
      paramInt1 >>>= 7;
    }
    a.b(paramInt1);
  }
  
  private void a(d.f paramf)
    throws IOException
  {
    a(paramf.h(), 127);
    a.a(paramf);
  }
  
  final void a(List<f> paramList)
    throws IOException
  {
    int j = paramList.size();
    int i = 0;
    while (i < j)
    {
      d.f localf = geth.g();
      Integer localInteger = (Integer)h.b().get(localf);
      if (localInteger != null)
      {
        a(localInteger.intValue() + 1, 15);
        a(geti);
      }
      else
      {
        a.b(0);
        a(localf);
        a(geti);
      }
      i += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.h.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */