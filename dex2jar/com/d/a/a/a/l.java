package com.d.a.a.a;

import java.util.concurrent.CountDownLatch;

public final class l
{
  final CountDownLatch a = new CountDownLatch(1);
  long b = -1L;
  long c = -1L;
  
  final void a()
  {
    if (c == -1L)
    {
      long l = b;
      if (l != -1L)
      {
        c = (l - 1L);
        a.countDown();
        return;
      }
    }
    throw new IllegalStateException();
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */