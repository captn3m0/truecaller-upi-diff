package com.d.a.a.a;

public final class f
{
  public static final d.f a = d.f.a(":status");
  public static final d.f b = d.f.a(":method");
  public static final d.f c = d.f.a(":path");
  public static final d.f d = d.f.a(":scheme");
  public static final d.f e = d.f.a(":authority");
  public static final d.f f = d.f.a(":host");
  public static final d.f g = d.f.a(":version");
  public final d.f h;
  public final d.f i;
  final int j;
  
  public f(d.f paramf1, d.f paramf2)
  {
    h = paramf1;
    i = paramf2;
    j = (paramf1.h() + 32 + paramf2.h());
  }
  
  public f(d.f paramf, String paramString)
  {
    this(paramf, d.f.a(paramString));
  }
  
  public f(String paramString1, String paramString2)
  {
    this(d.f.a(paramString1), d.f.a(paramString2));
  }
  
  public final boolean equals(Object paramObject)
  {
    if ((paramObject instanceof f))
    {
      paramObject = (f)paramObject;
      return (h.equals(h)) && (i.equals(i));
    }
    return false;
  }
  
  public final int hashCode()
  {
    return (h.hashCode() + 527) * 31 + i.hashCode();
  }
  
  public final String toString()
  {
    return String.format("%s: %s", new Object[] { h.a(), i.a() });
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */