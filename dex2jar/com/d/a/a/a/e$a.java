package com.d.a.a.a;

import d.c;
import d.t;
import d.v;
import java.io.IOException;

final class e$a
  implements t
{
  private final c c = new c();
  private boolean d;
  private boolean e;
  
  e$a(e parame) {}
  
  private void a(boolean paramBoolean)
    throws IOException
  {
    for (;;)
    {
      synchronized (b)
      {
        e.g(b).m_();
        try
        {
          if ((b.b <= 0L) && (!e) && (!d) && (e.d(b) == null))
          {
            e.e(b);
            continue;
          }
          e.g(b).b();
          e.h(b);
          long l = Math.min(b.b, c.b);
          e locale = b;
          b -= l;
          e.g(b).m_();
          try
          {
            ??? = e.a(b);
            int i = e.b(b);
            if ((!paramBoolean) || (l != c.b)) {
              break label230;
            }
            paramBoolean = true;
            ((d)???).a(i, paramBoolean, c, l);
            return;
          }
          finally
          {
            e.g(b).b();
          }
          localObject4 = finally;
        }
        finally
        {
          e.g(b).b();
        }
      }
      label230:
      paramBoolean = false;
    }
  }
  
  public final void a_(c paramc, long paramLong)
    throws IOException
  {
    if ((!a) && (Thread.holdsLock(b))) {
      throw new AssertionError();
    }
    c.a_(paramc, paramLong);
    while (c.b >= 16384L) {
      a(false);
    }
  }
  
  public final void close()
    throws IOException
  {
    if ((!a) && (Thread.holdsLock(b))) {
      throw new AssertionError();
    }
    synchronized (b)
    {
      if (d) {
        return;
      }
      if (!b.g.e)
      {
        if (c.b > 0L) {
          while (c.b > 0L) {
            a(true);
          }
        }
        e.a(b).a(e.b(b), true, null, 0L);
      }
      synchronized (b)
      {
        d = true;
        e.a(b).b();
        e.f(b);
        return;
      }
    }
  }
  
  public final void flush()
    throws IOException
  {
    if ((!a) && (Thread.holdsLock(b))) {
      throw new AssertionError();
    }
    synchronized (b)
    {
      e.h(b);
      while (c.b > 0L)
      {
        a(false);
        e.a(b).b();
      }
      return;
    }
  }
  
  public final v l_()
  {
    return e.g(b);
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.e.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */