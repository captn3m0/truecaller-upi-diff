package com.d.a.a.a;

import com.d.a.a.j;
import com.d.a.u;
import java.io.Closeable;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class d
  implements Closeable
{
  private static final ExecutorService l = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60L, TimeUnit.SECONDS, new SynchronousQueue(), j.b("OkHttp FramedConnection"));
  public final u a;
  final boolean b;
  long c = 0L;
  long d;
  public n e = new n();
  final n f = new n();
  final p g;
  final Socket h;
  public final c i;
  final c j;
  private final b m;
  private final Map<Integer, e> n = new HashMap();
  private final String o;
  private int p;
  private int q;
  private boolean r;
  private long s = System.nanoTime();
  private final ExecutorService t;
  private Map<Integer, l> u;
  private final m v;
  private int w;
  private boolean x = false;
  private final Set<Integer> y = new LinkedHashSet();
  
  private d(a parama)
    throws IOException
  {
    a = f;
    v = g;
    b = h;
    m = e;
    boolean bool = h;
    int i2 = 2;
    if (bool) {
      i1 = 1;
    } else {
      i1 = 2;
    }
    q = i1;
    if ((h) && (a == u.d)) {
      q += 2;
    }
    int i1 = i2;
    if (h) {
      i1 = 1;
    }
    w = i1;
    if (h) {
      e.a(7, 0, 16777216);
    }
    o = b;
    if (a == u.d)
    {
      g = new i();
      t = new ThreadPoolExecutor(0, 1, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue(), j.b(String.format("OkHttp %s Push Observer", new Object[] { o })));
      f.a(7, 0, 65535);
      f.a(5, 0, 16384);
    }
    else
    {
      if (a != u.c) {
        break label400;
      }
      g = new o();
      t = null;
    }
    d = f.b();
    h = a;
    i = g.a(d, b);
    j = new c(g.a(c, b), (byte)0);
    new Thread(j).start();
    return;
    label400:
    throw new AssertionError(a);
  }
  
  private void a(a parama)
    throws IOException
  {
    synchronized (i)
    {
      try
      {
        if (r) {
          return;
        }
        r = true;
        int i1 = p;
        i.a(i1, parama, j.a);
        return;
      }
      finally {}
    }
  }
  
  private void a(a parama1, a parama2)
    throws IOException
  {
    if ((!k) && (Thread.holdsLock(this))) {
      throw new AssertionError();
    }
    l[] arrayOfl = null;
    try
    {
      a(parama1);
      parama1 = null;
    }
    catch (IOException parama1) {}
    for (;;)
    {
      try
      {
        boolean bool = n.isEmpty();
        int i2 = 0;
        if (!bool)
        {
          arrayOfe = (e[])n.values().toArray(new e[n.size()]);
          n.clear();
          a(false);
          if (u != null)
          {
            arrayOfl = (l[])u.values().toArray(new l[u.size()]);
            u = null;
          }
          Object localObject = parama1;
          int i3;
          int i1;
          if (arrayOfe != null)
          {
            i3 = arrayOfe.length;
            i1 = 0;
            if (i1 < i3)
            {
              localObject = arrayOfe[i1];
              try
              {
                ((e)localObject).a(parama2);
                localObject = parama1;
              }
              catch (IOException localIOException)
              {
                localObject = parama1;
                if (parama1 != null) {
                  localObject = localIOException;
                }
              }
              i1 += 1;
              parama1 = (a)localObject;
              continue;
            }
            localObject = parama1;
          }
          if (arrayOfl != null)
          {
            i3 = arrayOfl.length;
            i1 = i2;
            if (i1 < i3)
            {
              arrayOfl[i1].a();
              i1 += 1;
              continue;
            }
          }
          try
          {
            i.close();
            parama1 = (a)localObject;
          }
          catch (IOException parama2)
          {
            parama1 = (a)localObject;
            if (localObject == null) {
              parama1 = parama2;
            }
          }
          try
          {
            h.close();
          }
          catch (IOException parama1) {}
          if (parama1 == null) {
            return;
          }
          throw parama1;
        }
      }
      finally {}
      e[] arrayOfe = null;
    }
  }
  
  private void a(boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (;;)
    {
      try
      {
        l1 = System.nanoTime();
        s = l1;
        return;
      }
      finally
      {
        Object localObject1;
        continue;
      }
      throw ((Throwable)localObject1);
      long l1 = Long.MAX_VALUE;
    }
  }
  
  private l c(int paramInt)
  {
    try
    {
      if (u != null)
      {
        l locall = (l)u.remove(Integer.valueOf(paramInt));
        return locall;
      }
      return null;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final int a()
  {
    try
    {
      n localn = f;
      if ((a & 0x10) != 0)
      {
        int i1 = d[4];
        return i1;
      }
      return Integer.MAX_VALUE;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  final e a(int paramInt)
  {
    try
    {
      e locale = (e)n.get(Integer.valueOf(paramInt));
      return locale;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final e a(List<f> paramList, boolean paramBoolean)
    throws IOException
  {
    boolean bool = paramBoolean ^ true;
    synchronized (i)
    {
      try
      {
        if (!r)
        {
          int i1 = q;
          q += 2;
          e locale = new e(i1, this, bool, false, paramList);
          if (locale.a())
          {
            n.put(Integer.valueOf(i1), locale);
            a(false);
          }
          i.a(bool, i1, paramList);
          if (!paramBoolean) {
            i.b();
          }
          return locale;
        }
        throw new IOException("shutdown");
      }
      finally {}
    }
  }
  
  final void a(final int paramInt, final long paramLong)
  {
    l.execute(new com.d.a.a.f("OkHttp Window Update %s stream %d", new Object[] { o, Integer.valueOf(paramInt) })
    {
      public final void a()
      {
        try
        {
          i.a(paramInt, paramLong);
          return;
        }
        catch (IOException localIOException) {}
      }
    });
  }
  
  final void a(final int paramInt, final a parama)
  {
    l.submit(new com.d.a.a.f("OkHttp %s stream %d", new Object[] { o, Integer.valueOf(paramInt) })
    {
      public final void a()
      {
        try
        {
          b(paramInt, parama);
          return;
        }
        catch (IOException localIOException) {}
      }
    });
  }
  
  /* Error */
  public final void a(int paramInt, boolean paramBoolean, d.c paramc, long paramLong)
    throws IOException
  {
    // Byte code:
    //   0: lload 4
    //   2: lstore 8
    //   4: lload 4
    //   6: lconst_0
    //   7: lcmp
    //   8: ifne +17 -> 25
    //   11: aload_0
    //   12: getfield 224	com/d/a/a/a/d:i	Lcom/d/a/a/a/c;
    //   15: iload_2
    //   16: iload_1
    //   17: aload_3
    //   18: iconst_0
    //   19: invokeinterface 488 5 0
    //   24: return
    //   25: lload 8
    //   27: lconst_0
    //   28: lcmp
    //   29: ifle +159 -> 188
    //   32: aload_0
    //   33: monitorenter
    //   34: aload_0
    //   35: getfield 210	com/d/a/a/a/d:d	J
    //   38: lconst_0
    //   39: lcmp
    //   40: ifgt +37 -> 77
    //   43: aload_0
    //   44: getfield 125	com/d/a/a/a/d:n	Ljava/util/Map;
    //   47: iload_1
    //   48: invokestatic 326	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   51: invokeinterface 491 2 0
    //   56: ifeq +10 -> 66
    //   59: aload_0
    //   60: invokevirtual 494	java/lang/Object:wait	()V
    //   63: goto -29 -> 34
    //   66: new 119	java/io/IOException
    //   69: dup
    //   70: ldc_w 496
    //   73: invokespecial 381	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   76: athrow
    //   77: lload 8
    //   79: aload_0
    //   80: getfield 210	com/d/a/a/a/d:d	J
    //   83: invokestatic 502	java/lang/Math:min	(JJ)J
    //   86: l2i
    //   87: aload_0
    //   88: getfield 224	com/d/a/a/a/d:i	Lcom/d/a/a/a/c;
    //   91: invokeinterface 504 1 0
    //   96: invokestatic 507	java/lang/Math:min	(II)I
    //   99: istore 6
    //   101: aload_0
    //   102: getfield 210	com/d/a/a/a/d:d	J
    //   105: lstore 4
    //   107: iload 6
    //   109: i2l
    //   110: lstore 10
    //   112: aload_0
    //   113: lload 4
    //   115: lload 10
    //   117: lsub
    //   118: putfield 210	com/d/a/a/a/d:d	J
    //   121: aload_0
    //   122: monitorexit
    //   123: lload 8
    //   125: lload 10
    //   127: lsub
    //   128: lstore 8
    //   130: aload_0
    //   131: getfield 224	com/d/a/a/a/d:i	Lcom/d/a/a/a/c;
    //   134: astore 12
    //   136: iload_2
    //   137: ifeq +16 -> 153
    //   140: lload 8
    //   142: lconst_0
    //   143: lcmp
    //   144: ifne +9 -> 153
    //   147: iconst_1
    //   148: istore 7
    //   150: goto +6 -> 156
    //   153: iconst_0
    //   154: istore 7
    //   156: aload 12
    //   158: iload 7
    //   160: iload_1
    //   161: aload_3
    //   162: iload 6
    //   164: invokeinterface 488 5 0
    //   169: goto -144 -> 25
    //   172: astore_3
    //   173: goto +11 -> 184
    //   176: new 509	java/io/InterruptedIOException
    //   179: dup
    //   180: invokespecial 510	java/io/InterruptedIOException:<init>	()V
    //   183: athrow
    //   184: aload_0
    //   185: monitorexit
    //   186: aload_3
    //   187: athrow
    //   188: return
    //   189: astore_3
    //   190: goto -14 -> 176
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	193	0	this	d
    //   0	193	1	paramInt	int
    //   0	193	2	paramBoolean	boolean
    //   0	193	3	paramc	d.c
    //   0	193	4	paramLong	long
    //   99	64	6	i1	int
    //   148	11	7	bool	boolean
    //   2	139	8	l1	long
    //   110	16	10	l2	long
    //   134	23	12	localc	c
    // Exception table:
    //   from	to	target	type
    //   34	63	172	finally
    //   66	77	172	finally
    //   77	107	172	finally
    //   112	123	172	finally
    //   176	184	172	finally
    //   184	186	172	finally
    //   34	63	189	java/lang/InterruptedException
    //   66	77	189	java/lang/InterruptedException
  }
  
  final e b(int paramInt)
  {
    try
    {
      e locale = (e)n.remove(Integer.valueOf(paramInt));
      if ((locale != null) && (n.isEmpty())) {
        a(true);
      }
      notifyAll();
      return locale;
    }
    finally {}
  }
  
  public final void b()
    throws IOException
  {
    i.b();
  }
  
  final void b(int paramInt, a parama)
    throws IOException
  {
    i.a(paramInt, parama);
  }
  
  public final void close()
    throws IOException
  {
    a(a.a, a.l);
  }
  
  public static final class a
  {
    public Socket a;
    public String b;
    public d.e c;
    public d.d d;
    d.b e = d.b.a;
    public u f = u.c;
    m g = m.a;
    boolean h = true;
    
    public a()
      throws IOException
    {}
  }
  
  public static abstract class b
  {
    public static final b a = new b()
    {
      public final void a(e paramAnonymouse)
        throws IOException
      {
        paramAnonymouse.a(a.k);
      }
    };
    
    public abstract void a(e parame)
      throws IOException;
  }
  
  final class c
    extends com.d.a.a.f
    implements b.a
  {
    final b b;
    
    private c(b paramb)
    {
      super(new Object[] { d.a(d.this) });
      b = paramb;
    }
    
    /* Error */
    public final void a()
    {
      // Byte code:
      //   0: getstatic 48	com/d/a/a/a/a:g	Lcom/d/a/a/a/a;
      //   3: astore_2
      //   4: getstatic 48	com/d/a/a/a/a:g	Lcom/d/a/a/a/a;
      //   7: astore 5
      //   9: aload_2
      //   10: astore_1
      //   11: aload_2
      //   12: astore_3
      //   13: aload_0
      //   14: getfield 22	com/d/a/a/a/d$c:c	Lcom/d/a/a/a/d;
      //   17: getfield 51	com/d/a/a/a/d:b	Z
      //   20: ifne +16 -> 36
      //   23: aload_2
      //   24: astore_1
      //   25: aload_2
      //   26: astore_3
      //   27: aload_0
      //   28: getfield 35	com/d/a/a/a/d$c:b	Lcom/d/a/a/a/b;
      //   31: invokeinterface 55 1 0
      //   36: aload_2
      //   37: astore_1
      //   38: aload_2
      //   39: astore_3
      //   40: aload_0
      //   41: getfield 35	com/d/a/a/a/d$c:b	Lcom/d/a/a/a/b;
      //   44: aload_0
      //   45: invokeinterface 58 2 0
      //   50: ifne -14 -> 36
      //   53: aload_2
      //   54: astore_1
      //   55: aload_2
      //   56: astore_3
      //   57: getstatic 60	com/d/a/a/a/a:a	Lcom/d/a/a/a/a;
      //   60: astore_2
      //   61: aload_2
      //   62: astore_1
      //   63: aload_2
      //   64: astore_3
      //   65: getstatic 63	com/d/a/a/a/a:l	Lcom/d/a/a/a/a;
      //   68: astore 4
      //   70: aload_0
      //   71: getfield 22	com/d/a/a/a/d$c:c	Lcom/d/a/a/a/d;
      //   74: astore_3
      //   75: aload 4
      //   77: astore_1
      //   78: aload_3
      //   79: aload_2
      //   80: aload_1
      //   81: invokestatic 66	com/d/a/a/a/d:a	(Lcom/d/a/a/a/d;Lcom/d/a/a/a/a;Lcom/d/a/a/a/a;)V
      //   84: aload_0
      //   85: getfield 35	com/d/a/a/a/d$c:b	Lcom/d/a/a/a/b;
      //   88: invokestatic 71	com/d/a/a/j:a	(Ljava/io/Closeable;)V
      //   91: return
      //   92: astore_2
      //   93: goto +29 -> 122
      //   96: aload_3
      //   97: astore_1
      //   98: getstatic 73	com/d/a/a/a/a:b	Lcom/d/a/a/a/a;
      //   101: astore_2
      //   102: aload_2
      //   103: astore_1
      //   104: getstatic 73	com/d/a/a/a/a:b	Lcom/d/a/a/a/a;
      //   107: astore_3
      //   108: aload_0
      //   109: getfield 22	com/d/a/a/a/d$c:c	Lcom/d/a/a/a/d;
      //   112: astore 4
      //   114: aload_3
      //   115: astore_1
      //   116: aload 4
      //   118: astore_3
      //   119: goto -41 -> 78
      //   122: aload_0
      //   123: getfield 22	com/d/a/a/a/d$c:c	Lcom/d/a/a/a/d;
      //   126: aload_1
      //   127: aload 5
      //   129: invokestatic 66	com/d/a/a/a/d:a	(Lcom/d/a/a/a/d;Lcom/d/a/a/a/a;Lcom/d/a/a/a/a;)V
      //   132: aload_0
      //   133: getfield 35	com/d/a/a/a/d$c:b	Lcom/d/a/a/a/b;
      //   136: invokestatic 71	com/d/a/a/j:a	(Ljava/io/Closeable;)V
      //   139: aload_2
      //   140: athrow
      //   141: astore_1
      //   142: goto -46 -> 96
      //   145: astore_1
      //   146: goto -62 -> 84
      //   149: astore_1
      //   150: goto -18 -> 132
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	153	0	this	c
      //   10	117	1	localObject1	Object
      //   141	1	1	localIOException1	IOException
      //   145	1	1	localIOException2	IOException
      //   149	1	1	localIOException3	IOException
      //   3	77	2	locala1	a
      //   92	1	2	localObject2	Object
      //   101	39	2	locala2	a
      //   12	107	3	localObject3	Object
      //   68	49	4	localObject4	Object
      //   7	121	5	locala3	a
      // Exception table:
      //   from	to	target	type
      //   13	23	92	finally
      //   27	36	92	finally
      //   40	53	92	finally
      //   57	61	92	finally
      //   65	70	92	finally
      //   98	102	92	finally
      //   104	108	92	finally
      //   13	23	141	java/io/IOException
      //   27	36	141	java/io/IOException
      //   40	53	141	java/io/IOException
      //   57	61	141	java/io/IOException
      //   65	70	141	java/io/IOException
      //   70	75	145	java/io/IOException
      //   78	84	145	java/io/IOException
      //   108	114	145	java/io/IOException
      //   122	132	149	java/io/IOException
    }
    
    public final void a(int paramInt)
    {
      synchronized (d.this)
      {
        e[] arrayOfe = (e[])d.e(d.this).values().toArray(new e[d.e(d.this).size()]);
        d.i(d.this);
        int j = arrayOfe.length;
        int i = 0;
        while (i < j)
        {
          ??? = arrayOfe[i];
          if ((c > paramInt) && (???.b()))
          {
            ???.c(a.k);
            b(c);
          }
          i += 1;
        }
        return;
      }
    }
    
    public final void a(int paramInt, long paramLong)
    {
      if (paramInt == 0) {
        synchronized (d.this)
        {
          d locald = d.this;
          d += paramLong;
          notifyAll();
          return;
        }
      }
      ??? = a(paramInt);
      if (??? != null) {
        try
        {
          ((e)???).a(paramLong);
          return;
        }
        finally {}
      }
    }
    
    public final void a(int paramInt, a parama)
    {
      if (d.a(d.this, paramInt))
      {
        d.a(d.this, paramInt, parama);
        return;
      }
      e locale = b(paramInt);
      if (locale != null) {
        locale.c(parama);
      }
    }
    
    public final void a(int paramInt, List<f> paramList)
    {
      d.a(d.this, paramInt, paramList);
    }
    
    public final void a(boolean paramBoolean, int paramInt1, int paramInt2)
    {
      if (paramBoolean)
      {
        l locall = d.c(d.this, paramInt1);
        if (locall != null)
        {
          if ((c == -1L) && (b != -1L))
          {
            c = System.nanoTime();
            a.countDown();
            return;
          }
          throw new IllegalStateException();
        }
        return;
      }
      d.a(d.this, paramInt1, paramInt2);
    }
    
    public final void a(boolean paramBoolean, int paramInt1, d.e parame, int paramInt2)
      throws IOException
    {
      if (d.a(d.this, paramInt1))
      {
        d.a(d.this, paramInt1, parame, paramInt2, paramBoolean);
        return;
      }
      e locale = a(paramInt1);
      if (locale == null)
      {
        d.this.a(paramInt1, a.c);
        parame.h(paramInt2);
        return;
      }
      if ((!e.j) && (Thread.holdsLock(locale))) {
        throw new AssertionError();
      }
      f.a(parame, paramInt2);
      if (paramBoolean) {
        locale.e();
      }
    }
    
    public final void a(boolean paramBoolean, final n paramn)
    {
      for (;;)
      {
        int i;
        synchronized (d.this)
        {
          int k = f.b();
          int j = 0;
          if (paramBoolean)
          {
            localObject = f;
            c = 0;
            b = 0;
            a = 0;
            Arrays.fill(d, 0);
          }
          Object localObject = f;
          i = 0;
          if (i < 10)
          {
            if (paramn.a(i)) {
              ((n)localObject).a(i, paramn.b(i), d[i]);
            }
          }
          else
          {
            if (a == u.d) {
              d.c().execute(new com.d.a.a.f("OkHttp %s ACK Settings", new Object[] { d.a(d.this) })
              {
                public final void a()
                {
                  try
                  {
                    i.a(paramn);
                    return;
                  }
                  catch (IOException localIOException) {}
                }
              });
            }
            i = f.b();
            paramn = null;
            if ((i == -1) || (i == k)) {
              break label404;
            }
            long l2 = i - k;
            if (!d.g(d.this))
            {
              localObject = d.this;
              d += l2;
              if (l2 > 0L) {
                localObject.notifyAll();
              }
              d.h(d.this);
            }
            l1 = l2;
            if (!d.e(d.this).isEmpty())
            {
              paramn = (e[])d.e(d.this).values().toArray(new e[d.e(d.this).size()]);
              l1 = l2;
            }
            d.c().execute(new com.d.a.a.f("OkHttp %s settings", new Object[] { d.a(d.this) })
            {
              public final void a()
              {
                d.f(d.this);
              }
            });
            if ((paramn != null) && (l1 != 0L))
            {
              k = paramn.length;
              i = j;
              if (i < k) {
                synchronized (paramn[i])
                {
                  ???.a(l1);
                  i += 1;
                }
              }
            }
            return;
          }
        }
        i += 1;
        continue;
        label404:
        long l1 = 0L;
      }
    }
    
    public final void a(boolean paramBoolean1, boolean paramBoolean2, int paramInt, final List<f> paramList, g paramg)
    {
      if (d.a(d.this, paramInt))
      {
        d.a(d.this, paramInt, paramList, paramBoolean2);
        return;
      }
      for (;;)
      {
        synchronized (d.this)
        {
          if (d.b(d.this)) {
            return;
          }
          e locale = a(paramInt);
          int k = 0;
          int j = 0;
          boolean bool = true;
          if (locale == null)
          {
            if (paramg == g.b) {
              break label512;
            }
            if (paramg == g.c)
            {
              break label512;
              if (i != 0)
              {
                d.this.a(paramInt, a.c);
                return;
              }
              if (paramInt <= d.c(d.this)) {
                return;
              }
              if (paramInt % 2 == d.d(d.this) % 2) {
                return;
              }
              paramList = new e(paramInt, d.this, paramBoolean1, paramBoolean2, paramList);
              d.b(d.this, paramInt);
              d.e(d.this).put(Integer.valueOf(paramInt), paramList);
              d.c().execute(new com.d.a.a.f("OkHttp %s stream %d", new Object[] { d.a(d.this), Integer.valueOf(paramInt) })
              {
                public final void a()
                {
                  try
                  {
                    d.f(d.this).a(paramList);
                    return;
                  }
                  catch (IOException localIOException1)
                  {
                    Logger localLogger = com.d.a.a.d.a;
                    Level localLevel = Level.INFO;
                    StringBuilder localStringBuilder = new StringBuilder("FramedConnection.Listener failure for ");
                    localStringBuilder.append(d.a(d.this));
                    localLogger.log(localLevel, localStringBuilder.toString(), localIOException1);
                    try
                    {
                      paramList.a(a.b);
                      return;
                    }
                    catch (IOException localIOException2) {}
                  }
                }
              });
            }
          }
          else
          {
            if (paramg == g.a) {
              i = 1;
            } else {
              i = 0;
            }
            if (i != 0)
            {
              locale.b(a.b);
              b(paramInt);
              return;
            }
            if ((!e.j) && (Thread.holdsLock(locale))) {
              throw new AssertionError();
            }
            ??? = null;
            try
            {
              if (e == null)
              {
                paramInt = j;
                if (paramg == g.c) {
                  paramInt = 1;
                }
                if (paramInt != 0)
                {
                  paramList = a.b;
                  paramBoolean1 = bool;
                }
                else
                {
                  e = paramList;
                  paramBoolean1 = locale.a();
                  locale.notifyAll();
                  paramList = ???;
                }
              }
              else
              {
                paramInt = k;
                if (paramg == g.b) {
                  paramInt = 1;
                }
                if (paramInt != 0)
                {
                  paramList = a.e;
                  paramBoolean1 = bool;
                }
                else
                {
                  paramg = new ArrayList();
                  paramg.addAll(e);
                  paramg.addAll(paramList);
                  e = paramg;
                  paramBoolean1 = bool;
                  paramList = ???;
                }
              }
              if (paramList != null) {
                locale.b(paramList);
              } else if (!paramBoolean1) {
                d.b(c);
              }
              if (paramBoolean2) {
                locale.e();
              }
              return;
            }
            finally {}
          }
        }
        int i = 0;
        continue;
        label512:
        i = 1;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */