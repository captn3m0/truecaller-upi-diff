package com.d.a.a.a;

import d.e;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;

public abstract interface b
  extends Closeable
{
  public abstract void a()
    throws IOException;
  
  public abstract boolean a(a parama)
    throws IOException;
  
  public static abstract interface a
  {
    public abstract void a(int paramInt);
    
    public abstract void a(int paramInt, long paramLong);
    
    public abstract void a(int paramInt, a parama);
    
    public abstract void a(int paramInt, List<f> paramList)
      throws IOException;
    
    public abstract void a(boolean paramBoolean, int paramInt1, int paramInt2);
    
    public abstract void a(boolean paramBoolean, int paramInt1, e parame, int paramInt2)
      throws IOException;
    
    public abstract void a(boolean paramBoolean, n paramn);
    
    public abstract void a(boolean paramBoolean1, boolean paramBoolean2, int paramInt, List<f> paramList, g paramg);
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */