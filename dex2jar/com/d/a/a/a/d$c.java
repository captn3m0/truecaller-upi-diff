package com.d.a.a.a;

import com.d.a.u;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;

final class d$c
  extends com.d.a.a.f
  implements b.a
{
  final b b;
  
  private d$c(d paramd, b paramb)
  {
    super("OkHttp %s", new Object[] { d.a(paramd) });
    b = paramb;
  }
  
  /* Error */
  public final void a()
  {
    // Byte code:
    //   0: getstatic 48	com/d/a/a/a/a:g	Lcom/d/a/a/a/a;
    //   3: astore_2
    //   4: getstatic 48	com/d/a/a/a/a:g	Lcom/d/a/a/a/a;
    //   7: astore 5
    //   9: aload_2
    //   10: astore_1
    //   11: aload_2
    //   12: astore_3
    //   13: aload_0
    //   14: getfield 22	com/d/a/a/a/d$c:c	Lcom/d/a/a/a/d;
    //   17: getfield 51	com/d/a/a/a/d:b	Z
    //   20: ifne +16 -> 36
    //   23: aload_2
    //   24: astore_1
    //   25: aload_2
    //   26: astore_3
    //   27: aload_0
    //   28: getfield 35	com/d/a/a/a/d$c:b	Lcom/d/a/a/a/b;
    //   31: invokeinterface 55 1 0
    //   36: aload_2
    //   37: astore_1
    //   38: aload_2
    //   39: astore_3
    //   40: aload_0
    //   41: getfield 35	com/d/a/a/a/d$c:b	Lcom/d/a/a/a/b;
    //   44: aload_0
    //   45: invokeinterface 58 2 0
    //   50: ifne -14 -> 36
    //   53: aload_2
    //   54: astore_1
    //   55: aload_2
    //   56: astore_3
    //   57: getstatic 60	com/d/a/a/a/a:a	Lcom/d/a/a/a/a;
    //   60: astore_2
    //   61: aload_2
    //   62: astore_1
    //   63: aload_2
    //   64: astore_3
    //   65: getstatic 63	com/d/a/a/a/a:l	Lcom/d/a/a/a/a;
    //   68: astore 4
    //   70: aload_0
    //   71: getfield 22	com/d/a/a/a/d$c:c	Lcom/d/a/a/a/d;
    //   74: astore_3
    //   75: aload 4
    //   77: astore_1
    //   78: aload_3
    //   79: aload_2
    //   80: aload_1
    //   81: invokestatic 66	com/d/a/a/a/d:a	(Lcom/d/a/a/a/d;Lcom/d/a/a/a/a;Lcom/d/a/a/a/a;)V
    //   84: aload_0
    //   85: getfield 35	com/d/a/a/a/d$c:b	Lcom/d/a/a/a/b;
    //   88: invokestatic 71	com/d/a/a/j:a	(Ljava/io/Closeable;)V
    //   91: return
    //   92: astore_2
    //   93: goto +29 -> 122
    //   96: aload_3
    //   97: astore_1
    //   98: getstatic 73	com/d/a/a/a/a:b	Lcom/d/a/a/a/a;
    //   101: astore_2
    //   102: aload_2
    //   103: astore_1
    //   104: getstatic 73	com/d/a/a/a/a:b	Lcom/d/a/a/a/a;
    //   107: astore_3
    //   108: aload_0
    //   109: getfield 22	com/d/a/a/a/d$c:c	Lcom/d/a/a/a/d;
    //   112: astore 4
    //   114: aload_3
    //   115: astore_1
    //   116: aload 4
    //   118: astore_3
    //   119: goto -41 -> 78
    //   122: aload_0
    //   123: getfield 22	com/d/a/a/a/d$c:c	Lcom/d/a/a/a/d;
    //   126: aload_1
    //   127: aload 5
    //   129: invokestatic 66	com/d/a/a/a/d:a	(Lcom/d/a/a/a/d;Lcom/d/a/a/a/a;Lcom/d/a/a/a/a;)V
    //   132: aload_0
    //   133: getfield 35	com/d/a/a/a/d$c:b	Lcom/d/a/a/a/b;
    //   136: invokestatic 71	com/d/a/a/j:a	(Ljava/io/Closeable;)V
    //   139: aload_2
    //   140: athrow
    //   141: astore_1
    //   142: goto -46 -> 96
    //   145: astore_1
    //   146: goto -62 -> 84
    //   149: astore_1
    //   150: goto -18 -> 132
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	153	0	this	c
    //   10	117	1	localObject1	Object
    //   141	1	1	localIOException1	IOException
    //   145	1	1	localIOException2	IOException
    //   149	1	1	localIOException3	IOException
    //   3	77	2	locala1	a
    //   92	1	2	localObject2	Object
    //   101	39	2	locala2	a
    //   12	107	3	localObject3	Object
    //   68	49	4	localObject4	Object
    //   7	121	5	locala3	a
    // Exception table:
    //   from	to	target	type
    //   13	23	92	finally
    //   27	36	92	finally
    //   40	53	92	finally
    //   57	61	92	finally
    //   65	70	92	finally
    //   98	102	92	finally
    //   104	108	92	finally
    //   13	23	141	java/io/IOException
    //   27	36	141	java/io/IOException
    //   40	53	141	java/io/IOException
    //   57	61	141	java/io/IOException
    //   65	70	141	java/io/IOException
    //   70	75	145	java/io/IOException
    //   78	84	145	java/io/IOException
    //   108	114	145	java/io/IOException
    //   122	132	149	java/io/IOException
  }
  
  public final void a(int paramInt)
  {
    synchronized (c)
    {
      e[] arrayOfe = (e[])d.e(c).values().toArray(new e[d.e(c).size()]);
      d.i(c);
      int j = arrayOfe.length;
      int i = 0;
      while (i < j)
      {
        ??? = arrayOfe[i];
        if ((c > paramInt) && (???.b()))
        {
          ???.c(a.k);
          c.b(c);
        }
        i += 1;
      }
      return;
    }
  }
  
  public final void a(int paramInt, long paramLong)
  {
    if (paramInt == 0) {
      synchronized (c)
      {
        d locald = c;
        d += paramLong;
        c.notifyAll();
        return;
      }
    }
    ??? = c.a(paramInt);
    if (??? != null) {
      try
      {
        ((e)???).a(paramLong);
        return;
      }
      finally {}
    }
  }
  
  public final void a(int paramInt, a parama)
  {
    if (d.a(c, paramInt))
    {
      d.a(c, paramInt, parama);
      return;
    }
    e locale = c.b(paramInt);
    if (locale != null) {
      locale.c(parama);
    }
  }
  
  public final void a(int paramInt, List<f> paramList)
  {
    d.a(c, paramInt, paramList);
  }
  
  public final void a(boolean paramBoolean, int paramInt1, int paramInt2)
  {
    if (paramBoolean)
    {
      l locall = d.c(c, paramInt1);
      if (locall != null)
      {
        if ((c == -1L) && (b != -1L))
        {
          c = System.nanoTime();
          a.countDown();
          return;
        }
        throw new IllegalStateException();
      }
      return;
    }
    d.a(c, paramInt1, paramInt2);
  }
  
  public final void a(boolean paramBoolean, int paramInt1, d.e parame, int paramInt2)
    throws IOException
  {
    if (d.a(c, paramInt1))
    {
      d.a(c, paramInt1, parame, paramInt2, paramBoolean);
      return;
    }
    e locale = c.a(paramInt1);
    if (locale == null)
    {
      c.a(paramInt1, a.c);
      parame.h(paramInt2);
      return;
    }
    if ((!e.j) && (Thread.holdsLock(locale))) {
      throw new AssertionError();
    }
    f.a(parame, paramInt2);
    if (paramBoolean) {
      locale.e();
    }
  }
  
  public final void a(boolean paramBoolean, final n paramn)
  {
    for (;;)
    {
      int i;
      synchronized (c)
      {
        int k = c.f.b();
        int j = 0;
        if (paramBoolean)
        {
          localObject = c.f;
          c = 0;
          b = 0;
          a = 0;
          Arrays.fill(d, 0);
        }
        Object localObject = c.f;
        i = 0;
        if (i < 10)
        {
          if (paramn.a(i)) {
            ((n)localObject).a(i, paramn.b(i), d[i]);
          }
        }
        else
        {
          if (c.a == u.d) {
            d.c().execute(new com.d.a.a.f("OkHttp %s ACK Settings", new Object[] { d.a(c) })
            {
              public final void a()
              {
                try
                {
                  c.i.a(paramn);
                  return;
                }
                catch (IOException localIOException) {}
              }
            });
          }
          i = c.f.b();
          paramn = null;
          if ((i == -1) || (i == k)) {
            break label404;
          }
          long l2 = i - k;
          if (!d.g(c))
          {
            localObject = c;
            d += l2;
            if (l2 > 0L) {
              localObject.notifyAll();
            }
            d.h(c);
          }
          l1 = l2;
          if (!d.e(c).isEmpty())
          {
            paramn = (e[])d.e(c).values().toArray(new e[d.e(c).size()]);
            l1 = l2;
          }
          d.c().execute(new com.d.a.a.f("OkHttp %s settings", new Object[] { d.a(c) })
          {
            public final void a()
            {
              d.f(c);
            }
          });
          if ((paramn != null) && (l1 != 0L))
          {
            k = paramn.length;
            i = j;
            if (i < k) {
              synchronized (paramn[i])
              {
                ???.a(l1);
                i += 1;
              }
            }
          }
          return;
        }
      }
      i += 1;
      continue;
      label404:
      long l1 = 0L;
    }
  }
  
  public final void a(boolean paramBoolean1, boolean paramBoolean2, int paramInt, final List<f> paramList, g paramg)
  {
    if (d.a(c, paramInt))
    {
      d.a(c, paramInt, paramList, paramBoolean2);
      return;
    }
    for (;;)
    {
      synchronized (c)
      {
        if (d.b(c)) {
          return;
        }
        e locale = c.a(paramInt);
        int k = 0;
        int j = 0;
        boolean bool = true;
        if (locale == null)
        {
          if (paramg == g.b) {
            break label512;
          }
          if (paramg == g.c)
          {
            break label512;
            if (i != 0)
            {
              c.a(paramInt, a.c);
              return;
            }
            if (paramInt <= d.c(c)) {
              return;
            }
            if (paramInt % 2 == d.d(c) % 2) {
              return;
            }
            paramList = new e(paramInt, c, paramBoolean1, paramBoolean2, paramList);
            d.b(c, paramInt);
            d.e(c).put(Integer.valueOf(paramInt), paramList);
            d.c().execute(new com.d.a.a.f("OkHttp %s stream %d", new Object[] { d.a(c), Integer.valueOf(paramInt) })
            {
              public final void a()
              {
                try
                {
                  d.f(c).a(paramList);
                  return;
                }
                catch (IOException localIOException1)
                {
                  Logger localLogger = com.d.a.a.d.a;
                  Level localLevel = Level.INFO;
                  StringBuilder localStringBuilder = new StringBuilder("FramedConnection.Listener failure for ");
                  localStringBuilder.append(d.a(c));
                  localLogger.log(localLevel, localStringBuilder.toString(), localIOException1);
                  try
                  {
                    paramList.a(a.b);
                    return;
                  }
                  catch (IOException localIOException2) {}
                }
              }
            });
          }
        }
        else
        {
          if (paramg == g.a) {
            i = 1;
          } else {
            i = 0;
          }
          if (i != 0)
          {
            locale.b(a.b);
            c.b(paramInt);
            return;
          }
          if ((!e.j) && (Thread.holdsLock(locale))) {
            throw new AssertionError();
          }
          ??? = null;
          try
          {
            if (e == null)
            {
              paramInt = j;
              if (paramg == g.c) {
                paramInt = 1;
              }
              if (paramInt != 0)
              {
                paramList = a.b;
                paramBoolean1 = bool;
              }
              else
              {
                e = paramList;
                paramBoolean1 = locale.a();
                locale.notifyAll();
                paramList = ???;
              }
            }
            else
            {
              paramInt = k;
              if (paramg == g.b) {
                paramInt = 1;
              }
              if (paramInt != 0)
              {
                paramList = a.e;
                paramBoolean1 = bool;
              }
              else
              {
                paramg = new ArrayList();
                paramg.addAll(e);
                paramg.addAll(paramList);
                e = paramg;
                paramBoolean1 = bool;
                paramList = ???;
              }
            }
            if (paramList != null) {
              locale.b(paramList);
            } else if (!paramBoolean1) {
              d.b(c);
            }
            if (paramBoolean2) {
              locale.e();
            }
            return;
          }
          finally {}
        }
      }
      int i = 0;
      continue;
      label512:
      i = 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.d.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */