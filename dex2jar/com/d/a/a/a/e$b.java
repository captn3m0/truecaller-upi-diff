package com.d.a.a.a;

import d.c;
import d.u;
import d.v;
import java.io.EOFException;
import java.io.IOException;

final class e$b
  implements u
{
  private final c c = new c();
  private final c d = new c();
  private final long e;
  private boolean f;
  private boolean g;
  
  private e$b(e parame, long paramLong)
  {
    e = paramLong;
  }
  
  private void b()
    throws IOException
  {
    e.c(b).m_();
    try
    {
      while ((d.b == 0L) && (!g) && (!f) && (e.d(b) == null)) {
        e.e(b);
      }
      return;
    }
    finally
    {
      e.c(b).b();
    }
  }
  
  public final long a(c arg1, long paramLong)
    throws IOException
  {
    if (paramLong >= 0L) {
      synchronized (b)
      {
        b();
        if (!f)
        {
          if (e.d(b) == null)
          {
            if (d.b == 0L) {
              return -1L;
            }
            paramLong = d.a(???, Math.min(paramLong, d.b));
            ??? = b;
            a += paramLong;
            if (b.a >= ab).e.b() / 2)
            {
              e.a(b).a(e.b(b), b.a);
              b.a = 0L;
            }
            synchronized (e.a(b))
            {
              ??? = e.a(b);
              c += paramLong;
              if (ab).c >= ab).e.b() / 2)
              {
                e.a(b).a(0, ab).c);
                ab).c = 0L;
              }
              return paramLong;
            }
          }
          ??? = new StringBuilder("stream was reset: ");
          ???.append(e.d(b));
          throw new IOException(???.toString());
        }
        throw new IOException("stream closed");
      }
    }
    throw new IllegalArgumentException("byteCount < 0: ".concat(String.valueOf(paramLong)));
  }
  
  final void a(d.e parame, long paramLong)
    throws IOException
  {
    long l1 = paramLong;
    if (!a) {
      if (!Thread.holdsLock(b)) {
        l1 = paramLong;
      } else {
        throw new AssertionError();
      }
    }
    if (l1 > 0L) {}
    for (;;)
    {
      synchronized (b)
      {
        boolean bool = g;
        paramLong = d.b;
        long l2 = e;
        int j = 1;
        if (paramLong + l1 <= l2) {
          break label235;
        }
        i = 1;
        if (i != 0)
        {
          parame.h(l1);
          b.b(a.h);
          return;
        }
        if (bool)
        {
          parame.h(l1);
          return;
        }
        paramLong = parame.a(c, l1);
        if (paramLong != -1L)
        {
          l1 -= paramLong;
          synchronized (b)
          {
            if (d.b != 0L) {
              break label241;
            }
            i = j;
            d.a(c);
            if (i != 0) {
              b.notifyAll();
            }
          }
        }
        throw new EOFException();
      }
      return;
      label235:
      int i = 0;
      continue;
      label241:
      i = 0;
    }
  }
  
  public final void close()
    throws IOException
  {
    synchronized (b)
    {
      f = true;
      d.s();
      b.notifyAll();
      e.f(b);
      return;
    }
  }
  
  public final v l_()
  {
    return e.c(b);
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.e.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */