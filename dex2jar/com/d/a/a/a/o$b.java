package com.d.a.a.a;

import com.d.a.a.j;
import d.d;
import d.g;
import java.io.IOException;
import java.util.List;
import java.util.zip.Deflater;

final class o$b
  implements c
{
  private final d a;
  private final d.c b;
  private final d c;
  private final boolean d;
  private boolean e;
  
  o$b(d paramd, boolean paramBoolean)
  {
    a = paramd;
    d = paramBoolean;
    paramd = new Deflater();
    paramd.setDictionary(o.a);
    b = new d.c();
    c = d.n.a(new g(b, paramd));
  }
  
  private void a(List<f> paramList)
    throws IOException
  {
    c.h(paramList.size());
    int j = paramList.size();
    int i = 0;
    while (i < j)
    {
      d.f localf = geth;
      c.h(localf.h());
      c.c(localf);
      localf = geti;
      c.h(localf.h());
      c.c(localf);
      i += 1;
    }
    c.flush();
  }
  
  public final void a() {}
  
  public final void a(int paramInt, long paramLong)
    throws IOException
  {
    try
    {
      if (!e)
      {
        if ((paramLong != 0L) && (paramLong <= 2147483647L))
        {
          a.h(-2147287031);
          a.h(8);
          a.h(paramInt);
          a.h((int)paramLong);
          a.flush();
          return;
        }
        throw new IllegalArgumentException("windowSizeIncrement must be between 1 and 0x7fffffff: ".concat(String.valueOf(paramLong)));
      }
      throw new IOException("closed");
    }
    finally {}
  }
  
  public final void a(int paramInt, a parama)
    throws IOException
  {
    try
    {
      if (!e)
      {
        if (t != -1)
        {
          a.h(-2147287037);
          a.h(8);
          a.h(paramInt & 0x7FFFFFFF);
          a.h(t);
          a.flush();
          return;
        }
        throw new IllegalArgumentException();
      }
      throw new IOException("closed");
    }
    finally {}
  }
  
  public final void a(int paramInt, a parama, byte[] paramArrayOfByte)
    throws IOException
  {
    try
    {
      if (!e)
      {
        if (u != -1)
        {
          a.h(-2147287033);
          a.h(8);
          a.h(paramInt);
          a.h(u);
          a.flush();
          return;
        }
        throw new IllegalArgumentException("errorCode.spdyGoAwayCode == -1");
      }
      throw new IOException("closed");
    }
    finally {}
  }
  
  public final void a(n paramn) {}
  
  public final void a(boolean paramBoolean, int paramInt1, int paramInt2)
    throws IOException
  {
    for (;;)
    {
      boolean bool3;
      boolean bool2;
      try
      {
        if (!e)
        {
          bool3 = d;
          bool2 = false;
          if ((paramInt1 & 0x1) == 1)
          {
            bool1 = true;
            break label113;
            if (paramBoolean == bool2)
            {
              a.h(-2147287034);
              a.h(4);
              a.h(paramInt1);
              a.flush();
              return;
            }
            throw new IllegalArgumentException("payload != reply");
          }
        }
        else
        {
          throw new IOException("closed");
        }
      }
      finally {}
      boolean bool1 = false;
      label113:
      if (bool3 != bool1) {
        bool2 = true;
      }
    }
  }
  
  public final void a(boolean paramBoolean, int paramInt1, d.c paramc, int paramInt2)
    throws IOException
  {
    int i;
    if (paramBoolean) {
      i = 1;
    } else {
      i = 0;
    }
    try
    {
      if (!e)
      {
        long l = paramInt2;
        if (l <= 16777215L)
        {
          a.h(paramInt1 & 0x7FFFFFFF);
          a.h((i & 0xFF) << 24 | 0xFFFFFF & paramInt2);
          if (paramInt2 > 0) {
            a.a_(paramc, l);
          }
          return;
        }
        throw new IllegalArgumentException("FRAME_TOO_LARGE max size is 16Mib: ".concat(String.valueOf(paramInt2)));
      }
      throw new IOException("closed");
    }
    finally {}
  }
  
  public final void a(boolean paramBoolean, int paramInt, List<f> paramList)
    throws IOException
  {
    for (;;)
    {
      try
      {
        if (!e)
        {
          a(paramList);
          int j = (int)(b.b + 10L);
          if (paramBoolean)
          {
            i = 1;
            a.h(-2147287039);
            a.h(((i | 0x0) & 0xFF) << 24 | j & 0xFFFFFF);
            a.h(paramInt & 0x7FFFFFFF);
            a.h(0);
            a.i(0);
            a.a(b);
            a.flush();
          }
        }
        else
        {
          throw new IOException("closed");
        }
      }
      finally {}
      int i = 0;
    }
  }
  
  public final void b()
    throws IOException
  {
    try
    {
      if (!e)
      {
        a.flush();
        return;
      }
      throw new IOException("closed");
    }
    finally {}
  }
  
  public final void b(n paramn)
    throws IOException
  {
    for (;;)
    {
      int i;
      try
      {
        if (!e)
        {
          int j = Integer.bitCount(a);
          a.h(-2147287036);
          d locald = a;
          i = 0;
          locald.h(j * 8 + 4 & 0xFFFFFF | 0x0);
          a.h(j);
          if (i <= 10)
          {
            if (paramn.a(i))
            {
              j = paramn.b(i);
              a.h((j & 0xFF) << 24 | i & 0xFFFFFF);
              a.h(d[i]);
            }
          }
          else {
            a.flush();
          }
        }
        else
        {
          throw new IOException("closed");
        }
      }
      finally {}
      i += 1;
    }
  }
  
  public final int c()
  {
    return 16383;
  }
  
  public final void close()
    throws IOException
  {
    try
    {
      e = true;
      j.a(a, c);
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.o.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */