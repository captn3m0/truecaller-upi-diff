package com.d.a.a.a;

import d.d;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

final class i$d
  implements c
{
  private final d a;
  private final boolean b;
  private final d.c c;
  private final h.b d;
  private int e;
  private boolean f;
  
  i$d(d paramd, boolean paramBoolean)
  {
    a = paramd;
    b = paramBoolean;
    c = new d.c();
    d = new h.b(c);
    e = 16384;
  }
  
  private void a(int paramInt1, int paramInt2, byte paramByte1, byte paramByte2)
    throws IOException
  {
    if (i.b().isLoggable(Level.FINE)) {
      i.b().fine(i.b.a(false, paramInt1, paramInt2, paramByte1, paramByte2));
    }
    int i = e;
    if (paramInt2 <= i)
    {
      if ((0x80000000 & paramInt1) == 0)
      {
        i.a(a, paramInt2);
        a.j(paramByte1 & 0xFF);
        a.j(paramByte2 & 0xFF);
        a.h(paramInt1 & 0x7FFFFFFF);
        return;
      }
      throw i.b("reserved bit set: %s", new Object[] { Integer.valueOf(paramInt1) });
    }
    throw i.b("FRAME_SIZE_ERROR length > %d: %d", new Object[] { Integer.valueOf(i), Integer.valueOf(paramInt2) });
  }
  
  private void b(int paramInt, long paramLong)
    throws IOException
  {
    while (paramLong > 0L)
    {
      int i = (int)Math.min(e, paramLong);
      long l = i;
      paramLong -= l;
      byte b1;
      if (paramLong == 0L) {
        b1 = 4;
      } else {
        b1 = 0;
      }
      a(paramInt, i, (byte)9, b1);
      a.a_(c, l);
    }
  }
  
  public final void a()
    throws IOException
  {
    try
    {
      if (!f)
      {
        boolean bool = b;
        if (!bool) {
          return;
        }
        if (i.b().isLoggable(Level.FINE)) {
          i.b().fine(String.format(">> CONNECTION %s", new Object[] { i.a().f() }));
        }
        a.c(i.a().i());
        a.flush();
        return;
      }
      throw new IOException("closed");
    }
    finally {}
  }
  
  public final void a(int paramInt, long paramLong)
    throws IOException
  {
    try
    {
      if (!f)
      {
        if ((paramLong != 0L) && (paramLong <= 2147483647L))
        {
          a(paramInt, 4, (byte)8, (byte)0);
          a.h((int)paramLong);
          a.flush();
          return;
        }
        throw i.b("windowSizeIncrement == 0 || windowSizeIncrement > 0x7fffffffL: %s", new Object[] { Long.valueOf(paramLong) });
      }
      throw new IOException("closed");
    }
    finally {}
  }
  
  public final void a(int paramInt, a parama)
    throws IOException
  {
    try
    {
      if (!f)
      {
        if (s != -1)
        {
          a(paramInt, 4, (byte)3, (byte)0);
          a.h(s);
          a.flush();
          return;
        }
        throw new IllegalArgumentException();
      }
      throw new IOException("closed");
    }
    finally {}
  }
  
  public final void a(int paramInt, a parama, byte[] paramArrayOfByte)
    throws IOException
  {
    try
    {
      if (!f)
      {
        if (s != -1)
        {
          a(0, paramArrayOfByte.length + 8, (byte)7, (byte)0);
          a.h(paramInt);
          a.h(s);
          if (paramArrayOfByte.length > 0) {
            a.c(paramArrayOfByte);
          }
          a.flush();
          return;
        }
        throw i.b("errorCode.httpCode == -1", new Object[0]);
      }
      throw new IOException("closed");
    }
    finally {}
  }
  
  public final void a(n paramn)
    throws IOException
  {
    try
    {
      if (!f)
      {
        int i = e;
        if ((a & 0x20) != 0) {
          i = d[5];
        }
        e = i;
        a(0, 0, (byte)4, (byte)1);
        a.flush();
        return;
      }
      throw new IOException("closed");
    }
    finally {}
  }
  
  public final void a(boolean paramBoolean, int paramInt1, int paramInt2)
    throws IOException
  {
    for (;;)
    {
      try
      {
        if (!f)
        {
          if (paramBoolean)
          {
            b1 = 1;
            a(0, 8, (byte)6, b1);
            a.h(paramInt1);
            a.h(paramInt2);
            a.flush();
          }
        }
        else {
          throw new IOException("closed");
        }
      }
      finally {}
      byte b1 = 0;
    }
  }
  
  public final void a(boolean paramBoolean, int paramInt1, d.c paramc, int paramInt2)
    throws IOException
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
  }
  
  public final void a(boolean paramBoolean, int paramInt, List<f> paramList)
    throws IOException
  {
    for (;;)
    {
      try
      {
        if (!f)
        {
          if (!f)
          {
            d.a(paramList);
            long l1 = c.b;
            int i = (int)Math.min(e, l1);
            long l2 = i;
            if (l1 == l2)
            {
              b1 = 4;
              break label139;
              a(paramInt, i, (byte)1, b2);
              a.a_(c, l2);
              if (l1 > l2) {
                b(paramInt, l1 - l2);
              }
            }
          }
          else
          {
            throw new IOException("closed");
          }
        }
        else {
          throw new IOException("closed");
        }
      }
      finally {}
      byte b1 = 0;
      label139:
      byte b2 = b1;
      if (paramBoolean) {
        b2 = (byte)(b1 | 0x1);
      }
    }
  }
  
  public final void b()
    throws IOException
  {
    try
    {
      if (!f)
      {
        a.flush();
        return;
      }
      throw new IOException("closed");
    }
    finally {}
  }
  
  public final void b(n paramn)
    throws IOException
  {
    for (;;)
    {
      int j;
      int i;
      try
      {
        if (!f)
        {
          j = Integer.bitCount(a);
          i = 0;
          a(0, j * 6, (byte)4, (byte)0);
          if (i < 10)
          {
            if (!paramn.a(i)) {
              break label127;
            }
            if (i == 4)
            {
              j = 3;
              a.i(j);
              a.h(d[i]);
              break label127;
            }
          }
          else
          {
            a.flush();
          }
        }
        else
        {
          throw new IOException("closed");
        }
      }
      finally {}
      if (i == 7)
      {
        j = 4;
      }
      else
      {
        j = i;
        continue;
        label127:
        i += 1;
      }
    }
  }
  
  public final int c()
  {
    return e;
  }
  
  public final void close()
    throws IOException
  {
    try
    {
      f = true;
      a.close();
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.i.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */