package com.d.a.a.a;

import com.d.a.a.j;
import d.d;
import d.e;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ProtocolException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.zip.Deflater;

public final class o
  implements p
{
  static final byte[] a;
  
  static
  {
    try
    {
      a = "\000\000\000\007options\000\000\000\004head\000\000\000\004post\000\000\000\003put\000\000\000\006delete\000\000\000\005trace\000\000\000\006accept\000\000\000\016accept-charset\000\000\000\017accept-encoding\000\000\000\017accept-language\000\000\000\raccept-ranges\000\000\000\003age\000\000\000\005allow\000\000\000\rauthorization\000\000\000\rcache-control\000\000\000\nconnection\000\000\000\fcontent-base\000\000\000\020content-encoding\000\000\000\020content-language\000\000\000\016content-length\000\000\000\020content-location\000\000\000\013content-md5\000\000\000\rcontent-range\000\000\000\fcontent-type\000\000\000\004date\000\000\000\004etag\000\000\000\006expect\000\000\000\007expires\000\000\000\004from\000\000\000\004host\000\000\000\bif-match\000\000\000\021if-modified-since\000\000\000\rif-none-match\000\000\000\bif-range\000\000\000\023if-unmodified-since\000\000\000\rlast-modified\000\000\000\blocation\000\000\000\fmax-forwards\000\000\000\006pragma\000\000\000\022proxy-authenticate\000\000\000\023proxy-authorization\000\000\000\005range\000\000\000\007referer\000\000\000\013retry-after\000\000\000\006server\000\000\000\002te\000\000\000\007trailer\000\000\000\021transfer-encoding\000\000\000\007upgrade\000\000\000\nuser-agent\000\000\000\004vary\000\000\000\003via\000\000\000\007warning\000\000\000\020www-authenticate\000\000\000\006method\000\000\000\003get\000\000\000\006status\000\000\000\006200 OK\000\000\000\007version\000\000\000\bHTTP/1.1\000\000\000\003url\000\000\000\006public\000\000\000\nset-cookie\000\000\000\nkeep-alive\000\000\000\006origin100101201202205206300302303304305306307402405406407408409410411412413414415416417502504505203 Non-Authoritative Information204 No Content301 Moved Permanently400 Bad Request401 Unauthorized403 Forbidden404 Not Found500 Internal Server Error501 Not Implemented503 Service UnavailableJan Feb Mar Apr May Jun Jul Aug Sept Oct Nov Dec 00:00:00 Mon, Tue, Wed, Thu, Fri, Sat, Sun, GMTchunked,text/html,image/png,image/jpg,image/gif,application/xml,application/xhtml+xml,text/plain,text/javascript,publicprivatemax-age=gzip,deflate,sdchcharset=utf-8charset=iso-8859-1,utf-,*,enq=0.".getBytes(j.c.name());
      return;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      for (;;) {}
    }
    throw new AssertionError();
  }
  
  public final b a(e parame, boolean paramBoolean)
  {
    return new a(parame, paramBoolean);
  }
  
  public final c a(d paramd, boolean paramBoolean)
  {
    return new b(paramd, paramBoolean);
  }
  
  static final class a
    implements b
  {
    private final e a;
    private final boolean b;
    private final k c;
    
    a(e parame, boolean paramBoolean)
    {
      a = parame;
      c = new k(a);
      b = paramBoolean;
    }
    
    private static IOException a(String paramString, Object... paramVarArgs)
      throws IOException
    {
      throw new IOException(String.format(paramString, paramVarArgs));
    }
    
    private void a(b.a parama, int paramInt1, int paramInt2)
      throws IOException
    {
      int i = a.j();
      boolean bool = false;
      if (paramInt2 == i * 8 + 4)
      {
        n localn = new n();
        paramInt2 = 0;
        while (paramInt2 < i)
        {
          int j = a.j();
          localn.a(j & 0xFFFFFF, (0xFF000000 & j) >>> 24, a.j());
          paramInt2 += 1;
        }
        if ((paramInt1 & 0x1) != 0) {
          bool = true;
        }
        parama.a(bool, localn);
        return;
      }
      throw a("TYPE_SETTINGS length: %d != 4 + 8 * %d", new Object[] { Integer.valueOf(paramInt2), Integer.valueOf(i) });
    }
    
    public final void a() {}
    
    public final boolean a(b.a parama)
      throws IOException
    {
      boolean bool1 = false;
      try
      {
        int j = a.j();
        int m = a.j();
        int i;
        if ((0x80000000 & j) != 0) {
          i = 1;
        } else {
          i = 0;
        }
        int k = (0xFF000000 & m) >>> 24;
        m &= 0xFFFFFF;
        if (i != 0)
        {
          i = (0x7FFF0000 & j) >>> 16;
          if (i == 3)
          {
            boolean bool2;
            switch (j & 0xFFFF)
            {
            case 5: 
            default: 
              a.h(m);
              return true;
            case 9: 
              if (m == 8)
              {
                i = a.j();
                long l = a.j() & 0x7FFFFFFF;
                if (l != 0L)
                {
                  parama.a(i & 0x7FFFFFFF, l);
                  return true;
                }
                throw a("windowSizeIncrement was 0", new Object[] { Long.valueOf(l) });
              }
              throw a("TYPE_WINDOW_UPDATE length: %d != 8", new Object[] { Integer.valueOf(m) });
            case 8: 
              parama.a(false, false, a.j() & 0x7FFFFFFF, c.a(m - 4), g.c);
              return true;
            case 7: 
              if (m == 8)
              {
                i = a.j();
                j = a.j();
                if (a.c(j) != null)
                {
                  localObject = d.f.b;
                  parama.a(i & 0x7FFFFFFF);
                  return true;
                }
                throw a("TYPE_GOAWAY unexpected error code: %d", new Object[] { Integer.valueOf(j) });
              }
              throw a("TYPE_GOAWAY length: %d != 8", new Object[] { Integer.valueOf(m) });
            case 6: 
              if (m == 4)
              {
                i = a.j();
                bool2 = b;
                if ((i & 0x1) == 1) {
                  bool1 = true;
                } else {
                  bool1 = false;
                }
                if (bool2 == bool1) {
                  bool1 = true;
                } else {
                  bool1 = false;
                }
                parama.a(bool1, i, 0);
                return true;
              }
              throw a("TYPE_PING length: %d != 4", new Object[] { Integer.valueOf(m) });
            case 4: 
              a(parama, k, m);
              return true;
            case 3: 
              if (m == 8)
              {
                i = a.j();
                j = a.j();
                localObject = a.a(j);
                if (localObject != null)
                {
                  parama.a(i & 0x7FFFFFFF, (a)localObject);
                  return true;
                }
                throw a("TYPE_RST_STREAM unexpected error code: %d", new Object[] { Integer.valueOf(j) });
              }
              throw a("TYPE_RST_STREAM length: %d != 8", new Object[] { Integer.valueOf(m) });
            case 2: 
              i = a.j();
              localObject = c.a(m - 4);
              if ((k & 0x1) != 0) {
                bool1 = true;
              } else {
                bool1 = false;
              }
              parama.a(false, bool1, i & 0x7FFFFFFF, (List)localObject, g.b);
              return true;
            }
            i = a.j();
            a.j();
            a.i();
            Object localObject = c.a(m - 10);
            if ((k & 0x1) != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            }
            if ((k & 0x2) != 0) {
              bool2 = true;
            } else {
              bool2 = false;
            }
            parama.a(bool2, bool1, i & 0x7FFFFFFF, (List)localObject, g.a);
            return true;
          }
          throw new ProtocolException("version != 3: ".concat(String.valueOf(i)));
        }
        if ((k & 0x1) != 0) {
          bool1 = true;
        }
        parama.a(bool1, j & 0x7FFFFFFF, a, m);
        return true;
      }
      catch (IOException parama) {}
      return false;
    }
    
    public final void close()
      throws IOException
    {
      c.b.close();
    }
  }
  
  static final class b
    implements c
  {
    private final d a;
    private final d.c b;
    private final d c;
    private final boolean d;
    private boolean e;
    
    b(d paramd, boolean paramBoolean)
    {
      a = paramd;
      d = paramBoolean;
      paramd = new Deflater();
      paramd.setDictionary(o.a);
      b = new d.c();
      c = d.n.a(new d.g(b, paramd));
    }
    
    private void a(List<f> paramList)
      throws IOException
    {
      c.h(paramList.size());
      int j = paramList.size();
      int i = 0;
      while (i < j)
      {
        d.f localf = geth;
        c.h(localf.h());
        c.c(localf);
        localf = geti;
        c.h(localf.h());
        c.c(localf);
        i += 1;
      }
      c.flush();
    }
    
    public final void a() {}
    
    public final void a(int paramInt, long paramLong)
      throws IOException
    {
      try
      {
        if (!e)
        {
          if ((paramLong != 0L) && (paramLong <= 2147483647L))
          {
            a.h(-2147287031);
            a.h(8);
            a.h(paramInt);
            a.h((int)paramLong);
            a.flush();
            return;
          }
          throw new IllegalArgumentException("windowSizeIncrement must be between 1 and 0x7fffffff: ".concat(String.valueOf(paramLong)));
        }
        throw new IOException("closed");
      }
      finally {}
    }
    
    public final void a(int paramInt, a parama)
      throws IOException
    {
      try
      {
        if (!e)
        {
          if (t != -1)
          {
            a.h(-2147287037);
            a.h(8);
            a.h(paramInt & 0x7FFFFFFF);
            a.h(t);
            a.flush();
            return;
          }
          throw new IllegalArgumentException();
        }
        throw new IOException("closed");
      }
      finally {}
    }
    
    public final void a(int paramInt, a parama, byte[] paramArrayOfByte)
      throws IOException
    {
      try
      {
        if (!e)
        {
          if (u != -1)
          {
            a.h(-2147287033);
            a.h(8);
            a.h(paramInt);
            a.h(u);
            a.flush();
            return;
          }
          throw new IllegalArgumentException("errorCode.spdyGoAwayCode == -1");
        }
        throw new IOException("closed");
      }
      finally {}
    }
    
    public final void a(n paramn) {}
    
    public final void a(boolean paramBoolean, int paramInt1, int paramInt2)
      throws IOException
    {
      for (;;)
      {
        boolean bool3;
        boolean bool2;
        try
        {
          if (!e)
          {
            bool3 = d;
            bool2 = false;
            if ((paramInt1 & 0x1) == 1)
            {
              bool1 = true;
              break label113;
              if (paramBoolean == bool2)
              {
                a.h(-2147287034);
                a.h(4);
                a.h(paramInt1);
                a.flush();
                return;
              }
              throw new IllegalArgumentException("payload != reply");
            }
          }
          else
          {
            throw new IOException("closed");
          }
        }
        finally {}
        boolean bool1 = false;
        label113:
        if (bool3 != bool1) {
          bool2 = true;
        }
      }
    }
    
    public final void a(boolean paramBoolean, int paramInt1, d.c paramc, int paramInt2)
      throws IOException
    {
      int i;
      if (paramBoolean) {
        i = 1;
      } else {
        i = 0;
      }
      try
      {
        if (!e)
        {
          long l = paramInt2;
          if (l <= 16777215L)
          {
            a.h(paramInt1 & 0x7FFFFFFF);
            a.h((i & 0xFF) << 24 | 0xFFFFFF & paramInt2);
            if (paramInt2 > 0) {
              a.a_(paramc, l);
            }
            return;
          }
          throw new IllegalArgumentException("FRAME_TOO_LARGE max size is 16Mib: ".concat(String.valueOf(paramInt2)));
        }
        throw new IOException("closed");
      }
      finally {}
    }
    
    public final void a(boolean paramBoolean, int paramInt, List<f> paramList)
      throws IOException
    {
      for (;;)
      {
        try
        {
          if (!e)
          {
            a(paramList);
            int j = (int)(b.b + 10L);
            if (paramBoolean)
            {
              i = 1;
              a.h(-2147287039);
              a.h(((i | 0x0) & 0xFF) << 24 | j & 0xFFFFFF);
              a.h(paramInt & 0x7FFFFFFF);
              a.h(0);
              a.i(0);
              a.a(b);
              a.flush();
            }
          }
          else
          {
            throw new IOException("closed");
          }
        }
        finally {}
        int i = 0;
      }
    }
    
    public final void b()
      throws IOException
    {
      try
      {
        if (!e)
        {
          a.flush();
          return;
        }
        throw new IOException("closed");
      }
      finally {}
    }
    
    public final void b(n paramn)
      throws IOException
    {
      for (;;)
      {
        int i;
        try
        {
          if (!e)
          {
            int j = Integer.bitCount(a);
            a.h(-2147287036);
            d locald = a;
            i = 0;
            locald.h(j * 8 + 4 & 0xFFFFFF | 0x0);
            a.h(j);
            if (i <= 10)
            {
              if (paramn.a(i))
              {
                j = paramn.b(i);
                a.h((j & 0xFF) << 24 | i & 0xFFFFFF);
                a.h(d[i]);
              }
            }
            else {
              a.flush();
            }
          }
          else
          {
            throw new IOException("closed");
          }
        }
        finally {}
        i += 1;
      }
    }
    
    public final int c()
    {
      return 16383;
    }
    
    public final void close()
      throws IOException
    {
      try
      {
        e = true;
        j.a(a, c);
        return;
      }
      finally
      {
        localObject = finally;
        throw ((Throwable)localObject);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */