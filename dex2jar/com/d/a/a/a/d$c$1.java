package com.d.a.a.a;

import com.d.a.a.f;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

final class d$c$1
  extends f
{
  d$c$1(d.c paramc, String paramString, Object[] paramArrayOfObject, e paramVarArgs)
  {
    super(paramString, paramArrayOfObject);
  }
  
  public final void a()
  {
    try
    {
      d.f(c.c).a(b);
      return;
    }
    catch (IOException localIOException1)
    {
      Logger localLogger = com.d.a.a.d.a;
      Level localLevel = Level.INFO;
      StringBuilder localStringBuilder = new StringBuilder("FramedConnection.Listener failure for ");
      localStringBuilder.append(d.a(c.c));
      localLogger.log(localLevel, localStringBuilder.toString(), localIOException1);
      try
      {
        b.a(a.b);
        return;
      }
      catch (IOException localIOException2) {}
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.d.c.1
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */