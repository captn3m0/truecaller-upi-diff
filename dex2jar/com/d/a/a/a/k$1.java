package com.d.a.a.a;

import d.c;
import d.i;
import d.u;
import java.io.IOException;

final class k$1
  extends i
{
  k$1(k paramk, u paramu)
  {
    super(paramu);
  }
  
  public final long a(c paramc, long paramLong)
    throws IOException
  {
    if (a.a == 0) {
      return -1L;
    }
    paramLong = super.a(paramc, Math.min(paramLong, a.a));
    if (paramLong == -1L) {
      return -1L;
    }
    paramc = a;
    a = ((int)(a - paramLong));
    return paramLong;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.k.1
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */