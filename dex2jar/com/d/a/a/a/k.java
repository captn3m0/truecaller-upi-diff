package com.d.a.a.a;

import d.c;
import d.e;
import d.i;
import d.m;
import d.n;
import d.u;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

final class k
{
  int a;
  final e b = n.a(c);
  private final m c = new m(new i(parame)new Inflater
  {
    public final long a(c paramAnonymousc, long paramAnonymousLong)
      throws IOException
    {
      if (a == 0) {
        return -1L;
      }
      paramAnonymousLong = super.a(paramAnonymousc, Math.min(paramAnonymousLong, a));
      if (paramAnonymousLong == -1L) {
        return -1L;
      }
      paramAnonymousc = k.this;
      a = ((int)(a - paramAnonymousLong));
      return paramAnonymousLong;
    }
  }, new Inflater()
  {
    public final int inflate(byte[] paramAnonymousArrayOfByte, int paramAnonymousInt1, int paramAnonymousInt2)
      throws DataFormatException
    {
      int j = super.inflate(paramAnonymousArrayOfByte, paramAnonymousInt1, paramAnonymousInt2);
      int i = j;
      if (j == 0)
      {
        i = j;
        if (needsDictionary())
        {
          setDictionary(o.a);
          i = super.inflate(paramAnonymousArrayOfByte, paramAnonymousInt1, paramAnonymousInt2);
        }
      }
      return i;
    }
  });
  
  public k(e parame) {}
  
  private d.f a()
    throws IOException
  {
    int i = b.j();
    return b.d(i);
  }
  
  public final List<f> a(int paramInt)
    throws IOException
  {
    a += paramInt;
    int i = b.j();
    if (i >= 0)
    {
      if (i <= 1024)
      {
        Object localObject = new ArrayList(i);
        paramInt = 0;
        while (paramInt < i)
        {
          d.f localf1 = a().g();
          d.f localf2 = a();
          if (localf1.h() != 0)
          {
            ((List)localObject).add(new f(localf1, localf2));
            paramInt += 1;
          }
          else
          {
            throw new IOException("name.size == 0");
          }
        }
        if (a > 0)
        {
          c.b();
          if (a == 0) {
            return (List<f>)localObject;
          }
          localObject = new StringBuilder("compressedLimit > 0: ");
          ((StringBuilder)localObject).append(a);
          throw new IOException(((StringBuilder)localObject).toString());
        }
        return (List<f>)localObject;
      }
      throw new IOException("numberOfPairs > 1024: ".concat(String.valueOf(i)));
    }
    throw new IOException("numberOfPairs < 0: ".concat(String.valueOf(i)));
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */