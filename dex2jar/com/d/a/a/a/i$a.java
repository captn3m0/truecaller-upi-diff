package com.d.a.a.a;

import d.c;
import d.e;
import d.u;
import d.v;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

final class i$a
  implements u
{
  int a;
  byte b;
  int c;
  int d;
  short e;
  private final e f;
  
  public i$a(e parame)
  {
    f = parame;
  }
  
  public final long a(c paramc, long paramLong)
    throws IOException
  {
    int i;
    byte b1;
    do
    {
      i = d;
      if (i != 0) {
        break label203;
      }
      f.h(e);
      e = 0;
      if ((b & 0x4) != 0) {
        return -1L;
      }
      i = c;
      int j = i.a(f);
      d = j;
      a = j;
      b1 = (byte)(f.h() & 0xFF);
      b = ((byte)(f.h() & 0xFF));
      if (i.b().isLoggable(Level.FINE)) {
        i.b().fine(i.b.a(true, c, a, b1, b));
      }
      c = (f.j() & 0x7FFFFFFF);
      if (b1 != 9) {
        break;
      }
    } while (c == i);
    throw i.a("TYPE_CONTINUATION streamId changed", new Object[0]);
    throw i.a("%s != TYPE_CONTINUATION", new Object[] { Byte.valueOf(b1) });
    label203:
    paramLong = f.a(paramc, Math.min(paramLong, i));
    if (paramLong == -1L) {
      return -1L;
    }
    d = ((int)(d - paramLong));
    return paramLong;
  }
  
  public final void close()
    throws IOException
  {}
  
  public final v l_()
  {
    return f.l_();
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.i.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */