package com.d.a.a.a;

import d.e;
import d.n;
import d.u;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

final class h$a
{
  int a = 4096;
  int b = 4096;
  f[] c = new f[8];
  int d = c.length - 1;
  int e = 0;
  int f = 0;
  private final List<f> g = new ArrayList();
  private final e h;
  
  h$a(u paramu)
  {
    h = n.a(paramu);
  }
  
  private int a(int paramInt)
  {
    int i = 0;
    int k = 0;
    if (paramInt > 0)
    {
      i = c.length - 1;
      int j = paramInt;
      paramInt = k;
      while ((i >= d) && (j > 0))
      {
        j -= c[i].j;
        f -= c[i].j;
        e -= 1;
        paramInt += 1;
        i -= 1;
      }
      f[] arrayOff = c;
      i = d;
      System.arraycopy(arrayOff, i + 1, arrayOff, i + 1 + paramInt, e);
      d += paramInt;
      i = paramInt;
    }
    return i;
  }
  
  private int a(int paramInt1, int paramInt2)
    throws IOException
  {
    paramInt1 &= paramInt2;
    if (paramInt1 < paramInt2) {
      return paramInt1;
    }
    paramInt1 = 0;
    int i;
    for (;;)
    {
      i = e();
      if ((i & 0x80) == 0) {
        break;
      }
      paramInt2 += ((i & 0x7F) << paramInt1);
      paramInt1 += 7;
    }
    return paramInt2 + (i << paramInt1);
  }
  
  private void a(f paramf)
  {
    g.add(paramf);
    int i = j;
    int j = b;
    if (i > j)
    {
      d();
      return;
    }
    a(f + i - j);
    j = e;
    f[] arrayOff1 = c;
    if (j + 1 > arrayOff1.length)
    {
      f[] arrayOff2 = new f[arrayOff1.length * 2];
      System.arraycopy(arrayOff1, 0, arrayOff2, arrayOff1.length, arrayOff1.length);
      d = (c.length - 1);
      c = arrayOff2;
    }
    j = d;
    d = (j - 1);
    c[j] = paramf;
    e += 1;
    f += i;
  }
  
  private int b(int paramInt)
  {
    return d + 1 + paramInt;
  }
  
  private d.f c(int paramInt)
  {
    if (d(paramInt)) {
      return ah;
    }
    return c[b(paramInt - h.a().length)].h;
  }
  
  private void d()
  {
    g.clear();
    Arrays.fill(c, null);
    d = (c.length - 1);
    e = 0;
    f = 0;
  }
  
  private static boolean d(int paramInt)
  {
    return (paramInt >= 0) && (paramInt <= h.a().length - 1);
  }
  
  private int e()
    throws IOException
  {
    return h.h() & 0xFF;
  }
  
  private d.f f()
    throws IOException
  {
    int j = e();
    int i;
    if ((j & 0x80) == 128) {
      i = 1;
    } else {
      i = 0;
    }
    j = a(j, 127);
    if (i != 0) {
      return d.f.a(j.a().a(h.g(j)));
    }
    return h.d(j);
  }
  
  final void a()
  {
    int i = b;
    int j = f;
    if (i < j)
    {
      if (i == 0)
      {
        d();
        return;
      }
      a(j - i);
    }
  }
  
  final void b()
    throws IOException
  {
    while (!h.e())
    {
      int i = h.h() & 0xFF;
      if (i != 128)
      {
        Object localObject;
        if ((i & 0x80) == 128)
        {
          i = a(i, 127) - 1;
          if (d(i))
          {
            localObject = h.a()[i];
            g.add(localObject);
          }
          else
          {
            int j = b(i - h.a().length);
            if (j >= 0)
            {
              localObject = c;
              if (j <= localObject.length - 1)
              {
                g.add(localObject[j]);
                continue;
              }
            }
            localObject = new StringBuilder("Header index too large ");
            ((StringBuilder)localObject).append(i + 1);
            throw new IOException(((StringBuilder)localObject).toString());
          }
        }
        else if (i == 64)
        {
          a(new f(h.a(f()), f()));
        }
        else if ((i & 0x40) == 64)
        {
          a(new f(c(a(i, 63) - 1), f()));
        }
        else if ((i & 0x20) == 32)
        {
          b = a(i, 31);
          i = b;
          if ((i >= 0) && (i <= a))
          {
            a();
          }
          else
          {
            localObject = new StringBuilder("Invalid dynamic table size update ");
            ((StringBuilder)localObject).append(b);
            throw new IOException(((StringBuilder)localObject).toString());
          }
        }
        else
        {
          d.f localf;
          if ((i != 16) && (i != 0))
          {
            localObject = c(a(i, 15) - 1);
            localf = f();
            g.add(new f((d.f)localObject, localf));
          }
          else
          {
            localObject = h.a(f());
            localf = f();
            g.add(new f((d.f)localObject, localf));
          }
        }
      }
      else
      {
        throw new IOException("index == 0");
      }
    }
  }
  
  public final List<f> c()
  {
    ArrayList localArrayList = new ArrayList(g);
    g.clear();
    return localArrayList;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.h.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */