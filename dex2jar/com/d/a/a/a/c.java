package com.d.a.a.a;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;

public abstract interface c
  extends Closeable
{
  public abstract void a()
    throws IOException;
  
  public abstract void a(int paramInt, long paramLong)
    throws IOException;
  
  public abstract void a(int paramInt, a parama)
    throws IOException;
  
  public abstract void a(int paramInt, a parama, byte[] paramArrayOfByte)
    throws IOException;
  
  public abstract void a(n paramn)
    throws IOException;
  
  public abstract void a(boolean paramBoolean, int paramInt1, int paramInt2)
    throws IOException;
  
  public abstract void a(boolean paramBoolean, int paramInt1, d.c paramc, int paramInt2)
    throws IOException;
  
  public abstract void a(boolean paramBoolean, int paramInt, List<f> paramList)
    throws IOException;
  
  public abstract void b()
    throws IOException;
  
  public abstract void b(n paramn)
    throws IOException;
  
  public abstract int c();
}

/* Location:
 * Qualified Name:     com.d.a.a.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */