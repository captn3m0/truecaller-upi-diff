package com.d.a.a;

import com.d.a.k;
import java.io.IOException;
import java.net.UnknownServiceException;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLSocket;

public final class a
{
  public boolean a;
  public boolean b;
  private final List<k> c;
  private int d = 0;
  
  public a(List<k> paramList)
  {
    c = paramList;
  }
  
  private boolean b(SSLSocket paramSSLSocket)
  {
    int i = d;
    while (i < c.size())
    {
      if (((k)c.get(i)).a(paramSSLSocket)) {
        return true;
      }
      i += 1;
    }
    return false;
  }
  
  public final k a(SSLSocket paramSSLSocket)
    throws IOException
  {
    int i = d;
    int j = c.size();
    while (i < j)
    {
      localObject = (k)c.get(i);
      if (((k)localObject).a(paramSSLSocket))
      {
        d = (i + 1);
        break label64;
      }
      i += 1;
    }
    Object localObject = null;
    label64:
    if (localObject != null)
    {
      a = b(paramSSLSocket);
      d.b.a((k)localObject, paramSSLSocket, b);
      return (k)localObject;
    }
    localObject = new StringBuilder("Unable to find acceptable protocols. isFallback=");
    ((StringBuilder)localObject).append(b);
    ((StringBuilder)localObject).append(", modes=");
    ((StringBuilder)localObject).append(c);
    ((StringBuilder)localObject).append(", supported protocols=");
    ((StringBuilder)localObject).append(Arrays.toString(paramSSLSocket.getEnabledProtocols()));
    throw new UnknownServiceException(((StringBuilder)localObject).toString());
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */