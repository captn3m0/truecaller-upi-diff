package com.d.a.a;

import com.d.a.a.c.a;
import d.t;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public final class b$a
{
  final b.b a;
  final boolean[] b;
  boolean c;
  private boolean e;
  
  private b$a(b paramb, b.b paramb1)
  {
    a = paramb1;
    if (e) {
      paramb = null;
    } else {
      paramb = new boolean[b.h(paramb)];
    }
    b = paramb;
  }
  
  public final t a(int paramInt)
    throws IOException
  {
    synchronized (d)
    {
      if (a.f == this)
      {
        if (!a.e) {
          b[paramInt] = true;
        }
        localObject1 = a.d[paramInt];
      }
      try
      {
        localObject1 = b.i(d).b((File)localObject1);
        localObject1 = new c((t)localObject1)
        {
          protected final void a()
          {
            synchronized (d)
            {
              c = true;
              return;
            }
          }
        };
        return (t)localObject1;
      }
      catch (FileNotFoundException localFileNotFoundException)
      {
        for (;;) {}
      }
      Object localObject1 = b.a();
      return (t)localObject1;
      throw new IllegalStateException();
    }
  }
  
  public final void a()
    throws IOException
  {
    synchronized (d)
    {
      if (c)
      {
        b.a(d, this, false);
        b.a(d, a);
      }
      else
      {
        b.a(d, this, true);
      }
      e = true;
      return;
    }
  }
  
  public final void b()
    throws IOException
  {
    synchronized (d)
    {
      b.a(d, this, false);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */