package com.d.a.a;

import com.d.a.q;
import d.f;
import d.u;
import java.io.Closeable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.Socket;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public final class j
{
  public static final byte[] a = new byte[0];
  public static final String[] b = new String[0];
  public static final Charset c = Charset.forName("UTF-8");
  
  public static f a(f paramf)
  {
    try
    {
      paramf = f.a(MessageDigest.getInstance("SHA-1").digest(paramf.i()));
      return paramf;
    }
    catch (NoSuchAlgorithmException paramf)
    {
      throw new AssertionError(paramf);
    }
  }
  
  public static String a(q paramq)
  {
    if (c != q.a(a))
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(b);
      localStringBuilder.append(":");
      localStringBuilder.append(c);
      return localStringBuilder.toString();
    }
    return b;
  }
  
  public static String a(String paramString)
  {
    try
    {
      paramString = f.a(MessageDigest.getInstance("MD5").digest(paramString.getBytes("UTF-8"))).f();
      return paramString;
    }
    catch (UnsupportedEncodingException paramString) {}catch (NoSuchAlgorithmException paramString) {}
    throw new AssertionError(paramString);
  }
  
  public static <T> List<T> a(List<T> paramList)
  {
    return Collections.unmodifiableList(new ArrayList(paramList));
  }
  
  public static <T> List<T> a(T... paramVarArgs)
  {
    return Collections.unmodifiableList(Arrays.asList((Object[])paramVarArgs.clone()));
  }
  
  public static <K, V> Map<K, V> a(Map<K, V> paramMap)
  {
    return Collections.unmodifiableMap(new LinkedHashMap(paramMap));
  }
  
  public static void a(long paramLong1, long paramLong2)
  {
    if (((paramLong2 | 0L) >= 0L) && (0L <= paramLong1) && (paramLong1 - 0L >= paramLong2)) {
      return;
    }
    throw new ArrayIndexOutOfBoundsException();
  }
  
  public static void a(Closeable paramCloseable)
  {
    if (paramCloseable != null) {}
    try
    {
      paramCloseable.close();
      return;
    }
    catch (RuntimeException paramCloseable)
    {
      throw paramCloseable;
      return;
    }
    catch (Exception paramCloseable) {}
  }
  
  public static void a(Closeable paramCloseable1, Closeable paramCloseable2)
    throws IOException
  {
    try
    {
      paramCloseable1.close();
      paramCloseable1 = null;
    }
    catch (Throwable paramCloseable1) {}
    try
    {
      paramCloseable2.close();
      paramCloseable2 = paramCloseable1;
    }
    catch (Throwable localThrowable)
    {
      paramCloseable2 = paramCloseable1;
      if (paramCloseable1 == null) {
        paramCloseable2 = localThrowable;
      }
    }
    if (paramCloseable2 == null) {
      return;
    }
    if (!(paramCloseable2 instanceof IOException))
    {
      if (!(paramCloseable2 instanceof RuntimeException))
      {
        if ((paramCloseable2 instanceof Error)) {
          throw ((Error)paramCloseable2);
        }
        throw new AssertionError(paramCloseable2);
      }
      throw ((RuntimeException)paramCloseable2);
    }
    throw ((IOException)paramCloseable2);
  }
  
  public static void a(Socket paramSocket)
  {
    if (paramSocket != null) {}
    try
    {
      paramSocket.close();
      return;
    }
    catch (RuntimeException paramSocket)
    {
      throw paramSocket;
    }
    catch (AssertionError paramSocket)
    {
      if (a(paramSocket)) {
        return;
      }
      throw paramSocket;
      return;
    }
    catch (Exception paramSocket) {}
  }
  
  /* Error */
  public static boolean a(u paramu, int paramInt, TimeUnit paramTimeUnit)
    throws IOException
  {
    // Byte code:
    //   0: invokestatic 178	java/lang/System:nanoTime	()J
    //   3: lstore 5
    //   5: aload_0
    //   6: invokeinterface 184 1 0
    //   11: invokevirtual 190	d/v:p_	()Z
    //   14: ifeq +19 -> 33
    //   17: aload_0
    //   18: invokeinterface 184 1 0
    //   23: invokevirtual 192	d/v:c	()J
    //   26: lload 5
    //   28: lsub
    //   29: lstore_3
    //   30: goto +7 -> 37
    //   33: ldc2_w 193
    //   36: lstore_3
    //   37: aload_0
    //   38: invokeinterface 184 1 0
    //   43: lload_3
    //   44: aload_2
    //   45: iload_1
    //   46: i2l
    //   47: invokevirtual 200	java/util/concurrent/TimeUnit:toNanos	(J)J
    //   50: invokestatic 206	java/lang/Math:min	(JJ)J
    //   53: lload 5
    //   55: ladd
    //   56: invokevirtual 209	d/v:a	(J)Ld/v;
    //   59: pop
    //   60: new 211	d/c
    //   63: dup
    //   64: invokespecial 212	d/c:<init>	()V
    //   67: astore_2
    //   68: aload_0
    //   69: aload_2
    //   70: ldc2_w 213
    //   73: invokeinterface 217 4 0
    //   78: ldc2_w 218
    //   81: lcmp
    //   82: ifeq +10 -> 92
    //   85: aload_2
    //   86: invokevirtual 222	d/c:s	()V
    //   89: goto -21 -> 68
    //   92: lload_3
    //   93: ldc2_w 193
    //   96: lcmp
    //   97: ifne +16 -> 113
    //   100: aload_0
    //   101: invokeinterface 184 1 0
    //   106: invokevirtual 225	d/v:q_	()Ld/v;
    //   109: pop
    //   110: goto +17 -> 127
    //   113: aload_0
    //   114: invokeinterface 184 1 0
    //   119: lload 5
    //   121: lload_3
    //   122: ladd
    //   123: invokevirtual 209	d/v:a	(J)Ld/v;
    //   126: pop
    //   127: iconst_1
    //   128: ireturn
    //   129: astore_2
    //   130: lload_3
    //   131: ldc2_w 193
    //   134: lcmp
    //   135: ifne +16 -> 151
    //   138: aload_0
    //   139: invokeinterface 184 1 0
    //   144: invokevirtual 225	d/v:q_	()Ld/v;
    //   147: pop
    //   148: goto +17 -> 165
    //   151: aload_0
    //   152: invokeinterface 184 1 0
    //   157: lload 5
    //   159: lload_3
    //   160: ladd
    //   161: invokevirtual 209	d/v:a	(J)Ld/v;
    //   164: pop
    //   165: aload_2
    //   166: athrow
    //   167: lload_3
    //   168: ldc2_w 193
    //   171: lcmp
    //   172: ifne +16 -> 188
    //   175: aload_0
    //   176: invokeinterface 184 1 0
    //   181: invokevirtual 225	d/v:q_	()Ld/v;
    //   184: pop
    //   185: goto +17 -> 202
    //   188: aload_0
    //   189: invokeinterface 184 1 0
    //   194: lload 5
    //   196: lload_3
    //   197: ladd
    //   198: invokevirtual 209	d/v:a	(J)Ld/v;
    //   201: pop
    //   202: iconst_0
    //   203: ireturn
    //   204: astore_2
    //   205: goto -38 -> 167
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	208	0	paramu	u
    //   0	208	1	paramInt	int
    //   0	208	2	paramTimeUnit	TimeUnit
    //   29	168	3	l1	long
    //   3	192	5	l2	long
    // Exception table:
    //   from	to	target	type
    //   60	68	129	finally
    //   68	89	129	finally
    //   60	68	204	java/io/InterruptedIOException
    //   68	89	204	java/io/InterruptedIOException
  }
  
  public static boolean a(u paramu, TimeUnit paramTimeUnit)
  {
    try
    {
      boolean bool = a(paramu, 100, paramTimeUnit);
      return bool;
    }
    catch (IOException paramu)
    {
      for (;;) {}
    }
    return false;
  }
  
  public static boolean a(AssertionError paramAssertionError)
  {
    return (paramAssertionError.getCause() != null) && (paramAssertionError.getMessage() != null) && (paramAssertionError.getMessage().contains("getsockname failed"));
  }
  
  public static boolean a(Object paramObject1, Object paramObject2)
  {
    return (paramObject1 == paramObject2) || ((paramObject1 != null) && (paramObject1.equals(paramObject2)));
  }
  
  public static boolean a(String[] paramArrayOfString, String paramString)
  {
    return Arrays.asList(paramArrayOfString).contains(paramString);
  }
  
  public static <T> T[] a(Class<T> paramClass, T[] paramArrayOfT1, T[] paramArrayOfT2)
  {
    ArrayList localArrayList = new ArrayList();
    int k = paramArrayOfT1.length;
    int i = 0;
    while (i < k)
    {
      T ? = paramArrayOfT1[i];
      int m = paramArrayOfT2.length;
      int j = 0;
      while (j < m)
      {
        T ? = paramArrayOfT2[j];
        if (?.equals(?))
        {
          localArrayList.add(?);
          break;
        }
        j += 1;
      }
      i += 1;
    }
    return localArrayList.toArray((Object[])Array.newInstance(paramClass, localArrayList.size()));
  }
  
  public static ThreadFactory b(String paramString)
  {
    new ThreadFactory()
    {
      public final Thread newThread(Runnable paramAnonymousRunnable)
      {
        paramAnonymousRunnable = new Thread(paramAnonymousRunnable, a);
        paramAnonymousRunnable.setDaemon(b);
        return paramAnonymousRunnable;
      }
    };
  }
  
  public static String[] b(String[] paramArrayOfString, String paramString)
  {
    String[] arrayOfString = new String[paramArrayOfString.length + 1];
    System.arraycopy(paramArrayOfString, 0, arrayOfString, 0, paramArrayOfString.length);
    arrayOfString[(arrayOfString.length - 1)] = paramString;
    return arrayOfString;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */