package com.d.a.a;

import com.d.a.z;
import java.util.LinkedHashSet;
import java.util.Set;

public final class i
{
  private final Set<z> a = new LinkedHashSet();
  
  public final void a(z paramz)
  {
    try
    {
      a.add(paramz);
      return;
    }
    finally
    {
      paramz = finally;
      throw paramz;
    }
  }
  
  public final void b(z paramz)
  {
    try
    {
      a.remove(paramz);
      return;
    }
    finally
    {
      paramz = finally;
      throw paramz;
    }
  }
  
  public final boolean c(z paramz)
  {
    try
    {
      boolean bool = a.contains(paramz);
      return bool;
    }
    finally
    {
      paramz = finally;
      throw paramz;
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */