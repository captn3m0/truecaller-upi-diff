package com.d.a.a;

import d.h;
import d.t;
import java.io.IOException;

class c
  extends h
{
  private boolean a;
  
  public c(t paramt)
  {
    super(paramt);
  }
  
  protected void a() {}
  
  public final void a_(d.c paramc, long paramLong)
    throws IOException
  {
    if (a)
    {
      paramc.h(paramLong);
      return;
    }
    try
    {
      super.a_(paramc, paramLong);
      return;
    }
    catch (IOException paramc)
    {
      for (;;) {}
    }
    a = true;
    a();
  }
  
  public void close()
    throws IOException
  {
    if (a) {
      return;
    }
    try
    {
      super.close();
      return;
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
    a = true;
    a();
  }
  
  public void flush()
    throws IOException
  {
    if (a) {
      return;
    }
    try
    {
      super.flush();
      return;
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
    a = true;
    a();
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */