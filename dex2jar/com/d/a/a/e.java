package com.d.a.a;

import com.d.a.a.b.b;
import com.d.a.a.b.c;
import com.d.a.v;
import com.d.a.x;
import java.io.IOException;

public abstract interface e
{
  public abstract b a(x paramx)
    throws IOException;
  
  public abstract x a(v paramv)
    throws IOException;
  
  public abstract void a();
  
  public abstract void a(c paramc);
  
  public abstract void a(x paramx1, x paramx2)
    throws IOException;
  
  public abstract void b(v paramv)
    throws IOException;
}

/* Location:
 * Qualified Name:     com.d.a.a.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */