package com.d.a.a;

import com.d.a.a.c.a;
import d.d;
import d.u;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

final class b$b
{
  final String a;
  final long[] b;
  final File[] c;
  final File[] d;
  boolean e;
  b.a f;
  long g;
  
  private b$b(b paramb, String paramString)
  {
    a = paramString;
    b = new long[b.h(paramb)];
    c = new File[b.h(paramb)];
    d = new File[b.h(paramb)];
    paramString = new StringBuilder(paramString);
    paramString.append('.');
    int j = paramString.length();
    int i = 0;
    while (i < b.h(paramb))
    {
      paramString.append(i);
      c[i] = new File(b.j(paramb), paramString.toString());
      paramString.append(".tmp");
      d[i] = new File(b.j(paramb), paramString.toString());
      paramString.setLength(j);
      i += 1;
    }
  }
  
  private static IOException b(String[] paramArrayOfString)
    throws IOException
  {
    StringBuilder localStringBuilder = new StringBuilder("unexpected journal line: ");
    localStringBuilder.append(Arrays.toString(paramArrayOfString));
    throw new IOException(localStringBuilder.toString());
  }
  
  final b.c a()
  {
    if (Thread.holdsLock(h))
    {
      u[] arrayOfu = new u[b.h(h)];
      Object localObject = (long[])b.clone();
      j = 0;
      i = 0;
      for (;;)
      {
        try
        {
          if (i < b.h(h))
          {
            arrayOfu[i] = b.i(h).a(c[i]);
            i += 1;
            continue;
          }
          localObject = new b.c(h, a, g, arrayOfu, (long[])localObject, (byte)0);
          return (b.c)localObject;
        }
        catch (FileNotFoundException localFileNotFoundException)
        {
          i = j;
          continue;
        }
        if ((i >= b.h(h)) || (arrayOfu[i] == null)) {
          continue;
        }
        j.a(arrayOfu[i]);
        i += 1;
      }
      return null;
    }
    throw new AssertionError();
  }
  
  final void a(d paramd)
    throws IOException
  {
    long[] arrayOfLong = b;
    int j = arrayOfLong.length;
    int i = 0;
    while (i < j)
    {
      long l = arrayOfLong[i];
      paramd.j(32).l(l);
      i += 1;
    }
  }
  
  final void a(String[] paramArrayOfString)
    throws IOException
  {
    int i;
    if (paramArrayOfString.length == b.h(h)) {
      i = 0;
    }
    try
    {
      while (i < paramArrayOfString.length)
      {
        b[i] = Long.parseLong(paramArrayOfString[i]);
        i += 1;
      }
      return;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      for (;;) {}
    }
    throw b(paramArrayOfString);
    throw b(paramArrayOfString);
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */