package com.d.a.a.c;

import com.d.a.a;
import com.d.a.a.b.k;
import com.d.a.a.b.s;
import com.d.a.a.d.f;
import com.d.a.a.h;
import com.d.a.a.j;
import com.d.a.i;
import com.d.a.o;
import com.d.a.q;
import com.d.a.v.a;
import com.d.a.x;
import com.d.a.x.a;
import com.d.a.z;
import d.c;
import java.io.IOException;
import java.lang.ref.Reference;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

public final class b
  implements i
{
  private static SSLSocketFactory m;
  private static f n;
  public final z a;
  public Socket b;
  public o c;
  public volatile com.d.a.a.a.d d;
  public int e;
  public d.e f;
  public d.d g;
  public final List<Reference<s>> h = new ArrayList();
  public boolean i;
  public long j = Long.MAX_VALUE;
  private Socket k;
  private com.d.a.u l;
  
  public b(z paramz)
  {
    a = paramz;
  }
  
  private static f a(SSLSocketFactory paramSSLSocketFactory)
  {
    try
    {
      if (paramSSLSocketFactory != m)
      {
        X509TrustManager localX509TrustManager = h.a().a(paramSSLSocketFactory);
        n = h.a().a(localX509TrustManager);
        m = paramSSLSocketFactory;
      }
      paramSSLSocketFactory = n;
      return paramSSLSocketFactory;
    }
    finally {}
  }
  
  private void a(int paramInt1, int paramInt2)
    throws IOException
  {
    Object localObject1 = new v.a().a(a.a.a).a("Host", j.a(a.a.a)).a("Proxy-Connection", "Keep-Alive").a("User-Agent", "okhttp/2.7.5").a();
    Object localObject2 = a;
    Object localObject3 = new StringBuilder("CONNECT ");
    ((StringBuilder)localObject3).append(b);
    ((StringBuilder)localObject3).append(":");
    ((StringBuilder)localObject3).append(c);
    ((StringBuilder)localObject3).append(" HTTP/1.1");
    localObject2 = ((StringBuilder)localObject3).toString();
    do
    {
      localObject3 = new com.d.a.a.b.e(null, f, g);
      f.l_().a(paramInt1, TimeUnit.MILLISECONDS);
      g.l_().a(paramInt2, TimeUnit.MILLISECONDS);
      ((com.d.a.a.b.e)localObject3).a(c, (String)localObject2);
      ((com.d.a.a.b.e)localObject3).b();
      x.a locala = ((com.d.a.a.b.e)localObject3).c();
      a = ((com.d.a.v)localObject1);
      localObject1 = locala.a();
      long l2 = k.a((x)localObject1);
      long l1 = l2;
      if (l2 == -1L) {
        l1 = 0L;
      }
      localObject3 = ((com.d.a.a.b.e)localObject3).a(l1);
      j.a((d.u)localObject3, Integer.MAX_VALUE, TimeUnit.MILLISECONDS);
      ((d.u)localObject3).close();
      int i1 = c;
      if (i1 == 200) {
        break label359;
      }
      if (i1 != 407) {
        break;
      }
      localObject1 = k.a(a.a.d, (x)localObject1, a.b);
    } while (localObject1 != null);
    throw new IOException("Failed to authenticate with proxy");
    localObject2 = new StringBuilder("Unexpected response code for CONNECT: ");
    ((StringBuilder)localObject2).append(c);
    throw new IOException(((StringBuilder)localObject2).toString());
    label359:
    if ((f.b().e()) && (g.b().e())) {
      return;
    }
    throw new IOException("TLS tunnel buffered too many bytes!");
  }
  
  public final z a()
  {
    return a;
  }
  
  /* Error */
  public final void a(int paramInt1, int paramInt2, int paramInt3, List<com.d.a.k> paramList, boolean paramBoolean)
    throws com.d.a.a.b.p
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 249	com/d/a/a/c/b:l	Lcom/d/a/u;
    //   4: ifnonnull +1237 -> 1241
    //   7: new 251	com/d/a/a/a
    //   10: dup
    //   11: aload 4
    //   13: invokespecial 254	com/d/a/a/a:<init>	(Ljava/util/List;)V
    //   16: astore 12
    //   18: aload_0
    //   19: getfield 50	com/d/a/a/c/b:a	Lcom/d/a/z;
    //   22: getfield 215	com/d/a/z:b	Ljava/net/Proxy;
    //   25: astore 13
    //   27: aload_0
    //   28: getfield 50	com/d/a/a/c/b:a	Lcom/d/a/z;
    //   31: getfield 78	com/d/a/z:a	Lcom/d/a/a;
    //   34: astore 14
    //   36: aload_0
    //   37: getfield 50	com/d/a/a/c/b:a	Lcom/d/a/z;
    //   40: getfield 78	com/d/a/z:a	Lcom/d/a/a;
    //   43: getfield 256	com/d/a/a:i	Ljavax/net/ssl/SSLSocketFactory;
    //   46: ifnonnull +45 -> 91
    //   49: aload 4
    //   51: getstatic 261	com/d/a/k:c	Lcom/d/a/k;
    //   54: invokeinterface 267 2 0
    //   59: ifeq +6 -> 65
    //   62: goto +29 -> 91
    //   65: new 239	com/d/a/a/b/p
    //   68: dup
    //   69: new 269	java/net/UnknownServiceException
    //   72: dup
    //   73: ldc_w 271
    //   76: aload 4
    //   78: invokestatic 277	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   81: invokevirtual 281	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   84: invokespecial 282	java/net/UnknownServiceException:<init>	(Ljava/lang/String;)V
    //   87: invokespecial 285	com/d/a/a/b/p:<init>	(Ljava/io/IOException;)V
    //   90: athrow
    //   91: aconst_null
    //   92: astore 10
    //   94: aload_0
    //   95: getfield 249	com/d/a/a/c/b:l	Lcom/d/a/u;
    //   98: ifnonnull +1142 -> 1240
    //   101: aload 13
    //   103: invokevirtual 291	java/net/Proxy:type	()Ljava/net/Proxy$Type;
    //   106: getstatic 297	java/net/Proxy$Type:DIRECT	Ljava/net/Proxy$Type;
    //   109: if_acmpeq +31 -> 140
    //   112: aload 13
    //   114: invokevirtual 291	java/net/Proxy:type	()Ljava/net/Proxy$Type;
    //   117: getstatic 300	java/net/Proxy$Type:HTTP	Ljava/net/Proxy$Type;
    //   120: if_acmpne +6 -> 126
    //   123: goto +17 -> 140
    //   126: new 302	java/net/Socket
    //   129: dup
    //   130: aload 13
    //   132: invokespecial 305	java/net/Socket:<init>	(Ljava/net/Proxy;)V
    //   135: astore 4
    //   137: goto +13 -> 150
    //   140: aload 14
    //   142: getfield 308	com/d/a/a:c	Ljavax/net/SocketFactory;
    //   145: invokevirtual 314	javax/net/SocketFactory:createSocket	()Ljava/net/Socket;
    //   148: astore 4
    //   150: aload_0
    //   151: aload 4
    //   153: putfield 316	com/d/a/a/c/b:k	Ljava/net/Socket;
    //   156: aload_0
    //   157: getfield 316	com/d/a/a/c/b:k	Ljava/net/Socket;
    //   160: iload_2
    //   161: invokevirtual 320	java/net/Socket:setSoTimeout	(I)V
    //   164: invokestatic 59	com/d/a/a/h:a	()Lcom/d/a/a/h;
    //   167: astore 4
    //   169: aload_0
    //   170: getfield 316	com/d/a/a/c/b:k	Ljava/net/Socket;
    //   173: astore 9
    //   175: aload_0
    //   176: getfield 50	com/d/a/a/c/b:a	Lcom/d/a/z;
    //   179: getfield 323	com/d/a/z:c	Ljava/net/InetSocketAddress;
    //   182: astore 11
    //   184: aload 4
    //   186: aload 9
    //   188: aload 11
    //   190: iload_1
    //   191: invokevirtual 326	com/d/a/a/h:a	(Ljava/net/Socket;Ljava/net/InetSocketAddress;I)V
    //   194: aload_0
    //   195: aload_0
    //   196: getfield 316	com/d/a/a/c/b:k	Ljava/net/Socket;
    //   199: invokestatic 331	d/n:b	(Ljava/net/Socket;)Ld/u;
    //   202: invokestatic 334	d/n:a	(Ld/u;)Ld/e;
    //   205: putfield 143	com/d/a/a/c/b:f	Ld/e;
    //   208: aload_0
    //   209: aload_0
    //   210: getfield 316	com/d/a/a/c/b:k	Ljava/net/Socket;
    //   213: invokestatic 337	d/n:a	(Ljava/net/Socket;)Ld/t;
    //   216: invokestatic 340	d/n:a	(Ld/t;)Ld/d;
    //   219: putfield 145	com/d/a/a/c/b:g	Ld/d;
    //   222: aload_0
    //   223: getfield 50	com/d/a/a/c/b:a	Lcom/d/a/z;
    //   226: getfield 78	com/d/a/z:a	Lcom/d/a/a;
    //   229: getfield 256	com/d/a/a:i	Ljavax/net/ssl/SSLSocketFactory;
    //   232: ifnull +551 -> 783
    //   235: aload_0
    //   236: getfield 50	com/d/a/a/c/b:a	Lcom/d/a/z;
    //   239: astore 4
    //   241: aload 4
    //   243: getfield 78	com/d/a/z:a	Lcom/d/a/a;
    //   246: getfield 256	com/d/a/a:i	Ljavax/net/ssl/SSLSocketFactory;
    //   249: ifnull +31 -> 280
    //   252: aload 4
    //   254: getfield 215	com/d/a/z:b	Ljava/net/Proxy;
    //   257: invokevirtual 291	java/net/Proxy:type	()Ljava/net/Proxy$Type;
    //   260: astore 4
    //   262: getstatic 300	java/net/Proxy$Type:HTTP	Ljava/net/Proxy$Type;
    //   265: astore 9
    //   267: aload 4
    //   269: aload 9
    //   271: if_acmpne +9 -> 280
    //   274: iconst_1
    //   275: istore 6
    //   277: goto +6 -> 283
    //   280: iconst_0
    //   281: istore 6
    //   283: iload 6
    //   285: ifeq +982 -> 1267
    //   288: aload_0
    //   289: iload_2
    //   290: iload_3
    //   291: invokespecial 342	com/d/a/a/c/b:a	(II)V
    //   294: goto +3 -> 297
    //   297: aload_0
    //   298: getfield 50	com/d/a/a/c/b:a	Lcom/d/a/z;
    //   301: getfield 78	com/d/a/z:a	Lcom/d/a/a;
    //   304: astore 4
    //   306: aload 4
    //   308: getfield 256	com/d/a/a:i	Ljavax/net/ssl/SSLSocketFactory;
    //   311: astore 9
    //   313: aload 9
    //   315: aload_0
    //   316: getfield 316	com/d/a/a/c/b:k	Ljava/net/Socket;
    //   319: aload 4
    //   321: getfield 83	com/d/a/a:a	Lcom/d/a/q;
    //   324: getfield 122	com/d/a/q:b	Ljava/lang/String;
    //   327: aload 4
    //   329: getfield 83	com/d/a/a:a	Lcom/d/a/q;
    //   332: getfield 130	com/d/a/q:c	I
    //   335: iconst_1
    //   336: invokevirtual 347	javax/net/ssl/SSLSocketFactory:createSocket	(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;
    //   339: checkcast 349	javax/net/ssl/SSLSocket
    //   342: astore 9
    //   344: aload 12
    //   346: aload 9
    //   348: invokevirtual 352	com/d/a/a/a:a	(Ljavax/net/ssl/SSLSocket;)Lcom/d/a/k;
    //   351: astore 15
    //   353: aload 15
    //   355: getfield 354	com/d/a/k:e	Z
    //   358: ifeq +24 -> 382
    //   361: invokestatic 59	com/d/a/a/h:a	()Lcom/d/a/a/h;
    //   364: aload 9
    //   366: aload 4
    //   368: getfield 83	com/d/a/a:a	Lcom/d/a/q;
    //   371: getfield 122	com/d/a/q:b	Ljava/lang/String;
    //   374: aload 4
    //   376: getfield 356	com/d/a/a:e	Ljava/util/List;
    //   379: invokevirtual 359	com/d/a/a/h:a	(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;Ljava/util/List;)V
    //   382: aload 9
    //   384: invokevirtual 362	javax/net/ssl/SSLSocket:startHandshake	()V
    //   387: aload 9
    //   389: invokevirtual 366	javax/net/ssl/SSLSocket:getSession	()Ljavax/net/ssl/SSLSession;
    //   392: invokestatic 371	com/d/a/o:a	(Ljavax/net/ssl/SSLSession;)Lcom/d/a/o;
    //   395: astore 11
    //   397: aload 4
    //   399: getfield 374	com/d/a/a:j	Ljavax/net/ssl/HostnameVerifier;
    //   402: aload 4
    //   404: getfield 83	com/d/a/a:a	Lcom/d/a/q;
    //   407: getfield 122	com/d/a/q:b	Ljava/lang/String;
    //   410: aload 9
    //   412: invokevirtual 366	javax/net/ssl/SSLSocket:getSession	()Ljavax/net/ssl/SSLSession;
    //   415: invokeinterface 380 3 0
    //   420: ifeq +160 -> 580
    //   423: aload 4
    //   425: getfield 383	com/d/a/a:k	Lcom/d/a/f;
    //   428: getstatic 387	com/d/a/f:a	Lcom/d/a/f;
    //   431: if_acmpeq +46 -> 477
    //   434: new 389	com/d/a/a/d/b
    //   437: dup
    //   438: aload 4
    //   440: getfield 256	com/d/a/a:i	Ljavax/net/ssl/SSLSocketFactory;
    //   443: invokestatic 391	com/d/a/a/c/b:a	(Ljavax/net/ssl/SSLSocketFactory;)Lcom/d/a/a/d/f;
    //   446: invokespecial 394	com/d/a/a/d/b:<init>	(Lcom/d/a/a/d/f;)V
    //   449: aload 11
    //   451: getfield 396	com/d/a/o:b	Ljava/util/List;
    //   454: invokevirtual 399	com/d/a/a/d/b:a	(Ljava/util/List;)Ljava/util/List;
    //   457: astore 16
    //   459: aload 4
    //   461: getfield 383	com/d/a/a:k	Lcom/d/a/f;
    //   464: aload 4
    //   466: getfield 83	com/d/a/a:a	Lcom/d/a/q;
    //   469: getfield 122	com/d/a/q:b	Ljava/lang/String;
    //   472: aload 16
    //   474: invokevirtual 402	com/d/a/f:a	(Ljava/lang/String;Ljava/util/List;)V
    //   477: aload 15
    //   479: getfield 354	com/d/a/k:e	Z
    //   482: ifeq +788 -> 1270
    //   485: invokestatic 59	com/d/a/a/h:a	()Lcom/d/a/a/h;
    //   488: aload 9
    //   490: invokevirtual 405	com/d/a/a/h:b	(Ljavax/net/ssl/SSLSocket;)Ljava/lang/String;
    //   493: astore 4
    //   495: goto +3 -> 498
    //   498: aload_0
    //   499: aload 9
    //   501: putfield 407	com/d/a/a/c/b:b	Ljava/net/Socket;
    //   504: aload_0
    //   505: aload_0
    //   506: getfield 407	com/d/a/a/c/b:b	Ljava/net/Socket;
    //   509: invokestatic 331	d/n:b	(Ljava/net/Socket;)Ld/u;
    //   512: invokestatic 334	d/n:a	(Ld/u;)Ld/e;
    //   515: putfield 143	com/d/a/a/c/b:f	Ld/e;
    //   518: aload_0
    //   519: aload_0
    //   520: getfield 407	com/d/a/a/c/b:b	Ljava/net/Socket;
    //   523: invokestatic 337	d/n:a	(Ljava/net/Socket;)Ld/t;
    //   526: invokestatic 340	d/n:a	(Ld/t;)Ld/d;
    //   529: putfield 145	com/d/a/a/c/b:g	Ld/d;
    //   532: aload_0
    //   533: aload 11
    //   535: putfield 409	com/d/a/a/c/b:c	Lcom/d/a/o;
    //   538: aload 4
    //   540: ifnull +13 -> 553
    //   543: aload 4
    //   545: invokestatic 414	com/d/a/u:a	(Ljava/lang/String;)Lcom/d/a/u;
    //   548: astore 4
    //   550: goto +8 -> 558
    //   553: getstatic 416	com/d/a/u:b	Lcom/d/a/u;
    //   556: astore 4
    //   558: aload_0
    //   559: aload 4
    //   561: putfield 249	com/d/a/a/c/b:l	Lcom/d/a/u;
    //   564: aload 9
    //   566: ifnull +232 -> 798
    //   569: invokestatic 59	com/d/a/a/h:a	()Lcom/d/a/a/h;
    //   572: aload 9
    //   574: invokevirtual 419	com/d/a/a/h:a	(Ljavax/net/ssl/SSLSocket;)V
    //   577: goto +221 -> 798
    //   580: aload 11
    //   582: getfield 396	com/d/a/o:b	Ljava/util/List;
    //   585: iconst_0
    //   586: invokeinterface 423 2 0
    //   591: checkcast 425	java/security/cert/X509Certificate
    //   594: astore 11
    //   596: new 112	java/lang/StringBuilder
    //   599: dup
    //   600: ldc_w 427
    //   603: invokespecial 117	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   606: astore 15
    //   608: aload 15
    //   610: aload 4
    //   612: getfield 83	com/d/a/a:a	Lcom/d/a/q;
    //   615: getfield 122	com/d/a/q:b	Ljava/lang/String;
    //   618: invokevirtual 126	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   621: pop
    //   622: aload 15
    //   624: ldc_w 429
    //   627: invokevirtual 126	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   630: pop
    //   631: aload 15
    //   633: aload 11
    //   635: invokestatic 432	com/d/a/f:a	(Ljava/security/cert/Certificate;)Ljava/lang/String;
    //   638: invokevirtual 126	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   641: pop
    //   642: aload 15
    //   644: ldc_w 434
    //   647: invokevirtual 126	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   650: pop
    //   651: aload 15
    //   653: aload 11
    //   655: invokevirtual 438	java/security/cert/X509Certificate:getSubjectDN	()Ljava/security/Principal;
    //   658: invokeinterface 443 1 0
    //   663: invokevirtual 126	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   666: pop
    //   667: aload 15
    //   669: ldc_w 445
    //   672: invokevirtual 126	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   675: pop
    //   676: aload 15
    //   678: aload 11
    //   680: invokestatic 450	com/d/a/a/d/d:a	(Ljava/security/cert/X509Certificate;)Ljava/util/List;
    //   683: invokevirtual 453	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   686: pop
    //   687: new 455	javax/net/ssl/SSLPeerUnverifiedException
    //   690: dup
    //   691: aload 15
    //   693: invokevirtual 139	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   696: invokespecial 456	javax/net/ssl/SSLPeerUnverifiedException:<init>	(Ljava/lang/String;)V
    //   699: athrow
    //   700: astore 4
    //   702: goto +60 -> 762
    //   705: astore 11
    //   707: aload 9
    //   709: astore 4
    //   711: aload 11
    //   713: astore 9
    //   715: goto +16 -> 731
    //   718: astore 4
    //   720: aconst_null
    //   721: astore 9
    //   723: goto +39 -> 762
    //   726: astore 9
    //   728: aconst_null
    //   729: astore 4
    //   731: aload 9
    //   733: invokestatic 459	com/d/a/a/j:a	(Ljava/lang/AssertionError;)Z
    //   736: ifeq +13 -> 749
    //   739: new 70	java/io/IOException
    //   742: dup
    //   743: aload 9
    //   745: invokespecial 462	java/io/IOException:<init>	(Ljava/lang/Throwable;)V
    //   748: athrow
    //   749: aload 9
    //   751: athrow
    //   752: astore 11
    //   754: aload 4
    //   756: astore 9
    //   758: aload 11
    //   760: astore 4
    //   762: aload 9
    //   764: ifnull +11 -> 775
    //   767: invokestatic 59	com/d/a/a/h:a	()Lcom/d/a/a/h;
    //   770: aload 9
    //   772: invokevirtual 419	com/d/a/a/h:a	(Ljavax/net/ssl/SSLSocket;)V
    //   775: aload 9
    //   777: invokestatic 465	com/d/a/a/j:a	(Ljava/net/Socket;)V
    //   780: aload 4
    //   782: athrow
    //   783: aload_0
    //   784: getstatic 416	com/d/a/u:b	Lcom/d/a/u;
    //   787: putfield 249	com/d/a/a/c/b:l	Lcom/d/a/u;
    //   790: aload_0
    //   791: aload_0
    //   792: getfield 316	com/d/a/a/c/b:k	Ljava/net/Socket;
    //   795: putfield 407	com/d/a/a/c/b:b	Ljava/net/Socket;
    //   798: aload_0
    //   799: getfield 249	com/d/a/a/c/b:l	Lcom/d/a/u;
    //   802: getstatic 467	com/d/a/u:c	Lcom/d/a/u;
    //   805: if_acmpeq +13 -> 818
    //   808: aload_0
    //   809: getfield 249	com/d/a/a/c/b:l	Lcom/d/a/u;
    //   812: getstatic 469	com/d/a/u:d	Lcom/d/a/u;
    //   815: if_acmpne -721 -> 94
    //   818: aload_0
    //   819: getfield 407	com/d/a/a/c/b:b	Ljava/net/Socket;
    //   822: iconst_0
    //   823: invokevirtual 320	java/net/Socket:setSoTimeout	(I)V
    //   826: new 471	com/d/a/a/a/d$a
    //   829: dup
    //   830: invokespecial 472	com/d/a/a/a/d$a:<init>	()V
    //   833: astore 4
    //   835: aload_0
    //   836: getfield 407	com/d/a/a/c/b:b	Ljava/net/Socket;
    //   839: astore 9
    //   841: aload_0
    //   842: getfield 50	com/d/a/a/c/b:a	Lcom/d/a/z;
    //   845: getfield 78	com/d/a/z:a	Lcom/d/a/a;
    //   848: getfield 83	com/d/a/a:a	Lcom/d/a/q;
    //   851: getfield 122	com/d/a/q:b	Ljava/lang/String;
    //   854: astore 11
    //   856: aload_0
    //   857: getfield 143	com/d/a/a/c/b:f	Ld/e;
    //   860: astore 15
    //   862: aload_0
    //   863: getfield 145	com/d/a/a/c/b:g	Ld/d;
    //   866: astore 16
    //   868: aload 4
    //   870: aload 9
    //   872: putfield 474	com/d/a/a/a/d$a:a	Ljava/net/Socket;
    //   875: aload 4
    //   877: aload 11
    //   879: putfield 475	com/d/a/a/a/d$a:b	Ljava/lang/String;
    //   882: aload 4
    //   884: aload 15
    //   886: putfield 477	com/d/a/a/a/d$a:c	Ld/e;
    //   889: aload 4
    //   891: aload 16
    //   893: putfield 479	com/d/a/a/a/d$a:d	Ld/d;
    //   896: aload 4
    //   898: aload_0
    //   899: getfield 249	com/d/a/a/c/b:l	Lcom/d/a/u;
    //   902: putfield 481	com/d/a/a/a/d$a:f	Lcom/d/a/u;
    //   905: new 483	com/d/a/a/a/d
    //   908: dup
    //   909: aload 4
    //   911: iconst_0
    //   912: invokespecial 486	com/d/a/a/a/d:<init>	(Lcom/d/a/a/a/d$a;B)V
    //   915: astore 4
    //   917: aload 4
    //   919: getfield 489	com/d/a/a/a/d:i	Lcom/d/a/a/a/c;
    //   922: invokeinterface 493 1 0
    //   927: aload 4
    //   929: getfield 489	com/d/a/a/a/d:i	Lcom/d/a/a/a/c;
    //   932: aload 4
    //   934: getfield 496	com/d/a/a/a/d:e	Lcom/d/a/a/a/n;
    //   937: invokeinterface 499 2 0
    //   942: aload 4
    //   944: getfield 496	com/d/a/a/a/d:e	Lcom/d/a/a/a/n;
    //   947: invokevirtual 504	com/d/a/a/a/n:b	()I
    //   950: istore 6
    //   952: iload 6
    //   954: ldc_w 505
    //   957: if_icmpeq +21 -> 978
    //   960: aload 4
    //   962: getfield 489	com/d/a/a/a/d:i	Lcom/d/a/a/a/c;
    //   965: iconst_0
    //   966: iload 6
    //   968: ldc_w 505
    //   971: isub
    //   972: i2l
    //   973: invokeinterface 508 4 0
    //   978: aload_0
    //   979: aload 4
    //   981: putfield 510	com/d/a/a/c/b:d	Lcom/d/a/a/a/d;
    //   984: goto -890 -> 94
    //   987: new 112	java/lang/StringBuilder
    //   990: dup
    //   991: ldc_w 512
    //   994: invokespecial 117	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   997: astore 4
    //   999: aload 4
    //   1001: aload_0
    //   1002: getfield 50	com/d/a/a/c/b:a	Lcom/d/a/z;
    //   1005: getfield 323	com/d/a/z:c	Ljava/net/InetSocketAddress;
    //   1008: invokevirtual 453	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1011: pop
    //   1012: new 241	java/net/ConnectException
    //   1015: dup
    //   1016: aload 4
    //   1018: invokevirtual 139	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1021: invokespecial 513	java/net/ConnectException:<init>	(Ljava/lang/String;)V
    //   1024: athrow
    //   1025: astore 4
    //   1027: goto +5 -> 1032
    //   1030: astore 4
    //   1032: aload_0
    //   1033: getfield 407	com/d/a/a/c/b:b	Ljava/net/Socket;
    //   1036: invokestatic 465	com/d/a/a/j:a	(Ljava/net/Socket;)V
    //   1039: aload_0
    //   1040: getfield 316	com/d/a/a/c/b:k	Ljava/net/Socket;
    //   1043: invokestatic 465	com/d/a/a/j:a	(Ljava/net/Socket;)V
    //   1046: aload_0
    //   1047: aconst_null
    //   1048: putfield 407	com/d/a/a/c/b:b	Ljava/net/Socket;
    //   1051: aload_0
    //   1052: aconst_null
    //   1053: putfield 316	com/d/a/a/c/b:k	Ljava/net/Socket;
    //   1056: aload_0
    //   1057: aconst_null
    //   1058: putfield 143	com/d/a/a/c/b:f	Ld/e;
    //   1061: aload_0
    //   1062: aconst_null
    //   1063: putfield 145	com/d/a/a/c/b:g	Ld/d;
    //   1066: aload_0
    //   1067: aconst_null
    //   1068: putfield 409	com/d/a/a/c/b:c	Lcom/d/a/o;
    //   1071: aload_0
    //   1072: aconst_null
    //   1073: putfield 249	com/d/a/a/c/b:l	Lcom/d/a/u;
    //   1076: aload 10
    //   1078: ifnonnull +17 -> 1095
    //   1081: new 239	com/d/a/a/b/p
    //   1084: dup
    //   1085: aload 4
    //   1087: invokespecial 285	com/d/a/a/b/p:<init>	(Ljava/io/IOException;)V
    //   1090: astore 10
    //   1092: goto +41 -> 1133
    //   1095: aload 10
    //   1097: getfield 516	com/d/a/a/b/p:b	Ljava/io/IOException;
    //   1100: astore 9
    //   1102: getstatic 519	com/d/a/a/b/p:a	Ljava/lang/reflect/Method;
    //   1105: ifnull +21 -> 1126
    //   1108: getstatic 519	com/d/a/a/b/p:a	Ljava/lang/reflect/Method;
    //   1111: aload 4
    //   1113: iconst_1
    //   1114: anewarray 4	java/lang/Object
    //   1117: dup
    //   1118: iconst_0
    //   1119: aload 9
    //   1121: aastore
    //   1122: invokevirtual 525	java/lang/reflect/Method:invoke	(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    //   1125: pop
    //   1126: aload 10
    //   1128: aload 4
    //   1130: putfield 516	com/d/a/a/b/p:b	Ljava/io/IOException;
    //   1133: iload 5
    //   1135: ifeq +102 -> 1237
    //   1138: iconst_1
    //   1139: istore 7
    //   1141: aload 12
    //   1143: iconst_1
    //   1144: putfield 527	com/d/a/a/a:b	Z
    //   1147: aload 12
    //   1149: getfield 529	com/d/a/a/a:a	Z
    //   1152: ifeq +74 -> 1226
    //   1155: aload 4
    //   1157: instanceof 531
    //   1160: ifne +66 -> 1226
    //   1163: aload 4
    //   1165: instanceof 533
    //   1168: ifne +58 -> 1226
    //   1171: aload 4
    //   1173: instanceof 535
    //   1176: istore 8
    //   1178: iload 8
    //   1180: ifeq +14 -> 1194
    //   1183: aload 4
    //   1185: invokevirtual 539	java/io/IOException:getCause	()Ljava/lang/Throwable;
    //   1188: instanceof 541
    //   1191: ifne +35 -> 1226
    //   1194: aload 4
    //   1196: instanceof 455
    //   1199: ifne +27 -> 1226
    //   1202: iload 7
    //   1204: istore 6
    //   1206: iload 8
    //   1208: ifne +21 -> 1229
    //   1211: aload 4
    //   1213: instanceof 543
    //   1216: ifeq +10 -> 1226
    //   1219: iload 7
    //   1221: istore 6
    //   1223: goto +6 -> 1229
    //   1226: iconst_0
    //   1227: istore 6
    //   1229: iload 6
    //   1231: ifeq +6 -> 1237
    //   1234: goto -1140 -> 94
    //   1237: aload 10
    //   1239: athrow
    //   1240: return
    //   1241: new 545	java/lang/IllegalStateException
    //   1244: dup
    //   1245: ldc_w 547
    //   1248: invokespecial 548	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   1251: athrow
    //   1252: astore 4
    //   1254: goto +27 -> 1281
    //   1257: astore 4
    //   1259: goto -272 -> 987
    //   1262: astore 9
    //   1264: goto -138 -> 1126
    //   1267: goto -970 -> 297
    //   1270: aconst_null
    //   1271: astore 4
    //   1273: goto -775 -> 498
    //   1276: astore 4
    //   1278: goto -246 -> 1032
    //   1281: goto -294 -> 987
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1284	0	this	b
    //   0	1284	1	paramInt1	int
    //   0	1284	2	paramInt2	int
    //   0	1284	3	paramInt3	int
    //   0	1284	4	paramList	List<com.d.a.k>
    //   0	1284	5	paramBoolean	boolean
    //   275	955	6	i1	int
    //   1139	81	7	i2	int
    //   1176	31	8	bool	boolean
    //   173	549	9	localObject1	Object
    //   726	24	9	localAssertionError1	AssertionError
    //   756	364	9	localObject2	Object
    //   1262	1	9	localInvocationTargetException	java.lang.reflect.InvocationTargetException
    //   92	1146	10	localp	com.d.a.a.b.p
    //   182	497	11	localObject3	Object
    //   705	7	11	localAssertionError2	AssertionError
    //   752	7	11	localObject4	Object
    //   854	24	11	str	String
    //   16	1132	12	locala	com.d.a.a.a
    //   25	106	13	localProxy	java.net.Proxy
    //   34	107	14	locala1	a
    //   351	534	15	localObject5	Object
    //   457	435	16	localObject6	Object
    // Exception table:
    //   from	to	target	type
    //   344	382	700	finally
    //   382	477	700	finally
    //   477	495	700	finally
    //   498	538	700	finally
    //   543	550	700	finally
    //   553	558	700	finally
    //   558	564	700	finally
    //   580	700	700	finally
    //   344	382	705	java/lang/AssertionError
    //   382	477	705	java/lang/AssertionError
    //   477	495	705	java/lang/AssertionError
    //   498	538	705	java/lang/AssertionError
    //   543	550	705	java/lang/AssertionError
    //   553	558	705	java/lang/AssertionError
    //   558	564	705	java/lang/AssertionError
    //   580	700	705	java/lang/AssertionError
    //   313	344	718	finally
    //   313	344	726	java/lang/AssertionError
    //   731	749	752	finally
    //   749	752	752	finally
    //   288	294	1025	java/io/IOException
    //   297	313	1025	java/io/IOException
    //   569	577	1025	java/io/IOException
    //   767	775	1025	java/io/IOException
    //   775	783	1025	java/io/IOException
    //   783	798	1025	java/io/IOException
    //   798	818	1025	java/io/IOException
    //   818	952	1025	java/io/IOException
    //   960	978	1025	java/io/IOException
    //   978	984	1025	java/io/IOException
    //   987	1025	1025	java/io/IOException
    //   101	123	1030	java/io/IOException
    //   126	137	1030	java/io/IOException
    //   140	150	1030	java/io/IOException
    //   150	164	1030	java/io/IOException
    //   164	184	1030	java/io/IOException
    //   164	184	1252	java/net/ConnectException
    //   184	194	1257	java/net/ConnectException
    //   1108	1126	1262	java/lang/reflect/InvocationTargetException
    //   1108	1126	1262	java/lang/IllegalAccessException
    //   184	194	1276	java/io/IOException
    //   194	267	1276	java/io/IOException
  }
  
  public final boolean a(boolean paramBoolean)
  {
    if ((!b.isClosed()) && (!b.isInputShutdown()))
    {
      if (b.isOutputShutdown()) {
        return false;
      }
      if (d != null) {
        return true;
      }
      if (!paramBoolean) {}
    }
    try
    {
      int i1 = b.getSoTimeout();
      try
      {
        b.setSoTimeout(1);
        paramBoolean = f.e();
        return !paramBoolean;
      }
      finally
      {
        b.setSoTimeout(i1);
      }
      return true;
    }
    catch (SocketTimeoutException localSocketTimeoutException)
    {
      return true;
    }
    catch (IOException localIOException) {}
    return false;
    return false;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("Connection{");
    localStringBuilder.append(a.a.a.b);
    localStringBuilder.append(":");
    localStringBuilder.append(a.a.a.c);
    localStringBuilder.append(", proxy=");
    localStringBuilder.append(a.b);
    localStringBuilder.append(" hostAddress=");
    localStringBuilder.append(a.c);
    localStringBuilder.append(" cipherSuite=");
    Object localObject = c;
    if (localObject != null) {
      localObject = a;
    } else {
      localObject = "none";
    }
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(" protocol=");
    localStringBuilder.append(l);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.c.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */