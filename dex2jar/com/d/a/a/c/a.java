package com.d.a.a.c;

import d.n;
import d.t;
import d.u;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public abstract interface a
{
  public static final a a = new a()
  {
    public final u a(File paramAnonymousFile)
      throws FileNotFoundException
    {
      return n.a(paramAnonymousFile);
    }
    
    public final void a(File paramAnonymousFile1, File paramAnonymousFile2)
      throws IOException
    {
      d(paramAnonymousFile2);
      if (paramAnonymousFile1.renameTo(paramAnonymousFile2)) {
        return;
      }
      StringBuilder localStringBuilder = new StringBuilder("failed to rename ");
      localStringBuilder.append(paramAnonymousFile1);
      localStringBuilder.append(" to ");
      localStringBuilder.append(paramAnonymousFile2);
      throw new IOException(localStringBuilder.toString());
    }
    
    public final t b(File paramAnonymousFile)
      throws FileNotFoundException
    {
      try
      {
        t localt = n.b(paramAnonymousFile);
        return localt;
      }
      catch (FileNotFoundException localFileNotFoundException)
      {
        for (;;) {}
      }
      paramAnonymousFile.getParentFile().mkdirs();
      return n.b(paramAnonymousFile);
    }
    
    public final t c(File paramAnonymousFile)
      throws FileNotFoundException
    {
      try
      {
        t localt = n.c(paramAnonymousFile);
        return localt;
      }
      catch (FileNotFoundException localFileNotFoundException)
      {
        for (;;) {}
      }
      paramAnonymousFile.getParentFile().mkdirs();
      return n.c(paramAnonymousFile);
    }
    
    public final void d(File paramAnonymousFile)
      throws IOException
    {
      if (!paramAnonymousFile.delete())
      {
        if (!paramAnonymousFile.exists()) {
          return;
        }
        throw new IOException("failed to delete ".concat(String.valueOf(paramAnonymousFile)));
      }
    }
    
    public final boolean e(File paramAnonymousFile)
      throws IOException
    {
      return paramAnonymousFile.exists();
    }
    
    public final long f(File paramAnonymousFile)
    {
      return paramAnonymousFile.length();
    }
    
    public final void g(File paramAnonymousFile)
      throws IOException
    {
      File[] arrayOfFile = paramAnonymousFile.listFiles();
      if (arrayOfFile != null)
      {
        int j = arrayOfFile.length;
        int i = 0;
        while (i < j)
        {
          paramAnonymousFile = arrayOfFile[i];
          if (paramAnonymousFile.isDirectory()) {
            g(paramAnonymousFile);
          }
          if (paramAnonymousFile.delete()) {
            i += 1;
          } else {
            throw new IOException("failed to delete ".concat(String.valueOf(paramAnonymousFile)));
          }
        }
        return;
      }
      throw new IOException("not a readable directory: ".concat(String.valueOf(paramAnonymousFile)));
    }
  };
  
  public abstract u a(File paramFile)
    throws FileNotFoundException;
  
  public abstract void a(File paramFile1, File paramFile2)
    throws IOException;
  
  public abstract t b(File paramFile)
    throws FileNotFoundException;
  
  public abstract t c(File paramFile)
    throws FileNotFoundException;
  
  public abstract void d(File paramFile)
    throws IOException;
  
  public abstract boolean e(File paramFile)
    throws IOException;
  
  public abstract long f(File paramFile);
  
  public abstract void g(File paramFile)
    throws IOException;
}

/* Location:
 * Qualified Name:     com.d.a.a.c.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */