package com.d.a.a.c;

import d.n;
import d.t;
import d.u;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

final class a$1
  implements a
{
  public final u a(File paramFile)
    throws FileNotFoundException
  {
    return n.a(paramFile);
  }
  
  public final void a(File paramFile1, File paramFile2)
    throws IOException
  {
    d(paramFile2);
    if (paramFile1.renameTo(paramFile2)) {
      return;
    }
    StringBuilder localStringBuilder = new StringBuilder("failed to rename ");
    localStringBuilder.append(paramFile1);
    localStringBuilder.append(" to ");
    localStringBuilder.append(paramFile2);
    throw new IOException(localStringBuilder.toString());
  }
  
  public final t b(File paramFile)
    throws FileNotFoundException
  {
    try
    {
      t localt = n.b(paramFile);
      return localt;
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      for (;;) {}
    }
    paramFile.getParentFile().mkdirs();
    return n.b(paramFile);
  }
  
  public final t c(File paramFile)
    throws FileNotFoundException
  {
    try
    {
      t localt = n.c(paramFile);
      return localt;
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      for (;;) {}
    }
    paramFile.getParentFile().mkdirs();
    return n.c(paramFile);
  }
  
  public final void d(File paramFile)
    throws IOException
  {
    if (!paramFile.delete())
    {
      if (!paramFile.exists()) {
        return;
      }
      throw new IOException("failed to delete ".concat(String.valueOf(paramFile)));
    }
  }
  
  public final boolean e(File paramFile)
    throws IOException
  {
    return paramFile.exists();
  }
  
  public final long f(File paramFile)
  {
    return paramFile.length();
  }
  
  public final void g(File paramFile)
    throws IOException
  {
    File[] arrayOfFile = paramFile.listFiles();
    if (arrayOfFile != null)
    {
      int j = arrayOfFile.length;
      int i = 0;
      while (i < j)
      {
        paramFile = arrayOfFile[i];
        if (paramFile.isDirectory()) {
          g(paramFile);
        }
        if (paramFile.delete()) {
          i += 1;
        } else {
          throw new IOException("failed to delete ".concat(String.valueOf(paramFile)));
        }
      }
      return;
    }
    throw new IOException("not a readable directory: ".concat(String.valueOf(paramFile)));
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.c.a.1
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */