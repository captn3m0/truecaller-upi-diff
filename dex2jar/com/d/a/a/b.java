package com.d.a.a;

import com.d.a.a.c.a;
import d.d;
import d.e;
import d.n;
import d.t;
import d.u;
import d.v;
import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class b
  implements Closeable
{
  static final Pattern a = Pattern.compile("[a-z0-9_-]{1,120}");
  private static final t u = new t()
  {
    public final void a_(d.c paramAnonymousc, long paramAnonymousLong)
      throws IOException
    {
      paramAnonymousc.h(paramAnonymousLong);
    }
    
    public final void close()
      throws IOException
    {}
    
    public final void flush()
      throws IOException
    {}
    
    public final v l_()
    {
      return v.c;
    }
  };
  private final a c;
  private final File d;
  private final File e;
  private final File f;
  private final File g;
  private final int h;
  private long i;
  private final int j;
  private long k = 0L;
  private d l;
  private final LinkedHashMap<String, b> m = new LinkedHashMap(0, 0.75F, true);
  private int n;
  private boolean o;
  private boolean p;
  private boolean q;
  private long r = 0L;
  private final Executor s;
  private final Runnable t = new Runnable()
  {
    public final void run()
    {
      for (;;)
      {
        synchronized (b.this)
        {
          if (!b.a(b.this))
          {
            i = 1;
            if ((i | b.b(b.this)) != 0) {
              return;
            }
            try
            {
              b.c(b.this);
              if (b.d(b.this))
              {
                b.e(b.this);
                b.f(b.this);
              }
              return;
            }
            catch (IOException localIOException)
            {
              throw new RuntimeException(localIOException);
            }
          }
        }
        int i = 0;
      }
    }
  };
  
  private b(a parama, File paramFile, long paramLong, Executor paramExecutor)
  {
    c = parama;
    d = paramFile;
    h = 201105;
    e = new File(paramFile, "journal");
    f = new File(paramFile, "journal.tmp");
    g = new File(paramFile, "journal.bkp");
    j = 2;
    i = paramLong;
    s = paramExecutor;
  }
  
  public static b a(a parama, File paramFile, long paramLong)
  {
    if (paramLong > 0L) {
      return new b(parama, paramFile, paramLong, new ThreadPoolExecutor(0, 1, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue(), j.b("OkHttp DiskLruCache")));
    }
    throw new IllegalArgumentException("maxSize <= 0");
  }
  
  private void a(a parama, boolean paramBoolean)
    throws IOException
  {
    for (;;)
    {
      int i2;
      try
      {
        b localb = a;
        if (f == parama)
        {
          int i3 = 0;
          i2 = i3;
          if (paramBoolean)
          {
            i2 = i3;
            if (!e)
            {
              int i1 = 0;
              i2 = i3;
              if (i1 < j)
              {
                if (b[i1] != 0)
                {
                  if (!c.e(d[i1]))
                  {
                    parama.b();
                    return;
                  }
                  i1 += 1;
                  continue;
                }
                parama.b();
                throw new IllegalStateException("Newly created entry didn't create value for index ".concat(String.valueOf(i1)));
              }
            }
          }
          long l1;
          if (i2 < j)
          {
            parama = d[i2];
            if (paramBoolean)
            {
              if (c.e(parama))
              {
                File localFile = c[i2];
                c.a(parama, localFile);
                l1 = b[i2];
                long l2 = c.f(localFile);
                b[i2] = l2;
                k = (k - l1 + l2);
              }
            }
            else {
              c.d(parama);
            }
          }
          else
          {
            n += 1;
            f = null;
            if ((e | paramBoolean))
            {
              e = true;
              l.b("CLEAN").j(32);
              l.b(a);
              localb.a(l);
              l.j(10);
              if (paramBoolean)
              {
                l1 = r;
                r = (1L + l1);
                g = l1;
              }
            }
            else
            {
              m.remove(a);
              l.b("REMOVE").j(32);
              l.b(a);
              l.j(10);
            }
            l.flush();
            if ((k > i) || (g())) {
              s.execute(t);
            }
          }
        }
        else
        {
          throw new IllegalStateException();
        }
      }
      finally {}
      i2 += 1;
    }
  }
  
  private boolean a(b paramb)
    throws IOException
  {
    if (f != null) {
      f.c = true;
    }
    int i1 = 0;
    while (i1 < j)
    {
      c.d(c[i1]);
      k -= b[i1];
      b[i1] = 0L;
      i1 += 1;
    }
    n += 1;
    l.b("REMOVE").j(32).b(a).j(10);
    m.remove(a);
    if (g()) {
      s.execute(t);
    }
    return true;
  }
  
  private void b()
    throws IOException
  {
    try
    {
      if ((!b) && (!Thread.holdsLock(this))) {
        throw new AssertionError();
      }
      boolean bool = p;
      if (bool) {
        return;
      }
      if (c.e(g)) {
        if (c.e(e)) {
          c.d(g);
        } else {
          c.a(g, e);
        }
      }
      bool = c.e(e);
      if (bool) {
        try
        {
          c();
          e();
          p = true;
          return;
        }
        catch (IOException localIOException)
        {
          h.a();
          StringBuilder localStringBuilder = new StringBuilder("DiskLruCache ");
          localStringBuilder.append(d);
          localStringBuilder.append(" is corrupt: ");
          localStringBuilder.append(localIOException.getMessage());
          localStringBuilder.append(", removing");
          h.a(localStringBuilder.toString());
          close();
          c.g(d);
          q = false;
        }
      }
      f();
      p = true;
      return;
    }
    finally {}
  }
  
  private void c()
    throws IOException
  {
    e locale = n.a(c.a(e));
    int i1;
    label593:
    for (;;)
    {
      try
      {
        localObject1 = locale.q();
        localObject3 = locale.q();
        localObject5 = locale.q();
        localObject4 = locale.q();
        str = locale.q();
        if (("libcore.io.DiskLruCache".equals(localObject1)) && ("1".equals(localObject3)) && (Integer.toString(h).equals(localObject5)) && (Integer.toString(j).equals(localObject4)))
        {
          boolean bool = "".equals(str);
          if (bool) {
            i1 = 0;
          }
        }
      }
      finally
      {
        Object localObject1;
        Object localObject3;
        Object localObject5;
        Object localObject4;
        String str;
        int i2;
        j.a(locale);
      }
      try
      {
        str = locale.q();
        i2 = str.indexOf(' ');
        if (i2 != -1)
        {
          int i3 = i2 + 1;
          int i4 = str.indexOf(' ', i3);
          if (i4 == -1)
          {
            localObject3 = str.substring(i3);
            localObject1 = localObject3;
            if (i2 == 6)
            {
              localObject1 = localObject3;
              if (str.startsWith("REMOVE"))
              {
                m.remove(localObject3);
                break label593;
              }
            }
          }
          else
          {
            localObject1 = str.substring(i3, i4);
          }
          localObject4 = (b)m.get(localObject1);
          localObject3 = localObject4;
          if (localObject4 == null)
          {
            localObject3 = new b((String)localObject1, (byte)0);
            m.put(localObject1, localObject3);
          }
          if ((i4 != -1) && (i2 == 5) && (str.startsWith("CLEAN")))
          {
            localObject1 = str.substring(i4 + 1).split(" ");
            e = true;
            f = null;
            ((b)localObject3).a((String[])localObject1);
            break label593;
          }
          if ((i4 == -1) && (i2 == 5) && (str.startsWith("DIRTY")))
          {
            f = new a((b)localObject3, (byte)0);
            break label593;
          }
          if ((i4 == -1) && (i2 == 4) && (str.startsWith("READ"))) {
            break label593;
          }
          throw new IOException("unexpected journal line: ".concat(String.valueOf(str)));
        }
        throw new IOException("unexpected journal line: ".concat(String.valueOf(str)));
      }
      catch (EOFException localEOFException)
      {
        continue;
        i1 += 1;
      }
      n = (i1 - m.size());
      if (!locale.e()) {
        f();
      } else {
        l = d();
      }
      j.a(locale);
      return;
      localObject5 = new StringBuilder("unexpected journal header: [");
      ((StringBuilder)localObject5).append((String)localObject1);
      ((StringBuilder)localObject5).append(", ");
      ((StringBuilder)localObject5).append((String)localObject3);
      ((StringBuilder)localObject5).append(", ");
      ((StringBuilder)localObject5).append((String)localObject4);
      ((StringBuilder)localObject5).append(", ");
      ((StringBuilder)localObject5).append(str);
      ((StringBuilder)localObject5).append("]");
      throw new IOException(((StringBuilder)localObject5).toString());
    }
  }
  
  private static void c(String paramString)
  {
    if (a.matcher(paramString).matches()) {
      return;
    }
    StringBuilder localStringBuilder = new StringBuilder("keys must match regex [a-z0-9_-]{1,120}: \"");
    localStringBuilder.append(paramString);
    localStringBuilder.append("\"");
    throw new IllegalArgumentException(localStringBuilder.toString());
  }
  
  private d d()
    throws FileNotFoundException
  {
    n.a(new c(c.c(e))
    {
      protected final void a()
      {
        if ((!a) && (!Thread.holdsLock(b.this))) {
          throw new AssertionError();
        }
        b.g(b.this);
      }
    });
  }
  
  private void e()
    throws IOException
  {
    c.d(f);
    Iterator localIterator = m.values().iterator();
    while (localIterator.hasNext())
    {
      b localb = (b)localIterator.next();
      a locala = f;
      int i2 = 0;
      int i1 = 0;
      if (locala == null)
      {
        while (i1 < j)
        {
          k += b[i1];
          i1 += 1;
        }
      }
      else
      {
        f = null;
        i1 = i2;
        while (i1 < j)
        {
          c.d(c[i1]);
          c.d(d[i1]);
          i1 += 1;
        }
        localIterator.remove();
      }
    }
  }
  
  /* Error */
  private void f()
    throws IOException
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 224	com/d/a/a/b:l	Ld/d;
    //   6: ifnull +12 -> 18
    //   9: aload_0
    //   10: getfield 224	com/d/a/a/b:l	Ld/d;
    //   13: invokeinterface 467 1 0
    //   18: aload_0
    //   19: getfield 100	com/d/a/a/b:c	Lcom/d/a/a/c/a;
    //   22: aload_0
    //   23: getfield 118	com/d/a/a/b:f	Ljava/io/File;
    //   26: invokeinterface 469 2 0
    //   31: invokestatic 444	d/n:a	(Ld/t;)Ld/d;
    //   34: astore_1
    //   35: aload_1
    //   36: ldc_w 340
    //   39: invokeinterface 231 2 0
    //   44: bipush 10
    //   46: invokeinterface 234 2 0
    //   51: pop
    //   52: aload_1
    //   53: ldc_w 345
    //   56: invokeinterface 231 2 0
    //   61: bipush 10
    //   63: invokeinterface 234 2 0
    //   68: pop
    //   69: aload_1
    //   70: aload_0
    //   71: getfield 105	com/d/a/a/b:h	I
    //   74: i2l
    //   75: invokeinterface 472 3 0
    //   80: bipush 10
    //   82: invokeinterface 234 2 0
    //   87: pop
    //   88: aload_1
    //   89: aload_0
    //   90: getfield 124	com/d/a/a/b:j	I
    //   93: i2l
    //   94: invokeinterface 472 3 0
    //   99: bipush 10
    //   101: invokeinterface 234 2 0
    //   106: pop
    //   107: aload_1
    //   108: bipush 10
    //   110: invokeinterface 234 2 0
    //   115: pop
    //   116: aload_0
    //   117: getfield 91	com/d/a/a/b:m	Ljava/util/LinkedHashMap;
    //   120: invokevirtual 448	java/util/LinkedHashMap:values	()Ljava/util/Collection;
    //   123: invokeinterface 454 1 0
    //   128: astore_2
    //   129: aload_2
    //   130: invokeinterface 459 1 0
    //   135: ifeq +104 -> 239
    //   138: aload_2
    //   139: invokeinterface 463 1 0
    //   144: checkcast 19	com/d/a/a/b$b
    //   147: astore_3
    //   148: aload_3
    //   149: getfield 176	com/d/a/a/b$b:f	Lcom/d/a/a/b$a;
    //   152: ifnull +43 -> 195
    //   155: aload_1
    //   156: ldc_w 389
    //   159: invokeinterface 231 2 0
    //   164: bipush 32
    //   166: invokeinterface 234 2 0
    //   171: pop
    //   172: aload_1
    //   173: aload_3
    //   174: getfield 237	com/d/a/a/b$b:a	Ljava/lang/String;
    //   177: invokeinterface 231 2 0
    //   182: pop
    //   183: aload_1
    //   184: bipush 10
    //   186: invokeinterface 234 2 0
    //   191: pop
    //   192: goto -63 -> 129
    //   195: aload_1
    //   196: ldc -30
    //   198: invokeinterface 231 2 0
    //   203: bipush 32
    //   205: invokeinterface 234 2 0
    //   210: pop
    //   211: aload_1
    //   212: aload_3
    //   213: getfield 237	com/d/a/a/b$b:a	Ljava/lang/String;
    //   216: invokeinterface 231 2 0
    //   221: pop
    //   222: aload_3
    //   223: aload_1
    //   224: invokevirtual 240	com/d/a/a/b$b:a	(Ld/d;)V
    //   227: aload_1
    //   228: bipush 10
    //   230: invokeinterface 234 2 0
    //   235: pop
    //   236: goto -107 -> 129
    //   239: aload_1
    //   240: invokeinterface 467 1 0
    //   245: aload_0
    //   246: getfield 100	com/d/a/a/b:c	Lcom/d/a/a/c/a;
    //   249: aload_0
    //   250: getfield 114	com/d/a/a/b:e	Ljava/io/File;
    //   253: invokeinterface 189 2 0
    //   258: ifeq +20 -> 278
    //   261: aload_0
    //   262: getfield 100	com/d/a/a/b:c	Lcom/d/a/a/c/a;
    //   265: aload_0
    //   266: getfield 114	com/d/a/a/b:e	Ljava/io/File;
    //   269: aload_0
    //   270: getfield 122	com/d/a/a/b:g	Ljava/io/File;
    //   273: invokeinterface 211 3 0
    //   278: aload_0
    //   279: getfield 100	com/d/a/a/b:c	Lcom/d/a/a/c/a;
    //   282: aload_0
    //   283: getfield 118	com/d/a/a/b:f	Ljava/io/File;
    //   286: aload_0
    //   287: getfield 114	com/d/a/a/b:e	Ljava/io/File;
    //   290: invokeinterface 211 3 0
    //   295: aload_0
    //   296: getfield 100	com/d/a/a/b:c	Lcom/d/a/a/c/a;
    //   299: aload_0
    //   300: getfield 122	com/d/a/a/b:g	Ljava/io/File;
    //   303: invokeinterface 220 2 0
    //   308: aload_0
    //   309: aload_0
    //   310: invokespecial 409	com/d/a/a/b:d	()Ld/d;
    //   313: putfield 224	com/d/a/a/b:l	Ld/d;
    //   316: aload_0
    //   317: iconst_0
    //   318: putfield 474	com/d/a/a/b:o	Z
    //   321: aload_0
    //   322: monitorexit
    //   323: return
    //   324: astore_2
    //   325: aload_1
    //   326: invokeinterface 467 1 0
    //   331: aload_2
    //   332: athrow
    //   333: astore_1
    //   334: aload_0
    //   335: monitorexit
    //   336: aload_1
    //   337: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	338	0	this	b
    //   34	292	1	locald	d
    //   333	4	1	localObject1	Object
    //   128	11	2	localIterator	Iterator
    //   324	8	2	localObject2	Object
    //   147	76	3	localb	b
    // Exception table:
    //   from	to	target	type
    //   35	129	324	finally
    //   129	192	324	finally
    //   195	236	324	finally
    //   2	18	333	finally
    //   18	35	333	finally
    //   239	278	333	finally
    //   278	321	333	finally
    //   325	333	333	finally
  }
  
  private boolean g()
  {
    int i1 = n;
    return (i1 >= 2000) && (i1 >= m.size());
  }
  
  private boolean h()
  {
    try
    {
      boolean bool = q;
      return bool;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  private void i()
  {
    try
    {
      boolean bool = h();
      if (!bool) {
        return;
      }
      throw new IllegalStateException("cache is closed");
    }
    finally {}
  }
  
  private void j()
    throws IOException
  {
    while (k > i) {
      a((b)m.values().iterator().next());
    }
  }
  
  public final a a(String paramString, long paramLong)
    throws IOException
  {
    try
    {
      b();
      i();
      c(paramString);
      b localb = (b)m.get(paramString);
      if (paramLong != -1L) {
        if (localb != null)
        {
          long l1 = g;
          if (l1 == paramLong) {}
        }
        else
        {
          return null;
        }
      }
      if (localb != null)
      {
        localObject = f;
        if (localObject != null) {
          return null;
        }
      }
      l.b("DIRTY").j(32).b(paramString).j(10);
      l.flush();
      boolean bool = o;
      if (bool) {
        return null;
      }
      Object localObject = localb;
      if (localb == null)
      {
        localObject = new b(paramString, (byte)0);
        m.put(paramString, localObject);
      }
      paramString = new a((b)localObject, (byte)0);
      f = paramString;
      return paramString;
    }
    finally {}
  }
  
  public final c a(String paramString)
    throws IOException
  {
    try
    {
      b();
      i();
      c(paramString);
      Object localObject = (b)m.get(paramString);
      if ((localObject != null) && (e))
      {
        localObject = ((b)localObject).a();
        if (localObject == null) {
          return null;
        }
        n += 1;
        l.b("READ").j(32).b(paramString).j(10);
        if (g()) {
          s.execute(t);
        }
        return (c)localObject;
      }
      return null;
    }
    finally {}
  }
  
  public final boolean b(String paramString)
    throws IOException
  {
    try
    {
      b();
      i();
      c(paramString);
      paramString = (b)m.get(paramString);
      if (paramString == null) {
        return false;
      }
      boolean bool = a(paramString);
      return bool;
    }
    finally {}
  }
  
  public final void close()
    throws IOException
  {
    for (;;)
    {
      int i1;
      try
      {
        if ((p) && (!q))
        {
          b[] arrayOfb = (b[])m.values().toArray(new b[m.size()]);
          int i2 = arrayOfb.length;
          i1 = 0;
          if (i1 < i2)
          {
            b localb = arrayOfb[i1];
            if (f != null) {
              f.b();
            }
          }
          else
          {
            j();
            l.close();
            l = null;
            q = true;
          }
        }
        else
        {
          q = true;
          return;
        }
      }
      finally {}
      i1 += 1;
    }
  }
  
  public final class a
  {
    final b.b a;
    final boolean[] b;
    boolean c;
    private boolean e;
    
    private a(b.b paramb)
    {
      a = paramb;
      if (e) {
        this$1 = null;
      } else {
        this$1 = new boolean[b.h(b.this)];
      }
      b = b.this;
    }
    
    public final t a(int paramInt)
      throws IOException
    {
      synchronized (b.this)
      {
        if (a.f == this)
        {
          if (!a.e) {
            b[paramInt] = true;
          }
          localObject1 = a.d[paramInt];
        }
        try
        {
          localObject1 = b.i(b.this).b((File)localObject1);
          localObject1 = new c((t)localObject1)
          {
            protected final void a()
            {
              synchronized (b.this)
              {
                c = true;
                return;
              }
            }
          };
          return (t)localObject1;
        }
        catch (FileNotFoundException localFileNotFoundException)
        {
          for (;;) {}
        }
        Object localObject1 = b.a();
        return (t)localObject1;
        throw new IllegalStateException();
      }
    }
    
    public final void a()
      throws IOException
    {
      synchronized (b.this)
      {
        if (c)
        {
          b.a(b.this, this, false);
          b.a(b.this, a);
        }
        else
        {
          b.a(b.this, this, true);
        }
        e = true;
        return;
      }
    }
    
    public final void b()
      throws IOException
    {
      synchronized (b.this)
      {
        b.a(b.this, this, false);
        return;
      }
    }
  }
  
  final class b
  {
    final String a;
    final long[] b;
    final File[] c;
    final File[] d;
    boolean e;
    b.a f;
    long g;
    
    private b(String paramString)
    {
      a = paramString;
      b = new long[b.h(b.this)];
      c = new File[b.h(b.this)];
      d = new File[b.h(b.this)];
      paramString = new StringBuilder(paramString);
      paramString.append('.');
      int j = paramString.length();
      int i = 0;
      while (i < b.h(b.this))
      {
        paramString.append(i);
        c[i] = new File(b.j(b.this), paramString.toString());
        paramString.append(".tmp");
        d[i] = new File(b.j(b.this), paramString.toString());
        paramString.setLength(j);
        i += 1;
      }
    }
    
    private static IOException b(String[] paramArrayOfString)
      throws IOException
    {
      StringBuilder localStringBuilder = new StringBuilder("unexpected journal line: ");
      localStringBuilder.append(Arrays.toString(paramArrayOfString));
      throw new IOException(localStringBuilder.toString());
    }
    
    final b.c a()
    {
      if (Thread.holdsLock(b.this))
      {
        u[] arrayOfu = new u[b.h(b.this)];
        Object localObject = (long[])b.clone();
        j = 0;
        i = 0;
        for (;;)
        {
          try
          {
            if (i < b.h(b.this))
            {
              arrayOfu[i] = b.i(b.this).a(c[i]);
              i += 1;
              continue;
            }
            localObject = new b.c(b.this, a, g, arrayOfu, (long[])localObject, (byte)0);
            return (b.c)localObject;
          }
          catch (FileNotFoundException localFileNotFoundException)
          {
            i = j;
            continue;
          }
          if ((i >= b.h(b.this)) || (arrayOfu[i] == null)) {
            continue;
          }
          j.a(arrayOfu[i]);
          i += 1;
        }
        return null;
      }
      throw new AssertionError();
    }
    
    final void a(d paramd)
      throws IOException
    {
      long[] arrayOfLong = b;
      int j = arrayOfLong.length;
      int i = 0;
      while (i < j)
      {
        long l = arrayOfLong[i];
        paramd.j(32).l(l);
        i += 1;
      }
    }
    
    final void a(String[] paramArrayOfString)
      throws IOException
    {
      int i;
      if (paramArrayOfString.length == b.h(b.this)) {
        i = 0;
      }
      try
      {
        while (i < paramArrayOfString.length)
        {
          b[i] = Long.parseLong(paramArrayOfString[i]);
          i += 1;
        }
        return;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        for (;;) {}
      }
      throw b(paramArrayOfString);
      throw b(paramArrayOfString);
    }
  }
  
  public final class c
    implements Closeable
  {
    public final String a;
    public final long b;
    public final u[] c;
    private final long[] e;
    
    private c(String paramString, long paramLong, u[] paramArrayOfu, long[] paramArrayOfLong)
    {
      a = paramString;
      b = paramLong;
      c = paramArrayOfu;
      e = paramArrayOfLong;
    }
    
    public final void close()
    {
      u[] arrayOfu = c;
      int j = arrayOfu.length;
      int i = 0;
      while (i < j)
      {
        j.a(arrayOfu[i]);
        i += 1;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */