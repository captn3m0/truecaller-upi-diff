package com.d.a.a.b;

import d.c;
import java.io.IOException;

final class e$f
  extends e.a
{
  private boolean e;
  
  private e$f(e parame)
  {
    super(parame, (byte)0);
  }
  
  public final long a(c paramc, long paramLong)
    throws IOException
  {
    if (paramLong >= 0L)
    {
      if (!b)
      {
        if (e) {
          return -1L;
        }
        paramLong = d.b.a(paramc, paramLong);
        if (paramLong == -1L)
        {
          e = true;
          b();
          return -1L;
        }
        return paramLong;
      }
      throw new IllegalStateException("closed");
    }
    throw new IllegalArgumentException("byteCount < 0: ".concat(String.valueOf(paramLong)));
  }
  
  public final void close()
    throws IOException
  {
    if (b) {
      return;
    }
    if (!e) {
      c();
    }
    b = true;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.e.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */