package com.d.a.a.b;

import com.d.a.a.j;
import d.c;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;

final class e$e
  extends e.a
{
  private long e;
  
  public e$e(e parame, long paramLong)
    throws IOException
  {
    super(parame, (byte)0);
    e = paramLong;
    if (e == 0L) {
      b();
    }
  }
  
  public final long a(c paramc, long paramLong)
    throws IOException
  {
    if (paramLong >= 0L)
    {
      if (!b)
      {
        if (e == 0L) {
          return -1L;
        }
        paramLong = d.b.a(paramc, Math.min(e, paramLong));
        if (paramLong != -1L)
        {
          e -= paramLong;
          if (e == 0L) {
            b();
          }
          return paramLong;
        }
        c();
        throw new ProtocolException("unexpected end of stream");
      }
      throw new IllegalStateException("closed");
    }
    throw new IllegalArgumentException("byteCount < 0: ".concat(String.valueOf(paramLong)));
  }
  
  public final void close()
    throws IOException
  {
    if (b) {
      return;
    }
    if ((e != 0L) && (!j.a(this, TimeUnit.MILLISECONDS))) {
      c();
    }
    b = true;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.e.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */