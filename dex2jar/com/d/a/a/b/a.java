package com.d.a.a.b;

import com.d.a.b;
import com.d.a.g;
import com.d.a.l;
import com.d.a.q;
import com.d.a.v;
import com.d.a.v.a;
import com.d.a.x;
import java.io.IOException;
import java.net.Authenticator;
import java.net.Authenticator.RequestorType;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.util.List;

public final class a
  implements b
{
  public static final b a = new a();
  
  private static InetAddress a(Proxy paramProxy, q paramq)
    throws IOException
  {
    if ((paramProxy != null) && (paramProxy.type() != Proxy.Type.DIRECT)) {
      return ((InetSocketAddress)paramProxy.address()).getAddress();
    }
    return InetAddress.getByName(b);
  }
  
  public final v a(Proxy paramProxy, x paramx)
    throws IOException
  {
    List localList = paramx.f();
    paramx = a;
    q localq = a;
    int j = localList.size();
    int i = 0;
    while (i < j)
    {
      Object localObject = (g)localList.get(i);
      if ("Basic".equalsIgnoreCase(a))
      {
        localObject = Authenticator.requestPasswordAuthentication(b, a(paramProxy, localq), c, a, b, a, localq.a(), Authenticator.RequestorType.SERVER);
        if (localObject != null)
        {
          paramProxy = l.a(((PasswordAuthentication)localObject).getUserName(), new String(((PasswordAuthentication)localObject).getPassword()));
          return paramx.b().a("Authorization", paramProxy).a();
        }
      }
      i += 1;
    }
    return null;
  }
  
  public final v b(Proxy paramProxy, x paramx)
    throws IOException
  {
    List localList = paramx.f();
    paramx = a;
    q localq = a;
    int j = localList.size();
    int i = 0;
    while (i < j)
    {
      Object localObject = (g)localList.get(i);
      if ("Basic".equalsIgnoreCase(a))
      {
        InetSocketAddress localInetSocketAddress = (InetSocketAddress)paramProxy.address();
        localObject = Authenticator.requestPasswordAuthentication(localInetSocketAddress.getHostName(), a(paramProxy, localq), localInetSocketAddress.getPort(), a, b, a, localq.a(), Authenticator.RequestorType.PROXY);
        if (localObject != null)
        {
          paramProxy = l.a(((PasswordAuthentication)localObject).getUserName(), new String(((PasswordAuthentication)localObject).getPassword()));
          return paramx.b().a("Proxy-Authorization", paramProxy).a();
        }
      }
      i += 1;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */