package com.d.a.a.b;

import com.d.a.p;
import com.d.a.v;
import com.d.a.x;
import java.util.Date;

public final class c
{
  public final v a;
  public final x b;
  
  private c(v paramv, x paramx)
  {
    a = paramv;
    b = paramx;
  }
  
  public static boolean a(x paramx, v paramv)
  {
    switch (c)
    {
    default: 
      return false;
    case 302: 
    case 307: 
      if ((paramx.a("Expires") == null) && (ge == -1) && (!gg) && (!gf)) {
        return false;
      }
      break;
    }
    return (!gd) && (!cd);
  }
  
  public static final class a
  {
    public final long a;
    public final v b;
    public final x c;
    public Date d;
    public String e;
    public Date f;
    public String g;
    public Date h;
    public long i;
    public long j;
    public String k;
    public int l = -1;
    
    public a(long paramLong, v paramv, x paramx)
    {
      a = paramLong;
      b = paramv;
      c = paramx;
      if (paramx != null)
      {
        paramv = f;
        int m = 0;
        int n = a.length / 2;
        while (m < n)
        {
          paramx = paramv.a(m);
          String str = paramv.b(m);
          if ("Date".equalsIgnoreCase(paramx))
          {
            d = g.a(str);
            e = str;
          }
          else if ("Expires".equalsIgnoreCase(paramx))
          {
            h = g.a(str);
          }
          else if ("Last-Modified".equalsIgnoreCase(paramx))
          {
            f = g.a(str);
            g = str;
          }
          else if ("ETag".equalsIgnoreCase(paramx))
          {
            k = str;
          }
          else if ("Age".equalsIgnoreCase(paramx))
          {
            l = d.b(str, -1);
          }
          else if (k.b.equalsIgnoreCase(paramx))
          {
            i = Long.parseLong(str);
          }
          else if (k.c.equalsIgnoreCase(paramx))
          {
            j = Long.parseLong(str);
          }
          m += 1;
        }
      }
    }
    
    public static boolean a(v paramv)
    {
      return (paramv.a("If-Modified-Since") != null) || (paramv.a("If-None-Match") != null);
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */