package com.d.a.a.b;

import com.d.a.u;
import java.io.IOException;
import java.net.ProtocolException;

public final class r
{
  public final u a;
  public final int b;
  public final String c;
  
  public r(u paramu, int paramInt, String paramString)
  {
    a = paramu;
    b = paramInt;
    c = paramString;
  }
  
  public static r a(String paramString)
    throws IOException
  {
    boolean bool = paramString.startsWith("HTTP/1.");
    int i = 9;
    u localu;
    if (bool)
    {
      if ((paramString.length() >= 9) && (paramString.charAt(8) == ' '))
      {
        j = paramString.charAt(7) - '0';
        if (j == 0) {
          localu = u.a;
        } else if (j == 1) {
          localu = u.b;
        } else {
          throw new ProtocolException("Unexpected status line: ".concat(String.valueOf(paramString)));
        }
      }
      else
      {
        throw new ProtocolException("Unexpected status line: ".concat(String.valueOf(paramString)));
      }
    }
    else
    {
      if (!paramString.startsWith("ICY ")) {
        break label243;
      }
      localu = u.a;
      i = 4;
    }
    int k = paramString.length();
    int j = i + 3;
    if (k >= j) {}
    try
    {
      k = Integer.parseInt(paramString.substring(i, j));
      String str = "";
      if (paramString.length() > j) {
        if (paramString.charAt(j) == ' ') {
          str = paramString.substring(i + 4);
        } else {
          throw new ProtocolException("Unexpected status line: ".concat(String.valueOf(paramString)));
        }
      }
      return new r(localu, k, str);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      for (;;) {}
    }
    throw new ProtocolException("Unexpected status line: ".concat(String.valueOf(paramString)));
    throw new ProtocolException("Unexpected status line: ".concat(String.valueOf(paramString)));
    label243:
    throw new ProtocolException("Unexpected status line: ".concat(String.valueOf(paramString)));
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    String str;
    if (a == u.a) {
      str = "HTTP/1.0";
    } else {
      str = "HTTP/1.1";
    }
    localStringBuilder.append(str);
    localStringBuilder.append(' ');
    localStringBuilder.append(b);
    if (c != null)
    {
      localStringBuilder.append(' ');
      localStringBuilder.append(c);
    }
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.r
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */