package com.d.a.a.b;

import com.d.a.p;
import com.d.a.y;
import d.e;

public final class l
  extends y
{
  private final p a;
  private final e b;
  
  public l(p paramp, e parame)
  {
    a = paramp;
    b = parame;
  }
  
  public final long a()
  {
    return k.a(a);
  }
  
  public final e b()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */