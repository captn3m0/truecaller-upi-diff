package com.d.a.a.b;

import com.d.a.a;
import com.d.a.i;
import com.d.a.q;
import com.d.a.r;
import com.d.a.r.a;
import com.d.a.t;
import com.d.a.v;
import com.d.a.x;
import com.d.a.y;
import com.d.a.z;
import d.d;
import d.n;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.List;

public final class h$a
  implements r.a
{
  private final int b;
  private final v c;
  private int d;
  
  public h$a(h paramh, int paramInt, v paramv)
  {
    b = paramInt;
    c = paramv;
  }
  
  private i a()
  {
    return a.c.a();
  }
  
  public final x a(v paramv)
    throws IOException
  {
    d += 1;
    Object localObject2;
    if (b > 0)
    {
      localObject1 = (r)a.b.h.get(b - 1);
      localObject2 = aaa;
      if ((a.b.equals(a.b)) && (a.c == a.c))
      {
        if (d > 1)
        {
          paramv = new StringBuilder("network interceptor ");
          paramv.append(localObject1);
          paramv.append(" must call proceed() exactly once");
          throw new IllegalStateException(paramv.toString());
        }
      }
      else
      {
        paramv = new StringBuilder("network interceptor ");
        paramv.append(localObject1);
        paramv.append(" must retain the same host and port");
        throw new IllegalStateException(paramv.toString());
      }
    }
    if (b < a.b.h.size())
    {
      localObject1 = new a(a, b + 1, paramv);
      paramv = (r)a.b.h.get(b);
      localObject2 = paramv.a();
      if (d == 1)
      {
        if (localObject2 != null) {
          return (x)localObject2;
        }
        localObject1 = new StringBuilder("network interceptor ");
        ((StringBuilder)localObject1).append(paramv);
        ((StringBuilder)localObject1).append(" returned null");
        throw new NullPointerException(((StringBuilder)localObject1).toString());
      }
      localObject1 = new StringBuilder("network interceptor ");
      ((StringBuilder)localObject1).append(paramv);
      ((StringBuilder)localObject1).append(" must call proceed() exactly once");
      throw new IllegalStateException(((StringBuilder)localObject1).toString());
    }
    h.a(a).a(paramv);
    h.a(a, paramv);
    if ((h.a(paramv)) && (d != null)) {
      n.a(h.a(a).a(paramv, -1L)).close();
    }
    paramv = h.b(a);
    int i = c;
    if (((i != 204) && (i != 205)) || (g.a() <= 0L)) {
      return paramv;
    }
    Object localObject1 = new StringBuilder("HTTP ");
    ((StringBuilder)localObject1).append(i);
    ((StringBuilder)localObject1).append(" had non-zero Content-Length: ");
    ((StringBuilder)localObject1).append(g.a());
    throw new ProtocolException(((StringBuilder)localObject1).toString());
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.h.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */