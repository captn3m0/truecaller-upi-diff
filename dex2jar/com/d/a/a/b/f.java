package com.d.a.a.b;

import com.d.a.a.a.d;
import com.d.a.a.a.e;
import com.d.a.p;
import com.d.a.p.a;
import com.d.a.q;
import com.d.a.x;
import com.d.a.x.a;
import com.d.a.y;
import d.i;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public final class f
  implements j
{
  private static final d.f a = d.f.a("connection");
  private static final d.f b = d.f.a("host");
  private static final d.f c = d.f.a("keep-alive");
  private static final d.f d = d.f.a("proxy-connection");
  private static final d.f e = d.f.a("transfer-encoding");
  private static final d.f f = d.f.a("te");
  private static final d.f g = d.f.a("encoding");
  private static final d.f h = d.f.a("upgrade");
  private static final List<d.f> i = com.d.a.a.j.a(new d.f[] { a, b, c, d, e, com.d.a.a.a.f.b, com.d.a.a.a.f.c, com.d.a.a.a.f.d, com.d.a.a.a.f.e, com.d.a.a.a.f.f, com.d.a.a.a.f.g });
  private static final List<d.f> j = com.d.a.a.j.a(new d.f[] { a, b, c, d, e });
  private static final List<d.f> k = com.d.a.a.j.a(new d.f[] { a, b, c, d, f, e, g, h, com.d.a.a.a.f.b, com.d.a.a.a.f.c, com.d.a.a.a.f.d, com.d.a.a.a.f.e, com.d.a.a.a.f.f, com.d.a.a.a.f.g });
  private static final List<d.f> l = com.d.a.a.j.a(new d.f[] { a, b, c, d, f, e, g, h });
  private final s m;
  private final d n;
  private h o;
  private e p;
  
  public f(s params, d paramd)
  {
    m = params;
    n = paramd;
  }
  
  private static x.a a(List<com.d.a.a.a.f> paramList)
    throws IOException
  {
    p.a locala = new p.a();
    int i5 = paramList.size();
    Object localObject1 = null;
    Object localObject2 = "HTTP/1.1";
    int i1 = 0;
    while (i1 < i5)
    {
      d.f localf = geth;
      String str2 = geti.a();
      Object localObject3 = localObject1;
      int i2 = 0;
      localObject1 = localObject2;
      localObject2 = localObject3;
      while (i2 < str2.length())
      {
        int i4 = str2.indexOf(0, i2);
        int i3 = i4;
        if (i4 == -1) {
          i3 = str2.length();
        }
        String str1 = str2.substring(i2, i3);
        Object localObject4;
        if (localf.equals(com.d.a.a.a.f.a))
        {
          localObject3 = str1;
          localObject4 = localObject1;
        }
        else if (localf.equals(com.d.a.a.a.f.g))
        {
          localObject3 = localObject2;
          localObject4 = str1;
        }
        else
        {
          localObject3 = localObject2;
          localObject4 = localObject1;
          if (!j.contains(localf))
          {
            locala.a(localf.a(), str1);
            localObject4 = localObject1;
            localObject3 = localObject2;
          }
        }
        i2 = i3 + 1;
        localObject2 = localObject3;
        localObject1 = localObject4;
      }
      i1 += 1;
      localObject3 = localObject1;
      localObject1 = localObject2;
      localObject2 = localObject3;
    }
    if (localObject1 != null)
    {
      paramList = new StringBuilder();
      paramList.append((String)localObject2);
      paramList.append(" ");
      paramList.append((String)localObject1);
      paramList = r.a(paramList.toString());
      localObject1 = new x.a();
      b = com.d.a.u.c;
      c = b;
      d = c;
      return ((x.a)localObject1).a(locala.a());
    }
    throw new ProtocolException("Expected ':status' header not present");
  }
  
  private static List<com.d.a.a.a.f> b(com.d.a.v paramv)
  {
    p localp = c;
    ArrayList localArrayList = new ArrayList(a.length / 2 + 5);
    localArrayList.add(new com.d.a.a.a.f(com.d.a.a.a.f.b, b));
    localArrayList.add(new com.d.a.a.a.f(com.d.a.a.a.f.c, n.a(a)));
    localArrayList.add(new com.d.a.a.a.f(com.d.a.a.a.f.g, "HTTP/1.1"));
    localArrayList.add(new com.d.a.a.a.f(com.d.a.a.a.f.f, com.d.a.a.j.a(a)));
    localArrayList.add(new com.d.a.a.a.f(com.d.a.a.a.f.d, a.a));
    paramv = new LinkedHashSet();
    int i3 = a.length / 2;
    int i1 = 0;
    while (i1 < i3)
    {
      d.f localf = d.f.a(localp.a(i1).toLowerCase(Locale.US));
      if (!i.contains(localf))
      {
        String str = localp.b(i1);
        if (paramv.add(localf))
        {
          localArrayList.add(new com.d.a.a.a.f(localf, str));
        }
        else
        {
          int i2 = 0;
          while (i2 < localArrayList.size())
          {
            if (geth.equals(localf))
            {
              StringBuilder localStringBuilder = new StringBuilder(geti.a());
              localStringBuilder.append('\000');
              localStringBuilder.append(str);
              localArrayList.set(i2, new com.d.a.a.a.f(localf, localStringBuilder.toString()));
              break;
            }
            i2 += 1;
          }
        }
      }
      i1 += 1;
    }
    return localArrayList;
  }
  
  public final x.a a()
    throws IOException
  {
    if (n.a == com.d.a.u.d)
    {
      List localList = p.c();
      Object localObject1 = null;
      p.a locala = new p.a();
      int i1 = 0;
      int i2 = localList.size();
      Object localObject2;
      while (i1 < i2)
      {
        d.f localf = geth;
        String str = geti.a();
        if (localf.equals(com.d.a.a.a.f.a))
        {
          localObject2 = str;
        }
        else
        {
          localObject2 = localObject1;
          if (!l.contains(localf))
          {
            locala.a(localf.a(), str);
            localObject2 = localObject1;
          }
        }
        i1 += 1;
        localObject1 = localObject2;
      }
      if (localObject1 != null)
      {
        localObject1 = r.a("HTTP/1.1 ".concat(String.valueOf(localObject1)));
        localObject2 = new x.a();
        b = com.d.a.u.d;
        c = b;
        d = c;
        return ((x.a)localObject2).a(locala.a());
      }
      throw new ProtocolException("Expected ':status' header not present");
    }
    return a(p.c());
  }
  
  public final y a(x paramx)
    throws IOException
  {
    a locala = new a(p.f);
    return new l(f, d.n.a(locala));
  }
  
  public final d.t a(com.d.a.v paramv, long paramLong)
    throws IOException
  {
    return p.d();
  }
  
  public final void a(h paramh)
  {
    o = paramh;
  }
  
  public final void a(o paramo)
    throws IOException
  {
    paramo.a(p.d());
  }
  
  public final void a(com.d.a.v paramv)
    throws IOException
  {
    if (p != null) {
      return;
    }
    o.a();
    boolean bool = h.a(paramv);
    if (n.a == com.d.a.u.d)
    {
      p localp = c;
      ArrayList localArrayList = new ArrayList(a.length / 2 + 4);
      localArrayList.add(new com.d.a.a.a.f(com.d.a.a.a.f.b, b));
      localArrayList.add(new com.d.a.a.a.f(com.d.a.a.a.f.c, n.a(a)));
      localArrayList.add(new com.d.a.a.a.f(com.d.a.a.a.f.e, com.d.a.a.j.a(a)));
      localArrayList.add(new com.d.a.a.a.f(com.d.a.a.a.f.d, a.a));
      int i1 = 0;
      int i2 = a.length / 2;
      for (;;)
      {
        paramv = localArrayList;
        if (i1 >= i2) {
          break;
        }
        paramv = d.f.a(localp.a(i1).toLowerCase(Locale.US));
        if (!k.contains(paramv)) {
          localArrayList.add(new com.d.a.a.a.f(paramv, localp.b(i1)));
        }
        i1 += 1;
      }
    }
    paramv = b(paramv);
    p = n.a(paramv, bool);
    p.h.a(o.b.w, TimeUnit.MILLISECONDS);
    p.i.a(o.b.x, TimeUnit.MILLISECONDS);
  }
  
  public final void b()
    throws IOException
  {
    p.d().close();
  }
  
  final class a
    extends i
  {
    public a(d.u paramu)
    {
      super();
    }
    
    public final void close()
      throws IOException
    {
      f.a(f.this).a(f.this);
      super.close();
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */