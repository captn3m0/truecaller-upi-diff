package com.d.a.a.b;

import com.d.a.a;
import com.d.a.f;
import com.d.a.p;
import com.d.a.p.a;
import com.d.a.q;
import com.d.a.r;
import com.d.a.r.a;
import com.d.a.x;
import com.d.a.x.a;
import com.d.a.y;
import com.d.a.z;
import d.d;
import d.e;
import d.n;
import d.u;
import java.io.Closeable;
import java.io.IOException;
import java.net.CookieHandler;
import java.net.ProtocolException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLSocketFactory;

public final class h
{
  public static final y a = new y()
  {
    public final long a()
    {
      return 0L;
    }
    
    public final e b()
    {
      return new d.c();
    }
  };
  public final com.d.a.t b;
  public final s c;
  public final x d;
  public j e;
  public long f = -1L;
  public boolean g;
  public final boolean h;
  public final com.d.a.v i;
  public com.d.a.v j;
  public x k;
  public x l;
  public d.t m;
  public d n;
  public final boolean o;
  public final boolean p;
  public b q;
  public c r;
  
  public h(com.d.a.t paramt, com.d.a.v paramv, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, s params, o paramo, x paramx)
  {
    b = paramt;
    i = paramv;
    h = paramBoolean1;
    o = paramBoolean2;
    p = paramBoolean3;
    if (params == null)
    {
      com.d.a.j localj = q;
      Object localObject1;
      f localf;
      Object localObject2;
      if (a.c())
      {
        localObject1 = m;
        params = n;
        localf = o;
        localObject2 = localf;
      }
      else
      {
        localf = null;
        params = localf;
        localObject2 = params;
        localObject1 = localf;
      }
      params = new s(localj, new a(a.b, a.c, r, l, (SSLSocketFactory)localObject1, params, (f)localObject2, p, d, e, f, i));
    }
    c = params;
    m = paramo;
    d = paramx;
  }
  
  public static p a(p paramp1, p paramp2)
    throws IOException
  {
    p.a locala = new p.a();
    int i3 = a.length / 2;
    int i2 = 0;
    int i1 = 0;
    while (i1 < i3)
    {
      String str1 = paramp1.a(i1);
      String str2 = paramp1.b(i1);
      if (((!"Warning".equalsIgnoreCase(str1)) || (!str2.startsWith("1"))) && ((!k.a(str1)) || (paramp2.a(str1) == null))) {
        locala.a(str1, str2);
      }
      i1 += 1;
    }
    i3 = a.length / 2;
    i1 = i2;
    while (i1 < i3)
    {
      paramp1 = paramp2.a(i1);
      if ((!"Content-Length".equalsIgnoreCase(paramp1)) && (k.a(paramp1))) {
        locala.a(paramp1, paramp2.b(i1));
      }
      i1 += 1;
    }
    return locala.a();
  }
  
  public static x a(x paramx)
  {
    x localx = paramx;
    if (paramx != null)
    {
      localx = paramx;
      if (g != null)
      {
        paramx = paramx.d();
        g = null;
        localx = paramx.a();
      }
    }
    return localx;
  }
  
  public static boolean a(com.d.a.v paramv)
  {
    return i.c(b);
  }
  
  public static boolean a(x paramx1, x paramx2)
  {
    if (c == 304) {
      return true;
    }
    paramx1 = f.b("Last-Modified");
    if (paramx1 != null)
    {
      paramx2 = f.b("Last-Modified");
      if ((paramx2 != null) && (paramx2.getTime() < paramx1.getTime())) {
        return true;
      }
    }
    return false;
  }
  
  public static boolean c(x paramx)
  {
    if (a.b.equals("HEAD")) {
      return false;
    }
    int i1 = c;
    if (((i1 < 100) || (i1 >= 200)) && (i1 != 204) && (i1 != 304)) {
      return true;
    }
    if (k.a(paramx) == -1L) {
      return "chunked".equalsIgnoreCase(paramx.a("Transfer-Encoding"));
    }
    return true;
  }
  
  public final void a()
  {
    if (f == -1L)
    {
      f = System.currentTimeMillis();
      return;
    }
    throw new IllegalStateException();
  }
  
  public final void a(p paramp)
    throws IOException
  {
    CookieHandler localCookieHandler = b.j;
    if (localCookieHandler != null) {
      localCookieHandler.put(i.a(), k.b(paramp));
    }
  }
  
  public final boolean a(q paramq)
  {
    q localq = i.a;
    return (b.equals(b)) && (c == c) && (a.equals(a));
  }
  
  public final s b()
  {
    Object localObject = n;
    if (localObject != null)
    {
      com.d.a.a.j.a((Closeable)localObject);
    }
    else
    {
      localObject = m;
      if (localObject != null) {
        com.d.a.a.j.a((Closeable)localObject);
      }
    }
    localObject = l;
    if (localObject != null) {
      com.d.a.a.j.a(g);
    } else {
      c.a(true, false, true);
    }
    return c;
  }
  
  public final x b(x paramx)
    throws IOException
  {
    if (g)
    {
      if (!"gzip".equalsIgnoreCase(l.a("Content-Encoding"))) {
        return paramx;
      }
      if (g == null) {
        return paramx;
      }
      d.l locall = new d.l(g.b());
      p localp = f.a().b("Content-Encoding").b("Content-Length").a();
      paramx = paramx.d().a(localp);
      g = new l(localp, n.a(locall));
      return paramx.a();
    }
    return paramx;
  }
  
  public final x c()
    throws IOException
  {
    e.b();
    Object localObject = e.a();
    a = j;
    e = c.a().c;
    x localx = ((x.a)localObject).a(k.b, Long.toString(f)).a(k.c, Long.toString(System.currentTimeMillis())).a();
    localObject = localx;
    if (!p)
    {
      localObject = localx.d();
      g = e.a(localx);
      localObject = ((x.a)localObject).a();
    }
    if (("close".equalsIgnoreCase(a.a("Connection"))) || ("close".equalsIgnoreCase(((x)localObject).a("Connection")))) {
      c.a(true, false, false);
    }
    return (x)localObject;
  }
  
  public final class a
    implements r.a
  {
    private final int b;
    private final com.d.a.v c;
    private int d;
    
    public a(int paramInt, com.d.a.v paramv)
    {
      b = paramInt;
      c = paramv;
    }
    
    private com.d.a.i a()
    {
      return c.a();
    }
    
    public final x a(com.d.a.v paramv)
      throws IOException
    {
      d += 1;
      Object localObject2;
      if (b > 0)
      {
        localObject1 = (r)b.h.get(b - 1);
        localObject2 = aaa;
        if ((a.b.equals(a.b)) && (a.c == a.c))
        {
          if (d > 1)
          {
            paramv = new StringBuilder("network interceptor ");
            paramv.append(localObject1);
            paramv.append(" must call proceed() exactly once");
            throw new IllegalStateException(paramv.toString());
          }
        }
        else
        {
          paramv = new StringBuilder("network interceptor ");
          paramv.append(localObject1);
          paramv.append(" must retain the same host and port");
          throw new IllegalStateException(paramv.toString());
        }
      }
      if (b < b.h.size())
      {
        localObject1 = new a(h.this, b + 1, paramv);
        paramv = (r)b.h.get(b);
        localObject2 = paramv.a();
        if (d == 1)
        {
          if (localObject2 != null) {
            return (x)localObject2;
          }
          localObject1 = new StringBuilder("network interceptor ");
          ((StringBuilder)localObject1).append(paramv);
          ((StringBuilder)localObject1).append(" returned null");
          throw new NullPointerException(((StringBuilder)localObject1).toString());
        }
        localObject1 = new StringBuilder("network interceptor ");
        ((StringBuilder)localObject1).append(paramv);
        ((StringBuilder)localObject1).append(" must call proceed() exactly once");
        throw new IllegalStateException(((StringBuilder)localObject1).toString());
      }
      h.a(h.this).a(paramv);
      h.a(h.this, paramv);
      if ((h.a(paramv)) && (d != null)) {
        n.a(h.a(h.this).a(paramv, -1L)).close();
      }
      paramv = h.b(h.this);
      int i = c;
      if (((i != 204) && (i != 205)) || (g.a() <= 0L)) {
        return paramv;
      }
      Object localObject1 = new StringBuilder("HTTP ");
      ((StringBuilder)localObject1).append(i);
      ((StringBuilder)localObject1).append(" had non-zero Content-Length: ");
      ((StringBuilder)localObject1).append(g.a());
      throw new ProtocolException(((StringBuilder)localObject1).toString());
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */