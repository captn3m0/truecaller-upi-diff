package com.d.a.a.b;

import d.c;
import d.d;
import d.j;
import d.t;
import d.v;
import java.io.IOException;

final class e$b
  implements t
{
  private final j b = new j(a.c.l_());
  private boolean c;
  
  private e$b(e parame) {}
  
  public final void a_(c paramc, long paramLong)
    throws IOException
  {
    if (!c)
    {
      if (paramLong == 0L) {
        return;
      }
      a.c.k(paramLong);
      a.c.b("\r\n");
      a.c.a_(paramc, paramLong);
      a.c.b("\r\n");
      return;
    }
    throw new IllegalStateException("closed");
  }
  
  public final void close()
    throws IOException
  {
    try
    {
      boolean bool = c;
      if (bool) {
        return;
      }
      c = true;
      a.c.b("0\r\n\r\n");
      e.a(b);
      a.d = 3;
      return;
    }
    finally {}
  }
  
  public final void flush()
    throws IOException
  {
    try
    {
      boolean bool = c;
      if (bool) {
        return;
      }
      a.c.flush();
      return;
    }
    finally {}
  }
  
  public final v l_()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.e.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */