package com.d.a.a.b;

import com.d.a.a.j;
import d.c;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;

final class e$c
  extends e.a
{
  private long e = -1L;
  private boolean f = true;
  private final h g;
  
  e$c(e parame, h paramh)
    throws IOException
  {
    super(parame, (byte)0);
    g = paramh;
  }
  
  public final long a(c paramc, long paramLong)
    throws IOException
  {
    if (paramLong >= 0L)
    {
      if (!b)
      {
        if (!f) {
          return -1L;
        }
        long l = e;
        if ((l == 0L) || (l == -1L)) {
          if (e != -1L) {
            d.b.q();
          }
        }
        try
        {
          e = d.b.n();
          String str = d.b.q().trim();
          if (e >= 0L) {
            if (!str.isEmpty())
            {
              boolean bool = str.startsWith(";");
              if (!bool) {}
            }
            else
            {
              if (e == 0L)
              {
                f = false;
                g.a(d.d());
                b();
              }
              if (!f) {
                return -1L;
              }
              paramLong = d.b.a(paramc, Math.min(paramLong, e));
              if (paramLong != -1L)
              {
                e -= paramLong;
                return paramLong;
              }
              c();
              throw new ProtocolException("unexpected end of stream");
            }
          }
          paramc = new StringBuilder("expected chunk size and optional extensions but was \"");
          paramc.append(e);
          paramc.append(str);
          paramc.append("\"");
          throw new ProtocolException(paramc.toString());
        }
        catch (NumberFormatException paramc)
        {
          throw new ProtocolException(paramc.getMessage());
        }
      }
      throw new IllegalStateException("closed");
    }
    throw new IllegalArgumentException("byteCount < 0: ".concat(String.valueOf(paramLong)));
  }
  
  public final void close()
    throws IOException
  {
    if (b) {
      return;
    }
    if ((f) && (!j.a(this, TimeUnit.MILLISECONDS))) {
      c();
    }
    b = true;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.e.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */