package com.d.a.a.b;

import com.d.a.a;
import com.d.a.a.c.b;
import com.d.a.a.d;
import com.d.a.a.i;
import com.d.a.z;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.ProxySelector;
import java.util.List;

public final class s
{
  public final a a;
  public q b;
  public b c;
  private final com.d.a.j d;
  private boolean e;
  private boolean f;
  private j g;
  
  public s(com.d.a.j paramj, a parama)
  {
    d = paramj;
    a = parama;
  }
  
  private b a(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
    throws IOException, p
  {
    synchronized (d)
    {
      if (!e)
      {
        if (g == null)
        {
          if (!f)
          {
            b localb = c;
            if ((localb != null) && (!i)) {
              return localb;
            }
            localb = d.b.a(d, a, this);
            if (localb != null)
            {
              c = localb;
              return localb;
            }
            if (b == null) {
              b = new q(a, b());
            }
            localb = new b(b.b());
            a(localb);
            synchronized (d)
            {
              d.b.b(d, localb);
              c = localb;
              if (!f)
              {
                localb.a(paramInt1, paramInt2, paramInt3, a.f, paramBoolean);
                b().b(a);
                return localb;
              }
              throw new IOException("Canceled");
            }
          }
          throw new IOException("Canceled");
        }
        throw new IllegalStateException("stream != null");
      }
      throw new IllegalStateException("released");
    }
  }
  
  private b b(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean1, boolean paramBoolean2)
    throws IOException, p
  {
    for (;;)
    {
      b localb = a(paramInt1, paramInt2, paramInt3, paramBoolean1);
      synchronized (d)
      {
        if (e == 0) {
          return localb;
        }
        if (localb.a(paramBoolean2)) {
          return localb;
        }
        a(true, false, true);
      }
    }
  }
  
  private i b()
  {
    return d.b.a(d);
  }
  
  private void b(b paramb)
  {
    int j = h.size();
    int i = 0;
    while (i < j)
    {
      if (((Reference)h.get(i)).get() == this)
      {
        h.remove(i);
        return;
      }
      i += 1;
    }
    throw new IllegalStateException();
  }
  
  /* Error */
  public final j a(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean1, boolean paramBoolean2)
    throws p, IOException
  {
    // Byte code:
    //   0: aload_0
    //   1: iload_1
    //   2: iload_2
    //   3: iload_3
    //   4: iload 4
    //   6: iload 5
    //   8: invokespecial 143	com/d/a/a/b/s:b	(IIIZZ)Lcom/d/a/a/c/b;
    //   11: astore 7
    //   13: aload 7
    //   15: getfield 146	com/d/a/a/c/b:d	Lcom/d/a/a/a/d;
    //   18: ifnull +21 -> 39
    //   21: new 148	com/d/a/a/b/f
    //   24: dup
    //   25: aload_0
    //   26: aload 7
    //   28: getfield 146	com/d/a/a/c/b:d	Lcom/d/a/a/a/d;
    //   31: invokespecial 151	com/d/a/a/b/f:<init>	(Lcom/d/a/a/b/s;Lcom/d/a/a/a/d;)V
    //   34: astore 6
    //   36: goto +70 -> 106
    //   39: aload 7
    //   41: getfield 154	com/d/a/a/c/b:b	Ljava/net/Socket;
    //   44: iload_2
    //   45: invokevirtual 160	java/net/Socket:setSoTimeout	(I)V
    //   48: aload 7
    //   50: getfield 163	com/d/a/a/c/b:f	Ld/e;
    //   53: invokeinterface 169 1 0
    //   58: iload_2
    //   59: i2l
    //   60: getstatic 175	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   63: invokevirtual 180	d/v:a	(JLjava/util/concurrent/TimeUnit;)Ld/v;
    //   66: pop
    //   67: aload 7
    //   69: getfield 183	com/d/a/a/c/b:g	Ld/d;
    //   72: invokeinterface 186 1 0
    //   77: iload_3
    //   78: i2l
    //   79: getstatic 175	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   82: invokevirtual 180	d/v:a	(JLjava/util/concurrent/TimeUnit;)Ld/v;
    //   85: pop
    //   86: new 188	com/d/a/a/b/e
    //   89: dup
    //   90: aload_0
    //   91: aload 7
    //   93: getfield 163	com/d/a/a/c/b:f	Ld/e;
    //   96: aload 7
    //   98: getfield 183	com/d/a/a/c/b:g	Ld/d;
    //   101: invokespecial 191	com/d/a/a/b/e:<init>	(Lcom/d/a/a/b/s;Ld/e;Ld/d;)V
    //   104: astore 6
    //   106: aload_0
    //   107: getfield 24	com/d/a/a/b/s:d	Lcom/d/a/j;
    //   110: astore 8
    //   112: aload 8
    //   114: monitorenter
    //   115: aload 7
    //   117: aload 7
    //   119: getfield 109	com/d/a/a/c/b:e	I
    //   122: iconst_1
    //   123: iadd
    //   124: putfield 109	com/d/a/a/c/b:e	I
    //   127: aload_0
    //   128: aload 6
    //   130: putfield 36	com/d/a/a/b/s:g	Lcom/d/a/a/b/j;
    //   133: aload 8
    //   135: monitorexit
    //   136: aload 6
    //   138: areturn
    //   139: astore 6
    //   141: aload 8
    //   143: monitorexit
    //   144: aload 6
    //   146: athrow
    //   147: astore 6
    //   149: new 32	com/d/a/a/b/p
    //   152: dup
    //   153: aload 6
    //   155: invokespecial 194	com/d/a/a/b/p:<init>	(Ljava/io/IOException;)V
    //   158: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	159	0	this	s
    //   0	159	1	paramInt1	int
    //   0	159	2	paramInt2	int
    //   0	159	3	paramInt3	int
    //   0	159	4	paramBoolean1	boolean
    //   0	159	5	paramBoolean2	boolean
    //   34	103	6	localObject1	Object
    //   139	6	6	localObject2	Object
    //   147	7	6	localIOException	IOException
    //   11	107	7	localb	b
    // Exception table:
    //   from	to	target	type
    //   115	136	139	finally
    //   141	144	139	finally
    //   0	36	147	java/io/IOException
    //   39	106	147	java/io/IOException
    //   106	115	147	java/io/IOException
    //   144	147	147	java/io/IOException
  }
  
  public final b a()
  {
    try
    {
      b localb = c;
      return localb;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final void a(j paramj)
  {
    com.d.a.j localj = d;
    if (paramj != null) {}
    try
    {
      if (paramj == g)
      {
        a(false, false, true);
        return;
      }
      StringBuilder localStringBuilder = new StringBuilder("expected ");
      localStringBuilder.append(g);
      localStringBuilder.append(" but was ");
      localStringBuilder.append(paramj);
      throw new IllegalStateException(localStringBuilder.toString());
    }
    finally {}
  }
  
  public final void a(b paramb)
  {
    h.add(new WeakReference(this));
  }
  
  public final void a(IOException paramIOException)
  {
    synchronized (d)
    {
      if (b != null) {
        if (c.e == 0)
        {
          z localz = c.a;
          q localq = b;
          if ((b.type() != Proxy.Type.DIRECT) && (a.g != null)) {
            a.g.connectFailed(a.a.b(), b.address(), paramIOException);
          }
          b.a(localz);
        }
        else
        {
          b = null;
        }
      }
      a(true, false, true);
      return;
    }
  }
  
  public final void a(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    com.d.a.j localj = d;
    if (paramBoolean3) {}
    try
    {
      g = null;
      if (paramBoolean2) {
        e = true;
      }
      if (c == null) {
        break label189;
      }
      if (paramBoolean1) {
        c.i = true;
      }
      if ((g != null) || ((!e) && (!c.i))) {
        break label189;
      }
      b(c);
      if (c.e > 0) {
        b = null;
      }
      if (!c.h.isEmpty()) {
        break label183;
      }
      c.j = System.nanoTime();
      if (!d.b.a(d, c)) {
        break label183;
      }
      localb = c;
    }
    finally
    {
      for (;;)
      {
        b localb;
        continue;
        Object localObject2 = null;
        continue;
        localObject2 = null;
      }
    }
    c = null;
    if (localb != null) {
      com.d.a.a.j.a(b);
    }
    return;
    throw localb;
  }
  
  public final String toString()
  {
    return a.toString();
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.s
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */