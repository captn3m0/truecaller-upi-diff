package com.d.a.a.b;

import com.d.a.a.h;
import com.d.a.a.j;
import com.d.a.b;
import com.d.a.g;
import com.d.a.p;
import com.d.a.p.a;
import com.d.a.v;
import com.d.a.v.a;
import com.d.a.x;
import java.io.IOException;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public final class k
{
  static final String a;
  public static final String b;
  public static final String c;
  public static final String d;
  public static final String e;
  private static final Comparator<String> f = new Comparator() {};
  
  static
  {
    h.a();
    a = h.b();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(a);
    localStringBuilder.append("-Sent-Millis");
    b = localStringBuilder.toString();
    localStringBuilder = new StringBuilder();
    localStringBuilder.append(a);
    localStringBuilder.append("-Received-Millis");
    c = localStringBuilder.toString();
    localStringBuilder = new StringBuilder();
    localStringBuilder.append(a);
    localStringBuilder.append("-Selected-Protocol");
    d = localStringBuilder.toString();
    localStringBuilder = new StringBuilder();
    localStringBuilder.append(a);
    localStringBuilder.append("-Response-Source");
    e = localStringBuilder.toString();
  }
  
  public static long a(p paramp)
  {
    return b(paramp.a("Content-Length"));
  }
  
  public static long a(v paramv)
  {
    return a(c);
  }
  
  public static long a(x paramx)
  {
    return a(f);
  }
  
  public static v a(b paramb, x paramx, Proxy paramProxy)
    throws IOException
  {
    if (c == 407) {
      return paramb.b(paramProxy, paramx);
    }
    return paramb.a(paramProxy, paramx);
  }
  
  public static List<g> a(p paramp, String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    int k = a.length / 2;
    int i = 0;
    while (i < k)
    {
      if (paramString.equalsIgnoreCase(paramp.a(i)))
      {
        String str1 = paramp.b(i);
        int j = 0;
        while (j < str1.length())
        {
          int m = d.a(str1, j, " ");
          String str2 = str1.substring(j, m).trim();
          j = d.a(str1, m);
          if (!str1.regionMatches(true, j, "realm=\"", 0, 7)) {
            break;
          }
          j += 7;
          m = d.a(str1, j, "\"");
          String str3 = str1.substring(j, m);
          j = d.a(str1, d.a(str1, m + 1, ",") + 1);
          localArrayList.add(new g(str2, str3));
        }
      }
      i += 1;
    }
    return localArrayList;
  }
  
  public static void a(v.a parama, Map<String, List<String>> paramMap)
  {
    Iterator localIterator = paramMap.entrySet().iterator();
    while (localIterator.hasNext())
    {
      paramMap = (Map.Entry)localIterator.next();
      String str = (String)paramMap.getKey();
      if ((("Cookie".equalsIgnoreCase(str)) || ("Cookie2".equalsIgnoreCase(str))) && (!((List)paramMap.getValue()).isEmpty()))
      {
        paramMap = (List)paramMap.getValue();
        int j = paramMap.size();
        int i = 0;
        if (j == 1)
        {
          paramMap = (String)paramMap.get(0);
        }
        else
        {
          StringBuilder localStringBuilder = new StringBuilder();
          j = paramMap.size();
          while (i < j)
          {
            if (i > 0) {
              localStringBuilder.append("; ");
            }
            localStringBuilder.append((String)paramMap.get(i));
            i += 1;
          }
          paramMap = localStringBuilder.toString();
        }
        parama.b(str, paramMap);
      }
    }
  }
  
  public static boolean a(x paramx, p paramp, v paramv)
  {
    paramx = c(f).iterator();
    while (paramx.hasNext())
    {
      String str = (String)paramx.next();
      if (!j.a(paramp.c(str), c.c(str))) {
        return false;
      }
    }
    return true;
  }
  
  static boolean a(String paramString)
  {
    return (!"Connection".equalsIgnoreCase(paramString)) && (!"Keep-Alive".equalsIgnoreCase(paramString)) && (!"Proxy-Authenticate".equalsIgnoreCase(paramString)) && (!"Proxy-Authorization".equalsIgnoreCase(paramString)) && (!"TE".equalsIgnoreCase(paramString)) && (!"Trailers".equalsIgnoreCase(paramString)) && (!"Transfer-Encoding".equalsIgnoreCase(paramString)) && (!"Upgrade".equalsIgnoreCase(paramString));
  }
  
  private static long b(String paramString)
  {
    if (paramString == null) {
      return -1L;
    }
    try
    {
      long l = Long.parseLong(paramString);
      return l;
    }
    catch (NumberFormatException paramString) {}
    return -1L;
  }
  
  public static Map<String, List<String>> b(p paramp)
  {
    TreeMap localTreeMap = new TreeMap(f);
    int j = a.length / 2;
    int i = 0;
    while (i < j)
    {
      String str1 = paramp.a(i);
      String str2 = paramp.b(i);
      ArrayList localArrayList = new ArrayList();
      List localList = (List)localTreeMap.get(str1);
      if (localList != null) {
        localArrayList.addAll(localList);
      }
      localArrayList.add(str2);
      localTreeMap.put(str1, Collections.unmodifiableList(localArrayList));
      i += 1;
    }
    return Collections.unmodifiableMap(localTreeMap);
  }
  
  public static boolean b(x paramx)
  {
    return c(f).contains("*");
  }
  
  public static p c(x paramx)
  {
    p localp = h.a.c;
    paramx = c(f);
    if (paramx.isEmpty()) {
      return new p.a().a();
    }
    p.a locala = new p.a();
    int i = 0;
    int j = a.length / 2;
    while (i < j)
    {
      String str = localp.a(i);
      if (paramx.contains(str)) {
        locala.a(str, localp.b(i));
      }
      i += 1;
    }
    return locala.a();
  }
  
  private static Set<String> c(p paramp)
  {
    Object localObject2 = Collections.emptySet();
    int k = a.length / 2;
    int i = 0;
    while (i < k)
    {
      Object localObject3 = localObject2;
      if ("Vary".equalsIgnoreCase(paramp.a(i)))
      {
        localObject3 = paramp.b(i);
        Object localObject1 = localObject2;
        if (((Set)localObject2).isEmpty()) {
          localObject1 = new TreeSet(String.CASE_INSENSITIVE_ORDER);
        }
        localObject2 = ((String)localObject3).split(",");
        int m = localObject2.length;
        int j = 0;
        for (;;)
        {
          localObject3 = localObject1;
          if (j >= m) {
            break;
          }
          ((Set)localObject1).add(localObject2[j].trim());
          j += 1;
        }
      }
      i += 1;
      localObject2 = localObject3;
    }
    return (Set<String>)localObject2;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */