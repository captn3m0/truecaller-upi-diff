package com.d.a.a.b;

import com.d.a.v;
import com.d.a.x;
import com.d.a.x.a;
import com.d.a.y;
import d.t;
import java.io.IOException;

public abstract interface j
{
  public abstract x.a a()
    throws IOException;
  
  public abstract y a(x paramx)
    throws IOException;
  
  public abstract t a(v paramv, long paramLong)
    throws IOException;
  
  public abstract void a(h paramh);
  
  public abstract void a(o paramo)
    throws IOException;
  
  public abstract void a(v paramv)
    throws IOException;
  
  public abstract void b()
    throws IOException;
}

/* Location:
 * Qualified Name:     com.d.a.a.b.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */