package com.d.a.a.b;

import com.d.a.a.j;
import d.c;
import d.t;
import d.v;
import java.io.IOException;
import java.net.ProtocolException;

public final class o
  implements t
{
  public final c a = new c();
  private boolean b;
  private final int c;
  
  public o()
  {
    this(-1);
  }
  
  public o(int paramInt)
  {
    c = paramInt;
  }
  
  public final void a(t paramt)
    throws IOException
  {
    c localc1 = new c();
    c localc2 = a;
    localc2.a(localc1, 0L, b);
    paramt.a_(localc1, b);
  }
  
  public final void a_(c paramc, long paramLong)
    throws IOException
  {
    if (!b)
    {
      j.a(b, paramLong);
      if ((c != -1) && (a.b > c - paramLong))
      {
        paramc = new StringBuilder("exceeded content-length limit of ");
        paramc.append(c);
        paramc.append(" bytes");
        throw new ProtocolException(paramc.toString());
      }
      a.a_(paramc, paramLong);
      return;
    }
    throw new IllegalStateException("closed");
  }
  
  public final void close()
    throws IOException
  {
    if (b) {
      return;
    }
    b = true;
    if (a.b >= c) {
      return;
    }
    StringBuilder localStringBuilder = new StringBuilder("content-length promised ");
    localStringBuilder.append(c);
    localStringBuilder.append(" bytes, but received ");
    localStringBuilder.append(a.b);
    throw new ProtocolException(localStringBuilder.toString());
  }
  
  public final void flush()
    throws IOException
  {}
  
  public final v l_()
  {
    return v.c;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */