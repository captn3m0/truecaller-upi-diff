package com.d.a.a.b;

import d.c;
import d.d;
import d.t;
import d.v;
import java.io.IOException;
import java.net.ProtocolException;

final class e$d
  implements t
{
  private final d.j b = new d.j(a.c.l_());
  private boolean c;
  private long d;
  
  private e$d(e parame, long paramLong)
  {
    d = paramLong;
  }
  
  public final void a_(c paramc, long paramLong)
    throws IOException
  {
    if (!c)
    {
      com.d.a.a.j.a(b, paramLong);
      if (paramLong <= d)
      {
        a.c.a_(paramc, paramLong);
        d -= paramLong;
        return;
      }
      paramc = new StringBuilder("expected ");
      paramc.append(d);
      paramc.append(" bytes but received ");
      paramc.append(paramLong);
      throw new ProtocolException(paramc.toString());
    }
    throw new IllegalStateException("closed");
  }
  
  public final void close()
    throws IOException
  {
    if (c) {
      return;
    }
    c = true;
    if (d <= 0L)
    {
      e.a(b);
      a.d = 3;
      return;
    }
    throw new ProtocolException("unexpected end of stream");
  }
  
  public final void flush()
    throws IOException
  {
    if (c) {
      return;
    }
    a.c.flush();
  }
  
  public final v l_()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.e.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */