package com.d.a.a.b;

import com.d.a.i;
import com.d.a.p;
import com.d.a.p.a;
import com.d.a.q;
import com.d.a.x;
import com.d.a.x.a;
import com.d.a.y;
import com.d.a.z;
import d.c;
import d.t;
import d.u;
import java.io.EOFException;
import java.io.IOException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.util.concurrent.TimeUnit;

public final class e
  implements j
{
  final s a;
  final d.e b;
  final d.d c;
  int d = 0;
  private h e;
  
  public e(s params, d.e parame, d.d paramd)
  {
    a = params;
    b = parame;
    c = paramd;
  }
  
  public final x.a a()
    throws IOException
  {
    return c();
  }
  
  public final y a(x paramx)
    throws IOException
  {
    Object localObject;
    if (!h.c(paramx))
    {
      localObject = a(0L);
    }
    else if ("chunked".equalsIgnoreCase(paramx.a("Transfer-Encoding")))
    {
      localObject = e;
      if (d == 4)
      {
        d = 5;
        localObject = new c((h)localObject);
      }
      else
      {
        paramx = new StringBuilder("state: ");
        paramx.append(d);
        throw new IllegalStateException(paramx.toString());
      }
    }
    else
    {
      long l = k.a(paramx);
      if (l != -1L)
      {
        localObject = a(l);
      }
      else
      {
        if (d != 4) {
          break label189;
        }
        localObject = a;
        if (localObject == null) {
          break label179;
        }
        d = 5;
        ((s)localObject).a(true, false, false);
        localObject = new f((byte)0);
      }
    }
    return new l(f, d.n.a((u)localObject));
    label179:
    throw new IllegalStateException("streamAllocation == null");
    label189:
    paramx = new StringBuilder("state: ");
    paramx.append(d);
    throw new IllegalStateException(paramx.toString());
  }
  
  public final t a(com.d.a.v paramv, long paramLong)
    throws IOException
  {
    if ("chunked".equalsIgnoreCase(paramv.a("Transfer-Encoding")))
    {
      if (d == 1)
      {
        d = 2;
        return new b((byte)0);
      }
      paramv = new StringBuilder("state: ");
      paramv.append(d);
      throw new IllegalStateException(paramv.toString());
    }
    if (paramLong != -1L)
    {
      if (d == 1)
      {
        d = 2;
        return new d(paramLong, (byte)0);
      }
      paramv = new StringBuilder("state: ");
      paramv.append(d);
      throw new IllegalStateException(paramv.toString());
    }
    throw new IllegalStateException("Cannot stream a request body without chunked encoding or a known content length!");
  }
  
  public final u a(long paramLong)
    throws IOException
  {
    if (d == 4)
    {
      d = 5;
      return new e(paramLong);
    }
    StringBuilder localStringBuilder = new StringBuilder("state: ");
    localStringBuilder.append(d);
    throw new IllegalStateException(localStringBuilder.toString());
  }
  
  public final void a(h paramh)
  {
    e = paramh;
  }
  
  public final void a(o paramo)
    throws IOException
  {
    if (d == 1)
    {
      d = 3;
      paramo.a(c);
      return;
    }
    paramo = new StringBuilder("state: ");
    paramo.append(d);
    throw new IllegalStateException(paramo.toString());
  }
  
  public final void a(p paramp, String paramString)
    throws IOException
  {
    if (d == 0)
    {
      c.b(paramString).b("\r\n");
      int i = 0;
      int j = a.length / 2;
      while (i < j)
      {
        c.b(paramp.a(i)).b(": ").b(paramp.b(i)).b("\r\n");
        i += 1;
      }
      c.b("\r\n");
      d = 1;
      return;
    }
    paramp = new StringBuilder("state: ");
    paramp.append(d);
    throw new IllegalStateException(paramp.toString());
  }
  
  public final void a(com.d.a.v paramv)
    throws IOException
  {
    e.a();
    Object localObject = e.c.a().a().b.type();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(b);
    localStringBuilder.append(' ');
    int i;
    if ((!a.c()) && (localObject == Proxy.Type.HTTP)) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      localStringBuilder.append(a);
    } else {
      localStringBuilder.append(n.a(a));
    }
    localStringBuilder.append(" HTTP/1.1");
    localObject = localStringBuilder.toString();
    a(c, (String)localObject);
  }
  
  public final void b()
    throws IOException
  {
    c.flush();
  }
  
  public final x.a c()
    throws IOException
  {
    int i = d;
    Object localObject1;
    if ((i != 1) && (i != 3))
    {
      localObject1 = new StringBuilder("state: ");
      ((StringBuilder)localObject1).append(d);
      throw new IllegalStateException(((StringBuilder)localObject1).toString());
    }
    try
    {
      do
      {
        localObject1 = r.a(b.q());
        localObject2 = new x.a();
        b = a;
        c = b;
        d = c;
        localObject2 = ((x.a)localObject2).a(d());
      } while (b == 100);
      d = 4;
      return (x.a)localObject2;
    }
    catch (EOFException localEOFException)
    {
      Object localObject2 = new StringBuilder("unexpected end of stream on ");
      ((StringBuilder)localObject2).append(a);
      localObject2 = new IOException(((StringBuilder)localObject2).toString());
      ((IOException)localObject2).initCause(localEOFException);
      throw ((Throwable)localObject2);
    }
  }
  
  public final p d()
    throws IOException
  {
    p.a locala = new p.a();
    for (;;)
    {
      String str = b.q();
      if (str.length() == 0) {
        break;
      }
      com.d.a.a.d.b.a(locala, str);
    }
    return locala.a();
  }
  
  abstract class a
    implements u
  {
    protected final d.j a = new d.j(b.l_());
    protected boolean b;
    
    private a() {}
    
    protected final void b()
      throws IOException
    {
      if (d == 5)
      {
        e.a(a);
        localObject = e.this;
        d = 6;
        if (a != null) {
          a.a(e.this);
        }
        return;
      }
      Object localObject = new StringBuilder("state: ");
      ((StringBuilder)localObject).append(d);
      throw new IllegalStateException(((StringBuilder)localObject).toString());
    }
    
    protected final void c()
    {
      if (d == 6) {
        return;
      }
      e locale = e.this;
      d = 6;
      if (a != null)
      {
        a.a(true, false, false);
        a.a(e.this);
      }
    }
    
    public final d.v l_()
    {
      return a;
    }
  }
  
  final class b
    implements t
  {
    private final d.j b = new d.j(c.l_());
    private boolean c;
    
    private b() {}
    
    public final void a_(c paramc, long paramLong)
      throws IOException
    {
      if (!c)
      {
        if (paramLong == 0L) {
          return;
        }
        c.k(paramLong);
        c.b("\r\n");
        c.a_(paramc, paramLong);
        c.b("\r\n");
        return;
      }
      throw new IllegalStateException("closed");
    }
    
    public final void close()
      throws IOException
    {
      try
      {
        boolean bool = c;
        if (bool) {
          return;
        }
        c = true;
        c.b("0\r\n\r\n");
        e.a(b);
        d = 3;
        return;
      }
      finally {}
    }
    
    public final void flush()
      throws IOException
    {
      try
      {
        boolean bool = c;
        if (bool) {
          return;
        }
        c.flush();
        return;
      }
      finally {}
    }
    
    public final d.v l_()
    {
      return b;
    }
  }
  
  final class c
    extends e.a
  {
    private long e = -1L;
    private boolean f = true;
    private final h g;
    
    c(h paramh)
      throws IOException
    {
      super((byte)0);
      g = paramh;
    }
    
    public final long a(c paramc, long paramLong)
      throws IOException
    {
      if (paramLong >= 0L)
      {
        if (!b)
        {
          if (!f) {
            return -1L;
          }
          long l = e;
          if ((l == 0L) || (l == -1L)) {
            if (e != -1L) {
              b.q();
            }
          }
          try
          {
            e = b.n();
            String str = b.q().trim();
            if (e >= 0L) {
              if (!str.isEmpty())
              {
                boolean bool = str.startsWith(";");
                if (!bool) {}
              }
              else
              {
                if (e == 0L)
                {
                  f = false;
                  g.a(d());
                  b();
                }
                if (!f) {
                  return -1L;
                }
                paramLong = b.a(paramc, Math.min(paramLong, e));
                if (paramLong != -1L)
                {
                  e -= paramLong;
                  return paramLong;
                }
                c();
                throw new ProtocolException("unexpected end of stream");
              }
            }
            paramc = new StringBuilder("expected chunk size and optional extensions but was \"");
            paramc.append(e);
            paramc.append(str);
            paramc.append("\"");
            throw new ProtocolException(paramc.toString());
          }
          catch (NumberFormatException paramc)
          {
            throw new ProtocolException(paramc.getMessage());
          }
        }
        throw new IllegalStateException("closed");
      }
      throw new IllegalArgumentException("byteCount < 0: ".concat(String.valueOf(paramLong)));
    }
    
    public final void close()
      throws IOException
    {
      if (b) {
        return;
      }
      if ((f) && (!com.d.a.a.j.a(this, TimeUnit.MILLISECONDS))) {
        c();
      }
      b = true;
    }
  }
  
  final class d
    implements t
  {
    private final d.j b = new d.j(c.l_());
    private boolean c;
    private long d;
    
    private d(long paramLong)
    {
      d = paramLong;
    }
    
    public final void a_(c paramc, long paramLong)
      throws IOException
    {
      if (!c)
      {
        com.d.a.a.j.a(b, paramLong);
        if (paramLong <= d)
        {
          c.a_(paramc, paramLong);
          d -= paramLong;
          return;
        }
        paramc = new StringBuilder("expected ");
        paramc.append(d);
        paramc.append(" bytes but received ");
        paramc.append(paramLong);
        throw new ProtocolException(paramc.toString());
      }
      throw new IllegalStateException("closed");
    }
    
    public final void close()
      throws IOException
    {
      if (c) {
        return;
      }
      c = true;
      if (d <= 0L)
      {
        e.a(b);
        d = 3;
        return;
      }
      throw new ProtocolException("unexpected end of stream");
    }
    
    public final void flush()
      throws IOException
    {
      if (c) {
        return;
      }
      c.flush();
    }
    
    public final d.v l_()
    {
      return b;
    }
  }
  
  final class e
    extends e.a
  {
    private long e;
    
    public e(long paramLong)
      throws IOException
    {
      super((byte)0);
      e = paramLong;
      if (e == 0L) {
        b();
      }
    }
    
    public final long a(c paramc, long paramLong)
      throws IOException
    {
      if (paramLong >= 0L)
      {
        if (!b)
        {
          if (e == 0L) {
            return -1L;
          }
          paramLong = b.a(paramc, Math.min(e, paramLong));
          if (paramLong != -1L)
          {
            e -= paramLong;
            if (e == 0L) {
              b();
            }
            return paramLong;
          }
          c();
          throw new ProtocolException("unexpected end of stream");
        }
        throw new IllegalStateException("closed");
      }
      throw new IllegalArgumentException("byteCount < 0: ".concat(String.valueOf(paramLong)));
    }
    
    public final void close()
      throws IOException
    {
      if (b) {
        return;
      }
      if ((e != 0L) && (!com.d.a.a.j.a(this, TimeUnit.MILLISECONDS))) {
        c();
      }
      b = true;
    }
  }
  
  final class f
    extends e.a
  {
    private boolean e;
    
    private f()
    {
      super((byte)0);
    }
    
    public final long a(c paramc, long paramLong)
      throws IOException
    {
      if (paramLong >= 0L)
      {
        if (!b)
        {
          if (e) {
            return -1L;
          }
          paramLong = b.a(paramc, paramLong);
          if (paramLong == -1L)
          {
            e = true;
            b();
            return -1L;
          }
          return paramLong;
        }
        throw new IllegalStateException("closed");
      }
      throw new IllegalArgumentException("byteCount < 0: ".concat(String.valueOf(paramLong)));
    }
    
    public final void close()
      throws IOException
    {
      if (b) {
        return;
      }
      if (!e) {
        c();
      }
      b = true;
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */