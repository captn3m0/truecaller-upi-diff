package com.d.a.a.b;

import com.d.a.a.j;
import d.c;
import d.d;
import d.e;
import d.u;
import d.v;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public final class h$2
  implements u
{
  boolean a;
  
  public h$2(h paramh, e parame, b paramb, d paramd) {}
  
  public final long a(c paramc, long paramLong)
    throws IOException
  {
    try
    {
      paramLong = b.a(paramc, paramLong);
      if (paramLong == -1L)
      {
        if (!a)
        {
          a = true;
          d.close();
        }
        return -1L;
      }
      paramc.a(d.b(), b - paramLong, paramLong);
      d.v();
      return paramLong;
    }
    catch (IOException paramc)
    {
      if (!a)
      {
        a = true;
        c.a();
      }
      throw paramc;
    }
  }
  
  public final void close()
    throws IOException
  {
    if ((!a) && (!j.a(this, TimeUnit.MILLISECONDS)))
    {
      a = true;
      c.a();
    }
    b.close();
  }
  
  public final v l_()
  {
    return b.l_();
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.h.2
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */