package com.d.a.a.b;

import com.d.a.a;
import com.d.a.a.i;
import com.d.a.n;
import com.d.a.z;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.ProxySelector;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

public final class q
{
  final a a;
  final i b;
  private Proxy c;
  private InetSocketAddress d;
  private List<Proxy> e = Collections.emptyList();
  private int f;
  private List<InetSocketAddress> g = Collections.emptyList();
  private int h;
  private final List<z> i = new ArrayList();
  
  public q(a parama, i parami)
  {
    a = parama;
    b = parami;
    parami = a;
    parama = h;
    if (parama != null)
    {
      e = Collections.singletonList(parama);
    }
    else
    {
      e = new ArrayList();
      parama = a.g.select(parami.b());
      if (parama != null) {
        e.addAll(parama);
      }
      e.removeAll(Collections.singleton(Proxy.NO_PROXY));
      e.add(Proxy.NO_PROXY);
    }
    f = 0;
  }
  
  private void a(Proxy paramProxy)
    throws IOException
  {
    g = new ArrayList();
    Object localObject;
    int j;
    if ((paramProxy.type() != Proxy.Type.DIRECT) && (paramProxy.type() != Proxy.Type.SOCKS))
    {
      localObject = paramProxy.address();
      if ((localObject instanceof InetSocketAddress))
      {
        InetSocketAddress localInetSocketAddress = (InetSocketAddress)localObject;
        localObject = localInetSocketAddress.getAddress();
        if (localObject == null) {
          localObject = localInetSocketAddress.getHostName();
        } else {
          localObject = ((InetAddress)localObject).getHostAddress();
        }
        j = localInetSocketAddress.getPort();
      }
      else
      {
        paramProxy = new StringBuilder("Proxy.address() is not an InetSocketAddress: ");
        paramProxy.append(localObject.getClass());
        throw new IllegalArgumentException(paramProxy.toString());
      }
    }
    else
    {
      localObject = a.a.b;
      j = a.a.c;
    }
    if ((j > 0) && (j <= 65535))
    {
      if (paramProxy.type() == Proxy.Type.SOCKS)
      {
        g.add(InetSocketAddress.createUnresolved((String)localObject, j));
      }
      else
      {
        paramProxy = a.b.a((String)localObject);
        int m = paramProxy.size();
        int k = 0;
        while (k < m)
        {
          localObject = (InetAddress)paramProxy.get(k);
          g.add(new InetSocketAddress((InetAddress)localObject, j));
          k += 1;
        }
      }
      h = 0;
      return;
    }
    paramProxy = new StringBuilder("No route to ");
    paramProxy.append((String)localObject);
    paramProxy.append(":");
    paramProxy.append(j);
    paramProxy.append("; port is out of range");
    throw new SocketException(paramProxy.toString());
  }
  
  private boolean c()
  {
    return f < e.size();
  }
  
  private boolean d()
  {
    return h < g.size();
  }
  
  private boolean e()
  {
    return !i.isEmpty();
  }
  
  public final boolean a()
  {
    return (d()) || (c()) || (e());
  }
  
  public final z b()
    throws IOException
  {
    for (;;)
    {
      if (!d())
      {
        if (!c())
        {
          if (e()) {
            return (z)i.remove(0);
          }
          throw new NoSuchElementException();
        }
        if (c())
        {
          localObject = e;
          j = f;
          f = (j + 1);
          localObject = (Proxy)((List)localObject).get(j);
          a((Proxy)localObject);
          c = ((Proxy)localObject);
        }
        else
        {
          localObject = new StringBuilder("No route to ");
          ((StringBuilder)localObject).append(a.a.b);
          ((StringBuilder)localObject).append("; exhausted proxy configurations: ");
          ((StringBuilder)localObject).append(e);
          throw new SocketException(((StringBuilder)localObject).toString());
        }
      }
      if (!d()) {
        break label229;
      }
      localObject = g;
      int j = h;
      h = (j + 1);
      d = ((InetSocketAddress)((List)localObject).get(j));
      localObject = new z(a, c, d);
      if (!b.c((z)localObject)) {
        break;
      }
      i.add(localObject);
    }
    return (z)localObject;
    label229:
    Object localObject = new StringBuilder("No route to ");
    ((StringBuilder)localObject).append(a.a.b);
    ((StringBuilder)localObject).append("; exhausted inet socket addresses: ");
    ((StringBuilder)localObject).append(g);
    throw new SocketException(((StringBuilder)localObject).toString());
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */