package com.d.a.a;

import d.u;
import java.io.Closeable;

public final class b$c
  implements Closeable
{
  public final String a;
  public final long b;
  public final u[] c;
  private final long[] e;
  
  private b$c(b paramb, String paramString, long paramLong, u[] paramArrayOfu, long[] paramArrayOfLong)
  {
    a = paramString;
    b = paramLong;
    c = paramArrayOfu;
    e = paramArrayOfLong;
  }
  
  public final void close()
  {
    u[] arrayOfu = c;
    int j = arrayOfu.length;
    int i = 0;
    while (i < j)
    {
      j.a(arrayOfu[i]);
      i += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */