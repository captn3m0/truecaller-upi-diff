package com.d.a.a;

import com.d.a.u;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLSocket;

final class h$c
  extends h.b
{
  private final Method a;
  private final Method b;
  private final Method c;
  private final Class<?> d;
  private final Class<?> e;
  
  public h$c(Class<?> paramClass1, Method paramMethod1, Method paramMethod2, Method paramMethod3, Class<?> paramClass2, Class<?> paramClass3)
  {
    super(paramClass1);
    a = paramMethod1;
    b = paramMethod2;
    c = paramMethod3;
    d = paramClass2;
    e = paramClass3;
  }
  
  public final void a(SSLSocket paramSSLSocket)
  {
    try
    {
      c.invoke(null, new Object[] { paramSSLSocket });
      return;
    }
    catch (IllegalAccessException|InvocationTargetException paramSSLSocket)
    {
      for (;;) {}
    }
    throw new AssertionError();
  }
  
  public final void a(SSLSocket paramSSLSocket, String paramString, List<u> paramList)
  {
    paramString = new ArrayList(paramList.size());
    int j = paramList.size();
    int i = 0;
    Object localObject;
    while (i < j)
    {
      localObject = (u)paramList.get(i);
      if (localObject != u.a) {
        paramString.add(((u)localObject).toString());
      }
      i += 1;
    }
    try
    {
      paramList = h.class.getClassLoader();
      localObject = d;
      Class localClass = e;
      paramString = new h.d(paramString);
      paramString = Proxy.newProxyInstance(paramList, new Class[] { localObject, localClass }, paramString);
      a.invoke(null, new Object[] { paramSSLSocket, paramString });
      return;
    }
    catch (IllegalAccessException paramSSLSocket) {}catch (InvocationTargetException paramSSLSocket) {}
    throw new AssertionError(paramSSLSocket);
  }
  
  public final String b(SSLSocket paramSSLSocket)
  {
    try
    {
      paramSSLSocket = (h.d)Proxy.getInvocationHandler(b.invoke(null, new Object[] { paramSSLSocket }));
      if ((!h.d.a(paramSSLSocket)) && (h.d.b(paramSSLSocket) == null))
      {
        d.a.log(Level.INFO, "ALPN callback dropped: SPDY and HTTP/2 are disabled. Is alpn-boot on the boot class path?");
        return null;
      }
      if (h.d.a(paramSSLSocket)) {
        return null;
      }
      paramSSLSocket = h.d.b(paramSSLSocket);
      return paramSSLSocket;
    }
    catch (InvocationTargetException|IllegalAccessException paramSSLSocket)
    {
      for (;;) {}
    }
    throw new AssertionError();
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.h.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */