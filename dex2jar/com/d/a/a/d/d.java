package com.d.a.a.d;

import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;

public final class d
  implements HostnameVerifier
{
  public static final d a = new d();
  private static final Pattern b = Pattern.compile("([0-9a-fA-F]*:[0-9a-fA-F:.]*)|([\\d.]+)");
  
  public static List<String> a(X509Certificate paramX509Certificate)
  {
    List localList = a(paramX509Certificate, 7);
    paramX509Certificate = a(paramX509Certificate, 2);
    ArrayList localArrayList = new ArrayList(localList.size() + paramX509Certificate.size());
    localArrayList.addAll(localList);
    localArrayList.addAll(paramX509Certificate);
    return localArrayList;
  }
  
  private static List<String> a(X509Certificate paramX509Certificate, int paramInt)
  {
    ArrayList localArrayList = new ArrayList();
    try
    {
      paramX509Certificate = paramX509Certificate.getSubjectAlternativeNames();
      if (paramX509Certificate == null) {
        return Collections.emptyList();
      }
      paramX509Certificate = paramX509Certificate.iterator();
      while (paramX509Certificate.hasNext())
      {
        Object localObject = (List)paramX509Certificate.next();
        if ((localObject != null) && (((List)localObject).size() >= 2))
        {
          Integer localInteger = (Integer)((List)localObject).get(0);
          if ((localInteger != null) && (localInteger.intValue() == paramInt))
          {
            localObject = (String)((List)localObject).get(1);
            if (localObject != null) {
              localArrayList.add(localObject);
            }
          }
        }
      }
      return localArrayList;
    }
    catch (CertificateParsingException paramX509Certificate)
    {
      for (;;) {}
    }
    return Collections.emptyList();
  }
  
  private static boolean a(String paramString1, String paramString2)
  {
    if ((paramString1 != null) && (paramString1.length() != 0) && (!paramString1.startsWith(".")))
    {
      if (paramString1.endsWith("..")) {
        return false;
      }
      if ((paramString2 != null) && (paramString2.length() != 0) && (!paramString2.startsWith(".")))
      {
        if (paramString2.endsWith("..")) {
          return false;
        }
        Object localObject = paramString1;
        if (!paramString1.endsWith("."))
        {
          localObject = new StringBuilder();
          ((StringBuilder)localObject).append(paramString1);
          ((StringBuilder)localObject).append('.');
          localObject = ((StringBuilder)localObject).toString();
        }
        paramString1 = paramString2;
        if (!paramString2.endsWith("."))
        {
          paramString1 = new StringBuilder();
          paramString1.append(paramString2);
          paramString1.append('.');
          paramString1 = paramString1.toString();
        }
        paramString1 = paramString1.toLowerCase(Locale.US);
        if (!paramString1.contains("*")) {
          return ((String)localObject).equals(paramString1);
        }
        if (paramString1.startsWith("*."))
        {
          if (paramString1.indexOf('*', 1) != -1) {
            return false;
          }
          if (((String)localObject).length() < paramString1.length()) {
            return false;
          }
          if ("*.".equals(paramString1)) {
            return false;
          }
          paramString1 = paramString1.substring(1);
          if (!((String)localObject).endsWith(paramString1)) {
            return false;
          }
          int i = ((String)localObject).length() - paramString1.length();
          return (i <= 0) || (((String)localObject).lastIndexOf('.', i - 1) == -1);
        }
        return false;
      }
      return false;
    }
    return false;
  }
  
  public final boolean verify(String paramString, SSLSession paramSSLSession)
  {
    for (;;)
    {
      int i;
      try
      {
        paramSSLSession = (X509Certificate)paramSSLSession.getPeerCertificates()[0];
        if (b.matcher(paramString).matches())
        {
          paramSSLSession = a(paramSSLSession, 7);
          j = paramSSLSession.size();
          i = 0;
          if (i >= j) {
            break label754;
          }
          if (paramString.equalsIgnoreCase((String)paramSSLSession.get(i))) {
            return true;
          }
        }
        else
        {
          String str = paramString.toLowerCase(Locale.US);
          paramString = a(paramSSLSession, 2);
          int k = paramString.size();
          i = 0;
          j = 0;
          if (i < k)
          {
            if (!a(str, (String)paramString.get(i))) {
              break label756;
            }
            return true;
          }
          c localc;
          Object localObject;
          if (j == 0)
          {
            localc = new c(paramSSLSession.getSubjectX500Principal());
            c = 0;
            d = 0;
            e = 0;
            f = 0;
            g = a.toCharArray();
            paramString = localc.a();
            localObject = null;
            paramSSLSession = paramString;
            if (paramString == null)
            {
              paramString = (String)localObject;
            }
            else
            {
              paramString = "";
              if (c == b) {
                paramString = (String)localObject;
              }
            }
          }
          switch (g[c])
          {
          case '#': 
            paramString = localc.c();
            continue;
            paramString = localc.b();
            break;
          case '"': 
            c += 1;
            d = c;
            e = d;
            if (c != b)
            {
              if (g[c] == '"')
              {
                c += 1;
                if ((c < b) && (g[c] == ' '))
                {
                  c += 1;
                  continue;
                }
                paramString = new String(g, d, e - d);
              }
              else
              {
                if (g[c] == '\\') {
                  g[e] = localc.d();
                } else {
                  g[e] = g[c];
                }
                c += 1;
                e += 1;
              }
            }
            else
            {
              paramString = new StringBuilder("Unexpected end of DN: ");
              paramString.append(a);
              throw new IllegalStateException(paramString.toString());
            }
          case '+': 
          case ',': 
          case ';': 
            if (!"cn".equalsIgnoreCase(paramSSLSession))
            {
              if (c >= b) {
                paramString = (String)localObject;
              }
            }
            else
            {
              if (paramString == null) {
                continue;
              }
              return a(str, paramString);
            }
            if ((g[c] != ',') && (g[c] != ';') && (g[c] != '+'))
            {
              paramString = new StringBuilder("Malformed DN: ");
              paramString.append(a);
              throw new IllegalStateException(paramString.toString());
            }
            c += 1;
            paramSSLSession = localc.a();
            if (paramSSLSession != null) {
              continue;
            }
            paramString = new StringBuilder("Malformed DN: ");
            paramString.append(a);
            throw new IllegalStateException(paramString.toString());
            return false;
          }
        }
      }
      catch (SSLException paramString)
      {
        return false;
      }
      i += 1;
      continue;
      label754:
      return false;
      label756:
      i += 1;
      int j = 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.d.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */