package com.d.a.a.d;

import javax.security.auth.x500.X500Principal;

final class c
{
  final String a;
  final int b;
  int c;
  int d;
  int e;
  int f;
  char[] g;
  
  public c(X500Principal paramX500Principal)
  {
    a = paramX500Principal.getName("RFC2253");
    b = a.length();
  }
  
  private int a(int paramInt)
  {
    int i = paramInt + 1;
    if (i < b)
    {
      paramInt = g[paramInt];
      if ((paramInt >= 48) && (paramInt <= 57))
      {
        paramInt -= 48;
      }
      else if ((paramInt >= 97) && (paramInt <= 102))
      {
        paramInt -= 87;
      }
      else
      {
        if ((paramInt < 65) || (paramInt > 70)) {
          break label177;
        }
        paramInt -= 55;
      }
      i = g[i];
      if ((i >= 48) && (i <= 57))
      {
        i -= 48;
      }
      else if ((i >= 97) && (i <= 102))
      {
        i -= 87;
      }
      else
      {
        if ((i < 65) || (i > 70)) {
          break label146;
        }
        i -= 55;
      }
      return (paramInt << 4) + i;
      label146:
      localStringBuilder = new StringBuilder("Malformed DN: ");
      localStringBuilder.append(a);
      throw new IllegalStateException(localStringBuilder.toString());
      label177:
      localStringBuilder = new StringBuilder("Malformed DN: ");
      localStringBuilder.append(a);
      throw new IllegalStateException(localStringBuilder.toString());
    }
    StringBuilder localStringBuilder = new StringBuilder("Malformed DN: ");
    localStringBuilder.append(a);
    throw new IllegalStateException(localStringBuilder.toString());
  }
  
  private char e()
  {
    int i = a(c);
    c += 1;
    if (i < 128) {
      return (char)i;
    }
    if ((i >= 192) && (i <= 247))
    {
      int j;
      if (i <= 223)
      {
        i &= 0x1F;
        j = 1;
      }
      else if (i <= 239)
      {
        j = 2;
        i &= 0xF;
      }
      else
      {
        j = 3;
        i &= 0x7;
      }
      int k = 0;
      while (k < j)
      {
        c += 1;
        int m = c;
        if (m != b)
        {
          if (g[m] != '\\') {
            return '?';
          }
          c = (m + 1);
          m = a(c);
          c += 1;
          if ((m & 0xC0) != 128) {
            return '?';
          }
          i = (i << 6) + (m & 0x3F);
          k += 1;
        }
        else
        {
          return '?';
        }
      }
      return (char)i;
    }
    return '?';
  }
  
  final String a()
  {
    for (;;)
    {
      i = c;
      if ((i >= b) || (g[i] != ' ')) {
        break;
      }
      c = (i + 1);
    }
    int i = c;
    if (i == b) {
      return null;
    }
    d = i;
    for (c = (i + 1);; c = (i + 1))
    {
      i = c;
      if (i >= b) {
        break;
      }
      localObject = g;
      if ((localObject[i] == '=') || (localObject[i] == ' ')) {
        break;
      }
    }
    i = c;
    if (i < b)
    {
      e = i;
      if (g[i] == ' ')
      {
        for (;;)
        {
          i = c;
          if (i >= b) {
            break;
          }
          localObject = g;
          if ((localObject[i] == '=') || (localObject[i] != ' ')) {
            break;
          }
          c = (i + 1);
        }
        localObject = g;
        i = c;
        if ((localObject[i] != '=') || (i == b))
        {
          localObject = new StringBuilder("Unexpected end of DN: ");
          ((StringBuilder)localObject).append(a);
          throw new IllegalStateException(((StringBuilder)localObject).toString());
        }
      }
      do
      {
        c += 1;
        i = c;
      } while ((i < b) && (g[i] == ' '));
      i = e;
      int j = d;
      if (i - j > 4)
      {
        localObject = g;
        if ((localObject[(j + 3)] == '.') && ((localObject[j] == 'O') || (localObject[j] == 'o')))
        {
          localObject = g;
          i = d;
          if ((localObject[(i + 1)] == 'I') || (localObject[(i + 1)] == 'i'))
          {
            localObject = g;
            i = d;
            if ((localObject[(i + 2)] == 'D') || (localObject[(i + 2)] == 'd')) {
              d += 4;
            }
          }
        }
      }
      localObject = g;
      i = d;
      return new String((char[])localObject, i, e - i);
    }
    Object localObject = new StringBuilder("Unexpected end of DN: ");
    ((StringBuilder)localObject).append(a);
    throw new IllegalStateException(((StringBuilder)localObject).toString());
  }
  
  final String b()
  {
    int i = c;
    if (i + 4 < b)
    {
      d = i;
      for (c = (i + 1);; c += 1)
      {
        i = c;
        if (i == b) {
          break;
        }
        localObject = g;
        if ((localObject[i] == '+') || (localObject[i] == ',') || (localObject[i] == ';')) {
          break;
        }
        if (localObject[i] == ' ')
        {
          e = i;
          for (c = (i + 1);; c = (i + 1))
          {
            i = c;
            if ((i >= b) || (g[i] != ' ')) {
              break;
            }
          }
        }
        if ((localObject[i] >= 'A') && (localObject[i] <= 'F')) {
          localObject[i] = ((char)(localObject[i] + ' '));
        }
      }
      e = c;
      i = e;
      int j = d;
      int k = i - j;
      if ((k >= 5) && ((k & 0x1) != 0))
      {
        localObject = new byte[k / 2];
        i = 0;
        j += 1;
        while (i < localObject.length)
        {
          localObject[i] = ((byte)a(j));
          j += 2;
          i += 1;
        }
        return new String(g, d, k);
      }
      localObject = new StringBuilder("Unexpected end of DN: ");
      ((StringBuilder)localObject).append(a);
      throw new IllegalStateException(((StringBuilder)localObject).toString());
    }
    Object localObject = new StringBuilder("Unexpected end of DN: ");
    ((StringBuilder)localObject).append(a);
    throw new IllegalStateException(((StringBuilder)localObject).toString());
  }
  
  final String c()
  {
    int i = c;
    d = i;
    e = i;
    label190:
    do
    {
      for (;;)
      {
        i = c;
        if (i >= b)
        {
          arrayOfChar = g;
          i = d;
          return new String(arrayOfChar, i, e - i);
        }
        arrayOfChar = g;
        j = arrayOfChar[i];
        if (j == 32) {
          break label190;
        }
        if (j == 59) {
          break;
        }
        if (j != 92) {}
        switch (j)
        {
        default: 
          j = e;
          e = (j + 1);
          arrayOfChar[j] = arrayOfChar[i];
          c = (i + 1);
          continue;
          i = e;
          e = (i + 1);
          arrayOfChar[i] = d();
          c += 1;
        }
      }
      arrayOfChar = g;
      i = d;
      return new String(arrayOfChar, i, e - i);
      int j = e;
      f = j;
      c = (i + 1);
      e = (j + 1);
      arrayOfChar[j] = ' ';
      for (;;)
      {
        i = c;
        if (i >= b) {
          break;
        }
        arrayOfChar = g;
        if (arrayOfChar[i] != ' ') {
          break;
        }
        j = e;
        e = (j + 1);
        arrayOfChar[j] = ' ';
        c = (i + 1);
      }
      i = c;
      if (i == b) {
        break;
      }
      arrayOfChar = g;
    } while ((arrayOfChar[i] != ',') && (arrayOfChar[i] != '+') && (arrayOfChar[i] != ';'));
    char[] arrayOfChar = g;
    i = d;
    return new String(arrayOfChar, i, f - i);
  }
  
  final char d()
  {
    c += 1;
    int i = c;
    if (i != b)
    {
      i = g[i];
      if ((i != 32) && (i != 37) && (i != 92) && (i != 95)) {
        switch (i)
        {
        default: 
          switch (i)
          {
          default: 
            switch (i)
            {
            default: 
              return e();
            }
            break;
          }
          break;
        }
      }
      return g[c];
    }
    StringBuilder localStringBuilder = new StringBuilder("Unexpected end of DN: ");
    localStringBuilder.append(a);
    throw new IllegalStateException(localStringBuilder.toString());
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.d.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */