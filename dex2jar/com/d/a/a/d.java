package com.d.a.a;

import com.d.a.a;
import com.d.a.a.b.s;
import com.d.a.a.c.b;
import com.d.a.j;
import com.d.a.k;
import com.d.a.p.a;
import com.d.a.t;
import java.util.logging.Logger;
import javax.net.ssl.SSLSocket;

public abstract class d
{
  public static final Logger a = Logger.getLogger(t.class.getName());
  public static d b;
  
  public abstract b a(j paramj, a parama, s params);
  
  public abstract e a(t paramt);
  
  public abstract i a(j paramj);
  
  public abstract void a(k paramk, SSLSocket paramSSLSocket, boolean paramBoolean);
  
  public abstract void a(p.a parama, String paramString);
  
  public abstract boolean a(j paramj, b paramb);
  
  public abstract void b(j paramj, b paramb);
}

/* Location:
 * Qualified Name:     com.d.a.a.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */