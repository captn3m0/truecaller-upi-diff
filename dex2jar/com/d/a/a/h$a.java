package com.d.a.a;

import com.d.a.a.d.a;
import com.d.a.a.d.f;
import com.d.a.u;
import d.c;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.List;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

final class h$a
  extends h
{
  private final Class<?> a;
  private final g<Socket> b;
  private final g<Socket> c;
  private final Method d;
  private final Method e;
  private final g<Socket> f;
  private final g<Socket> g;
  
  public h$a(Class<?> paramClass, g<Socket> paramg1, g<Socket> paramg2, Method paramMethod1, Method paramMethod2, g<Socket> paramg3, g<Socket> paramg4)
  {
    a = paramClass;
    b = paramg1;
    c = paramg2;
    d = paramMethod1;
    e = paramMethod2;
    f = paramg3;
    g = paramg4;
  }
  
  public final f a(X509TrustManager paramX509TrustManager)
  {
    f localf = a.a(paramX509TrustManager);
    if (localf != null) {
      return localf;
    }
    return super.a(paramX509TrustManager);
  }
  
  public final X509TrustManager a(SSLSocketFactory paramSSLSocketFactory)
  {
    Object localObject2 = a(paramSSLSocketFactory, a, "sslParameters");
    Object localObject1 = localObject2;
    if (localObject2 == null) {}
    try
    {
      localObject1 = a(paramSSLSocketFactory, Class.forName("com.google.android.gms.org.conscrypt.SSLParametersImpl", false, paramSSLSocketFactory.getClass().getClassLoader()), "sslParameters");
    }
    catch (ClassNotFoundException paramSSLSocketFactory)
    {
      for (;;) {}
    }
    return null;
    paramSSLSocketFactory = (X509TrustManager)a(localObject1, X509TrustManager.class, "x509TrustManager");
    if (paramSSLSocketFactory != null) {
      return paramSSLSocketFactory;
    }
    return (X509TrustManager)a(localObject1, X509TrustManager.class, "trustManager");
  }
  
  public final void a(Socket paramSocket, InetSocketAddress paramInetSocketAddress, int paramInt)
    throws IOException
  {
    try
    {
      paramSocket.connect(paramInetSocketAddress, paramInt);
      return;
    }
    catch (SecurityException paramSocket)
    {
      paramInetSocketAddress = new IOException("Exception in connect");
      paramInetSocketAddress.initCause(paramSocket);
      throw paramInetSocketAddress;
    }
    catch (AssertionError paramSocket)
    {
      if (j.a(paramSocket)) {
        throw new IOException(paramSocket);
      }
      throw paramSocket;
    }
  }
  
  public final void a(SSLSocket paramSSLSocket, String paramString, List<u> paramList)
  {
    if (paramString != null)
    {
      b.a(paramSSLSocket, new Object[] { Boolean.TRUE });
      c.a(paramSSLSocket, new Object[] { paramString });
    }
    paramString = g;
    if ((paramString != null) && (paramString.a(paramSSLSocket)))
    {
      paramString = new c();
      int j = paramList.size();
      int i = 0;
      while (i < j)
      {
        u localu = (u)paramList.get(i);
        if (localu != u.a)
        {
          paramString.b(localu.toString().length());
          paramString.a(localu.toString());
        }
        i += 1;
      }
      paramString = paramString.r();
      g.b(paramSSLSocket, new Object[] { paramString });
    }
  }
  
  public final String b(SSLSocket paramSSLSocket)
  {
    g localg = f;
    if (localg == null) {
      return null;
    }
    if (!localg.a(paramSSLSocket)) {
      return null;
    }
    paramSSLSocket = (byte[])f.b(paramSSLSocket, new Object[0]);
    if (paramSSLSocket != null) {
      return new String(paramSSLSocket, j.c);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.h.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */