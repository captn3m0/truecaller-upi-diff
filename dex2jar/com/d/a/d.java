package com.d.a;

import java.util.concurrent.TimeUnit;

public final class d
{
  public static final d a;
  public static final d b;
  public final boolean c;
  public final boolean d;
  public final int e;
  public final boolean f;
  public final boolean g;
  public final boolean h;
  public final int i;
  public final int j;
  public final boolean k;
  String l;
  private final int m;
  private final boolean n;
  
  static
  {
    a locala = new a();
    a = true;
    a = locala.c();
    locala = new a();
    f = true;
    long l1 = TimeUnit.SECONDS.toSeconds(2147483647L);
    int i1;
    if (l1 > 2147483647L) {
      i1 = Integer.MAX_VALUE;
    } else {
      i1 = (int)l1;
    }
    d = i1;
    b = locala.c();
  }
  
  private d(a parama)
  {
    c = a;
    d = b;
    e = c;
    m = -1;
    f = false;
    g = false;
    h = false;
    i = d;
    j = e;
    k = f;
    n = g;
  }
  
  private d(boolean paramBoolean1, boolean paramBoolean2, int paramInt1, int paramInt2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, int paramInt3, int paramInt4, boolean paramBoolean6, boolean paramBoolean7, String paramString)
  {
    c = paramBoolean1;
    d = paramBoolean2;
    e = paramInt1;
    m = paramInt2;
    f = paramBoolean3;
    g = paramBoolean4;
    h = paramBoolean5;
    i = paramInt3;
    j = paramInt4;
    k = paramBoolean6;
    n = paramBoolean7;
    l = paramString;
  }
  
  public static d a(p paramp)
  {
    int i8 = a.length / 2;
    int i7 = 0;
    int i2 = 1;
    Object localObject1 = null;
    boolean bool5 = false;
    boolean bool6 = false;
    int i6 = -1;
    int i5 = -1;
    boolean bool7 = false;
    boolean bool4 = false;
    boolean bool3 = false;
    int i4 = -1;
    int i3 = -1;
    boolean bool2 = false;
    boolean bool1 = false;
    for (;;)
    {
      Object localObject2 = paramp;
      if (i7 >= i8) {
        break;
      }
      String str2 = ((p)localObject2).a(i7);
      String str1 = ((p)localObject2).b(i7);
      if (str2.equalsIgnoreCase("Cache-Control"))
      {
        if (localObject1 != null) {
          i2 = 0;
        } else {
          localObject1 = str1;
        }
      }
      else
      {
        if (!str2.equalsIgnoreCase("Pragma")) {
          break label486;
        }
        i2 = 0;
      }
      int i1 = 0;
      while (i1 < str1.length())
      {
        int i9 = com.d.a.a.b.d.a(str1, i1, "=,;");
        str2 = str1.substring(i1, i9).trim();
        if ((i9 != str1.length()) && (str1.charAt(i9) != ',') && (str1.charAt(i9) != ';'))
        {
          i9 = com.d.a.a.b.d.a(str1, i9 + 1);
          if ((i9 < str1.length()) && (str1.charAt(i9) == '"'))
          {
            i1 = i9 + 1;
            i9 = com.d.a.a.b.d.a(str1, i1, "\"");
            localObject2 = str1.substring(i1, i9);
            i1 = i9 + 1;
          }
          else
          {
            i1 = com.d.a.a.b.d.a(str1, i9, ",;");
            localObject2 = str1.substring(i9, i1).trim();
          }
        }
        else
        {
          i1 = i9 + 1;
          localObject2 = null;
        }
        if ("no-cache".equalsIgnoreCase(str2)) {
          bool5 = true;
        } else if ("no-store".equalsIgnoreCase(str2)) {
          bool6 = true;
        } else if ("max-age".equalsIgnoreCase(str2)) {
          i6 = com.d.a.a.b.d.b((String)localObject2, -1);
        } else if ("s-maxage".equalsIgnoreCase(str2)) {
          i5 = com.d.a.a.b.d.b((String)localObject2, -1);
        } else if ("private".equalsIgnoreCase(str2)) {
          bool7 = true;
        } else if ("public".equalsIgnoreCase(str2)) {
          bool4 = true;
        } else if ("must-revalidate".equalsIgnoreCase(str2)) {
          bool3 = true;
        } else if ("max-stale".equalsIgnoreCase(str2)) {
          i4 = com.d.a.a.b.d.b((String)localObject2, Integer.MAX_VALUE);
        } else if ("min-fresh".equalsIgnoreCase(str2)) {
          i3 = com.d.a.a.b.d.b((String)localObject2, -1);
        } else if ("only-if-cached".equalsIgnoreCase(str2)) {
          bool2 = true;
        } else if ("no-transform".equalsIgnoreCase(str2)) {
          bool1 = true;
        }
      }
      label486:
      i7 += 1;
    }
    if (i2 == 0) {
      localObject1 = null;
    }
    return new d(bool5, bool6, i6, i5, bool7, bool4, bool3, i4, i3, bool2, bool1, (String)localObject1);
  }
  
  public final String toString()
  {
    Object localObject = l;
    if (localObject != null) {
      return (String)localObject;
    }
    localObject = new StringBuilder();
    if (c) {
      ((StringBuilder)localObject).append("no-cache, ");
    }
    if (d) {
      ((StringBuilder)localObject).append("no-store, ");
    }
    if (e != -1)
    {
      ((StringBuilder)localObject).append("max-age=");
      ((StringBuilder)localObject).append(e);
      ((StringBuilder)localObject).append(", ");
    }
    if (m != -1)
    {
      ((StringBuilder)localObject).append("s-maxage=");
      ((StringBuilder)localObject).append(m);
      ((StringBuilder)localObject).append(", ");
    }
    if (f) {
      ((StringBuilder)localObject).append("private, ");
    }
    if (g) {
      ((StringBuilder)localObject).append("public, ");
    }
    if (h) {
      ((StringBuilder)localObject).append("must-revalidate, ");
    }
    if (i != -1)
    {
      ((StringBuilder)localObject).append("max-stale=");
      ((StringBuilder)localObject).append(i);
      ((StringBuilder)localObject).append(", ");
    }
    if (j != -1)
    {
      ((StringBuilder)localObject).append("min-fresh=");
      ((StringBuilder)localObject).append(j);
      ((StringBuilder)localObject).append(", ");
    }
    if (k) {
      ((StringBuilder)localObject).append("only-if-cached, ");
    }
    if (n) {
      ((StringBuilder)localObject).append("no-transform, ");
    }
    if (((StringBuilder)localObject).length() == 0)
    {
      localObject = "";
    }
    else
    {
      ((StringBuilder)localObject).delete(((StringBuilder)localObject).length() - 2, ((StringBuilder)localObject).length());
      localObject = ((StringBuilder)localObject).toString();
    }
    l = ((String)localObject);
    return (String)localObject;
  }
  
  public static final class a
  {
    boolean a;
    boolean b;
    int c = -1;
    int d = -1;
    int e = -1;
    boolean f;
    boolean g;
    
    public final a a()
    {
      a = true;
      return this;
    }
    
    public final a b()
    {
      b = true;
      return this;
    }
    
    public final d c()
    {
      return new d(this, (byte)0);
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */