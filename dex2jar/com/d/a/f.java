package com.d.a;

import com.d.a.a.j;
import java.security.Principal;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.net.ssl.SSLPeerUnverifiedException;

public final class f
{
  public static final f a = new f(new a(), (byte)0);
  private final Map<String, Set<d.f>> b;
  
  private f(a parama)
  {
    b = j.a(a);
  }
  
  private static d.f a(X509Certificate paramX509Certificate)
  {
    return j.a(d.f.a(paramX509Certificate.getPublicKey().getEncoded()));
  }
  
  public static String a(Certificate paramCertificate)
  {
    if ((paramCertificate instanceof X509Certificate))
    {
      StringBuilder localStringBuilder = new StringBuilder("sha1/");
      localStringBuilder.append(a((X509Certificate)paramCertificate).b());
      return localStringBuilder.toString();
    }
    throw new IllegalArgumentException("Certificate pinning requires X509 certificates");
  }
  
  public final void a(String paramString, List<Certificate> paramList)
    throws SSLPeerUnverifiedException
  {
    Object localObject2 = (Set)b.get(paramString);
    int i = paramString.indexOf('.');
    Object localObject1;
    Object localObject3;
    if (i != paramString.lastIndexOf('.'))
    {
      localObject1 = b;
      localObject3 = new StringBuilder("*.");
      ((StringBuilder)localObject3).append(paramString.substring(i + 1));
      localObject1 = (Set)((Map)localObject1).get(((StringBuilder)localObject3).toString());
    }
    else
    {
      localObject1 = null;
    }
    if ((localObject2 == null) && (localObject1 == null))
    {
      localObject1 = null;
    }
    else if ((localObject2 != null) && (localObject1 != null))
    {
      localObject3 = new LinkedHashSet();
      ((Set)localObject3).addAll((Collection)localObject2);
      ((Set)localObject3).addAll((Collection)localObject1);
      localObject1 = localObject3;
    }
    else if (localObject2 != null)
    {
      localObject1 = localObject2;
    }
    if (localObject1 == null) {
      return;
    }
    int k = paramList.size();
    int j = 0;
    i = 0;
    while (i < k)
    {
      if (((Set)localObject1).contains(a((X509Certificate)paramList.get(i)))) {
        return;
      }
      i += 1;
    }
    localObject2 = new StringBuilder("Certificate pinning failure!\n  Peer certificate chain:");
    k = paramList.size();
    i = j;
    while (i < k)
    {
      localObject3 = (X509Certificate)paramList.get(i);
      ((StringBuilder)localObject2).append("\n    ");
      ((StringBuilder)localObject2).append(a((Certificate)localObject3));
      ((StringBuilder)localObject2).append(": ");
      ((StringBuilder)localObject2).append(((X509Certificate)localObject3).getSubjectDN().getName());
      i += 1;
    }
    ((StringBuilder)localObject2).append("\n  Pinned certificates for ");
    ((StringBuilder)localObject2).append(paramString);
    ((StringBuilder)localObject2).append(":");
    paramString = ((Set)localObject1).iterator();
    while (paramString.hasNext())
    {
      paramList = (d.f)paramString.next();
      ((StringBuilder)localObject2).append("\n    sha1/");
      ((StringBuilder)localObject2).append(paramList.b());
    }
    throw new SSLPeerUnverifiedException(((StringBuilder)localObject2).toString());
  }
  
  public static final class a
  {
    final Map<String, Set<d.f>> a = new LinkedHashMap();
  }
}

/* Location:
 * Qualified Name:     com.d.a.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */