package com.d.a;

import com.d.a.a.j;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLSocket;

public final class k
{
  public static final k a = new a(true).a(h).a(new aa[] { aa.a, aa.b, aa.c }).a().b();
  public static final k b = new a(a).a(new aa[] { aa.c }).a().b();
  public static final k c = new a(false).b();
  private static final h[] h = { h.aK, h.aO, h.W, h.am, h.al, h.av, h.aw, h.F, h.J, h.U, h.D, h.H, h.h };
  public final boolean d;
  public final boolean e;
  final String[] f;
  final String[] g;
  
  private k(a parama)
  {
    d = a;
    f = b;
    g = c;
    e = d;
  }
  
  private static boolean a(String[] paramArrayOfString1, String[] paramArrayOfString2)
  {
    if ((paramArrayOfString1 != null) && (paramArrayOfString2 != null) && (paramArrayOfString1.length != 0))
    {
      if (paramArrayOfString2.length == 0) {
        return false;
      }
      int j = paramArrayOfString1.length;
      int i = 0;
      while (i < j)
      {
        if (j.a(paramArrayOfString2, paramArrayOfString1[i])) {
          return true;
        }
        i += 1;
      }
      return false;
    }
    return false;
  }
  
  public final List<h> a()
  {
    Object localObject = f;
    if (localObject == null) {
      return null;
    }
    localObject = new h[localObject.length];
    int i = 0;
    for (;;)
    {
      String[] arrayOfString = f;
      if (i >= arrayOfString.length) {
        break;
      }
      localObject[i] = h.a(arrayOfString[i]);
      i += 1;
    }
    return j.a((Object[])localObject);
  }
  
  public final boolean a(SSLSocket paramSSLSocket)
  {
    if (!d) {
      return false;
    }
    String[] arrayOfString = g;
    if ((arrayOfString != null) && (!a(arrayOfString, paramSSLSocket.getEnabledProtocols()))) {
      return false;
    }
    arrayOfString = f;
    return (arrayOfString == null) || (a(arrayOfString, paramSSLSocket.getEnabledCipherSuites()));
  }
  
  public final List<aa> b()
  {
    Object localObject = g;
    if (localObject == null) {
      return null;
    }
    localObject = new aa[localObject.length];
    int i = 0;
    for (;;)
    {
      String[] arrayOfString = g;
      if (i >= arrayOfString.length) {
        break;
      }
      localObject[i] = aa.a(arrayOfString[i]);
      i += 1;
    }
    return j.a((Object[])localObject);
  }
  
  public final boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof k)) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    paramObject = (k)paramObject;
    boolean bool = d;
    if (bool != d) {
      return false;
    }
    if (bool)
    {
      if (!Arrays.equals(f, f)) {
        return false;
      }
      if (!Arrays.equals(g, g)) {
        return false;
      }
      if (e != e) {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    if (d) {
      return ((Arrays.hashCode(f) + 527) * 31 + Arrays.hashCode(g)) * 31 + (e ^ true);
    }
    return 17;
  }
  
  public final String toString()
  {
    if (!d) {
      return "ConnectionSpec()";
    }
    String str1;
    if (f != null) {
      str1 = a().toString();
    } else {
      str1 = "[all enabled]";
    }
    String str2;
    if (g != null) {
      str2 = b().toString();
    } else {
      str2 = "[all enabled]";
    }
    StringBuilder localStringBuilder = new StringBuilder("ConnectionSpec(cipherSuites=");
    localStringBuilder.append(str1);
    localStringBuilder.append(", tlsVersions=");
    localStringBuilder.append(str2);
    localStringBuilder.append(", supportsTlsExtensions=");
    localStringBuilder.append(e);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public static final class a
  {
    boolean a;
    String[] b;
    String[] c;
    boolean d;
    
    public a(k paramk)
    {
      a = k.a(paramk);
      b = k.b(paramk);
      c = k.c(paramk);
      d = k.d(paramk);
    }
    
    a(boolean paramBoolean)
    {
      a = paramBoolean;
    }
    
    public final a a()
    {
      if (a)
      {
        d = true;
        return this;
      }
      throw new IllegalStateException("no TLS extensions for cleartext connections");
    }
    
    public final a a(aa... paramVarArgs)
    {
      if (a)
      {
        String[] arrayOfString = new String[paramVarArgs.length];
        int i = 0;
        while (i < paramVarArgs.length)
        {
          arrayOfString[i] = e;
          i += 1;
        }
        return b(arrayOfString);
      }
      throw new IllegalStateException("no TLS versions for cleartext connections");
    }
    
    public final a a(h... paramVarArgs)
    {
      if (a)
      {
        String[] arrayOfString = new String[paramVarArgs.length];
        int i = 0;
        while (i < paramVarArgs.length)
        {
          arrayOfString[i] = aS;
          i += 1;
        }
        return a(arrayOfString);
      }
      throw new IllegalStateException("no cipher suites for cleartext connections");
    }
    
    public final a a(String... paramVarArgs)
    {
      if (a)
      {
        if (paramVarArgs.length != 0)
        {
          b = ((String[])paramVarArgs.clone());
          return this;
        }
        throw new IllegalArgumentException("At least one cipher suite is required");
      }
      throw new IllegalStateException("no cipher suites for cleartext connections");
    }
    
    public final a b(String... paramVarArgs)
    {
      if (a)
      {
        if (paramVarArgs.length != 0)
        {
          c = ((String[])paramVarArgs.clone());
          return this;
        }
        throw new IllegalArgumentException("At least one TLS version is required");
      }
      throw new IllegalStateException("no TLS versions for cleartext connections");
    }
    
    public final k b()
    {
      return new k(this, (byte)0);
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */