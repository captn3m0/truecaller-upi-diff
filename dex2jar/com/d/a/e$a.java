package com.d.a;

import java.io.IOException;
import java.util.List;

final class e$a
  implements r.a
{
  private final int b;
  private final v c;
  private final boolean d;
  
  e$a(e parame, int paramInt, v paramv, boolean paramBoolean)
  {
    b = paramInt;
    c = paramv;
    d = paramBoolean;
  }
  
  public final x a(v paramv)
    throws IOException
  {
    if (b < a.a.g.size())
    {
      new a(a, b + 1, paramv, d);
      paramv = (r)a.a.g.get(b);
      Object localObject = paramv.a();
      if (localObject != null) {
        return (x)localObject;
      }
      localObject = new StringBuilder("application interceptor ");
      ((StringBuilder)localObject).append(paramv);
      ((StringBuilder)localObject).append(" returned null");
      throw new NullPointerException(((StringBuilder)localObject).toString());
    }
    return a.a(paramv, d);
  }
}

/* Location:
 * Qualified Name:     com.d.a.e.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */