package com.d.a;

import com.d.a.a.b.a;
import com.d.a.a.b.k;
import com.d.a.a.b.r;
import com.d.a.a.j;
import d.d;
import d.e;
import d.f;
import d.n;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final class c$c
{
  final String a;
  final p b;
  final String c;
  final u d;
  final int e;
  final String f;
  final p g;
  final o h;
  
  public c$c(x paramx)
  {
    a = a.a.toString();
    b = k.c(paramx);
    c = a.b;
    d = b;
    e = c;
    f = d;
    g = f;
    h = e;
  }
  
  public c$c(d.u paramu)
    throws IOException
  {
    try
    {
      Object localObject1 = n.a(paramu);
      a = ((e)localObject1).q();
      c = ((e)localObject1).q();
      Object localObject3 = new p.a();
      int k = c.a((e)localObject1);
      int j = 0;
      int i = 0;
      while (i < k)
      {
        ((p.a)localObject3).a(((e)localObject1).q());
        i += 1;
      }
      b = ((p.a)localObject3).a();
      localObject3 = r.a(((e)localObject1).q());
      d = a;
      e = b;
      f = c;
      localObject3 = new p.a();
      k = c.a((e)localObject1);
      i = j;
      while (i < k)
      {
        ((p.a)localObject3).a(((e)localObject1).q());
        i += 1;
      }
      g = ((p.a)localObject3).a();
      if (a())
      {
        localObject3 = ((e)localObject1).q();
        if (((String)localObject3).length() <= 0)
        {
          localObject3 = ((e)localObject1).q();
          List localList = a((e)localObject1);
          localObject1 = a((e)localObject1);
          if (localObject3 != null) {
            h = new o((String)localObject3, j.a(localList), j.a((List)localObject1));
          } else {
            throw new IllegalArgumentException("cipherSuite == null");
          }
        }
        else
        {
          localObject1 = new StringBuilder("expected \"\" but was \"");
          ((StringBuilder)localObject1).append((String)localObject3);
          ((StringBuilder)localObject1).append("\"");
          throw new IOException(((StringBuilder)localObject1).toString());
        }
      }
      else
      {
        h = null;
      }
      return;
    }
    finally
    {
      paramu.close();
    }
  }
  
  private static List<Certificate> a(e parame)
    throws IOException
  {
    int j = c.a(parame);
    if (j == -1) {
      return Collections.emptyList();
    }
    try
    {
      CertificateFactory localCertificateFactory = CertificateFactory.getInstance("X.509");
      ArrayList localArrayList = new ArrayList(j);
      int i = 0;
      while (i < j)
      {
        String str = parame.q();
        d.c localc = new d.c();
        localc.a(f.b(str));
        localArrayList.add(localCertificateFactory.generateCertificate(localc.f()));
        i += 1;
      }
      return localArrayList;
    }
    catch (CertificateException parame)
    {
      throw new IOException(parame.getMessage());
    }
  }
  
  private static void a(d paramd, List<Certificate> paramList)
    throws IOException
  {
    try
    {
      paramd.l(paramList.size());
      paramd.j(10);
      int i = 0;
      int j = paramList.size();
      while (i < j)
      {
        paramd.b(f.a(((Certificate)paramList.get(i)).getEncoded()).b());
        paramd.j(10);
        i += 1;
      }
      return;
    }
    catch (CertificateEncodingException paramd)
    {
      throw new IOException(paramd.getMessage());
    }
  }
  
  private boolean a()
  {
    return a.startsWith("https://");
  }
  
  public final void a(b.a parama)
    throws IOException
  {
    int j = 0;
    parama = n.a(parama.a(0));
    parama.b(a);
    parama.j(10);
    parama.b(c);
    parama.j(10);
    parama.l(b.a.length / 2);
    parama.j(10);
    int k = b.a.length / 2;
    int i = 0;
    while (i < k)
    {
      parama.b(b.a(i));
      parama.b(": ");
      parama.b(b.b(i));
      parama.j(10);
      i += 1;
    }
    parama.b(new r(d, e, f).toString());
    parama.j(10);
    parama.l(g.a.length / 2);
    parama.j(10);
    k = g.a.length / 2;
    i = j;
    while (i < k)
    {
      parama.b(g.a(i));
      parama.b(": ");
      parama.b(g.b(i));
      parama.j(10);
      i += 1;
    }
    if (a())
    {
      parama.j(10);
      parama.b(h.a);
      parama.j(10);
      a(parama, h.b);
      a(parama, h.c);
    }
    parama.close();
  }
}

/* Location:
 * Qualified Name:     com.d.a.c.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */