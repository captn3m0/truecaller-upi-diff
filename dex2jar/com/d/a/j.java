package com.d.a;

import com.d.a.a.c.b;
import com.d.a.a.d;
import com.d.a.a.i;
import java.lang.ref.Reference;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public final class j
{
  private static final j g;
  final Executor a = new ThreadPoolExecutor(0, 1, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue(), com.d.a.a.j.b("OkHttp ConnectionPool"));
  final int b;
  Runnable c = new Runnable()
  {
    public final void run()
    {
      for (;;)
      {
        long l1 = a(System.nanoTime());
        if (l1 == -1L) {
          return;
        }
        long l2;
        if (l1 > 0L) {
          l2 = l1 / 1000000L;
        }
        try
        {
          synchronized (j.this)
          {
            wait(l2, (int)(l1 - 1000000L * l2));
          }
        }
        catch (InterruptedException localInterruptedException)
        {
          for (;;) {}
        }
      }
      throw ((Throwable)localObject);
    }
  };
  final Deque<b> d = new ArrayDeque();
  final i e = new i();
  private final long h;
  
  static
  {
    String str1 = System.getProperty("http.keepAlive");
    String str2 = System.getProperty("http.keepAliveDuration");
    String str3 = System.getProperty("http.maxConnections");
    long l;
    if (str2 != null) {
      l = Long.parseLong(str2);
    } else {
      l = 300000L;
    }
    if ((str1 != null) && (!Boolean.parseBoolean(str1)))
    {
      g = new j(0, l);
      return;
    }
    if (str3 != null)
    {
      g = new j(Integer.parseInt(str3), l);
      return;
    }
    g = new j(5, l);
  }
  
  private j(int paramInt, long paramLong)
  {
    this(paramInt, paramLong, TimeUnit.MILLISECONDS);
  }
  
  private j(int paramInt, long paramLong, TimeUnit paramTimeUnit)
  {
    b = paramInt;
    h = paramTimeUnit.toNanos(paramLong);
    if (paramLong > 0L) {
      return;
    }
    throw new IllegalArgumentException("keepAliveDuration <= 0: ".concat(String.valueOf(paramLong)));
  }
  
  public static j a()
  {
    return g;
  }
  
  final long a(long paramLong)
  {
    for (;;)
    {
      int j;
      int k;
      try
      {
        Iterator localIterator = d.iterator();
        long l1 = Long.MIN_VALUE;
        Object localObject1 = null;
        int i = 0;
        j = 0;
        if (localIterator.hasNext())
        {
          b localb = (b)localIterator.next();
          List localList = h;
          k = 0;
          if (k < localList.size())
          {
            if (((Reference)localList.get(k)).get() != null)
            {
              k += 1;
              continue;
            }
            Logger localLogger = d.a;
            StringBuilder localStringBuilder = new StringBuilder("A connection to ");
            localStringBuilder.append(a.a.a);
            localStringBuilder.append(" was leaked. Did you forget to close a response body?");
            localLogger.warning(localStringBuilder.toString());
            localList.remove(k);
            i = true;
            if (!localList.isEmpty()) {
              continue;
            }
            j = (paramLong - h);
            k = 0;
          }
          else
          {
            k = localList.size();
            break label330;
            k = i + 1;
            long l2 = paramLong - j;
            i = k;
            if (l2 <= l1) {
              continue;
            }
            localObject1 = localb;
            l1 = l2;
            i = k;
          }
        }
        else
        {
          if ((l1 < h) && (i <= b))
          {
            if (i > 0)
            {
              paramLong = h;
              return paramLong - l1;
            }
            if (j > 0)
            {
              paramLong = h;
              return paramLong;
            }
            return -1L;
          }
          d.remove(localObject1);
          com.d.a.a.j.a(b);
          return 0L;
        }
      }
      finally {}
      label330:
      if (k > 0) {
        j += 1;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */