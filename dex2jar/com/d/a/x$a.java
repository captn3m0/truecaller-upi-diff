package com.d.a;

public final class x$a
{
  public v a;
  public u b;
  public int c = -1;
  public String d;
  public o e;
  p.a f;
  public y g;
  x h;
  x i;
  x j;
  
  public x$a()
  {
    f = new p.a();
  }
  
  private x$a(x paramx)
  {
    a = a;
    b = b;
    c = c;
    d = d;
    e = e;
    f = f.a();
    g = g;
    h = h;
    i = i;
    j = j;
  }
  
  private static void a(String paramString, x paramx)
  {
    if (g == null)
    {
      if (h == null)
      {
        if (i == null)
        {
          if (j == null) {
            return;
          }
          paramx = new StringBuilder();
          paramx.append(paramString);
          paramx.append(".priorResponse != null");
          throw new IllegalArgumentException(paramx.toString());
        }
        paramx = new StringBuilder();
        paramx.append(paramString);
        paramx.append(".cacheResponse != null");
        throw new IllegalArgumentException(paramx.toString());
      }
      paramx = new StringBuilder();
      paramx.append(paramString);
      paramx.append(".networkResponse != null");
      throw new IllegalArgumentException(paramx.toString());
    }
    paramx = new StringBuilder();
    paramx.append(paramString);
    paramx.append(".body != null");
    throw new IllegalArgumentException(paramx.toString());
  }
  
  private static void d(x paramx)
  {
    if (g == null) {
      return;
    }
    throw new IllegalArgumentException("priorResponse.body != null");
  }
  
  public final a a(p paramp)
  {
    f = paramp.a();
    return this;
  }
  
  public final a a(x paramx)
  {
    if (paramx != null) {
      a("networkResponse", paramx);
    }
    h = paramx;
    return this;
  }
  
  public final a a(String paramString1, String paramString2)
  {
    f.b(paramString1, paramString2);
    return this;
  }
  
  public final x a()
  {
    if (a != null)
    {
      if (b != null)
      {
        if (c >= 0) {
          return new x(this, (byte)0);
        }
        StringBuilder localStringBuilder = new StringBuilder("code < 0: ");
        localStringBuilder.append(c);
        throw new IllegalStateException(localStringBuilder.toString());
      }
      throw new IllegalStateException("protocol == null");
    }
    throw new IllegalStateException("request == null");
  }
  
  public final a b(x paramx)
  {
    if (paramx != null) {
      a("cacheResponse", paramx);
    }
    i = paramx;
    return this;
  }
  
  public final a b(String paramString1, String paramString2)
  {
    f.a(paramString1, paramString2);
    return this;
  }
  
  public final a c(x paramx)
  {
    if (paramx != null) {
      d(paramx);
    }
    j = paramx;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.d.a.x.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */