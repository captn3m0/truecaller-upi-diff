package com.d.a;

import com.d.a.a.j;

public final class g
{
  public final String a;
  public final String b;
  
  public g(String paramString1, String paramString2)
  {
    a = paramString1;
    b = paramString2;
  }
  
  public final boolean equals(Object paramObject)
  {
    if ((paramObject instanceof g))
    {
      String str = a;
      paramObject = (g)paramObject;
      if ((j.a(str, a)) && (j.a(b, b))) {
        return true;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    String str = b;
    int j = 0;
    int i;
    if (str != null) {
      i = str.hashCode();
    } else {
      i = 0;
    }
    str = a;
    if (str != null) {
      j = str.hashCode();
    }
    return (i + 899) * 31 + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(a);
    localStringBuilder.append(" realm=\"");
    localStringBuilder.append(b);
    localStringBuilder.append("\"");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.d.a.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */