package com.d.a;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

final class n$1
  implements n
{
  public final List<InetAddress> a(String paramString)
    throws UnknownHostException
  {
    if (paramString != null) {
      return Arrays.asList(InetAddress.getAllByName(paramString));
    }
    throw new UnknownHostException("hostname == null");
  }
}

/* Location:
 * Qualified Name:     com.d.a.n.1
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */