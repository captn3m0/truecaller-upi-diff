package com.d.a;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

public abstract interface n
{
  public static final n a = new n()
  {
    public final List<InetAddress> a(String paramAnonymousString)
      throws UnknownHostException
    {
      if (paramAnonymousString != null) {
        return Arrays.asList(InetAddress.getAllByName(paramAnonymousString));
      }
      throw new UnknownHostException("hostname == null");
    }
  };
  
  public abstract List<InetAddress> a(String paramString)
    throws UnknownHostException;
}

/* Location:
 * Qualified Name:     com.d.a.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */