package com.d.a;

import com.d.a.a.b.c;
import d.e;
import d.i;
import d.n;
import d.u;
import java.io.IOException;

final class c$b
  extends y
{
  private final b.c a;
  private final e b;
  private final String c;
  private final String d;
  
  public c$b(final b.c paramc, String paramString1, String paramString2)
  {
    a = paramc;
    c = paramString1;
    d = paramString2;
    b = n.a(new i(c[1])
    {
      public final void close()
        throws IOException
      {
        paramc.close();
        super.close();
      }
    });
  }
  
  public final long a()
  {
    long l = -1L;
    try
    {
      if (d != null) {
        l = Long.parseLong(d);
      }
      return l;
    }
    catch (NumberFormatException localNumberFormatException) {}
    return -1L;
  }
  
  public final e b()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     com.d.a.c.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */