package com.d.a;

import com.d.a.a.b.g;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public final class p
{
  public final String[] a;
  
  private p(a parama)
  {
    a = ((String[])a.toArray(new String[a.size()]));
  }
  
  private static String a(String[] paramArrayOfString, String paramString)
  {
    int i = paramArrayOfString.length - 2;
    while (i >= 0)
    {
      if (paramString.equalsIgnoreCase(paramArrayOfString[i])) {
        return paramArrayOfString[(i + 1)];
      }
      i -= 2;
    }
    return null;
  }
  
  public final a a()
  {
    a locala = new a();
    Collections.addAll(a, a);
    return locala;
  }
  
  public final String a(int paramInt)
  {
    paramInt *= 2;
    if (paramInt >= 0)
    {
      String[] arrayOfString = a;
      if (paramInt < arrayOfString.length) {
        return arrayOfString[paramInt];
      }
    }
    return null;
  }
  
  public final String a(String paramString)
  {
    return a(a, paramString);
  }
  
  public final String b(int paramInt)
  {
    paramInt = paramInt * 2 + 1;
    if (paramInt >= 0)
    {
      String[] arrayOfString = a;
      if (paramInt < arrayOfString.length) {
        return arrayOfString[paramInt];
      }
    }
    return null;
  }
  
  public final Date b(String paramString)
  {
    paramString = a(paramString);
    if (paramString != null) {
      return g.a(paramString);
    }
    return null;
  }
  
  public final List<String> c(String paramString)
  {
    int j = a.length / 2;
    Object localObject1 = null;
    int i = 0;
    while (i < j)
    {
      Object localObject2 = localObject1;
      if (paramString.equalsIgnoreCase(a(i)))
      {
        localObject2 = localObject1;
        if (localObject1 == null) {
          localObject2 = new ArrayList(2);
        }
        ((List)localObject2).add(b(i));
      }
      i += 1;
      localObject1 = localObject2;
    }
    if (localObject1 != null) {
      return Collections.unmodifiableList((List)localObject1);
    }
    return Collections.emptyList();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    int j = a.length / 2;
    int i = 0;
    while (i < j)
    {
      localStringBuilder.append(a(i));
      localStringBuilder.append(": ");
      localStringBuilder.append(b(i));
      localStringBuilder.append("\n");
      i += 1;
    }
    return localStringBuilder.toString();
  }
  
  public static final class a
  {
    final List<String> a = new ArrayList(20);
    
    private a c(String paramString1, String paramString2)
    {
      a.add(paramString1);
      a.add(paramString2.trim());
      return this;
    }
    
    private static void d(String paramString1, String paramString2)
    {
      if (paramString1 != null)
      {
        if (!paramString1.isEmpty())
        {
          int j = paramString1.length();
          int i = 0;
          int k;
          while (i < j)
          {
            k = paramString1.charAt(i);
            if ((k > 31) && (k < 127)) {
              i += 1;
            } else {
              throw new IllegalArgumentException(String.format("Unexpected char %#04x at %d in header name: %s", new Object[] { Integer.valueOf(k), Integer.valueOf(i), paramString1 }));
            }
          }
          if (paramString2 != null)
          {
            j = paramString2.length();
            i = 0;
            while (i < j)
            {
              k = paramString2.charAt(i);
              if ((k > 31) && (k < 127)) {
                i += 1;
              } else {
                throw new IllegalArgumentException(String.format("Unexpected char %#04x at %d in header value: %s", new Object[] { Integer.valueOf(k), Integer.valueOf(i), paramString2 }));
              }
            }
            return;
          }
          throw new IllegalArgumentException("value == null");
        }
        throw new IllegalArgumentException("name is empty");
      }
      throw new IllegalArgumentException("name == null");
    }
    
    final a a(String paramString)
    {
      int i = paramString.indexOf(":", 1);
      if (i != -1) {
        return c(paramString.substring(0, i), paramString.substring(i + 1));
      }
      if (paramString.startsWith(":")) {
        return c("", paramString.substring(1));
      }
      return c("", paramString);
    }
    
    public final a a(String paramString1, String paramString2)
    {
      d(paramString1, paramString2);
      return c(paramString1, paramString2);
    }
    
    public final p a()
    {
      return new p(this, (byte)0);
    }
    
    public final a b(String paramString)
    {
      int j;
      for (int i = 0; i < a.size(); i = j + 2)
      {
        j = i;
        if (paramString.equalsIgnoreCase((String)a.get(i)))
        {
          a.remove(i);
          a.remove(i);
          j = i - 2;
        }
      }
      return this;
    }
    
    public final a b(String paramString1, String paramString2)
    {
      d(paramString1, paramString2);
      b(paramString1);
      c(paramString1, paramString2);
      return this;
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */