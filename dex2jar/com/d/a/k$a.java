package com.d.a;

public final class k$a
{
  boolean a;
  String[] b;
  String[] c;
  boolean d;
  
  public k$a(k paramk)
  {
    a = k.a(paramk);
    b = k.b(paramk);
    c = k.c(paramk);
    d = k.d(paramk);
  }
  
  k$a(boolean paramBoolean)
  {
    a = paramBoolean;
  }
  
  public final a a()
  {
    if (a)
    {
      d = true;
      return this;
    }
    throw new IllegalStateException("no TLS extensions for cleartext connections");
  }
  
  public final a a(aa... paramVarArgs)
  {
    if (a)
    {
      String[] arrayOfString = new String[paramVarArgs.length];
      int i = 0;
      while (i < paramVarArgs.length)
      {
        arrayOfString[i] = e;
        i += 1;
      }
      return b(arrayOfString);
    }
    throw new IllegalStateException("no TLS versions for cleartext connections");
  }
  
  public final a a(h... paramVarArgs)
  {
    if (a)
    {
      String[] arrayOfString = new String[paramVarArgs.length];
      int i = 0;
      while (i < paramVarArgs.length)
      {
        arrayOfString[i] = aS;
        i += 1;
      }
      return a(arrayOfString);
    }
    throw new IllegalStateException("no cipher suites for cleartext connections");
  }
  
  public final a a(String... paramVarArgs)
  {
    if (a)
    {
      if (paramVarArgs.length != 0)
      {
        b = ((String[])paramVarArgs.clone());
        return this;
      }
      throw new IllegalArgumentException("At least one cipher suite is required");
    }
    throw new IllegalStateException("no cipher suites for cleartext connections");
  }
  
  public final a b(String... paramVarArgs)
  {
    if (a)
    {
      if (paramVarArgs.length != 0)
      {
        c = ((String[])paramVarArgs.clone());
        return this;
      }
      throw new IllegalArgumentException("At least one TLS version is required");
    }
    throw new IllegalStateException("no TLS versions for cleartext connections");
  }
  
  public final k b()
  {
    return new k(this, (byte)0);
  }
}

/* Location:
 * Qualified Name:     com.d.a.k.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */