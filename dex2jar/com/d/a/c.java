package com.d.a;

import com.d.a.a.b.a;
import com.d.a.a.b.c;
import com.d.a.a.b.k;
import com.d.a.a.b.r;
import com.d.a.a.c.a;
import com.d.a.a.j;
import d.d;
import d.f;
import d.h;
import d.n;
import d.t;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class c
{
  final com.d.a.a.e a = new com.d.a.a.e()
  {
    public final com.d.a.a.b.b a(x paramAnonymousx)
      throws IOException
    {
      return c.this.a(paramAnonymousx);
    }
    
    public final x a(v paramAnonymousv)
      throws IOException
    {
      return c.this.a(paramAnonymousv);
    }
    
    public final void a()
    {
      b();
    }
    
    public final void a(com.d.a.a.b.c paramAnonymousc)
    {
      c.this.a(paramAnonymousc);
    }
    
    public final void a(x paramAnonymousx1, x paramAnonymousx2)
      throws IOException
    {
      paramAnonymousx2 = new c.c(paramAnonymousx2);
      paramAnonymousx1 = c.b.a((c.b)g);
      try
      {
        paramAnonymousx1 = com.d.a.a.b.a(d, a, b);
        if (paramAnonymousx1 == null) {}
      }
      catch (IOException paramAnonymousx1)
      {
        label52:
        for (;;) {}
      }
      try
      {
        paramAnonymousx2.a(paramAnonymousx1);
        paramAnonymousx1.a();
        return;
      }
      catch (IOException paramAnonymousx2)
      {
        break label52;
      }
      paramAnonymousx1 = null;
      c.a(paramAnonymousx1);
    }
    
    public final void b(v paramAnonymousv)
      throws IOException
    {
      c.this.b(paramAnonymousv);
    }
  };
  int b;
  int c;
  private final com.d.a.a.b d;
  private int e;
  private int f;
  private int g;
  
  public c(File paramFile, long paramLong)
  {
    this(paramFile, paramLong, a.a);
  }
  
  private c(File paramFile, long paramLong, a parama)
  {
    d = com.d.a.a.b.a(parama, paramFile, paramLong);
  }
  
  static int a(d.e parame)
    throws IOException
  {
    try
    {
      long l = parame.m();
      parame = parame.q();
      if ((l >= 0L) && (l <= 2147483647L) && (parame.isEmpty())) {
        return (int)l;
      }
      StringBuilder localStringBuilder = new StringBuilder("expected an int but was \"");
      localStringBuilder.append(l);
      localStringBuilder.append(parame);
      localStringBuilder.append("\"");
      throw new IOException(localStringBuilder.toString());
    }
    catch (NumberFormatException parame)
    {
      throw new IOException(parame.getMessage());
    }
  }
  
  static void a(b.a parama)
  {
    if (parama != null) {}
    try
    {
      parama.b();
      return;
    }
    catch (IOException parama) {}
    return;
    return;
  }
  
  private static String c(v paramv)
  {
    return j.a(a.toString());
  }
  
  final com.d.a.a.b.b a(x paramx)
    throws IOException
  {
    Object localObject = a.b;
    if (com.d.a.a.b.i.a(a.b)) {}
    try
    {
      b(a);
      return null;
    }
    catch (IOException paramx)
    {
      return null;
    }
    if (!((String)localObject).equals("GET")) {
      return null;
    }
    if (k.b(paramx)) {
      return null;
    }
    localObject = new c(paramx);
    try
    {
      paramx = d.a(c(a), -1L);
      if (paramx == null) {
        return null;
      }
    }
    catch (IOException paramx)
    {
      for (;;) {}
    }
    try
    {
      ((c)localObject).a(paramx);
      localObject = new a(paramx);
      return (com.d.a.a.b.b)localObject;
    }
    catch (IOException localIOException)
    {
      break label103;
    }
    paramx = null;
    label103:
    a(paramx);
    return null;
  }
  
  final x a(v paramv)
  {
    Object localObject1 = c(paramv);
    for (;;)
    {
      try
      {
        localObject1 = d.a((String)localObject1);
        if (localObject1 == null) {
          return null;
        }
      }
      catch (IOException paramv)
      {
        Object localObject2;
        int j;
        String str1;
        String str2;
        Object localObject3;
        x.a locala;
        int i;
        return null;
      }
      try
      {
        localObject2 = c;
        j = 0;
        localObject2 = new c(localObject2[0]);
        str1 = g.a("Content-Type");
        str2 = g.a("Content-Length");
        localObject3 = new v.a().a(a).c(c);
        c = b.a();
        localObject3 = ((v.a)localObject3).a();
        locala = new x.a();
        a = ((v)localObject3);
        b = d;
        c = e;
        d = f;
        localObject3 = locala.a(g);
        g = new b((b.c)localObject1, str1, str2);
        e = h;
        localObject1 = ((x.a)localObject3).a();
        i = j;
        if (a.equals(a.toString()))
        {
          i = j;
          if (c.equals(b))
          {
            i = j;
            if (k.a((x)localObject1, b, paramv)) {
              i = 1;
            }
          }
        }
        if (i == 0)
        {
          j.a(g);
          return null;
        }
        return (x)localObject1;
      }
      catch (IOException paramv) {}
    }
    j.a((Closeable)localObject1);
    return null;
  }
  
  public final void a()
    throws IOException
  {
    d.close();
  }
  
  final void a(com.d.a.a.b.c paramc)
  {
    try
    {
      g += 1;
      if (a != null)
      {
        e += 1;
        return;
      }
      if (b != null) {
        f += 1;
      }
      return;
    }
    finally {}
  }
  
  final void b()
  {
    try
    {
      f += 1;
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  final void b(v paramv)
    throws IOException
  {
    d.b(c(paramv));
  }
  
  final class a
    implements com.d.a.a.b.b
  {
    boolean a;
    private final b.a c;
    private t d;
    private t e;
    
    public a(final b.a parama)
      throws IOException
    {
      c = parama;
      d = parama.a(1);
      e = new h(d)
      {
        public final void close()
          throws IOException
        {
          synchronized (c.this)
          {
            if (a) {
              return;
            }
            a = true;
            c localc2 = c.this;
            b += 1;
            super.close();
            parama.a();
            return;
          }
        }
      };
    }
    
    public final void a()
    {
      synchronized (c.this)
      {
        if (a) {
          return;
        }
        a = true;
        c localc2 = c.this;
        c += 1;
        j.a(d);
      }
    }
    
    public final t b()
    {
      return e;
    }
  }
  
  static final class b
    extends y
  {
    private final b.c a;
    private final d.e b;
    private final String c;
    private final String d;
    
    public b(final b.c paramc, String paramString1, String paramString2)
    {
      a = paramc;
      c = paramString1;
      d = paramString2;
      b = n.a(new d.i(c[1])
      {
        public final void close()
          throws IOException
        {
          paramc.close();
          super.close();
        }
      });
    }
    
    public final long a()
    {
      long l = -1L;
      try
      {
        if (d != null) {
          l = Long.parseLong(d);
        }
        return l;
      }
      catch (NumberFormatException localNumberFormatException) {}
      return -1L;
    }
    
    public final d.e b()
    {
      return b;
    }
  }
  
  static final class c
  {
    final String a;
    final p b;
    final String c;
    final u d;
    final int e;
    final String f;
    final p g;
    final o h;
    
    public c(x paramx)
    {
      a = a.a.toString();
      b = k.c(paramx);
      c = a.b;
      d = b;
      e = c;
      f = d;
      g = f;
      h = e;
    }
    
    public c(d.u paramu)
      throws IOException
    {
      try
      {
        Object localObject1 = n.a(paramu);
        a = ((d.e)localObject1).q();
        c = ((d.e)localObject1).q();
        Object localObject3 = new p.a();
        int k = c.a((d.e)localObject1);
        int j = 0;
        int i = 0;
        while (i < k)
        {
          ((p.a)localObject3).a(((d.e)localObject1).q());
          i += 1;
        }
        b = ((p.a)localObject3).a();
        localObject3 = r.a(((d.e)localObject1).q());
        d = a;
        e = b;
        f = c;
        localObject3 = new p.a();
        k = c.a((d.e)localObject1);
        i = j;
        while (i < k)
        {
          ((p.a)localObject3).a(((d.e)localObject1).q());
          i += 1;
        }
        g = ((p.a)localObject3).a();
        if (a())
        {
          localObject3 = ((d.e)localObject1).q();
          if (((String)localObject3).length() <= 0)
          {
            localObject3 = ((d.e)localObject1).q();
            List localList = a((d.e)localObject1);
            localObject1 = a((d.e)localObject1);
            if (localObject3 != null) {
              h = new o((String)localObject3, j.a(localList), j.a((List)localObject1));
            } else {
              throw new IllegalArgumentException("cipherSuite == null");
            }
          }
          else
          {
            localObject1 = new StringBuilder("expected \"\" but was \"");
            ((StringBuilder)localObject1).append((String)localObject3);
            ((StringBuilder)localObject1).append("\"");
            throw new IOException(((StringBuilder)localObject1).toString());
          }
        }
        else
        {
          h = null;
        }
        return;
      }
      finally
      {
        paramu.close();
      }
    }
    
    private static List<Certificate> a(d.e parame)
      throws IOException
    {
      int j = c.a(parame);
      if (j == -1) {
        return Collections.emptyList();
      }
      try
      {
        CertificateFactory localCertificateFactory = CertificateFactory.getInstance("X.509");
        ArrayList localArrayList = new ArrayList(j);
        int i = 0;
        while (i < j)
        {
          String str = parame.q();
          d.c localc = new d.c();
          localc.a(f.b(str));
          localArrayList.add(localCertificateFactory.generateCertificate(localc.f()));
          i += 1;
        }
        return localArrayList;
      }
      catch (CertificateException parame)
      {
        throw new IOException(parame.getMessage());
      }
    }
    
    private static void a(d paramd, List<Certificate> paramList)
      throws IOException
    {
      try
      {
        paramd.l(paramList.size());
        paramd.j(10);
        int i = 0;
        int j = paramList.size();
        while (i < j)
        {
          paramd.b(f.a(((Certificate)paramList.get(i)).getEncoded()).b());
          paramd.j(10);
          i += 1;
        }
        return;
      }
      catch (CertificateEncodingException paramd)
      {
        throw new IOException(paramd.getMessage());
      }
    }
    
    private boolean a()
    {
      return a.startsWith("https://");
    }
    
    public final void a(b.a parama)
      throws IOException
    {
      int j = 0;
      parama = n.a(parama.a(0));
      parama.b(a);
      parama.j(10);
      parama.b(c);
      parama.j(10);
      parama.l(b.a.length / 2);
      parama.j(10);
      int k = b.a.length / 2;
      int i = 0;
      while (i < k)
      {
        parama.b(b.a(i));
        parama.b(": ");
        parama.b(b.b(i));
        parama.j(10);
        i += 1;
      }
      parama.b(new r(d, e, f).toString());
      parama.j(10);
      parama.l(g.a.length / 2);
      parama.j(10);
      k = g.a.length / 2;
      i = j;
      while (i < k)
      {
        parama.b(g.a(i));
        parama.b(": ");
        parama.b(g.b(i));
        parama.j(10);
        i += 1;
      }
      if (a())
      {
        parama.j(10);
        parama.b(h.a);
        parama.j(10);
        a(parama, h.b);
        a(parama, h.c);
      }
      parama.close();
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */