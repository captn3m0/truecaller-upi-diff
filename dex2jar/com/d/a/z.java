package com.d.a;

import java.net.InetSocketAddress;
import java.net.Proxy;

public final class z
{
  public final a a;
  public final Proxy b;
  public final InetSocketAddress c;
  
  public z(a parama, Proxy paramProxy, InetSocketAddress paramInetSocketAddress)
  {
    if (parama != null)
    {
      if (paramProxy != null)
      {
        if (paramInetSocketAddress != null)
        {
          a = parama;
          b = paramProxy;
          c = paramInetSocketAddress;
          return;
        }
        throw new NullPointerException("inetSocketAddress == null");
      }
      throw new NullPointerException("proxy == null");
    }
    throw new NullPointerException("address == null");
  }
  
  public final boolean equals(Object paramObject)
  {
    if ((paramObject instanceof z))
    {
      paramObject = (z)paramObject;
      return (a.equals(a)) && (b.equals(b)) && (c.equals(c));
    }
    return false;
  }
  
  public final int hashCode()
  {
    return ((a.hashCode() + 527) * 31 + b.hashCode()) * 31 + c.hashCode();
  }
}

/* Location:
 * Qualified Name:     com.d.a.z
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */