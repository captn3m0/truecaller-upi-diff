package com.d.a;

import com.d.a.a.b.s;
import com.d.a.a.c.b;
import com.d.a.a.e;
import com.d.a.a.i;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import javax.net.ssl.SSLSocket;

final class t$1
  extends com.d.a.a.d
{
  public final b a(j paramj, a parama, s params)
  {
    if ((!j.f) && (!Thread.holdsLock(paramj))) {
      throw new AssertionError();
    }
    paramj = d.iterator();
    while (paramj.hasNext())
    {
      b localb = (b)paramj.next();
      int j = h.size();
      com.d.a.a.a.d locald = d;
      int i;
      if (locald != null) {
        i = locald.a();
      } else {
        i = 1;
      }
      if ((j < i) && (parama.equals(a.a)) && (!i))
      {
        params.a(localb);
        return localb;
      }
    }
    return null;
  }
  
  public final e a(t paramt)
  {
    return k;
  }
  
  public final i a(j paramj)
  {
    return e;
  }
  
  public final void a(k paramk, SSLSocket paramSSLSocket, boolean paramBoolean)
  {
    String[] arrayOfString1;
    if (f != null) {
      arrayOfString1 = (String[])com.d.a.a.j.a(String.class, f, paramSSLSocket.getEnabledCipherSuites());
    } else {
      arrayOfString1 = paramSSLSocket.getEnabledCipherSuites();
    }
    String[] arrayOfString2;
    if (g != null) {
      arrayOfString2 = (String[])com.d.a.a.j.a(String.class, g, paramSSLSocket.getEnabledProtocols());
    } else {
      arrayOfString2 = paramSSLSocket.getEnabledProtocols();
    }
    String[] arrayOfString3 = arrayOfString1;
    if (paramBoolean)
    {
      arrayOfString3 = arrayOfString1;
      if (com.d.a.a.j.a(paramSSLSocket.getSupportedCipherSuites(), "TLS_FALLBACK_SCSV")) {
        arrayOfString3 = com.d.a.a.j.b(arrayOfString1, "TLS_FALLBACK_SCSV");
      }
    }
    paramk = new k.a(paramk).a(arrayOfString3).b(arrayOfString2).b();
    if (g != null) {
      paramSSLSocket.setEnabledProtocols(g);
    }
    if (f != null) {
      paramSSLSocket.setEnabledCipherSuites(f);
    }
  }
  
  public final void a(p.a parama, String paramString)
  {
    parama.a(paramString);
  }
  
  public final boolean a(j paramj, b paramb)
  {
    if ((!j.f) && (!Thread.holdsLock(paramj))) {
      throw new AssertionError();
    }
    if ((!i) && (b != 0))
    {
      paramj.notifyAll();
      return false;
    }
    d.remove(paramb);
    return true;
  }
  
  public final void b(j paramj, b paramb)
  {
    if ((!j.f) && (!Thread.holdsLock(paramj))) {
      throw new AssertionError();
    }
    if (d.isEmpty()) {
      a.execute(c);
    }
    d.add(paramb);
  }
}

/* Location:
 * Qualified Name:     com.d.a.t.1
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */