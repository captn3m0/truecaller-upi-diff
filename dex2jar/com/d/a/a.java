package com.d.a;

import com.d.a.a.j;
import java.net.Proxy;
import java.net.ProxySelector;
import java.util.List;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;

public final class a
{
  public final q a;
  public final n b;
  public final SocketFactory c;
  public final b d;
  public final List<u> e;
  public final List<k> f;
  public final ProxySelector g;
  public final Proxy h;
  public final SSLSocketFactory i;
  public final HostnameVerifier j;
  public final f k;
  
  public a(String paramString, int paramInt, n paramn, SocketFactory paramSocketFactory, SSLSocketFactory paramSSLSocketFactory, HostnameVerifier paramHostnameVerifier, f paramf, b paramb, Proxy paramProxy, List<u> paramList, List<k> paramList1, ProxySelector paramProxySelector)
  {
    q.a locala = new q.a();
    String str;
    if (paramSSLSocketFactory != null) {
      str = "https";
    } else {
      str = "http";
    }
    a = locala.a(str).b(paramString).a(paramInt).b();
    if (paramn != null)
    {
      b = paramn;
      if (paramSocketFactory != null)
      {
        c = paramSocketFactory;
        if (paramb != null)
        {
          d = paramb;
          if (paramList != null)
          {
            e = j.a(paramList);
            if (paramList1 != null)
            {
              f = j.a(paramList1);
              if (paramProxySelector != null)
              {
                g = paramProxySelector;
                h = paramProxy;
                i = paramSSLSocketFactory;
                j = paramHostnameVerifier;
                k = paramf;
                return;
              }
              throw new IllegalArgumentException("proxySelector == null");
            }
            throw new IllegalArgumentException("connectionSpecs == null");
          }
          throw new IllegalArgumentException("protocols == null");
        }
        throw new IllegalArgumentException("authenticator == null");
      }
      throw new IllegalArgumentException("socketFactory == null");
    }
    throw new IllegalArgumentException("dns == null");
  }
  
  public final boolean equals(Object paramObject)
  {
    if ((paramObject instanceof a))
    {
      paramObject = (a)paramObject;
      return (a.equals(a)) && (b.equals(b)) && (d.equals(d)) && (e.equals(e)) && (f.equals(f)) && (g.equals(g)) && (j.a(h, h)) && (j.a(i, i)) && (j.a(j, j)) && (j.a(k, k));
    }
    return false;
  }
  
  public final int hashCode()
  {
    int i3 = a.hashCode();
    int i4 = b.hashCode();
    int i5 = d.hashCode();
    int i6 = e.hashCode();
    int i7 = f.hashCode();
    int i8 = g.hashCode();
    Object localObject = h;
    int i2 = 0;
    int m;
    if (localObject != null) {
      m = ((Proxy)localObject).hashCode();
    } else {
      m = 0;
    }
    localObject = i;
    int n;
    if (localObject != null) {
      n = localObject.hashCode();
    } else {
      n = 0;
    }
    localObject = j;
    int i1;
    if (localObject != null) {
      i1 = localObject.hashCode();
    } else {
      i1 = 0;
    }
    localObject = k;
    if (localObject != null) {
      i2 = localObject.hashCode();
    }
    return (((((((((i3 + 527) * 31 + i4) * 31 + i5) * 31 + i6) * 31 + i7) * 31 + i8) * 31 + m) * 31 + n) * 31 + i1) * 31 + i2;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */