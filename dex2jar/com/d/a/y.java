package com.d.a;

import d.e;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

public abstract class y
  implements Closeable
{
  public abstract long a()
    throws IOException;
  
  public abstract e b()
    throws IOException;
  
  public final InputStream c()
    throws IOException
  {
    return b().f();
  }
  
  public void close()
    throws IOException
  {
    b().close();
  }
}

/* Location:
 * Qualified Name:     com.d.a.y
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */