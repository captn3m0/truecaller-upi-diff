package com.d.a;

import com.d.a.a.b.s;
import com.d.a.a.i;
import java.net.CookieHandler;
import java.net.Proxy;
import java.net.ProxySelector;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class t
  implements Cloneable
{
  static final List<u> a = com.d.a.a.j.a(new u[] { u.d, u.c, u.b });
  static final List<k> b = com.d.a.a.j.a(new k[] { k.a, k.b, k.c });
  private static SSLSocketFactory y;
  private c A;
  m c;
  public Proxy d;
  public List<u> e;
  public List<k> f;
  final List<r> g = new ArrayList();
  public final List<r> h = new ArrayList();
  public ProxySelector i;
  public CookieHandler j;
  com.d.a.a.e k;
  public SocketFactory l;
  public SSLSocketFactory m;
  public HostnameVerifier n;
  public f o;
  public b p;
  public j q;
  public n r;
  public boolean s = true;
  public boolean t = true;
  public boolean u = true;
  public int v = 10000;
  public int w = 10000;
  public int x = 10000;
  private final i z;
  
  static
  {
    com.d.a.a.d.b = new com.d.a.a.d()
    {
      public final com.d.a.a.c.b a(j paramAnonymousj, a paramAnonymousa, s paramAnonymouss)
      {
        if ((!j.f) && (!Thread.holdsLock(paramAnonymousj))) {
          throw new AssertionError();
        }
        paramAnonymousj = d.iterator();
        while (paramAnonymousj.hasNext())
        {
          com.d.a.a.c.b localb = (com.d.a.a.c.b)paramAnonymousj.next();
          int j = h.size();
          com.d.a.a.a.d locald = d;
          int i;
          if (locald != null) {
            i = locald.a();
          } else {
            i = 1;
          }
          if ((j < i) && (paramAnonymousa.equals(a.a)) && (!i))
          {
            paramAnonymouss.a(localb);
            return localb;
          }
        }
        return null;
      }
      
      public final com.d.a.a.e a(t paramAnonymoust)
      {
        return k;
      }
      
      public final i a(j paramAnonymousj)
      {
        return e;
      }
      
      public final void a(k paramAnonymousk, SSLSocket paramAnonymousSSLSocket, boolean paramAnonymousBoolean)
      {
        String[] arrayOfString1;
        if (f != null) {
          arrayOfString1 = (String[])com.d.a.a.j.a(String.class, f, paramAnonymousSSLSocket.getEnabledCipherSuites());
        } else {
          arrayOfString1 = paramAnonymousSSLSocket.getEnabledCipherSuites();
        }
        String[] arrayOfString2;
        if (g != null) {
          arrayOfString2 = (String[])com.d.a.a.j.a(String.class, g, paramAnonymousSSLSocket.getEnabledProtocols());
        } else {
          arrayOfString2 = paramAnonymousSSLSocket.getEnabledProtocols();
        }
        String[] arrayOfString3 = arrayOfString1;
        if (paramAnonymousBoolean)
        {
          arrayOfString3 = arrayOfString1;
          if (com.d.a.a.j.a(paramAnonymousSSLSocket.getSupportedCipherSuites(), "TLS_FALLBACK_SCSV")) {
            arrayOfString3 = com.d.a.a.j.b(arrayOfString1, "TLS_FALLBACK_SCSV");
          }
        }
        paramAnonymousk = new k.a(paramAnonymousk).a(arrayOfString3).b(arrayOfString2).b();
        if (g != null) {
          paramAnonymousSSLSocket.setEnabledProtocols(g);
        }
        if (f != null) {
          paramAnonymousSSLSocket.setEnabledCipherSuites(f);
        }
      }
      
      public final void a(p.a paramAnonymousa, String paramAnonymousString)
      {
        paramAnonymousa.a(paramAnonymousString);
      }
      
      public final boolean a(j paramAnonymousj, com.d.a.a.c.b paramAnonymousb)
      {
        if ((!j.f) && (!Thread.holdsLock(paramAnonymousj))) {
          throw new AssertionError();
        }
        if ((!i) && (b != 0))
        {
          paramAnonymousj.notifyAll();
          return false;
        }
        d.remove(paramAnonymousb);
        return true;
      }
      
      public final void b(j paramAnonymousj, com.d.a.a.c.b paramAnonymousb)
      {
        if ((!j.f) && (!Thread.holdsLock(paramAnonymousj))) {
          throw new AssertionError();
        }
        if (d.isEmpty()) {
          a.execute(c);
        }
        d.add(paramAnonymousb);
      }
    };
  }
  
  public t()
  {
    z = new i();
    c = new m();
  }
  
  t(t paramt)
  {
    z = z;
    c = c;
    d = d;
    e = e;
    f = f;
    g.addAll(g);
    h.addAll(h);
    i = i;
    j = j;
    A = A;
    Object localObject = A;
    if (localObject != null) {
      localObject = a;
    } else {
      localObject = k;
    }
    k = ((com.d.a.a.e)localObject);
    l = l;
    m = m;
    n = n;
    o = o;
    p = p;
    q = q;
    r = r;
    s = s;
    t = t;
    u = u;
    v = v;
    w = w;
    x = x;
  }
  
  public final c a()
  {
    return A;
  }
  
  public final e a(v paramv)
  {
    return new e(this, paramv);
  }
  
  public final t a(c paramc)
  {
    A = paramc;
    k = null;
    return this;
  }
  
  public final void a(TimeUnit paramTimeUnit)
  {
    if (paramTimeUnit != null)
    {
      long l1 = paramTimeUnit.toMillis(15000L);
      if (l1 <= 2147483647L)
      {
        if (l1 == 0L) {
          throw new IllegalArgumentException("Timeout too small.");
        }
        v = ((int)l1);
        return;
      }
      throw new IllegalArgumentException("Timeout too large.");
    }
    throw new IllegalArgumentException("unit == null");
  }
  
  final SSLSocketFactory b()
  {
    for (;;)
    {
      Object localObject1;
      try
      {
        localObject1 = y;
        if (localObject1 != null) {}
      }
      finally {}
      try
      {
        localObject1 = SSLContext.getInstance("TLS");
        ((SSLContext)localObject1).init(null, null, null);
        y = ((SSLContext)localObject1).getSocketFactory();
      }
      catch (GeneralSecurityException localGeneralSecurityException) {}
    }
    throw new AssertionError();
    localObject1 = y;
    return (SSLSocketFactory)localObject1;
  }
  
  public final void b(TimeUnit paramTimeUnit)
  {
    if (paramTimeUnit != null)
    {
      long l1 = paramTimeUnit.toMillis(20000L);
      if (l1 <= 2147483647L)
      {
        if (l1 == 0L) {
          throw new IllegalArgumentException("Timeout too small.");
        }
        w = ((int)l1);
        return;
      }
      throw new IllegalArgumentException("Timeout too large.");
    }
    throw new IllegalArgumentException("unit == null");
  }
  
  public final void c(TimeUnit paramTimeUnit)
  {
    if (paramTimeUnit != null)
    {
      long l1 = paramTimeUnit.toMillis(20000L);
      if (l1 <= 2147483647L)
      {
        if (l1 == 0L) {
          throw new IllegalArgumentException("Timeout too small.");
        }
        x = ((int)l1);
        return;
      }
      throw new IllegalArgumentException("Timeout too large.");
    }
    throw new IllegalArgumentException("unit == null");
  }
}

/* Location:
 * Qualified Name:     com.d.a.t
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */