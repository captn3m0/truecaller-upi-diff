package com.d.b;

import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.ImageView;
import java.lang.ref.WeakReference;

public final class h
  implements ViewTreeObserver.OnPreDrawListener
{
  final ab a;
  final WeakReference<ImageView> b;
  e c;
  
  h(ab paramab, ImageView paramImageView, e parame)
  {
    a = paramab;
    b = new WeakReference(paramImageView);
    c = parame;
    paramImageView.getViewTreeObserver().addOnPreDrawListener(this);
  }
  
  public final void a()
  {
    c = null;
    Object localObject = (ImageView)b.get();
    if (localObject == null) {
      return;
    }
    localObject = ((ImageView)localObject).getViewTreeObserver();
    if (!((ViewTreeObserver)localObject).isAlive()) {
      return;
    }
    ((ViewTreeObserver)localObject).removeOnPreDrawListener(this);
  }
  
  public final boolean onPreDraw()
  {
    ImageView localImageView = (ImageView)b.get();
    if (localImageView == null) {
      return true;
    }
    Object localObject = localImageView.getViewTreeObserver();
    if (!((ViewTreeObserver)localObject).isAlive()) {
      return true;
    }
    int i = localImageView.getWidth();
    int j = localImageView.getHeight();
    if (i > 0)
    {
      if (j <= 0) {
        return true;
      }
      ((ViewTreeObserver)localObject).removeOnPreDrawListener(this);
      localObject = a;
      c = false;
      ((ab)localObject).b(i, j).a(localImageView, c);
      return true;
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.d.b.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */