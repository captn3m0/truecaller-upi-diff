package com.d.b;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

final class ah
  extends a<ag>
{
  ah(w paramw, ag paramag, aa paramaa, int paramInt1, int paramInt2, Drawable paramDrawable, String paramString, Object paramObject, int paramInt3)
  {
    super(paramw, paramag, paramaa, paramInt1, paramInt2, paramInt3, paramDrawable, paramString, paramObject, false);
  }
  
  final void a()
  {
    ag localag = (ag)c();
    if (localag != null)
    {
      if (g != 0)
      {
        localag.a(a.e.getResources().getDrawable(g));
        return;
      }
      localag.a(h);
    }
  }
  
  final void a(Bitmap paramBitmap, w.d paramd)
  {
    if (paramBitmap != null)
    {
      paramd = (ag)c();
      if (paramd != null)
      {
        paramd.a(paramBitmap);
        if (!paramBitmap.isRecycled()) {
          return;
        }
        throw new IllegalStateException("Target callback must not recycle bitmap!");
      }
      return;
    }
    throw new AssertionError(String.format("Attempted to complete action with no result!\n%s", new Object[] { this }));
  }
}

/* Location:
 * Qualified Name:     com.d.b.ah
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */