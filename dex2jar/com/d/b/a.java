package com.d.b;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

abstract class a<T>
{
  final w a;
  final aa b;
  final WeakReference<T> c;
  final boolean d;
  final int e;
  final int f;
  final int g;
  final Drawable h;
  final String i;
  final Object j;
  boolean k;
  boolean l;
  
  a(w paramw, T paramT, aa paramaa, int paramInt1, int paramInt2, int paramInt3, Drawable paramDrawable, String paramString, Object paramObject, boolean paramBoolean)
  {
    a = paramw;
    b = paramaa;
    if (paramT == null) {
      paramw = null;
    } else {
      paramw = new a(this, paramT, k);
    }
    c = paramw;
    e = paramInt1;
    f = paramInt2;
    d = paramBoolean;
    g = paramInt3;
    h = paramDrawable;
    i = paramString;
    if (paramObject != null) {
      paramw = (w)paramObject;
    } else {
      paramw = this;
    }
    j = paramw;
  }
  
  abstract void a();
  
  abstract void a(Bitmap paramBitmap, w.d paramd);
  
  void b()
  {
    l = true;
  }
  
  T c()
  {
    WeakReference localWeakReference = c;
    if (localWeakReference == null) {
      return null;
    }
    return (T)localWeakReference.get();
  }
  
  static final class a<M>
    extends WeakReference<M>
  {
    final a a;
    
    public a(a parama, M paramM, ReferenceQueue<? super M> paramReferenceQueue)
    {
      super(paramReferenceQueue);
      a = parama;
    }
  }
}

/* Location:
 * Qualified Name:     com.d.b.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */