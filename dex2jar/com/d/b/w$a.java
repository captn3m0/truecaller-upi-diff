package com.d.b;

import android.content.Context;
import android.graphics.Bitmap.Config;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

public final class w$a
{
  public d a;
  public boolean b;
  public boolean c;
  private final Context d;
  private j e;
  private ExecutorService f;
  private w.c g;
  private w.f h;
  private List<ac> i;
  private Bitmap.Config j;
  
  public w$a(Context paramContext)
  {
    if (paramContext != null)
    {
      d = paramContext.getApplicationContext();
      return;
    }
    throw new IllegalArgumentException("Context must not be null.");
  }
  
  public final a a(ac paramac)
  {
    if (paramac != null)
    {
      if (i == null) {
        i = new ArrayList();
      }
      if (!i.contains(paramac))
      {
        i.add(paramac);
        return this;
      }
      throw new IllegalStateException("RequestHandler already registered.");
    }
    throw new IllegalArgumentException("RequestHandler must not be null.");
  }
  
  public final a a(j paramj)
  {
    if (e == null)
    {
      e = paramj;
      return this;
    }
    throw new IllegalStateException("Downloader already set.");
  }
  
  public final w a()
  {
    Context localContext = d;
    if (e == null) {
      e = al.a(localContext);
    }
    if (a == null) {
      a = new p(localContext);
    }
    if (f == null) {
      f = new y();
    }
    if (h == null) {
      h = w.f.a;
    }
    ae localae = new ae(a);
    return new w(localContext, new i(localContext, f, w.a, e, a, localae), a, g, h, i, localae, j, b, c);
  }
}

/* Location:
 * Qualified Name:     com.d.b.w.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */