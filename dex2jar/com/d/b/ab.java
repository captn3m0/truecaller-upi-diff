package com.d.b;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public final class ab
{
  private static final AtomicInteger g = new AtomicInteger();
  public final w a;
  public final aa.a b;
  public boolean c;
  public int d;
  public int e;
  public Object f;
  private boolean h;
  private boolean i = true;
  private int j;
  private int k;
  private Drawable l;
  private Drawable m;
  
  ab()
  {
    a = null;
    b = new aa.a(null, 0, null);
  }
  
  ab(w paramw, Uri paramUri, int paramInt)
  {
    if (!o)
    {
      a = paramw;
      b = new aa.a(paramUri, paramInt, l);
      return;
    }
    throw new IllegalStateException("Picasso instance already shut down. Cannot submit new requests.");
  }
  
  private Drawable e()
  {
    if (j != 0) {
      return a.e.getResources().getDrawable(j);
    }
    return l;
  }
  
  public final aa a(long paramLong)
  {
    int n = g.getAndIncrement();
    aa localaa1 = b.c();
    a = n;
    b = paramLong;
    boolean bool = a.n;
    if (bool) {
      al.a("Main", "created", localaa1.b(), localaa1.toString());
    }
    aa localaa2 = a.a(localaa1);
    if (localaa2 != localaa1)
    {
      a = n;
      b = paramLong;
      if (bool) {
        al.a("Main", "changed", localaa2.a(), "into ".concat(String.valueOf(localaa2)));
      }
    }
    return localaa2;
  }
  
  public final ab a()
  {
    if (j == 0)
    {
      if (l == null)
      {
        i = false;
        return this;
      }
      throw new IllegalStateException("Placeholder image already set.");
    }
    throw new IllegalStateException("Placeholder resource already set.");
  }
  
  public final ab a(int paramInt)
  {
    if (i)
    {
      if (paramInt != 0)
      {
        if (l == null)
        {
          j = paramInt;
          return this;
        }
        throw new IllegalStateException("Placeholder image already set.");
      }
      throw new IllegalArgumentException("Placeholder image resource invalid.");
    }
    throw new IllegalStateException("Already explicitly declared as no placeholder.");
  }
  
  public final ab a(int paramInt1, int paramInt2)
  {
    Resources localResources = a.e.getResources();
    return b(localResources.getDimensionPixelSize(paramInt1), localResources.getDimensionPixelSize(paramInt2));
  }
  
  public final ab a(Drawable paramDrawable)
  {
    if (i)
    {
      if (j == 0)
      {
        l = paramDrawable;
        return this;
      }
      throw new IllegalStateException("Placeholder image already set.");
    }
    throw new IllegalStateException("Already explicitly declared as no placeholder.");
  }
  
  public final ab a(ai paramai)
  {
    aa.a locala = b;
    if (paramai != null)
    {
      if (paramai.a() != null)
      {
        if (f == null) {
          f = new ArrayList(2);
        }
        f.add(paramai);
        return this;
      }
      throw new IllegalArgumentException("Transformation key must not be null.");
    }
    throw new IllegalArgumentException("Transformation must not be null.");
  }
  
  public final ab a(s paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      int n = d;
      d = (c | n);
      return this;
    }
    throw new IllegalArgumentException("Memory policy cannot be null.");
  }
  
  public final ab a(t paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      int n = e;
      e = (d | n);
      return this;
    }
    throw new IllegalArgumentException("Network policy cannot be null.");
  }
  
  public final ab a(Object paramObject)
  {
    if (paramObject != null)
    {
      if (f == null)
      {
        f = paramObject;
        return this;
      }
      throw new IllegalStateException("Tag already set.");
    }
    throw new IllegalArgumentException("Tag invalid.");
  }
  
  public final void a(ImageView paramImageView, e parame)
  {
    long l1 = System.nanoTime();
    al.b();
    if (paramImageView != null)
    {
      if (!b.a())
      {
        a.d(paramImageView);
        if (i) {
          x.a(paramImageView, e());
        }
        return;
      }
      if (c) {
        if (!b.b())
        {
          int n = paramImageView.getWidth();
          int i1 = paramImageView.getHeight();
          if ((n != 0) && (i1 != 0))
          {
            b.a(n, i1);
          }
          else
          {
            if (i) {
              x.a(paramImageView, e());
            }
            a.a(paramImageView, new h(this, paramImageView, parame));
          }
        }
        else
        {
          throw new IllegalStateException("Fit cannot be used with resize.");
        }
      }
      Object localObject = a(l1);
      String str = al.a((aa)localObject);
      if (s.a(d))
      {
        Bitmap localBitmap = a.b(str);
        if (localBitmap != null)
        {
          a.d(paramImageView);
          x.a(paramImageView, a.e, localBitmap, w.d.a, h, a.m);
          if (a.n)
          {
            paramImageView = ((aa)localObject).b();
            localObject = new StringBuilder("from ");
            ((StringBuilder)localObject).append(w.d.a);
            al.a("Main", "completed", paramImageView, ((StringBuilder)localObject).toString());
          }
          if (parame != null) {
            parame.onSuccess();
          }
          return;
        }
      }
      if (i) {
        x.a(paramImageView, e());
      }
      paramImageView = new o(a, paramImageView, (aa)localObject, d, e, k, m, str, f, parame, h);
      a.a(paramImageView);
      return;
    }
    throw new IllegalArgumentException("Target must not be null.");
  }
  
  public final void a(ag paramag)
  {
    long l1 = System.nanoTime();
    al.b();
    if (paramag != null)
    {
      if (!c)
      {
        boolean bool = b.a();
        Object localObject = null;
        aa localaa = null;
        if (!bool)
        {
          a.d(paramag);
          localObject = localaa;
          if (i) {
            localObject = e();
          }
          paramag.b((Drawable)localObject);
          return;
        }
        localaa = a(l1);
        String str = al.a(localaa);
        if (s.a(d))
        {
          Bitmap localBitmap = a.b(str);
          if (localBitmap != null)
          {
            a.d(paramag);
            localObject = w.d.a;
            paramag.a(localBitmap);
            return;
          }
        }
        if (i) {
          localObject = e();
        }
        paramag.b((Drawable)localObject);
        paramag = new ah(a, paramag, localaa, d, e, m, str, f, k);
        a.a(paramag);
        return;
      }
      throw new IllegalStateException("Fit cannot be used with a Target.");
    }
    throw new IllegalArgumentException("Target must not be null.");
  }
  
  public final ab b()
  {
    aa.a locala = b;
    if (!d)
    {
      c = true;
      return this;
    }
    throw new IllegalStateException("Center crop can not be used after calling centerInside");
  }
  
  public final ab b(int paramInt)
  {
    if (paramInt != 0)
    {
      if (m == null)
      {
        k = paramInt;
        return this;
      }
      throw new IllegalStateException("Error image already set.");
    }
    throw new IllegalArgumentException("Error image resource invalid.");
  }
  
  public final ab b(int paramInt1, int paramInt2)
  {
    b.a(paramInt1, paramInt2);
    return this;
  }
  
  public final ab b(Drawable paramDrawable)
  {
    if (paramDrawable != null)
    {
      if (k == 0)
      {
        m = paramDrawable;
        return this;
      }
      throw new IllegalStateException("Error image already set.");
    }
    throw new IllegalArgumentException("Error image may not be null.");
  }
  
  public final ab c()
  {
    aa.a locala = b;
    if (!c)
    {
      d = true;
      return this;
    }
    throw new IllegalStateException("Center inside can not be used after calling centerCrop");
  }
  
  public final Bitmap d()
    throws IOException
  {
    long l1 = System.nanoTime();
    al.a();
    if (!c)
    {
      if (!b.a()) {
        return null;
      }
      Object localObject1 = a(l1);
      Object localObject2 = al.a((aa)localObject1, new StringBuilder());
      localObject1 = new n(a, (aa)localObject1, d, e, f, (String)localObject2);
      localObject2 = a;
      return c.a((w)localObject2, f, a.g, a.h, (a)localObject1).a();
    }
    throw new IllegalStateException("Fit cannot be used with get.");
  }
}

/* Location:
 * Qualified Name:     com.d.b.ab
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */