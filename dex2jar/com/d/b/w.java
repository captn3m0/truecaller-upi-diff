package com.d.b;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.widget.ImageView;
import java.lang.ref.ReferenceQueue;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;

public class w
{
  public static final Handler a = new Handler(Looper.getMainLooper())
  {
    public final void handleMessage(Message paramAnonymousMessage)
    {
      int j = what;
      if (j != 3)
      {
        int i = 0;
        Object localObject1;
        Object localObject2;
        Object localObject3;
        if (j != 8)
        {
          if (j == 13)
          {
            localObject1 = (List)obj;
            j = ((List)localObject1).size();
            while (i < j)
            {
              localObject2 = (a)((List)localObject1).get(i);
              localObject3 = a;
              paramAnonymousMessage = null;
              if (s.a(e)) {
                paramAnonymousMessage = ((w)localObject3).b(i);
              }
              if (paramAnonymousMessage != null)
              {
                ((w)localObject3).a(paramAnonymousMessage, w.d.a, (a)localObject2);
                if (n)
                {
                  paramAnonymousMessage = b.a();
                  localObject2 = new StringBuilder("from ");
                  ((StringBuilder)localObject2).append(w.d.a);
                  al.a("Main", "completed", paramAnonymousMessage, ((StringBuilder)localObject2).toString());
                }
              }
              else
              {
                ((w)localObject3).a((a)localObject2);
                if (n) {
                  al.a("Main", "resumed", b.a());
                }
              }
              i += 1;
            }
            return;
          }
          localObject1 = new StringBuilder("Unknown handler message received: ");
          ((StringBuilder)localObject1).append(what);
          throw new AssertionError(((StringBuilder)localObject1).toString());
        }
        paramAnonymousMessage = (List)obj;
        int n = paramAnonymousMessage.size();
        i = 0;
        while (i < n)
        {
          Object localObject4 = (c)paramAnonymousMessage.get(i);
          localObject1 = b;
          localObject2 = k;
          localObject3 = l;
          int m = 1;
          if ((localObject3 != null) && (!((List)localObject3).isEmpty())) {
            j = 1;
          } else {
            j = 0;
          }
          int k = m;
          if (localObject2 == null) {
            if (j != 0) {
              k = m;
            } else {
              k = 0;
            }
          }
          if (k != 0)
          {
            Bitmap localBitmap = m;
            localObject4 = o;
            if (localObject2 != null) {
              ((w)localObject1).a(localBitmap, (w.d)localObject4, (a)localObject2);
            }
            if (j != 0)
            {
              k = ((List)localObject3).size();
              j = 0;
              while (j < k)
              {
                ((w)localObject1).a(localBitmap, (w.d)localObject4, (a)((List)localObject3).get(j));
                j += 1;
              }
            }
          }
          i += 1;
        }
        return;
      }
      paramAnonymousMessage = (a)obj;
      if (a.n) {
        al.a("Main", "canceled", b.a(), "target got garbage collected");
      }
      w.a(a, paramAnonymousMessage.c());
    }
  };
  public static volatile w b = null;
  public final b c;
  public final List<ac> d;
  final Context e;
  public final i f;
  public final d g;
  public final ae h;
  final Map<Object, a> i;
  public final Map<ImageView, h> j;
  final ReferenceQueue<Object> k;
  final Bitmap.Config l;
  boolean m;
  public volatile boolean n;
  public boolean o;
  private final c p;
  private final f q;
  
  w(Context paramContext, i parami, d paramd, c paramc, f paramf, List<ac> paramList, ae paramae, Bitmap.Config paramConfig, boolean paramBoolean1, boolean paramBoolean2)
  {
    e = paramContext;
    f = parami;
    g = paramd;
    p = paramc;
    q = paramf;
    l = paramConfig;
    int i1;
    if (paramList != null) {
      i1 = paramList.size();
    } else {
      i1 = 0;
    }
    paramd = new ArrayList(i1 + 7);
    paramd.add(new ad(paramContext));
    if (paramList != null) {
      paramd.addAll(paramList);
    }
    paramd.add(new f(paramContext));
    paramd.add(new r(paramContext));
    paramd.add(new g(paramContext));
    paramd.add(new b(paramContext));
    paramd.add(new l(paramContext));
    paramd.add(new u(d, paramae));
    d = Collections.unmodifiableList(paramd);
    h = paramae;
    i = new WeakHashMap();
    j = new WeakHashMap();
    m = paramBoolean1;
    n = paramBoolean2;
    k = new ReferenceQueue();
    c = new b(k, a);
    c.start();
  }
  
  public static w a(Context paramContext)
  {
    if (b == null) {
      try
      {
        if (b == null) {
          b = new a(paramContext).a();
        }
      }
      finally {}
    }
    return b;
  }
  
  public static void a(w paramw)
  {
    try
    {
      if (b == null)
      {
        b = paramw;
        return;
      }
      throw new IllegalStateException("Singleton instance already exists.");
    }
    finally {}
  }
  
  final aa a(aa paramaa)
  {
    Object localObject = q.a(paramaa);
    if (localObject != null) {
      return (aa)localObject;
    }
    localObject = new StringBuilder("Request transformer ");
    ((StringBuilder)localObject).append(q.getClass().getCanonicalName());
    ((StringBuilder)localObject).append(" returned null for ");
    ((StringBuilder)localObject).append(paramaa);
    throw new IllegalStateException(((StringBuilder)localObject).toString());
  }
  
  public final ab a(int paramInt)
  {
    if (paramInt != 0) {
      return new ab(this, null, paramInt);
    }
    throw new IllegalArgumentException("Resource ID must not be zero.");
  }
  
  public final ab a(Uri paramUri)
  {
    return new ab(this, paramUri, 0);
  }
  
  public final ab a(String paramString)
  {
    if (paramString == null) {
      return new ab(this, null, 0);
    }
    if (paramString.trim().length() != 0) {
      return a(Uri.parse(paramString));
    }
    throw new IllegalArgumentException("Path must not be empty.");
  }
  
  final void a(Bitmap paramBitmap, d paramd, a parama)
  {
    if (l) {
      return;
    }
    if (!k) {
      i.remove(parama.c());
    }
    if (paramBitmap != null)
    {
      if (paramd != null)
      {
        parama.a(paramBitmap, paramd);
        if (n) {
          al.a("Main", "completed", b.a(), "from ".concat(String.valueOf(paramd)));
        }
      }
      else
      {
        throw new AssertionError("LoadedFrom cannot be null.");
      }
    }
    else
    {
      parama.a();
      if (n) {
        al.a("Main", "errored", b.a());
      }
    }
  }
  
  final void a(ImageView paramImageView, h paramh)
  {
    j.put(paramImageView, paramh);
  }
  
  final void a(a parama)
  {
    Object localObject = parama.c();
    if ((localObject != null) && (i.get(localObject) != parama))
    {
      d(localObject);
      i.put(localObject, parama);
    }
    b(parama);
  }
  
  public final void a(Object paramObject)
  {
    al.b();
    ArrayList localArrayList = new ArrayList(i.values());
    int i2 = localArrayList.size();
    int i1 = 0;
    while (i1 < i2)
    {
      a locala = (a)localArrayList.get(i1);
      if (j.equals(paramObject)) {
        d(locala.c());
      }
      i1 += 1;
    }
  }
  
  public final Bitmap b(String paramString)
  {
    paramString = g.a(paramString);
    if (paramString != null)
    {
      h.a();
      return paramString;
    }
    h.b();
    return paramString;
  }
  
  public final void b(a parama)
  {
    f.a(parama);
  }
  
  public final void b(Object paramObject)
  {
    i locali = f;
    i.sendMessage(i.obtainMessage(11, paramObject));
  }
  
  public final void c(Object paramObject)
  {
    i locali = f;
    i.sendMessage(i.obtainMessage(12, paramObject));
  }
  
  public final void d(Object paramObject)
  {
    al.b();
    a locala = (a)i.remove(paramObject);
    if (locala != null)
    {
      locala.b();
      f.b(locala);
    }
    if ((paramObject instanceof ImageView))
    {
      paramObject = (ImageView)paramObject;
      paramObject = (h)j.remove(paramObject);
      if (paramObject != null) {
        ((h)paramObject).a();
      }
    }
  }
  
  public static final class a
  {
    public d a;
    public boolean b;
    public boolean c;
    private final Context d;
    private j e;
    private ExecutorService f;
    private w.c g;
    private w.f h;
    private List<ac> i;
    private Bitmap.Config j;
    
    public a(Context paramContext)
    {
      if (paramContext != null)
      {
        d = paramContext.getApplicationContext();
        return;
      }
      throw new IllegalArgumentException("Context must not be null.");
    }
    
    public final a a(ac paramac)
    {
      if (paramac != null)
      {
        if (i == null) {
          i = new ArrayList();
        }
        if (!i.contains(paramac))
        {
          i.add(paramac);
          return this;
        }
        throw new IllegalStateException("RequestHandler already registered.");
      }
      throw new IllegalArgumentException("RequestHandler must not be null.");
    }
    
    public final a a(j paramj)
    {
      if (e == null)
      {
        e = paramj;
        return this;
      }
      throw new IllegalStateException("Downloader already set.");
    }
    
    public final w a()
    {
      Context localContext = d;
      if (e == null) {
        e = al.a(localContext);
      }
      if (a == null) {
        a = new p(localContext);
      }
      if (f == null) {
        f = new y();
      }
      if (h == null) {
        h = w.f.a;
      }
      ae localae = new ae(a);
      return new w(localContext, new i(localContext, f, w.a, e, a, localae), a, g, h, i, localae, j, b, c);
    }
  }
  
  public static final class b
    extends Thread
  {
    private final ReferenceQueue<Object> a;
    private final Handler b;
    
    b(ReferenceQueue<Object> paramReferenceQueue, Handler paramHandler)
    {
      a = paramReferenceQueue;
      b = paramHandler;
      setDaemon(true);
      setName("Picasso-refQueue");
    }
    
    public final void run()
    {
      Process.setThreadPriority(10);
      try
      {
        for (;;)
        {
          a.a locala = (a.a)a.remove(1000L);
          Message localMessage = b.obtainMessage();
          if (locala != null)
          {
            what = 3;
            obj = a;
            b.sendMessage(localMessage);
          }
          else
          {
            localMessage.recycle();
          }
        }
        return;
      }
      catch (Exception localException)
      {
        b.post(new Runnable()
        {
          public final void run()
          {
            throw new RuntimeException(localException);
          }
        });
        return;
      }
      catch (InterruptedException localInterruptedException) {}
    }
  }
  
  public static abstract interface c {}
  
  public static enum d
  {
    final int d;
    
    private d(int paramInt)
    {
      d = paramInt;
    }
  }
  
  public static enum e
  {
    private e() {}
  }
  
  public static abstract interface f
  {
    public static final f a = new f()
    {
      public final aa a(aa paramAnonymousaa)
      {
        return paramAnonymousaa;
      }
    };
    
    public abstract aa a(aa paramaa);
  }
}

/* Location:
 * Qualified Name:     com.d.b.w
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */