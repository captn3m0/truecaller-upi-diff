package com.d.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;

public final class i
{
  public final b a = new b();
  final Context b;
  public final ExecutorService c;
  public final j d;
  final Map<String, c> e;
  final Map<Object, a> f;
  final Map<Object, a> g;
  final Set<Object> h;
  final Handler i;
  final Handler j;
  final d k;
  final ae l;
  final List<c> m;
  final c n;
  final boolean o;
  boolean p;
  
  i(Context paramContext, ExecutorService paramExecutorService, Handler paramHandler, j paramj, d paramd, ae paramae)
  {
    a.start();
    al.a(a.getLooper());
    b = paramContext;
    c = paramExecutorService;
    e = new LinkedHashMap();
    f = new WeakHashMap();
    g = new WeakHashMap();
    h = new HashSet();
    i = new a(a.getLooper(), this);
    d = paramj;
    j = paramHandler;
    k = paramd;
    l = paramae;
    m = new ArrayList(4);
    p = al.d(b);
    o = al.b(paramContext, "android.permission.ACCESS_NETWORK_STATE");
    n = new c(this);
    paramContext = n;
    paramExecutorService = new IntentFilter();
    paramExecutorService.addAction("android.intent.action.AIRPLANE_MODE");
    if (a.o) {
      paramExecutorService.addAction("android.net.conn.CONNECTIVITY_CHANGE");
    }
    a.b.registerReceiver(paramContext, paramExecutorService);
  }
  
  static void a(List<c> paramList)
  {
    if (paramList.isEmpty()) {
      return;
    }
    if (get0b.n)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      paramList = paramList.iterator();
      while (paramList.hasNext())
      {
        c localc = (c)paramList.next();
        if (localStringBuilder.length() > 0) {
          localStringBuilder.append(", ");
        }
        localStringBuilder.append(al.a(localc));
      }
      al.a("Dispatcher", "delivered", localStringBuilder.toString());
    }
  }
  
  private void c(a parama)
  {
    Object localObject = parama.c();
    if (localObject != null)
    {
      k = true;
      f.put(localObject, parama);
    }
  }
  
  private void e(c paramc)
  {
    a locala = k;
    if (locala != null) {
      c(locala);
    }
    paramc = l;
    if (paramc != null)
    {
      int i1 = 0;
      int i2 = paramc.size();
      while (i1 < i2)
      {
        c((a)paramc.get(i1));
        i1 += 1;
      }
    }
  }
  
  final void a(NetworkInfo paramNetworkInfo)
  {
    Object localObject = c;
    if ((localObject instanceof y))
    {
      localObject = (y)localObject;
      int i1;
      if ((paramNetworkInfo != null) && (paramNetworkInfo.isConnectedOrConnecting()))
      {
        i1 = paramNetworkInfo.getType();
        if ((i1 == 6) || (i1 == 9)) {}
      }
      switch (i1)
      {
      default: 
        ((y)localObject).a(3);
        break;
      case 0: 
        i1 = paramNetworkInfo.getSubtype();
        switch (i1)
        {
        default: 
          switch (i1)
          {
          default: 
            ((y)localObject).a(3);
            break;
          case 13: 
          case 14: 
          case 15: 
            ((y)localObject).a(3);
          }
          break;
        case 3: 
        case 4: 
        case 5: 
        case 6: 
          ((y)localObject).a(2);
          break;
        case 1: 
        case 2: 
          ((y)localObject).a(1);
        }
        break;
      case 1: 
        ((y)localObject).a(4);
        break;
        ((y)localObject).a(3);
      }
    }
    if ((paramNetworkInfo != null) && (paramNetworkInfo.isConnected()) && (!f.isEmpty()))
    {
      paramNetworkInfo = f.values().iterator();
      while (paramNetworkInfo.hasNext())
      {
        localObject = (a)paramNetworkInfo.next();
        paramNetworkInfo.remove();
        if (a.n) {
          al.a("Dispatcher", "replaying", b.a());
        }
        a((a)localObject, false);
      }
    }
  }
  
  final void a(a parama)
  {
    Handler localHandler = i;
    localHandler.sendMessage(localHandler.obtainMessage(1, parama));
  }
  
  final void a(a parama, boolean paramBoolean)
  {
    Object localObject2;
    if (h.contains(j))
    {
      g.put(parama.c(), parama);
      if (a.n)
      {
        localObject1 = b.a();
        localObject2 = new StringBuilder("because tag '");
        ((StringBuilder)localObject2).append(j);
        ((StringBuilder)localObject2).append("' is paused");
        al.a("Dispatcher", "paused", (String)localObject1, ((StringBuilder)localObject2).toString());
      }
      return;
    }
    Object localObject1 = (c)e.get(i);
    if (localObject1 != null)
    {
      paramBoolean = b.n;
      localObject2 = b;
      if (k == null)
      {
        k = parama;
        if (paramBoolean)
        {
          if ((l != null) && (!l.isEmpty()))
          {
            al.a("Hunter", "joined", ((aa)localObject2).a(), al.a((c)localObject1, "to "));
            return;
          }
          al.a("Hunter", "joined", ((aa)localObject2).a(), "to empty hunter");
          return;
        }
        return;
      }
      if (l == null) {
        l = new ArrayList(3);
      }
      l.add(parama);
      if (paramBoolean) {
        al.a("Hunter", "joined", ((aa)localObject2).a(), al.a((c)localObject1, "to "));
      }
      parama = b.r;
      if (parama.ordinal() > s.ordinal()) {
        s = parama;
      }
      return;
    }
    if (c.isShutdown())
    {
      if (a.n) {
        al.a("Dispatcher", "ignored", b.a(), "because shut down");
      }
      return;
    }
    localObject1 = c.a(a, this, k, l, parama);
    n = c.submit((Runnable)localObject1);
    e.put(i, localObject1);
    if (paramBoolean) {
      f.remove(parama.c());
    }
    if (a.n) {
      al.a("Dispatcher", "enqueued", b.a());
    }
  }
  
  final void a(c paramc)
  {
    Handler localHandler = i;
    localHandler.sendMessageDelayed(localHandler.obtainMessage(5, paramc), 500L);
  }
  
  final void a(c paramc, boolean paramBoolean)
  {
    if (b.n)
    {
      String str2 = al.a(paramc);
      StringBuilder localStringBuilder = new StringBuilder("for error");
      String str1;
      if (paramBoolean) {
        str1 = " (will replay)";
      } else {
        str1 = "";
      }
      localStringBuilder.append(str1);
      al.a("Dispatcher", "batched", str2, localStringBuilder.toString());
    }
    e.remove(f);
    d(paramc);
  }
  
  final void a(Object paramObject)
  {
    if (!h.remove(paramObject)) {
      return;
    }
    Object localObject1 = null;
    Iterator localIterator = g.values().iterator();
    while (localIterator.hasNext())
    {
      a locala = (a)localIterator.next();
      if (j.equals(paramObject))
      {
        Object localObject2 = localObject1;
        if (localObject1 == null) {
          localObject2 = new ArrayList();
        }
        ((List)localObject2).add(locala);
        localIterator.remove();
        localObject1 = localObject2;
      }
    }
    if (localObject1 != null)
    {
      paramObject = j;
      ((Handler)paramObject).sendMessage(((Handler)paramObject).obtainMessage(13, localObject1));
    }
  }
  
  final void b(a parama)
  {
    Handler localHandler = i;
    localHandler.sendMessage(localHandler.obtainMessage(2, parama));
  }
  
  final void b(c paramc)
  {
    Handler localHandler = i;
    localHandler.sendMessage(localHandler.obtainMessage(6, paramc));
  }
  
  final void c(c paramc)
  {
    if (paramc.c()) {
      return;
    }
    boolean bool1 = c.isShutdown();
    boolean bool2 = false;
    if (bool1)
    {
      a(paramc, false);
      return;
    }
    NetworkInfo localNetworkInfo = null;
    if (o) {
      localNetworkInfo = ((ConnectivityManager)al.a(b, "connectivity")).getActiveNetworkInfo();
    }
    int i1;
    if ((localNetworkInfo != null) && (localNetworkInfo.isConnected())) {
      i1 = 1;
    } else {
      i1 = 0;
    }
    int i2;
    if (r > 0) {
      i2 = 1;
    } else {
      i2 = 0;
    }
    if (i2 == 0)
    {
      bool1 = false;
    }
    else
    {
      r -= 1;
      bool1 = j.a(localNetworkInfo);
    }
    boolean bool3 = j.b();
    if (!bool1)
    {
      bool1 = bool2;
      if (o)
      {
        bool1 = bool2;
        if (bool3) {
          bool1 = true;
        }
      }
      a(paramc, bool1);
      if (bool1) {
        e(paramc);
      }
      return;
    }
    if ((o) && (i1 == 0))
    {
      a(paramc, bool3);
      if (bool3) {
        e(paramc);
      }
      return;
    }
    if (b.n) {
      al.a("Dispatcher", "retrying", al.a(paramc));
    }
    if ((p instanceof u.a)) {
      i |= ad;
    }
    n = c.submit(paramc);
  }
  
  final void d(c paramc)
  {
    if (paramc.c()) {
      return;
    }
    m.add(paramc);
    if (!i.hasMessages(7)) {
      i.sendEmptyMessageDelayed(7, 200L);
    }
  }
  
  static final class a
    extends Handler
  {
    private final i a;
    
    public a(Looper paramLooper, i parami)
    {
      super();
      a = parami;
    }
    
    public final void handleMessage(final Message paramMessage)
    {
      int i = what;
      boolean bool = false;
      Object localObject1;
      Object localObject2;
      c localc;
      switch (i)
      {
      case 3: 
      case 8: 
      default: 
        w.a.post(new Runnable()
        {
          public final void run()
          {
            StringBuilder localStringBuilder = new StringBuilder("Unknown handler message received: ");
            localStringBuilder.append(paramMessagewhat);
            throw new AssertionError(localStringBuilder.toString());
          }
        });
        return;
      case 12: 
        paramMessage = obj;
        a.a(paramMessage);
        return;
      case 11: 
        paramMessage = obj;
        localObject1 = a;
        if (h.add(paramMessage))
        {
          localObject2 = e.values().iterator();
          while (((Iterator)localObject2).hasNext())
          {
            localc = (c)((Iterator)localObject2).next();
            bool = b.n;
            Object localObject3 = k;
            List localList = l;
            if ((localList != null) && (!localList.isEmpty())) {
              i = 1;
            } else {
              i = 0;
            }
            if ((localObject3 != null) || (i != 0))
            {
              StringBuilder localStringBuilder;
              if ((localObject3 != null) && (j.equals(paramMessage)))
              {
                localc.a((a)localObject3);
                g.put(((a)localObject3).c(), localObject3);
                if (bool)
                {
                  localObject3 = b.a();
                  localStringBuilder = new StringBuilder("because tag '");
                  localStringBuilder.append(paramMessage);
                  localStringBuilder.append("' was paused");
                  al.a("Dispatcher", "paused", (String)localObject3, localStringBuilder.toString());
                }
              }
              if (i != 0)
              {
                i = localList.size() - 1;
                while (i >= 0)
                {
                  localObject3 = (a)localList.get(i);
                  if (j.equals(paramMessage))
                  {
                    localc.a((a)localObject3);
                    g.put(((a)localObject3).c(), localObject3);
                    if (bool)
                    {
                      localObject3 = b.a();
                      localStringBuilder = new StringBuilder("because tag '");
                      localStringBuilder.append(paramMessage);
                      localStringBuilder.append("' was paused");
                      al.a("Dispatcher", "paused", (String)localObject3, localStringBuilder.toString());
                    }
                  }
                  i -= 1;
                }
              }
              if (localc.b())
              {
                ((Iterator)localObject2).remove();
                if (bool) {
                  al.a("Dispatcher", "canceled", al.a(localc), "all actions paused");
                }
              }
            }
          }
        }
        return;
      case 10: 
        localObject1 = a;
        if (arg1 == 1) {
          bool = true;
        }
        p = bool;
        return;
      case 9: 
        paramMessage = (NetworkInfo)obj;
        a.a(paramMessage);
        return;
      case 7: 
        paramMessage = a;
        localObject1 = new ArrayList(m);
        m.clear();
        j.sendMessage(j.obtainMessage(8, localObject1));
        i.a((List)localObject1);
        return;
      case 6: 
        paramMessage = (c)obj;
        a.a(paramMessage, false);
        return;
      case 5: 
        paramMessage = (c)obj;
        a.c(paramMessage);
        return;
      case 4: 
        paramMessage = (c)obj;
        localObject1 = a;
        if (s.b(h)) {
          k.a(f, m);
        }
        e.remove(f);
        ((i)localObject1).d(paramMessage);
        if (b.n) {
          al.a("Dispatcher", "batched", al.a(paramMessage), "for completion");
        }
        return;
      case 2: 
        paramMessage = (a)obj;
        localObject1 = a;
        localObject2 = i;
        localc = (c)e.get(localObject2);
        if (localc != null)
        {
          localc.a(paramMessage);
          if (localc.b())
          {
            e.remove(localObject2);
            if (a.n) {
              al.a("Dispatcher", "canceled", b.a());
            }
          }
        }
        if (h.contains(j))
        {
          g.remove(paramMessage.c());
          if (a.n) {
            al.a("Dispatcher", "canceled", b.a(), "because paused request got canceled");
          }
        }
        paramMessage = (a)f.remove(paramMessage.c());
        if ((paramMessage != null) && (a.n)) {
          al.a("Dispatcher", "canceled", b.a(), "from replaying");
        }
        return;
      }
      paramMessage = (a)obj;
      a.a(paramMessage, true);
    }
  }
  
  public static final class b
    extends HandlerThread
  {
    b()
    {
      super(10);
    }
  }
  
  static final class c
    extends BroadcastReceiver
  {
    final i a;
    
    c(i parami)
    {
      a = parami;
    }
    
    public final void onReceive(Context paramContext, Intent paramIntent)
    {
      throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
    }
  }
}

/* Location:
 * Qualified Name:     com.d.b.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */