package com.d.b;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;

public final class ae
{
  public final HandlerThread a;
  final d b;
  final Handler c;
  long d;
  long e;
  long f;
  long g;
  long h;
  long i;
  long j;
  long k;
  int l;
  int m;
  int n;
  
  ae(d paramd)
  {
    b = paramd;
    a = new HandlerThread("Picasso-Stats", 10);
    a.start();
    al.a(a.getLooper());
    c = new a(a.getLooper(), this);
  }
  
  final void a()
  {
    c.sendEmptyMessage(0);
  }
  
  final void a(Bitmap paramBitmap, int paramInt)
  {
    int i1 = al.a(paramBitmap);
    paramBitmap = c;
    paramBitmap.sendMessage(paramBitmap.obtainMessage(paramInt, i1, 0));
  }
  
  final void b()
  {
    c.sendEmptyMessage(1);
  }
  
  static final class a
    extends Handler
  {
    private final ae a;
    
    public a(Looper paramLooper, ae paramae)
    {
      super();
      a = paramae;
    }
    
    public final void handleMessage(final Message paramMessage)
    {
      ae localae;
      int i;
      long l;
      switch (what)
      {
      default: 
        w.a.post(new Runnable()
        {
          public final void run()
          {
            StringBuilder localStringBuilder = new StringBuilder("Unhandled stats message.");
            localStringBuilder.append(paramMessagewhat);
            throw new AssertionError(localStringBuilder.toString());
          }
        });
        return;
      case 4: 
        localae = a;
        paramMessage = (Long)obj;
        l += 1;
        f += paramMessage.longValue();
        i = l;
        i = (f / i);
        return;
      case 3: 
        localae = a;
        l = arg1;
        n += 1;
        h += l;
        i = m;
        k = (h / i);
        return;
      case 2: 
        localae = a;
        l = arg1;
        m += 1;
        g += l;
        i = m;
        j = (g / i);
        return;
      case 1: 
        paramMessage = a;
        e += 1L;
        return;
      }
      paramMessage = a;
      d += 1L;
    }
  }
}

/* Location:
 * Qualified Name:     com.d.b.ae
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */