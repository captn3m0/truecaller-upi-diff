package com.d.b;

import android.graphics.Bitmap;
import android.net.Uri;
import java.io.IOException;
import java.io.InputStream;

public abstract interface j
{
  public abstract a a(Uri paramUri, int paramInt)
    throws IOException;
  
  public abstract void a();
  
  public static final class a
  {
    final InputStream a;
    final Bitmap b;
    final boolean c;
    final long d;
    
    public a(InputStream paramInputStream, boolean paramBoolean, long paramLong)
    {
      if (paramInputStream != null)
      {
        a = paramInputStream;
        b = null;
        c = paramBoolean;
        d = paramLong;
        return;
      }
      throw new IllegalArgumentException("Stream may not be null.");
    }
  }
  
  public static final class b
    extends IOException
  {
    final boolean a;
    final int b;
    
    public b(String paramString, int paramInt1, int paramInt2)
    {
      super();
      a = t.c(paramInt1);
      b = paramInt2;
    }
  }
}

/* Location:
 * Qualified Name:     com.d.b.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */