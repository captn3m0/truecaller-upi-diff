package com.d.b;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class ae$a
  extends Handler
{
  private final ae a;
  
  public ae$a(Looper paramLooper, ae paramae)
  {
    super(paramLooper);
    a = paramae;
  }
  
  public final void handleMessage(final Message paramMessage)
  {
    ae localae;
    int i;
    long l;
    switch (what)
    {
    default: 
      w.a.post(new Runnable()
      {
        public final void run()
        {
          StringBuilder localStringBuilder = new StringBuilder("Unhandled stats message.");
          localStringBuilder.append(paramMessagewhat);
          throw new AssertionError(localStringBuilder.toString());
        }
      });
      return;
    case 4: 
      localae = a;
      paramMessage = (Long)obj;
      l += 1;
      f += paramMessage.longValue();
      i = l;
      i = (f / i);
      return;
    case 3: 
      localae = a;
      l = arg1;
      n += 1;
      h += l;
      i = m;
      k = (h / i);
      return;
    case 2: 
      localae = a;
      l = arg1;
      m += 1;
      g += l;
      i = m;
      j = (g / i);
      return;
    case 1: 
      paramMessage = a;
      e += 1L;
      return;
    }
    paramMessage = a;
    d += 1L;
  }
}

/* Location:
 * Qualified Name:     com.d.b.ae.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */