package com.d.b;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory.Options;
import android.net.NetworkInfo;
import java.io.IOException;
import java.io.InputStream;

public abstract class ac
{
  static void a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, BitmapFactory.Options paramOptions, aa paramaa)
  {
    if ((paramInt4 <= paramInt2) && (paramInt3 <= paramInt1))
    {
      paramInt1 = 1;
    }
    else if (paramInt2 == 0)
    {
      paramInt1 = (int)Math.floor(paramInt3 / paramInt1);
    }
    else if (paramInt1 == 0)
    {
      paramInt1 = (int)Math.floor(paramInt4 / paramInt2);
    }
    else
    {
      paramInt2 = (int)Math.floor(paramInt4 / paramInt2);
      paramInt1 = (int)Math.floor(paramInt3 / paramInt1);
      if (k) {
        paramInt1 = Math.max(paramInt2, paramInt1);
      } else {
        paramInt1 = Math.min(paramInt2, paramInt1);
      }
    }
    inSampleSize = paramInt1;
    inJustDecodeBounds = false;
  }
  
  static void a(int paramInt1, int paramInt2, BitmapFactory.Options paramOptions, aa paramaa)
  {
    a(paramInt1, paramInt2, outWidth, outHeight, paramOptions, paramaa);
  }
  
  static boolean a(BitmapFactory.Options paramOptions)
  {
    return (paramOptions != null) && (inJustDecodeBounds);
  }
  
  static BitmapFactory.Options c(aa paramaa)
  {
    boolean bool = paramaa.c();
    int i;
    if (q != null) {
      i = 1;
    } else {
      i = 0;
    }
    Object localObject = null;
    if ((bool) || (i != 0))
    {
      BitmapFactory.Options localOptions = new BitmapFactory.Options();
      inJustDecodeBounds = bool;
      localObject = localOptions;
      if (i != 0)
      {
        inPreferredConfig = q;
        localObject = localOptions;
      }
    }
    return (BitmapFactory.Options)localObject;
  }
  
  int a()
  {
    return 0;
  }
  
  public abstract a a(aa paramaa, int paramInt)
    throws IOException;
  
  boolean a(NetworkInfo paramNetworkInfo)
  {
    return false;
  }
  
  public abstract boolean a(aa paramaa);
  
  boolean b()
  {
    return false;
  }
  
  public static final class a
  {
    final w.d a;
    final Bitmap b;
    final InputStream c;
    final int d;
    
    public a(Bitmap paramBitmap, w.d paramd)
    {
      this((Bitmap)al.a(paramBitmap, "bitmap == null"), null, paramd, 0);
    }
    
    a(Bitmap paramBitmap, InputStream paramInputStream, w.d paramd, int paramInt)
    {
      int j = 1;
      int i;
      if (paramBitmap != null) {
        i = 1;
      } else {
        i = 0;
      }
      if (paramInputStream == null) {
        j = 0;
      }
      if ((j ^ i) != 0)
      {
        b = paramBitmap;
        c = paramInputStream;
        a = ((w.d)al.a(paramd, "loadedFrom == null"));
        d = paramInt;
        return;
      }
      throw new AssertionError();
    }
    
    public a(InputStream paramInputStream, w.d paramd)
    {
      this(null, (InputStream)al.a(paramInputStream, "stream == null"), paramd, 0);
    }
  }
}

/* Location:
 * Qualified Name:     com.d.b.ac
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */