package com.d.b;

import android.graphics.Bitmap;
import java.io.InputStream;

public final class j$a
{
  final InputStream a;
  final Bitmap b;
  final boolean c;
  final long d;
  
  public j$a(InputStream paramInputStream, boolean paramBoolean, long paramLong)
  {
    if (paramInputStream != null)
    {
      a = paramInputStream;
      b = null;
      c = paramBoolean;
      d = paramLong;
      return;
    }
    throw new IllegalArgumentException("Stream may not be null.");
  }
}

/* Location:
 * Qualified Name:     com.d.b.j.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */