package com.d.b;

public final class af
{
  public final int a;
  public final int b;
  public final long c;
  public final long d;
  public final long e;
  public final long f;
  public final long g;
  public final long h;
  public final long i;
  public final long j;
  public final int k;
  public final int l;
  public final int m;
  public final long n;
  
  public af(int paramInt1, int paramInt2, long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5, long paramLong6, long paramLong7, long paramLong8, int paramInt3, int paramInt4, int paramInt5, long paramLong9)
  {
    a = paramInt1;
    b = paramInt2;
    c = paramLong1;
    d = paramLong2;
    e = paramLong3;
    f = paramLong4;
    g = paramLong5;
    h = paramLong6;
    i = paramLong7;
    j = paramLong8;
    k = paramInt3;
    l = paramInt4;
    m = paramInt5;
    n = paramLong9;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("StatsSnapshot{maxSize=");
    localStringBuilder.append(a);
    localStringBuilder.append(", size=");
    localStringBuilder.append(b);
    localStringBuilder.append(", cacheHits=");
    localStringBuilder.append(c);
    localStringBuilder.append(", cacheMisses=");
    localStringBuilder.append(d);
    localStringBuilder.append(", downloadCount=");
    localStringBuilder.append(k);
    localStringBuilder.append(", totalDownloadSize=");
    localStringBuilder.append(e);
    localStringBuilder.append(", averageDownloadSize=");
    localStringBuilder.append(h);
    localStringBuilder.append(", totalOriginalBitmapSize=");
    localStringBuilder.append(f);
    localStringBuilder.append(", totalTransformedBitmapSize=");
    localStringBuilder.append(g);
    localStringBuilder.append(", averageOriginalBitmapSize=");
    localStringBuilder.append(i);
    localStringBuilder.append(", averageTransformedBitmapSize=");
    localStringBuilder.append(j);
    localStringBuilder.append(", originalBitmapCount=");
    localStringBuilder.append(l);
    localStringBuilder.append(", transformedBitmapCount=");
    localStringBuilder.append(m);
    localStringBuilder.append(", timeStamp=");
    localStringBuilder.append(n);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.d.b.af
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */