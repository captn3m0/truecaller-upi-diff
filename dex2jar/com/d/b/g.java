package com.d.b;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

class g
  extends ac
{
  final Context a;
  
  g(Context paramContext)
  {
    a = paramContext;
  }
  
  public ac.a a(aa paramaa, int paramInt)
    throws IOException
  {
    return new ac.a(b(paramaa), w.d.b);
  }
  
  public boolean a(aa paramaa)
  {
    return "content".equals(d.getScheme());
  }
  
  final InputStream b(aa paramaa)
    throws FileNotFoundException
  {
    return a.getContentResolver().openInputStream(d);
  }
}

/* Location:
 * Qualified Name:     com.d.b.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */