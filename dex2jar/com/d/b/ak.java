package com.d.b;

import android.content.Context;
import android.net.Uri;
import android.net.http.HttpResponseCache;
import android.os.Build.VERSION;
import com.google.firebase.perf.network.FirebasePerfUrlConnection;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public final class ak
  implements j
{
  static volatile Object a;
  private static final Object b = new Object();
  private static final ThreadLocal<StringBuilder> c = new ThreadLocal() {};
  private final Context d;
  
  public ak(Context paramContext)
  {
    d = paramContext.getApplicationContext();
  }
  
  public final j.a a(Uri paramUri, int paramInt)
    throws IOException
  {
    Object localObject1;
    if (Build.VERSION.SDK_INT >= 14)
    {
      localObject1 = d;
      if (a != null) {}
    }
    try
    {
      synchronized (b)
      {
        if (a == null)
        {
          File localFile = al.b((Context)localObject1);
          HttpResponseCache localHttpResponseCache = HttpResponseCache.getInstalled();
          localObject1 = localHttpResponseCache;
          if (localHttpResponseCache == null) {
            localObject1 = HttpResponseCache.install(localFile, al.a(localFile));
          }
          a = localObject1;
        }
      }
    }
    catch (IOException localIOException)
    {
      HttpURLConnection localHttpURLConnection;
      int i;
      long l;
      boolean bool;
      for (;;) {}
    }
    localHttpURLConnection = (HttpURLConnection)FirebasePerfUrlConnection.instrument(new URL(paramUri.toString()).openConnection());
    localHttpURLConnection.setConnectTimeout(15000);
    localHttpURLConnection.setReadTimeout(20000);
    localHttpURLConnection.setUseCaches(true);
    if (paramInt != 0)
    {
      if (t.c(paramInt))
      {
        paramUri = "only-if-cached,max-age=2147483647";
      }
      else
      {
        paramUri = (StringBuilder)c.get();
        paramUri.setLength(0);
        if (!t.a(paramInt)) {
          paramUri.append("no-cache");
        }
        if (!t.b(paramInt))
        {
          if (paramUri.length() > 0) {
            paramUri.append(',');
          }
          paramUri.append("no-store");
        }
        paramUri = paramUri.toString();
      }
      localHttpURLConnection.setRequestProperty("Cache-Control", paramUri);
    }
    i = localHttpURLConnection.getResponseCode();
    if (i < 300)
    {
      l = localHttpURLConnection.getHeaderFieldInt("Content-Length", -1);
      bool = al.a(localHttpURLConnection.getHeaderField("X-Android-Response-Source"));
      return new j.a(localHttpURLConnection.getInputStream(), bool, l);
    }
    localHttpURLConnection.disconnect();
    paramUri = new StringBuilder();
    paramUri.append(i);
    paramUri.append(" ");
    paramUri.append(localHttpURLConnection.getResponseMessage());
    throw new j.b(paramUri.toString(), paramInt, i);
  }
  
  public final void a()
  {
    Object localObject;
    if ((Build.VERSION.SDK_INT >= 14) && (a != null)) {
      localObject = a;
    }
    try
    {
      ((HttpResponseCache)localObject).close();
      return;
    }
    catch (IOException localIOException) {}
    return;
  }
}

/* Location:
 * Qualified Name:     com.d.b.ak
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */