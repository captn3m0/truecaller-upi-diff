package com.d.b;

import android.content.ContentUris;
import android.content.UriMatcher;
import android.net.Uri;
import android.provider.ContactsContract.Contacts;
import android.text.TextUtils;
import com.truecaller.common.h.am;
import com.truecaller.content.TruecallerContract;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public final class aj
  extends ac
{
  private u a;
  private f b;
  private final UriMatcher c = new UriMatcher(-1);
  
  public aj()
  {
    c.addURI(TruecallerContract.a, "photo", 0);
  }
  
  public final ac.a a(aa paramaa, int paramInt)
    throws IOException
  {
    long l = am.h(d.getQueryParameter("pbid"));
    if (l > 0L)
    {
      localObject = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, l);
      localObject = paramaa.f().a((Uri)localObject).c();
      if (b.a((aa)localObject))
      {
        localObject = b.a((aa)localObject, paramInt);
        if (localObject != null) {
          return (ac.a)localObject;
        }
      }
    }
    Object localObject = d.getQueryParameter("tcphoto");
    if (!TextUtils.isEmpty((CharSequence)localObject))
    {
      paramaa = paramaa.f().a(Uri.parse((String)localObject)).c();
      if (a.a(paramaa))
      {
        paramaa = a.a(paramaa, paramInt);
        if (paramaa != null) {
          return paramaa;
        }
      }
    }
    return null;
  }
  
  public final void a(w paramw)
  {
    paramw = d.iterator();
    while (paramw.hasNext())
    {
      ac localac = (ac)paramw.next();
      if ((localac instanceof u)) {
        a = ((u)localac);
      } else if ((localac instanceof f)) {
        b = ((f)localac);
      }
    }
  }
  
  public final boolean a(aa paramaa)
  {
    return c.match(d) != -1;
  }
}

/* Location:
 * Qualified Name:     com.d.b.aj
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */