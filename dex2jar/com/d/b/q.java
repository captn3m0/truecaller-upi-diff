package com.d.b;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

final class q
  extends InputStream
{
  private final InputStream a;
  private long b;
  private long c;
  private long d;
  private long e = -1L;
  
  public q(InputStream paramInputStream)
  {
    this(paramInputStream, (byte)0);
  }
  
  private q(InputStream paramInputStream, byte paramByte)
  {
    Object localObject = paramInputStream;
    if (!paramInputStream.markSupported()) {
      localObject = new BufferedInputStream(paramInputStream, 4096);
    }
    a = ((InputStream)localObject);
  }
  
  private void a(long paramLong1, long paramLong2)
    throws IOException
  {
    while (paramLong1 < paramLong2)
    {
      long l2 = a.skip(paramLong2 - paramLong1);
      long l1 = l2;
      if (l2 == 0L)
      {
        if (read() == -1) {
          break;
        }
        l1 = 1L;
      }
      paramLong1 += l1;
    }
  }
  
  private void b(long paramLong)
  {
    try
    {
      if ((c < b) && (b <= d))
      {
        a.reset();
        a.mark((int)(paramLong - c));
        a(c, b);
      }
      else
      {
        c = b;
        a.mark((int)(paramLong - b));
      }
      d = paramLong;
      return;
    }
    catch (IOException localIOException)
    {
      throw new IllegalStateException("Unable to mark: ".concat(String.valueOf(localIOException)));
    }
  }
  
  public final long a(int paramInt)
  {
    long l = b + paramInt;
    if (d < l) {
      b(l);
    }
    return b;
  }
  
  public final void a(long paramLong)
    throws IOException
  {
    if ((b <= d) && (paramLong >= c))
    {
      a.reset();
      a(c, paramLong);
      b = paramLong;
      return;
    }
    throw new IOException("Cannot reset");
  }
  
  public final int available()
    throws IOException
  {
    return a.available();
  }
  
  public final void close()
    throws IOException
  {
    a.close();
  }
  
  public final void mark(int paramInt)
  {
    e = a(paramInt);
  }
  
  public final boolean markSupported()
  {
    return a.markSupported();
  }
  
  public final int read()
    throws IOException
  {
    int i = a.read();
    if (i != -1) {
      b += 1L;
    }
    return i;
  }
  
  public final int read(byte[] paramArrayOfByte)
    throws IOException
  {
    int i = a.read(paramArrayOfByte);
    if (i != -1) {
      b += i;
    }
    return i;
  }
  
  public final int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    paramInt1 = a.read(paramArrayOfByte, paramInt1, paramInt2);
    if (paramInt1 != -1) {
      b += paramInt1;
    }
    return paramInt1;
  }
  
  public final void reset()
    throws IOException
  {
    a(e);
  }
  
  public final long skip(long paramLong)
    throws IOException
  {
    paramLong = a.skip(paramLong);
    b += paramLong;
    return paramLong;
  }
}

/* Location:
 * Qualified Name:     com.d.b.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */