package com.d.b;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.util.List;

final class w$1
  extends Handler
{
  w$1(Looper paramLooper)
  {
    super(paramLooper);
  }
  
  public final void handleMessage(Message paramMessage)
  {
    int j = what;
    if (j != 3)
    {
      int i = 0;
      Object localObject1;
      Object localObject2;
      Object localObject3;
      if (j != 8)
      {
        if (j == 13)
        {
          localObject1 = (List)obj;
          j = ((List)localObject1).size();
          while (i < j)
          {
            localObject2 = (a)((List)localObject1).get(i);
            localObject3 = a;
            paramMessage = null;
            if (s.a(e)) {
              paramMessage = ((w)localObject3).b(i);
            }
            if (paramMessage != null)
            {
              ((w)localObject3).a(paramMessage, w.d.a, (a)localObject2);
              if (n)
              {
                paramMessage = b.a();
                localObject2 = new StringBuilder("from ");
                ((StringBuilder)localObject2).append(w.d.a);
                al.a("Main", "completed", paramMessage, ((StringBuilder)localObject2).toString());
              }
            }
            else
            {
              ((w)localObject3).a((a)localObject2);
              if (n) {
                al.a("Main", "resumed", b.a());
              }
            }
            i += 1;
          }
          return;
        }
        localObject1 = new StringBuilder("Unknown handler message received: ");
        ((StringBuilder)localObject1).append(what);
        throw new AssertionError(((StringBuilder)localObject1).toString());
      }
      paramMessage = (List)obj;
      int n = paramMessage.size();
      i = 0;
      while (i < n)
      {
        Object localObject4 = (c)paramMessage.get(i);
        localObject1 = b;
        localObject2 = k;
        localObject3 = l;
        int m = 1;
        if ((localObject3 != null) && (!((List)localObject3).isEmpty())) {
          j = 1;
        } else {
          j = 0;
        }
        int k = m;
        if (localObject2 == null) {
          if (j != 0) {
            k = m;
          } else {
            k = 0;
          }
        }
        if (k != 0)
        {
          Bitmap localBitmap = m;
          localObject4 = o;
          if (localObject2 != null) {
            ((w)localObject1).a(localBitmap, (w.d)localObject4, (a)localObject2);
          }
          if (j != 0)
          {
            k = ((List)localObject3).size();
            j = 0;
            while (j < k)
            {
              ((w)localObject1).a(localBitmap, (w.d)localObject4, (a)((List)localObject3).get(j));
              j += 1;
            }
          }
        }
        i += 1;
      }
      return;
    }
    paramMessage = (a)obj;
    if (a.n) {
      al.a("Main", "canceled", b.a(), "target got garbage collected");
    }
    w.a(a, paramMessage.c());
  }
}

/* Location:
 * Qualified Name:     com.d.b.w.1
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */