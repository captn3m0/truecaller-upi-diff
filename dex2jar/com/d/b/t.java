package com.d.b;

public enum t
{
  final int d;
  
  private t(int paramInt)
  {
    d = paramInt;
  }
  
  public static boolean a(int paramInt)
  {
    return (paramInt & ad) == 0;
  }
  
  public static boolean b(int paramInt)
  {
    return (paramInt & bd) == 0;
  }
  
  public static boolean c(int paramInt)
  {
    return (paramInt & cd) != 0;
  }
}

/* Location:
 * Qualified Name:     com.d.b.t
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */