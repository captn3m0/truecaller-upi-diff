package com.d.b;

import android.content.Context;
import android.media.ExifInterface;
import android.net.Uri;
import java.io.IOException;
import java.io.InputStream;

final class l
  extends g
{
  l(Context paramContext)
  {
    super(paramContext);
  }
  
  public final ac.a a(aa paramaa, int paramInt)
    throws IOException
  {
    InputStream localInputStream = b(paramaa);
    w.d locald = w.d.b;
    paramInt = new ExifInterface(d.getPath()).getAttributeInt("Orientation", 1);
    if (paramInt != 3)
    {
      if (paramInt != 6)
      {
        if (paramInt != 8) {
          paramInt = 0;
        } else {
          paramInt = 270;
        }
      }
      else {
        paramInt = 90;
      }
    }
    else {
      paramInt = 180;
    }
    return new ac.a(null, localInputStream, locald, paramInt);
  }
  
  public final boolean a(aa paramaa)
  {
    return "file".equals(d.getScheme());
  }
}

/* Location:
 * Qualified Name:     com.d.b.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */