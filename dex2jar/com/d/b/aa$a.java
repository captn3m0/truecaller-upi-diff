package com.d.b;

import android.graphics.Bitmap.Config;
import android.net.Uri;
import java.util.ArrayList;
import java.util.List;

public final class aa$a
{
  public int a;
  public int b;
  boolean c;
  boolean d;
  public boolean e;
  List<ai> f;
  public Bitmap.Config g;
  public w.e h;
  private Uri i;
  private int j;
  private String k;
  private float l;
  private float m;
  private float n;
  private boolean o;
  
  aa$a(Uri paramUri, int paramInt, Bitmap.Config paramConfig)
  {
    i = paramUri;
    j = paramInt;
    g = paramConfig;
  }
  
  private aa$a(aa paramaa)
  {
    i = d;
    j = e;
    k = f;
    a = h;
    b = i;
    c = j;
    d = k;
    l = m;
    m = n;
    n = o;
    o = p;
    e = l;
    if (g != null) {
      f = new ArrayList(g);
    }
    g = q;
    h = r;
  }
  
  public final a a(int paramInt1, int paramInt2)
  {
    if (paramInt1 >= 0)
    {
      if (paramInt2 >= 0)
      {
        if ((paramInt2 == 0) && (paramInt1 == 0)) {
          throw new IllegalArgumentException("At least one dimension has to be positive number.");
        }
        a = paramInt1;
        b = paramInt2;
        return this;
      }
      throw new IllegalArgumentException("Height must be positive number or 0.");
    }
    throw new IllegalArgumentException("Width must be positive number or 0.");
  }
  
  public final a a(Uri paramUri)
  {
    if (paramUri != null)
    {
      i = paramUri;
      j = 0;
      return this;
    }
    throw new IllegalArgumentException("Image URI may not be null.");
  }
  
  public final boolean a()
  {
    return (i != null) || (j != 0);
  }
  
  final boolean b()
  {
    return (a != 0) || (b != 0);
  }
  
  public final aa c()
  {
    if ((d) && (c)) {
      throw new IllegalStateException("Center crop and center inside can not be used together.");
    }
    if ((c) && (a == 0) && (b == 0)) {
      throw new IllegalStateException("Center crop requires calling resize with positive width and height.");
    }
    if ((d) && (a == 0) && (b == 0)) {
      throw new IllegalStateException("Center inside requires calling resize with positive width and height.");
    }
    if (h == null) {
      h = w.e.b;
    }
    return new aa(i, j, k, f, a, b, c, d, e, l, m, n, o, g, h, (byte)0);
  }
}

/* Location:
 * Qualified Name:     com.d.b.aa.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */