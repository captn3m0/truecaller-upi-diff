package com.d.b;

import android.content.ContentUris;
import android.content.UriMatcher;
import android.net.Uri;
import android.provider.ContactsContract.Contacts;
import c.g.b.k;
import com.truecaller.common.h.am;
import com.truecaller.content.TruecallerContract;
import java.io.IOException;

public final class m
  extends ac
{
  public u a;
  public f b;
  private final UriMatcher c = new UriMatcher(-1);
  
  public m()
  {
    c.addURI(TruecallerContract.a(), "photo", 0);
  }
  
  public final ac.a a(aa paramaa, int paramInt)
    throws IOException
  {
    k.b(paramaa, "request");
    long l = am.h(d.getQueryParameter("pbid"));
    if (l > 0L)
    {
      localObject = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, l);
      aa localaa = paramaa.f().a((Uri)localObject).c();
      localObject = b;
      if (localObject != null)
      {
        if (!((f)localObject).a(localaa)) {
          localObject = null;
        }
        if (localObject != null)
        {
          localObject = ((f)localObject).a(localaa, paramInt);
          if (localObject != null) {
            return (ac.a)localObject;
          }
        }
      }
    }
    Object localObject = d.getQueryParameter("tcphoto");
    if (localObject == null) {
      return null;
    }
    int i;
    if (((CharSequence)localObject).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0)
    {
      localObject = paramaa.f().a(Uri.parse((String)localObject)).c();
      paramaa = a;
      if (paramaa != null)
      {
        if (!paramaa.a((aa)localObject)) {
          paramaa = null;
        }
        if (paramaa != null)
        {
          paramaa = paramaa.a((aa)localObject, paramInt);
          if (paramaa != null) {
            return paramaa;
          }
        }
      }
    }
    return null;
  }
  
  public final boolean a(aa paramaa)
  {
    k.b(paramaa, "data");
    return c.match(d) != -1;
  }
}

/* Location:
 * Qualified Name:     com.d.b.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */