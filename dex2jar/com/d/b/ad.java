package com.d.b;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import java.io.IOException;

final class ad
  extends ac
{
  private final Context a;
  
  ad(Context paramContext)
  {
    a = paramContext;
  }
  
  public final ac.a a(aa paramaa, int paramInt)
    throws IOException
  {
    Resources localResources = al.a(a, paramaa);
    paramInt = al.a(localResources, paramaa);
    BitmapFactory.Options localOptions = c(paramaa);
    if (a(localOptions))
    {
      BitmapFactory.decodeResource(localResources, paramInt, localOptions);
      a(h, i, localOptions, paramaa);
    }
    return new ac.a(BitmapFactory.decodeResource(localResources, paramInt, localOptions), w.d.b);
  }
  
  public final boolean a(aa paramaa)
  {
    if (e != 0) {
      return true;
    }
    return "android.resource".equals(d.getScheme());
  }
}

/* Location:
 * Qualified Name:     com.d.b.ad
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */