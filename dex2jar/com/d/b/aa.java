package com.d.b;

import android.graphics.Bitmap.Config;
import android.net.Uri;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public final class aa
{
  private static final long s = TimeUnit.SECONDS.toNanos(5L);
  int a;
  long b;
  int c;
  public final Uri d;
  public final int e;
  public final String f;
  public final List<ai> g;
  public final int h;
  public final int i;
  public final boolean j;
  public final boolean k;
  public final boolean l;
  public final float m;
  public final float n;
  public final float o;
  public final boolean p;
  public final Bitmap.Config q;
  public final w.e r;
  
  private aa(Uri paramUri, int paramInt1, String paramString, List<ai> paramList, int paramInt2, int paramInt3, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, float paramFloat1, float paramFloat2, float paramFloat3, boolean paramBoolean4, Bitmap.Config paramConfig, w.e parame)
  {
    d = paramUri;
    e = paramInt1;
    f = paramString;
    if (paramList == null) {
      g = null;
    } else {
      g = Collections.unmodifiableList(paramList);
    }
    h = paramInt2;
    i = paramInt3;
    j = paramBoolean1;
    k = paramBoolean2;
    l = paramBoolean3;
    m = paramFloat1;
    n = paramFloat2;
    o = paramFloat3;
    p = paramBoolean4;
    q = paramConfig;
    r = parame;
  }
  
  final String a()
  {
    long l1 = System.nanoTime() - b;
    if (l1 > s)
    {
      localStringBuilder = new StringBuilder();
      localStringBuilder.append(b());
      localStringBuilder.append('+');
      localStringBuilder.append(TimeUnit.NANOSECONDS.toSeconds(l1));
      localStringBuilder.append('s');
      return localStringBuilder.toString();
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(b());
    localStringBuilder.append('+');
    localStringBuilder.append(TimeUnit.NANOSECONDS.toMillis(l1));
    localStringBuilder.append("ms");
    return localStringBuilder.toString();
  }
  
  public final String b()
  {
    StringBuilder localStringBuilder = new StringBuilder("[R");
    localStringBuilder.append(a);
    localStringBuilder.append(']');
    return localStringBuilder.toString();
  }
  
  public final boolean c()
  {
    return (h != 0) || (i != 0);
  }
  
  final boolean d()
  {
    return (c()) || (m != 0.0F);
  }
  
  final boolean e()
  {
    return g != null;
  }
  
  public final a f()
  {
    return new a(this, (byte)0);
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("Request{");
    int i1 = e;
    if (i1 > 0) {
      localStringBuilder.append(i1);
    } else {
      localStringBuilder.append(d);
    }
    Object localObject = g;
    if ((localObject != null) && (!((List)localObject).isEmpty()))
    {
      localObject = g.iterator();
      while (((Iterator)localObject).hasNext())
      {
        ai localai = (ai)((Iterator)localObject).next();
        localStringBuilder.append(' ');
        localStringBuilder.append(localai.a());
      }
    }
    if (f != null)
    {
      localStringBuilder.append(" stableKey(");
      localStringBuilder.append(f);
      localStringBuilder.append(')');
    }
    if (h > 0)
    {
      localStringBuilder.append(" resize(");
      localStringBuilder.append(h);
      localStringBuilder.append(',');
      localStringBuilder.append(i);
      localStringBuilder.append(')');
    }
    if (j) {
      localStringBuilder.append(" centerCrop");
    }
    if (k) {
      localStringBuilder.append(" centerInside");
    }
    if (m != 0.0F)
    {
      localStringBuilder.append(" rotation(");
      localStringBuilder.append(m);
      if (p)
      {
        localStringBuilder.append(" @ ");
        localStringBuilder.append(n);
        localStringBuilder.append(',');
        localStringBuilder.append(o);
      }
      localStringBuilder.append(')');
    }
    if (q != null)
    {
      localStringBuilder.append(' ');
      localStringBuilder.append(q);
    }
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
  
  public static final class a
  {
    public int a;
    public int b;
    boolean c;
    boolean d;
    public boolean e;
    List<ai> f;
    public Bitmap.Config g;
    public w.e h;
    private Uri i;
    private int j;
    private String k;
    private float l;
    private float m;
    private float n;
    private boolean o;
    
    a(Uri paramUri, int paramInt, Bitmap.Config paramConfig)
    {
      i = paramUri;
      j = paramInt;
      g = paramConfig;
    }
    
    private a(aa paramaa)
    {
      i = d;
      j = e;
      k = f;
      a = h;
      b = i;
      c = j;
      d = k;
      l = m;
      m = n;
      n = o;
      o = p;
      e = l;
      if (g != null) {
        f = new ArrayList(g);
      }
      g = q;
      h = r;
    }
    
    public final a a(int paramInt1, int paramInt2)
    {
      if (paramInt1 >= 0)
      {
        if (paramInt2 >= 0)
        {
          if ((paramInt2 == 0) && (paramInt1 == 0)) {
            throw new IllegalArgumentException("At least one dimension has to be positive number.");
          }
          a = paramInt1;
          b = paramInt2;
          return this;
        }
        throw new IllegalArgumentException("Height must be positive number or 0.");
      }
      throw new IllegalArgumentException("Width must be positive number or 0.");
    }
    
    public final a a(Uri paramUri)
    {
      if (paramUri != null)
      {
        i = paramUri;
        j = 0;
        return this;
      }
      throw new IllegalArgumentException("Image URI may not be null.");
    }
    
    public final boolean a()
    {
      return (i != null) || (j != 0);
    }
    
    final boolean b()
    {
      return (a != 0) || (b != 0);
    }
    
    public final aa c()
    {
      if ((d) && (c)) {
        throw new IllegalStateException("Center crop and center inside can not be used together.");
      }
      if ((c) && (a == 0) && (b == 0)) {
        throw new IllegalStateException("Center crop requires calling resize with positive width and height.");
      }
      if ((d) && (a == 0) && (b == 0)) {
        throw new IllegalStateException("Center inside requires calling resize with positive width and height.");
      }
      if (h == null) {
        h = w.e.b;
      }
      return new aa(i, j, k, f, a, b, c, d, e, l, m, n, o, g, h, (byte)0);
    }
  }
}

/* Location:
 * Qualified Name:     com.d.b.aa
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */