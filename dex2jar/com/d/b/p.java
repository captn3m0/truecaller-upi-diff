package com.d.b;

import android.content.Context;
import android.graphics.Bitmap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;

public final class p
  implements d
{
  final LinkedHashMap<String, Bitmap> b;
  private final int c;
  private int d;
  private int e;
  private int f;
  private int g;
  private int h;
  
  public p(int paramInt)
  {
    if (paramInt > 0)
    {
      c = paramInt;
      b = new LinkedHashMap(0, 0.75F, true);
      return;
    }
    throw new IllegalArgumentException("Max size must be positive.");
  }
  
  public p(Context paramContext)
  {
    this(al.c(paramContext));
  }
  
  public final int a()
  {
    try
    {
      int i = d;
      return i;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final Bitmap a(String paramString)
  {
    if (paramString != null) {
      try
      {
        paramString = (Bitmap)b.get(paramString);
        if (paramString != null)
        {
          g += 1;
          return paramString;
        }
        h += 1;
        return null;
      }
      finally {}
    }
    throw new NullPointerException("key == null");
  }
  
  public final void a(int paramInt)
  {
    for (;;)
    {
      try
      {
        if ((d >= 0) && ((!b.isEmpty()) || (d == 0)))
        {
          if ((d > paramInt) && (!b.isEmpty()))
          {
            Object localObject3 = (Map.Entry)b.entrySet().iterator().next();
            localObject1 = (String)((Map.Entry)localObject3).getKey();
            localObject3 = (Bitmap)((Map.Entry)localObject3).getValue();
            b.remove(localObject1);
            d -= al.a((Bitmap)localObject3);
            f += 1;
            continue;
          }
          return;
        }
        Object localObject1 = new StringBuilder();
        ((StringBuilder)localObject1).append(getClass().getName());
        ((StringBuilder)localObject1).append(".sizeOf() is reporting inconsistent results!");
        throw new IllegalStateException(((StringBuilder)localObject1).toString());
      }
      finally {}
    }
  }
  
  public final void a(String paramString, Bitmap paramBitmap)
  {
    if ((paramString != null) && (paramBitmap != null)) {
      try
      {
        e += 1;
        d += al.a(paramBitmap);
        paramString = (Bitmap)b.put(paramString, paramBitmap);
        if (paramString != null) {
          d -= al.a(paramString);
        }
        a(c);
        return;
      }
      finally {}
    }
    throw new NullPointerException("key == null || bitmap == null");
  }
  
  public final int b()
  {
    try
    {
      int i = c;
      return i;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final void b(String paramString)
  {
    try
    {
      int j = paramString.length();
      Iterator localIterator = b.entrySet().iterator();
      for (int i = 0; localIterator.hasNext(); i = 1)
      {
        label23:
        Object localObject = (Map.Entry)localIterator.next();
        String str = (String)((Map.Entry)localObject).getKey();
        localObject = (Bitmap)((Map.Entry)localObject).getValue();
        int k = str.indexOf('\n');
        if ((k != j) || (!str.substring(0, k).equals(paramString))) {
          break label23;
        }
        localIterator.remove();
        d -= al.a((Bitmap)localObject);
      }
      if (i != 0) {
        a(c);
      }
      return;
    }
    finally {}
  }
  
  public final void c()
  {
    try
    {
      a(-1);
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.d.b.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */