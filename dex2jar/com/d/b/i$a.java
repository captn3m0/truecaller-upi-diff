package com.d.b;

import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class i$a
  extends Handler
{
  private final i a;
  
  public i$a(Looper paramLooper, i parami)
  {
    super(paramLooper);
    a = parami;
  }
  
  public final void handleMessage(final Message paramMessage)
  {
    int i = what;
    boolean bool = false;
    Object localObject1;
    Object localObject2;
    c localc;
    switch (i)
    {
    case 3: 
    case 8: 
    default: 
      w.a.post(new Runnable()
      {
        public final void run()
        {
          StringBuilder localStringBuilder = new StringBuilder("Unknown handler message received: ");
          localStringBuilder.append(paramMessagewhat);
          throw new AssertionError(localStringBuilder.toString());
        }
      });
      return;
    case 12: 
      paramMessage = obj;
      a.a(paramMessage);
      return;
    case 11: 
      paramMessage = obj;
      localObject1 = a;
      if (h.add(paramMessage))
      {
        localObject2 = e.values().iterator();
        while (((Iterator)localObject2).hasNext())
        {
          localc = (c)((Iterator)localObject2).next();
          bool = b.n;
          Object localObject3 = k;
          List localList = l;
          if ((localList != null) && (!localList.isEmpty())) {
            i = 1;
          } else {
            i = 0;
          }
          if ((localObject3 != null) || (i != 0))
          {
            StringBuilder localStringBuilder;
            if ((localObject3 != null) && (j.equals(paramMessage)))
            {
              localc.a((a)localObject3);
              g.put(((a)localObject3).c(), localObject3);
              if (bool)
              {
                localObject3 = b.a();
                localStringBuilder = new StringBuilder("because tag '");
                localStringBuilder.append(paramMessage);
                localStringBuilder.append("' was paused");
                al.a("Dispatcher", "paused", (String)localObject3, localStringBuilder.toString());
              }
            }
            if (i != 0)
            {
              i = localList.size() - 1;
              while (i >= 0)
              {
                localObject3 = (a)localList.get(i);
                if (j.equals(paramMessage))
                {
                  localc.a((a)localObject3);
                  g.put(((a)localObject3).c(), localObject3);
                  if (bool)
                  {
                    localObject3 = b.a();
                    localStringBuilder = new StringBuilder("because tag '");
                    localStringBuilder.append(paramMessage);
                    localStringBuilder.append("' was paused");
                    al.a("Dispatcher", "paused", (String)localObject3, localStringBuilder.toString());
                  }
                }
                i -= 1;
              }
            }
            if (localc.b())
            {
              ((Iterator)localObject2).remove();
              if (bool) {
                al.a("Dispatcher", "canceled", al.a(localc), "all actions paused");
              }
            }
          }
        }
      }
      return;
    case 10: 
      localObject1 = a;
      if (arg1 == 1) {
        bool = true;
      }
      p = bool;
      return;
    case 9: 
      paramMessage = (NetworkInfo)obj;
      a.a(paramMessage);
      return;
    case 7: 
      paramMessage = a;
      localObject1 = new ArrayList(m);
      m.clear();
      j.sendMessage(j.obtainMessage(8, localObject1));
      i.a((List)localObject1);
      return;
    case 6: 
      paramMessage = (c)obj;
      a.a(paramMessage, false);
      return;
    case 5: 
      paramMessage = (c)obj;
      a.c(paramMessage);
      return;
    case 4: 
      paramMessage = (c)obj;
      localObject1 = a;
      if (s.b(h)) {
        k.a(f, m);
      }
      e.remove(f);
      ((i)localObject1).d(paramMessage);
      if (b.n) {
        al.a("Dispatcher", "batched", al.a(paramMessage), "for completion");
      }
      return;
    case 2: 
      paramMessage = (a)obj;
      localObject1 = a;
      localObject2 = i;
      localc = (c)e.get(localObject2);
      if (localc != null)
      {
        localc.a(paramMessage);
        if (localc.b())
        {
          e.remove(localObject2);
          if (a.n) {
            al.a("Dispatcher", "canceled", b.a());
          }
        }
      }
      if (h.contains(j))
      {
        g.remove(paramMessage.c());
        if (a.n) {
          al.a("Dispatcher", "canceled", b.a(), "because paused request got canceled");
        }
      }
      paramMessage = (a)f.remove(paramMessage.c());
      if ((paramMessage != null) && (a.n)) {
        al.a("Dispatcher", "canceled", b.a(), "from replaying");
      }
      return;
    }
    paramMessage = (a)obj;
    a.a(paramMessage, true);
  }
}

/* Location:
 * Qualified Name:     com.d.b.i.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */