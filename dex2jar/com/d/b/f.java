package com.d.b;

import android.content.ContentResolver;
import android.content.Context;
import android.content.UriMatcher;
import android.net.Uri;
import android.os.Build.VERSION;
import android.provider.ContactsContract.Contacts;
import java.io.IOException;

public final class f
  extends ac
{
  private static final UriMatcher a;
  private final Context b;
  
  static
  {
    UriMatcher localUriMatcher = new UriMatcher(-1);
    a = localUriMatcher;
    localUriMatcher.addURI("com.android.contacts", "contacts/lookup/*/#", 1);
    a.addURI("com.android.contacts", "contacts/lookup/*", 1);
    a.addURI("com.android.contacts", "contacts/#/photo", 2);
    a.addURI("com.android.contacts", "contacts/#", 3);
    a.addURI("com.android.contacts", "display_photo/#", 4);
  }
  
  f(Context paramContext)
  {
    b = paramContext;
  }
  
  public final ac.a a(aa paramaa, int paramInt)
    throws IOException
  {
    ContentResolver localContentResolver = b.getContentResolver();
    Uri localUri = d;
    paramaa = localUri;
    switch (a.match(localUri))
    {
    default: 
      throw new IllegalStateException("Invalid uri: ".concat(String.valueOf(localUri)));
    case 2: 
    case 4: 
      paramaa = localContentResolver.openInputStream(localUri);
      break;
    case 1: 
      localUri = ContactsContract.Contacts.lookupContact(localContentResolver, localUri);
      paramaa = localUri;
      if (localUri == null) {
        paramaa = null;
      }
      break;
    }
    if (Build.VERSION.SDK_INT < 14) {
      paramaa = ContactsContract.Contacts.openContactPhotoInputStream(localContentResolver, paramaa);
    } else {
      paramaa = ContactsContract.Contacts.openContactPhotoInputStream(localContentResolver, paramaa, true);
    }
    if (paramaa != null) {
      return new ac.a(paramaa, w.d.b);
    }
    return null;
  }
  
  public final boolean a(aa paramaa)
  {
    Uri localUri = d;
    return ("content".equals(localUri.getScheme())) && (ContactsContract.Contacts.CONTENT_URI.getHost().equals(localUri.getHost())) && (a.match(d) != -1);
  }
}

/* Location:
 * Qualified Name:     com.d.b.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */