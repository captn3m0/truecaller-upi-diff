package com.d.b;

import android.graphics.Bitmap;
import java.io.InputStream;

public final class ac$a
{
  final w.d a;
  final Bitmap b;
  final InputStream c;
  final int d;
  
  public ac$a(Bitmap paramBitmap, w.d paramd)
  {
    this((Bitmap)al.a(paramBitmap, "bitmap == null"), null, paramd, 0);
  }
  
  ac$a(Bitmap paramBitmap, InputStream paramInputStream, w.d paramd, int paramInt)
  {
    int j = 1;
    int i;
    if (paramBitmap != null) {
      i = 1;
    } else {
      i = 0;
    }
    if (paramInputStream == null) {
      j = 0;
    }
    if ((j ^ i) != 0)
    {
      b = paramBitmap;
      c = paramInputStream;
      a = ((w.d)al.a(paramd, "loadedFrom == null"));
      d = paramInt;
      return;
    }
    throw new AssertionError();
  }
  
  public ac$a(InputStream paramInputStream, w.d paramd)
  {
    this(null, (InputStream)al.a(paramInputStream, "stream == null"), paramd, 0);
  }
}

/* Location:
 * Qualified Name:     com.d.b.ac.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */