package com.d.b;

import android.content.Context;
import android.net.Uri;
import com.d.a.c;
import com.d.a.d;
import com.d.a.d.a;
import com.d.a.e;
import com.d.a.v.a;
import com.d.a.x;
import com.d.a.y;
import java.io.File;
import java.io.IOException;

public final class v
  implements j
{
  private final com.d.a.t a;
  
  public v(Context paramContext)
  {
    this(al.b(paramContext));
  }
  
  private v(com.d.a.t paramt)
  {
    a = paramt;
  }
  
  private v(File paramFile)
  {
    this(paramFile, al.a(paramFile));
  }
  
  private v(File paramFile, long paramLong)
  {
    this(localt);
    try
    {
      a.a(new c(paramFile, paramLong));
      return;
    }
    catch (IOException paramFile) {}
  }
  
  public final j.a a(Uri paramUri, int paramInt)
    throws IOException
  {
    if (paramInt != 0)
    {
      if (t.c(paramInt))
      {
        localObject = d.b;
      }
      else
      {
        localObject = new d.a();
        if (!t.a(paramInt)) {
          ((d.a)localObject).a();
        }
        if (!t.b(paramInt)) {
          ((d.a)localObject).b();
        }
        localObject = ((d.a)localObject).c();
      }
    }
    else {
      localObject = null;
    }
    paramUri = new v.a().a(paramUri.toString());
    if (localObject != null) {
      paramUri.a((d)localObject);
    }
    paramUri = a.a(paramUri.a()).a();
    int i = paramUri.a();
    if (i < 300)
    {
      boolean bool;
      if (paramUri.e() != null) {
        bool = true;
      } else {
        bool = false;
      }
      paramUri = paramUri.c();
      return new j.a(paramUri.c(), bool, paramUri.a());
    }
    paramUri.c().close();
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append(i);
    ((StringBuilder)localObject).append(" ");
    ((StringBuilder)localObject).append(paramUri.b());
    throw new j.b(((StringBuilder)localObject).toString(), paramInt, i);
  }
  
  public final void a()
  {
    c localc = a.a();
    if (localc != null) {}
    try
    {
      localc.a();
      return;
    }
    catch (IOException localIOException) {}
    return;
  }
}

/* Location:
 * Qualified Name:     com.d.b.v
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */