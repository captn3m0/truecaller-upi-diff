package com.d.b;

import android.graphics.Bitmap;

public abstract interface d
{
  public static final d a = new d()
  {
    public final int a()
    {
      return 0;
    }
    
    public final Bitmap a(String paramAnonymousString)
    {
      return null;
    }
    
    public final void a(String paramAnonymousString, Bitmap paramAnonymousBitmap) {}
    
    public final int b()
    {
      return 0;
    }
    
    public final void b(String paramAnonymousString) {}
    
    public final void c() {}
  };
  
  public abstract int a();
  
  public abstract Bitmap a(String paramString);
  
  public abstract void a(String paramString, Bitmap paramBitmap);
  
  public abstract int b();
  
  public abstract void b(String paramString);
  
  public abstract void c();
}

/* Location:
 * Qualified Name:     com.d.b.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */