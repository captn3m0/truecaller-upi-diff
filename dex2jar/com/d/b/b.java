package com.d.b;

import android.content.Context;
import android.content.res.AssetManager;
import android.net.Uri;
import java.io.IOException;
import java.util.List;

final class b
  extends ac
{
  private static final int a = 22;
  private final AssetManager b;
  
  public b(Context paramContext)
  {
    b = paramContext.getAssets();
  }
  
  public final ac.a a(aa paramaa, int paramInt)
    throws IOException
  {
    return new ac.a(b.open(d.toString().substring(a)), w.d.b);
  }
  
  public final boolean a(aa paramaa)
  {
    paramaa = d;
    return ("file".equals(paramaa.getScheme())) && (!paramaa.getPathSegments().isEmpty()) && ("android_asset".equals(paramaa.getPathSegments().get(0)));
  }
}

/* Location:
 * Qualified Name:     com.d.b.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */