package com.d.b;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.provider.MediaStore.Images.Thumbnails;
import android.provider.MediaStore.Video.Thumbnails;
import java.io.IOException;

final class r
  extends g
{
  private static final String[] b = { "orientation" };
  
  r(Context paramContext)
  {
    super(paramContext);
  }
  
  /* Error */
  private static int a(ContentResolver paramContentResolver, Uri paramUri)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: aconst_null
    //   4: astore_3
    //   5: aload_0
    //   6: aload_1
    //   7: getstatic 17	com/d/b/r:b	[Ljava/lang/String;
    //   10: aconst_null
    //   11: aconst_null
    //   12: aconst_null
    //   13: invokevirtual 31	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   16: astore_0
    //   17: aload_0
    //   18: ifnull +45 -> 63
    //   21: aload_0
    //   22: astore_3
    //   23: aload_0
    //   24: astore 4
    //   26: aload_0
    //   27: invokeinterface 37 1 0
    //   32: ifne +6 -> 38
    //   35: goto +28 -> 63
    //   38: aload_0
    //   39: astore_3
    //   40: aload_0
    //   41: astore 4
    //   43: aload_0
    //   44: iconst_0
    //   45: invokeinterface 41 2 0
    //   50: istore_2
    //   51: aload_0
    //   52: ifnull +9 -> 61
    //   55: aload_0
    //   56: invokeinterface 44 1 0
    //   61: iload_2
    //   62: ireturn
    //   63: aload_0
    //   64: ifnull +9 -> 73
    //   67: aload_0
    //   68: invokeinterface 44 1 0
    //   73: iconst_0
    //   74: ireturn
    //   75: astore_0
    //   76: aload_3
    //   77: ifnull +9 -> 86
    //   80: aload_3
    //   81: invokeinterface 44 1 0
    //   86: aload_0
    //   87: athrow
    //   88: aload 4
    //   90: ifnull +10 -> 100
    //   93: aload 4
    //   95: invokeinterface 44 1 0
    //   100: iconst_0
    //   101: ireturn
    //   102: astore_0
    //   103: goto -15 -> 88
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	106	0	paramContentResolver	ContentResolver
    //   0	106	1	paramUri	Uri
    //   50	12	2	i	int
    //   4	77	3	localContentResolver1	ContentResolver
    //   1	93	4	localContentResolver2	ContentResolver
    // Exception table:
    //   from	to	target	type
    //   5	17	75	finally
    //   26	35	75	finally
    //   43	51	75	finally
    //   5	17	102	java/lang/RuntimeException
    //   26	35	102	java/lang/RuntimeException
    //   43	51	102	java/lang/RuntimeException
  }
  
  public final ac.a a(aa paramaa, int paramInt)
    throws IOException
  {
    ContentResolver localContentResolver = a.getContentResolver();
    int i = a(localContentResolver, d);
    Object localObject = localContentResolver.getType(d);
    if ((localObject != null) && (((String)localObject).startsWith("video/"))) {
      paramInt = 1;
    } else {
      paramInt = 0;
    }
    if (paramaa.c())
    {
      int j = h;
      int k = i;
      if ((j <= ae) && (k <= af)) {
        localObject = a.a;
      } else if ((j <= be) && (k <= bf)) {
        localObject = a.b;
      } else {
        localObject = a.c;
      }
      if ((paramInt == 0) && (localObject == a.c)) {
        return new ac.a(null, b(paramaa), w.d.b, i);
      }
      long l = ContentUris.parseId(d);
      BitmapFactory.Options localOptions = c(paramaa);
      inJustDecodeBounds = true;
      a(h, i, e, f, localOptions, paramaa);
      if (paramInt != 0)
      {
        if (localObject == a.c) {
          paramInt = 1;
        } else {
          paramInt = d;
        }
        localObject = MediaStore.Video.Thumbnails.getThumbnail(localContentResolver, l, paramInt, localOptions);
      }
      else
      {
        localObject = MediaStore.Images.Thumbnails.getThumbnail(localContentResolver, l, d, localOptions);
      }
      if (localObject != null) {
        return new ac.a((Bitmap)localObject, null, w.d.b, i);
      }
    }
    return new ac.a(null, b(paramaa), w.d.b, i);
  }
  
  public final boolean a(aa paramaa)
  {
    paramaa = d;
    return ("content".equals(paramaa.getScheme())) && ("media".equals(paramaa.getAuthority()));
  }
  
  static enum a
  {
    final int d;
    final int e;
    final int f;
    
    private a(int paramInt1, int paramInt2, int paramInt3)
    {
      d = paramInt1;
      e = paramInt2;
      f = paramInt3;
    }
  }
}

/* Location:
 * Qualified Name:     com.d.b.r
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */