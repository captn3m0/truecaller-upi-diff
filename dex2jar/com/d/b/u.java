package com.d.b;

import android.graphics.Bitmap;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import java.io.IOException;
import java.io.InputStream;

public final class u
  extends ac
{
  private final j a;
  private final ae b;
  
  public u(j paramj, ae paramae)
  {
    a = paramj;
    b = paramae;
  }
  
  final int a()
  {
    return 2;
  }
  
  public final ac.a a(aa paramaa, int paramInt)
    throws IOException
  {
    j.a locala = a.a(d, c);
    if (c) {
      paramaa = w.d.b;
    } else {
      paramaa = w.d.c;
    }
    Object localObject = b;
    if (localObject != null) {
      return new ac.a((Bitmap)localObject, paramaa);
    }
    localObject = a;
    if (localObject == null) {
      return null;
    }
    if ((paramaa == w.d.b) && (d == 0L))
    {
      al.a((InputStream)localObject);
      throw new a("Received response with 0 content-length header.");
    }
    if ((paramaa == w.d.c) && (d > 0L))
    {
      ae localae = b;
      long l = d;
      c.sendMessage(c.obtainMessage(4, Long.valueOf(l)));
    }
    return new ac.a((InputStream)localObject, paramaa);
  }
  
  final boolean a(NetworkInfo paramNetworkInfo)
  {
    return (paramNetworkInfo == null) || (paramNetworkInfo.isConnected());
  }
  
  public final boolean a(aa paramaa)
  {
    paramaa = d.getScheme();
    return ("http".equals(paramaa)) || ("https".equals(paramaa));
  }
  
  final boolean b()
  {
    return true;
  }
  
  static final class a
    extends IOException
  {
    public a(String paramString)
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     com.d.b.u
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */