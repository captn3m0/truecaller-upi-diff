package com.d.b;

public abstract interface e
{
  public abstract void onError();
  
  public abstract void onSuccess();
  
  public static class a
    implements e
  {
    public void onError() {}
    
    public void onSuccess() {}
  }
}

/* Location:
 * Qualified Name:     com.d.b.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */