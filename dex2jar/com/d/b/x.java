package com.d.b;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.widget.ImageView;

final class x
  extends BitmapDrawable
{
  private static final Paint e = new Paint();
  Drawable a;
  long b;
  boolean c;
  int d = 255;
  private final boolean f;
  private final float g;
  private final w.d h;
  
  private x(Context paramContext, Bitmap paramBitmap, Drawable paramDrawable, w.d paramd, boolean paramBoolean1, boolean paramBoolean2)
  {
    super(paramContext.getResources(), paramBitmap);
    f = paramBoolean2;
    g = getResourcesgetDisplayMetricsdensity;
    h = paramd;
    int i;
    if ((paramd != w.d.a) && (!paramBoolean1)) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0)
    {
      a = paramDrawable;
      c = true;
      b = SystemClock.uptimeMillis();
    }
  }
  
  private static Path a(Point paramPoint, int paramInt)
  {
    Point localPoint1 = new Point(x + paramInt, y);
    Point localPoint2 = new Point(x, y + paramInt);
    Path localPath = new Path();
    localPath.moveTo(x, y);
    localPath.lineTo(x, y);
    localPath.lineTo(x, y);
    return localPath;
  }
  
  static void a(ImageView paramImageView, Context paramContext, Bitmap paramBitmap, w.d paramd, boolean paramBoolean1, boolean paramBoolean2)
  {
    Drawable localDrawable = paramImageView.getDrawable();
    if ((localDrawable instanceof AnimationDrawable)) {
      ((AnimationDrawable)localDrawable).stop();
    }
    paramImageView.setImageDrawable(new x(paramContext, paramBitmap, localDrawable, paramd, paramBoolean1, paramBoolean2));
  }
  
  static void a(ImageView paramImageView, Drawable paramDrawable)
  {
    paramImageView.setImageDrawable(paramDrawable);
    if ((paramImageView.getDrawable() instanceof AnimationDrawable)) {
      ((AnimationDrawable)paramImageView.getDrawable()).start();
    }
  }
  
  public final void draw(Canvas paramCanvas)
  {
    if (!c)
    {
      super.draw(paramCanvas);
    }
    else
    {
      float f1 = (float)(SystemClock.uptimeMillis() - b) / 200.0F;
      if (f1 >= 1.0F)
      {
        c = false;
        a = null;
        super.draw(paramCanvas);
      }
      else
      {
        Drawable localDrawable = a;
        if (localDrawable != null) {
          localDrawable.draw(paramCanvas);
        }
        super.setAlpha((int)(d * f1));
        super.draw(paramCanvas);
        super.setAlpha(d);
        if (Build.VERSION.SDK_INT <= 10) {
          invalidateSelf();
        }
      }
    }
    if (f)
    {
      e.setColor(-1);
      paramCanvas.drawPath(a(new Point(0, 0), (int)(g * 16.0F)), e);
      e.setColor(h.d);
      paramCanvas.drawPath(a(new Point(0, 0), (int)(g * 15.0F)), e);
    }
  }
  
  protected final void onBoundsChange(Rect paramRect)
  {
    Drawable localDrawable = a;
    if (localDrawable != null) {
      localDrawable.setBounds(paramRect);
    }
    super.onBoundsChange(paramRect);
  }
  
  public final void setAlpha(int paramInt)
  {
    d = paramInt;
    Drawable localDrawable = a;
    if (localDrawable != null) {
      localDrawable.setAlpha(paramInt);
    }
    super.setAlpha(paramInt);
  }
  
  public final void setColorFilter(ColorFilter paramColorFilter)
  {
    Drawable localDrawable = a;
    if (localDrawable != null) {
      localDrawable.setColorFilter(paramColorFilter);
    }
    super.setColorFilter(paramColorFilter);
  }
}

/* Location:
 * Qualified Name:     com.d.b.x
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */