package com.d.b;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import java.lang.ref.WeakReference;

final class o
  extends a<ImageView>
{
  e m;
  
  o(w paramw, ImageView paramImageView, aa paramaa, int paramInt1, int paramInt2, int paramInt3, Drawable paramDrawable, String paramString, Object paramObject, e parame, boolean paramBoolean)
  {
    super(paramw, paramImageView, paramaa, paramInt1, paramInt2, paramInt3, paramDrawable, paramString, paramObject, paramBoolean);
    m = parame;
  }
  
  public final void a()
  {
    Object localObject = (ImageView)c.get();
    if (localObject == null) {
      return;
    }
    if (g != 0) {
      ((ImageView)localObject).setImageResource(g);
    } else if (h != null) {
      ((ImageView)localObject).setImageDrawable(h);
    }
    localObject = m;
    if (localObject != null) {
      ((e)localObject).onError();
    }
  }
  
  public final void a(Bitmap paramBitmap, w.d paramd)
  {
    if (paramBitmap != null)
    {
      ImageView localImageView = (ImageView)c.get();
      if (localImageView == null) {
        return;
      }
      Context localContext = a.e;
      boolean bool = a.m;
      x.a(localImageView, localContext, paramBitmap, paramd, d, bool);
      paramBitmap = m;
      if (paramBitmap != null) {
        paramBitmap.onSuccess();
      }
      return;
    }
    throw new AssertionError(String.format("Attempted to complete action with no result!\n%s", new Object[] { this }));
  }
  
  final void b()
  {
    super.b();
    if (m != null) {
      m = null;
    }
  }
}

/* Location:
 * Qualified Name:     com.d.b.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */