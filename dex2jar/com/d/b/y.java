package com.d.b;

import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class y
  extends ThreadPoolExecutor
{
  y()
  {
    super(3, 3, 0L, TimeUnit.MILLISECONDS, new PriorityBlockingQueue(), new al.b());
  }
  
  final void a(int paramInt)
  {
    setCorePoolSize(paramInt);
    setMaximumPoolSize(paramInt);
  }
  
  public final Future<?> submit(Runnable paramRunnable)
  {
    paramRunnable = new a((c)paramRunnable);
    execute(paramRunnable);
    return paramRunnable;
  }
  
  static final class a
    extends FutureTask<c>
    implements Comparable<a>
  {
    private final c a;
    
    public a(c paramc)
    {
      super(null);
      a = paramc;
    }
  }
}

/* Location:
 * Qualified Name:     com.d.b.y
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */