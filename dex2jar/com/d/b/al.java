package com.d.b;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.StatFs;
import android.provider.Settings.System;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.ThreadFactory;

public final class al
{
  static final StringBuilder a = new StringBuilder();
  
  static int a(Resources paramResources, aa paramaa)
    throws FileNotFoundException
  {
    String str;
    List localList;
    if ((e == 0) && (d != null))
    {
      str = d.getAuthority();
      if (str != null)
      {
        localList = d.getPathSegments();
        if ((localList != null) && (!localList.isEmpty()) && (localList.size() != 1)) {}
      }
    }
    try
    {
      int i = Integer.parseInt((String)localList.get(0));
      return i;
    }
    catch (NumberFormatException paramResources)
    {
      for (;;) {}
    }
    paramResources = new StringBuilder("Last path segment is not a resource ID: ");
    paramResources.append(d);
    throw new FileNotFoundException(paramResources.toString());
    if (localList.size() == 2)
    {
      paramaa = (String)localList.get(0);
      return paramResources.getIdentifier((String)localList.get(1), paramaa, str);
    }
    paramResources = new StringBuilder("More than two path segments: ");
    paramResources.append(d);
    throw new FileNotFoundException(paramResources.toString());
    paramResources = new StringBuilder("No path segments: ");
    paramResources.append(d);
    throw new FileNotFoundException(paramResources.toString());
    paramResources = new StringBuilder("No package provided: ");
    paramResources.append(d);
    throw new FileNotFoundException(paramResources.toString());
    return e;
  }
  
  static int a(Bitmap paramBitmap)
  {
    int i;
    if (Build.VERSION.SDK_INT >= 12) {
      i = paramBitmap.getByteCount();
    } else {
      i = paramBitmap.getRowBytes() * paramBitmap.getHeight();
    }
    if (i >= 0) {
      return i;
    }
    throw new IllegalStateException("Negative size: ".concat(String.valueOf(paramBitmap)));
  }
  
  static long a(File paramFile)
  {
    try
    {
      paramFile = new StatFs(paramFile.getAbsolutePath());
      l = paramFile.getBlockCount() * paramFile.getBlockSize() / 50L;
    }
    catch (IllegalArgumentException paramFile)
    {
      long l;
      for (;;) {}
    }
    l = 5242880L;
    return Math.max(Math.min(l, 52428800L), 5242880L);
  }
  
  static Resources a(Context paramContext, aa paramaa)
    throws FileNotFoundException
  {
    String str;
    if ((e == 0) && (d != null))
    {
      str = d.getAuthority();
      if (str == null) {}
    }
    try
    {
      paramContext = paramContext.getPackageManager().getResourcesForApplication(str);
      return paramContext;
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      for (;;) {}
    }
    paramContext = new StringBuilder("Unable to obtain resources for package: ");
    paramContext.append(d);
    throw new FileNotFoundException(paramContext.toString());
    paramContext = new StringBuilder("No package provided: ");
    paramContext.append(d);
    throw new FileNotFoundException(paramContext.toString());
    return paramContext.getResources();
  }
  
  static j a(Context paramContext)
  {
    try
    {
      Class.forName("com.d.a.t");
      v localv = new v(paramContext);
      return localv;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      for (;;) {}
    }
    return new ak(paramContext);
  }
  
  static <T> T a(Context paramContext, String paramString)
  {
    return (T)paramContext.getSystemService(paramString);
  }
  
  static <T> T a(T paramT, String paramString)
  {
    if (paramT != null) {
      return paramT;
    }
    throw new NullPointerException(paramString);
  }
  
  static String a(aa paramaa)
  {
    paramaa = a(paramaa, a);
    a.setLength(0);
    return paramaa;
  }
  
  public static String a(aa paramaa, StringBuilder paramStringBuilder)
  {
    if (f != null)
    {
      paramStringBuilder.ensureCapacity(f.length() + 50);
      paramStringBuilder.append(f);
    }
    else if (d != null)
    {
      String str = d.toString();
      paramStringBuilder.ensureCapacity(str.length() + 50);
      paramStringBuilder.append(str);
    }
    else
    {
      paramStringBuilder.ensureCapacity(50);
      paramStringBuilder.append(e);
    }
    paramStringBuilder.append('\n');
    if (m != 0.0F)
    {
      paramStringBuilder.append("rotation:");
      paramStringBuilder.append(m);
      if (p)
      {
        paramStringBuilder.append('@');
        paramStringBuilder.append(n);
        paramStringBuilder.append('x');
        paramStringBuilder.append(o);
      }
      paramStringBuilder.append('\n');
    }
    if (paramaa.c())
    {
      paramStringBuilder.append("resize:");
      paramStringBuilder.append(h);
      paramStringBuilder.append('x');
      paramStringBuilder.append(paramaa.i);
      paramStringBuilder.append('\n');
    }
    if (paramaa.j) {
      paramStringBuilder.append("centerCrop\n");
    } else if (k) {
      paramStringBuilder.append("centerInside\n");
    }
    if (g != null)
    {
      int i = 0;
      int j = g.size();
      while (i < j)
      {
        paramStringBuilder.append(((ai)g.get(i)).a());
        paramStringBuilder.append('\n');
        i += 1;
      }
    }
    return paramStringBuilder.toString();
  }
  
  static String a(c paramc)
  {
    return a(paramc, "");
  }
  
  static String a(c paramc, String paramString)
  {
    paramString = new StringBuilder(paramString);
    a locala = k;
    if (locala != null) {
      paramString.append(b.a());
    }
    paramc = l;
    if (paramc != null)
    {
      int i = 0;
      int j = paramc.size();
      while (i < j)
      {
        if ((i > 0) || (locala != null)) {
          paramString.append(", ");
        }
        paramString.append(getb.a());
        i += 1;
      }
    }
    return paramString.toString();
  }
  
  static void a()
  {
    if (!c()) {
      return;
    }
    throw new IllegalStateException("Method call should not happen from the main thread.");
  }
  
  static void a(Looper paramLooper)
  {
    paramLooper = new Handler(paramLooper)
    {
      public final void handleMessage(Message paramAnonymousMessage)
      {
        sendMessageDelayed(obtainMessage(), 1000L);
      }
    };
    paramLooper.sendMessageDelayed(paramLooper.obtainMessage(), 1000L);
  }
  
  static void a(InputStream paramInputStream)
  {
    if (paramInputStream == null) {
      return;
    }
    try
    {
      paramInputStream.close();
      return;
    }
    catch (IOException paramInputStream) {}
    return;
  }
  
  static void a(String paramString1, String paramString2, String paramString3)
  {
    a(paramString1, paramString2, paramString3, "");
  }
  
  public static void a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    String.format("%1$-11s %2$-12s %3$s %4$s", new Object[] { paramString1, paramString2, paramString3, paramString4 });
  }
  
  static boolean a(String paramString)
  {
    if (paramString == null) {
      return false;
    }
    paramString = paramString.split(" ", 2);
    if ("CACHE".equals(paramString[0])) {
      return true;
    }
    if (paramString.length == 1) {
      return false;
    }
    try
    {
      if ("CONDITIONAL_CACHE".equals(paramString[0]))
      {
        int i = Integer.parseInt(paramString[1]);
        if (i == 304) {
          return true;
        }
      }
      return false;
    }
    catch (NumberFormatException paramString) {}
    return false;
  }
  
  static File b(Context paramContext)
  {
    paramContext = new File(paramContext.getApplicationContext().getCacheDir(), "picasso-cache");
    if (!paramContext.exists()) {
      paramContext.mkdirs();
    }
    return paramContext;
  }
  
  static void b()
  {
    if (c()) {
      return;
    }
    throw new IllegalStateException("Method call should happen from the main thread.");
  }
  
  static boolean b(Context paramContext, String paramString)
  {
    return paramContext.checkCallingOrSelfPermission(paramString) == 0;
  }
  
  static byte[] b(InputStream paramInputStream)
    throws IOException
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    byte[] arrayOfByte = new byte['က'];
    for (;;)
    {
      int i = paramInputStream.read(arrayOfByte);
      if (-1 == i) {
        break;
      }
      localByteArrayOutputStream.write(arrayOfByte, 0, i);
    }
    return localByteArrayOutputStream.toByteArray();
  }
  
  static int c(Context paramContext)
  {
    ActivityManager localActivityManager = (ActivityManager)paramContext.getSystemService("activity");
    int i;
    if ((getApplicationInfoflags & 0x100000) != 0) {
      i = 1;
    } else {
      i = 0;
    }
    int k = localActivityManager.getMemoryClass();
    int j = k;
    if (i != 0)
    {
      j = k;
      if (Build.VERSION.SDK_INT >= 11) {
        j = localActivityManager.getLargeMemoryClass();
      }
    }
    return j * 1048576 / 7;
  }
  
  private static boolean c()
  {
    return Looper.getMainLooper().getThread() == Thread.currentThread();
  }
  
  static boolean c(InputStream paramInputStream)
    throws IOException
  {
    byte[] arrayOfByte = new byte[12];
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (paramInputStream.read(arrayOfByte, 0, 12) == 12)
    {
      bool1 = bool2;
      if ("RIFF".equals(new String(arrayOfByte, 0, 4, "US-ASCII")))
      {
        bool1 = bool2;
        if ("WEBP".equals(new String(arrayOfByte, 8, 4, "US-ASCII"))) {
          bool1 = true;
        }
      }
    }
    return bool1;
  }
  
  static boolean d(Context paramContext)
  {
    paramContext = paramContext.getContentResolver();
    try
    {
      int i = Settings.System.getInt(paramContext, "airplane_mode_on", 0);
      return i != 0;
    }
    catch (NullPointerException paramContext) {}
    return false;
  }
  
  static final class a
    extends Thread
  {
    public a(Runnable paramRunnable)
    {
      super();
    }
    
    public final void run()
    {
      Process.setThreadPriority(10);
      super.run();
    }
  }
  
  static final class b
    implements ThreadFactory
  {
    public final Thread newThread(Runnable paramRunnable)
    {
      return new al.a(paramRunnable);
    }
  }
}

/* Location:
 * Qualified Name:     com.d.b.al
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */