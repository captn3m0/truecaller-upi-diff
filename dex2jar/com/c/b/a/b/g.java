package com.c.b.a.b;

import e.c;
import e.c.a;
import e.r;
import e.s;
import e.w;
import io.reactivex.b;
import io.reactivex.h;
import io.reactivex.k;
import io.reactivex.n;
import io.reactivex.o;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public final class g
  extends c.a
{
  private final n a = null;
  
  public final c<?> a(Type paramType, Annotation[] paramArrayOfAnnotation, s params)
  {
    paramArrayOfAnnotation = w.a(paramType);
    if (paramArrayOfAnnotation == b.class) {
      return new f(Void.class, a, false, true, false, false, false, true);
    }
    boolean bool3;
    if (paramArrayOfAnnotation == io.reactivex.d.class) {
      bool3 = true;
    } else {
      bool3 = false;
    }
    boolean bool4;
    if (paramArrayOfAnnotation == o.class) {
      bool4 = true;
    } else {
      bool4 = false;
    }
    boolean bool5;
    if (paramArrayOfAnnotation == h.class) {
      bool5 = true;
    } else {
      bool5 = false;
    }
    if ((paramArrayOfAnnotation != k.class) && (!bool3) && (!bool4) && (!bool5)) {
      return null;
    }
    if (!(paramType instanceof ParameterizedType))
    {
      if (!bool3)
      {
        if (bool4) {
          paramType = "Single";
        } else {
          paramType = "Observable";
        }
      }
      else {
        paramType = "Flowable";
      }
      paramArrayOfAnnotation = new StringBuilder();
      paramArrayOfAnnotation.append(paramType);
      paramArrayOfAnnotation.append(" return type must be parameterized as ");
      paramArrayOfAnnotation.append(paramType);
      paramArrayOfAnnotation.append("<Foo> or ");
      paramArrayOfAnnotation.append(paramType);
      paramArrayOfAnnotation.append("<? extends Foo>");
      throw new IllegalStateException(paramArrayOfAnnotation.toString());
    }
    paramType = w.a(0, (ParameterizedType)paramType);
    paramArrayOfAnnotation = w.a(paramType);
    boolean bool1;
    boolean bool2;
    if (paramArrayOfAnnotation == r.class)
    {
      if ((paramType instanceof ParameterizedType))
      {
        paramType = w.a(0, (ParameterizedType)paramType);
        bool1 = false;
        bool2 = false;
      }
      else
      {
        throw new IllegalStateException("Response must be parameterized as Response<Foo> or Response<? extends Foo>");
      }
    }
    else if (paramArrayOfAnnotation == d.class)
    {
      if ((paramType instanceof ParameterizedType))
      {
        paramType = w.a(0, (ParameterizedType)paramType);
        bool1 = true;
        bool2 = false;
      }
      else
      {
        throw new IllegalStateException("Result must be parameterized as Result<Foo> or Result<? extends Foo>");
      }
    }
    else
    {
      bool1 = false;
      bool2 = true;
    }
    return new f(paramType, a, bool1, bool2, bool3, bool4, bool5, false);
  }
}

/* Location:
 * Qualified Name:     com.c.b.a.b.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */