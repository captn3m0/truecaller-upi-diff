package com.c.b.a.b;

import e.r;
import io.reactivex.m;

final class e$a<R>
  implements m<r<R>>
{
  private final m<? super d<R>> a;
  
  e$a(m<? super d<R>> paramm)
  {
    a = paramm;
  }
  
  public final void a(io.reactivex.a.b paramb)
  {
    a.a(paramb);
  }
  
  public final void a(Throwable paramThrowable)
  {
    try
    {
      a.b_(d.a(paramThrowable));
      a.ak_();
      return;
    }
    catch (Throwable paramThrowable)
    {
      try
      {
        a.a(paramThrowable);
        return;
      }
      catch (Throwable localThrowable)
      {
        io.reactivex.b.b.a(localThrowable);
        io.reactivex.e.a.a(new io.reactivex.b.a(new Throwable[] { paramThrowable, localThrowable }));
      }
    }
  }
  
  public final void ak_()
  {
    a.ak_();
  }
}

/* Location:
 * Qualified Name:     com.c.b.a.b.e.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */