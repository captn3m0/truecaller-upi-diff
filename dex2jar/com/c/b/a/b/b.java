package com.c.b.a.b;

import e.r;
import io.reactivex.k;
import io.reactivex.m;

final class b<T>
  extends k<r<T>>
{
  private final e.b<T> a;
  
  b(e.b<T> paramb)
  {
    a = paramb;
  }
  
  public final void a(m<? super r<T>> paramm)
  {
    e.b localb = a.e();
    paramm.a(new a(localb));
    try
    {
      r localr = localb.c();
      if (!localb.a()) {
        paramm.b_(localr);
      }
      boolean bool = localb.a();
      if (!bool) {
        try
        {
          paramm.ak_();
          return;
        }
        catch (Throwable localThrowable1)
        {
          i = 1;
        }
      } else {
        return;
      }
    }
    catch (Throwable localThrowable2)
    {
      int i = 0;
      io.reactivex.b.b.a(localThrowable2);
      if (i != 0)
      {
        io.reactivex.e.a.a(localThrowable2);
        return;
      }
      if (!localb.a()) {
        try
        {
          paramm.a(localThrowable2);
          return;
        }
        catch (Throwable paramm)
        {
          io.reactivex.b.b.a(paramm);
          io.reactivex.e.a.a(new io.reactivex.b.a(new Throwable[] { localThrowable2, paramm }));
        }
      }
    }
  }
  
  static final class a
    implements io.reactivex.a.b
  {
    private final e.b<?> a;
    
    a(e.b<?> paramb)
    {
      a = paramb;
    }
    
    public final void a()
    {
      a.d();
    }
  }
}

/* Location:
 * Qualified Name:     com.c.b.a.b.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */