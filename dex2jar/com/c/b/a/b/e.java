package com.c.b.a.b;

import e.r;
import io.reactivex.k;
import io.reactivex.m;

final class e<T>
  extends k<d<T>>
{
  private final k<r<T>> a;
  
  e(k<r<T>> paramk)
  {
    a = paramk;
  }
  
  public final void a(m<? super d<T>> paramm)
  {
    a.b(new a(paramm));
  }
  
  static final class a<R>
    implements m<r<R>>
  {
    private final m<? super d<R>> a;
    
    a(m<? super d<R>> paramm)
    {
      a = paramm;
    }
    
    public final void a(io.reactivex.a.b paramb)
    {
      a.a(paramb);
    }
    
    public final void a(Throwable paramThrowable)
    {
      try
      {
        a.b_(d.a(paramThrowable));
        a.ak_();
        return;
      }
      catch (Throwable paramThrowable)
      {
        try
        {
          a.a(paramThrowable);
          return;
        }
        catch (Throwable localThrowable)
        {
          io.reactivex.b.b.a(localThrowable);
          io.reactivex.e.a.a(new io.reactivex.b.a(new Throwable[] { paramThrowable, localThrowable }));
        }
      }
    }
    
    public final void ak_()
    {
      a.ak_();
    }
  }
}

/* Location:
 * Qualified Name:     com.c.b.a.b.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */