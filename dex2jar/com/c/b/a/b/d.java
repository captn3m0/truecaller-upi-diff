package com.c.b.a.b;

import e.r;

public final class d<T>
{
  private final r<T> a;
  private final Throwable b;
  
  private d(r<T> paramr, Throwable paramThrowable)
  {
    a = paramr;
    b = paramThrowable;
  }
  
  public static <T> d<T> a(r<T> paramr)
  {
    if (paramr != null) {
      return new d(paramr, null);
    }
    throw new NullPointerException("response == null");
  }
  
  public static <T> d<T> a(Throwable paramThrowable)
  {
    if (paramThrowable != null) {
      return new d(null, paramThrowable);
    }
    throw new NullPointerException("error == null");
  }
}

/* Location:
 * Qualified Name:     com.c.b.a.b.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */