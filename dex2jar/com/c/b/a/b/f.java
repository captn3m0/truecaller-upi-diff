package com.c.b.a.b;

import io.reactivex.d;
import io.reactivex.d.e.a.o;
import io.reactivex.d.e.a.s;
import io.reactivex.d.e.a.t;
import io.reactivex.d.e.a.u;
import io.reactivex.d.e.a.v;
import io.reactivex.k;
import io.reactivex.l;
import io.reactivex.n;
import java.lang.reflect.Type;

final class f
  implements e.c<Object>
{
  private final Type a;
  private final n b;
  private final boolean c;
  private final boolean d;
  private final boolean e;
  private final boolean f;
  private final boolean g;
  private final boolean h;
  
  f(Type paramType, n paramn, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6)
  {
    a = paramType;
    b = paramn;
    c = paramBoolean1;
    d = paramBoolean2;
    e = paramBoolean3;
    f = paramBoolean4;
    g = paramBoolean5;
    h = paramBoolean6;
  }
  
  public final <R> Object a(e.b<R> paramb)
  {
    paramb = new b(paramb);
    if (c) {
      paramb = new e(paramb);
    } else if (d) {
      paramb = new a(paramb);
    }
    n localn = b;
    Object localObject = paramb;
    if (localn != null) {
      localObject = paramb.b(localn);
    }
    if (e)
    {
      paramb = io.reactivex.a.e;
      localObject = new o((k)localObject);
      switch (io.reactivex.k.1.a[paramb.ordinal()])
      {
      default: 
        int i = d.b();
        io.reactivex.d.b.b.a(i, "bufferSize");
        return io.reactivex.e.a.a(new s((d)localObject, i, io.reactivex.d.b.a.c));
      case 4: 
        return io.reactivex.e.a.a(new u((d)localObject));
      case 3: 
        return localObject;
      case 2: 
        return io.reactivex.e.a.a(new v((d)localObject));
      }
      return io.reactivex.e.a.a(new t((d)localObject));
    }
    if (f) {
      return io.reactivex.e.a.a(new io.reactivex.d.e.c.f((l)localObject));
    }
    if (g) {
      return io.reactivex.e.a.a(new io.reactivex.d.e.c.e((l)localObject));
    }
    if (h)
    {
      paramb = new io.reactivex.d.e.c.c((l)localObject);
      localObject = io.reactivex.e.a.n;
      if (localObject != null) {
        return (io.reactivex.b)io.reactivex.e.a.a((io.reactivex.c.e)localObject, paramb);
      }
      return paramb;
    }
    return localObject;
  }
  
  public final Type a()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.c.b.a.b.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */