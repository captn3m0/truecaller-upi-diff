package com.c.b.a.b;

import e.r;
import io.reactivex.a.b;
import io.reactivex.k;
import io.reactivex.m;

final class a<T>
  extends k<T>
{
  private final k<r<T>> a;
  
  a(k<r<T>> paramk)
  {
    a = paramk;
  }
  
  public final void a(m<? super T> paramm)
  {
    a.b(new a(paramm));
  }
  
  static final class a<R>
    implements m<r<R>>
  {
    private final m<? super R> a;
    private boolean b;
    
    a(m<? super R> paramm)
    {
      a = paramm;
    }
    
    public final void a(b paramb)
    {
      a.a(paramb);
    }
    
    public final void a(Throwable paramThrowable)
    {
      if (!b)
      {
        a.a(paramThrowable);
        return;
      }
      AssertionError localAssertionError = new AssertionError("This should never happen! Report as a bug with the full stacktrace.");
      localAssertionError.initCause(paramThrowable);
      io.reactivex.e.a.a(localAssertionError);
    }
    
    public final void ak_()
    {
      if (!b) {
        a.ak_();
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.c.b.a.b.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */