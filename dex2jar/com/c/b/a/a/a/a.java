package com.c.b.a.a.a;

import c.g.b.k;
import c.g.b.l;
import c.x;
import e.c;
import e.c.a;
import e.d;
import e.h;
import e.r;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import kotlinx.coroutines.ao;

public final class a
  extends c.a
{
  public static final b a = new b((byte)0);
  
  public static final a a()
  {
    return b.a();
  }
  
  public final c<?, ?> a(Type paramType, Annotation[] paramArrayOfAnnotation, e.s params)
  {
    k.b(paramType, "returnType");
    k.b(paramArrayOfAnnotation, "annotations");
    k.b(params, "retrofit");
    if ((k.a(ao.class, c.a.a(paramType)) ^ true)) {
      return null;
    }
    if ((paramType instanceof ParameterizedType))
    {
      paramType = c.a.a((ParameterizedType)paramType);
      if (k.a(c.a.a(paramType), r.class))
      {
        if ((paramType instanceof ParameterizedType))
        {
          paramType = c.a.a((ParameterizedType)paramType);
          k.a(paramType, "getParameterUpperBound(0, responseType)");
          return (c)new c(paramType);
        }
        throw ((Throwable)new IllegalStateException("Response must be parameterized as Response<Foo> or Response<out Foo>"));
      }
      k.a(paramType, "responseType");
      return (c)new a(paramType);
    }
    throw ((Throwable)new IllegalStateException("Deferred return type must be parameterized as Deferred<Foo> or Deferred<out Foo>"));
  }
  
  static final class a<T>
    implements c<T, ao<? extends T>>
  {
    private final Type a;
    
    public a(Type paramType)
    {
      a = paramType;
    }
    
    public final Type a()
    {
      return a;
    }
    
    static final class a
      extends l
      implements c.g.a.b<Throwable, x>
    {
      a(kotlinx.coroutines.s params, e.b paramb)
      {
        super();
      }
    }
    
    public static final class b
      implements d<T>
    {
      b(kotlinx.coroutines.s params) {}
      
      public final void onFailure(e.b<T> paramb, Throwable paramThrowable)
      {
        k.b(paramb, "call");
        k.b(paramThrowable, "t");
        a.a(paramThrowable);
      }
      
      public final void onResponse(e.b<T> paramb, r<T> paramr)
      {
        k.b(paramb, "call");
        k.b(paramr, "response");
        if (paramr.d())
        {
          paramb = a;
          paramr = paramr.e();
          if (paramr == null) {
            k.a();
          }
          paramb.a(paramr);
          return;
        }
        a.a((Throwable)new h(paramr));
      }
    }
  }
  
  public static final class b
  {
    public static a a()
    {
      return new a((byte)0);
    }
  }
  
  static final class c<T>
    implements c<T, ao<? extends r<T>>>
  {
    private final Type a;
    
    public c(Type paramType)
    {
      a = paramType;
    }
    
    public final Type a()
    {
      return a;
    }
    
    static final class a
      extends l
      implements c.g.a.b<Throwable, x>
    {
      a(kotlinx.coroutines.s params, e.b paramb)
      {
        super();
      }
    }
    
    public static final class b
      implements d<T>
    {
      b(kotlinx.coroutines.s params) {}
      
      public final void onFailure(e.b<T> paramb, Throwable paramThrowable)
      {
        k.b(paramb, "call");
        k.b(paramThrowable, "t");
        a.a(paramThrowable);
      }
      
      public final void onResponse(e.b<T> paramb, r<T> paramr)
      {
        k.b(paramb, "call");
        k.b(paramr, "response");
        a.a(paramr);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.c.b.a.a.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */