package com.c.b.a.a.a;

import c.g.b.k;
import c.g.b.l;
import c.x;
import e.c;
import e.d;
import e.h;
import e.r;
import java.lang.reflect.Type;
import kotlinx.coroutines.ao;
import kotlinx.coroutines.s;

final class a$a<T>
  implements c<T, ao<? extends T>>
{
  private final Type a;
  
  public a$a(Type paramType)
  {
    a = paramType;
  }
  
  public final Type a()
  {
    return a;
  }
  
  static final class a
    extends l
    implements c.g.a.b<Throwable, x>
  {
    a(s params, e.b paramb)
    {
      super();
    }
  }
  
  public static final class b
    implements d<T>
  {
    b(s params) {}
    
    public final void onFailure(e.b<T> paramb, Throwable paramThrowable)
    {
      k.b(paramb, "call");
      k.b(paramThrowable, "t");
      a.a(paramThrowable);
    }
    
    public final void onResponse(e.b<T> paramb, r<T> paramr)
    {
      k.b(paramb, "call");
      k.b(paramr, "response");
      if (paramr.d())
      {
        paramb = a;
        paramr = paramr.e();
        if (paramr == null) {
          k.a();
        }
        paramb.a(paramr);
        return;
      }
      a.a((Throwable)new h(paramr));
    }
  }
}

/* Location:
 * Qualified Name:     com.c.b.a.a.a.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */