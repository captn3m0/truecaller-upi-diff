package com.c.a;

import android.net.Uri;
import com.d.b.j;
import com.d.b.j.a;
import com.d.b.j.b;
import com.d.b.t;
import com.google.firebase.perf.network.FirebasePerfOkHttpClient;
import java.io.IOException;
import okhttp3.ab.a;
import okhttp3.ad;
import okhttp3.ae;
import okhttp3.c;
import okhttp3.d;
import okhttp3.d.a;
import okhttp3.e.a;
import okhttp3.y;

public final class a
  implements j
{
  private final e.a a;
  private final c b;
  
  public a(y paramy)
  {
    a = paramy;
    b = l;
  }
  
  public final j.a a(Uri paramUri, int paramInt)
    throws IOException
  {
    boolean bool = true;
    if (paramInt != 0)
    {
      if (t.c(paramInt))
      {
        localObject = d.b;
      }
      else
      {
        localObject = new d.a();
        if (!t.a(paramInt)) {
          a = true;
        }
        if (!t.b(paramInt)) {
          b = true;
        }
        localObject = ((d.a)localObject).a();
      }
    }
    else {
      localObject = null;
    }
    paramUri = new ab.a().a(paramUri.toString());
    if (localObject != null)
    {
      localObject = ((d)localObject).toString();
      if (((String)localObject).isEmpty()) {
        paramUri.b("Cache-Control");
      } else {
        paramUri.a("Cache-Control", (String)localObject);
      }
    }
    paramUri = FirebasePerfOkHttpClient.execute(a.a(paramUri.a()));
    int i = c;
    if (i < 300)
    {
      if (i == null) {
        bool = false;
      }
      paramUri = g;
      return new j.a(paramUri.d(), bool, paramUri.b());
    }
    g.close();
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append(i);
    ((StringBuilder)localObject).append(" ");
    ((StringBuilder)localObject).append(d);
    throw new j.b(((StringBuilder)localObject).toString(), paramInt, i);
  }
  
  public final void a()
  {
    c localc = b;
    if (localc != null) {}
    try
    {
      localc.close();
      return;
    }
    catch (IOException localIOException) {}
    return;
  }
}

/* Location:
 * Qualified Name:     com.c.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */