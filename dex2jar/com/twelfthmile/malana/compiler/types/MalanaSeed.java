package com.twelfthmile.malana.compiler.types;

public abstract interface MalanaSeed
{
  public abstract String getMalanaAddr();
  
  public abstract String getMalanaAirport();
  
  public abstract String getMalanaBank();
  
  public abstract String getMalanaLocation();
  
  public abstract String getMalanaOffers();
  
  public abstract String getMalanaSemantic();
  
  public abstract String getMalanaSyntax();
}

/* Location:
 * Qualified Name:     com.twelfthmile.malana.compiler.types.MalanaSeed
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */