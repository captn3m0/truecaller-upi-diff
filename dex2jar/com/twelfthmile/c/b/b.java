package com.twelfthmile.c.b;

import com.twelfthmile.c.c.c;
import com.twelfthmile.c.c.c.a;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

public abstract class b
{
  private HashMap<Long, b.b> a;
  List<Long> d;
  int e;
  
  protected b(int paramInt)
  {
    e = paramInt;
    d = new ArrayList();
    a = new HashMap();
  }
  
  protected abstract int a(String paramString1, long paramLong, String paramString2);
  
  protected final String a(String paramString)
  {
    int i = e;
    if (i == 1) {
      return aa.getJSONObject(paramString).getString("delta");
    }
    if (i == 2) {
      return ac.getJSONObject("rules").getJSONObject(paramString).getJSONObject("rule").getString("delta");
    }
    return ab.getJSONObject(paramString).getString("delta");
  }
  
  final void a()
  {
    a(d, a);
  }
  
  final void a(long paramLong1, long paramLong2, int paramInt)
  {
    if ((e == 2) && (b.a.a.a().a(paramLong1)) && (b.a.a.a().a(paramLong2))) {
      return;
    }
    if (a.containsKey(Long.valueOf(paramLong1)))
    {
      if (a.get(Long.valueOf(paramLong1))).b < paramInt) {
        a.get(Long.valueOf(paramLong1))).b = paramInt;
      }
      if (!a.get(Long.valueOf(paramLong1))).a.contains(Long.valueOf(paramLong2))) {
        a.get(Long.valueOf(paramLong1))).a.add(Long.valueOf(paramLong2));
      }
    }
    else
    {
      localObject = a.keySet().iterator();
      while (((Iterator)localObject).hasNext())
      {
        l = ((Long)((Iterator)localObject).next()).longValue();
        if ((a.get(Long.valueOf(l))).b == paramInt) && (a.get(Long.valueOf(l))).a.contains(Long.valueOf(paramLong1)))) {
          break label243;
        }
      }
      long l = Long.MAX_VALUE;
      label243:
      if (l != Long.MAX_VALUE)
      {
        if (!a.get(Long.valueOf(l))).a.contains(Long.valueOf(paramLong2))) {
          a.get(Long.valueOf(l))).a.add(Long.valueOf(paramLong2));
        }
      }
      else
      {
        a.put(Long.valueOf(paramLong1), new b.b(paramInt));
        a.get(Long.valueOf(paramLong1))).a.add(Long.valueOf(paramLong1));
        a.get(Long.valueOf(paramLong1))).a.add(Long.valueOf(paramLong2));
      }
    }
    Object localObject = b.a.a.a();
    Iterator localIterator = Arrays.asList(new Long[] { Long.valueOf(paramLong1), Long.valueOf(paramLong2) }).iterator();
    while (localIterator.hasNext())
    {
      paramLong1 = ((Long)localIterator.next()).longValue();
      a.put(Long.valueOf(paramLong1), Boolean.TRUE);
    }
  }
  
  protected abstract void a(List<Long> paramList, HashMap<Long, b.b> paramHashMap);
  
  protected abstract boolean a(long paramLong, String paramString);
}

/* Location:
 * Qualified Name:     com.twelfthmile.c.b.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */