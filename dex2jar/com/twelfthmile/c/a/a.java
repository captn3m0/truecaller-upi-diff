package com.twelfthmile.c.a;

import com.twelfthmile.c.b.b;
import com.twelfthmile.c.b.b.b;
import com.twelfthmile.c.c.d;
import java.util.HashMap;
import java.util.List;

public abstract class a
  extends b
{
  protected a()
  {
    super(0);
  }
  
  public final int a(String paramString1, long paramLong, String paramString2)
  {
    return 0;
  }
  
  public abstract void a(List<Long> paramList);
  
  public final void a(List<Long> paramList, HashMap<Long, b.b> paramHashMap)
  {
    a(paramList);
  }
  
  public final boolean a(long paramLong, String paramString)
  {
    float f = d.a(a(paramString));
    return (float)(paramLong / 3600000L) <= f;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.c.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */