package com.twelfthmile.c.a;

import com.twelfthmile.c.b.b;
import com.twelfthmile.c.b.b.b;
import com.twelfthmile.c.c.d;
import java.util.HashMap;
import java.util.List;

public abstract class c
  extends b
{
  protected c()
  {
    super(1);
  }
  
  public final int a(String paramString1, long paramLong, String paramString2)
  {
    float f = d.a(a(paramString2));
    switch (paramString1.hashCode())
    {
    default: 
      break;
    case 2001868029: 
      if (paramString1.equals("due_amt")) {
        i = 6;
      }
      break;
    case 1615403742: 
      if (paramString1.equals("alert_id")) {
        i = 4;
      }
      break;
    case -820075192: 
      if (paramString1.equals("vendor")) {
        i = 1;
      }
      break;
    case -868537476: 
      if (paramString1.equals("to_loc")) {
        i = 5;
      }
      break;
    case -934835129: 
      if (paramString1.equals("ref_id")) {
        i = 0;
      }
      break;
    case -983424250: 
      if (paramString1.equals("pnr_id")) {
        i = 3;
      }
      break;
    case -1374560575: 
      if (paramString1.equals("booking_id")) {
        i = 2;
      }
      break;
    }
    int i = -1;
    switch (i)
    {
    default: 
      return 0;
    }
    if ((float)(paramLong / 3600000L) <= f) {
      return 2;
    }
    return 0;
  }
  
  protected abstract void a(HashMap<Long, b.b> paramHashMap);
  
  public final void a(List<Long> paramList, HashMap<Long, b.b> paramHashMap)
  {
    a(paramHashMap);
  }
  
  public final boolean a(long paramLong, String paramString)
  {
    return false;
  }
}

/* Location:
 * Qualified Name:     com.twelfthmile.c.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */