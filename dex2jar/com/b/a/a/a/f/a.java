package com.b.a.a.a.f;

import android.app.Activity;
import android.view.View;
import com.b.a.a.a.d;
import com.b.a.a.a.f.a.j;
import java.util.UUID;

public abstract class a<T extends View>
{
  public String a = UUID.randomUUID().toString();
  
  public final void a()
  {
    d.b();
    com.b.a.a.a.f.a.a locala = d.a(a);
    if (locala != null) {
      locala.e();
    }
  }
  
  public final void a(T paramT)
  {
    d.b();
    com.b.a.a.a.f.a.a locala = d.a(a);
    if (locala != null) {
      locala.b(paramT);
    }
  }
  
  public final void a(T paramT, Activity paramActivity)
  {
    d.b();
    com.b.a.a.a.f.a.a locala = d.a(a);
    if (locala != null) {
      locala.a(paramT);
    }
    d.b();
    d.a(paramActivity);
  }
  
  public final com.b.a.a.a.c.a b()
  {
    d.b();
    Object localObject = d.a(a);
    if (localObject != null) {
      localObject = c;
    } else {
      localObject = null;
    }
    if (localObject != null) {
      return (com.b.a.a.a.c.a)localObject;
    }
    throw new IllegalStateException("The AVID ad session is not deferred. Please ensure you are only using AvidDeferredAdSessionListener for deferred AVID ad session.");
  }
  
  public final void b(View paramView)
  {
    d.b();
    com.b.a.a.a.f.a.a locala = d.a(a);
    if (locala != null) {
      g.a(paramView);
    }
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.f.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */