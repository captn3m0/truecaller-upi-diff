package com.b.a.a.a.f.a.a;

import android.webkit.WebView;
import com.b.a.a.a.f.a.b;
import java.lang.ref.WeakReference;

public final class d
  implements c.a
{
  private final b a;
  private final com.b.a.a.a.j.c b = new com.b.a.a.a.j.c(null);
  private final a c;
  private c d;
  
  public d(b paramb, a parama)
  {
    a = paramb;
    c = parama;
  }
  
  private void b()
  {
    c localc = d;
    if (localc != null)
    {
      a = null;
      d = null;
    }
  }
  
  public final void a()
  {
    c.a((WebView)b.a.get());
  }
  
  public final void a(WebView paramWebView)
  {
    if (b.a.get() == paramWebView) {
      return;
    }
    c.a(null);
    b();
    b.a(paramWebView);
    if (paramWebView != null)
    {
      d = new c(a);
      c localc = d;
      a = this;
      paramWebView.addJavascriptInterface(localc, "avid");
    }
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.f.a.a.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */