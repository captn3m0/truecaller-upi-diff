package com.b.a.a.a.f.a;

import android.content.Context;
import android.view.View;
import android.webkit.WebView;
import com.b.a.a.a.f.f;
import java.lang.ref.WeakReference;
import org.json.JSONObject;

public abstract class a<T extends View>
  implements com.b.a.a.a.f.a.a.a.a
{
  public final b a;
  public com.b.a.a.a.f.a.a.a b;
  public com.b.a.a.a.c.b c;
  public c d;
  public boolean e;
  public boolean f;
  public final j g;
  public a h;
  public double i;
  private com.b.a.a.a.f.a.a.d j;
  private com.b.a.a.a.j.b<T> k;
  
  public a(Context paramContext, String paramString, f paramf)
  {
    a = new b(paramContext, paramString, a().toString(), b().toString(), paramf);
    b = new com.b.a.a.a.f.a.a.a(a);
    paramContext = b;
    c = this;
    j = new com.b.a.a.a.f.a.a.d(a, paramContext);
    k = new com.b.a.a.a.j.b(null);
    e = (b ^ true);
    if (!e) {
      c = new com.b.a.a.a.c.b(this, b);
    }
    g = new j();
    m();
  }
  
  private void b(boolean paramBoolean)
  {
    f = paramBoolean;
    c localc = d;
    if (localc != null)
    {
      if (paramBoolean)
      {
        localc.c();
        return;
      }
      localc.d();
    }
  }
  
  private boolean c(View paramView)
  {
    return k.b(paramView);
  }
  
  private void l()
  {
    if (f) {
      b.b(com.b.a.a.a.g.b.a().toString());
    }
  }
  
  private void m()
  {
    i = com.b.a.a.a.g.c.a();
    h = a.a;
  }
  
  public abstract k a();
  
  public final void a(T paramT)
  {
    if (!c(paramT))
    {
      m();
      k.a(paramT);
      g();
      j();
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    if (f)
    {
      String str;
      if (paramBoolean) {
        str = "active";
      } else {
        str = "inactive";
      }
      b.c(str);
    }
  }
  
  public abstract i b();
  
  public final void b(T paramT)
  {
    if (c(paramT))
    {
      m();
      l();
      k.a(null);
      h();
      j();
    }
  }
  
  public final T c()
  {
    return (View)k.a.get();
  }
  
  public void d() {}
  
  public void e()
  {
    l();
    Object localObject = c;
    if (localObject != null) {
      ((com.b.a.a.a.c.b)localObject).a();
    }
    b.a(null);
    j.a(null);
    e = false;
    j();
    localObject = d;
    if (localObject != null) {
      ((c)localObject).a(this);
    }
  }
  
  public final void f()
  {
    j();
  }
  
  protected void g() {}
  
  protected void h() {}
  
  protected final void i()
  {
    j.a(k());
  }
  
  public final void j()
  {
    boolean bool;
    if ((b.a) && (e) && (!k.a())) {
      bool = true;
    } else {
      bool = false;
    }
    if (f != bool) {
      b(bool);
    }
  }
  
  public abstract WebView k();
  
  public static enum a
  {
    private a() {}
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.f.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */