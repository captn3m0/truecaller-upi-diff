package com.b.a.a.a.f.a.a;

import android.os.Handler;
import android.webkit.JavascriptInterface;
import com.b.a.a.a.f.a.b;
import org.json.JSONObject;

public final class c
{
  a a;
  private final b b;
  private final Handler c = new Handler();
  
  public c(b paramb)
  {
    b = paramb;
  }
  
  @JavascriptInterface
  public final String getAvidAdSessionContext()
  {
    c.post(new b());
    return b.b().toString();
  }
  
  public static abstract interface a
  {
    public abstract void a();
  }
  
  final class b
    implements Runnable
  {
    b() {}
    
    public final void run()
    {
      if (a != null)
      {
        a.a();
        a = null;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.f.a.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */