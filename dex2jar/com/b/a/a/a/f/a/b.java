package com.b.a.a.a.f.a;

import android.content.Context;
import com.b.a.a.a.f.f;
import org.json.JSONException;
import org.json.JSONObject;

public final class b
{
  public String a;
  private f b;
  private String c;
  private String d;
  
  public b(Context paramContext, String paramString1, String paramString2, String paramString3, f paramf)
  {
    com.b.a.a.a.b localb = com.b.a.a.a.b.a();
    if (a == null) {
      a = paramContext.getApplicationContext().getPackageName();
    }
    a = paramString1;
    b = paramf;
    c = paramString2;
    d = paramString3;
  }
  
  public final JSONObject a()
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("avidAdSessionId", a);
      localJSONObject.put("bundleIdentifier", aa);
      com.b.a.a.a.b.a();
      localJSONObject.put("partner", com.b.a.a.a.b.c());
      localJSONObject.put("partnerVersion", b.a);
      com.b.a.a.a.b.a();
      localJSONObject.put("avidLibraryVersion", com.b.a.a.a.b.b());
      localJSONObject.put("avidAdSessionType", c);
      localJSONObject.put("mediaType", d);
      localJSONObject.put("isDeferred", b.b);
      return localJSONObject;
    }
    catch (JSONException localJSONException)
    {
      localJSONException.printStackTrace();
    }
    return localJSONObject;
  }
  
  public final JSONObject b()
  {
    JSONObject localJSONObject = a();
    try
    {
      localJSONObject.put("avidApiLevel", "2");
      localJSONObject.put("mode", "stub");
      return localJSONObject;
    }
    catch (JSONException localJSONException)
    {
      localJSONException.printStackTrace();
    }
    return localJSONObject;
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.f.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */