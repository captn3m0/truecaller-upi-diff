package com.b.a.a.a.f.a.a;

import android.text.TextUtils;
import android.webkit.WebView;
import com.b.a.a.a.j.c;
import com.b.a.a.a.j.d;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONObject;

public final class a
{
  public boolean a;
  public boolean b;
  public a c;
  private final com.b.a.a.a.f.a.b d;
  private c e;
  private final ArrayList<b> f = new ArrayList();
  
  public a(com.b.a.a.a.f.a.b paramb)
  {
    d = paramb;
    e = new c(null);
  }
  
  private void b(String paramString, JSONObject paramJSONObject)
  {
    if (paramJSONObject != null) {
      paramJSONObject = paramJSONObject.toString();
    } else {
      paramJSONObject = null;
    }
    if (!TextUtils.isEmpty(paramJSONObject))
    {
      a(com.b.a.a.a.g.a.a(paramString, paramJSONObject));
      return;
    }
    a(com.b.a.a.a.g.a.c(paramString));
  }
  
  private void c()
  {
    a(com.b.a.a.a.g.a.d(d.a().toString()));
  }
  
  private void d()
  {
    a locala = c;
    if (locala != null) {
      locala.f();
    }
  }
  
  private void e()
  {
    Iterator localIterator = f.iterator();
    while (localIterator.hasNext())
    {
      b localb = (b)localIterator.next();
      b(a, b);
    }
    f.clear();
  }
  
  public final void a()
  {
    if (e.a()) {
      return;
    }
    a = true;
    e.a(com.b.a.a.a.a.a);
    c();
    b();
    e();
    d();
  }
  
  public final void a(WebView paramWebView)
  {
    if (e.a.get() == paramWebView) {
      return;
    }
    e.a(paramWebView);
    a = false;
    if (com.b.a.a.a.a.a()) {
      a();
    }
  }
  
  public final void a(String paramString)
  {
    e.b(paramString);
  }
  
  public final void a(String paramString, JSONObject paramJSONObject)
  {
    if (a)
    {
      b(paramString, paramJSONObject);
      return;
    }
    f.add(new b(paramString, paramJSONObject));
  }
  
  public final void b()
  {
    if ((a) && (b)) {
      a(com.b.a.a.a.g.a.e("publishReadyEventForDeferredAdSession()"));
    }
  }
  
  public final void b(String paramString)
  {
    a(com.b.a.a.a.g.a.a(paramString));
  }
  
  public final void c(String paramString)
  {
    a(com.b.a.a.a.g.a.b(paramString));
  }
  
  public static abstract interface a
  {
    public abstract void f();
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.f.a.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */