package com.b.a.a.a.f.a;

import android.content.Context;
import android.view.View;
import android.webkit.WebView;
import com.b.a.a.a.f.a.b.b;
import com.b.a.a.a.j.d;
import java.lang.ref.WeakReference;

public abstract class f
  extends a<View>
{
  public b j;
  private final WebView k;
  
  public f(Context paramContext, String paramString, com.b.a.a.a.f.f paramf)
  {
    super(paramContext, paramString, paramf);
    k = new WebView(paramContext.getApplicationContext());
    j = new b(k);
  }
  
  public final void d()
  {
    super.d();
    i();
    b localb = j;
    WebView localWebView = (WebView)a.a.get();
    if ((localWebView != null) && (b == 0))
    {
      b = 1;
      localWebView.loadData("<html><body></body></html>", "text/html", null);
    }
  }
  
  public final WebView k()
  {
    return k;
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.f.a.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */