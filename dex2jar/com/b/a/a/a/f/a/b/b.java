package com.b.a.a.a.f.a.b;

import android.webkit.WebSettings;
import android.webkit.WebView;
import java.util.ArrayList;
import java.util.Iterator;

public final class b
  implements a, c.a
{
  public final com.b.a.a.a.j.c a;
  public int b = 0;
  private final c c;
  private final ArrayList<String> d = new ArrayList();
  
  public b(WebView paramWebView)
  {
    a = new com.b.a.a.a.j.c(paramWebView);
    paramWebView.getSettings().setJavaScriptEnabled(true);
    c = new c();
    c localc = c;
    a = this;
    paramWebView.setWebViewClient(localc);
  }
  
  private void b(String paramString)
  {
    paramString = "(function () {\nvar script=document.createElement('script');script.setAttribute(\"type\",\"text/javascript\");script.setAttribute(\"src\",\"%SCRIPT_SRC%\");document.body.appendChild(script);\n})();".replace("%SCRIPT_SRC%", paramString);
    a.a(paramString);
  }
  
  public final void a()
  {
    b = 2;
    Iterator localIterator = d.iterator();
    while (localIterator.hasNext()) {
      b((String)localIterator.next());
    }
    d.clear();
  }
  
  public final void a(String paramString)
  {
    if (b == 2)
    {
      b(paramString);
      return;
    }
    d.add(paramString);
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.f.a.b.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */