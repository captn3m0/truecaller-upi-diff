package com.b.a.a.a.d;

import android.os.Build.VERSION;
import android.view.View;
import android.view.ViewGroup;
import com.b.a.a.a.g.b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONObject;

public final class c
  implements d
{
  private final int[] a = new int[2];
  
  public final JSONObject a(View paramView)
  {
    int i = paramView.getWidth();
    int j = paramView.getHeight();
    paramView.getLocationOnScreen(a);
    paramView = a;
    return b.a(paramView[0], paramView[1], i, j);
  }
  
  public final void a(View paramView, JSONObject paramJSONObject, d.a parama, boolean paramBoolean)
  {
    if (!(paramView instanceof ViewGroup)) {
      return;
    }
    ViewGroup localViewGroup = (ViewGroup)paramView;
    int k = 0;
    int j = 0;
    int i = k;
    if (paramBoolean) {
      if (Build.VERSION.SDK_INT < 21)
      {
        i = k;
      }
      else
      {
        HashMap localHashMap = new HashMap();
        i = j;
        Object localObject;
        while (i < localViewGroup.getChildCount())
        {
          View localView = localViewGroup.getChildAt(i);
          localObject = (ArrayList)localHashMap.get(Float.valueOf(localView.getZ()));
          paramView = (View)localObject;
          if (localObject == null)
          {
            paramView = new ArrayList();
            localHashMap.put(Float.valueOf(localView.getZ()), paramView);
          }
          paramView.add(localView);
          i += 1;
        }
        paramView = new ArrayList(localHashMap.keySet());
        Collections.sort(paramView);
        paramView = paramView.iterator();
        while (paramView.hasNext())
        {
          localObject = ((ArrayList)localHashMap.get((Float)paramView.next())).iterator();
          while (((Iterator)localObject).hasNext()) {
            parama.a((View)((Iterator)localObject).next(), this, paramJSONObject);
          }
        }
        return;
      }
    }
    while (i < localViewGroup.getChildCount())
    {
      parama.a(localViewGroup.getChildAt(i), this, paramJSONObject);
      i += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.d.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */