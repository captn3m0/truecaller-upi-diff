package com.b.a.a.a.d;

import android.view.View;
import org.json.JSONObject;

public abstract interface d
{
  public abstract JSONObject a(View paramView);
  
  public abstract void a(View paramView, JSONObject paramJSONObject, a parama, boolean paramBoolean);
  
  public static abstract interface a
  {
    public abstract void a(View paramView, d paramd, JSONObject paramJSONObject);
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.d.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */