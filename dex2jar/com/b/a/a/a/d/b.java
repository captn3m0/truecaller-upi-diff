package com.b.a.a.a.d;

import android.app.Activity;
import android.os.Build.VERSION;
import android.view.View;
import android.view.Window;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

public final class b
  implements d
{
  private final d a;
  
  public b(d paramd)
  {
    a = paramd;
  }
  
  public final JSONObject a(View paramView)
  {
    return com.b.a.a.a.g.b.a(0, 0, 0, 0);
  }
  
  public final void a(View paramView, JSONObject paramJSONObject, d.a parama, boolean paramBoolean)
  {
    paramView = com.b.a.a.a.a.a.a();
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = a.iterator();
    View localView = null;
    while (localIterator.hasNext())
    {
      paramView = (com.b.a.a.a.j.a)localIterator.next();
      Object localObject = (Activity)a.get();
      if (localObject == null) {
        paramBoolean = true;
      } else if (Build.VERSION.SDK_INT >= 17) {
        paramBoolean = ((Activity)localObject).isDestroyed();
      } else {
        paramBoolean = ((Activity)localObject).isFinishing();
      }
      if (paramBoolean)
      {
        localIterator.remove();
      }
      else
      {
        paramView = (Activity)a.get();
        if (paramView != null)
        {
          localObject = paramView.getWindow();
          if ((localObject != null) && (paramView.hasWindowFocus()))
          {
            paramView = ((Window)localObject).getDecorView();
            if ((paramView != null) && (paramView.isShown())) {
              break label172;
            }
          }
          else
          {
            paramView = null;
            break label172;
          }
        }
        paramView = null;
        label172:
        if (paramView != null) {
          localView = paramView;
        }
      }
    }
    if (localView != null) {
      localArrayList.add(localView);
    }
    paramView = localArrayList.iterator();
    while (paramView.hasNext()) {
      parama.a((View)paramView.next(), a, paramJSONObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.d.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */