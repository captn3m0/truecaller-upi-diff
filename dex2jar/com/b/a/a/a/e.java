package com.b.a.a.a;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

public final class e
{
  private static e e = new e();
  Context a;
  boolean b;
  boolean c;
  a d;
  private BroadcastReceiver f;
  
  public static e a()
  {
    return e;
  }
  
  private void e()
  {
    f = new BroadcastReceiver()
    {
      public final void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
      {
        if (paramAnonymousIntent == null) {
          return;
        }
        if ("android.intent.action.SCREEN_OFF".equals(paramAnonymousIntent.getAction()))
        {
          e.a(e.this, true);
          return;
        }
        if ("android.intent.action.USER_PRESENT".equals(paramAnonymousIntent.getAction()))
        {
          e.a(e.this, false);
          return;
        }
        if ("android.intent.action.SCREEN_ON".equals(paramAnonymousIntent.getAction()))
        {
          paramAnonymousContext = (KeyguardManager)paramAnonymousContext.getSystemService("keyguard");
          if ((paramAnonymousContext != null) && (!paramAnonymousContext.inKeyguardRestrictedInputMode())) {
            e.a(e.this, false);
          }
        }
      }
    };
    IntentFilter localIntentFilter = new IntentFilter();
    localIntentFilter.addAction("android.intent.action.SCREEN_OFF");
    localIntentFilter.addAction("android.intent.action.SCREEN_ON");
    localIntentFilter.addAction("android.intent.action.USER_PRESENT");
    a.registerReceiver(f, localIntentFilter);
  }
  
  public final void a(Context paramContext)
  {
    c();
    a = paramContext;
    e();
  }
  
  public final boolean b()
  {
    return !c;
  }
  
  final void c()
  {
    Context localContext = a;
    if (localContext != null)
    {
      BroadcastReceiver localBroadcastReceiver = f;
      if (localBroadcastReceiver != null)
      {
        localContext.unregisterReceiver(localBroadcastReceiver);
        f = null;
      }
    }
  }
  
  final void d()
  {
    boolean bool = c;
    Iterator localIterator = aa.values().iterator();
    while (localIterator.hasNext()) {
      ((com.b.a.a.a.f.a.a)localIterator.next()).a(bool ^ true);
    }
  }
  
  public static abstract interface a
  {
    public abstract void a(boolean paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */