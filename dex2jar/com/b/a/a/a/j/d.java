package com.b.a.a.a.j;

import java.lang.ref.WeakReference;

public class d<T>
{
  public WeakReference<T> a;
  
  public d(T paramT)
  {
    a = new WeakReference(paramT);
  }
  
  public final void a(T paramT)
  {
    a = new WeakReference(paramT);
  }
  
  public final boolean a()
  {
    return a.get() == null;
  }
  
  public final boolean b(Object paramObject)
  {
    Object localObject = a.get();
    return (localObject != null) && (paramObject != null) && (localObject.equals(paramObject));
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.j.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */