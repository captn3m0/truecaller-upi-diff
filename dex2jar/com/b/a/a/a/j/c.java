package com.b.a.a.a.j;

import android.webkit.WebView;
import com.b.a.a.a.g.a;
import java.lang.ref.WeakReference;

public final class c
  extends b<WebView>
{
  public c(WebView paramWebView)
  {
    super(paramWebView);
  }
  
  public final void a(String paramString)
  {
    b(a.f(paramString));
  }
  
  public final void b(String paramString)
  {
    WebView localWebView = (WebView)a.get();
    if (localWebView != null) {
      localWebView.loadUrl(paramString);
    }
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.j.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */