package com.b.a.a.a.h;

import org.json.JSONException;
import org.json.JSONObject;

public final class b
  extends com.b.a.a.a.b.a
  implements a
{
  public b(com.b.a.a.a.f.a.a parama, com.b.a.a.a.f.a.a.a parama1)
  {
    super(parama, parama1);
  }
  
  private void a(String paramString, JSONObject paramJSONObject)
  {
    b();
    s();
    b.a(paramString, paramJSONObject);
  }
  
  private void s()
  {
    if (a.e) {
      return;
    }
    throw new IllegalStateException("The AVID ad session is not ready. Please ensure you have called recordReadyEvent for the deferred AVID ad session before recording any video event.");
  }
  
  public final void a(Integer paramInteger)
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("volume", paramInteger);
    }
    catch (JSONException paramInteger)
    {
      paramInteger.printStackTrace();
    }
    a("AdVolumeChange", localJSONObject);
  }
  
  public final void a(String paramString)
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("message", paramString);
    }
    catch (JSONException paramString)
    {
      paramString.printStackTrace();
    }
    a("AdError", localJSONObject);
  }
  
  public final void c()
  {
    a("AdLoaded", null);
  }
  
  public final void d()
  {
    a("AdVideoStart", null);
  }
  
  public final void e()
  {
    a("AdStopped", null);
  }
  
  public final void f()
  {
    a("AdVideoComplete", null);
  }
  
  public final void g()
  {
    a("AdClickThru", null);
  }
  
  public final void h()
  {
    a("AdVideoFirstQuartile", null);
  }
  
  public final void i()
  {
    a("AdVideoMidpoint", null);
  }
  
  public final void j()
  {
    a("AdVideoThirdQuartile", null);
  }
  
  public final void j_()
  {
    a("AdImpression", null);
  }
  
  public final void k()
  {
    a("AdPaused", null);
  }
  
  public final void k_()
  {
    a("AdStarted", null);
  }
  
  public final void l()
  {
    a("AdPlaying", null);
  }
  
  public final void m()
  {
    a("AdExpandedChange", null);
  }
  
  public final void n()
  {
    a("AdUserMinimize", null);
  }
  
  public final void o()
  {
    a("AdUserClose", null);
  }
  
  public final void p()
  {
    a("AdSkipped", null);
  }
  
  public final void q()
  {
    a("AdEnteredFullscreen", null);
  }
  
  public final void r()
  {
    a("AdExitedFullscreen", null);
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.h.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */