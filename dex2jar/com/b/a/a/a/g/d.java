package com.b.a.a.a.g;

import android.os.Build.VERSION;
import android.view.View;

public final class d
{
  public static boolean a(View paramView)
  {
    if (paramView.getVisibility() != 0) {
      return false;
    }
    if (Build.VERSION.SDK_INT >= 11) {
      return paramView.getAlpha() > 0.0D;
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.g.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */