package com.b.a.a.a.g;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class b
{
  static float a = getSystemgetDisplayMetricsdensity;
  private static String[] b = { "x", "y", "width", "height" };
  
  public static JSONObject a()
  {
    return a(a(0, 0, 0, 0), c.a());
  }
  
  public static JSONObject a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("x", paramInt1 / a);
      localJSONObject.put("y", paramInt2 / a);
      localJSONObject.put("width", paramInt3 / a);
      localJSONObject.put("height", paramInt4 / a);
      return localJSONObject;
    }
    catch (JSONException localJSONException)
    {
      for (;;) {}
    }
    TextUtils.isEmpty("Error with creating viewStateObject");
    return localJSONObject;
  }
  
  public static JSONObject a(JSONObject paramJSONObject, double paramDouble)
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      localJSONObject.put("timestamp", paramDouble);
      localJSONObject.put("rootView", paramJSONObject);
      return localJSONObject;
    }
    catch (JSONException paramJSONObject)
    {
      for (;;) {}
    }
    TextUtils.isEmpty("Error with creating treeJSONObject");
    return localJSONObject;
  }
  
  public static void a(Context paramContext)
  {
    if (paramContext != null) {
      a = getResourcesgetDisplayMetricsdensity;
    }
  }
  
  public static void a(JSONObject paramJSONObject)
  {
    JSONArray localJSONArray = paramJSONObject.optJSONArray("childViews");
    if (localJSONArray == null) {
      return;
    }
    int i1 = localJSONArray.length();
    int j = 0;
    int k = 0;
    int m;
    for (int i = 0; j < i1; i = m)
    {
      JSONObject localJSONObject = localJSONArray.optJSONObject(j);
      int n = k;
      m = i;
      if (localJSONObject != null)
      {
        n = localJSONObject.optInt("x");
        m = localJSONObject.optInt("y");
        int i3 = localJSONObject.optInt("width");
        int i2 = localJSONObject.optInt("height");
        n = Math.max(k, n + i3);
        m = Math.max(i, m + i2);
      }
      j += 1;
      k = n;
    }
    try
    {
      paramJSONObject.put("width", k);
      paramJSONObject.put("height", i);
      return;
    }
    catch (JSONException paramJSONObject)
    {
      paramJSONObject.printStackTrace();
    }
  }
  
  public static void a(JSONObject paramJSONObject, String paramString)
  {
    try
    {
      paramJSONObject.put("id", paramString);
      return;
    }
    catch (JSONException paramJSONObject)
    {
      for (;;) {}
    }
    TextUtils.isEmpty("Error with setting avid id");
  }
  
  public static void a(JSONObject paramJSONObject, List<String> paramList)
  {
    JSONArray localJSONArray = new JSONArray();
    paramList = paramList.iterator();
    while (paramList.hasNext()) {
      localJSONArray.put((String)paramList.next());
    }
    try
    {
      paramJSONObject.put("isFriendlyObstructionFor", localJSONArray);
      return;
    }
    catch (JSONException paramJSONObject)
    {
      for (;;) {}
    }
    TextUtils.isEmpty("Error with setting friendly obstruction");
  }
  
  public static void a(JSONObject paramJSONObject1, JSONObject paramJSONObject2)
  {
    try
    {
      JSONArray localJSONArray2 = paramJSONObject1.optJSONArray("childViews");
      JSONArray localJSONArray1 = localJSONArray2;
      if (localJSONArray2 == null)
      {
        localJSONArray1 = new JSONArray();
        paramJSONObject1.put("childViews", localJSONArray1);
      }
      localJSONArray1.put(paramJSONObject2);
      return;
    }
    catch (JSONException paramJSONObject1)
    {
      paramJSONObject1.printStackTrace();
    }
  }
  
  private static boolean a(JSONArray paramJSONArray1, JSONArray paramJSONArray2)
  {
    if ((paramJSONArray1 == null) && (paramJSONArray2 == null)) {
      return true;
    }
    if (((paramJSONArray1 == null) && (paramJSONArray2 != null)) || ((paramJSONArray1 != null) && (paramJSONArray2 == null))) {
      return false;
    }
    return paramJSONArray1.length() == paramJSONArray2.length();
  }
  
  public static boolean b(JSONObject paramJSONObject1, JSONObject paramJSONObject2)
  {
    if (paramJSONObject2 == null) {
      return false;
    }
    Object localObject1 = b;
    int j = localObject1.length;
    int i = 0;
    Object localObject2;
    while (i < j)
    {
      localObject2 = localObject1[i];
      if (paramJSONObject1.optDouble((String)localObject2) != paramJSONObject2.optDouble((String)localObject2))
      {
        i = 0;
        break label58;
      }
      i += 1;
    }
    i = 1;
    label58:
    if ((i != 0) && (paramJSONObject1.optString("id", "").equals(paramJSONObject2.optString("id", ""))))
    {
      localObject1 = paramJSONObject1.optJSONArray("isFriendlyObstructionFor");
      localObject2 = paramJSONObject2.optJSONArray("isFriendlyObstructionFor");
      if (!a((JSONArray)localObject1, (JSONArray)localObject2))
      {
        i = 0;
      }
      else
      {
        if (localObject1 != null)
        {
          i = 0;
          while (i < ((JSONArray)localObject1).length())
          {
            if (!((JSONArray)localObject1).optString(i, "").equals(((JSONArray)localObject2).optString(i, "")))
            {
              i = 0;
              break label167;
            }
            i += 1;
          }
        }
        i = 1;
      }
      label167:
      if (i != 0)
      {
        paramJSONObject1 = paramJSONObject1.optJSONArray("childViews");
        paramJSONObject2 = paramJSONObject2.optJSONArray("childViews");
        if (!a(paramJSONObject1, paramJSONObject2))
        {
          i = 0;
        }
        else
        {
          if (paramJSONObject1 != null)
          {
            i = 0;
            while (i < paramJSONObject1.length())
            {
              if (!b(paramJSONObject1.optJSONObject(i), paramJSONObject2.optJSONObject(i)))
              {
                i = 0;
                break label242;
              }
              i += 1;
            }
          }
          i = 1;
        }
        label242:
        if (i != 0) {
          return true;
        }
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.g.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */