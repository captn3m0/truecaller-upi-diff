package com.b.a.a.a;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public final class d
  implements c.a, e.a, com.b.a.a.a.e.b
{
  private static d a = new d();
  private static Context b;
  
  public static com.b.a.a.a.f.a.a a(String paramString)
  {
    return (com.b.a.a.a.f.a.a)aa.get(paramString);
  }
  
  public static void a(Activity paramActivity)
  {
    com.b.a.a.a.a.a locala1 = com.b.a.a.a.a.a.a();
    Iterator localIterator = a.iterator();
    while (localIterator.hasNext())
    {
      locala = (com.b.a.a.a.j.a)localIterator.next();
      if (locala.b(paramActivity)) {
        break label44;
      }
    }
    com.b.a.a.a.j.a locala = null;
    label44:
    if (locala == null) {
      a.add(new com.b.a.a.a.j.a(paramActivity));
    }
  }
  
  public static void a(com.b.a.a.a.f.a parama, com.b.a.a.a.f.a.a parama1)
  {
    com.b.a.a.a.e.a locala = com.b.a.a.a.e.a.a();
    b.put(a, parama);
    a.put(a, parama1);
    d = locala;
    if ((b.size() == 1) && (c != null)) {
      c.a(locala);
    }
  }
  
  public static d b()
  {
    return a;
  }
  
  private void c()
  {
    ad = this;
    Object localObject = e.a();
    b = true;
    ((e)localObject).d();
    if (e.a().b())
    {
      localObject = f.a();
      f.c();
      ((f)localObject).b();
    }
  }
  
  public final void a()
  {
    if ((ab.isEmpty() ^ true))
    {
      ac = null;
      Iterator localIterator = aa.values().iterator();
      while (localIterator.hasNext()) {
        nextb.a();
      }
      ac = this;
      if (com.b.a.a.a.e.a.a().b()) {
        c();
      }
    }
  }
  
  public final void a(Context paramContext)
  {
    if (b == null)
    {
      b = paramContext.getApplicationContext();
      e.a().a(b);
      ac = this;
      com.b.a.a.a.g.b.a(b);
    }
  }
  
  public final void a(com.b.a.a.a.e.a parama)
  {
    if (b.isEmpty()) {
      return;
    }
    if (!a.a())
    {
      aa = this;
      parama = c.a();
      b = b;
      c = new c.c(parama);
      parama.b();
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      f localf = f.a();
      f.c();
      localf.b();
      return;
    }
    f.a();
    f.d();
  }
  
  public final void b(com.b.a.a.a.e.a parama)
  {
    if ((parama.b()) && (a.a()))
    {
      c();
      return;
    }
    aa.clear();
    parama = f.a();
    f.d();
    a.clear();
    b.a();
    parama = e.a();
    parama.c();
    a = null;
    b = false;
    c = false;
    d = null;
    parama = c.a();
    if (c != null)
    {
      c.c localc = c;
      a.removeCallbacks(c.a(b));
      c = null;
    }
    a = null;
    b = null;
    b = null;
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */