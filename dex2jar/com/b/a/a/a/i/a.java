package com.b.a.a.a.i;

import android.view.View;
import com.b.a.a.a.f.a.j;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public final class a
{
  public final HashMap<View, String> a = new HashMap();
  public final HashMap<View, ArrayList<String>> b = new HashMap();
  public final HashSet<View> c = new HashSet();
  public final HashSet<String> d = new HashSet();
  public final HashSet<String> e = new HashSet();
  public boolean f;
  private final com.b.a.a.a.e.a g;
  
  public a(com.b.a.a.a.e.a parama)
  {
    g = parama;
  }
  
  private void a(View paramView, com.b.a.a.a.f.a.a parama)
  {
    ArrayList localArrayList2 = (ArrayList)b.get(paramView);
    ArrayList localArrayList1 = localArrayList2;
    if (localArrayList2 == null)
    {
      localArrayList1 = new ArrayList();
      b.put(paramView, localArrayList1);
    }
    localArrayList1.add(a.a);
  }
  
  private void a(com.b.a.a.a.f.a.a parama)
  {
    Iterator localIterator = g.a.iterator();
    while (localIterator.hasNext())
    {
      com.b.a.a.a.j.b localb = (com.b.a.a.a.j.b)localIterator.next();
      if (!localb.a()) {
        a((View)a.get(), parama);
      }
    }
  }
  
  private boolean a(View paramView)
  {
    if (!paramView.hasWindowFocus()) {
      return false;
    }
    HashSet localHashSet = new HashSet();
    while (paramView != null) {
      if (com.b.a.a.a.g.d.a(paramView))
      {
        localHashSet.add(paramView);
        paramView = paramView.getParent();
        if ((paramView instanceof View)) {
          paramView = (View)paramView;
        } else {
          paramView = null;
        }
      }
      else
      {
        return false;
      }
    }
    c.addAll(localHashSet);
    return true;
  }
  
  public final void a()
  {
    Iterator localIterator = g.a.values().iterator();
    while (localIterator.hasNext())
    {
      com.b.a.a.a.f.a.a locala = (com.b.a.a.a.f.a.a)localIterator.next();
      View localView = locala.c();
      if ((f) && (localView != null)) {
        if (a(localView))
        {
          d.add(a.a);
          a.put(localView, a.a);
          a(locala);
        }
        else
        {
          e.add(a.a);
        }
      }
    }
  }
  
  public final void b()
  {
    a.clear();
    b.clear();
    c.clear();
    d.clear();
    e.clear();
    f = false;
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.i.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */