package com.b.a.a.a.i.a;

import java.util.ArrayDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class c
  implements b.a
{
  private final BlockingQueue<Runnable> a = new LinkedBlockingQueue();
  private final ThreadPoolExecutor b = new ThreadPoolExecutor(1, 1, 1L, TimeUnit.SECONDS, a);
  private final ArrayDeque<b> c = new ArrayDeque();
  private b d = null;
  
  private void b()
  {
    d = ((b)c.poll());
    b localb = d;
    if (localb != null) {
      localb.a(b);
    }
  }
  
  public final void a()
  {
    d = null;
    b();
  }
  
  public final void a(b paramb)
  {
    e = this;
    c.add(paramb);
    if (d == null) {
      b();
    }
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.i.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */