package com.b.a.a.a.i.a;

import java.util.HashSet;
import org.json.JSONObject;

public abstract class a
  extends b
{
  protected final com.b.a.a.a.e.a a;
  protected final HashSet<String> b;
  protected final JSONObject c;
  protected final double d;
  
  public a(b.b paramb, com.b.a.a.a.e.a parama, HashSet<String> paramHashSet, JSONObject paramJSONObject, double paramDouble)
  {
    super(paramb);
    a = parama;
    b = new HashSet(paramHashSet);
    c = paramJSONObject;
    d = paramDouble;
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.i.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */