package com.b.a.a.a.i.a;

import android.os.AsyncTask;
import android.os.Build.VERSION;
import java.util.concurrent.ThreadPoolExecutor;
import org.json.JSONObject;

public abstract class b
  extends AsyncTask<Object, Void, String>
{
  a e;
  protected final b f;
  
  public b(b paramb)
  {
    f = paramb;
  }
  
  protected void a(String paramString)
  {
    paramString = e;
    if (paramString != null) {
      paramString.a();
    }
  }
  
  public final void a(ThreadPoolExecutor paramThreadPoolExecutor)
  {
    if (Build.VERSION.SDK_INT > 11)
    {
      executeOnExecutor(paramThreadPoolExecutor, new Object[0]);
      return;
    }
    execute(new Object[0]);
  }
  
  public static abstract interface a
  {
    public abstract void a();
  }
  
  public static abstract interface b
  {
    public abstract void a(JSONObject paramJSONObject);
    
    public abstract JSONObject b();
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.i.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */