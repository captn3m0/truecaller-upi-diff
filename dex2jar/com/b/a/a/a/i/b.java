package com.b.a.a.a.i;

import com.b.a.a.a.e.a;
import com.b.a.a.a.i.a.b.b;
import com.b.a.a.a.i.a.c;
import com.b.a.a.a.i.a.d;
import com.b.a.a.a.i.a.e;
import com.b.a.a.a.i.a.f;
import java.util.HashSet;
import org.json.JSONObject;

public final class b
  implements b.b
{
  private final a a;
  private JSONObject b;
  private final c c;
  
  public b(a parama, c paramc)
  {
    a = parama;
    c = paramc;
  }
  
  public final void a()
  {
    c.a(new d(this));
  }
  
  public final void a(JSONObject paramJSONObject)
  {
    b = paramJSONObject;
  }
  
  public final void a(JSONObject paramJSONObject, HashSet<String> paramHashSet, double paramDouble)
  {
    c.a(new f(this, a, paramHashSet, paramJSONObject, paramDouble));
  }
  
  public final JSONObject b()
  {
    return b;
  }
  
  public final void b(JSONObject paramJSONObject, HashSet<String> paramHashSet, double paramDouble)
  {
    c.a(new e(this, a, paramHashSet, paramJSONObject, paramDouble));
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.i.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */