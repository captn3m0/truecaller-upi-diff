package com.b.a.a.a;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Handler;

public final class c
  implements g.a
{
  private static c d = new c();
  a a;
  Context b;
  c c;
  private g e;
  private b f = new b();
  private final Runnable g = new Runnable()
  {
    public final void run()
    {
      if (c.b(c.this) != null)
      {
        NetworkInfo localNetworkInfo = ((ConnectivityManager)c.b(c.this).getSystemService("connectivity")).getActiveNetworkInfo();
        int i;
        if ((localNetworkInfo != null) && (localNetworkInfo.isConnectedOrConnecting())) {
          i = 1;
        } else {
          i = 0;
        }
        if (i != 0)
        {
          c.c(c.this);
          return;
        }
      }
      c.d(c.this);
    }
  };
  
  public static c a()
  {
    return d;
  }
  
  private void d()
  {
    c localc = c;
    if (localc != null) {
      a.postDelayed(b.g, 2000L);
    }
  }
  
  public final void a(String paramString)
  {
    e = null;
    a.a = paramString;
    paramString = a;
    if (paramString != null) {
      paramString.a();
    }
  }
  
  final void b()
  {
    if ((!a.a()) && (e == null))
    {
      e = new g();
      e.a = this;
      b localb = f;
      if (Build.VERSION.SDK_INT >= 11)
      {
        a.e.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[] { "https://mobile-static.adsafeprotected.com/avid-v2.js" });
        return;
      }
      a.e.execute(new String[] { "https://mobile-static.adsafeprotected.com/avid-v2.js" });
    }
  }
  
  public final void c()
  {
    e = null;
    d();
  }
  
  public static abstract interface a
  {
    public abstract void a();
  }
  
  public final class b
  {
    public b() {}
  }
  
  public final class c
  {
    Handler a = new Handler();
    
    public c() {}
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */