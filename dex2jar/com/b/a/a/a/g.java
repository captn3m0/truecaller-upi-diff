package com.b.a.a.a;

import android.os.AsyncTask;

public final class g
  extends AsyncTask<String, Void, String>
{
  a a;
  
  /* Error */
  private static String a(String... paramVarArgs)
  {
    // Byte code:
    //   0: aload_0
    //   1: iconst_0
    //   2: aaload
    //   3: astore_3
    //   4: aload_3
    //   5: invokestatic 25	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   8: ifeq +11 -> 19
    //   11: ldc 27
    //   13: invokestatic 25	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   16: pop
    //   17: aconst_null
    //   18: areturn
    //   19: new 29	java/net/URL
    //   22: dup
    //   23: aload_3
    //   24: invokespecial 32	java/net/URL:<init>	(Ljava/lang/String;)V
    //   27: invokevirtual 36	java/net/URL:openConnection	()Ljava/net/URLConnection;
    //   30: invokestatic 42	com/google/firebase/perf/network/FirebasePerfUrlConnection:instrument	(Ljava/lang/Object;)Ljava/lang/Object;
    //   33: checkcast 44	java/net/URLConnection
    //   36: astore_0
    //   37: aload_0
    //   38: invokevirtual 47	java/net/URLConnection:connect	()V
    //   41: new 49	java/io/BufferedInputStream
    //   44: dup
    //   45: aload_0
    //   46: invokevirtual 53	java/net/URLConnection:getInputStream	()Ljava/io/InputStream;
    //   49: invokespecial 56	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   52: astore_2
    //   53: aload_2
    //   54: astore_0
    //   55: new 58	java/io/StringWriter
    //   58: dup
    //   59: invokespecial 59	java/io/StringWriter:<init>	()V
    //   62: astore 4
    //   64: aload_2
    //   65: astore_0
    //   66: sipush 1024
    //   69: newarray <illegal type>
    //   71: astore 5
    //   73: aload_2
    //   74: astore_0
    //   75: aload_2
    //   76: aload 5
    //   78: invokevirtual 65	java/io/InputStream:read	([B)I
    //   81: istore_1
    //   82: iload_1
    //   83: iconst_m1
    //   84: if_icmpeq +24 -> 108
    //   87: aload_2
    //   88: astore_0
    //   89: aload 4
    //   91: new 67	java/lang/String
    //   94: dup
    //   95: aload 5
    //   97: iconst_0
    //   98: iload_1
    //   99: invokespecial 70	java/lang/String:<init>	([BII)V
    //   102: invokevirtual 75	java/io/Writer:write	(Ljava/lang/String;)V
    //   105: goto -32 -> 73
    //   108: aload_2
    //   109: astore_0
    //   110: aload 4
    //   112: invokevirtual 81	java/lang/Object:toString	()Ljava/lang/String;
    //   115: astore 4
    //   117: aload_2
    //   118: invokevirtual 84	java/io/InputStream:close	()V
    //   121: aload 4
    //   123: areturn
    //   124: ldc 86
    //   126: invokestatic 25	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   129: pop
    //   130: aconst_null
    //   131: areturn
    //   132: astore_3
    //   133: goto +12 -> 145
    //   136: astore_2
    //   137: aconst_null
    //   138: astore_0
    //   139: goto +124 -> 263
    //   142: astore_3
    //   143: aconst_null
    //   144: astore_2
    //   145: aload_2
    //   146: astore_0
    //   147: new 88	java/lang/StringBuilder
    //   150: dup
    //   151: ldc 90
    //   153: invokespecial 91	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   156: astore 4
    //   158: aload_2
    //   159: astore_0
    //   160: aload 4
    //   162: aload_3
    //   163: invokevirtual 94	java/io/IOException:getLocalizedMessage	()Ljava/lang/String;
    //   166: invokevirtual 98	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   169: pop
    //   170: aload_2
    //   171: astore_0
    //   172: aload 4
    //   174: invokevirtual 99	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   177: invokestatic 25	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   180: pop
    //   181: aload_2
    //   182: ifnull +15 -> 197
    //   185: aload_2
    //   186: invokevirtual 84	java/io/InputStream:close	()V
    //   189: aconst_null
    //   190: areturn
    //   191: ldc 86
    //   193: invokestatic 25	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   196: pop
    //   197: aconst_null
    //   198: areturn
    //   199: aconst_null
    //   200: astore_2
    //   201: aload_2
    //   202: astore_0
    //   203: new 88	java/lang/StringBuilder
    //   206: dup
    //   207: ldc 101
    //   209: invokespecial 91	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   212: astore 4
    //   214: aload_2
    //   215: astore_0
    //   216: aload 4
    //   218: aload_3
    //   219: invokevirtual 98	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   222: pop
    //   223: aload_2
    //   224: astore_0
    //   225: aload 4
    //   227: ldc 103
    //   229: invokevirtual 98	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   232: pop
    //   233: aload_2
    //   234: astore_0
    //   235: aload 4
    //   237: invokevirtual 99	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   240: invokestatic 25	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   243: pop
    //   244: aload_2
    //   245: ifnull +15 -> 260
    //   248: aload_2
    //   249: invokevirtual 84	java/io/InputStream:close	()V
    //   252: aconst_null
    //   253: areturn
    //   254: ldc 86
    //   256: invokestatic 25	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   259: pop
    //   260: aconst_null
    //   261: areturn
    //   262: astore_2
    //   263: aload_0
    //   264: ifnull +18 -> 282
    //   267: aload_0
    //   268: invokevirtual 84	java/io/InputStream:close	()V
    //   271: goto +11 -> 282
    //   274: ldc 86
    //   276: invokestatic 25	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   279: pop
    //   280: aconst_null
    //   281: areturn
    //   282: aload_2
    //   283: athrow
    //   284: astore_0
    //   285: goto -86 -> 199
    //   288: astore_0
    //   289: goto -88 -> 201
    //   292: astore_0
    //   293: goto -169 -> 124
    //   296: astore_0
    //   297: goto -106 -> 191
    //   300: astore_0
    //   301: goto -47 -> 254
    //   304: astore_0
    //   305: goto -31 -> 274
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	308	0	paramVarArgs	String[]
    //   81	18	1	i	int
    //   52	66	2	localBufferedInputStream	java.io.BufferedInputStream
    //   136	1	2	localObject1	Object
    //   144	105	2	localObject2	Object
    //   262	21	2	localObject3	Object
    //   3	21	3	str	String
    //   132	1	3	localIOException1	java.io.IOException
    //   142	77	3	localIOException2	java.io.IOException
    //   62	174	4	localObject4	Object
    //   71	25	5	arrayOfByte	byte[]
    // Exception table:
    //   from	to	target	type
    //   55	64	132	java/io/IOException
    //   66	73	132	java/io/IOException
    //   75	82	132	java/io/IOException
    //   89	105	132	java/io/IOException
    //   110	117	132	java/io/IOException
    //   19	53	136	finally
    //   19	53	142	java/io/IOException
    //   55	64	262	finally
    //   66	73	262	finally
    //   75	82	262	finally
    //   89	105	262	finally
    //   110	117	262	finally
    //   147	158	262	finally
    //   160	170	262	finally
    //   172	181	262	finally
    //   203	214	262	finally
    //   216	223	262	finally
    //   225	233	262	finally
    //   235	244	262	finally
    //   19	53	284	java/net/MalformedURLException
    //   55	64	288	java/net/MalformedURLException
    //   66	73	288	java/net/MalformedURLException
    //   75	82	288	java/net/MalformedURLException
    //   89	105	288	java/net/MalformedURLException
    //   110	117	288	java/net/MalformedURLException
    //   117	121	292	java/io/IOException
    //   185	189	296	java/io/IOException
    //   248	252	300	java/io/IOException
    //   267	271	304	java/io/IOException
  }
  
  protected final void onCancelled()
  {
    a locala = a;
    if (locala != null) {
      locala.c();
    }
  }
  
  public static abstract interface a
  {
    public abstract void a(String paramString);
    
    public abstract void c();
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */