package com.b.a.a.a;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

final class c$1
  implements Runnable
{
  c$1(c paramc) {}
  
  public final void run()
  {
    if (c.b(a) != null)
    {
      NetworkInfo localNetworkInfo = ((ConnectivityManager)c.b(a).getSystemService("connectivity")).getActiveNetworkInfo();
      int i;
      if ((localNetworkInfo != null) && (localNetworkInfo.isConnectedOrConnecting())) {
        i = 1;
      } else {
        i = 0;
      }
      if (i != 0)
      {
        c.c(a);
        return;
      }
    }
    c.d(a);
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.c.1
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */