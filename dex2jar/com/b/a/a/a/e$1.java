package com.b.a.a.a;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class e$1
  extends BroadcastReceiver
{
  e$1(e parame) {}
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramIntent == null) {
      return;
    }
    if ("android.intent.action.SCREEN_OFF".equals(paramIntent.getAction()))
    {
      e.a(a, true);
      return;
    }
    if ("android.intent.action.USER_PRESENT".equals(paramIntent.getAction()))
    {
      e.a(a, false);
      return;
    }
    if ("android.intent.action.SCREEN_ON".equals(paramIntent.getAction()))
    {
      paramContext = (KeyguardManager)paramContext.getSystemService("keyguard");
      if ((paramContext != null) && (!paramContext.inKeyguardRestrictedInputMode())) {
        e.a(a, false);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.e.1
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */