package com.b.a.a.a;

import android.os.Handler;
import android.os.Message;
import android.view.View;
import com.b.a.a.a.d.d.a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

public final class f
  implements d.a
{
  private static f c = new f();
  private static a d;
  private static final Runnable j = new Runnable()
  {
    public final void run()
    {
      if (f.e() != null)
      {
        f.e().sendEmptyMessage(0);
        f.e().postDelayed(f.f(), 200L);
      }
    }
  };
  List<Object> a = new ArrayList();
  com.b.a.a.a.i.b b = new com.b.a.a.a.i.b(com.b.a.a.a.e.a.a(), new com.b.a.a.a.i.a.c());
  private int e;
  private com.b.a.a.a.d.a f = new com.b.a.a.a.d.a();
  private com.b.a.a.a.i.a g = new com.b.a.a.a.i.a(com.b.a.a.a.e.a.a());
  private double h;
  private double i;
  
  public static f a()
  {
    return c;
  }
  
  private void a(View paramView, com.b.a.a.a.d.d paramd, JSONObject paramJSONObject, com.b.a.a.a.i.c paramc)
  {
    boolean bool;
    if (paramc == com.b.a.a.a.i.c.a) {
      bool = true;
    } else {
      bool = false;
    }
    paramd.a(paramView, paramJSONObject, this, bool);
  }
  
  static void c()
  {
    if (d == null)
    {
      a locala = new a((byte)0);
      d = locala;
      locala.postDelayed(j, 200L);
    }
  }
  
  static void d()
  {
    a locala = d;
    if (locala != null)
    {
      locala.removeCallbacks(j);
      d = null;
    }
  }
  
  private void g()
  {
    e = 0;
    h = com.b.a.a.a.g.c.a();
  }
  
  private void h()
  {
    i = com.b.a.a.a.g.c.a();
    j();
  }
  
  private void i()
  {
    g.a();
    double d1 = com.b.a.a.a.g.c.a();
    com.b.a.a.a.d.b localb = f.a;
    JSONObject localJSONObject;
    if (g.e.size() > 0)
    {
      localJSONObject = localb.a(null);
      b.b(localJSONObject, g.e, d1);
    }
    if (g.d.size() > 0)
    {
      localJSONObject = localb.a(null);
      a(null, localb, localJSONObject, com.b.a.a.a.i.c.a);
      com.b.a.a.a.g.b.a(localJSONObject);
      b.a(localJSONObject, g.d, d1);
    }
    else
    {
      b.a();
    }
    g.b();
  }
  
  private void j()
  {
    if (a.size() > 0)
    {
      Iterator localIterator = a.iterator();
      while (localIterator.hasNext()) {
        localIterator.next();
      }
    }
  }
  
  public final void a(View paramView, com.b.a.a.a.d.d paramd, JSONObject paramJSONObject)
  {
    if (!com.b.a.a.a.g.d.a(paramView)) {
      return;
    }
    Object localObject1 = g;
    if (c.contains(paramView)) {
      localObject1 = com.b.a.a.a.i.c.a;
    } else if (f) {
      localObject1 = com.b.a.a.a.i.c.b;
    } else {
      localObject1 = com.b.a.a.a.i.c.c;
    }
    if (localObject1 == com.b.a.a.a.i.c.c) {
      return;
    }
    JSONObject localJSONObject = paramd.a(paramView);
    com.b.a.a.a.g.b.a(paramJSONObject, localJSONObject);
    com.b.a.a.a.i.a locala = g;
    int k = a.size();
    ArrayList localArrayList = null;
    Object localObject2;
    if (k == 0)
    {
      paramJSONObject = null;
    }
    else
    {
      localObject2 = (String)a.get(paramView);
      paramJSONObject = (JSONObject)localObject2;
      if (localObject2 != null)
      {
        a.remove(paramView);
        paramJSONObject = (JSONObject)localObject2;
      }
    }
    if (paramJSONObject != null)
    {
      com.b.a.a.a.g.b.a(localJSONObject, paramJSONObject);
      g.f = true;
      k = 1;
    }
    else
    {
      k = 0;
    }
    if (k == 0)
    {
      localObject2 = g;
      if (b.size() == 0)
      {
        paramJSONObject = localArrayList;
      }
      else
      {
        localArrayList = (ArrayList)b.get(paramView);
        paramJSONObject = localArrayList;
        if (localArrayList != null)
        {
          b.remove(paramView);
          Collections.sort(localArrayList);
          paramJSONObject = localArrayList;
        }
      }
      if (paramJSONObject != null) {
        com.b.a.a.a.g.b.a(localJSONObject, paramJSONObject);
      }
      a(paramView, paramd, localJSONObject, (com.b.a.a.a.i.c)localObject1);
    }
    e += 1;
  }
  
  final void b()
  {
    g();
    i();
    h();
  }
  
  static final class a
    extends Handler
  {
    public final void handleMessage(Message paramMessage)
    {
      super.handleMessage(paramMessage);
      f.a(f.a());
    }
  }
}

/* Location:
 * Qualified Name:     com.b.a.a.a.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */