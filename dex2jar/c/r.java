package c;

import c.g.a.a;
import c.g.b.k;
import java.io.Serializable;

final class r<T>
  implements f<T>, Serializable
{
  private a<? extends T> a;
  private volatile Object b;
  private final Object c;
  
  private r(a<? extends T> parama)
  {
    a = parama;
    b = v.a;
    c = this;
  }
  
  private final Object writeReplace()
  {
    return new d(b());
  }
  
  public final boolean a()
  {
    return b != v.a;
  }
  
  public final T b()
  {
    Object localObject1 = b;
    if (localObject1 != v.a) {
      return (T)localObject1;
    }
    synchronized (c)
    {
      localObject1 = b;
      if (localObject1 == v.a)
      {
        localObject1 = a;
        if (localObject1 == null) {
          k.a();
        }
        localObject1 = ((a)localObject1).invoke();
        b = localObject1;
        a = null;
      }
      return (T)localObject1;
    }
  }
  
  public final String toString()
  {
    if (a()) {
      return String.valueOf(b());
    }
    return "Lazy value not initialized yet.";
  }
}

/* Location:
 * Qualified Name:     c.r
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */