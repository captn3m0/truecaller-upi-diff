package c;

import c.g.b.k;
import java.io.Serializable;

public final class s<A, B, C>
  implements Serializable
{
  public final A a;
  public final B b;
  public final C c;
  
  public s(A paramA, B paramB, C paramC)
  {
    a = paramA;
    b = paramB;
    c = paramC;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject) {
      if ((paramObject instanceof s))
      {
        paramObject = (s)paramObject;
        if ((k.a(a, a)) && (k.a(b, b)) && (k.a(c, c))) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    Object localObject = a;
    int k = 0;
    int i;
    if (localObject != null) {
      i = localObject.hashCode();
    } else {
      i = 0;
    }
    localObject = b;
    int j;
    if (localObject != null) {
      j = localObject.hashCode();
    } else {
      j = 0;
    }
    localObject = c;
    if (localObject != null) {
      k = localObject.hashCode();
    }
    return (i * 31 + j) * 31 + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("(");
    localStringBuilder.append(a);
    localStringBuilder.append(", ");
    localStringBuilder.append(b);
    localStringBuilder.append(", ");
    localStringBuilder.append(c);
    localStringBuilder.append(')');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     c.s
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */