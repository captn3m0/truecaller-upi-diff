package c.n;

import c.g.b.a.a;
import c.k.h;
import c.n;
import java.util.Iterator;

final class e
  implements c.m.i<h>
{
  final CharSequence a;
  final int b;
  final int c;
  final c.g.a.m<CharSequence, Integer, n<Integer, Integer>> d;
  
  public e(CharSequence paramCharSequence, int paramInt, c.g.a.m<? super CharSequence, ? super Integer, n<Integer, Integer>> paramm)
  {
    a = paramCharSequence;
    b = 0;
    c = paramInt;
    d = paramm;
  }
  
  public final Iterator<h> a()
  {
    return (Iterator)new a(this);
  }
  
  public static final class a
    implements a, Iterator<h>
  {
    private int b = -1;
    private int c;
    private int d;
    private h e;
    private int f;
    
    a()
    {
      c = c.k.i.a(b, 0, a.length());
      d = c;
    }
    
    private final void a()
    {
      int j = d;
      int i = 0;
      if (j < 0)
      {
        b = 0;
        e = null;
        return;
      }
      if (a.c > 0)
      {
        f += 1;
        if (f >= a.c) {}
      }
      else
      {
        if (d <= a.a.length()) {
          break label108;
        }
      }
      e = new h(c, m.d(a.a));
      d = -1;
      break label238;
      label108:
      n localn = (n)a.d.invoke(a.a, Integer.valueOf(d));
      if (localn == null)
      {
        e = new h(c, m.d(a.a));
        d = -1;
      }
      else
      {
        int k = ((Number)a).intValue();
        j = ((Number)b).intValue();
        e = c.k.i.b(c, k);
        c = (k + j);
        k = c;
        if (j == 0) {
          i = 1;
        }
        d = (k + i);
      }
      label238:
      b = 1;
    }
    
    public final boolean hasNext()
    {
      if (b == -1) {
        a();
      }
      return b == 1;
    }
    
    public final void remove()
    {
      throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
  }
}

/* Location:
 * Qualified Name:     c.n.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */