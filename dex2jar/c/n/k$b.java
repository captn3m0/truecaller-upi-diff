package c.n;

import java.io.Serializable;
import java.util.regex.Pattern;

final class k$b
  implements Serializable
{
  public static final a a = new a((byte)0);
  private static final long serialVersionUID = 0L;
  private final String b;
  private final int c;
  
  public k$b(String paramString, int paramInt)
  {
    b = paramString;
    c = paramInt;
  }
  
  private final Object readResolve()
  {
    Pattern localPattern = Pattern.compile(b, c);
    c.g.b.k.a(localPattern, "Pattern.compile(pattern, flags)");
    return new k(localPattern);
  }
  
  public static final class a {}
}

/* Location:
 * Qualified Name:     c.n.k.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */