package c.n;

import c.g.b.k;

public class t
  extends s
{
  public static final Integer b(String paramString)
  {
    k.b(paramString, "receiver$0");
    return m.c(paramString);
  }
  
  public static final Integer c(String paramString)
  {
    k.b(paramString, "receiver$0");
    int i1 = paramString.length();
    if (i1 == 0) {
      return null;
    }
    int m = 0;
    int n = 0;
    int i = paramString.charAt(0);
    int k = -2147483647;
    int j;
    if (i < 48)
    {
      if (i1 == 1) {
        return null;
      }
      if (i == 45)
      {
        k = Integer.MIN_VALUE;
        i = 1;
        j = 1;
      }
      else if (i == 43)
      {
        i = 1;
        j = 0;
      }
      else
      {
        return null;
      }
    }
    else
    {
      i = 0;
      j = 0;
    }
    i1 -= 1;
    if (i <= i1) {
      for (m = n;; m = n)
      {
        n = Character.digit(paramString.charAt(i), 10);
        if (n < 0) {
          return null;
        }
        if (m < -214748364) {
          return null;
        }
        m *= 10;
        if (m < k + n) {
          return null;
        }
        n = m - n;
        m = n;
        if (i == i1) {
          break;
        }
        i += 1;
      }
    }
    if (j != 0) {
      return Integer.valueOf(m);
    }
    return Integer.valueOf(-m);
  }
  
  public static final Long d(String paramString)
  {
    k.b(paramString, "receiver$0");
    return m.e(paramString);
  }
  
  public static final Long e(String paramString)
  {
    k.b(paramString, "receiver$0");
    int k = paramString.length();
    if (k == 0) {
      return null;
    }
    int i = 0;
    int j = paramString.charAt(0);
    long l1 = -9223372036854775807L;
    if (j < 48)
    {
      if (k == 1) {
        return null;
      }
      if (j == 45)
      {
        l1 = Long.MIN_VALUE;
        i = 1;
        j = 1;
      }
      else if (j == 43)
      {
        i = 1;
        j = 0;
      }
      else
      {
        return null;
      }
    }
    else
    {
      j = 0;
    }
    long l3 = 0L;
    k -= 1;
    long l2 = l3;
    if (i <= k) {
      for (;;)
      {
        int m = Character.digit(paramString.charAt(i), 10);
        if (m < 0) {
          return null;
        }
        if (l3 < -922337203685477580L) {
          return null;
        }
        l2 = l3 * 10L;
        l3 = m;
        if (l2 < l1 + l3) {
          return null;
        }
        l3 = l2 - l3;
        l2 = l3;
        if (i == k) {
          break;
        }
        i += 1;
      }
    }
    if (j != 0) {
      return Long.valueOf(l2);
    }
    return Long.valueOf(-l2);
  }
}

/* Location:
 * Qualified Name:     c.n.t
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */