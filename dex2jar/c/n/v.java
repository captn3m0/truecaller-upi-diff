package c.n;

import c.g.a.b;
import c.g.b.k;
import c.k.h;
import c.n;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class v
  extends u
{
  public static final int a(CharSequence paramCharSequence, char paramChar, int paramInt)
  {
    k.b(paramCharSequence, "receiver$0");
    if (!(paramCharSequence instanceof String)) {
      return m.a(paramCharSequence, new char[] { paramChar }, paramInt, false);
    }
    return ((String)paramCharSequence).indexOf(paramChar, paramInt);
  }
  
  public static final int a(CharSequence paramCharSequence1, CharSequence paramCharSequence2, int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
  {
    c.k.f localf;
    if (!paramBoolean2) {
      localf = (c.k.f)new h(c.k.i.c(paramInt1, 0), c.k.i.d(paramInt2, paramCharSequence1.length()));
    } else {
      localf = c.k.i.a(c.k.i.d(paramInt1, m.d(paramCharSequence1)), c.k.i.c(paramInt2, 0));
    }
    int i;
    if (((paramCharSequence1 instanceof String)) && ((paramCharSequence2 instanceof String)))
    {
      paramInt1 = a;
      paramInt2 = b;
      i = c;
      if (i > 0 ? paramInt1 <= paramInt2 : paramInt1 >= paramInt2) {
        for (;;)
        {
          if (m.a((String)paramCharSequence2, 0, (String)paramCharSequence1, paramInt1, paramCharSequence2.length(), paramBoolean1)) {
            return paramInt1;
          }
          if (paramInt1 == paramInt2) {
            break;
          }
          paramInt1 += i;
        }
      }
    }
    else
    {
      paramInt1 = a;
      paramInt2 = b;
      i = c;
      if (i > 0 ? paramInt1 <= paramInt2 : paramInt1 >= paramInt2) {
        for (;;)
        {
          if (m.a(paramCharSequence2, paramCharSequence1, paramInt1, paramCharSequence2.length(), paramBoolean1)) {
            return paramInt1;
          }
          if (paramInt1 == paramInt2) {
            break;
          }
          paramInt1 += i;
        }
      }
    }
    return -1;
  }
  
  public static final int a(CharSequence paramCharSequence, String paramString, int paramInt, boolean paramBoolean)
  {
    k.b(paramCharSequence, "receiver$0");
    k.b(paramString, "string");
    if ((!paramBoolean) && ((paramCharSequence instanceof String))) {
      return ((String)paramCharSequence).indexOf(paramString, paramInt);
    }
    return b(paramCharSequence, (CharSequence)paramString, paramInt, paramCharSequence.length(), paramBoolean);
  }
  
  public static final int a(CharSequence paramCharSequence, char[] paramArrayOfChar, int paramInt, boolean paramBoolean)
  {
    k.b(paramCharSequence, "receiver$0");
    k.b(paramArrayOfChar, "chars");
    int i;
    if ((!paramBoolean) && (paramArrayOfChar.length == 1) && ((paramCharSequence instanceof String)))
    {
      k.b(paramArrayOfChar, "receiver$0");
      switch (paramArrayOfChar.length)
      {
      default: 
        throw ((Throwable)new IllegalArgumentException("Array has more than one element."));
      case 1: 
        i = paramArrayOfChar[0];
        return ((String)paramCharSequence).indexOf(i, paramInt);
      }
      throw ((Throwable)new NoSuchElementException("Array is empty."));
    }
    paramInt = c.k.i.c(paramInt, 0);
    int j = m.d(paramCharSequence);
    if (paramInt <= j) {
      for (;;)
      {
        char c = paramCharSequence.charAt(paramInt);
        int k = paramArrayOfChar.length;
        i = 0;
        while (i < k)
        {
          if (a.a(paramArrayOfChar[i], c, paramBoolean))
          {
            i = 1;
            break label174;
          }
          i += 1;
        }
        i = 0;
        label174:
        if (i != 0) {
          return paramInt;
        }
        if (paramInt == j) {
          break;
        }
        paramInt += 1;
      }
    }
    return -1;
  }
  
  static final c.m.i<h> a(CharSequence paramCharSequence, String[] paramArrayOfString, final boolean paramBoolean, int paramInt)
  {
    int i;
    if (paramInt >= 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      return (c.m.i)new e(paramCharSequence, paramInt, (c.g.a.m)new b(c.a.f.a(paramArrayOfString), paramBoolean));
    }
    paramCharSequence = new StringBuilder("Limit must be non-negative, but was ");
    paramCharSequence.append(paramInt);
    paramCharSequence.append('.');
    throw ((Throwable)new IllegalArgumentException(paramCharSequence.toString().toString()));
  }
  
  public static final CharSequence a(CharSequence paramCharSequence, int paramInt, char paramChar)
  {
    k.b(paramCharSequence, "receiver$0");
    if (paramInt >= 0)
    {
      if (paramInt <= paramCharSequence.length()) {
        return paramCharSequence.subSequence(0, paramCharSequence.length());
      }
      StringBuilder localStringBuilder = new StringBuilder(paramInt);
      int i = paramInt - paramCharSequence.length();
      if (i > 0)
      {
        paramInt = 1;
        for (;;)
        {
          localStringBuilder.append(paramChar);
          if (paramInt == i) {
            break;
          }
          paramInt += 1;
        }
      }
      localStringBuilder.append(paramCharSequence);
      return (CharSequence)localStringBuilder;
    }
    paramCharSequence = new StringBuilder("Desired length ");
    paramCharSequence.append(paramInt);
    paramCharSequence.append(" is less than zero.");
    throw ((Throwable)new IllegalArgumentException(paramCharSequence.toString()));
  }
  
  public static final String a(CharSequence paramCharSequence, h paramh)
  {
    k.b(paramCharSequence, "receiver$0");
    k.b(paramh, "range");
    return paramCharSequence.subSequence(a, b + 1).toString();
  }
  
  public static final String a(String paramString, int paramInt, char paramChar)
  {
    k.b(paramString, "receiver$0");
    return m.a((CharSequence)paramString, paramInt, paramChar).toString();
  }
  
  public static final String a(String paramString, CharSequence paramCharSequence)
  {
    k.b(paramString, "receiver$0");
    k.b(paramCharSequence, "prefix");
    if (m.a((CharSequence)paramString, paramCharSequence))
    {
      paramString = paramString.substring(paramCharSequence.length());
      k.a(paramString, "(this as java.lang.String).substring(startIndex)");
      return paramString;
    }
    return paramString;
  }
  
  private static final List<String> a(CharSequence paramCharSequence, String paramString, boolean paramBoolean, int paramInt)
  {
    int k = 0;
    int i;
    if (paramInt >= 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0)
    {
      int m = m.a(paramCharSequence, paramString, 0, paramBoolean);
      if ((m != -1) && (paramInt != 1))
      {
        if (paramInt > 0) {
          i = 1;
        } else {
          i = 0;
        }
        int j = 10;
        if (i != 0) {
          j = c.k.i.d(paramInt, 10);
        }
        ArrayList localArrayList = new ArrayList(j);
        j = m;
        int n;
        do
        {
          localArrayList.add(paramCharSequence.subSequence(k, j).toString());
          m = paramString.length() + j;
          if ((i != 0) && (localArrayList.size() == paramInt - 1)) {
            break;
          }
          n = m.a(paramCharSequence, paramString, m, paramBoolean);
          k = m;
          j = n;
        } while (n != -1);
        localArrayList.add(paramCharSequence.subSequence(m, paramCharSequence.length()).toString());
        return (List)localArrayList;
      }
      return c.a.m.a(paramCharSequence.toString());
    }
    paramCharSequence = new StringBuilder("Limit must be non-negative, but was ");
    paramCharSequence.append(paramInt);
    paramCharSequence.append('.');
    throw ((Throwable)new IllegalArgumentException(paramCharSequence.toString().toString()));
  }
  
  public static final List<String> a(CharSequence paramCharSequence, char[] paramArrayOfChar, int paramInt)
  {
    k.b(paramCharSequence, "receiver$0");
    k.b(paramArrayOfChar, "delimiters");
    int j = paramArrayOfChar.length;
    int i = 1;
    if (j == 1) {
      return a(paramCharSequence, String.valueOf(paramArrayOfChar[0]), false, paramInt);
    }
    if (paramInt < 0) {
      i = 0;
    }
    if (i != 0)
    {
      Object localObject = c.m.l.f((c.m.i)new e(paramCharSequence, paramInt, (c.g.a.m)new a(paramArrayOfChar)));
      paramArrayOfChar = (Collection)new ArrayList(c.a.m.a((Iterable)localObject, 10));
      localObject = ((Iterable)localObject).iterator();
      while (((Iterator)localObject).hasNext()) {
        paramArrayOfChar.add(m.a(paramCharSequence, (h)((Iterator)localObject).next()));
      }
      return (List)paramArrayOfChar;
    }
    paramCharSequence = new StringBuilder("Limit must be non-negative, but was ");
    paramCharSequence.append(paramInt);
    paramCharSequence.append('.');
    throw ((Throwable)new IllegalArgumentException(paramCharSequence.toString().toString()));
  }
  
  public static final boolean a(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
  {
    k.b(paramCharSequence1, "receiver$0");
    k.b(paramCharSequence2, "prefix");
    if (((paramCharSequence1 instanceof String)) && ((paramCharSequence2 instanceof String))) {
      return m.b((String)paramCharSequence1, (String)paramCharSequence2, false);
    }
    return m.a(paramCharSequence1, paramCharSequence2, 0, paramCharSequence2.length(), false);
  }
  
  public static final boolean a(CharSequence paramCharSequence1, CharSequence paramCharSequence2, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    k.b(paramCharSequence1, "receiver$0");
    k.b(paramCharSequence2, "other");
    if ((paramInt1 >= 0) && (paramCharSequence1.length() - paramInt2 >= 0))
    {
      if (paramInt1 > paramCharSequence2.length() - paramInt2) {
        return false;
      }
      int i = 0;
      while (i < paramInt2)
      {
        if (!a.a(paramCharSequence1.charAt(i + 0), paramCharSequence2.charAt(paramInt1 + i), paramBoolean)) {
          return false;
        }
        i += 1;
      }
      return true;
    }
    return false;
  }
  
  public static final boolean a(CharSequence paramCharSequence1, CharSequence paramCharSequence2, boolean paramBoolean)
  {
    k.b(paramCharSequence1, "receiver$0");
    k.b(paramCharSequence2, "other");
    if ((paramCharSequence2 instanceof String)) {
      return m.a(paramCharSequence1, (String)paramCharSequence2, 0, paramBoolean, 2) >= 0;
    }
    return b(paramCharSequence1, paramCharSequence2, 0, paramCharSequence1.length(), paramBoolean) >= 0;
  }
  
  public static final CharSequence b(CharSequence paramCharSequence)
  {
    k.b(paramCharSequence, "receiver$0");
    int j = paramCharSequence.length() - 1;
    int i = 0;
    int k = 0;
    while (i <= j)
    {
      int m;
      if (k == 0) {
        m = i;
      } else {
        m = j;
      }
      boolean bool = a.a(paramCharSequence.charAt(m));
      if (k == 0)
      {
        if (!bool) {
          k = 1;
        } else {
          i += 1;
        }
      }
      else
      {
        if (!bool) {
          break;
        }
        j -= 1;
      }
    }
    return paramCharSequence.subSequence(i, j + 1);
  }
  
  public static final List<String> b(CharSequence paramCharSequence, String[] paramArrayOfString, boolean paramBoolean, int paramInt)
  {
    k.b(paramCharSequence, "receiver$0");
    k.b(paramArrayOfString, "delimiters");
    if (paramArrayOfString.length == 1)
    {
      int i = 0;
      localObject = paramArrayOfString[0];
      if (((CharSequence)localObject).length() == 0) {
        i = 1;
      }
      if (i == 0) {
        return a(paramCharSequence, (String)localObject, paramBoolean, paramInt);
      }
    }
    Object localObject = c.m.l.f(a(paramCharSequence, paramArrayOfString, paramBoolean, paramInt));
    paramArrayOfString = (Collection)new ArrayList(c.a.m.a((Iterable)localObject, 10));
    localObject = ((Iterable)localObject).iterator();
    while (((Iterator)localObject).hasNext()) {
      paramArrayOfString.add(m.a(paramCharSequence, (h)((Iterator)localObject).next()));
    }
    return (List)paramArrayOfString;
  }
  
  public static final h c(CharSequence paramCharSequence)
  {
    k.b(paramCharSequence, "receiver$0");
    return new h(0, paramCharSequence.length() - 1);
  }
  
  public static final String c(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "receiver$0");
    k.b(paramString2, "delimiter");
    k.b(paramString3, "missingDelimiterValue");
    int i = m.a((CharSequence)paramString1, paramString2, 0, false, 6);
    if (i == -1) {
      return paramString3;
    }
    paramString1 = paramString1.substring(0, i);
    k.a(paramString1, "(this as java.lang.Strin…ing(startIndex, endIndex)");
    return paramString1;
  }
  
  public static final int d(CharSequence paramCharSequence)
  {
    k.b(paramCharSequence, "receiver$0");
    return paramCharSequence.length() - 1;
  }
  
  public static final String d(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "receiver$0");
    k.b(paramString2, "delimiter");
    k.b(paramString3, "missingDelimiterValue");
    int i = m.a((CharSequence)paramString1, paramString2, 0, false, 6);
    if (i == -1) {
      return paramString3;
    }
    paramString1 = paramString1.substring(i + paramString2.length(), paramString1.length());
    k.a(paramString1, "(this as java.lang.Strin…ing(startIndex, endIndex)");
    return paramString1;
  }
  
  static final class a
    extends c.g.b.l
    implements c.g.a.m<CharSequence, Integer, n<? extends Integer, ? extends Integer>>
  {
    a(char[] paramArrayOfChar)
    {
      super();
    }
  }
  
  static final class b
    extends c.g.b.l
    implements c.g.a.m<CharSequence, Integer, n<? extends Integer, ? extends Integer>>
  {
    b(List paramList, boolean paramBoolean)
    {
      super();
    }
  }
  
  static final class c
    extends c.g.b.l
    implements b<h, String>
  {
    c(CharSequence paramCharSequence)
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     c.n.v
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */