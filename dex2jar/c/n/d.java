package c.n;

import c.g.b.k;
import java.nio.charset.Charset;

public final class d
{
  public static final Charset a;
  public static final Charset b;
  public static final Charset c;
  public static final Charset d;
  public static final Charset e;
  public static final Charset f;
  public static final d g = new d();
  
  static
  {
    Charset localCharset = Charset.forName("UTF-8");
    k.a(localCharset, "Charset.forName(\"UTF-8\")");
    a = localCharset;
    localCharset = Charset.forName("UTF-16");
    k.a(localCharset, "Charset.forName(\"UTF-16\")");
    b = localCharset;
    localCharset = Charset.forName("UTF-16BE");
    k.a(localCharset, "Charset.forName(\"UTF-16BE\")");
    c = localCharset;
    localCharset = Charset.forName("UTF-16LE");
    k.a(localCharset, "Charset.forName(\"UTF-16LE\")");
    d = localCharset;
    localCharset = Charset.forName("US-ASCII");
    k.a(localCharset, "Charset.forName(\"US-ASCII\")");
    e = localCharset;
    localCharset = Charset.forName("ISO-8859-1");
    k.a(localCharset, "Charset.forName(\"ISO-8859-1\")");
    f = localCharset;
  }
}

/* Location:
 * Qualified Name:     c.n.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */