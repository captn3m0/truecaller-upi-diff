package c.n;

import c.a.a;
import c.a.m;
import c.g.a.b;
import c.m.i;
import java.util.Iterator;
import java.util.regex.MatchResult;

public final class j$b
  extends a<f>
  implements h
{
  public final int a()
  {
    return ((MatchResult)a.a).groupCount() + 1;
  }
  
  public final boolean isEmpty()
  {
    return false;
  }
  
  public final Iterator<f> iterator()
  {
    return c.m.l.c(m.n((Iterable)m.a(this)), (b)new a(this)).a();
  }
  
  static final class a
    extends c.g.b.l
    implements b<Integer, f>
  {
    a(j.b paramb)
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     c.n.j.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */