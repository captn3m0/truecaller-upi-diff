package c.n;

import c.a.a;
import c.a.d;
import c.a.m;
import c.g.a.b;
import c.g.b.k;
import java.util.Iterator;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;

public final class j
  implements i
{
  final Matcher a;
  private final g b;
  private List<String> c;
  private final CharSequence d;
  
  public j(Matcher paramMatcher, CharSequence paramCharSequence)
  {
    a = paramMatcher;
    d = paramCharSequence;
    b = ((g)new b(this));
  }
  
  public final List<String> a()
  {
    if (c == null) {
      c = ((List)new a(this));
    }
    List localList = c;
    if (localList == null) {
      k.a();
    }
    return localList;
  }
  
  public static final class a
    extends d<String>
  {
    public final int a()
    {
      return ((MatchResult)b.a).groupCount() + 1;
    }
  }
  
  public static final class b
    extends a<f>
    implements h
  {
    public final int a()
    {
      return ((MatchResult)a.a).groupCount() + 1;
    }
    
    public final boolean isEmpty()
    {
      return false;
    }
    
    public final Iterator<f> iterator()
    {
      return c.m.l.c(m.n((Iterable)m.a(this)), (b)new a(this)).a();
    }
    
    static final class a
      extends c.g.b.l
      implements b<Integer, f>
    {
      a(j.b paramb)
      {
        super();
      }
    }
  }
}

/* Location:
 * Qualified Name:     c.n.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */