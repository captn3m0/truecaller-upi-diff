package c.n;

import c.a.m;
import c.k.i;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class k
  implements Serializable
{
  public static final a b = new a((byte)0);
  public final Pattern a;
  
  public k(String paramString)
  {
    this(paramString);
  }
  
  public k(Pattern paramPattern)
  {
    a = paramPattern;
  }
  
  private final Object writeReplace()
  {
    String str = a.pattern();
    c.g.b.k.a(str, "nativePattern.pattern()");
    return new b(str, a.flags());
  }
  
  public final String a(CharSequence paramCharSequence, String paramString)
  {
    c.g.b.k.b(paramCharSequence, "input");
    c.g.b.k.b(paramString, "replacement");
    paramCharSequence = a.matcher(paramCharSequence).replaceAll(paramString);
    c.g.b.k.a(paramCharSequence, "nativePattern.matcher(in…).replaceAll(replacement)");
    return paramCharSequence;
  }
  
  public final List<String> a(CharSequence paramCharSequence, int paramInt)
  {
    c.g.b.k.b(paramCharSequence, "input");
    int j = 0;
    int i;
    if (paramInt >= 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0)
    {
      Matcher localMatcher = a.matcher(paramCharSequence);
      if ((localMatcher.find()) && (paramInt != 1))
      {
        i = 10;
        if (paramInt > 0) {
          i = i.d(paramInt, 10);
        }
        ArrayList localArrayList = new ArrayList(i);
        int k = paramInt - 1;
        paramInt = j;
        do
        {
          localArrayList.add(paramCharSequence.subSequence(paramInt, localMatcher.start()).toString());
          i = localMatcher.end();
          if ((k >= 0) && (localArrayList.size() == k)) {
            break;
          }
          paramInt = i;
        } while (localMatcher.find());
        localArrayList.add(paramCharSequence.subSequence(i, paramCharSequence.length()).toString());
        return (List)localArrayList;
      }
      return m.a(paramCharSequence.toString());
    }
    paramCharSequence = new StringBuilder("Limit must be non-negative, but was ");
    paramCharSequence.append(paramInt);
    paramCharSequence.append('.');
    throw ((Throwable)new IllegalArgumentException(paramCharSequence.toString().toString()));
  }
  
  public final boolean a(CharSequence paramCharSequence)
  {
    c.g.b.k.b(paramCharSequence, "input");
    return a.matcher(paramCharSequence).matches();
  }
  
  public final String toString()
  {
    String str = a.toString();
    c.g.b.k.a(str, "nativePattern.toString()");
    return str;
  }
  
  public static final class a {}
  
  static final class b
    implements Serializable
  {
    public static final a a = new a((byte)0);
    private static final long serialVersionUID = 0L;
    private final String b;
    private final int c;
    
    public b(String paramString, int paramInt)
    {
      b = paramString;
      c = paramInt;
    }
    
    private final Object readResolve()
    {
      Pattern localPattern = Pattern.compile(b, c);
      c.g.b.k.a(localPattern, "Pattern.compile(pattern, flags)");
      return new k(localPattern);
    }
    
    public static final class a {}
  }
}

/* Location:
 * Qualified Name:     c.n.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */