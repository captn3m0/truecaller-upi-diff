package c.n;

import c.a.ae;
import c.g.a.b;
import c.g.b.k;
import c.m.l;
import java.util.Iterator;

public class u
  extends t
{
  public static final String a(String paramString, char paramChar1, char paramChar2)
  {
    k.b(paramString, "receiver$0");
    paramString = paramString.replace(paramChar1, paramChar2);
    k.a(paramString, "(this as java.lang.Strin…replace(oldChar, newChar)");
    return paramString;
  }
  
  public static final String a(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "receiver$0");
    k.b(paramString2, "oldValue");
    k.b(paramString3, "newValue");
    paramString1 = (CharSequence)paramString1;
    String[] arrayOfString = new String[1];
    arrayOfString[0] = paramString2;
    k.b(paramString1, "receiver$0");
    k.b(arrayOfString, "delimiters");
    return l.a(l.c(v.a(paramString1, arrayOfString, false, 0), (b)new v.c(paramString1)), (CharSequence)paramString3);
  }
  
  public static final boolean a(CharSequence paramCharSequence)
  {
    k.b(paramCharSequence, "receiver$0");
    if (paramCharSequence.length() != 0)
    {
      Iterator localIterator = ((Iterable)m.c(paramCharSequence)).iterator();
      while (localIterator.hasNext()) {
        if (!a.a(paramCharSequence.charAt(((ae)localIterator).a())))
        {
          i = 0;
          break label63;
        }
      }
      int i = 1;
      label63:
      return i != 0;
    }
    return true;
  }
  
  public static final boolean a(String paramString1, int paramInt1, String paramString2, int paramInt2, int paramInt3, boolean paramBoolean)
  {
    k.b(paramString1, "receiver$0");
    k.b(paramString2, "other");
    if (!paramBoolean) {
      return paramString1.regionMatches(paramInt1, paramString2, paramInt2, paramInt3);
    }
    return paramString1.regionMatches(paramBoolean, paramInt1, paramString2, paramInt2, paramInt3);
  }
  
  public static final boolean a(String paramString1, String paramString2, boolean paramBoolean)
  {
    if (paramString1 == null) {
      return paramString2 == null;
    }
    if (!paramBoolean) {
      return paramString1.equals(paramString2);
    }
    return paramString1.equalsIgnoreCase(paramString2);
  }
  
  public static final String b(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "receiver$0");
    k.b(paramString2, "oldValue");
    k.b(paramString3, "newValue");
    CharSequence localCharSequence = (CharSequence)paramString1;
    int i = m.a(localCharSequence, paramString2, 0, false, 2);
    if (i < 0) {
      return paramString1;
    }
    int j = paramString2.length() + i;
    paramString1 = (CharSequence)paramString3;
    k.b(localCharSequence, "receiver$0");
    k.b(paramString1, "replacement");
    if (j >= i)
    {
      paramString2 = new StringBuilder();
      paramString2.append(localCharSequence, 0, i);
      paramString2.append(paramString1);
      paramString2.append(localCharSequence, j, localCharSequence.length());
      return ((CharSequence)paramString2).toString();
    }
    paramString1 = new StringBuilder("End index (");
    paramString1.append(j);
    paramString1.append(") is less than start index (");
    paramString1.append(i);
    paramString1.append(").");
    throw ((Throwable)new IndexOutOfBoundsException(paramString1.toString()));
  }
  
  public static final boolean b(String paramString1, String paramString2, boolean paramBoolean)
  {
    k.b(paramString1, "receiver$0");
    k.b(paramString2, "prefix");
    if (!paramBoolean) {
      return paramString1.startsWith(paramString2);
    }
    return m.a(paramString1, 0, paramString2, 0, paramString2.length(), paramBoolean);
  }
  
  public static final boolean c(String paramString1, String paramString2, boolean paramBoolean)
  {
    k.b(paramString1, "receiver$0");
    k.b(paramString2, "suffix");
    if (!paramBoolean) {
      return paramString1.endsWith(paramString2);
    }
    return m.a(paramString1, paramString1.length() - paramString2.length(), paramString2, 0, paramString2.length(), true);
  }
  
  public static final String f(String paramString)
  {
    k.b(paramString, "receiver$0");
    int i;
    if (((CharSequence)paramString).length() > 0) {
      i = 1;
    } else {
      i = 0;
    }
    if ((i != 0) && (Character.isLowerCase(paramString.charAt(0))))
    {
      StringBuilder localStringBuilder = new StringBuilder();
      String str = paramString.substring(0, 1);
      k.a(str, "(this as java.lang.Strin…ing(startIndex, endIndex)");
      if (str != null)
      {
        str = str.toUpperCase();
        k.a(str, "(this as java.lang.String).toUpperCase()");
        localStringBuilder.append(str);
        paramString = paramString.substring(1);
        k.a(paramString, "(this as java.lang.String).substring(startIndex)");
        localStringBuilder.append(paramString);
        return localStringBuilder.toString();
      }
      throw new c.u("null cannot be cast to non-null type java.lang.String");
    }
    return paramString;
  }
}

/* Location:
 * Qualified Name:     c.n.u
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */