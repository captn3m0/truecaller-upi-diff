package c.n;

import c.g.a.b;
import c.g.b.k;

public class r
  extends q
{
  public static final <T> void a(Appendable paramAppendable, T paramT, b<? super T, ? extends CharSequence> paramb)
  {
    k.b(paramAppendable, "receiver$0");
    if (paramb != null)
    {
      paramAppendable.append((CharSequence)paramb.invoke(paramT));
      return;
    }
    boolean bool;
    if (paramT != null) {
      bool = paramT instanceof CharSequence;
    } else {
      bool = true;
    }
    if (bool)
    {
      paramAppendable.append((CharSequence)paramT);
      return;
    }
    if ((paramT instanceof Character))
    {
      paramAppendable.append(((Character)paramT).charValue());
      return;
    }
    paramAppendable.append((CharSequence)String.valueOf(paramT));
  }
}

/* Location:
 * Qualified Name:     c.n.r
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */