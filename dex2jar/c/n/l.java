package c.n;

final class l
{
  public static final k a;
  public static final l b = new l();
  
  static
  {
    String str = "[eE][+-]?".concat(String.valueOf("(\\p{Digit}+)"));
    Object localObject = new StringBuilder("(0[xX]");
    ((StringBuilder)localObject).append("(\\p{XDigit}+)");
    ((StringBuilder)localObject).append("(\\.)?)|(0[xX]");
    ((StringBuilder)localObject).append("(\\p{XDigit}+)");
    ((StringBuilder)localObject).append("?(\\.)");
    ((StringBuilder)localObject).append("(\\p{XDigit}+)");
    ((StringBuilder)localObject).append(')');
    localObject = ((StringBuilder)localObject).toString();
    StringBuilder localStringBuilder = new StringBuilder("(");
    localStringBuilder.append("(\\p{Digit}+)");
    localStringBuilder.append("(\\.)?(");
    localStringBuilder.append("(\\p{Digit}+)");
    localStringBuilder.append("?)(");
    localStringBuilder.append(str);
    localStringBuilder.append(")?)|(\\.(");
    localStringBuilder.append("(\\p{Digit}+)");
    localStringBuilder.append(")(");
    localStringBuilder.append(str);
    localStringBuilder.append(")?)|((");
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")[pP][+-]?");
    localStringBuilder.append("(\\p{Digit}+)");
    localStringBuilder.append(')');
    str = localStringBuilder.toString();
    localObject = new StringBuilder("[\\x00-\\x20]*[+-]?(NaN|Infinity|((");
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append(")[fFdD]?))[\\x00-\\x20]*");
    a = new k(((StringBuilder)localObject).toString());
  }
}

/* Location:
 * Qualified Name:     c.n.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */