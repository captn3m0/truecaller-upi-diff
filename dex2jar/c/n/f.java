package c.n;

import c.g.b.k;
import c.k.h;

public final class f
{
  private final String a;
  private final h b;
  
  public f(String paramString, h paramh)
  {
    a = paramString;
    b = paramh;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject) {
      if ((paramObject instanceof f))
      {
        paramObject = (f)paramObject;
        if ((k.a(a, a)) && (k.a(b, b))) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    Object localObject = a;
    int j = 0;
    int i;
    if (localObject != null) {
      i = localObject.hashCode();
    } else {
      i = 0;
    }
    localObject = b;
    if (localObject != null) {
      j = localObject.hashCode();
    }
    return i * 31 + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("MatchGroup(value=");
    localStringBuilder.append(a);
    localStringBuilder.append(", range=");
    localStringBuilder.append(b);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     c.n.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */