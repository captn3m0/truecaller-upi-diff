package c.n;

import c.a.y;
import c.g.b.k;
import c.k.i;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class x
  extends w
{
  public static final Character a(CharSequence paramCharSequence, int paramInt)
  {
    k.b(paramCharSequence, "receiver$0");
    if ((paramInt >= 0) && (paramInt <= m.d(paramCharSequence))) {
      return Character.valueOf(paramCharSequence.charAt(paramInt));
    }
    return null;
  }
  
  public static final String a(String paramString, int paramInt)
  {
    k.b(paramString, "receiver$0");
    int i;
    if (paramInt >= 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0)
    {
      paramString = paramString.substring(0, i.d(paramInt, paramString.length()));
      k.a(paramString, "(this as java.lang.Strin…ing(startIndex, endIndex)");
      return paramString;
    }
    paramString = new StringBuilder("Requested character count ");
    paramString.append(paramInt);
    paramString.append(" is less than zero.");
    throw ((Throwable)new IllegalArgumentException(paramString.toString().toString()));
  }
  
  public static final <C extends Collection<? super Character>> C a(CharSequence paramCharSequence, C paramC)
  {
    k.b(paramCharSequence, "receiver$0");
    k.b(paramC, "destination");
    int i = 0;
    while (i < paramCharSequence.length())
    {
      paramC.add(Character.valueOf(paramCharSequence.charAt(i)));
      i += 1;
    }
    return paramC;
  }
  
  public static final String b(String paramString, int paramInt)
  {
    k.b(paramString, "receiver$0");
    int i;
    if (paramInt >= 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0)
    {
      i = paramString.length();
      paramString = paramString.substring(i - i.d(paramInt, i));
      k.a(paramString, "(this as java.lang.String).substring(startIndex)");
      return paramString;
    }
    paramString = new StringBuilder("Requested character count ");
    paramString.append(paramInt);
    paramString.append(" is less than zero.");
    throw ((Throwable)new IllegalArgumentException(paramString.toString().toString()));
  }
  
  public static final Character f(CharSequence paramCharSequence)
  {
    k.b(paramCharSequence, "receiver$0");
    int i;
    if (paramCharSequence.length() == 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      return null;
    }
    return Character.valueOf(paramCharSequence.charAt(0));
  }
  
  public static final List<Character> g(CharSequence paramCharSequence)
  {
    k.b(paramCharSequence, "receiver$0");
    switch (paramCharSequence.length())
    {
    default: 
      return m.h(paramCharSequence);
    case 1: 
      return c.a.m.a(Character.valueOf(paramCharSequence.charAt(0)));
    }
    return (List)y.a;
  }
  
  public static final String h(String paramString)
  {
    k.b(paramString, "receiver$0");
    paramString = paramString.substring(i.d(1, paramString.length()));
    k.a(paramString, "(this as java.lang.String).substring(startIndex)");
    return paramString;
  }
  
  public static final List<Character> h(CharSequence paramCharSequence)
  {
    k.b(paramCharSequence, "receiver$0");
    return (List)m.a(paramCharSequence, (Collection)new ArrayList(paramCharSequence.length()));
  }
}

/* Location:
 * Qualified Name:     c.n.x
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */