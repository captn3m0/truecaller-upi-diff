package c.a;

import c.g.a.b;
import c.g.b.c;
import c.g.b.k;
import c.m.e;
import c.n;
import c.t;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

public class j
  extends i
{
  public static final long a(long[] paramArrayOfLong)
  {
    k.b(paramArrayOfLong, "receiver$0");
    int i;
    if (paramArrayOfLong.length == 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i == 0) {
      return paramArrayOfLong[f.c(paramArrayOfLong)];
    }
    throw ((Throwable)new NoSuchElementException("Array is empty."));
  }
  
  public static final <T, A extends Appendable> A a(T[] paramArrayOfT, A paramA, CharSequence paramCharSequence1, CharSequence paramCharSequence2, CharSequence paramCharSequence3, int paramInt, CharSequence paramCharSequence4, b<? super T, ? extends CharSequence> paramb)
  {
    k.b(paramArrayOfT, "receiver$0");
    k.b(paramA, "buffer");
    k.b(paramCharSequence1, "separator");
    k.b(paramCharSequence2, "prefix");
    k.b(paramCharSequence3, "postfix");
    k.b(paramCharSequence4, "truncated");
    paramA.append(paramCharSequence2);
    int m = paramArrayOfT.length;
    int j = 0;
    int i = 0;
    int k;
    for (;;)
    {
      k = i;
      if (j >= m) {
        break;
      }
      paramCharSequence2 = paramArrayOfT[j];
      i += 1;
      if (i > 1) {
        paramA.append(paramCharSequence1);
      }
      if (paramInt >= 0)
      {
        k = i;
        if (i > paramInt) {
          break;
        }
      }
      c.n.m.a(paramA, paramCharSequence2, paramb);
      j += 1;
    }
    if ((paramInt >= 0) && (k > paramInt)) {
      paramA.append(paramCharSequence4);
    }
    paramA.append(paramCharSequence3);
    return paramA;
  }
  
  public static final <T> String a(T[] paramArrayOfT, CharSequence paramCharSequence1, CharSequence paramCharSequence2, CharSequence paramCharSequence3, int paramInt, CharSequence paramCharSequence4, b<? super T, ? extends CharSequence> paramb)
  {
    k.b(paramArrayOfT, "receiver$0");
    k.b(paramCharSequence1, "separator");
    k.b(paramCharSequence2, "prefix");
    k.b(paramCharSequence3, "postfix");
    k.b(paramCharSequence4, "truncated");
    paramArrayOfT = ((StringBuilder)f.a(paramArrayOfT, (Appendable)new StringBuilder(), paramCharSequence1, paramCharSequence2, paramCharSequence3, paramInt, paramCharSequence4, paramb)).toString();
    k.a(paramArrayOfT, "joinTo(StringBuilder(), …ed, transform).toString()");
    return paramArrayOfT;
  }
  
  public static final <C extends Collection<? super T>, T> C a(T[] paramArrayOfT, C paramC)
  {
    k.b(paramArrayOfT, "receiver$0");
    k.b(paramC, "destination");
    int j = paramArrayOfT.length;
    int i = 0;
    while (i < j)
    {
      T ? = paramArrayOfT[i];
      if (? != null) {
        paramC.add(?);
      }
      i += 1;
    }
    return paramC;
  }
  
  public static final <T, R> List<n<T, R>> a(T[] paramArrayOfT, Iterable<? extends R> paramIterable)
  {
    k.b(paramArrayOfT, "receiver$0");
    k.b(paramIterable, "other");
    int j = paramArrayOfT.length;
    ArrayList localArrayList = new ArrayList(Math.min(m.a(paramIterable, 10), j));
    paramIterable = paramIterable.iterator();
    int i = 0;
    while (paramIterable.hasNext())
    {
      Object localObject = paramIterable.next();
      if (i >= j) {
        break;
      }
      localArrayList.add(t.a(paramArrayOfT[i], localObject));
      i += 1;
    }
    return (List)localArrayList;
  }
  
  public static final boolean a(int[] paramArrayOfInt, int paramInt)
  {
    k.b(paramArrayOfInt, "receiver$0");
    return f.b(paramArrayOfInt, paramInt) >= 0;
  }
  
  public static final int b(int[] paramArrayOfInt)
  {
    k.b(paramArrayOfInt, "receiver$0");
    int i;
    if (paramArrayOfInt.length == 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i == 0) {
      return paramArrayOfInt[0];
    }
    throw ((Throwable)new NoSuchElementException("Array is empty."));
  }
  
  public static final int b(int[] paramArrayOfInt, int paramInt)
  {
    k.b(paramArrayOfInt, "receiver$0");
    int j = paramArrayOfInt.length;
    int i = 0;
    while (i < j)
    {
      if (paramInt == paramArrayOfInt[i]) {
        return i;
      }
      i += 1;
    }
    return -1;
  }
  
  public static final <T> T b(T[] paramArrayOfT)
  {
    k.b(paramArrayOfT, "receiver$0");
    int i;
    if (paramArrayOfT.length == 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i == 0) {
      return paramArrayOfT[0];
    }
    throw ((Throwable)new NoSuchElementException("Array is empty."));
  }
  
  public static final <T, C extends Collection<? super T>> C b(T[] paramArrayOfT, C paramC)
  {
    k.b(paramArrayOfT, "receiver$0");
    k.b(paramC, "destination");
    int j = paramArrayOfT.length;
    int i = 0;
    while (i < j)
    {
      paramC.add(paramArrayOfT[i]);
      i += 1;
    }
    return paramC;
  }
  
  public static final List<Long> b(long[] paramArrayOfLong)
  {
    k.b(paramArrayOfLong, "receiver$0");
    if (100 >= paramArrayOfLong.length) {
      return f.d(paramArrayOfLong);
    }
    ArrayList localArrayList = new ArrayList(100);
    int k = paramArrayOfLong.length;
    int j = 0;
    int i = 0;
    while (j < k)
    {
      long l = paramArrayOfLong[j];
      if (i == 100) {
        break;
      }
      localArrayList.add(Long.valueOf(l));
      j += 1;
      i += 1;
    }
    return (List)localArrayList;
  }
  
  public static final <T> boolean b(T[] paramArrayOfT, T paramT)
  {
    k.b(paramArrayOfT, "receiver$0");
    return f.c(paramArrayOfT, paramT) >= 0;
  }
  
  public static final int c(long[] paramArrayOfLong)
  {
    k.b(paramArrayOfLong, "receiver$0");
    return paramArrayOfLong.length - 1;
  }
  
  public static final <T> int c(T[] paramArrayOfT, T paramT)
  {
    k.b(paramArrayOfT, "receiver$0");
    int j = 0;
    int i = 0;
    if (paramT == null)
    {
      j = paramArrayOfT.length;
      while (i < j)
      {
        if (paramArrayOfT[i] == null) {
          return i;
        }
        i += 1;
      }
    }
    int k = paramArrayOfT.length;
    i = j;
    while (i < k)
    {
      if (k.a(paramT, paramArrayOfT[i])) {
        return i;
      }
      i += 1;
    }
    return -1;
  }
  
  public static final <T> T c(T[] paramArrayOfT)
  {
    k.b(paramArrayOfT, "receiver$0");
    int i;
    if (paramArrayOfT.length == 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      return null;
    }
    return paramArrayOfT[0];
  }
  
  public static final List<Long> d(long[] paramArrayOfLong)
  {
    k.b(paramArrayOfLong, "receiver$0");
    switch (paramArrayOfLong.length)
    {
    default: 
      return f.e(paramArrayOfLong);
    case 1: 
      return m.a(Long.valueOf(paramArrayOfLong[0]));
    }
    return (List)y.a;
  }
  
  public static final <T> List<T> d(T[] paramArrayOfT)
  {
    k.b(paramArrayOfT, "receiver$0");
    return (List)f.a(paramArrayOfT, (Collection)new ArrayList());
  }
  
  public static final <T> int e(T[] paramArrayOfT)
  {
    k.b(paramArrayOfT, "receiver$0");
    return paramArrayOfT.length - 1;
  }
  
  public static final List<Long> e(long[] paramArrayOfLong)
  {
    k.b(paramArrayOfLong, "receiver$0");
    ArrayList localArrayList = new ArrayList(paramArrayOfLong.length);
    int j = paramArrayOfLong.length;
    int i = 0;
    while (i < j)
    {
      localArrayList.add(Long.valueOf(paramArrayOfLong[i]));
      i += 1;
    }
    return (List)localArrayList;
  }
  
  public static final <T> List<T> f(T[] paramArrayOfT)
  {
    k.b(paramArrayOfT, "receiver$0");
    switch (paramArrayOfT.length)
    {
    default: 
      return f.g(paramArrayOfT);
    case 1: 
      return m.a(paramArrayOfT[0]);
    }
    return (List)y.a;
  }
  
  public static final <T> List<T> g(T[] paramArrayOfT)
  {
    k.b(paramArrayOfT, "receiver$0");
    return (List)new ArrayList(m.a(paramArrayOfT));
  }
  
  public static final <T> Set<T> h(T[] paramArrayOfT)
  {
    k.b(paramArrayOfT, "receiver$0");
    switch (paramArrayOfT.length)
    {
    default: 
      return (Set)f.b(paramArrayOfT, (Collection)new LinkedHashSet(ag.a(paramArrayOfT.length)));
    case 1: 
      return an.a(paramArrayOfT[0]);
    }
    return (Set)aa.a;
  }
  
  public static final <T> Iterable<T> i(T[] paramArrayOfT)
  {
    k.b(paramArrayOfT, "receiver$0");
    int i;
    if (paramArrayOfT.length == 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      return (Iterable)y.a;
    }
    return (Iterable)new a(paramArrayOfT);
  }
  
  public static final <T> c.m.i<T> j(T[] paramArrayOfT)
  {
    k.b(paramArrayOfT, "receiver$0");
    int i;
    if (paramArrayOfT.length == 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0) {
      return (c.m.i)e.a;
    }
    return (c.m.i)new c(paramArrayOfT);
  }
  
  public static final class a
    implements c.g.b.a.a, Iterable<T>
  {
    public a(Object[] paramArrayOfObject) {}
    
    public final Iterator<T> iterator()
    {
      return c.a(a);
    }
  }
  
  public static final class b
    implements c.g.b.a.a, Iterable<Byte>
  {
    public b(byte[] paramArrayOfByte) {}
    
    public final Iterator<Byte> iterator()
    {
      byte[] arrayOfByte = a;
      k.b(arrayOfByte, "array");
      return (Iterator)new c.g.b.a(arrayOfByte);
    }
  }
  
  public static final class c
    implements c.m.i<T>
  {
    public c(Object[] paramArrayOfObject) {}
    
    public final Iterator<T> a()
    {
      return c.a(a);
    }
  }
}

/* Location:
 * Qualified Name:     c.a.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */