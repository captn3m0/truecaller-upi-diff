package c.a;

import c.g.b.a.a;
import java.util.ListIterator;

public final class x
  implements a, ListIterator
{
  public static final x a = new x();
  
  public final boolean hasNext()
  {
    return false;
  }
  
  public final boolean hasPrevious()
  {
    return false;
  }
  
  public final int nextIndex()
  {
    return 0;
  }
  
  public final int previousIndex()
  {
    return -1;
  }
  
  public final void remove()
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
}

/* Location:
 * Qualified Name:     c.a.x
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */