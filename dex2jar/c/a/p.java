package c.a;

import c.g.b.k;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class p
  extends o
{
  public static final <T> int a(Iterable<? extends T> paramIterable, int paramInt)
  {
    k.b(paramIterable, "receiver$0");
    if ((paramIterable instanceof Collection)) {
      return ((Collection)paramIterable).size();
    }
    return paramInt;
  }
  
  public static final <T> Collection<T> a(Iterable<? extends T> paramIterable1, Iterable<? extends T> paramIterable2)
  {
    k.b(paramIterable1, "receiver$0");
    k.b(paramIterable2, "source");
    if ((paramIterable1 instanceof Set)) {
      return (Collection)paramIterable1;
    }
    if ((paramIterable1 instanceof Collection))
    {
      if (((paramIterable2 instanceof Collection)) && (((Collection)paramIterable2).size() < 2)) {
        return (Collection)paramIterable1;
      }
      paramIterable2 = (Collection)paramIterable1;
      int i;
      if ((paramIterable2.size() > 2) && ((paramIterable2 instanceof ArrayList))) {
        i = 1;
      } else {
        i = 0;
      }
      if (i == 0) {
        return paramIterable2;
      }
    }
    k.b(paramIterable1, "receiver$0");
    return (Collection)m.b(paramIterable1, (Collection)new HashSet(ag.a(m.a(paramIterable1, 12))));
  }
}

/* Location:
 * Qualified Name:     c.a.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */