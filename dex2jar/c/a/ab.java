package c.a;

import c.g.b.k;

public final class ab<T>
{
  public final int a;
  public final T b;
  
  public ab(int paramInt, T paramT)
  {
    a = paramInt;
    b = paramT;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      if ((paramObject instanceof ab))
      {
        paramObject = (ab)paramObject;
        int i;
        if (a == a) {
          i = 1;
        } else {
          i = 0;
        }
        if ((i != 0) && (k.a(b, b))) {
          return true;
        }
      }
      return false;
    }
    return true;
  }
  
  public final int hashCode()
  {
    int j = a;
    Object localObject = b;
    int i;
    if (localObject != null) {
      i = localObject.hashCode();
    } else {
      i = 0;
    }
    return j * 31 + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("IndexedValue(index=");
    localStringBuilder.append(a);
    localStringBuilder.append(", value=");
    localStringBuilder.append(b);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     c.a.ab
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */