package c.a;

import c.g.a.a;
import c.g.a.b;
import c.g.b.k;
import c.g.b.l;
import c.n;
import c.t;
import c.u;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.RandomAccess;
import java.util.Set;

public class w
  extends v
{
  public static final <T, A extends Appendable> A a(Iterable<? extends T> paramIterable, A paramA, CharSequence paramCharSequence1, CharSequence paramCharSequence2, CharSequence paramCharSequence3, int paramInt, CharSequence paramCharSequence4, b<? super T, ? extends CharSequence> paramb)
  {
    k.b(paramIterable, "receiver$0");
    k.b(paramA, "buffer");
    k.b(paramCharSequence1, "separator");
    k.b(paramCharSequence2, "prefix");
    k.b(paramCharSequence3, "postfix");
    k.b(paramCharSequence4, "truncated");
    paramA.append(paramCharSequence2);
    paramIterable = paramIterable.iterator();
    int i = 0;
    int j;
    for (;;)
    {
      j = i;
      if (!paramIterable.hasNext()) {
        break;
      }
      paramCharSequence2 = paramIterable.next();
      i += 1;
      if (i > 1) {
        paramA.append(paramCharSequence1);
      }
      if (paramInt >= 0)
      {
        j = i;
        if (i > paramInt) {
          break;
        }
      }
      c.n.m.a(paramA, paramCharSequence2, paramb);
    }
    if ((paramInt >= 0) && (j > paramInt)) {
      paramA.append(paramCharSequence4);
    }
    paramA.append(paramCharSequence3);
    return paramA;
  }
  
  public static final <T> T a(Iterable<? extends T> paramIterable, int paramInt, b<? super Integer, ? extends T> paramb)
  {
    k.b(paramIterable, "receiver$0");
    k.b(paramb, "defaultValue");
    if ((paramIterable instanceof List))
    {
      paramIterable = (List)paramIterable;
      if ((paramInt >= 0) && (paramInt <= m.a(paramIterable))) {
        return (T)paramIterable.get(paramInt);
      }
      return (T)paramb.invoke(Integer.valueOf(paramInt));
    }
    if (paramInt < 0) {
      return (T)paramb.invoke(Integer.valueOf(paramInt));
    }
    paramIterable = paramIterable.iterator();
    int i = 0;
    while (paramIterable.hasNext())
    {
      Object localObject = paramIterable.next();
      if (paramInt == i) {
        return (T)localObject;
      }
      i += 1;
    }
    return (T)paramb.invoke(Integer.valueOf(paramInt));
  }
  
  public static final <T> T a(List<? extends T> paramList, int paramInt)
  {
    k.b(paramList, "receiver$0");
    if ((paramInt >= 0) && (paramInt <= m.a(paramList))) {
      return (T)paramList.get(paramInt);
    }
    return null;
  }
  
  public static final <T> String a(Iterable<? extends T> paramIterable, CharSequence paramCharSequence1, CharSequence paramCharSequence2, CharSequence paramCharSequence3, int paramInt, CharSequence paramCharSequence4, b<? super T, ? extends CharSequence> paramb)
  {
    k.b(paramIterable, "receiver$0");
    k.b(paramCharSequence1, "separator");
    k.b(paramCharSequence2, "prefix");
    k.b(paramCharSequence3, "postfix");
    k.b(paramCharSequence4, "truncated");
    paramIterable = ((StringBuilder)m.a(paramIterable, (Appendable)new StringBuilder(), paramCharSequence1, paramCharSequence2, paramCharSequence3, paramInt, paramCharSequence4, paramb)).toString();
    k.a(paramIterable, "joinTo(StringBuilder(), …ed, transform).toString()");
    return paramIterable;
  }
  
  public static final <C extends Collection<? super T>, T> C a(Iterable<? extends T> paramIterable, C paramC)
  {
    k.b(paramIterable, "receiver$0");
    k.b(paramC, "destination");
    paramIterable = paramIterable.iterator();
    while (paramIterable.hasNext())
    {
      Object localObject = paramIterable.next();
      if (localObject != null) {
        paramC.add(localObject);
      }
    }
    return paramC;
  }
  
  public static final <T> List<List<T>> a(Iterable<? extends T> paramIterable, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    k.b(paramIterable, "receiver$0");
    ar.a(paramInt1, paramInt2);
    if (((paramIterable instanceof RandomAccess)) && ((paramIterable instanceof List)))
    {
      paramIterable = (List)paramIterable;
      int k = paramIterable.size();
      localArrayList1 = new ArrayList((k + paramInt2 - 1) / paramInt2);
      int i = 0;
      while (i < k)
      {
        int m = c.k.i.d(paramInt1, k - i);
        if ((m < paramInt1) && (!paramBoolean)) {
          break;
        }
        ArrayList localArrayList2 = new ArrayList(m);
        int j = 0;
        while (j < m)
        {
          localArrayList2.add(paramIterable.get(j + i));
          j += 1;
        }
        localArrayList1.add((List)localArrayList2);
        i += paramInt2;
      }
      return (List)localArrayList1;
    }
    ArrayList localArrayList1 = new ArrayList();
    paramIterable = ar.a(paramIterable.iterator(), paramInt1, paramInt2, paramBoolean, false);
    while (paramIterable.hasNext()) {
      localArrayList1.add((List)paramIterable.next());
    }
    return (List)localArrayList1;
  }
  
  public static final <T> List<T> a(Iterable<? extends T> paramIterable, Comparator<? super T> paramComparator)
  {
    k.b(paramIterable, "receiver$0");
    k.b(paramComparator, "comparator");
    if ((paramIterable instanceof Collection))
    {
      Collection localCollection = (Collection)paramIterable;
      if (localCollection.size() <= 1) {
        return m.g(paramIterable);
      }
      paramIterable = localCollection.toArray(new Object[0]);
      if (paramIterable != null)
      {
        if (paramIterable != null)
        {
          f.a(paramIterable, paramComparator);
          return f.a(paramIterable);
        }
        throw new u("null cannot be cast to non-null type kotlin.Array<T>");
      }
      throw new u("null cannot be cast to non-null type kotlin.Array<T>");
    }
    paramIterable = m.h(paramIterable);
    m.a(paramIterable, paramComparator);
    return paramIterable;
  }
  
  public static final <T> List<T> a(Collection<? extends T> paramCollection, T paramT)
  {
    k.b(paramCollection, "receiver$0");
    ArrayList localArrayList = new ArrayList(paramCollection.size() + 1);
    localArrayList.addAll(paramCollection);
    localArrayList.add(paramT);
    return (List)localArrayList;
  }
  
  public static final <T> boolean a(Iterable<? extends T> paramIterable, T paramT)
  {
    k.b(paramIterable, "receiver$0");
    if ((paramIterable instanceof Collection)) {
      return ((Collection)paramIterable).contains(paramT);
    }
    return m.b(paramIterable, paramT) >= 0;
  }
  
  public static final <T> int b(Iterable<? extends T> paramIterable, T paramT)
  {
    k.b(paramIterable, "receiver$0");
    if ((paramIterable instanceof List)) {
      return ((List)paramIterable).indexOf(paramT);
    }
    int i = 0;
    paramIterable = paramIterable.iterator();
    while (paramIterable.hasNext())
    {
      Object localObject = paramIterable.next();
      if (i < 0) {
        m.a();
      }
      if (k.a(paramT, localObject)) {
        return i;
      }
      i += 1;
    }
    return -1;
  }
  
  public static final <T> T b(Iterable<? extends T> paramIterable)
  {
    k.b(paramIterable, "receiver$0");
    if ((paramIterable instanceof List)) {
      return (T)m.d((List)paramIterable);
    }
    paramIterable = paramIterable.iterator();
    if (paramIterable.hasNext()) {
      return (T)paramIterable.next();
    }
    throw ((Throwable)new NoSuchElementException("Collection is empty."));
  }
  
  public static final <T> T b(Iterable<? extends T> paramIterable, int paramInt)
  {
    k.b(paramIterable, "receiver$0");
    if ((paramIterable instanceof List)) {
      return (T)((List)paramIterable).get(paramInt);
    }
    return (T)m.a(paramIterable, paramInt, (b)new b(paramInt));
  }
  
  public static final <T, C extends Collection<? super T>> C b(Iterable<? extends T> paramIterable, C paramC)
  {
    k.b(paramIterable, "receiver$0");
    k.b(paramC, "destination");
    paramIterable = paramIterable.iterator();
    while (paramIterable.hasNext()) {
      paramC.add(paramIterable.next());
    }
    return paramC;
  }
  
  public static final <T> List<T> b(List<? extends T> paramList, int paramInt)
  {
    k.b(paramList, "receiver$0");
    int i;
    if (paramInt >= 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0)
    {
      if (paramInt == 0) {
        return (List)y.a;
      }
      i = paramList.size();
      if (paramInt >= i) {
        return m.g((Iterable)paramList);
      }
      if (paramInt == 1) {
        return m.a(m.f(paramList));
      }
      ArrayList localArrayList = new ArrayList(paramInt);
      paramInt = i - paramInt;
      while (paramInt < i)
      {
        localArrayList.add(paramList.get(paramInt));
        paramInt += 1;
      }
      return (List)localArrayList;
    }
    paramList = new StringBuilder("Requested element count ");
    paramList.append(paramInt);
    paramList.append(" is less than zero.");
    throw ((Throwable)new IllegalArgumentException(paramList.toString().toString()));
  }
  
  public static final <T> Set<T> b(Iterable<? extends T> paramIterable1, Iterable<? extends T> paramIterable2)
  {
    k.b(paramIterable1, "receiver$0");
    k.b(paramIterable2, "other");
    paramIterable1 = m.l(paramIterable1);
    m.b((Collection)paramIterable1, paramIterable2);
    return paramIterable1;
  }
  
  public static final int[] b(Collection<Integer> paramCollection)
  {
    k.b(paramCollection, "receiver$0");
    int[] arrayOfInt = new int[paramCollection.size()];
    paramCollection = paramCollection.iterator();
    int i = 0;
    while (paramCollection.hasNext())
    {
      arrayOfInt[i] = ((Number)paramCollection.next()).intValue();
      i += 1;
    }
    return arrayOfInt;
  }
  
  public static final <T> T c(Iterable<? extends T> paramIterable)
  {
    k.b(paramIterable, "receiver$0");
    if ((paramIterable instanceof List)) {
      return (T)m.f((List)paramIterable);
    }
    paramIterable = paramIterable.iterator();
    if (paramIterable.hasNext())
    {
      Object localObject;
      do
      {
        localObject = paramIterable.next();
      } while (paramIterable.hasNext());
      return (T)localObject;
    }
    throw ((Throwable)new NoSuchElementException("Collection is empty."));
  }
  
  public static final <T> List<T> c(Iterable<? extends T> paramIterable, int paramInt)
  {
    k.b(paramIterable, "receiver$0");
    int j = 0;
    int i;
    if (paramInt >= 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0)
    {
      if (paramInt == 0) {
        return m.g(paramIterable);
      }
      Object localObject2;
      Object localObject1;
      if ((paramIterable instanceof Collection))
      {
        Collection localCollection = (Collection)paramIterable;
        i = localCollection.size() - paramInt;
        if (i <= 0) {
          return (List)y.a;
        }
        if (i == 1) {
          return m.a(m.c(paramIterable));
        }
        localObject2 = new ArrayList(i);
        localObject1 = localObject2;
        if ((paramIterable instanceof List))
        {
          if ((paramIterable instanceof RandomAccess))
          {
            i = localCollection.size();
            while (paramInt < i)
            {
              ((ArrayList)localObject2).add(((List)paramIterable).get(paramInt));
              paramInt += 1;
            }
          }
          paramIterable = (Iterator)((List)paramIterable).listIterator(paramInt);
          while (paramIterable.hasNext()) {
            ((ArrayList)localObject2).add(paramIterable.next());
          }
          return (List)localObject2;
        }
      }
      else
      {
        localObject1 = new ArrayList();
      }
      paramIterable = paramIterable.iterator();
      i = j;
      while (paramIterable.hasNext())
      {
        localObject2 = paramIterable.next();
        if (i >= paramInt) {
          ((ArrayList)localObject1).add(localObject2);
        }
        i += 1;
      }
      return m.b((List)localObject1);
    }
    paramIterable = new StringBuilder("Requested element count ");
    paramIterable.append(paramInt);
    paramIterable.append(" is less than zero.");
    throw ((Throwable)new IllegalArgumentException(paramIterable.toString().toString()));
  }
  
  public static final <T> List<T> c(Iterable<? extends T> paramIterable1, Iterable<? extends T> paramIterable2)
  {
    k.b(paramIterable1, "receiver$0");
    k.b(paramIterable2, "elements");
    paramIterable2 = m.a(paramIterable2, paramIterable1);
    if (paramIterable2.isEmpty()) {
      return m.g(paramIterable1);
    }
    Collection localCollection = (Collection)new ArrayList();
    paramIterable1 = paramIterable1.iterator();
    while (paramIterable1.hasNext())
    {
      Object localObject = paramIterable1.next();
      if (!paramIterable2.contains(localObject)) {
        localCollection.add(localObject);
      }
    }
    return (List)localCollection;
  }
  
  public static final <T> List<T> c(Iterable<? extends T> paramIterable, T paramT)
  {
    k.b(paramIterable, "receiver$0");
    ArrayList localArrayList = new ArrayList();
    m.a((Collection)localArrayList, paramIterable);
    localArrayList.add(paramT);
    return (List)localArrayList;
  }
  
  public static final <T> List<T> c(Collection<? extends T> paramCollection, Iterable<? extends T> paramIterable)
  {
    k.b(paramCollection, "receiver$0");
    k.b(paramIterable, "elements");
    if ((paramIterable instanceof Collection))
    {
      int i = paramCollection.size();
      Collection localCollection = (Collection)paramIterable;
      paramIterable = new ArrayList(i + localCollection.size());
      paramIterable.addAll(paramCollection);
      paramIterable.addAll(localCollection);
      return (List)paramIterable;
    }
    paramCollection = new ArrayList(paramCollection);
    m.a((Collection)paramCollection, paramIterable);
    return (List)paramCollection;
  }
  
  public static final long[] c(Collection<Long> paramCollection)
  {
    k.b(paramCollection, "receiver$0");
    long[] arrayOfLong = new long[paramCollection.size()];
    paramCollection = paramCollection.iterator();
    int i = 0;
    while (paramCollection.hasNext())
    {
      arrayOfLong[i] = ((Number)paramCollection.next()).longValue();
      i += 1;
    }
    return arrayOfLong;
  }
  
  public static final <T> T d(Iterable<? extends T> paramIterable)
  {
    k.b(paramIterable, "receiver$0");
    if ((paramIterable instanceof List))
    {
      paramIterable = (List)paramIterable;
      if (paramIterable.isEmpty()) {
        return null;
      }
      return (T)paramIterable.get(paramIterable.size() - 1);
    }
    paramIterable = paramIterable.iterator();
    if (!paramIterable.hasNext()) {
      return null;
    }
    Object localObject;
    do
    {
      localObject = paramIterable.next();
    } while (paramIterable.hasNext());
    return (T)localObject;
  }
  
  public static final <T> T d(List<? extends T> paramList)
  {
    k.b(paramList, "receiver$0");
    if (!paramList.isEmpty()) {
      return (T)paramList.get(0);
    }
    throw ((Throwable)new NoSuchElementException("List is empty."));
  }
  
  public static final <T> List<T> d(Iterable<? extends T> paramIterable, int paramInt)
  {
    k.b(paramIterable, "receiver$0");
    int j = 0;
    int i;
    if (paramInt >= 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0)
    {
      if (paramInt == 0) {
        return (List)y.a;
      }
      if ((paramIterable instanceof Collection))
      {
        if (paramInt >= ((Collection)paramIterable).size()) {
          return m.g(paramIterable);
        }
        if (paramInt == 1) {
          return m.a(m.b(paramIterable));
        }
      }
      ArrayList localArrayList = new ArrayList(paramInt);
      paramIterable = paramIterable.iterator();
      i = j;
      while (paramIterable.hasNext())
      {
        Object localObject = paramIterable.next();
        if (i == paramInt) {
          break;
        }
        localArrayList.add(localObject);
        i += 1;
      }
      return m.b((List)localArrayList);
    }
    paramIterable = new StringBuilder("Requested element count ");
    paramIterable.append(paramInt);
    paramIterable.append(" is less than zero.");
    throw ((Throwable)new IllegalArgumentException(paramIterable.toString().toString()));
  }
  
  public static final <T, R> List<n<T, R>> d(Iterable<? extends T> paramIterable, Iterable<? extends R> paramIterable1)
  {
    k.b(paramIterable, "receiver$0");
    k.b(paramIterable1, "other");
    Iterator localIterator1 = paramIterable.iterator();
    Iterator localIterator2 = paramIterable1.iterator();
    paramIterable = new ArrayList(Math.min(m.a(paramIterable, 10), m.a(paramIterable1, 10)));
    while ((localIterator1.hasNext()) && (localIterator2.hasNext())) {
      paramIterable.add(t.a(localIterator1.next(), localIterator2.next()));
    }
    return (List)paramIterable;
  }
  
  public static final <T> List<T> d(Collection<? extends T> paramCollection)
  {
    k.b(paramCollection, "receiver$0");
    return (List)new ArrayList(paramCollection);
  }
  
  public static final <T> T e(List<? extends T> paramList)
  {
    k.b(paramList, "receiver$0");
    if (paramList.isEmpty()) {
      return null;
    }
    return (T)paramList.get(0);
  }
  
  public static final <T> List<T> e(Iterable<? extends T> paramIterable)
  {
    k.b(paramIterable, "receiver$0");
    return (List)m.a(paramIterable, (Collection)new ArrayList());
  }
  
  public static final <T> List<List<T>> e(Iterable<? extends T> paramIterable, int paramInt)
  {
    k.b(paramIterable, "receiver$0");
    return m.a(paramIterable, paramInt, paramInt, true);
  }
  
  public static final <T> T f(List<? extends T> paramList)
  {
    k.b(paramList, "receiver$0");
    if (!paramList.isEmpty()) {
      return (T)paramList.get(m.a(paramList));
    }
    throw ((Throwable)new NoSuchElementException("List is empty."));
  }
  
  public static final <T> List<T> f(Iterable<? extends T> paramIterable)
  {
    k.b(paramIterable, "receiver$0");
    if (((paramIterable instanceof Collection)) && (((Collection)paramIterable).size() <= 1)) {
      return m.g(paramIterable);
    }
    paramIterable = m.h(paramIterable);
    m.c(paramIterable);
    return paramIterable;
  }
  
  public static final <T> T g(List<? extends T> paramList)
  {
    k.b(paramList, "receiver$0");
    if (paramList.isEmpty()) {
      return null;
    }
    return (T)paramList.get(paramList.size() - 1);
  }
  
  public static final <T> List<T> g(Iterable<? extends T> paramIterable)
  {
    k.b(paramIterable, "receiver$0");
    if ((paramIterable instanceof Collection))
    {
      Collection localCollection = (Collection)paramIterable;
      switch (localCollection.size())
      {
      default: 
        return m.d(localCollection);
      case 1: 
        if ((paramIterable instanceof List)) {
          paramIterable = ((List)paramIterable).get(0);
        } else {
          paramIterable = paramIterable.iterator().next();
        }
        return m.a(paramIterable);
      }
      return (List)y.a;
    }
    return m.b(m.h(paramIterable));
  }
  
  public static final <T> T h(List<? extends T> paramList)
  {
    k.b(paramList, "receiver$0");
    switch (paramList.size())
    {
    default: 
      throw ((Throwable)new IllegalArgumentException("List has more than one element."));
    case 1: 
      return (T)paramList.get(0);
    }
    throw ((Throwable)new NoSuchElementException("List is empty."));
  }
  
  public static final <T> List<T> h(Iterable<? extends T> paramIterable)
  {
    k.b(paramIterable, "receiver$0");
    if ((paramIterable instanceof Collection)) {
      return m.d((Collection)paramIterable);
    }
    return (List)m.b(paramIterable, (Collection)new ArrayList());
  }
  
  public static final <T> Set<T> i(Iterable<? extends T> paramIterable)
  {
    k.b(paramIterable, "receiver$0");
    if ((paramIterable instanceof Collection))
    {
      Collection localCollection = (Collection)paramIterable;
      switch (localCollection.size())
      {
      default: 
        return (Set)m.b(paramIterable, (Collection)new LinkedHashSet(ag.a(localCollection.size())));
      case 1: 
        if ((paramIterable instanceof List)) {
          paramIterable = ((List)paramIterable).get(0);
        } else {
          paramIterable = paramIterable.iterator().next();
        }
        return an.a(paramIterable);
      }
      return (Set)aa.a;
    }
    return an.a((Set)m.b(paramIterable, (Collection)new LinkedHashSet()));
  }
  
  public static final <T> Iterable<ab<T>> j(Iterable<? extends T> paramIterable)
  {
    k.b(paramIterable, "receiver$0");
    return (Iterable)new ac((a)new c(paramIterable));
  }
  
  public static final <T> List<T> k(Iterable<? extends T> paramIterable)
  {
    k.b(paramIterable, "receiver$0");
    return m.g((Iterable)m.l(paramIterable));
  }
  
  public static final <T> Set<T> l(Iterable<? extends T> paramIterable)
  {
    k.b(paramIterable, "receiver$0");
    if ((paramIterable instanceof Collection)) {
      return (Set)new LinkedHashSet((Collection)paramIterable);
    }
    return (Set)m.b(paramIterable, (Collection)new LinkedHashSet());
  }
  
  public static final <T extends Comparable<? super T>> T m(Iterable<? extends T> paramIterable)
  {
    k.b(paramIterable, "receiver$0");
    Iterator localIterator = paramIterable.iterator();
    if (!localIterator.hasNext()) {
      return null;
    }
    paramIterable = (Comparable)localIterator.next();
    while (localIterator.hasNext())
    {
      Comparable localComparable = (Comparable)localIterator.next();
      if (paramIterable.compareTo(localComparable) > 0) {
        paramIterable = localComparable;
      }
    }
    return paramIterable;
  }
  
  public static final <T> c.m.i<T> n(Iterable<? extends T> paramIterable)
  {
    k.b(paramIterable, "receiver$0");
    return (c.m.i)new a(paramIterable);
  }
  
  public static final class a
    implements c.m.i<T>
  {
    public a(Iterable paramIterable) {}
    
    public final Iterator<T> a()
    {
      return a.iterator();
    }
  }
  
  static final class b
    extends l
    implements b
  {
    b(int paramInt)
    {
      super();
    }
  }
  
  static final class c
    extends l
    implements a<Iterator<? extends T>>
  {
    c(Iterable paramIterable)
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     c.a.w
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */