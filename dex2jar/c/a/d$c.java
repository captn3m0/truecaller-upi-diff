package c.a;

import c.g.b.a.a;
import java.util.ListIterator;
import java.util.NoSuchElementException;

final class d$c
  extends d<E>.b
  implements a, ListIterator<E>
{
  public d$c(int paramInt)
  {
    super(paramInt);
    d.a locala = d.a;
    int j = paramInt.size();
    int i;
    if ((i >= 0) && (i <= j))
    {
      a = i;
      return;
    }
    paramInt = new StringBuilder("index: ");
    paramInt.append(i);
    paramInt.append(", size: ");
    paramInt.append(j);
    throw ((Throwable)new IndexOutOfBoundsException(paramInt.toString()));
  }
  
  public final void add(E paramE)
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public final boolean hasPrevious()
  {
    return a > 0;
  }
  
  public final int nextIndex()
  {
    return a;
  }
  
  public final E previous()
  {
    if (hasPrevious())
    {
      d locald = c;
      a -= 1;
      return (E)locald.get(a);
    }
    throw ((Throwable)new NoSuchElementException());
  }
  
  public final int previousIndex()
  {
    return a - 1;
  }
  
  public final void set(E paramE)
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
}

/* Location:
 * Qualified Name:     c.a.d.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */