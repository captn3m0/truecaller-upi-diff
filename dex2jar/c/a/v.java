package c.a;

import c.g.b.k;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

public class v
  extends u
{
  public static final <T extends Comparable<? super T>> SortedSet<T> a(Iterable<? extends T> paramIterable)
  {
    k.b(paramIterable, "receiver$0");
    return (SortedSet)m.b(paramIterable, (Collection)new TreeSet());
  }
  
  public static final <T> void c(List<T> paramList)
  {
    k.b(paramList, "receiver$0");
    Collections.reverse(paramList);
  }
}

/* Location:
 * Qualified Name:     c.a.v
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */