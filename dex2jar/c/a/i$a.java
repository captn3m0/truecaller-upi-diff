package c.a;

import c.g.b.k;
import c.k.h;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;

public final class i$a
  extends d<Integer>
  implements RandomAccess
{
  i$a(int[] paramArrayOfInt) {}
  
  public final int a()
  {
    return b.length;
  }
  
  public final boolean contains(Object paramObject)
  {
    if (!(paramObject instanceof Integer)) {
      return false;
    }
    int i = ((Number)paramObject).intValue();
    return f.a(b, i);
  }
  
  public final int indexOf(Object paramObject)
  {
    if (!(paramObject instanceof Integer)) {
      return -1;
    }
    int i = ((Number)paramObject).intValue();
    return f.b(b, i);
  }
  
  public final boolean isEmpty()
  {
    return b.length == 0;
  }
  
  public final int lastIndexOf(Object paramObject)
  {
    if (!(paramObject instanceof Integer)) {
      return -1;
    }
    int i = ((Number)paramObject).intValue();
    paramObject = b;
    k.b(paramObject, "receiver$0");
    k.b(paramObject, "receiver$0");
    k.b(paramObject, "receiver$0");
    Iterator localIterator = m.f((Iterable)new h(0, paramObject.length - 1)).iterator();
    while (localIterator.hasNext())
    {
      int j = ((Number)localIterator.next()).intValue();
      if (i == paramObject[j]) {
        return j;
      }
    }
    return -1;
  }
}

/* Location:
 * Qualified Name:     c.a.i.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */