package c.a;

import c.g.b.a.a;
import java.util.Iterator;
import java.util.NoSuchElementException;

class d$b
  implements a, Iterator<E>
{
  int a;
  
  public boolean hasNext()
  {
    return a < b.size();
  }
  
  public E next()
  {
    if (hasNext())
    {
      d locald = b;
      int i = a;
      a = (i + 1);
      return (E)locald.get(i);
    }
    throw ((Throwable)new NoSuchElementException());
  }
  
  public void remove()
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
}

/* Location:
 * Qualified Name:     c.a.d.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */