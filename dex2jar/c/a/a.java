package c.a;

import c.g.a.b;
import c.g.b.g;
import c.g.b.k;
import c.g.b.l;
import c.u;
import java.util.Collection;
import java.util.Iterator;

public abstract class a<E>
  implements c.g.b.a.a, Collection<E>
{
  public abstract int a();
  
  public boolean add(E paramE)
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public boolean addAll(Collection<? extends E> paramCollection)
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public void clear()
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public boolean contains(Object paramObject)
  {
    if (!((Collection)this).isEmpty())
    {
      Iterator localIterator = iterator();
      while (localIterator.hasNext()) {
        if (k.a(localIterator.next(), paramObject)) {
          return true;
        }
      }
    }
    return false;
  }
  
  public boolean containsAll(Collection<? extends Object> paramCollection)
  {
    k.b(paramCollection, "elements");
    paramCollection = (Iterable)paramCollection;
    if (!((Collection)paramCollection).isEmpty())
    {
      paramCollection = paramCollection.iterator();
      while (paramCollection.hasNext()) {
        if (!contains(paramCollection.next())) {
          return false;
        }
      }
    }
    return true;
  }
  
  public boolean isEmpty()
  {
    return size() == 0;
  }
  
  public boolean remove(Object paramObject)
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public boolean removeAll(Collection<? extends Object> paramCollection)
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public boolean retainAll(Collection<? extends Object> paramCollection)
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public final int size()
  {
    return a();
  }
  
  public Object[] toArray()
  {
    return g.a((Collection)this);
  }
  
  public <T> T[] toArray(T[] paramArrayOfT)
  {
    k.b(paramArrayOfT, "array");
    paramArrayOfT = g.a((Collection)this, paramArrayOfT);
    if (paramArrayOfT != null) {
      return paramArrayOfT;
    }
    throw new u("null cannot be cast to non-null type kotlin.Array<T>");
  }
  
  public String toString()
  {
    return m.a(this, (CharSequence)", ", (CharSequence)"[", (CharSequence)"]", 0, null, (b)new a(this), 24);
  }
  
  static final class a
    extends l
    implements b<E, CharSequence>
  {
    a(a parama)
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     c.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */