package c.a;

import c.g.b.k;
import c.k.h;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class o
  extends n
{
  public static final <T> int a(List<? extends T> paramList)
  {
    k.b(paramList, "receiver$0");
    return paramList.size() - 1;
  }
  
  public static final h a(Collection<?> paramCollection)
  {
    k.b(paramCollection, "receiver$0");
    return new h(0, paramCollection.size() - 1);
  }
  
  public static final <T> Collection<T> a(T[] paramArrayOfT)
  {
    k.b(paramArrayOfT, "receiver$0");
    return (Collection)new e(paramArrayOfT, false);
  }
  
  public static final void a()
  {
    throw ((Throwable)new ArithmeticException("Index overflow has happened."));
  }
  
  public static final <T> List<T> b(T paramT)
  {
    if (paramT != null) {
      return m.a(paramT);
    }
    return (List)y.a;
  }
  
  public static final <T> List<T> b(List<? extends T> paramList)
  {
    k.b(paramList, "receiver$0");
    switch (paramList.size())
    {
    default: 
      return paramList;
    case 1: 
      return m.a(paramList.get(0));
    }
    return (List)y.a;
  }
  
  public static final <T> List<T> b(T... paramVarArgs)
  {
    k.b(paramVarArgs, "elements");
    if (paramVarArgs.length > 0) {
      return f.a(paramVarArgs);
    }
    return (List)y.a;
  }
  
  public static final <T> List<T> c(T... paramVarArgs)
  {
    k.b(paramVarArgs, "elements");
    if (paramVarArgs.length == 0) {
      return (List)new ArrayList();
    }
    return (List)new ArrayList((Collection)new e(paramVarArgs, true));
  }
  
  public static final <T> ArrayList<T> d(T... paramVarArgs)
  {
    k.b(paramVarArgs, "elements");
    if (paramVarArgs.length == 0) {
      return new ArrayList();
    }
    return new ArrayList((Collection)new e(paramVarArgs, true));
  }
  
  public static final <T> List<T> e(T... paramVarArgs)
  {
    k.b(paramVarArgs, "elements");
    return f.d(paramVarArgs);
  }
}

/* Location:
 * Qualified Name:     c.a.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */