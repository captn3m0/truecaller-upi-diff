package c.a;

import c.g.b.k;
import c.n;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class ai
  extends ah
{
  public static final <K, V> Map<K, V> a(n<? extends K, ? extends V> paramn)
  {
    k.b(paramn, "pair");
    paramn = Collections.singletonMap(a, b);
    k.a(paramn, "java.util.Collections.si…(pair.first, pair.second)");
    return paramn;
  }
  
  public static final <K, V> Map<K, V> a(Map<? extends K, ? extends V> paramMap)
  {
    k.b(paramMap, "receiver$0");
    paramMap = (Map.Entry)paramMap.entrySet().iterator().next();
    paramMap = Collections.singletonMap(paramMap.getKey(), paramMap.getValue());
    k.a(paramMap, "java.util.Collections.singletonMap(key, value)");
    k.a(paramMap, "with(entries.iterator().…ingletonMap(key, value) }");
    return paramMap;
  }
}

/* Location:
 * Qualified Name:     c.a.ai
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */