package c.a;

import c.k.h;
import java.util.List;

public final class al<T>
  extends d<T>
{
  private final List<T> b;
  
  public al(List<? extends T> paramList)
  {
    b = paramList;
  }
  
  public final int a()
  {
    return b.size();
  }
  
  public final T get(int paramInt)
  {
    Object localObject = b;
    int i = m.a(this);
    if ((paramInt >= 0) && (i >= paramInt)) {
      return (T)((List)localObject).get(m.a(this) - paramInt);
    }
    localObject = new StringBuilder("Element index ");
    ((StringBuilder)localObject).append(paramInt);
    ((StringBuilder)localObject).append(" must be in range [");
    ((StringBuilder)localObject).append(new h(0, m.a(this)));
    ((StringBuilder)localObject).append("].");
    throw ((Throwable)new IndexOutOfBoundsException(((StringBuilder)localObject).toString()));
  }
}

/* Location:
 * Qualified Name:     c.a.al
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */