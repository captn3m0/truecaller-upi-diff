package c.a;

import c.g.b.a.a;
import c.g.b.c;
import c.g.b.g;
import c.g.b.k;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

final class e<T>
  implements a, Collection<T>
{
  private final T[] a;
  private final boolean b;
  
  public e(T[] paramArrayOfT, boolean paramBoolean)
  {
    a = paramArrayOfT;
    b = paramBoolean;
  }
  
  public final boolean add(T paramT)
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public final boolean addAll(Collection<? extends T> paramCollection)
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public final void clear()
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public final boolean contains(Object paramObject)
  {
    return f.b(a, paramObject);
  }
  
  public final boolean containsAll(Collection<? extends Object> paramCollection)
  {
    k.b(paramCollection, "elements");
    paramCollection = (Iterable)paramCollection;
    if (!((Collection)paramCollection).isEmpty())
    {
      paramCollection = paramCollection.iterator();
      while (paramCollection.hasNext()) {
        if (!contains(paramCollection.next())) {
          return false;
        }
      }
    }
    return true;
  }
  
  public final boolean isEmpty()
  {
    return a.length == 0;
  }
  
  public final Iterator<T> iterator()
  {
    return c.a(a);
  }
  
  public final boolean remove(Object paramObject)
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public final boolean removeAll(Collection<? extends Object> paramCollection)
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public final boolean retainAll(Collection<? extends Object> paramCollection)
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public final Object[] toArray()
  {
    Object[] arrayOfObject = a;
    boolean bool = b;
    k.b(arrayOfObject, "receiver$0");
    if ((bool) && (k.a(arrayOfObject.getClass(), Object[].class))) {
      return arrayOfObject;
    }
    arrayOfObject = Arrays.copyOf(arrayOfObject, arrayOfObject.length, Object[].class);
    k.a(arrayOfObject, "java.util.Arrays.copyOf(… Array<Any?>::class.java)");
    return arrayOfObject;
  }
  
  public final <T> T[] toArray(T[] paramArrayOfT)
  {
    return g.a(this, paramArrayOfT);
  }
}

/* Location:
 * Qualified Name:     c.a.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */