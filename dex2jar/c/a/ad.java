package c.a;

import c.g.b.a.a;
import java.util.Iterator;

public final class ad<T>
  implements a, Iterator<ab<? extends T>>
{
  private int a;
  private final Iterator<T> b;
  
  public ad(Iterator<? extends T> paramIterator)
  {
    b = paramIterator;
  }
  
  public final boolean hasNext()
  {
    return b.hasNext();
  }
  
  public final void remove()
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
}

/* Location:
 * Qualified Name:     c.a.ad
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */