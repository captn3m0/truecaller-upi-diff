package c.a;

import c.g.b.k;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;

public class i
  extends h
{
  public static final List<Integer> a(int[] paramArrayOfInt)
  {
    k.b(paramArrayOfInt, "receiver$0");
    return (List)new a(paramArrayOfInt);
  }
  
  public static final <T> List<T> a(T[] paramArrayOfT)
  {
    k.b(paramArrayOfT, "receiver$0");
    paramArrayOfT = Arrays.asList(paramArrayOfT);
    k.a(paramArrayOfT, "ArraysUtilJVM.asList(this)");
    return paramArrayOfT;
  }
  
  public static final <T> void a(T[] paramArrayOfT, Comparator<? super T> paramComparator)
  {
    k.b(paramArrayOfT, "receiver$0");
    k.b(paramComparator, "comparator");
    if (paramArrayOfT.length > 1) {
      Arrays.sort(paramArrayOfT, paramComparator);
    }
  }
  
  public static final <T> T[] a(T[] paramArrayOfT, T paramT)
  {
    k.b(paramArrayOfT, "receiver$0");
    int i = paramArrayOfT.length;
    paramArrayOfT = Arrays.copyOf(paramArrayOfT, i + 1);
    paramArrayOfT[i] = paramT;
    k.a(paramArrayOfT, "result");
    return paramArrayOfT;
  }
  
  public static final <T> T[] a(T[] paramArrayOfT1, T[] paramArrayOfT2)
  {
    k.b(paramArrayOfT1, "receiver$0");
    k.b(paramArrayOfT2, "elements");
    int i = paramArrayOfT1.length;
    int j = paramArrayOfT2.length;
    paramArrayOfT1 = Arrays.copyOf(paramArrayOfT1, i + j);
    System.arraycopy(paramArrayOfT2, 0, paramArrayOfT1, i, j);
    k.a(paramArrayOfT1, "result");
    return paramArrayOfT1;
  }
  
  public static final class a
    extends d<Integer>
    implements RandomAccess
  {
    a(int[] paramArrayOfInt) {}
    
    public final int a()
    {
      return b.length;
    }
    
    public final boolean contains(Object paramObject)
    {
      if (!(paramObject instanceof Integer)) {
        return false;
      }
      int i = ((Number)paramObject).intValue();
      return f.a(b, i);
    }
    
    public final int indexOf(Object paramObject)
    {
      if (!(paramObject instanceof Integer)) {
        return -1;
      }
      int i = ((Number)paramObject).intValue();
      return f.b(b, i);
    }
    
    public final boolean isEmpty()
    {
      return b.length == 0;
    }
    
    public final int lastIndexOf(Object paramObject)
    {
      if (!(paramObject instanceof Integer)) {
        return -1;
      }
      int i = ((Number)paramObject).intValue();
      paramObject = b;
      k.b(paramObject, "receiver$0");
      k.b(paramObject, "receiver$0");
      k.b(paramObject, "receiver$0");
      Iterator localIterator = m.f((Iterable)new c.k.h(0, paramObject.length - 1)).iterator();
      while (localIterator.hasNext())
      {
        int j = ((Number)localIterator.next()).intValue();
        if (i == paramObject[j]) {
          return j;
        }
      }
      return -1;
    }
  }
}

/* Location:
 * Qualified Name:     c.a.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */