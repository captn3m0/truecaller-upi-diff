package c.a;

import c.g.b.a.a;
import java.util.Iterator;
import java.util.NoSuchElementException;

public abstract class b<T>
  implements a, Iterator<T>
{
  protected as a = as.b;
  private T b;
  
  protected abstract void a();
  
  protected final void a(T paramT)
  {
    b = paramT;
    a = as.a;
  }
  
  public boolean hasNext()
  {
    int i;
    if (a != as.d) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0)
    {
      as localas = a;
      switch (c.a[localas.ordinal()])
      {
      default: 
        a = as.d;
        a();
        if (a == as.a) {
          return true;
        }
        break;
      case 2: 
        return true;
      case 1: 
        return false;
      }
      return false;
    }
    throw ((Throwable)new IllegalArgumentException("Failed requirement.".toString()));
  }
  
  public T next()
  {
    if (hasNext())
    {
      a = as.b;
      return (T)b;
    }
    throw ((Throwable)new NoSuchElementException());
  }
  
  public void remove()
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
}

/* Location:
 * Qualified Name:     c.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */