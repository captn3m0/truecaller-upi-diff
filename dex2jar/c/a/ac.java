package c.a;

import java.util.Iterator;

public final class ac<T>
  implements c.g.b.a.a, Iterable<ab<? extends T>>
{
  private final c.g.a.a<Iterator<T>> a;
  
  public ac(c.g.a.a<? extends Iterator<? extends T>> parama)
  {
    a = parama;
  }
  
  public final Iterator<ab<T>> iterator()
  {
    return (Iterator)new ad((Iterator)a.invoke());
  }
}

/* Location:
 * Qualified Name:     c.a.ac
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */