package c.a;

import c.g.b.k;
import c.u;
import java.util.Arrays;
import java.util.Iterator;
import java.util.RandomAccess;

final class am<T>
  extends d<T>
  implements RandomAccess
{
  final Object[] b;
  int c;
  int d;
  final int e;
  
  public am(int paramInt)
  {
    e = paramInt;
    if (e >= 0) {
      paramInt = 1;
    } else {
      paramInt = 0;
    }
    if (paramInt != 0)
    {
      b = new Object[e];
      return;
    }
    StringBuilder localStringBuilder = new StringBuilder("ring buffer capacity should not be negative but it is ");
    localStringBuilder.append(e);
    throw ((Throwable)new IllegalArgumentException(localStringBuilder.toString().toString()));
  }
  
  private static <T> void a(T[] paramArrayOfT, int paramInt1, int paramInt2)
  {
    while (paramInt1 < paramInt2)
    {
      paramArrayOfT[paramInt1] = null;
      paramInt1 += 1;
    }
  }
  
  public final int a()
  {
    return d;
  }
  
  public final void a(int paramInt)
  {
    int j = 1;
    int i;
    if (paramInt >= 0) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0)
    {
      if (paramInt <= size()) {
        i = j;
      } else {
        i = 0;
      }
      if (i != 0)
      {
        if (paramInt > 0)
        {
          i = c;
          j = e;
          int k = (i + paramInt) % j;
          if (i > k)
          {
            a(b, i, j);
            a(b, 0, k);
          }
          else
          {
            a(b, i, k);
          }
          c = k;
          d = (size() - paramInt);
        }
        return;
      }
      StringBuilder localStringBuilder = new StringBuilder("n shouldn't be greater than the buffer size: n = ");
      localStringBuilder.append(paramInt);
      localStringBuilder.append(", size = ");
      localStringBuilder.append(size());
      throw ((Throwable)new IllegalArgumentException(localStringBuilder.toString().toString()));
    }
    throw ((Throwable)new IllegalArgumentException("n shouldn't be negative but it is ".concat(String.valueOf(paramInt)).toString()));
  }
  
  public final boolean b()
  {
    return size() == e;
  }
  
  public final T get(int paramInt)
  {
    d.a.a(paramInt, size());
    return (T)b[((c + paramInt) % e)];
  }
  
  public final Iterator<T> iterator()
  {
    return (Iterator)new a(this);
  }
  
  public final Object[] toArray()
  {
    return toArray(new Object[size()]);
  }
  
  public final <T> T[] toArray(T[] paramArrayOfT)
  {
    k.b(paramArrayOfT, "array");
    Object localObject = paramArrayOfT;
    if (paramArrayOfT.length < size())
    {
      localObject = Arrays.copyOf(paramArrayOfT, size());
      k.a(localObject, "java.util.Arrays.copyOf(this, newSize)");
    }
    int i1 = size();
    int j = c;
    int n = 0;
    int i = 0;
    int k;
    int m;
    for (;;)
    {
      k = i;
      m = n;
      if (i >= i1) {
        break;
      }
      k = i;
      m = n;
      if (j >= e) {
        break;
      }
      localObject[i] = b[j];
      i += 1;
      j += 1;
    }
    while (k < i1)
    {
      localObject[k] = b[m];
      k += 1;
      m += 1;
    }
    if (localObject.length > size()) {
      localObject[size()] = null;
    }
    if (localObject != null) {
      return (T[])localObject;
    }
    throw new u("null cannot be cast to non-null type kotlin.Array<T>");
  }
  
  public static final class a
    extends b<T>
  {
    private int c;
    private int d;
    
    a()
    {
      c = localam.size();
      d = am.b(localam);
    }
    
    protected final void a()
    {
      if (c == 0)
      {
        a = as.c;
        return;
      }
      a(am.a(b)[d]);
      am localam = b;
      d = ((d + 1) % e);
      c -= 1;
    }
  }
}

/* Location:
 * Qualified Name:     c.a.am
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */