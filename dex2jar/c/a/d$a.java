package c.a;

public final class d$a
{
  public static void a(int paramInt1, int paramInt2)
  {
    if ((paramInt1 >= 0) && (paramInt1 < paramInt2)) {
      return;
    }
    StringBuilder localStringBuilder = new StringBuilder("index: ");
    localStringBuilder.append(paramInt1);
    localStringBuilder.append(", size: ");
    localStringBuilder.append(paramInt2);
    throw ((Throwable)new IndexOutOfBoundsException(localStringBuilder.toString()));
  }
}

/* Location:
 * Qualified Name:     c.a.d.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */