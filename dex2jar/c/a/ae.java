package c.a;

import c.g.b.a.a;
import java.util.Iterator;

public abstract class ae
  implements a, Iterator<Integer>
{
  public abstract int a();
  
  public void remove()
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
}

/* Location:
 * Qualified Name:     c.a.ae
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */