package c.a;

import c.g.b.a.a;
import java.util.Iterator;

public abstract class k
  implements a, Iterator<Byte>
{
  public abstract byte a();
  
  public void remove()
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
}

/* Location:
 * Qualified Name:     c.a.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */