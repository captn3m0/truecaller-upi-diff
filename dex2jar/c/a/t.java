package c.a;

import c.g.b.aa;
import c.g.b.k;
import java.util.Collection;
import java.util.Iterator;

public class t
  extends s
{
  public static final <T> boolean a(Collection<? super T> paramCollection, Iterable<? extends T> paramIterable)
  {
    k.b(paramCollection, "receiver$0");
    k.b(paramIterable, "elements");
    if ((paramIterable instanceof Collection)) {
      return paramCollection.addAll((Collection)paramIterable);
    }
    boolean bool = false;
    paramIterable = paramIterable.iterator();
    while (paramIterable.hasNext()) {
      if (paramCollection.add(paramIterable.next())) {
        bool = true;
      }
    }
    return bool;
  }
  
  public static final <T> boolean a(Collection<? super T> paramCollection, T[] paramArrayOfT)
  {
    k.b(paramCollection, "receiver$0");
    k.b(paramArrayOfT, "elements");
    return paramCollection.addAll((Collection)f.a(paramArrayOfT));
  }
  
  public static final <T> boolean b(Collection<? super T> paramCollection, Iterable<? extends T> paramIterable)
  {
    k.b(paramCollection, "receiver$0");
    k.b(paramIterable, "elements");
    paramIterable = m.a(paramIterable, (Iterable)paramCollection);
    return aa.a(paramCollection).retainAll(paramIterable);
  }
}

/* Location:
 * Qualified Name:     c.a.t
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */