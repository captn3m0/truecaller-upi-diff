package c.a;

import c.g.b.k;
import java.util.Map;
import java.util.NoSuchElementException;

class ah
{
  public static final <K, V> V a(Map<K, ? extends V> paramMap, K paramK)
  {
    k.b(paramMap, "receiver$0");
    if ((paramMap instanceof af)) {
      return (V)((af)paramMap).a();
    }
    Object localObject = paramMap.get(paramK);
    if (localObject == null)
    {
      if (paramMap.containsKey(paramK)) {
        return (V)localObject;
      }
      paramMap = new StringBuilder("Key ");
      paramMap.append(paramK);
      paramMap.append(" is missing in the map.");
      throw ((Throwable)new NoSuchElementException(paramMap.toString()));
    }
    return (V)localObject;
  }
}

/* Location:
 * Qualified Name:     c.a.ah
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */