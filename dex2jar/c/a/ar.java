package c.a;

import c.d.a.a;
import c.d.b.a.f;
import c.d.b.a.j;
import c.d.c;
import c.g.a.m;
import c.m.i;
import c.m.l;
import c.o.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class ar
{
  public static final <T> i<List<T>> a(i<? extends T> parami, final int paramInt1, final int paramInt2)
  {
    c.g.b.k.b(parami, "receiver$0");
    a(paramInt1, paramInt2);
    return (i)new b(parami, paramInt1, paramInt2);
  }
  
  public static final <T> Iterator<List<T>> a(final Iterator<? extends T> paramIterator, final int paramInt1, int paramInt2, final boolean paramBoolean1, final boolean paramBoolean2)
  {
    c.g.b.k.b(paramIterator, "iterator");
    if (!paramIterator.hasNext()) {
      return (Iterator)x.a;
    }
    return l.b((m)new a(paramInt2, paramInt1, paramIterator, paramBoolean2, paramBoolean1, null));
  }
  
  public static final void a(int paramInt1, int paramInt2)
  {
    int i;
    if ((paramInt1 > 0) && (paramInt2 > 0)) {
      i = 1;
    } else {
      i = 0;
    }
    if (i == 0)
    {
      Object localObject;
      if (paramInt1 != paramInt2)
      {
        localObject = new StringBuilder("Both size ");
        ((StringBuilder)localObject).append(paramInt1);
        ((StringBuilder)localObject).append(" and step ");
        ((StringBuilder)localObject).append(paramInt2);
        ((StringBuilder)localObject).append(" must be greater than zero.");
        localObject = ((StringBuilder)localObject).toString();
      }
      else
      {
        localObject = new StringBuilder("size ");
        ((StringBuilder)localObject).append(paramInt1);
        ((StringBuilder)localObject).append(" must be greater than zero.");
        localObject = ((StringBuilder)localObject).toString();
      }
      throw ((Throwable)new IllegalArgumentException(localObject.toString()));
    }
  }
  
  @f(b="SlidingWindow.kt", c={33, 39, 46, 52, 55}, d="invokeSuspend", e="kotlin/collections/SlidingWindowKt$windowedIterator$1")
  static final class a
    extends j
    implements m<c.m.k<? super List<? extends T>>, c<? super c.x>, Object>
  {
    Object a;
    Object b;
    Object c;
    Object d;
    int e;
    int f;
    int g;
    private c.m.k m;
    
    a(int paramInt1, int paramInt2, Iterator paramIterator, boolean paramBoolean1, boolean paramBoolean2, c paramc)
    {
      super();
    }
    
    public final c<c.x> a(Object paramObject, c<?> paramc)
    {
      c.g.b.k.b(paramc, "completion");
      paramc = new a(h, paramInt1, paramIterator, paramBoolean2, paramBoolean1, paramc);
      m = ((c.m.k)paramObject);
      return paramc;
    }
    
    public final Object a(Object paramObject)
    {
      Object localObject3 = a.a;
      Object localObject2;
      int n;
      Object localObject4;
      Object localObject6;
      Object localObject7;
      Object localObject5;
      Object localObject1;
      Object localObject8;
      int i1;
      switch (g)
      {
      default: 
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
      case 5: 
        if (!(paramObject instanceof o.b)) {
          break label945;
        }
        throw a;
      case 4: 
        localObject2 = (am)b;
        n = e;
        localObject4 = (c.m.k)a;
        if (!(paramObject instanceof o.b))
        {
          localObject6 = this;
          break label878;
        }
        throw a;
      case 3: 
        localObject4 = (Iterator)d;
        localObject2 = (am)b;
        n = e;
        localObject6 = (c.m.k)a;
        if (!(paramObject instanceof o.b))
        {
          localObject7 = this;
          break label747;
        }
        throw a;
      case 2: 
        if (!(paramObject instanceof o.b)) {
          break label945;
        }
        throw a;
      case 1: 
        localObject5 = (Iterator)d;
        localObject1 = (ArrayList)b;
        n = e;
        localObject6 = (c.m.k)a;
        if (!(paramObject instanceof o.b))
        {
          localObject2 = this;
          localObject8 = localObject3;
        }
        else
        {
          throw a;
        }
        break;
      case 0: 
        if ((paramObject instanceof o.b)) {
          break label949;
        }
        paramObject = m;
        i1 = h;
        n = paramInt1;
        i1 -= n;
        if (i1 < 0) {
          break label561;
        }
        localObject1 = new ArrayList(n);
        localObject5 = paramIterator;
        localObject7 = this;
        n = 0;
        localObject4 = paramObject;
        paramObject = localObject1;
      }
      while (((Iterator)localObject5).hasNext())
      {
        localObject1 = ((Iterator)localObject5).next();
        if (n > 0)
        {
          n -= 1;
        }
        else
        {
          ((ArrayList)paramObject).add(localObject1);
          if (((ArrayList)paramObject).size() == paramInt1)
          {
            a = localObject4;
            e = i1;
            b = paramObject;
            f = n;
            c = localObject1;
            d = localObject5;
            g = 1;
            localObject2 = localObject7;
            localObject1 = paramObject;
            n = i1;
            localObject6 = localObject4;
            localObject8 = localObject3;
            if (((c.m.k)localObject4).a(paramObject, (c)localObject7) == localObject3) {
              return localObject3;
            }
            if (paramBoolean2)
            {
              ((ArrayList)localObject1).clear();
              paramObject = localObject1;
            }
            else
            {
              paramObject = new ArrayList(paramInt1);
            }
            int i2 = n;
            localObject7 = localObject2;
            i1 = n;
            localObject4 = localObject6;
            localObject3 = localObject8;
            n = i2;
          }
        }
      }
      if (((((Collection)paramObject).isEmpty() ^ true)) && ((paramBoolean1) || (((ArrayList)paramObject).size() == paramInt1)))
      {
        e = i1;
        a = paramObject;
        f = n;
        g = 2;
        if (((c.m.k)localObject4).a(paramObject, (c)localObject7) == localObject3)
        {
          return localObject3;
          label561:
          localObject2 = new am(n);
          localObject4 = paramIterator;
          localObject1 = paramObject;
          paramObject = this;
          n = i1;
          while (((Iterator)localObject4).hasNext())
          {
            localObject6 = ((Iterator)localObject4).next();
            if (!((am)localObject2).b())
            {
              b[((c + localObject2.size()) % e)] = localObject6;
              d = (((am)localObject2).size() + 1);
              if (((am)localObject2).b())
              {
                if (paramBoolean2) {
                  localObject5 = (List)localObject2;
                } else {
                  localObject5 = (List)new ArrayList((Collection)localObject2);
                }
                a = localObject1;
                e = n;
                b = localObject2;
                c = localObject6;
                d = localObject4;
                g = 3;
                localObject6 = localObject1;
                localObject7 = paramObject;
                if (((c.m.k)localObject1).a(localObject5, (c)paramObject) == localObject3) {
                  return localObject3;
                }
                label747:
                ((am)localObject2).a(h);
                localObject1 = localObject6;
                paramObject = localObject7;
              }
            }
            else
            {
              throw ((Throwable)new IllegalStateException("ring buffer is full"));
            }
          }
          if (paramBoolean1)
          {
            while (((am)localObject2).size() > h)
            {
              if (paramBoolean2) {
                localObject5 = (List)localObject2;
              } else {
                localObject5 = (List)new ArrayList((Collection)localObject2);
              }
              a = localObject1;
              e = n;
              b = localObject2;
              g = 4;
              localObject4 = localObject1;
              localObject6 = paramObject;
              if (((c.m.k)localObject1).a(localObject5, (c)paramObject) == localObject3) {
                return localObject3;
              }
              label878:
              ((am)localObject2).a(h);
              localObject1 = localObject4;
              paramObject = localObject6;
            }
            if ((true ^ ((Collection)localObject2).isEmpty()))
            {
              e = n;
              a = localObject2;
              g = 5;
              if (((c.m.k)localObject1).a(localObject2, (c)paramObject) == localObject3) {
                return localObject3;
              }
            }
          }
        }
      }
      label945:
      return c.x.a;
      label949:
      throw a;
    }
    
    public final Object invoke(Object paramObject1, Object paramObject2)
    {
      return ((a)a(paramObject1, (c)paramObject2)).a(c.x.a);
    }
  }
  
  public static final class b
    implements i<List<? extends T>>
  {
    public b(i parami, int paramInt1, int paramInt2) {}
    
    public final Iterator<List<? extends T>> a()
    {
      return ar.a(a.a(), paramInt1, paramInt2, d, e);
    }
  }
}

/* Location:
 * Qualified Name:     c.a.ar
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */