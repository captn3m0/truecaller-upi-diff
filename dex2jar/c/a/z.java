package c.a;

import c.g.b.a.a;
import c.g.b.k;
import java.io.Serializable;
import java.util.Map;

final class z
  implements a, Serializable, Map
{
  public static final z a = new z();
  private static final long serialVersionUID = 8246714829545688274L;
  
  private final Object readResolve()
  {
    return a;
  }
  
  public final void clear()
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public final boolean containsKey(Object paramObject)
  {
    return false;
  }
  
  public final boolean containsValue(Object paramObject)
  {
    if (!(paramObject instanceof Void)) {
      return false;
    }
    k.b((Void)paramObject, "value");
    return false;
  }
  
  public final boolean equals(Object paramObject)
  {
    return ((paramObject instanceof Map)) && (((Map)paramObject).isEmpty());
  }
  
  public final int hashCode()
  {
    return 0;
  }
  
  public final boolean isEmpty()
  {
    return true;
  }
  
  public final void putAll(Map paramMap)
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public final Object remove(Object paramObject)
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public final String toString()
  {
    return "{}";
  }
}

/* Location:
 * Qualified Name:     c.a.z
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */