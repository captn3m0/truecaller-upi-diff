package c.a;

import c.g.b.k;
import c.n;
import c.u;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class aj
  extends ai
{
  public static final int a(int paramInt)
  {
    if (paramInt < 3) {
      return paramInt + 1;
    }
    if (paramInt < 1073741824) {
      return paramInt + paramInt / 3;
    }
    return Integer.MAX_VALUE;
  }
  
  public static final <K, V> Map<K, V> a()
  {
    z localz = z.a;
    if (localz != null) {
      return (Map)localz;
    }
    throw new u("null cannot be cast to non-null type kotlin.collections.Map<K, V>");
  }
  
  public static final <K, V> Map<K, V> a(Iterable<? extends n<? extends K, ? extends V>> paramIterable)
  {
    k.b(paramIterable, "receiver$0");
    if ((paramIterable instanceof Collection))
    {
      Collection localCollection = (Collection)paramIterable;
      switch (localCollection.size())
      {
      default: 
        return ag.a(paramIterable, (Map)new LinkedHashMap(ag.a(localCollection.size())));
      case 1: 
        if ((paramIterable instanceof List)) {
          paramIterable = ((List)paramIterable).get(0);
        } else {
          paramIterable = paramIterable.iterator().next();
        }
        return ag.a((n)paramIterable);
      }
      return ag.a();
    }
    return ag.b(ag.a(paramIterable, (Map)new LinkedHashMap()));
  }
  
  public static final <K, V, M extends Map<? super K, ? super V>> M a(Iterable<? extends n<? extends K, ? extends V>> paramIterable, M paramM)
  {
    k.b(paramIterable, "receiver$0");
    k.b(paramM, "destination");
    ag.a(paramM, paramIterable);
    return paramM;
  }
  
  public static final <K, V> Map<K, V> a(Map<? extends K, ? extends V> paramMap, n<? extends K, ? extends V> paramn)
  {
    k.b(paramMap, "receiver$0");
    k.b(paramn, "pair");
    if (paramMap.isEmpty()) {
      return ag.a(paramn);
    }
    paramMap = new LinkedHashMap(paramMap);
    paramMap.put(a, b);
    return (Map)paramMap;
  }
  
  public static final <K, V> Map<K, V> a(n<? extends K, ? extends V>... paramVarArgs)
  {
    k.b(paramVarArgs, "pairs");
    if (paramVarArgs.length > 0) {
      return ag.a(paramVarArgs, (Map)new LinkedHashMap(ag.a(paramVarArgs.length)));
    }
    return ag.a();
  }
  
  public static final <K, V, M extends Map<? super K, ? super V>> M a(n<? extends K, ? extends V>[] paramArrayOfn, M paramM)
  {
    k.b(paramArrayOfn, "receiver$0");
    k.b(paramM, "destination");
    ag.a(paramM, paramArrayOfn);
    return paramM;
  }
  
  public static final <K, V> void a(Map<? super K, ? super V> paramMap, Iterable<? extends n<? extends K, ? extends V>> paramIterable)
  {
    k.b(paramMap, "receiver$0");
    k.b(paramIterable, "pairs");
    paramIterable = paramIterable.iterator();
    while (paramIterable.hasNext())
    {
      n localn = (n)paramIterable.next();
      paramMap.put(a, b);
    }
  }
  
  public static final <K, V> void a(Map<? super K, ? super V> paramMap, n<? extends K, ? extends V>[] paramArrayOfn)
  {
    k.b(paramMap, "receiver$0");
    k.b(paramArrayOfn, "pairs");
    int j = paramArrayOfn.length;
    int i = 0;
    while (i < j)
    {
      n<? extends K, ? extends V> localn = paramArrayOfn[i];
      paramMap.put(a, b);
      i += 1;
    }
  }
  
  public static final <K, V> V b(Map<K, ? extends V> paramMap, K paramK)
  {
    k.b(paramMap, "receiver$0");
    return (V)ag.a(paramMap, paramK);
  }
  
  public static final <K, V> Map<K, V> b(Map<K, ? extends V> paramMap)
  {
    k.b(paramMap, "receiver$0");
    switch (paramMap.size())
    {
    default: 
      return paramMap;
    case 1: 
      return ag.a(paramMap);
    }
    return ag.a();
  }
  
  public static final <K, V> Map<K, V> b(n<? extends K, ? extends V>... paramVarArgs)
  {
    k.b(paramVarArgs, "pairs");
    Map localMap = (Map)new LinkedHashMap(ag.a(paramVarArgs.length));
    ag.a(localMap, paramVarArgs);
    return localMap;
  }
  
  public static final <K, V> HashMap<K, V> c(n<? extends K, ? extends V>... paramVarArgs)
  {
    k.b(paramVarArgs, "pairs");
    HashMap localHashMap = new HashMap(ag.a(1));
    ag.a((Map)localHashMap, paramVarArgs);
    return localHashMap;
  }
}

/* Location:
 * Qualified Name:     c.a.aj
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */