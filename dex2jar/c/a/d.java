package c.a;

import c.g.b.k;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.RandomAccess;

public abstract class d<E>
  extends a<E>
  implements c.g.b.a.a, List<E>
{
  public static final a a = new a((byte)0);
  
  public void add(int paramInt, E paramE)
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public boolean addAll(int paramInt, Collection<? extends E> paramCollection)
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == (d)this) {
      return true;
    }
    if (!(paramObject instanceof List)) {
      return false;
    }
    Object localObject = (Collection)this;
    paramObject = (Collection)paramObject;
    k.b(localObject, "c");
    k.b(paramObject, "other");
    if (((Collection)localObject).size() != ((Collection)paramObject).size()) {
      return false;
    }
    paramObject = ((Collection)paramObject).iterator();
    localObject = ((Collection)localObject).iterator();
    while (((Iterator)localObject).hasNext()) {
      if ((k.a(((Iterator)localObject).next(), ((Iterator)paramObject).next()) ^ true)) {
        return false;
      }
    }
    return true;
  }
  
  public abstract E get(int paramInt);
  
  public int hashCode()
  {
    Object localObject1 = (Collection)this;
    k.b(localObject1, "c");
    localObject1 = ((Collection)localObject1).iterator();
    int j;
    for (int i = 1; ((Iterator)localObject1).hasNext(); i = i * 31 + j)
    {
      Object localObject2 = ((Iterator)localObject1).next();
      if (localObject2 != null) {
        j = localObject2.hashCode();
      } else {
        j = 0;
      }
    }
    return i;
  }
  
  public int indexOf(Object paramObject)
  {
    Iterator localIterator = iterator();
    int i = 0;
    while (localIterator.hasNext())
    {
      if (k.a(localIterator.next(), paramObject)) {
        return i;
      }
      i += 1;
    }
    return -1;
  }
  
  public Iterator<E> iterator()
  {
    return (Iterator)new b();
  }
  
  public int lastIndexOf(Object paramObject)
  {
    ListIterator localListIterator = listIterator(size());
    while (localListIterator.hasPrevious()) {
      if (k.a(localListIterator.previous(), paramObject)) {
        return localListIterator.nextIndex();
      }
    }
    return -1;
  }
  
  public ListIterator<E> listIterator()
  {
    return (ListIterator)new c(0);
  }
  
  public ListIterator<E> listIterator(int paramInt)
  {
    return (ListIterator)new c(paramInt);
  }
  
  public E remove(int paramInt)
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public E set(int paramInt, E paramE)
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public List<E> subList(int paramInt1, int paramInt2)
  {
    return (List)new d(this, paramInt1, paramInt2);
  }
  
  public static final class a
  {
    public static void a(int paramInt1, int paramInt2)
    {
      if ((paramInt1 >= 0) && (paramInt1 < paramInt2)) {
        return;
      }
      StringBuilder localStringBuilder = new StringBuilder("index: ");
      localStringBuilder.append(paramInt1);
      localStringBuilder.append(", size: ");
      localStringBuilder.append(paramInt2);
      throw ((Throwable)new IndexOutOfBoundsException(localStringBuilder.toString()));
    }
  }
  
  class b
    implements c.g.b.a.a, Iterator<E>
  {
    int a;
    
    public boolean hasNext()
    {
      return a < b.size();
    }
    
    public E next()
    {
      if (hasNext())
      {
        d locald = b;
        int i = a;
        a = (i + 1);
        return (E)locald.get(i);
      }
      throw ((Throwable)new NoSuchElementException());
    }
    
    public void remove()
    {
      throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
  }
  
  final class c
    extends d<E>.b
    implements c.g.b.a.a, ListIterator<E>
  {
    public c()
    {
      super();
      d.a locala = d.a;
      int j = size();
      int i;
      if ((i >= 0) && (i <= j))
      {
        a = i;
        return;
      }
      this$1 = new StringBuilder("index: ");
      append(i);
      append(", size: ");
      append(j);
      throw ((Throwable)new IndexOutOfBoundsException(toString()));
    }
    
    public final void add(E paramE)
    {
      throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    public final boolean hasPrevious()
    {
      return a > 0;
    }
    
    public final int nextIndex()
    {
      return a;
    }
    
    public final E previous()
    {
      if (hasPrevious())
      {
        d locald = d.this;
        a -= 1;
        return (E)locald.get(a);
      }
      throw ((Throwable)new NoSuchElementException());
    }
    
    public final int previousIndex()
    {
      return a - 1;
    }
    
    public final void set(E paramE)
    {
      throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
  }
  
  static final class d<E>
    extends d<E>
    implements RandomAccess
  {
    private int b;
    private final d<E> c;
    private final int d;
    
    public d(d<? extends E> paramd, int paramInt1, int paramInt2)
    {
      c = paramd;
      d = paramInt1;
      paramInt1 = d;
      int i = c.size();
      if ((paramInt1 >= 0) && (paramInt2 <= i))
      {
        if (paramInt1 <= paramInt2)
        {
          b = (paramInt2 - d);
          return;
        }
        paramd = new StringBuilder("fromIndex: ");
        paramd.append(paramInt1);
        paramd.append(" > toIndex: ");
        paramd.append(paramInt2);
        throw ((Throwable)new IllegalArgumentException(paramd.toString()));
      }
      paramd = new StringBuilder("fromIndex: ");
      paramd.append(paramInt1);
      paramd.append(", toIndex: ");
      paramd.append(paramInt2);
      paramd.append(", size: ");
      paramd.append(i);
      throw ((Throwable)new IndexOutOfBoundsException(paramd.toString()));
    }
    
    public final int a()
    {
      return b;
    }
    
    public final E get(int paramInt)
    {
      d.a.a(paramInt, b);
      return (E)c.get(d + paramInt);
    }
  }
}

/* Location:
 * Qualified Name:     c.a.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */