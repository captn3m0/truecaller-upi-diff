package c.a;

import c.g.b.a.a;
import c.g.b.g;
import c.g.b.k;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

public final class y
  implements a, Serializable, List, RandomAccess
{
  public static final y a = new y();
  private static final long serialVersionUID = -7390468764508069838L;
  
  private final Object readResolve()
  {
    return a;
  }
  
  public final boolean addAll(int paramInt, Collection paramCollection)
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public final boolean addAll(Collection paramCollection)
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public final void clear()
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public final boolean contains(Object paramObject)
  {
    if (!(paramObject instanceof Void)) {
      return false;
    }
    k.b((Void)paramObject, "element");
    return false;
  }
  
  public final boolean containsAll(Collection paramCollection)
  {
    k.b(paramCollection, "elements");
    return paramCollection.isEmpty();
  }
  
  public final boolean equals(Object paramObject)
  {
    return ((paramObject instanceof List)) && (((List)paramObject).isEmpty());
  }
  
  public final int hashCode()
  {
    return 1;
  }
  
  public final int indexOf(Object paramObject)
  {
    if (!(paramObject instanceof Void)) {
      return -1;
    }
    k.b((Void)paramObject, "element");
    return -1;
  }
  
  public final boolean isEmpty()
  {
    return true;
  }
  
  public final Iterator iterator()
  {
    return (Iterator)x.a;
  }
  
  public final int lastIndexOf(Object paramObject)
  {
    if (!(paramObject instanceof Void)) {
      return -1;
    }
    k.b((Void)paramObject, "element");
    return -1;
  }
  
  public final ListIterator listIterator()
  {
    return (ListIterator)x.a;
  }
  
  public final ListIterator listIterator(int paramInt)
  {
    if (paramInt == 0) {
      return (ListIterator)x.a;
    }
    throw ((Throwable)new IndexOutOfBoundsException("Index: ".concat(String.valueOf(paramInt))));
  }
  
  public final boolean remove(Object paramObject)
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public final boolean removeAll(Collection paramCollection)
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public final boolean retainAll(Collection paramCollection)
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
  
  public final List subList(int paramInt1, int paramInt2)
  {
    if ((paramInt1 == 0) && (paramInt2 == 0)) {
      return (List)this;
    }
    StringBuilder localStringBuilder = new StringBuilder("fromIndex: ");
    localStringBuilder.append(paramInt1);
    localStringBuilder.append(", toIndex: ");
    localStringBuilder.append(paramInt2);
    throw ((Throwable)new IndexOutOfBoundsException(localStringBuilder.toString()));
  }
  
  public final Object[] toArray()
  {
    return g.a(this);
  }
  
  public final <T> T[] toArray(T[] paramArrayOfT)
  {
    return g.a(this, paramArrayOfT);
  }
  
  public final String toString()
  {
    return "[]";
  }
}

/* Location:
 * Qualified Name:     c.a.y
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */