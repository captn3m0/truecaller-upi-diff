package c.a;

import java.util.RandomAccess;

final class d$d<E>
  extends d<E>
  implements RandomAccess
{
  private int b;
  private final d<E> c;
  private final int d;
  
  public d$d(d<? extends E> paramd, int paramInt1, int paramInt2)
  {
    c = paramd;
    d = paramInt1;
    paramInt1 = d;
    int i = c.size();
    if ((paramInt1 >= 0) && (paramInt2 <= i))
    {
      if (paramInt1 <= paramInt2)
      {
        b = (paramInt2 - d);
        return;
      }
      paramd = new StringBuilder("fromIndex: ");
      paramd.append(paramInt1);
      paramd.append(" > toIndex: ");
      paramd.append(paramInt2);
      throw ((Throwable)new IllegalArgumentException(paramd.toString()));
    }
    paramd = new StringBuilder("fromIndex: ");
    paramd.append(paramInt1);
    paramd.append(", toIndex: ");
    paramd.append(paramInt2);
    paramd.append(", size: ");
    paramd.append(i);
    throw ((Throwable)new IndexOutOfBoundsException(paramd.toString()));
  }
  
  public final int a()
  {
    return b;
  }
  
  public final E get(int paramInt)
  {
    d.a.a(paramInt, b);
    return (E)c.get(d + paramInt);
  }
}

/* Location:
 * Qualified Name:     c.a.d.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */