package c.a;

public enum as
{
  static
  {
    as localas1 = new as("Ready", 0);
    a = localas1;
    as localas2 = new as("NotReady", 1);
    b = localas2;
    as localas3 = new as("Done", 2);
    c = localas3;
    as localas4 = new as("Failed", 3);
    d = localas4;
    e = new as[] { localas1, localas2, localas3, localas4 };
  }
  
  private as() {}
}

/* Location:
 * Qualified Name:     c.a.as
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */