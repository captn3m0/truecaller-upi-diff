package c.a;

import c.g.b.k;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class ap
  extends ao
{
  public static final <T> Set<T> a(Set<? extends T> paramSet)
  {
    k.b(paramSet, "receiver$0");
    switch (paramSet.size())
    {
    default: 
      return paramSet;
    case 1: 
      return an.a(paramSet.iterator().next());
    }
    return (Set)aa.a;
  }
  
  public static final <T> Set<T> a(T... paramVarArgs)
  {
    k.b(paramVarArgs, "elements");
    if (paramVarArgs.length > 0) {
      return f.h(paramVarArgs);
    }
    return (Set)aa.a;
  }
  
  public static final <T> Set<T> b(T... paramVarArgs)
  {
    k.b(paramVarArgs, "elements");
    return (Set)f.b(paramVarArgs, (Collection)new LinkedHashSet(ag.a(paramVarArgs.length)));
  }
}

/* Location:
 * Qualified Name:     c.a.ap
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */