package c;

import c.g.b.k;
import java.io.Serializable;

public final class n<A, B>
  implements Serializable
{
  public final A a;
  public final B b;
  
  public n(A paramA, B paramB)
  {
    a = paramA;
    b = paramB;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject) {
      if ((paramObject instanceof n))
      {
        paramObject = (n)paramObject;
        if ((k.a(a, a)) && (k.a(b, b))) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    Object localObject = a;
    int j = 0;
    int i;
    if (localObject != null) {
      i = localObject.hashCode();
    } else {
      i = 0;
    }
    localObject = b;
    if (localObject != null) {
      j = localObject.hashCode();
    }
    return i * 31 + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("(");
    localStringBuilder.append(a);
    localStringBuilder.append(", ");
    localStringBuilder.append(b);
    localStringBuilder.append(')');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     c.n
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */