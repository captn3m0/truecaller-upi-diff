package c.l;

import c.g.a.b;

public abstract interface i<T, R>
  extends b<T, R>, g<R>
{
  public abstract R a(T paramT);
  
  public abstract a<T, R> e();
  
  public static abstract interface a<T, R>
    extends b<T, R>, g.a<R>
  {}
}

/* Location:
 * Qualified Name:     c.l.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */