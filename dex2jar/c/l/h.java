package c.l;

import c.g.a.a;

public abstract interface h<R>
  extends a<R>, g<R>
{
  public abstract a<R> e();
  
  public abstract R j();
  
  public static abstract interface a<R>
    extends a<R>, g.a<R>
  {}
}

/* Location:
 * Qualified Name:     c.l.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */