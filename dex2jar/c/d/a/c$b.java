package c.d.a;

import c.d.b.a.d;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.g.b.aa;
import c.u;

public final class c$b
  extends d
{
  private int e;
  
  public c$b(c paramc1, f paramf1, c paramc2, f paramf2, m paramm, Object paramObject)
  {
    super(paramc2, paramf2);
  }
  
  public final Object a(Object paramObject)
  {
    switch (e)
    {
    default: 
      throw ((Throwable)new IllegalStateException("This coroutine had already completed".toString()));
    case 1: 
      e = 2;
      return paramObject;
    }
    e = 1;
    paramObject = (c)this;
    m localm = c;
    if (localm != null) {
      return ((m)aa.a(localm, 2)).invoke(d, paramObject);
    }
    throw new u("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
  }
}

/* Location:
 * Qualified Name:     c.d.a.c.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */