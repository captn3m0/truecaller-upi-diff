package c.d.a;

import c.d.b.a.i;
import c.d.c;
import c.g.a.m;
import c.g.b.aa;
import c.u;

public final class c$a
  extends i
{
  private int d;
  
  public c$a(c paramc1, c paramc2, m paramm, Object paramObject)
  {
    super(paramc2);
  }
  
  public final Object a(Object paramObject)
  {
    switch (d)
    {
    default: 
      throw ((Throwable)new IllegalStateException("This coroutine had already completed".toString()));
    case 1: 
      d = 2;
      return paramObject;
    }
    d = 1;
    paramObject = (c)this;
    m localm = b;
    if (localm != null) {
      return ((m)aa.a(localm, 2)).invoke(c, paramObject);
    }
    throw new u("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
  }
}

/* Location:
 * Qualified Name:     c.d.a.c.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */