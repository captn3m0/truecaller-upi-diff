package c.d.a;

public enum a
{
  static
  {
    a locala1 = new a("COROUTINE_SUSPENDED", 0);
    a = locala1;
    a locala2 = new a("UNDECIDED", 1);
    b = locala2;
    a locala3 = new a("RESUMED", 2);
    c = locala3;
    d = new a[] { locala1, locala2, locala3 };
  }
  
  private a() {}
}

/* Location:
 * Qualified Name:     c.d.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */