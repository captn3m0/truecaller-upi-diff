package c.d.a;

import c.d.b.a.a;
import c.d.b.a.d;
import c.d.b.a.i;
import c.d.f;
import c.d.g;
import c.g.a.m;
import c.g.b.aa;
import c.g.b.k;
import c.u;
import c.x;

public class c
{
  public static final <T> c.d.c<T> a(c.d.c<? super T> paramc)
  {
    k.b(paramc, "receiver$0");
    if (!(paramc instanceof d)) {
      localObject = null;
    } else {
      localObject = paramc;
    }
    d locald = (d)localObject;
    Object localObject = paramc;
    if (locald != null)
    {
      localObject = locald.r_();
      if (localObject == null) {
        return paramc;
      }
    }
    return (c.d.c<T>)localObject;
  }
  
  public static final <R, T> c.d.c<x> a(final m<? super R, ? super c.d.c<? super T>, ? extends Object> paramm, final R paramR, c.d.c<? super T> paramc)
  {
    k.b(paramm, "receiver$0");
    k.b(paramc, "completion");
    k.b(paramc, "completion");
    if ((paramm instanceof a)) {
      return ((a)paramm).a(paramR, paramc);
    }
    final f localf = paramc.ao_();
    if (localf == g.a)
    {
      if (paramc != null) {
        return (c.d.c)new a(paramc, paramc, paramm, paramR);
      }
      throw new u("null cannot be cast to non-null type kotlin.coroutines.Continuation<kotlin.Any?>");
    }
    if (paramc != null) {
      return (c.d.c)new b(paramc, localf, paramc, localf, paramm, paramR);
    }
    throw new u("null cannot be cast to non-null type kotlin.coroutines.Continuation<kotlin.Any?>");
  }
  
  public static final class a
    extends i
  {
    private int d;
    
    public a(c.d.c paramc1, c.d.c paramc2, m paramm, Object paramObject)
    {
      super();
    }
    
    public final Object a(Object paramObject)
    {
      switch (d)
      {
      default: 
        throw ((Throwable)new IllegalStateException("This coroutine had already completed".toString()));
      case 1: 
        d = 2;
        return paramObject;
      }
      d = 1;
      paramObject = (c.d.c)this;
      m localm = paramm;
      if (localm != null) {
        return ((m)aa.a(localm, 2)).invoke(paramR, paramObject);
      }
      throw new u("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
    }
  }
  
  public static final class b
    extends d
  {
    private int e;
    
    public b(c.d.c paramc1, f paramf1, c.d.c paramc2, f paramf2, m paramm, Object paramObject)
    {
      super(paramf2);
    }
    
    public final Object a(Object paramObject)
    {
      switch (e)
      {
      default: 
        throw ((Throwable)new IllegalStateException("This coroutine had already completed".toString()));
      case 1: 
        e = 2;
        return paramObject;
      }
      e = 1;
      paramObject = (c.d.c)this;
      m localm = paramm;
      if (localm != null) {
        return ((m)aa.a(localm, 2)).invoke(paramR, paramObject);
      }
      throw new u("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
    }
  }
}

/* Location:
 * Qualified Name:     c.d.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */