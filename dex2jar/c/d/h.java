package c.d;

import c.d.a.a;
import c.d.b.a.e;
import c.o.b;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

public final class h<T>
  implements e, c<T>
{
  @Deprecated
  public static final a a = new a((byte)0);
  private static final AtomicReferenceFieldUpdater<h<?>, Object> d = AtomicReferenceFieldUpdater.newUpdater(h.class, Object.class, "b");
  private volatile Object b;
  private final c<T> c;
  
  public h(c<? super T> paramc)
  {
    this(paramc, a.b);
  }
  
  private h(c<? super T> paramc, Object paramObject)
  {
    c = paramc;
    b = paramObject;
  }
  
  public final f ao_()
  {
    return c.ao_();
  }
  
  public final StackTraceElement ar_()
  {
    return null;
  }
  
  public final Object b()
  {
    Object localObject2 = b;
    Object localObject1 = localObject2;
    if (localObject2 == a.b)
    {
      if (d.compareAndSet(this, a.b, a.a)) {
        return a.a;
      }
      localObject1 = b;
    }
    if (localObject1 == a.c) {
      return a.a;
    }
    if (!(localObject1 instanceof o.b)) {
      return localObject1;
    }
    throw a;
  }
  
  public final void b(Object paramObject)
  {
    do
    {
      Object localObject;
      do
      {
        localObject = b;
        if (localObject != a.b) {
          break;
        }
      } while (!d.compareAndSet(this, a.b, paramObject));
      return;
      if (localObject != a.a) {
        break;
      }
    } while (!d.compareAndSet(this, a.a, a.c));
    c.b(paramObject);
    return;
    throw ((Throwable)new IllegalStateException("Already resumed"));
  }
  
  public final e c()
  {
    c localc2 = c;
    c localc1 = localc2;
    if (!(localc2 instanceof e)) {
      localc1 = null;
    }
    return (e)localc1;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("SafeContinuation for ");
    localStringBuilder.append(c);
    return localStringBuilder.toString();
  }
  
  static final class a {}
}

/* Location:
 * Qualified Name:     c.d.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */