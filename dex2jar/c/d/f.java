package c.d;

import c.g.a.m;
import c.g.b.k;
import c.g.b.l;

public abstract interface f
{
  public abstract <R> R fold(R paramR, m<? super R, ? super b, ? extends R> paramm);
  
  public abstract <E extends b> E get(c<E> paramc);
  
  public abstract f minusKey(c<?> paramc);
  
  public abstract f plus(f paramf);
  
  public static final class a
  {
    public static f a(f paramf1, f paramf2)
    {
      k.b(paramf2, "context");
      if (paramf2 == g.a) {
        return paramf1;
      }
      return (f)paramf2.fold(paramf1, (m)a.a);
    }
    
    static final class a
      extends l
      implements m<f, f.b, f>
    {
      public static final a a = new a();
      
      a()
      {
        super();
      }
    }
  }
  
  public static abstract interface b
    extends f
  {
    public abstract <E extends b> E get(f.c<E> paramc);
    
    public abstract f.c<?> getKey();
    
    public static final class a
    {
      public static <E extends f.b> E a(f.b paramb, f.c<E> paramc)
      {
        k.b(paramc, "key");
        if (k.a(paramb.getKey(), paramc)) {
          return paramb;
        }
        return null;
      }
      
      public static f a(f.b paramb, f paramf)
      {
        k.b(paramf, "context");
        return f.a.a((f)paramb, paramf);
      }
      
      public static <R> R a(f.b paramb, R paramR, m<? super R, ? super f.b, ? extends R> paramm)
      {
        k.b(paramm, "operation");
        return (R)paramm.invoke(paramR, paramb);
      }
      
      public static f b(f.b paramb, f.c<?> paramc)
      {
        k.b(paramc, "key");
        if (k.a(paramb.getKey(), paramc)) {
          return (f)g.a;
        }
        return (f)paramb;
      }
    }
  }
  
  public static abstract interface c<E extends f.b> {}
}

/* Location:
 * Qualified Name:     c.d.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */