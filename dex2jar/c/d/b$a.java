package c.d;

import java.io.Serializable;

final class b$a
  implements Serializable
{
  public static final a a = new a((byte)0);
  private static final long serialVersionUID = 0L;
  private final f[] b;
  
  public b$a(f[] paramArrayOff)
  {
    b = paramArrayOff;
  }
  
  private final Object readResolve()
  {
    f[] arrayOff = b;
    Object localObject = g.a;
    int j = arrayOff.length;
    int i = 0;
    while (i < j)
    {
      f localf = arrayOff[i];
      localObject = ((f)localObject).plus(localf);
      i += 1;
    }
    return localObject;
  }
  
  public static final class a {}
}

/* Location:
 * Qualified Name:     c.d.b.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */