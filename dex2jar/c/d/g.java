package c.d;

import c.g.a.m;
import c.g.b.k;
import java.io.Serializable;

public final class g
  implements f, Serializable
{
  public static final g a = new g();
  private static final long serialVersionUID = 0L;
  
  private final Object readResolve()
  {
    return a;
  }
  
  public final <R> R fold(R paramR, m<? super R, ? super f.b, ? extends R> paramm)
  {
    k.b(paramm, "operation");
    return paramR;
  }
  
  public final <E extends f.b> E get(f.c<E> paramc)
  {
    k.b(paramc, "key");
    return null;
  }
  
  public final int hashCode()
  {
    return 0;
  }
  
  public final f minusKey(f.c<?> paramc)
  {
    k.b(paramc, "key");
    return (f)this;
  }
  
  public final f plus(f paramf)
  {
    k.b(paramf, "context");
    return paramf;
  }
  
  public final String toString()
  {
    return "EmptyCoroutineContext";
  }
}

/* Location:
 * Qualified Name:     c.d.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */