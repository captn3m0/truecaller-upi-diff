package c.d;

import c.g.a.m;
import c.g.b.k;
import c.g.b.l;
import c.g.b.v.b;
import c.u;
import c.x;
import java.io.Serializable;

public final class b
  implements f, Serializable
{
  private final f a;
  private final f.b b;
  
  public b(f paramf, f.b paramb)
  {
    a = paramf;
    b = paramb;
  }
  
  private final int a()
  {
    Object localObject = (b)this;
    int i = 2;
    for (;;)
    {
      f localf = a;
      localObject = localf;
      if (!(localf instanceof b)) {
        localObject = null;
      }
      localObject = (b)localObject;
      if (localObject == null) {
        return i;
      }
      i += 1;
    }
  }
  
  private final boolean a(f.b paramb)
  {
    return k.a(get(paramb.getKey()), paramb);
  }
  
  private final Object writeReplace()
  {
    int j = a();
    f[] arrayOff = new f[j];
    final v.b localb = new v.b();
    int i = 0;
    a = 0;
    fold(x.a, (m)new c(arrayOff, localb));
    if (a == j) {
      i = 1;
    }
    if (i != 0) {
      return new a(arrayOff);
    }
    throw ((Throwable)new IllegalStateException("Check failed.".toString()));
  }
  
  public final boolean equals(Object paramObject)
  {
    if ((b)this != paramObject)
    {
      if ((paramObject instanceof b))
      {
        b localb = (b)paramObject;
        if (localb.a() == a())
        {
          boolean bool;
          for (paramObject = this;; paramObject = (b)paramObject)
          {
            if (!localb.a(b))
            {
              bool = false;
              break label82;
            }
            paramObject = a;
            if (!(paramObject instanceof b)) {
              break;
            }
          }
          if (paramObject != null)
          {
            bool = localb.a((f.b)paramObject);
            label82:
            if (bool) {
              break label101;
            }
          }
          else
          {
            throw new u("null cannot be cast to non-null type kotlin.coroutines.CoroutineContext.Element");
          }
        }
      }
      return false;
    }
    label101:
    return true;
  }
  
  public final <R> R fold(R paramR, m<? super R, ? super f.b, ? extends R> paramm)
  {
    k.b(paramm, "operation");
    return (R)paramm.invoke(a.fold(paramR, paramm), b);
  }
  
  public final <E extends f.b> E get(f.c<E> paramc)
  {
    k.b(paramc, "key");
    for (Object localObject = (b)this;; localObject = (b)localObject)
    {
      f.b localb = b.get(paramc);
      if (localb != null) {
        return localb;
      }
      localObject = a;
      if (!(localObject instanceof b)) {
        break;
      }
    }
    return ((f)localObject).get(paramc);
  }
  
  public final int hashCode()
  {
    return a.hashCode() + b.hashCode();
  }
  
  public final f minusKey(f.c<?> paramc)
  {
    k.b(paramc, "key");
    if (b.get(paramc) != null) {
      return a;
    }
    paramc = a.minusKey(paramc);
    if (paramc == a) {
      return (f)this;
    }
    if (paramc == g.a) {
      return (f)b;
    }
    return (f)new b(paramc, b);
  }
  
  public final f plus(f paramf)
  {
    k.b(paramf, "context");
    return f.a.a(this, paramf);
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("[");
    localStringBuilder.append((String)fold("", (m)b.a));
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
  
  static final class a
    implements Serializable
  {
    public static final a a = new a((byte)0);
    private static final long serialVersionUID = 0L;
    private final f[] b;
    
    public a(f[] paramArrayOff)
    {
      b = paramArrayOff;
    }
    
    private final Object readResolve()
    {
      f[] arrayOff = b;
      Object localObject = g.a;
      int j = arrayOff.length;
      int i = 0;
      while (i < j)
      {
        f localf = arrayOff[i];
        localObject = ((f)localObject).plus(localf);
        i += 1;
      }
      return localObject;
    }
    
    public static final class a {}
  }
  
  static final class b
    extends l
    implements m<String, f.b, String>
  {
    public static final b a = new b();
    
    b()
    {
      super();
    }
  }
  
  static final class c
    extends l
    implements m<x, f.b, x>
  {
    c(f[] paramArrayOff, v.b paramb)
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     c.d.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */