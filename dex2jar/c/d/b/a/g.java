package c.d.b.a;

import c.g.b.k;
import java.lang.reflect.Field;

public final class g
{
  public static final StackTraceElement a(a parama)
  {
    k.b(parama, "receiver$0");
    f localf = b(parama);
    if (localf == null) {
      return null;
    }
    a(localf.a());
    int i = c(parama);
    if (i < 0) {
      i = -1;
    } else {
      i = localf.c()[i];
    }
    Object localObject = h.b;
    parama = h.a(parama);
    if (parama == null)
    {
      parama = localf.e();
    }
    else
    {
      localObject = new StringBuilder();
      ((StringBuilder)localObject).append(parama);
      ((StringBuilder)localObject).append('/');
      ((StringBuilder)localObject).append(localf.e());
      parama = ((StringBuilder)localObject).toString();
    }
    return new StackTraceElement(parama, localf.d(), localf.b(), i);
  }
  
  private static final void a(int paramInt)
  {
    if (paramInt <= 1) {
      return;
    }
    StringBuilder localStringBuilder = new StringBuilder("Debug metadata version mismatch. Expected: 1, got ");
    localStringBuilder.append(paramInt);
    localStringBuilder.append(". Please update the Kotlin standard library.");
    throw ((Throwable)new IllegalStateException(localStringBuilder.toString().toString()));
  }
  
  private static final f b(a parama)
  {
    return (f)parama.getClass().getAnnotation(f.class);
  }
  
  private static final int c(a parama)
  {
    try
    {
      Object localObject = parama.getClass().getDeclaredField("label");
      k.a(localObject, "field");
      ((Field)localObject).setAccessible(true);
      localObject = ((Field)localObject).get(parama);
      parama = (a)localObject;
      if (!(localObject instanceof Integer)) {
        parama = null;
      }
      parama = (Integer)parama;
      int i;
      if (parama != null) {
        i = parama.intValue();
      } else {
        i = 0;
      }
      return i - 1;
    }
    catch (Exception parama)
    {
      for (;;) {}
    }
    return -1;
  }
}

/* Location:
 * Qualified Name:     c.d.b.a.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */