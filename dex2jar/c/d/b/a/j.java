package c.d.b.a;

import c.d.c;
import c.g.b.k;
import c.g.b.w;

public abstract class j
  extends i
  implements c.g.b.i<Object>
{
  private final int a = 2;
  
  public j(c<Object> paramc)
  {
    super(paramc);
  }
  
  public final int e()
  {
    return a;
  }
  
  public String toString()
  {
    if (t == null)
    {
      String str = w.a((c.g.b.i)this);
      k.a(str, "Reflection.renderLambdaToString(this)");
      return str;
    }
    return super.toString();
  }
}

/* Location:
 * Qualified Name:     c.d.b.a.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */