package c.d.b.a;

import c.d.c;
import c.g.b.i;
import c.g.b.w;

public abstract class k
  extends d
  implements i<Object>
{
  private final int a;
  
  public k(int paramInt, c<Object> paramc)
  {
    super(paramc);
    a = paramInt;
  }
  
  public final int e()
  {
    return a;
  }
  
  public String toString()
  {
    if (t == null)
    {
      String str = w.a((i)this);
      c.g.b.k.a(str, "Reflection.renderLambdaToString(this)");
      return str;
    }
    return super.toString();
  }
}

/* Location:
 * Qualified Name:     c.d.b.a.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */