package c.d.b.a;

import c.d.f;
import c.d.f.b;
import c.d.f.c;
import c.g.b.k;

public abstract class d
  extends a
{
  private transient c.d.c<Object> a;
  private final f b;
  
  public d(c.d.c<Object> paramc)
  {
    this(paramc, localf);
  }
  
  public d(c.d.c<Object> paramc, f paramf)
  {
    super(paramc);
    b = paramf;
  }
  
  public final f ao_()
  {
    f localf = b;
    if (localf == null) {
      k.a();
    }
    return localf;
  }
  
  protected final void b()
  {
    c.d.c localc = a;
    if ((localc != null) && (localc != (d)this))
    {
      f.b localb = ao_().get((f.c)c.d.d.a);
      if (localb == null) {
        k.a();
      }
      ((c.d.d)localb).b(localc);
    }
    a = ((c.d.c)c.a);
  }
  
  public final c.d.c<Object> r_()
  {
    c.d.c localc = a;
    Object localObject = localc;
    if (localc == null)
    {
      localObject = (c.d.d)ao_().get((f.c)c.d.d.a);
      if (localObject != null) {
        localObject = ((c.d.d)localObject).a((c.d.c)this);
      } else {
        localObject = (c.d.c)this;
      }
      a = ((c.d.c)localObject);
    }
    return (c.d.c<Object>)localObject;
  }
}

/* Location:
 * Qualified Name:     c.d.b.a.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */