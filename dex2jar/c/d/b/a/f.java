package c.d.b.a;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({java.lang.annotation.ElementType.TYPE})
public @interface f
{
  int a() default 1;
  
  String b() default "";
  
  int[] c() default {};
  
  String d() default "";
  
  String e() default "";
}

/* Location:
 * Qualified Name:     c.d.b.a.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */