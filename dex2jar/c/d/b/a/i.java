package c.d.b.a;

import c.d.c;
import c.d.f;
import c.d.g;

public abstract class i
  extends a
{
  public i(c<Object> paramc)
  {
    super(paramc);
    if (paramc != null)
    {
      int i;
      if (paramc.ao_() == g.a) {
        i = 1;
      } else {
        i = 0;
      }
      if (i != 0) {
        return;
      }
      throw ((Throwable)new IllegalArgumentException("Coroutines with restricted suspension must have EmptyCoroutineContext".toString()));
    }
  }
  
  public final f ao_()
  {
    return (f)g.a;
  }
}

/* Location:
 * Qualified Name:     c.d.b.a.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */