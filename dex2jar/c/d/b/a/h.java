package c.d.b.a;

import c.g.b.k;
import java.lang.reflect.Method;

final class h
{
  public static a a;
  public static final h b = new h();
  private static final a c = new a(null, null, null);
  
  public static String a(a parama)
  {
    k.b(parama, "continuation");
    Object localObject2 = a;
    Object localObject1 = localObject2;
    if (localObject2 == null) {
      localObject1 = b(parama);
    }
    if (localObject1 == c) {
      return null;
    }
    localObject2 = a;
    if (localObject2 != null)
    {
      parama = ((Method)localObject2).invoke(parama.getClass(), new Object[0]);
      if (parama == null) {
        return null;
      }
      localObject2 = b;
      if (localObject2 != null)
      {
        parama = ((Method)localObject2).invoke(parama, new Object[0]);
        if (parama == null) {
          return null;
        }
        localObject1 = c;
        if (localObject1 != null) {
          parama = ((Method)localObject1).invoke(parama, new Object[0]);
        } else {
          parama = null;
        }
        localObject1 = parama;
        if (!(parama instanceof String)) {
          localObject1 = null;
        }
        return (String)localObject1;
      }
      return null;
    }
    return null;
  }
  
  private static a b(a parama)
  {
    try
    {
      parama = new a(Class.class.getDeclaredMethod("getModule", new Class[0]), parama.getClass().getClassLoader().loadClass("java.lang.Module").getDeclaredMethod("getDescriptor", new Class[0]), parama.getClass().getClassLoader().loadClass("java.lang.module.ModuleDescriptor").getDeclaredMethod("name", new Class[0]));
      a = parama;
      return parama;
    }
    catch (Exception parama)
    {
      for (;;) {}
    }
    parama = c;
    a = parama;
    return parama;
  }
  
  static final class a
  {
    public final Method a;
    public final Method b;
    public final Method c;
    
    public a(Method paramMethod1, Method paramMethod2, Method paramMethod3)
    {
      a = paramMethod1;
      b = paramMethod2;
      c = paramMethod3;
    }
  }
}

/* Location:
 * Qualified Name:     c.d.b.a.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */