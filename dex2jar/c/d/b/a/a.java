package c.d.b.a;

import c.d.c;
import c.g.b.k;
import c.o;
import c.o.a;
import c.p;
import c.x;
import java.io.Serializable;

public abstract class a
  implements e, c<Object>, Serializable
{
  final c<Object> t;
  
  public a(c<Object> paramc)
  {
    t = paramc;
  }
  
  public c<x> a(Object paramObject, c<?> paramc)
  {
    k.b(paramc, "completion");
    throw ((Throwable)new UnsupportedOperationException("create(Any?;Continuation) has not been overridden"));
  }
  
  protected abstract Object a(Object paramObject);
  
  public final StackTraceElement ar_()
  {
    return g.a(this);
  }
  
  protected void b() {}
  
  public final void b(Object paramObject)
  {
    k.b((c)this, "frame");
    c localc;
    for (a locala = (a)this;; locala = (a)localc)
    {
      localc = t;
      if (localc == null) {
        k.a();
      }
      try
      {
        paramObject = locala.a(paramObject);
        if (paramObject == c.d.a.a.a) {
          return;
        }
        locala1 = o.a;
        paramObject = o.d(paramObject);
      }
      catch (Throwable paramObject)
      {
        o.a locala1 = o.a;
        paramObject = o.d(p.a((Throwable)paramObject));
      }
      locala.b();
      if (!(localc instanceof a)) {
        break;
      }
    }
    localc.b(paramObject);
  }
  
  public final e c()
  {
    c localc2 = t;
    c localc1 = localc2;
    if (!(localc2 instanceof e)) {
      localc1 = null;
    }
    return (e)localc1;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("Continuation at ");
    Object localObject = g.a(this);
    if (localObject == null) {
      localObject = getClass().getName();
    }
    localStringBuilder.append((Serializable)localObject);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     c.d.b.a.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */