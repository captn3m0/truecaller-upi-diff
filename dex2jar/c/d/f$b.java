package c.d;

import c.g.a.m;
import c.g.b.k;

public abstract interface f$b
  extends f
{
  public abstract <E extends b> E get(f.c<E> paramc);
  
  public abstract f.c<?> getKey();
  
  public static final class a
  {
    public static <E extends f.b> E a(f.b paramb, f.c<E> paramc)
    {
      k.b(paramc, "key");
      if (k.a(paramb.getKey(), paramc)) {
        return paramb;
      }
      return null;
    }
    
    public static f a(f.b paramb, f paramf)
    {
      k.b(paramf, "context");
      return f.a.a((f)paramb, paramf);
    }
    
    public static <R> R a(f.b paramb, R paramR, m<? super R, ? super f.b, ? extends R> paramm)
    {
      k.b(paramm, "operation");
      return (R)paramm.invoke(paramR, paramb);
    }
    
    public static f b(f.b paramb, f.c<?> paramc)
    {
      k.b(paramc, "key");
      if (k.a(paramb.getKey(), paramc)) {
        return (f)g.a;
      }
      return (f)paramb;
    }
  }
}

/* Location:
 * Qualified Name:     c.d.f.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */