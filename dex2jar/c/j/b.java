package c.j;

import c.g.b.k;
import java.util.Random;

public final class b
  extends a
{
  private final a d = new a();
  
  public final Random a()
  {
    Object localObject = d.get();
    k.a(localObject, "implStorage.get()");
    return (Random)localObject;
  }
  
  public static final class a
    extends ThreadLocal<Random>
  {}
}

/* Location:
 * Qualified Name:     c.j.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */