package c.j;

import c.e.a;
import c.e.b;
import c.g.b.k;

public abstract class c
{
  static final c a = a.a();
  public static final a b = a.d;
  public static final b c = new b((byte)0);
  
  static
  {
    a locala = b.a;
  }
  
  public abstract int a(int paramInt);
  
  public int a(int paramInt1, int paramInt2)
  {
    int i;
    if (paramInt2 > paramInt1) {
      i = 1;
    } else {
      i = 0;
    }
    if (i != 0)
    {
      i = paramInt2 - paramInt1;
      if ((i <= 0) && (i != Integer.MIN_VALUE))
      {
        do
        {
          i = b();
        } while ((paramInt1 > i) || (paramInt2 <= i));
        return i;
      }
      if ((-i & i) == i)
      {
        paramInt2 = a(31 - Integer.numberOfLeadingZeros(i));
      }
      else
      {
        int j;
        do
        {
          j = b() >>> 1;
          paramInt2 = j % i;
        } while (j - paramInt2 + (i - 1) < 0);
      }
      return paramInt1 + paramInt2;
    }
    Integer localInteger1 = Integer.valueOf(paramInt1);
    Integer localInteger2 = Integer.valueOf(paramInt2);
    k.b(localInteger1, "from");
    k.b(localInteger2, "until");
    StringBuilder localStringBuilder = new StringBuilder("Random range is empty: [");
    localStringBuilder.append(localInteger1);
    localStringBuilder.append(", ");
    localStringBuilder.append(localInteger2);
    localStringBuilder.append(").");
    throw ((Throwable)new IllegalArgumentException(localStringBuilder.toString().toString()));
  }
  
  public int b()
  {
    return a(32);
  }
  
  public int b(int paramInt)
  {
    return a(0, paramInt);
  }
  
  public long c()
  {
    return (b() << 32) + b();
  }
  
  public static final class a
    extends c
  {
    public static final a d = new a();
    
    public final int a(int paramInt)
    {
      return c.a.a(paramInt);
    }
  }
  
  public static final class b
    extends c
  {
    public final int a(int paramInt)
    {
      return c.a.a(paramInt);
    }
    
    public final int a(int paramInt1, int paramInt2)
    {
      return c.a.a(paramInt1, paramInt2);
    }
    
    public final int b()
    {
      return c.a.b();
    }
    
    public final int b(int paramInt)
    {
      return c.a.b(paramInt);
    }
    
    public final long c()
    {
      return c.a.c();
    }
  }
}

/* Location:
 * Qualified Name:     c.j.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */