package c;

import c.g.a.a;
import java.io.Serializable;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

final class q<T>
  implements f<T>, Serializable
{
  public static final a a = new a((byte)0);
  private static final AtomicReferenceFieldUpdater<q<?>, Object> e = AtomicReferenceFieldUpdater.newUpdater(q.class, Object.class, "c");
  private volatile a<? extends T> b;
  private volatile Object c;
  private final Object d;
  
  public q(a<? extends T> parama)
  {
    b = parama;
    c = v.a;
    d = v.a;
  }
  
  private final Object writeReplace()
  {
    return new d(b());
  }
  
  public final boolean a()
  {
    return c != v.a;
  }
  
  public final T b()
  {
    Object localObject = c;
    if (localObject != v.a) {
      return (T)localObject;
    }
    localObject = b;
    if (localObject != null)
    {
      localObject = ((a)localObject).invoke();
      if (e.compareAndSet(this, v.a, localObject))
      {
        b = null;
        return (T)localObject;
      }
    }
    return (T)c;
  }
  
  public final String toString()
  {
    if (a()) {
      return String.valueOf(b());
    }
    return "Lazy value not initialized yet.";
  }
  
  public static final class a {}
}

/* Location:
 * Qualified Name:     c.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */