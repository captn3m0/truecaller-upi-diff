package c.i;

import c.g.b.k;
import c.l.g;

public abstract class c<T>
  implements d<Object, T>
{
  private T a;
  
  public c(T paramT)
  {
    a = paramT;
  }
  
  public final T a(g<?> paramg)
  {
    k.b(paramg, "property");
    return (T)a;
  }
  
  public final void a(g<?> paramg, T paramT)
  {
    k.b(paramg, "property");
    Object localObject = a;
    k.b(paramg, "property");
    a = paramT;
    a(paramg, localObject, paramT);
  }
  
  protected void a(g<?> paramg, T paramT1, T paramT2)
  {
    k.b(paramg, "property");
  }
}

/* Location:
 * Qualified Name:     c.i.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */