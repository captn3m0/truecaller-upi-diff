package c.i;

import c.g.b.k;
import c.l.g;

final class b<T>
  implements d<Object, T>
{
  private T a;
  
  public final T a(g<?> paramg)
  {
    k.b(paramg, "property");
    Object localObject = a;
    if (localObject != null) {
      return (T)localObject;
    }
    localObject = new StringBuilder("Property ");
    ((StringBuilder)localObject).append(paramg.b());
    ((StringBuilder)localObject).append(" should be initialized before get.");
    throw ((Throwable)new IllegalStateException(((StringBuilder)localObject).toString()));
  }
  
  public final void a(g<?> paramg, T paramT)
  {
    k.b(paramg, "property");
    k.b(paramT, "value");
    a = paramT;
  }
}

/* Location:
 * Qualified Name:     c.i.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */