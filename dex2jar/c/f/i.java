package c.f;

import c.g.b.k;
import java.io.File;

public class i
  extends h
{
  /* Error */
  public static final File a(File paramFile1, File paramFile2)
  {
    // Byte code:
    //   0: aload_0
    //   1: ldc 10
    //   3: invokestatic 16	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_1
    //   7: ldc 18
    //   9: invokestatic 16	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   12: aload_0
    //   13: invokevirtual 24	java/io/File:exists	()Z
    //   16: ifeq +183 -> 199
    //   19: aload_1
    //   20: invokevirtual 24	java/io/File:exists	()Z
    //   23: ifne +161 -> 184
    //   26: aload_0
    //   27: invokevirtual 27	java/io/File:isDirectory	()Z
    //   30: ifeq +27 -> 57
    //   33: aload_1
    //   34: invokevirtual 30	java/io/File:mkdirs	()Z
    //   37: ifeq +5 -> 42
    //   40: aload_1
    //   41: areturn
    //   42: new 32	c/f/d
    //   45: dup
    //   46: aload_0
    //   47: aload_1
    //   48: ldc 34
    //   50: invokespecial 38	c/f/d:<init>	(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)V
    //   53: checkcast 8	java/lang/Throwable
    //   56: athrow
    //   57: aload_1
    //   58: invokevirtual 42	java/io/File:getParentFile	()Ljava/io/File;
    //   61: astore_2
    //   62: aload_2
    //   63: ifnull +8 -> 71
    //   66: aload_2
    //   67: invokevirtual 30	java/io/File:mkdirs	()Z
    //   70: pop
    //   71: new 44	java/io/FileInputStream
    //   74: dup
    //   75: aload_0
    //   76: invokespecial 47	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   79: checkcast 49	java/io/Closeable
    //   82: astore 4
    //   84: aconst_null
    //   85: astore_3
    //   86: aload_3
    //   87: astore_0
    //   88: aload 4
    //   90: checkcast 44	java/io/FileInputStream
    //   93: astore_2
    //   94: aload_3
    //   95: astore_0
    //   96: new 51	java/io/FileOutputStream
    //   99: dup
    //   100: aload_1
    //   101: invokespecial 52	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   104: checkcast 49	java/io/Closeable
    //   107: astore 5
    //   109: aload 5
    //   111: checkcast 51	java/io/FileOutputStream
    //   114: astore_0
    //   115: aload_2
    //   116: checkcast 54	java/io/InputStream
    //   119: aload_0
    //   120: checkcast 56	java/io/OutputStream
    //   123: invokestatic 61	c/f/a:a	(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    //   126: pop2
    //   127: aload_3
    //   128: astore_0
    //   129: aload 5
    //   131: aconst_null
    //   132: invokestatic 66	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   135: aload 4
    //   137: aconst_null
    //   138: invokestatic 66	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   141: aload_1
    //   142: areturn
    //   143: astore_2
    //   144: aconst_null
    //   145: astore_1
    //   146: goto +9 -> 155
    //   149: astore_0
    //   150: aload_0
    //   151: athrow
    //   152: astore_2
    //   153: aload_0
    //   154: astore_1
    //   155: aload_3
    //   156: astore_0
    //   157: aload 5
    //   159: aload_1
    //   160: invokestatic 66	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   163: aload_3
    //   164: astore_0
    //   165: aload_2
    //   166: athrow
    //   167: astore_1
    //   168: goto +8 -> 176
    //   171: astore_1
    //   172: aload_1
    //   173: astore_0
    //   174: aload_1
    //   175: athrow
    //   176: aload 4
    //   178: aload_0
    //   179: invokestatic 66	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   182: aload_1
    //   183: athrow
    //   184: new 68	c/f/c
    //   187: dup
    //   188: aload_0
    //   189: aload_1
    //   190: ldc 70
    //   192: invokespecial 71	c/f/c:<init>	(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)V
    //   195: checkcast 8	java/lang/Throwable
    //   198: athrow
    //   199: new 73	c/f/k
    //   202: dup
    //   203: aload_0
    //   204: ldc 75
    //   206: iconst_0
    //   207: invokespecial 78	c/f/k:<init>	(Ljava/io/File;Ljava/lang/String;B)V
    //   210: checkcast 8	java/lang/Throwable
    //   213: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	214	0	paramFile1	File
    //   0	214	1	paramFile2	File
    //   61	55	2	localObject1	Object
    //   143	1	2	localObject2	Object
    //   152	14	2	localObject3	Object
    //   85	79	3	localObject4	Object
    //   82	95	4	localCloseable1	java.io.Closeable
    //   107	51	5	localCloseable2	java.io.Closeable
    // Exception table:
    //   from	to	target	type
    //   109	127	143	finally
    //   109	127	149	java/lang/Throwable
    //   150	152	152	finally
    //   88	94	167	finally
    //   96	109	167	finally
    //   129	135	167	finally
    //   157	163	167	finally
    //   165	167	167	finally
    //   174	176	167	finally
    //   88	94	171	java/lang/Throwable
    //   96	109	171	java/lang/Throwable
    //   129	135	171	java/lang/Throwable
    //   157	163	171	java/lang/Throwable
    //   165	167	171	java/lang/Throwable
  }
  
  public static final File a(String paramString1, String paramString2, File paramFile)
  {
    k.b(paramString1, "prefix");
    paramString1 = File.createTempFile(paramString1, paramString2, paramFile);
    k.a(paramString1, "File.createTempFile(prefix, suffix, directory)");
    return paramString1;
  }
}

/* Location:
 * Qualified Name:     c.f.i
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */