package c.f;

import c.g.b.a.a;
import java.io.BufferedReader;
import java.util.Iterator;

public final class j$a
  implements a, Iterator<String>
{
  private String b;
  private boolean c;
  
  public final boolean hasNext()
  {
    if ((b == null) && (!c))
    {
      b = a.a.readLine();
      if (b == null) {
        c = true;
      }
    }
    return b != null;
  }
  
  public final void remove()
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
}

/* Location:
 * Qualified Name:     c.f.j.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */