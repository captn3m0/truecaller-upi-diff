package c.f;

import c.a;
import java.io.Closeable;

public final class b
{
  public static final void a(Closeable paramCloseable, Throwable paramThrowable)
  {
    if (paramCloseable != null)
    {
      if (paramThrowable == null)
      {
        paramCloseable.close();
        return;
      }
      try
      {
        paramCloseable.close();
        return;
      }
      catch (Throwable paramCloseable)
      {
        a.a(paramThrowable, paramCloseable);
      }
    }
  }
}

/* Location:
 * Qualified Name:     c.f.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */