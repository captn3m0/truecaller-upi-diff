package c.f;

import c.g.b.k;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public final class a
{
  public static final long a(InputStream paramInputStream, OutputStream paramOutputStream)
  {
    k.b(paramInputStream, "receiver$0");
    k.b(paramOutputStream, "out");
    byte[] arrayOfByte = new byte[' '];
    int i = paramInputStream.read(arrayOfByte);
    long l = 0L;
    while (i >= 0)
    {
      paramOutputStream.write(arrayOfByte, 0, i);
      l += i;
      i = paramInputStream.read(arrayOfByte);
    }
    return l;
  }
  
  public static final byte[] a(InputStream paramInputStream)
  {
    k.b(paramInputStream, "receiver$0");
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream(Math.max(8192, paramInputStream.available()));
    b(paramInputStream, (OutputStream)localByteArrayOutputStream);
    paramInputStream = localByteArrayOutputStream.toByteArray();
    k.a(paramInputStream, "buffer.toByteArray()");
    return paramInputStream;
  }
}

/* Location:
 * Qualified Name:     c.f.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */