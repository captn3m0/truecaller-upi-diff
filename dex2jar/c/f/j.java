package c.f;

import c.g.b.a.a;
import c.m.i;
import java.io.BufferedReader;
import java.util.Iterator;

final class j
  implements i<String>
{
  final BufferedReader a;
  
  public j(BufferedReader paramBufferedReader)
  {
    a = paramBufferedReader;
  }
  
  public final Iterator<String> a()
  {
    return (Iterator)new a(this);
  }
  
  public static final class a
    implements a, Iterator<String>
  {
    private String b;
    private boolean c;
    
    public final boolean hasNext()
    {
      if ((b == null) && (!c))
      {
        b = a.a.readLine();
        if (b == null) {
          c = true;
        }
      }
      return b != null;
    }
    
    public final void remove()
    {
      throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
  }
}

/* Location:
 * Qualified Name:     c.f.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */