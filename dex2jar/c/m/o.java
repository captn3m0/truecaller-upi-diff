package c.m;

import c.a.f;
import c.g.a.b;
import c.g.b.k;
import c.g.b.l;
import java.util.Iterator;

public class o
  extends n
{
  public static final <T> i<T> a(i<? extends Iterable<? extends T>> parami)
  {
    k.b(parami, "receiver$0");
    return a(parami, (b)b.a);
  }
  
  private static final <T, R> i<R> a(i<? extends T> parami, b<? super T, ? extends Iterator<? extends R>> paramb)
  {
    if ((parami instanceof t)) {
      return ((t)parami).a(paramb);
    }
    return (i)new g(parami, (b)c.a, paramb);
  }
  
  public static final <T> i<T> a(T paramT, b<? super T, ? extends T> paramb)
  {
    k.b(paramb, "nextFunction");
    return (i)new h((c.g.a.a)new e(paramT), paramb);
  }
  
  public static final <T> i<T> a(T... paramVarArgs)
  {
    k.b(paramVarArgs, "elements");
    return f.j(paramVarArgs);
  }
  
  public static final <T> i<T> b(i<? extends T> parami)
  {
    k.b(parami, "receiver$0");
    if ((parami instanceof a)) {
      return parami;
    }
    return (i)new a(parami);
  }
  
  public static final class a
    implements i<T>
  {
    public a(Iterator paramIterator) {}
    
    public final Iterator<T> a()
    {
      return a;
    }
  }
  
  static final class b
    extends l
    implements b<Iterable<? extends T>, Iterator<? extends T>>
  {
    public static final b a = new b();
    
    b()
    {
      super();
    }
  }
  
  static final class c
    extends l
    implements b<T, T>
  {
    public static final c a = new c();
    
    c()
    {
      super();
    }
    
    public final T invoke(T paramT)
    {
      return paramT;
    }
  }
  
  public static final class d
    extends l
    implements b<T, T>
  {
    public d(c.g.a.a parama)
    {
      super();
    }
    
    public final T invoke(T paramT)
    {
      k.b(paramT, "it");
      return (T)a.invoke();
    }
  }
  
  static final class e
    extends l
    implements c.g.a.a<T>
  {
    e(Object paramObject)
    {
      super();
    }
    
    public final T invoke()
    {
      return (T)a;
    }
  }
}

/* Location:
 * Qualified Name:     c.m.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */