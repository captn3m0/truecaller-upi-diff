package c.m;

import c.a.as;
import java.util.HashSet;
import java.util.Iterator;

final class b<T, K>
  extends c.a.b<T>
{
  private final HashSet<K> b;
  private final Iterator<T> c;
  private final c.g.a.b<T, K> d;
  
  public b(Iterator<? extends T> paramIterator, c.g.a.b<? super T, ? extends K> paramb)
  {
    c = paramIterator;
    d = paramb;
    b = new HashSet();
  }
  
  public final void a()
  {
    while (c.hasNext())
    {
      Object localObject1 = c.next();
      Object localObject2 = d.invoke(localObject1);
      if (b.add(localObject2))
      {
        a(localObject1);
        return;
      }
    }
    a = as.c;
  }
}

/* Location:
 * Qualified Name:     c.m.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */