package c.m;

import java.util.Iterator;

public final class c<T, K>
  implements i<T>
{
  private final i<T> a;
  private final c.g.a.b<T, K> b;
  
  public c(i<? extends T> parami, c.g.a.b<? super T, ? extends K> paramb)
  {
    a = parami;
    b = paramb;
  }
  
  public final Iterator<T> a()
  {
    return (Iterator)new b(a.a(), b);
  }
}

/* Location:
 * Qualified Name:     c.m.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */