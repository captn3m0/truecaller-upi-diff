package c.m;

import c.g.b.a.a;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class r$a
  implements a, Iterator<T>
{
  private int b;
  private final Iterator<T> c;
  
  r$a()
  {
    b = b;
    c = a.a();
  }
  
  public final boolean hasNext()
  {
    return (b > 0) && (c.hasNext());
  }
  
  public final T next()
  {
    int i = b;
    if (i != 0)
    {
      b = (i - 1);
      return (T)c.next();
    }
    throw ((Throwable)new NoSuchElementException());
  }
  
  public final void remove()
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
}

/* Location:
 * Qualified Name:     c.m.r.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */