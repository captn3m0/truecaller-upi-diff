package c.m;

import c.d.c;
import c.d.f;
import c.d.g;
import c.o;
import c.o.a;
import c.x;
import java.util.Iterator;
import java.util.NoSuchElementException;

final class j<T>
  extends k<T>
  implements c<x>, c.g.b.a.a, Iterator<T>
{
  c<? super x> a;
  private int b;
  private T c;
  private Iterator<? extends T> d;
  
  private final Throwable b()
  {
    switch (b)
    {
    default: 
      StringBuilder localStringBuilder = new StringBuilder("Unexpected state of the iterator: ");
      localStringBuilder.append(b);
      return (Throwable)new IllegalStateException(localStringBuilder.toString());
    case 5: 
      return (Throwable)new IllegalStateException("Iterator has failed.");
    }
    return (Throwable)new NoSuchElementException();
  }
  
  public final Object a(T paramT, c<? super x> paramc)
  {
    c = paramT;
    b = 3;
    a = paramc;
    paramT = c.d.a.a.a;
    if (paramT == c.d.a.a.a) {
      c.g.b.k.b(paramc, "frame");
    }
    return paramT;
  }
  
  public final f ao_()
  {
    return (f)g.a;
  }
  
  public final void b(Object paramObject)
  {
    b = 4;
  }
  
  public final boolean hasNext()
  {
    for (;;)
    {
      switch (b)
      {
      default: 
        throw b();
      case 4: 
        return false;
      case 2: 
      case 3: 
        return true;
      case 1: 
        localObject = d;
        if (localObject == null) {
          c.g.b.k.a();
        }
        if (((Iterator)localObject).hasNext())
        {
          b = 2;
          return true;
        }
        d = null;
      }
      b = 5;
      Object localObject = a;
      if (localObject == null) {
        c.g.b.k.a();
      }
      a = null;
      x localx = x.a;
      o.a locala = o.a;
      ((c)localObject).b(o.d(localx));
    }
  }
  
  public final T next()
  {
    do
    {
      Object localObject;
      switch (b)
      {
      default: 
        throw b();
      case 3: 
        b = 0;
        localObject = c;
        c = null;
        return (T)localObject;
      case 2: 
        b = 1;
        localObject = d;
        if (localObject == null) {
          c.g.b.k.a();
        }
        return (T)((Iterator)localObject).next();
      }
    } while (hasNext());
    throw ((Throwable)new NoSuchElementException());
  }
  
  public final void remove()
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
}

/* Location:
 * Qualified Name:     c.m.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */