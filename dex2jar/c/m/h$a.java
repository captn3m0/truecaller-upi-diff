package c.m;

import c.g.a.b;
import c.g.b.k;
import c.u;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class h$a
  implements c.g.b.a.a, Iterator<T>
{
  private T b;
  private int c = -2;
  
  private final void a()
  {
    Object localObject1;
    if (c == -2)
    {
      localObject1 = a.a.invoke();
    }
    else
    {
      localObject1 = a.b;
      Object localObject2 = b;
      if (localObject2 == null) {
        k.a();
      }
      localObject1 = ((b)localObject1).invoke(localObject2);
    }
    b = localObject1;
    int i;
    if (b == null) {
      i = 0;
    } else {
      i = 1;
    }
    c = i;
  }
  
  public final boolean hasNext()
  {
    if (c < 0) {
      a();
    }
    return c == 1;
  }
  
  public final T next()
  {
    if (c < 0) {
      a();
    }
    if (c != 0)
    {
      Object localObject = b;
      if (localObject != null)
      {
        c = -1;
        return (T)localObject;
      }
      throw new u("null cannot be cast to non-null type T");
    }
    throw ((Throwable)new NoSuchElementException());
  }
  
  public final void remove()
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
}

/* Location:
 * Qualified Name:     c.m.h.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */