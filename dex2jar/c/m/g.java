package c.m;

import c.g.a.b;
import c.g.b.a.a;
import c.g.b.k;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class g<T, R, E>
  implements i<E>
{
  final i<T> a;
  final b<T, R> b;
  final b<R, Iterator<E>> c;
  
  public g(i<? extends T> parami, b<? super T, ? extends R> paramb, b<? super R, ? extends Iterator<? extends E>> paramb1)
  {
    a = parami;
    b = paramb;
    c = paramb1;
  }
  
  public final Iterator<E> a()
  {
    return (Iterator)new a(this);
  }
  
  public static final class a
    implements a, Iterator<E>
  {
    private final Iterator<T> b;
    private Iterator<? extends E> c;
    
    a()
    {
      b = a.a();
    }
    
    private final boolean a()
    {
      Object localObject = c;
      if ((localObject != null) && (!((Iterator)localObject).hasNext())) {
        c = null;
      }
      while (c == null)
      {
        if (!b.hasNext()) {
          return false;
        }
        localObject = b.next();
        localObject = (Iterator)a.c.invoke(a.b.invoke(localObject));
        if (((Iterator)localObject).hasNext())
        {
          c = ((Iterator)localObject);
          return true;
        }
      }
      return true;
    }
    
    public final boolean hasNext()
    {
      return a();
    }
    
    public final E next()
    {
      if (a())
      {
        Iterator localIterator = c;
        if (localIterator == null) {
          k.a();
        }
        return (E)localIterator.next();
      }
      throw ((Throwable)new NoSuchElementException());
    }
    
    public final void remove()
    {
      throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
  }
}

/* Location:
 * Qualified Name:     c.m.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */