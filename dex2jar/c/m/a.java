package c.m;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReference;

public final class a<T>
  implements i<T>
{
  private final AtomicReference<i<T>> a;
  
  public a(i<? extends T> parami)
  {
    a = new AtomicReference(parami);
  }
  
  public final Iterator<T> a()
  {
    i locali = (i)a.getAndSet(null);
    if (locali != null) {
      return locali.a();
    }
    throw ((Throwable)new IllegalStateException("This sequence can be consumed only once."));
  }
}

/* Location:
 * Qualified Name:     c.m.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */