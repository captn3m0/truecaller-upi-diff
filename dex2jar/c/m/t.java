package c.m;

import c.g.a.b;
import c.g.b.a.a;
import c.g.b.k;
import java.util.Iterator;

public final class t<T, R>
  implements i<R>
{
  final i<T> a;
  final b<T, R> b;
  
  public t(i<? extends T> parami, b<? super T, ? extends R> paramb)
  {
    a = parami;
    b = paramb;
  }
  
  public final <E> i<E> a(b<? super R, ? extends Iterator<? extends E>> paramb)
  {
    k.b(paramb, "iterator");
    return (i)new g(a, b, paramb);
  }
  
  public final Iterator<R> a()
  {
    return (Iterator)new a(this);
  }
  
  public static final class a
    implements a, Iterator<R>
  {
    private final Iterator<T> b;
    
    a()
    {
      b = a.a();
    }
    
    public final boolean hasNext()
    {
      return b.hasNext();
    }
    
    public final R next()
    {
      return (R)a.b.invoke(b.next());
    }
    
    public final void remove()
    {
      throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
  }
}

/* Location:
 * Qualified Name:     c.m.t
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */