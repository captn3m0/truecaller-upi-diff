package c.m;

import c.g.a.b;
import c.g.b.a.a;
import java.util.Iterator;

public final class t$a
  implements a, Iterator<R>
{
  private final Iterator<T> b;
  
  t$a()
  {
    b = a.a();
  }
  
  public final boolean hasNext()
  {
    return b.hasNext();
  }
  
  public final R next()
  {
    return (R)a.b.invoke(b.next());
  }
  
  public final void remove()
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
}

/* Location:
 * Qualified Name:     c.m.t.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */