package c.m;

import c.g.a.b;
import c.g.b.a.a;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class s$a
  implements a, Iterator<T>
{
  private final Iterator<T> b;
  private int c;
  private T d;
  
  s$a()
  {
    b = a.a();
    c = -1;
  }
  
  private final void a()
  {
    if (b.hasNext())
    {
      Object localObject = b.next();
      if (((Boolean)a.b.invoke(localObject)).booleanValue())
      {
        c = 1;
        d = localObject;
        return;
      }
    }
    c = 0;
  }
  
  public final boolean hasNext()
  {
    if (c == -1) {
      a();
    }
    return c == 1;
  }
  
  public final T next()
  {
    if (c == -1) {
      a();
    }
    if (c != 0)
    {
      Object localObject = d;
      d = null;
      c = -1;
      return (T)localObject;
    }
    throw ((Throwable)new NoSuchElementException());
  }
  
  public final void remove()
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
}

/* Location:
 * Qualified Name:     c.m.s.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */