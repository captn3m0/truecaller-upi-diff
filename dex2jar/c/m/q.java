package c.m;

import c.a.ar;
import c.g.a.b;
import c.g.b.a.a;
import c.g.b.k;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class q
  extends p
{
  public static final <T> i<List<T>> a(i<? extends T> parami, int paramInt)
  {
    k.b(parami, "receiver$0");
    return l.a(parami, paramInt, paramInt);
  }
  
  public static final <T> i<List<T>> a(i<? extends T> parami, int paramInt1, int paramInt2)
  {
    k.b(parami, "receiver$0");
    return ar.a(parami, paramInt1, paramInt2);
  }
  
  public static final <T> i<T> a(i<? extends T> parami, b<? super T, Boolean> paramb)
  {
    k.b(parami, "receiver$0");
    k.b(paramb, "predicate");
    return (i)new f(parami, true, paramb);
  }
  
  public static final <T, A extends Appendable> A a(i<? extends T> parami, A paramA, CharSequence paramCharSequence1, CharSequence paramCharSequence2, CharSequence paramCharSequence3, CharSequence paramCharSequence4)
  {
    k.b(parami, "receiver$0");
    k.b(paramA, "buffer");
    k.b(paramCharSequence1, "separator");
    k.b(paramCharSequence2, "prefix");
    k.b(paramCharSequence3, "postfix");
    k.b(paramCharSequence4, "truncated");
    paramA.append(paramCharSequence2);
    parami = parami.a();
    int i = 0;
    while (parami.hasNext())
    {
      paramCharSequence2 = parami.next();
      i += 1;
      if (i > 1) {
        paramA.append(paramCharSequence1);
      }
      c.n.m.a(paramA, paramCharSequence2, null);
    }
    paramA.append(paramCharSequence3);
    return paramA;
  }
  
  public static final <T> String a(i<? extends T> parami, CharSequence paramCharSequence1, CharSequence paramCharSequence2, CharSequence paramCharSequence3, CharSequence paramCharSequence4)
  {
    k.b(parami, "receiver$0");
    k.b(paramCharSequence1, "separator");
    k.b(paramCharSequence2, "prefix");
    k.b(paramCharSequence3, "postfix");
    k.b(paramCharSequence4, "truncated");
    parami = ((StringBuilder)l.a(parami, (Appendable)new StringBuilder(), paramCharSequence1, paramCharSequence2, paramCharSequence3, paramCharSequence4)).toString();
    k.a(parami, "joinTo(StringBuilder(), …ed, transform).toString()");
    return parami;
  }
  
  public static final <T, C extends Collection<? super T>> C a(i<? extends T> parami, C paramC)
  {
    k.b(parami, "receiver$0");
    k.b(paramC, "destination");
    parami = parami.a();
    while (parami.hasNext()) {
      paramC.add(parami.next());
    }
    return paramC;
  }
  
  public static final <T> i<T> b(i<? extends T> parami, b<? super T, Boolean> paramb)
  {
    k.b(parami, "receiver$0");
    k.b(paramb, "predicate");
    return (i)new f(parami, false, paramb);
  }
  
  public static final <T> i<T> c(i<? extends T> parami)
  {
    k.b(parami, "receiver$0");
    return l.b(parami, (b)b.a);
  }
  
  public static final <T, R> i<R> c(i<? extends T> parami, b<? super T, ? extends R> paramb)
  {
    k.b(parami, "receiver$0");
    k.b(paramb, "transform");
    return (i)new t(parami, paramb);
  }
  
  public static final <T, R> i<R> d(i<? extends T> parami, b<? super T, ? extends R> paramb)
  {
    k.b(parami, "receiver$0");
    k.b(paramb, "transform");
    return l.c((i)new t(parami, paramb));
  }
  
  public static final <T> List<T> d(i<? extends T> parami)
  {
    k.b(parami, "receiver$0");
    return c.a.m.b(l.e(parami));
  }
  
  public static final <T> List<T> e(i<? extends T> parami)
  {
    k.b(parami, "receiver$0");
    return (List)l.a(parami, (Collection)new ArrayList());
  }
  
  public static final <T> Iterable<T> f(i<? extends T> parami)
  {
    k.b(parami, "receiver$0");
    return (Iterable)new a(parami);
  }
  
  public static final class a
    implements a, Iterable<T>
  {
    public a(i parami) {}
    
    public final Iterator<T> iterator()
    {
      return a.a();
    }
  }
  
  static final class b
    extends c.g.b.l
    implements b<T, Boolean>
  {
    public static final b a = new b();
    
    b()
    {
      super();
    }
  }
}

/* Location:
 * Qualified Name:     c.m.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */