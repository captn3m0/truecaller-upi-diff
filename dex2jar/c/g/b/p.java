package c.g.b;

public final class p
  implements e
{
  private final Class<?> a;
  private final String b;
  
  public p(Class<?> paramClass, String paramString)
  {
    a = paramClass;
    b = paramString;
  }
  
  public final Class<?> a()
  {
    return a;
  }
  
  public final boolean equals(Object paramObject)
  {
    return ((paramObject instanceof p)) && (k.a(a, a));
  }
  
  public final int hashCode()
  {
    return a.hashCode();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(a.toString());
    localStringBuilder.append(" (Kotlin reflection is not available)");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     c.g.b.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */