package c.g.b;

import c.l.a;

public class j
  extends d
  implements i, c.l.d
{
  private final int a;
  
  public j()
  {
    a = 1;
  }
  
  public j(int paramInt, Object paramObject)
  {
    super(paramObject);
    a = paramInt;
  }
  
  protected final a d()
  {
    return w.a(this);
  }
  
  public final int e()
  {
    return a;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == this) {
      return true;
    }
    if ((paramObject instanceof j))
    {
      paramObject = (j)paramObject;
      return (a() == null ? ((j)paramObject).a() == null : a().equals(((j)paramObject).a())) && (b().equals(((j)paramObject).b())) && (c().equals(((j)paramObject).c())) && (k.a(f(), ((j)paramObject).f()));
    }
    if ((paramObject instanceof c.l.d)) {
      return paramObject.equals(g());
    }
    return false;
  }
  
  public int hashCode()
  {
    int i;
    if (a() == null) {
      i = 0;
    } else {
      i = a().hashCode() * 31;
    }
    return (i + b().hashCode()) * 31 + c().hashCode();
  }
  
  public String toString()
  {
    Object localObject = g();
    if (localObject != this) {
      return localObject.toString();
    }
    if ("<init>".equals(b())) {
      return "constructor (Kotlin reflection is not available)";
    }
    localObject = new StringBuilder("function ");
    ((StringBuilder)localObject).append(b());
    ((StringBuilder)localObject).append(" (Kotlin reflection is not available)");
    return ((StringBuilder)localObject).toString();
  }
}

/* Location:
 * Qualified Name:     c.g.b.j
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */