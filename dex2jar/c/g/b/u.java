package c.g.b;

import c.l.c;
import c.l.i.a;

public final class u
  extends t
{
  private final c a;
  private final String d;
  private final String e;
  
  public u(c paramc, String paramString1, String paramString2)
  {
    a = paramc;
    d = paramString1;
    e = paramString2;
  }
  
  public final c a()
  {
    return a;
  }
  
  public final Object a(Object paramObject)
  {
    return e().a(new Object[] { paramObject });
  }
  
  public final String b()
  {
    return d;
  }
  
  public final String c()
  {
    return e;
  }
}

/* Location:
 * Qualified Name:     c.g.b.u
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */