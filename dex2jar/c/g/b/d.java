package c.g.b;

import c.g.b;
import c.l.a;
import c.l.c;
import java.io.ObjectStreamException;
import java.io.Serializable;

public abstract class d
  implements a, Serializable
{
  public static final Object c = ;
  private transient a a;
  protected final Object b;
  
  public d()
  {
    this(c);
  }
  
  protected d(Object paramObject)
  {
    b = paramObject;
  }
  
  public c a()
  {
    throw new AbstractMethodError();
  }
  
  public final Object a(Object... paramVarArgs)
  {
    return h().a(paramVarArgs);
  }
  
  public String b()
  {
    throw new AbstractMethodError();
  }
  
  public String c()
  {
    throw new AbstractMethodError();
  }
  
  protected abstract a d();
  
  public final Object f()
  {
    return b;
  }
  
  public final a g()
  {
    a locala2 = a;
    a locala1 = locala2;
    if (locala2 == null)
    {
      locala1 = d();
      a = locala1;
    }
    return locala1;
  }
  
  protected a h()
  {
    a locala = g();
    if (locala != this) {
      return locala;
    }
    throw new b();
  }
  
  static final class a
    implements Serializable
  {
    private static final a a = new a();
    
    private Object readResolve()
      throws ObjectStreamException
    {
      return a;
    }
  }
}

/* Location:
 * Qualified Name:     c.g.b.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */