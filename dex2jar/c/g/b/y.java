package c.g.b;

import java.util.ArrayList;
import java.util.Collections;

public final class y
{
  public final ArrayList<Object> a;
  
  public y(int paramInt)
  {
    a = new ArrayList(paramInt);
  }
  
  public final void a(Object paramObject)
  {
    if (paramObject == null) {
      return;
    }
    if ((paramObject instanceof Object[]))
    {
      paramObject = (Object[])paramObject;
      if (paramObject.length > 0)
      {
        localObject = a;
        ((ArrayList)localObject).ensureCapacity(((ArrayList)localObject).size() + paramObject.length);
        Collections.addAll(a, (Object[])paramObject);
      }
      return;
    }
    Object localObject = new StringBuilder("Don't know how to spread ");
    ((StringBuilder)localObject).append(paramObject.getClass());
    throw new UnsupportedOperationException(((StringBuilder)localObject).toString());
  }
  
  public final Object[] a(Object[] paramArrayOfObject)
  {
    return a.toArray(paramArrayOfObject);
  }
  
  public final void b(Object paramObject)
  {
    a.add(paramObject);
  }
}

/* Location:
 * Qualified Name:     c.g.b.y
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */