package c.g.b;

import c.g.a;
import c.l.b;

public final class f
  implements e, b<Object>
{
  private final Class<?> a;
  
  public f(Class<?> paramClass)
  {
    a = paramClass;
  }
  
  public final Class<?> a()
  {
    return a;
  }
  
  public final boolean equals(Object paramObject)
  {
    return ((paramObject instanceof f)) && (k.a(a.b(this), a.b((b)paramObject)));
  }
  
  public final int hashCode()
  {
    return a.b(this).hashCode();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(a.toString());
    localStringBuilder.append(" (Kotlin reflection is not available)");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     c.g.b.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */