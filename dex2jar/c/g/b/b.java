package c.g.b;

import c.g.b.a.a;
import java.util.Iterator;
import java.util.NoSuchElementException;

final class b<T>
  implements a, Iterator<T>
{
  private int a;
  private final T[] b;
  
  public b(T[] paramArrayOfT)
  {
    b = paramArrayOfT;
  }
  
  public final boolean hasNext()
  {
    return a < b.length;
  }
  
  public final T next()
  {
    try
    {
      Object localObject = b;
      int i = a;
      a = (i + 1);
      localObject = localObject[i];
      return (T)localObject;
    }
    catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
    {
      a -= 1;
      throw ((Throwable)new NoSuchElementException(localArrayIndexOutOfBoundsException.getMessage()));
    }
  }
  
  public final void remove()
  {
    throw new UnsupportedOperationException("Operation is not supported for read-only collection");
  }
}

/* Location:
 * Qualified Name:     c.g.b.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */