package c.g.b;

import c.a.k;
import java.util.NoSuchElementException;

public final class a
  extends k
{
  private int a;
  private final byte[] b;
  
  public a(byte[] paramArrayOfByte)
  {
    b = paramArrayOfByte;
  }
  
  public final byte a()
  {
    try
    {
      byte[] arrayOfByte = b;
      int i = a;
      a = (i + 1);
      byte b1 = arrayOfByte[i];
      return b1;
    }
    catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
    {
      a -= 1;
      throw ((Throwable)new NoSuchElementException(localArrayIndexOutOfBoundsException.getMessage()));
    }
  }
  
  public final boolean hasNext()
  {
    return a < b.length;
  }
}

/* Location:
 * Qualified Name:     c.g.b.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */