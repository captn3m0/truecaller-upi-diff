package c.g.b;

import c.l.b;
import c.l.c;
import c.l.d;
import c.l.h;

public final class w
{
  private static final x a;
  private static final b[] b = new b[0];
  
  static
  {
    Object localObject = null;
    try
    {
      x localx = (x)Class.forName("kotlin.reflect.jvm.internal.ReflectionFactoryImpl").newInstance();
      localObject = localx;
    }
    catch (ClassCastException localClassCastException)
    {
      for (;;) {}
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      for (;;) {}
    }
    catch (InstantiationException localInstantiationException)
    {
      for (;;) {}
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      for (;;) {}
    }
    break label28;
    label28:
    if (localObject == null) {
      localObject = new x();
    }
    a = (x)localObject;
  }
  
  public static b a(Class paramClass)
  {
    return new f(paramClass);
  }
  
  public static c a(Class paramClass, String paramString)
  {
    return new p(paramClass, paramString);
  }
  
  public static d a(j paramj)
  {
    return paramj;
  }
  
  public static c.l.f a(n paramn)
  {
    return paramn;
  }
  
  public static h a(r paramr)
  {
    return paramr;
  }
  
  public static c.l.i a(t paramt)
  {
    return paramt;
  }
  
  public static String a(i parami)
  {
    return x.a(parami);
  }
  
  public static String a(l paraml)
  {
    return x.a(paraml);
  }
}

/* Location:
 * Qualified Name:     c.g.b.w
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */