package c.g.b;

import c.l.g;

public abstract class q
  extends d
  implements g
{
  public boolean equals(Object paramObject)
  {
    if (paramObject == this) {
      return true;
    }
    if ((paramObject instanceof q))
    {
      paramObject = (q)paramObject;
      return (a().equals(((q)paramObject).a())) && (b().equals(((q)paramObject).b())) && (c().equals(((q)paramObject).c())) && (k.a(f(), ((q)paramObject).f()));
    }
    if ((paramObject instanceof g)) {
      return paramObject.equals(g());
    }
    return false;
  }
  
  public int hashCode()
  {
    return (a().hashCode() * 31 + b().hashCode()) * 31 + c().hashCode();
  }
  
  protected final g i()
  {
    return (g)super.h();
  }
  
  public String toString()
  {
    Object localObject = g();
    if (localObject != this) {
      return localObject.toString();
    }
    localObject = new StringBuilder("property ");
    ((StringBuilder)localObject).append(b());
    ((StringBuilder)localObject).append(" (Kotlin reflection is not available)");
    return ((StringBuilder)localObject).toString();
  }
}

/* Location:
 * Qualified Name:     c.g.b.q
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */