package c.g.b;

import c.g.a.e;
import c.g.a.f;
import c.g.a.g;
import c.g.a.h;
import c.g.a.j;
import c.g.a.l;
import c.g.a.m;
import c.g.a.n;
import c.g.a.o;
import c.g.a.p;
import c.g.a.q;
import c.g.a.r;
import c.g.a.s;
import c.g.a.t;
import c.g.a.u;
import c.g.a.v;
import c.g.a.w;
import java.util.Collection;
import java.util.Set;

public class aa
{
  public static Object a(Object paramObject, int paramInt)
  {
    if (paramObject != null)
    {
      boolean bool = paramObject instanceof c.c;
      int j = 1;
      if (bool)
      {
        if ((paramObject instanceof i)) {
          i = ((i)paramObject).e();
        } else if ((paramObject instanceof c.g.a.a)) {
          i = 0;
        } else if ((paramObject instanceof c.g.a.b)) {
          i = 1;
        } else if ((paramObject instanceof m)) {
          i = 2;
        } else if ((paramObject instanceof q)) {
          i = 3;
        } else if ((paramObject instanceof r)) {
          i = 4;
        } else if ((paramObject instanceof s)) {
          i = 5;
        } else if ((paramObject instanceof t)) {
          i = 6;
        } else if ((paramObject instanceof u)) {
          i = 7;
        } else if ((paramObject instanceof v)) {
          i = 8;
        } else if ((paramObject instanceof w)) {
          i = 9;
        } else if ((paramObject instanceof c.g.a.c)) {
          i = 10;
        } else if ((paramObject instanceof c.g.a.d)) {
          i = 11;
        } else if ((paramObject instanceof e)) {
          i = 12;
        } else if ((paramObject instanceof f)) {
          i = 13;
        } else if ((paramObject instanceof g)) {
          i = 14;
        } else if ((paramObject instanceof h)) {
          i = 15;
        } else if ((paramObject instanceof c.g.a.i)) {
          i = 16;
        } else if ((paramObject instanceof j)) {
          i = 17;
        } else if ((paramObject instanceof c.g.a.k)) {
          i = 18;
        } else if ((paramObject instanceof l)) {
          i = 19;
        } else if ((paramObject instanceof n)) {
          i = 20;
        } else if ((paramObject instanceof o)) {
          i = 21;
        } else if ((paramObject instanceof p)) {
          i = 22;
        } else {
          i = -1;
        }
        if (i == paramInt)
        {
          i = j;
          break label344;
        }
      }
      int i = 0;
      label344:
      if (i == 0) {
        a(paramObject, "kotlin.jvm.functions.Function".concat(String.valueOf(paramInt)));
      }
    }
    return paramObject;
  }
  
  private static <T extends Throwable> T a(T paramT)
  {
    return k.a(paramT, aa.class.getName());
  }
  
  public static Collection a(Object paramObject)
  {
    if (((paramObject instanceof c.g.b.a.a)) && (!(paramObject instanceof c.g.b.a.b))) {
      a(paramObject, "kotlin.collections.MutableCollection");
    }
    return c(paramObject);
  }
  
  private static void a(Object paramObject, String paramString)
  {
    if (paramObject == null) {
      paramObject = "null";
    } else {
      paramObject = paramObject.getClass().getName();
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append((String)paramObject);
    localStringBuilder.append(" cannot be cast to ");
    localStringBuilder.append(paramString);
    throw ((ClassCastException)a(new ClassCastException(localStringBuilder.toString())));
  }
  
  public static Set b(Object paramObject)
  {
    if (((paramObject instanceof c.g.b.a.a)) && (!(paramObject instanceof c.g.b.a.d))) {
      a(paramObject, "kotlin.collections.MutableSet");
    }
    return d(paramObject);
  }
  
  private static Collection c(Object paramObject)
  {
    try
    {
      paramObject = (Collection)paramObject;
      return (Collection)paramObject;
    }
    catch (ClassCastException paramObject)
    {
      throw ((ClassCastException)a((Throwable)paramObject));
    }
  }
  
  private static Set d(Object paramObject)
  {
    try
    {
      paramObject = (Set)paramObject;
      return (Set)paramObject;
    }
    catch (ClassCastException paramObject)
    {
      throw ((ClassCastException)a((Throwable)paramObject));
    }
  }
}

/* Location:
 * Qualified Name:     c.g.b.aa
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */