package c.g.b;

import c.u;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

public final class g
{
  private static final Object[] a = new Object[0];
  
  public static final Object[] a(Collection<?> paramCollection)
  {
    k.b(paramCollection, "collection");
    int i = paramCollection.size();
    if (i == 0) {
      return a;
    }
    Iterator localIterator = paramCollection.iterator();
    if (!localIterator.hasNext()) {
      return a;
    }
    paramCollection = new Object[i];
    i = 0;
    for (;;)
    {
      int j = i + 1;
      paramCollection[i] = localIterator.next();
      if (j >= paramCollection.length)
      {
        if (!localIterator.hasNext()) {
          return paramCollection;
        }
        int k = j * 3 + 1 >>> 1;
        i = k;
        if (k <= j) {
          if (j < 2147483645) {
            i = 2147483645;
          } else {
            throw ((Throwable)new OutOfMemoryError());
          }
        }
        paramCollection = Arrays.copyOf(paramCollection, i);
        k.a(paramCollection, "Arrays.copyOf(result, newSize)");
        i = j;
      }
      else
      {
        if (!localIterator.hasNext())
        {
          paramCollection = Arrays.copyOf(paramCollection, j);
          k.a(paramCollection, "Arrays.copyOf(result, size)");
          return paramCollection;
        }
        i = j;
      }
    }
  }
  
  public static final Object[] a(Collection<?> paramCollection, Object[] paramArrayOfObject)
  {
    k.b(paramCollection, "collection");
    if (paramArrayOfObject != null)
    {
      int j = paramCollection.size();
      int i = 0;
      if (j == 0)
      {
        if (paramArrayOfObject.length > 0) {
          paramArrayOfObject[0] = null;
        }
        return paramArrayOfObject;
      }
      Iterator localIterator = paramCollection.iterator();
      if (!localIterator.hasNext())
      {
        if (paramArrayOfObject.length > 0) {
          paramArrayOfObject[0] = null;
        }
        return paramArrayOfObject;
      }
      if (j <= paramArrayOfObject.length)
      {
        paramCollection = paramArrayOfObject;
      }
      else
      {
        paramCollection = Array.newInstance(paramArrayOfObject.getClass().getComponentType(), j);
        if (paramCollection == null) {
          break label225;
        }
        paramCollection = (Object[])paramCollection;
      }
      for (;;)
      {
        j = i + 1;
        paramCollection[i] = localIterator.next();
        if (j >= paramCollection.length)
        {
          if (!localIterator.hasNext()) {
            return paramCollection;
          }
          int k = j * 3 + 1 >>> 1;
          i = k;
          if (k <= j) {
            if (j < 2147483645) {
              i = 2147483645;
            } else {
              throw ((Throwable)new OutOfMemoryError());
            }
          }
          paramCollection = Arrays.copyOf(paramCollection, i);
          k.a(paramCollection, "Arrays.copyOf(result, newSize)");
          i = j;
        }
        else
        {
          if (!localIterator.hasNext())
          {
            if (paramCollection == paramArrayOfObject)
            {
              paramArrayOfObject[j] = null;
              return paramArrayOfObject;
            }
            paramCollection = Arrays.copyOf(paramCollection, j);
            k.a(paramCollection, "Arrays.copyOf(result, size)");
            return paramCollection;
          }
          i = j;
        }
      }
      label225:
      throw new u("null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
    }
    throw ((Throwable)new NullPointerException());
  }
}

/* Location:
 * Qualified Name:     c.g.b.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */