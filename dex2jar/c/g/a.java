package c.g;

import c.g.b.e;
import c.g.b.k;
import c.l.b;
import c.u;

public final class a
{
  public static final <T> Class<T> a(b<T> paramb)
  {
    k.b(paramb, "receiver$0");
    paramb = ((e)paramb).a();
    if (paramb != null) {
      return paramb;
    }
    throw new u("null cannot be cast to non-null type java.lang.Class<T>");
  }
  
  public static final <T> Class<T> b(b<T> paramb)
  {
    k.b(paramb, "receiver$0");
    paramb = ((e)paramb).a();
    if (!paramb.isPrimitive())
    {
      if (paramb != null) {
        return paramb;
      }
      throw new u("null cannot be cast to non-null type java.lang.Class<T>");
    }
    String str = paramb.getName();
    if (str != null) {
      switch (str.hashCode())
      {
      default: 
        break;
      case 109413500: 
        if (str.equals("short")) {
          paramb = Short.class;
        }
        break;
      case 97526364: 
        if (str.equals("float")) {
          paramb = Float.class;
        }
        break;
      case 64711720: 
        if (str.equals("boolean")) {
          paramb = Boolean.class;
        }
        break;
      case 3625364: 
        if (str.equals("void")) {
          paramb = Void.class;
        }
        break;
      case 3327612: 
        if (str.equals("long")) {
          paramb = Long.class;
        }
        break;
      case 3052374: 
        if (str.equals("char")) {
          paramb = Character.class;
        }
        break;
      case 3039496: 
        if (str.equals("byte")) {
          paramb = Byte.class;
        }
        break;
      case 104431: 
        if (str.equals("int")) {
          paramb = Integer.class;
        }
        break;
      case -1325958191: 
        if (str.equals("double")) {
          paramb = Double.class;
        }
        break;
      }
    }
    if (paramb != null) {
      return paramb;
    }
    throw new u("null cannot be cast to non-null type java.lang.Class<T>");
  }
}

/* Location:
 * Qualified Name:     c.g.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */