package c.g.a;

import c.c;

public abstract interface b<P1, R>
  extends c<R>
{
  public abstract R invoke(P1 paramP1);
}

/* Location:
 * Qualified Name:     c.g.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */