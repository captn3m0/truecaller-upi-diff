package c.g.a;

import c.c;

public abstract interface m<P1, P2, R>
  extends c<R>
{
  public abstract R invoke(P1 paramP1, P2 paramP2);
}

/* Location:
 * Qualified Name:     c.g.a.m
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */