package c;

public enum k
{
  static
  {
    k localk1 = new k("SYNCHRONIZED", 0);
    a = localk1;
    k localk2 = new k("PUBLICATION", 1);
    b = localk2;
    k localk3 = new k("NONE", 2);
    c = localk3;
    d = new k[] { localk1, localk2, localk3 };
  }
  
  private k() {}
}

/* Location:
 * Qualified Name:     c.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */