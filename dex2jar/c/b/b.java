package c.b;

public class b
{
  public static final <T extends Comparable<?>> int a(T paramT1, T paramT2)
  {
    if (paramT1 == paramT2) {
      return 0;
    }
    if (paramT1 == null) {
      return -1;
    }
    if (paramT2 == null) {
      return 1;
    }
    return paramT1.compareTo(paramT2);
  }
}

/* Location:
 * Qualified Name:     c.b.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */