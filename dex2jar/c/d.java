package c;

import java.io.Serializable;

public final class d<T>
  implements f<T>, Serializable
{
  private final T a;
  
  public d(T paramT)
  {
    a = paramT;
  }
  
  public final boolean a()
  {
    return true;
  }
  
  public final T b()
  {
    return (T)a;
  }
  
  public final String toString()
  {
    return String.valueOf(b());
  }
}

/* Location:
 * Qualified Name:     c.d
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */