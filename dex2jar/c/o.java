package c;

import c.g.b.k;
import java.io.Serializable;

public final class o<T>
  implements Serializable
{
  public static final a a = new a((byte)0);
  private final Object b;
  
  public static final boolean a(Object paramObject)
  {
    return !(paramObject instanceof b);
  }
  
  public static final boolean b(Object paramObject)
  {
    return paramObject instanceof b;
  }
  
  public static final Throwable c(Object paramObject)
  {
    if ((paramObject instanceof b)) {
      return a;
    }
    return null;
  }
  
  public static Object d(Object paramObject)
  {
    return paramObject;
  }
  
  public final boolean equals(Object paramObject)
  {
    Object localObject = b;
    return ((paramObject instanceof o)) && (k.a(localObject, b));
  }
  
  public final int hashCode()
  {
    Object localObject = b;
    if (localObject != null) {
      return localObject.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    Object localObject = b;
    if ((localObject instanceof b)) {
      return localObject.toString();
    }
    StringBuilder localStringBuilder = new StringBuilder("Success(");
    localStringBuilder.append(localObject);
    localStringBuilder.append(')');
    return localStringBuilder.toString();
  }
  
  public static final class a {}
  
  public static final class b
    implements Serializable
  {
    public final Throwable a;
    
    public b(Throwable paramThrowable)
    {
      a = paramThrowable;
    }
    
    public final boolean equals(Object paramObject)
    {
      return ((paramObject instanceof b)) && (k.a(a, a));
    }
    
    public final int hashCode()
    {
      return a.hashCode();
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder("Failure(");
      localStringBuilder.append(a);
      localStringBuilder.append(')');
      return localStringBuilder.toString();
    }
  }
}

/* Location:
 * Qualified Name:     c.o
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */