package c;

import c.g.b.k;
import java.io.Serializable;

public final class o$b
  implements Serializable
{
  public final Throwable a;
  
  public o$b(Throwable paramThrowable)
  {
    a = paramThrowable;
  }
  
  public final boolean equals(Object paramObject)
  {
    return ((paramObject instanceof b)) && (k.a(a, a));
  }
  
  public final int hashCode()
  {
    return a.hashCode();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("Failure(");
    localStringBuilder.append(a);
    localStringBuilder.append(')');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     c.o.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */