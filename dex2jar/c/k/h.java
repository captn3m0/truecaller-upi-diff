package c.k;

public final class h
  extends f
  implements e<Integer>
{
  public static final a e = new a((byte)0);
  private static final h f = new h(1, 0);
  
  public h(int paramInt1, int paramInt2)
  {
    super(paramInt1, paramInt2, 1);
  }
  
  public final boolean a()
  {
    return a > b;
  }
  
  public final boolean a(int paramInt)
  {
    return (a <= paramInt) && (paramInt <= b);
  }
  
  public final boolean equals(Object paramObject)
  {
    if ((paramObject instanceof h)) {
      if ((!a()) || (!((h)paramObject).a()))
      {
        int i = a;
        paramObject = (h)paramObject;
        if ((i != a) || (b != b)) {}
      }
      else
      {
        return true;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    if (a()) {
      return -1;
    }
    return a * 31 + b;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(a);
    localStringBuilder.append("..");
    localStringBuilder.append(b);
    return localStringBuilder.toString();
  }
  
  public static final class a {}
}

/* Location:
 * Qualified Name:     c.k.h
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */