package c.k;

import c.a.ae;
import java.util.NoSuchElementException;

public final class g
  extends ae
{
  private final int a;
  private boolean b;
  private int c;
  private final int d;
  
  public g(int paramInt1, int paramInt2, int paramInt3)
  {
    d = paramInt3;
    a = paramInt2;
    paramInt3 = d;
    boolean bool = true;
    if (paramInt3 > 0 ? paramInt1 > paramInt2 : paramInt1 < paramInt2) {
      bool = false;
    }
    b = bool;
    if (!b) {
      paramInt1 = a;
    }
    c = paramInt1;
  }
  
  public final int a()
  {
    int i = c;
    if (i == a)
    {
      if (b)
      {
        b = false;
        return i;
      }
      throw ((Throwable)new NoSuchElementException());
    }
    c = (d + i);
    return i;
  }
  
  public final boolean hasNext()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     c.k.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */