package c.k;

import c.e.c;
import c.g.b.a.a;

public class f
  implements a, Iterable<Integer>
{
  public static final a d = new a((byte)0);
  public final int a;
  public final int b;
  public final int c;
  
  public f(int paramInt1, int paramInt2, int paramInt3)
  {
    if (paramInt3 != 0)
    {
      if (paramInt3 != Integer.MIN_VALUE)
      {
        a = paramInt1;
        b = c.a(paramInt1, paramInt2, paramInt3);
        c = paramInt3;
        return;
      }
      throw ((Throwable)new IllegalArgumentException("Step must be greater than Int.MIN_VALUE to avoid overflow on negation."));
    }
    throw ((Throwable)new IllegalArgumentException("Step must be non-zero."));
  }
  
  public boolean a()
  {
    if (c > 0) {
      return a > b;
    }
    return a < b;
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject instanceof f)) {
      if ((!a()) || (!((f)paramObject).a()))
      {
        int i = a;
        paramObject = (f)paramObject;
        if ((i != a) || (b != b) || (c != c)) {}
      }
      else
      {
        return true;
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    if (a()) {
      return -1;
    }
    return (a * 31 + b) * 31 + c;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder;
    if (c > 0)
    {
      localStringBuilder = new StringBuilder();
      localStringBuilder.append(a);
      localStringBuilder.append("..");
      localStringBuilder.append(b);
      localStringBuilder.append(" step ");
    }
    for (int i = c;; i = -c)
    {
      localStringBuilder.append(i);
      return localStringBuilder.toString();
      localStringBuilder = new StringBuilder();
      localStringBuilder.append(a);
      localStringBuilder.append(" downTo ");
      localStringBuilder.append(b);
      localStringBuilder.append(" step ");
    }
  }
  
  public static final class a
  {
    public static f a(int paramInt1, int paramInt2)
    {
      return new f(paramInt1, paramInt2, -1);
    }
  }
}

/* Location:
 * Qualified Name:     c.k.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */