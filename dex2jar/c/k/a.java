package c.k;

import c.e.c;

public class a
  implements c.g.b.a.a, Iterable<Character>
{
  public static final a c = new a((byte)0);
  public final char a;
  public final char b;
  private final int d;
  
  public a(char paramChar1, char paramChar2)
  {
    a = paramChar1;
    b = ((char)c.a(paramChar1, paramChar2, 1));
    d = 1;
  }
  
  public boolean a()
  {
    if (d > 0) {
      return a > b;
    }
    return a < b;
  }
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject instanceof a)) {
      if ((!a()) || (!((a)paramObject).a()))
      {
        int i = a;
        paramObject = (a)paramObject;
        if ((i != a) || (b != b) || (d != d)) {}
      }
      else
      {
        return true;
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    if (a()) {
      return -1;
    }
    return (a * '\037' + b) * 31 + d;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder;
    if (d > 0)
    {
      localStringBuilder = new StringBuilder();
      localStringBuilder.append(a);
      localStringBuilder.append("..");
      localStringBuilder.append(b);
      localStringBuilder.append(" step ");
    }
    for (int i = d;; i = -d)
    {
      localStringBuilder.append(i);
      return localStringBuilder.toString();
      localStringBuilder = new StringBuilder();
      localStringBuilder.append(a);
      localStringBuilder.append(" downTo ");
      localStringBuilder.append(b);
      localStringBuilder.append(" step ");
    }
  }
  
  public static final class a {}
}

/* Location:
 * Qualified Name:     c.k.a
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */