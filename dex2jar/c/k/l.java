package c.k;

public class l
  extends k
{
  public static final float a(float paramFloat)
  {
    if (paramFloat < 0.0F) {
      return 0.0F;
    }
    if (paramFloat > 1.0F) {
      return 1.0F;
    }
    return paramFloat;
  }
  
  public static final int a(int paramInt1, int paramInt2, int paramInt3)
  {
    if (paramInt2 <= paramInt3)
    {
      if (paramInt1 < paramInt2) {
        return paramInt2;
      }
      if (paramInt1 > paramInt3) {
        return paramInt3;
      }
      return paramInt1;
    }
    StringBuilder localStringBuilder = new StringBuilder("Cannot coerce value to an empty range: maximum ");
    localStringBuilder.append(paramInt3);
    localStringBuilder.append(" is less than minimum ");
    localStringBuilder.append(paramInt2);
    localStringBuilder.append('.');
    throw ((Throwable)new IllegalArgumentException(localStringBuilder.toString()));
  }
  
  public static final int a(int paramInt, e<Integer> parame)
  {
    c.g.b.k.b(parame, "range");
    if ((parame instanceof d)) {
      return ((Number)i.a((Comparable)Integer.valueOf(paramInt), (d)parame)).intValue();
    }
    if (!parame.a())
    {
      if (paramInt < ((Number)parame.b()).intValue()) {
        return ((Number)parame.b()).intValue();
      }
      if (paramInt > ((Number)parame.c()).intValue()) {
        return ((Number)parame.c()).intValue();
      }
      return paramInt;
    }
    StringBuilder localStringBuilder = new StringBuilder("Cannot coerce value to an empty range: ");
    localStringBuilder.append(parame);
    localStringBuilder.append('.');
    throw ((Throwable)new IllegalArgumentException(localStringBuilder.toString()));
  }
  
  public static final long a(long paramLong1, long paramLong2)
  {
    if (paramLong1 < paramLong2) {
      return paramLong2;
    }
    return paramLong1;
  }
  
  public static final f a(int paramInt1, int paramInt2)
  {
    f.a locala = f.d;
    return f.a.a(paramInt1, paramInt2);
  }
  
  public static final <T extends Comparable<? super T>> T a(T paramT, d<T> paramd)
  {
    c.g.b.k.b(paramT, "receiver$0");
    c.g.b.k.b(paramd, "range");
    if (!paramd.a())
    {
      paramd.b();
      if (paramd.d())
      {
        paramd.b();
        if (!paramd.d()) {
          return paramd.b();
        }
      }
      paramd.c();
      if (paramd.d())
      {
        paramd.c();
        if (!paramd.d()) {
          return paramd.c();
        }
      }
      return paramT;
    }
    paramT = new StringBuilder("Cannot coerce value to an empty range: ");
    paramT.append(paramd);
    paramT.append('.');
    throw ((Throwable)new IllegalArgumentException(paramT.toString()));
  }
  
  public static final long b(long paramLong1, long paramLong2)
  {
    if (paramLong1 > paramLong2) {
      return paramLong2;
    }
    return paramLong1;
  }
  
  public static final h b(int paramInt1, int paramInt2)
  {
    if (paramInt2 <= Integer.MIN_VALUE)
    {
      h.a locala = h.e;
      return h.d();
    }
    return new h(paramInt1, paramInt2 - 1);
  }
  
  public static final int c(int paramInt1, int paramInt2)
  {
    if (paramInt1 < paramInt2) {
      return paramInt2;
    }
    return paramInt1;
  }
  
  public static final int d(int paramInt1, int paramInt2)
  {
    if (paramInt1 > paramInt2) {
      return paramInt2;
    }
    return paramInt1;
  }
}

/* Location:
 * Qualified Name:     c.k.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */