package c.k;

public final class c
  extends a
  implements e<Character>
{
  public static final a d = new a((byte)0);
  private static final c e = new c('\001', '\000');
  
  public c(char paramChar1, char paramChar2)
  {
    super(paramChar1, paramChar2);
  }
  
  public final boolean a()
  {
    return a > b;
  }
  
  public final boolean equals(Object paramObject)
  {
    if ((paramObject instanceof c)) {
      if ((!a()) || (!((c)paramObject).a()))
      {
        int i = a;
        paramObject = (c)paramObject;
        if ((i != a) || (b != b)) {}
      }
      else
      {
        return true;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    if (a()) {
      return -1;
    }
    return a * '\037' + b;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(a);
    localStringBuilder.append("..");
    localStringBuilder.append(b);
    return localStringBuilder.toString();
  }
  
  public static final class a {}
}

/* Location:
 * Qualified Name:     c.k.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */