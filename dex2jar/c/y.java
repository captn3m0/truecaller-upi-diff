package c;

import c.g.a.a;
import c.g.b.k;
import java.io.Serializable;

public final class y<T>
  implements f<T>, Serializable
{
  private a<? extends T> a;
  private Object b;
  
  public y(a<? extends T> parama)
  {
    a = parama;
    b = v.a;
  }
  
  private final Object writeReplace()
  {
    return new d(b());
  }
  
  public final boolean a()
  {
    return b != v.a;
  }
  
  public final T b()
  {
    if (b == v.a)
    {
      a locala = a;
      if (locala == null) {
        k.a();
      }
      b = locala.invoke();
      a = null;
    }
    return (T)b;
  }
  
  public final String toString()
  {
    if (a()) {
      return String.valueOf(b());
    }
    return "Lazy value not initialized yet.";
  }
}

/* Location:
 * Qualified Name:     c.y
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */