package org.a.a;

import org.a.a.a.l;
import org.a.a.d.n;
import org.joda.convert.ToString;

public final class g
  extends l
{
  public static final g a = new g(0);
  public static final g b = new g(1);
  public static final g c = new g(2);
  public static final g d = new g(3);
  public static final g e = new g(4);
  public static final g f = new g(5);
  public static final g g = new g(6);
  public static final g h = new g(7);
  public static final g i = new g(Integer.MAX_VALUE);
  public static final g j = new g(Integer.MIN_VALUE);
  private static final n l = org.a.a.d.j.a().a(s.c());
  private static final long serialVersionUID = 87525275727380865L;
  
  private g(int paramInt)
  {
    super(paramInt);
  }
  
  private static g a(int paramInt)
  {
    if (paramInt != Integer.MIN_VALUE)
    {
      if (paramInt != Integer.MAX_VALUE)
      {
        switch (paramInt)
        {
        default: 
          return new g(paramInt);
        case 7: 
          return h;
        case 6: 
          return g;
        case 5: 
          return f;
        case 4: 
          return e;
        case 3: 
          return d;
        case 2: 
          return c;
        case 1: 
          return b;
        }
        return a;
      }
      return i;
    }
    return j;
  }
  
  public static g a(x paramx1, x paramx2)
  {
    return a(l.a(paramx1, paramx2, j.f()));
  }
  
  private Object readResolve()
  {
    return a(k);
  }
  
  public final j a()
  {
    return j.f();
  }
  
  public final s b()
  {
    return s.c();
  }
  
  public final int c()
  {
    return k;
  }
  
  @ToString
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("P");
    localStringBuilder.append(String.valueOf(k));
    localStringBuilder.append("D");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     org.a.a.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */