package org.a.a;

import java.io.Serializable;
import org.a.a.a.g;
import org.a.a.d.i.a;
import org.joda.convert.FromString;

public final class b
  extends g
  implements Serializable, v
{
  private static final long serialVersionUID = -5171125899451703815L;
  
  public b() {}
  
  public b(long paramLong)
  {
    super(paramLong);
  }
  
  public b(long paramLong, a parama)
  {
    super(paramLong, parama);
  }
  
  public b(f paramf)
  {
    super(paramf, (byte)0);
  }
  
  @FromString
  public static b a(String paramString)
  {
    org.a.a.d.b localb = i.a.a();
    if (d != true) {
      localb = new org.a.a.d.b(a, b, c, true, e, null, f, g);
    }
    return localb.b(paramString);
  }
  
  public static b ay_()
  {
    return new b();
  }
  
  public final b a(int paramInt)
  {
    if (paramInt == 0) {
      return this;
    }
    return a_(b.s().a(a, paramInt));
  }
  
  public final b a(f paramf)
  {
    paramf = e.a(b.a(paramf));
    if (paramf == b) {
      return this;
    }
    return new b(a, paramf);
  }
  
  public final b a_(long paramLong)
  {
    if (paramLong == a) {
      return this;
    }
    return new b(paramLong, b);
  }
  
  public final b az_()
  {
    return e().a(b().a());
  }
  
  public final b b(int paramInt)
  {
    if (paramInt == 0) {
      return this;
    }
    return a_(b.l().a(a, paramInt));
  }
  
  public final b b(long paramLong)
  {
    if (paramLong == 0L) {
      return this;
    }
    return a_(b.a(a, paramLong));
  }
  
  public final b c()
  {
    return a_(b.c().a(a, 10000));
  }
  
  public final b c(int paramInt)
  {
    if (paramInt == 0) {
      return this;
    }
    return a_(b.s().b(a, paramInt));
  }
  
  public final b d()
  {
    return a_(b.B().b(a, 1));
  }
  
  public final p e()
  {
    return new p(a, b);
  }
  
  public final b f()
  {
    return a_(b.d().b(a, 0));
  }
}

/* Location:
 * Qualified Name:     org.a.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */