package org.a.a;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.a.a.b.q;
import org.a.a.d.i.a;
import org.joda.convert.ToString;

public final class p
  extends org.a.a.a.j
  implements Serializable, z
{
  private static final Set<j> c;
  private static final long serialVersionUID = -8775358157899L;
  public final long a;
  public final a b;
  private transient int d;
  
  static
  {
    HashSet localHashSet = new HashSet();
    c = localHashSet;
    localHashSet.add(j.f());
    c.add(j.g());
    c.add(j.i());
    c.add(j.h());
    c.add(j.j());
    c.add(j.k());
    c.add(j.l());
  }
  
  public p()
  {
    this(e.a(), q.M());
  }
  
  public p(long paramLong)
  {
    this(paramLong, q.M());
  }
  
  public p(long paramLong, a parama)
  {
    a locala = e.a(parama);
    f localf2 = locala.a();
    f localf1 = f.a;
    parama = localf1;
    if (localf1 == null) {
      parama = f.a();
    }
    if (parama != localf2) {
      paramLong = parama.a(localf2.f(paramLong), paramLong);
    }
    parama = locala.b();
    a = parama.u().d(paramLong);
    b = parama;
  }
  
  public static p a()
  {
    return new p();
  }
  
  private Object readResolve()
  {
    if (b == null) {
      return new p(a, q.L());
    }
    if (!f.a.equals(b.a())) {
      return new p(a, b.b());
    }
    return this;
  }
  
  public final int a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      throw new IndexOutOfBoundsException("Invalid index: ".concat(String.valueOf(paramInt)));
    case 2: 
      return b.u().a(a);
    case 1: 
      return b.C().a(a);
    }
    return b.E().a(a);
  }
  
  public final int a(d paramd)
  {
    if (paramd != null)
    {
      if (b(paramd)) {
        return paramd.a(b).a(a);
      }
      StringBuilder localStringBuilder = new StringBuilder("Field '");
      localStringBuilder.append(paramd);
      localStringBuilder.append("' is not supported");
      throw new IllegalArgumentException(localStringBuilder.toString());
    }
    throw new IllegalArgumentException("The DateTimeFieldType must not be null");
  }
  
  public final int a(z paramz)
  {
    if (this == paramz) {
      return 0;
    }
    if ((paramz instanceof p))
    {
      p localp = (p)paramz;
      if (b.equals(b))
      {
        long l1 = a;
        long l2 = a;
        if (l1 < l2) {
          return -1;
        }
        if (l1 == l2) {
          return 0;
        }
        return 1;
      }
    }
    return super.a(paramz);
  }
  
  public final b a(f paramf)
  {
    paramf = e.a(paramf);
    a locala = b.a(paramf);
    long l = paramf.g(a + 21600000L);
    return new b(locala.u().d(l), locala);
  }
  
  public final c a(int paramInt, a parama)
  {
    switch (paramInt)
    {
    default: 
      throw new IndexOutOfBoundsException("Invalid index: ".concat(String.valueOf(paramInt)));
    case 2: 
      return parama.u();
    case 1: 
      return parama.C();
    }
    return parama.E();
  }
  
  public final a b()
  {
    return b;
  }
  
  public final boolean b(d paramd)
  {
    if (paramd == null) {
      return false;
    }
    j localj = paramd.x();
    if ((!c.contains(localj)) && (localj.a(b).d() < b.s().d())) {
      return false;
    }
    return paramd.a(b).c();
  }
  
  public final int c()
  {
    return b.E().a(a);
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject instanceof p))
    {
      p localp = (p)paramObject;
      if (b.equals(b)) {
        return a == a;
      }
    }
    return super.equals(paramObject);
  }
  
  public final int hashCode()
  {
    int j = d;
    int i = j;
    if (j == 0)
    {
      i = super.hashCode();
      d = i;
    }
    return i;
  }
  
  @ToString
  public final String toString()
  {
    return i.a.d().a(this);
  }
}

/* Location:
 * Qualified Name:     org.a.a.p
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */