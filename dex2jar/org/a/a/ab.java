package org.a.a;

import org.a.a.a.l;
import org.a.a.d.n;
import org.joda.convert.ToString;

public final class ab
  extends l
{
  public static final ab a = new ab(0);
  public static final ab b = new ab(1);
  public static final ab c = new ab(2);
  public static final ab d = new ab(3);
  public static final ab e = new ab(Integer.MAX_VALUE);
  public static final ab f = new ab(Integer.MIN_VALUE);
  private static final n g = org.a.a.d.j.a().a(s.d());
  private static final long serialVersionUID = 87525275727380862L;
  
  private ab(int paramInt)
  {
    super(paramInt);
  }
  
  private static ab a(int paramInt)
  {
    if (paramInt != Integer.MIN_VALUE)
    {
      if (paramInt != Integer.MAX_VALUE)
      {
        switch (paramInt)
        {
        default: 
          return new ab(paramInt);
        case 3: 
          return d;
        case 2: 
          return c;
        case 1: 
          return b;
        }
        return a;
      }
      return e;
    }
    return f;
  }
  
  public static ab a(x paramx1, x paramx2)
  {
    return a(l.a(paramx1, paramx2, j.b()));
  }
  
  private Object readResolve()
  {
    return a(k);
  }
  
  public final j a()
  {
    return j.b();
  }
  
  public final s b()
  {
    return s.d();
  }
  
  public final int c()
  {
    return k;
  }
  
  @ToString
  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("PT");
    localStringBuilder.append(String.valueOf(k));
    localStringBuilder.append("S");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     org.a.a.ab
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */