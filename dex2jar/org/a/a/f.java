package org.a.a;

import java.io.File;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicReference;
import org.a.a.d.b;
import org.a.a.e.c;
import org.a.a.e.d;
import org.a.a.e.e;
import org.a.a.e.g;
import org.joda.convert.FromString;

public abstract class f
  implements Serializable
{
  public static final f a = ac.c;
  private static final AtomicReference<org.a.a.e.f> c = new AtomicReference();
  private static final AtomicReference<e> d = new AtomicReference();
  private static final AtomicReference<f> e = new AtomicReference();
  private static final long serialVersionUID = 5546345482340108586L;
  public final String b;
  
  protected f(String paramString)
  {
    if (paramString != null)
    {
      b = paramString;
      return;
    }
    throw new IllegalArgumentException("Id must not be null");
  }
  
  public static f a()
  {
    localObject3 = (f)e.get();
    Object localObject5 = localObject3;
    if (localObject3 == null) {
      localObject1 = localObject3;
    }
    try
    {
      localObject5 = System.getProperty("user.timezone");
      localObject1 = localObject3;
      if (localObject5 != null)
      {
        localObject1 = localObject3;
        localObject5 = a((String)localObject5);
        localObject1 = localObject5;
      }
    }
    catch (RuntimeException localRuntimeException)
    {
      for (;;)
      {
        localObject2 = localObject3;
      }
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      for (;;)
      {
        Object localObject2;
        Object localObject4 = localObject2;
      }
    }
    localObject3 = localObject1;
    if (localObject1 == null) {
      localObject3 = a(TimeZone.getDefault());
    }
    Object localObject1 = localObject3;
    if (localObject3 == null) {
      localObject1 = a;
    }
    localObject5 = localObject1;
    if (!e.compareAndSet(null, localObject1)) {
      localObject5 = (f)e.get();
    }
    return (f)localObject5;
  }
  
  public static f a(int paramInt)
  {
    if ((paramInt >= -86399999) && (paramInt <= 86399999)) {
      return a(b(paramInt), paramInt);
    }
    throw new IllegalArgumentException("Millis out of range: ".concat(String.valueOf(paramInt)));
  }
  
  @FromString
  public static f a(String paramString)
  {
    if (paramString == null) {
      return a();
    }
    if (paramString.equals("UTC")) {
      return a;
    }
    Object localObject = e().a(paramString);
    if (localObject != null) {
      return (f)localObject;
    }
    if ((!paramString.startsWith("+")) && (!paramString.startsWith("-")))
    {
      localObject = new StringBuilder("The datetime zone id '");
      ((StringBuilder)localObject).append(paramString);
      ((StringBuilder)localObject).append("' is not recognised");
      throw new IllegalArgumentException(((StringBuilder)localObject).toString());
    }
    int i = d(paramString);
    if (i == 0L) {
      return a;
    }
    return a(b(i), i);
  }
  
  private static f a(String paramString, int paramInt)
  {
    if (paramInt == 0) {
      return a;
    }
    return new d(paramString, null, paramInt, paramInt);
  }
  
  public static f a(TimeZone paramTimeZone)
  {
    if (paramTimeZone == null) {
      return a();
    }
    String str1 = paramTimeZone.getID();
    if (str1 != null)
    {
      if (str1.equals("UTC")) {
        return a;
      }
      paramTimeZone = null;
      String str2 = c(str1);
      org.a.a.e.f localf = e();
      if (str2 != null) {
        paramTimeZone = localf.a(str2);
      }
      Object localObject = paramTimeZone;
      if (paramTimeZone == null) {
        localObject = localf.a(str1);
      }
      if (localObject != null) {
        return (f)localObject;
      }
      if ((str2 == null) && ((str1.startsWith("GMT+")) || (str1.startsWith("GMT-"))))
      {
        localObject = str1.substring(3);
        paramTimeZone = (TimeZone)localObject;
        if (((String)localObject).length() > 2)
        {
          char c1 = ((String)localObject).charAt(1);
          paramTimeZone = (TimeZone)localObject;
          if (c1 > '9')
          {
            paramTimeZone = (TimeZone)localObject;
            if (Character.isDigit(c1)) {
              paramTimeZone = b((String)localObject);
            }
          }
        }
        int i = d(paramTimeZone);
        if (i == 0L) {
          return a;
        }
        return a(b(i), i);
      }
      paramTimeZone = new StringBuilder("The datetime zone id '");
      paramTimeZone.append(str1);
      paramTimeZone.append("' is not recognised");
      throw new IllegalArgumentException(paramTimeZone.toString());
    }
    throw new IllegalArgumentException("The TimeZone id must not be null");
  }
  
  public static void a(org.a.a.e.f paramf)
    throws SecurityException
  {
    SecurityManager localSecurityManager = System.getSecurityManager();
    if (localSecurityManager != null) {
      localSecurityManager.checkPermission(new o("DateTimeZone.setProvider"));
    }
    b(paramf);
    c.set(paramf);
  }
  
  public static void a(f paramf)
    throws SecurityException
  {
    SecurityManager localSecurityManager = System.getSecurityManager();
    if (localSecurityManager != null) {
      localSecurityManager.checkPermission(new o("DateTimeZone.setDefault"));
    }
    if (paramf != null)
    {
      e.set(paramf);
      return;
    }
    throw new IllegalArgumentException("The datetime zone must not be null");
  }
  
  public static String b(int paramInt)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    if (paramInt >= 0)
    {
      localStringBuffer.append('+');
    }
    else
    {
      localStringBuffer.append('-');
      paramInt = -paramInt;
    }
    int i = paramInt / 3600000;
    org.a.a.d.h.a(localStringBuffer, i, 2);
    paramInt -= i * 3600000;
    i = paramInt / 60000;
    localStringBuffer.append(':');
    org.a.a.d.h.a(localStringBuffer, i, 2);
    paramInt -= i * 60000;
    if (paramInt == 0) {
      return localStringBuffer.toString();
    }
    i = paramInt / 1000;
    localStringBuffer.append(':');
    org.a.a.d.h.a(localStringBuffer, i, 2);
    paramInt -= i * 1000;
    if (paramInt == 0) {
      return localStringBuffer.toString();
    }
    localStringBuffer.append('.');
    org.a.a.d.h.a(localStringBuffer, paramInt, 3);
    return localStringBuffer.toString();
  }
  
  private static String b(String paramString)
  {
    paramString = new StringBuilder(paramString);
    int i = 0;
    while (i < paramString.length())
    {
      int j = Character.digit(paramString.charAt(i), 10);
      if (j >= 0) {
        paramString.setCharAt(i, (char)(j + 48));
      }
      i += 1;
    }
    return paramString.toString();
  }
  
  public static Set<String> b()
  {
    return e().a();
  }
  
  private static org.a.a.e.f b(org.a.a.e.f paramf)
  {
    Set localSet = paramf.a();
    if ((localSet != null) && (localSet.size() != 0))
    {
      if (localSet.contains("UTC"))
      {
        if (a.equals(paramf.a("UTC"))) {
          return paramf;
        }
        throw new IllegalArgumentException("Invalid UTC zone provided");
      }
      throw new IllegalArgumentException("The provider doesn't support UTC");
    }
    throw new IllegalArgumentException("The provider doesn't have any available ids");
  }
  
  private static String c(String paramString)
  {
    return (String)f.a.a.get(paramString);
  }
  
  public static e c()
  {
    e locale2 = (e)d.get();
    e locale1 = locale2;
    if (locale2 == null)
    {
      locale2 = g();
      locale1 = locale2;
      if (!d.compareAndSet(null, locale2)) {
        locale1 = (e)d.get();
      }
    }
    return locale1;
  }
  
  private static int d(String paramString)
  {
    return -(int)f.a.b.a(paramString);
  }
  
  private static org.a.a.e.f e()
  {
    org.a.a.e.f localf2 = (org.a.a.e.f)c.get();
    org.a.a.e.f localf1 = localf2;
    if (localf2 == null)
    {
      localf2 = f();
      localf1 = localf2;
      if (!c.compareAndSet(null, localf2)) {
        localf1 = (org.a.a.e.f)c.get();
      }
    }
    return localf1;
  }
  
  private static org.a.a.e.f f()
  {
    for (;;)
    {
      try
      {
        Object localObject1 = System.getProperty("org.joda.time.DateTimeZone.Provider");
        if (localObject1 != null) {
          try
          {
            localObject1 = b((org.a.a.e.f)Class.forName((String)localObject1).newInstance());
            return (org.a.a.e.f)localObject1;
          }
          catch (Exception localException1)
          {
            throw new RuntimeException(localException1);
          }
        }
      }
      catch (SecurityException localSecurityException1)
      {
        Object localObject2;
        org.a.a.e.f localf;
        continue;
      }
      try
      {
        localObject2 = System.getProperty("org.joda.time.DateTimeZone.Folder");
        if (localObject2 != null) {
          try
          {
            localObject2 = b(new org.a.a.e.h(new File((String)localObject2)));
            return (org.a.a.e.f)localObject2;
          }
          catch (Exception localException2)
          {
            throw new RuntimeException(localException2);
          }
        }
      }
      catch (SecurityException localSecurityException2)
      {
        continue;
      }
      try
      {
        localf = b(new org.a.a.e.h("org/joda/time/tz/data"));
        return localf;
      }
      catch (Exception localException3)
      {
        localException3.printStackTrace();
        return new g();
      }
    }
  }
  
  private static e g()
  {
    try
    {
      Object localObject1 = System.getProperty("org.joda.time.DateTimeZone.NameProvider");
      if (localObject1 != null) {
        try
        {
          localObject1 = (e)Class.forName((String)localObject1).newInstance();
        }
        catch (Exception localException)
        {
          throw new RuntimeException(localException);
        }
      }
    }
    catch (SecurityException localSecurityException)
    {
      Object localObject2;
      Object localObject3;
      for (;;) {}
    }
    localObject2 = null;
    localObject3 = localObject2;
    if (localObject2 == null) {
      localObject3 = new c();
    }
    return (e)localObject3;
  }
  
  public final long a(long paramLong1, long paramLong2)
  {
    int i = b(paramLong2);
    paramLong2 = paramLong1 - i;
    if (b(paramLong2) == i) {
      return paramLong2;
    }
    return g(paramLong1);
  }
  
  public abstract String a(long paramLong);
  
  public abstract int b(long paramLong);
  
  public abstract int c(long paramLong);
  
  public abstract boolean d();
  
  public final boolean d(long paramLong)
  {
    return b(paramLong) == c(paramLong);
  }
  
  public int e(long paramLong)
  {
    int i = b(paramLong);
    long l3 = paramLong - i;
    int j = b(l3);
    if (i != j)
    {
      if (i - j < 0)
      {
        long l2 = h(l3);
        long l1 = l2;
        if (l2 == l3) {
          l1 = Long.MAX_VALUE;
        }
        l3 = paramLong - j;
        l2 = h(l3);
        paramLong = l2;
        if (l2 == l3) {
          paramLong = Long.MAX_VALUE;
        }
        if (l1 != paramLong) {
          return i;
        }
      }
    }
    else if (i >= 0)
    {
      paramLong = i(l3);
      if (paramLong < l3)
      {
        int k = b(paramLong);
        if (l3 - paramLong <= k - i) {
          return k;
        }
      }
    }
    return j;
  }
  
  public abstract boolean equals(Object paramObject);
  
  public final long f(long paramLong)
  {
    long l1 = b(paramLong);
    long l2 = paramLong + l1;
    if ((paramLong ^ l2) < 0L)
    {
      if ((paramLong ^ l1) < 0L) {
        return l2;
      }
      throw new ArithmeticException("Adding time zone offset caused overflow");
    }
    return l2;
  }
  
  public final long g(long paramLong)
  {
    int i = b(paramLong);
    long l4 = paramLong - i;
    int j = b(l4);
    if ((i != j) && (i < 0))
    {
      long l3 = h(l4);
      l2 = Long.MAX_VALUE;
      l1 = l3;
      if (l3 == l4) {
        l1 = Long.MAX_VALUE;
      }
      l4 = paramLong - j;
      l3 = h(l4);
      if (l3 != l4) {
        l2 = l3;
      }
      if (l1 != l2) {}
    }
    else
    {
      i = j;
    }
    long l1 = i;
    long l2 = paramLong - l1;
    if ((paramLong ^ l2) < 0L)
    {
      if ((paramLong ^ l1) >= 0L) {
        return l2;
      }
      throw new ArithmeticException("Subtracting time zone offset caused overflow");
    }
    return l2;
  }
  
  public abstract long h(long paramLong);
  
  public int hashCode()
  {
    return b.hashCode() + 57;
  }
  
  public abstract long i(long paramLong);
  
  public String toString()
  {
    return b;
  }
  
  protected Object writeReplace()
    throws ObjectStreamException
  {
    return new f.b(b);
  }
}

/* Location:
 * Qualified Name:     org.a.a.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */