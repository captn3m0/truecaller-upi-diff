package org.a.a.a;

import org.a.a.aa;
import org.a.a.d.n;
import org.a.a.s;
import org.joda.convert.ToString;

public abstract class f
  implements aa
{
  public final int a(org.a.a.j paramj)
  {
    int i = b().b(paramj);
    if (i == -1) {
      return 0;
    }
    return d(i);
  }
  
  public final org.a.a.j c(int paramInt)
  {
    return bi[paramInt];
  }
  
  public final int[] c()
  {
    int[] arrayOfInt = new int[d()];
    int i = 0;
    while (i < arrayOfInt.length)
    {
      arrayOfInt[i] = d(i);
      i += 1;
    }
    return arrayOfInt;
  }
  
  public final int d()
  {
    return bi.length;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof aa)) {
      return false;
    }
    paramObject = (aa)paramObject;
    if (d() != ((aa)paramObject).d()) {
      return false;
    }
    int j = d();
    int i = 0;
    while (i < j) {
      if (d(i) == ((aa)paramObject).d(i))
      {
        if (c(i) != ((aa)paramObject).c(i)) {
          return false;
        }
        i += 1;
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public int hashCode()
  {
    int k = d();
    int j = 17;
    int i = 0;
    while (i < k)
    {
      j = (j * 27 + d(i)) * 27 + c(i).hashCode();
      i += 1;
    }
    return j;
  }
  
  @ToString
  public String toString()
  {
    return org.a.a.d.j.a().a(this);
  }
}

/* Location:
 * Qualified Name:     org.a.a.a.f
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */