package org.a.a.a;

import org.a.a.c.g;
import org.a.a.d.b;
import org.a.a.d.i.a;
import org.a.a.e;
import org.a.a.x;
import org.joda.convert.ToString;

public abstract class c
  implements x
{
  public final int a(x paramx)
  {
    if (this == paramx) {
      return 0;
    }
    long l1 = paramx.a();
    long l2 = a();
    if (l2 == l1) {
      return 0;
    }
    if (l2 < l1) {
      return -1;
    }
    return 1;
  }
  
  public final boolean b(x paramx)
  {
    return c(e.a(paramx));
  }
  
  public final boolean c(long paramLong)
  {
    return a() > paramLong;
  }
  
  public final boolean c(x paramx)
  {
    return d(e.a(paramx));
  }
  
  public final boolean d(long paramLong)
  {
    return a() < paramLong;
  }
  
  public final boolean d(x paramx)
  {
    return e(e.a(paramx));
  }
  
  public final boolean e(long paramLong)
  {
    return a() == paramLong;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof x)) {
      return false;
    }
    paramObject = (x)paramObject;
    return (a() == ((x)paramObject).a()) && (g.a(b(), ((x)paramObject).b()));
  }
  
  public int hashCode()
  {
    return (int)(a() ^ a() >>> 32) + b().hashCode();
  }
  
  @ToString
  public String toString()
  {
    return i.a.b().a(this);
  }
}

/* Location:
 * Qualified Name:     org.a.a.a.c
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */