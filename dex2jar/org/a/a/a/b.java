package org.a.a.a;

import org.a.a.d.h;
import org.a.a.w;
import org.joda.convert.ToString;

public abstract class b
  implements w
{
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof w)) {
      return false;
    }
    paramObject = (w)paramObject;
    return a() == ((w)paramObject).a();
  }
  
  public int hashCode()
  {
    long l = a();
    return (int)(l ^ l >>> 32);
  }
  
  @ToString
  public String toString()
  {
    long l = a();
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append("PT");
    int i;
    if (l < 0L) {
      i = 1;
    } else {
      i = 0;
    }
    h.a(localStringBuffer, l);
    for (;;)
    {
      int m = localStringBuffer.length();
      int j;
      if (i != 0) {
        j = 7;
      } else {
        j = 6;
      }
      int k = 3;
      if (m >= j) {
        break;
      }
      if (i != 0) {
        j = k;
      } else {
        j = 2;
      }
      localStringBuffer.insert(j, "0");
    }
    if (l / 1000L * 1000L == l) {
      localStringBuffer.setLength(localStringBuffer.length() - 3);
    } else {
      localStringBuffer.insert(localStringBuffer.length() - 3, ".");
    }
    localStringBuffer.append('S');
    return localStringBuffer.toString();
  }
}

/* Location:
 * Qualified Name:     org.a.a.a.b
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */