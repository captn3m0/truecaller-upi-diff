package org.a.a.a;

import org.a.a.a;
import org.a.a.c;
import org.a.a.c.g;
import org.a.a.d;
import org.a.a.z;

public abstract class e
  implements Comparable<z>, z
{
  private int c(d paramd)
  {
    int i = 0;
    while (i < 3)
    {
      if (b(i) == paramd) {
        return i;
      }
      i += 1;
    }
    return -1;
  }
  
  public int a(d paramd)
  {
    int i = c(paramd);
    if (i != -1) {
      return a(i);
    }
    StringBuilder localStringBuilder = new StringBuilder("Field '");
    localStringBuilder.append(paramd);
    localStringBuilder.append("' is not supported");
    throw new IllegalArgumentException(localStringBuilder.toString());
  }
  
  public int a(z paramz)
  {
    if (this == paramz) {
      return 0;
    }
    int i = 0;
    while (i < 3) {
      if (b(i) == paramz.b(i)) {
        i += 1;
      } else {
        throw new ClassCastException("ReadablePartial objects must have matching field types");
      }
    }
    i = 0;
    while (i < 3)
    {
      if (a(i) > paramz.a(i)) {
        return 1;
      }
      if (a(i) < paramz.a(i)) {
        return -1;
      }
      i += 1;
    }
    return 0;
  }
  
  protected abstract c a(int paramInt, a parama);
  
  public final d b(int paramInt)
  {
    return a(paramInt, b()).a();
  }
  
  public boolean b(d paramd)
  {
    return c(paramd) != -1;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof z)) {
      return false;
    }
    paramObject = (z)paramObject;
    int i = 0;
    while (i < 3) {
      if (a(i) == ((z)paramObject).a(i))
      {
        if (b(i) != ((z)paramObject).b(i)) {
          return false;
        }
        i += 1;
      }
      else
      {
        return false;
      }
    }
    return g.a(b(), ((z)paramObject).b());
  }
  
  public int hashCode()
  {
    int j = 157;
    int i = 0;
    while (i < 3)
    {
      j = (j * 23 + a(i)) * 23 + b(i).hashCode();
      i += 1;
    }
    return j + b().hashCode();
  }
}

/* Location:
 * Qualified Name:     org.a.a.a.e
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */