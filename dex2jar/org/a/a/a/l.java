package org.a.a.a;

import java.io.Serializable;
import org.a.a.aa;
import org.a.a.e;
import org.a.a.i;
import org.a.a.j;
import org.a.a.s;
import org.a.a.x;

public abstract class l
  implements Serializable, Comparable<l>, aa
{
  private static final long serialVersionUID = 9386874258972L;
  protected volatile int k;
  
  protected l(int paramInt)
  {
    k = paramInt;
  }
  
  public static int a(x paramx1, x paramx2, j paramj)
  {
    if ((paramx1 != null) && (paramx2 != null)) {
      return paramj.a(e.b(paramx1)).b(paramx2.a(), paramx1.a());
    }
    throw new IllegalArgumentException("ReadableInstant objects must not be null");
  }
  
  public final int a(j paramj)
  {
    if (paramj == a()) {
      return k;
    }
    return 0;
  }
  
  public abstract j a();
  
  public abstract s b();
  
  public final j c(int paramInt)
  {
    if (paramInt == 0) {
      return a();
    }
    throw new IndexOutOfBoundsException(String.valueOf(paramInt));
  }
  
  public final int d()
  {
    return 1;
  }
  
  public final int d(int paramInt)
  {
    if (paramInt == 0) {
      return k;
    }
    throw new IndexOutOfBoundsException(String.valueOf(paramInt));
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if (!(paramObject instanceof aa)) {
      return false;
    }
    paramObject = (aa)paramObject;
    return (((aa)paramObject).b() == b()) && (((aa)paramObject).d(0) == k);
  }
  
  public int hashCode()
  {
    return (k + 459) * 27 + a().hashCode();
  }
}

/* Location:
 * Qualified Name:     org.a.a.a.l
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */