package org.a.a.a;

import java.io.Serializable;
import org.a.a.b.q;
import org.a.a.e;
import org.a.a.f;
import org.a.a.v;

public abstract class g
  extends a
  implements Serializable, v
{
  private static final long serialVersionUID = -6728882245981L;
  public volatile long a;
  public volatile org.a.a.a b;
  
  public g()
  {
    this(e.a(), q.M());
  }
  
  public g(long paramLong)
  {
    this(paramLong, q.M());
  }
  
  public g(long paramLong, org.a.a.a parama)
  {
    b = e.a(parama);
    parama = b;
    a = paramLong;
    c();
  }
  
  private g(org.a.a.a parama)
  {
    b = e.a(parama);
    long l = b.a(9999, 1, 1, 0, 0, 0, 0);
    parama = b;
    a = l;
    c();
  }
  
  public g(f paramf)
  {
    this(0L, q.b(paramf));
  }
  
  public g(f paramf, byte paramByte)
  {
    this(q.b(paramf));
  }
  
  private void c()
  {
    if ((a == Long.MIN_VALUE) || (a == Long.MAX_VALUE)) {
      b = b.b();
    }
  }
  
  public final long a()
  {
    return a;
  }
  
  protected void a(long paramLong)
  {
    org.a.a.a locala = b;
    a = paramLong;
  }
  
  public final org.a.a.a b()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     org.a.a.a.g
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */