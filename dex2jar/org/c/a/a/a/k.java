package org.c.a.a.a;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class k
{
  private static String a(char paramChar, int paramInt)
  {
    char[] arrayOfChar = new char[paramInt];
    paramInt -= 1;
    while (paramInt >= 0)
    {
      arrayOfChar[paramInt] = paramChar;
      paramInt -= 1;
    }
    return new String(arrayOfChar);
  }
  
  public static String a(Iterable<?> paramIterable, char paramChar)
  {
    if (paramIterable == null) {
      return null;
    }
    return a(paramIterable.iterator(), paramChar);
  }
  
  public static String a(Iterable<?> paramIterable, String paramString)
  {
    if (paramIterable == null) {
      return null;
    }
    return a(paramIterable.iterator(), paramString);
  }
  
  private static String a(String paramString, int paramInt)
  {
    if (paramString == null) {
      return null;
    }
    if (paramInt <= 0) {
      return "";
    }
    int m = paramString.length();
    if (paramInt != 1)
    {
      if (m == 0) {
        return paramString;
      }
      int k = 0;
      if ((m == 1) && (paramInt <= 8192)) {
        return a(paramString.charAt(0), paramInt);
      }
      int n = m * paramInt;
      StringBuilder localStringBuilder;
      switch (m)
      {
      default: 
        localStringBuilder = new StringBuilder(n);
        break;
      case 2: 
        int i = paramString.charAt(0);
        int j = paramString.charAt(1);
        paramString = new char[n];
        for (paramInt = paramInt * 2 - 2; paramInt >= 0; paramInt = paramInt - 1 - 1)
        {
          paramString[paramInt] = i;
          paramString[(paramInt + 1)] = j;
        }
        return new String(paramString);
      case 1: 
        return a(paramString.charAt(0), paramInt);
      }
      while (k < paramInt)
      {
        localStringBuilder.append(paramString);
        k += 1;
      }
      return localStringBuilder.toString();
    }
    return paramString;
  }
  
  public static String a(String paramString, int paramInt, char paramChar)
  {
    if (paramString == null) {
      return null;
    }
    int i = paramInt - paramString.length();
    if (i <= 0) {
      return paramString;
    }
    if (i > 8192) {
      return a(paramString, paramInt, String.valueOf(paramChar));
    }
    return a(paramChar, i).concat(paramString);
  }
  
  private static String a(String paramString1, int paramInt, String paramString2)
  {
    if (paramString1 == null) {
      return null;
    }
    Object localObject = paramString2;
    if (b(paramString2)) {
      localObject = " ";
    }
    int j = ((String)localObject).length();
    int k = paramInt - paramString1.length();
    if (k <= 0) {
      return paramString1;
    }
    int i = 0;
    if ((j == 1) && (k <= 8192)) {
      return a(paramString1, paramInt, ((String)localObject).charAt(0));
    }
    if (k == j) {
      return ((String)localObject).concat(paramString1);
    }
    if (k < j) {
      return ((String)localObject).substring(0, k).concat(paramString1);
    }
    paramString2 = new char[k];
    localObject = ((String)localObject).toCharArray();
    paramInt = i;
    while (paramInt < k)
    {
      paramString2[paramInt] = localObject[(paramInt % j)];
      paramInt += 1;
    }
    return new String(paramString2).concat(paramString1);
  }
  
  public static String a(String paramString1, String paramString2, int paramInt)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString1);
    localStringBuilder.append(paramString2);
    return b(a(localStringBuilder.toString(), paramInt), paramString2);
  }
  
  public static String a(String paramString1, String paramString2, String paramString3)
  {
    if (!b(paramString1))
    {
      if (b(paramString2)) {
        return paramString1;
      }
      int k = paramString3.length();
      int m = paramString1.length();
      StringBuilder localStringBuilder = new StringBuilder(m);
      int i = 0;
      int j = 0;
      while (i < m)
      {
        char c = paramString1.charAt(i);
        int n = paramString2.indexOf(c);
        if (n >= 0)
        {
          if (n < k) {
            localStringBuilder.append(paramString3.charAt(n));
          }
          j = 1;
        }
        else
        {
          localStringBuilder.append(c);
        }
        i += 1;
      }
      if (j != 0) {
        return localStringBuilder.toString();
      }
      return paramString1;
    }
    return paramString1;
  }
  
  private static String a(Iterator<?> paramIterator, char paramChar)
  {
    if (paramIterator == null) {
      return null;
    }
    if (!paramIterator.hasNext()) {
      return "";
    }
    Object localObject = paramIterator.next();
    if (!paramIterator.hasNext()) {
      return h.a(localObject);
    }
    StringBuilder localStringBuilder = new StringBuilder(256);
    if (localObject != null) {
      localStringBuilder.append(localObject);
    }
    while (paramIterator.hasNext())
    {
      localStringBuilder.append(paramChar);
      localObject = paramIterator.next();
      if (localObject != null) {
        localStringBuilder.append(localObject);
      }
    }
    return localStringBuilder.toString();
  }
  
  private static String a(Iterator<?> paramIterator, String paramString)
  {
    if (paramIterator == null) {
      return null;
    }
    if (!paramIterator.hasNext()) {
      return "";
    }
    Object localObject = paramIterator.next();
    if (!paramIterator.hasNext()) {
      return h.a(localObject);
    }
    StringBuilder localStringBuilder = new StringBuilder(256);
    if (localObject != null) {
      localStringBuilder.append(localObject);
    }
    while (paramIterator.hasNext())
    {
      if (paramString != null) {
        localStringBuilder.append(paramString);
      }
      localObject = paramIterator.next();
      if (localObject != null) {
        localStringBuilder.append(localObject);
      }
    }
    return localStringBuilder.toString();
  }
  
  public static String a(int[] paramArrayOfInt, int paramInt)
  {
    if (paramArrayOfInt == null) {
      return null;
    }
    int i = paramInt + 0;
    if (i <= 0) {
      return "";
    }
    StringBuilder localStringBuilder = new StringBuilder(i * 16);
    i = 0;
    while (i < paramInt)
    {
      if (i > 0) {
        localStringBuilder.append(',');
      }
      localStringBuilder.append(paramArrayOfInt[i]);
      i += 1;
    }
    return localStringBuilder.toString();
  }
  
  public static String a(Object[] paramArrayOfObject, String paramString)
  {
    if (paramArrayOfObject == null) {
      return null;
    }
    return a(paramArrayOfObject, paramString, paramArrayOfObject.length);
  }
  
  private static String a(Object[] paramArrayOfObject, String paramString, int paramInt)
  {
    if (paramArrayOfObject == null) {
      return null;
    }
    String str = paramString;
    if (paramString == null) {
      str = "";
    }
    int i = paramInt + 0;
    if (i <= 0) {
      return "";
    }
    paramString = new StringBuilder(i * 16);
    i = 0;
    while (i < paramInt)
    {
      if (i > 0) {
        paramString.append(str);
      }
      if (paramArrayOfObject[i] != null) {
        paramString.append(paramArrayOfObject[i]);
      }
      i += 1;
    }
    return paramString.toString();
  }
  
  public static boolean a(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
  {
    if (paramCharSequence1 == paramCharSequence2) {
      return true;
    }
    if (paramCharSequence1 != null)
    {
      if (paramCharSequence2 == null) {
        return false;
      }
      if (((paramCharSequence1 instanceof String)) && ((paramCharSequence2 instanceof String))) {
        return paramCharSequence1.equals(paramCharSequence2);
      }
      return c.a(paramCharSequence1, false, 0, paramCharSequence2, Math.max(paramCharSequence1.length(), paramCharSequence2.length()));
    }
    return false;
  }
  
  public static boolean a(CharSequence paramCharSequence1, CharSequence paramCharSequence2, boolean paramBoolean)
  {
    if ((paramCharSequence1 != null) && (paramCharSequence2 != null))
    {
      if (paramCharSequence2.length() > paramCharSequence1.length()) {
        return false;
      }
      return c.a(paramCharSequence1, paramBoolean, 0, paramCharSequence2, paramCharSequence2.length());
    }
    return (paramCharSequence1 == null) && (paramCharSequence2 == null);
  }
  
  public static boolean a(CharSequence... paramVarArgs)
  {
    return !c(paramVarArgs);
  }
  
  public static String[] a(String paramString, char paramChar)
  {
    if (paramString == null) {
      return null;
    }
    int n = paramString.length();
    if (n == 0) {
      return a.c;
    }
    ArrayList localArrayList = new ArrayList();
    int i = 0;
    int j = 0;
    int k = 0;
    while (i < n) {
      if (paramString.charAt(i) == paramChar)
      {
        int m = j;
        if (j != 0)
        {
          localArrayList.add(paramString.substring(k, i));
          m = 0;
        }
        k = i + 1;
        i = k;
        j = m;
      }
      else
      {
        i += 1;
        j = 1;
      }
    }
    if (j != 0) {
      localArrayList.add(paramString.substring(k, i));
    }
    return (String[])localArrayList.toArray(new String[localArrayList.size()]);
  }
  
  public static String[] a(String paramString1, String paramString2)
  {
    if (paramString1 == null) {
      return null;
    }
    int m = paramString1.length();
    if (m == 0) {
      return a.c;
    }
    ArrayList localArrayList = new ArrayList();
    int i1;
    int i2;
    int i3;
    if (paramString2.length() == 1)
    {
      int i4 = paramString2.charAt(0);
      i = 0;
      k = 0;
      j = 0;
      n = 1;
      for (;;)
      {
        i1 = i;
        i2 = k;
        i3 = j;
        if (i >= m) {
          break;
        }
        if (paramString1.charAt(i) == i4)
        {
          i3 = i;
          i2 = j;
          i1 = n;
          if (j != 0)
          {
            if (n == -1) {
              i = m;
            }
            localArrayList.add(paramString1.substring(k, i));
            i1 = n + 1;
            i2 = 0;
            i3 = i;
          }
          k = i3 + 1;
          i = k;
          j = i2;
          n = i1;
        }
        else
        {
          i += 1;
          j = 1;
        }
      }
    }
    int i = 0;
    int k = 0;
    int j = 0;
    int n = 1;
    for (;;)
    {
      i1 = i;
      i2 = k;
      i3 = j;
      if (i >= m) {
        break;
      }
      if (paramString2.indexOf(paramString1.charAt(i)) >= 0)
      {
        i3 = i;
        i2 = j;
        i1 = n;
        if (j != 0)
        {
          if (n == -1) {
            i = m;
          }
          localArrayList.add(paramString1.substring(k, i));
          i1 = n + 1;
          i2 = 0;
          i3 = i;
        }
        k = i3 + 1;
        i = k;
        j = i2;
        n = i1;
      }
      else
      {
        i += 1;
        j = 1;
      }
    }
    if (i3 != 0) {
      localArrayList.add(paramString1.substring(i2, i1));
    }
    return (String[])localArrayList.toArray(new String[localArrayList.size()]);
  }
  
  private static String b(String paramString1, String paramString2)
  {
    if (!b(paramString1))
    {
      if (b(paramString2)) {
        return paramString1;
      }
      if (paramString1.endsWith(paramString2)) {
        return paramString1.substring(0, paramString1.length() - paramString2.length());
      }
      return paramString1;
    }
    return paramString1;
  }
  
  public static boolean b(CharSequence paramCharSequence)
  {
    return (paramCharSequence == null) || (paramCharSequence.length() == 0);
  }
  
  public static boolean b(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
  {
    if ((paramCharSequence1 != null) && (paramCharSequence2 != null))
    {
      if (paramCharSequence1 == paramCharSequence2) {
        return true;
      }
      if (paramCharSequence1.length() != paramCharSequence2.length()) {
        return false;
      }
      return c.a(paramCharSequence1, true, 0, paramCharSequence2, paramCharSequence1.length());
    }
    return paramCharSequence1 == paramCharSequence2;
  }
  
  public static boolean b(CharSequence... paramVarArgs)
  {
    if (a.a(paramVarArgs)) {
      return true;
    }
    int i = 0;
    while (i <= 0)
    {
      if (d(paramVarArgs[0])) {
        return true;
      }
      i += 1;
    }
    return false;
  }
  
  public static String c(String paramString, Locale paramLocale)
  {
    if (paramString == null) {
      return null;
    }
    return paramString.toUpperCase(paramLocale);
  }
  
  public static boolean c(CharSequence paramCharSequence)
  {
    return !b(paramCharSequence);
  }
  
  public static boolean c(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
  {
    if (paramCharSequence1 != null)
    {
      if (paramCharSequence2 == null) {
        return false;
      }
      int j = paramCharSequence2.length();
      int k = paramCharSequence1.length();
      int i = 0;
      while (i <= k - j)
      {
        if (c.a(paramCharSequence1, true, i, paramCharSequence2, j)) {
          return true;
        }
        i += 1;
      }
      return false;
    }
    return false;
  }
  
  private static boolean c(CharSequence... paramVarArgs)
  {
    if (a.a(paramVarArgs)) {
      return true;
    }
    int j = paramVarArgs.length;
    int i = 0;
    while (i < j)
    {
      if (b(paramVarArgs[i])) {
        return true;
      }
      i += 1;
    }
    return false;
  }
  
  public static <T extends CharSequence> T d(T paramT1, T paramT2)
  {
    if (d(paramT1)) {
      return paramT2;
    }
    return paramT1;
  }
  
  public static String d(String paramString, Locale paramLocale)
  {
    if (paramString == null) {
      return null;
    }
    return paramString.toLowerCase(paramLocale);
  }
  
  public static boolean d(CharSequence paramCharSequence)
  {
    if (paramCharSequence != null)
    {
      int j = paramCharSequence.length();
      if (j == 0) {
        return true;
      }
      int i = 0;
      while (i < j)
      {
        if (!Character.isWhitespace(paramCharSequence.charAt(i))) {
          return false;
        }
        i += 1;
      }
      return true;
    }
    return true;
  }
  
  public static <T extends CharSequence> T e(T paramT1, T paramT2)
  {
    if (b(paramT1)) {
      return paramT2;
    }
    return paramT1;
  }
  
  public static boolean e(CharSequence paramCharSequence)
  {
    return !d(paramCharSequence);
  }
  
  public static int f(CharSequence paramCharSequence)
  {
    if (paramCharSequence == null) {
      return 0;
    }
    return paramCharSequence.length();
  }
  
  public static boolean f(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
  {
    if (paramCharSequence1 == null) {
      return false;
    }
    if (paramCharSequence2.length() > paramCharSequence1.length()) {
      return false;
    }
    return c.a(paramCharSequence1, false, paramCharSequence1.length() - paramCharSequence2.length(), paramCharSequence2, paramCharSequence2.length());
  }
  
  public static boolean g(CharSequence paramCharSequence)
  {
    if (b(paramCharSequence)) {
      return false;
    }
    int j = paramCharSequence.length();
    int i = 0;
    while (i < j)
    {
      if (!Character.isDigit(paramCharSequence.charAt(i))) {
        return false;
      }
      i += 1;
    }
    return true;
  }
  
  public static boolean h(CharSequence paramCharSequence)
  {
    if (paramCharSequence != null)
    {
      if (b(paramCharSequence)) {
        return false;
      }
      int j = paramCharSequence.length();
      int i = 0;
      while (i < j)
      {
        if (!Character.isLowerCase(paramCharSequence.charAt(i))) {
          return false;
        }
        i += 1;
      }
      return true;
    }
    return false;
  }
  
  public static String i(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return paramString.trim();
  }
  
  public static boolean i(CharSequence paramCharSequence)
  {
    if (paramCharSequence != null)
    {
      if (b(paramCharSequence)) {
        return false;
      }
      int j = paramCharSequence.length();
      int i = 0;
      while (i < j)
      {
        if (!Character.isUpperCase(paramCharSequence.charAt(i))) {
          return false;
        }
        i += 1;
      }
      return true;
    }
    return false;
  }
  
  public static String j(String paramString)
  {
    if (paramString == null) {
      return "";
    }
    return paramString.trim();
  }
  
  public static String k(String paramString)
  {
    if (b(paramString)) {
      return paramString;
    }
    int m = paramString.length();
    char[] arrayOfChar = new char[m];
    int i = 0;
    int k;
    for (int j = 0; i < m; j = k)
    {
      k = j;
      if (!Character.isWhitespace(paramString.charAt(i)))
      {
        arrayOfChar[j] = paramString.charAt(i);
        k = j + 1;
      }
      i += 1;
    }
    if (j == m) {
      return paramString;
    }
    return new String(arrayOfChar, 0, j);
  }
  
  public static String l(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return paramString.toUpperCase();
  }
  
  public static String m(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return paramString.toLowerCase();
  }
  
  public static String n(String paramString)
  {
    String str = paramString;
    if (paramString == null) {
      str = "";
    }
    return str;
  }
}

/* Location:
 * Qualified Name:     org.c.a.a.a.k
 * Java Class Version: 6 (50.0)
 * JD-Core Version:    0.7.1
 */