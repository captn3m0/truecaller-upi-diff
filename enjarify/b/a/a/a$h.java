package b.a.a;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

public final class a$h
  implements a.r
{
  private final File a;
  private final OutputStream b;
  
  public a$h(File paramFile)
  {
    paramFile = File.createTempFile("NanoHTTPD-", "", paramFile);
    a = paramFile;
    paramFile = new java/io/FileOutputStream;
    File localFile = a;
    paramFile.<init>(localFile);
    b = paramFile;
  }
  
  public final void a()
  {
    a.a(b);
    Object localObject1 = a;
    boolean bool = ((File)localObject1).delete();
    if (bool) {
      return;
    }
    localObject1 = new java/lang/Exception;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("could not delete temporary file: ");
    String str = a.getAbsolutePath();
    ((StringBuilder)localObject2).append(str);
    localObject2 = ((StringBuilder)localObject2).toString();
    ((Exception)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
  }
  
  public final String b()
  {
    return a.getAbsolutePath();
  }
}

/* Location:
 * Qualified Name:     b.a.a.a.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */