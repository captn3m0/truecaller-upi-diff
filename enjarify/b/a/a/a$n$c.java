package b.a.a;

public enum a$n$c
  implements a.n.b
{
  private final int G;
  private final String H;
  
  static
  {
    Object localObject = new b/a/a/a$n$c;
    ((c)localObject).<init>("SWITCH_PROTOCOL", 0, 101, "Switching Protocols");
    a = (c)localObject;
    localObject = new b/a/a/a$n$c;
    int i1 = 1;
    ((c)localObject).<init>("OK", i1, 200, "OK");
    b = (c)localObject;
    localObject = new b/a/a/a$n$c;
    int i2 = 2;
    ((c)localObject).<init>("CREATED", i2, 201, "Created");
    c = (c)localObject;
    localObject = new b/a/a/a$n$c;
    int i3 = 3;
    ((c)localObject).<init>("ACCEPTED", i3, 202, "Accepted");
    d = (c)localObject;
    localObject = new b/a/a/a$n$c;
    int i4 = 4;
    ((c)localObject).<init>("NO_CONTENT", i4, 204, "No Content");
    e = (c)localObject;
    localObject = new b/a/a/a$n$c;
    int i5 = 5;
    ((c)localObject).<init>("PARTIAL_CONTENT", i5, 206, "Partial Content");
    f = (c)localObject;
    localObject = new b/a/a/a$n$c;
    int i6 = 6;
    ((c)localObject).<init>("MULTI_STATUS", i6, 207, "Multi-Status");
    g = (c)localObject;
    localObject = new b/a/a/a$n$c;
    int i7 = 7;
    ((c)localObject).<init>("REDIRECT", i7, 301, "Moved Permanently");
    h = (c)localObject;
    localObject = new b/a/a/a$n$c;
    int i8 = 8;
    ((c)localObject).<init>("FOUND", i8, 302, "Found");
    i = (c)localObject;
    localObject = new b/a/a/a$n$c;
    int i9 = 9;
    ((c)localObject).<init>("REDIRECT_SEE_OTHER", i9, 303, "See Other");
    j = (c)localObject;
    localObject = new b/a/a/a$n$c;
    int i10 = 10;
    ((c)localObject).<init>("NOT_MODIFIED", i10, 304, "Not Modified");
    k = (c)localObject;
    localObject = new b/a/a/a$n$c;
    ((c)localObject).<init>("TEMPORARY_REDIRECT", 11, 307, "Temporary Redirect");
    l = (c)localObject;
    localObject = new b/a/a/a$n$c;
    ((c)localObject).<init>("BAD_REQUEST", 12, 400, "Bad Request");
    m = (c)localObject;
    localObject = new b/a/a/a$n$c;
    ((c)localObject).<init>("UNAUTHORIZED", 13, 401, "Unauthorized");
    n = (c)localObject;
    localObject = new b/a/a/a$n$c;
    ((c)localObject).<init>("FORBIDDEN", 14, 403, "Forbidden");
    o = (c)localObject;
    localObject = new b/a/a/a$n$c;
    ((c)localObject).<init>("NOT_FOUND", 15, 404, "Not Found");
    p = (c)localObject;
    localObject = new b/a/a/a$n$c;
    ((c)localObject).<init>("METHOD_NOT_ALLOWED", 16, 405, "Method Not Allowed");
    q = (c)localObject;
    localObject = new b/a/a/a$n$c;
    ((c)localObject).<init>("NOT_ACCEPTABLE", 17, 406, "Not Acceptable");
    r = (c)localObject;
    localObject = new b/a/a/a$n$c;
    ((c)localObject).<init>("REQUEST_TIMEOUT", 18, 408, "Request Timeout");
    s = (c)localObject;
    localObject = new b/a/a/a$n$c;
    ((c)localObject).<init>("CONFLICT", 19, 409, "Conflict");
    t = (c)localObject;
    localObject = new b/a/a/a$n$c;
    ((c)localObject).<init>("GONE", 20, 410, "Gone");
    u = (c)localObject;
    localObject = new b/a/a/a$n$c;
    ((c)localObject).<init>("LENGTH_REQUIRED", 21, 411, "Length Required");
    v = (c)localObject;
    localObject = new b/a/a/a$n$c;
    ((c)localObject).<init>("PRECONDITION_FAILED", 22, 412, "Precondition Failed");
    w = (c)localObject;
    localObject = new b/a/a/a$n$c;
    ((c)localObject).<init>("PAYLOAD_TOO_LARGE", 23, 413, "Payload Too Large");
    x = (c)localObject;
    localObject = new b/a/a/a$n$c;
    ((c)localObject).<init>("UNSUPPORTED_MEDIA_TYPE", 24, 415, "Unsupported Media Type");
    y = (c)localObject;
    localObject = new b/a/a/a$n$c;
    ((c)localObject).<init>("RANGE_NOT_SATISFIABLE", 25, 416, "Requested Range Not Satisfiable");
    z = (c)localObject;
    localObject = new b/a/a/a$n$c;
    ((c)localObject).<init>("EXPECTATION_FAILED", 26, 417, "Expectation Failed");
    A = (c)localObject;
    localObject = new b/a/a/a$n$c;
    ((c)localObject).<init>("TOO_MANY_REQUESTS", 27, 429, "Too Many Requests");
    B = (c)localObject;
    localObject = new b/a/a/a$n$c;
    ((c)localObject).<init>("INTERNAL_ERROR", 28, 500, "Internal Server Error");
    C = (c)localObject;
    localObject = new b/a/a/a$n$c;
    ((c)localObject).<init>("NOT_IMPLEMENTED", 29, 501, "Not Implemented");
    D = (c)localObject;
    localObject = new b/a/a/a$n$c;
    ((c)localObject).<init>("SERVICE_UNAVAILABLE", 30, 503, "Service Unavailable");
    E = (c)localObject;
    localObject = new b/a/a/a$n$c;
    ((c)localObject).<init>("UNSUPPORTED_HTTP_VERSION", 31, 505, "HTTP Version Not Supported");
    F = (c)localObject;
    localObject = new c[32];
    c localc = a;
    localObject[0] = localc;
    localc = b;
    localObject[i1] = localc;
    localc = c;
    localObject[i2] = localc;
    localc = d;
    localObject[i3] = localc;
    localc = e;
    localObject[i4] = localc;
    localc = f;
    localObject[i5] = localc;
    localc = g;
    localObject[i6] = localc;
    localc = h;
    localObject[i7] = localc;
    localc = i;
    localObject[i8] = localc;
    localc = j;
    localObject[i9] = localc;
    localc = k;
    localObject[i10] = localc;
    localc = l;
    localObject[11] = localc;
    localc = m;
    localObject[12] = localc;
    localc = n;
    localObject[13] = localc;
    localc = o;
    localObject[14] = localc;
    localc = p;
    localObject[15] = localc;
    localc = q;
    localObject[16] = localc;
    localc = r;
    localObject[17] = localc;
    localc = s;
    localObject[18] = localc;
    localc = t;
    localObject[19] = localc;
    localc = u;
    localObject[20] = localc;
    localc = v;
    localObject[21] = localc;
    localc = w;
    localObject[22] = localc;
    localc = x;
    localObject[23] = localc;
    localc = y;
    localObject[24] = localc;
    localc = z;
    localObject[25] = localc;
    localc = A;
    localObject[26] = localc;
    localc = B;
    localObject[27] = localc;
    localc = C;
    localObject[28] = localc;
    localc = D;
    localObject[29] = localc;
    localc = E;
    localObject[30] = localc;
    localc = F;
    localObject[31] = localc;
    I = (c[])localObject;
  }
  
  private a$n$c(int paramInt1, String paramString1)
  {
    G = paramInt1;
    H = paramString1;
  }
  
  public final String a()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    int i1 = G;
    localStringBuilder.append(i1);
    localStringBuilder.append(" ");
    String str = H;
    localStringBuilder.append(str);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     b.a.a.a.n.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */