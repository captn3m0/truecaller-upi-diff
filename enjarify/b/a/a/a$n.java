package b.a.a;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPOutputStream;

public final class a$n
  implements Closeable
{
  String a;
  a.m b;
  boolean c;
  boolean d;
  private a.n.b e;
  private InputStream f;
  private long g;
  private final Map h;
  private final Map i;
  private boolean j;
  
  protected a$n(a.n.b paramb, String paramString, InputStream paramInputStream, long paramLong)
  {
    Object localObject = new b/a/a/a$n$1;
    ((a.n.1)localObject).<init>(this);
    h = ((Map)localObject);
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    i = ((Map)localObject);
    e = paramb;
    a = paramString;
    long l1 = 0L;
    boolean bool1 = false;
    localObject = null;
    if (paramInputStream == null)
    {
      paramInputStream = new java/io/ByteArrayInputStream;
      byte[] arrayOfByte = new byte[0];
      paramInputStream.<init>(arrayOfByte);
      f = paramInputStream;
      g = l1;
    }
    else
    {
      f = paramInputStream;
      g = paramLong;
    }
    long l2 = g;
    boolean bool2 = true;
    boolean bool3 = l2 < l1;
    if (bool3) {
      bool1 = true;
    }
    j = bool1;
    d = bool2;
  }
  
  private long a(PrintWriter paramPrintWriter, long paramLong)
  {
    Object localObject = a("content-length");
    if (localObject != null) {
      try
      {
        paramLong = Long.parseLong((String)localObject);
      }
      catch (NumberFormatException localNumberFormatException)
      {
        Logger localLogger = a.c();
        String str = "content-length was no number ";
        localObject = String.valueOf(localObject);
        localObject = str.concat((String)localObject);
        localLogger.severe((String)localObject);
      }
    }
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("Content-Length: ");
    ((StringBuilder)localObject).append(paramLong);
    ((StringBuilder)localObject).append("\r\n");
    localObject = ((StringBuilder)localObject).toString();
    paramPrintWriter.print((String)localObject);
    return paramLong;
  }
  
  private void a(OutputStream paramOutputStream, long paramLong)
  {
    boolean bool = c;
    if (bool)
    {
      GZIPOutputStream localGZIPOutputStream = new java/util/zip/GZIPOutputStream;
      localGZIPOutputStream.<init>(paramOutputStream);
      b(localGZIPOutputStream, -1);
      localGZIPOutputStream.finish();
      return;
    }
    b(paramOutputStream, paramLong);
  }
  
  private static void a(PrintWriter paramPrintWriter, String paramString1, String paramString2)
  {
    paramPrintWriter.append(paramString1).append(": ").append(paramString2).append("\r\n");
  }
  
  private void b(OutputStream paramOutputStream, long paramLong)
  {
    int k = 16384;
    byte[] arrayOfByte = new byte[k];
    long l1 = -1;
    boolean bool1 = paramLong < l1;
    int n;
    if (!bool1) {
      n = 1;
    } else {
      n = 0;
    }
    for (;;)
    {
      long l2 = 0L;
      boolean bool2 = paramLong < l2;
      if ((!bool2) && (n == 0)) {
        break;
      }
      l2 = 16384L;
      if (n == 0) {
        l2 = Math.min(paramLong, l2);
      }
      InputStream localInputStream = f;
      int m = (int)l2;
      int i1 = localInputStream.read(arrayOfByte, 0, m);
      if (i1 <= 0) {
        break;
      }
      paramOutputStream.write(arrayOfByte, 0, i1);
      if (n == 0)
      {
        l2 = i1;
        paramLong -= l2;
      }
    }
  }
  
  public final String a(String paramString)
  {
    Map localMap = i;
    paramString = paramString.toLowerCase();
    return (String)localMap.get(paramString);
  }
  
  protected final void a(OutputStream paramOutputStream)
  {
    Object localObject1 = new java/text/SimpleDateFormat;
    Object localObject2 = Locale.US;
    ((SimpleDateFormat)localObject1).<init>("E, d MMM yyyy HH:mm:ss 'GMT'", (Locale)localObject2);
    Object localObject3 = TimeZone.getTimeZone("GMT");
    ((SimpleDateFormat)localObject1).setTimeZone((TimeZone)localObject3);
    try
    {
      localObject3 = e;
      if (localObject3 != null)
      {
        localObject3 = new java/io/PrintWriter;
        localObject2 = new java/io/BufferedWriter;
        OutputStreamWriter localOutputStreamWriter = new java/io/OutputStreamWriter;
        Object localObject4 = new b/a/a/a$c;
        String str = a;
        ((a.c)localObject4).<init>(str);
        localObject4 = ((a.c)localObject4).a();
        localOutputStreamWriter.<init>(paramOutputStream, (String)localObject4);
        ((BufferedWriter)localObject2).<init>(localOutputStreamWriter);
        localOutputStreamWriter = null;
        ((PrintWriter)localObject3).<init>((Writer)localObject2, false);
        localObject2 = "HTTP/1.1 ";
        localObject2 = ((PrintWriter)localObject3).append((CharSequence)localObject2);
        localObject4 = e;
        localObject4 = ((a.n.b)localObject4).a();
        localObject2 = ((PrintWriter)localObject2).append((CharSequence)localObject4);
        localObject4 = " \r\n";
        ((PrintWriter)localObject2).append((CharSequence)localObject4);
        localObject2 = a;
        if (localObject2 != null)
        {
          localObject2 = "Content-Type";
          localObject4 = a;
          a((PrintWriter)localObject3, (String)localObject2, (String)localObject4);
        }
        localObject2 = "date";
        localObject2 = a((String)localObject2);
        if (localObject2 == null)
        {
          localObject2 = "Date";
          localObject4 = new java/util/Date;
          ((Date)localObject4).<init>();
          localObject1 = ((SimpleDateFormat)localObject1).format((Date)localObject4);
          a((PrintWriter)localObject3, (String)localObject2, (String)localObject1);
        }
        localObject1 = h;
        localObject1 = ((Map)localObject1).entrySet();
        localObject1 = ((Set)localObject1).iterator();
        boolean bool1;
        for (;;)
        {
          bool1 = ((Iterator)localObject1).hasNext();
          if (!bool1) {
            break;
          }
          localObject2 = ((Iterator)localObject1).next();
          localObject2 = (Map.Entry)localObject2;
          localObject4 = ((Map.Entry)localObject2).getKey();
          localObject4 = (String)localObject4;
          localObject2 = ((Map.Entry)localObject2).getValue();
          localObject2 = (String)localObject2;
          a((PrintWriter)localObject3, (String)localObject4, (String)localObject2);
        }
        localObject1 = "connection";
        localObject1 = a((String)localObject1);
        if (localObject1 == null)
        {
          localObject1 = "Connection";
          bool1 = d;
          if (bool1) {
            localObject2 = "keep-alive";
          } else {
            localObject2 = "close";
          }
          a((PrintWriter)localObject3, (String)localObject1, (String)localObject2);
        }
        localObject1 = "content-length";
        localObject1 = a((String)localObject1);
        if (localObject1 != null) {
          c = false;
        }
        boolean bool2 = c;
        if (bool2)
        {
          localObject1 = "Content-Encoding";
          localObject2 = "gzip";
          a((PrintWriter)localObject3, (String)localObject1, (String)localObject2);
          bool2 = true;
          j = bool2;
        }
        localObject1 = f;
        long l1;
        if (localObject1 != null) {
          l1 = g;
        } else {
          l1 = 0L;
        }
        localObject1 = b;
        localObject4 = a.m.e;
        if (localObject1 != localObject4)
        {
          bool2 = j;
          if (bool2)
          {
            localObject1 = "Transfer-Encoding";
            localObject4 = "chunked";
            a((PrintWriter)localObject3, (String)localObject1, (String)localObject4);
            break label482;
          }
        }
        bool2 = c;
        if (!bool2) {
          l1 = a((PrintWriter)localObject3, l1);
        }
        label482:
        localObject1 = "\r\n";
        ((PrintWriter)localObject3).append((CharSequence)localObject1);
        ((PrintWriter)localObject3).flush();
        localObject1 = b;
        localObject3 = a.m.e;
        if (localObject1 != localObject3)
        {
          bool2 = j;
          if (bool2)
          {
            localObject1 = new b/a/a/a$n$a;
            ((a.n.a)localObject1).<init>(paramOutputStream);
            long l2 = -1;
            a((OutputStream)localObject1, l2);
            ((a.n.a)localObject1).a();
            break label558;
          }
        }
        a(paramOutputStream, l1);
        label558:
        paramOutputStream.flush();
        paramOutputStream = f;
        a.a(paramOutputStream);
        return;
      }
      paramOutputStream = new java/lang/Error;
      localObject1 = "sendResponse(): Status can't be null.";
      paramOutputStream.<init>((String)localObject1);
      throw paramOutputStream;
    }
    catch (IOException paramOutputStream)
    {
      localObject1 = a.c();
      localObject3 = Level.SEVERE;
      ((Logger)localObject1).log((Level)localObject3, "Could not send response to the client", paramOutputStream);
    }
  }
  
  public final void a(String paramString1, String paramString2)
  {
    h.put(paramString1, paramString2);
  }
  
  public final void close()
  {
    InputStream localInputStream = f;
    if (localInputStream != null) {
      localInputStream.close();
    }
  }
}

/* Location:
 * Qualified Name:     b.a.a.a.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */