package b.a.a;

import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public abstract class a
{
  private static final Pattern d;
  private static final Pattern e;
  private static final Pattern f = Pattern.compile("[ |\t]*([a-zA-Z]*)[ |\t]*=[ |\t]*['|\"]([^\"^']*)['|\"]");
  private static final Logger g = Logger.getLogger(a.class.getName());
  protected volatile ServerSocket a;
  protected Thread b;
  protected a.a c;
  private final String h;
  private final int i;
  private a.q j;
  private a.t k;
  
  static
  {
    int m = 2;
    d = Pattern.compile("([ |\t]*Content-Disposition[ |\t]*:)(.*)", m);
    e = Pattern.compile("([ |\t]*content-type[ |\t]*:)(.*)", m);
  }
  
  public a()
  {
    this((byte)0);
  }
  
  private a(byte paramByte)
  {
    Object localObject = new b/a/a/a$g;
    ((a.g)localObject).<init>();
    j = ((a.q)localObject);
    h = null;
    i = 8080;
    localObject = new b/a/a/a$j;
    ((a.j)localObject).<init>(this, (byte)0);
    k = ((a.t)localObject);
    localObject = new b/a/a/a$f;
    ((a.f)localObject).<init>();
    c = ((a.a)localObject);
  }
  
  public static a.n a(a.n.b paramb, String paramString, InputStream paramInputStream, long paramLong)
  {
    a.n localn = new b/a/a/a$n;
    localn.<init>(paramb, paramString, paramInputStream, paramLong);
    return localn;
  }
  
  public static a.n a(a.n.b paramb, String paramString1, String paramString2)
  {
    Object localObject1 = new b/a/a/a$c;
    ((a.c)localObject1).<init>(paramString1);
    if (paramString2 == null)
    {
      paramString2 = new java/io/ByteArrayInputStream;
      localObject1 = new byte[0];
      paramString2.<init>((byte[])localObject1);
      return a(paramb, paramString1, paramString2, 0L);
    }
    try
    {
      paramString1 = ((a.c)localObject1).a();
      paramString1 = Charset.forName(paramString1);
      paramString1 = paramString1.newEncoder();
      boolean bool = paramString1.canEncode(paramString2);
      if (!bool)
      {
        paramString1 = c;
        if (paramString1 == null)
        {
          paramString1 = new b/a/a/a$c;
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>();
          str = a;
          ((StringBuilder)localObject2).append(str);
          str = "; charset=UTF-8";
          ((StringBuilder)localObject2).append(str);
          localObject2 = ((StringBuilder)localObject2).toString();
          paramString1.<init>((String)localObject2);
          localObject1 = paramString1;
        }
      }
      paramString1 = ((a.c)localObject1).a();
      paramString1 = paramString2.getBytes(paramString1);
    }
    catch (UnsupportedEncodingException paramString1)
    {
      paramString2 = g;
      Object localObject2 = Level.SEVERE;
      String str = "encoding problem, responding nothing";
      paramString2.log((Level)localObject2, str, paramString1);
      paramString1 = new byte[0];
    }
    paramString2 = a;
    localObject1 = new java/io/ByteArrayInputStream;
    ((ByteArrayInputStream)localObject1).<init>(paramString1);
    long l = paramString1.length;
    return a(paramb, paramString2, (InputStream)localObject1, l);
  }
  
  protected static String a(String paramString)
  {
    Object localObject = "UTF8";
    try
    {
      paramString = URLDecoder.decode(paramString, (String)localObject);
    }
    catch (UnsupportedEncodingException paramString)
    {
      localObject = g;
      Level localLevel = Level.WARNING;
      String str = "Encoding not supported, ignored";
      ((Logger)localObject).log(localLevel, str, paramString);
      paramString = null;
    }
    return paramString;
  }
  
  protected static boolean a(a.n paramn)
  {
    String str1 = a;
    if (str1 != null)
    {
      str1 = a.toLowerCase();
      String str2 = "text/";
      boolean bool1 = str1.contains(str2);
      if (!bool1)
      {
        paramn = a.toLowerCase();
        str1 = "/json";
        boolean bool2 = paramn.contains(str1);
        if (!bool2) {}
      }
      else
      {
        return true;
      }
    }
    return false;
  }
  
  public static a.n b(String paramString)
  {
    return a(a.n.c.b, "text/html", paramString);
  }
  
  private static final void b(Object paramObject)
  {
    if (paramObject != null)
    {
      boolean bool = paramObject instanceof Closeable;
      if (bool) {}
      try
      {
        paramObject = (Closeable)paramObject;
        ((Closeable)paramObject).close();
        return;
      }
      catch (IOException paramObject)
      {
        Object localObject = g;
        Level localLevel = Level.SEVERE;
        ((Logger)localObject).log(localLevel, "Could not close", (Throwable)paramObject);
        return;
      }
      bool = paramObject instanceof Socket;
      if (bool)
      {
        paramObject = (Socket)paramObject;
        ((Socket)paramObject).close();
        return;
      }
      bool = paramObject instanceof ServerSocket;
      if (bool)
      {
        paramObject = (ServerSocket)paramObject;
        ((ServerSocket)paramObject).close();
        return;
      }
      paramObject = new java/lang/IllegalArgumentException;
      localObject = "Unknown object to close";
      ((IllegalArgumentException)paramObject).<init>((String)localObject);
      throw ((Throwable)paramObject);
    }
  }
  
  public a.n a(a.l paraml)
  {
    Object localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    a.m localm = paraml.b();
    Object localObject2 = a.m.b;
    boolean bool1 = ((a.m)localObject2).equals(localm);
    if (!bool1)
    {
      localObject2 = a.m.c;
      boolean bool2 = ((a.m)localObject2).equals(localm);
      if (!bool2) {
        break label58;
      }
    }
    try
    {
      paraml.a((Map)localObject1);
      label58:
      localObject1 = paraml.c();
      paraml = paraml.d();
      ((Map)localObject1).put("NanoHttpd.QUERY_STRING", paraml);
      return a(a.n.c.p, "text/plain", "Not Found");
    }
    catch (a.o paraml)
    {
      localObject1 = a;
      paraml = paraml.getMessage();
      return a((a.n.b)localObject1, "text/plain", paraml);
    }
    catch (IOException paraml)
    {
      localObject1 = a.n.c.C;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("SERVER INTERNAL ERROR: IOException: ");
      paraml = paraml.getMessage();
      ((StringBuilder)localObject2).append(paraml);
      paraml = ((StringBuilder)localObject2).toString();
    }
    return a((a.n.b)localObject1, "text/plain", paraml);
  }
  
  public void a()
  {
    Object localObject1 = j.a();
    a = ((ServerSocket)localObject1);
    localObject1 = a;
    boolean bool = true;
    ((ServerSocket)localObject1).setReuseAddress(bool);
    localObject1 = new b/a/a/a$p;
    ((a.p)localObject1).<init>(this);
    Object localObject2 = new java/lang/Thread;
    ((Thread)localObject2).<init>((Runnable)localObject1);
    b = ((Thread)localObject2);
    b.setDaemon(bool);
    Object localObject3 = b;
    localObject2 = "NanoHttpd Main Listener";
    ((Thread)localObject3).setName((String)localObject2);
    localObject3 = b;
    ((Thread)localObject3).start();
    for (;;)
    {
      bool = a.p.a((a.p)localObject1);
      long l;
      if (!bool)
      {
        localObject3 = a.p.b((a.p)localObject1);
        if (localObject3 == null) {
          l = 10;
        }
      }
      try
      {
        Thread.sleep(l);
      }
      finally
      {
        continue;
        localObject3 = a.p.b((a.p)localObject1);
        if (localObject3 == null) {
          return;
        }
        throw a.p.b((a.p)localObject1);
      }
    }
  }
  
  public final void b()
  {
    try
    {
      Object localObject = a;
      b(localObject);
      localObject = c;
      ((a.a)localObject).a();
      localObject = b;
      if (localObject != null)
      {
        localObject = b;
        ((Thread)localObject).join();
      }
      return;
    }
    catch (Exception localException)
    {
      Logger localLogger = g;
      Level localLevel = Level.SEVERE;
      localLogger.log(localLevel, "Could not stop all connections", localException);
    }
  }
}

/* Location:
 * Qualified Name:     b.a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */