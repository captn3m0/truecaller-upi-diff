package b.a.a;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class a$p
  implements Runnable
{
  private final int b = 5000;
  private IOException c;
  private boolean d = false;
  
  public a$p(a parama) {}
  
  public final void run()
  {
    try
    {
      Object localObject1 = a;
      localObject1 = a.d((a)localObject1);
      Object localObject2 = a;
      localObject2 = a.b((a)localObject2);
      Object localObject3;
      Object localObject4;
      if (localObject2 != null)
      {
        localObject2 = new java/net/InetSocketAddress;
        localObject3 = a;
        localObject3 = a.b((a)localObject3);
        localObject4 = a;
        int i = a.c((a)localObject4);
        ((InetSocketAddress)localObject2).<init>((String)localObject3, i);
      }
      else
      {
        localObject2 = new java/net/InetSocketAddress;
        localObject3 = a;
        int j = a.c((a)localObject3);
        ((InetSocketAddress)localObject2).<init>(j);
      }
      ((ServerSocket)localObject1).bind((SocketAddress)localObject2);
      boolean bool = true;
      d = bool;
      do
      {
        try
        {
          localObject1 = a;
          localObject1 = a.d((a)localObject1);
          localObject1 = ((ServerSocket)localObject1).accept();
          int k = b;
          if (k > 0)
          {
            k = b;
            ((Socket)localObject1).setSoTimeout(k);
          }
          localObject2 = ((Socket)localObject1).getInputStream();
          localObject3 = a;
          localObject3 = c;
          localObject4 = a;
          a.b localb = new b/a/a/a$b;
          localb.<init>((a)localObject4, (InputStream)localObject2, (Socket)localObject1);
          ((a.a)localObject3).b(localb);
        }
        catch (IOException localIOException1)
        {
          localObject2 = a.c();
          localObject3 = Level.FINE;
          localObject4 = "Communication with the client broken";
          ((Logger)localObject2).log((Level)localObject3, (String)localObject4, localIOException1);
        }
        ServerSocket localServerSocket = a.d(a);
        bool = localServerSocket.isClosed();
      } while (!bool);
      return;
    }
    catch (IOException localIOException2)
    {
      c = localIOException2;
    }
  }
}

/* Location:
 * Qualified Name:     b.a.a.a.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */