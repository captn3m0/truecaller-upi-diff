package b.a.a;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class a$c
{
  private static final Pattern e;
  private static final Pattern f;
  private static final Pattern g;
  final String a;
  final String b;
  final String c;
  final String d;
  
  static
  {
    int i = 2;
    e = Pattern.compile("[ |\t]*([^/^ ^;^,]+/[^ ^;^,]+)", i);
    f = Pattern.compile("[ |\t]*(charset)[ |\t]*=[ |\t]*['|\"]?([^\"^'^;^,]*)['|\"]?", i);
    g = Pattern.compile("[ |\t]*(boundary)[ |\t]*=[ |\t]*['|\"]?([^\"^'^;^,]*)['|\"]?", i);
  }
  
  public a$c(String paramString)
  {
    a = paramString;
    int i = 2;
    if (paramString != null)
    {
      localObject = e;
      str = "";
      int j = 1;
      localObject = a(paramString, (Pattern)localObject, str, j);
      b = ((String)localObject);
      localObject = f;
      localObject = a(paramString, (Pattern)localObject, null, i);
      c = ((String)localObject);
    }
    else
    {
      b = "";
      localObject = "UTF-8";
      c = ((String)localObject);
    }
    Object localObject = "multipart/form-data";
    String str = b;
    boolean bool = ((String)localObject).equalsIgnoreCase(str);
    if (bool)
    {
      localObject = g;
      paramString = a(paramString, (Pattern)localObject, null, i);
      d = paramString;
      return;
    }
    d = null;
  }
  
  private static String a(String paramString1, Pattern paramPattern, String paramString2, int paramInt)
  {
    paramString1 = paramPattern.matcher(paramString1);
    boolean bool = paramString1.find();
    if (bool) {
      return paramString1.group(paramInt);
    }
    return paramString2;
  }
  
  public final String a()
  {
    String str = c;
    if (str == null) {
      str = "US-ASCII";
    }
    return str;
  }
}

/* Location:
 * Qualified Name:     b.a.a.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */