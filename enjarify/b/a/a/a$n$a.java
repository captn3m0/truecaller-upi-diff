package b.a.a;

import java.io.FilterOutputStream;
import java.io.OutputStream;

final class a$n$a
  extends FilterOutputStream
{
  public a$n$a(OutputStream paramOutputStream)
  {
    super(paramOutputStream);
  }
  
  public final void a()
  {
    OutputStream localOutputStream = out;
    byte[] arrayOfByte = "0\r\n\r\n".getBytes();
    localOutputStream.write(arrayOfByte);
  }
  
  public final void write(int paramInt)
  {
    int i = 1;
    byte[] arrayOfByte = new byte[i];
    paramInt = (byte)paramInt;
    arrayOfByte[0] = paramInt;
    write(arrayOfByte, 0, i);
  }
  
  public final void write(byte[] paramArrayOfByte)
  {
    int i = paramArrayOfByte.length;
    write(paramArrayOfByte, 0, i);
  }
  
  public final void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    if (paramInt2 == 0) {
      return;
    }
    OutputStream localOutputStream = out;
    Object[] arrayOfObject = new Object[1];
    Integer localInteger = Integer.valueOf(paramInt2);
    arrayOfObject[0] = localInteger;
    byte[] arrayOfByte1 = String.format("%x\r\n", arrayOfObject).getBytes();
    localOutputStream.write(arrayOfByte1);
    out.write(paramArrayOfByte, paramInt1, paramInt2);
    paramArrayOfByte = out;
    byte[] arrayOfByte2 = "\r\n".getBytes();
    paramArrayOfByte.write(arrayOfByte2);
  }
}

/* Location:
 * Qualified Name:     b.a.a.a.n.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */