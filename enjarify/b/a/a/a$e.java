package b.a.a;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public final class a$e
  implements Iterable
{
  private final HashMap b;
  private final ArrayList c;
  
  public a$e(a parama, Map paramMap)
  {
    parama = new java/util/HashMap;
    parama.<init>();
    b = parama;
    parama = new java/util/ArrayList;
    parama.<init>();
    c = parama;
    parama = (String)paramMap.get("cookie");
    if (parama != null)
    {
      paramMap = ";";
      parama = parama.split(paramMap);
      int i = parama.length;
      int j = 0;
      while (j < i)
      {
        Object localObject1 = parama[j].trim();
        Object localObject2 = "=";
        localObject1 = ((String)localObject1).split((String)localObject2);
        int k = localObject1.length;
        int m = 2;
        if (k == m)
        {
          localObject2 = b;
          Object localObject3 = localObject1[0];
          int n = 1;
          localObject1 = localObject1[n];
          ((HashMap)localObject2).put(localObject3, localObject1);
        }
        j += 1;
      }
    }
  }
  
  public final void a(a.n paramn)
  {
    Iterator localIterator = c.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      Object localObject = (a.d)localIterator.next();
      String str1 = "Set-Cookie";
      String str2 = "%s=%s; expires=%s";
      int i = 3;
      Object[] arrayOfObject = new Object[i];
      String str3 = a;
      arrayOfObject[0] = str3;
      str3 = b;
      arrayOfObject[1] = str3;
      int j = 2;
      localObject = c;
      arrayOfObject[j] = localObject;
      localObject = String.format(str2, arrayOfObject);
      paramn.a(str1, (String)localObject);
    }
  }
  
  public final Iterator iterator()
  {
    return b.keySet().iterator();
  }
}

/* Location:
 * Qualified Name:     b.a.a.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */