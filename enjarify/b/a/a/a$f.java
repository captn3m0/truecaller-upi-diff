package b.a.a;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class a$f
  implements a.a
{
  private long a;
  private final List b;
  
  public a$f()
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    localObject = Collections.synchronizedList((List)localObject);
    b = ((List)localObject);
  }
  
  public final void a()
  {
    Object localObject1 = new java/util/ArrayList;
    Object localObject2 = b;
    ((ArrayList)localObject1).<init>((Collection)localObject2);
    localObject1 = ((ArrayList)localObject1).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (a.b)((Iterator)localObject1).next();
      InputStream localInputStream = a;
      a.a(localInputStream);
      localObject2 = b;
      a.a(localObject2);
    }
  }
  
  public final void a(a.b paramb)
  {
    b.remove(paramb);
  }
  
  public final void b(a.b paramb)
  {
    long l1 = a + 1L;
    a = l1;
    Thread localThread = new java/lang/Thread;
    localThread.<init>(paramb);
    localThread.setDaemon(true);
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("NanoHttpd Request Processor (#");
    long l2 = a;
    ((StringBuilder)localObject).append(l2);
    ((StringBuilder)localObject).append(")");
    localObject = ((StringBuilder)localObject).toString();
    localThread.setName((String)localObject);
    b.add(paramb);
    localThread.start();
  }
}

/* Location:
 * Qualified Name:     b.a.a.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */