package b.a.a;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class a$i
  implements a.s
{
  private final File a;
  private final List b;
  
  public a$i()
  {
    Object localObject = new java/io/File;
    String str = System.getProperty("java.io.tmpdir");
    ((File)localObject).<init>(str);
    a = ((File)localObject);
    localObject = a;
    boolean bool = ((File)localObject).exists();
    if (!bool)
    {
      localObject = a;
      ((File)localObject).mkdirs();
    }
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    b = ((List)localObject);
  }
  
  public final void a()
  {
    Iterator localIterator = b.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      a.r localr = (a.r)localIterator.next();
      try
      {
        localr.a();
      }
      catch (Exception localException)
      {
        Logger localLogger = a.c();
        Level localLevel = Level.WARNING;
        String str = "could not delete file ";
        localLogger.log(localLevel, str, localException);
      }
    }
    b.clear();
  }
  
  public final a.r b()
  {
    a.h localh = new b/a/a/a$h;
    File localFile = a;
    localh.<init>(localFile);
    b.add(localh);
    return localh;
  }
}

/* Location:
 * Qualified Name:     b.a.a.a.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */