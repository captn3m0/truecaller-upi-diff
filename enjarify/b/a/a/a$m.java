package b.a.a;

public enum a$m
{
  static
  {
    Object localObject = new b/a/a/a$m;
    ((m)localObject).<init>("GET", 0);
    a = (m)localObject;
    localObject = new b/a/a/a$m;
    int i1 = 1;
    ((m)localObject).<init>("PUT", i1);
    b = (m)localObject;
    localObject = new b/a/a/a$m;
    int i2 = 2;
    ((m)localObject).<init>("POST", i2);
    c = (m)localObject;
    localObject = new b/a/a/a$m;
    int i3 = 3;
    ((m)localObject).<init>("DELETE", i3);
    d = (m)localObject;
    localObject = new b/a/a/a$m;
    int i4 = 4;
    ((m)localObject).<init>("HEAD", i4);
    e = (m)localObject;
    localObject = new b/a/a/a$m;
    int i5 = 5;
    ((m)localObject).<init>("OPTIONS", i5);
    f = (m)localObject;
    localObject = new b/a/a/a$m;
    int i6 = 6;
    ((m)localObject).<init>("TRACE", i6);
    g = (m)localObject;
    localObject = new b/a/a/a$m;
    int i7 = 7;
    ((m)localObject).<init>("CONNECT", i7);
    h = (m)localObject;
    localObject = new b/a/a/a$m;
    int i8 = 8;
    ((m)localObject).<init>("PATCH", i8);
    i = (m)localObject;
    localObject = new b/a/a/a$m;
    int i9 = 9;
    ((m)localObject).<init>("PROPFIND", i9);
    j = (m)localObject;
    localObject = new b/a/a/a$m;
    int i10 = 10;
    ((m)localObject).<init>("PROPPATCH", i10);
    k = (m)localObject;
    localObject = new b/a/a/a$m;
    int i11 = 11;
    ((m)localObject).<init>("MKCOL", i11);
    l = (m)localObject;
    localObject = new b/a/a/a$m;
    int i12 = 12;
    ((m)localObject).<init>("MOVE", i12);
    m = (m)localObject;
    localObject = new b/a/a/a$m;
    ((m)localObject).<init>("COPY", 13);
    n = (m)localObject;
    localObject = new b/a/a/a$m;
    ((m)localObject).<init>("LOCK", 14);
    o = (m)localObject;
    localObject = new b/a/a/a$m;
    ((m)localObject).<init>("UNLOCK", 15);
    p = (m)localObject;
    localObject = new m[16];
    m localm = a;
    localObject[0] = localm;
    localm = b;
    localObject[i1] = localm;
    localm = c;
    localObject[i2] = localm;
    localm = d;
    localObject[i3] = localm;
    localm = e;
    localObject[i4] = localm;
    localm = f;
    localObject[i5] = localm;
    localm = g;
    localObject[i6] = localm;
    localm = h;
    localObject[i7] = localm;
    localm = i;
    localObject[i8] = localm;
    localm = j;
    localObject[i9] = localm;
    localm = k;
    localObject[i10] = localm;
    localm = l;
    localObject[i11] = localm;
    localm = m;
    localObject[i12] = localm;
    localm = n;
    localObject[13] = localm;
    localm = o;
    localObject[14] = localm;
    localm = p;
    localObject[15] = localm;
    q = (m[])localObject;
  }
  
  static m a(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    try
    {
      return valueOf(paramString);
    }
    catch (IllegalArgumentException localIllegalArgumentException) {}
    return null;
  }
}

/* Location:
 * Qualified Name:     b.a.a.a.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */