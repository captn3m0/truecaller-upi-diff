package a.a;

final class b$c
{
  int a;
  int b;
  String c;
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Start:");
    int i = a;
    localStringBuilder.append(i);
    localStringBuilder.append(" End:");
    i = b;
    localStringBuilder.append(i);
    localStringBuilder.append(" '");
    String str = c;
    localStringBuilder.append(str);
    localStringBuilder.append("'");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     a.a.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */