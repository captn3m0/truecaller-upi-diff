package a.a;

final class c
{
  private final a a;
  private boolean b;
  private a.a c;
  private int d;
  private int e;
  private int f;
  private int g;
  private int h;
  private int i;
  private int j;
  private Integer k;
  private Integer l;
  private Integer m;
  private Integer n;
  private Integer o;
  private Integer p;
  private Integer q;
  
  c(a parama, a.a parama1)
  {
    a = parama;
    parama = a;
    int i1 = 6;
    a.d[] arrayOfd = new a.d[i1];
    a.d locald = a.d.a;
    int i2 = 0;
    arrayOfd[0] = locald;
    locald = a.d.b;
    int i3 = 1;
    arrayOfd[i3] = locald;
    locald = a.d.c;
    int i4 = 2;
    arrayOfd[i4] = locald;
    locald = a.d.d;
    int i5 = 3;
    arrayOfd[i5] = locald;
    locald = a.d.e;
    arrayOfd[4] = locald;
    locald = a.d.f;
    int i6 = 5;
    arrayOfd[i6] = locald;
    boolean bool = parama.a(arrayOfd);
    if (bool) {}
    do
    {
      do
      {
        bool = true;
        break label378;
        parama = a;
        arrayOfd = new a.d[i5];
        locald = a.d.a;
        arrayOfd[0] = locald;
        locald = a.d.b;
        arrayOfd[i3] = locald;
        locald = a.d.c;
        arrayOfd[i4] = locald;
        bool = parama.a(arrayOfd);
        if (!bool) {
          break;
        }
        parama = a;
        arrayOfd = new a.d[i5];
        locald = a.d.d;
        arrayOfd[0] = locald;
        locald = a.d.e;
        arrayOfd[i3] = locald;
        locald = a.d.f;
        arrayOfd[i4] = locald;
        bool = parama.b(arrayOfd);
      } while (bool);
      parama = a;
      arrayOfd = new a.d[i5];
      locald = a.d.a;
      arrayOfd[0] = locald;
      locald = a.d.b;
      arrayOfd[i3] = locald;
      locald = a.d.c;
      arrayOfd[i4] = locald;
      bool = parama.b(arrayOfd);
      if (!bool) {
        break;
      }
      parama = a;
      arrayOfd = new a.d[i5];
      locald = a.d.d;
      arrayOfd[0] = locald;
      locald = a.d.e;
      arrayOfd[i3] = locald;
      locald = a.d.f;
      arrayOfd[i4] = locald;
      bool = parama.a(arrayOfd);
    } while (bool);
    bool = false;
    parama = null;
    label378:
    if (bool)
    {
      parama = a.a();
      int i7;
      if (parama == null)
      {
        bool = true;
      }
      else
      {
        parama = a.a();
        i7 = parama.intValue();
      }
      parama = Integer.valueOf(i7);
      k = parama;
      parama = a.b();
      if (parama == null)
      {
        i7 = 1;
      }
      else
      {
        parama = a.b();
        i7 = parama.intValue();
      }
      parama = Integer.valueOf(i7);
      l = parama;
      parama = a.c();
      if (parama != null)
      {
        parama = a.c();
        i3 = parama.intValue();
      }
      parama = Integer.valueOf(i3);
      m = parama;
      parama = a.d();
      if (parama == null)
      {
        i7 = 0;
        parama = null;
      }
      else
      {
        parama = a.d();
        i7 = parama.intValue();
      }
      parama = Integer.valueOf(i7);
      n = parama;
      parama = a.e();
      if (parama == null)
      {
        i7 = 0;
        parama = null;
      }
      else
      {
        parama = a.e();
        i7 = parama.intValue();
      }
      parama = Integer.valueOf(i7);
      o = parama;
      parama = a.f();
      if (parama == null)
      {
        i7 = 0;
        parama = null;
      }
      else
      {
        parama = a.f();
        i7 = parama.intValue();
      }
      parama = Integer.valueOf(i7);
      p = parama;
      parama = a.g();
      if (parama != null)
      {
        parama = a.g();
        i2 = parama.intValue();
      }
      parama = Integer.valueOf(i2);
      q = parama;
      c = parama1;
      return;
    }
    parama = new java/lang/IllegalArgumentException;
    parama.<init>("For interval calculations, DateTime must have year-month-day, or hour-minute-second, or both.");
    throw parama;
  }
  
  private void a()
  {
    boolean bool = b;
    Integer localInteger;
    int i1;
    int i2;
    if (bool)
    {
      localInteger = k;
      i1 = localInteger.intValue();
      i2 = d;
      i1 += i2;
    }
    for (;;)
    {
      localInteger = Integer.valueOf(i1);
      k = localInteger;
      return;
      localInteger = a.a();
      i1 = localInteger.intValue();
      i2 = d;
      i1 -= i2;
    }
  }
  
  private static void a(Integer paramInteger)
  {
    int i1 = paramInteger.intValue();
    if (i1 >= 0)
    {
      int i2 = paramInteger.intValue();
      i1 = 999999999;
      if (i2 <= i1) {
        return;
      }
    }
    paramInteger = new java/lang/IllegalArgumentException;
    paramInteger.<init>("Nanosecond interval is not in the range 0..999999999");
    throw paramInteger;
  }
  
  private static void a(Integer paramInteger, String paramString)
  {
    int i1 = paramInteger.intValue();
    if (i1 >= 0)
    {
      int i2 = paramInteger.intValue();
      i1 = 9999;
      if (i2 <= i1) {
        return;
      }
    }
    paramInteger = new java/lang/IllegalArgumentException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramString);
    localStringBuilder.append(" is not in the range 0..9999");
    paramString = localStringBuilder.toString();
    paramInteger.<init>(paramString);
    throw paramInteger;
  }
  
  private void b()
  {
    int i1 = 0;
    for (;;)
    {
      int i2 = e;
      if (i1 >= i2) {
        break;
      }
      i();
      i1 += 1;
    }
  }
  
  private void c()
  {
    int i1 = 0;
    for (;;)
    {
      int i2 = f;
      if (i1 >= i2) {
        break;
      }
      j();
      i1 += 1;
    }
  }
  
  private void d()
  {
    int i1 = 0;
    for (;;)
    {
      int i2 = g;
      if (i1 >= i2) {
        break;
      }
      m();
      i1 += 1;
    }
  }
  
  private void e()
  {
    int i1 = 0;
    for (;;)
    {
      int i2 = h;
      if (i1 >= i2) {
        break;
      }
      n();
      i1 += 1;
    }
  }
  
  private void f()
  {
    int i1 = 0;
    for (;;)
    {
      int i2 = i;
      if (i1 >= i2) {
        break;
      }
      o();
      i1 += 1;
    }
  }
  
  private void g()
  {
    boolean bool = b;
    if (bool)
    {
      localInteger = q;
      i1 = localInteger.intValue();
      i2 = j;
      i1 += i2;
    }
    else
    {
      localInteger = q;
      i1 = localInteger.intValue();
      i2 = j;
      i1 -= i2;
    }
    Integer localInteger = Integer.valueOf(i1);
    q = localInteger;
    localInteger = q;
    int i1 = localInteger.intValue();
    int i2 = 999999999;
    if (i1 > i2)
    {
      o();
      localInteger = Integer.valueOf(q.intValue() - i2 + -1);
      q = localInteger;
      return;
    }
    localInteger = q;
    i1 = localInteger.intValue();
    if (i1 < 0)
    {
      o();
      i1 = q.intValue() + i2 + 1;
      localInteger = Integer.valueOf(i1);
      q = localInteger;
    }
  }
  
  private void h()
  {
    boolean bool = b;
    Integer localInteger;
    if (bool) {
      localInteger = k;
    }
    for (int i1 = localInteger.intValue() + 1;; i1 = localInteger.intValue() + -1)
    {
      localInteger = Integer.valueOf(i1);
      k = localInteger;
      return;
      localInteger = k;
    }
  }
  
  private void i()
  {
    boolean bool = b;
    int i2 = 1;
    if (bool)
    {
      localInteger = l;
      i1 = localInteger.intValue() + i2;
    }
    else
    {
      localInteger = l;
      i1 = localInteger.intValue() - i2;
    }
    Integer localInteger = Integer.valueOf(i1);
    l = localInteger;
    localInteger = l;
    int i1 = localInteger.intValue();
    int i3 = 12;
    if (i1 > i3)
    {
      localInteger = Integer.valueOf(i2);
      l = localInteger;
      h();
      return;
    }
    localInteger = l;
    i1 = localInteger.intValue();
    if (i1 <= 0)
    {
      localInteger = Integer.valueOf(i3);
      l = localInteger;
      h();
    }
  }
  
  private void j()
  {
    boolean bool = b;
    int i2 = 1;
    if (bool)
    {
      localInteger = m;
      i1 = localInteger.intValue() + i2;
    }
    else
    {
      localInteger = m;
      i1 = localInteger.intValue() - i2;
    }
    Integer localInteger = Integer.valueOf(i1);
    m = localInteger;
    localInteger = m;
    int i1 = localInteger.intValue();
    int i3 = k();
    if (i1 > i3)
    {
      localInteger = Integer.valueOf(i2);
      m = localInteger;
      i();
      return;
    }
    localInteger = m;
    i1 = localInteger.intValue();
    if (i1 <= 0)
    {
      i1 = l();
      localInteger = Integer.valueOf(i1);
      m = localInteger;
      i();
    }
  }
  
  private int k()
  {
    Integer localInteger1 = k;
    Integer localInteger2 = l;
    return a.a(localInteger1, localInteger2).intValue();
  }
  
  private int l()
  {
    Integer localInteger1 = l;
    int i1 = localInteger1.intValue();
    int i2 = 1;
    Integer localInteger3;
    if (i1 > i2)
    {
      localInteger1 = k;
      Integer localInteger2 = l;
      int i3 = localInteger2.intValue() - i2;
      localInteger3 = Integer.valueOf(i3);
    }
    else
    {
      i1 = k.intValue() - i2;
      localInteger1 = Integer.valueOf(i1);
      i2 = 12;
      localInteger3 = Integer.valueOf(i2);
    }
    return a.a(localInteger1, localInteger3).intValue();
  }
  
  private void m()
  {
    boolean bool = b;
    if (bool)
    {
      localInteger = n;
      i1 = localInteger.intValue() + 1;
    }
    else
    {
      localInteger = n;
      i1 = localInteger.intValue() + -1;
    }
    Integer localInteger = Integer.valueOf(i1);
    n = localInteger;
    localInteger = n;
    int i1 = localInteger.intValue();
    int i2 = 23;
    if (i1 > i2)
    {
      localInteger = Integer.valueOf(0);
      n = localInteger;
      j();
      return;
    }
    localInteger = n;
    i1 = localInteger.intValue();
    if (i1 < 0)
    {
      localInteger = Integer.valueOf(i2);
      n = localInteger;
      j();
    }
  }
  
  private void n()
  {
    boolean bool = b;
    if (bool)
    {
      localInteger = o;
      i1 = localInteger.intValue() + 1;
    }
    else
    {
      localInteger = o;
      i1 = localInteger.intValue() + -1;
    }
    Integer localInteger = Integer.valueOf(i1);
    o = localInteger;
    localInteger = o;
    int i1 = localInteger.intValue();
    int i2 = 59;
    if (i1 > i2)
    {
      localInteger = Integer.valueOf(0);
      o = localInteger;
      m();
      return;
    }
    localInteger = o;
    i1 = localInteger.intValue();
    if (i1 < 0)
    {
      localInteger = Integer.valueOf(i2);
      o = localInteger;
      m();
    }
  }
  
  private void o()
  {
    boolean bool = b;
    if (bool)
    {
      localInteger = p;
      i1 = localInteger.intValue() + 1;
    }
    else
    {
      localInteger = p;
      i1 = localInteger.intValue() + -1;
    }
    Integer localInteger = Integer.valueOf(i1);
    p = localInteger;
    localInteger = p;
    int i1 = localInteger.intValue();
    int i2 = 59;
    if (i1 > i2)
    {
      localInteger = Integer.valueOf(0);
      p = localInteger;
      n();
      return;
    }
    localInteger = p;
    i1 = localInteger.intValue();
    if (i1 < 0)
    {
      localInteger = Integer.valueOf(i2);
      p = localInteger;
      n();
    }
  }
  
  private void p()
  {
    int i1 = k();
    Object localObject1 = m;
    int i2 = ((Integer)localObject1).intValue();
    if (i2 > i1)
    {
      localObject1 = a.a.d;
      Object localObject2 = c;
      Object localObject3;
      if (localObject1 != localObject2)
      {
        localObject1 = a.a.b;
        localObject2 = c;
        if (localObject1 == localObject2)
        {
          localObject3 = Integer.valueOf(1);
          m = ((Integer)localObject3);
          i();
          return;
        }
        localObject1 = a.a.a;
        localObject2 = c;
        if (localObject1 == localObject2)
        {
          localObject3 = Integer.valueOf(i1);
          m = ((Integer)localObject3);
          return;
        }
        localObject1 = a.a.c;
        localObject2 = c;
        if (localObject1 == localObject2)
        {
          localObject1 = m;
          i2 = ((Integer)localObject1).intValue() - i1;
          localObject3 = Integer.valueOf(i2);
          m = ((Integer)localObject3);
          i();
        }
      }
      else
      {
        localObject1 = new java/lang/RuntimeException;
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("Day Overflow: Year:");
        Integer localInteger = k;
        ((StringBuilder)localObject2).append(localInteger);
        ((StringBuilder)localObject2).append(" Month:");
        localInteger = l;
        ((StringBuilder)localObject2).append(localInteger);
        ((StringBuilder)localObject2).append(" has ");
        ((StringBuilder)localObject2).append(i1);
        ((StringBuilder)localObject2).append(" days, but day has value:");
        localObject3 = m;
        ((StringBuilder)localObject2).append(localObject3);
        ((StringBuilder)localObject2).append(" To avoid these exceptions, please specify a different DayOverflow policy.");
        localObject3 = ((StringBuilder)localObject2).toString();
        ((RuntimeException)localObject1).<init>((String)localObject3);
        throw ((Throwable)localObject1);
      }
    }
  }
  
  final a a(boolean paramBoolean, Integer paramInteger1, Integer paramInteger2, Integer paramInteger3, Integer paramInteger4, Integer paramInteger5, Integer paramInteger6, Integer paramInteger7)
  {
    boolean bool = paramBoolean;
    b = paramBoolean;
    int i1 = paramInteger1.intValue();
    d = i1;
    i1 = paramInteger2.intValue();
    e = i1;
    i1 = paramInteger3.intValue();
    f = i1;
    i1 = paramInteger4.intValue();
    g = i1;
    i1 = paramInteger5.intValue();
    h = i1;
    i1 = paramInteger6.intValue();
    i = i1;
    i1 = paramInteger7.intValue();
    j = i1;
    a(Integer.valueOf(d), "Year");
    a(Integer.valueOf(e), "Month");
    a(Integer.valueOf(f), "Day");
    a(Integer.valueOf(g), "Hour");
    a(Integer.valueOf(h), "Minute");
    a(Integer.valueOf(i), "Second");
    a(Integer.valueOf(j));
    a();
    b();
    p();
    c();
    d();
    e();
    f();
    g();
    a locala = new a/a/a;
    Integer localInteger1 = k;
    Integer localInteger2 = l;
    Integer localInteger3 = m;
    Integer localInteger4 = n;
    Integer localInteger5 = o;
    Integer localInteger6 = p;
    Integer localInteger7 = q;
    paramInteger1 = localInteger1;
    paramInteger2 = localInteger2;
    paramInteger3 = localInteger3;
    paramInteger4 = localInteger4;
    paramInteger5 = localInteger5;
    paramInteger6 = localInteger6;
    paramInteger7 = localInteger7;
    locala.<init>(localInteger1, localInteger2, localInteger3, localInteger4, localInteger5, localInteger6, localInteger7);
    return locala;
  }
}

/* Location:
 * Qualified Name:     a.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */