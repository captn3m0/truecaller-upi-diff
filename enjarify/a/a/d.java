package a.a;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class d
{
  private static final Pattern a = Pattern.compile("(\\d{1,4})-(\\d\\d)-(\\d\\d)|(\\d{1,4})-(\\d\\d)|(\\d{1,4})");
  private static final Integer b = Integer.valueOf("9");
  private static final Pattern c = Pattern.compile("(\\d\\d)\\:(\\d\\d)\\:(\\d\\d)\\.(\\d{1,9})|(\\d\\d)\\:(\\d\\d)\\:(\\d\\d)|(\\d\\d)\\:(\\d\\d)|(\\d\\d)");
  private Integer d;
  private Integer e;
  private Integer f;
  private Integer g;
  private Integer h;
  private Integer i;
  private Integer j;
  
  private static String a(Matcher paramMatcher, int... paramVarArgs)
  {
    int k = paramVarArgs.length;
    int m = 0;
    String str = null;
    int n = 0;
    while (n < k)
    {
      m = paramVarArgs[n];
      str = paramMatcher.group(m);
      if (str != null) {
        break;
      }
      n += 1;
    }
    return str;
  }
  
  private d.a b(String paramString)
  {
    d.a locala = new a/a/d$a;
    boolean bool = false;
    String str = null;
    locala.<init>(this, (byte)0);
    int k = c(paramString);
    int m = 1;
    if (k > 0)
    {
      n = paramString.length();
      if (k < n)
      {
        n = 1;
        break label51;
      }
    }
    int n = 0;
    label51:
    if (n != 0)
    {
      str = paramString.substring(0, k);
      a = str;
      k += m;
      paramString = paramString.substring(k);
    }
    do
    {
      b = paramString;
      break;
      bool = d(paramString);
    } while (bool);
    a = paramString;
    return locala;
  }
  
  private static int c(String paramString)
  {
    String str = " ";
    int k = paramString.indexOf(str);
    int m = -1;
    if (k == m)
    {
      str = "T";
      k = paramString.indexOf(str);
    }
    return k;
  }
  
  private static boolean d(String paramString)
  {
    int k = paramString.length();
    int m = 2;
    boolean bool;
    if (k >= m)
    {
      String str = ":";
      int n = 3;
      paramString = paramString.substring(m, n);
      bool = str.equals(paramString);
    }
    else
    {
      bool = false;
      paramString = null;
    }
    return bool;
  }
  
  private void e(String paramString)
  {
    Object localObject1 = a.matcher(paramString);
    boolean bool = ((Matcher)localObject1).matches();
    if (bool)
    {
      int m = 3;
      Object localObject2 = new int[m];
      Object tmp28_26 = localObject2;
      tmp28_26[0] = 1;
      Object tmp32_28 = tmp28_26;
      tmp32_28[1] = 4;
      tmp32_28[2] = 6;
      localObject2 = a((Matcher)localObject1, (int[])localObject2);
      if (localObject2 != null)
      {
        localObject2 = Integer.valueOf((String)localObject2);
        d = ((Integer)localObject2);
      }
      int k = 2;
      localObject2 = new int[k];
      Object tmp75_73 = localObject2;
      tmp75_73[0] = 2;
      tmp75_73[1] = 5;
      localObject2 = a((Matcher)localObject1, (int[])localObject2);
      if (localObject2 != null)
      {
        localObject2 = Integer.valueOf((String)localObject2);
        e = ((Integer)localObject2);
      }
      k = 1;
      localObject2 = new int[k];
      localObject2[0] = m;
      paramString = a((Matcher)localObject1, (int[])localObject2);
      if (paramString != null)
      {
        paramString = Integer.valueOf(paramString);
        f = paramString;
      }
      return;
    }
    localObject1 = new a/a/d$b;
    paramString = String.valueOf(paramString);
    paramString = "Unexpected format for date:".concat(paramString);
    ((d.b)localObject1).<init>(paramString);
    throw ((Throwable)localObject1);
  }
  
  private void f(String paramString)
  {
    Object localObject1 = c.matcher(paramString);
    boolean bool = ((Matcher)localObject1).matches();
    if (bool)
    {
      int m = 4;
      Object localObject2 = new int[m];
      Object tmp28_26 = localObject2;
      Object tmp29_28 = tmp28_26;
      Object tmp29_28 = tmp28_26;
      tmp29_28[0] = 1;
      tmp29_28[1] = 5;
      tmp29_28[2] = 8;
      tmp29_28[3] = 10;
      localObject2 = a((Matcher)localObject1, (int[])localObject2);
      if (localObject2 != null)
      {
        localObject2 = Integer.valueOf((String)localObject2);
        g = ((Integer)localObject2);
      }
      int k = 3;
      localObject2 = new int[k];
      Object tmp79_77 = localObject2;
      tmp79_77[0] = 2;
      Object tmp83_79 = tmp79_77;
      tmp83_79[1] = 6;
      tmp83_79[2] = 9;
      localObject2 = a((Matcher)localObject1, (int[])localObject2);
      if (localObject2 != null)
      {
        localObject2 = Integer.valueOf((String)localObject2);
        h = ((Integer)localObject2);
      }
      k = 2;
      localObject2 = new int[k];
      Object tmp127_125 = localObject2;
      tmp127_125[0] = 3;
      tmp127_125[1] = 7;
      localObject2 = a((Matcher)localObject1, (int[])localObject2);
      if (localObject2 != null)
      {
        localObject2 = Integer.valueOf((String)localObject2);
        i = ((Integer)localObject2);
      }
      k = 1;
      localObject2 = new int[k];
      localObject2[0] = m;
      paramString = a((Matcher)localObject1, (int[])localObject2);
      if (paramString != null)
      {
        paramString = Integer.valueOf(g(paramString));
        j = paramString;
      }
      return;
    }
    localObject1 = new a/a/d$b;
    paramString = String.valueOf(paramString);
    paramString = "Unexpected format for time:".concat(paramString);
    ((d.b)localObject1).<init>(paramString);
    throw ((Throwable)localObject1);
  }
  
  private static String g(String paramString)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(paramString);
    for (;;)
    {
      int k = localStringBuilder.length();
      Integer localInteger = b;
      int m = localInteger.intValue();
      if (k >= m) {
        break;
      }
      paramString = "0";
      localStringBuilder.append(paramString);
    }
    return localStringBuilder.toString();
  }
  
  final a a(String paramString)
  {
    if (paramString != null)
    {
      paramString = paramString.trim();
      paramString = b(paramString);
      boolean bool = paramString.a();
      if (bool)
      {
        str = a;
        e(str);
      }
      do
      {
        paramString = b;
        f(paramString);
        break;
        bool = paramString.b();
        if (bool)
        {
          paramString = a;
          e(paramString);
          break;
        }
        bool = paramString.c();
      } while (bool);
      paramString = new a/a/a;
      Integer localInteger1 = d;
      Integer localInteger2 = e;
      Integer localInteger3 = f;
      Integer localInteger4 = g;
      Integer localInteger5 = h;
      Integer localInteger6 = i;
      Integer localInteger7 = j;
      String str = paramString;
      paramString.<init>(localInteger1, localInteger2, localInteger3, localInteger4, localInteger5, localInteger6, localInteger7);
      return paramString;
    }
    paramString = new java/lang/NullPointerException;
    paramString.<init>("DateTime string is null");
    throw paramString;
  }
}

/* Location:
 * Qualified Name:     a.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */