package a.a;

import java.lang.reflect.Array;

final class e
{
  static int a(int paramInt, Object paramObject)
  {
    int i = 0;
    if (paramObject == null)
    {
      paramInt = paramInt * 37 + 0;
    }
    else
    {
      boolean bool = a(paramObject);
      if (!bool)
      {
        int k = paramObject.hashCode();
        paramInt = paramInt * 37 + k;
      }
      else
      {
        int j = Array.getLength(paramObject);
        while (i < j)
        {
          Object localObject = Array.get(paramObject, i);
          paramInt = a(paramInt, localObject);
          i += 1;
        }
      }
    }
    return paramInt;
  }
  
  static int a(Comparable paramComparable1, Comparable paramComparable2, e.a parama)
  {
    int i;
    if ((paramComparable1 != null) && (paramComparable2 != null))
    {
      i = paramComparable1.compareTo(paramComparable2);
    }
    else
    {
      if ((paramComparable1 != null) || (paramComparable2 != null))
      {
        if ((paramComparable1 == null) && (paramComparable2 != null))
        {
          i = -1;
          break label57;
        }
        if ((paramComparable1 != null) && (paramComparable2 == null))
        {
          i = 1;
          break label57;
        }
      }
      i = 0;
      paramComparable1 = null;
      label57:
      paramComparable2 = e.a.b;
      if (paramComparable2 == parama) {
        i *= -1;
      }
    }
    return i;
  }
  
  static boolean a(Object paramObject)
  {
    if (paramObject != null)
    {
      paramObject = paramObject.getClass();
      boolean bool = ((Class)paramObject).isArray();
      if (bool) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     a.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */