package a.a;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class b
{
  private static final Pattern i = Pattern.compile("\\|[^\\|]*\\|");
  private static final Pattern j = Pattern.compile("f{1,9}");
  private static final List k;
  Collection a;
  Collection b;
  private final String c;
  private final Locale d;
  private final Map e;
  private final Map f;
  private final Map g;
  private final b.a h;
  
  static
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    k = localArrayList;
    localArrayList.add("YYYY");
    k.add("YY");
    k.add("MMMM");
    k.add("MMM");
    k.add("MM");
    k.add("M");
    k.add("DD");
    k.add("D");
    k.add("WWWW");
    k.add("WWW");
    k.add("hh12");
    k.add("h12");
    k.add("hh");
    k.add("h");
    k.add("mm");
    k.add("m");
    k.add("ss");
    k.add("s");
    k.add("a");
    k.add("fffffffff");
    k.add("ffffffff");
    k.add("fffffff");
    k.add("ffffff");
    k.add("fffff");
    k.add("ffff");
    k.add("fff");
    k.add("ff");
    k.add("f");
  }
  
  b(String paramString)
  {
    LinkedHashMap localLinkedHashMap = new java/util/LinkedHashMap;
    localLinkedHashMap.<init>();
    e = localLinkedHashMap;
    localLinkedHashMap = new java/util/LinkedHashMap;
    localLinkedHashMap.<init>();
    f = localLinkedHashMap;
    localLinkedHashMap = new java/util/LinkedHashMap;
    localLinkedHashMap.<init>();
    g = localLinkedHashMap;
    c = paramString;
    d = null;
    h = null;
    paramString = c;
    boolean bool = f.a(paramString);
    if (bool) {
      return;
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("DateTime format has no content.");
    throw paramString;
  }
  
  private b.c a(int paramInt)
  {
    Iterator localIterator = a.iterator();
    Object localObject = null;
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      b.c localc = (b.c)localIterator.next();
      int m = a;
      if (m == paramInt) {
        localObject = localc;
      }
    }
    return (b.c)localObject;
  }
  
  private static String a(Integer paramInteger)
  {
    String str;
    for (paramInteger = a(paramInteger);; paramInteger = str.concat(paramInteger))
    {
      int m = paramInteger.length();
      int n = 9;
      if (m >= n) {
        break;
      }
      str = "0";
      paramInteger = String.valueOf(paramInteger);
    }
    return paramInteger;
  }
  
  private static String a(Object paramObject)
  {
    String str = "";
    if (paramObject != null) {
      str = String.valueOf(paramObject);
    }
    return str;
  }
  
  private static String a(String paramString)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    int m = 1;
    for (;;)
    {
      int n = paramString.length();
      if (m > n) {
        break;
      }
      String str = "@";
      localStringBuilder.append(str);
      m += 1;
    }
    return localStringBuilder.toString();
  }
  
  private String b(Integer paramInteger)
  {
    Object localObject = "";
    if (paramInteger != null)
    {
      localObject = h;
      if (localObject != null)
      {
        localObject = c(paramInteger);
      }
      else
      {
        localObject = d;
        if (localObject != null)
        {
          localObject = d(paramInteger);
        }
        else
        {
          paramInteger = new java/lang/IllegalArgumentException;
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>("Your date pattern requires either a Locale, or your own custom localizations for text:");
          String str = f.a(c);
          ((StringBuilder)localObject).append(str);
          localObject = ((StringBuilder)localObject).toString();
          paramInteger.<init>((String)localObject);
          throw paramInteger;
        }
      }
    }
    return (String)localObject;
  }
  
  private static String b(String paramString)
  {
    boolean bool = f.a(paramString);
    if (bool)
    {
      int m = paramString.length();
      int n = 1;
      if (m == n)
      {
        String str = "0";
        paramString = String.valueOf(paramString);
        paramString = str.concat(paramString);
      }
    }
    return paramString;
  }
  
  private String c(Integer paramInteger)
  {
    List localList = h.a;
    int m = paramInteger.intValue() + -1;
    return (String)localList.get(m);
  }
  
  private static String c(String paramString)
  {
    boolean bool = f.a(paramString);
    if (bool)
    {
      int m = paramString.length();
      int n = 3;
      if (m >= n)
      {
        m = 0;
        paramString = paramString.substring(0, n);
      }
    }
    return paramString;
  }
  
  private String d(Integer paramInteger)
  {
    Object localObject1 = e;
    Locale localLocale1 = d;
    boolean bool = ((Map)localObject1).containsKey(localLocale1);
    int m = 1;
    if (!bool)
    {
      localObject1 = new java/util/ArrayList;
      ((ArrayList)localObject1).<init>();
      localObject2 = new java/text/SimpleDateFormat;
      Object localObject3 = d;
      ((SimpleDateFormat)localObject2).<init>("MMMM", (Locale)localObject3);
      int n = 0;
      Locale localLocale2 = null;
      for (;;)
      {
        int i1 = 11;
        if (n > i1) {
          break;
        }
        localObject3 = new java/util/GregorianCalendar;
        ((GregorianCalendar)localObject3).<init>();
        ((Calendar)localObject3).set(m, 2000);
        ((Calendar)localObject3).set(2, n);
        int i2 = 5;
        int i3 = 15;
        ((Calendar)localObject3).set(i2, i3);
        localObject3 = ((Calendar)localObject3).getTime();
        localObject3 = ((SimpleDateFormat)localObject2).format((Date)localObject3);
        ((List)localObject1).add(localObject3);
        n += 1;
      }
      localObject2 = e;
      localLocale2 = d;
      ((Map)localObject2).put(localLocale2, localObject1);
    }
    localObject1 = e;
    Object localObject2 = d;
    localObject1 = (List)((Map)localObject1).get(localObject2);
    int i4 = paramInteger.intValue() - m;
    return (String)((List)localObject1).get(i4);
  }
  
  private String e(Integer paramInteger)
  {
    Object localObject = "";
    if (paramInteger != null)
    {
      localObject = h;
      if (localObject != null)
      {
        localObject = f(paramInteger);
      }
      else
      {
        localObject = d;
        if (localObject != null)
        {
          localObject = g(paramInteger);
        }
        else
        {
          paramInteger = new java/lang/IllegalArgumentException;
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>("Your date pattern requires either a Locale, or your own custom localizations for text:");
          String str = f.a(c);
          ((StringBuilder)localObject).append(str);
          localObject = ((StringBuilder)localObject).toString();
          paramInteger.<init>((String)localObject);
          throw paramInteger;
        }
      }
    }
    return (String)localObject;
  }
  
  private String f(Integer paramInteger)
  {
    List localList = h.b;
    int m = paramInteger.intValue() + -1;
    return (String)localList.get(m);
  }
  
  private String g(Integer paramInteger)
  {
    Object localObject1 = f;
    Locale localLocale = d;
    boolean bool = ((Map)localObject1).containsKey(localLocale);
    int m = 1;
    if (!bool)
    {
      localObject1 = new java/util/ArrayList;
      ((ArrayList)localObject1).<init>();
      localObject2 = new java/text/SimpleDateFormat;
      Object localObject3 = "EEEE";
      Object localObject4 = d;
      ((SimpleDateFormat)localObject2).<init>((String)localObject3, (Locale)localObject4);
      int n = 8;
      for (;;)
      {
        int i1 = 14;
        if (n > i1) {
          break;
        }
        localObject4 = new java/util/GregorianCalendar;
        ((GregorianCalendar)localObject4).<init>();
        ((Calendar)localObject4).set(m, 2009);
        ((Calendar)localObject4).set(2, m);
        int i2 = 5;
        ((Calendar)localObject4).set(i2, n);
        localObject4 = ((Calendar)localObject4).getTime();
        localObject4 = ((SimpleDateFormat)localObject2).format((Date)localObject4);
        ((List)localObject1).add(localObject4);
        n += 1;
      }
      localObject2 = f;
      localObject3 = d;
      ((Map)localObject2).put(localObject3, localObject1);
    }
    localObject1 = f;
    Object localObject2 = d;
    localObject1 = (List)((Map)localObject1).get(localObject2);
    int i3 = paramInteger.intValue() - m;
    return (String)((List)localObject1).get(i3);
  }
  
  private static Integer h(Integer paramInteger)
  {
    if (paramInteger != null)
    {
      int m = paramInteger.intValue();
      int n = 12;
      if (m == 0)
      {
        paramInteger = Integer.valueOf(n);
      }
      else
      {
        m = paramInteger.intValue();
        if (m > n)
        {
          int i1 = paramInteger.intValue() - n;
          paramInteger = Integer.valueOf(i1);
        }
      }
    }
    return paramInteger;
  }
  
  private String i(Integer paramInteger)
  {
    SimpleDateFormat localSimpleDateFormat = new java/text/SimpleDateFormat;
    Locale localLocale = d;
    localSimpleDateFormat.<init>("a", localLocale);
    GregorianCalendar localGregorianCalendar = new java/util/GregorianCalendar;
    localGregorianCalendar.<init>();
    localGregorianCalendar.set(1, 2000);
    localGregorianCalendar.set(2, 6);
    localGregorianCalendar.set(5, 15);
    int m = paramInteger.intValue();
    localGregorianCalendar.set(11, m);
    paramInteger = localGregorianCalendar.getTime();
    return localSimpleDateFormat.format(paramInteger);
  }
  
  final void a()
  {
    Object localObject1 = i;
    Object localObject2 = c;
    localObject1 = ((Pattern)localObject1).matcher((CharSequence)localObject2);
    for (;;)
    {
      boolean bool = ((Matcher)localObject1).find();
      if (!bool) {
        break;
      }
      localObject2 = new a/a/b$b;
      ((b.b)localObject2).<init>((byte)0);
      int m = ((Matcher)localObject1).start();
      a = m;
      m = ((Matcher)localObject1).end() + -1;
      b = m;
      Collection localCollection = b;
      localCollection.add(localObject2);
    }
  }
  
  final void a(a parama)
  {
    Object localObject1 = c;
    Object localObject2 = k.iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject2).hasNext();
      if (!bool1) {
        break;
      }
      String str = (String)((Iterator)localObject2).next();
      Object localObject3 = Pattern.compile(str).matcher((CharSequence)localObject1);
      Object localObject5;
      for (;;)
      {
        boolean bool2 = ((Matcher)localObject3).find();
        if (!bool2) {
          break label1404;
        }
        b.c localc = new a/a/b$c;
        int m = 0;
        Object localObject4 = null;
        localc.<init>((byte)0);
        int n = ((Matcher)localObject3).start();
        a = n;
        n = ((Matcher)localObject3).end();
        int i1 = 1;
        n -= i1;
        b = n;
        localObject5 = b.iterator();
        Object localObject6;
        int i5;
        int i6;
        int i3;
        do
        {
          do
          {
            boolean bool4 = ((Iterator)localObject5).hasNext();
            if (!bool4) {
              break;
            }
            localObject6 = (b.b)((Iterator)localObject5).next();
            i5 = a;
            i6 = a;
          } while (i5 > i6);
          i5 = a;
          i3 = b;
        } while (i5 > i3);
        n = 1;
        break label207;
        n = 0;
        localObject5 = null;
        label207:
        if (n == 0)
        {
          localObject5 = ((Matcher)localObject3).group();
          localObject6 = "YYYY";
          boolean bool5 = ((String)localObject6).equals(localObject5);
          if (bool5) {
            localObject4 = parama.a();
          }
          for (;;)
          {
            localObject4 = a(localObject4);
            break label1328;
            localObject6 = "YY";
            bool5 = ((String)localObject6).equals(localObject5);
            int i2;
            if (bool5)
            {
              localObject4 = a(parama.a());
              localObject5 = "";
              i2 = f.a((String)localObject4);
              if (i2 != 0)
              {
                n = 2;
                localObject4 = ((String)localObject4).substring(n);
                break label1328;
              }
              localObject4 = localObject5;
              break label1328;
            }
            localObject6 = "MMMM";
            bool5 = ((String)localObject6).equals(localObject5);
            if (bool5)
            {
              m = parama.b().intValue();
              localObject4 = Integer.valueOf(m);
              localObject4 = b((Integer)localObject4);
              break label1328;
            }
            localObject6 = "MMM";
            bool5 = ((String)localObject6).equals(localObject5);
            if (bool5)
            {
              m = parama.b().intValue();
              localObject4 = Integer.valueOf(m);
              localObject4 = b((Integer)localObject4);
              label405:
              localObject4 = c((String)localObject4);
              break label1328;
            }
            localObject6 = "MM";
            bool5 = ((String)localObject6).equals(localObject5);
            if (bool5) {
              localObject4 = parama.b();
            }
            for (;;)
            {
              localObject4 = b(a(localObject4));
              break label1328;
              localObject6 = "M";
              bool5 = ((String)localObject6).equals(localObject5);
              if (bool5)
              {
                localObject4 = parama.b();
                break;
              }
              localObject6 = "DD";
              bool5 = ((String)localObject6).equals(localObject5);
              if (bool5)
              {
                localObject4 = parama.c();
              }
              else
              {
                localObject6 = "D";
                bool5 = ((String)localObject6).equals(localObject5);
                if (bool5)
                {
                  localObject4 = parama.c();
                  break;
                }
                localObject6 = "WWWW";
                bool5 = ((String)localObject6).equals(localObject5);
                if (bool5)
                {
                  m = parama.h().intValue();
                  localObject4 = Integer.valueOf(m);
                  localObject4 = e((Integer)localObject4);
                  break label1328;
                }
                localObject6 = "WWW";
                bool5 = ((String)localObject6).equals(localObject5);
                if (bool5)
                {
                  m = parama.h().intValue();
                  localObject4 = Integer.valueOf(m);
                  localObject4 = e((Integer)localObject4);
                  break label405;
                }
                localObject6 = "hh";
                bool5 = ((String)localObject6).equals(localObject5);
                if (bool5)
                {
                  localObject4 = parama.d();
                }
                else
                {
                  localObject6 = "h";
                  bool5 = ((String)localObject6).equals(localObject5);
                  if (bool5)
                  {
                    localObject4 = parama.d();
                    break;
                  }
                  localObject6 = "h12";
                  bool5 = ((String)localObject6).equals(localObject5);
                  if (bool5)
                  {
                    localObject4 = h(parama.d());
                    break;
                  }
                  localObject6 = "hh12";
                  bool5 = ((String)localObject6).equals(localObject5);
                  if (bool5)
                  {
                    localObject4 = h(parama.d());
                  }
                  else
                  {
                    localObject6 = "a";
                    bool5 = ((String)localObject6).equals(localObject5);
                    if (bool5)
                    {
                      n = parama.d().intValue();
                      localObject5 = Integer.valueOf(n);
                      localObject6 = "";
                      if (localObject5 != null)
                      {
                        localObject6 = h;
                        i5 = 12;
                        if (localObject6 != null)
                        {
                          n = ((Integer)localObject5).intValue();
                          if (n < i5)
                          {
                            localObject5 = h.c;
                            localObject4 = ((List)localObject5).get(0);
                          }
                          else
                          {
                            localObject4 = h.c.get(i2);
                          }
                          localObject4 = (String)localObject4;
                          break label1328;
                        }
                        localObject6 = d;
                        if (localObject6 != null)
                        {
                          Object localObject7 = g;
                          bool5 = ((Map)localObject7).containsKey(localObject6);
                          if (!bool5)
                          {
                            localObject6 = new java/util/ArrayList;
                            ((ArrayList)localObject6).<init>();
                            localObject7 = Integer.valueOf(6);
                            localObject7 = i((Integer)localObject7);
                            ((List)localObject6).add(localObject7);
                            i6 = 18;
                            localObject7 = Integer.valueOf(i6);
                            localObject7 = i((Integer)localObject7);
                            ((List)localObject6).add(localObject7);
                            localObject7 = g;
                            Locale localLocale = d;
                            ((Map)localObject7).put(localLocale, localObject6);
                          }
                          n = ((Integer)localObject5).intValue();
                          if (n < i5)
                          {
                            localObject5 = g;
                            localObject8 = d;
                            localObject5 = (List)((Map)localObject5).get(localObject8);
                            localObject4 = ((List)localObject5).get(0);
                          }
                          else
                          {
                            localObject4 = g;
                            localObject5 = d;
                            localObject4 = ((List)((Map)localObject4).get(localObject5)).get(i2);
                          }
                          localObject4 = (String)localObject4;
                          break label1328;
                        }
                        parama = new java/lang/IllegalArgumentException;
                        localObject1 = new java/lang/StringBuilder;
                        ((StringBuilder)localObject1).<init>("Your date pattern requires either a Locale, or your own custom localizations for text:");
                        localObject2 = f.a(c);
                        ((StringBuilder)localObject1).append((String)localObject2);
                        localObject1 = ((StringBuilder)localObject1).toString();
                        parama.<init>((String)localObject1);
                        throw parama;
                      }
                      localObject4 = localObject6;
                      break label1328;
                    }
                    localObject8 = "mm";
                    bool3 = ((String)localObject8).equals(localObject5);
                    if (bool3)
                    {
                      localObject4 = parama.e();
                    }
                    else
                    {
                      localObject8 = "m";
                      bool3 = ((String)localObject8).equals(localObject5);
                      if (bool3)
                      {
                        localObject4 = parama.e();
                        break;
                      }
                      localObject8 = "ss";
                      bool3 = ((String)localObject8).equals(localObject5);
                      if (!bool3) {
                        break label1202;
                      }
                      localObject4 = parama.f();
                    }
                  }
                }
              }
            }
            label1202:
            localObject8 = "s";
            bool3 = ((String)localObject8).equals(localObject5);
            if (!bool3) {
              break;
            }
            localObject4 = parama.f();
          }
          Object localObject8 = "f";
          boolean bool3 = ((String)localObject5).startsWith((String)localObject8);
          if (!bool3) {
            break label1379;
          }
          localObject8 = j.matcher((CharSequence)localObject5);
          bool3 = ((Matcher)localObject8).matches();
          if (!bool3) {
            break;
          }
          localObject8 = a(parama.g());
          n = ((String)localObject5).length();
          bool5 = f.a((String)localObject8);
          if (bool5)
          {
            int i4 = ((String)localObject8).length();
            if (i4 >= n)
            {
              localObject4 = ((String)localObject8).substring(0, n);
              break label1328;
            }
          }
          localObject4 = localObject8;
          label1328:
          c = ((String)localObject4);
          localObject4 = a;
          ((Collection)localObject4).add(localc);
        }
      }
      parama = new java/lang/IllegalArgumentException;
      localObject1 = String.valueOf(localObject5);
      localObject1 = "Unknown token in date formatting pattern: ".concat((String)localObject1);
      parama.<init>((String)localObject1);
      throw parama;
      label1379:
      parama = new java/lang/IllegalArgumentException;
      localObject1 = String.valueOf(localObject5);
      localObject1 = "Unknown token in date formatting pattern: ".concat((String)localObject1);
      parama.<init>((String)localObject1);
      throw parama;
      label1404:
      localObject3 = a(str);
      localObject1 = ((String)localObject1).replace(str, (CharSequence)localObject3);
    }
  }
  
  final String b()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    int m = 0;
    String str1 = null;
    for (;;)
    {
      String str2 = c;
      int n = str2.length();
      if (m >= n) {
        break;
      }
      str2 = c;
      int i1 = m + 1;
      str2 = str2.substring(m, i1);
      Object localObject = a(m);
      if (localObject != null)
      {
        str1 = c;
        localStringBuilder.append(str1);
        m = b;
      }
      else
      {
        localObject = "|";
        boolean bool = ((String)localObject).equals(str2);
        if (!bool) {
          localStringBuilder.append(str2);
        }
      }
      m += 1;
    }
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     a.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */