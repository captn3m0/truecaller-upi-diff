package a.a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public final class a
  implements Serializable, Comparable
{
  private static int k = 2400000;
  public Integer a;
  public Integer b;
  private String c;
  private Integer d;
  private Integer e;
  private Integer f;
  private Integer g;
  private Integer h;
  private boolean i = true;
  private int j;
  
  public a(Integer paramInteger1, Integer paramInteger2, Integer paramInteger3, Integer paramInteger4, Integer paramInteger5, Integer paramInteger6, Integer paramInteger7)
  {
    a = paramInteger1;
    b = paramInteger2;
    d = paramInteger3;
    e = paramInteger4;
    f = paramInteger5;
    g = paramInteger6;
    h = paramInteger7;
    n();
  }
  
  public static a a(TimeZone paramTimeZone)
  {
    long l = System.currentTimeMillis();
    Object localObject1 = new java/util/GregorianCalendar;
    ((GregorianCalendar)localObject1).<init>(paramTimeZone);
    ((Calendar)localObject1).setTimeInMillis(l);
    int m = 1;
    int n = ((Calendar)localObject1).get(m);
    int i1 = ((Calendar)localObject1).get(2) + m;
    m = ((Calendar)localObject1).get(5);
    int i2 = ((Calendar)localObject1).get(11);
    int i3 = ((Calendar)localObject1).get(12);
    int i4 = ((Calendar)localObject1).get(13);
    int i5 = 14;
    int i6 = ((Calendar)localObject1).get(i5) * 1000 * 1000;
    a locala = new a/a/a;
    Integer localInteger1 = Integer.valueOf(n);
    Integer localInteger2 = Integer.valueOf(i1);
    Integer localInteger3 = Integer.valueOf(m);
    Integer localInteger4 = Integer.valueOf(i2);
    Integer localInteger5 = Integer.valueOf(i3);
    Integer localInteger6 = Integer.valueOf(i4);
    Integer localInteger7 = Integer.valueOf(i6);
    Object localObject2 = locala;
    locala.<init>(localInteger1, localInteger2, localInteger3, localInteger4, localInteger5, localInteger6, localInteger7);
    paramTimeZone = a.d.c;
    locala.l();
    a.d locald = a.d.g;
    if (locald != paramTimeZone)
    {
      locald = a.d.f;
      Integer localInteger8;
      Integer localInteger9;
      Integer localInteger10;
      if (locald == paramTimeZone)
      {
        paramTimeZone = new a/a/a;
        localObject1 = a;
        localInteger8 = b;
        localInteger9 = d;
        localInteger10 = e;
        localObject2 = f;
        localInteger1 = g;
        localInteger2 = null;
        paramTimeZone.<init>((Integer)localObject1, localInteger8, localInteger9, localInteger10, (Integer)localObject2, localInteger1, null);
      }
      else
      {
        locald = a.d.e;
        if (locald == paramTimeZone)
        {
          paramTimeZone = new a/a/a;
          localObject1 = a;
          localInteger8 = b;
          localInteger9 = d;
          localInteger10 = e;
          localObject2 = f;
          localInteger1 = null;
          localInteger2 = null;
          paramTimeZone.<init>((Integer)localObject1, localInteger8, localInteger9, localInteger10, (Integer)localObject2, null, null);
        }
        else
        {
          locald = a.d.d;
          if (locald == paramTimeZone)
          {
            paramTimeZone = new a/a/a;
            localObject1 = a;
            localInteger8 = b;
            localInteger9 = d;
            localInteger10 = e;
            i5 = 0;
            localObject2 = null;
            localInteger1 = null;
            localInteger2 = null;
            paramTimeZone.<init>((Integer)localObject1, localInteger8, localInteger9, localInteger10, null, null, null);
          }
          else
          {
            locald = a.d.c;
            if (locald == paramTimeZone)
            {
              paramTimeZone = new a/a/a;
              localObject1 = a;
              localInteger8 = b;
              localInteger9 = d;
              i4 = 0;
              localInteger10 = null;
              i5 = 0;
              localObject2 = null;
              localInteger1 = null;
              localInteger2 = null;
              paramTimeZone.<init>((Integer)localObject1, localInteger8, localInteger9, null, null, null, null);
            }
            else
            {
              locald = a.d.b;
              if (locald == paramTimeZone)
              {
                paramTimeZone = new a/a/a;
                localObject1 = a;
                localInteger8 = b;
                i3 = 0;
                localInteger9 = null;
                i4 = 0;
                localInteger10 = null;
                i5 = 0;
                localObject2 = null;
                localInteger1 = null;
                localInteger2 = null;
                paramTimeZone.<init>((Integer)localObject1, localInteger8, null, null, null, null, null);
              }
              else
              {
                locald = a.d.a;
                if (locald == paramTimeZone)
                {
                  paramTimeZone = new a/a/a;
                  localObject1 = a;
                  i2 = 0;
                  localInteger8 = null;
                  i3 = 0;
                  localInteger9 = null;
                  i4 = 0;
                  localInteger10 = null;
                  i5 = 0;
                  localObject2 = null;
                  localInteger1 = null;
                  localInteger2 = null;
                  paramTimeZone.<init>((Integer)localObject1, null, null, null, null, null, null);
                }
                else
                {
                  m = 0;
                  paramTimeZone = null;
                }
              }
            }
          }
        }
      }
      return paramTimeZone;
    }
    paramTimeZone = new java/lang/IllegalArgumentException;
    paramTimeZone.<init>("It makes no sense to truncate to nanosecond precision, since that's the highest precision available.");
    throw paramTimeZone;
  }
  
  static Integer a(Integer paramInteger1, Integer paramInteger2)
  {
    if ((paramInteger1 != null) && (paramInteger2 != null))
    {
      int m = paramInteger2.intValue();
      int n = 1;
      int i1 = 31;
      if (m == n) {}
      do
      {
        do
        {
          paramInteger1 = Integer.valueOf(i1);
          break;
          m = paramInteger2.intValue();
          n = 2;
          if (m == n)
          {
            boolean bool = b(paramInteger1);
            if (bool) {
              i2 = 29;
            } else {
              i2 = 28;
            }
            paramInteger1 = Integer.valueOf(i2);
            break;
          }
          i2 = paramInteger2.intValue();
          m = 3;
        } while (i2 == m);
        i2 = paramInteger2.intValue();
        m = 4;
        n = 30;
        if (i2 == m) {}
        do
        {
          do
          {
            do
            {
              return Integer.valueOf(n);
              i2 = paramInteger2.intValue();
              m = 5;
              if (i2 == m) {
                break;
              }
              i2 = paramInteger2.intValue();
              m = 6;
            } while (i2 == m);
            i2 = paramInteger2.intValue();
            m = 7;
            if (i2 == m) {
              break;
            }
            i2 = paramInteger2.intValue();
            m = 8;
            if (i2 == m) {
              break;
            }
            i2 = paramInteger2.intValue();
            m = 9;
          } while (i2 == m);
          i2 = paramInteger2.intValue();
          m = 10;
          if (i2 == m) {
            break;
          }
          i2 = paramInteger2.intValue();
          m = 11;
        } while (i2 == m);
        i2 = paramInteger2.intValue();
        m = 12;
      } while (i2 == m);
      paramInteger1 = new java/lang/AssertionError;
      paramInteger2 = String.valueOf(paramInteger2);
      paramInteger2 = "Month is out of range 1..12:".concat(paramInteger2);
      paramInteger1.<init>(paramInteger2);
      throw paramInteger1;
    }
    int i2 = 0;
    paramInteger1 = null;
    return paramInteger1;
  }
  
  private static void a(Integer paramInteger, int paramInt1, int paramInt2, String paramString)
  {
    if (paramInteger != null)
    {
      int m = paramInteger.intValue();
      if (m >= paramInt1)
      {
        m = paramInteger.intValue();
        if (m <= paramInt2) {}
      }
      else
      {
        a.b localb = new a/a/a$b;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append(paramString);
        localStringBuilder.append(" is not in the range ");
        localStringBuilder.append(paramInt1);
        localStringBuilder.append("..");
        localStringBuilder.append(paramInt2);
        localStringBuilder.append(". Value is:");
        localStringBuilder.append(paramInteger);
        paramInteger = localStringBuilder.toString();
        localb.<init>(paramInteger);
        throw localb;
      }
    }
  }
  
  private static void a(Integer paramInteger1, Integer paramInteger2, Integer paramInteger3)
  {
    boolean bool = b(paramInteger1, paramInteger2, paramInteger3);
    if (bool)
    {
      int m = paramInteger3.intValue();
      Object localObject = a(paramInteger1, paramInteger2);
      int n = ((Integer)localObject).intValue();
      if (m > n)
      {
        a.b localb = new a/a/a$b;
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>("The day-of-the-month value '");
        ((StringBuilder)localObject).append(paramInteger3);
        ((StringBuilder)localObject).append("' exceeds the number of days in the month: ");
        paramInteger1 = a(paramInteger1, paramInteger2);
        ((StringBuilder)localObject).append(paramInteger1);
        paramInteger1 = ((StringBuilder)localObject).toString();
        localb.<init>(paramInteger1);
        throw localb;
      }
    }
  }
  
  private static void a(String paramString, Object paramObject, StringBuilder paramStringBuilder)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramString);
    localStringBuilder.append(":");
    paramString = String.valueOf(paramObject);
    localStringBuilder.append(paramString);
    localStringBuilder.append(" ");
    paramString = localStringBuilder.toString();
    paramStringBuilder.append(paramString);
  }
  
  private static boolean a(Object... paramVarArgs)
  {
    boolean bool = false;
    int m = 0;
    for (;;)
    {
      int n = 3;
      if (m >= n) {
        break;
      }
      Object localObject = paramVarArgs[m];
      if (localObject == null) {
        return bool;
      }
      m += 1;
    }
    bool = true;
    return bool;
  }
  
  private static boolean b(Integer paramInteger)
  {
    int m = paramInteger.intValue() % 100;
    boolean bool = true;
    int n;
    if (m == 0)
    {
      n = paramInteger.intValue() % 400;
      if (n == 0) {
        return bool;
      }
    }
    else
    {
      n = paramInteger.intValue() % 4;
      if (n == 0) {
        return bool;
      }
    }
    bool = false;
    return bool;
  }
  
  private static boolean b(Integer paramInteger1, Integer paramInteger2, Integer paramInteger3)
  {
    Object[] arrayOfObject = new Object[3];
    arrayOfObject[0] = paramInteger1;
    arrayOfObject[1] = paramInteger2;
    arrayOfObject[2] = paramInteger3;
    return a(arrayOfObject);
  }
  
  private int c(a parama)
  {
    if (this == parama) {
      return 0;
    }
    l();
    parama.l();
    e.a locala = e.a.a;
    Integer localInteger1 = a;
    Integer localInteger2 = a;
    int m = e.a(localInteger1, localInteger2, locala);
    if (m != 0) {
      return m;
    }
    localInteger1 = b;
    localInteger2 = b;
    m = e.a(localInteger1, localInteger2, locala);
    if (m != 0) {
      return m;
    }
    localInteger1 = d;
    localInteger2 = d;
    m = e.a(localInteger1, localInteger2, locala);
    if (m != 0) {
      return m;
    }
    localInteger1 = e;
    localInteger2 = e;
    m = e.a(localInteger1, localInteger2, locala);
    if (m != 0) {
      return m;
    }
    localInteger1 = f;
    localInteger2 = f;
    m = e.a(localInteger1, localInteger2, locala);
    if (m != 0) {
      return m;
    }
    localInteger1 = g;
    localInteger2 = g;
    m = e.a(localInteger1, localInteger2, locala);
    if (m != 0) {
      return m;
    }
    localInteger1 = h;
    parama = h;
    int n = e.a(localInteger1, parama, locala);
    if (n != 0) {
      return n;
    }
    return 0;
  }
  
  private boolean k()
  {
    a.d[] arrayOfd = new a.d[3];
    a.d locald = a.d.a;
    arrayOfd[0] = locald;
    locald = a.d.b;
    arrayOfd[1] = locald;
    locald = a.d.c;
    arrayOfd[2] = locald;
    return a(arrayOfd);
  }
  
  private void l()
  {
    boolean bool = i;
    if (!bool) {
      o();
    }
  }
  
  private int m()
  {
    int m = a.intValue();
    int n = b.intValue();
    int i1 = d.intValue();
    int i2 = m + 4800;
    int i3 = (n + -14) / 12;
    i2 = (i2 + i3) * 1461 / 4;
    n += -2;
    int i4 = i3 * 12;
    n = (n - i4) * 367 / 12;
    i2 += n;
    m = (m + 4900 + i3) / 100 * 3 / 4;
    return i2 - m + i1 + 33461;
  }
  
  private void n()
  {
    Integer localInteger1 = a;
    int m = 1;
    a(localInteger1, m, 9999, "Year");
    a(b, m, 12, "Month");
    a(d, m, 31, "Day");
    a(e, 0, 23, "Hour");
    localInteger1 = f;
    int n = 59;
    a(localInteger1, 0, n, "Minute");
    a(g, 0, n, "Second");
    a(h, 0, 999999999, "Nanosecond");
    localInteger1 = a;
    Integer localInteger2 = b;
    Integer localInteger3 = d;
    a(localInteger1, localInteger2, localInteger3);
  }
  
  private void o()
  {
    Object localObject1 = new a/a/d;
    ((d)localObject1).<init>();
    Object localObject2 = c;
    localObject1 = ((d)localObject1).a((String)localObject2);
    localObject2 = a;
    a = ((Integer)localObject2);
    localObject2 = b;
    b = ((Integer)localObject2);
    localObject2 = d;
    d = ((Integer)localObject2);
    localObject2 = e;
    e = ((Integer)localObject2);
    localObject2 = f;
    f = ((Integer)localObject2);
    localObject2 = g;
    g = ((Integer)localObject2);
    localObject1 = h;
    h = ((Integer)localObject1);
    n();
  }
  
  private Object[] p()
  {
    Object[] arrayOfObject = new Object[7];
    Integer localInteger = a;
    arrayOfObject[0] = localInteger;
    localInteger = b;
    arrayOfObject[1] = localInteger;
    localInteger = d;
    arrayOfObject[2] = localInteger;
    localInteger = e;
    arrayOfObject[3] = localInteger;
    localInteger = f;
    arrayOfObject[4] = localInteger;
    localInteger = g;
    arrayOfObject[5] = localInteger;
    localInteger = h;
    arrayOfObject[6] = localInteger;
    return arrayOfObject;
  }
  
  private String q()
  {
    boolean bool1 = true;
    a.d[] arrayOfd = new a.d[bool1];
    a.d locald1 = a.d.a;
    arrayOfd[0] = locald1;
    boolean bool2 = a(arrayOfd);
    int m = 6;
    int n = 5;
    int i1 = 4;
    int i2 = 3;
    int i3 = 2;
    String str;
    if (bool2)
    {
      arrayOfd = new a.d[m];
      locald2 = a.d.b;
      arrayOfd[0] = locald2;
      locald2 = a.d.c;
      arrayOfd[bool1] = locald2;
      locald2 = a.d.d;
      arrayOfd[i3] = locald2;
      locald2 = a.d.e;
      arrayOfd[i2] = locald2;
      locald2 = a.d.f;
      arrayOfd[i1] = locald2;
      locald2 = a.d.g;
      arrayOfd[n] = locald2;
      bool2 = b(arrayOfd);
      if (bool2)
      {
        str = "YYYY";
        break label1124;
      }
    }
    arrayOfd = new a.d[i3];
    a.d locald2 = a.d.a;
    arrayOfd[0] = locald2;
    locald2 = a.d.b;
    arrayOfd[bool1] = locald2;
    bool2 = a(arrayOfd);
    if (bool2)
    {
      arrayOfd = new a.d[n];
      locald2 = a.d.c;
      arrayOfd[0] = locald2;
      locald2 = a.d.d;
      arrayOfd[bool1] = locald2;
      locald2 = a.d.e;
      arrayOfd[i3] = locald2;
      locald2 = a.d.f;
      arrayOfd[i2] = locald2;
      locald2 = a.d.g;
      arrayOfd[i1] = locald2;
      bool2 = b(arrayOfd);
      if (bool2)
      {
        str = "YYYY-MM";
        break label1124;
      }
    }
    arrayOfd = new a.d[i2];
    locald2 = a.d.a;
    arrayOfd[0] = locald2;
    locald2 = a.d.b;
    arrayOfd[bool1] = locald2;
    locald2 = a.d.c;
    arrayOfd[i3] = locald2;
    bool2 = a(arrayOfd);
    if (bool2)
    {
      arrayOfd = new a.d[i1];
      locald2 = a.d.d;
      arrayOfd[0] = locald2;
      locald2 = a.d.e;
      arrayOfd[bool1] = locald2;
      locald2 = a.d.f;
      arrayOfd[i3] = locald2;
      locald2 = a.d.g;
      arrayOfd[i2] = locald2;
      bool2 = b(arrayOfd);
      if (bool2)
      {
        str = "YYYY-MM-DD";
        break label1124;
      }
    }
    arrayOfd = new a.d[i1];
    locald2 = a.d.a;
    arrayOfd[0] = locald2;
    locald2 = a.d.b;
    arrayOfd[bool1] = locald2;
    locald2 = a.d.c;
    arrayOfd[i3] = locald2;
    locald2 = a.d.d;
    arrayOfd[i2] = locald2;
    bool2 = a(arrayOfd);
    if (bool2)
    {
      arrayOfd = new a.d[i2];
      locald2 = a.d.e;
      arrayOfd[0] = locald2;
      locald2 = a.d.f;
      arrayOfd[bool1] = locald2;
      locald2 = a.d.g;
      arrayOfd[i3] = locald2;
      bool2 = b(arrayOfd);
      if (bool2)
      {
        str = "YYYY-MM-DD hh";
        break label1124;
      }
    }
    arrayOfd = new a.d[n];
    locald2 = a.d.a;
    arrayOfd[0] = locald2;
    locald2 = a.d.b;
    arrayOfd[bool1] = locald2;
    locald2 = a.d.c;
    arrayOfd[i3] = locald2;
    locald2 = a.d.d;
    arrayOfd[i2] = locald2;
    locald2 = a.d.e;
    arrayOfd[i1] = locald2;
    bool2 = a(arrayOfd);
    if (bool2)
    {
      arrayOfd = new a.d[i3];
      locald2 = a.d.f;
      arrayOfd[0] = locald2;
      locald2 = a.d.g;
      arrayOfd[bool1] = locald2;
      bool2 = b(arrayOfd);
      if (bool2)
      {
        str = "YYYY-MM-DD hh:mm";
        break label1124;
      }
    }
    arrayOfd = new a.d[m];
    locald2 = a.d.a;
    arrayOfd[0] = locald2;
    locald2 = a.d.b;
    arrayOfd[bool1] = locald2;
    locald2 = a.d.c;
    arrayOfd[i3] = locald2;
    locald2 = a.d.d;
    arrayOfd[i2] = locald2;
    locald2 = a.d.e;
    arrayOfd[i1] = locald2;
    locald2 = a.d.f;
    arrayOfd[n] = locald2;
    bool2 = a(arrayOfd);
    if (bool2)
    {
      arrayOfd = new a.d[bool1];
      locald2 = a.d.g;
      arrayOfd[0] = locald2;
      bool2 = b(arrayOfd);
      if (bool2)
      {
        str = "YYYY-MM-DD hh:mm:ss";
        break label1124;
      }
    }
    arrayOfd = new a.d[7];
    locald2 = a.d.a;
    arrayOfd[0] = locald2;
    locald2 = a.d.b;
    arrayOfd[bool1] = locald2;
    locald2 = a.d.c;
    arrayOfd[i3] = locald2;
    locald2 = a.d.d;
    arrayOfd[i2] = locald2;
    locald2 = a.d.e;
    arrayOfd[i1] = locald2;
    locald2 = a.d.f;
    arrayOfd[n] = locald2;
    locald2 = a.d.g;
    arrayOfd[m] = locald2;
    bool2 = a(arrayOfd);
    if (bool2)
    {
      str = "YYYY-MM-DD hh:mm:ss.fffffffff";
    }
    else
    {
      arrayOfd = new a.d[i2];
      locald1 = a.d.a;
      arrayOfd[0] = locald1;
      locald1 = a.d.b;
      arrayOfd[bool1] = locald1;
      locald1 = a.d.c;
      arrayOfd[i3] = locald1;
      bool2 = b(arrayOfd);
      if (bool2)
      {
        arrayOfd = new a.d[i1];
        locald1 = a.d.d;
        arrayOfd[0] = locald1;
        locald1 = a.d.e;
        arrayOfd[bool1] = locald1;
        locald1 = a.d.f;
        arrayOfd[i3] = locald1;
        locald1 = a.d.g;
        arrayOfd[i2] = locald1;
        bool2 = a(arrayOfd);
        if (bool2)
        {
          str = "hh:mm:ss.fffffffff";
          break label1124;
        }
      }
      arrayOfd = new a.d[i1];
      locald1 = a.d.a;
      arrayOfd[0] = locald1;
      locald1 = a.d.b;
      arrayOfd[bool1] = locald1;
      locald1 = a.d.c;
      arrayOfd[i3] = locald1;
      locald1 = a.d.g;
      arrayOfd[i2] = locald1;
      bool2 = b(arrayOfd);
      if (bool2)
      {
        arrayOfd = new a.d[i2];
        locald1 = a.d.d;
        arrayOfd[0] = locald1;
        locald1 = a.d.e;
        arrayOfd[bool1] = locald1;
        locald1 = a.d.f;
        arrayOfd[i3] = locald1;
        bool2 = a(arrayOfd);
        if (bool2)
        {
          str = "hh:mm:ss";
          break label1124;
        }
      }
      arrayOfd = new a.d[n];
      locald1 = a.d.a;
      arrayOfd[0] = locald1;
      locald1 = a.d.b;
      arrayOfd[bool1] = locald1;
      locald1 = a.d.c;
      arrayOfd[i3] = locald1;
      locald1 = a.d.f;
      arrayOfd[i2] = locald1;
      locald1 = a.d.g;
      arrayOfd[i1] = locald1;
      bool2 = b(arrayOfd);
      if (bool2)
      {
        arrayOfd = new a.d[i3];
        locald1 = a.d.d;
        arrayOfd[0] = locald1;
        locald1 = a.d.e;
        arrayOfd[bool1] = locald1;
        bool1 = a(arrayOfd);
        if (bool1)
        {
          str = "hh:mm";
          break label1124;
        }
      }
      bool1 = false;
      str = null;
    }
    label1124:
    return str;
  }
  
  public final a a(Integer paramInteger)
  {
    j();
    j();
    int m = m() + -1;
    int n = k;
    m = m - n + 1 + n;
    n = paramInteger.intValue();
    m = m + n + 68569;
    n = m * 4;
    int i1 = 146097;
    n /= i1;
    i1 = (i1 * n + 3) / 4;
    m -= i1;
    i1 = (m + 1) * 4000 / 1461001;
    int i2 = i1 * 1461 / 4;
    m = m - i2 + 31;
    i2 = m * 80 / 2447;
    int i3 = i2 * 2447 / 80;
    m -= i3;
    i3 = i2 / 11;
    i2 += 2;
    int i4 = i3 * 12;
    i2 -= i4;
    Integer localInteger1 = Integer.valueOf((n + -49) * 100 + i1 + i3);
    Integer localInteger2 = Integer.valueOf(i2);
    Integer localInteger3 = Integer.valueOf(m);
    Object localObject1 = new a/a/a;
    Object localObject2 = localObject1;
    ((a)localObject1).<init>(localInteger1, localInteger2, localInteger3, null, null, null, null);
    a locala = new a/a/a;
    Integer localInteger4 = ((a)localObject1).a();
    Integer localInteger5 = ((a)localObject1).b();
    Integer localInteger6 = ((a)localObject1).c();
    localObject1 = e;
    Integer localInteger7 = f;
    Integer localInteger8 = g;
    localObject2 = h;
    locala.<init>(localInteger4, localInteger5, localInteger6, (Integer)localObject1, localInteger7, localInteger8, (Integer)localObject2);
    return locala;
  }
  
  public final a a(Integer paramInteger1, Integer paramInteger2, Integer paramInteger3, Integer paramInteger4, Integer paramInteger5, Integer paramInteger6, Integer paramInteger7, a.a parama)
  {
    c localc = new a/a/c;
    localc.<init>(this, parama);
    int m = paramInteger1.intValue();
    int n = paramInteger2.intValue();
    int i1 = paramInteger3.intValue();
    int i2 = paramInteger4.intValue();
    int i3 = paramInteger5.intValue();
    int i4 = paramInteger6.intValue();
    int i5 = paramInteger7.intValue();
    Object localObject1 = Integer.valueOf(m);
    Integer localInteger1 = Integer.valueOf(n);
    Integer localInteger2 = Integer.valueOf(i1);
    Integer localInteger3 = Integer.valueOf(i2);
    Integer localInteger4 = Integer.valueOf(i3);
    Integer localInteger5 = Integer.valueOf(i4);
    Integer localInteger6 = Integer.valueOf(i5);
    Object localObject2 = localObject1;
    localObject1 = localInteger6;
    return localc.a(true, (Integer)localObject2, localInteger1, localInteger2, localInteger3, localInteger4, localInteger5, localInteger6);
  }
  
  public final Integer a()
  {
    l();
    return a;
  }
  
  public final String a(String paramString)
  {
    b localb = new a/a/b;
    localb.<init>(paramString);
    paramString = new java/util/ArrayList;
    paramString.<init>();
    b = paramString;
    paramString = new java/util/ArrayList;
    paramString.<init>();
    a = paramString;
    localb.a();
    localb.a(this);
    return localb.b();
  }
  
  public final boolean a(a parama)
  {
    int m = c(parama);
    return m < 0;
  }
  
  public final boolean a(a.d... paramVarArgs)
  {
    l();
    int m = paramVarArgs.length;
    int n = 0;
    boolean bool = true;
    while (n < m)
    {
      a.d locald1 = paramVarArgs[n];
      a.d locald2 = a.d.g;
      Integer localInteger;
      if (locald2 == locald1) {
        if (bool)
        {
          localInteger = h;
          if (localInteger == null) {}
        }
      }
      for (;;)
      {
        bool = true;
        break;
        label127:
        label158:
        label189:
        label220:
        do
        {
          do
          {
            do
            {
              do
              {
                do
                {
                  do
                  {
                    do
                    {
                      do
                      {
                        do
                        {
                          do
                          {
                            do
                            {
                              do
                              {
                                bool = false;
                                localInteger = null;
                                break label251;
                                locald2 = a.d.f;
                                if (locald2 != locald1) {
                                  break;
                                }
                              } while (!bool);
                              localInteger = g;
                            } while (localInteger == null);
                            break;
                            locald2 = a.d.e;
                            if (locald2 != locald1) {
                              break label127;
                            }
                          } while (!bool);
                          localInteger = f;
                        } while (localInteger == null);
                        break;
                        locald2 = a.d.d;
                        if (locald2 != locald1) {
                          break label158;
                        }
                      } while (!bool);
                      localInteger = e;
                    } while (localInteger == null);
                    break;
                    locald2 = a.d.c;
                    if (locald2 != locald1) {
                      break label189;
                    }
                  } while (!bool);
                  localInteger = d;
                } while (localInteger == null);
                break;
                locald2 = a.d.b;
                if (locald2 != locald1) {
                  break label220;
                }
              } while (!bool);
              localInteger = b;
            } while (localInteger == null);
            break;
            locald2 = a.d.a;
            if (locald2 != locald1) {
              break label251;
            }
          } while (!bool);
          localInteger = a;
        } while (localInteger == null);
      }
      label251:
      n += 1;
    }
    return bool;
  }
  
  public final a b(Integer paramInteger1, Integer paramInteger2, Integer paramInteger3, Integer paramInteger4, Integer paramInteger5, Integer paramInteger6, Integer paramInteger7, a.a parama)
  {
    c localc = new a/a/c;
    localc.<init>(this, parama);
    int m = paramInteger1.intValue();
    int n = paramInteger2.intValue();
    int i1 = paramInteger3.intValue();
    int i2 = paramInteger4.intValue();
    int i3 = paramInteger5.intValue();
    int i4 = paramInteger6.intValue();
    int i5 = paramInteger7.intValue();
    Object localObject1 = Integer.valueOf(m);
    Integer localInteger1 = Integer.valueOf(n);
    Integer localInteger2 = Integer.valueOf(i1);
    Integer localInteger3 = Integer.valueOf(i2);
    Integer localInteger4 = Integer.valueOf(i3);
    Integer localInteger5 = Integer.valueOf(i4);
    Integer localInteger6 = Integer.valueOf(i5);
    Object localObject2 = localObject1;
    localObject1 = localInteger6;
    return localc.a(false, (Integer)localObject2, localInteger1, localInteger2, localInteger3, localInteger4, localInteger5, localInteger6);
  }
  
  public final Integer b()
  {
    l();
    return b;
  }
  
  public final boolean b(a parama)
  {
    int m = c(parama);
    return m > 0;
  }
  
  public final boolean b(a.d... paramVarArgs)
  {
    l();
    int m = paramVarArgs.length;
    int n = 0;
    boolean bool = true;
    while (n < m)
    {
      a.d locald1 = paramVarArgs[n];
      a.d locald2 = a.d.g;
      Integer localInteger;
      if (locald2 == locald1) {
        if (bool)
        {
          localInteger = h;
          if (localInteger != null) {}
        }
      }
      for (;;)
      {
        bool = true;
        break;
        label127:
        label158:
        label189:
        label220:
        do
        {
          do
          {
            do
            {
              do
              {
                do
                {
                  do
                  {
                    do
                    {
                      do
                      {
                        do
                        {
                          do
                          {
                            do
                            {
                              do
                              {
                                bool = false;
                                localInteger = null;
                                break label251;
                                locald2 = a.d.f;
                                if (locald2 != locald1) {
                                  break;
                                }
                              } while (!bool);
                              localInteger = g;
                            } while (localInteger != null);
                            break;
                            locald2 = a.d.e;
                            if (locald2 != locald1) {
                              break label127;
                            }
                          } while (!bool);
                          localInteger = f;
                        } while (localInteger != null);
                        break;
                        locald2 = a.d.d;
                        if (locald2 != locald1) {
                          break label158;
                        }
                      } while (!bool);
                      localInteger = e;
                    } while (localInteger != null);
                    break;
                    locald2 = a.d.c;
                    if (locald2 != locald1) {
                      break label189;
                    }
                  } while (!bool);
                  localInteger = d;
                } while (localInteger != null);
                break;
                locald2 = a.d.b;
                if (locald2 != locald1) {
                  break label220;
                }
              } while (!bool);
              localInteger = b;
            } while (localInteger != null);
            break;
            locald2 = a.d.a;
            if (locald2 != locald1) {
              break label251;
            }
          } while (!bool);
          localInteger = a;
        } while (localInteger != null);
      }
      label251:
      n += 1;
    }
    return bool;
  }
  
  public final Integer c()
  {
    l();
    return d;
  }
  
  public final Integer d()
  {
    l();
    return e;
  }
  
  public final Integer e()
  {
    l();
    return f;
  }
  
  public final boolean equals(Object paramObject)
  {
    l();
    Object localObject1;
    if (this == paramObject)
    {
      localObject1 = Boolean.TRUE;
    }
    else
    {
      localObject1 = getClass();
      boolean bool1 = ((Class)localObject1).isInstance(paramObject);
      if (!bool1)
      {
        localObject1 = Boolean.FALSE;
      }
      else
      {
        bool1 = false;
        localObject1 = null;
      }
    }
    if (localObject1 == null)
    {
      paramObject = (a)paramObject;
      ((a)paramObject).l();
      localObject1 = p();
      paramObject = ((a)paramObject).p();
      boolean bool2 = false;
      int m = 0;
      for (;;)
      {
        int n = 7;
        if (m >= n) {
          break label182;
        }
        Object localObject2 = localObject1[m];
        Object localObject3 = paramObject[m];
        boolean bool4 = e.a(localObject2);
        if (bool4) {
          break;
        }
        bool4 = e.a(localObject3);
        if (bool4) {
          break;
        }
        boolean bool3;
        if (localObject2 == null)
        {
          if (localObject3 == null)
          {
            n = 1;
          }
          else
          {
            n = 0;
            localObject2 = null;
          }
        }
        else {
          bool3 = localObject2.equals(localObject3);
        }
        if (!bool3) {
          break label185;
        }
        m += 1;
      }
      paramObject = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)paramObject).<init>("This method does not currently support arrays.");
      throw ((Throwable)paramObject);
      label182:
      bool2 = true;
      label185:
      localObject1 = Boolean.valueOf(bool2);
    }
    return ((Boolean)localObject1).booleanValue();
  }
  
  public final Integer f()
  {
    l();
    return g;
  }
  
  public final Integer g()
  {
    l();
    return h;
  }
  
  public final Integer h()
  {
    j();
    return Integer.valueOf((m() + 1) % 7 + 1);
  }
  
  public final int hashCode()
  {
    int m = j;
    if (m == 0)
    {
      l();
      Object[] arrayOfObject = p();
      int n = 23;
      int i1 = 0;
      for (;;)
      {
        int i2 = 7;
        if (i1 >= i2) {
          break;
        }
        Object localObject = arrayOfObject[i1];
        n = e.a(n, localObject);
        i1 += 1;
      }
      j = n;
    }
    return j;
  }
  
  public final int i()
  {
    j();
    Integer localInteger1 = a;
    Integer localInteger2 = b;
    return a(localInteger1, localInteger2).intValue();
  }
  
  public final void j()
  {
    l();
    boolean bool = k();
    if (bool) {
      return;
    }
    a.c localc = new a/a/a$c;
    localc.<init>("DateTime does not include year/month/day.");
    throw localc;
  }
  
  public final String toString()
  {
    Object localObject = c;
    boolean bool = f.a((String)localObject);
    if (bool)
    {
      localObject = c;
    }
    else
    {
      localObject = q();
      if (localObject != null)
      {
        localObject = q();
        localObject = a((String)localObject);
      }
      else
      {
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        Integer localInteger = a;
        a("Y", localInteger, (StringBuilder)localObject);
        localInteger = b;
        a("M", localInteger, (StringBuilder)localObject);
        localInteger = d;
        a("D", localInteger, (StringBuilder)localObject);
        localInteger = e;
        a("h", localInteger, (StringBuilder)localObject);
        localInteger = f;
        a("m", localInteger, (StringBuilder)localObject);
        localInteger = g;
        a("s", localInteger, (StringBuilder)localObject);
        String str = "f";
        localInteger = h;
        a(str, localInteger, (StringBuilder)localObject);
        localObject = ((StringBuilder)localObject).toString().trim();
      }
    }
    return (String)localObject;
  }
}

/* Location:
 * Qualified Name:     a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */