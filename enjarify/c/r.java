package c;

import c.g.a.a;
import c.g.b.k;
import java.io.Serializable;

final class r
  implements f, Serializable
{
  private a a;
  private volatile Object b;
  private final Object c;
  
  private r(a parama)
  {
    a = parama;
    parama = v.a;
    b = parama;
    c = this;
  }
  
  private final Object writeReplace()
  {
    d locald = new c/d;
    Object localObject = b();
    locald.<init>(localObject);
    return locald;
  }
  
  public final boolean a()
  {
    Object localObject = b;
    v localv = v.a;
    return localObject != localv;
  }
  
  public final Object b()
  {
    ??? = b;
    Object localObject2 = v.a;
    if (??? != localObject2) {
      return ???;
    }
    synchronized (c)
    {
      localObject2 = b;
      v localv = v.a;
      if (localObject2 == localv)
      {
        localObject2 = a;
        if (localObject2 == null) {
          k.a();
        }
        localObject2 = ((a)localObject2).invoke();
        b = localObject2;
        localv = null;
        a = null;
      }
      return localObject2;
    }
  }
  
  public final String toString()
  {
    boolean bool = a();
    if (bool) {
      return String.valueOf(b());
    }
    return "Lazy value not initialized yet.";
  }
}

/* Location:
 * Qualified Name:     c.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */