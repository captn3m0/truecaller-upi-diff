package c.n;

import c.a.m;
import c.k.i;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class k
  implements Serializable
{
  public static final k.a b;
  public final Pattern a;
  
  static
  {
    k.a locala = new c/n/k$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  public k(String paramString)
  {
    this(paramString);
  }
  
  public k(Pattern paramPattern)
  {
    a = paramPattern;
  }
  
  private final Object writeReplace()
  {
    k.b localb = new c/n/k$b;
    String str = a.pattern();
    c.g.b.k.a(str, "nativePattern.pattern()");
    int i = a.flags();
    localb.<init>(str, i);
    return localb;
  }
  
  public final String a(CharSequence paramCharSequence, String paramString)
  {
    c.g.b.k.b(paramCharSequence, "input");
    c.g.b.k.b(paramString, "replacement");
    paramCharSequence = a.matcher(paramCharSequence).replaceAll(paramString);
    c.g.b.k.a(paramCharSequence, "nativePattern.matcher(in…).replaceAll(replacement)");
    return paramCharSequence;
  }
  
  public final List a(CharSequence paramCharSequence, int paramInt)
  {
    c.g.b.k.b(paramCharSequence, "input");
    int i = 0;
    String str = null;
    int j = 1;
    int k;
    Matcher localMatcher;
    if (paramInt >= 0)
    {
      k = 1;
    }
    else
    {
      k = 0;
      localMatcher = null;
    }
    if (k != 0)
    {
      localMatcher = a.matcher(paramCharSequence);
      boolean bool2 = localMatcher.find();
      if ((bool2) && (paramInt != j))
      {
        ArrayList localArrayList = new java/util/ArrayList;
        int m = 10;
        if (paramInt > 0) {
          m = i.d(paramInt, m);
        }
        localArrayList.<init>(m);
        paramInt -= j;
        boolean bool1;
        do
        {
          j = localMatcher.start();
          str = paramCharSequence.subSequence(i, j).toString();
          localArrayList.add(str);
          i = localMatcher.end();
          if (paramInt >= 0)
          {
            j = localArrayList.size();
            if (j == paramInt) {
              break;
            }
          }
          bool1 = localMatcher.find();
        } while (bool1);
        paramInt = paramCharSequence.length();
        paramCharSequence = paramCharSequence.subSequence(i, paramInt).toString();
        localArrayList.add(paramCharSequence);
        return (List)localArrayList;
      }
      return m.a(paramCharSequence.toString());
    }
    paramCharSequence = new java/lang/StringBuilder;
    paramCharSequence.<init>("Limit must be non-negative, but was ");
    paramCharSequence.append(paramInt);
    paramCharSequence.append('.');
    paramCharSequence = paramCharSequence.toString();
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    paramCharSequence = paramCharSequence.toString();
    localIllegalArgumentException.<init>(paramCharSequence);
    throw ((Throwable)localIllegalArgumentException);
  }
  
  public final boolean a(CharSequence paramCharSequence)
  {
    c.g.b.k.b(paramCharSequence, "input");
    return a.matcher(paramCharSequence).matches();
  }
  
  public final String toString()
  {
    String str = a.toString();
    c.g.b.k.a(str, "nativePattern.toString()");
    return str;
  }
}

/* Location:
 * Qualified Name:     c.n.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */