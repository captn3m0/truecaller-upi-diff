package c.n;

import c.g.b.k;
import c.k.h;
import c.m.l;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class v
  extends u
{
  public static final int a(CharSequence paramCharSequence, char paramChar, int paramInt)
  {
    Object localObject = "receiver$0";
    k.b(paramCharSequence, (String)localObject);
    boolean bool = paramCharSequence instanceof String;
    if (!bool)
    {
      localObject = new char[1];
      localObject[0] = paramChar;
      return m.a(paramCharSequence, (char[])localObject, paramInt, false);
    }
    return ((String)paramCharSequence).indexOf(paramChar, paramInt);
  }
  
  public static final int a(CharSequence paramCharSequence1, CharSequence paramCharSequence2, int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
  {
    int i = 0;
    Object localObject1 = null;
    Object localObject2;
    if (!paramBoolean2)
    {
      paramInt1 = c.k.i.c(paramInt1, 0);
      localObject2 = new c/k/h;
      i = paramCharSequence1.length();
      paramInt2 = c.k.i.d(paramInt2, i);
      ((h)localObject2).<init>(paramInt1, paramInt2);
      localObject2 = (c.k.f)localObject2;
    }
    else
    {
      paramBoolean2 = m.d(paramCharSequence1);
      paramInt1 = c.k.i.d(paramInt1, paramBoolean2);
      paramInt2 = c.k.i.c(paramInt2, 0);
      localObject2 = c.k.i.a(paramInt1, paramInt2);
    }
    paramInt1 = paramCharSequence1 instanceof String;
    if (paramInt1 != 0)
    {
      paramInt1 = paramCharSequence2 instanceof String;
      if (paramInt1 != 0)
      {
        paramInt1 = a;
        paramInt2 = b;
        paramBoolean2 = c;
        if (paramBoolean2 ? paramInt1 > paramInt2 : paramInt1 < paramInt2) {
          break label279;
        }
        for (;;)
        {
          localObject1 = paramCharSequence2;
          localObject1 = (String)paramCharSequence2;
          Object localObject3 = paramCharSequence1;
          localObject3 = (String)paramCharSequence1;
          int k = ((CharSequence)paramCharSequence2).length();
          boolean bool1 = m.a((String)localObject1, 0, (String)localObject3, paramInt1, k, paramBoolean1);
          if (bool1) {
            return paramInt1;
          }
          if (paramInt1 == paramInt2) {
            break;
          }
          paramInt1 += paramBoolean2;
        }
      }
    }
    paramInt1 = a;
    paramInt2 = b;
    paramBoolean2 = c;
    if (paramBoolean2 ? paramInt1 <= paramInt2 : paramInt1 >= paramInt2) {
      for (;;)
      {
        int j = paramCharSequence2.length();
        boolean bool2 = m.a(paramCharSequence2, (CharSequence)paramCharSequence1, paramInt1, j, paramBoolean1);
        if (bool2) {
          return paramInt1;
        }
        if (paramInt1 == paramInt2) {
          break;
        }
        paramInt1 += paramBoolean2;
      }
    }
    label279:
    return -1;
  }
  
  public static final int a(CharSequence paramCharSequence, String paramString, int paramInt, boolean paramBoolean)
  {
    k.b(paramCharSequence, "receiver$0");
    String str = "string";
    k.b(paramString, str);
    if (!paramBoolean)
    {
      boolean bool = paramCharSequence instanceof String;
      if (bool) {
        return ((String)paramCharSequence).indexOf(paramString, paramInt);
      }
    }
    paramString = (CharSequence)paramString;
    int i = paramCharSequence.length();
    return b(paramCharSequence, paramString, paramInt, i, paramBoolean);
  }
  
  public static final int a(CharSequence paramCharSequence, char[] paramArrayOfChar, int paramInt, boolean paramBoolean)
  {
    k.b(paramCharSequence, "receiver$0");
    String str1 = "chars";
    k.b(paramArrayOfChar, str1);
    int i = 1;
    if (!paramBoolean)
    {
      int j = paramArrayOfChar.length;
      if (j == i)
      {
        boolean bool1 = paramCharSequence instanceof String;
        if (bool1)
        {
          String str2 = "receiver$0";
          k.b(paramArrayOfChar, str2);
          paramBoolean = paramArrayOfChar.length;
          switch (paramBoolean)
          {
          default: 
            paramCharSequence = new java/lang/IllegalArgumentException;
            paramCharSequence.<init>("Array has more than one element.");
            throw ((Throwable)paramCharSequence);
          case 1: 
            int m = paramArrayOfChar[0];
            return ((String)paramCharSequence).indexOf(m, paramInt);
          }
          paramCharSequence = new java/util/NoSuchElementException;
          paramCharSequence.<init>("Array is empty.");
          throw ((Throwable)paramCharSequence);
        }
      }
    }
    paramInt = c.k.i.c(paramInt, 0);
    int k = m.d(paramCharSequence);
    if (paramInt <= k) {
      for (;;)
      {
        char c = paramCharSequence.charAt(paramInt);
        int n = paramArrayOfChar.length;
        int i1 = 0;
        while (i1 < n)
        {
          boolean bool2 = a.a(paramArrayOfChar[i1], c, paramBoolean);
          if (bool2)
          {
            c = '\001';
            break label202;
          }
          i1 += 1;
        }
        c = '\000';
        label202:
        if (c != 0) {
          return paramInt;
        }
        if (paramInt == k) {
          break;
        }
        paramInt += 1;
      }
    }
    return -1;
  }
  
  static final c.m.i a(CharSequence paramCharSequence, String[] paramArrayOfString, boolean paramBoolean, int paramInt)
  {
    int i;
    e locale;
    if (paramInt >= 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      locale = null;
    }
    if (i != 0)
    {
      paramArrayOfString = c.a.f.a(paramArrayOfString);
      locale = new c/n/e;
      Object localObject = new c/n/v$b;
      ((v.b)localObject).<init>(paramArrayOfString, paramBoolean);
      localObject = (c.g.a.m)localObject;
      locale.<init>(paramCharSequence, paramInt, (c.g.a.m)localObject);
      return (c.m.i)locale;
    }
    paramCharSequence = new java/lang/StringBuilder;
    paramCharSequence.<init>("Limit must be non-negative, but was ");
    paramCharSequence.append(paramInt);
    paramCharSequence.append('.');
    paramCharSequence = paramCharSequence.toString();
    paramArrayOfString = new java/lang/IllegalArgumentException;
    paramCharSequence = paramCharSequence.toString();
    paramArrayOfString.<init>(paramCharSequence);
    throw ((Throwable)paramArrayOfString);
  }
  
  public static final CharSequence a(CharSequence paramCharSequence, int paramInt, char paramChar)
  {
    Object localObject = "receiver$0";
    k.b(paramCharSequence, (String)localObject);
    if (paramInt >= 0)
    {
      int i = paramCharSequence.length();
      if (paramInt <= i)
      {
        paramChar = paramCharSequence.length();
        return paramCharSequence.subSequence(0, paramChar);
      }
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>(paramInt);
      int j = paramCharSequence.length();
      paramInt -= j;
      if (paramInt > 0)
      {
        j = 1;
        for (;;)
        {
          ((StringBuilder)localObject).append(paramChar);
          if (j == paramInt) {
            break;
          }
          j += 1;
        }
      }
      ((StringBuilder)localObject).append(paramCharSequence);
      return (CharSequence)localObject;
    }
    paramCharSequence = new java/lang/IllegalArgumentException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Desired length ");
    localStringBuilder.append(paramInt);
    localStringBuilder.append(" is less than zero.");
    String str = localStringBuilder.toString();
    paramCharSequence.<init>(str);
    throw ((Throwable)paramCharSequence);
  }
  
  public static final String a(CharSequence paramCharSequence, h paramh)
  {
    k.b(paramCharSequence, "receiver$0");
    k.b(paramh, "range");
    int i = a;
    int j = b + 1;
    return paramCharSequence.subSequence(i, j).toString();
  }
  
  public static final String a(String paramString, int paramInt, char paramChar)
  {
    k.b(paramString, "receiver$0");
    return m.a((CharSequence)paramString, paramInt, paramChar).toString();
  }
  
  public static final String a(String paramString, CharSequence paramCharSequence)
  {
    k.b(paramString, "receiver$0");
    k.b(paramCharSequence, "prefix");
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    boolean bool = m.a((CharSequence)localObject, paramCharSequence);
    if (bool)
    {
      int i = paramCharSequence.length();
      paramString = paramString.substring(i);
      k.a(paramString, "(this as java.lang.String).substring(startIndex)");
      return paramString;
    }
    return paramString;
  }
  
  private static final List a(CharSequence paramCharSequence, String paramString, boolean paramBoolean, int paramInt)
  {
    int i = 1;
    int j = 0;
    String str = null;
    int k;
    if (paramInt >= 0) {
      k = 1;
    } else {
      k = 0;
    }
    if (k != 0)
    {
      k = m.a(paramCharSequence, paramString, 0, paramBoolean);
      int m = -1;
      if ((k != m) && (paramInt != i))
      {
        int n;
        if (paramInt > 0) {
          n = 1;
        } else {
          n = 0;
        }
        ArrayList localArrayList = new java/util/ArrayList;
        int i1 = 10;
        if (n != 0) {
          i1 = c.k.i.d(paramInt, i1);
        }
        localArrayList.<init>(i1);
        do
        {
          str = paramCharSequence.subSequence(j, k).toString();
          localArrayList.add(str);
          j = paramString.length() + k;
          if (n != 0)
          {
            k = localArrayList.size();
            i1 = paramInt + -1;
            if (k == i1) {
              break;
            }
          }
          k = m.a(paramCharSequence, paramString, j, paramBoolean);
        } while (k != m);
        int i2 = paramCharSequence.length();
        paramCharSequence = paramCharSequence.subSequence(j, i2).toString();
        localArrayList.add(paramCharSequence);
        return (List)localArrayList;
      }
      return c.a.m.a(paramCharSequence.toString());
    }
    paramCharSequence = new java/lang/StringBuilder;
    paramCharSequence.<init>("Limit must be non-negative, but was ");
    paramCharSequence.append(paramInt);
    paramCharSequence.append('.');
    paramCharSequence = paramCharSequence.toString();
    paramString = new java/lang/IllegalArgumentException;
    paramCharSequence = paramCharSequence.toString();
    paramString.<init>(paramCharSequence);
    throw ((Throwable)paramString);
  }
  
  public static final List a(CharSequence paramCharSequence, char[] paramArrayOfChar, int paramInt)
  {
    k.b(paramCharSequence, "receiver$0");
    Object localObject1 = "delimiters";
    k.b(paramArrayOfChar, (String)localObject1);
    int i = paramArrayOfChar.length;
    int j = 1;
    if (i == j)
    {
      paramArrayOfChar = String.valueOf(paramArrayOfChar[0]);
      return a(paramCharSequence, paramArrayOfChar, false, paramInt);
    }
    Object localObject2;
    if (paramInt < 0)
    {
      j = 0;
      localObject2 = null;
    }
    if (j != 0)
    {
      localObject1 = new c/n/e;
      localObject2 = new c/n/v$a;
      ((v.a)localObject2).<init>(paramArrayOfChar);
      localObject2 = (c.g.a.m)localObject2;
      ((e)localObject1).<init>(paramCharSequence, paramInt, (c.g.a.m)localObject2);
      localObject1 = (c.m.i)localObject1;
      paramArrayOfChar = l.f((c.m.i)localObject1);
      Object localObject3 = new java/util/ArrayList;
      i = c.a.m.a(paramArrayOfChar, 10);
      ((ArrayList)localObject3).<init>(i);
      localObject3 = (Collection)localObject3;
      paramArrayOfChar = paramArrayOfChar.iterator();
      for (;;)
      {
        boolean bool = paramArrayOfChar.hasNext();
        if (!bool) {
          break;
        }
        localObject1 = (h)paramArrayOfChar.next();
        localObject1 = m.a(paramCharSequence, (h)localObject1);
        ((Collection)localObject3).add(localObject1);
      }
      return (List)localObject3;
    }
    paramCharSequence = new java/lang/StringBuilder;
    paramCharSequence.<init>("Limit must be non-negative, but was ");
    paramCharSequence.append(paramInt);
    paramCharSequence.append('.');
    paramCharSequence = paramCharSequence.toString();
    paramArrayOfChar = new java/lang/IllegalArgumentException;
    paramCharSequence = paramCharSequence.toString();
    paramArrayOfChar.<init>(paramCharSequence);
    throw ((Throwable)paramArrayOfChar);
  }
  
  public static final boolean a(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
  {
    k.b(paramCharSequence1, "receiver$0");
    String str = "prefix";
    k.b(paramCharSequence2, str);
    boolean bool = paramCharSequence1 instanceof String;
    if (bool)
    {
      bool = paramCharSequence2 instanceof String;
      if (bool)
      {
        paramCharSequence1 = (String)paramCharSequence1;
        paramCharSequence2 = (String)paramCharSequence2;
        return m.b(paramCharSequence1, paramCharSequence2, false);
      }
    }
    int i = paramCharSequence2.length();
    return m.a(paramCharSequence1, paramCharSequence2, 0, i, false);
  }
  
  public static final boolean a(CharSequence paramCharSequence1, CharSequence paramCharSequence2, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    k.b(paramCharSequence1, "receiver$0");
    k.b(paramCharSequence2, "other");
    if (paramInt1 >= 0)
    {
      int i = paramCharSequence1.length() - paramInt2;
      if (i >= 0)
      {
        i = paramCharSequence2.length() - paramInt2;
        if (paramInt1 <= i)
        {
          i = 0;
          while (i < paramInt2)
          {
            int j = i + 0;
            j = paramCharSequence1.charAt(j);
            int k = paramInt1 + i;
            k = paramCharSequence2.charAt(k);
            boolean bool = a.a(j, k, paramBoolean);
            if (!bool) {
              return false;
            }
            i += 1;
          }
          return true;
        }
      }
    }
    return false;
  }
  
  public static final boolean a(CharSequence paramCharSequence1, CharSequence paramCharSequence2, boolean paramBoolean)
  {
    k.b(paramCharSequence1, "receiver$0");
    String str = "other";
    k.b(paramCharSequence2, str);
    boolean bool1 = paramCharSequence2 instanceof String;
    boolean bool2 = true;
    if (bool1)
    {
      paramCharSequence2 = (String)paramCharSequence2;
      i = 2;
      j = m.a(paramCharSequence1, paramCharSequence2, 0, paramBoolean, i);
      if (j >= 0) {
        return bool2;
      }
      return false;
    }
    int i = paramCharSequence1.length();
    int j = b(paramCharSequence1, paramCharSequence2, 0, i, paramBoolean);
    if (j >= 0) {
      return bool2;
    }
    return false;
  }
  
  public static final CharSequence b(CharSequence paramCharSequence)
  {
    String str = "receiver$0";
    k.b(paramCharSequence, str);
    int i = paramCharSequence.length();
    int j = 1;
    i -= j;
    int k = 0;
    int m = 0;
    while (k <= i)
    {
      int n;
      if (m == 0) {
        n = k;
      } else {
        n = i;
      }
      boolean bool = a.a(paramCharSequence.charAt(n));
      if (m == 0)
      {
        if (!bool) {
          m = 1;
        } else {
          k += 1;
        }
      }
      else
      {
        if (!bool) {
          break;
        }
        i += -1;
      }
    }
    i += j;
    return paramCharSequence.subSequence(k, i);
  }
  
  public static final List b(CharSequence paramCharSequence, String[] paramArrayOfString, boolean paramBoolean, int paramInt)
  {
    k.b(paramCharSequence, "receiver$0");
    String str1 = "delimiters";
    k.b(paramArrayOfString, str1);
    int i = paramArrayOfString.length;
    int j = 1;
    if (i == j)
    {
      i = 0;
      str1 = null;
      String str2 = paramArrayOfString[0];
      Object localObject1 = str2;
      localObject1 = (CharSequence)str2;
      int k = ((CharSequence)localObject1).length();
      if (k == 0) {
        i = 1;
      }
      if (i == 0) {
        return a(paramCharSequence, str2, paramBoolean, paramInt);
      }
    }
    paramArrayOfString = l.f(a(paramCharSequence, paramArrayOfString, paramBoolean, paramInt));
    Object localObject2 = new java/util/ArrayList;
    paramInt = c.a.m.a(paramArrayOfString, 10);
    ((ArrayList)localObject2).<init>(paramInt);
    localObject2 = (Collection)localObject2;
    paramArrayOfString = paramArrayOfString.iterator();
    for (;;)
    {
      paramInt = paramArrayOfString.hasNext();
      if (paramInt == 0) {
        break;
      }
      Object localObject3 = (h)paramArrayOfString.next();
      localObject3 = m.a(paramCharSequence, (h)localObject3);
      ((Collection)localObject2).add(localObject3);
    }
    return (List)localObject2;
  }
  
  public static final h c(CharSequence paramCharSequence)
  {
    k.b(paramCharSequence, "receiver$0");
    h localh = new c/k/h;
    int i = paramCharSequence.length() + -1;
    localh.<init>(0, i);
    return localh;
  }
  
  public static final String c(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "receiver$0");
    k.b(paramString2, "delimiter");
    k.b(paramString3, "missingDelimiterValue");
    Object localObject = paramString1;
    localObject = (CharSequence)paramString1;
    int i = 6;
    int j = m.a((CharSequence)localObject, paramString2, 0, false, i);
    int k = -1;
    if (j == k) {
      return paramString3;
    }
    paramString1 = paramString1.substring(0, j);
    k.a(paramString1, "(this as java.lang.Strin…ing(startIndex, endIndex)");
    return paramString1;
  }
  
  public static final int d(CharSequence paramCharSequence)
  {
    k.b(paramCharSequence, "receiver$0");
    return paramCharSequence.length() + -1;
  }
  
  public static final String d(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "receiver$0");
    k.b(paramString2, "delimiter");
    k.b(paramString3, "missingDelimiterValue");
    Object localObject = paramString1;
    localObject = (CharSequence)paramString1;
    int i = 6;
    int j = m.a((CharSequence)localObject, paramString2, 0, false, i);
    int k = -1;
    if (j == k) {
      return paramString3;
    }
    int m = paramString2.length();
    j += m;
    m = paramString1.length();
    paramString1 = paramString1.substring(j, m);
    k.a(paramString1, "(this as java.lang.Strin…ing(startIndex, endIndex)");
    return paramString1;
  }
}

/* Location:
 * Qualified Name:     c.n.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */