package c.n;

import c.g.b.k;
import c.k.h;

public final class f
{
  private final String a;
  private final h b;
  
  public f(String paramString, h paramh)
  {
    a = paramString;
    b = paramh;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof f;
      if (bool1)
      {
        paramObject = (f)paramObject;
        Object localObject = a;
        String str = a;
        bool1 = k.a(localObject, str);
        if (bool1)
        {
          localObject = b;
          paramObject = b;
          boolean bool2 = k.a(localObject, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final int hashCode()
  {
    String str = a;
    int i = 0;
    int j;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    h localh = b;
    if (localh != null) {
      i = localh.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("MatchGroup(value=");
    Object localObject = a;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", range=");
    localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     c.n.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */