package c.n;

public class c
  extends b
{
  public static final boolean a(char paramChar1, char paramChar2, boolean paramBoolean)
  {
    boolean bool1 = true;
    if (paramChar1 == paramChar2) {
      return bool1;
    }
    if (!paramBoolean) {
      return false;
    }
    paramBoolean = Character.toUpperCase(paramChar1);
    boolean bool2 = Character.toUpperCase(paramChar2);
    if (paramBoolean == bool2) {
      return bool1;
    }
    paramChar1 = Character.toLowerCase(paramChar1);
    paramChar2 = Character.toLowerCase(paramChar2);
    if (paramChar1 == paramChar2) {
      return bool1;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     c.n.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */