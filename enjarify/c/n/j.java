package c.n;

import c.g.b.k;
import java.util.List;
import java.util.regex.Matcher;

public final class j
  implements i
{
  final Matcher a;
  private final g b;
  private List c;
  private final CharSequence d;
  
  public j(Matcher paramMatcher, CharSequence paramCharSequence)
  {
    a = paramMatcher;
    d = paramCharSequence;
    paramMatcher = new c/n/j$b;
    paramMatcher.<init>(this);
    paramMatcher = (g)paramMatcher;
    b = paramMatcher;
  }
  
  public final List a()
  {
    Object localObject = c;
    if (localObject == null)
    {
      localObject = new c/n/j$a;
      ((j.a)localObject).<init>(this);
      localObject = (List)localObject;
      c = ((List)localObject);
    }
    localObject = c;
    if (localObject == null) {
      k.a();
    }
    return (List)localObject;
  }
}

/* Location:
 * Qualified Name:     c.n.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */