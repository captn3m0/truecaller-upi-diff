package c.n;

import c.g.b.k;
import java.nio.charset.Charset;

public final class d
{
  public static final Charset a;
  public static final Charset b;
  public static final Charset c;
  public static final Charset d;
  public static final Charset e;
  public static final Charset f;
  public static final d g;
  
  static
  {
    Object localObject = new c/n/d;
    ((d)localObject).<init>();
    g = (d)localObject;
    localObject = Charset.forName("UTF-8");
    k.a(localObject, "Charset.forName(\"UTF-8\")");
    a = (Charset)localObject;
    localObject = Charset.forName("UTF-16");
    k.a(localObject, "Charset.forName(\"UTF-16\")");
    b = (Charset)localObject;
    localObject = Charset.forName("UTF-16BE");
    k.a(localObject, "Charset.forName(\"UTF-16BE\")");
    c = (Charset)localObject;
    localObject = Charset.forName("UTF-16LE");
    k.a(localObject, "Charset.forName(\"UTF-16LE\")");
    d = (Charset)localObject;
    localObject = Charset.forName("US-ASCII");
    k.a(localObject, "Charset.forName(\"US-ASCII\")");
    e = (Charset)localObject;
    localObject = Charset.forName("ISO-8859-1");
    k.a(localObject, "Charset.forName(\"ISO-8859-1\")");
    f = (Charset)localObject;
  }
}

/* Location:
 * Qualified Name:     c.n.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */