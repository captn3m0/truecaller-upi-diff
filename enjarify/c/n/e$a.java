package c.n;

import c.g.b.a.a;
import c.k.h;
import c.k.i;
import c.n;
import java.util.Iterator;

public final class e$a
  implements a, Iterator
{
  private int b = -1;
  private int c;
  private int d;
  private h e;
  private int f;
  
  e$a(e parame)
  {
    int i = b;
    int j = a.length();
    j = i.a(i, 0, j);
    c = j;
    j = c;
    d = j;
  }
  
  private final void a()
  {
    int i = d;
    int j = 0;
    h localh = null;
    if (i < 0)
    {
      b = 0;
      e = null;
      return;
    }
    Object localObject1 = a;
    i = c;
    int k = -1;
    int m = 1;
    if (i > 0)
    {
      i = f + m;
      f = i;
      i = f;
      localObject2 = a;
      n = c;
      if (i >= n) {}
    }
    else
    {
      i = d;
      localObject2 = a.a;
      n = ((CharSequence)localObject2).length();
      if (i <= n) {
        break label158;
      }
    }
    i = c;
    localh = new c/k/h;
    Object localObject2 = a.a;
    int n = m.d((CharSequence)localObject2);
    localh.<init>(i, n);
    e = localh;
    d = k;
    break label346;
    label158:
    localObject1 = a.d;
    localObject2 = a.a;
    int i1 = d;
    Integer localInteger = Integer.valueOf(i1);
    localObject1 = (n)((c.g.a.m)localObject1).invoke(localObject2, localInteger);
    if (localObject1 == null)
    {
      i = c;
      localh = new c/k/h;
      localObject2 = a.a;
      n = m.d((CharSequence)localObject2);
      localh.<init>(i, n);
      e = localh;
      d = k;
    }
    else
    {
      Number localNumber = (Number)a;
      k = localNumber.intValue();
      localObject1 = (Number)b;
      i = ((Number)localObject1).intValue();
      n = c;
      localObject2 = i.b(n, k);
      e = ((h)localObject2);
      k += i;
      c = k;
      k = c;
      if (i == 0) {
        j = 1;
      }
      k += j;
      d = k;
    }
    label346:
    b = m;
  }
  
  public final boolean hasNext()
  {
    int i = b;
    int j = -1;
    if (i == j) {
      a();
    }
    i = b;
    j = 1;
    if (i == j) {
      return j;
    }
    return false;
  }
  
  public final void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
}

/* Location:
 * Qualified Name:     c.n.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */