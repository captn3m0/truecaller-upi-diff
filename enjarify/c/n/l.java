package c.n;

final class l
{
  public static final k a;
  public static final l b;
  
  static
  {
    Object localObject1 = new c/n/l;
    ((l)localObject1).<init>();
    b = (l)localObject1;
    localObject1 = "(\\p{Digit}+)";
    Object localObject2 = "(\\p{XDigit}+)";
    Object localObject3 = String.valueOf(localObject1);
    String str = "[eE][+-]?".concat((String)localObject3);
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("(0[xX]");
    ((StringBuilder)localObject3).append((String)localObject2);
    ((StringBuilder)localObject3).append("(\\.)?)|(0[xX]");
    ((StringBuilder)localObject3).append((String)localObject2);
    ((StringBuilder)localObject3).append("?(\\.)");
    ((StringBuilder)localObject3).append((String)localObject2);
    char c = ')';
    ((StringBuilder)localObject3).append(c);
    localObject3 = ((StringBuilder)localObject3).toString();
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("(");
    localStringBuilder.append((String)localObject1);
    localStringBuilder.append("(\\.)?(");
    localStringBuilder.append((String)localObject1);
    localStringBuilder.append("?)(");
    localStringBuilder.append(str);
    localStringBuilder.append(")?)|(\\.(");
    localStringBuilder.append((String)localObject1);
    localStringBuilder.append(")(");
    localStringBuilder.append(str);
    localStringBuilder.append(")?)|((");
    localStringBuilder.append((String)localObject3);
    localStringBuilder.append(")[pP][+-]?");
    localStringBuilder.append((String)localObject1);
    localStringBuilder.append(c);
    localObject1 = localStringBuilder.toString();
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("[\\x00-\\x20]*[+-]?(NaN|Infinity|((");
    ((StringBuilder)localObject2).append((String)localObject1);
    ((StringBuilder)localObject2).append(")[fFdD]?))[\\x00-\\x20]*");
    localObject1 = ((StringBuilder)localObject2).toString();
    localObject2 = new c/n/k;
    ((k)localObject2).<init>((String)localObject1);
    a = (k)localObject2;
  }
}

/* Location:
 * Qualified Name:     c.n.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */