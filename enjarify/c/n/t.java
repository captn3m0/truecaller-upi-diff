package c.n;

import c.g.b.k;

public class t
  extends s
{
  public static final Integer b(String paramString)
  {
    k.b(paramString, "receiver$0");
    return m.c(paramString);
  }
  
  public static final Integer c(String paramString)
  {
    String str = "receiver$0";
    k.b(paramString, str);
    int i = paramString.length();
    if (i == 0) {
      return null;
    }
    int j = 0;
    int k = paramString.charAt(0);
    int m = 48;
    int n = -2147483647;
    int i1 = 1;
    if (k < m)
    {
      if (i == i1) {
        return null;
      }
      m = 45;
      if (k == m)
      {
        n = -1 << -1;
        k = 1;
        m = 1;
      }
      else
      {
        m = 43;
        if (k == m)
        {
          k = 1;
          m = 0;
        }
        else
        {
          return null;
        }
      }
    }
    else
    {
      k = 0;
      m = 0;
    }
    i -= i1;
    if (k <= i) {
      for (;;)
      {
        i1 = paramString.charAt(k);
        int i2 = 10;
        i1 = Character.digit(i1, i2);
        if (i1 < 0) {
          return null;
        }
        i2 = -214748364;
        if (j < i2) {
          return null;
        }
        j *= 10;
        i2 = n + i1;
        if (j < i2) {
          return null;
        }
        j -= i1;
        if (k == i) {
          break;
        }
        k += 1;
      }
    }
    if (m != 0) {
      return Integer.valueOf(j);
    }
    return Integer.valueOf(-j);
  }
  
  public static final Long d(String paramString)
  {
    k.b(paramString, "receiver$0");
    return m.e(paramString);
  }
  
  public static final Long e(String paramString)
  {
    String str = "receiver$0";
    k.b(paramString, str);
    int i = paramString.length();
    if (i == 0) {
      return null;
    }
    int j = 0;
    int k = paramString.charAt(0);
    int m = 48;
    long l1 = -9223372036854775807L;
    int n = 1;
    if (k < m)
    {
      if (i == n) {
        return null;
      }
      m = 45;
      if (k == m)
      {
        l1 = Long.MIN_VALUE;
        j = 1;
        k = 1;
      }
      else
      {
        m = 43;
        if (k == m)
        {
          j = 1;
          k = 0;
        }
        else
        {
          return null;
        }
      }
    }
    else
    {
      k = 0;
    }
    long l2 = 0L;
    i -= n;
    if (j <= i) {
      for (;;)
      {
        m = paramString.charAt(j);
        n = 10;
        m = Character.digit(m, n);
        if (m < 0) {
          return null;
        }
        long l3 = -922337203685477580L;
        boolean bool2 = l2 < l3;
        if (bool2) {
          return null;
        }
        l2 *= 10;
        l3 = m;
        long l4 = l1 + l3;
        boolean bool1 = l2 < l4;
        if (bool1) {
          return null;
        }
        l2 -= l3;
        if (j == i) {
          break;
        }
        j += 1;
      }
    }
    if (k != 0) {
      return Long.valueOf(l2);
    }
    return Long.valueOf(-l2);
  }
}

/* Location:
 * Qualified Name:     c.n.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */