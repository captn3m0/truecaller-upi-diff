package c.n;

import c.a.a;
import c.a.m;
import c.g.a.b;
import c.m.i;
import c.m.l;
import java.util.Iterator;
import java.util.regex.MatchResult;

public final class j$b
  extends a
  implements h
{
  j$b(j paramj) {}
  
  public final int a()
  {
    return ((MatchResult)a.a).groupCount() + 1;
  }
  
  public final boolean isEmpty()
  {
    return false;
  }
  
  public final Iterator iterator()
  {
    i locali = m.n((Iterable)m.a(this));
    Object localObject = new c/n/j$b$a;
    ((j.b.a)localObject).<init>(this);
    localObject = (b)localObject;
    return l.c(locali, (b)localObject).a();
  }
}

/* Location:
 * Qualified Name:     c.n.j.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */