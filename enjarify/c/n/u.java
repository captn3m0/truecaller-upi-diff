package c.n;

import c.a.ae;
import c.g.a.b;
import c.g.b.k;
import c.m.l;
import java.util.Iterator;

public class u
  extends t
{
  public static final String a(String paramString, char paramChar1, char paramChar2)
  {
    k.b(paramString, "receiver$0");
    paramString = paramString.replace(paramChar1, paramChar2);
    k.a(paramString, "(this as java.lang.Strin…replace(oldChar, newChar)");
    return paramString;
  }
  
  public static final String a(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "receiver$0");
    k.b(paramString2, "oldValue");
    k.b(paramString3, "newValue");
    paramString1 = (CharSequence)paramString1;
    Object localObject = new String[1];
    localObject[0] = paramString2;
    k.b(paramString1, "receiver$0");
    k.b(localObject, "delimiters");
    paramString2 = v.a(paramString1, (String[])localObject, false, 0);
    localObject = new c/n/v$c;
    ((v.c)localObject).<init>(paramString1);
    localObject = (b)localObject;
    paramString1 = l.c(paramString2, (b)localObject);
    paramString3 = (CharSequence)paramString3;
    return l.a(paramString1, paramString3);
  }
  
  public static final boolean a(CharSequence paramCharSequence)
  {
    Object localObject1 = "receiver$0";
    k.b(paramCharSequence, (String)localObject1);
    int i = paramCharSequence.length();
    boolean bool1 = true;
    if (i != 0)
    {
      localObject1 = ((Iterable)m.c(paramCharSequence)).iterator();
      boolean bool3;
      do
      {
        boolean bool2 = ((Iterator)localObject1).hasNext();
        if (!bool2) {
          break;
        }
        Object localObject2 = localObject1;
        localObject2 = (ae)localObject1;
        int j = ((ae)localObject2).a();
        bool3 = a.a(paramCharSequence.charAt(j));
      } while (bool3);
      int k = 0;
      paramCharSequence = null;
      break label92;
      k = 1;
      label92:
      if (k == 0) {
        return false;
      }
    }
    return bool1;
  }
  
  public static final boolean a(String paramString1, int paramInt1, String paramString2, int paramInt2, int paramInt3, boolean paramBoolean)
  {
    k.b(paramString1, "receiver$0");
    String str = "other";
    k.b(paramString2, str);
    if (!paramBoolean) {
      return paramString1.regionMatches(paramInt1, paramString2, paramInt2, paramInt3);
    }
    str = paramString1;
    return paramString1.regionMatches(paramBoolean, paramInt1, paramString2, paramInt2, paramInt3);
  }
  
  public static final boolean a(String paramString1, String paramString2, boolean paramBoolean)
  {
    if (paramString1 == null) {
      return paramString2 == null;
    }
    if (!paramBoolean) {
      return paramString1.equals(paramString2);
    }
    return paramString1.equalsIgnoreCase(paramString2);
  }
  
  public static final String b(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "receiver$0");
    k.b(paramString2, "oldValue");
    k.b(paramString3, "newValue");
    Object localObject = paramString1;
    localObject = (CharSequence)paramString1;
    int i = m.a((CharSequence)localObject, paramString2, 0, false, 2);
    if (i < 0) {
      return paramString1;
    }
    int j = paramString2.length() + i;
    paramString3 = (CharSequence)paramString3;
    k.b(localObject, "receiver$0");
    paramString2 = "replacement";
    k.b(paramString3, paramString2);
    if (j >= i)
    {
      paramString2 = new java/lang/StringBuilder;
      paramString2.<init>();
      paramString2.append((CharSequence)localObject, 0, i);
      paramString2.append(paramString3);
      int k = ((CharSequence)localObject).length();
      paramString2.append((CharSequence)localObject, j, k);
      return ((CharSequence)paramString2).toString();
    }
    paramString2 = new java/lang/IndexOutOfBoundsException;
    paramString3 = new java/lang/StringBuilder;
    paramString3.<init>("End index (");
    paramString3.append(j);
    paramString3.append(") is less than start index (");
    paramString3.append(i);
    paramString3.append(").");
    paramString1 = paramString3.toString();
    paramString2.<init>(paramString1);
    throw ((Throwable)paramString2);
  }
  
  public static final boolean b(String paramString1, String paramString2, boolean paramBoolean)
  {
    k.b(paramString1, "receiver$0");
    String str = "prefix";
    k.b(paramString2, str);
    if (!paramBoolean) {
      return paramString1.startsWith(paramString2);
    }
    int i = paramString2.length();
    str = paramString1;
    return m.a(paramString1, 0, paramString2, 0, i, paramBoolean);
  }
  
  public static final boolean c(String paramString1, String paramString2, boolean paramBoolean)
  {
    k.b(paramString1, "receiver$0");
    String str = "suffix";
    k.b(paramString2, str);
    if (!paramBoolean) {
      return paramString1.endsWith(paramString2);
    }
    paramBoolean = paramString1.length();
    boolean bool = paramString2.length();
    int i = paramBoolean - bool;
    int j = paramString2.length();
    return m.a(paramString1, i, paramString2, 0, j, true);
  }
  
  public static final String f(String paramString)
  {
    k.b(paramString, "receiver$0");
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    int i = ((CharSequence)localObject).length();
    int j = 1;
    String str1 = null;
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0)
    {
      boolean bool = Character.isLowerCase(paramString.charAt(0));
      if (bool)
      {
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        str1 = paramString.substring(0, j);
        String str2 = "(this as java.lang.Strin…ing(startIndex, endIndex)";
        k.a(str1, str2);
        if (str1 != null)
        {
          str1 = str1.toUpperCase();
          k.a(str1, "(this as java.lang.String).toUpperCase()");
          ((StringBuilder)localObject).append(str1);
          paramString = paramString.substring(j);
          k.a(paramString, "(this as java.lang.String).substring(startIndex)");
          ((StringBuilder)localObject).append(paramString);
          return ((StringBuilder)localObject).toString();
        }
        paramString = new c/u;
        paramString.<init>("null cannot be cast to non-null type java.lang.String");
        throw paramString;
      }
    }
    return paramString;
  }
}

/* Location:
 * Qualified Name:     c.n.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */