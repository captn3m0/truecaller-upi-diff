package c.n;

import java.io.Serializable;
import java.util.regex.Pattern;

final class k$b
  implements Serializable
{
  public static final k.b.a a;
  private static final long serialVersionUID;
  private final String b;
  private final int c;
  
  static
  {
    k.b.a locala = new c/n/k$b$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public k$b(String paramString, int paramInt)
  {
    b = paramString;
    c = paramInt;
  }
  
  private final Object readResolve()
  {
    k localk = new c/n/k;
    Object localObject = b;
    int i = c;
    localObject = Pattern.compile((String)localObject, i);
    c.g.b.k.a(localObject, "Pattern.compile(pattern, flags)");
    localk.<init>((Pattern)localObject);
    return localk;
  }
}

/* Location:
 * Qualified Name:     c.n.k.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */