package c.i;

import c.g.b.k;
import c.l.g;

final class b
  implements d
{
  private Object a;
  
  public final Object a(g paramg)
  {
    k.b(paramg, "property");
    Object localObject = a;
    if (localObject != null) {
      return localObject;
    }
    localObject = new java/lang/IllegalStateException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Property ");
    paramg = paramg.b();
    localStringBuilder.append(paramg);
    localStringBuilder.append(" should be initialized before get.");
    paramg = localStringBuilder.toString();
    ((IllegalStateException)localObject).<init>(paramg);
    throw ((Throwable)localObject);
  }
  
  public final void a(g paramg, Object paramObject)
  {
    k.b(paramg, "property");
    k.b(paramObject, "value");
    a = paramObject;
  }
}

/* Location:
 * Qualified Name:     c.i.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */