package c;

import c.g.a.a;
import c.g.b.k;
import java.io.Serializable;

public final class y
  implements f, Serializable
{
  private a a;
  private Object b;
  
  public y(a parama)
  {
    a = parama;
    parama = v.a;
    b = parama;
  }
  
  private final Object writeReplace()
  {
    d locald = new c/d;
    Object localObject = b();
    locald.<init>(localObject);
    return locald;
  }
  
  public final boolean a()
  {
    Object localObject = b;
    v localv = v.a;
    return localObject != localv;
  }
  
  public final Object b()
  {
    Object localObject = b;
    v localv = v.a;
    if (localObject == localv)
    {
      localObject = a;
      if (localObject == null) {
        k.a();
      }
      localObject = ((a)localObject).invoke();
      b = localObject;
      localObject = null;
      a = null;
    }
    return b;
  }
  
  public final String toString()
  {
    boolean bool = a();
    if (bool) {
      return String.valueOf(b());
    }
    return "Lazy value not initialized yet.";
  }
}

/* Location:
 * Qualified Name:     c.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */