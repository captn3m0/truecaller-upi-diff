package c;

import c.g.b.k;
import java.io.Serializable;

public final class n
  implements Serializable
{
  public final Object a;
  public final Object b;
  
  public n(Object paramObject1, Object paramObject2)
  {
    a = paramObject1;
    b = paramObject2;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof n;
      if (bool1)
      {
        paramObject = (n)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = b;
          paramObject = b;
          boolean bool2 = k.a(localObject1, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final int hashCode()
  {
    Object localObject1 = a;
    int i = 0;
    int j;
    if (localObject1 != null)
    {
      j = localObject1.hashCode();
    }
    else
    {
      j = 0;
      localObject1 = null;
    }
    j *= 31;
    Object localObject2 = b;
    if (localObject2 != null) {
      i = localObject2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("(");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", ");
    localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(')');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     c.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */