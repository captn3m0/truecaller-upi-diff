package c.m;

import c.d.c;
import c.d.f;
import c.d.g;
import c.o;
import c.x;
import java.util.Iterator;
import java.util.NoSuchElementException;

final class j
  extends k
  implements c, c.g.b.a.a, Iterator
{
  c a;
  private int b;
  private Object c;
  private Iterator d;
  
  private final Throwable b()
  {
    int i = b;
    switch (i)
    {
    default: 
      localObject1 = new java/lang/IllegalStateException;
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Unexpected state of the iterator: ");
      int j = b;
      ((StringBuilder)localObject2).append(j);
      localObject2 = ((StringBuilder)localObject2).toString();
      ((IllegalStateException)localObject1).<init>((String)localObject2);
      return (Throwable)localObject1;
    case 5: 
      localObject1 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject1).<init>("Iterator has failed.");
      return (Throwable)localObject1;
    }
    Object localObject1 = new java/util/NoSuchElementException;
    ((NoSuchElementException)localObject1).<init>();
    return (Throwable)localObject1;
  }
  
  public final Object a(Object paramObject, c paramc)
  {
    c = paramObject;
    int i = 3;
    b = i;
    a = paramc;
    paramObject = c.d.a.a.a;
    Object localObject = c.d.a.a.a;
    if (paramObject == localObject)
    {
      localObject = "frame";
      c.g.b.k.b(paramc, (String)localObject);
    }
    return paramObject;
  }
  
  public final f ao_()
  {
    return (f)g.a;
  }
  
  public final void b(Object paramObject)
  {
    b = 4;
  }
  
  public final boolean hasNext()
  {
    for (;;)
    {
      int i = b;
      Object localObject1 = null;
      boolean bool2 = true;
      switch (i)
      {
      default: 
        throw b();
      case 4: 
        return false;
      case 2: 
      case 3: 
        return bool2;
      case 1: 
        localObject2 = d;
        if (localObject2 == null) {
          c.g.b.k.a();
        }
        boolean bool1 = ((Iterator)localObject2).hasNext();
        if (bool1)
        {
          b = 2;
          return bool2;
        }
        d = null;
      }
      int j = 5;
      b = j;
      Object localObject2 = a;
      if (localObject2 == null) {
        c.g.b.k.a();
      }
      a = null;
      localObject1 = x.a;
      localObject1 = o.d(localObject1);
      ((c)localObject2).b(localObject1);
    }
  }
  
  public final Object next()
  {
    boolean bool;
    do
    {
      int i = b;
      switch (i)
      {
      default: 
        throw b();
      case 3: 
        b = 0;
        localObject = c;
        c = null;
        return localObject;
      case 2: 
        i = 1;
        b = i;
        localObject = d;
        if (localObject == null) {
          c.g.b.k.a();
        }
        return ((Iterator)localObject).next();
      }
      bool = hasNext();
    } while (bool);
    Object localObject = new java/util/NoSuchElementException;
    ((NoSuchElementException)localObject).<init>();
    throw ((Throwable)localObject);
  }
  
  public final void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
}

/* Location:
 * Qualified Name:     c.m.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */