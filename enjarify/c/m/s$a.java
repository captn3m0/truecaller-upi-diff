package c.m;

import c.g.a.b;
import c.g.b.a.a;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class s$a
  implements a, Iterator
{
  private final Iterator b;
  private int c;
  private Object d;
  
  s$a(s params)
  {
    params = a.a();
    b = params;
    c = -1;
  }
  
  private final void a()
  {
    Object localObject = b;
    boolean bool1 = ((Iterator)localObject).hasNext();
    if (bool1)
    {
      localObject = b.next();
      Boolean localBoolean = (Boolean)a.b.invoke(localObject);
      boolean bool2 = localBoolean.booleanValue();
      if (bool2)
      {
        c = 1;
        d = localObject;
        return;
      }
    }
    c = 0;
  }
  
  public final boolean hasNext()
  {
    int i = c;
    int j = -1;
    if (i == j) {
      a();
    }
    i = c;
    j = 1;
    if (i == j) {
      return j;
    }
    return false;
  }
  
  public final Object next()
  {
    int i = c;
    int j = -1;
    if (i == j) {
      a();
    }
    i = c;
    if (i != 0)
    {
      localObject = d;
      d = null;
      c = j;
      return localObject;
    }
    Object localObject = new java/util/NoSuchElementException;
    ((NoSuchElementException)localObject).<init>();
    throw ((Throwable)localObject);
  }
  
  public final void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
}

/* Location:
 * Qualified Name:     c.m.s.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */