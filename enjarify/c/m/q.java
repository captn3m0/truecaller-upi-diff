package c.m;

import c.a.ar;
import c.g.a.b;
import c.g.b.k;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class q
  extends p
{
  public static final i a(i parami, int paramInt)
  {
    k.b(parami, "receiver$0");
    return l.a(parami, paramInt, paramInt);
  }
  
  public static final i a(i parami, int paramInt1, int paramInt2)
  {
    k.b(parami, "receiver$0");
    return ar.a(parami, paramInt1, paramInt2);
  }
  
  public static final i a(i parami, b paramb)
  {
    k.b(parami, "receiver$0");
    k.b(paramb, "predicate");
    f localf = new c/m/f;
    localf.<init>(parami, true, paramb);
    return (i)localf;
  }
  
  public static final Appendable a(i parami, Appendable paramAppendable, CharSequence paramCharSequence1, CharSequence paramCharSequence2, CharSequence paramCharSequence3, CharSequence paramCharSequence4)
  {
    k.b(parami, "receiver$0");
    k.b(paramAppendable, "buffer");
    k.b(paramCharSequence1, "separator");
    k.b(paramCharSequence2, "prefix");
    k.b(paramCharSequence3, "postfix");
    String str = "truncated";
    k.b(paramCharSequence4, str);
    paramAppendable.append(paramCharSequence2);
    parami = parami.a();
    int i = 0;
    paramCharSequence2 = null;
    for (;;)
    {
      boolean bool = parami.hasNext();
      if (!bool) {
        break;
      }
      paramCharSequence4 = parami.next();
      int j = 1;
      i += j;
      if (i > j) {
        paramAppendable.append(paramCharSequence1);
      }
      j = 0;
      str = null;
      c.n.m.a(paramAppendable, paramCharSequence4, null);
    }
    paramAppendable.append(paramCharSequence3);
    return paramAppendable;
  }
  
  public static final String a(i parami, CharSequence paramCharSequence1, CharSequence paramCharSequence2, CharSequence paramCharSequence3, CharSequence paramCharSequence4)
  {
    k.b(parami, "receiver$0");
    k.b(paramCharSequence1, "separator");
    k.b(paramCharSequence2, "prefix");
    k.b(paramCharSequence3, "postfix");
    k.b(paramCharSequence4, "truncated");
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    Object localObject = localStringBuilder;
    localObject = (Appendable)localStringBuilder;
    parami = ((StringBuilder)l.a(parami, (Appendable)localObject, paramCharSequence1, paramCharSequence2, paramCharSequence3, paramCharSequence4)).toString();
    k.a(parami, "joinTo(StringBuilder(), …ed, transform).toString()");
    return parami;
  }
  
  public static final Collection a(i parami, Collection paramCollection)
  {
    k.b(parami, "receiver$0");
    Object localObject = "destination";
    k.b(paramCollection, (String)localObject);
    parami = parami.a();
    for (;;)
    {
      boolean bool = parami.hasNext();
      if (!bool) {
        break;
      }
      localObject = parami.next();
      paramCollection.add(localObject);
    }
    return paramCollection;
  }
  
  public static final i b(i parami, b paramb)
  {
    k.b(parami, "receiver$0");
    k.b(paramb, "predicate");
    f localf = new c/m/f;
    localf.<init>(parami, false, paramb);
    return (i)localf;
  }
  
  public static final i c(i parami)
  {
    k.b(parami, "receiver$0");
    b localb = (b)q.b.a;
    return l.b(parami, localb);
  }
  
  public static final i c(i parami, b paramb)
  {
    k.b(parami, "receiver$0");
    k.b(paramb, "transform");
    t localt = new c/m/t;
    localt.<init>(parami, paramb);
    return (i)localt;
  }
  
  public static final i d(i parami, b paramb)
  {
    k.b(parami, "receiver$0");
    k.b(paramb, "transform");
    t localt = new c/m/t;
    localt.<init>(parami, paramb);
    return l.c((i)localt);
  }
  
  public static final List d(i parami)
  {
    k.b(parami, "receiver$0");
    return c.a.m.b(l.e(parami));
  }
  
  public static final List e(i parami)
  {
    k.b(parami, "receiver$0");
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    localObject = (Collection)localObject;
    return (List)l.a(parami, (Collection)localObject);
  }
  
  public static final Iterable f(i parami)
  {
    k.b(parami, "receiver$0");
    q.a locala = new c/m/q$a;
    locala.<init>(parami);
    return (Iterable)locala;
  }
}

/* Location:
 * Qualified Name:     c.m.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */