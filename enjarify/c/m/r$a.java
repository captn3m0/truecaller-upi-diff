package c.m;

import c.g.b.a.a;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class r$a
  implements a, Iterator
{
  private int b;
  private final Iterator c;
  
  r$a(r paramr)
  {
    int i = b;
    b = i;
    paramr = a.a();
    c = paramr;
  }
  
  public final boolean hasNext()
  {
    int i = b;
    if (i > 0)
    {
      Iterator localIterator = c;
      boolean bool = localIterator.hasNext();
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public final Object next()
  {
    int i = b;
    if (i != 0)
    {
      i += -1;
      b = i;
      return c.next();
    }
    NoSuchElementException localNoSuchElementException = new java/util/NoSuchElementException;
    localNoSuchElementException.<init>();
    throw ((Throwable)localNoSuchElementException);
  }
  
  public final void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
}

/* Location:
 * Qualified Name:     c.m.r.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */