package c.m;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReference;

public final class a
  implements i
{
  private final AtomicReference a;
  
  public a(i parami)
  {
    AtomicReference localAtomicReference = new java/util/concurrent/atomic/AtomicReference;
    localAtomicReference.<init>(parami);
    a = localAtomicReference;
  }
  
  public final Iterator a()
  {
    Object localObject = (i)a.getAndSet(null);
    if (localObject != null) {
      return ((i)localObject).a();
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("This sequence can be consumed only once.");
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     c.m.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */