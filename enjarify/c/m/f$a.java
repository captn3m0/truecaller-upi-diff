package c.m;

import c.g.a.b;
import c.g.b.a.a;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class f$a
  implements a, Iterator
{
  private final Iterator b;
  private int c;
  private Object d;
  
  f$a(f paramf)
  {
    paramf = a.a();
    b = paramf;
    c = -1;
  }
  
  private final void a()
  {
    Object localObject;
    boolean bool2;
    boolean bool3;
    do
    {
      localObject = b;
      boolean bool1 = ((Iterator)localObject).hasNext();
      if (!bool1) {
        break;
      }
      localObject = b.next();
      Boolean localBoolean = (Boolean)a.c.invoke(localObject);
      bool2 = localBoolean.booleanValue();
      f localf = a;
      bool3 = b;
    } while (bool2 != bool3);
    d = localObject;
    c = 1;
    return;
    c = 0;
  }
  
  public final boolean hasNext()
  {
    int i = c;
    int j = -1;
    if (i == j) {
      a();
    }
    i = c;
    j = 1;
    if (i == j) {
      return j;
    }
    return false;
  }
  
  public final Object next()
  {
    int i = c;
    int j = -1;
    if (i == j) {
      a();
    }
    i = c;
    if (i != 0)
    {
      localObject = d;
      d = null;
      c = j;
      return localObject;
    }
    Object localObject = new java/util/NoSuchElementException;
    ((NoSuchElementException)localObject).<init>();
    throw ((Throwable)localObject);
  }
  
  public final void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
}

/* Location:
 * Qualified Name:     c.m.f.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */