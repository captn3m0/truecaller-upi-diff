package c.m;

import c.g.a.b;
import c.g.b.k;
import c.u;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class h$a
  implements c.g.b.a.a, Iterator
{
  private Object b;
  private int c = -2;
  
  h$a(h paramh) {}
  
  private final void a()
  {
    int i = c;
    int j = -2;
    if (i == j)
    {
      localObject1 = a.a.invoke();
    }
    else
    {
      localObject1 = a.b;
      Object localObject2 = b;
      if (localObject2 == null) {
        k.a();
      }
      localObject1 = ((b)localObject1).invoke(localObject2);
    }
    b = localObject1;
    Object localObject1 = b;
    if (localObject1 == null)
    {
      i = 0;
      localObject1 = null;
    }
    else
    {
      i = 1;
    }
    c = i;
  }
  
  public final boolean hasNext()
  {
    int i = c;
    if (i < 0) {
      a();
    }
    i = c;
    int j = 1;
    if (i == j) {
      return j;
    }
    return false;
  }
  
  public final Object next()
  {
    int i = c;
    if (i < 0) {
      a();
    }
    i = c;
    if (i != 0)
    {
      localObject = b;
      if (localObject != null)
      {
        c = -1;
        return localObject;
      }
      localObject = new c/u;
      ((u)localObject).<init>("null cannot be cast to non-null type T");
      throw ((Throwable)localObject);
    }
    Object localObject = new java/util/NoSuchElementException;
    ((NoSuchElementException)localObject).<init>();
    throw ((Throwable)localObject);
  }
  
  public final void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
}

/* Location:
 * Qualified Name:     c.m.h.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */