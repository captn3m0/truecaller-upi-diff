package c.m;

import c.g.a.b;
import c.g.b.a.a;
import c.g.b.k;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class g$a
  implements a, Iterator
{
  private final Iterator b;
  private Iterator c;
  
  g$a(g paramg)
  {
    paramg = a.a();
    b = paramg;
  }
  
  private final boolean a()
  {
    Object localObject = c;
    boolean bool1;
    if (localObject != null)
    {
      bool1 = ((Iterator)localObject).hasNext();
      if (!bool1)
      {
        bool1 = false;
        localObject = null;
        c = null;
      }
    }
    boolean bool2;
    boolean bool3;
    do
    {
      localObject = c;
      bool2 = true;
      if (localObject != null) {
        break;
      }
      localObject = b;
      bool1 = ((Iterator)localObject).hasNext();
      if (!bool1) {
        return false;
      }
      localObject = b.next();
      b localb1 = a.c;
      b localb2 = a.b;
      localObject = localb2.invoke(localObject);
      localObject = (Iterator)localb1.invoke(localObject);
      bool3 = ((Iterator)localObject).hasNext();
    } while (!bool3);
    c = ((Iterator)localObject);
    return bool2;
    return bool2;
  }
  
  public final boolean hasNext()
  {
    return a();
  }
  
  public final Object next()
  {
    boolean bool = a();
    if (bool)
    {
      localObject = c;
      if (localObject == null) {
        k.a();
      }
      return ((Iterator)localObject).next();
    }
    Object localObject = new java/util/NoSuchElementException;
    ((NoSuchElementException)localObject).<init>();
    throw ((Throwable)localObject);
  }
  
  public final void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
}

/* Location:
 * Qualified Name:     c.m.g.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */