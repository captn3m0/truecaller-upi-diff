package c.m;

import java.util.Iterator;

public final class r
  implements d, i
{
  final i a;
  final int b;
  
  public r(i parami)
  {
    a = parami;
    b = 2;
    int i = b;
    if (i >= 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      parami = null;
    }
    if (i != 0) {
      return;
    }
    parami = new java/lang/StringBuilder;
    parami.<init>("count must be non-negative, but was ");
    int j = b;
    parami.append(j);
    parami.append('.');
    parami = parami.toString();
    localObject = new java/lang/IllegalArgumentException;
    parami = parami.toString();
    ((IllegalArgumentException)localObject).<init>(parami);
    throw ((Throwable)localObject);
  }
  
  public final Iterator a()
  {
    r.a locala = new c/m/r$a;
    locala.<init>(this);
    return (Iterator)locala;
  }
}

/* Location:
 * Qualified Name:     c.m.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */