package c.m;

import c.g.a.b;
import c.g.b.a.a;
import java.util.Iterator;

public final class t$a
  implements a, Iterator
{
  private final Iterator b;
  
  t$a(t paramt)
  {
    paramt = a.a();
    b = paramt;
  }
  
  public final boolean hasNext()
  {
    return b.hasNext();
  }
  
  public final Object next()
  {
    b localb = a.b;
    Object localObject = b.next();
    return localb.invoke(localObject);
  }
  
  public final void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
}

/* Location:
 * Qualified Name:     c.m.t.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */