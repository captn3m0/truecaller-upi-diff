package c.m;

import c.g.a.b;
import c.g.b.k;
import java.util.Iterator;

public final class t
  implements i
{
  final i a;
  final b b;
  
  public t(i parami, b paramb)
  {
    a = parami;
    b = paramb;
  }
  
  public final i a(b paramb)
  {
    k.b(paramb, "iterator");
    g localg = new c/m/g;
    i locali = a;
    b localb = b;
    localg.<init>(locali, localb, paramb);
    return (i)localg;
  }
  
  public final Iterator a()
  {
    t.a locala = new c/m/t$a;
    locala.<init>(this);
    return (Iterator)locala;
  }
}

/* Location:
 * Qualified Name:     c.m.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */