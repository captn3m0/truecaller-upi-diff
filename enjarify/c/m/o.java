package c.m;

import c.a.f;
import c.g.a.b;
import c.g.b.k;

public class o
  extends n
{
  public static final i a(i parami)
  {
    k.b(parami, "receiver$0");
    b localb = (b)o.b.a;
    return a(parami, localb);
  }
  
  private static final i a(i parami, b paramb)
  {
    boolean bool = parami instanceof t;
    if (bool) {
      return ((t)parami).a(paramb);
    }
    g localg = new c/m/g;
    b localb = (b)o.c.a;
    localg.<init>(parami, localb, paramb);
    return (i)localg;
  }
  
  public static final i a(Object paramObject, b paramb)
  {
    k.b(paramb, "nextFunction");
    h localh = new c/m/h;
    Object localObject = new c/m/o$e;
    ((o.e)localObject).<init>(paramObject);
    localObject = (c.g.a.a)localObject;
    localh.<init>((c.g.a.a)localObject, paramb);
    return (i)localh;
  }
  
  public static final i a(Object... paramVarArgs)
  {
    k.b(paramVarArgs, "elements");
    return f.j(paramVarArgs);
  }
  
  public static final i b(i parami)
  {
    Object localObject = "receiver$0";
    k.b(parami, (String)localObject);
    boolean bool = parami instanceof a;
    if (bool) {
      return (i)parami;
    }
    localObject = new c/m/a;
    ((a)localObject).<init>(parami);
    return (i)localObject;
  }
}

/* Location:
 * Qualified Name:     c.m.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */