package c.j;

import java.util.Random;

public abstract class a
  extends c
{
  public final int a(int paramInt)
  {
    int i = a().nextInt();
    int j = 32 - paramInt;
    i >>>= j;
    return -paramInt >> 31 & i;
  }
  
  public abstract Random a();
  
  public final int b()
  {
    return a().nextInt();
  }
  
  public final int b(int paramInt)
  {
    return a().nextInt(paramInt);
  }
  
  public final long c()
  {
    return a().nextLong();
  }
}

/* Location:
 * Qualified Name:     c.j.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */