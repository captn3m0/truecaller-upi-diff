package c.j;

import c.e.a;
import c.e.b;
import c.g.b.k;

public abstract class c
{
  static final c a = a.a();
  public static final c.a b = c.a.d;
  public static final c.b c;
  
  static
  {
    Object localObject = new c/j/c$b;
    ((c.b)localObject).<init>((byte)0);
    c = (c.b)localObject;
    localObject = b.a;
  }
  
  public abstract int a(int paramInt);
  
  public int a(int paramInt1, int paramInt2)
  {
    int i = 1;
    int j;
    if (paramInt2 > paramInt1) {
      j = 1;
    } else {
      j = 0;
    }
    if (j != 0)
    {
      j = paramInt2 - paramInt1;
      int k;
      if (j <= 0)
      {
        k = -1 << -1;
        if (j != k)
        {
          do
          {
            i = b();
          } while ((paramInt1 > i) || (paramInt2 <= i));
          return i;
        }
      }
      paramInt2 = -j & j;
      if (paramInt2 == j)
      {
        paramInt2 = Integer.numberOfLeadingZeros(j);
        paramInt2 = 31 - paramInt2;
        paramInt2 = a(paramInt2);
      }
      else
      {
        do
        {
          paramInt2 = b() >>> i;
          k = paramInt2 % j;
          paramInt2 -= k;
          int m = j + -1;
          paramInt2 += m;
        } while (paramInt2 < 0);
        paramInt2 = k;
      }
      return paramInt1 + paramInt2;
    }
    Object localObject1 = Integer.valueOf(paramInt1);
    Object localObject2 = Integer.valueOf(paramInt2);
    k.b(localObject1, "from");
    k.b(localObject2, "until");
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Random range is empty: [");
    localStringBuilder.append(localObject1);
    localStringBuilder.append(", ");
    localStringBuilder.append(localObject2);
    localStringBuilder.append(").");
    localObject1 = localStringBuilder.toString();
    localObject2 = new java/lang/IllegalArgumentException;
    localObject1 = localObject1.toString();
    ((IllegalArgumentException)localObject2).<init>((String)localObject1);
    throw ((Throwable)localObject2);
  }
  
  public int b()
  {
    return a(32);
  }
  
  public int b(int paramInt)
  {
    return a(0, paramInt);
  }
  
  public long c()
  {
    long l1 = b() << 32;
    long l2 = b();
    return l1 + l2;
  }
}

/* Location:
 * Qualified Name:     c.j.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */