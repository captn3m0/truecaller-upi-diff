package c.k;

import c.e.c;
import c.g.b.a.a;

public class f
  implements a, Iterable
{
  public static final f.a d;
  public final int a;
  public final int b;
  public final int c;
  
  static
  {
    f.a locala = new c/k/f$a;
    locala.<init>((byte)0);
    d = locala;
  }
  
  public f(int paramInt1, int paramInt2, int paramInt3)
  {
    if (paramInt3 != 0)
    {
      int i = -1 << -1;
      if (paramInt3 != i)
      {
        a = paramInt1;
        paramInt1 = c.a(paramInt1, paramInt2, paramInt3);
        b = paramInt1;
        c = paramInt3;
        return;
      }
      localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localIllegalArgumentException.<init>("Step must be greater than Int.MIN_VALUE to avoid overflow on negation.");
      throw ((Throwable)localIllegalArgumentException);
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    localIllegalArgumentException.<init>("Step must be non-zero.");
    throw ((Throwable)localIllegalArgumentException);
  }
  
  public boolean a()
  {
    int i = c;
    boolean bool = true;
    if (i > 0)
    {
      i = a;
      j = b;
      if (i > j) {
        return bool;
      }
      return false;
    }
    i = a;
    int j = b;
    if (i < j) {
      return bool;
    }
    return false;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = paramObject instanceof f;
    if (bool)
    {
      bool = a();
      if (bool)
      {
        Object localObject = paramObject;
        localObject = (f)paramObject;
        bool = ((f)localObject).a();
        if (bool) {}
      }
      else
      {
        int i = a;
        paramObject = (f)paramObject;
        int j = a;
        if (i != j) {
          break label92;
        }
        i = b;
        j = b;
        if (i != j) {
          break label92;
        }
        i = c;
        int k = c;
        if (i != k) {
          break label92;
        }
      }
      return true;
    }
    label92:
    return false;
  }
  
  public int hashCode()
  {
    boolean bool = a();
    if (bool) {
      return -1;
    }
    int i = a * 31;
    int j = b;
    i = (i + j) * 31;
    j = c;
    return i + j;
  }
  
  public String toString()
  {
    int i = c;
    StringBuilder localStringBuilder;
    String str;
    if (i > 0)
    {
      localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      j = a;
      localStringBuilder.append(j);
      localStringBuilder.append("..");
      j = b;
      localStringBuilder.append(j);
      str = " step ";
      localStringBuilder.append(str);
    }
    for (int j = c;; j = -c)
    {
      localStringBuilder.append(j);
      return localStringBuilder.toString();
      localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      j = a;
      localStringBuilder.append(j);
      localStringBuilder.append(" downTo ");
      j = b;
      localStringBuilder.append(j);
      str = " step ";
      localStringBuilder.append(str);
    }
  }
}

/* Location:
 * Qualified Name:     c.k.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */