package c.k;

import c.a.ae;
import java.util.NoSuchElementException;

public final class g
  extends ae
{
  private final int a;
  private boolean b;
  private int c;
  private final int d;
  
  public g(int paramInt1, int paramInt2, int paramInt3)
  {
    d = paramInt3;
    a = paramInt2;
    paramInt3 = d;
    boolean bool = true;
    if (paramInt3 > 0 ? paramInt1 > paramInt2 : paramInt1 < paramInt2) {
      bool = false;
    }
    b = bool;
    paramInt2 = b;
    if (paramInt2 == 0) {
      paramInt1 = a;
    }
    c = paramInt1;
  }
  
  public final int a()
  {
    int i = c;
    int j = a;
    if (i == j)
    {
      boolean bool = b;
      if (bool)
      {
        bool = false;
        b = false;
      }
      else
      {
        NoSuchElementException localNoSuchElementException = new java/util/NoSuchElementException;
        localNoSuchElementException.<init>();
        throw ((Throwable)localNoSuchElementException);
      }
    }
    else
    {
      int k = d + i;
      c = k;
    }
    return i;
  }
  
  public final boolean hasNext()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     c.k.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */