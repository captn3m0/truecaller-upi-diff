package c.k;

public class l
  extends k
{
  public static final float a(float paramFloat)
  {
    float f = 0.0F;
    boolean bool = paramFloat < 0.0F;
    if (bool) {
      return 0.0F;
    }
    f = 1.0F;
    bool = paramFloat < f;
    if (bool) {
      return f;
    }
    return paramFloat;
  }
  
  public static final int a(int paramInt1, int paramInt2, int paramInt3)
  {
    if (paramInt2 <= paramInt3)
    {
      if (paramInt1 < paramInt2) {
        return paramInt2;
      }
      if (paramInt1 > paramInt3) {
        return paramInt3;
      }
      return paramInt1;
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Cannot coerce value to an empty range: maximum ");
    localStringBuilder.append(paramInt3);
    localStringBuilder.append(" is less than minimum ");
    localStringBuilder.append(paramInt2);
    localStringBuilder.append('.');
    String str = localStringBuilder.toString();
    localIllegalArgumentException.<init>(str);
    throw ((Throwable)localIllegalArgumentException);
  }
  
  public static final int a(int paramInt, e parame)
  {
    Object localObject1 = "range";
    c.g.b.k.b(parame, (String)localObject1);
    boolean bool = parame instanceof d;
    if (bool)
    {
      localObject2 = (Comparable)Integer.valueOf(paramInt);
      parame = (d)parame;
      return ((Number)i.a((Comparable)localObject2, parame)).intValue();
    }
    bool = parame.a();
    if (!bool)
    {
      localObject1 = (Number)parame.b();
      int i = ((Number)localObject1).intValue();
      if (paramInt < i) {
        return ((Number)parame.b()).intValue();
      }
      localObject1 = (Number)parame.c();
      i = ((Number)localObject1).intValue();
      if (paramInt > i) {
        return ((Number)parame.c()).intValue();
      }
      return paramInt;
    }
    Object localObject2 = new java/lang/IllegalArgumentException;
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("Cannot coerce value to an empty range: ");
    ((StringBuilder)localObject1).append(parame);
    ((StringBuilder)localObject1).append('.');
    parame = ((StringBuilder)localObject1).toString();
    ((IllegalArgumentException)localObject2).<init>(parame);
    throw ((Throwable)localObject2);
  }
  
  public static final long a(long paramLong1, long paramLong2)
  {
    boolean bool = paramLong1 < paramLong2;
    if (bool) {
      return paramLong2;
    }
    return paramLong1;
  }
  
  public static final f a(int paramInt1, int paramInt2)
  {
    return f.a.a(paramInt1, paramInt2);
  }
  
  public static final Comparable a(Comparable paramComparable, d paramd)
  {
    c.g.b.k.b(paramComparable, "receiver$0");
    Object localObject = "range";
    c.g.b.k.b(paramd, (String)localObject);
    boolean bool = paramd.a();
    if (!bool)
    {
      paramd.b();
      bool = paramd.d();
      if (bool)
      {
        paramd.b();
        bool = paramd.d();
        if (!bool) {
          return paramd.b();
        }
      }
      paramd.c();
      bool = paramd.d();
      if (bool)
      {
        paramd.c();
        bool = paramd.d();
        if (!bool) {
          return paramd.c();
        }
      }
      return paramComparable;
    }
    paramComparable = new java/lang/IllegalArgumentException;
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("Cannot coerce value to an empty range: ");
    ((StringBuilder)localObject).append(paramd);
    ((StringBuilder)localObject).append('.');
    paramd = ((StringBuilder)localObject).toString();
    paramComparable.<init>(paramd);
    throw ((Throwable)paramComparable);
  }
  
  public static final long b(long paramLong1, long paramLong2)
  {
    boolean bool = paramLong1 < paramLong2;
    if (bool) {
      return paramLong2;
    }
    return paramLong1;
  }
  
  public static final h b(int paramInt1, int paramInt2)
  {
    int i = -1 << -1;
    if (paramInt2 <= i) {
      return h.d();
    }
    h localh = new c/k/h;
    paramInt2 += -1;
    localh.<init>(paramInt1, paramInt2);
    return localh;
  }
  
  public static final int c(int paramInt1, int paramInt2)
  {
    if (paramInt1 < paramInt2) {
      return paramInt2;
    }
    return paramInt1;
  }
  
  public static final int d(int paramInt1, int paramInt2)
  {
    if (paramInt1 > paramInt2) {
      return paramInt2;
    }
    return paramInt1;
  }
}

/* Location:
 * Qualified Name:     c.k.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */