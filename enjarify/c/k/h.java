package c.k;

public final class h
  extends f
  implements e
{
  public static final h.a e;
  private static final h f;
  
  static
  {
    Object localObject = new c/k/h$a;
    ((h.a)localObject).<init>((byte)0);
    e = (h.a)localObject;
    localObject = new c/k/h;
    ((h)localObject).<init>(1, 0);
    f = (h)localObject;
  }
  
  public h(int paramInt1, int paramInt2)
  {
    super(paramInt1, paramInt2, 1);
  }
  
  public final boolean a()
  {
    int i = a;
    int j = b;
    return i > j;
  }
  
  public final boolean a(int paramInt)
  {
    int i = a;
    if (i <= paramInt)
    {
      i = b;
      if (paramInt <= i) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool = paramObject instanceof h;
    if (bool)
    {
      bool = a();
      if (bool)
      {
        Object localObject = paramObject;
        localObject = (h)paramObject;
        bool = ((h)localObject).a();
        if (bool) {}
      }
      else
      {
        int i = a;
        paramObject = (h)paramObject;
        int j = a;
        if (i != j) {
          break label75;
        }
        i = b;
        int k = b;
        if (i != k) {
          break label75;
        }
      }
      return true;
    }
    label75:
    return false;
  }
  
  public final int hashCode()
  {
    boolean bool = a();
    if (bool) {
      return -1;
    }
    int i = a * 31;
    int j = b;
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    int i = a;
    localStringBuilder.append(i);
    localStringBuilder.append("..");
    i = b;
    localStringBuilder.append(i);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     c.k.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */