package c.k;

import c.a.l;
import java.util.NoSuchElementException;

public final class b
  extends l
{
  private final int a;
  private boolean b;
  private int c;
  private final int d;
  
  public b(char paramChar1, char paramChar2, int paramInt)
  {
    d = paramInt;
    a = paramChar2;
    paramInt = d;
    boolean bool = true;
    if (paramInt > 0 ? paramChar1 > paramChar2 : paramChar1 < paramChar2) {
      bool = false;
    }
    b = bool;
    paramChar2 = b;
    if (paramChar2 == 0) {
      paramChar1 = a;
    }
    c = paramChar1;
  }
  
  public final char a()
  {
    int i = c;
    int j = a;
    if (i == j)
    {
      boolean bool = b;
      if (bool)
      {
        bool = false;
        b = false;
      }
      else
      {
        NoSuchElementException localNoSuchElementException = new java/util/NoSuchElementException;
        localNoSuchElementException.<init>();
        throw ((Throwable)localNoSuchElementException);
      }
    }
    else
    {
      int k = d + i;
      c = k;
    }
    return (char)i;
  }
  
  public final boolean hasNext()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     c.k.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */