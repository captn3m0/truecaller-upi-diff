package c.k;

import c.e.c;

public class a
  implements c.g.b.a.a, Iterable
{
  public static final a.a c;
  public final char a;
  public final char b;
  private final int d;
  
  static
  {
    a.a locala = new c/k/a$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public a(char paramChar1, char paramChar2)
  {
    a = paramChar1;
    int i = 1;
    paramChar1 = (char)c.a(paramChar1, paramChar2, i);
    b = paramChar1;
    d = i;
  }
  
  public boolean a()
  {
    int i = d;
    boolean bool = true;
    if (i > 0)
    {
      i = a;
      j = b;
      if (i > j) {
        return bool;
      }
      return false;
    }
    i = a;
    int j = b;
    if (i < j) {
      return bool;
    }
    return false;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = paramObject instanceof a;
    if (bool)
    {
      bool = a();
      if (bool)
      {
        Object localObject = paramObject;
        localObject = (a)paramObject;
        bool = ((a)localObject).a();
        if (bool) {}
      }
      else
      {
        int i = a;
        paramObject = (a)paramObject;
        int j = a;
        if (i != j) {
          break label92;
        }
        i = b;
        j = b;
        if (i != j) {
          break label92;
        }
        i = d;
        int k = d;
        if (i != k) {
          break label92;
        }
      }
      return true;
    }
    label92:
    return false;
  }
  
  public int hashCode()
  {
    boolean bool = a();
    if (bool) {
      return -1;
    }
    int i = a * '\037';
    int j = b;
    i = (i + j) * 31;
    j = d;
    return i + j;
  }
  
  public String toString()
  {
    int i = d;
    StringBuilder localStringBuilder;
    String str;
    if (i > 0)
    {
      localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      char c1 = a;
      localStringBuilder.append(c1);
      localStringBuilder.append("..");
      c1 = b;
      localStringBuilder.append(c1);
      str = " step ";
      localStringBuilder.append(str);
    }
    int k;
    for (int j = d;; k = -d)
    {
      localStringBuilder.append(j);
      return localStringBuilder.toString();
      localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      j = a;
      localStringBuilder.append(j);
      localStringBuilder.append(" downTo ");
      char c2 = b;
      localStringBuilder.append(c2);
      str = " step ";
      localStringBuilder.append(str);
    }
  }
}

/* Location:
 * Qualified Name:     c.k.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */