package c;

import c.g.a.a;
import java.io.Serializable;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

final class q
  implements f, Serializable
{
  public static final q.a a;
  private static final AtomicReferenceFieldUpdater e = AtomicReferenceFieldUpdater.newUpdater(q.class, Object.class, "c");
  private volatile a b;
  private volatile Object c;
  private final Object d;
  
  static
  {
    q.a locala = new c/q$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public q(a parama)
  {
    b = parama;
    parama = v.a;
    c = parama;
    parama = v.a;
    d = parama;
  }
  
  private final Object writeReplace()
  {
    d locald = new c/d;
    Object localObject = b();
    locald.<init>(localObject);
    return locald;
  }
  
  public final boolean a()
  {
    Object localObject = c;
    v localv = v.a;
    return localObject != localv;
  }
  
  public final Object b()
  {
    Object localObject1 = c;
    Object localObject2 = v.a;
    if (localObject1 != localObject2) {
      return localObject1;
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      localObject1 = ((a)localObject1).invoke();
      localObject2 = e;
      v localv = v.a;
      boolean bool = ((AtomicReferenceFieldUpdater)localObject2).compareAndSet(this, localv, localObject1);
      if (bool)
      {
        b = null;
        return localObject1;
      }
    }
    return c;
  }
  
  public final String toString()
  {
    boolean bool = a();
    if (bool) {
      return String.valueOf(b());
    }
    return "Lazy value not initialized yet.";
  }
}

/* Location:
 * Qualified Name:     c.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */