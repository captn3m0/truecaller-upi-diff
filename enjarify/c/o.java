package c;

import c.g.b.k;
import java.io.Serializable;

public final class o
  implements Serializable
{
  public static final o.a a;
  private final Object b;
  
  static
  {
    o.a locala = new c/o$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public static final boolean a(Object paramObject)
  {
    boolean bool = paramObject instanceof o.b;
    return !bool;
  }
  
  public static final boolean b(Object paramObject)
  {
    return paramObject instanceof o.b;
  }
  
  public static final Throwable c(Object paramObject)
  {
    boolean bool = paramObject instanceof o.b;
    if (bool) {
      return a;
    }
    return null;
  }
  
  public static Object d(Object paramObject)
  {
    return paramObject;
  }
  
  public final boolean equals(Object paramObject)
  {
    Object localObject = b;
    boolean bool1 = paramObject instanceof o;
    if (bool1)
    {
      paramObject = b;
      boolean bool2 = k.a(localObject, paramObject);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    Object localObject = b;
    if (localObject != null) {
      return localObject.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    Object localObject = b;
    boolean bool = localObject instanceof o.b;
    if (bool) {
      return localObject.toString();
    }
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Success(");
    localStringBuilder.append(localObject);
    localStringBuilder.append(')');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     c.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */