package c.a;

import c.g.b.k;
import c.n;
import c.u;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class aj
  extends ai
{
  public static final int a(int paramInt)
  {
    int i = 3;
    if (paramInt < i) {
      return paramInt + 1;
    }
    i = 1073741824;
    if (paramInt < i)
    {
      i = paramInt / 3;
      return paramInt + i;
    }
    return -1 >>> 1;
  }
  
  public static final Map a()
  {
    Object localObject = z.a;
    if (localObject != null) {
      return (Map)localObject;
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type kotlin.collections.Map<K, V>");
    throw ((Throwable)localObject);
  }
  
  public static final Map a(Iterable paramIterable)
  {
    Object localObject1 = "receiver$0";
    k.b(paramIterable, (String)localObject1);
    boolean bool1 = paramIterable instanceof Collection;
    if (bool1)
    {
      localObject1 = paramIterable;
      localObject1 = (Collection)paramIterable;
      int j = ((Collection)localObject1).size();
      switch (j)
      {
      default: 
        Object localObject2 = new java/util/LinkedHashMap;
        int i = ag.a(((Collection)localObject1).size());
        ((LinkedHashMap)localObject2).<init>(i);
        localObject2 = (Map)localObject2;
        return ag.a((Iterable)paramIterable, (Map)localObject2);
      case 1: 
        boolean bool2 = paramIterable instanceof List;
        if (bool2)
        {
          paramIterable = (List)paramIterable;
          bool2 = false;
          localObject1 = null;
          paramIterable = paramIterable.get(0);
        }
        else
        {
          paramIterable = ((Iterable)paramIterable).iterator().next();
        }
        return ag.a((n)paramIterable);
      }
      return ag.a();
    }
    localObject1 = new java/util/LinkedHashMap;
    ((LinkedHashMap)localObject1).<init>();
    localObject1 = (Map)localObject1;
    return ag.b(ag.a(paramIterable, (Map)localObject1));
  }
  
  public static final Map a(Iterable paramIterable, Map paramMap)
  {
    k.b(paramIterable, "receiver$0");
    k.b(paramMap, "destination");
    ag.a(paramMap, paramIterable);
    return paramMap;
  }
  
  public static final Map a(Map paramMap, n paramn)
  {
    k.b(paramMap, "receiver$0");
    Object localObject = "pair";
    k.b(paramn, (String)localObject);
    boolean bool = paramMap.isEmpty();
    if (bool) {
      return ag.a(paramn);
    }
    localObject = new java/util/LinkedHashMap;
    ((LinkedHashMap)localObject).<init>(paramMap);
    paramMap = a;
    paramn = b;
    ((LinkedHashMap)localObject).put(paramMap, paramn);
    return (Map)localObject;
  }
  
  public static final Map a(n... paramVarArgs)
  {
    Object localObject = "pairs";
    k.b(paramVarArgs, (String)localObject);
    int i = paramVarArgs.length;
    if (i > 0)
    {
      localObject = new java/util/LinkedHashMap;
      int j = ag.a(paramVarArgs.length);
      ((LinkedHashMap)localObject).<init>(j);
      localObject = (Map)localObject;
      return ag.a(paramVarArgs, (Map)localObject);
    }
    return ag.a();
  }
  
  public static final Map a(n[] paramArrayOfn, Map paramMap)
  {
    k.b(paramArrayOfn, "receiver$0");
    k.b(paramMap, "destination");
    ag.a(paramMap, paramArrayOfn);
    return paramMap;
  }
  
  public static final void a(Map paramMap, Iterable paramIterable)
  {
    k.b(paramMap, "receiver$0");
    Object localObject1 = "pairs";
    k.b(paramIterable, (String)localObject1);
    paramIterable = paramIterable.iterator();
    for (;;)
    {
      boolean bool = paramIterable.hasNext();
      if (!bool) {
        break;
      }
      localObject1 = (n)paramIterable.next();
      Object localObject2 = a;
      localObject1 = b;
      paramMap.put(localObject2, localObject1);
    }
  }
  
  public static final void a(Map paramMap, n[] paramArrayOfn)
  {
    k.b(paramMap, "receiver$0");
    String str = "pairs";
    k.b(paramArrayOfn, str);
    int i = paramArrayOfn.length;
    int j = 0;
    while (j < i)
    {
      Object localObject1 = paramArrayOfn[j];
      Object localObject2 = a;
      localObject1 = b;
      paramMap.put(localObject2, localObject1);
      j += 1;
    }
  }
  
  public static final Object b(Map paramMap, Object paramObject)
  {
    k.b(paramMap, "receiver$0");
    return ag.a(paramMap, paramObject);
  }
  
  public static final Map b(Map paramMap)
  {
    String str = "receiver$0";
    k.b(paramMap, str);
    int i = paramMap.size();
    switch (i)
    {
    default: 
      return paramMap;
    case 1: 
      return ag.a(paramMap);
    }
    return ag.a();
  }
  
  public static final Map b(n... paramVarArgs)
  {
    k.b(paramVarArgs, "pairs");
    Object localObject = new java/util/LinkedHashMap;
    int i = ag.a(paramVarArgs.length);
    ((LinkedHashMap)localObject).<init>(i);
    localObject = (Map)localObject;
    ag.a((Map)localObject, paramVarArgs);
    return (Map)localObject;
  }
  
  public static final HashMap c(n... paramVarArgs)
  {
    k.b(paramVarArgs, "pairs");
    HashMap localHashMap = new java/util/HashMap;
    int i = ag.a(1);
    localHashMap.<init>(i);
    ag.a((Map)localHashMap, paramVarArgs);
    return localHashMap;
  }
}

/* Location:
 * Qualified Name:     c.a.aj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */