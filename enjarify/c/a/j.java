package c.a;

import c.g.a.b;
import c.g.b.k;
import c.m.e;
import c.n;
import c.t;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

public class j
  extends i
{
  public static final long a(long[] paramArrayOfLong)
  {
    String str = "receiver$0";
    k.b(paramArrayOfLong, str);
    int i = paramArrayOfLong.length;
    if (i == 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      str = null;
    }
    if (i == 0)
    {
      i = f.c(paramArrayOfLong);
      return paramArrayOfLong[i];
    }
    paramArrayOfLong = new java/util/NoSuchElementException;
    paramArrayOfLong.<init>("Array is empty.");
    throw ((Throwable)paramArrayOfLong);
  }
  
  public static final Appendable a(Object[] paramArrayOfObject, Appendable paramAppendable, CharSequence paramCharSequence1, CharSequence paramCharSequence2, CharSequence paramCharSequence3, int paramInt, CharSequence paramCharSequence4, b paramb)
  {
    k.b(paramArrayOfObject, "receiver$0");
    k.b(paramAppendable, "buffer");
    k.b(paramCharSequence1, "separator");
    k.b(paramCharSequence2, "prefix");
    k.b(paramCharSequence3, "postfix");
    k.b(paramCharSequence4, "truncated");
    paramAppendable.append(paramCharSequence2);
    int i = paramArrayOfObject.length;
    int j = 0;
    int k = 0;
    while (j < i)
    {
      Object localObject = paramArrayOfObject[j];
      k += 1;
      int m = 1;
      if (k > m) {
        paramAppendable.append(paramCharSequence1);
      }
      if ((paramInt >= 0) && (k > paramInt)) {
        break;
      }
      c.n.m.a(paramAppendable, localObject, paramb);
      j += 1;
    }
    if ((paramInt >= 0) && (k > paramInt)) {
      paramAppendable.append(paramCharSequence4);
    }
    paramAppendable.append(paramCharSequence3);
    return paramAppendable;
  }
  
  public static final String a(Object[] paramArrayOfObject, CharSequence paramCharSequence1, CharSequence paramCharSequence2, CharSequence paramCharSequence3, int paramInt, CharSequence paramCharSequence4, b paramb)
  {
    k.b(paramArrayOfObject, "receiver$0");
    k.b(paramCharSequence1, "separator");
    k.b(paramCharSequence2, "prefix");
    k.b(paramCharSequence3, "postfix");
    k.b(paramCharSequence4, "truncated");
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    Object localObject = localStringBuilder;
    localObject = (Appendable)localStringBuilder;
    paramArrayOfObject = ((StringBuilder)f.a(paramArrayOfObject, (Appendable)localObject, paramCharSequence1, paramCharSequence2, paramCharSequence3, paramInt, paramCharSequence4, paramb)).toString();
    k.a(paramArrayOfObject, "joinTo(StringBuilder(), …ed, transform).toString()");
    return paramArrayOfObject;
  }
  
  public static final Collection a(Object[] paramArrayOfObject, Collection paramCollection)
  {
    k.b(paramArrayOfObject, "receiver$0");
    String str = "destination";
    k.b(paramCollection, str);
    int i = paramArrayOfObject.length;
    int j = 0;
    while (j < i)
    {
      Object localObject = paramArrayOfObject[j];
      if (localObject != null) {
        paramCollection.add(localObject);
      }
      j += 1;
    }
    return paramCollection;
  }
  
  public static final List a(Object[] paramArrayOfObject, Iterable paramIterable)
  {
    k.b(paramArrayOfObject, "receiver$0");
    String str = "other";
    k.b(paramIterable, str);
    int i = paramArrayOfObject.length;
    int j = Math.min(m.a(paramIterable, 10), i);
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>(j);
    paramIterable = paramIterable.iterator();
    j = 0;
    n localn = null;
    for (;;)
    {
      boolean bool = paramIterable.hasNext();
      if (!bool) {
        break;
      }
      Object localObject = paramIterable.next();
      if (j >= i) {
        break;
      }
      int k = j + 1;
      localn = t.a(paramArrayOfObject[j], localObject);
      localArrayList.add(localn);
      j = k;
    }
    return (List)localArrayList;
  }
  
  public static final boolean a(int[] paramArrayOfInt, int paramInt)
  {
    String str = "receiver$0";
    k.b(paramArrayOfInt, str);
    int i = f.b(paramArrayOfInt, paramInt);
    return i >= 0;
  }
  
  public static final int b(int[] paramArrayOfInt)
  {
    String str = "receiver$0";
    k.b(paramArrayOfInt, str);
    int i = paramArrayOfInt.length;
    if (i == 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      str = null;
    }
    if (i == 0) {
      return paramArrayOfInt[0];
    }
    paramArrayOfInt = new java/util/NoSuchElementException;
    paramArrayOfInt.<init>("Array is empty.");
    throw ((Throwable)paramArrayOfInt);
  }
  
  public static final int b(int[] paramArrayOfInt, int paramInt)
  {
    String str = "receiver$0";
    k.b(paramArrayOfInt, str);
    int i = paramArrayOfInt.length;
    int j = 0;
    while (j < i)
    {
      int k = paramArrayOfInt[j];
      if (paramInt == k) {
        return j;
      }
      j += 1;
    }
    return -1;
  }
  
  public static final Object b(Object[] paramArrayOfObject)
  {
    String str = "receiver$0";
    k.b(paramArrayOfObject, str);
    int i = paramArrayOfObject.length;
    if (i == 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      str = null;
    }
    if (i == 0) {
      return paramArrayOfObject[0];
    }
    paramArrayOfObject = new java/util/NoSuchElementException;
    paramArrayOfObject.<init>("Array is empty.");
    throw ((Throwable)paramArrayOfObject);
  }
  
  public static final Collection b(Object[] paramArrayOfObject, Collection paramCollection)
  {
    k.b(paramArrayOfObject, "receiver$0");
    String str = "destination";
    k.b(paramCollection, str);
    int i = paramArrayOfObject.length;
    int j = 0;
    while (j < i)
    {
      Object localObject = paramArrayOfObject[j];
      paramCollection.add(localObject);
      j += 1;
    }
    return paramCollection;
  }
  
  public static final List b(long[] paramArrayOfLong)
  {
    Object localObject = "receiver$0";
    k.b(paramArrayOfLong, (String)localObject);
    int i = paramArrayOfLong.length;
    int j = 100;
    if (j >= i) {
      return f.d(paramArrayOfLong);
    }
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>(j);
    int k = paramArrayOfLong.length;
    int m = 0;
    int n = 0;
    Long localLong = null;
    while (m < k)
    {
      long l = paramArrayOfLong[m];
      int i1 = n + 1;
      if (n == j) {
        break;
      }
      localLong = Long.valueOf(l);
      ((ArrayList)localObject).add(localLong);
      m += 1;
      n = i1;
    }
    return (List)localObject;
  }
  
  public static final boolean b(Object[] paramArrayOfObject, Object paramObject)
  {
    String str = "receiver$0";
    k.b(paramArrayOfObject, str);
    int i = f.c(paramArrayOfObject, paramObject);
    return i >= 0;
  }
  
  public static final int c(long[] paramArrayOfLong)
  {
    k.b(paramArrayOfLong, "receiver$0");
    return paramArrayOfLong.length + -1;
  }
  
  public static final int c(Object[] paramArrayOfObject, Object paramObject)
  {
    k.b(paramArrayOfObject, "receiver$0");
    int i = 0;
    if (paramObject == null)
    {
      int j = paramArrayOfObject.length;
      while (i < j)
      {
        Object localObject1 = paramArrayOfObject[i];
        if (localObject1 == null) {
          return i;
        }
        i += 1;
      }
    }
    int k = paramArrayOfObject.length;
    while (i < k)
    {
      Object localObject2 = paramArrayOfObject[i];
      boolean bool = k.a(paramObject, localObject2);
      if (bool) {
        return i;
      }
      i += 1;
    }
    return -1;
  }
  
  public static final Object c(Object[] paramArrayOfObject)
  {
    String str = "receiver$0";
    k.b(paramArrayOfObject, str);
    int i = paramArrayOfObject.length;
    if (i == 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      str = null;
    }
    if (i != 0) {
      return null;
    }
    return paramArrayOfObject[0];
  }
  
  public static final List d(long[] paramArrayOfLong)
  {
    String str = "receiver$0";
    k.b(paramArrayOfLong, str);
    int i = paramArrayOfLong.length;
    switch (i)
    {
    default: 
      return f.e(paramArrayOfLong);
    case 1: 
      return m.a(Long.valueOf(paramArrayOfLong[0]));
    }
    return (List)y.a;
  }
  
  public static final List d(Object[] paramArrayOfObject)
  {
    k.b(paramArrayOfObject, "receiver$0");
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    localObject = (Collection)localObject;
    return (List)f.a(paramArrayOfObject, (Collection)localObject);
  }
  
  public static final int e(Object[] paramArrayOfObject)
  {
    k.b(paramArrayOfObject, "receiver$0");
    return paramArrayOfObject.length + -1;
  }
  
  public static final List e(long[] paramArrayOfLong)
  {
    k.b(paramArrayOfLong, "receiver$0");
    ArrayList localArrayList = new java/util/ArrayList;
    int i = paramArrayOfLong.length;
    localArrayList.<init>(i);
    i = paramArrayOfLong.length;
    int j = 0;
    while (j < i)
    {
      long l = paramArrayOfLong[j];
      Long localLong = Long.valueOf(l);
      localArrayList.add(localLong);
      j += 1;
    }
    return (List)localArrayList;
  }
  
  public static final List f(Object[] paramArrayOfObject)
  {
    String str = "receiver$0";
    k.b(paramArrayOfObject, str);
    int i = paramArrayOfObject.length;
    switch (i)
    {
    default: 
      return f.g(paramArrayOfObject);
    case 1: 
      return m.a(paramArrayOfObject[0]);
    }
    return (List)y.a;
  }
  
  public static final List g(Object[] paramArrayOfObject)
  {
    k.b(paramArrayOfObject, "receiver$0");
    ArrayList localArrayList = new java/util/ArrayList;
    paramArrayOfObject = m.a(paramArrayOfObject);
    localArrayList.<init>(paramArrayOfObject);
    return (List)localArrayList;
  }
  
  public static final Set h(Object[] paramArrayOfObject)
  {
    Object localObject = "receiver$0";
    k.b(paramArrayOfObject, (String)localObject);
    int i = paramArrayOfObject.length;
    switch (i)
    {
    default: 
      localObject = new java/util/LinkedHashSet;
      int j = ag.a(paramArrayOfObject.length);
      ((LinkedHashSet)localObject).<init>(j);
      localObject = (Collection)localObject;
      return (Set)f.b(paramArrayOfObject, (Collection)localObject);
    case 1: 
      return an.a(paramArrayOfObject[0]);
    }
    return (Set)aa.a;
  }
  
  public static final Iterable i(Object[] paramArrayOfObject)
  {
    Object localObject = "receiver$0";
    k.b(paramArrayOfObject, (String)localObject);
    int i = paramArrayOfObject.length;
    if (i == 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      return (Iterable)y.a;
    }
    localObject = new c/a/j$a;
    ((j.a)localObject).<init>(paramArrayOfObject);
    return (Iterable)localObject;
  }
  
  public static final c.m.i j(Object[] paramArrayOfObject)
  {
    Object localObject = "receiver$0";
    k.b(paramArrayOfObject, (String)localObject);
    int i = paramArrayOfObject.length;
    if (i == 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      return (c.m.i)e.a;
    }
    localObject = new c/a/j$c;
    ((j.c)localObject).<init>(paramArrayOfObject);
    return (c.m.i)localObject;
  }
}

/* Location:
 * Qualified Name:     c.a.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */