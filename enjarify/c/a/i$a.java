package c.a;

import c.g.b.k;
import c.k.h;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;

public final class i$a
  extends d
  implements RandomAccess
{
  i$a(int[] paramArrayOfInt) {}
  
  public final int a()
  {
    return b.length;
  }
  
  public final boolean contains(Object paramObject)
  {
    boolean bool = paramObject instanceof Integer;
    if (!bool) {
      return false;
    }
    int i = ((Number)paramObject).intValue();
    return f.a(b, i);
  }
  
  public final int indexOf(Object paramObject)
  {
    boolean bool = paramObject instanceof Integer;
    if (!bool) {
      return -1;
    }
    int i = ((Number)paramObject).intValue();
    return f.b(b, i);
  }
  
  public final boolean isEmpty()
  {
    int[] arrayOfInt = b;
    int i = arrayOfInt.length;
    return i == 0;
  }
  
  public final int lastIndexOf(Object paramObject)
  {
    boolean bool1 = paramObject instanceof Integer;
    int i = -1;
    if (!bool1) {
      return i;
    }
    paramObject = (Number)paramObject;
    int j = ((Number)paramObject).intValue();
    int[] arrayOfInt = b;
    k.b(arrayOfInt, "receiver$0");
    k.b(arrayOfInt, "receiver$0");
    Object localObject = new c/k/h;
    boolean bool2 = false;
    Number localNumber = null;
    String str = "receiver$0";
    k.b(arrayOfInt, str);
    int m = arrayOfInt.length + -1;
    ((h)localObject).<init>(0, m);
    localObject = m.f((Iterable)localObject).iterator();
    int k;
    do
    {
      bool2 = ((Iterator)localObject).hasNext();
      if (!bool2) {
        break;
      }
      localNumber = (Number)((Iterator)localObject).next();
      k = localNumber.intValue();
      m = arrayOfInt[k];
    } while (j != m);
    return k;
    return i;
  }
}

/* Location:
 * Qualified Name:     c.a.i.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */