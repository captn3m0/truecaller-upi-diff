package c.a;

import c.g.b.a.a;
import java.util.Iterator;
import java.util.NoSuchElementException;

class d$b
  implements a, Iterator
{
  int a;
  
  public d$b(d paramd) {}
  
  public boolean hasNext()
  {
    int i = a;
    d locald = b;
    int j = locald.size();
    return i < j;
  }
  
  public Object next()
  {
    boolean bool = hasNext();
    if (bool)
    {
      localObject = b;
      int i = a;
      int j = i + 1;
      a = j;
      return ((d)localObject).get(i);
    }
    Object localObject = new java/util/NoSuchElementException;
    ((NoSuchElementException)localObject).<init>();
    throw ((Throwable)localObject);
  }
  
  public void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
}

/* Location:
 * Qualified Name:     c.a.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */