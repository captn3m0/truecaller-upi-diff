package c.a;

import c.g.b.k;

public final class ab
{
  public final int a;
  public final Object b;
  
  public ab(int paramInt, Object paramObject)
  {
    a = paramInt;
    b = paramObject;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof ab;
      if (bool2)
      {
        paramObject = (ab)paramObject;
        int i = a;
        int j = a;
        Object localObject;
        if (i == j)
        {
          i = 1;
        }
        else
        {
          i = 0;
          localObject = null;
        }
        if (i != 0)
        {
          localObject = b;
          paramObject = b;
          boolean bool3 = k.a(localObject, paramObject);
          if (bool3) {
            return bool1;
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    int i = a * 31;
    Object localObject = b;
    int j;
    if (localObject != null)
    {
      j = localObject.hashCode();
    }
    else
    {
      j = 0;
      localObject = null;
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("IndexedValue(index=");
    int i = a;
    localStringBuilder.append(i);
    localStringBuilder.append(", value=");
    Object localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     c.a.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */