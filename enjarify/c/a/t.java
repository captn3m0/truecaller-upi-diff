package c.a;

import c.g.b.aa;
import c.g.b.k;
import java.util.Collection;
import java.util.Iterator;

public class t
  extends s
{
  public static final boolean a(Collection paramCollection, Iterable paramIterable)
  {
    k.b(paramCollection, "receiver$0");
    String str = "elements";
    k.b(paramIterable, str);
    boolean bool1 = paramIterable instanceof Collection;
    if (bool1)
    {
      paramIterable = (Collection)paramIterable;
      return paramCollection.addAll(paramIterable);
    }
    bool1 = false;
    str = null;
    paramIterable = paramIterable.iterator();
    for (;;)
    {
      boolean bool2 = paramIterable.hasNext();
      if (!bool2) {
        break;
      }
      Object localObject = paramIterable.next();
      bool2 = paramCollection.add(localObject);
      if (bool2) {
        bool1 = true;
      }
    }
    return bool1;
  }
  
  public static final boolean a(Collection paramCollection, Object[] paramArrayOfObject)
  {
    k.b(paramCollection, "receiver$0");
    k.b(paramArrayOfObject, "elements");
    paramArrayOfObject = (Collection)f.a(paramArrayOfObject);
    return paramCollection.addAll(paramArrayOfObject);
  }
  
  public static final boolean b(Collection paramCollection, Iterable paramIterable)
  {
    k.b(paramCollection, "receiver$0");
    k.b(paramIterable, "elements");
    Object localObject = paramCollection;
    localObject = (Iterable)paramCollection;
    paramIterable = m.a(paramIterable, (Iterable)localObject);
    return aa.a(paramCollection).retainAll(paramIterable);
  }
}

/* Location:
 * Qualified Name:     c.a.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */