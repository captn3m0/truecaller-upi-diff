package c.a;

import c.d.a.a;
import c.d.b.a.j;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

final class ar$a
  extends j
  implements m
{
  Object a;
  Object b;
  Object c;
  Object d;
  int e;
  int f;
  int g;
  private c.m.k m;
  
  ar$a(int paramInt1, int paramInt2, Iterator paramIterator, boolean paramBoolean1, boolean paramBoolean2, c paramc)
  {
    super(paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new c/a/ar$a;
    int n = h;
    int i1 = i;
    Iterator localIterator = j;
    boolean bool1 = k;
    boolean bool2 = l;
    locala.<init>(n, i1, localIterator, bool1, bool2, paramc);
    paramObject = (c.m.k)paramObject;
    m = ((c.m.k)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int n = g;
    int i3 = 1;
    boolean bool4;
    int i4;
    Object localObject3;
    Object localObject4;
    int i8;
    Object localObject5;
    boolean bool7;
    Object localObject6;
    int i10;
    switch (n)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 5: 
      bool4 = paramObject instanceof o.b;
      if (!bool4) {
        break label1136;
      }
      throw a;
    case 4: 
      localObject2 = (am)b;
      i4 = e;
      localObject3 = (c.m.k)a;
      boolean bool5 = paramObject instanceof o.b;
      if (!bool5)
      {
        paramObject = this;
        break label1051;
      }
      throw a;
    case 3: 
      localObject2 = (Iterator)d;
      localObject4 = (am)b;
      i8 = e;
      localObject5 = (c.m.k)a;
      bool7 = paramObject instanceof o.b;
      if (!bool7)
      {
        paramObject = this;
        break label879;
      }
      throw a;
    case 2: 
      bool4 = paramObject instanceof o.b;
      if (!bool4) {
        break label1136;
      }
      throw a;
    case 1: 
      localObject2 = (Iterator)d;
      localObject4 = (ArrayList)b;
      i8 = e;
      localObject5 = (c.m.k)a;
      bool7 = paramObject instanceof o.b;
      if (!bool7)
      {
        localObject6 = localObject1;
        localObject1 = this;
      }
      else
      {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label1140;
      }
      paramObject = m;
      int i1 = h;
      i4 = i;
      i1 -= i4;
      if (i1 < 0) {
        break label620;
      }
      localObject3 = new java/util/ArrayList;
      ((ArrayList)localObject3).<init>(i4);
      i4 = 0;
      localObject5 = j;
      localObject6 = localObject1;
      localObject4 = localObject3;
      localObject1 = this;
      i8 = i1;
      localObject2 = localObject5;
      localObject5 = paramObject;
      i10 = 0;
      paramObject = null;
    }
    boolean bool8;
    Object localObject7;
    int i14;
    int i15;
    int i11;
    for (;;)
    {
      bool8 = ((Iterator)localObject2).hasNext();
      if (!bool8) {
        break;
      }
      localObject7 = ((Iterator)localObject2).next();
      if (i10 > 0)
      {
        i10 += -1;
      }
      else
      {
        ((ArrayList)localObject4).add(localObject7);
        i14 = ((ArrayList)localObject4).size();
        i15 = i;
        if (i14 == i15)
        {
          a = localObject5;
          e = i8;
          b = localObject4;
          f = i10;
          c = localObject7;
          d = localObject2;
          g = i3;
          paramObject = ((c.m.k)localObject5).a(localObject4, (c)localObject1);
          if (paramObject == localObject6) {
            return localObject6;
          }
          i11 = k;
          if (i11 != 0)
          {
            ((ArrayList)localObject4).clear();
          }
          else
          {
            paramObject = new java/util/ArrayList;
            i4 = i;
            ((ArrayList)paramObject).<init>(i4);
            localObject4 = paramObject;
          }
          i11 = i8;
        }
      }
    }
    Object localObject2 = localObject4;
    localObject2 = (Collection)localObject4;
    boolean bool2 = ((Collection)localObject2).isEmpty() ^ i3;
    if (bool2)
    {
      bool2 = l;
      int i2;
      if (!bool2)
      {
        i2 = ((ArrayList)localObject4).size();
        i3 = i;
        if (i2 != i3) {}
      }
      else
      {
        e = i8;
        a = localObject4;
        f = i11;
        int i12 = 2;
        g = i12;
        paramObject = ((c.m.k)localObject5).a(localObject4, (c)localObject1);
        if (paramObject == localObject6)
        {
          return localObject6;
          label620:
          localObject3 = new c/a/am;
          ((am)localObject3).<init>(i4);
          localObject4 = j;
          localObject5 = paramObject;
          paramObject = this;
          i8 = i2;
          localObject2 = localObject4;
          localObject4 = localObject3;
          int i9;
          for (;;)
          {
            bool7 = ((Iterator)localObject2).hasNext();
            if (!bool7) {
              break label910;
            }
            localObject6 = ((Iterator)localObject2).next();
            bool8 = ((am)localObject4).b();
            if (bool8) {
              break;
            }
            localObject7 = b;
            i14 = c;
            i15 = ((am)localObject4).size();
            i14 += i15;
            i15 = e;
            i14 %= i15;
            localObject7[i14] = localObject6;
            int i13 = ((am)localObject4).size() + i3;
            d = i13;
            boolean bool9 = ((am)localObject4).b();
            if (bool9)
            {
              bool9 = k;
              if (bool9)
              {
                localObject7 = localObject4;
                localObject7 = (List)localObject4;
              }
              else
              {
                localObject7 = new java/util/ArrayList;
                Object localObject8 = localObject4;
                localObject8 = (Collection)localObject4;
                ((ArrayList)localObject7).<init>((Collection)localObject8);
                localObject7 = (List)localObject7;
              }
              a = localObject5;
              e = i8;
              b = localObject4;
              c = localObject6;
              d = localObject2;
              i9 = 3;
              g = i9;
              localObject6 = ((c.m.k)localObject5).a(localObject7, (c)paramObject);
              if (localObject6 == localObject1) {
                return localObject1;
              }
              label879:
              i9 = h;
              ((am)localObject4).a(i9);
            }
          }
          paramObject = new java/lang/IllegalStateException;
          ((IllegalStateException)paramObject).<init>("ring buffer is full");
          throw ((Throwable)paramObject);
          label910:
          boolean bool3 = l;
          if (bool3)
          {
            localObject2 = localObject4;
            i4 = i8;
            localObject3 = localObject5;
            for (;;)
            {
              int i5 = ((am)localObject2).size();
              i9 = h;
              if (i5 <= i9) {
                break;
              }
              boolean bool6 = k;
              if (bool6)
              {
                localObject5 = localObject2;
                localObject5 = (List)localObject2;
              }
              else
              {
                localObject5 = new java/util/ArrayList;
                localObject6 = localObject2;
                localObject6 = (Collection)localObject2;
                ((ArrayList)localObject5).<init>((Collection)localObject6);
                localObject5 = (List)localObject5;
              }
              a = localObject3;
              e = i4;
              b = localObject2;
              i9 = 4;
              g = i9;
              localObject5 = ((c.m.k)localObject3).a(localObject5, (c)paramObject);
              if (localObject5 == localObject1) {
                return localObject1;
              }
              label1051:
              int i6 = h;
              ((am)localObject2).a(i6);
            }
            localObject5 = localObject2;
            localObject5 = (Collection)localObject2;
            int i7 = ((Collection)localObject5).isEmpty();
            i3 ^= i7;
            if (i3 != 0)
            {
              e = i4;
              a = localObject2;
              i3 = 5;
              g = i3;
              paramObject = ((c.m.k)localObject3).a(localObject2, (c)paramObject);
              if (paramObject == localObject1) {
                return localObject1;
              }
            }
          }
        }
      }
    }
    label1136:
    return x.a;
    label1140:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     c.a.ar.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */