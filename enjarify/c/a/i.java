package c.a;

import c.g.b.k;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class i
  extends h
{
  public static final List a(int[] paramArrayOfInt)
  {
    k.b(paramArrayOfInt, "receiver$0");
    i.a locala = new c/a/i$a;
    locala.<init>(paramArrayOfInt);
    return (List)locala;
  }
  
  public static final List a(Object[] paramArrayOfObject)
  {
    k.b(paramArrayOfObject, "receiver$0");
    paramArrayOfObject = Arrays.asList(paramArrayOfObject);
    k.a(paramArrayOfObject, "ArraysUtilJVM.asList(this)");
    return paramArrayOfObject;
  }
  
  public static final void a(Object[] paramArrayOfObject, Comparator paramComparator)
  {
    k.b(paramArrayOfObject, "receiver$0");
    String str = "comparator";
    k.b(paramComparator, str);
    int i = paramArrayOfObject.length;
    int j = 1;
    if (i > j) {
      Arrays.sort(paramArrayOfObject, paramComparator);
    }
  }
  
  public static final Object[] a(Object[] paramArrayOfObject, Object paramObject)
  {
    k.b(paramArrayOfObject, "receiver$0");
    int i = paramArrayOfObject.length;
    int j = i + 1;
    paramArrayOfObject = Arrays.copyOf(paramArrayOfObject, j);
    paramArrayOfObject[i] = paramObject;
    k.a(paramArrayOfObject, "result");
    return paramArrayOfObject;
  }
  
  public static final Object[] a(Object[] paramArrayOfObject1, Object[] paramArrayOfObject2)
  {
    k.b(paramArrayOfObject1, "receiver$0");
    k.b(paramArrayOfObject2, "elements");
    int i = paramArrayOfObject1.length;
    int j = paramArrayOfObject2.length;
    int k = i + j;
    paramArrayOfObject1 = Arrays.copyOf(paramArrayOfObject1, k);
    System.arraycopy(paramArrayOfObject2, 0, paramArrayOfObject1, i, j);
    k.a(paramArrayOfObject1, "result");
    return paramArrayOfObject1;
  }
}

/* Location:
 * Qualified Name:     c.a.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */