package c.a;

import java.util.Iterator;

public final class ac
  implements c.g.b.a.a, Iterable
{
  private final c.g.a.a a;
  
  public ac(c.g.a.a parama)
  {
    a = parama;
  }
  
  public final Iterator iterator()
  {
    ad localad = new c/a/ad;
    Iterator localIterator = (Iterator)a.invoke();
    localad.<init>(localIterator);
    return (Iterator)localad;
  }
}

/* Location:
 * Qualified Name:     c.a.ac
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */