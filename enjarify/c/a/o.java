package c.a;

import c.g.b.k;
import c.k.h;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class o
  extends n
{
  public static final int a(List paramList)
  {
    k.b(paramList, "receiver$0");
    return paramList.size() + -1;
  }
  
  public static final h a(Collection paramCollection)
  {
    k.b(paramCollection, "receiver$0");
    h localh = new c/k/h;
    int i = paramCollection.size() + -1;
    localh.<init>(0, i);
    return localh;
  }
  
  public static final Collection a(Object[] paramArrayOfObject)
  {
    k.b(paramArrayOfObject, "receiver$0");
    e locale = new c/a/e;
    locale.<init>(paramArrayOfObject, false);
    return (Collection)locale;
  }
  
  public static final void a()
  {
    ArithmeticException localArithmeticException = new java/lang/ArithmeticException;
    localArithmeticException.<init>("Index overflow has happened.");
    throw ((Throwable)localArithmeticException);
  }
  
  public static final List b(Object paramObject)
  {
    if (paramObject != null) {
      return m.a(paramObject);
    }
    return (List)y.a;
  }
  
  public static final List b(List paramList)
  {
    String str = "receiver$0";
    k.b(paramList, str);
    int i = paramList.size();
    switch (i)
    {
    default: 
      return paramList;
    case 1: 
      return m.a(paramList.get(0));
    }
    return (List)y.a;
  }
  
  public static final List b(Object... paramVarArgs)
  {
    String str = "elements";
    k.b(paramVarArgs, str);
    int i = paramVarArgs.length;
    if (i > 0) {
      return f.a(paramVarArgs);
    }
    return (List)y.a;
  }
  
  public static final List c(Object... paramVarArgs)
  {
    Object localObject1 = "elements";
    k.b(paramVarArgs, (String)localObject1);
    int i = paramVarArgs.length;
    if (i == 0)
    {
      paramVarArgs = new java/util/ArrayList;
      paramVarArgs.<init>();
      return (List)paramVarArgs;
    }
    localObject1 = new java/util/ArrayList;
    Object localObject2 = new c/a/e;
    ((e)localObject2).<init>(paramVarArgs, true);
    localObject2 = (Collection)localObject2;
    ((ArrayList)localObject1).<init>((Collection)localObject2);
    return (List)localObject1;
  }
  
  public static final ArrayList d(Object... paramVarArgs)
  {
    Object localObject1 = "elements";
    k.b(paramVarArgs, (String)localObject1);
    int i = paramVarArgs.length;
    if (i == 0)
    {
      paramVarArgs = new java/util/ArrayList;
      paramVarArgs.<init>();
      return paramVarArgs;
    }
    localObject1 = new java/util/ArrayList;
    Object localObject2 = new c/a/e;
    ((e)localObject2).<init>(paramVarArgs, true);
    localObject2 = (Collection)localObject2;
    ((ArrayList)localObject1).<init>((Collection)localObject2);
    return (ArrayList)localObject1;
  }
  
  public static final List e(Object... paramVarArgs)
  {
    k.b(paramVarArgs, "elements");
    return f.d(paramVarArgs);
  }
}

/* Location:
 * Qualified Name:     c.a.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */