package c.a;

import c.g.b.k;
import java.util.Map;
import java.util.NoSuchElementException;

class ah
{
  public static final Object a(Map paramMap, Object paramObject)
  {
    Object localObject = "receiver$0";
    k.b(paramMap, (String)localObject);
    boolean bool1 = paramMap instanceof af;
    if (bool1) {
      return ((af)paramMap).a();
    }
    localObject = paramMap.get(paramObject);
    if (localObject == null)
    {
      boolean bool2 = paramMap.containsKey(paramObject);
      if (!bool2)
      {
        paramMap = new java/util/NoSuchElementException;
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>("Key ");
        ((StringBuilder)localObject).append(paramObject);
        ((StringBuilder)localObject).append(" is missing in the map.");
        paramObject = ((StringBuilder)localObject).toString();
        paramMap.<init>((String)paramObject);
        throw ((Throwable)paramMap);
      }
    }
    return localObject;
  }
}

/* Location:
 * Qualified Name:     c.a.ah
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */