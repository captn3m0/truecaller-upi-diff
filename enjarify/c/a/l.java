package c.a;

import c.g.b.a.a;
import java.util.Iterator;

public abstract class l
  implements a, Iterator
{
  public abstract char a();
  
  public void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
}

/* Location:
 * Qualified Name:     c.a.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */