package c.a;

import c.g.b.a.a;
import java.util.ListIterator;

public final class x
  implements a, ListIterator
{
  public static final x a;
  
  static
  {
    x localx = new c/a/x;
    localx.<init>();
    a = localx;
  }
  
  public final boolean hasNext()
  {
    return false;
  }
  
  public final boolean hasPrevious()
  {
    return false;
  }
  
  public final int nextIndex()
  {
    return 0;
  }
  
  public final int previousIndex()
  {
    return -1;
  }
  
  public final void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
}

/* Location:
 * Qualified Name:     c.a.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */