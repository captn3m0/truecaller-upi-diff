package c.a;

import c.g.b.k;
import c.u;
import java.util.Arrays;
import java.util.Iterator;
import java.util.RandomAccess;

final class am
  extends d
  implements RandomAccess
{
  final Object[] b;
  int c;
  int d;
  final int e;
  
  public am(int paramInt)
  {
    e = paramInt;
    paramInt = e;
    if (paramInt >= 0)
    {
      paramInt = 1;
    }
    else
    {
      paramInt = 0;
      localObject = null;
    }
    if (paramInt != 0)
    {
      localObject = new Object[e];
      b = ((Object[])localObject);
      return;
    }
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("ring buffer capacity should not be negative but it is ");
    int i = e;
    ((StringBuilder)localObject).append(i);
    localObject = ((StringBuilder)localObject).toString();
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    localObject = localObject.toString();
    localIllegalArgumentException.<init>((String)localObject);
    throw ((Throwable)localIllegalArgumentException);
  }
  
  private static void a(Object[] paramArrayOfObject, int paramInt1, int paramInt2)
  {
    while (paramInt1 < paramInt2)
    {
      paramArrayOfObject[paramInt1] = null;
      paramInt1 += 1;
    }
  }
  
  public final int a()
  {
    return d;
  }
  
  public final void a(int paramInt)
  {
    int i = 1;
    Object[] arrayOfObject1 = null;
    int j;
    if (paramInt >= 0) {
      j = 1;
    } else {
      j = 0;
    }
    if (j != 0)
    {
      j = size();
      if (paramInt > j)
      {
        i = 0;
        localObject = null;
      }
      if (i != 0)
      {
        if (paramInt > 0)
        {
          i = c;
          j = i + paramInt;
          int k = e;
          j %= k;
          if (i > j)
          {
            Object[] arrayOfObject2 = b;
            a(arrayOfObject2, i, k);
            localObject = b;
            a((Object[])localObject, 0, j);
          }
          else
          {
            arrayOfObject1 = b;
            a(arrayOfObject1, i, j);
          }
          c = j;
          i = size() - paramInt;
          d = i;
        }
        return;
      }
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>("n shouldn't be greater than the buffer size: n = ");
      ((StringBuilder)localObject).append(paramInt);
      ((StringBuilder)localObject).append(", size = ");
      paramInt = size();
      ((StringBuilder)localObject).append(paramInt);
      str = ((StringBuilder)localObject).toString();
      localObject = new java/lang/IllegalArgumentException;
      str = str.toString();
      ((IllegalArgumentException)localObject).<init>(str);
      throw ((Throwable)localObject);
    }
    String str = String.valueOf(paramInt);
    str = "n shouldn't be negative but it is ".concat(str);
    Object localObject = new java/lang/IllegalArgumentException;
    str = str.toString();
    ((IllegalArgumentException)localObject).<init>(str);
    throw ((Throwable)localObject);
  }
  
  public final boolean b()
  {
    int i = size();
    int j = e;
    return i == j;
  }
  
  public final Object get(int paramInt)
  {
    int i = size();
    d.a.a(paramInt, i);
    Object[] arrayOfObject = b;
    int j = c + paramInt;
    paramInt = e;
    j %= paramInt;
    return arrayOfObject[j];
  }
  
  public final Iterator iterator()
  {
    am.a locala = new c/a/am$a;
    locala.<init>(this);
    return (Iterator)locala;
  }
  
  public final Object[] toArray()
  {
    Object[] arrayOfObject = new Object[size()];
    return toArray(arrayOfObject);
  }
  
  public final Object[] toArray(Object[] paramArrayOfObject)
  {
    String str = "array";
    k.b(paramArrayOfObject, str);
    int i = paramArrayOfObject.length;
    int j = size();
    if (i < j)
    {
      i = size();
      paramArrayOfObject = Arrays.copyOf(paramArrayOfObject, i);
      str = "java.util.Arrays.copyOf(this, newSize)";
      k.a(paramArrayOfObject, str);
    }
    i = size();
    j = c;
    int k = 0;
    int m = j;
    j = 0;
    while (j < i)
    {
      int n = e;
      if (m >= n) {
        break;
      }
      Object localObject1 = b[m];
      paramArrayOfObject[j] = localObject1;
      j += 1;
      m += 1;
    }
    while (j < i)
    {
      Object localObject2 = b[k];
      paramArrayOfObject[j] = localObject2;
      j += 1;
      k += 1;
    }
    i = paramArrayOfObject.length;
    j = size();
    if (i > j)
    {
      i = size();
      j = 0;
      paramArrayOfObject[i] = null;
    }
    if (paramArrayOfObject != null) {
      return paramArrayOfObject;
    }
    paramArrayOfObject = new c/u;
    paramArrayOfObject.<init>("null cannot be cast to non-null type kotlin.Array<T>");
    throw paramArrayOfObject;
  }
}

/* Location:
 * Qualified Name:     c.a.am
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */