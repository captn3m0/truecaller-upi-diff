package c.a;

import c.g.b.a.a;
import c.g.b.g;
import c.g.b.k;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public final class aa
  implements a, Serializable, Set
{
  public static final aa a;
  private static final long serialVersionUID = 3406603774387020532L;
  
  static
  {
    aa localaa = new c/a/aa;
    localaa.<init>();
    a = localaa;
  }
  
  private final Object readResolve()
  {
    return a;
  }
  
  public final boolean addAll(Collection paramCollection)
  {
    paramCollection = new java/lang/UnsupportedOperationException;
    paramCollection.<init>("Operation is not supported for read-only collection");
    throw paramCollection;
  }
  
  public final void clear()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
  
  public final boolean contains(Object paramObject)
  {
    boolean bool = paramObject instanceof Void;
    if (!bool) {
      return false;
    }
    k.b((Void)paramObject, "element");
    return false;
  }
  
  public final boolean containsAll(Collection paramCollection)
  {
    k.b(paramCollection, "elements");
    return paramCollection.isEmpty();
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof Set;
    if (bool1)
    {
      paramObject = (Set)paramObject;
      boolean bool2 = ((Set)paramObject).isEmpty();
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    return 0;
  }
  
  public final boolean isEmpty()
  {
    return true;
  }
  
  public final Iterator iterator()
  {
    return (Iterator)x.a;
  }
  
  public final boolean remove(Object paramObject)
  {
    paramObject = new java/lang/UnsupportedOperationException;
    ((UnsupportedOperationException)paramObject).<init>("Operation is not supported for read-only collection");
    throw ((Throwable)paramObject);
  }
  
  public final boolean removeAll(Collection paramCollection)
  {
    paramCollection = new java/lang/UnsupportedOperationException;
    paramCollection.<init>("Operation is not supported for read-only collection");
    throw paramCollection;
  }
  
  public final boolean retainAll(Collection paramCollection)
  {
    paramCollection = new java/lang/UnsupportedOperationException;
    paramCollection.<init>("Operation is not supported for read-only collection");
    throw paramCollection;
  }
  
  public final Object[] toArray()
  {
    return g.a(this);
  }
  
  public final Object[] toArray(Object[] paramArrayOfObject)
  {
    return g.a(this, paramArrayOfObject);
  }
  
  public final String toString()
  {
    return "[]";
  }
}

/* Location:
 * Qualified Name:     c.a.aa
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */