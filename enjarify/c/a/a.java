package c.a;

import c.g.a.b;
import c.g.b.g;
import c.g.b.k;
import c.u;
import java.util.Collection;
import java.util.Iterator;

public abstract class a
  implements c.g.b.a.a, Collection
{
  public abstract int a();
  
  public boolean add(Object paramObject)
  {
    paramObject = new java/lang/UnsupportedOperationException;
    ((UnsupportedOperationException)paramObject).<init>("Operation is not supported for read-only collection");
    throw ((Throwable)paramObject);
  }
  
  public boolean addAll(Collection paramCollection)
  {
    paramCollection = new java/lang/UnsupportedOperationException;
    paramCollection.<init>("Operation is not supported for read-only collection");
    throw paramCollection;
  }
  
  public void clear()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
  
  public boolean contains(Object paramObject)
  {
    Object localObject1 = this;
    localObject1 = (Collection)this;
    boolean bool1 = ((Collection)localObject1).isEmpty();
    if (!bool1)
    {
      localObject1 = iterator();
      boolean bool2;
      do
      {
        bool2 = ((Iterator)localObject1).hasNext();
        if (!bool2) {
          break;
        }
        Object localObject2 = ((Iterator)localObject1).next();
        bool2 = k.a(localObject2, paramObject);
      } while (!bool2);
      return true;
    }
    return false;
  }
  
  public boolean containsAll(Collection paramCollection)
  {
    k.b(paramCollection, "elements");
    paramCollection = (Iterable)paramCollection;
    Object localObject = paramCollection;
    localObject = (Collection)paramCollection;
    boolean bool = ((Collection)localObject).isEmpty();
    if (!bool)
    {
      paramCollection = paramCollection.iterator();
      do
      {
        bool = paramCollection.hasNext();
        if (!bool) {
          break;
        }
        localObject = paramCollection.next();
        bool = contains(localObject);
      } while (bool);
      return false;
    }
    return true;
  }
  
  public boolean isEmpty()
  {
    int i = size();
    return i == 0;
  }
  
  public boolean remove(Object paramObject)
  {
    paramObject = new java/lang/UnsupportedOperationException;
    ((UnsupportedOperationException)paramObject).<init>("Operation is not supported for read-only collection");
    throw ((Throwable)paramObject);
  }
  
  public boolean removeAll(Collection paramCollection)
  {
    paramCollection = new java/lang/UnsupportedOperationException;
    paramCollection.<init>("Operation is not supported for read-only collection");
    throw paramCollection;
  }
  
  public boolean retainAll(Collection paramCollection)
  {
    paramCollection = new java/lang/UnsupportedOperationException;
    paramCollection.<init>("Operation is not supported for read-only collection");
    throw paramCollection;
  }
  
  public final int size()
  {
    return a();
  }
  
  public Object[] toArray()
  {
    return g.a((Collection)this);
  }
  
  public Object[] toArray(Object[] paramArrayOfObject)
  {
    k.b(paramArrayOfObject, "array");
    Object localObject = this;
    localObject = (Collection)this;
    paramArrayOfObject = g.a((Collection)localObject, paramArrayOfObject);
    if (paramArrayOfObject != null) {
      return paramArrayOfObject;
    }
    paramArrayOfObject = new c/u;
    paramArrayOfObject.<init>("null cannot be cast to non-null type kotlin.Array<T>");
    throw paramArrayOfObject;
  }
  
  public String toString()
  {
    CharSequence localCharSequence1 = (CharSequence)", ";
    CharSequence localCharSequence2 = (CharSequence)"[";
    CharSequence localCharSequence3 = (CharSequence)"]";
    a.a locala = new c/a/a$a;
    locala.<init>(this);
    Object localObject = locala;
    localObject = (b)locala;
    return m.a(this, localCharSequence1, localCharSequence2, localCharSequence3, 0, null, (b)localObject, 24);
  }
}

/* Location:
 * Qualified Name:     c.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */