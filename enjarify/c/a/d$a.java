package c.a;

public final class d$a
{
  public static void a(int paramInt1, int paramInt2)
  {
    if ((paramInt1 >= 0) && (paramInt1 < paramInt2)) {
      return;
    }
    IndexOutOfBoundsException localIndexOutOfBoundsException = new java/lang/IndexOutOfBoundsException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("index: ");
    localStringBuilder.append(paramInt1);
    localStringBuilder.append(", size: ");
    localStringBuilder.append(paramInt2);
    String str = localStringBuilder.toString();
    localIndexOutOfBoundsException.<init>(str);
    throw ((Throwable)localIndexOutOfBoundsException);
  }
}

/* Location:
 * Qualified Name:     c.a.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */