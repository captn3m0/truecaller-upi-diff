package c.a;

import c.g.b.a.a;
import java.util.Iterator;

public final class ad
  implements a, Iterator
{
  private int a;
  private final Iterator b;
  
  public ad(Iterator paramIterator)
  {
    b = paramIterator;
  }
  
  public final boolean hasNext()
  {
    return b.hasNext();
  }
  
  public final void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
}

/* Location:
 * Qualified Name:     c.a.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */