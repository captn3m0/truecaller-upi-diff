package c.a;

import c.g.b.a.a;
import java.util.ListIterator;
import java.util.NoSuchElementException;

final class d$c
  extends d.b
  implements a, ListIterator
{
  public d$c(d paramd, int paramInt)
  {
    super(paramd);
    Object localObject = d.a;
    int i = paramd.size();
    if ((paramInt >= 0) && (paramInt <= i))
    {
      a = paramInt;
      return;
    }
    localObject = new java/lang/IndexOutOfBoundsException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("index: ");
    localStringBuilder.append(paramInt);
    localStringBuilder.append(", size: ");
    localStringBuilder.append(i);
    paramd = localStringBuilder.toString();
    ((IndexOutOfBoundsException)localObject).<init>(paramd);
    throw ((Throwable)localObject);
  }
  
  public final void add(Object paramObject)
  {
    paramObject = new java/lang/UnsupportedOperationException;
    ((UnsupportedOperationException)paramObject).<init>("Operation is not supported for read-only collection");
    throw ((Throwable)paramObject);
  }
  
  public final boolean hasPrevious()
  {
    int i = a;
    return i > 0;
  }
  
  public final int nextIndex()
  {
    return a;
  }
  
  public final Object previous()
  {
    boolean bool = hasPrevious();
    if (bool)
    {
      localObject = c;
      int i = a + -1;
      a = i;
      i = a;
      return ((d)localObject).get(i);
    }
    Object localObject = new java/util/NoSuchElementException;
    ((NoSuchElementException)localObject).<init>();
    throw ((Throwable)localObject);
  }
  
  public final int previousIndex()
  {
    return a + -1;
  }
  
  public final void set(Object paramObject)
  {
    paramObject = new java/lang/UnsupportedOperationException;
    ((UnsupportedOperationException)paramObject).<init>("Operation is not supported for read-only collection");
    throw ((Throwable)paramObject);
  }
}

/* Location:
 * Qualified Name:     c.a.d.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */