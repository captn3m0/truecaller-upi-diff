package c.a;

import c.g.a.m;
import c.g.b.k;
import c.m.i;
import c.m.l;
import java.util.Iterator;

public final class ar
{
  public static final i a(i parami, int paramInt1, int paramInt2)
  {
    k.b(parami, "receiver$0");
    a(paramInt1, paramInt2);
    ar.b localb = new c/a/ar$b;
    localb.<init>(parami, paramInt1, paramInt2);
    return (i)localb;
  }
  
  public static final Iterator a(Iterator paramIterator, int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
  {
    Object localObject = "iterator";
    k.b(paramIterator, (String)localObject);
    boolean bool = paramIterator.hasNext();
    if (!bool) {
      return (Iterator)x.a;
    }
    ar.a locala = new c/a/ar$a;
    localObject = locala;
    locala.<init>(paramInt2, paramInt1, paramIterator, paramBoolean2, paramBoolean1, null);
    return l.b((m)locala);
  }
  
  public static final void a(int paramInt1, int paramInt2)
  {
    int i;
    Object localObject1;
    if ((paramInt1 > 0) && (paramInt2 > 0))
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject1 = null;
    }
    if (i == 0)
    {
      if (paramInt1 != paramInt2)
      {
        localObject1 = new java/lang/StringBuilder;
        String str1 = "Both size ";
        ((StringBuilder)localObject1).<init>(str1);
        ((StringBuilder)localObject1).append(paramInt1);
        ((StringBuilder)localObject1).append(" and step ");
        ((StringBuilder)localObject1).append(paramInt2);
        ((StringBuilder)localObject1).append(" must be greater than zero.");
        str2 = ((StringBuilder)localObject1).toString();
      }
      else
      {
        localObject2 = new java/lang/StringBuilder;
        localObject1 = "size ";
        ((StringBuilder)localObject2).<init>((String)localObject1);
        ((StringBuilder)localObject2).append(paramInt1);
        ((StringBuilder)localObject2).append(" must be greater than zero.");
        str2 = ((StringBuilder)localObject2).toString();
      }
      Object localObject2 = new java/lang/IllegalArgumentException;
      String str2 = str2.toString();
      ((IllegalArgumentException)localObject2).<init>(str2);
      throw ((Throwable)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     c.a.ar
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */