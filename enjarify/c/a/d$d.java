package c.a;

import java.util.RandomAccess;

final class d$d
  extends d
  implements RandomAccess
{
  private int b;
  private final d c;
  private final int d;
  
  public d$d(d paramd, int paramInt1, int paramInt2)
  {
    c = paramd;
    d = paramInt1;
    int i = d;
    Object localObject2 = c;
    paramInt1 = ((d)localObject2).size();
    if ((i >= 0) && (paramInt2 <= paramInt1))
    {
      if (i <= paramInt2)
      {
        i = d;
        paramInt2 -= i;
        b = paramInt2;
        return;
      }
      localObject2 = new java/lang/IllegalArgumentException;
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("fromIndex: ");
      ((StringBuilder)localObject1).append(i);
      ((StringBuilder)localObject1).append(" > toIndex: ");
      ((StringBuilder)localObject1).append(paramInt2);
      paramd = ((StringBuilder)localObject1).toString();
      ((IllegalArgumentException)localObject2).<init>(paramd);
      throw ((Throwable)localObject2);
    }
    localObject1 = new java/lang/IndexOutOfBoundsException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("fromIndex: ");
    localStringBuilder.append(i);
    localStringBuilder.append(", toIndex: ");
    localStringBuilder.append(paramInt2);
    localStringBuilder.append(", size: ");
    localStringBuilder.append(paramInt1);
    paramd = localStringBuilder.toString();
    ((IndexOutOfBoundsException)localObject1).<init>(paramd);
    throw ((Throwable)localObject1);
  }
  
  public final int a()
  {
    return b;
  }
  
  public final Object get(int paramInt)
  {
    int i = b;
    d.a.a(paramInt, i);
    d locald = c;
    int j = d + paramInt;
    return locald.get(j);
  }
}

/* Location:
 * Qualified Name:     c.a.d.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */