package c.a;

import c.g.b.a.a;
import java.util.Iterator;
import java.util.NoSuchElementException;

public abstract class b
  implements a, Iterator
{
  protected as a;
  private Object b;
  
  public b()
  {
    as localas = as.b;
    a = localas;
  }
  
  protected abstract void a();
  
  protected final void a(Object paramObject)
  {
    b = paramObject;
    paramObject = as.a;
    a = ((as)paramObject);
  }
  
  public boolean hasNext()
  {
    Object localObject1 = a;
    Object localObject2 = as.d;
    boolean bool = true;
    int i;
    if (localObject1 != localObject2)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject1 = null;
    }
    if (i != 0)
    {
      localObject1 = a;
      localObject2 = c.a;
      i = ((as)localObject1).ordinal();
      i = localObject2[i];
      switch (i)
      {
      default: 
        localObject1 = as.d;
        a = ((as)localObject1);
        a();
        localObject1 = a;
        localObject2 = as.a;
        if (localObject1 == localObject2) {
          return bool;
        }
        break;
      case 2: 
        return bool;
      case 1: 
        return false;
      }
      return false;
    }
    localObject1 = new java/lang/IllegalArgumentException;
    localObject2 = "Failed requirement.".toString();
    ((IllegalArgumentException)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
  }
  
  public Object next()
  {
    boolean bool = hasNext();
    if (bool)
    {
      localObject = as.b;
      a = ((as)localObject);
      return b;
    }
    Object localObject = new java/util/NoSuchElementException;
    ((NoSuchElementException)localObject).<init>();
    throw ((Throwable)localObject);
  }
  
  public void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
}

/* Location:
 * Qualified Name:     c.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */