package c.a;

import c.g.b.a.a;
import c.g.b.k;
import java.io.Serializable;
import java.util.Map;

final class z
  implements a, Serializable, Map
{
  public static final z a;
  private static final long serialVersionUID = 8246714829545688274L;
  
  static
  {
    z localz = new c/a/z;
    localz.<init>();
    a = localz;
  }
  
  private final Object readResolve()
  {
    return a;
  }
  
  public final void clear()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
  
  public final boolean containsKey(Object paramObject)
  {
    return false;
  }
  
  public final boolean containsValue(Object paramObject)
  {
    boolean bool = paramObject instanceof Void;
    if (!bool) {
      return false;
    }
    k.b((Void)paramObject, "value");
    return false;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof Map;
    if (bool1)
    {
      paramObject = (Map)paramObject;
      boolean bool2 = ((Map)paramObject).isEmpty();
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    return 0;
  }
  
  public final boolean isEmpty()
  {
    return true;
  }
  
  public final void putAll(Map paramMap)
  {
    paramMap = new java/lang/UnsupportedOperationException;
    paramMap.<init>("Operation is not supported for read-only collection");
    throw paramMap;
  }
  
  public final Object remove(Object paramObject)
  {
    paramObject = new java/lang/UnsupportedOperationException;
    ((UnsupportedOperationException)paramObject).<init>("Operation is not supported for read-only collection");
    throw ((Throwable)paramObject);
  }
  
  public final String toString()
  {
    return "{}";
  }
}

/* Location:
 * Qualified Name:     c.a.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */