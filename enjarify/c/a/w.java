package c.a;

import c.g.a.a;
import c.g.a.b;
import c.g.b.k;
import c.t;
import c.u;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.RandomAccess;
import java.util.Set;

public class w
  extends v
{
  public static final Appendable a(Iterable paramIterable, Appendable paramAppendable, CharSequence paramCharSequence1, CharSequence paramCharSequence2, CharSequence paramCharSequence3, int paramInt, CharSequence paramCharSequence4, b paramb)
  {
    k.b(paramIterable, "receiver$0");
    k.b(paramAppendable, "buffer");
    k.b(paramCharSequence1, "separator");
    k.b(paramCharSequence2, "prefix");
    k.b(paramCharSequence3, "postfix");
    Object localObject = "truncated";
    k.b(paramCharSequence4, (String)localObject);
    paramAppendable.append(paramCharSequence2);
    paramIterable = paramIterable.iterator();
    int i = 0;
    paramCharSequence2 = null;
    for (;;)
    {
      boolean bool = paramIterable.hasNext();
      if (!bool) {
        break;
      }
      localObject = paramIterable.next();
      i += 1;
      int j = 1;
      if (i > j) {
        paramAppendable.append(paramCharSequence1);
      }
      if ((paramInt >= 0) && (i > paramInt)) {
        break;
      }
      c.n.m.a(paramAppendable, localObject, paramb);
    }
    if ((paramInt >= 0) && (i > paramInt)) {
      paramAppendable.append(paramCharSequence4);
    }
    paramAppendable.append(paramCharSequence3);
    return paramAppendable;
  }
  
  public static final Object a(Iterable paramIterable, int paramInt, b paramb)
  {
    k.b(paramIterable, "receiver$0");
    String str = "defaultValue";
    k.b(paramb, str);
    boolean bool1 = paramIterable instanceof List;
    if (bool1)
    {
      paramIterable = (List)paramIterable;
      if (paramInt >= 0)
      {
        i = m.a(paramIterable);
        if (paramInt <= i) {
          return paramIterable.get(paramInt);
        }
      }
      paramIterable = Integer.valueOf(paramInt);
      return paramb.invoke(paramIterable);
    }
    if (paramInt < 0)
    {
      paramIterable = Integer.valueOf(paramInt);
      return paramb.invoke(paramIterable);
    }
    paramIterable = paramIterable.iterator();
    int i = 0;
    str = null;
    for (;;)
    {
      boolean bool2 = paramIterable.hasNext();
      if (!bool2) {
        break;
      }
      Object localObject = paramIterable.next();
      int j = i + 1;
      if (paramInt == i) {
        return localObject;
      }
      i = j;
    }
    paramIterable = Integer.valueOf(paramInt);
    return paramb.invoke(paramIterable);
  }
  
  public static final Object a(List paramList, int paramInt)
  {
    String str = "receiver$0";
    k.b(paramList, str);
    if (paramInt >= 0)
    {
      int i = m.a(paramList);
      if (paramInt <= i) {
        return paramList.get(paramInt);
      }
    }
    return null;
  }
  
  public static final String a(Iterable paramIterable, CharSequence paramCharSequence1, CharSequence paramCharSequence2, CharSequence paramCharSequence3, int paramInt, CharSequence paramCharSequence4, b paramb)
  {
    k.b(paramIterable, "receiver$0");
    k.b(paramCharSequence1, "separator");
    k.b(paramCharSequence2, "prefix");
    k.b(paramCharSequence3, "postfix");
    k.b(paramCharSequence4, "truncated");
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    Object localObject = localStringBuilder;
    localObject = (Appendable)localStringBuilder;
    paramIterable = ((StringBuilder)m.a(paramIterable, (Appendable)localObject, paramCharSequence1, paramCharSequence2, paramCharSequence3, paramInt, paramCharSequence4, paramb)).toString();
    k.a(paramIterable, "joinTo(StringBuilder(), …ed, transform).toString()");
    return paramIterable;
  }
  
  public static final Collection a(Iterable paramIterable, Collection paramCollection)
  {
    k.b(paramIterable, "receiver$0");
    Object localObject = "destination";
    k.b(paramCollection, (String)localObject);
    paramIterable = paramIterable.iterator();
    for (;;)
    {
      boolean bool = paramIterable.hasNext();
      if (!bool) {
        break;
      }
      localObject = paramIterable.next();
      if (localObject != null) {
        paramCollection.add(localObject);
      }
    }
    return paramCollection;
  }
  
  public static final List a(Iterable paramIterable, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    Object localObject1 = "receiver$0";
    k.b(paramIterable, (String)localObject1);
    ar.a(paramInt1, paramInt2);
    boolean bool = paramIterable instanceof RandomAccess;
    if (bool)
    {
      bool = paramIterable instanceof List;
      if (bool)
      {
        paramIterable = (List)paramIterable;
        int i = paramIterable.size();
        ArrayList localArrayList = new java/util/ArrayList;
        int j = (i + paramInt2 + -1) / paramInt2;
        localArrayList.<init>(j);
        j = 0;
        while (j < i)
        {
          int k = i - j;
          k = c.k.i.d(paramInt1, k);
          if ((k < paramInt1) && (!paramBoolean)) {
            break;
          }
          Object localObject2 = new java/util/ArrayList;
          ((ArrayList)localObject2).<init>(k);
          int m = 0;
          while (m < k)
          {
            int n = m + j;
            Object localObject3 = paramIterable.get(n);
            ((ArrayList)localObject2).add(localObject3);
            m += 1;
          }
          localObject2 = (List)localObject2;
          localArrayList.add(localObject2);
          j += paramInt2;
        }
        return (List)localArrayList;
      }
    }
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    paramIterable = ar.a(paramIterable.iterator(), paramInt1, paramInt2, paramBoolean, false);
    for (;;)
    {
      paramInt1 = paramIterable.hasNext();
      if (paramInt1 == 0) {
        break;
      }
      List localList = (List)paramIterable.next();
      ((ArrayList)localObject1).add(localList);
    }
    return (List)localObject1;
  }
  
  public static final List a(Iterable paramIterable, Comparator paramComparator)
  {
    k.b(paramIterable, "receiver$0");
    Object localObject = "comparator";
    k.b(paramComparator, (String)localObject);
    boolean bool = paramIterable instanceof Collection;
    if (bool)
    {
      localObject = paramIterable;
      localObject = (Collection)paramIterable;
      int i = ((Collection)localObject).size();
      int j = 1;
      if (i <= j) {
        return m.g((Iterable)paramIterable);
      }
      paramIterable = new Object[0];
      paramIterable = ((Collection)localObject).toArray(paramIterable);
      if (paramIterable != null)
      {
        if (paramIterable != null)
        {
          f.a(paramIterable, paramComparator);
          return f.a(paramIterable);
        }
        paramIterable = new c/u;
        paramIterable.<init>("null cannot be cast to non-null type kotlin.Array<T>");
        throw paramIterable;
      }
      paramIterable = new c/u;
      paramIterable.<init>("null cannot be cast to non-null type kotlin.Array<T>");
      throw paramIterable;
    }
    paramIterable = m.h(paramIterable);
    m.a(paramIterable, paramComparator);
    return paramIterable;
  }
  
  public static final List a(Collection paramCollection, Object paramObject)
  {
    k.b(paramCollection, "receiver$0");
    ArrayList localArrayList = new java/util/ArrayList;
    int i = paramCollection.size() + 1;
    localArrayList.<init>(i);
    localArrayList.addAll(paramCollection);
    localArrayList.add(paramObject);
    return (List)localArrayList;
  }
  
  public static final boolean a(Iterable paramIterable, Object paramObject)
  {
    String str = "receiver$0";
    k.b(paramIterable, str);
    boolean bool = paramIterable instanceof Collection;
    if (bool) {
      return ((Collection)paramIterable).contains(paramObject);
    }
    int i = m.b(paramIterable, paramObject);
    return i >= 0;
  }
  
  public static final int b(Iterable paramIterable, Object paramObject)
  {
    String str = "receiver$0";
    k.b(paramIterable, str);
    int i = paramIterable instanceof List;
    if (i != 0) {
      return ((List)paramIterable).indexOf(paramObject);
    }
    i = 0;
    str = null;
    paramIterable = paramIterable.iterator();
    for (;;)
    {
      boolean bool = paramIterable.hasNext();
      if (!bool) {
        break;
      }
      Object localObject = paramIterable.next();
      if (i < 0) {
        m.a();
      }
      bool = k.a(paramObject, localObject);
      if (bool) {
        return i;
      }
      int j;
      i += 1;
    }
    return -1;
  }
  
  public static final Object b(Iterable paramIterable)
  {
    String str = "receiver$0";
    k.b(paramIterable, str);
    boolean bool = paramIterable instanceof List;
    if (bool) {
      return m.d((List)paramIterable);
    }
    paramIterable = paramIterable.iterator();
    bool = paramIterable.hasNext();
    if (bool) {
      return paramIterable.next();
    }
    paramIterable = new java/util/NoSuchElementException;
    paramIterable.<init>("Collection is empty.");
    throw ((Throwable)paramIterable);
  }
  
  public static final Object b(Iterable paramIterable, int paramInt)
  {
    Object localObject = "receiver$0";
    k.b(paramIterable, (String)localObject);
    boolean bool = paramIterable instanceof List;
    if (bool) {
      return ((List)paramIterable).get(paramInt);
    }
    localObject = new c/a/w$b;
    ((w.b)localObject).<init>(paramInt);
    localObject = (b)localObject;
    return m.a(paramIterable, paramInt, (b)localObject);
  }
  
  public static final Collection b(Iterable paramIterable, Collection paramCollection)
  {
    k.b(paramIterable, "receiver$0");
    Object localObject = "destination";
    k.b(paramCollection, (String)localObject);
    paramIterable = paramIterable.iterator();
    for (;;)
    {
      boolean bool = paramIterable.hasNext();
      if (!bool) {
        break;
      }
      localObject = paramIterable.next();
      paramCollection.add(localObject);
    }
    return paramCollection;
  }
  
  public static final List b(List paramList, int paramInt)
  {
    Object localObject1 = "receiver$0";
    k.b(paramList, (String)localObject1);
    int i = 1;
    int j;
    if (paramInt >= 0) {
      j = 1;
    } else {
      j = 0;
    }
    if (j != 0)
    {
      if (paramInt == 0) {
        return (List)y.a;
      }
      j = paramList.size();
      if (paramInt >= j) {
        return m.g((Iterable)paramList);
      }
      if (paramInt == i) {
        return m.a(m.f(paramList));
      }
      localObject1 = new java/util/ArrayList;
      ((ArrayList)localObject1).<init>(paramInt);
      paramInt = j - paramInt;
      while (paramInt < j)
      {
        Object localObject2 = paramList.get(paramInt);
        ((ArrayList)localObject1).add(localObject2);
        paramInt += 1;
      }
      return (List)localObject1;
    }
    paramList = new java/lang/StringBuilder;
    paramList.<init>("Requested element count ");
    paramList.append(paramInt);
    paramList.append(" is less than zero.");
    paramList = paramList.toString();
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    paramList = paramList.toString();
    localIllegalArgumentException.<init>(paramList);
    throw ((Throwable)localIllegalArgumentException);
  }
  
  public static final Set b(Iterable paramIterable1, Iterable paramIterable2)
  {
    k.b(paramIterable1, "receiver$0");
    k.b(paramIterable2, "other");
    paramIterable1 = m.l(paramIterable1);
    m.b((Collection)paramIterable1, paramIterable2);
    return paramIterable1;
  }
  
  public static final int[] b(Collection paramCollection)
  {
    k.b(paramCollection, "receiver$0");
    int i = paramCollection.size();
    int[] arrayOfInt = new int[i];
    paramCollection = paramCollection.iterator();
    int m;
    for (int j = 0;; j = m)
    {
      boolean bool = paramCollection.hasNext();
      if (!bool) {
        break;
      }
      Number localNumber = (Number)paramCollection.next();
      int k = localNumber.intValue();
      m = j + 1;
      arrayOfInt[j] = k;
    }
    return arrayOfInt;
  }
  
  public static final Object c(Iterable paramIterable)
  {
    Object localObject = "receiver$0";
    k.b(paramIterable, (String)localObject);
    boolean bool1 = paramIterable instanceof List;
    if (bool1) {
      return m.f((List)paramIterable);
    }
    paramIterable = paramIterable.iterator();
    bool1 = paramIterable.hasNext();
    if (bool1)
    {
      boolean bool2;
      do
      {
        localObject = paramIterable.next();
        bool2 = paramIterable.hasNext();
      } while (bool2);
      return localObject;
    }
    paramIterable = new java/util/NoSuchElementException;
    paramIterable.<init>("Collection is empty.");
    throw ((Throwable)paramIterable);
  }
  
  public static final List c(Iterable paramIterable, int paramInt)
  {
    k.b(paramIterable, "receiver$0");
    boolean bool1 = false;
    int j = 1;
    boolean bool2;
    Object localObject1;
    if (paramInt >= 0)
    {
      bool2 = true;
    }
    else
    {
      bool2 = false;
      localObject1 = null;
    }
    if (bool2)
    {
      if (paramInt == 0) {
        return m.g(paramIterable);
      }
      bool2 = paramIterable instanceof Collection;
      ArrayList localArrayList;
      int i;
      if (bool2)
      {
        localObject1 = paramIterable;
        localObject1 = (Collection)paramIterable;
        int k = ((Collection)localObject1).size() - paramInt;
        if (k <= 0) {
          return (List)y.a;
        }
        if (k == j) {
          return m.a(m.c((Iterable)paramIterable));
        }
        localArrayList = new java/util/ArrayList;
        localArrayList.<init>(k);
        boolean bool3 = paramIterable instanceof List;
        if (bool3)
        {
          bool1 = paramIterable instanceof RandomAccess;
          if (bool1)
          {
            i = ((Collection)localObject1).size();
            while (paramInt < i)
            {
              localObject1 = paramIterable;
              localObject1 = ((List)paramIterable).get(paramInt);
              localArrayList.add(localObject1);
              paramInt += 1;
            }
          }
          paramIterable = (Iterator)((List)paramIterable).listIterator(paramInt);
          for (;;)
          {
            paramInt = paramIterable.hasNext();
            if (paramInt == 0) {
              break;
            }
            localObject2 = paramIterable.next();
            localArrayList.add(localObject2);
          }
          return (List)localArrayList;
        }
      }
      else
      {
        localArrayList = new java/util/ArrayList;
        localArrayList.<init>();
      }
      paramIterable = ((Iterable)paramIterable).iterator();
      for (;;)
      {
        bool2 = paramIterable.hasNext();
        if (!bool2) {
          break;
        }
        localObject1 = paramIterable.next();
        int m = i + 1;
        if (i >= paramInt) {
          localArrayList.add(localObject1);
        }
        i = m;
      }
      return m.b((List)localArrayList);
    }
    paramIterable = new java/lang/StringBuilder;
    paramIterable.<init>("Requested element count ");
    paramIterable.append(paramInt);
    paramIterable.append(" is less than zero.");
    paramIterable = paramIterable.toString();
    Object localObject2 = new java/lang/IllegalArgumentException;
    paramIterable = paramIterable.toString();
    ((IllegalArgumentException)localObject2).<init>(paramIterable);
    throw ((Throwable)localObject2);
  }
  
  public static final List c(Iterable paramIterable1, Iterable paramIterable2)
  {
    k.b(paramIterable1, "receiver$0");
    Object localObject1 = "elements";
    k.b(paramIterable2, (String)localObject1);
    paramIterable2 = m.a(paramIterable2, paramIterable1);
    boolean bool1 = paramIterable2.isEmpty();
    if (bool1) {
      return m.g(paramIterable1);
    }
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (Collection)localObject1;
    paramIterable1 = paramIterable1.iterator();
    for (;;)
    {
      boolean bool2 = paramIterable1.hasNext();
      if (!bool2) {
        break;
      }
      Object localObject2 = paramIterable1.next();
      boolean bool3 = paramIterable2.contains(localObject2);
      if (!bool3) {
        ((Collection)localObject1).add(localObject2);
      }
    }
    return (List)localObject1;
  }
  
  public static final List c(Iterable paramIterable, Object paramObject)
  {
    k.b(paramIterable, "receiver$0");
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    m.a((Collection)localArrayList, paramIterable);
    localArrayList.add(paramObject);
    return (List)localArrayList;
  }
  
  public static final List c(Collection paramCollection, Iterable paramIterable)
  {
    k.b(paramCollection, "receiver$0");
    Object localObject = "elements";
    k.b(paramIterable, (String)localObject);
    boolean bool = paramIterable instanceof Collection;
    if (bool)
    {
      localObject = new java/util/ArrayList;
      int i = paramCollection.size();
      paramIterable = (Collection)paramIterable;
      int j = paramIterable.size();
      i += j;
      ((ArrayList)localObject).<init>(i);
      ((ArrayList)localObject).addAll(paramCollection);
      ((ArrayList)localObject).addAll(paramIterable);
      return (List)localObject;
    }
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>(paramCollection);
    paramCollection = (Collection)localObject;
    m.a((Collection)localObject, paramIterable);
    return (List)localObject;
  }
  
  public static final long[] c(Collection paramCollection)
  {
    k.b(paramCollection, "receiver$0");
    int i = paramCollection.size();
    long[] arrayOfLong = new long[i];
    paramCollection = paramCollection.iterator();
    int k;
    for (int j = 0;; j = k)
    {
      boolean bool = paramCollection.hasNext();
      if (!bool) {
        break;
      }
      Number localNumber = (Number)paramCollection.next();
      long l = localNumber.longValue();
      k = j + 1;
      arrayOfLong[j] = l;
    }
    return arrayOfLong;
  }
  
  public static final Object d(Iterable paramIterable)
  {
    Object localObject = "receiver$0";
    k.b(paramIterable, (String)localObject);
    boolean bool1 = paramIterable instanceof List;
    boolean bool3 = false;
    if (bool1)
    {
      paramIterable = (List)paramIterable;
      bool1 = paramIterable.isEmpty();
      if (bool1) {
        return null;
      }
      int i = paramIterable.size() + -1;
      return paramIterable.get(i);
    }
    paramIterable = paramIterable.iterator();
    boolean bool2 = paramIterable.hasNext();
    if (!bool2) {
      return null;
    }
    do
    {
      localObject = paramIterable.next();
      bool3 = paramIterable.hasNext();
    } while (bool3);
    return localObject;
  }
  
  public static final Object d(List paramList)
  {
    String str = "receiver$0";
    k.b(paramList, str);
    boolean bool = paramList.isEmpty();
    if (!bool) {
      return paramList.get(0);
    }
    paramList = new java/util/NoSuchElementException;
    paramList.<init>("List is empty.");
    throw ((Throwable)paramList);
  }
  
  public static final List d(Iterable paramIterable, int paramInt)
  {
    k.b(paramIterable, "receiver$0");
    int i = 0;
    int j = 1;
    boolean bool1;
    Object localObject;
    if (paramInt >= 0)
    {
      bool1 = true;
    }
    else
    {
      bool1 = false;
      localObject = null;
    }
    if (bool1)
    {
      if (paramInt == 0) {
        return (List)y.a;
      }
      bool1 = paramIterable instanceof Collection;
      if (bool1)
      {
        localObject = paramIterable;
        localObject = (Collection)paramIterable;
        int k = ((Collection)localObject).size();
        if (paramInt >= k) {
          return m.g((Iterable)paramIterable);
        }
        if (paramInt == j) {
          return m.a(m.b((Iterable)paramIterable));
        }
      }
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>(paramInt);
      paramIterable = ((Iterable)paramIterable).iterator();
      for (;;)
      {
        boolean bool2 = paramIterable.hasNext();
        if (!bool2) {
          break;
        }
        localObject = paramIterable.next();
        int m = i + 1;
        if (i == paramInt) {
          break;
        }
        localArrayList.add(localObject);
        i = m;
      }
      return m.b((List)localArrayList);
    }
    paramIterable = new java/lang/StringBuilder;
    paramIterable.<init>("Requested element count ");
    paramIterable.append(paramInt);
    paramIterable.append(" is less than zero.");
    paramIterable = paramIterable.toString();
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    paramIterable = paramIterable.toString();
    localIllegalArgumentException.<init>(paramIterable);
    throw ((Throwable)localIllegalArgumentException);
  }
  
  public static final List d(Iterable paramIterable1, Iterable paramIterable2)
  {
    k.b(paramIterable1, "receiver$0");
    k.b(paramIterable2, "other");
    Iterator localIterator1 = paramIterable1.iterator();
    Iterator localIterator2 = paramIterable2.iterator();
    int i = 10;
    int j = m.a(paramIterable1, i);
    int k = m.a(paramIterable2, i);
    j = Math.min(j, k);
    paramIterable2 = new java/util/ArrayList;
    paramIterable2.<init>(j);
    for (;;)
    {
      boolean bool = localIterator1.hasNext();
      if (!bool) {
        break;
      }
      bool = localIterator2.hasNext();
      if (!bool) {
        break;
      }
      paramIterable1 = localIterator1.next();
      Object localObject = localIterator2.next();
      paramIterable1 = t.a(paramIterable1, localObject);
      paramIterable2.add(paramIterable1);
    }
    return (List)paramIterable2;
  }
  
  public static final List d(Collection paramCollection)
  {
    k.b(paramCollection, "receiver$0");
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>(paramCollection);
    return (List)localArrayList;
  }
  
  public static final Object e(List paramList)
  {
    String str = "receiver$0";
    k.b(paramList, str);
    boolean bool = paramList.isEmpty();
    if (bool) {
      return null;
    }
    return paramList.get(0);
  }
  
  public static final List e(Iterable paramIterable)
  {
    k.b(paramIterable, "receiver$0");
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    localObject = (Collection)localObject;
    return (List)m.a(paramIterable, (Collection)localObject);
  }
  
  public static final List e(Iterable paramIterable, int paramInt)
  {
    k.b(paramIterable, "receiver$0");
    return m.a(paramIterable, paramInt, paramInt, true);
  }
  
  public static final Object f(List paramList)
  {
    String str = "receiver$0";
    k.b(paramList, str);
    boolean bool = paramList.isEmpty();
    if (!bool)
    {
      int i = m.a(paramList);
      return paramList.get(i);
    }
    paramList = new java/util/NoSuchElementException;
    paramList.<init>("List is empty.");
    throw ((Throwable)paramList);
  }
  
  public static final List f(Iterable paramIterable)
  {
    Object localObject = "receiver$0";
    k.b(paramIterable, (String)localObject);
    boolean bool = paramIterable instanceof Collection;
    if (bool)
    {
      localObject = paramIterable;
      localObject = (Collection)paramIterable;
      int i = ((Collection)localObject).size();
      int j = 1;
      if (i <= j) {
        return m.g((Iterable)paramIterable);
      }
    }
    paramIterable = m.h((Iterable)paramIterable);
    m.c(paramIterable);
    return paramIterable;
  }
  
  public static final Object g(List paramList)
  {
    String str = "receiver$0";
    k.b(paramList, str);
    boolean bool = paramList.isEmpty();
    if (bool) {
      return null;
    }
    int i = paramList.size() + -1;
    return paramList.get(i);
  }
  
  public static final List g(Iterable paramIterable)
  {
    Object localObject = "receiver$0";
    k.b(paramIterable, (String)localObject);
    boolean bool = paramIterable instanceof Collection;
    if (bool)
    {
      localObject = paramIterable;
      localObject = (Collection)paramIterable;
      int i = ((Collection)localObject).size();
      switch (i)
      {
      default: 
        return m.d((Collection)localObject);
      case 1: 
        bool = paramIterable instanceof List;
        if (bool)
        {
          paramIterable = (List)paramIterable;
          bool = false;
          localObject = null;
          paramIterable = paramIterable.get(0);
        }
        else
        {
          paramIterable = ((Iterable)paramIterable).iterator().next();
        }
        return m.a(paramIterable);
      }
      return (List)y.a;
    }
    return m.b(m.h(paramIterable));
  }
  
  public static final Object h(List paramList)
  {
    String str = "receiver$0";
    k.b(paramList, str);
    int i = paramList.size();
    switch (i)
    {
    default: 
      paramList = new java/lang/IllegalArgumentException;
      paramList.<init>("List has more than one element.");
      throw ((Throwable)paramList);
    case 1: 
      return paramList.get(0);
    }
    paramList = new java/util/NoSuchElementException;
    paramList.<init>("List is empty.");
    throw ((Throwable)paramList);
  }
  
  public static final List h(Iterable paramIterable)
  {
    Object localObject = "receiver$0";
    k.b(paramIterable, (String)localObject);
    boolean bool = paramIterable instanceof Collection;
    if (bool) {
      return m.d((Collection)paramIterable);
    }
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    localObject = (Collection)localObject;
    return (List)m.b(paramIterable, (Collection)localObject);
  }
  
  public static final Set i(Iterable paramIterable)
  {
    Object localObject1 = "receiver$0";
    k.b(paramIterable, (String)localObject1);
    boolean bool1 = paramIterable instanceof Collection;
    if (bool1)
    {
      localObject1 = paramIterable;
      localObject1 = (Collection)paramIterable;
      int j = ((Collection)localObject1).size();
      switch (j)
      {
      default: 
        Object localObject2 = new java/util/LinkedHashSet;
        int i = ag.a(((Collection)localObject1).size());
        ((LinkedHashSet)localObject2).<init>(i);
        localObject2 = (Collection)localObject2;
        return (Set)m.b((Iterable)paramIterable, (Collection)localObject2);
      case 1: 
        boolean bool2 = paramIterable instanceof List;
        if (bool2)
        {
          paramIterable = (List)paramIterable;
          bool2 = false;
          localObject1 = null;
          paramIterable = paramIterable.get(0);
        }
        else
        {
          paramIterable = ((Iterable)paramIterable).iterator().next();
        }
        return an.a(paramIterable);
      }
      return (Set)aa.a;
    }
    localObject1 = new java/util/LinkedHashSet;
    ((LinkedHashSet)localObject1).<init>();
    localObject1 = (Collection)localObject1;
    return an.a((Set)m.b(paramIterable, (Collection)localObject1));
  }
  
  public static final Iterable j(Iterable paramIterable)
  {
    k.b(paramIterable, "receiver$0");
    ac localac = new c/a/ac;
    Object localObject = new c/a/w$c;
    ((w.c)localObject).<init>(paramIterable);
    localObject = (a)localObject;
    localac.<init>((a)localObject);
    return (Iterable)localac;
  }
  
  public static final List k(Iterable paramIterable)
  {
    k.b(paramIterable, "receiver$0");
    return m.g((Iterable)m.l(paramIterable));
  }
  
  public static final Set l(Iterable paramIterable)
  {
    Object localObject = "receiver$0";
    k.b(paramIterable, (String)localObject);
    boolean bool = paramIterable instanceof Collection;
    if (bool)
    {
      localObject = new java/util/LinkedHashSet;
      paramIterable = (Collection)paramIterable;
      ((LinkedHashSet)localObject).<init>(paramIterable);
      return (Set)localObject;
    }
    localObject = new java/util/LinkedHashSet;
    ((LinkedHashSet)localObject).<init>();
    localObject = (Collection)localObject;
    return (Set)m.b(paramIterable, (Collection)localObject);
  }
  
  public static final Comparable m(Iterable paramIterable)
  {
    Object localObject = "receiver$0";
    k.b(paramIterable, (String)localObject);
    paramIterable = paramIterable.iterator();
    boolean bool1 = paramIterable.hasNext();
    if (!bool1) {
      return null;
    }
    localObject = (Comparable)paramIterable.next();
    for (;;)
    {
      boolean bool2 = paramIterable.hasNext();
      if (!bool2) {
        break;
      }
      Comparable localComparable = (Comparable)paramIterable.next();
      int i = ((Comparable)localObject).compareTo(localComparable);
      if (i > 0) {
        localObject = localComparable;
      }
    }
    return (Comparable)localObject;
  }
  
  public static final c.m.i n(Iterable paramIterable)
  {
    k.b(paramIterable, "receiver$0");
    w.a locala = new c/a/w$a;
    locala.<init>(paramIterable);
    return (c.m.i)locala;
  }
}

/* Location:
 * Qualified Name:     c.a.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */