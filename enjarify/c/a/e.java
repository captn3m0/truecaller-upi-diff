package c.a;

import c.g.b.a.a;
import c.g.b.c;
import c.g.b.g;
import c.g.b.k;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

final class e
  implements a, Collection
{
  private final Object[] a;
  private final boolean b;
  
  public e(Object[] paramArrayOfObject, boolean paramBoolean)
  {
    a = paramArrayOfObject;
    b = paramBoolean;
  }
  
  public final boolean add(Object paramObject)
  {
    paramObject = new java/lang/UnsupportedOperationException;
    ((UnsupportedOperationException)paramObject).<init>("Operation is not supported for read-only collection");
    throw ((Throwable)paramObject);
  }
  
  public final boolean addAll(Collection paramCollection)
  {
    paramCollection = new java/lang/UnsupportedOperationException;
    paramCollection.<init>("Operation is not supported for read-only collection");
    throw paramCollection;
  }
  
  public final void clear()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
  
  public final boolean contains(Object paramObject)
  {
    return f.b(a, paramObject);
  }
  
  public final boolean containsAll(Collection paramCollection)
  {
    k.b(paramCollection, "elements");
    paramCollection = (Iterable)paramCollection;
    Object localObject = paramCollection;
    localObject = (Collection)paramCollection;
    boolean bool = ((Collection)localObject).isEmpty();
    if (!bool)
    {
      paramCollection = paramCollection.iterator();
      do
      {
        bool = paramCollection.hasNext();
        if (!bool) {
          break;
        }
        localObject = paramCollection.next();
        bool = contains(localObject);
      } while (bool);
      return false;
    }
    return true;
  }
  
  public final boolean isEmpty()
  {
    Object[] arrayOfObject = a;
    int i = arrayOfObject.length;
    return i == 0;
  }
  
  public final Iterator iterator()
  {
    return c.a(a);
  }
  
  public final boolean remove(Object paramObject)
  {
    paramObject = new java/lang/UnsupportedOperationException;
    ((UnsupportedOperationException)paramObject).<init>("Operation is not supported for read-only collection");
    throw ((Throwable)paramObject);
  }
  
  public final boolean removeAll(Collection paramCollection)
  {
    paramCollection = new java/lang/UnsupportedOperationException;
    paramCollection.<init>("Operation is not supported for read-only collection");
    throw paramCollection;
  }
  
  public final boolean retainAll(Collection paramCollection)
  {
    paramCollection = new java/lang/UnsupportedOperationException;
    paramCollection.<init>("Operation is not supported for read-only collection");
    throw paramCollection;
  }
  
  public final Object[] toArray()
  {
    Object[] arrayOfObject = a;
    boolean bool = b;
    Object localObject = "receiver$0";
    k.b(arrayOfObject, (String)localObject);
    if (bool)
    {
      Class localClass = arrayOfObject.getClass();
      localObject = Object[].class;
      bool = k.a(localClass, localObject);
      if (bool) {
        return arrayOfObject;
      }
    }
    int i = arrayOfObject.length;
    arrayOfObject = Arrays.copyOf(arrayOfObject, i, Object[].class);
    k.a(arrayOfObject, "java.util.Arrays.copyOf(… Array<Any?>::class.java)");
    return arrayOfObject;
  }
  
  public final Object[] toArray(Object[] paramArrayOfObject)
  {
    return g.a(this, paramArrayOfObject);
  }
}

/* Location:
 * Qualified Name:     c.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */