package c.a;

import c.g.b.k;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public abstract class d
  extends a
  implements c.g.b.a.a, List
{
  public static final d.a a;
  
  static
  {
    d.a locala = new c/a/d$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public void add(int paramInt, Object paramObject)
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
  
  public boolean addAll(int paramInt, Collection paramCollection)
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
  
  public boolean equals(Object paramObject)
  {
    Object localObject1 = this;
    localObject1 = (d)this;
    boolean bool1 = true;
    if (paramObject == localObject1) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof List;
    if (!bool2) {
      return false;
    }
    localObject1 = this;
    localObject1 = (Collection)this;
    paramObject = (Collection)paramObject;
    k.b(localObject1, "c");
    Object localObject2 = "other";
    k.b(paramObject, (String)localObject2);
    int i = ((Collection)localObject1).size();
    int j = ((Collection)paramObject).size();
    if (i != j) {
      return false;
    }
    paramObject = ((Collection)paramObject).iterator();
    localObject1 = ((Collection)localObject1).iterator();
    boolean bool3;
    do
    {
      bool3 = ((Iterator)localObject1).hasNext();
      if (!bool3) {
        break;
      }
      localObject2 = ((Iterator)localObject1).next();
      Object localObject3 = ((Iterator)paramObject).next();
      bool3 = k.a(localObject2, localObject3) ^ bool1;
    } while (!bool3);
    return false;
    return bool1;
  }
  
  public abstract Object get(int paramInt);
  
  public int hashCode()
  {
    Object localObject1 = this;
    localObject1 = (Collection)this;
    String str = "c";
    k.b(localObject1, str);
    localObject1 = ((Collection)localObject1).iterator();
    int i = 1;
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      Object localObject2 = ((Iterator)localObject1).next();
      i *= 31;
      int j;
      if (localObject2 != null)
      {
        j = localObject2.hashCode();
      }
      else
      {
        j = 0;
        localObject2 = null;
      }
      i += j;
    }
    return i;
  }
  
  public int indexOf(Object paramObject)
  {
    Iterator localIterator = iterator();
    int i = 0;
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      Object localObject = localIterator.next();
      bool = k.a(localObject, paramObject);
      if (bool) {
        return i;
      }
      i += 1;
    }
    return -1;
  }
  
  public Iterator iterator()
  {
    d.b localb = new c/a/d$b;
    localb.<init>(this);
    return (Iterator)localb;
  }
  
  public int lastIndexOf(Object paramObject)
  {
    int i = size();
    ListIterator localListIterator = listIterator(i);
    boolean bool;
    do
    {
      bool = localListIterator.hasPrevious();
      if (!bool) {
        break;
      }
      Object localObject = localListIterator.previous();
      bool = k.a(localObject, paramObject);
    } while (!bool);
    return localListIterator.nextIndex();
    return -1;
  }
  
  public ListIterator listIterator()
  {
    d.c localc = new c/a/d$c;
    localc.<init>(this, 0);
    return (ListIterator)localc;
  }
  
  public ListIterator listIterator(int paramInt)
  {
    d.c localc = new c/a/d$c;
    localc.<init>(this, paramInt);
    return (ListIterator)localc;
  }
  
  public Object remove(int paramInt)
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
  
  public Object set(int paramInt, Object paramObject)
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
  
  public List subList(int paramInt1, int paramInt2)
  {
    d.d locald = new c/a/d$d;
    locald.<init>(this, paramInt1, paramInt2);
    return (List)locald;
  }
}

/* Location:
 * Qualified Name:     c.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */