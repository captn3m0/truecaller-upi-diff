package c.a;

import c.k.h;
import java.util.List;

public final class al
  extends d
{
  private final List b;
  
  public al(List paramList)
  {
    b = paramList;
  }
  
  public final int a()
  {
    return b.size();
  }
  
  public final Object get(int paramInt)
  {
    Object localObject1 = b;
    int i = m.a(this);
    if ((paramInt >= 0) && (i >= paramInt))
    {
      i = m.a(this) - paramInt;
      return ((List)localObject1).get(i);
    }
    localObject1 = new java/lang/IndexOutOfBoundsException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Element index ");
    localStringBuilder.append(paramInt);
    localStringBuilder.append(" must be in range [");
    Object localObject2 = new c/k/h;
    int j = m.a(this);
    ((h)localObject2).<init>(0, j);
    localStringBuilder.append(localObject2);
    localStringBuilder.append("].");
    localObject2 = localStringBuilder.toString();
    ((IndexOutOfBoundsException)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
  }
}

/* Location:
 * Qualified Name:     c.a.al
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */