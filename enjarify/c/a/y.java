package c.a;

import c.g.b.a.a;
import c.g.b.g;
import c.g.b.k;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

public final class y
  implements a, Serializable, List, RandomAccess
{
  public static final y a;
  private static final long serialVersionUID = -7390468764508069838L;
  
  static
  {
    y localy = new c/a/y;
    localy.<init>();
    a = localy;
  }
  
  private final Object readResolve()
  {
    return a;
  }
  
  public final boolean addAll(int paramInt, Collection paramCollection)
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
  
  public final boolean addAll(Collection paramCollection)
  {
    paramCollection = new java/lang/UnsupportedOperationException;
    paramCollection.<init>("Operation is not supported for read-only collection");
    throw paramCollection;
  }
  
  public final void clear()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
  
  public final boolean contains(Object paramObject)
  {
    boolean bool = paramObject instanceof Void;
    if (!bool) {
      return false;
    }
    k.b((Void)paramObject, "element");
    return false;
  }
  
  public final boolean containsAll(Collection paramCollection)
  {
    k.b(paramCollection, "elements");
    return paramCollection.isEmpty();
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof List;
    if (bool1)
    {
      paramObject = (List)paramObject;
      boolean bool2 = ((List)paramObject).isEmpty();
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    return 1;
  }
  
  public final int indexOf(Object paramObject)
  {
    boolean bool = paramObject instanceof Void;
    int i = -1;
    if (!bool) {
      return i;
    }
    k.b((Void)paramObject, "element");
    return i;
  }
  
  public final boolean isEmpty()
  {
    return true;
  }
  
  public final Iterator iterator()
  {
    return (Iterator)x.a;
  }
  
  public final int lastIndexOf(Object paramObject)
  {
    boolean bool = paramObject instanceof Void;
    int i = -1;
    if (!bool) {
      return i;
    }
    k.b((Void)paramObject, "element");
    return i;
  }
  
  public final ListIterator listIterator()
  {
    return (ListIterator)x.a;
  }
  
  public final ListIterator listIterator(int paramInt)
  {
    if (paramInt == 0) {
      return (ListIterator)x.a;
    }
    IndexOutOfBoundsException localIndexOutOfBoundsException = new java/lang/IndexOutOfBoundsException;
    String str = String.valueOf(paramInt);
    str = "Index: ".concat(str);
    localIndexOutOfBoundsException.<init>(str);
    throw ((Throwable)localIndexOutOfBoundsException);
  }
  
  public final boolean remove(Object paramObject)
  {
    paramObject = new java/lang/UnsupportedOperationException;
    ((UnsupportedOperationException)paramObject).<init>("Operation is not supported for read-only collection");
    throw ((Throwable)paramObject);
  }
  
  public final boolean removeAll(Collection paramCollection)
  {
    paramCollection = new java/lang/UnsupportedOperationException;
    paramCollection.<init>("Operation is not supported for read-only collection");
    throw paramCollection;
  }
  
  public final boolean retainAll(Collection paramCollection)
  {
    paramCollection = new java/lang/UnsupportedOperationException;
    paramCollection.<init>("Operation is not supported for read-only collection");
    throw paramCollection;
  }
  
  public final List subList(int paramInt1, int paramInt2)
  {
    if ((paramInt1 == 0) && (paramInt2 == 0))
    {
      localObject = this;
      return (List)this;
    }
    IndexOutOfBoundsException localIndexOutOfBoundsException = new java/lang/IndexOutOfBoundsException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("fromIndex: ");
    localStringBuilder.append(paramInt1);
    localStringBuilder.append(", toIndex: ");
    localStringBuilder.append(paramInt2);
    Object localObject = localStringBuilder.toString();
    localIndexOutOfBoundsException.<init>((String)localObject);
    throw ((Throwable)localIndexOutOfBoundsException);
  }
  
  public final Object[] toArray()
  {
    return g.a(this);
  }
  
  public final Object[] toArray(Object[] paramArrayOfObject)
  {
    return g.a(this, paramArrayOfObject);
  }
  
  public final String toString()
  {
    return "[]";
  }
}

/* Location:
 * Qualified Name:     c.a.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */