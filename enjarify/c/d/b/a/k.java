package c.d.b.a;

import c.d.c;
import c.g.b.i;
import c.g.b.w;

public abstract class k
  extends d
  implements i
{
  private final int a;
  
  public k(int paramInt, c paramc)
  {
    super(paramc);
    a = paramInt;
  }
  
  public final int e()
  {
    return a;
  }
  
  public String toString()
  {
    Object localObject = t;
    if (localObject == null)
    {
      localObject = this;
      localObject = w.a((i)this);
      c.g.b.k.a(localObject, "Reflection.renderLambdaToString(this)");
      return (String)localObject;
    }
    return super.toString();
  }
}

/* Location:
 * Qualified Name:     c.d.b.a.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */