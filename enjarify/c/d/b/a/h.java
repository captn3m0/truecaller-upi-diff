package c.d.b.a;

import c.g.b.k;
import java.lang.reflect.Method;

final class h
{
  public static h.a a;
  public static final h b;
  private static final h.a c;
  
  static
  {
    Object localObject = new c/d/b/a/h;
    ((h)localObject).<init>();
    b = (h)localObject;
    localObject = new c/d/b/a/h$a;
    ((h.a)localObject).<init>(null, null, null);
    c = (h.a)localObject;
  }
  
  public static String a(a parama)
  {
    k.b(parama, "continuation");
    Object localObject1 = a;
    if (localObject1 == null) {
      localObject1 = b(parama);
    }
    Object localObject2 = c;
    if (localObject1 == localObject2) {
      return null;
    }
    localObject2 = a;
    if (localObject2 != null)
    {
      parama = parama.getClass();
      Object[] arrayOfObject = new Object[0];
      parama = ((Method)localObject2).invoke(parama, arrayOfObject);
      if (parama != null)
      {
        localObject2 = b;
        if (localObject2 != null)
        {
          arrayOfObject = new Object[0];
          parama = ((Method)localObject2).invoke(parama, arrayOfObject);
          if (parama != null)
          {
            localObject1 = c;
            if (localObject1 != null)
            {
              localObject2 = new Object[0];
              parama = ((Method)localObject1).invoke(parama, (Object[])localObject2);
            }
            else
            {
              parama = null;
            }
            boolean bool = parama instanceof String;
            if (!bool) {
              parama = null;
            }
            return (String)parama;
          }
        }
        return null;
      }
    }
    return null;
  }
  
  private static h.a b(a parama)
  {
    Object localObject1 = Class.class;
    Object localObject2 = "getModule";
    Object localObject3 = null;
    try
    {
      Object localObject4 = new Class[0];
      localObject1 = ((Class)localObject1).getDeclaredMethod((String)localObject2, (Class[])localObject4);
      localObject2 = parama.getClass();
      localObject2 = ((Class)localObject2).getClassLoader();
      localObject4 = "java.lang.Module";
      localObject2 = ((ClassLoader)localObject2).loadClass((String)localObject4);
      localObject4 = "getDescriptor";
      Class[] arrayOfClass = new Class[0];
      localObject2 = ((Class)localObject2).getDeclaredMethod((String)localObject4, arrayOfClass);
      parama = parama.getClass();
      parama = parama.getClassLoader();
      localObject4 = "java.lang.module.ModuleDescriptor";
      parama = parama.loadClass((String)localObject4);
      localObject4 = "name";
      localObject3 = new Class[0];
      parama = parama.getDeclaredMethod((String)localObject4, (Class[])localObject3);
      localObject3 = new c/d/b/a/h$a;
      ((h.a)localObject3).<init>((Method)localObject1, (Method)localObject2, parama);
      a = (h.a)localObject3;
      return (h.a)localObject3;
    }
    catch (Exception localException)
    {
      parama = c;
      a = parama;
    }
    return parama;
  }
}

/* Location:
 * Qualified Name:     c.d.b.a.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */