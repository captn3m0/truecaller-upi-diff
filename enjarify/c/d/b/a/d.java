package c.d.b.a;

import c.d.f;
import c.d.f.c;
import c.g.b.k;

public abstract class d
  extends a
{
  private transient c.d.c a;
  private final f b;
  
  public d(c.d.c paramc)
  {
    this(paramc, localf);
  }
  
  public d(c.d.c paramc, f paramf)
  {
    super(paramc);
    b = paramf;
  }
  
  public final f ao_()
  {
    f localf = b;
    if (localf == null) {
      k.a();
    }
    return localf;
  }
  
  protected final void b()
  {
    c.d.c localc = a;
    if (localc != null)
    {
      Object localObject = this;
      localObject = (d)this;
      if (localc != localObject)
      {
        localObject = ao_();
        f.c localc1 = (f.c)c.d.d.a;
        localObject = ((f)localObject).get(localc1);
        if (localObject == null) {
          k.a();
        }
        localObject = (c.d.d)localObject;
        ((c.d.d)localObject).b(localc);
      }
    }
    localc = (c.d.c)c.a;
    a = localc;
  }
  
  public final c.d.c r_()
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject1 = ao_();
      Object localObject2 = (f.c)c.d.d.a;
      localObject1 = (c.d.d)((f)localObject1).get((f.c)localObject2);
      if (localObject1 != null)
      {
        localObject2 = this;
        localObject2 = (c.d.c)this;
        localObject1 = ((c.d.d)localObject1).a((c.d.c)localObject2);
      }
      else
      {
        localObject1 = this;
        localObject1 = (c.d.c)this;
      }
      a = ((c.d.c)localObject1);
    }
    return (c.d.c)localObject1;
  }
}

/* Location:
 * Qualified Name:     c.d.b.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */