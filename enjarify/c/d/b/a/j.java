package c.d.b.a;

import c.d.c;
import c.g.b.k;
import c.g.b.w;

public abstract class j
  extends i
  implements c.g.b.i
{
  private final int a = 2;
  
  public j(c paramc)
  {
    super(paramc);
  }
  
  public final int e()
  {
    return a;
  }
  
  public String toString()
  {
    Object localObject = t;
    if (localObject == null)
    {
      localObject = this;
      localObject = w.a((c.g.b.i)this);
      k.a(localObject, "Reflection.renderLambdaToString(this)");
      return (String)localObject;
    }
    return super.toString();
  }
}

/* Location:
 * Qualified Name:     c.d.b.a.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */