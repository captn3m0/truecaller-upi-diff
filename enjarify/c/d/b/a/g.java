package c.d.b.a;

import c.g.b.k;
import java.lang.reflect.Field;

public final class g
{
  public static final StackTraceElement a(a parama)
  {
    k.b(parama, "receiver$0");
    Object localObject1 = b(parama);
    if (localObject1 == null) {
      return null;
    }
    a(((f)localObject1).a());
    int i = c(parama);
    if (i < 0)
    {
      i = -1;
    }
    else
    {
      localObject2 = ((f)localObject1).c();
      i = localObject2[i];
    }
    Object localObject2 = h.b;
    parama = h.a(parama);
    if (parama == null)
    {
      parama = ((f)localObject1).e();
    }
    else
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append(parama);
      char c = '/';
      ((StringBuilder)localObject2).append(c);
      parama = ((f)localObject1).e();
      ((StringBuilder)localObject2).append(parama);
      parama = ((StringBuilder)localObject2).toString();
    }
    localObject2 = new java/lang/StackTraceElement;
    String str = ((f)localObject1).d();
    localObject1 = ((f)localObject1).b();
    ((StackTraceElement)localObject2).<init>(parama, str, (String)localObject1, i);
    return (StackTraceElement)localObject2;
  }
  
  private static final void a(int paramInt)
  {
    int i = 1;
    if (paramInt <= i) {
      return;
    }
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("Debug metadata version mismatch. Expected: 1, got ");
    ((StringBuilder)localObject).append(paramInt);
    ((StringBuilder)localObject).append(". Please update the Kotlin standard library.");
    String str = ((StringBuilder)localObject).toString();
    localObject = new java/lang/IllegalStateException;
    str = str.toString();
    ((IllegalStateException)localObject).<init>(str);
    throw ((Throwable)localObject);
  }
  
  private static final f b(a parama)
  {
    return (f)parama.getClass().getAnnotation(f.class);
  }
  
  private static final int c(a parama)
  {
    int k;
    try
    {
      Object localObject = parama.getClass();
      String str = "label";
      localObject = ((Class)localObject).getDeclaredField(str);
      str = "field";
      k.a(localObject, str);
      int i = 1;
      ((Field)localObject).setAccessible(i);
      parama = ((Field)localObject).get(parama);
      boolean bool = parama instanceof Integer;
      int j;
      if (!bool)
      {
        j = 0;
        parama = null;
      }
      parama = (Integer)parama;
      if (parama != null)
      {
        j = parama.intValue();
      }
      else
      {
        j = 0;
        parama = null;
      }
      j -= i;
    }
    catch (Exception localException)
    {
      k = -1;
    }
    return k;
  }
}

/* Location:
 * Qualified Name:     c.d.b.a.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */