package c.d.b.a;

import c.d.f;

public final class c
  implements c.d.c
{
  public static final c a;
  
  static
  {
    c localc = new c/d/b/a/c;
    localc.<init>();
    a = localc;
  }
  
  public final f ao_()
  {
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    String str = "This continuation is already complete".toString();
    localIllegalStateException.<init>(str);
    throw ((Throwable)localIllegalStateException);
  }
  
  public final void b(Object paramObject)
  {
    paramObject = new java/lang/IllegalStateException;
    String str = "This continuation is already complete".toString();
    ((IllegalStateException)paramObject).<init>(str);
    throw ((Throwable)paramObject);
  }
  
  public final String toString()
  {
    return "This continuation is already complete";
  }
}

/* Location:
 * Qualified Name:     c.d.b.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */