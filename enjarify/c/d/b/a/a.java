package c.d.b.a;

import c.d.c;
import c.g.b.k;
import c.o;
import c.p;
import java.io.Serializable;

public abstract class a
  implements e, c, Serializable
{
  final c t;
  
  public a(c paramc)
  {
    t = paramc;
  }
  
  public c a(Object paramObject, c paramc)
  {
    k.b(paramc, "completion");
    paramObject = new java/lang/UnsupportedOperationException;
    ((UnsupportedOperationException)paramObject).<init>("create(Any?;Continuation) has not been overridden");
    throw ((Throwable)paramObject);
  }
  
  protected abstract Object a(Object paramObject);
  
  public final StackTraceElement ar_()
  {
    return g.a(this);
  }
  
  protected void b() {}
  
  public final void b(Object paramObject)
  {
    Object localObject1 = this;
    localObject1 = (c)this;
    Object localObject2 = "frame";
    k.b(localObject1, (String)localObject2);
    localObject1 = this;
    for (localObject1 = (a)this;; localObject1 = (a)localObject2)
    {
      localObject2 = t;
      if (localObject2 == null) {
        k.a();
      }
      try
      {
        paramObject = ((a)localObject1).a(paramObject);
        localObject3 = c.d.a.a.a;
        if (paramObject == localObject3) {
          return;
        }
        localObject3 = o.a;
        paramObject = o.d(paramObject);
      }
      finally
      {
        Object localObject3 = o.a;
        paramObject = o.d(p.a((Throwable)paramObject));
      }
      ((a)localObject1).b();
      boolean bool = localObject2 instanceof a;
      if (!bool) {
        break;
      }
      localObject1 = localObject2;
    }
    ((c)localObject2).b(paramObject);
  }
  
  public final e c()
  {
    c localc = t;
    boolean bool = localc instanceof e;
    if (!bool) {
      localc = null;
    }
    return (e)localc;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Continuation at ");
    Object localObject = g.a(this);
    if (localObject == null) {
      localObject = getClass().getName();
    }
    localObject = (Serializable)localObject;
    localStringBuilder.append(localObject);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     c.d.b.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */