package c.d;

import c.g.a.m;
import c.g.b.k;
import java.io.Serializable;

public final class g
  implements f, Serializable
{
  public static final g a;
  private static final long serialVersionUID;
  
  static
  {
    g localg = new c/d/g;
    localg.<init>();
    a = localg;
  }
  
  private final Object readResolve()
  {
    return a;
  }
  
  public final Object fold(Object paramObject, m paramm)
  {
    k.b(paramm, "operation");
    return paramObject;
  }
  
  public final f.b get(f.c paramc)
  {
    k.b(paramc, "key");
    return null;
  }
  
  public final int hashCode()
  {
    return 0;
  }
  
  public final f minusKey(f.c paramc)
  {
    k.b(paramc, "key");
    paramc = this;
    return (f)this;
  }
  
  public final f plus(f paramf)
  {
    k.b(paramf, "context");
    return paramf;
  }
  
  public final String toString()
  {
    return "EmptyCoroutineContext";
  }
}

/* Location:
 * Qualified Name:     c.d.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */