package c.d;

import c.g.a.m;
import c.g.b.k;
import c.g.b.v.b;
import c.u;
import c.x;
import java.io.Serializable;

public final class b
  implements f, Serializable
{
  private final f a;
  private final f.b b;
  
  public b(f paramf, f.b paramb)
  {
    a = paramf;
    b = paramb;
  }
  
  private final int a()
  {
    Object localObject = this;
    localObject = (b)this;
    int i = 2;
    for (;;)
    {
      localObject = a;
      boolean bool = localObject instanceof b;
      if (!bool) {
        localObject = null;
      }
      localObject = (b)localObject;
      if (localObject == null) {
        return i;
      }
      i += 1;
    }
  }
  
  private final boolean a(f.b paramb)
  {
    f.c localc = paramb.getKey();
    return k.a(get(localc), paramb);
  }
  
  private final Object writeReplace()
  {
    int i = a();
    Object localObject1 = new f[i];
    v.b localb = new c/g/b/v$b;
    localb.<init>();
    int j = 0;
    a = 0;
    x localx = x.a;
    Object localObject2 = new c/d/b$c;
    ((b.c)localObject2).<init>((f[])localObject1, localb);
    localObject2 = (m)localObject2;
    fold(localx, (m)localObject2);
    int k = a;
    if (k == i) {
      j = 1;
    }
    if (j != 0)
    {
      localObject3 = new c/d/b$a;
      ((b.a)localObject3).<init>((f[])localObject1);
      return localObject3;
    }
    Object localObject3 = new java/lang/IllegalStateException;
    localObject1 = "Check failed.".toString();
    ((IllegalStateException)localObject3).<init>((String)localObject1);
    throw ((Throwable)localObject3);
  }
  
  public final boolean equals(Object paramObject)
  {
    Object localObject = this;
    localObject = (b)this;
    if (localObject != paramObject)
    {
      boolean bool1 = paramObject instanceof b;
      if (bool1)
      {
        paramObject = (b)paramObject;
        int i = ((b)paramObject).a();
        int j = a();
        if (i == j)
        {
          boolean bool3;
          for (localObject = this;; localObject = (b)localObject)
          {
            f.b localb = b;
            boolean bool2 = ((b)paramObject).a(localb);
            if (!bool2)
            {
              bool3 = false;
              paramObject = null;
              break label112;
            }
            localObject = a;
            bool2 = localObject instanceof b;
            if (!bool2) {
              break;
            }
          }
          if (localObject != null)
          {
            localObject = (f.b)localObject;
            bool3 = ((b)paramObject).a((f.b)localObject);
            label112:
            if (bool3) {
              break label134;
            }
          }
          else
          {
            paramObject = new c/u;
            ((u)paramObject).<init>("null cannot be cast to non-null type kotlin.coroutines.CoroutineContext.Element");
            throw ((Throwable)paramObject);
          }
        }
      }
      return false;
    }
    label134:
    return true;
  }
  
  public final Object fold(Object paramObject, m paramm)
  {
    k.b(paramm, "operation");
    paramObject = a.fold(paramObject, paramm);
    f.b localb = b;
    return paramm.invoke(paramObject, localb);
  }
  
  public final f.b get(f.c paramc)
  {
    k.b(paramc, "key");
    Object localObject = this;
    for (localObject = (b)this;; localObject = (b)localObject)
    {
      f.b localb = b.get(paramc);
      if (localb != null) {
        return localb;
      }
      localObject = a;
      boolean bool = localObject instanceof b;
      if (!bool) {
        break;
      }
    }
    return ((f)localObject).get(paramc);
  }
  
  public final int hashCode()
  {
    int i = a.hashCode();
    int j = b.hashCode();
    return i + j;
  }
  
  public final f minusKey(f.c paramc)
  {
    k.b(paramc, "key");
    Object localObject = b.get(paramc);
    if (localObject != null) {
      return a;
    }
    paramc = a.minusKey(paramc);
    localObject = a;
    if (paramc == localObject)
    {
      paramc = this;
      return (f)this;
    }
    localObject = g.a;
    if (paramc == localObject) {
      return (f)b;
    }
    localObject = new c/d/b;
    f.b localb = b;
    ((b)localObject).<init>(paramc, localb);
    return (f)localObject;
  }
  
  public final f plus(f paramf)
  {
    k.b(paramf, "context");
    return f.a.a(this, paramf);
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("[");
    m localm = (m)b.b.a;
    String str = (String)fold("", localm);
    localStringBuilder.append(str);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     c.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */