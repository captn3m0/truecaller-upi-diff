package c.d;

import c.g.a.m;
import c.g.b.k;

public abstract class a
  implements f.b
{
  private final f.c key;
  
  public a(f.c paramc)
  {
    key = paramc;
  }
  
  public Object fold(Object paramObject, m paramm)
  {
    k.b(paramm, "operation");
    return f.b.a.a(this, paramObject, paramm);
  }
  
  public f.b get(f.c paramc)
  {
    k.b(paramc, "key");
    return f.b.a.a(this, paramc);
  }
  
  public f.c getKey()
  {
    return key;
  }
  
  public f minusKey(f.c paramc)
  {
    k.b(paramc, "key");
    return f.b.a.b(this, paramc);
  }
  
  public f plus(f paramf)
  {
    k.b(paramf, "context");
    return f.b.a.a(this, paramf);
  }
}

/* Location:
 * Qualified Name:     c.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */