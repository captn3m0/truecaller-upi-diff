package c.d;

import c.d.a.a;
import c.d.b.a.e;
import c.o.b;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

public final class h
  implements e, c
{
  public static final h.a a;
  private static final AtomicReferenceFieldUpdater d = AtomicReferenceFieldUpdater.newUpdater(h.class, Object.class, "b");
  private volatile Object b;
  private final c c;
  
  static
  {
    h.a locala = new c/d/h$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public h(c paramc)
  {
    this(paramc, locala);
  }
  
  private h(c paramc, Object paramObject)
  {
    c = paramc;
    b = paramObject;
  }
  
  public final f ao_()
  {
    return c.ao_();
  }
  
  public final StackTraceElement ar_()
  {
    return null;
  }
  
  public final Object b()
  {
    Object localObject = b;
    a locala1 = a.b;
    if (localObject == locala1)
    {
      localObject = d;
      locala1 = a.b;
      a locala2 = a.a;
      boolean bool1 = ((AtomicReferenceFieldUpdater)localObject).compareAndSet(this, locala1, locala2);
      if (bool1) {
        return a.a;
      }
      localObject = b;
    }
    locala1 = a.c;
    if (localObject == locala1) {
      return a.a;
    }
    boolean bool2 = localObject instanceof o.b;
    if (!bool2) {
      return localObject;
    }
    throw a;
  }
  
  public final void b(Object paramObject)
  {
    boolean bool;
    do
    {
      do
      {
        localObject = b;
        locala1 = a.b;
        if (localObject != locala1) {
          break;
        }
        localObject = d;
        locala1 = a.b;
        bool = ((AtomicReferenceFieldUpdater)localObject).compareAndSet(this, locala1, paramObject);
      } while (!bool);
      return;
      a locala1 = a.a;
      if (localObject != locala1) {
        break;
      }
      Object localObject = d;
      locala1 = a.a;
      a locala2 = a.c;
      bool = ((AtomicReferenceFieldUpdater)localObject).compareAndSet(this, locala1, locala2);
    } while (!bool);
    c.b(paramObject);
    return;
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("Already resumed");
    throw ((Throwable)paramObject);
  }
  
  public final e c()
  {
    c localc = c;
    boolean bool = localc instanceof e;
    if (!bool) {
      localc = null;
    }
    return (e)localc;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SafeContinuation for ");
    c localc = c;
    localStringBuilder.append(localc);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     c.d.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */