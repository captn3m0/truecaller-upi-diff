package c.d;

import c.g.a.m;
import c.g.b.k;

public final class f$b$a
{
  public static f.b a(f.b paramb, f.c paramc)
  {
    k.b(paramc, "key");
    f.c localc = paramb.getKey();
    boolean bool = k.a(localc, paramc);
    if (bool) {
      return paramb;
    }
    return null;
  }
  
  public static f a(f.b paramb, f paramf)
  {
    k.b(paramf, "context");
    return f.a.a((f)paramb, paramf);
  }
  
  public static Object a(f.b paramb, Object paramObject, m paramm)
  {
    k.b(paramm, "operation");
    return paramm.invoke(paramObject, paramb);
  }
  
  public static f b(f.b paramb, f.c paramc)
  {
    k.b(paramc, "key");
    f.c localc = paramb.getKey();
    boolean bool = k.a(localc, paramc);
    if (bool) {
      return (f)g.a;
    }
    return (f)paramb;
  }
}

/* Location:
 * Qualified Name:     c.d.f.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */