package c.d;

import java.io.Serializable;

final class b$a
  implements Serializable
{
  public static final b.a.a a;
  private static final long serialVersionUID;
  private final f[] b;
  
  static
  {
    b.a.a locala = new c/d/b$a$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public b$a(f[] paramArrayOff)
  {
    b = paramArrayOff;
  }
  
  private final Object readResolve()
  {
    f[] arrayOff = b;
    Object localObject = g.a;
    int i = arrayOff.length;
    int j = 0;
    while (j < i)
    {
      f localf = arrayOff[j];
      localObject = ((f)localObject).plus(localf);
      j += 1;
    }
    return localObject;
  }
}

/* Location:
 * Qualified Name:     c.d.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */