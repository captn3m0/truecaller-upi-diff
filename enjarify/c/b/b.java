package c.b;

public class b
{
  public static final int a(Comparable paramComparable1, Comparable paramComparable2)
  {
    if (paramComparable1 == paramComparable2) {
      return 0;
    }
    if (paramComparable1 == null) {
      return -1;
    }
    if (paramComparable2 == null) {
      return 1;
    }
    return paramComparable1.compareTo(paramComparable2);
  }
}

/* Location:
 * Qualified Name:     c.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */