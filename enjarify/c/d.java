package c;

import java.io.Serializable;

public final class d
  implements f, Serializable
{
  private final Object a;
  
  public d(Object paramObject)
  {
    a = paramObject;
  }
  
  public final boolean a()
  {
    return true;
  }
  
  public final Object b()
  {
    return a;
  }
  
  public final String toString()
  {
    return String.valueOf(b());
  }
}

/* Location:
 * Qualified Name:     c.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */