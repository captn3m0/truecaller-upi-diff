package c;

import c.g.b.k;
import java.io.Serializable;

public final class o$b
  implements Serializable
{
  public final Throwable a;
  
  public o$b(Throwable paramThrowable)
  {
    a = paramThrowable;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof b;
    if (bool1)
    {
      Throwable localThrowable = a;
      paramObject = a;
      boolean bool2 = k.a(localThrowable, paramObject);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    return a.hashCode();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Failure(");
    Throwable localThrowable = a;
    localStringBuilder.append(localThrowable);
    localStringBuilder.append(')');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     c.o.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */