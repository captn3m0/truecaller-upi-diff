package c.g.b;

import c.l.a;
import c.l.g;

public abstract class q
  extends d
  implements g
{
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (paramObject == this) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof q;
    if (bool2)
    {
      paramObject = (q)paramObject;
      Object localObject1 = a();
      Object localObject2 = ((q)paramObject).a();
      bool2 = localObject1.equals(localObject2);
      if (bool2)
      {
        localObject1 = b();
        localObject2 = ((q)paramObject).b();
        bool2 = ((String)localObject1).equals(localObject2);
        if (bool2)
        {
          localObject1 = c();
          localObject2 = ((q)paramObject).c();
          bool2 = ((String)localObject1).equals(localObject2);
          if (bool2)
          {
            localObject1 = f();
            paramObject = ((q)paramObject).f();
            boolean bool3 = k.a(localObject1, paramObject);
            if (bool3) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    bool1 = paramObject instanceof g;
    if (bool1)
    {
      a locala = g();
      return paramObject.equals(locala);
    }
    return false;
  }
  
  public int hashCode()
  {
    int i = a().hashCode() * 31;
    int j = b().hashCode();
    i = (i + j) * 31;
    j = c().hashCode();
    return i + j;
  }
  
  protected final g i()
  {
    return (g)super.h();
  }
  
  public String toString()
  {
    Object localObject = g();
    if (localObject != this) {
      return localObject.toString();
    }
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("property ");
    String str = b();
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append(" (Kotlin reflection is not available)");
    return ((StringBuilder)localObject).toString();
  }
}

/* Location:
 * Qualified Name:     c.g.b.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */