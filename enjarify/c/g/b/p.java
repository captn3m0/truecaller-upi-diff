package c.g.b;

public final class p
  implements e
{
  private final Class a;
  private final String b;
  
  public p(Class paramClass, String paramString)
  {
    a = paramClass;
    b = paramString;
  }
  
  public final Class a()
  {
    return a;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof p;
    if (bool1)
    {
      Class localClass = a;
      paramObject = a;
      boolean bool2 = k.a(localClass, paramObject);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    return a.hashCode();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str = a.toString();
    localStringBuilder.append(str);
    localStringBuilder.append(" (Kotlin reflection is not available)");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     c.g.b.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */