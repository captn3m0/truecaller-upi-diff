package c.g.b;

import c.g.b;
import c.l.a;
import c.l.c;
import java.io.Serializable;

public abstract class d
  implements a, Serializable
{
  public static final Object c = ;
  private transient a a;
  protected final Object b;
  
  public d()
  {
    this(localObject);
  }
  
  protected d(Object paramObject)
  {
    b = paramObject;
  }
  
  public c a()
  {
    AbstractMethodError localAbstractMethodError = new java/lang/AbstractMethodError;
    localAbstractMethodError.<init>();
    throw localAbstractMethodError;
  }
  
  public final Object a(Object... paramVarArgs)
  {
    return h().a(paramVarArgs);
  }
  
  public String b()
  {
    AbstractMethodError localAbstractMethodError = new java/lang/AbstractMethodError;
    localAbstractMethodError.<init>();
    throw localAbstractMethodError;
  }
  
  public String c()
  {
    AbstractMethodError localAbstractMethodError = new java/lang/AbstractMethodError;
    localAbstractMethodError.<init>();
    throw localAbstractMethodError;
  }
  
  protected abstract a d();
  
  public final Object f()
  {
    return b;
  }
  
  public final a g()
  {
    a locala = a;
    if (locala == null)
    {
      locala = d();
      a = locala;
    }
    return locala;
  }
  
  protected a h()
  {
    Object localObject = g();
    if (localObject != this) {
      return (a)localObject;
    }
    localObject = new c/g/b;
    ((b)localObject).<init>();
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     c.g.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */