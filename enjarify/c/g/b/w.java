package c.g.b;

import c.l.b;
import c.l.c;
import c.l.d;
import c.l.h;

public final class w
{
  private static final x a;
  private static final b[] b = new b[0];
  
  static
  {
    Object localObject1 = null;
    Object localObject2 = "kotlin.reflect.jvm.internal.ReflectionFactoryImpl";
    try
    {
      localObject2 = Class.forName((String)localObject2);
      localObject2 = ((Class)localObject2).newInstance();
      localObject2 = (x)localObject2;
      localObject1 = localObject2;
    }
    catch (IllegalAccessException localIllegalAccessException) {}catch (InstantiationException localInstantiationException) {}catch (ClassNotFoundException localClassNotFoundException) {}catch (ClassCastException localClassCastException) {}
    if (localObject1 == null)
    {
      localObject1 = new c/g/b/x;
      ((x)localObject1).<init>();
    }
    a = (x)localObject1;
  }
  
  public static b a(Class paramClass)
  {
    f localf = new c/g/b/f;
    localf.<init>(paramClass);
    return localf;
  }
  
  public static c a(Class paramClass, String paramString)
  {
    p localp = new c/g/b/p;
    localp.<init>(paramClass, paramString);
    return localp;
  }
  
  public static d a(j paramj)
  {
    return paramj;
  }
  
  public static c.l.f a(n paramn)
  {
    return paramn;
  }
  
  public static h a(r paramr)
  {
    return paramr;
  }
  
  public static c.l.i a(t paramt)
  {
    return paramt;
  }
  
  public static String a(i parami)
  {
    return x.a(parami);
  }
  
  public static String a(l paraml)
  {
    return x.a(paraml);
  }
}

/* Location:
 * Qualified Name:     c.g.b.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */