package c.g.b;

import c.g.a;
import c.l.b;

public final class f
  implements e, b
{
  private final Class a;
  
  public f(Class paramClass)
  {
    a = paramClass;
  }
  
  public final Class a()
  {
    return a;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof f;
    if (bool1)
    {
      Class localClass = a.b(this);
      paramObject = a.b((b)paramObject);
      boolean bool2 = k.a(localClass, paramObject);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    return a.b(this).hashCode();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str = a.toString();
    localStringBuilder.append(str);
    localStringBuilder.append(" (Kotlin reflection is not available)");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     c.g.b.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */