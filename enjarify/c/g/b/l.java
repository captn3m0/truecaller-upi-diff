package c.g.b;

import java.io.Serializable;

public abstract class l
  implements i, Serializable
{
  private final int a;
  
  public l(int paramInt)
  {
    a = paramInt;
  }
  
  public final int e()
  {
    return a;
  }
  
  public String toString()
  {
    String str = w.a(this);
    k.a(str, "Reflection.renderLambdaToString(this)");
    return str;
  }
}

/* Location:
 * Qualified Name:     c.g.b.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */