package c.g.b;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public final class y
{
  public final ArrayList a;
  
  public y(int paramInt)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>(paramInt);
    a = localArrayList;
  }
  
  public final void a(Object paramObject)
  {
    if (paramObject == null) {
      return;
    }
    boolean bool = paramObject instanceof Object[];
    if (bool)
    {
      paramObject = (Object[])paramObject;
      int i = paramObject.length;
      if (i > 0)
      {
        localObject = a;
        int j = ((ArrayList)localObject).size();
        int k = paramObject.length;
        j += k;
        ((ArrayList)localObject).ensureCapacity(j);
        localObject = a;
        Collections.addAll((Collection)localObject, (Object[])paramObject);
      }
      return;
    }
    Object localObject = new java/lang/UnsupportedOperationException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Don't know how to spread ");
    paramObject = paramObject.getClass();
    localStringBuilder.append(paramObject);
    paramObject = localStringBuilder.toString();
    ((UnsupportedOperationException)localObject).<init>((String)paramObject);
    throw ((Throwable)localObject);
  }
  
  public final Object[] a(Object[] paramArrayOfObject)
  {
    return a.toArray(paramArrayOfObject);
  }
  
  public final void b(Object paramObject)
  {
    a.add(paramObject);
  }
}

/* Location:
 * Qualified Name:     c.g.b.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */