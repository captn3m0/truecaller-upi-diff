package c.g.b;

import c.u;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

public final class g
{
  private static final Object[] a = new Object[0];
  
  public static final Object[] a(Collection paramCollection)
  {
    Object localObject1 = "collection";
    k.b(paramCollection, (String)localObject1);
    int i = paramCollection.size();
    if (i == 0) {
      return a;
    }
    paramCollection = paramCollection.iterator();
    boolean bool1 = paramCollection.hasNext();
    if (!bool1) {
      return a;
    }
    localObject1 = new Object[i];
    bool1 = false;
    String str = null;
    for (;;)
    {
      int m = bool1 + true;
      Object localObject2 = paramCollection.next();
      localObject1[bool1] = localObject2;
      int j = localObject1.length;
      if (m >= j)
      {
        boolean bool2 = paramCollection.hasNext();
        if (!bool2) {
          return (Object[])localObject1;
        }
        int k = m * 3 + 1 >>> 1;
        int n = 2147483645;
        if (k <= m) {
          if (m < n)
          {
            k = 2147483645;
          }
          else
          {
            paramCollection = new java/lang/OutOfMemoryError;
            paramCollection.<init>();
            throw ((Throwable)paramCollection);
          }
        }
        localObject1 = Arrays.copyOf((Object[])localObject1, k);
        str = "Arrays.copyOf(result, newSize)";
        k.a(localObject1, str);
        k = m;
      }
      else
      {
        boolean bool3 = paramCollection.hasNext();
        if (!bool3)
        {
          paramCollection = Arrays.copyOf((Object[])localObject1, m);
          k.a(paramCollection, "Arrays.copyOf(result, size)");
          return paramCollection;
        }
        bool3 = m;
      }
    }
  }
  
  public static final Object[] a(Collection paramCollection, Object[] paramArrayOfObject)
  {
    Object localObject1 = "collection";
    k.b(paramCollection, (String)localObject1);
    if (paramArrayOfObject != null)
    {
      int i = paramCollection.size();
      int j = 0;
      String str = null;
      int m;
      if (i == 0)
      {
        m = paramArrayOfObject.length;
        if (m > 0) {
          paramArrayOfObject[0] = null;
        }
        return paramArrayOfObject;
      }
      paramCollection = paramCollection.iterator();
      boolean bool3 = paramCollection.hasNext();
      if (!bool3)
      {
        m = paramArrayOfObject.length;
        if (m > 0) {
          paramArrayOfObject[0] = null;
        }
        return paramArrayOfObject;
      }
      int n = paramArrayOfObject.length;
      if (i <= n)
      {
        localObject1 = paramArrayOfObject;
      }
      else
      {
        Class localClass = paramArrayOfObject.getClass().getComponentType();
        localObject1 = Array.newInstance(localClass, i);
        if (localObject1 == null) {
          break label284;
        }
        localObject1 = (Object[])localObject1;
      }
      for (;;)
      {
        n = j + 1;
        Object localObject2 = paramCollection.next();
        localObject1[j] = localObject2;
        j = localObject1.length;
        if (n >= j)
        {
          boolean bool1 = paramCollection.hasNext();
          if (!bool1) {
            return (Object[])localObject1;
          }
          int k = n * 3 + 1 >>> 1;
          int i1 = 2147483645;
          if (k <= n) {
            if (n < i1)
            {
              k = 2147483645;
            }
            else
            {
              paramCollection = new java/lang/OutOfMemoryError;
              paramCollection.<init>();
              throw ((Throwable)paramCollection);
            }
          }
          localObject1 = Arrays.copyOf((Object[])localObject1, k);
          str = "Arrays.copyOf(result, newSize)";
          k.a(localObject1, str);
          k = n;
        }
        else
        {
          boolean bool2 = paramCollection.hasNext();
          if (!bool2)
          {
            if (localObject1 == paramArrayOfObject)
            {
              paramArrayOfObject[n] = null;
              return paramArrayOfObject;
            }
            paramCollection = Arrays.copyOf((Object[])localObject1, n);
            k.a(paramCollection, "Arrays.copyOf(result, size)");
            return paramCollection;
          }
          bool2 = n;
        }
      }
      label284:
      paramCollection = new c/u;
      paramCollection.<init>("null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
      throw paramCollection;
    }
    paramCollection = new java/lang/NullPointerException;
    paramCollection.<init>();
    throw ((Throwable)paramCollection);
  }
}

/* Location:
 * Qualified Name:     c.g.b.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */