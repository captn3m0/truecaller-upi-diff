package c.g.b;

import c.g.b.a.a;
import java.util.Iterator;
import java.util.NoSuchElementException;

final class b
  implements a, Iterator
{
  private int a;
  private final Object[] b;
  
  public b(Object[] paramArrayOfObject)
  {
    b = paramArrayOfObject;
  }
  
  public final boolean hasNext()
  {
    int i = a;
    Object[] arrayOfObject = b;
    int j = arrayOfObject.length;
    return i < j;
  }
  
  public final Object next()
  {
    try
    {
      Object[] arrayOfObject = b;
      i = a;
      int j = i + 1;
      a = j;
      return arrayOfObject[i];
    }
    catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
    {
      int i = a + -1;
      a = i;
      NoSuchElementException localNoSuchElementException = new java/util/NoSuchElementException;
      String str = localArrayIndexOutOfBoundsException.getMessage();
      localNoSuchElementException.<init>(str);
      throw ((Throwable)localNoSuchElementException);
    }
  }
  
  public final void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
}

/* Location:
 * Qualified Name:     c.g.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */