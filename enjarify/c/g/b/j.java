package c.g.b;

import c.l.a;
import c.l.c;

public class j
  extends d
  implements i, c.l.d
{
  private final int a;
  
  public j()
  {
    a = 1;
  }
  
  public j(int paramInt, Object paramObject)
  {
    super(paramObject);
    a = paramInt;
  }
  
  protected final a d()
  {
    return w.a(this);
  }
  
  public final int e()
  {
    return a;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (paramObject == this) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof j;
    if (bool2)
    {
      paramObject = (j)paramObject;
      Object localObject1 = a();
      if (localObject1 == null)
      {
        localObject1 = ((j)paramObject).a();
        if (localObject1 != null) {
          break label146;
        }
      }
      else
      {
        localObject1 = a();
        localObject2 = ((j)paramObject).a();
        bool2 = localObject1.equals(localObject2);
        if (!bool2) {
          break label146;
        }
      }
      localObject1 = b();
      Object localObject2 = ((j)paramObject).b();
      bool2 = ((String)localObject1).equals(localObject2);
      if (bool2)
      {
        localObject1 = c();
        localObject2 = ((j)paramObject).c();
        bool2 = ((String)localObject1).equals(localObject2);
        if (bool2)
        {
          localObject1 = f();
          paramObject = ((j)paramObject).f();
          boolean bool3 = k.a(localObject1, paramObject);
          if (bool3) {
            return bool1;
          }
        }
      }
      label146:
      return false;
    }
    bool1 = paramObject instanceof c.l.d;
    if (bool1)
    {
      a locala = g();
      return paramObject.equals(locala);
    }
    return false;
  }
  
  public int hashCode()
  {
    c localc = a();
    if (localc == null)
    {
      i = 0;
      localc = null;
    }
    else
    {
      localc = a();
      i = localc.hashCode() * 31;
    }
    int j = b().hashCode();
    int i = (i + j) * 31;
    j = c().hashCode();
    return i + j;
  }
  
  public String toString()
  {
    Object localObject = g();
    if (localObject != this) {
      return localObject.toString();
    }
    localObject = "<init>";
    String str = b();
    boolean bool = ((String)localObject).equals(str);
    if (bool) {
      return "constructor (Kotlin reflection is not available)";
    }
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("function ");
    str = b();
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append(" (Kotlin reflection is not available)");
    return ((StringBuilder)localObject).toString();
  }
}

/* Location:
 * Qualified Name:     c.g.b.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */