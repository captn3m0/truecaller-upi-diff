package c.g.b;

import c.e;
import c.w;
import java.util.Arrays;
import java.util.List;

public class k
{
  public static int a(int paramInt1, int paramInt2)
  {
    if (paramInt1 < paramInt2) {
      return -1;
    }
    if (paramInt1 == paramInt2) {
      return 0;
    }
    return 1;
  }
  
  private static Throwable a(Throwable paramThrowable)
  {
    String str = k.class.getName();
    return a(paramThrowable, str);
  }
  
  static Throwable a(Throwable paramThrowable, String paramString)
  {
    StackTraceElement[] arrayOfStackTraceElement = paramThrowable.getStackTrace();
    int i = arrayOfStackTraceElement.length;
    int j = -1;
    int k = 0;
    while (k < i)
    {
      String str = arrayOfStackTraceElement[k].getClassName();
      boolean bool = paramString.equals(str);
      if (bool) {
        j = k;
      }
      k += 1;
    }
    paramString = Arrays.asList(arrayOfStackTraceElement);
    j += 1;
    paramString = paramString.subList(j, i);
    arrayOfStackTraceElement = new StackTraceElement[paramString.size()];
    paramString = (StackTraceElement[])paramString.toArray(arrayOfStackTraceElement);
    paramThrowable.setStackTrace(paramString);
    return paramThrowable;
  }
  
  public static void a()
  {
    e locale = new c/e;
    locale.<init>();
    throw ((e)a(locale));
  }
  
  public static void a(Object paramObject, String paramString)
  {
    if (paramObject != null) {
      return;
    }
    paramObject = new java/lang/IllegalStateException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramString);
    localStringBuilder.append(" must not be null");
    paramString = localStringBuilder.toString();
    ((IllegalStateException)paramObject).<init>(paramString);
    throw ((IllegalStateException)a((Throwable)paramObject));
  }
  
  public static void a(String paramString)
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("lateinit property ");
    ((StringBuilder)localObject).append(paramString);
    ((StringBuilder)localObject).append(" has not been initialized");
    paramString = ((StringBuilder)localObject).toString();
    localObject = new c/w;
    ((w)localObject).<init>(paramString);
    throw ((w)a((Throwable)localObject));
  }
  
  public static boolean a(Double paramDouble1, Double paramDouble2)
  {
    boolean bool1 = true;
    if (paramDouble1 == null)
    {
      if (paramDouble2 == null) {
        return bool1;
      }
      return false;
    }
    if (paramDouble2 != null)
    {
      double d1 = paramDouble1.doubleValue();
      double d2 = paramDouble2.doubleValue();
      boolean bool2 = d1 < d2;
      if (!bool2) {
        return bool1;
      }
    }
    return false;
  }
  
  public static boolean a(Object paramObject1, Object paramObject2)
  {
    if (paramObject1 == null) {
      return paramObject2 == null;
    }
    return paramObject1.equals(paramObject2);
  }
  
  public static void b(Object paramObject, String paramString)
  {
    if (paramObject != null) {
      return;
    }
    paramObject = Thread.currentThread().getStackTrace()[3];
    String str = ((StackTraceElement)paramObject).getClassName();
    paramObject = ((StackTraceElement)paramObject).getMethodName();
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Parameter specified as non-null is null: method ");
    localStringBuilder.append(str);
    localStringBuilder.append(".");
    localStringBuilder.append((String)paramObject);
    localStringBuilder.append(", parameter ");
    localStringBuilder.append(paramString);
    paramObject = localStringBuilder.toString();
    localIllegalArgumentException.<init>((String)paramObject);
    throw ((IllegalArgumentException)a(localIllegalArgumentException));
  }
}

/* Location:
 * Qualified Name:     c.g.b.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */