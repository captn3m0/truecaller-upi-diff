package c.g.b;

import c.a.k;
import java.util.NoSuchElementException;

public final class a
  extends k
{
  private int a;
  private final byte[] b;
  
  public a(byte[] paramArrayOfByte)
  {
    b = paramArrayOfByte;
  }
  
  public final byte a()
  {
    try
    {
      byte[] arrayOfByte = b;
      i = a;
      int j = i + 1;
      a = j;
      return arrayOfByte[i];
    }
    catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
    {
      int i = a + -1;
      a = i;
      NoSuchElementException localNoSuchElementException = new java/util/NoSuchElementException;
      String str = localArrayIndexOutOfBoundsException.getMessage();
      localNoSuchElementException.<init>(str);
      throw ((Throwable)localNoSuchElementException);
    }
  }
  
  public final boolean hasNext()
  {
    int i = a;
    byte[] arrayOfByte = b;
    int j = arrayOfByte.length;
    return i < j;
  }
}

/* Location:
 * Qualified Name:     c.g.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */