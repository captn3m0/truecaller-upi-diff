package c.g;

public final class b
  extends Error
{
  public b()
  {
    super("Kotlin reflection implementation is not found at runtime. Make sure you have kotlin-reflect.jar in the classpath");
  }
}

/* Location:
 * Qualified Name:     c.g.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */