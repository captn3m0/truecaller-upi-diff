package c.g;

import c.g.b.e;
import c.g.b.k;
import c.l.b;
import c.u;

public final class a
{
  public static final Class a(b paramb)
  {
    String str = "receiver$0";
    k.b(paramb, str);
    paramb = ((e)paramb).a();
    if (paramb != null) {
      return paramb;
    }
    paramb = new c/u;
    paramb.<init>("null cannot be cast to non-null type java.lang.Class<T>");
    throw paramb;
  }
  
  public static final Class b(b paramb)
  {
    String str1 = "receiver$0";
    k.b(paramb, str1);
    paramb = ((e)paramb).a();
    boolean bool = paramb.isPrimitive();
    if (!bool)
    {
      if (paramb != null) {
        return paramb;
      }
      paramb = new c/u;
      paramb.<init>("null cannot be cast to non-null type java.lang.Class<T>");
      throw paramb;
    }
    str1 = paramb.getName();
    if (str1 != null)
    {
      int i = str1.hashCode();
      String str2;
      switch (i)
      {
      default: 
        break;
      case 109413500: 
        str2 = "short";
        bool = str1.equals(str2);
        if (bool) {
          paramb = Short.class;
        }
        break;
      case 97526364: 
        str2 = "float";
        bool = str1.equals(str2);
        if (bool) {
          paramb = Float.class;
        }
        break;
      case 64711720: 
        str2 = "boolean";
        bool = str1.equals(str2);
        if (bool) {
          paramb = Boolean.class;
        }
        break;
      case 3625364: 
        str2 = "void";
        bool = str1.equals(str2);
        if (bool) {
          paramb = Void.class;
        }
        break;
      case 3327612: 
        str2 = "long";
        bool = str1.equals(str2);
        if (bool) {
          paramb = Long.class;
        }
        break;
      case 3052374: 
        str2 = "char";
        bool = str1.equals(str2);
        if (bool) {
          paramb = Character.class;
        }
        break;
      case 3039496: 
        str2 = "byte";
        bool = str1.equals(str2);
        if (bool) {
          paramb = Byte.class;
        }
        break;
      case 104431: 
        str2 = "int";
        bool = str1.equals(str2);
        if (bool) {
          paramb = Integer.class;
        }
        break;
      case -1325958191: 
        str2 = "double";
        bool = str1.equals(str2);
        if (bool) {
          paramb = Double.class;
        }
        break;
      }
    }
    if (paramb != null) {
      return paramb;
    }
    paramb = new c/u;
    paramb.<init>("null cannot be cast to non-null type java.lang.Class<T>");
    throw paramb;
  }
}

/* Location:
 * Qualified Name:     c.g.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */