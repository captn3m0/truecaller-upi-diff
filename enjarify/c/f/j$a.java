package c.f;

import c.g.b.a.a;
import java.io.BufferedReader;
import java.util.Iterator;

public final class j$a
  implements a, Iterator
{
  private String b;
  private boolean c;
  
  j$a(j paramj) {}
  
  public final boolean hasNext()
  {
    String str = b;
    boolean bool1 = true;
    if (str == null)
    {
      boolean bool2 = c;
      if (!bool2)
      {
        str = a.a.readLine();
        b = str;
        str = b;
        if (str == null) {
          c = bool1;
        }
      }
    }
    str = b;
    if (str != null) {
      return bool1;
    }
    return false;
  }
  
  public final void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Operation is not supported for read-only collection");
    throw localUnsupportedOperationException;
  }
}

/* Location:
 * Qualified Name:     c.f.j.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */