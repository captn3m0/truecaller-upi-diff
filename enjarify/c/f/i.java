package c.f;

import c.g.b.k;
import java.io.File;

public class i
  extends h
{
  /* Error */
  public static final File a(File paramFile1, File paramFile2)
  {
    // Byte code:
    //   0: aload_0
    //   1: ldc 6
    //   3: invokestatic 12	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: ldc 14
    //   8: astore_2
    //   9: aload_1
    //   10: aload_2
    //   11: invokestatic 12	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   14: aload_0
    //   15: invokevirtual 20	java/io/File:exists	()Z
    //   18: istore_3
    //   19: iload_3
    //   20: ifeq +205 -> 225
    //   23: aload_1
    //   24: invokevirtual 20	java/io/File:exists	()Z
    //   27: istore_3
    //   28: iload_3
    //   29: ifne +179 -> 208
    //   32: aload_0
    //   33: invokevirtual 23	java/io/File:isDirectory	()Z
    //   36: istore_3
    //   37: iload_3
    //   38: ifeq +32 -> 70
    //   41: aload_1
    //   42: invokevirtual 26	java/io/File:mkdirs	()Z
    //   45: istore_3
    //   46: iload_3
    //   47: ifeq +6 -> 53
    //   50: goto +121 -> 171
    //   53: new 28	c/f/d
    //   56: astore_2
    //   57: aload_2
    //   58: aload_0
    //   59: aload_1
    //   60: ldc 30
    //   62: invokespecial 34	c/f/d:<init>	(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)V
    //   65: aload_2
    //   66: checkcast 36	java/lang/Throwable
    //   69: athrow
    //   70: aload_1
    //   71: invokevirtual 40	java/io/File:getParentFile	()Ljava/io/File;
    //   74: astore_2
    //   75: aload_2
    //   76: ifnull +8 -> 84
    //   79: aload_2
    //   80: invokevirtual 26	java/io/File:mkdirs	()Z
    //   83: pop
    //   84: new 42	java/io/FileInputStream
    //   87: astore_2
    //   88: aload_2
    //   89: aload_0
    //   90: invokespecial 45	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   93: aload_2
    //   94: checkcast 47	java/io/Closeable
    //   97: astore_2
    //   98: aconst_null
    //   99: astore_0
    //   100: aload_2
    //   101: astore 4
    //   103: aload_2
    //   104: checkcast 42	java/io/FileInputStream
    //   107: astore 4
    //   109: new 49	java/io/FileOutputStream
    //   112: astore 5
    //   114: aload 5
    //   116: aload_1
    //   117: invokespecial 50	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   120: aload 5
    //   122: checkcast 47	java/io/Closeable
    //   125: astore 5
    //   127: aload 5
    //   129: astore 6
    //   131: aload 5
    //   133: checkcast 49	java/io/FileOutputStream
    //   136: astore 6
    //   138: aload 4
    //   140: checkcast 52	java/io/InputStream
    //   143: astore 4
    //   145: aload 6
    //   147: checkcast 54	java/io/OutputStream
    //   150: astore 6
    //   152: aload 4
    //   154: aload 6
    //   156: invokestatic 60	c/f/a:a	(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    //   159: pop2
    //   160: aload 5
    //   162: aconst_null
    //   163: invokestatic 65	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   166: aload_2
    //   167: aconst_null
    //   168: invokestatic 65	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   171: aload_1
    //   172: areturn
    //   173: astore_1
    //   174: aload_1
    //   175: athrow
    //   176: astore 7
    //   178: aload_1
    //   179: astore 4
    //   181: aload 7
    //   183: astore_1
    //   184: aload 5
    //   186: aload 4
    //   188: invokestatic 65	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   191: aload 7
    //   193: athrow
    //   194: astore_1
    //   195: goto +6 -> 201
    //   198: astore_0
    //   199: aload_0
    //   200: athrow
    //   201: aload_2
    //   202: aload_0
    //   203: invokestatic 65	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   206: aload_1
    //   207: athrow
    //   208: new 67	c/f/c
    //   211: astore_2
    //   212: aload_2
    //   213: aload_0
    //   214: aload_1
    //   215: ldc 69
    //   217: invokespecial 70	c/f/c:<init>	(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)V
    //   220: aload_2
    //   221: checkcast 36	java/lang/Throwable
    //   224: athrow
    //   225: new 72	c/f/k
    //   228: astore_1
    //   229: aload_1
    //   230: aload_0
    //   231: ldc 74
    //   233: iconst_0
    //   234: invokespecial 77	c/f/k:<init>	(Ljava/io/File;Ljava/lang/String;B)V
    //   237: aload_1
    //   238: checkcast 36	java/lang/Throwable
    //   241: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	242	0	paramFile1	File
    //   0	242	1	paramFile2	File
    //   8	213	2	localObject1	Object
    //   18	29	3	bool	boolean
    //   101	86	4	localObject2	Object
    //   112	73	5	localObject3	Object
    //   129	26	6	localObject4	Object
    //   176	16	7	localObject5	Object
    // Exception table:
    //   from	to	target	type
    //   131	136	173	finally
    //   138	143	173	finally
    //   145	150	173	finally
    //   154	160	173	finally
    //   174	176	176	finally
    //   199	201	194	finally
    //   103	107	198	finally
    //   109	112	198	finally
    //   116	120	198	finally
    //   120	125	198	finally
    //   162	166	198	finally
    //   186	191	198	finally
    //   191	194	198	finally
  }
  
  public static final File a(String paramString1, String paramString2, File paramFile)
  {
    k.b(paramString1, "prefix");
    paramString1 = File.createTempFile(paramString1, paramString2, paramFile);
    k.a(paramString1, "File.createTempFile(prefix, suffix, directory)");
    return paramString1;
  }
}

/* Location:
 * Qualified Name:     c.f.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */