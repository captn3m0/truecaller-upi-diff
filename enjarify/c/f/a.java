package c.f;

import c.g.b.k;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public final class a
{
  public static final long a(InputStream paramInputStream, OutputStream paramOutputStream)
  {
    k.b(paramInputStream, "receiver$0");
    k.b(paramOutputStream, "out");
    int i = 8192;
    byte[] arrayOfByte = new byte[i];
    int j = paramInputStream.read(arrayOfByte);
    long l1 = 0L;
    while (j >= 0)
    {
      paramOutputStream.write(arrayOfByte, 0, j);
      long l2 = j;
      l1 += l2;
      j = paramInputStream.read(arrayOfByte);
    }
    return l1;
  }
  
  public static final byte[] a(InputStream paramInputStream)
  {
    k.b(paramInputStream, "receiver$0");
    int i = paramInputStream.available();
    i = Math.max(8192, i);
    ByteArrayOutputStream localByteArrayOutputStream = new java/io/ByteArrayOutputStream;
    localByteArrayOutputStream.<init>(i);
    Object localObject = localByteArrayOutputStream;
    localObject = (OutputStream)localByteArrayOutputStream;
    b(paramInputStream, (OutputStream)localObject);
    paramInputStream = localByteArrayOutputStream.toByteArray();
    k.a(paramInputStream, "buffer.toByteArray()");
    return paramInputStream;
  }
}

/* Location:
 * Qualified Name:     c.f.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */