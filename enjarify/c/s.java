package c;

import c.g.b.k;
import java.io.Serializable;

public final class s
  implements Serializable
{
  public final Object a;
  public final Object b;
  public final Object c;
  
  public s(Object paramObject1, Object paramObject2, Object paramObject3)
  {
    a = paramObject1;
    b = paramObject2;
    c = paramObject3;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof s;
      if (bool1)
      {
        paramObject = (s)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = b;
          localObject2 = b;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = c;
            paramObject = c;
            boolean bool2 = k.a(localObject1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final int hashCode()
  {
    Object localObject1 = a;
    int i = 0;
    if (localObject1 != null)
    {
      j = localObject1.hashCode();
    }
    else
    {
      j = 0;
      localObject1 = null;
    }
    j *= 31;
    Object localObject2 = b;
    int k;
    if (localObject2 != null)
    {
      k = localObject2.hashCode();
    }
    else
    {
      k = 0;
      localObject2 = null;
    }
    int j = (j + k) * 31;
    localObject2 = c;
    if (localObject2 != null) {
      i = localObject2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("(");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", ");
    localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", ");
    localObject = c;
    localStringBuilder.append(localObject);
    localStringBuilder.append(')');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     c.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */