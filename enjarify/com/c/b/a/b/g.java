package com.c.b.a.b;

import e.c;
import e.c.a;
import e.r;
import e.s;
import e.w;
import io.reactivex.b;
import io.reactivex.h;
import io.reactivex.k;
import io.reactivex.n;
import io.reactivex.o;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public final class g
  extends c.a
{
  private final n a = null;
  
  public final c a(Type paramType, Annotation[] paramArrayOfAnnotation, s params)
  {
    paramArrayOfAnnotation = w.a(paramType);
    params = b.class;
    if (paramArrayOfAnnotation == params)
    {
      paramType = new com/c/b/a/b/f;
      localObject = a;
      paramType.<init>(Void.class, (n)localObject, false, true, false, false, false, true);
      return paramType;
    }
    params = io.reactivex.d.class;
    boolean bool1;
    if (paramArrayOfAnnotation == params) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    params = o.class;
    boolean bool2;
    if (paramArrayOfAnnotation == params) {
      bool2 = true;
    } else {
      bool2 = false;
    }
    params = h.class;
    boolean bool3;
    if (paramArrayOfAnnotation == params) {
      bool3 = true;
    } else {
      bool3 = false;
    }
    params = k.class;
    if ((paramArrayOfAnnotation != params) && (!bool1) && (!bool2) && (!bool3)) {
      return null;
    }
    boolean bool4 = paramType instanceof ParameterizedType;
    if (!bool4)
    {
      if (!bool1)
      {
        if (bool2) {
          paramType = "Single";
        } else {
          paramType = "Observable";
        }
      }
      else {
        paramType = "Flowable";
      }
      paramArrayOfAnnotation = new java/lang/IllegalStateException;
      params = new java/lang/StringBuilder;
      params.<init>();
      params.append(paramType);
      params.append(" return type must be parameterized as ");
      params.append(paramType);
      params.append("<Foo> or ");
      params.append(paramType);
      params.append("<? extends Foo>");
      paramType = params.toString();
      paramArrayOfAnnotation.<init>(paramType);
      throw paramArrayOfAnnotation;
    }
    paramType = (ParameterizedType)paramType;
    paramType = w.a(0, paramType);
    paramArrayOfAnnotation = w.a(paramType);
    params = r.class;
    Type localType;
    boolean bool5;
    boolean bool6;
    if (paramArrayOfAnnotation == params)
    {
      bool4 = paramType instanceof ParameterizedType;
      if (bool4)
      {
        paramType = (ParameterizedType)paramType;
        paramType = w.a(0, paramType);
        localType = paramType;
        bool5 = false;
        bool6 = false;
      }
      else
      {
        paramType = new java/lang/IllegalStateException;
        paramType.<init>("Response must be parameterized as Response<Foo> or Response<? extends Foo>");
        throw paramType;
      }
    }
    else
    {
      params = d.class;
      if (paramArrayOfAnnotation == params)
      {
        bool4 = paramType instanceof ParameterizedType;
        if (bool4)
        {
          paramType = (ParameterizedType)paramType;
          paramType = w.a(0, paramType);
          localType = paramType;
          bool5 = true;
          bool6 = false;
        }
        else
        {
          paramType = new java/lang/IllegalStateException;
          paramType.<init>("Result must be parameterized as Result<Foo> or Result<? extends Foo>");
          throw paramType;
        }
      }
      else
      {
        localType = paramType;
        bool5 = false;
        bool6 = true;
      }
    }
    paramType = new com/c/b/a/b/f;
    n localn = a;
    Object localObject = paramType;
    paramType.<init>(localType, localn, bool5, bool6, bool1, bool2, bool3, false);
    return paramType;
  }
}

/* Location:
 * Qualified Name:     com.c.b.a.b.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */