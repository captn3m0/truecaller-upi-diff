package com.c.b.a.b;

import io.reactivex.d;
import io.reactivex.d.e.a.s;
import io.reactivex.d.e.a.t;
import io.reactivex.d.e.a.u;
import io.reactivex.d.e.a.v;
import io.reactivex.h;
import io.reactivex.k;
import io.reactivex.k.1;
import io.reactivex.n;
import java.lang.reflect.Type;

final class f
  implements e.c
{
  private final Type a;
  private final n b;
  private final boolean c;
  private final boolean d;
  private final boolean e;
  private final boolean f;
  private final boolean g;
  private final boolean h;
  
  f(Type paramType, n paramn, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6)
  {
    a = paramType;
    b = paramn;
    c = paramBoolean1;
    d = paramBoolean2;
    e = paramBoolean3;
    f = paramBoolean4;
    g = paramBoolean5;
    h = paramBoolean6;
  }
  
  public final Object a(e.b paramb)
  {
    Object localObject = new com/c/b/a/b/b;
    ((b)localObject).<init>(paramb);
    boolean bool1 = c;
    if (bool1)
    {
      paramb = new com/c/b/a/b/e;
      paramb.<init>((k)localObject);
    }
    else
    {
      bool1 = d;
      if (bool1)
      {
        paramb = new com/c/b/a/b/a;
        paramb.<init>((k)localObject);
      }
      else
      {
        paramb = (e.b)localObject;
      }
    }
    localObject = b;
    if (localObject != null) {
      paramb = paramb.b((n)localObject);
    }
    boolean bool2 = e;
    if (bool2)
    {
      localObject = io.reactivex.a.e;
      io.reactivex.d.e.a.o localo = new io/reactivex/d/e/a/o;
      localo.<init>(paramb);
      paramb = k.1.a;
      int j = ((io.reactivex.a)localObject).ordinal();
      int i = paramb[j];
      switch (i)
      {
      default: 
        i = d.b();
        io.reactivex.d.b.b.a(i, "bufferSize");
        localObject = new io/reactivex/d/e/a/s;
        io.reactivex.c.a locala = io.reactivex.d.b.a.c;
        ((s)localObject).<init>(localo, i, locala);
        return io.reactivex.e.a.a((d)localObject);
      case 4: 
        paramb = new io/reactivex/d/e/a/u;
        paramb.<init>(localo);
        return io.reactivex.e.a.a(paramb);
      case 3: 
        return localo;
      case 2: 
        paramb = new io/reactivex/d/e/a/v;
        paramb.<init>(localo);
        return io.reactivex.e.a.a(paramb);
      }
      paramb = new io/reactivex/d/e/a/t;
      paramb.<init>(localo);
      return io.reactivex.e.a.a(paramb);
    }
    boolean bool3 = f;
    if (bool3)
    {
      localObject = new io/reactivex/d/e/c/f;
      ((io.reactivex.d.e.c.f)localObject).<init>(paramb);
      return io.reactivex.e.a.a((io.reactivex.o)localObject);
    }
    bool3 = g;
    if (bool3)
    {
      localObject = new io/reactivex/d/e/c/e;
      ((io.reactivex.d.e.c.e)localObject).<init>(paramb);
      return io.reactivex.e.a.a((h)localObject);
    }
    bool3 = h;
    if (bool3)
    {
      localObject = new io/reactivex/d/e/c/c;
      ((io.reactivex.d.e.c.c)localObject).<init>(paramb);
      paramb = io.reactivex.e.a.n;
      if (paramb != null) {
        return (io.reactivex.b)io.reactivex.e.a.a(paramb, localObject);
      }
      return localObject;
    }
    return paramb;
  }
  
  public final Type a()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.c.b.a.b.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */