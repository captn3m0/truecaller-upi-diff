package com.c.b.a.b;

import e.r;

public final class d
{
  private final r a;
  private final Throwable b;
  
  private d(r paramr, Throwable paramThrowable)
  {
    a = paramr;
    b = paramThrowable;
  }
  
  public static d a(r paramr)
  {
    if (paramr != null)
    {
      d locald = new com/c/b/a/b/d;
      locald.<init>(paramr, null);
      return locald;
    }
    paramr = new java/lang/NullPointerException;
    paramr.<init>("response == null");
    throw paramr;
  }
  
  public static d a(Throwable paramThrowable)
  {
    if (paramThrowable != null)
    {
      d locald = new com/c/b/a/b/d;
      locald.<init>(null, paramThrowable);
      return locald;
    }
    paramThrowable = new java/lang/NullPointerException;
    paramThrowable.<init>("error == null");
    throw paramThrowable;
  }
}

/* Location:
 * Qualified Name:     com.c.b.a.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */