package com.c.b.a.b;

import io.reactivex.a.b;
import io.reactivex.e.a;
import io.reactivex.m;

final class a$a
  implements m
{
  private final m a;
  private boolean b;
  
  a$a(m paramm)
  {
    a = paramm;
  }
  
  public final void a(b paramb)
  {
    a.a(paramb);
  }
  
  public final void a(Throwable paramThrowable)
  {
    boolean bool = b;
    if (!bool)
    {
      a.a(paramThrowable);
      return;
    }
    AssertionError localAssertionError = new java/lang/AssertionError;
    localAssertionError.<init>("This should never happen! Report as a bug with the full stacktrace.");
    localAssertionError.initCause(paramThrowable);
    a.a(localAssertionError);
  }
  
  public final void ak_()
  {
    boolean bool = b;
    if (!bool)
    {
      m localm = a;
      localm.ak_();
    }
  }
}

/* Location:
 * Qualified Name:     com.c.b.a.b.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */