package com.c.b.a.a.a;

import c.g.b.k;
import e.c;
import e.c.a;
import e.r;
import e.s;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import kotlinx.coroutines.ao;

public final class a
  extends c.a
{
  public static final a.b a;
  
  static
  {
    a.b localb = new com/c/b/a/a/a/a$b;
    localb.<init>((byte)0);
    a = localb;
  }
  
  public static final a a()
  {
    return a.b.a();
  }
  
  public final c a(Type paramType, Annotation[] paramArrayOfAnnotation, s params)
  {
    k.b(paramType, "returnType");
    String str = "annotations";
    k.b(paramArrayOfAnnotation, str);
    k.b(params, "retrofit");
    paramArrayOfAnnotation = ao.class;
    params = c.a.a(paramType);
    boolean bool = k.a(paramArrayOfAnnotation, params) ^ true;
    if (bool) {
      return null;
    }
    bool = paramType instanceof ParameterizedType;
    if (bool)
    {
      paramType = c.a.a((ParameterizedType)paramType);
      paramArrayOfAnnotation = c.a.a(paramType);
      params = r.class;
      bool = k.a(paramArrayOfAnnotation, params);
      if (bool)
      {
        bool = paramType instanceof ParameterizedType;
        if (bool)
        {
          paramArrayOfAnnotation = new com/c/b/a/a/a/a$c;
          paramType = c.a.a((ParameterizedType)paramType);
          k.a(paramType, "getParameterUpperBound(0, responseType)");
          paramArrayOfAnnotation.<init>(paramType);
          return (c)paramArrayOfAnnotation;
        }
        paramType = new java/lang/IllegalStateException;
        paramType.<init>("Response must be parameterized as Response<Foo> or Response<out Foo>");
        throw ((Throwable)paramType);
      }
      paramArrayOfAnnotation = new com/c/b/a/a/a/a$a;
      k.a(paramType, "responseType");
      paramArrayOfAnnotation.<init>(paramType);
      return (c)paramArrayOfAnnotation;
    }
    paramType = new java/lang/IllegalStateException;
    paramType.<init>("Deferred return type must be parameterized as Deferred<Foo> or Deferred<out Foo>");
    throw ((Throwable)paramType);
  }
}

/* Location:
 * Qualified Name:     com.c.b.a.a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */