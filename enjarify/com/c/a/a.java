package com.c.a;

import android.net.Uri;
import com.d.b.j;
import com.d.b.j.a;
import com.d.b.j.b;
import com.d.b.t;
import com.google.firebase.perf.network.FirebasePerfOkHttpClient;
import java.io.IOException;
import java.io.InputStream;
import okhttp3.ab.a;
import okhttp3.ad;
import okhttp3.ae;
import okhttp3.c;
import okhttp3.d;
import okhttp3.d.a;
import okhttp3.e.a;
import okhttp3.y;

public final class a
  implements j
{
  private final e.a a;
  private final c b;
  
  public a(y paramy)
  {
    a = paramy;
    paramy = l;
    b = paramy;
  }
  
  public final j.a a(Uri paramUri, int paramInt)
  {
    boolean bool1 = true;
    boolean bool2;
    boolean bool3;
    if (paramInt != 0)
    {
      bool2 = t.c(paramInt);
      if (bool2)
      {
        localObject1 = d.b;
      }
      else
      {
        localObject1 = new okhttp3/d$a;
        ((d.a)localObject1).<init>();
        bool3 = t.a(paramInt);
        if (!bool3) {
          a = bool1;
        }
        bool3 = t.b(paramInt);
        if (!bool3) {
          b = bool1;
        }
        localObject1 = ((d.a)localObject1).a();
      }
    }
    else
    {
      bool2 = false;
      localObject1 = null;
    }
    Object localObject2 = new okhttp3/ab$a;
    ((ab.a)localObject2).<init>();
    paramUri = paramUri.toString();
    paramUri = ((ab.a)localObject2).a(paramUri);
    if (localObject1 != null)
    {
      localObject1 = ((d)localObject1).toString();
      bool3 = ((String)localObject1).isEmpty();
      if (bool3)
      {
        localObject1 = "Cache-Control";
        paramUri.b((String)localObject1);
      }
      else
      {
        localObject2 = "Cache-Control";
        paramUri.a((String)localObject2, (String)localObject1);
      }
    }
    Object localObject1 = a;
    paramUri = paramUri.a();
    paramUri = FirebasePerfOkHttpClient.execute(((e.a)localObject1).a(paramUri));
    int i = c;
    int j = 300;
    if (i < j)
    {
      Object localObject3 = i;
      if (localObject3 == null)
      {
        bool1 = false;
        localb = null;
      }
      paramUri = g;
      localObject3 = new com/d/b/j$a;
      localObject1 = paramUri.d();
      long l = paramUri.b();
      ((j.a)localObject3).<init>((InputStream)localObject1, bool1, l);
      return (j.a)localObject3;
    }
    g.close();
    j.b localb = new com/d/b/j$b;
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    ((StringBuilder)localObject2).append(i);
    ((StringBuilder)localObject2).append(" ");
    paramUri = d;
    ((StringBuilder)localObject2).append(paramUri);
    paramUri = ((StringBuilder)localObject2).toString();
    localb.<init>(paramUri, paramInt, i);
    throw localb;
  }
  
  public final void a()
  {
    c localc = b;
    if (localc != null) {
      try
      {
        localc.close();
        return;
      }
      catch (IOException localIOException) {}
    }
  }
}

/* Location:
 * Qualified Name:     com.c.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */