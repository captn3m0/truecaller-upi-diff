package com.daamitt.prime.sdk;

public abstract interface ScoreCallback
{
  public abstract void onError(String paramString);
  
  public abstract void onScoreData(String paramString);
  
  public abstract void onScoreDataProgress(int paramInt1, int paramInt2);
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.ScoreCallback
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */