package com.daamitt.prime.sdk.b;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.daamitt.prime.sdk.a.k;
import com.daamitt.prime.sdk.a.l;
import com.daamitt.prime.sdk.a.m;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class b
  extends SQLiteOpenHelper
{
  private static final String f = "b";
  private static b g;
  public a a;
  public f b;
  public e c;
  public d d;
  public c e;
  
  private b(Context paramContext)
  {
    super(paramContext, "sdk.db", null, 2);
  }
  
  public static b a(Context paramContext)
  {
    synchronized (b.class)
    {
      Object localObject = g;
      if (localObject == null)
      {
        localObject = new com/daamitt/prime/sdk/b/b;
        ((b)localObject).<init>(paramContext);
        g = (b)localObject;
        paramContext = a.a((b)localObject);
        a = paramContext;
        paramContext = g;
        localObject = f.a(paramContext);
        b = ((f)localObject);
        paramContext = g;
        localObject = e.a(paramContext);
        c = ((e)localObject);
        paramContext = g;
        localObject = d.a(paramContext);
        d = ((d)localObject);
        paramContext = g;
        localObject = c.a(paramContext);
        e = ((c)localObject);
      }
      paramContext = g;
      return paramContext;
    }
  }
  
  public final int a(long paramLong, ContentValues paramContentValues)
  {
    return d.a(paramLong, paramContentValues);
  }
  
  public final int a(com.daamitt.prime.sdk.a.a parama, ContentValues paramContentValues)
  {
    return a.a(parama, paramContentValues);
  }
  
  public final int a(l paraml, ContentValues paramContentValues)
  {
    return c.a(paraml, paramContentValues);
  }
  
  public final com.daamitt.prime.sdk.a.a a(String paramString1, String paramString2, int paramInt)
  {
    return a.a(paramString1, paramString2, paramInt);
  }
  
  public final com.daamitt.prime.sdk.a.a a(String paramString1, String paramString2, int paramInt, boolean paramBoolean, String paramString3)
  {
    return a.a(paramString1, paramString2, paramInt, paramBoolean, paramString3);
  }
  
  public final k a(long paramLong)
  {
    return d.a(paramLong);
  }
  
  public final k a(String paramString)
  {
    d locald = d;
    k localk = null;
    if (paramString == null) {
      return null;
    }
    SQLiteDatabase localSQLiteDatabase = a;
    String str1 = "sms";
    String[] arrayOfString1 = b;
    String str2 = "UUID =?";
    String[] arrayOfString2 = new String[1];
    int i = 0;
    locald = null;
    arrayOfString2[0] = paramString;
    paramString = localSQLiteDatabase.query(str1, arrayOfString1, str2, arrayOfString2, null, null, null);
    if (paramString != null)
    {
      i = paramString.getCount();
      if (i > 0)
      {
        paramString.moveToFirst();
        boolean bool = paramString.isAfterLast();
        if (!bool) {
          localk = d.a(paramString);
        }
      }
    }
    if (paramString != null) {
      paramString.close();
    }
    return localk;
  }
  
  public final ArrayList a(int paramInt)
  {
    return a.a(paramInt);
  }
  
  public final ArrayList a(int[] paramArrayOfInt)
  {
    return b.a(paramArrayOfInt);
  }
  
  public final void a(com.daamitt.prime.sdk.a.a parama1, com.daamitt.prime.sdk.a.a parama2)
  {
    int i = i;
    int j = 3;
    if (j != i)
    {
      i = 6;
      j = i;
      if (i != j) {}
    }
    else
    {
      localObject = c;
      ((e)localObject).a(parama1, parama2);
    }
    Object localObject = m;
    com.daamitt.prime.sdk.a.b localb = m;
    Date localDate1 = c;
    Date localDate2;
    boolean bool;
    double d1;
    if (localDate1 != null)
    {
      localDate1 = c;
      if (localDate1 != null)
      {
        localDate1 = c;
        localDate2 = c;
        bool = localDate1.after(localDate2);
        if (!bool) {}
      }
      else
      {
        localDate1 = c;
        c = localDate1;
        d1 = a;
        a = d1;
      }
    }
    localDate1 = d;
    if (localDate1 != null)
    {
      localDate1 = d;
      if (localDate1 != null)
      {
        localDate1 = d;
        localDate2 = d;
        bool = localDate1.after(localDate2);
        if (!bool) {}
      }
      else
      {
        localDate1 = d;
        c = localDate1;
        d1 = b;
        b = d1;
      }
    }
    long l = System.currentTimeMillis();
    b.a(parama1, l);
    a.a(parama1, parama2, l);
  }
  
  public final void a(m paramm, ContentValues paramContentValues)
  {
    b.a(paramm, paramContentValues);
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt)
  {
    return b.d(paramCalendar1, paramCalendar2, paramInt);
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt, String paramString1, String paramString2)
  {
    return b.a(paramCalendar1, paramCalendar2, paramInt, paramString1, paramString2);
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2, ArrayList paramArrayList)
  {
    return d.a(paramCalendar1, paramCalendar2, paramArrayList);
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2, int[] paramArrayOfInt, int paramInt)
  {
    return e.a(paramCalendar1, paramCalendar2, paramArrayOfInt, paramInt);
  }
  
  public final com.daamitt.prime.sdk.a.a b(int paramInt)
  {
    return a.b(paramInt);
  }
  
  public final com.daamitt.prime.sdk.a.a b(long paramLong)
  {
    return a.b(paramLong);
  }
  
  public final ArrayList b(int[] paramArrayOfInt)
  {
    return a.b(paramArrayOfInt);
  }
  
  public final String[] b(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt)
  {
    return d.a(paramCalendar1, paramCalendar2, paramInt);
  }
  
  public final String c(long paramLong)
  {
    return a.c(paramLong);
  }
  
  public final String c(int[] paramArrayOfInt)
  {
    Object localObject1 = a;
    int i = 0;
    while (i <= 0)
    {
      int j = paramArrayOfInt[0];
      long l = j;
      Object localObject2 = ((a)localObject1).a(l);
      Object localObject3 = new java/util/ArrayList;
      ((ArrayList)localObject3).<init>();
      ((ArrayList)localObject3).add(localObject2);
      localObject2 = ((a)localObject1).a(0, (String)localObject2, (ArrayList)localObject3);
      localObject3 = "UUID IN ";
      localObject2 = String.valueOf(localObject2);
      String str1 = ((String)localObject3).concat((String)localObject2);
      SQLiteDatabase localSQLiteDatabase = a;
      String str2 = "accounts";
      String[] arrayOfString = { "_id" };
      localObject2 = localSQLiteDatabase.query(str2, arrayOfString, str1, null, null, null, null);
      if (localObject2 != null)
      {
        int k = ((Cursor)localObject2).getCount();
        if (k > 0)
        {
          paramArrayOfInt = new java/util/ArrayList;
          paramArrayOfInt.<init>();
          ((Cursor)localObject2).moveToFirst();
          for (;;)
          {
            boolean bool = ((Cursor)localObject2).isAfterLast();
            if (bool) {
              break;
            }
            int m = ((Cursor)localObject2).getColumnIndexOrThrow("_id");
            localObject1 = ((Cursor)localObject2).getString(m);
            paramArrayOfInt.add(localObject1);
            ((Cursor)localObject2).moveToNext();
          }
          localObject1 = new String[paramArrayOfInt.size()];
          paramArrayOfInt = a.a((String[])paramArrayOfInt.toArray((Object[])localObject1));
          ((Cursor)localObject2).close();
          return paramArrayOfInt;
        }
      }
      if (localObject2 != null) {
        ((Cursor)localObject2).close();
      }
      i += 1;
    }
    return a.a(paramArrayOfInt);
  }
  
  public void onCreate(SQLiteDatabase paramSQLiteDatabase)
  {
    a.a(paramSQLiteDatabase);
    f.a(paramSQLiteDatabase);
    e.a(paramSQLiteDatabase);
    d.a(paramSQLiteDatabase);
    c.a(paramSQLiteDatabase);
  }
  
  public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    com.daamitt.prime.sdk.a.e.b();
    d.a(paramSQLiteDatabase, paramInt1);
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */