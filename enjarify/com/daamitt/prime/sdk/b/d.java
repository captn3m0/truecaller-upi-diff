package com.daamitt.prime.sdk.b;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import com.daamitt.prime.sdk.a.e;
import com.daamitt.prime.sdk.a.i.a;
import com.daamitt.prime.sdk.a.k;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class d
{
  private static final String c = "d";
  private static d d;
  private static String e = "COUNT(*) as bounceCount ";
  private static String f = "COUNT(*) as notPaidCount ";
  public SQLiteDatabase a = null;
  public String[] b;
  
  private d()
  {
    String[] tmp14_11 = new String[22];
    String[] tmp15_14 = tmp14_11;
    String[] tmp15_14 = tmp14_11;
    tmp15_14[0] = "_id";
    tmp15_14[1] = "smsId";
    String[] tmp24_15 = tmp15_14;
    String[] tmp24_15 = tmp15_14;
    tmp24_15[2] = "sender";
    tmp24_15[3] = "date";
    String[] tmp33_24 = tmp24_15;
    String[] tmp33_24 = tmp24_15;
    tmp33_24[4] = "body";
    tmp33_24[5] = "lat";
    String[] tmp42_33 = tmp33_24;
    String[] tmp42_33 = tmp33_24;
    tmp42_33[6] = "long";
    tmp42_33[7] = "locAccuracy";
    String[] tmp53_42 = tmp42_33;
    String[] tmp53_42 = tmp42_33;
    tmp53_42[8] = "tags";
    tmp53_42[9] = "accountId";
    String[] tmp64_53 = tmp53_42;
    String[] tmp64_53 = tmp53_42;
    tmp64_53[10] = "parsed";
    tmp64_53[11] = "UUID";
    String[] tmp75_64 = tmp64_53;
    String[] tmp75_64 = tmp64_53;
    tmp75_64[12] = "modifyCount";
    tmp75_64[13] = "smsFlags";
    String[] tmp86_75 = tmp75_64;
    String[] tmp86_75 = tmp75_64;
    tmp86_75[14] = "patternUID";
    tmp86_75[15] = "previousUUID";
    String[] tmp97_86 = tmp86_75;
    String[] tmp97_86 = tmp86_75;
    tmp97_86[16] = "URI";
    tmp97_86[17] = "probability";
    String[] tmp108_97 = tmp97_86;
    String[] tmp108_97 = tmp97_86;
    tmp108_97[18] = "simSubscriptionId";
    tmp108_97[19] = "simSlotId";
    tmp108_97[20] = "threadId";
    String[] tmp124_108 = tmp108_97;
    tmp124_108[21] = "metaData";
    String[] arrayOfString = tmp124_108;
    b = arrayOfString;
  }
  
  public static k a(Cursor paramCursor)
  {
    int i = paramCursor.getColumnIndexOrThrow("_id");
    i = paramCursor.getInt(i);
    int j = paramCursor.getColumnIndexOrThrow("sender");
    String str1 = paramCursor.getString(j);
    Date localDate = new java/util/Date;
    int k = paramCursor.getColumnIndexOrThrow("date");
    long l1 = paramCursor.getLong(k);
    localDate.<init>(l1);
    k = paramCursor.getColumnIndexOrThrow("body");
    String str2 = paramCursor.getString(k);
    String str3 = "accountId";
    int m = paramCursor.getColumnIndexOrThrow(str3);
    m = paramCursor.getInt(m);
    String str4 = "smsId";
    int n = paramCursor.getColumnIndexOrThrow(str4);
    long l2 = paramCursor.getLong(n);
    String str5 = "smsFlags";
    int i1 = paramCursor.getColumnIndexOrThrow(str5);
    i1 = paramCursor.getInt(i1);
    int i2 = paramCursor.getColumnIndexOrThrow("UUID");
    String str6 = paramCursor.getString(i2);
    int i3 = paramCursor.getColumnIndexOrThrow("previousUUID");
    String str7 = paramCursor.getString(i3);
    int i4 = paramCursor.getColumnIndexOrThrow("URI");
    String str8 = paramCursor.getString(i4);
    String str9 = "probability";
    int i5 = paramCursor.getColumnIndexOrThrow(str9);
    double d1 = paramCursor.getDouble(i5);
    String str10 = "simSubscriptionId";
    int i6 = paramCursor.getColumnIndexOrThrow(str10);
    i6 = paramCursor.getInt(i6);
    int i7 = paramCursor.getColumnIndexOrThrow("simSlotId");
    int i8 = paramCursor.getInt(i7);
    i7 = paramCursor.getColumnIndexOrThrow("threadId");
    i7 = paramCursor.getInt(i7);
    int i9 = i7;
    k localk = new com/daamitt/prime/sdk/a/k;
    localk.<init>(str1, str2, localDate);
    C = l2;
    long l3 = i;
    o = l3;
    w = m;
    String str11 = "parsed";
    i = paramCursor.getColumnIndexOrThrow(str11);
    int i10 = paramCursor.getInt(i);
    i = 1;
    if (i10 != i)
    {
      i = 0;
      str11 = null;
    }
    E = i;
    G = i1;
    H = str6;
    I = str7;
    M = str8;
    localk.a(d1);
    O = i6;
    i10 = i8;
    P = i8;
    i10 = i9;
    Q = i9;
    return localk;
  }
  
  public static d a(b paramb)
  {
    d locald = d;
    if (locald == null)
    {
      locald = new com/daamitt/prime/sdk/b/d;
      locald.<init>();
      d = locald;
      paramb = paramb.getWritableDatabase();
      a = paramb;
    }
    return d;
  }
  
  public static void a(SQLiteDatabase paramSQLiteDatabase)
  {
    e.b();
    paramSQLiteDatabase.execSQL("create table if not exists sms(_id integer primary key autoincrement,smsId integer not null,sender text not null,date integer not null,body text not null,lat double default 360,long double default 360,locAccuracy double default -1, tags text not null default other,accountId integer not null,parsed boolean default 0,UUID text,modifyCount integer default 1,smsFlags integer default 0,patternUID  integer default 0,previousUUID text,URI text, probability real,simSubscriptionId integer,simSlotId integer,threadId integer,creator text,metaData real);");
    paramSQLiteDatabase.execSQL("create trigger if not exists SmsTriggerModifiedFlag After update on sms for each row  Begin  Update sms Set modifyCount = modifyCount + 1  Where _id =  New._id;  End; ");
  }
  
  public static void a(SQLiteDatabase paramSQLiteDatabase, int paramInt)
  {
    e.b();
    int i = 1;
    if (paramInt == i)
    {
      e.b();
      String str = "ALTER TABLE sms ADD COLUMN metaData real";
      paramSQLiteDatabase.execSQL(str);
    }
  }
  
  public static void b(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.beginTransaction();
    String str = "drop table if exists sms";
    try
    {
      paramSQLiteDatabase.execSQL(str);
      str = "drop trigger if exists SmsTriggerModifiedFlag";
      paramSQLiteDatabase.execSQL(str);
      a(paramSQLiteDatabase);
      paramSQLiteDatabase.setTransactionSuccessful();
      return;
    }
    finally
    {
      paramSQLiteDatabase.endTransaction();
    }
  }
  
  public final int a(long paramLong, ContentValues paramContentValues)
  {
    long l = 0L;
    boolean bool = paramLong < l;
    if (!bool)
    {
      SQLiteDatabase localSQLiteDatabase = a;
      String str = String.valueOf(paramLong);
      str = "_id = ".concat(str);
      return localSQLiteDatabase.update("sms", paramContentValues, str, null);
    }
    e.a();
    return -1;
  }
  
  public final k a(long paramLong)
  {
    SQLiteDatabase localSQLiteDatabase = a;
    String str1 = "sms";
    String[] arrayOfString = b;
    Object localObject = String.valueOf(paramLong);
    String str2 = "_id = ".concat((String)localObject);
    localObject = localSQLiteDatabase.query(str1, arrayOfString, str2, null, null, null, null);
    k localk = null;
    if (localObject != null)
    {
      int i = ((Cursor)localObject).getCount();
      if (i > 0)
      {
        ((Cursor)localObject).moveToFirst();
        boolean bool = ((Cursor)localObject).isAfterLast();
        if (!bool) {
          localk = a((Cursor)localObject);
        }
        ((Cursor)localObject).close();
        return localk;
      }
    }
    if (localObject != null) {
      ((Cursor)localObject).close();
    }
    return null;
  }
  
  public final boolean a(String paramString1, String paramString2, Date paramDate, long paramLong)
  {
    String str1 = paramString2;
    String str2 = "total";
    boolean bool1 = true;
    Object localObject1 = new String[bool1];
    String str3 = String.valueOf(str2);
    Object localObject2 = "COUNT(_id) AS ".concat(str3);
    localObject1[0] = localObject2;
    str3 = "";
    Object localObject3 = paramString1;
    localObject2 = paramString1.replace("-", str3);
    int i = 2;
    int j = 3;
    long l1 = 0L;
    boolean bool2 = paramLong < l1;
    int k;
    Object localObject4;
    String str4;
    Object localObject5;
    if (bool2)
    {
      k = 4;
      localObject4 = new String[k];
      str4 = String.valueOf(paramDate.getTime() - paramLong);
      localObject4[0] = str4;
      long l2 = paramDate.getTime() + paramLong;
      str4 = String.valueOf(l2);
      localObject4[bool1] = str4;
      localObject2 = ((String)localObject2).toUpperCase();
      localObject4[i] = localObject2;
      localObject4[j] = paramString2;
      localObject3 = "date >=? AND date <=? AND  replace(sender,'-','') =? AND body =? ";
      localObject5 = localObject4;
    }
    else
    {
      localObject3 = new String[j];
      long l3 = paramDate.getTime();
      localObject4 = String.valueOf(l3);
      localObject3[0] = localObject4;
      localObject2 = ((String)localObject2).toUpperCase();
      localObject3[bool1] = localObject2;
      localObject3[i] = paramString2;
      String str5 = "date =? AND replace(sender,'-','') =? AND body =? ";
      localObject5 = localObject3;
      localObject3 = str5;
    }
    Cursor localCursor = null;
    int i2;
    try
    {
      localObject2 = a;
      str3 = "sms";
      k = 0;
      localObject4 = null;
      bool2 = false;
      str4 = null;
      localCursor = ((SQLiteDatabase)localObject2).query(str3, (String[])localObject1, (String)localObject3, (String[])localObject5, null, null, null);
      localCursor.moveToFirst();
      m = localCursor.isAfterLast();
      if (m == 0)
      {
        i1 = localCursor.getColumnIndex(str2);
        i2 = localCursor.getInt(i1);
      }
      else
      {
        i2 = 0;
        str1 = null;
      }
    }
    catch (SQLiteException localSQLiteException)
    {
      int i1 = paramString2.length();
      i1 = str1.codePointCount(0, i1);
      int m = 0;
      localObject2 = null;
      while (m < i1)
      {
        str3 = "%04x";
        localObject1 = new Object[bool1];
        j = str1.codePointAt(m);
        localObject3 = Integer.valueOf(j);
        localObject1[0] = localObject3;
        String.format(str3, (Object[])localObject1);
        int n;
        m += 1;
      }
      e.c();
      i2 = 0;
      str1 = null;
    }
    if (localCursor != null) {
      localCursor.close();
    }
    if (i2 > 0) {
      return bool1;
    }
    return false;
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2)
  {
    int i = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    e.a();
    boolean bool1 = paramCalendar1.before(paramCalendar2);
    if (bool1)
    {
      String[] arrayOfString = new String[i];
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>(" date between ");
      long l1 = paramCalendar1.getTime().getTime();
      localStringBuilder.append(l1);
      localStringBuilder.append(" AND ");
      l1 = paramCalendar2.getTime().getTime();
      localStringBuilder.append(l1);
      localStringBuilder.append(" AND parsed != 1 ");
      String str1 = localStringBuilder.toString();
      paramCalendar2 = Calendar.getInstance().getTimeZone();
      long l2 = paramCalendar2.getRawOffset();
      int j = paramCalendar2.getDSTSavings();
      long l3 = j;
      l2 += l3;
      l3 = 3600000L;
      long l4 = l2 / l3;
      l2 %= l3;
      l3 = 60000L;
      l2 /= l3;
      paramCalendar2 = new java/lang/StringBuilder;
      paramCalendar2.<init>("strftime('%j',date(date/1000,'unixepoch','");
      paramCalendar2.append(l4);
      paramCalendar2.append(" hours','");
      paramCalendar2.append(l2);
      paramCalendar2.append(" minutes')) as Day");
      paramCalendar2 = paramCalendar2.toString();
      String str2 = "Day";
      Object localObject1 = new String[2];
      int k = 0;
      localStringBuilder = null;
      localObject1[0] = paramCalendar2;
      paramCalendar2 = e;
      int m = 1;
      localObject1[m] = paramCalendar2;
      Object localObject2 = a;
      String str3 = "sms";
      paramCalendar2 = ((SQLiteDatabase)localObject2).query(str3, (String[])localObject1, str1, null, str2, null, null, null);
      if (paramCalendar2 != null)
      {
        int n = paramCalendar2.getCount();
        if (n > 0)
        {
          paramCalendar2.moveToFirst();
          n = 6;
          int i2 = paramCalendar1.get(n);
          int i3 = paramCalendar1.getActualMaximum(n);
          for (;;)
          {
            boolean bool2 = paramCalendar2.isAfterLast();
            if (bool2) {
              break;
            }
            int i1 = paramCalendar2.getColumnIndexOrThrow("Day");
            localObject2 = paramCalendar2.getString(i1);
            int i4 = paramCalendar2.getColumnIndexOrThrow("bounceCount");
            localObject1 = paramCalendar2.getString(i4);
            int i5 = -1;
            try
            {
              i1 = Integer.parseInt((String)localObject2);
            }
            catch (NumberFormatException localNumberFormatException)
            {
              i1 = -1;
            }
            if ((i1 != i5) && (localObject1 != null)) {
              if (i1 >= i2)
              {
                i1 -= i2;
                i1 = i - i1 - m;
                arrayOfString[i1] = localObject1;
              }
              else
              {
                i5 = i3 - i2 + i1;
                i1 = i - i5 - m;
                arrayOfString[i1] = localObject1;
              }
            }
            paramCalendar2.moveToNext();
          }
        }
      }
      if (paramCalendar2 != null) {
        paramCalendar2.close();
      }
      while (k < i)
      {
        paramCalendar1 = arrayOfString[k];
        boolean bool3 = TextUtils.isEmpty(paramCalendar1);
        if (bool3)
        {
          paramCalendar1 = "";
          arrayOfString[k] = paramCalendar1;
        }
        k += 1;
      }
      return arrayOfString;
    }
    return null;
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt)
  {
    int i = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    e.a();
    boolean bool1 = paramCalendar1.before(paramCalendar2);
    if (bool1)
    {
      String[] arrayOfString = new String[i];
      Object localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>(" date between ");
      long l1 = paramCalendar1.getTime().getTime();
      ((StringBuilder)localObject1).append(l1);
      String str1 = " AND ";
      ((StringBuilder)localObject1).append(str1);
      l1 = paramCalendar2.getTime().getTime();
      ((StringBuilder)localObject1).append(l1);
      paramCalendar2 = ((StringBuilder)localObject1).toString();
      Calendar localCalendar;
      if (paramInt != 0)
      {
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        ((StringBuilder)localObject1).append(paramCalendar2);
        ((StringBuilder)localObject1).append(" AND smsFlags & ");
        ((StringBuilder)localObject1).append(paramInt);
        ((StringBuilder)localObject1).append(" != 0 ");
        paramCalendar2 = ((StringBuilder)localObject1).toString();
        localCalendar = paramCalendar2;
      }
      else
      {
        localCalendar = paramCalendar2;
      }
      paramCalendar2 = Calendar.getInstance().getTimeZone();
      long l2 = paramCalendar2.getRawOffset();
      int j = paramCalendar2.getDSTSavings();
      long l3 = j;
      l2 += l3;
      l3 = 3600000L;
      long l4 = l2 / l3;
      l2 %= l3;
      l3 = 60000L;
      l2 /= l3;
      paramCalendar2 = new java/lang/StringBuilder;
      paramCalendar2.<init>("strftime('%j',date(date/1000,'unixepoch','");
      paramCalendar2.append(l4);
      paramCalendar2.append(" hours','");
      paramCalendar2.append(l2);
      paramCalendar2.append(" minutes')) as Day");
      paramCalendar2 = paramCalendar2.toString();
      String str2 = "Day";
      Object localObject2 = new String[2];
      paramInt = 0;
      localObject2[0] = paramCalendar2;
      paramCalendar2 = e;
      int k = 1;
      localObject2[k] = paramCalendar2;
      localObject1 = a;
      str1 = "sms";
      paramCalendar2 = ((SQLiteDatabase)localObject1).query(str1, (String[])localObject2, localCalendar, null, str2, null, null, null);
      if (paramCalendar2 != null)
      {
        int m = paramCalendar2.getCount();
        if (m > 0)
        {
          paramCalendar2.moveToFirst();
          m = 6;
          int i1 = paramCalendar1.get(m);
          int i2 = paramCalendar1.getActualMaximum(m);
          for (;;)
          {
            boolean bool2 = paramCalendar2.isAfterLast();
            if (bool2) {
              break;
            }
            int n = paramCalendar2.getColumnIndexOrThrow("Day");
            localObject1 = paramCalendar2.getString(n);
            int i3 = paramCalendar2.getColumnIndexOrThrow("bounceCount");
            localObject2 = paramCalendar2.getString(i3);
            int i4 = -1;
            try
            {
              n = Integer.parseInt((String)localObject1);
            }
            catch (NumberFormatException localNumberFormatException)
            {
              n = -1;
            }
            if ((n != i4) && (localObject2 != null)) {
              if (n >= i1)
              {
                n -= i1;
                n = i - n - k;
                arrayOfString[n] = localObject2;
              }
              else
              {
                i4 = i2 - i1 + n;
                n = i - i4 - k;
                arrayOfString[n] = localObject2;
              }
            }
            paramCalendar2.moveToNext();
          }
        }
      }
      if (paramCalendar2 != null) {
        paramCalendar2.close();
      }
      while (paramInt < i)
      {
        paramCalendar1 = arrayOfString[paramInt];
        boolean bool3 = TextUtils.isEmpty(paramCalendar1);
        if (bool3)
        {
          paramCalendar1 = "";
          arrayOfString[paramInt] = paramCalendar1;
        }
        paramInt += 1;
      }
      return arrayOfString;
    }
    return null;
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2, ArrayList paramArrayList)
  {
    int i = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    e.a();
    boolean bool1 = paramCalendar1.before(paramCalendar2);
    if (bool1)
    {
      String[] arrayOfString = new String[i];
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>(" date between ");
      long l1 = paramCalendar1.getTime().getTime();
      localStringBuilder.append(l1);
      Object localObject1 = " AND ";
      localStringBuilder.append((String)localObject1);
      l1 = paramCalendar2.getTime().getTime();
      localStringBuilder.append(l1);
      localStringBuilder.append(" AND smsFlags & 16777216 != 0 ");
      paramCalendar2 = localStringBuilder.toString();
      int j = 0;
      localStringBuilder = null;
      int k;
      int i1;
      if (paramArrayList != null)
      {
        k = paramArrayList.size();
        if (k > 0)
        {
          localObject1 = new java/lang/StringBuilder;
          ((StringBuilder)localObject1).<init>();
          ((StringBuilder)localObject1).append(paramCalendar2);
          ((StringBuilder)localObject1).append(" AND ( ");
          localObject1 = ((StringBuilder)localObject1).toString();
          n = 0;
          paramCalendar2 = null;
          for (;;)
          {
            i1 = paramArrayList.size();
            if (n >= i1) {
              break;
            }
            str1 = (String)paramArrayList.get(n);
            if (n != 0)
            {
              localObject2 = new java/lang/StringBuilder;
              ((StringBuilder)localObject2).<init>();
              ((StringBuilder)localObject2).append((String)localObject1);
              ((StringBuilder)localObject2).append(" OR ");
              localObject1 = ((StringBuilder)localObject2).toString();
            }
            localObject2 = new java/lang/StringBuilder;
            ((StringBuilder)localObject2).<init>();
            ((StringBuilder)localObject2).append((String)localObject1);
            ((StringBuilder)localObject2).append(" lower(sender) LIKE '");
            ((StringBuilder)localObject2).append(str1);
            ((StringBuilder)localObject2).append("' ");
            localObject1 = ((StringBuilder)localObject2).toString();
            n += 1;
          }
          paramCalendar2 = new java/lang/StringBuilder;
          paramCalendar2.<init>();
          paramCalendar2.append((String)localObject1);
          paramArrayList = ")";
          paramCalendar2.append(paramArrayList);
          paramCalendar2 = paramCalendar2.toString();
          localCalendar = paramCalendar2;
          break label335;
        }
      }
      Calendar localCalendar = paramCalendar2;
      label335:
      paramCalendar2 = Calendar.getInstance().getTimeZone();
      l1 = paramCalendar2.getRawOffset();
      int n = paramCalendar2.getDSTSavings();
      long l2 = n;
      l1 += l2;
      l2 = 3600000L;
      long l3 = l1 / l2;
      l1 %= l2;
      l2 = 60000L;
      l1 /= l2;
      paramCalendar2 = new java/lang/StringBuilder;
      paramCalendar2.<init>("strftime('%j',date(date/1000,'unixepoch','");
      paramCalendar2.append(l3);
      paramCalendar2.append(" hours','");
      paramCalendar2.append(l1);
      paramArrayList = " minutes')) as Day";
      paramCalendar2.append(paramArrayList);
      paramCalendar2 = paramCalendar2.toString();
      String str2 = "Day";
      Object localObject2 = new String[2];
      localObject2[0] = paramCalendar2;
      paramCalendar2 = f;
      int i2 = 1;
      localObject2[i2] = paramCalendar2;
      localObject1 = a;
      String str1 = "sms";
      paramCalendar2 = ((SQLiteDatabase)localObject1).query(str1, (String[])localObject2, localCalendar, null, str2, null, null, null);
      if (paramCalendar2 != null)
      {
        k = paramCalendar2.getCount();
        if (k > 0)
        {
          paramCalendar2.moveToFirst();
          k = 6;
          i1 = paramCalendar1.get(k);
          int i3 = paramCalendar1.getActualMaximum(k);
          for (;;)
          {
            boolean bool2 = paramCalendar2.isAfterLast();
            if (bool2) {
              break;
            }
            int m = paramCalendar2.getColumnIndexOrThrow("Day");
            localObject1 = paramCalendar2.getString(m);
            int i4 = paramCalendar2.getColumnIndexOrThrow("notPaidCount");
            localObject2 = paramCalendar2.getString(i4);
            int i5 = -1;
            try
            {
              m = Integer.parseInt((String)localObject1);
            }
            catch (NumberFormatException localNumberFormatException)
            {
              m = -1;
            }
            if ((m != i5) && (localObject2 != null)) {
              if (m >= i1)
              {
                m -= i1;
                m = i - m - i2;
                arrayOfString[m] = localObject2;
              }
              else
              {
                i5 = i3 - i1 + m;
                m = i - i5 - i2;
                arrayOfString[m] = localObject2;
              }
            }
            paramCalendar2.moveToNext();
          }
        }
      }
      if (paramCalendar2 != null) {
        paramCalendar2.close();
      }
      while (j < i)
      {
        paramCalendar1 = arrayOfString[j];
        boolean bool3 = TextUtils.isEmpty(paramCalendar1);
        if (bool3)
        {
          paramCalendar1 = "";
          arrayOfString[j] = paramCalendar1;
        }
        j += 1;
      }
      return arrayOfString;
    }
    return null;
  }
  
  public final String[] b(Calendar paramCalendar1, Calendar paramCalendar2)
  {
    int i = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    e.a();
    boolean bool1 = paramCalendar1.before(paramCalendar2);
    if (bool1)
    {
      String[] arrayOfString = new String[i];
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>(" date between ");
      long l1 = paramCalendar1.getTime().getTime();
      localStringBuilder.append(l1);
      localStringBuilder.append(" AND ");
      l1 = paramCalendar2.getTime().getTime();
      localStringBuilder.append(l1);
      localStringBuilder.append(" AND parsed != 1  AND smsFlags & 134217728 != 0 ");
      String str1 = localStringBuilder.toString();
      paramCalendar2 = Calendar.getInstance().getTimeZone();
      long l2 = paramCalendar2.getRawOffset();
      int j = paramCalendar2.getDSTSavings();
      long l3 = j;
      l2 += l3;
      l3 = 3600000L;
      long l4 = l2 / l3;
      l2 %= l3;
      l3 = 60000L;
      l2 /= l3;
      paramCalendar2 = new java/lang/StringBuilder;
      paramCalendar2.<init>("strftime('%j',date(date/1000,'unixepoch','");
      paramCalendar2.append(l4);
      paramCalendar2.append(" hours','");
      paramCalendar2.append(l2);
      paramCalendar2.append(" minutes')) as Day");
      paramCalendar2 = paramCalendar2.toString();
      String str2 = "Day";
      Object localObject1 = new String[2];
      int k = 0;
      localStringBuilder = null;
      localObject1[0] = paramCalendar2;
      int m = 1;
      localObject1[m] = "metaData";
      String str3 = " MAX(metaData)";
      Object localObject2 = a;
      String str4 = "sms";
      paramCalendar2 = ((SQLiteDatabase)localObject2).query(str4, (String[])localObject1, str1, null, str2, str3, null, null);
      if (paramCalendar2 != null)
      {
        int n = paramCalendar2.getCount();
        if (n > 0)
        {
          paramCalendar2.moveToFirst();
          n = 6;
          int i2 = paramCalendar1.get(n);
          int i3 = paramCalendar1.getActualMaximum(n);
          for (;;)
          {
            boolean bool2 = paramCalendar2.isAfterLast();
            if (bool2) {
              break;
            }
            int i1 = paramCalendar2.getColumnIndexOrThrow("Day");
            localObject2 = paramCalendar2.getString(i1);
            int i4 = paramCalendar2.getColumnIndexOrThrow("metaData");
            localObject1 = paramCalendar2.getString(i4);
            int i5 = -1;
            try
            {
              i1 = Integer.parseInt((String)localObject2);
            }
            catch (NumberFormatException localNumberFormatException)
            {
              i1 = -1;
            }
            if ((i1 != i5) && (localObject1 != null))
            {
              localObject1 = Double.valueOf((String)localObject1);
              double d1 = ((Double)localObject1).doubleValue();
              i4 = (int)d1;
              if (i1 >= i2)
              {
                i1 -= i2;
                i1 = i - i1 - m;
                localObject1 = String.valueOf(i4);
                arrayOfString[i1] = localObject1;
              }
              else
              {
                i5 = i3 - i2 + i1;
                i1 = i - i5 - m;
                localObject1 = String.valueOf(i4);
                arrayOfString[i1] = localObject1;
              }
            }
            paramCalendar2.moveToNext();
          }
        }
      }
      if (paramCalendar2 != null) {
        paramCalendar2.close();
      }
      while (k < i)
      {
        paramCalendar1 = arrayOfString[k];
        boolean bool3 = TextUtils.isEmpty(paramCalendar1);
        if (bool3)
        {
          paramCalendar1 = "";
          arrayOfString[k] = paramCalendar1;
        }
        k += 1;
      }
      return arrayOfString;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */