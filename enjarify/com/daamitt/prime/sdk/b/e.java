package com.daamitt.prime.sdk.b;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.text.TextUtils;
import com.daamitt.prime.sdk.a.a;
import com.daamitt.prime.sdk.a.c.a;
import com.daamitt.prime.sdk.a.h;
import com.daamitt.prime.sdk.a.i.a;
import com.daamitt.prime.sdk.a.k;
import com.daamitt.prime.sdk.a.l;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.TimeZone;

public class e
{
  private static final String b = "e";
  private static String[] d;
  private static String[] e = tmp145_129;
  private static e f;
  private static String g = "SUM(amount) as Amount ";
  private static String h = "statements JOIN sms ON statements.wSmsId = sms._id";
  public SQLiteDatabase a = null;
  private b c;
  
  static
  {
    String[] tmp5_2 = new String[13];
    String[] tmp6_5 = tmp5_2;
    String[] tmp6_5 = tmp5_2;
    tmp6_5[0] = "_id";
    tmp6_5[1] = "wSmsId";
    String[] tmp15_6 = tmp6_5;
    String[] tmp15_6 = tmp6_5;
    tmp15_6[2] = "amount";
    tmp15_6[3] = "minDueAmount";
    String[] tmp24_15 = tmp15_6;
    String[] tmp24_15 = tmp15_6;
    tmp24_15[4] = "dueDate";
    tmp24_15[5] = "type";
    String[] tmp33_24 = tmp24_15;
    String[] tmp33_24 = tmp24_15;
    tmp33_24[6] = "status";
    tmp33_24[7] = "accountId";
    String[] tmp44_33 = tmp33_24;
    String[] tmp44_33 = tmp33_24;
    tmp44_33[8] = "UUID";
    tmp44_33[9] = "modifyCount";
    String[] tmp55_44 = tmp44_33;
    String[] tmp55_44 = tmp44_33;
    tmp55_44[10] = "paymentDate";
    tmp55_44[11] = "billType";
    tmp55_44[12] = "txnUUID";
    d = tmp55_44;
    String[] tmp79_76 = new String[14];
    String[] tmp80_79 = tmp79_76;
    String[] tmp80_79 = tmp79_76;
    tmp80_79[0] = "statements._id";
    tmp80_79[1] = "statements.wSmsId";
    String[] tmp89_80 = tmp80_79;
    String[] tmp89_80 = tmp80_79;
    tmp89_80[2] = "statements.amount";
    tmp89_80[3] = "statements.minDueAmount";
    String[] tmp98_89 = tmp89_80;
    String[] tmp98_89 = tmp89_80;
    tmp98_89[4] = "statements.dueDate";
    tmp98_89[5] = "statements.type";
    String[] tmp107_98 = tmp98_89;
    String[] tmp107_98 = tmp98_89;
    tmp107_98[6] = "statements.status";
    tmp107_98[7] = "statements.accountId";
    String[] tmp118_107 = tmp107_98;
    String[] tmp118_107 = tmp107_98;
    tmp118_107[8] = "statements.UUID";
    tmp118_107[9] = "statements.modifyCount";
    String[] tmp129_118 = tmp118_107;
    String[] tmp129_118 = tmp118_107;
    tmp129_118[10] = "statements.paymentDate";
    tmp129_118[11] = "statements.billType";
    tmp129_118[12] = "statements.txnUUID";
    String[] tmp145_129 = tmp129_118;
    tmp145_129[13] = "pan";
  }
  
  private e(b paramb)
  {
    c = paramb;
  }
  
  private int a(long paramLong)
  {
    com.daamitt.prime.sdk.a.e.a();
    String str = String.valueOf(paramLong);
    str = "update statements set status = status | 2 where _id = ".concat(str);
    a.execSQL(str);
    return 1;
  }
  
  private static l a(Cursor paramCursor)
  {
    Object localObject1 = paramCursor;
    int i = paramCursor.getColumnIndexOrThrow("_id");
    i = paramCursor.getInt(i);
    int j = paramCursor.getColumnIndexOrThrow("wSmsId");
    j = paramCursor.getInt(j);
    int k = paramCursor.getColumnIndexOrThrow("amount");
    double d1 = paramCursor.getDouble(k);
    k = paramCursor.getColumnIndexOrThrow("minDueAmount");
    double d2 = paramCursor.getDouble(k);
    Object localObject2 = Calendar.getInstance();
    int m = paramCursor.getColumnIndexOrThrow("dueDate");
    long l1 = paramCursor.getLong(m);
    ((Calendar)localObject2).setTimeInMillis(l1);
    Date localDate = ((Calendar)localObject2).getTime();
    localObject2 = Calendar.getInstance();
    m = paramCursor.getColumnIndexOrThrow("paymentDate");
    l1 = paramCursor.getLong(m);
    ((Calendar)localObject2).setTimeInMillis(l1);
    localObject2 = ((Calendar)localObject2).getTime();
    m = paramCursor.getColumnIndexOrThrow("type");
    int n = paramCursor.getInt(m);
    m = paramCursor.getColumnIndexOrThrow("accountId");
    m = paramCursor.getInt(m);
    int i1 = paramCursor.getColumnIndexOrThrow("status");
    i1 = paramCursor.getInt(i1);
    int i2 = paramCursor.getColumnIndex("UUID");
    String str = paramCursor.getString(i2);
    int i3 = paramCursor.getColumnIndexOrThrow("billType");
    i3 = paramCursor.getInt(i3);
    int i4 = paramCursor.getColumnIndexOrThrow("txnUUID");
    localObject1 = paramCursor.getString(i4);
    l locall = new com/daamitt/prime/sdk/a/l;
    locall.<init>(null, null, null);
    paramCursor = (Cursor)localObject1;
    long l2 = i;
    o = l2;
    l2 = j;
    C = l2;
    w = m;
    j = i1;
    u = 7;
    locall.a(null, "Bills");
    locall.a(null, d1, localDate, n);
    h = ((Date)localObject2);
    k = str;
    l = i3;
    e = d2;
    m = ((String)localObject1);
    return locall;
  }
  
  public static e a(b paramb)
  {
    e locale = f;
    if (locale == null)
    {
      locale = new com/daamitt/prime/sdk/b/e;
      locale.<init>(paramb);
      f = locale;
      paramb = paramb.getWritableDatabase();
      a = paramb;
    }
    return f;
  }
  
  public static String a(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt != null)
    {
      int i = paramArrayOfInt.length;
      if (i > 0)
      {
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        int j = paramArrayOfInt.length * 2;
        int k = 1;
        j -= k;
        localStringBuilder.<init>(j);
        Object localObject = null;
        j = paramArrayOfInt[0];
        localStringBuilder.append(j);
        for (;;)
        {
          j = paramArrayOfInt.length;
          if (k >= j) {
            break;
          }
          localObject = new java/lang/StringBuilder;
          String str = ",";
          ((StringBuilder)localObject).<init>(str);
          int m = paramArrayOfInt[k];
          ((StringBuilder)localObject).append(m);
          localObject = ((StringBuilder)localObject).toString();
          localStringBuilder.append((String)localObject);
          k += 1;
        }
        return localStringBuilder.toString();
      }
    }
    return null;
  }
  
  public static void a(SQLiteDatabase paramSQLiteDatabase)
  {
    com.daamitt.prime.sdk.a.e.b();
    paramSQLiteDatabase.execSQL("create table if not exists statements(_id integer primary key autoincrement, wSmsId integer not null, amount real not null, minDueAmount real default -1, dueDate integer not null, type integer not null, status integer default 0, accountId integer not null,UUID text,modifyCount integer default 1, paymentDate integer, billType integer,txnUUID text);");
    paramSQLiteDatabase.execSQL("create trigger if not exists StmtTriggerModifiedFlag After update on statements for each row  Begin  Update statements Set modifyCount = modifyCount + 1  Where _id =  New._id;  End; ");
  }
  
  private String b(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt)
  {
    StringBuilder localStringBuilder1 = new java/lang/StringBuilder;
    String str = "sms.date between ";
    localStringBuilder1.<init>(str);
    long l1 = paramCalendar1.getTime().getTime();
    localStringBuilder1.append(l1);
    localStringBuilder1.append(" AND ");
    long l2 = paramCalendar2.getTime().getTime();
    localStringBuilder1.append(l2);
    paramCalendar1 = localStringBuilder1.toString();
    if (paramInt != 0)
    {
      int i = 1;
      paramCalendar2 = new int[i];
      localStringBuilder1 = null;
      paramCalendar2[0] = paramInt;
      StringBuilder localStringBuilder2 = new java/lang/StringBuilder;
      localStringBuilder2.<init>();
      localStringBuilder2.append(paramCalendar1);
      localStringBuilder2.append(" AND statements.accountId IN (");
      paramCalendar1 = c.c(paramCalendar2);
      localStringBuilder2.append(paramCalendar1);
      localStringBuilder2.append(")");
      paramCalendar1 = localStringBuilder2.toString();
    }
    return paramCalendar1;
  }
  
  public static void b(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.beginTransaction();
    String str = "drop table if exists statements";
    try
    {
      paramSQLiteDatabase.execSQL(str);
      str = "drop trigger if exists StmtTriggerModifiedFlag";
      paramSQLiteDatabase.execSQL(str);
      a(paramSQLiteDatabase);
      paramSQLiteDatabase.setTransactionSuccessful();
      return;
    }
    finally
    {
      paramSQLiteDatabase.endTransaction();
    }
  }
  
  public final int a(l paraml, ContentValues paramContentValues)
  {
    long l1 = o;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (!bool)
    {
      int i = w;
      if (i >= 0)
      {
        SQLiteDatabase localSQLiteDatabase = a;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>("_id = ");
        long l3 = o;
        localStringBuilder.append(l3);
        paraml = localStringBuilder.toString();
        return localSQLiteDatabase.update("statements", paramContentValues, paraml, null);
      }
    }
    com.daamitt.prime.sdk.a.e.a();
    return -1;
  }
  
  public final l a(ArrayList paramArrayList, l paraml)
  {
    e locale = this;
    l locall = paraml;
    com.daamitt.prime.sdk.a.e.a();
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("statements.accountId = ");
    int i = w;
    ((StringBuilder)localObject2).append(i);
    localObject2 = ((StringBuilder)localObject2).toString();
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    Object localObject3 = paramArrayList.iterator();
    int j = 0;
    Object localObject4 = null;
    int k = 0;
    b localb = null;
    int i8;
    Object localObject7;
    int i3;
    label289:
    Object localObject9;
    int i5;
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject3).hasNext();
      boolean bool3 = true;
      int i4 = 2;
      if (!bool2) {
        break;
      }
      Object localObject5 = (c.a)((Iterator)localObject3).next();
      localObject6 = a;
      int i6 = ((String)localObject6).hashCode();
      int i7 = -1413853096;
      i8 = 3;
      if (i6 != i7)
      {
        i7 = -102973965;
        if (i6 != i7)
        {
          i7 = 110749;
          if (i6 != i7)
          {
            i7 = 3076014;
            if (i6 == i7)
            {
              localObject7 = "date";
              bool6 = ((String)localObject6).equals(localObject7);
              if (bool6) {
                break label289;
              }
            }
          }
          else
          {
            localObject8 = "pan";
            bool3 = ((String)localObject6).equals(localObject8);
            if (bool3)
            {
              int i1 = 2;
              break label289;
            }
          }
        }
        else
        {
          localObject8 = "sms_time";
          boolean bool4 = ((String)localObject6).equals(localObject8);
          if (bool4)
          {
            int i2 = 3;
            break label289;
          }
        }
      }
      else
      {
        localObject8 = "amount";
        boolean bool5 = ((String)localObject6).equals(localObject8);
        if (bool5)
        {
          bool5 = false;
          localObject8 = null;
          break label289;
        }
      }
      i3 = -1;
      switch (i3)
      {
      default: 
        break;
      case 3: 
        localObject6 = "date";
        break;
      case 2: 
        localObject6 = "pan";
        break;
      case 1: 
        localObject6 = "dueDate";
        break;
      case 0: 
        localObject6 = "amount";
      }
      Object localObject8 = b;
      localObject9 = locall.b((String)localObject8);
      if (localObject9 != null) {
        localObject8 = localObject9;
      }
      localObject9 = c;
      com.daamitt.prime.sdk.a.e.a();
      localObject7 = "dueDate";
      boolean bool6 = TextUtils.equals((CharSequence)localObject6, (CharSequence)localObject7);
      if (!bool6)
      {
        localObject7 = "date";
        bool6 = TextUtils.equals((CharSequence)localObject6, (CharSequence)localObject7);
        if (!bool6)
        {
          localObject7 = "deleted";
          bool6 = TextUtils.equals((CharSequence)localObject6, (CharSequence)localObject7);
          if (bool6)
          {
            k = d;
            if (k == i8)
            {
              bool2 = TextUtils.isEmpty((CharSequence)localObject1);
              if (!bool2)
              {
                localObject5 = " AND ";
                ((StringBuilder)localObject1).append((String)localObject5);
              }
              localObject5 = "statements.status & 2 = 0";
              ((StringBuilder)localObject1).append((String)localObject5);
              continue;
            }
            if (k != i4) {
              continue;
            }
            bool2 = TextUtils.isEmpty((CharSequence)localObject1);
            if (!bool2)
            {
              localObject5 = " AND ";
              ((StringBuilder)localObject1).append((String)localObject5);
            }
            localObject5 = "statements.status & 2 != 0";
            ((StringBuilder)localObject1).append((String)localObject5);
            continue;
          }
          localObject7 = "pattern_UID";
          bool6 = TextUtils.equals((CharSequence)localObject6, (CharSequence)localObject7);
          if (bool6)
          {
            int m = d;
            if (m == i8)
            {
              n = TextUtils.isEmpty((CharSequence)localObject1);
              if (n == 0)
              {
                localObject5 = " AND ";
                ((StringBuilder)localObject1).append((String)localObject5);
              }
              ((StringBuilder)localObject1).append("patternUID !=?");
              l1 = F.j;
              localObject5 = String.valueOf(l1);
              ((ArrayList)localObject2).add(localObject5);
              continue;
            }
            if (n != i4) {
              continue;
            }
            n = TextUtils.isEmpty((CharSequence)localObject1);
            if (n == 0)
            {
              localObject5 = " AND ";
              ((StringBuilder)localObject1).append((String)localObject5);
            }
            ((StringBuilder)localObject1).append("patternUID =?");
            l1 = F.j;
            localObject5 = String.valueOf(l1);
            ((ArrayList)localObject2).add(localObject5);
            continue;
          }
          localObject5 = "contains";
          int n = TextUtils.equals((CharSequence)localObject9, (CharSequence)localObject5);
          if (n != 0)
          {
            n = TextUtils.isEmpty((CharSequence)localObject1);
            if (n == 0)
            {
              localObject5 = " AND ";
              ((StringBuilder)localObject1).append((String)localObject5);
            }
            ((StringBuilder)localObject1).append((String)localObject6);
            ((StringBuilder)localObject1).append(" LIKE '%");
            ((StringBuilder)localObject1).append((String)localObject8);
            localObject5 = "%'";
            ((StringBuilder)localObject1).append((String)localObject5);
            continue;
          }
          localObject5 = "exact";
          n = TextUtils.equals((CharSequence)localObject9, (CharSequence)localObject5);
          if (n == 0) {
            continue;
          }
          n = TextUtils.isEmpty((CharSequence)localObject1);
          if (n == 0)
          {
            localObject5 = " AND ";
            ((StringBuilder)localObject1).append((String)localObject5);
          }
          ((StringBuilder)localObject1).append((String)localObject6);
          localObject5 = " =? ";
          ((StringBuilder)localObject1).append((String)localObject5);
          ((ArrayList)localObject2).add(localObject8);
          continue;
        }
      }
      i5 = TextUtils.isEmpty((CharSequence)localObject1);
      if (i5 == 0)
      {
        str1 = " AND ";
        ((StringBuilder)localObject1).append(str1);
      }
      long l2 = e;
      long l3 = Long.valueOf((String)localObject8).longValue() - l2;
      long l1 = Long.valueOf((String)localObject8).longValue() + l2;
      ((StringBuilder)localObject1).append((String)localObject6);
      ((StringBuilder)localObject1).append(" >=? AND ");
      ((StringBuilder)localObject1).append((String)localObject6);
      ((StringBuilder)localObject1).append(" <=?");
      String str1 = String.valueOf(l3);
      ((ArrayList)localObject2).add(str1);
      localObject5 = String.valueOf(l1);
      ((ArrayList)localObject2).add(localObject5);
    }
    Object localObject6 = new android/database/sqlite/SQLiteQueryBuilder;
    ((SQLiteQueryBuilder)localObject6).<init>();
    ((SQLiteQueryBuilder)localObject6).setTables("statements JOIN accounts ON statements.accountId = accounts._id JOIN sms ON sms._id = statements.wSmsId");
    locall = null;
    String str2;
    Object localObject10;
    String str3;
    try
    {
      localObject9 = a;
      localObject7 = e;
      str2 = ((StringBuilder)localObject1).toString();
      i9 = ((ArrayList)localObject2).size();
      localObject1 = new String[i9];
      localObject1 = ((ArrayList)localObject2).toArray((Object[])localObject1);
      localObject10 = localObject1;
      localObject10 = (String[])localObject1;
      str3 = "statements._id DESC";
      localObject1 = ((SQLiteQueryBuilder)localObject6).query((SQLiteDatabase)localObject9, (String[])localObject7, str2, (String[])localObject10, null, null, str3);
    }
    catch (RuntimeException localRuntimeException)
    {
      com.daamitt.prime.sdk.a.e.b();
      int i9 = 0;
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      int i10 = ((Cursor)localObject1).getCount();
      if (i10 > 0)
      {
        ((Cursor)localObject1).moveToFirst();
        boolean bool7 = ((Cursor)localObject1).isAfterLast();
        if (!bool7)
        {
          localObject2 = a((Cursor)localObject1);
          i = ((Cursor)localObject1).getColumnIndexOrThrow("pan");
          localObject3 = ((Cursor)localObject1).getString(i);
          b = ((String)localObject3);
          i = j & i5;
          if ((i != 0) || (k != i3)) {
            break label1424;
          }
          long l4 = C;
          localb = c;
          localObject3 = localb.a(l4);
          for (;;)
          {
            localObject4 = I;
            if (localObject4 == null) {
              break;
            }
            localObject2 = c;
            localObject3 = I;
            localObject3 = ((b)localObject2).a((String)localObject3);
            long l5 = o;
            localObject6 = a;
            localObject9 = "statements";
            localObject7 = d;
            localObject4 = String.valueOf(l5);
            str2 = "wSmsId = ".concat((String)localObject4);
            i8 = 0;
            localObject10 = null;
            str3 = null;
            localObject2 = ((SQLiteDatabase)localObject6).query((String)localObject9, (String[])localObject7, str2, null, null, null, null);
            if (localObject2 != null)
            {
              j = ((Cursor)localObject2).getCount();
              if (j > 0)
              {
                ((Cursor)localObject2).moveToFirst();
                bool1 = ((Cursor)localObject2).isAfterLast();
                if (!bool1)
                {
                  localObject4 = a((Cursor)localObject2);
                  break label1377;
                }
              }
            }
            boolean bool1 = false;
            localObject4 = null;
            label1377:
            if (localObject2 != null) {
              ((Cursor)localObject2).close();
            }
            i11 = j & i5;
            if (i11 != 0)
            {
              localObject2 = localObject4;
              break;
            }
            localObject2 = localObject4;
          }
        }
      }
    }
    int i11 = 0;
    localObject2 = null;
    label1424:
    if (localObject1 != null) {
      ((Cursor)localObject1).close();
    }
    return (l)localObject2;
  }
  
  public final void a(a parama1, a parama2)
  {
    Object localObject1 = a;
    Object localObject2 = "statements";
    Object localObject3 = d;
    Object localObject4 = new java/lang/StringBuilder;
    ((StringBuilder)localObject4).<init>("accountId =");
    int i = a;
    ((StringBuilder)localObject4).append(i);
    localObject4 = ((StringBuilder)localObject4).toString();
    Object localObject5 = null;
    int j = 0;
    boolean bool1 = false;
    parama1 = ((SQLiteDatabase)localObject1).query((String)localObject2, (String[])localObject3, (String)localObject4, null, null, null, null);
    if (parama1 != null)
    {
      int k = parama1.getCount();
      if (k > 0)
      {
        parama1.moveToFirst();
        for (;;)
        {
          boolean bool2 = parama1.isAfterLast();
          if (bool2) {
            break;
          }
          localObject1 = a(parama1);
          localObject2 = a;
          localObject3 = "statements";
          localObject4 = d;
          localObject5 = new java/lang/StringBuilder;
          ((StringBuilder)localObject5).<init>("accountId =");
          j = a;
          ((StringBuilder)localObject5).append(j);
          ((StringBuilder)localObject5).append(" AND dueDate =");
          long l1 = g.getTime();
          ((StringBuilder)localObject5).append(l1);
          localObject5 = ((StringBuilder)localObject5).toString();
          j = 0;
          bool1 = false;
          localObject2 = ((SQLiteDatabase)localObject2).query((String)localObject3, (String[])localObject4, (String)localObject5, null, null, null, null);
          if (localObject2 != null)
          {
            int m = ((Cursor)localObject2).getCount();
            if (m > 0)
            {
              ((Cursor)localObject2).moveToFirst();
              for (;;)
              {
                boolean bool3 = ((Cursor)localObject2).isAfterLast();
                if (bool3) {
                  break;
                }
                localObject3 = a((Cursor)localObject2);
                double d1 = d;
                double d2 = d;
                d1 = Math.abs(d1 - d2);
                d2 = 1.0D;
                bool1 = d1 < d2;
                if (bool1)
                {
                  long l2 = o;
                  a(l2);
                  break;
                }
                ((Cursor)localObject2).moveToNext();
              }
              ((Cursor)localObject2).close();
            }
          }
          parama1.moveToNext();
        }
        parama1.close();
      }
    }
    if (parama1 != null) {
      parama1.close();
    }
  }
  
  public final boolean a(l paraml, a parama)
  {
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("accountId IN (");
    Object localObject2 = c;
    int i = w;
    long l1 = i;
    localObject2 = ((b)localObject2).c(l1);
    ((StringBuilder)localObject1).append((String)localObject2);
    ((StringBuilder)localObject1).append(") AND dueDate='");
    long l2 = g.getTime();
    ((StringBuilder)localObject1).append(l2);
    localObject2 = "' AND statements.status & 2 = 0 ";
    ((StringBuilder)localObject1).append((String)localObject2);
    Object localObject3 = ((StringBuilder)localObject1).toString();
    SQLiteDatabase localSQLiteDatabase1 = a;
    String[] arrayOfString1 = d;
    int j = 0;
    Object localObject4 = null;
    boolean bool2 = false;
    SQLiteDatabase localSQLiteDatabase2 = null;
    String[] arrayOfString2 = null;
    boolean bool3 = false;
    localObject1 = localSQLiteDatabase1.query("statements", arrayOfString1, (String)localObject3, null, null, null, null);
    l2 = 4607182418800017408L;
    double d1 = 1.0D;
    boolean bool4 = true;
    double d2;
    double d3;
    if (localObject1 != null)
    {
      int k = ((Cursor)localObject1).getCount();
      if (k > 0)
      {
        ((Cursor)localObject1).moveToFirst();
        for (;;)
        {
          boolean bool5 = ((Cursor)localObject1).isAfterLast();
          if (bool5) {
            break;
          }
          localObject3 = a((Cursor)localObject1);
          d2 = d;
          d3 = d;
          d2 = Math.abs(d2 - d3);
          bool2 = d2 < d1;
          if (bool2)
          {
            com.daamitt.prime.sdk.a.e.b();
            ((Cursor)localObject1).close();
            n = 1;
            break label263;
          }
          ((Cursor)localObject1).moveToNext();
        }
      }
    }
    if (localObject1 != null) {
      ((Cursor)localObject1).close();
    }
    int n = 0;
    localObject1 = null;
    label263:
    if (n != 0) {
      return bool4;
    }
    n = i;
    int m = 6;
    if (n == m)
    {
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("Select statements.* from statements, accounts Where statements.accountId = accounts._id AND dueDate=");
      long l3 = g.getTime();
      ((StringBuilder)localObject1).append(l3);
      ((StringBuilder)localObject1).append(" AND statements.status & 2 = 0  AND (accounts.type = 3 OR accounts.type = 6) AND accounts.name=? ");
      localObject1 = ((StringBuilder)localObject1).toString();
      com.daamitt.prime.sdk.a.e.a();
      localObject3 = a;
      localObject4 = new String[bool4];
      parama = d;
      localObject4[0] = parama;
      parama = ((SQLiteDatabase)localObject3).rawQuery((String)localObject1, (String[])localObject4);
      if (parama != null)
      {
        n = parama.getCount();
        if (n > 0)
        {
          parama.moveToFirst();
          for (;;)
          {
            boolean bool6 = parama.isAfterLast();
            if (bool6) {
              break;
            }
            localObject1 = a(parama);
            d2 = d;
            d3 = d;
            d2 = Math.abs(d2 - d3);
            bool6 = d2 < d1;
            if (bool6)
            {
              com.daamitt.prime.sdk.a.e.b();
              parama.close();
              return bool4;
            }
            parama.moveToNext();
          }
          parama.close();
        }
      }
      if (parama != null) {
        parama.close();
      }
    }
    else
    {
      localObject1 = "accType";
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>("Select statements.*,accounts.type as ");
      ((StringBuilder)localObject4).append((String)localObject1);
      ((StringBuilder)localObject4).append(" from statements, accounts Where statements.accountId = accounts._id AND dueDate=");
      long l4 = g.getTime();
      ((StringBuilder)localObject4).append(l4);
      ((StringBuilder)localObject4).append(" AND statements.status & 2 = 0  AND (accounts.type = 3 OR accounts.type = 6) AND accounts.name=? ");
      localObject4 = ((StringBuilder)localObject4).toString();
      com.daamitt.prime.sdk.a.e.a();
      localSQLiteDatabase2 = a;
      arrayOfString2 = new String[bool4];
      parama = d;
      arrayOfString2[0] = parama;
      parama = localSQLiteDatabase2.rawQuery((String)localObject4, arrayOfString2);
      if (parama != null)
      {
        j = parama.getCount();
        if (j > 0)
        {
          parama.moveToFirst();
          for (;;)
          {
            boolean bool1 = parama.isAfterLast();
            if (bool1) {
              break;
            }
            localObject4 = a(parama);
            d3 = d;
            double d4 = d;
            d3 = Math.abs(d3 - d4);
            bool3 = d3 < d1;
            if (bool3)
            {
              int i1 = parama.getColumnIndexOrThrow((String)localObject1);
              i1 = parama.getInt(i1);
              if (i1 == m)
              {
                com.daamitt.prime.sdk.a.e.b();
                long l5 = o;
                a(l5);
                parama.close();
                return false;
              }
              com.daamitt.prime.sdk.a.e.b();
              parama.close();
              return bool4;
            }
            parama.moveToNext();
          }
          parama.close();
        }
      }
      if (parama != null) {
        parama.close();
      }
    }
    return false;
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt)
  {
    int i = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    com.daamitt.prime.sdk.a.e.a();
    boolean bool1 = paramCalendar1.before(paramCalendar2);
    if (bool1)
    {
      String[] arrayOfString = new String[i];
      String str1 = b(paramCalendar1, paramCalendar2, paramInt);
      paramCalendar2 = Calendar.getInstance().getTimeZone();
      long l1 = paramCalendar2.getRawOffset();
      int j = paramCalendar2.getDSTSavings();
      long l2 = j;
      l1 += l2;
      l2 = 3600000L;
      long l3 = l1 / l2;
      l1 %= l2;
      l2 = 60000L;
      l1 /= l2;
      paramCalendar2 = new java/lang/StringBuilder;
      paramCalendar2.<init>("strftime('%j',date(date/1000,'unixepoch','");
      paramCalendar2.append(l3);
      paramCalendar2.append(" hours','");
      paramCalendar2.append(l1);
      paramCalendar2.append(" minutes')) as Day");
      paramCalendar2 = paramCalendar2.toString();
      String str2 = "Day";
      Object localObject1 = new String[2];
      paramInt = 0;
      localObject1[0] = paramCalendar2;
      paramCalendar2 = g;
      int k = 1;
      localObject1[k] = paramCalendar2;
      Object localObject2 = a;
      String str3 = h;
      paramCalendar2 = ((SQLiteDatabase)localObject2).query(str3, (String[])localObject1, str1, null, str2, null, null, null);
      if (paramCalendar2 != null)
      {
        int m = paramCalendar2.getCount();
        if (m > 0)
        {
          paramCalendar2.moveToFirst();
          m = 6;
          int i1 = paramCalendar1.get(m);
          int i2 = paramCalendar1.getActualMaximum(m);
          for (;;)
          {
            boolean bool2 = paramCalendar2.isAfterLast();
            if (bool2) {
              break;
            }
            int n = paramCalendar2.getColumnIndexOrThrow("Day");
            localObject2 = paramCalendar2.getString(n);
            int i3 = paramCalendar2.getColumnIndexOrThrow("Amount");
            localObject1 = paramCalendar2.getString(i3);
            int i4 = -1;
            try
            {
              n = Integer.parseInt((String)localObject2);
            }
            catch (NumberFormatException localNumberFormatException)
            {
              localNumberFormatException.printStackTrace();
              n = -1;
            }
            if ((n != i4) && (localObject1 != null))
            {
              localObject1 = Double.valueOf((String)localObject1);
              double d1 = ((Double)localObject1).doubleValue();
              i3 = (int)d1;
              if (n >= i1)
              {
                n -= i1;
                n = i - n - k;
              }
              else
              {
                i4 = i2 - i1 + n;
                n = i - i4 - k;
              }
              if (n >= 0)
              {
                localObject1 = String.valueOf(i3);
                arrayOfString[n] = localObject1;
              }
            }
            paramCalendar2.moveToNext();
          }
        }
      }
      if (paramCalendar2 != null) {
        paramCalendar2.close();
      }
      while (paramInt < i)
      {
        paramCalendar1 = arrayOfString[paramInt];
        boolean bool3 = TextUtils.isEmpty(paramCalendar1);
        if (bool3)
        {
          paramCalendar1 = "";
          arrayOfString[paramInt] = paramCalendar1;
        }
        paramInt += 1;
      }
      return arrayOfString;
    }
    return null;
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt, int[] paramArrayOfInt1, int[] paramArrayOfInt2)
  {
    Object localObject1 = paramCalendar1;
    int i = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    com.daamitt.prime.sdk.a.e.a();
    boolean bool1 = paramCalendar1.before(paramCalendar2);
    if (bool1)
    {
      String[] arrayOfString1 = new String[i];
      localObject2 = b(paramCalendar1, paramCalendar2, paramInt);
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append((String)localObject2);
      localStringBuilder.append(" AND statements.type IN (");
      localObject2 = a(paramArrayOfInt1);
      localStringBuilder.append((String)localObject2);
      localStringBuilder.append(")");
      localObject2 = localStringBuilder.toString();
      localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append((String)localObject2);
      localStringBuilder.append(" AND sms.patternUID IN (");
      localObject2 = a(paramArrayOfInt2);
      localStringBuilder.append((String)localObject2);
      localStringBuilder.append(")");
      Object localObject3 = localStringBuilder.toString();
      localObject2 = Calendar.getInstance().getTimeZone();
      long l1 = ((TimeZone)localObject2).getRawOffset();
      int j = ((TimeZone)localObject2).getDSTSavings();
      long l2 = j;
      l1 += l2;
      l2 = 3600000L;
      long l3 = l1 / l2;
      l1 %= l2;
      l2 = 60000L;
      l1 /= l2;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("strftime('%j',date(date/1000,'unixepoch','");
      ((StringBuilder)localObject2).append(l3);
      ((StringBuilder)localObject2).append(" hours','");
      ((StringBuilder)localObject2).append(l1);
      ((StringBuilder)localObject2).append(" minutes')) as Day");
      localObject2 = ((StringBuilder)localObject2).toString();
      String str1 = "Day";
      String str2 = " MAX(amount)";
      String[] arrayOfString2 = new String[2];
      int k = 0;
      localStringBuilder = null;
      arrayOfString2[0] = localObject2;
      int m = 1;
      arrayOfString2[m] = "amount";
      localObject2 = this;
      Object localObject4 = a;
      String str3 = h;
      int n = 0;
      localObject4 = ((SQLiteDatabase)localObject4).query(str3, arrayOfString2, (String)localObject3, null, str1, str2, null, null);
      if (localObject4 != null)
      {
        int i1 = ((Cursor)localObject4).getCount();
        if (i1 > 0)
        {
          ((Cursor)localObject4).moveToFirst();
          i1 = 6;
          int i3 = paramCalendar1.get(i1);
          int i4 = paramCalendar1.getActualMaximum(i1);
          for (;;)
          {
            boolean bool2 = ((Cursor)localObject4).isAfterLast();
            if (bool2) {
              break;
            }
            int i2 = ((Cursor)localObject4).getColumnIndexOrThrow("Day");
            str3 = ((Cursor)localObject4).getString(i2);
            int i5 = ((Cursor)localObject4).getColumnIndexOrThrow("amount");
            localObject3 = ((Cursor)localObject4).getString(i5);
            n = -1;
            try
            {
              i2 = Integer.parseInt(str3);
            }
            catch (NumberFormatException localNumberFormatException)
            {
              i2 = -1;
            }
            if ((i2 != n) && (localObject3 != null))
            {
              localObject3 = Double.valueOf((String)localObject3);
              double d1 = ((Double)localObject3).doubleValue();
              i5 = (int)d1;
              if (i2 >= i3)
              {
                i2 -= i3;
                i2 = i - i2 - m;
              }
              else
              {
                n = i4 - i3 + i2;
                i2 = i - n - m;
              }
              if (i2 >= 0)
              {
                localObject3 = String.valueOf(i5);
                arrayOfString1[i2] = localObject3;
              }
            }
            ((Cursor)localObject4).moveToNext();
          }
        }
      }
      if (localObject4 != null) {
        ((Cursor)localObject4).close();
      }
      while (k < i)
      {
        localObject1 = arrayOfString1[k];
        boolean bool3 = TextUtils.isEmpty((CharSequence)localObject1);
        if (bool3)
        {
          localObject1 = "";
          arrayOfString1[k] = localObject1;
        }
        k += 1;
      }
      return arrayOfString1;
    }
    Object localObject2 = this;
    return null;
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */