package com.daamitt.prime.sdk.b;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.text.TextUtils;
import com.daamitt.prime.sdk.a.c.a;
import com.daamitt.prime.sdk.a.d;
import com.daamitt.prime.sdk.a.e;
import com.daamitt.prime.sdk.a.h;
import com.daamitt.prime.sdk.a.i.a;
import com.daamitt.prime.sdk.a.k;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.TimeZone;

public class c
{
  public static final String a = "c";
  private static String[] d;
  private static String[] e = tmp151_135;
  private static c f;
  private static String g = "COUNT(*) as eventCount ";
  private static String h = "events JOIN sms ON events.wSmsId = sms._id";
  public SQLiteDatabase b = null;
  private b c;
  
  static
  {
    String[] tmp5_2 = new String[14];
    String[] tmp6_5 = tmp5_2;
    String[] tmp6_5 = tmp5_2;
    tmp6_5[0] = "_id";
    tmp6_5[1] = "wSmsId";
    String[] tmp15_6 = tmp6_5;
    String[] tmp15_6 = tmp6_5;
    tmp15_6[2] = "name";
    tmp15_6[3] = "pnr";
    String[] tmp24_15 = tmp15_6;
    String[] tmp24_15 = tmp15_6;
    tmp24_15[4] = "dueDate";
    tmp24_15[5] = "type";
    String[] tmp33_24 = tmp24_15;
    String[] tmp33_24 = tmp24_15;
    tmp33_24[6] = "flags";
    tmp33_24[7] = "location";
    String[] tmp44_33 = tmp33_24;
    String[] tmp44_33 = tmp33_24;
    tmp44_33[8] = "info";
    tmp44_33[9] = "contact";
    String[] tmp55_44 = tmp44_33;
    String[] tmp55_44 = tmp44_33;
    tmp55_44[10] = "UUID";
    tmp55_44[11] = "modifyCount";
    tmp55_44[12] = "amount";
    String[] tmp71_55 = tmp55_44;
    tmp71_55[13] = "reminderTimeSpan";
    d = tmp71_55;
    String[] tmp85_82 = new String[14];
    String[] tmp86_85 = tmp85_82;
    String[] tmp86_85 = tmp85_82;
    tmp86_85[0] = "events._id";
    tmp86_85[1] = "events.wSmsId";
    String[] tmp95_86 = tmp86_85;
    String[] tmp95_86 = tmp86_85;
    tmp95_86[2] = "events.name";
    tmp95_86[3] = "events.pnr";
    String[] tmp104_95 = tmp95_86;
    String[] tmp104_95 = tmp95_86;
    tmp104_95[4] = "events.dueDate";
    tmp104_95[5] = "events.type";
    String[] tmp113_104 = tmp104_95;
    String[] tmp113_104 = tmp104_95;
    tmp113_104[6] = "events.flags";
    tmp113_104[7] = "events.location";
    String[] tmp124_113 = tmp113_104;
    String[] tmp124_113 = tmp113_104;
    tmp124_113[8] = "events.info";
    tmp124_113[9] = "events.contact";
    String[] tmp135_124 = tmp124_113;
    String[] tmp135_124 = tmp124_113;
    tmp135_124[10] = "events.UUID";
    tmp135_124[11] = "events.modifyCount";
    tmp135_124[12] = "events.amount";
    String[] tmp151_135 = tmp135_124;
    tmp151_135[13] = "events.reminderTimeSpan";
  }
  
  private c(b paramb)
  {
    c = paramb;
  }
  
  private static d a(Cursor paramCursor)
  {
    int i = paramCursor.getColumnIndexOrThrow("_id");
    i = paramCursor.getInt(i);
    int j = paramCursor.getColumnIndexOrThrow("wSmsId");
    j = paramCursor.getInt(j);
    int k = paramCursor.getColumnIndexOrThrow("name");
    String str1 = paramCursor.getString(k);
    int m = paramCursor.getColumnIndexOrThrow("pnr");
    String str2 = paramCursor.getString(m);
    int n = paramCursor.getColumnIndexOrThrow("location");
    String str3 = paramCursor.getString(n);
    int i1 = paramCursor.getColumnIndexOrThrow("info");
    String str4 = paramCursor.getString(i1);
    int i2 = paramCursor.getColumnIndexOrThrow("flags");
    i2 = paramCursor.getInt(i2);
    int i3 = paramCursor.getColumnIndexOrThrow("amount");
    double d1 = paramCursor.getDouble(i3);
    int i4 = paramCursor.getColumnIndexOrThrow("contact");
    String str5 = paramCursor.getString(i4);
    int i5 = paramCursor.getColumnIndexOrThrow("reminderTimeSpan");
    long l1 = paramCursor.getLong(i5);
    int i6 = paramCursor.getColumnIndexOrThrow("UUID");
    String str6 = paramCursor.getString(i6);
    Calendar localCalendar = Calendar.getInstance();
    int i7 = paramCursor.getColumnIndexOrThrow("dueDate");
    long l2 = l1;
    l1 = paramCursor.getLong(i7);
    localCalendar.setTimeInMillis(l1);
    Date localDate = localCalendar.getTime();
    int i8 = paramCursor.getColumnIndexOrThrow("type");
    int i9 = paramCursor.getInt(i8);
    d locald = new com/daamitt/prime/sdk/a/d;
    locald.<init>(null, null, null);
    long l3 = i;
    o = l3;
    long l4 = j;
    C = l4;
    u = 5;
    locald.a(null, "Events");
    locald.a(str1, str2, localDate, i9);
    f = str3;
    g = str4;
    h = i2;
    j = str5;
    i = d1;
    m = l2;
    n = str6;
    return locald;
  }
  
  public static c a(b paramb)
  {
    c localc = f;
    if (localc == null)
    {
      localc = new com/daamitt/prime/sdk/b/c;
      localc.<init>(paramb);
      f = localc;
      paramb = paramb.getWritableDatabase();
      b = paramb;
    }
    return f;
  }
  
  public static void a(SQLiteDatabase paramSQLiteDatabase)
  {
    e.b();
    paramSQLiteDatabase.execSQL("create table if not exists events(_id integer primary key autoincrement, wSmsId integer not null, name text not null, pnr text not null, dueDate integer not null, type integer not null, flags integer default 0,location text, info text, contact text,UUID text,modifyCount integer default 1,amount real default 0,reminderTimeSpan integer default 1800000);");
    paramSQLiteDatabase.execSQL("create trigger if not exists EventsTriggerModifiedFlag After update on events for each row  Begin  Update events Set modifyCount = modifyCount + 1  Where _id =  New._id;  End; ");
  }
  
  public static void b(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.beginTransaction();
    String str = "drop table if exists events";
    try
    {
      paramSQLiteDatabase.execSQL(str);
      str = "drop trigger if exists EventsTriggerModifiedFlag";
      paramSQLiteDatabase.execSQL(str);
      a(paramSQLiteDatabase);
      paramSQLiteDatabase.setTransactionSuccessful();
      return;
    }
    finally
    {
      paramSQLiteDatabase.endTransaction();
    }
  }
  
  public final d a(ArrayList paramArrayList, d paramd)
  {
    c localc = this;
    d locald = paramd;
    e.a();
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    Object localObject3 = paramArrayList.iterator();
    int i = 0;
    Object localObject4 = null;
    int j = 0;
    b localb = null;
    int i8;
    Object localObject8;
    int i4;
    Object localObject9;
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject3).hasNext();
      boolean bool3 = true;
      if (!bool2) {
        break;
      }
      Object localObject5 = (c.a)((Iterator)localObject3).next();
      localObject6 = a;
      int i5 = -1;
      int i6 = ((String)localObject6).hashCode();
      int i7 = 2;
      i8 = 3;
      boolean bool6;
      switch (i6)
      {
      default: 
        break;
      case 984038195: 
        localObject7 = "event_info";
        bool3 = ((String)localObject6).equals(localObject7);
        if (bool3) {
          int n = 2;
        }
        break;
      case 951526432: 
        localObject7 = "contact";
        boolean bool4 = ((String)localObject6).equals(localObject7);
        if (bool4) {
          int i1 = 4;
        }
        break;
      case 943500218: 
        localObject7 = "event_location";
        boolean bool5 = ((String)localObject6).equals(localObject7);
        if (bool5) {
          int i2 = 3;
        }
        break;
      case 3373707: 
        localObject7 = "name";
        bool6 = ((String)localObject6).equals(localObject7);
        if (bool6)
        {
          bool6 = false;
          localObject7 = null;
        }
        break;
      case 3076014: 
        localObject7 = "date";
        bool6 = ((String)localObject6).equals(localObject7);
        if (bool6) {
          int i3 = 5;
        }
        break;
      case 111156: 
        localObject8 = "pnr";
        bool9 = ((String)localObject6).equals(localObject8);
        if (!bool9) {
          break;
        }
        break;
      case -102973965: 
        localObject7 = "sms_time";
        boolean bool7 = ((String)localObject6).equals(localObject7);
        if (bool7) {
          i4 = 6;
        }
        break;
      }
      i4 = -1;
      switch (i4)
      {
      default: 
        break;
      case 6: 
        localObject6 = "date";
        break;
      case 5: 
        localObject6 = "dueDate";
        break;
      case 4: 
        localObject6 = "contact";
        break;
      case 3: 
        localObject6 = "location";
        break;
      case 2: 
        localObject6 = "info";
        break;
      case 1: 
        localObject6 = "pnr";
        break;
      case 0: 
        localObject6 = "name";
      }
      Object localObject7 = b;
      localObject9 = locald.b((String)localObject7);
      if (localObject9 != null) {
        localObject7 = localObject9;
      }
      localObject9 = c;
      localObject8 = "dueDate";
      boolean bool9 = TextUtils.equals((CharSequence)localObject6, (CharSequence)localObject8);
      if (!bool9)
      {
        localObject8 = "date";
        bool9 = TextUtils.equals((CharSequence)localObject6, (CharSequence)localObject8);
        if (!bool9)
        {
          localObject8 = "deleted";
          bool9 = TextUtils.equals((CharSequence)localObject6, (CharSequence)localObject8);
          if (bool9)
          {
            j = d;
            if (j == i8)
            {
              bool2 = TextUtils.isEmpty((CharSequence)localObject1);
              if (!bool2)
              {
                localObject5 = " AND ";
                ((StringBuilder)localObject1).append((String)localObject5);
              }
              localObject5 = "flags & 4 = 0";
              ((StringBuilder)localObject1).append((String)localObject5);
              continue;
            }
            if (j != i7) {
              continue;
            }
            bool2 = TextUtils.isEmpty((CharSequence)localObject1);
            if (!bool2)
            {
              localObject5 = " AND ";
              ((StringBuilder)localObject1).append((String)localObject5);
            }
            localObject5 = "flags & 4 != 0";
            ((StringBuilder)localObject1).append((String)localObject5);
            continue;
          }
          localObject8 = "pattern_UID";
          bool9 = TextUtils.equals((CharSequence)localObject6, (CharSequence)localObject8);
          if (bool9)
          {
            int k = d;
            if (k == i8)
            {
              m = TextUtils.isEmpty((CharSequence)localObject1);
              if (m == 0)
              {
                localObject5 = " AND ";
                ((StringBuilder)localObject1).append((String)localObject5);
              }
              ((StringBuilder)localObject1).append("patternUID !=?");
              l1 = F.j;
              localObject5 = String.valueOf(l1);
              ((ArrayList)localObject2).add(localObject5);
              continue;
            }
            if (m != i7) {
              continue;
            }
            m = TextUtils.isEmpty((CharSequence)localObject1);
            if (m == 0)
            {
              localObject5 = " AND ";
              ((StringBuilder)localObject1).append((String)localObject5);
            }
            ((StringBuilder)localObject1).append("patternUID =?");
            l1 = F.j;
            localObject5 = String.valueOf(l1);
            ((ArrayList)localObject2).add(localObject5);
            continue;
          }
          localObject5 = "contains";
          int m = TextUtils.equals((CharSequence)localObject9, (CharSequence)localObject5);
          if (m != 0)
          {
            m = TextUtils.isEmpty((CharSequence)localObject1);
            if (m == 0)
            {
              localObject5 = " AND ";
              ((StringBuilder)localObject1).append((String)localObject5);
            }
            ((StringBuilder)localObject1).append((String)localObject6);
            ((StringBuilder)localObject1).append(" LIKE '%");
            ((StringBuilder)localObject1).append((String)localObject7);
            localObject5 = "%'";
            ((StringBuilder)localObject1).append((String)localObject5);
            continue;
          }
          localObject5 = "exact";
          m = TextUtils.equals((CharSequence)localObject9, (CharSequence)localObject5);
          if (m == 0) {
            continue;
          }
          m = TextUtils.isEmpty((CharSequence)localObject1);
          if (m == 0)
          {
            localObject5 = " AND ";
            ((StringBuilder)localObject1).append((String)localObject5);
          }
          ((StringBuilder)localObject1).append((String)localObject6);
          localObject5 = " =? ";
          ((StringBuilder)localObject1).append((String)localObject5);
          ((ArrayList)localObject2).add(localObject7);
          continue;
        }
      }
      boolean bool8 = TextUtils.isEmpty((CharSequence)localObject1);
      if (!bool8)
      {
        localObject9 = " AND ";
        ((StringBuilder)localObject1).append((String)localObject9);
      }
      long l2 = e;
      long l3 = Long.valueOf((String)localObject7).longValue() - l2;
      long l1 = Long.valueOf((String)localObject7).longValue() + l2;
      ((StringBuilder)localObject1).append((String)localObject6);
      localObject9 = " >=? AND ";
      ((StringBuilder)localObject1).append((String)localObject9);
      ((StringBuilder)localObject1).append((String)localObject6);
      ((StringBuilder)localObject1).append(" <=?");
      localObject6 = String.valueOf(l3);
      ((ArrayList)localObject2).add(localObject6);
      localObject5 = String.valueOf(l1);
      ((ArrayList)localObject2).add(localObject5);
    }
    Object localObject6 = new android/database/sqlite/SQLiteQueryBuilder;
    ((SQLiteQueryBuilder)localObject6).<init>();
    ((SQLiteQueryBuilder)localObject6).setTables("events JOIN sms ON sms._id = events.wSmsId");
    locald = null;
    String str1;
    Object localObject10;
    String str2;
    try
    {
      localObject9 = b;
      localObject8 = e;
      str1 = ((StringBuilder)localObject1).toString();
      i9 = ((ArrayList)localObject2).size();
      localObject1 = new String[i9];
      localObject1 = ((ArrayList)localObject2).toArray((Object[])localObject1);
      localObject10 = localObject1;
      localObject10 = (String[])localObject1;
      str2 = "events._id DESC";
      localObject1 = ((SQLiteQueryBuilder)localObject6).query((SQLiteDatabase)localObject9, (String[])localObject8, str1, (String[])localObject10, null, null, str2);
    }
    catch (RuntimeException localRuntimeException)
    {
      e.b();
      int i9 = 0;
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      int i10 = ((Cursor)localObject1).getCount();
      if (i10 > 0)
      {
        ((Cursor)localObject1).moveToFirst();
        boolean bool10 = ((Cursor)localObject1).isAfterLast();
        if (!bool10)
        {
          localObject2 = a((Cursor)localObject1);
          int i12 = h & i4;
          if ((i12 != 0) || (j != i4)) {
            break label1496;
          }
          long l4 = C;
          localb = c;
          localObject3 = localb.a(l4);
          for (;;)
          {
            localObject4 = I;
            if (localObject4 == null) {
              break;
            }
            localObject2 = c;
            localObject3 = I;
            localObject3 = ((b)localObject2).a((String)localObject3);
            long l5 = o;
            localObject6 = b;
            localObject9 = "events";
            localObject8 = d;
            localObject4 = String.valueOf(l5);
            str1 = "wSmsId = ".concat((String)localObject4);
            i8 = 0;
            localObject10 = null;
            str2 = null;
            localObject2 = ((SQLiteDatabase)localObject6).query((String)localObject9, (String[])localObject8, str1, null, null, null, null);
            if (localObject2 != null)
            {
              i = ((Cursor)localObject2).getCount();
              if (i > 0)
              {
                ((Cursor)localObject2).moveToFirst();
                bool1 = ((Cursor)localObject2).isAfterLast();
                if (!bool1)
                {
                  localObject4 = a((Cursor)localObject2);
                  break label1449;
                }
              }
            }
            boolean bool1 = false;
            localObject4 = null;
            label1449:
            if (localObject2 != null) {
              ((Cursor)localObject2).close();
            }
            i11 = h & i4;
            if (i11 != 0)
            {
              localObject2 = localObject4;
              break;
            }
            localObject2 = localObject4;
          }
        }
      }
    }
    int i11 = 0;
    localObject2 = null;
    label1496:
    if (localObject1 != null) {
      ((Cursor)localObject1).close();
    }
    return (d)localObject2;
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2, int[] paramArrayOfInt, int paramInt)
  {
    Object localObject1 = paramCalendar1;
    Object localObject2 = paramArrayOfInt;
    int i = paramInt;
    int j = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    e.c();
    boolean bool1 = paramCalendar1.before(paramCalendar2);
    int k = 0;
    Object localObject3 = null;
    if (bool1)
    {
      String[] arrayOfString = new String[j];
      int n = 2;
      int i1 = 0;
      int i2 = 1;
      int i3;
      int i4;
      if (paramArrayOfInt != null)
      {
        i3 = paramArrayOfInt.length;
        if (i3 > 0)
        {
          localObject3 = new java/lang/StringBuilder;
          i3 = paramArrayOfInt.length * 2 - i2;
          ((StringBuilder)localObject3).<init>(i3);
          i3 = paramArrayOfInt[0];
          ((StringBuilder)localObject3).append(i3);
          i3 = 1;
          for (;;)
          {
            i4 = localObject2.length;
            if (i3 >= i4) {
              break;
            }
            localObject4 = new java/lang/StringBuilder;
            localObject5 = ",";
            ((StringBuilder)localObject4).<init>((String)localObject5);
            int i5 = localObject2[i3];
            ((StringBuilder)localObject4).append(i5);
            localObject4 = ((StringBuilder)localObject4).toString();
            ((StringBuilder)localObject3).append((String)localObject4);
            i3 += 1;
          }
          localObject3 = ((StringBuilder)localObject3).toString();
        }
      }
      localObject2 = new java/lang/StringBuilder;
      Object localObject6 = "events.type in ( ";
      ((StringBuilder)localObject2).<init>((String)localObject6);
      ((StringBuilder)localObject2).append((String)localObject3);
      ((StringBuilder)localObject2).append("  )  AND sms.date between ");
      long l1 = paramCalendar1.getTime().getTime();
      ((StringBuilder)localObject2).append(l1);
      ((StringBuilder)localObject2).append(" AND ");
      l1 = paramCalendar2.getTime().getTime();
      ((StringBuilder)localObject2).append(l1);
      localObject3 = " AND sms.body NOTNULL";
      ((StringBuilder)localObject2).append((String)localObject3);
      localObject2 = ((StringBuilder)localObject2).toString();
      if (i != 0)
      {
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        ((StringBuilder)localObject3).append((String)localObject2);
        ((StringBuilder)localObject3).append(" AND flags & ");
        ((StringBuilder)localObject3).append(i);
        ((StringBuilder)localObject3).append(" != 0 ");
        localObject2 = ((StringBuilder)localObject3).toString();
      }
      Object localObject7 = new java/lang/StringBuilder;
      ((StringBuilder)localObject7).<init>();
      ((StringBuilder)localObject7).append((String)localObject2);
      ((StringBuilder)localObject7).append(" AND flags & 8 = 0 ");
      String str1 = ((StringBuilder)localObject7).toString();
      localObject2 = Calendar.getInstance().getTimeZone();
      i = ((TimeZone)localObject2).getRawOffset();
      l1 = i;
      int i6 = ((TimeZone)localObject2).getDSTSavings();
      long l2 = i6;
      l1 += l2;
      l2 = 3600000L;
      long l3 = l1 / l2;
      l1 %= l2;
      l2 = 60000L;
      l1 /= l2;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("strftime('%j',date(date/1000,'unixepoch','");
      ((StringBuilder)localObject2).append(l3);
      ((StringBuilder)localObject2).append(" hours','");
      ((StringBuilder)localObject2).append(l1);
      ((StringBuilder)localObject2).append(" minutes')) as Day");
      localObject2 = ((StringBuilder)localObject2).toString();
      String str2 = "Day";
      Object localObject5 = new String[n];
      localObject5[0] = localObject2;
      localObject2 = g;
      localObject5[i2] = localObject2;
      localObject2 = this;
      localObject6 = b;
      Object localObject4 = h;
      localObject7 = ((SQLiteDatabase)localObject6).query((String)localObject4, (String[])localObject5, str1, null, str2, null, null, null);
      if (localObject7 != null)
      {
        k = ((Cursor)localObject7).getCount();
        if (k > 0)
        {
          ((Cursor)localObject7).moveToFirst();
          k = 6;
          n = ((Calendar)localObject1).get(k);
          int i7 = ((Calendar)localObject1).getActualMaximum(k);
          for (;;)
          {
            boolean bool2 = ((Cursor)localObject7).isAfterLast();
            if (bool2) {
              break;
            }
            int m = ((Cursor)localObject7).getColumnIndexOrThrow("Day");
            localObject3 = ((Cursor)localObject7).getString(m);
            i3 = ((Cursor)localObject7).getColumnIndexOrThrow("eventCount");
            localObject6 = ((Cursor)localObject7).getString(i3);
            i4 = -1;
            try
            {
              m = Integer.parseInt((String)localObject3);
            }
            catch (NumberFormatException localNumberFormatException)
            {
              m = -1;
            }
            if ((m != i4) && (localObject6 != null))
            {
              if (m >= n)
              {
                m -= n;
                m = j - m - i2;
              }
              else
              {
                i4 = i7 - n + m;
                m = j - i4 - i2;
              }
              if (m >= 0) {
                arrayOfString[m] = localObject6;
              }
            }
            ((Cursor)localObject7).moveToNext();
          }
        }
      }
      if (localObject7 != null) {
        ((Cursor)localObject7).close();
      }
      while (i1 < j)
      {
        localObject1 = arrayOfString[i1];
        boolean bool3 = TextUtils.isEmpty((CharSequence)localObject1);
        if (bool3)
        {
          localObject1 = "";
          arrayOfString[i1] = localObject1;
        }
        i1 += 1;
      }
      return arrayOfString;
    }
    localObject2 = this;
    return null;
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */