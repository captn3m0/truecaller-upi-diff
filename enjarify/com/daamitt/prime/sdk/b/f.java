package com.daamitt.prime.sdk.b;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.location.Location;
import android.text.TextUtils;
import com.daamitt.prime.sdk.a.a;
import com.daamitt.prime.sdk.a.c.a;
import com.daamitt.prime.sdk.a.e;
import com.daamitt.prime.sdk.a.h;
import com.daamitt.prime.sdk.a.i.a;
import com.daamitt.prime.sdk.a.k;
import com.daamitt.prime.sdk.a.m;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.TimeZone;
import java.util.UUID;

public class f
{
  public static String[] b = tmp360_344;
  private static final String d = "f";
  private static String f = "SUM(amount) as Amount ";
  private static String g = "transactions JOIN accounts ON transactions.accountId = accounts._id";
  private static String[] h;
  private static f i;
  public b a;
  String[] c;
  private SQLiteDatabase e = null;
  
  static
  {
    String[] tmp5_2 = new String[28];
    String[] tmp6_5 = tmp5_2;
    String[] tmp6_5 = tmp5_2;
    tmp6_5[0] = "_id";
    tmp6_5[1] = "wSmsId";
    String[] tmp15_6 = tmp6_5;
    String[] tmp15_6 = tmp6_5;
    tmp15_6[2] = "pos";
    tmp15_6[3] = "amount";
    String[] tmp24_15 = tmp15_6;
    String[] tmp24_15 = tmp15_6;
    tmp24_15[4] = "txnDate";
    tmp24_15[5] = "type";
    String[] tmp33_24 = tmp24_15;
    String[] tmp33_24 = tmp24_15;
    tmp33_24[6] = "flags";
    tmp33_24[7] = "accountId";
    String[] tmp44_33 = tmp33_24;
    String[] tmp44_33 = tmp33_24;
    tmp44_33[8] = "merchantId";
    tmp44_33[9] = "categories";
    String[] tmp55_44 = tmp44_33;
    String[] tmp55_44 = tmp44_33;
    tmp55_44[10] = "placeId";
    tmp55_44[11] = "placeName";
    String[] tmp66_55 = tmp55_44;
    String[] tmp66_55 = tmp55_44;
    tmp66_55[12] = "placeDisplayName";
    tmp66_55[13] = "placeLat";
    String[] tmp77_66 = tmp66_55;
    String[] tmp77_66 = tmp66_55;
    tmp77_66[14] = "placeLon";
    tmp77_66[15] = "txnTags";
    String[] tmp88_77 = tmp77_66;
    String[] tmp88_77 = tmp77_66;
    tmp88_77[16] = "txnNote";
    tmp88_77[17] = "txnPhoto";
    String[] tmp99_88 = tmp88_77;
    String[] tmp99_88 = tmp88_77;
    tmp99_88[18] = "UUID";
    tmp99_88[19] = "modifyCount";
    String[] tmp110_99 = tmp99_88;
    String[] tmp110_99 = tmp99_88;
    tmp110_99[20] = "txnBalance";
    tmp110_99[21] = "txnOutstandingBalance";
    String[] tmp121_110 = tmp110_99;
    String[] tmp121_110 = tmp110_99;
    tmp121_110[22] = "txnPhotoServerPath";
    tmp121_110[23] = "conversionRate";
    String[] tmp132_121 = tmp121_110;
    String[] tmp132_121 = tmp121_110;
    tmp132_121[24] = "currencyAmount";
    tmp132_121[25] = "currencySymbol";
    tmp132_121[26] = "txnRecursionAccountID";
    String[] tmp148_132 = tmp132_121;
    tmp148_132[27] = "recursionAccountUUID";
    h = tmp148_132;
    String[] tmp162_159 = new String[38];
    String[] tmp163_162 = tmp162_159;
    String[] tmp163_162 = tmp162_159;
    tmp163_162[0] = "transactions._id";
    tmp163_162[1] = "wSmsId";
    String[] tmp172_163 = tmp163_162;
    String[] tmp172_163 = tmp163_162;
    tmp172_163[2] = "pos";
    tmp172_163[3] = "amount";
    String[] tmp181_172 = tmp172_163;
    String[] tmp181_172 = tmp172_163;
    tmp181_172[4] = "txnDate";
    tmp181_172[5] = "transactions.type";
    String[] tmp190_181 = tmp181_172;
    String[] tmp190_181 = tmp181_172;
    tmp190_181[6] = "transactions.flags";
    tmp190_181[7] = "transactions.accountId";
    String[] tmp201_190 = tmp190_181;
    String[] tmp201_190 = tmp190_181;
    tmp201_190[8] = "merchantId";
    tmp201_190[9] = "categories";
    String[] tmp212_201 = tmp201_190;
    String[] tmp212_201 = tmp201_190;
    tmp212_201[10] = "placeId";
    tmp212_201[11] = "placeName";
    String[] tmp223_212 = tmp212_201;
    String[] tmp223_212 = tmp212_201;
    tmp223_212[12] = "placeDisplayName";
    tmp223_212[13] = "placeLat";
    String[] tmp234_223 = tmp223_212;
    String[] tmp234_223 = tmp223_212;
    tmp234_223[14] = "placeLon";
    tmp234_223[15] = "txnTags";
    String[] tmp245_234 = tmp234_223;
    String[] tmp245_234 = tmp234_223;
    tmp245_234[16] = "txnNote";
    tmp245_234[17] = "txnPhoto";
    String[] tmp256_245 = tmp245_234;
    String[] tmp256_245 = tmp245_234;
    tmp256_245[18] = "txnPhotoServerPath";
    tmp256_245[19] = "txnBalance";
    String[] tmp267_256 = tmp256_245;
    String[] tmp267_256 = tmp256_245;
    tmp267_256[20] = "txnOutstandingBalance";
    tmp267_256[21] = "conversionRate";
    String[] tmp278_267 = tmp267_256;
    String[] tmp278_267 = tmp267_256;
    tmp278_267[22] = "currencyAmount";
    tmp278_267[23] = "currencySymbol";
    String[] tmp289_278 = tmp278_267;
    String[] tmp289_278 = tmp278_267;
    tmp289_278[24] = "recursionAccountUUID";
    tmp289_278[25] = "sender";
    String[] tmp300_289 = tmp289_278;
    String[] tmp300_289 = tmp289_278;
    tmp300_289[26] = "date";
    tmp300_289[27] = "body";
    String[] tmp311_300 = tmp300_289;
    String[] tmp311_300 = tmp300_289;
    tmp311_300[28] = "ifnull(lat, 360) as lat";
    tmp311_300[29] = "ifnull(long, 360) as long";
    String[] tmp322_311 = tmp311_300;
    String[] tmp322_311 = tmp311_300;
    tmp322_311[30] = "ifnull(locAccuracy, -1) as locAccuracy";
    tmp322_311[31] = "tags";
    String[] tmp333_322 = tmp322_311;
    String[] tmp333_322 = tmp322_311;
    tmp333_322[32] = "name";
    tmp333_322[33] = "displayName";
    String[] tmp344_333 = tmp333_322;
    String[] tmp344_333 = tmp333_322;
    tmp344_333[34] = "displayPan";
    tmp344_333[35] = "enabled";
    tmp344_333[36] = "accounts.flags as accountFlags";
    String[] tmp360_344 = tmp344_333;
    tmp360_344[37] = "transactions.UUID";
  }
  
  private f(b paramb)
  {
    String[] tmp14_11 = new String[29];
    String[] tmp15_14 = tmp14_11;
    String[] tmp15_14 = tmp14_11;
    tmp15_14[0] = "transactions._id";
    tmp15_14[1] = "transactions.wSmsId";
    String[] tmp24_15 = tmp15_14;
    String[] tmp24_15 = tmp15_14;
    tmp24_15[2] = "transactions.pos";
    tmp24_15[3] = "transactions.amount";
    String[] tmp33_24 = tmp24_15;
    String[] tmp33_24 = tmp24_15;
    tmp33_24[4] = "transactions.txnDate";
    tmp33_24[5] = "transactions.type";
    String[] tmp42_33 = tmp33_24;
    String[] tmp42_33 = tmp33_24;
    tmp42_33[6] = "transactions.flags";
    tmp42_33[7] = "transactions.accountId";
    String[] tmp53_42 = tmp42_33;
    String[] tmp53_42 = tmp42_33;
    tmp53_42[8] = "transactions.merchantId";
    tmp53_42[9] = "transactions.categories";
    String[] tmp64_53 = tmp53_42;
    String[] tmp64_53 = tmp53_42;
    tmp64_53[10] = "transactions.placeId";
    tmp64_53[11] = "transactions.placeName";
    String[] tmp75_64 = tmp64_53;
    String[] tmp75_64 = tmp64_53;
    tmp75_64[12] = "transactions.placeDisplayName";
    tmp75_64[13] = "transactions.placeLat";
    String[] tmp86_75 = tmp75_64;
    String[] tmp86_75 = tmp75_64;
    tmp86_75[14] = "transactions.placeLon";
    tmp86_75[15] = "transactions.txnTags";
    String[] tmp97_86 = tmp86_75;
    String[] tmp97_86 = tmp86_75;
    tmp97_86[16] = "transactions.txnNote";
    tmp97_86[17] = "transactions.txnPhoto";
    String[] tmp108_97 = tmp97_86;
    String[] tmp108_97 = tmp97_86;
    tmp108_97[18] = "transactions.txnPhotoServerPath";
    tmp108_97[19] = "transactions.UUID";
    String[] tmp119_108 = tmp108_97;
    String[] tmp119_108 = tmp108_97;
    tmp119_108[20] = "transactions.modifyCount";
    tmp119_108[21] = "transactions.txnBalance";
    String[] tmp130_119 = tmp119_108;
    String[] tmp130_119 = tmp119_108;
    tmp130_119[22] = "transactions.txnOutstandingBalance";
    tmp130_119[23] = "transactions.conversionRate";
    String[] tmp141_130 = tmp130_119;
    String[] tmp141_130 = tmp130_119;
    tmp141_130[24] = "transactions.currencyAmount";
    tmp141_130[25] = "transactions.currencySymbol";
    String[] tmp152_141 = tmp141_130;
    String[] tmp152_141 = tmp141_130;
    tmp152_141[26] = "transactions.txnRecursionAccountID";
    tmp152_141[27] = "transactions.recursionAccountUUID";
    tmp152_141[28] = "pan";
    Object localObject = tmp152_141;
    c = ((String[])localObject);
    localObject = paramb;
    a = paramb;
  }
  
  private static m a(Cursor paramCursor)
  {
    Object localObject1 = paramCursor;
    int j = paramCursor.getColumnIndexOrThrow("_id");
    j = paramCursor.getInt(j);
    int k = paramCursor.getColumnIndexOrThrow("wSmsId");
    k = paramCursor.getInt(k);
    int m = paramCursor.getColumnIndexOrThrow("pos");
    String str1 = paramCursor.getString(m);
    m = paramCursor.getColumnIndexOrThrow("amount");
    double d1 = paramCursor.getDouble(m);
    Object localObject2 = Calendar.getInstance();
    int n = paramCursor.getColumnIndexOrThrow("txnDate");
    long l1 = paramCursor.getLong(n);
    ((Calendar)localObject2).setTimeInMillis(l1);
    Date localDate = ((Calendar)localObject2).getTime();
    int i1 = paramCursor.getColumnIndexOrThrow("type");
    int i2 = paramCursor.getInt(i1);
    i1 = paramCursor.getColumnIndexOrThrow("flags");
    int i3 = paramCursor.getInt(i1);
    i1 = paramCursor.getColumnIndexOrThrow("accountId");
    i1 = paramCursor.getInt(i1);
    n = paramCursor.getColumnIndexOrThrow("txnBalance");
    double d2 = paramCursor.getDouble(n);
    n = paramCursor.getColumnIndexOrThrow("txnOutstandingBalance");
    double d3 = paramCursor.getDouble(n);
    n = paramCursor.getColumnIndexOrThrow("conversionRate");
    double d4 = d3;
    d3 = paramCursor.getDouble(n);
    n = paramCursor.getColumnIndexOrThrow("currencyAmount");
    double d5 = d3;
    d3 = paramCursor.getDouble(n);
    n = paramCursor.getColumnIndexOrThrow("currencySymbol");
    Object localObject3 = paramCursor.getString(n);
    m localm = new com/daamitt/prime/sdk/a/m;
    Object localObject4 = localObject3;
    n = 0;
    localm.<init>(null, null, null);
    l1 = j;
    o = l1;
    long l2 = k;
    C = l2;
    w = i1;
    u = 3;
    localm.a(null, "Spends");
    j = paramCursor.getColumnIndexOrThrow("txnTags");
    Object localObject5 = paramCursor.getString(j);
    U = ((String)localObject5);
    j = paramCursor.getColumnIndexOrThrow("txnNote");
    localObject5 = paramCursor.getString(j);
    V = ((String)localObject5);
    j = paramCursor.getColumnIndexOrThrow("txnPhoto");
    localObject5 = paramCursor.getString(j);
    Z = ((String)localObject5);
    j = paramCursor.getColumnIndexOrThrow("txnPhotoServerPath");
    localObject5 = paramCursor.getString(j);
    aa = ((String)localObject5);
    localObject3 = Double.valueOf(d1);
    i1 = 0;
    localObject2 = null;
    localObject5 = localObject4;
    localm.a(null, (Double)localObject3, localDate, str1, i2);
    k = paramCursor.getColumnIndexOrThrow("categories");
    Object localObject6 = paramCursor.getString(k);
    T = ((String)localObject6);
    Y = i3;
    k = paramCursor.getColumnIndexOrThrow("UUID");
    localObject6 = paramCursor.getString(k);
    ab = ((String)localObject6);
    localObject6 = new com/daamitt/prime/sdk/a/b;
    ((com.daamitt.prime.sdk.a.b)localObject6).<init>();
    a = d2;
    d1 = d4;
    b = d4;
    i = ((com.daamitt.prime.sdk.a.b)localObject6);
    k = paramCursor.getColumnIndexOrThrow("merchantId");
    long l3 = paramCursor.getLong(k);
    k = l3;
    k = paramCursor.getColumnIndexOrThrow("placeId");
    localObject6 = paramCursor.getString(k);
    l = ((String)localObject6);
    k = paramCursor.getColumnIndexOrThrow("placeName");
    localObject6 = paramCursor.getString(k);
    m = ((String)localObject6);
    k = paramCursor.getColumnIndexOrThrow("placeDisplayName");
    localObject6 = paramCursor.getString(k);
    n = ((String)localObject6);
    g = d5;
    h = d3;
    e = ((String)localObject4);
    j = paramCursor.getColumnIndexOrThrow("recursionAccountUUID");
    localObject5 = paramCursor.getString(j);
    W = ((String)localObject5);
    localObject5 = "placeLat";
    j = paramCursor.getColumnIndex((String)localObject5);
    double d6 = paramCursor.getDouble(j);
    String str2 = "placeLon";
    m = paramCursor.getColumnIndex(str2);
    d1 = paramCursor.getDouble(m);
    double d7 = 360.0D;
    boolean bool = d6 < d7;
    if (bool)
    {
      bool = d1 < d7;
      if (bool)
      {
        localObject1 = new android/location/Location;
        localObject2 = "walnutloc";
        ((Location)localObject1).<init>((String)localObject2);
        ((Location)localObject1).setLatitude(d6);
        ((Location)localObject1).setLongitude(d1);
        X = ((Location)localObject1);
      }
    }
    return localm;
  }
  
  public static f a(b paramb)
  {
    f localf = i;
    if (localf == null)
    {
      localf = new com/daamitt/prime/sdk/b/f;
      localf.<init>(paramb);
      i = localf;
      paramb = paramb.getWritableDatabase();
      e = paramb;
    }
    return i;
  }
  
  private static String a(int paramInt)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    int j = paramInt * 2;
    int k = 1;
    j -= k;
    localStringBuilder.<init>(j);
    String str = "?";
    localStringBuilder.append(str);
    while (k < paramInt)
    {
      str = ",?";
      localStringBuilder.append(str);
      k += 1;
    }
    return localStringBuilder.toString();
  }
  
  private String a(Calendar paramCalendar1, Calendar paramCalendar2, String paramString1, int paramInt, String paramString2)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    String str = "transactions.type in ( ";
    localStringBuilder.<init>(str);
    localStringBuilder.append(paramString2);
    paramString2 = "  )  AND txnDate between ";
    localStringBuilder.append(paramString2);
    long l1 = paramCalendar1.getTime().getTime();
    localStringBuilder.append(l1);
    localStringBuilder.append(" AND ");
    long l2 = paramCalendar2.getTime().getTime();
    localStringBuilder.append(l2);
    localStringBuilder.append(" AND transactions.flags & 128 = 0 ");
    paramCalendar1 = localStringBuilder.toString();
    boolean bool = TextUtils.isEmpty(paramString1);
    if (!bool)
    {
      paramCalendar2 = new java/lang/StringBuilder;
      paramCalendar2.<init>();
      paramCalendar2.append(paramCalendar1);
      paramCalendar2.append(" AND accounts.type in ( ");
      paramCalendar2.append(paramString1);
      paramCalendar2.append("  ) ");
      paramCalendar1 = paramCalendar2.toString();
    }
    if (paramInt != 0)
    {
      bool = true;
      paramCalendar2 = new int[bool];
      paramCalendar2[0] = paramInt;
      paramString1 = new java/lang/StringBuilder;
      paramString1.<init>();
      paramString1.append(paramCalendar1);
      paramString1.append(" AND accounts._id IN (");
      paramCalendar1 = a.c(paramCalendar2);
      paramString1.append(paramCalendar1);
      paramString1.append(")");
      paramCalendar1 = paramString1.toString();
    }
    return paramCalendar1;
  }
  
  public static void a(SQLiteDatabase paramSQLiteDatabase)
  {
    e.b();
    paramSQLiteDatabase.execSQL("create table if not exists transactions(_id integer primary key autoincrement,wSmsId integer not null, pos text not null,amount real not null,txnDate integer not null,type integer not null,flags integer default 0,accountId integer not null,merchantId integer default -1,categories text not null default other,placeId text default null,placeName text default null,placeDisplayName text default null,placeLat double default 360,placeLon double default 360,txnTags text,txnNote text,txnPhoto text,UUID text,modifyCount integer default 1,txnBalance real,txnOutstandingBalance real,txnPhotoServerPath text,conversionRate real default 1,currencyAmount real default 0,currencySymbol text, txnRecursionAccountID int default 0,recursionAccountUUID text );");
    e.b();
    paramSQLiteDatabase.execSQL("create trigger if not exists TxnTriggerModifiedFlag After update on transactions for each row  Begin  Update transactions Set modifyCount = modifyCount + 1  Where _id =  New._id;  End; ");
  }
  
  private static String b(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt != null)
    {
      int j = paramArrayOfInt.length;
      if (j > 0)
      {
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        int k = paramArrayOfInt.length * 2;
        int m = 1;
        k -= m;
        localStringBuilder.<init>(k);
        Object localObject = null;
        k = paramArrayOfInt[0];
        localStringBuilder.append(k);
        for (;;)
        {
          k = paramArrayOfInt.length;
          if (m >= k) {
            break;
          }
          localObject = new java/lang/StringBuilder;
          String str = ",";
          ((StringBuilder)localObject).<init>(str);
          int n = paramArrayOfInt[m];
          ((StringBuilder)localObject).append(n);
          localObject = ((StringBuilder)localObject).toString();
          localStringBuilder.append((String)localObject);
          m += 1;
        }
        return localStringBuilder.toString();
      }
    }
    return null;
  }
  
  public static void b(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.beginTransaction();
    String str = "drop table if exists transactions";
    try
    {
      paramSQLiteDatabase.execSQL(str);
      str = "drop trigger if exists TxnTriggerModifiedFlag";
      paramSQLiteDatabase.execSQL(str);
      a(paramSQLiteDatabase);
      paramSQLiteDatabase.setTransactionSuccessful();
      return;
    }
    finally
    {
      paramSQLiteDatabase.endTransaction();
    }
  }
  
  private String f(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt)
  {
    int[] arrayOfInt = new int[1];
    arrayOfInt[0] = paramInt;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("transactions.type in ( ");
    String str = m.e();
    localStringBuilder.append(str);
    localStringBuilder.append("  )  and txnDate between ");
    long l1 = paramCalendar1.getTime().getTime();
    localStringBuilder.append(l1);
    localStringBuilder.append(" AND ");
    long l2 = paramCalendar2.getTime().getTime();
    localStringBuilder.append(l2);
    localStringBuilder.append(" AND txnBalance NOTNULL  AND accountId IN (");
    paramCalendar1 = a.c(arrayOfInt);
    localStringBuilder.append(paramCalendar1);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final int a(m paramm, ContentValues paramContentValues)
  {
    long l1 = o;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (!bool)
    {
      SQLiteDatabase localSQLiteDatabase = e;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>("_id = ");
      long l3 = o;
      localStringBuilder.append(l3);
      paramm = localStringBuilder.toString();
      return localSQLiteDatabase.update("transactions", paramContentValues, paramm, null);
    }
    e.a();
    return -1;
  }
  
  public final long a(m paramm)
  {
    boolean bool1 = paramm.l();
    int j = 0;
    Object localObject1 = null;
    int k = 1;
    String[] arrayOfString1;
    String str1;
    String[] arrayOfString2;
    SQLiteQueryBuilder localSQLiteQueryBuilder;
    SQLiteDatabase localSQLiteDatabase;
    String str2;
    if (bool1)
    {
      arrayOfString1 = new String[] { "sms._id" };
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("sms._id < ");
      l1 = C;
      ((StringBuilder)localObject2).append(l1);
      ((StringBuilder)localObject2).append(" AND body =? AND transactions.flags & 16 = 0 ");
      str1 = ((StringBuilder)localObject2).toString();
      arrayOfString2 = new String[k];
      localObject2 = q;
      arrayOfString2[0] = localObject2;
      localSQLiteQueryBuilder = new android/database/sqlite/SQLiteQueryBuilder;
      localSQLiteQueryBuilder.<init>();
      localSQLiteQueryBuilder.setTables("sms JOIN transactions ON transactions.wSmsId = sms._id");
      localSQLiteDatabase = e;
      str2 = null;
      localObject2 = localSQLiteQueryBuilder.query(localSQLiteDatabase, arrayOfString1, str1, arrayOfString2, null, null, null);
      if (localObject2 != null)
      {
        ((Cursor)localObject2).getCount();
        e.a();
      }
      if (localObject2 != null)
      {
        j = ((Cursor)localObject2).getCount();
        if (j > 0)
        {
          e.a();
          paramm.o();
          paramm.m();
        }
      }
    }
    else
    {
      bool1 = paramm.k();
      if (bool1)
      {
        localObject2 = r;
        if (localObject2 != null)
        {
          arrayOfString1 = new String[] { "sms._id" };
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>("date > ");
          l1 = r.getTime();
          long l2 = 3600000L;
          l1 -= l2;
          ((StringBuilder)localObject2).append(l1);
          ((StringBuilder)localObject2).append(" AND sms._id < ");
          l1 = C;
          ((StringBuilder)localObject2).append(l1);
          ((StringBuilder)localObject2).append(" AND body =? AND transactions.flags & 16 = 0 ");
          str1 = ((StringBuilder)localObject2).toString();
          arrayOfString2 = new String[k];
          localObject2 = q;
          arrayOfString2[0] = localObject2;
          localSQLiteQueryBuilder = new android/database/sqlite/SQLiteQueryBuilder;
          localSQLiteQueryBuilder.<init>();
          localSQLiteQueryBuilder.setTables("sms JOIN transactions ON transactions.wSmsId = sms._id");
          localSQLiteDatabase = e;
          str2 = "sms._id DESC";
          String str3 = "1";
          localObject2 = localSQLiteQueryBuilder.query(localSQLiteDatabase, arrayOfString1, str1, arrayOfString2, null, null, str2, str3);
          if (localObject2 == null) {
            break label406;
          }
          j = ((Cursor)localObject2).getCount();
          if (j <= 0) {
            break label406;
          }
          e.a();
          paramm.o();
          paramm.m();
          break label406;
        }
      }
      bool1 = false;
      localObject2 = null;
    }
    label406:
    if (localObject2 != null) {
      ((Cursor)localObject2).close();
    }
    Object localObject2 = new android/content/ContentValues;
    ((ContentValues)localObject2).<init>();
    long l1 = C;
    Object localObject3 = Long.valueOf(l1);
    ((ContentValues)localObject2).put("wSmsId", (Long)localObject3);
    localObject3 = c.toLowerCase();
    ((ContentValues)localObject2).put("pos", (String)localObject3);
    localObject1 = n;
    if (localObject1 != null)
    {
      localObject1 = "placeDisplayName";
      localObject3 = n;
      ((ContentValues)localObject2).put((String)localObject1, (String)localObject3);
    }
    localObject3 = m;
    ((ContentValues)localObject2).put("placeName", (String)localObject3);
    localObject3 = n;
    ((ContentValues)localObject2).put("placeDisplayName", (String)localObject3);
    double d1 = f;
    localObject3 = Double.valueOf(d1);
    ((ContentValues)localObject2).put("amount", (Double)localObject3);
    l1 = ad.getTime();
    localObject3 = Long.valueOf(l1);
    ((ContentValues)localObject2).put("txnDate", (Long)localObject3);
    localObject3 = Integer.valueOf(j);
    ((ContentValues)localObject2).put("type", (Integer)localObject3);
    localObject3 = Integer.valueOf(Y);
    ((ContentValues)localObject2).put("flags", (Integer)localObject3);
    k = w;
    localObject3 = Integer.valueOf(k);
    ((ContentValues)localObject2).put("accountId", (Integer)localObject3);
    localObject3 = V;
    ((ContentValues)localObject2).put("txnNote", (String)localObject3);
    localObject1 = l;
    if (localObject1 != null)
    {
      localObject1 = "placeId";
      localObject3 = l;
      ((ContentValues)localObject2).put((String)localObject1, (String)localObject3);
    }
    long l3 = k;
    l1 = -1;
    d1 = 0.0D / 0.0D;
    boolean bool3 = l3 < l1;
    if (bool3)
    {
      localObject1 = "merchantId";
      l1 = k;
      localObject3 = Long.valueOf(l1);
      ((ContentValues)localObject2).put((String)localObject1, (Long)localObject3);
    }
    localObject1 = T;
    if (localObject1 != null)
    {
      localObject1 = "categories";
      localObject3 = T;
      ((ContentValues)localObject2).put((String)localObject1, (String)localObject3);
    }
    localObject1 = D;
    if (localObject1 != null)
    {
      localObject3 = Double.valueOf(D.getLatitude());
      ((ContentValues)localObject2).put("placeLat", (Double)localObject3);
      localObject1 = "placeLon";
      d1 = D.getLongitude();
      localObject3 = Double.valueOf(d1);
      ((ContentValues)localObject2).put((String)localObject1, (Double)localObject3);
    }
    localObject1 = U;
    if (localObject1 != null)
    {
      localObject1 = "txnTags";
      localObject3 = U;
      ((ContentValues)localObject2).put((String)localObject1, (String)localObject3);
    }
    localObject1 = V;
    if (localObject1 != null)
    {
      localObject1 = "txnNote";
      localObject3 = V;
      ((ContentValues)localObject2).put((String)localObject1, (String)localObject3);
    }
    localObject1 = Z;
    if (localObject1 != null)
    {
      localObject1 = "txnPhoto";
      localObject3 = Z;
      ((ContentValues)localObject2).put((String)localObject1, (String)localObject3);
    }
    localObject1 = aa;
    if (localObject1 != null)
    {
      localObject1 = "txnPhotoServerPath";
      localObject3 = aa;
      ((ContentValues)localObject2).put((String)localObject1, (String)localObject3);
    }
    d1 = h;
    localObject3 = Double.valueOf(d1);
    ((ContentValues)localObject2).put("currencyAmount", (Double)localObject3);
    localObject1 = e;
    boolean bool2 = TextUtils.isEmpty((CharSequence)localObject1);
    if (!bool2)
    {
      localObject1 = "currencySymbol";
      localObject3 = e;
      ((ContentValues)localObject2).put((String)localObject1, (String)localObject3);
    }
    localObject1 = UUID.randomUUID().toString();
    ab = ((String)localObject1);
    localObject3 = "UUID";
    ((ContentValues)localObject2).put((String)localObject3, (String)localObject1);
    localObject1 = i;
    if (localObject1 != null)
    {
      localObject3 = Double.valueOf(i.a);
      ((ContentValues)localObject2).put("txnBalance", (Double)localObject3);
      localObject1 = "txnOutstandingBalance";
      d1 = i.b;
      paramm = Double.valueOf(d1);
      ((ContentValues)localObject2).put((String)localObject1, paramm);
    }
    return e.insert("transactions", null, (ContentValues)localObject2);
  }
  
  public final m a(ArrayList paramArrayList1, ArrayList paramArrayList2, m paramm)
  {
    f localf = this;
    m localm = paramm;
    e.a();
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    int j = paramArrayList1.isEmpty();
    int n = 0;
    Object localObject2 = null;
    int i1 = 1;
    int k;
    if (j == 0)
    {
      ((StringBuilder)localObject1).append("transactions.accountId IN(");
      j = 0;
      localObject3 = null;
      for (;;)
      {
        int i2 = paramArrayList1.size();
        if (j >= i2) {
          break;
        }
        localObject4 = paramArrayList1;
        localObject5 = (a)paramArrayList1.get(j);
        int i3 = a;
        ((StringBuilder)localObject1).append(i3);
        i3 = paramArrayList1.size() - i1;
        if (j == i3)
        {
          localObject5 = ")";
          ((StringBuilder)localObject1).append((String)localObject5);
        }
        else
        {
          localObject5 = ",";
          ((StringBuilder)localObject1).append((String)localObject5);
        }
        j += 1;
      }
    }
    Object localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>();
    Object localObject4 = paramArrayList2.iterator();
    int i4 = 0;
    Object localObject5 = null;
    Object localObject6;
    Object localObject7;
    int i9;
    boolean bool4;
    Object localObject9;
    String str1;
    boolean bool5;
    for (;;)
    {
      boolean bool3 = ((Iterator)localObject4).hasNext();
      if (!bool3) {
        break;
      }
      localObject6 = (c.a)((Iterator)localObject4).next();
      localObject7 = a;
      int i7 = -1;
      int i8 = ((String)localObject7).hashCode();
      i9 = 3;
      int i10 = 2;
      switch (i8)
      {
      default: 
        break;
      case 3387378: 
        localObject8 = "note";
        bool4 = ((String)localObject7).equals(localObject8);
        if (bool4) {
          i7 = 1;
        }
        break;
      case 3076014: 
        localObject8 = "date";
        bool4 = ((String)localObject7).equals(localObject8);
        if (bool4) {
          i7 = 3;
        }
        break;
      case 111188: 
        localObject8 = "pos";
        bool4 = ((String)localObject7).equals(localObject8);
        if (bool4) {
          i7 = 2;
        }
        break;
      case 110749: 
        localObject8 = "pan";
        bool4 = ((String)localObject7).equals(localObject8);
        if (bool4) {
          i7 = 4;
        }
        break;
      case -1413853096: 
        localObject8 = "amount";
        bool4 = ((String)localObject7).equals(localObject8);
        if (bool4)
        {
          i7 = 0;
          localObject9 = null;
        }
        break;
      }
      switch (i7)
      {
      default: 
        break;
      case 4: 
        localObject7 = "pan";
        break;
      case 3: 
        localObject7 = "txnDate";
        break;
      case 2: 
        localObject7 = "pos";
        break;
      case 1: 
        localObject7 = "txnNote";
        break;
      case 0: 
        localObject7 = "amount";
      }
      localObject9 = b;
      localObject8 = localm.b((String)localObject9);
      if (localObject8 != null) {
        localObject9 = localObject8;
      }
      localObject8 = c;
      e.a();
      str1 = "txnDate";
      bool5 = TextUtils.equals((CharSequence)localObject7, str1);
      if (bool5)
      {
        bool4 = TextUtils.isEmpty((CharSequence)localObject1);
        if (!bool4)
        {
          localObject8 = " AND ";
          ((StringBuilder)localObject1).append((String)localObject8);
        }
        long l1 = e;
        long l2 = Long.valueOf((String)localObject9).longValue() - l1;
        long l3 = Long.valueOf((String)localObject9).longValue() + l1;
        ((StringBuilder)localObject1).append((String)localObject7);
        ((StringBuilder)localObject1).append(" >=? AND ");
        ((StringBuilder)localObject1).append((String)localObject7);
        ((StringBuilder)localObject1).append(" <=?");
        localObject6 = String.valueOf(l2);
        ((ArrayList)localObject3).add(localObject6);
        localObject6 = String.valueOf(l3);
        ((ArrayList)localObject3).add(localObject6);
      }
      else
      {
        str1 = "deleted";
        bool5 = TextUtils.equals((CharSequence)localObject7, str1);
        if (bool5)
        {
          i4 = d;
          if (i4 == i9)
          {
            bool3 = TextUtils.isEmpty((CharSequence)localObject1);
            if (!bool3)
            {
              localObject6 = " AND ";
              ((StringBuilder)localObject1).append((String)localObject6);
            }
            localObject6 = "transactions.flags & 16 = 0";
            ((StringBuilder)localObject1).append((String)localObject6);
          }
          else if (i4 == i10)
          {
            bool3 = TextUtils.isEmpty((CharSequence)localObject1);
            if (!bool3)
            {
              localObject6 = " AND ";
              ((StringBuilder)localObject1).append((String)localObject6);
            }
            localObject6 = "transactions.flags & 16 != 0";
            ((StringBuilder)localObject1).append((String)localObject6);
          }
          bool3 = TextUtils.isEmpty((CharSequence)localObject1);
          if (!bool3)
          {
            localObject6 = " AND ";
            ((StringBuilder)localObject1).append((String)localObject6);
          }
          localObject6 = "transactions.flags & 256 != 0";
          ((StringBuilder)localObject1).append((String)localObject6);
        }
        else
        {
          str1 = "pattern_UID";
          bool5 = TextUtils.equals((CharSequence)localObject7, str1);
          int i6;
          if (bool5)
          {
            int i5 = d;
            long l4;
            if (i5 == i9)
            {
              i6 = TextUtils.isEmpty((CharSequence)localObject1);
              if (i6 == 0)
              {
                localObject6 = " AND ";
                ((StringBuilder)localObject1).append((String)localObject6);
              }
              ((StringBuilder)localObject1).append("patternUID !=?");
              l4 = F.j;
              localObject6 = String.valueOf(l4);
              ((ArrayList)localObject3).add(localObject6);
            }
            else if (i6 == i10)
            {
              i6 = TextUtils.isEmpty((CharSequence)localObject1);
              if (i6 == 0)
              {
                localObject6 = " AND ";
                ((StringBuilder)localObject1).append((String)localObject6);
              }
              ((StringBuilder)localObject1).append("patternUID =?");
              l4 = F.j;
              localObject6 = String.valueOf(l4);
              ((ArrayList)localObject3).add(localObject6);
            }
          }
          else
          {
            localObject6 = "contains";
            i6 = TextUtils.equals((CharSequence)localObject8, (CharSequence)localObject6);
            if (i6 != 0)
            {
              i6 = TextUtils.isEmpty((CharSequence)localObject1);
              if (i6 == 0)
              {
                localObject6 = " AND ";
                ((StringBuilder)localObject1).append((String)localObject6);
              }
              ((StringBuilder)localObject1).append((String)localObject7);
              ((StringBuilder)localObject1).append(" LIKE '%");
              ((StringBuilder)localObject1).append((String)localObject9);
              localObject6 = "%'";
              ((StringBuilder)localObject1).append((String)localObject6);
            }
            else
            {
              localObject6 = "exact";
              i6 = TextUtils.equals((CharSequence)localObject8, (CharSequence)localObject6);
              if (i6 != 0)
              {
                i6 = TextUtils.isEmpty((CharSequence)localObject1);
                if (i6 == 0)
                {
                  localObject6 = " AND ";
                  ((StringBuilder)localObject1).append((String)localObject6);
                }
                ((StringBuilder)localObject1).append((String)localObject7);
                localObject6 = " =? ";
                ((StringBuilder)localObject1).append((String)localObject6);
                ((ArrayList)localObject3).add(localObject9);
              }
            }
          }
        }
      }
    }
    Object localObject8 = new android/database/sqlite/SQLiteQueryBuilder;
    ((SQLiteQueryBuilder)localObject8).<init>();
    ((SQLiteQueryBuilder)localObject8).setTables("transactions JOIN accounts ON transactions.accountId = accounts._id JOIN sms ON sms._id = transactions.wSmsId");
    localm = null;
    SQLiteDatabase localSQLiteDatabase;
    String[] arrayOfString;
    try
    {
      localSQLiteDatabase = e;
      arrayOfString = c;
      str1 = ((StringBuilder)localObject1).toString();
      i12 = ((ArrayList)localObject3).size();
      localObject1 = new String[i12];
      localObject1 = ((ArrayList)localObject3).toArray((Object[])localObject1);
      Object localObject10 = localObject1;
      localObject10 = (String[])localObject1;
      String str2 = "transactions._id DESC";
      localObject1 = ((SQLiteQueryBuilder)localObject8).query(localSQLiteDatabase, arrayOfString, str1, (String[])localObject10, null, null, str2);
    }
    catch (RuntimeException localRuntimeException)
    {
      e.b();
      int i12 = 0;
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      k = ((Cursor)localObject1).getCount();
      if (k > 0)
      {
        ((Cursor)localObject1).moveToFirst();
        boolean bool1 = ((Cursor)localObject1).isAfterLast();
        if (!bool1)
        {
          localObject3 = a((Cursor)localObject1);
          n = ((Cursor)localObject1).getColumnIndexOrThrow("pan");
          localObject2 = ((Cursor)localObject1).getString(n);
          b = ((String)localObject2);
          n = Y & 0x10;
          if ((n != 0) || (i4 != i1)) {
            break label1586;
          }
          long l5 = C;
          localObject4 = a;
          localObject2 = ((b)localObject4).a(l5);
          for (;;)
          {
            Object localObject11 = I;
            if (localObject11 == null) {
              break;
            }
            localObject3 = a;
            localObject2 = I;
            localObject2 = ((b)localObject3).a((String)localObject2);
            long l6 = o;
            localObject5 = e;
            localObject6 = "transactions";
            localObject7 = h;
            localObject11 = String.valueOf(l6);
            localObject9 = "wSmsId = ".concat((String)localObject11);
            bool4 = false;
            localObject8 = null;
            i9 = 0;
            localSQLiteDatabase = null;
            int i11 = 0;
            arrayOfString = null;
            bool5 = false;
            str1 = null;
            localObject3 = ((SQLiteDatabase)localObject5).query((String)localObject6, (String[])localObject7, (String)localObject9, null, null, null, null);
            if (localObject3 != null)
            {
              i1 = ((Cursor)localObject3).getCount();
              if (i1 > 0)
              {
                ((Cursor)localObject3).moveToFirst();
                bool2 = ((Cursor)localObject3).isAfterLast();
                if (!bool2)
                {
                  localObject11 = a((Cursor)localObject3);
                  break label1539;
                }
              }
            }
            boolean bool2 = false;
            localObject11 = null;
            label1539:
            if (localObject3 != null) {
              ((Cursor)localObject3).close();
            }
            m = Y & 0x10;
            if (m != 0)
            {
              localObject3 = localObject11;
              break;
            }
            localObject3 = localObject11;
          }
        }
      }
    }
    int m = 0;
    localObject3 = null;
    label1586:
    if (localObject1 != null) {
      ((Cursor)localObject1).close();
    }
    return (m)localObject3;
  }
  
  public final ArrayList a(int[] paramArrayOfInt)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>(" WHERE type IN ( ");
    paramArrayOfInt = b(paramArrayOfInt);
    ((StringBuilder)localObject).append(paramArrayOfInt);
    ((StringBuilder)localObject).append(" )");
    paramArrayOfInt = String.valueOf(((StringBuilder)localObject).toString());
    paramArrayOfInt = "SELECT DISTINCT accountId FROM transactions".concat(paramArrayOfInt);
    localObject = e;
    paramArrayOfInt = ((SQLiteDatabase)localObject).rawQuery(paramArrayOfInt, null);
    if (paramArrayOfInt != null)
    {
      boolean bool = paramArrayOfInt.moveToFirst();
      if (bool) {
        for (;;)
        {
          bool = paramArrayOfInt.isAfterLast();
          if (bool) {
            break;
          }
          int j = paramArrayOfInt.getColumnIndexOrThrow("accountId");
          j = paramArrayOfInt.getInt(j);
          localObject = Integer.valueOf(j);
          localArrayList.add(localObject);
          paramArrayOfInt.moveToNext();
        }
      }
    }
    if (paramArrayOfInt != null) {
      paramArrayOfInt.close();
    }
    return localArrayList;
  }
  
  public final ArrayList a(int[] paramArrayOfInt1, int[] paramArrayOfInt2, Date paramDate1, Date paramDate2)
  {
    Object localObject1 = paramArrayOfInt1;
    Object localObject2 = paramArrayOfInt2;
    Object localObject3 = "txnDate ASC";
    Object localObject4 = new java/util/ArrayList;
    ((ArrayList)localObject4).<init>();
    Object localObject5 = "";
    Object localObject6 = new java/util/ArrayList;
    ((ArrayList)localObject6).<init>();
    int j = 0;
    String str1 = null;
    int k = 1;
    if (paramArrayOfInt1 != null)
    {
      m = paramArrayOfInt1.length;
      if (m > k) {}
    }
    else
    {
      if (localObject1 != null) {
        break label119;
      }
    }
    localObject5 = new java/lang/StringBuilder;
    ((StringBuilder)localObject5).<init>("accounts.enabled=");
    Object localObject7 = a(k);
    ((StringBuilder)localObject5).append((String)localObject7);
    localObject5 = ((StringBuilder)localObject5).toString();
    localObject7 = "1";
    ((ArrayList)localObject6).add(localObject7);
    int m = 1;
    break label125;
    label119:
    m = 0;
    localObject7 = null;
    label125:
    if (localObject2 != null)
    {
      localObject8 = new java/lang/StringBuilder;
      ((StringBuilder)localObject8).<init>();
      if (m != 0) {
        localObject7 = " AND ";
      } else {
        localObject7 = "";
      }
      ((StringBuilder)localObject8).append((String)localObject7);
      ((StringBuilder)localObject8).append("transactions.type IN (");
      localObject7 = a(localObject2.length);
      ((StringBuilder)localObject8).append((String)localObject7);
      ((StringBuilder)localObject8).append(")");
      localObject7 = ((StringBuilder)localObject8).toString();
      localObject5 = ((String)localObject5).concat((String)localObject7);
      m = localObject2.length;
      n = 0;
      localObject8 = null;
      while (n < m)
      {
        i1 = localObject2[n];
        localObject9 = String.valueOf(i1);
        ((ArrayList)localObject6).add(localObject9);
        n += 1;
      }
      m = 1;
    }
    if (localObject1 != null)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      if (m != 0) {
        localObject7 = " AND ";
      } else {
        localObject7 = "";
      }
      ((StringBuilder)localObject2).append((String)localObject7);
      localObject7 = "transactions.accountId IN (";
      ((StringBuilder)localObject2).append((String)localObject7);
      localObject1 = b(paramArrayOfInt1);
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append(")");
      localObject1 = ((StringBuilder)localObject2).toString();
      localObject5 = ((String)localObject5).concat((String)localObject1);
      m = 1;
    }
    if (paramDate1 != null)
    {
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      if (m != 0) {
        localObject2 = " AND ";
      } else {
        localObject2 = "";
      }
      ((StringBuilder)localObject1).append((String)localObject2);
      localObject2 = "txnDate >=?";
      ((StringBuilder)localObject1).append((String)localObject2);
      localObject1 = ((StringBuilder)localObject1).toString();
      localObject5 = ((String)localObject5).concat((String)localObject1);
      long l1 = paramDate1.getTime();
      localObject1 = String.valueOf(l1);
      ((ArrayList)localObject6).add(localObject1);
      m = 1;
    }
    long l2;
    if (paramDate2 != null)
    {
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      if (m != 0) {
        localObject2 = " AND ";
      } else {
        localObject2 = "";
      }
      ((StringBuilder)localObject1).append((String)localObject2);
      ((StringBuilder)localObject1).append("txnDate <=?");
      localObject1 = ((StringBuilder)localObject1).toString();
      localObject1 = ((String)localObject5).concat((String)localObject1);
      l2 = paramDate2.getTime();
      localObject2 = String.valueOf(l2);
      ((ArrayList)localObject6).add(localObject2);
      localObject7 = localObject1;
    }
    else
    {
      localObject7 = localObject5;
    }
    localObject1 = new android/database/sqlite/SQLiteQueryBuilder;
    ((SQLiteQueryBuilder)localObject1).<init>();
    ((SQLiteQueryBuilder)localObject1).setTables("transactions JOIN accounts ON transactions.accountId = accounts._id LEFT OUTER JOIN sms ON transactions.wSmsId = sms._id");
    Object localObject10 = this;
    localObject2 = e;
    localObject5 = b;
    Object localObject8 = new String[0];
    localObject6 = ((ArrayList)localObject6).toArray((Object[])localObject8);
    localObject8 = localObject6;
    localObject8 = (String[])localObject6;
    int i2 = 0;
    Object localObject11 = null;
    localObject6 = localObject7;
    localObject7 = localObject8;
    int n = 0;
    localObject8 = null;
    int i1 = 0;
    Object localObject9 = null;
    localObject1 = ((SQLiteQueryBuilder)localObject1).query((SQLiteDatabase)localObject2, (String[])localObject5, (String)localObject6, (String[])localObject7, null, null, (String)localObject3);
    ((Cursor)localObject1).moveToFirst();
    for (;;)
    {
      boolean bool1 = ((Cursor)localObject1).isAfterLast();
      if (bool1) {
        break;
      }
      localObject2 = "_id";
      int i3 = ((Cursor)localObject1).getColumnIndexOrThrow((String)localObject2);
      i3 = ((Cursor)localObject1).getInt(i3);
      int i4 = ((Cursor)localObject1).getColumnIndexOrThrow("sender");
      localObject5 = ((Cursor)localObject1).getString(i4);
      localObject6 = Calendar.getInstance();
      m = ((Cursor)localObject1).getColumnIndexOrThrow("date");
      long l3 = ((Cursor)localObject1).getLong(m);
      ((Calendar)localObject6).setTimeInMillis(l3);
      localObject6 = ((Calendar)localObject6).getTime();
      m = ((Cursor)localObject1).getColumnIndexOrThrow("body");
      localObject7 = ((Cursor)localObject1).getString(m);
      n = ((Cursor)localObject1).getColumnIndexOrThrow("pos");
      String str2 = ((Cursor)localObject1).getString(n);
      localObject8 = "amount";
      n = ((Cursor)localObject1).getColumnIndexOrThrow((String)localObject8);
      double d1 = ((Cursor)localObject1).getDouble(n);
      localObject3 = Calendar.getInstance();
      i2 = ((Cursor)localObject1).getColumnIndexOrThrow("txnDate");
      long l4 = ((Cursor)localObject1).getLong(i2);
      ((Calendar)localObject3).setTimeInMillis(l4);
      Date localDate = ((Calendar)localObject3).getTime();
      int i6 = ((Cursor)localObject1).getColumnIndexOrThrow("type");
      int i7 = ((Cursor)localObject1).getInt(i6);
      i6 = ((Cursor)localObject1).getColumnIndexOrThrow("flags");
      i6 = ((Cursor)localObject1).getInt(i6);
      localObject11 = "accountId";
      i2 = ((Cursor)localObject1).getColumnIndexOrThrow((String)localObject11);
      i2 = ((Cursor)localObject1).getInt(i2);
      int i8 = ((Cursor)localObject1).getColumnIndex("displayPan");
      String str3 = ((Cursor)localObject1).getString(i8);
      int i9 = ((Cursor)localObject1).getColumnIndexOrThrow("name");
      Object localObject12 = ((Cursor)localObject1).getString(i9);
      str1 = "wSmsId";
      j = ((Cursor)localObject1).getColumnIndexOrThrow(str1);
      j = ((Cursor)localObject1).getInt(j);
      k = ((Cursor)localObject1).getColumnIndexOrThrow("tags");
      ((Cursor)localObject1).getString(k);
      k = ((Cursor)localObject1).getColumnIndexOrThrow("txnBalance");
      double d2 = ((Cursor)localObject1).getDouble(k);
      Object localObject13 = localObject4;
      int i10 = ((Cursor)localObject1).getColumnIndexOrThrow("txnOutstandingBalance");
      double d3 = d2;
      d2 = ((Cursor)localObject1).getDouble(i10);
      i10 = ((Cursor)localObject1).getColumnIndexOrThrow("name");
      localObject4 = ((Cursor)localObject1).getString(i10);
      double d4 = d2;
      k = ((Cursor)localObject1).getColumnIndexOrThrow("displayName");
      String str4 = ((Cursor)localObject1).getString(k);
      localObject10 = "accountFlags";
      int i11 = ((Cursor)localObject1).getColumnIndex((String)localObject10);
      int i12 = i6;
      i6 = ((Cursor)localObject1).getColumnIndexOrThrow("conversionRate");
      double d5 = d1;
      d1 = ((Cursor)localObject1).getDouble(i6);
      i6 = ((Cursor)localObject1).getColumnIndexOrThrow("currencyAmount");
      double d6 = d1;
      d1 = ((Cursor)localObject1).getDouble(i6);
      i6 = ((Cursor)localObject1).getColumnIndexOrThrow("currencySymbol");
      localObject3 = ((Cursor)localObject1).getString(i6);
      Object localObject14 = localObject3;
      i6 = -1;
      if (i11 != i6)
      {
        i6 = ((Cursor)localObject1).getInt(i11) & 0x10;
        if (i6 == 0)
        {
          i6 = 1;
        }
        else
        {
          i6 = 0;
          localObject3 = null;
        }
      }
      else
      {
        i6 = 1;
      }
      i11 = ((Cursor)localObject1).getColumnIndexOrThrow("UUID");
      localObject10 = ((Cursor)localObject1).getString(i11);
      localObject8 = new com/daamitt/prime/sdk/a/m;
      ((m)localObject8).<init>((String)localObject5, (String)localObject7, (Date)localObject6);
      l2 = i3;
      o = l2;
      w = i2;
      A = ((String)localObject4);
      y = str4;
      l2 = j;
      C = l2;
      float f1 = 4.2E-45F;
      u = 3;
      ((m)localObject8).a((String)localObject12, "Spends");
      localObject12 = Double.valueOf(d5);
      localObject11 = localObject8;
      ((m)localObject8).a(str3, (Double)localObject12, localDate, str2, i7);
      i3 = i12;
      Y = i12;
      localObject2 = new com/daamitt/prime/sdk/a/b;
      ((com.daamitt.prime.sdk.a.b)localObject2).<init>();
      double d7 = d3;
      a = d3;
      d7 = d4;
      b = d4;
      i = ((com.daamitt.prime.sdk.a.b)localObject2);
      L = i6;
      ab = ((String)localObject10);
      i3 = ((Cursor)localObject1).getColumnIndexOrThrow("merchantId");
      l2 = ((Cursor)localObject1).getLong(i3);
      k = l2;
      i3 = ((Cursor)localObject1).getColumnIndexOrThrow("categories");
      localObject2 = ((Cursor)localObject1).getString(i3);
      T = ((String)localObject2);
      i3 = ((Cursor)localObject1).getColumnIndexOrThrow("txnTags");
      localObject2 = ((Cursor)localObject1).getString(i3);
      U = ((String)localObject2);
      i3 = ((Cursor)localObject1).getColumnIndexOrThrow("txnNote");
      localObject2 = ((Cursor)localObject1).getString(i3);
      V = ((String)localObject2);
      i3 = ((Cursor)localObject1).getColumnIndexOrThrow("txnPhoto");
      localObject2 = ((Cursor)localObject1).getString(i3);
      Z = ((String)localObject2);
      i3 = ((Cursor)localObject1).getColumnIndexOrThrow("txnPhotoServerPath");
      localObject2 = ((Cursor)localObject1).getString(i3);
      aa = ((String)localObject2);
      i3 = ((Cursor)localObject1).getColumnIndexOrThrow("placeId");
      localObject2 = ((Cursor)localObject1).getString(i3);
      l = ((String)localObject2);
      i3 = ((Cursor)localObject1).getColumnIndexOrThrow("placeName");
      localObject2 = ((Cursor)localObject1).getString(i3);
      m = ((String)localObject2);
      i3 = ((Cursor)localObject1).getColumnIndexOrThrow("placeDisplayName");
      localObject2 = ((Cursor)localObject1).getString(i3);
      n = ((String)localObject2);
      double d8 = d6;
      g = d6;
      d8 = d1;
      h = d1;
      localObject2 = localObject14;
      e = ((String)localObject14);
      i3 = ((Cursor)localObject1).getColumnIndexOrThrow("recursionAccountUUID");
      localObject2 = ((Cursor)localObject1).getString(i3);
      W = ((String)localObject2);
      localObject2 = "placeLat";
      i3 = ((Cursor)localObject1).getColumnIndex((String)localObject2);
      d8 = ((Cursor)localObject1).getDouble(i3);
      localObject6 = "placeLon";
      int i13 = ((Cursor)localObject1).getColumnIndex((String)localObject6);
      double d9 = ((Cursor)localObject1).getDouble(i13);
      double d10 = 360.0D;
      boolean bool3 = d8 < d10;
      if (bool3)
      {
        bool3 = d9 < d10;
        if (bool3)
        {
          localObject9 = new android/location/Location;
          localObject3 = "walnutloc";
          ((Location)localObject9).<init>((String)localObject3);
          ((Location)localObject9).setLatitude(d8);
          ((Location)localObject9).setLongitude(d9);
          X = ((Location)localObject9);
        }
      }
      localObject2 = "locAccuracy";
      i3 = ((Cursor)localObject1).getColumnIndex((String)localObject2);
      f1 = ((Cursor)localObject1).getFloat(i3);
      float f2 = -1.0F;
      boolean bool2 = f1 < f2;
      if (bool2)
      {
        localObject5 = "lat";
        int i5 = ((Cursor)localObject1).getColumnIndex((String)localObject5);
        d7 = ((Cursor)localObject1).getDouble(i5);
        m = ((Cursor)localObject1).getColumnIndex("long");
        d10 = ((Cursor)localObject1).getDouble(m);
        localObject7 = new android/location/Location;
        localObject4 = "walnutloc";
        ((Location)localObject7).<init>((String)localObject4);
        ((Location)localObject7).setAccuracy(f1);
        ((Location)localObject7).setLatitude(d7);
        ((Location)localObject7).setLongitude(d10);
        D = ((Location)localObject7);
        localObject2 = localObject13;
      }
      else
      {
        localObject2 = localObject13;
      }
      ((ArrayList)localObject2).add(localObject8);
      ((Cursor)localObject1).moveToNext();
      localObject4 = localObject2;
      j = 0;
      str1 = null;
      k = 1;
      localObject10 = this;
    }
    localObject2 = localObject4;
    ((Cursor)localObject1).close();
    return (ArrayList)localObject4;
  }
  
  public final void a(a parama, long paramLong)
  {
    m localm = new com/daamitt/prime/sdk/a/m;
    localm.<init>(null, null, null);
    C = 0L;
    Y = 16;
    int j = a;
    w = j;
    Double localDouble = Double.valueOf(0.0D);
    Date localDate = new java/util/Date;
    localDate.<init>(paramLong);
    localm.a("DUMMY", localDouble, localDate, "DUMMY", 11);
    a(localm);
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt)
  {
    int j = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    e.a();
    boolean bool1 = paramCalendar1.before(paramCalendar2);
    if (bool1)
    {
      String[] arrayOfString = new String[j];
      String str1 = f(paramCalendar1, paramCalendar2, paramInt);
      paramCalendar2 = Calendar.getInstance().getTimeZone();
      long l1 = paramCalendar2.getRawOffset();
      int k = paramCalendar2.getDSTSavings();
      long l2 = k;
      l1 += l2;
      l2 = 3600000L;
      long l3 = l1 / l2;
      l1 %= l2;
      l2 = 60000L;
      l1 /= l2;
      paramCalendar2 = new java/lang/StringBuilder;
      paramCalendar2.<init>("strftime('%j',date(txnDate/1000,'unixepoch','");
      paramCalendar2.append(l3);
      paramCalendar2.append(" hours','");
      paramCalendar2.append(l1);
      paramCalendar2.append(" minutes')) as Day");
      paramCalendar2 = paramCalendar2.toString();
      String str2 = "Day";
      Object localObject1 = new String[2];
      paramInt = 0;
      localObject1[0] = paramCalendar2;
      int m = 1;
      localObject1[m] = "txnBalance";
      String str3 = " MAX(txnDate)";
      Object localObject2 = e;
      String str4 = "transactions";
      paramCalendar2 = ((SQLiteDatabase)localObject2).query(str4, (String[])localObject1, str1, null, str2, str3, null, null);
      if (paramCalendar2 != null)
      {
        int n = paramCalendar2.getCount();
        if (n > 0)
        {
          paramCalendar2.moveToFirst();
          n = 6;
          int i2 = paramCalendar1.get(n);
          int i3 = paramCalendar1.getActualMaximum(n);
          for (;;)
          {
            boolean bool2 = paramCalendar2.isAfterLast();
            if (bool2) {
              break;
            }
            int i1 = paramCalendar2.getColumnIndexOrThrow("Day");
            localObject2 = paramCalendar2.getString(i1);
            int i4 = paramCalendar2.getColumnIndexOrThrow("txnBalance");
            localObject1 = paramCalendar2.getString(i4);
            int i5 = -1;
            try
            {
              i1 = Integer.parseInt((String)localObject2);
            }
            catch (NumberFormatException localNumberFormatException)
            {
              i1 = -1;
            }
            if ((i1 != i5) && (localObject1 != null))
            {
              localObject1 = Double.valueOf((String)localObject1);
              double d1 = ((Double)localObject1).doubleValue();
              i4 = (int)d1;
              if (i1 >= i2)
              {
                i1 -= i2;
                i1 = j - i1 - m;
                localObject1 = String.valueOf(i4);
                arrayOfString[i1] = localObject1;
              }
              else
              {
                i5 = i3 - i2 + i1;
                i1 = j - i5 - m;
                localObject1 = String.valueOf(i4);
                arrayOfString[i1] = localObject1;
              }
            }
            paramCalendar2.moveToNext();
          }
        }
      }
      if (paramCalendar2 != null) {
        paramCalendar2.close();
      }
      while (paramInt < j)
      {
        paramCalendar1 = arrayOfString[paramInt];
        boolean bool3 = TextUtils.isEmpty(paramCalendar1);
        if (bool3)
        {
          paramCalendar1 = "";
          arrayOfString[paramInt] = paramCalendar1;
        }
        paramInt += 1;
      }
      return arrayOfString;
    }
    return null;
  }
  
  public final String[] a(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt, String paramString1, String paramString2)
  {
    Object localObject1 = paramCalendar1;
    int j = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    e.a();
    boolean bool1 = paramCalendar1.before(paramCalendar2);
    if (bool1)
    {
      String[] arrayOfString1 = new String[j];
      localObject2 = this;
      Object localObject3 = paramCalendar2;
      Object localObject4 = paramString1;
      int m = paramInt;
      String str1 = a(paramCalendar1, paramCalendar2, paramString1, paramInt, paramString2);
      localObject2 = Calendar.getInstance().getTimeZone();
      long l1 = ((TimeZone)localObject2).getRawOffset();
      int k = ((TimeZone)localObject2).getDSTSavings();
      long l2 = k;
      l1 += l2;
      l2 = 3600000L;
      long l3 = l1 / l2;
      l1 %= l2;
      l2 = 60000L;
      l1 /= l2;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("strftime('%j',date(txnDate/1000,'unixepoch','");
      ((StringBuilder)localObject2).append(l3);
      ((StringBuilder)localObject2).append(" hours','");
      ((StringBuilder)localObject2).append(l1);
      ((StringBuilder)localObject2).append(" minutes')) as Day");
      localObject2 = ((StringBuilder)localObject2).toString();
      String str2 = "Day";
      String[] arrayOfString2 = new String[2];
      int n = 0;
      arrayOfString2[0] = localObject2;
      localObject2 = f;
      int i1 = 1;
      arrayOfString2[i1] = localObject2;
      localObject2 = this;
      Object localObject5 = e;
      String str3 = g;
      localObject4 = ((SQLiteDatabase)localObject5).query(str3, arrayOfString2, str1, null, str2, null, null, null);
      if (localObject4 != null)
      {
        m = ((Cursor)localObject4).getCount();
        if (m > 0)
        {
          ((Cursor)localObject4).moveToFirst();
          m = 6;
          int i2 = paramCalendar1.get(m);
          m = paramCalendar1.getActualMaximum(m);
          for (;;)
          {
            boolean bool3 = ((Cursor)localObject4).isAfterLast();
            if (bool3) {
              break;
            }
            int i3 = ((Cursor)localObject4).getColumnIndexOrThrow("Day");
            localObject1 = ((Cursor)localObject4).getString(i3);
            int i4 = ((Cursor)localObject4).getColumnIndexOrThrow("Amount");
            localObject5 = ((Cursor)localObject4).getString(i4);
            int i5 = -1;
            try
            {
              i3 = Integer.parseInt((String)localObject1);
            }
            catch (NumberFormatException localNumberFormatException)
            {
              i3 = -1;
            }
            if ((i3 != i5) && (localObject5 != null))
            {
              localObject5 = Double.valueOf((String)localObject5);
              double d1 = ((Double)localObject5).doubleValue();
              i4 = (int)d1;
              if (i3 >= i2)
              {
                i3 -= i2;
                i3 = j - i3 - i1;
                localObject5 = String.valueOf(i4);
                arrayOfString1[i3] = localObject5;
              }
              else
              {
                i5 = m - i2 + i3;
                i3 = j - i5 - i1;
                localObject5 = String.valueOf(i4);
                arrayOfString1[i3] = localObject5;
              }
            }
            ((Cursor)localObject4).moveToNext();
          }
        }
      }
      if (localObject4 != null) {
        ((Cursor)localObject4).close();
      }
      while (n < j)
      {
        localObject3 = arrayOfString1[n];
        boolean bool2 = TextUtils.isEmpty((CharSequence)localObject3);
        if (bool2)
        {
          localObject3 = "";
          arrayOfString1[n] = localObject3;
        }
        n += 1;
      }
      return arrayOfString1;
    }
    Object localObject2 = this;
    return null;
  }
  
  public final String[] b(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt)
  {
    Object localObject1 = paramCalendar1;
    int j = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    e.a();
    boolean bool1 = paramCalendar1.before(paramCalendar2);
    if (bool1)
    {
      String[] arrayOfString1 = new String[j];
      int k = 1;
      Object localObject2 = new int[k];
      int m = 0;
      localObject2[0] = paramInt;
      Object localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("transactions.type in ( ");
      Object localObject4 = m.f();
      ((StringBuilder)localObject3).append((String)localObject4);
      ((StringBuilder)localObject3).append("  )  and txnDate between ");
      long l1 = paramCalendar1.getTime().getTime();
      ((StringBuilder)localObject3).append(l1);
      ((StringBuilder)localObject3).append(" AND ");
      l1 = paramCalendar2.getTime().getTime();
      ((StringBuilder)localObject3).append(l1);
      ((StringBuilder)localObject3).append(" AND txnBalance NOTNULL  AND accountId IN (");
      localObject2 = a.c((int[])localObject2);
      ((StringBuilder)localObject3).append((String)localObject2);
      ((StringBuilder)localObject3).append(")");
      String str1 = ((StringBuilder)localObject3).toString();
      localObject2 = Calendar.getInstance().getTimeZone();
      long l2 = ((TimeZone)localObject2).getRawOffset();
      int n = ((TimeZone)localObject2).getDSTSavings();
      long l3 = n;
      l2 += l3;
      l3 = 3600000L;
      long l4 = l2 / l3;
      l2 %= l3;
      l3 = 60000L;
      double d1 = 2.9644E-319D;
      l2 /= l3;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("strftime('%j',date(txnDate/1000,'unixepoch','");
      ((StringBuilder)localObject2).append(l4);
      ((StringBuilder)localObject2).append(" hours','");
      ((StringBuilder)localObject2).append(l2);
      localObject3 = " minutes')) as Day";
      ((StringBuilder)localObject2).append((String)localObject3);
      localObject2 = ((StringBuilder)localObject2).toString();
      String str2 = "Day";
      int i1 = 2;
      String[] arrayOfString2 = new String[i1];
      arrayOfString2[0] = localObject2;
      arrayOfString2[k] = "txnBalance";
      String str3 = " MIN(txnBalance)";
      localObject4 = e;
      Object localObject5 = "transactions";
      localObject2 = ((SQLiteDatabase)localObject4).query((String)localObject5, arrayOfString2, str1, null, str2, str3, null, null);
      if (localObject2 != null)
      {
        i1 = ((Cursor)localObject2).getCount();
        if (i1 > 0)
        {
          ((Cursor)localObject2).moveToFirst();
          i1 = 6;
          int i3 = paramCalendar1.get(i1);
          int i4 = paramCalendar1.getActualMaximum(i1);
          for (;;)
          {
            boolean bool2 = ((Cursor)localObject2).isAfterLast();
            if (bool2) {
              break;
            }
            int i2 = ((Cursor)localObject2).getColumnIndexOrThrow("Day");
            localObject3 = ((Cursor)localObject2).getString(i2);
            int i5 = ((Cursor)localObject2).getColumnIndexOrThrow("txnBalance");
            localObject5 = ((Cursor)localObject2).getString(i5);
            int i6 = -1;
            try
            {
              i2 = Integer.parseInt((String)localObject3);
            }
            catch (NumberFormatException localNumberFormatException)
            {
              i2 = -1;
            }
            if ((i2 != i6) && (localObject5 != null))
            {
              localObject5 = Double.valueOf((String)localObject5);
              d1 = ((Double)localObject5).doubleValue();
              i5 = (int)d1;
              if (i2 >= i3)
              {
                i2 -= i3;
                i2 = j - i2 - k;
                localObject5 = String.valueOf(i5);
                arrayOfString1[i2] = localObject5;
              }
              else
              {
                i6 = i4 - i3 + i2;
                i2 = j - i6 - k;
                localObject5 = String.valueOf(i5);
                arrayOfString1[i2] = localObject5;
              }
            }
            ((Cursor)localObject2).moveToNext();
          }
        }
      }
      if (localObject2 != null) {
        ((Cursor)localObject2).close();
      }
      while (m < j)
      {
        localObject1 = arrayOfString1[m];
        boolean bool3 = TextUtils.isEmpty((CharSequence)localObject1);
        if (bool3)
        {
          localObject1 = "";
          arrayOfString1[m] = localObject1;
        }
        m += 1;
      }
      return arrayOfString1;
    }
    return null;
  }
  
  public final String[][] c(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt)
  {
    int j = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    e.a();
    boolean bool1 = paramCalendar1.before(paramCalendar2);
    if (bool1)
    {
      int k = 2;
      Object localObject1 = { k, j };
      localObject1 = (String[][])Array.newInstance(String.class, (int[])localObject1);
      Object localObject2 = new android/database/sqlite/SQLiteQueryBuilder;
      ((SQLiteQueryBuilder)localObject2).<init>();
      String str1 = g;
      ((SQLiteQueryBuilder)localObject2).setTables(str1);
      Object localObject3 = f(paramCalendar1, paramCalendar2, paramInt);
      localObject2 = Calendar.getInstance().getTimeZone();
      long l1 = ((TimeZone)localObject2).getRawOffset();
      int m = ((TimeZone)localObject2).getDSTSavings();
      long l2 = m;
      l1 += l2;
      l2 = 3600000L;
      long l3 = l1 / l2;
      l1 %= l2;
      l2 = 60000L;
      l1 /= l2;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("strftime('%j',date(txnDate/1000,'unixepoch','");
      ((StringBuilder)localObject2).append(l3);
      ((StringBuilder)localObject2).append(" hours','");
      ((StringBuilder)localObject2).append(l1);
      ((StringBuilder)localObject2).append(" minutes')) as Day");
      localObject2 = ((StringBuilder)localObject2).toString();
      String str2 = "Day, accountId";
      Object localObject4 = new String[3];
      str1 = null;
      localObject4[0] = localObject2;
      int n = 1;
      localObject4[n] = "txnBalance";
      localObject4[k] = "txnOutstandingBalance";
      Object localObject5 = e;
      String str3 = g;
      int i1 = 0;
      Object localObject6 = null;
      boolean bool3 = false;
      localObject2 = ((SQLiteDatabase)localObject5).query(str3, (String[])localObject4, (String)localObject3, null, str2, null, null, null);
      if (localObject2 != null)
      {
        int i2 = ((Cursor)localObject2).getCount();
        if (i2 > 0)
        {
          ((Cursor)localObject2).moveToFirst();
          i2 = 6;
          int i4 = paramCalendar1.get(i2);
          i5 = paramCalendar1.getActualMaximum(i2);
          for (;;)
          {
            boolean bool4 = ((Cursor)localObject2).isAfterLast();
            if (bool4) {
              break;
            }
            int i3 = ((Cursor)localObject2).getColumnIndexOrThrow("Day");
            localObject5 = ((Cursor)localObject2).getString(i3);
            int i6 = ((Cursor)localObject2).getColumnIndexOrThrow("txnBalance");
            localObject4 = ((Cursor)localObject2).getString(i6);
            int i7 = ((Cursor)localObject2).getColumnIndexOrThrow("txnOutstandingBalance");
            localObject3 = ((Cursor)localObject2).getString(i7);
            i1 = -1;
            try
            {
              i3 = Integer.parseInt((String)localObject5);
            }
            catch (NumberFormatException localNumberFormatException)
            {
              i3 = -1;
            }
            if ((i3 != i1) && (localObject4 != null) && (localObject3 != null))
            {
              localObject4 = Double.valueOf((String)localObject4);
              double d1 = ((Double)localObject4).doubleValue();
              i6 = (int)d1;
              localObject3 = Double.valueOf((String)localObject3);
              double d2 = ((Double)localObject3).doubleValue();
              double d3 = Double.MIN_VALUE;
              bool3 = d2 < d3;
              if (bool3)
              {
                i7 = (int)d2;
                if (i3 >= i4)
                {
                  localObject6 = localObject1[0];
                  i3 -= i4;
                  i3 = j - i3 - n;
                  localObject4 = String.valueOf(i6);
                  localObject6[i3] = localObject4;
                  localObject4 = localObject1[n];
                  localObject3 = String.valueOf(i7);
                  localObject4[i3] = localObject3;
                }
                else
                {
                  localObject6 = localObject1[0];
                  int i8 = i5 - i4 + i3;
                  i3 = j - i8 - n;
                  localObject4 = String.valueOf(i6);
                  localObject6[i3] = localObject4;
                  localObject4 = localObject1[n];
                  localObject3 = String.valueOf(i7);
                  localObject4[i3] = localObject3;
                }
              }
            }
            ((Cursor)localObject2).moveToNext();
          }
        }
      }
      if (localObject2 != null) {
        ((Cursor)localObject2).close();
      }
      int i5 = 0;
      while (i5 < j)
      {
        localObject2 = localObject1[0][i5];
        localObject5 = localObject1[n][i5];
        boolean bool2 = TextUtils.isEmpty((CharSequence)localObject2);
        if (bool2)
        {
          localObject2 = localObject1[0];
          str3 = "";
          localObject2[i5] = str3;
        }
        bool2 = TextUtils.isEmpty((CharSequence)localObject5);
        if (bool2)
        {
          localObject2 = localObject1[n];
          localObject5 = "";
          localObject2[i5] = localObject5;
        }
        i5 += 1;
      }
      return (String[][])localObject1;
    }
    return null;
  }
  
  public final String[] d(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt)
  {
    int j = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    e.a();
    boolean bool1 = paramCalendar1.before(paramCalendar2);
    if (bool1)
    {
      String[] arrayOfString = new String[j];
      int k = 0;
      Calendar localCalendar = null;
      String str = m.d();
      Object localObject1 = this;
      Object localObject2 = paramCalendar1;
      Object localObject3 = paramCalendar2;
      paramCalendar2 = a(paramCalendar1, paramCalendar2, null, 0, str);
      if (paramInt != 0)
      {
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        ((StringBuilder)localObject1).append(paramCalendar2);
        ((StringBuilder)localObject1).append(" AND transactions.flags & ");
        ((StringBuilder)localObject1).append(paramInt);
        ((StringBuilder)localObject1).append(" != 0 ");
        paramCalendar2 = ((StringBuilder)localObject1).toString();
        localCalendar = paramCalendar2;
      }
      else
      {
        localCalendar = paramCalendar2;
      }
      paramCalendar2 = Calendar.getInstance().getTimeZone();
      long l1 = paramCalendar2.getRawOffset();
      int m = paramCalendar2.getDSTSavings();
      long l2 = m;
      l1 += l2;
      l2 = 3600000L;
      long l3 = l1 / l2;
      l1 %= l2;
      l2 = 60000L;
      l1 /= l2;
      paramCalendar2 = new java/lang/StringBuilder;
      paramCalendar2.<init>("strftime('%j',date(txnDate/1000,'unixepoch','");
      paramCalendar2.append(l3);
      paramCalendar2.append(" hours','");
      paramCalendar2.append(l1);
      paramCalendar2.append(" minutes')) as Day");
      paramCalendar2 = paramCalendar2.toString();
      str = "Day";
      localObject3 = new String[2];
      paramInt = 0;
      localObject3[0] = paramCalendar2;
      paramCalendar2 = f;
      int n = 1;
      localObject3[n] = paramCalendar2;
      localObject1 = e;
      localObject2 = g;
      paramCalendar2 = ((SQLiteDatabase)localObject1).query((String)localObject2, (String[])localObject3, localCalendar, null, str, null, null, null);
      if (paramCalendar2 != null)
      {
        int i1 = paramCalendar2.getCount();
        if (i1 > 0)
        {
          paramCalendar2.moveToFirst();
          i1 = 6;
          int i3 = paramCalendar1.get(i1);
          int i4 = paramCalendar1.getActualMaximum(i1);
          for (;;)
          {
            boolean bool2 = paramCalendar2.isAfterLast();
            if (bool2) {
              break;
            }
            int i2 = paramCalendar2.getColumnIndexOrThrow("Day");
            localObject1 = paramCalendar2.getString(i2);
            int i5 = paramCalendar2.getColumnIndexOrThrow("Amount");
            localObject3 = paramCalendar2.getString(i5);
            k = -1;
            try
            {
              i2 = Integer.parseInt((String)localObject1);
            }
            catch (NumberFormatException localNumberFormatException)
            {
              i2 = -1;
            }
            if ((i2 != k) && (localObject3 != null))
            {
              localObject3 = Double.valueOf((String)localObject3);
              double d1 = ((Double)localObject3).doubleValue();
              i5 = (int)d1;
              if (i2 >= i3)
              {
                i2 -= i3;
                i2 = j - i2 - n;
                localObject3 = String.valueOf(i5);
                arrayOfString[i2] = localObject3;
              }
              else
              {
                k = i4 - i3 + i2;
                i2 = j - k - n;
                localObject3 = String.valueOf(i5);
                arrayOfString[i2] = localObject3;
              }
            }
            paramCalendar2.moveToNext();
          }
        }
      }
      if (paramCalendar2 != null) {
        paramCalendar2.close();
      }
      while (paramInt < j)
      {
        paramCalendar1 = arrayOfString[paramInt];
        boolean bool3 = TextUtils.isEmpty(paramCalendar1);
        if (bool3)
        {
          paramCalendar1 = "";
          arrayOfString[paramInt] = paramCalendar1;
        }
        paramInt += 1;
      }
      return arrayOfString;
    }
    return null;
  }
  
  public final String[] e(Calendar paramCalendar1, Calendar paramCalendar2, int paramInt)
  {
    Object localObject1 = paramCalendar1;
    int j = i.a.a(paramCalendar1, paramCalendar2);
    paramCalendar1.getTime();
    paramCalendar2.getTime();
    e.a();
    boolean bool1 = paramCalendar1.before(paramCalendar2);
    if (bool1)
    {
      String[] arrayOfString1 = new String[j];
      int k = 1;
      Object localObject2 = new int[k];
      int m = 0;
      localObject2[0] = paramInt;
      Object localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("transactions.type = 17 AND transactions.flags & 2097152 != 0  AND txnDate between ");
      long l1 = paramCalendar1.getTime().getTime();
      ((StringBuilder)localObject3).append(l1);
      ((StringBuilder)localObject3).append(" AND ");
      l1 = paramCalendar2.getTime().getTime();
      ((StringBuilder)localObject3).append(l1);
      ((StringBuilder)localObject3).append(" AND accountId IN (");
      localObject2 = a.c((int[])localObject2);
      ((StringBuilder)localObject3).append((String)localObject2);
      ((StringBuilder)localObject3).append(")");
      String str1 = ((StringBuilder)localObject3).toString();
      localObject2 = Calendar.getInstance().getTimeZone();
      long l2 = ((TimeZone)localObject2).getRawOffset();
      int n = ((TimeZone)localObject2).getDSTSavings();
      long l3 = n;
      l2 += l3;
      l3 = 3600000L;
      long l4 = l2 / l3;
      l2 %= l3;
      l3 = 60000L;
      double d1 = 2.9644E-319D;
      l2 /= l3;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("strftime('%j',date(txnDate/1000,'unixepoch','");
      ((StringBuilder)localObject2).append(l4);
      ((StringBuilder)localObject2).append(" hours','");
      ((StringBuilder)localObject2).append(l2);
      localObject3 = " minutes')) as Day";
      ((StringBuilder)localObject2).append((String)localObject3);
      localObject2 = ((StringBuilder)localObject2).toString();
      String str2 = "Day";
      int i1 = 2;
      String[] arrayOfString2 = new String[i1];
      arrayOfString2[0] = localObject2;
      arrayOfString2[k] = "amount";
      String str3 = " MAX(amount)";
      SQLiteDatabase localSQLiteDatabase = e;
      Object localObject4 = "transactions";
      localObject2 = localSQLiteDatabase.query((String)localObject4, arrayOfString2, str1, null, str2, str3, null, null);
      if (localObject2 != null)
      {
        i1 = ((Cursor)localObject2).getCount();
        if (i1 > 0)
        {
          ((Cursor)localObject2).moveToFirst();
          i1 = 6;
          int i3 = paramCalendar1.get(i1);
          int i4 = paramCalendar1.getActualMaximum(i1);
          for (;;)
          {
            boolean bool2 = ((Cursor)localObject2).isAfterLast();
            if (bool2) {
              break;
            }
            int i2 = ((Cursor)localObject2).getColumnIndexOrThrow("Day");
            localObject3 = ((Cursor)localObject2).getString(i2);
            int i5 = ((Cursor)localObject2).getColumnIndexOrThrow("amount");
            localObject4 = ((Cursor)localObject2).getString(i5);
            int i6 = -1;
            try
            {
              i2 = Integer.parseInt((String)localObject3);
            }
            catch (NumberFormatException localNumberFormatException)
            {
              i2 = -1;
            }
            if ((i2 != i6) && (localObject4 != null))
            {
              localObject4 = Double.valueOf((String)localObject4);
              d1 = ((Double)localObject4).doubleValue();
              i5 = (int)d1;
              if (i2 >= i3)
              {
                i2 -= i3;
                i2 = j - i2 - k;
                localObject4 = String.valueOf(i5);
                arrayOfString1[i2] = localObject4;
              }
              else
              {
                i6 = i4 - i3 + i2;
                i2 = j - i6 - k;
                localObject4 = String.valueOf(i5);
                arrayOfString1[i2] = localObject4;
              }
            }
            ((Cursor)localObject2).moveToNext();
          }
        }
      }
      if (localObject2 != null) {
        ((Cursor)localObject2).close();
      }
      while (m < j)
      {
        localObject1 = arrayOfString1[m];
        boolean bool3 = TextUtils.isEmpty((CharSequence)localObject1);
        if (bool3)
        {
          localObject1 = "";
          arrayOfString1[m] = localObject1;
        }
        m += 1;
      }
      return arrayOfString1;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.b.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */