package com.daamitt.prime.sdk.b;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.daamitt.prime.sdk.a.e;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class a
{
  private static String c = "a";
  private static a e;
  SQLiteDatabase a = null;
  public b b;
  private String[] d;
  
  private a(b paramb)
  {
    String[] tmp14_11 = new String[21];
    String[] tmp15_14 = tmp14_11;
    String[] tmp15_14 = tmp14_11;
    tmp15_14[0] = "_id";
    tmp15_14[1] = "name";
    String[] tmp24_15 = tmp15_14;
    String[] tmp24_15 = tmp15_14;
    tmp24_15[2] = "displayName";
    tmp24_15[3] = "pan";
    String[] tmp33_24 = tmp24_15;
    String[] tmp33_24 = tmp24_15;
    tmp33_24[4] = "displayPan";
    tmp33_24[5] = "type";
    String[] tmp42_33 = tmp33_24;
    String[] tmp42_33 = tmp33_24;
    tmp42_33[6] = "flags";
    tmp42_33[7] = "startDate";
    String[] tmp53_42 = tmp42_33;
    String[] tmp53_42 = tmp42_33;
    tmp53_42[8] = "endDate";
    tmp53_42[9] = "enabled";
    String[] tmp64_53 = tmp53_42;
    String[] tmp64_53 = tmp53_42;
    tmp64_53[10] = "UUID";
    tmp64_53[11] = "modifyCount";
    String[] tmp75_64 = tmp64_53;
    String[] tmp75_64 = tmp64_53;
    tmp75_64[12] = "MUUID";
    tmp75_64[13] = "balance";
    String[] tmp86_75 = tmp75_64;
    String[] tmp86_75 = tmp75_64;
    tmp86_75[14] = "outstandingBalance";
    tmp86_75[15] = "balLastSyncTime";
    String[] tmp97_86 = tmp86_75;
    String[] tmp97_86 = tmp86_75;
    tmp97_86[16] = "outBalLastSyncTime";
    tmp97_86[17] = "updatedTime";
    String[] tmp108_97 = tmp97_86;
    String[] tmp108_97 = tmp97_86;
    tmp108_97[18] = "accountColor";
    tmp108_97[19] = "cardIssuer";
    tmp108_97[20] = "recursionFlag";
    Object localObject = tmp108_97;
    d = ((String[])localObject);
    localObject = paramb;
    b = paramb;
  }
  
  private static com.daamitt.prime.sdk.a.a a(Cursor paramCursor)
  {
    int i = paramCursor.getColumnIndexOrThrow("name");
    Object localObject = paramCursor.getString(i);
    int j = paramCursor.getColumnIndexOrThrow("pan");
    String str1 = paramCursor.getString(j);
    String str2 = "type";
    int k = paramCursor.getColumnIndexOrThrow(str2);
    k = paramCursor.getInt(k);
    com.daamitt.prime.sdk.a.a locala = new com/daamitt/prime/sdk/a/a;
    locala.<init>((String)localObject, str1, k);
    i = paramCursor.getColumnIndexOrThrow("displayName");
    localObject = paramCursor.getString(i);
    e = ((String)localObject);
    i = paramCursor.getColumnIndexOrThrow("displayPan");
    localObject = paramCursor.getString(i);
    g = ((String)localObject);
    i = paramCursor.getColumnIndexOrThrow("_id");
    i = paramCursor.getInt(i);
    a = i;
    i = paramCursor.getColumnIndexOrThrow("flags");
    i = paramCursor.getInt(i);
    l = i;
    i = paramCursor.getColumnIndexOrThrow("UUID");
    localObject = paramCursor.getString(i);
    b = ((String)localObject);
    i = paramCursor.getColumnIndexOrThrow("MUUID");
    localObject = paramCursor.getString(i);
    c = ((String)localObject);
    localObject = "enabled";
    i = paramCursor.getColumnIndexOrThrow((String)localObject);
    i = paramCursor.getInt(i);
    j = 1;
    if (i != j)
    {
      j = 0;
      str1 = null;
    }
    k = j;
    i = paramCursor.getColumnIndexOrThrow("startDate");
    i = paramCursor.getInt(i);
    p = i;
    i = paramCursor.getColumnIndexOrThrow("endDate");
    i = paramCursor.getInt(i);
    q = i;
    i = paramCursor.getColumnIndexOrThrow("updatedTime");
    long l = paramCursor.getLong(i);
    j = l;
    localObject = b(paramCursor);
    m = ((com.daamitt.prime.sdk.a.b)localObject);
    i = paramCursor.getColumnIndexOrThrow("accountColor");
    i = paramCursor.getInt(i);
    n = i;
    i = paramCursor.getColumnIndexOrThrow("UUID");
    localObject = paramCursor.getString(i);
    b = ((String)localObject);
    i = paramCursor.getColumnIndexOrThrow("cardIssuer");
    localObject = paramCursor.getString(i);
    h = ((String)localObject);
    i = paramCursor.getColumnIndexOrThrow("recursionFlag");
    int m = paramCursor.getInt(i);
    r = m;
    return locala;
  }
  
  public static a a(b paramb)
  {
    a locala = e;
    if (locala == null)
    {
      locala = new com/daamitt/prime/sdk/b/a;
      locala.<init>(paramb);
      e = locala;
      paramb = paramb.getWritableDatabase();
      a = paramb;
    }
    return e;
  }
  
  static String a(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt != null)
    {
      int i = paramArrayOfInt.length;
      if (i > 0)
      {
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        int j = paramArrayOfInt.length * 2;
        int k = 1;
        j -= k;
        localStringBuilder.<init>(j);
        Object localObject = null;
        j = paramArrayOfInt[0];
        localStringBuilder.append(j);
        for (;;)
        {
          j = paramArrayOfInt.length;
          if (k >= j) {
            break;
          }
          localObject = new java/lang/StringBuilder;
          String str = ",";
          ((StringBuilder)localObject).<init>(str);
          int m = paramArrayOfInt[k];
          ((StringBuilder)localObject).append(m);
          localObject = ((StringBuilder)localObject).toString();
          localStringBuilder.append((String)localObject);
          k += 1;
        }
        return localStringBuilder.toString();
      }
    }
    return null;
  }
  
  static String a(String[] paramArrayOfString)
  {
    if (paramArrayOfString != null)
    {
      int i = paramArrayOfString.length;
      if (i > 0)
      {
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        int j = paramArrayOfString.length * 2;
        int k = 1;
        j -= k;
        localStringBuilder.<init>(j);
        j = 0;
        Object localObject = paramArrayOfString[0];
        localStringBuilder.append((String)localObject);
        for (;;)
        {
          j = paramArrayOfString.length;
          if (k >= j) {
            break;
          }
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>(",");
          String str = paramArrayOfString[k];
          ((StringBuilder)localObject).append(str);
          localObject = ((StringBuilder)localObject).toString();
          localStringBuilder.append((String)localObject);
          k += 1;
        }
        return localStringBuilder.toString();
      }
    }
    return null;
  }
  
  public static void a(ContentValues paramContentValues, com.daamitt.prime.sdk.a.b paramb)
  {
    if (paramb != null)
    {
      double d1 = a;
      double d2 = Double.MIN_VALUE;
      boolean bool = d1 < d2;
      Object localObject1;
      Object localObject2;
      if (bool)
      {
        double d3 = a;
        localObject1 = Double.valueOf(d3);
        paramContentValues.put("balance", (Double)localObject1);
        localObject2 = c;
        if (localObject2 != null)
        {
          localObject2 = "balLastSyncTime";
          long l1 = c.getTime();
          localObject1 = Long.valueOf(l1);
          paramContentValues.put((String)localObject2, (Long)localObject1);
        }
      }
      d1 = b;
      bool = d1 < d2;
      if (bool)
      {
        double d4 = b;
        localObject1 = Double.valueOf(d4);
        paramContentValues.put("outstandingBalance", (Double)localObject1);
        localObject2 = d;
        if (localObject2 != null)
        {
          localObject2 = "outBalLastSyncTime";
          long l2 = d.getTime();
          paramb = Long.valueOf(l2);
          paramContentValues.put((String)localObject2, paramb);
        }
      }
    }
  }
  
  public static void a(SQLiteDatabase paramSQLiteDatabase)
  {
    e.b();
    paramSQLiteDatabase.execSQL("create table if not exists accounts(_id integer primary key autoincrement, name text not null, displayName text, pan text not null,displayPan text,type integer not null, flags integer default 0,startDate integer,endDate integer,enabled boolean default 1,UUID text,modifyCount integer default 1,MUUID text,balance real,outstandingBalance real,balLastSyncTime integer, outBalLastSyncTime integer, updatedTime integer,accountColor integer,cardIssuer text,recursionFlag int default -1);");
    paramSQLiteDatabase.execSQL("create trigger if not exists AccountsTriggerModifiedFlag After update on accounts for each row  Begin  Update accounts Set modifyCount = modifyCount + 1  Where _id =  New._id;  End; ");
    paramSQLiteDatabase.execSQL("PRAGMA recursive_triggers = false;");
  }
  
  private static com.daamitt.prime.sdk.a.b b(Cursor paramCursor)
  {
    com.daamitt.prime.sdk.a.b localb = new com/daamitt/prime/sdk/a/b;
    localb.<init>();
    int i = paramCursor.getColumnIndexOrThrow("balance");
    double d1 = paramCursor.getDouble(i);
    a = d1;
    i = paramCursor.getColumnIndexOrThrow("outstandingBalance");
    d1 = paramCursor.getDouble(i);
    b = d1;
    String str1 = "balLastSyncTime";
    i = paramCursor.getColumnIndexOrThrow(str1);
    long l1 = paramCursor.getLong(i);
    String str2 = "outBalLastSyncTime";
    int j = paramCursor.getColumnIndexOrThrow(str2);
    long l2 = paramCursor.getLong(j);
    long l3 = 0L;
    boolean bool = l1 < l3;
    if (bool)
    {
      paramCursor = Calendar.getInstance();
      paramCursor.setTimeInMillis(l1);
      paramCursor = paramCursor.getTime();
      c = paramCursor;
    }
    bool = l2 < l3;
    if (bool)
    {
      paramCursor = Calendar.getInstance();
      paramCursor.setTimeInMillis(l2);
      paramCursor = paramCursor.getTime();
      d = paramCursor;
    }
    return localb;
  }
  
  private String b(String paramString)
  {
    Object localObject1 = a;
    Object localObject2 = "accounts";
    Object localObject3 = { "MUUID" };
    Object localObject4 = new java/lang/StringBuilder;
    ((StringBuilder)localObject4).<init>("UUID = '");
    ((StringBuilder)localObject4).append(paramString);
    ((StringBuilder)localObject4).append("'");
    localObject4 = ((StringBuilder)localObject4).toString();
    localObject1 = ((SQLiteDatabase)localObject1).query((String)localObject2, (String[])localObject3, (String)localObject4, null, null, null, null);
    if (localObject1 != null)
    {
      int i = ((Cursor)localObject1).getCount();
      if (i > 0)
      {
        localObject2 = new java/util/ArrayList;
        ((ArrayList)localObject2).<init>();
        for (;;)
        {
          boolean bool = ((Cursor)localObject1).moveToNext();
          if (!bool) {
            break;
          }
          int j = ((Cursor)localObject1).getColumnIndex("MUUID");
          localObject3 = ((Cursor)localObject1).getString(j);
          ((ArrayList)localObject2).add(localObject3);
          int k = 1;
          a(k, (String)localObject3, (ArrayList)localObject2);
        }
      }
    }
    if (localObject1 != null) {
      ((Cursor)localObject1).close();
    }
    return paramString;
  }
  
  public static void b(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.beginTransaction();
    String str = "drop table if exists accounts";
    try
    {
      paramSQLiteDatabase.execSQL(str);
      str = "drop trigger if exists AccountsTriggerModifiedFlag";
      paramSQLiteDatabase.execSQL(str);
      a(paramSQLiteDatabase);
      paramSQLiteDatabase.setTransactionSuccessful();
      return;
    }
    finally
    {
      paramSQLiteDatabase.endTransaction();
    }
  }
  
  private String d(long paramLong)
  {
    String str1 = "_id = ";
    Object localObject = String.valueOf(paramLong);
    String str2 = str1.concat((String)localObject);
    SQLiteDatabase localSQLiteDatabase = a;
    String str3 = "accounts";
    String[] arrayOfString = { "MUUID" };
    localObject = localSQLiteDatabase.query(str3, arrayOfString, str2, null, null, null, null);
    ((Cursor)localObject).moveToFirst();
    boolean bool = ((Cursor)localObject).isAfterLast();
    int i;
    String str4;
    if (!bool)
    {
      i = ((Cursor)localObject).getColumnIndexOrThrow("MUUID");
      str4 = ((Cursor)localObject).getString(i);
    }
    else
    {
      i = 0;
      str4 = null;
    }
    ((Cursor)localObject).close();
    return str4;
  }
  
  public final int a(com.daamitt.prime.sdk.a.a parama, ContentValues paramContentValues)
  {
    int i = a;
    if (i >= 0)
    {
      SQLiteDatabase localSQLiteDatabase = a;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>("_id = ");
      int j = a;
      localStringBuilder.append(j);
      parama = localStringBuilder.toString();
      return localSQLiteDatabase.update("accounts", paramContentValues, parama, null);
    }
    return -1;
  }
  
  public final com.daamitt.prime.sdk.a.a a(String paramString1, String paramString2, int paramInt)
  {
    return a(paramString1, paramString2, paramInt, true, null);
  }
  
  public final com.daamitt.prime.sdk.a.a a(String paramString1, String paramString2, int paramInt, boolean paramBoolean, String paramString3)
  {
    SQLiteDatabase localSQLiteDatabase = a;
    localSQLiteDatabase.beginTransaction();
    try
    {
      Object localObject = a;
      String str1 = "accounts";
      String[] arrayOfString1 = d;
      String str2 = "name=? AND pan=?";
      int i = 2;
      String[] arrayOfString2 = new String[i];
      i = 0;
      localSQLiteDatabase = null;
      arrayOfString2[0] = paramString1;
      i = 1;
      arrayOfString2[i] = paramString2;
      localObject = ((SQLiteDatabase)localObject).query(str1, arrayOfString1, str2, arrayOfString2, null, null, null);
      if (localObject != null)
      {
        int j = ((Cursor)localObject).getCount();
        if (j > 0)
        {
          ((Cursor)localObject).moveToFirst();
          paramString1 = a((Cursor)localObject);
          ((Cursor)localObject).close();
          return paramString1;
        }
      }
      if (localObject != null) {
        ((Cursor)localObject).close();
      }
      localObject = new android/content/ContentValues;
      ((ContentValues)localObject).<init>();
      str1 = "name";
      ((ContentValues)localObject).put(str1, paramString1);
      str1 = "displayName";
      boolean bool2 = TextUtils.isEmpty(paramString3);
      if (!bool2) {
        paramString1 = paramString3;
      }
      ((ContentValues)localObject).put(str1, paramString1);
      paramString1 = "pan";
      ((ContentValues)localObject).put(paramString1, paramString2);
      paramString1 = "displayPan";
      paramString3 = null;
      boolean bool1 = TextUtils.isEmpty(null);
      int k;
      if (!bool1)
      {
        k = 0;
        paramString2 = null;
      }
      ((ContentValues)localObject).put(paramString1, paramString2);
      paramString1 = "type";
      paramString2 = Integer.valueOf(paramInt);
      ((ContentValues)localObject).put(paramString1, paramString2);
      if (!paramBoolean)
      {
        paramString1 = "flags";
        k = 16;
        paramString2 = Integer.valueOf(k);
        ((ContentValues)localObject).put(paramString1, paramString2);
      }
      paramString1 = "updatedTime";
      long l1 = System.currentTimeMillis();
      paramString2 = Long.valueOf(l1);
      ((ContentValues)localObject).put(paramString1, paramString2);
      paramString1 = "UUID";
      paramString2 = UUID.randomUUID();
      paramString2 = paramString2.toString();
      ((ContentValues)localObject).put(paramString1, paramString2);
      paramString1 = a;
      paramString2 = "accounts";
      long l2 = paramString1.insertOrThrow(paramString2, null, (ContentValues)localObject);
      localObject = a;
      str1 = "accounts";
      arrayOfString1 = d;
      String str3 = "_id = ";
      paramString1 = String.valueOf(l2);
      str2 = str3.concat(paramString1);
      arrayOfString2 = null;
      paramString1 = ((SQLiteDatabase)localObject).query(str1, arrayOfString1, str2, null, null, null, null);
      paramString1.moveToFirst();
      paramString2 = a(paramString1);
      s = i;
      paramString1.close();
      paramString1 = a;
      paramString1.setTransactionSuccessful();
      return paramString2;
    }
    finally
    {
      a.endTransaction();
    }
  }
  
  public final String a(int paramInt, String paramString, ArrayList paramArrayList)
  {
    Object localObject1 = a;
    Object localObject2 = { "UUID" };
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("MUUID = '");
    ((StringBuilder)localObject3).append(paramString);
    ((StringBuilder)localObject3).append("'");
    localObject3 = ((StringBuilder)localObject3).toString();
    localObject1 = ((SQLiteDatabase)localObject1).query("accounts", (String[])localObject2, (String)localObject3, null, null, null, null);
    String str = "";
    if (localObject1 != null)
    {
      int i = ((Cursor)localObject1).getCount();
      if (i > 0)
      {
        ((Cursor)localObject1).moveToNext();
        for (;;)
        {
          boolean bool1 = ((Cursor)localObject1).isAfterLast();
          if (bool1) {
            break;
          }
          int j = ((Cursor)localObject1).getColumnIndex("UUID");
          localObject2 = ((Cursor)localObject1).getString(j);
          boolean bool2 = TextUtils.equals(paramString, (CharSequence)localObject2);
          if (!bool2)
          {
            bool2 = paramArrayList.contains(localObject2);
            if (!bool2)
            {
              paramArrayList.add(localObject2);
              localObject3 = new java/lang/StringBuilder;
              ((StringBuilder)localObject3).<init>();
              ((StringBuilder)localObject3).append(str);
              int k = paramInt + 1;
              str = a(k, (String)localObject2, paramArrayList);
              ((StringBuilder)localObject3).append(str);
              str = ((StringBuilder)localObject3).toString();
            }
          }
          ((Cursor)localObject1).moveToNext();
        }
      }
    }
    if (localObject1 != null) {
      ((Cursor)localObject1).close();
    }
    paramArrayList = new java/lang/StringBuilder;
    paramArrayList.<init>();
    paramArrayList.append(str);
    localObject1 = "'";
    paramArrayList.append((String)localObject1);
    paramArrayList.append(paramString);
    paramArrayList.append("'");
    paramString = paramArrayList.toString();
    Object localObject4;
    if (paramInt > 0)
    {
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>();
      ((StringBuilder)localObject4).append(paramString);
      paramString = ", ";
      ((StringBuilder)localObject4).append(paramString);
      localObject4 = ((StringBuilder)localObject4).toString();
    }
    else
    {
      localObject4 = new java/lang/StringBuilder;
      paramArrayList = "(";
      ((StringBuilder)localObject4).<init>(paramArrayList);
      ((StringBuilder)localObject4).append(paramString);
      paramString = ")";
      ((StringBuilder)localObject4).append(paramString);
      localObject4 = ((StringBuilder)localObject4).toString();
    }
    return (String)localObject4;
  }
  
  public final String a(long paramLong)
  {
    String str1 = "_id = ";
    Object localObject = String.valueOf(paramLong);
    String str2 = str1.concat((String)localObject);
    SQLiteDatabase localSQLiteDatabase = a;
    String str3 = "accounts";
    String[] arrayOfString = { "UUID" };
    localObject = localSQLiteDatabase.query(str3, arrayOfString, str2, null, null, null, null);
    ((Cursor)localObject).moveToFirst();
    boolean bool = ((Cursor)localObject).isAfterLast();
    int i;
    String str4;
    if (!bool)
    {
      i = ((Cursor)localObject).getColumnIndexOrThrow("UUID");
      str4 = ((Cursor)localObject).getString(i);
    }
    else
    {
      i = 0;
      str4 = null;
    }
    ((Cursor)localObject).close();
    return str4;
  }
  
  public final ArrayList a(int paramInt)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject1 = "flags & 1 = 0 AND flags & 2 = 0 AND enabled = 1 AND flags & 64  = 0 AND type = ";
    Object localObject2 = String.valueOf(paramInt);
    String str1 = ((String)localObject1).concat((String)localObject2);
    SQLiteDatabase localSQLiteDatabase = a;
    String str2 = "accounts";
    String[] arrayOfString = d;
    localObject2 = localSQLiteDatabase.query(str2, arrayOfString, str1, null, null, null, null);
    if (localObject2 != null)
    {
      int i = ((Cursor)localObject2).getCount();
      if (i > 0)
      {
        ((Cursor)localObject2).moveToFirst();
        for (;;)
        {
          boolean bool = ((Cursor)localObject2).isAfterLast();
          if (bool) {
            break;
          }
          localObject1 = a((Cursor)localObject2);
          localArrayList.add(localObject1);
          ((Cursor)localObject2).moveToNext();
        }
      }
    }
    if (localObject2 != null) {
      ((Cursor)localObject2).close();
    }
    return localArrayList;
  }
  
  public final ArrayList a(String paramString)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("name LIKE '");
    ((StringBuilder)localObject).append(paramString);
    ((StringBuilder)localObject).append("%' AND flags & 1 = 0 AND flags & 2 = 0  AND flags & 64  = 0  AND (type = 1 OR type = 2 OR type = 3 )");
    String str1 = ((StringBuilder)localObject).toString();
    SQLiteDatabase localSQLiteDatabase = a;
    String str2 = "accounts";
    String[] arrayOfString = d;
    paramString = localSQLiteDatabase.query(str2, arrayOfString, str1, null, null, null, null);
    if (paramString != null)
    {
      int i = paramString.getCount();
      if (i > 0)
      {
        paramString.moveToFirst();
        for (;;)
        {
          boolean bool = paramString.isAfterLast();
          if (bool) {
            break;
          }
          localObject = a(paramString);
          localArrayList.add(localObject);
          paramString.moveToNext();
        }
      }
    }
    if (paramString != null) {
      paramString.close();
    }
    return localArrayList;
  }
  
  public final void a(com.daamitt.prime.sdk.a.a parama1, com.daamitt.prime.sdk.a.a parama2, long paramLong)
  {
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    Object localObject1 = Long.valueOf(paramLong);
    localContentValues.put("updatedTime", (Long)localObject1);
    long l1 = a;
    Object localObject2 = a(l1);
    localContentValues.put("MUUID", (String)localObject2);
    a(parama1, localContentValues);
    localObject1 = new android/content/ContentValues;
    ((ContentValues)localObject1).<init>();
    localObject2 = m;
    a((ContentValues)localObject1, (com.daamitt.prime.sdk.a.b)localObject2);
    int i = ((ContentValues)localObject1).size();
    int k = 1;
    if (i > 0)
    {
      localObject2 = a;
      localObject3 = "accounts";
      localObject4 = "_id =?";
      localObject5 = new String[k];
      localObject6 = new java/lang/StringBuilder;
      ((StringBuilder)localObject6).<init>();
      m = a;
      ((StringBuilder)localObject6).append(m);
      parama2 = ((StringBuilder)localObject6).toString();
      localObject5[0] = parama2;
      ((SQLiteDatabase)localObject2).update((String)localObject3, (ContentValues)localObject1, (String)localObject4, (String[])localObject5);
    }
    int m = a;
    long l2 = m;
    parama2 = a(l2);
    Object localObject3 = a;
    Object localObject4 = "accounts";
    Object localObject5 = d;
    Object localObject6 = "MUUID =?";
    String[] arrayOfString1 = new String[k];
    arrayOfString1[0] = parama2;
    localObject1 = ((SQLiteDatabase)localObject3).query((String)localObject4, (String[])localObject5, (String)localObject6, arrayOfString1, null, null, null);
    if (localObject1 != null)
    {
      i = ((Cursor)localObject1).getCount();
      if (i > 0)
      {
        ((Cursor)localObject1).moveToFirst();
        for (;;)
        {
          boolean bool = ((Cursor)localObject1).isAfterLast();
          if (bool) {
            break;
          }
          localObject2 = a((Cursor)localObject1);
          localObject3 = a;
          localObject4 = new java/lang/StringBuilder;
          localObject5 = "Update transactions Set modifyCount = modifyCount + 1 Where accountId = ";
          ((StringBuilder)localObject4).<init>((String)localObject5);
          int j = a;
          ((StringBuilder)localObject4).append(j);
          localObject2 = ((StringBuilder)localObject4).toString();
          ((SQLiteDatabase)localObject3).execSQL((String)localObject2);
          ((Cursor)localObject1).moveToNext();
        }
        ((Cursor)localObject1).close();
        localObject2 = a;
        localObject3 = "accounts";
        localObject4 = "MUUID =?";
        String[] arrayOfString2 = new String[k];
        arrayOfString2[0] = parama2;
        ((SQLiteDatabase)localObject2).update((String)localObject3, localContentValues, (String)localObject4, arrayOfString2);
      }
    }
    if (localObject1 != null) {
      ((Cursor)localObject1).close();
    }
    parama2 = new java/lang/StringBuilder;
    parama2.<init>("update accounts set flags = flags | 2 where _id = ");
    int n = a;
    parama2.append(n);
    parama1 = parama2.toString();
    a.execSQL(parama1);
  }
  
  public final com.daamitt.prime.sdk.a.a b(int paramInt)
  {
    Object localObject1 = "_id = ";
    Object localObject2 = String.valueOf(paramInt);
    String str1 = ((String)localObject1).concat((String)localObject2);
    SQLiteDatabase localSQLiteDatabase = a;
    String str2 = "accounts";
    String[] arrayOfString = d;
    localObject2 = localSQLiteDatabase.query(str2, arrayOfString, str1, null, null, null, null);
    if (localObject2 != null)
    {
      int i = ((Cursor)localObject2).getCount();
      if (i > 0)
      {
        ((Cursor)localObject2).moveToFirst();
        localObject1 = a((Cursor)localObject2);
        ((Cursor)localObject2).close();
        return (com.daamitt.prime.sdk.a.a)localObject1;
      }
    }
    if (localObject2 != null) {
      ((Cursor)localObject2).close();
    }
    return null;
  }
  
  public final com.daamitt.prime.sdk.a.a b(long paramLong)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("_id IN(");
    Object localObject = c(paramLong);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(") AND flags & 1 = 0 AND flags & 64 = 0 ");
    String str1 = localStringBuilder.toString();
    SQLiteDatabase localSQLiteDatabase = a;
    String str2 = "accounts";
    String[] arrayOfString = d;
    localObject = localSQLiteDatabase.query(str2, arrayOfString, str1, null, null, null, null);
    if (localObject != null)
    {
      i = ((Cursor)localObject).getCount();
      if (i > 0)
      {
        ((Cursor)localObject).moveToFirst();
        locala = a((Cursor)localObject);
        break label115;
      }
    }
    int i = 0;
    com.daamitt.prime.sdk.a.a locala = null;
    label115:
    if (localObject != null) {
      ((Cursor)localObject).close();
    }
    return locala;
  }
  
  public final ArrayList b(int[] paramArrayOfInt)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    paramArrayOfInt = a(paramArrayOfInt);
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("accounts.flags & 1 = 0 AND accounts.flags & 2 = 0 AND accounts.enabled = 1 AND accounts.flags & 64  = 0 AND accounts.type IN  (");
    ((StringBuilder)localObject).append(paramArrayOfInt);
    ((StringBuilder)localObject).append(")");
    paramArrayOfInt = ((StringBuilder)localObject).toString();
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("Select accounts.*, count(*) as count from accounts, transactions where accounts._id = transactions.accountId AND ");
    ((StringBuilder)localObject).append(paramArrayOfInt);
    ((StringBuilder)localObject).append(" group by accountId order by count desc, type asc, pan asc");
    paramArrayOfInt = ((StringBuilder)localObject).toString();
    localObject = a;
    paramArrayOfInt = ((SQLiteDatabase)localObject).rawQuery(paramArrayOfInt, null);
    if (paramArrayOfInt != null)
    {
      int i = paramArrayOfInt.getCount();
      if (i > 0)
      {
        paramArrayOfInt.moveToFirst();
        for (;;)
        {
          boolean bool = paramArrayOfInt.isAfterLast();
          if (bool) {
            break;
          }
          localObject = a(paramArrayOfInt);
          localArrayList.add(localObject);
          paramArrayOfInt.moveToNext();
        }
      }
    }
    if (paramArrayOfInt != null) {
      paramArrayOfInt.close();
    }
    return localArrayList;
  }
  
  public final String c(long paramLong)
  {
    Object localObject = d(paramLong);
    boolean bool = TextUtils.isEmpty((CharSequence)localObject);
    if (!bool)
    {
      localObject = b((String)localObject);
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>("UUID = '");
      localStringBuilder.append((String)localObject);
      localStringBuilder.append("'");
      String str1 = localStringBuilder.toString();
      SQLiteDatabase localSQLiteDatabase = a;
      String str2 = "accounts";
      String[] arrayOfString = { "_id" };
      localObject = localSQLiteDatabase.query(str2, arrayOfString, str1, null, null, null, null);
      if (localObject != null)
      {
        int i = ((Cursor)localObject).getCount();
        if (i > 0)
        {
          ((Cursor)localObject).moveToFirst();
          int j = ((Cursor)localObject).getColumnIndexOrThrow("_id");
          String str3 = ((Cursor)localObject).getString(j);
          ((Cursor)localObject).close();
          return str3;
        }
      }
      if (localObject != null) {
        ((Cursor)localObject).close();
      }
    }
    return String.valueOf(paramLong);
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */