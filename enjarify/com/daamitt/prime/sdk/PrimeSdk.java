package com.daamitt.prime.sdk;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.Telephony.Sms.Inbox;
import android.text.TextUtils;
import com.daamitt.prime.sdk.b.c;
import io.reactivex.k;
import io.reactivex.n;
import java.util.Date;
import org.json.JSONException;

public final class PrimeSdk
{
  private static final String EXTRA_MAX = "prime.sdk.EXTRA_MAX";
  private static final String EXTRA_PROGRESS = "prime.sdk.EXTRA_PROGRESS";
  private static final String PRIME_FINISH = "prime.sdk.PRIME_FINISH";
  private static final String PRIME_PROGRESS = "prime.sdk.PRIME_PROGRESS";
  private static final String PRIME_REQUEST_FOR_READ_SMS_PERM = "prime.sdk.PRIME_REQUEST_FOR_READ_SMS_PERM";
  private static final String TAG = "PrimeSdk";
  private static PrimeSdk sInstance;
  private io.reactivex.a.a mCompositeDisposable;
  private b mPrimeParsingManager = null;
  private BroadcastReceiver mReceiver;
  private ScoreCallback mScoreCallback = null;
  
  private PrimeSdk()
  {
    Object localObject = new com/daamitt/prime/sdk/PrimeSdk$1;
    ((PrimeSdk.1)localObject).<init>(this);
    mReceiver = ((BroadcastReceiver)localObject);
    localObject = new io/reactivex/a/a;
    ((io.reactivex.a.a)localObject).<init>();
    mCompositeDisposable = ((io.reactivex.a.a)localObject);
  }
  
  static void broadcastFinish(android.support.v4.content.d paramd)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("prime.sdk.PRIME_FINISH");
    paramd.a(localIntent);
  }
  
  static void broadcastProgress(android.support.v4.content.d paramd, int paramInt1, int paramInt2)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("prime.sdk.PRIME_PROGRESS");
    localIntent.putExtra("prime.sdk.EXTRA_PROGRESS", paramInt1);
    localIntent.putExtra("prime.sdk.EXTRA_MAX", paramInt2);
    paramd.a(localIntent);
  }
  
  static void broadcastReadSmsPermissionRequest(android.support.v4.content.d paramd)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("prime.sdk.PRIME_REQUEST_FOR_READ_SMS_PERM");
    paramd.a(localIntent);
  }
  
  private void clearStorage(Context paramContext)
  {
    PreferenceManager.getDefaultSharedPreferences(paramContext.getApplicationContext()).edit().clear().apply();
    paramContext = com.daamitt.prime.sdk.b.b.a(paramContext).getWritableDatabase();
    com.daamitt.prime.sdk.b.a.b(paramContext);
    com.daamitt.prime.sdk.b.f.b(paramContext);
    com.daamitt.prime.sdk.b.e.b(paramContext);
    com.daamitt.prime.sdk.b.d.b(paramContext);
    c.b(paramContext);
  }
  
  public static PrimeSdk getInstance()
  {
    synchronized (PrimeSdk.class)
    {
      PrimeSdk localPrimeSdk = sInstance;
      if (localPrimeSdk == null)
      {
        localPrimeSdk = new com/daamitt/prime/sdk/PrimeSdk;
        localPrimeSdk.<init>();
        sInstance = localPrimeSdk;
      }
      com.daamitt.prime.sdk.a.e.a();
      localPrimeSdk = sInstance;
      return localPrimeSdk;
    }
  }
  
  private void registerCallback(Context paramContext, ScoreCallback paramScoreCallback)
  {
    mScoreCallback = paramScoreCallback;
    paramScoreCallback = new android/content/IntentFilter;
    paramScoreCallback.<init>("prime.sdk.PRIME_PROGRESS");
    paramScoreCallback.addAction("prime.sdk.PRIME_FINISH");
    paramScoreCallback.addAction("prime.sdk.PRIME_REQUEST_FOR_READ_SMS_PERM");
    Object localObject = android.support.v4.content.d.a(paramContext);
    BroadcastReceiver localBroadcastReceiver = mReceiver;
    ((android.support.v4.content.d)localObject).a(localBroadcastReceiver);
    paramContext = android.support.v4.content.d.a(paramContext);
    localObject = mReceiver;
    paramContext.a((BroadcastReceiver)localObject, paramScoreCallback);
  }
  
  public final void destroy(Context paramContext)
  {
    com.daamitt.prime.sdk.a.e.a();
    paramContext = android.support.v4.content.d.a(paramContext);
    BroadcastReceiver localBroadcastReceiver = mReceiver;
    paramContext.a(localBroadcastReceiver);
    paramContext = mCompositeDisposable;
    if (paramContext != null)
    {
      boolean bool = b;
      if (!bool)
      {
        paramContext = mCompositeDisposable;
        paramContext.a();
      }
    }
    paramContext = mPrimeParsingManager;
    localBroadcastReceiver = null;
    if (paramContext != null)
    {
      com.daamitt.prime.sdk.a.e.a();
      b.b = null;
      c = null;
      d = null;
      f = null;
      i = null;
      g = null;
      h = null;
      mPrimeParsingManager = null;
    }
    mCompositeDisposable = null;
    mScoreCallback = null;
    sInstance = null;
  }
  
  public final void enableLogging(boolean paramBoolean)
  {
    com.daamitt.prime.sdk.a.e.a = paramBoolean;
  }
  
  public final void getPreScoreData(Context paramContext, PreScoreCallback paramPreScoreCallback)
  {
    com.daamitt.prime.sdk.a.e.a();
    Object localObject1 = "android.permission.READ_SMS";
    int i = android.support.v4.content.b.a(paramContext, (String)localObject1);
    if (i != 0)
    {
      paramPreScoreCallback.onError("Cannot perform scoring without read SMS permission");
      return;
    }
    localObject1 = mCompositeDisposable;
    if (localObject1 == null)
    {
      paramPreScoreCallback.onError("Destroy has been called on this object.");
      return;
    }
    localObject1 = new com/daamitt/prime/sdk/a/f;
    ((com.daamitt.prime.sdk.a.f)localObject1).<init>();
    long l = System.currentTimeMillis();
    io.reactivex.a.a locala = mCompositeDisposable;
    paramContext = ((com.daamitt.prime.sdk.a.f)localObject1).a(paramContext);
    Object localObject2 = io.reactivex.android.b.a.a();
    paramContext = paramContext.a((n)localObject2);
    localObject2 = new com/daamitt/prime/sdk/-$$Lambda$PrimeSdk$V_izwBet5CCXFQP6GlG22dOOcbk;
    ((-..Lambda.PrimeSdk.V_izwBet5CCXFQP6GlG22dOOcbk)localObject2).<init>(l, paramPreScoreCallback, (com.daamitt.prime.sdk.a.f)localObject1);
    localObject1 = new com/daamitt/prime/sdk/-$$Lambda$PrimeSdk$H1EPxNo75BO-zPisMlnj0J1z3LM;
    ((-..Lambda.PrimeSdk.H1EPxNo75BO-zPisMlnj0J1z3LM)localObject1).<init>(paramPreScoreCallback);
    paramContext = paramContext.a((io.reactivex.c.d)localObject2, (io.reactivex.c.d)localObject1);
    locala.a(paramContext);
  }
  
  public final void getScoreData(Context paramContext, String paramString, ScoreCallback paramScoreCallback)
  {
    PrimeSdk localPrimeSdk = this;
    Object localObject1 = paramContext;
    localScoreCallback = paramScoreCallback;
    com.daamitt.prime.sdk.a.e.a();
    Object localObject3 = "android.permission.READ_SMS";
    int i = android.support.v4.content.b.a(paramContext, (String)localObject3);
    if (i != 0)
    {
      paramScoreCallback.onError("Cannot perform scoring without read SMS permission");
      return;
    }
    boolean bool1 = TextUtils.isEmpty(paramString);
    if (bool1)
    {
      paramScoreCallback.onError("Rules not present");
      return;
    }
    localObject3 = b.a(paramContext);
    mPrimeParsingManager = ((b)localObject3);
    for (;;)
    {
      try
      {
        localObject3 = mPrimeParsingManager;
        localObject4 = paramString;
        ((b)localObject3).a(paramString);
        registerCallback(paramContext, paramScoreCallback);
      }
      catch (JSONException localJSONException)
      {
        Object localObject4;
        long l1;
        long l2;
        String str1;
        localScoreCallback.onError("Rules file not formatted correctly");
        return;
      }
      try
      {
        localObject3 = mPrimeParsingManager;
        localObject1 = b.a;
        localObject1 = b.b;
        com.daamitt.prime.sdk.a.e.a();
        l1 = System.currentTimeMillis();
        l2 = 16416000000L;
        l1 -= l2;
        localObject1 = "content://sms/inbox";
        try
        {
          localObject1 = Uri.parse((String)localObject1);
          Object localObject5 = "_id";
          Object localObject6 = "body";
          Object localObject7 = "address";
          Object localObject8 = "date_sent";
          Object localObject9 = "date";
          Object localObject10 = "thread_id";
          String str2 = "case when date_sent IS NOT 0 then date_sent else date END as dateSent";
          String[] tmp180_177 = new String[7];
          String[] tmp181_180 = tmp180_177;
          String[] tmp181_180 = tmp180_177;
          tmp181_180[0] = localObject5;
          tmp181_180[1] = localObject6;
          String[] tmp190_181 = tmp181_180;
          String[] tmp190_181 = tmp181_180;
          tmp190_181[2] = localObject7;
          tmp190_181[3] = localObject8;
          String[] tmp199_190 = tmp190_181;
          String[] tmp199_190 = tmp190_181;
          tmp199_190[4] = localObject9;
          tmp199_190[5] = localObject10;
          tmp199_190[6] = str2;
          localObject7 = tmp199_190;
          localObject5 = "dateSent > ";
          localObject6 = String.valueOf(l1);
          localObject8 = ((String)localObject5).concat((String)localObject6);
          int j = 1;
          String[] arrayOfString = new String[j];
          localObject5 = String.valueOf(l1);
          int k = 0;
          arrayOfString[0] = localObject5;
          localObject10 = "dateSent ASC";
          Cursor localCursor = null;
          int m;
          int n;
          float f1;
          try
          {
            localObject5 = c;
            localObject5 = ((Context)localObject5).getContentResolver();
            m = 0;
            localObject9 = null;
            localObject6 = localObject1;
            localCursor = ((ContentResolver)localObject5).query((Uri)localObject1, (String[])localObject7, (String)localObject8, null, (String)localObject10);
            n = 0;
            f1 = 0.0F;
            arrayOfString = null;
          }
          catch (SQLiteException localSQLiteException)
          {
            localObject5 = "_id";
            localObject6 = "body";
            localObject7 = "address";
            localObject8 = "date";
            localObject9 = "thread_id";
            String[] tmp352_349 = new String[5];
            String[] tmp353_352 = tmp352_349;
            String[] tmp353_352 = tmp352_349;
            tmp353_352[0] = localObject5;
            tmp353_352[1] = localObject6;
            String[] tmp362_353 = tmp353_352;
            String[] tmp362_353 = tmp353_352;
            tmp362_353[2] = localObject7;
            tmp362_353[3] = localObject8;
            tmp362_353[4] = localObject9;
            localObject7 = tmp362_353;
            localObject8 = "date > ? ";
            localObject10 = "date ASC";
            localObject5 = c;
            localObject5 = ((Context)localObject5).getContentResolver();
            localObject6 = localObject1;
            localObject9 = arrayOfString;
            localCursor = ((ContentResolver)localObject5).query((Uri)localObject1, (String[])localObject7, (String)localObject8, arrayOfString, (String)localObject10);
            n = 1;
            f1 = Float.MIN_VALUE;
          }
          catch (SecurityException localSecurityException)
          {
            localObject1 = e;
            broadcastReadSmsPermissionRequest((android.support.v4.content.d)localObject1);
            n = 0;
            f1 = 0.0F;
            arrayOfString = null;
          }
          if (localCursor != null)
          {
            int i1 = localCursor.getCount();
            int i2 = localCursor.getCount();
            if (i2 > 0) {
              com.daamitt.prime.sdk.a.e.b();
            }
            localCursor.moveToFirst();
            boolean bool2 = localCursor.isAfterLast();
            if (!bool2)
            {
              localObject1 = "body";
              int i3 = localCursor.getColumnIndexOrThrow((String)localObject1);
              localObject1 = localCursor.getString(i3);
              localObject5 = "address";
              int i4 = localCursor.getColumnIndexOrThrow((String)localObject5);
              localObject5 = localCursor.getString(i4);
              localObject6 = "thread_id";
              int i5 = localCursor.getColumnIndexOrThrow((String)localObject6);
              m = localCursor.getInt(i5);
              if (n == 0)
              {
                localObject4 = "date_sent";
                i6 = localCursor.getColumnIndex((String)localObject4);
                l1 = localCursor.getLong(i6);
                localObject6 = "date_sent";
                i5 = localCursor.getColumnIndex((String)localObject6);
                localCursor.getLong(i5);
                localObject6 = "date";
                i5 = localCursor.getColumnIndexOrThrow((String)localObject6);
                localCursor.getLong(i5);
                com.daamitt.prime.sdk.a.e.b();
              }
              long l3 = 0L;
              if (n == 0)
              {
                boolean bool4 = l1 < l3;
                if (bool4) {}
              }
              else
              {
                com.daamitt.prime.sdk.a.e.b();
                localObject4 = "date";
                i6 = localCursor.getColumnIndexOrThrow((String)localObject4);
                l1 = localCursor.getLong(i6);
              }
              localObject8 = "_id";
              int i7 = localCursor.getColumnIndexOrThrow((String)localObject8);
              i7 = localCursor.getInt(i7);
              int i8 = n;
              float f2 = f1;
              long l4 = i7;
              localObject8 = new java/util/Date;
              ((Date)localObject8).<init>(l1);
              long l6;
              if ((localObject5 != null) && (localObject1 != null))
              {
                com.daamitt.prime.sdk.a.e.b();
                boolean bool5 = l1 < l3;
                long l5;
                if (bool5)
                {
                  l3 = 86400000L;
                  l5 = l3;
                }
                else
                {
                  l5 = l3;
                }
                localObject6 = d;
                localObject6 = d;
                Object localObject11 = localObject6;
                Object localObject12 = localObject5;
                boolean bool3 = ((com.daamitt.prime.sdk.b.d)localObject6).a((String)localObject5, (String)localObject1, (Date)localObject8, l5);
                if (!bool3)
                {
                  try
                  {
                    localObject6 = new java/lang/StringBuilder;
                    ((StringBuilder)localObject6).<init>();
                    localObject7 = Telephony.Sms.Inbox.CONTENT_URI;
                    ((StringBuilder)localObject6).append(localObject7);
                    localObject7 = "/";
                    ((StringBuilder)localObject6).append((String)localObject7);
                    ((StringBuilder)localObject6).append(l4);
                    localObject11 = ((StringBuilder)localObject6).toString();
                    l6 = l1;
                    localObject4 = localObject3;
                    localObject5 = localObject1;
                    localObject6 = localObject8;
                    j = i1;
                    localObject10 = localObject11;
                    try
                    {
                      ((b)localObject3).a((String)localObject12, (String)localObject1, (Date)localObject8, l4, m, (String)localObject11);
                    }
                    catch (RuntimeException localRuntimeException1) {}
                    com.daamitt.prime.sdk.a.e.c();
                  }
                  catch (RuntimeException localRuntimeException2)
                  {
                    l6 = l1;
                    j = i1;
                  }
                  localObject4 = i;
                  if (localObject4 == null) {
                    throw localRuntimeException2;
                  }
                }
                else
                {
                  l6 = l1;
                  j = i1;
                  com.daamitt.prime.sdk.a.e.b();
                }
              }
              else
              {
                l6 = l1;
                j = i1;
              }
              localCursor.moveToNext();
              int i6 = 1;
              k += i6;
              localObject2 = e;
              broadcastProgress((android.support.v4.content.d)localObject2, k, j);
              n = i8;
              f1 = f2;
              i1 = j;
              l1 = l6;
              j = 1;
              continue;
            }
            localCursor.close();
          }
          Object localObject2 = e;
          broadcastFinish((android.support.v4.content.d)localObject2);
          localObject2 = b.a;
          com.daamitt.prime.sdk.a.e.b();
          return;
        }
        catch (RuntimeException localRuntimeException3)
        {
          throw localRuntimeException3;
        }
      }
      catch (RuntimeException localRuntimeException4) {}
    }
    str1 = "Something went wrong while parsing";
    localObject3 = mPrimeParsingManager.i;
    if (localObject3 == null)
    {
      str1 = "Rules not available";
      localObject3 = mCompositeDisposable;
      if (localObject3 == null) {
        str1 = "Destroy has been called on this object.";
      }
    }
    localScoreCallback.onError(str1);
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.PrimeSdk
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */