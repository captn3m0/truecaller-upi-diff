package com.daamitt.prime.sdk.a;

import java.util.Date;

public class d
  extends k
{
  public static final String a = "d";
  public String b;
  public String c;
  public Date d;
  public int e;
  public String f;
  public String g;
  public int h;
  public double i;
  public String j;
  public boolean k;
  public boolean l;
  public long m;
  public String n;
  
  public d(String paramString1, String paramString2, Date paramDate)
  {
    super(paramString1, paramString2, paramDate);
  }
  
  public static int a(String paramString)
  {
    String str = "movie";
    boolean bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 1;
    }
    str = "taxi";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 2;
    }
    str = "flight";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 3;
    }
    str = "shipment";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 4;
    }
    str = "train";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 5;
    }
    str = "default";
    boolean bool2 = paramString.equalsIgnoreCase(str);
    int i1 = 9;
    if (bool2) {
      return i1;
    }
    return i1;
  }
  
  public final void a(String paramString1, String paramString2, Date paramDate, int paramInt)
  {
    b = paramString1;
    c = paramString2;
    d = paramDate;
    e = paramInt;
  }
  
  public final String b(String paramString)
  {
    int i1 = paramString.hashCode();
    String str;
    boolean bool2;
    switch (i1)
    {
    default: 
      break;
    case 984038195: 
      str = "event_info";
      boolean bool1 = paramString.equals(str);
      if (bool1) {
        int i2 = 2;
      }
      break;
    case 951526432: 
      str = "contact";
      bool2 = paramString.equals(str);
      if (bool2) {
        bool2 = true;
      }
      break;
    case 943500218: 
      str = "event_location";
      bool2 = paramString.equals(str);
      if (bool2) {
        int i3 = 3;
      }
      break;
    case 3076014: 
      str = "date";
      boolean bool3 = paramString.equals(str);
      if (bool3) {
        int i4 = 5;
      }
      break;
    case 111156: 
      str = "pnr";
      boolean bool4 = paramString.equals(str);
      if (bool4) {
        int i5 = 4;
      }
      break;
    case -102973965: 
      str = "sms_time";
      boolean bool5 = paramString.equals(str);
      if (bool5) {
        int i6 = 6;
      }
      break;
    case -1413853096: 
      str = "amount";
      boolean bool6 = paramString.equals(str);
      if (bool6)
      {
        bool6 = false;
        paramString = null;
      }
      break;
    }
    int i7 = -1;
    switch (i7)
    {
    default: 
      return null;
    case 6: 
      return String.valueOf(r.getTime());
    case 5: 
      return String.valueOf(d.getTime());
    case 4: 
      return c;
    case 3: 
      return f;
    case 2: 
      return g;
    case 1: 
      return j;
    }
    return String.valueOf(i);
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */