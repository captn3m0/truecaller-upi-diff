package com.daamitt.prime.sdk.a;

import android.text.TextUtils;
import java.util.HashMap;

public class a
{
  private static final String t = "a";
  public int a;
  public String b;
  public String c;
  public String d;
  public String e;
  public String f;
  public String g;
  public String h;
  public int i;
  public long j;
  public boolean k;
  public int l;
  public b m;
  public int n;
  public boolean o;
  public int p;
  public int q;
  public int r;
  public boolean s;
  private HashMap u;
  private int v;
  private HashMap w;
  private HashMap x;
  private HashMap y;
  
  private a()
  {
    int i1 = -1;
    n = i1;
    o = false;
    r = i1;
    s = false;
  }
  
  public a(String paramString1, String paramString2, int paramInt)
  {
    int i1 = -1;
    n = i1;
    o = false;
    r = i1;
    s = false;
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    u = localHashMap;
    localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    w = localHashMap;
    localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    y = localHashMap;
    localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    x = localHashMap;
    d = paramString1;
    f = paramString2;
    i = 99;
    int i2 = 1;
    v = i2;
    i = paramInt;
    a = i1;
    l = 0;
    k = i2;
    r = i1;
  }
  
  public static int a(String paramString)
  {
    String str = "debit_card";
    boolean bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 1;
    }
    str = "bank";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 2;
    }
    str = "credit_card";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 3;
    }
    str = "bill_pay";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 4;
    }
    str = "phone";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 5;
    }
    str = "generic";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 9;
    }
    str = "filter";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 10;
    }
    str = "placeholder";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 6;
    }
    str = "unknown";
    bool1 = paramString.equalsIgnoreCase(str);
    int i1 = 99;
    if (bool1) {
      return i1;
    }
    str = "ignore";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 9999;
    }
    str = "prepaid";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 17;
    }
    str = "prepaid_dth";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 18;
    }
    str = "electricity";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 19;
    }
    str = "insurance";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 20;
    }
    str = "loan";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 21;
    }
    str = "gas";
    boolean bool2 = paramString.equalsIgnoreCase(str);
    if (bool2) {
      return 22;
    }
    return i1;
  }
  
  public static String a(int paramInt)
  {
    int i1 = 3;
    if (paramInt != i1) {
      return "";
    }
    return "credit";
  }
  
  public final String a()
  {
    String str = e;
    boolean bool = TextUtils.isEmpty(str);
    if (bool) {
      return d;
    }
    return e;
  }
  
  public final String b()
  {
    String str1 = a().trim();
    Object localObject = "unknown";
    String str2 = c();
    boolean bool = ((String)localObject).equalsIgnoreCase(str2);
    if (!bool)
    {
      localObject = c();
      bool = TextUtils.isEmpty((CharSequence)localObject);
      if (!bool)
      {
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append(str1);
        ((StringBuilder)localObject).append(" - ");
        str1 = c();
        ((StringBuilder)localObject).append(str1);
        str1 = ((StringBuilder)localObject).toString();
      }
    }
    return str1;
  }
  
  public final String c()
  {
    String str = g;
    boolean bool = TextUtils.isEmpty(str);
    if (bool) {
      return f;
    }
    return g;
  }
  
  public final String d()
  {
    String str = g;
    boolean bool = TextUtils.isEmpty(str);
    if (bool) {
      str = f;
    } else {
      str = g;
    }
    int i1 = str.length();
    int i2 = 3;
    if (i1 > i2)
    {
      i1 = str.length() - i2;
      str = str.substring(i1);
    }
    return str;
  }
  
  public final boolean e()
  {
    int i1 = l;
    int i2 = 2;
    i1 &= i2;
    return i1 == i2;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("[");
    String str = d;
    localStringBuilder.append(str);
    localStringBuilder.append("] [");
    str = f;
    localStringBuilder.append(str);
    localStringBuilder.append("] type ");
    int i1 = i;
    localStringBuilder.append(i1);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */