package com.daamitt.prime.sdk.a;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Telephony.Sms.Inbox;
import io.reactivex.c.e;
import io.reactivex.g.a;
import io.reactivex.k;
import io.reactivex.n;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Callable;

public class f
{
  private static final String a = "f";
  private int[] b;
  private int[] c;
  private int[] d;
  private int[] e;
  private int[] f;
  private int[] g;
  private int[] h;
  private int[] i;
  private int[] j;
  private int[] k;
  private int[] l;
  private int[] m;
  private int[] n;
  private int[] o;
  private int[] p;
  private int[] q;
  private int[] r;
  private int[] s;
  
  private static int[] a(Context paramContext, String paramString)
  {
    Object localObject = "(case when datetime(date/1000, 'unixepoch')>=datetime('now','-1 months') then 1\nwhen datetime(date/1000, 'unixepoch')>=datetime('now','-2 months') then 2\nwhen datetime(date/1000, 'unixepoch')>=datetime('now','-3 months') then 3\nwhen datetime(date/1000, 'unixepoch')>=datetime('now','-4 months') then 4\nwhen datetime(date/1000, 'unixepoch')>=datetime('now','-5 months') then 5\nwhen datetime(date/1000, 'unixepoch')>=datetime('now','-6 months') then 6 end) as month ";
    String[] arrayOfString = { localObject };
    try
    {
      ContentResolver localContentResolver = paramContext.getContentResolver();
      Uri localUri = Telephony.Sms.Inbox.CONTENT_URI;
      paramContext = localContentResolver.query(localUri, arrayOfString, paramString, null, null);
      int i1 = 6;
      localObject = new int[i1];
      localContentResolver = null;
      int i3 = 0;
      localUri = null;
      while (i3 < i1)
      {
        localObject[i3] = 0;
        i3 += 1;
      }
      if (paramContext != null)
      {
        paramContext.moveToFirst();
        for (;;)
        {
          boolean bool = paramContext.isAfterLast();
          if (bool) {
            break;
          }
          int i2 = paramContext.getInt(0);
          if (i2 > 0)
          {
            i3 = 7;
            if (i2 < i3)
            {
              i2 += -1;
              i3 = localObject[i2] + 1;
              localObject[i2] = i3;
            }
          }
          paramContext.moveToNext();
        }
        paramContext.close();
      }
      return (int[])localObject;
    }
    catch (SecurityException localSecurityException)
    {
      throw localSecurityException;
    }
  }
  
  private static int[] a(Context paramContext, ArrayList paramArrayList1, ArrayList paramArrayList2)
  {
    String[] arrayOfString = { "(case when datetime(date/1000, 'unixepoch')>=datetime('now','-1 months') then 1\nwhen datetime(date/1000, 'unixepoch')>=datetime('now','-2 months') then 2\nwhen datetime(date/1000, 'unixepoch')>=datetime('now','-3 months') then 3\nwhen datetime(date/1000, 'unixepoch')>=datetime('now','-4 months') then 4\nwhen datetime(date/1000, 'unixepoch')>=datetime('now','-5 months') then 5\nwhen datetime(date/1000, 'unixepoch')>=datetime('now','-6 months') then 6 end) as month " };
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("datetime(date/1000, 'unixepoch') >= datetime('now','-6 months') AND (");
    int i1 = 0;
    ContentResolver localContentResolver = null;
    int i2;
    Object localObject1;
    for (;;)
    {
      i2 = paramArrayList1.size();
      if (i1 >= i2) {
        break;
      }
      if (i1 != 0)
      {
        localObject1 = " OR ";
        localStringBuilder.append((String)localObject1);
      }
      localObject1 = "body LIKE ? ";
      localStringBuilder.append((String)localObject1);
      i1 += 1;
    }
    if (paramArrayList2 != null)
    {
      i1 = 0;
      localContentResolver = null;
      for (;;)
      {
        i2 = paramArrayList1.size();
        if (i1 >= i2) {
          break;
        }
        localStringBuilder.append(" OR ");
        localObject1 = "lower(body) LIKE ? ";
        localStringBuilder.append((String)localObject1);
        i1 += 1;
      }
      paramArrayList1.addAll(paramArrayList2);
    }
    paramArrayList2 = ")";
    localStringBuilder.append(paramArrayList2);
    try
    {
      localContentResolver = paramContext.getContentResolver();
      localObject1 = Telephony.Sms.Inbox.CONTENT_URI;
      String str = localStringBuilder.toString();
      int i3 = paramArrayList1.size();
      paramContext = new String[i3];
      paramContext = paramArrayList1.toArray(paramContext);
      Object localObject2 = paramContext;
      localObject2 = (String[])paramContext;
      paramContext = localContentResolver.query((Uri)localObject1, arrayOfString, str, (String[])localObject2, null);
      int i4 = 6;
      paramArrayList2 = new int[i4];
      int i6 = 0;
      localStringBuilder = null;
      while (i6 < i4)
      {
        paramArrayList2[i6] = 0;
        i6 += 1;
      }
      if (paramContext != null)
      {
        paramContext.moveToFirst();
        for (;;)
        {
          boolean bool = paramContext.isAfterLast();
          if (bool) {
            break;
          }
          int i5 = paramContext.getInt(0);
          if (i5 > 0)
          {
            i6 = 7;
            if (i5 < i6)
            {
              i5 += -1;
              i6 = paramArrayList2[i5] + 1;
              paramArrayList2[i5] = i6;
            }
          }
          paramContext.moveToNext();
        }
        paramContext.close();
      }
      return paramArrayList2;
    }
    catch (SecurityException localSecurityException)
    {
      throw localSecurityException;
    }
  }
  
  public final k a(Context paramContext)
  {
    Object localObject1 = new k[18];
    Object localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$f$SNuiBE0AGXVKBSzoNs3pkECHEd0;
    ((-..Lambda.f.SNuiBE0AGXVKBSzoNs3pkECHEd0)localObject2).<init>(this, paramContext);
    localObject2 = k.a((Callable)localObject2);
    n localn = a.b();
    localObject2 = ((k)localObject2).b(localn);
    localObject1[0] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$f$1bf6vpbldhAt42N4mBoHIfIwcKM;
    ((-..Lambda.f.1bf6vpbldhAt42N4mBoHIfIwcKM)localObject2).<init>(this, paramContext);
    localObject2 = k.a((Callable)localObject2);
    localn = a.b();
    localObject2 = ((k)localObject2).b(localn);
    localObject1[1] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$f$pu17R9OnYzRIq_0EDGSaJY2pzAA;
    ((-..Lambda.f.pu17R9OnYzRIq_0EDGSaJY2pzAA)localObject2).<init>(this, paramContext);
    localObject2 = k.a((Callable)localObject2);
    localn = a.b();
    localObject2 = ((k)localObject2).b(localn);
    localObject1[2] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$f$7Nvqy5_AEmt5LdVUsKs1MCvX1eM;
    ((-..Lambda.f.7Nvqy5_AEmt5LdVUsKs1MCvX1eM)localObject2).<init>(this, paramContext);
    localObject2 = k.a((Callable)localObject2);
    localn = a.b();
    localObject2 = ((k)localObject2).b(localn);
    localObject1[3] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$f$L88A9kDX6PaKjyUYyvMVI_XDVAs;
    ((-..Lambda.f.L88A9kDX6PaKjyUYyvMVI_XDVAs)localObject2).<init>(this, paramContext);
    localObject2 = k.a((Callable)localObject2);
    localn = a.b();
    localObject2 = ((k)localObject2).b(localn);
    localObject1[4] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$f$gn7BXQb1Jj8EKWX77PmqCX9yDlU;
    ((-..Lambda.f.gn7BXQb1Jj8EKWX77PmqCX9yDlU)localObject2).<init>(this, paramContext);
    localObject2 = k.a((Callable)localObject2);
    localn = a.b();
    localObject2 = ((k)localObject2).b(localn);
    localObject1[5] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$f$_gWz0i7SHcqrxgMh7FtVRSGVmRM;
    ((-..Lambda.f._gWz0i7SHcqrxgMh7FtVRSGVmRM)localObject2).<init>(this, paramContext);
    localObject2 = k.a((Callable)localObject2);
    localn = a.b();
    localObject2 = ((k)localObject2).b(localn);
    localObject1[6] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$f$YWBtGRd7eafWfCfFQQUPY-w3h0I;
    ((-..Lambda.f.YWBtGRd7eafWfCfFQQUPY-w3h0I)localObject2).<init>(this, paramContext);
    localObject2 = k.a((Callable)localObject2);
    localn = a.b();
    localObject2 = ((k)localObject2).b(localn);
    localObject1[7] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$f$f_PqfwnbqyDucNejEekUFMIESZE;
    ((-..Lambda.f.f_PqfwnbqyDucNejEekUFMIESZE)localObject2).<init>(this, paramContext);
    localObject2 = k.a((Callable)localObject2);
    localn = a.b();
    localObject2 = ((k)localObject2).b(localn);
    localObject1[8] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$f$FTx5vKJpPhn2kRhhbTyHusSA-Sw;
    ((-..Lambda.f.FTx5vKJpPhn2kRhhbTyHusSA-Sw)localObject2).<init>(this, paramContext);
    localObject2 = k.a((Callable)localObject2);
    localn = a.b();
    localObject2 = ((k)localObject2).b(localn);
    localObject1[9] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$f$uZ05SDtP8pVZpe7JMdtUA5hYyjk;
    ((-..Lambda.f.uZ05SDtP8pVZpe7JMdtUA5hYyjk)localObject2).<init>(this, paramContext);
    localObject2 = k.a((Callable)localObject2);
    localn = a.b();
    localObject2 = ((k)localObject2).b(localn);
    localObject1[10] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$f$Vw3xfRQ8OqSPae9siyqybhfQFFw;
    ((-..Lambda.f.Vw3xfRQ8OqSPae9siyqybhfQFFw)localObject2).<init>(this, paramContext);
    localObject2 = k.a((Callable)localObject2);
    localn = a.b();
    localObject2 = ((k)localObject2).b(localn);
    localObject1[11] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$f$QJJjl1bzHZ0Diw51I8Kbg97-ITE;
    ((-..Lambda.f.QJJjl1bzHZ0Diw51I8Kbg97-ITE)localObject2).<init>(this, paramContext);
    localObject2 = k.a((Callable)localObject2);
    localn = a.b();
    localObject2 = ((k)localObject2).b(localn);
    localObject1[12] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$f$p2lnPuEExFykEWsEyXcg0ugdN6Y;
    ((-..Lambda.f.p2lnPuEExFykEWsEyXcg0ugdN6Y)localObject2).<init>(this, paramContext);
    localObject2 = k.a((Callable)localObject2);
    localn = a.b();
    localObject2 = ((k)localObject2).b(localn);
    localObject1[13] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$f$zBB-jZVXXq1ZNZFpF_sI8J1kXFI;
    ((-..Lambda.f.zBB-jZVXXq1ZNZFpF_sI8J1kXFI)localObject2).<init>(this, paramContext);
    localObject2 = k.a((Callable)localObject2);
    localn = a.b();
    localObject2 = ((k)localObject2).b(localn);
    localObject1[14] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$f$lIfMgURLBbljPtiftNlwZjkiQNE;
    ((-..Lambda.f.lIfMgURLBbljPtiftNlwZjkiQNE)localObject2).<init>(this, paramContext);
    localObject2 = k.a((Callable)localObject2);
    localn = a.b();
    localObject2 = ((k)localObject2).b(localn);
    localObject1[15] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$f$H6u39WDSRIXTFJFkuf1xYrFWdSg;
    ((-..Lambda.f.H6u39WDSRIXTFJFkuf1xYrFWdSg)localObject2).<init>(this, paramContext);
    localObject2 = k.a((Callable)localObject2);
    localn = a.b();
    localObject2 = ((k)localObject2).b(localn);
    localObject1[16] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$f$OJ8k2u5ZQZ8PdQtbApPl5vUnw7I;
    ((-..Lambda.f.OJ8k2u5ZQZ8PdQtbApPl5vUnw7I)localObject2).<init>(this, paramContext);
    paramContext = k.a((Callable)localObject2);
    localObject2 = a.b();
    paramContext = paramContext.b((n)localObject2);
    localObject1[17] = paramContext;
    paramContext = Arrays.asList((Object[])localObject1);
    localObject1 = -..Lambda.f.pIZjUsvVPdru3kiZ5DD56IUpWfc.INSTANCE;
    paramContext = k.a(paramContext, (e)localObject1);
    localObject1 = a.b();
    return paramContext.b((n)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */