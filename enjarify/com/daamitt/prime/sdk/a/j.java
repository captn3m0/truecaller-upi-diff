package com.daamitt.prime.sdk.a;

import android.content.Context;
import android.text.TextUtils;
import io.reactivex.c.e;
import io.reactivex.n;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;

public final class j
{
  private HashMap A;
  private HashMap B;
  private String[] C;
  private HashMap D;
  private HashMap E;
  private HashMap F;
  private String[] a;
  private String[] b;
  private HashMap c;
  private HashMap d;
  private HashMap e;
  private HashMap f;
  private HashMap g;
  private HashMap h;
  private HashMap i;
  private HashMap j;
  private HashMap k;
  private HashMap l;
  private String[] m;
  private String[] n;
  private HashMap o;
  private String[] p;
  private String[] q;
  private String[] r;
  private String[] s;
  private String[] t;
  private String[] u;
  private String[] v;
  private String[] w;
  private String[] x;
  private HashMap y;
  private String[] z;
  
  private static Calendar a(Calendar paramCalendar)
  {
    paramCalendar.set(11, 0);
    paramCalendar.set(12, 0);
    paramCalendar.set(13, 0);
    paramCalendar.set(14, 0);
    return paramCalendar;
  }
  
  private void a(String paramString, HashMap paramHashMap, ArrayList paramArrayList)
  {
    int i1 = 1;
    int i2 = 1;
    for (;;)
    {
      int i3 = paramArrayList.size();
      if (i2 >= i3) {
        break;
      }
      i3 = i2 + -1;
      Object localObject1 = (k)paramArrayList.get(i3);
      Object localObject2 = (k)paramArrayList.get(i2);
      int i5 = w;
      Object localObject3 = Integer.valueOf(i5);
      localObject3 = (a)paramHashMap.get(localObject3);
      int i6 = w;
      Object localObject4 = Integer.valueOf(i6);
      localObject4 = (a)paramHashMap.get(localObject4);
      String str1 = ((a)localObject4).d();
      int i7 = i;
      if (i7 != i1)
      {
        i7 = i;
        if (i7 == i1) {
          str1 = ((a)localObject3).d();
        }
      }
      String str2 = ((a)localObject4).d();
      int i8 = i;
      int i9 = 2;
      if (i8 != i9)
      {
        i8 = i;
        if (i8 == i9) {
          str2 = ((a)localObject3).d();
        }
      }
      localObject1 = (m)localObject1;
      localObject2 = (m)localObject2;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append(paramString);
      String str3 = "_";
      localStringBuilder.append(str3);
      localStringBuilder.append(str1);
      localStringBuilder.append("_");
      localStringBuilder.append(str2);
      str1 = localStringBuilder.toString();
      i7 = i;
      i8 = i;
      if (i7 != i8)
      {
        localObject3 = ((a)localObject3).d();
        localObject4 = ((a)localObject4).d();
        boolean bool2 = TextUtils.equals((CharSequence)localObject3, (CharSequence)localObject4);
        if (!bool2)
        {
          localObject3 = i;
          if (localObject3 != null)
          {
            localObject3 = i;
            double d1 = a;
            double d2 = 0.0D;
            boolean bool4 = d1 < d2;
            if (bool4)
            {
              localObject3 = i;
              if (localObject3 != null)
              {
                localObject3 = i;
                d1 = a;
                bool4 = d1 < d2;
                if (bool4)
                {
                  d1 = i.a;
                  localObject1 = i;
                  d2 = a;
                  d1 -= d2;
                  double d3 = f;
                  d3 = Math.abs(d1 - d3);
                  d1 = 1.0D;
                  boolean bool3 = d3 < d1;
                  if (bool3)
                  {
                    localObject1 = E;
                    boolean bool1 = ((HashMap)localObject1).containsKey(str1);
                    if (bool1)
                    {
                      int i4 = ((Integer)E.get(str1)).intValue() + i1;
                      localObject2 = E;
                      localObject1 = Integer.valueOf(i4);
                      ((HashMap)localObject2).put(str1, localObject1);
                    }
                    else
                    {
                      localObject1 = E;
                      localObject2 = Integer.valueOf(i1);
                      ((HashMap)localObject1).put(str1, localObject2);
                    }
                  }
                }
              }
            }
          }
        }
      }
      i2 += 1;
    }
  }
  
  private static int[] a(List paramList)
  {
    int i1 = paramList.size();
    int[] arrayOfInt = new int[i1];
    int i2 = 0;
    for (;;)
    {
      int i3 = arrayOfInt.length;
      if (i2 >= i3) {
        break;
      }
      Integer localInteger = (Integer)paramList.get(i2);
      i3 = localInteger.intValue();
      arrayOfInt[i2] = i3;
      i2 += 1;
    }
    return arrayOfInt;
  }
  
  private static Calendar b(Calendar paramCalendar)
  {
    paramCalendar.set(11, 23);
    int i1 = 59;
    paramCalendar.set(12, i1);
    paramCalendar.set(13, i1);
    paramCalendar.set(14, 999);
    return paramCalendar;
  }
  
  public final io.reactivex.k a(Context paramContext)
  {
    Object localObject1 = new io.reactivex.k[31];
    Object localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$qvd7RHnHbSRfhbl5cLSIPFm7NRc;
    ((-..Lambda.j.qvd7RHnHbSRfhbl5cLSIPFm7NRc)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[0] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$ZV_EFkwBPra5lghqf3F7YwIshiY;
    ((-..Lambda.j.ZV_EFkwBPra5lghqf3F7YwIshiY)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[1] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$QyN0J46Jx5vhMPwPJsGMo5URlJs;
    ((-..Lambda.j.QyN0J46Jx5vhMPwPJsGMo5URlJs)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[2] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$P4k9GE_rwPOcIP2QMgAm2a4bsFs;
    ((-..Lambda.j.P4k9GE_rwPOcIP2QMgAm2a4bsFs)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[3] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$bPJCmj6Jeu8vOba70vzZJbYyIxA;
    ((-..Lambda.j.bPJCmj6Jeu8vOba70vzZJbYyIxA)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[4] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$JturQhz75MLkIChgwNsk6S6ny1Q;
    ((-..Lambda.j.JturQhz75MLkIChgwNsk6S6ny1Q)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[5] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$tkAIJ6olByMr1Sg64Tb12K1NGgc;
    ((-..Lambda.j.tkAIJ6olByMr1Sg64Tb12K1NGgc)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[6] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$p6riCNkt7WokzvpHKJ_v-6lqN9Y;
    ((-..Lambda.j.p6riCNkt7WokzvpHKJ_v-6lqN9Y)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[7] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$1pskZWBUm4W-f_RHPZ7LRr_ilxo;
    ((-..Lambda.j.1pskZWBUm4W-f_RHPZ7LRr_ilxo)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[8] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$cTYFocjl_m1UgDvSDFQUAnQt99E;
    ((-..Lambda.j.cTYFocjl_m1UgDvSDFQUAnQt99E)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[9] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$69ddauwtav6x6cYSiFvSDY81m9w;
    ((-..Lambda.j.69ddauwtav6x6cYSiFvSDY81m9w)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[10] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$YmagHgIZryxITzaVbvL8pEcUU1o;
    ((-..Lambda.j.YmagHgIZryxITzaVbvL8pEcUU1o)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[11] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$xkxlCrZB-JDRHr1PcMWaoF7zjiY;
    ((-..Lambda.j.xkxlCrZB-JDRHr1PcMWaoF7zjiY)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[12] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$M07sWhhSAZfUFGEGMDlrp7lBRBo;
    ((-..Lambda.j.M07sWhhSAZfUFGEGMDlrp7lBRBo)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[13] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$lrWGjwRs76uagLiu-IWANY1OADA;
    ((-..Lambda.j.lrWGjwRs76uagLiu-IWANY1OADA)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[14] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$PHj5ag8MkxEWcfuqLEXwIO_g_ws;
    ((-..Lambda.j.PHj5ag8MkxEWcfuqLEXwIO_g_ws)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[15] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$cJYovwWf4hQS50ZMAx8Wps3-XdU;
    ((-..Lambda.j.cJYovwWf4hQS50ZMAx8Wps3-XdU)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[16] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$sXZEzNXd06xlhtkMM7hvJmMtf04;
    ((-..Lambda.j.sXZEzNXd06xlhtkMM7hvJmMtf04)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[17] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$htoaonATVf4eWD-X4Iuzw06V0-o;
    ((-..Lambda.j.htoaonATVf4eWD-X4Iuzw06V0-o)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[18] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$aYZLvOKT9gZuj0dOq-Vs3ZZPdcM;
    ((-..Lambda.j.aYZLvOKT9gZuj0dOq-Vs3ZZPdcM)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[19] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$g1BoMh6qnqNbN8pnT535xQFcuI0;
    ((-..Lambda.j.g1BoMh6qnqNbN8pnT535xQFcuI0)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[20] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$BWD-YMepf65sAsnONrnKbKtpE-w;
    ((-..Lambda.j.BWD-YMepf65sAsnONrnKbKtpE-w)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[21] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$9X_Z8vdoYoP5otiXw3Oglzvs61A;
    ((-..Lambda.j.9X_Z8vdoYoP5otiXw3Oglzvs61A)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[22] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$6MuyvFjH0KMIbhUHzJESJEtFtKw;
    ((-..Lambda.j.6MuyvFjH0KMIbhUHzJESJEtFtKw)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[23] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$aDSUuvfJX1Ij4c0Ie0At39yTa58;
    ((-..Lambda.j.aDSUuvfJX1Ij4c0Ie0At39yTa58)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[24] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$48t0dvjTZR4CB359DxfdqGSKu14;
    ((-..Lambda.j.48t0dvjTZR4CB359DxfdqGSKu14)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[25] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$Cv9lZgNO1yyp3WIYpAXUBadMzz4;
    ((-..Lambda.j.Cv9lZgNO1yyp3WIYpAXUBadMzz4)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[26] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$n2-BMuJDE3dYdY-qrtT5u36vSxo;
    ((-..Lambda.j.n2-BMuJDE3dYdY-qrtT5u36vSxo)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[27] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$Ds8J7CNDrPFUXVN_1BiJMQCP1ig;
    ((-..Lambda.j.Ds8J7CNDrPFUXVN_1BiJMQCP1ig)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[28] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$GMts-r4630U19uwh3hL0RgzLDik;
    ((-..Lambda.j.GMts-r4630U19uwh3hL0RgzLDik)localObject2).<init>(this, paramContext);
    localObject2 = io.reactivex.k.a((Callable)localObject2);
    localObject1[29] = localObject2;
    localObject2 = new com/daamitt/prime/sdk/a/-$$Lambda$j$sC-L9kxc2JrS2O-Fu9Sjv40WjCk;
    ((-..Lambda.j.sC-L9kxc2JrS2O-Fu9Sjv40WjCk)localObject2).<init>(this, paramContext);
    paramContext = io.reactivex.k.a((Callable)localObject2);
    localObject1[30] = paramContext;
    paramContext = Arrays.asList((Object[])localObject1);
    localObject1 = -..Lambda.j.7P9x_y2mMlatVQdvVdI8V3Q7vl8.INSTANCE;
    paramContext = io.reactivex.k.a(paramContext, (e)localObject1);
    localObject1 = io.reactivex.g.a.c();
    return paramContext.b((n)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.a.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */