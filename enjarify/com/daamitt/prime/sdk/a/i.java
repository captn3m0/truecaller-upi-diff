package com.daamitt.prime.sdk.a;

import android.text.TextUtils;

public final class i
{
  public static String a(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool) {
      return "";
    }
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    int i = paramString.length();
    int j = 0;
    while (j < i)
    {
      int k = paramString.charAt(j);
      int m = Character.digit(k, 10);
      int n = -1;
      if (m != n)
      {
        localStringBuilder.append(m);
      }
      else
      {
        m = localStringBuilder.length();
        if (m == 0)
        {
          m = 43;
          if (k == m) {
            localStringBuilder.append(k);
          }
        }
      }
      j += 1;
    }
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.a.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */