package com.daamitt.prime.sdk.a;

import android.location.Location;
import java.util.Date;

public class m
  extends k
{
  public static final String a = "m";
  public String T;
  public String U;
  public String V;
  public String W;
  public Location X;
  public int Y;
  public String Z;
  public String aa;
  public String ab;
  public boolean ac = false;
  public Date ad;
  public String b;
  public String c;
  public String d;
  public String e;
  public double f;
  public double g;
  public double h;
  public b i;
  public int j;
  public long k;
  public String l;
  public String m;
  public String n;
  
  public m(String paramString1, String paramString2, Date paramDate)
  {
    super(paramString1, paramString2, paramDate);
  }
  
  public static int a(String paramString)
  {
    int i1 = 9;
    if (paramString == null) {
      return i1;
    }
    String str = "credit_card";
    boolean bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 1;
    }
    str = "debit_card";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 2;
    }
    str = "debit_atm";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 3;
    }
    str = "net_banking";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 4;
    }
    str = "bill_pay";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 5;
    }
    str = "ecs";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 6;
    }
    str = "cheque";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 10;
    }
    str = "default";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return i1;
    }
    str = "balance";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 12;
    }
    str = "credit";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 17;
    }
    str = "credit_atm";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 15;
    }
    str = "debit_prepaid";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 16;
    }
    str = "upi";
    boolean bool2 = paramString.equalsIgnoreCase(str);
    if (bool2) {
      return 18;
    }
    return i1;
  }
  
  public static String a(int paramInt1, int paramInt2)
  {
    int i1 = 10;
    if (paramInt1 != i1)
    {
      i1 = 15;
      if (paramInt1 != i1)
      {
        i1 = 18;
        if (paramInt1 != i1)
        {
          switch (paramInt1)
          {
          default: 
            return "";
          case 6: 
            return "";
          case 5: 
            return "";
          case 4: 
            return "";
          case 3: 
            paramInt1 = 2;
            if (paramInt2 == paramInt1) {
              return "";
            }
            return "debit";
          case 2: 
            return "debit";
          }
          return "credit";
        }
        return "";
      }
      return "credit";
    }
    return "";
  }
  
  public static boolean a(int paramInt)
  {
    int i1 = 5;
    if (paramInt != i1)
    {
      i1 = 6;
      if (paramInt != i1) {
        return false;
      }
    }
    return true;
  }
  
  public static boolean b(int paramInt)
  {
    int i1 = 4;
    if (paramInt != i1)
    {
      i1 = 5;
      if (paramInt != i1)
      {
        i1 = 10;
        if (paramInt != i1)
        {
          i1 = 9;
          if (paramInt != i1)
          {
            i1 = 13;
            if (paramInt != i1) {
              return false;
            }
          }
        }
      }
    }
    return true;
  }
  
  public static int[] c()
  {
    int[] arrayOfInt = new int[8];
    arrayOfInt[0] = 2;
    arrayOfInt[1] = 3;
    arrayOfInt[2] = 4;
    arrayOfInt[3] = 5;
    arrayOfInt[4] = 6;
    arrayOfInt[5] = 9;
    arrayOfInt[6] = 10;
    arrayOfInt[7] = 12;
    return arrayOfInt;
  }
  
  public static String d()
  {
    return "1 , 2 , 3 , 4 , 5 , 6 , 7 , 8, 9 , 14 , 16 , 10 , 13 , 15 , 18";
  }
  
  public static String e()
  {
    return "1 , 2 , 3 , 4 , 5 , 6 , 7 , 8, 9 , 14 , 16 , 10 , 13 , 15 , 12 , 17 , 18";
  }
  
  public static String f()
  {
    return "2 , 3 , 4 , 5 , 6 , 10 , 12 , 17";
  }
  
  public static String g()
  {
    return "3 , 15";
  }
  
  public static String h()
  {
    return "4";
  }
  
  public static String i()
  {
    return "17";
  }
  
  public final void a(String paramString1, Double paramDouble, Date paramDate, String paramString2, int paramInt)
  {
    double d1 = paramDouble.doubleValue();
    f = d1;
    ad = paramDate;
    j = paramInt;
    b = paramString1;
    c = paramString2;
    k = -1;
    T = "other";
  }
  
  public final String b(String paramString)
  {
    int i1 = paramString.hashCode();
    String str;
    boolean bool2;
    switch (i1)
    {
    default: 
      break;
    case 509054971: 
      str = "transaction_type";
      boolean bool1 = paramString.equals(str);
      if (bool1) {
        int i2 = 4;
      }
      break;
    case 3387378: 
      str = "note";
      bool2 = paramString.equals(str);
      if (bool2) {
        bool2 = true;
      }
      break;
    case 3076014: 
      str = "date";
      bool2 = paramString.equals(str);
      if (bool2) {
        int i3 = 5;
      }
      break;
    case 111188: 
      str = "pos";
      boolean bool3 = paramString.equals(str);
      if (bool3) {
        int i4 = 2;
      }
      break;
    case 110749: 
      str = "pan";
      boolean bool4 = paramString.equals(str);
      if (bool4) {
        int i5 = 3;
      }
      break;
    case -1413853096: 
      str = "amount";
      boolean bool5 = paramString.equals(str);
      if (bool5)
      {
        bool5 = false;
        paramString = null;
      }
      break;
    }
    int i6 = -1;
    switch (i6)
    {
    default: 
      return null;
    case 5: 
      return String.valueOf(ad.getTime());
    case 4: 
      return String.valueOf(j);
    case 3: 
      return String.valueOf(b);
    case 2: 
      return c;
    case 1: 
      return V;
    }
    return String.valueOf(f);
  }
  
  public final void j()
  {
    int i1 = Y | 0x1;
    Y = i1;
  }
  
  public final boolean k()
  {
    int i1 = Y;
    int i2 = 1;
    i1 &= i2;
    if (i1 != i2) {
      return i2;
    }
    return false;
  }
  
  public final boolean l()
  {
    int i1 = Y;
    int i2 = 4;
    i1 &= i2;
    return i1 == i2;
  }
  
  public final void m()
  {
    int i1 = Y | 0x20;
    Y = i1;
  }
  
  public final void n()
  {
    int i1 = Y & 0xFFFFFFDF;
    Y = i1;
  }
  
  public final void o()
  {
    int i1 = Y | 0x80;
    Y = i1;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    Object localObject1 = super.toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\n_id : ");
    long l1 = o;
    ((StringBuilder)localObject1).append(l1);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\npan : ");
    Object localObject2 = b;
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\npos : ");
    localObject2 = c;
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\namount : ");
    double d1 = f;
    ((StringBuilder)localObject1).append(d1);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\ntxnType : ");
    int i1 = j;
    ((StringBuilder)localObject1).append(i1);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\ntxnCategories : ");
    localObject2 = T;
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\ntxnTags : ");
    localObject2 = U;
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\nplaceId : ");
    localObject2 = l;
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\nplaceName : ");
    localObject2 = m;
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\nbal : ");
    localObject2 = i;
    ((StringBuilder)localObject1).append(localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\ndisplayPan : ");
    localObject2 = d;
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\naccountDisplayName : ");
    localObject2 = y;
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\naccountType : ");
    i1 = v;
    ((StringBuilder)localObject1).append(i1);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\ntxnDate : ");
    localObject2 = ad;
    ((StringBuilder)localObject1).append(localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\ndate : ");
    localObject2 = r;
    ((StringBuilder)localObject1).append(localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.a.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */