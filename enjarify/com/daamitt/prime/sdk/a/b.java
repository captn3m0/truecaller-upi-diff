package com.daamitt.prime.sdk.a;

import java.util.Date;

public final class b
{
  public double a;
  public double b;
  public Date c;
  public Date d;
  
  public static boolean a(Date paramDate1, Date paramDate2)
  {
    boolean bool1 = true;
    if ((paramDate1 != null) && (paramDate2 != null))
    {
      boolean bool2 = paramDate1.equals(paramDate2);
      if (bool2) {
        return bool1;
      }
      boolean bool3 = paramDate1.after(paramDate2);
      if (bool3) {
        return bool1;
      }
      return false;
    }
    return bool1;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("AccountBalance{balance=");
    double d1 = a;
    localStringBuilder.append(d1);
    localStringBuilder.append(", outstandingBalance=");
    d1 = b;
    localStringBuilder.append(d1);
    localStringBuilder.append(", lastBalSyncdate=");
    Date localDate = c;
    localStringBuilder.append(localDate);
    localStringBuilder.append(", lastOutbalSyncdate=");
    localDate = d;
    localStringBuilder.append(localDate);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */