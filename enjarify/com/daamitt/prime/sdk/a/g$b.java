package com.daamitt.prime.sdk.a;

import c.g.b.k;

public final class g$b
  extends g
{
  final Object a;
  
  public g$b(Object paramObject)
  {
    super((byte)0);
    a = paramObject;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof b;
      if (bool1)
      {
        paramObject = (b)paramObject;
        Object localObject = a;
        paramObject = a;
        boolean bool2 = k.a(localObject, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    Object localObject = a;
    if (localObject != null) {
      return localObject.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Error(error=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.a.g.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */