package com.daamitt.prime.sdk.a;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class k
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private static final String a = "k";
  public String A;
  public String B;
  public long C;
  public Location D;
  public boolean E;
  public h F;
  public int G;
  public String H;
  public String I;
  public c J;
  public boolean K;
  public boolean L;
  public String M;
  public double N;
  public int O;
  public int P;
  public int Q;
  public String R;
  public double S;
  public long o;
  public String p;
  public String q;
  public Date r;
  public String s;
  String t;
  public int u;
  public int v;
  public int w;
  public int x;
  public String y;
  public boolean z;
  
  static
  {
    k.1 local1 = new com/daamitt/prime/sdk/a/k$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  public k()
  {
    boolean bool = true;
    z = bool;
    L = bool;
    S = Double.MIN_VALUE;
  }
  
  public k(Parcel paramParcel)
  {
    int i = 1;
    z = i;
    L = i;
    S = Double.MIN_VALUE;
    long l1 = paramParcel.readLong();
    o = l1;
    l1 = paramParcel.readLong();
    C = l1;
    Object localObject = paramParcel.readString();
    p = ((String)localObject);
    localObject = paramParcel.readString();
    q = ((String)localObject);
    localObject = new java/util/Date;
    long l2 = paramParcel.readLong();
    ((Date)localObject).<init>(l2);
    r = ((Date)localObject);
    localObject = new android/location/Location;
    ((Location)localObject).<init>("wLocation");
    D = ((Location)localObject);
    localObject = D;
    double d1 = paramParcel.readDouble();
    ((Location)localObject).setLatitude(d1);
    localObject = D;
    d1 = paramParcel.readDouble();
    ((Location)localObject).setLongitude(d1);
    localObject = D;
    float f = paramParcel.readFloat();
    ((Location)localObject).setAccuracy(f);
    localObject = paramParcel.readString();
    M = ((String)localObject);
    double d2 = paramParcel.readDouble();
    N = d2;
    i = paramParcel.readInt();
    O = i;
    i = paramParcel.readInt();
    P = i;
    i = paramParcel.readInt();
    Q = i;
    d2 = paramParcel.readDouble();
    S = d2;
  }
  
  public k(String paramString1, String paramString2, Date paramDate)
  {
    boolean bool = true;
    z = bool;
    L = bool;
    S = Double.MIN_VALUE;
    p = paramString1;
    q = paramString2;
    r = paramDate;
    long l = -1;
    C = l;
    o = l;
    int i = 99;
    u = i;
    v = i;
    w = -1;
    E = false;
  }
  
  public final ArrayList a()
  {
    c localc = J;
    if (localc != null) {
      return a;
    }
    return null;
  }
  
  public final void a(double paramDouble)
  {
    N = paramDouble;
    int i = G | 0x10;
    G = i;
  }
  
  public final void a(String paramString1, String paramString2)
  {
    s = paramString1;
    t = paramString2;
  }
  
  public final boolean b()
  {
    Object localObject = J;
    if (localObject != null)
    {
      localObject = c;
      if (localObject != null)
      {
        localObject = b.iterator();
        c.a locala;
        boolean bool2;
        do
        {
          boolean bool1 = ((Iterator)localObject).hasNext();
          if (!bool1) {
            break;
          }
          locala = (c.a)((Iterator)localObject).next();
          bool2 = f;
        } while (!bool2);
        bool3 = f;
        break label72;
      }
    }
    boolean bool3 = false;
    localObject = null;
    label72:
    return bool3;
  }
  
  public final String c(String paramString)
  {
    paramString = paramString.replaceAll("\\+", "").toCharArray();
    String str1 = "[a-z]*?\\s?";
    String str2 = "[a-z]*";
    String str3 = "(?i)\\b(";
    int i = 0;
    int k;
    int m;
    for (;;)
    {
      int j = paramString.length;
      k = 1;
      if (i >= j) {
        break;
      }
      j = paramString.length - k;
      StringBuilder localStringBuilder;
      if (i < j)
      {
        localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append(str3);
        m = paramString[i];
        localStringBuilder.append(m);
        localStringBuilder.append(str1);
        str3 = localStringBuilder.toString();
      }
      else
      {
        localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append(str3);
        m = paramString[i];
        localStringBuilder.append(m);
        localStringBuilder.append(str2);
        str3 = localStringBuilder.toString();
      }
      i += 1;
    }
    paramString = new java/lang/StringBuilder;
    paramString.<init>();
    paramString.append(str3);
    paramString.append(")");
    paramString = Pattern.compile(paramString.toString());
    str1 = q;
    paramString = paramString.matcher(str1);
    boolean bool = paramString.find();
    str2 = null;
    if (bool)
    {
      str1 = paramString.group(k);
      int n = str1.length();
      m = 20;
      if (n <= m) {
        return paramString.group(k);
      }
      return null;
    }
    return null;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\nnumber :");
    Object localObject2 = p;
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\ndate :");
    localObject2 = r;
    ((StringBuilder)localObject1).append(localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\ncategory :");
    localObject2 = s;
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\nsubCategory :");
    localObject2 = t;
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\nsmsType :");
    int i = u;
    ((StringBuilder)localObject1).append(i);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\naccountType :");
    i = v;
    ((StringBuilder)localObject1).append(i);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\naccountId :");
    i = w;
    ((StringBuilder)localObject1).append(i);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\nparsed :");
    boolean bool = E;
    ((StringBuilder)localObject1).append(bool);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\nUUID :");
    localObject2 = H;
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\nURI :");
    localObject2 = M;
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\nprobability :");
    double d = N;
    ((StringBuilder)localObject1).append(d);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\nbody :");
    localObject2 = q;
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\nuri :");
    localObject2 = M;
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\nthreadID :");
    int j = Q;
    ((StringBuilder)localObject1).append(j);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    long l = o;
    paramParcel.writeLong(l);
    l = C;
    paramParcel.writeLong(l);
    Object localObject = p;
    paramParcel.writeString((String)localObject);
    localObject = q;
    paramParcel.writeString((String)localObject);
    l = r.getTime();
    paramParcel.writeLong(l);
    localObject = D;
    float f;
    if (localObject != null)
    {
      d = ((Location)localObject).getLatitude();
      paramParcel.writeDouble(d);
      d = D.getLongitude();
      paramParcel.writeDouble(d);
      localObject = D;
      f = ((Location)localObject).getAccuracy();
      paramParcel.writeFloat(f);
    }
    else
    {
      l = -4616189618054758400L;
      d = -1.0D;
      paramParcel.writeDouble(d);
      paramParcel.writeDouble(d);
      paramInt = -1082130432;
      f = -1.0F;
      paramParcel.writeFloat(f);
    }
    localObject = M;
    paramParcel.writeString((String)localObject);
    double d = N;
    paramParcel.writeDouble(d);
    paramInt = O;
    paramParcel.writeInt(paramInt);
    paramInt = P;
    paramParcel.writeInt(paramInt);
    paramInt = Q;
    paramParcel.writeInt(paramInt);
    d = S;
    paramParcel.writeDouble(d);
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.a.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */