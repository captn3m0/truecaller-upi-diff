package com.daamitt.prime.sdk.a;

import java.util.Date;

public class l
  extends k
{
  public static final String a = "l";
  public String b;
  public String c;
  public double d;
  public double e = -1.0D;
  public double f;
  public Date g;
  public Date h;
  public int i;
  public int j;
  public String k;
  public int l;
  public String m;
  public m n = null;
  
  public l(String paramString1, String paramString2, Date paramDate)
  {
    super(paramString1, paramString2, paramDate);
  }
  
  public static int a(String paramString)
  {
    String str = "bill";
    boolean bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 1;
    }
    str = "balance";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 2;
    }
    str = "credit_card_bill";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 3;
    }
    str = "mobile_bill";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 4;
    }
    str = "dth_recharge";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 10;
    }
    str = "electricity_bill";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 11;
    }
    str = "insurance_premium";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 12;
    }
    str = "loan_emi";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 13;
    }
    str = "internet_bill";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 14;
    }
    str = "gas_bill";
    bool1 = paramString.equalsIgnoreCase(str);
    if (bool1) {
      return 15;
    }
    str = "default";
    boolean bool2 = paramString.equalsIgnoreCase(str);
    int i1 = 9;
    if (bool2) {
      return i1;
    }
    return i1;
  }
  
  public static String a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return "";
    case 4: 
      return "";
    }
    return "credit";
  }
  
  public final void a(String paramString, double paramDouble, Date paramDate, int paramInt)
  {
    b = paramString;
    d = paramDouble;
    g = paramDate;
    i = paramInt;
  }
  
  public final String b(String paramString)
  {
    int i1 = paramString.hashCode();
    String str;
    boolean bool2;
    switch (i1)
    {
    default: 
      break;
    case 3076014: 
      str = "date";
      boolean bool1 = paramString.equals(str);
      if (bool1) {
        int i2 = 3;
      }
      break;
    case 110749: 
      str = "pan";
      bool2 = paramString.equals(str);
      if (bool2) {
        bool2 = true;
      }
      break;
    case -102973965: 
      str = "sms_time";
      bool2 = paramString.equals(str);
      if (bool2) {
        int i3 = 4;
      }
      break;
    case -264918006: 
      str = "statement_type";
      boolean bool3 = paramString.equals(str);
      if (bool3) {
        int i4 = 2;
      }
      break;
    case -1413853096: 
      str = "amount";
      boolean bool4 = paramString.equals(str);
      if (bool4)
      {
        bool4 = false;
        paramString = null;
      }
      break;
    }
    int i5 = -1;
    switch (i5)
    {
    default: 
      return null;
    case 4: 
      return String.valueOf(r.getTime());
    case 3: 
      return String.valueOf(g.getTime());
    case 2: 
      return String.valueOf(i);
    case 1: 
      return String.valueOf(b);
    }
    return String.valueOf(d);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    Object localObject1 = super.toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\npan : ");
    Object localObject2 = b;
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\nstmtType : ");
    int i1 = i;
    ((StringBuilder)localObject1).append(i1);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\namount : ");
    double d1 = d;
    ((StringBuilder)localObject1).append(d1);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\nminDueAmount : ");
    d1 = e;
    ((StringBuilder)localObject1).append(d1);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\nDueDate : ");
    localObject2 = g;
    ((StringBuilder)localObject1).append(localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("\nflags : ");
    i1 = j;
    ((StringBuilder)localObject1).append(i1);
    localObject1 = ((StringBuilder)localObject1).toString();
    localStringBuilder.append((String)localObject1);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.a.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */