package com.daamitt.prime.sdk;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import com.daamitt.prime.sdk.a.c.a;
import com.daamitt.prime.sdk.a.c.b;
import com.daamitt.prime.sdk.a.h;
import com.daamitt.prime.sdk.a.i;
import com.daamitt.prime.sdk.a.k;
import com.daamitt.prime.sdk.a.l;
import com.daamitt.prime.sdk.a.m;
import com.daamitt.prime.sdk.b.f;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;

public class b
{
  static final String a = "b";
  static b b;
  Context c;
  com.daamitt.prime.sdk.b.b d;
  android.support.v4.content.d e;
  SharedPreferences f;
  String g;
  public String h;
  HashMap i = null;
  
  private b(Context paramContext)
  {
    c = paramContext;
    android.support.v4.content.d locald = android.support.v4.content.d.a(paramContext);
    e = locald;
    paramContext = com.daamitt.prime.sdk.b.b.a(paramContext);
    d = paramContext;
    paramContext = PreferenceManager.getDefaultSharedPreferences(c);
    f = paramContext;
  }
  
  public static b a(Context paramContext)
  {
    synchronized (b.class)
    {
      b localb = b;
      if (localb == null)
      {
        localb = new com/daamitt/prime/sdk/b;
        localb.<init>(paramContext);
        b = localb;
      }
      com.daamitt.prime.sdk.a.e.a();
      paramContext = b;
      return paramContext;
    }
  }
  
  private void a(com.daamitt.prime.sdk.a.a parama)
  {
    SharedPreferences.Editor localEditor = f.edit();
    int j = i;
    int k = 4;
    int i1 = 2;
    int i6 = 1;
    Object localObject1;
    Object localObject2;
    Object localObject3;
    int i2;
    Object localObject5;
    Object localObject6;
    int m;
    if (j == i1)
    {
      localObject1 = d.a(i6);
      localObject2 = d;
      localObject3 = ((com.daamitt.prime.sdk.b.b)localObject2).a(k);
      localObject1 = ((ArrayList)localObject1).iterator();
      for (;;)
      {
        i2 = ((Iterator)localObject1).hasNext();
        if (i2 == 0) {
          break;
        }
        localObject2 = (com.daamitt.prime.sdk.a.a)((Iterator)localObject1).next();
        localObject4 = d;
        localObject5 = "";
        localObject4 = ((String)localObject4).replace("debit", (CharSequence)localObject5).trim();
        localObject6 = d.trim();
        bool5 = TextUtils.equals((CharSequence)localObject6, (CharSequence)localObject4);
        if (bool5)
        {
          bool5 = ((com.daamitt.prime.sdk.a.a)localObject2).e();
          if (!bool5)
          {
            bool5 = parama.e();
            if (!bool5)
            {
              localObject4 = f;
              localObject6 = new java/lang/StringBuilder;
              ((StringBuilder)localObject6).<init>();
              localObject5 = ((com.daamitt.prime.sdk.a.a)localObject2).b();
              ((StringBuilder)localObject6).append((String)localObject5);
              ((StringBuilder)localObject6).append("->");
              localObject5 = parama.b();
              ((StringBuilder)localObject6).append((String)localObject5);
              localObject6 = ((StringBuilder)localObject6).toString();
              bool5 = ((SharedPreferences)localObject4).getBoolean((String)localObject6, false);
              if (!bool5)
              {
                d.a((com.daamitt.prime.sdk.a.a)localObject2, parama);
                com.daamitt.prime.sdk.a.e.b();
                localObject4 = new java/lang/StringBuilder;
                ((StringBuilder)localObject4).<init>();
                localObject2 = ((com.daamitt.prime.sdk.a.a)localObject2).b();
                ((StringBuilder)localObject4).append((String)localObject2);
                ((StringBuilder)localObject4).append("->");
                localObject2 = parama.b();
                ((StringBuilder)localObject4).append((String)localObject2);
                localObject2 = ((StringBuilder)localObject4).toString();
                localEditor.putBoolean((String)localObject2, i6);
              }
            }
          }
        }
      }
      localObject1 = ((ArrayList)localObject3).iterator();
      for (;;)
      {
        m = ((Iterator)localObject1).hasNext();
        if (m == 0) {
          break;
        }
        localObject3 = (com.daamitt.prime.sdk.a.a)((Iterator)localObject1).next();
        localObject2 = d.trim();
        localObject4 = d;
        localObject6 = "billpay";
        localObject5 = "";
        localObject4 = ((String)localObject4).replace((CharSequence)localObject6, (CharSequence)localObject5).trim();
        i2 = TextUtils.equals((CharSequence)localObject2, (CharSequence)localObject4);
        if (i2 != 0)
        {
          i2 = parama.e();
          if (i2 == 0)
          {
            i2 = ((com.daamitt.prime.sdk.a.a)localObject3).e();
            if (i2 == 0)
            {
              localObject2 = f;
              localObject4 = new java/lang/StringBuilder;
              ((StringBuilder)localObject4).<init>();
              localObject6 = ((com.daamitt.prime.sdk.a.a)localObject3).b();
              ((StringBuilder)localObject4).append((String)localObject6);
              ((StringBuilder)localObject4).append("->");
              localObject6 = parama.b();
              ((StringBuilder)localObject4).append((String)localObject6);
              localObject4 = ((StringBuilder)localObject4).toString();
              i2 = ((SharedPreferences)localObject2).getBoolean((String)localObject4, false);
              if (i2 == 0)
              {
                com.daamitt.prime.sdk.a.e.b();
                d.a((com.daamitt.prime.sdk.a.a)localObject3, parama);
                localObject2 = new java/lang/StringBuilder;
                ((StringBuilder)localObject2).<init>();
                localObject3 = ((com.daamitt.prime.sdk.a.a)localObject3).b();
                ((StringBuilder)localObject2).append((String)localObject3);
                ((StringBuilder)localObject2).append("->");
                localObject3 = parama.b();
                ((StringBuilder)localObject2).append((String)localObject3);
                localObject3 = ((StringBuilder)localObject2).toString();
                localEditor.putBoolean((String)localObject3, i6);
              }
            }
          }
        }
      }
    }
    j = i;
    boolean bool5 = false;
    Object localObject4 = null;
    int i3;
    String str;
    boolean bool6;
    if (j == i6)
    {
      localObject1 = d.a(i2).iterator();
      m = 0;
      localObject3 = null;
      for (;;)
      {
        i3 = ((Iterator)localObject1).hasNext();
        if (i3 == 0) {
          break;
        }
        localObject2 = (com.daamitt.prime.sdk.a.a)((Iterator)localObject1).next();
        localObject6 = d;
        str = "";
        localObject6 = ((String)localObject6).replace("debit", str).trim();
        localObject5 = d.trim();
        bool6 = TextUtils.equals((CharSequence)localObject5, (CharSequence)localObject6);
        if (bool6)
        {
          bool6 = ((com.daamitt.prime.sdk.a.a)localObject2).e();
          if (!bool6)
          {
            localObject6 = f;
            localObject5 = new java/lang/StringBuilder;
            ((StringBuilder)localObject5).<init>();
            str = parama.b();
            ((StringBuilder)localObject5).append(str);
            ((StringBuilder)localObject5).append("->");
            str = ((com.daamitt.prime.sdk.a.a)localObject2).b();
            ((StringBuilder)localObject5).append(str);
            localObject5 = ((StringBuilder)localObject5).toString();
            bool6 = ((SharedPreferences)localObject6).getBoolean((String)localObject5, false);
            if (!bool6)
            {
              if (localObject3 != null)
              {
                m = 0;
                localObject3 = null;
                break;
              }
              localObject3 = localObject2;
            }
          }
        }
      }
      if (localObject3 != null)
      {
        d.a(parama, (com.daamitt.prime.sdk.a.a)localObject3);
        com.daamitt.prime.sdk.a.e.b();
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        parama = parama.b();
        ((StringBuilder)localObject1).append(parama);
        ((StringBuilder)localObject1).append("->");
        parama = ((com.daamitt.prime.sdk.a.a)localObject3).b();
        ((StringBuilder)localObject1).append(parama);
        parama = ((StringBuilder)localObject1).toString();
        localEditor.putBoolean(parama, i6);
      }
    }
    else
    {
      j = i;
      int n;
      boolean bool2;
      if (j == m)
      {
        localObject1 = d.a(i3).iterator();
        n = 0;
        localObject3 = null;
        for (;;)
        {
          bool2 = ((Iterator)localObject1).hasNext();
          if (!bool2) {
            break;
          }
          localObject2 = (com.daamitt.prime.sdk.a.a)((Iterator)localObject1).next();
          localObject6 = d;
          str = "";
          localObject6 = ((String)localObject6).replace("billpay", str).trim();
          localObject5 = d.trim();
          bool6 = TextUtils.equals((CharSequence)localObject5, (CharSequence)localObject6);
          if (bool6)
          {
            bool6 = ((com.daamitt.prime.sdk.a.a)localObject2).e();
            if (!bool6)
            {
              localObject6 = f;
              localObject5 = new java/lang/StringBuilder;
              ((StringBuilder)localObject5).<init>();
              str = parama.b();
              ((StringBuilder)localObject5).append(str);
              ((StringBuilder)localObject5).append("->");
              str = ((com.daamitt.prime.sdk.a.a)localObject2).b();
              ((StringBuilder)localObject5).append(str);
              localObject5 = ((StringBuilder)localObject5).toString();
              bool6 = ((SharedPreferences)localObject6).getBoolean((String)localObject5, false);
              if (!bool6)
              {
                if (localObject3 != null)
                {
                  n = 0;
                  localObject3 = null;
                  break;
                }
                localObject3 = localObject2;
              }
            }
          }
        }
        if (localObject3 != null)
        {
          d.a(parama, (com.daamitt.prime.sdk.a.a)localObject3);
          com.daamitt.prime.sdk.a.e.b();
          localObject1 = new java/lang/StringBuilder;
          ((StringBuilder)localObject1).<init>();
          parama = parama.b();
          ((StringBuilder)localObject1).append(parama);
          ((StringBuilder)localObject1).append("->");
          parama = ((com.daamitt.prime.sdk.a.a)localObject3).b();
          ((StringBuilder)localObject1).append(parama);
          parama = ((StringBuilder)localObject1).toString();
          localEditor.putBoolean(parama, i6);
        }
      }
      else
      {
        j = i;
        n = 3;
        if (j == n)
        {
          localObject1 = d.a(n).iterator();
          boolean bool4;
          do
          {
            do
            {
              do
              {
                int i7;
                int i5;
                do
                {
                  boolean bool3;
                  do
                  {
                    do
                    {
                      do
                      {
                        do
                        {
                          int i4;
                          do
                          {
                            do
                            {
                              boolean bool1 = ((Iterator)localObject1).hasNext();
                              if (!bool1) {
                                break label1708;
                              }
                              localObject3 = (com.daamitt.prime.sdk.a.a)((Iterator)localObject1).next();
                              localObject2 = f.toUpperCase();
                              localObject4 = "XXXX";
                              bool2 = TextUtils.equals((CharSequence)localObject2, (CharSequence)localObject4);
                              if (!bool2) {
                                break;
                              }
                              localObject2 = d;
                              localObject4 = d;
                              bool2 = TextUtils.equals((CharSequence)localObject2, (CharSequence)localObject4);
                            } while (!bool2);
                            i4 = a;
                            i7 = a;
                          } while (i4 == i7);
                          bool3 = ((com.daamitt.prime.sdk.a.a)localObject3).e();
                        } while (bool3);
                        bool3 = parama.e();
                      } while (bool3);
                      localObject2 = f;
                      localObject4 = new java/lang/StringBuilder;
                      ((StringBuilder)localObject4).<init>();
                      localObject6 = parama.b();
                      ((StringBuilder)localObject4).append((String)localObject6);
                      ((StringBuilder)localObject4).append("->");
                      localObject6 = ((com.daamitt.prime.sdk.a.a)localObject3).b();
                      ((StringBuilder)localObject4).append((String)localObject6);
                      localObject4 = ((StringBuilder)localObject4).toString();
                      bool3 = ((SharedPreferences)localObject2).getBoolean((String)localObject4, false);
                    } while (bool3);
                    d.a(parama, (com.daamitt.prime.sdk.a.a)localObject3);
                    com.daamitt.prime.sdk.a.e.b();
                    localObject1 = new java/lang/StringBuilder;
                    ((StringBuilder)localObject1).<init>();
                    parama = parama.b();
                    ((StringBuilder)localObject1).append(parama);
                    ((StringBuilder)localObject1).append("->");
                    parama = ((com.daamitt.prime.sdk.a.a)localObject3).b();
                    ((StringBuilder)localObject1).append(parama);
                    parama = ((StringBuilder)localObject1).toString();
                    localEditor.putBoolean(parama, i6);
                    break;
                    localObject2 = d;
                    localObject4 = d;
                    bool3 = TextUtils.equals((CharSequence)localObject2, (CharSequence)localObject4);
                  } while (!bool3);
                  i5 = a;
                  i7 = a;
                } while (i5 == i7);
                bool4 = ((com.daamitt.prime.sdk.a.a)localObject3).e();
              } while (bool4);
              bool4 = parama.e();
            } while (bool4);
            localObject2 = f;
            localObject4 = new java/lang/StringBuilder;
            ((StringBuilder)localObject4).<init>();
            localObject6 = ((com.daamitt.prime.sdk.a.a)localObject3).b();
            ((StringBuilder)localObject4).append((String)localObject6);
            ((StringBuilder)localObject4).append("->");
            localObject6 = parama.b();
            ((StringBuilder)localObject4).append((String)localObject6);
            localObject4 = ((StringBuilder)localObject4).toString();
            bool4 = ((SharedPreferences)localObject2).getBoolean((String)localObject4, false);
          } while (bool4);
          d.a((com.daamitt.prime.sdk.a.a)localObject3, parama);
          com.daamitt.prime.sdk.a.e.b();
          localObject1 = new java/lang/StringBuilder;
          ((StringBuilder)localObject1).<init>();
          localObject3 = ((com.daamitt.prime.sdk.a.a)localObject3).b();
          ((StringBuilder)localObject1).append((String)localObject3);
          localObject3 = "->";
          ((StringBuilder)localObject1).append((String)localObject3);
          parama = parama.b();
          ((StringBuilder)localObject1).append(parama);
          parama = ((StringBuilder)localObject1).toString();
          localEditor.putBoolean(parama, i6);
        }
      }
    }
    label1708:
    localEditor.apply();
  }
  
  private void a(com.daamitt.prime.sdk.a.d paramd1, com.daamitt.prime.sdk.a.d paramd2)
  {
    com.daamitt.prime.sdk.a.e.a();
    Object localObject1 = J;
    int j = 4;
    int k = 1;
    boolean bool1 = false;
    Object localObject2 = null;
    boolean bool7;
    if (localObject1 != null)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        localIterator = a.iterator();
        boolean bool2;
        Object localObject3;
        do
        {
          bool2 = localIterator.hasNext();
          if (!bool2) {
            break;
          }
          localObject3 = (c.a)localIterator.next();
          bool7 = f;
        } while (!bool7);
        bool8 = f;
        break label96;
        bool8 = false;
        localIterator = null;
        label96:
        localObject1 = b.iterator();
        for (;;)
        {
          bool2 = ((Iterator)localObject1).hasNext();
          if (!bool2) {
            break;
          }
          localObject3 = (c.a)((Iterator)localObject1).next();
          bool7 = f;
          if (bool7)
          {
            bool1 = f;
            break;
          }
          Object localObject4 = a;
          localObject4 = paramd2.b((String)localObject4);
          localObject3 = b;
          boolean bool9 = TextUtils.isEmpty((CharSequence)localObject4);
          if (!bool9)
          {
            com.daamitt.prime.sdk.a.e.a();
            int i4 = -1;
            int i5 = ((String)localObject3).hashCode();
            String str1;
            boolean bool3;
            switch (i5)
            {
            default: 
              break;
            case 984038195: 
              str1 = "event_info";
              bool2 = ((String)localObject3).equals(str1);
              if (bool2) {
                int m = 2;
              }
              break;
            case 951526432: 
              str1 = "contact";
              bool3 = ((String)localObject3).equals(str1);
              if (bool3) {
                bool3 = true;
              }
              break;
            case 943500218: 
              str1 = "event_location";
              bool3 = ((String)localObject3).equals(str1);
              if (bool3) {
                int n = 3;
              }
              break;
            case 3076014: 
              str1 = "date";
              boolean bool4 = ((String)localObject3).equals(str1);
              if (bool4) {
                int i1 = 5;
              }
              break;
            case 111156: 
              str1 = "pnr";
              boolean bool5 = ((String)localObject3).equals(str1);
              if (bool5) {
                int i2 = 4;
              }
              break;
            case -1413853096: 
              str1 = "amount";
              boolean bool6 = ((String)localObject3).equals(str1);
              if (bool6)
              {
                bool6 = false;
                localObject3 = null;
              }
              break;
            }
            int i3 = -1;
            switch (i3)
            {
            default: 
              break;
            case 5: 
              localObject3 = new java/util/Date;
              localObject4 = Long.valueOf((String)localObject4);
              long l1 = ((Long)localObject4).longValue();
              ((Date)localObject3).<init>(l1);
              d = ((Date)localObject3);
              break;
            case 4: 
              c = ((String)localObject4);
              break;
            case 3: 
              f = ((String)localObject4);
              break;
            case 2: 
              g = ((String)localObject4);
              break;
            case 1: 
              j = ((String)localObject4);
              break;
            case 0: 
              localObject3 = Double.valueOf((String)localObject4);
              double d1 = ((Double)localObject3).doubleValue();
              i = d1;
            }
          }
        }
      }
    }
    boolean bool8 = false;
    Iterator localIterator = null;
    int i6;
    if (bool1) {
      i6 = h | k;
    } else {
      i6 = h & 0xFFFFFFFE;
    }
    h = i6;
    if (bool8)
    {
      i6 = h | j | k;
      h = i6;
      Object localObject5 = new android/content/ContentValues;
      ((ContentValues)localObject5).<init>();
      String str2 = "flags";
      localObject1 = Integer.valueOf(i6);
      ((ContentValues)localObject5).put(str2, (Integer)localObject1);
      localObject1 = d.e;
      long l2 = o;
      long l3 = 0L;
      bool7 = l2 < l3;
      if (!bool7)
      {
        str2 = com.daamitt.prime.sdk.b.c.a;
        com.daamitt.prime.sdk.a.e.a();
        localObject1 = b;
        str2 = "events";
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("_id = ");
        l3 = o;
        ((StringBuilder)localObject2).append(l3);
        localObject2 = ((StringBuilder)localObject2).toString();
        bool8 = false;
        localIterator = null;
        ((SQLiteDatabase)localObject1).update(str2, (ContentValues)localObject5, (String)localObject2, null);
      }
      else
      {
        localObject1 = com.daamitt.prime.sdk.b.c.a;
        com.daamitt.prime.sdk.a.e.a();
      }
      long l4 = C;
      paramd2 = d.a(l4);
      localObject1 = new android/content/ContentValues;
      ((ContentValues)localObject1).<init>();
      localObject5 = "previousUUID";
      paramd2 = H;
      ((ContentValues)localObject1).put((String)localObject5, paramd2);
      paramd2 = d;
      long l5 = C;
      paramd2.a(l5, (ContentValues)localObject1);
    }
  }
  
  private void a(l paraml1, l paraml2)
  {
    Object localObject1 = J;
    int j = 2;
    int k = 0;
    Object localObject2 = null;
    if (localObject1 != null)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        Iterator localIterator = a.iterator();
        bool1 = false;
        boolean bool2;
        Object localObject3;
        boolean bool5;
        for (;;)
        {
          bool2 = localIterator.hasNext();
          if (!bool2) {
            break;
          }
          localObject3 = (c.a)localIterator.next();
          bool5 = f;
          if (bool5) {
            bool1 = f;
          }
        }
        localObject1 = b;
        if (localObject1 == null) {
          break label491;
        }
        localObject1 = ((ArrayList)localObject1).iterator();
        boolean bool6 = false;
        localIterator = null;
        for (;;)
        {
          bool2 = ((Iterator)localObject1).hasNext();
          if (!bool2) {
            break;
          }
          localObject3 = (c.a)((Iterator)localObject1).next();
          bool5 = f;
          if (bool5) {
            bool6 = f;
          }
          Object localObject4 = a;
          localObject4 = paraml2.b((String)localObject4);
          localObject3 = b;
          boolean bool7 = TextUtils.isEmpty((CharSequence)localObject4);
          if (!bool7)
          {
            com.daamitt.prime.sdk.a.e.a();
            int i2 = -1;
            int i3 = ((String)localObject3).hashCode();
            int i4 = -1413853096;
            String str;
            if (i3 != i4)
            {
              i4 = -264918006;
              boolean bool3;
              if (i3 != i4)
              {
                i4 = 110749;
                if (i3 != i4)
                {
                  i4 = 3076014;
                  if (i3 == i4)
                  {
                    str = "date";
                    bool2 = ((String)localObject3).equals(str);
                    if (bool2)
                    {
                      int m = 3;
                      break label356;
                    }
                  }
                }
                else
                {
                  str = "pan";
                  bool3 = ((String)localObject3).equals(str);
                  if (bool3)
                  {
                    bool3 = true;
                    break label356;
                  }
                }
              }
              else
              {
                str = "statement_type";
                bool3 = ((String)localObject3).equals(str);
                if (bool3)
                {
                  int n = 2;
                  break label356;
                }
              }
            }
            else
            {
              str = "amount";
              boolean bool4 = ((String)localObject3).equals(str);
              if (bool4)
              {
                bool4 = false;
                localObject3 = null;
                break label356;
              }
            }
            int i1 = -1;
            switch (i1)
            {
            default: 
              break;
            case 3: 
              localObject3 = new java/util/Date;
              localObject4 = Long.valueOf((String)localObject4);
              long l1 = ((Long)localObject4).longValue();
              ((Date)localObject3).<init>(l1);
              g = ((Date)localObject3);
              break;
            case 2: 
              localObject3 = Integer.valueOf((String)localObject4);
              i1 = ((Integer)localObject3).intValue();
              i = i1;
              break;
            case 1: 
              b = ((String)localObject4);
              break;
            case 0: 
              label356:
              localObject3 = Double.valueOf((String)localObject4);
              double d1 = ((Double)localObject3).doubleValue();
              d = d1;
            }
          }
        }
        k = bool6;
        break label491;
      }
    }
    boolean bool1 = false;
    label491:
    int i5;
    if (k != 0) {
      i5 = j | j;
    } else {
      i5 = j & 0xFFFFFFFD;
    }
    j = i5;
    ContentValues localContentValues;
    if (bool1)
    {
      i5 |= j;
      j = i5;
      localContentValues = new android/content/ContentValues;
      localContentValues.<init>();
      localObject2 = "status";
      localObject1 = Integer.valueOf(i5);
      localContentValues.put((String)localObject2, (Integer)localObject1);
      localObject1 = d;
      ((com.daamitt.prime.sdk.b.b)localObject1).a(paraml2, localContentValues);
    }
    else
    {
      i5 &= 0xFFFFFFFD;
      j = i5;
      localContentValues = new android/content/ContentValues;
      localContentValues.<init>();
      localObject1 = Integer.valueOf(i5);
      localContentValues.put("status", (Integer)localObject1);
      long l2 = C;
      localObject2 = Long.valueOf(l2);
      localContentValues.put("wSmsId", (Long)localObject2);
      localObject1 = d;
      ((com.daamitt.prime.sdk.b.b)localObject1).a(paraml2, localContentValues);
    }
    long l3 = C;
    paraml2 = d.a(l3);
    localObject1 = new android/content/ContentValues;
    ((ContentValues)localObject1).<init>();
    paraml2 = H;
    ((ContentValues)localObject1).put("previousUUID", paraml2);
    paraml2 = d;
    long l4 = C;
    paraml2.a(l4, (ContentValues)localObject1);
  }
  
  private void a(m paramm1, m paramm2)
  {
    com.daamitt.prime.sdk.a.e.a();
    Object localObject1 = J;
    int j = 0;
    ContentValues localContentValues = null;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        localObject2 = a.iterator();
        bool1 = false;
        bool2 = false;
        boolean bool4;
        for (;;)
        {
          bool3 = ((Iterator)localObject2).hasNext();
          if (!bool3) {
            break;
          }
          locala = (c.a)((Iterator)localObject2).next();
          bool4 = f;
          if (bool4) {
            bool1 = f;
          }
          bool4 = g;
          if (bool4) {
            bool2 = g;
          }
        }
        localObject1 = b;
        if (localObject1 != null)
        {
          localObject1 = ((ArrayList)localObject1).iterator();
          boolean bool9 = false;
          localObject2 = null;
          bool3 = false;
          locala = null;
          for (;;)
          {
            bool4 = ((Iterator)localObject1).hasNext();
            if (!bool4) {
              break;
            }
            Object localObject3 = (c.a)((Iterator)localObject1).next();
            boolean bool10 = f;
            if (bool10) {
              bool9 = f;
            }
            bool10 = g;
            if (bool10) {
              bool3 = g;
            }
            Object localObject4 = a;
            localObject4 = paramm2.b((String)localObject4);
            localObject3 = b;
            boolean bool11 = TextUtils.isEmpty((CharSequence)localObject4);
            if (!bool11)
            {
              com.daamitt.prime.sdk.a.e.a();
              int i3 = -1;
              int i4 = ((String)localObject3).hashCode();
              String str;
              boolean bool5;
              switch (i4)
              {
              default: 
                break;
              case 509054971: 
                str = "transaction_type";
                bool4 = ((String)localObject3).equals(str);
                if (bool4) {
                  int k = 4;
                }
                break;
              case 3387378: 
                str = "note";
                bool5 = ((String)localObject3).equals(str);
                if (bool5) {
                  bool5 = true;
                }
                break;
              case 3076014: 
                str = "date";
                bool5 = ((String)localObject3).equals(str);
                if (bool5) {
                  int m = 5;
                }
                break;
              case 111188: 
                str = "pos";
                boolean bool6 = ((String)localObject3).equals(str);
                if (bool6) {
                  int n = 2;
                }
                break;
              case 110749: 
                str = "pan";
                boolean bool7 = ((String)localObject3).equals(str);
                if (bool7) {
                  int i1 = 3;
                }
                break;
              case -1413853096: 
                str = "amount";
                boolean bool8 = ((String)localObject3).equals(str);
                if (bool8)
                {
                  bool8 = false;
                  localObject3 = null;
                }
                break;
              }
              int i2 = -1;
              switch (i2)
              {
              default: 
                break;
              case 5: 
                localObject3 = new java/util/Date;
                localObject4 = Long.valueOf((String)localObject4);
                long l1 = ((Long)localObject4).longValue();
                ((Date)localObject3).<init>(l1);
                ad = ((Date)localObject3);
                break;
              case 4: 
                localObject3 = Integer.valueOf((String)localObject4);
                i2 = ((Integer)localObject3).intValue();
                j = i2;
                break;
              case 3: 
                b = ((String)localObject4);
                break;
              case 2: 
                c = ((String)localObject4);
                m = ((String)localObject4);
                break;
              case 1: 
                V = ((String)localObject4);
                break;
              case 0: 
                localObject3 = Double.valueOf((String)localObject4);
                double d1 = ((Double)localObject3).doubleValue();
                f = d1;
              }
            }
          }
          j = bool9;
          break label649;
        }
        bool3 = false;
        locala = null;
        break label649;
      }
    }
    boolean bool1 = false;
    boolean bool2 = false;
    boolean bool3 = false;
    c.a locala = null;
    label649:
    int i5;
    if (j != 0) {
      i5 = Y | 0x10;
    } else {
      i5 = Y & 0xFFFFFFEF;
    }
    if (bool3) {
      i5 |= 0x100;
    } else {
      i5 &= 0xFEFF;
    }
    Y = i5;
    if (bool2) {
      i5 = Y | 0x100;
    } else {
      i5 = Y & 0xFEFF;
    }
    if (bool1)
    {
      i5 |= 0x10;
      Y = i5;
      localContentValues = new android/content/ContentValues;
      localContentValues.<init>();
      localObject2 = "flags";
      localObject1 = Integer.valueOf(i5);
      localContentValues.put((String)localObject2, (Integer)localObject1);
      localObject1 = d;
      ((com.daamitt.prime.sdk.b.b)localObject1).a(paramm2, localContentValues);
    }
    else
    {
      i5 &= 0xFFFFFFEF;
      Y = i5;
      localContentValues = new android/content/ContentValues;
      localContentValues.<init>();
      localObject1 = Integer.valueOf(i5);
      localContentValues.put("flags", (Integer)localObject1);
      long l2 = C;
      localObject2 = Long.valueOf(l2);
      localContentValues.put("wSmsId", (Long)localObject2);
      localObject1 = d;
      ((com.daamitt.prime.sdk.b.b)localObject1).a(paramm2, localContentValues);
    }
    long l3 = C;
    paramm2 = d.a(l3);
    localObject1 = new android/content/ContentValues;
    ((ContentValues)localObject1).<init>();
    paramm2 = H;
    ((ContentValues)localObject1).put("previousUUID", paramm2);
    paramm2 = d;
    long l4 = C;
    paramm2.a(l4, (ContentValues)localObject1);
  }
  
  public final void a(String paramString)
  {
    b localb = this;
    com.daamitt.prime.sdk.a.e.a();
    Object localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    i = ((HashMap)localObject1);
    localObject1 = new org/json/JSONObject;
    Object localObject2 = paramString;
    ((JSONObject)localObject1).<init>(paramString);
    localObject2 = ((JSONObject)localObject1).getString("version");
    h = ((String)localObject2);
    com.daamitt.prime.sdk.a.e.b();
    localObject2 = ((JSONObject)localObject1).getJSONArray("rules");
    localObject1 = ((JSONObject)localObject1).optString("blacklist_regex");
    g = ((String)localObject1);
    int j = 0;
    for (;;)
    {
      int k = ((JSONArray)localObject2).length();
      if (j >= k) {
        break;
      }
      JSONObject localJSONObject1 = ((JSONArray)localObject2).getJSONObject(j);
      Object localObject3 = localJSONObject1.optString("name", "Unknown");
      String str1 = "sender_UID";
      long l1 = localJSONObject1.getLong(str1);
      String str2 = "set_account_as_expense";
      boolean bool1 = true;
      boolean bool2 = localJSONObject1.optBoolean(str2, bool1);
      JSONArray localJSONArray = localJSONObject1.getJSONArray("patterns");
      JSONObject localJSONObject2 = localJSONObject1.optJSONObject("sms_preprocessor");
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      int m = 0;
      for (;;)
      {
        int n = localJSONArray.length();
        if (m >= n) {
          break;
        }
        JSONObject localJSONObject3 = localJSONArray.getJSONObject(m);
        h localh = new com/daamitt/prime/sdk/a/h;
        localh.<init>();
        String str3 = localJSONObject3.getString("regex");
        localObject1 = "\\\\";
        boolean bool3 = str3.contains((CharSequence)localObject1);
        if (bool3)
        {
          localObject4 = localObject2;
          localObject2 = "\\";
          localObject1 = str3.replaceAll("\\\\", (String)localObject2);
          a = ((String)localObject1);
        }
        else
        {
          localObject4 = localObject2;
          a = str3;
        }
        localObject1 = Pattern.compile(str3);
        b = ((Pattern)localObject1);
        localObject1 = localJSONObject3.getString("account_type");
        c = ((String)localObject1);
        localObject1 = localJSONObject3.getString("sms_type");
        d = ((String)localObject1);
        localObject1 = localJSONObject3.optString("account_name_override");
        f = ((String)localObject1);
        long l2 = localJSONObject3.getLong("pattern_UID");
        j = l2;
        l2 = localJSONObject3.getLong("sort_UID");
        k = l2;
        l = l1;
        localObject2 = null;
        bool3 = localJSONObject3.optBoolean("reparse", false);
        m = bool3;
        localObject1 = localJSONObject3.optJSONObject("data_fields");
        g = ((JSONObject)localObject1);
        h = localJSONObject2;
        e = ((String)localObject3);
        localObject1 = "set_account_as_expense";
        bool3 = localJSONObject3.has((String)localObject1);
        if (bool3)
        {
          localObject1 = "set_account_as_expense";
          bool1 = true;
          bool3 = localJSONObject3.optBoolean((String)localObject1, bool1);
          i = bool3;
        }
        else
        {
          bool1 = true;
          i = bool2;
        }
        localArrayList.add(localh);
        m += 1;
        localObject2 = localObject4;
      }
      Object localObject4 = localObject2;
      localObject2 = null;
      localObject1 = -..Lambda.b.PEB0H7o8fLABaP65zRnHEKlTBfo.INSTANCE;
      Collections.sort(localArrayList, (Comparator)localObject1);
      localObject1 = localJSONObject1.getJSONArray("senders");
      k = 0;
      localJSONObject1 = null;
      for (;;)
      {
        int i1 = ((JSONArray)localObject1).length();
        if (k >= i1) {
          break;
        }
        localObject3 = i;
        str1 = ((JSONArray)localObject1).getString(k);
        ((HashMap)localObject3).put(str1, localArrayList);
        k += 1;
      }
      j += 1;
      localObject2 = localObject4;
    }
  }
  
  final void a(String paramString1, String paramString2, Date paramDate, long paramLong, int paramInt, String paramString3)
  {
    b localb = this;
    Object localObject1 = i;
    Object localObject2 = g;
    Object localObject3 = paramString1;
    Object localObject4 = paramString2;
    Object localObject5 = paramDate;
    localObject1 = a.a(paramString1, paramString2, paramDate, (HashMap)localObject1, (String)localObject2);
    if (localObject1 != null)
    {
      boolean bool1 = ((ArrayList)localObject1).isEmpty();
      if (!bool1)
      {
        localObject1 = ((ArrayList)localObject1).iterator();
        for (;;)
        {
          bool1 = ((Iterator)localObject1).hasNext();
          if (!bool1) {
            break;
          }
          localObject2 = (k)((Iterator)localObject1).next();
          if (localObject2 != null)
          {
            localObject3 = paramString3;
            M = paramString3;
            int i4 = paramInt;
            Q = paramInt;
            localObject5 = s;
            if (localObject5 != null)
            {
              long l1 = paramLong;
              C = paramLong;
              int i8 = v;
              int i9 = 99;
              Object localObject6;
              if (i8 == i9)
              {
                localObject6 = d;
                localObject7 = s;
                localObject8 = "Messages";
                localObject6 = ((com.daamitt.prime.sdk.b.b)localObject6).a((String)localObject7, (String)localObject8, i9);
              }
              else
              {
                localObject6 = d;
                localObject9 = s;
                localObject7 = "Messages";
                int i10 = 9;
                localObject6 = ((com.daamitt.prime.sdk.b.b)localObject6).a((String)localObject9, (String)localObject7, i10);
              }
              i9 = a;
              w = i9;
              Object localObject9 = new android/content/ContentValues;
              ((ContentValues)localObject9).<init>();
              Object localObject7 = d.d;
              Object localObject8 = new android/content/ContentValues;
              ((ContentValues)localObject8).<init>();
              Object localObject10 = p;
              ((ContentValues)localObject8).put("sender", (String)localObject10);
              localObject10 = Long.valueOf(r.getTime());
              ((ContentValues)localObject8).put("date", (Long)localObject10);
              localObject10 = q;
              ((ContentValues)localObject8).put("body", (String)localObject10);
              Object localObject11 = "smsId";
              long l2 = C;
              localObject10 = Long.valueOf(l2);
              ((ContentValues)localObject8).put((String)localObject11, (Long)localObject10);
              if (localObject6 != null)
              {
                localObject11 = "accountId";
                i11 = a;
                localObject10 = Integer.valueOf(i11);
                ((ContentValues)localObject8).put((String)localObject11, (Integer)localObject10);
              }
              else
              {
                localObject11 = "accountId";
                i11 = w;
                localObject10 = Integer.valueOf(i11);
                ((ContentValues)localObject8).put((String)localObject11, (Integer)localObject10);
              }
              int i11 = G;
              localObject10 = Integer.valueOf(i11);
              ((ContentValues)localObject8).put("smsFlags", (Integer)localObject10);
              localObject10 = Boolean.FALSE;
              ((ContentValues)localObject8).put("parsed", (Boolean)localObject10);
              localObject11 = D;
              float f1;
              if (localObject11 != null)
              {
                localObject12 = Double.valueOf(((Location)localObject11).getLatitude());
                ((ContentValues)localObject8).put("lat", (Double)localObject12);
                d1 = ((Location)localObject11).getLongitude();
                localObject12 = Double.valueOf(d1);
                ((ContentValues)localObject8).put("long", (Double)localObject12);
                localObject10 = "locAccuracy";
                f1 = ((Location)localObject11).getAccuracy();
                localObject11 = Float.valueOf(f1);
                ((ContentValues)localObject8).put((String)localObject10, (Float)localObject11);
              }
              localObject11 = UUID.randomUUID().toString();
              H = ((String)localObject11);
              localObject10 = H;
              ((ContentValues)localObject8).put("UUID", (String)localObject10);
              localObject11 = F;
              long l3;
              long l4;
              boolean bool11;
              if (localObject11 != null)
              {
                localObject11 = F;
                l3 = j;
                l4 = 0L;
                d1 = 0.0D;
                bool11 = l3 < l4;
                if (bool11)
                {
                  localObject11 = "patternUID";
                  l2 = F.j;
                  localObject10 = Long.valueOf(l2);
                  ((ContentValues)localObject8).put((String)localObject11, (Long)localObject10);
                }
              }
              localObject11 = "URI";
              localObject10 = M;
              ((ContentValues)localObject8).put((String)localObject11, (String)localObject10);
              int i14 = G;
              i11 = 16;
              i14 &= i11;
              if (i14 == i11)
              {
                i14 = 1;
                f1 = Float.MIN_VALUE;
              }
              else
              {
                i14 = 0;
                f1 = 0.0F;
                localObject11 = null;
              }
              if (i14 != 0)
              {
                localObject11 = "probability";
                d1 = N;
                localObject12 = Double.valueOf(d1);
                ((ContentValues)localObject8).put((String)localObject11, (Double)localObject12);
              }
              Object localObject12 = Integer.valueOf(O);
              ((ContentValues)localObject8).put("simSubscriptionId", (Integer)localObject12);
              localObject12 = Integer.valueOf(P);
              ((ContentValues)localObject8).put("simSlotId", (Integer)localObject12);
              int i19 = Q;
              localObject12 = Integer.valueOf(i19);
              ((ContentValues)localObject8).put("threadId", (Integer)localObject12);
              localObject11 = "creator";
              localObject12 = R;
              ((ContentValues)localObject8).put((String)localObject11, (String)localObject12);
              double d1 = S;
              long l5 = 1L;
              double d2 = Double.MIN_VALUE;
              boolean bool12 = d1 < d2;
              if (bool12)
              {
                localObject11 = "metaData";
                d1 = S;
                localObject12 = Double.valueOf(d1);
                ((ContentValues)localObject8).put((String)localObject11, (Double)localObject12);
              }
              localObject7 = a;
              localObject11 = "sms";
              i19 = 0;
              localObject12 = null;
              long l6 = ((SQLiteDatabase)localObject7).insert((String)localObject11, null, (ContentValues)localObject8);
              o = l6;
              l6 = o;
              bool12 = localObject2 instanceof m;
              Object localObject13;
              Object localObject14;
              Object localObject15;
              int i22;
              int i23;
              label1405:
              int i13;
              double d5;
              int i5;
              boolean bool15;
              boolean bool20;
              label2488:
              long l8;
              if (bool12)
              {
                localObject6 = localObject2;
                localObject6 = (m)localObject2;
                long l7 = o;
                C = l7;
                bool1 = K;
                if (bool1)
                {
                  localObject2 = ((m)localObject6).a();
                  localObject11 = d;
                  localObject13 = s;
                  localObject11 = a.a((String)localObject13);
                  localObject13 = d.b;
                  localObject2 = ((f)localObject13).a((ArrayList)localObject11, (ArrayList)localObject2, (m)localObject6);
                  if (localObject2 != null)
                  {
                    localb.a((m)localObject6, (m)localObject2);
                  }
                  else
                  {
                    bool1 = ((m)localObject6).b();
                    if (bool1)
                    {
                      j = Y | i11;
                      Y = j;
                    }
                    else
                    {
                      j = Y & 0xFFFFFFEF;
                      Y = j;
                    }
                  }
                }
                int j = j;
                int i15 = 17;
                f1 = 2.4E-44F;
                int i20 = 12;
                if (j != i20)
                {
                  j = j;
                  if (j != i15)
                  {
                    localObject14 = d;
                    localObject2 = new java/lang/StringBuilder;
                    ((StringBuilder)localObject2).<init>();
                    localObject10 = s;
                    ((StringBuilder)localObject2).append((String)localObject10);
                    localObject10 = " ";
                    ((StringBuilder)localObject2).append((String)localObject10);
                    i11 = j;
                    i15 = v;
                    localObject11 = m.a(i11, i15);
                    ((StringBuilder)localObject2).append((String)localObject11);
                    localObject15 = ((StringBuilder)localObject2).toString();
                    localObject2 = b;
                    i15 = v;
                    i12 = L;
                    localObject13 = B;
                    i22 = i15;
                    localObject2 = ((com.daamitt.prime.sdk.b.b)localObject14).a((String)localObject15, (String)localObject2, i15, i12, (String)localObject13);
                    break label1405;
                  }
                }
                localObject13 = d;
                localObject2 = new java/lang/StringBuilder;
                ((StringBuilder)localObject2).<init>();
                localObject11 = s;
                ((StringBuilder)localObject2).append((String)localObject11);
                ((StringBuilder)localObject2).append(" ");
                localObject11 = com.daamitt.prime.sdk.a.a.a(v);
                ((StringBuilder)localObject2).append((String)localObject11);
                localObject14 = ((StringBuilder)localObject2).toString();
                localObject2 = b;
                i15 = v;
                int i12 = L;
                localObject12 = B;
                localObject15 = localObject2;
                i23 = i15;
                i22 = i12;
                localObject2 = ((com.daamitt.prime.sdk.b.b)localObject13).a((String)localObject14, (String)localObject2, i15, i12, (String)localObject12);
                boolean bool13 = s;
                if (bool13) {
                  localb.a((com.daamitt.prime.sdk.a.a)localObject2);
                }
                int i16 = a;
                w = i16;
                boolean bool14 = k;
                z = bool14;
                localObject11 = ((com.daamitt.prime.sdk.a.a)localObject2).c();
                d = ((String)localObject11);
                localObject11 = ((com.daamitt.prime.sdk.a.a)localObject2).a();
                y = ((String)localObject11);
                bool14 = k;
                if (!bool14)
                {
                  int i17 = Y | 0x8;
                  Y = i17;
                }
                localObject11 = i;
                boolean bool16;
                if (localObject11 != null)
                {
                  localObject11 = d;
                  i13 = a;
                  l2 = i13;
                  localObject11 = ((com.daamitt.prime.sdk.b.b)localObject11).b(l2);
                  if (localObject11 == null) {
                    localObject11 = localObject2;
                  }
                  localObject2 = m;
                  boolean bool2;
                  if (localObject2 != null)
                  {
                    localObject2 = r;
                    localObject10 = m.c;
                    bool16 = com.daamitt.prime.sdk.a.b.a((Date)localObject2, (Date)localObject10);
                    localObject2 = r;
                    localObject10 = m.d;
                    bool2 = com.daamitt.prime.sdk.a.b.a((Date)localObject2, (Date)localObject10);
                  }
                  else
                  {
                    bool2 = true;
                    bool16 = true;
                  }
                  localObject10 = new com/daamitt/prime/sdk/a/b;
                  ((com.daamitt.prime.sdk.a.b)localObject10).<init>();
                  localObject12 = new android/content/ContentValues;
                  ((ContentValues)localObject12).<init>();
                  if (bool16)
                  {
                    double d3 = i.a;
                    a = d3;
                    localObject13 = r;
                    c = ((Date)localObject13);
                  }
                  else
                  {
                    a = d2;
                  }
                  if (bool2)
                  {
                    double d4 = i.b;
                    b = d4;
                    localObject2 = r;
                    d = ((Date)localObject2);
                  }
                  else
                  {
                    b = d2;
                  }
                  m = ((com.daamitt.prime.sdk.a.b)localObject10);
                  localObject2 = m;
                  com.daamitt.prime.sdk.b.a.a((ContentValues)localObject12, (com.daamitt.prime.sdk.a.b)localObject2);
                  k = ((ContentValues)localObject12).size();
                  if (k > 0)
                  {
                    localObject2 = "updatedTime";
                    l5 = System.currentTimeMillis();
                    localObject3 = Long.valueOf(l5);
                    ((ContentValues)localObject12).put((String)localObject2, (Long)localObject3);
                    k = l;
                    localObject3 = "balance";
                    boolean bool17 = ((ContentValues)localObject12).containsKey((String)localObject3);
                    if (bool17) {
                      k &= 0xFFFFFFFB;
                    } else {
                      k |= 0x4;
                    }
                    localObject3 = "outstandingBalance";
                    bool17 = ((ContentValues)localObject12).containsKey((String)localObject3);
                    if (bool17) {
                      k &= 0xFFFFFFF7;
                    } else {
                      k |= 0x8;
                    }
                    l = k;
                    localObject3 = "flags";
                    localObject2 = Integer.valueOf(k);
                    ((ContentValues)localObject12).put((String)localObject3, (Integer)localObject2);
                    localObject2 = d;
                    ((com.daamitt.prime.sdk.b.b)localObject2).a((com.daamitt.prime.sdk.a.a)localObject11, (ContentValues)localObject12);
                  }
                  else
                  {
                    k = 0;
                    localObject2 = null;
                    i = null;
                  }
                }
                else
                {
                  localObject3 = d;
                  i4 = a;
                  l3 = i4;
                  localObject3 = ((com.daamitt.prime.sdk.b.b)localObject3).b(l3);
                  if (localObject3 != null) {
                    localObject2 = localObject3;
                  }
                  i24 = l;
                  localObject4 = m;
                  boolean bool8;
                  if (localObject4 != null)
                  {
                    localObject4 = ad;
                    localObject11 = m.c;
                    bool16 = com.daamitt.prime.sdk.a.b.a((Date)localObject4, (Date)localObject11);
                    localObject4 = ad;
                    localObject11 = m.d;
                    bool8 = com.daamitt.prime.sdk.a.b.a((Date)localObject4, (Date)localObject11);
                  }
                  else
                  {
                    bool8 = true;
                    bool16 = true;
                  }
                  if (bool16) {
                    i24 |= 0x4;
                  }
                  if (bool8) {
                    i24 |= 0x8;
                  }
                  l = i24;
                  localObject4 = new android/content/ContentValues;
                  ((ContentValues)localObject4).<init>();
                  l2 = System.currentTimeMillis();
                  localObject10 = Long.valueOf(l2);
                  ((ContentValues)localObject4).put("updatedTime", (Long)localObject10);
                  localObject11 = "flags";
                  localObject3 = Integer.valueOf(i24);
                  ((ContentValues)localObject4).put((String)localObject11, (Integer)localObject3);
                  localObject3 = d;
                  ((com.daamitt.prime.sdk.b.b)localObject3).a((com.daamitt.prime.sdk.a.a)localObject2, (ContentValues)localObject4);
                }
                int k = j;
                int i24 = 12;
                if (k != i24)
                {
                  k = j;
                  i24 = 17;
                  if (k != i24) {}
                }
                else
                {
                  k = Y;
                  i24 = 16;
                  k |= i24;
                  Y = k;
                }
                localObject2 = T;
                boolean bool18 = TextUtils.isEmpty((CharSequence)localObject2);
                if (!bool18)
                {
                  bool18 = ac;
                  if (!bool18)
                  {
                    int i25 = j;
                    l3 = 4666723172467343360L;
                    d5 = 10000.0D;
                    i5 = 4;
                    if (i25 != i5)
                    {
                      i25 = j;
                      i19 = 18;
                      if (i25 != i19) {}
                    }
                    else
                    {
                      d1 = f;
                      boolean bool19 = d1 < d5;
                      if (bool19)
                      {
                        ((m)localObject6).n();
                        i26 = j;
                        if (i26 != i5) {
                          break label2488;
                        }
                        localObject3 = "walnut_bills";
                        T = ((String)localObject3);
                        break label2488;
                      }
                    }
                    int i26 = j;
                    if (i26 != i5)
                    {
                      i26 = j;
                      i5 = 18;
                      if (i26 != i5) {}
                    }
                    else
                    {
                      d2 = f;
                      bool15 = d2 < d5;
                      if (!bool15)
                      {
                        ((m)localObject6).m();
                        localObject3 = "walnut_transfer";
                        T = ((String)localObject3);
                        break label2488;
                      }
                    }
                    i26 = j;
                    i5 = 5;
                    if (i26 == i5)
                    {
                      ((m)localObject6).n();
                    }
                    else
                    {
                      bool20 = m.b(j);
                      if (bool20)
                      {
                        localObject3 = "other";
                        bool20 = ((String)localObject2).equals(localObject3);
                        if (bool20) {
                          ((m)localObject6).m();
                        }
                      }
                    }
                  }
                  bool20 = m.a(j);
                  if (bool20)
                  {
                    localObject3 = "other";
                    boolean bool3 = ((String)localObject2).equals(localObject3);
                    if (bool3)
                    {
                      localObject2 = "walnut_bills";
                      T = ((String)localObject2);
                    }
                  }
                }
                l8 = d.b.a((m)localObject6);
                o = l8;
                localObject3 = Boolean.TRUE;
                ((ContentValues)localObject9).put("parsed", (Boolean)localObject3);
                localObject2 = d;
                ((com.daamitt.prime.sdk.b.b)localObject2).a(l6, (ContentValues)localObject9);
              }
              else
              {
                bool20 = localObject2 instanceof l;
                i5 = 2;
                double d6;
                long l9;
                boolean bool6;
                int i28;
                if (bool20)
                {
                  localObject3 = localObject2;
                  localObject3 = (l)localObject2;
                  localObject11 = d;
                  localObject6 = new java/lang/StringBuilder;
                  ((StringBuilder)localObject6).<init>();
                  localObject10 = s;
                  ((StringBuilder)localObject6).append((String)localObject10);
                  ((StringBuilder)localObject6).append(" ");
                  localObject10 = l.a(i);
                  ((StringBuilder)localObject6).append((String)localObject10);
                  localObject10 = ((StringBuilder)localObject6).toString();
                  localObject12 = b;
                  int i21 = v;
                  bool11 = L;
                  localObject6 = B;
                  localObject15 = localObject6;
                  localObject6 = ((com.daamitt.prime.sdk.b.b)localObject11).a((String)localObject10, (String)localObject12, i21, bool11, (String)localObject6);
                  l3 = o;
                  C = l3;
                  int m = a;
                  w = m;
                  boolean bool4 = k;
                  z = bool4;
                  int n = n;
                  x = n;
                  localObject2 = ((com.daamitt.prime.sdk.a.a)localObject6).a();
                  y = ((String)localObject2);
                  localObject2 = ((com.daamitt.prime.sdk.a.a)localObject6).c();
                  c = ((String)localObject2);
                  localObject2 = Calendar.getInstance();
                  localObject11 = r;
                  ((Calendar)localObject2).setTime((Date)localObject11);
                  f1 = 0.0F / 0.0F;
                  i13 = 5;
                  ((Calendar)localObject2).add(i13, -1);
                  localObject11 = g;
                  localObject12 = ((Calendar)localObject2).getTime();
                  int i18 = ((Date)localObject11).before((Date)localObject12);
                  boolean bool5;
                  if (i18 != 0)
                  {
                    n = 0;
                    localObject2 = null;
                  }
                  else
                  {
                    i18 = 1;
                    f1 = Float.MIN_VALUE;
                    ((Calendar)localObject2).add(i13, i18);
                    ((Calendar)localObject2).add(i5, i5);
                    localObject11 = g;
                    localObject2 = ((Calendar)localObject2).getTime();
                    bool5 = ((Date)localObject11).after((Date)localObject2);
                    if (bool5)
                    {
                      bool5 = false;
                      localObject2 = null;
                    }
                    else
                    {
                      bool5 = true;
                    }
                  }
                  if (bool5)
                  {
                    d5 = f;
                    l4 = -4616189618054758400L;
                    d1 = -1.0D;
                    bool5 = d5 < d1;
                    int i6;
                    if (bool5)
                    {
                      localObject2 = d.b;
                      localObject11 = a;
                      l2 = C;
                      localObject11 = ((com.daamitt.prime.sdk.b.b)localObject11).a(l2);
                      localObject14 = new com/daamitt/prime/sdk/a/m;
                      ((m)localObject14).<init>(null, null, null);
                      l2 = C;
                      C = l2;
                      ((m)localObject14).m();
                      i13 = a;
                      w = i13;
                      localObject12 = f;
                      d6 = f;
                      localObject13 = Double.valueOf(d6);
                      localObject4 = r;
                      localObject5 = d;
                      i23 = 16;
                      localObject10 = localObject14;
                      localObject11 = localObject14;
                      localObject14 = localObject4;
                      localObject15 = localObject5;
                      ((m)localObject10).a((String)localObject12, (Double)localObject13, (Date)localObject4, (String)localObject5, i23);
                      i5 = i;
                      int i27 = 10;
                      if (i5 == i27)
                      {
                        localObject4 = "walnut_bills";
                        T = ((String)localObject4);
                      }
                      boolean bool9 = k;
                      if (!bool9)
                      {
                        i6 = Y | 0x8;
                        Y = i6;
                      }
                      l9 = ((f)localObject2).a((m)localObject11);
                      o = l9;
                    }
                    bool5 = K;
                    if (bool5)
                    {
                      localObject2 = ((l)localObject3).a();
                      localObject4 = d.c;
                      localObject2 = ((com.daamitt.prime.sdk.b.e)localObject4).a((ArrayList)localObject2, (l)localObject3);
                      if (localObject2 != null)
                      {
                        localb.a((l)localObject3, (l)localObject2);
                      }
                      else
                      {
                        bool5 = ((l)localObject3).b();
                        int i1;
                        if (bool5)
                        {
                          i1 = j;
                          i6 = 2;
                          i1 |= i6;
                          j = i1;
                        }
                        else
                        {
                          i1 = j & 0xFFFFFFFD;
                          j = i1;
                        }
                      }
                    }
                    localObject2 = d.c;
                    bool6 = ((com.daamitt.prime.sdk.b.e)localObject2).a((l)localObject3, (com.daamitt.prime.sdk.a.a)localObject6);
                    if (!bool6)
                    {
                      localObject2 = d.c;
                      localObject4 = new android/content/ContentValues;
                      ((ContentValues)localObject4).<init>();
                      Object localObject16 = Long.valueOf(C);
                      ((ContentValues)localObject4).put("wSmsId", (Long)localObject16);
                      localObject16 = Double.valueOf(d);
                      ((ContentValues)localObject4).put("amount", (Double)localObject16);
                      double d7 = e;
                      localObject16 = Double.valueOf(d7);
                      ((ContentValues)localObject4).put("minDueAmount", (Double)localObject16);
                      long l10 = g.getTime();
                      localObject16 = Long.valueOf(l10);
                      ((ContentValues)localObject4).put("dueDate", (Long)localObject16);
                      localObject5 = h;
                      if (localObject5 != null)
                      {
                        localObject5 = "paymentDate";
                        l10 = h.getTime();
                        localObject16 = Long.valueOf(l10);
                        ((ContentValues)localObject4).put((String)localObject5, (Long)localObject16);
                      }
                      localObject16 = Integer.valueOf(i);
                      ((ContentValues)localObject4).put("type", (Integer)localObject16);
                      localObject16 = Integer.valueOf(w);
                      ((ContentValues)localObject4).put("accountId", (Integer)localObject16);
                      localObject16 = Integer.valueOf(j);
                      ((ContentValues)localObject4).put("status", (Integer)localObject16);
                      localObject5 = UUID.randomUUID().toString();
                      k = ((String)localObject5);
                      localObject16 = k;
                      ((ContentValues)localObject4).put("UUID", (String)localObject16);
                      int i33 = l;
                      localObject16 = Integer.valueOf(i33);
                      ((ContentValues)localObject4).put("billType", (Integer)localObject16);
                      localObject5 = m;
                      i28 = TextUtils.isEmpty((CharSequence)localObject5);
                      if (i28 == 0)
                      {
                        localObject5 = "txnUUID";
                        localObject16 = m;
                        ((ContentValues)localObject4).put((String)localObject5, (String)localObject16);
                      }
                      localObject2 = a;
                      localObject5 = "statements";
                      i33 = 0;
                      localObject16 = null;
                      l9 = ((SQLiteDatabase)localObject2).insert((String)localObject5, null, (ContentValues)localObject4);
                      o = l9;
                    }
                    else
                    {
                      l9 = -1;
                      d6 = 0.0D / 0.0D;
                      o = l9;
                    }
                    localObject3 = Boolean.TRUE;
                    ((ContentValues)localObject9).put("parsed", (Boolean)localObject3);
                    localObject2 = d;
                    ((com.daamitt.prime.sdk.b.b)localObject2).a(l6, (ContentValues)localObject9);
                  }
                }
                else
                {
                  bool20 = localObject2 instanceof com.daamitt.prime.sdk.a.d;
                  int i7;
                  if (bool20)
                  {
                    localObject3 = localObject2;
                    localObject3 = (com.daamitt.prime.sdk.a.d)localObject2;
                    l9 = o;
                    C = l9;
                    bool6 = K;
                    if (bool6)
                    {
                      localObject2 = ((com.daamitt.prime.sdk.a.d)localObject3).a();
                      if (localObject2 != null)
                      {
                        boolean bool10 = ((ArrayList)localObject2).isEmpty();
                        if (!bool10)
                        {
                          localObject4 = d.e;
                          localObject2 = ((com.daamitt.prime.sdk.b.c)localObject4).a((ArrayList)localObject2, (com.daamitt.prime.sdk.a.d)localObject3);
                          if (localObject2 != null)
                          {
                            i7 = h;
                            i28 = 1;
                            i7 &= i28;
                            if (i7 == 0) {
                              l = i28;
                            }
                            localb.a((com.daamitt.prime.sdk.a.d)localObject3, (com.daamitt.prime.sdk.a.d)localObject2);
                          }
                          else
                          {
                            i29 = 1;
                            bool6 = ((com.daamitt.prime.sdk.a.d)localObject3).b();
                            if (bool6)
                            {
                              i2 = h | i29;
                              h = i2;
                            }
                            else
                            {
                              i2 = h & 0xFFFFFFFE;
                              h = i2;
                            }
                          }
                        }
                      }
                    }
                    localObject11 = d.e.b;
                    localObject10 = "events";
                    localObject12 = new String[] { "_id" };
                    localObject2 = new java/lang/StringBuilder;
                    ((StringBuilder)localObject2).<init>("name=? AND dueDate=");
                    l9 = d.getTime();
                    ((StringBuilder)localObject2).append(l9);
                    ((StringBuilder)localObject2).append(" AND pnr=? AND flags & 4 =0");
                    localObject13 = ((StringBuilder)localObject2).toString();
                    int i2 = 2;
                    localObject14 = new String[i2];
                    localObject2 = b;
                    i7 = 0;
                    localObject4 = null;
                    localObject14[0] = localObject2;
                    localObject2 = c;
                    int i29 = 1;
                    localObject14[i29] = localObject2;
                    localObject15 = null;
                    i23 = 0;
                    i22 = 0;
                    localObject2 = ((SQLiteDatabase)localObject11).query((String)localObject10, (String[])localObject12, (String)localObject13, (String[])localObject14, null, null, null);
                    if (localObject2 != null)
                    {
                      i8 = ((Cursor)localObject2).getCount();
                      if (i8 > 0)
                      {
                        localObject4 = com.daamitt.prime.sdk.b.c.a;
                        com.daamitt.prime.sdk.a.e.b();
                        ((Cursor)localObject2).close();
                        i7 = 1;
                        break label4147;
                      }
                    }
                    if (localObject2 != null) {
                      ((Cursor)localObject2).close();
                    }
                    label4147:
                    if (i7 == 0)
                    {
                      localObject2 = d.e;
                      localObject4 = new android/content/ContentValues;
                      ((ContentValues)localObject4).<init>();
                      localObject6 = Long.valueOf(C);
                      ((ContentValues)localObject4).put("wSmsId", (Long)localObject6);
                      localObject6 = b;
                      ((ContentValues)localObject4).put("name", (String)localObject6);
                      localObject6 = c;
                      ((ContentValues)localObject4).put("pnr", (String)localObject6);
                      l3 = d.getTime();
                      localObject6 = Long.valueOf(l3);
                      ((ContentValues)localObject4).put("dueDate", (Long)localObject6);
                      i8 = e;
                      localObject6 = Integer.valueOf(i8);
                      ((ContentValues)localObject4).put("type", (Integer)localObject6);
                      localObject5 = f;
                      if (localObject5 != null)
                      {
                        localObject5 = "location";
                        localObject6 = f;
                        ((ContentValues)localObject4).put((String)localObject5, (String)localObject6);
                      }
                      localObject5 = g;
                      if (localObject5 != null)
                      {
                        localObject5 = "info";
                        localObject6 = g;
                        ((ContentValues)localObject4).put((String)localObject5, (String)localObject6);
                      }
                      localObject6 = UUID.randomUUID().toString();
                      ((ContentValues)localObject4).put("UUID", (String)localObject6);
                      i8 = h;
                      localObject6 = Integer.valueOf(i8);
                      ((ContentValues)localObject4).put("flags", (Integer)localObject6);
                      d5 = i;
                      localObject6 = Double.valueOf(d5);
                      ((ContentValues)localObject4).put("amount", (Double)localObject6);
                      l3 = m;
                      localObject6 = Long.valueOf(l3);
                      ((ContentValues)localObject4).put("reminderTimeSpan", (Long)localObject6);
                      localObject5 = j;
                      if (localObject5 != null)
                      {
                        localObject5 = "contact";
                        localObject6 = j;
                        ((ContentValues)localObject4).put((String)localObject5, (String)localObject6);
                      }
                      localObject2 = b;
                      localObject5 = "events";
                      i8 = 0;
                      localObject6 = null;
                      l9 = ((SQLiteDatabase)localObject2).insert((String)localObject5, null, (ContentValues)localObject4);
                      o = l9;
                      d6 = i;
                      l3 = 0L;
                      d5 = 0.0D;
                      boolean bool7 = d6 < d5;
                      if (bool7)
                      {
                        bool7 = k;
                        if (bool7)
                        {
                          localObject2 = d.b;
                          localObject4 = a;
                          l4 = C;
                          localObject4 = ((com.daamitt.prime.sdk.b.b)localObject4).a(l4);
                          i29 = w;
                          localObject6 = new com/daamitt/prime/sdk/a/m;
                          bool15 = false;
                          ((m)localObject6).<init>(null, null, null);
                          l4 = C;
                          C = l4;
                          ((m)localObject6).m();
                          w = i29;
                          localObject13 = b;
                          d5 = i;
                          localObject14 = Double.valueOf(d5);
                          localObject4 = r;
                          localObject5 = b;
                          i22 = 13;
                          localObject12 = localObject6;
                          localObject15 = localObject4;
                          ((m)localObject6).a((String)localObject13, (Double)localObject14, (Date)localObject4, (String)localObject5, i22);
                          i7 = 3;
                          i29 = e;
                          if (i7 != i29)
                          {
                            i7 = e;
                            int i30 = 2;
                            if (i30 != i7)
                            {
                              i7 = e;
                              int i31 = 5;
                              if (i31 != i7) {
                                break label4744;
                              }
                            }
                          }
                          localObject4 = "walnut_travel";
                          T = ((String)localObject4);
                          label4744:
                          d6 = i;
                          l3 = 0L;
                          d5 = 0.0D;
                          boolean bool21 = d6 < d5;
                          if (bool21)
                          {
                            localObject4 = new java/lang/StringBuilder;
                            ((StringBuilder)localObject4).<init>();
                            localObject3 = b;
                            ((StringBuilder)localObject4).append((String)localObject3);
                            ((StringBuilder)localObject4).append(" refund");
                            localObject3 = ((StringBuilder)localObject4).toString();
                            V = ((String)localObject3);
                          }
                          l8 = ((f)localObject2).a((m)localObject6);
                          o = l8;
                        }
                      }
                    }
                    localObject3 = Boolean.TRUE;
                    ((ContentValues)localObject9).put("parsed", (Boolean)localObject3);
                    localObject2 = d;
                    ((com.daamitt.prime.sdk.b.b)localObject2).a(l6, (ContentValues)localObject9);
                  }
                  else
                  {
                    i7 = 0;
                    localObject4 = null;
                    int i32 = 1;
                    localObject3 = i.a(((com.daamitt.prime.sdk.a.a)localObject6).a());
                    int i34 = l;
                    i9 = 128;
                    i34 &= i9;
                    if (i34 == i9) {
                      i7 = 1;
                    }
                    if (i7 == 0)
                    {
                      localObject4 = "(?i).*[0-9]{10}\\s*";
                      bool20 = ((String)localObject3).matches((String)localObject4);
                      if (!bool20)
                      {
                        localObject3 = ((com.daamitt.prime.sdk.a.a)localObject6).a();
                        localObject2 = ((k)localObject2).c((String)localObject3);
                        if (localObject2 != null)
                        {
                          com.daamitt.prime.sdk.a.e.a();
                          e = ((String)localObject2);
                          int i3 = l | i9;
                          l = i3;
                          localObject2 = d.a;
                          localObject3 = new android/content/ContentValues;
                          ((ContentValues)localObject3).<init>();
                          localObject5 = ((com.daamitt.prime.sdk.a.a)localObject6).a();
                          ((ContentValues)localObject3).put("displayName", (String)localObject5);
                          i32 = l;
                          localObject5 = Integer.valueOf(i32);
                          ((ContentValues)localObject3).put("flags", (Integer)localObject5);
                          localObject4 = "updatedTime";
                          l1 = System.currentTimeMillis();
                          localObject5 = Long.valueOf(l1);
                          ((ContentValues)localObject3).put((String)localObject4, (Long)localObject5);
                          localObject2 = b;
                          ((com.daamitt.prime.sdk.b.b)localObject2).a((com.daamitt.prime.sdk.a.a)localObject6, (ContentValues)localObject3);
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */