package com.daamitt.prime.sdk;

public abstract interface PreScoreCallback
{
  public abstract void onError(String paramString);
  
  public abstract void onPreScoreData(String paramString);
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.PreScoreCallback
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */