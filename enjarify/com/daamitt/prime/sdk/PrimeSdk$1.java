package com.daamitt.prime.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.daamitt.prime.sdk.a.e;
import com.daamitt.prime.sdk.a.j;
import io.reactivex.a.b;
import io.reactivex.k;
import io.reactivex.n;

final class PrimeSdk$1
  extends BroadcastReceiver
{
  PrimeSdk$1(PrimeSdk paramPrimeSdk) {}
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    Object localObject1 = paramIntent.getAction();
    Object localObject2 = "prime.sdk.PRIME_PROGRESS";
    boolean bool1 = TextUtils.equals((CharSequence)localObject1, (CharSequence)localObject2);
    if (bool1)
    {
      int i = paramIntent.getIntExtra("prime.sdk.EXTRA_PROGRESS", 0);
      int j = paramIntent.getIntExtra("prime.sdk.EXTRA_MAX", 0);
      PrimeSdk.access$000(a).onScoreDataProgress(i, j);
      return;
    }
    localObject1 = paramIntent.getAction();
    localObject2 = "prime.sdk.PRIME_FINISH";
    bool1 = TextUtils.equals((CharSequence)localObject1, (CharSequence)localObject2);
    if (bool1)
    {
      paramIntent = PrimeSdk.access$100(a);
      if (paramIntent == null)
      {
        PrimeSdk.access$200();
        e.c();
        android.support.v4.content.d.a(paramContext).a(this);
        PrimeSdk.access$300(a, paramContext);
        return;
      }
      j localj = new com/daamitt/prime/sdk/a/j;
      localj.<init>();
      long l = System.currentTimeMillis();
      paramIntent = PrimeSdk.access$100(a);
      localObject1 = localj.a(paramContext);
      localObject2 = io.reactivex.android.b.a.a();
      k localk = ((k)localObject1).a((n)localObject2);
      -..Lambda.PrimeSdk.1.g05bcbMyFXjnhQvrfzzurqMGYaY localg05bcbMyFXjnhQvrfzzurqMGYaY = new com/daamitt/prime/sdk/-$$Lambda$PrimeSdk$1$g05bcbMyFXjnhQvrfzzurqMGYaY;
      localObject1 = localg05bcbMyFXjnhQvrfzzurqMGYaY;
      localObject2 = this;
      localg05bcbMyFXjnhQvrfzzurqMGYaY.<init>(this, l, localj, paramContext);
      localObject1 = new com/daamitt/prime/sdk/-$$Lambda$PrimeSdk$1$0nKziViHl7uhR-vAcI4vMAYz0-E;
      ((-..Lambda.PrimeSdk.1.0nKziViHl7uhR-vAcI4vMAYz0-E)localObject1).<init>(this, paramContext);
      localObject1 = localk.a(localg05bcbMyFXjnhQvrfzzurqMGYaY, (io.reactivex.c.d)localObject1);
      paramIntent.a((b)localObject1);
      android.support.v4.content.d.a(paramContext).a(this);
      return;
    }
    paramIntent = paramIntent.getAction();
    localObject1 = "prime.sdk.PRIME_REQUEST_FOR_READ_SMS_PERM";
    boolean bool2 = TextUtils.equals(paramIntent, (CharSequence)localObject1);
    if (bool2)
    {
      paramIntent = PrimeSdk.access$000(a);
      localObject1 = "Cannot perform scoring without read SMS permission";
      paramIntent.onError((String)localObject1);
      paramContext = android.support.v4.content.d.a(paramContext);
      paramContext.a(this);
    }
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.PrimeSdk.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */