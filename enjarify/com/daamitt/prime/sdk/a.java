package com.daamitt.prime.sdk;

import android.text.TextUtils;
import com.daamitt.prime.sdk.a.c;
import com.daamitt.prime.sdk.a.c.a;
import com.daamitt.prime.sdk.a.c.b;
import com.daamitt.prime.sdk.a.d;
import com.daamitt.prime.sdk.a.e;
import com.daamitt.prime.sdk.a.h;
import com.daamitt.prime.sdk.a.k;
import com.daamitt.prime.sdk.a.l;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a
{
  private static final String a = "a";
  private static final Pattern b = Pattern.compile("(?i)(?:(?:.*)?(?:(?:[Rr][Ss][.]?)|(?:[Ii][Nn][Rr][.]?)))[ ]?([1-9][0-9,.]*).*");
  private static final Pattern c = Pattern.compile("(?i).*salary.*");
  private static final Pattern d = Pattern.compile("(?i).* sal .*");
  private static final Pattern e = Pattern.compile("(?i).*credit.*");
  private static final Pattern f = Pattern.compile("(?i).*deposit.*");
  private static final Pattern g = Pattern.compile("(?i).*reimb.*");
  private static final Pattern h = Pattern.compile("(?i).*debit.*");
  private static final ArrayList i;
  private static final ArrayList j;
  private static final ArrayList k;
  private static final ArrayList l;
  private static final ArrayList m;
  private static final ArrayList n;
  private static final ArrayList o;
  private static final ArrayList p;
  private static final ArrayList q;
  private static final ArrayList r;
  private static final ArrayList s;
  private static final ArrayList t;
  private static final ArrayList u;
  private static final ArrayList v;
  
  static
  {
    ArrayList localArrayList = new java/util/ArrayList;
    Object localObject = new Pattern[34];
    Pattern localPattern = Pattern.compile("(?i).*reliance[ ]?fresh.*");
    localObject[0] = localPattern;
    localPattern = Pattern.compile("(?i).*big[ ]?bas.*");
    int i1 = 1;
    localObject[i1] = localPattern;
    localPattern = Pattern.compile("(?i).*he[i]?r[i]?tage[ ]?.*");
    int i2 = 2;
    localObject[i2] = localPattern;
    localPattern = Pattern.compile("(?i)food[ ]?bazaar");
    int i3 = 3;
    localObject[i3] = localPattern;
    localPattern = Pattern.compile("(?i)(.*metro[ ]?cash.*)|(^cash n carry$$)");
    int i4 = 4;
    localObject[i4] = localPattern;
    localPattern = Pattern.compile("(?i)(.*nilgiri.*)|(nilagiri[s]?)");
    int i5 = 5;
    localObject[i5] = localPattern;
    localPattern = Pattern.compile("(?i).*sar[a]?vana.*");
    int i6 = 6;
    localObject[i6] = localPattern;
    localPattern = Pattern.compile("(?i).*grofers.*");
    int i7 = 7;
    localObject[i7] = localPattern;
    localPattern = Pattern.compile("(?i)^srs[ ]?val.*");
    int i8 = 8;
    localObject[i8] = localPattern;
    localPattern = Pattern.compile("(?i)(.*d[ ]?mart.*)|(d[-]?mart)|(^avenue.*)");
    int i9 = 9;
    localObject[i9] = localPattern;
    localPattern = Pattern.compile("(?i)(.*more[ ]?mega.*)|(^more)|(^more[.]?super.*)");
    int i10 = 10;
    localObject[i10] = localPattern;
    localPattern = Pattern.compile("(?i).*vishal[ ]?m.*");
    int i11 = 11;
    localObject[i11] = localPattern;
    localPattern = Pattern.compile("(?i).*easy[ ]?day.*");
    int i12 = 12;
    localObject[i12] = localPattern;
    localPattern = Pattern.compile("(?i)(^spar[ ]hyper.*)|(^spar[ ]?sln.*)");
    int i13 = 13;
    localObject[i13] = localPattern;
    localPattern = Pattern.compile("(?i).*star[ ]?baz.*");
    int i14 = 14;
    localObject[i14] = localPattern;
    localPattern = Pattern.compile("(?i).*vijetha.*");
    localObject[15] = localPattern;
    localPattern = Pattern.compile("(?i).*m[.][ ]?k[.]?[ ]?ret.*");
    localObject[16] = localPattern;
    localPattern = Pattern.compile("(?i)(.*big[ ]?baz[z]?a[a]?r.*)|(^fbb$$)(big[_]?bazar)");
    localObject[17] = localPattern;
    localPattern = Pattern.compile("(?i).*hyper[ ]?city.*");
    localObject[18] = localPattern;
    localPattern = Pattern.compile("(?i).*village[ ]?hyper.*");
    localObject[19] = localPattern;
    localPattern = Pattern.compile("(?i).*haiko.*");
    localObject[20] = localPattern;
    localPattern = Pattern.compile("(?i).*city[ ]super.*");
    localObject[21] = localPattern;
    localPattern = Pattern.compile("(?i).*twenty[ ]?four.*");
    localObject[22] = localPattern;
    localPattern = Pattern.compile("(?i)^grace[ ]sup.*");
    localObject[23] = localPattern;
    localPattern = Pattern.compile("(?i)^sansar[ ]sup.*");
    localObject[24] = localPattern;
    localPattern = Pattern.compile("(?i).*sahakari[ ]?bhan.*");
    localObject[25] = localPattern;
    localPattern = Pattern.compile("(?i).*food[ ]?wor.*");
    localObject[26] = localPattern;
    localPattern = Pattern.compile("(?i)^show[ ]?off.*");
    localObject[27] = localPattern;
    localPattern = Pattern.compile("(?i)^grace[ ]sup.*");
    localObject[28] = localPattern;
    localPattern = Pattern.compile("(?i)^zopnow$$");
    localObject[29] = localPattern;
    localPattern = Pattern.compile("(?i)swaraj.*mark");
    localObject[30] = localPattern;
    localPattern = Pattern.compile("(?i)^green city.*super");
    localObject[31] = localPattern;
    localPattern = Pattern.compile("(?i)m[ .]?k[ .]?ahmed");
    localObject[32] = localPattern;
    localPattern = Pattern.compile("(?i)royal.*mart");
    localObject[33] = localPattern;
    localObject = Arrays.asList((Object[])localObject);
    localArrayList.<init>((Collection)localObject);
    i = localArrayList;
    localArrayList = new java/util/ArrayList;
    localObject = new Pattern[19];
    localPattern = Pattern.compile("(?i)(^uber$$)|((^uber [^cafe].*)&(^uber [^lounge].*))|(.*^uber\\d.*)");
    localObject[0] = localPattern;
    localPattern = Pattern.compile("(?i)(^(?:www.)?ola[ ]?(?:cabs)?.*)|(.*ani[ ]?tech.*)");
    localObject[i1] = localPattern;
    localPattern = Pattern.compile("(?i)(.*indian rail.*)|(.*irct[c]?.*)");
    localObject[i2] = localPattern;
    localPattern = Pattern.compile("(?i)(.*red[ ]?bus.*)");
    localObject[i3] = localPattern;
    localPattern = Pattern.compile("(?i)(.*make[ ]?my.*)");
    localObject[i4] = localPattern;
    localPattern = Pattern.compile("(?i)(.*goibi.*)|(.*ibibo.*)");
    localObject[i5] = localPattern;
    localPattern = Pattern.compile("(?i)(.*ksrtc.*)|(karnataka state road)");
    localObject[i6] = localPattern;
    localPattern = Pattern.compile("(?i)(.*abhi[ ]?bus.*)");
    localObject[i7] = localPattern;
    localPattern = Pattern.compile("(?i)(.*apsrtc.*)");
    localObject[i8] = localPattern;
    localPattern = Pattern.compile("(?i)(.*indigo.*)");
    localObject[i9] = localPattern;
    localPattern = Pattern.compile("(?i)(.*airbnb.*)");
    localObject[i10] = localPattern;
    localPattern = Pattern.compile("(?i)(.*jet[ ]?air.*)");
    localObject[i11] = localPattern;
    localPattern = Pattern.compile("(?i)(.*msrtc.*)");
    localObject[i12] = localPattern;
    localPattern = Pattern.compile("(?i)(tnstcltd)");
    localObject[i13] = localPattern;
    localPattern = Pattern.compile("(?i)(.*travelyaari.*)");
    localObject[i14] = localPattern;
    localPattern = Pattern.compile("(?i)(.*irc[ ]?logistics.*)|(^irc$$)");
    localObject[15] = localPattern;
    localPattern = Pattern.compile("(?i)(.*dhanunjaya.*)");
    localObject[16] = localPattern;
    localPattern = Pattern.compile("(?i).*spicejet.*");
    localObject[17] = localPattern;
    localPattern = Pattern.compile("(?i).*yatra.*");
    localObject[18] = localPattern;
    localObject = Arrays.asList((Object[])localObject);
    localArrayList.<init>((Collection)localObject);
    j = localArrayList;
    localArrayList = new java/util/ArrayList;
    localObject = new Pattern[64];
    localPattern = Pattern.compile("(?i)(amazon attire)");
    localObject[0] = localPattern;
    localPattern = Pattern.compile("(?i)(.*amazon.*)|(^amaz$$)");
    localObject[i1] = localPattern;
    localPattern = Pattern.compile("(?i)(men[']?s aven.*)");
    localObject[i2] = localPattern;
    localPattern = Pattern.compile("(?i)(.*pantaloons.*)");
    localObject[i3] = localPattern;
    localPattern = Pattern.compile("(?i)(.*mark[s]?[ ]?spen.*)|(.*spencer[s]?)|(.*mark[s]?[ ]?and[ ]?.*)");
    localObject[i4] = localPattern;
    localPattern = Pattern.compile("(?i)(.*life[ ]?style.*)");
    localObject[i5] = localPattern;
    localPattern = Pattern.compile("(?i)(.*bata.*)");
    localObject[i6] = localPattern;
    localPattern = Pattern.compile("(?i)(.*shopper.*)");
    localObject[i7] = localPattern;
    localPattern = Pattern.compile("(?i)(.*westside.*)");
    localObject[i8] = localPattern;
    localPattern = Pattern.compile("(?i)(.*croma.*)");
    localObject[i9] = localPattern;
    localPattern = Pattern.compile("(?i)(.*brand[ ]fac.*)");
    localObject[i10] = localPattern;
    localPattern = Pattern.compile("(?i)(.*central.*)");
    localObject[i11] = localPattern;
    localPattern = Pattern.compile("(?i)(.*amway.*)");
    localObject[i12] = localPattern;
    localPattern = Pattern.compile("(?i)(.*seasons.*)");
    localObject[i13] = localPattern;
    localPattern = Pattern.compile("(?i)(.*health[ ]?and[ ]?gl.*)");
    localObject[i14] = localPattern;
    localPattern = Pattern.compile("(?i)(.*snap[ ]?deal.*)");
    localObject[15] = localPattern;
    localPattern = Pattern.compile("(?i)(.*myntra.*)");
    localObject[16] = localPattern;
    localPattern = Pattern.compile("(?i)(.*ebay.*)");
    localObject[17] = localPattern;
    localPattern = Pattern.compile("(?i)(.*apple[ ]?itune.*)");
    localObject[18] = localPattern;
    localPattern = Pattern.compile("(?i)(.*jabong.*)");
    localObject[19] = localPattern;
    localPattern = Pattern.compile("(?i)(.*shop[ ]?cl.*)");
    localObject[20] = localPattern;
    localPattern = Pattern.compile("(?i)(.*ali[ ]?exp.*)");
    localObject[21] = localPattern;
    localPattern = Pattern.compile("(?i)(^easy[ ]?mobile.*)");
    localObject[22] = localPattern;
    localPattern = Pattern.compile("(?i)(^fab[ ]?india.*)");
    localObject[23] = localPattern;
    localPattern = Pattern.compile("(?i)(^aiswarya$$)");
    localObject[24] = localPattern;
    localPattern = Pattern.compile("(?i)(^max$$)|(.*max[ ]?hyper.*)");
    localObject[25] = localPattern;
    localPattern = Pattern.compile("(?i)(.*max[ ]?shoppee.*)");
    localObject[26] = localPattern;
    localPattern = Pattern.compile("(?i)(.*titan.*)");
    localObject[27] = localPattern;
    localPattern = Pattern.compile("(?i)(^zara$$)");
    localObject[28] = localPattern;
    localPattern = Pattern.compile("(?i)(.*windmills[ ]?craftworks.*)");
    localObject[29] = localPattern;
    localPattern = Pattern.compile("(?i)(.*bionic[ ]nat.*)");
    localObject[30] = localPattern;
    localPattern = Pattern.compile("(?i)(.*raymond[s]?.*)");
    localObject[31] = localPattern;
    localPattern = Pattern.compile("(?i)(.*herbalife.*)");
    localObject[32] = localPattern;
    localPattern = Pattern.compile("(?i)(^jockey.*)");
    localObject[33] = localPattern;
    localPattern = Pattern.compile("(?i)(^blackberry[s]?.*)");
    localObject[34] = localPattern;
    localPattern = Pattern.compile("(?i)(^mochi$$)");
    localObject[35] = localPattern;
    localPattern = Pattern.compile("(?i)(^soch$$)");
    localObject[36] = localPattern;
    localPattern = Pattern.compile("(?i)(.*first[ ]?cry.*)");
    localObject[37] = localPattern;
    localPattern = Pattern.compile("(?i)(^khadims$$)");
    localObject[38] = localPattern;
    localPattern = Pattern.compile("(?i)(^poorvika[ ]?mob.*)");
    localObject[39] = localPattern;
    localPattern = Pattern.compile("(?i)(^levi[']?s.*)");
    localObject[40] = localPattern;
    localPattern = Pattern.compile("(?i)(^forever[ ]?21.*)");
    localObject[41] = localPattern;
    localPattern = Pattern.compile("(?i)(.*jaihind.*)");
    localObject[42] = localPattern;
    localPattern = Pattern.compile("(?i)(.*masoom[ ]?play.*)");
    localObject[43] = localPattern;
    localPattern = Pattern.compile("(?i)(.*decathlon[ ]?[s]?.*)");
    localObject[44] = localPattern;
    localPattern = Pattern.compile("(?i)(.*madura[ ]?.*)");
    localObject[45] = localPattern;
    localPattern = Pattern.compile("(?i)(^show[ ]?off.*)");
    localObject[46] = localPattern;
    localPattern = Pattern.compile("(?i)(.*future[ ]?life.*)");
    localObject[47] = localPattern;
    localPattern = Pattern.compile("(?i)(^lulu[ ]?.*)");
    localObject[48] = localPattern;
    localPattern = Pattern.compile("(?i)(^woodland$$)");
    localObject[49] = localPattern;
    localPattern = Pattern.compile("(?i)(.*vijay[ ]?sal.*)");
    localObject[50] = localPattern;
    localPattern = Pattern.compile("(?i)(.*body[ ]?shop.*)");
    localObject[51] = localPattern;
    localPattern = Pattern.compile("(?i)(.*grt.*)");
    localObject[52] = localPattern;
    localPattern = Pattern.compile("(?i)(.*india[ ]?idea.*)");
    localObject[53] = localPattern;
    localPattern = Pattern.compile("(?i)(.*sherali.*)");
    localObject[54] = localPattern;
    localPattern = Pattern.compile("(?i)(.*globus.*)");
    localObject[55] = localPattern;
    localPattern = Pattern.compile("(?i)(^lakme[ ]?.*)");
    localObject[56] = localPattern;
    localPattern = Pattern.compile("(?i)(^smartbuy.*)");
    localObject[57] = localPattern;
    localPattern = Pattern.compile("(?i)(.*chennai shopping mall.*)");
    localObject[58] = localPattern;
    localPattern = Pattern.compile("(?i)(.*flipk.*)|(^ws ret.*)");
    localObject[59] = localPattern;
    localPattern = Pattern.compile("(?i)(.*reliance[ ]?trends.*)|(^reliance[ ]ma.*)|(.*reliance[_]?foo.*)|(.*reliance[ ]foot.*)");
    localObject[60] = localPattern;
    localPattern = Pattern.compile("(?i)(.*apple[ ]?itune.*)");
    localObject[61] = localPattern;
    localPattern = Pattern.compile("(?i)(^aiswarya$$)");
    localObject[62] = localPattern;
    localPattern = Pattern.compile("(?i)(.*pothys.*)");
    localObject[63] = localPattern;
    localObject = Arrays.asList((Object[])localObject);
    localArrayList.<init>((Collection)localObject);
    k = localArrayList;
    localArrayList = new java/util/ArrayList;
    localObject = new Pattern[114];
    localPattern = Pattern.compile("(?i)(.*juice[ ]?square.*)");
    localObject[0] = localPattern;
    localPattern = Pattern.compile("(?i)(^via$$)|(via[ ]?south)|(via[ ]?milano)");
    localObject[i1] = localPattern;
    localPattern = Pattern.compile("(?i)(.*facebake.*)");
    localObject[i2] = localPattern;
    localPattern = Pattern.compile("(?i)(^cbtl.*)");
    localObject[i3] = localPattern;
    localPattern = Pattern.compile("(?i)(^tea$$)");
    localObject[i4] = localPattern;
    localPattern = Pattern.compile("(?i)(^hazel[ ]?nut.*)");
    localObject[i5] = localPattern;
    localPattern = Pattern.compile("(?i)(^baskin[ ]?rob.*)");
    localObject[i6] = localPattern;
    localPattern = Pattern.compile("(?i)(^foodexo.*)");
    localObject[i7] = localPattern;
    localPattern = Pattern.compile("(?i)(.*sodexo.*)");
    localObject[i8] = localPattern;
    localPattern = Pattern.compile("(?i)(.*freshmenu.*)");
    localObject[i9] = localPattern;
    localPattern = Pattern.compile("(?i)(^chilis$$)");
    localObject[i10] = localPattern;
    localPattern = Pattern.compile("(?i)(.*house[ ]?of[ ]?spi.*)");
    localObject[i11] = localPattern;
    localPattern = Pattern.compile("(?i)(.*mdp[ ]?cof.*)");
    localObject[i12] = localPattern;
    localPattern = Pattern.compile("(?i)(.*cream[ ]?stone.*)");
    localObject[i13] = localPattern;
    localPattern = Pattern.compile("(?i)(.*m[a]?c[ ]*donald.*)|(.*hard[ ]?castle.*)");
    localObject[i14] = localPattern;
    localPattern = Pattern.compile("(?i)mcdeliv");
    localObject[15] = localPattern;
    localPattern = Pattern.compile("(?i)(.*cafe[ ]*coffee.*)|([ ]*ccd[^a-z].*)");
    localObject[16] = localPattern;
    localPattern = Pattern.compile("(?i)( PHD )|(.*pizza[ ]?hut[ ]?delivery.*)");
    localObject[17] = localPattern;
    localPattern = Pattern.compile("(?i)(.*pizza[ ]?hut.*)");
    localObject[18] = localPattern;
    localPattern = Pattern.compile("(?i)smokin.*joe");
    localObject[19] = localPattern;
    localPattern = Pattern.compile("(?i)cali.*pizza");
    localObject[20] = localPattern;
    localPattern = Pattern.compile("(?i)cali.*burrito");
    localObject[21] = localPattern;
    localPattern = Pattern.compile("(?i)ibaco");
    localObject[22] = localPattern;
    localPattern = Pattern.compile("(?i)just.*bake");
    localObject[23] = localPattern;
    localPattern = Pattern.compile("(?i)arbor[ ]?brew");
    localObject[24] = localPattern;
    localPattern = Pattern.compile("(?i)toit");
    localObject[25] = localPattern;
    localPattern = Pattern.compile("(?i)madhuloka");
    localObject[26] = localPattern;
    localPattern = Pattern.compile("(?i)^lake.*forest");
    localObject[27] = localPattern;
    localPattern = Pattern.compile("(?i)(.*burger[ ]?king)");
    localObject[28] = localPattern;
    localPattern = Pattern.compile("(?i)^anand sweet");
    localObject[29] = localPattern;
    localPattern = Pattern.compile("(?i)(.*domino.*)|(jubilant[ ]*fo)");
    localObject[30] = localPattern;
    localPattern = Pattern.compile("(?i)(haldiram)");
    localObject[31] = localPattern;
    localPattern = Pattern.compile("(?i)(bikaner[vw]a[a]?la)");
    localObject[32] = localPattern;
    localPattern = Pattern.compile("(?i)(rajdhani)");
    localObject[33] = localPattern;
    localPattern = Pattern.compile("(?i)k[ ]?c[ ]?s[ ]?(snack.*)?");
    localObject[34] = localPattern;
    localPattern = Pattern.compile("(?i)karachi[ ]?((bak.*)|(sweet.*)).*");
    localObject[35] = localPattern;
    localPattern = Pattern.compile("(?i)(star[ ]?buck)|(tata starbu)");
    localObject[36] = localPattern;
    localPattern = Pattern.compile("(?i)(sub[ ]?way)");
    localObject[37] = localPattern;
    localPattern = Pattern.compile("(?i)food[ ]?pan");
    localObject[38] = localPattern;
    localPattern = Pattern.compile("(?i)box8");
    localObject[39] = localPattern;
    localPattern = Pattern.compile("(?i)just[ ]?eat");
    localObject[40] = localPattern;
    localPattern = Pattern.compile("(?i)(.*zomato.*)");
    localObject[41] = localPattern;
    localPattern = Pattern.compile("(?i)(kfc)|(kentucky)");
    localObject[42] = localPattern;
    localPattern = Pattern.compile("(?i)([kc]rispy.*[kc]re[am][me])");
    localObject[43] = localPattern;
    localPattern = Pattern.compile("(?i)(barbeq.*nation)|(bbq.*nation)");
    localObject[44] = localPattern;
    localPattern = Pattern.compile("(?i)(main.*china)");
    localObject[45] = localPattern;
    localPattern = Pattern.compile("(?i)(faaso[']?s)|(fasso[']?s)");
    localObject[46] = localPattern;
    localPattern = Pattern.compile("(?i)(tiny[ ]?owl)");
    localObject[47] = localPattern;
    localPattern = Pattern.compile("(?i)(swiggy)|(bundl)");
    localObject[48] = localPattern;
    localPattern = Pattern.compile("(?i)(costa)");
    localObject[49] = localPattern;
    localPattern = Pattern.compile("(?i)(dunkin)");
    localObject[50] = localPattern;
    localPattern = Pattern.compile("(?i)(barista)|(lavazza)");
    localObject[51] = localPattern;
    localPattern = Pattern.compile("(?i)ice cap");
    localObject[52] = localPattern;
    localPattern = Pattern.compile("(?i)(sar[a]?van[a]?.*bha[vw]an)");
    localObject[53] = localPattern;
    localPattern = Pattern.compile("(?i)kant[h]?i.*sweet");
    localObject[54] = localPattern;
    localPattern = Pattern.compile("(?i)(tamanna)|(hotel taman)");
    localObject[55] = localPattern;
    localPattern = Pattern.compile("(?i)au bon pain");
    localObject[56] = localPattern;
    localPattern = Pattern.compile("(?i)^om sweet");
    localObject[57] = localPattern;
    localPattern = Pattern.compile("(?i)gud dhani");
    localObject[58] = localPattern;
    localPattern = Pattern.compile("(?i)hamza (ba.*)?");
    localObject[59] = localPattern;
    localPattern = Pattern.compile("(?i)hotel green city");
    localObject[60] = localPattern;
    localPattern = Pattern.compile("(?i)srm catering");
    localObject[61] = localPattern;
    localPattern = Pattern.compile("(?i)bar stock");
    localObject[62] = localPattern;
    localPattern = Pattern.compile("(?i)irish house");
    localObject[63] = localPattern;
    localPattern = Pattern.compile("(?i)t[ .]?g[ .]?i[ .]*f");
    localObject[64] = localPattern;
    localPattern = Pattern.compile("(?i)cha[a]?yos");
    localObject[65] = localPattern;
    localPattern = Pattern.compile("(?i)sai.*dosa");
    localObject[66] = localPattern;
    localPattern = Pattern.compile("(?i)theobroma");
    localObject[67] = localPattern;
    localPattern = Pattern.compile("(?i)chaturvedis");
    localObject[68] = localPattern;
    localPattern = Pattern.compile("(?i)kesar[']?[s]?sweet");
    localObject[69] = localPattern;
    localPattern = Pattern.compile("(?i)momoe vas");
    localObject[70] = localPattern;
    localPattern = Pattern.compile("(?i)anjappar.*chettinad");
    localObject[71] = localPattern;
    localPattern = Pattern.compile("(?i)benjarong");
    localObject[72] = localPattern;
    localPattern = Pattern.compile("(?i)meghana foo");
    localObject[73] = localPattern;
    localPattern = Pattern.compile("(?i)meghana.*spic");
    localObject[74] = localPattern;
    localPattern = Pattern.compile("(?i)bindras");
    localObject[75] = localPattern;
    localPattern = Pattern.compile("(?i)kovai.*pa.*am");
    localObject[76] = localPattern;
    localPattern = Pattern.compile("(?i)bindra reso");
    localObject[77] = localPattern;
    localPattern = Pattern.compile("(?i)(mad.*over.*do)|(m[.]o[.]d)");
    localObject[78] = localPattern;
    localPattern = Pattern.compile("(?i)vassi.*palaz");
    localObject[79] = localPattern;
    localPattern = Pattern.compile("(?i)tiswa");
    localObject[80] = localPattern;
    localPattern = Pattern.compile("(?i)chutneys");
    localObject[81] = localPattern;
    localPattern = Pattern.compile("(?i)cbtl");
    localObject[82] = localPattern;
    localPattern = Pattern.compile("(?i)^residency fresh");
    localObject[83] = localPattern;
    localPattern = Pattern.compile("(?i)swaraj bar");
    localObject[84] = localPattern;
    localPattern = Pattern.compile("(?i)drops[ ]?total");
    localObject[85] = localPattern;
    localPattern = Pattern.compile("(?i)^balaji wine");
    localObject[86] = localPattern;
    localPattern = Pattern.compile("(?i)sai balaji wine");
    localObject[87] = localPattern;
    localPattern = Pattern.compile("(?i)sr(i|e)[e]? balaji wine");
    localObject[88] = localPattern;
    localPattern = Pattern.compile("(?i)^(the )?beer.*cafe");
    localObject[89] = localPattern;
    localPattern = Pattern.compile("(?i)discovery[ ]?wine");
    localObject[90] = localPattern;
    localPattern = Pattern.compile("(?i)new paradise rest");
    localObject[91] = localPattern;
    localPattern = Pattern.compile("(?i)(^thalappakat$$)");
    localObject[92] = localPattern;
    localPattern = Pattern.compile("(?i)(dind.*thalappakat(ti)?$$)");
    localObject[93] = localPattern;
    localPattern = Pattern.compile("(?i)(^chil[l]?i[e]?[']?[s]?[ ]*$$)");
    localObject[94] = localPattern;
    localPattern = Pattern.compile("(?i)nkp.*empire");
    localObject[95] = localPattern;
    localPattern = Pattern.compile("(?i)beijing[ ]?bit");
    localObject[96] = localPattern;
    localPattern = Pattern.compile("(?i)plaza.*pre");
    localObject[97] = localPattern;
    localPattern = Pattern.compile("(?i)mast.*kal");
    localObject[98] = localPattern;
    localPattern = Pattern.compile("(?i)sri.*krishna.*sagar");
    localObject[99] = localPattern;
    localPattern = Pattern.compile("(?i)(^sangeetha( veg.*)$$)");
    localObject[100] = localPattern;
    localPattern = Pattern.compile("(?i)moriz");
    localObject[101] = localPattern;
    localPattern = Pattern.compile("(?i)nagarjuna");
    localObject[102] = localPattern;
    localPattern = Pattern.compile("(?i)sagar ratna");
    localObject[103] = localPattern;
    localPattern = Pattern.compile("(?i)^inchara.*((fam.*)|(gard.*)|(rest.*))");
    localObject[104] = localPattern;
    localPattern = Pattern.compile("(?i)currys.*crunch");
    localObject[105] = localPattern;
    localPattern = Pattern.compile("(?i)nut.*n.*spic");
    localObject[106] = localPattern;
    localPattern = Pattern.compile("(?i)citrus cafe");
    localObject[107] = localPattern;
    localPattern = Pattern.compile("(?i)corner.*hou");
    localObject[108] = localPattern;
    localPattern = Pattern.compile("(?i)ubiquitous");
    localObject[109] = localPattern;
    localPattern = Pattern.compile("(?i)papa.*jo[h]n");
    localObject[110] = localPattern;
    localPattern = Pattern.compile("(?i)nagarjuna.*chim");
    localObject[111] = localPattern;
    localPattern = Pattern.compile("(?i).*chefs corner.*");
    localObject[112] = localPattern;
    localPattern = Pattern.compile("(?i).*hungerbox.*");
    localObject[113] = localPattern;
    localObject = Arrays.asList((Object[])localObject);
    localArrayList.<init>((Collection)localObject);
    l = localArrayList;
    localArrayList = new java/util/ArrayList;
    localObject = new Pattern[i9];
    localPattern = Pattern.compile("(?i).*(EMI|emi) (of|for) Rs(.|)([\\d,.]*).*[Aa]\\/[Cc] (\\S*).*on ([0-9A-Za-z-:\\/]*).");
    localObject[0] = localPattern;
    localPattern = Pattern.compile("(?i)Your EMI Conversion request is processed");
    localObject[i1] = localPattern;
    localPattern = Pattern.compile("(?i).*(Loan|loan) [Aa]\\/[Cc] (\\S*).*EMI will Rs. ([\\d,.]*).*");
    localObject[i2] = localPattern;
    localPattern = Pattern.compile("(?i)You have opted for EMI facility.*");
    localObject[i3] = localPattern;
    localPattern = Pattern.compile("(?i).*NETSECURE code.*");
    localObject[i4] = localPattern;
    localPattern = Pattern.compile("(?i).*EMI conversion.*successfully.*");
    localObject[i5] = localPattern;
    localPattern = Pattern.compile("(?i).*(has bounced|did not clear|is due|is in overdue| in arrears|insufficient funds).*");
    localObject[i6] = localPattern;
    localPattern = Pattern.compile("(?i).*[Pp]ersonal loan of Rs\\.* ([\\d,.]*).*EMI Rs\\.* ([\\d,.]*).*is credited.*");
    localObject[i7] = localPattern;
    localPattern = Pattern.compile("(?i).*we have foreclosed.*loan of Rs\\.*\\ *([\\d,.]*).*");
    localObject[i8] = localPattern;
    localObject = Arrays.asList((Object[])localObject);
    localArrayList.<init>((Collection)localObject);
    m = localArrayList;
    localArrayList = new java/util/ArrayList;
    localObject = new Pattern[22];
    localPattern = Pattern.compile("(?i).*revised T&C .* (loan|LOAN|Loan) & (EMI|emi) (Cards|cards)");
    localObject[0] = localPattern;
    localPattern = Pattern.compile("(?i).*(EMI|emi) (Card|card) holder");
    localObject[i1] = localPattern;
    localPattern = Pattern.compile("(?i).*EMI of Rs.([\\d,.]*).*Loan#(\\S*) is due on ([0-9A-za-z-:\\/]*).");
    localObject[i2] = localPattern;
    localPattern = Pattern.compile("(?i).*Total loan amt on your Bajaj EMI Card is upgraded to.*");
    localObject[i3] = localPattern;
    localPattern = Pattern.compile("(?i).*[Yy]our BFL CD loan has been booked.*No. (\\S*) for Rs. ([\\d,.]*).*(EMI|emi) (of|for) Rs. ([\\d,.]*).*due on ([0-9A-za-z-:\\/]*).");
    localObject[i4] = localPattern;
    localPattern = Pattern.compile("(?i)You are eligible for an EMI card with limit of Rs. ([\\d,.]*)");
    localObject[i5] = localPattern;
    localPattern = Pattern.compile("(?i)Now swipe your EMI card.*");
    localObject[i6] = localPattern;
    localPattern = Pattern.compile("(?i).*your (EMI|emi) (Card|card) available loan amount is Rs.([\\d,.]*).*");
    localObject[i7] = localPattern;
    localPattern = Pattern.compile("(?i).*[Yy]our (EMI|emi) (card|Card) (number|no) (\\S*) has been delivered.*");
    localObject[i8] = localPattern;
    localPattern = Pattern.compile("(?i).*[Aa]vailable loan (Amt|amount).*(EMI|emi) (Card|card) ending (with)* (\\S*).*is Rs\\.* ([\\d,.]*)");
    localObject[i9] = localPattern;
    localPattern = Pattern.compile("(?i).*[Aa]vailable (Loan|loan) (Amt|Amount|amount).*(EMI|emi) (Card|card) ending (\\d*).*Rs. ([\\d,.]*).*");
    localObject[i10] = localPattern;
    localPattern = Pattern.compile("(?i).*BFL CD loan has been booked.*[Nn]o\\.*(\\S*) for Rs. ([\\d,.]*).* EMI for Rs. ([\\d,.]*).*due on ([0-9A-za-z-:\\/]*)");
    localObject[i11] = localPattern;
    localPattern = Pattern.compile("(?i).*pay your (EMI|emi) (ONLINE|online).*loan (account|acct) (no|no\\.|number) is (\\S*).*");
    localObject[i12] = localPattern;
    localPattern = Pattern.compile("(?i).*financed for.*ending (\\S*) is Rs.([\\d,.]*).*(EMI|emi) of Rs.([\\d,.]*).*");
    localObject[i13] = localPattern;
    localPattern = Pattern.compile("(?i).*loan (amt|amount) on (ur|your).*(EMI|emi) (Card|card) upgraded to Rs\\.* ([\\d,.]*).*");
    localObject[i14] = localPattern;
    localPattern = Pattern.compile("(?i).*4 digit PIN.* (EMI|emi) card number (\\S*).*");
    localObject[15] = localPattern;
    localPattern = Pattern.compile("(?i).*(EMI|emi) (Card|card) (number|No|no)\\.* (\\S*) is now unblocked.");
    localObject[16] = localPattern;
    localPattern = Pattern.compile("(?i).*BFL (EMI|emi) (Card|card).*(loan|Loan) (amount|amt) on (your|ur) (Card|card) is Rs([\\d,.]*).*");
    localObject[17] = localPattern;
    localPattern = Pattern.compile("(?i).*Transaction for Rs\\.*([\\d,.]*) on your (EMI|emi) (Card|card) (\\S*) is successful.");
    localObject[18] = localPattern;
    localPattern = Pattern.compile("(?i).*(EMI|emi) (Card|card) is upgraded to (Rs|INR|inr)\\.* ([\\d,.]*).*");
    localObject[19] = localPattern;
    localPattern = Pattern.compile("(?i).*entered wrong pin .*");
    localObject[20] = localPattern;
    localPattern = Pattern.compile("(?i).*( suspended | outstanding | bounced |did not clear|overdue| in arrears|insufficient funds|non receipt).*");
    localObject[21] = localPattern;
    localObject = Arrays.asList((Object[])localObject);
    localArrayList.<init>((Collection)localObject);
    n = localArrayList;
    localArrayList = new java/util/ArrayList;
    localObject = new Pattern[i10];
    localPattern = Pattern.compile("(?i)EMI due on([0-9A-za-z-:\\/]*) in a\\/c ([A-Za-z0-9]*)");
    localObject[0] = localPattern;
    localPattern = Pattern.compile("(?i)\\D* EMI on your account ([A-Za-z0-9]*) revised");
    localObject[i1] = localPattern;
    localPattern = Pattern.compile("(?i).* [Ff]lexipay [Ss]ervice [Rr]equest .* has been processed.*");
    localObject[i2] = localPattern;
    localPattern = Pattern.compile("(?i).*Flexipay.*will be processed within.*");
    localObject[i3] = localPattern;
    localPattern = Pattern.compile("(?i).*Balance Transfer\\D*([\\d,.]+).*EMI\\D*([\\d,.]+).*processed within.*");
    localObject[i4] = localPattern;
    localPattern = Pattern.compile("(?i).*Balance Transfer\\D*([\\d,.]+).*EMI\\D*([\\d,.]+).*processed successfully.*");
    localObject[i5] = localPattern;
    localPattern = Pattern.compile("(?i).*request for Encash.*processed within.*");
    localObject[i6] = localPattern;
    localPattern = Pattern.compile("(?i).*Encash.*processed successfully.*");
    localObject[i7] = localPattern;
    localPattern = Pattern.compile("(?i).*Flexipay.*been booked.*");
    localObject[i8] = localPattern;
    localPattern = Pattern.compile("(?i).*(not recd|over credit limit|unpaid|has bounced|did not clear|overdue| in arrears|insufficient funds|cancelled).*");
    localObject[i9] = localPattern;
    localObject = Arrays.asList((Object[])localObject);
    localArrayList.<init>((Collection)localObject);
    o = localArrayList;
    localArrayList = new java/util/ArrayList;
    localObject = new Pattern[i4];
    localPattern = Pattern.compile("(?i)Payment of Rs.([\\d,.]*).* (Loan|loan|LOAN) ([A-Za-z0-9]*) on (\\w{3} \\d{2} \\d{4}).");
    localObject[0] = localPattern;
    localPattern = Pattern.compile("(?i)Your (EMI|emi) (of|for).*(Loan|loan) ([A-Za-z0-9]*) is due on (\\d+\\D+ \\w{3}\\'\\d{2})");
    localObject[i1] = localPattern;
    localPattern = Pattern.compile("(?i).*loan details are as follows- Amount (INR|inr|Rs|Rs.) ([\\d,.]*).*EMI (INR|inr|Rs|Rs.) ([\\d,.]*)");
    localObject[i2] = localPattern;
    localPattern = Pattern.compile("(?i).*(over credit limit|unpaid|has bounced|did not clear|overdue| in arrears|insufficient funds).*");
    localObject[i3] = localPattern;
    localObject = Arrays.asList((Object[])localObject);
    localArrayList.<init>((Collection)localObject);
    p = localArrayList;
    localArrayList = new java/util/ArrayList;
    localObject = new Pattern[21];
    localPattern = Pattern.compile("(?i)Dear Customer,( )*Gentle reminder that your EMI of (Rs\\D*|INR\\D*|inr\\D*)([\\d,.]*)\\D*loan a\\/c no. (\\S*) is due on ([0-9A-Za-z-:\\/]*).");
    localObject[0] = localPattern;
    localPattern = Pattern.compile("(?i)Dear Customer,( )*Gentle reminder\\D*([\\d,.]*)\\D*a\\/c( )*no.(\\S*) is due on ([0-9A-Za-z-:\\/]*).");
    localObject[i1] = localPattern;
    localPattern = Pattern.compile("(?i)Dear Customer, the EMI on your HDFC Loan acct no. (\\S*) will be debited from your nominated acct. for a value of (Rs.|INR|inr)([\\d,.]*) on ([0-9A-Za-z-:\\/]*)");
    localObject[i2] = localPattern;
    localPattern = Pattern.compile("(?i)Dear Customer, First EMI due on ([0-9A-za-z-:\\/]*) is (Rs|INR|inr) ([\\d,.]*) on HDFC Bank loan (\\S*) \\D*");
    localObject[i3] = localPattern;
    localPattern = Pattern.compile("(?i)Alert from HDFC Bank. Next EMI of (Rs.|INR|inr)([\\d,.]*) on your loan account: (\\S*) due on ([0-9A-za-z-:\\/]*).\\D*");
    localObject[i4] = localPattern;
    localPattern = Pattern.compile("(?i)Dear Customer. Greetings from HDFC Bank. We wish to remind you that an EMI of (Rs|INR|inr) ([\\d,.]*)\\/- on your HDFC loan A\\/c (\\S*) falls due on the ([0-9A-za-z-:\\/]*).");
    localObject[i5] = localPattern;
    localPattern = Pattern.compile("(?i)Dear HDFC Bank Cardmember, (\\d|\\D)* is converted to EMI");
    localObject[i6] = localPattern;
    localPattern = Pattern.compile("(?i)Dear Sir, Loan application of (\\D*) of (\\D* (loan|LOAN)),(\\d|\\D)* is Disbursed");
    localObject[i7] = localPattern;
    localPattern = Pattern.compile("(?i)Dear Customer, First EMI due on ([0-9A-Za-z-:\\/]*) is Rs ([\\d,.]*) \\D*(\\S*)");
    localObject[i8] = localPattern;
    localPattern = Pattern.compile("(?i)(HDFC|hdfc) (EMI|emi) alert \\D*loan ac no (\\S*)");
    localObject[i9] = localPattern;
    localPattern = Pattern.compile("(?i)Dear Customer, Greetings from (HDFC|hdfc) (BANK|bank). Timely \\D*Rs.([\\d,.]*)\\D*No: (\\S*) is due on ([0-9A-Za-z-:\\/]*)");
    localObject[i10] = localPattern;
    localPattern = Pattern.compile("(?i)\\D*, Txn request on \\S* for Rs.([0-9,.]*)\\D* is converted to (EMI|emi)");
    localObject[i11] = localPattern;
    localPattern = Pattern.compile("(?i)Dear Customer, ride home your dream bike with an HDFC Bank Two Wheeler Loan.");
    localObject[i12] = localPattern;
    localPattern = Pattern.compile("(?i)Your Loan account (\\S*) is now in much better health.");
    localObject[i13] = localPattern;
    localPattern = Pattern.compile("(?i)Get Insta.*");
    localObject[i14] = localPattern;
    localPattern = Pattern.compile("(?i)Convert.* EMI.*");
    localObject[15] = localPattern;
    localPattern = Pattern.compile("(?i)Get Jumbo Loan of Rs\\D*([\\d,.]+).*");
    localObject[16] = localPattern;
    localPattern = Pattern.compile("(?i)Pre-approved Insta Loan\\D*Rs\\D*([\\d,.]+).*");
    localObject[17] = localPattern;
    localPattern = Pattern.compile("(?i).*Pre-approved EMI Gold Loan.*");
    localObject[18] = localPattern;
    localPattern = Pattern.compile("(?i).*Insta Loan upto Rs\\.*\\D*([\\d,.]+).*");
    localObject[19] = localPattern;
    localPattern = Pattern.compile("(?i).*(over credit limit|unpaid|has bounced|did not clear|overdue| in arrears|insufficient funds).*");
    localObject[20] = localPattern;
    localObject = Arrays.asList((Object[])localObject);
    localArrayList.<init>((Collection)localObject);
    q = localArrayList;
    localArrayList = new java/util/ArrayList;
    localObject = new Pattern[i4];
    localPattern = Pattern.compile("(?i)Dear Customer, EMI of Rs. ([\\d,.]*) (\\d|\\D)* Loan (\\S*) is due on ([0-9A-za-z-:\\/]*).");
    localObject[0] = localPattern;
    localPattern = Pattern.compile("(?i).*EMI (for|of) Rs.( |)([\\d,.]*).* Loan (Acct|Account)( |)(\\S*).*on ([0-9A-za-z-:\\/]*)");
    localObject[i1] = localPattern;
    localPattern = Pattern.compile("(?i).*credit card.*");
    localObject[i2] = localPattern;
    localPattern = Pattern.compile("(?i).*(over credit limit|unpaid|has bounced|did not clear|overdue| in arrears|insufficient funds|cancelled).*");
    localObject[i3] = localPattern;
    localObject = Arrays.asList((Object[])localObject);
    localArrayList.<init>((Collection)localObject);
    r = localArrayList;
    localArrayList = new java/util/ArrayList;
    localObject = new Pattern[i2];
    localPattern = Pattern.compile("(?i).*bounced.*");
    localObject[0] = localPattern;
    localPattern = Pattern.compile("(?i).*cheque.*");
    localObject[i1] = localPattern;
    localObject = Arrays.asList((Object[])localObject);
    localArrayList.<init>((Collection)localObject);
    s = localArrayList;
    localArrayList = new java/util/ArrayList;
    localObject = new Pattern[i2];
    localPattern = Pattern.compile("(?i).*premium.*");
    localObject[0] = localPattern;
    localPattern = Pattern.compile("(?i).*insurance.*");
    localObject[i1] = localPattern;
    localObject = Arrays.asList((Object[])localObject);
    localArrayList.<init>((Collection)localObject);
    t = localArrayList;
    localArrayList = new java/util/ArrayList;
    localObject = new Pattern[i10];
    localPattern = Pattern.compile("(?i).* get .*insurance.*to (enrol|subscribe).*");
    localObject[0] = localPattern;
    localPattern = Pattern.compile("(?i)sms (pmsby|pmjjby|muthoot|family).*");
    localObject[i1] = localPattern;
    localPattern = Pattern.compile("(?i).*dear customer, pay your.*insurance.*just a few clicks.*");
    localObject[i2] = localPattern;
    localPattern = Pattern.compile("(?i).*get.*insurance.*save tax.*");
    localObject[i3] = localPattern;
    localPattern = Pattern.compile("(?i).*your.*(has been|is) credited.*");
    localObject[i4] = localPattern;
    localPattern = Pattern.compile("(?i)Dear Customer, due premiums? No more a hassle.*");
    localObject[i5] = localPattern;
    localPattern = Pattern.compile("(?i).*get.*insurance.*save up.*");
    localObject[i6] = localPattern;
    localPattern = Pattern.compile("(?i).*regular use of Axis Bank Debit Card.*");
    localObject[i7] = localPattern;
    localPattern = Pattern.compile("(?i).*get.*insurance.*to know.*");
    localObject[i8] = localPattern;
    localPattern = Pattern.compile("(?i).*bajaj.*brin?gs you (tax saving|health insurance).*");
    localObject[i9] = localPattern;
    localObject = Arrays.asList((Object[])localObject);
    localArrayList.<init>((Collection)localObject);
    u = localArrayList;
    localArrayList = new java/util/ArrayList;
    localObject = new Pattern[34];
    localPattern = Pattern.compile("(?i)Tired of tracking.*");
    localObject[0] = localPattern;
    localPattern = Pattern.compile("(?i)1082.*");
    localObject[i1] = localPattern;
    localPattern = Pattern.compile("(?i).*StanChart credit card.*");
    localObject[i2] = localPattern;
    localPattern = Pattern.compile("(?i).*To enroll, SMS.*");
    localObject[i3] = localPattern;
    localPattern = Pattern.compile("(?i).*Your premium is due.*");
    localObject[i4] = localPattern;
    localPattern = Pattern.compile("(?i).*Thank you for your payment.*");
    localObject[i5] = localPattern;
    localPattern = Pattern.compile("(?i).*due for renewal.*");
    localObject[i6] = localPattern;
    localPattern = Pattern.compile("(?i).*pay premium at your.*");
    localObject[i7] = localPattern;
    localPattern = Pattern.compile("(?i).*premium payment for your policy.*");
    localObject[i8] = localPattern;
    localPattern = Pattern.compile("(?i).*premium was due for policy.*");
    localObject[i9] = localPattern;
    localPattern = Pattern.compile("(?i).*renewal premium payment for your.*");
    localObject[i10] = localPattern;
    localPattern = Pattern.compile("(?i).*revival of.*policy.*");
    localObject[i11] = localPattern;
    localPattern = Pattern.compile("(?i).*Premium for.*policy.*due.*");
    localObject[i12] = localPattern;
    localPattern = Pattern.compile("(?i).*pay your.*policy.*due premium using.*");
    localObject[i13] = localPattern;
    localPattern = Pattern.compile("(?i).*kindly pay your renewal.*");
    localObject[i14] = localPattern;
    localPattern = Pattern.compile("(?i).*pay your renewal premium of.*");
    localObject[15] = localPattern;
    localPattern = Pattern.compile("(?i).*pay your premium before.*");
    localObject[16] = localPattern;
    localPattern = Pattern.compile("(?i).*dear policyholder.*");
    localObject[17] = localPattern;
    localPattern = Pattern.compile("(?i).*dear customer.*");
    localObject[18] = localPattern;
    localPattern = Pattern.compile("(?i).*You can now pay the premium.*policy.*");
    localObject[19] = localPattern;
    localPattern = Pattern.compile("(?i).*Your a/c no..*registered for.*with.*");
    localObject[20] = localPattern;
    localPattern = Pattern.compile("(?i).*insurance expiring on.*");
    localObject[21] = localPattern;
    localPattern = Pattern.compile("(?i).*Your premium of Rs..*");
    localObject[22] = localPattern;
    localPattern = Pattern.compile("(?i).*Enhance your life insurance.*");
    localObject[23] = localPattern;
    localPattern = Pattern.compile("(?i).*health insurance.*due for.*enewal.*");
    localObject[24] = localPattern;
    localPattern = Pattern.compile("(?i).*insurance will lapse due.*");
    localObject[25] = localPattern;
    localPattern = Pattern.compile("(?i).*insurance.*is due in.*");
    localObject[26] = localPattern;
    localPattern = Pattern.compile("(?i).*your.*insurance policy.*due on.*");
    localObject[27] = localPattern;
    localPattern = Pattern.compile("(?i).*insurance premium has been debited.*");
    localObject[28] = localPattern;
    localPattern = Pattern.compile("(?i)dear.*customer.*your.*insurance.*renewed.*");
    localObject[29] = localPattern;
    localPattern = Pattern.compile("(?i).*your health.*policy.*expires.*");
    localObject[30] = localPattern;
    localPattern = Pattern.compile("(?i).*towards your.*insurance policy.*");
    localObject[31] = localPattern;
    localPattern = Pattern.compile("(?i).*insurance.*expiring soon.*");
    localObject[32] = localPattern;
    localPattern = Pattern.compile("(?i).*insurance is due on.*");
    localObject[33] = localPattern;
    localObject = Arrays.asList((Object[])localObject);
    localArrayList.<init>((Collection)localObject);
    v = localArrayList;
  }
  
  private static c.a a(JSONObject paramJSONObject, Matcher paramMatcher)
  {
    String str1 = paramJSONObject.optString("parent_field");
    JSONObject localJSONObject = paramJSONObject.optJSONObject("child_field");
    String str2 = "deleted";
    int i1 = 0;
    boolean bool1 = paramJSONObject.optBoolean(str2, false);
    String str3 = "incomplete";
    boolean bool2 = paramJSONObject.optBoolean(str3, false);
    String str4 = paramJSONObject.optString("match_type");
    String str5 = "delta";
    boolean bool3 = TextUtils.equals(str4, str5);
    long l1 = -1;
    if (bool3)
    {
      str5 = "match_value";
      l1 = paramJSONObject.optLong(str5, l1);
    }
    if (localJSONObject != null)
    {
      paramJSONObject = "group_id";
      int i2 = -1;
      int i3 = localJSONObject.optInt(paramJSONObject, i2);
      if (i3 >= 0)
      {
        paramJSONObject = paramMatcher.group(i3);
        paramJSONObject = c(str1, paramJSONObject);
      }
      else
      {
        paramJSONObject = localJSONObject.optString("field", "Unknown");
        paramMatcher = "Unknown";
        bool4 = TextUtils.equals(paramJSONObject, paramMatcher);
        if (bool4)
        {
          paramMatcher = "Unknown";
          paramJSONObject = localJSONObject.optString("value", paramMatcher);
        }
      }
    }
    else
    {
      paramMatcher = "child_field";
      paramJSONObject = paramJSONObject.optString(paramMatcher);
    }
    paramMatcher = "deleted";
    boolean bool4 = TextUtils.equals(str1, paramMatcher);
    if (!bool4)
    {
      paramMatcher = "pattern_UID";
      bool4 = TextUtils.equals(str1, paramMatcher);
      if (!bool4) {}
    }
    else
    {
      paramMatcher = "exact";
      bool4 = TextUtils.equals(str4, paramMatcher);
      if (bool4)
      {
        i1 = 2;
      }
      else
      {
        paramMatcher = "contains";
        bool4 = TextUtils.equals(str4, paramMatcher);
        if (bool4)
        {
          i1 = 1;
        }
        else
        {
          paramMatcher = "none";
          bool4 = TextUtils.equals(str4, paramMatcher);
          if (bool4) {
            i1 = 3;
          }
        }
      }
    }
    paramMatcher = new com/daamitt/prime/sdk/a/c$a;
    paramMatcher.<init>();
    a = str1;
    b = paramJSONObject;
    f = bool1;
    g = bool2;
    c = str4;
    d = i1;
    e = l1;
    return paramMatcher;
  }
  
  /* Error */
  private static c.b a(JSONObject paramJSONObject)
  {
    // Byte code:
    //   0: new 1003	com/daamitt/prime/sdk/a/c$b
    //   3: astore_1
    //   4: aload_1
    //   5: invokespecial 1004	com/daamitt/prime/sdk/a/c$b:<init>	()V
    //   8: iconst_0
    //   9: istore_2
    //   10: ldc_w 1006
    //   13: astore_3
    //   14: aload_0
    //   15: aload_3
    //   16: invokevirtual 1010	org/json/JSONObject:getJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   19: astore_3
    //   20: aload_3
    //   21: ifnull +92 -> 113
    //   24: iconst_0
    //   25: istore 4
    //   27: aconst_null
    //   28: astore 5
    //   30: aload_3
    //   31: invokevirtual 1016	org/json/JSONArray:length	()I
    //   34: istore 6
    //   36: iload 4
    //   38: iload 6
    //   40: if_icmpge +73 -> 113
    //   43: aload_3
    //   44: iload 4
    //   46: invokevirtual 1020	org/json/JSONArray:getJSONObject	(I)Lorg/json/JSONObject;
    //   49: astore 7
    //   51: aload 7
    //   53: aconst_null
    //   54: invokestatic 1023	com/daamitt/prime/sdk/a:a	(Lorg/json/JSONObject;Ljava/util/regex/Matcher;)Lcom/daamitt/prime/sdk/a/c$a;
    //   57: astore 7
    //   59: aload_1
    //   60: getfield 1025	com/daamitt/prime/sdk/a/c$b:a	Ljava/util/ArrayList;
    //   63: astore 8
    //   65: aload 8
    //   67: ifnonnull +19 -> 86
    //   70: new 67	java/util/ArrayList
    //   73: astore 8
    //   75: aload 8
    //   77: invokespecial 1026	java/util/ArrayList:<init>	()V
    //   80: aload_1
    //   81: aload 8
    //   83: putfield 1025	com/daamitt/prime/sdk/a/c$b:a	Ljava/util/ArrayList;
    //   86: aload_1
    //   87: getfield 1025	com/daamitt/prime/sdk/a/c$b:a	Ljava/util/ArrayList;
    //   90: astore 8
    //   92: aload 8
    //   94: aload 7
    //   96: invokevirtual 1030	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   99: pop
    //   100: iload 4
    //   102: iconst_1
    //   103: iadd
    //   104: istore 4
    //   106: goto -76 -> 30
    //   109: pop
    //   110: invokestatic 1034	com/daamitt/prime/sdk/a/e:b	()V
    //   113: ldc_w 1036
    //   116: astore_3
    //   117: aload_0
    //   118: aload_3
    //   119: invokevirtual 1010	org/json/JSONObject:getJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   122: astore_0
    //   123: aload_0
    //   124: ifnull +78 -> 202
    //   127: aload_0
    //   128: invokevirtual 1016	org/json/JSONArray:length	()I
    //   131: istore 9
    //   133: iload_2
    //   134: iload 9
    //   136: if_icmpge +66 -> 202
    //   139: aload_0
    //   140: iload_2
    //   141: invokevirtual 1020	org/json/JSONArray:getJSONObject	(I)Lorg/json/JSONObject;
    //   144: astore_3
    //   145: aload_3
    //   146: aconst_null
    //   147: invokestatic 1023	com/daamitt/prime/sdk/a:a	(Lorg/json/JSONObject;Ljava/util/regex/Matcher;)Lcom/daamitt/prime/sdk/a/c$a;
    //   150: astore_3
    //   151: aload_1
    //   152: getfield 1038	com/daamitt/prime/sdk/a/c$b:b	Ljava/util/ArrayList;
    //   155: astore 5
    //   157: aload 5
    //   159: ifnonnull +19 -> 178
    //   162: new 67	java/util/ArrayList
    //   165: astore 5
    //   167: aload 5
    //   169: invokespecial 1026	java/util/ArrayList:<init>	()V
    //   172: aload_1
    //   173: aload 5
    //   175: putfield 1038	com/daamitt/prime/sdk/a/c$b:b	Ljava/util/ArrayList;
    //   178: aload_1
    //   179: getfield 1038	com/daamitt/prime/sdk/a/c$b:b	Ljava/util/ArrayList;
    //   182: astore 5
    //   184: aload 5
    //   186: aload_3
    //   187: invokevirtual 1030	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   190: pop
    //   191: iload_2
    //   192: iconst_1
    //   193: iadd
    //   194: istore_2
    //   195: goto -68 -> 127
    //   198: pop
    //   199: invokestatic 1034	com/daamitt/prime/sdk/a/e:b	()V
    //   202: aload_1
    //   203: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	204	0	paramJSONObject	JSONObject
    //   3	200	1	localb	c.b
    //   9	186	2	i1	int
    //   13	174	3	localObject1	Object
    //   25	80	4	i2	int
    //   28	157	5	localArrayList1	ArrayList
    //   34	7	6	i3	int
    //   49	46	7	localObject2	Object
    //   63	30	8	localArrayList2	ArrayList
    //   131	6	9	i4	int
    //   109	1	10	localJSONException1	JSONException
    //   198	1	11	localJSONException2	JSONException
    // Exception table:
    //   from	to	target	type
    //   15	19	109	org/json/JSONException
    //   30	34	109	org/json/JSONException
    //   44	49	109	org/json/JSONException
    //   53	57	109	org/json/JSONException
    //   59	63	109	org/json/JSONException
    //   70	73	109	org/json/JSONException
    //   75	80	109	org/json/JSONException
    //   81	86	109	org/json/JSONException
    //   86	90	109	org/json/JSONException
    //   94	100	109	org/json/JSONException
    //   118	122	198	org/json/JSONException
    //   127	131	198	org/json/JSONException
    //   140	144	198	org/json/JSONException
    //   146	150	198	org/json/JSONException
    //   151	155	198	org/json/JSONException
    //   162	165	198	org/json/JSONException
    //   167	172	198	org/json/JSONException
    //   173	178	198	org/json/JSONException
    //   178	182	198	org/json/JSONException
    //   186	191	198	org/json/JSONException
  }
  
  private static k a(String paramString1, String paramString2, String paramString3, Date paramDate)
  {
    k localk = new com/daamitt/prime/sdk/a/k;
    localk.<init>(paramString2, paramString3, paramDate);
    localk.a(paramString1, "Messages");
    v = 99;
    return localk;
  }
  
  /* Error */
  private static com.daamitt.prime.sdk.a.m a(Matcher paramMatcher, h paramh, String paramString1, String paramString2, Date paramDate)
  {
    // Byte code:
    //   0: aload_0
    //   1: astore 5
    //   3: aload_1
    //   4: astore 6
    //   6: aload 4
    //   8: astore 7
    //   10: iconst_0
    //   11: istore 8
    //   13: aconst_null
    //   14: astore 9
    //   16: new 1054	com/daamitt/prime/sdk/a/m
    //   19: astore 10
    //   21: aload_2
    //   22: astore 11
    //   24: aload 10
    //   26: aload_2
    //   27: aload_3
    //   28: aload 4
    //   30: invokespecial 1055	com/daamitt/prime/sdk/a/m:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)V
    //   33: iconst_1
    //   34: istore 12
    //   36: aload 10
    //   38: iload 12
    //   40: putfield 1058	com/daamitt/prime/sdk/a/k:E	Z
    //   43: aload_1
    //   44: getfield 1061	com/daamitt/prime/sdk/a/h:c	Ljava/lang/String;
    //   47: astore 11
    //   49: aload 11
    //   51: invokestatic 1066	com/daamitt/prime/sdk/a/a:a	(Ljava/lang/String;)I
    //   54: istore 13
    //   56: aload 10
    //   58: iload 13
    //   60: putfield 1052	com/daamitt/prime/sdk/a/k:v	I
    //   63: aload_1
    //   64: getfield 1068	com/daamitt/prime/sdk/a/h:e	Ljava/lang/String;
    //   67: astore 11
    //   69: ldc_w 1070
    //   72: astore 14
    //   74: aload 10
    //   76: aload 11
    //   78: aload 14
    //   80: invokevirtual 1071	com/daamitt/prime/sdk/a/m:a	(Ljava/lang/String;Ljava/lang/String;)V
    //   83: aload_1
    //   84: getfield 1073	com/daamitt/prime/sdk/a/h:f	Ljava/lang/String;
    //   87: astore 11
    //   89: aload 10
    //   91: aload 11
    //   93: putfield 1076	com/daamitt/prime/sdk/a/k:B	Ljava/lang/String;
    //   96: aload_1
    //   97: getfield 1079	com/daamitt/prime/sdk/a/h:g	Lorg/json/JSONObject;
    //   100: astore 15
    //   102: ldc_w 1081
    //   105: astore 11
    //   107: aload 15
    //   109: aload 11
    //   111: invokevirtual 1085	org/json/JSONObject:has	(Ljava/lang/String;)Z
    //   114: istore 13
    //   116: iload 13
    //   118: ifeq +47 -> 165
    //   121: ldc_w 1081
    //   124: astore 11
    //   126: aload 15
    //   128: aload 11
    //   130: iload 12
    //   132: invokevirtual 932	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   135: istore 13
    //   137: iload 13
    //   139: ifeq +11 -> 150
    //   142: aload 10
    //   144: invokevirtual 1087	com/daamitt/prime/sdk/a/m:n	()V
    //   147: goto +8 -> 155
    //   150: aload 10
    //   152: invokevirtual 1089	com/daamitt/prime/sdk/a/m:m	()V
    //   155: aload 10
    //   157: iload 12
    //   159: putfield 1092	com/daamitt/prime/sdk/a/m:ac	Z
    //   162: goto +8 -> 170
    //   165: aload 10
    //   167: invokevirtual 1087	com/daamitt/prime/sdk/a/m:n	()V
    //   170: aload 6
    //   172: getfield 1094	com/daamitt/prime/sdk/a/h:i	Z
    //   175: istore 16
    //   177: aload 10
    //   179: iload 16
    //   181: putfield 1097	com/daamitt/prime/sdk/a/k:L	Z
    //   184: ldc_w 1099
    //   187: astore 6
    //   189: aload 15
    //   191: aload 6
    //   193: invokevirtual 926	org/json/JSONObject:optJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   196: astore 6
    //   198: iconst_m1
    //   199: istore 13
    //   201: aconst_null
    //   202: astore 17
    //   204: aload 6
    //   206: ifnull +195 -> 401
    //   209: ldc_w 1101
    //   212: astore 14
    //   214: aload 6
    //   216: aload 14
    //   218: iconst_0
    //   219: invokevirtual 932	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   222: istore 18
    //   224: iload 18
    //   226: ifeq +8 -> 234
    //   229: aload 10
    //   231: invokevirtual 1103	com/daamitt/prime/sdk/a/m:j	()V
    //   234: ldc_w 952
    //   237: astore 14
    //   239: aload 6
    //   241: aload 14
    //   243: iload 13
    //   245: invokevirtual 956	org/json/JSONObject:optInt	(Ljava/lang/String;I)I
    //   248: istore 18
    //   250: iload 18
    //   252: iflt +15 -> 267
    //   255: aload 5
    //   257: iload 18
    //   259: invokevirtual 962	java/util/regex/Matcher:group	(I)Ljava/lang/String;
    //   262: astore 6
    //   264: goto +143 -> 407
    //   267: ldc_w 1105
    //   270: astore 14
    //   272: aload 6
    //   274: aload 14
    //   276: invokevirtual 1108	org/json/JSONObject:optJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   279: astore 14
    //   281: aload 14
    //   283: ifnull +96 -> 379
    //   286: new 1110	java/lang/StringBuilder
    //   289: astore 6
    //   291: aload 6
    //   293: invokespecial 1111	java/lang/StringBuilder:<init>	()V
    //   296: iconst_0
    //   297: istore 19
    //   299: aconst_null
    //   300: astore 20
    //   302: aload 14
    //   304: invokevirtual 1016	org/json/JSONArray:length	()I
    //   307: istore 21
    //   309: iload 19
    //   311: iload 21
    //   313: if_icmpge +56 -> 369
    //   316: iload 19
    //   318: ifle +16 -> 334
    //   321: ldc_w 1113
    //   324: astore 22
    //   326: aload 6
    //   328: aload 22
    //   330: invokevirtual 1117	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   333: pop
    //   334: aload 14
    //   336: iload 19
    //   338: invokevirtual 1121	org/json/JSONArray:getInt	(I)I
    //   341: istore 21
    //   343: aload 5
    //   345: iload 21
    //   347: invokevirtual 962	java/util/regex/Matcher:group	(I)Ljava/lang/String;
    //   350: astore 22
    //   352: aload 6
    //   354: aload 22
    //   356: invokevirtual 1117	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   359: pop
    //   360: iload 19
    //   362: iconst_1
    //   363: iadd
    //   364: istore 19
    //   366: goto -64 -> 302
    //   369: aload 6
    //   371: invokevirtual 1125	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   374: astore 6
    //   376: goto +31 -> 407
    //   379: ldc_w 973
    //   382: astore 14
    //   384: aload 6
    //   386: aload 14
    //   388: invokevirtual 920	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   391: astore 6
    //   393: aload 10
    //   395: invokevirtual 1103	com/daamitt/prime/sdk/a/m:j	()V
    //   398: goto +9 -> 407
    //   401: iconst_0
    //   402: istore 16
    //   404: aconst_null
    //   405: astore 6
    //   407: aload 6
    //   409: invokestatic 1129	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   412: istore 18
    //   414: iload 18
    //   416: ifeq +28 -> 444
    //   419: aload 10
    //   421: invokevirtual 1132	com/daamitt/prime/sdk/a/m:k	()Z
    //   424: istore 16
    //   426: iload 16
    //   428: ifeq +6 -> 434
    //   431: invokestatic 1034	com/daamitt/prime/sdk/a/e:b	()V
    //   434: ldc_w 969
    //   437: astore 6
    //   439: aload 10
    //   441: invokevirtual 1103	com/daamitt/prime/sdk/a/m:j	()V
    //   444: aload 6
    //   446: invokestatic 1134	com/daamitt/prime/sdk/a:d	(Ljava/lang/String;)Ljava/lang/String;
    //   449: astore 6
    //   451: ldc_w 1136
    //   454: astore 14
    //   456: aload 15
    //   458: aload 14
    //   460: aconst_null
    //   461: invokevirtual 971	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   464: astore 14
    //   466: aload 14
    //   468: ifnonnull +256 -> 724
    //   471: ldc_w 1138
    //   474: astore 20
    //   476: aload 15
    //   478: aload 20
    //   480: invokevirtual 1140	org/json/JSONObject:getJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   483: astore 20
    //   485: ldc_w 952
    //   488: astore 22
    //   490: aload 20
    //   492: aload 22
    //   494: invokevirtual 1142	org/json/JSONObject:getInt	(Ljava/lang/String;)I
    //   497: istore 21
    //   499: iload 21
    //   501: iflt +223 -> 724
    //   504: aload 5
    //   506: iload 21
    //   508: invokevirtual 962	java/util/regex/Matcher:group	(I)Ljava/lang/String;
    //   511: astore 22
    //   513: aload 22
    //   515: invokevirtual 1147	java/lang/String:trim	()Ljava/lang/String;
    //   518: astore 22
    //   520: aload 22
    //   522: invokevirtual 1150	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   525: astore 22
    //   527: ldc_w 1152
    //   530: astore 23
    //   532: aload 20
    //   534: aload 23
    //   536: invokevirtual 1010	org/json/JSONObject:getJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   539: astore 20
    //   541: iconst_0
    //   542: istore 24
    //   544: aconst_null
    //   545: astore 23
    //   547: aload 20
    //   549: invokevirtual 1016	org/json/JSONArray:length	()I
    //   552: istore 25
    //   554: iload 24
    //   556: iload 25
    //   558: if_icmpge +166 -> 724
    //   561: aload 20
    //   563: iload 24
    //   565: invokevirtual 1020	org/json/JSONArray:getJSONObject	(I)Lorg/json/JSONObject;
    //   568: astore 26
    //   570: ldc_w 973
    //   573: astore 27
    //   575: aload 26
    //   577: aload 27
    //   579: invokevirtual 1155	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   582: astore 27
    //   584: aload 22
    //   586: aload 27
    //   588: invokevirtual 1157	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   591: istore 28
    //   593: iload 28
    //   595: ifeq +120 -> 715
    //   598: ldc_w 1159
    //   601: astore 14
    //   603: aload 26
    //   605: aload 14
    //   607: invokevirtual 1155	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   610: astore 14
    //   612: ldc_w 1161
    //   615: astore 20
    //   617: aload 26
    //   619: aload 20
    //   621: invokevirtual 920	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   624: astore 20
    //   626: ldc_w 1101
    //   629: astore 22
    //   631: aload 26
    //   633: aload 22
    //   635: iconst_0
    //   636: invokevirtual 932	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   639: istore 21
    //   641: iload 21
    //   643: ifeq +8 -> 651
    //   646: aload 10
    //   648: invokevirtual 1103	com/daamitt/prime/sdk/a/m:j	()V
    //   651: aload 20
    //   653: invokevirtual 1163	java/lang/String:isEmpty	()Z
    //   656: istore 21
    //   658: iload 21
    //   660: ifne +17 -> 677
    //   663: aload 20
    //   665: invokestatic 1066	com/daamitt/prime/sdk/a/a:a	(Ljava/lang/String;)I
    //   668: istore 19
    //   670: aload 10
    //   672: iload 19
    //   674: putfield 1052	com/daamitt/prime/sdk/a/k:v	I
    //   677: ldc_w 1165
    //   680: astore 20
    //   682: aload 26
    //   684: aload 20
    //   686: invokevirtual 920	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   689: astore 20
    //   691: aload 20
    //   693: invokevirtual 1163	java/lang/String:isEmpty	()Z
    //   696: istore 21
    //   698: iload 21
    //   700: ifne +24 -> 724
    //   703: aload 10
    //   705: invokevirtual 1103	com/daamitt/prime/sdk/a/m:j	()V
    //   708: aload 20
    //   710: astore 6
    //   712: goto +12 -> 724
    //   715: iload 24
    //   717: iconst_1
    //   718: iadd
    //   719: istore 24
    //   721: goto -174 -> 547
    //   724: aload 10
    //   726: getfield 1052	com/daamitt/prime/sdk/a/k:v	I
    //   729: istore 19
    //   731: sipush 9999
    //   734: istore 21
    //   736: iload 19
    //   738: iload 21
    //   740: if_icmpne +8 -> 748
    //   743: invokestatic 1034	com/daamitt/prime/sdk/a/e:b	()V
    //   746: aconst_null
    //   747: areturn
    //   748: ldc_w 1168
    //   751: astore 20
    //   753: aload 15
    //   755: aload 20
    //   757: invokevirtual 926	org/json/JSONObject:optJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   760: astore 20
    //   762: aload 20
    //   764: ifnull +2928 -> 3692
    //   767: ldc_w 952
    //   770: astore 22
    //   772: aload 20
    //   774: aload 22
    //   776: iload 13
    //   778: invokevirtual 956	org/json/JSONObject:optInt	(Ljava/lang/String;I)I
    //   781: istore 21
    //   783: iconst_2
    //   784: istore 28
    //   786: iload 21
    //   788: iflt +15 -> 803
    //   791: aload 5
    //   793: iload 21
    //   795: invokevirtual 962	java/util/regex/Matcher:group	(I)Ljava/lang/String;
    //   798: astore 20
    //   800: goto +41 -> 841
    //   803: ldc_w 973
    //   806: astore 22
    //   808: ldc_w 969
    //   811: astore 23
    //   813: aload 20
    //   815: aload 22
    //   817: aload 23
    //   819: invokevirtual 971	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   822: astore 20
    //   824: aload 10
    //   826: getfield 1171	com/daamitt/prime/sdk/a/m:Y	I
    //   829: iload 28
    //   831: ior
    //   832: istore 21
    //   834: aload 10
    //   836: iload 21
    //   838: putfield 1171	com/daamitt/prime/sdk/a/m:Y	I
    //   841: ldc_w 1173
    //   844: astore 22
    //   846: aload 15
    //   848: aload 22
    //   850: invokevirtual 926	org/json/JSONObject:optJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   853: astore 22
    //   855: ldc2_w 1174
    //   858: dstore 29
    //   860: dload 29
    //   862: invokestatic 1181	java/lang/Double:valueOf	(D)Ljava/lang/Double;
    //   865: astore 23
    //   867: aload 22
    //   869: ifnull +138 -> 1007
    //   872: ldc_w 952
    //   875: astore 26
    //   877: aload 22
    //   879: aload 26
    //   881: iload 13
    //   883: invokevirtual 956	org/json/JSONObject:optInt	(Ljava/lang/String;I)I
    //   886: istore 25
    //   888: iload 25
    //   890: iflt +117 -> 1007
    //   893: aload 5
    //   895: iload 25
    //   897: invokevirtual 962	java/util/regex/Matcher:group	(I)Ljava/lang/String;
    //   900: astore 26
    //   902: ldc_w 1183
    //   905: astore 27
    //   907: ldc_w 1185
    //   910: astore 31
    //   912: aload 26
    //   914: aload 27
    //   916: aload 31
    //   918: invokevirtual 1189	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   921: astore 26
    //   923: aload 26
    //   925: invokestatic 1192	java/lang/Double:valueOf	(Ljava/lang/String;)Ljava/lang/Double;
    //   928: astore 23
    //   930: ldc_w 1194
    //   933: astore 26
    //   935: ldc_w 1185
    //   938: astore 31
    //   940: aload 22
    //   942: aload 26
    //   944: aload 31
    //   946: invokevirtual 971	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   949: astore 22
    //   951: ldc_w 1196
    //   954: astore 26
    //   956: aload 22
    //   958: aload 26
    //   960: invokevirtual 1199	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
    //   963: istore 21
    //   965: iload 21
    //   967: ifeq +22 -> 989
    //   970: aload 23
    //   972: invokevirtual 1203	java/lang/Double:doubleValue	()D
    //   975: dneg
    //   976: dstore 32
    //   978: dload 32
    //   980: invokestatic 1181	java/lang/Double:valueOf	(D)Ljava/lang/Double;
    //   983: astore 22
    //   985: aload 22
    //   987: astore 23
    //   989: aload 23
    //   991: astore 17
    //   993: goto +18 -> 1011
    //   996: pop
    //   997: invokestatic 1205	com/daamitt/prime/sdk/a/e:d	()V
    //   1000: aload 23
    //   1002: astore 17
    //   1004: goto +7 -> 1011
    //   1007: aload 23
    //   1009: astore 17
    //   1011: ldc_w 1207
    //   1014: astore 22
    //   1016: aload 15
    //   1018: aload 22
    //   1020: invokevirtual 926	org/json/JSONObject:optJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   1023: astore 22
    //   1025: dload 29
    //   1027: invokestatic 1181	java/lang/Double:valueOf	(D)Ljava/lang/Double;
    //   1030: astore 23
    //   1032: aload 22
    //   1034: ifnull +138 -> 1172
    //   1037: ldc_w 952
    //   1040: astore 26
    //   1042: aload 22
    //   1044: aload 26
    //   1046: iload 13
    //   1048: invokevirtual 956	org/json/JSONObject:optInt	(Ljava/lang/String;I)I
    //   1051: istore 25
    //   1053: iload 25
    //   1055: iflt +117 -> 1172
    //   1058: aload 5
    //   1060: iload 25
    //   1062: invokevirtual 962	java/util/regex/Matcher:group	(I)Ljava/lang/String;
    //   1065: astore 26
    //   1067: ldc_w 1183
    //   1070: astore 27
    //   1072: ldc_w 1185
    //   1075: astore 31
    //   1077: aload 26
    //   1079: aload 27
    //   1081: aload 31
    //   1083: invokevirtual 1189	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   1086: astore 26
    //   1088: aload 26
    //   1090: invokestatic 1192	java/lang/Double:valueOf	(Ljava/lang/String;)Ljava/lang/Double;
    //   1093: astore 23
    //   1095: ldc_w 1194
    //   1098: astore 26
    //   1100: ldc_w 1185
    //   1103: astore 31
    //   1105: aload 22
    //   1107: aload 26
    //   1109: aload 31
    //   1111: invokevirtual 971	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   1114: astore 22
    //   1116: ldc_w 1196
    //   1119: astore 26
    //   1121: aload 22
    //   1123: aload 26
    //   1125: invokevirtual 1199	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
    //   1128: istore 21
    //   1130: iload 21
    //   1132: ifeq +22 -> 1154
    //   1135: aload 23
    //   1137: invokevirtual 1203	java/lang/Double:doubleValue	()D
    //   1140: dneg
    //   1141: dstore 34
    //   1143: dload 34
    //   1145: invokestatic 1181	java/lang/Double:valueOf	(D)Ljava/lang/Double;
    //   1148: astore 9
    //   1150: aload 9
    //   1152: astore 23
    //   1154: aload 23
    //   1156: astore 9
    //   1158: goto +18 -> 1176
    //   1161: pop
    //   1162: invokestatic 1205	com/daamitt/prime/sdk/a/e:d	()V
    //   1165: aload 23
    //   1167: astore 9
    //   1169: goto +7 -> 1176
    //   1172: aload 23
    //   1174: astore 9
    //   1176: ldc_w 1209
    //   1179: astore 11
    //   1181: aload 15
    //   1183: aload 11
    //   1185: invokevirtual 926	org/json/JSONObject:optJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   1188: astore 11
    //   1190: dconst_0
    //   1191: dstore 36
    //   1193: dload 36
    //   1195: invokestatic 1181	java/lang/Double:valueOf	(D)Ljava/lang/Double;
    //   1198: astore 31
    //   1200: aload 11
    //   1202: ifnull +152 -> 1354
    //   1205: ldc_w 952
    //   1208: astore 22
    //   1210: iconst_m1
    //   1211: istore 28
    //   1213: aload 11
    //   1215: aload 22
    //   1217: iload 28
    //   1219: invokevirtual 956	org/json/JSONObject:optInt	(Ljava/lang/String;I)I
    //   1222: istore 21
    //   1224: iload 21
    //   1226: istore 28
    //   1228: iload 21
    //   1230: iflt +144 -> 1374
    //   1233: aload 5
    //   1235: iload 21
    //   1237: invokevirtual 962	java/util/regex/Matcher:group	(I)Ljava/lang/String;
    //   1240: astore 31
    //   1242: ldc_w 1183
    //   1245: astore 27
    //   1247: ldc_w 1185
    //   1250: astore 22
    //   1252: aload 31
    //   1254: aload 27
    //   1256: aload 22
    //   1258: invokevirtual 1189	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   1261: astore 22
    //   1263: aload 22
    //   1265: invokestatic 1192	java/lang/Double:valueOf	(Ljava/lang/String;)Ljava/lang/Double;
    //   1268: astore 22
    //   1270: aload 22
    //   1272: astore 31
    //   1274: goto +18 -> 1292
    //   1277: pop
    //   1278: dload 36
    //   1280: invokestatic 1181	java/lang/Double:valueOf	(D)Ljava/lang/Double;
    //   1283: astore 22
    //   1285: invokestatic 1205	com/daamitt/prime/sdk/a/e:d	()V
    //   1288: aload 22
    //   1290: astore 31
    //   1292: ldc_w 1194
    //   1295: astore 22
    //   1297: ldc_w 1185
    //   1300: astore 27
    //   1302: aload 11
    //   1304: aload 22
    //   1306: aload 27
    //   1308: invokevirtual 971	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   1311: astore 11
    //   1313: ldc_w 1196
    //   1316: astore 22
    //   1318: aload 11
    //   1320: aload 22
    //   1322: invokevirtual 1199	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
    //   1325: istore 13
    //   1327: iload 13
    //   1329: ifeq +45 -> 1374
    //   1332: aload 31
    //   1334: invokevirtual 1203	java/lang/Double:doubleValue	()D
    //   1337: dstore 36
    //   1339: dload 36
    //   1341: dneg
    //   1342: dstore 38
    //   1344: dload 38
    //   1346: invokestatic 1181	java/lang/Double:valueOf	(D)Ljava/lang/Double;
    //   1349: astore 31
    //   1351: goto +23 -> 1374
    //   1354: aload 14
    //   1356: invokestatic 1210	com/daamitt/prime/sdk/a/m:a	(Ljava/lang/String;)I
    //   1359: istore 13
    //   1361: bipush 12
    //   1363: istore 21
    //   1365: iload 13
    //   1367: iload 21
    //   1369: if_icmpeq +5 -> 1374
    //   1372: aconst_null
    //   1373: areturn
    //   1374: aload 31
    //   1376: astore 23
    //   1378: ldc_w 1212
    //   1381: astore 11
    //   1383: aload 15
    //   1385: aload 11
    //   1387: invokevirtual 926	org/json/JSONObject:optJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   1390: astore 11
    //   1392: aload 11
    //   1394: ifnull +324 -> 1718
    //   1397: ldc_w 1214
    //   1400: astore 22
    //   1402: iconst_0
    //   1403: istore 12
    //   1405: aconst_null
    //   1406: astore 31
    //   1408: aload 11
    //   1410: aload 22
    //   1412: iconst_0
    //   1413: invokevirtual 932	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   1416: istore 21
    //   1418: iload 21
    //   1420: ifeq +9 -> 1429
    //   1423: iconst_4
    //   1424: istore 28
    //   1426: goto +295 -> 1721
    //   1429: ldc_w 952
    //   1432: astore 22
    //   1434: iconst_m1
    //   1435: istore 12
    //   1437: aload 11
    //   1439: aload 22
    //   1441: iload 12
    //   1443: invokevirtual 956	org/json/JSONObject:optInt	(Ljava/lang/String;I)I
    //   1446: istore 21
    //   1448: iload 21
    //   1450: istore 12
    //   1452: iload 21
    //   1454: iflt +15 -> 1469
    //   1457: aload 5
    //   1459: iload 21
    //   1461: invokevirtual 962	java/util/regex/Matcher:group	(I)Ljava/lang/String;
    //   1464: astore 31
    //   1466: goto +9 -> 1475
    //   1469: iconst_0
    //   1470: istore 12
    //   1472: aconst_null
    //   1473: astore 31
    //   1475: ldc_w 1216
    //   1478: astore 22
    //   1480: aload 11
    //   1482: aload 22
    //   1484: invokevirtual 1108	org/json/JSONObject:optJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   1487: astore 11
    //   1489: aload 11
    //   1491: ifnull +206 -> 1697
    //   1494: iconst_0
    //   1495: istore 21
    //   1497: aconst_null
    //   1498: astore 22
    //   1500: aconst_null
    //   1501: astore 40
    //   1503: aload 11
    //   1505: invokevirtual 1016	org/json/JSONArray:length	()I
    //   1508: istore 28
    //   1510: iload 21
    //   1512: iload 28
    //   1514: if_icmpge +177 -> 1691
    //   1517: aload 11
    //   1519: iload 21
    //   1521: invokevirtual 1020	org/json/JSONArray:getJSONObject	(I)Lorg/json/JSONObject;
    //   1524: astore 27
    //   1526: ldc_w 1214
    //   1529: astore 26
    //   1531: aload 11
    //   1533: astore 41
    //   1535: iconst_0
    //   1536: istore 13
    //   1538: aconst_null
    //   1539: astore 11
    //   1541: aload 27
    //   1543: aload 26
    //   1545: iconst_0
    //   1546: invokevirtual 932	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   1549: istore 25
    //   1551: aload 31
    //   1553: astore 11
    //   1555: iload 25
    //   1557: ifeq +13 -> 1570
    //   1560: aload 7
    //   1562: astore 40
    //   1564: iconst_4
    //   1565: istore 28
    //   1567: goto +136 -> 1703
    //   1570: ldc_w 1218
    //   1573: astore 26
    //   1575: aload 27
    //   1577: aload 26
    //   1579: invokevirtual 920	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   1582: astore 26
    //   1584: aload 31
    //   1586: aload 26
    //   1588: invokestatic 1221	com/daamitt/prime/sdk/a:b	(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;
    //   1591: astore 26
    //   1593: aload 26
    //   1595: invokevirtual 1226	java/util/Date:getYear	()I
    //   1598: istore 28
    //   1600: bipush 70
    //   1602: istore 12
    //   1604: iload 28
    //   1606: iload 12
    //   1608: if_icmpne +17 -> 1625
    //   1611: aload 4
    //   1613: invokevirtual 1226	java/util/Date:getYear	()I
    //   1616: istore 12
    //   1618: aload 26
    //   1620: iload 12
    //   1622: invokevirtual 1230	java/util/Date:setYear	(I)V
    //   1625: aload 10
    //   1627: getfield 1171	com/daamitt/prime/sdk/a/m:Y	I
    //   1630: istore 12
    //   1632: iconst_4
    //   1633: istore 28
    //   1635: iload 12
    //   1637: iload 28
    //   1639: ior
    //   1640: istore 12
    //   1642: aload 10
    //   1644: iload 12
    //   1646: putfield 1171	com/daamitt/prime/sdk/a/m:Y	I
    //   1649: aload 26
    //   1651: astore 40
    //   1653: goto +50 -> 1703
    //   1656: pop
    //   1657: iconst_4
    //   1658: istore 28
    //   1660: aload 26
    //   1662: astore 40
    //   1664: goto +7 -> 1671
    //   1667: pop
    //   1668: iconst_4
    //   1669: istore 28
    //   1671: invokestatic 1034	com/daamitt/prime/sdk/a/e:b	()V
    //   1674: iload 21
    //   1676: iconst_1
    //   1677: iadd
    //   1678: istore 21
    //   1680: aload 11
    //   1682: astore 31
    //   1684: aload 41
    //   1686: astore 11
    //   1688: goto -185 -> 1503
    //   1691: iconst_4
    //   1692: istore 28
    //   1694: goto +9 -> 1703
    //   1697: iconst_4
    //   1698: istore 28
    //   1700: aconst_null
    //   1701: astore 40
    //   1703: aload 40
    //   1705: ifnonnull +20 -> 1725
    //   1708: invokestatic 1034	com/daamitt/prime/sdk/a/e:b	()V
    //   1711: aload 7
    //   1713: astore 40
    //   1715: goto +10 -> 1725
    //   1718: iconst_4
    //   1719: istore 28
    //   1721: aload 7
    //   1723: astore 40
    //   1725: ldc_w 1232
    //   1728: astore 11
    //   1730: aload 15
    //   1732: aload 11
    //   1734: invokevirtual 926	org/json/JSONObject:optJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   1737: astore 11
    //   1739: aload 11
    //   1741: ifnull +176 -> 1917
    //   1744: ldc_w 952
    //   1747: astore 22
    //   1749: iconst_m1
    //   1750: istore 25
    //   1752: aload 11
    //   1754: aload 22
    //   1756: iload 25
    //   1758: invokevirtual 956	org/json/JSONObject:optInt	(Ljava/lang/String;I)I
    //   1761: istore 12
    //   1763: ldc_w 1234
    //   1766: astore 26
    //   1768: ldc_w 1185
    //   1771: astore 22
    //   1773: aload 11
    //   1775: aload 26
    //   1777: aload 22
    //   1779: invokevirtual 971	org/json/JSONObject:optString	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   1782: astore 11
    //   1784: iload 12
    //   1786: iflt +131 -> 1917
    //   1789: aload 11
    //   1791: invokevirtual 1163	java/lang/String:isEmpty	()Z
    //   1794: istore 21
    //   1796: iload 21
    //   1798: ifeq +57 -> 1855
    //   1801: aload 5
    //   1803: iload 12
    //   1805: invokevirtual 962	java/util/regex/Matcher:group	(I)Ljava/lang/String;
    //   1808: astore 11
    //   1810: aload 11
    //   1812: invokevirtual 1147	java/lang/String:trim	()Ljava/lang/String;
    //   1815: astore 11
    //   1817: aload 11
    //   1819: invokestatic 1129	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   1822: istore 13
    //   1824: iload 13
    //   1826: ifne +91 -> 1917
    //   1829: aload 5
    //   1831: iload 12
    //   1833: invokevirtual 962	java/util/regex/Matcher:group	(I)Ljava/lang/String;
    //   1836: astore 11
    //   1838: aload 11
    //   1840: invokevirtual 1147	java/lang/String:trim	()Ljava/lang/String;
    //   1843: astore 11
    //   1845: aload 10
    //   1847: aload 11
    //   1849: putfield 1237	com/daamitt/prime/sdk/a/m:V	Ljava/lang/String;
    //   1852: goto +65 -> 1917
    //   1855: new 1110	java/lang/StringBuilder
    //   1858: astore 22
    //   1860: aload 22
    //   1862: invokespecial 1111	java/lang/StringBuilder:<init>	()V
    //   1865: aload 22
    //   1867: aload 11
    //   1869: invokevirtual 1117	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1872: pop
    //   1873: ldc_w 1239
    //   1876: astore 11
    //   1878: aload 22
    //   1880: aload 11
    //   1882: invokevirtual 1117	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1885: pop
    //   1886: aload 5
    //   1888: iload 12
    //   1890: invokevirtual 962	java/util/regex/Matcher:group	(I)Ljava/lang/String;
    //   1893: astore 11
    //   1895: aload 22
    //   1897: aload 11
    //   1899: invokevirtual 1117	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1902: pop
    //   1903: aload 22
    //   1905: invokevirtual 1125	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1908: astore 11
    //   1910: aload 10
    //   1912: aload 11
    //   1914: putfield 1237	com/daamitt/prime/sdk/a/m:V	Ljava/lang/String;
    //   1917: ldc_w 1241
    //   1920: astore 11
    //   1922: aload 15
    //   1924: aload 11
    //   1926: invokevirtual 926	org/json/JSONObject:optJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   1929: astore 11
    //   1931: aload 11
    //   1933: ifnull +137 -> 2070
    //   1936: ldc_w 952
    //   1939: astore 22
    //   1941: iconst_m1
    //   1942: istore 25
    //   1944: aload 11
    //   1946: aload 22
    //   1948: iload 25
    //   1950: invokevirtual 956	org/json/JSONObject:optInt	(Ljava/lang/String;I)I
    //   1953: istore 13
    //   1955: iload 13
    //   1957: iflt +113 -> 2070
    //   1960: aload 5
    //   1962: iload 13
    //   1964: invokevirtual 962	java/util/regex/Matcher:group	(I)Ljava/lang/String;
    //   1967: astore 11
    //   1969: aload 11
    //   1971: invokevirtual 1147	java/lang/String:trim	()Ljava/lang/String;
    //   1974: astore 11
    //   1976: aload 11
    //   1978: invokevirtual 1244	java/lang/String:toUpperCase	()Ljava/lang/String;
    //   1981: astore 11
    //   1983: aload 10
    //   1985: aload 11
    //   1987: putfield 1245	com/daamitt/prime/sdk/a/m:e	Ljava/lang/String;
    //   1990: ldc_w 1247
    //   1993: astore 22
    //   1995: aload 11
    //   1997: aload 22
    //   1999: invokestatic 944	android/text/TextUtils:equals	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   2002: istore 21
    //   2004: iload 21
    //   2006: ifne +64 -> 2070
    //   2009: ldc_w 1249
    //   2012: astore 22
    //   2014: aload 11
    //   2016: aload 22
    //   2018: invokestatic 944	android/text/TextUtils:equals	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   2021: istore 13
    //   2023: iload 13
    //   2025: ifne +45 -> 2070
    //   2028: aload 10
    //   2030: getfield 1171	com/daamitt/prime/sdk/a/m:Y	I
    //   2033: sipush 1024
    //   2036: ior
    //   2037: istore 13
    //   2039: aload 10
    //   2041: iload 13
    //   2043: putfield 1171	com/daamitt/prime/sdk/a/m:Y	I
    //   2046: aload 23
    //   2048: invokevirtual 1203	java/lang/Double:doubleValue	()D
    //   2051: dstore 42
    //   2053: aload 10
    //   2055: dload 42
    //   2057: putfield 1252	com/daamitt/prime/sdk/a/m:h	D
    //   2060: dconst_0
    //   2061: dstore 42
    //   2063: aload 10
    //   2065: dload 42
    //   2067: putfield 1254	com/daamitt/prime/sdk/a/m:g	D
    //   2070: ldc_w 928
    //   2073: astore 11
    //   2075: iconst_0
    //   2076: istore 21
    //   2078: aconst_null
    //   2079: astore 22
    //   2081: aload 15
    //   2083: aload 11
    //   2085: iconst_0
    //   2086: invokevirtual 932	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   2089: istore 13
    //   2091: iload 13
    //   2093: ifeq +20 -> 2113
    //   2096: aload 10
    //   2098: getfield 1171	com/daamitt/prime/sdk/a/m:Y	I
    //   2101: bipush 16
    //   2103: ior
    //   2104: istore 13
    //   2106: aload 10
    //   2108: iload 13
    //   2110: putfield 1171	com/daamitt/prime/sdk/a/m:Y	I
    //   2113: ldc_w 934
    //   2116: astore 11
    //   2118: iconst_0
    //   2119: istore 21
    //   2121: aconst_null
    //   2122: astore 22
    //   2124: aload 15
    //   2126: aload 11
    //   2128: iconst_0
    //   2129: invokevirtual 932	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   2132: istore 13
    //   2134: iload 13
    //   2136: ifeq +21 -> 2157
    //   2139: aload 10
    //   2141: getfield 1171	com/daamitt/prime/sdk/a/m:Y	I
    //   2144: sipush 256
    //   2147: ior
    //   2148: istore 13
    //   2150: aload 10
    //   2152: iload 13
    //   2154: putfield 1171	com/daamitt/prime/sdk/a/m:Y	I
    //   2157: aload 20
    //   2159: invokestatic 1134	com/daamitt/prime/sdk/a:d	(Ljava/lang/String;)Ljava/lang/String;
    //   2162: astore 20
    //   2164: aload 14
    //   2166: invokestatic 1210	com/daamitt/prime/sdk/a/m:a	(Ljava/lang/String;)I
    //   2169: istore 25
    //   2171: aload 10
    //   2173: astore 11
    //   2175: aload 20
    //   2177: astore 14
    //   2179: aload 23
    //   2181: astore 20
    //   2183: aload 40
    //   2185: astore 22
    //   2187: aload 6
    //   2189: astore 23
    //   2191: iconst_4
    //   2192: istore 44
    //   2194: aload 10
    //   2196: aload 14
    //   2198: aload 20
    //   2200: aload 40
    //   2202: aload 6
    //   2204: iload 25
    //   2206: invokevirtual 1257	com/daamitt/prime/sdk/a/m:a	(Ljava/lang/String;Ljava/lang/Double;Ljava/util/Date;Ljava/lang/String;I)V
    //   2209: aload 10
    //   2211: aload 6
    //   2213: putfield 1259	com/daamitt/prime/sdk/a/m:m	Ljava/lang/String;
    //   2216: aload 17
    //   2218: invokevirtual 1203	java/lang/Double:doubleValue	()D
    //   2221: dstore 45
    //   2223: dload 45
    //   2225: dload 29
    //   2227: dcmpl
    //   2228: istore 19
    //   2230: iload 19
    //   2232: ifne +34 -> 2266
    //   2235: aload 9
    //   2237: invokevirtual 1203	java/lang/Double:doubleValue	()D
    //   2240: dstore 45
    //   2242: dload 45
    //   2244: dload 29
    //   2246: dcmpl
    //   2247: istore 19
    //   2249: iload 19
    //   2251: ifeq +6 -> 2257
    //   2254: goto +12 -> 2266
    //   2257: iconst_0
    //   2258: istore 13
    //   2260: aconst_null
    //   2261: astore 11
    //   2263: goto +93 -> 2356
    //   2266: new 1261	com/daamitt/prime/sdk/a/b
    //   2269: astore 11
    //   2271: aload 11
    //   2273: invokespecial 1262	com/daamitt/prime/sdk/a/b:<init>	()V
    //   2276: aload 17
    //   2278: invokevirtual 1203	java/lang/Double:doubleValue	()D
    //   2281: dstore 47
    //   2283: aload 11
    //   2285: dload 47
    //   2287: putfield 1264	com/daamitt/prime/sdk/a/b:a	D
    //   2290: aload 9
    //   2292: invokevirtual 1203	java/lang/Double:doubleValue	()D
    //   2295: dstore 47
    //   2297: aload 11
    //   2299: dload 47
    //   2301: putfield 1266	com/daamitt/prime/sdk/a/b:b	D
    //   2304: aload 17
    //   2306: invokevirtual 1203	java/lang/Double:doubleValue	()D
    //   2309: dstore 47
    //   2311: dload 47
    //   2313: dload 29
    //   2315: dcmpl
    //   2316: istore 21
    //   2318: iload 21
    //   2320: ifeq +10 -> 2330
    //   2323: aload 11
    //   2325: aload 7
    //   2327: putfield 1269	com/daamitt/prime/sdk/a/b:c	Ljava/util/Date;
    //   2330: aload 9
    //   2332: invokevirtual 1203	java/lang/Double:doubleValue	()D
    //   2335: dstore 47
    //   2337: dload 47
    //   2339: dload 29
    //   2341: dcmpl
    //   2342: istore 8
    //   2344: iload 8
    //   2346: ifeq +10 -> 2356
    //   2349: aload 11
    //   2351: aload 7
    //   2353: putfield 1271	com/daamitt/prime/sdk/a/b:d	Ljava/util/Date;
    //   2356: aload 10
    //   2358: aload 11
    //   2360: putfield 1274	com/daamitt/prime/sdk/a/m:i	Lcom/daamitt/prime/sdk/a/b;
    //   2363: aload 10
    //   2365: getfield 1276	com/daamitt/prime/sdk/a/m:j	I
    //   2368: istore 49
    //   2370: iconst_3
    //   2371: istore 8
    //   2373: iload 49
    //   2375: iload 8
    //   2377: if_icmpeq +21 -> 2398
    //   2380: aload 10
    //   2382: getfield 1276	com/daamitt/prime/sdk/a/m:j	I
    //   2385: istore 49
    //   2387: bipush 15
    //   2389: istore 13
    //   2391: iload 49
    //   2393: iload 13
    //   2395: if_icmpne +8 -> 2403
    //   2398: aload 10
    //   2400: invokevirtual 1103	com/daamitt/prime/sdk/a/m:j	()V
    //   2403: ldc_w 1278
    //   2406: astore 7
    //   2408: iconst_0
    //   2409: istore 13
    //   2411: aconst_null
    //   2412: astore 11
    //   2414: aload 15
    //   2416: aload 7
    //   2418: iconst_0
    //   2419: invokevirtual 932	org/json/JSONObject:optBoolean	(Ljava/lang/String;Z)Z
    //   2422: istore 49
    //   2424: aload 10
    //   2426: iload 49
    //   2428: putfield 1281	com/daamitt/prime/sdk/a/k:K	Z
    //   2431: iload 49
    //   2433: ifeq +197 -> 2630
    //   2436: ldc_w 1283
    //   2439: astore 7
    //   2441: aload 15
    //   2443: aload 7
    //   2445: invokevirtual 1140	org/json/JSONObject:getJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   2448: astore 7
    //   2450: aload 7
    //   2452: ifnull +178 -> 2630
    //   2455: new 1285	com/daamitt/prime/sdk/a/c
    //   2458: astore 11
    //   2460: aload 11
    //   2462: invokespecial 1286	com/daamitt/prime/sdk/a/c:<init>	()V
    //   2465: ldc_w 1288
    //   2468: astore 14
    //   2470: aload 7
    //   2472: aload 14
    //   2474: invokevirtual 1108	org/json/JSONObject:optJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   2477: astore 14
    //   2479: aload 14
    //   2481: ifnull +69 -> 2550
    //   2484: aload 14
    //   2486: invokevirtual 1016	org/json/JSONArray:length	()I
    //   2489: istore 19
    //   2491: iload 19
    //   2493: ifle +57 -> 2550
    //   2496: iconst_0
    //   2497: istore 19
    //   2499: aconst_null
    //   2500: astore 20
    //   2502: aload 14
    //   2504: invokevirtual 1016	org/json/JSONArray:length	()I
    //   2507: istore 21
    //   2509: iload 19
    //   2511: iload 21
    //   2513: if_icmpge +37 -> 2550
    //   2516: aload 14
    //   2518: iload 19
    //   2520: invokevirtual 1020	org/json/JSONArray:getJSONObject	(I)Lorg/json/JSONObject;
    //   2523: astore 22
    //   2525: aload 22
    //   2527: aload 5
    //   2529: invokestatic 1023	com/daamitt/prime/sdk/a:a	(Lorg/json/JSONObject;Ljava/util/regex/Matcher;)Lcom/daamitt/prime/sdk/a/c$a;
    //   2532: astore 22
    //   2534: aload 11
    //   2536: aload 22
    //   2538: invokevirtual 1291	com/daamitt/prime/sdk/a/c:a	(Lcom/daamitt/prime/sdk/a/c$a;)V
    //   2541: iload 19
    //   2543: iconst_1
    //   2544: iadd
    //   2545: istore 19
    //   2547: goto -45 -> 2502
    //   2550: ldc_w 1293
    //   2553: astore 5
    //   2555: aload 7
    //   2557: aload 5
    //   2559: invokevirtual 926	org/json/JSONObject:optJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   2562: astore 5
    //   2564: aload 5
    //   2566: ifnull +17 -> 2583
    //   2569: aload 5
    //   2571: invokestatic 1296	com/daamitt/prime/sdk/a:a	(Lorg/json/JSONObject;)Lcom/daamitt/prime/sdk/a/c$b;
    //   2574: astore 5
    //   2576: aload 11
    //   2578: aload 5
    //   2580: putfield 1299	com/daamitt/prime/sdk/a/c:b	Lcom/daamitt/prime/sdk/a/c$b;
    //   2583: ldc_w 1301
    //   2586: astore 5
    //   2588: aload 7
    //   2590: aload 5
    //   2592: invokevirtual 926	org/json/JSONObject:optJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   2595: astore 5
    //   2597: aload 5
    //   2599: ifnull +17 -> 2616
    //   2602: aload 5
    //   2604: invokestatic 1296	com/daamitt/prime/sdk/a:a	(Lorg/json/JSONObject;)Lcom/daamitt/prime/sdk/a/c$b;
    //   2607: astore 5
    //   2609: aload 11
    //   2611: aload 5
    //   2613: putfield 1303	com/daamitt/prime/sdk/a/c:c	Lcom/daamitt/prime/sdk/a/c$b;
    //   2616: aload 10
    //   2618: aload 11
    //   2620: putfield 1306	com/daamitt/prime/sdk/a/k:J	Lcom/daamitt/prime/sdk/a/c;
    //   2623: goto +7 -> 2630
    //   2626: pop
    //   2627: invokestatic 1034	com/daamitt/prime/sdk/a/e:b	()V
    //   2630: getstatic 179	com/daamitt/prime/sdk/a:i	Ljava/util/ArrayList;
    //   2633: astore 5
    //   2635: aload 6
    //   2637: aload 5
    //   2639: invokestatic 1309	com/daamitt/prime/sdk/a:a	(Ljava/lang/String;Ljava/util/ArrayList;)Z
    //   2642: istore 50
    //   2644: iload 50
    //   2646: ifeq +41 -> 2687
    //   2649: aload 10
    //   2651: getfield 1171	com/daamitt/prime/sdk/a/m:Y	I
    //   2654: istore 50
    //   2656: ldc_w 1310
    //   2659: istore 16
    //   2661: iload 50
    //   2663: iload 16
    //   2665: ior
    //   2666: istore 50
    //   2668: aload 10
    //   2670: iload 50
    //   2672: putfield 1171	com/daamitt/prime/sdk/a/m:Y	I
    //   2675: iconst_0
    //   2676: istore 49
    //   2678: aconst_null
    //   2679: astore 7
    //   2681: iconst_1
    //   2682: istore 12
    //   2684: goto +798 -> 3482
    //   2687: getstatic 375	com/daamitt/prime/sdk/a:k	Ljava/util/ArrayList;
    //   2690: astore 5
    //   2692: aload 6
    //   2694: aload 5
    //   2696: invokestatic 1309	com/daamitt/prime/sdk/a:a	(Ljava/lang/String;Ljava/util/ArrayList;)Z
    //   2699: istore 50
    //   2701: iload 50
    //   2703: ifeq +41 -> 2744
    //   2706: aload 10
    //   2708: getfield 1171	com/daamitt/prime/sdk/a/m:Y	I
    //   2711: istore 50
    //   2713: ldc_w 1312
    //   2716: istore 16
    //   2718: iload 50
    //   2720: iload 16
    //   2722: ior
    //   2723: istore 50
    //   2725: aload 10
    //   2727: iload 50
    //   2729: putfield 1171	com/daamitt/prime/sdk/a/m:Y	I
    //   2732: iconst_0
    //   2733: istore 49
    //   2735: aconst_null
    //   2736: astore 7
    //   2738: iconst_1
    //   2739: istore 12
    //   2741: goto +741 -> 3482
    //   2744: getstatic 655	com/daamitt/prime/sdk/a:l	Ljava/util/ArrayList;
    //   2747: astore 5
    //   2749: aload 6
    //   2751: aload 5
    //   2753: invokestatic 1309	com/daamitt/prime/sdk/a:a	(Ljava/lang/String;Ljava/util/ArrayList;)Z
    //   2756: istore 50
    //   2758: iload 50
    //   2760: ifeq +41 -> 2801
    //   2763: aload 10
    //   2765: getfield 1171	com/daamitt/prime/sdk/a/m:Y	I
    //   2768: istore 50
    //   2770: ldc_w 1314
    //   2773: istore 16
    //   2775: iload 50
    //   2777: iload 16
    //   2779: ior
    //   2780: istore 50
    //   2782: aload 10
    //   2784: iload 50
    //   2786: putfield 1171	com/daamitt/prime/sdk/a/m:Y	I
    //   2789: iconst_0
    //   2790: istore 49
    //   2792: aconst_null
    //   2793: astore 7
    //   2795: iconst_1
    //   2796: istore 12
    //   2798: goto +684 -> 3482
    //   2801: getstatic 219	com/daamitt/prime/sdk/a:j	Ljava/util/ArrayList;
    //   2804: astore 5
    //   2806: aload 6
    //   2808: aload 5
    //   2810: invokestatic 1309	com/daamitt/prime/sdk/a:a	(Ljava/lang/String;Ljava/util/ArrayList;)Z
    //   2813: istore 50
    //   2815: iload 50
    //   2817: ifeq +41 -> 2858
    //   2820: aload 10
    //   2822: getfield 1171	com/daamitt/prime/sdk/a/m:Y	I
    //   2825: istore 50
    //   2827: ldc_w 1316
    //   2830: istore 16
    //   2832: iload 50
    //   2834: iload 16
    //   2836: ior
    //   2837: istore 50
    //   2839: aload 10
    //   2841: iload 50
    //   2843: putfield 1171	com/daamitt/prime/sdk/a/m:Y	I
    //   2846: iconst_0
    //   2847: istore 49
    //   2849: aconst_null
    //   2850: astore 7
    //   2852: iconst_1
    //   2853: istore 12
    //   2855: goto +627 -> 3482
    //   2858: aload_3
    //   2859: invokevirtual 1150	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   2862: astore 5
    //   2864: getstatic 817	com/daamitt/prime/sdk/a:t	Ljava/util/ArrayList;
    //   2867: astore 6
    //   2869: iconst_0
    //   2870: istore 49
    //   2872: aconst_null
    //   2873: astore 7
    //   2875: aload 6
    //   2877: iconst_0
    //   2878: invokevirtual 1321	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   2881: astore 6
    //   2883: aload 6
    //   2885: checkcast 35	java/util/regex/Pattern
    //   2888: astore 6
    //   2890: aload 6
    //   2892: aload 5
    //   2894: invokevirtual 1325	java/util/regex/Pattern:matcher	(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
    //   2897: astore 6
    //   2899: aload 6
    //   2901: invokevirtual 1328	java/util/regex/Matcher:matches	()Z
    //   2904: istore 16
    //   2906: iload 16
    //   2908: ifeq +528 -> 3436
    //   2911: getstatic 817	com/daamitt/prime/sdk/a:t	Ljava/util/ArrayList;
    //   2914: astore 6
    //   2916: iconst_1
    //   2917: istore 49
    //   2919: aload 6
    //   2921: iload 49
    //   2923: invokevirtual 1321	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   2926: astore 6
    //   2928: aload 6
    //   2930: checkcast 35	java/util/regex/Pattern
    //   2933: astore 6
    //   2935: aload 6
    //   2937: aload 5
    //   2939: invokevirtual 1325	java/util/regex/Pattern:matcher	(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
    //   2942: astore 6
    //   2944: aload 6
    //   2946: invokevirtual 1328	java/util/regex/Matcher:matches	()Z
    //   2949: istore 16
    //   2951: iload 16
    //   2953: ifeq +471 -> 3424
    //   2956: getstatic 839	com/daamitt/prime/sdk/a:u	Ljava/util/ArrayList;
    //   2959: astore 6
    //   2961: iconst_0
    //   2962: istore 49
    //   2964: aconst_null
    //   2965: astore 7
    //   2967: aload 6
    //   2969: iconst_0
    //   2970: invokevirtual 1321	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   2973: astore 6
    //   2975: aload 6
    //   2977: checkcast 35	java/util/regex/Pattern
    //   2980: astore 6
    //   2982: aload 6
    //   2984: aload 5
    //   2986: invokevirtual 1325	java/util/regex/Pattern:matcher	(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
    //   2989: astore 6
    //   2991: aload 6
    //   2993: invokevirtual 1328	java/util/regex/Matcher:matches	()Z
    //   2996: istore 16
    //   2998: iload 16
    //   3000: ifne +412 -> 3412
    //   3003: getstatic 839	com/daamitt/prime/sdk/a:u	Ljava/util/ArrayList;
    //   3006: astore 6
    //   3008: iconst_1
    //   3009: istore 12
    //   3011: aload 6
    //   3013: iload 12
    //   3015: invokevirtual 1321	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   3018: astore 6
    //   3020: aload 6
    //   3022: checkcast 35	java/util/regex/Pattern
    //   3025: astore 6
    //   3027: aload 6
    //   3029: aload 5
    //   3031: invokevirtual 1325	java/util/regex/Pattern:matcher	(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
    //   3034: astore 6
    //   3036: aload 6
    //   3038: invokevirtual 1328	java/util/regex/Matcher:matches	()Z
    //   3041: istore 16
    //   3043: iload 16
    //   3045: ifne +370 -> 3415
    //   3048: getstatic 839	com/daamitt/prime/sdk/a:u	Ljava/util/ArrayList;
    //   3051: astore 6
    //   3053: iconst_2
    //   3054: istore 13
    //   3056: aload 6
    //   3058: iload 13
    //   3060: invokevirtual 1321	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   3063: astore 6
    //   3065: aload 6
    //   3067: checkcast 35	java/util/regex/Pattern
    //   3070: astore 6
    //   3072: aload 6
    //   3074: aload 5
    //   3076: invokevirtual 1325	java/util/regex/Pattern:matcher	(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
    //   3079: astore 6
    //   3081: aload 6
    //   3083: invokevirtual 1328	java/util/regex/Matcher:matches	()Z
    //   3086: istore 16
    //   3088: iload 16
    //   3090: ifne +325 -> 3415
    //   3093: getstatic 839	com/daamitt/prime/sdk/a:u	Ljava/util/ArrayList;
    //   3096: astore 6
    //   3098: aload 6
    //   3100: iload 8
    //   3102: invokevirtual 1321	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   3105: astore 6
    //   3107: aload 6
    //   3109: checkcast 35	java/util/regex/Pattern
    //   3112: astore 6
    //   3114: aload 6
    //   3116: aload 5
    //   3118: invokevirtual 1325	java/util/regex/Pattern:matcher	(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
    //   3121: astore 6
    //   3123: aload 6
    //   3125: invokevirtual 1328	java/util/regex/Matcher:matches	()Z
    //   3128: istore 16
    //   3130: iload 16
    //   3132: ifne +283 -> 3415
    //   3135: getstatic 839	com/daamitt/prime/sdk/a:u	Ljava/util/ArrayList;
    //   3138: astore 6
    //   3140: aload 6
    //   3142: iload 44
    //   3144: invokevirtual 1321	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   3147: astore 6
    //   3149: aload 6
    //   3151: checkcast 35	java/util/regex/Pattern
    //   3154: astore 6
    //   3156: aload 6
    //   3158: aload 5
    //   3160: invokevirtual 1325	java/util/regex/Pattern:matcher	(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
    //   3163: astore 6
    //   3165: aload 6
    //   3167: invokevirtual 1328	java/util/regex/Matcher:matches	()Z
    //   3170: istore 16
    //   3172: iload 16
    //   3174: ifne +241 -> 3415
    //   3177: getstatic 839	com/daamitt/prime/sdk/a:u	Ljava/util/ArrayList;
    //   3180: astore 6
    //   3182: iconst_5
    //   3183: istore 8
    //   3185: aload 6
    //   3187: iload 8
    //   3189: invokevirtual 1321	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   3192: astore 6
    //   3194: aload 6
    //   3196: checkcast 35	java/util/regex/Pattern
    //   3199: astore 6
    //   3201: aload 6
    //   3203: aload 5
    //   3205: invokevirtual 1325	java/util/regex/Pattern:matcher	(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
    //   3208: astore 6
    //   3210: aload 6
    //   3212: invokevirtual 1328	java/util/regex/Matcher:matches	()Z
    //   3215: istore 16
    //   3217: iload 16
    //   3219: ifne +196 -> 3415
    //   3222: getstatic 839	com/daamitt/prime/sdk/a:u	Ljava/util/ArrayList;
    //   3225: astore 6
    //   3227: bipush 6
    //   3229: istore 8
    //   3231: aload 6
    //   3233: iload 8
    //   3235: invokevirtual 1321	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   3238: astore 6
    //   3240: aload 6
    //   3242: checkcast 35	java/util/regex/Pattern
    //   3245: astore 6
    //   3247: aload 6
    //   3249: aload 5
    //   3251: invokevirtual 1325	java/util/regex/Pattern:matcher	(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
    //   3254: astore 6
    //   3256: aload 6
    //   3258: invokevirtual 1328	java/util/regex/Matcher:matches	()Z
    //   3261: istore 16
    //   3263: iload 16
    //   3265: ifne +150 -> 3415
    //   3268: getstatic 839	com/daamitt/prime/sdk/a:u	Ljava/util/ArrayList;
    //   3271: astore 6
    //   3273: bipush 7
    //   3275: istore 8
    //   3277: aload 6
    //   3279: iload 8
    //   3281: invokevirtual 1321	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   3284: astore 6
    //   3286: aload 6
    //   3288: checkcast 35	java/util/regex/Pattern
    //   3291: astore 6
    //   3293: aload 6
    //   3295: aload 5
    //   3297: invokevirtual 1325	java/util/regex/Pattern:matcher	(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
    //   3300: astore 6
    //   3302: aload 6
    //   3304: invokevirtual 1328	java/util/regex/Matcher:matches	()Z
    //   3307: istore 16
    //   3309: iload 16
    //   3311: ifne +104 -> 3415
    //   3314: getstatic 839	com/daamitt/prime/sdk/a:u	Ljava/util/ArrayList;
    //   3317: astore 6
    //   3319: bipush 8
    //   3321: istore 8
    //   3323: aload 6
    //   3325: iload 8
    //   3327: invokevirtual 1321	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   3330: astore 6
    //   3332: aload 6
    //   3334: checkcast 35	java/util/regex/Pattern
    //   3337: astore 6
    //   3339: aload 6
    //   3341: aload 5
    //   3343: invokevirtual 1325	java/util/regex/Pattern:matcher	(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
    //   3346: astore 6
    //   3348: aload 6
    //   3350: invokevirtual 1328	java/util/regex/Matcher:matches	()Z
    //   3353: istore 16
    //   3355: iload 16
    //   3357: ifne +58 -> 3415
    //   3360: getstatic 839	com/daamitt/prime/sdk/a:u	Ljava/util/ArrayList;
    //   3363: astore 6
    //   3365: bipush 9
    //   3367: istore 8
    //   3369: aload 6
    //   3371: iload 8
    //   3373: invokevirtual 1321	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   3376: astore 6
    //   3378: aload 6
    //   3380: checkcast 35	java/util/regex/Pattern
    //   3383: astore 6
    //   3385: aload 6
    //   3387: aload 5
    //   3389: invokevirtual 1325	java/util/regex/Pattern:matcher	(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
    //   3392: astore 5
    //   3394: aload 5
    //   3396: invokevirtual 1328	java/util/regex/Matcher:matches	()Z
    //   3399: istore 50
    //   3401: iload 50
    //   3403: ifne +12 -> 3415
    //   3406: iconst_1
    //   3407: istore 50
    //   3409: goto +42 -> 3451
    //   3412: iconst_1
    //   3413: istore 12
    //   3415: iconst_0
    //   3416: istore 50
    //   3418: aconst_null
    //   3419: astore 5
    //   3421: goto +30 -> 3451
    //   3424: iconst_0
    //   3425: istore 49
    //   3427: aconst_null
    //   3428: astore 7
    //   3430: iconst_1
    //   3431: istore 12
    //   3433: goto +12 -> 3445
    //   3436: iconst_0
    //   3437: istore 49
    //   3439: aconst_null
    //   3440: astore 7
    //   3442: iconst_1
    //   3443: istore 12
    //   3445: iconst_0
    //   3446: istore 50
    //   3448: aconst_null
    //   3449: astore 5
    //   3451: iload 50
    //   3453: ifeq +29 -> 3482
    //   3456: aload 10
    //   3458: getfield 1171	com/daamitt/prime/sdk/a/m:Y	I
    //   3461: istore 50
    //   3463: ldc_w 1329
    //   3466: istore 16
    //   3468: iload 50
    //   3470: iload 16
    //   3472: ior
    //   3473: istore 50
    //   3475: aload 10
    //   3477: iload 50
    //   3479: putfield 1171	com/daamitt/prime/sdk/a/m:Y	I
    //   3482: aload 10
    //   3484: getfield 1276	com/daamitt/prime/sdk/a/m:j	I
    //   3487: istore 50
    //   3489: bipush 17
    //   3491: istore 16
    //   3493: iload 50
    //   3495: iload 16
    //   3497: if_icmpne +171 -> 3668
    //   3500: getstatic 45	com/daamitt/prime/sdk/a:c	Ljava/util/regex/Pattern;
    //   3503: astore 5
    //   3505: aload_3
    //   3506: invokevirtual 1150	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   3509: astore 6
    //   3511: aload 5
    //   3513: aload 6
    //   3515: invokevirtual 1325	java/util/regex/Pattern:matcher	(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
    //   3518: astore 5
    //   3520: aload 5
    //   3522: invokevirtual 1328	java/util/regex/Matcher:matches	()Z
    //   3525: istore 50
    //   3527: iload 50
    //   3529: ifne +35 -> 3564
    //   3532: getstatic 49	com/daamitt/prime/sdk/a:d	Ljava/util/regex/Pattern;
    //   3535: astore 5
    //   3537: aload_3
    //   3538: invokevirtual 1150	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   3541: astore 6
    //   3543: aload 5
    //   3545: aload 6
    //   3547: invokevirtual 1325	java/util/regex/Pattern:matcher	(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
    //   3550: astore 5
    //   3552: aload 5
    //   3554: invokevirtual 1328	java/util/regex/Matcher:matches	()Z
    //   3557: istore 50
    //   3559: iload 50
    //   3561: ifeq +70 -> 3631
    //   3564: getstatic 53	com/daamitt/prime/sdk/a:e	Ljava/util/regex/Pattern;
    //   3567: astore 5
    //   3569: aload_3
    //   3570: invokevirtual 1150	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   3573: astore 6
    //   3575: aload 5
    //   3577: aload 6
    //   3579: invokevirtual 1325	java/util/regex/Pattern:matcher	(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
    //   3582: astore 5
    //   3584: aload 5
    //   3586: invokevirtual 1328	java/util/regex/Matcher:matches	()Z
    //   3589: istore 50
    //   3591: iload 50
    //   3593: ifne +44 -> 3637
    //   3596: getstatic 57	com/daamitt/prime/sdk/a:f	Ljava/util/regex/Pattern;
    //   3599: astore 5
    //   3601: aload_3
    //   3602: invokevirtual 1150	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   3605: astore 6
    //   3607: aload 5
    //   3609: aload 6
    //   3611: invokevirtual 1325	java/util/regex/Pattern:matcher	(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
    //   3614: astore 5
    //   3616: aload 5
    //   3618: invokevirtual 1328	java/util/regex/Matcher:matches	()Z
    //   3621: istore 50
    //   3623: iload 50
    //   3625: ifeq +6 -> 3631
    //   3628: goto +9 -> 3637
    //   3631: iconst_0
    //   3632: istore 12
    //   3634: aconst_null
    //   3635: astore 31
    //   3637: iload 12
    //   3639: ifeq +29 -> 3668
    //   3642: aload 10
    //   3644: getfield 1171	com/daamitt/prime/sdk/a/m:Y	I
    //   3647: istore 50
    //   3649: ldc_w 1331
    //   3652: istore 16
    //   3654: iload 50
    //   3656: iload 16
    //   3658: ior
    //   3659: istore 50
    //   3661: aload 10
    //   3663: iload 50
    //   3665: putfield 1171	com/daamitt/prime/sdk/a/m:Y	I
    //   3668: aload 10
    //   3670: areturn
    //   3671: pop
    //   3672: iconst_0
    //   3673: istore 16
    //   3675: aconst_null
    //   3676: astore 6
    //   3678: goto +23 -> 3701
    //   3681: astore 5
    //   3683: iconst_0
    //   3684: istore 16
    //   3686: aconst_null
    //   3687: astore 6
    //   3689: goto +22 -> 3711
    //   3692: aconst_null
    //   3693: areturn
    //   3694: pop
    //   3695: iconst_0
    //   3696: istore 16
    //   3698: aconst_null
    //   3699: astore 6
    //   3701: aconst_null
    //   3702: areturn
    //   3703: astore 5
    //   3705: iconst_0
    //   3706: istore 16
    //   3708: aconst_null
    //   3709: astore 6
    //   3711: aload 5
    //   3713: invokevirtual 1335	org/json/JSONException:printStackTrace	()V
    //   3716: aconst_null
    //   3717: areturn
    //   3718: pop
    //   3719: goto -2059 -> 1660
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	3722	0	paramMatcher	Matcher
    //   0	3722	1	paramh	h
    //   0	3722	2	paramString1	String
    //   0	3722	3	paramString2	String
    //   0	3722	4	paramDate	Date
    //   1	3616	5	localObject1	Object
    //   3681	1	5	localJSONException1	JSONException
    //   3703	9	5	localJSONException2	JSONException
    //   4	3706	6	localObject2	Object
    //   8	3433	7	localObject3	Object
    //   11	2334	8	bool1	boolean
    //   2371	1001	8	i1	int
    //   14	2317	9	localObject4	Object
    //   19	3650	10	localm	com.daamitt.prime.sdk.a.m
    //   22	2597	11	localObject5	Object
    //   34	1370	12	bool2	boolean
    //   1435	2203	12	i2	int
    //   54	5	13	i3	int
    //   114	24	13	bool3	boolean
    //   199	848	13	i4	int
    //   1325	3	13	bool4	boolean
    //   1359	178	13	i5	int
    //   1822	3	13	bool5	boolean
    //   1953	10	13	i6	int
    //   2021	3	13	bool6	boolean
    //   2037	5	13	i7	int
    //   2089	3	13	bool7	boolean
    //   2104	5	13	i8	int
    //   2132	3	13	bool8	boolean
    //   2148	911	13	i9	int
    //   72	2445	14	localObject6	Object
    //   100	2342	15	localJSONObject	JSONObject
    //   175	252	16	bool9	boolean
    //   2659	178	16	i10	int
    //   2904	452	16	bool10	boolean
    //   3466	241	16	i11	int
    //   202	2103	17	localObject7	Object
    //   222	3	18	bool11	boolean
    //   248	10	18	i12	int
    //   412	3	18	bool12	boolean
    //   297	444	19	i13	int
    //   2228	22	19	bool13	boolean
    //   2489	57	19	i14	int
    //   300	2201	20	localObject8	Object
    //   307	200	21	i15	int
    //   639	60	21	bool14	boolean
    //   734	103	21	i16	int
    //   963	168	21	bool15	boolean
    //   1222	148	21	i17	int
    //   1416	3	21	bool16	boolean
    //   1446	233	21	i18	int
    //   1794	525	21	bool17	boolean
    //   2507	7	21	i19	int
    //   324	2213	22	localObject9	Object
    //   530	1660	23	localObject10	Object
    //   542	178	24	i20	int
    //   552	509	25	i21	int
    //   1549	7	25	bool18	boolean
    //   1750	455	25	i22	int
    //   568	1208	26	localObject11	Object
    //   573	1003	27	localObject12	Object
    //   591	3	28	bool19	boolean
    //   784	936	28	i23	int
    //   858	1482	29	d1	double
    //   910	2726	31	localObject13	Object
    //   976	3	32	d2	double
    //   1141	3	34	d3	double
    //   1191	149	36	d4	double
    //   1342	3	38	d5	double
    //   1501	700	40	localObject14	Object
    //   1533	152	41	localObject15	Object
    //   2051	15	42	d6	double
    //   2192	951	44	i24	int
    //   2221	22	45	d7	double
    //   2281	57	47	d8	double
    //   2368	28	49	i25	int
    //   2422	500	49	i26	int
    //   2962	476	49	i27	int
    //   2642	3	50	bool20	boolean
    //   2654	17	50	i28	int
    //   2699	3	50	bool21	boolean
    //   2711	17	50	i29	int
    //   2756	3	50	bool22	boolean
    //   2768	17	50	i30	int
    //   2813	3	50	bool23	boolean
    //   2825	17	50	i31	int
    //   3399	53	50	bool24	boolean
    //   3461	37	50	i32	int
    //   3525	99	50	bool25	boolean
    //   3647	17	50	i33	int
    //   996	1	90	localNumberFormatException1	NumberFormatException
    //   1161	1	91	localNumberFormatException2	NumberFormatException
    //   1277	1	92	localNumberFormatException3	NumberFormatException
    //   1656	1	93	localParseException1	ParseException
    //   1667	1	94	localParseException2	ParseException
    //   2626	1	95	localJSONException3	JSONException
    //   3671	1	96	localNumberFormatException4	NumberFormatException
    //   3694	1	97	localNumberFormatException5	NumberFormatException
    //   3718	1	98	localParseException3	ParseException
    // Exception table:
    //   from	to	target	type
    //   895	900	996	java/lang/NumberFormatException
    //   916	921	996	java/lang/NumberFormatException
    //   923	928	996	java/lang/NumberFormatException
    //   944	949	996	java/lang/NumberFormatException
    //   958	963	996	java/lang/NumberFormatException
    //   970	975	996	java/lang/NumberFormatException
    //   978	983	996	java/lang/NumberFormatException
    //   1060	1065	1161	java/lang/NumberFormatException
    //   1081	1086	1161	java/lang/NumberFormatException
    //   1088	1093	1161	java/lang/NumberFormatException
    //   1109	1114	1161	java/lang/NumberFormatException
    //   1123	1128	1161	java/lang/NumberFormatException
    //   1135	1140	1161	java/lang/NumberFormatException
    //   1143	1148	1161	java/lang/NumberFormatException
    //   1235	1240	1277	java/lang/NumberFormatException
    //   1256	1261	1277	java/lang/NumberFormatException
    //   1263	1268	1277	java/lang/NumberFormatException
    //   1593	1598	1656	java/text/ParseException
    //   1611	1616	1656	java/text/ParseException
    //   1620	1625	1656	java/text/ParseException
    //   1625	1630	1656	java/text/ParseException
    //   1586	1591	1667	java/text/ParseException
    //   2443	2448	2626	org/json/JSONException
    //   2455	2458	2626	org/json/JSONException
    //   2460	2465	2626	org/json/JSONException
    //   2472	2477	2626	org/json/JSONException
    //   2484	2489	2626	org/json/JSONException
    //   2502	2507	2626	org/json/JSONException
    //   2518	2523	2626	org/json/JSONException
    //   2527	2532	2626	org/json/JSONException
    //   2536	2541	2626	org/json/JSONException
    //   2557	2562	2626	org/json/JSONException
    //   2569	2574	2626	org/json/JSONException
    //   2578	2583	2626	org/json/JSONException
    //   2590	2595	2626	org/json/JSONException
    //   2602	2607	2626	org/json/JSONException
    //   2611	2616	2626	org/json/JSONException
    //   2618	2623	2626	org/json/JSONException
    //   1162	1165	3671	java/lang/NumberFormatException
    //   1183	1188	3671	java/lang/NumberFormatException
    //   1193	1198	3671	java/lang/NumberFormatException
    //   1217	1222	3671	java/lang/NumberFormatException
    //   1278	1283	3671	java/lang/NumberFormatException
    //   1285	1288	3671	java/lang/NumberFormatException
    //   1306	1311	3671	java/lang/NumberFormatException
    //   1320	1325	3671	java/lang/NumberFormatException
    //   1332	1337	3671	java/lang/NumberFormatException
    //   1344	1349	3671	java/lang/NumberFormatException
    //   1354	1359	3671	java/lang/NumberFormatException
    //   1385	1390	3671	java/lang/NumberFormatException
    //   1412	1416	3671	java/lang/NumberFormatException
    //   1441	1446	3671	java/lang/NumberFormatException
    //   1459	1464	3671	java/lang/NumberFormatException
    //   1482	1487	3671	java/lang/NumberFormatException
    //   1503	1508	3671	java/lang/NumberFormatException
    //   1519	1524	3671	java/lang/NumberFormatException
    //   1545	1549	3671	java/lang/NumberFormatException
    //   1577	1582	3671	java/lang/NumberFormatException
    //   1586	1591	3671	java/lang/NumberFormatException
    //   1593	1598	3671	java/lang/NumberFormatException
    //   1611	1616	3671	java/lang/NumberFormatException
    //   1620	1625	3671	java/lang/NumberFormatException
    //   1625	1630	3671	java/lang/NumberFormatException
    //   1644	1649	3671	java/lang/NumberFormatException
    //   1671	1674	3671	java/lang/NumberFormatException
    //   1708	1711	3671	java/lang/NumberFormatException
    //   1732	1737	3671	java/lang/NumberFormatException
    //   1756	1761	3671	java/lang/NumberFormatException
    //   1777	1782	3671	java/lang/NumberFormatException
    //   1789	1794	3671	java/lang/NumberFormatException
    //   1803	1808	3671	java/lang/NumberFormatException
    //   1810	1815	3671	java/lang/NumberFormatException
    //   1817	1822	3671	java/lang/NumberFormatException
    //   1831	1836	3671	java/lang/NumberFormatException
    //   1838	1843	3671	java/lang/NumberFormatException
    //   1847	1852	3671	java/lang/NumberFormatException
    //   1855	1858	3671	java/lang/NumberFormatException
    //   1860	1865	3671	java/lang/NumberFormatException
    //   1867	1873	3671	java/lang/NumberFormatException
    //   1880	1886	3671	java/lang/NumberFormatException
    //   1888	1893	3671	java/lang/NumberFormatException
    //   1897	1903	3671	java/lang/NumberFormatException
    //   1903	1908	3671	java/lang/NumberFormatException
    //   1912	1917	3671	java/lang/NumberFormatException
    //   1924	1929	3671	java/lang/NumberFormatException
    //   1948	1953	3671	java/lang/NumberFormatException
    //   1962	1967	3671	java/lang/NumberFormatException
    //   1969	1974	3671	java/lang/NumberFormatException
    //   1976	1981	3671	java/lang/NumberFormatException
    //   1985	1990	3671	java/lang/NumberFormatException
    //   1997	2002	3671	java/lang/NumberFormatException
    //   2016	2021	3671	java/lang/NumberFormatException
    //   2028	2033	3671	java/lang/NumberFormatException
    //   2041	2046	3671	java/lang/NumberFormatException
    //   2046	2051	3671	java/lang/NumberFormatException
    //   2055	2060	3671	java/lang/NumberFormatException
    //   2065	2070	3671	java/lang/NumberFormatException
    //   2085	2089	3671	java/lang/NumberFormatException
    //   2096	2101	3671	java/lang/NumberFormatException
    //   2108	2113	3671	java/lang/NumberFormatException
    //   2128	2132	3671	java/lang/NumberFormatException
    //   2139	2144	3671	java/lang/NumberFormatException
    //   2152	2157	3671	java/lang/NumberFormatException
    //   2157	2162	3671	java/lang/NumberFormatException
    //   2164	2169	3671	java/lang/NumberFormatException
    //   2204	2209	3671	java/lang/NumberFormatException
    //   2211	2216	3671	java/lang/NumberFormatException
    //   2216	2221	3671	java/lang/NumberFormatException
    //   2235	2240	3671	java/lang/NumberFormatException
    //   2266	2269	3671	java/lang/NumberFormatException
    //   2271	2276	3671	java/lang/NumberFormatException
    //   2276	2281	3671	java/lang/NumberFormatException
    //   2285	2290	3671	java/lang/NumberFormatException
    //   2290	2295	3671	java/lang/NumberFormatException
    //   2299	2304	3671	java/lang/NumberFormatException
    //   2304	2309	3671	java/lang/NumberFormatException
    //   2325	2330	3671	java/lang/NumberFormatException
    //   2330	2335	3671	java/lang/NumberFormatException
    //   2351	2356	3671	java/lang/NumberFormatException
    //   2358	2363	3671	java/lang/NumberFormatException
    //   2363	2368	3671	java/lang/NumberFormatException
    //   2380	2385	3671	java/lang/NumberFormatException
    //   2398	2403	3671	java/lang/NumberFormatException
    //   2418	2422	3671	java/lang/NumberFormatException
    //   2426	2431	3671	java/lang/NumberFormatException
    //   2443	2448	3671	java/lang/NumberFormatException
    //   2455	2458	3671	java/lang/NumberFormatException
    //   2460	2465	3671	java/lang/NumberFormatException
    //   2472	2477	3671	java/lang/NumberFormatException
    //   2484	2489	3671	java/lang/NumberFormatException
    //   2502	2507	3671	java/lang/NumberFormatException
    //   2518	2523	3671	java/lang/NumberFormatException
    //   2527	2532	3671	java/lang/NumberFormatException
    //   2536	2541	3671	java/lang/NumberFormatException
    //   2557	2562	3671	java/lang/NumberFormatException
    //   2569	2574	3671	java/lang/NumberFormatException
    //   2578	2583	3671	java/lang/NumberFormatException
    //   2590	2595	3671	java/lang/NumberFormatException
    //   2602	2607	3671	java/lang/NumberFormatException
    //   2611	2616	3671	java/lang/NumberFormatException
    //   2618	2623	3671	java/lang/NumberFormatException
    //   2627	2630	3671	java/lang/NumberFormatException
    //   2630	2633	3671	java/lang/NumberFormatException
    //   2637	2642	3671	java/lang/NumberFormatException
    //   2649	2654	3671	java/lang/NumberFormatException
    //   2670	2675	3671	java/lang/NumberFormatException
    //   2687	2690	3671	java/lang/NumberFormatException
    //   2694	2699	3671	java/lang/NumberFormatException
    //   2706	2711	3671	java/lang/NumberFormatException
    //   2727	2732	3671	java/lang/NumberFormatException
    //   2744	2747	3671	java/lang/NumberFormatException
    //   2751	2756	3671	java/lang/NumberFormatException
    //   2763	2768	3671	java/lang/NumberFormatException
    //   2784	2789	3671	java/lang/NumberFormatException
    //   2801	2804	3671	java/lang/NumberFormatException
    //   2808	2813	3671	java/lang/NumberFormatException
    //   2820	2825	3671	java/lang/NumberFormatException
    //   2841	2846	3671	java/lang/NumberFormatException
    //   2858	2862	3671	java/lang/NumberFormatException
    //   2864	2867	3671	java/lang/NumberFormatException
    //   2877	2881	3671	java/lang/NumberFormatException
    //   2883	2888	3671	java/lang/NumberFormatException
    //   2892	2897	3671	java/lang/NumberFormatException
    //   2899	2904	3671	java/lang/NumberFormatException
    //   2911	2914	3671	java/lang/NumberFormatException
    //   2921	2926	3671	java/lang/NumberFormatException
    //   2928	2933	3671	java/lang/NumberFormatException
    //   2937	2942	3671	java/lang/NumberFormatException
    //   2944	2949	3671	java/lang/NumberFormatException
    //   2956	2959	3671	java/lang/NumberFormatException
    //   2969	2973	3671	java/lang/NumberFormatException
    //   2975	2980	3671	java/lang/NumberFormatException
    //   2984	2989	3671	java/lang/NumberFormatException
    //   2991	2996	3671	java/lang/NumberFormatException
    //   3003	3006	3671	java/lang/NumberFormatException
    //   3013	3018	3671	java/lang/NumberFormatException
    //   3020	3025	3671	java/lang/NumberFormatException
    //   3029	3034	3671	java/lang/NumberFormatException
    //   3036	3041	3671	java/lang/NumberFormatException
    //   3048	3051	3671	java/lang/NumberFormatException
    //   3058	3063	3671	java/lang/NumberFormatException
    //   3065	3070	3671	java/lang/NumberFormatException
    //   3074	3079	3671	java/lang/NumberFormatException
    //   3081	3086	3671	java/lang/NumberFormatException
    //   3093	3096	3671	java/lang/NumberFormatException
    //   3100	3105	3671	java/lang/NumberFormatException
    //   3107	3112	3671	java/lang/NumberFormatException
    //   3116	3121	3671	java/lang/NumberFormatException
    //   3123	3128	3671	java/lang/NumberFormatException
    //   3135	3138	3671	java/lang/NumberFormatException
    //   3142	3147	3671	java/lang/NumberFormatException
    //   3149	3154	3671	java/lang/NumberFormatException
    //   3158	3163	3671	java/lang/NumberFormatException
    //   3165	3170	3671	java/lang/NumberFormatException
    //   3177	3180	3671	java/lang/NumberFormatException
    //   3187	3192	3671	java/lang/NumberFormatException
    //   3194	3199	3671	java/lang/NumberFormatException
    //   3203	3208	3671	java/lang/NumberFormatException
    //   3210	3215	3671	java/lang/NumberFormatException
    //   3222	3225	3671	java/lang/NumberFormatException
    //   3233	3238	3671	java/lang/NumberFormatException
    //   3240	3245	3671	java/lang/NumberFormatException
    //   3249	3254	3671	java/lang/NumberFormatException
    //   3256	3261	3671	java/lang/NumberFormatException
    //   3268	3271	3671	java/lang/NumberFormatException
    //   3279	3284	3671	java/lang/NumberFormatException
    //   3286	3291	3671	java/lang/NumberFormatException
    //   3295	3300	3671	java/lang/NumberFormatException
    //   3302	3307	3671	java/lang/NumberFormatException
    //   3314	3317	3671	java/lang/NumberFormatException
    //   3325	3330	3671	java/lang/NumberFormatException
    //   3332	3337	3671	java/lang/NumberFormatException
    //   3341	3346	3671	java/lang/NumberFormatException
    //   3348	3353	3671	java/lang/NumberFormatException
    //   3360	3363	3671	java/lang/NumberFormatException
    //   3371	3376	3671	java/lang/NumberFormatException
    //   3378	3383	3671	java/lang/NumberFormatException
    //   3387	3392	3671	java/lang/NumberFormatException
    //   3394	3399	3671	java/lang/NumberFormatException
    //   3456	3461	3671	java/lang/NumberFormatException
    //   3477	3482	3671	java/lang/NumberFormatException
    //   3482	3487	3671	java/lang/NumberFormatException
    //   3500	3503	3671	java/lang/NumberFormatException
    //   3505	3509	3671	java/lang/NumberFormatException
    //   3513	3518	3671	java/lang/NumberFormatException
    //   3520	3525	3671	java/lang/NumberFormatException
    //   3532	3535	3671	java/lang/NumberFormatException
    //   3537	3541	3671	java/lang/NumberFormatException
    //   3545	3550	3671	java/lang/NumberFormatException
    //   3552	3557	3671	java/lang/NumberFormatException
    //   3564	3567	3671	java/lang/NumberFormatException
    //   3569	3573	3671	java/lang/NumberFormatException
    //   3577	3582	3671	java/lang/NumberFormatException
    //   3584	3589	3671	java/lang/NumberFormatException
    //   3596	3599	3671	java/lang/NumberFormatException
    //   3601	3605	3671	java/lang/NumberFormatException
    //   3609	3614	3671	java/lang/NumberFormatException
    //   3616	3621	3671	java/lang/NumberFormatException
    //   3642	3647	3671	java/lang/NumberFormatException
    //   3663	3668	3671	java/lang/NumberFormatException
    //   1135	1140	3681	org/json/JSONException
    //   1143	1148	3681	org/json/JSONException
    //   1162	1165	3681	org/json/JSONException
    //   1183	1188	3681	org/json/JSONException
    //   1193	1198	3681	org/json/JSONException
    //   1217	1222	3681	org/json/JSONException
    //   1235	1240	3681	org/json/JSONException
    //   1256	1261	3681	org/json/JSONException
    //   1263	1268	3681	org/json/JSONException
    //   1278	1283	3681	org/json/JSONException
    //   1285	1288	3681	org/json/JSONException
    //   1306	1311	3681	org/json/JSONException
    //   1320	1325	3681	org/json/JSONException
    //   1332	1337	3681	org/json/JSONException
    //   1344	1349	3681	org/json/JSONException
    //   1354	1359	3681	org/json/JSONException
    //   1385	1390	3681	org/json/JSONException
    //   1412	1416	3681	org/json/JSONException
    //   1441	1446	3681	org/json/JSONException
    //   1459	1464	3681	org/json/JSONException
    //   1482	1487	3681	org/json/JSONException
    //   1503	1508	3681	org/json/JSONException
    //   1519	1524	3681	org/json/JSONException
    //   1545	1549	3681	org/json/JSONException
    //   1577	1582	3681	org/json/JSONException
    //   1586	1591	3681	org/json/JSONException
    //   1593	1598	3681	org/json/JSONException
    //   1611	1616	3681	org/json/JSONException
    //   1620	1625	3681	org/json/JSONException
    //   1625	1630	3681	org/json/JSONException
    //   1644	1649	3681	org/json/JSONException
    //   1671	1674	3681	org/json/JSONException
    //   1708	1711	3681	org/json/JSONException
    //   1732	1737	3681	org/json/JSONException
    //   1756	1761	3681	org/json/JSONException
    //   1777	1782	3681	org/json/JSONException
    //   1789	1794	3681	org/json/JSONException
    //   1803	1808	3681	org/json/JSONException
    //   1810	1815	3681	org/json/JSONException
    //   1817	1822	3681	org/json/JSONException
    //   1831	1836	3681	org/json/JSONException
    //   1838	1843	3681	org/json/JSONException
    //   1847	1852	3681	org/json/JSONException
    //   1855	1858	3681	org/json/JSONException
    //   1860	1865	3681	org/json/JSONException
    //   1867	1873	3681	org/json/JSONException
    //   1880	1886	3681	org/json/JSONException
    //   1888	1893	3681	org/json/JSONException
    //   1897	1903	3681	org/json/JSONException
    //   1903	1908	3681	org/json/JSONException
    //   1912	1917	3681	org/json/JSONException
    //   1924	1929	3681	org/json/JSONException
    //   1948	1953	3681	org/json/JSONException
    //   1962	1967	3681	org/json/JSONException
    //   1969	1974	3681	org/json/JSONException
    //   1976	1981	3681	org/json/JSONException
    //   1985	1990	3681	org/json/JSONException
    //   1997	2002	3681	org/json/JSONException
    //   2016	2021	3681	org/json/JSONException
    //   2028	2033	3681	org/json/JSONException
    //   2041	2046	3681	org/json/JSONException
    //   2046	2051	3681	org/json/JSONException
    //   2055	2060	3681	org/json/JSONException
    //   2065	2070	3681	org/json/JSONException
    //   2085	2089	3681	org/json/JSONException
    //   2096	2101	3681	org/json/JSONException
    //   2108	2113	3681	org/json/JSONException
    //   2128	2132	3681	org/json/JSONException
    //   2139	2144	3681	org/json/JSONException
    //   2152	2157	3681	org/json/JSONException
    //   2157	2162	3681	org/json/JSONException
    //   2164	2169	3681	org/json/JSONException
    //   2204	2209	3681	org/json/JSONException
    //   2211	2216	3681	org/json/JSONException
    //   2216	2221	3681	org/json/JSONException
    //   2235	2240	3681	org/json/JSONException
    //   2266	2269	3681	org/json/JSONException
    //   2271	2276	3681	org/json/JSONException
    //   2276	2281	3681	org/json/JSONException
    //   2285	2290	3681	org/json/JSONException
    //   2290	2295	3681	org/json/JSONException
    //   2299	2304	3681	org/json/JSONException
    //   2304	2309	3681	org/json/JSONException
    //   2325	2330	3681	org/json/JSONException
    //   2330	2335	3681	org/json/JSONException
    //   2351	2356	3681	org/json/JSONException
    //   2358	2363	3681	org/json/JSONException
    //   2363	2368	3681	org/json/JSONException
    //   2380	2385	3681	org/json/JSONException
    //   2398	2403	3681	org/json/JSONException
    //   2418	2422	3681	org/json/JSONException
    //   2426	2431	3681	org/json/JSONException
    //   2627	2630	3681	org/json/JSONException
    //   2630	2633	3681	org/json/JSONException
    //   2637	2642	3681	org/json/JSONException
    //   2649	2654	3681	org/json/JSONException
    //   2670	2675	3681	org/json/JSONException
    //   2687	2690	3681	org/json/JSONException
    //   2694	2699	3681	org/json/JSONException
    //   2706	2711	3681	org/json/JSONException
    //   2727	2732	3681	org/json/JSONException
    //   2744	2747	3681	org/json/JSONException
    //   2751	2756	3681	org/json/JSONException
    //   2763	2768	3681	org/json/JSONException
    //   2784	2789	3681	org/json/JSONException
    //   2801	2804	3681	org/json/JSONException
    //   2808	2813	3681	org/json/JSONException
    //   2820	2825	3681	org/json/JSONException
    //   2841	2846	3681	org/json/JSONException
    //   2858	2862	3681	org/json/JSONException
    //   2864	2867	3681	org/json/JSONException
    //   2877	2881	3681	org/json/JSONException
    //   2883	2888	3681	org/json/JSONException
    //   2892	2897	3681	org/json/JSONException
    //   2899	2904	3681	org/json/JSONException
    //   2911	2914	3681	org/json/JSONException
    //   2921	2926	3681	org/json/JSONException
    //   2928	2933	3681	org/json/JSONException
    //   2937	2942	3681	org/json/JSONException
    //   2944	2949	3681	org/json/JSONException
    //   2956	2959	3681	org/json/JSONException
    //   2969	2973	3681	org/json/JSONException
    //   2975	2980	3681	org/json/JSONException
    //   2984	2989	3681	org/json/JSONException
    //   2991	2996	3681	org/json/JSONException
    //   3003	3006	3681	org/json/JSONException
    //   3013	3018	3681	org/json/JSONException
    //   3020	3025	3681	org/json/JSONException
    //   3029	3034	3681	org/json/JSONException
    //   3036	3041	3681	org/json/JSONException
    //   3048	3051	3681	org/json/JSONException
    //   3058	3063	3681	org/json/JSONException
    //   3065	3070	3681	org/json/JSONException
    //   3074	3079	3681	org/json/JSONException
    //   3081	3086	3681	org/json/JSONException
    //   3093	3096	3681	org/json/JSONException
    //   3100	3105	3681	org/json/JSONException
    //   3107	3112	3681	org/json/JSONException
    //   3116	3121	3681	org/json/JSONException
    //   3123	3128	3681	org/json/JSONException
    //   3135	3138	3681	org/json/JSONException
    //   3142	3147	3681	org/json/JSONException
    //   3149	3154	3681	org/json/JSONException
    //   3158	3163	3681	org/json/JSONException
    //   3165	3170	3681	org/json/JSONException
    //   3177	3180	3681	org/json/JSONException
    //   3187	3192	3681	org/json/JSONException
    //   3194	3199	3681	org/json/JSONException
    //   3203	3208	3681	org/json/JSONException
    //   3210	3215	3681	org/json/JSONException
    //   3222	3225	3681	org/json/JSONException
    //   3233	3238	3681	org/json/JSONException
    //   3240	3245	3681	org/json/JSONException
    //   3249	3254	3681	org/json/JSONException
    //   3256	3261	3681	org/json/JSONException
    //   3268	3271	3681	org/json/JSONException
    //   3279	3284	3681	org/json/JSONException
    //   3286	3291	3681	org/json/JSONException
    //   3295	3300	3681	org/json/JSONException
    //   3302	3307	3681	org/json/JSONException
    //   3314	3317	3681	org/json/JSONException
    //   3325	3330	3681	org/json/JSONException
    //   3332	3337	3681	org/json/JSONException
    //   3341	3346	3681	org/json/JSONException
    //   3348	3353	3681	org/json/JSONException
    //   3360	3363	3681	org/json/JSONException
    //   3371	3376	3681	org/json/JSONException
    //   3378	3383	3681	org/json/JSONException
    //   3387	3392	3681	org/json/JSONException
    //   3394	3399	3681	org/json/JSONException
    //   3456	3461	3681	org/json/JSONException
    //   3477	3482	3681	org/json/JSONException
    //   3482	3487	3681	org/json/JSONException
    //   3500	3503	3681	org/json/JSONException
    //   3505	3509	3681	org/json/JSONException
    //   3513	3518	3681	org/json/JSONException
    //   3520	3525	3681	org/json/JSONException
    //   3532	3535	3681	org/json/JSONException
    //   3537	3541	3681	org/json/JSONException
    //   3545	3550	3681	org/json/JSONException
    //   3552	3557	3681	org/json/JSONException
    //   3564	3567	3681	org/json/JSONException
    //   3569	3573	3681	org/json/JSONException
    //   3577	3582	3681	org/json/JSONException
    //   3584	3589	3681	org/json/JSONException
    //   3596	3599	3681	org/json/JSONException
    //   3601	3605	3681	org/json/JSONException
    //   3609	3614	3681	org/json/JSONException
    //   3616	3621	3681	org/json/JSONException
    //   3642	3647	3681	org/json/JSONException
    //   3663	3668	3681	org/json/JSONException
    //   16	19	3694	java/lang/NumberFormatException
    //   28	33	3694	java/lang/NumberFormatException
    //   38	43	3694	java/lang/NumberFormatException
    //   43	47	3694	java/lang/NumberFormatException
    //   49	54	3694	java/lang/NumberFormatException
    //   58	63	3694	java/lang/NumberFormatException
    //   63	67	3694	java/lang/NumberFormatException
    //   78	83	3694	java/lang/NumberFormatException
    //   83	87	3694	java/lang/NumberFormatException
    //   91	96	3694	java/lang/NumberFormatException
    //   96	100	3694	java/lang/NumberFormatException
    //   109	114	3694	java/lang/NumberFormatException
    //   130	135	3694	java/lang/NumberFormatException
    //   142	147	3694	java/lang/NumberFormatException
    //   150	155	3694	java/lang/NumberFormatException
    //   157	162	3694	java/lang/NumberFormatException
    //   165	170	3694	java/lang/NumberFormatException
    //   170	175	3694	java/lang/NumberFormatException
    //   179	184	3694	java/lang/NumberFormatException
    //   191	196	3694	java/lang/NumberFormatException
    //   218	222	3694	java/lang/NumberFormatException
    //   229	234	3694	java/lang/NumberFormatException
    //   243	248	3694	java/lang/NumberFormatException
    //   257	262	3694	java/lang/NumberFormatException
    //   274	279	3694	java/lang/NumberFormatException
    //   286	289	3694	java/lang/NumberFormatException
    //   291	296	3694	java/lang/NumberFormatException
    //   302	307	3694	java/lang/NumberFormatException
    //   328	334	3694	java/lang/NumberFormatException
    //   336	341	3694	java/lang/NumberFormatException
    //   345	350	3694	java/lang/NumberFormatException
    //   354	360	3694	java/lang/NumberFormatException
    //   369	374	3694	java/lang/NumberFormatException
    //   386	391	3694	java/lang/NumberFormatException
    //   393	398	3694	java/lang/NumberFormatException
    //   407	412	3694	java/lang/NumberFormatException
    //   419	424	3694	java/lang/NumberFormatException
    //   431	434	3694	java/lang/NumberFormatException
    //   439	444	3694	java/lang/NumberFormatException
    //   444	449	3694	java/lang/NumberFormatException
    //   460	464	3694	java/lang/NumberFormatException
    //   478	483	3694	java/lang/NumberFormatException
    //   492	497	3694	java/lang/NumberFormatException
    //   506	511	3694	java/lang/NumberFormatException
    //   513	518	3694	java/lang/NumberFormatException
    //   520	525	3694	java/lang/NumberFormatException
    //   534	539	3694	java/lang/NumberFormatException
    //   547	552	3694	java/lang/NumberFormatException
    //   563	568	3694	java/lang/NumberFormatException
    //   577	582	3694	java/lang/NumberFormatException
    //   586	591	3694	java/lang/NumberFormatException
    //   605	610	3694	java/lang/NumberFormatException
    //   619	624	3694	java/lang/NumberFormatException
    //   635	639	3694	java/lang/NumberFormatException
    //   646	651	3694	java/lang/NumberFormatException
    //   651	656	3694	java/lang/NumberFormatException
    //   663	668	3694	java/lang/NumberFormatException
    //   672	677	3694	java/lang/NumberFormatException
    //   684	689	3694	java/lang/NumberFormatException
    //   691	696	3694	java/lang/NumberFormatException
    //   703	708	3694	java/lang/NumberFormatException
    //   724	729	3694	java/lang/NumberFormatException
    //   743	746	3694	java/lang/NumberFormatException
    //   755	760	3694	java/lang/NumberFormatException
    //   776	781	3694	java/lang/NumberFormatException
    //   793	798	3694	java/lang/NumberFormatException
    //   817	822	3694	java/lang/NumberFormatException
    //   824	829	3694	java/lang/NumberFormatException
    //   836	841	3694	java/lang/NumberFormatException
    //   848	853	3694	java/lang/NumberFormatException
    //   860	865	3694	java/lang/NumberFormatException
    //   881	886	3694	java/lang/NumberFormatException
    //   997	1000	3694	java/lang/NumberFormatException
    //   1018	1023	3694	java/lang/NumberFormatException
    //   1025	1030	3694	java/lang/NumberFormatException
    //   1046	1051	3694	java/lang/NumberFormatException
    //   16	19	3703	org/json/JSONException
    //   28	33	3703	org/json/JSONException
    //   38	43	3703	org/json/JSONException
    //   43	47	3703	org/json/JSONException
    //   49	54	3703	org/json/JSONException
    //   58	63	3703	org/json/JSONException
    //   63	67	3703	org/json/JSONException
    //   78	83	3703	org/json/JSONException
    //   83	87	3703	org/json/JSONException
    //   91	96	3703	org/json/JSONException
    //   96	100	3703	org/json/JSONException
    //   109	114	3703	org/json/JSONException
    //   130	135	3703	org/json/JSONException
    //   142	147	3703	org/json/JSONException
    //   150	155	3703	org/json/JSONException
    //   157	162	3703	org/json/JSONException
    //   165	170	3703	org/json/JSONException
    //   170	175	3703	org/json/JSONException
    //   179	184	3703	org/json/JSONException
    //   191	196	3703	org/json/JSONException
    //   218	222	3703	org/json/JSONException
    //   229	234	3703	org/json/JSONException
    //   243	248	3703	org/json/JSONException
    //   257	262	3703	org/json/JSONException
    //   274	279	3703	org/json/JSONException
    //   286	289	3703	org/json/JSONException
    //   291	296	3703	org/json/JSONException
    //   302	307	3703	org/json/JSONException
    //   328	334	3703	org/json/JSONException
    //   336	341	3703	org/json/JSONException
    //   345	350	3703	org/json/JSONException
    //   354	360	3703	org/json/JSONException
    //   369	374	3703	org/json/JSONException
    //   386	391	3703	org/json/JSONException
    //   393	398	3703	org/json/JSONException
    //   407	412	3703	org/json/JSONException
    //   419	424	3703	org/json/JSONException
    //   431	434	3703	org/json/JSONException
    //   439	444	3703	org/json/JSONException
    //   444	449	3703	org/json/JSONException
    //   460	464	3703	org/json/JSONException
    //   478	483	3703	org/json/JSONException
    //   492	497	3703	org/json/JSONException
    //   506	511	3703	org/json/JSONException
    //   513	518	3703	org/json/JSONException
    //   520	525	3703	org/json/JSONException
    //   534	539	3703	org/json/JSONException
    //   547	552	3703	org/json/JSONException
    //   563	568	3703	org/json/JSONException
    //   577	582	3703	org/json/JSONException
    //   586	591	3703	org/json/JSONException
    //   605	610	3703	org/json/JSONException
    //   619	624	3703	org/json/JSONException
    //   635	639	3703	org/json/JSONException
    //   646	651	3703	org/json/JSONException
    //   651	656	3703	org/json/JSONException
    //   663	668	3703	org/json/JSONException
    //   672	677	3703	org/json/JSONException
    //   684	689	3703	org/json/JSONException
    //   691	696	3703	org/json/JSONException
    //   703	708	3703	org/json/JSONException
    //   724	729	3703	org/json/JSONException
    //   743	746	3703	org/json/JSONException
    //   755	760	3703	org/json/JSONException
    //   776	781	3703	org/json/JSONException
    //   793	798	3703	org/json/JSONException
    //   817	822	3703	org/json/JSONException
    //   824	829	3703	org/json/JSONException
    //   836	841	3703	org/json/JSONException
    //   848	853	3703	org/json/JSONException
    //   860	865	3703	org/json/JSONException
    //   881	886	3703	org/json/JSONException
    //   895	900	3703	org/json/JSONException
    //   916	921	3703	org/json/JSONException
    //   923	928	3703	org/json/JSONException
    //   944	949	3703	org/json/JSONException
    //   958	963	3703	org/json/JSONException
    //   970	975	3703	org/json/JSONException
    //   978	983	3703	org/json/JSONException
    //   997	1000	3703	org/json/JSONException
    //   1018	1023	3703	org/json/JSONException
    //   1025	1030	3703	org/json/JSONException
    //   1046	1051	3703	org/json/JSONException
    //   1060	1065	3703	org/json/JSONException
    //   1081	1086	3703	org/json/JSONException
    //   1088	1093	3703	org/json/JSONException
    //   1109	1114	3703	org/json/JSONException
    //   1123	1128	3703	org/json/JSONException
    //   1644	1649	3718	java/text/ParseException
  }
  
  private static ArrayList a(String paramString1, String paramString2, String paramString3, Date paramDate, HashMap paramHashMap, String paramString4)
  {
    String str1 = paramString2;
    String str2 = paramString3;
    Object localObject1 = paramDate;
    String str3 = paramString4;
    Object localObject2 = paramString1.trim();
    Object localObject4 = paramHashMap;
    localObject2 = paramHashMap.get(localObject2);
    localObject4 = localObject2;
    localObject4 = (ArrayList)localObject2;
    int i1 = 32768;
    Object localObject3;
    label533:
    int i12;
    if (localObject4 != null)
    {
      int i2 = ((ArrayList)localObject4).size();
      if (i2 > 0)
      {
        localObject2 = paramString3.replaceAll("\\s{2,}", " ");
        localArrayList = new java/util/ArrayList;
        localArrayList.<init>();
        e.a();
        int i3 = 0;
        Object localObject5 = null;
        Object localObject6 = localObject2;
        for (;;)
        {
          int i4 = ((String)localObject6).length();
          if (i4 <= 0) {
            break;
          }
          Object localObject7 = localObject2;
          i4 = 0;
          for (;;)
          {
            i2 = ((ArrayList)localObject4).size();
            if (i4 >= i2) {
              break;
            }
            localObject2 = ((ArrayList)localObject4).get(i4);
            Object localObject8 = localObject2;
            localObject8 = (h)localObject2;
            localObject2 = h;
            String str4;
            String str5;
            if (localObject2 != null)
            {
              str4 = "replace";
              try
              {
                str4 = ((JSONObject)localObject2).getString(str4);
                str5 = "by";
                localObject2 = ((JSONObject)localObject2).getString(str5);
                localObject2 = ((String)localObject7).replaceAll(str4, (String)localObject2);
                localObject7 = localObject2;
              }
              catch (JSONException localJSONException)
              {
                localJSONException.printStackTrace();
              }
            }
            localObject3 = b.matcher((CharSequence)localObject6);
            boolean bool3 = ((Matcher)localObject3).find();
            if (bool3)
            {
              str5 = ((Matcher)localObject3).replaceFirst("");
              e.a();
              localObject6 = c;
              i3 = com.daamitt.prime.sdk.a.a.a((String)localObject6);
              int i6 = 9999;
              if (i3 != i6)
              {
                i6 = 99;
                if (i3 != i6)
                {
                  try
                  {
                    localObject6 = d;
                    str4 = "transaction";
                    boolean bool2 = ((String)localObject6).equalsIgnoreCase(str4);
                    if (bool2)
                    {
                      localObject5 = a((Matcher)localObject3, (h)localObject8, str1, str2, (Date)localObject1);
                      if (localObject5 != null) {
                        b((k)localObject5);
                      }
                    }
                    else
                    {
                      localObject6 = d;
                      str4 = "statement";
                      bool2 = ((String)localObject6).equalsIgnoreCase(str4);
                      if (bool2)
                      {
                        localObject5 = b((Matcher)localObject3, (h)localObject8, str1, str2, (Date)localObject1);
                        if (localObject5 != null) {
                          b((k)localObject5);
                        }
                      }
                      else
                      {
                        localObject6 = d;
                        str4 = "event";
                        bool2 = ((String)localObject6).equalsIgnoreCase(str4);
                        if (bool2) {
                          localObject5 = c((Matcher)localObject3, (h)localObject8, str1, str2, (Date)localObject1);
                        }
                      }
                    }
                  }
                  catch (RuntimeException localRuntimeException)
                  {
                    e.d();
                  }
                  if (localObject5 != null)
                  {
                    F = ((h)localObject8);
                    localArrayList.add(localObject5);
                  }
                  bool1 = m;
                  if (bool1)
                  {
                    bool1 = true;
                    localObject6 = str5;
                    break label533;
                  }
                  localObject6 = str5;
                  bool1 = false;
                  localObject3 = null;
                  break label533;
                }
              }
              localObject6 = str5;
              bool1 = false;
              localObject3 = null;
              break label533;
            }
            i4 += 1;
          }
          bool1 = false;
          localObject3 = null;
          int i5 = ((ArrayList)localObject4).size();
          if ((i4 == i5) || (!bool1)) {
            break;
          }
          localObject3 = localObject7;
        }
        bool1 = localArrayList.isEmpty();
        if (!bool1) {
          break label860;
        }
        bool1 = a(str2, str3);
        if (bool1) {
          break label860;
        }
        localObject3 = new com/daamitt/prime/sdk/a/k;
        ((k)localObject3).<init>(str1, str2, (Date)localObject1);
        int i7 = 9;
        u = i7;
        v = i7;
        str1 = get0e;
        localObject1 = "Messages";
        ((k)localObject3).a(str1, (String)localObject1);
        boolean bool4 = a(paramString3);
        if (bool4)
        {
          int i8 = G | i1;
          G = i8;
        }
        else
        {
          boolean bool5 = b(paramString3);
          if (bool5)
          {
            int i9 = G;
            i12 = 65536;
            i9 |= i12;
            G = i9;
          }
        }
        b((k)localObject3);
        a((k)localObject3);
        localArrayList.add(localObject3);
        break label860;
      }
    }
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    boolean bool1 = a(str2, str3);
    if (!bool1)
    {
      localObject3 = a(paramString1, paramString2, paramString3, paramDate);
      boolean bool6 = a(paramString3);
      if (bool6)
      {
        int i10 = G | i1;
        G = i10;
      }
      else
      {
        boolean bool7 = b(paramString3);
        if (bool7)
        {
          int i11 = G;
          i12 = 65536;
          i11 |= i12;
          G = i11;
        }
      }
      b((k)localObject3);
      a((k)localObject3);
      localArrayList.add(localObject3);
    }
    label860:
    return localArrayList;
  }
  
  public static ArrayList a(String paramString1, String paramString2, Date paramDate, HashMap paramHashMap, String paramString3)
  {
    String str1 = paramString1.toUpperCase();
    paramString1 = "(?i)[A-Z]{2}-?[!,A-Z,0-9]{1,8}\\s*";
    boolean bool1 = str1.matches(paramString1);
    if (!bool1)
    {
      paramString1 = "(?i)[0-9]{1,9}\\s*";
      bool1 = str1.matches(paramString1);
      if (!bool1) {
        return null;
      }
    }
    paramString1 = "Unknown";
    Object localObject = str1.split("-");
    int i2 = localObject.length;
    int i3 = 1;
    int i4 = 2;
    String str2;
    if (i2 == i4)
    {
      localObject = localObject[i3];
      str2 = paramString2;
      return a((String)localObject, str1, paramString2, paramDate, paramHashMap, paramString3);
    }
    i2 = localObject.length;
    if (i2 == i3)
    {
      str2 = "(?i)[0-9]{1,9}\\s*";
      boolean bool2 = str1.matches(str2);
      if (bool2)
      {
        localObject = localObject[0];
        str2 = paramString2;
        return a((String)localObject, str1, paramString2, paramDate, paramHashMap, paramString3);
      }
      localObject = str1.substring(i4).trim();
      localObject = (ArrayList)paramHashMap.get(localObject);
      if (localObject != null)
      {
        int i5 = ((ArrayList)localObject).size();
        if (i5 > 0)
        {
          paramString1 = str1.substring(i4);
          break label248;
        }
      }
      localObject = str1.trim();
      localObject = (ArrayList)paramHashMap.get(localObject);
      if (localObject != null)
      {
        boolean bool3 = ((ArrayList)localObject).isEmpty();
        if (!bool3)
        {
          paramString1 = str1;
          break label248;
        }
      }
      i3 = 0;
      label248:
      if (i3 == 0)
      {
        int i1 = str1.length();
        int i6 = 7;
        if (i1 >= i6)
        {
          paramString1 = str1.substring(i4);
          localObject = paramString1;
        }
        else
        {
          localObject = str1;
        }
      }
      else
      {
        localObject = paramString1;
      }
      str2 = paramString2;
      return a((String)localObject, str1, paramString2, paramDate, paramHashMap, paramString3);
    }
    paramHashMap = new java/util/ArrayList;
    paramHashMap.<init>();
    boolean bool4 = a(paramString2, paramString3);
    if (!bool4)
    {
      paramString1 = a(paramString1, str1, paramString2, paramDate);
      paramHashMap.add(paramString1);
    }
    return paramHashMap;
  }
  
  private static void a(k paramk)
  {
    Object localObject1 = q;
    if (localObject1 != null)
    {
      Object localObject2 = ((String)localObject1).trim();
      boolean bool = TextUtils.isEmpty((CharSequence)localObject2);
      if (!bool)
      {
        localObject2 = c;
        String str1 = ((String)localObject1).toLowerCase();
        localObject2 = ((Pattern)localObject2).matcher(str1);
        bool = ((Matcher)localObject2).matches();
        if (!bool)
        {
          localObject2 = d;
          str1 = ((String)localObject1).toLowerCase();
          localObject2 = ((Pattern)localObject2).matcher(str1);
          bool = ((Matcher)localObject2).matches();
          if (!bool) {}
        }
        else
        {
          localObject2 = e;
          str1 = ((String)localObject1).toLowerCase();
          localObject2 = ((Pattern)localObject2).matcher(str1);
          bool = ((Matcher)localObject2).matches();
          if (!bool)
          {
            localObject2 = f;
            str1 = ((String)localObject1).toLowerCase();
            localObject2 = ((Pattern)localObject2).matcher(str1);
            bool = ((Matcher)localObject2).matches();
            if (!bool) {}
          }
          else
          {
            localObject2 = g;
            str1 = ((String)localObject1).toLowerCase();
            localObject2 = ((Pattern)localObject2).matcher(str1);
            bool = ((Matcher)localObject2).matches();
            if (!bool)
            {
              localObject2 = h;
              str1 = ((String)localObject1).toLowerCase();
              localObject2 = ((Pattern)localObject2).matcher(str1);
              bool = ((Matcher)localObject2).matches();
              if (!bool)
              {
                try
                {
                  localObject2 = b;
                  str1 = ((String)localObject1).toLowerCase();
                  localObject2 = ((Pattern)localObject2).matcher(str1);
                  bool = ((Matcher)localObject2).matches();
                  if (bool)
                  {
                    localObject2 = b;
                    localObject1 = ((String)localObject1).toLowerCase();
                    localObject1 = ((Pattern)localObject2).matcher((CharSequence)localObject1);
                    bool = ((Matcher)localObject1).find();
                    if (bool)
                    {
                      double d1 = Double.MIN_VALUE;
                      int i2 = 1;
                      try
                      {
                        localObject1 = ((Matcher)localObject1).group(i2);
                        String str2 = ",";
                        String str3 = "";
                        localObject1 = ((String)localObject1).replace(str2, str3);
                        localObject1 = Double.valueOf((String)localObject1);
                        d1 = ((Double)localObject1).doubleValue();
                      }
                      catch (NumberFormatException localNumberFormatException)
                      {
                        e.a();
                      }
                      S = d1;
                    }
                  }
                }
                catch (IllegalStateException localIllegalStateException)
                {
                  localIllegalStateException.getMessage();
                  e.c();
                }
                int i3 = G;
                int i1 = 134217728;
                i3 |= i1;
                G = i3;
              }
            }
          }
        }
      }
    }
  }
  
  private static boolean a(String paramString)
  {
    Object localObject = (Pattern)s.get(0);
    String str = paramString.toLowerCase();
    localObject = ((Pattern)localObject).matcher(str);
    boolean bool1 = ((Matcher)localObject).matches();
    if (bool1)
    {
      localObject = s;
      int i1 = 1;
      localObject = (Pattern)((ArrayList)localObject).get(i1);
      paramString = paramString.toLowerCase();
      paramString = ((Pattern)localObject).matcher(paramString);
      boolean bool2 = paramString.matches();
      if (bool2) {
        return i1;
      }
    }
    return false;
  }
  
  private static boolean a(String paramString1, String paramString2)
  {
    String str1 = "\n";
    String str2 = "";
    paramString1 = paramString1.replaceAll(str1, str2).toLowerCase();
    if (paramString2 != null)
    {
      boolean bool = paramString1.matches(paramString2);
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  private static boolean a(String paramString, ArrayList paramArrayList)
  {
    paramArrayList = paramArrayList.iterator();
    boolean bool;
    do
    {
      bool = paramArrayList.hasNext();
      if (!bool) {
        break;
      }
      Matcher localMatcher = ((Pattern)paramArrayList.next()).matcher(paramString);
      bool = localMatcher.find();
    } while (!bool);
    return true;
    return false;
  }
  
  private static l b(Matcher paramMatcher, h paramh, String paramString1, String paramString2, Date paramDate)
  {
    Object localObject1 = paramMatcher;
    Object localObject2 = paramh;
    Object localObject3 = paramDate;
    for (;;)
    {
      try
      {
        localObject4 = g;
        str1 = "statement_type";
        str1 = ((JSONObject)localObject4).getString(str1);
        localObject5 = "pan";
        localObject5 = ((JSONObject)localObject4).optJSONObject((String)localObject5);
        i1 = -1;
        if (localObject5 != null)
        {
          localObject6 = "group_id";
          i3 = ((JSONObject)localObject5).optInt((String)localObject6, i1);
          if (i3 >= 0)
          {
            localObject6 = paramMatcher.group(i3);
          }
          else
          {
            i3 = 0;
            localObject6 = null;
          }
          if (localObject6 == null)
          {
            localObject6 = "value";
            str2 = "Unknown";
            localObject5 = ((JSONObject)localObject5).optString((String)localObject6, str2);
          }
          else
          {
            localObject5 = localObject6;
          }
        }
        else
        {
          localObject5 = null;
        }
        localObject6 = "amount";
        localObject6 = ((JSONObject)localObject4).optJSONObject((String)localObject6);
        int i4;
        if (localObject6 != null)
        {
          str2 = "group_id";
          i4 = ((JSONObject)localObject6).optInt(str2, i1);
          if (i4 >= 0)
          {
            str2 = ((Matcher)localObject1).group(i4);
            localObject7 = ",";
            localObject8 = "";
            str2 = str2.replace((CharSequence)localObject7, (CharSequence)localObject8);
          }
          else
          {
            str2 = "value";
            str2 = ((JSONObject)localObject6).optString(str2);
          }
        }
        else
        {
          i4 = 0;
          str2 = null;
        }
        localObject7 = "min_due_amount";
        localObject7 = ((JSONObject)localObject4).optJSONObject((String)localObject7);
        if (localObject7 != null)
        {
          localObject8 = "group_id";
          i5 = ((JSONObject)localObject7).optInt((String)localObject8, i1);
          if (i5 >= 0)
          {
            localObject7 = ((Matcher)localObject1).group(i5);
            localObject8 = ",";
            String str3 = "";
            localObject7 = ((String)localObject7).replace((CharSequence)localObject8, str3);
            continue;
          }
        }
        int i5 = 0;
        localObject7 = null;
        localObject8 = "date";
        localObject8 = ((JSONObject)localObject4).optJSONObject((String)localObject8);
        i6 = 1;
        i7 = 0;
        if (localObject8 != null)
        {
          str4 = "group_id";
          int i8 = ((JSONObject)localObject8).optInt(str4, i1);
          if (i8 >= 0)
          {
            str4 = ((Matcher)localObject1).group(i8);
            str4 = str4.trim();
            str4 = str4.toLowerCase();
          }
          else
          {
            i8 = 0;
            str4 = null;
          }
          String str5 = "formats";
          localObject8 = ((JSONObject)localObject8).optJSONArray(str5);
          if (localObject8 != null)
          {
            i9 = 0;
            str5 = null;
            localObject9 = null;
            i1 = ((JSONArray)localObject8).length();
            if (i9 < i1)
            {
              localObject10 = ((JSONArray)localObject8).getJSONObject(i9);
              localObject11 = "use_sms_time";
              boolean bool2 = ((JSONObject)localObject10).optBoolean((String)localObject11, false);
              if (bool2)
              {
                localObject12 = localObject3;
                continue;
              }
              localObject11 = "format";
              localObject11 = ((JSONObject)localObject10).optString((String)localObject11);
            }
          }
        }
      }
      catch (NumberFormatException localNumberFormatException)
      {
        Object localObject4;
        String str1;
        Object localObject5;
        int i1;
        Object localObject6;
        int i3;
        String str2;
        Object localObject7;
        Object localObject8;
        int i6;
        int i7;
        String str4;
        int i9;
        Object localObject9;
        Object localObject10;
        Object localObject11;
        Object localObject12;
        int i11;
        int i10;
        boolean bool3;
        int i12;
        String str6;
        double d1;
        int i13;
        boolean bool4;
        double d2;
        int i14;
        return null;
      }
      catch (JSONException localJSONException2)
      {
        localJSONException2.printStackTrace();
        return null;
      }
      try
      {
        localObject11 = b(str4, (String)localObject11);
        try
        {
          i1 = ((Date)localObject11).getYear();
          i7 = 70;
          if (i1 == i7)
          {
            i1 = paramDate.getYear();
            ((Date)localObject11).setYear(i1);
            boolean bool1 = ((Date)localObject11).before((Date)localObject3);
            if (bool1)
            {
              int i2 = paramDate.getYear() + i6;
              ((Date)localObject11).setYear(i2);
            }
          }
          localObject12 = localObject11;
        }
        catch (ParseException localParseException1)
        {
          localObject9 = localObject11;
        }
        e.b();
      }
      catch (ParseException localParseException2)
      {
        continue;
      }
      i9 += 1;
      i7 = 0;
    }
    localObject12 = localObject9;
    break label583;
    localObject12 = null;
    label583:
    if (localObject12 == null)
    {
      e.b();
      return null;
    }
    localObject11 = "-1";
    localObject10 = "create_txn";
    localObject10 = ((JSONObject)localObject4).optJSONObject((String)localObject10);
    if (localObject10 != null)
    {
      localObject8 = "amount";
      localObject10 = ((JSONObject)localObject10).optJSONObject((String)localObject8);
      if (localObject10 != null)
      {
        localObject11 = "group_id";
        i11 = -1;
        i10 = ((JSONObject)localObject10).optInt((String)localObject11, i11);
        if (i10 >= 0)
        {
          localObject11 = ((Matcher)localObject1).group(i10);
          localObject10 = ",";
          localObject6 = "";
          localObject11 = ((String)localObject11).replace((CharSequence)localObject10, (CharSequence)localObject6);
        }
        else
        {
          localObject11 = "value";
          localObject11 = ((JSONObject)localObject6).optString((String)localObject11);
        }
      }
    }
    localObject10 = new com/daamitt/prime/sdk/a/l;
    localObject6 = paramString1;
    localObject8 = paramString2;
    ((l)localObject10).<init>(paramString1, paramString2, (Date)localObject3);
    localObject3 = "deleted";
    i3 = 0;
    localObject6 = null;
    bool3 = ((JSONObject)localObject4).optBoolean((String)localObject3, false);
    if (bool3)
    {
      i12 = j | 0x2;
      j = i12;
    }
    i12 = 7;
    u = i12;
    E = i6;
    localObject3 = c;
    i12 = com.daamitt.prime.sdk.a.a.a((String)localObject3);
    v = i12;
    localObject3 = f;
    B = ((String)localObject3);
    localObject3 = e;
    localObject6 = "Bills";
    ((l)localObject10).a((String)localObject3, (String)localObject6);
    str6 = d((String)localObject5);
    localObject3 = Double.valueOf(str2);
    d1 = ((Double)localObject3).doubleValue();
    i13 = l.a(str1);
    ((l)localObject10).a(str6, d1, (Date)localObject12, i13);
    bool4 = i;
    L = bool4;
    bool4 = TextUtils.isEmpty((CharSequence)localObject7);
    if (!bool4)
    {
      localObject2 = Double.valueOf((String)localObject7);
      d2 = ((Double)localObject2).doubleValue();
      e = d2;
    }
    localObject2 = Double.valueOf((String)localObject11);
    d2 = ((Double)localObject2).doubleValue();
    f = d2;
    localObject2 = "enable_chaining";
    i3 = 0;
    localObject6 = null;
    bool4 = ((JSONObject)localObject4).optBoolean((String)localObject2, false);
    K = bool4;
    if (bool4)
    {
      localObject2 = "chaining_rule";
      try
      {
        localObject2 = ((JSONObject)localObject4).getJSONObject((String)localObject2);
        if (localObject2 != null)
        {
          localObject3 = new com/daamitt/prime/sdk/a/c;
          ((c)localObject3).<init>();
          localObject11 = "parent_selection";
          localObject11 = ((JSONObject)localObject2).optJSONArray((String)localObject11);
          if (localObject11 != null)
          {
            i14 = ((JSONArray)localObject11).length();
            if (i14 > 0) {
              for (;;)
              {
                i14 = ((JSONArray)localObject11).length();
                if (i3 >= i14) {
                  break;
                }
                localObject4 = ((JSONArray)localObject11).getJSONObject(i3);
                localObject4 = a((JSONObject)localObject4, (Matcher)localObject1);
                ((c)localObject3).a((c.a)localObject4);
                i3 += 1;
              }
            }
          }
          localObject1 = "parent_match";
          localObject1 = ((JSONObject)localObject2).optJSONObject((String)localObject1);
          if (localObject1 != null)
          {
            localObject1 = a((JSONObject)localObject1);
            b = ((c.b)localObject1);
          }
          localObject1 = "parent_nomatch";
          localObject1 = ((JSONObject)localObject2).optJSONObject((String)localObject1);
          if (localObject1 != null)
          {
            localObject1 = a((JSONObject)localObject1);
            c = ((c.b)localObject1);
          }
          J = ((c)localObject3);
        }
      }
      catch (JSONException localJSONException1)
      {
        e.b();
      }
    }
    return (l)localObject10;
  }
  
  private static Date b(String paramString1, String paramString2)
  {
    SimpleDateFormat localSimpleDateFormat = new java/text/SimpleDateFormat;
    localSimpleDateFormat.<init>(paramString2);
    return localSimpleDateFormat.parse(paramString1);
  }
  
  private static boolean b(k paramk)
  {
    String str1 = q;
    boolean bool2;
    if (str1 != null)
    {
      str2 = str1.trim();
      bool1 = TextUtils.isEmpty(str2);
      if (!bool1)
      {
        str1 = str1.toLowerCase().trim();
        str2 = "(?i).* emi .*";
        bool2 = str1.matches(str2);
        if (!bool2) {
          return false;
        }
      }
    }
    str1 = p;
    String str2 = "(?i).*axis.*";
    boolean bool1 = str1.matches(str2);
    if (bool1) {
      return c(paramk);
    }
    str2 = "(?i).*bajajf";
    bool1 = str1.matches(str2);
    if (!bool1)
    {
      str2 = "(?i).*040801";
      bool1 = str1.matches(str2);
      if (!bool1)
      {
        str2 = "(?i).*icici.*";
        bool1 = str1.matches(str2);
        if (bool1) {
          return h(paramk);
        }
        str2 = "(?i).*sbi.*";
        bool1 = str1.matches(str2);
        if (bool1) {
          return e(paramk);
        }
        str2 = "(?i).*citi.*";
        bool1 = str1.matches(str2);
        if (bool1) {
          return f(paramk);
        }
        str2 = "(?i).*hdfc.*";
        bool1 = str1.matches(str2);
        if (!bool1)
        {
          str2 = "(?i).*020001";
          bool1 = str1.matches(str2);
          if (!bool1)
          {
            str2 = "(?i).*080008";
            bool2 = str1.matches(str2);
            if (!bool2) {
              return false;
            }
          }
        }
        return g(paramk);
      }
    }
    return d(paramk);
  }
  
  private static boolean b(String paramString)
  {
    paramString = paramString.toLowerCase();
    Object localObject = ((Pattern)t.get(0)).matcher(paramString);
    boolean bool1 = ((Matcher)localObject).matches();
    if (bool1)
    {
      localObject = t;
      int i1 = 1;
      localObject = ((Pattern)((ArrayList)localObject).get(i1)).matcher(paramString);
      bool1 = ((Matcher)localObject).matches();
      if (bool1)
      {
        localObject = ((Pattern)v.get(0)).matcher(paramString);
        bool1 = ((Matcher)localObject).matches();
        if (!bool1)
        {
          localObject = ((Pattern)v.get(i1)).matcher(paramString);
          bool1 = ((Matcher)localObject).matches();
          if (!bool1)
          {
            localObject = v;
            int i2 = 2;
            localObject = ((Pattern)((ArrayList)localObject).get(i2)).matcher(paramString);
            bool1 = ((Matcher)localObject).matches();
            if (!bool1)
            {
              localObject = v;
              int i3 = 3;
              localObject = ((Pattern)((ArrayList)localObject).get(i3)).matcher(paramString);
              bool1 = ((Matcher)localObject).matches();
              if (!bool1)
              {
                localObject = v;
                int i4 = 4;
                localObject = ((Pattern)((ArrayList)localObject).get(i4)).matcher(paramString);
                bool1 = ((Matcher)localObject).matches();
                int i5 = 9;
                int i6 = 8;
                int i7 = 7;
                int i8 = 6;
                int i9 = 5;
                if (!bool1)
                {
                  localObject = ((Pattern)v.get(i9)).matcher(paramString);
                  bool1 = ((Matcher)localObject).matches();
                  if (!bool1)
                  {
                    localObject = ((Pattern)v.get(i8)).matcher(paramString);
                    bool1 = ((Matcher)localObject).matches();
                    if (!bool1)
                    {
                      localObject = ((Pattern)v.get(i7)).matcher(paramString);
                      bool1 = ((Matcher)localObject).matches();
                      if (!bool1)
                      {
                        localObject = ((Pattern)v.get(i6)).matcher(paramString);
                        bool1 = ((Matcher)localObject).matches();
                        if (!bool1)
                        {
                          localObject = ((Pattern)v.get(i5)).matcher(paramString);
                          bool1 = ((Matcher)localObject).matches();
                          if (!bool1)
                          {
                            localObject = v;
                            int i10 = 10;
                            localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                            bool1 = ((Matcher)localObject).matches();
                            if (!bool1)
                            {
                              localObject = v;
                              i10 = 11;
                              localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                              bool1 = ((Matcher)localObject).matches();
                              if (!bool1)
                              {
                                localObject = v;
                                i10 = 12;
                                localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                                bool1 = ((Matcher)localObject).matches();
                                if (!bool1)
                                {
                                  localObject = v;
                                  i10 = 13;
                                  localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                                  bool1 = ((Matcher)localObject).matches();
                                  if (!bool1)
                                  {
                                    localObject = v;
                                    i10 = 14;
                                    localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                                    bool1 = ((Matcher)localObject).matches();
                                    if (!bool1)
                                    {
                                      localObject = v;
                                      i10 = 15;
                                      localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                                      bool1 = ((Matcher)localObject).matches();
                                      if (!bool1)
                                      {
                                        localObject = v;
                                        i10 = 16;
                                        localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                                        bool1 = ((Matcher)localObject).matches();
                                        if (!bool1)
                                        {
                                          localObject = v;
                                          i10 = 17;
                                          localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                                          bool1 = ((Matcher)localObject).matches();
                                          if (!bool1)
                                          {
                                            localObject = v;
                                            i10 = 18;
                                            localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                                            bool1 = ((Matcher)localObject).matches();
                                            if (!bool1)
                                            {
                                              localObject = v;
                                              i10 = 19;
                                              localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                                              bool1 = ((Matcher)localObject).matches();
                                              if (!bool1)
                                              {
                                                localObject = v;
                                                i10 = 20;
                                                localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                                                bool1 = ((Matcher)localObject).matches();
                                                if (!bool1)
                                                {
                                                  localObject = v;
                                                  i10 = 21;
                                                  localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                                                  bool1 = ((Matcher)localObject).matches();
                                                  if (!bool1)
                                                  {
                                                    localObject = v;
                                                    i10 = 22;
                                                    localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                                                    bool1 = ((Matcher)localObject).matches();
                                                    if (!bool1)
                                                    {
                                                      localObject = v;
                                                      i10 = 23;
                                                      localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                                                      bool1 = ((Matcher)localObject).matches();
                                                      if (!bool1)
                                                      {
                                                        localObject = v;
                                                        i10 = 24;
                                                        localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                                                        bool1 = ((Matcher)localObject).matches();
                                                        if (!bool1)
                                                        {
                                                          localObject = v;
                                                          i10 = 25;
                                                          localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                                                          bool1 = ((Matcher)localObject).matches();
                                                          if (!bool1)
                                                          {
                                                            localObject = v;
                                                            i10 = 26;
                                                            localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                                                            bool1 = ((Matcher)localObject).matches();
                                                            if (!bool1)
                                                            {
                                                              localObject = v;
                                                              i10 = 27;
                                                              localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                                                              bool1 = ((Matcher)localObject).matches();
                                                              if (!bool1)
                                                              {
                                                                localObject = v;
                                                                i10 = 28;
                                                                localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                                                                bool1 = ((Matcher)localObject).matches();
                                                                if (!bool1)
                                                                {
                                                                  localObject = v;
                                                                  i10 = 29;
                                                                  localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                                                                  bool1 = ((Matcher)localObject).matches();
                                                                  if (!bool1)
                                                                  {
                                                                    localObject = v;
                                                                    i10 = 30;
                                                                    localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                                                                    bool1 = ((Matcher)localObject).matches();
                                                                    if (!bool1)
                                                                    {
                                                                      localObject = v;
                                                                      i10 = 31;
                                                                      localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                                                                      bool1 = ((Matcher)localObject).matches();
                                                                      if (!bool1)
                                                                      {
                                                                        localObject = v;
                                                                        i10 = 32;
                                                                        localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                                                                        bool1 = ((Matcher)localObject).matches();
                                                                        if (!bool1)
                                                                        {
                                                                          localObject = v;
                                                                          i10 = 33;
                                                                          localObject = ((Pattern)((ArrayList)localObject).get(i10)).matcher(paramString);
                                                                          bool1 = ((Matcher)localObject).matches();
                                                                          if (!bool1) {
                                                                            break label1339;
                                                                          }
                                                                        }
                                                                      }
                                                                    }
                                                                  }
                                                                }
                                                              }
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
                localObject = ((Pattern)u.get(0)).matcher(paramString);
                bool1 = ((Matcher)localObject).matches();
                if (!bool1)
                {
                  localObject = ((Pattern)u.get(i1)).matcher(paramString);
                  bool1 = ((Matcher)localObject).matches();
                  if (!bool1)
                  {
                    localObject = ((Pattern)u.get(i2)).matcher(paramString);
                    bool1 = ((Matcher)localObject).matches();
                    if (!bool1)
                    {
                      localObject = ((Pattern)u.get(i3)).matcher(paramString);
                      bool1 = ((Matcher)localObject).matches();
                      if (!bool1)
                      {
                        localObject = ((Pattern)u.get(i4)).matcher(paramString);
                        bool1 = ((Matcher)localObject).matches();
                        if (!bool1)
                        {
                          localObject = ((Pattern)u.get(i9)).matcher(paramString);
                          bool1 = ((Matcher)localObject).matches();
                          if (!bool1)
                          {
                            localObject = ((Pattern)u.get(i8)).matcher(paramString);
                            bool1 = ((Matcher)localObject).matches();
                            if (!bool1)
                            {
                              localObject = ((Pattern)u.get(i7)).matcher(paramString);
                              bool1 = ((Matcher)localObject).matches();
                              if (!bool1)
                              {
                                localObject = ((Pattern)u.get(i6)).matcher(paramString);
                                bool1 = ((Matcher)localObject).matches();
                                if (!bool1)
                                {
                                  localObject = (Pattern)u.get(i5);
                                  paramString = ((Pattern)localObject).matcher(paramString);
                                  boolean bool2 = paramString.matches();
                                  if (!bool2) {
                                    return i1;
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
                return false;
              }
            }
          }
        }
      }
    }
    label1339:
    return false;
  }
  
  private static d c(Matcher paramMatcher, h paramh, String paramString1, String paramString2, Date paramDate)
  {
    Object localObject1 = paramMatcher;
    Object localObject2 = paramh;
    try
    {
      Object localObject3 = g;
      String str1 = "event_type";
      str1 = ((JSONObject)localObject3).getString(str1);
      Object localObject4 = "name";
      localObject4 = ((JSONObject)localObject3).optJSONObject((String)localObject4);
      int i1 = -1;
      if (localObject4 != null)
      {
        localObject5 = "group_id";
        i3 = ((JSONObject)localObject4).optInt((String)localObject5, i1);
        if (i3 >= 0)
        {
          localObject4 = paramMatcher.group(i3);
        }
        else
        {
          localObject5 = "value";
          localObject6 = "Unknown";
          localObject4 = ((JSONObject)localObject4).optString((String)localObject5, (String)localObject6);
        }
      }
      else
      {
        localObject4 = null;
      }
      Object localObject5 = "pnr";
      localObject5 = ((JSONObject)localObject3).optJSONObject((String)localObject5);
      if (localObject5 != null)
      {
        localObject6 = "group_id";
        i3 = ((JSONObject)localObject5).optInt((String)localObject6, i1);
        if (i3 >= 0)
        {
          localObject5 = ((Matcher)localObject1).group(i3);
          localObject6 = ",";
          localObject7 = "";
          localObject5 = ((String)localObject5).replace((CharSequence)localObject6, (CharSequence)localObject7);
          break label186;
        }
      }
      int i3 = 0;
      localObject5 = null;
      label186:
      Object localObject6 = "contact";
      localObject6 = ((JSONObject)localObject3).optJSONObject((String)localObject6);
      Object localObject8;
      int i5;
      if (localObject6 != null)
      {
        localObject7 = "prefix";
        localObject8 = "";
        localObject7 = ((JSONObject)localObject6).optString((String)localObject7, (String)localObject8);
        localObject8 = "group_id";
        i4 = ((JSONObject)localObject6).optInt((String)localObject8, i1);
        if (i4 >= 0)
        {
          i5 = ((String)localObject7).length();
          if (i5 > 0)
          {
            localObject8 = new java/lang/StringBuilder;
            ((StringBuilder)localObject8).<init>();
            ((StringBuilder)localObject8).append((String)localObject7);
            localObject7 = " ";
            ((StringBuilder)localObject8).append((String)localObject7);
            localObject6 = ((Matcher)localObject1).group(i4);
            ((StringBuilder)localObject8).append((String)localObject6);
            localObject6 = ((StringBuilder)localObject8).toString();
            break label335;
          }
          localObject6 = ((Matcher)localObject1).group(i4);
          break label335;
        }
      }
      int i4 = 0;
      localObject6 = null;
      label335:
      Object localObject7 = "amount";
      localObject7 = ((JSONObject)localObject3).optJSONObject((String)localObject7);
      int i6 = 0;
      Object localObject9 = null;
      Object localObject11;
      boolean bool3;
      double d2;
      if (localObject7 != null)
      {
        localObject10 = "group_id";
        int i7 = ((JSONObject)localObject7).optInt((String)localObject10, i1);
        if (i7 >= 0)
        {
          double d1;
          try
          {
            localObject10 = ((Matcher)localObject1).group(i7);
            localObject11 = ",";
            localObject12 = "";
            localObject10 = ((String)localObject10).replace((CharSequence)localObject11, (CharSequence)localObject12);
            localObject10 = Double.valueOf((String)localObject10);
            d1 = ((Double)localObject10).doubleValue();
          }
          catch (NumberFormatException localNumberFormatException1)
          {
            e.d();
            d1 = 0.0D;
          }
          localObject12 = "txn_direction";
          localObject13 = "";
          localObject13 = ((JSONObject)localObject7).optString((String)localObject12, (String)localObject13);
          localObject12 = "incoming";
          bool3 = ((String)localObject13).equalsIgnoreCase((String)localObject12);
          if (bool3)
          {
            d1 = -d1;
            d2 = d1;
          }
          else
          {
            d2 = d1;
          }
        }
        else
        {
          localObject13 = "group_ids";
          localObject13 = ((JSONObject)localObject7).optJSONArray((String)localObject13);
          if (localObject13 != null)
          {
            i7 = 0;
            localObject10 = null;
            double d3 = 0.0D;
            for (;;)
            {
              i5 = ((JSONArray)localObject13).length();
              if (i7 >= i5) {
                break;
              }
              i5 = ((JSONArray)localObject13).getInt(i7);
              localObject8 = ((Matcher)localObject1).group(i5);
              String str2 = ",";
              localObject14 = "";
              localObject14 = ((String)localObject8).replace(str2, (CharSequence)localObject14);
              localObject14 = Double.valueOf((String)localObject14);
              d2 = ((Double)localObject14).doubleValue();
              d3 += d2;
              i7 += 1;
              i1 = -1;
            }
            d2 = d3;
          }
          else
          {
            d2 = 0.0D;
          }
        }
        localObject13 = "create_txn";
        bool3 = ((JSONObject)localObject7).optBoolean((String)localObject13, false);
      }
      else
      {
        bool3 = false;
        localObject13 = null;
        d2 = 0.0D;
      }
      Object localObject14 = "date";
      localObject14 = ((JSONObject)localObject3).optJSONObject((String)localObject14);
      int i9;
      if (localObject14 != null)
      {
        localObject10 = "use_sms_time";
        boolean bool2 = ((JSONObject)localObject14).optBoolean((String)localObject10, false);
        if (bool2)
        {
          localObject11 = paramDate;
          i9 = 1;
        }
        else
        {
          localObject10 = "group_ids";
          localObject10 = ((JSONObject)localObject14).optJSONArray((String)localObject10);
          int i10;
          if (localObject10 != null)
          {
            localObject11 = new java/lang/StringBuilder;
            ((StringBuilder)localObject11).<init>();
            i10 = 0;
            localObject12 = null;
            for (;;)
            {
              i9 = ((JSONArray)localObject10).length();
              if (i10 >= i9) {
                break;
              }
              if (i10 > 0)
              {
                localObject7 = " ";
                ((StringBuilder)localObject11).append((String)localObject7);
              }
              i9 = ((JSONArray)localObject10).getInt(i10);
              localObject7 = ((Matcher)localObject1).group(i9);
              ((StringBuilder)localObject11).append((String)localObject7);
              i10 += 1;
            }
            localObject7 = ((StringBuilder)localObject11).toString();
          }
          else
          {
            localObject7 = "group_id";
            i8 = -1;
            i9 = ((JSONObject)localObject14).optInt((String)localObject7, i8);
            if (i9 >= 0)
            {
              localObject7 = ((Matcher)localObject1).group(i9);
            }
            else
            {
              i9 = 0;
              localObject7 = null;
            }
          }
          localObject10 = "formats";
          localObject14 = ((JSONObject)localObject14).optJSONArray((String)localObject10);
          if (localObject14 != null)
          {
            i8 = 0;
            localObject10 = null;
            localObject11 = null;
            for (;;)
            {
              i10 = ((JSONArray)localObject14).length();
              if (i8 >= i10) {
                break;
              }
              localObject12 = ((JSONArray)localObject14).getJSONObject(i8);
              Object localObject15 = localObject14;
              localObject14 = "use_sms_time";
              boolean bool1 = ((JSONObject)localObject12).optBoolean((String)localObject14, false);
              if (bool1)
              {
                localObject11 = paramDate;
                i9 = 1;
                break label1038;
              }
              localObject14 = "format";
              localObject14 = ((JSONObject)localObject12).optString((String)localObject14);
              try
              {
                localObject11 = b((String)localObject7, (String)localObject14);
                localObject12 = "HH";
                boolean bool4 = ((String)localObject14).contains((CharSequence)localObject12);
                if (!bool4)
                {
                  localObject12 = "hh";
                  bool1 = ((String)localObject14).contains((CharSequence)localObject12);
                  if (!bool1)
                  {
                    i9 = 0;
                    localObject7 = null;
                    break label1038;
                  }
                }
                i9 = 1;
              }
              catch (ParseException localParseException)
              {
                e.b();
                i8 += 1;
                localObject14 = localObject15;
              }
            }
            i9 = 1;
          }
          else
          {
            i9 = 1;
            localObject11 = null;
          }
          label1038:
          if (localObject11 == null)
          {
            e.b();
            localObject11 = paramDate;
          }
        }
      }
      else
      {
        i9 = 1;
        localObject11 = null;
      }
      int i2 = ((Date)localObject11).getYear();
      int i8 = 70;
      if (i2 == i8)
      {
        i2 = paramDate.getYear();
        ((Date)localObject11).setYear(i2);
      }
      localObject14 = "event_info";
      localObject14 = ((JSONObject)localObject3).optJSONObject((String)localObject14);
      if (localObject14 != null)
      {
        localObject10 = "group_id";
        i11 = -1;
        i8 = ((JSONObject)localObject14).optInt((String)localObject10, i11);
        if (i8 >= 0)
        {
          localObject14 = ((Matcher)localObject1).group(i8);
          localObject14 = ((String)localObject14).trim();
        }
        else
        {
          localObject10 = "value";
          localObject14 = ((JSONObject)localObject14).optString((String)localObject10);
        }
      }
      else
      {
        i2 = 0;
        localObject14 = null;
      }
      Object localObject10 = "event_location";
      localObject10 = ((JSONObject)localObject3).optJSONObject((String)localObject10);
      Object localObject16;
      if (localObject10 != null)
      {
        localObject12 = "group_id";
        i6 = -1;
        i6 = ((JSONObject)localObject10).optInt((String)localObject12, i6);
        if (i6 >= 0)
        {
          localObject9 = ((Matcher)localObject1).group(i6);
          localObject16 = localObject6;
          break label1372;
        }
        localObject9 = "group_ids";
        localObject9 = ((JSONObject)localObject10).optJSONArray((String)localObject9);
        if (localObject9 != null)
        {
          localObject10 = new java/lang/StringBuilder;
          ((StringBuilder)localObject10).<init>();
          localObject16 = localObject6;
          i11 = 0;
          localObject12 = null;
          for (;;)
          {
            i4 = ((JSONArray)localObject9).length();
            if (i11 >= i4) {
              break;
            }
            if (i11 > 0)
            {
              localObject6 = "-";
              ((StringBuilder)localObject10).append((String)localObject6);
            }
            i4 = ((JSONArray)localObject9).getInt(i11);
            localObject6 = ((Matcher)localObject1).group(i4);
            ((StringBuilder)localObject10).append((String)localObject6);
            i11 += 1;
          }
          localObject6 = ((StringBuilder)localObject10).toString();
          localObject9 = localObject6;
          break label1372;
        }
        localObject16 = localObject6;
      }
      else
      {
        localObject16 = localObject6;
      }
      i6 = 0;
      localObject9 = null;
      label1372:
      localObject6 = "event_reminder_span";
      localObject6 = ((JSONObject)localObject3).optJSONObject((String)localObject6);
      long l1 = 1800000L;
      long l2;
      if (localObject6 != null)
      {
        localObject10 = "value";
        i11 = 30;
        i4 = ((JSONObject)localObject6).optInt((String)localObject10, i11);
        localObject12 = localObject9;
        l2 = i4;
        l1 = 60000L * l2;
        l2 = l1;
      }
      else
      {
        localObject12 = localObject9;
        l2 = l1;
      }
      localObject6 = new com/daamitt/prime/sdk/a/d;
      localObject1 = paramString2;
      Object localObject17 = localObject3;
      Object localObject18 = localObject12;
      Object localObject12 = paramString1;
      localObject3 = paramDate;
      ((d)localObject6).<init>(paramString1, paramString2, paramDate);
      int i12 = d.a(str1);
      str1 = e;
      localObject12 = "events";
      ((d)localObject6).a(str1, (String)localObject12);
      int i15 = 5;
      u = i15;
      int i11 = 1;
      E = i11;
      localObject2 = c;
      int i16 = com.daamitt.prime.sdk.a.a.a((String)localObject2);
      v = i16;
      m = l2;
      if (localObject5 != null) {
        localObject2 = d((String)localObject5);
      } else {
        localObject2 = "";
      }
      i3 = 2;
      if (i3 == i12)
      {
        localObject9 = " ";
        localObject10 = "";
        localObject2 = ((String)localObject2).replace((CharSequence)localObject9, (CharSequence)localObject10);
      }
      localObject4 = d((String)localObject4);
      ((d)localObject6).a((String)localObject4, (String)localObject2, (Date)localObject11, i12);
      g = ((String)localObject14);
      k = bool3;
      localObject2 = "Cancelled";
      boolean bool5 = TextUtils.equals((CharSequence)localObject2, (CharSequence)localObject14);
      int i17;
      if (bool5)
      {
        i17 = h | 0x8;
        h = i17;
      }
      double d4;
      if (i9 == 0)
      {
        i17 = h | i3;
        h = i17;
        d4 = 0.0D;
      }
      else
      {
        d4 = 0.0D;
      }
      int i13 = d2 < d4;
      if (i13 != 0) {
        i = d2;
      }
      boolean bool6 = TextUtils.isEmpty((CharSequence)localObject16);
      if (!bool6)
      {
        localObject13 = localObject16;
        j = ((String)localObject16);
        localObject9 = localObject18;
      }
      else
      {
        localObject9 = localObject18;
      }
      f = ((String)localObject9);
      localObject2 = "deleted";
      Object localObject13 = localObject17;
      i13 = 0;
      localObject3 = null;
      bool6 = ((JSONObject)localObject17).optBoolean((String)localObject2, false);
      if (bool6)
      {
        int i18 = h;
        i13 = 1;
        i18 |= i13;
        h = i18;
      }
      localObject2 = "enable_chaining";
      i13 = 0;
      localObject3 = null;
      boolean bool7 = ((JSONObject)localObject13).optBoolean((String)localObject2, false);
      K = bool7;
      if (bool7)
      {
        localObject2 = "chaining_rule";
        try
        {
          localObject2 = ((JSONObject)localObject13).getJSONObject((String)localObject2);
          if (localObject2 != null)
          {
            localObject13 = new com/daamitt/prime/sdk/a/c;
            ((c)localObject13).<init>();
            localObject4 = "parent_selection";
            localObject4 = ((JSONObject)localObject2).optJSONArray((String)localObject4);
            if (localObject4 != null)
            {
              i2 = ((JSONArray)localObject4).length();
              if (i2 > 0) {
                for (;;)
                {
                  i2 = ((JSONArray)localObject4).length();
                  if (i13 >= i2) {
                    break;
                  }
                  localObject14 = ((JSONArray)localObject4).getJSONObject(i13);
                  localObject1 = paramMatcher;
                  localObject14 = a((JSONObject)localObject14, paramMatcher);
                  ((c)localObject13).a((c.a)localObject14);
                  int i14;
                  i13 += 1;
                  localObject1 = paramString2;
                }
              }
            }
            localObject1 = "parent_match";
            localObject1 = ((JSONObject)localObject2).optJSONObject((String)localObject1);
            if (localObject1 != null)
            {
              localObject1 = a((JSONObject)localObject1);
              b = ((c.b)localObject1);
            }
            localObject1 = "parent_nomatch";
            localObject1 = ((JSONObject)localObject2).optJSONObject((String)localObject1);
            if (localObject1 != null)
            {
              localObject1 = a((JSONObject)localObject1);
              c = ((c.b)localObject1);
            }
            J = ((c)localObject13);
          }
        }
        catch (JSONException localJSONException1)
        {
          e.b();
        }
      }
      int i19 = e;
      if (i19 == i15)
      {
        localObject1 = c(paramString2);
        if (localObject1 != null)
        {
          localObject2 = ((String)localObject1).trim();
          bool7 = TextUtils.isEmpty((CharSequence)localObject2);
          if (!bool7)
          {
            localObject2 = ((String)localObject1).trim();
            localObject13 = "3A";
            bool7 = ((String)localObject2).equalsIgnoreCase((String)localObject13);
            if (!bool7)
            {
              localObject2 = ((String)localObject1).trim();
              localObject13 = "2A";
              bool7 = ((String)localObject2).equalsIgnoreCase((String)localObject13);
              if (!bool7)
              {
                localObject2 = ((String)localObject1).trim();
                localObject13 = "1A";
                bool7 = ((String)localObject2).equalsIgnoreCase((String)localObject13);
                if (!bool7)
                {
                  localObject2 = ((String)localObject1).trim();
                  localObject13 = "CC";
                  bool7 = ((String)localObject2).equalsIgnoreCase((String)localObject13);
                  if (!bool7)
                  {
                    localObject2 = ((String)localObject1).trim();
                    localObject13 = "FC";
                    bool7 = ((String)localObject2).equalsIgnoreCase((String)localObject13);
                    if (!bool7)
                    {
                      localObject2 = ((String)localObject1).trim();
                      localObject13 = "3E";
                      bool7 = ((String)localObject2).equalsIgnoreCase((String)localObject13);
                      if (!bool7)
                      {
                        localObject1 = ((String)localObject1).trim();
                        localObject2 = "EC";
                        boolean bool8 = ((String)localObject1).equalsIgnoreCase((String)localObject2);
                        if (!bool8) {
                          break label2291;
                        }
                      }
                    }
                  }
                }
              }
            }
            int i20 = h & 0x10;
            h = i20;
          }
        }
      }
      label2291:
      return (d)localObject6;
    }
    catch (NumberFormatException localNumberFormatException2)
    {
      return null;
    }
    catch (JSONException localJSONException2)
    {
      localJSONException2;
    }
    return null;
  }
  
  private static String c(String paramString)
  {
    String str1 = "\\s{2,}";
    paramString = paramString.replaceAll(str1, " ");
    int i1 = 1;
    String str2 = null;
    Object localObject1 = "(?i).*DOJ:.*,TIME:.*";
    boolean bool1;
    boolean bool2;
    try
    {
      bool1 = paramString.matches((String)localObject1);
      if (bool1)
      {
        localObject1 = "(?i).*DOJ:[\\w-]+,(?:TIME):?\\s?[\\w.:]+?,(.*?),.*";
        localObject1 = Pattern.compile((String)localObject1);
        localObject1 = ((Pattern)localObject1).matcher(paramString);
        bool2 = ((Matcher)localObject1).find();
        if (bool2) {
          str2 = ((Matcher)localObject1).group(i1);
        }
      }
    }
    catch (IllegalStateException localIllegalStateException1)
    {
      localIllegalStateException1.getMessage();
      e.c();
    }
    if (str2 == null)
    {
      Object localObject2 = "(?i).*,.*";
      try
      {
        bool1 = paramString.matches((String)localObject2);
        if (!bool1)
        {
          localObject2 = "(?i).*DOJ:\\d\\d-\\d\\d-\\d\\d\\d\\d.*";
          bool1 = paramString.matches((String)localObject2);
          if (!bool1)
          {
            localObject2 = "(?i).*DOJ:\\d\\d-\\d\\d-\\d\\d(\\w\\w).*";
            localObject2 = Pattern.compile((String)localObject2);
            localObject2 = ((Pattern)localObject2).matcher(paramString);
            bool2 = ((Matcher)localObject2).find();
            if (bool2) {
              str2 = ((Matcher)localObject2).group(i1);
            }
          }
        }
      }
      catch (IllegalStateException localIllegalStateException2)
      {
        localIllegalStateException2.getMessage();
        e.c();
      }
    }
    if (str2 == null)
    {
      Object localObject3 = "(?i).*,.*";
      try
      {
        bool1 = paramString.matches((String)localObject3);
        if (!bool1)
        {
          localObject3 = "(?i).*TIME:N.A..*";
          bool1 = paramString.matches((String)localObject3);
          if (bool1)
          {
            localObject3 = "(?i).*DOJ:\\d\\d-\\d\\d-\\d\\d\\d\\d(?:TIME):N.A.(\\w\\w).*";
            localObject3 = Pattern.compile((String)localObject3);
            localObject3 = ((Pattern)localObject3).matcher(paramString);
            bool2 = ((Matcher)localObject3).find();
            if (bool2) {
              str2 = ((Matcher)localObject3).group(i1);
            }
          }
        }
      }
      catch (IllegalStateException localIllegalStateException3)
      {
        localIllegalStateException3.getMessage();
        e.c();
      }
    }
    if (str2 == null)
    {
      Object localObject4 = "(?i).*,.*";
      try
      {
        bool1 = paramString.matches((String)localObject4);
        if (!bool1)
        {
          localObject4 = "(?i).*DOJ:\\d\\d-\\d\\d-\\d\\d\\d\\d.*";
          bool1 = paramString.matches((String)localObject4);
          if (bool1)
          {
            localObject4 = "(?i).*DOJ:\\d\\d-\\d\\d-\\d\\d\\d\\d(?:TIME)?:?\\d?\\d?:?\\d?\\d?(\\w\\w).*";
            localObject4 = Pattern.compile((String)localObject4);
            localObject4 = ((Pattern)localObject4).matcher(paramString);
            bool2 = ((Matcher)localObject4).find();
            if (bool2) {
              str2 = ((Matcher)localObject4).group(i1);
            }
          }
        }
      }
      catch (IllegalStateException localIllegalStateException4)
      {
        localIllegalStateException4.getMessage();
        e.c();
      }
    }
    if (str2 == null)
    {
      Object localObject5 = "(?i).*DOJ:[\\w-]+,(.*?),.*";
      try
      {
        localObject5 = Pattern.compile((String)localObject5);
        paramString = ((Pattern)localObject5).matcher(paramString);
        bool1 = paramString.find();
        if (bool1)
        {
          paramString = paramString.group(i1);
          str2 = paramString;
        }
      }
      catch (IllegalStateException paramString)
      {
        paramString.getMessage();
        e.c();
      }
    }
    return str2;
  }
  
  private static String c(String paramString1, String paramString2)
  {
    int i1 = paramString1.hashCode();
    int i2 = -1413853096;
    boolean bool;
    if (i1 != i2)
    {
      i2 = 111156;
      if (i1 == i2)
      {
        str = "pnr";
        bool = paramString1.equals(str);
        if (bool)
        {
          bool = true;
          break label79;
        }
      }
    }
    else
    {
      str = "amount";
      bool = paramString1.equals(str);
      if (bool)
      {
        bool = false;
        paramString1 = null;
        break label79;
      }
    }
    int i3 = -1;
    switch (i3)
    {
    default: 
      return paramString2;
    case 1: 
      label79:
      return d(paramString2);
    }
    paramString1 = ",";
    String str = "";
    double d1;
    try
    {
      paramString1 = paramString2.replace(paramString1, str);
      paramString1 = Double.valueOf(paramString1);
      d1 = paramString1.doubleValue();
    }
    catch (NumberFormatException localNumberFormatException)
    {
      d1 = 0.0D;
    }
    return String.valueOf(d1);
  }
  
  private static boolean c(k paramk)
  {
    Object localObject1 = q.toLowerCase();
    Object localObject2 = ((Pattern)m.get(0)).matcher((CharSequence)localObject1);
    boolean bool1 = ((Matcher)localObject2).matches();
    int i1 = 1;
    int i2;
    if (bool1)
    {
      i2 = G | 0x20000;
      G = i2;
      return i1;
    }
    localObject2 = ((Pattern)m.get(i1)).matcher((CharSequence)localObject1);
    bool1 = ((Matcher)localObject2).matches();
    int i4 = 262144;
    if (bool1)
    {
      i2 = G | i4;
      G = i2;
      return i1;
    }
    localObject2 = ((Pattern)m.get(2)).matcher((CharSequence)localObject1);
    bool1 = ((Matcher)localObject2).matches();
    int i5 = 524288;
    if (bool1)
    {
      i2 = G | i5;
      G = i2;
      return i1;
    }
    localObject2 = m;
    int i6 = 3;
    localObject2 = ((Pattern)((ArrayList)localObject2).get(i6)).matcher((CharSequence)localObject1);
    bool1 = ((Matcher)localObject2).matches();
    if (!bool1)
    {
      localObject2 = m;
      i6 = 4;
      localObject2 = ((Pattern)((ArrayList)localObject2).get(i6)).matcher((CharSequence)localObject1);
      bool1 = ((Matcher)localObject2).matches();
      if (!bool1)
      {
        localObject2 = m;
        i6 = 5;
        localObject2 = ((Pattern)((ArrayList)localObject2).get(i6)).matcher((CharSequence)localObject1);
        bool1 = ((Matcher)localObject2).matches();
        if (!bool1)
        {
          localObject2 = m;
          i4 = 6;
          localObject2 = ((Pattern)((ArrayList)localObject2).get(i4)).matcher((CharSequence)localObject1);
          bool1 = ((Matcher)localObject2).matches();
          if (bool1)
          {
            i2 = G | 0x1000000;
            G = i2;
            return i1;
          }
          localObject2 = m;
          i4 = 7;
          localObject2 = ((Pattern)((ArrayList)localObject2).get(i4)).matcher((CharSequence)localObject1);
          bool1 = ((Matcher)localObject2).matches();
          if (bool1)
          {
            i2 = G | i5;
            G = i2;
            return i1;
          }
          localObject2 = m;
          i4 = 8;
          localObject2 = (Pattern)((ArrayList)localObject2).get(i4);
          localObject1 = ((Pattern)localObject2).matcher((CharSequence)localObject1);
          boolean bool2 = ((Matcher)localObject1).matches();
          if (bool2)
          {
            i3 = G | 0x200000;
            G = i3;
            return i1;
          }
          return false;
        }
      }
    }
    int i3 = G | i4;
    G = i3;
    return i1;
  }
  
  private static String d(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return paramString.replaceAll("(?i)[^a-z0-9\\)\\]]*$", "").replaceAll("(?i)^[^a-z0-9\\)\\]]*", "");
  }
  
  private static boolean d(k paramk)
  {
    Object localObject1 = q.toLowerCase();
    Object localObject2 = ((Pattern)n.get(0)).matcher((CharSequence)localObject1);
    boolean bool1 = ((Matcher)localObject2).matches();
    int i1 = 4194304;
    int i2 = 1;
    if (!bool1)
    {
      localObject2 = ((Pattern)n.get(i2)).matcher((CharSequence)localObject1);
      bool1 = ((Matcher)localObject2).matches();
      if (!bool1)
      {
        localObject2 = n;
        int i3 = 2;
        localObject2 = ((Pattern)((ArrayList)localObject2).get(i3)).matcher((CharSequence)localObject1);
        bool1 = ((Matcher)localObject2).matches();
        if (!bool1)
        {
          localObject2 = ((Pattern)n.get(3)).matcher((CharSequence)localObject1);
          bool1 = ((Matcher)localObject2).matches();
          i3 = 131072;
          int i4;
          if (bool1)
          {
            i4 = G | i3;
            G = i4;
            return i2;
          }
          localObject2 = n;
          int i6 = 4;
          localObject2 = ((Pattern)((ArrayList)localObject2).get(i6)).matcher((CharSequence)localObject1);
          bool1 = ((Matcher)localObject2).matches();
          if (bool1)
          {
            i4 = G | i1;
            G = i4;
            return i2;
          }
          localObject2 = n;
          i6 = 5;
          localObject2 = ((Pattern)((ArrayList)localObject2).get(i6)).matcher((CharSequence)localObject1);
          bool1 = ((Matcher)localObject2).matches();
          if (bool1)
          {
            i4 = G | i3;
            G = i4;
            return i2;
          }
          localObject2 = ((Pattern)n.get(6)).matcher((CharSequence)localObject1);
          bool1 = ((Matcher)localObject2).matches();
          i3 = 524288;
          if (bool1)
          {
            i4 = G | i3;
            G = i4;
            return i2;
          }
          localObject2 = n;
          i6 = 7;
          localObject2 = ((Pattern)((ArrayList)localObject2).get(i6)).matcher((CharSequence)localObject1);
          bool1 = ((Matcher)localObject2).matches();
          if (!bool1)
          {
            localObject2 = n;
            i6 = 8;
            localObject2 = ((Pattern)((ArrayList)localObject2).get(i6)).matcher((CharSequence)localObject1);
            bool1 = ((Matcher)localObject2).matches();
            if (!bool1)
            {
              localObject2 = n;
              i6 = 9;
              localObject2 = ((Pattern)((ArrayList)localObject2).get(i6)).matcher((CharSequence)localObject1);
              bool1 = ((Matcher)localObject2).matches();
              if (bool1)
              {
                i4 = G | i3;
                G = i4;
                return i2;
              }
              localObject2 = n;
              i6 = 10;
              localObject2 = ((Pattern)((ArrayList)localObject2).get(i6)).matcher((CharSequence)localObject1);
              bool1 = ((Matcher)localObject2).matches();
              if (!bool1)
              {
                localObject2 = n;
                i6 = 11;
                localObject2 = ((Pattern)((ArrayList)localObject2).get(i6)).matcher((CharSequence)localObject1);
                bool1 = ((Matcher)localObject2).matches();
                if (!bool1)
                {
                  localObject2 = n;
                  i6 = 12;
                  localObject2 = ((Pattern)((ArrayList)localObject2).get(i6)).matcher((CharSequence)localObject1);
                  bool1 = ((Matcher)localObject2).matches();
                  if (!bool1)
                  {
                    localObject2 = n;
                    i6 = 13;
                    localObject2 = ((Pattern)((ArrayList)localObject2).get(i6)).matcher((CharSequence)localObject1);
                    bool1 = ((Matcher)localObject2).matches();
                    if (!bool1)
                    {
                      localObject2 = n;
                      i6 = 14;
                      localObject2 = ((Pattern)((ArrayList)localObject2).get(i6)).matcher((CharSequence)localObject1);
                      bool1 = ((Matcher)localObject2).matches();
                      if (bool1)
                      {
                        i4 = G | i3;
                        G = i4;
                        return i2;
                      }
                      localObject2 = n;
                      i3 = 15;
                      localObject2 = ((Pattern)((ArrayList)localObject2).get(i3)).matcher((CharSequence)localObject1);
                      bool1 = ((Matcher)localObject2).matches();
                      if (!bool1)
                      {
                        localObject2 = n;
                        i3 = 16;
                        localObject2 = ((Pattern)((ArrayList)localObject2).get(i3)).matcher((CharSequence)localObject1);
                        bool1 = ((Matcher)localObject2).matches();
                        if (!bool1)
                        {
                          localObject2 = n;
                          i3 = 17;
                          localObject2 = ((Pattern)((ArrayList)localObject2).get(i3)).matcher((CharSequence)localObject1);
                          bool1 = ((Matcher)localObject2).matches();
                          if (!bool1)
                          {
                            localObject2 = n;
                            i3 = 18;
                            localObject2 = ((Pattern)((ArrayList)localObject2).get(i3)).matcher((CharSequence)localObject1);
                            bool1 = ((Matcher)localObject2).matches();
                            if (!bool1)
                            {
                              localObject2 = n;
                              i3 = 19;
                              localObject2 = ((Pattern)((ArrayList)localObject2).get(i3)).matcher((CharSequence)localObject1);
                              bool1 = ((Matcher)localObject2).matches();
                              if (!bool1)
                              {
                                localObject2 = n;
                                i3 = 20;
                                localObject2 = ((Pattern)((ArrayList)localObject2).get(i3)).matcher((CharSequence)localObject1);
                                bool1 = ((Matcher)localObject2).matches();
                                if (!bool1)
                                {
                                  localObject2 = n;
                                  i1 = 21;
                                  localObject2 = (Pattern)((ArrayList)localObject2).get(i1);
                                  localObject1 = ((Pattern)localObject2).matcher((CharSequence)localObject1);
                                  boolean bool2 = ((Matcher)localObject1).matches();
                                  if (bool2)
                                  {
                                    i5 = G | 0x1000000;
                                    G = i5;
                                    return i2;
                                  }
                                  return false;
                                }
                              }
                            }
                          }
                        }
                      }
                      i5 = G | i1;
                      G = i5;
                      return i2;
                    }
                  }
                }
              }
              i5 = G | i1;
              G = i5;
              return i2;
            }
          }
          i5 = G | i1;
          G = i5;
          return i2;
        }
      }
    }
    int i5 = G | i1;
    G = i5;
    return i2;
  }
  
  private static boolean e(k paramk)
  {
    Object localObject1 = q.toLowerCase();
    Object localObject2 = ((Pattern)o.get(0)).matcher((CharSequence)localObject1);
    boolean bool1 = ((Matcher)localObject2).matches();
    int i1 = 1;
    int i2;
    if (bool1)
    {
      i2 = G | 0x20000;
      G = i2;
      return i1;
    }
    localObject2 = ((Pattern)o.get(i1)).matcher((CharSequence)localObject1);
    bool1 = ((Matcher)localObject2).matches();
    if (bool1)
    {
      i2 = G | 0x400000;
      G = i2;
      return i1;
    }
    localObject2 = ((Pattern)o.get(2)).matcher((CharSequence)localObject1);
    bool1 = ((Matcher)localObject2).matches();
    int i4 = 524288;
    if (bool1)
    {
      i2 = G | i4;
      G = i2;
      return i1;
    }
    localObject2 = ((Pattern)o.get(3)).matcher((CharSequence)localObject1);
    bool1 = ((Matcher)localObject2).matches();
    int i5 = 1048576;
    if (!bool1)
    {
      localObject2 = o;
      int i6 = 4;
      localObject2 = ((Pattern)((ArrayList)localObject2).get(i6)).matcher((CharSequence)localObject1);
      bool1 = ((Matcher)localObject2).matches();
      if (!bool1)
      {
        localObject2 = o;
        i6 = 5;
        localObject2 = ((Pattern)((ArrayList)localObject2).get(i6)).matcher((CharSequence)localObject1);
        bool1 = ((Matcher)localObject2).matches();
        if (bool1)
        {
          i2 = G | i4;
          G = i2;
          return i1;
        }
        localObject2 = o;
        i6 = 6;
        localObject2 = ((Pattern)((ArrayList)localObject2).get(i6)).matcher((CharSequence)localObject1);
        bool1 = ((Matcher)localObject2).matches();
        if (bool1)
        {
          i2 = G | i5;
          G = i2;
          return i1;
        }
        localObject2 = o;
        i5 = 7;
        localObject2 = ((Pattern)((ArrayList)localObject2).get(i5)).matcher((CharSequence)localObject1);
        bool1 = ((Matcher)localObject2).matches();
        if (!bool1)
        {
          localObject2 = o;
          i5 = 8;
          localObject2 = ((Pattern)((ArrayList)localObject2).get(i5)).matcher((CharSequence)localObject1);
          bool1 = ((Matcher)localObject2).matches();
          if (!bool1)
          {
            localObject2 = o;
            i4 = 9;
            localObject2 = (Pattern)((ArrayList)localObject2).get(i4);
            localObject1 = ((Pattern)localObject2).matcher((CharSequence)localObject1);
            boolean bool2 = ((Matcher)localObject1).matches();
            if (bool2)
            {
              i3 = G | 0x1000000;
              G = i3;
              return i1;
            }
            return false;
          }
        }
        i3 = G | i4;
        G = i3;
        return i1;
      }
    }
    int i3 = G | i5;
    G = i3;
    return i1;
  }
  
  private static boolean f(k paramk)
  {
    Object localObject1 = q.toLowerCase();
    Object localObject2 = ((Pattern)p.get(0)).matcher((CharSequence)localObject1);
    boolean bool1 = ((Matcher)localObject2).matches();
    int i1 = 1;
    int i2;
    if (bool1)
    {
      i2 = G | 0x400000;
      G = i2;
      return i1;
    }
    localObject2 = ((Pattern)p.get(i1)).matcher((CharSequence)localObject1);
    bool1 = ((Matcher)localObject2).matches();
    if (bool1)
    {
      i2 = G | 0x20000;
      G = i2;
      return i1;
    }
    localObject2 = p;
    int i4 = 2;
    localObject2 = ((Pattern)((ArrayList)localObject2).get(i4)).matcher((CharSequence)localObject1);
    bool1 = ((Matcher)localObject2).matches();
    if (bool1)
    {
      i2 = G | 0x80000;
      G = i2;
      return i1;
    }
    localObject2 = p;
    i4 = 3;
    localObject2 = (Pattern)((ArrayList)localObject2).get(i4);
    localObject1 = ((Pattern)localObject2).matcher((CharSequence)localObject1);
    boolean bool2 = ((Matcher)localObject1).matches();
    if (bool2)
    {
      int i3 = G | 0x1000000;
      G = i3;
      return i1;
    }
    return false;
  }
  
  private static boolean g(k paramk)
  {
    Object localObject1 = q.toLowerCase();
    Object localObject2 = ((Pattern)q.get(0)).matcher((CharSequence)localObject1);
    boolean bool1 = ((Matcher)localObject2).matches();
    int i1 = 131072;
    int i2 = 1;
    if (!bool1)
    {
      localObject2 = ((Pattern)q.get(i2)).matcher((CharSequence)localObject1);
      bool1 = ((Matcher)localObject2).matches();
      if (!bool1)
      {
        localObject2 = q;
        int i3 = 2;
        localObject2 = ((Pattern)((ArrayList)localObject2).get(i3)).matcher((CharSequence)localObject1);
        bool1 = ((Matcher)localObject2).matches();
        if (!bool1)
        {
          localObject2 = q;
          i3 = 3;
          localObject2 = ((Pattern)((ArrayList)localObject2).get(i3)).matcher((CharSequence)localObject1);
          bool1 = ((Matcher)localObject2).matches();
          if (!bool1)
          {
            localObject2 = q;
            i3 = 4;
            localObject2 = ((Pattern)((ArrayList)localObject2).get(i3)).matcher((CharSequence)localObject1);
            bool1 = ((Matcher)localObject2).matches();
            if (!bool1)
            {
              localObject2 = q;
              i3 = 5;
              localObject2 = ((Pattern)((ArrayList)localObject2).get(i3)).matcher((CharSequence)localObject1);
              bool1 = ((Matcher)localObject2).matches();
              if (!bool1)
              {
                localObject2 = ((Pattern)q.get(6)).matcher((CharSequence)localObject1);
                bool1 = ((Matcher)localObject2).matches();
                i3 = 524288;
                if (!bool1)
                {
                  localObject2 = q;
                  int i4 = 7;
                  localObject2 = ((Pattern)((ArrayList)localObject2).get(i4)).matcher((CharSequence)localObject1);
                  bool1 = ((Matcher)localObject2).matches();
                  if (!bool1)
                  {
                    localObject2 = q;
                    i4 = 8;
                    localObject2 = ((Pattern)((ArrayList)localObject2).get(i4)).matcher((CharSequence)localObject1);
                    bool1 = ((Matcher)localObject2).matches();
                    if (!bool1)
                    {
                      localObject2 = q;
                      i4 = 9;
                      localObject2 = ((Pattern)((ArrayList)localObject2).get(i4)).matcher((CharSequence)localObject1);
                      bool1 = ((Matcher)localObject2).matches();
                      if (!bool1)
                      {
                        localObject2 = q;
                        i4 = 10;
                        localObject2 = ((Pattern)((ArrayList)localObject2).get(i4)).matcher((CharSequence)localObject1);
                        bool1 = ((Matcher)localObject2).matches();
                        if (!bool1)
                        {
                          localObject2 = q;
                          i1 = 11;
                          localObject2 = ((Pattern)((ArrayList)localObject2).get(i1)).matcher((CharSequence)localObject1);
                          bool1 = ((Matcher)localObject2).matches();
                          int i5;
                          if (bool1)
                          {
                            i5 = G | i3;
                            G = i5;
                            return i2;
                          }
                          localObject2 = ((Pattern)q.get(12)).matcher((CharSequence)localObject1);
                          bool1 = ((Matcher)localObject2).matches();
                          i1 = 8388608;
                          if (bool1)
                          {
                            i5 = G | i1;
                            G = i5;
                            return i2;
                          }
                          localObject2 = q;
                          i3 = 13;
                          localObject2 = ((Pattern)((ArrayList)localObject2).get(i3)).matcher((CharSequence)localObject1);
                          bool1 = ((Matcher)localObject2).matches();
                          if (bool1)
                          {
                            i5 = G | 0x2000000;
                            G = i5;
                            return i2;
                          }
                          localObject2 = q;
                          i3 = 14;
                          localObject2 = ((Pattern)((ArrayList)localObject2).get(i3)).matcher((CharSequence)localObject1);
                          bool1 = ((Matcher)localObject2).matches();
                          if (!bool1)
                          {
                            localObject2 = q;
                            i3 = 15;
                            localObject2 = ((Pattern)((ArrayList)localObject2).get(i3)).matcher((CharSequence)localObject1);
                            bool1 = ((Matcher)localObject2).matches();
                            if (!bool1)
                            {
                              localObject2 = q;
                              i3 = 16;
                              localObject2 = ((Pattern)((ArrayList)localObject2).get(i3)).matcher((CharSequence)localObject1);
                              bool1 = ((Matcher)localObject2).matches();
                              if (!bool1)
                              {
                                localObject2 = q;
                                i3 = 17;
                                localObject2 = ((Pattern)((ArrayList)localObject2).get(i3)).matcher((CharSequence)localObject1);
                                bool1 = ((Matcher)localObject2).matches();
                                if (!bool1)
                                {
                                  localObject2 = q;
                                  i3 = 18;
                                  localObject2 = ((Pattern)((ArrayList)localObject2).get(i3)).matcher((CharSequence)localObject1);
                                  bool1 = ((Matcher)localObject2).matches();
                                  if (!bool1)
                                  {
                                    localObject2 = q;
                                    i3 = 19;
                                    localObject2 = ((Pattern)((ArrayList)localObject2).get(i3)).matcher((CharSequence)localObject1);
                                    bool1 = ((Matcher)localObject2).matches();
                                    if (!bool1)
                                    {
                                      localObject2 = q;
                                      i1 = 20;
                                      localObject2 = (Pattern)((ArrayList)localObject2).get(i1);
                                      localObject1 = ((Pattern)localObject2).matcher((CharSequence)localObject1);
                                      boolean bool2 = ((Matcher)localObject1).matches();
                                      if (bool2)
                                      {
                                        i6 = G | 0x1000000;
                                        G = i6;
                                        return i2;
                                      }
                                      return false;
                                    }
                                  }
                                }
                              }
                            }
                          }
                          i6 = G | i1;
                          G = i6;
                          return i2;
                        }
                      }
                    }
                    i6 = G | i1;
                    G = i6;
                    return i2;
                  }
                }
                i6 = G | i3;
                G = i6;
                return i2;
              }
            }
          }
        }
      }
    }
    int i6 = G | i1;
    G = i6;
    return i2;
  }
  
  private static boolean h(k paramk)
  {
    Object localObject1 = q.toLowerCase();
    Object localObject2 = ((Pattern)r.get(0)).matcher((CharSequence)localObject1);
    boolean bool1 = ((Matcher)localObject2).matches();
    int i1 = 1;
    if (!bool1)
    {
      localObject2 = ((Pattern)r.get(i1)).matcher((CharSequence)localObject1);
      bool1 = ((Matcher)localObject2).matches();
      if (!bool1)
      {
        localObject2 = ((Pattern)r.get(2)).matcher((CharSequence)localObject1);
        bool1 = ((Matcher)localObject2).matches();
        int i2 = 3;
        if (bool1)
        {
          localObject2 = ((Pattern)r.get(i2)).matcher((CharSequence)localObject1);
          bool1 = ((Matcher)localObject2).matches();
          if (!bool1)
          {
            int i3 = G | 0x4000000;
            G = i3;
            return i1;
          }
        }
        localObject2 = (Pattern)r.get(i2);
        localObject1 = ((Pattern)localObject2).matcher((CharSequence)localObject1);
        boolean bool2 = ((Matcher)localObject1).matches();
        if (bool2)
        {
          i4 = G | 0x1000000;
          G = i4;
          return i1;
        }
        return false;
      }
    }
    int i4 = G | 0x20000;
    G = i4;
    return i1;
  }
}

/* Location:
 * Qualified Name:     com.daamitt.prime.sdk.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */