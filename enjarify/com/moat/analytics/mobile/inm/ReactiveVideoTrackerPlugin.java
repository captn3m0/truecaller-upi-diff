package com.moat.analytics.mobile.inm;

public class ReactiveVideoTrackerPlugin
  implements MoatPlugin
{
  private final String a;
  
  public ReactiveVideoTrackerPlugin(String paramString)
  {
    a = paramString;
  }
  
  public ReactiveVideoTracker c()
  {
    ReactiveVideoTrackerPlugin.1 local1 = new com/moat/analytics/mobile/inm/ReactiveVideoTrackerPlugin$1;
    local1.<init>(this);
    return (ReactiveVideoTracker)x.a(local1, ReactiveVideoTracker.class);
  }
  
  public ReactiveVideoTracker d()
  {
    ReactiveVideoTrackerPlugin.a locala = new com/moat/analytics/mobile/inm/ReactiveVideoTrackerPlugin$a;
    locala.<init>();
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.ReactiveVideoTrackerPlugin
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */