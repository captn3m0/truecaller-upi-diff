package com.moat.analytics.mobile.inm;

import android.view.View;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

class y
  extends c
  implements ReactiveVideoTracker
{
  private Integer l;
  
  public y(String paramString)
  {
    super(paramString);
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    String str = a();
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append(" created");
    localObject = ((StringBuilder)localObject).toString();
    p.a("[SUCCESS] ", (String)localObject);
  }
  
  String a()
  {
    return "ReactiveVideoTracker";
  }
  
  protected JSONObject a(MoatAdEvent paramMoatAdEvent)
  {
    Object localObject1 = d;
    Object localObject2 = MoatAdEventType.AD_EVT_COMPLETE;
    if (localObject1 == localObject2)
    {
      localObject1 = b;
      localObject2 = MoatAdEvent.a;
      boolean bool = ((Integer)localObject1).equals(localObject2);
      if (!bool)
      {
        localObject1 = b;
        localObject2 = l;
        bool = a((Integer)localObject1, (Integer)localObject2);
        if (!bool)
        {
          localObject1 = MoatAdEventType.AD_EVT_STOPPED;
          d = ((MoatAdEventType)localObject1);
        }
      }
    }
    return super.a(paramMoatAdEvent);
  }
  
  protected Map g()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    View localView = (View)k.get();
    int i = 0;
    Integer localInteger1 = Integer.valueOf(0);
    Integer localInteger2 = Integer.valueOf(0);
    if (localView != null)
    {
      i = localView.getWidth();
      localInteger1 = Integer.valueOf(i);
      int j = localView.getHeight();
      localInteger2 = Integer.valueOf(j);
    }
    Integer localInteger3 = l;
    localHashMap.put("duration", localInteger3);
    localHashMap.put("width", localInteger1);
    localHashMap.put("height", localInteger2);
    return localHashMap;
  }
  
  public boolean trackVideoAd(Map paramMap, Integer paramInteger, View paramView)
  {
    boolean bool = e;
    int j = 3;
    if (bool)
    {
      p.a(j, "ReactiveVideoTracker", this, "trackVideoAd already called");
      paramInteger = new java/lang/StringBuilder;
      paramInteger.<init>();
      paramView = a();
      paramInteger.append(paramView);
      paramInteger.append(" trackVideoAd can't be called twice");
      paramInteger = paramInteger.toString();
      p.a("[ERROR] ", paramInteger);
      return false;
    }
    int i = paramInteger.intValue();
    int k = 1000;
    if (i < k)
    {
      paramView = Locale.ROOT;
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = paramInteger;
      paramInteger = String.format(paramView, "Invalid duration = %d. Please make sure duration is in milliseconds.", arrayOfObject);
      p.a(j, "ReactiveVideoTracker", this, paramInteger);
      return false;
    }
    l = paramInteger;
    paramInteger = new java/lang/Object;
    paramInteger.<init>();
    return super.a(paramMap, paramInteger, paramView);
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */