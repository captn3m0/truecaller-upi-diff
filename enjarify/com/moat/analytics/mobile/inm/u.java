package com.moat.analytics.mobile.inm;

import android.media.MediaPlayer;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

class u
  extends h
  implements NativeVideoTracker
{
  u(String paramString)
  {
    super(paramString);
    p.a(3, "NativeVideoTracker", this, "In initialization method.");
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    String str = a();
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append(" created");
    localObject = ((StringBuilder)localObject).toString();
    p.a("[SUCCESS] ", (String)localObject);
  }
  
  String a()
  {
    return "NativeVideoTracker";
  }
  
  protected Map g()
  {
    Object localObject = (MediaPlayer)j.get();
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    Integer localInteger = Integer.valueOf(((MediaPlayer)localObject).getVideoWidth());
    localHashMap.put("width", localInteger);
    localInteger = Integer.valueOf(((MediaPlayer)localObject).getVideoHeight());
    localHashMap.put("height", localInteger);
    localObject = Integer.valueOf(((MediaPlayer)localObject).getDuration());
    localHashMap.put("duration", localObject);
    return localHashMap;
  }
  
  protected Integer j()
  {
    return Integer.valueOf(((MediaPlayer)j.get()).getCurrentPosition());
  }
  
  protected boolean k()
  {
    return ((MediaPlayer)j.get()).isPlaying();
  }
  
  protected Integer l()
  {
    return Integer.valueOf(((MediaPlayer)j.get()).getDuration());
  }
  
  public boolean trackVideoAd(Map paramMap, MediaPlayer paramMediaPlayer, View paramView)
  {
    int i = 3;
    if (paramMediaPlayer == null)
    {
      p.a(i, "NativeVideoTracker", this, "Null player instance. Not tracking.");
      paramMap = "[ERROR] ";
      paramMediaPlayer = new java/lang/StringBuilder;
      paramMediaPlayer.<init>();
      paramView = a();
      paramMediaPlayer.append(paramView);
      paramView = " tracking didn't start, Null player instance";
    }
    for (;;)
    {
      paramMediaPlayer.append(paramView);
      paramMediaPlayer = paramMediaPlayer.toString();
      p.a(paramMap, paramMediaPlayer);
      return false;
      try
      {
        paramMediaPlayer.getCurrentPosition();
        return super.a(paramMap, paramMediaPlayer, paramView);
      }
      catch (Exception localException)
      {
        p.a(i, "NativeVideoTracker", this, "Playback has already completed. Not tracking.");
        paramMap = "[ERROR] ";
        paramMediaPlayer = new java/lang/StringBuilder;
        paramMediaPlayer.<init>();
        paramView = a();
        paramMediaPlayer.append(paramView);
        paramMediaPlayer.append(" tracking didn't started for ");
        paramView = e();
        paramMediaPlayer.append(paramView);
        paramView = ", playback has already completed";
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */