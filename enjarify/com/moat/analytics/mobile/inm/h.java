package com.moat.analytics.mobile.inm;

import android.os.Handler;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.Map;
import org.json.JSONObject;

abstract class h
  extends c
{
  int l;
  private h.a m;
  private int n;
  private double o;
  private int p;
  private int q;
  
  h(String paramString)
  {
    super(paramString);
    int i = -1 << -1;
    p = i;
    l = i;
    n = i;
    o = (0.0D / 0.0D);
    q = 0;
    paramString = h.a.a;
    m = paramString;
  }
  
  private void n()
  {
    h.1 local1 = new com/moat/analytics/mobile/inm/h$1;
    local1.<init>(this);
    h.postDelayed(local1, 200L);
  }
  
  protected JSONObject a(MoatAdEvent paramMoatAdEvent)
  {
    Object localObject1 = b;
    Object localObject2 = MoatAdEvent.a;
    boolean bool1 = ((Integer)localObject1).equals(localObject2);
    int i;
    if (!bool1)
    {
      localObject1 = b;
    }
    else
    {
      try
      {
        localObject1 = j();
      }
      catch (Exception localException)
      {
        i = n;
        localObject1 = Integer.valueOf(i);
      }
      b = ((Integer)localObject1);
    }
    localObject2 = b;
    int j = ((Integer)localObject2).intValue();
    if (j >= 0)
    {
      localObject2 = b;
      j = ((Integer)localObject2).intValue();
      if (j == 0)
      {
        localObject2 = d;
        localMoatAdEventType = MoatAdEventType.AD_EVT_COMPLETE;
        if (localObject2 == localMoatAdEventType)
        {
          j = n;
          if (j <= 0) {}
        }
      }
    }
    else
    {
      i = n;
      localObject1 = Integer.valueOf(i);
      b = ((Integer)localObject1);
    }
    localObject2 = d;
    MoatAdEventType localMoatAdEventType = MoatAdEventType.AD_EVT_COMPLETE;
    if (localObject2 == localMoatAdEventType)
    {
      j = ((Integer)localObject1).intValue();
      int k = -1 << -1;
      if (j != k)
      {
        j = l;
        if (j != k)
        {
          localObject2 = Integer.valueOf(j);
          boolean bool2 = a((Integer)localObject1, (Integer)localObject2);
          if (bool2)
          {
            localObject1 = h.a.e;
            m = ((h.a)localObject1);
            break label230;
          }
        }
      }
      localObject1 = h.a.d;
      m = ((h.a)localObject1);
      localObject1 = MoatAdEventType.AD_EVT_STOPPED;
      d = ((MoatAdEventType)localObject1);
    }
    label230:
    return super.a(paramMoatAdEvent);
  }
  
  public boolean a(Map paramMap, Object paramObject, View paramView)
  {
    try
    {
      boolean bool = e;
      if (!bool) {
        n();
      }
    }
    catch (Exception localException)
    {
      m.a(localException);
    }
    return super.a(paramMap, paramObject, paramView);
  }
  
  protected abstract Integer j();
  
  protected abstract boolean k();
  
  protected abstract Integer l();
  
  boolean m()
  {
    Object localObject1 = this.j.get();
    if (localObject1 != null)
    {
      int i = i();
      if (i == 0)
      {
        i = 1;
        try
        {
          Integer localInteger1 = j();
          j = localInteger1.intValue();
          k = n;
          if ((k >= 0) && (j < 0)) {
            return false;
          }
          n = j;
          if (j == 0) {
            return i;
          }
          Object localObject2 = l();
          k = ((Integer)localObject2).intValue();
          boolean bool1 = k();
          double d1 = k;
          double d2 = 4.0D;
          Double.isNaN(d1);
          d1 /= d2;
          d2 = s.a();
          Object localObject3 = null;
          int i2 = p;
          if (j > i2) {
            p = j;
          }
          i2 = l;
          int i3 = -1 << -1;
          if (i2 == i3) {
            l = k;
          }
          Object localObject4;
          if (bool1)
          {
            localObject2 = m;
            localObject4 = h.a.a;
            if (localObject2 == localObject4)
            {
              localObject3 = MoatAdEventType.AD_EVT_START;
              localObject2 = h.a.c;
            }
          }
          boolean bool2;
          for (;;)
          {
            m = ((h.a)localObject2);
            break;
            localObject2 = m;
            localObject4 = h.a.b;
            if (localObject2 == localObject4)
            {
              localObject3 = MoatAdEventType.AD_EVT_PLAYING;
              localObject2 = h.a.c;
            }
            else
            {
              double d3 = j;
              Double.isNaN(d3);
              d3 /= d1;
              d3 = Math.floor(d3);
              k = (int)d3 - i;
              if (k < 0) {
                break;
              }
              int i1 = 3;
              if (k >= i1) {
                break;
              }
              localObject4 = f;
              localObject2 = localObject4[k];
              localObject4 = g;
              bool2 = ((Map)localObject4).containsKey(localObject2);
              if (bool2) {
                break;
              }
              localObject4 = g;
              Integer localInteger2 = Integer.valueOf(i);
              ((Map)localObject4).put(localObject2, localInteger2);
              localObject3 = localObject2;
              break;
              localObject2 = m;
              localObject4 = h.a.b;
              if (localObject2 == localObject4) {
                break;
              }
              localObject3 = MoatAdEventType.AD_EVT_PAUSED;
              localObject2 = h.a.b;
            }
          }
          if (localObject3 != null)
          {
            k = 1;
          }
          else
          {
            k = 0;
            localObject2 = null;
          }
          if (k == 0)
          {
            double d4 = o;
            bool2 = Double.isNaN(d4);
            if (!bool2)
            {
              d4 = o - d2;
              d4 = Math.abs(d4);
              double d5 = 0.05D;
              boolean bool3 = d4 < d5;
              if (bool3)
              {
                localObject3 = MoatAdEventType.AD_EVT_VOLUME_CHANGE;
                k = 1;
              }
            }
          }
          if (k != 0)
          {
            localObject2 = new com/moat/analytics/mobile/inm/MoatAdEvent;
            localInteger1 = Integer.valueOf(j);
            localObject4 = Double.valueOf(d2);
            ((MoatAdEvent)localObject2).<init>((MoatAdEventType)localObject3, localInteger1, (Double)localObject4);
            dispatchEvent((MoatAdEvent)localObject2);
          }
          o = d2;
          q = 0;
          return i;
        }
        catch (Exception localException)
        {
          int j = q;
          int k = j + 1;
          q = k;
          k = 5;
          if (j < k) {
            return i;
          }
        }
      }
    }
    return false;
  }
  
  public void stopTracking()
  {
    try
    {
      Object localObject1 = new com/moat/analytics/mobile/inm/MoatAdEvent;
      Object localObject2 = MoatAdEventType.AD_EVT_COMPLETE;
      ((MoatAdEvent)localObject1).<init>((MoatAdEventType)localObject2);
      dispatchEvent((MoatAdEvent)localObject1);
      localObject1 = "[SUCCESS] ";
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      String str = a();
      ((StringBuilder)localObject2).append(str);
      str = " stopTracking succeeded for ";
      ((StringBuilder)localObject2).append(str);
      str = e();
      ((StringBuilder)localObject2).append(str);
      localObject2 = ((StringBuilder)localObject2).toString();
      p.a((String)localObject1, (String)localObject2);
      return;
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */