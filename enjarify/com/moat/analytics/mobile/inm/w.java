package com.moat.analytics.mobile.inm;

import android.os.Handler;
import android.os.Looper;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

class w
{
  private static w g;
  private static final Queue h;
  volatile w.d a;
  volatile boolean b;
  volatile boolean c;
  volatile int d;
  private long e = 1800000L;
  private long f;
  private Handler i;
  private final AtomicBoolean j;
  private volatile long k;
  private final AtomicInteger l;
  private final AtomicBoolean m;
  
  static
  {
    ConcurrentLinkedQueue localConcurrentLinkedQueue = new java/util/concurrent/ConcurrentLinkedQueue;
    localConcurrentLinkedQueue.<init>();
    h = localConcurrentLinkedQueue;
  }
  
  private w()
  {
    long l1 = 60000L;
    f = l1;
    Object localObject1 = w.d.a;
    a = ((w.d)localObject1);
    localObject1 = null;
    b = false;
    c = false;
    int n = 200;
    d = n;
    Object localObject2 = new java/util/concurrent/atomic/AtomicBoolean;
    ((AtomicBoolean)localObject2).<init>(false);
    j = ((AtomicBoolean)localObject2);
    long l2 = 0L;
    k = l2;
    localObject2 = new java/util/concurrent/atomic/AtomicInteger;
    ((AtomicInteger)localObject2).<init>(0);
    l = ((AtomicInteger)localObject2);
    localObject2 = new java/util/concurrent/atomic/AtomicBoolean;
    ((AtomicBoolean)localObject2).<init>(false);
    m = ((AtomicBoolean)localObject2);
    try
    {
      localObject1 = new android/os/Handler;
      localObject2 = Looper.getMainLooper();
      ((Handler)localObject1).<init>((Looper)localObject2);
      i = ((Handler)localObject1);
      return;
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
  }
  
  static w a()
  {
    synchronized (w.class)
    {
      w localw = g;
      if (localw == null)
      {
        localw = new com/moat/analytics/mobile/inm/w;
        localw.<init>();
        g = localw;
      }
      localw = g;
      return localw;
    }
  }
  
  private void a(long paramLong)
  {
    Object localObject = m;
    boolean bool1 = true;
    boolean bool2 = ((AtomicBoolean)localObject).compareAndSet(false, bool1);
    if (!bool2) {
      return;
    }
    p.a(3, "OnOff", this, "Performing status check.");
    localObject = new com/moat/analytics/mobile/inm/w$1;
    ((w.1)localObject).<init>(this, paramLong);
    ((Thread)localObject).start();
  }
  
  private void d()
  {
    synchronized (h)
    {
      long l1 = System.currentTimeMillis();
      Object localObject1 = h;
      localObject1 = ((Queue)localObject1).iterator();
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        Object localObject2 = ((Iterator)localObject1).next();
        localObject2 = (w.c)localObject2;
        localObject2 = a;
        long l2 = ((Long)localObject2).longValue();
        l2 = l1 - l2;
        long l3 = 60000L;
        boolean bool2 = l2 < l3;
        if (!bool2) {
          ((Iterator)localObject1).remove();
        }
      }
      Queue localQueue2 = h;
      int n = localQueue2.size();
      int i1 = 15;
      if (n >= i1)
      {
        n = 0;
        localQueue2 = null;
        for (;;)
        {
          i1 = 5;
          if (n >= i1) {
            break;
          }
          Queue localQueue3 = h;
          localQueue3.remove();
          n += 1;
        }
      }
      return;
    }
  }
  
  private void e()
  {
    Object localObject = j;
    boolean bool1 = true;
    boolean bool2 = ((AtomicBoolean)localObject).compareAndSet(false, bool1);
    if (!bool2) {
      return;
    }
    localObject = new com/moat/analytics/mobile/inm/w$2;
    ((w.2)localObject).<init>(this);
    i.postDelayed((Runnable)localObject, 60000L);
  }
  
  void a(w.b paramb)
  {
    Object localObject1 = a;
    Object localObject2 = w.d.b;
    if (localObject1 == localObject2)
    {
      paramb.b();
      return;
    }
    d();
    localObject1 = h;
    localObject2 = new com/moat/analytics/mobile/inm/w$c;
    Long localLong = Long.valueOf(System.currentTimeMillis());
    ((w.c)localObject2).<init>(this, localLong, paramb);
    ((Queue)localObject1).add(localObject2);
    e();
  }
  
  void b()
  {
    long l1 = System.currentTimeMillis();
    long l2 = k;
    l1 -= l2;
    l2 = e;
    boolean bool = l1 < l2;
    if (bool)
    {
      l1 = 0L;
      a(l1);
    }
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */