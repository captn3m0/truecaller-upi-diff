package com.moat.analytics.mobile.inm;

import android.os.Handler;
import android.os.Looper;
import com.moat.analytics.mobile.inm.a.b.a;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

class w$a
  implements Runnable
{
  private final Handler b;
  private final String c;
  private final w.e d;
  
  private w$a(w paramw, String paramString, Handler paramHandler, w.e parame)
  {
    d = parame;
    b = paramHandler;
    paramw = new java/lang/StringBuilder;
    paramw.<init>("https://z.moatads.com/");
    paramw.append(paramString);
    paramw.append("/android/");
    paramString = "3f2ae9c1894282b5e0222f0d06bbf457191f816f".substring(0, 7);
    paramw.append(paramString);
    paramw.append("/status.json");
    paramw = paramw.toString();
    c = paramw;
  }
  
  private void a()
  {
    Object localObject1 = b();
    Object localObject2 = new com/moat/analytics/mobile/inm/l;
    ((l)localObject2).<init>((String)localObject1);
    Object localObject3 = a;
    boolean bool = ((l)localObject2).a();
    b = bool;
    localObject3 = a;
    bool = ((l)localObject2).b();
    c = bool;
    localObject3 = a;
    int i = ((l)localObject2).c();
    d = i;
    localObject3 = Looper.getMainLooper();
    Handler localHandler = new android/os/Handler;
    localHandler.<init>((Looper)localObject3);
    localObject3 = new com/moat/analytics/mobile/inm/w$a$1;
    ((w.a.1)localObject3).<init>(this, (l)localObject2);
    localHandler.post((Runnable)localObject3);
    localObject2 = a;
    long l1 = System.currentTimeMillis();
    w.a((w)localObject2, l1);
    localObject2 = w.d(a);
    localObject3 = null;
    i = 1;
    ((AtomicBoolean)localObject2).compareAndSet(i, false);
    if (localObject1 == null)
    {
      localObject1 = w.e(a);
      int j = ((AtomicInteger)localObject1).incrementAndGet();
      int k = 10;
      if (j < k)
      {
        localObject1 = a;
        long l2 = w.f((w)localObject1);
        w.b((w)localObject1, l2);
      }
    }
    else
    {
      localObject1 = w.e(a);
      ((AtomicInteger)localObject1).set(0);
    }
  }
  
  private String b()
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    String str = c;
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append("?ts=");
    long l = System.currentTimeMillis();
    ((StringBuilder)localObject).append(l);
    str = "&v=2.2.0";
    ((StringBuilder)localObject).append(str);
    localObject = q.a(((StringBuilder)localObject).toString());
    try
    {
      localObject = ((a)localObject).b();
      localObject = (String)localObject;
    }
    catch (Exception localException)
    {
      localObject = null;
    }
    return (String)localObject;
  }
  
  public void run()
  {
    try
    {
      a();
    }
    catch (Exception localException)
    {
      m.a(localException);
    }
    b.removeCallbacks(this);
    Looper localLooper = Looper.myLooper();
    if (localLooper != null) {
      localLooper.quit();
    }
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.w.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */