package com.moat.analytics.mobile.inm;

import android.app.Activity;
import android.media.MediaPlayer;
import android.view.View;
import java.util.Map;

class v$d
  implements NativeVideoTracker
{
  public void changeTargetView(View paramView) {}
  
  public void dispatchEvent(MoatAdEvent paramMoatAdEvent) {}
  
  public void setActivity(Activity paramActivity) {}
  
  public void setPlayerVolume(Double paramDouble) {}
  
  public void stopTracking() {}
  
  public boolean trackVideoAd(Map paramMap, MediaPlayer paramMediaPlayer, View paramView)
  {
    return false;
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.v.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */