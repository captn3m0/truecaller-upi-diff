package com.moat.analytics.mobile.inm;

import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.Map;

class x$b
{
  private final WeakReference[] b;
  private final LinkedList c;
  private final Method d;
  
  private x$b(x paramx, Method paramMethod, Object... paramVarArgs)
  {
    paramx = new java/util/LinkedList;
    paramx.<init>();
    c = paramx;
    if (paramVarArgs == null) {
      paramVarArgs = x.a();
    }
    int i = paramVarArgs.length;
    paramx = new WeakReference[i];
    int j = paramVarArgs.length;
    int k = 0;
    label113:
    int n;
    for (int m = 0; k < j; m = n)
    {
      Object localObject = paramVarArgs[k];
      boolean bool = localObject instanceof Map;
      if (!bool)
      {
        bool = localObject instanceof Integer;
        if (!bool)
        {
          bool = localObject instanceof Double;
          if (!bool) {
            break label113;
          }
        }
      }
      LinkedList localLinkedList = c;
      localLinkedList.add(localObject);
      n = m + 1;
      WeakReference localWeakReference = new java/lang/ref/WeakReference;
      localWeakReference.<init>(localObject);
      paramx[m] = localWeakReference;
      k += 1;
    }
    b = paramx;
    d = paramMethod;
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.x.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */