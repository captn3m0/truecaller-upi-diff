package com.moat.analytics.mobile.inm;

import android.app.Application;
import android.os.Handler;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

abstract class c
  extends b
{
  static final MoatAdEventType[] f;
  final Map g;
  final Handler h;
  Map i;
  WeakReference j;
  WeakReference k;
  private boolean l;
  private Double m;
  private final g n;
  private final String o;
  
  static
  {
    MoatAdEventType[] arrayOfMoatAdEventType = new MoatAdEventType[3];
    MoatAdEventType localMoatAdEventType = MoatAdEventType.AD_EVT_FIRST_QUARTILE;
    arrayOfMoatAdEventType[0] = localMoatAdEventType;
    localMoatAdEventType = MoatAdEventType.AD_EVT_MID_POINT;
    arrayOfMoatAdEventType[1] = localMoatAdEventType;
    localMoatAdEventType = MoatAdEventType.AD_EVT_THIRD_QUARTILE;
    arrayOfMoatAdEventType[2] = localMoatAdEventType;
    f = arrayOfMoatAdEventType;
  }
  
  c(String paramString)
  {
    super(null, false, true);
    p.a(3, "BaseVideoTracker", this, "Initializing.");
    o = paramString;
    g localg = new com/moat/analytics/mobile/inm/g;
    Application localApplication = a.a();
    g.a locala = g.a.b;
    localg.<init>(localApplication, locala);
    n = localg;
    n.a(paramString);
    paramString = n.b;
    super.a(paramString);
    paramString = n.a;
    super.a(paramString);
    paramString = new java/util/HashMap;
    paramString.<init>();
    g = paramString;
    paramString = new android/os/Handler;
    paramString.<init>();
    h = paramString;
    l = false;
    paramString = Double.valueOf(1.0D);
    m = paramString;
  }
  
  private void b(MoatAdEvent paramMoatAdEvent)
  {
    Object localObject1 = a(paramMoatAdEvent);
    int i1 = 1;
    Object localObject2 = new Object[i1];
    Object localObject3 = ((JSONObject)localObject1).toString();
    localObject2[0] = localObject3;
    Object localObject4 = String.format("Received event: %s", (Object[])localObject2);
    int i2 = 3;
    p.a(i2, "BaseVideoTracker", this, (String)localObject4);
    localObject4 = new java/lang/StringBuilder;
    ((StringBuilder)localObject4).<init>();
    localObject2 = a();
    ((StringBuilder)localObject4).append((String)localObject2);
    localObject3 = new Object[i1];
    String str = ((JSONObject)localObject1).toString();
    localObject3[0] = str;
    localObject2 = String.format(" Received event: %s", (Object[])localObject3);
    ((StringBuilder)localObject4).append((String)localObject2);
    localObject4 = ((StringBuilder)localObject4).toString();
    p.a("[SUCCESS] ", (String)localObject4);
    Object localObject5 = a;
    localObject4 = n.c;
    ((j)localObject5).a((String)localObject4, (JSONObject)localObject1);
    paramMoatAdEvent = d;
    localObject1 = MoatAdEventType.AD_EVT_COMPLETE;
    if (paramMoatAdEvent != localObject1)
    {
      localObject1 = MoatAdEventType.AD_EVT_STOPPED;
      if (paramMoatAdEvent != localObject1)
      {
        localObject1 = MoatAdEventType.AD_EVT_SKIPPED;
        if (paramMoatAdEvent != localObject1) {
          return;
        }
      }
    }
    localObject1 = g;
    localObject5 = Integer.valueOf(i1);
    ((Map)localObject1).put(paramMoatAdEvent, localObject5);
    h();
  }
  
  private void j()
  {
    Object localObject1 = g();
    Object localObject2 = ((Map)localObject1).get("width");
    Object localObject3 = localObject2;
    localObject3 = (Integer)localObject2;
    localObject2 = ((Map)localObject1).get("height");
    Object localObject4 = localObject2;
    localObject4 = (Integer)localObject2;
    localObject1 = ((Map)localObject1).get("duration");
    Object localObject5 = localObject1;
    localObject5 = (Integer)localObject1;
    localObject2 = Locale.ROOT;
    int i1 = 3;
    Object localObject6 = new Object[i1];
    localObject6[0] = localObject4;
    localObject6[1] = localObject3;
    localObject6[2] = localObject5;
    localObject2 = String.format((Locale)localObject2, "Player metadata: height = %d, width = %d, duration = %d", (Object[])localObject6);
    p.a(i1, "BaseVideoTracker", this, (String)localObject2);
    g localg = n;
    String str = o;
    localObject6 = i;
    localg.a(str, (Map)localObject6, (Integer)localObject3, (Integer)localObject4, (Integer)localObject5);
    localObject1 = (View)k.get();
    super.changeTargetView((View)localObject1);
    super.b();
  }
  
  JSONObject a(MoatAdEvent paramMoatAdEvent)
  {
    Object localObject1 = c;
    double d1 = ((Double)localObject1).doubleValue();
    boolean bool = Double.isNaN(d1);
    if (bool) {
      try
      {
        d1 = s.a();
        localObject1 = Double.valueOf(d1);
        c = ((Double)localObject1);
      }
      catch (Exception localException)
      {
        d1 = 1.0D;
        localObject1 = Double.valueOf(d1);
        c = ((Double)localObject1);
      }
    }
    Object localObject2 = Locale.ROOT;
    int i1 = 1;
    Object[] arrayOfObject1 = new Object[i1];
    Double localDouble = c;
    arrayOfObject1[0] = localDouble;
    localObject2 = String.format((Locale)localObject2, "adVolume before adjusting for player volume %f", arrayOfObject1);
    int i2 = 3;
    p.a(i2, "BaseVideoTracker", this, (String)localObject2);
    d1 = c.doubleValue();
    double d2 = m.doubleValue();
    localObject1 = Double.valueOf(d1 * d2);
    c = ((Double)localObject1);
    localObject2 = Locale.ROOT;
    Object[] arrayOfObject2 = new Object[i1];
    localDouble = c;
    arrayOfObject2[0] = localDouble;
    localObject2 = String.format((Locale)localObject2, "adVolume after adjusting for player volume %f", arrayOfObject2);
    p.a(i2, "BaseVideoTracker", this, (String)localObject2);
    localObject1 = new org/json/JSONObject;
    paramMoatAdEvent = paramMoatAdEvent.a();
    ((JSONObject)localObject1).<init>(paramMoatAdEvent);
    return (JSONObject)localObject1;
  }
  
  boolean a(Integer paramInteger1, Integer paramInteger2)
  {
    int i1 = paramInteger2.intValue();
    int i2 = paramInteger1.intValue();
    i1 -= i2;
    i2 = Math.abs(i1);
    int i3 = paramInteger2.intValue();
    double d1 = i3;
    Double.isNaN(d1);
    d1 *= 0.05D;
    double d2 = 750.0D;
    d1 = Math.min(d2, d1);
    double d3 = i2;
    boolean bool = d3 < d1;
    return !bool;
  }
  
  public boolean a(Map paramMap, Object paramObject, View paramView)
  {
    boolean bool1 = false;
    int i1 = 3;
    try
    {
      boolean bool2 = e;
      int i2 = 1;
      String str;
      Object localObject1;
      Object localObject2;
      if (bool2)
      {
        str = "BaseVideoTracker";
        localObject1 = "trackVideoAd already called";
        p.a(i1, str, this, (String)localObject1);
        str = "[ERROR] ";
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        localObject2 = a();
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject2 = " trackVideoAd can't be called twice";
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject1 = ((StringBuilder)localObject1).toString();
        p.a(str, (String)localObject1);
        bool2 = false;
        str = null;
      }
      else
      {
        bool2 = true;
      }
      if (paramMap == null)
      {
        str = "BaseVideoTracker";
        localObject1 = "trackVideoAd received null adIds object. Not tracking.";
        p.a(i1, str, this, (String)localObject1);
        str = "[ERROR] ";
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        localObject2 = a();
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject2 = " trackVideoAd failed, received null adIds object";
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject1 = ((StringBuilder)localObject1).toString();
        p.a(str, (String)localObject1);
        bool2 = false;
        str = null;
      }
      if (paramView == null)
      {
        localObject1 = "BaseVideoTracker";
        localObject2 = "trackVideoAd received null video view instance";
        p.a(i1, (String)localObject1, this, (String)localObject2);
      }
      if (paramObject == null)
      {
        str = "BaseVideoTracker";
        localObject1 = "trackVideoAd received null ad instance. Not tracking.";
        p.a(i1, str, this, (String)localObject1);
        str = "[ERROR] ";
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        localObject2 = a();
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject2 = " trackVideoAd failed, received null ad instance";
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject1 = ((StringBuilder)localObject1).toString();
        p.a(str, (String)localObject1);
        bool2 = false;
        str = null;
      }
      if (bool2)
      {
        localObject1 = "BaseVideoTracker";
        localObject2 = "trackVideoAd tracking ids: %s | ad: %s | view: %s";
        Object localObject3 = new Object[i1];
        Object localObject4 = new org/json/JSONObject;
        ((JSONObject)localObject4).<init>(paramMap);
        localObject4 = ((JSONObject)localObject4).toString();
        localObject3[0] = localObject4;
        localObject4 = paramObject.toString();
        localObject3[i2] = localObject4;
        if (paramView != null)
        {
          localObject4 = new java/lang/StringBuilder;
          ((StringBuilder)localObject4).<init>();
          Object localObject5 = paramView.getClass();
          localObject5 = ((Class)localObject5).getSimpleName();
          ((StringBuilder)localObject4).append((String)localObject5);
          localObject5 = "@";
          ((StringBuilder)localObject4).append((String)localObject5);
          i3 = paramView.hashCode();
          ((StringBuilder)localObject4).append(i3);
          localObject4 = ((StringBuilder)localObject4).toString();
        }
        else
        {
          localObject4 = "null";
        }
        int i3 = 2;
        localObject3[i3] = localObject4;
        localObject2 = String.format((String)localObject2, (Object[])localObject3);
        p.a(i1, (String)localObject1, this, (String)localObject2);
        localObject1 = "[SUCCESS] ";
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        localObject3 = a();
        ((StringBuilder)localObject2).append((String)localObject3);
        localObject3 = " trackVideoAd succeeded with ids: %s | ad: %s | view: %s";
        localObject4 = new Object[i1];
        Object localObject6 = new org/json/JSONObject;
        ((JSONObject)localObject6).<init>(paramMap);
        localObject6 = ((JSONObject)localObject6).toString();
        localObject4[0] = localObject6;
        localObject6 = paramObject.toString();
        localObject4[i2] = localObject6;
        if (paramView != null)
        {
          localObject7 = new java/lang/StringBuilder;
          ((StringBuilder)localObject7).<init>();
          localObject6 = paramView.getClass();
          localObject6 = ((Class)localObject6).getSimpleName();
          ((StringBuilder)localObject7).append((String)localObject6);
          localObject6 = "@";
          ((StringBuilder)localObject7).append((String)localObject6);
          int i4 = paramView.hashCode();
          ((StringBuilder)localObject7).append(i4);
          localObject7 = ((StringBuilder)localObject7).toString();
        }
        else
        {
          localObject7 = "null";
        }
        localObject4[i3] = localObject7;
        Object localObject7 = String.format((String)localObject3, (Object[])localObject4);
        ((StringBuilder)localObject2).append((String)localObject7);
        localObject7 = ((StringBuilder)localObject2).toString();
        p.a((String)localObject1, (String)localObject7);
        i = paramMap;
        paramMap = new java/lang/ref/WeakReference;
        paramMap.<init>(paramObject);
        j = paramMap;
        paramMap = new java/lang/ref/WeakReference;
        paramMap.<init>(paramView);
        k = paramMap;
        j();
      }
      bool1 = bool2;
    }
    catch (Exception paramMap)
    {
      m.a(paramMap);
    }
    paramMap = "BaseVideoTracker";
    paramObject = new java/lang/StringBuilder;
    paramView = "Attempt to start tracking ad was ";
    ((StringBuilder)paramObject).<init>(paramView);
    if (bool1) {
      paramView = "";
    } else {
      paramView = "un";
    }
    ((StringBuilder)paramObject).append(paramView);
    ((StringBuilder)paramObject).append("successful.");
    paramObject = ((StringBuilder)paramObject).toString();
    p.a(i1, paramMap, this, (String)paramObject);
    return bool1;
  }
  
  public void changeTargetView(View paramView)
  {
    if (paramView != null)
    {
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      String str1 = paramView.getClass().getSimpleName();
      ((StringBuilder)localObject).append(str1);
      str1 = "@";
      ((StringBuilder)localObject).append(str1);
      i1 = paramView.hashCode();
      ((StringBuilder)localObject).append(i1);
      localObject = ((StringBuilder)localObject).toString();
    }
    else
    {
      localObject = "null";
    }
    int i1 = 3;
    String str2 = "BaseVideoTracker";
    String str3 = "changing view to ";
    Object localObject = String.valueOf(localObject);
    localObject = str3.concat((String)localObject);
    p.a(i1, str2, this, (String)localObject);
    localObject = new java/lang/ref/WeakReference;
    ((WeakReference)localObject).<init>(paramView);
    k = ((WeakReference)localObject);
    try
    {
      super.changeTargetView(paramView);
      return;
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
  }
  
  public void dispatchEvent(MoatAdEvent paramMoatAdEvent)
  {
    try
    {
      b(paramMoatAdEvent);
      return;
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
  }
  
  protected abstract Map g();
  
  void h()
  {
    boolean bool = l;
    if (!bool)
    {
      c.1 local1 = new com/moat/analytics/mobile/inm/c$1;
      local1.<init>(this);
      Handler localHandler = h;
      long l1 = 500L;
      localHandler.postDelayed(local1, l1);
      bool = true;
      l = bool;
    }
  }
  
  boolean i()
  {
    Map localMap = g;
    MoatAdEventType localMoatAdEventType = MoatAdEventType.AD_EVT_COMPLETE;
    boolean bool = localMap.containsKey(localMoatAdEventType);
    if (!bool)
    {
      localMap = g;
      localMoatAdEventType = MoatAdEventType.AD_EVT_STOPPED;
      bool = localMap.containsKey(localMoatAdEventType);
      if (!bool)
      {
        localMap = g;
        localMoatAdEventType = MoatAdEventType.AD_EVT_SKIPPED;
        bool = localMap.containsKey(localMoatAdEventType);
        if (!bool) {
          return false;
        }
      }
    }
    return true;
  }
  
  public void setPlayerVolume(Double paramDouble)
  {
    Object localObject1 = m;
    boolean bool = paramDouble.equals(localObject1);
    if (!bool)
    {
      int i1 = 3;
      Object localObject2 = Locale.ROOT;
      String str = "player volume changed to %f ";
      int i2 = 1;
      Object[] arrayOfObject = new Object[i2];
      arrayOfObject[0] = paramDouble;
      localObject2 = String.format((Locale)localObject2, str, arrayOfObject);
      p.a(i1, "BaseVideoTracker", this, (String)localObject2);
      m = paramDouble;
      paramDouble = new com/moat/analytics/mobile/inm/MoatAdEvent;
      localObject1 = MoatAdEventType.AD_EVT_VOLUME_CHANGE;
      Integer localInteger = MoatAdEvent.a;
      paramDouble.<init>((MoatAdEventType)localObject1, localInteger);
      dispatchEvent(paramDouble);
    }
  }
  
  public void stopTracking()
  {
    try
    {
      boolean bool = super.c();
      String str1;
      if (bool) {
        str1 = "[SUCCESS] ";
      } else {
        str1 = "[ERROR] ";
      }
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      String str2 = a();
      localStringBuilder.append(str2);
      str2 = " stopTracking ";
      localStringBuilder.append(str2);
      if (bool) {
        str3 = "succeeded";
      } else {
        str3 = "failed";
      }
      localStringBuilder.append(str3);
      String str3 = " for ";
      localStringBuilder.append(str3);
      str3 = e();
      localStringBuilder.append(str3);
      str3 = localStringBuilder.toString();
      p.a(str1, str3);
      h();
      return;
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */