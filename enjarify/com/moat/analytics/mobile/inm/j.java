package com.moat.analytics.mobile.inm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Build.VERSION;
import android.support.v4.content.d;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

class j
{
  boolean a = false;
  private int b = 0;
  private boolean c = false;
  private boolean d = false;
  private final WeakReference e;
  private final Map f;
  private final LinkedList g;
  private final AtomicBoolean h;
  private final long i;
  private final s.a j;
  private final List k;
  private final j.a l;
  private final BroadcastReceiver m;
  private final BroadcastReceiver n;
  
  j(WebView paramWebView, j.a parama)
  {
    Object localObject1 = new java/util/concurrent/atomic/AtomicBoolean;
    ((AtomicBoolean)localObject1).<init>(false);
    h = ((AtomicBoolean)localObject1);
    Object localObject2 = new com/moat/analytics/mobile/inm/j$2;
    ((j.2)localObject2).<init>(this);
    m = ((BroadcastReceiver)localObject2);
    localObject2 = new com/moat/analytics/mobile/inm/j$3;
    ((j.3)localObject2).<init>(this);
    n = ((BroadcastReceiver)localObject2);
    localObject2 = new java/lang/ref/WeakReference;
    ((WeakReference)localObject2).<init>(paramWebView);
    e = ((WeakReference)localObject2);
    l = parama;
    paramWebView = new com/moat/analytics/mobile/inm/s$a;
    paramWebView.<init>();
    j = paramWebView;
    paramWebView = new java/util/LinkedList;
    paramWebView.<init>();
    g = paramWebView;
    paramWebView = new java/util/ArrayList;
    paramWebView.<init>();
    k = paramWebView;
    paramWebView = new java/util/WeakHashMap;
    paramWebView.<init>();
    f = paramWebView;
    long l1 = System.currentTimeMillis();
    i = l1;
    paramWebView = new android/content/IntentFilter;
    paramWebView.<init>("UPDATE_METADATA");
    parama = new android/content/IntentFilter;
    parama.<init>("UPDATE_VIEW_INFO");
    localObject2 = d.a(s.c());
    localObject1 = m;
    ((d)localObject2).a((BroadcastReceiver)localObject1, paramWebView);
    paramWebView = d.a(s.c());
    localObject2 = n;
    paramWebView.a((BroadcastReceiver)localObject2, parama);
    try
    {
      boolean bool = b();
      int i1 = 3;
      localObject2 = "JavaScriptBridge";
      if (bool) {
        paramWebView = "bridge installed";
      } else {
        paramWebView = "bridge not installed";
      }
      p.a(i1, (String)localObject2, this, paramWebView);
      return;
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
  }
  
  private boolean a(WebView paramWebView, String paramString)
  {
    int i1 = 6;
    if (paramWebView == null) {
      paramWebView = "JavaScriptBridge";
    }
    for (String str = "WebView is null. Can't ";; str = "JavaScript is not enabled in the given WebView. Can't ")
    {
      paramString = String.valueOf(paramString);
      paramString = str.concat(paramString);
      p.a(i1, paramWebView, this, paramString);
      return false;
      paramWebView = paramWebView.getSettings();
      boolean bool = paramWebView.getJavaScriptEnabled();
      if (bool) {
        break;
      }
      paramWebView = "JavaScriptBridge";
    }
    return true;
  }
  
  private boolean b()
  {
    WebView localWebView = h();
    if (localWebView != null)
    {
      localWebView = h();
      localObject = "installBridge";
      bool = a(localWebView, (String)localObject);
      if (!bool) {
        return false;
      }
    }
    boolean bool = true;
    a = bool;
    Object localObject = i.a();
    Context localContext = s.c();
    ((i)localObject).a(localContext, this);
    return bool;
  }
  
  private void c()
  {
    Object localObject1 = w.a();
    try
    {
      localObject1 = a;
      Object localObject2 = w.d.a;
      if (localObject1 == localObject2) {
        return;
      }
      boolean bool1 = d;
      boolean bool2 = true;
      if (!bool1)
      {
        int i1 = 3;
        str1 = "JavaScriptBridge";
        str2 = "Ready for communication (setting environment variables).";
        p.a(i1, str1, this, str2);
        d = bool2;
      }
      localObject1 = "javascript:(function(e,k){function l(){function f(a){var b=a.c,c=a.a,f=a.b;a=a.f;var d=[];if(c)b[c]&&d.push(b[c].fn[0]);else for(key in b)if(b[key])for(var g=0,e=b[key].fn.length;g<e;g++)d.push(b[key].fn[g]);g=0;for(e=d.length;g<e;g++){var h=d[g];if('function'===typeof h)try{f?h(f):h()}catch(k){}a&&delete b[c]}}function d(a,b,c){'function'===typeof a&&(b===kuea&&c[b]?c[b].fn.push(a):c[b]={ts:+new Date,fn:[a]})}kuea=+new Date;iymv={};briz=!1;ewat=+new Date;bnkr=[];bjmk={};dptk={};uqaj={};ryup={};yhgt={};csif={};this.g=function(a){this.namespace=a.namespace;this.version=a.version;this.appName=a.appName;this.deviceOS=a.deviceOS;this.isNative=a.isNative;this.versionHash=a.versionHash;this.aqzx=a.aqzx;this.appId=a.appId};this.nvsj=function(a){briz||(d(a,ewat,iymv),briz=!0)};this.bpsy=function(a,b){var c=b||kuea;c!==kuea&&bjmk[c]||d(a,c,bjmk)};this.qmrv=function(a,b){var c=b||kuea;c!==kuea&&uqaj[c]||d(a,c,uqaj)};this.lgpr=function(a,b){d(a,b||kuea,yhgt)};this.hgen=function(a,b){d(a,b||kuea,csif)};this.xrnk=function(a){delete yhgt[a||kuea]};this.vgft=function(a){return dptk[a||kuea]||!1};this.lkpu=function(a){return ryup[a||kuea]||!1};this.crts=function(a){var b={c:iymv,b:a,a:ewat};briz?f(b):bnkr.push(a)};this.mqjh=function(a){var b=a||kuea;dptk[b]=!0;var c={c:bjmk,f:!0};b!==kuea&&(c.b=a,c.a=a);f(c)};this.egpw=function(a){var b=a||kuea;ryup[b]=!0;var c={c:uqaj,f:!0};b!==kuea&&(c.b=a,c.a=a);f(c)};this.sglu=function(a){var b={c:yhgt,b:a.event||a,f:!1};(a.adKey||kuea)!==kuea&&(b.a=a.adKey);f(b);return 0<Object.keys(yhgt).length};this.ucbx=function(a){f({a:a.adKey||kuea,c:csif,b:a.event,f:!1})}}'undefined'===typeof e.MoatMAK&&(e.MoatMAK=new l,e.MoatMAK.g(k),e.__zMoatInit__=!0)})(window,%s);";
      localObject2 = new Object[bool2];
      String str1 = null;
      String str2 = i();
      localObject2[0] = str2;
      localObject1 = String.format((String)localObject1, (Object[])localObject2);
      bool2 = g();
      if (bool2)
      {
        localObject2 = h();
        ((WebView)localObject2).loadUrl((String)localObject1);
      }
      return;
    }
    catch (Exception localException)
    {
      p.a("JavaScriptBridge", this, "Failed to initialize communication (did not set environment variables).", localException);
    }
  }
  
  private void d()
  {
    try
    {
      Object localObject1 = w.a();
      localObject1 = a;
      w.d locald = w.d.a;
      if (localObject1 == locald) {
        return;
      }
      localObject1 = e;
      int i1 = 3;
      Object localObject2;
      if (localObject1 != null)
      {
        boolean bool1 = g();
        if (bool1)
        {
          bool1 = c;
          if (bool1)
          {
            localObject1 = h();
            localObject1 = ((WebView)localObject1).getUrl();
            if (localObject1 == null) {}
          }
          else
          {
            localObject1 = h();
            localObject1 = ((WebView)localObject1).getUrl();
            boolean bool2 = true;
            if (localObject1 != null) {
              c = bool2;
            }
            localObject1 = f;
            localObject1 = ((Map)localObject1).entrySet();
            localObject1 = ((Set)localObject1).iterator();
            for (;;)
            {
              boolean bool3 = ((Iterator)localObject1).hasNext();
              if (!bool3) {
                break;
              }
              localObject2 = ((Iterator)localObject1).next();
              localObject2 = (Map.Entry)localObject2;
              localObject2 = ((Map.Entry)localObject2).getKey();
              localObject2 = (b)localObject2;
              Object localObject3;
              Object localObject4;
              if (localObject2 != null)
              {
                localObject3 = ((b)localObject2).d();
                if (localObject3 != null) {}
              }
              else
              {
                localObject3 = "JavaScriptBridge";
                localObject4 = "Tracker has no subject";
                p.a(i1, (String)localObject3, this, (String)localObject4);
                if (localObject2 == null) {
                  break label356;
                }
                bool4 = c;
                if (!bool4) {
                  break label356;
                }
              }
              boolean bool4 = d;
              if (bool4)
              {
                localObject3 = "javascript: MoatMAK.mqjh(\"%s\")";
                localObject4 = new Object[bool2];
                String str = b;
                localObject4[0] = str;
                localObject3 = String.format((String)localObject3, (Object[])localObject4);
                f((String)localObject3);
                localObject2 = ((b)localObject2).f();
                localObject3 = "javascript: MoatMAK.sglu(%s)";
                localObject4 = new Object[bool2];
                localObject4[0] = localObject2;
                localObject2 = String.format((String)localObject3, (Object[])localObject4);
                int i2 = Build.VERSION.SDK_INT;
                int i3 = 19;
                if (i2 >= i3)
                {
                  localObject3 = h();
                  localObject4 = new com/moat/analytics/mobile/inm/j$1;
                  ((j.1)localObject4).<init>(this);
                  ((WebView)localObject3).evaluateJavascript((String)localObject2, (ValueCallback)localObject4);
                }
                else
                {
                  localObject3 = h();
                  ((WebView)localObject3).loadUrl((String)localObject2);
                  continue;
                  label356:
                  c((b)localObject2);
                }
              }
            }
            return;
          }
        }
      }
      localObject1 = e;
      if (localObject1 == null) {
        localObject1 = "JavaScriptBridge";
      }
      for (Object localObject5 = "WebView ref became null, stopping tracking loop";; localObject5 = ((StringBuilder)localObject5).toString())
      {
        p.a(i1, (String)localObject1, this, (String)localObject5);
        break;
        localObject1 = "JavaScriptBridge";
        localObject5 = new java/lang/StringBuilder;
        localObject2 = "WebView became null";
        ((StringBuilder)localObject5).<init>((String)localObject2);
        localObject2 = h();
        if (localObject2 == null) {
          localObject2 = "";
        } else {
          localObject2 = "based on null url";
        }
        ((StringBuilder)localObject5).append((String)localObject2);
        localObject2 = ", stopping tracking loop";
        ((StringBuilder)localObject5).append((String)localObject2);
      }
      f();
      return;
    }
    catch (Exception localException)
    {
      m.a(localException);
      f();
    }
  }
  
  private void d(b paramb)
  {
    Object localObject = "JavaScriptBridge";
    String str = "Stopping view update loop";
    int i1 = 3;
    p.a(i1, (String)localObject, this, str);
    if (paramb != null)
    {
      localObject = i.a();
      ((i)localObject).a(paramb);
    }
  }
  
  private void d(String paramString)
  {
    List localList = k;
    int i1 = localList.size();
    int i2 = 50;
    if (i1 >= i2)
    {
      localList = k;
      i2 = 0;
      int i3 = 25;
      localList = localList.subList(0, i3);
      localList.clear();
    }
    k.add(paramString);
  }
  
  private void e()
  {
    p.a(3, "JavaScriptBridge", this, "Stopping metadata reporting loop");
    i.a().a(this);
    d locald = d.a(s.c());
    BroadcastReceiver localBroadcastReceiver = m;
    locald.a(localBroadcastReceiver);
  }
  
  private boolean e(String paramString)
  {
    boolean bool = a;
    if (!bool)
    {
      paramString = String.valueOf(paramString);
      paramString = "Bridge is not installed in the given WebView. Can't ".concat(paramString);
      p.a(6, "JavaScriptBridge", this, paramString);
      return false;
    }
    return true;
  }
  
  private void f()
  {
    Object localObject1 = "Cleaning up";
    int i1 = 3;
    p.a(i1, "JavaScriptBridge", this, (String)localObject1);
    e();
    Object localObject2 = f.entrySet().iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject2).hasNext();
      if (!bool) {
        break;
      }
      localObject1 = (b)((Map.Entry)((Iterator)localObject2).next()).getKey();
      d((b)localObject1);
    }
    f.clear();
    localObject2 = d.a(s.c());
    localObject1 = n;
    ((d)localObject2).a((BroadcastReceiver)localObject1);
  }
  
  private void f(String paramString)
  {
    boolean bool = g();
    if (bool)
    {
      WebView localWebView = h();
      localWebView.loadUrl(paramString);
    }
  }
  
  private boolean g()
  {
    WebView localWebView = h();
    return localWebView != null;
  }
  
  private WebView h()
  {
    return (WebView)e.get();
  }
  
  private String i()
  {
    try
    {
      HashMap localHashMap = new java/util/HashMap;
      localHashMap.<init>();
      Object localObject1 = j;
      localObject1 = ((s.a)localObject1).a();
      Object localObject2 = j;
      localObject2 = ((s.a)localObject2).b();
      int i1 = Build.VERSION.SDK_INT;
      String str1 = Integer.toString(i1);
      String str2 = s.b();
      Object localObject3 = l;
      Object localObject4 = j.a.a;
      if (localObject3 == localObject4) {
        localObject3 = "0";
      } else {
        localObject3 = "1";
      }
      localObject4 = "versionHash";
      String str3 = "3f2ae9c1894282b5e0222f0d06bbf457191f816f";
      localHashMap.put(localObject4, str3);
      localObject4 = "appName";
      localHashMap.put(localObject4, localObject1);
      localObject1 = "namespace";
      localObject4 = "INM";
      localHashMap.put(localObject1, localObject4);
      localObject1 = "version";
      localObject4 = "2.2.0";
      localHashMap.put(localObject1, localObject4);
      localObject1 = "deviceOS";
      localHashMap.put(localObject1, str1);
      localObject1 = "isNative";
      localHashMap.put(localObject1, localObject3);
      localObject1 = "appId";
      localHashMap.put(localObject1, localObject2);
      if (str2 != null)
      {
        localObject1 = "aqzx";
        localHashMap.put(localObject1, str2);
      }
      localObject1 = new org/json/JSONObject;
      ((JSONObject)localObject1).<init>(localHashMap);
      return ((JSONObject)localObject1).toString();
    }
    catch (Exception localException) {}
    return "{}";
  }
  
  void a()
  {
    p.a(3, "JavaScriptBridge", this, "webViewReady");
    Object localObject = h;
    boolean bool1 = false;
    String str = null;
    boolean bool2 = true;
    ((AtomicBoolean)localObject).compareAndSet(false, bool2);
    e();
    localObject = k.iterator();
    for (;;)
    {
      bool1 = ((Iterator)localObject).hasNext();
      if (!bool1) {
        break;
      }
      str = (String)((Iterator)localObject).next();
      f(str);
    }
    k.clear();
  }
  
  void a(b paramb)
  {
    if (paramb != null)
    {
      int i1 = 3;
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>("adding tracker");
      String str1 = b;
      ((StringBuilder)localObject).append(str1);
      localObject = ((StringBuilder)localObject).toString();
      p.a(i1, "JavaScriptBridge", this, (String)localObject);
      Map localMap = f;
      String str2 = "";
      localMap.put(paramb, str2);
    }
  }
  
  void a(String paramString)
  {
    int i1 = 1;
    Object[] arrayOfObject = new Object[i1];
    arrayOfObject[0] = paramString;
    paramString = String.format("javascript: MoatMAK.crts(%s)", arrayOfObject);
    AtomicBoolean localAtomicBoolean = h;
    boolean bool = localAtomicBoolean.get();
    if (bool)
    {
      f(paramString);
      return;
    }
    d(paramString);
  }
  
  void a(String paramString, JSONObject paramJSONObject)
  {
    paramJSONObject = paramJSONObject.toString();
    AtomicBoolean localAtomicBoolean = h;
    boolean bool = localAtomicBoolean.get();
    if (bool)
    {
      bool = g();
      if (bool)
      {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = paramString;
        arrayOfObject[1] = paramJSONObject;
        paramString = String.format("javascript:%s.dispatchEvent(%s);", arrayOfObject);
        f(paramString);
        return;
      }
    }
    g.add(paramJSONObject);
  }
  
  void b(String paramString)
  {
    String str = String.valueOf(paramString);
    Object localObject = "markUserInteractionEvent:".concat(str);
    p.a(3, "JavaScriptBridge", this, (String)localObject);
    int i1 = 1;
    localObject = new Object[i1];
    str = null;
    localObject[0] = paramString;
    paramString = String.format("javascript: MoatMAK.ucbx(%s)", (Object[])localObject);
    AtomicBoolean localAtomicBoolean = h;
    boolean bool = localAtomicBoolean.get();
    if (bool)
    {
      f(paramString);
      return;
    }
    d(paramString);
  }
  
  boolean b(b paramb)
  {
    try
    {
      boolean bool = g();
      if (bool)
      {
        Object localObject1 = h();
        String str1 = "startTracking";
        bool = a((WebView)localObject1, str1);
        if (bool)
        {
          localObject1 = "startTracking";
          bool = e((String)localObject1);
          if (bool)
          {
            localObject1 = paramb.d();
            int i1 = 3;
            if (localObject1 == null)
            {
              bool = c;
              if (!bool)
              {
                paramb = "JavaScriptBridge";
                localObject1 = "Tracker subject is null, won't start tracking";
                p.a(i1, paramb, this, (String)localObject1);
                return false;
              }
              localObject1 = "JavaScriptBridge";
              localObject2 = "Tracker subject is null at start";
              p.a(i1, (String)localObject1, this, (String)localObject2);
            }
            localObject1 = "JavaScriptBridge";
            Object localObject2 = new java/lang/StringBuilder;
            String str2 = "Starting tracking on tracker";
            ((StringBuilder)localObject2).<init>(str2);
            str2 = b;
            ((StringBuilder)localObject2).append(str2);
            localObject2 = ((StringBuilder)localObject2).toString();
            p.a(i1, (String)localObject1, this, (String)localObject2);
            localObject1 = "javascript: MoatMAK.mqjh(\"%s\")";
            i1 = 1;
            localObject2 = new Object[i1];
            str2 = b;
            localObject2[0] = str2;
            localObject1 = String.format((String)localObject1, (Object[])localObject2);
            f((String)localObject1);
            localObject1 = i.a();
            localObject2 = s.c();
            ((i)localObject1).a((Context)localObject2, paramb);
            return i1;
          }
        }
      }
      return false;
    }
    catch (Exception paramb)
    {
      p.a("JavaScriptBridge", this, "Failed to initialize impression start.", paramb);
    }
    return false;
  }
  
  void c(String paramString)
  {
    p.a(3, "JavaScriptBridge", this, "flushDispatchQueue");
    Object localObject1 = h;
    boolean bool1 = true;
    ((AtomicBoolean)localObject1).compareAndSet(false, bool1);
    localObject1 = g;
    int i1 = ((LinkedList)localObject1).size();
    int i2 = 200;
    Object localObject2;
    Object localObject3;
    Object localObject4;
    boolean bool2;
    if (i1 >= i2)
    {
      localObject1 = new java/util/LinkedList;
      ((LinkedList)localObject1).<init>();
      int i3 = 0;
      localObject2 = null;
      for (;;)
      {
        i4 = 10;
        if (i3 >= i4) {
          break;
        }
        localObject3 = (String)g.removeFirst();
        ((LinkedList)localObject1).addFirst(localObject3);
        i3 += 1;
      }
      localObject2 = g;
      i3 = Math.min(((LinkedList)localObject2).size() / i2, i4) + i2;
      int i4 = g.size();
      i3 = Math.min(i3, i4);
      i4 = 0;
      localObject3 = null;
      while (i4 < i3)
      {
        localObject4 = g;
        ((LinkedList)localObject4).removeFirst();
        i4 += 1;
      }
      localObject1 = ((LinkedList)localObject1).iterator();
      for (;;)
      {
        bool2 = ((Iterator)localObject1).hasNext();
        if (!bool2) {
          break;
        }
        localObject2 = (String)((Iterator)localObject1).next();
        localObject3 = g;
        ((LinkedList)localObject3).addFirst(localObject2);
      }
    }
    i1 = 0;
    localObject1 = null;
    for (;;)
    {
      localObject2 = g;
      bool2 = ((LinkedList)localObject2).isEmpty();
      if ((bool2) || (i1 >= i2)) {
        break;
      }
      i1 += 1;
      localObject2 = "javascript:%s.dispatchMany([%s])";
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      int i5 = 1;
      for (;;)
      {
        Object localObject5 = g;
        boolean bool3 = ((LinkedList)localObject5).isEmpty();
        if ((bool3) || (i1 >= i2)) {
          break;
        }
        i1 += 1;
        localObject5 = (String)g.getFirst();
        int i6 = ((StringBuilder)localObject3).length();
        int i7 = ((String)localObject5).length();
        i6 += i7;
        i7 = 2000;
        if (i6 > i7) {
          break;
        }
        Object localObject6 = g;
        ((LinkedList)localObject6).removeFirst();
        if (i5 != 0)
        {
          i5 = 0;
          localObject4 = null;
        }
        else
        {
          localObject6 = ",";
          ((StringBuilder)localObject3).append((String)localObject6);
        }
        ((StringBuilder)localObject3).append((String)localObject5);
      }
      i5 = 2;
      localObject4 = new Object[i5];
      localObject4[0] = paramString;
      localObject3 = ((StringBuilder)localObject3).toString();
      localObject4[bool1] = localObject3;
      localObject2 = String.format((String)localObject2, (Object[])localObject4);
      f((String)localObject2);
    }
    g.clear();
  }
  
  boolean c(b paramb)
  {
    boolean bool1 = g();
    boolean bool2 = false;
    String str1 = null;
    int i2 = 1;
    if (bool1)
    {
      Object localObject1 = h();
      Object localObject2 = "stopTracking";
      bool1 = a((WebView)localObject1, (String)localObject2);
      if (bool1)
      {
        localObject1 = "stopTracking";
        bool1 = e((String)localObject1);
        if (bool1)
        {
          int i1 = 3;
          localObject2 = "JavaScriptBridge";
          try
          {
            Object localObject3 = new java/lang/StringBuilder;
            String str2 = "Ending tracking on tracker";
            ((StringBuilder)localObject3).<init>(str2);
            str2 = b;
            ((StringBuilder)localObject3).append(str2);
            localObject3 = ((StringBuilder)localObject3).toString();
            p.a(i1, (String)localObject2, this, (String)localObject3);
            localObject1 = "javascript: MoatMAK.egpw(\"%s\")";
            localObject2 = new Object[i2];
            localObject3 = b;
            localObject2[0] = localObject3;
            localObject1 = String.format((String)localObject1, (Object[])localObject2);
            f((String)localObject1);
          }
          catch (Exception localException)
          {
            str1 = "JavaScriptBridge";
            localObject2 = "Failed to end impression.";
            p.a(str1, this, (String)localObject2, localException);
          }
          bool2 = true;
        }
      }
    }
    j.a locala1 = l;
    j.a locala2 = j.a.b;
    if (locala1 == locala2) {
      d(paramb);
    } else {
      f();
    }
    f.remove(paramb);
    return bool2;
  }
  
  protected void finalize()
  {
    try
    {
      super.finalize();
      int i1 = 3;
      String str1 = "JavaScriptBridge";
      String str2 = "finalize";
      p.a(i1, str1, this, str2);
      f();
      return;
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */