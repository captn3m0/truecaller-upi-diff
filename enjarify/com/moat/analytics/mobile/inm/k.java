package com.moat.analytics.mobile.inm;

import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import java.lang.ref.WeakReference;

class k
  extends MoatAnalytics
  implements w.b
{
  boolean a = false;
  boolean b = false;
  boolean c = false;
  g d;
  WeakReference e;
  private boolean f = false;
  private String g;
  
  private void a(MoatOptions paramMoatOptions, Application paramApplication)
  {
    boolean bool1 = f;
    if (bool1)
    {
      p.a(3, "Analytics", this, "Moat SDK has already been started.");
      return;
    }
    Object localObject = w.a();
    ((w)localObject).b();
    bool1 = loggingEnabled;
    boolean bool2 = true;
    if (bool1)
    {
      localObject = paramApplication.getApplicationContext();
      bool1 = a((Context)localObject);
      if (bool1) {
        a = bool2;
      }
    }
    bool1 = disableLocationServices;
    c = bool1;
    if (paramApplication == null)
    {
      p.a("[ERROR] ", "Moat Analytics SDK didn't start, application was null");
      return;
    }
    localObject = new java/lang/ref/WeakReference;
    Context localContext = paramApplication.getApplicationContext();
    ((WeakReference)localObject).<init>(localContext);
    e = ((WeakReference)localObject);
    f = bool2;
    bool1 = autoTrackGMAInterstitials;
    b = bool1;
    a.a(paramApplication);
    localObject = w.a();
    ((w)localObject).a(this);
    boolean bool3 = disableAdIdCollection;
    if (!bool3) {
      s.a(paramApplication);
    }
    p.a("[SUCCESS] ", "Moat Analytics SDK Version 2.2.0 started");
  }
  
  private static boolean a(Context paramContext)
  {
    paramContext = paramContext.getApplicationInfo();
    int i = flags & 0x2;
    return i != 0;
  }
  
  private void d()
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new com/moat/analytics/mobile/inm/g;
      Object localObject2 = a.a();
      Object localObject3 = g.a.a;
      ((g)localObject1).<init>((Context)localObject2, (g.a)localObject3);
      d = ((g)localObject1);
      localObject1 = d;
      localObject2 = g;
      ((g)localObject1).a((String)localObject2);
      int i = 3;
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("Preparing native display tracking with partner code ");
      String str = g;
      ((StringBuilder)localObject3).append(str);
      localObject3 = ((StringBuilder)localObject3).toString();
      p.a(i, "Analytics", this, (String)localObject3);
      localObject1 = "[SUCCESS] ";
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Prepared for native display tracking with partner code ");
      localObject3 = g;
      ((StringBuilder)localObject2).append((String)localObject3);
      localObject2 = ((StringBuilder)localObject2).toString();
      p.a((String)localObject1, (String)localObject2);
    }
  }
  
  boolean a()
  {
    return f;
  }
  
  public void b()
  {
    o.a();
    String str = g;
    if (str != null) {
      try
      {
        d();
        return;
      }
      catch (Exception localException)
      {
        m.a(localException);
      }
    }
  }
  
  public void c() {}
  
  public void prepareNativeDisplayTracking(String paramString)
  {
    g = paramString;
    paramString = aa;
    w.d locald = w.d.a;
    if (paramString == locald) {
      return;
    }
    try
    {
      d();
      return;
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
  }
  
  public void start(Application paramApplication)
  {
    MoatOptions localMoatOptions = new com/moat/analytics/mobile/inm/MoatOptions;
    localMoatOptions.<init>();
    start(localMoatOptions, paramApplication);
  }
  
  public void start(MoatOptions paramMoatOptions, Application paramApplication)
  {
    try
    {
      a(paramMoatOptions, paramApplication);
      return;
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */