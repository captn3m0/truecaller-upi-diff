package com.moat.analytics.mobile.inm;

import android.app.Activity;
import android.view.View;
import android.webkit.WebView;
import com.moat.analytics.mobile.inm.a.a.a;
import java.lang.ref.WeakReference;

abstract class b
{
  j a;
  final String b;
  final boolean c;
  boolean d;
  boolean e;
  private WeakReference f;
  private WeakReference g;
  private final z h;
  private final boolean i;
  
  b(View paramView, boolean paramBoolean1, boolean paramBoolean2)
  {
    Object localObject = "BaseTracker";
    String str = "Initializing.";
    int j = 3;
    p.a(j, (String)localObject, this, str);
    if (paramBoolean1)
    {
      localObject = new java/lang/StringBuilder;
      str = "m";
      ((StringBuilder)localObject).<init>(str);
      int k = hashCode();
      ((StringBuilder)localObject).append(k);
      localObject = ((StringBuilder)localObject).toString();
    }
    else
    {
      localObject = "";
    }
    b = ((String)localObject);
    localObject = new java/lang/ref/WeakReference;
    ((WeakReference)localObject).<init>(paramView);
    f = ((WeakReference)localObject);
    i = paramBoolean1;
    c = paramBoolean2;
    d = false;
    e = false;
    paramView = new com/moat/analytics/mobile/inm/z;
    paramView.<init>();
    h = paramView;
  }
  
  private void g()
  {
    a.a(g);
    Object localObject1 = "Attempting bridge installation.";
    int j = 3;
    p.a(j, "BaseTracker", this, (String)localObject1);
    Object localObject2 = g.get();
    if (localObject2 != null)
    {
      boolean bool = i;
      if (!bool)
      {
        bool = c;
        if (!bool)
        {
          localObject2 = new com/moat/analytics/mobile/inm/j;
          localObject1 = (WebView)g.get();
          localObject3 = j.a.a;
          ((j)localObject2).<init>((WebView)localObject1, (j.a)localObject3);
          a = ((j)localObject2);
        }
      }
      localObject2 = a;
      bool = a;
      localObject1 = "BaseTracker";
      Object localObject3 = new java/lang/StringBuilder;
      String str = "Bridge ";
      ((StringBuilder)localObject3).<init>(str);
      if (bool) {
        localObject2 = "";
      } else {
        localObject2 = "not ";
      }
      ((StringBuilder)localObject3).append((String)localObject2);
      ((StringBuilder)localObject3).append("installed.");
      localObject2 = ((StringBuilder)localObject3).toString();
      p.a(j, (String)localObject1, this, (String)localObject2);
      return;
    }
    a = null;
    p.a(j, "BaseTracker", this, "Bridge not installed, WebView is null.");
  }
  
  abstract String a();
  
  void a(WebView paramWebView)
  {
    if (paramWebView != null)
    {
      WeakReference localWeakReference = new java/lang/ref/WeakReference;
      localWeakReference.<init>(paramWebView);
      g = localWeakReference;
      paramWebView = a;
      if (paramWebView == null) {
        g();
      }
      paramWebView = a;
      if (paramWebView != null)
      {
        boolean bool = a;
        if (bool)
        {
          paramWebView = a;
          paramWebView.a(this);
        }
      }
    }
  }
  
  void a(j paramj)
  {
    a = paramj;
  }
  
  boolean b()
  {
    Object localObject1 = "BaseTracker";
    Object localObject2 = "Attempting to start impression.";
    int j = 3;
    p.a(j, (String)localObject1, this, (String)localObject2);
    boolean bool1 = e;
    if (!bool1)
    {
      localObject1 = a;
      bool1 = ((j)localObject1).b(this);
      localObject2 = "BaseTracker";
      Object localObject3 = new java/lang/StringBuilder;
      String str1 = "Impression ";
      ((StringBuilder)localObject3).<init>(str1);
      if (bool1) {
        str1 = "";
      } else {
        str1 = "not ";
      }
      ((StringBuilder)localObject3).append(str1);
      str1 = "started.";
      ((StringBuilder)localObject3).append(str1);
      localObject3 = ((StringBuilder)localObject3).toString();
      p.a(j, (String)localObject2, this, (String)localObject3);
      if (bool1)
      {
        boolean bool2 = true;
        d = bool2;
        e = bool2;
      }
    }
    else
    {
      p.a(j, "BaseTracker", this, "startTracking failed, tracker already started");
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      String str2 = a();
      ((StringBuilder)localObject2).append(str2);
      str2 = " already started";
      ((StringBuilder)localObject2).append(str2);
      localObject2 = ((StringBuilder)localObject2).toString();
      p.a("[INFO] ", (String)localObject2);
      bool1 = false;
      localObject1 = null;
    }
    return bool1;
  }
  
  boolean c()
  {
    int j = 3;
    p.a(j, "BaseTracker", this, "Attempting to stop impression.");
    d = false;
    j localj = a;
    boolean bool = localj.c(this);
    String str1 = "BaseTracker";
    Object localObject = new java/lang/StringBuilder;
    String str2 = "Impression tracking ";
    ((StringBuilder)localObject).<init>(str2);
    if (bool) {
      str2 = "";
    } else {
      str2 = "not ";
    }
    ((StringBuilder)localObject).append(str2);
    ((StringBuilder)localObject).append("stopped.");
    localObject = ((StringBuilder)localObject).toString();
    p.a(j, str1, this, (String)localObject);
    return bool;
  }
  
  public void changeTargetView(View paramView)
  {
    Object localObject1 = "BaseTracker";
    Object localObject2 = new java/lang/StringBuilder;
    Object localObject3 = "changing view to ";
    ((StringBuilder)localObject2).<init>((String)localObject3);
    if (paramView != null)
    {
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      String str = paramView.getClass().getSimpleName();
      ((StringBuilder)localObject3).append(str);
      str = "@";
      ((StringBuilder)localObject3).append(str);
      int j = paramView.hashCode();
      ((StringBuilder)localObject3).append(j);
      localObject3 = ((StringBuilder)localObject3).toString();
    }
    else
    {
      localObject3 = "null";
    }
    ((StringBuilder)localObject2).append((String)localObject3);
    localObject2 = ((StringBuilder)localObject2).toString();
    p.a(3, (String)localObject1, this, (String)localObject2);
    localObject1 = new java/lang/ref/WeakReference;
    ((WeakReference)localObject1).<init>(paramView);
    f = ((WeakReference)localObject1);
  }
  
  View d()
  {
    return (View)f.get();
  }
  
  String e()
  {
    Object localObject = d();
    if (localObject != null)
    {
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      String str = d().getClass().getSimpleName();
      ((StringBuilder)localObject).append(str);
      ((StringBuilder)localObject).append("@");
      int j = d().hashCode();
      ((StringBuilder)localObject).append(j);
      return ((StringBuilder)localObject).toString();
    }
    return "";
  }
  
  String f()
  {
    z localz = h;
    String str = b;
    View localView = d();
    localz.a(str, localView);
    return h.a;
  }
  
  public void setActivity(Activity paramActivity) {}
  
  public void startTracking()
  {
    int j = 3;
    String str1 = "BaseTracker";
    Object localObject1 = "In startTracking method.";
    boolean bool;
    try
    {
      p.a(j, str1, this, (String)localObject1);
      bool = b();
    }
    catch (Exception localException)
    {
      m.a(localException);
      bool = false;
      str1 = null;
    }
    localObject1 = "BaseTracker";
    Object localObject2 = new java/lang/StringBuilder;
    String str2 = "Attempt to start tracking ad impression was ";
    ((StringBuilder)localObject2).<init>(str2);
    if (bool) {
      str2 = "";
    } else {
      str2 = "un";
    }
    ((StringBuilder)localObject2).append(str2);
    str2 = "successful.";
    ((StringBuilder)localObject2).append(str2);
    localObject2 = ((StringBuilder)localObject2).toString();
    p.a(j, (String)localObject1, this, (String)localObject2);
    String str3;
    if (bool) {
      str3 = "[SUCCESS] ";
    } else {
      str3 = "[ERROR] ";
    }
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    localObject2 = a();
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject2 = " startTracking ";
    ((StringBuilder)localObject1).append((String)localObject2);
    if (bool) {
      str1 = "succeeded";
    } else {
      str1 = "failed";
    }
    ((StringBuilder)localObject1).append(str1);
    ((StringBuilder)localObject1).append(" for ");
    str1 = e();
    ((StringBuilder)localObject1).append(str1);
    str1 = ((StringBuilder)localObject1).toString();
    p.a(str3, str1);
  }
  
  public void stopTracking()
  {
    int j = 3;
    String str1 = "BaseTracker";
    Object localObject1 = "In stopTracking method.";
    boolean bool;
    try
    {
      p.a(j, str1, this, (String)localObject1);
      bool = c();
    }
    catch (Exception localException)
    {
      m.a(localException);
      bool = false;
      str1 = null;
    }
    localObject1 = "BaseTracker";
    Object localObject2 = new java/lang/StringBuilder;
    String str2 = "Attempt to stop tracking ad impression was ";
    ((StringBuilder)localObject2).<init>(str2);
    if (bool) {
      str2 = "";
    } else {
      str2 = "un";
    }
    ((StringBuilder)localObject2).append(str2);
    str2 = "successful.";
    ((StringBuilder)localObject2).append(str2);
    localObject2 = ((StringBuilder)localObject2).toString();
    p.a(j, (String)localObject1, this, (String)localObject2);
    String str3;
    if (bool) {
      str3 = "[SUCCESS] ";
    } else {
      str3 = "[ERROR] ";
    }
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    localObject2 = a();
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject2 = " stopTracking ";
    ((StringBuilder)localObject1).append((String)localObject2);
    if (bool) {
      str1 = "succeeded";
    } else {
      str1 = "failed";
    }
    ((StringBuilder)localObject1).append(str1);
    ((StringBuilder)localObject1).append(" for ");
    str1 = e();
    ((StringBuilder)localObject1).append(str1);
    str1 = ((StringBuilder)localObject1).toString();
    p.a(str3, str1);
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */