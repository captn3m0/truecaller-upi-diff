package com.moat.analytics.mobile.inm;

import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import java.util.Map;

public abstract class MoatFactory
{
  public static MoatFactory create()
  {
    Object localObject;
    try
    {
      localObject = new com/moat/analytics/mobile/inm/n;
      ((n)localObject).<init>();
      return (MoatFactory)localObject;
    }
    catch (Exception localException)
    {
      m.a(localException);
      localObject = new com/moat/analytics/mobile/inm/v$b;
      ((v.b)localObject).<init>();
    }
    return (MoatFactory)localObject;
  }
  
  public abstract Object createCustomTracker(MoatPlugin paramMoatPlugin);
  
  public abstract NativeDisplayTracker createNativeDisplayTracker(View paramView, Map paramMap);
  
  public abstract NativeVideoTracker createNativeVideoTracker(String paramString);
  
  public abstract WebAdTracker createWebAdTracker(ViewGroup paramViewGroup);
  
  public abstract WebAdTracker createWebAdTracker(WebView paramWebView);
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.MoatFactory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */