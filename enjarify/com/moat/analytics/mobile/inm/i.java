package com.moat.analytics.mobile.inm;

import android.content.Context;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

class i
{
  private static final i a;
  private final Map b;
  private final Map c;
  private final ScheduledExecutorService d;
  private ScheduledFuture e;
  private ScheduledFuture f;
  
  static
  {
    i locali = new com/moat/analytics/mobile/inm/i;
    locali.<init>();
    a = locali;
  }
  
  private i()
  {
    Object localObject = Executors.newScheduledThreadPool(1);
    d = ((ScheduledExecutorService)localObject);
    localObject = new java/util/WeakHashMap;
    ((WeakHashMap)localObject).<init>();
    b = ((Map)localObject);
    localObject = new java/util/WeakHashMap;
    ((WeakHashMap)localObject).<init>();
    c = ((Map)localObject);
  }
  
  static i a()
  {
    return a;
  }
  
  private void a(Context paramContext)
  {
    ScheduledFuture localScheduledFuture = f;
    if (localScheduledFuture != null)
    {
      boolean bool = localScheduledFuture.isDone();
      if (!bool) {}
    }
    else
    {
      int i = 3;
      String str1 = "JSUpdateLooper";
      String str2 = "Starting metadata reporting loop";
      p.a(i, str1, this, str2);
      i.1 local1 = new com/moat/analytics/mobile/inm/i$1;
      local1.<init>(this, paramContext);
      ScheduledExecutorService localScheduledExecutorService = d;
      long l1 = 0L;
      long l2 = 50;
      TimeUnit localTimeUnit = TimeUnit.MILLISECONDS;
      paramContext = localScheduledExecutorService.scheduleWithFixedDelay(local1, l1, l2, localTimeUnit);
      f = paramContext;
    }
  }
  
  private void b(Context paramContext)
  {
    ScheduledFuture localScheduledFuture = e;
    if (localScheduledFuture != null)
    {
      boolean bool = localScheduledFuture.isDone();
      if (!bool) {}
    }
    else
    {
      int i = 3;
      String str1 = "JSUpdateLooper";
      String str2 = "Starting view update loop";
      p.a(i, str1, this, str2);
      i.2 local2 = new com/moat/analytics/mobile/inm/i$2;
      local2.<init>(this, paramContext);
      ScheduledExecutorService localScheduledExecutorService = d;
      long l1 = 0L;
      int j = ad;
      long l2 = j;
      TimeUnit localTimeUnit = TimeUnit.MILLISECONDS;
      paramContext = localScheduledExecutorService.scheduleWithFixedDelay(local2, l1, l2, localTimeUnit);
      e = paramContext;
    }
  }
  
  void a(Context paramContext, b paramb)
  {
    if (paramb != null)
    {
      int i = 3;
      String str1 = "JSUpdateLooper";
      Object localObject = new java/lang/StringBuilder;
      String str2 = "addActiveTracker";
      ((StringBuilder)localObject).<init>(str2);
      int j = paramb.hashCode();
      ((StringBuilder)localObject).append(j);
      localObject = ((StringBuilder)localObject).toString();
      p.a(i, str1, this, (String)localObject);
      Map localMap = c;
      if (localMap != null)
      {
        boolean bool = localMap.containsKey(paramb);
        if (!bool)
        {
          localMap = c;
          str1 = "";
          localMap.put(paramb, str1);
          b(paramContext);
        }
      }
    }
  }
  
  void a(Context paramContext, j paramj)
  {
    Map localMap = b;
    if ((localMap != null) && (paramj != null))
    {
      String str = "";
      localMap.put(paramj, str);
      a(paramContext);
    }
  }
  
  void a(b paramb)
  {
    if (paramb != null)
    {
      int i = 3;
      String str1 = "JSUpdateLooper";
      Object localObject = new java/lang/StringBuilder;
      String str2 = "removeActiveTracker";
      ((StringBuilder)localObject).<init>(str2);
      int j = paramb.hashCode();
      ((StringBuilder)localObject).append(j);
      localObject = ((StringBuilder)localObject).toString();
      p.a(i, str1, this, (String)localObject);
      Map localMap = c;
      if (localMap != null) {
        localMap.remove(paramb);
      }
    }
  }
  
  void a(j paramj)
  {
    if (paramj != null)
    {
      int i = 3;
      String str1 = "JSUpdateLooper";
      Object localObject = new java/lang/StringBuilder;
      String str2 = "removeSetupNeededBridge";
      ((StringBuilder)localObject).<init>(str2);
      int j = paramj.hashCode();
      ((StringBuilder)localObject).append(j);
      localObject = ((StringBuilder)localObject).toString();
      p.a(i, str1, this, (String)localObject);
      Map localMap = b;
      if (localMap != null) {
        localMap.remove(paramj);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */