package com.moat.analytics.mobile.inm;

public enum MoatAdEventType
{
  private final String a;
  
  static
  {
    Object localObject = new com/moat/analytics/mobile/inm/MoatAdEventType;
    ((MoatAdEventType)localObject).<init>("AD_EVT_FIRST_QUARTILE", 0, "AdVideoFirstQuartile");
    AD_EVT_FIRST_QUARTILE = (MoatAdEventType)localObject;
    localObject = new com/moat/analytics/mobile/inm/MoatAdEventType;
    int i = 1;
    ((MoatAdEventType)localObject).<init>("AD_EVT_MID_POINT", i, "AdVideoMidpoint");
    AD_EVT_MID_POINT = (MoatAdEventType)localObject;
    localObject = new com/moat/analytics/mobile/inm/MoatAdEventType;
    int j = 2;
    ((MoatAdEventType)localObject).<init>("AD_EVT_THIRD_QUARTILE", j, "AdVideoThirdQuartile");
    AD_EVT_THIRD_QUARTILE = (MoatAdEventType)localObject;
    localObject = new com/moat/analytics/mobile/inm/MoatAdEventType;
    int k = 3;
    ((MoatAdEventType)localObject).<init>("AD_EVT_COMPLETE", k, "AdVideoComplete");
    AD_EVT_COMPLETE = (MoatAdEventType)localObject;
    localObject = new com/moat/analytics/mobile/inm/MoatAdEventType;
    int m = 4;
    ((MoatAdEventType)localObject).<init>("AD_EVT_PAUSED", m, "AdPaused");
    AD_EVT_PAUSED = (MoatAdEventType)localObject;
    localObject = new com/moat/analytics/mobile/inm/MoatAdEventType;
    int n = 5;
    ((MoatAdEventType)localObject).<init>("AD_EVT_PLAYING", n, "AdPlaying");
    AD_EVT_PLAYING = (MoatAdEventType)localObject;
    localObject = new com/moat/analytics/mobile/inm/MoatAdEventType;
    int i1 = 6;
    ((MoatAdEventType)localObject).<init>("AD_EVT_START", i1, "AdVideoStart");
    AD_EVT_START = (MoatAdEventType)localObject;
    localObject = new com/moat/analytics/mobile/inm/MoatAdEventType;
    int i2 = 7;
    ((MoatAdEventType)localObject).<init>("AD_EVT_STOPPED", i2, "AdStopped");
    AD_EVT_STOPPED = (MoatAdEventType)localObject;
    localObject = new com/moat/analytics/mobile/inm/MoatAdEventType;
    int i3 = 8;
    ((MoatAdEventType)localObject).<init>("AD_EVT_SKIPPED", i3, "AdSkipped");
    AD_EVT_SKIPPED = (MoatAdEventType)localObject;
    localObject = new com/moat/analytics/mobile/inm/MoatAdEventType;
    int i4 = 9;
    ((MoatAdEventType)localObject).<init>("AD_EVT_VOLUME_CHANGE", i4, "AdVolumeChange");
    AD_EVT_VOLUME_CHANGE = (MoatAdEventType)localObject;
    localObject = new com/moat/analytics/mobile/inm/MoatAdEventType;
    int i5 = 10;
    ((MoatAdEventType)localObject).<init>("AD_EVT_ENTER_FULLSCREEN", i5, "fullScreen");
    AD_EVT_ENTER_FULLSCREEN = (MoatAdEventType)localObject;
    localObject = new com/moat/analytics/mobile/inm/MoatAdEventType;
    int i6 = 11;
    ((MoatAdEventType)localObject).<init>("AD_EVT_EXIT_FULLSCREEN", i6, "exitFullscreen");
    AD_EVT_EXIT_FULLSCREEN = (MoatAdEventType)localObject;
    localObject = new MoatAdEventType[12];
    MoatAdEventType localMoatAdEventType = AD_EVT_FIRST_QUARTILE;
    localObject[0] = localMoatAdEventType;
    localMoatAdEventType = AD_EVT_MID_POINT;
    localObject[i] = localMoatAdEventType;
    localMoatAdEventType = AD_EVT_THIRD_QUARTILE;
    localObject[j] = localMoatAdEventType;
    localMoatAdEventType = AD_EVT_COMPLETE;
    localObject[k] = localMoatAdEventType;
    localMoatAdEventType = AD_EVT_PAUSED;
    localObject[m] = localMoatAdEventType;
    localMoatAdEventType = AD_EVT_PLAYING;
    localObject[n] = localMoatAdEventType;
    localMoatAdEventType = AD_EVT_START;
    localObject[i1] = localMoatAdEventType;
    localMoatAdEventType = AD_EVT_STOPPED;
    localObject[i2] = localMoatAdEventType;
    localMoatAdEventType = AD_EVT_SKIPPED;
    localObject[i3] = localMoatAdEventType;
    localMoatAdEventType = AD_EVT_VOLUME_CHANGE;
    localObject[i4] = localMoatAdEventType;
    localMoatAdEventType = AD_EVT_ENTER_FULLSCREEN;
    localObject[i5] = localMoatAdEventType;
    localMoatAdEventType = AD_EVT_EXIT_FULLSCREEN;
    localObject[i6] = localMoatAdEventType;
    b = (MoatAdEventType[])localObject;
  }
  
  private MoatAdEventType(String paramString1)
  {
    a = paramString1;
  }
  
  public static MoatAdEventType fromString(String paramString)
  {
    if (paramString != null)
    {
      MoatAdEventType[] arrayOfMoatAdEventType = values();
      int i = arrayOfMoatAdEventType.length;
      int j = 0;
      while (j < i)
      {
        MoatAdEventType localMoatAdEventType = arrayOfMoatAdEventType[j];
        String str = localMoatAdEventType.toString();
        boolean bool = paramString.equalsIgnoreCase(str);
        if (bool) {
          return localMoatAdEventType;
        }
        j += 1;
      }
    }
    return null;
  }
  
  public final String toString()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.MoatAdEventType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */