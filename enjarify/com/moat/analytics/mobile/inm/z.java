package com.moat.analytics.mobile.inm;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.location.Location;
import android.os.Build.VERSION;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ListView;
import java.lang.ref.WeakReference;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

class z
{
  String a;
  private z.a b;
  private JSONObject c;
  private Rect d;
  private Rect e;
  private JSONObject f;
  private JSONObject g;
  private Location h;
  private Map i;
  
  z()
  {
    Object localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    i = ((Map)localObject);
    a = "{}";
    localObject = new com/moat/analytics/mobile/inm/z$a;
    ((z.a)localObject).<init>();
    b = ((z.a)localObject);
  }
  
  static int a(Rect paramRect, Set paramSet)
  {
    boolean bool1 = paramSet.isEmpty();
    int j = 0;
    Integer localInteger1 = null;
    if (!bool1)
    {
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      localArrayList.addAll(paramSet);
      paramSet = new com/moat/analytics/mobile/inm/z$1;
      paramSet.<init>();
      Collections.sort(localArrayList, paramSet);
      paramSet = new java/util/ArrayList;
      paramSet.<init>();
      Iterator localIterator = localArrayList.iterator();
      Object localObject1;
      int m;
      int k;
      for (;;)
      {
        boolean bool2 = localIterator.hasNext();
        if (!bool2) {
          break;
        }
        localObject1 = (Rect)localIterator.next();
        m = left;
        Integer localInteger2 = Integer.valueOf(m);
        paramSet.add(localInteger2);
        k = right;
        localObject1 = Integer.valueOf(k);
        paramSet.add(localObject1);
      }
      Collections.sort(paramSet);
      int n = 0;
      localIterator = null;
      for (;;)
      {
        k = paramSet.size() + -1;
        if (j >= k) {
          break;
        }
        localObject1 = (Integer)paramSet.get(j);
        m = j + 1;
        Object localObject2 = paramSet.get(m);
        boolean bool3 = ((Integer)localObject1).equals(localObject2);
        if (!bool3)
        {
          localObject1 = new android/graphics/Rect;
          localInteger1 = (Integer)paramSet.get(j);
          j = localInteger1.intValue();
          int i1 = top;
          Object localObject3 = (Integer)paramSet.get(m);
          int i2 = ((Integer)localObject3).intValue();
          int i4 = bottom;
          ((Rect)localObject1).<init>(j, i1, i2, i4);
          j = top;
          localObject2 = localArrayList.iterator();
          int i5;
          int i3;
          do
          {
            boolean bool5;
            do
            {
              boolean bool4 = ((Iterator)localObject2).hasNext();
              if (!bool4) {
                break;
              }
              localObject3 = (Rect)((Iterator)localObject2).next();
              bool5 = Rect.intersects((Rect)localObject3, (Rect)localObject1);
            } while (!bool5);
            i5 = bottom;
            if (i5 > j)
            {
              i5 = ((Rect)localObject1).width();
              int i6 = bottom;
              int i7 = top;
              j = Math.max(j, i7);
              i6 -= j;
              i5 *= i6;
              n += i5;
              j = bottom;
            }
            i3 = bottom;
            i5 = bottom;
          } while (i3 != i5);
        }
        j = m;
      }
      j = n;
    }
    return j;
  }
  
  private static Rect a(DisplayMetrics paramDisplayMetrics)
  {
    int j = widthPixels;
    int k = heightPixels;
    Rect localRect = new android/graphics/Rect;
    localRect.<init>(0, 0, j, k);
    return localRect;
  }
  
  static Rect a(View paramView)
  {
    if (paramView != null) {
      return h(paramView);
    }
    paramView = new android/graphics/Rect;
    paramView.<init>(0, 0, 0, 0);
    return paramView;
  }
  
  private static z.a a(View paramView, Rect paramRect, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    z.a locala = new com/moat/analytics/mobile/inm/z$a;
    locala.<init>();
    boolean bool1 = b(paramRect);
    if ((paramView != null) && (paramBoolean1) && (paramBoolean2) && (!paramBoolean3) && (bool1))
    {
      Rect localRect = new android/graphics/Rect;
      localRect.<init>(0, 0, 0, 0);
      paramBoolean2 = paramView.getGlobalVisibleRect(localRect);
      if (paramBoolean2)
      {
        paramBoolean2 = b(localRect);
        if (paramBoolean2 < bool1)
        {
          paramBoolean3 = true;
          localObject = "VisibilityInfo";
          String str = "Ad is clipped";
          p.b(paramBoolean3, (String)localObject, null, str);
        }
        HashSet localHashSet = new java/util/HashSet;
        localHashSet.<init>();
        Object localObject = paramView.getRootView();
        boolean bool2 = localObject instanceof ViewGroup;
        if (bool2)
        {
          a = localRect;
          boolean bool3 = a(localRect, paramView, localHashSet);
          double d1 = 1.0D;
          if (bool3) {
            c = d1;
          }
          if (!bool3)
          {
            boolean bool4 = a(localRect, localHashSet);
            if (bool4)
            {
              double d2 = bool4;
              double d3 = paramBoolean2;
              Double.isNaN(d3);
              d3 *= d1;
              Double.isNaN(d2);
              d2 /= d3;
              c = d2;
            }
            paramBoolean2 -= bool4;
            double d4 = paramBoolean2;
            double d5 = bool1;
            Double.isNaN(d5);
            d5 *= d1;
            Double.isNaN(d4);
            d4 /= d5;
            b = d4;
          }
        }
      }
    }
    return locala;
  }
  
  private static Map a(Rect paramRect)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    String str = String.valueOf(left);
    localHashMap.put("x", str);
    str = String.valueOf(top);
    localHashMap.put("y", str);
    int j = right;
    int k = left;
    str = String.valueOf(j - k);
    localHashMap.put("w", str);
    j = bottom;
    int m = top;
    paramRect = String.valueOf(j - m);
    localHashMap.put("h", paramRect);
    return localHashMap;
  }
  
  private static Map a(Rect paramRect, DisplayMetrics paramDisplayMetrics)
  {
    return a(b(paramRect, paramDisplayMetrics));
  }
  
  private static JSONObject a(Location paramLocation)
  {
    paramLocation = b(paramLocation);
    if (paramLocation == null) {
      return null;
    }
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>(paramLocation);
    return localJSONObject;
  }
  
  private static boolean a(Rect paramRect, View paramView, Set paramSet)
  {
    View localView = paramView.getRootView();
    ArrayDeque localArrayDeque = new java/util/ArrayDeque;
    localArrayDeque.<init>();
    localArrayDeque.add(localView);
    String str1 = "starting covering rect search";
    int j = 2;
    p.b(j, "VisibilityInfo", paramView, str1);
    localView = null;
    int k = 1;
    int m = 0;
    int n = 0;
    String str2 = null;
    label321:
    boolean bool2;
    do
    {
      Object localObject1;
      Object localObject2;
      boolean bool5;
      do
      {
        boolean bool4;
        do
        {
          do
          {
            for (;;)
            {
              boolean bool1 = localArrayDeque.isEmpty();
              if (bool1) {
                break label462;
              }
              int i1 = 250;
              if (m >= i1) {
                break label462;
              }
              m += 1;
              localObject1 = (View)localArrayDeque.pollLast();
              bool3 = localObject1.equals(paramView);
              if (!bool3) {
                break;
              }
              str2 = "VisibilityInfo";
              localObject1 = "found target";
              p.b(j, str2, paramRect, (String)localObject1);
              n = 1;
            }
            bool3 = g((View)localObject1);
          } while (!bool3);
          boolean bool3 = localObject1 instanceof ViewGroup;
          if (bool3)
          {
            bool3 = localObject1 instanceof ListView;
            if (!bool3)
            {
              localObject2 = localObject1;
              localObject2 = (ViewGroup)localObject1;
              i3 = ((ViewGroup)localObject2).getChildCount() - k;
              while (i3 >= 0)
              {
                localObject3 = ((ViewGroup)localObject2).getChildAt(i3);
                localArrayDeque.add(localObject3);
                i3 += -1;
              }
            }
          }
          int i2 = Build.VERSION.SDK_INT;
          int i3 = 21;
          float f1 = 2.9E-44F;
          if (i2 >= i3)
          {
            f2 = ((View)localObject1).getElevation();
            f1 = paramView.getElevation();
            bool4 = f2 < f1;
            if (!bool4)
            {
              if (n == 0) {
                break label321;
              }
              f2 = ((View)localObject1).getElevation();
              f1 = paramView.getElevation();
              bool4 = f2 < f1;
              if (bool4) {
                break label321;
              }
            }
          }
          else
          {
            if (n == 0) {
              break label321;
            }
          }
          bool4 = true;
          float f2 = Float.MIN_VALUE;
          continue;
          bool4 = false;
          f2 = 0.0F;
          localObject2 = null;
        } while (!bool4);
        localObject2 = h((View)localObject1);
        bool5 = ((Rect)localObject2).setIntersect(paramRect, (Rect)localObject2);
      } while (!bool5);
      String str3 = "VisibilityInfo";
      Object localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("Covered by ");
      String str4 = localObject1.getClass().getSimpleName();
      ((StringBuilder)localObject3).append(str4);
      ((StringBuilder)localObject3).append("-");
      str4 = ((Rect)localObject2).toString();
      ((StringBuilder)localObject3).append(str4);
      localObject3 = ((StringBuilder)localObject3).toString();
      p.b(j, str3, localObject1, (String)localObject3);
      paramSet.add(localObject2);
      bool2 = ((Rect)localObject2).contains(paramRect);
    } while (!bool2);
    return k;
    label462:
    return false;
  }
  
  private static int b(Rect paramRect)
  {
    int j = paramRect.width();
    int k = paramRect.height();
    return j * k;
  }
  
  private static Rect b(Rect paramRect, DisplayMetrics paramDisplayMetrics)
  {
    float f1 = density;
    boolean bool = f1 < 0.0F;
    if (!bool) {
      return paramRect;
    }
    int j = Math.round(left / f1);
    int k = Math.round(right / f1);
    int m = Math.round(top / f1);
    int n = Math.round(bottom / f1);
    paramDisplayMetrics = new android/graphics/Rect;
    paramDisplayMetrics.<init>(j, m, k, n);
    return paramDisplayMetrics;
  }
  
  private static Map b(Location paramLocation)
  {
    if (paramLocation == null) {
      return null;
    }
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    String str = Double.toString(paramLocation.getLatitude());
    localHashMap.put("latitude", str);
    str = Double.toString(paramLocation.getLongitude());
    localHashMap.put("longitude", str);
    str = Long.toString(paramLocation.getTime());
    localHashMap.put("timestamp", str);
    paramLocation = Float.toString(paramLocation.getAccuracy());
    localHashMap.put("horizontalAccuracy", paramLocation);
    return localHashMap;
  }
  
  private static boolean b(View paramView)
  {
    int j = Build.VERSION.SDK_INT;
    boolean bool1 = true;
    int k = 19;
    if (j >= k)
    {
      if (paramView != null)
      {
        boolean bool2 = paramView.isAttachedToWindow();
        if (bool2) {
          return bool1;
        }
      }
      return false;
    }
    if (paramView != null)
    {
      paramView = paramView.getWindowToken();
      if (paramView != null) {
        return bool1;
      }
    }
    return false;
  }
  
  private static boolean c(View paramView)
  {
    if (paramView != null)
    {
      boolean bool = paramView.hasWindowFocus();
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  private static boolean d(View paramView)
  {
    if (paramView != null)
    {
      boolean bool = paramView.isShown();
      if (bool) {
        return false;
      }
    }
    return true;
  }
  
  private static float e(View paramView)
  {
    if (paramView == null) {
      return 0.0F;
    }
    return f(paramView);
  }
  
  private static float f(View paramView)
  {
    float f1 = paramView.getAlpha();
    while (paramView != null)
    {
      Object localObject = paramView.getParent();
      if (localObject == null) {
        break;
      }
      double d1 = f1;
      double d2 = 0.0D;
      boolean bool1 = d1 < d2;
      if (!bool1) {
        break;
      }
      localObject = paramView.getParent();
      boolean bool2 = localObject instanceof View;
      if (!bool2) {
        break;
      }
      localObject = (View)paramView.getParent();
      float f2 = ((View)localObject).getAlpha();
      f1 *= f2;
      paramView = (View)paramView.getParent();
    }
    return f1;
  }
  
  private static boolean g(View paramView)
  {
    boolean bool1 = paramView.isShown();
    if (bool1)
    {
      float f1 = paramView.getAlpha();
      double d1 = f1;
      double d2 = 0.0D;
      boolean bool2 = d1 < d2;
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  private static Rect h(View paramView)
  {
    int[] arrayOfInt = new int[2];
    int[] tmp5_4 = arrayOfInt;
    tmp5_4[0] = (-1 << -1);
    tmp5_4[1] = (-1 << -1);
    paramView.getLocationInWindow(arrayOfInt);
    int j = arrayOfInt[0];
    int k = arrayOfInt[1];
    Rect localRect = new android/graphics/Rect;
    int m = paramView.getWidth() + j;
    int n = paramView.getHeight() + k;
    localRect.<init>(j, k, m, n);
    return localRect;
  }
  
  private static DisplayMetrics i(View paramView)
  {
    int j = Build.VERSION.SDK_INT;
    int k = 17;
    if (j >= k)
    {
      Object localObject = a.a;
      if (localObject != null)
      {
        localObject = (Activity)a.a.get();
        if (localObject != null)
        {
          paramView = new android/util/DisplayMetrics;
          paramView.<init>();
          ((Activity)localObject).getWindowManager().getDefaultDisplay().getRealMetrics(paramView);
          return paramView;
        }
      }
    }
    return paramView.getContext().getResources().getDisplayMetrics();
  }
  
  void a(String paramString, View paramView)
  {
    Object localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    String str1 = "{}";
    if (paramView != null) {
      try
      {
        Object localObject2 = i(paramView);
        boolean bool1 = b(paramView);
        boolean bool2 = c(paramView);
        boolean bool3 = d(paramView);
        float f1 = e(paramView);
        String str2 = "dr";
        float f2 = density;
        Object localObject3 = Float.valueOf(f2);
        ((Map)localObject1).put(str2, localObject3);
        str2 = "dv";
        double d1 = s.a();
        localObject3 = Double.valueOf(d1);
        ((Map)localObject1).put(str2, localObject3);
        str2 = "adKey";
        ((Map)localObject1).put(str2, paramString);
        paramString = "isAttached";
        int j = 0;
        str2 = null;
        f2 = Float.MIN_VALUE;
        int k;
        if (bool1)
        {
          k = 1;
        }
        else
        {
          k = 0;
          localInteger = null;
        }
        Integer localInteger = Integer.valueOf(k);
        ((Map)localObject1).put(paramString, localInteger);
        paramString = "inFocus";
        if (bool2)
        {
          k = 1;
        }
        else
        {
          k = 0;
          localInteger = null;
        }
        localInteger = Integer.valueOf(k);
        ((Map)localObject1).put(paramString, localInteger);
        paramString = "isHidden";
        if (bool3)
        {
          k = 1;
        }
        else
        {
          k = 0;
          localInteger = null;
        }
        localInteger = Integer.valueOf(k);
        ((Map)localObject1).put(paramString, localInteger);
        paramString = "opacity";
        Object localObject4 = Float.valueOf(f1);
        ((Map)localObject1).put(paramString, localObject4);
        paramString = a((DisplayMetrics)localObject2);
        localObject4 = a(paramView);
        paramView = a(paramView, (Rect)localObject4, bool1, bool2, bool3);
        Object localObject5 = c;
        if (localObject5 != null)
        {
          double d2 = b;
          z.a locala = b;
          double d3 = b;
          bool3 = d2 < d3;
          if (!bool3)
          {
            localObject5 = a;
            localObject6 = b;
            localObject6 = a;
            bool1 = ((Rect)localObject5).equals(localObject6);
            if (bool1)
            {
              d2 = c;
              locala = b;
              d3 = c;
              bool3 = d2 < d3;
              if (!bool3) {
                break label443;
              }
            }
          }
        }
        b = paramView;
        localObject5 = new org/json/JSONObject;
        Object localObject6 = b;
        localObject6 = a;
        localObject6 = a((Rect)localObject6, (DisplayMetrics)localObject2);
        ((JSONObject)localObject5).<init>((Map)localObject6);
        c = ((JSONObject)localObject5);
        j = 1;
        label443:
        localObject5 = "coveredPercent";
        double d4 = c;
        paramView = Double.valueOf(d4);
        ((Map)localObject1).put(localObject5, paramView);
        paramView = g;
        if (paramView != null)
        {
          paramView = e;
          bool4 = paramString.equals(paramView);
          if (bool4) {}
        }
        else
        {
          e = paramString;
          paramView = new org/json/JSONObject;
          paramString = a(paramString, (DisplayMetrics)localObject2);
          paramView.<init>(paramString);
          g = paramView;
          j = 1;
        }
        paramString = f;
        boolean bool5;
        if (paramString != null)
        {
          paramString = d;
          bool5 = ((Rect)localObject4).equals(paramString);
          if (bool5) {}
        }
        else
        {
          d = ((Rect)localObject4);
          paramString = new org/json/JSONObject;
          paramView = a((Rect)localObject4, (DisplayMetrics)localObject2);
          paramString.<init>(paramView);
          f = paramString;
          j = 1;
        }
        paramString = i;
        if (paramString != null)
        {
          paramString = i;
          bool5 = ((Map)localObject1).equals(paramString);
          if (bool5) {}
        }
        else
        {
          i = ((Map)localObject1);
          j = 1;
        }
        paramString = o.a();
        paramString = paramString.b();
        paramView = h;
        boolean bool4 = o.a(paramString, paramView);
        if (!bool4)
        {
          h = paramString;
          j = 1;
        }
        if (j != 0)
        {
          paramView = new org/json/JSONObject;
          localObject1 = i;
          paramView.<init>((Map)localObject1);
          localObject1 = "screen";
          localObject2 = g;
          paramView.accumulate((String)localObject1, localObject2);
          localObject1 = "view";
          localObject2 = f;
          paramView.accumulate((String)localObject1, localObject2);
          localObject1 = "visible";
          localObject2 = c;
          paramView.accumulate((String)localObject1, localObject2);
          localObject1 = "maybe";
          localObject2 = c;
          paramView.accumulate((String)localObject1, localObject2);
          localObject1 = "visiblePercent";
          localObject2 = b;
          double d5 = b;
          localObject2 = Double.valueOf(d5);
          paramView.accumulate((String)localObject1, localObject2);
          if (paramString != null)
          {
            localObject1 = "location";
            paramString = a(paramString);
            paramView.accumulate((String)localObject1, paramString);
          }
          str1 = paramView.toString();
          a = str1;
        }
      }
      catch (Exception paramString)
      {
        m.a(paramString);
        a = str1;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */