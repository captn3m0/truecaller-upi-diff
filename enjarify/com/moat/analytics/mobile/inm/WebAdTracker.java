package com.moat.analytics.mobile.inm;

import android.app.Activity;

public abstract interface WebAdTracker
{
  public abstract void setActivity(Activity paramActivity);
  
  public abstract void startTracking();
  
  public abstract void stopTracking();
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.WebAdTracker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */