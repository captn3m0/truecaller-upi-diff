package com.moat.analytics.mobile.inm;

import android.app.Application;

public abstract class MoatAnalytics
{
  private static MoatAnalytics a;
  
  public static MoatAnalytics getInstance()
  {
    synchronized (MoatAnalytics.class)
    {
      Object localObject1 = a;
      if (localObject1 == null) {
        try
        {
          localObject1 = new com/moat/analytics/mobile/inm/k;
          ((k)localObject1).<init>();
          a = (MoatAnalytics)localObject1;
        }
        catch (Exception localException)
        {
          m.a(localException);
          localObject2 = new com/moat/analytics/mobile/inm/v$a;
          ((v.a)localObject2).<init>();
          a = (MoatAnalytics)localObject2;
        }
      }
      Object localObject2 = a;
      return (MoatAnalytics)localObject2;
    }
  }
  
  public abstract void prepareNativeDisplayTracking(String paramString);
  
  public abstract void start(Application paramApplication);
  
  public abstract void start(MoatOptions paramMoatOptions, Application paramApplication);
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.MoatAnalytics
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */