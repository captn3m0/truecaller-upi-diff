package com.moat.analytics.mobile.inm;

import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.moat.analytics.mobile.inm.a.b.a;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

class ab
{
  static a a(ViewGroup paramViewGroup)
  {
    boolean bool1 = paramViewGroup instanceof WebView;
    if (bool1) {
      return a.a((WebView)paramViewGroup);
    }
    LinkedList localLinkedList = new java/util/LinkedList;
    localLinkedList.<init>();
    localLinkedList.add(paramViewGroup);
    paramViewGroup = new java/util/HashSet;
    paramViewGroup.<init>();
    int i = 0;
    int j = 0;
    Object localObject1 = null;
    label49:
    boolean bool2 = localLinkedList.isEmpty();
    if (!bool2)
    {
      int k = 100;
      if (i < k)
      {
        i += 1;
        Object localObject2 = (ViewGroup)localLinkedList.poll();
        Object localObject3 = new com/moat/analytics/mobile/inm/ab$a;
        ((ab.a)localObject3).<init>((ViewGroup)localObject2, null);
        localObject2 = ((ab.a)localObject3).iterator();
        for (;;)
        {
          boolean bool3 = ((Iterator)localObject2).hasNext();
          if (!bool3) {
            break label49;
          }
          localObject3 = (View)((Iterator)localObject2).next();
          boolean bool4 = localObject3 instanceof WebView;
          if (bool4) {
            if (localObject1 == null)
            {
              localObject1 = localObject3;
              localObject1 = (WebView)localObject3;
            }
            else
            {
              j = 3;
              String str = "Ambiguous ad container: multiple WebViews reside within it.";
              p.a(j, "WebViewHound", localObject3, str);
              localObject1 = "[ERROR] ";
              localObject2 = "WebAdTracker not created, ambiguous ad container: multiple WebViews reside within it";
              p.a((String)localObject1, (String)localObject2);
              break;
            }
          }
          bool4 = localObject3 instanceof ViewGroup;
          if (bool4)
          {
            localObject3 = (ViewGroup)localObject3;
            bool4 = paramViewGroup.contains(localObject3);
            if (!bool4)
            {
              paramViewGroup.add(localObject3);
              localLinkedList.add(localObject3);
            }
          }
        }
      }
    }
    return a.b(localObject1);
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */