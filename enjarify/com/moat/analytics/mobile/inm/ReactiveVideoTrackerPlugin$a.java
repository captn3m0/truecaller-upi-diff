package com.moat.analytics.mobile.inm;

import android.app.Activity;
import android.view.View;
import java.util.Map;

class ReactiveVideoTrackerPlugin$a
  implements ReactiveVideoTracker
{
  public void changeTargetView(View paramView) {}
  
  public void dispatchEvent(MoatAdEvent paramMoatAdEvent) {}
  
  public void setActivity(Activity paramActivity) {}
  
  public void setPlayerVolume(Double paramDouble) {}
  
  public void stopTracking() {}
  
  public boolean trackVideoAd(Map paramMap, Integer paramInteger, View paramView)
  {
    return false;
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.ReactiveVideoTrackerPlugin.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */