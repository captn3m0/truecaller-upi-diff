package com.moat.analytics.mobile.inm;

public enum NativeDisplayTracker$MoatUserInteractionType
{
  static
  {
    Object localObject = new com/moat/analytics/mobile/inm/NativeDisplayTracker$MoatUserInteractionType;
    ((MoatUserInteractionType)localObject).<init>("TOUCH", 0);
    TOUCH = (MoatUserInteractionType)localObject;
    localObject = new com/moat/analytics/mobile/inm/NativeDisplayTracker$MoatUserInteractionType;
    int i = 1;
    ((MoatUserInteractionType)localObject).<init>("CLICK", i);
    CLICK = (MoatUserInteractionType)localObject;
    localObject = new MoatUserInteractionType[2];
    MoatUserInteractionType localMoatUserInteractionType = TOUCH;
    localObject[0] = localMoatUserInteractionType;
    localMoatUserInteractionType = CLICK;
    localObject[i] = localMoatUserInteractionType;
    $VALUES = (MoatUserInteractionType[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.NativeDisplayTracker.MoatUserInteractionType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */