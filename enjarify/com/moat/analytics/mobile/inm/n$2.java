package com.moat.analytics.mobile.inm;

import android.view.ViewGroup;
import android.webkit.WebView;
import com.moat.analytics.mobile.inm.a.b.a;
import java.lang.ref.WeakReference;

class n$2
  implements x.a
{
  n$2(n paramn, WeakReference paramWeakReference) {}
  
  public a a()
  {
    Object localObject1 = (ViewGroup)a.get();
    int i = 3;
    if (localObject1 == null)
    {
      p.a(i, "Factory", this, "Target ViewGroup is null. Not creating WebAdTracker.");
      p.a("[ERROR] ", "WebAdTracker not created, adContainer ViewGroup is null");
      return a.a();
    }
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Creating WebAdTracker for ");
    Object localObject3 = localObject1.getClass().getSimpleName();
    ((StringBuilder)localObject2).append((String)localObject3);
    ((StringBuilder)localObject2).append("@");
    int j = localObject1.hashCode();
    ((StringBuilder)localObject2).append(j);
    localObject2 = ((StringBuilder)localObject2).toString();
    p.a(i, "Factory", this, (String)localObject2);
    String str1 = "[INFO] ";
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Attempting to create WebAdTracker for ");
    localObject3 = localObject1.getClass().getSimpleName();
    ((StringBuilder)localObject2).append((String)localObject3);
    ((StringBuilder)localObject2).append("@");
    j = localObject1.hashCode();
    ((StringBuilder)localObject2).append(j);
    localObject2 = ((StringBuilder)localObject2).toString();
    p.a(str1, (String)localObject2);
    localObject1 = ab.a((ViewGroup)localObject1);
    boolean bool = ((a)localObject1).c();
    localObject2 = "Factory";
    localObject3 = new java/lang/StringBuilder;
    String str2 = "WebView ";
    ((StringBuilder)localObject3).<init>(str2);
    if (bool) {
      str2 = "";
    } else {
      str2 = "not ";
    }
    ((StringBuilder)localObject3).append(str2);
    str2 = "found inside of ad container.";
    ((StringBuilder)localObject3).append(str2);
    localObject3 = ((StringBuilder)localObject3).toString();
    p.a(i, (String)localObject2, this, (String)localObject3);
    if (!bool)
    {
      localObject4 = "[ERROR] ";
      str1 = "WebAdTracker not created, no WebView to track inside of ad container";
      p.a((String)localObject4, str1);
    }
    Object localObject4 = new com/moat/analytics/mobile/inm/aa;
    localObject1 = (WebView)((a)localObject1).c(null);
    ((aa)localObject4).<init>((WebView)localObject1);
    return a.a(localObject4);
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.n.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */