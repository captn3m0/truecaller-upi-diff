package com.moat.analytics.mobile.inm;

import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Iterator;
import java.util.LinkedList;

class x
  implements InvocationHandler
{
  private static final Object[] a = new Object[0];
  private final x.a b;
  private final Class c;
  private final LinkedList d;
  private boolean e;
  private Object f;
  
  x(x.a parama, Class paramClass)
  {
    com.moat.analytics.mobile.inm.a.a.a.a(parama);
    com.moat.analytics.mobile.inm.a.a.a.a(paramClass);
    b = parama;
    c = paramClass;
    parama = new java/util/LinkedList;
    parama.<init>();
    d = parama;
    parama = w.a();
    paramClass = new com/moat/analytics/mobile/inm/x$1;
    paramClass.<init>(this);
    parama.a(paramClass);
  }
  
  static Object a(x.a parama, Class paramClass)
  {
    ClassLoader localClassLoader = paramClass.getClassLoader();
    x localx = new com/moat/analytics/mobile/inm/x;
    localx.<init>(parama, paramClass);
    parama = new Class[1];
    parama[0] = paramClass;
    return Proxy.newProxyInstance(localClassLoader, parama, localx);
  }
  
  private Object a(Method paramMethod)
  {
    try
    {
      Class localClass = Boolean.TYPE;
      paramMethod = paramMethod.getReturnType();
      boolean bool = localClass.equals(paramMethod);
      if (bool) {
        return Boolean.TRUE;
      }
    }
    catch (Exception paramMethod)
    {
      m.a(paramMethod);
    }
    return null;
  }
  
  private Object a(Method paramMethod, Object[] paramArrayOfObject)
  {
    Object localObject1 = paramMethod.getDeclaringClass();
    Object localObject2 = w.a();
    Object localObject3 = Object.class;
    boolean bool1 = localObject3.equals(localObject1);
    if (bool1)
    {
      localObject1 = paramMethod.getName();
      localObject2 = "getClass";
      boolean bool2 = ((String)localObject2).equals(localObject1);
      if (bool2) {
        return c;
      }
      localObject2 = "toString";
      bool1 = ((String)localObject2).equals(localObject1);
      if (bool1)
      {
        paramMethod = paramMethod.invoke(this, paramArrayOfObject);
        paramArrayOfObject = x.class.getName();
        localObject1 = c.getName();
        return String.valueOf(paramMethod).replace(paramArrayOfObject, (CharSequence)localObject1);
      }
      return paramMethod.invoke(this, paramArrayOfObject);
    }
    bool1 = e;
    if (bool1)
    {
      localObject1 = f;
      if (localObject1 == null)
      {
        d.clear();
        return a(paramMethod);
      }
    }
    localObject1 = a;
    localObject3 = w.d.b;
    if (localObject1 == localObject3)
    {
      c();
      localObject1 = f;
      if (localObject1 != null) {
        return paramMethod.invoke(localObject1, paramArrayOfObject);
      }
    }
    localObject1 = a;
    localObject2 = w.d.a;
    if (localObject1 == localObject2)
    {
      bool1 = e;
      if (bool1)
      {
        localObject1 = f;
        if (localObject1 == null) {}
      }
      else
      {
        b(paramMethod, paramArrayOfObject);
      }
    }
    return a(paramMethod);
  }
  
  private void b()
  {
    boolean bool = e;
    if (!bool)
    {
      try
      {
        Object localObject = b;
        localObject = ((x.a)localObject).a();
        str1 = null;
        localObject = ((com.moat.analytics.mobile.inm.a.b.a)localObject).c(null);
        f = localObject;
      }
      catch (Exception localException)
      {
        String str1 = "OnOffTrackerProxy";
        String str2 = "Could not create instance";
        p.a(str1, this, str2, localException);
        m.a(localException);
      }
      bool = true;
      e = bool;
    }
  }
  
  private void b(Method paramMethod, Object[] paramArrayOfObject)
  {
    LinkedList localLinkedList = d;
    int i = localLinkedList.size();
    int j = 15;
    if (i >= j)
    {
      localLinkedList = d;
      j = 5;
      localLinkedList.remove(j);
    }
    localLinkedList = d;
    x.b localb = new com/moat/analytics/mobile/inm/x$b;
    localb.<init>(this, paramMethod, paramArrayOfObject, null);
    localLinkedList.add(localb);
  }
  
  private void c()
  {
    b();
    Object localObject1 = f;
    if (localObject1 == null) {
      return;
    }
    localObject1 = d.iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      Object localObject2 = (x.b)((Iterator)localObject1).next();
      try
      {
        Object localObject3 = x.b.a((x.b)localObject2);
        int i = localObject3.length;
        localObject3 = new Object[i];
        Object localObject4 = x.b.a((x.b)localObject2);
        int j = localObject4.length;
        int k = 0;
        int n;
        for (int m = 0; k < j; m = n)
        {
          Object localObject5 = localObject4[k];
          n = m + 1;
          localObject5 = ((WeakReference)localObject5).get();
          localObject3[m] = localObject5;
          k += 1;
        }
        localObject2 = x.b.b((x.b)localObject2);
        localObject4 = f;
        ((Method)localObject2).invoke(localObject4, (Object[])localObject3);
      }
      catch (Exception localException)
      {
        m.a(localException);
      }
    }
    d.clear();
  }
  
  public Object invoke(Object paramObject, Method paramMethod, Object[] paramArrayOfObject)
  {
    try
    {
      return a(paramMethod, paramArrayOfObject);
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
    return a(paramMethod);
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */