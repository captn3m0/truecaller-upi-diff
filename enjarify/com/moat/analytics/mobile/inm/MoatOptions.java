package com.moat.analytics.mobile.inm;

public class MoatOptions
{
  public boolean autoTrackGMAInterstitials = false;
  public boolean disableAdIdCollection = false;
  public boolean disableLocationServices = false;
  public boolean loggingEnabled = false;
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.MoatOptions
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */