package com.moat.analytics.mobile.inm;

import android.view.View;
import com.moat.analytics.mobile.inm.a.b.a;
import java.lang.ref.WeakReference;
import java.util.Map;

class n$3
  implements x.a
{
  n$3(n paramn, WeakReference paramWeakReference, Map paramMap) {}
  
  public a a()
  {
    View localView = (View)a.get();
    int i = 3;
    if (localView == null)
    {
      p.a(i, "Factory", this, "Target view is null. Not creating NativeDisplayTracker.");
      p.a("[ERROR] ", "NativeDisplayTracker creation failed, subject view is null");
      return a.a();
    }
    Object localObject1 = b;
    if (localObject1 != null)
    {
      boolean bool = ((Map)localObject1).isEmpty();
      if (!bool)
      {
        Object localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("Creating NativeDisplayTracker for ");
        String str = localView.getClass().getSimpleName();
        ((StringBuilder)localObject2).append(str);
        ((StringBuilder)localObject2).append("@");
        int j = localView.hashCode();
        ((StringBuilder)localObject2).append(j);
        localObject2 = ((StringBuilder)localObject2).toString();
        p.a(i, "Factory", this, (String)localObject2);
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>("Attempting to create NativeDisplayTracker for ");
        localObject2 = localView.getClass().getSimpleName();
        ((StringBuilder)localObject1).append((String)localObject2);
        ((StringBuilder)localObject1).append("@");
        int k = localView.hashCode();
        ((StringBuilder)localObject1).append(k);
        localObject1 = ((StringBuilder)localObject1).toString();
        p.a("[INFO] ", (String)localObject1);
        t localt = new com/moat/analytics/mobile/inm/t;
        localObject1 = b;
        localt.<init>(localView, (Map)localObject1);
        return a.a(localt);
      }
    }
    p.a(i, "Factory", this, "adIds is null or empty. NativeDisplayTracker initialization failed.");
    p.a("[ERROR] ", "NativeDisplayTracker creation failed, adIds is null or empty");
    return a.a();
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.n.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */