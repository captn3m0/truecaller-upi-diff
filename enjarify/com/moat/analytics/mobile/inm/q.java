package com.moat.analytics.mobile.inm;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

class q
{
  /* Error */
  static com.moat.analytics.mobile.inm.a.b.a a(String paramString)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: new 10	java/net/URL
    //   5: astore_2
    //   6: aload_2
    //   7: aload_0
    //   8: invokespecial 13	java/net/URL:<init>	(Ljava/lang/String;)V
    //   11: aload_2
    //   12: invokevirtual 17	java/net/URL:openConnection	()Ljava/net/URLConnection;
    //   15: astore_0
    //   16: aload_0
    //   17: invokestatic 23	com/google/firebase/perf/network/FirebasePerfUrlConnection:instrument	(Ljava/lang/Object;)Ljava/lang/Object;
    //   20: astore_0
    //   21: aload_0
    //   22: checkcast 25	java/net/URLConnection
    //   25: astore_0
    //   26: aload_0
    //   27: checkcast 27	java/net/HttpURLConnection
    //   30: astore_0
    //   31: iconst_0
    //   32: istore_3
    //   33: aconst_null
    //   34: astore_2
    //   35: aload_0
    //   36: iconst_0
    //   37: invokevirtual 31	java/net/HttpURLConnection:setUseCaches	(Z)V
    //   40: sipush 10000
    //   43: istore_3
    //   44: aload_0
    //   45: iload_3
    //   46: invokevirtual 36	java/net/HttpURLConnection:setReadTimeout	(I)V
    //   49: sipush 15000
    //   52: istore_3
    //   53: aload_0
    //   54: iload_3
    //   55: invokevirtual 40	java/net/HttpURLConnection:setConnectTimeout	(I)V
    //   58: ldc 42
    //   60: astore_2
    //   61: aload_0
    //   62: aload_2
    //   63: invokevirtual 45	java/net/HttpURLConnection:setRequestMethod	(Ljava/lang/String;)V
    //   66: iconst_1
    //   67: istore_3
    //   68: aload_0
    //   69: iload_3
    //   70: invokevirtual 49	java/net/HttpURLConnection:setDoInput	(Z)V
    //   73: aload_0
    //   74: invokevirtual 52	java/net/HttpURLConnection:connect	()V
    //   77: aload_0
    //   78: invokevirtual 56	java/net/HttpURLConnection:getResponseCode	()I
    //   81: istore_3
    //   82: sipush 400
    //   85: istore 4
    //   87: iload_3
    //   88: iload 4
    //   90: if_icmplt +7 -> 97
    //   93: invokestatic 63	com/moat/analytics/mobile/inm/a/b/a:a	()Lcom/moat/analytics/mobile/inm/a/b/a;
    //   96: areturn
    //   97: aload_0
    //   98: invokevirtual 67	java/net/HttpURLConnection:getInputStream	()Ljava/io/InputStream;
    //   101: astore_1
    //   102: aload_1
    //   103: invokestatic 70	com/moat/analytics/mobile/inm/q:a	(Ljava/io/InputStream;)Ljava/lang/String;
    //   106: astore_0
    //   107: aload_0
    //   108: invokestatic 73	com/moat/analytics/mobile/inm/a/b/a:a	(Ljava/lang/Object;)Lcom/moat/analytics/mobile/inm/a/b/a;
    //   111: astore_0
    //   112: aload_1
    //   113: ifnull +7 -> 120
    //   116: aload_1
    //   117: invokevirtual 78	java/io/InputStream:close	()V
    //   120: aload_0
    //   121: areturn
    //   122: astore_0
    //   123: goto +18 -> 141
    //   126: pop
    //   127: invokestatic 63	com/moat/analytics/mobile/inm/a/b/a:a	()Lcom/moat/analytics/mobile/inm/a/b/a;
    //   130: astore_0
    //   131: aload_1
    //   132: ifnull +7 -> 139
    //   135: aload_1
    //   136: invokevirtual 78	java/io/InputStream:close	()V
    //   139: aload_0
    //   140: areturn
    //   141: aload_1
    //   142: ifnull +7 -> 149
    //   145: aload_1
    //   146: invokevirtual 78	java/io/InputStream:close	()V
    //   149: aload_0
    //   150: athrow
    //   151: pop
    //   152: goto -32 -> 120
    //   155: pop
    //   156: goto -17 -> 139
    //   159: pop
    //   160: goto -11 -> 149
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	163	0	paramString	String
    //   1	145	1	localInputStream	InputStream
    //   5	58	2	localObject	Object
    //   32	38	3	i	int
    //   81	10	3	j	int
    //   85	6	4	k	int
    //   126	1	6	localIOException1	java.io.IOException
    //   151	1	7	localIOException2	java.io.IOException
    //   155	1	8	localIOException3	java.io.IOException
    //   159	1	9	localIOException4	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   2	5	122	finally
    //   7	11	122	finally
    //   11	15	122	finally
    //   16	20	122	finally
    //   21	25	122	finally
    //   26	30	122	finally
    //   36	40	122	finally
    //   45	49	122	finally
    //   54	58	122	finally
    //   62	66	122	finally
    //   69	73	122	finally
    //   73	77	122	finally
    //   77	81	122	finally
    //   93	96	122	finally
    //   97	101	122	finally
    //   102	106	122	finally
    //   107	111	122	finally
    //   127	130	122	finally
    //   2	5	126	java/io/IOException
    //   7	11	126	java/io/IOException
    //   11	15	126	java/io/IOException
    //   16	20	126	java/io/IOException
    //   21	25	126	java/io/IOException
    //   26	30	126	java/io/IOException
    //   36	40	126	java/io/IOException
    //   45	49	126	java/io/IOException
    //   54	58	126	java/io/IOException
    //   62	66	126	java/io/IOException
    //   69	73	126	java/io/IOException
    //   73	77	126	java/io/IOException
    //   77	81	126	java/io/IOException
    //   93	96	126	java/io/IOException
    //   97	101	126	java/io/IOException
    //   102	106	126	java/io/IOException
    //   107	111	126	java/io/IOException
    //   116	120	151	java/io/IOException
    //   135	139	155	java/io/IOException
    //   145	149	159	java/io/IOException
  }
  
  private static String a(InputStream paramInputStream)
  {
    int i = 256;
    char[] arrayOfChar = new char[i];
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    InputStreamReader localInputStreamReader = new java/io/InputStreamReader;
    localInputStreamReader.<init>(paramInputStream, "UTF-8");
    paramInputStream = null;
    int j = 0;
    int k;
    do
    {
      k = localInputStreamReader.read(arrayOfChar, 0, i);
      if (k <= 0) {
        break;
      }
      j += k;
      localStringBuilder.append(arrayOfChar, 0, k);
      k = 1024;
    } while (j < k);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */