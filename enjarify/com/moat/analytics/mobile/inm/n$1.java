package com.moat.analytics.mobile.inm;

import android.webkit.WebView;
import com.moat.analytics.mobile.inm.a.b.a;
import java.lang.ref.WeakReference;

class n$1
  implements x.a
{
  n$1(n paramn, WeakReference paramWeakReference) {}
  
  public a a()
  {
    WebView localWebView = (WebView)a.get();
    int i = 3;
    if (localWebView == null)
    {
      p.a(i, "Factory", this, "Target ViewGroup is null. Not creating WebAdTracker.");
      p.a("[ERROR] ", "WebAdTracker not created, webView is null");
      return a.a();
    }
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("Creating WebAdTracker for ");
    String str = localWebView.getClass().getSimpleName();
    ((StringBuilder)localObject1).append(str);
    ((StringBuilder)localObject1).append("@");
    int j = localWebView.hashCode();
    ((StringBuilder)localObject1).append(j);
    localObject1 = ((StringBuilder)localObject1).toString();
    p.a(i, "Factory", this, (String)localObject1);
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Attempting to create WebAdTracker for ");
    localObject1 = localWebView.getClass().getSimpleName();
    ((StringBuilder)localObject2).append((String)localObject1);
    ((StringBuilder)localObject2).append("@");
    int k = localWebView.hashCode();
    ((StringBuilder)localObject2).append(k);
    localObject2 = ((StringBuilder)localObject2).toString();
    p.a("[INFO] ", (String)localObject2);
    aa localaa = new com/moat/analytics/mobile/inm/aa;
    localaa.<init>(localWebView);
    return a.a(localaa);
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.n.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */