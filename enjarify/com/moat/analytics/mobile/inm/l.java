package com.moat.analytics.mobile.inm;

import android.os.Build.VERSION;
import org.json.JSONArray;
import org.json.JSONObject;

class l
{
  private boolean a = false;
  private boolean b = false;
  private boolean c = false;
  private int d = 200;
  
  l(String paramString)
  {
    a(paramString);
  }
  
  private void a(String paramString)
  {
    if (paramString == null) {
      return;
    }
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>(paramString);
      paramString = "sa";
      paramString = localJSONObject.getString(paramString);
      String str1 = "3f2ae9c1894282b5e0222f0d06bbf457191f816f";
      boolean bool1 = paramString.equals(str1);
      String str2 = "8f1d08a2d6496191a5ebae8f0590f513e2619489";
      boolean bool2 = paramString.equals(str2);
      String str3 = "on";
      boolean bool3 = paramString.equals(str3);
      boolean bool5 = true;
      if ((bool3) || (bool1) || (bool2))
      {
        bool3 = a(localJSONObject);
        if (!bool3)
        {
          bool3 = b(localJSONObject);
          if (!bool3)
          {
            a = bool5;
            b = bool1;
            c = bool2;
            bool3 = c;
            if (bool3) {
              b = bool5;
            }
          }
        }
      }
      paramString = "in";
      bool3 = localJSONObject.has(paramString);
      if (bool3)
      {
        paramString = "in";
        int j = localJSONObject.getInt(paramString);
        int i = 100;
        if (j >= i)
        {
          i = 1000;
          if (j <= i) {
            d = j;
          }
        }
      }
      boolean bool4 = c(localJSONObject);
      if (bool4)
      {
        paramString = MoatAnalytics.getInstance();
        paramString = (k)paramString;
        c = bool5;
      }
      return;
    }
    catch (Exception paramString)
    {
      a = false;
      b = false;
      d = 200;
      m.a(paramString);
    }
  }
  
  private boolean a(JSONObject paramJSONObject)
  {
    int i = 16;
    boolean bool2 = true;
    try
    {
      int k = Build.VERSION.SDK_INT;
      if (i > k) {
        return bool2;
      }
      String str = "ob";
      boolean bool1 = paramJSONObject.has(str);
      k = 0;
      if (bool1)
      {
        str = "ob";
        paramJSONObject = paramJSONObject.getJSONArray(str);
        int j = paramJSONObject.length();
        int m = 0;
        while (m < j)
        {
          int n = paramJSONObject.getInt(m);
          int i1 = Build.VERSION.SDK_INT;
          if (n == i1) {
            return bool2;
          }
          m += 1;
        }
      }
      return false;
    }
    catch (Exception localException) {}
    return bool2;
  }
  
  private boolean b(JSONObject paramJSONObject)
  {
    Object localObject = "ap";
    try
    {
      boolean bool1 = paramJSONObject.has((String)localObject);
      if (bool1)
      {
        localObject = new com/moat/analytics/mobile/inm/s$a;
        ((s.a)localObject).<init>();
        localObject = ((s.a)localObject).b();
        String str1 = "ap";
        paramJSONObject = paramJSONObject.getJSONArray(str1);
        int i = paramJSONObject.length();
        int j = 0;
        while (j < i)
        {
          String str2 = paramJSONObject.getString(j);
          boolean bool2 = str2.contentEquals((CharSequence)localObject);
          if (bool2) {
            return true;
          }
          j += 1;
        }
      }
      return false;
    }
    catch (Exception paramJSONObject)
    {
      m.a(paramJSONObject);
    }
  }
  
  private boolean c(JSONObject paramJSONObject)
  {
    Object localObject = "al";
    try
    {
      boolean bool1 = paramJSONObject.has((String)localObject);
      if (bool1)
      {
        localObject = new com/moat/analytics/mobile/inm/s$a;
        ((s.a)localObject).<init>();
        localObject = ((s.a)localObject).b();
        String str1 = "al";
        paramJSONObject = paramJSONObject.getJSONArray(str1);
        int i = paramJSONObject.length();
        int j = 0;
        while (j < i)
        {
          String str2 = paramJSONObject.getString(j);
          boolean bool2 = str2.contentEquals((CharSequence)localObject);
          if (bool2) {
            return true;
          }
          j += 1;
        }
      }
      return false;
    }
    catch (Exception paramJSONObject)
    {
      m.a(paramJSONObject);
    }
  }
  
  boolean a()
  {
    return b;
  }
  
  boolean b()
  {
    return c;
  }
  
  int c()
  {
    return d;
  }
  
  w.d d()
  {
    boolean bool = a;
    if (bool) {
      return w.d.b;
    }
    return w.d.a;
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */