package com.moat.analytics.mobile.inm;

import android.content.Context;
import android.os.Build.VERSION;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

class g
{
  WebView a;
  j b;
  final String c;
  private final g.a d;
  private boolean e;
  
  g(Context paramContext, g.a parama)
  {
    d = parama;
    int i = 0;
    Object localObject1 = null;
    e = false;
    Object localObject2 = Locale.ROOT;
    String str = "_moatTracker%d";
    int j = 1;
    Object[] arrayOfObject = new Object[j];
    double d1 = Math.random();
    double d2 = 1.0E8D;
    d1 *= d2;
    int k = (int)d1;
    Integer localInteger = Integer.valueOf(k);
    arrayOfObject[0] = localInteger;
    localObject2 = String.format((Locale)localObject2, str, arrayOfObject);
    c = ((String)localObject2);
    localObject2 = new android/webkit/WebView;
    ((WebView)localObject2).<init>(paramContext);
    a = ((WebView)localObject2);
    paramContext = a.getSettings();
    paramContext.setJavaScriptEnabled(j);
    paramContext.setAllowContentAccess(false);
    paramContext.setAllowFileAccess(false);
    paramContext.setDatabaseEnabled(false);
    paramContext.setDomStorageEnabled(false);
    paramContext.setGeolocationEnabled(false);
    paramContext.setJavaScriptCanOpenWindowsAutomatically(false);
    paramContext.setSaveFormData(false);
    int m = Build.VERSION.SDK_INT;
    int n = 16;
    if (m >= n)
    {
      paramContext.setAllowFileAccessFromFileURLs(false);
      paramContext.setAllowUniversalAccessFromFileURLs(false);
    }
    i = Build.VERSION.SDK_INT;
    m = 21;
    if (i >= m) {
      paramContext.setMixedContentMode(j);
    }
    paramContext = j.a.b;
    localObject1 = g.a.b;
    if (parama == localObject1) {
      paramContext = j.a.c;
    }
    parama = new com/moat/analytics/mobile/inm/j;
    localObject1 = a;
    parama.<init>((WebView)localObject1, paramContext);
    b = parama;
  }
  
  private static String a(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2, JSONObject paramJSONObject, Integer paramInteger3)
  {
    Locale localLocale = Locale.ROOT;
    Object[] arrayOfObject = new Object[9];
    arrayOfObject[0] = "mianahwvc";
    arrayOfObject[1] = paramInteger1;
    arrayOfObject[2] = paramInteger2;
    arrayOfObject[3] = "mianahwvc";
    paramInteger1 = Long.valueOf(System.currentTimeMillis());
    arrayOfObject[4] = paramInteger1;
    arrayOfObject[5] = paramString1;
    arrayOfObject[6] = paramString2;
    paramString1 = paramJSONObject.toString();
    arrayOfObject[7] = paramString1;
    arrayOfObject[8] = paramInteger3;
    return String.format(localLocale, "<html><head></head><body><div id=\"%s\" style=\"width: %dpx; height: %dpx;\"></div><script>(function initMoatTracking(apiname, pcode, ids, duration) {var events = [];window[pcode + '_moatElToTrack'] = document.getElementById('%s');var moatapi = {'dropTime':%d,'adData': {'ids': ids, 'duration': duration, 'url': 'n/a'},'dispatchEvent': function(ev) {if (this.sendEvent) {if (events) { events.push(ev); ev = events; events = false; }this.sendEvent(ev);} else {events.push(ev);}},'dispatchMany': function(evs){for (var i=0, l=evs.length; i<l; i++) {this.dispatchEvent(evs[i]);}}};Object.defineProperty(window, apiname, {'value': moatapi});var s = document.createElement('script');s.src = 'https://z.moatads.com/' + pcode + '/moatvideo.js?' + apiname + '#' + apiname;document.body.appendChild(s);})('%s', '%s', %s, %s);</script></body></html>", arrayOfObject);
  }
  
  private static String b(String paramString)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("<!DOCTYPE html>\n<html>\n<head lang=\"en\">\n   <meta charset=\"UTF-8\">\n   <title></title>\n</head>\n<body style=\"margin:0;padding:0;\">\n    <script src=\"https://z.moatads.com/");
    localStringBuilder.append(paramString);
    localStringBuilder.append("/moatad.js\" type=\"text/javascript\"></script>\n</body>\n</html>");
    return localStringBuilder.toString();
  }
  
  void a()
  {
    b = null;
    a.destroy();
    a = null;
  }
  
  void a(String paramString)
  {
    Object localObject1 = d;
    Object localObject2 = g.a.a;
    if (localObject1 == localObject2)
    {
      localObject1 = a;
      localObject2 = new com/moat/analytics/mobile/inm/g$1;
      ((g.1)localObject2).<init>(this);
      ((WebView)localObject1).setWebViewClient((WebViewClient)localObject2);
      localObject1 = a;
      paramString = b(paramString);
      localObject2 = "text/html";
      String str = "utf-8";
      ((WebView)localObject1).loadData(paramString, (String)localObject2, str);
    }
  }
  
  void a(String paramString, Map paramMap, Integer paramInteger1, Integer paramInteger2, Integer paramInteger3)
  {
    Object localObject1 = d;
    Object localObject2 = g.a.b;
    if (localObject1 == localObject2)
    {
      localObject1 = a;
      localObject2 = new com/moat/analytics/mobile/inm/g$2;
      ((g.2)localObject2).<init>(this);
      ((WebView)localObject1).setWebViewClient((WebViewClient)localObject2);
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>(paramMap);
      paramMap = a;
      String str = c;
      paramString = a(str, paramString, paramInteger1, paramInteger2, localJSONObject, paramInteger3);
      paramInteger1 = "text/html";
      paramInteger2 = null;
      paramMap.loadData(paramString, paramInteger1, null);
    }
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */