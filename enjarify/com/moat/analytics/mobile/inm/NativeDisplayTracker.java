package com.moat.analytics.mobile.inm;

import android.app.Activity;

public abstract interface NativeDisplayTracker
{
  public abstract void reportUserInteractionEvent(NativeDisplayTracker.MoatUserInteractionType paramMoatUserInteractionType);
  
  public abstract void setActivity(Activity paramActivity);
  
  public abstract void startTracking();
  
  public abstract void stopTracking();
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.NativeDisplayTracker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */