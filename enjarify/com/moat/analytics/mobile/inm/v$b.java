package com.moat.analytics.mobile.inm;

import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import java.util.Map;

public class v$b
  extends MoatFactory
{
  public Object createCustomTracker(MoatPlugin paramMoatPlugin)
  {
    return null;
  }
  
  public NativeDisplayTracker createNativeDisplayTracker(View paramView, Map paramMap)
  {
    paramView = new com/moat/analytics/mobile/inm/v$c;
    paramView.<init>();
    return paramView;
  }
  
  public NativeVideoTracker createNativeVideoTracker(String paramString)
  {
    paramString = new com/moat/analytics/mobile/inm/v$d;
    paramString.<init>();
    return paramString;
  }
  
  public WebAdTracker createWebAdTracker(ViewGroup paramViewGroup)
  {
    paramViewGroup = new com/moat/analytics/mobile/inm/v$e;
    paramViewGroup.<init>();
    return paramViewGroup;
  }
  
  public WebAdTracker createWebAdTracker(WebView paramWebView)
  {
    paramWebView = new com/moat/analytics/mobile/inm/v$e;
    paramWebView.<init>();
    return paramWebView;
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.v.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */