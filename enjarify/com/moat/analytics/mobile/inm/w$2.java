package com.moat.analytics.mobile.inm;

import android.os.Handler;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;

class w$2
  implements Runnable
{
  w$2(w paramw) {}
  
  public void run()
  {
    try
    {
      Object localObject = w.c();
      int i = ((Queue)localObject).size();
      if (i > 0)
      {
        localObject = a;
        w.a((w)localObject);
        localObject = a;
        localObject = w.b((w)localObject);
        long l = 60000L;
        ((Handler)localObject).postDelayed(this, l);
        return;
      }
      localObject = a;
      localObject = w.c((w)localObject);
      boolean bool = true;
      ((AtomicBoolean)localObject).compareAndSet(bool, false);
      localObject = a;
      localObject = w.b((w)localObject);
      ((Handler)localObject).removeCallbacks(this);
      return;
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.w.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */