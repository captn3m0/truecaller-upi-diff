package com.moat.analytics.mobile.inm;

import android.app.Activity;
import android.view.View;
import java.util.Map;

public abstract interface ReactiveVideoTracker
{
  public abstract void changeTargetView(View paramView);
  
  public abstract void dispatchEvent(MoatAdEvent paramMoatAdEvent);
  
  public abstract void setActivity(Activity paramActivity);
  
  public abstract void setPlayerVolume(Double paramDouble);
  
  public abstract void stopTracking();
  
  public abstract boolean trackVideoAd(Map paramMap, Integer paramInteger, View paramView);
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.ReactiveVideoTracker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */