package com.moat.analytics.mobile.inm;

import android.view.View;
import android.view.ViewGroup;
import java.util.Iterator;

class ab$a$a
  implements Iterator
{
  private int b = -1;
  
  private ab$a$a(ab.a parama) {}
  
  public View a()
  {
    int i = b + 1;
    b = i;
    ViewGroup localViewGroup = ab.a.a(a);
    int j = b;
    return localViewGroup.getChildAt(j);
  }
  
  public boolean hasNext()
  {
    int i = b;
    int j = 1;
    i += j;
    ViewGroup localViewGroup = ab.a.a(a);
    int k = localViewGroup.getChildCount();
    if (i < k) {
      return j;
    }
    return false;
  }
  
  public void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Not implemented. Under development.");
    throw localUnsupportedOperationException;
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.ab.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */