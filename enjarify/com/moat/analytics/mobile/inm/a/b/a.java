package com.moat.analytics.mobile.inm.a.b;

import java.util.NoSuchElementException;

public final class a
{
  private static final a a;
  private final Object b;
  
  static
  {
    a locala = new com/moat/analytics/mobile/inm/a/b/a;
    locala.<init>();
    a = locala;
  }
  
  private a()
  {
    b = null;
  }
  
  private a(Object paramObject)
  {
    if (paramObject != null)
    {
      b = paramObject;
      return;
    }
    paramObject = new java/lang/NullPointerException;
    ((NullPointerException)paramObject).<init>("Optional of null value.");
    throw ((Throwable)paramObject);
  }
  
  public static a a()
  {
    return a;
  }
  
  public static a a(Object paramObject)
  {
    a locala = new com/moat/analytics/mobile/inm/a/b/a;
    locala.<init>(paramObject);
    return locala;
  }
  
  public static a b(Object paramObject)
  {
    if (paramObject == null) {
      return a();
    }
    return a(paramObject);
  }
  
  public final Object b()
  {
    Object localObject = b;
    if (localObject != null) {
      return localObject;
    }
    localObject = new java/util/NoSuchElementException;
    ((NoSuchElementException)localObject).<init>("No value present");
    throw ((Throwable)localObject);
  }
  
  public final Object c(Object paramObject)
  {
    Object localObject = b;
    if (localObject != null) {
      return localObject;
    }
    return paramObject;
  }
  
  public final boolean c()
  {
    Object localObject = b;
    return localObject != null;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof a;
    if (!bool2) {
      return false;
    }
    paramObject = (a)paramObject;
    Object localObject = b;
    paramObject = b;
    if (localObject != paramObject) {
      if ((localObject != null) && (paramObject != null))
      {
        boolean bool3 = localObject.equals(paramObject);
        if (bool3) {}
      }
      else
      {
        return false;
      }
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    Object localObject = b;
    if (localObject == null) {
      return 0;
    }
    return localObject.hashCode();
  }
  
  public final String toString()
  {
    Object localObject = b;
    if (localObject != null)
    {
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = localObject;
      return String.format("Optional[%s]", arrayOfObject);
    }
    return "Optional.empty";
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.a.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */