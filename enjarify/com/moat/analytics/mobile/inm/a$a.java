package com.moat.analytics.mobile.inm;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;
import java.lang.ref.WeakReference;

class a$a
  implements Application.ActivityLifecycleCallbacks
{
  private static void a(boolean paramBoolean)
  {
    w.d locald = null;
    int i = 3;
    String str;
    Object localObject;
    if (paramBoolean)
    {
      str = "App became visible";
      p.a(i, "ActivityState", null, str);
      localObject = aa;
      locald = w.d.b;
      if (localObject == locald)
      {
        localObject = (k)MoatAnalytics.getInstance();
        paramBoolean = c;
        if (!paramBoolean) {
          o.a().c();
        }
      }
    }
    else
    {
      str = "App became invisible";
      p.a(i, "ActivityState", null, str);
      localObject = aa;
      locald = w.d.b;
      if (localObject == locald)
      {
        localObject = (k)MoatAnalytics.getInstance();
        paramBoolean = c;
        if (!paramBoolean)
        {
          localObject = o.a();
          ((o)localObject).d();
        }
      }
    }
  }
  
  public void onActivityCreated(Activity paramActivity, Bundle paramBundle)
  {
    a.a(1);
  }
  
  public void onActivityDestroyed(Activity paramActivity)
  {
    try
    {
      int i = a.c();
      int k = 3;
      if (i != k)
      {
        i = a.c();
        int m = 5;
        if (i != m)
        {
          boolean bool1 = a.b();
          m = 0;
          localObject1 = null;
          if (bool1) {
            a(false);
          }
          a.a(false);
        }
      }
      int j = 6;
      a.a(j);
      String str = "ActivityState";
      Object localObject1 = new java/lang/StringBuilder;
      Object localObject2 = "Activity destroyed: ";
      ((StringBuilder)localObject1).<init>((String)localObject2);
      localObject2 = paramActivity.getClass();
      ((StringBuilder)localObject1).append(localObject2);
      localObject2 = "@";
      ((StringBuilder)localObject1).append((String)localObject2);
      int n = paramActivity.hashCode();
      ((StringBuilder)localObject1).append(n);
      localObject1 = ((StringBuilder)localObject1).toString();
      p.a(k, str, this, (String)localObject1);
      boolean bool2 = a.a(paramActivity);
      if (bool2)
      {
        paramActivity = new java/lang/ref/WeakReference;
        j = 0;
        str = null;
        paramActivity.<init>(null);
        a.a = paramActivity;
      }
      return;
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
  }
  
  public void onActivityPaused(Activity paramActivity)
  {
    int i = 4;
    try
    {
      a.a(i);
      boolean bool = a.a(paramActivity);
      if (bool)
      {
        WeakReference localWeakReference = new java/lang/ref/WeakReference;
        str = null;
        localWeakReference.<init>(null);
        a.a = localWeakReference;
      }
      int j = 3;
      String str = "ActivityState";
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      Object localObject = "Activity paused: ";
      localStringBuilder.<init>((String)localObject);
      localObject = paramActivity.getClass();
      localStringBuilder.append(localObject);
      localObject = "@";
      localStringBuilder.append((String)localObject);
      int k = paramActivity.hashCode();
      localStringBuilder.append(k);
      paramActivity = localStringBuilder.toString();
      p.a(j, str, this, paramActivity);
      return;
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
  }
  
  public void onActivityResumed(Activity paramActivity)
  {
    try
    {
      Object localObject1 = new java/lang/ref/WeakReference;
      ((WeakReference)localObject1).<init>(paramActivity);
      a.a = (WeakReference)localObject1;
      int i = 3;
      a.a(i);
      Object localObject2 = w.a();
      ((w)localObject2).b();
      localObject2 = "ActivityState";
      Object localObject3 = new java/lang/StringBuilder;
      Object localObject4 = "Activity resumed: ";
      ((StringBuilder)localObject3).<init>((String)localObject4);
      localObject4 = paramActivity.getClass();
      ((StringBuilder)localObject3).append(localObject4);
      localObject4 = "@";
      ((StringBuilder)localObject3).append((String)localObject4);
      int j = paramActivity.hashCode();
      ((StringBuilder)localObject3).append(j);
      localObject3 = ((StringBuilder)localObject3).toString();
      p.a(i, (String)localObject2, this, (String)localObject3);
      localObject1 = MoatAnalytics.getInstance();
      localObject1 = (k)localObject1;
      boolean bool = b;
      if (bool) {
        f.a(paramActivity);
      }
      return;
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
  }
  
  public void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle) {}
  
  public void onActivityStarted(Activity paramActivity)
  {
    try
    {
      WeakReference localWeakReference = new java/lang/ref/WeakReference;
      localWeakReference.<init>(paramActivity);
      a.a = localWeakReference;
      int i = 2;
      a.a(i);
      boolean bool1 = a.b();
      boolean bool2 = true;
      if (!bool1) {
        a(bool2);
      }
      a.a(bool2);
      int j = 3;
      String str = "ActivityState";
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      Object localObject = "Activity started: ";
      localStringBuilder.<init>((String)localObject);
      localObject = paramActivity.getClass();
      localStringBuilder.append(localObject);
      localObject = "@";
      localStringBuilder.append((String)localObject);
      int k = paramActivity.hashCode();
      localStringBuilder.append(k);
      paramActivity = localStringBuilder.toString();
      p.a(j, str, this, paramActivity);
      return;
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
  }
  
  public void onActivityStopped(Activity paramActivity)
  {
    try
    {
      int i = a.c();
      int j = 3;
      if (i != j)
      {
        i = 0;
        localObject1 = null;
        a.a(false);
        a(false);
      }
      i = 5;
      a.a(i);
      boolean bool = a.a(paramActivity);
      if (bool)
      {
        localObject1 = new java/lang/ref/WeakReference;
        localStringBuilder = null;
        ((WeakReference)localObject1).<init>(null);
        a.a = (WeakReference)localObject1;
      }
      Object localObject1 = "ActivityState";
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      Object localObject2 = "Activity stopped: ";
      localStringBuilder.<init>((String)localObject2);
      localObject2 = paramActivity.getClass();
      localStringBuilder.append(localObject2);
      localObject2 = "@";
      localStringBuilder.append((String)localObject2);
      int k = paramActivity.hashCode();
      localStringBuilder.append(k);
      paramActivity = localStringBuilder.toString();
      p.a(j, (String)localObject1, this, paramActivity);
      return;
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */