package com.moat.analytics.mobile.inm;

import android.content.Context;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;

final class s$1
  implements Runnable
{
  s$1(Context paramContext) {}
  
  public final void run()
  {
    try
    {
      Object localObject1 = a;
      localObject1 = AdvertisingIdClient.getAdvertisingIdInfo((Context)localObject1);
      boolean bool = ((AdvertisingIdClient.Info)localObject1).isLimitAdTrackingEnabled();
      int i = 3;
      if (!bool)
      {
        localObject1 = ((AdvertisingIdClient.Info)localObject1).getId();
        s.a((String)localObject1);
        localObject1 = "Util";
        localObject2 = new java/lang/StringBuilder;
        String str = "Retrieved Advertising ID = ";
        ((StringBuilder)localObject2).<init>(str);
        str = s.d();
        ((StringBuilder)localObject2).append(str);
        localObject2 = ((StringBuilder)localObject2).toString();
        p.a(i, (String)localObject1, this, (String)localObject2);
        return;
      }
      localObject1 = "Util";
      Object localObject2 = "User has limited ad tracking";
      p.a(i, (String)localObject1, this, (String)localObject2);
      return;
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.s.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */