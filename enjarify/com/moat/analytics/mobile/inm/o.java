package com.moat.analytics.mobile.inm;

import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.content.b;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

class o
  implements LocationListener
{
  private static o a;
  private ScheduledExecutorService b;
  private ScheduledFuture c;
  private ScheduledFuture d;
  private LocationManager e;
  private boolean f;
  private Location g;
  private boolean h;
  
  private o()
  {
    try
    {
      Object localObject = MoatAnalytics.getInstance();
      localObject = (k)localObject;
      int i = c;
      f = i;
      i = f;
      int k = 3;
      if (i != 0)
      {
        localObject = "LocationManager";
        str = "Moat location services disabled";
        p.a(k, (String)localObject, this, str);
        return;
      }
      i = 1;
      localObject = Executors.newScheduledThreadPool(i);
      b = ((ScheduledExecutorService)localObject);
      localObject = a.a();
      String str = "location";
      localObject = ((Application)localObject).getSystemService(str);
      localObject = (LocationManager)localObject;
      e = ((LocationManager)localObject);
      localObject = e;
      localObject = ((LocationManager)localObject).getAllProviders();
      int j = ((List)localObject).size();
      if (j == 0)
      {
        localObject = "LocationManager";
        str = "Device has no location providers";
        p.a(k, (String)localObject, this, str);
        return;
      }
      e();
      return;
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
  }
  
  static o a()
  {
    o localo = a;
    if (localo == null)
    {
      localo = new com/moat/analytics/mobile/inm/o;
      localo.<init>();
      a = localo;
    }
    return a;
  }
  
  private void a(boolean paramBoolean)
  {
    int i = 3;
    String str1 = "LocationManager";
    String str2 = "stopping location fetch";
    try
    {
      p.a(i, str1, this, str2);
      h();
      i();
      if (paramBoolean)
      {
        k();
        return;
      }
      j();
      return;
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
  }
  
  private static boolean a(Location paramLocation)
  {
    boolean bool1 = false;
    if (paramLocation != null)
    {
      double d1 = paramLocation.getLatitude();
      double d2 = 0.0D;
      boolean bool2 = d1 < d2;
      if (!bool2)
      {
        d1 = paramLocation.getLongitude();
        bool2 = d1 < d2;
        if (!bool2) {}
      }
      else
      {
        float f1 = paramLocation.getAccuracy();
        boolean bool3 = f1 < 0.0F;
        if (!bool3)
        {
          float f2 = b(paramLocation);
          int i = 1142292480;
          f1 = 600.0F;
          boolean bool4 = f2 < f1;
          if (bool4) {
            bool1 = true;
          }
        }
      }
    }
    return bool1;
  }
  
  static boolean a(Location paramLocation1, Location paramLocation2)
  {
    boolean bool1 = true;
    if (paramLocation1 == paramLocation2) {
      return bool1;
    }
    if ((paramLocation1 != null) && (paramLocation2 != null))
    {
      long l1 = paramLocation1.getTime();
      long l2 = paramLocation2.getTime();
      boolean bool2 = l1 < l2;
      if (!bool2) {
        return bool1;
      }
    }
    return false;
  }
  
  private static boolean a(String paramString)
  {
    Context localContext = a.a().getApplicationContext();
    int i = b.a(localContext, paramString);
    return i == 0;
  }
  
  private static float b(Location paramLocation)
  {
    long l1 = System.currentTimeMillis();
    long l2 = paramLocation.getTime();
    return (float)((l1 - l2) / 1000L);
  }
  
  private static Location b(Location paramLocation1, Location paramLocation2)
  {
    boolean bool1 = a(paramLocation1);
    boolean bool2 = a(paramLocation2);
    if (!bool1)
    {
      if (!bool2) {
        return null;
      }
      return paramLocation2;
    }
    if (!bool2) {
      return paramLocation1;
    }
    float f1 = paramLocation1.getAccuracy();
    float f2 = paramLocation1.getAccuracy();
    bool1 = f1 < f2;
    if (bool1) {
      return paramLocation1;
    }
    return paramLocation2;
  }
  
  private void e()
  {
    try
    {
      boolean bool = f;
      if (!bool)
      {
        Object localObject = e;
        if (localObject != null)
        {
          bool = h;
          int i = 3;
          if (bool)
          {
            localObject = "LocationManager";
            str1 = "already updating location";
            p.a(i, (String)localObject, this, str1);
          }
          localObject = "LocationManager";
          String str1 = "starting location fetch";
          p.a(i, (String)localObject, this, str1);
          localObject = b();
          if (localObject != null)
          {
            str1 = "LocationManager";
            StringBuilder localStringBuilder = new java/lang/StringBuilder;
            String str2 = "Have a valid location, won't fetch = ";
            localStringBuilder.<init>(str2);
            localObject = ((Location)localObject).toString();
            localStringBuilder.append((String)localObject);
            localObject = localStringBuilder.toString();
            p.a(i, str1, this, (String)localObject);
            k();
            return;
          }
          g();
        }
      }
      return;
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
  }
  
  private Location f()
  {
    localLocation = null;
    try
    {
      boolean bool1 = l();
      boolean bool2 = m();
      Object localObject1;
      Object localObject2;
      if ((bool1) && (bool2))
      {
        localObject1 = e;
        localObject2 = "gps";
        localObject1 = ((LocationManager)localObject1).getLastKnownLocation((String)localObject2);
        localObject2 = e;
        String str = "network";
        localObject2 = ((LocationManager)localObject2).getLastKnownLocation(str);
        localLocation = b((Location)localObject1, (Location)localObject2);
      }
      else
      {
        if (bool1) {
          localObject1 = e;
        }
        for (localObject2 = "gps";; localObject2 = "network")
        {
          localLocation = ((LocationManager)localObject1).getLastKnownLocation((String)localObject2);
          break;
          if (!bool2) {
            break;
          }
          localObject1 = e;
        }
      }
      return localLocation;
    }
    catch (SecurityException localSecurityException)
    {
      m.a(localSecurityException);
    }
  }
  
  private void g()
  {
    try
    {
      boolean bool1 = h;
      if (!bool1)
      {
        Object localObject1 = "LocationManager";
        Object localObject2 = "Attempting to start update";
        int i = 3;
        p.a(i, (String)localObject1, this, (String)localObject2);
        bool1 = l();
        boolean bool2 = true;
        String str1;
        Object localObject3;
        String str2;
        long l1;
        Looper localLooper;
        if (bool1)
        {
          localObject1 = "LocationManager";
          str1 = "start updating gps location";
          p.a(i, (String)localObject1, this, str1);
          localObject3 = e;
          str2 = "gps";
          l1 = 0L;
          localLooper = Looper.getMainLooper();
          ((LocationManager)localObject3).requestLocationUpdates(str2, l1, 0.0F, this, localLooper);
          h = bool2;
        }
        bool1 = m();
        if (bool1)
        {
          localObject1 = "LocationManager";
          str1 = "start updating network location";
          p.a(i, (String)localObject1, this, str1);
          localObject3 = e;
          str2 = "network";
          l1 = 0L;
          localLooper = Looper.getMainLooper();
          ((LocationManager)localObject3).requestLocationUpdates(str2, l1, 0.0F, this, localLooper);
          h = bool2;
        }
        bool1 = h;
        if (bool1)
        {
          i();
          localObject1 = b;
          localObject2 = new com/moat/analytics/mobile/inm/o$1;
          ((o.1)localObject2).<init>(this);
          long l2 = 60;
          localObject3 = TimeUnit.SECONDS;
          localObject1 = ((ScheduledExecutorService)localObject1).schedule((Runnable)localObject2, l2, (TimeUnit)localObject3);
          d = ((ScheduledFuture)localObject1);
        }
      }
      return;
    }
    catch (SecurityException localSecurityException)
    {
      m.a(localSecurityException;
    }
  }
  
  private void h()
  {
    int i = 3;
    String str1 = "LocationManager";
    String str2 = "Stopping to update location";
    try
    {
      p.a(i, str1, this, str2);
      boolean bool = n();
      if (bool)
      {
        LocationManager localLocationManager = e;
        if (localLocationManager != null)
        {
          localLocationManager = e;
          localLocationManager.removeUpdates(this);
          bool = false;
          localLocationManager = null;
          h = false;
        }
      }
      return;
    }
    catch (SecurityException localSecurityException)
    {
      m.a(localSecurityException;
    }
  }
  
  private void i()
  {
    ScheduledFuture localScheduledFuture = d;
    if (localScheduledFuture != null)
    {
      boolean bool1 = localScheduledFuture.isCancelled();
      if (!bool1)
      {
        localScheduledFuture = d;
        boolean bool2 = true;
        localScheduledFuture.cancel(bool2);
        bool1 = false;
        localScheduledFuture = null;
        d = null;
      }
    }
  }
  
  private void j()
  {
    ScheduledFuture localScheduledFuture = c;
    if (localScheduledFuture != null)
    {
      boolean bool1 = localScheduledFuture.isCancelled();
      if (!bool1)
      {
        localScheduledFuture = c;
        boolean bool2 = true;
        localScheduledFuture.cancel(bool2);
        bool1 = false;
        localScheduledFuture = null;
        c = null;
      }
    }
  }
  
  private void k()
  {
    String str = "Resetting fetch timer";
    int i = 3;
    p.a(i, "LocationManager", this, str);
    j();
    Object localObject = b();
    float f1 = 600.0F;
    if (localObject != null)
    {
      float f2 = b((Location)localObject);
      f1 -= f2;
      f2 = 0.0F;
      localObject = null;
      f1 = Math.max(f1, 0.0F);
    }
    long l = f1;
    ScheduledExecutorService localScheduledExecutorService = b;
    o.2 local2 = new com/moat/analytics/mobile/inm/o$2;
    local2.<init>(this);
    TimeUnit localTimeUnit = TimeUnit.SECONDS;
    localObject = localScheduledExecutorService.schedule(local2, l, localTimeUnit);
    c = ((ScheduledFuture)localObject);
  }
  
  private boolean l()
  {
    Object localObject = "android.permission.ACCESS_FINE_LOCATION";
    boolean bool = a((String)localObject);
    if (bool)
    {
      localObject = e;
      String str = "gps";
      localObject = ((LocationManager)localObject).getProvider(str);
      if (localObject != null)
      {
        localObject = e;
        str = "gps";
        bool = ((LocationManager)localObject).isProviderEnabled(str);
        if (bool) {
          return true;
        }
      }
    }
    return false;
  }
  
  private boolean m()
  {
    boolean bool = n();
    if (bool)
    {
      Object localObject = e;
      String str = "network";
      localObject = ((LocationManager)localObject).getProvider(str);
      if (localObject != null)
      {
        localObject = e;
        str = "network";
        bool = ((LocationManager)localObject).isProviderEnabled(str);
        if (bool) {
          return true;
        }
      }
    }
    return false;
  }
  
  private static boolean n()
  {
    String str = "android.permission.ACCESS_FINE_LOCATION";
    boolean bool = a(str);
    if (!bool)
    {
      str = "android.permission.ACCESS_COARSE_LOCATION";
      bool = a(str);
      if (!bool) {
        return false;
      }
    }
    return true;
  }
  
  Location b()
  {
    boolean bool = f;
    if (!bool)
    {
      Object localObject = e;
      if (localObject != null) {
        try
        {
          localObject = g;
          Location localLocation = f();
          localObject = b((Location)localObject, localLocation);
          g = ((Location)localObject);
          return g;
        }
        catch (Exception localException)
        {
          m.a(localException);
        }
      }
    }
    return null;
  }
  
  void c()
  {
    e();
  }
  
  void d()
  {
    a(false);
  }
  
  public void onLocationChanged(Location paramLocation)
  {
    Object localObject1 = "LocationManager";
    try
    {
      Object localObject2 = new java/lang/StringBuilder;
      String str = "Received an updated location = ";
      ((StringBuilder)localObject2).<init>(str);
      str = paramLocation.toString();
      ((StringBuilder)localObject2).append(str);
      localObject2 = ((StringBuilder)localObject2).toString();
      int i = 3;
      p.a(i, (String)localObject1, this, (String)localObject2);
      float f1 = b(paramLocation);
      boolean bool1 = paramLocation.hasAccuracy();
      if (bool1)
      {
        float f2 = paramLocation.getAccuracy();
        float f3 = 100.0F;
        bool1 = f2 < f3;
        if (!bool1)
        {
          int j = 1142292480;
          f2 = 600.0F;
          boolean bool2 = f1 < f2;
          if (bool2)
          {
            localObject1 = g;
            paramLocation = b((Location)localObject1, paramLocation);
            g = paramLocation;
            paramLocation = "LocationManager";
            localObject1 = "fetchCompleted";
            p.a(i, paramLocation, this, (String)localObject1);
            boolean bool3 = true;
            a(bool3);
          }
        }
      }
      return;
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
  }
  
  public void onProviderDisabled(String paramString) {}
  
  public void onProviderEnabled(String paramString) {}
  
  public void onStatusChanged(String paramString, int paramInt, Bundle paramBundle) {}
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */