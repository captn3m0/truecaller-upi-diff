package com.moat.analytics.mobile.inm;

import android.app.Activity;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import com.moat.analytics.mobile.inm.a.b.a;
import java.lang.ref.WeakReference;

class f
{
  private static WebAdTracker a;
  private static WeakReference b;
  
  static
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(null);
    b = localWeakReference;
  }
  
  private static void a()
  {
    WebAdTracker localWebAdTracker = a;
    if (localWebAdTracker != null)
    {
      String str1 = "GMAInterstitialHelper";
      Object localObject = b.get();
      String str2 = "Stopping to track GMA interstitial";
      p.a(3, str1, localObject, str2);
      a.stopTracking();
      localWebAdTracker = null;
      a = null;
    }
  }
  
  static void a(Activity paramActivity)
  {
    try
    {
      Object localObject1 = w.a();
      localObject1 = a;
      Object localObject2 = w.d.a;
      if (localObject1 == localObject2) {
        return;
      }
      boolean bool1 = b(paramActivity);
      int i;
      if (bool1)
      {
        localObject1 = b;
        localObject1 = ((WeakReference)localObject1).get();
        if (localObject1 != null)
        {
          localObject1 = b;
          localObject1 = ((WeakReference)localObject1).get();
          if (localObject1 == paramActivity) {}
        }
        else
        {
          localObject1 = paramActivity.getWindow();
          localObject1 = ((Window)localObject1).getDecorView();
          boolean bool2 = localObject1 instanceof ViewGroup;
          if (bool2)
          {
            localObject1 = (ViewGroup)localObject1;
            localObject1 = ab.a((ViewGroup)localObject1);
            bool2 = ((a)localObject1).c();
            if (bool2)
            {
              localObject2 = new java/lang/ref/WeakReference;
              ((WeakReference)localObject2).<init>(paramActivity);
              b = (WeakReference)localObject2;
              paramActivity = ((a)localObject1).b();
              paramActivity = (WebView)paramActivity;
              a(paramActivity);
              return;
            }
            i = 3;
            localObject2 = "GMAInterstitialHelper";
            String str = "Sorry, no WebView in this activity";
            p.a(i, (String)localObject2, paramActivity, str);
          }
        }
      }
      else
      {
        a();
        paramActivity = new java/lang/ref/WeakReference;
        i = 0;
        localObject1 = null;
        paramActivity.<init>(null);
        b = paramActivity;
      }
      return;
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
  }
  
  private static void a(WebView paramWebView)
  {
    Object localObject = b.get();
    p.a(3, "GMAInterstitialHelper", localObject, "Starting to track GMA interstitial");
    paramWebView = MoatFactory.create().createWebAdTracker(paramWebView);
    a = paramWebView;
    paramWebView.startTracking();
  }
  
  private static boolean b(Activity paramActivity)
  {
    String str1 = paramActivity.getClass().getName();
    String str2 = "GMAInterstitialHelper";
    String str3 = String.valueOf(str1);
    String str4 = "Activity name: ".concat(str3);
    int i = 3;
    p.a(i, str2, paramActivity, str4);
    paramActivity = "com.google.android.gms.ads.AdActivity";
    boolean bool = str1.contains(paramActivity);
    return bool;
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */