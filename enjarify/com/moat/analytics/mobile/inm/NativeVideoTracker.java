package com.moat.analytics.mobile.inm;

import android.app.Activity;
import android.media.MediaPlayer;
import android.view.View;
import java.util.Map;

public abstract interface NativeVideoTracker
{
  public abstract void changeTargetView(View paramView);
  
  public abstract void dispatchEvent(MoatAdEvent paramMoatAdEvent);
  
  public abstract void setActivity(Activity paramActivity);
  
  public abstract void setPlayerVolume(Double paramDouble);
  
  public abstract void stopTracking();
  
  public abstract boolean trackVideoAd(Map paramMap, MediaPlayer paramMediaPlayer, View paramView);
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.NativeVideoTracker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */