package com.moat.analytics.mobile.inm;

import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.moat.analytics.mobile.inm.a.a.a;
import java.lang.ref.WeakReference;
import java.util.Map;

class n
  extends MoatFactory
{
  n()
  {
    boolean bool = a();
    if (bool) {
      return;
    }
    p.a(3, "Factory", this, "Failed to initialize MoatFactory. Please check that you've initialized the Moat SDK correctly.");
    p.a("[ERROR] ", "Failed to initialize MoatFactory, SDK was not started");
    m localm = new com/moat/analytics/mobile/inm/m;
    localm.<init>();
    throw localm;
  }
  
  private NativeDisplayTracker a(View paramView, Map paramMap)
  {
    a.a(paramView);
    a.a(paramMap);
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramView);
    paramView = new com/moat/analytics/mobile/inm/n$3;
    paramView.<init>(this, localWeakReference, paramMap);
    return (NativeDisplayTracker)x.a(paramView, NativeDisplayTracker.class);
  }
  
  private NativeVideoTracker a(String paramString)
  {
    n.4 local4 = new com/moat/analytics/mobile/inm/n$4;
    local4.<init>(this, paramString);
    return (NativeVideoTracker)x.a(local4, NativeVideoTracker.class);
  }
  
  private WebAdTracker a(ViewGroup paramViewGroup)
  {
    a.a(paramViewGroup);
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramViewGroup);
    paramViewGroup = new com/moat/analytics/mobile/inm/n$2;
    paramViewGroup.<init>(this, localWeakReference);
    return (WebAdTracker)x.a(paramViewGroup, WebAdTracker.class);
  }
  
  private WebAdTracker a(WebView paramWebView)
  {
    a.a(paramWebView);
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramWebView);
    paramWebView = new com/moat/analytics/mobile/inm/n$1;
    paramWebView.<init>(this, localWeakReference);
    return (WebAdTracker)x.a(paramWebView, WebAdTracker.class);
  }
  
  private Object a(MoatPlugin paramMoatPlugin)
  {
    return paramMoatPlugin.a();
  }
  
  private boolean a()
  {
    return ((k)k.getInstance()).a();
  }
  
  public Object createCustomTracker(MoatPlugin paramMoatPlugin)
  {
    try
    {
      return a(paramMoatPlugin);
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
    return paramMoatPlugin.b();
  }
  
  public NativeDisplayTracker createNativeDisplayTracker(View paramView, Map paramMap)
  {
    try
    {
      return a(paramView, paramMap);
    }
    catch (Exception localException)
    {
      m.a(localException);
      paramView = new com/moat/analytics/mobile/inm/v$c;
      paramView.<init>();
    }
    return paramView;
  }
  
  public NativeVideoTracker createNativeVideoTracker(String paramString)
  {
    try
    {
      return a(paramString);
    }
    catch (Exception localException)
    {
      m.a(localException);
      paramString = new com/moat/analytics/mobile/inm/v$d;
      paramString.<init>();
    }
    return paramString;
  }
  
  public WebAdTracker createWebAdTracker(ViewGroup paramViewGroup)
  {
    try
    {
      return a(paramViewGroup);
    }
    catch (Exception localException)
    {
      m.a(localException);
      paramViewGroup = new com/moat/analytics/mobile/inm/v$e;
      paramViewGroup.<init>();
    }
    return paramViewGroup;
  }
  
  public WebAdTracker createWebAdTracker(WebView paramWebView)
  {
    try
    {
      return a(paramWebView);
    }
    catch (Exception localException)
    {
      m.a(localException);
      paramWebView = new com/moat/analytics/mobile/inm/v$e;
      paramWebView.<init>();
    }
    return paramWebView;
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */