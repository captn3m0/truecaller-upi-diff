package com.moat.analytics.mobile.inm;

import android.graphics.Rect;
import android.view.View;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

class t
  extends b
  implements NativeDisplayTracker
{
  private final Map f;
  private final Set g;
  
  t(View paramView, Map paramMap)
  {
    super(paramView, bool, false);
    paramView = new java/util/HashSet;
    paramView.<init>();
    g = paramView;
    Object localObject = "Initializing.";
    int i = 3;
    p.a(i, "NativeDisplayTracker", this, (String)localObject);
    f = paramMap;
    paramView = getInstanced;
    if (paramView != null)
    {
      localObject = b;
      super.a((j)localObject);
      paramView = a;
      super.a(paramView);
    }
    g();
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    str = a();
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append(" created for ");
    str = e();
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append(", with adIds:");
    paramMap = paramMap.toString();
    ((StringBuilder)localObject).append(paramMap);
    paramMap = ((StringBuilder)localObject).toString();
    p.a("[SUCCESS] ", paramMap);
  }
  
  private static String a(Map paramMap)
  {
    LinkedHashMap localLinkedHashMap = new java/util/LinkedHashMap;
    localLinkedHashMap.<init>();
    int i = 0;
    Iterator localIterator = null;
    int j = 0;
    String str1 = null;
    int k;
    Object localObject;
    String str2;
    boolean bool3;
    for (;;)
    {
      k = 8;
      if (j >= k) {
        break;
      }
      localObject = String.valueOf(j);
      str2 = "moatClientLevel".concat((String)localObject);
      bool3 = paramMap.containsKey(str2);
      if (bool3)
      {
        localObject = paramMap.get(str2);
        localLinkedHashMap.put(str2, localObject);
      }
      j += 1;
    }
    while (i < k)
    {
      localObject = String.valueOf(i);
      str1 = "moatClientSlicer".concat((String)localObject);
      bool3 = paramMap.containsKey(str1);
      if (bool3)
      {
        localObject = paramMap.get(str1);
        localLinkedHashMap.put(str1, localObject);
      }
      i += 1;
    }
    localIterator = paramMap.keySet().iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      str1 = (String)localIterator.next();
      boolean bool2 = localLinkedHashMap.containsKey(str1);
      if (!bool2)
      {
        str2 = (String)paramMap.get(str1);
        localLinkedHashMap.put(str1, str2);
      }
    }
    paramMap = new org/json/JSONObject;
    paramMap.<init>(localLinkedHashMap);
    return paramMap.toString();
  }
  
  private void g()
  {
    j localj = a;
    if (localj != null)
    {
      localj = a;
      String str = h();
      localj.a(str);
    }
  }
  
  private String h()
  {
    String str1 = "";
    try
    {
      Object localObject = f;
      localObject = a((Map)localObject);
      int i = 3;
      String str2 = "NativeDisplayTracker";
      String str3 = "Parsed ad ids = ";
      String str4 = String.valueOf(localObject);
      str3 = str3.concat(str4);
      p.a(i, str2, this, str3);
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      str2 = "{\"adIds\":";
      localStringBuilder.<init>(str2);
      localStringBuilder.append((String)localObject);
      localObject = ", \"adKey\":\"";
      localStringBuilder.append((String)localObject);
      localObject = b;
      localStringBuilder.append((String)localObject);
      localObject = "\", \"adSize\":";
      localStringBuilder.append((String)localObject);
      localObject = i();
      localStringBuilder.append((String)localObject);
      localObject = "}";
      localStringBuilder.append((String)localObject);
      str1 = localStringBuilder.toString();
    }
    catch (Exception localException)
    {
      m.a(localException);
    }
    return str1;
  }
  
  private String i()
  {
    try
    {
      Object localObject = super.d();
      localObject = z.a((View)localObject);
      int i = ((Rect)localObject).width();
      int j = ((Rect)localObject).height();
      HashMap localHashMap = new java/util/HashMap;
      localHashMap.<init>();
      String str1 = "width";
      String str2 = Integer.toString(i);
      localHashMap.put(str1, str2);
      str2 = "height";
      localObject = Integer.toString(j);
      localHashMap.put(str2, localObject);
      localObject = new org/json/JSONObject;
      ((JSONObject)localObject).<init>(localHashMap);
      return ((JSONObject)localObject).toString();
    }
    catch (Exception localException)
    {
      m.a(localException;
    }
    return null;
  }
  
  String a()
  {
    return "NativeDisplayTracker";
  }
  
  public void reportUserInteractionEvent(NativeDisplayTracker.MoatUserInteractionType paramMoatUserInteractionType)
  {
    int i = 3;
    String str1 = "NativeDisplayTracker";
    try
    {
      Object localObject1 = new java/lang/StringBuilder;
      String str2 = "reportUserInteractionEvent:";
      ((StringBuilder)localObject1).<init>(str2);
      str2 = paramMoatUserInteractionType.name();
      ((StringBuilder)localObject1).append(str2);
      localObject1 = ((StringBuilder)localObject1).toString();
      p.a(i, str1, this, (String)localObject1);
      Object localObject2 = g;
      boolean bool = ((Set)localObject2).contains(paramMoatUserInteractionType);
      if (!bool)
      {
        localObject2 = g;
        ((Set)localObject2).add(paramMoatUserInteractionType);
        localObject2 = new org/json/JSONObject;
        ((JSONObject)localObject2).<init>();
        str1 = "adKey";
        localObject1 = b;
        ((JSONObject)localObject2).accumulate(str1, localObject1);
        str1 = "event";
        paramMoatUserInteractionType = paramMoatUserInteractionType.name();
        paramMoatUserInteractionType = paramMoatUserInteractionType.toLowerCase();
        ((JSONObject)localObject2).accumulate(str1, paramMoatUserInteractionType);
        paramMoatUserInteractionType = a;
        if (paramMoatUserInteractionType != null)
        {
          paramMoatUserInteractionType = a;
          localObject2 = ((JSONObject)localObject2).toString();
          paramMoatUserInteractionType.b((String)localObject2);
        }
      }
      return;
    }
    catch (Exception localException)
    {
      m.a(localException);
      return;
    }
    catch (JSONException paramMoatUserInteractionType)
    {
      p.b(2, "NativeDisplayTracker", this, "Got JSON exception");
      m.a(paramMoatUserInteractionType);
    }
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */