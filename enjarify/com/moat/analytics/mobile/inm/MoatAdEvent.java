package com.moat.analytics.mobile.inm;

import java.util.HashMap;
import java.util.Map;

public class MoatAdEvent
{
  public static final Double VOLUME_MUTED = Double.valueOf(0.0D);
  public static final Double VOLUME_UNMUTED = Double.valueOf(1.0D);
  static final Integer a = Integer.valueOf(-1 << -1);
  private static final Double e = Double.valueOf(0.0D / 0.0D);
  Integer b;
  Double c;
  MoatAdEventType d;
  private final Long f;
  
  public MoatAdEvent(MoatAdEventType paramMoatAdEventType)
  {
    this(paramMoatAdEventType, localInteger, localDouble);
  }
  
  public MoatAdEvent(MoatAdEventType paramMoatAdEventType, Integer paramInteger)
  {
    this(paramMoatAdEventType, paramInteger, localDouble);
  }
  
  public MoatAdEvent(MoatAdEventType paramMoatAdEventType, Integer paramInteger, Double paramDouble)
  {
    Long localLong = Long.valueOf(System.currentTimeMillis());
    f = localLong;
    d = paramMoatAdEventType;
    c = paramDouble;
    b = paramInteger;
  }
  
  Map a()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    Object localObject = c;
    localHashMap.put("adVolume", localObject);
    localObject = b;
    localHashMap.put("playhead", localObject);
    localObject = f;
    localHashMap.put("aTimeStamp", localObject);
    localObject = d.toString();
    localHashMap.put("type", localObject);
    return localHashMap;
  }
}

/* Location:
 * Qualified Name:     com.moat.analytics.mobile.inm.MoatAdEvent
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */