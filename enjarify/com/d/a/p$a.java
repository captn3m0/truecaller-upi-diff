package com.d.a;

import java.util.ArrayList;
import java.util.List;

public final class p$a
{
  final List a;
  
  public p$a()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>(20);
    a = localArrayList;
  }
  
  private a c(String paramString1, String paramString2)
  {
    a.add(paramString1);
    paramString1 = a;
    paramString2 = paramString2.trim();
    paramString1.add(paramString2);
    return this;
  }
  
  private static void d(String paramString1, String paramString2)
  {
    if (paramString1 != null)
    {
      boolean bool = paramString1.isEmpty();
      if (!bool)
      {
        int i = paramString1.length();
        Integer localInteger1 = null;
        int j = 0;
        Integer localInteger2 = null;
        int k;
        int m;
        int n;
        int i1;
        int i2;
        int i3;
        for (;;)
        {
          k = 127;
          m = 2;
          n = 3;
          i1 = 31;
          i2 = 1;
          if (j >= i) {
            break label137;
          }
          i3 = paramString1.charAt(j);
          if ((i3 <= i1) || (i3 >= k)) {
            break;
          }
          j += 1;
        }
        paramString2 = new java/lang/IllegalArgumentException;
        Object localObject1 = new Object[n];
        Object localObject2 = Integer.valueOf(i3);
        localObject1[0] = localObject2;
        localInteger1 = Integer.valueOf(j);
        localObject1[i2] = localInteger1;
        localObject1[m] = paramString1;
        paramString1 = String.format("Unexpected char %#04x at %d in header name: %s", (Object[])localObject1);
        paramString2.<init>(paramString1);
        throw paramString2;
        label137:
        if (paramString2 != null)
        {
          int i4 = paramString2.length();
          i = 0;
          localObject1 = null;
          while (i < i4)
          {
            j = paramString2.charAt(i);
            if ((j > i1) && (j < k))
            {
              i += 1;
            }
            else
            {
              paramString1 = new java/lang/IllegalArgumentException;
              localObject2 = new Object[n];
              localInteger2 = Integer.valueOf(j);
              localObject2[0] = localInteger2;
              localObject1 = Integer.valueOf(i);
              localObject2[i2] = localObject1;
              localObject2[m] = paramString2;
              paramString2 = String.format("Unexpected char %#04x at %d in header value: %s", (Object[])localObject2);
              paramString1.<init>(paramString2);
              throw paramString1;
            }
          }
          return;
        }
        paramString1 = new java/lang/IllegalArgumentException;
        paramString1.<init>("value == null");
        throw paramString1;
      }
      paramString1 = new java/lang/IllegalArgumentException;
      paramString1.<init>("name is empty");
      throw paramString1;
    }
    paramString1 = new java/lang/IllegalArgumentException;
    paramString1.<init>("name == null");
    throw paramString1;
  }
  
  final a a(String paramString)
  {
    String str1 = ":";
    int i = 1;
    int j = paramString.indexOf(str1, i);
    int k = -1;
    if (j != k)
    {
      String str2 = paramString.substring(0, j);
      j += i;
      paramString = paramString.substring(j);
      return c(str2, paramString);
    }
    str1 = ":";
    boolean bool = paramString.startsWith(str1);
    if (bool)
    {
      paramString = paramString.substring(i);
      return c("", paramString);
    }
    return c("", paramString);
  }
  
  public final a a(String paramString1, String paramString2)
  {
    d(paramString1, paramString2);
    return c(paramString1, paramString2);
  }
  
  public final p a()
  {
    p localp = new com/d/a/p;
    localp.<init>(this, (byte)0);
    return localp;
  }
  
  public final a b(String paramString)
  {
    int i = 0;
    for (;;)
    {
      Object localObject = a;
      int j = ((List)localObject).size();
      if (i >= j) {
        break;
      }
      localObject = (String)a.get(i);
      boolean bool = paramString.equalsIgnoreCase((String)localObject);
      if (bool)
      {
        a.remove(i);
        localObject = a;
        ((List)localObject).remove(i);
        i += -2;
      }
      i += 2;
    }
    return this;
  }
  
  public final a b(String paramString1, String paramString2)
  {
    d(paramString1, paramString2);
    b(paramString1);
    c(paramString1, paramString2);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.d.a.p.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */