package com.d.a;

import java.io.IOException;

public enum u
{
  private final String e;
  
  static
  {
    Object localObject = new com/d/a/u;
    ((u)localObject).<init>("HTTP_1_0", 0, "http/1.0");
    a = (u)localObject;
    localObject = new com/d/a/u;
    int i = 1;
    ((u)localObject).<init>("HTTP_1_1", i, "http/1.1");
    b = (u)localObject;
    localObject = new com/d/a/u;
    int j = 2;
    ((u)localObject).<init>("SPDY_3", j, "spdy/3.1");
    c = (u)localObject;
    localObject = new com/d/a/u;
    int k = 3;
    ((u)localObject).<init>("HTTP_2", k, "h2");
    d = (u)localObject;
    localObject = new u[4];
    u localu = a;
    localObject[0] = localu;
    localu = b;
    localObject[i] = localu;
    localu = c;
    localObject[j] = localu;
    localu = d;
    localObject[k] = localu;
    f = (u[])localObject;
  }
  
  private u(String paramString1)
  {
    e = paramString1;
  }
  
  public static u a(String paramString)
  {
    Object localObject = ae;
    boolean bool = paramString.equals(localObject);
    if (bool) {
      return a;
    }
    localObject = be;
    bool = paramString.equals(localObject);
    if (bool) {
      return b;
    }
    localObject = de;
    bool = paramString.equals(localObject);
    if (bool) {
      return d;
    }
    localObject = ce;
    bool = paramString.equals(localObject);
    if (bool) {
      return c;
    }
    localObject = new java/io/IOException;
    paramString = String.valueOf(paramString);
    paramString = "Unexpected protocol: ".concat(paramString);
    ((IOException)localObject).<init>(paramString);
    throw ((Throwable)localObject);
  }
  
  public final String toString()
  {
    return e;
  }
}

/* Location:
 * Qualified Name:     com.d.a.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */