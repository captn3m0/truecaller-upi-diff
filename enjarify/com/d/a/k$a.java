package com.d.a;

public final class k$a
{
  boolean a;
  String[] b;
  String[] c;
  boolean d;
  
  public k$a(k paramk)
  {
    boolean bool1 = k.a(paramk);
    a = bool1;
    String[] arrayOfString = k.b(paramk);
    b = arrayOfString;
    arrayOfString = k.c(paramk);
    c = arrayOfString;
    boolean bool2 = k.d(paramk);
    d = bool2;
  }
  
  k$a(boolean paramBoolean)
  {
    a = paramBoolean;
  }
  
  public final a a()
  {
    boolean bool = a;
    if (bool)
    {
      d = true;
      return this;
    }
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>("no TLS extensions for cleartext connections");
    throw localIllegalStateException;
  }
  
  public final a a(aa... paramVarArgs)
  {
    boolean bool = a;
    if (bool)
    {
      int i = paramVarArgs.length;
      String[] arrayOfString = new String[i];
      int j = 0;
      for (;;)
      {
        int k = paramVarArgs.length;
        if (j >= k) {
          break;
        }
        String str = e;
        arrayOfString[j] = str;
        j += 1;
      }
      return b(arrayOfString);
    }
    paramVarArgs = new java/lang/IllegalStateException;
    paramVarArgs.<init>("no TLS versions for cleartext connections");
    throw paramVarArgs;
  }
  
  public final a a(h... paramVarArgs)
  {
    boolean bool = a;
    if (bool)
    {
      int i = paramVarArgs.length;
      String[] arrayOfString = new String[i];
      int j = 0;
      for (;;)
      {
        int k = paramVarArgs.length;
        if (j >= k) {
          break;
        }
        String str = aS;
        arrayOfString[j] = str;
        j += 1;
      }
      return a(arrayOfString);
    }
    paramVarArgs = new java/lang/IllegalStateException;
    paramVarArgs.<init>("no cipher suites for cleartext connections");
    throw paramVarArgs;
  }
  
  public final a a(String... paramVarArgs)
  {
    boolean bool = a;
    if (bool)
    {
      int i = paramVarArgs.length;
      if (i != 0)
      {
        paramVarArgs = (String[])paramVarArgs.clone();
        b = paramVarArgs;
        return this;
      }
      paramVarArgs = new java/lang/IllegalArgumentException;
      paramVarArgs.<init>("At least one cipher suite is required");
      throw paramVarArgs;
    }
    paramVarArgs = new java/lang/IllegalStateException;
    paramVarArgs.<init>("no cipher suites for cleartext connections");
    throw paramVarArgs;
  }
  
  public final a b(String... paramVarArgs)
  {
    boolean bool = a;
    if (bool)
    {
      int i = paramVarArgs.length;
      if (i != 0)
      {
        paramVarArgs = (String[])paramVarArgs.clone();
        c = paramVarArgs;
        return this;
      }
      paramVarArgs = new java/lang/IllegalArgumentException;
      paramVarArgs.<init>("At least one TLS version is required");
      throw paramVarArgs;
    }
    paramVarArgs = new java/lang/IllegalStateException;
    paramVarArgs.<init>("no TLS versions for cleartext connections");
    throw paramVarArgs;
  }
  
  public final k b()
  {
    k localk = new com/d/a/k;
    localk.<init>(this, (byte)0);
    return localk;
  }
}

/* Location:
 * Qualified Name:     com.d.a.k.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */