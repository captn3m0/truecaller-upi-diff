package com.d.a;

import com.d.a.a.j;
import java.util.Collections;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;

public final class o
{
  public final String a;
  public final List b;
  final List c;
  
  o(String paramString, List paramList1, List paramList2)
  {
    a = paramString;
    b = paramList1;
    c = paramList2;
  }
  
  public static o a(SSLSession paramSSLSession)
  {
    String str = paramSSLSession.getCipherSuite();
    if (str != null)
    {
      Object localObject;
      try
      {
        localObject = paramSSLSession.getPeerCertificates();
      }
      catch (SSLPeerUnverifiedException localSSLPeerUnverifiedException)
      {
        localObject = null;
      }
      if (localObject != null) {
        localObject = j.a((Object[])localObject);
      } else {
        localObject = Collections.emptyList();
      }
      paramSSLSession = paramSSLSession.getLocalCertificates();
      if (paramSSLSession != null) {
        paramSSLSession = j.a(paramSSLSession);
      } else {
        paramSSLSession = Collections.emptyList();
      }
      o localo = new com/d/a/o;
      localo.<init>(str, (List)localObject, paramSSLSession);
      return localo;
    }
    paramSSLSession = new java/lang/IllegalStateException;
    paramSSLSession.<init>("cipherSuite == null");
    throw paramSSLSession;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof o;
    if (!bool1) {
      return false;
    }
    paramObject = (o)paramObject;
    Object localObject1 = a;
    Object localObject2 = a;
    bool1 = ((String)localObject1).equals(localObject2);
    if (bool1)
    {
      localObject1 = b;
      localObject2 = b;
      bool1 = ((List)localObject1).equals(localObject2);
      if (bool1)
      {
        localObject1 = c;
        paramObject = c;
        boolean bool2 = ((List)localObject1).equals(paramObject);
        if (bool2) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    int i = (a.hashCode() + 527) * 31;
    int j = b.hashCode();
    i = (i + j) * 31;
    j = c.hashCode();
    return i + j;
  }
}

/* Location:
 * Qualified Name:     com.d.a.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */