package com.d.a;

public final class x$a
{
  public v a;
  public u b;
  public int c = -1;
  public String d;
  public o e;
  p.a f;
  public y g;
  x h;
  x i;
  x j;
  
  public x$a()
  {
    p.a locala = new com/d/a/p$a;
    locala.<init>();
    f = locala;
  }
  
  private x$a(x paramx)
  {
    Object localObject = a;
    a = ((v)localObject);
    localObject = b;
    b = ((u)localObject);
    int k = c;
    c = k;
    localObject = d;
    d = ((String)localObject);
    localObject = e;
    e = ((o)localObject);
    localObject = f.a();
    f = ((p.a)localObject);
    localObject = g;
    g = ((y)localObject);
    localObject = h;
    h = ((x)localObject);
    localObject = i;
    i = ((x)localObject);
    paramx = j;
    j = paramx;
  }
  
  private static void a(String paramString, x paramx)
  {
    Object localObject = g;
    if (localObject == null)
    {
      localObject = h;
      if (localObject == null)
      {
        localObject = i;
        if (localObject == null)
        {
          paramx = j;
          if (paramx == null) {
            return;
          }
          paramx = new java/lang/IllegalArgumentException;
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>();
          ((StringBuilder)localObject).append(paramString);
          ((StringBuilder)localObject).append(".priorResponse != null");
          paramString = ((StringBuilder)localObject).toString();
          paramx.<init>(paramString);
          throw paramx;
        }
        paramx = new java/lang/IllegalArgumentException;
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append(paramString);
        ((StringBuilder)localObject).append(".cacheResponse != null");
        paramString = ((StringBuilder)localObject).toString();
        paramx.<init>(paramString);
        throw paramx;
      }
      paramx = new java/lang/IllegalArgumentException;
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      ((StringBuilder)localObject).append(paramString);
      ((StringBuilder)localObject).append(".networkResponse != null");
      paramString = ((StringBuilder)localObject).toString();
      paramx.<init>(paramString);
      throw paramx;
    }
    paramx = new java/lang/IllegalArgumentException;
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    ((StringBuilder)localObject).append(paramString);
    ((StringBuilder)localObject).append(".body != null");
    paramString = ((StringBuilder)localObject).toString();
    paramx.<init>(paramString);
    throw paramx;
  }
  
  private static void d(x paramx)
  {
    paramx = g;
    if (paramx == null) {
      return;
    }
    paramx = new java/lang/IllegalArgumentException;
    paramx.<init>("priorResponse.body != null");
    throw paramx;
  }
  
  public final a a(p paramp)
  {
    paramp = paramp.a();
    f = paramp;
    return this;
  }
  
  public final a a(x paramx)
  {
    if (paramx != null)
    {
      String str = "networkResponse";
      a(str, paramx);
    }
    h = paramx;
    return this;
  }
  
  public final a a(String paramString1, String paramString2)
  {
    f.b(paramString1, paramString2);
    return this;
  }
  
  public final x a()
  {
    Object localObject1 = a;
    if (localObject1 != null)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        int k = c;
        if (k >= 0)
        {
          localObject1 = new com/d/a/x;
          ((x)localObject1).<init>(this, (byte)0);
          return (x)localObject1;
        }
        localObject1 = new java/lang/IllegalStateException;
        Object localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("code < 0: ");
        int m = c;
        ((StringBuilder)localObject2).append(m);
        localObject2 = ((StringBuilder)localObject2).toString();
        ((IllegalStateException)localObject1).<init>((String)localObject2);
        throw ((Throwable)localObject1);
      }
      localObject1 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject1).<init>("protocol == null");
      throw ((Throwable)localObject1);
    }
    localObject1 = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject1).<init>("request == null");
    throw ((Throwable)localObject1);
  }
  
  public final a b(x paramx)
  {
    if (paramx != null)
    {
      String str = "cacheResponse";
      a(str, paramx);
    }
    i = paramx;
    return this;
  }
  
  public final a b(String paramString1, String paramString2)
  {
    f.a(paramString1, paramString2);
    return this;
  }
  
  public final a c(x paramx)
  {
    if (paramx != null) {
      d(paramx);
    }
    j = paramx;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.d.a.x.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */