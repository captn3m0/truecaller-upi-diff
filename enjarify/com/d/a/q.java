package com.d.a;

import d.c;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class q
{
  private static final char[] e;
  public final String a;
  public final String b;
  public final int c;
  final List d;
  private final String f;
  private final String g;
  private final List h;
  private final String i;
  private final String j;
  
  static
  {
    char[] arrayOfChar = new char[16];
    arrayOfChar[0] = 48;
    arrayOfChar[1] = 49;
    arrayOfChar[2] = 50;
    arrayOfChar[3] = 51;
    arrayOfChar[4] = 52;
    arrayOfChar[5] = 53;
    arrayOfChar[6] = 54;
    arrayOfChar[7] = 55;
    arrayOfChar[8] = 56;
    arrayOfChar[9] = 57;
    arrayOfChar[10] = 65;
    arrayOfChar[11] = 66;
    arrayOfChar[12] = 67;
    arrayOfChar[13] = 68;
    arrayOfChar[14] = 69;
    arrayOfChar[15] = 70;
    e = arrayOfChar;
  }
  
  private q(q.a parama)
  {
    Object localObject = a;
    a = ((String)localObject);
    localObject = a(b, false);
    f = ((String)localObject);
    localObject = a(c, false);
    g = ((String)localObject);
    localObject = d;
    b = ((String)localObject);
    int k = parama.a();
    c = k;
    localObject = a(f, false);
    h = ((List)localObject);
    localObject = g;
    String str = null;
    if (localObject != null)
    {
      localObject = g;
      boolean bool = true;
      localObject = a((List)localObject, bool);
    }
    else
    {
      k = 0;
      localObject = null;
    }
    d = ((List)localObject);
    localObject = h;
    if (localObject != null)
    {
      localObject = h;
      str = a((String)localObject, false);
    }
    i = str;
    parama = parama.toString();
    j = parama;
  }
  
  static int a(char paramChar)
  {
    char c1 = '0';
    char c2;
    if (paramChar >= c1)
    {
      c2 = '9';
      if (paramChar <= c2) {
        return paramChar - c1;
      }
    }
    c1 = 'a';
    if (paramChar >= c1)
    {
      c2 = 'f';
      if (paramChar <= c2) {
        return paramChar - c1 + 10;
      }
    }
    c1 = 'A';
    if (paramChar >= c1)
    {
      c2 = 'F';
      if (paramChar <= c2) {
        return paramChar - c1 + 10;
      }
    }
    return -1;
  }
  
  public static int a(String paramString)
  {
    String str = "http";
    boolean bool1 = paramString.equals(str);
    if (bool1) {
      return 80;
    }
    str = "https";
    boolean bool2 = paramString.equals(str);
    if (bool2) {
      return 443;
    }
    return -1;
  }
  
  static String a(String paramString1, int paramInt1, int paramInt2, String paramString2, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    String str1 = paramString1;
    int k = paramInt2;
    String str2 = paramString2;
    int m = paramInt1;
    while (m < k)
    {
      int n = str1.codePointAt(m);
      int i1 = 43;
      int i2 = -1;
      int i3 = 128;
      int i4 = 127;
      int i5 = 32;
      int i6 = 37;
      if ((n >= i5) && (n != i4) && ((n < i3) || (!paramBoolean3)))
      {
        i7 = str2.indexOf(n);
        if ((i7 == i2) && ((n != i6) || (paramBoolean1)) && ((n != i1) || (!paramBoolean2)))
        {
          n = Character.charCount(n);
          m += n;
          continue;
        }
      }
      c localc1 = new d/c;
      localc1.<init>();
      i7 = paramInt1;
      localc1.a(str1, paramInt1, m);
      i7 = 0;
      c localc2 = null;
      while (m < k)
      {
        int i8 = str1.codePointAt(m);
        int i9;
        if (paramBoolean1)
        {
          i9 = 9;
          if (i8 != i9)
          {
            i9 = 10;
            if (i8 != i9)
            {
              i9 = 12;
              if (i8 != i9)
              {
                i9 = 13;
                if (i8 == i9) {}
              }
            }
          }
        }
        else if ((i8 == i1) && (paramBoolean2))
        {
          String str3;
          if (paramBoolean1) {
            str3 = "+";
          } else {
            str3 = "%2B";
          }
          localc1.a(str3);
        }
        else
        {
          if ((i8 >= i5) && (i8 != i4) && ((i8 < i3) || (!paramBoolean3)))
          {
            i9 = str2.indexOf(i8);
            if ((i9 == i2) && ((i8 != i6) || (paramBoolean1)))
            {
              localc1.a(i8);
              break label453;
            }
          }
          if (localc2 == null)
          {
            localc2 = new d/c;
            localc2.<init>();
          }
          localc2.a(i8);
          for (;;)
          {
            boolean bool = localc2.e();
            if (bool) {
              break;
            }
            int i10 = localc2.h() & 0xFF;
            localc1.b(i6);
            char[] arrayOfChar = e;
            int i11 = i10 >> 4 & 0xF;
            int i12 = arrayOfChar[i11];
            localc1.b(i12);
            arrayOfChar = e;
            i10 &= 0xF;
            i10 = arrayOfChar[i10];
            localc1.b(i10);
          }
        }
        label453:
        i8 = Character.charCount(i8);
        m += i8;
      }
      return localc1.p();
    }
    int i7 = paramInt1;
    return paramString1.substring(paramInt1, paramInt2);
  }
  
  static String a(String paramString, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    int k = paramInt1;
    while (k < paramInt2)
    {
      int m = paramString.charAt(k);
      int n = 37;
      if (m != n)
      {
        n = 43;
        if ((m != n) || (!paramBoolean))
        {
          k += 1;
          continue;
        }
      }
      c localc = new d/c;
      localc.<init>();
      localc.a(paramString, paramInt1, k);
      a(localc, paramString, k, paramInt2, paramBoolean);
      return localc.p();
    }
    return paramString.substring(paramInt1, paramInt2);
  }
  
  static String a(String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2)
  {
    int k = paramString1.length();
    return a(paramString1, 0, k, paramString2, true, paramBoolean1, paramBoolean2);
  }
  
  private static String a(String paramString, boolean paramBoolean)
  {
    int k = paramString.length();
    return a(paramString, 0, k, paramBoolean);
  }
  
  private static List a(List paramList, boolean paramBoolean)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    int k = paramList.size();
    localArrayList.<init>(k);
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      String str = (String)paramList.next();
      if (str != null)
      {
        str = a(str, paramBoolean);
      }
      else
      {
        bool = false;
        str = null;
      }
      localArrayList.add(str);
    }
    return Collections.unmodifiableList(localArrayList);
  }
  
  private static void a(c paramc, String paramString, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    while (paramInt1 < paramInt2)
    {
      int k = paramString.codePointAt(paramInt1);
      int m = 37;
      if (k == m)
      {
        m = paramInt1 + 2;
        if (m < paramInt2)
        {
          int n = paramInt1 + 1;
          n = a(paramString.charAt(n));
          int i1 = a(paramString.charAt(m));
          int i2 = -1;
          if ((n == i2) || (i1 == i2)) {
            break label128;
          }
          paramInt1 = (n << 4) + i1;
          paramc.b(paramInt1);
          paramInt1 = m;
          break label135;
        }
      }
      m = 43;
      if ((k == m) && (paramBoolean))
      {
        m = 32;
        paramc.b(m);
      }
      else
      {
        label128:
        paramc.a(k);
      }
      label135:
      k = Character.charCount(k);
      paramInt1 += k;
    }
  }
  
  static void a(StringBuilder paramStringBuilder, List paramList)
  {
    int k = paramList.size();
    int m = 0;
    while (m < k)
    {
      char c1 = '/';
      paramStringBuilder.append(c1);
      String str = (String)paramList.get(m);
      paramStringBuilder.append(str);
      m += 1;
    }
  }
  
  private static int b(String paramString1, int paramInt1, int paramInt2, String paramString2)
  {
    while (paramInt1 < paramInt2)
    {
      int k = paramString1.charAt(paramInt1);
      k = paramString2.indexOf(k);
      int m = -1;
      if (k != m) {
        return paramInt1;
      }
      paramInt1 += 1;
    }
    return paramInt2;
  }
  
  static List b(String paramString)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    int k = 0;
    String str = null;
    for (;;)
    {
      int m = paramString.length();
      if (k > m) {
        break;
      }
      m = paramString.indexOf('&', k);
      int n = -1;
      if (m == n) {
        m = paramString.length();
      }
      int i1 = paramString.indexOf('=', k);
      if ((i1 != n) && (i1 <= m))
      {
        str = paramString.substring(k, i1);
        localArrayList.add(str);
        i1 += 1;
        str = paramString.substring(i1, m);
        localArrayList.add(str);
      }
      else
      {
        str = paramString.substring(k, m);
        localArrayList.add(str);
        k = 0;
        str = null;
        localArrayList.add(null);
      }
      k = m + 1;
    }
    return localArrayList;
  }
  
  static void b(StringBuilder paramStringBuilder, List paramList)
  {
    int k = paramList.size();
    int m = 0;
    while (m < k)
    {
      String str1 = (String)paramList.get(m);
      int n = m + 1;
      String str2 = (String)paramList.get(n);
      if (m > 0)
      {
        char c1 = '&';
        paramStringBuilder.append(c1);
      }
      paramStringBuilder.append(str1);
      if (str2 != null)
      {
        char c2 = '=';
        paramStringBuilder.append(c2);
        paramStringBuilder.append(str2);
      }
      m += 2;
    }
  }
  
  public static q c(String paramString)
  {
    q.a locala = new com/d/a/q$a;
    locala.<init>();
    paramString = locala.a(null, paramString);
    q.a.a locala1 = q.a.a.a;
    if (paramString == locala1) {
      return locala.b();
    }
    return null;
  }
  
  public final URL a()
  {
    try
    {
      URL localURL = new java/net/URL;
      localObject = j;
      localURL.<init>((String)localObject);
      return localURL;
    }
    catch (MalformedURLException localMalformedURLException)
    {
      Object localObject = new java/lang/RuntimeException;
      ((RuntimeException)localObject).<init>(localMalformedURLException);
      throw ((Throwable)localObject);
    }
  }
  
  public final URI b()
  {
    try
    {
      localObject1 = new com/d/a/q$a;
      ((q.a)localObject1).<init>();
      localObject2 = a;
      a = ((String)localObject2);
      localObject2 = d();
      b = ((String)localObject2);
      localObject2 = e();
      c = ((String)localObject2);
      localObject2 = b;
      d = ((String)localObject2);
      int k = c;
      localObject3 = a;
      int m = a((String)localObject3);
      if (k != m) {
        k = c;
      } else {
        k = -1;
      }
      e = k;
      localObject2 = f;
      ((List)localObject2).clear();
      localObject2 = f;
      localObject3 = g();
      ((List)localObject2).addAll((Collection)localObject3);
      localObject2 = h();
      ((q.a)localObject1).c((String)localObject2);
      localObject2 = i;
      m = 1;
      if (localObject2 == null)
      {
        k = 0;
        localObject2 = null;
      }
      else
      {
        localObject2 = j;
        n = 35;
        k = ((String)localObject2).indexOf(n) + m;
        str1 = j;
        localObject2 = str1.substring(k);
      }
      h = ((String)localObject2);
      localObject2 = f;
      k = ((List)localObject2).size();
      int n = 0;
      String str1 = null;
      int i1 = 0;
      Object localObject4;
      List localList;
      String str2;
      while (i1 < k)
      {
        localObject4 = f;
        localObject4 = ((List)localObject4).get(i1);
        localObject4 = (String)localObject4;
        localList = f;
        str2 = "[]";
        localObject4 = a((String)localObject4, str2, false, m);
        localList.set(i1, localObject4);
        i1 += 1;
      }
      localObject2 = g;
      if (localObject2 != null)
      {
        localObject2 = g;
        k = ((List)localObject2).size();
        i1 = 0;
        while (i1 < k)
        {
          localObject4 = g;
          localObject4 = ((List)localObject4).get(i1);
          localObject4 = (String)localObject4;
          if (localObject4 != null)
          {
            localList = g;
            str2 = "\\^`{|}";
            localObject4 = a((String)localObject4, str2, m, m);
            localList.set(i1, localObject4);
          }
          i1 += 1;
        }
      }
      localObject2 = h;
      if (localObject2 != null)
      {
        localObject2 = h;
        localObject3 = " \"#<>\\^`{|}";
        localObject2 = a((String)localObject2, (String)localObject3, false, false);
        h = ((String)localObject2);
      }
      localObject1 = ((q.a)localObject1).toString();
      localObject2 = new java/net/URI;
      ((URI)localObject2).<init>((String)localObject1);
      return (URI)localObject2;
    }
    catch (URISyntaxException localURISyntaxException)
    {
      Object localObject1 = new java/lang/IllegalStateException;
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("not valid as a java.net.URI: ");
      Object localObject3 = j;
      ((StringBuilder)localObject2).append((String)localObject3);
      localObject2 = ((StringBuilder)localObject2).toString();
      ((IllegalStateException)localObject1).<init>((String)localObject2);
      throw ((Throwable)localObject1);
    }
  }
  
  public final boolean c()
  {
    return a.equals("https");
  }
  
  public final String d()
  {
    String str1 = f;
    boolean bool = str1.isEmpty();
    if (bool) {
      return "";
    }
    int k = a.length() + 3;
    String str2 = j;
    int m = str2.length();
    int n = b(str2, k, m, ":@");
    return j.substring(k, n);
  }
  
  public final String e()
  {
    String str = g;
    boolean bool = str.isEmpty();
    if (bool) {
      return "";
    }
    str = j;
    int m = a.length() + 3;
    int k = str.indexOf(':', m) + 1;
    int n = j.indexOf('@');
    return j.substring(k, n);
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof q;
    if (bool1)
    {
      paramObject = j;
      String str = j;
      boolean bool2 = ((String)paramObject).equals(str);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final String f()
  {
    String str1 = j;
    int k = a.length() + 3;
    int m = str1.indexOf('/', k);
    String str2 = j;
    int n = str2.length();
    k = b(str2, m, n, "?#");
    return j.substring(m, k);
  }
  
  public final List g()
  {
    String str1 = j;
    int k = a.length() + 3;
    int m = str1.indexOf('/', k);
    String str2 = j;
    int n = str2.length();
    String str3 = "?#";
    k = b(str2, m, n, str3);
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    while (m < k)
    {
      m += 1;
      str3 = j;
      int i1 = b(str3, m, k, "/");
      String str4 = j;
      str1 = str4.substring(m, i1);
      localArrayList.add(str1);
      m = i1;
    }
    return localArrayList;
  }
  
  public final String h()
  {
    List localList = d;
    if (localList == null) {
      return null;
    }
    int k = j.indexOf('?') + 1;
    String str = j;
    int m = k + 1;
    int n = str.length();
    int i1 = b(str, m, n, "#");
    return j.substring(k, i1);
  }
  
  public final int hashCode()
  {
    return j.hashCode();
  }
  
  public final String toString()
  {
    return j;
  }
}

/* Location:
 * Qualified Name:     com.d.a.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */