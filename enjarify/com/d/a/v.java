package com.d.a;

import java.io.IOException;
import java.net.URI;

public final class v
{
  public final q a;
  public final String b;
  public final p c;
  public final w d;
  final Object e;
  private volatile URI f;
  private volatile d g;
  
  private v(v.a parama)
  {
    Object localObject = a;
    a = ((q)localObject);
    localObject = b;
    b = ((String)localObject);
    localObject = c.a();
    c = ((p)localObject);
    localObject = d;
    d = ((w)localObject);
    localObject = e;
    if (localObject != null) {
      parama = e;
    } else {
      parama = this;
    }
    e = parama;
  }
  
  public final String a(String paramString)
  {
    return c.a(paramString);
  }
  
  public final URI a()
  {
    try
    {
      Object localObject = f;
      if (localObject != null) {
        return (URI)localObject;
      }
      localObject = a;
      localObject = ((q)localObject).b();
      f = ((URI)localObject);
      return (URI)localObject;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      IOException localIOException = new java/io/IOException;
      String str = localIllegalStateException.getMessage();
      localIOException.<init>(str);
      throw localIOException;
    }
  }
  
  public final v.a b()
  {
    v.a locala = new com/d/a/v$a;
    locala.<init>(this, (byte)0);
    return locala;
  }
  
  public final d c()
  {
    d locald = g;
    if (locald != null) {
      return locald;
    }
    locald = d.a(c);
    g = locald;
    return locald;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Request{method=");
    Object localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", url=");
    localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", tag=");
    localObject = e;
    if (localObject == this) {
      localObject = null;
    }
    localStringBuilder.append(localObject);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.d.a.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */