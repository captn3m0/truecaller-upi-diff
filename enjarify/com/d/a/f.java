package com.d.a;

import com.d.a.a.j;
import java.security.Principal;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.net.ssl.SSLPeerUnverifiedException;

public final class f
{
  public static final f a;
  private final Map b;
  
  static
  {
    f.a locala = new com/d/a/f$a;
    locala.<init>();
    f localf = new com/d/a/f;
    localf.<init>(locala, (byte)0);
    a = localf;
  }
  
  private f(f.a parama)
  {
    parama = j.a(a);
    b = parama;
  }
  
  private static d.f a(X509Certificate paramX509Certificate)
  {
    return j.a(d.f.a(paramX509Certificate.getPublicKey().getEncoded()));
  }
  
  public static String a(Certificate paramCertificate)
  {
    boolean bool = paramCertificate instanceof X509Certificate;
    if (bool)
    {
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>("sha1/");
      paramCertificate = a((X509Certificate)paramCertificate).b();
      localStringBuilder.append(paramCertificate);
      return localStringBuilder.toString();
    }
    paramCertificate = new java/lang/IllegalArgumentException;
    paramCertificate.<init>("Certificate pinning requires X509 certificates");
    throw paramCertificate;
  }
  
  public final void a(String paramString, List paramList)
  {
    Object localObject1 = (Set)b.get(paramString);
    int i = 46;
    int j = paramString.indexOf(i);
    i = paramString.lastIndexOf(i);
    int k = 0;
    String str1 = null;
    Object localObject3;
    String str2;
    if (j != i)
    {
      localObject2 = b;
      localObject3 = new java/lang/StringBuilder;
      str2 = "*.";
      ((StringBuilder)localObject3).<init>(str2);
      j += 1;
      localObject4 = paramString.substring(j);
      ((StringBuilder)localObject3).append((String)localObject4);
      localObject4 = ((StringBuilder)localObject3).toString();
      localObject2 = (Set)((Map)localObject2).get(localObject4);
    }
    else
    {
      i = 0;
      localObject2 = null;
    }
    if ((localObject1 == null) && (localObject2 == null))
    {
      localObject1 = null;
    }
    else if ((localObject1 != null) && (localObject2 != null))
    {
      localObject4 = new java/util/LinkedHashSet;
      ((LinkedHashSet)localObject4).<init>();
      ((Set)localObject4).addAll((Collection)localObject1);
      ((Set)localObject4).addAll((Collection)localObject2);
      localObject1 = localObject4;
    }
    else if (localObject1 == null)
    {
      localObject1 = localObject2;
    }
    if (localObject1 == null) {
      return;
    }
    i = paramList.size();
    j = 0;
    Object localObject4 = null;
    k = 0;
    str1 = null;
    while (k < i)
    {
      localObject3 = a((X509Certificate)paramList.get(k));
      boolean bool1 = ((Set)localObject1).contains(localObject3);
      if (bool1) {
        return;
      }
      k += 1;
    }
    Object localObject2 = new java/lang/StringBuilder;
    str1 = "Certificate pinning failure!\n  Peer certificate chain:";
    ((StringBuilder)localObject2).<init>(str1);
    k = paramList.size();
    while (j < k)
    {
      localObject3 = (X509Certificate)paramList.get(j);
      ((StringBuilder)localObject2).append("\n    ");
      str2 = a((Certificate)localObject3);
      ((StringBuilder)localObject2).append(str2);
      str2 = ": ";
      ((StringBuilder)localObject2).append(str2);
      localObject3 = ((X509Certificate)localObject3).getSubjectDN().getName();
      ((StringBuilder)localObject2).append((String)localObject3);
      j += 1;
    }
    paramList = "\n  Pinned certificates for ";
    ((StringBuilder)localObject2).append(paramList);
    ((StringBuilder)localObject2).append(paramString);
    ((StringBuilder)localObject2).append(":");
    paramString = ((Set)localObject1).iterator();
    for (;;)
    {
      boolean bool2 = paramString.hasNext();
      if (!bool2) {
        break;
      }
      paramList = (d.f)paramString.next();
      localObject1 = "\n    sha1/";
      ((StringBuilder)localObject2).append((String)localObject1);
      paramList = paramList.b();
      ((StringBuilder)localObject2).append(paramList);
    }
    paramString = new javax/net/ssl/SSLPeerUnverifiedException;
    paramList = ((StringBuilder)localObject2).toString();
    paramString.<init>(paramList);
    throw paramString;
  }
}

/* Location:
 * Qualified Name:     com.d.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */