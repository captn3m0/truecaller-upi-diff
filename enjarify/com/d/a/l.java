package com.d.a;

import d.f;
import java.io.UnsupportedEncodingException;

public final class l
{
  public static String a(String paramString1, String paramString2)
  {
    try
    {
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append(paramString1);
      paramString1 = ":";
      localStringBuilder.append(paramString1);
      localStringBuilder.append(paramString2);
      paramString1 = localStringBuilder.toString();
      paramString2 = "ISO-8859-1";
      paramString1 = paramString1.getBytes(paramString2);
      paramString1 = f.a(paramString1);
      paramString1 = paramString1.b();
      paramString2 = "Basic ";
      paramString1 = String.valueOf(paramString1);
      return paramString2.concat(paramString1);
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      paramString1 = new java/lang/AssertionError;
      paramString1.<init>();
      throw paramString1;
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */