package com.d.a;

public enum aa
{
  public final String e;
  
  static
  {
    Object localObject = new com/d/a/aa;
    ((aa)localObject).<init>("TLS_1_2", 0, "TLSv1.2");
    a = (aa)localObject;
    localObject = new com/d/a/aa;
    int i = 1;
    ((aa)localObject).<init>("TLS_1_1", i, "TLSv1.1");
    b = (aa)localObject;
    localObject = new com/d/a/aa;
    int j = 2;
    ((aa)localObject).<init>("TLS_1_0", j, "TLSv1");
    c = (aa)localObject;
    localObject = new com/d/a/aa;
    int k = 3;
    ((aa)localObject).<init>("SSL_3_0", k, "SSLv3");
    d = (aa)localObject;
    localObject = new aa[4];
    aa localaa = a;
    localObject[0] = localaa;
    localaa = b;
    localObject[i] = localaa;
    localaa = c;
    localObject[j] = localaa;
    localaa = d;
    localObject[k] = localaa;
    f = (aa[])localObject;
  }
  
  private aa(String paramString1)
  {
    e = paramString1;
  }
  
  public static aa a(String paramString)
  {
    int i = paramString.hashCode();
    Object localObject;
    boolean bool3;
    switch (i)
    {
    default: 
      break;
    case 79923350: 
      localObject = "TLSv1";
      boolean bool1 = paramString.equals(localObject);
      if (bool1) {
        int j = 2;
      }
      break;
    case 79201641: 
      localObject = "SSLv3";
      boolean bool2 = paramString.equals(localObject);
      if (bool2) {
        int k = 3;
      }
      break;
    case -503070502: 
      localObject = "TLSv1.2";
      bool3 = paramString.equals(localObject);
      if (bool3)
      {
        bool3 = false;
        localObject = null;
      }
      break;
    case -503070503: 
      localObject = "TLSv1.1";
      bool3 = paramString.equals(localObject);
      if (bool3) {
        bool3 = true;
      }
      break;
    }
    int m = -1;
    switch (m)
    {
    default: 
      localObject = new java/lang/IllegalArgumentException;
      paramString = String.valueOf(paramString);
      paramString = "Unexpected TLS version: ".concat(paramString);
      ((IllegalArgumentException)localObject).<init>(paramString);
      throw ((Throwable)localObject);
    case 3: 
      return d;
    case 2: 
      return c;
    case 1: 
      return b;
    }
    return a;
  }
}

/* Location:
 * Qualified Name:     com.d.a.aa
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */