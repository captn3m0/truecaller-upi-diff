package com.d.a;

import com.d.a.a.b.k;
import java.util.Collections;
import java.util.List;

public final class x
{
  public final v a;
  final u b;
  public final int c;
  final String d;
  public final o e;
  public final p f;
  public final y g;
  public x h;
  x i;
  final x j;
  private volatile d k;
  
  private x(x.a parama)
  {
    Object localObject = a;
    a = ((v)localObject);
    localObject = b;
    b = ((u)localObject);
    int m = c;
    c = m;
    localObject = d;
    d = ((String)localObject);
    localObject = e;
    e = ((o)localObject);
    localObject = f.a();
    f = ((p)localObject);
    localObject = g;
    g = ((y)localObject);
    localObject = h;
    h = ((x)localObject);
    localObject = i;
    i = ((x)localObject);
    parama = j;
    j = parama;
  }
  
  public final int a()
  {
    return c;
  }
  
  public final String a(String paramString)
  {
    p localp = f;
    paramString = localp.a(paramString);
    if (paramString != null) {
      return paramString;
    }
    return null;
  }
  
  public final String b()
  {
    return d;
  }
  
  public final y c()
  {
    return g;
  }
  
  public final x.a d()
  {
    x.a locala = new com/d/a/x$a;
    locala.<init>(this, (byte)0);
    return locala;
  }
  
  public final x e()
  {
    return i;
  }
  
  public final List f()
  {
    int m = c;
    int n = 401;
    String str;
    if (m == n)
    {
      str = "WWW-Authenticate";
    }
    else
    {
      n = 407;
      if (m != n) {
        break label41;
      }
      str = "Proxy-Authenticate";
    }
    return k.a(f, str);
    label41:
    return Collections.emptyList();
  }
  
  public final d g()
  {
    d locald = k;
    if (locald != null) {
      return locald;
    }
    locald = d.a(f);
    k = locald;
    return locald;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Response{protocol=");
    Object localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", code=");
    int m = c;
    localStringBuilder.append(m);
    localStringBuilder.append(", message=");
    localObject = d;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", url=");
    localObject = a.a.toString();
    localStringBuilder.append((String)localObject);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.d.a.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */