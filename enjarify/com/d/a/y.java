package com.d.a;

import d.e;
import java.io.Closeable;
import java.io.InputStream;

public abstract class y
  implements Closeable
{
  public abstract long a();
  
  public abstract e b();
  
  public final InputStream c()
  {
    return b().f();
  }
  
  public void close()
  {
    b().close();
  }
}

/* Location:
 * Qualified Name:     com.d.a.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */