package com.d.a;

import com.d.a.a.b.s;
import com.d.a.a.c.b;
import com.d.a.a.e;
import com.d.a.a.i;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import javax.net.ssl.SSLSocket;

final class t$1
  extends com.d.a.a.d
{
  public final b a(j paramj, a parama, s params)
  {
    boolean bool1 = j.f;
    if (!bool1)
    {
      bool1 = Thread.holdsLock(paramj);
      if (!bool1)
      {
        paramj = new java/lang/AssertionError;
        paramj.<init>();
        throw paramj;
      }
    }
    paramj = d.iterator();
    b localb;
    boolean bool2;
    do
    {
      do
      {
        int i;
        int j;
        do
        {
          bool1 = paramj.hasNext();
          if (!bool1) {
            break;
          }
          localb = (b)paramj.next();
          localObject = h;
          i = ((List)localObject).size();
          com.d.a.a.a.d locald = d;
          if (locald != null) {
            j = locald.a();
          } else {
            j = 1;
          }
        } while (i >= j);
        Object localObject = a.a;
        bool2 = parama.equals(localObject);
      } while (!bool2);
      bool2 = i;
    } while (bool2);
    params.a(localb);
    return localb;
    return null;
  }
  
  public final e a(t paramt)
  {
    return k;
  }
  
  public final i a(j paramj)
  {
    return e;
  }
  
  public final void a(k paramk, SSLSocket paramSSLSocket, boolean paramBoolean)
  {
    String[] arrayOfString1 = f;
    Object localObject1;
    if (arrayOfString1 != null)
    {
      arrayOfString2 = f;
      localObject1 = paramSSLSocket.getEnabledCipherSuites();
      arrayOfString1 = (String[])com.d.a.a.j.a(String.class, arrayOfString2, (Object[])localObject1);
    }
    else
    {
      arrayOfString1 = paramSSLSocket.getEnabledCipherSuites();
    }
    String[] arrayOfString2 = g;
    if (arrayOfString2 != null)
    {
      localObject1 = g;
      String[] arrayOfString3 = paramSSLSocket.getEnabledProtocols();
      arrayOfString2 = (String[])com.d.a.a.j.a(String.class, (Object[])localObject1, arrayOfString3);
    }
    else
    {
      arrayOfString2 = paramSSLSocket.getEnabledProtocols();
    }
    if (paramBoolean)
    {
      localObject2 = paramSSLSocket.getSupportedCipherSuites();
      localObject1 = "TLS_FALLBACK_SCSV";
      paramBoolean = com.d.a.a.j.a((String[])localObject2, (String)localObject1);
      if (paramBoolean)
      {
        localObject2 = "TLS_FALLBACK_SCSV";
        arrayOfString1 = com.d.a.a.j.b(arrayOfString1, (String)localObject2);
      }
    }
    Object localObject2 = new com/d/a/k$a;
    ((k.a)localObject2).<init>(paramk);
    paramk = ((k.a)localObject2).a(arrayOfString1).b(arrayOfString2).b();
    localObject2 = g;
    if (localObject2 != null)
    {
      localObject2 = g;
      paramSSLSocket.setEnabledProtocols((String[])localObject2);
    }
    localObject2 = f;
    if (localObject2 != null)
    {
      paramk = f;
      paramSSLSocket.setEnabledCipherSuites(paramk);
    }
  }
  
  public final void a(p.a parama, String paramString)
  {
    parama.a(paramString);
  }
  
  public final boolean a(j paramj, b paramb)
  {
    boolean bool = j.f;
    if (!bool)
    {
      bool = Thread.holdsLock(paramj);
      if (!bool)
      {
        paramj = new java/lang/AssertionError;
        paramj.<init>();
        throw paramj;
      }
    }
    bool = i;
    if (!bool)
    {
      int i = b;
      if (i != 0)
      {
        paramj.notifyAll();
        return false;
      }
    }
    d.remove(paramb);
    return true;
  }
  
  public final void b(j paramj, b paramb)
  {
    boolean bool = j.f;
    if (!bool)
    {
      bool = Thread.holdsLock(paramj);
      if (!bool)
      {
        paramj = new java/lang/AssertionError;
        paramj.<init>();
        throw paramj;
      }
    }
    Object localObject = d;
    bool = ((Deque)localObject).isEmpty();
    if (bool)
    {
      localObject = a;
      Runnable localRunnable = c;
      ((Executor)localObject).execute(localRunnable);
    }
    d.add(paramb);
  }
}

/* Location:
 * Qualified Name:     com.d.a.t.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */