package com.d.a;

import java.util.concurrent.TimeUnit;

public final class d
{
  public static final d a;
  public static final d b;
  public final boolean c;
  public final boolean d;
  public final int e;
  public final boolean f;
  public final boolean g;
  public final boolean h;
  public final int i;
  public final int j;
  public final boolean k;
  String l;
  private final int m;
  private final boolean n;
  
  static
  {
    d.a locala = new com/d/a/d$a;
    locala.<init>();
    boolean bool = true;
    a = bool;
    a = locala.c();
    locala = new com/d/a/d$a;
    locala.<init>();
    f = bool;
    TimeUnit localTimeUnit = TimeUnit.SECONDS;
    long l1 = 2147483647L;
    long l2 = localTimeUnit.toSeconds(l1);
    bool = l2 < l1;
    int i1;
    if (bool) {
      i1 = -1 >>> 1;
    } else {
      i1 = (int)l2;
    }
    d = i1;
    b = locala.c();
  }
  
  private d(d.a parama)
  {
    boolean bool1 = a;
    c = bool1;
    bool1 = b;
    d = bool1;
    int i1 = c;
    e = i1;
    m = -1;
    f = false;
    g = false;
    h = false;
    i1 = d;
    i = i1;
    i1 = e;
    j = i1;
    boolean bool2 = f;
    k = bool2;
    boolean bool3 = g;
    n = bool3;
  }
  
  private d(boolean paramBoolean1, boolean paramBoolean2, int paramInt1, int paramInt2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, int paramInt3, int paramInt4, boolean paramBoolean6, boolean paramBoolean7, String paramString)
  {
    c = paramBoolean1;
    d = paramBoolean2;
    e = paramInt1;
    m = paramInt2;
    f = paramBoolean3;
    g = paramBoolean4;
    h = paramBoolean5;
    i = paramInt3;
    j = paramInt4;
    k = paramBoolean6;
    n = paramBoolean7;
    l = paramString;
  }
  
  public static d a(p paramp)
  {
    Object localObject1 = paramp;
    String[] arrayOfString = a;
    int i1 = arrayOfString.length / 2;
    int i2 = 0;
    int i3 = 1;
    Object localObject2 = null;
    boolean bool1 = false;
    boolean bool2 = false;
    int i4 = -1;
    int i5 = -1;
    boolean bool3 = false;
    boolean bool4 = false;
    boolean bool5 = false;
    int i6 = -1;
    int i7 = -1;
    boolean bool6 = false;
    boolean bool7 = false;
    while (i2 < i1)
    {
      String str1 = ((p)localObject1).a(i2);
      String str2 = ((p)localObject1).b(i2);
      String str3 = "Cache-Control";
      boolean bool8 = str1.equalsIgnoreCase(str3);
      if (bool8)
      {
        if (localObject2 != null) {
          i3 = 0;
        } else {
          localObject2 = str2;
        }
      }
      else
      {
        str3 = "Pragma";
        i9 = str1.equalsIgnoreCase(str3);
        if (i9 == 0) {
          break label745;
        }
        i3 = 0;
      }
      int i9 = 0;
      str1 = null;
      for (;;)
      {
        int i8 = str2.length();
        if (i9 >= i8) {
          break;
        }
        str3 = "=,;";
        i8 = com.d.a.a.b.d.a(str2, i9, str3);
        str1 = str2.substring(i9, i8).trim();
        int i10 = str2.length();
        if (i8 != i10)
        {
          int i11 = str2.charAt(i8);
          int i16 = 44;
          if (i11 != i16)
          {
            int i17 = str2.charAt(i8);
            i11 = 59;
            if (i17 != i11)
            {
              i8 += 1;
              int i18 = com.d.a.a.b.d.a(str2, i8);
              i8 = str2.length();
              if (i18 < i8)
              {
                i8 = str2.charAt(i18);
                i11 = 34;
                if (i8 == i11)
                {
                  i18 += 1;
                  str3 = "\"";
                  i8 = com.d.a.a.b.d.a(str2, i19, str3);
                  localObject1 = str2.substring(i19, i8);
                  int i12 = 1;
                  i8 += i12;
                  break label380;
                }
              }
              bool9 = true;
              str3 = ",;";
              i8 = com.d.a.a.b.d.a(str2, i19, str3);
              localObject1 = str2.substring(i19, i8).trim();
              break label380;
            }
          }
        }
        boolean bool9 = true;
        i8 += 1;
        int i19 = 0;
        localObject1 = null;
        label380:
        String str4 = "no-cache";
        bool9 = str4.equalsIgnoreCase(str1);
        if (bool9)
        {
          i9 = i8;
          localObject1 = paramp;
          bool1 = true;
        }
        else
        {
          str4 = "no-store";
          bool9 = str4.equalsIgnoreCase(str1);
          if (bool9)
          {
            i9 = i8;
            localObject1 = paramp;
            bool2 = true;
          }
          else
          {
            str4 = "max-age";
            bool9 = str4.equalsIgnoreCase(str1);
            if (bool9)
            {
              int i13 = -1;
              i4 = com.d.a.a.b.d.b((String)localObject1, i13);
              i9 = i8;
              localObject1 = paramp;
            }
            else
            {
              str4 = "s-maxage";
              boolean bool10 = str4.equalsIgnoreCase(str1);
              if (bool10)
              {
                int i14 = -1;
                i5 = com.d.a.a.b.d.b((String)localObject1, i14);
                i9 = i8;
                localObject1 = paramp;
              }
              else
              {
                str4 = "private";
                boolean bool11 = str4.equalsIgnoreCase(str1);
                if (bool11)
                {
                  i9 = i8;
                  localObject1 = paramp;
                  bool3 = true;
                }
                else
                {
                  str4 = "public";
                  bool11 = str4.equalsIgnoreCase(str1);
                  if (bool11)
                  {
                    i9 = i8;
                    localObject1 = paramp;
                    bool4 = true;
                  }
                  else
                  {
                    str4 = "must-revalidate";
                    bool11 = str4.equalsIgnoreCase(str1);
                    if (bool11)
                    {
                      i9 = i8;
                      localObject1 = paramp;
                      bool5 = true;
                    }
                    else
                    {
                      str4 = "max-stale";
                      bool11 = str4.equalsIgnoreCase(str1);
                      if (bool11)
                      {
                        i6 = com.d.a.a.b.d.b((String)localObject1, -1 >>> 1);
                        i9 = i8;
                        localObject1 = paramp;
                      }
                      else
                      {
                        str4 = "min-fresh";
                        bool11 = str4.equalsIgnoreCase(str1);
                        if (bool11)
                        {
                          i15 = -1;
                          i7 = com.d.a.a.b.d.b((String)localObject1, i15);
                          i9 = i8;
                          localObject1 = paramp;
                        }
                        else
                        {
                          i15 = -1;
                          localObject1 = "only-if-cached";
                          boolean bool12 = ((String)localObject1).equalsIgnoreCase(str1);
                          if (bool12)
                          {
                            i9 = i8;
                            localObject1 = paramp;
                            bool6 = true;
                          }
                          else
                          {
                            localObject1 = "no-transform";
                            bool12 = ((String)localObject1).equalsIgnoreCase(str1);
                            if (bool12) {
                              bool7 = true;
                            }
                            i9 = i8;
                            localObject1 = paramp;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      int i15 = -1;
      break label748;
      label745:
      i15 = -1;
      label748:
      i2 += 1;
      localObject1 = paramp;
    }
    Object localObject3;
    if (i3 == 0) {
      localObject3 = null;
    } else {
      localObject3 = localObject2;
    }
    localObject1 = new com/d/a/d;
    localObject2 = localObject1;
    ((d)localObject1).<init>(bool1, bool2, i4, i5, bool3, bool4, bool5, i6, i7, bool6, bool7, (String)localObject3);
    return (d)localObject1;
  }
  
  public final String toString()
  {
    Object localObject = l;
    if (localObject != null) {
      return (String)localObject;
    }
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    boolean bool1 = c;
    String str;
    if (bool1)
    {
      str = "no-cache, ";
      ((StringBuilder)localObject).append(str);
    }
    bool1 = d;
    if (bool1)
    {
      str = "no-store, ";
      ((StringBuilder)localObject).append(str);
    }
    int i1 = e;
    int i4 = -1;
    if (i1 != i4)
    {
      ((StringBuilder)localObject).append("max-age=");
      i1 = e;
      ((StringBuilder)localObject).append(i1);
      str = ", ";
      ((StringBuilder)localObject).append(str);
    }
    i1 = m;
    if (i1 != i4)
    {
      ((StringBuilder)localObject).append("s-maxage=");
      i1 = m;
      ((StringBuilder)localObject).append(i1);
      str = ", ";
      ((StringBuilder)localObject).append(str);
    }
    boolean bool2 = f;
    if (bool2)
    {
      str = "private, ";
      ((StringBuilder)localObject).append(str);
    }
    bool2 = g;
    if (bool2)
    {
      str = "public, ";
      ((StringBuilder)localObject).append(str);
    }
    bool2 = h;
    if (bool2)
    {
      str = "must-revalidate, ";
      ((StringBuilder)localObject).append(str);
    }
    int i2 = i;
    if (i2 != i4)
    {
      ((StringBuilder)localObject).append("max-stale=");
      i2 = i;
      ((StringBuilder)localObject).append(i2);
      str = ", ";
      ((StringBuilder)localObject).append(str);
    }
    i2 = j;
    if (i2 != i4)
    {
      ((StringBuilder)localObject).append("min-fresh=");
      i2 = j;
      ((StringBuilder)localObject).append(i2);
      str = ", ";
      ((StringBuilder)localObject).append(str);
    }
    boolean bool3 = k;
    if (bool3)
    {
      str = "only-if-cached, ";
      ((StringBuilder)localObject).append(str);
    }
    bool3 = n;
    if (bool3)
    {
      str = "no-transform, ";
      ((StringBuilder)localObject).append(str);
    }
    int i3 = ((StringBuilder)localObject).length();
    if (i3 == 0)
    {
      localObject = "";
    }
    else
    {
      i3 = ((StringBuilder)localObject).length() + -2;
      i4 = ((StringBuilder)localObject).length();
      ((StringBuilder)localObject).delete(i3, i4);
      localObject = ((StringBuilder)localObject).toString();
    }
    l = ((String)localObject);
    return (String)localObject;
  }
}

/* Location:
 * Qualified Name:     com.d.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */