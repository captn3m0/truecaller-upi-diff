package com.d.a;

import d.c;
import java.net.IDN;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

public final class q$a
{
  String a;
  String b = "";
  String c = "";
  String d;
  int e = -1;
  final List f;
  List g;
  String h;
  
  public q$a()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    f = localArrayList;
    f.add("");
  }
  
  private static int a(String paramString, int paramInt)
  {
    int i = 0;
    while (i < paramInt)
    {
      int j = paramString.charAt(i);
      switch (j)
      {
      default: 
        return i;
      }
      i += 1;
    }
    return paramInt;
  }
  
  private static String a(byte[] paramArrayOfByte)
  {
    int i = 0;
    int j = 0;
    c localc = null;
    int k = -1;
    int m = 0;
    int n;
    int i1;
    int i2;
    for (;;)
    {
      n = paramArrayOfByte.length;
      i1 = 16;
      if (j >= n) {
        break;
      }
      n = j;
      while (n < i1)
      {
        i2 = paramArrayOfByte[n];
        if (i2 != 0) {
          break;
        }
        i2 = n + 1;
        i2 = paramArrayOfByte[i2];
        if (i2 != 0) {
          break;
        }
        n += 2;
      }
      i1 = n - j;
      if (i1 > m)
      {
        k = j;
        m = i1;
      }
      j = n + 2;
    }
    localc = new d/c;
    localc.<init>();
    for (;;)
    {
      n = paramArrayOfByte.length;
      if (i >= n) {
        break;
      }
      n = 58;
      if (i == k)
      {
        localc.b(n);
        i += m;
        if (i == i1) {
          localc.b(n);
        }
      }
      else
      {
        if (i > 0) {
          localc.b(n);
        }
        n = (paramArrayOfByte[i] & 0xFF) << 8;
        i2 = i + 1;
        i2 = paramArrayOfByte[i2] & 0xFF;
        n |= i2;
        long l = n;
        localc.j(l);
        i += 2;
      }
    }
    return localc.p();
  }
  
  private void a(String paramString, int paramInt1, int paramInt2)
  {
    if (paramInt1 == paramInt2) {
      return;
    }
    int i = paramString.charAt(paramInt1);
    int j = 47;
    int k = 1;
    String str1;
    if (i != j)
    {
      j = 92;
      if (i != j)
      {
        localObject = f;
        j = ((List)localObject).size() - k;
        str1 = "";
        ((List)localObject).set(j, str1);
        m = paramInt1;
        break label117;
      }
    }
    f.clear();
    Object localObject = f;
    String str2 = "";
    ((List)localObject).add(str2);
    paramInt1 += 1;
    label117:
    label240:
    label320:
    int n;
    for (int m = paramInt1; m < paramInt2; n = paramInt1)
    {
      String str3 = "/\\";
      paramInt1 = q.a(paramString, m, paramInt2, str3);
      i = 0;
      localObject = null;
      if (paramInt1 < paramInt2)
      {
        j = 1;
      }
      else
      {
        j = 0;
        str2 = null;
      }
      String str4 = " \"<>^`{}|/\\?#";
      boolean bool3 = true;
      boolean bool4 = true;
      str1 = paramString;
      str1 = q.a(paramString, m, paramInt1, str4, bool3, false, bool4);
      String str5 = ".";
      boolean bool2 = str1.equals(str5);
      if (!bool2)
      {
        str5 = "%2e";
        bool2 = str1.equalsIgnoreCase(str5);
        if (!bool2)
        {
          bool2 = false;
          str5 = null;
          break label240;
        }
      }
      bool2 = true;
      if (!bool2)
      {
        str5 = "..";
        bool2 = str1.equals(str5);
        if (!bool2)
        {
          str5 = "%2e.";
          bool2 = str1.equalsIgnoreCase(str5);
          if (!bool2)
          {
            str5 = ".%2e";
            bool2 = str1.equalsIgnoreCase(str5);
            if (!bool2)
            {
              str5 = "%2e%2e";
              bool2 = str1.equalsIgnoreCase(str5);
              if (!bool2) {
                break label320;
              }
            }
          }
        }
        i = 1;
        boolean bool1;
        if (i != 0)
        {
          localObject = f;
          int i1 = ((List)localObject).size() - k;
          localObject = (String)((List)localObject).remove(i1);
          bool1 = ((String)localObject).isEmpty();
          if (bool1)
          {
            localObject = f;
            bool1 = ((List)localObject).isEmpty();
            if (!bool1)
            {
              localObject = f;
              i1 = ((List)localObject).size() - k;
              str5 = "";
              ((List)localObject).set(i1, str5);
              break label567;
            }
          }
          localObject = f;
          str1 = "";
          ((List)localObject).add(str1);
        }
        else
        {
          localObject = f;
          n = ((List)localObject).size() - k;
          localObject = (String)((List)localObject).get(n);
          bool1 = ((String)localObject).isEmpty();
          if (bool1)
          {
            localObject = f;
            n = ((List)localObject).size() - k;
            ((List)localObject).set(n, str1);
          }
          else
          {
            localObject = f;
            ((List)localObject).add(str1);
          }
          if (j != 0)
          {
            localObject = f;
            str1 = "";
            ((List)localObject).add(str1);
          }
        }
      }
      label567:
      if (j != 0) {
        paramInt1 += 1;
      }
    }
  }
  
  private static int b(String paramString, int paramInt1, int paramInt2)
  {
    paramInt2 += -1;
    while (paramInt2 >= paramInt1)
    {
      int i = paramString.charAt(paramInt2);
      switch (i)
      {
      default: 
        return paramInt2 + 1;
      }
      paramInt2 += -1;
    }
    return paramInt1;
  }
  
  private static InetAddress b(String paramString, int paramInt)
  {
    Object localObject = paramString;
    int i = paramInt;
    int j = 16;
    byte[] arrayOfByte = new byte[j];
    int k = -1;
    int m = 1;
    int n = 0;
    int i1 = 1;
    int i3 = 0;
    int i4 = -1;
    int i5 = -1;
    while (i1 < i)
    {
      if (i3 == j) {
        return null;
      }
      int i6 = i1 + 2;
      int i8 = 255;
      String str1;
      if (i6 <= i)
      {
        str1 = "::";
        boolean bool2 = ((String)localObject).regionMatches(i1, str1, 0, 2);
        if (bool2)
        {
          if (i4 != k) {
            return null;
          }
          i3 += 2;
          if (i6 == i)
          {
            i4 = i3;
            i10 = 16;
            break label612;
          }
          i4 = i3;
          i5 = i6;
          break label473;
        }
      }
      if (i3 != 0)
      {
        String str2 = ":";
        int i7 = ((String)localObject).regionMatches(i1, str2, 0, m);
        if (i7 != 0)
        {
          i1 += 1;
          i5 = i1;
        }
        else
        {
          str2 = ".";
          boolean bool1 = ((String)localObject).regionMatches(i1, str2, 0, m);
          if (bool1)
          {
            i2 = i3 + -2;
            i7 = i2;
            while (i5 < i)
            {
              if (i7 == j) {
                break label447;
              }
              if (i7 != i2)
              {
                i9 = ((String)localObject).charAt(i5);
                i11 = 46;
                if (i9 != i11) {
                  break label447;
                }
                i5 += 1;
              }
              int i9 = i5;
              int i11 = 0;
              str1 = null;
              while (i9 < i)
              {
                m = ((String)localObject).charAt(i9);
                n = 48;
                if (m < n) {
                  break;
                }
                j = 57;
                if (m > j) {
                  break;
                }
                if ((i11 == 0) && (i5 != i9))
                {
                  n = 0;
                  break label447;
                }
                i11 = i11 * 10 + m - n;
                if (i11 > i8)
                {
                  n = 0;
                  break label447;
                }
                i9 += 1;
                j = 16;
                m = 1;
                n = 0;
              }
              j = i9 - i5;
              if (j == 0)
              {
                n = 0;
                break label447;
              }
              j = i7 + 1;
              m = (byte)i11;
              arrayOfByte[i7] = m;
              i7 = j;
              i5 = i9;
              j = 16;
              m = 1;
              n = 0;
            }
            j = 4;
            i2 += j;
            if (i7 != i2) {
              n = 0;
            } else {
              n = 1;
            }
            label447:
            if (n == 0) {
              return null;
            }
            i3 += 2;
            i10 = 16;
            break label612;
          }
          return null;
        }
      }
      else
      {
        i5 = i2;
      }
      label473:
      int i2 = i5;
      j = 0;
      while (i2 < i)
      {
        m = q.a(((String)localObject).charAt(i2));
        if (m == k) {
          break;
        }
        j = (j << 4) + m;
        i2 += 1;
      }
      m = i2 - i5;
      if (m != 0)
      {
        n = 4;
        if (m <= n)
        {
          m = i3 + 1;
          n = (byte)(j >>> 8 & i8);
          arrayOfByte[i3] = n;
          i3 = m + 1;
          j = (byte)(j & 0xFF);
          arrayOfByte[m] = j;
          j = 16;
          m = 1;
          n = 0;
          continue;
        }
      }
      return null;
    }
    int i10 = 16;
    label612:
    if (i3 != i10)
    {
      if (i4 == k) {
        return null;
      }
      i = i3 - i4;
      j = 16 - i;
      System.arraycopy(arrayOfByte, i4, arrayOfByte, j, i);
      j = 16 - i3 + i4;
      i10 = 0;
      localObject = null;
      Arrays.fill(arrayOfByte, i4, j, (byte)0);
    }
    try
    {
      return InetAddress.getByAddress(arrayOfByte);
    }
    catch (UnknownHostException localUnknownHostException)
    {
      localObject = new java/lang/AssertionError;
      ((AssertionError)localObject).<init>();
      throw ((Throwable)localObject);
    }
  }
  
  private static int c(String paramString, int paramInt1, int paramInt2)
  {
    int i = paramInt2 - paramInt1;
    int j = -1;
    int k = 2;
    if (i < k) {
      return j;
    }
    i = paramString.charAt(paramInt1);
    k = 90;
    int m = 122;
    int n = 65;
    int i1 = 97;
    if (((i >= i1) && (i <= m)) || ((i >= n) && (i <= k)))
    {
      int i2;
      do
      {
        do
        {
          do
          {
            do
            {
              do
              {
                paramInt1 += 1;
                if (paramInt1 >= paramInt2) {
                  break;
                }
                i = paramString.charAt(paramInt1);
              } while (((i >= i1) && (i <= m)) || ((i >= n) && (i <= k)));
              i2 = 48;
              if (i < i2) {
                break;
              }
              i2 = 57;
            } while (i <= i2);
            i2 = 43;
          } while (i == i2);
          i2 = 45;
        } while (i == i2);
        i2 = 46;
      } while (i == i2);
      int i3 = 58;
      if (i == i3) {
        return paramInt1;
      }
      return j;
      return j;
    }
    return j;
  }
  
  private static int d(String paramString, int paramInt1, int paramInt2)
  {
    int i = 0;
    while (paramInt1 < paramInt2)
    {
      int j = paramString.charAt(paramInt1);
      int k = 92;
      if (j != k)
      {
        k = 47;
        if (j != k) {
          break;
        }
      }
      i += 1;
      paramInt1 += 1;
    }
    return i;
  }
  
  private static String d(String paramString)
  {
    try
    {
      paramString = IDN.toASCII(paramString);
      Locale localLocale = Locale.US;
      paramString = paramString.toLowerCase(localLocale);
      boolean bool = paramString.isEmpty();
      if (bool) {
        return null;
      }
      bool = false;
      localLocale = null;
      int i = 0;
      for (;;)
      {
        int j = paramString.length();
        k = 1;
        if (i >= j) {
          break;
        }
        j = paramString.charAt(i);
        int m = 31;
        if (j <= m) {
          break label115;
        }
        m = 127;
        if (j >= m) {
          break label115;
        }
        String str = " #%/:?@[\\]";
        j = str.indexOf(j);
        m = -1;
        if (j != m) {
          break label115;
        }
        i += 1;
      }
      int k = 0;
      label115:
      if (k != 0) {
        return null;
      }
      return paramString;
    }
    catch (IllegalArgumentException localIllegalArgumentException) {}
    return null;
  }
  
  private static int e(String paramString, int paramInt1, int paramInt2)
  {
    while (paramInt1 < paramInt2)
    {
      int i = paramString.charAt(paramInt1);
      int j = 58;
      if (i != j)
      {
        j = 91;
        if (i == j) {
          do
          {
            paramInt1 += 1;
            if (paramInt1 >= paramInt2) {
              break;
            }
            i = paramString.charAt(paramInt1);
            j = 93;
          } while (i != j);
        }
        paramInt1 += 1;
      }
      else
      {
        return paramInt1;
      }
    }
    return paramInt2;
  }
  
  private static String f(String paramString, int paramInt1, int paramInt2)
  {
    paramString = q.a(paramString, paramInt1, paramInt2, false);
    String str = "[";
    paramInt1 = paramString.startsWith(str);
    if (paramInt1 != 0)
    {
      str = "]";
      paramInt1 = paramString.endsWith(str);
      if (paramInt1 != 0)
      {
        paramInt1 = paramString.length() + -1;
        paramString = b(paramString, paramInt1);
        if (paramString == null) {
          return null;
        }
        paramString = paramString.getAddress();
        paramInt1 = paramString.length;
        paramInt2 = 16;
        if (paramInt1 == paramInt2) {
          return a(paramString);
        }
        paramString = new java/lang/AssertionError;
        paramString.<init>();
        throw paramString;
      }
    }
    return d(paramString);
  }
  
  private static int g(String paramString, int paramInt1, int paramInt2)
  {
    int i = -1;
    String str = "";
    boolean bool = true;
    try
    {
      paramString = q.a(paramString, paramInt1, paramInt2, str, false, false, bool);
      int j = Integer.parseInt(paramString);
      if (j > 0)
      {
        paramInt1 = (char)-1;
        if (j <= paramInt1) {
          return j;
        }
      }
      return i;
    }
    catch (NumberFormatException localNumberFormatException) {}
    return i;
  }
  
  final int a()
  {
    int i = e;
    int j = -1;
    if (i != j) {
      return i;
    }
    return q.a(a);
  }
  
  final q.a.a a(q paramq, String paramString)
  {
    a locala = this;
    String str1 = paramString;
    int i = paramString.length();
    int k = a(paramString, i);
    i = paramString.length();
    int m = b(paramString, k, i);
    i = c(paramString, k, m);
    int n = -1;
    String str2;
    boolean bool4;
    int i5;
    if (i != n)
    {
      boolean bool3 = true;
      str2 = "https:";
      bool4 = false;
      i5 = 6;
      localObject1 = paramString;
      int i7 = k;
      boolean bool1 = paramString.regionMatches(bool3, k, str2, 0, i5);
      if (bool1)
      {
        localObject1 = "https";
        a = ((String)localObject1);
        k += 6;
      }
      else
      {
        bool3 = true;
        str2 = "http:";
        bool4 = false;
        i5 = 5;
        bool1 = paramString.regionMatches(bool3, k, str2, 0, i5);
        if (bool1)
        {
          localObject1 = "http";
          a = ((String)localObject1);
          k += 5;
        }
        else
        {
          return q.a.a.c;
        }
      }
    }
    else
    {
      if (paramq == null) {
        break label1121;
      }
      localObject1 = q.a(paramq);
      a = ((String)localObject1);
    }
    int j = d(str1, k, m);
    int i2 = 2;
    int i11 = 35;
    Object localObject2;
    String str3;
    if ((j < i2) && (paramq != null))
    {
      localObject2 = q.a(paramq);
      str3 = a;
      i3 = ((String)localObject2).equals(str3);
      if (i3 != 0)
      {
        localObject1 = paramq.d();
        b = ((String)localObject1);
        localObject1 = paramq.e();
        c = ((String)localObject1);
        localObject1 = q.b(paramq);
        d = ((String)localObject1);
        j = q.c(paramq);
        e = j;
        f.clear();
        localObject1 = f;
        localObject2 = paramq.g();
        ((List)localObject1).addAll((Collection)localObject2);
        if (k != m)
        {
          j = str1.charAt(k);
          if (j != i11) {}
        }
        else
        {
          localObject1 = paramq.h();
          locala.c((String)localObject1);
        }
        int i12 = k;
        break label915;
      }
    }
    k += j;
    j = 0;
    Object localObject1 = null;
    int i3 = k;
    int i14 = 0;
    int i15 = 0;
    int i13;
    int i4;
    boolean bool2;
    for (;;)
    {
      localObject1 = "@/\\?#";
      i13 = q.a(str1, i3, m, (String)localObject1);
      if (i13 != m) {
        j = str1.charAt(i13);
      } else {
        j = -1;
      }
      if ((j == n) || (j == i11)) {
        break;
      }
      int i8 = 47;
      if (j == i8) {
        break;
      }
      int i9 = 92;
      if (j == i9) {
        break;
      }
      switch (j)
      {
      default: 
        break;
      case 64: 
        if (i14 == 0)
        {
          k = q.a(str1, i3, i13, ":");
          str2 = " \"':;<=>@[]^`{}|/\\?#";
          bool4 = true;
          i5 = 0;
          int i16 = 1;
          localObject1 = paramString;
          i9 = k;
          int i17 = k;
          k = i16;
          localObject1 = q.a(paramString, i3, i9, str2, bool4, false, i16);
          if (i15 != 0)
          {
            localObject2 = new java/lang/StringBuilder;
            ((StringBuilder)localObject2).<init>();
            str3 = b;
            ((StringBuilder)localObject2).append(str3);
            str3 = "%40";
            ((StringBuilder)localObject2).append(str3);
            ((StringBuilder)localObject2).append((String)localObject1);
            localObject1 = ((StringBuilder)localObject2).toString();
          }
          b = ((String)localObject1);
          if (i17 != i13)
          {
            i4 = i17 + 1;
            str2 = " \"':;<=>@[]^`{}|/\\?#";
            bool4 = true;
            i5 = 0;
            k = 1;
            localObject1 = paramString;
            i9 = i13;
            localObject1 = q.a(paramString, i4, i13, str2, bool4, false, k);
            c = ((String)localObject1);
            i14 = 1;
          }
          i15 = 1;
        }
        else
        {
          StringBuilder localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>();
          localObject1 = c;
          localStringBuilder.append((String)localObject1);
          localStringBuilder.append("%40");
          str2 = " \"':;<=>@[]^`{}|/\\?#";
          bool4 = true;
          i5 = 0;
          bool2 = true;
          localObject1 = paramString;
          i9 = i13;
          localObject1 = q.a(paramString, i4, i13, str2, bool4, false, bool2);
          localStringBuilder.append((String)localObject1);
          localObject1 = localStringBuilder.toString();
          c = ((String)localObject1);
        }
        i4 = i13 + 1;
      }
    }
    j = e(str1, i4, i13);
    int i10 = j + 1;
    if (i10 < i13)
    {
      localObject1 = f(str1, i4, j);
      d = ((String)localObject1);
      j = g(str1, i10, i13);
      e = j;
      j = e;
      if (j == n) {
        return q.a.a.d;
      }
    }
    else
    {
      localObject1 = f(str1, i4, j);
      d = ((String)localObject1);
      localObject1 = a;
      j = q.a((String)localObject1);
      e = j;
    }
    localObject1 = d;
    if (localObject1 == null) {
      return q.a.a.e;
    }
    label915:
    localObject1 = "?#";
    j = q.a(str1, i13, m, (String)localObject1);
    locala.a(str1, i13, j);
    if (j < m)
    {
      i4 = str1.charAt(j);
      i10 = 63;
      if (i4 == i10)
      {
        localObject2 = "#";
        int i1 = q.a(str1, j, m, (String)localObject2);
        i4 = j + 1;
        str2 = " \"'<>#";
        bool4 = true;
        i5 = 1;
        bool2 = true;
        localObject1 = paramString;
        i10 = i1;
        localObject1 = q.b(q.a(paramString, i4, i1, str2, bool4, i5, bool2));
        g = ((List)localObject1);
        j = i1;
      }
    }
    if (j < m)
    {
      i4 = str1.charAt(j);
      if (i4 == i11)
      {
        i4 = 1 + j;
        str2 = "";
        bool4 = true;
        int i6 = 0;
        bool2 = false;
        localObject1 = paramString;
        i10 = m;
        localObject1 = q.a(paramString, i4, m, str2, bool4, false, false);
        h = ((String)localObject1);
      }
    }
    return q.a.a.a;
    label1121:
    return q.a.a.b;
  }
  
  public final a a(int paramInt)
  {
    if (paramInt > 0)
    {
      int i = (char)-1;
      if (paramInt <= i)
      {
        e = paramInt;
        return this;
      }
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    String str = String.valueOf(paramInt);
    str = "unexpected port: ".concat(str);
    localIllegalArgumentException.<init>(str);
    throw localIllegalArgumentException;
  }
  
  public final a a(String paramString)
  {
    Object localObject = "http";
    boolean bool = paramString.equalsIgnoreCase((String)localObject);
    if (bool)
    {
      paramString = "http";
      a = paramString;
    }
    else
    {
      localObject = "https";
      bool = paramString.equalsIgnoreCase((String)localObject);
      if (!bool) {
        break label47;
      }
      paramString = "https";
      a = paramString;
    }
    return this;
    label47:
    localObject = new java/lang/IllegalArgumentException;
    paramString = String.valueOf(paramString);
    paramString = "unexpected scheme: ".concat(paramString);
    ((IllegalArgumentException)localObject).<init>(paramString);
    throw ((Throwable)localObject);
  }
  
  public final a b(String paramString)
  {
    if (paramString != null)
    {
      int i = paramString.length();
      Object localObject = f(paramString, 0, i);
      if (localObject != null)
      {
        d = ((String)localObject);
        return this;
      }
      localObject = new java/lang/IllegalArgumentException;
      paramString = String.valueOf(paramString);
      paramString = "unexpected host: ".concat(paramString);
      ((IllegalArgumentException)localObject).<init>(paramString);
      throw ((Throwable)localObject);
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("host == null");
    throw paramString;
  }
  
  public final q b()
  {
    Object localObject = a;
    if (localObject != null)
    {
      localObject = d;
      if (localObject != null)
      {
        localObject = new com/d/a/q;
        ((q)localObject).<init>(this, (byte)0);
        return (q)localObject;
      }
      localObject = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject).<init>("host == null");
      throw ((Throwable)localObject);
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("scheme == null");
    throw ((Throwable)localObject);
  }
  
  public final a c(String paramString)
  {
    if (paramString != null)
    {
      String str = " \"'<>#";
      boolean bool = true;
      paramString = q.b(q.a(paramString, str, bool, bool));
    }
    else
    {
      paramString = null;
    }
    g = paramString;
    return this;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    Object localObject = a;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append("://");
    localObject = b;
    boolean bool = ((String)localObject).isEmpty();
    char c3 = ':';
    if (bool)
    {
      localObject = c;
      bool = ((String)localObject).isEmpty();
      if (bool) {}
    }
    else
    {
      localObject = b;
      localStringBuilder.append((String)localObject);
      localObject = c;
      bool = ((String)localObject).isEmpty();
      if (!bool)
      {
        localStringBuilder.append(c3);
        localObject = c;
        localStringBuilder.append((String)localObject);
      }
      char c1 = '@';
      localStringBuilder.append(c1);
    }
    localObject = d;
    int i = ((String)localObject).indexOf(c3);
    int k = -1;
    if (i != k)
    {
      localStringBuilder.append('[');
      localObject = d;
      localStringBuilder.append((String)localObject);
      i = 93;
      localStringBuilder.append(i);
    }
    else
    {
      localObject = d;
      localStringBuilder.append((String)localObject);
    }
    int j = a();
    String str = a;
    int m = q.a(str);
    if (j != m)
    {
      localStringBuilder.append(c3);
      localStringBuilder.append(j);
    }
    localObject = f;
    q.a(localStringBuilder, (List)localObject);
    localObject = g;
    if (localObject != null)
    {
      j = 63;
      localStringBuilder.append(j);
      localObject = g;
      q.b(localStringBuilder, (List)localObject);
    }
    localObject = h;
    if (localObject != null)
    {
      char c2 = '#';
      localStringBuilder.append(c2);
      localObject = h;
      localStringBuilder.append((String)localObject);
    }
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.d.a.q.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */