package com.d.a;

import com.d.a.a.j;
import java.net.Proxy;
import java.net.ProxySelector;
import java.util.List;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;

public final class a
{
  public final q a;
  public final n b;
  public final SocketFactory c;
  public final b d;
  public final List e;
  public final List f;
  public final ProxySelector g;
  public final Proxy h;
  public final SSLSocketFactory i;
  public final HostnameVerifier j;
  public final f k;
  
  public a(String paramString, int paramInt, n paramn, SocketFactory paramSocketFactory, SSLSocketFactory paramSSLSocketFactory, HostnameVerifier paramHostnameVerifier, f paramf, b paramb, Proxy paramProxy, List paramList1, List paramList2, ProxySelector paramProxySelector)
  {
    q.a locala = new com/d/a/q$a;
    locala.<init>();
    String str;
    if (paramSSLSocketFactory != null) {
      str = "https";
    } else {
      str = "http";
    }
    locala = locala.a(str);
    paramString = locala.b(paramString).a(paramInt).b();
    a = paramString;
    if (paramn != null)
    {
      b = paramn;
      if (paramSocketFactory != null)
      {
        c = paramSocketFactory;
        if (paramb != null)
        {
          d = paramb;
          if (paramList1 != null)
          {
            paramString = j.a(paramList1);
            e = paramString;
            if (paramList2 != null)
            {
              paramString = j.a(paramList2);
              f = paramString;
              if (paramProxySelector != null)
              {
                g = paramProxySelector;
                h = paramProxy;
                i = paramSSLSocketFactory;
                j = paramHostnameVerifier;
                k = paramf;
                return;
              }
              paramString = new java/lang/IllegalArgumentException;
              paramString.<init>("proxySelector == null");
              throw paramString;
            }
            paramString = new java/lang/IllegalArgumentException;
            paramString.<init>("connectionSpecs == null");
            throw paramString;
          }
          paramString = new java/lang/IllegalArgumentException;
          paramString.<init>("protocols == null");
          throw paramString;
        }
        paramString = new java/lang/IllegalArgumentException;
        paramString.<init>("authenticator == null");
        throw paramString;
      }
      paramString = new java/lang/IllegalArgumentException;
      paramString.<init>("socketFactory == null");
      throw paramString;
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("dns == null");
    throw paramString;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof a;
    if (bool1)
    {
      paramObject = (a)paramObject;
      Object localObject1 = a;
      Object localObject2 = a;
      bool1 = ((q)localObject1).equals(localObject2);
      if (bool1)
      {
        localObject1 = b;
        localObject2 = b;
        bool1 = localObject1.equals(localObject2);
        if (bool1)
        {
          localObject1 = d;
          localObject2 = d;
          bool1 = localObject1.equals(localObject2);
          if (bool1)
          {
            localObject1 = e;
            localObject2 = e;
            bool1 = ((List)localObject1).equals(localObject2);
            if (bool1)
            {
              localObject1 = f;
              localObject2 = f;
              bool1 = ((List)localObject1).equals(localObject2);
              if (bool1)
              {
                localObject1 = g;
                localObject2 = g;
                bool1 = localObject1.equals(localObject2);
                if (bool1)
                {
                  localObject1 = h;
                  localObject2 = h;
                  bool1 = j.a(localObject1, localObject2);
                  if (bool1)
                  {
                    localObject1 = i;
                    localObject2 = i;
                    bool1 = j.a(localObject1, localObject2);
                    if (bool1)
                    {
                      localObject1 = j;
                      localObject2 = j;
                      bool1 = j.a(localObject1, localObject2);
                      if (bool1)
                      {
                        localObject1 = k;
                        paramObject = k;
                        boolean bool2 = j.a(localObject1, paramObject);
                        if (bool2) {
                          return true;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return false;
  }
  
  public final int hashCode()
  {
    q localq = a;
    int m = (localq.hashCode() + 527) * 31;
    int n = b.hashCode();
    m = (m + n) * 31;
    n = d.hashCode();
    m = (m + n) * 31;
    n = e.hashCode();
    m = (m + n) * 31;
    n = f.hashCode();
    m = (m + n) * 31;
    n = g.hashCode();
    m = (m + n) * 31;
    Object localObject = h;
    int i1 = 0;
    if (localObject != null)
    {
      n = ((Proxy)localObject).hashCode();
    }
    else
    {
      n = 0;
      localObject = null;
    }
    m = (m + n) * 31;
    localObject = i;
    if (localObject != null)
    {
      n = localObject.hashCode();
    }
    else
    {
      n = 0;
      localObject = null;
    }
    m = (m + n) * 31;
    localObject = j;
    if (localObject != null)
    {
      n = localObject.hashCode();
    }
    else
    {
      n = 0;
      localObject = null;
    }
    m = (m + n) * 31;
    localObject = k;
    if (localObject != null) {
      i1 = localObject.hashCode();
    }
    return m + i1;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */