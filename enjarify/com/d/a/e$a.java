package com.d.a;

import java.util.List;

final class e$a
  implements r.a
{
  private final int b;
  private final v c;
  private final boolean d;
  
  e$a(e parame, int paramInt, v paramv, boolean paramBoolean)
  {
    b = paramInt;
    c = paramv;
    d = paramBoolean;
  }
  
  public final x a(v paramv)
  {
    int i = b;
    Object localObject1 = a.a.g;
    int j = ((List)localObject1).size();
    if (i < j)
    {
      localObject2 = new com/d/a/e$a;
      localObject1 = a;
      int k = b + 1;
      boolean bool2 = d;
      ((a)localObject2).<init>((e)localObject1, k, paramv, bool2);
      paramv = a.a.g;
      i = b;
      paramv = (r)paramv.get(i);
      localObject2 = paramv.a();
      if (localObject2 != null) {
        return (x)localObject2;
      }
      localObject2 = new java/lang/NullPointerException;
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("application interceptor ");
      ((StringBuilder)localObject1).append(paramv);
      ((StringBuilder)localObject1).append(" returned null");
      paramv = ((StringBuilder)localObject1).toString();
      ((NullPointerException)localObject2).<init>(paramv);
      throw ((Throwable)localObject2);
    }
    Object localObject2 = a;
    boolean bool1 = d;
    return ((e)localObject2).a(paramv, bool1);
  }
}

/* Location:
 * Qualified Name:     com.d.a.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */