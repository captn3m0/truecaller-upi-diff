package com.d.a.a;

import com.d.a.a.c.a;
import d.d;
import d.e;
import d.n;
import d.t;
import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class b
  implements Closeable
{
  static final Pattern a;
  private static final t u;
  private final a c;
  private final File d;
  private final File e;
  private final File f;
  private final File g;
  private final int h;
  private long i;
  private final int j;
  private long k;
  private d l;
  private final LinkedHashMap m;
  private int n;
  private boolean o;
  private boolean p;
  private boolean q;
  private long r;
  private final Executor s;
  private final Runnable t;
  
  static
  {
    a = Pattern.compile("[a-z0-9_-]{1,120}");
    b.3 local3 = new com/d/a/a/b$3;
    local3.<init>();
    u = local3;
  }
  
  private b(a parama, File paramFile, long paramLong, Executor paramExecutor)
  {
    long l1 = 0L;
    k = l1;
    LinkedHashMap localLinkedHashMap = new java/util/LinkedHashMap;
    localLinkedHashMap.<init>(0, 0.75F, true);
    m = localLinkedHashMap;
    r = l1;
    b.1 local1 = new com/d/a/a/b$1;
    local1.<init>(this);
    t = local1;
    c = parama;
    d = paramFile;
    h = 201105;
    parama = new java/io/File;
    parama.<init>(paramFile, "journal");
    e = parama;
    parama = new java/io/File;
    parama.<init>(paramFile, "journal.tmp");
    f = parama;
    parama = new java/io/File;
    parama.<init>(paramFile, "journal.bkp");
    g = parama;
    j = 2;
    i = paramLong;
    s = paramExecutor;
  }
  
  public static b a(a parama, File paramFile, long paramLong)
  {
    long l1 = 0L;
    boolean bool = paramLong < l1;
    if (bool)
    {
      ThreadPoolExecutor localThreadPoolExecutor = new java/util/concurrent/ThreadPoolExecutor;
      Object localObject = TimeUnit.SECONDS;
      LinkedBlockingQueue localLinkedBlockingQueue = new java/util/concurrent/LinkedBlockingQueue;
      localLinkedBlockingQueue.<init>();
      ThreadFactory localThreadFactory = j.b("OkHttp DiskLruCache");
      localThreadPoolExecutor.<init>(0, 1, 60, (TimeUnit)localObject, localLinkedBlockingQueue, localThreadFactory);
      b localb = new com/d/a/a/b;
      localObject = localThreadPoolExecutor;
      localb.<init>(parama, paramFile, paramLong, localThreadPoolExecutor);
      return localb;
    }
    parama = new java/lang/IllegalArgumentException;
    parama.<init>("maxSize <= 0");
    throw parama;
  }
  
  private void a(b.a parama, boolean paramBoolean)
  {
    try
    {
      Object localObject1 = a;
      Object localObject2 = f;
      if (localObject2 == parama)
      {
        int i1 = 0;
        localObject2 = null;
        Object localObject3;
        Object localObject4;
        Object localObject6;
        if (paramBoolean)
        {
          int i2 = e;
          if (i2 == 0)
          {
            i2 = 0;
            localObject3 = null;
            int i3;
            for (;;)
            {
              int i5 = j;
              if (i2 >= i5) {
                break label162;
              }
              localObject4 = b;
              int i6 = localObject4[i2];
              if (i6 == 0) {
                break;
              }
              localObject4 = c;
              Object localObject5 = d;
              localObject5 = localObject5[i2];
              boolean bool3 = ((a)localObject4).e((File)localObject5);
              if (!bool3)
              {
                parama.b();
                return;
              }
              i2 += 1;
            }
            parama.b();
            parama = new java/lang/IllegalStateException;
            localObject6 = "Newly created entry didn't create value for index ";
            localObject1 = String.valueOf(i3);
            localObject6 = ((String)localObject6).concat((String)localObject1);
            parama.<init>((String)localObject6);
            throw parama;
          }
        }
        for (;;)
        {
          label162:
          i8 = j;
          if (i1 >= i8) {
            break;
          }
          parama = d;
          parama = parama[i1];
          if (paramBoolean)
          {
            localObject3 = c;
            boolean bool1 = ((a)localObject3).e(parama);
            if (bool1)
            {
              localObject3 = c;
              localObject3 = localObject3[i1];
              localObject4 = c;
              ((a)localObject4).a(parama, (File)localObject3);
              parama = b;
              long l1 = parama[i1];
              parama = c;
              long l2 = parama.f((File)localObject3);
              parama = b;
              parama[i1] = l2;
              long l3 = k - l1 + l2;
              k = l3;
            }
          }
          else
          {
            localObject3 = c;
            ((a)localObject3).d(parama);
          }
          i1 += 1;
        }
        int i8 = n;
        i1 = 1;
        i8 += i1;
        n = i8;
        i8 = 0;
        parama = null;
        f = null;
        boolean bool4 = e | paramBoolean;
        int i4 = 10;
        int i7 = 32;
        if (bool4)
        {
          e = i1;
          parama = l;
          localObject2 = "CLEAN";
          parama = parama.b((String)localObject2);
          parama.j(i7);
          parama = l;
          localObject2 = a;
          parama.b((String)localObject2);
          parama = l;
          ((b.b)localObject1).a(parama);
          parama = l;
          parama.j(i4);
          if (paramBoolean)
          {
            l4 = r;
            long l5 = 1L + l4;
            r = l5;
            g = l4;
          }
        }
        else
        {
          parama = m;
          localObject6 = a;
          parama.remove(localObject6);
          parama = l;
          localObject6 = "REMOVE";
          parama = parama.b((String)localObject6);
          parama.j(i7);
          parama = l;
          localObject6 = a;
          parama.b((String)localObject6);
          parama = l;
          parama.j(i4);
        }
        parama = l;
        parama.flush();
        long l4 = k;
        long l6 = i;
        boolean bool2 = l4 < l6;
        if (!bool2)
        {
          bool4 = g();
          if (!bool4) {}
        }
        else
        {
          parama = s;
          localObject6 = t;
          parama.execute((Runnable)localObject6);
        }
        return;
      }
      parama = new java/lang/IllegalStateException;
      parama.<init>();
      throw parama;
    }
    finally {}
  }
  
  private boolean a(b.b paramb)
  {
    Object localObject1 = f;
    int i1 = 1;
    if (localObject1 != null)
    {
      localObject1 = f;
      c = i1;
    }
    int i2 = 0;
    localObject1 = null;
    for (;;)
    {
      i3 = j;
      if (i2 >= i3) {
        break;
      }
      localObject2 = c;
      File localFile = c[i2];
      ((a)localObject2).d(localFile);
      long l1 = k;
      long[] arrayOfLong = b;
      long l2 = arrayOfLong[i2];
      l1 -= l2;
      k = l1;
      localObject2 = b;
      long l3 = 0L;
      localObject2[i2] = l3;
      i2 += 1;
    }
    i2 = n + i1;
    n = i2;
    localObject1 = l.b("REMOVE").j(32);
    Object localObject2 = a;
    localObject1 = ((d)localObject1).b((String)localObject2);
    int i3 = 10;
    ((d)localObject1).j(i3);
    localObject1 = m;
    paramb = a;
    ((LinkedHashMap)localObject1).remove(paramb);
    boolean bool = g();
    if (bool)
    {
      paramb = s;
      localObject1 = t;
      paramb.execute((Runnable)localObject1);
    }
    return i1;
  }
  
  private void b()
  {
    try
    {
      boolean bool1 = b;
      if (!bool1)
      {
        bool1 = Thread.holdsLock(this);
        if (!bool1)
        {
          localObject1 = new java/lang/AssertionError;
          ((AssertionError)localObject1).<init>();
          throw ((Throwable)localObject1);
        }
      }
      bool1 = p;
      if (bool1) {
        return;
      }
      Object localObject1 = c;
      File localFile = g;
      bool1 = ((a)localObject1).e(localFile);
      Object localObject4;
      if (bool1)
      {
        localObject1 = c;
        localFile = e;
        bool1 = ((a)localObject1).e(localFile);
        if (bool1)
        {
          localObject1 = c;
          localFile = g;
          ((a)localObject1).d(localFile);
        }
        else
        {
          localObject1 = c;
          localFile = g;
          localObject4 = e;
          ((a)localObject1).a(localFile, (File)localObject4);
        }
      }
      localObject1 = c;
      localFile = e;
      bool1 = ((a)localObject1).e(localFile);
      boolean bool2 = true;
      if (bool1) {
        try
        {
          c();
          e();
          p = bool2;
          return;
        }
        catch (IOException localIOException)
        {
          h.a();
          localObject4 = new java/lang/StringBuilder;
          Object localObject5 = "DiskLruCache ";
          ((StringBuilder)localObject4).<init>((String)localObject5);
          localObject5 = d;
          ((StringBuilder)localObject4).append(localObject5);
          localObject5 = " is corrupt: ";
          ((StringBuilder)localObject4).append((String)localObject5);
          Object localObject2 = localIOException.getMessage();
          ((StringBuilder)localObject4).append((String)localObject2);
          localObject2 = ", removing";
          ((StringBuilder)localObject4).append((String)localObject2);
          localObject2 = ((StringBuilder)localObject4).toString();
          h.a((String)localObject2);
          close();
          localObject2 = c;
          localObject4 = d;
          ((a)localObject2).g((File)localObject4);
          bool1 = false;
          localObject2 = null;
          q = false;
        }
      }
      f();
      p = bool2;
      return;
    }
    finally {}
  }
  
  private void c()
  {
    localObject1 = c;
    Object localObject2 = e;
    localObject1 = n.a(((a)localObject1).a((File)localObject2));
    for (;;)
    {
      try
      {
        localObject2 = ((e)localObject1).q();
        str1 = ((e)localObject1).q();
        localObject4 = ((e)localObject1).q();
        str2 = ((e)localObject1).q();
        str3 = ((e)localObject1).q();
        localObject5 = "libcore.io.DiskLruCache";
        boolean bool1 = ((String)localObject5).equals(localObject2);
        if (bool1)
        {
          localObject5 = "1";
          bool1 = ((String)localObject5).equals(str1);
          if (bool1)
          {
            i1 = h;
            localObject5 = Integer.toString(i1);
            boolean bool2 = ((String)localObject5).equals(localObject4);
            if (bool2)
            {
              int i2 = j;
              localObject4 = Integer.toString(i2);
              boolean bool3 = ((String)localObject4).equals(str2);
              if (bool3)
              {
                localObject4 = "";
                bool3 = ((String)localObject4).equals(str3);
                if (bool3)
                {
                  i3 = 0;
                  localObject2 = null;
                  i4 = 0;
                  str1 = null;
                }
              }
            }
          }
        }
      }
      finally
      {
        String str1;
        Object localObject4;
        String str2;
        String str3;
        Object localObject5;
        int i1;
        int i3;
        int i4;
        int i5;
        int i6;
        String str4;
        boolean bool4;
        j.a((Closeable)localObject1);
      }
      try
      {
        localObject4 = ((e)localObject1).q();
        i5 = 32;
        i6 = ((String)localObject4).indexOf(i5);
        i1 = -1;
        if (i6 != i1)
        {
          int i7 = i6 + 1;
          i5 = ((String)localObject4).indexOf(i5, i7);
          if (i5 == i1)
          {
            str4 = ((String)localObject4).substring(i7);
            int i8 = 6;
            if (i6 == i8)
            {
              localObject6 = "REMOVE";
              boolean bool7 = ((String)localObject4).startsWith((String)localObject6);
              if (bool7)
              {
                localObject4 = m;
                ((LinkedHashMap)localObject4).remove(str4);
                continue;
              }
            }
          }
          else
          {
            str4 = ((String)localObject4).substring(i7, i5);
          }
          Object localObject6 = m;
          localObject6 = ((LinkedHashMap)localObject6).get(str4);
          localObject6 = (b.b)localObject6;
          Object localObject7;
          if (localObject6 == null)
          {
            localObject6 = new com/d/a/a/b$b;
            ((b.b)localObject6).<init>(this, str4, (byte)0);
            localObject7 = m;
            ((LinkedHashMap)localObject7).put(str4, localObject6);
          }
          i7 = 5;
          if ((i5 != i1) && (i6 == i7))
          {
            localObject7 = "CLEAN";
            boolean bool8 = ((String)localObject4).startsWith((String)localObject7);
            if (bool8)
            {
              i5 += 1;
              localObject4 = ((String)localObject4).substring(i5);
              str2 = " ";
              localObject4 = ((String)localObject4).split(str2);
              i5 = 1;
              e = i5;
              i5 = 0;
              str2 = null;
              f = null;
              ((b.b)localObject6).a((String[])localObject4);
              continue;
            }
          }
          if ((i5 == i1) && (i6 == i7))
          {
            str4 = "DIRTY";
            boolean bool6 = ((String)localObject4).startsWith(str4);
            if (bool6)
            {
              localObject4 = new com/d/a/a/b$a;
              ((b.a)localObject4).<init>(this, (b.b)localObject6, (byte)0);
              f = ((b.a)localObject4);
              continue;
            }
          }
          if (i5 == i1)
          {
            i5 = 4;
            if (i6 == i5)
            {
              str2 = "READ";
              boolean bool5 = ((String)localObject4).startsWith(str2);
              if (bool5)
              {
                i4 += 1;
                continue;
              }
            }
          }
          localObject2 = new java/io/IOException;
          str2 = "unexpected journal line: ";
          localObject4 = String.valueOf(localObject4);
          localObject4 = str2.concat((String)localObject4);
          ((IOException)localObject2).<init>((String)localObject4);
          throw ((Throwable)localObject2);
        }
        else
        {
          localObject2 = new java/io/IOException;
          str2 = "unexpected journal line: ";
          localObject4 = String.valueOf(localObject4);
          localObject4 = str2.concat((String)localObject4);
          ((IOException)localObject2).<init>((String)localObject4);
          throw ((Throwable)localObject2);
        }
      }
      catch (EOFException localEOFException) {}
    }
    localObject2 = m;
    i3 = ((LinkedHashMap)localObject2).size();
    i4 -= i3;
    n = i4;
    bool4 = ((e)localObject1).e();
    if (!bool4)
    {
      f();
    }
    else
    {
      localObject2 = d();
      l = ((d)localObject2);
    }
    j.a((Closeable)localObject1);
    return;
    localObject4 = new java/io/IOException;
    localObject5 = new java/lang/StringBuilder;
    str4 = "unexpected journal header: [";
    ((StringBuilder)localObject5).<init>(str4);
    ((StringBuilder)localObject5).append((String)localObject2);
    localObject2 = ", ";
    ((StringBuilder)localObject5).append((String)localObject2);
    ((StringBuilder)localObject5).append(str1);
    localObject2 = ", ";
    ((StringBuilder)localObject5).append((String)localObject2);
    ((StringBuilder)localObject5).append(str2);
    localObject2 = ", ";
    ((StringBuilder)localObject5).append((String)localObject2);
    ((StringBuilder)localObject5).append(str3);
    localObject2 = "]";
    ((StringBuilder)localObject5).append((String)localObject2);
    localObject2 = ((StringBuilder)localObject5).toString();
    ((IOException)localObject4).<init>((String)localObject2);
    throw ((Throwable)localObject4);
  }
  
  private static void c(String paramString)
  {
    Object localObject = a.matcher(paramString);
    boolean bool = ((Matcher)localObject).matches();
    if (bool) {
      return;
    }
    localObject = new java/lang/IllegalArgumentException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("keys must match regex [a-z0-9_-]{1,120}: \"");
    localStringBuilder.append(paramString);
    localStringBuilder.append("\"");
    paramString = localStringBuilder.toString();
    ((IllegalArgumentException)localObject).<init>(paramString);
    throw ((Throwable)localObject);
  }
  
  private d d()
  {
    Object localObject1 = c;
    Object localObject2 = e;
    localObject1 = ((a)localObject1).c((File)localObject2);
    localObject2 = new com/d/a/a/b$2;
    ((b.2)localObject2).<init>(this, (t)localObject1);
    return n.a((t)localObject2);
  }
  
  private void e()
  {
    Object localObject1 = c;
    Object localObject2 = f;
    ((a)localObject1).d((File)localObject2);
    localObject1 = m.values().iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (b.b)((Iterator)localObject1).next();
      Object localObject3 = f;
      int i1 = 0;
      if (localObject3 == null) {
        for (;;)
        {
          i2 = j;
          if (i1 >= i2) {
            break;
          }
          long l1 = k;
          localObject3 = b;
          long l2 = localObject3[i1];
          l1 += l2;
          k = l1;
          i1 += 1;
        }
      }
      int i2 = 0;
      localObject3 = null;
      f = null;
      for (;;)
      {
        i2 = j;
        if (i1 >= i2) {
          break;
        }
        localObject3 = c;
        File localFile = c[i1];
        ((a)localObject3).d(localFile);
        localObject3 = c;
        localFile = d[i1];
        ((a)localObject3).d(localFile);
        i1 += 1;
      }
      ((Iterator)localObject1).remove();
    }
  }
  
  /* Error */
  private void f()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 214	com/d/a/a/b:l	Ld/d;
    //   6: astore_1
    //   7: aload_1
    //   8: ifnull +14 -> 22
    //   11: aload_0
    //   12: getfield 214	com/d/a/a/b:l	Ld/d;
    //   15: astore_1
    //   16: aload_1
    //   17: invokeinterface 461 1 0
    //   22: aload_0
    //   23: getfield 88	com/d/a/a/b:c	Lcom/d/a/a/c/a;
    //   26: astore_1
    //   27: aload_0
    //   28: getfield 107	com/d/a/a/b:f	Ljava/io/File;
    //   31: astore_2
    //   32: aload_1
    //   33: aload_2
    //   34: invokeinterface 463 2 0
    //   39: astore_1
    //   40: aload_1
    //   41: invokestatic 439	d/n:a	(Ld/t;)Ld/d;
    //   44: astore_1
    //   45: ldc_w 330
    //   48: astore_2
    //   49: aload_1
    //   50: aload_2
    //   51: invokeinterface 221 2 0
    //   56: astore_2
    //   57: bipush 10
    //   59: istore_3
    //   60: aload_2
    //   61: iload_3
    //   62: invokeinterface 224 2 0
    //   67: pop
    //   68: ldc_w 335
    //   71: astore_2
    //   72: aload_1
    //   73: aload_2
    //   74: invokeinterface 221 2 0
    //   79: astore_2
    //   80: aload_2
    //   81: iload_3
    //   82: invokeinterface 224 2 0
    //   87: pop
    //   88: aload_0
    //   89: getfield 94	com/d/a/a/b:h	I
    //   92: istore 4
    //   94: iload 4
    //   96: i2l
    //   97: lstore 5
    //   99: aload_1
    //   100: lload 5
    //   102: invokeinterface 466 3 0
    //   107: astore_2
    //   108: aload_2
    //   109: iload_3
    //   110: invokeinterface 224 2 0
    //   115: pop
    //   116: aload_0
    //   117: getfield 114	com/d/a/a/b:j	I
    //   120: istore 4
    //   122: iload 4
    //   124: i2l
    //   125: lstore 5
    //   127: aload_1
    //   128: lload 5
    //   130: invokeinterface 466 3 0
    //   135: astore_2
    //   136: aload_2
    //   137: iload_3
    //   138: invokeinterface 224 2 0
    //   143: pop
    //   144: aload_1
    //   145: iload_3
    //   146: invokeinterface 224 2 0
    //   151: pop
    //   152: aload_0
    //   153: getfield 77	com/d/a/a/b:m	Ljava/util/LinkedHashMap;
    //   156: astore_2
    //   157: aload_2
    //   158: invokevirtual 443	java/util/LinkedHashMap:values	()Ljava/util/Collection;
    //   161: astore_2
    //   162: aload_2
    //   163: invokeinterface 449 1 0
    //   168: astore_2
    //   169: aload_2
    //   170: invokeinterface 454 1 0
    //   175: istore 7
    //   177: iload 7
    //   179: ifeq +143 -> 322
    //   182: aload_2
    //   183: invokeinterface 458 1 0
    //   188: astore 8
    //   190: aload 8
    //   192: checkcast 161	com/d/a/a/b$b
    //   195: astore 8
    //   197: aload 8
    //   199: getfield 164	com/d/a/a/b$b:f	Lcom/d/a/a/b$a;
    //   202: astore 9
    //   204: bipush 32
    //   206: istore 10
    //   208: aload 9
    //   210: ifnull +55 -> 265
    //   213: ldc_w 381
    //   216: astore 9
    //   218: aload_1
    //   219: aload 9
    //   221: invokeinterface 221 2 0
    //   226: astore 9
    //   228: aload 9
    //   230: iload 10
    //   232: invokeinterface 224 2 0
    //   237: pop
    //   238: aload 8
    //   240: getfield 227	com/d/a/a/b$b:a	Ljava/lang/String;
    //   243: astore 8
    //   245: aload_1
    //   246: aload 8
    //   248: invokeinterface 221 2 0
    //   253: pop
    //   254: aload_1
    //   255: iload_3
    //   256: invokeinterface 224 2 0
    //   261: pop
    //   262: goto -93 -> 169
    //   265: ldc -40
    //   267: astore 9
    //   269: aload_1
    //   270: aload 9
    //   272: invokeinterface 221 2 0
    //   277: astore 9
    //   279: aload 9
    //   281: iload 10
    //   283: invokeinterface 224 2 0
    //   288: pop
    //   289: aload 8
    //   291: getfield 227	com/d/a/a/b$b:a	Ljava/lang/String;
    //   294: astore 9
    //   296: aload_1
    //   297: aload 9
    //   299: invokeinterface 221 2 0
    //   304: pop
    //   305: aload 8
    //   307: aload_1
    //   308: invokevirtual 230	com/d/a/a/b$b:a	(Ld/d;)V
    //   311: aload_1
    //   312: iload_3
    //   313: invokeinterface 224 2 0
    //   318: pop
    //   319: goto -150 -> 169
    //   322: aload_1
    //   323: invokeinterface 461 1 0
    //   328: aload_0
    //   329: getfield 88	com/d/a/a/b:c	Lcom/d/a/a/c/a;
    //   332: astore_1
    //   333: aload_0
    //   334: getfield 103	com/d/a/a/b:e	Ljava/io/File;
    //   337: astore_2
    //   338: aload_1
    //   339: aload_2
    //   340: invokeinterface 177 2 0
    //   345: istore 11
    //   347: iload 11
    //   349: ifeq +28 -> 377
    //   352: aload_0
    //   353: getfield 88	com/d/a/a/b:c	Lcom/d/a/a/c/a;
    //   356: astore_1
    //   357: aload_0
    //   358: getfield 103	com/d/a/a/b:e	Ljava/io/File;
    //   361: astore_2
    //   362: aload_0
    //   363: getfield 111	com/d/a/a/b:g	Ljava/io/File;
    //   366: astore 12
    //   368: aload_1
    //   369: aload_2
    //   370: aload 12
    //   372: invokeinterface 199 3 0
    //   377: aload_0
    //   378: getfield 88	com/d/a/a/b:c	Lcom/d/a/a/c/a;
    //   381: astore_1
    //   382: aload_0
    //   383: getfield 107	com/d/a/a/b:f	Ljava/io/File;
    //   386: astore_2
    //   387: aload_0
    //   388: getfield 103	com/d/a/a/b:e	Ljava/io/File;
    //   391: astore 12
    //   393: aload_1
    //   394: aload_2
    //   395: aload 12
    //   397: invokeinterface 199 3 0
    //   402: aload_0
    //   403: getfield 88	com/d/a/a/b:c	Lcom/d/a/a/c/a;
    //   406: astore_1
    //   407: aload_0
    //   408: getfield 111	com/d/a/a/b:g	Ljava/io/File;
    //   411: astore_2
    //   412: aload_1
    //   413: aload_2
    //   414: invokeinterface 208 2 0
    //   419: aload_0
    //   420: invokespecial 402	com/d/a/a/b:d	()Ld/d;
    //   423: astore_1
    //   424: aload_0
    //   425: aload_1
    //   426: putfield 214	com/d/a/a/b:l	Ld/d;
    //   429: iconst_0
    //   430: istore 11
    //   432: aconst_null
    //   433: astore_1
    //   434: aload_0
    //   435: iconst_0
    //   436: putfield 468	com/d/a/a/b:o	Z
    //   439: aload_0
    //   440: monitorexit
    //   441: return
    //   442: astore_2
    //   443: aload_1
    //   444: invokeinterface 461 1 0
    //   449: aload_2
    //   450: athrow
    //   451: astore_1
    //   452: aload_0
    //   453: monitorexit
    //   454: aload_1
    //   455: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	456	0	this	b
    //   6	438	1	localObject1	Object
    //   451	4	1	localObject2	Object
    //   31	383	2	localObject3	Object
    //   442	8	2	localObject4	Object
    //   59	254	3	i1	int
    //   92	31	4	i2	int
    //   97	32	5	l1	long
    //   175	3	7	bool1	boolean
    //   188	118	8	localObject5	Object
    //   202	96	9	localObject6	Object
    //   206	76	10	i3	int
    //   345	86	11	bool2	boolean
    //   366	30	12	localFile	File
    // Exception table:
    //   from	to	target	type
    //   50	56	442	finally
    //   61	68	442	finally
    //   73	79	442	finally
    //   81	88	442	finally
    //   88	92	442	finally
    //   100	107	442	finally
    //   109	116	442	finally
    //   116	120	442	finally
    //   128	135	442	finally
    //   137	144	442	finally
    //   145	152	442	finally
    //   152	156	442	finally
    //   157	161	442	finally
    //   162	168	442	finally
    //   169	175	442	finally
    //   182	188	442	finally
    //   190	195	442	finally
    //   197	202	442	finally
    //   219	226	442	finally
    //   230	238	442	finally
    //   238	243	442	finally
    //   246	254	442	finally
    //   255	262	442	finally
    //   270	277	442	finally
    //   281	289	442	finally
    //   289	294	442	finally
    //   297	305	442	finally
    //   307	311	442	finally
    //   312	319	442	finally
    //   2	6	451	finally
    //   11	15	451	finally
    //   16	22	451	finally
    //   22	26	451	finally
    //   27	31	451	finally
    //   33	39	451	finally
    //   40	44	451	finally
    //   322	328	451	finally
    //   328	332	451	finally
    //   333	337	451	finally
    //   339	345	451	finally
    //   352	356	451	finally
    //   357	361	451	finally
    //   362	366	451	finally
    //   370	377	451	finally
    //   377	381	451	finally
    //   382	386	451	finally
    //   387	391	451	finally
    //   395	402	451	finally
    //   402	406	451	finally
    //   407	411	451	finally
    //   413	419	451	finally
    //   419	423	451	finally
    //   425	429	451	finally
    //   435	439	451	finally
    //   443	449	451	finally
    //   449	451	451	finally
  }
  
  private boolean g()
  {
    int i1 = n;
    int i2 = 2000;
    if (i1 >= i2)
    {
      LinkedHashMap localLinkedHashMap = m;
      i2 = localLinkedHashMap.size();
      if (i1 >= i2) {
        return true;
      }
    }
    return false;
  }
  
  private boolean h()
  {
    try
    {
      boolean bool = q;
      return bool;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  private void i()
  {
    try
    {
      boolean bool = h();
      if (!bool) {
        return;
      }
      IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
      String str = "cache is closed";
      localIllegalStateException.<init>(str);
      throw localIllegalStateException;
    }
    finally {}
  }
  
  private void j()
  {
    for (;;)
    {
      long l1 = k;
      long l2 = i;
      boolean bool = l1 < l2;
      if (!bool) {
        break;
      }
      b.b localb = (b.b)m.values().iterator().next();
      a(localb);
    }
  }
  
  public final b.a a(String paramString, long paramLong)
  {
    try
    {
      b();
      i();
      c(paramString);
      Object localObject1 = m;
      localObject1 = ((LinkedHashMap)localObject1).get(paramString);
      localObject1 = (b.b)localObject1;
      long l1 = -1;
      boolean bool1 = paramLong < l1;
      if (bool1) {
        if (localObject1 != null)
        {
          l1 = g;
          bool1 = l1 < paramLong;
          if (!bool1) {}
        }
        else
        {
          return null;
        }
      }
      if (localObject1 != null)
      {
        localObject2 = f;
        if (localObject2 != null) {
          return null;
        }
      }
      Object localObject2 = l;
      Object localObject3 = "DIRTY";
      localObject2 = ((d)localObject2).b((String)localObject3);
      int i1 = 32;
      localObject2 = ((d)localObject2).j(i1);
      localObject2 = ((d)localObject2).b(paramString);
      i1 = 10;
      ((d)localObject2).j(i1);
      localObject2 = l;
      ((d)localObject2).flush();
      boolean bool2 = o;
      if (bool2) {
        return null;
      }
      bool2 = false;
      localObject2 = null;
      if (localObject1 == null)
      {
        localObject1 = new com/d/a/a/b$b;
        ((b.b)localObject1).<init>(this, paramString, (byte)0);
        localObject3 = m;
        ((LinkedHashMap)localObject3).put(paramString, localObject1);
      }
      paramString = new com/d/a/a/b$a;
      paramString.<init>(this, (b.b)localObject1, (byte)0);
      f = paramString;
      return paramString;
    }
    finally {}
  }
  
  public final b.c a(String paramString)
  {
    try
    {
      b();
      i();
      c(paramString);
      Object localObject1 = m;
      localObject1 = ((LinkedHashMap)localObject1).get(paramString);
      localObject1 = (b.b)localObject1;
      int i1 = 0;
      Object localObject2 = null;
      if (localObject1 != null)
      {
        boolean bool1 = e;
        if (bool1)
        {
          localObject1 = ((b.b)localObject1).a();
          if (localObject1 == null) {
            return null;
          }
          i1 = n + 1;
          n = i1;
          localObject2 = l;
          String str = "READ";
          localObject2 = ((d)localObject2).b(str);
          int i2 = 32;
          localObject2 = ((d)localObject2).j(i2);
          paramString = ((d)localObject2).b(paramString);
          i1 = 10;
          paramString.j(i1);
          boolean bool2 = g();
          if (bool2)
          {
            paramString = s;
            localObject2 = t;
            paramString.execute((Runnable)localObject2);
          }
          return (b.c)localObject1;
        }
      }
      return null;
    }
    finally {}
  }
  
  public final boolean b(String paramString)
  {
    try
    {
      b();
      i();
      c(paramString);
      LinkedHashMap localLinkedHashMap = m;
      paramString = localLinkedHashMap.get(paramString);
      paramString = (b.b)paramString;
      if (paramString == null) {
        return false;
      }
      boolean bool = a(paramString);
      return bool;
    }
    finally {}
  }
  
  public final void close()
  {
    try
    {
      boolean bool1 = p;
      boolean bool2 = true;
      if (bool1)
      {
        bool1 = q;
        if (!bool1)
        {
          Object localObject1 = m;
          localObject1 = ((LinkedHashMap)localObject1).values();
          Object localObject3 = m;
          int i1 = ((LinkedHashMap)localObject3).size();
          localObject3 = new b.b[i1];
          localObject1 = ((Collection)localObject1).toArray((Object[])localObject3);
          localObject1 = (b.b[])localObject1;
          i1 = localObject1.length;
          int i2 = 0;
          while (i2 < i1)
          {
            b.a locala1 = localObject1[i2];
            b.a locala2 = f;
            if (locala2 != null)
            {
              locala1 = f;
              locala1.b();
            }
            i2 += 1;
          }
          j();
          localObject1 = l;
          ((d)localObject1).close();
          bool1 = false;
          localObject1 = null;
          l = null;
          q = bool2;
          return;
        }
      }
      q = bool2;
      return;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */