package com.d.a.a;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

final class g
{
  private final Class a;
  private final String b;
  private final Class[] c;
  
  public g(Class paramClass, String paramString, Class... paramVarArgs)
  {
    a = paramClass;
    b = paramString;
    c = paramVarArgs;
  }
  
  private Method a(Class paramClass)
  {
    Object localObject1 = b;
    Class localClass = null;
    if (localObject1 != null)
    {
      Object localObject2 = c;
      paramClass = a(paramClass, (String)localObject1, (Class[])localObject2);
      if (paramClass != null)
      {
        localObject1 = a;
        if (localObject1 != null)
        {
          localObject2 = paramClass.getReturnType();
          boolean bool = ((Class)localObject1).isAssignableFrom((Class)localObject2);
          if (!bool) {
            return localClass;
          }
        }
      }
      localClass = paramClass;
    }
    return localClass;
  }
  
  private static Method a(Class paramClass, String paramString, Class[] paramArrayOfClass)
  {
    try
    {
      paramClass = paramClass.getMethod(paramString, paramArrayOfClass);
      int i;
      return paramClass;
    }
    catch (NoSuchMethodException localNoSuchMethodException1)
    {
      try
      {
        i = paramClass.getModifiers() & 0x1;
        if (i != 0) {
          break label26;
        }
        paramClass = null;
      }
      catch (NoSuchMethodException localNoSuchMethodException2)
      {
        for (;;) {}
      }
      localNoSuchMethodException1;
      paramClass = null;
    }
  }
  
  private Object c(Object paramObject, Object... paramVarArgs)
  {
    Object localObject = paramObject.getClass();
    localObject = a((Class)localObject);
    if (localObject == null) {
      return null;
    }
    try
    {
      return ((Method)localObject).invoke(paramObject, paramVarArgs);
    }
    catch (IllegalAccessException localIllegalAccessException) {}
    return null;
  }
  
  private Object d(Object paramObject, Object... paramVarArgs)
  {
    Object localObject = paramObject.getClass();
    localObject = a((Class)localObject);
    if (localObject != null) {
      try
      {
        return ((Method)localObject).invoke(paramObject, paramVarArgs);
      }
      catch (IllegalAccessException paramObject)
      {
        paramVarArgs = new java/lang/AssertionError;
        localObject = String.valueOf(localObject);
        localObject = "Unexpectedly could not call: ".concat((String)localObject);
        paramVarArgs.<init>(localObject);
        paramVarArgs.initCause((Throwable)paramObject);
        throw paramVarArgs;
      }
    }
    paramVarArgs = new java/lang/AssertionError;
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("Method ");
    String str = b;
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append(" not supported for object ");
    ((StringBuilder)localObject).append(paramObject);
    paramObject = ((StringBuilder)localObject).toString();
    paramVarArgs.<init>(paramObject);
    throw paramVarArgs;
  }
  
  public final Object a(Object paramObject, Object... paramVarArgs)
  {
    try
    {
      return c(paramObject, paramVarArgs);
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      paramObject = localInvocationTargetException.getTargetException();
      boolean bool = paramObject instanceof RuntimeException;
      if (bool) {
        throw ((RuntimeException)paramObject);
      }
      paramVarArgs = new java/lang/AssertionError;
      paramVarArgs.<init>("Unexpected exception");
      paramVarArgs.initCause((Throwable)paramObject);
      throw paramVarArgs;
    }
  }
  
  public final boolean a(Object paramObject)
  {
    paramObject = paramObject.getClass();
    paramObject = a((Class)paramObject);
    return paramObject != null;
  }
  
  public final Object b(Object paramObject, Object... paramVarArgs)
  {
    try
    {
      return d(paramObject, paramVarArgs);
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      paramObject = localInvocationTargetException.getTargetException();
      boolean bool = paramObject instanceof RuntimeException;
      if (bool) {
        throw ((RuntimeException)paramObject);
      }
      paramVarArgs = new java/lang/AssertionError;
      paramVarArgs.<init>("Unexpected exception");
      paramVarArgs.initCause((Throwable)paramObject);
      throw paramVarArgs;
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */