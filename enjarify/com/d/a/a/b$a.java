package com.d.a.a;

import com.d.a.a.c.a;
import d.t;
import java.io.File;
import java.io.FileNotFoundException;

public final class b$a
{
  final b.b a;
  final boolean[] b;
  boolean c;
  private boolean e;
  
  private b$a(b paramb, b.b paramb1)
  {
    a = paramb1;
    boolean bool = e;
    int i;
    if (bool)
    {
      i = 0;
      paramb = null;
    }
    else
    {
      i = b.h(paramb);
      paramb = new boolean[i];
    }
    b = paramb;
  }
  
  public final t a(int paramInt)
  {
    synchronized (d)
    {
      Object localObject1 = a;
      localObject1 = f;
      if (localObject1 == this)
      {
        localObject1 = a;
        boolean bool = e;
        if (!bool)
        {
          localObject1 = b;
          int i = 1;
          localObject1[paramInt] = i;
        }
        localObject1 = a;
        localObject1 = d;
        localObject2 = localObject1[paramInt];
        try
        {
          localObject1 = d;
          localObject1 = b.i((b)localObject1);
          localObject2 = ((a)localObject1).b((File)localObject2);
          localObject1 = new com/d/a/a/b$a$1;
          ((b.a.1)localObject1).<init>(this, (t)localObject2);
          return (t)localObject1;
        }
        catch (FileNotFoundException localFileNotFoundException)
        {
          localObject2 = b.a();
          return (t)localObject2;
        }
      }
      Object localObject2 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject2).<init>();
      throw ((Throwable)localObject2);
    }
  }
  
  public final void a()
  {
    synchronized (d)
    {
      boolean bool1 = c;
      boolean bool2 = true;
      b localb2;
      if (bool1)
      {
        localb2 = d;
        b.b localb = null;
        b.a(localb2, this, false);
        localb2 = d;
        localb = a;
        b.a(localb2, localb);
      }
      else
      {
        localb2 = d;
        b.a(localb2, this, bool2);
      }
      e = bool2;
      return;
    }
  }
  
  public final void b()
  {
    synchronized (d)
    {
      b localb2 = d;
      b.a(localb2, this, false);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */