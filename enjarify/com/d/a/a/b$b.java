package com.d.a.a;

import com.d.a.a.c.a;
import d.d;
import d.u;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

final class b$b
{
  final String a;
  final long[] b;
  final File[] c;
  final File[] d;
  boolean e;
  b.a f;
  long g;
  
  private b$b(b paramb, String paramString)
  {
    a = paramString;
    Object localObject = new long[b.h(paramb)];
    b = ((long[])localObject);
    localObject = new File[b.h(paramb)];
    c = ((File[])localObject);
    int i = b.h(paramb);
    localObject = new File[i];
    d = ((File[])localObject);
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>(paramString);
    ((StringBuilder)localObject).append('.');
    int j = ((StringBuilder)localObject).length();
    int k = 0;
    for (;;)
    {
      int m = b.h(paramb);
      if (k >= m) {
        break;
      }
      ((StringBuilder)localObject).append(k);
      File[] arrayOfFile = c;
      File localFile1 = new java/io/File;
      File localFile2 = b.j(paramb);
      String str = ((StringBuilder)localObject).toString();
      localFile1.<init>(localFile2, str);
      arrayOfFile[k] = localFile1;
      ((StringBuilder)localObject).append(".tmp");
      arrayOfFile = d;
      localFile1 = new java/io/File;
      localFile2 = b.j(paramb);
      str = ((StringBuilder)localObject).toString();
      localFile1.<init>(localFile2, str);
      arrayOfFile[k] = localFile1;
      ((StringBuilder)localObject).setLength(j);
      k += 1;
    }
  }
  
  private static IOException b(String[] paramArrayOfString)
  {
    IOException localIOException = new java/io/IOException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("unexpected journal line: ");
    paramArrayOfString = Arrays.toString(paramArrayOfString);
    localStringBuilder.append(paramArrayOfString);
    paramArrayOfString = localStringBuilder.toString();
    localIOException.<init>(paramArrayOfString);
    throw localIOException;
  }
  
  final b.c a()
  {
    Object localObject1 = h;
    boolean bool = Thread.holdsLock(localObject1);
    if (bool)
    {
      int i = b.h(h);
      localObject1 = new u[i];
      Object localObject2 = b.clone();
      Object localObject3 = localObject2;
      localObject3 = (long[])localObject2;
      int j = 0;
      int k = 0;
      localObject2 = null;
      try
      {
        for (;;)
        {
          localObject4 = h;
          int m = b.h((b)localObject4);
          if (k >= m) {
            break;
          }
          localObject4 = h;
          localObject4 = b.i((b)localObject4);
          localObject5 = c;
          localObject5 = localObject5[k];
          localObject4 = ((a)localObject4).a((File)localObject5);
          localObject1[k] = localObject4;
          k += 1;
        }
        b.c localc = new com/d/a/a/b$c;
        Object localObject4 = h;
        Object localObject5 = a;
        long l = g;
        localObject2 = localc;
        localc.<init>((b)localObject4, (String)localObject5, l, (u[])localObject1, (long[])localObject3, (byte)0);
        return localc;
      }
      catch (FileNotFoundException localFileNotFoundException)
      {
        for (;;)
        {
          localObject2 = h;
          k = b.h((b)localObject2);
          if (j >= k) {
            break;
          }
          localObject2 = localObject1[j];
          if (localObject2 == null) {
            break;
          }
          localObject2 = localObject1[j];
          j.a((Closeable)localObject2);
          j += 1;
        }
        return null;
      }
    }
    localObject1 = new java/lang/AssertionError;
    ((AssertionError)localObject1).<init>();
    throw ((Throwable)localObject1);
  }
  
  final void a(d paramd)
  {
    long[] arrayOfLong = b;
    int i = arrayOfLong.length;
    int j = 0;
    while (j < i)
    {
      long l = arrayOfLong[j];
      int k = 32;
      d locald = paramd.j(k);
      locald.l(l);
      j += 1;
    }
  }
  
  final void a(String[] paramArrayOfString)
  {
    int i = paramArrayOfString.length;
    Object localObject = h;
    int j = b.h((b)localObject);
    if (i == j)
    {
      i = 0;
      try
      {
        for (;;)
        {
          j = paramArrayOfString.length;
          if (i >= j) {
            break;
          }
          localObject = b;
          String str = paramArrayOfString[i];
          long l = Long.parseLong(str);
          localObject[i] = l;
          i += 1;
        }
        return;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        throw b(paramArrayOfString);
      }
    }
    throw b(paramArrayOfString);
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */