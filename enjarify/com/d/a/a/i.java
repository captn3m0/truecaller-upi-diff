package com.d.a.a;

import com.d.a.z;
import java.util.LinkedHashSet;
import java.util.Set;

public final class i
{
  private final Set a;
  
  public i()
  {
    LinkedHashSet localLinkedHashSet = new java/util/LinkedHashSet;
    localLinkedHashSet.<init>();
    a = localLinkedHashSet;
  }
  
  public final void a(z paramz)
  {
    try
    {
      Set localSet = a;
      localSet.add(paramz);
      return;
    }
    finally {}
  }
  
  public final void b(z paramz)
  {
    try
    {
      Set localSet = a;
      localSet.remove(paramz);
      return;
    }
    finally {}
  }
  
  public final boolean c(z paramz)
  {
    try
    {
      Set localSet = a;
      boolean bool = localSet.contains(paramz);
      return bool;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */