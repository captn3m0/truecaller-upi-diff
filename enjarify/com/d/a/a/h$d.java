package com.d.a.a;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.List;

final class h$d
  implements InvocationHandler
{
  private final List a;
  private boolean b;
  private String c;
  
  public h$d(List paramList)
  {
    a = paramList;
  }
  
  public final Object invoke(Object paramObject, Method paramMethod, Object[] paramArrayOfObject)
  {
    paramObject = paramMethod.getName();
    Object localObject1 = paramMethod.getReturnType();
    if (paramArrayOfObject == null) {
      paramArrayOfObject = j.b;
    }
    Object localObject2 = "supports";
    boolean bool1 = ((String)paramObject).equals(localObject2);
    if (bool1)
    {
      localObject2 = Boolean.TYPE;
      if (localObject2 == localObject1) {
        return Boolean.TRUE;
      }
    }
    localObject2 = "unsupported";
    bool1 = ((String)paramObject).equals(localObject2);
    int j = 1;
    if (bool1)
    {
      localObject2 = Void.TYPE;
      if (localObject2 == localObject1)
      {
        b = j;
        return null;
      }
    }
    localObject2 = "protocols";
    bool1 = ((String)paramObject).equals(localObject2);
    if (bool1)
    {
      int i = paramArrayOfObject.length;
      if (i == 0) {
        return a;
      }
    }
    localObject2 = "selectProtocol";
    boolean bool2 = ((String)paramObject).equals(localObject2);
    if (!bool2)
    {
      localObject2 = "select";
      bool2 = ((String)paramObject).equals(localObject2);
      if (!bool2) {}
    }
    else
    {
      localObject2 = String.class;
      if (localObject2 == localObject1)
      {
        int k = paramArrayOfObject.length;
        if (k == j)
        {
          localObject1 = paramArrayOfObject[0];
          bool3 = localObject1 instanceof List;
          if (bool3)
          {
            paramObject = (List)paramArrayOfObject[0];
            int m = ((List)paramObject).size();
            int n = 0;
            paramArrayOfObject = null;
            while (n < m)
            {
              localObject1 = a;
              localObject2 = ((List)paramObject).get(n);
              bool3 = ((List)localObject1).contains(localObject2);
              if (bool3)
              {
                paramObject = (String)((List)paramObject).get(n);
                c = ((String)paramObject);
                return paramObject;
              }
              n += 1;
            }
            paramObject = (String)a.get(0);
            c = ((String)paramObject);
            return paramObject;
          }
        }
      }
    }
    localObject1 = "protocolSelected";
    boolean bool3 = ((String)paramObject).equals(localObject1);
    if (!bool3)
    {
      localObject1 = "selected";
      boolean bool4 = ((String)paramObject).equals(localObject1);
      if (!bool4) {}
    }
    else
    {
      int i1 = paramArrayOfObject.length;
      if (i1 == j)
      {
        paramObject = (String)paramArrayOfObject[0];
        c = ((String)paramObject);
        return null;
      }
    }
    return paramMethod.invoke(this, paramArrayOfObject);
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.h.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */