package com.d.a.a;

import com.d.a.q;
import d.f;
import d.u;
import java.io.Closeable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.Socket;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public final class j
{
  public static final byte[] a = new byte[0];
  public static final String[] b = new String[0];
  public static final Charset c = Charset.forName("UTF-8");
  
  public static f a(f paramf)
  {
    Object localObject = "SHA-1";
    try
    {
      localObject = MessageDigest.getInstance((String)localObject);
      paramf = paramf.i();
      paramf = ((MessageDigest)localObject).digest(paramf);
      return f.a(paramf);
    }
    catch (NoSuchAlgorithmException paramf)
    {
      localObject = new java/lang/AssertionError;
      ((AssertionError)localObject).<init>(paramf);
      throw ((Throwable)localObject);
    }
  }
  
  public static String a(q paramq)
  {
    int i = c;
    String str = a;
    int j = q.a(str);
    if (i != j)
    {
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      str = b;
      localStringBuilder.append(str);
      localStringBuilder.append(":");
      int k = c;
      localStringBuilder.append(k);
      return localStringBuilder.toString();
    }
    return b;
  }
  
  public static String a(String paramString)
  {
    Object localObject = "MD5";
    try
    {
      localObject = MessageDigest.getInstance((String)localObject);
      String str = "UTF-8";
      paramString = paramString.getBytes(str);
      paramString = ((MessageDigest)localObject).digest(paramString);
      paramString = f.a(paramString);
      return paramString.f();
    }
    catch (UnsupportedEncodingException paramString) {}catch (NoSuchAlgorithmException paramString) {}
    localObject = new java/lang/AssertionError;
    ((AssertionError)localObject).<init>(paramString);
    throw ((Throwable)localObject);
  }
  
  public static List a(List paramList)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>(paramList);
    return Collections.unmodifiableList(localArrayList);
  }
  
  public static List a(Object... paramVarArgs)
  {
    return Collections.unmodifiableList(Arrays.asList((Object[])paramVarArgs.clone()));
  }
  
  public static Map a(Map paramMap)
  {
    LinkedHashMap localLinkedHashMap = new java/util/LinkedHashMap;
    localLinkedHashMap.<init>(paramMap);
    return Collections.unmodifiableMap(localLinkedHashMap);
  }
  
  public static void a(long paramLong1, long paramLong2)
  {
    long l1 = 0L;
    long l2 = paramLong2 | l1;
    boolean bool1 = l2 < l1;
    if (!bool1)
    {
      boolean bool2 = l1 < paramLong1;
      if (!bool2)
      {
        paramLong1 -= l1;
        boolean bool3 = paramLong1 < paramLong2;
        if (!bool3) {
          return;
        }
      }
    }
    ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException = new java/lang/ArrayIndexOutOfBoundsException;
    localArrayIndexOutOfBoundsException.<init>();
    throw localArrayIndexOutOfBoundsException;
  }
  
  public static void a(Closeable paramCloseable)
  {
    if (paramCloseable != null) {
      try
      {
        paramCloseable.close();
        return;
      }
      catch (RuntimeException localRuntimeException)
      {
        throw localRuntimeException;
      }
      catch (Exception localException) {}
    }
  }
  
  /* Error */
  public static void a(Closeable paramCloseable1, Closeable paramCloseable2)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokeinterface 137 1 0
    //   6: aconst_null
    //   7: astore_0
    //   8: goto +4 -> 12
    //   11: astore_0
    //   12: aload_1
    //   13: invokeinterface 137 1 0
    //   18: goto +10 -> 28
    //   21: astore_1
    //   22: aload_0
    //   23: ifnonnull +5 -> 28
    //   26: aload_1
    //   27: astore_0
    //   28: aload_0
    //   29: ifnonnull +4 -> 33
    //   32: return
    //   33: aload_0
    //   34: instanceof 143
    //   37: istore_2
    //   38: iload_2
    //   39: ifne +42 -> 81
    //   42: aload_0
    //   43: instanceof 139
    //   46: istore_2
    //   47: iload_2
    //   48: ifne +28 -> 76
    //   51: aload_0
    //   52: instanceof 145
    //   55: istore_2
    //   56: iload_2
    //   57: ifeq +8 -> 65
    //   60: aload_0
    //   61: checkcast 145	java/lang/Error
    //   64: athrow
    //   65: new 49	java/lang/AssertionError
    //   68: astore_1
    //   69: aload_1
    //   70: aload_0
    //   71: invokespecial 53	java/lang/AssertionError:<init>	(Ljava/lang/Object;)V
    //   74: aload_1
    //   75: athrow
    //   76: aload_0
    //   77: checkcast 139	java/lang/RuntimeException
    //   80: athrow
    //   81: aload_0
    //   82: checkcast 143	java/io/IOException
    //   85: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	86	0	paramCloseable1	Closeable
    //   0	86	1	paramCloseable2	Closeable
    //   37	20	2	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   0	6	11	finally
    //   12	18	21	finally
  }
  
  public static void a(Socket paramSocket)
  {
    if (paramSocket != null) {
      try
      {
        paramSocket.close();
        return;
      }
      catch (RuntimeException localRuntimeException)
      {
        throw localRuntimeException;
      }
      catch (AssertionError paramSocket)
      {
        boolean bool = a(paramSocket);
        if (bool) {
          return;
        }
        throw paramSocket;
      }
      catch (Exception localException) {}
    }
  }
  
  /* Error */
  public static boolean a(u paramu, int paramInt, TimeUnit paramTimeUnit)
  {
    // Byte code:
    //   0: invokestatic 157	java/lang/System:nanoTime	()J
    //   3: lstore_3
    //   4: aload_0
    //   5: invokeinterface 163 1 0
    //   10: astore 5
    //   12: aload 5
    //   14: invokevirtual 169	d/v:p_	()Z
    //   17: istore 6
    //   19: ldc2_w 170
    //   22: lstore 7
    //   24: iload 6
    //   26: ifeq +23 -> 49
    //   29: aload_0
    //   30: invokeinterface 163 1 0
    //   35: astore 5
    //   37: aload 5
    //   39: invokevirtual 173	d/v:c	()J
    //   42: lload_3
    //   43: lsub
    //   44: lstore 9
    //   46: goto +7 -> 53
    //   49: lload 7
    //   51: lstore 9
    //   53: aload_0
    //   54: invokeinterface 163 1 0
    //   59: astore 5
    //   61: iload_1
    //   62: i2l
    //   63: lstore 11
    //   65: aload_2
    //   66: lload 11
    //   68: invokevirtual 179	java/util/concurrent/TimeUnit:toNanos	(J)J
    //   71: lstore 13
    //   73: lload 9
    //   75: lload 13
    //   77: invokestatic 185	java/lang/Math:min	(JJ)J
    //   80: lload_3
    //   81: ladd
    //   82: lstore 13
    //   84: aload 5
    //   86: lload 13
    //   88: invokevirtual 188	d/v:a	(J)Ld/v;
    //   91: pop
    //   92: new 190	d/c
    //   95: astore 15
    //   97: aload 15
    //   99: invokespecial 191	d/c:<init>	()V
    //   102: ldc2_w 192
    //   105: lstore 11
    //   107: aload_0
    //   108: aload 15
    //   110: lload 11
    //   112: invokeinterface 198 4 0
    //   117: lstore 11
    //   119: iconst_m1
    //   120: i2l
    //   121: lstore 16
    //   123: lload 11
    //   125: lload 16
    //   127: lcmp
    //   128: istore 18
    //   130: iload 18
    //   132: ifeq +11 -> 143
    //   135: aload 15
    //   137: invokevirtual 201	d/c:s	()V
    //   140: goto -38 -> 102
    //   143: lload 9
    //   145: lload 7
    //   147: lcmp
    //   148: istore_1
    //   149: iload_1
    //   150: ifne +18 -> 168
    //   153: aload_0
    //   154: invokeinterface 163 1 0
    //   159: astore_0
    //   160: aload_0
    //   161: invokevirtual 204	d/v:q_	()Ld/v;
    //   164: pop
    //   165: goto +21 -> 186
    //   168: aload_0
    //   169: invokeinterface 163 1 0
    //   174: astore_0
    //   175: lload_3
    //   176: lload 9
    //   178: ladd
    //   179: lstore_3
    //   180: aload_0
    //   181: lload_3
    //   182: invokevirtual 188	d/v:a	(J)Ld/v;
    //   185: pop
    //   186: iconst_1
    //   187: ireturn
    //   188: astore 15
    //   190: lload 9
    //   192: lload 7
    //   194: lcmp
    //   195: istore 18
    //   197: iload 18
    //   199: ifne +18 -> 217
    //   202: aload_0
    //   203: invokeinterface 163 1 0
    //   208: astore_0
    //   209: aload_0
    //   210: invokevirtual 204	d/v:q_	()Ld/v;
    //   213: pop
    //   214: goto +21 -> 235
    //   217: aload_0
    //   218: invokeinterface 163 1 0
    //   223: astore_0
    //   224: lload_3
    //   225: lload 9
    //   227: ladd
    //   228: lstore_3
    //   229: aload_0
    //   230: lload_3
    //   231: invokevirtual 188	d/v:a	(J)Ld/v;
    //   234: pop
    //   235: aload 15
    //   237: athrow
    //   238: pop
    //   239: lload 9
    //   241: lload 7
    //   243: lcmp
    //   244: istore_1
    //   245: iload_1
    //   246: ifne +18 -> 264
    //   249: aload_0
    //   250: invokeinterface 163 1 0
    //   255: astore_0
    //   256: aload_0
    //   257: invokevirtual 204	d/v:q_	()Ld/v;
    //   260: pop
    //   261: goto +21 -> 282
    //   264: aload_0
    //   265: invokeinterface 163 1 0
    //   270: astore_0
    //   271: lload_3
    //   272: lload 9
    //   274: ladd
    //   275: lstore_3
    //   276: aload_0
    //   277: lload_3
    //   278: invokevirtual 188	d/v:a	(J)Ld/v;
    //   281: pop
    //   282: iconst_0
    //   283: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	284	0	paramu	u
    //   0	284	1	paramInt	int
    //   0	284	2	paramTimeUnit	TimeUnit
    //   3	275	3	l1	long
    //   10	75	5	localv	d.v
    //   17	8	6	bool1	boolean
    //   22	220	7	l2	long
    //   44	229	9	l3	long
    //   63	61	11	l4	long
    //   71	16	13	l5	long
    //   238	1	14	localInterruptedIOException	java.io.InterruptedIOException
    //   95	41	15	localc	d.c
    //   188	48	15	localObject	Object
    //   121	5	16	l6	long
    //   128	70	18	bool2	boolean
    // Exception table:
    //   from	to	target	type
    //   92	95	188	finally
    //   97	102	188	finally
    //   110	117	188	finally
    //   135	140	188	finally
    //   92	95	238	java/io/InterruptedIOException
    //   97	102	238	java/io/InterruptedIOException
    //   110	117	238	java/io/InterruptedIOException
    //   135	140	238	java/io/InterruptedIOException
  }
  
  public static boolean a(u paramu, TimeUnit paramTimeUnit)
  {
    int i = 100;
    try
    {
      return a(paramu, i, paramTimeUnit);
    }
    catch (IOException localIOException) {}
    return false;
  }
  
  public static boolean a(AssertionError paramAssertionError)
  {
    Object localObject = paramAssertionError.getCause();
    if (localObject != null)
    {
      localObject = paramAssertionError.getMessage();
      if (localObject != null)
      {
        paramAssertionError = paramAssertionError.getMessage();
        localObject = "getsockname failed";
        boolean bool = paramAssertionError.contains((CharSequence)localObject);
        if (bool) {
          return true;
        }
      }
    }
    return false;
  }
  
  public static boolean a(Object paramObject1, Object paramObject2)
  {
    if (paramObject1 != paramObject2) {
      if (paramObject1 != null)
      {
        boolean bool = paramObject1.equals(paramObject2);
        if (bool) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public static boolean a(String[] paramArrayOfString, String paramString)
  {
    return Arrays.asList(paramArrayOfString).contains(paramString);
  }
  
  public static Object[] a(Class paramClass, Object[] paramArrayOfObject1, Object[] paramArrayOfObject2)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    int i = paramArrayOfObject1.length;
    int j = 0;
    while (j < i)
    {
      Object localObject1 = paramArrayOfObject1[j];
      int k = paramArrayOfObject2.length;
      int m = 0;
      while (m < k)
      {
        Object localObject2 = paramArrayOfObject2[m];
        boolean bool = localObject1.equals(localObject2);
        if (bool)
        {
          localArrayList.add(localObject2);
          break;
        }
        m += 1;
      }
      j += 1;
    }
    int n = localArrayList.size();
    paramClass = (Object[])Array.newInstance(paramClass, n);
    return localArrayList.toArray(paramClass);
  }
  
  public static ThreadFactory b(String paramString)
  {
    j.1 local1 = new com/d/a/a/j$1;
    local1.<init>(paramString);
    return local1;
  }
  
  public static String[] b(String[] paramArrayOfString, String paramString)
  {
    String[] arrayOfString = new String[paramArrayOfString.length + 1];
    int i = paramArrayOfString.length;
    System.arraycopy(paramArrayOfString, 0, arrayOfString, 0, i);
    int j = arrayOfString.length + -1;
    arrayOfString[j] = paramString;
    return arrayOfString;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */