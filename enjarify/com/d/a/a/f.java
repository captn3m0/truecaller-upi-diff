package com.d.a.a;

public abstract class f
  implements Runnable
{
  protected final String a;
  
  public f(String paramString, Object... paramVarArgs)
  {
    paramString = String.format(paramString, paramVarArgs);
    a = paramString;
  }
  
  protected abstract void a();
  
  public final void run()
  {
    String str1 = Thread.currentThread().getName();
    Thread localThread = Thread.currentThread();
    String str2 = a;
    localThread.setName(str2);
    try
    {
      a();
      return;
    }
    finally
    {
      Thread.currentThread().setName(str1);
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */