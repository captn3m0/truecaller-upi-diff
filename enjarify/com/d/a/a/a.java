package com.d.a.a;

import com.d.a.k;
import java.net.UnknownServiceException;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLSocket;

public final class a
{
  public boolean a;
  public boolean b;
  private final List c;
  private int d = 0;
  
  public a(List paramList)
  {
    c = paramList;
  }
  
  private boolean b(SSLSocket paramSSLSocket)
  {
    int i = d;
    for (;;)
    {
      Object localObject = c;
      int j = ((List)localObject).size();
      if (i >= j) {
        break;
      }
      localObject = (k)c.get(i);
      boolean bool = ((k)localObject).a(paramSSLSocket);
      if (bool) {
        return true;
      }
      i += 1;
    }
    return false;
  }
  
  public final k a(SSLSocket paramSSLSocket)
  {
    int i = d;
    Object localObject1 = c;
    int j = ((List)localObject1).size();
    while (i < j)
    {
      localObject2 = (k)c.get(i);
      boolean bool3 = ((k)localObject2).a(paramSSLSocket);
      if (bool3)
      {
        i += 1;
        d = i;
        break label77;
      }
      i += 1;
    }
    boolean bool4 = false;
    Object localObject2 = null;
    label77:
    if (localObject2 != null)
    {
      boolean bool1 = b(paramSSLSocket);
      a = bool1;
      localObject3 = d.b;
      boolean bool2 = b;
      ((d)localObject3).a((k)localObject2, paramSSLSocket, bool2);
      return (k)localObject2;
    }
    Object localObject3 = new java/net/UnknownServiceException;
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("Unable to find acceptable protocols. isFallback=");
    bool4 = b;
    ((StringBuilder)localObject1).append(bool4);
    ((StringBuilder)localObject1).append(", modes=");
    localObject2 = c;
    ((StringBuilder)localObject1).append(localObject2);
    ((StringBuilder)localObject1).append(", supported protocols=");
    paramSSLSocket = Arrays.toString(paramSSLSocket.getEnabledProtocols());
    ((StringBuilder)localObject1).append(paramSSLSocket);
    paramSSLSocket = ((StringBuilder)localObject1).toString();
    ((UnknownServiceException)localObject3).<init>(paramSSLSocket);
    throw ((Throwable)localObject3);
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */