package com.d.a.a.b;

import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public final class g
{
  private static final TimeZone a = TimeZone.getTimeZone("GMT");
  private static final ThreadLocal b;
  private static final String[] c = tmp86_75;
  private static final DateFormat[] d = new DateFormat[15];
  
  static
  {
    g.1 local1 = new com/d/a/a/b/g$1;
    local1.<init>();
    b = local1;
    String[] tmp25_22 = new String[15];
    String[] tmp26_25 = tmp25_22;
    String[] tmp26_25 = tmp25_22;
    tmp26_25[0] = "EEE, dd MMM yyyy HH:mm:ss zzz";
    tmp26_25[1] = "EEEE, dd-MMM-yy HH:mm:ss zzz";
    String[] tmp35_26 = tmp26_25;
    String[] tmp35_26 = tmp26_25;
    tmp35_26[2] = "EEE MMM d HH:mm:ss yyyy";
    tmp35_26[3] = "EEE, dd-MMM-yyyy HH:mm:ss z";
    String[] tmp44_35 = tmp35_26;
    String[] tmp44_35 = tmp35_26;
    tmp44_35[4] = "EEE, dd-MMM-yyyy HH-mm-ss z";
    tmp44_35[5] = "EEE, dd MMM yy HH:mm:ss z";
    String[] tmp53_44 = tmp44_35;
    String[] tmp53_44 = tmp44_35;
    tmp53_44[6] = "EEE dd-MMM-yyyy HH:mm:ss z";
    tmp53_44[7] = "EEE dd MMM yyyy HH:mm:ss z";
    String[] tmp64_53 = tmp53_44;
    String[] tmp64_53 = tmp53_44;
    tmp64_53[8] = "EEE dd-MMM-yyyy HH-mm-ss z";
    tmp64_53[9] = "EEE dd-MMM-yy HH:mm:ss z";
    String[] tmp75_64 = tmp64_53;
    String[] tmp75_64 = tmp64_53;
    tmp75_64[10] = "EEE dd MMM yy HH:mm:ss z";
    tmp75_64[11] = "EEE,dd-MMM-yy HH:mm:ss z";
    String[] tmp86_75 = tmp75_64;
    String[] tmp86_75 = tmp75_64;
    tmp86_75[12] = "EEE,dd-MMM-yyyy HH:mm:ss z";
    tmp86_75[13] = "EEE, dd-MM-yyyy HH:mm:ss z";
    tmp86_75[14] = "EEE MMM d yyyy HH:mm:ss z";
  }
  
  public static Date a(String paramString)
  {
    int i = paramString.length();
    if (i == 0) {
      return null;
    }
    ParsePosition localParsePosition = new java/text/ParsePosition;
    localParsePosition.<init>(0);
    ??? = ((DateFormat)b.get()).parse(paramString, localParsePosition);
    int j = localParsePosition.getIndex();
    int k = paramString.length();
    if (j == k) {
      return (Date)???;
    }
    synchronized (c)
    {
      String[] arrayOfString = c;
      j = arrayOfString.length;
      k = 0;
      while (k < j)
      {
        Object localObject2 = d;
        localObject2 = localObject2[k];
        if (localObject2 == null)
        {
          localObject2 = new java/text/SimpleDateFormat;
          Object localObject3 = c;
          localObject3 = localObject3[k];
          Locale localLocale = Locale.US;
          ((SimpleDateFormat)localObject2).<init>((String)localObject3, localLocale);
          localObject3 = a;
          ((DateFormat)localObject2).setTimeZone((TimeZone)localObject3);
          localObject3 = d;
          localObject3[k] = localObject2;
        }
        localParsePosition.setIndex(0);
        localObject2 = ((DateFormat)localObject2).parse(paramString, localParsePosition);
        int m = localParsePosition.getIndex();
        if (m != 0) {
          return (Date)localObject2;
        }
        k += 1;
      }
      return null;
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */