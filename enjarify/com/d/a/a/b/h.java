package com.d.a.a.b;

import com.d.a.a;
import com.d.a.f;
import com.d.a.p;
import com.d.a.p.a;
import com.d.a.q;
import com.d.a.v;
import com.d.a.x;
import com.d.a.x.a;
import com.d.a.y;
import d.d;
import d.e;
import d.u;
import java.io.Closeable;
import java.net.CookieHandler;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.URI;
import java.util.Date;
import java.util.List;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;

public final class h
{
  public static final y a;
  public final com.d.a.t b;
  public final s c;
  public final x d;
  public j e;
  public long f;
  public boolean g;
  public final boolean h;
  public final v i;
  public v j;
  public x k;
  public x l;
  public d.t m;
  public d n;
  public final boolean o;
  public final boolean p;
  public b q;
  public c r;
  
  static
  {
    h.1 local1 = new com/d/a/a/b/h$1;
    local1.<init>();
    a = local1;
  }
  
  public h(com.d.a.t paramt, v paramv, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, s params, o paramo, x paramx)
  {
    long l1 = -1;
    f = l1;
    b = paramt;
    i = paramv;
    h = paramBoolean1;
    o = paramBoolean2;
    p = paramBoolean3;
    s locals;
    if (params != null)
    {
      locals = params;
    }
    else
    {
      locals = new com/d/a/a/b/s;
      com.d.a.j localj = q;
      Object localObject3 = a;
      boolean bool = ((q)localObject3).c();
      Object localObject4 = null;
      Object localObject6;
      Object localObject7;
      Object localObject8;
      if (bool)
      {
        localObject4 = m;
        localObject3 = n;
        localObject5 = o;
        localObject6 = localObject3;
        localObject7 = localObject4;
        localObject8 = localObject5;
      }
      else
      {
        localObject7 = null;
        localObject6 = null;
        localObject8 = null;
      }
      localObject3 = new com/d/a/a;
      String str = a.b;
      int i1 = a.c;
      com.d.a.n localn = r;
      SocketFactory localSocketFactory = l;
      localObject2 = p;
      localObject4 = d;
      Object localObject5 = e;
      Object localObject9 = f;
      localObject1 = i;
      Object localObject10 = localObject9;
      localObject9 = localObject3;
      ((a)localObject3).<init>(str, i1, localn, localSocketFactory, (SSLSocketFactory)localObject7, (HostnameVerifier)localObject6, (f)localObject8, (com.d.a.b)localObject2, (Proxy)localObject4, (List)localObject5, (List)localObject10, (ProxySelector)localObject1);
      locals.<init>(localj, (a)localObject3);
    }
    c = locals;
    localObject1 = paramo;
    m = paramo;
    localObject1 = paramx;
    d = paramx;
  }
  
  public static p a(p paramp1, p paramp2)
  {
    p.a locala = new com/d/a/p$a;
    locala.<init>();
    Object localObject = a;
    int i1 = localObject.length / 2;
    int i2 = 0;
    int i3 = 0;
    String str1 = null;
    while (i3 < i1)
    {
      String str2 = paramp1.a(i3);
      String str3 = paramp1.b(i3);
      String str4 = "Warning";
      boolean bool2 = str4.equalsIgnoreCase(str2);
      if (bool2)
      {
        str4 = "1";
        bool2 = str3.startsWith(str4);
        if (bool2) {}
      }
      else
      {
        bool2 = k.a(str2);
        if (bool2)
        {
          str4 = paramp2.a(str2);
          if (str4 != null) {}
        }
        else
        {
          locala.a(str2, str3);
        }
      }
      i3 += 1;
    }
    paramp1 = a;
    int i4 = paramp1.length / 2;
    while (i2 < i4)
    {
      localObject = paramp2.a(i2);
      str1 = "Content-Length";
      boolean bool1 = str1.equalsIgnoreCase((String)localObject);
      if (!bool1)
      {
        bool1 = k.a((String)localObject);
        if (bool1)
        {
          str1 = paramp2.b(i2);
          locala.a((String)localObject, str1);
        }
      }
      i2 += 1;
    }
    return locala.a();
  }
  
  public static x a(x paramx)
  {
    if (paramx != null)
    {
      y localy = g;
      if (localy != null)
      {
        paramx = paramx.d();
        localy = null;
        g = null;
        paramx = paramx.a();
      }
    }
    return paramx;
  }
  
  public static boolean a(v paramv)
  {
    return i.c(b);
  }
  
  public static boolean a(x paramx1, x paramx2)
  {
    int i1 = c;
    boolean bool2 = true;
    int i2 = 304;
    if (i1 == i2) {
      return bool2;
    }
    paramx1 = f;
    String str = "Last-Modified";
    paramx1 = paramx1.b(str);
    if (paramx1 != null)
    {
      paramx2 = f;
      str = "Last-Modified";
      paramx2 = paramx2.b(str);
      if (paramx2 != null)
      {
        long l1 = paramx2.getTime();
        long l2 = paramx1.getTime();
        boolean bool1 = l1 < l2;
        if (bool1) {
          return bool2;
        }
      }
    }
    return false;
  }
  
  public static boolean c(x paramx)
  {
    String str1 = a.b;
    boolean bool1 = str1.equals("HEAD");
    if (bool1) {
      return false;
    }
    int i1 = c;
    int i2 = 100;
    boolean bool3 = true;
    if (i1 >= i2)
    {
      i2 = 200;
      if (i1 < i2) {}
    }
    else
    {
      i2 = 204;
      if (i1 != i2)
      {
        i2 = 304;
        if (i1 != i2) {
          return bool3;
        }
      }
    }
    long l1 = k.a(paramx);
    long l2 = -1;
    boolean bool2 = l1 < l2;
    if (!bool2)
    {
      str1 = "chunked";
      String str2 = "Transfer-Encoding";
      paramx = paramx.a(str2);
      boolean bool4 = str1.equalsIgnoreCase(paramx);
      if (!bool4) {
        return false;
      }
    }
    return bool3;
  }
  
  public final void a()
  {
    long l1 = f;
    long l2 = -1;
    boolean bool = l1 < l2;
    if (!bool)
    {
      l1 = System.currentTimeMillis();
      f = l1;
      return;
    }
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>();
    throw localIllegalStateException;
  }
  
  public final void a(p paramp)
  {
    CookieHandler localCookieHandler = b.j;
    if (localCookieHandler != null)
    {
      URI localURI = i.a();
      paramp = k.b(paramp);
      localCookieHandler.put(localURI, paramp);
    }
  }
  
  public final boolean a(q paramq)
  {
    Object localObject = i.a;
    String str1 = b;
    String str2 = b;
    boolean bool1 = str1.equals(str2);
    if (bool1)
    {
      int i1 = c;
      int i2 = c;
      if (i1 == i2)
      {
        localObject = a;
        paramq = a;
        boolean bool2 = ((String)localObject).equals(paramq);
        if (bool2) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final s b()
  {
    Object localObject = n;
    if (localObject != null)
    {
      com.d.a.a.j.a((Closeable)localObject);
    }
    else
    {
      localObject = m;
      if (localObject != null) {
        com.d.a.a.j.a((Closeable)localObject);
      }
    }
    localObject = l;
    if (localObject != null)
    {
      localObject = g;
      com.d.a.a.j.a((Closeable)localObject);
    }
    else
    {
      localObject = c;
      boolean bool = true;
      ((s)localObject).a(bool, false, bool);
    }
    return c;
  }
  
  public final x b(x paramx)
  {
    boolean bool = g;
    if (bool)
    {
      Object localObject1 = "gzip";
      Object localObject2 = l;
      Object localObject3 = "Content-Encoding";
      localObject2 = ((x)localObject2).a((String)localObject3);
      bool = ((String)localObject1).equalsIgnoreCase((String)localObject2);
      if (bool)
      {
        localObject1 = g;
        if (localObject1 == null) {
          return paramx;
        }
        localObject1 = new d/l;
        localObject2 = g.b();
        ((d.l)localObject1).<init>((u)localObject2);
        localObject2 = f.a().b("Content-Encoding").b("Content-Length").a();
        paramx = paramx.d().a((p)localObject2);
        localObject3 = new com/d/a/a/b/l;
        localObject1 = d.n.a((u)localObject1);
        ((l)localObject3).<init>((p)localObject2, (e)localObject1);
        g = ((y)localObject3);
        return paramx.a();
      }
    }
    return paramx;
  }
  
  public final x c()
  {
    e.b();
    Object localObject1 = e.a();
    Object localObject2 = j;
    a = ((v)localObject2);
    localObject2 = c.a().c;
    e = ((com.d.a.o)localObject2);
    localObject2 = k.b;
    Object localObject3 = Long.toString(f);
    localObject1 = ((x.a)localObject1).a((String)localObject2, (String)localObject3);
    localObject2 = k.c;
    long l1 = System.currentTimeMillis();
    localObject3 = Long.toString(l1);
    localObject1 = ((x.a)localObject1).a((String)localObject2, (String)localObject3).a();
    boolean bool1 = p;
    if (!bool1)
    {
      localObject2 = ((x)localObject1).d();
      localObject3 = e;
      localObject1 = ((j)localObject3).a((x)localObject1);
      g = ((y)localObject1);
      localObject1 = ((x.a)localObject2).a();
    }
    localObject2 = "close";
    localObject3 = a;
    String str = "Connection";
    localObject3 = ((v)localObject3).a(str);
    bool1 = ((String)localObject2).equalsIgnoreCase((String)localObject3);
    if (!bool1)
    {
      localObject2 = "close";
      localObject3 = ((x)localObject1).a("Connection");
      bool1 = ((String)localObject2).equalsIgnoreCase((String)localObject3);
      if (!bool1) {}
    }
    else
    {
      localObject2 = c;
      boolean bool2 = true;
      str = null;
      ((s)localObject2).a(bool2, false, false);
    }
    return (x)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */