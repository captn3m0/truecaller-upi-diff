package com.d.a.a.b;

public final class i
{
  public static boolean a(String paramString)
  {
    String str = "POST";
    boolean bool1 = paramString.equals(str);
    if (!bool1)
    {
      str = "PATCH";
      bool1 = paramString.equals(str);
      if (!bool1)
      {
        str = "PUT";
        bool1 = paramString.equals(str);
        if (!bool1)
        {
          str = "DELETE";
          bool1 = paramString.equals(str);
          if (!bool1)
          {
            str = "MOVE";
            boolean bool2 = paramString.equals(str);
            if (!bool2) {
              return false;
            }
          }
        }
      }
    }
    return true;
  }
  
  public static boolean b(String paramString)
  {
    String str = "POST";
    boolean bool1 = paramString.equals(str);
    if (!bool1)
    {
      str = "PUT";
      bool1 = paramString.equals(str);
      if (!bool1)
      {
        str = "PATCH";
        bool1 = paramString.equals(str);
        if (!bool1)
        {
          str = "PROPPATCH";
          bool1 = paramString.equals(str);
          if (!bool1)
          {
            str = "REPORT";
            boolean bool2 = paramString.equals(str);
            if (!bool2) {
              return false;
            }
          }
        }
      }
    }
    return true;
  }
  
  public static boolean c(String paramString)
  {
    boolean bool1 = b(paramString);
    if (!bool1)
    {
      String str = "OPTIONS";
      bool1 = paramString.equals(str);
      if (!bool1)
      {
        str = "DELETE";
        bool1 = paramString.equals(str);
        if (!bool1)
        {
          str = "PROPFIND";
          bool1 = paramString.equals(str);
          if (!bool1)
          {
            str = "MKCOL";
            bool1 = paramString.equals(str);
            if (!bool1)
            {
              str = "LOCK";
              boolean bool2 = paramString.equals(str);
              if (!bool2) {
                return false;
              }
            }
          }
        }
      }
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */