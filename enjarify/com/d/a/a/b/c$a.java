package com.d.a.a.b;

import com.d.a.p;
import com.d.a.v;
import com.d.a.x;
import java.util.Date;

public final class c$a
{
  public final long a;
  public final v b;
  public final x c;
  public Date d;
  public String e;
  public Date f;
  public String g;
  public Date h;
  public long i;
  public long j;
  public String k;
  public int l;
  
  public c$a(long paramLong, v paramv, x paramx)
  {
    int m = -1;
    l = m;
    a = paramLong;
    b = paramv;
    c = paramx;
    if (paramx != null)
    {
      p localp = f;
      int n = 0;
      paramv = a;
      int i1 = paramv.length / 2;
      while (n < i1)
      {
        paramx = localp.a(n);
        String str1 = localp.b(n);
        String str2 = "Date";
        boolean bool1 = str2.equalsIgnoreCase(paramx);
        if (bool1)
        {
          paramx = g.a(str1);
          d = paramx;
          e = str1;
        }
        else
        {
          str2 = "Expires";
          bool1 = str2.equalsIgnoreCase(paramx);
          if (bool1)
          {
            paramx = g.a(str1);
            h = paramx;
          }
          else
          {
            str2 = "Last-Modified";
            bool1 = str2.equalsIgnoreCase(paramx);
            if (bool1)
            {
              paramx = g.a(str1);
              f = paramx;
              g = str1;
            }
            else
            {
              str2 = "ETag";
              bool1 = str2.equalsIgnoreCase(paramx);
              if (bool1)
              {
                k = str1;
              }
              else
              {
                str2 = "Age";
                bool1 = str2.equalsIgnoreCase(paramx);
                if (bool1)
                {
                  int i2 = d.b(str1, m);
                  l = i2;
                }
                else
                {
                  str2 = k.b;
                  bool1 = str2.equalsIgnoreCase(paramx);
                  long l1;
                  if (bool1)
                  {
                    l1 = Long.parseLong(str1);
                    i = l1;
                  }
                  else
                  {
                    str2 = k.c;
                    boolean bool2 = str2.equalsIgnoreCase(paramx);
                    if (bool2)
                    {
                      l1 = Long.parseLong(str1);
                      j = l1;
                    }
                  }
                }
              }
            }
          }
        }
        n += 1;
      }
    }
  }
  
  public static boolean a(v paramv)
  {
    String str = paramv.a("If-Modified-Since");
    if (str == null)
    {
      str = "If-None-Match";
      paramv = paramv.a(str);
      if (paramv == null) {
        return false;
      }
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */