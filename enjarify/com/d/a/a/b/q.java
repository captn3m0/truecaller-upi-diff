package com.d.a.a.b;

import com.d.a.a;
import com.d.a.a.i;
import com.d.a.n;
import com.d.a.z;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.ProxySelector;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

public final class q
{
  final a a;
  final i b;
  private Proxy c;
  private InetSocketAddress d;
  private List e;
  private int f;
  private List g;
  private int h;
  private final List i;
  
  public q(a parama, i parami)
  {
    Object localObject = Collections.emptyList();
    e = ((List)localObject);
    localObject = Collections.emptyList();
    g = ((List)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    i = ((List)localObject);
    a = parama;
    b = parami;
    parami = a;
    parama = h;
    if (parama != null)
    {
      parama = Collections.singletonList(parama);
      e = parama;
    }
    else
    {
      parama = new java/util/ArrayList;
      parama.<init>();
      e = parama;
      parama = a.g;
      parami = parami.b();
      parama = parama.select(parami);
      if (parama != null)
      {
        parami = e;
        parami.addAll(parama);
      }
      parama = e;
      parami = Collections.singleton(Proxy.NO_PROXY);
      parama.removeAll(parami);
      parama = e;
      parami = Proxy.NO_PROXY;
      parama.add(parami);
    }
    f = 0;
  }
  
  private void a(Proxy paramProxy)
  {
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    g = ((List)localObject1);
    localObject1 = paramProxy.type();
    Object localObject2 = Proxy.Type.DIRECT;
    if (localObject1 != localObject2)
    {
      localObject1 = paramProxy.type();
      localObject2 = Proxy.Type.SOCKS;
      if (localObject1 != localObject2)
      {
        localObject1 = paramProxy.address();
        boolean bool = localObject1 instanceof InetSocketAddress;
        if (bool)
        {
          localObject1 = (InetSocketAddress)localObject1;
          localObject2 = ((InetSocketAddress)localObject1).getAddress();
          if (localObject2 == null) {
            localObject2 = ((InetSocketAddress)localObject1).getHostName();
          } else {
            localObject2 = ((InetAddress)localObject2).getHostAddress();
          }
          k = ((InetSocketAddress)localObject1).getPort();
          break label158;
        }
        paramProxy = new java/lang/IllegalArgumentException;
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("Proxy.address() is not an InetSocketAddress: ");
        localObject1 = localObject1.getClass();
        ((StringBuilder)localObject2).append(localObject1);
        localObject1 = ((StringBuilder)localObject2).toString();
        paramProxy.<init>((String)localObject1);
        throw paramProxy;
      }
    }
    localObject2 = a.a.b;
    localObject1 = a.a;
    int k = c;
    label158:
    if (k > 0)
    {
      int m = (char)-1;
      if (k <= m)
      {
        paramProxy = paramProxy.type();
        localObject3 = Proxy.Type.SOCKS;
        if (paramProxy == localObject3)
        {
          paramProxy = g;
          localObject1 = InetSocketAddress.createUnresolved((String)localObject2, k);
          paramProxy.add(localObject1);
        }
        else
        {
          paramProxy = a.b.a((String)localObject2);
          int j = paramProxy.size();
          m = 0;
          localObject3 = null;
          while (m < j)
          {
            InetAddress localInetAddress = (InetAddress)paramProxy.get(m);
            List localList = g;
            InetSocketAddress localInetSocketAddress = new java/net/InetSocketAddress;
            localInetSocketAddress.<init>(localInetAddress, k);
            localList.add(localInetSocketAddress);
            m += 1;
          }
        }
        h = 0;
        return;
      }
    }
    paramProxy = new java/net/SocketException;
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("No route to ");
    ((StringBuilder)localObject3).append((String)localObject2);
    ((StringBuilder)localObject3).append(":");
    ((StringBuilder)localObject3).append(k);
    ((StringBuilder)localObject3).append("; port is out of range");
    localObject1 = ((StringBuilder)localObject3).toString();
    paramProxy.<init>((String)localObject1);
    throw paramProxy;
  }
  
  private boolean c()
  {
    int j = f;
    List localList = e;
    int k = localList.size();
    return j < k;
  }
  
  private boolean d()
  {
    int j = h;
    List localList = g;
    int k = localList.size();
    return j < k;
  }
  
  private boolean e()
  {
    List localList = i;
    boolean bool = localList.isEmpty();
    return !bool;
  }
  
  public final boolean a()
  {
    boolean bool = d();
    if (!bool)
    {
      bool = c();
      if (!bool)
      {
        bool = e();
        if (!bool) {
          return false;
        }
      }
    }
    return true;
  }
  
  public final z b()
  {
    for (;;)
    {
      boolean bool1 = d();
      if (!bool1)
      {
        bool1 = c();
        if (!bool1)
        {
          bool1 = e();
          if (bool1) {
            return (z)i.remove(0);
          }
          localObject1 = new java/util/NoSuchElementException;
          ((NoSuchElementException)localObject1).<init>();
          throw ((Throwable)localObject1);
        }
        bool1 = c();
        if (bool1)
        {
          localObject1 = e;
          j = f;
          k = j + 1;
          f = k;
          localObject1 = (Proxy)((List)localObject1).get(j);
          a((Proxy)localObject1);
          c = ((Proxy)localObject1);
        }
        else
        {
          localObject1 = new java/net/SocketException;
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>("No route to ");
          localObject3 = a.a.b;
          ((StringBuilder)localObject2).append((String)localObject3);
          ((StringBuilder)localObject2).append("; exhausted proxy configurations: ");
          localObject3 = e;
          ((StringBuilder)localObject2).append(localObject3);
          localObject2 = ((StringBuilder)localObject2).toString();
          ((SocketException)localObject1).<init>((String)localObject2);
          throw ((Throwable)localObject1);
        }
      }
      bool1 = d();
      if (!bool1) {
        break label293;
      }
      localObject1 = g;
      int j = h;
      int k = j + 1;
      h = k;
      localObject1 = (InetSocketAddress)((List)localObject1).get(j);
      d = ((InetSocketAddress)localObject1);
      localObject1 = new com/d/a/z;
      localObject2 = a;
      localObject3 = c;
      InetSocketAddress localInetSocketAddress = d;
      ((z)localObject1).<init>((a)localObject2, (Proxy)localObject3, localInetSocketAddress);
      localObject2 = b;
      boolean bool2 = ((i)localObject2).c((z)localObject1);
      if (!bool2) {
        break;
      }
      localObject2 = i;
      ((List)localObject2).add(localObject1);
    }
    return (z)localObject1;
    label293:
    Object localObject1 = new java/net/SocketException;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("No route to ");
    Object localObject3 = a.a.b;
    ((StringBuilder)localObject2).append((String)localObject3);
    ((StringBuilder)localObject2).append("; exhausted inet socket addresses: ");
    localObject3 = g;
    ((StringBuilder)localObject2).append(localObject3);
    localObject2 = ((StringBuilder)localObject2).toString();
    ((SocketException)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */