package com.d.a.a.b;

import com.d.a.a.j;
import d.c;
import d.d;
import d.e;
import d.u;
import d.v;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public final class h$2
  implements u
{
  boolean a;
  
  public h$2(h paramh, e parame, b paramb, d paramd) {}
  
  public final long a(c paramc, long paramLong)
  {
    boolean bool1 = true;
    try
    {
      e locale = b;
      paramLong = locale.a(paramc, paramLong);
      long l1 = -1;
      boolean bool2 = paramLong < l1;
      if (!bool2)
      {
        boolean bool3 = a;
        if (!bool3)
        {
          a = bool1;
          paramc = d;
          paramc.close();
        }
        return l1;
      }
      c localc = d.b();
      long l2 = b - paramLong;
      paramc.a(localc, l2, paramLong);
      d.v();
      return paramLong;
    }
    catch (IOException paramc)
    {
      boolean bool4 = a;
      if (!bool4)
      {
        a = bool1;
        b localb = c;
        localb.a();
      }
      throw paramc;
    }
  }
  
  public final void close()
  {
    boolean bool = a;
    if (!bool)
    {
      Object localObject = TimeUnit.MILLISECONDS;
      bool = j.a(this, (TimeUnit)localObject);
      if (!bool)
      {
        bool = true;
        a = bool;
        localObject = c;
        ((b)localObject).a();
      }
    }
    b.close();
  }
  
  public final v l_()
  {
    return b.l_();
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.h.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */