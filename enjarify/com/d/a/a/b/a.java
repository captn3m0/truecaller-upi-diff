package com.d.a.a.b;

import com.d.a.b;
import com.d.a.g;
import com.d.a.l;
import com.d.a.q;
import com.d.a.v;
import com.d.a.v.a;
import com.d.a.x;
import java.net.Authenticator;
import java.net.Authenticator.RequestorType;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.URL;
import java.util.List;

public final class a
  implements b
{
  public static final b a;
  
  static
  {
    a locala = new com/d/a/a/b/a;
    locala.<init>();
    a = locala;
  }
  
  private static InetAddress a(Proxy paramProxy, q paramq)
  {
    if (paramProxy != null)
    {
      Proxy.Type localType1 = paramProxy.type();
      Proxy.Type localType2 = Proxy.Type.DIRECT;
      if (localType1 != localType2) {
        return ((InetSocketAddress)paramProxy.address()).getAddress();
      }
    }
    return InetAddress.getByName(b);
  }
  
  public final v a(Proxy paramProxy, x paramx)
  {
    Object localObject1 = paramx.f();
    Object localObject2 = paramx;
    localObject2 = a;
    Object localObject3 = a;
    int i = ((List)localObject1).size();
    int j = 0;
    while (j < i)
    {
      Object localObject4 = (g)((List)localObject1).get(j);
      Object localObject5 = "Basic";
      String str1 = a;
      boolean bool = ((String)localObject5).equalsIgnoreCase(str1);
      if (bool)
      {
        str1 = b;
        localObject5 = paramProxy;
        InetAddress localInetAddress = a(paramProxy, (q)localObject3);
        int k = c;
        String str2 = a;
        String str3 = b;
        String str4 = a;
        URL localURL = ((q)localObject3).a();
        Authenticator.RequestorType localRequestorType = Authenticator.RequestorType.SERVER;
        localObject4 = Authenticator.requestPasswordAuthentication(str1, localInetAddress, k, str2, str3, str4, localURL, localRequestorType);
        if (localObject4 != null)
        {
          localObject1 = ((PasswordAuthentication)localObject4).getUserName();
          localObject3 = new java/lang/String;
          char[] arrayOfChar = ((PasswordAuthentication)localObject4).getPassword();
          ((String)localObject3).<init>(arrayOfChar);
          localObject1 = l.a((String)localObject1, (String)localObject3);
          return ((v)localObject2).b().a("Authorization", (String)localObject1).a();
        }
      }
      else
      {
        localObject5 = paramProxy;
      }
      j += 1;
    }
    return null;
  }
  
  public final v b(Proxy paramProxy, x paramx)
  {
    Object localObject1 = paramx.f();
    Object localObject2 = paramx;
    localObject2 = a;
    Object localObject3 = a;
    int i = ((List)localObject1).size();
    int j = 0;
    while (j < i)
    {
      Object localObject4 = (g)((List)localObject1).get(j);
      Object localObject5 = "Basic";
      String str1 = a;
      boolean bool = ((String)localObject5).equalsIgnoreCase(str1);
      if (bool)
      {
        localObject5 = (InetSocketAddress)paramProxy.address();
        str1 = ((InetSocketAddress)localObject5).getHostName();
        InetAddress localInetAddress = a(paramProxy, (q)localObject3);
        int k = ((InetSocketAddress)localObject5).getPort();
        String str2 = a;
        String str3 = b;
        String str4 = a;
        URL localURL = ((q)localObject3).a();
        Authenticator.RequestorType localRequestorType = Authenticator.RequestorType.PROXY;
        localObject4 = Authenticator.requestPasswordAuthentication(str1, localInetAddress, k, str2, str3, str4, localURL, localRequestorType);
        if (localObject4 != null)
        {
          localObject1 = ((PasswordAuthentication)localObject4).getUserName();
          localObject3 = new java/lang/String;
          char[] arrayOfChar = ((PasswordAuthentication)localObject4).getPassword();
          ((String)localObject3).<init>(arrayOfChar);
          localObject1 = l.a((String)localObject1, (String)localObject3);
          return ((v)localObject2).b().a("Proxy-Authorization", (String)localObject1).a();
        }
      }
      j += 1;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */