package com.d.a.a.b;

import com.d.a.a;
import com.d.a.i;
import com.d.a.q;
import com.d.a.r;
import com.d.a.r.a;
import com.d.a.t;
import com.d.a.v;
import com.d.a.x;
import com.d.a.y;
import com.d.a.z;
import d.d;
import d.n;
import java.net.ProtocolException;
import java.util.List;

public final class h$a
  implements r.a
{
  private final int b;
  private final v c;
  private int d;
  
  public h$a(h paramh, int paramInt, v paramv)
  {
    b = paramInt;
    c = paramv;
  }
  
  private i a()
  {
    return a.c.a();
  }
  
  public final x a(v paramv)
  {
    int i = d;
    int k = 1;
    i += k;
    d = i;
    i = b;
    int n;
    if (i > 0)
    {
      localObject1 = a.b.h;
      m = b - k;
      localObject1 = (r)((List)localObject1).get(m);
      localObject2 = aaa;
      Object localObject3 = a.b;
      String str = a.b;
      boolean bool2 = ((String)localObject3).equals(str);
      if (bool2)
      {
        localObject3 = a;
        n = c;
        localObject2 = a;
        m = c;
        if (n == m)
        {
          m = d;
          if (m <= k) {
            break label249;
          }
          paramv = new java/lang/IllegalStateException;
          localObject4 = new java/lang/StringBuilder;
          ((StringBuilder)localObject4).<init>("network interceptor ");
          ((StringBuilder)localObject4).append(localObject1);
          ((StringBuilder)localObject4).append(" must call proceed() exactly once");
          localObject1 = ((StringBuilder)localObject4).toString();
          paramv.<init>((String)localObject1);
          throw paramv;
        }
      }
      paramv = new java/lang/IllegalStateException;
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>("network interceptor ");
      ((StringBuilder)localObject4).append(localObject1);
      ((StringBuilder)localObject4).append(" must retain the same host and port");
      localObject1 = ((StringBuilder)localObject4).toString();
      paramv.<init>((String)localObject1);
      throw paramv;
    }
    label249:
    i = b;
    Object localObject2 = a.b.h;
    int m = ((List)localObject2).size();
    if (i < m)
    {
      localObject1 = new com/d/a/a/b/h$a;
      localObject2 = a;
      n = b + k;
      ((a)localObject1).<init>((h)localObject2, n, paramv);
      paramv = a.b.h;
      m = b;
      paramv = (r)paramv.get(m);
      localObject2 = paramv.a();
      i = d;
      if (i == k)
      {
        if (localObject2 != null) {
          return (x)localObject2;
        }
        localObject1 = new java/lang/NullPointerException;
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>("network interceptor ");
        ((StringBuilder)localObject4).append(paramv);
        ((StringBuilder)localObject4).append(" returned null");
        paramv = ((StringBuilder)localObject4).toString();
        ((NullPointerException)localObject1).<init>(paramv);
        throw ((Throwable)localObject1);
      }
      localObject1 = new java/lang/IllegalStateException;
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>("network interceptor ");
      ((StringBuilder)localObject4).append(paramv);
      ((StringBuilder)localObject4).append(" must call proceed() exactly once");
      paramv = ((StringBuilder)localObject4).toString();
      ((IllegalStateException)localObject1).<init>(paramv);
      throw ((Throwable)localObject1);
    }
    h.a(a).a(paramv);
    Object localObject1 = a;
    h.a((h)localObject1, paramv);
    boolean bool1 = h.a(paramv);
    long l1;
    if (bool1)
    {
      localObject1 = d;
      if (localObject1 != null)
      {
        localObject1 = h.a(a);
        l1 = -1;
        paramv = n.a(((j)localObject1).a(paramv, l1));
        paramv.close();
      }
    }
    paramv = h.b(a);
    int j = c;
    k = 204;
    if (j != k)
    {
      k = 205;
      if (j != k) {}
    }
    else
    {
      localObject4 = g;
      l1 = ((y)localObject4).a();
      l2 = 0L;
      boolean bool3 = l1 < l2;
      if (bool3) {
        break label600;
      }
    }
    return paramv;
    label600:
    Object localObject4 = new java/net/ProtocolException;
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("HTTP ");
    ((StringBuilder)localObject2).append(j);
    ((StringBuilder)localObject2).append(" had non-zero Content-Length: ");
    long l2 = g.a();
    ((StringBuilder)localObject2).append(l2);
    paramv = ((StringBuilder)localObject2).toString();
    ((ProtocolException)localObject4).<init>(paramv);
    throw ((Throwable)localObject4);
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.h.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */