package com.d.a.a.b;

import d.c;
import d.d;
import d.t;
import d.v;
import java.net.ProtocolException;

final class e$d
  implements t
{
  private final d.j b;
  private boolean c;
  private long d;
  
  private e$d(e parame, long paramLong)
  {
    parame = new d/j;
    v localv = a.c.l_();
    parame.<init>(localv);
    b = parame;
    d = paramLong;
  }
  
  public final void a_(c paramc, long paramLong)
  {
    boolean bool1 = c;
    if (!bool1)
    {
      com.d.a.a.j.a(b, paramLong);
      long l1 = d;
      boolean bool2 = paramLong < l1;
      if (!bool2)
      {
        a.c.a_(paramc, paramLong);
        l1 = d - paramLong;
        d = l1;
        return;
      }
      paramc = new java/net/ProtocolException;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>("expected ");
      long l2 = d;
      localStringBuilder.append(l2);
      localStringBuilder.append(" bytes but received ");
      localStringBuilder.append(paramLong);
      String str = localStringBuilder.toString();
      paramc.<init>(str);
      throw paramc;
    }
    paramc = new java/lang/IllegalStateException;
    paramc.<init>("closed");
    throw paramc;
  }
  
  public final void close()
  {
    boolean bool1 = c;
    if (bool1) {
      return;
    }
    bool1 = true;
    c = bool1;
    long l1 = d;
    long l2 = 0L;
    boolean bool2 = l1 < l2;
    if (!bool2)
    {
      e.a(b);
      a.d = 3;
      return;
    }
    ProtocolException localProtocolException = new java/net/ProtocolException;
    localProtocolException.<init>("unexpected end of stream");
    throw localProtocolException;
  }
  
  public final void flush()
  {
    boolean bool = c;
    if (bool) {
      return;
    }
    a.c.flush();
  }
  
  public final v l_()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.e.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */