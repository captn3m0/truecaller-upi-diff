package com.d.a.a.b;

import java.io.IOException;
import java.lang.reflect.Method;

public final class p
  extends Exception
{
  public static final Method a;
  public IOException b;
  
  static
  {
    Object localObject = Throwable.class;
    String str = "addSuppressed";
    int i = 1;
    try
    {
      Class[] arrayOfClass = new Class[i];
      Class localClass = Throwable.class;
      arrayOfClass[0] = localClass;
      localObject = ((Class)localObject).getDeclaredMethod(str, arrayOfClass);
    }
    catch (Exception localException)
    {
      localObject = null;
    }
    a = (Method)localObject;
  }
  
  public p(IOException paramIOException)
  {
    super(paramIOException);
    b = paramIOException;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */