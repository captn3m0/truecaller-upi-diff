package com.d.a.a.b;

import com.d.a.u;
import java.net.ProtocolException;

public final class r
{
  public final u a;
  public final int b;
  public final String c;
  
  public r(u paramu, int paramInt, String paramString)
  {
    a = paramu;
    b = paramInt;
    c = paramString;
  }
  
  public static r a(String paramString)
  {
    Object localObject = "HTTP/1.";
    boolean bool1 = paramString.startsWith((String)localObject);
    int j = 32;
    int k = 4;
    int m = 9;
    if (bool1)
    {
      int i = paramString.length();
      if (i >= m)
      {
        i = paramString.charAt(8);
        if (i == j)
        {
          i = paramString.charAt(7) + '￐';
          if (i == 0)
          {
            localObject = u.a;
            break label149;
          }
          n = 1;
          if (i == n)
          {
            localObject = u.b;
            break label149;
          }
          localObject = new java/net/ProtocolException;
          paramString = String.valueOf(paramString);
          paramString = "Unexpected status line: ".concat(paramString);
          ((ProtocolException)localObject).<init>(paramString);
          throw ((Throwable)localObject);
        }
      }
      localObject = new java/net/ProtocolException;
      paramString = String.valueOf(paramString);
      paramString = "Unexpected status line: ".concat(paramString);
      ((ProtocolException)localObject).<init>(paramString);
      throw ((Throwable)localObject);
    }
    else
    {
      localObject = "ICY ";
      boolean bool2 = paramString.startsWith((String)localObject);
      if (!bool2) {
        break label319;
      }
      localObject = u.a;
      m = 4;
    }
    label149:
    int n = paramString.length();
    int i1 = m + 3;
    if (n >= i1) {
      try
      {
        String str1 = paramString.substring(m, i1);
        n = Integer.parseInt(str1);
        String str2 = "";
        int i2 = paramString.length();
        if (i2 > i1)
        {
          i1 = paramString.charAt(i1);
          if (i1 == j)
          {
            m += k;
            str2 = paramString.substring(m);
          }
          else
          {
            localObject = new java/net/ProtocolException;
            paramString = String.valueOf(paramString);
            paramString = "Unexpected status line: ".concat(paramString);
            ((ProtocolException)localObject).<init>(paramString);
            throw ((Throwable)localObject);
          }
        }
        paramString = new com/d/a/a/b/r;
        paramString.<init>((u)localObject, n, str2);
        return paramString;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        localObject = new java/net/ProtocolException;
        paramString = String.valueOf(paramString);
        paramString = "Unexpected status line: ".concat(paramString);
        ((ProtocolException)localObject).<init>(paramString);
        throw ((Throwable)localObject);
      }
    }
    localObject = new java/net/ProtocolException;
    paramString = String.valueOf(paramString);
    paramString = "Unexpected status line: ".concat(paramString);
    ((ProtocolException)localObject).<init>(paramString);
    throw ((Throwable)localObject);
    label319:
    localObject = new java/net/ProtocolException;
    paramString = String.valueOf(paramString);
    paramString = "Unexpected status line: ".concat(paramString);
    ((ProtocolException)localObject).<init>(paramString);
    throw ((Throwable)localObject);
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    Object localObject1 = a;
    Object localObject2 = u.a;
    if (localObject1 == localObject2) {
      localObject1 = "HTTP/1.0";
    } else {
      localObject1 = "HTTP/1.1";
    }
    localStringBuilder.append((String)localObject1);
    char c1 = ' ';
    localStringBuilder.append(c1);
    int i = b;
    localStringBuilder.append(i);
    localObject2 = c;
    if (localObject2 != null)
    {
      localStringBuilder.append(c1);
      localObject1 = c;
      localStringBuilder.append((String)localObject1);
    }
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */