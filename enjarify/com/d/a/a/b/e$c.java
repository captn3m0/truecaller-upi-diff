package com.d.a.a.b;

import com.d.a.a.j;
import com.d.a.p;
import d.c;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;

final class e$c
  extends e.a
{
  private long e = -1;
  private boolean f = true;
  private final h g;
  
  e$c(e parame, h paramh)
  {
    super(parame, (byte)0);
    g = paramh;
  }
  
  public final long a(c paramc, long paramLong)
  {
    long l1 = 0L;
    boolean bool1 = paramLong < l1;
    if (!bool1)
    {
      bool1 = b;
      if (!bool1)
      {
        bool1 = f;
        long l2 = -1;
        if (!bool1) {
          return l2;
        }
        long l3 = e;
        bool1 = l3 < l1;
        Object localObject1;
        if (bool1)
        {
          bool1 = l3 < l2;
          if (bool1) {}
        }
        else
        {
          l3 = e;
          bool1 = l3 < l2;
          if (bool1)
          {
            localObject1 = d.b;
            ((d.e)localObject1).q();
          }
        }
        try
        {
          localObject1 = d;
          localObject1 = b;
          l3 = ((d.e)localObject1).n();
          e = l3;
          localObject1 = d;
          localObject1 = b;
          localObject1 = ((d.e)localObject1).q();
          localObject1 = ((String)localObject1).trim();
          l3 = e;
          boolean bool2 = l3 < l1;
          if (!bool2)
          {
            boolean bool3 = ((String)localObject1).isEmpty();
            if (!bool3)
            {
              String str1 = ";";
              bool3 = ((String)localObject1).startsWith(str1);
              if (!bool3) {}
            }
            else
            {
              l3 = e;
              bool1 = l3 < l1;
              if (!bool1)
              {
                bool4 = false;
                f = false;
                localObject2 = g;
                p localp = d.d();
                ((h)localObject2).a(localp);
                b();
              }
              boolean bool4 = f;
              if (!bool4) {
                return l2;
              }
              Object localObject2 = d.b;
              long l4 = e;
              paramLong = Math.min(paramLong, l4);
              long l5 = ((d.e)localObject2).a(paramc, paramLong);
              boolean bool5 = l5 < l2;
              if (bool5)
              {
                l1 = e - l5;
                e = l1;
                return l5;
              }
              c();
              paramc = new java/net/ProtocolException;
              paramc.<init>("unexpected end of stream");
              throw paramc;
            }
          }
          paramc = new java/net/ProtocolException;
          localObject3 = new java/lang/StringBuilder;
          String str2 = "expected chunk size and optional extensions but was \"";
          ((StringBuilder)localObject3).<init>(str2);
          l1 = e;
          ((StringBuilder)localObject3).append(l1);
          ((StringBuilder)localObject3).append((String)localObject1);
          str2 = "\"";
          ((StringBuilder)localObject3).append(str2);
          localObject3 = ((StringBuilder)localObject3).toString();
          paramc.<init>((String)localObject3);
          throw paramc;
        }
        catch (NumberFormatException paramc)
        {
          localObject3 = new java/net/ProtocolException;
          paramc = paramc.getMessage();
          ((ProtocolException)localObject3).<init>(paramc);
          throw ((Throwable)localObject3);
        }
      }
      paramc = new java/lang/IllegalStateException;
      paramc.<init>("closed");
      throw paramc;
    }
    paramc = new java/lang/IllegalArgumentException;
    Object localObject3 = String.valueOf(paramLong);
    localObject3 = "byteCount < 0: ".concat((String)localObject3);
    paramc.<init>((String)localObject3);
    throw paramc;
  }
  
  public final void close()
  {
    boolean bool = b;
    if (bool) {
      return;
    }
    bool = f;
    if (bool)
    {
      TimeUnit localTimeUnit = TimeUnit.MILLISECONDS;
      bool = j.a(this, localTimeUnit);
      if (!bool) {
        c();
      }
    }
    b = true;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.e.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */