package com.d.a.a.b;

import com.d.a.a.h;
import com.d.a.a.j;
import com.d.a.b;
import com.d.a.g;
import com.d.a.p;
import com.d.a.p.a;
import com.d.a.v;
import com.d.a.v.a;
import com.d.a.x;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public final class k
{
  static final String a;
  public static final String b;
  public static final String c;
  public static final String d;
  public static final String e;
  private static final Comparator f;
  
  static
  {
    Object localObject = new com/d/a/a/b/k$1;
    ((k.1)localObject).<init>();
    f = (Comparator)localObject;
    h.a();
    a = h.b();
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    String str = a;
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append("-Sent-Millis");
    b = ((StringBuilder)localObject).toString();
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    str = a;
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append("-Received-Millis");
    c = ((StringBuilder)localObject).toString();
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    str = a;
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append("-Selected-Protocol");
    d = ((StringBuilder)localObject).toString();
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    str = a;
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append("-Response-Source");
    e = ((StringBuilder)localObject).toString();
  }
  
  public static long a(p paramp)
  {
    return b(paramp.a("Content-Length"));
  }
  
  public static long a(v paramv)
  {
    return a(c);
  }
  
  public static long a(x paramx)
  {
    return a(f);
  }
  
  public static v a(b paramb, x paramx, Proxy paramProxy)
  {
    int i = c;
    int j = 407;
    if (i == j) {
      return paramb.b(paramProxy, paramx);
    }
    return paramb.a(paramProxy, paramx);
  }
  
  public static List a(p paramp, String paramString)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    String[] arrayOfString = a;
    int i = arrayOfString.length / 2;
    int j = 0;
    while (j < i)
    {
      String str1 = paramp.a(j);
      boolean bool1 = paramString.equalsIgnoreCase(str1);
      if (bool1)
      {
        str1 = paramp.b(j);
        int k = 0;
        String str2 = null;
        for (;;)
        {
          int n = str1.length();
          if (k >= n) {
            break;
          }
          String str3 = " ";
          n = d.a(str1, k, str3);
          String str4 = str1.substring(k, n).trim();
          int i1 = d.a(str1, n);
          n = 1;
          String str5 = "realm=\"";
          int i2 = 7;
          str2 = str1;
          boolean bool2 = str1.regionMatches(n, i1, str5, 0, i2);
          if (!bool2) {
            break;
          }
          i1 += 7;
          str2 = "\"";
          int m = d.a(str1, i1, str2);
          str3 = str1.substring(i1, m);
          m += 1;
          m = d.a(str1, m, ",") + 1;
          m = d.a(str1, m);
          g localg = new com/d/a/g;
          localg.<init>(str4, str3);
          localArrayList.add(localg);
        }
      }
      j += 1;
    }
    return localArrayList;
  }
  
  public static void a(v.a parama, Map paramMap)
  {
    paramMap = paramMap.entrySet().iterator();
    for (;;)
    {
      boolean bool1 = paramMap.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject1 = (Map.Entry)paramMap.next();
      String str1 = (String)((Map.Entry)localObject1).getKey();
      Object localObject2 = "Cookie";
      boolean bool2 = ((String)localObject2).equalsIgnoreCase(str1);
      if (!bool2)
      {
        localObject2 = "Cookie2";
        bool2 = ((String)localObject2).equalsIgnoreCase(str1);
        if (!bool2) {}
      }
      else
      {
        localObject2 = (List)((Map.Entry)localObject1).getValue();
        bool2 = ((List)localObject2).isEmpty();
        if (!bool2)
        {
          localObject1 = (List)((Map.Entry)localObject1).getValue();
          int i = ((List)localObject1).size();
          int j = 0;
          int k = 1;
          if (i == k)
          {
            localObject1 = (String)((List)localObject1).get(0);
          }
          else
          {
            localObject2 = new java/lang/StringBuilder;
            ((StringBuilder)localObject2).<init>();
            k = ((List)localObject1).size();
            while (j < k)
            {
              if (j > 0)
              {
                str2 = "; ";
                ((StringBuilder)localObject2).append(str2);
              }
              String str2 = (String)((List)localObject1).get(j);
              ((StringBuilder)localObject2).append(str2);
              j += 1;
            }
            localObject1 = ((StringBuilder)localObject2).toString();
          }
          parama.b(str1, (String)localObject1);
        }
      }
    }
  }
  
  public static boolean a(x paramx, p paramp, v paramv)
  {
    paramx = c(f).iterator();
    boolean bool;
    do
    {
      bool = paramx.hasNext();
      if (!bool) {
        break;
      }
      Object localObject = (String)paramx.next();
      List localList = paramp.c((String)localObject);
      p localp = c;
      localObject = localp.c((String)localObject);
      bool = j.a(localList, localObject);
    } while (bool);
    return false;
    return true;
  }
  
  static boolean a(String paramString)
  {
    String str = "Connection";
    boolean bool1 = str.equalsIgnoreCase(paramString);
    if (!bool1)
    {
      str = "Keep-Alive";
      bool1 = str.equalsIgnoreCase(paramString);
      if (!bool1)
      {
        str = "Proxy-Authenticate";
        bool1 = str.equalsIgnoreCase(paramString);
        if (!bool1)
        {
          str = "Proxy-Authorization";
          bool1 = str.equalsIgnoreCase(paramString);
          if (!bool1)
          {
            str = "TE";
            bool1 = str.equalsIgnoreCase(paramString);
            if (!bool1)
            {
              str = "Trailers";
              bool1 = str.equalsIgnoreCase(paramString);
              if (!bool1)
              {
                str = "Transfer-Encoding";
                bool1 = str.equalsIgnoreCase(paramString);
                if (!bool1)
                {
                  str = "Upgrade";
                  boolean bool2 = str.equalsIgnoreCase(paramString);
                  if (!bool2) {
                    return true;
                  }
                }
              }
            }
          }
        }
      }
    }
    return false;
  }
  
  private static long b(String paramString)
  {
    long l = -1;
    if (paramString == null) {
      return l;
    }
    try
    {
      return Long.parseLong(paramString);
    }
    catch (NumberFormatException localNumberFormatException) {}
    return l;
  }
  
  public static Map b(p paramp)
  {
    TreeMap localTreeMap = new java/util/TreeMap;
    Object localObject1 = f;
    localTreeMap.<init>((Comparator)localObject1);
    localObject1 = a;
    int i = localObject1.length / 2;
    int j = 0;
    while (j < i)
    {
      String str = paramp.a(j);
      Object localObject2 = paramp.b(j);
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      List localList = (List)localTreeMap.get(str);
      if (localList != null) {
        localArrayList.addAll(localList);
      }
      localArrayList.add(localObject2);
      localObject2 = Collections.unmodifiableList(localArrayList);
      localTreeMap.put(str, localObject2);
      j += 1;
    }
    return Collections.unmodifiableMap(localTreeMap);
  }
  
  public static boolean b(x paramx)
  {
    return c(f).contains("*");
  }
  
  public static p c(x paramx)
  {
    p localp = h.a.c;
    paramx = c(f);
    boolean bool1 = paramx.isEmpty();
    if (bool1)
    {
      paramx = new com/d/a/p$a;
      paramx.<init>();
      return paramx.a();
    }
    p.a locala = new com/d/a/p$a;
    locala.<init>();
    int i = 0;
    String[] arrayOfString = a;
    int j = arrayOfString.length / 2;
    while (i < j)
    {
      String str1 = localp.a(i);
      boolean bool2 = paramx.contains(str1);
      if (bool2)
      {
        String str2 = localp.b(i);
        locala.a(str1, str2);
      }
      i += 1;
    }
    return locala.a();
  }
  
  private static Set c(p paramp)
  {
    Set localSet = Collections.emptySet();
    String[] arrayOfString = a;
    int i = arrayOfString.length / 2;
    Object localObject1 = localSet;
    int j = 0;
    localSet = null;
    while (j < i)
    {
      Object localObject2 = "Vary";
      Object localObject3 = paramp.a(j);
      boolean bool1 = ((String)localObject2).equalsIgnoreCase((String)localObject3);
      if (bool1)
      {
        localObject2 = paramp.b(j);
        boolean bool2 = ((Set)localObject1).isEmpty();
        if (bool2)
        {
          localObject1 = new java/util/TreeSet;
          localObject3 = String.CASE_INSENSITIVE_ORDER;
          ((TreeSet)localObject1).<init>((Comparator)localObject3);
        }
        localObject3 = ",";
        localObject2 = ((String)localObject2).split((String)localObject3);
        int k = localObject2.length;
        int m = 0;
        while (m < k)
        {
          String str = localObject2[m].trim();
          ((Set)localObject1).add(str);
          m += 1;
        }
      }
      j += 1;
    }
    return (Set)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */