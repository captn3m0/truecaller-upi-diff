package com.d.a.a.b;

import com.d.a.a;
import com.d.a.a.c.b;
import com.d.a.a.d;
import com.d.a.a.i;
import com.d.a.z;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.ProxySelector;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.URI;
import java.util.List;

public final class s
{
  public final a a;
  public q b;
  public b c;
  private final com.d.a.j d;
  private boolean e;
  private boolean f;
  private j g;
  
  public s(com.d.a.j paramj, a parama)
  {
    d = paramj;
    a = parama;
  }
  
  private b a(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
  {
    synchronized (d)
    {
      boolean bool1 = e;
      if (!bool1)
      {
        ??? = g;
        if (??? == null)
        {
          bool1 = f;
          if (!bool1)
          {
            ??? = c;
            if (??? != null)
            {
              boolean bool2 = i;
              if (!bool2) {
                return (b)???;
              }
            }
            ??? = d.b;
            Object localObject3 = d;
            Object localObject4 = a;
            ??? = ((d)???).a((com.d.a.j)localObject3, (a)localObject4, this);
            if (??? != null)
            {
              c = ((b)???);
              return (b)???;
            }
            ??? = b;
            if (??? == null)
            {
              ??? = new com/d/a/a/b/q;
              localObject3 = a;
              localObject4 = b();
              ((q)???).<init>((a)localObject3, (i)localObject4);
              b = ((q)???);
            }
            ??? = b.b();
            b localb = new com/d/a/a/c/b;
            localb.<init>((z)???);
            a(localb);
            synchronized (d)
            {
              ??? = d.b;
              localObject3 = d;
              ((d)???).b((com.d.a.j)localObject3, localb);
              c = localb;
              boolean bool3 = f;
              if (!bool3)
              {
                List localList = a.f;
                ??? = localb;
                int i = paramInt1;
                localb.a(paramInt1, paramInt2, paramInt3, localList, paramBoolean);
                localObject5 = b();
                localObject9 = a;
                ((i)localObject5).b((z)localObject9);
                return localb;
              }
              Object localObject5 = new java/io/IOException;
              localObject9 = "Canceled";
              ((IOException)localObject5).<init>((String)localObject9);
              throw ((Throwable)localObject5);
            }
          }
          localObject7 = new java/io/IOException;
          localObject9 = "Canceled";
          ((IOException)localObject7).<init>((String)localObject9);
          throw ((Throwable)localObject7);
        }
        localObject7 = new java/lang/IllegalStateException;
        localObject9 = "stream != null";
        ((IllegalStateException)localObject7).<init>((String)localObject9);
        throw ((Throwable)localObject7);
      }
      Object localObject7 = new java/lang/IllegalStateException;
      Object localObject9 = "released";
      ((IllegalStateException)localObject7).<init>((String)localObject9);
      throw ((Throwable)localObject7);
    }
  }
  
  private b b(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean1, boolean paramBoolean2)
  {
    for (;;)
    {
      b localb = a(paramInt1, paramInt2, paramInt3, paramBoolean1);
      synchronized (d)
      {
        int i = e;
        if (i == 0) {
          return localb;
        }
        boolean bool = localb.a(paramBoolean2);
        if (bool) {
          return localb;
        }
        localb = null;
        bool = true;
        a(bool, false, bool);
      }
    }
  }
  
  private i b()
  {
    d locald = d.b;
    com.d.a.j localj = d;
    return locald.a(localj);
  }
  
  private void b(b paramb)
  {
    List localList = h;
    int i = localList.size();
    int j = 0;
    while (j < i)
    {
      Object localObject = ((Reference)h.get(j)).get();
      if (localObject == this)
      {
        h.remove(j);
        return;
      }
      j += 1;
    }
    paramb = new java/lang/IllegalStateException;
    paramb.<init>();
    throw paramb;
  }
  
  /* Error */
  public final j a(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean1, boolean paramBoolean2)
  {
    // Byte code:
    //   0: aload_0
    //   1: iload_1
    //   2: iload_2
    //   3: iload_3
    //   4: iload 4
    //   6: iload 5
    //   8: invokespecial 138	com/d/a/a/b/s:b	(IIIZZ)Lcom/d/a/a/c/b;
    //   11: astore 6
    //   13: aload 6
    //   15: getfield 141	com/d/a/a/c/b:d	Lcom/d/a/a/a/d;
    //   18: astore 7
    //   20: aload 7
    //   22: ifnull +26 -> 48
    //   25: new 143	com/d/a/a/b/f
    //   28: astore 8
    //   30: aload 6
    //   32: getfield 141	com/d/a/a/c/b:d	Lcom/d/a/a/a/d;
    //   35: astore 9
    //   37: aload 8
    //   39: aload_0
    //   40: aload 9
    //   42: invokespecial 146	com/d/a/a/b/f:<init>	(Lcom/d/a/a/b/s;Lcom/d/a/a/a/d;)V
    //   45: goto +115 -> 160
    //   48: aload 6
    //   50: getfield 149	com/d/a/a/c/b:b	Ljava/net/Socket;
    //   53: astore 7
    //   55: aload 7
    //   57: iload_2
    //   58: invokevirtual 155	java/net/Socket:setSoTimeout	(I)V
    //   61: aload 6
    //   63: getfield 158	com/d/a/a/c/b:f	Ld/e;
    //   66: astore 7
    //   68: aload 7
    //   70: invokeinterface 164 1 0
    //   75: astore 7
    //   77: iload_2
    //   78: i2l
    //   79: lstore 10
    //   81: getstatic 170	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   84: astore 8
    //   86: aload 7
    //   88: lload 10
    //   90: aload 8
    //   92: invokevirtual 175	d/v:a	(JLjava/util/concurrent/TimeUnit;)Ld/v;
    //   95: pop
    //   96: aload 6
    //   98: getfield 178	com/d/a/a/c/b:g	Ld/d;
    //   101: astore 8
    //   103: aload 8
    //   105: invokeinterface 181 1 0
    //   110: astore 8
    //   112: iload_3
    //   113: i2l
    //   114: lstore 12
    //   116: getstatic 170	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   119: astore 14
    //   121: aload 8
    //   123: lload 12
    //   125: aload 14
    //   127: invokevirtual 175	d/v:a	(JLjava/util/concurrent/TimeUnit;)Ld/v;
    //   130: pop
    //   131: new 183	com/d/a/a/b/e
    //   134: astore 8
    //   136: aload 6
    //   138: getfield 158	com/d/a/a/c/b:f	Ld/e;
    //   141: astore 9
    //   143: aload 6
    //   145: getfield 178	com/d/a/a/c/b:g	Ld/d;
    //   148: astore 7
    //   150: aload 8
    //   152: aload_0
    //   153: aload 9
    //   155: aload 7
    //   157: invokespecial 186	com/d/a/a/b/e:<init>	(Lcom/d/a/a/b/s;Ld/e;Ld/d;)V
    //   160: aload_0
    //   161: getfield 23	com/d/a/a/b/s:d	Lcom/d/a/j;
    //   164: astore 9
    //   166: aload 9
    //   168: monitorenter
    //   169: aload 6
    //   171: getfield 103	com/d/a/a/c/b:e	I
    //   174: iconst_1
    //   175: iadd
    //   176: istore 4
    //   178: aload 6
    //   180: iload 4
    //   182: putfield 103	com/d/a/a/c/b:e	I
    //   185: aload_0
    //   186: aload 8
    //   188: putfield 29	com/d/a/a/b/s:g	Lcom/d/a/a/b/j;
    //   191: aload 9
    //   193: monitorexit
    //   194: aload 8
    //   196: areturn
    //   197: astore 6
    //   199: aload 9
    //   201: monitorexit
    //   202: aload 6
    //   204: athrow
    //   205: astore 6
    //   207: new 188	com/d/a/a/b/p
    //   210: astore 8
    //   212: aload 8
    //   214: aload 6
    //   216: invokespecial 191	com/d/a/a/b/p:<init>	(Ljava/io/IOException;)V
    //   219: aload 8
    //   221: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	222	0	this	s
    //   0	222	1	paramInt1	int
    //   0	222	2	paramInt2	int
    //   0	222	3	paramInt3	int
    //   0	222	4	paramBoolean1	boolean
    //   0	222	5	paramBoolean2	boolean
    //   11	168	6	localb	b
    //   197	6	6	localObject1	Object
    //   205	10	6	localIOException	IOException
    //   18	138	7	localObject2	Object
    //   28	192	8	localObject3	Object
    //   79	10	10	l1	long
    //   114	10	12	l2	long
    //   119	7	14	localTimeUnit	java.util.concurrent.TimeUnit
    // Exception table:
    //   from	to	target	type
    //   169	174	197	finally
    //   180	185	197	finally
    //   186	191	197	finally
    //   191	194	197	finally
    //   199	202	197	finally
    //   6	11	205	java/io/IOException
    //   13	18	205	java/io/IOException
    //   25	28	205	java/io/IOException
    //   30	35	205	java/io/IOException
    //   40	45	205	java/io/IOException
    //   48	53	205	java/io/IOException
    //   57	61	205	java/io/IOException
    //   61	66	205	java/io/IOException
    //   68	75	205	java/io/IOException
    //   81	84	205	java/io/IOException
    //   90	96	205	java/io/IOException
    //   96	101	205	java/io/IOException
    //   103	110	205	java/io/IOException
    //   116	119	205	java/io/IOException
    //   125	131	205	java/io/IOException
    //   131	134	205	java/io/IOException
    //   136	141	205	java/io/IOException
    //   143	148	205	java/io/IOException
    //   155	160	205	java/io/IOException
    //   160	164	205	java/io/IOException
    //   166	169	205	java/io/IOException
    //   202	205	205	java/io/IOException
  }
  
  public final b a()
  {
    try
    {
      b localb = c;
      return localb;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final void a(j paramj)
  {
    com.d.a.j localj = d;
    if (paramj != null) {}
    try
    {
      Object localObject1 = g;
      if (paramj == localObject1)
      {
        a(false, false, true);
        return;
      }
      localObject1 = new java/lang/IllegalStateException;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      Object localObject2 = "expected ";
      localStringBuilder.<init>((String)localObject2);
      localObject2 = g;
      localStringBuilder.append(localObject2);
      localObject2 = " but was ";
      localStringBuilder.append((String)localObject2);
      localStringBuilder.append(paramj);
      paramj = localStringBuilder.toString();
      ((IllegalStateException)localObject1).<init>(paramj);
      throw ((Throwable)localObject1);
    }
    finally {}
  }
  
  public final void a(b paramb)
  {
    paramb = h;
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(this);
    paramb.add(localWeakReference);
  }
  
  public final void a(IOException paramIOException)
  {
    synchronized (d)
    {
      Object localObject1 = b;
      if (localObject1 != null)
      {
        localObject1 = c;
        int i = e;
        if (i == 0)
        {
          localObject1 = c;
          localObject1 = a;
          q localq = b;
          Object localObject2 = b;
          localObject2 = ((Proxy)localObject2).type();
          Object localObject3 = Proxy.Type.DIRECT;
          if (localObject2 != localObject3)
          {
            localObject2 = a;
            localObject2 = g;
            if (localObject2 != null)
            {
              localObject2 = a;
              localObject2 = g;
              localObject3 = a;
              localObject3 = a;
              localObject3 = ((com.d.a.q)localObject3).b();
              Object localObject4 = b;
              localObject4 = ((Proxy)localObject4).address();
              ((ProxySelector)localObject2).connectFailed((URI)localObject3, (SocketAddress)localObject4, paramIOException);
            }
          }
          paramIOException = b;
          paramIOException.a((z)localObject1);
        }
        else
        {
          paramIOException = null;
          b = null;
        }
      }
      boolean bool = true;
      a(bool, false, bool);
      return;
    }
  }
  
  public final void a(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    com.d.a.j localj = d;
    if (paramBoolean3) {
      try
      {
        g = null;
      }
      finally
      {
        break label255;
      }
    }
    paramBoolean3 = true;
    if (paramBoolean2) {
      e = paramBoolean3;
    }
    Object localObject3 = c;
    if (localObject3 != null)
    {
      if (paramBoolean1)
      {
        localObject2 = c;
        i = paramBoolean3;
      }
      localObject2 = g;
      if (localObject2 == null)
      {
        paramBoolean1 = e;
        if (!paramBoolean1)
        {
          localObject2 = c;
          paramBoolean1 = i;
          if (!paramBoolean1) {}
        }
        else
        {
          localObject2 = c;
          b((b)localObject2);
          localObject2 = c;
          paramBoolean1 = e;
          if (paramBoolean1) {
            b = null;
          }
          localObject2 = c;
          localObject2 = h;
          paramBoolean1 = ((List)localObject2).isEmpty();
          if (paramBoolean1)
          {
            localObject2 = c;
            long l = System.nanoTime();
            j = l;
            localObject2 = d.b;
            localObject3 = d;
            b localb = c;
            paramBoolean1 = ((d)localObject2).a((com.d.a.j)localObject3, localb);
            if (paramBoolean1)
            {
              localObject2 = c;
              break label221;
            }
          }
          paramBoolean1 = false;
          localObject2 = null;
          label221:
          c = null;
          break label234;
        }
      }
    }
    paramBoolean1 = false;
    Object localObject2 = null;
    label234:
    if (localObject2 != null)
    {
      localObject2 = b;
      com.d.a.a.j.a((Socket)localObject2);
    }
    return;
    label255:
    throw ((Throwable)localObject2);
  }
  
  public final String toString()
  {
    return a.toString();
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */