package com.d.a.a.b;

import com.d.a.a.a.d;
import com.d.a.p;
import com.d.a.p.a;
import com.d.a.q;
import com.d.a.x;
import com.d.a.x.a;
import com.d.a.y;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public final class f
  implements j
{
  private static final d.f a = d.f.a("connection");
  private static final d.f b = d.f.a("host");
  private static final d.f c = d.f.a("keep-alive");
  private static final d.f d = d.f.a("proxy-connection");
  private static final d.f e = d.f.a("transfer-encoding");
  private static final d.f f = d.f.a("te");
  private static final d.f g = d.f.a("encoding");
  private static final d.f h = d.f.a("upgrade");
  private static final List i;
  private static final List j;
  private static final List k;
  private static final List l;
  private final s m;
  private final d n;
  private h o;
  private com.d.a.a.a.e p;
  
  static
  {
    int i1 = 11;
    Object localObject1 = new d.f[i1];
    d.f localf = a;
    localObject1[0] = localf;
    localf = b;
    int i2 = 1;
    localObject1[i2] = localf;
    localf = c;
    int i3 = 2;
    localObject1[i3] = localf;
    localf = d;
    int i4 = 3;
    localObject1[i4] = localf;
    localf = e;
    int i5 = 4;
    localObject1[i5] = localf;
    localf = com.d.a.a.a.f.b;
    int i6 = 5;
    localObject1[i6] = localf;
    localf = com.d.a.a.a.f.c;
    int i7 = 6;
    localObject1[i7] = localf;
    localf = com.d.a.a.a.f.d;
    int i8 = 7;
    localObject1[i8] = localf;
    localf = com.d.a.a.a.f.e;
    int i9 = 8;
    localObject1[i9] = localf;
    localf = com.d.a.a.a.f.f;
    int i10 = 9;
    localObject1[i10] = localf;
    localf = com.d.a.a.a.f.g;
    int i11 = 10;
    localObject1[i11] = localf;
    i = com.d.a.a.j.a((Object[])localObject1);
    localObject1 = new d.f[i6];
    localf = a;
    localObject1[0] = localf;
    localf = b;
    localObject1[i2] = localf;
    localf = c;
    localObject1[i3] = localf;
    localf = d;
    localObject1[i4] = localf;
    localf = e;
    localObject1[i5] = localf;
    j = com.d.a.a.j.a((Object[])localObject1);
    localObject1 = new d.f[14];
    localf = a;
    localObject1[0] = localf;
    localf = b;
    localObject1[i2] = localf;
    localf = c;
    localObject1[i3] = localf;
    localf = d;
    localObject1[i4] = localf;
    localf = f;
    localObject1[i5] = localf;
    localf = e;
    localObject1[i6] = localf;
    localf = g;
    localObject1[i7] = localf;
    localf = h;
    localObject1[i8] = localf;
    localf = com.d.a.a.a.f.b;
    localObject1[i9] = localf;
    localf = com.d.a.a.a.f.c;
    localObject1[i10] = localf;
    localf = com.d.a.a.a.f.d;
    localObject1[i11] = localf;
    localf = com.d.a.a.a.f.e;
    localObject1[i1] = localf;
    Object localObject2 = com.d.a.a.a.f.f;
    localObject1[12] = localObject2;
    localObject2 = com.d.a.a.a.f.g;
    localObject1[13] = localObject2;
    k = com.d.a.a.j.a((Object[])localObject1);
    localObject2 = new d.f[i9];
    localObject1 = a;
    localObject2[0] = localObject1;
    localObject1 = b;
    localObject2[i2] = localObject1;
    localObject1 = c;
    localObject2[i3] = localObject1;
    localObject1 = d;
    localObject2[i4] = localObject1;
    localObject1 = f;
    localObject2[i5] = localObject1;
    localObject1 = e;
    localObject2[i6] = localObject1;
    localObject1 = g;
    localObject2[i7] = localObject1;
    localObject1 = h;
    localObject2[i8] = localObject1;
    l = com.d.a.a.j.a((Object[])localObject2);
  }
  
  public f(s params, d paramd)
  {
    m = params;
    n = paramd;
  }
  
  private static x.a a(List paramList)
  {
    p.a locala = new com/d/a/p$a;
    locala.<init>();
    int i1 = paramList.size();
    int i2 = 0;
    Object localObject1 = null;
    Object localObject2 = "HTTP/1.1";
    int i3 = 0;
    x.a locala1 = null;
    while (i3 < i1)
    {
      d.f localf = geth;
      String str = geti.a();
      Object localObject3 = localObject2;
      localObject2 = localObject1;
      i2 = 0;
      localObject1 = null;
      for (;;)
      {
        int i4 = str.length();
        if (i2 >= i4) {
          break;
        }
        i4 = str.indexOf(0, i2);
        int i5 = -1;
        if (i4 == i5) {
          i4 = str.length();
        }
        localObject1 = str.substring(i2, i4);
        Object localObject4 = com.d.a.a.a.f.a;
        boolean bool = localf.equals(localObject4);
        if (bool)
        {
          localObject2 = localObject1;
        }
        else
        {
          localObject4 = com.d.a.a.a.f.g;
          bool = localf.equals(localObject4);
          if (bool)
          {
            localObject3 = localObject1;
          }
          else
          {
            localObject4 = j;
            bool = ((List)localObject4).contains(localf);
            if (!bool)
            {
              localObject4 = localf.a();
              locala.a((String)localObject4, (String)localObject1);
            }
          }
        }
        i2 = i4 + 1;
      }
      i3 += 1;
      localObject1 = localObject2;
      localObject2 = localObject3;
    }
    if (localObject1 != null)
    {
      paramList = new java/lang/StringBuilder;
      paramList.<init>();
      paramList.append((String)localObject2);
      paramList.append(" ");
      paramList.append((String)localObject1);
      paramList = r.a(paramList.toString());
      locala1 = new com/d/a/x$a;
      locala1.<init>();
      com.d.a.u localu = com.d.a.u.c;
      b = localu;
      i1 = b;
      c = i1;
      paramList = c;
      d = paramList;
      paramList = locala.a();
      return locala1.a(paramList);
    }
    paramList = new java/net/ProtocolException;
    paramList.<init>("Expected ':status' header not present");
    throw paramList;
  }
  
  private static List b(com.d.a.v paramv)
  {
    p localp = c;
    ArrayList localArrayList = new java/util/ArrayList;
    int i1 = a.length / 2 + 5;
    localArrayList.<init>(i1);
    Object localObject1 = new com/d/a/a/a/f;
    d.f localf = com.d.a.a.a.f.b;
    String str = b;
    ((com.d.a.a.a.f)localObject1).<init>(localf, str);
    localArrayList.add(localObject1);
    localObject1 = new com/d/a/a/a/f;
    localf = com.d.a.a.a.f.c;
    str = n.a(a);
    ((com.d.a.a.a.f)localObject1).<init>(localf, str);
    localArrayList.add(localObject1);
    localObject1 = new com/d/a/a/a/f;
    localf = com.d.a.a.a.f.g;
    ((com.d.a.a.a.f)localObject1).<init>(localf, "HTTP/1.1");
    localArrayList.add(localObject1);
    localObject1 = new com/d/a/a/a/f;
    localf = com.d.a.a.a.f.f;
    str = com.d.a.a.j.a(a);
    ((com.d.a.a.a.f)localObject1).<init>(localf, str);
    localArrayList.add(localObject1);
    localObject1 = new com/d/a/a/a/f;
    localf = com.d.a.a.a.f.d;
    paramv = a.a;
    ((com.d.a.a.a.f)localObject1).<init>(localf, paramv);
    localArrayList.add(localObject1);
    paramv = new java/util/LinkedHashSet;
    paramv.<init>();
    localObject1 = a;
    i1 = localObject1.length / 2;
    localf = null;
    int i2 = 0;
    str = null;
    while (i2 < i1)
    {
      Object localObject2 = localp.a(i2);
      Object localObject3 = Locale.US;
      localObject2 = d.f.a(((String)localObject2).toLowerCase((Locale)localObject3));
      localObject3 = i;
      boolean bool1 = ((List)localObject3).contains(localObject2);
      if (!bool1)
      {
        localObject3 = localp.b(i2);
        int i3 = paramv.add(localObject2);
        com.d.a.a.a.f localf1;
        if (i3 != 0)
        {
          localf1 = new com/d/a/a/a/f;
          localf1.<init>((d.f)localObject2, (String)localObject3);
          localArrayList.add(localf1);
        }
        else
        {
          i3 = 0;
          localf1 = null;
          for (;;)
          {
            int i5 = localArrayList.size();
            if (i3 >= i5) {
              break;
            }
            Object localObject4 = geth;
            boolean bool2 = ((d.f)localObject4).equals(localObject2);
            if (bool2)
            {
              localObject4 = geti.a();
              StringBuilder localStringBuilder = new java/lang/StringBuilder;
              localStringBuilder.<init>((String)localObject4);
              localStringBuilder.append('\000');
              localStringBuilder.append((String)localObject3);
              localObject3 = localStringBuilder.toString();
              localObject4 = new com/d/a/a/a/f;
              ((com.d.a.a.a.f)localObject4).<init>((d.f)localObject2, (String)localObject3);
              localArrayList.set(i3, localObject4);
              break;
            }
            int i4;
            i3 += 1;
          }
        }
      }
      i2 += 1;
    }
    return localArrayList;
  }
  
  public final x.a a()
  {
    Object localObject1 = n.a;
    Object localObject2 = com.d.a.u.d;
    if (localObject1 == localObject2)
    {
      localObject1 = p.c();
      localObject2 = null;
      p.a locala = new com/d/a/p$a;
      locala.<init>();
      int i1 = 0;
      com.d.a.u localu = null;
      int i2 = ((List)localObject1).size();
      while (i1 < i2)
      {
        Object localObject3 = geth;
        String str = geti.a();
        Object localObject4 = com.d.a.a.a.f.a;
        boolean bool = ((d.f)localObject3).equals(localObject4);
        if (bool)
        {
          localObject2 = str;
        }
        else
        {
          localObject4 = l;
          bool = ((List)localObject4).contains(localObject3);
          if (!bool)
          {
            localObject3 = ((d.f)localObject3).a();
            locala.a((String)localObject3, str);
          }
        }
        i1 += 1;
      }
      if (localObject2 != null)
      {
        localObject2 = String.valueOf(localObject2);
        localObject1 = r.a("HTTP/1.1 ".concat((String)localObject2));
        localObject2 = new com/d/a/x$a;
        ((x.a)localObject2).<init>();
        localu = com.d.a.u.d;
        b = localu;
        i1 = b;
        c = i1;
        localObject1 = c;
        d = ((String)localObject1);
        localObject1 = locala.a();
        return ((x.a)localObject2).a((p)localObject1);
      }
      localObject1 = new java/net/ProtocolException;
      ((ProtocolException)localObject1).<init>("Expected ':status' header not present");
      throw ((Throwable)localObject1);
    }
    return a(p.c());
  }
  
  public final y a(x paramx)
  {
    Object localObject1 = new com/d/a/a/b/f$a;
    Object localObject2 = p.f;
    ((f.a)localObject1).<init>(this, (d.u)localObject2);
    localObject2 = new com/d/a/a/b/l;
    paramx = f;
    localObject1 = d.n.a((d.u)localObject1);
    ((l)localObject2).<init>(paramx, (d.e)localObject1);
    return (y)localObject2;
  }
  
  public final d.t a(com.d.a.v paramv, long paramLong)
  {
    return p.d();
  }
  
  public final void a(h paramh)
  {
    o = paramh;
  }
  
  public final void a(o paramo)
  {
    d.t localt = p.d();
    paramo.a(localt);
  }
  
  public final void a(com.d.a.v paramv)
  {
    Object localObject1 = p;
    if (localObject1 != null) {
      return;
    }
    localObject1 = o;
    ((h)localObject1).a();
    boolean bool1 = h.a(paramv);
    Object localObject2 = n.a;
    Object localObject3 = com.d.a.u.d;
    if (localObject2 == localObject3)
    {
      localObject2 = c;
      localObject3 = new java/util/ArrayList;
      int i1 = a.length / 2 + 4;
      ((ArrayList)localObject3).<init>(i1);
      Object localObject4 = new com/d/a/a/a/f;
      Object localObject5 = com.d.a.a.a.f.b;
      Object localObject6 = b;
      ((com.d.a.a.a.f)localObject4).<init>((d.f)localObject5, (String)localObject6);
      ((List)localObject3).add(localObject4);
      localObject4 = new com/d/a/a/a/f;
      localObject5 = com.d.a.a.a.f.c;
      localObject6 = n.a(a);
      ((com.d.a.a.a.f)localObject4).<init>((d.f)localObject5, (String)localObject6);
      ((List)localObject3).add(localObject4);
      localObject4 = new com/d/a/a/a/f;
      localObject5 = com.d.a.a.a.f.e;
      localObject6 = com.d.a.a.j.a(a);
      ((com.d.a.a.a.f)localObject4).<init>((d.f)localObject5, (String)localObject6);
      ((List)localObject3).add(localObject4);
      localObject4 = new com/d/a/a/a/f;
      localObject5 = com.d.a.a.a.f.d;
      paramv = a.a;
      ((com.d.a.a.a.f)localObject4).<init>((d.f)localObject5, paramv);
      ((List)localObject3).add(localObject4);
      int i2 = 0;
      paramv = null;
      localObject4 = a;
      i1 = localObject4.length / 2;
      while (i2 < i1)
      {
        localObject5 = ((p)localObject2).a(i2);
        localObject6 = Locale.US;
        localObject5 = d.f.a(((String)localObject5).toLowerCase((Locale)localObject6));
        localObject6 = k;
        boolean bool2 = ((List)localObject6).contains(localObject5);
        if (!bool2)
        {
          localObject6 = new com/d/a/a/a/f;
          String str = ((p)localObject2).b(i2);
          ((com.d.a.a.a.f)localObject6).<init>((d.f)localObject5, str);
          ((List)localObject3).add(localObject6);
        }
        i2 += 1;
      }
    }
    localObject3 = b(paramv);
    paramv = n.a((List)localObject3, bool1);
    p = paramv;
    paramv = p.h;
    long l1 = o.b.w;
    localObject3 = TimeUnit.MILLISECONDS;
    paramv.a(l1, (TimeUnit)localObject3);
    paramv = p.i;
    l1 = o.b.x;
    localObject3 = TimeUnit.MILLISECONDS;
    paramv.a(l1, (TimeUnit)localObject3);
  }
  
  public final void b()
  {
    p.d().close();
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */