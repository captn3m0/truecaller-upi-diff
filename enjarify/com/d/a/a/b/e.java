package com.d.a.a.b;

import com.d.a.i;
import com.d.a.p;
import com.d.a.p.a;
import com.d.a.q;
import com.d.a.v;
import com.d.a.x;
import com.d.a.x.a;
import com.d.a.y;
import com.d.a.z;
import d.t;
import java.io.EOFException;
import java.io.IOException;
import java.net.Proxy;
import java.net.Proxy.Type;

public final class e
  implements j
{
  final s a;
  final d.e b;
  final d.d c;
  int d = 0;
  private h e;
  
  public e(s params, d.e parame, d.d paramd)
  {
    a = params;
    b = parame;
    c = paramd;
  }
  
  public final x.a a()
  {
    return c();
  }
  
  public final y a(x paramx)
  {
    boolean bool = h.c(paramx);
    if (!bool)
    {
      long l1 = 0L;
      localObject1 = a(l1);
    }
    else
    {
      localObject1 = "chunked";
      localObject2 = paramx.a("Transfer-Encoding");
      bool = ((String)localObject1).equalsIgnoreCase((String)localObject2);
      int j = 5;
      int m = 4;
      if (bool)
      {
        localObject1 = e;
        int n = d;
        if (n == m)
        {
          d = j;
          localObject2 = new com/d/a/a/b/e$c;
          ((e.c)localObject2).<init>(this, (h)localObject1);
          localObject1 = localObject2;
        }
        else
        {
          paramx = new java/lang/IllegalStateException;
          localObject1 = new java/lang/StringBuilder;
          ((StringBuilder)localObject1).<init>("state: ");
          j = d;
          ((StringBuilder)localObject1).append(j);
          localObject1 = ((StringBuilder)localObject1).toString();
          paramx.<init>((String)localObject1);
          throw paramx;
        }
      }
      else
      {
        long l2 = k.a(paramx);
        long l3 = -1;
        bool = l2 < l3;
        if (bool)
        {
          localObject1 = a(l2);
        }
        else
        {
          int i = d;
          if (i != m) {
            break label267;
          }
          localObject1 = a;
          if (localObject1 == null) {
            break label255;
          }
          d = j;
          j = 1;
          m = 0;
          ((s)localObject1).a(j, false, false);
          localObject1 = new com/d/a/a/b/e$f;
          ((e.f)localObject1).<init>(this, (byte)0);
        }
      }
    }
    Object localObject2 = new com/d/a/a/b/l;
    paramx = f;
    Object localObject1 = d.n.a((d.u)localObject1);
    ((l)localObject2).<init>(paramx, (d.e)localObject1);
    return (y)localObject2;
    label255:
    paramx = new java/lang/IllegalStateException;
    paramx.<init>("streamAllocation == null");
    throw paramx;
    label267:
    paramx = new java/lang/IllegalStateException;
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("state: ");
    int k = d;
    ((StringBuilder)localObject1).append(k);
    localObject1 = ((StringBuilder)localObject1).toString();
    paramx.<init>((String)localObject1);
    throw paramx;
  }
  
  public final t a(v paramv, long paramLong)
  {
    String str = "Transfer-Encoding";
    paramv = paramv.a(str);
    boolean bool1 = "chunked".equalsIgnoreCase(paramv);
    int k = 2;
    int m = 1;
    Object localObject;
    int n;
    if (bool1)
    {
      int i = d;
      if (i == m)
      {
        d = k;
        paramv = new com/d/a/a/b/e$b;
        paramv.<init>(this, (byte)0);
        return paramv;
      }
      paramv = new java/lang/IllegalStateException;
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>("state: ");
      n = d;
      ((StringBuilder)localObject).append(n);
      localObject = ((StringBuilder)localObject).toString();
      paramv.<init>((String)localObject);
      throw paramv;
    }
    long l = -1;
    boolean bool2 = paramLong < l;
    if (bool2)
    {
      int j = d;
      if (j == m)
      {
        d = k;
        paramv = new com/d/a/a/b/e$d;
        paramv.<init>(this, paramLong, (byte)0);
        return paramv;
      }
      paramv = new java/lang/IllegalStateException;
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>("state: ");
      n = d;
      ((StringBuilder)localObject).append(n);
      localObject = ((StringBuilder)localObject).toString();
      paramv.<init>((String)localObject);
      throw paramv;
    }
    paramv = new java/lang/IllegalStateException;
    paramv.<init>("Cannot stream a request body without chunked encoding or a known content length!");
    throw paramv;
  }
  
  public final d.u a(long paramLong)
  {
    int i = d;
    int j = 4;
    if (i == j)
    {
      d = 5;
      e.e locale = new com/d/a/a/b/e$e;
      locale.<init>(this, paramLong);
      return locale;
    }
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("state: ");
    i = d;
    ((StringBuilder)localObject).append(i);
    localObject = ((StringBuilder)localObject).toString();
    localIllegalStateException.<init>((String)localObject);
    throw localIllegalStateException;
  }
  
  public final void a(h paramh)
  {
    e = paramh;
  }
  
  public final void a(o paramo)
  {
    int i = d;
    int j = 1;
    if (i == j)
    {
      d = 3;
      localObject = c;
      paramo.a((t)localObject);
      return;
    }
    paramo = new java/lang/IllegalStateException;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("state: ");
    j = d;
    ((StringBuilder)localObject).append(j);
    localObject = ((StringBuilder)localObject).toString();
    paramo.<init>((String)localObject);
    throw paramo;
  }
  
  public final void a(p paramp, String paramString)
  {
    int i = d;
    if (i == 0)
    {
      c.b(paramString).b("\r\n");
      int j = 0;
      paramString = null;
      String[] arrayOfString = a;
      i = arrayOfString.length / 2;
      while (j < i)
      {
        d.d locald = c;
        String str = paramp.a(j);
        locald = locald.b(str).b(": ");
        str = paramp.b(j);
        locald = locald.b(str);
        str = "\r\n";
        locald.b(str);
        j += 1;
      }
      c.b("\r\n");
      d = 1;
      return;
    }
    paramp = new java/lang/IllegalStateException;
    paramString = new java/lang/StringBuilder;
    paramString.<init>("state: ");
    i = d;
    paramString.append(i);
    paramString = paramString.toString();
    paramp.<init>(paramString);
    throw paramp;
  }
  
  public final void a(v paramv)
  {
    e.a();
    Object localObject1 = e.c.a().a().b.type();
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    Object localObject2 = b;
    localStringBuilder.append((String)localObject2);
    localStringBuilder.append(' ');
    localObject2 = a;
    boolean bool = ((q)localObject2).c();
    if (!bool)
    {
      localObject2 = Proxy.Type.HTTP;
      if (localObject1 == localObject2)
      {
        i = 1;
        break label97;
      }
    }
    int i = 0;
    localObject1 = null;
    label97:
    if (i != 0)
    {
      localObject1 = a;
      localStringBuilder.append(localObject1);
    }
    else
    {
      localObject1 = n.a(a);
      localStringBuilder.append((String)localObject1);
    }
    localStringBuilder.append(" HTTP/1.1");
    localObject1 = localStringBuilder.toString();
    paramv = c;
    a(paramv, (String)localObject1);
  }
  
  public final void b()
  {
    c.flush();
  }
  
  public final x.a c()
  {
    int i = d;
    int j = 1;
    Object localObject1;
    Object localObject2;
    int k;
    if (i != j)
    {
      j = 3;
      if (i != j)
      {
        localObject1 = new java/lang/IllegalStateException;
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("state: ");
        k = d;
        ((StringBuilder)localObject2).append(k);
        localObject2 = ((StringBuilder)localObject2).toString();
        ((IllegalStateException)localObject1).<init>((String)localObject2);
        throw ((Throwable)localObject1);
      }
    }
    try
    {
      do
      {
        localObject1 = b;
        localObject1 = ((d.e)localObject1).q();
        localObject1 = r.a((String)localObject1);
        localObject2 = new com/d/a/x$a;
        ((x.a)localObject2).<init>();
        localObject3 = a;
        b = ((com.d.a.u)localObject3);
        k = b;
        c = k;
        localObject3 = c;
        d = ((String)localObject3);
        localObject3 = d();
        localObject2 = ((x.a)localObject2).a((p)localObject3);
        i = b;
        k = 100;
      } while (i == k);
      i = 4;
      d = i;
      return (x.a)localObject2;
    }
    catch (EOFException localEOFException)
    {
      localObject2 = new java/io/IOException;
      Object localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("unexpected end of stream on ");
      s locals = a;
      ((StringBuilder)localObject3).append(locals);
      localObject3 = ((StringBuilder)localObject3).toString();
      ((IOException)localObject2).<init>((String)localObject3);
      ((IOException)localObject2).initCause(localEOFException);
      throw ((Throwable)localObject2);
    }
  }
  
  public final p d()
  {
    p.a locala = new com/d/a/p$a;
    locala.<init>();
    for (;;)
    {
      String str = b.q();
      int i = str.length();
      if (i == 0) {
        break;
      }
      com.d.a.a.d locald = com.d.a.a.d.b;
      locald.a(locala, str);
    }
    return locala.a();
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */