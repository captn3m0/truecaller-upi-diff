package com.d.a.a.b;

import com.d.a.a.j;
import d.c;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;

final class e$e
  extends e.a
{
  private long e;
  
  public e$e(e parame, long paramLong)
  {
    super(parame, (byte)0);
    e = paramLong;
    long l1 = e;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (!bool) {
      b();
    }
  }
  
  public final long a(c paramc, long paramLong)
  {
    long l1 = 0L;
    boolean bool1 = paramLong < l1;
    if (!bool1)
    {
      bool1 = b;
      if (!bool1)
      {
        long l2 = e;
        long l3 = -1;
        boolean bool2 = l2 < l1;
        if (!bool2) {
          return l3;
        }
        d.e locale = d.b;
        long l4 = e;
        paramLong = Math.min(l4, paramLong);
        long l5 = locale.a(paramc, paramLong);
        boolean bool3 = l5 < l3;
        if (bool3)
        {
          l2 = e - l5;
          e = l2;
          l2 = e;
          bool3 = l2 < l1;
          if (!bool3) {
            b();
          }
          return l5;
        }
        c();
        paramc = new java/net/ProtocolException;
        paramc.<init>("unexpected end of stream");
        throw paramc;
      }
      paramc = new java/lang/IllegalStateException;
      paramc.<init>("closed");
      throw paramc;
    }
    paramc = new java/lang/IllegalArgumentException;
    String str = String.valueOf(paramLong);
    str = "byteCount < 0: ".concat(str);
    paramc.<init>(str);
    throw paramc;
  }
  
  public final void close()
  {
    boolean bool1 = b;
    if (bool1) {
      return;
    }
    long l1 = e;
    long l2 = 0L;
    boolean bool2 = l1 < l2;
    if (bool2)
    {
      TimeUnit localTimeUnit = TimeUnit.MILLISECONDS;
      bool1 = j.a(this, localTimeUnit);
      if (!bool1) {
        c();
      }
    }
    b = true;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.e.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */