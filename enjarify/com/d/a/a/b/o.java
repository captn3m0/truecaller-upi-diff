package com.d.a.a.b;

import com.d.a.a.j;
import d.c;
import d.t;
import d.v;
import java.net.ProtocolException;

public final class o
  implements t
{
  public final c a;
  private boolean b;
  private final int c;
  
  public o()
  {
    this(-1);
  }
  
  public o(int paramInt)
  {
    c localc = new d/c;
    localc.<init>();
    a = localc;
    c = paramInt;
  }
  
  public final void a(t paramt)
  {
    c localc1 = new d/c;
    localc1.<init>();
    c localc2 = a;
    long l1 = b;
    localc2.a(localc1, 0L, l1);
    long l2 = b;
    paramt.a_(localc1, l2);
  }
  
  public final void a_(c paramc, long paramLong)
  {
    boolean bool1 = b;
    if (!bool1)
    {
      long l1 = b;
      j.a(l1, paramLong);
      int i = c;
      int j = -1;
      if (i != j)
      {
        c localc = a;
        l1 = b;
        int k = c;
        long l2 = k - paramLong;
        boolean bool2 = l1 < l2;
        if (bool2)
        {
          paramc = new java/net/ProtocolException;
          Object localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>("exceeded content-length limit of ");
          int m = c;
          ((StringBuilder)localObject).append(m);
          ((StringBuilder)localObject).append(" bytes");
          localObject = ((StringBuilder)localObject).toString();
          paramc.<init>((String)localObject);
          throw paramc;
        }
      }
      a.a_(paramc, paramLong);
      return;
    }
    paramc = new java/lang/IllegalStateException;
    paramc.<init>("closed");
    throw paramc;
  }
  
  public final void close()
  {
    boolean bool1 = b;
    if (bool1) {
      return;
    }
    bool1 = true;
    b = bool1;
    Object localObject1 = a;
    long l1 = b;
    int i = c;
    long l2 = i;
    boolean bool2 = l1 < l2;
    if (!bool2) {
      return;
    }
    localObject1 = new java/net/ProtocolException;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("content-length promised ");
    i = c;
    ((StringBuilder)localObject2).append(i);
    ((StringBuilder)localObject2).append(" bytes, but received ");
    l2 = a.b;
    ((StringBuilder)localObject2).append(l2);
    localObject2 = ((StringBuilder)localObject2).toString();
    ((ProtocolException)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
  }
  
  public final void flush() {}
  
  public final v l_()
  {
    return v.c;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */