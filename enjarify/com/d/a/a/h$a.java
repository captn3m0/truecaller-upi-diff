package com.d.a.a;

import com.d.a.a.d.a;
import com.d.a.a.d.f;
import com.d.a.u;
import d.c;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.List;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

final class h$a
  extends h
{
  private final Class a;
  private final g b;
  private final g c;
  private final Method d;
  private final Method e;
  private final g f;
  private final g g;
  
  public h$a(Class paramClass, g paramg1, g paramg2, Method paramMethod1, Method paramMethod2, g paramg3, g paramg4)
  {
    a = paramClass;
    b = paramg1;
    c = paramg2;
    d = paramMethod1;
    e = paramMethod2;
    f = paramg3;
    g = paramg4;
  }
  
  public final f a(X509TrustManager paramX509TrustManager)
  {
    f localf = a.a(paramX509TrustManager);
    if (localf != null) {
      return localf;
    }
    return super.a(paramX509TrustManager);
  }
  
  public final X509TrustManager a(SSLSocketFactory paramSSLSocketFactory)
  {
    Object localObject1 = a;
    String str = "sslParameters";
    localObject1 = a(paramSSLSocketFactory, (Class)localObject1, str);
    if (localObject1 == null)
    {
      localObject1 = "com.google.android.gms.org.conscrypt.SSLParametersImpl";
      str = null;
      try
      {
        Object localObject2 = paramSSLSocketFactory.getClass();
        localObject2 = ((Class)localObject2).getClassLoader();
        localObject1 = Class.forName((String)localObject1, false, (ClassLoader)localObject2);
        str = "sslParameters";
        localObject1 = a(paramSSLSocketFactory, (Class)localObject1, str);
      }
      catch (ClassNotFoundException localClassNotFoundException)
      {
        return null;
      }
    }
    str = "x509TrustManager";
    paramSSLSocketFactory = (X509TrustManager)a(localObject1, X509TrustManager.class, str);
    if (paramSSLSocketFactory != null) {
      return paramSSLSocketFactory;
    }
    return (X509TrustManager)a(localObject1, X509TrustManager.class, "trustManager");
  }
  
  public final void a(Socket paramSocket, InetSocketAddress paramInetSocketAddress, int paramInt)
  {
    try
    {
      paramSocket.connect(paramInetSocketAddress, paramInt);
      return;
    }
    catch (SecurityException paramSocket)
    {
      paramInetSocketAddress = new java/io/IOException;
      paramInetSocketAddress.<init>("Exception in connect");
      paramInetSocketAddress.initCause(paramSocket);
      throw paramInetSocketAddress;
    }
    catch (AssertionError paramSocket)
    {
      boolean bool = j.a(paramSocket);
      if (bool)
      {
        paramInetSocketAddress = new java/io/IOException;
        paramInetSocketAddress.<init>(paramSocket);
        throw paramInetSocketAddress;
      }
      throw paramSocket;
    }
  }
  
  public final void a(SSLSocket paramSSLSocket, String paramString, List paramList)
  {
    int i = 1;
    Object[] arrayOfObject;
    Object localObject1;
    if (paramString != null)
    {
      g localg = b;
      arrayOfObject = new Object[i];
      localObject1 = Boolean.TRUE;
      arrayOfObject[0] = localObject1;
      localg.a(paramSSLSocket, arrayOfObject);
      localg = c;
      arrayOfObject = new Object[i];
      arrayOfObject[0] = paramString;
      localg.a(paramSSLSocket, arrayOfObject);
    }
    paramString = g;
    if (paramString != null)
    {
      boolean bool = paramString.a(paramSSLSocket);
      if (bool)
      {
        paramString = new Object[i];
        c localc = new d/c;
        localc.<init>();
        int j = paramList.size();
        int k = 0;
        arrayOfObject = null;
        while (k < j)
        {
          localObject1 = (u)paramList.get(k);
          Object localObject2 = u.a;
          if (localObject1 != localObject2)
          {
            localObject2 = ((u)localObject1).toString();
            int m = ((String)localObject2).length();
            localc.b(m);
            localObject1 = ((u)localObject1).toString();
            localc.a((String)localObject1);
          }
          k += 1;
        }
        paramList = localc.r();
        paramString[0] = paramList;
        paramList = g;
        paramList.b(paramSSLSocket, paramString);
      }
    }
  }
  
  public final String b(SSLSocket paramSSLSocket)
  {
    Object localObject = f;
    Charset localCharset = null;
    if (localObject == null) {
      return null;
    }
    boolean bool = ((g)localObject).a(paramSSLSocket);
    if (!bool) {
      return null;
    }
    localObject = f;
    Object[] arrayOfObject = new Object[0];
    paramSSLSocket = (byte[])((g)localObject).b(paramSSLSocket, arrayOfObject);
    if (paramSSLSocket != null)
    {
      localObject = new java/lang/String;
      localCharset = j.c;
      ((String)localObject).<init>(paramSSLSocket, localCharset);
      return (String)localObject;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.h.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */