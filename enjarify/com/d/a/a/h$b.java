package com.d.a.a;

import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

class h$b
  extends h
{
  private final Class a;
  
  public h$b(Class paramClass)
  {
    a = paramClass;
  }
  
  public final X509TrustManager a(SSLSocketFactory paramSSLSocketFactory)
  {
    Class localClass = a;
    String str = "context";
    paramSSLSocketFactory = a(paramSSLSocketFactory, localClass, str);
    if (paramSSLSocketFactory == null) {
      return null;
    }
    return (X509TrustManager)a(paramSSLSocketFactory, X509TrustManager.class, "trustManager");
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.h.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */