package com.d.a.a.d;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import javax.net.ssl.X509TrustManager;

public final class a
  implements f
{
  private final X509TrustManager a;
  private final Method b;
  
  private a(X509TrustManager paramX509TrustManager, Method paramMethod)
  {
    b = paramMethod;
    a = paramX509TrustManager;
  }
  
  public static f a(X509TrustManager paramX509TrustManager)
  {
    try
    {
      Object localObject1 = paramX509TrustManager.getClass();
      Object localObject2 = "findTrustAnchorByIssuerAndSignature";
      boolean bool = true;
      Class[] arrayOfClass = new Class[bool];
      Class localClass = X509Certificate.class;
      arrayOfClass[0] = localClass;
      localObject1 = ((Class)localObject1).getDeclaredMethod((String)localObject2, arrayOfClass);
      ((Method)localObject1).setAccessible(bool);
      localObject2 = new com/d/a/a/d/a;
      ((a)localObject2).<init>(paramX509TrustManager, (Method)localObject1);
      return (f)localObject2;
    }
    catch (NoSuchMethodException localNoSuchMethodException) {}
    return null;
  }
  
  public final X509Certificate a(X509Certificate paramX509Certificate)
  {
    try
    {
      Method localMethod = b;
      X509TrustManager localX509TrustManager = a;
      int i = 1;
      Object[] arrayOfObject = new Object[i];
      arrayOfObject[0] = paramX509Certificate;
      paramX509Certificate = localMethod.invoke(localX509TrustManager, arrayOfObject);
      paramX509Certificate = (TrustAnchor)paramX509Certificate;
      if (paramX509Certificate != null) {
        return paramX509Certificate.getTrustedCert();
      }
      return null;
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      return null;
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      paramX509Certificate = new java/lang/AssertionError;
      paramX509Certificate.<init>();
      throw paramX509Certificate;
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */