package com.d.a.a.d;

import java.security.GeneralSecurityException;
import java.security.Principal;
import java.security.cert.X509Certificate;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;

public final class b
{
  private final f a;
  
  public b(f paramf)
  {
    a = paramf;
  }
  
  private static boolean a(X509Certificate paramX509Certificate1, X509Certificate paramX509Certificate2)
  {
    Principal localPrincipal1 = paramX509Certificate1.getIssuerDN();
    Principal localPrincipal2 = paramX509Certificate2.getSubjectDN();
    boolean bool = localPrincipal1.equals(localPrincipal2);
    localPrincipal2 = null;
    if (!bool) {
      return false;
    }
    try
    {
      paramX509Certificate2 = paramX509Certificate2.getPublicKey();
      paramX509Certificate1.verify(paramX509Certificate2);
      return true;
    }
    catch (GeneralSecurityException localGeneralSecurityException) {}
    return false;
  }
  
  public final List a(List paramList)
  {
    Object localObject1 = new java/util/ArrayDeque;
    ((ArrayDeque)localObject1).<init>(paramList);
    paramList = new java/util/ArrayList;
    paramList.<init>();
    Object localObject2 = ((Deque)localObject1).removeFirst();
    paramList.add(localObject2);
    int i = 0;
    localObject2 = null;
    int j = 0;
    X509Certificate localX509Certificate1;
    boolean bool1;
    for (;;)
    {
      int k = 9;
      if (i >= k) {
        break label264;
      }
      k = paramList.size();
      int m = 1;
      k -= m;
      localX509Certificate1 = (X509Certificate)paramList.get(k);
      X509Certificate localX509Certificate2 = a.a(localX509Certificate1);
      if (localX509Certificate2 != null)
      {
        j = paramList.size();
        if (j <= m)
        {
          bool1 = localX509Certificate1.equals(localX509Certificate2);
          if (bool1) {}
        }
        else
        {
          paramList.add(localX509Certificate2);
        }
        bool1 = a(localX509Certificate2, localX509Certificate2);
        if (bool1) {
          return paramList;
        }
        bool1 = true;
      }
      else
      {
        Iterator localIterator = ((Deque)localObject1).iterator();
        boolean bool3;
        do
        {
          boolean bool2 = localIterator.hasNext();
          if (!bool2) {
            break;
          }
          localX509Certificate2 = (X509Certificate)localIterator.next();
          bool3 = a(localX509Certificate1, localX509Certificate2);
        } while (!bool3);
        localIterator.remove();
        paramList.add(localX509Certificate2);
      }
      i += 1;
    }
    if (bool1) {
      return paramList;
    }
    paramList = new javax/net/ssl/SSLPeerUnverifiedException;
    localObject1 = String.valueOf(localX509Certificate1);
    localObject1 = "Failed to find a trusted cert that signed ".concat((String)localObject1);
    paramList.<init>((String)localObject1);
    throw paramList;
    label264:
    localObject1 = new javax/net/ssl/SSLPeerUnverifiedException;
    paramList = String.valueOf(paramList);
    paramList = "Certificate chain too long: ".concat(paramList);
    ((SSLPeerUnverifiedException)localObject1).<init>(paramList);
    throw ((Throwable)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */