package com.d.a.a.d;

import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.security.auth.x500.X500Principal;

public final class e
  implements f
{
  private final Map a;
  
  public e(X509Certificate... paramVarArgs)
  {
    LinkedHashMap localLinkedHashMap = new java/util/LinkedHashMap;
    localLinkedHashMap.<init>();
    a = localLinkedHashMap;
    int i = paramVarArgs.length;
    int j = 0;
    while (j < i)
    {
      X509Certificate localX509Certificate = paramVarArgs[j];
      X500Principal localX500Principal = localX509Certificate.getSubjectX500Principal();
      Object localObject = (List)a.get(localX500Principal);
      if (localObject == null)
      {
        localObject = new java/util/ArrayList;
        int k = 1;
        ((ArrayList)localObject).<init>(k);
        Map localMap = a;
        localMap.put(localX500Principal, localObject);
      }
      ((List)localObject).add(localX509Certificate);
      j += 1;
    }
  }
  
  public final X509Certificate a(X509Certificate paramX509Certificate)
  {
    Object localObject = paramX509Certificate.getIssuerX500Principal();
    localObject = (List)a.get(localObject);
    if (localObject == null) {
      return null;
    }
    localObject = ((List)localObject).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject).hasNext();
      X509Certificate localX509Certificate;
      PublicKey localPublicKey;
      if (bool)
      {
        localX509Certificate = (X509Certificate)((Iterator)localObject).next();
        localPublicKey = localX509Certificate.getPublicKey();
      }
      try
      {
        paramX509Certificate.verify(localPublicKey);
        return localX509Certificate;
      }
      catch (Exception localException) {}
      return null;
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.d.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */