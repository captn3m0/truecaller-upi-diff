package com.d.a.a.d;

import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;

public final class d
  implements HostnameVerifier
{
  public static final d a;
  private static final Pattern b = Pattern.compile("([0-9a-fA-F]*:[0-9a-fA-F:.]*)|([\\d.]+)");
  
  static
  {
    d locald = new com/d/a/a/d/d;
    locald.<init>();
    a = locald;
  }
  
  public static List a(X509Certificate paramX509Certificate)
  {
    List localList = a(paramX509Certificate, 7);
    paramX509Certificate = a(paramX509Certificate, 2);
    ArrayList localArrayList = new java/util/ArrayList;
    int i = localList.size();
    int j = paramX509Certificate.size();
    i += j;
    localArrayList.<init>(i);
    localArrayList.addAll(localList);
    localArrayList.addAll(paramX509Certificate);
    return localArrayList;
  }
  
  private static List a(X509Certificate paramX509Certificate, int paramInt)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    try
    {
      paramX509Certificate = paramX509Certificate.getSubjectAlternativeNames();
      if (paramX509Certificate == null) {
        return Collections.emptyList();
      }
      paramX509Certificate = paramX509Certificate.iterator();
      for (;;)
      {
        boolean bool = paramX509Certificate.hasNext();
        if (!bool) {
          break;
        }
        Object localObject1 = paramX509Certificate.next();
        localObject1 = (List)localObject1;
        if (localObject1 != null)
        {
          int i = ((List)localObject1).size();
          int j = 2;
          if (i >= j)
          {
            i = 0;
            Object localObject2 = null;
            localObject2 = ((List)localObject1).get(0);
            localObject2 = (Integer)localObject2;
            if (localObject2 != null)
            {
              i = ((Integer)localObject2).intValue();
              if (i == paramInt)
              {
                i = 1;
                localObject1 = ((List)localObject1).get(i);
                localObject1 = (String)localObject1;
                if (localObject1 != null) {
                  localArrayList.add(localObject1);
                }
              }
            }
          }
        }
      }
      return localArrayList;
    }
    catch (CertificateParsingException localCertificateParsingException) {}
    return Collections.emptyList();
  }
  
  private static boolean a(String paramString1, String paramString2)
  {
    if (paramString1 != null)
    {
      int i = paramString1.length();
      if (i != 0)
      {
        Object localObject = ".";
        boolean bool1 = paramString1.startsWith((String)localObject);
        if (!bool1)
        {
          localObject = "..";
          bool1 = paramString1.endsWith((String)localObject);
          if (!bool1)
          {
            if (paramString2 != null)
            {
              int j = paramString2.length();
              if (j != 0)
              {
                localObject = ".";
                boolean bool2 = paramString2.startsWith((String)localObject);
                if (!bool2)
                {
                  localObject = "..";
                  bool2 = paramString2.endsWith((String)localObject);
                  if (!bool2)
                  {
                    localObject = ".";
                    bool2 = paramString1.endsWith((String)localObject);
                    char c = '.';
                    if (!bool2)
                    {
                      localObject = new java/lang/StringBuilder;
                      ((StringBuilder)localObject).<init>();
                      ((StringBuilder)localObject).append(paramString1);
                      ((StringBuilder)localObject).append(c);
                      paramString1 = ((StringBuilder)localObject).toString();
                    }
                    localObject = ".";
                    bool2 = paramString2.endsWith((String)localObject);
                    if (!bool2)
                    {
                      localObject = new java/lang/StringBuilder;
                      ((StringBuilder)localObject).<init>();
                      ((StringBuilder)localObject).append(paramString2);
                      ((StringBuilder)localObject).append(c);
                      paramString2 = ((StringBuilder)localObject).toString();
                    }
                    localObject = Locale.US;
                    paramString2 = paramString2.toLowerCase((Locale)localObject);
                    localObject = "*";
                    bool2 = paramString2.contains((CharSequence)localObject);
                    if (!bool2) {
                      return paramString1.equals(paramString2);
                    }
                    localObject = "*.";
                    bool2 = paramString2.startsWith((String)localObject);
                    if (bool2)
                    {
                      int n = 1;
                      int k = paramString2.indexOf('*', n);
                      int i1 = -1;
                      if (k == i1)
                      {
                        k = paramString1.length();
                        int i2 = paramString2.length();
                        if (k < i2) {
                          return false;
                        }
                        localObject = "*.";
                        boolean bool3 = ((String)localObject).equals(paramString2);
                        if (bool3) {
                          return false;
                        }
                        paramString2 = paramString2.substring(n);
                        bool3 = paramString1.endsWith(paramString2);
                        if (!bool3) {
                          return false;
                        }
                        int m = paramString1.length();
                        int i3 = paramString2.length();
                        m -= i3;
                        if (m > 0)
                        {
                          m -= n;
                          int i4 = paramString1.lastIndexOf(c, m);
                          if (i4 != i1) {
                            return false;
                          }
                        }
                        return n;
                      }
                    }
                    return false;
                  }
                }
              }
            }
            return false;
          }
        }
      }
    }
    return false;
  }
  
  public final boolean verify(String paramString, SSLSession paramSSLSession)
  {
    try
    {
      paramSSLSession = paramSSLSession.getPeerCertificates();
      paramSSLSession = paramSSLSession[0];
      paramSSLSession = (X509Certificate)paramSSLSession;
      Object localObject1 = b;
      localObject1 = ((Pattern)localObject1).matcher(paramString);
      boolean bool1 = ((Matcher)localObject1).matches();
      int j = 1;
      Object localObject2;
      if (bool1)
      {
        i = 7;
        paramSSLSession = a(paramSSLSession, i);
        i = paramSSLSession.size();
        k = 0;
        localObject2 = null;
        while (k < i)
        {
          localObject3 = paramSSLSession.get(k);
          localObject3 = (String)localObject3;
          n = paramString.equalsIgnoreCase((String)localObject3);
          if (n != 0) {
            return j;
          }
          k += 1;
        }
        return false;
      }
      localObject1 = Locale.US;
      paramString = paramString.toLowerCase((Locale)localObject1);
      int i = 2;
      localObject1 = a(paramSSLSession, i);
      int k = ((List)localObject1).size();
      int n = 0;
      Object localObject3 = null;
      boolean bool3 = false;
      Object localObject4 = null;
      int i1;
      while (n < k)
      {
        localObject4 = ((List)localObject1).get(n);
        localObject4 = (String)localObject4;
        bool3 = a(paramString, (String)localObject4);
        if (bool3) {
          return j;
        }
        n += 1;
        bool3 = true;
      }
      if (!bool3)
      {
        paramSSLSession = paramSSLSession.getSubjectX500Principal();
        localObject1 = new com/d/a/a/d/c;
        ((c)localObject1).<init>(paramSSLSession);
        paramSSLSession = "cn";
        c = 0;
        d = 0;
        e = 0;
        f = 0;
        localObject2 = a;
        localObject2 = ((String)localObject2).toCharArray();
        g = ((char[])localObject2);
        localObject2 = ((c)localObject1).a();
        i1 = 0;
        localObject3 = null;
        if (localObject2 != null) {}
        label796:
        label847:
        do
        {
          localObject4 = "";
          int i3 = c;
          int i4 = b;
          if (i3 != i4)
          {
            char[] arrayOfChar1 = g;
            i4 = c;
            i3 = arrayOfChar1[i4];
            switch (i3)
            {
            default: 
              localObject4 = ((c)localObject1).c();
              break;
            case 35: 
              localObject4 = ((c)localObject1).b();
              break;
            case 34: 
              i2 = c + j;
              c = i2;
              i2 = c;
              d = i2;
              i2 = d;
              for (e = i2;; e = i2)
              {
                i2 = c;
                i3 = b;
                if (i2 == i3) {
                  break;
                }
                localObject4 = g;
                i3 = c;
                i2 = localObject4[i3];
                i3 = 34;
                int i5;
                if (i2 == i3)
                {
                  i2 = c + j;
                  for (c = i2;; c = i2)
                  {
                    i2 = c;
                    i3 = b;
                    if (i2 >= i3) {
                      break;
                    }
                    localObject4 = g;
                    i3 = c;
                    i2 = localObject4[i3];
                    i3 = 32;
                    if (i2 != i3) {
                      break;
                    }
                    i2 = c + j;
                  }
                  localObject4 = new java/lang/String;
                  arrayOfChar1 = g;
                  i4 = d;
                  i5 = e;
                  int i6 = d;
                  i5 -= i6;
                  ((String)localObject4).<init>(arrayOfChar1, i4, i5);
                  break label796;
                }
                localObject4 = g;
                i3 = c;
                i2 = localObject4[i3];
                i3 = 92;
                if (i2 == i3)
                {
                  localObject4 = g;
                  i3 = e;
                  i4 = ((c)localObject1).d();
                  localObject4[i3] = i4;
                }
                else
                {
                  localObject4 = g;
                  i3 = e;
                  char[] arrayOfChar2 = g;
                  i5 = c;
                  i4 = arrayOfChar2[i5];
                  localObject4[i3] = i4;
                }
                i2 = c + j;
                c = i2;
                i2 = e + j;
              }
              paramString = new java/lang/IllegalStateException;
              paramSSLSession = new java/lang/StringBuilder;
              str = "Unexpected end of DN: ";
              paramSSLSession.<init>(str);
              localObject1 = a;
              paramSSLSession.append((String)localObject1);
              paramSSLSession = paramSSLSession.toString();
              paramString.<init>(paramSSLSession);
              throw paramString;
            }
            boolean bool2 = paramSSLSession.equalsIgnoreCase((String)localObject2);
            if (bool2)
            {
              localObject3 = localObject4;
            }
            else
            {
              m = c;
              i2 = b;
              if (m < i2) {
                break label847;
              }
            }
          }
          if (localObject3 == null) {
            break;
          }
          return a(paramString, (String)localObject3);
          localObject2 = g;
          int i2 = c;
          int m = localObject2[i2];
          i2 = 44;
          if (m != i2)
          {
            localObject2 = g;
            i2 = c;
            m = localObject2[i2];
            i2 = 59;
            if (m != i2)
            {
              localObject2 = g;
              i2 = c;
              m = localObject2[i2];
              i2 = 43;
              if (m != i2)
              {
                paramString = new java/lang/IllegalStateException;
                paramSSLSession = new java/lang/StringBuilder;
                str = "Malformed DN: ";
                paramSSLSession.<init>(str);
                localObject1 = a;
                paramSSLSession.append((String)localObject1);
                paramSSLSession = paramSSLSession.toString();
                paramString.<init>(paramSSLSession);
                throw paramString;
              }
            }
          }
          m = c + j;
          c = m;
          localObject2 = ((c)localObject1).a();
        } while (localObject2 != null);
        paramString = new java/lang/IllegalStateException;
        paramSSLSession = new java/lang/StringBuilder;
        String str = "Malformed DN: ";
        paramSSLSession.<init>(str);
        localObject1 = a;
        paramSSLSession.append((String)localObject1);
        paramSSLSession = paramSSLSession.toString();
        paramString.<init>(paramSSLSession);
        throw paramString;
      }
      return false;
    }
    catch (SSLException localSSLException) {}
    return false;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.d.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */