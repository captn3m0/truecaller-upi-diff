package com.d.a.a;

import d.h;
import d.t;
import java.io.IOException;

class c
  extends h
{
  private boolean a;
  
  public c(t paramt)
  {
    super(paramt);
  }
  
  protected void a() {}
  
  public final void a_(d.c paramc, long paramLong)
  {
    boolean bool = a;
    if (bool)
    {
      paramc.h(paramLong);
      return;
    }
    try
    {
      super.a_(paramc, paramLong);
      return;
    }
    catch (IOException localIOException)
    {
      a = true;
      a();
    }
  }
  
  public void close()
  {
    boolean bool = a;
    if (bool) {
      return;
    }
    try
    {
      super.close();
      return;
    }
    catch (IOException localIOException)
    {
      a = true;
      a();
    }
  }
  
  public void flush()
  {
    boolean bool = a;
    if (bool) {
      return;
    }
    try
    {
      super.flush();
      return;
    }
    catch (IOException localIOException)
    {
      a = true;
      a();
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */