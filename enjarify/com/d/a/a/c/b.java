package com.d.a.a.c;

import com.d.a.a;
import com.d.a.a.b.k;
import com.d.a.a.d.f;
import com.d.a.a.h;
import com.d.a.a.j;
import com.d.a.i;
import com.d.a.o;
import com.d.a.p;
import com.d.a.q;
import com.d.a.v.a;
import com.d.a.x;
import com.d.a.x.a;
import com.d.a.z;
import d.c;
import java.io.IOException;
import java.net.Proxy;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

public final class b
  implements i
{
  private static SSLSocketFactory m;
  private static f n;
  public final z a;
  public Socket b;
  public o c;
  public volatile com.d.a.a.a.d d;
  public int e;
  public d.e f;
  public d.d g;
  public final List h;
  public boolean i;
  public long j;
  private Socket k;
  private com.d.a.u l;
  
  public b(z paramz)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    h = localArrayList;
    j = Long.MAX_VALUE;
    a = paramz;
  }
  
  private static f a(SSLSocketFactory paramSSLSocketFactory)
  {
    synchronized (b.class)
    {
      Object localObject = m;
      if (paramSSLSocketFactory != localObject)
      {
        localObject = h.a();
        localObject = ((h)localObject).a(paramSSLSocketFactory);
        h localh = h.a();
        localObject = localh.a((X509TrustManager)localObject);
        n = (f)localObject;
        m = paramSSLSocketFactory;
      }
      paramSSLSocketFactory = n;
      return paramSSLSocketFactory;
    }
  }
  
  private void a(int paramInt1, int paramInt2)
  {
    Object localObject1 = new com/d/a/v$a;
    ((v.a)localObject1).<init>();
    Object localObject2 = a.a.a;
    localObject1 = ((v.a)localObject1).a((q)localObject2);
    Object localObject3 = j.a(a.a.a);
    localObject1 = ((v.a)localObject1).a("Host", (String)localObject3).a("Proxy-Connection", "Keep-Alive").a("User-Agent", "okhttp/2.7.5").a();
    localObject2 = a;
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("CONNECT ");
    Object localObject4 = b;
    ((StringBuilder)localObject3).append((String)localObject4);
    localObject4 = ":";
    ((StringBuilder)localObject3).append((String)localObject4);
    int i1 = c;
    ((StringBuilder)localObject3).append(i1);
    ((StringBuilder)localObject3).append(" HTTP/1.1");
    localObject2 = ((StringBuilder)localObject3).toString();
    do
    {
      localObject3 = new com/d/a/a/b/e;
      int i2 = 0;
      Object localObject5 = f;
      d.d locald = g;
      ((com.d.a.a.b.e)localObject3).<init>(null, (d.e)localObject5, locald);
      localObject4 = f.l_();
      long l1 = paramInt1;
      TimeUnit localTimeUnit = TimeUnit.MILLISECONDS;
      ((d.v)localObject4).a(l1, localTimeUnit);
      localObject4 = g.l_();
      l1 = paramInt2;
      localTimeUnit = TimeUnit.MILLISECONDS;
      ((d.v)localObject4).a(l1, localTimeUnit);
      localObject4 = c;
      ((com.d.a.a.b.e)localObject3).a((p)localObject4, (String)localObject2);
      ((com.d.a.a.b.e)localObject3).b();
      localObject4 = ((com.d.a.a.b.e)localObject3).c();
      a = ((com.d.a.v)localObject1);
      localObject1 = ((x.a)localObject4).a();
      long l2 = k.a((x)localObject1);
      long l3 = -1;
      boolean bool = l2 < l3;
      if (!bool) {
        l2 = 0L;
      }
      localObject3 = ((com.d.a.a.b.e)localObject3).a(l2);
      localObject5 = TimeUnit.MILLISECONDS;
      j.a((d.u)localObject3, -1 >>> 1, (TimeUnit)localObject5);
      ((d.u)localObject3).close();
      int i3 = c;
      i2 = 200;
      if (i3 == i2) {
        break label459;
      }
      i2 = 407;
      if (i3 != i2) {
        break;
      }
      localObject3 = a.a.d;
      localObject4 = a.b;
      localObject1 = k.a((com.d.a.b)localObject3, (x)localObject1, (Proxy)localObject4);
    } while (localObject1 != null);
    Object localObject6 = new java/io/IOException;
    ((IOException)localObject6).<init>("Failed to authenticate with proxy");
    throw ((Throwable)localObject6);
    localObject6 = new java/io/IOException;
    Object localObject7 = new java/lang/StringBuilder;
    ((StringBuilder)localObject7).<init>("Unexpected response code for CONNECT: ");
    int i4 = c;
    ((StringBuilder)localObject7).append(i4);
    localObject7 = ((StringBuilder)localObject7).toString();
    ((IOException)localObject6).<init>((String)localObject7);
    throw ((Throwable)localObject6);
    label459:
    localObject6 = f.b();
    paramInt1 = ((c)localObject6).e();
    if (paramInt1 != 0)
    {
      localObject6 = g.b();
      paramInt1 = ((c)localObject6).e();
      if (paramInt1 != 0) {
        return;
      }
    }
    localObject6 = new java/io/IOException;
    ((IOException)localObject6).<init>("TLS tunnel buffered too many bytes!");
    throw ((Throwable)localObject6);
  }
  
  public final z a()
  {
    return a;
  }
  
  /* Error */
  public final void a(int paramInt1, int paramInt2, int paramInt3, List paramList, boolean paramBoolean)
  {
    // Byte code:
    //   0: aload_0
    //   1: astore 6
    //   3: iload_2
    //   4: istore 7
    //   6: aload 4
    //   8: astore 8
    //   10: aload_0
    //   11: getfield 230	com/d/a/a/c/b:l	Lcom/d/a/u;
    //   14: astore 9
    //   16: aload 9
    //   18: ifnonnull +1776 -> 1794
    //   21: new 232	com/d/a/a/a
    //   24: astore 9
    //   26: aload 9
    //   28: aload 4
    //   30: invokespecial 235	com/d/a/a/a:<init>	(Ljava/util/List;)V
    //   33: aload_0
    //   34: getfield 48	com/d/a/a/c/b:a	Lcom/d/a/z;
    //   37: getfield 207	com/d/a/z:b	Ljava/net/Proxy;
    //   40: astore 10
    //   42: aload_0
    //   43: getfield 48	com/d/a/a/c/b:a	Lcom/d/a/z;
    //   46: getfield 71	com/d/a/z:a	Lcom/d/a/a;
    //   49: astore 11
    //   51: aload_0
    //   52: getfield 48	com/d/a/a/c/b:a	Lcom/d/a/z;
    //   55: getfield 71	com/d/a/z:a	Lcom/d/a/a;
    //   58: getfield 237	com/d/a/a:i	Ljavax/net/ssl/SSLSocketFactory;
    //   61: astore 12
    //   63: aload 12
    //   65: ifnonnull +71 -> 136
    //   68: getstatic 242	com/d/a/k:c	Lcom/d/a/k;
    //   71: astore 12
    //   73: aload 4
    //   75: aload 12
    //   77: invokeinterface 248 2 0
    //   82: istore 13
    //   84: iload 13
    //   86: ifeq +6 -> 92
    //   89: goto +47 -> 136
    //   92: new 250	com/d/a/a/b/p
    //   95: astore 14
    //   97: new 252	java/net/UnknownServiceException
    //   100: astore 9
    //   102: aload 4
    //   104: invokestatic 258	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   107: astore 8
    //   109: ldc_w 260
    //   112: aload 8
    //   114: invokevirtual 264	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   117: astore 8
    //   119: aload 9
    //   121: aload 8
    //   123: invokespecial 265	java/net/UnknownServiceException:<init>	(Ljava/lang/String;)V
    //   126: aload 14
    //   128: aload 9
    //   130: invokespecial 268	com/d/a/a/b/p:<init>	(Ljava/io/IOException;)V
    //   133: aload 14
    //   135: athrow
    //   136: aconst_null
    //   137: astore 15
    //   139: aload 6
    //   141: getfield 230	com/d/a/a/c/b:l	Lcom/d/a/u;
    //   144: astore 8
    //   146: aload 8
    //   148: ifnonnull +1645 -> 1793
    //   151: iconst_1
    //   152: istore 16
    //   154: aload 10
    //   156: invokevirtual 275	java/net/Proxy:type	()Ljava/net/Proxy$Type;
    //   159: astore 8
    //   161: getstatic 281	java/net/Proxy$Type:DIRECT	Ljava/net/Proxy$Type;
    //   164: astore 17
    //   166: aload 8
    //   168: aload 17
    //   170: if_acmpeq +40 -> 210
    //   173: aload 10
    //   175: invokevirtual 275	java/net/Proxy:type	()Ljava/net/Proxy$Type;
    //   178: astore 8
    //   180: getstatic 284	java/net/Proxy$Type:HTTP	Ljava/net/Proxy$Type;
    //   183: astore 17
    //   185: aload 8
    //   187: aload 17
    //   189: if_acmpne +6 -> 195
    //   192: goto +18 -> 210
    //   195: new 286	java/net/Socket
    //   198: astore 8
    //   200: aload 8
    //   202: aload 10
    //   204: invokespecial 289	java/net/Socket:<init>	(Ljava/net/Proxy;)V
    //   207: goto +17 -> 224
    //   210: aload 11
    //   212: getfield 292	com/d/a/a:c	Ljavax/net/SocketFactory;
    //   215: astore 8
    //   217: aload 8
    //   219: invokevirtual 298	javax/net/SocketFactory:createSocket	()Ljava/net/Socket;
    //   222: astore 8
    //   224: aload 6
    //   226: aload 8
    //   228: putfield 300	com/d/a/a/c/b:k	Ljava/net/Socket;
    //   231: aload 6
    //   233: getfield 300	com/d/a/a/c/b:k	Ljava/net/Socket;
    //   236: astore 8
    //   238: aload 8
    //   240: iload 7
    //   242: invokevirtual 304	java/net/Socket:setSoTimeout	(I)V
    //   245: invokestatic 55	com/d/a/a/h:a	()Lcom/d/a/a/h;
    //   248: astore 8
    //   250: aload 6
    //   252: getfield 300	com/d/a/a/c/b:k	Ljava/net/Socket;
    //   255: astore 17
    //   257: aload 6
    //   259: getfield 48	com/d/a/a/c/b:a	Lcom/d/a/z;
    //   262: astore 18
    //   264: aload 18
    //   266: getfield 307	com/d/a/z:c	Ljava/net/InetSocketAddress;
    //   269: astore 18
    //   271: aload 8
    //   273: aload 17
    //   275: aload 18
    //   277: iload_1
    //   278: invokevirtual 310	com/d/a/a/h:a	(Ljava/net/Socket;Ljava/net/InetSocketAddress;I)V
    //   281: aload 6
    //   283: getfield 300	com/d/a/a/c/b:k	Ljava/net/Socket;
    //   286: astore 8
    //   288: aload 8
    //   290: invokestatic 315	d/n:b	(Ljava/net/Socket;)Ld/u;
    //   293: astore 8
    //   295: aload 8
    //   297: invokestatic 318	d/n:a	(Ld/u;)Ld/e;
    //   300: astore 8
    //   302: aload 6
    //   304: aload 8
    //   306: putfield 136	com/d/a/a/c/b:f	Ld/e;
    //   309: aload 6
    //   311: getfield 300	com/d/a/a/c/b:k	Ljava/net/Socket;
    //   314: astore 8
    //   316: aload 8
    //   318: invokestatic 321	d/n:a	(Ljava/net/Socket;)Ld/t;
    //   321: astore 8
    //   323: aload 8
    //   325: invokestatic 324	d/n:a	(Ld/t;)Ld/d;
    //   328: astore 8
    //   330: aload 6
    //   332: aload 8
    //   334: putfield 138	com/d/a/a/c/b:g	Ld/d;
    //   337: aload 6
    //   339: getfield 48	com/d/a/a/c/b:a	Lcom/d/a/z;
    //   342: astore 8
    //   344: aload 8
    //   346: getfield 71	com/d/a/z:a	Lcom/d/a/a;
    //   349: astore 8
    //   351: aload 8
    //   353: getfield 237	com/d/a/a:i	Ljavax/net/ssl/SSLSocketFactory;
    //   356: astore 8
    //   358: aload 8
    //   360: ifnull +799 -> 1159
    //   363: aload 6
    //   365: getfield 48	com/d/a/a/c/b:a	Lcom/d/a/z;
    //   368: astore 8
    //   370: aload 8
    //   372: getfield 71	com/d/a/z:a	Lcom/d/a/a;
    //   375: astore 17
    //   377: aload 17
    //   379: getfield 237	com/d/a/a:i	Ljavax/net/ssl/SSLSocketFactory;
    //   382: astore 17
    //   384: aload 17
    //   386: ifnull +35 -> 421
    //   389: aload 8
    //   391: getfield 207	com/d/a/z:b	Ljava/net/Proxy;
    //   394: astore 8
    //   396: aload 8
    //   398: invokevirtual 275	java/net/Proxy:type	()Ljava/net/Proxy$Type;
    //   401: astore 8
    //   403: getstatic 284	java/net/Proxy$Type:HTTP	Ljava/net/Proxy$Type;
    //   406: astore 17
    //   408: aload 8
    //   410: aload 17
    //   412: if_acmpne +9 -> 421
    //   415: iconst_1
    //   416: istore 19
    //   418: goto +9 -> 427
    //   421: iconst_0
    //   422: istore 19
    //   424: aconst_null
    //   425: astore 8
    //   427: iload 19
    //   429: ifeq +14 -> 443
    //   432: aload 6
    //   434: iload 7
    //   436: iload_3
    //   437: invokespecial 327	com/d/a/a/c/b:a	(II)V
    //   440: goto +3 -> 443
    //   443: aload 6
    //   445: getfield 48	com/d/a/a/c/b:a	Lcom/d/a/z;
    //   448: astore 8
    //   450: aload 8
    //   452: getfield 71	com/d/a/z:a	Lcom/d/a/a;
    //   455: astore 8
    //   457: aload 8
    //   459: getfield 237	com/d/a/a:i	Ljavax/net/ssl/SSLSocketFactory;
    //   462: astore 18
    //   464: aload 6
    //   466: getfield 300	com/d/a/a/c/b:k	Ljava/net/Socket;
    //   469: astore 20
    //   471: aload 8
    //   473: getfield 76	com/d/a/a:a	Lcom/d/a/q;
    //   476: astore 21
    //   478: aload 21
    //   480: getfield 115	com/d/a/q:b	Ljava/lang/String;
    //   483: astore 21
    //   485: aload 8
    //   487: getfield 76	com/d/a/a:a	Lcom/d/a/q;
    //   490: astore 22
    //   492: aload 22
    //   494: getfield 123	com/d/a/q:c	I
    //   497: istore 23
    //   499: aload 18
    //   501: aload 20
    //   503: aload 21
    //   505: iload 23
    //   507: iload 16
    //   509: invokevirtual 332	javax/net/ssl/SSLSocketFactory:createSocket	(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;
    //   512: astore 18
    //   514: aload 18
    //   516: checkcast 334	javax/net/ssl/SSLSocket
    //   519: astore 18
    //   521: aload 9
    //   523: aload 18
    //   525: invokevirtual 337	com/d/a/a/a:a	(Ljavax/net/ssl/SSLSocket;)Lcom/d/a/k;
    //   528: astore 20
    //   530: aload 20
    //   532: getfield 339	com/d/a/k:e	Z
    //   535: istore 24
    //   537: iload 24
    //   539: ifeq +40 -> 579
    //   542: invokestatic 55	com/d/a/a/h:a	()Lcom/d/a/a/h;
    //   545: astore 21
    //   547: aload 8
    //   549: getfield 76	com/d/a/a:a	Lcom/d/a/q;
    //   552: astore 22
    //   554: aload 22
    //   556: getfield 115	com/d/a/q:b	Ljava/lang/String;
    //   559: astore 22
    //   561: aload 8
    //   563: getfield 341	com/d/a/a:e	Ljava/util/List;
    //   566: astore 25
    //   568: aload 21
    //   570: aload 18
    //   572: aload 22
    //   574: aload 25
    //   576: invokevirtual 344	com/d/a/a/h:a	(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;Ljava/util/List;)V
    //   579: aload 18
    //   581: invokevirtual 347	javax/net/ssl/SSLSocket:startHandshake	()V
    //   584: aload 18
    //   586: invokevirtual 351	javax/net/ssl/SSLSocket:getSession	()Ljavax/net/ssl/SSLSession;
    //   589: astore 25
    //   591: aload 25
    //   593: invokestatic 356	com/d/a/o:a	(Ljavax/net/ssl/SSLSession;)Lcom/d/a/o;
    //   596: astore 25
    //   598: aload 8
    //   600: getfield 359	com/d/a/a:j	Ljavax/net/ssl/HostnameVerifier;
    //   603: astore 21
    //   605: aload 8
    //   607: getfield 76	com/d/a/a:a	Lcom/d/a/q;
    //   610: astore 22
    //   612: aload 22
    //   614: getfield 115	com/d/a/q:b	Ljava/lang/String;
    //   617: astore 22
    //   619: aload 18
    //   621: invokevirtual 351	javax/net/ssl/SSLSocket:getSession	()Ljavax/net/ssl/SSLSession;
    //   624: astore 12
    //   626: aload 21
    //   628: aload 22
    //   630: aload 12
    //   632: invokeinterface 365 3 0
    //   637: istore 13
    //   639: iload 13
    //   641: ifeq +246 -> 887
    //   644: aload 8
    //   646: getfield 368	com/d/a/a:k	Lcom/d/a/f;
    //   649: astore 12
    //   651: getstatic 372	com/d/a/f:a	Lcom/d/a/f;
    //   654: astore 21
    //   656: aload 12
    //   658: aload 21
    //   660: if_acmpeq +75 -> 735
    //   663: aload 8
    //   665: getfield 237	com/d/a/a:i	Ljavax/net/ssl/SSLSocketFactory;
    //   668: astore 12
    //   670: aload 12
    //   672: invokestatic 375	com/d/a/a/c/b:a	(Ljavax/net/ssl/SSLSocketFactory;)Lcom/d/a/a/d/f;
    //   675: astore 12
    //   677: new 377	com/d/a/a/d/b
    //   680: astore 21
    //   682: aload 21
    //   684: aload 12
    //   686: invokespecial 380	com/d/a/a/d/b:<init>	(Lcom/d/a/a/d/f;)V
    //   689: aload 25
    //   691: getfield 382	com/d/a/o:b	Ljava/util/List;
    //   694: astore 12
    //   696: aload 21
    //   698: aload 12
    //   700: invokevirtual 385	com/d/a/a/d/b:a	(Ljava/util/List;)Ljava/util/List;
    //   703: astore 12
    //   705: aload 8
    //   707: getfield 368	com/d/a/a:k	Lcom/d/a/f;
    //   710: astore 21
    //   712: aload 8
    //   714: getfield 76	com/d/a/a:a	Lcom/d/a/q;
    //   717: astore 8
    //   719: aload 8
    //   721: getfield 115	com/d/a/q:b	Ljava/lang/String;
    //   724: astore 8
    //   726: aload 21
    //   728: aload 8
    //   730: aload 12
    //   732: invokevirtual 388	com/d/a/f:a	(Ljava/lang/String;Ljava/util/List;)V
    //   735: aload 20
    //   737: getfield 339	com/d/a/k:e	Z
    //   740: istore 19
    //   742: iload 19
    //   744: ifeq +20 -> 764
    //   747: invokestatic 55	com/d/a/a/h:a	()Lcom/d/a/a/h;
    //   750: astore 8
    //   752: aload 8
    //   754: aload 18
    //   756: invokevirtual 391	com/d/a/a/h:b	(Ljavax/net/ssl/SSLSocket;)Ljava/lang/String;
    //   759: astore 12
    //   761: goto +9 -> 770
    //   764: iconst_0
    //   765: istore 13
    //   767: aconst_null
    //   768: astore 12
    //   770: aload 6
    //   772: aload 18
    //   774: putfield 393	com/d/a/a/c/b:b	Ljava/net/Socket;
    //   777: aload 6
    //   779: getfield 393	com/d/a/a/c/b:b	Ljava/net/Socket;
    //   782: astore 8
    //   784: aload 8
    //   786: invokestatic 315	d/n:b	(Ljava/net/Socket;)Ld/u;
    //   789: astore 8
    //   791: aload 8
    //   793: invokestatic 318	d/n:a	(Ld/u;)Ld/e;
    //   796: astore 8
    //   798: aload 6
    //   800: aload 8
    //   802: putfield 136	com/d/a/a/c/b:f	Ld/e;
    //   805: aload 6
    //   807: getfield 393	com/d/a/a/c/b:b	Ljava/net/Socket;
    //   810: astore 8
    //   812: aload 8
    //   814: invokestatic 321	d/n:a	(Ljava/net/Socket;)Ld/t;
    //   817: astore 8
    //   819: aload 8
    //   821: invokestatic 324	d/n:a	(Ld/t;)Ld/d;
    //   824: astore 8
    //   826: aload 6
    //   828: aload 8
    //   830: putfield 138	com/d/a/a/c/b:g	Ld/d;
    //   833: aload 6
    //   835: aload 25
    //   837: putfield 395	com/d/a/a/c/b:c	Lcom/d/a/o;
    //   840: aload 12
    //   842: ifnull +13 -> 855
    //   845: aload 12
    //   847: invokestatic 400	com/d/a/u:a	(Ljava/lang/String;)Lcom/d/a/u;
    //   850: astore 8
    //   852: goto +8 -> 860
    //   855: getstatic 402	com/d/a/u:b	Lcom/d/a/u;
    //   858: astore 8
    //   860: aload 6
    //   862: aload 8
    //   864: putfield 230	com/d/a/a/c/b:l	Lcom/d/a/u;
    //   867: aload 18
    //   869: ifnull +316 -> 1185
    //   872: invokestatic 55	com/d/a/a/h:a	()Lcom/d/a/a/h;
    //   875: astore 8
    //   877: aload 8
    //   879: aload 18
    //   881: invokevirtual 405	com/d/a/a/h:a	(Ljavax/net/ssl/SSLSocket;)V
    //   884: goto +301 -> 1185
    //   887: aload 25
    //   889: getfield 382	com/d/a/o:b	Ljava/util/List;
    //   892: astore 12
    //   894: aload 12
    //   896: iconst_0
    //   897: invokeinterface 409 2 0
    //   902: astore 12
    //   904: aload 12
    //   906: checkcast 411	java/security/cert/X509Certificate
    //   909: astore 12
    //   911: new 413	javax/net/ssl/SSLPeerUnverifiedException
    //   914: astore 25
    //   916: new 105	java/lang/StringBuilder
    //   919: astore 20
    //   921: ldc_w 415
    //   924: astore 21
    //   926: aload 20
    //   928: aload 21
    //   930: invokespecial 110	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   933: aload 8
    //   935: getfield 76	com/d/a/a:a	Lcom/d/a/q;
    //   938: astore 8
    //   940: aload 8
    //   942: getfield 115	com/d/a/q:b	Ljava/lang/String;
    //   945: astore 8
    //   947: aload 20
    //   949: aload 8
    //   951: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   954: pop
    //   955: ldc_w 417
    //   958: astore 8
    //   960: aload 20
    //   962: aload 8
    //   964: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   967: pop
    //   968: aload 12
    //   970: invokestatic 420	com/d/a/f:a	(Ljava/security/cert/Certificate;)Ljava/lang/String;
    //   973: astore 8
    //   975: aload 20
    //   977: aload 8
    //   979: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   982: pop
    //   983: ldc_w 422
    //   986: astore 8
    //   988: aload 20
    //   990: aload 8
    //   992: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   995: pop
    //   996: aload 12
    //   998: invokevirtual 426	java/security/cert/X509Certificate:getSubjectDN	()Ljava/security/Principal;
    //   1001: astore 8
    //   1003: aload 8
    //   1005: invokeinterface 431 1 0
    //   1010: astore 8
    //   1012: aload 20
    //   1014: aload 8
    //   1016: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1019: pop
    //   1020: ldc_w 433
    //   1023: astore 8
    //   1025: aload 20
    //   1027: aload 8
    //   1029: invokevirtual 119	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1032: pop
    //   1033: aload 12
    //   1035: invokestatic 438	com/d/a/a/d/d:a	(Ljava/security/cert/X509Certificate;)Ljava/util/List;
    //   1038: astore 8
    //   1040: aload 20
    //   1042: aload 8
    //   1044: invokevirtual 441	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1047: pop
    //   1048: aload 20
    //   1050: invokevirtual 132	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1053: astore 8
    //   1055: aload 25
    //   1057: aload 8
    //   1059: invokespecial 442	javax/net/ssl/SSLPeerUnverifiedException:<init>	(Ljava/lang/String;)V
    //   1062: aload 25
    //   1064: athrow
    //   1065: astore 8
    //   1067: goto +67 -> 1134
    //   1070: astore 8
    //   1072: aload 18
    //   1074: astore 12
    //   1076: goto +22 -> 1098
    //   1079: astore 8
    //   1081: iconst_0
    //   1082: istore 26
    //   1084: aconst_null
    //   1085: astore 18
    //   1087: goto +47 -> 1134
    //   1090: astore 8
    //   1092: iconst_0
    //   1093: istore 13
    //   1095: aconst_null
    //   1096: astore 12
    //   1098: aload 8
    //   1100: invokestatic 445	com/d/a/a/j:a	(Ljava/lang/AssertionError;)Z
    //   1103: istore 16
    //   1105: iload 16
    //   1107: ifeq +18 -> 1125
    //   1110: new 212	java/io/IOException
    //   1113: astore 25
    //   1115: aload 25
    //   1117: aload 8
    //   1119: invokespecial 448	java/io/IOException:<init>	(Ljava/lang/Throwable;)V
    //   1122: aload 25
    //   1124: athrow
    //   1125: aload 8
    //   1127: athrow
    //   1128: astore 8
    //   1130: aload 12
    //   1132: astore 18
    //   1134: aload 18
    //   1136: ifnull +15 -> 1151
    //   1139: invokestatic 55	com/d/a/a/h:a	()Lcom/d/a/a/h;
    //   1142: astore 12
    //   1144: aload 12
    //   1146: aload 18
    //   1148: invokevirtual 405	com/d/a/a/h:a	(Ljavax/net/ssl/SSLSocket;)V
    //   1151: aload 18
    //   1153: invokestatic 451	com/d/a/a/j:a	(Ljava/net/Socket;)V
    //   1156: aload 8
    //   1158: athrow
    //   1159: getstatic 402	com/d/a/u:b	Lcom/d/a/u;
    //   1162: astore 8
    //   1164: aload 6
    //   1166: aload 8
    //   1168: putfield 230	com/d/a/a/c/b:l	Lcom/d/a/u;
    //   1171: aload 6
    //   1173: getfield 300	com/d/a/a/c/b:k	Ljava/net/Socket;
    //   1176: astore 8
    //   1178: aload 6
    //   1180: aload 8
    //   1182: putfield 393	com/d/a/a/c/b:b	Ljava/net/Socket;
    //   1185: aload 6
    //   1187: getfield 230	com/d/a/a/c/b:l	Lcom/d/a/u;
    //   1190: astore 8
    //   1192: getstatic 453	com/d/a/u:c	Lcom/d/a/u;
    //   1195: astore 12
    //   1197: aload 8
    //   1199: aload 12
    //   1201: if_acmpeq +22 -> 1223
    //   1204: aload 6
    //   1206: getfield 230	com/d/a/a/c/b:l	Lcom/d/a/u;
    //   1209: astore 8
    //   1211: getstatic 455	com/d/a/u:d	Lcom/d/a/u;
    //   1214: astore 12
    //   1216: aload 8
    //   1218: aload 12
    //   1220: if_acmpne -1081 -> 139
    //   1223: aload 6
    //   1225: getfield 393	com/d/a/a/c/b:b	Ljava/net/Socket;
    //   1228: astore 8
    //   1230: aload 8
    //   1232: iconst_0
    //   1233: invokevirtual 304	java/net/Socket:setSoTimeout	(I)V
    //   1236: new 457	com/d/a/a/a/d$a
    //   1239: astore 8
    //   1241: aload 8
    //   1243: invokespecial 458	com/d/a/a/a/d$a:<init>	()V
    //   1246: aload 6
    //   1248: getfield 393	com/d/a/a/c/b:b	Ljava/net/Socket;
    //   1251: astore 12
    //   1253: aload 6
    //   1255: getfield 48	com/d/a/a/c/b:a	Lcom/d/a/z;
    //   1258: astore 25
    //   1260: aload 25
    //   1262: getfield 71	com/d/a/z:a	Lcom/d/a/a;
    //   1265: astore 25
    //   1267: aload 25
    //   1269: getfield 76	com/d/a/a:a	Lcom/d/a/q;
    //   1272: astore 25
    //   1274: aload 25
    //   1276: getfield 115	com/d/a/q:b	Ljava/lang/String;
    //   1279: astore 25
    //   1281: aload 6
    //   1283: getfield 136	com/d/a/a/c/b:f	Ld/e;
    //   1286: astore 18
    //   1288: aload 6
    //   1290: getfield 138	com/d/a/a/c/b:g	Ld/d;
    //   1293: astore 20
    //   1295: aload 8
    //   1297: aload 12
    //   1299: putfield 460	com/d/a/a/a/d$a:a	Ljava/net/Socket;
    //   1302: aload 8
    //   1304: aload 25
    //   1306: putfield 461	com/d/a/a/a/d$a:b	Ljava/lang/String;
    //   1309: aload 8
    //   1311: aload 18
    //   1313: putfield 463	com/d/a/a/a/d$a:c	Ld/e;
    //   1316: aload 8
    //   1318: aload 20
    //   1320: putfield 465	com/d/a/a/a/d$a:d	Ld/d;
    //   1323: aload 6
    //   1325: getfield 230	com/d/a/a/c/b:l	Lcom/d/a/u;
    //   1328: astore 12
    //   1330: aload 8
    //   1332: aload 12
    //   1334: putfield 467	com/d/a/a/a/d$a:f	Lcom/d/a/u;
    //   1337: new 469	com/d/a/a/a/d
    //   1340: astore 12
    //   1342: aload 12
    //   1344: aload 8
    //   1346: iconst_0
    //   1347: invokespecial 472	com/d/a/a/a/d:<init>	(Lcom/d/a/a/a/d$a;B)V
    //   1350: aload 12
    //   1352: getfield 475	com/d/a/a/a/d:i	Lcom/d/a/a/a/c;
    //   1355: astore 8
    //   1357: aload 8
    //   1359: invokeinterface 479 1 0
    //   1364: aload 12
    //   1366: getfield 475	com/d/a/a/a/d:i	Lcom/d/a/a/a/c;
    //   1369: astore 8
    //   1371: aload 12
    //   1373: getfield 482	com/d/a/a/a/d:e	Lcom/d/a/a/a/n;
    //   1376: astore 25
    //   1378: aload 8
    //   1380: aload 25
    //   1382: invokeinterface 485 2 0
    //   1387: aload 12
    //   1389: getfield 482	com/d/a/a/a/d:e	Lcom/d/a/a/a/n;
    //   1392: astore 8
    //   1394: aload 8
    //   1396: invokevirtual 490	com/d/a/a/a/n:b	()I
    //   1399: istore 19
    //   1401: ldc_w 491
    //   1404: istore 16
    //   1406: iload 19
    //   1408: iload 16
    //   1410: if_icmpeq +32 -> 1442
    //   1413: aload 12
    //   1415: getfield 475	com/d/a/a/a/d:i	Lcom/d/a/a/a/c;
    //   1418: astore 18
    //   1420: iload 19
    //   1422: iload 16
    //   1424: isub
    //   1425: istore 19
    //   1427: iload 19
    //   1429: i2l
    //   1430: lstore 27
    //   1432: aload 18
    //   1434: iconst_0
    //   1435: lload 27
    //   1437: invokeinterface 495 4 0
    //   1442: aload 6
    //   1444: aload 12
    //   1446: putfield 497	com/d/a/a/c/b:d	Lcom/d/a/a/a/d;
    //   1449: goto -1310 -> 139
    //   1452: astore 8
    //   1454: goto +72 -> 1526
    //   1457: pop
    //   1458: new 499	java/net/ConnectException
    //   1461: astore 8
    //   1463: new 105	java/lang/StringBuilder
    //   1466: astore 12
    //   1468: ldc_w 501
    //   1471: astore 25
    //   1473: aload 12
    //   1475: aload 25
    //   1477: invokespecial 110	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1480: aload 6
    //   1482: getfield 48	com/d/a/a/c/b:a	Lcom/d/a/z;
    //   1485: astore 25
    //   1487: aload 25
    //   1489: getfield 307	com/d/a/z:c	Ljava/net/InetSocketAddress;
    //   1492: astore 25
    //   1494: aload 12
    //   1496: aload 25
    //   1498: invokevirtual 441	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1501: pop
    //   1502: aload 12
    //   1504: invokevirtual 132	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1507: astore 12
    //   1509: aload 8
    //   1511: aload 12
    //   1513: invokespecial 502	java/net/ConnectException:<init>	(Ljava/lang/String;)V
    //   1516: aload 8
    //   1518: athrow
    //   1519: astore 8
    //   1521: goto +5 -> 1526
    //   1524: astore 8
    //   1526: aload 6
    //   1528: getfield 393	com/d/a/a/c/b:b	Ljava/net/Socket;
    //   1531: invokestatic 451	com/d/a/a/j:a	(Ljava/net/Socket;)V
    //   1534: aload 6
    //   1536: getfield 300	com/d/a/a/c/b:k	Ljava/net/Socket;
    //   1539: invokestatic 451	com/d/a/a/j:a	(Ljava/net/Socket;)V
    //   1542: iconst_0
    //   1543: istore 13
    //   1545: aconst_null
    //   1546: astore 12
    //   1548: aload 6
    //   1550: aconst_null
    //   1551: putfield 393	com/d/a/a/c/b:b	Ljava/net/Socket;
    //   1554: aload 6
    //   1556: aconst_null
    //   1557: putfield 300	com/d/a/a/c/b:k	Ljava/net/Socket;
    //   1560: aload 6
    //   1562: aconst_null
    //   1563: putfield 136	com/d/a/a/c/b:f	Ld/e;
    //   1566: aload 6
    //   1568: aconst_null
    //   1569: putfield 138	com/d/a/a/c/b:g	Ld/d;
    //   1572: aload 6
    //   1574: aconst_null
    //   1575: putfield 395	com/d/a/a/c/b:c	Lcom/d/a/o;
    //   1578: aload 6
    //   1580: aconst_null
    //   1581: putfield 230	com/d/a/a/c/b:l	Lcom/d/a/u;
    //   1584: aload 15
    //   1586: ifnonnull +18 -> 1604
    //   1589: new 250	com/d/a/a/b/p
    //   1592: astore 15
    //   1594: aload 15
    //   1596: aload 8
    //   1598: invokespecial 268	com/d/a/a/b/p:<init>	(Ljava/io/IOException;)V
    //   1601: goto +58 -> 1659
    //   1604: aload 15
    //   1606: getfield 505	com/d/a/a/b/p:b	Ljava/io/IOException;
    //   1609: astore 25
    //   1611: getstatic 508	com/d/a/a/b/p:a	Ljava/lang/reflect/Method;
    //   1614: astore 18
    //   1616: aload 18
    //   1618: ifnull +34 -> 1652
    //   1621: getstatic 508	com/d/a/a/b/p:a	Ljava/lang/reflect/Method;
    //   1624: astore 18
    //   1626: iconst_1
    //   1627: istore 29
    //   1629: iload 29
    //   1631: anewarray 4	java/lang/Object
    //   1634: astore 21
    //   1636: aload 21
    //   1638: iconst_0
    //   1639: aload 25
    //   1641: aastore
    //   1642: aload 18
    //   1644: aload 8
    //   1646: aload 21
    //   1648: invokevirtual 514	java/lang/reflect/Method:invoke	(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    //   1651: pop
    //   1652: aload 15
    //   1654: aload 8
    //   1656: putfield 505	com/d/a/a/b/p:b	Ljava/io/IOException;
    //   1659: iload 5
    //   1661: ifeq +129 -> 1790
    //   1664: iconst_1
    //   1665: istore 29
    //   1667: aload 9
    //   1669: iload 29
    //   1671: putfield 516	com/d/a/a/a:b	Z
    //   1674: aload 9
    //   1676: getfield 518	com/d/a/a/a:a	Z
    //   1679: istore 16
    //   1681: iload 16
    //   1683: ifeq +93 -> 1776
    //   1686: aload 8
    //   1688: instanceof 520
    //   1691: istore 16
    //   1693: iload 16
    //   1695: ifne +81 -> 1776
    //   1698: aload 8
    //   1700: instanceof 522
    //   1703: istore 16
    //   1705: iload 16
    //   1707: ifne +69 -> 1776
    //   1710: aload 8
    //   1712: instanceof 524
    //   1715: istore 16
    //   1717: iload 16
    //   1719: ifeq +25 -> 1744
    //   1722: aload 8
    //   1724: checkcast 212	java/io/IOException
    //   1727: invokevirtual 528	java/io/IOException:getCause	()Ljava/lang/Throwable;
    //   1730: astore 18
    //   1732: aload 18
    //   1734: instanceof 530
    //   1737: istore 26
    //   1739: iload 26
    //   1741: ifne +35 -> 1776
    //   1744: aload 8
    //   1746: instanceof 413
    //   1749: istore 26
    //   1751: iload 26
    //   1753: ifne +23 -> 1776
    //   1756: iload 16
    //   1758: ifne +24 -> 1782
    //   1761: aload 8
    //   1763: instanceof 532
    //   1766: istore 19
    //   1768: iload 19
    //   1770: ifeq +6 -> 1776
    //   1773: goto +9 -> 1782
    //   1776: iconst_0
    //   1777: istore 29
    //   1779: aconst_null
    //   1780: astore 20
    //   1782: iload 29
    //   1784: ifeq +6 -> 1790
    //   1787: goto -1648 -> 139
    //   1790: aload 15
    //   1792: athrow
    //   1793: return
    //   1794: new 534	java/lang/IllegalStateException
    //   1797: astore 8
    //   1799: aload 8
    //   1801: ldc_w 536
    //   1804: invokespecial 537	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   1807: aload 8
    //   1809: athrow
    //   1810: pop
    //   1811: goto -353 -> 1458
    //   1814: pop
    //   1815: goto -163 -> 1652
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1818	0	this	b
    //   0	1818	1	paramInt1	int
    //   0	1818	2	paramInt2	int
    //   0	1818	3	paramInt3	int
    //   0	1818	4	paramList	List
    //   0	1818	5	paramBoolean	boolean
    //   1	1578	6	localb	b
    //   4	431	7	i1	int
    //   8	1050	8	localObject1	Object
    //   1065	1	8	localObject2	Object
    //   1070	1	8	localAssertionError1	AssertionError
    //   1079	1	8	localObject3	Object
    //   1090	36	8	localAssertionError2	AssertionError
    //   1128	29	8	localObject4	Object
    //   1162	233	8	localObject5	Object
    //   1452	1	8	localIOException1	IOException
    //   1461	56	8	localConnectException1	java.net.ConnectException
    //   1519	1	8	localIOException2	IOException
    //   1524	238	8	localIOException3	IOException
    //   1797	11	8	localIllegalStateException	IllegalStateException
    //   14	1661	9	localObject6	Object
    //   40	163	10	localProxy	Proxy
    //   49	162	11	locala	a
    //   61	1486	12	localObject7	Object
    //   82	1462	13	bool1	boolean
    //   95	39	14	localp1	com.d.a.a.b.p
    //   137	1654	15	localp2	com.d.a.a.b.p
    //   152	954	16	bool2	boolean
    //   1404	21	16	i2	int
    //   1679	78	16	bool3	boolean
    //   164	247	17	localObject8	Object
    //   262	1471	18	localObject9	Object
    //   416	327	19	bool4	boolean
    //   1399	29	19	i3	int
    //   1766	3	19	bool5	boolean
    //   469	1312	20	localObject10	Object
    //   476	1171	21	localObject11	Object
    //   490	139	22	localObject12	Object
    //   497	9	23	i4	int
    //   535	3	24	bool6	boolean
    //   566	1074	25	localObject13	Object
    //   1082	670	26	bool7	boolean
    //   1430	6	27	l1	long
    //   1627	156	29	bool8	boolean
    //   1457	1	44	localConnectException2	java.net.ConnectException
    //   1810	1	45	localConnectException3	java.net.ConnectException
    //   1814	1	46	localInvocationTargetException	java.lang.reflect.InvocationTargetException
    // Exception table:
    //   from	to	target	type
    //   523	528	1065	finally
    //   530	535	1065	finally
    //   542	545	1065	finally
    //   547	552	1065	finally
    //   554	559	1065	finally
    //   561	566	1065	finally
    //   574	579	1065	finally
    //   579	584	1065	finally
    //   584	589	1065	finally
    //   591	596	1065	finally
    //   598	603	1065	finally
    //   605	610	1065	finally
    //   612	617	1065	finally
    //   619	624	1065	finally
    //   630	637	1065	finally
    //   644	649	1065	finally
    //   651	654	1065	finally
    //   663	668	1065	finally
    //   670	675	1065	finally
    //   677	680	1065	finally
    //   684	689	1065	finally
    //   689	694	1065	finally
    //   698	703	1065	finally
    //   705	710	1065	finally
    //   712	717	1065	finally
    //   719	724	1065	finally
    //   730	735	1065	finally
    //   735	740	1065	finally
    //   747	750	1065	finally
    //   754	759	1065	finally
    //   772	777	1065	finally
    //   777	782	1065	finally
    //   784	789	1065	finally
    //   791	796	1065	finally
    //   800	805	1065	finally
    //   805	810	1065	finally
    //   812	817	1065	finally
    //   819	824	1065	finally
    //   828	833	1065	finally
    //   835	840	1065	finally
    //   845	850	1065	finally
    //   855	858	1065	finally
    //   862	867	1065	finally
    //   887	892	1065	finally
    //   896	902	1065	finally
    //   904	909	1065	finally
    //   911	914	1065	finally
    //   916	919	1065	finally
    //   928	933	1065	finally
    //   933	938	1065	finally
    //   940	945	1065	finally
    //   949	955	1065	finally
    //   962	968	1065	finally
    //   968	973	1065	finally
    //   977	983	1065	finally
    //   990	996	1065	finally
    //   996	1001	1065	finally
    //   1003	1010	1065	finally
    //   1014	1020	1065	finally
    //   1027	1033	1065	finally
    //   1033	1038	1065	finally
    //   1042	1048	1065	finally
    //   1048	1053	1065	finally
    //   1057	1062	1065	finally
    //   1062	1065	1065	finally
    //   523	528	1070	java/lang/AssertionError
    //   530	535	1070	java/lang/AssertionError
    //   542	545	1070	java/lang/AssertionError
    //   547	552	1070	java/lang/AssertionError
    //   554	559	1070	java/lang/AssertionError
    //   561	566	1070	java/lang/AssertionError
    //   574	579	1070	java/lang/AssertionError
    //   579	584	1070	java/lang/AssertionError
    //   584	589	1070	java/lang/AssertionError
    //   591	596	1070	java/lang/AssertionError
    //   598	603	1070	java/lang/AssertionError
    //   605	610	1070	java/lang/AssertionError
    //   612	617	1070	java/lang/AssertionError
    //   619	624	1070	java/lang/AssertionError
    //   630	637	1070	java/lang/AssertionError
    //   644	649	1070	java/lang/AssertionError
    //   651	654	1070	java/lang/AssertionError
    //   663	668	1070	java/lang/AssertionError
    //   670	675	1070	java/lang/AssertionError
    //   677	680	1070	java/lang/AssertionError
    //   684	689	1070	java/lang/AssertionError
    //   689	694	1070	java/lang/AssertionError
    //   698	703	1070	java/lang/AssertionError
    //   705	710	1070	java/lang/AssertionError
    //   712	717	1070	java/lang/AssertionError
    //   719	724	1070	java/lang/AssertionError
    //   730	735	1070	java/lang/AssertionError
    //   735	740	1070	java/lang/AssertionError
    //   747	750	1070	java/lang/AssertionError
    //   754	759	1070	java/lang/AssertionError
    //   772	777	1070	java/lang/AssertionError
    //   777	782	1070	java/lang/AssertionError
    //   784	789	1070	java/lang/AssertionError
    //   791	796	1070	java/lang/AssertionError
    //   800	805	1070	java/lang/AssertionError
    //   805	810	1070	java/lang/AssertionError
    //   812	817	1070	java/lang/AssertionError
    //   819	824	1070	java/lang/AssertionError
    //   828	833	1070	java/lang/AssertionError
    //   835	840	1070	java/lang/AssertionError
    //   845	850	1070	java/lang/AssertionError
    //   855	858	1070	java/lang/AssertionError
    //   862	867	1070	java/lang/AssertionError
    //   887	892	1070	java/lang/AssertionError
    //   896	902	1070	java/lang/AssertionError
    //   904	909	1070	java/lang/AssertionError
    //   911	914	1070	java/lang/AssertionError
    //   916	919	1070	java/lang/AssertionError
    //   928	933	1070	java/lang/AssertionError
    //   933	938	1070	java/lang/AssertionError
    //   940	945	1070	java/lang/AssertionError
    //   949	955	1070	java/lang/AssertionError
    //   962	968	1070	java/lang/AssertionError
    //   968	973	1070	java/lang/AssertionError
    //   977	983	1070	java/lang/AssertionError
    //   990	996	1070	java/lang/AssertionError
    //   996	1001	1070	java/lang/AssertionError
    //   1003	1010	1070	java/lang/AssertionError
    //   1014	1020	1070	java/lang/AssertionError
    //   1027	1033	1070	java/lang/AssertionError
    //   1033	1038	1070	java/lang/AssertionError
    //   1042	1048	1070	java/lang/AssertionError
    //   1048	1053	1070	java/lang/AssertionError
    //   1057	1062	1070	java/lang/AssertionError
    //   1062	1065	1070	java/lang/AssertionError
    //   464	469	1079	finally
    //   471	476	1079	finally
    //   478	483	1079	finally
    //   485	490	1079	finally
    //   492	497	1079	finally
    //   507	512	1079	finally
    //   514	519	1079	finally
    //   464	469	1090	java/lang/AssertionError
    //   471	476	1090	java/lang/AssertionError
    //   478	483	1090	java/lang/AssertionError
    //   485	490	1090	java/lang/AssertionError
    //   492	497	1090	java/lang/AssertionError
    //   507	512	1090	java/lang/AssertionError
    //   514	519	1090	java/lang/AssertionError
    //   1098	1103	1128	finally
    //   1110	1113	1128	finally
    //   1117	1122	1128	finally
    //   1122	1125	1128	finally
    //   1125	1128	1128	finally
    //   277	281	1452	java/io/IOException
    //   281	286	1452	java/io/IOException
    //   288	293	1452	java/io/IOException
    //   295	300	1452	java/io/IOException
    //   304	309	1452	java/io/IOException
    //   309	314	1452	java/io/IOException
    //   316	321	1452	java/io/IOException
    //   323	328	1452	java/io/IOException
    //   332	337	1452	java/io/IOException
    //   337	342	1452	java/io/IOException
    //   344	349	1452	java/io/IOException
    //   351	356	1452	java/io/IOException
    //   363	368	1452	java/io/IOException
    //   370	375	1452	java/io/IOException
    //   377	382	1452	java/io/IOException
    //   389	394	1452	java/io/IOException
    //   396	401	1452	java/io/IOException
    //   403	406	1452	java/io/IOException
    //   245	248	1457	java/net/ConnectException
    //   250	255	1457	java/net/ConnectException
    //   257	262	1457	java/net/ConnectException
    //   264	269	1457	java/net/ConnectException
    //   436	440	1519	java/io/IOException
    //   443	448	1519	java/io/IOException
    //   450	455	1519	java/io/IOException
    //   457	462	1519	java/io/IOException
    //   872	875	1519	java/io/IOException
    //   879	884	1519	java/io/IOException
    //   1139	1142	1519	java/io/IOException
    //   1146	1151	1519	java/io/IOException
    //   1151	1156	1519	java/io/IOException
    //   1156	1159	1519	java/io/IOException
    //   1159	1162	1519	java/io/IOException
    //   1166	1171	1519	java/io/IOException
    //   1171	1176	1519	java/io/IOException
    //   1180	1185	1519	java/io/IOException
    //   1185	1190	1519	java/io/IOException
    //   1192	1195	1519	java/io/IOException
    //   1204	1209	1519	java/io/IOException
    //   1211	1214	1519	java/io/IOException
    //   1223	1228	1519	java/io/IOException
    //   1232	1236	1519	java/io/IOException
    //   1236	1239	1519	java/io/IOException
    //   1241	1246	1519	java/io/IOException
    //   1246	1251	1519	java/io/IOException
    //   1253	1258	1519	java/io/IOException
    //   1260	1265	1519	java/io/IOException
    //   1267	1272	1519	java/io/IOException
    //   1274	1279	1519	java/io/IOException
    //   1281	1286	1519	java/io/IOException
    //   1288	1293	1519	java/io/IOException
    //   1297	1302	1519	java/io/IOException
    //   1304	1309	1519	java/io/IOException
    //   1311	1316	1519	java/io/IOException
    //   1318	1323	1519	java/io/IOException
    //   1323	1328	1519	java/io/IOException
    //   1332	1337	1519	java/io/IOException
    //   1337	1340	1519	java/io/IOException
    //   1346	1350	1519	java/io/IOException
    //   1350	1355	1519	java/io/IOException
    //   1357	1364	1519	java/io/IOException
    //   1364	1369	1519	java/io/IOException
    //   1371	1376	1519	java/io/IOException
    //   1380	1387	1519	java/io/IOException
    //   1387	1392	1519	java/io/IOException
    //   1394	1399	1519	java/io/IOException
    //   1413	1418	1519	java/io/IOException
    //   1435	1442	1519	java/io/IOException
    //   1444	1449	1519	java/io/IOException
    //   1458	1461	1519	java/io/IOException
    //   1463	1466	1519	java/io/IOException
    //   1475	1480	1519	java/io/IOException
    //   1480	1485	1519	java/io/IOException
    //   1487	1492	1519	java/io/IOException
    //   1496	1502	1519	java/io/IOException
    //   1502	1507	1519	java/io/IOException
    //   1511	1516	1519	java/io/IOException
    //   1516	1519	1519	java/io/IOException
    //   154	159	1524	java/io/IOException
    //   161	164	1524	java/io/IOException
    //   173	178	1524	java/io/IOException
    //   180	183	1524	java/io/IOException
    //   195	198	1524	java/io/IOException
    //   202	207	1524	java/io/IOException
    //   210	215	1524	java/io/IOException
    //   217	222	1524	java/io/IOException
    //   226	231	1524	java/io/IOException
    //   231	236	1524	java/io/IOException
    //   240	245	1524	java/io/IOException
    //   245	248	1524	java/io/IOException
    //   250	255	1524	java/io/IOException
    //   257	262	1524	java/io/IOException
    //   264	269	1524	java/io/IOException
    //   277	281	1810	java/net/ConnectException
    //   1621	1624	1814	java/lang/reflect/InvocationTargetException
    //   1621	1624	1814	java/lang/IllegalAccessException
    //   1629	1634	1814	java/lang/reflect/InvocationTargetException
    //   1629	1634	1814	java/lang/IllegalAccessException
    //   1639	1642	1814	java/lang/reflect/InvocationTargetException
    //   1639	1642	1814	java/lang/IllegalAccessException
    //   1646	1652	1814	java/lang/reflect/InvocationTargetException
    //   1646	1652	1814	java/lang/IllegalAccessException
  }
  
  public final boolean a(boolean paramBoolean)
  {
    Object localObject1 = b;
    boolean bool = ((Socket)localObject1).isClosed();
    if (!bool)
    {
      localObject1 = b;
      bool = ((Socket)localObject1).isInputShutdown();
      if (!bool)
      {
        localObject1 = b;
        bool = ((Socket)localObject1).isOutputShutdown();
        if (!bool)
        {
          localObject1 = d;
          i1 = 1;
          if (localObject1 != null) {
            return i1;
          }
          if (!paramBoolean) {}
        }
      }
    }
    try
    {
      Socket localSocket1 = b;
      paramBoolean = localSocket1.getSoTimeout();
      try
      {
        localObject1 = b;
        ((Socket)localObject1).setSoTimeout(i1);
        localObject1 = f;
        bool = ((d.e)localObject1).e();
        if (bool)
        {
          localObject1 = b;
          ((Socket)localObject1).setSoTimeout(paramBoolean);
          return false;
        }
        localObject1 = b;
        ((Socket)localObject1).setSoTimeout(paramBoolean);
        return i1;
      }
      finally
      {
        Socket localSocket2 = b;
        localSocket2.setSoTimeout(paramBoolean);
      }
    }
    catch (SocketTimeoutException localSocketTimeoutException)
    {
      return i1;
      return false;
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
    return false;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Connection{");
    Object localObject = a.a.a.b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(":");
    int i1 = a.a.a.c;
    localStringBuilder.append(i1);
    localStringBuilder.append(", proxy=");
    localObject = a.b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(" hostAddress=");
    localObject = a.c;
    localStringBuilder.append(localObject);
    localStringBuilder.append(" cipherSuite=");
    localObject = c;
    if (localObject != null) {
      localObject = a;
    } else {
      localObject = "none";
    }
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(" protocol=");
    localObject = l;
    localStringBuilder.append(localObject);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */