package com.d.a.a.c;

import d.n;
import d.t;
import d.u;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

final class a$1
  implements a
{
  public final u a(File paramFile)
  {
    return n.a(paramFile);
  }
  
  public final void a(File paramFile1, File paramFile2)
  {
    d(paramFile2);
    boolean bool = paramFile1.renameTo(paramFile2);
    if (bool) {
      return;
    }
    IOException localIOException = new java/io/IOException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("failed to rename ");
    localStringBuilder.append(paramFile1);
    localStringBuilder.append(" to ");
    localStringBuilder.append(paramFile2);
    paramFile1 = localStringBuilder.toString();
    localIOException.<init>(paramFile1);
    throw localIOException;
  }
  
  public final t b(File paramFile)
  {
    try
    {
      return n.b(paramFile);
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      paramFile.getParentFile().mkdirs();
    }
    return n.b(paramFile);
  }
  
  public final t c(File paramFile)
  {
    try
    {
      return n.c(paramFile);
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      paramFile.getParentFile().mkdirs();
    }
    return n.c(paramFile);
  }
  
  public final void d(File paramFile)
  {
    boolean bool = paramFile.delete();
    if (!bool)
    {
      bool = paramFile.exists();
      if (bool)
      {
        IOException localIOException = new java/io/IOException;
        paramFile = String.valueOf(paramFile);
        paramFile = "failed to delete ".concat(paramFile);
        localIOException.<init>(paramFile);
        throw localIOException;
      }
    }
  }
  
  public final boolean e(File paramFile)
  {
    return paramFile.exists();
  }
  
  public final long f(File paramFile)
  {
    return paramFile.length();
  }
  
  public final void g(File paramFile)
  {
    Object localObject = paramFile.listFiles();
    if (localObject != null)
    {
      int i = localObject.length;
      int j = 0;
      while (j < i)
      {
        File localFile = localObject[j];
        boolean bool = localFile.isDirectory();
        if (bool) {
          g(localFile);
        }
        bool = localFile.delete();
        if (bool)
        {
          j += 1;
        }
        else
        {
          paramFile = new java/io/IOException;
          localObject = String.valueOf(localFile);
          localObject = "failed to delete ".concat((String)localObject);
          paramFile.<init>((String)localObject);
          throw paramFile;
        }
      }
      return;
    }
    localObject = new java/io/IOException;
    paramFile = String.valueOf(paramFile);
    paramFile = "not a readable directory: ".concat(paramFile);
    ((IOException)localObject).<init>(paramFile);
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.c.a.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */