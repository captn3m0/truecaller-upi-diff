package com.d.a.a;

import d.u;
import java.io.Closeable;

public final class b$c
  implements Closeable
{
  public final String a;
  public final long b;
  public final u[] c;
  private final long[] e;
  
  private b$c(b paramb, String paramString, long paramLong, u[] paramArrayOfu, long[] paramArrayOfLong)
  {
    a = paramString;
    b = paramLong;
    c = paramArrayOfu;
    e = paramArrayOfLong;
  }
  
  public final void close()
  {
    u[] arrayOfu = c;
    int i = arrayOfu.length;
    int j = 0;
    while (j < i)
    {
      u localu = arrayOfu[j];
      j.a(localu);
      j += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */