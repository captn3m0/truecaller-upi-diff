package com.d.a.a;

import com.d.a.u;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLSocket;

final class h$c
  extends h.b
{
  private final Method a;
  private final Method b;
  private final Method c;
  private final Class d;
  private final Class e;
  
  public h$c(Class paramClass1, Method paramMethod1, Method paramMethod2, Method paramMethod3, Class paramClass2, Class paramClass3)
  {
    super(paramClass1);
    a = paramMethod1;
    b = paramMethod2;
    c = paramMethod3;
    d = paramClass2;
    e = paramClass3;
  }
  
  public final void a(SSLSocket paramSSLSocket)
  {
    try
    {
      Method localMethod = c;
      int i = 1;
      Object[] arrayOfObject = new Object[i];
      arrayOfObject[0] = paramSSLSocket;
      localMethod.invoke(null, arrayOfObject);
      return;
    }
    catch (IllegalAccessException|InvocationTargetException localIllegalAccessException)
    {
      paramSSLSocket = new java/lang/AssertionError;
      paramSSLSocket.<init>();
      throw paramSSLSocket;
    }
  }
  
  public final void a(SSLSocket paramSSLSocket, String paramString, List paramList)
  {
    paramString = new java/util/ArrayList;
    int i = paramList.size();
    paramString.<init>(i);
    i = paramList.size();
    int j = 0;
    Class[] arrayOfClass = null;
    Object localObject;
    while (j < i)
    {
      localObject = (u)paramList.get(j);
      u localu = u.a;
      if (localObject != localu)
      {
        localObject = ((u)localObject).toString();
        paramString.add(localObject);
      }
      j += 1;
    }
    paramList = h.class;
    try
    {
      paramList = paramList.getClassLoader();
      i = 2;
      arrayOfClass = new Class[i];
      localObject = d;
      arrayOfClass[0] = localObject;
      localObject = e;
      int k = 1;
      arrayOfClass[k] = localObject;
      localObject = new com/d/a/a/h$d;
      ((h.d)localObject).<init>(paramString);
      paramString = Proxy.newProxyInstance(paramList, arrayOfClass, (InvocationHandler)localObject);
      paramList = a;
      j = 0;
      arrayOfClass = null;
      Object[] arrayOfObject = new Object[i];
      arrayOfObject[0] = paramSSLSocket;
      arrayOfObject[k] = paramString;
      paramList.invoke(null, arrayOfObject);
      return;
    }
    catch (IllegalAccessException paramSSLSocket) {}catch (InvocationTargetException paramSSLSocket) {}
    paramString = new java/lang/AssertionError;
    paramString.<init>(paramSSLSocket);
    throw paramString;
  }
  
  public final String b(SSLSocket paramSSLSocket)
  {
    try
    {
      Object localObject1 = b;
      boolean bool = true;
      Object localObject2 = new Object[bool];
      String str = null;
      localObject2[0] = paramSSLSocket;
      paramSSLSocket = null;
      localObject1 = ((Method)localObject1).invoke(null, (Object[])localObject2);
      localObject1 = Proxy.getInvocationHandler(localObject1);
      localObject1 = (h.d)localObject1;
      bool = h.d.a((h.d)localObject1);
      if (!bool)
      {
        localObject2 = h.d.b((h.d)localObject1);
        if (localObject2 == null)
        {
          localObject1 = d.a;
          localObject2 = Level.INFO;
          str = "ALPN callback dropped: SPDY and HTTP/2 are disabled. Is alpn-boot on the boot class path?";
          ((Logger)localObject1).log((Level)localObject2, str);
          return null;
        }
      }
      bool = h.d.a((h.d)localObject1);
      if (bool) {
        return null;
      }
      return h.d.b((h.d)localObject1);
    }
    catch (InvocationTargetException|IllegalAccessException localInvocationTargetException)
    {
      paramSSLSocket = new java/lang/AssertionError;
      paramSSLSocket.<init>();
      throw paramSSLSocket;
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.h.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */