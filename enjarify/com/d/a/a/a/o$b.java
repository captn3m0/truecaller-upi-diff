package com.d.a.a.a;

import com.d.a.a.j;
import d.d;
import d.g;
import d.t;
import d.u;
import java.io.IOException;
import java.util.List;
import java.util.zip.Deflater;

final class o$b
  implements c
{
  private final d a;
  private final d.c b;
  private final d c;
  private final boolean d;
  private boolean e;
  
  o$b(d paramd, boolean paramBoolean)
  {
    a = paramd;
    d = paramBoolean;
    paramd = new java/util/zip/Deflater;
    paramd.<init>();
    Object localObject = o.a;
    paramd.setDictionary((byte[])localObject);
    localObject = new d/c;
    ((d.c)localObject).<init>();
    b = ((d.c)localObject);
    localObject = new d/g;
    d.c localc = b;
    ((g)localObject).<init>(localc, paramd);
    paramd = d.n.a((t)localObject);
    c = paramd;
  }
  
  private void a(List paramList)
  {
    d locald1 = c;
    int i = paramList.size();
    locald1.h(i);
    int j = paramList.size();
    i = 0;
    while (i < j)
    {
      d.f localf = geth;
      d locald2 = c;
      int k = localf.h();
      locald2.h(k);
      c.c(localf);
      localf = geti;
      locald2 = c;
      k = localf.h();
      locald2.h(k);
      locald2 = c;
      locald2.c(localf);
      i += 1;
    }
    c.flush();
  }
  
  public final void a() {}
  
  public final void a(int paramInt, long paramLong)
  {
    try
    {
      boolean bool1 = e;
      if (!bool1)
      {
        long l = 0L;
        boolean bool2 = paramLong < l;
        if (bool2)
        {
          l = 2147483647L;
          bool2 = paramLong < l;
          if (!bool2)
          {
            localObject1 = a;
            int i = -2147287031;
            ((d)localObject1).h(i);
            localObject1 = a;
            i = 8;
            ((d)localObject1).h(i);
            localObject1 = a;
            ((d)localObject1).h(paramInt);
            localObject2 = a;
            int j = (int)paramLong;
            ((d)localObject2).h(j);
            localObject2 = a;
            ((d)localObject2).flush();
            return;
          }
        }
        localObject2 = new java/lang/IllegalArgumentException;
        Object localObject1 = "windowSizeIncrement must be between 1 and 0x7fffffff: ";
        str = String.valueOf(paramLong);
        str = ((String)localObject1).concat(str);
        ((IllegalArgumentException)localObject2).<init>(str);
        throw ((Throwable)localObject2);
      }
      Object localObject2 = new java/io/IOException;
      String str = "closed";
      ((IOException)localObject2).<init>(str);
      throw ((Throwable)localObject2);
    }
    finally {}
  }
  
  public final void a(int paramInt, a parama)
  {
    try
    {
      boolean bool = e;
      if (!bool)
      {
        int i = t;
        int j = -1;
        if (i != j)
        {
          d locald = a;
          j = -2147287037;
          locald.h(j);
          locald = a;
          j = 8;
          locald.h(j);
          locald = a;
          j = -1 >>> 1;
          paramInt &= j;
          locald.h(paramInt);
          localObject1 = a;
          int k = t;
          ((d)localObject1).h(k);
          localObject1 = a;
          ((d)localObject1).flush();
          return;
        }
        localObject1 = new java/lang/IllegalArgumentException;
        ((IllegalArgumentException)localObject1).<init>();
        throw ((Throwable)localObject1);
      }
      Object localObject1 = new java/io/IOException;
      parama = "closed";
      ((IOException)localObject1).<init>(parama);
      throw ((Throwable)localObject1);
    }
    finally {}
  }
  
  public final void a(int paramInt, a parama, byte[] paramArrayOfByte)
  {
    try
    {
      boolean bool = e;
      if (!bool)
      {
        int i = u;
        int j = -1;
        if (i != j)
        {
          paramArrayOfByte = a;
          j = -2147287033;
          paramArrayOfByte.h(j);
          paramArrayOfByte = a;
          j = 8;
          paramArrayOfByte.h(j);
          paramArrayOfByte = a;
          paramArrayOfByte.h(paramInt);
          localObject1 = a;
          int k = u;
          ((d)localObject1).h(k);
          localObject1 = a;
          ((d)localObject1).flush();
          return;
        }
        localObject1 = new java/lang/IllegalArgumentException;
        parama = "errorCode.spdyGoAwayCode == -1";
        ((IllegalArgumentException)localObject1).<init>(parama);
        throw ((Throwable)localObject1);
      }
      Object localObject1 = new java/io/IOException;
      parama = "closed";
      ((IOException)localObject1).<init>(parama);
      throw ((Throwable)localObject1);
    }
    finally {}
  }
  
  public final void a(n paramn) {}
  
  public final void a(boolean paramBoolean, int paramInt1, int paramInt2)
  {
    try
    {
      paramInt2 = e;
      if (paramInt2 == 0)
      {
        paramInt2 = d;
        int i = paramInt1 & 0x1;
        boolean bool = false;
        int j = 1;
        if (i == j) {
          i = 1;
        } else {
          i = 0;
        }
        if (paramInt2 != i) {
          bool = true;
        }
        if (paramBoolean == bool)
        {
          localObject1 = a;
          paramInt2 = -2147287034;
          ((d)localObject1).h(paramInt2);
          localObject1 = a;
          paramInt2 = 4;
          ((d)localObject1).h(paramInt2);
          localObject1 = a;
          ((d)localObject1).h(paramInt1);
          localObject1 = a;
          ((d)localObject1).flush();
          return;
        }
        localObject1 = new java/lang/IllegalArgumentException;
        str = "payload != reply";
        ((IllegalArgumentException)localObject1).<init>(str);
        throw ((Throwable)localObject1);
      }
      Object localObject1 = new java/io/IOException;
      String str = "closed";
      ((IOException)localObject1).<init>(str);
      throw ((Throwable)localObject1);
    }
    finally {}
  }
  
  public final void a(boolean paramBoolean, int paramInt1, d.c paramc, int paramInt2)
  {
    Object localObject1;
    if (paramBoolean)
    {
      paramBoolean = true;
    }
    else
    {
      paramBoolean = false;
      localObject1 = null;
    }
    try
    {
      boolean bool1 = e;
      if (!bool1)
      {
        long l1 = paramInt2;
        long l2 = 16777215L;
        boolean bool2 = l1 < l2;
        if (!bool2)
        {
          d locald = a;
          int i = -1 >>> 1;
          paramInt1 &= i;
          locald.h(paramInt1);
          localObject3 = a;
          paramBoolean = (paramBoolean & true) << true;
          boolean bool3 = 0xFFFFFF & paramInt2;
          paramBoolean |= bool3;
          ((d)localObject3).h(paramBoolean);
          if (paramInt2 > 0)
          {
            localObject1 = a;
            ((d)localObject1).a_(paramc, l1);
          }
          return;
        }
        localObject1 = new java/lang/IllegalArgumentException;
        localObject3 = "FRAME_TOO_LARGE max size is 16Mib: ";
        paramc = String.valueOf(paramInt2);
        localObject3 = ((String)localObject3).concat(paramc);
        ((IllegalArgumentException)localObject1).<init>((String)localObject3);
        throw ((Throwable)localObject1);
      }
      localObject1 = new java/io/IOException;
      Object localObject3 = "closed";
      ((IOException)localObject1).<init>((String)localObject3);
      throw ((Throwable)localObject1);
    }
    finally {}
  }
  
  public final void a(boolean paramBoolean, int paramInt, List paramList)
  {
    try
    {
      boolean bool1 = e;
      if (!bool1)
      {
        a(paramList);
        long l1 = 10;
        paramList = b;
        long l2 = b + l1;
        boolean bool2 = (int)l2;
        bool1 = false;
        if (paramBoolean)
        {
          paramBoolean = true;
        }
        else
        {
          paramBoolean = false;
          localObject1 = null;
        }
        paramBoolean |= false;
        d locald = a;
        boolean bool3 = -2147287039;
        locald.h(bool3);
        locald = a;
        paramBoolean = (paramBoolean & true) << true;
        bool3 = 16777215;
        bool2 &= bool3;
        paramBoolean |= bool2;
        locald.h(paramBoolean);
        localObject1 = a;
        int i = -1 >>> 1;
        paramInt &= i;
        ((d)localObject1).h(paramInt);
        localObject1 = a;
        ((d)localObject1).h(0);
        localObject1 = a;
        ((d)localObject1).i(0);
        localObject1 = a;
        localObject3 = b;
        ((d)localObject1).a((u)localObject3);
        localObject1 = a;
        ((d)localObject1).flush();
        return;
      }
      Object localObject1 = new java/io/IOException;
      Object localObject3 = "closed";
      ((IOException)localObject1).<init>((String)localObject3);
      throw ((Throwable)localObject1);
    }
    finally {}
  }
  
  public final void b()
  {
    try
    {
      boolean bool = e;
      if (!bool)
      {
        localObject1 = a;
        ((d)localObject1).flush();
        return;
      }
      Object localObject1 = new java/io/IOException;
      String str = "closed";
      ((IOException)localObject1).<init>(str);
      throw ((Throwable)localObject1);
    }
    finally {}
  }
  
  public final void b(n paramn)
  {
    try
    {
      boolean bool1 = e;
      if (!bool1)
      {
        int i = a;
        i = Integer.bitCount(i);
        int k = i * 8 + 4;
        d locald = a;
        int m = -2147287036;
        locald.h(m);
        locald = a;
        m = 16777215;
        k &= m;
        int n = 0;
        k |= 0x0;
        locald.h(k);
        Object localObject1 = a;
        ((d)localObject1).h(i);
        for (;;)
        {
          i = 10;
          if (n > i) {
            break;
          }
          boolean bool2 = paramn.a(n);
          if (bool2)
          {
            int j = paramn.b(n);
            localObject1 = a;
            j = (j & 0xFF) << 24;
            int i1 = n & m;
            j |= i1;
            ((d)localObject1).h(j);
            localObject2 = a;
            localObject1 = d;
            k = localObject1[n];
            ((d)localObject2).h(k);
          }
          n += 1;
        }
        paramn = a;
        paramn.flush();
        return;
      }
      paramn = new java/io/IOException;
      Object localObject2 = "closed";
      paramn.<init>((String)localObject2);
      throw paramn;
    }
    finally {}
  }
  
  public final int c()
  {
    return 16383;
  }
  
  public final void close()
  {
    boolean bool = true;
    try
    {
      e = bool;
      d locald1 = a;
      d locald2 = c;
      j.a(locald1, locald2);
      return;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.o.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */