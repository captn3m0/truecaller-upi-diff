package com.d.a.a.a;

import java.io.ByteArrayOutputStream;

final class j
{
  private static final int[] a;
  private static final byte[] b;
  private static final j c;
  private final j.a d;
  
  static
  {
    int i = 256;
    int[] arrayOfInt = new int[i];
    int[] tmp9_8 = arrayOfInt;
    int[] tmp10_9 = tmp9_8;
    int[] tmp10_9 = tmp9_8;
    tmp10_9[0] = 'Ὸ';
    tmp10_9[1] = 8388568;
    int[] tmp20_10 = tmp10_9;
    int[] tmp20_10 = tmp10_9;
    tmp20_10[2] = 268435426;
    tmp20_10[3] = 268435427;
    int[] tmp29_20 = tmp20_10;
    int[] tmp29_20 = tmp20_10;
    tmp29_20[4] = 268435428;
    tmp29_20[5] = 268435429;
    int[] tmp38_29 = tmp29_20;
    int[] tmp38_29 = tmp29_20;
    tmp38_29[6] = 268435430;
    tmp38_29[7] = 268435431;
    int[] tmp49_38 = tmp38_29;
    int[] tmp49_38 = tmp38_29;
    tmp49_38[8] = 268435432;
    tmp49_38[9] = 16777194;
    int[] tmp60_49 = tmp49_38;
    int[] tmp60_49 = tmp49_38;
    tmp60_49[10] = 1073741820;
    tmp60_49[11] = 268435433;
    int[] tmp71_60 = tmp60_49;
    int[] tmp71_60 = tmp60_49;
    tmp71_60[12] = 268435434;
    tmp71_60[13] = 1073741821;
    int[] tmp82_71 = tmp71_60;
    int[] tmp82_71 = tmp71_60;
    tmp82_71[14] = 268435435;
    tmp82_71[15] = 268435436;
    int[] tmp93_82 = tmp82_71;
    int[] tmp93_82 = tmp82_71;
    tmp93_82[16] = 268435437;
    tmp93_82[17] = 268435438;
    int[] tmp104_93 = tmp93_82;
    int[] tmp104_93 = tmp93_82;
    tmp104_93[18] = 268435439;
    tmp104_93[19] = 268435440;
    int[] tmp115_104 = tmp104_93;
    int[] tmp115_104 = tmp104_93;
    tmp115_104[20] = 268435441;
    tmp115_104[21] = 268435442;
    int[] tmp126_115 = tmp115_104;
    int[] tmp126_115 = tmp115_104;
    tmp126_115[22] = 1073741822;
    tmp126_115[23] = 268435443;
    int[] tmp137_126 = tmp126_115;
    int[] tmp137_126 = tmp126_115;
    tmp137_126[24] = 268435444;
    tmp137_126[25] = 268435445;
    int[] tmp148_137 = tmp137_126;
    int[] tmp148_137 = tmp137_126;
    tmp148_137[26] = 268435446;
    tmp148_137[27] = 268435447;
    int[] tmp159_148 = tmp148_137;
    int[] tmp159_148 = tmp148_137;
    tmp159_148[28] = 268435448;
    tmp159_148[29] = 268435449;
    int[] tmp170_159 = tmp159_148;
    int[] tmp170_159 = tmp159_148;
    tmp170_159[30] = 268435450;
    tmp170_159[31] = 268435451;
    int[] tmp181_170 = tmp170_159;
    int[] tmp181_170 = tmp170_159;
    tmp181_170[32] = 20;
    tmp181_170[33] = 'ϸ';
    int[] tmp193_181 = tmp181_170;
    int[] tmp193_181 = tmp181_170;
    tmp193_181[34] = 'Ϲ';
    tmp193_181[35] = '࿺';
    int[] tmp206_193 = tmp193_181;
    int[] tmp206_193 = tmp193_181;
    tmp206_193[36] = 'Ό';
    tmp206_193[37] = 21;
    int[] tmp218_206 = tmp206_193;
    int[] tmp218_206 = tmp206_193;
    tmp218_206[38] = 'ø';
    tmp218_206[39] = 'ߺ';
    int[] tmp231_218 = tmp218_206;
    int[] tmp231_218 = tmp218_206;
    tmp231_218[40] = 'Ϻ';
    tmp231_218[41] = 'ϻ';
    int[] tmp244_231 = tmp231_218;
    int[] tmp244_231 = tmp231_218;
    tmp244_231[42] = 'ù';
    tmp244_231[43] = '߻';
    int[] tmp257_244 = tmp244_231;
    int[] tmp257_244 = tmp244_231;
    tmp257_244[44] = 'ú';
    tmp257_244[45] = 22;
    int[] tmp269_257 = tmp257_244;
    int[] tmp269_257 = tmp257_244;
    tmp269_257[46] = 23;
    tmp269_257[47] = 24;
    int[] tmp280_269 = tmp269_257;
    int[] tmp280_269 = tmp269_257;
    tmp280_269[48] = 0;
    tmp280_269[49] = 1;
    int[] tmp289_280 = tmp280_269;
    int[] tmp289_280 = tmp280_269;
    tmp289_280[50] = 2;
    tmp289_280[51] = 25;
    int[] tmp299_289 = tmp289_280;
    int[] tmp299_289 = tmp289_280;
    tmp299_289[52] = 26;
    tmp299_289[53] = 27;
    int[] tmp310_299 = tmp299_289;
    int[] tmp310_299 = tmp299_289;
    tmp310_299[54] = 28;
    tmp310_299[55] = 29;
    int[] tmp321_310 = tmp310_299;
    int[] tmp321_310 = tmp310_299;
    tmp321_310[56] = 30;
    tmp321_310[57] = 31;
    int[] tmp332_321 = tmp321_310;
    int[] tmp332_321 = tmp321_310;
    tmp332_321[58] = 92;
    tmp332_321[59] = 'û';
    int[] tmp344_332 = tmp332_321;
    int[] tmp344_332 = tmp332_321;
    tmp344_332[60] = '翼';
    tmp344_332[61] = 32;
    int[] tmp356_344 = tmp344_332;
    int[] tmp356_344 = tmp344_332;
    tmp356_344[62] = '࿻';
    tmp356_344[63] = 'ϼ';
    int[] tmp369_356 = tmp356_344;
    int[] tmp369_356 = tmp356_344;
    tmp369_356[64] = 'Ὼ';
    tmp369_356[65] = 33;
    int[] tmp381_369 = tmp369_356;
    int[] tmp381_369 = tmp369_356;
    tmp381_369[66] = 93;
    tmp381_369[67] = 94;
    int[] tmp392_381 = tmp381_369;
    int[] tmp392_381 = tmp381_369;
    tmp392_381[68] = 95;
    tmp392_381[69] = 96;
    int[] tmp403_392 = tmp392_381;
    int[] tmp403_392 = tmp392_381;
    tmp403_392[70] = 97;
    tmp403_392[71] = 98;
    int[] tmp414_403 = tmp403_392;
    int[] tmp414_403 = tmp403_392;
    tmp414_403[72] = 99;
    tmp414_403[73] = 100;
    int[] tmp425_414 = tmp414_403;
    int[] tmp425_414 = tmp414_403;
    tmp425_414[74] = 101;
    tmp425_414[75] = 102;
    int[] tmp436_425 = tmp425_414;
    int[] tmp436_425 = tmp425_414;
    tmp436_425[76] = 103;
    tmp436_425[77] = 104;
    int[] tmp447_436 = tmp436_425;
    int[] tmp447_436 = tmp436_425;
    tmp447_436[78] = 105;
    tmp447_436[79] = 106;
    int[] tmp458_447 = tmp447_436;
    int[] tmp458_447 = tmp447_436;
    tmp458_447[80] = 107;
    tmp458_447[81] = 108;
    int[] tmp469_458 = tmp458_447;
    int[] tmp469_458 = tmp458_447;
    tmp469_458[82] = 109;
    tmp469_458[83] = 110;
    int[] tmp480_469 = tmp469_458;
    int[] tmp480_469 = tmp469_458;
    tmp480_469[84] = 111;
    tmp480_469[85] = 112;
    int[] tmp491_480 = tmp480_469;
    int[] tmp491_480 = tmp480_469;
    tmp491_480[86] = 113;
    tmp491_480[87] = 114;
    int[] tmp502_491 = tmp491_480;
    int[] tmp502_491 = tmp491_480;
    tmp502_491[88] = 'ü';
    tmp502_491[89] = 115;
    int[] tmp514_502 = tmp502_491;
    int[] tmp514_502 = tmp502_491;
    tmp514_502[90] = 'ý';
    tmp514_502[91] = 'Ώ';
    int[] tmp527_514 = tmp514_502;
    int[] tmp527_514 = tmp514_502;
    tmp527_514[92] = 524272;
    tmp527_514[93] = 'ῼ';
    int[] tmp539_527 = tmp527_514;
    int[] tmp539_527 = tmp527_514;
    tmp539_527[94] = '㿼';
    tmp539_527[95] = 34;
    int[] tmp551_539 = tmp539_527;
    int[] tmp551_539 = tmp539_527;
    tmp551_539[96] = '翽';
    tmp551_539[97] = 3;
    int[] tmp562_551 = tmp551_539;
    int[] tmp562_551 = tmp551_539;
    tmp562_551[98] = 35;
    tmp562_551[99] = 4;
    int[] tmp572_562 = tmp562_551;
    int[] tmp572_562 = tmp562_551;
    tmp572_562[100] = 36;
    tmp572_562[101] = 5;
    int[] tmp582_572 = tmp572_562;
    int[] tmp582_572 = tmp572_562;
    tmp582_572[102] = 37;
    tmp582_572[103] = 38;
    int[] tmp593_582 = tmp582_572;
    int[] tmp593_582 = tmp582_572;
    tmp593_582[104] = 39;
    tmp593_582[105] = 6;
    int[] tmp604_593 = tmp593_582;
    int[] tmp604_593 = tmp593_582;
    tmp604_593[106] = 116;
    tmp604_593[107] = 117;
    int[] tmp615_604 = tmp604_593;
    int[] tmp615_604 = tmp604_593;
    tmp615_604[108] = 40;
    tmp615_604[109] = 41;
    int[] tmp626_615 = tmp615_604;
    int[] tmp626_615 = tmp615_604;
    tmp626_615[110] = 42;
    tmp626_615[111] = 7;
    int[] tmp637_626 = tmp626_615;
    int[] tmp637_626 = tmp626_615;
    tmp637_626[112] = 43;
    tmp637_626[113] = 118;
    int[] tmp648_637 = tmp637_626;
    int[] tmp648_637 = tmp637_626;
    tmp648_637[114] = 44;
    tmp648_637[115] = 8;
    int[] tmp659_648 = tmp648_637;
    int[] tmp659_648 = tmp648_637;
    tmp659_648[116] = 9;
    tmp659_648[117] = 45;
    int[] tmp670_659 = tmp659_648;
    int[] tmp670_659 = tmp659_648;
    tmp670_659[118] = 119;
    tmp670_659[119] = 120;
    int[] tmp681_670 = tmp670_659;
    int[] tmp681_670 = tmp670_659;
    tmp681_670[120] = 121;
    tmp681_670[121] = 122;
    int[] tmp692_681 = tmp681_670;
    int[] tmp692_681 = tmp681_670;
    tmp692_681[122] = 123;
    tmp692_681[123] = '翾';
    int[] tmp704_692 = tmp692_681;
    int[] tmp704_692 = tmp692_681;
    tmp704_692[124] = '߼';
    tmp704_692[125] = '㿽';
    int[] tmp717_704 = tmp704_692;
    int[] tmp717_704 = tmp704_692;
    tmp717_704[126] = '´';
    tmp717_704[127] = 268435452;
    int[] tmp729_717 = tmp717_704;
    int[] tmp729_717 = tmp717_704;
    tmp729_717[''] = 1048550;
    tmp729_717[''] = 4194258;
    int[] tmp742_729 = tmp729_717;
    int[] tmp742_729 = tmp729_717;
    tmp742_729[''] = 1048551;
    tmp742_729[''] = 1048552;
    int[] tmp755_742 = tmp742_729;
    int[] tmp755_742 = tmp742_729;
    tmp755_742[''] = 4194259;
    tmp755_742[''] = 4194260;
    int[] tmp768_755 = tmp755_742;
    int[] tmp768_755 = tmp755_742;
    tmp768_755[''] = 4194261;
    tmp768_755[''] = 8388569;
    int[] tmp781_768 = tmp768_755;
    int[] tmp781_768 = tmp768_755;
    tmp781_768[''] = 4194262;
    tmp781_768[''] = 8388570;
    int[] tmp794_781 = tmp781_768;
    int[] tmp794_781 = tmp781_768;
    tmp794_781[''] = 8388571;
    tmp794_781[''] = 8388572;
    int[] tmp807_794 = tmp794_781;
    int[] tmp807_794 = tmp794_781;
    tmp807_794[''] = 8388573;
    tmp807_794[''] = 8388574;
    int[] tmp820_807 = tmp807_794;
    int[] tmp820_807 = tmp807_794;
    tmp820_807[''] = 16777195;
    tmp820_807[''] = 8388575;
    int[] tmp833_820 = tmp820_807;
    int[] tmp833_820 = tmp820_807;
    tmp833_820[''] = 16777196;
    tmp833_820[''] = 16777197;
    int[] tmp846_833 = tmp833_820;
    int[] tmp846_833 = tmp833_820;
    tmp846_833[''] = 4194263;
    tmp846_833[''] = 8388576;
    int[] tmp859_846 = tmp846_833;
    int[] tmp859_846 = tmp846_833;
    tmp859_846[''] = 16777198;
    tmp859_846[''] = 8388577;
    int[] tmp872_859 = tmp859_846;
    int[] tmp872_859 = tmp859_846;
    tmp872_859[''] = 8388578;
    tmp872_859[''] = 8388579;
    int[] tmp885_872 = tmp872_859;
    int[] tmp885_872 = tmp872_859;
    tmp885_872[''] = 8388580;
    tmp885_872[''] = 2097116;
    int[] tmp898_885 = tmp885_872;
    int[] tmp898_885 = tmp885_872;
    tmp898_885[''] = 4194264;
    tmp898_885[''] = 8388581;
    int[] tmp911_898 = tmp898_885;
    int[] tmp911_898 = tmp898_885;
    tmp911_898[''] = 4194265;
    tmp911_898[''] = 8388582;
    int[] tmp924_911 = tmp911_898;
    int[] tmp924_911 = tmp911_898;
    tmp924_911[''] = 8388583;
    tmp924_911[''] = 16777199;
    int[] tmp937_924 = tmp924_911;
    int[] tmp937_924 = tmp924_911;
    tmp937_924[' '] = 4194266;
    tmp937_924['¡'] = 2097117;
    int[] tmp950_937 = tmp937_924;
    int[] tmp950_937 = tmp937_924;
    tmp950_937['¢'] = 1048553;
    tmp950_937['£'] = 4194267;
    int[] tmp963_950 = tmp950_937;
    int[] tmp963_950 = tmp950_937;
    tmp963_950['¤'] = 4194268;
    tmp963_950['¥'] = 8388584;
    int[] tmp976_963 = tmp963_950;
    int[] tmp976_963 = tmp963_950;
    tmp976_963['¦'] = 8388585;
    tmp976_963['§'] = 2097118;
    int[] tmp989_976 = tmp976_963;
    int[] tmp989_976 = tmp976_963;
    tmp989_976['¨'] = 8388586;
    tmp989_976['©'] = 4194269;
    int[] tmp1002_989 = tmp989_976;
    int[] tmp1002_989 = tmp989_976;
    tmp1002_989['ª'] = 4194270;
    tmp1002_989['«'] = 16777200;
    int[] tmp1015_1002 = tmp1002_989;
    int[] tmp1015_1002 = tmp1002_989;
    tmp1015_1002['¬'] = 2097119;
    tmp1015_1002['­'] = 4194271;
    int[] tmp1028_1015 = tmp1015_1002;
    int[] tmp1028_1015 = tmp1015_1002;
    tmp1028_1015['®'] = 8388587;
    tmp1028_1015['¯'] = 8388588;
    int[] tmp1041_1028 = tmp1028_1015;
    int[] tmp1041_1028 = tmp1028_1015;
    tmp1041_1028['°'] = 2097120;
    tmp1041_1028['±'] = 2097121;
    int[] tmp1054_1041 = tmp1041_1028;
    int[] tmp1054_1041 = tmp1041_1028;
    tmp1054_1041['²'] = 4194272;
    tmp1054_1041['³'] = 2097122;
    int[] tmp1067_1054 = tmp1054_1041;
    int[] tmp1067_1054 = tmp1054_1041;
    tmp1067_1054['´'] = 8388589;
    tmp1067_1054['µ'] = 4194273;
    int[] tmp1080_1067 = tmp1067_1054;
    int[] tmp1080_1067 = tmp1067_1054;
    tmp1080_1067['¶'] = 8388590;
    tmp1080_1067['·'] = 8388591;
    int[] tmp1093_1080 = tmp1080_1067;
    int[] tmp1093_1080 = tmp1080_1067;
    tmp1093_1080['¸'] = 1048554;
    tmp1093_1080['¹'] = 4194274;
    int[] tmp1106_1093 = tmp1093_1080;
    int[] tmp1106_1093 = tmp1093_1080;
    tmp1106_1093['º'] = 4194275;
    tmp1106_1093['»'] = 4194276;
    int[] tmp1119_1106 = tmp1106_1093;
    int[] tmp1119_1106 = tmp1106_1093;
    tmp1119_1106['¼'] = 8388592;
    tmp1119_1106['½'] = 4194277;
    int[] tmp1132_1119 = tmp1119_1106;
    int[] tmp1132_1119 = tmp1119_1106;
    tmp1132_1119['¾'] = 4194278;
    tmp1132_1119['¿'] = 8388593;
    int[] tmp1145_1132 = tmp1132_1119;
    int[] tmp1145_1132 = tmp1132_1119;
    tmp1145_1132['À'] = 67108832;
    tmp1145_1132['Á'] = 67108833;
    int[] tmp1158_1145 = tmp1145_1132;
    int[] tmp1158_1145 = tmp1145_1132;
    tmp1158_1145['Â'] = 1048555;
    tmp1158_1145['Ã'] = 524273;
    int[] tmp1171_1158 = tmp1158_1145;
    int[] tmp1171_1158 = tmp1158_1145;
    tmp1171_1158['Ä'] = 4194279;
    tmp1171_1158['Å'] = 8388594;
    int[] tmp1184_1171 = tmp1171_1158;
    int[] tmp1184_1171 = tmp1171_1158;
    tmp1184_1171['Æ'] = 4194280;
    tmp1184_1171['Ç'] = 33554412;
    int[] tmp1197_1184 = tmp1184_1171;
    int[] tmp1197_1184 = tmp1184_1171;
    tmp1197_1184['È'] = 67108834;
    tmp1197_1184['É'] = 67108835;
    int[] tmp1210_1197 = tmp1197_1184;
    int[] tmp1210_1197 = tmp1197_1184;
    tmp1210_1197['Ê'] = 67108836;
    tmp1210_1197['Ë'] = 134217694;
    int[] tmp1223_1210 = tmp1210_1197;
    int[] tmp1223_1210 = tmp1210_1197;
    tmp1223_1210['Ì'] = 134217695;
    tmp1223_1210['Í'] = 67108837;
    int[] tmp1236_1223 = tmp1223_1210;
    int[] tmp1236_1223 = tmp1223_1210;
    tmp1236_1223['Î'] = 16777201;
    tmp1236_1223['Ï'] = 33554413;
    int[] tmp1249_1236 = tmp1236_1223;
    int[] tmp1249_1236 = tmp1236_1223;
    tmp1249_1236['Ð'] = 524274;
    tmp1249_1236['Ñ'] = 2097123;
    int[] tmp1262_1249 = tmp1249_1236;
    int[] tmp1262_1249 = tmp1249_1236;
    tmp1262_1249['Ò'] = 67108838;
    tmp1262_1249['Ó'] = 134217696;
    int[] tmp1275_1262 = tmp1262_1249;
    int[] tmp1275_1262 = tmp1262_1249;
    tmp1275_1262['Ô'] = 134217697;
    tmp1275_1262['Õ'] = 67108839;
    int[] tmp1288_1275 = tmp1275_1262;
    int[] tmp1288_1275 = tmp1275_1262;
    tmp1288_1275['Ö'] = 134217698;
    tmp1288_1275['×'] = 16777202;
    int[] tmp1301_1288 = tmp1288_1275;
    int[] tmp1301_1288 = tmp1288_1275;
    tmp1301_1288['Ø'] = 2097124;
    tmp1301_1288['Ù'] = 2097125;
    int[] tmp1314_1301 = tmp1301_1288;
    int[] tmp1314_1301 = tmp1301_1288;
    tmp1314_1301['Ú'] = 67108840;
    tmp1314_1301['Û'] = 67108841;
    int[] tmp1327_1314 = tmp1314_1301;
    int[] tmp1327_1314 = tmp1314_1301;
    tmp1327_1314['Ü'] = 268435453;
    tmp1327_1314['Ý'] = 134217699;
    int[] tmp1340_1327 = tmp1327_1314;
    int[] tmp1340_1327 = tmp1327_1314;
    tmp1340_1327['Þ'] = 134217700;
    tmp1340_1327['ß'] = 134217701;
    int[] tmp1353_1340 = tmp1340_1327;
    int[] tmp1353_1340 = tmp1340_1327;
    tmp1353_1340['à'] = 1048556;
    tmp1353_1340['á'] = 16777203;
    int[] tmp1366_1353 = tmp1353_1340;
    int[] tmp1366_1353 = tmp1353_1340;
    tmp1366_1353['â'] = 1048557;
    tmp1366_1353['ã'] = 2097126;
    int[] tmp1379_1366 = tmp1366_1353;
    int[] tmp1379_1366 = tmp1366_1353;
    tmp1379_1366['ä'] = 4194281;
    tmp1379_1366['å'] = 2097127;
    int[] tmp1392_1379 = tmp1379_1366;
    int[] tmp1392_1379 = tmp1379_1366;
    tmp1392_1379['æ'] = 2097128;
    tmp1392_1379['ç'] = 8388595;
    int[] tmp1405_1392 = tmp1392_1379;
    int[] tmp1405_1392 = tmp1392_1379;
    tmp1405_1392['è'] = 4194282;
    tmp1405_1392['é'] = 4194283;
    int[] tmp1418_1405 = tmp1405_1392;
    int[] tmp1418_1405 = tmp1405_1392;
    tmp1418_1405['ê'] = 33554414;
    tmp1418_1405['ë'] = 33554415;
    int[] tmp1431_1418 = tmp1418_1405;
    int[] tmp1431_1418 = tmp1418_1405;
    tmp1431_1418['ì'] = 16777204;
    tmp1431_1418['í'] = 16777205;
    int[] tmp1444_1431 = tmp1431_1418;
    int[] tmp1444_1431 = tmp1431_1418;
    tmp1444_1431['î'] = 67108842;
    tmp1444_1431['ï'] = 8388596;
    int[] tmp1457_1444 = tmp1444_1431;
    int[] tmp1457_1444 = tmp1444_1431;
    tmp1457_1444['ð'] = 67108843;
    tmp1457_1444['ñ'] = 134217702;
    int[] tmp1470_1457 = tmp1457_1444;
    int[] tmp1470_1457 = tmp1457_1444;
    tmp1470_1457['ò'] = 67108844;
    tmp1470_1457['ó'] = 67108845;
    int[] tmp1483_1470 = tmp1470_1457;
    int[] tmp1483_1470 = tmp1470_1457;
    tmp1483_1470['ô'] = 134217703;
    tmp1483_1470['õ'] = 134217704;
    int[] tmp1496_1483 = tmp1483_1470;
    int[] tmp1496_1483 = tmp1483_1470;
    tmp1496_1483['ö'] = 134217705;
    tmp1496_1483['÷'] = 134217706;
    int[] tmp1509_1496 = tmp1496_1483;
    int[] tmp1509_1496 = tmp1496_1483;
    tmp1509_1496['ø'] = 134217707;
    tmp1509_1496['ù'] = 268435454;
    int[] tmp1522_1509 = tmp1509_1496;
    int[] tmp1522_1509 = tmp1509_1496;
    tmp1522_1509['ú'] = 134217708;
    tmp1522_1509['û'] = 134217709;
    int[] tmp1535_1522 = tmp1522_1509;
    int[] tmp1535_1522 = tmp1522_1509;
    tmp1535_1522['ü'] = 134217710;
    tmp1535_1522['ý'] = 134217711;
    tmp1535_1522['þ'] = 134217712;
    tmp1535_1522['ÿ'] = 67108846;
    a = arrayOfInt;
    Object localObject = new byte[i];
    Object tmp1569_1568 = localObject;
    Object tmp1570_1569 = tmp1569_1568;
    Object tmp1570_1569 = tmp1569_1568;
    tmp1570_1569[0] = 13;
    tmp1570_1569[1] = 23;
    Object tmp1579_1570 = tmp1570_1569;
    Object tmp1579_1570 = tmp1570_1569;
    tmp1579_1570[2] = 28;
    tmp1579_1570[3] = 28;
    Object tmp1588_1579 = tmp1579_1570;
    Object tmp1588_1579 = tmp1579_1570;
    tmp1588_1579[4] = 28;
    tmp1588_1579[5] = 28;
    Object tmp1597_1588 = tmp1588_1579;
    Object tmp1597_1588 = tmp1588_1579;
    tmp1597_1588[6] = 28;
    tmp1597_1588[7] = 28;
    Object tmp1608_1597 = tmp1597_1588;
    Object tmp1608_1597 = tmp1597_1588;
    tmp1608_1597[8] = 28;
    tmp1608_1597[9] = 24;
    Object tmp1619_1608 = tmp1608_1597;
    Object tmp1619_1608 = tmp1608_1597;
    tmp1619_1608[10] = 30;
    tmp1619_1608[11] = 28;
    Object tmp1630_1619 = tmp1619_1608;
    Object tmp1630_1619 = tmp1619_1608;
    tmp1630_1619[12] = 28;
    tmp1630_1619[13] = 30;
    Object tmp1641_1630 = tmp1630_1619;
    Object tmp1641_1630 = tmp1630_1619;
    tmp1641_1630[14] = 28;
    tmp1641_1630[15] = 28;
    Object tmp1652_1641 = tmp1641_1630;
    Object tmp1652_1641 = tmp1641_1630;
    tmp1652_1641[16] = 28;
    tmp1652_1641[17] = 28;
    Object tmp1663_1652 = tmp1652_1641;
    Object tmp1663_1652 = tmp1652_1641;
    tmp1663_1652[18] = 28;
    tmp1663_1652[19] = 28;
    Object tmp1674_1663 = tmp1663_1652;
    Object tmp1674_1663 = tmp1663_1652;
    tmp1674_1663[20] = 28;
    tmp1674_1663[21] = 28;
    Object tmp1685_1674 = tmp1674_1663;
    Object tmp1685_1674 = tmp1674_1663;
    tmp1685_1674[22] = 30;
    tmp1685_1674[23] = 28;
    Object tmp1696_1685 = tmp1685_1674;
    Object tmp1696_1685 = tmp1685_1674;
    tmp1696_1685[24] = 28;
    tmp1696_1685[25] = 28;
    Object tmp1707_1696 = tmp1696_1685;
    Object tmp1707_1696 = tmp1696_1685;
    tmp1707_1696[26] = 28;
    tmp1707_1696[27] = 28;
    Object tmp1718_1707 = tmp1707_1696;
    Object tmp1718_1707 = tmp1707_1696;
    tmp1718_1707[28] = 28;
    tmp1718_1707[29] = 28;
    Object tmp1729_1718 = tmp1718_1707;
    Object tmp1729_1718 = tmp1718_1707;
    tmp1729_1718[30] = 28;
    tmp1729_1718[31] = 28;
    Object tmp1740_1729 = tmp1729_1718;
    Object tmp1740_1729 = tmp1729_1718;
    tmp1740_1729[32] = 6;
    tmp1740_1729[33] = 10;
    Object tmp1751_1740 = tmp1740_1729;
    Object tmp1751_1740 = tmp1740_1729;
    tmp1751_1740[34] = 10;
    tmp1751_1740[35] = 12;
    Object tmp1762_1751 = tmp1751_1740;
    Object tmp1762_1751 = tmp1751_1740;
    tmp1762_1751[36] = 13;
    tmp1762_1751[37] = 6;
    Object tmp1773_1762 = tmp1762_1751;
    Object tmp1773_1762 = tmp1762_1751;
    tmp1773_1762[38] = 8;
    tmp1773_1762[39] = 11;
    Object tmp1784_1773 = tmp1773_1762;
    Object tmp1784_1773 = tmp1773_1762;
    tmp1784_1773[40] = 10;
    tmp1784_1773[41] = 10;
    Object tmp1795_1784 = tmp1784_1773;
    Object tmp1795_1784 = tmp1784_1773;
    tmp1795_1784[42] = 8;
    tmp1795_1784[43] = 11;
    Object tmp1806_1795 = tmp1795_1784;
    Object tmp1806_1795 = tmp1795_1784;
    tmp1806_1795[44] = 8;
    tmp1806_1795[45] = 6;
    Object tmp1817_1806 = tmp1806_1795;
    Object tmp1817_1806 = tmp1806_1795;
    tmp1817_1806[46] = 6;
    tmp1817_1806[47] = 6;
    Object tmp1828_1817 = tmp1817_1806;
    Object tmp1828_1817 = tmp1817_1806;
    tmp1828_1817[48] = 5;
    tmp1828_1817[49] = 5;
    Object tmp1837_1828 = tmp1828_1817;
    Object tmp1837_1828 = tmp1828_1817;
    tmp1837_1828[50] = 5;
    tmp1837_1828[51] = 6;
    Object tmp1847_1837 = tmp1837_1828;
    Object tmp1847_1837 = tmp1837_1828;
    tmp1847_1837[52] = 6;
    tmp1847_1837[53] = 6;
    Object tmp1858_1847 = tmp1847_1837;
    Object tmp1858_1847 = tmp1847_1837;
    tmp1858_1847[54] = 6;
    tmp1858_1847[55] = 6;
    Object tmp1869_1858 = tmp1858_1847;
    Object tmp1869_1858 = tmp1858_1847;
    tmp1869_1858[56] = 6;
    tmp1869_1858[57] = 6;
    Object tmp1880_1869 = tmp1869_1858;
    Object tmp1880_1869 = tmp1869_1858;
    tmp1880_1869[58] = 7;
    tmp1880_1869[59] = 8;
    Object tmp1891_1880 = tmp1880_1869;
    Object tmp1891_1880 = tmp1880_1869;
    tmp1891_1880[60] = 15;
    tmp1891_1880[61] = 6;
    Object tmp1902_1891 = tmp1891_1880;
    Object tmp1902_1891 = tmp1891_1880;
    tmp1902_1891[62] = 12;
    tmp1902_1891[63] = 10;
    Object tmp1913_1902 = tmp1902_1891;
    Object tmp1913_1902 = tmp1902_1891;
    tmp1913_1902[64] = 13;
    tmp1913_1902[65] = 6;
    Object tmp1924_1913 = tmp1913_1902;
    Object tmp1924_1913 = tmp1913_1902;
    tmp1924_1913[66] = 7;
    tmp1924_1913[67] = 7;
    Object tmp1935_1924 = tmp1924_1913;
    Object tmp1935_1924 = tmp1924_1913;
    tmp1935_1924[68] = 7;
    tmp1935_1924[69] = 7;
    Object tmp1946_1935 = tmp1935_1924;
    Object tmp1946_1935 = tmp1935_1924;
    tmp1946_1935[70] = 7;
    tmp1946_1935[71] = 7;
    Object tmp1957_1946 = tmp1946_1935;
    Object tmp1957_1946 = tmp1946_1935;
    tmp1957_1946[72] = 7;
    tmp1957_1946[73] = 7;
    Object tmp1968_1957 = tmp1957_1946;
    Object tmp1968_1957 = tmp1957_1946;
    tmp1968_1957[74] = 7;
    tmp1968_1957[75] = 7;
    Object tmp1979_1968 = tmp1968_1957;
    Object tmp1979_1968 = tmp1968_1957;
    tmp1979_1968[76] = 7;
    tmp1979_1968[77] = 7;
    Object tmp1990_1979 = tmp1979_1968;
    Object tmp1990_1979 = tmp1979_1968;
    tmp1990_1979[78] = 7;
    tmp1990_1979[79] = 7;
    Object tmp2001_1990 = tmp1990_1979;
    Object tmp2001_1990 = tmp1990_1979;
    tmp2001_1990[80] = 7;
    tmp2001_1990[81] = 7;
    Object tmp2012_2001 = tmp2001_1990;
    Object tmp2012_2001 = tmp2001_1990;
    tmp2012_2001[82] = 7;
    tmp2012_2001[83] = 7;
    Object tmp2023_2012 = tmp2012_2001;
    Object tmp2023_2012 = tmp2012_2001;
    tmp2023_2012[84] = 7;
    tmp2023_2012[85] = 7;
    Object tmp2034_2023 = tmp2023_2012;
    Object tmp2034_2023 = tmp2023_2012;
    tmp2034_2023[86] = 7;
    tmp2034_2023[87] = 7;
    Object tmp2045_2034 = tmp2034_2023;
    Object tmp2045_2034 = tmp2034_2023;
    tmp2045_2034[88] = 8;
    tmp2045_2034[89] = 7;
    Object tmp2056_2045 = tmp2045_2034;
    Object tmp2056_2045 = tmp2045_2034;
    tmp2056_2045[90] = 8;
    tmp2056_2045[91] = 13;
    Object tmp2067_2056 = tmp2056_2045;
    Object tmp2067_2056 = tmp2056_2045;
    tmp2067_2056[92] = 19;
    tmp2067_2056[93] = 13;
    Object tmp2078_2067 = tmp2067_2056;
    Object tmp2078_2067 = tmp2067_2056;
    tmp2078_2067[94] = 14;
    tmp2078_2067[95] = 6;
    Object tmp2089_2078 = tmp2078_2067;
    Object tmp2089_2078 = tmp2078_2067;
    tmp2089_2078[96] = 15;
    tmp2089_2078[97] = 5;
    Object tmp2099_2089 = tmp2089_2078;
    Object tmp2099_2089 = tmp2089_2078;
    tmp2099_2089[98] = 6;
    tmp2099_2089[99] = 5;
    Object tmp2109_2099 = tmp2099_2089;
    Object tmp2109_2099 = tmp2099_2089;
    tmp2109_2099[100] = 6;
    tmp2109_2099[101] = 5;
    Object tmp2119_2109 = tmp2109_2099;
    Object tmp2119_2109 = tmp2109_2099;
    tmp2119_2109[102] = 6;
    tmp2119_2109[103] = 6;
    Object tmp2130_2119 = tmp2119_2109;
    Object tmp2130_2119 = tmp2119_2109;
    tmp2130_2119[104] = 6;
    tmp2130_2119[105] = 5;
    Object tmp2140_2130 = tmp2130_2119;
    Object tmp2140_2130 = tmp2130_2119;
    tmp2140_2130[106] = 7;
    tmp2140_2130[107] = 7;
    Object tmp2151_2140 = tmp2140_2130;
    Object tmp2151_2140 = tmp2140_2130;
    tmp2151_2140[108] = 6;
    tmp2151_2140[109] = 6;
    Object tmp2162_2151 = tmp2151_2140;
    Object tmp2162_2151 = tmp2151_2140;
    tmp2162_2151[110] = 6;
    tmp2162_2151[111] = 5;
    Object tmp2172_2162 = tmp2162_2151;
    Object tmp2172_2162 = tmp2162_2151;
    tmp2172_2162[112] = 6;
    tmp2172_2162[113] = 7;
    Object tmp2183_2172 = tmp2172_2162;
    Object tmp2183_2172 = tmp2172_2162;
    tmp2183_2172[114] = 6;
    tmp2183_2172[115] = 5;
    Object tmp2193_2183 = tmp2183_2172;
    Object tmp2193_2183 = tmp2183_2172;
    tmp2193_2183[116] = 5;
    tmp2193_2183[117] = 6;
    Object tmp2203_2193 = tmp2193_2183;
    Object tmp2203_2193 = tmp2193_2183;
    tmp2203_2193[118] = 7;
    tmp2203_2193[119] = 7;
    Object tmp2214_2203 = tmp2203_2193;
    Object tmp2214_2203 = tmp2203_2193;
    tmp2214_2203[120] = 7;
    tmp2214_2203[121] = 7;
    Object tmp2225_2214 = tmp2214_2203;
    Object tmp2225_2214 = tmp2214_2203;
    tmp2225_2214[122] = 7;
    tmp2225_2214[123] = 15;
    Object tmp2236_2225 = tmp2225_2214;
    Object tmp2236_2225 = tmp2225_2214;
    tmp2236_2225[124] = 11;
    tmp2236_2225[125] = 14;
    Object tmp2247_2236 = tmp2236_2225;
    Object tmp2247_2236 = tmp2236_2225;
    tmp2247_2236[126] = 13;
    tmp2247_2236[127] = 28;
    Object tmp2258_2247 = tmp2247_2236;
    Object tmp2258_2247 = tmp2247_2236;
    tmp2258_2247[''] = 20;
    tmp2258_2247[''] = 22;
    Object tmp2271_2258 = tmp2258_2247;
    Object tmp2271_2258 = tmp2258_2247;
    tmp2271_2258[''] = 20;
    tmp2271_2258[''] = 20;
    Object tmp2284_2271 = tmp2271_2258;
    Object tmp2284_2271 = tmp2271_2258;
    tmp2284_2271[''] = 22;
    tmp2284_2271[''] = 22;
    Object tmp2297_2284 = tmp2284_2271;
    Object tmp2297_2284 = tmp2284_2271;
    tmp2297_2284[''] = 22;
    tmp2297_2284[''] = 23;
    Object tmp2310_2297 = tmp2297_2284;
    Object tmp2310_2297 = tmp2297_2284;
    tmp2310_2297[''] = 22;
    tmp2310_2297[''] = 23;
    Object tmp2323_2310 = tmp2310_2297;
    Object tmp2323_2310 = tmp2310_2297;
    tmp2323_2310[''] = 23;
    tmp2323_2310[''] = 23;
    Object tmp2336_2323 = tmp2323_2310;
    Object tmp2336_2323 = tmp2323_2310;
    tmp2336_2323[''] = 23;
    tmp2336_2323[''] = 23;
    Object tmp2349_2336 = tmp2336_2323;
    Object tmp2349_2336 = tmp2336_2323;
    tmp2349_2336[''] = 24;
    tmp2349_2336[''] = 23;
    Object tmp2362_2349 = tmp2349_2336;
    Object tmp2362_2349 = tmp2349_2336;
    tmp2362_2349[''] = 24;
    tmp2362_2349[''] = 24;
    Object tmp2375_2362 = tmp2362_2349;
    Object tmp2375_2362 = tmp2362_2349;
    tmp2375_2362[''] = 22;
    tmp2375_2362[''] = 23;
    Object tmp2388_2375 = tmp2375_2362;
    Object tmp2388_2375 = tmp2375_2362;
    tmp2388_2375[''] = 24;
    tmp2388_2375[''] = 23;
    Object tmp2401_2388 = tmp2388_2375;
    Object tmp2401_2388 = tmp2388_2375;
    tmp2401_2388[''] = 23;
    tmp2401_2388[''] = 23;
    Object tmp2414_2401 = tmp2401_2388;
    Object tmp2414_2401 = tmp2401_2388;
    tmp2414_2401[''] = 23;
    tmp2414_2401[''] = 21;
    Object tmp2427_2414 = tmp2414_2401;
    Object tmp2427_2414 = tmp2414_2401;
    tmp2427_2414[''] = 22;
    tmp2427_2414[''] = 23;
    Object tmp2440_2427 = tmp2427_2414;
    Object tmp2440_2427 = tmp2427_2414;
    tmp2440_2427[''] = 22;
    tmp2440_2427[''] = 23;
    Object tmp2453_2440 = tmp2440_2427;
    Object tmp2453_2440 = tmp2440_2427;
    tmp2453_2440[''] = 23;
    tmp2453_2440[''] = 24;
    Object tmp2466_2453 = tmp2453_2440;
    Object tmp2466_2453 = tmp2453_2440;
    tmp2466_2453[' '] = 22;
    tmp2466_2453['¡'] = 21;
    Object tmp2479_2466 = tmp2466_2453;
    Object tmp2479_2466 = tmp2466_2453;
    tmp2479_2466['¢'] = 20;
    tmp2479_2466['£'] = 22;
    Object tmp2492_2479 = tmp2479_2466;
    Object tmp2492_2479 = tmp2479_2466;
    tmp2492_2479['¤'] = 22;
    tmp2492_2479['¥'] = 23;
    Object tmp2505_2492 = tmp2492_2479;
    Object tmp2505_2492 = tmp2492_2479;
    tmp2505_2492['¦'] = 23;
    tmp2505_2492['§'] = 21;
    Object tmp2518_2505 = tmp2505_2492;
    Object tmp2518_2505 = tmp2505_2492;
    tmp2518_2505['¨'] = 23;
    tmp2518_2505['©'] = 22;
    Object tmp2531_2518 = tmp2518_2505;
    Object tmp2531_2518 = tmp2518_2505;
    tmp2531_2518['ª'] = 22;
    tmp2531_2518['«'] = 24;
    Object tmp2544_2531 = tmp2531_2518;
    Object tmp2544_2531 = tmp2531_2518;
    tmp2544_2531['¬'] = 21;
    tmp2544_2531['­'] = 22;
    Object tmp2557_2544 = tmp2544_2531;
    Object tmp2557_2544 = tmp2544_2531;
    tmp2557_2544['®'] = 23;
    tmp2557_2544['¯'] = 23;
    Object tmp2570_2557 = tmp2557_2544;
    Object tmp2570_2557 = tmp2557_2544;
    tmp2570_2557['°'] = 21;
    tmp2570_2557['±'] = 21;
    Object tmp2583_2570 = tmp2570_2557;
    Object tmp2583_2570 = tmp2570_2557;
    tmp2583_2570['²'] = 22;
    tmp2583_2570['³'] = 21;
    Object tmp2596_2583 = tmp2583_2570;
    Object tmp2596_2583 = tmp2583_2570;
    tmp2596_2583['´'] = 23;
    tmp2596_2583['µ'] = 22;
    Object tmp2609_2596 = tmp2596_2583;
    Object tmp2609_2596 = tmp2596_2583;
    tmp2609_2596['¶'] = 23;
    tmp2609_2596['·'] = 23;
    Object tmp2622_2609 = tmp2609_2596;
    Object tmp2622_2609 = tmp2609_2596;
    tmp2622_2609['¸'] = 20;
    tmp2622_2609['¹'] = 22;
    Object tmp2635_2622 = tmp2622_2609;
    Object tmp2635_2622 = tmp2622_2609;
    tmp2635_2622['º'] = 22;
    tmp2635_2622['»'] = 22;
    Object tmp2648_2635 = tmp2635_2622;
    Object tmp2648_2635 = tmp2635_2622;
    tmp2648_2635['¼'] = 23;
    tmp2648_2635['½'] = 22;
    Object tmp2661_2648 = tmp2648_2635;
    Object tmp2661_2648 = tmp2648_2635;
    tmp2661_2648['¾'] = 22;
    tmp2661_2648['¿'] = 23;
    Object tmp2674_2661 = tmp2661_2648;
    Object tmp2674_2661 = tmp2661_2648;
    tmp2674_2661['À'] = 26;
    tmp2674_2661['Á'] = 26;
    Object tmp2687_2674 = tmp2674_2661;
    Object tmp2687_2674 = tmp2674_2661;
    tmp2687_2674['Â'] = 20;
    tmp2687_2674['Ã'] = 19;
    Object tmp2700_2687 = tmp2687_2674;
    Object tmp2700_2687 = tmp2687_2674;
    tmp2700_2687['Ä'] = 22;
    tmp2700_2687['Å'] = 23;
    Object tmp2713_2700 = tmp2700_2687;
    Object tmp2713_2700 = tmp2700_2687;
    tmp2713_2700['Æ'] = 22;
    tmp2713_2700['Ç'] = 25;
    Object tmp2726_2713 = tmp2713_2700;
    Object tmp2726_2713 = tmp2713_2700;
    tmp2726_2713['È'] = 26;
    tmp2726_2713['É'] = 26;
    Object tmp2739_2726 = tmp2726_2713;
    Object tmp2739_2726 = tmp2726_2713;
    tmp2739_2726['Ê'] = 26;
    tmp2739_2726['Ë'] = 27;
    Object tmp2752_2739 = tmp2739_2726;
    Object tmp2752_2739 = tmp2739_2726;
    tmp2752_2739['Ì'] = 27;
    tmp2752_2739['Í'] = 26;
    Object tmp2765_2752 = tmp2752_2739;
    Object tmp2765_2752 = tmp2752_2739;
    tmp2765_2752['Î'] = 24;
    tmp2765_2752['Ï'] = 25;
    Object tmp2778_2765 = tmp2765_2752;
    Object tmp2778_2765 = tmp2765_2752;
    tmp2778_2765['Ð'] = 19;
    tmp2778_2765['Ñ'] = 21;
    Object tmp2791_2778 = tmp2778_2765;
    Object tmp2791_2778 = tmp2778_2765;
    tmp2791_2778['Ò'] = 26;
    tmp2791_2778['Ó'] = 27;
    Object tmp2804_2791 = tmp2791_2778;
    Object tmp2804_2791 = tmp2791_2778;
    tmp2804_2791['Ô'] = 27;
    tmp2804_2791['Õ'] = 26;
    Object tmp2817_2804 = tmp2804_2791;
    Object tmp2817_2804 = tmp2804_2791;
    tmp2817_2804['Ö'] = 27;
    tmp2817_2804['×'] = 24;
    Object tmp2830_2817 = tmp2817_2804;
    Object tmp2830_2817 = tmp2817_2804;
    tmp2830_2817['Ø'] = 21;
    tmp2830_2817['Ù'] = 21;
    Object tmp2843_2830 = tmp2830_2817;
    Object tmp2843_2830 = tmp2830_2817;
    tmp2843_2830['Ú'] = 26;
    tmp2843_2830['Û'] = 26;
    Object tmp2856_2843 = tmp2843_2830;
    Object tmp2856_2843 = tmp2843_2830;
    tmp2856_2843['Ü'] = 28;
    tmp2856_2843['Ý'] = 27;
    Object tmp2869_2856 = tmp2856_2843;
    Object tmp2869_2856 = tmp2856_2843;
    tmp2869_2856['Þ'] = 27;
    tmp2869_2856['ß'] = 27;
    Object tmp2882_2869 = tmp2869_2856;
    Object tmp2882_2869 = tmp2869_2856;
    tmp2882_2869['à'] = 20;
    tmp2882_2869['á'] = 24;
    Object tmp2895_2882 = tmp2882_2869;
    Object tmp2895_2882 = tmp2882_2869;
    tmp2895_2882['â'] = 20;
    tmp2895_2882['ã'] = 21;
    Object tmp2908_2895 = tmp2895_2882;
    Object tmp2908_2895 = tmp2895_2882;
    tmp2908_2895['ä'] = 22;
    tmp2908_2895['å'] = 21;
    Object tmp2921_2908 = tmp2908_2895;
    Object tmp2921_2908 = tmp2908_2895;
    tmp2921_2908['æ'] = 21;
    tmp2921_2908['ç'] = 23;
    Object tmp2934_2921 = tmp2921_2908;
    Object tmp2934_2921 = tmp2921_2908;
    tmp2934_2921['è'] = 22;
    tmp2934_2921['é'] = 22;
    Object tmp2947_2934 = tmp2934_2921;
    Object tmp2947_2934 = tmp2934_2921;
    tmp2947_2934['ê'] = 25;
    tmp2947_2934['ë'] = 25;
    Object tmp2960_2947 = tmp2947_2934;
    Object tmp2960_2947 = tmp2947_2934;
    tmp2960_2947['ì'] = 24;
    tmp2960_2947['í'] = 24;
    Object tmp2973_2960 = tmp2960_2947;
    Object tmp2973_2960 = tmp2960_2947;
    tmp2973_2960['î'] = 26;
    tmp2973_2960['ï'] = 23;
    Object tmp2986_2973 = tmp2973_2960;
    Object tmp2986_2973 = tmp2973_2960;
    tmp2986_2973['ð'] = 26;
    tmp2986_2973['ñ'] = 27;
    Object tmp2999_2986 = tmp2986_2973;
    Object tmp2999_2986 = tmp2986_2973;
    tmp2999_2986['ò'] = 26;
    tmp2999_2986['ó'] = 26;
    Object tmp3012_2999 = tmp2999_2986;
    Object tmp3012_2999 = tmp2999_2986;
    tmp3012_2999['ô'] = 27;
    tmp3012_2999['õ'] = 27;
    Object tmp3025_3012 = tmp3012_2999;
    Object tmp3025_3012 = tmp3012_2999;
    tmp3025_3012['ö'] = 27;
    tmp3025_3012['÷'] = 27;
    Object tmp3038_3025 = tmp3025_3012;
    Object tmp3038_3025 = tmp3025_3012;
    tmp3038_3025['ø'] = 27;
    tmp3038_3025['ù'] = 28;
    Object tmp3051_3038 = tmp3038_3025;
    Object tmp3051_3038 = tmp3038_3025;
    tmp3051_3038['ú'] = 27;
    tmp3051_3038['û'] = 27;
    Object tmp3064_3051 = tmp3051_3038;
    Object tmp3064_3051 = tmp3051_3038;
    tmp3064_3051['ü'] = 27;
    tmp3064_3051['ý'] = 27;
    tmp3064_3051['þ'] = 27;
    tmp3064_3051['ÿ'] = 26;
    b = (byte[])localObject;
    localObject = new com/d/a/a/a/j;
    ((j)localObject).<init>();
    c = (j)localObject;
  }
  
  private j()
  {
    j.a locala = new com/d/a/a/a/j$a;
    locala.<init>();
    d = locala;
    b();
  }
  
  public static j a()
  {
    return c;
  }
  
  private void b()
  {
    int i = 0;
    IllegalStateException localIllegalStateException = null;
    for (;;)
    {
      byte[] arrayOfByte = b;
      int j = arrayOfByte.length;
      if (i >= j) {
        break;
      }
      int[] arrayOfInt = a;
      j = arrayOfInt[i];
      int k = arrayOfByte[i];
      j.a locala1 = new com/d/a/a/a/j$a;
      locala1.<init>(i, k);
      Object localObject;
      for (j.a locala2 = d;; locala2 = a[m])
      {
        m = 8;
        if (k <= m) {
          break label159;
        }
        k = (byte)(k + -8);
        m = j >>> k & 0xFF;
        localObject = a;
        if (localObject == null) {
          break;
        }
        localObject = a[m];
        if (localObject == null)
        {
          localObject = a;
          j.a locala3 = new com/d/a/a/a/j$a;
          locala3.<init>();
          localObject[m] = locala3;
        }
      }
      localIllegalStateException = new java/lang/IllegalStateException;
      localIllegalStateException.<init>("invalid dictionary: prefix not unique");
      throw localIllegalStateException;
      label159:
      m -= k;
      k = j << m & 0xFF;
      j = 1 << m;
      int m = k;
      for (;;)
      {
        int n = k + j;
        if (m >= n) {
          break;
        }
        localObject = a;
        localObject[m] = locala1;
        m += 1;
      }
      i += 1;
    }
  }
  
  final byte[] a(byte[] paramArrayOfByte)
  {
    ByteArrayOutputStream localByteArrayOutputStream = new java/io/ByteArrayOutputStream;
    localByteArrayOutputStream.<init>();
    j.a locala1 = d;
    int i = 0;
    j.a[] arrayOfa1 = null;
    j.a locala2 = locala1;
    int j = 0;
    locala1 = null;
    int k = 0;
    for (;;)
    {
      int m = paramArrayOfByte.length;
      int n = 8;
      if (i >= m) {
        break;
      }
      m = paramArrayOfByte[i] & 0xFF;
      j = j << 8 | m;
      k += 8;
      while (k >= n)
      {
        m = k + -8;
        m = j >>> m & 0xFF;
        locala2 = a[m];
        j.a[] arrayOfa2 = a;
        if (arrayOfa2 == null)
        {
          m = b;
          localByteArrayOutputStream.write(m);
          int i1 = c;
          k -= i1;
          locala2 = d;
        }
        else
        {
          k += -8;
        }
      }
      i += 1;
    }
    while (k > 0)
    {
      int i2 = 8 - k;
      i2 = j << i2 & 0xFF;
      paramArrayOfByte = a[i2];
      arrayOfa1 = a;
      if (arrayOfa1 != null) {
        break;
      }
      i = c;
      if (i > k) {
        break;
      }
      i = b;
      localByteArrayOutputStream.write(i);
      i2 = c;
      k -= i2;
      locala2 = d;
    }
    return localByteArrayOutputStream.toByteArray();
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */