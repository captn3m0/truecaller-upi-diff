package com.d.a.a.a;

import d.c;
import d.u;
import d.v;
import java.io.EOFException;
import java.io.IOException;

final class e$b
  implements u
{
  private final c c;
  private final c d;
  private final long e;
  private boolean f;
  private boolean g;
  
  private e$b(e parame, long paramLong)
  {
    parame = new d/c;
    parame.<init>();
    c = parame;
    parame = new d/c;
    parame.<init>();
    d = parame;
    e = paramLong;
  }
  
  private void b()
  {
    Object localObject1 = e.c(b);
    ((e.c)localObject1).m_();
    try
    {
      for (;;)
      {
        localObject1 = d;
        long l1 = b;
        long l2 = 0L;
        boolean bool1 = l1 < l2;
        if (bool1) {
          break;
        }
        boolean bool2 = g;
        if (bool2) {
          break;
        }
        bool2 = f;
        if (bool2) {
          break;
        }
        localObject1 = b;
        localObject1 = e.d((e)localObject1);
        if (localObject1 != null) {
          break;
        }
        localObject1 = b;
        e.e((e)localObject1);
      }
      return;
    }
    finally
    {
      e.c(b).b();
    }
  }
  
  public final long a(c paramc, long paramLong)
  {
    long l1 = 0L;
    boolean bool1 = paramLong < l1;
    if (!bool1) {
      synchronized (b)
      {
        b();
        boolean bool2 = f;
        if (!bool2)
        {
          Object localObject2 = b;
          localObject2 = e.d((e)localObject2);
          if (localObject2 == null)
          {
            localObject2 = d;
            long l2 = b;
            boolean bool3 = l2 < l1;
            if (!bool3)
            {
              l3 = -1;
              return l3;
            }
            localObject2 = d;
            Object localObject3 = d;
            long l4 = b;
            paramLong = Math.min(paramLong, l4);
            long l3 = ((c)localObject2).a(paramc, paramLong);
            ??? = b;
            l2 = a + l3;
            a = l2;
            ??? = b;
            l2 = a;
            ??? = b;
            ??? = e.a((e)???);
            ??? = e;
            int j = ((n)???).b();
            long l5 = j / 2;
            boolean bool4 = l2 < l5;
            int i;
            if (!bool4)
            {
              ??? = b;
              ??? = e.a((e)???);
              localObject2 = b;
              i = e.b((e)localObject2);
              localObject3 = b;
              l4 = a;
              ((d)???).a(i, l4);
              ??? = b;
              a = l1;
            }
            synchronized (e.a(b))
            {
              ??? = b;
              ??? = e.a((e)???);
              l2 = c + l3;
              c = l2;
              ??? = b;
              ??? = e.a((e)???);
              long l6 = c;
              localObject3 = b;
              localObject3 = e.a((e)localObject3);
              localObject3 = e;
              int k = ((n)localObject3).b();
              k /= 2;
              l4 = k;
              boolean bool5 = l6 < l4;
              if (!bool5)
              {
                ??? = b;
                ??? = e.a((e)???);
                i = 0;
                localObject2 = null;
                localObject3 = b;
                localObject3 = e.a((e)localObject3);
                l4 = c;
                ((d)???).a(0, l4);
                ??? = b;
                ??? = e.a((e)???);
                c = l1;
              }
              return l3;
            }
          }
          paramc = new java/io/IOException;
          localObject5 = new java/lang/StringBuilder;
          ??? = "stream was reset: ";
          ((StringBuilder)localObject5).<init>((String)???);
          ??? = b;
          ??? = e.d((e)???);
          ((StringBuilder)localObject5).append(???);
          localObject5 = ((StringBuilder)localObject5).toString();
          paramc.<init>((String)localObject5);
          throw paramc;
        }
        paramc = new java/io/IOException;
        localObject5 = "stream closed";
        paramc.<init>((String)localObject5);
        throw paramc;
      }
    }
    paramc = new java/lang/IllegalArgumentException;
    Object localObject5 = String.valueOf(paramLong);
    localObject5 = "byteCount < 0: ".concat((String)localObject5);
    paramc.<init>((String)localObject5);
  }
  
  final void a(d.e parame, long paramLong)
  {
    boolean bool1 = a;
    Object localObject1;
    if (!bool1)
    {
      localObject1 = b;
      bool1 = Thread.holdsLock(localObject1);
      if (bool1)
      {
        parame = new java/lang/AssertionError;
        parame.<init>();
        throw parame;
      }
    }
    long l1 = 0L;
    boolean bool2 = paramLong < l1;
    if (bool2) {
      synchronized (b)
      {
        boolean bool3 = g;
        c localc1 = d;
        long l2 = b + paramLong;
        long l3 = e;
        int i = 1;
        boolean bool4 = l2 < l3;
        int j;
        if (bool4)
        {
          j = 1;
        }
        else
        {
          j = 0;
          localc1 = null;
        }
        if (j != 0)
        {
          parame.h(paramLong);
          parame = b;
          a locala = a.h;
          parame.b(locala);
          return;
        }
        if (bool3)
        {
          parame.h(paramLong);
          return;
        }
        ??? = c;
        long l4 = parame.a((c)???, paramLong);
        l2 = -1;
        boolean bool5 = l4 < l2;
        if (bool5)
        {
          paramLong -= l4;
          synchronized (b)
          {
            c localc2 = d;
            long l5 = b;
            boolean bool6 = l5 < l1;
            if (bool6) {
              i = 0;
            }
            localObject1 = d;
            c localc3 = c;
            ((c)localObject1).a(localc3);
            if (i != 0)
            {
              localObject1 = b;
              localObject1.notifyAll();
            }
          }
        }
        parame = new java/io/EOFException;
        parame.<init>();
        throw parame;
      }
    }
  }
  
  public final void close()
  {
    e locale = b;
    boolean bool = true;
    try
    {
      f = bool;
      Object localObject1 = d;
      ((c)localObject1).s();
      localObject1 = b;
      localObject1.notifyAll();
      e.f(b);
      return;
    }
    finally {}
  }
  
  public final v l_()
  {
    return e.c(b);
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */