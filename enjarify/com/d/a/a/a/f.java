package com.d.a.a.a;

public final class f
{
  public static final d.f a = d.f.a(":status");
  public static final d.f b = d.f.a(":method");
  public static final d.f c = d.f.a(":path");
  public static final d.f d = d.f.a(":scheme");
  public static final d.f e = d.f.a(":authority");
  public static final d.f f = d.f.a(":host");
  public static final d.f g = d.f.a(":version");
  public final d.f h;
  public final d.f i;
  final int j;
  
  public f(d.f paramf1, d.f paramf2)
  {
    h = paramf1;
    i = paramf2;
    int k = paramf1.h() + 32;
    int m = paramf2.h();
    k += m;
    j = k;
  }
  
  public f(d.f paramf, String paramString)
  {
    this(paramf, paramString);
  }
  
  public f(String paramString1, String paramString2)
  {
    this(paramString1, paramString2);
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof f;
    if (bool1)
    {
      paramObject = (f)paramObject;
      d.f localf1 = h;
      d.f localf2 = h;
      bool1 = localf1.equals(localf2);
      if (bool1)
      {
        localf1 = i;
        paramObject = i;
        boolean bool2 = localf1.equals(paramObject);
        if (bool2) {
          return true;
        }
      }
      return false;
    }
    return false;
  }
  
  public final int hashCode()
  {
    int k = (h.hashCode() + 527) * 31;
    int m = i.hashCode();
    return k + m;
  }
  
  public final String toString()
  {
    Object[] arrayOfObject = new Object[2];
    String str = h.a();
    arrayOfObject[0] = str;
    str = i.a();
    arrayOfObject[1] = str;
    return String.format("%s: %s", arrayOfObject);
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */