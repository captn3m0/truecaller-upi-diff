package com.d.a.a.a;

import d.e;
import d.f;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.List;

final class o$a
  implements b
{
  private final e a;
  private final boolean b;
  private final k c;
  
  o$a(e parame, boolean paramBoolean)
  {
    a = parame;
    parame = new com/d/a/a/a/k;
    e locale = a;
    parame.<init>(locale);
    c = parame;
    b = paramBoolean;
  }
  
  private static IOException a(String paramString, Object... paramVarArgs)
  {
    IOException localIOException = new java/io/IOException;
    paramString = String.format(paramString, paramVarArgs);
    localIOException.<init>(paramString);
    throw localIOException;
  }
  
  private void a(b.a parama, int paramInt1, int paramInt2)
  {
    e locale1 = a;
    int i = locale1.j();
    int j = i * 8 + 4;
    boolean bool = false;
    int k = 1;
    if (paramInt2 == j)
    {
      n localn = new com/d/a/a/a/n;
      localn.<init>();
      j = 0;
      while (j < i)
      {
        e locale2 = a;
        int m = locale2.j();
        e locale3 = a;
        int n = locale3.j();
        int i1 = (0xFF000000 & m) >>> 24;
        int i2 = 16777215;
        m &= i2;
        localn.a(m, i1, n);
        j += 1;
      }
      paramInt1 &= k;
      if (paramInt1 != 0) {
        bool = true;
      }
      parama.a(bool, localn);
      return;
    }
    parama = new Object[2];
    Integer localInteger = Integer.valueOf(paramInt2);
    parama[0] = localInteger;
    localInteger = Integer.valueOf(i);
    parama[k] = localInteger;
    throw a("TYPE_SETTINGS length: %d != 4 + 8 * %d", parama);
  }
  
  public final void a() {}
  
  public final boolean a(b.a parama)
  {
    boolean bool1 = false;
    Object localObject1 = null;
    try
    {
      Object localObject2 = a;
      int i = ((e)localObject2).j();
      e locale = a;
      int j = locale.j();
      int n = -1 << -1 & i;
      int i2 = 1;
      if (n != 0)
      {
        n = 1;
      }
      else
      {
        n = 0;
        localObject3 = null;
      }
      int i3 = (0xFF000000 & j) >>> 24;
      j &= 0xFFFFFF;
      int i4 = -1 >>> 1;
      int m;
      if (n != 0)
      {
        n = (0x7FFF0000 & i) >>> 16;
        i &= (char)-1;
        int i5 = 3;
        if (n == i5)
        {
          n = 8;
          int i6 = 4;
          int i7;
          int i8;
          g localg;
          int k;
          switch (i)
          {
          case 5: 
          default: 
            parama = a;
            long l1 = j;
            parama.h(l1);
            return i2;
          case 9: 
            if (j == n)
            {
              localObject2 = a;
              i = ((e)localObject2).j();
              locale = a;
              j = locale.j();
              i &= i4;
              j &= i4;
              long l2 = j;
              long l3 = 0L;
              i7 = l2 < l3;
              if (i7 != 0)
              {
                parama.a(i, l2);
                return i2;
              }
              parama = new Object[i2];
              localObject2 = Long.valueOf(l2);
              parama[0] = localObject2;
              throw a("windowSizeIncrement was 0", parama);
            }
            parama = new Object[i2];
            localObject2 = Integer.valueOf(j);
            parama[0] = localObject2;
            throw a("TYPE_WINDOW_UPDATE length: %d != 8", parama);
          case 8: 
            i8 = a.j() & i4;
            localObject1 = c;
            j -= i7;
            localObject4 = ((k)localObject1).a(j);
            localg = g.c;
            parama.a(false, false, i8, (List)localObject4, localg);
            return i2;
          case 7: 
            if (j == n)
            {
              localObject2 = a;
              i = ((e)localObject2).j() & i4;
              locale = a;
              j = locale.j();
              localObject3 = a.c(j);
              if (localObject3 != null)
              {
                localObject1 = f.b;
                parama.a(i);
                return i2;
              }
              parama = new Object[i2];
              localObject2 = Integer.valueOf(j);
              parama[0] = localObject2;
              throw a("TYPE_GOAWAY unexpected error code: %d", parama);
            }
            parama = new Object[i2];
            localObject2 = Integer.valueOf(j);
            parama[0] = localObject2;
            throw a("TYPE_GOAWAY length: %d != 8", parama);
          case 6: 
            if (j == i7)
            {
              localObject2 = a;
              i = ((e)localObject2).j();
              k = b;
              n = i & 0x1;
              if (n == i2)
              {
                n = 1;
              }
              else
              {
                n = 0;
                localObject3 = null;
              }
              if (k == n)
              {
                k = 1;
              }
              else
              {
                k = 0;
                locale = null;
              }
              parama.a(k, i, 0);
              return i2;
            }
            parama = new Object[i2];
            localObject2 = Integer.valueOf(k);
            parama[0] = localObject2;
            throw a("TYPE_PING length: %d != 4", parama);
          case 4: 
            a(parama, i3, k);
            return i2;
          case 3: 
            if (k == n)
            {
              localObject2 = a;
              i = ((e)localObject2).j() & i4;
              locale = a;
              m = locale.j();
              localObject3 = a.a(m);
              if (localObject3 != null)
              {
                parama.a(i, (a)localObject3);
                return i2;
              }
              parama = new Object[i2];
              localObject2 = Integer.valueOf(m);
              parama[0] = localObject2;
              throw a("TYPE_RST_STREAM unexpected error code: %d", parama);
            }
            parama = new Object[i2];
            localObject2 = Integer.valueOf(m);
            parama[0] = localObject2;
            throw a("TYPE_RST_STREAM length: %d != 8", parama);
          case 2: 
            i8 = a.j() & i4;
            localObject2 = c;
            m -= i7;
            localObject4 = ((k)localObject2).a(m);
            i = i3 & 0x1;
            if (i != 0) {
              i9 = 1;
            } else {
              i9 = 0;
            }
            localg = g.b;
            parama.a(false, i9, i8, (List)localObject4, localg);
            return i2;
          }
          i = a.j();
          localObject3 = a;
          ((e)localObject3).j();
          int i9 = i & i4;
          a.i();
          localObject2 = c;
          m += -10;
          List localList = ((k)localObject2).a(m);
          i = i3 & 0x1;
          boolean bool2;
          if (i != 0) {
            bool2 = true;
          } else {
            bool2 = false;
          }
          i = i3 & 0x2;
          boolean bool3;
          if (i != 0) {
            bool3 = true;
          } else {
            bool3 = false;
          }
          Object localObject4 = g.a;
          parama.a(bool3, bool2, i9, localList, (g)localObject4);
          return i2;
        }
        parama = new java/net/ProtocolException;
        localObject1 = String.valueOf(n);
        localObject1 = "version != 3: ".concat((String)localObject1);
        parama.<init>((String)localObject1);
        throw parama;
      }
      i &= i4;
      int i1 = i3 & 0x1;
      if (i1 != 0) {
        bool1 = true;
      }
      Object localObject3 = a;
      parama.a(bool1, i, (e)localObject3, m);
      return i2;
    }
    catch (IOException localIOException) {}
    return false;
  }
  
  public final void close()
  {
    c.b.close();
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.o.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */