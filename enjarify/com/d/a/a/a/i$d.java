package com.d.a.a.a;

import d.d;
import d.f;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

final class i$d
  implements c
{
  private final d a;
  private final boolean b;
  private final d.c c;
  private final h.b d;
  private int e;
  private boolean f;
  
  i$d(d paramd, boolean paramBoolean)
  {
    a = paramd;
    b = paramBoolean;
    paramd = new d/c;
    paramd.<init>();
    c = paramd;
    paramd = new com/d/a/a/a/h$b;
    d.c localc = c;
    paramd.<init>(localc);
    d = paramd;
    e = 16384;
  }
  
  private void a(int paramInt1, int paramInt2, byte paramByte1, byte paramByte2)
  {
    Logger localLogger = i.b();
    Level localLevel = Level.FINE;
    boolean bool = localLogger.isLoggable(localLevel);
    localLevel = null;
    if (bool)
    {
      localLogger = i.b();
      String str = i.b.a(false, paramInt1, paramInt2, paramByte1, paramByte2);
      localLogger.fine(str);
    }
    int i = e;
    int j = 1;
    if (paramInt2 <= i)
    {
      i = -1 << -1 & paramInt1;
      if (i == 0)
      {
        i.a(a, paramInt2);
        localObject1 = a;
        paramByte1 &= 0xFF;
        ((d)localObject1).j(paramByte1);
        localObject1 = a;
        paramByte1 = paramByte2 & 0xFF;
        ((d)localObject1).j(paramByte1);
        localObject1 = a;
        paramInt1 &= -1 >>> 1;
        ((d)localObject1).h(paramInt1);
        return;
      }
      localObject1 = new Object[j];
      localObject2 = Integer.valueOf(paramInt1);
      localObject1[0] = localObject2;
      throw i.b("reserved bit set: %s", (Object[])localObject1);
    }
    Object localObject2 = new Object[2];
    Integer localInteger = Integer.valueOf(i);
    localObject2[0] = localInteger;
    Object localObject1 = Integer.valueOf(paramInt2);
    localObject2[j] = localObject1;
    throw i.b("FRAME_SIZE_ERROR length > %d: %d", (Object[])localObject2);
  }
  
  private void b(int paramInt, long paramLong)
  {
    for (;;)
    {
      long l1 = 0L;
      boolean bool1 = paramLong < l1;
      if (!bool1) {
        break;
      }
      long l2 = Math.min(e, paramLong);
      int i = (int)l2;
      long l3 = i;
      paramLong -= l3;
      byte b1 = 9;
      boolean bool2 = paramLong < l1;
      byte b2;
      if (!bool2)
      {
        b2 = 4;
      }
      else
      {
        b2 = 0;
        locald = null;
      }
      a(paramInt, i, b1, b2);
      d locald = a;
      d.c localc = c;
      locald.a_(localc, l3);
    }
  }
  
  public final void a()
  {
    try
    {
      boolean bool = f;
      if (!bool)
      {
        bool = b;
        if (!bool) {
          return;
        }
        localObject1 = i.b();
        localObject3 = Level.FINE;
        bool = ((Logger)localObject1).isLoggable((Level)localObject3);
        if (bool)
        {
          localObject1 = i.b();
          localObject3 = ">> CONNECTION %s";
          int i = 1;
          Object[] arrayOfObject = new Object[i];
          Object localObject4 = i.a();
          localObject4 = ((f)localObject4).f();
          arrayOfObject[0] = localObject4;
          localObject3 = String.format((String)localObject3, arrayOfObject);
          ((Logger)localObject1).fine((String)localObject3);
        }
        localObject1 = a;
        localObject3 = i.a();
        localObject3 = ((f)localObject3).i();
        ((d)localObject1).c((byte[])localObject3);
        localObject1 = a;
        ((d)localObject1).flush();
        return;
      }
      Object localObject1 = new java/io/IOException;
      Object localObject3 = "closed";
      ((IOException)localObject1).<init>((String)localObject3);
      throw ((Throwable)localObject1);
    }
    finally {}
  }
  
  public final void a(int paramInt, long paramLong)
  {
    try
    {
      boolean bool1 = f;
      if (!bool1)
      {
        long l = 0L;
        boolean bool2 = paramLong < l;
        if (bool2)
        {
          l = 2147483647L;
          bool2 = paramLong < l;
          if (!bool2)
          {
            i = 4;
            byte b1 = 8;
            a(paramInt, i, b1, (byte)0);
            localObject1 = a;
            int j = (int)paramLong;
            ((d)localObject1).h(j);
            localObject1 = a;
            ((d)localObject1).flush();
            return;
          }
        }
        localObject1 = "windowSizeIncrement == 0 || windowSizeIncrement > 0x7fffffffL: %s";
        int i = 1;
        Object[] arrayOfObject = new Object[i];
        localObject3 = Long.valueOf(paramLong);
        arrayOfObject[0] = localObject3;
        localObject1 = i.b((String)localObject1, arrayOfObject);
        throw ((Throwable)localObject1);
      }
      Object localObject1 = new java/io/IOException;
      Object localObject3 = "closed";
      ((IOException)localObject1).<init>((String)localObject3);
      throw ((Throwable)localObject1);
    }
    finally {}
  }
  
  public final void a(int paramInt, a parama)
  {
    try
    {
      boolean bool = f;
      if (!bool)
      {
        int i = s;
        int j = -1;
        if (i != j)
        {
          i = 4;
          j = 3;
          a(paramInt, i, j, (byte)0);
          localObject1 = a;
          int k = s;
          ((d)localObject1).h(k);
          localObject1 = a;
          ((d)localObject1).flush();
          return;
        }
        localObject1 = new java/lang/IllegalArgumentException;
        ((IllegalArgumentException)localObject1).<init>();
        throw ((Throwable)localObject1);
      }
      Object localObject1 = new java/io/IOException;
      parama = "closed";
      ((IOException)localObject1).<init>(parama);
      throw ((Throwable)localObject1);
    }
    finally {}
  }
  
  public final void a(int paramInt, a parama, byte[] paramArrayOfByte)
  {
    try
    {
      boolean bool = f;
      if (!bool)
      {
        int i = s;
        int j = -1;
        if (i != j)
        {
          i = paramArrayOfByte.length + 8;
          j = 7;
          a(0, i, j, (byte)0);
          d locald = a;
          locald.h(paramInt);
          localObject1 = a;
          int k = s;
          ((d)localObject1).h(k);
          paramInt = paramArrayOfByte.length;
          if (paramInt > 0)
          {
            localObject1 = a;
            ((d)localObject1).c(paramArrayOfByte);
          }
          localObject1 = a;
          ((d)localObject1).flush();
          return;
        }
        localObject1 = "errorCode.httpCode == -1";
        parama = new Object[0];
        localObject1 = i.b((String)localObject1, parama);
        throw ((Throwable)localObject1);
      }
      Object localObject1 = new java/io/IOException;
      parama = "closed";
      ((IOException)localObject1).<init>(parama);
      throw ((Throwable)localObject1);
    }
    finally {}
  }
  
  public final void a(n paramn)
  {
    try
    {
      boolean bool = f;
      if (!bool)
      {
        int i = e;
        int j = a & 0x20;
        if (j != 0)
        {
          paramn = d;
          i = 5;
          i = paramn[i];
        }
        e = i;
        byte b1 = 4;
        i = 1;
        j = 0;
        a(0, 0, b1, i);
        paramn = a;
        paramn.flush();
        return;
      }
      paramn = new java/io/IOException;
      String str = "closed";
      paramn.<init>(str);
      throw paramn;
    }
    finally {}
  }
  
  public final void a(boolean paramBoolean, int paramInt1, int paramInt2)
  {
    try
    {
      boolean bool = f;
      if (!bool)
      {
        bool = false;
        if (paramBoolean)
        {
          paramBoolean = true;
        }
        else
        {
          paramBoolean = false;
          localObject1 = null;
        }
        int i = 8;
        byte b1 = 6;
        a(0, i, b1, paramBoolean);
        localObject1 = a;
        ((d)localObject1).h(paramInt1);
        localObject1 = a;
        ((d)localObject1).h(paramInt2);
        localObject1 = a;
        ((d)localObject1).flush();
        return;
      }
      Object localObject1 = new java/io/IOException;
      String str = "closed";
      ((IOException)localObject1).<init>(str);
      throw ((Throwable)localObject1);
    }
    finally {}
  }
  
  public final void a(boolean paramBoolean, int paramInt1, d.c paramc, int paramInt2)
  {
    try
    {
      boolean bool = f;
      if (!bool)
      {
        bool = false;
        a(paramInt1, paramInt2, (byte)0, paramBoolean);
        if (paramInt2 > 0)
        {
          localObject1 = a;
          long l = paramInt2;
          ((d)localObject1).a_(paramc, l);
        }
        return;
      }
      Object localObject1 = new java/io/IOException;
      String str = "closed";
      ((IOException)localObject1).<init>(str);
      throw ((Throwable)localObject1);
    }
    finally {}
  }
  
  public final void a(boolean paramBoolean, int paramInt, List paramList)
  {
    try
    {
      boolean bool1 = f;
      if (!bool1)
      {
        bool1 = f;
        if (!bool1)
        {
          h.b localb = d;
          localb.a(paramList);
          paramList = c;
          long l1 = b;
          int i = e;
          long l2 = i;
          i = (int)Math.min(l2, l1);
          l2 = i;
          boolean bool2 = l1 < l2;
          byte b1;
          if (!bool2) {
            b1 = 4;
          } else {
            b1 = 0;
          }
          if (paramBoolean)
          {
            paramBoolean = b1 | 0x1;
            b1 = (byte)paramBoolean;
          }
          paramBoolean = true;
          a(paramInt, i, paramBoolean, b1);
          localObject1 = a;
          paramList = c;
          ((d)localObject1).a_(paramList, l2);
          paramBoolean = l1 < l2;
          if (paramBoolean)
          {
            l1 -= l2;
            b(paramInt, l1);
          }
          return;
        }
        localObject1 = new java/io/IOException;
        str = "closed";
        ((IOException)localObject1).<init>(str);
        throw ((Throwable)localObject1);
      }
      Object localObject1 = new java/io/IOException;
      String str = "closed";
      ((IOException)localObject1).<init>(str);
      throw ((Throwable)localObject1);
    }
    finally {}
  }
  
  public final void b()
  {
    try
    {
      boolean bool = f;
      if (!bool)
      {
        localObject1 = a;
        ((d)localObject1).flush();
        return;
      }
      Object localObject1 = new java/io/IOException;
      String str = "closed";
      ((IOException)localObject1).<init>(str);
      throw ((Throwable)localObject1);
    }
    finally {}
  }
  
  public final void b(n paramn)
  {
    try
    {
      boolean bool1 = f;
      if (!bool1)
      {
        int i = a;
        i = Integer.bitCount(i) * 6;
        int k = 4;
        int m = 0;
        a(0, i, k, (byte)0);
        for (;;)
        {
          i = 10;
          if (m >= i) {
            break;
          }
          boolean bool2 = paramn.a(m);
          if (bool2)
          {
            int j;
            if (m == k)
            {
              j = 3;
            }
            else
            {
              j = 7;
              if (m == j) {
                j = 4;
              } else {
                j = m;
              }
            }
            Object localObject1 = a;
            ((d)localObject1).i(j);
            localObject2 = a;
            localObject1 = d;
            int i1 = localObject1[m];
            ((d)localObject2).h(i1);
          }
          int n;
          m += 1;
        }
        paramn = a;
        paramn.flush();
        return;
      }
      paramn = new java/io/IOException;
      Object localObject2 = "closed";
      paramn.<init>((String)localObject2);
      throw paramn;
    }
    finally {}
  }
  
  public final int c()
  {
    return e;
  }
  
  public final void close()
  {
    boolean bool = true;
    try
    {
      f = bool;
      d locald = a;
      locald.close();
      return;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.i.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */