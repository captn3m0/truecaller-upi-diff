package com.d.a.a.a;

import java.util.zip.Inflater;

final class k$2
  extends Inflater
{
  k$2(k paramk) {}
  
  public final int inflate(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = super.inflate(paramArrayOfByte, paramInt1, paramInt2);
    if (i == 0)
    {
      boolean bool = needsDictionary();
      if (bool)
      {
        byte[] arrayOfByte = o.a;
        setDictionary(arrayOfByte);
        i = super.inflate(paramArrayOfByte, paramInt1, paramInt2);
      }
    }
    return i;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.k.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */