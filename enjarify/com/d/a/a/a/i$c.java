package com.d.a.a.a;

import d.e;
import d.f;
import d.u;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

final class i$c
  implements b
{
  final h.a a;
  private final e b;
  private final i.a c;
  private final boolean d;
  
  i$c(e parame, boolean paramBoolean)
  {
    b = parame;
    d = paramBoolean;
    parame = new com/d/a/a/a/i$a;
    Object localObject = b;
    parame.<init>((e)localObject);
    c = parame;
    parame = new com/d/a/a/a/h$a;
    localObject = c;
    parame.<init>((u)localObject);
    a = parame;
  }
  
  private List a(int paramInt1, short paramShort, byte paramByte, int paramInt2)
  {
    i.a locala = c;
    d = paramInt1;
    a = paramInt1;
    e = paramShort;
    b = paramByte;
    c = paramInt2;
    a.b();
    return a.c();
  }
  
  private void b()
  {
    b.j();
    b.h();
  }
  
  public final void a()
  {
    boolean bool1 = d;
    if (bool1) {
      return;
    }
    Object localObject1 = b;
    long l = i.a().h();
    localObject1 = ((e)localObject1).d(l);
    Object localObject2 = i.b();
    Level localLevel = Level.FINE;
    boolean bool2 = ((Logger)localObject2).isLoggable(localLevel);
    localLevel = null;
    int i = 1;
    if (bool2)
    {
      localObject2 = i.b();
      Object[] arrayOfObject = new Object[i];
      String str1 = ((f)localObject1).f();
      arrayOfObject[0] = str1;
      String str2 = String.format("<< CONNECTION %s", arrayOfObject);
      ((Logger)localObject2).fine(str2);
    }
    localObject2 = i.a();
    bool2 = ((f)localObject2).equals(localObject1);
    if (bool2) {
      return;
    }
    localObject2 = new Object[i];
    localObject1 = ((f)localObject1).a();
    localObject2[0] = localObject1;
    throw i.a("Expected a connection header but was %s", (Object[])localObject2);
  }
  
  public final boolean a(b.a parama)
  {
    boolean bool1 = false;
    Object localObject1 = null;
    try
    {
      Object localObject2 = b;
      long l1 = 9;
      ((e)localObject2).a(l1);
      localObject2 = b;
      int j = i.a((e)localObject2);
      int n = 1;
      int m;
      if (j >= 0)
      {
        int i1 = 16384;
        if (j <= i1)
        {
          Object localObject3 = b;
          byte b1 = (byte)(((e)localObject3).h() & 0xFF);
          Object localObject4 = b;
          int i3 = (byte)(((e)localObject4).h() & 0xFF);
          int i5 = b.j();
          int i7 = -1 >>> 1;
          int i9 = i5 & i7;
          Logger localLogger = i.b();
          Object localObject5 = Level.FINE;
          boolean bool2 = localLogger.isLoggable((Level)localObject5);
          if (bool2)
          {
            localLogger = i.b();
            localObject5 = i.b.a(n, i9, j, b1, i3);
            localLogger.fine((String)localObject5);
          }
          int i6 = 4;
          int i10 = 8;
          long l2;
          int k;
          Object localObject6;
          int i2;
          int i4;
          int i14;
          int i;
          switch (b1)
          {
          default: 
            parama = b;
            l2 = j;
            parama.h(l2);
            break;
          case 8: 
            if (j == i6)
            {
              localObject2 = b;
              long l3 = ((e)localObject2).j() & 0x7FFFFFFF;
              long l4 = 0L;
              k = l3 < l4;
              if (k != 0)
              {
                parama.a(i9, l3);
              }
              else
              {
                parama = new Object[n];
                localObject2 = Long.valueOf(l3);
                parama[0] = localObject2;
                throw i.a("windowSizeIncrement was 0", parama);
              }
            }
            else
            {
              parama = new Object[n];
              localObject2 = Integer.valueOf(k);
              parama[0] = localObject2;
              throw i.a("TYPE_WINDOW_UPDATE length !=4: %s", parama);
            }
            break;
          case 7: 
            if (k >= i10)
            {
              if (i9 == 0)
              {
                localObject6 = b;
                i1 = ((e)localObject6).j();
                localObject3 = b;
                i2 = ((e)localObject3).j();
                k -= i10;
                localObject4 = a.b(i2);
                if (localObject4 != null)
                {
                  localObject1 = f.b;
                  if (m > 0)
                  {
                    localObject1 = b;
                    long l5 = m;
                    ((e)localObject1).d(l5);
                  }
                  parama.a(i1);
                }
                else
                {
                  parama = new Object[n];
                  localObject2 = Integer.valueOf(i2);
                  parama[0] = localObject2;
                  throw i.a("TYPE_GOAWAY unexpected error code: %d", parama);
                }
              }
              else
              {
                parama = new Object[0];
                throw i.a("TYPE_GOAWAY streamId != 0", parama);
              }
            }
            else
            {
              parama = new Object[n];
              localObject2 = Integer.valueOf(m);
              parama[0] = localObject2;
              throw i.a("TYPE_GOAWAY length < 8: %s", parama);
            }
            break;
          case 6: 
            if (m == i10)
            {
              if (i9 == 0)
              {
                localObject2 = b;
                m = ((e)localObject2).j();
                localObject6 = b;
                i1 = ((e)localObject6).j();
                i2 = i3 & 0x1;
                if (i2 != 0) {
                  bool1 = true;
                }
                parama.a(bool1, m, i1);
              }
              else
              {
                parama = new Object[0];
                throw i.a("TYPE_PING streamId != 0", parama);
              }
            }
            else
            {
              parama = new Object[n];
              localObject2 = Integer.valueOf(m);
              parama[0] = localObject2;
              throw i.a("TYPE_PING length != 8: %s", parama);
            }
            break;
          case 5: 
            if (i9 != 0)
            {
              i1 = i3 & 0x8;
              short s1;
              if (i1 != 0)
              {
                localObject1 = b;
                s1 = (short)(((e)localObject1).h() & 0xFF);
              }
              localObject6 = b;
              i1 = ((e)localObject6).j() & i7;
              m = i.a(m + -4, i3, s1);
              localObject1 = a(m, s1, i3, i9);
              parama.a(i1, (List)localObject1);
            }
            else
            {
              parama = new Object[0];
              throw i.a("PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0", parama);
            }
            break;
          case 4: 
            if (i9 == 0)
            {
              i2 = i3 & 0x1;
              if (i2 != 0)
              {
                if (m != 0)
                {
                  parama = new Object[0];
                  throw i.a("FRAME_SIZE_ERROR ack frame should be empty!", parama);
                }
              }
              else
              {
                i2 = m % 6;
                if (i2 == 0)
                {
                  localObject3 = new com/d/a/a/a/n;
                  ((n)localObject3).<init>();
                  i3 = 0;
                  localObject4 = null;
                  while (i3 < m)
                  {
                    e locale = b;
                    i7 = locale.i();
                    localObject5 = b;
                    int i11 = ((e)localObject5).j();
                    int i8;
                    switch (i7)
                    {
                    default: 
                      parama = new Object[n];
                      localObject2 = Short.valueOf(i7);
                      parama[0] = localObject2;
                      throw i.a("PROTOCOL_ERROR invalid settings id: %s", parama);
                    case 5: 
                      if (i11 >= i1)
                      {
                        int i12 = 16777215;
                        if (i11 <= i12) {
                          break;
                        }
                      }
                      else
                      {
                        parama = new Object[n];
                        localObject2 = Integer.valueOf(i11);
                        parama[0] = localObject2;
                        throw i.a("PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s", parama);
                      }
                      break;
                    case 4: 
                      i8 = 7;
                      if (i11 < 0)
                      {
                        parama = new Object[0];
                        throw i.a("PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1", parama);
                      }
                      break;
                    case 3: 
                      i8 = 4;
                      break;
                    case 2: 
                      if ((i11 != 0) && (i11 != n))
                      {
                        parama = new Object[0];
                        throw i.a("PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1", parama);
                      }
                      break;
                    }
                    ((n)localObject3).a(i8, 0, i11);
                    i3 += 6;
                  }
                  parama.a(false, (n)localObject3);
                  i14 = ((n)localObject3).a();
                  if (i14 >= 0)
                  {
                    parama = a;
                    i = ((n)localObject3).a();
                    a = i;
                    b = i;
                    parama.a();
                  }
                }
                else
                {
                  parama = new Object[n];
                  localObject2 = Integer.valueOf(m);
                  parama[0] = localObject2;
                  throw i.a("TYPE_SETTINGS length %% 6 != 0: %s", parama);
                }
              }
            }
            else
            {
              parama = new Object[0];
              throw i.a("TYPE_SETTINGS streamId != 0", parama);
            }
            break;
          case 3: 
            if (m == i6)
            {
              if (i9 != 0)
              {
                localObject2 = b;
                m = ((e)localObject2).j();
                localObject6 = a.b(m);
                if (localObject6 != null)
                {
                  parama.a(i9, (a)localObject6);
                }
                else
                {
                  parama = new Object[n];
                  localObject2 = Integer.valueOf(m);
                  parama[0] = localObject2;
                  throw i.a("TYPE_RST_STREAM unexpected error code: %d", parama);
                }
              }
              else
              {
                parama = new Object[0];
                throw i.a("TYPE_RST_STREAM streamId == 0", parama);
              }
            }
            else
            {
              parama = new Object[n];
              localObject2 = Integer.valueOf(m);
              parama[0] = localObject2;
              throw i.a("TYPE_RST_STREAM length: %d != 4", parama);
            }
            break;
          case 2: 
            i14 = 5;
            if (m == i14)
            {
              if (i9 != 0)
              {
                b();
              }
              else
              {
                parama = new Object[0];
                throw i.a("TYPE_PRIORITY streamId == 0", parama);
              }
            }
            else
            {
              parama = new Object[n];
              localObject2 = Integer.valueOf(m);
              parama[0] = localObject2;
              throw i.a("TYPE_PRIORITY length: %d != 5", parama);
            }
            break;
          case 1: 
            if (i9 != 0)
            {
              i1 = i4 & 0x1;
              boolean bool3;
              if (i1 != 0) {
                bool3 = true;
              } else {
                bool3 = false;
              }
              i1 = i4 & 0x8;
              if (i1 != 0)
              {
                localObject1 = b;
                i = (short)(((e)localObject1).h() & 0xFF);
              }
              i1 = i4 & 0x20;
              if (i1 != 0)
              {
                b();
                m += -5;
              }
              m = i.a(m, i4, i);
              List localList = a(m, i, i4, i9);
              int i13 = 0;
              g localg = g.d;
              localObject5 = parama;
              parama.a(false, bool3, i9, localList, localg);
            }
            else
            {
              parama = new Object[0];
              throw i.a("PROTOCOL_ERROR: TYPE_HEADERS streamId == 0", parama);
            }
            break;
          case 0: 
            i1 = i4 & 0x1;
            if (i1 != 0)
            {
              i1 = 1;
            }
            else
            {
              i1 = 0;
              localObject6 = null;
            }
            i2 = i4 & 0x20;
            if (i2 != 0)
            {
              i2 = 1;
            }
            else
            {
              i2 = 0;
              localObject3 = null;
            }
            if (i2 == 0)
            {
              i2 = i4 & 0x8;
              short s2;
              if (i2 != 0)
              {
                localObject1 = b;
                s2 = (short)(((e)localObject1).h() & 0xFF);
              }
              m = i.a(m, i4, s2);
              localObject3 = b;
              parama.a(i1, i9, (e)localObject3, m);
              parama = b;
              l2 = s2;
              parama.h(l2);
            }
            else
            {
              parama = new Object[0];
              throw i.a("PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA", parama);
            }
            break;
          }
          return n;
        }
      }
      parama = new Object[n];
      localObject2 = Integer.valueOf(m);
      parama[0] = localObject2;
      throw i.a("FRAME_SIZE_ERROR: %s", parama);
    }
    catch (IOException localIOException) {}
    return false;
  }
  
  public final void close()
  {
    b.close();
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.i.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */