package com.d.a.a.a;

import d.e;
import d.n;
import d.u;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

final class h$a
{
  int a;
  int b;
  f[] c;
  int d;
  int e;
  int f;
  private final List g;
  private final e h;
  
  h$a(u paramu)
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    g = ((List)localObject);
    localObject = new f[8];
    c = ((f[])localObject);
    int i = c.length + -1;
    d = i;
    e = 0;
    f = 0;
    i = 4096;
    a = i;
    b = i;
    paramu = n.a(paramu);
    h = paramu;
  }
  
  private int a(int paramInt)
  {
    int i = 0;
    if (paramInt > 0)
    {
      f[] arrayOff1 = c;
      int j = arrayOff1.length + -1;
      for (;;)
      {
        k = d;
        if ((j < k) || (paramInt <= 0)) {
          break;
        }
        f localf1 = c[j];
        k = j;
        paramInt -= k;
        k = f;
        f localf2 = c[j];
        m = j;
        k -= m;
        f = k;
        k = e + -1;
        e = k;
        i += 1;
        j += -1;
      }
      f[] arrayOff2 = c;
      j = d;
      int k = j + 1;
      j = j + 1 + i;
      int m = e;
      System.arraycopy(arrayOff2, k, arrayOff2, j, m);
      paramInt = d + i;
      d = paramInt;
    }
    return i;
  }
  
  private int a(int paramInt1, int paramInt2)
  {
    paramInt1 &= paramInt2;
    if (paramInt1 < paramInt2) {
      return paramInt1;
    }
    paramInt1 = 0;
    int i;
    for (;;)
    {
      i = e();
      int j = i & 0x80;
      if (j == 0) {
        break;
      }
      i = (i & 0x7F) << paramInt1;
      paramInt2 += i;
      paramInt1 += 7;
    }
    paramInt1 = i << paramInt1;
    return paramInt2 + paramInt1;
  }
  
  private void a(f paramf)
  {
    List localList = g;
    localList.add(paramf);
    int i = j;
    int j = b;
    if (i > j)
    {
      d();
      return;
    }
    int k = f + i - j;
    a(k);
    j = e + 1;
    f[] arrayOff1 = c;
    int m = arrayOff1.length;
    if (j > m)
    {
      j = arrayOff1.length * 2;
      f[] arrayOff2 = new f[j];
      m = 0;
      int n = arrayOff1.length;
      int i1 = arrayOff1.length;
      System.arraycopy(arrayOff1, 0, arrayOff2, n, i1);
      arrayOff1 = c;
      k = arrayOff1.length + -1;
      d = k;
      c = arrayOff2;
    }
    j = d;
    k = j + -1;
    d = k;
    c[j] = paramf;
    int i2 = e + 1;
    e = i2;
    i2 = f + i;
    f = i2;
  }
  
  private int b(int paramInt)
  {
    return d + 1 + paramInt;
  }
  
  private d.f c(int paramInt)
  {
    boolean bool = d(paramInt);
    if (bool) {
      return ah;
    }
    f[] arrayOff = c;
    int i = h.a().length;
    paramInt -= i;
    paramInt = b(paramInt);
    return h;
  }
  
  private void d()
  {
    g.clear();
    Arrays.fill(c, null);
    int i = c.length + -1;
    d = i;
    e = 0;
    f = 0;
  }
  
  private static boolean d(int paramInt)
  {
    if (paramInt >= 0)
    {
      f[] arrayOff = h.a();
      int i = arrayOff.length;
      int j = 1;
      i -= j;
      if (paramInt <= i) {
        return j;
      }
    }
    return false;
  }
  
  private int e()
  {
    return h.h() & 0xFF;
  }
  
  private d.f f()
  {
    int i = e();
    int j = i & 0x80;
    int k = 128;
    if (j == k)
    {
      j = 1;
    }
    else
    {
      j = 0;
      localObject = null;
    }
    k = 127;
    i = a(i, k);
    if (j != 0)
    {
      localObject = j.a();
      e locale = h;
      long l1 = i;
      byte[] arrayOfByte = locale.g(l1);
      return d.f.a(((j)localObject).a(arrayOfByte));
    }
    Object localObject = h;
    long l2 = i;
    return ((e)localObject).d(l2);
  }
  
  final void a()
  {
    int i = b;
    int j = f;
    if (i < j)
    {
      if (i == 0)
      {
        d();
        return;
      }
      j -= i;
      a(j);
    }
  }
  
  final void b()
  {
    for (;;)
    {
      localObject1 = h;
      boolean bool1 = ((e)localObject1).e();
      if (bool1) {
        return;
      }
      localObject1 = h;
      int i = ((e)localObject1).h() & 0xFF;
      int j = 128;
      if (i == j) {
        break;
      }
      int m = i & 0x80;
      Object localObject2;
      int k;
      Object localObject3;
      if (m == j)
      {
        i = a(i, 127) + -1;
        boolean bool2 = d(i);
        if (bool2)
        {
          localObject1 = h.a()[i];
          localObject2 = g;
          ((List)localObject2).add(localObject1);
        }
        else
        {
          localObject2 = h.a();
          k = localObject2.length;
          k = i - k;
          k = b(k);
          if (k >= 0)
          {
            localObject3 = c;
            int n = localObject3.length + -1;
            if (k <= n)
            {
              localObject1 = g;
              localObject2 = localObject3[k];
              ((List)localObject1).add(localObject2);
              continue;
            }
          }
          localObject2 = new java/io/IOException;
          localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>("Header index too large ");
          i += 1;
          ((StringBuilder)localObject3).append(i);
          localObject1 = ((StringBuilder)localObject3).toString();
          ((IOException)localObject2).<init>((String)localObject1);
          throw ((Throwable)localObject2);
        }
      }
      else
      {
        k = 64;
        if (i == k)
        {
          localObject1 = h.a(f());
          localObject2 = f();
          localObject3 = new com/d/a/a/a/f;
          ((f)localObject3).<init>((d.f)localObject1, (d.f)localObject2);
          a((f)localObject3);
        }
        else
        {
          m = i & 0x40;
          if (m == k)
          {
            k = 63;
            i = a(i, k) + -1;
            localObject1 = c(i);
            localObject2 = f();
            localObject3 = new com/d/a/a/a/f;
            ((f)localObject3).<init>((d.f)localObject1, (d.f)localObject2);
            a((f)localObject3);
          }
          else
          {
            k = i & 0x20;
            m = 32;
            if (k == m)
            {
              k = 31;
              i = a(i, k);
              b = i;
              i = b;
              if (i >= 0)
              {
                k = a;
                if (i <= k)
                {
                  a();
                  continue;
                }
              }
              localObject1 = new java/io/IOException;
              localObject2 = new java/lang/StringBuilder;
              ((StringBuilder)localObject2).<init>("Invalid dynamic table size update ");
              m = b;
              ((StringBuilder)localObject2).append(m);
              localObject2 = ((StringBuilder)localObject2).toString();
              ((IOException)localObject1).<init>((String)localObject2);
              throw ((Throwable)localObject1);
            }
            else
            {
              k = 16;
              f localf;
              if ((i != k) && (i != 0))
              {
                k = 15;
                i = a(i, k) + -1;
                localObject1 = c(i);
                localObject2 = f();
                localObject3 = g;
                localf = new com/d/a/a/a/f;
                localf.<init>((d.f)localObject1, (d.f)localObject2);
                ((List)localObject3).add(localf);
              }
              else
              {
                localObject1 = h.a(f());
                localObject2 = f();
                localObject3 = g;
                localf = new com/d/a/a/a/f;
                localf.<init>((d.f)localObject1, (d.f)localObject2);
                ((List)localObject3).add(localf);
              }
            }
          }
        }
      }
    }
    Object localObject1 = new java/io/IOException;
    ((IOException)localObject1).<init>("index == 0");
    throw ((Throwable)localObject1);
  }
  
  public final List c()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    List localList = g;
    localArrayList.<init>(localList);
    g.clear();
    return localArrayList;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.h.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */