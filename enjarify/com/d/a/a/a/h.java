package com.d.a.a.a;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

final class h
{
  private static final f[] a;
  private static final Map b;
  
  static
  {
    int i = 61;
    Object localObject1 = new f[i];
    Object localObject2 = new com/d/a/a/a/f;
    d.f localf = f.e;
    ((f)localObject2).<init>(localf, "");
    int j = 0;
    localf = null;
    localObject1[0] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    Object localObject3 = f.b;
    ((f)localObject2).<init>((d.f)localObject3, "GET");
    localObject1[1] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    localObject3 = f.b;
    ((f)localObject2).<init>((d.f)localObject3, "POST");
    localObject1[2] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    localObject3 = f.c;
    ((f)localObject2).<init>((d.f)localObject3, "/");
    localObject1[3] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    localObject3 = f.c;
    ((f)localObject2).<init>((d.f)localObject3, "/index.html");
    localObject1[4] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    localObject3 = f.d;
    ((f)localObject2).<init>((d.f)localObject3, "http");
    localObject1[5] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    localObject3 = f.d;
    ((f)localObject2).<init>((d.f)localObject3, "https");
    localObject1[6] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    localObject3 = f.a;
    ((f)localObject2).<init>((d.f)localObject3, "200");
    localObject1[7] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    localObject3 = f.a;
    ((f)localObject2).<init>((d.f)localObject3, "204");
    localObject1[8] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    localObject3 = f.a;
    ((f)localObject2).<init>((d.f)localObject3, "206");
    localObject1[9] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    localObject3 = f.a;
    ((f)localObject2).<init>((d.f)localObject3, "304");
    localObject1[10] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    localObject3 = f.a;
    ((f)localObject2).<init>((d.f)localObject3, "400");
    localObject1[11] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    localObject3 = f.a;
    ((f)localObject2).<init>((d.f)localObject3, "404");
    localObject1[12] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    localObject3 = f.a;
    ((f)localObject2).<init>((d.f)localObject3, "500");
    localObject1[13] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("accept-charset", "");
    localObject1[14] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("accept-encoding", "gzip, deflate");
    localObject1[15] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("accept-language", "");
    localObject1[16] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("accept-ranges", "");
    localObject1[17] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("accept", "");
    localObject1[18] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("access-control-allow-origin", "");
    localObject1[19] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("age", "");
    localObject1[20] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("allow", "");
    localObject1[21] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("authorization", "");
    localObject1[22] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("cache-control", "");
    localObject1[23] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("content-disposition", "");
    localObject1[24] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("content-encoding", "");
    localObject1[25] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("content-language", "");
    localObject1[26] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("content-length", "");
    localObject1[27] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("content-location", "");
    localObject1[28] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("content-range", "");
    localObject1[29] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("content-type", "");
    localObject1[30] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("cookie", "");
    localObject1[31] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("date", "");
    localObject1[32] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("etag", "");
    localObject1[33] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("expect", "");
    localObject1[34] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("expires", "");
    localObject1[35] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("from", "");
    localObject1[36] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("host", "");
    localObject1[37] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("if-match", "");
    localObject1[38] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("if-modified-since", "");
    localObject1[39] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("if-none-match", "");
    localObject1[40] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("if-range", "");
    localObject1[41] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("if-unmodified-since", "");
    localObject1[42] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("last-modified", "");
    localObject1[43] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("link", "");
    localObject1[44] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("location", "");
    localObject1[45] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("max-forwards", "");
    localObject1[46] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("proxy-authenticate", "");
    localObject1[47] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("proxy-authorization", "");
    localObject1[48] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("range", "");
    localObject1[49] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("referer", "");
    localObject1[50] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("refresh", "");
    localObject1[51] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("retry-after", "");
    localObject1[52] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("server", "");
    localObject1[53] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("set-cookie", "");
    localObject1[54] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("strict-transport-security", "");
    localObject1[55] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("transfer-encoding", "");
    localObject1[56] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("user-agent", "");
    localObject1[57] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("vary", "");
    localObject1[58] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    ((f)localObject2).<init>("via", "");
    localObject1[59] = localObject2;
    localObject2 = new com/d/a/a/a/f;
    localObject3 = "www-authenticate";
    String str = "";
    ((f)localObject2).<init>((String)localObject3, str);
    int k = 60;
    localObject1[k] = localObject2;
    a = (f[])localObject1;
    localObject1 = new java/util/LinkedHashMap;
    localObject2 = a;
    int m = localObject2.length;
    ((LinkedHashMap)localObject1).<init>(m);
    for (;;)
    {
      localObject2 = a;
      k = localObject2.length;
      if (j >= k) {
        break;
      }
      localObject2 = h;
      boolean bool = ((Map)localObject1).containsKey(localObject2);
      if (!bool)
      {
        localObject2 = ah;
        localObject3 = Integer.valueOf(j);
        ((Map)localObject1).put(localObject2, localObject3);
      }
      j += 1;
    }
    b = Collections.unmodifiableMap((Map)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */