package com.d.a.a.a;

import d.t;
import java.io.InterruptedIOException;
import java.util.List;

public final class e
{
  long a;
  long b;
  final int c;
  final d d;
  List e;
  public final e.b f;
  final e.a g;
  public final e.c h;
  public final e.c i;
  private final List k;
  private a l;
  
  e(int paramInt, d paramd, boolean paramBoolean1, boolean paramBoolean2, List paramList)
  {
    long l1 = 0L;
    a = l1;
    e.c localc = new com/d/a/a/a/e$c;
    localc.<init>(this);
    h = localc;
    localc = new com/d/a/a/a/e$c;
    localc.<init>(this);
    i = localc;
    localc = null;
    l = null;
    if (paramd != null)
    {
      if (paramList != null)
      {
        c = paramInt;
        d = paramd;
        l1 = f.b();
        b = l1;
        localObject = new com/d/a/a/a/e$b;
        l1 = e.b();
        ((e.b)localObject).<init>(this, l1, (byte)0);
        f = ((e.b)localObject);
        localObject = new com/d/a/a/a/e$a;
        ((e.a)localObject).<init>(this);
        g = ((e.a)localObject);
        e.b.a(f, paramBoolean2);
        e.a.a(g, paramBoolean1);
        k = paramList;
        return;
      }
      localObject = new java/lang/NullPointerException;
      ((NullPointerException)localObject).<init>("requestHeaders == null");
      throw ((Throwable)localObject);
    }
    Object localObject = new java/lang/NullPointerException;
    ((NullPointerException)localObject).<init>("connection == null");
    throw ((Throwable)localObject);
  }
  
  private boolean d(a parama)
  {
    boolean bool = j;
    if (!bool)
    {
      bool = Thread.holdsLock(this);
      if (bool)
      {
        parama = new java/lang/AssertionError;
        parama.<init>();
        throw parama;
      }
    }
    try
    {
      Object localObject = l;
      if (localObject != null) {
        return false;
      }
      localObject = f;
      bool = e.b.a((e.b)localObject);
      if (bool)
      {
        localObject = g;
        bool = e.a.a((e.a)localObject);
        if (bool) {
          return false;
        }
      }
      l = parama;
      notifyAll();
      parama = d;
      int m = c;
      parama.b(m);
      return true;
    }
    finally {}
  }
  
  private void f()
  {
    try
    {
      wait();
      return;
    }
    catch (InterruptedException localInterruptedException)
    {
      InterruptedIOException localInterruptedIOException = new java/io/InterruptedIOException;
      localInterruptedIOException.<init>();
      throw localInterruptedIOException;
    }
  }
  
  final void a(long paramLong)
  {
    long l1 = b + paramLong;
    b = l1;
    l1 = 0L;
    boolean bool = paramLong < l1;
    if (bool) {
      notifyAll();
    }
  }
  
  public final void a(a parama)
  {
    boolean bool = d(parama);
    if (!bool) {
      return;
    }
    d locald = d;
    int m = c;
    locald.b(m, parama);
  }
  
  public final boolean a()
  {
    try
    {
      Object localObject1 = l;
      if (localObject1 != null) {
        return false;
      }
      localObject1 = f;
      boolean bool = e.b.a((e.b)localObject1);
      if (!bool)
      {
        localObject1 = f;
        bool = e.b.b((e.b)localObject1);
        if (!bool) {}
      }
      else
      {
        localObject1 = g;
        bool = e.a.a((e.a)localObject1);
        if (!bool)
        {
          localObject1 = g;
          bool = e.a.b((e.a)localObject1);
          if (!bool) {}
        }
        else
        {
          localObject1 = e;
          if (localObject1 != null) {
            return false;
          }
        }
      }
      return true;
    }
    finally {}
  }
  
  public final void b(a parama)
  {
    boolean bool = d(parama);
    if (!bool) {
      return;
    }
    d locald = d;
    int m = c;
    locald.a(m, parama);
  }
  
  public final boolean b()
  {
    int m = c;
    int n = 1;
    m &= n;
    if (m == n) {
      m = 1;
    } else {
      m = 0;
    }
    d locald = d;
    int i1 = b;
    if (i1 == m) {
      return n;
    }
    return false;
  }
  
  /* Error */
  public final List c()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 46	com/d/a/a/a/e:h	Lcom/d/a/a/a/e$c;
    //   6: astore_1
    //   7: aload_1
    //   8: invokevirtual 176	com/d/a/a/a/e$c:m_	()V
    //   11: aload_0
    //   12: getfield 169	com/d/a/a/a/e:e	Ljava/util/List;
    //   15: astore_1
    //   16: aload_1
    //   17: ifnonnull +19 -> 36
    //   20: aload_0
    //   21: getfield 50	com/d/a/a/a/e:l	Lcom/d/a/a/a/a;
    //   24: astore_1
    //   25: aload_1
    //   26: ifnonnull +10 -> 36
    //   29: aload_0
    //   30: invokespecial 121	com/d/a/a/a/e:f	()V
    //   33: goto -22 -> 11
    //   36: aload_0
    //   37: getfield 46	com/d/a/a/a/e:h	Lcom/d/a/a/a/e$c;
    //   40: astore_1
    //   41: aload_1
    //   42: invokevirtual 178	com/d/a/a/a/e$c:b	()V
    //   45: aload_0
    //   46: getfield 169	com/d/a/a/a/e:e	Ljava/util/List;
    //   49: astore_1
    //   50: aload_1
    //   51: ifnull +12 -> 63
    //   54: aload_0
    //   55: getfield 169	com/d/a/a/a/e:e	Ljava/util/List;
    //   58: astore_1
    //   59: aload_0
    //   60: monitorexit
    //   61: aload_1
    //   62: areturn
    //   63: new 143	java/io/IOException
    //   66: astore_1
    //   67: new 145	java/lang/StringBuilder
    //   70: astore_2
    //   71: ldc -109
    //   73: astore_3
    //   74: aload_2
    //   75: aload_3
    //   76: invokespecial 148	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   79: aload_0
    //   80: getfield 50	com/d/a/a/a/e:l	Lcom/d/a/a/a/a;
    //   83: astore_3
    //   84: aload_2
    //   85: aload_3
    //   86: invokevirtual 152	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   89: pop
    //   90: aload_2
    //   91: invokevirtual 156	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   94: astore_2
    //   95: aload_1
    //   96: aload_2
    //   97: invokespecial 157	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   100: aload_1
    //   101: athrow
    //   102: astore_1
    //   103: aload_0
    //   104: getfield 46	com/d/a/a/a/e:h	Lcom/d/a/a/a/e$c;
    //   107: astore_2
    //   108: aload_2
    //   109: invokevirtual 178	com/d/a/a/a/e$c:b	()V
    //   112: aload_1
    //   113: athrow
    //   114: astore_1
    //   115: aload_0
    //   116: monitorexit
    //   117: aload_1
    //   118: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	119	0	this	e
    //   6	95	1	localObject1	Object
    //   102	11	1	localObject2	Object
    //   114	4	1	localObject3	Object
    //   70	39	2	localObject4	Object
    //   73	13	3	localObject5	Object
    // Exception table:
    //   from	to	target	type
    //   11	15	102	finally
    //   20	24	102	finally
    //   29	33	102	finally
    //   2	6	114	finally
    //   7	11	114	finally
    //   36	40	114	finally
    //   41	45	114	finally
    //   45	49	114	finally
    //   54	58	114	finally
    //   63	66	114	finally
    //   67	70	114	finally
    //   75	79	114	finally
    //   79	83	114	finally
    //   85	90	114	finally
    //   90	94	114	finally
    //   96	100	114	finally
    //   100	102	114	finally
    //   103	107	114	finally
    //   108	112	114	finally
    //   112	114	114	finally
  }
  
  final void c(a parama)
  {
    try
    {
      a locala = l;
      if (locala == null)
      {
        l = parama;
        notifyAll();
      }
      return;
    }
    finally {}
  }
  
  public final t d()
  {
    try
    {
      Object localObject1 = e;
      if (localObject1 == null)
      {
        boolean bool = b();
        if (!bool)
        {
          localObject1 = new java/lang/IllegalStateException;
          String str = "reply before requesting the sink";
          ((IllegalStateException)localObject1).<init>(str);
          throw ((Throwable)localObject1);
        }
      }
      return g;
    }
    finally {}
  }
  
  final void e()
  {
    boolean bool1 = j;
    Object localObject1;
    if (!bool1)
    {
      bool1 = Thread.holdsLock(this);
      if (bool1)
      {
        localObject1 = new java/lang/AssertionError;
        ((AssertionError)localObject1).<init>();
        throw ((Throwable)localObject1);
      }
    }
    try
    {
      localObject1 = f;
      boolean bool2 = true;
      e.b.a((e.b)localObject1, bool2);
      bool1 = a();
      notifyAll();
      if (!bool1)
      {
        localObject1 = d;
        int m = c;
        ((d)localObject1).b(m);
      }
      return;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */