package com.d.a.a.a;

import d.e;
import d.m;
import d.n;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

final class k
{
  int a;
  final e b;
  private final m c;
  
  public k(e parame)
  {
    k.1 local1 = new com/d/a/a/a/k$1;
    local1.<init>(this, parame);
    parame = new com/d/a/a/a/k$2;
    parame.<init>(this);
    m localm = new d/m;
    localm.<init>(local1, parame);
    c = localm;
    parame = n.a(c);
    b = parame;
  }
  
  private d.f a()
  {
    int i = b.j();
    e locale = b;
    long l = i;
    return locale.d(l);
  }
  
  public final List a(int paramInt)
  {
    int i = a + paramInt;
    a = i;
    Object localObject1 = b;
    paramInt = ((e)localObject1).j();
    if (paramInt >= 0)
    {
      i = 1024;
      if (paramInt <= i)
      {
        localObject2 = new java/util/ArrayList;
        ((ArrayList)localObject2).<init>(paramInt);
        int j = 0;
        while (j < paramInt)
        {
          d.f localf1 = a().g();
          d.f localf2 = a();
          int k = localf1.h();
          if (k != 0)
          {
            f localf = new com/d/a/a/a/f;
            localf.<init>(localf1, localf2);
            ((List)localObject2).add(localf);
            j += 1;
          }
          else
          {
            localObject1 = new java/io/IOException;
            ((IOException)localObject1).<init>("name.size == 0");
            throw ((Throwable)localObject1);
          }
        }
        paramInt = a;
        if (paramInt > 0)
        {
          localObject1 = c;
          ((m)localObject1).b();
          paramInt = a;
          if (paramInt != 0)
          {
            localObject1 = new java/io/IOException;
            localObject2 = new java/lang/StringBuilder;
            ((StringBuilder)localObject2).<init>("compressedLimit > 0: ");
            j = a;
            ((StringBuilder)localObject2).append(j);
            localObject2 = ((StringBuilder)localObject2).toString();
            ((IOException)localObject1).<init>((String)localObject2);
            throw ((Throwable)localObject1);
          }
        }
        return (List)localObject2;
      }
      localObject2 = new java/io/IOException;
      localObject1 = String.valueOf(paramInt);
      localObject1 = "numberOfPairs > 1024: ".concat((String)localObject1);
      ((IOException)localObject2).<init>((String)localObject1);
      throw ((Throwable)localObject2);
    }
    Object localObject2 = new java/io/IOException;
    localObject1 = String.valueOf(paramInt);
    localObject1 = "numberOfPairs < 0: ".concat((String)localObject1);
    ((IOException)localObject2).<init>((String)localObject1);
    throw ((Throwable)localObject2);
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */