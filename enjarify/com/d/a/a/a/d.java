package com.d.a.a.a;

import com.d.a.a.j;
import com.d.a.u;
import java.io.Closeable;
import java.io.IOException;
import java.net.Socket;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class d
  implements Closeable
{
  private static final ExecutorService l;
  public final u a;
  final boolean b;
  long c;
  long d;
  public n e;
  final n f;
  final p g;
  final Socket h;
  public final c i;
  final d.c j;
  private final d.b m;
  private final Map n;
  private final String o;
  private int p;
  private int q;
  private boolean r;
  private long s;
  private final ExecutorService t;
  private Map u;
  private final m v;
  private int w;
  private boolean x;
  private final Set y;
  
  static
  {
    ThreadPoolExecutor localThreadPoolExecutor = new java/util/concurrent/ThreadPoolExecutor;
    TimeUnit localTimeUnit = TimeUnit.SECONDS;
    SynchronousQueue localSynchronousQueue = new java/util/concurrent/SynchronousQueue;
    localSynchronousQueue.<init>();
    ThreadFactory localThreadFactory = j.b("OkHttp FramedConnection");
    localThreadPoolExecutor.<init>(0, -1 >>> 1, 60, localTimeUnit, localSynchronousQueue, localThreadFactory);
    l = localThreadPoolExecutor;
  }
  
  private d(d.a parama)
  {
    Object localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    n = ((Map)localObject1);
    long l1 = System.nanoTime();
    s = l1;
    l1 = 0L;
    c = l1;
    localObject1 = new com/d/a/a/a/n;
    ((n)localObject1).<init>();
    e = ((n)localObject1);
    localObject1 = new com/d/a/a/a/n;
    ((n)localObject1).<init>();
    f = ((n)localObject1);
    localObject1 = null;
    x = false;
    Object localObject2 = new java/util/LinkedHashSet;
    ((LinkedHashSet)localObject2).<init>();
    y = ((Set)localObject2);
    localObject2 = f;
    a = ((u)localObject2);
    localObject2 = g;
    v = ((m)localObject2);
    boolean bool1 = h;
    b = bool1;
    localObject2 = e;
    m = ((d.b)localObject2);
    bool1 = h;
    int i3 = 1;
    int i4 = 2;
    int i1;
    if (bool1) {
      bool1 = true;
    } else {
      i1 = 2;
    }
    q = i1;
    boolean bool2 = h;
    if (bool2)
    {
      localObject2 = a;
      localObject3 = u.d;
      if (localObject2 == localObject3)
      {
        int i2 = q + i4;
        q = i2;
      }
    }
    boolean bool3 = h;
    if (bool3) {
      i4 = 1;
    }
    w = i4;
    bool3 = h;
    i4 = 7;
    if (bool3)
    {
      localObject2 = e;
      int i5 = 16777216;
      ((n)localObject2).a(i4, 0, i5);
    }
    localObject2 = b;
    o = ((String)localObject2);
    localObject2 = a;
    Object localObject3 = u.d;
    if (localObject2 == localObject3)
    {
      localObject2 = new com/d/a/a/a/i;
      ((i)localObject2).<init>();
      g = ((p)localObject2);
      localObject2 = new java/util/concurrent/ThreadPoolExecutor;
      int i6 = 1;
      long l2 = 60;
      TimeUnit localTimeUnit = TimeUnit.SECONDS;
      LinkedBlockingQueue localLinkedBlockingQueue = new java/util/concurrent/LinkedBlockingQueue;
      localLinkedBlockingQueue.<init>();
      localObject4 = new Object[i3];
      Object localObject5 = o;
      localObject4[0] = localObject5;
      localObject4 = String.format("OkHttp %s Push Observer", (Object[])localObject4);
      localObject5 = j.b((String)localObject4);
      localObject3 = localObject2;
      ((ThreadPoolExecutor)localObject2).<init>(0, i6, l2, localTimeUnit, localLinkedBlockingQueue, (ThreadFactory)localObject5);
      t = ((ExecutorService)localObject2);
      f.a(i4, 0, (char)-1);
      localObject2 = f;
      i3 = 5;
      i4 = 16384;
      ((n)localObject2).a(i3, 0, i4);
    }
    else
    {
      localObject2 = a;
      localObject4 = u.c;
      if (localObject2 != localObject4) {
        break label614;
      }
      localObject2 = new com/d/a/a/a/o;
      ((o)localObject2).<init>();
      g = ((p)localObject2);
      bool3 = false;
      localObject2 = null;
      t = null;
    }
    long l3 = f.b();
    d = l3;
    localObject2 = a;
    h = ((Socket)localObject2);
    localObject2 = g;
    Object localObject4 = d;
    boolean bool4 = b;
    localObject2 = ((p)localObject2).a((d.d)localObject4, bool4);
    i = ((c)localObject2);
    localObject2 = new com/d/a/a/a/d$c;
    localObject4 = g;
    parama = c;
    bool4 = b;
    parama = ((p)localObject4).a(parama, bool4);
    ((d.c)localObject2).<init>(this, parama, (byte)0);
    j = ((d.c)localObject2);
    parama = new java/lang/Thread;
    localObject1 = j;
    parama.<init>((Runnable)localObject1);
    parama.start();
    return;
    label614:
    parama = new java/lang/AssertionError;
    localObject1 = a;
    parama.<init>(localObject1);
    throw parama;
  }
  
  private void a(a parama)
  {
    synchronized (i)
    {
      try
      {
        boolean bool = r;
        if (bool) {
          return;
        }
        bool = true;
        r = bool;
        int i1 = p;
        c localc2 = i;
        byte[] arrayOfByte = j.a;
        localc2.a(i1, parama, arrayOfByte);
        return;
      }
      finally {}
    }
  }
  
  private void a(a parama1, a parama2)
  {
    boolean bool1 = k;
    if (!bool1)
    {
      bool1 = Thread.holdsLock(this);
      if (bool1)
      {
        parama1 = new java/lang/AssertionError;
        parama1.<init>();
        throw parama1;
      }
    }
    bool1 = false;
    Object localObject1 = null;
    int i1;
    try
    {
      a(parama1);
      i1 = 0;
      parama1 = null;
    }
    catch (IOException parama1) {}
    try
    {
      Object localObject2 = n;
      boolean bool2 = ((Map)localObject2).isEmpty();
      int i2 = 0;
      int i3;
      if (!bool2)
      {
        localObject2 = n;
        localObject2 = ((Map)localObject2).values();
        localObject3 = n;
        i3 = ((Map)localObject3).size();
        localObject3 = new e[i3];
        localObject2 = ((Collection)localObject2).toArray((Object[])localObject3);
        localObject2 = (e[])localObject2;
        localObject3 = n;
        ((Map)localObject3).clear();
        a(false);
      }
      else
      {
        bool2 = false;
        localObject2 = null;
      }
      Object localObject3 = u;
      Object localObject4;
      if (localObject3 != null)
      {
        localObject3 = u;
        localObject3 = ((Map)localObject3).values();
        localObject4 = u;
        int i4 = ((Map)localObject4).size();
        localObject4 = new l[i4];
        localObject3 = ((Collection)localObject3).toArray((Object[])localObject4);
        localObject3 = (l[])localObject3;
        u = null;
        localObject1 = localObject3;
      }
      if (localObject2 != null)
      {
        i3 = localObject2.length;
        localObject4 = parama1;
        i1 = 0;
        parama1 = null;
        while (i1 < i3)
        {
          Object localObject5 = localObject2[i1];
          try
          {
            ((e)localObject5).a(parama2);
          }
          catch (IOException localIOException)
          {
            if (localObject4 != null) {
              localObject4 = localIOException;
            }
          }
          i1 += 1;
        }
        parama1 = (a)localObject4;
      }
      if (localObject1 != null)
      {
        int i5 = localObject1.length;
        while (i2 < i5)
        {
          localObject2 = localObject1[i2];
          ((l)localObject2).a();
          i2 += 1;
        }
      }
      try
      {
        parama2 = i;
        parama2.close();
      }
      catch (IOException parama2)
      {
        if (parama1 == null) {
          parama1 = parama2;
        }
      }
      try
      {
        parama2 = h;
        parama2.close();
      }
      catch (IOException parama1) {}
      if (parama1 == null) {
        return;
      }
      throw parama1;
    }
    finally {}
  }
  
  private void a(boolean paramBoolean)
  {
    long l1;
    if (paramBoolean) {
      try
      {
        l1 = System.nanoTime();
      }
      finally
      {
        break label30;
      }
    } else {
      l1 = Long.MAX_VALUE;
    }
    s = l1;
    return;
    label30:
    throw ((Throwable)localObject);
  }
  
  private l c(int paramInt)
  {
    try
    {
      Map localMap = u;
      if (localMap != null)
      {
        localMap = u;
        Object localObject1 = Integer.valueOf(paramInt);
        localObject1 = localMap.remove(localObject1);
        localObject1 = (l)localObject1;
        return (l)localObject1;
      }
      return null;
    }
    finally {}
  }
  
  public final int a()
  {
    try
    {
      Object localObject1 = f;
      int i1 = a & 0x10;
      if (i1 != 0)
      {
        localObject1 = d;
        i1 = 4;
        int i2 = localObject1[i1];
        return i2;
      }
      return -1 >>> 1;
    }
    finally {}
  }
  
  final e a(int paramInt)
  {
    try
    {
      Map localMap = n;
      Object localObject1 = Integer.valueOf(paramInt);
      localObject1 = localMap.get(localObject1);
      localObject1 = (e)localObject1;
      return (e)localObject1;
    }
    finally {}
  }
  
  public final e a(List paramList, boolean paramBoolean)
  {
    boolean bool1 = paramBoolean ^ true;
    synchronized (i)
    {
      try
      {
        boolean bool2 = r;
        if (!bool2)
        {
          int i2 = q;
          int i1 = q + 2;
          q = i1;
          e locale = new com/d/a/a/a/e;
          Object localObject = locale;
          locale.<init>(i2, this, bool1, false, paramList);
          boolean bool3 = locale.a();
          if (bool3)
          {
            localObject = n;
            Integer localInteger = Integer.valueOf(i2);
            ((Map)localObject).put(localInteger, locale);
            bool3 = false;
            localObject = null;
            a(false);
          }
          localObject = i;
          ((c)localObject).a(bool1, i2, paramList);
          if (!paramBoolean)
          {
            paramList = i;
            paramList.b();
          }
          return locale;
        }
        paramList = new java/io/IOException;
        String str = "shutdown";
        paramList.<init>(str);
        throw paramList;
      }
      finally {}
    }
  }
  
  final void a(int paramInt, long paramLong)
  {
    ExecutorService localExecutorService = l;
    d.2 local2 = new com/d/a/a/a/d$2;
    Object[] arrayOfObject = new Object[2];
    Object localObject = o;
    arrayOfObject[0] = localObject;
    localObject = Integer.valueOf(paramInt);
    arrayOfObject[1] = localObject;
    localObject = local2;
    local2.<init>(this, "OkHttp Window Update %s stream %d", arrayOfObject, paramInt, paramLong);
    localExecutorService.execute(local2);
  }
  
  final void a(int paramInt, a parama)
  {
    ExecutorService localExecutorService = l;
    d.1 local1 = new com/d/a/a/a/d$1;
    Object[] arrayOfObject = new Object[2];
    Object localObject = o;
    arrayOfObject[0] = localObject;
    localObject = Integer.valueOf(paramInt);
    arrayOfObject[1] = localObject;
    localObject = local1;
    local1.<init>(this, "OkHttp %s stream %d", arrayOfObject, paramInt, parama);
    localExecutorService.submit(local1);
  }
  
  /* Error */
  public final void a(int paramInt, boolean paramBoolean, d.c paramc, long paramLong)
  {
    // Byte code:
    //   0: lconst_0
    //   1: lstore 6
    //   3: lload 4
    //   5: lload 6
    //   7: lcmp
    //   8: istore 8
    //   10: iload 8
    //   12: ifne +17 -> 29
    //   15: aload_0
    //   16: getfield 195	com/d/a/a/a/d:i	Lcom/d/a/a/a/c;
    //   19: iload_2
    //   20: iload_1
    //   21: aload_3
    //   22: iconst_0
    //   23: invokeinterface 450 5 0
    //   28: return
    //   29: lload 4
    //   31: lload 6
    //   33: lcmp
    //   34: istore 8
    //   36: iload 8
    //   38: ifle +226 -> 264
    //   41: aload_0
    //   42: monitorenter
    //   43: aload_0
    //   44: getfield 181	com/d/a/a/a/d:d	J
    //   47: lstore 9
    //   49: lload 9
    //   51: lload 6
    //   53: lcmp
    //   54: istore 11
    //   56: iload 11
    //   58: ifgt +58 -> 116
    //   61: aload_0
    //   62: getfield 88	com/d/a/a/a/d:n	Ljava/util/Map;
    //   65: astore 12
    //   67: iload_1
    //   68: invokestatic 300	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   71: astore 13
    //   73: aload 12
    //   75: aload 13
    //   77: invokeinterface 453 2 0
    //   82: istore 8
    //   84: iload 8
    //   86: ifeq +10 -> 96
    //   89: aload_0
    //   90: invokevirtual 456	java/lang/Object:wait	()V
    //   93: goto -50 -> 43
    //   96: new 289	java/io/IOException
    //   99: astore 14
    //   101: ldc_w 458
    //   104: astore 15
    //   106: aload 14
    //   108: aload 15
    //   110: invokespecial 357	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   113: aload 14
    //   115: athrow
    //   116: aload_0
    //   117: getfield 181	com/d/a/a/a/d:d	J
    //   120: lstore 9
    //   122: lload 4
    //   124: lload 9
    //   126: invokestatic 464	java/lang/Math:min	(JJ)J
    //   129: lstore 9
    //   131: lload 9
    //   133: l2i
    //   134: istore 16
    //   136: aload_0
    //   137: getfield 195	com/d/a/a/a/d:i	Lcom/d/a/a/a/c;
    //   140: astore 12
    //   142: aload 12
    //   144: invokeinterface 466 1 0
    //   149: istore 8
    //   151: iload 16
    //   153: iload 8
    //   155: invokestatic 469	java/lang/Math:min	(II)I
    //   158: istore 8
    //   160: aload_0
    //   161: getfield 181	com/d/a/a/a/d:d	J
    //   164: lstore 17
    //   166: iload 8
    //   168: i2l
    //   169: lstore 19
    //   171: lload 17
    //   173: lload 19
    //   175: lsub
    //   176: lstore 17
    //   178: aload_0
    //   179: lload 17
    //   181: putfield 181	com/d/a/a/a/d:d	J
    //   184: aload_0
    //   185: monitorexit
    //   186: lload 4
    //   188: lload 19
    //   190: lsub
    //   191: lstore 4
    //   193: aload_0
    //   194: getfield 195	com/d/a/a/a/d:i	Lcom/d/a/a/a/c;
    //   197: astore 13
    //   199: iload_2
    //   200: ifeq +21 -> 221
    //   203: lload 4
    //   205: lload 6
    //   207: lcmp
    //   208: istore 11
    //   210: iload 11
    //   212: ifne +9 -> 221
    //   215: iconst_1
    //   216: istore 11
    //   218: goto +6 -> 224
    //   221: iconst_0
    //   222: istore 11
    //   224: aload 13
    //   226: iload 11
    //   228: iload_1
    //   229: aload_3
    //   230: iload 8
    //   232: invokeinterface 450 5 0
    //   237: goto -208 -> 29
    //   240: astore 14
    //   242: goto +17 -> 259
    //   245: pop
    //   246: new 471	java/io/InterruptedIOException
    //   249: astore 14
    //   251: aload 14
    //   253: invokespecial 472	java/io/InterruptedIOException:<init>	()V
    //   256: aload 14
    //   258: athrow
    //   259: aload_0
    //   260: monitorexit
    //   261: aload 14
    //   263: athrow
    //   264: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	265	0	this	d
    //   0	265	1	paramInt	int
    //   0	265	2	paramBoolean	boolean
    //   0	265	3	paramc	d.c
    //   0	265	4	paramLong	long
    //   1	205	6	l1	long
    //   8	77	8	bool1	boolean
    //   149	82	8	i1	int
    //   47	85	9	l2	long
    //   54	173	11	bool2	boolean
    //   65	78	12	localObject1	Object
    //   71	154	13	localObject2	Object
    //   99	15	14	localIOException	IOException
    //   240	1	14	localObject3	Object
    //   249	13	14	localInterruptedIOException	java.io.InterruptedIOException
    //   104	5	15	str	String
    //   134	18	16	i2	int
    //   164	16	17	l3	long
    //   169	20	19	l4	long
    //   245	1	19	localInterruptedException	InterruptedException
    // Exception table:
    //   from	to	target	type
    //   43	47	240	finally
    //   61	65	240	finally
    //   67	71	240	finally
    //   75	82	240	finally
    //   89	93	240	finally
    //   96	99	240	finally
    //   108	113	240	finally
    //   113	116	240	finally
    //   116	120	240	finally
    //   124	129	240	finally
    //   136	140	240	finally
    //   142	149	240	finally
    //   153	158	240	finally
    //   160	164	240	finally
    //   179	184	240	finally
    //   184	186	240	finally
    //   246	249	240	finally
    //   251	256	240	finally
    //   256	259	240	finally
    //   259	261	240	finally
    //   43	47	245	java/lang/InterruptedException
    //   61	65	245	java/lang/InterruptedException
    //   67	71	245	java/lang/InterruptedException
    //   75	82	245	java/lang/InterruptedException
    //   89	93	245	java/lang/InterruptedException
    //   96	99	245	java/lang/InterruptedException
    //   108	113	245	java/lang/InterruptedException
    //   113	116	245	java/lang/InterruptedException
  }
  
  final e b(int paramInt)
  {
    try
    {
      Map localMap = n;
      Object localObject1 = Integer.valueOf(paramInt);
      localObject1 = localMap.remove(localObject1);
      localObject1 = (e)localObject1;
      if (localObject1 != null)
      {
        localMap = n;
        boolean bool = localMap.isEmpty();
        if (bool)
        {
          bool = true;
          a(bool);
        }
      }
      notifyAll();
      return (e)localObject1;
    }
    finally {}
  }
  
  public final void b()
  {
    i.b();
  }
  
  final void b(int paramInt, a parama)
  {
    i.a(paramInt, parama);
  }
  
  public final void close()
  {
    a locala1 = a.a;
    a locala2 = a.l;
    a(locala1, locala2);
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */