package com.d.a.a.a;

import d.d;
import d.e;
import d.f;
import java.io.IOException;
import java.util.logging.Logger;

public final class i
  implements p
{
  private static final Logger a = Logger.getLogger(i.b.class.getName());
  private static final f b = f.a("PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n");
  
  private static IOException c(String paramString, Object... paramVarArgs)
  {
    IOException localIOException = new java/io/IOException;
    paramString = String.format(paramString, paramVarArgs);
    localIOException.<init>(paramString);
    throw localIOException;
  }
  
  public final b a(e parame, boolean paramBoolean)
  {
    i.c localc = new com/d/a/a/a/i$c;
    localc.<init>(parame, paramBoolean);
    return localc;
  }
  
  public final c a(d paramd, boolean paramBoolean)
  {
    i.d locald = new com/d/a/a/a/i$d;
    locald.<init>(paramd, paramBoolean);
    return locald;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */