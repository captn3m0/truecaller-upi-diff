package com.d.a.a.a;

public enum a
{
  public final int s;
  public final int t;
  public final int u;
  
  static
  {
    a locala = new com/d/a/a/a/a;
    Object localObject1 = locala;
    locala.<init>("NO_ERROR", 0, 0, -1, 0);
    a = locala;
    localObject1 = new com/d/a/a/a/a;
    int i1 = 1;
    ((a)localObject1).<init>("PROTOCOL_ERROR", 1, i1, 1, 1);
    b = (a)localObject1;
    localObject1 = new com/d/a/a/a/a;
    int i2 = 1;
    int i3 = -1;
    Object localObject2 = localObject1;
    ((a)localObject1).<init>("INVALID_STREAM", 2, i2, 2, i3);
    c = (a)localObject1;
    localObject1 = new com/d/a/a/a/a;
    int i4 = -1;
    ((a)localObject1).<init>("UNSUPPORTED_VERSION", 3, i1, 4, i4);
    d = (a)localObject1;
    localObject1 = new com/d/a/a/a/a;
    localObject2 = localObject1;
    ((a)localObject1).<init>("STREAM_IN_USE", 4, i2, 8, i3);
    e = (a)localObject1;
    localObject1 = new com/d/a/a/a/a;
    ((a)localObject1).<init>("STREAM_ALREADY_CLOSED", 5, i1, 9, i4);
    f = (a)localObject1;
    localObject1 = new com/d/a/a/a/a;
    localObject2 = localObject1;
    ((a)localObject1).<init>("INTERNAL_ERROR", 6, 2, 6, 2);
    g = (a)localObject1;
    localObject1 = new com/d/a/a/a/a;
    ((a)localObject1).<init>("FLOW_CONTROL_ERROR", 7, 3, 7, i4);
    h = (a)localObject1;
    localObject1 = new com/d/a/a/a/a;
    i3 = -1;
    localObject2 = localObject1;
    ((a)localObject1).<init>("STREAM_CLOSED", 8, 5, -1, i3);
    i = (a)localObject1;
    localObject1 = new com/d/a/a/a/a;
    ((a)localObject1).<init>("FRAME_TOO_LARGE", 9, 6, 11, i4);
    j = (a)localObject1;
    localObject1 = new com/d/a/a/a/a;
    localObject2 = localObject1;
    ((a)localObject1).<init>("REFUSED_STREAM", 10, 7, 3, i3);
    k = (a)localObject1;
    localObject1 = new com/d/a/a/a/a;
    ((a)localObject1).<init>("CANCEL", 11, 8, 5, i4);
    l = (a)localObject1;
    localObject1 = new com/d/a/a/a/a;
    int i5 = -1;
    localObject2 = localObject1;
    ((a)localObject1).<init>("COMPRESSION_ERROR", 12, 9, i5, i3);
    m = (a)localObject1;
    localObject1 = new com/d/a/a/a/a;
    int i6 = -1;
    ((a)localObject1).<init>("CONNECT_ERROR", 13, 10, i6, i4);
    n = (a)localObject1;
    localObject1 = new com/d/a/a/a/a;
    localObject2 = localObject1;
    ((a)localObject1).<init>("ENHANCE_YOUR_CALM", 14, 11, i5, i3);
    o = (a)localObject1;
    localObject1 = new com/d/a/a/a/a;
    ((a)localObject1).<init>("INADEQUATE_SECURITY", 15, 12, i6, i4);
    p = (a)localObject1;
    localObject1 = new com/d/a/a/a/a;
    localObject2 = localObject1;
    ((a)localObject1).<init>("HTTP_1_1_REQUIRED", 16, 13, i5, i3);
    q = (a)localObject1;
    localObject1 = new com/d/a/a/a/a;
    ((a)localObject1).<init>("INVALID_CREDENTIALS", 17, -1, 10, i4);
    r = (a)localObject1;
    localObject1 = new a[18];
    localObject2 = a;
    localObject1[0] = localObject2;
    localObject2 = b;
    localObject1[1] = localObject2;
    localObject2 = c;
    localObject1[2] = localObject2;
    localObject2 = d;
    localObject1[3] = localObject2;
    localObject2 = e;
    localObject1[4] = localObject2;
    localObject2 = f;
    localObject1[5] = localObject2;
    localObject2 = g;
    localObject1[6] = localObject2;
    localObject2 = h;
    localObject1[7] = localObject2;
    localObject2 = i;
    localObject1[8] = localObject2;
    localObject2 = j;
    localObject1[9] = localObject2;
    localObject2 = k;
    localObject1[10] = localObject2;
    localObject2 = l;
    localObject1[11] = localObject2;
    localObject2 = m;
    localObject1[12] = localObject2;
    localObject2 = n;
    localObject1[13] = localObject2;
    localObject2 = o;
    localObject1[14] = localObject2;
    localObject2 = p;
    localObject1[15] = localObject2;
    localObject2 = q;
    localObject1[16] = localObject2;
    localObject2 = r;
    localObject1[17] = localObject2;
    v = (a[])localObject1;
  }
  
  private a(int paramInt2, int paramInt3, int paramInt4)
  {
    s = paramInt2;
    t = paramInt3;
    u = paramInt4;
  }
  
  public static a a(int paramInt)
  {
    a[] arrayOfa = values();
    int i1 = arrayOfa.length;
    int i2 = 0;
    while (i2 < i1)
    {
      a locala = arrayOfa[i2];
      int i3 = t;
      if (i3 == paramInt) {
        return locala;
      }
      i2 += 1;
    }
    return null;
  }
  
  public static a b(int paramInt)
  {
    a[] arrayOfa = values();
    int i1 = arrayOfa.length;
    int i2 = 0;
    while (i2 < i1)
    {
      a locala = arrayOfa[i2];
      int i3 = s;
      if (i3 == paramInt) {
        return locala;
      }
      i2 += 1;
    }
    return null;
  }
  
  public static a c(int paramInt)
  {
    a[] arrayOfa = values();
    int i1 = arrayOfa.length;
    int i2 = 0;
    while (i2 < i1)
    {
      a locala = arrayOfa[i2];
      int i3 = u;
      if (i3 == paramInt) {
        return locala;
      }
      i2 += 1;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */