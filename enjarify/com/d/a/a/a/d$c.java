package com.d.a.a.a;

import com.d.a.a.f;
import com.d.a.u;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;

final class d$c
  extends f
  implements b.a
{
  final b b;
  
  private d$c(d paramd, b paramb)
  {
    super("OkHttp %s", arrayOfObject);
    b = paramb;
  }
  
  /* Error */
  public final void a()
  {
    // Byte code:
    //   0: getstatic 38	com/d/a/a/a/a:g	Lcom/d/a/a/a/a;
    //   3: astore_1
    //   4: getstatic 38	com/d/a/a/a/a:g	Lcom/d/a/a/a/a;
    //   7: astore_2
    //   8: aload_0
    //   9: getfield 12	com/d/a/a/a/d$c:c	Lcom/d/a/a/a/d;
    //   12: astore_3
    //   13: aload_3
    //   14: getfield 41	com/d/a/a/a/d:b	Z
    //   17: istore 4
    //   19: iload 4
    //   21: ifne +14 -> 35
    //   24: aload_0
    //   25: getfield 29	com/d/a/a/a/d$c:b	Lcom/d/a/a/a/b;
    //   28: astore_3
    //   29: aload_3
    //   30: invokeinterface 46 1 0
    //   35: aload_0
    //   36: getfield 29	com/d/a/a/a/d$c:b	Lcom/d/a/a/a/b;
    //   39: astore_3
    //   40: aload_3
    //   41: aload_0
    //   42: invokeinterface 49 2 0
    //   47: istore 4
    //   49: iload 4
    //   51: ifne -16 -> 35
    //   54: getstatic 51	com/d/a/a/a/a:a	Lcom/d/a/a/a/a;
    //   57: astore_1
    //   58: getstatic 54	com/d/a/a/a/a:l	Lcom/d/a/a/a/a;
    //   61: astore_2
    //   62: aload_0
    //   63: getfield 12	com/d/a/a/a/d$c:c	Lcom/d/a/a/a/d;
    //   66: astore_3
    //   67: aload_3
    //   68: aload_1
    //   69: aload_2
    //   70: invokestatic 57	com/d/a/a/a/d:a	(Lcom/d/a/a/a/d;Lcom/d/a/a/a/a;Lcom/d/a/a/a/a;)V
    //   73: aload_0
    //   74: getfield 29	com/d/a/a/a/d$c:b	Lcom/d/a/a/a/b;
    //   77: invokestatic 62	com/d/a/a/j:a	(Ljava/io/Closeable;)V
    //   80: return
    //   81: astore_3
    //   82: goto +20 -> 102
    //   85: pop
    //   86: getstatic 64	com/d/a/a/a/a:b	Lcom/d/a/a/a/a;
    //   89: astore_1
    //   90: getstatic 64	com/d/a/a/a/a:b	Lcom/d/a/a/a/a;
    //   93: astore_2
    //   94: aload_0
    //   95: getfield 12	com/d/a/a/a/d$c:c	Lcom/d/a/a/a/d;
    //   98: astore_3
    //   99: goto -32 -> 67
    //   102: aload_0
    //   103: getfield 12	com/d/a/a/a/d$c:c	Lcom/d/a/a/a/d;
    //   106: astore 5
    //   108: aload 5
    //   110: aload_1
    //   111: aload_2
    //   112: invokestatic 57	com/d/a/a/a/d:a	(Lcom/d/a/a/a/d;Lcom/d/a/a/a/a;Lcom/d/a/a/a/a;)V
    //   115: aload_0
    //   116: getfield 29	com/d/a/a/a/d$c:b	Lcom/d/a/a/a/b;
    //   119: invokestatic 62	com/d/a/a/j:a	(Ljava/io/Closeable;)V
    //   122: aload_3
    //   123: athrow
    //   124: pop
    //   125: goto -52 -> 73
    //   128: pop
    //   129: goto -14 -> 115
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	132	0	this	c
    //   3	108	1	locala1	a
    //   7	105	2	locala2	a
    //   12	56	3	localObject1	Object
    //   81	1	3	localObject2	Object
    //   98	25	3	locald1	d
    //   17	33	4	bool	boolean
    //   106	3	5	locald2	d
    //   85	1	8	localIOException1	java.io.IOException
    //   124	1	9	localIOException2	java.io.IOException
    //   128	1	10	localIOException3	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   8	12	81	finally
    //   13	17	81	finally
    //   24	28	81	finally
    //   29	35	81	finally
    //   35	39	81	finally
    //   41	47	81	finally
    //   54	57	81	finally
    //   58	61	81	finally
    //   86	89	81	finally
    //   90	93	81	finally
    //   8	12	85	java/io/IOException
    //   13	17	85	java/io/IOException
    //   24	28	85	java/io/IOException
    //   29	35	85	java/io/IOException
    //   35	39	85	java/io/IOException
    //   41	47	85	java/io/IOException
    //   54	57	85	java/io/IOException
    //   58	61	85	java/io/IOException
    //   62	66	124	java/io/IOException
    //   69	73	124	java/io/IOException
    //   94	98	124	java/io/IOException
    //   102	106	128	java/io/IOException
    //   111	115	128	java/io/IOException
  }
  
  public final void a(int paramInt)
  {
    synchronized (c)
    {
      Object localObject1 = c;
      localObject1 = d.e((d)localObject1);
      localObject1 = ((Map)localObject1).values();
      Object localObject2 = c;
      localObject2 = d.e((d)localObject2);
      int i = ((Map)localObject2).size();
      localObject2 = new e[i];
      localObject1 = ((Collection)localObject1).toArray((Object[])localObject2);
      localObject1 = (e[])localObject1;
      localObject2 = c;
      d.i((d)localObject2);
      int j = localObject1.length;
      i = 0;
      localObject2 = null;
      while (i < j)
      {
        Object localObject3 = localObject1[i];
        int k = c;
        if (k > paramInt)
        {
          boolean bool = ((e)localObject3).b();
          if (bool)
          {
            Object localObject4 = a.k;
            ((e)localObject3).c((a)localObject4);
            localObject4 = c;
            int m = c;
            ((d)localObject4).b(m);
          }
        }
        i += 1;
      }
      return;
    }
  }
  
  public final void a(int paramInt, long paramLong)
  {
    if (paramInt == 0) {
      synchronized (c)
      {
        d locald2 = c;
        long l = d + paramLong;
        d = l;
        locald2 = c;
        locald2.notifyAll();
        return;
      }
    }
    ??? = c;
    e locale = ???.a(paramInt);
    if (locale != null) {
      try
      {
        locale.a(paramLong);
        return;
      }
      finally {}
    }
  }
  
  public final void a(int paramInt, a parama)
  {
    d locald = c;
    boolean bool = d.a(locald, paramInt);
    if (bool)
    {
      d.a(c, paramInt, parama);
      return;
    }
    locald = c;
    e locale = locald.b(paramInt);
    if (locale != null) {
      locale.c(parama);
    }
  }
  
  public final void a(int paramInt, List paramList)
  {
    d.a(c, paramInt, paramList);
  }
  
  public final void a(boolean paramBoolean, int paramInt1, int paramInt2)
  {
    if (paramBoolean)
    {
      Object localObject = d.c(c, paramInt1);
      if (localObject != null)
      {
        long l1 = c;
        long l2 = -1;
        boolean bool = l1 < l2;
        if (!bool)
        {
          l1 = b;
          bool = l1 < l2;
          if (bool)
          {
            l1 = System.nanoTime();
            c = l1;
            localObject = a;
            ((CountDownLatch)localObject).countDown();
            break label101;
          }
        }
        localObject = new java/lang/IllegalStateException;
        ((IllegalStateException)localObject).<init>();
        throw ((Throwable)localObject);
      }
      label101:
      return;
    }
    d.a(c, paramInt1, paramInt2);
  }
  
  public final void a(boolean paramBoolean, int paramInt1, d.e parame, int paramInt2)
  {
    Object localObject1 = c;
    boolean bool = d.a((d)localObject1, paramInt1);
    if (bool)
    {
      d.a(c, paramInt1, parame, paramInt2, paramBoolean);
      return;
    }
    localObject1 = c.a(paramInt1);
    Object localObject2;
    if (localObject1 == null)
    {
      localObject2 = c;
      localObject1 = a.c;
      ((d)localObject2).a(paramInt1, (a)localObject1);
      long l1 = paramInt2;
      parame.h(l1);
      return;
    }
    paramInt1 = e.j;
    if (paramInt1 == 0)
    {
      paramInt1 = Thread.holdsLock(localObject1);
      if (paramInt1 != 0)
      {
        localObject2 = new java/lang/AssertionError;
        ((AssertionError)localObject2).<init>();
        throw ((Throwable)localObject2);
      }
    }
    e.b localb = f;
    long l2 = paramInt2;
    localb.a(parame, l2);
    if (paramBoolean) {
      ((e)localObject1).e();
    }
  }
  
  public final void a(boolean paramBoolean, n paramn)
  {
    synchronized (c)
    {
      ??? = c;
      ??? = f;
      boolean bool1 = ((n)???).b();
      int i = 0;
      if (paramBoolean)
      {
        localObject2 = c;
        localObject2 = f;
        c = 0;
        b = 0;
        a = 0;
        localObject2 = d;
        Arrays.fill((int[])localObject2, 0);
      }
      Object localObject2 = c;
      localObject2 = f;
      int j = 0;
      Object localObject5 = null;
      Object localObject6;
      for (;;)
      {
        int k = 10;
        if (j >= k) {
          break;
        }
        boolean bool2 = paramn.a(j);
        if (bool2)
        {
          m = paramn.b(j);
          localObject6 = d;
          int n = localObject6[j];
          ((n)localObject2).a(j, m, n);
        }
        j += 1;
      }
      localObject2 = c;
      localObject2 = a;
      localObject5 = u.d;
      int m = 1;
      if (localObject2 == localObject5)
      {
        localObject2 = d.c();
        localObject5 = new com/d/a/a/a/d$c$3;
        localObject6 = "OkHttp %s ACK Settings";
        Object[] arrayOfObject1 = new Object[m];
        localObject7 = c;
        localObject7 = d.a((d)localObject7);
        arrayOfObject1[0] = localObject7;
        ((d.c.3)localObject5).<init>(this, (String)localObject6, arrayOfObject1, paramn);
        ((ExecutorService)localObject2).execute((Runnable)localObject5);
      }
      localObject2 = c;
      localObject2 = f;
      paramBoolean = ((n)localObject2).b();
      boolean bool3 = true;
      long l1 = 0L;
      j = 0;
      localObject5 = null;
      long l2;
      if ((paramBoolean != bool3) && (paramBoolean != bool1))
      {
        paramBoolean -= bool1;
        l2 = paramBoolean;
        ??? = c;
        bool1 = d.g((d)???);
        if (!bool1)
        {
          ??? = c;
          long l3 = d + l2;
          d = l3;
          boolean bool4 = l2 < l1;
          if (bool4) {
            ???.notifyAll();
          }
          ??? = c;
          d.h((d)???);
        }
        ??? = c;
        ??? = d.e((d)???);
        bool1 = ((Map)???).isEmpty();
        if (!bool1)
        {
          ??? = c;
          ??? = d.e((d)???);
          ??? = ((Map)???).values();
          localObject5 = c;
          localObject5 = d.e((d)localObject5);
          j = ((Map)localObject5).size();
          localObject5 = new e[j];
          ??? = ((Collection)???).toArray((Object[])localObject5);
          localObject5 = ???;
          localObject5 = (e[])???;
        }
      }
      else
      {
        l2 = l1;
      }
      ??? = d.c();
      Object localObject7 = new com/d/a/a/a/d$c$2;
      String str = "OkHttp %s settings";
      Object[] arrayOfObject2 = new Object[m];
      Object localObject8 = c;
      localObject8 = d.a((d)localObject8);
      arrayOfObject2[0] = localObject8;
      ((d.c.2)localObject7).<init>(this, str, arrayOfObject2);
      ((ExecutorService)???).execute((Runnable)localObject7);
      if (localObject5 != null)
      {
        boolean bool5 = l2 < l1;
        if (bool5)
        {
          int i1 = localObject5.length;
          while (i < i1) {
            synchronized (localObject5[i])
            {
              ((e)???).a(l2);
              i += 1;
            }
          }
        }
      }
      return;
    }
  }
  
  public final void a(boolean paramBoolean1, boolean paramBoolean2, int paramInt, List paramList, g paramg)
  {
    c localc = this;
    boolean bool1 = paramBoolean2;
    int i = paramInt;
    List localList = paramList;
    Object localObject1 = paramg;
    Object localObject2 = c;
    boolean bool2 = d.a((d)localObject2, paramInt);
    if (bool2)
    {
      d.a(c, paramInt, paramList, paramBoolean2);
      return;
    }
    synchronized (c)
    {
      localObject2 = c;
      bool2 = d.b((d)localObject2);
      if (bool2) {
        return;
      }
      localObject2 = c;
      localObject2 = ((d)localObject2).a(paramInt);
      int k = 0;
      boolean bool3 = true;
      label143:
      Object localObject3;
      Object localObject7;
      if (localObject2 == null)
      {
        localObject2 = g.b;
        if (paramg != localObject2)
        {
          localObject2 = g.c;
          if (paramg != localObject2)
          {
            m = 0;
            localObject1 = null;
            break label143;
          }
        }
        int m = 1;
        if (m != 0)
        {
          localObject3 = c;
          localObject1 = a.c;
          ((d)localObject3).a(i, (a)localObject1);
          return;
        }
        localObject1 = c;
        m = d.c((d)localObject1);
        if (i <= m) {
          return;
        }
        m = i % 2;
        localObject2 = c;
        int j = d.d((d)localObject2);
        int n = 2;
        j %= n;
        if (m == j) {
          return;
        }
        e locale = new com/d/a/a/a/e;
        localObject6 = c;
        localObject1 = locale;
        j = paramInt;
        localList = paramList;
        locale.<init>(paramInt, (d)localObject6, paramBoolean1, paramBoolean2, paramList);
        localObject3 = c;
        d.b((d)localObject3, i);
        localObject3 = c;
        localObject3 = d.e((d)localObject3);
        localObject1 = Integer.valueOf(paramInt);
        ((Map)localObject3).put(localObject1, locale);
        localObject3 = d.c();
        localObject1 = new com/d/a/a/a/d$c$1;
        localObject2 = "OkHttp %s stream %d";
        localObject6 = new Object[n];
        localObject7 = c;
        localObject7 = d.a((d)localObject7);
        localObject6[0] = localObject7;
        localObject7 = Integer.valueOf(paramInt);
        localObject6[bool3] = localObject7;
        ((d.c.1)localObject1).<init>(this, (String)localObject2, (Object[])localObject6, locale);
        ((ExecutorService)localObject3).execute((Runnable)localObject1);
        return;
      }
      Object localObject6 = g.a;
      if (paramg == localObject6)
      {
        bool4 = true;
      }
      else
      {
        bool4 = false;
        localObject6 = null;
      }
      if (bool4)
      {
        localObject3 = a.b;
        ((e)localObject2).b((a)localObject3);
        c.b(i);
        return;
      }
      boolean bool4 = e.j;
      if (!bool4)
      {
        bool4 = Thread.holdsLock(localObject2);
        if (bool4)
        {
          localObject3 = new java/lang/AssertionError;
          ((AssertionError)localObject3).<init>();
          throw ((Throwable)localObject3);
        }
      }
      bool4 = false;
      localObject6 = null;
      try
      {
        localObject7 = e;
        if (localObject7 == null)
        {
          localObject7 = g.c;
          if (localObject1 == localObject7) {
            k = 1;
          }
          if (k != 0)
          {
            localObject6 = a.b;
          }
          else
          {
            e = localList;
            bool3 = ((e)localObject2).a();
            localObject2.notifyAll();
          }
        }
        else
        {
          localObject7 = g.b;
          if (localObject1 == localObject7) {
            k = 1;
          }
          if (k != 0)
          {
            localObject6 = a.e;
          }
          else
          {
            localObject1 = new java/util/ArrayList;
            ((ArrayList)localObject1).<init>();
            localObject7 = e;
            ((List)localObject1).addAll((Collection)localObject7);
            ((List)localObject1).addAll(localList);
            e = ((List)localObject1);
          }
        }
        if (localObject6 != null)
        {
          ((e)localObject2).b((a)localObject6);
        }
        else if (!bool3)
        {
          localObject1 = d;
          int i1 = c;
          ((d)localObject1).b(i1);
        }
        if (bool1) {
          ((e)localObject2).e();
        }
        return;
      }
      finally {}
    }
  }
}

/* Location:
 * Qualified Name:     com.d.a.a.a.d.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */