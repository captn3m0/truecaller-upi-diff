package com.d.a;

import com.d.a.a.b.g;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public final class p
{
  public final String[] a;
  
  private p(p.a parama)
  {
    List localList = a;
    parama = new String[a.size()];
    parama = (String[])localList.toArray(parama);
    a = parama;
  }
  
  private static String a(String[] paramArrayOfString, String paramString)
  {
    int i = paramArrayOfString.length + -2;
    while (i >= 0)
    {
      String str = paramArrayOfString[i];
      boolean bool = paramString.equalsIgnoreCase(str);
      if (bool)
      {
        i += 1;
        return paramArrayOfString[i];
      }
      i += -2;
    }
    return null;
  }
  
  public final p.a a()
  {
    p.a locala = new com/d/a/p$a;
    locala.<init>();
    List localList = a;
    String[] arrayOfString = a;
    Collections.addAll(localList, arrayOfString);
    return locala;
  }
  
  public final String a(int paramInt)
  {
    paramInt *= 2;
    if (paramInt >= 0)
    {
      String[] arrayOfString = a;
      int i = arrayOfString.length;
      if (paramInt < i) {
        return arrayOfString[paramInt];
      }
    }
    return null;
  }
  
  public final String a(String paramString)
  {
    return a(a, paramString);
  }
  
  public final String b(int paramInt)
  {
    paramInt = paramInt * 2 + 1;
    if (paramInt >= 0)
    {
      String[] arrayOfString = a;
      int i = arrayOfString.length;
      if (paramInt < i) {
        return arrayOfString[paramInt];
      }
    }
    return null;
  }
  
  public final Date b(String paramString)
  {
    paramString = a(paramString);
    if (paramString != null) {
      return g.a(paramString);
    }
    return null;
  }
  
  public final List c(String paramString)
  {
    String[] arrayOfString = a;
    int i = arrayOfString.length;
    int j = 2;
    i /= j;
    ArrayList localArrayList = null;
    int k = 0;
    while (k < i)
    {
      String str = a(k);
      boolean bool = paramString.equalsIgnoreCase(str);
      if (bool)
      {
        if (localArrayList == null)
        {
          localArrayList = new java/util/ArrayList;
          localArrayList.<init>(j);
        }
        str = b(k);
        localArrayList.add(str);
      }
      k += 1;
    }
    if (localArrayList != null) {
      return Collections.unmodifiableList(localArrayList);
    }
    return Collections.emptyList();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String[] arrayOfString = a;
    int i = arrayOfString.length / 2;
    int j = 0;
    while (j < i)
    {
      String str = a(j);
      localStringBuilder.append(str);
      localStringBuilder.append(": ");
      str = b(j);
      localStringBuilder.append(str);
      str = "\n";
      localStringBuilder.append(str);
      j += 1;
    }
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.d.a.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */