package com.d.a;

import com.d.a.a.b.a;
import com.d.a.a.b.c;
import com.d.a.a.b.i;
import com.d.a.a.b.k;
import com.d.a.a.c.a;
import com.d.a.a.j;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;

public final class c
{
  final com.d.a.a.e a;
  int b;
  int c;
  private final com.d.a.a.b d;
  private int e;
  private int f;
  private int g;
  
  public c(File paramFile, long paramLong)
  {
    this(paramFile, paramLong, locala);
  }
  
  private c(File paramFile, long paramLong, a parama)
  {
    c.1 local1 = new com/d/a/c$1;
    local1.<init>(this);
    a = local1;
    paramFile = com.d.a.a.b.a(parama, paramFile, paramLong);
    d = paramFile;
  }
  
  static int a(d.e parame)
  {
    try
    {
      long l1 = parame.m();
      parame = parame.q();
      long l2 = 0L;
      boolean bool1 = l1 < l2;
      if (!bool1)
      {
        l2 = 2147483647L;
        bool1 = l1 < l2;
        if (!bool1)
        {
          boolean bool2 = parame.isEmpty();
          if (bool2) {
            return (int)l1;
          }
        }
      }
      IOException localIOException1 = new java/io/IOException;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      String str = "expected an int but was \"";
      localStringBuilder.<init>(str);
      localStringBuilder.append(l1);
      localStringBuilder.append(parame);
      parame = "\"";
      localStringBuilder.append(parame);
      parame = localStringBuilder.toString();
      localIOException1.<init>(parame);
      throw localIOException1;
    }
    catch (NumberFormatException parame)
    {
      IOException localIOException2 = new java/io/IOException;
      parame = parame.getMessage();
      localIOException2.<init>(parame);
      throw localIOException2;
    }
  }
  
  static void a(b.a parama)
  {
    if (parama != null) {
      try
      {
        parama.b();
      }
      catch (IOException localIOException) {}
    }
  }
  
  private static String c(v paramv)
  {
    return j.a(a.toString());
  }
  
  final com.d.a.a.b.b a(x paramx)
  {
    Object localObject1 = a.b;
    Object localObject2 = a.b;
    boolean bool1 = i.a((String)localObject2);
    if (bool1) {}
    try
    {
      paramx = a;
      b(paramx);
    }
    catch (IOException localIOException2)
    {
      boolean bool2;
      for (;;) {}
    }
    return null;
    localObject2 = "GET";
    bool2 = ((String)localObject1).equals(localObject2);
    if (!bool2) {
      return null;
    }
    bool2 = k.b(paramx);
    if (bool2) {
      return null;
    }
    localObject1 = new com/d/a/c$c;
    ((c.c)localObject1).<init>(paramx);
    try
    {
      localObject2 = d;
      paramx = a;
      paramx = c(paramx);
      long l = -1;
      paramx = ((com.d.a.a.b)localObject2).a(paramx, l);
      if (paramx == null) {
        return null;
      }
      a(paramx);
    }
    catch (IOException localIOException1)
    {
      try
      {
        ((c.c)localObject1).a(paramx);
        localObject1 = new com/d/a/c$a;
        ((c.a)localObject1).<init>(this, paramx);
        return (com.d.a.a.b.b)localObject1;
      }
      catch (IOException localIOException3)
      {
        for (;;) {}
      }
      localIOException1;
      paramx = null;
    }
    return null;
  }
  
  final x a(v paramv)
  {
    Object localObject1 = c(paramv);
    try
    {
      Object localObject2 = d;
      localObject1 = ((com.d.a.a.b)localObject2).a((String)localObject1);
      if (localObject1 == null) {
        return null;
      }
      try
      {
        localObject2 = new com/d/a/c$c;
        Object localObject3 = c;
        int i = 0;
        localObject3 = localObject3[0];
        ((c.c)localObject2).<init>((d.u)localObject3);
        localObject3 = g.a("Content-Type");
        String str = g.a("Content-Length");
        Object localObject4 = new com/d/a/v$a;
        ((v.a)localObject4).<init>();
        Object localObject5 = a;
        localObject4 = ((v.a)localObject4).a((String)localObject5);
        localObject5 = c;
        localObject4 = ((v.a)localObject4).c((String)localObject5);
        localObject5 = b.a();
        c = ((p.a)localObject5);
        localObject4 = ((v.a)localObject4).a();
        localObject5 = new com/d/a/x$a;
        ((x.a)localObject5).<init>();
        a = ((v)localObject4);
        localObject4 = d;
        b = ((u)localObject4);
        int j = e;
        c = j;
        localObject4 = f;
        d = ((String)localObject4);
        localObject4 = g;
        localObject4 = ((x.a)localObject5).a((p)localObject4);
        localObject5 = new com/d/a/c$b;
        ((c.b)localObject5).<init>((b.c)localObject1, (String)localObject3, str);
        g = ((y)localObject5);
        localObject1 = h;
        e = ((o)localObject1);
        localObject1 = ((x.a)localObject4).a();
        localObject3 = a;
        str = a.toString();
        boolean bool1 = ((String)localObject3).equals(str);
        if (bool1)
        {
          localObject3 = c;
          str = b;
          bool1 = ((String)localObject3).equals(str);
          if (bool1)
          {
            localObject2 = b;
            boolean bool2 = k.a((x)localObject1, (p)localObject2, paramv);
            if (bool2) {
              i = 1;
            }
          }
        }
        if (i == 0)
        {
          j.a(g);
          return null;
        }
        return (x)localObject1;
      }
      catch (IOException localIOException1)
      {
        j.a((Closeable)localObject1);
        return null;
      }
      return null;
    }
    catch (IOException localIOException2) {}
  }
  
  public final void a()
  {
    d.close();
  }
  
  final void a(com.d.a.a.b.c paramc)
  {
    try
    {
      int i = g + 1;
      g = i;
      v localv = a;
      int j;
      if (localv != null)
      {
        j = e + 1;
        e = j;
        return;
      }
      paramc = b;
      if (paramc != null)
      {
        j = f + 1;
        f = j;
      }
      return;
    }
    finally {}
  }
  
  final void b()
  {
    try
    {
      int i = f + 1;
      f = i;
      return;
    }
    finally {}
  }
  
  final void b(v paramv)
  {
    com.d.a.a.b localb = d;
    paramv = c(paramv);
    localb.b(paramv);
  }
}

/* Location:
 * Qualified Name:     com.d.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */