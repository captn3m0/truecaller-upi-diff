package com.d.a;

import com.d.a.a.d;
import com.d.a.a.i;
import java.net.CookieHandler;
import java.net.Proxy;
import java.net.ProxySelector;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

public class t
  implements Cloneable
{
  static final List a;
  static final List b;
  private static SSLSocketFactory y;
  private c A;
  m c;
  public Proxy d;
  public List e;
  public List f;
  final List g;
  public final List h;
  public ProxySelector i;
  public CookieHandler j;
  com.d.a.a.e k;
  public SocketFactory l;
  public SSLSocketFactory m;
  public HostnameVerifier n;
  public f o;
  public b p;
  public j q;
  public n r;
  public boolean s;
  public boolean t;
  public boolean u;
  public int v;
  public int w;
  public int x;
  private final i z;
  
  static
  {
    int i1 = 3;
    Object localObject1 = new u[i1];
    u localu = u.d;
    localObject1[0] = localu;
    localu = u.c;
    int i2 = 1;
    localObject1[i2] = localu;
    localu = u.b;
    int i3 = 2;
    localObject1[i3] = localu;
    a = com.d.a.a.j.a((Object[])localObject1);
    Object localObject2 = new k[i1];
    localObject1 = k.a;
    localObject2[0] = localObject1;
    localObject1 = k.b;
    localObject2[i2] = localObject1;
    localObject1 = k.c;
    localObject2[i3] = localObject1;
    b = com.d.a.a.j.a((Object[])localObject2);
    localObject2 = new com/d/a/t$1;
    ((t.1)localObject2).<init>();
    d.b = (d)localObject2;
  }
  
  public t()
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    g = ((List)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    h = ((List)localObject);
    int i1 = 1;
    s = i1;
    t = i1;
    u = i1;
    i1 = 10000;
    v = i1;
    w = i1;
    x = i1;
    localObject = new com/d/a/a/i;
    ((i)localObject).<init>();
    z = ((i)localObject);
    localObject = new com/d/a/m;
    ((m)localObject).<init>();
    c = ((m)localObject);
  }
  
  t(t paramt)
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    g = ((List)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    h = ((List)localObject);
    int i1 = 1;
    s = i1;
    t = i1;
    u = i1;
    i1 = 10000;
    v = i1;
    w = i1;
    x = i1;
    localObject = z;
    z = ((i)localObject);
    localObject = c;
    c = ((m)localObject);
    localObject = d;
    d = ((Proxy)localObject);
    localObject = e;
    e = ((List)localObject);
    localObject = f;
    f = ((List)localObject);
    localObject = g;
    List localList = g;
    ((List)localObject).addAll(localList);
    localObject = h;
    localList = h;
    ((List)localObject).addAll(localList);
    localObject = i;
    i = ((ProxySelector)localObject);
    localObject = j;
    j = ((CookieHandler)localObject);
    localObject = A;
    A = ((c)localObject);
    localObject = A;
    if (localObject != null) {
      localObject = a;
    } else {
      localObject = k;
    }
    k = ((com.d.a.a.e)localObject);
    localObject = l;
    l = ((SocketFactory)localObject);
    localObject = m;
    m = ((SSLSocketFactory)localObject);
    localObject = n;
    n = ((HostnameVerifier)localObject);
    localObject = o;
    o = ((f)localObject);
    localObject = p;
    p = ((b)localObject);
    localObject = q;
    q = ((j)localObject);
    localObject = r;
    r = ((n)localObject);
    boolean bool = s;
    s = bool;
    bool = t;
    t = bool;
    bool = u;
    u = bool;
    int i2 = v;
    v = i2;
    i2 = w;
    w = i2;
    int i3 = x;
    x = i3;
  }
  
  public final c a()
  {
    return A;
  }
  
  public final e a(v paramv)
  {
    e locale = new com/d/a/e;
    locale.<init>(this, paramv);
    return locale;
  }
  
  public final t a(c paramc)
  {
    A = paramc;
    k = null;
    return this;
  }
  
  public final void a(TimeUnit paramTimeUnit)
  {
    if (paramTimeUnit != null)
    {
      long l1 = paramTimeUnit.toMillis(15000L);
      long l2 = 2147483647L;
      boolean bool = l1 < l2;
      if (!bool)
      {
        l2 = 0L;
        bool = l1 < l2;
        if (!bool)
        {
          paramTimeUnit = new java/lang/IllegalArgumentException;
          paramTimeUnit.<init>("Timeout too small.");
          throw paramTimeUnit;
        }
        int i1 = (int)l1;
        v = i1;
        return;
      }
      paramTimeUnit = new java/lang/IllegalArgumentException;
      paramTimeUnit.<init>("Timeout too large.");
      throw paramTimeUnit;
    }
    paramTimeUnit = new java/lang/IllegalArgumentException;
    paramTimeUnit.<init>("unit == null");
    throw paramTimeUnit;
  }
  
  final SSLSocketFactory b()
  {
    try
    {
      Object localObject1 = y;
      if (localObject1 == null)
      {
        localObject1 = "TLS";
        try
        {
          localObject1 = SSLContext.getInstance((String)localObject1);
          ((SSLContext)localObject1).init(null, null, null);
          localObject1 = ((SSLContext)localObject1).getSocketFactory();
          y = (SSLSocketFactory)localObject1;
        }
        catch (GeneralSecurityException localGeneralSecurityException)
        {
          localObject1 = new java/lang/AssertionError;
          ((AssertionError)localObject1).<init>();
          throw ((Throwable)localObject1);
        }
      }
      localObject1 = y;
      return (SSLSocketFactory)localObject1;
    }
    finally {}
  }
  
  public final void b(TimeUnit paramTimeUnit)
  {
    if (paramTimeUnit != null)
    {
      long l1 = paramTimeUnit.toMillis(20000L);
      long l2 = 2147483647L;
      boolean bool = l1 < l2;
      if (!bool)
      {
        l2 = 0L;
        bool = l1 < l2;
        if (!bool)
        {
          paramTimeUnit = new java/lang/IllegalArgumentException;
          paramTimeUnit.<init>("Timeout too small.");
          throw paramTimeUnit;
        }
        int i1 = (int)l1;
        w = i1;
        return;
      }
      paramTimeUnit = new java/lang/IllegalArgumentException;
      paramTimeUnit.<init>("Timeout too large.");
      throw paramTimeUnit;
    }
    paramTimeUnit = new java/lang/IllegalArgumentException;
    paramTimeUnit.<init>("unit == null");
    throw paramTimeUnit;
  }
  
  public final void c(TimeUnit paramTimeUnit)
  {
    if (paramTimeUnit != null)
    {
      long l1 = paramTimeUnit.toMillis(20000L);
      long l2 = 2147483647L;
      boolean bool = l1 < l2;
      if (!bool)
      {
        l2 = 0L;
        bool = l1 < l2;
        if (!bool)
        {
          paramTimeUnit = new java/lang/IllegalArgumentException;
          paramTimeUnit.<init>("Timeout too small.");
          throw paramTimeUnit;
        }
        int i1 = (int)l1;
        x = i1;
        return;
      }
      paramTimeUnit = new java/lang/IllegalArgumentException;
      paramTimeUnit.<init>("Timeout too large.");
      throw paramTimeUnit;
    }
    paramTimeUnit = new java/lang/IllegalArgumentException;
    paramTimeUnit.<init>("unit == null");
    throw paramTimeUnit;
  }
}

/* Location:
 * Qualified Name:     com.d.a.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */