package com.d.a;

import com.d.a.a.b.i;

public final class v$a
{
  q a;
  String b;
  p.a c;
  w d;
  Object e;
  
  public v$a()
  {
    b = "GET";
    p.a locala = new com/d/a/p$a;
    locala.<init>();
    c = locala;
  }
  
  private v$a(v paramv)
  {
    Object localObject = a;
    a = ((q)localObject);
    localObject = b;
    b = ((String)localObject);
    localObject = d;
    d = ((w)localObject);
    localObject = e;
    e = localObject;
    paramv = c.a();
    c = paramv;
  }
  
  public final a a(d paramd)
  {
    paramd = paramd.toString();
    boolean bool = paramd.isEmpty();
    if (bool) {
      return b("Cache-Control");
    }
    return a("Cache-Control", paramd);
  }
  
  public final a a(q paramq)
  {
    if (paramq != null)
    {
      a = paramq;
      return this;
    }
    paramq = new java/lang/IllegalArgumentException;
    paramq.<init>("url == null");
    throw paramq;
  }
  
  public final a a(String paramString)
  {
    if (paramString != null)
    {
      boolean bool1 = true;
      String str1 = "ws:";
      int k = 3;
      Object localObject = paramString;
      boolean bool2 = paramString.regionMatches(bool1, 0, str1, 0, k);
      String str2;
      int i;
      if (bool2)
      {
        localObject = new java/lang/StringBuilder;
        str2 = "http:";
        ((StringBuilder)localObject).<init>(str2);
        i = 3;
        paramString = paramString.substring(i);
        ((StringBuilder)localObject).append(paramString);
        paramString = ((StringBuilder)localObject).toString();
      }
      else
      {
        i = 1;
        str1 = "wss:";
        k = 4;
        bool2 = paramString.regionMatches(i, 0, str1, 0, k);
        if (bool2)
        {
          localObject = new java/lang/StringBuilder;
          str2 = "https:";
          ((StringBuilder)localObject).<init>(str2);
          int j = 4;
          paramString = paramString.substring(j);
          ((StringBuilder)localObject).append(paramString);
          paramString = ((StringBuilder)localObject).toString();
        }
      }
      localObject = q.c(paramString);
      if (localObject != null) {
        return a((q)localObject);
      }
      localObject = new java/lang/IllegalArgumentException;
      paramString = String.valueOf(paramString);
      paramString = "unexpected url: ".concat(paramString);
      ((IllegalArgumentException)localObject).<init>(paramString);
      throw ((Throwable)localObject);
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("url == null");
    throw paramString;
  }
  
  public final a a(String paramString1, String paramString2)
  {
    c.b(paramString1, paramString2);
    return this;
  }
  
  public final v a()
  {
    Object localObject = a;
    if (localObject != null)
    {
      localObject = new com/d/a/v;
      ((v)localObject).<init>(this, (byte)0);
      return (v)localObject;
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("url == null");
    throw ((Throwable)localObject);
  }
  
  public final a b(String paramString)
  {
    c.b(paramString);
    return this;
  }
  
  public final a b(String paramString1, String paramString2)
  {
    c.a(paramString1, paramString2);
    return this;
  }
  
  public final a c(String paramString)
  {
    if (paramString != null)
    {
      int i = paramString.length();
      if (i != 0)
      {
        boolean bool = i.b(paramString);
        if (!bool)
        {
          b = paramString;
          d = null;
          return this;
        }
        IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>("method ");
        localStringBuilder.append(paramString);
        localStringBuilder.append(" must have a request body.");
        paramString = localStringBuilder.toString();
        localIllegalArgumentException.<init>(paramString);
        throw localIllegalArgumentException;
      }
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("method == null || method.length() == 0");
    throw paramString;
  }
}

/* Location:
 * Qualified Name:     com.d.a.v.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */