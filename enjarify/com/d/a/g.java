package com.d.a;

import com.d.a.a.j;

public final class g
{
  public final String a;
  public final String b;
  
  public g(String paramString1, String paramString2)
  {
    a = paramString1;
    b = paramString2;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof g;
    if (bool1)
    {
      String str1 = a;
      paramObject = (g)paramObject;
      String str2 = a;
      bool1 = j.a(str1, str2);
      if (bool1)
      {
        str1 = b;
        paramObject = b;
        boolean bool2 = j.a(str1, paramObject);
        if (bool2) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    String str1 = b;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    int j = (j + 899) * 31;
    String str2 = a;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str = a;
    localStringBuilder.append(str);
    localStringBuilder.append(" realm=\"");
    str = b;
    localStringBuilder.append(str);
    localStringBuilder.append("\"");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.d.a.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */