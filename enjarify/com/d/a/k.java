package com.d.a;

import com.d.a.a.j;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLSocket;

public final class k
{
  public static final k a;
  public static final k b;
  public static final k c;
  private static final h[] h;
  public final boolean d;
  public final boolean e;
  final String[] f;
  final String[] g;
  
  static
  {
    Object localObject1 = new h[13];
    Object localObject2 = h.aK;
    localObject1[0] = localObject2;
    localObject2 = h.aO;
    boolean bool = true;
    localObject1[bool] = localObject2;
    localObject2 = h.W;
    int i = 2;
    localObject1[i] = localObject2;
    localObject2 = h.am;
    int j = 3;
    localObject1[j] = localObject2;
    localObject2 = h.al;
    localObject1[4] = localObject2;
    localObject2 = h.av;
    localObject1[5] = localObject2;
    localObject2 = h.aw;
    localObject1[6] = localObject2;
    localObject2 = h.F;
    localObject1[7] = localObject2;
    localObject2 = h.J;
    localObject1[8] = localObject2;
    localObject2 = h.U;
    localObject1[9] = localObject2;
    localObject2 = h.D;
    localObject1[10] = localObject2;
    localObject2 = h.H;
    localObject1[11] = localObject2;
    localObject2 = h.h;
    localObject1[12] = localObject2;
    h = (h[])localObject1;
    localObject1 = new com/d/a/k$a;
    ((k.a)localObject1).<init>(bool);
    localObject2 = h;
    localObject1 = ((k.a)localObject1).a((h[])localObject2);
    localObject2 = new aa[j];
    aa localaa1 = aa.a;
    localObject2[0] = localaa1;
    localaa1 = aa.b;
    localObject2[bool] = localaa1;
    localaa1 = aa.c;
    localObject2[i] = localaa1;
    a = ((k.a)localObject1).a((aa[])localObject2).a().b();
    localObject1 = new com/d/a/k$a;
    localObject2 = a;
    ((k.a)localObject1).<init>((k)localObject2);
    localObject2 = new aa[bool];
    aa localaa2 = aa.c;
    localObject2[0] = localaa2;
    b = ((k.a)localObject1).a((aa[])localObject2).a().b();
    localObject1 = new com/d/a/k$a;
    ((k.a)localObject1).<init>(false);
    c = ((k.a)localObject1).b();
  }
  
  private k(k.a parama)
  {
    boolean bool1 = a;
    d = bool1;
    String[] arrayOfString = b;
    f = arrayOfString;
    arrayOfString = c;
    g = arrayOfString;
    boolean bool2 = d;
    e = bool2;
  }
  
  private static boolean a(String[] paramArrayOfString1, String[] paramArrayOfString2)
  {
    if ((paramArrayOfString1 != null) && (paramArrayOfString2 != null))
    {
      int i = paramArrayOfString1.length;
      if (i != 0)
      {
        i = paramArrayOfString2.length;
        if (i != 0)
        {
          i = paramArrayOfString1.length;
          int j = 0;
          while (j < i)
          {
            String str = paramArrayOfString1[j];
            boolean bool = j.a(paramArrayOfString2, str);
            if (bool) {
              return true;
            }
            j += 1;
          }
          return false;
        }
      }
    }
    return false;
  }
  
  public final List a()
  {
    Object localObject1 = f;
    if (localObject1 == null) {
      return null;
    }
    int i = localObject1.length;
    localObject1 = new h[i];
    int j = 0;
    for (;;)
    {
      Object localObject2 = f;
      int k = localObject2.length;
      if (j >= k) {
        break;
      }
      localObject2 = h.a(localObject2[j]);
      localObject1[j] = localObject2;
      j += 1;
    }
    return j.a((Object[])localObject1);
  }
  
  public final boolean a(SSLSocket paramSSLSocket)
  {
    boolean bool1 = d;
    if (!bool1) {
      return false;
    }
    String[] arrayOfString1 = g;
    if (arrayOfString1 != null)
    {
      String[] arrayOfString2 = paramSSLSocket.getEnabledProtocols();
      bool1 = a(arrayOfString1, arrayOfString2);
      if (!bool1) {
        return false;
      }
    }
    arrayOfString1 = f;
    if (arrayOfString1 != null)
    {
      paramSSLSocket = paramSSLSocket.getEnabledCipherSuites();
      boolean bool2 = a(arrayOfString1, paramSSLSocket);
      if (!bool2) {
        return false;
      }
    }
    return true;
  }
  
  public final List b()
  {
    Object localObject1 = g;
    if (localObject1 == null) {
      return null;
    }
    int i = localObject1.length;
    localObject1 = new aa[i];
    int j = 0;
    for (;;)
    {
      Object localObject2 = g;
      int k = localObject2.length;
      if (j >= k) {
        break;
      }
      localObject2 = aa.a(localObject2[j]);
      localObject1[j] = localObject2;
      j += 1;
    }
    return j.a((Object[])localObject1);
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof k;
    if (!bool1) {
      return false;
    }
    bool1 = true;
    if (paramObject == this) {
      return bool1;
    }
    paramObject = (k)paramObject;
    boolean bool2 = d;
    boolean bool3 = d;
    if (bool2 != bool3) {
      return false;
    }
    if (bool2)
    {
      String[] arrayOfString1 = f;
      String[] arrayOfString2 = f;
      bool2 = Arrays.equals(arrayOfString1, arrayOfString2);
      if (!bool2) {
        return false;
      }
      arrayOfString1 = g;
      arrayOfString2 = g;
      bool2 = Arrays.equals(arrayOfString1, arrayOfString2);
      if (!bool2) {
        return false;
      }
      bool2 = e;
      boolean bool4 = e;
      if (bool2 != bool4) {
        return false;
      }
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    boolean bool = d;
    int i;
    if (bool)
    {
      String[] arrayOfString1 = f;
      i = (Arrays.hashCode(arrayOfString1) + 527) * 31;
      String[] arrayOfString2 = g;
      int j = Arrays.hashCode(arrayOfString2);
      i = (i + j) * 31;
      int k = e ^ true;
      i += k;
    }
    else
    {
      i = 17;
    }
    return i;
  }
  
  public final String toString()
  {
    boolean bool = d;
    if (!bool) {
      return "ConnectionSpec()";
    }
    Object localObject1 = f;
    if (localObject1 != null) {
      localObject1 = a().toString();
    } else {
      localObject1 = "[all enabled]";
    }
    Object localObject2 = g;
    if (localObject2 != null) {
      localObject2 = b().toString();
    } else {
      localObject2 = "[all enabled]";
    }
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ConnectionSpec(cipherSuites=");
    localStringBuilder.append((String)localObject1);
    localStringBuilder.append(", tlsVersions=");
    localStringBuilder.append((String)localObject2);
    localStringBuilder.append(", supportsTlsExtensions=");
    bool = e;
    localStringBuilder.append(bool);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.d.a.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */