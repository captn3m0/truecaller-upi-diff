package com.d.a;

import com.d.a.a.b.a;
import com.d.a.a.b.k;
import com.d.a.a.b.r;
import com.d.a.a.j;
import d.d;
import d.e;
import d.f;
import d.n;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final class c$c
{
  final String a;
  final p b;
  final String c;
  final u d;
  final int e;
  final String f;
  final p g;
  final o h;
  
  public c$c(x paramx)
  {
    Object localObject = a.a.toString();
    a = ((String)localObject);
    localObject = k.c(paramx);
    b = ((p)localObject);
    localObject = a.b;
    c = ((String)localObject);
    localObject = b;
    d = ((u)localObject);
    int i = c;
    e = i;
    localObject = d;
    f = ((String)localObject);
    localObject = f;
    g = ((p)localObject);
    paramx = e;
    h = paramx;
  }
  
  public c$c(d.u paramu)
  {
    try
    {
      Object localObject1 = n.a(paramu);
      Object localObject3 = ((e)localObject1).q();
      a = ((String)localObject3);
      localObject3 = ((e)localObject1).q();
      c = ((String)localObject3);
      localObject3 = new com/d/a/p$a;
      ((p.a)localObject3).<init>();
      int i = c.a((e)localObject1);
      int j = 0;
      Object localObject4 = null;
      int k = 0;
      String str1 = null;
      while (k < i)
      {
        String str2 = ((e)localObject1).q();
        ((p.a)localObject3).a(str2);
        k += 1;
      }
      localObject3 = ((p.a)localObject3).a();
      b = ((p)localObject3);
      localObject3 = ((e)localObject1).q();
      localObject3 = r.a((String)localObject3);
      Object localObject5 = a;
      d = ((u)localObject5);
      i = b;
      e = i;
      localObject3 = c;
      f = ((String)localObject3);
      localObject3 = new com/d/a/p$a;
      ((p.a)localObject3).<init>();
      i = c.a((e)localObject1);
      while (j < i)
      {
        str1 = ((e)localObject1).q();
        ((p.a)localObject3).a(str1);
        j += 1;
      }
      localObject3 = ((p.a)localObject3).a();
      g = ((p)localObject3);
      boolean bool = a();
      if (bool)
      {
        localObject3 = ((e)localObject1).q();
        i = ((String)localObject3).length();
        if (i <= 0)
        {
          localObject3 = ((e)localObject1).q();
          localObject5 = a((e)localObject1);
          localObject1 = a((e)localObject1);
          if (localObject3 != null)
          {
            localObject4 = new com/d/a/o;
            localObject5 = j.a((List)localObject5);
            localObject1 = j.a((List)localObject1);
            ((o)localObject4).<init>((String)localObject3, (List)localObject5, (List)localObject1);
            h = ((o)localObject4);
          }
          else
          {
            localObject1 = new java/lang/IllegalArgumentException;
            localObject3 = "cipherSuite == null";
            ((IllegalArgumentException)localObject1).<init>((String)localObject3);
            throw ((Throwable)localObject1);
          }
        }
        else
        {
          localObject1 = new java/io/IOException;
          localObject5 = new java/lang/StringBuilder;
          localObject4 = "expected \"\" but was \"";
          ((StringBuilder)localObject5).<init>((String)localObject4);
          ((StringBuilder)localObject5).append((String)localObject3);
          localObject3 = "\"";
          ((StringBuilder)localObject5).append((String)localObject3);
          localObject3 = ((StringBuilder)localObject5).toString();
          ((IOException)localObject1).<init>((String)localObject3);
          throw ((Throwable)localObject1);
        }
      }
      else
      {
        localObject1 = null;
        h = null;
      }
      return;
    }
    finally
    {
      paramu.close();
    }
  }
  
  private static List a(e parame)
  {
    int i = c.a(parame);
    int j = -1;
    if (i == j) {
      return Collections.emptyList();
    }
    Object localObject1 = "X.509";
    try
    {
      localObject1 = CertificateFactory.getInstance((String)localObject1);
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>(i);
      int k = 0;
      while (k < i)
      {
        Object localObject2 = parame.q();
        d.c localc = new d/c;
        localc.<init>();
        localObject2 = f.b((String)localObject2);
        localc.a((f)localObject2);
        localObject2 = localc.f();
        localObject2 = ((CertificateFactory)localObject1).generateCertificate((InputStream)localObject2);
        localArrayList.add(localObject2);
        k += 1;
      }
      return localArrayList;
    }
    catch (CertificateException parame)
    {
      IOException localIOException = new java/io/IOException;
      parame = parame.getMessage();
      localIOException.<init>(parame);
      throw localIOException;
    }
  }
  
  private static void a(d paramd, List paramList)
  {
    try
    {
      int i = paramList.size();
      long l = i;
      paramd.l(l);
      i = 10;
      paramd.j(i);
      int j = 0;
      int k = paramList.size();
      while (j < k)
      {
        Object localObject = paramList.get(j);
        localObject = (Certificate)localObject;
        localObject = ((Certificate)localObject).getEncoded();
        localObject = f.a((byte[])localObject);
        localObject = ((f)localObject).b();
        paramd.b((String)localObject);
        paramd.j(i);
        j += 1;
      }
      return;
    }
    catch (CertificateEncodingException paramd)
    {
      paramList = new java/io/IOException;
      paramd = paramd.getMessage();
      paramList.<init>(paramd);
      throw paramList;
    }
  }
  
  private boolean a()
  {
    return a.startsWith("https://");
  }
  
  public final void a(b.a parama)
  {
    int i = 0;
    Object localObject1 = null;
    parama = n.a(parama.a(0));
    String str1 = a;
    parama.b(str1);
    int j = 10;
    parama.j(j);
    Object localObject2 = c;
    parama.b((String)localObject2);
    parama.j(j);
    long l = b.a.length / 2;
    parama.l(l);
    parama.j(j);
    localObject2 = b.a;
    int k = localObject2.length / 2;
    int m = 0;
    Object localObject3 = null;
    while (m < k)
    {
      String str2 = b.a(m);
      parama.b(str2);
      parama.b(": ");
      str2 = b.b(m);
      parama.b(str2);
      parama.j(j);
      m += 1;
    }
    localObject2 = new com/d/a/a/b/r;
    localObject3 = d;
    int n = e;
    String str3 = f;
    ((r)localObject2).<init>((u)localObject3, n, str3);
    localObject2 = ((r)localObject2).toString();
    parama.b((String)localObject2);
    parama.j(j);
    l = g.a.length / 2;
    parama.l(l);
    parama.j(j);
    localObject2 = g.a;
    k = localObject2.length / 2;
    while (i < k)
    {
      localObject3 = g.a(i);
      parama.b((String)localObject3);
      parama.b(": ");
      localObject3 = g.b(i);
      parama.b((String)localObject3);
      parama.j(j);
      i += 1;
    }
    boolean bool = a();
    if (bool)
    {
      parama.j(j);
      localObject1 = h.a;
      parama.b((String)localObject1);
      parama.j(j);
      localObject1 = h.b;
      a(parama, (List)localObject1);
      localObject1 = h.c;
      a(parama, (List)localObject1);
    }
    parama.close();
  }
}

/* Location:
 * Qualified Name:     com.d.a.c.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */