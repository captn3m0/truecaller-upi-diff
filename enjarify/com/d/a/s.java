package com.d.a;

import java.util.regex.Pattern;

public final class s
{
  private static final Pattern a = Pattern.compile("([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)/([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)");
  private static final Pattern b = Pattern.compile(";\\s*(?:([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)=(?:([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)|\"([^\"]*)\"))?");
  private final String c;
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof s;
    if (bool1)
    {
      paramObject = c;
      String str = c;
      boolean bool2 = ((String)paramObject).equals(str);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    return c.hashCode();
  }
  
  public final String toString()
  {
    return c;
  }
}

/* Location:
 * Qualified Name:     com.d.a.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */