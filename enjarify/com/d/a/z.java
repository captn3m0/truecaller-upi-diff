package com.d.a;

import java.net.InetSocketAddress;
import java.net.Proxy;

public final class z
{
  public final a a;
  public final Proxy b;
  public final InetSocketAddress c;
  
  public z(a parama, Proxy paramProxy, InetSocketAddress paramInetSocketAddress)
  {
    if (parama != null)
    {
      if (paramProxy != null)
      {
        if (paramInetSocketAddress != null)
        {
          a = parama;
          b = paramProxy;
          c = paramInetSocketAddress;
          return;
        }
        parama = new java/lang/NullPointerException;
        parama.<init>("inetSocketAddress == null");
        throw parama;
      }
      parama = new java/lang/NullPointerException;
      parama.<init>("proxy == null");
      throw parama;
    }
    parama = new java/lang/NullPointerException;
    parama.<init>("address == null");
    throw parama;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof z;
    if (bool1)
    {
      paramObject = (z)paramObject;
      Object localObject1 = a;
      Object localObject2 = a;
      bool1 = ((a)localObject1).equals(localObject2);
      if (bool1)
      {
        localObject1 = b;
        localObject2 = b;
        bool1 = ((Proxy)localObject1).equals(localObject2);
        if (bool1)
        {
          localObject1 = c;
          paramObject = c;
          boolean bool2 = ((InetSocketAddress)localObject1).equals(paramObject);
          if (bool2) {
            return true;
          }
        }
      }
      return false;
    }
    return false;
  }
  
  public final int hashCode()
  {
    int i = (a.hashCode() + 527) * 31;
    int j = b.hashCode();
    i = (i + j) * 31;
    j = c.hashCode();
    return i + j;
  }
}

/* Location:
 * Qualified Name:     com.d.a.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */