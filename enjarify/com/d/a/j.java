package com.d.a;

import com.d.a.a.c.b;
import com.d.a.a.d;
import com.d.a.a.i;
import java.lang.ref.Reference;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public final class j
{
  private static final j g;
  final Executor a;
  final int b;
  Runnable c;
  final Deque d;
  final i e;
  private final long h;
  
  static
  {
    boolean bool = j.class.desiredAssertionStatus() ^ true;
    f = bool;
    Object localObject = System.getProperty("http.keepAlive");
    String str1 = System.getProperty("http.keepAliveDuration");
    String str2 = System.getProperty("http.maxConnections");
    long l;
    if (str1 != null) {
      l = Long.parseLong(str1);
    } else {
      l = 300000L;
    }
    if (localObject != null)
    {
      bool = Boolean.parseBoolean((String)localObject);
      if (!bool)
      {
        localObject = new com/d/a/j;
        ((j)localObject).<init>(0, l);
        g = (j)localObject;
        return;
      }
    }
    if (str2 != null)
    {
      localObject = new com/d/a/j;
      int i = Integer.parseInt(str2);
      ((j)localObject).<init>(i, l);
      g = (j)localObject;
      return;
    }
    localObject = new com/d/a/j;
    ((j)localObject).<init>(5, l);
    g = (j)localObject;
  }
  
  private j(int paramInt, long paramLong)
  {
    this(paramInt, paramLong, localTimeUnit);
  }
  
  private j(int paramInt, long paramLong, TimeUnit paramTimeUnit)
  {
    ThreadPoolExecutor localThreadPoolExecutor = new java/util/concurrent/ThreadPoolExecutor;
    TimeUnit localTimeUnit = TimeUnit.SECONDS;
    LinkedBlockingQueue localLinkedBlockingQueue = new java/util/concurrent/LinkedBlockingQueue;
    localLinkedBlockingQueue.<init>();
    ThreadFactory localThreadFactory = com.d.a.a.j.b("OkHttp ConnectionPool");
    int i = 1;
    long l1 = 60;
    Object localObject = localThreadPoolExecutor;
    localThreadPoolExecutor.<init>(0, i, l1, localTimeUnit, localLinkedBlockingQueue, localThreadFactory);
    a = localThreadPoolExecutor;
    localObject = new com/d/a/j$1;
    ((j.1)localObject).<init>(this);
    c = ((Runnable)localObject);
    localObject = new java/util/ArrayDeque;
    ((ArrayDeque)localObject).<init>();
    d = ((Deque)localObject);
    localObject = new com/d/a/a/i;
    ((i)localObject).<init>();
    e = ((i)localObject);
    b = paramInt;
    long l2 = paramTimeUnit.toNanos(paramLong);
    h = l2;
    l2 = 0L;
    paramInt = paramLong < l2;
    if (paramInt > 0) {
      return;
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    String str = String.valueOf(paramLong);
    str = "keepAliveDuration <= 0: ".concat(str);
    localIllegalArgumentException.<init>(str);
    throw localIllegalArgumentException;
  }
  
  public static j a()
  {
    return g;
  }
  
  final long a(long paramLong)
  {
    try
    {
      Object localObject1 = d;
      localObject1 = ((Deque)localObject1).iterator();
      long l1 = Long.MIN_VALUE;
      Object localObject2 = null;
      int i = 0;
      int j = 0;
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        Object localObject3 = ((Iterator)localObject1).next();
        localObject3 = (b)localObject3;
        List localList = h;
        int k = 0;
        boolean bool2;
        do
        {
          for (;;)
          {
            m = localList.size();
            if (k >= m) {
              break label259;
            }
            localObject4 = localList.get(k);
            localObject4 = (Reference)localObject4;
            localObject4 = ((Reference)localObject4).get();
            if (localObject4 == null) {
              break;
            }
            k += 1;
          }
          Object localObject4 = d.a;
          Object localObject5 = new java/lang/StringBuilder;
          Object localObject6 = "A connection to ";
          ((StringBuilder)localObject5).<init>((String)localObject6);
          localObject6 = a;
          localObject6 = a;
          localObject6 = a;
          ((StringBuilder)localObject5).append(localObject6);
          localObject6 = " was leaked. Did you forget to close a response body?";
          ((StringBuilder)localObject5).append((String)localObject6);
          localObject5 = ((StringBuilder)localObject5).toString();
          ((Logger)localObject4).warning((String)localObject5);
          localList.remove(k);
          int m = 1;
          i = m;
          bool2 = localList.isEmpty();
        } while (!bool2);
        long l2 = h;
        l2 = paramLong - l2;
        j = l2;
        int n = 0;
        localList = null;
        break label268;
        label259:
        n = localList.size();
        label268:
        if (n > 0)
        {
          j += 1;
        }
        else
        {
          i += 1;
          l2 = j;
          l2 = paramLong - l2;
          bool2 = l2 < l1;
          if (bool2)
          {
            localObject2 = localObject3;
            l1 = l2;
          }
        }
      }
      paramLong = h;
      boolean bool3 = l1 < paramLong;
      if (bool3)
      {
        int i1 = b;
        if (i <= i1)
        {
          if (i > 0)
          {
            paramLong = h - l1;
            return paramLong;
          }
          if (j > 0)
          {
            paramLong = h;
            return paramLong;
          }
          paramLong = -1;
          return paramLong;
        }
      }
      Deque localDeque = d;
      localDeque.remove(localObject2);
      com.d.a.a.j.a(b);
      return 0L;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.d.a.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */