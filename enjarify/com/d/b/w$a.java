package com.d.b;

import android.content.Context;
import android.graphics.Bitmap.Config;
import android.os.Handler;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

public final class w$a
{
  public d a;
  public boolean b;
  public boolean c;
  private final Context d;
  private j e;
  private ExecutorService f;
  private w.c g;
  private w.f h;
  private List i;
  private Bitmap.Config j;
  
  public w$a(Context paramContext)
  {
    if (paramContext != null)
    {
      paramContext = paramContext.getApplicationContext();
      d = paramContext;
      return;
    }
    paramContext = new java/lang/IllegalArgumentException;
    paramContext.<init>("Context must not be null.");
    throw paramContext;
  }
  
  public final a a(ac paramac)
  {
    if (paramac != null)
    {
      Object localObject = i;
      if (localObject == null)
      {
        localObject = new java/util/ArrayList;
        ((ArrayList)localObject).<init>();
        i = ((List)localObject);
      }
      localObject = i;
      boolean bool = ((List)localObject).contains(paramac);
      if (!bool)
      {
        i.add(paramac);
        return this;
      }
      paramac = new java/lang/IllegalStateException;
      paramac.<init>("RequestHandler already registered.");
      throw paramac;
    }
    paramac = new java/lang/IllegalArgumentException;
    paramac.<init>("RequestHandler must not be null.");
    throw paramac;
  }
  
  public final a a(j paramj)
  {
    j localj = e;
    if (localj == null)
    {
      e = paramj;
      return this;
    }
    paramj = new java/lang/IllegalStateException;
    paramj.<init>("Downloader already set.");
    throw paramj;
  }
  
  public final w a()
  {
    Object localObject1 = d;
    Object localObject2 = e;
    if (localObject2 == null)
    {
      localObject2 = al.a((Context)localObject1);
      e = ((j)localObject2);
    }
    localObject2 = a;
    if (localObject2 == null)
    {
      localObject2 = new com/d/b/p;
      ((p)localObject2).<init>((Context)localObject1);
      a = ((d)localObject2);
    }
    localObject2 = f;
    if (localObject2 == null)
    {
      localObject2 = new com/d/b/y;
      ((y)localObject2).<init>();
      f = ((ExecutorService)localObject2);
    }
    localObject2 = h;
    if (localObject2 == null)
    {
      localObject2 = w.f.a;
      h = ((w.f)localObject2);
    }
    Object localObject3 = new com/d/b/ae;
    localObject2 = a;
    ((ae)localObject3).<init>((d)localObject2);
    i locali = new com/d/b/i;
    Object localObject4 = f;
    Object localObject5 = w.a;
    Object localObject6 = e;
    Object localObject7 = a;
    localObject2 = locali;
    Object localObject8 = localObject1;
    Object localObject9 = localObject3;
    locali.<init>((Context)localObject1, (ExecutorService)localObject4, (Handler)localObject5, (j)localObject6, (d)localObject7, (ae)localObject3);
    w localw = new com/d/b/w;
    localObject5 = a;
    localObject6 = g;
    localObject7 = h;
    localObject9 = i;
    Bitmap.Config localConfig = j;
    boolean bool1 = b;
    boolean bool2 = c;
    localObject2 = localw;
    localObject4 = locali;
    localObject1 = localObject3;
    localObject3 = localConfig;
    localw.<init>((Context)localObject8, locali, (d)localObject5, (w.c)localObject6, (w.f)localObject7, (List)localObject9, (ae)localObject1, localConfig, bool1, bool2);
    return localw;
  }
}

/* Location:
 * Qualified Name:     com.d.b.w.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */