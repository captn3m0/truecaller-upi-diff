package com.d.b;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import java.io.InputStream;

class g
  extends ac
{
  final Context a;
  
  g(Context paramContext)
  {
    a = paramContext;
  }
  
  public ac.a a(aa paramaa, int paramInt)
  {
    ac.a locala = new com/d/b/ac$a;
    paramaa = b(paramaa);
    w.d locald = w.d.b;
    locala.<init>(paramaa, locald);
    return locala;
  }
  
  public boolean a(aa paramaa)
  {
    paramaa = d.getScheme();
    return "content".equals(paramaa);
  }
  
  final InputStream b(aa paramaa)
  {
    ContentResolver localContentResolver = a.getContentResolver();
    paramaa = d;
    return localContentResolver.openInputStream(paramaa);
  }
}

/* Location:
 * Qualified Name:     com.d.b.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */