package com.d.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Handler;

final class i$c
  extends BroadcastReceiver
{
  final i a;
  
  i$c(i parami)
  {
    a = parami;
  }
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramIntent == null) {
      return;
    }
    Object localObject = paramIntent.getAction();
    String str = "android.intent.action.AIRPLANE_MODE";
    boolean bool1 = str.equals(localObject);
    if (bool1)
    {
      paramContext = "state";
      boolean bool2 = paramIntent.hasExtra(paramContext);
      if (!bool2) {
        return;
      }
      paramContext = a;
      int j = paramIntent.getBooleanExtra("state", false);
      localObject = i;
      paramContext = i.obtainMessage(10, j, 0);
      ((Handler)localObject).sendMessage(paramContext);
      return;
    }
    paramIntent = "android.net.conn.CONNECTIVITY_CHANGE";
    boolean bool3 = paramIntent.equals(localObject);
    if (bool3)
    {
      paramContext = (ConnectivityManager)al.a(paramContext, "connectivity");
      paramIntent = a;
      paramContext = paramContext.getActiveNetworkInfo();
      localObject = i;
      paramIntent = i;
      int i = 9;
      paramContext = paramIntent.obtainMessage(i, paramContext);
      ((Handler)localObject).sendMessage(paramContext);
    }
  }
}

/* Location:
 * Qualified Name:     com.d.b.i.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */