package com.d.b;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.ImageView;
import java.lang.ref.ReferenceQueue;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

public class w
{
  public static final Handler a;
  public static volatile w b = null;
  public final w.b c;
  public final List d;
  final Context e;
  public final i f;
  public final d g;
  public final ae h;
  final Map i;
  public final Map j;
  final ReferenceQueue k;
  final Bitmap.Config l;
  boolean m;
  public volatile boolean n;
  public boolean o;
  private final w.c p;
  private final w.f q;
  
  static
  {
    w.1 local1 = new com/d/b/w$1;
    Looper localLooper = Looper.getMainLooper();
    local1.<init>(localLooper);
    a = local1;
  }
  
  w(Context paramContext, i parami, d paramd, w.c paramc, w.f paramf, List paramList, ae paramae, Bitmap.Config paramConfig, boolean paramBoolean1, boolean paramBoolean2)
  {
    e = paramContext;
    f = parami;
    g = paramd;
    p = paramc;
    q = paramf;
    l = paramConfig;
    int i1;
    if (paramList != null)
    {
      i1 = paramList.size();
    }
    else
    {
      i1 = 0;
      paramd = null;
    }
    paramc = new java/util/ArrayList;
    i1 += 7;
    paramc.<init>(i1);
    paramd = new com/d/b/ad;
    paramd.<init>(paramContext);
    paramc.add(paramd);
    if (paramList != null) {
      paramc.addAll(paramList);
    }
    paramd = new com/d/b/f;
    paramd.<init>(paramContext);
    paramc.add(paramd);
    paramd = new com/d/b/r;
    paramd.<init>(paramContext);
    paramc.add(paramd);
    paramd = new com/d/b/g;
    paramd.<init>(paramContext);
    paramc.add(paramd);
    paramd = new com/d/b/b;
    paramd.<init>(paramContext);
    paramc.add(paramd);
    paramd = new com/d/b/l;
    paramd.<init>(paramContext);
    paramc.add(paramd);
    paramContext = new com/d/b/u;
    parami = d;
    paramContext.<init>(parami, paramae);
    paramc.add(paramContext);
    paramContext = Collections.unmodifiableList(paramc);
    d = paramContext;
    h = paramae;
    paramContext = new java/util/WeakHashMap;
    paramContext.<init>();
    i = paramContext;
    paramContext = new java/util/WeakHashMap;
    paramContext.<init>();
    j = paramContext;
    m = paramBoolean1;
    n = paramBoolean2;
    paramContext = new java/lang/ref/ReferenceQueue;
    paramContext.<init>();
    k = paramContext;
    paramContext = new com/d/b/w$b;
    parami = k;
    paramd = a;
    paramContext.<init>(parami, paramd);
    c = paramContext;
    c.start();
  }
  
  public static w a(Context paramContext)
  {
    ??? = b;
    if (??? == null) {
      synchronized (w.class)
      {
        Object localObject2 = b;
        if (localObject2 == null)
        {
          localObject2 = new com/d/b/w$a;
          ((w.a)localObject2).<init>(paramContext);
          paramContext = ((w.a)localObject2).a();
          b = paramContext;
        }
      }
    }
    return b;
  }
  
  public static void a(w paramw)
  {
    synchronized (w.class)
    {
      Object localObject = b;
      if (localObject == null)
      {
        b = paramw;
        return;
      }
      paramw = new java/lang/IllegalStateException;
      localObject = "Singleton instance already exists.";
      paramw.<init>((String)localObject);
      throw paramw;
    }
  }
  
  final aa a(aa paramaa)
  {
    Object localObject = q.a(paramaa);
    if (localObject != null) {
      return (aa)localObject;
    }
    localObject = new java/lang/IllegalStateException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Request transformer ");
    String str = q.getClass().getCanonicalName();
    localStringBuilder.append(str);
    localStringBuilder.append(" returned null for ");
    localStringBuilder.append(paramaa);
    paramaa = localStringBuilder.toString();
    ((IllegalStateException)localObject).<init>(paramaa);
    throw ((Throwable)localObject);
  }
  
  public final ab a(int paramInt)
  {
    if (paramInt != 0)
    {
      ab localab = new com/d/b/ab;
      localab.<init>(this, null, paramInt);
      return localab;
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    localIllegalArgumentException.<init>("Resource ID must not be zero.");
    throw localIllegalArgumentException;
  }
  
  public final ab a(Uri paramUri)
  {
    ab localab = new com/d/b/ab;
    localab.<init>(this, paramUri, 0);
    return localab;
  }
  
  public final ab a(String paramString)
  {
    if (paramString == null)
    {
      paramString = new com/d/b/ab;
      paramString.<init>(this, null, 0);
      return paramString;
    }
    String str = paramString.trim();
    int i1 = str.length();
    if (i1 != 0)
    {
      paramString = Uri.parse(paramString);
      return a(paramString);
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>("Path must not be empty.");
    throw paramString;
  }
  
  final void a(Bitmap paramBitmap, w.d paramd, a parama)
  {
    boolean bool1 = l;
    if (bool1) {
      return;
    }
    bool1 = k;
    if (!bool1)
    {
      Map localMap = i;
      Object localObject = parama.c();
      localMap.remove(localObject);
    }
    boolean bool2;
    if (paramBitmap != null)
    {
      if (paramd != null)
      {
        parama.a(paramBitmap, paramd);
        bool2 = n;
        if (bool2)
        {
          parama = b.a();
          paramd = String.valueOf(paramd);
          paramd = "from ".concat(paramd);
          al.a("Main", "completed", parama, paramd);
        }
      }
      else
      {
        paramBitmap = new java/lang/AssertionError;
        paramBitmap.<init>("LoadedFrom cannot be null.");
        throw paramBitmap;
      }
    }
    else
    {
      parama.a();
      bool2 = n;
      if (bool2)
      {
        paramBitmap = "Main";
        paramd = "errored";
        parama = b.a();
        al.a(paramBitmap, paramd, parama);
      }
    }
  }
  
  final void a(ImageView paramImageView, h paramh)
  {
    j.put(paramImageView, paramh);
  }
  
  final void a(a parama)
  {
    Object localObject1 = parama.c();
    if (localObject1 != null)
    {
      Object localObject2 = i.get(localObject1);
      if (localObject2 != parama)
      {
        d(localObject1);
        localObject2 = i;
        ((Map)localObject2).put(localObject1, parama);
      }
    }
    b(parama);
  }
  
  public final void a(Object paramObject)
  {
    al.b();
    ArrayList localArrayList = new java/util/ArrayList;
    Collection localCollection = i.values();
    localArrayList.<init>(localCollection);
    int i1 = localArrayList.size();
    int i2 = 0;
    while (i2 < i1)
    {
      Object localObject1 = (a)localArrayList.get(i2);
      Object localObject2 = j;
      boolean bool = localObject2.equals(paramObject);
      if (bool)
      {
        localObject1 = ((a)localObject1).c();
        d(localObject1);
      }
      i2 += 1;
    }
  }
  
  public final Bitmap b(String paramString)
  {
    Object localObject = g;
    paramString = ((d)localObject).a(paramString);
    if (paramString != null)
    {
      localObject = h;
      ((ae)localObject).a();
    }
    else
    {
      localObject = h;
      ((ae)localObject).b();
    }
    return paramString;
  }
  
  public final void b(a parama)
  {
    f.a(parama);
  }
  
  public final void b(Object paramObject)
  {
    i locali = f;
    Handler localHandler = i;
    paramObject = i.obtainMessage(11, paramObject);
    localHandler.sendMessage((Message)paramObject);
  }
  
  public final void c(Object paramObject)
  {
    i locali = f;
    Handler localHandler = i;
    paramObject = i.obtainMessage(12, paramObject);
    localHandler.sendMessage((Message)paramObject);
  }
  
  public final void d(Object paramObject)
  {
    al.b();
    Object localObject = (a)i.remove(paramObject);
    if (localObject != null)
    {
      ((a)localObject).b();
      i locali = f;
      locali.b((a)localObject);
    }
    boolean bool = paramObject instanceof ImageView;
    if (bool)
    {
      paramObject = (ImageView)paramObject;
      localObject = j;
      paramObject = (h)((Map)localObject).remove(paramObject);
      if (paramObject != null) {
        ((h)paramObject).a();
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.d.b.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */