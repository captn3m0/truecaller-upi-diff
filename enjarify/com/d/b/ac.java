package com.d.b;

import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory.Options;
import android.net.NetworkInfo;

public abstract class ac
{
  static void a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, BitmapFactory.Options paramOptions, aa paramaa)
  {
    float f1;
    if ((paramInt4 <= paramInt2) && (paramInt3 <= paramInt1))
    {
      paramInt1 = 1;
      f1 = Float.MIN_VALUE;
    }
    else
    {
      float f2;
      double d1;
      if (paramInt2 == 0)
      {
        f2 = paramInt3;
        f1 = paramInt1;
        f2 /= f1;
        d1 = Math.floor(f2);
        paramInt1 = (int)d1;
      }
      else if (paramInt1 == 0)
      {
        f1 = paramInt4;
        f2 = paramInt2;
        f1 /= f2;
        d1 = Math.floor(f1);
        paramInt1 = (int)d1;
      }
      else
      {
        float f3 = paramInt4;
        f2 = paramInt2;
        f3 /= f2;
        double d2 = Math.floor(f3);
        paramInt2 = (int)d2;
        float f4 = paramInt3;
        f1 = paramInt1;
        f4 /= f1;
        double d3 = Math.floor(f4);
        paramInt1 = (int)d3;
        paramInt3 = k;
        if (paramInt3 != 0) {
          paramInt1 = Math.max(paramInt2, paramInt1);
        } else {
          paramInt1 = Math.min(paramInt2, paramInt1);
        }
      }
    }
    inSampleSize = paramInt1;
    inJustDecodeBounds = false;
  }
  
  static void a(int paramInt1, int paramInt2, BitmapFactory.Options paramOptions, aa paramaa)
  {
    int i = outWidth;
    int j = outHeight;
    a(paramInt1, paramInt2, i, j, paramOptions, paramaa);
  }
  
  static boolean a(BitmapFactory.Options paramOptions)
  {
    if (paramOptions != null)
    {
      boolean bool = inJustDecodeBounds;
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  static BitmapFactory.Options c(aa paramaa)
  {
    boolean bool = paramaa.c();
    Bitmap.Config localConfig = q;
    int i;
    if (localConfig != null)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localConfig = null;
    }
    BitmapFactory.Options localOptions = null;
    if ((bool) || (i != 0))
    {
      localOptions = new android/graphics/BitmapFactory$Options;
      localOptions.<init>();
      inJustDecodeBounds = bool;
      if (i != 0)
      {
        paramaa = q;
        inPreferredConfig = paramaa;
      }
    }
    return localOptions;
  }
  
  int a()
  {
    return 0;
  }
  
  public abstract ac.a a(aa paramaa, int paramInt);
  
  boolean a(NetworkInfo paramNetworkInfo)
  {
    return false;
  }
  
  public abstract boolean a(aa paramaa);
  
  boolean b()
  {
    return false;
  }
}

/* Location:
 * Qualified Name:     com.d.b.ac
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */