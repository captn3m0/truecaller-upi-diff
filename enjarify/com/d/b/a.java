package com.d.b;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import java.lang.ref.WeakReference;

abstract class a
{
  final w a;
  final aa b;
  final WeakReference c;
  final boolean d;
  final int e;
  final int f;
  final int g;
  final Drawable h;
  final String i;
  final Object j;
  boolean k;
  boolean l;
  
  a(w paramw, Object paramObject1, aa paramaa, int paramInt1, int paramInt2, int paramInt3, Drawable paramDrawable, String paramString, Object paramObject2, boolean paramBoolean)
  {
    a = paramw;
    b = paramaa;
    if (paramObject1 == null)
    {
      paramw = null;
    }
    else
    {
      paramaa = new com/d/b/a$a;
      paramw = k;
      paramaa.<init>(this, paramObject1, paramw);
      paramw = paramaa;
    }
    c = paramw;
    e = paramInt1;
    f = paramInt2;
    d = paramBoolean;
    g = paramInt3;
    h = paramDrawable;
    i = paramString;
    if (paramObject2 == null) {
      paramObject2 = this;
    }
    j = paramObject2;
  }
  
  abstract void a();
  
  abstract void a(Bitmap paramBitmap, w.d paramd);
  
  void b()
  {
    l = true;
  }
  
  Object c()
  {
    WeakReference localWeakReference = c;
    if (localWeakReference == null) {
      return null;
    }
    return localWeakReference.get();
  }
}

/* Location:
 * Qualified Name:     com.d.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */