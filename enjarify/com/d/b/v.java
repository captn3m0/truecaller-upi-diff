package com.d.b;

import android.content.Context;
import android.net.Uri;
import com.d.a.c;
import com.d.a.d;
import com.d.a.d.a;
import com.d.a.e;
import com.d.a.v.a;
import com.d.a.x;
import com.d.a.y;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public final class v
  implements j
{
  private final com.d.a.t a;
  
  public v(Context paramContext)
  {
    this(paramContext);
  }
  
  private v(com.d.a.t paramt)
  {
    a = paramt;
  }
  
  private v(File paramFile)
  {
    this(paramFile, l);
  }
  
  private v(File paramFile, long paramLong)
  {
    this(localt);
    try
    {
      localt = a;
      localObject = new com/d/a/c;
      ((c)localObject).<init>(paramFile, paramLong);
      localt.a((c)localObject);
      return;
    }
    catch (IOException localIOException) {}
  }
  
  public final j.a a(Uri paramUri, int paramInt)
  {
    boolean bool1;
    if (paramInt != 0)
    {
      bool1 = t.c(paramInt);
      if (bool1)
      {
        localObject1 = d.b;
      }
      else
      {
        localObject1 = new com/d/a/d$a;
        ((d.a)localObject1).<init>();
        boolean bool2 = t.a(paramInt);
        if (!bool2) {
          ((d.a)localObject1).a();
        }
        bool2 = t.b(paramInt);
        if (!bool2) {
          ((d.a)localObject1).b();
        }
        localObject1 = ((d.a)localObject1).c();
      }
    }
    else
    {
      bool1 = false;
      localObject1 = null;
    }
    Object localObject2 = new com/d/a/v$a;
    ((v.a)localObject2).<init>();
    paramUri = paramUri.toString();
    paramUri = ((v.a)localObject2).a(paramUri);
    if (localObject1 != null) {
      paramUri.a((d)localObject1);
    }
    Object localObject1 = a;
    paramUri = paramUri.a();
    paramUri = ((com.d.a.t)localObject1).a(paramUri).a();
    int i = paramUri.a();
    int j = 300;
    if (i < j)
    {
      x localx = paramUri.e();
      if (localx != null)
      {
        paramInt = 1;
      }
      else
      {
        paramInt = 0;
        localx = null;
      }
      paramUri = paramUri.c();
      localObject1 = new com/d/b/j$a;
      localObject2 = paramUri.c();
      long l = paramUri.a();
      ((j.a)localObject1).<init>((InputStream)localObject2, paramInt, l);
      return (j.a)localObject1;
    }
    paramUri.c().close();
    localObject2 = new com/d/b/j$b;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(i);
    localStringBuilder.append(" ");
    paramUri = paramUri.b();
    localStringBuilder.append(paramUri);
    paramUri = localStringBuilder.toString();
    ((j.b)localObject2).<init>(paramUri, paramInt, i);
    throw ((Throwable)localObject2);
  }
  
  public final void a()
  {
    c localc = a.a();
    if (localc != null) {
      try
      {
        localc.a();
        return;
      }
      catch (IOException localIOException) {}
    }
  }
}

/* Location:
 * Qualified Name:     com.d.b.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */