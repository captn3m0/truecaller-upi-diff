package com.d.b;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

final class q
  extends InputStream
{
  private final InputStream a;
  private long b;
  private long c;
  private long d;
  private long e;
  
  public q(InputStream paramInputStream)
  {
    this(paramInputStream, (byte)0);
  }
  
  private q(InputStream paramInputStream, byte paramByte)
  {
    long l = -1;
    e = l;
    paramByte = paramInputStream.markSupported();
    if (paramByte == 0)
    {
      BufferedInputStream localBufferedInputStream = new java/io/BufferedInputStream;
      int i = 4096;
      localBufferedInputStream.<init>(paramInputStream, i);
      paramInputStream = localBufferedInputStream;
    }
    a = paramInputStream;
  }
  
  private void a(long paramLong1, long paramLong2)
  {
    for (;;)
    {
      boolean bool1 = paramLong1 < paramLong2;
      if (!bool1) {
        break;
      }
      InputStream localInputStream = a;
      long l1 = paramLong2 - paramLong1;
      long l2 = localInputStream.skip(l1);
      long l3 = 0L;
      boolean bool2 = l2 < l3;
      if (!bool2)
      {
        int i = read();
        int j = -1;
        if (i == j) {
          break;
        }
        l2 = 1L;
      }
      paramLong1 += l2;
    }
  }
  
  private void b(long paramLong)
  {
    try
    {
      long l1 = c;
      long l2 = b;
      boolean bool = l1 < l2;
      if (bool)
      {
        l1 = b;
        l2 = d;
        bool = l1 < l2;
        if (!bool)
        {
          localInputStream = a;
          localInputStream.reset();
          localInputStream = a;
          l3 = c;
          l3 = paramLong - l3;
          i = (int)l3;
          localInputStream.mark(i);
          l1 = c;
          l2 = b;
          a(l1, l2);
          break label146;
        }
      }
      l1 = b;
      c = l1;
      InputStream localInputStream = a;
      long l3 = b;
      l3 = paramLong - l3;
      int i = (int)l3;
      localInputStream.mark(i);
      label146:
      d = paramLong;
      return;
    }
    catch (IOException localIOException)
    {
      IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
      String str = String.valueOf(localIOException);
      str = "Unable to mark: ".concat(str);
      localIllegalStateException.<init>(str);
      throw localIllegalStateException;
    }
  }
  
  public final long a(int paramInt)
  {
    long l1 = b;
    long l2 = paramInt;
    l1 += l2;
    l2 = d;
    paramInt = l2 < l1;
    if (paramInt < 0) {
      b(l1);
    }
    return b;
  }
  
  public final void a(long paramLong)
  {
    long l1 = b;
    long l2 = d;
    boolean bool1 = l1 < l2;
    if (!bool1)
    {
      l1 = c;
      boolean bool2 = paramLong < l1;
      if (!bool2)
      {
        a.reset();
        l1 = c;
        a(l1, paramLong);
        b = paramLong;
        return;
      }
    }
    IOException localIOException = new java/io/IOException;
    localIOException.<init>("Cannot reset");
    throw localIOException;
  }
  
  public final int available()
  {
    return a.available();
  }
  
  public final void close()
  {
    a.close();
  }
  
  public final void mark(int paramInt)
  {
    long l = a(paramInt);
    e = l;
  }
  
  public final boolean markSupported()
  {
    return a.markSupported();
  }
  
  public final int read()
  {
    InputStream localInputStream = a;
    int i = localInputStream.read();
    int j = -1;
    if (i != j)
    {
      long l1 = b;
      long l2 = 1L;
      l1 += l2;
      b = l1;
    }
    return i;
  }
  
  public final int read(byte[] paramArrayOfByte)
  {
    InputStream localInputStream = a;
    int i = localInputStream.read(paramArrayOfByte);
    int j = -1;
    if (i != j)
    {
      long l1 = b;
      long l2 = i;
      l1 += l2;
      b = l1;
    }
    return i;
  }
  
  public final int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    InputStream localInputStream = a;
    int i = localInputStream.read(paramArrayOfByte, paramInt1, paramInt2);
    paramInt1 = -1;
    if (i != paramInt1)
    {
      long l1 = b;
      long l2 = i;
      l1 += l2;
      b = l1;
    }
    return i;
  }
  
  public final void reset()
  {
    long l = e;
    a(l);
  }
  
  public final long skip(long paramLong)
  {
    paramLong = a.skip(paramLong);
    long l = b + paramLong;
    b = l;
    return paramLong;
  }
}

/* Location:
 * Qualified Name:     com.d.b.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */