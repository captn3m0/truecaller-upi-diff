package com.d.b;

public enum t
{
  final int d;
  
  static
  {
    Object localObject = new com/d/b/t;
    int i = 1;
    ((t)localObject).<init>("NO_CACHE", 0, i);
    a = (t)localObject;
    localObject = new com/d/b/t;
    int j = 2;
    ((t)localObject).<init>("NO_STORE", i, j);
    b = (t)localObject;
    localObject = new com/d/b/t;
    ((t)localObject).<init>("OFFLINE", j, 4);
    c = (t)localObject;
    localObject = new t[3];
    t localt = a;
    localObject[0] = localt;
    localt = b;
    localObject[i] = localt;
    localt = c;
    localObject[j] = localt;
    e = (t[])localObject;
  }
  
  private t(int paramInt1)
  {
    d = paramInt1;
  }
  
  public static boolean a(int paramInt)
  {
    t localt = a;
    int i = d;
    paramInt &= i;
    return paramInt == 0;
  }
  
  public static boolean b(int paramInt)
  {
    t localt = b;
    int i = d;
    paramInt &= i;
    return paramInt == 0;
  }
  
  public static boolean c(int paramInt)
  {
    t localt = c;
    int i = d;
    paramInt &= i;
    return paramInt != 0;
  }
}

/* Location:
 * Qualified Name:     com.d.b.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */