package com.d.b;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;

final class ad
  extends ac
{
  private final Context a;
  
  ad(Context paramContext)
  {
    a = paramContext;
  }
  
  public final ac.a a(aa paramaa, int paramInt)
  {
    Object localObject = al.a(a, paramaa);
    int i = al.a((Resources)localObject, paramaa);
    ac.a locala = new com/d/b/ac$a;
    BitmapFactory.Options localOptions = c(paramaa);
    boolean bool = a(localOptions);
    if (bool)
    {
      BitmapFactory.decodeResource((Resources)localObject, i, localOptions);
      int j = h;
      int k = i;
      a(j, k, localOptions, paramaa);
    }
    paramaa = BitmapFactory.decodeResource((Resources)localObject, i, localOptions);
    localObject = w.d.b;
    locala.<init>(paramaa, (w.d)localObject);
    return locala;
  }
  
  public final boolean a(aa paramaa)
  {
    int i = e;
    if (i != 0) {
      return true;
    }
    paramaa = d.getScheme();
    return "android.resource".equals(paramaa);
  }
}

/* Location:
 * Qualified Name:     com.d.b.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */