package com.d.b;

public final class af
{
  public final int a;
  public final int b;
  public final long c;
  public final long d;
  public final long e;
  public final long f;
  public final long g;
  public final long h;
  public final long i;
  public final long j;
  public final int k;
  public final int l;
  public final int m;
  public final long n;
  
  public af(int paramInt1, int paramInt2, long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5, long paramLong6, long paramLong7, long paramLong8, int paramInt3, int paramInt4, int paramInt5, long paramLong9)
  {
    a = paramInt1;
    b = paramInt2;
    c = paramLong1;
    d = paramLong2;
    e = paramLong3;
    f = paramLong4;
    g = paramLong5;
    h = paramLong6;
    i = paramLong7;
    j = paramLong8;
    k = paramInt3;
    l = paramInt4;
    m = paramInt5;
    n = paramLong9;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("StatsSnapshot{maxSize=");
    int i1 = a;
    localStringBuilder.append(i1);
    localStringBuilder.append(", size=");
    i1 = b;
    localStringBuilder.append(i1);
    localStringBuilder.append(", cacheHits=");
    long l1 = c;
    localStringBuilder.append(l1);
    localStringBuilder.append(", cacheMisses=");
    l1 = d;
    localStringBuilder.append(l1);
    localStringBuilder.append(", downloadCount=");
    i1 = k;
    localStringBuilder.append(i1);
    localStringBuilder.append(", totalDownloadSize=");
    l1 = e;
    localStringBuilder.append(l1);
    localStringBuilder.append(", averageDownloadSize=");
    l1 = h;
    localStringBuilder.append(l1);
    localStringBuilder.append(", totalOriginalBitmapSize=");
    l1 = f;
    localStringBuilder.append(l1);
    localStringBuilder.append(", totalTransformedBitmapSize=");
    l1 = g;
    localStringBuilder.append(l1);
    localStringBuilder.append(", averageOriginalBitmapSize=");
    l1 = i;
    localStringBuilder.append(l1);
    localStringBuilder.append(", averageTransformedBitmapSize=");
    l1 = j;
    localStringBuilder.append(l1);
    localStringBuilder.append(", originalBitmapCount=");
    i1 = l;
    localStringBuilder.append(i1);
    localStringBuilder.append(", transformedBitmapCount=");
    i1 = m;
    localStringBuilder.append(i1);
    localStringBuilder.append(", timeStamp=");
    l1 = n;
    localStringBuilder.append(l1);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.d.b.af
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */