package com.d.b;

import android.graphics.Bitmap;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import java.io.InputStream;

public final class u
  extends ac
{
  private final j a;
  private final ae b;
  
  public u(j paramj, ae paramae)
  {
    a = paramj;
    b = paramae;
  }
  
  final int a()
  {
    return 2;
  }
  
  public final ac.a a(aa paramaa, int paramInt)
  {
    Object localObject1 = a;
    Object localObject2 = d;
    int i = c;
    paramaa = ((j)localObject1).a((Uri)localObject2, i);
    paramInt = c;
    if (paramInt != 0) {
      localObject1 = w.d.b;
    } else {
      localObject1 = w.d.c;
    }
    localObject2 = b;
    if (localObject2 != null)
    {
      paramaa = new com/d/b/ac$a;
      paramaa.<init>((Bitmap)localObject2, (w.d)localObject1);
      return paramaa;
    }
    localObject2 = a;
    if (localObject2 == null) {
      return null;
    }
    Object localObject3 = w.d.b;
    long l1 = 0L;
    long l2;
    boolean bool;
    if (localObject1 == localObject3)
    {
      l2 = d;
      bool = l2 < l1;
      if (!bool)
      {
        al.a((InputStream)localObject2);
        paramaa = new com/d/b/u$a;
        paramaa.<init>("Received response with 0 content-length header.");
        throw paramaa;
      }
    }
    localObject3 = w.d.c;
    if (localObject1 == localObject3)
    {
      l2 = d;
      bool = l2 < l1;
      if (bool)
      {
        localObject3 = b;
        l1 = d;
        paramaa = c;
        localObject3 = c;
        int j = 4;
        Long localLong = Long.valueOf(l1);
        localObject3 = ((Handler)localObject3).obtainMessage(j, localLong);
        paramaa.sendMessage((Message)localObject3);
      }
    }
    paramaa = new com/d/b/ac$a;
    paramaa.<init>((InputStream)localObject2, (w.d)localObject1);
    return paramaa;
  }
  
  final boolean a(NetworkInfo paramNetworkInfo)
  {
    if (paramNetworkInfo != null)
    {
      boolean bool = paramNetworkInfo.isConnected();
      if (!bool) {
        return false;
      }
    }
    return true;
  }
  
  public final boolean a(aa paramaa)
  {
    paramaa = d.getScheme();
    String str = "http";
    boolean bool1 = str.equals(paramaa);
    if (!bool1)
    {
      str = "https";
      boolean bool2 = str.equals(paramaa);
      if (!bool2) {
        return false;
      }
    }
    return true;
  }
  
  final boolean b()
  {
    return true;
  }
}

/* Location:
 * Qualified Name:     com.d.b.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */