package com.d.b;

import android.graphics.Bitmap.Config;
import android.net.Uri;
import java.util.ArrayList;
import java.util.List;

public final class aa$a
{
  public int a;
  public int b;
  boolean c;
  boolean d;
  public boolean e;
  List f;
  public Bitmap.Config g;
  public w.e h;
  private Uri i;
  private int j;
  private String k;
  private float l;
  private float m;
  private float n;
  private boolean o;
  
  aa$a(Uri paramUri, int paramInt, Bitmap.Config paramConfig)
  {
    i = paramUri;
    j = paramInt;
    g = paramConfig;
  }
  
  private aa$a(aa paramaa)
  {
    Object localObject = d;
    i = ((Uri)localObject);
    int i1 = e;
    j = i1;
    localObject = f;
    k = ((String)localObject);
    i1 = h;
    a = i1;
    i1 = i;
    b = i1;
    boolean bool = j;
    c = bool;
    bool = k;
    d = bool;
    float f1 = m;
    l = f1;
    f1 = n;
    m = f1;
    f1 = o;
    n = f1;
    bool = p;
    o = bool;
    bool = l;
    e = bool;
    localObject = g;
    if (localObject != null)
    {
      localObject = new java/util/ArrayList;
      List localList = g;
      ((ArrayList)localObject).<init>(localList);
      f = ((List)localObject);
    }
    localObject = q;
    g = ((Bitmap.Config)localObject);
    paramaa = r;
    h = paramaa;
  }
  
  public final a a(int paramInt1, int paramInt2)
  {
    if (paramInt1 >= 0)
    {
      if (paramInt2 >= 0)
      {
        if ((paramInt2 == 0) && (paramInt1 == 0))
        {
          localIllegalArgumentException = new java/lang/IllegalArgumentException;
          localIllegalArgumentException.<init>("At least one dimension has to be positive number.");
          throw localIllegalArgumentException;
        }
        a = paramInt1;
        b = paramInt2;
        return this;
      }
      localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localIllegalArgumentException.<init>("Height must be positive number or 0.");
      throw localIllegalArgumentException;
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    localIllegalArgumentException.<init>("Width must be positive number or 0.");
    throw localIllegalArgumentException;
  }
  
  public final a a(Uri paramUri)
  {
    if (paramUri != null)
    {
      i = paramUri;
      j = 0;
      return this;
    }
    paramUri = new java/lang/IllegalArgumentException;
    paramUri.<init>("Image URI may not be null.");
    throw paramUri;
  }
  
  public final boolean a()
  {
    Uri localUri = i;
    if (localUri == null)
    {
      int i1 = j;
      if (i1 == 0) {
        return false;
      }
    }
    return true;
  }
  
  final boolean b()
  {
    int i1 = a;
    if (i1 == 0)
    {
      i1 = b;
      if (i1 == 0) {
        return false;
      }
    }
    return true;
  }
  
  public final aa c()
  {
    a locala = this;
    boolean bool1 = d;
    if (bool1)
    {
      bool1 = c;
      if (bool1)
      {
        localObject = new java/lang/IllegalStateException;
        ((IllegalStateException)localObject).<init>("Center crop and center inside can not be used together.");
        throw ((Throwable)localObject);
      }
    }
    bool1 = c;
    if (bool1)
    {
      int i1 = a;
      if (i1 == 0)
      {
        i1 = b;
        if (i1 == 0)
        {
          localObject = new java/lang/IllegalStateException;
          ((IllegalStateException)localObject).<init>("Center crop requires calling resize with positive width and height.");
          throw ((Throwable)localObject);
        }
      }
    }
    boolean bool2 = d;
    if (bool2)
    {
      int i2 = a;
      if (i2 == 0)
      {
        i2 = b;
        if (i2 == 0)
        {
          localObject = new java/lang/IllegalStateException;
          ((IllegalStateException)localObject).<init>("Center inside requires calling resize with positive width and height.");
          throw ((Throwable)localObject);
        }
      }
    }
    Object localObject = h;
    if (localObject == null)
    {
      localObject = w.e.b;
      h = ((w.e)localObject);
    }
    aa localaa = new com/d/b/aa;
    Uri localUri = i;
    int i3 = j;
    String str = k;
    List localList = f;
    int i4 = a;
    int i5 = b;
    boolean bool3 = c;
    boolean bool4 = d;
    boolean bool5 = e;
    float f1 = l;
    float f2 = m;
    float f3 = n;
    boolean bool6 = o;
    Bitmap.Config localConfig = g;
    localObject = h;
    localaa.<init>(localUri, i3, str, localList, i4, i5, bool3, bool4, bool5, f1, f2, f3, bool6, localConfig, (w.e)localObject, (byte)0);
    return localaa;
  }
}

/* Location:
 * Qualified Name:     com.d.b.aa.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */