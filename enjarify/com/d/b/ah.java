package com.d.b;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

final class ah
  extends a
{
  ah(w paramw, ag paramag, aa paramaa, int paramInt1, int paramInt2, Drawable paramDrawable, String paramString, Object paramObject, int paramInt3)
  {
    super(paramw, paramag, paramaa, paramInt1, paramInt2, paramInt3, paramDrawable, paramString, paramObject, false);
  }
  
  final void a()
  {
    ag localag = (ag)c();
    if (localag != null)
    {
      int i = g;
      if (i != 0)
      {
        localObject = a.e.getResources();
        int j = g;
        localObject = ((Resources)localObject).getDrawable(j);
        localag.a((Drawable)localObject);
        return;
      }
      Object localObject = h;
      localag.a((Drawable)localObject);
    }
  }
  
  final void a(Bitmap paramBitmap, w.d paramd)
  {
    if (paramBitmap != null)
    {
      paramd = (ag)c();
      if (paramd != null)
      {
        paramd.a(paramBitmap);
        boolean bool = paramBitmap.isRecycled();
        if (bool)
        {
          paramBitmap = new java/lang/IllegalStateException;
          paramBitmap.<init>("Target callback must not recycle bitmap!");
          throw paramBitmap;
        }
      }
      return;
    }
    paramBitmap = new java/lang/AssertionError;
    paramd = new Object[1];
    paramd[0] = this;
    paramd = String.format("Attempted to complete action with no result!\n%s", paramd);
    paramBitmap.<init>(paramd);
    throw paramBitmap;
  }
}

/* Location:
 * Qualified Name:     com.d.b.ah
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */