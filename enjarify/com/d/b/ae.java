package com.d.b;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

public final class ae
{
  public final HandlerThread a;
  final d b;
  final Handler c;
  long d;
  long e;
  long f;
  long g;
  long h;
  long i;
  long j;
  long k;
  int l;
  int m;
  int n;
  
  ae(d paramd)
  {
    b = paramd;
    paramd = new android/os/HandlerThread;
    paramd.<init>("Picasso-Stats", 10);
    a = paramd;
    a.start();
    al.a(a.getLooper());
    paramd = new com/d/b/ae$a;
    Looper localLooper = a.getLooper();
    paramd.<init>(localLooper, this);
    c = paramd;
  }
  
  final void a()
  {
    c.sendEmptyMessage(0);
  }
  
  final void a(Bitmap paramBitmap, int paramInt)
  {
    int i1 = al.a(paramBitmap);
    Handler localHandler = c;
    paramBitmap = localHandler.obtainMessage(paramInt, i1, 0);
    localHandler.sendMessage(paramBitmap);
  }
  
  final void b()
  {
    c.sendEmptyMessage(1);
  }
}

/* Location:
 * Qualified Name:     com.d.b.ae
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */