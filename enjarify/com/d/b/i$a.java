package com.d.b;

import android.graphics.Bitmap;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class i$a
  extends Handler
{
  private final i a;
  
  public i$a(Looper paramLooper, i parami)
  {
    super(paramLooper);
    a = parami;
  }
  
  public final void handleMessage(Message paramMessage)
  {
    int i = what;
    boolean bool2 = false;
    Object localObject1 = null;
    int j = 1;
    Object localObject2;
    Object localObject3;
    Object localObject4;
    label251:
    Object localObject12;
    boolean bool1;
    boolean bool3;
    switch (i)
    {
    case 3: 
    case 8: 
    default: 
      localObject2 = w.a;
      localObject1 = new com/d/b/i$a$1;
      ((i.a.1)localObject1).<init>(this, paramMessage);
      ((Handler)localObject2).post((Runnable)localObject1);
      return;
    case 12: 
      paramMessage = obj;
      a.a(paramMessage);
      return;
    case 11: 
      paramMessage = obj;
      localObject2 = a;
      localObject3 = h;
      boolean bool4 = ((Set)localObject3).add(paramMessage);
      if (bool4)
      {
        localObject3 = e.values().iterator();
        for (;;)
        {
          boolean bool5 = ((Iterator)localObject3).hasNext();
          if (!bool5) {
            break;
          }
          localObject4 = (c)((Iterator)localObject3).next();
          Object localObject5 = b;
          boolean bool6 = n;
          Object localObject6 = k;
          Object localObject7 = l;
          if (localObject7 != null)
          {
            bool7 = ((List)localObject7).isEmpty();
            if (!bool7)
            {
              bool7 = true;
              break label251;
            }
          }
          boolean bool7 = false;
          Object localObject8 = null;
          if ((localObject6 != null) || (bool7))
          {
            Object localObject9;
            boolean bool8;
            Object localObject10;
            Object localObject11;
            String str;
            if (localObject6 != null)
            {
              localObject9 = j;
              bool8 = localObject9.equals(paramMessage);
              if (bool8)
              {
                ((c)localObject4).a((a)localObject6);
                localObject9 = g;
                localObject10 = ((a)localObject6).c();
                ((Map)localObject9).put(localObject10, localObject6);
                if (bool6)
                {
                  localObject9 = "Dispatcher";
                  localObject10 = "paused";
                  localObject6 = b.a();
                  localObject11 = new java/lang/StringBuilder;
                  ((StringBuilder)localObject11).<init>("because tag '");
                  ((StringBuilder)localObject11).append(paramMessage);
                  str = "' was paused";
                  ((StringBuilder)localObject11).append(str);
                  localObject11 = ((StringBuilder)localObject11).toString();
                  al.a((String)localObject9, (String)localObject10, (String)localObject6, (String)localObject11);
                }
              }
            }
            if (bool7)
            {
              int k = ((List)localObject7).size() - j;
              while (k >= 0)
              {
                localObject8 = (a)((List)localObject7).get(k);
                localObject9 = j;
                bool8 = localObject9.equals(paramMessage);
                if (bool8)
                {
                  ((c)localObject4).a((a)localObject8);
                  localObject9 = g;
                  localObject10 = ((a)localObject8).c();
                  ((Map)localObject9).put(localObject10, localObject8);
                  if (bool6)
                  {
                    localObject9 = "Dispatcher";
                    localObject10 = "paused";
                    localObject8 = b.a();
                    localObject11 = new java/lang/StringBuilder;
                    ((StringBuilder)localObject11).<init>("because tag '");
                    ((StringBuilder)localObject11).append(paramMessage);
                    str = "' was paused";
                    ((StringBuilder)localObject11).append(str);
                    localObject11 = ((StringBuilder)localObject11).toString();
                    al.a((String)localObject9, (String)localObject10, (String)localObject8, (String)localObject11);
                  }
                }
                k += -1;
              }
            }
            boolean bool9 = ((c)localObject4).b();
            if (bool9)
            {
              ((Iterator)localObject3).remove();
              if (bool6)
              {
                localObject5 = "Dispatcher";
                localObject6 = "canceled";
                localObject4 = al.a((c)localObject4);
                localObject7 = "all actions paused";
                al.a((String)localObject5, (String)localObject6, (String)localObject4, (String)localObject7);
              }
            }
          }
        }
      }
      return;
    case 10: 
      localObject2 = a;
      int m = arg1;
      if (m == j) {
        bool2 = true;
      }
      p = bool2;
      return;
    case 9: 
      paramMessage = (NetworkInfo)obj;
      a.a(paramMessage);
      return;
    case 7: 
      paramMessage = a;
      localObject2 = new java/util/ArrayList;
      localObject1 = m;
      ((ArrayList)localObject2).<init>((Collection)localObject1);
      m.clear();
      localObject1 = j;
      paramMessage = j.obtainMessage(8, localObject2);
      ((Handler)localObject1).sendMessage(paramMessage);
      i.a((List)localObject2);
      return;
    case 6: 
      paramMessage = (c)obj;
      a.a(paramMessage, false);
      return;
    case 5: 
      paramMessage = (c)obj;
      a.c(paramMessage);
      return;
    case 4: 
      paramMessage = (c)obj;
      localObject2 = a;
      bool2 = s.b(h);
      if (bool2)
      {
        localObject1 = k;
        localObject12 = f;
        localObject3 = m;
        ((d)localObject1).a((String)localObject12, (Bitmap)localObject3);
      }
      localObject1 = e;
      localObject12 = f;
      ((Map)localObject1).remove(localObject12);
      ((i)localObject2).d(paramMessage);
      localObject2 = b;
      bool1 = n;
      if (bool1)
      {
        localObject2 = "Dispatcher";
        localObject1 = "batched";
        paramMessage = al.a(paramMessage);
        localObject12 = "for completion";
        al.a((String)localObject2, (String)localObject1, paramMessage, (String)localObject12);
      }
      return;
    case 2: 
      paramMessage = (a)obj;
      localObject2 = a;
      localObject1 = i;
      localObject12 = (c)e.get(localObject1);
      if (localObject12 != null)
      {
        ((c)localObject12).a(paramMessage);
        bool3 = ((c)localObject12).b();
        if (bool3)
        {
          localObject12 = e;
          ((Map)localObject12).remove(localObject1);
          localObject1 = a;
          bool2 = n;
          if (bool2)
          {
            localObject1 = "Dispatcher";
            localObject12 = "canceled";
            localObject3 = b.a();
            al.a((String)localObject1, (String)localObject12, (String)localObject3);
          }
        }
      }
      localObject1 = h;
      localObject12 = j;
      bool2 = ((Set)localObject1).contains(localObject12);
      if (bool2)
      {
        localObject1 = g;
        localObject12 = paramMessage.c();
        ((Map)localObject1).remove(localObject12);
        localObject1 = a;
        bool2 = n;
        if (bool2)
        {
          localObject1 = "Dispatcher";
          localObject12 = "canceled";
          localObject3 = b.a();
          localObject4 = "because paused request got canceled";
          al.a((String)localObject1, (String)localObject12, (String)localObject3, (String)localObject4);
        }
      }
      localObject2 = f;
      paramMessage = paramMessage.c();
      paramMessage = (a)((Map)localObject2).remove(paramMessage);
      if (paramMessage != null)
      {
        localObject2 = a;
        bool1 = n;
        if (bool1)
        {
          localObject2 = "Dispatcher";
          localObject1 = "canceled";
          paramMessage = b.a();
          localObject12 = "from replaying";
          al.a((String)localObject2, (String)localObject1, paramMessage, (String)localObject12);
        }
      }
      return;
    }
    paramMessage = (a)obj;
    a.a(paramMessage, bool3);
  }
}

/* Location:
 * Qualified Name:     com.d.b.i.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */