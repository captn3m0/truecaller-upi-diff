package com.d.b;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public final class i
{
  public final i.b a;
  final Context b;
  public final ExecutorService c;
  public final j d;
  final Map e;
  final Map f;
  final Map g;
  final Set h;
  final Handler i;
  final Handler j;
  final d k;
  final ae l;
  final List m;
  final i.c n;
  final boolean o;
  boolean p;
  
  i(Context paramContext, ExecutorService paramExecutorService, Handler paramHandler, j paramj, d paramd, ae paramae)
  {
    Object localObject = new com/d/b/i$b;
    ((i.b)localObject).<init>();
    a = ((i.b)localObject);
    a.start();
    al.a(a.getLooper());
    b = paramContext;
    c = paramExecutorService;
    paramExecutorService = new java/util/LinkedHashMap;
    paramExecutorService.<init>();
    e = paramExecutorService;
    paramExecutorService = new java/util/WeakHashMap;
    paramExecutorService.<init>();
    f = paramExecutorService;
    paramExecutorService = new java/util/WeakHashMap;
    paramExecutorService.<init>();
    g = paramExecutorService;
    paramExecutorService = new java/util/HashSet;
    paramExecutorService.<init>();
    h = paramExecutorService;
    paramExecutorService = new com/d/b/i$a;
    localObject = a.getLooper();
    paramExecutorService.<init>((Looper)localObject, this);
    i = paramExecutorService;
    d = paramj;
    j = paramHandler;
    k = paramd;
    l = paramae;
    paramExecutorService = new java/util/ArrayList;
    paramExecutorService.<init>(4);
    m = paramExecutorService;
    boolean bool1 = al.d(b);
    p = bool1;
    boolean bool2 = al.b(paramContext, "android.permission.ACCESS_NETWORK_STATE");
    o = bool2;
    paramContext = new com/d/b/i$c;
    paramContext.<init>(this);
    n = paramContext;
    paramContext = n;
    paramExecutorService = new android/content/IntentFilter;
    paramExecutorService.<init>();
    paramExecutorService.addAction("android.intent.action.AIRPLANE_MODE");
    paramHandler = a;
    boolean bool3 = o;
    if (bool3)
    {
      paramHandler = "android.net.conn.CONNECTIVITY_CHANGE";
      paramExecutorService.addAction(paramHandler);
    }
    a.b.registerReceiver(paramContext, paramExecutorService);
  }
  
  static void a(List paramList)
  {
    boolean bool1 = paramList.isEmpty();
    if (bool1) {
      return;
    }
    Object localObject1 = get0b;
    bool1 = n;
    if (bool1)
    {
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      paramList = paramList.iterator();
      for (;;)
      {
        boolean bool2 = paramList.hasNext();
        if (!bool2) {
          break;
        }
        localObject2 = (c)paramList.next();
        int i1 = ((StringBuilder)localObject1).length();
        if (i1 > 0)
        {
          String str = ", ";
          ((StringBuilder)localObject1).append(str);
        }
        localObject2 = al.a((c)localObject2);
        ((StringBuilder)localObject1).append((String)localObject2);
      }
      paramList = "Dispatcher";
      Object localObject2 = "delivered";
      localObject1 = ((StringBuilder)localObject1).toString();
      al.a(paramList, (String)localObject2, (String)localObject1);
    }
  }
  
  private void c(a parama)
  {
    Object localObject = parama.c();
    if (localObject != null)
    {
      boolean bool = true;
      k = bool;
      Map localMap = f;
      localMap.put(localObject, parama);
    }
  }
  
  private void e(c paramc)
  {
    a locala1 = k;
    if (locala1 != null) {
      c(locala1);
    }
    paramc = l;
    if (paramc != null)
    {
      int i1 = 0;
      locala1 = null;
      int i2 = paramc.size();
      while (i1 < i2)
      {
        a locala2 = (a)paramc.get(i1);
        c(locala2);
        i1 += 1;
      }
    }
  }
  
  final void a(NetworkInfo paramNetworkInfo)
  {
    Object localObject1 = c;
    boolean bool1 = localObject1 instanceof y;
    if (bool1)
    {
      localObject1 = (y)localObject1;
      int i1 = 3;
      if (paramNetworkInfo != null)
      {
        boolean bool3 = paramNetworkInfo.isConnectedOrConnecting();
        if (bool3)
        {
          int i2 = paramNetworkInfo.getType();
          int i3 = 6;
          if (i2 != i3)
          {
            i3 = 9;
            if (i2 != i3) {
              switch (i2)
              {
              default: 
                ((y)localObject1).a(i1);
                break;
              case 0: 
                i2 = paramNetworkInfo.getSubtype();
                switch (i2)
                {
                default: 
                  switch (i2)
                  {
                  default: 
                    ((y)localObject1).a(i1);
                    break;
                  case 13: 
                  case 14: 
                  case 15: 
                    ((y)localObject1).a(i1);
                  }
                  break;
                case 3: 
                case 4: 
                case 5: 
                case 6: 
                  i1 = 2;
                  ((y)localObject1).a(i1);
                  break;
                case 1: 
                case 2: 
                  i1 = 1;
                  ((y)localObject1).a(i1);
                }
                break;
              }
            }
          }
          i1 = 4;
          ((y)localObject1).a(i1);
          break label231;
        }
      }
      ((y)localObject1).a(i1);
    }
    label231:
    if (paramNetworkInfo != null)
    {
      boolean bool4 = paramNetworkInfo.isConnected();
      if (bool4)
      {
        paramNetworkInfo = f;
        bool4 = paramNetworkInfo.isEmpty();
        if (!bool4)
        {
          paramNetworkInfo = f.values().iterator();
          for (;;)
          {
            boolean bool5 = paramNetworkInfo.hasNext();
            if (!bool5) {
              break;
            }
            localObject1 = (a)paramNetworkInfo.next();
            paramNetworkInfo.remove();
            Object localObject2 = a;
            boolean bool2 = n;
            if (bool2)
            {
              localObject2 = "Dispatcher";
              String str1 = "replaying";
              String str2 = b.a();
              al.a((String)localObject2, str1, str2);
            }
            bool2 = false;
            localObject2 = null;
            a((a)localObject1, false);
          }
        }
      }
    }
  }
  
  final void a(a parama)
  {
    Handler localHandler = i;
    parama = localHandler.obtainMessage(1, parama);
    localHandler.sendMessage(parama);
  }
  
  final void a(a parama, boolean paramBoolean)
  {
    Object localObject1 = h;
    Object localObject2 = j;
    boolean bool1 = ((Set)localObject1).contains(localObject2);
    String str;
    if (bool1)
    {
      localObject3 = g;
      localObject1 = parama.c();
      ((Map)localObject3).put(localObject1, parama);
      localObject3 = a;
      paramBoolean = n;
      if (paramBoolean)
      {
        localObject3 = "Dispatcher";
        localObject1 = "paused";
        localObject2 = b.a();
        localObject4 = new java/lang/StringBuilder;
        str = "because tag '";
        ((StringBuilder)localObject4).<init>(str);
        parama = j;
        ((StringBuilder)localObject4).append(parama);
        ((StringBuilder)localObject4).append("' is paused");
        parama = ((StringBuilder)localObject4).toString();
        al.a((String)localObject3, (String)localObject1, (String)localObject2, parama);
      }
      return;
    }
    localObject1 = e;
    localObject2 = i;
    localObject1 = (c)((Map)localObject1).get(localObject2);
    if (localObject1 != null)
    {
      localObject3 = b;
      paramBoolean = n;
      localObject2 = b;
      localObject4 = k;
      if (localObject4 == null)
      {
        k = parama;
        if (paramBoolean)
        {
          parama = l;
          if (parama != null)
          {
            parama = l;
            boolean bool2 = parama.isEmpty();
            if (!bool2)
            {
              parama = "Hunter";
              localObject3 = "joined";
              localObject2 = ((aa)localObject2).a();
              localObject4 = "to ";
              localObject1 = al.a((c)localObject1, (String)localObject4);
              al.a(parama, (String)localObject3, (String)localObject2, (String)localObject1);
              break label289;
            }
          }
          localObject1 = ((aa)localObject2).a();
          al.a("Hunter", "joined", (String)localObject1, "to empty hunter");
          return;
        }
        label289:
        return;
      }
      localObject4 = l;
      if (localObject4 == null)
      {
        localObject4 = new java/util/ArrayList;
        int i1 = 3;
        ((ArrayList)localObject4).<init>(i1);
        l = ((List)localObject4);
      }
      localObject4 = l;
      ((List)localObject4).add(parama);
      if (paramBoolean)
      {
        localObject3 = "Hunter";
        localObject4 = "joined";
        localObject2 = ((aa)localObject2).a();
        str = al.a((c)localObject1, "to ");
        al.a((String)localObject3, (String)localObject4, (String)localObject2, str);
      }
      parama = b.r;
      paramBoolean = parama.ordinal();
      localObject2 = s;
      boolean bool3 = ((w.e)localObject2).ordinal();
      if (paramBoolean > bool3) {
        s = parama;
      }
      return;
    }
    localObject1 = c;
    bool1 = ((ExecutorService)localObject1).isShutdown();
    if (bool1)
    {
      localObject3 = a;
      paramBoolean = n;
      if (paramBoolean)
      {
        localObject3 = "Dispatcher";
        localObject1 = "ignored";
        parama = b.a();
        localObject2 = "because shut down";
        al.a((String)localObject3, (String)localObject1, parama, (String)localObject2);
      }
      return;
    }
    localObject1 = a;
    localObject2 = k;
    Object localObject4 = l;
    localObject1 = c.a((w)localObject1, this, (d)localObject2, (ae)localObject4, parama);
    localObject2 = c.submit((Runnable)localObject1);
    n = ((Future)localObject2);
    localObject2 = e;
    localObject4 = i;
    ((Map)localObject2).put(localObject4, localObject1);
    if (paramBoolean)
    {
      localObject3 = f;
      localObject1 = parama.c();
      ((Map)localObject3).remove(localObject1);
    }
    Object localObject3 = a;
    paramBoolean = n;
    if (paramBoolean)
    {
      localObject3 = "Dispatcher";
      localObject1 = "enqueued";
      parama = b.a();
      al.a((String)localObject3, (String)localObject1, parama);
    }
  }
  
  final void a(c paramc)
  {
    Handler localHandler = i;
    paramc = localHandler.obtainMessage(5, paramc);
    localHandler.sendMessageDelayed(paramc, 500L);
  }
  
  final void a(c paramc, boolean paramBoolean)
  {
    Object localObject1 = b;
    boolean bool = n;
    if (bool)
    {
      localObject1 = "Dispatcher";
      String str1 = "batched";
      String str2 = al.a(paramc);
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      String str3 = "for error";
      localStringBuilder.<init>(str3);
      if (paramBoolean) {
        localObject2 = " (will replay)";
      } else {
        localObject2 = "";
      }
      localStringBuilder.append((String)localObject2);
      localObject2 = localStringBuilder.toString();
      al.a((String)localObject1, str1, str2, (String)localObject2);
    }
    Object localObject2 = e;
    localObject1 = f;
    ((Map)localObject2).remove(localObject1);
    d(paramc);
  }
  
  final void a(Object paramObject)
  {
    Object localObject1 = h;
    boolean bool1 = ((Set)localObject1).remove(paramObject);
    if (!bool1) {
      return;
    }
    bool1 = false;
    localObject1 = null;
    Iterator localIterator = g.values().iterator();
    for (;;)
    {
      boolean bool2 = localIterator.hasNext();
      if (!bool2) {
        break;
      }
      a locala = (a)localIterator.next();
      Object localObject2 = j;
      boolean bool3 = localObject2.equals(paramObject);
      if (bool3)
      {
        if (localObject1 == null)
        {
          localObject1 = new java/util/ArrayList;
          ((ArrayList)localObject1).<init>();
        }
        ((List)localObject1).add(locala);
        localIterator.remove();
      }
    }
    if (localObject1 != null)
    {
      paramObject = j;
      int i1 = 13;
      localObject1 = ((Handler)paramObject).obtainMessage(i1, localObject1);
      ((Handler)paramObject).sendMessage((Message)localObject1);
    }
  }
  
  final void b(a parama)
  {
    Handler localHandler = i;
    parama = localHandler.obtainMessage(2, parama);
    localHandler.sendMessage(parama);
  }
  
  final void b(c paramc)
  {
    Handler localHandler = i;
    paramc = localHandler.obtainMessage(6, paramc);
    localHandler.sendMessage(paramc);
  }
  
  final void c(c paramc)
  {
    boolean bool1 = paramc.c();
    if (bool1) {
      return;
    }
    Object localObject1 = c;
    bool1 = ((ExecutorService)localObject1).isShutdown();
    boolean bool2 = false;
    Object localObject2 = null;
    if (bool1)
    {
      a(paramc, false);
      return;
    }
    bool1 = false;
    localObject1 = null;
    int i3 = o;
    String str;
    if (i3 != 0)
    {
      localObject1 = b;
      str = "connectivity";
      localObject1 = ((ConnectivityManager)al.a((Context)localObject1, str)).getActiveNetworkInfo();
    }
    i3 = 1;
    if (localObject1 != null)
    {
      bool3 = ((NetworkInfo)localObject1).isConnected();
      if (bool3)
      {
        bool3 = true;
        break label104;
      }
    }
    boolean bool3 = false;
    label104:
    int i4 = r;
    if (i4 > 0)
    {
      i4 = 1;
    }
    else
    {
      i4 = 0;
      localac = null;
    }
    if (i4 == 0)
    {
      bool1 = false;
      localObject1 = null;
    }
    else
    {
      i4 = r - i3;
      r = i4;
      localac = j;
      bool1 = localac.a((NetworkInfo)localObject1);
    }
    ac localac = j;
    boolean bool4 = localac.b();
    if (!bool1)
    {
      bool1 = o;
      if ((bool1) && (bool4)) {
        bool2 = true;
      }
      a(paramc, bool2);
      if (bool2) {
        e(paramc);
      }
      return;
    }
    bool1 = o;
    if ((bool1) && (!bool3))
    {
      a(paramc, bool4);
      if (bool4) {
        e(paramc);
      }
      return;
    }
    localObject1 = b;
    bool1 = n;
    if (bool1)
    {
      localObject1 = "Dispatcher";
      localObject2 = "retrying";
      str = al.a(paramc);
      al.a((String)localObject1, (String)localObject2, str);
    }
    localObject1 = p;
    bool1 = localObject1 instanceof u.a;
    if (bool1)
    {
      int i1 = i;
      localObject2 = t.a;
      int i2 = d;
      i1 |= i2;
      i = i1;
    }
    localObject1 = c.submit(paramc);
    n = ((Future)localObject1);
  }
  
  final void d(c paramc)
  {
    boolean bool1 = paramc.c();
    if (bool1) {
      return;
    }
    List localList = m;
    localList.add(paramc);
    paramc = i;
    int i1 = 7;
    boolean bool2 = paramc.hasMessages(i1);
    if (!bool2)
    {
      paramc = i;
      long l1 = 200L;
      paramc.sendEmptyMessageDelayed(i1, l1);
    }
  }
}

/* Location:
 * Qualified Name:     com.d.b.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */