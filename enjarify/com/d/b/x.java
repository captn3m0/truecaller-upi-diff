package com.d.b;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.widget.ImageView;

final class x
  extends BitmapDrawable
{
  private static final Paint e;
  Drawable a;
  long b;
  boolean c;
  int d = 255;
  private final boolean f;
  private final float g;
  private final w.d h;
  
  static
  {
    Paint localPaint = new android/graphics/Paint;
    localPaint.<init>();
    e = localPaint;
  }
  
  private x(Context paramContext, Bitmap paramBitmap, Drawable paramDrawable, w.d paramd, boolean paramBoolean1, boolean paramBoolean2)
  {
    super(localResources, paramBitmap);
    f = paramBoolean2;
    float f1 = getResourcesgetDisplayMetricsdensity;
    g = f1;
    h = paramd;
    paramContext = w.d.a;
    boolean bool = true;
    int i;
    if ((paramd != paramContext) && (!paramBoolean1))
    {
      i = 1;
      f1 = Float.MIN_VALUE;
    }
    else
    {
      i = 0;
      f1 = 0.0F;
      paramContext = null;
    }
    if (i != 0)
    {
      a = paramDrawable;
      c = bool;
      long l = SystemClock.uptimeMillis();
      b = l;
    }
  }
  
  private static Path a(Point paramPoint, int paramInt)
  {
    Point localPoint1 = new android/graphics/Point;
    int i = x + paramInt;
    int j = y;
    localPoint1.<init>(i, j);
    Point localPoint2 = new android/graphics/Point;
    j = x;
    int k = y + paramInt;
    localPoint2.<init>(j, k);
    Path localPath = new android/graphics/Path;
    localPath.<init>();
    float f1 = x;
    float f2 = y;
    localPath.moveTo(f1, f2);
    f2 = x;
    float f3 = y;
    localPath.lineTo(f2, f3);
    f2 = x;
    f3 = y;
    localPath.lineTo(f2, f3);
    return localPath;
  }
  
  static void a(ImageView paramImageView, Context paramContext, Bitmap paramBitmap, w.d paramd, boolean paramBoolean1, boolean paramBoolean2)
  {
    Drawable localDrawable = paramImageView.getDrawable();
    boolean bool = localDrawable instanceof AnimationDrawable;
    if (bool)
    {
      localObject = localDrawable;
      localObject = (AnimationDrawable)localDrawable;
      ((AnimationDrawable)localObject).stop();
    }
    x localx = new com/d/b/x;
    Object localObject = localx;
    localx.<init>(paramContext, paramBitmap, (Drawable)localDrawable, paramd, paramBoolean1, paramBoolean2);
    paramImageView.setImageDrawable(localx);
  }
  
  static void a(ImageView paramImageView, Drawable paramDrawable)
  {
    paramImageView.setImageDrawable(paramDrawable);
    paramDrawable = paramImageView.getDrawable();
    boolean bool = paramDrawable instanceof AnimationDrawable;
    if (bool)
    {
      paramImageView = (AnimationDrawable)paramImageView.getDrawable();
      paramImageView.start();
    }
  }
  
  public final void draw(Canvas paramCanvas)
  {
    boolean bool1 = c;
    int j = 0;
    float f1 = 0.0F;
    Paint localPaint = null;
    float f3;
    Object localObject1;
    Object localObject2;
    int k;
    if (!bool1)
    {
      super.draw(paramCanvas);
    }
    else
    {
      long l1 = SystemClock.uptimeMillis();
      long l2 = b;
      l1 -= l2;
      float f2 = (float)l1 / 200.0F;
      f3 = 1.0F;
      boolean bool3 = f2 < f3;
      if (!bool3)
      {
        c = false;
        bool1 = false;
        f2 = 0.0F;
        localObject1 = null;
        a = null;
        super.draw(paramCanvas);
      }
      else
      {
        localObject2 = a;
        if (localObject2 != null) {
          ((Drawable)localObject2).draw(paramCanvas);
        }
        int i = (int)(d * f2);
        super.setAlpha(i);
        super.draw(paramCanvas);
        i = d;
        super.setAlpha(i);
        i = Build.VERSION.SDK_INT;
        k = 10;
        f3 = 1.4E-44F;
        if (i <= k) {
          invalidateSelf();
        }
      }
    }
    boolean bool2 = f;
    if (bool2)
    {
      e.setColor(-1);
      localObject1 = new android/graphics/Point;
      ((Point)localObject1).<init>(0, 0);
      float f4 = g * 16.0F;
      k = (int)f4;
      localObject1 = a((Point)localObject1, k);
      localObject2 = e;
      paramCanvas.drawPath((Path)localObject1, (Paint)localObject2);
      localObject1 = e;
      localObject2 = h;
      k = d;
      ((Paint)localObject1).setColor(k);
      localObject1 = new android/graphics/Point;
      ((Point)localObject1).<init>(0, 0);
      f1 = 15.0F;
      f3 = g * f1;
      j = (int)f3;
      localObject1 = a((Point)localObject1, j);
      localPaint = e;
      paramCanvas.drawPath((Path)localObject1, localPaint);
    }
  }
  
  protected final void onBoundsChange(Rect paramRect)
  {
    Drawable localDrawable = a;
    if (localDrawable != null) {
      localDrawable.setBounds(paramRect);
    }
    super.onBoundsChange(paramRect);
  }
  
  public final void setAlpha(int paramInt)
  {
    d = paramInt;
    Drawable localDrawable = a;
    if (localDrawable != null) {
      localDrawable.setAlpha(paramInt);
    }
    super.setAlpha(paramInt);
  }
  
  public final void setColorFilter(ColorFilter paramColorFilter)
  {
    Drawable localDrawable = a;
    if (localDrawable != null) {
      localDrawable.setColorFilter(paramColorFilter);
    }
    super.setColorFilter(paramColorFilter);
  }
}

/* Location:
 * Qualified Name:     com.d.b.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */