package com.d.b;

import android.content.ContentUris;
import android.content.UriMatcher;
import android.net.Uri;
import android.provider.ContactsContract.Contacts;
import c.g.b.k;
import com.truecaller.common.h.am;
import com.truecaller.content.TruecallerContract;

public final class m
  extends ac
{
  public u a;
  public f b;
  private final UriMatcher c;
  
  public m()
  {
    UriMatcher localUriMatcher = new android/content/UriMatcher;
    localUriMatcher.<init>(-1);
    c = localUriMatcher;
    localUriMatcher = c;
    String str = TruecallerContract.a();
    localUriMatcher.addURI(str, "photo", 0);
  }
  
  public final ac.a a(aa paramaa, int paramInt)
  {
    k.b(paramaa, "request");
    Object localObject1 = d;
    Object localObject2 = "pbid";
    localObject1 = ((Uri)localObject1).getQueryParameter((String)localObject2);
    long l1 = am.h((String)localObject1);
    long l2 = 0L;
    boolean bool1 = l1 < l2;
    if (bool1)
    {
      Uri localUri = ContactsContract.Contacts.CONTENT_URI;
      localObject1 = ContentUris.withAppendedId(localUri, l1);
      localObject1 = paramaa.f().a((Uri)localObject1).c();
      localObject2 = b;
      if (localObject2 != null)
      {
        boolean bool2 = ((f)localObject2).a((aa)localObject1);
        if (!bool2)
        {
          i = 0;
          localObject2 = null;
        }
        if (localObject2 != null)
        {
          localObject1 = ((f)localObject2).a((aa)localObject1, paramInt);
          if (localObject1 != null) {
            return (ac.a)localObject1;
          }
        }
      }
    }
    localObject1 = d;
    localObject2 = "tcphoto";
    localObject1 = ((Uri)localObject1).getQueryParameter((String)localObject2);
    if (localObject1 == null) {
      return null;
    }
    localObject2 = localObject1;
    localObject2 = (CharSequence)localObject1;
    int i = ((CharSequence)localObject2).length();
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject2 = null;
    }
    if (i != 0)
    {
      paramaa = paramaa.f();
      localObject1 = Uri.parse((String)localObject1);
      paramaa = paramaa.a((Uri)localObject1).c();
      localObject1 = a;
      if (localObject1 != null)
      {
        boolean bool3 = ((u)localObject1).a(paramaa);
        if (!bool3) {
          localObject1 = null;
        }
        if (localObject1 != null)
        {
          paramaa = ((u)localObject1).a(paramaa, paramInt);
          if (paramaa != null) {
            return paramaa;
          }
        }
      }
    }
    return null;
  }
  
  public final boolean a(aa paramaa)
  {
    k.b(paramaa, "data");
    UriMatcher localUriMatcher = c;
    paramaa = d;
    int i = localUriMatcher.match(paramaa);
    int j = -1;
    return i != j;
  }
}

/* Location:
 * Qualified Name:     com.d.b.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */