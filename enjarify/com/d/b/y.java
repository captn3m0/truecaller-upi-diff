package com.d.b;

import java.util.concurrent.Future;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class y
  extends ThreadPoolExecutor
{
  y()
  {
    super(3, 3, 0L, localTimeUnit, localPriorityBlockingQueue, localb);
  }
  
  final void a(int paramInt)
  {
    setCorePoolSize(paramInt);
    setMaximumPoolSize(paramInt);
  }
  
  public final Future submit(Runnable paramRunnable)
  {
    y.a locala = new com/d/b/y$a;
    paramRunnable = (c)paramRunnable;
    locala.<init>(paramRunnable);
    execute(locala);
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.d.b.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */