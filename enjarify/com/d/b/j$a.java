package com.d.b;

import android.graphics.Bitmap;
import java.io.InputStream;

public final class j$a
{
  final InputStream a;
  final Bitmap b;
  final boolean c;
  final long d;
  
  public j$a(InputStream paramInputStream, boolean paramBoolean, long paramLong)
  {
    if (paramInputStream != null)
    {
      a = paramInputStream;
      b = null;
      c = paramBoolean;
      d = paramLong;
      return;
    }
    paramInputStream = new java/lang/IllegalArgumentException;
    paramInputStream.<init>("Stream may not be null.");
    throw paramInputStream;
  }
}

/* Location:
 * Qualified Name:     com.d.b.j.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */