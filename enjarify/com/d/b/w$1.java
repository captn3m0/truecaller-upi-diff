package com.d.b;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.util.List;

final class w$1
  extends Handler
{
  w$1(Looper paramLooper)
  {
    super(paramLooper);
  }
  
  public final void handleMessage(Message paramMessage)
  {
    int i = what;
    int j = 3;
    String str;
    Object localObject1;
    Object localObject2;
    if (i != j)
    {
      j = 8;
      int k = 0;
      str = null;
      Object localObject3;
      Object localObject4;
      Object localObject5;
      if (i != j)
      {
        j = 13;
        if (i == j)
        {
          paramMessage = (List)obj;
          i = paramMessage.size();
          while (k < i)
          {
            localObject1 = (a)paramMessage.get(k);
            localObject2 = a;
            localObject3 = null;
            boolean bool2 = s.a(e);
            if (bool2)
            {
              localObject3 = i;
              localObject3 = ((w)localObject2).b((String)localObject3);
            }
            boolean bool3;
            if (localObject3 != null)
            {
              localObject4 = w.d.a;
              ((w)localObject2).a((Bitmap)localObject3, (w.d)localObject4, (a)localObject1);
              bool3 = n;
              if (bool3)
              {
                localObject2 = "Main";
                localObject3 = "completed";
                localObject1 = b.a();
                localObject4 = new java/lang/StringBuilder;
                ((StringBuilder)localObject4).<init>("from ");
                localObject5 = w.d.a;
                ((StringBuilder)localObject4).append(localObject5);
                localObject4 = ((StringBuilder)localObject4).toString();
                al.a((String)localObject2, (String)localObject3, (String)localObject1, (String)localObject4);
              }
            }
            else
            {
              ((w)localObject2).a((a)localObject1);
              bool3 = n;
              if (bool3)
              {
                localObject2 = "Main";
                localObject3 = "resumed";
                localObject1 = b.a();
                al.a((String)localObject2, (String)localObject3, (String)localObject1);
              }
            }
            k += 1;
          }
          return;
        }
        localObject6 = new java/lang/AssertionError;
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>("Unknown handler message received: ");
        int n = what;
        ((StringBuilder)localObject1).append(n);
        paramMessage = ((StringBuilder)localObject1).toString();
        ((AssertionError)localObject6).<init>(paramMessage);
        throw ((Throwable)localObject6);
      }
      paramMessage = (List)obj;
      i = paramMessage.size();
      j = 0;
      localObject1 = null;
      while (j < i)
      {
        localObject2 = (c)paramMessage.get(j);
        localObject3 = b;
        localObject4 = k;
        localObject5 = l;
        int i1 = 1;
        if (localObject5 != null)
        {
          i2 = ((List)localObject5).isEmpty();
          if (i2 == 0)
          {
            i2 = 1;
            break label397;
          }
        }
        int i2 = 0;
        label397:
        Bitmap localBitmap;
        if ((localObject4 == null) && (i2 == 0))
        {
          i1 = 0;
          localBitmap = null;
        }
        if (i1 != 0)
        {
          localBitmap = m;
          localObject2 = o;
          if (localObject4 != null) {
            ((w)localObject3).a(localBitmap, (w.d)localObject2, (a)localObject4);
          }
          if (i2 != 0)
          {
            int m = ((List)localObject5).size();
            i2 = 0;
            while (i2 < m)
            {
              a locala = (a)((List)localObject5).get(i2);
              ((w)localObject3).a(localBitmap, (w.d)localObject2, locala);
              int i3;
              i2 += 1;
            }
          }
        }
        j += 1;
      }
      return;
    }
    paramMessage = (a)obj;
    Object localObject6 = a;
    boolean bool1 = n;
    if (bool1)
    {
      localObject6 = "Main";
      localObject1 = "canceled";
      str = b.a();
      localObject2 = "target got garbage collected";
      al.a((String)localObject6, (String)localObject1, str, (String)localObject2);
    }
    localObject6 = a;
    paramMessage = paramMessage.c();
    w.a((w)localObject6, paramMessage);
  }
}

/* Location:
 * Qualified Name:     com.d.b.w.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */