package com.d.b;

import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.ImageView;
import java.lang.ref.WeakReference;

public final class h
  implements ViewTreeObserver.OnPreDrawListener
{
  final ab a;
  final WeakReference b;
  e c;
  
  h(ab paramab, ImageView paramImageView, e parame)
  {
    a = paramab;
    paramab = new java/lang/ref/WeakReference;
    paramab.<init>(paramImageView);
    b = paramab;
    c = parame;
    paramImageView.getViewTreeObserver().addOnPreDrawListener(this);
  }
  
  public final void a()
  {
    c = null;
    Object localObject = (ImageView)b.get();
    if (localObject == null) {
      return;
    }
    localObject = ((ImageView)localObject).getViewTreeObserver();
    boolean bool = ((ViewTreeObserver)localObject).isAlive();
    if (!bool) {
      return;
    }
    ((ViewTreeObserver)localObject).removeOnPreDrawListener(this);
  }
  
  public final boolean onPreDraw()
  {
    ImageView localImageView = (ImageView)b.get();
    boolean bool1 = true;
    if (localImageView == null) {
      return bool1;
    }
    Object localObject = localImageView.getViewTreeObserver();
    boolean bool2 = ((ViewTreeObserver)localObject).isAlive();
    if (!bool2) {
      return bool1;
    }
    int i = localImageView.getWidth();
    int j = localImageView.getHeight();
    if ((i > 0) && (j > 0))
    {
      ((ViewTreeObserver)localObject).removeOnPreDrawListener(this);
      localObject = a;
      c = false;
      localObject = ((ab)localObject).b(i, j);
      e locale = c;
      ((ab)localObject).a(localImageView, locale);
      return bool1;
    }
    return bool1;
  }
}

/* Location:
 * Qualified Name:     com.d.b.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */