package com.d.b;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import java.lang.ref.WeakReference;

final class o
  extends a
{
  e m;
  
  o(w paramw, ImageView paramImageView, aa paramaa, int paramInt1, int paramInt2, int paramInt3, Drawable paramDrawable, String paramString, Object paramObject, e parame, boolean paramBoolean)
  {
    super(paramw, paramImageView, paramaa, paramInt1, paramInt2, paramInt3, paramDrawable, paramString, paramObject, paramBoolean);
    m = parame;
  }
  
  public final void a()
  {
    Object localObject = (ImageView)c.get();
    if (localObject == null) {
      return;
    }
    int i = g;
    if (i != 0)
    {
      i = g;
      ((ImageView)localObject).setImageResource(i);
    }
    else
    {
      Drawable localDrawable = h;
      if (localDrawable != null)
      {
        localDrawable = h;
        ((ImageView)localObject).setImageDrawable(localDrawable);
      }
    }
    localObject = m;
    if (localObject != null) {
      ((e)localObject).onError();
    }
  }
  
  public final void a(Bitmap paramBitmap, w.d paramd)
  {
    if (paramBitmap != null)
    {
      Object localObject1 = c.get();
      Object localObject2 = localObject1;
      localObject2 = (ImageView)localObject1;
      if (localObject2 == null) {
        return;
      }
      Context localContext = a.e;
      localObject1 = a;
      boolean bool1 = m;
      boolean bool2 = d;
      x.a((ImageView)localObject2, localContext, paramBitmap, paramd, bool2, bool1);
      paramBitmap = m;
      if (paramBitmap != null) {
        paramBitmap.onSuccess();
      }
      return;
    }
    paramBitmap = new java/lang/AssertionError;
    paramd = new Object[1];
    paramd[0] = this;
    paramd = String.format("Attempted to complete action with no result!\n%s", paramd);
    paramBitmap.<init>(paramd);
    throw paramBitmap;
  }
  
  final void b()
  {
    super.b();
    e locale = m;
    if (locale != null)
    {
      locale = null;
      m = null;
    }
  }
}

/* Location:
 * Qualified Name:     com.d.b.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */