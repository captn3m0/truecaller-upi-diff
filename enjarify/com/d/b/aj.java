package com.d.b;

import android.content.ContentUris;
import android.content.UriMatcher;
import android.net.Uri;
import android.provider.ContactsContract.Contacts;
import android.text.TextUtils;
import com.truecaller.common.h.am;
import com.truecaller.content.TruecallerContract;
import java.util.Iterator;
import java.util.List;

public final class aj
  extends ac
{
  private u a;
  private f b;
  private final UriMatcher c;
  
  public aj()
  {
    UriMatcher localUriMatcher = new android/content/UriMatcher;
    localUriMatcher.<init>(-1);
    c = localUriMatcher;
    localUriMatcher = c;
    String str = TruecallerContract.a;
    localUriMatcher.addURI(str, "photo", 0);
  }
  
  public final ac.a a(aa paramaa, int paramInt)
  {
    Object localObject1 = d;
    Object localObject2 = "pbid";
    localObject1 = ((Uri)localObject1).getQueryParameter((String)localObject2);
    long l1 = am.h((String)localObject1);
    long l2 = 0L;
    boolean bool1 = l1 < l2;
    if (bool1)
    {
      Uri localUri = ContactsContract.Contacts.CONTENT_URI;
      localObject1 = ContentUris.withAppendedId(localUri, l1);
      localObject1 = paramaa.f().a((Uri)localObject1).c();
      localObject2 = b;
      bool2 = ((f)localObject2).a((aa)localObject1);
      if (bool2)
      {
        localObject2 = b;
        localObject1 = ((f)localObject2).a((aa)localObject1, paramInt);
        if (localObject1 != null) {
          return (ac.a)localObject1;
        }
      }
    }
    localObject1 = d;
    localObject2 = "tcphoto";
    localObject1 = ((Uri)localObject1).getQueryParameter((String)localObject2);
    boolean bool2 = TextUtils.isEmpty((CharSequence)localObject1);
    if (!bool2)
    {
      paramaa = paramaa.f();
      localObject1 = Uri.parse((String)localObject1);
      paramaa = paramaa.a((Uri)localObject1).c();
      localObject1 = a;
      boolean bool3 = ((u)localObject1).a(paramaa);
      if (bool3)
      {
        localObject1 = a;
        paramaa = ((u)localObject1).a(paramaa, paramInt);
        if (paramaa != null) {
          return paramaa;
        }
      }
    }
    return null;
  }
  
  public final void a(w paramw)
  {
    paramw = d.iterator();
    for (;;)
    {
      boolean bool1 = paramw.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject = (ac)paramw.next();
      boolean bool2 = localObject instanceof u;
      if (bool2)
      {
        localObject = (u)localObject;
        a = ((u)localObject);
      }
      else
      {
        bool2 = localObject instanceof f;
        if (bool2)
        {
          localObject = (f)localObject;
          b = ((f)localObject);
        }
      }
    }
  }
  
  public final boolean a(aa paramaa)
  {
    UriMatcher localUriMatcher = c;
    paramaa = d;
    int i = localUriMatcher.match(paramaa);
    int j = -1;
    return i != j;
  }
}

/* Location:
 * Qualified Name:     com.d.b.aj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */