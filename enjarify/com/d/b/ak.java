package com.d.b;

import android.content.Context;
import android.net.Uri;
import android.net.http.HttpResponseCache;
import android.os.Build.VERSION;
import com.google.firebase.perf.network.FirebasePerfUrlConnection;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public final class ak
  implements j
{
  static volatile Object a;
  private static final Object b;
  private static final ThreadLocal c;
  private final Context d;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    b = localObject;
    localObject = new com/d/b/ak$1;
    ((ak.1)localObject).<init>();
    c = (ThreadLocal)localObject;
  }
  
  public ak(Context paramContext)
  {
    paramContext = paramContext.getApplicationContext();
    d = paramContext;
  }
  
  public final j.a a(Uri paramUri, int paramInt)
  {
    int i = Build.VERSION.SDK_INT;
    int k = 14;
    Object localObject1;
    if (i >= k)
    {
      localObject1 = d;
      ??? = a;
      if (??? != null) {}
    }
    try
    {
      synchronized (b)
      {
        localObject5 = a;
        if (localObject5 == null)
        {
          localObject1 = al.b((Context)localObject1);
          localObject5 = HttpResponseCache.getInstalled();
          if (localObject5 == null)
          {
            long l1 = al.a((File)localObject1);
            localObject5 = HttpResponseCache.install((File)localObject1, l1);
          }
          a = localObject5;
        }
      }
    }
    catch (IOException localIOException)
    {
      Object localObject5;
      Object localObject3;
      boolean bool1;
      boolean bool2;
      int m;
      int j;
      int n;
      long l2;
      for (;;) {}
    }
    localObject3 = new java/net/URL;
    paramUri = paramUri.toString();
    ((URL)localObject3).<init>(paramUri);
    paramUri = (HttpURLConnection)FirebasePerfUrlConnection.instrument(((URL)localObject3).openConnection());
    paramUri.setConnectTimeout(15000);
    paramUri.setReadTimeout(20000);
    i = 1;
    paramUri.setUseCaches(i);
    if (paramInt != 0)
    {
      bool1 = t.c(paramInt);
      if (bool1)
      {
        localObject3 = "only-if-cached,max-age=2147483647";
      }
      else
      {
        localObject3 = (StringBuilder)c.get();
        ??? = null;
        ((StringBuilder)localObject3).setLength(0);
        bool2 = t.a(paramInt);
        if (!bool2)
        {
          ??? = "no-cache";
          ((StringBuilder)localObject3).append((String)???);
        }
        bool2 = t.b(paramInt);
        if (!bool2)
        {
          m = ((StringBuilder)localObject3).length();
          if (m > 0)
          {
            m = 44;
            ((StringBuilder)localObject3).append(m);
          }
          ??? = "no-store";
          ((StringBuilder)localObject3).append((String)???);
        }
        localObject3 = ((StringBuilder)localObject3).toString();
      }
      ??? = "Cache-Control";
      paramUri.setRequestProperty((String)???, (String)localObject3);
    }
    j = paramUri.getResponseCode();
    n = 300;
    if (j < n)
    {
      l2 = paramUri.getHeaderFieldInt("Content-Length", -1);
      paramInt = al.a(paramUri.getHeaderField("X-Android-Response-Source"));
      localObject5 = new com/d/b/j$a;
      paramUri = paramUri.getInputStream();
      ((j.a)localObject5).<init>(paramUri, paramInt, l2);
      return (j.a)localObject5;
    }
    paramUri.disconnect();
    ??? = new com/d/b/j$b;
    localObject5 = new java/lang/StringBuilder;
    ((StringBuilder)localObject5).<init>();
    ((StringBuilder)localObject5).append(j);
    ((StringBuilder)localObject5).append(" ");
    paramUri = paramUri.getResponseMessage();
    ((StringBuilder)localObject5).append(paramUri);
    paramUri = ((StringBuilder)localObject5).toString();
    ((j.b)???).<init>(paramUri, paramInt, j);
    throw ((Throwable)???);
  }
  
  public final void a()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 14;
    if (i >= j)
    {
      Object localObject = a;
      if (localObject != null)
      {
        localObject = a;
        try
        {
          localObject = (HttpResponseCache)localObject;
          ((HttpResponseCache)localObject).close();
          return;
        }
        catch (IOException localIOException) {}
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.d.b.ak
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */