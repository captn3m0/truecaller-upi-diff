package com.d.b;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.provider.MediaStore.Images.Thumbnails;
import android.provider.MediaStore.Video.Thumbnails;
import java.io.InputStream;

final class r
  extends g
{
  private static final String[] b = { "orientation" };
  
  r(Context paramContext)
  {
    super(paramContext);
  }
  
  /* Error */
  private static int a(ContentResolver paramContentResolver, Uri paramUri)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: getstatic 12	com/d/b/r:b	[Ljava/lang/String;
    //   5: astore_3
    //   6: aload_0
    //   7: aload_1
    //   8: aload_3
    //   9: aconst_null
    //   10: aconst_null
    //   11: aconst_null
    //   12: invokevirtual 22	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   15: astore_2
    //   16: aload_2
    //   17: ifnull +41 -> 58
    //   20: aload_2
    //   21: invokeinterface 28 1 0
    //   26: istore 4
    //   28: iload 4
    //   30: ifne +6 -> 36
    //   33: goto +25 -> 58
    //   36: aload_2
    //   37: iconst_0
    //   38: invokeinterface 32 2 0
    //   43: istore 4
    //   45: aload_2
    //   46: ifnull +9 -> 55
    //   49: aload_2
    //   50: invokeinterface 36 1 0
    //   55: iload 4
    //   57: ireturn
    //   58: aload_2
    //   59: ifnull +9 -> 68
    //   62: aload_2
    //   63: invokeinterface 36 1 0
    //   68: iconst_0
    //   69: ireturn
    //   70: astore_0
    //   71: aload_2
    //   72: ifnull +9 -> 81
    //   75: aload_2
    //   76: invokeinterface 36 1 0
    //   81: aload_0
    //   82: athrow
    //   83: pop
    //   84: aload_2
    //   85: ifnull +9 -> 94
    //   88: aload_2
    //   89: invokeinterface 36 1 0
    //   94: iconst_0
    //   95: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	96	0	paramContentResolver	ContentResolver
    //   0	96	1	paramUri	Uri
    //   1	88	2	localCursor	android.database.Cursor
    //   5	4	3	arrayOfString	String[]
    //   26	3	4	bool	boolean
    //   43	13	4	i	int
    //   83	1	6	localRuntimeException	RuntimeException
    // Exception table:
    //   from	to	target	type
    //   2	5	70	finally
    //   11	15	70	finally
    //   20	26	70	finally
    //   37	43	70	finally
    //   2	5	83	java/lang/RuntimeException
    //   11	15	83	java/lang/RuntimeException
    //   20	26	83	java/lang/RuntimeException
    //   37	43	83	java/lang/RuntimeException
  }
  
  public final ac.a a(aa paramaa, int paramInt)
  {
    aa localaa = paramaa;
    ContentResolver localContentResolver = a.getContentResolver();
    Object localObject1 = d;
    int i = a(localContentResolver, (Uri)localObject1);
    localObject1 = d;
    localObject1 = localContentResolver.getType((Uri)localObject1);
    boolean bool1 = true;
    if (localObject1 != null)
    {
      localObject2 = "video/";
      bool2 = ((String)localObject1).startsWith((String)localObject2);
      if (bool2)
      {
        k = 1;
        break label82;
      }
    }
    boolean bool2 = false;
    localObject1 = null;
    int k = 0;
    label82:
    bool2 = paramaa.c();
    if (bool2)
    {
      int j = h;
      int m = i;
      localObject3 = r.a.a;
      int n = e;
      if (j <= n)
      {
        localObject3 = r.a.a;
        n = f;
        if (m <= n)
        {
          localObject1 = r.a.a;
          localObject4 = localObject1;
          break label214;
        }
      }
      localObject3 = r.a.b;
      n = e;
      if (j <= n)
      {
        localObject1 = r.a.b;
        j = f;
        if (m <= j)
        {
          localObject1 = r.a.b;
          localObject4 = localObject1;
          break label214;
        }
      }
      localObject1 = r.a.c;
      Object localObject4 = localObject1;
      label214:
      if (k == 0)
      {
        localObject1 = r.a.c;
        if (localObject4 == localObject1)
        {
          localObject1 = new com/d/b/ac$a;
          localObject2 = b(paramaa);
          localObject3 = w.d.b;
          ((ac.a)localObject1).<init>(null, (InputStream)localObject2, (w.d)localObject3, i);
          return (ac.a)localObject1;
        }
      }
      localObject1 = d;
      long l = ContentUris.parseId((Uri)localObject1);
      Object localObject5 = c(paramaa);
      inJustDecodeBounds = bool1;
      j = h;
      m = i;
      n = e;
      int i1 = f;
      Object localObject6 = localObject5;
      Object localObject7 = localObject5;
      localObject5 = paramaa;
      a(j, m, n, i1, (BitmapFactory.Options)localObject6, paramaa);
      if (k != 0)
      {
        localObject1 = r.a.c;
        if (localObject4 == localObject1) {
          j = 1;
        } else {
          j = d;
        }
        localObject1 = MediaStore.Video.Thumbnails.getThumbnail(localContentResolver, l, j, (BitmapFactory.Options)localObject7);
      }
      else
      {
        j = d;
        localObject1 = MediaStore.Images.Thumbnails.getThumbnail(localContentResolver, l, j, (BitmapFactory.Options)localObject6);
      }
      if (localObject1 != null)
      {
        localObject2 = new com/d/b/ac$a;
        localObject3 = w.d.b;
        ((ac.a)localObject2).<init>((Bitmap)localObject1, null, (w.d)localObject3, i);
        return (ac.a)localObject2;
      }
    }
    localObject1 = new com/d/b/ac$a;
    Object localObject2 = b(paramaa);
    Object localObject3 = w.d.b;
    ((ac.a)localObject1).<init>(null, (InputStream)localObject2, (w.d)localObject3, i);
    return (ac.a)localObject1;
  }
  
  public final boolean a(aa paramaa)
  {
    paramaa = d;
    String str1 = "content";
    String str2 = paramaa.getScheme();
    boolean bool1 = str1.equals(str2);
    if (bool1)
    {
      str1 = "media";
      paramaa = paramaa.getAuthority();
      boolean bool2 = str1.equals(paramaa);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.d.b.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */