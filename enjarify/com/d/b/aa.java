package com.d.b;

import android.graphics.Bitmap.Config;
import android.net.Uri;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public final class aa
{
  private static final long s = TimeUnit.SECONDS.toNanos(5);
  int a;
  long b;
  int c;
  public final Uri d;
  public final int e;
  public final String f;
  public final List g;
  public final int h;
  public final int i;
  public final boolean j;
  public final boolean k;
  public final boolean l;
  public final float m;
  public final float n;
  public final float o;
  public final boolean p;
  public final Bitmap.Config q;
  public final w.e r;
  
  private aa(Uri paramUri, int paramInt1, String paramString, List paramList, int paramInt2, int paramInt3, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, float paramFloat1, float paramFloat2, float paramFloat3, boolean paramBoolean4, Bitmap.Config paramConfig, w.e parame)
  {
    d = paramUri;
    e = paramInt1;
    f = paramString;
    if (paramList == null)
    {
      paramUri = null;
      g = null;
    }
    else
    {
      paramUri = Collections.unmodifiableList(paramList);
      g = paramUri;
    }
    h = paramInt2;
    i = paramInt3;
    j = paramBoolean1;
    k = paramBoolean2;
    l = paramBoolean3;
    m = paramFloat1;
    n = paramFloat2;
    o = paramFloat3;
    p = paramBoolean4;
    q = paramConfig;
    r = parame;
  }
  
  final String a()
  {
    long l1 = System.nanoTime();
    long l2 = b;
    l1 -= l2;
    l2 = s;
    char c1 = '+';
    boolean bool = l1 < l2;
    if (bool)
    {
      localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      str = b();
      localStringBuilder.append(str);
      localStringBuilder.append(c1);
      l1 = TimeUnit.NANOSECONDS.toSeconds(l1);
      localStringBuilder.append(l1);
      localStringBuilder.append('s');
      return localStringBuilder.toString();
    }
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str = b();
    localStringBuilder.append(str);
    localStringBuilder.append(c1);
    l1 = TimeUnit.NANOSECONDS.toMillis(l1);
    localStringBuilder.append(l1);
    localStringBuilder.append("ms");
    return localStringBuilder.toString();
  }
  
  public final String b()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("[R");
    int i1 = a;
    localStringBuilder.append(i1);
    localStringBuilder.append(']');
    return localStringBuilder.toString();
  }
  
  public final boolean c()
  {
    int i1 = h;
    if (i1 == 0)
    {
      i1 = i;
      if (i1 == 0) {
        return false;
      }
    }
    return true;
  }
  
  final boolean d()
  {
    boolean bool = c();
    if (!bool)
    {
      float f1 = m;
      bool = f1 < 0.0F;
      if (!bool) {
        return false;
      }
    }
    return true;
  }
  
  final boolean e()
  {
    List localList = g;
    return localList != null;
  }
  
  public final aa.a f()
  {
    aa.a locala = new com/d/b/aa$a;
    locala.<init>(this, (byte)0);
    return locala;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    Object localObject1 = "Request{";
    localStringBuilder.<init>((String)localObject1);
    int i1 = e;
    if (i1 > 0)
    {
      localStringBuilder.append(i1);
    }
    else
    {
      localObject1 = d;
      localStringBuilder.append(localObject1);
    }
    localObject1 = g;
    char c1 = ' ';
    if (localObject1 != null)
    {
      boolean bool1 = ((List)localObject1).isEmpty();
      if (!bool1)
      {
        localObject1 = g.iterator();
        for (;;)
        {
          boolean bool3 = ((Iterator)localObject1).hasNext();
          if (!bool3) {
            break;
          }
          Object localObject2 = (ai)((Iterator)localObject1).next();
          localStringBuilder.append(c1);
          localObject2 = ((ai)localObject2).a();
          localStringBuilder.append((String)localObject2);
        }
      }
    }
    localObject1 = f;
    char c2 = ')';
    if (localObject1 != null)
    {
      localStringBuilder.append(" stableKey(");
      localObject1 = f;
      localStringBuilder.append((String)localObject1);
      localStringBuilder.append(c2);
    }
    int i2 = h;
    char c3 = ',';
    if (i2 > 0)
    {
      localObject1 = " resize(";
      localStringBuilder.append((String)localObject1);
      i2 = h;
      localStringBuilder.append(i2);
      localStringBuilder.append(c3);
      i2 = i;
      localStringBuilder.append(i2);
      localStringBuilder.append(c2);
    }
    boolean bool2 = j;
    if (bool2)
    {
      localObject1 = " centerCrop";
      localStringBuilder.append((String)localObject1);
    }
    bool2 = k;
    if (bool2)
    {
      localObject1 = " centerInside";
      localStringBuilder.append((String)localObject1);
    }
    float f1 = m;
    bool2 = f1 < 0.0F;
    if (bool2)
    {
      localObject1 = " rotation(";
      localStringBuilder.append((String)localObject1);
      f1 = m;
      localStringBuilder.append(f1);
      bool2 = p;
      if (bool2)
      {
        localObject1 = " @ ";
        localStringBuilder.append((String)localObject1);
        f1 = n;
        localStringBuilder.append(f1);
        localStringBuilder.append(c3);
        f1 = o;
        localStringBuilder.append(f1);
      }
      localStringBuilder.append(c2);
    }
    localObject1 = q;
    if (localObject1 != null)
    {
      localStringBuilder.append(c1);
      localObject1 = q;
      localStringBuilder.append(localObject1);
    }
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.d.b.aa
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */