package com.d.b;

import android.content.ContentResolver;
import android.content.Context;
import android.content.UriMatcher;
import android.net.Uri;
import android.os.Build.VERSION;
import android.provider.ContactsContract.Contacts;

public final class f
  extends ac
{
  private static final UriMatcher a;
  private final Context b;
  
  static
  {
    UriMatcher localUriMatcher = new android/content/UriMatcher;
    localUriMatcher.<init>(-1);
    a = localUriMatcher;
    int i = 1;
    localUriMatcher.addURI("com.android.contacts", "contacts/lookup/*/#", i);
    a.addURI("com.android.contacts", "contacts/lookup/*", i);
    a.addURI("com.android.contacts", "contacts/#/photo", 2);
    a.addURI("com.android.contacts", "contacts/#", 3);
    a.addURI("com.android.contacts", "display_photo/#", 4);
  }
  
  f(Context paramContext)
  {
    b = paramContext;
  }
  
  public final ac.a a(aa paramaa, int paramInt)
  {
    Object localObject1 = b.getContentResolver();
    paramaa = d;
    Object localObject2 = a;
    int i = ((UriMatcher)localObject2).match(paramaa);
    switch (i)
    {
    default: 
      localObject1 = new java/lang/IllegalStateException;
      paramaa = String.valueOf(paramaa);
      paramaa = "Invalid uri: ".concat(paramaa);
      ((IllegalStateException)localObject1).<init>(paramaa);
      throw ((Throwable)localObject1);
    case 2: 
    case 4: 
      paramaa = ((ContentResolver)localObject1).openInputStream(paramaa);
      break;
    case 1: 
      paramaa = ContactsContract.Contacts.lookupContact((ContentResolver)localObject1, paramaa);
      if (paramaa == null) {
        paramaa = null;
      }
      break;
    }
    i = Build.VERSION.SDK_INT;
    int j = 14;
    if (i < j)
    {
      paramaa = ContactsContract.Contacts.openContactPhotoInputStream((ContentResolver)localObject1, paramaa);
    }
    else
    {
      i = 1;
      paramaa = ContactsContract.Contacts.openContactPhotoInputStream((ContentResolver)localObject1, paramaa, i);
    }
    if (paramaa != null)
    {
      localObject1 = new com/d/b/ac$a;
      localObject2 = w.d.b;
      ((ac.a)localObject1).<init>(paramaa, (w.d)localObject2);
      return (ac.a)localObject1;
    }
    return null;
  }
  
  public final boolean a(aa paramaa)
  {
    Object localObject = d;
    String str1 = "content";
    String str2 = ((Uri)localObject).getScheme();
    boolean bool1 = str1.equals(str2);
    if (bool1)
    {
      str1 = ContactsContract.Contacts.CONTENT_URI.getHost();
      localObject = ((Uri)localObject).getHost();
      boolean bool2 = str1.equals(localObject);
      if (bool2)
      {
        localObject = a;
        paramaa = d;
        int j = ((UriMatcher)localObject).match(paramaa);
        int i = -1;
        if (j != i) {
          return true;
        }
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.d.b.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */