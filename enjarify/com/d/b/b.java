package com.d.b;

import android.content.Context;
import android.content.res.AssetManager;
import android.net.Uri;
import java.util.List;

final class b
  extends ac
{
  private static final int a = 22;
  private final AssetManager b;
  
  public b(Context paramContext)
  {
    paramContext = paramContext.getAssets();
    b = paramContext;
  }
  
  public final ac.a a(aa paramaa, int paramInt)
  {
    Object localObject = b;
    paramaa = d.toString();
    int i = a;
    paramaa = paramaa.substring(i);
    paramaa = ((AssetManager)localObject).open(paramaa);
    localObject = new com/d/b/ac$a;
    w.d locald = w.d.b;
    ((ac.a)localObject).<init>(paramaa, locald);
    return (ac.a)localObject;
  }
  
  public final boolean a(aa paramaa)
  {
    paramaa = d;
    Object localObject = "file";
    String str = paramaa.getScheme();
    boolean bool1 = ((String)localObject).equals(str);
    str = null;
    if (bool1)
    {
      localObject = paramaa.getPathSegments();
      bool1 = ((List)localObject).isEmpty();
      if (!bool1)
      {
        localObject = "android_asset";
        paramaa = paramaa.getPathSegments().get(0);
        boolean bool2 = ((String)localObject).equals(paramaa);
        if (bool2) {
          return true;
        }
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.d.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */