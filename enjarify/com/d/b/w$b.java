package com.d.b;

import android.os.Handler;
import android.os.Message;
import android.os.Process;
import java.lang.ref.ReferenceQueue;

public final class w$b
  extends Thread
{
  private final ReferenceQueue a;
  private final Handler b;
  
  w$b(ReferenceQueue paramReferenceQueue, Handler paramHandler)
  {
    a = paramReferenceQueue;
    b = paramHandler;
    setDaemon(true);
    setName("Picasso-refQueue");
  }
  
  public final void run()
  {
    int i = 10;
    Process.setThreadPriority(i);
    try
    {
      Object localObject2;
      for (;;)
      {
        Object localObject1 = a;
        long l = 1000L;
        localObject1 = ((ReferenceQueue)localObject1).remove(l);
        localObject1 = (a.a)localObject1;
        localObject2 = b;
        localObject2 = ((Handler)localObject2).obtainMessage();
        if (localObject1 != null)
        {
          int j = 3;
          what = j;
          localObject1 = a;
          obj = localObject1;
          localObject1 = b;
          ((Handler)localObject1).sendMessage((Message)localObject2);
        }
        else
        {
          ((Message)localObject2).recycle();
        }
      }
      w.b.1 local1;
      return;
    }
    catch (Exception localException)
    {
      localObject2 = b;
      local1 = new com/d/b/w$b$1;
      local1.<init>(this, localException);
      ((Handler)localObject2).post(local1);
      return;
    }
    catch (InterruptedException localInterruptedException) {}
  }
}

/* Location:
 * Qualified Name:     com.d.b.w.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */