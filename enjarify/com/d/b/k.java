package com.d.b;

import android.graphics.Bitmap;

public final class k
  extends a
{
  private final Object m;
  private e n;
  
  public k(w paramw, aa paramaa, int paramInt1, int paramInt2, Object paramObject, String paramString, e parame)
  {
    super(paramw, null, paramaa, paramInt1, paramInt2, 0, null, paramString, paramObject, false);
    localObject = new java/lang/Object;
    localObject.<init>();
    m = localObject;
    localObject = parame;
    n = parame;
  }
  
  final void a()
  {
    e locale = n;
    if (locale != null) {
      locale.onError();
    }
  }
  
  final void a(Bitmap paramBitmap, w.d paramd)
  {
    paramBitmap = n;
    if (paramBitmap != null) {
      paramBitmap.onSuccess();
    }
  }
  
  final void b()
  {
    super.b();
    n = null;
  }
  
  final Object c()
  {
    return m;
  }
}

/* Location:
 * Qualified Name:     com.d.b.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */