package com.d.b;

import android.graphics.Bitmap;
import java.io.InputStream;

public final class ac$a
{
  final w.d a;
  final Bitmap b;
  final InputStream c;
  final int d;
  
  public ac$a(Bitmap paramBitmap, w.d paramd)
  {
    this(paramBitmap, null, paramd, 0);
  }
  
  ac$a(Bitmap paramBitmap, InputStream paramInputStream, w.d paramd, int paramInt)
  {
    int i = 1;
    int j;
    if (paramBitmap != null) {
      j = 1;
    } else {
      j = 0;
    }
    if (paramInputStream == null) {
      i = 0;
    }
    i ^= j;
    if (i != 0)
    {
      b = paramBitmap;
      c = paramInputStream;
      paramBitmap = (w.d)al.a(paramd, "loadedFrom == null");
      a = paramBitmap;
      d = paramInt;
      return;
    }
    paramBitmap = new java/lang/AssertionError;
    paramBitmap.<init>();
    throw paramBitmap;
  }
  
  public ac$a(InputStream paramInputStream, w.d paramd)
  {
    this(null, paramInputStream, paramd, 0);
  }
}

/* Location:
 * Qualified Name:     com.d.b.ac.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */