package com.d.b;

import android.content.Context;
import android.media.ExifInterface;
import android.net.Uri;
import java.io.InputStream;

final class l
  extends g
{
  l(Context paramContext)
  {
    super(paramContext);
  }
  
  public final ac.a a(aa paramaa, int paramInt)
  {
    ac.a locala = new com/d/b/ac$a;
    InputStream localInputStream = b(paramaa);
    w.d locald = w.d.b;
    paramaa = d;
    ExifInterface localExifInterface = new android/media/ExifInterface;
    paramaa = paramaa.getPath();
    localExifInterface.<init>(paramaa);
    paramaa = "Orientation";
    int i = 1;
    int j = localExifInterface.getAttributeInt(paramaa, i);
    int k = 3;
    if (j != k)
    {
      k = 6;
      if (j != k)
      {
        k = 8;
        if (j != k)
        {
          j = 0;
          paramaa = null;
        }
        else
        {
          j = 270;
        }
      }
      else
      {
        j = 90;
      }
    }
    else
    {
      j = 180;
    }
    locala.<init>(null, localInputStream, locald, j);
    return locala;
  }
  
  public final boolean a(aa paramaa)
  {
    paramaa = d.getScheme();
    return "file".equals(paramaa);
  }
}

/* Location:
 * Qualified Name:     com.d.b.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */