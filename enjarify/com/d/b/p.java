package com.d.b;

import android.content.Context;
import android.graphics.Bitmap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;

public final class p
  implements d
{
  final LinkedHashMap b;
  private final int c;
  private int d;
  private int e;
  private int f;
  private int g;
  private int h;
  
  public p(int paramInt)
  {
    if (paramInt > 0)
    {
      c = paramInt;
      localObject = new java/util/LinkedHashMap;
      ((LinkedHashMap)localObject).<init>(0, 0.75F, true);
      b = ((LinkedHashMap)localObject);
      return;
    }
    Object localObject = new java/lang/IllegalArgumentException;
    ((IllegalArgumentException)localObject).<init>("Max size must be positive.");
    throw ((Throwable)localObject);
  }
  
  public p(Context paramContext)
  {
    this(i);
  }
  
  public final int a()
  {
    try
    {
      int i = d;
      return i;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final Bitmap a(String paramString)
  {
    if (paramString != null) {
      try
      {
        LinkedHashMap localLinkedHashMap = b;
        paramString = localLinkedHashMap.get(paramString);
        paramString = (Bitmap)paramString;
        if (paramString != null)
        {
          int i = g + 1;
          g = i;
          return paramString;
        }
        int j = h + 1;
        h = j;
        return null;
      }
      finally {}
    }
    paramString = new java/lang/NullPointerException;
    paramString.<init>("key == null");
    throw paramString;
  }
  
  public final void a(int paramInt)
  {
    for (;;)
    {
      try
      {
        int i = d;
        if (i >= 0)
        {
          localObject1 = b;
          boolean bool1 = ((LinkedHashMap)localObject1).isEmpty();
          int j;
          if (bool1)
          {
            j = d;
            if (j != 0) {}
          }
          else
          {
            j = d;
            if (j > paramInt)
            {
              localObject1 = b;
              boolean bool2 = ((LinkedHashMap)localObject1).isEmpty();
              if (!bool2)
              {
                localObject1 = b;
                localObject1 = ((LinkedHashMap)localObject1).entrySet();
                localObject1 = ((Set)localObject1).iterator();
                localObject1 = ((Iterator)localObject1).next();
                localObject1 = (Map.Entry)localObject1;
                localObject2 = ((Map.Entry)localObject1).getKey();
                localObject2 = (String)localObject2;
                localObject1 = ((Map.Entry)localObject1).getValue();
                localObject1 = (Bitmap)localObject1;
                LinkedHashMap localLinkedHashMap = b;
                localLinkedHashMap.remove(localObject2);
                int m = d;
                int k = al.a((Bitmap)localObject1);
                m -= k;
                d = m;
                k = f + 1;
                f = k;
                continue;
              }
            }
            return;
          }
        }
        IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
        Object localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        Object localObject2 = getClass();
        localObject2 = ((Class)localObject2).getName();
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject2 = ".sizeOf() is reporting inconsistent results!";
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject1 = ((StringBuilder)localObject1).toString();
        localIllegalStateException.<init>((String)localObject1);
        throw localIllegalStateException;
      }
      finally {}
    }
  }
  
  public final void a(String paramString, Bitmap paramBitmap)
  {
    if ((paramString != null) && (paramBitmap != null)) {
      try
      {
        int i = e + 1;
        e = i;
        i = d;
        int j = al.a(paramBitmap);
        i += j;
        d = i;
        LinkedHashMap localLinkedHashMap = b;
        paramString = localLinkedHashMap.put(paramString, paramBitmap);
        paramString = (Bitmap)paramString;
        if (paramString != null)
        {
          int k = d;
          m = al.a(paramString);
          k -= m;
          d = k;
        }
        int m = c;
        a(m);
        return;
      }
      finally {}
    }
    paramString = new java/lang/NullPointerException;
    paramString.<init>("key == null || bitmap == null");
    throw paramString;
  }
  
  public final int b()
  {
    try
    {
      int i = c;
      return i;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final void b(String paramString)
  {
    try
    {
      int i = paramString.length();
      Object localObject1 = b;
      localObject1 = ((LinkedHashMap)localObject1).entrySet();
      localObject1 = ((Set)localObject1).iterator();
      int j = 0;
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        Object localObject2 = ((Iterator)localObject1).next();
        localObject2 = (Map.Entry)localObject2;
        Object localObject3 = ((Map.Entry)localObject2).getKey();
        localObject3 = (String)localObject3;
        localObject2 = ((Map.Entry)localObject2).getValue();
        localObject2 = (Bitmap)localObject2;
        int m = 10;
        m = ((String)localObject3).indexOf(m);
        if (m == i)
        {
          localObject3 = ((String)localObject3).substring(0, m);
          boolean bool2 = ((String)localObject3).equals(paramString);
          if (bool2)
          {
            ((Iterator)localObject1).remove();
            j = d;
            int k = al.a((Bitmap)localObject2);
            j -= k;
            d = j;
            j = 1;
          }
        }
      }
      if (j != 0)
      {
        int n = c;
        a(n);
      }
      return;
    }
    finally {}
  }
  
  public final void c()
  {
    int i = -1;
    try
    {
      a(i);
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.d.b.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */