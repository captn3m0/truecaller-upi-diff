package com.d.b;

import android.graphics.Bitmap;

public abstract interface d
{
  public static final d a;
  
  static
  {
    d.1 local1 = new com/d/b/d$1;
    local1.<init>();
    a = local1;
  }
  
  public abstract int a();
  
  public abstract Bitmap a(String paramString);
  
  public abstract void a(String paramString, Bitmap paramBitmap);
  
  public abstract int b();
  
  public abstract void b(String paramString);
  
  public abstract void c();
}

/* Location:
 * Qualified Name:     com.d.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */