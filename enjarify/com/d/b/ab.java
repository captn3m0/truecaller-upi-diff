package com.d.b;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public final class ab
{
  private static final AtomicInteger g;
  public final w a;
  public final aa.a b;
  public boolean c;
  public int d;
  public int e;
  public Object f;
  private boolean h;
  private boolean i = true;
  private int j;
  private int k;
  private Drawable l;
  private Drawable m;
  
  static
  {
    AtomicInteger localAtomicInteger = new java/util/concurrent/atomic/AtomicInteger;
    localAtomicInteger.<init>();
    g = localAtomicInteger;
  }
  
  ab()
  {
    a = null;
    aa.a locala = new com/d/b/aa$a;
    locala.<init>(null, 0, null);
    b = locala;
  }
  
  ab(w paramw, Uri paramUri, int paramInt)
  {
    boolean bool = o;
    if (!bool)
    {
      a = paramw;
      aa.a locala = new com/d/b/aa$a;
      paramw = l;
      locala.<init>(paramUri, paramInt, paramw);
      b = locala;
      return;
    }
    paramw = new java/lang/IllegalStateException;
    paramw.<init>("Picasso instance already shut down. Cannot submit new requests.");
    throw paramw;
  }
  
  private Drawable e()
  {
    int n = j;
    if (n != 0)
    {
      Resources localResources = a.e.getResources();
      int i1 = j;
      return localResources.getDrawable(i1);
    }
    return l;
  }
  
  public final aa a(long paramLong)
  {
    Object localObject1 = g;
    int n = ((AtomicInteger)localObject1).getAndIncrement();
    Object localObject2 = b.c();
    a = n;
    b = paramLong;
    Object localObject3 = a;
    boolean bool = n;
    if (bool)
    {
      localObject4 = "Main";
      String str1 = "created";
      String str2 = ((aa)localObject2).b();
      String str3 = ((aa)localObject2).toString();
      al.a((String)localObject4, str1, str2, str3);
    }
    Object localObject4 = a.a((aa)localObject2);
    if (localObject4 != localObject2)
    {
      a = n;
      b = paramLong;
      if (bool)
      {
        String str4 = "Main";
        String str5 = "changed";
        localObject1 = ((aa)localObject4).a();
        localObject3 = String.valueOf(localObject4);
        localObject2 = "into ".concat((String)localObject3);
        al.a(str4, str5, (String)localObject1, (String)localObject2);
      }
    }
    return (aa)localObject4;
  }
  
  public final ab a()
  {
    int n = j;
    if (n == 0)
    {
      localObject = l;
      if (localObject == null)
      {
        i = false;
        return this;
      }
      localObject = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject).<init>("Placeholder image already set.");
      throw ((Throwable)localObject);
    }
    Object localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("Placeholder resource already set.");
    throw ((Throwable)localObject);
  }
  
  public final ab a(int paramInt)
  {
    boolean bool = i;
    if (bool)
    {
      if (paramInt != 0)
      {
        Drawable localDrawable = l;
        if (localDrawable == null)
        {
          j = paramInt;
          return this;
        }
        localObject = new java/lang/IllegalStateException;
        ((IllegalStateException)localObject).<init>("Placeholder image already set.");
        throw ((Throwable)localObject);
      }
      localObject = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject).<init>("Placeholder image resource invalid.");
      throw ((Throwable)localObject);
    }
    Object localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("Already explicitly declared as no placeholder.");
    throw ((Throwable)localObject);
  }
  
  public final ab a(int paramInt1, int paramInt2)
  {
    Resources localResources = a.e.getResources();
    paramInt1 = localResources.getDimensionPixelSize(paramInt1);
    paramInt2 = localResources.getDimensionPixelSize(paramInt2);
    return b(paramInt1, paramInt2);
  }
  
  public final ab a(Drawable paramDrawable)
  {
    boolean bool = i;
    if (bool)
    {
      int n = j;
      if (n == 0)
      {
        l = paramDrawable;
        return this;
      }
      paramDrawable = new java/lang/IllegalStateException;
      paramDrawable.<init>("Placeholder image already set.");
      throw paramDrawable;
    }
    paramDrawable = new java/lang/IllegalStateException;
    paramDrawable.<init>("Already explicitly declared as no placeholder.");
    throw paramDrawable;
  }
  
  public final ab a(ai paramai)
  {
    aa.a locala = b;
    if (paramai != null)
    {
      Object localObject = paramai.a();
      if (localObject != null)
      {
        localObject = f;
        if (localObject == null)
        {
          localObject = new java/util/ArrayList;
          int n = 2;
          ((ArrayList)localObject).<init>(n);
          f = ((List)localObject);
        }
        f.add(paramai);
        return this;
      }
      paramai = new java/lang/IllegalArgumentException;
      paramai.<init>("Transformation key must not be null.");
      throw paramai;
    }
    paramai = new java/lang/IllegalArgumentException;
    paramai.<init>("Transformation must not be null.");
    throw paramai;
  }
  
  public final ab a(s paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      int n = d;
      int i1 = c | n;
      d = i1;
      return this;
    }
    paramVarArgs = new java/lang/IllegalArgumentException;
    paramVarArgs.<init>("Memory policy cannot be null.");
    throw paramVarArgs;
  }
  
  public final ab a(t paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      int n = e;
      int i1 = d | n;
      e = i1;
      return this;
    }
    paramVarArgs = new java/lang/IllegalArgumentException;
    paramVarArgs.<init>("Network policy cannot be null.");
    throw paramVarArgs;
  }
  
  public final ab a(Object paramObject)
  {
    if (paramObject != null)
    {
      Object localObject = f;
      if (localObject == null)
      {
        f = paramObject;
        return this;
      }
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("Tag already set.");
      throw ((Throwable)paramObject);
    }
    paramObject = new java/lang/IllegalArgumentException;
    ((IllegalArgumentException)paramObject).<init>("Tag invalid.");
    throw ((Throwable)paramObject);
  }
  
  public final void a(ImageView paramImageView, e parame)
  {
    ab localab = this;
    Object localObject1 = paramImageView;
    e locale = parame;
    long l1 = System.nanoTime();
    al.b();
    if (paramImageView != null)
    {
      Object localObject2 = b;
      boolean bool1 = ((aa.a)localObject2).a();
      if (!bool1)
      {
        localObject3 = a;
        ((w)localObject3).d(paramImageView);
        bool2 = i;
        if (bool2)
        {
          localObject3 = e();
          x.a(paramImageView, (Drawable)localObject3);
        }
        return;
      }
      bool1 = c;
      int i1;
      if (bool1)
      {
        localObject2 = b;
        bool1 = ((aa.a)localObject2).b();
        if (!bool1)
        {
          int n = paramImageView.getWidth();
          i1 = paramImageView.getHeight();
          if ((n != 0) && (i1 != 0))
          {
            aa.a locala = b;
            locala.a(n, i1);
          }
          else
          {
            bool2 = i;
            if (bool2)
            {
              localObject3 = e();
              x.a((ImageView)localObject1, (Drawable)localObject3);
            }
            localObject3 = a;
            localObject4 = new com/d/b/h;
            ((h)localObject4).<init>(this, (ImageView)localObject1, locale);
            ((w)localObject3).a((ImageView)localObject1, (h)localObject4);
          }
        }
        else
        {
          localObject3 = new java/lang/IllegalStateException;
          ((IllegalStateException)localObject3).<init>("Fit cannot be used with resize.");
          throw ((Throwable)localObject3);
        }
      }
      aa localaa = a(l1);
      String str = al.a(localaa);
      boolean bool2 = s.a(d);
      if (bool2)
      {
        localObject3 = a;
        localObject2 = ((w)localObject3).b(str);
        if (localObject2 != null)
        {
          a.d(localObject1);
          localObject4 = a.e;
          w.d locald = w.d.a;
          int i3 = h;
          int i5 = a.m;
          localObject3 = paramImageView;
          localObject1 = localObject2;
          localObject2 = locald;
          i1 = i3;
          i3 = i5;
          x.a(paramImageView, (Context)localObject4, (Bitmap)localObject1, locald, i1, i5);
          localObject3 = a;
          bool2 = n;
          if (bool2)
          {
            localObject3 = "Main";
            localObject4 = "completed";
            localObject1 = localaa.b();
            localObject2 = new java/lang/StringBuilder;
            ((StringBuilder)localObject2).<init>("from ");
            locald = w.d.a;
            ((StringBuilder)localObject2).append(locald);
            localObject2 = ((StringBuilder)localObject2).toString();
            al.a((String)localObject3, (String)localObject4, (String)localObject1, (String)localObject2);
          }
          if (locale != null) {
            parame.onSuccess();
          }
          return;
        }
      }
      bool2 = i;
      if (bool2)
      {
        localObject3 = e();
        x.a((ImageView)localObject1, (Drawable)localObject3);
      }
      o localo = new com/d/b/o;
      Object localObject4 = a;
      int i2 = d;
      int i4 = e;
      int i6 = k;
      Object localObject5 = m;
      Object localObject6 = f;
      boolean bool3 = h;
      localObject3 = localo;
      localObject1 = paramImageView;
      localObject2 = localaa;
      Object localObject7 = localObject5;
      localObject5 = localObject6;
      locale = parame;
      localo.<init>((w)localObject4, paramImageView, localaa, i2, i4, i6, (Drawable)localObject7, str, localObject6, parame, bool3);
      a.a(localo);
      return;
    }
    Object localObject3 = new java/lang/IllegalArgumentException;
    ((IllegalArgumentException)localObject3).<init>("Target must not be null.");
    throw ((Throwable)localObject3);
  }
  
  public final void a(ag paramag)
  {
    long l1 = System.nanoTime();
    al.b();
    if (paramag != null)
    {
      boolean bool1 = c;
      if (!bool1)
      {
        aa.a locala = b;
        bool1 = locala.a();
        Drawable localDrawable1 = null;
        if (!bool1)
        {
          localObject1 = a;
          ((w)localObject1).d(paramag);
          bool2 = i;
          if (bool2) {
            localDrawable1 = e();
          }
          paramag.b(localDrawable1);
          return;
        }
        aa localaa = a(l1);
        String str = al.a(localaa);
        boolean bool2 = s.a(d);
        if (bool2)
        {
          localObject1 = a.b(str);
          if (localObject1 != null)
          {
            a.d(paramag);
            paramag.a((Bitmap)localObject1);
            return;
          }
        }
        bool2 = i;
        if (bool2) {
          localDrawable1 = e();
        }
        paramag.b(localDrawable1);
        Object localObject1 = new com/d/b/ah;
        w localw = a;
        int n = d;
        int i1 = e;
        Drawable localDrawable2 = m;
        Object localObject2 = f;
        int i2 = k;
        ((ah)localObject1).<init>(localw, paramag, localaa, n, i1, localDrawable2, str, localObject2, i2);
        a.a((a)localObject1);
        return;
      }
      paramag = new java/lang/IllegalStateException;
      paramag.<init>("Fit cannot be used with a Target.");
      throw paramag;
    }
    paramag = new java/lang/IllegalArgumentException;
    paramag.<init>("Target must not be null.");
    throw paramag;
  }
  
  public final ab b()
  {
    Object localObject = b;
    boolean bool = d;
    if (!bool)
    {
      c = true;
      return this;
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("Center crop can not be used after calling centerInside");
    throw ((Throwable)localObject);
  }
  
  public final ab b(int paramInt)
  {
    if (paramInt != 0)
    {
      Drawable localDrawable = m;
      if (localDrawable == null)
      {
        k = paramInt;
        return this;
      }
      localObject = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject).<init>("Error image already set.");
      throw ((Throwable)localObject);
    }
    Object localObject = new java/lang/IllegalArgumentException;
    ((IllegalArgumentException)localObject).<init>("Error image resource invalid.");
    throw ((Throwable)localObject);
  }
  
  public final ab b(int paramInt1, int paramInt2)
  {
    b.a(paramInt1, paramInt2);
    return this;
  }
  
  public final ab b(Drawable paramDrawable)
  {
    if (paramDrawable != null)
    {
      int n = k;
      if (n == 0)
      {
        m = paramDrawable;
        return this;
      }
      paramDrawable = new java/lang/IllegalStateException;
      paramDrawable.<init>("Error image already set.");
      throw paramDrawable;
    }
    paramDrawable = new java/lang/IllegalArgumentException;
    paramDrawable.<init>("Error image may not be null.");
    throw paramDrawable;
  }
  
  public final ab c()
  {
    Object localObject = b;
    boolean bool = c;
    if (!bool)
    {
      d = true;
      return this;
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("Center inside can not be used after calling centerCrop");
    throw ((Throwable)localObject);
  }
  
  public final Bitmap d()
  {
    long l1 = System.nanoTime();
    al.a();
    boolean bool = c;
    if (!bool)
    {
      Object localObject1 = b;
      bool = ((aa.a)localObject1).a();
      if (!bool) {
        return null;
      }
      Object localObject2 = a(l1);
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      String str = al.a((aa)localObject2, (StringBuilder)localObject3);
      localObject3 = new com/d/b/n;
      localObject1 = a;
      int n = d;
      int i1 = e;
      Object localObject4 = f;
      Object localObject5 = localObject3;
      ((n)localObject3).<init>((w)localObject1, (aa)localObject2, n, i1, localObject4, str);
      localObject5 = a;
      localObject1 = f;
      localObject2 = a.g;
      ae localae = a.h;
      return c.a((w)localObject5, (i)localObject1, (d)localObject2, localae, (a)localObject3).a();
    }
    Object localObject3 = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject3).<init>("Fit cannot be used with get.");
    throw ((Throwable)localObject3);
  }
}

/* Location:
 * Qualified Name:     com.d.b.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */