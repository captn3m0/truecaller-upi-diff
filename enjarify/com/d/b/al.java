package com.d.b;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.os.StatFs;
import android.provider.Settings.System;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public final class al
{
  static final StringBuilder a;
  
  static
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    a = localStringBuilder;
  }
  
  static int a(Resources paramResources, aa paramaa)
  {
    int i = e;
    if (i == 0)
    {
      Object localObject1 = d;
      if (localObject1 != null)
      {
        localObject1 = d.getAuthority();
        if (localObject1 != null)
        {
          Object localObject2 = d.getPathSegments();
          if (localObject2 != null)
          {
            boolean bool = ((List)localObject2).isEmpty();
            if (!bool)
            {
              int j = ((List)localObject2).size();
              int k = 1;
              int m;
              if (j == k)
              {
                try
                {
                  paramResources = ((List)localObject2).get(0);
                  paramResources = (String)paramResources;
                  m = Integer.parseInt(paramResources);
                }
                catch (NumberFormatException localNumberFormatException)
                {
                  paramResources = new java/io/FileNotFoundException;
                  localObject1 = new java/lang/StringBuilder;
                  ((StringBuilder)localObject1).<init>("Last path segment is not a resource ID: ");
                  paramaa = d;
                  ((StringBuilder)localObject1).append(paramaa);
                  paramaa = ((StringBuilder)localObject1).toString();
                  paramResources.<init>(paramaa);
                  throw paramResources;
                }
              }
              else
              {
                j = ((List)localObject2).size();
                int n = 2;
                if (j != n) {
                  break label199;
                }
                paramaa = (String)((List)localObject2).get(0);
                localObject2 = (String)((List)localObject2).get(k);
                m = paramResources.getIdentifier((String)localObject2, paramaa, (String)localObject1);
              }
              return m;
              label199:
              paramResources = new java/io/FileNotFoundException;
              localObject1 = new java/lang/StringBuilder;
              ((StringBuilder)localObject1).<init>("More than two path segments: ");
              paramaa = d;
              ((StringBuilder)localObject1).append(paramaa);
              paramaa = ((StringBuilder)localObject1).toString();
              paramResources.<init>(paramaa);
              throw paramResources;
            }
          }
          paramResources = new java/io/FileNotFoundException;
          localObject1 = new java/lang/StringBuilder;
          ((StringBuilder)localObject1).<init>("No path segments: ");
          paramaa = d;
          ((StringBuilder)localObject1).append(paramaa);
          paramaa = ((StringBuilder)localObject1).toString();
          paramResources.<init>(paramaa);
          throw paramResources;
        }
        paramResources = new java/io/FileNotFoundException;
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>("No package provided: ");
        paramaa = d;
        ((StringBuilder)localObject1).append(paramaa);
        paramaa = ((StringBuilder)localObject1).toString();
        paramResources.<init>(paramaa);
        throw paramResources;
      }
    }
    return e;
  }
  
  static int a(Bitmap paramBitmap)
  {
    int i = Build.VERSION.SDK_INT;
    int j = 12;
    if (i >= j)
    {
      i = paramBitmap.getByteCount();
    }
    else
    {
      i = paramBitmap.getRowBytes();
      j = paramBitmap.getHeight();
      i *= j;
    }
    if (i >= 0) {
      return i;
    }
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    paramBitmap = String.valueOf(paramBitmap);
    paramBitmap = "Negative size: ".concat(paramBitmap);
    localIllegalStateException.<init>(paramBitmap);
    throw localIllegalStateException;
  }
  
  static long a(File paramFile)
  {
    long l1 = 5242880L;
    long l4;
    try
    {
      StatFs localStatFs = new android/os/StatFs;
      paramFile = paramFile.getAbsolutePath();
      localStatFs.<init>(paramFile);
      int i = localStatFs.getBlockCount();
      long l2 = i;
      i = localStatFs.getBlockSize();
      long l3 = i;
      l2 *= l3;
      l3 = 50;
      l4 = l2 / l3;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      l4 = l1;
    }
    return Math.max(Math.min(l4, 52428800L), l1);
  }
  
  static Resources a(Context paramContext, aa paramaa)
  {
    int i = e;
    if (i == 0)
    {
      Object localObject = d;
      if (localObject != null)
      {
        localObject = d.getAuthority();
        if (localObject != null) {
          try
          {
            paramContext = paramContext.getPackageManager();
            return paramContext.getResourcesForApplication((String)localObject);
          }
          catch (PackageManager.NameNotFoundException localNameNotFoundException)
          {
            paramContext = new java/io/FileNotFoundException;
            localObject = new java/lang/StringBuilder;
            ((StringBuilder)localObject).<init>("Unable to obtain resources for package: ");
            paramaa = d;
            ((StringBuilder)localObject).append(paramaa);
            paramaa = ((StringBuilder)localObject).toString();
            paramContext.<init>(paramaa);
            throw paramContext;
          }
        }
        paramContext = new java/io/FileNotFoundException;
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>("No package provided: ");
        paramaa = d;
        ((StringBuilder)localObject).append(paramaa);
        paramaa = ((StringBuilder)localObject).toString();
        paramContext.<init>(paramaa);
        throw paramContext;
      }
    }
    return paramContext.getResources();
  }
  
  static j a(Context paramContext)
  {
    Object localObject = "com.d.a.t";
    try
    {
      Class.forName((String)localObject);
      localObject = new com/d/b/v;
      ((v)localObject).<init>(paramContext);
      return (j)localObject;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      localObject = new com/d/b/ak;
      ((ak)localObject).<init>(paramContext);
    }
    return (j)localObject;
  }
  
  static Object a(Context paramContext, String paramString)
  {
    return paramContext.getSystemService(paramString);
  }
  
  static Object a(Object paramObject, String paramString)
  {
    if (paramObject != null) {
      return paramObject;
    }
    paramObject = new java/lang/NullPointerException;
    ((NullPointerException)paramObject).<init>(paramString);
    throw ((Throwable)paramObject);
  }
  
  static String a(aa paramaa)
  {
    StringBuilder localStringBuilder = a;
    paramaa = a(paramaa, localStringBuilder);
    a.setLength(0);
    return paramaa;
  }
  
  public static String a(aa paramaa, StringBuilder paramStringBuilder)
  {
    Object localObject1 = paramaa.f;
    int i = 50;
    float f = 7.0E-44F;
    if (localObject1 != null)
    {
      n = paramaa.f.length() + i;
      paramStringBuilder.ensureCapacity(n);
      localObject1 = paramaa.f;
      paramStringBuilder.append((String)localObject1);
    }
    else
    {
      localObject1 = d;
      if (localObject1 != null)
      {
        localObject1 = d.toString();
        i1 = ((String)localObject1).length() + i;
        paramStringBuilder.ensureCapacity(i1);
        paramStringBuilder.append((String)localObject1);
      }
      else
      {
        paramStringBuilder.ensureCapacity(i);
        n = e;
        paramStringBuilder.append(n);
      }
    }
    int n = 10;
    paramStringBuilder.append(n);
    f = paramaa.m;
    int i1 = 0;
    List localList = null;
    char c2 = 'x';
    boolean bool1 = f < 0.0F;
    if (bool1)
    {
      localObject2 = "rotation:";
      paramStringBuilder.append((String)localObject2);
      f = paramaa.m;
      paramStringBuilder.append(f);
      bool1 = p;
      if (bool1)
      {
        char c1 = '@';
        paramStringBuilder.append(c1);
        f = paramaa.n;
        paramStringBuilder.append(f);
        paramStringBuilder.append(c2);
        f = o;
        paramStringBuilder.append(f);
      }
      paramStringBuilder.append(n);
    }
    boolean bool2 = paramaa.c();
    if (bool2)
    {
      localObject2 = "resize:";
      paramStringBuilder.append((String)localObject2);
      int j = h;
      paramStringBuilder.append(j);
      paramStringBuilder.append(c2);
      j = paramaa.i;
      paramStringBuilder.append(j);
      paramStringBuilder.append(n);
    }
    int k = paramaa.j;
    if (k != 0)
    {
      localObject2 = "centerCrop\n";
      paramStringBuilder.append((String)localObject2);
    }
    else
    {
      k = paramaa.k;
      if (k != 0)
      {
        localObject2 = "centerInside\n";
        paramStringBuilder.append((String)localObject2);
      }
    }
    Object localObject2 = g;
    if (localObject2 != null)
    {
      k = 0;
      f = 0.0F;
      localObject2 = null;
      localList = g;
      i1 = localList.size();
      while (k < i1)
      {
        String str = ((ai)g.get(k)).a();
        paramStringBuilder.append(str);
        paramStringBuilder.append(n);
        int m;
        k += 1;
      }
    }
    return paramStringBuilder.toString();
  }
  
  static String a(c paramc)
  {
    return a(paramc, "");
  }
  
  static String a(c paramc, String paramString)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(paramString);
    paramString = k;
    String str1;
    if (paramString != null)
    {
      str1 = b.a();
      localStringBuilder.append(str1);
    }
    paramc = l;
    if (paramc != null)
    {
      int i = 0;
      str1 = null;
      int j = paramc.size();
      while (i < j)
      {
        if ((i > 0) || (paramString != null))
        {
          str2 = ", ";
          localStringBuilder.append(str2);
        }
        String str2 = getb.a();
        localStringBuilder.append(str2);
        i += 1;
      }
    }
    return localStringBuilder.toString();
  }
  
  static void a()
  {
    boolean bool = c();
    if (!bool) {
      return;
    }
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>("Method call should not happen from the main thread.");
    throw localIllegalStateException;
  }
  
  static void a(Looper paramLooper)
  {
    al.1 local1 = new com/d/b/al$1;
    local1.<init>(paramLooper);
    paramLooper = local1.obtainMessage();
    local1.sendMessageDelayed(paramLooper, 1000L);
  }
  
  static void a(InputStream paramInputStream)
  {
    if (paramInputStream == null) {
      return;
    }
    try
    {
      paramInputStream.close();
      return;
    }
    catch (IOException localIOException) {}
  }
  
  static void a(String paramString1, String paramString2, String paramString3)
  {
    a(paramString1, paramString2, paramString3, "");
  }
  
  public static void a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    Object[] arrayOfObject = new Object[4];
    arrayOfObject[0] = paramString1;
    arrayOfObject[1] = paramString2;
    arrayOfObject[2] = paramString3;
    arrayOfObject[3] = paramString4;
    String.format("%1$-11s %2$-12s %3$s %4$s", arrayOfObject);
  }
  
  static boolean a(String paramString)
  {
    if (paramString == null) {
      return false;
    }
    paramString = paramString.split(" ", 2);
    String str = "CACHE";
    Object localObject1 = paramString[0];
    boolean bool1 = str.equals(localObject1);
    int k = 1;
    if (bool1) {
      return k;
    }
    int i = paramString.length;
    if (i == k) {
      return false;
    }
    str = "CONDITIONAL_CACHE";
    try
    {
      Object localObject2 = paramString[0];
      boolean bool2 = str.equals(localObject2);
      if (bool2)
      {
        paramString = paramString[k];
        int m = Integer.parseInt(paramString);
        int j = 304;
        if (m == j) {
          return k;
        }
      }
      return false;
    }
    catch (NumberFormatException localNumberFormatException) {}
    return false;
  }
  
  static File b(Context paramContext)
  {
    File localFile = new java/io/File;
    paramContext = paramContext.getApplicationContext().getCacheDir();
    String str = "picasso-cache";
    localFile.<init>(paramContext, str);
    boolean bool = localFile.exists();
    if (!bool) {
      localFile.mkdirs();
    }
    return localFile;
  }
  
  static void b()
  {
    boolean bool = c();
    if (bool) {
      return;
    }
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>("Method call should happen from the main thread.");
    throw localIllegalStateException;
  }
  
  static boolean b(Context paramContext, String paramString)
  {
    int i = paramContext.checkCallingOrSelfPermission(paramString);
    return i == 0;
  }
  
  static byte[] b(InputStream paramInputStream)
  {
    ByteArrayOutputStream localByteArrayOutputStream = new java/io/ByteArrayOutputStream;
    localByteArrayOutputStream.<init>();
    int i = 4096;
    byte[] arrayOfByte = new byte[i];
    for (;;)
    {
      int j = -1;
      int k = paramInputStream.read(arrayOfByte);
      if (j == k) {
        break;
      }
      j = 0;
      localByteArrayOutputStream.write(arrayOfByte, 0, k);
    }
    return localByteArrayOutputStream.toByteArray();
  }
  
  static int c(Context paramContext)
  {
    ActivityManager localActivityManager = (ActivityManager)paramContext.getSystemService("activity");
    paramContext = paramContext.getApplicationInfo();
    int i = flags;
    int j = 1048576;
    i &= j;
    if (i != 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      paramContext = null;
    }
    int k = localActivityManager.getMemoryClass();
    if (i != 0)
    {
      i = Build.VERSION.SDK_INT;
      int m = 11;
      if (i >= m) {
        k = localActivityManager.getLargeMemoryClass();
      }
    }
    return k * j / 7;
  }
  
  private static boolean c()
  {
    Thread localThread1 = Looper.getMainLooper().getThread();
    Thread localThread2 = Thread.currentThread();
    return localThread1 == localThread2;
  }
  
  static boolean c(InputStream paramInputStream)
  {
    int i = 12;
    byte[] arrayOfByte = new byte[i];
    boolean bool1 = false;
    int j = paramInputStream.read(arrayOfByte, 0, i);
    if (j == i)
    {
      paramInputStream = "RIFF";
      String str1 = new java/lang/String;
      String str2 = "US-ASCII";
      int k = 4;
      str1.<init>(arrayOfByte, 0, k, str2);
      boolean bool2 = paramInputStream.equals(str1);
      if (bool2)
      {
        paramInputStream = "WEBP";
        str1 = new java/lang/String;
        int m = 8;
        String str3 = "US-ASCII";
        str1.<init>(arrayOfByte, m, k, str3);
        bool2 = paramInputStream.equals(str1);
        if (bool2)
        {
          bool2 = true;
          bool1 = true;
        }
      }
    }
    return bool1;
  }
  
  static boolean d(Context paramContext)
  {
    paramContext = paramContext.getContentResolver();
    String str = "airplane_mode_on";
    try
    {
      int i = Settings.System.getInt(paramContext, str, 0);
      return i != 0;
    }
    catch (NullPointerException localNullPointerException) {}
    return false;
  }
}

/* Location:
 * Qualified Name:     com.d.b.al
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */