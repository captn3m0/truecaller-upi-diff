package com.d.b;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class ae$a
  extends Handler
{
  private final ae a;
  
  public ae$a(Looper paramLooper, ae paramae)
  {
    super(paramLooper);
    a = paramae;
  }
  
  public final void handleMessage(Message paramMessage)
  {
    int i = what;
    long l1 = 1L;
    Object localObject;
    int k;
    switch (i)
    {
    default: 
      localObject = w.a;
      ae.a.1 local1 = new com/d/b/ae$a$1;
      local1.<init>(this, paramMessage);
      ((Handler)localObject).post(local1);
      return;
    case 4: 
      localObject = a;
      paramMessage = (Long)obj;
      int j = l + 1;
      l = j;
      l1 = f;
      l2 = paramMessage.longValue();
      l1 += l2;
      f = l1;
      k = l;
      l1 = f;
      l2 = k;
      l1 /= l2;
      i = l1;
      return;
    case 3: 
      localObject = a;
      l1 = arg1;
      k = n + 1;
      n = k;
      l2 = h + l1;
      h = l2;
      k = m;
      l1 = h;
      l2 = k;
      l1 /= l2;
      k = l1;
      return;
    case 2: 
      localObject = a;
      l1 = arg1;
      k = m + 1;
      m = k;
      l2 = g + l1;
      g = l2;
      k = m;
      l1 = g;
      l2 = k;
      l1 /= l2;
      j = l1;
      return;
    case 1: 
      paramMessage = a;
      l2 = e + l1;
      e = l2;
      return;
    }
    paramMessage = a;
    long l2 = d + l1;
    d = l2;
  }
}

/* Location:
 * Qualified Name:     com.d.b.ae.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */