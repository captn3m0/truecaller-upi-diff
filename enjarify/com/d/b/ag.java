package com.d.b;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

public abstract interface ag
{
  public abstract void a(Bitmap paramBitmap);
  
  public abstract void a(Drawable paramDrawable);
  
  public abstract void b(Drawable paramDrawable);
}

/* Location:
 * Qualified Name:     com.d.b.ag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */