package com.mopub.network;

import com.mopub.volley.Response.ErrorListener;

public abstract interface AdLoader$Listener
  extends Response.ErrorListener
{
  public abstract void onSuccess(AdResponse paramAdResponse);
}

/* Location:
 * Qualified Name:     com.mopub.network.AdLoader.Listener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */