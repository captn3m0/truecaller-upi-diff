package com.mopub.network;

public abstract interface MultiAdResponse$ServerOverrideListener
{
  public abstract void onForceExplicitNo(String paramString);
  
  public abstract void onForceGdprApplies();
  
  public abstract void onInvalidateConsent(String paramString);
  
  public abstract void onReacquireConsent(String paramString);
}

/* Location:
 * Qualified Name:     com.mopub.network.MultiAdResponse.ServerOverrideListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */