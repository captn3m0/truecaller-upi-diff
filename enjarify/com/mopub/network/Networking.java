package com.mopub.network;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.Looper;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.mopub.common.Preconditions;
import com.mopub.common.util.DeviceUtils;
import com.mopub.volley.Network;
import com.mopub.volley.RequestQueue;
import com.mopub.volley.toolbox.BaseHttpStack;
import com.mopub.volley.toolbox.BasicNetwork;
import com.mopub.volley.toolbox.DiskBasedCache;
import com.mopub.volley.toolbox.HurlStack.UrlRewriter;
import com.mopub.volley.toolbox.ImageLoader;
import java.io.File;
import javax.net.ssl.SSLSocketFactory;

public class Networking
{
  private static final String a = System.getProperty("http.agent");
  private static volatile MoPubRequestQueue b;
  private static volatile String c;
  private static volatile MaxWidthImageLoader d;
  private static boolean e = false;
  private static HurlStack.UrlRewriter f;
  
  public static void clearForTesting()
  {
    Class localClass = Networking.class;
    Object localObject1 = null;
    try
    {
      b = null;
      d = null;
      c = null;
      return;
    }
    finally {}
  }
  
  public static String getBaseUrlScheme()
  {
    boolean bool = shouldUseHttps();
    if (bool) {
      return "https";
    }
    return "http";
  }
  
  public static String getCachedUserAgent()
  {
    String str = c;
    if (str == null) {
      return a;
    }
    return str;
  }
  
  public static ImageLoader getImageLoader(Context paramContext)
  {
    Object localObject = d;
    if (localObject == null) {
      synchronized (Networking.class)
      {
        localObject = d;
        if (localObject == null)
        {
          localObject = getRequestQueue(paramContext);
          int i = DeviceUtils.memoryCacheSizeBytes(paramContext);
          Networking.1 local1 = new com/mopub/network/Networking$1;
          local1.<init>(i);
          MaxWidthImageLoader localMaxWidthImageLoader = new com/mopub/network/MaxWidthImageLoader;
          Networking.2 local2 = new com/mopub/network/Networking$2;
          local2.<init>(local1);
          localMaxWidthImageLoader.<init>((RequestQueue)localObject, paramContext, local2);
          d = localMaxWidthImageLoader;
          localObject = localMaxWidthImageLoader;
        }
      }
    }
    return (ImageLoader)localObject;
  }
  
  public static MoPubRequestQueue getRequestQueue()
  {
    return b;
  }
  
  public static MoPubRequestQueue getRequestQueue(Context paramContext)
  {
    Object localObject1 = b;
    if (localObject1 == null) {
      synchronized (Networking.class)
      {
        localObject1 = b;
        if (localObject1 == null)
        {
          int i = 10000;
          localObject1 = CustomSSLSocketFactory.getDefault(i);
          Object localObject2 = paramContext.getApplicationContext();
          localObject2 = getUserAgent((Context)localObject2);
          Object localObject3 = new com/mopub/network/RequestQueueHttpStack;
          HurlStack.UrlRewriter localUrlRewriter = getUrlRewriter(paramContext);
          ((RequestQueueHttpStack)localObject3).<init>((String)localObject2, localUrlRewriter, (SSLSocketFactory)localObject1);
          localObject1 = new com/mopub/volley/toolbox/BasicNetwork;
          ((BasicNetwork)localObject1).<init>((BaseHttpStack)localObject3);
          localObject2 = new java/io/File;
          localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>();
          paramContext = paramContext.getCacheDir();
          paramContext = paramContext.getPath();
          ((StringBuilder)localObject3).append(paramContext);
          paramContext = File.separator;
          ((StringBuilder)localObject3).append(paramContext);
          paramContext = "mopub-volley-cache";
          ((StringBuilder)localObject3).append(paramContext);
          paramContext = ((StringBuilder)localObject3).toString();
          ((File)localObject2).<init>(paramContext);
          paramContext = new com/mopub/volley/toolbox/DiskBasedCache;
          long l = 10485760L;
          l = DeviceUtils.diskCacheSizeBytes((File)localObject2, l);
          int j = (int)l;
          paramContext.<init>((File)localObject2, j);
          localObject2 = new com/mopub/network/MoPubRequestQueue;
          ((MoPubRequestQueue)localObject2).<init>(paramContext, (Network)localObject1);
          b = (MoPubRequestQueue)localObject2;
          ((MoPubRequestQueue)localObject2).start();
          localObject1 = localObject2;
        }
      }
    }
    return (MoPubRequestQueue)localObject1;
  }
  
  public static String getScheme()
  {
    return "https";
  }
  
  public static HurlStack.UrlRewriter getUrlRewriter(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext);
    paramContext = f;
    if (paramContext == null)
    {
      paramContext = new com/mopub/network/PlayServicesUrlRewriter;
      paramContext.<init>();
      f = paramContext;
    }
    return f;
  }
  
  public static String getUserAgent(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext);
    Object localObject = c;
    if (localObject == null) {
      synchronized (Networking.class)
      {
        localObject = c;
        if (localObject == null)
        {
          try
          {
            int i = Build.VERSION.SDK_INT;
            int j = 17;
            if (i >= j)
            {
              paramContext = WebSettings.getDefaultUserAgent(paramContext);
            }
            else
            {
              localObject = Looper.myLooper();
              Looper localLooper = Looper.getMainLooper();
              if (localObject == localLooper)
              {
                localObject = new android/webkit/WebView;
                ((WebView)localObject).<init>(paramContext);
                paramContext = ((WebView)localObject).getSettings();
                paramContext = paramContext.getUserAgentString();
              }
              else
              {
                paramContext = a;
              }
            }
          }
          catch (Exception localException)
          {
            paramContext = a;
          }
          c = paramContext;
          localObject = paramContext;
        }
      }
    }
    return (String)localObject;
  }
  
  public static void setImageLoaderForTesting(MaxWidthImageLoader paramMaxWidthImageLoader)
  {
    synchronized (Networking.class)
    {
      d = paramMaxWidthImageLoader;
      return;
    }
  }
  
  public static void setRequestQueueForTesting(MoPubRequestQueue paramMoPubRequestQueue)
  {
    synchronized (Networking.class)
    {
      b = paramMoPubRequestQueue;
      return;
    }
  }
  
  public static void setUserAgentForTesting(String paramString)
  {
    synchronized (Networking.class)
    {
      c = paramString;
      return;
    }
  }
  
  public static boolean shouldUseHttps()
  {
    return e;
  }
  
  public static void useHttps(boolean paramBoolean)
  {
    e = paramBoolean;
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.Networking
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */