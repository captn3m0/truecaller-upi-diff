package com.mopub.network;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import com.mopub.common.AdFormat;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.mobileads.MoPubError;
import com.mopub.volley.Request;
import com.mopub.volley.RequestQueue;
import java.lang.ref.WeakReference;
import java.util.List;

public class AdLoader
{
  protected MultiAdResponse a;
  protected AdResponse b;
  private final MultiAdRequest.Listener c;
  private final WeakReference d;
  private final AdLoader.Listener e;
  private MultiAdRequest f;
  private final Object g;
  private a h;
  private volatile boolean i;
  private volatile boolean j;
  private boolean k;
  private Handler l;
  
  public AdLoader(String paramString1, AdFormat paramAdFormat, String paramString2, Context paramContext, AdLoader.Listener paramListener)
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    g = localObject;
    b = null;
    Preconditions.checkArgument(TextUtils.isEmpty(paramString1) ^ true);
    Preconditions.checkNotNull(paramAdFormat);
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramListener);
    localObject = new java/lang/ref/WeakReference;
    ((WeakReference)localObject).<init>(paramContext);
    d = ((WeakReference)localObject);
    e = paramListener;
    paramListener = new android/os/Handler;
    paramListener.<init>();
    l = paramListener;
    paramListener = new com/mopub/network/AdLoader$1;
    paramListener.<init>(this);
    c = paramListener;
    i = false;
    j = false;
    paramListener = new com/mopub/network/MultiAdRequest;
    MultiAdRequest.Listener localListener = c;
    localObject = paramListener;
    paramListener.<init>(paramString1, paramAdFormat, paramString2, paramContext, localListener);
    f = paramListener;
  }
  
  private Request a(MultiAdRequest paramMultiAdRequest, Context paramContext)
  {
    Preconditions.checkNotNull(paramMultiAdRequest);
    if (paramContext == null) {
      return null;
    }
    i = true;
    paramContext = Networking.getRequestQueue(paramContext);
    f = paramMultiAdRequest;
    paramContext.add(paramMultiAdRequest);
    return paramMultiAdRequest;
  }
  
  public void creativeDownloadSuccess()
  {
    boolean bool = true;
    k = bool;
    Object localObject1 = h;
    if (localObject1 == null)
    {
      MoPubLog.e("Response analytics should not be null here");
      return;
    }
    localObject1 = (Context)d.get();
    if (localObject1 != null)
    {
      Object localObject2 = b;
      if (localObject2 != null)
      {
        localObject2 = h;
        Object localObject3 = null;
        ((a)localObject2).a((Context)localObject1, null);
        localObject2 = h;
        if (localObject1 != null)
        {
          localObject3 = a;
          if (localObject3 != null)
          {
            localObject3 = b.getAfterLoadSuccessUrls();
            String str = a.a.a(a.a.AD_LOADED);
            TrackingRequest.makeTrackingHttpRequest(((a)localObject2).a((List)localObject3, str), (Context)localObject1);
            return;
          }
        }
        return;
      }
    }
    MoPubLog.w("Cannot send 'x-after-load-url' analytics.");
  }
  
  public boolean hasMoreAds()
  {
    boolean bool1 = j;
    if (bool1) {
      return false;
    }
    bool1 = k;
    if (bool1) {
      return false;
    }
    Object localObject = a;
    if (localObject != null)
    {
      boolean bool2 = ((MultiAdResponse)localObject).hasNext();
      if (!bool2)
      {
        localObject = a;
        bool1 = TextUtils.isEmpty((CharSequence)localObject);
        if (bool1) {
          return false;
        }
      }
    }
    return true;
  }
  
  public boolean isFailed()
  {
    return j;
  }
  
  public boolean isRunning()
  {
    return i;
  }
  
  public Request loadNextAd(MoPubError paramMoPubError)
  {
    boolean bool1 = i;
    if (bool1) {
      return f;
    }
    bool1 = j;
    Object localObject1 = null;
    if (bool1)
    {
      paramMoPubError = l;
      ??? = new com/mopub/network/AdLoader$2;
      ((AdLoader.2)???).<init>(this);
      paramMoPubError.post((Runnable)???);
      return null;
    }
    synchronized (g)
    {
      Object localObject3 = a;
      if (localObject3 == null)
      {
        paramMoPubError = f;
        localObject1 = d;
        localObject1 = ((WeakReference)localObject1).get();
        localObject1 = (Context)localObject1;
        paramMoPubError = a(paramMoPubError, (Context)localObject1);
        return paramMoPubError;
      }
      Object localObject4;
      Object localObject5;
      if (paramMoPubError != null) {
        if (paramMoPubError == null)
        {
          paramMoPubError = "Must provide error code to report creative download error";
          MoPubLog.w(paramMoPubError);
        }
        else
        {
          localObject3 = d;
          localObject3 = ((WeakReference)localObject3).get();
          localObject3 = (Context)localObject3;
          if (localObject3 != null)
          {
            localObject4 = b;
            if (localObject4 != null)
            {
              localObject4 = h;
              if (localObject4 == null) {
                break label259;
              }
              localObject4 = h;
              ((a)localObject4).a((Context)localObject3, paramMoPubError);
              localObject4 = h;
              if (localObject3 == null) {
                break label259;
              }
              localObject5 = a;
              if (localObject5 == null) {
                break label259;
              }
              paramMoPubError = a.a(paramMoPubError);
              localObject5 = b;
              localObject5 = ((AdResponse)localObject5).getAfterLoadFailUrls();
              paramMoPubError = a.a.a(paramMoPubError);
              paramMoPubError = ((a)localObject4).a((List)localObject5, paramMoPubError);
              TrackingRequest.makeTrackingHttpRequest(paramMoPubError, (Context)localObject3);
              break label259;
            }
          }
          paramMoPubError = "Cannot send creative mFailed analytics.";
          MoPubLog.w(paramMoPubError);
        }
      }
      label259:
      paramMoPubError = a;
      boolean bool2 = paramMoPubError.hasNext();
      if (bool2)
      {
        paramMoPubError = a;
        paramMoPubError = paramMoPubError.next();
        localObject1 = l;
        localObject3 = new com/mopub/network/AdLoader$3;
        ((AdLoader.3)localObject3).<init>(this, paramMoPubError);
        ((Handler)localObject1).post((Runnable)localObject3);
        paramMoPubError = f;
        return paramMoPubError;
      }
      paramMoPubError = a;
      paramMoPubError = a;
      bool2 = TextUtils.isEmpty(paramMoPubError);
      if (!bool2)
      {
        paramMoPubError = new com/mopub/network/MultiAdRequest;
        localObject1 = a;
        localObject4 = ((MultiAdResponse)localObject1).getFailURL();
        localObject1 = f;
        localObject5 = a;
        localObject1 = f;
        String str = b;
        localObject1 = d;
        localObject1 = ((WeakReference)localObject1).get();
        Object localObject6 = localObject1;
        localObject6 = (Context)localObject1;
        MultiAdRequest.Listener localListener = c;
        localObject3 = paramMoPubError;
        paramMoPubError.<init>((String)localObject4, (AdFormat)localObject5, str, (Context)localObject6, localListener);
        f = paramMoPubError;
        paramMoPubError = f;
        localObject1 = d;
        localObject1 = ((WeakReference)localObject1).get();
        localObject1 = (Context)localObject1;
        paramMoPubError = a(paramMoPubError, (Context)localObject1);
        return paramMoPubError;
      }
      paramMoPubError = l;
      ??? = new com/mopub/network/AdLoader$4;
      ((AdLoader.4)???).<init>(this);
      paramMoPubError.post((Runnable)???);
      return null;
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.AdLoader
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */