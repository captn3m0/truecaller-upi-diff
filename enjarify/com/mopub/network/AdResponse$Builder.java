package com.mopub.network;

import com.mopub.common.MoPub.BrowserAgent;
import com.mopub.common.Preconditions;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.json.JSONObject;

public class AdResponse$Builder
{
  private MoPub.BrowserAgent A;
  private Map B;
  private String a;
  private String b;
  private String c;
  private String d;
  private String e;
  private String f;
  private String g;
  private String h;
  private Integer i;
  private boolean j;
  private String k;
  private List l;
  private String m;
  private String n;
  private List o;
  private List p;
  private List q;
  private String r;
  private Integer s;
  private Integer t;
  private Integer u;
  private Integer v;
  private String w;
  private String x;
  private JSONObject y;
  private String z;
  
  public AdResponse$Builder()
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    l = ((List)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    o = ((List)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    p = ((List)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    q = ((List)localObject);
    localObject = new java/util/TreeMap;
    ((TreeMap)localObject).<init>();
    B = ((Map)localObject);
  }
  
  public AdResponse build()
  {
    AdResponse localAdResponse = new com/mopub/network/AdResponse;
    localAdResponse.<init>(this, (byte)0);
    return localAdResponse;
  }
  
  public Builder setAdTimeoutDelayMilliseconds(Integer paramInteger)
  {
    u = paramInteger;
    return this;
  }
  
  public Builder setAdType(String paramString)
  {
    a = paramString;
    return this;
  }
  
  public Builder setAdUnitId(String paramString)
  {
    b = paramString;
    return this;
  }
  
  public Builder setAfterLoadFailUrls(List paramList)
  {
    Preconditions.checkNotNull(paramList);
    q = paramList;
    return this;
  }
  
  public Builder setAfterLoadSuccessUrls(List paramList)
  {
    Preconditions.checkNotNull(paramList);
    p = paramList;
    return this;
  }
  
  public Builder setAfterLoadUrls(List paramList)
  {
    Preconditions.checkNotNull(paramList);
    o = paramList;
    return this;
  }
  
  public Builder setBeforeLoadUrl(String paramString)
  {
    n = paramString;
    return this;
  }
  
  public Builder setBrowserAgent(MoPub.BrowserAgent paramBrowserAgent)
  {
    A = paramBrowserAgent;
    return this;
  }
  
  public Builder setClickTrackingUrl(String paramString)
  {
    k = paramString;
    return this;
  }
  
  public Builder setCustomEventClassName(String paramString)
  {
    z = paramString;
    return this;
  }
  
  public Builder setDimensions(Integer paramInteger1, Integer paramInteger2)
  {
    s = paramInteger1;
    t = paramInteger2;
    return this;
  }
  
  public Builder setDspCreativeId(String paramString)
  {
    w = paramString;
    return this;
  }
  
  public Builder setFailoverUrl(String paramString)
  {
    m = paramString;
    return this;
  }
  
  public Builder setFullAdType(String paramString)
  {
    c = paramString;
    return this;
  }
  
  public Builder setImpressionTrackingUrls(List paramList)
  {
    Preconditions.checkNotNull(paramList);
    l = paramList;
    return this;
  }
  
  public Builder setJsonBody(JSONObject paramJSONObject)
  {
    y = paramJSONObject;
    return this;
  }
  
  public Builder setNetworkType(String paramString)
  {
    d = paramString;
    return this;
  }
  
  public Builder setRefreshTimeMilliseconds(Integer paramInteger)
  {
    v = paramInteger;
    return this;
  }
  
  public Builder setRequestId(String paramString)
  {
    r = paramString;
    return this;
  }
  
  public Builder setResponseBody(String paramString)
  {
    x = paramString;
    return this;
  }
  
  public Builder setRewardedCurrencies(String paramString)
  {
    g = paramString;
    return this;
  }
  
  public Builder setRewardedDuration(Integer paramInteger)
  {
    i = paramInteger;
    return this;
  }
  
  public Builder setRewardedVideoCompletionUrl(String paramString)
  {
    h = paramString;
    return this;
  }
  
  public Builder setRewardedVideoCurrencyAmount(String paramString)
  {
    f = paramString;
    return this;
  }
  
  public Builder setRewardedVideoCurrencyName(String paramString)
  {
    e = paramString;
    return this;
  }
  
  public Builder setServerExtras(Map paramMap)
  {
    if (paramMap == null)
    {
      paramMap = new java/util/TreeMap;
      paramMap.<init>();
      B = paramMap;
    }
    else
    {
      TreeMap localTreeMap = new java/util/TreeMap;
      localTreeMap.<init>(paramMap);
      B = localTreeMap;
    }
    return this;
  }
  
  public Builder setShouldRewardOnClick(boolean paramBoolean)
  {
    j = paramBoolean;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.AdResponse.Builder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */