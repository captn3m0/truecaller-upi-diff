package com.mopub.network;

import android.content.Context;
import com.mopub.common.AdFormat;
import com.mopub.volley.NetworkResponse;
import com.mopub.volley.Response;
import com.mopub.volley.VolleyError;
import com.mopub.volley.toolbox.HttpHeaderParser;

public class MultiAdRequest
  extends MoPubRequest
{
  private final Context c;
  private int d;
  
  MultiAdRequest(String paramString1, AdFormat paramAdFormat, String paramString2, Context paramContext, MultiAdRequest.Listener paramListener) {}
  
  public void cancel()
  {
    super.cancel();
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (paramObject == this) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof MultiAdRequest;
    if (!bool2) {
      return false;
    }
    paramObject = (MultiAdRequest)paramObject;
    Object localObject1 = b;
    Object localObject2;
    int i;
    if (localObject1 != null)
    {
      localObject2 = b;
      if (localObject2 == null) {
        bool2 = true;
      } else {
        i = ((String)localObject1).compareTo((String)localObject2);
      }
    }
    else
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        i = -1;
      }
      else
      {
        i = 0;
        localObject1 = null;
      }
    }
    if (i == 0)
    {
      localObject1 = a;
      localObject2 = a;
      if (localObject1 == localObject2)
      {
        localObject1 = getUrl();
        paramObject = ((MultiAdRequest)paramObject).getUrl();
        int j = ((String)localObject1).compareTo((String)paramObject);
        if (j == 0) {
          return bool1;
        }
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    int i = d;
    if (i == 0)
    {
      String str1 = b;
      if (str1 == null) {
        i = 29;
      } else {
        i = str1.hashCode();
      }
      i *= 31;
      int j = a.hashCode();
      i = (i + j) * 31;
      String str2 = getOriginalUrl();
      j = str2.hashCode();
      i += j;
      d = i;
    }
    return d;
  }
  
  public final Response parseNetworkResponse(NetworkResponse paramNetworkResponse)
  {
    Object localObject1;
    try
    {
      localObject1 = new com/mopub/network/MultiAdResponse;
      localObject2 = c;
      AdFormat localAdFormat = a;
      String str = b;
      ((MultiAdResponse)localObject1).<init>((Context)localObject2, paramNetworkResponse, localAdFormat, str);
      paramNetworkResponse = HttpHeaderParser.parseCacheHeaders(paramNetworkResponse);
      return Response.success(localObject1, paramNetworkResponse);
    }
    catch (Exception paramNetworkResponse)
    {
      boolean bool = paramNetworkResponse instanceof MoPubNetworkError;
      if (bool) {
        return Response.error((MoPubNetworkError)paramNetworkResponse);
      }
      localObject1 = new com/mopub/network/MoPubNetworkError;
      Object localObject2 = MoPubNetworkError.Reason.UNSPECIFIED;
      ((MoPubNetworkError)localObject1).<init>(paramNetworkResponse, (MoPubNetworkError.Reason)localObject2);
    }
    return Response.error((VolleyError)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.MultiAdRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */