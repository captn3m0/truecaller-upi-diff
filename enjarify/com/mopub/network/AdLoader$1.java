package com.mopub.network;

import com.mopub.volley.VolleyError;

final class AdLoader$1
  implements MultiAdRequest.Listener
{
  AdLoader$1(AdLoader paramAdLoader) {}
  
  public final void onErrorResponse(VolleyError paramVolleyError)
  {
    AdLoader.a(a);
    AdLoader.b(a);
    AdLoader.a(a, paramVolleyError);
  }
  
  public final void onSuccessResponse(MultiAdResponse paramMultiAdResponse)
  {
    synchronized (AdLoader.c(a))
    {
      Object localObject2 = a;
      AdLoader.b((AdLoader)localObject2);
      localObject2 = a;
      a = paramMultiAdResponse;
      paramMultiAdResponse = a;
      paramMultiAdResponse = a;
      boolean bool = paramMultiAdResponse.hasNext();
      if (bool)
      {
        paramMultiAdResponse = a;
        localObject2 = a;
        localObject2 = a;
        localObject2 = ((MultiAdResponse)localObject2).next();
        AdLoader.a(paramMultiAdResponse, (AdResponse)localObject2);
      }
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.AdLoader.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */