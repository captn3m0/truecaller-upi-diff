package com.mopub.network;

import android.content.Context;
import com.mopub.common.AdFormat;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.ResponseHeader;
import com.mopub.volley.NetworkResponse;
import com.mopub.volley.toolbox.HttpHeaderParser;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MultiAdResponse
  implements Iterator
{
  private static MultiAdResponse.ServerOverrideListener c;
  String a;
  private final Iterator b;
  
  public MultiAdResponse(Context paramContext, NetworkResponse paramNetworkResponse, AdFormat paramAdFormat, String paramString)
  {
    Object localObject1 = a(paramNetworkResponse);
    Object localObject2 = new org/json/JSONObject;
    ((JSONObject)localObject2).<init>((String)localObject1);
    Object localObject4 = ResponseHeader.FAIL_URL.getKey();
    localObject4 = ((JSONObject)localObject2).optString((String)localObject4);
    a = ((String)localObject4);
    localObject4 = ResponseHeader.REQUEST_ID.getKey();
    localObject4 = ((JSONObject)localObject2).optString((String)localObject4);
    Object localObject5 = ResponseHeader.INVALIDATE_CONSENT;
    boolean bool1 = HeaderUtils.extractBooleanHeader((JSONObject)localObject2, (ResponseHeader)localObject5, false);
    Object localObject6 = ResponseHeader.FORCE_EXPLICIT_NO;
    boolean bool2 = HeaderUtils.extractBooleanHeader((JSONObject)localObject2, (ResponseHeader)localObject6, false);
    Object localObject7 = ResponseHeader.REACQUIRE_CONSENT;
    boolean bool3 = HeaderUtils.extractBooleanHeader((JSONObject)localObject2, (ResponseHeader)localObject7, false);
    Object localObject8 = ResponseHeader.CONSENT_CHANGE_REASON;
    localObject8 = HeaderUtils.extractHeader((JSONObject)localObject2, (ResponseHeader)localObject8);
    Object localObject9 = ResponseHeader.FORCE_GDPR_APPLIES;
    boolean bool4 = HeaderUtils.extractBooleanHeader((JSONObject)localObject2, (ResponseHeader)localObject9, false);
    Object localObject10 = c;
    if (localObject10 != null)
    {
      if (bool4) {
        ((MultiAdResponse.ServerOverrideListener)localObject10).onForceGdprApplies();
      }
      if (bool2)
      {
        localObject5 = c;
        ((MultiAdResponse.ServerOverrideListener)localObject5).onForceExplicitNo((String)localObject8);
      }
      else if (bool1)
      {
        localObject5 = c;
        ((MultiAdResponse.ServerOverrideListener)localObject5).onInvalidateConsent((String)localObject8);
      }
      else if (bool3)
      {
        localObject5 = c;
        ((MultiAdResponse.ServerOverrideListener)localObject5).onReacquireConsent((String)localObject8);
      }
    }
    localObject5 = ResponseHeader.AD_RESPONSES.getKey();
    JSONArray localJSONArray = ((JSONObject)localObject2).getJSONArray((String)localObject5);
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>(3);
    int i = 0;
    localObject2 = null;
    Object localObject11 = null;
    int k = 0;
    for (;;)
    {
      i = localJSONArray.length();
      if (k >= i) {
        break;
      }
      try
      {
        localObject2 = localJSONArray.getJSONObject(k);
        localObject5 = paramContext;
        localObject6 = paramNetworkResponse;
        localObject7 = localObject2;
        localObject8 = paramString;
        localObject9 = paramAdFormat;
        localObject10 = localObject4;
        localObject5 = a(paramContext, paramNetworkResponse, (JSONObject)localObject2, paramString, paramAdFormat, (String)localObject4);
        localObject6 = "clear";
        localObject7 = ((AdResponse)localObject5).getAdType();
        bool2 = ((String)localObject6).equals(localObject7);
        if (!bool2)
        {
          localArrayList.add(localObject5);
          break label633;
        }
        localObject6 = "";
        a = ((String)localObject6);
        try
        {
          Preconditions.checkNotNull(localObject2);
          localObject6 = ResponseHeader.METADATA;
          localObject6 = ((ResponseHeader)localObject6).getKey();
          localObject2 = ((JSONObject)localObject2).optJSONObject((String)localObject6);
          localObject6 = ResponseHeader.WARMUP;
          bool5 = HeaderUtils.extractBooleanHeader((JSONObject)localObject2, (ResponseHeader)localObject6, false);
          if (!bool5)
          {
            localObject11 = localObject5;
            break;
          }
          localObject2 = new com/mopub/network/MoPubNetworkError;
          localObject6 = "Server is preparing this Ad Unit.";
          localObject7 = MoPubNetworkError.Reason.WARMING_UP;
          localObject8 = ((AdResponse)localObject5).getRefreshTimeMillis();
          ((MoPubNetworkError)localObject2).<init>((String)localObject6, (MoPubNetworkError.Reason)localObject7, (Integer)localObject8);
          throw ((Throwable)localObject2);
        }
        catch (Exception localException1) {}catch (MoPubNetworkError localMoPubNetworkError1) {}catch (JSONException localJSONException1)
        {
          localObject11 = localObject5;
        }
        String str;
        localObject5 = String.valueOf(localObject1);
      }
      catch (Exception localException2)
      {
        localObject5 = localObject11;
        localObject6 = new java/lang/StringBuilder;
        localObject7 = "Unexpected error parsing response item. ";
        ((StringBuilder)localObject6).<init>((String)localObject7);
        str = localException2.getMessage();
        ((StringBuilder)localObject6).append(str);
        str = ((StringBuilder)localObject6).toString();
        MoPubLog.w(str);
        localObject11 = localObject5;
      }
      catch (MoPubNetworkError localMoPubNetworkError2)
      {
        localObject5 = localObject11;
        localObject6 = localMoPubNetworkError2.getReason();
        localObject7 = MoPubNetworkError.Reason.WARMING_UP;
        if (localObject6 != localObject7)
        {
          localObject6 = new java/lang/StringBuilder;
          localObject7 = "Invalid response item. Error: ";
          ((StringBuilder)localObject6).<init>((String)localObject7);
          localObject3 = localMoPubNetworkError2.getReason();
          ((StringBuilder)localObject6).append(localObject3);
          localObject3 = ((StringBuilder)localObject6).toString();
          MoPubLog.w((String)localObject3);
          localObject11 = localObject5;
          break label633;
        }
        throw ((Throwable)localObject3);
      }
      catch (JSONException localJSONException2) {}
      localObject3 = "Invalid response item. Body: ".concat((String)localObject5);
      MoPubLog.w((String)localObject3);
      label633:
      k += 1;
    }
    Object localObject3 = localArrayList.iterator();
    b = ((Iterator)localObject3);
    localObject3 = b;
    boolean bool5 = ((Iterator)localObject3).hasNext();
    if (!bool5)
    {
      int j = 30000;
      localObject3 = Integer.valueOf(j);
      if (localObject11 != null) {
        localObject3 = ((AdResponse)localObject11).getRefreshTimeMillis();
      }
      localObject1 = new com/mopub/network/MoPubNetworkError;
      localObject4 = MoPubNetworkError.Reason.NO_FILL;
      ((MoPubNetworkError)localObject1).<init>("No ads found for ad unit.", (MoPubNetworkError.Reason)localObject4, (Integer)localObject3);
      throw ((Throwable)localObject1);
    }
  }
  
  /* Error */
  private static AdResponse a(Context paramContext, NetworkResponse paramNetworkResponse, JSONObject paramJSONObject, String paramString1, AdFormat paramAdFormat, String paramString2)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokestatic 136	com/mopub/common/Preconditions:checkNotNull	(Ljava/lang/Object;)V
    //   4: aload_1
    //   5: invokestatic 136	com/mopub/common/Preconditions:checkNotNull	(Ljava/lang/Object;)V
    //   8: aload_2
    //   9: invokestatic 136	com/mopub/common/Preconditions:checkNotNull	(Ljava/lang/Object;)V
    //   12: aload 4
    //   14: invokestatic 136	com/mopub/common/Preconditions:checkNotNull	(Ljava/lang/Object;)V
    //   17: new 228	com/mopub/network/AdResponse$Builder
    //   20: astore_0
    //   21: aload_0
    //   22: invokespecial 229	com/mopub/network/AdResponse$Builder:<init>	()V
    //   25: getstatic 232	com/mopub/common/util/ResponseHeader:CONTENT	Lcom/mopub/common/util/ResponseHeader;
    //   28: invokevirtual 34	com/mopub/common/util/ResponseHeader:getKey	()Ljava/lang/String;
    //   31: astore_1
    //   32: aload_2
    //   33: aload_1
    //   34: invokevirtual 38	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   37: astore_1
    //   38: getstatic 139	com/mopub/common/util/ResponseHeader:METADATA	Lcom/mopub/common/util/ResponseHeader;
    //   41: invokevirtual 34	com/mopub/common/util/ResponseHeader:getKey	()Ljava/lang/String;
    //   44: astore 6
    //   46: aload_2
    //   47: aload 6
    //   49: invokevirtual 234	org/json/JSONObject:getJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   52: astore 6
    //   54: aload_0
    //   55: aload_3
    //   56: invokevirtual 238	com/mopub/network/AdResponse$Builder:setAdUnitId	(Ljava/lang/String;)Lcom/mopub/network/AdResponse$Builder;
    //   59: pop
    //   60: aload_0
    //   61: aload_1
    //   62: invokevirtual 241	com/mopub/network/AdResponse$Builder:setResponseBody	(Ljava/lang/String;)Lcom/mopub/network/AdResponse$Builder;
    //   65: pop
    //   66: getstatic 244	com/mopub/common/util/ResponseHeader:AD_TYPE	Lcom/mopub/common/util/ResponseHeader;
    //   69: astore_3
    //   70: aload 6
    //   72: aload_3
    //   73: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   76: astore_3
    //   77: getstatic 247	com/mopub/common/util/ResponseHeader:FULL_AD_TYPE	Lcom/mopub/common/util/ResponseHeader;
    //   80: astore 7
    //   82: aload 6
    //   84: aload 7
    //   86: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   89: astore 7
    //   91: aload_0
    //   92: aload_3
    //   93: invokevirtual 250	com/mopub/network/AdResponse$Builder:setAdType	(Ljava/lang/String;)Lcom/mopub/network/AdResponse$Builder;
    //   96: pop
    //   97: aload_0
    //   98: aload 7
    //   100: invokevirtual 253	com/mopub/network/AdResponse$Builder:setFullAdType	(Ljava/lang/String;)Lcom/mopub/network/AdResponse$Builder;
    //   103: pop
    //   104: aload_2
    //   105: invokestatic 136	com/mopub/common/Preconditions:checkNotNull	(Ljava/lang/Object;)V
    //   108: getstatic 139	com/mopub/common/util/ResponseHeader:METADATA	Lcom/mopub/common/util/ResponseHeader;
    //   111: invokevirtual 34	com/mopub/common/util/ResponseHeader:getKey	()Ljava/lang/String;
    //   114: astore 8
    //   116: aload_2
    //   117: aload 8
    //   119: invokevirtual 234	org/json/JSONObject:getJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   122: astore_2
    //   123: getstatic 256	com/mopub/common/util/ResponseHeader:REFRESH_TIME	Lcom/mopub/common/util/ResponseHeader;
    //   126: astore 8
    //   128: aload_2
    //   129: aload 8
    //   131: invokestatic 260	com/mopub/network/HeaderUtils:extractIntegerHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/Integer;
    //   134: astore_2
    //   135: aload_2
    //   136: ifnonnull +11 -> 147
    //   139: iconst_0
    //   140: istore 9
    //   142: aconst_null
    //   143: astore_2
    //   144: goto +19 -> 163
    //   147: aload_2
    //   148: invokevirtual 263	java/lang/Integer:intValue	()I
    //   151: sipush 1000
    //   154: imul
    //   155: istore 9
    //   157: iload 9
    //   159: invokestatic 219	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   162: astore_2
    //   163: aload_0
    //   164: aload_2
    //   165: invokevirtual 267	com/mopub/network/AdResponse$Builder:setRefreshTimeMilliseconds	(Ljava/lang/Integer;)Lcom/mopub/network/AdResponse$Builder;
    //   168: pop
    //   169: ldc 112
    //   171: astore_2
    //   172: aload_2
    //   173: aload_3
    //   174: invokevirtual 123	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   177: istore 9
    //   179: iload 9
    //   181: ifeq +8 -> 189
    //   184: aload_0
    //   185: invokevirtual 271	com/mopub/network/AdResponse$Builder:build	()Lcom/mopub/network/AdResponse;
    //   188: areturn
    //   189: getstatic 274	com/mopub/common/util/ResponseHeader:DSP_CREATIVE_ID	Lcom/mopub/common/util/ResponseHeader;
    //   192: astore_2
    //   193: aload 6
    //   195: aload_2
    //   196: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   199: astore_2
    //   200: aload_0
    //   201: aload_2
    //   202: invokevirtual 277	com/mopub/network/AdResponse$Builder:setDspCreativeId	(Ljava/lang/String;)Lcom/mopub/network/AdResponse$Builder;
    //   205: pop
    //   206: getstatic 280	com/mopub/common/util/ResponseHeader:NETWORK_TYPE	Lcom/mopub/common/util/ResponseHeader;
    //   209: astore_2
    //   210: aload 6
    //   212: aload_2
    //   213: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   216: astore_2
    //   217: aload_0
    //   218: aload_2
    //   219: invokevirtual 283	com/mopub/network/AdResponse$Builder:setNetworkType	(Ljava/lang/String;)Lcom/mopub/network/AdResponse$Builder;
    //   222: pop
    //   223: getstatic 286	com/mopub/common/util/ResponseHeader:CLICK_TRACKING_URL	Lcom/mopub/common/util/ResponseHeader;
    //   226: astore_2
    //   227: aload 6
    //   229: aload_2
    //   230: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   233: astore_2
    //   234: aload_0
    //   235: aload_2
    //   236: invokevirtual 289	com/mopub/network/AdResponse$Builder:setClickTrackingUrl	(Ljava/lang/String;)Lcom/mopub/network/AdResponse$Builder;
    //   239: pop
    //   240: getstatic 292	com/mopub/common/util/ResponseHeader:IMPRESSION_URLS	Lcom/mopub/common/util/ResponseHeader;
    //   243: astore 8
    //   245: aload 6
    //   247: aload 8
    //   249: invokestatic 295	com/mopub/network/HeaderUtils:a	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/util/List;
    //   252: astore 8
    //   254: aload 8
    //   256: invokeinterface 298 1 0
    //   261: istore 10
    //   263: iload 10
    //   265: ifeq +27 -> 292
    //   268: getstatic 301	com/mopub/common/util/ResponseHeader:IMPRESSION_URL	Lcom/mopub/common/util/ResponseHeader;
    //   271: astore 11
    //   273: aload 6
    //   275: aload 11
    //   277: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   280: astore 11
    //   282: aload 8
    //   284: aload 11
    //   286: invokeinterface 128 2 0
    //   291: pop
    //   292: aload_0
    //   293: aload 8
    //   295: invokevirtual 305	com/mopub/network/AdResponse$Builder:setImpressionTrackingUrls	(Ljava/util/List;)Lcom/mopub/network/AdResponse$Builder;
    //   298: pop
    //   299: getstatic 308	com/mopub/common/util/ResponseHeader:BEFORE_LOAD_URL	Lcom/mopub/common/util/ResponseHeader;
    //   302: astore 8
    //   304: aload 6
    //   306: aload 8
    //   308: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   311: astore 8
    //   313: aload_0
    //   314: aload 8
    //   316: invokevirtual 311	com/mopub/network/AdResponse$Builder:setBeforeLoadUrl	(Ljava/lang/String;)Lcom/mopub/network/AdResponse$Builder;
    //   319: pop
    //   320: getstatic 314	com/mopub/common/util/ResponseHeader:AFTER_LOAD_URL	Lcom/mopub/common/util/ResponseHeader;
    //   323: astore 8
    //   325: aload 6
    //   327: aload 8
    //   329: invokestatic 295	com/mopub/network/HeaderUtils:a	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/util/List;
    //   332: astore 8
    //   334: aload 8
    //   336: invokeinterface 298 1 0
    //   341: istore 10
    //   343: iload 10
    //   345: ifeq +27 -> 372
    //   348: getstatic 314	com/mopub/common/util/ResponseHeader:AFTER_LOAD_URL	Lcom/mopub/common/util/ResponseHeader;
    //   351: astore 11
    //   353: aload 6
    //   355: aload 11
    //   357: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   360: astore 11
    //   362: aload 8
    //   364: aload 11
    //   366: invokeinterface 128 2 0
    //   371: pop
    //   372: aload_0
    //   373: aload 8
    //   375: invokevirtual 317	com/mopub/network/AdResponse$Builder:setAfterLoadUrls	(Ljava/util/List;)Lcom/mopub/network/AdResponse$Builder;
    //   378: pop
    //   379: getstatic 320	com/mopub/common/util/ResponseHeader:AFTER_LOAD_SUCCESS_URL	Lcom/mopub/common/util/ResponseHeader;
    //   382: astore 8
    //   384: aload 6
    //   386: aload 8
    //   388: invokestatic 295	com/mopub/network/HeaderUtils:a	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/util/List;
    //   391: astore 8
    //   393: aload 8
    //   395: invokeinterface 298 1 0
    //   400: istore 10
    //   402: iload 10
    //   404: ifeq +27 -> 431
    //   407: getstatic 320	com/mopub/common/util/ResponseHeader:AFTER_LOAD_SUCCESS_URL	Lcom/mopub/common/util/ResponseHeader;
    //   410: astore 11
    //   412: aload 6
    //   414: aload 11
    //   416: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   419: astore 11
    //   421: aload 8
    //   423: aload 11
    //   425: invokeinterface 128 2 0
    //   430: pop
    //   431: aload_0
    //   432: aload 8
    //   434: invokevirtual 323	com/mopub/network/AdResponse$Builder:setAfterLoadSuccessUrls	(Ljava/util/List;)Lcom/mopub/network/AdResponse$Builder;
    //   437: pop
    //   438: getstatic 326	com/mopub/common/util/ResponseHeader:AFTER_LOAD_FAIL_URL	Lcom/mopub/common/util/ResponseHeader;
    //   441: astore 8
    //   443: aload 6
    //   445: aload 8
    //   447: invokestatic 295	com/mopub/network/HeaderUtils:a	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/util/List;
    //   450: astore 8
    //   452: aload 8
    //   454: invokeinterface 298 1 0
    //   459: istore 10
    //   461: iload 10
    //   463: ifeq +27 -> 490
    //   466: getstatic 326	com/mopub/common/util/ResponseHeader:AFTER_LOAD_FAIL_URL	Lcom/mopub/common/util/ResponseHeader;
    //   469: astore 11
    //   471: aload 6
    //   473: aload 11
    //   475: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   478: astore 11
    //   480: aload 8
    //   482: aload 11
    //   484: invokeinterface 128 2 0
    //   489: pop
    //   490: aload_0
    //   491: aload 8
    //   493: invokevirtual 329	com/mopub/network/AdResponse$Builder:setAfterLoadFailUrls	(Ljava/util/List;)Lcom/mopub/network/AdResponse$Builder;
    //   496: pop
    //   497: aload_0
    //   498: aload 5
    //   500: invokevirtual 332	com/mopub/network/AdResponse$Builder:setRequestId	(Ljava/lang/String;)Lcom/mopub/network/AdResponse$Builder;
    //   503: pop
    //   504: getstatic 335	com/mopub/common/util/ResponseHeader:WIDTH	Lcom/mopub/common/util/ResponseHeader;
    //   507: astore 5
    //   509: aload 6
    //   511: aload 5
    //   513: invokestatic 260	com/mopub/network/HeaderUtils:extractIntegerHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/Integer;
    //   516: astore 5
    //   518: getstatic 338	com/mopub/common/util/ResponseHeader:HEIGHT	Lcom/mopub/common/util/ResponseHeader;
    //   521: astore 8
    //   523: aload 6
    //   525: aload 8
    //   527: invokestatic 260	com/mopub/network/HeaderUtils:extractIntegerHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/Integer;
    //   530: astore 8
    //   532: aload_0
    //   533: aload 5
    //   535: aload 8
    //   537: invokevirtual 342	com/mopub/network/AdResponse$Builder:setDimensions	(Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/mopub/network/AdResponse$Builder;
    //   540: pop
    //   541: getstatic 345	com/mopub/common/util/ResponseHeader:AD_TIMEOUT	Lcom/mopub/common/util/ResponseHeader;
    //   544: astore 5
    //   546: aload 6
    //   548: aload 5
    //   550: invokestatic 260	com/mopub/network/HeaderUtils:extractIntegerHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/Integer;
    //   553: astore 5
    //   555: aload_0
    //   556: aload 5
    //   558: invokevirtual 348	com/mopub/network/AdResponse$Builder:setAdTimeoutDelayMilliseconds	(Ljava/lang/Integer;)Lcom/mopub/network/AdResponse$Builder;
    //   561: pop
    //   562: ldc_w 350
    //   565: astore 5
    //   567: aload 5
    //   569: aload_3
    //   570: invokevirtual 123	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   573: istore 12
    //   575: iload 12
    //   577: ifne +21 -> 598
    //   580: ldc_w 352
    //   583: astore 5
    //   585: aload 5
    //   587: aload_3
    //   588: invokevirtual 123	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   591: istore 12
    //   593: iload 12
    //   595: ifeq +21 -> 616
    //   598: new 21	org/json/JSONObject
    //   601: astore 5
    //   603: aload 5
    //   605: aload_1
    //   606: invokespecial 24	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   609: aload_0
    //   610: aload 5
    //   612: invokevirtual 356	com/mopub/network/AdResponse$Builder:setJsonBody	(Lorg/json/JSONObject;)Lcom/mopub/network/AdResponse$Builder;
    //   615: pop
    //   616: aload 4
    //   618: aload_3
    //   619: aload 7
    //   621: aload 6
    //   623: invokestatic 362	com/mopub/mobileads/AdTypeTranslator:getCustomEventName	(Lcom/mopub/common/AdFormat;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)Ljava/lang/String;
    //   626: astore 5
    //   628: aload_0
    //   629: aload 5
    //   631: invokevirtual 365	com/mopub/network/AdResponse$Builder:setCustomEventClassName	(Ljava/lang/String;)Lcom/mopub/network/AdResponse$Builder;
    //   634: pop
    //   635: getstatic 368	com/mopub/common/util/ResponseHeader:BROWSER_AGENT	Lcom/mopub/common/util/ResponseHeader;
    //   638: astore 5
    //   640: aload 6
    //   642: aload 5
    //   644: invokestatic 260	com/mopub/network/HeaderUtils:extractIntegerHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/Integer;
    //   647: invokestatic 374	com/mopub/common/MoPub$BrowserAgent:fromHeader	(Ljava/lang/Integer;)Lcom/mopub/common/MoPub$BrowserAgent;
    //   650: astore 5
    //   652: aload 5
    //   654: invokestatic 380	com/mopub/common/MoPub:setBrowserAgentFromAdServer	(Lcom/mopub/common/MoPub$BrowserAgent;)V
    //   657: aload_0
    //   658: aload 5
    //   660: invokevirtual 384	com/mopub/network/AdResponse$Builder:setBrowserAgent	(Lcom/mopub/common/MoPub$BrowserAgent;)Lcom/mopub/network/AdResponse$Builder;
    //   663: pop
    //   664: getstatic 387	com/mopub/common/util/ResponseHeader:CUSTOM_EVENT_DATA	Lcom/mopub/common/util/ResponseHeader;
    //   667: astore 5
    //   669: aload 6
    //   671: aload 5
    //   673: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   676: astore 5
    //   678: aload 5
    //   680: invokestatic 392	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   683: istore 13
    //   685: iload 13
    //   687: ifeq +17 -> 704
    //   690: getstatic 395	com/mopub/common/util/ResponseHeader:NATIVE_PARAMS	Lcom/mopub/common/util/ResponseHeader;
    //   693: astore 5
    //   695: aload 6
    //   697: aload 5
    //   699: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   702: astore 5
    //   704: aload 5
    //   706: invokestatic 401	com/mopub/common/util/Json:jsonStringToMap	(Ljava/lang/String;)Ljava/util/Map;
    //   709: astore 5
    //   711: ldc_w 403
    //   714: astore 8
    //   716: aload 6
    //   718: aload 8
    //   720: invokevirtual 38	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   723: astore 8
    //   725: aload 8
    //   727: invokevirtual 404	java/lang/String:isEmpty	()Z
    //   730: istore 13
    //   732: iload 13
    //   734: ifne +34 -> 768
    //   737: ldc_w 403
    //   740: astore 8
    //   742: ldc_w 403
    //   745: astore 11
    //   747: aload 6
    //   749: aload 11
    //   751: invokevirtual 407	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   754: astore 11
    //   756: aload 5
    //   758: aload 8
    //   760: aload 11
    //   762: invokeinterface 413 3 0
    //   767: pop
    //   768: aload_2
    //   769: invokestatic 392	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   772: istore 13
    //   774: iload 13
    //   776: ifne +19 -> 795
    //   779: ldc_w 415
    //   782: astore 8
    //   784: aload 5
    //   786: aload 8
    //   788: aload_2
    //   789: invokeinterface 413 3 0
    //   794: pop
    //   795: ldc_w 417
    //   798: astore_2
    //   799: aload_2
    //   800: aload_3
    //   801: invokevirtual 123	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   804: istore 9
    //   806: iconst_0
    //   807: istore 13
    //   809: aconst_null
    //   810: astore 8
    //   812: iload 9
    //   814: ifne +112 -> 926
    //   817: ldc_w 419
    //   820: astore_2
    //   821: aload_2
    //   822: aload_3
    //   823: invokevirtual 123	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   826: istore 9
    //   828: iload 9
    //   830: ifne +96 -> 926
    //   833: ldc_w 421
    //   836: astore_2
    //   837: aload_2
    //   838: aload_3
    //   839: invokevirtual 123	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   842: istore 9
    //   844: iload 9
    //   846: ifeq +20 -> 866
    //   849: ldc_w 423
    //   852: astore_2
    //   853: aload_2
    //   854: aload 7
    //   856: invokevirtual 123	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   859: istore 9
    //   861: iload 9
    //   863: ifne +63 -> 926
    //   866: ldc_w 425
    //   869: astore_2
    //   870: aload_2
    //   871: aload_3
    //   872: invokevirtual 123	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   875: istore 9
    //   877: iload 9
    //   879: ifeq +20 -> 899
    //   882: ldc_w 423
    //   885: astore_2
    //   886: aload_2
    //   887: aload 7
    //   889: invokevirtual 123	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   892: istore 9
    //   894: iload 9
    //   896: ifne +30 -> 926
    //   899: ldc_w 427
    //   902: astore_2
    //   903: aload_2
    //   904: aload_3
    //   905: invokevirtual 123	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   908: istore 9
    //   910: iload 9
    //   912: ifeq +6 -> 918
    //   915: goto +11 -> 926
    //   918: iconst_0
    //   919: istore 9
    //   921: aconst_null
    //   922: astore_2
    //   923: goto +6 -> 929
    //   926: iconst_1
    //   927: istore 9
    //   929: iload 9
    //   931: ifeq +40 -> 971
    //   934: aload 5
    //   936: ldc_w 430
    //   939: aload_1
    //   940: invokeinterface 413 3 0
    //   945: pop
    //   946: ldc_w 432
    //   949: astore_1
    //   950: getstatic 435	com/mopub/common/util/ResponseHeader:ORIENTATION	Lcom/mopub/common/util/ResponseHeader;
    //   953: astore_2
    //   954: aload 6
    //   956: aload_2
    //   957: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   960: astore_2
    //   961: aload 5
    //   963: aload_1
    //   964: aload_2
    //   965: invokeinterface 413 3 0
    //   970: pop
    //   971: ldc_w 350
    //   974: astore_1
    //   975: aload_1
    //   976: aload_3
    //   977: invokevirtual 123	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   980: istore 14
    //   982: iload 14
    //   984: ifne +19 -> 1003
    //   987: ldc_w 352
    //   990: astore_1
    //   991: aload_1
    //   992: aload_3
    //   993: invokevirtual 123	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   996: istore 14
    //   998: iload 14
    //   1000: ifeq +118 -> 1118
    //   1003: getstatic 438	com/mopub/common/util/ResponseHeader:IMPRESSION_MIN_VISIBLE_PERCENT	Lcom/mopub/common/util/ResponseHeader;
    //   1006: astore_1
    //   1007: aload 6
    //   1009: aload_1
    //   1010: invokestatic 440	com/mopub/network/HeaderUtils:b	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   1013: astore_1
    //   1014: getstatic 443	com/mopub/common/util/ResponseHeader:IMPRESSION_VISIBLE_MS	Lcom/mopub/common/util/ResponseHeader;
    //   1017: astore_2
    //   1018: aload 6
    //   1020: aload_2
    //   1021: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   1024: astore_2
    //   1025: getstatic 446	com/mopub/common/util/ResponseHeader:IMPRESSION_MIN_VISIBLE_PX	Lcom/mopub/common/util/ResponseHeader;
    //   1028: astore 11
    //   1030: aload 6
    //   1032: aload 11
    //   1034: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   1037: astore 11
    //   1039: aload_1
    //   1040: invokestatic 392	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   1043: istore 15
    //   1045: iload 15
    //   1047: ifne +19 -> 1066
    //   1050: ldc_w 448
    //   1053: astore 16
    //   1055: aload 5
    //   1057: aload 16
    //   1059: aload_1
    //   1060: invokeinterface 413 3 0
    //   1065: pop
    //   1066: aload_2
    //   1067: invokestatic 392	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   1070: istore 14
    //   1072: iload 14
    //   1074: ifne +17 -> 1091
    //   1077: ldc_w 450
    //   1080: astore_1
    //   1081: aload 5
    //   1083: aload_1
    //   1084: aload_2
    //   1085: invokeinterface 413 3 0
    //   1090: pop
    //   1091: aload 11
    //   1093: invokestatic 392	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   1096: istore 14
    //   1098: iload 14
    //   1100: ifne +18 -> 1118
    //   1103: ldc_w 452
    //   1106: astore_1
    //   1107: aload 5
    //   1109: aload_1
    //   1110: aload 11
    //   1112: invokeinterface 413 3 0
    //   1117: pop
    //   1118: ldc_w 352
    //   1121: astore_1
    //   1122: aload_1
    //   1123: aload_3
    //   1124: invokevirtual 123	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1127: istore 14
    //   1129: iload 14
    //   1131: ifeq +74 -> 1205
    //   1134: getstatic 457	com/mopub/common/util/ResponseHeader:PLAY_VISIBLE_PERCENT	Lcom/mopub/common/util/ResponseHeader;
    //   1137: astore_2
    //   1138: aload 6
    //   1140: aload_2
    //   1141: invokestatic 440	com/mopub/network/HeaderUtils:b	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   1144: astore_2
    //   1145: aload 5
    //   1147: ldc_w 454
    //   1150: aload_2
    //   1151: invokeinterface 413 3 0
    //   1156: pop
    //   1157: getstatic 462	com/mopub/common/util/ResponseHeader:PAUSE_VISIBLE_PERCENT	Lcom/mopub/common/util/ResponseHeader;
    //   1160: astore_2
    //   1161: aload 6
    //   1163: aload_2
    //   1164: invokestatic 440	com/mopub/network/HeaderUtils:b	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   1167: astore_2
    //   1168: aload 5
    //   1170: ldc_w 459
    //   1173: aload_2
    //   1174: invokeinterface 413 3 0
    //   1179: pop
    //   1180: ldc_w 464
    //   1183: astore_1
    //   1184: getstatic 467	com/mopub/common/util/ResponseHeader:MAX_BUFFER_MS	Lcom/mopub/common/util/ResponseHeader;
    //   1187: astore_2
    //   1188: aload 6
    //   1190: aload_2
    //   1191: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   1194: astore_2
    //   1195: aload 5
    //   1197: aload_1
    //   1198: aload_2
    //   1199: invokeinterface 413 3 0
    //   1204: pop
    //   1205: getstatic 470	com/mopub/common/util/ResponseHeader:VIDEO_TRACKERS	Lcom/mopub/common/util/ResponseHeader;
    //   1208: astore_1
    //   1209: aload 6
    //   1211: aload_1
    //   1212: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   1215: astore_1
    //   1216: aload_1
    //   1217: invokestatic 392	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   1220: istore 9
    //   1222: iload 9
    //   1224: ifne +17 -> 1241
    //   1227: ldc_w 472
    //   1230: astore_2
    //   1231: aload 5
    //   1233: aload_2
    //   1234: aload_1
    //   1235: invokeinterface 413 3 0
    //   1240: pop
    //   1241: ldc_w 425
    //   1244: astore_1
    //   1245: aload_1
    //   1246: aload_3
    //   1247: invokevirtual 123	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1250: istore 14
    //   1252: iload 14
    //   1254: ifne +36 -> 1290
    //   1257: ldc_w 421
    //   1260: astore_1
    //   1261: aload_1
    //   1262: aload_3
    //   1263: invokevirtual 123	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1266: istore 14
    //   1268: iload 14
    //   1270: ifeq +45 -> 1315
    //   1273: ldc_w 423
    //   1276: astore_1
    //   1277: aload_1
    //   1278: aload 7
    //   1280: invokevirtual 123	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1283: istore 14
    //   1285: iload 14
    //   1287: ifeq +28 -> 1315
    //   1290: ldc_w 474
    //   1293: astore_1
    //   1294: getstatic 477	com/mopub/common/util/ResponseHeader:VIDEO_VIEWABILITY_TRACKERS	Lcom/mopub/common/util/ResponseHeader;
    //   1297: astore_2
    //   1298: aload 6
    //   1300: aload_2
    //   1301: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   1304: astore_2
    //   1305: aload 5
    //   1307: aload_1
    //   1308: aload_2
    //   1309: invokeinterface 413 3 0
    //   1314: pop
    //   1315: getstatic 483	com/mopub/common/AdFormat:BANNER	Lcom/mopub/common/AdFormat;
    //   1318: astore_1
    //   1319: aload_1
    //   1320: aload 4
    //   1322: invokevirtual 484	com/mopub/common/AdFormat:equals	(Ljava/lang/Object;)Z
    //   1325: istore 14
    //   1327: iload 14
    //   1329: ifeq +51 -> 1380
    //   1332: getstatic 489	com/mopub/common/util/ResponseHeader:BANNER_IMPRESSION_MIN_VISIBLE_MS	Lcom/mopub/common/util/ResponseHeader;
    //   1335: astore_2
    //   1336: aload 6
    //   1338: aload_2
    //   1339: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   1342: astore_2
    //   1343: aload 5
    //   1345: ldc_w 486
    //   1348: aload_2
    //   1349: invokeinterface 413 3 0
    //   1354: pop
    //   1355: ldc_w 491
    //   1358: astore_1
    //   1359: getstatic 494	com/mopub/common/util/ResponseHeader:BANNER_IMPRESSION_MIN_VISIBLE_DIPS	Lcom/mopub/common/util/ResponseHeader;
    //   1362: astore_2
    //   1363: aload 6
    //   1365: aload_2
    //   1366: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   1369: astore_2
    //   1370: aload 5
    //   1372: aload_1
    //   1373: aload_2
    //   1374: invokeinterface 413 3 0
    //   1379: pop
    //   1380: getstatic 497	com/mopub/common/util/ResponseHeader:DISABLE_VIEWABILITY	Lcom/mopub/common/util/ResponseHeader;
    //   1383: astore_1
    //   1384: aload 6
    //   1386: aload_1
    //   1387: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   1390: astore_1
    //   1391: aload_1
    //   1392: invokestatic 392	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   1395: istore 9
    //   1397: iload 9
    //   1399: ifne +16 -> 1415
    //   1402: aload_1
    //   1403: invokestatic 503	com/mopub/common/ExternalViewabilitySessionManager$ViewabilityVendor:fromKey	(Ljava/lang/String;)Lcom/mopub/common/ExternalViewabilitySessionManager$ViewabilityVendor;
    //   1406: astore_1
    //   1407: aload_1
    //   1408: ifnull +7 -> 1415
    //   1411: aload_1
    //   1412: invokevirtual 506	com/mopub/common/ExternalViewabilitySessionManager$ViewabilityVendor:disable	()V
    //   1415: aload_0
    //   1416: aload 5
    //   1418: invokevirtual 510	com/mopub/network/AdResponse$Builder:setServerExtras	(Ljava/util/Map;)Lcom/mopub/network/AdResponse$Builder;
    //   1421: pop
    //   1422: ldc_w 425
    //   1425: astore_1
    //   1426: aload_1
    //   1427: aload_3
    //   1428: invokevirtual 123	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1431: istore 14
    //   1433: iload 14
    //   1435: ifne +35 -> 1470
    //   1438: ldc_w 512
    //   1441: astore_1
    //   1442: aload_1
    //   1443: aload_3
    //   1444: invokevirtual 123	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1447: istore 14
    //   1449: iload 14
    //   1451: ifne +19 -> 1470
    //   1454: ldc_w 427
    //   1457: astore_1
    //   1458: aload_1
    //   1459: aload_3
    //   1460: invokevirtual 123	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1463: istore 14
    //   1465: iload 14
    //   1467: ifeq +118 -> 1585
    //   1470: getstatic 515	com/mopub/common/util/ResponseHeader:REWARDED_VIDEO_CURRENCY_NAME	Lcom/mopub/common/util/ResponseHeader;
    //   1473: astore_1
    //   1474: aload 6
    //   1476: aload_1
    //   1477: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   1480: astore_1
    //   1481: getstatic 518	com/mopub/common/util/ResponseHeader:REWARDED_VIDEO_CURRENCY_AMOUNT	Lcom/mopub/common/util/ResponseHeader;
    //   1484: astore_2
    //   1485: aload 6
    //   1487: aload_2
    //   1488: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   1491: astore_2
    //   1492: getstatic 521	com/mopub/common/util/ResponseHeader:REWARDED_CURRENCIES	Lcom/mopub/common/util/ResponseHeader;
    //   1495: astore_3
    //   1496: aload 6
    //   1498: aload_3
    //   1499: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   1502: astore_3
    //   1503: getstatic 524	com/mopub/common/util/ResponseHeader:REWARDED_VIDEO_COMPLETION_URL	Lcom/mopub/common/util/ResponseHeader;
    //   1506: astore 4
    //   1508: aload 6
    //   1510: aload 4
    //   1512: invokestatic 65	com/mopub/network/HeaderUtils:extractHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/String;
    //   1515: astore 4
    //   1517: getstatic 527	com/mopub/common/util/ResponseHeader:REWARDED_DURATION	Lcom/mopub/common/util/ResponseHeader;
    //   1520: astore 5
    //   1522: aload 6
    //   1524: aload 5
    //   1526: invokestatic 260	com/mopub/network/HeaderUtils:extractIntegerHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;)Ljava/lang/Integer;
    //   1529: astore 5
    //   1531: getstatic 530	com/mopub/common/util/ResponseHeader:SHOULD_REWARD_ON_CLICK	Lcom/mopub/common/util/ResponseHeader;
    //   1534: astore 7
    //   1536: aload 6
    //   1538: aload 7
    //   1540: iconst_0
    //   1541: invokestatic 52	com/mopub/network/HeaderUtils:extractBooleanHeader	(Lorg/json/JSONObject;Lcom/mopub/common/util/ResponseHeader;Z)Z
    //   1544: istore 17
    //   1546: aload_0
    //   1547: aload_1
    //   1548: invokevirtual 533	com/mopub/network/AdResponse$Builder:setRewardedVideoCurrencyName	(Ljava/lang/String;)Lcom/mopub/network/AdResponse$Builder;
    //   1551: pop
    //   1552: aload_0
    //   1553: aload_2
    //   1554: invokevirtual 536	com/mopub/network/AdResponse$Builder:setRewardedVideoCurrencyAmount	(Ljava/lang/String;)Lcom/mopub/network/AdResponse$Builder;
    //   1557: pop
    //   1558: aload_0
    //   1559: aload_3
    //   1560: invokevirtual 539	com/mopub/network/AdResponse$Builder:setRewardedCurrencies	(Ljava/lang/String;)Lcom/mopub/network/AdResponse$Builder;
    //   1563: pop
    //   1564: aload_0
    //   1565: aload 4
    //   1567: invokevirtual 542	com/mopub/network/AdResponse$Builder:setRewardedVideoCompletionUrl	(Ljava/lang/String;)Lcom/mopub/network/AdResponse$Builder;
    //   1570: pop
    //   1571: aload_0
    //   1572: aload 5
    //   1574: invokevirtual 545	com/mopub/network/AdResponse$Builder:setRewardedDuration	(Ljava/lang/Integer;)Lcom/mopub/network/AdResponse$Builder;
    //   1577: pop
    //   1578: aload_0
    //   1579: iload 17
    //   1581: invokevirtual 549	com/mopub/network/AdResponse$Builder:setShouldRewardOnClick	(Z)Lcom/mopub/network/AdResponse$Builder;
    //   1584: pop
    //   1585: aload_0
    //   1586: invokevirtual 271	com/mopub/network/AdResponse$Builder:build	()Lcom/mopub/network/AdResponse;
    //   1589: areturn
    //   1590: astore_0
    //   1591: new 148	com/mopub/network/MoPubNetworkError
    //   1594: astore_1
    //   1595: getstatic 552	com/mopub/network/MoPubNetworkError$Reason:BAD_BODY	Lcom/mopub/network/MoPubNetworkError$Reason;
    //   1598: astore_2
    //   1599: aload_1
    //   1600: ldc_w 554
    //   1603: aload_0
    //   1604: aload_2
    //   1605: invokespecial 557	com/mopub/network/MoPubNetworkError:<init>	(Ljava/lang/String;Ljava/lang/Throwable;Lcom/mopub/network/MoPubNetworkError$Reason;)V
    //   1608: aload_1
    //   1609: athrow
    //   1610: astore_0
    //   1611: new 148	com/mopub/network/MoPubNetworkError
    //   1614: astore_1
    //   1615: getstatic 560	com/mopub/network/MoPubNetworkError$Reason:BAD_HEADER_DATA	Lcom/mopub/network/MoPubNetworkError$Reason;
    //   1618: astore_2
    //   1619: aload_1
    //   1620: ldc_w 562
    //   1623: aload_0
    //   1624: aload_2
    //   1625: invokespecial 557	com/mopub/network/MoPubNetworkError:<init>	(Ljava/lang/String;Ljava/lang/Throwable;Lcom/mopub/network/MoPubNetworkError$Reason;)V
    //   1628: aload_1
    //   1629: athrow
    //   1630: astore_0
    //   1631: new 148	com/mopub/network/MoPubNetworkError
    //   1634: astore_1
    //   1635: getstatic 552	com/mopub/network/MoPubNetworkError$Reason:BAD_BODY	Lcom/mopub/network/MoPubNetworkError$Reason;
    //   1638: astore_2
    //   1639: aload_1
    //   1640: ldc_w 564
    //   1643: aload_0
    //   1644: aload_2
    //   1645: invokespecial 557	com/mopub/network/MoPubNetworkError:<init>	(Ljava/lang/String;Ljava/lang/Throwable;Lcom/mopub/network/MoPubNetworkError$Reason;)V
    //   1648: aload_1
    //   1649: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1650	0	paramContext	Context
    //   0	1650	1	paramNetworkResponse	NetworkResponse
    //   0	1650	2	paramJSONObject	JSONObject
    //   0	1650	3	paramString1	String
    //   0	1650	4	paramAdFormat	AdFormat
    //   0	1650	5	paramString2	String
    //   44	1493	6	localObject1	Object
    //   80	1459	7	localObject2	Object
    //   114	697	8	localObject3	Object
    //   140	18	9	i	int
    //   177	1221	9	bool1	boolean
    //   261	201	10	bool2	boolean
    //   271	840	11	localObject4	Object
    //   573	21	12	bool3	boolean
    //   683	125	13	bool4	boolean
    //   980	486	14	bool5	boolean
    //   1043	3	15	bool6	boolean
    //   1053	5	16	str	String
    //   1544	36	17	bool7	boolean
    // Exception table:
    //   from	to	target	type
    //   718	723	1590	org/json/JSONException
    //   725	730	1590	org/json/JSONException
    //   749	754	1590	org/json/JSONException
    //   760	768	1590	org/json/JSONException
    //   704	709	1610	org/json/JSONException
    //   598	601	1630	org/json/JSONException
    //   605	609	1630	org/json/JSONException
    //   610	616	1630	org/json/JSONException
  }
  
  private static String a(NetworkResponse paramNetworkResponse)
  {
    Preconditions.checkNotNull(paramNetworkResponse);
    String str;
    try
    {
      str = new java/lang/String;
      byte[] arrayOfByte = data;
      Object localObject = headers;
      localObject = HttpHeaderParser.parseCharset((Map)localObject);
      str.<init>(arrayOfByte, (String)localObject);
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      str = new java/lang/String;
      paramNetworkResponse = data;
      str.<init>(paramNetworkResponse);
    }
    return str;
  }
  
  public static void setServerOverrideListener(MultiAdResponse.ServerOverrideListener paramServerOverrideListener)
  {
    c = paramServerOverrideListener;
  }
  
  public String getFailURL()
  {
    return a;
  }
  
  public boolean hasNext()
  {
    return b.hasNext();
  }
  
  public AdResponse next()
  {
    return (AdResponse)b.next();
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.MultiAdResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */