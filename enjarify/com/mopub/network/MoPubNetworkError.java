package com.mopub.network;

import com.mopub.volley.NetworkResponse;
import com.mopub.volley.VolleyError;

public class MoPubNetworkError
  extends VolleyError
{
  private final MoPubNetworkError.Reason b;
  private final Integer c;
  
  public MoPubNetworkError(MoPubNetworkError.Reason paramReason)
  {
    b = paramReason;
    c = null;
  }
  
  public MoPubNetworkError(NetworkResponse paramNetworkResponse, MoPubNetworkError.Reason paramReason)
  {
    super(paramNetworkResponse);
    b = paramReason;
    c = null;
  }
  
  public MoPubNetworkError(String paramString, MoPubNetworkError.Reason paramReason)
  {
    this(paramString, paramReason, null);
  }
  
  public MoPubNetworkError(String paramString, MoPubNetworkError.Reason paramReason, Integer paramInteger)
  {
    super(paramString);
    b = paramReason;
    c = paramInteger;
  }
  
  public MoPubNetworkError(String paramString, Throwable paramThrowable, MoPubNetworkError.Reason paramReason)
  {
    super(paramString, paramThrowable);
    b = paramReason;
    c = null;
  }
  
  public MoPubNetworkError(Throwable paramThrowable, MoPubNetworkError.Reason paramReason)
  {
    super(paramThrowable);
    b = paramReason;
    c = null;
  }
  
  public MoPubNetworkError.Reason getReason()
  {
    return b;
  }
  
  public Integer getRefreshTimeMillis()
  {
    return c;
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.MoPubNetworkError
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */