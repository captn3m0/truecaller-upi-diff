package com.mopub.network;

import com.mopub.volley.VolleyError;

public abstract class BackoffPolicy
{
  protected int a;
  protected int b;
  protected int c;
  
  public abstract void backoff(VolleyError paramVolleyError);
  
  public int getBackoffMs()
  {
    return a;
  }
  
  public int getRetryCount()
  {
    return b;
  }
  
  public boolean hasAttemptRemaining()
  {
    int i = b;
    int j = c;
    return i < j;
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.BackoffPolicy
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */