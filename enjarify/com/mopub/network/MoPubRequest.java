package com.mopub.network;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.os.LocaleList;
import android.text.TextUtils;
import com.mopub.common.Preconditions;
import com.mopub.common.util.ResponseHeader;
import com.mopub.volley.NetworkResponse;
import com.mopub.volley.Request;
import com.mopub.volley.Response.ErrorListener;
import com.mopub.volley.toolbox.HttpHeaderParser;
import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

public abstract class MoPubRequest
  extends Request
{
  private final String a;
  private final Context b;
  
  public MoPubRequest(Context paramContext, String paramString, Response.ErrorListener paramErrorListener)
  {
    super(i, str, paramErrorListener);
    a = paramString;
    paramContext = paramContext.getApplicationContext();
    b = paramContext;
  }
  
  protected static String a(NetworkResponse paramNetworkResponse)
  {
    Preconditions.checkNotNull(paramNetworkResponse);
    String str;
    try
    {
      str = new java/lang/String;
      byte[] arrayOfByte = data;
      Object localObject = headers;
      localObject = HttpHeaderParser.parseCharset((Map)localObject);
      str.<init>(arrayOfByte, (String)localObject);
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      str = new java/lang/String;
      paramNetworkResponse = data;
      str.<init>(paramNetworkResponse);
    }
    return str;
  }
  
  public byte[] getBody()
  {
    Object localObject = getParams();
    String str = getUrl();
    localObject = MoPubRequestUtils.generateBodyFromParams((Map)localObject, str);
    if (localObject == null) {
      return null;
    }
    return ((String)localObject).getBytes();
  }
  
  public String getBodyContentType()
  {
    String str = getUrl();
    boolean bool = MoPubRequestUtils.isMoPubRequest(str);
    if (bool) {
      return "application/json; charset=UTF-8";
    }
    return super.getBodyContentType();
  }
  
  public Map getHeaders()
  {
    TreeMap localTreeMap = new java/util/TreeMap;
    localTreeMap.<init>();
    int i = Build.VERSION.SDK_INT;
    int j = 24;
    if (i >= j)
    {
      localObject = b.getResources().getConfiguration().getLocales();
      j = ((LocaleList)localObject).size();
      if (j > 0)
      {
        j = 0;
        str = null;
        localObject = ((LocaleList)localObject).get(0);
      }
      else
      {
        i = 0;
        localObject = null;
      }
    }
    else
    {
      localObject = b.getResources().getConfiguration().locale;
    }
    if (localObject != null)
    {
      str = ((Locale)localObject).toString().trim();
      boolean bool1 = TextUtils.isEmpty(str);
      if (!bool1)
      {
        str = ((Locale)localObject).getLanguage().trim();
        localObject = ((Locale)localObject).getCountry().trim();
        break label154;
      }
    }
    String str = Locale.getDefault().getLanguage().trim();
    Object localObject = Locale.getDefault().getCountry().trim();
    label154:
    boolean bool2 = TextUtils.isEmpty(str);
    if (!bool2)
    {
      bool2 = TextUtils.isEmpty((CharSequence)localObject);
      if (!bool2)
      {
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append(str);
        localStringBuilder.append("-");
        localObject = ((String)localObject).toLowerCase();
        localStringBuilder.append((String)localObject);
        str = localStringBuilder.toString();
      }
      localObject = ResponseHeader.ACCEPT_LANGUAGE.getKey();
      localTreeMap.put(localObject, str);
    }
    return localTreeMap;
  }
  
  public String getOriginalUrl()
  {
    return a;
  }
  
  public final Map getParams()
  {
    Object localObject = getUrl();
    boolean bool = MoPubRequestUtils.isMoPubRequest((String)localObject);
    if (!bool) {
      return null;
    }
    localObject = b;
    String str = a;
    return MoPubRequestUtils.convertQueryToMap((Context)localObject, str);
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.MoPubRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */