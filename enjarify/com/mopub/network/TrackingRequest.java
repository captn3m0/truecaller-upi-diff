package com.mopub.network;

import android.content.Context;
import android.text.TextUtils;
import com.mopub.common.Preconditions;
import com.mopub.mobileads.VastErrorCode;
import com.mopub.mobileads.VastMacroHelper;
import com.mopub.mobileads.VastTracker;
import com.mopub.volley.DefaultRetryPolicy;
import com.mopub.volley.NetworkResponse;
import com.mopub.volley.RequestQueue;
import com.mopub.volley.Response;
import com.mopub.volley.toolbox.HttpHeaderParser;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class TrackingRequest
  extends MoPubRequest
{
  private final TrackingRequest.Listener a;
  
  private TrackingRequest(Context paramContext, String paramString, TrackingRequest.Listener paramListener)
  {
    super(paramContext, paramString, paramListener);
    a = paramListener;
    setShouldCache(false);
    paramString = new com/mopub/volley/DefaultRetryPolicy;
    paramString.<init>(2500, 0, 1.0F);
    setRetryPolicy(paramString);
  }
  
  public static void makeTrackingHttpRequest(Iterable paramIterable, Context paramContext)
  {
    makeTrackingHttpRequest(paramIterable, paramContext, null);
  }
  
  public static void makeTrackingHttpRequest(Iterable paramIterable, Context paramContext, TrackingRequest.Listener paramListener)
  {
    if ((paramIterable != null) && (paramContext != null))
    {
      MoPubRequestQueue localMoPubRequestQueue = Networking.getRequestQueue(paramContext);
      paramIterable = paramIterable.iterator();
      for (;;)
      {
        boolean bool1 = paramIterable.hasNext();
        if (!bool1) {
          break;
        }
        String str = (String)paramIterable.next();
        boolean bool2 = TextUtils.isEmpty(str);
        if (!bool2)
        {
          TrackingRequest.1 local1 = new com/mopub/network/TrackingRequest$1;
          local1.<init>(paramListener, str);
          TrackingRequest localTrackingRequest = new com/mopub/network/TrackingRequest;
          localTrackingRequest.<init>(paramContext, str, local1);
          localMoPubRequestQueue.add(localTrackingRequest);
        }
      }
      return;
    }
  }
  
  public static void makeTrackingHttpRequest(String paramString, Context paramContext)
  {
    makeTrackingHttpRequest(paramString, paramContext, null);
  }
  
  public static void makeTrackingHttpRequest(String paramString, Context paramContext, TrackingRequest.Listener paramListener)
  {
    if (paramString != null)
    {
      int i = 1;
      String[] arrayOfString = new String[i];
      arrayOfString[0] = paramString;
      paramString = Arrays.asList(arrayOfString);
      makeTrackingHttpRequest(paramString, paramContext, paramListener);
    }
  }
  
  public static void makeVastTrackingHttpRequest(List paramList, VastErrorCode paramVastErrorCode, Integer paramInteger, String paramString, Context paramContext)
  {
    Preconditions.checkNotNull(paramList);
    ArrayList localArrayList = new java/util/ArrayList;
    int i = paramList.size();
    localArrayList.<init>(i);
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      VastTracker localVastTracker = (VastTracker)paramList.next();
      if (localVastTracker != null)
      {
        boolean bool2 = localVastTracker.isTracked();
        if (bool2)
        {
          bool2 = localVastTracker.isRepeatable();
          if (!bool2) {}
        }
        else
        {
          String str = localVastTracker.getContent();
          localArrayList.add(str);
          localVastTracker.setTracked();
        }
      }
    }
    paramList = new com/mopub/mobileads/VastMacroHelper;
    paramList.<init>(localArrayList);
    makeTrackingHttpRequest(paramList.withErrorCode(paramVastErrorCode).withContentPlayHead(paramInteger).withAssetUri(paramString).getUris(), paramContext);
  }
  
  public void deliverResponse(Void paramVoid)
  {
    paramVoid = a;
    if (paramVoid != null)
    {
      String str = getUrl();
      paramVoid.onResponse(str);
    }
  }
  
  public final Response parseNetworkResponse(NetworkResponse paramNetworkResponse)
  {
    int i = statusCode;
    int j = 200;
    if (i != j)
    {
      MoPubNetworkError localMoPubNetworkError = new com/mopub/network/MoPubNetworkError;
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>("Failed to log tracking request. Response code: ");
      int k = statusCode;
      ((StringBuilder)localObject).append(k);
      ((StringBuilder)localObject).append(" for url: ");
      paramNetworkResponse = getUrl();
      ((StringBuilder)localObject).append(paramNetworkResponse);
      paramNetworkResponse = ((StringBuilder)localObject).toString();
      localObject = MoPubNetworkError.Reason.TRACKING_FAILURE;
      localMoPubNetworkError.<init>(paramNetworkResponse, (MoPubNetworkError.Reason)localObject);
      return Response.error(localMoPubNetworkError);
    }
    paramNetworkResponse = HttpHeaderParser.parseCacheHeaders(paramNetworkResponse);
    return Response.success(null, paramNetworkResponse);
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.TrackingRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */