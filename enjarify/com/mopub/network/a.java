package com.mopub.network;

import android.content.Context;
import android.net.Uri;
import android.os.SystemClock;
import com.mopub.common.Preconditions;
import com.mopub.mobileads.MoPubError;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class a
{
  Long a = null;
  AdResponse b;
  
  a(AdResponse paramAdResponse)
  {
    Preconditions.checkNotNull(paramAdResponse);
    b = paramAdResponse;
  }
  
  static a.a a(MoPubError paramMoPubError)
  {
    if (paramMoPubError == null) {
      return a.a.AD_LOADED;
    }
    int i = paramMoPubError.getIntCode();
    switch (i)
    {
    default: 
      return a.a.INVALID_DATA;
    case 2: 
      return a.a.TIMEOUT;
    case 1: 
      return a.a.MISSING_ADAPTER;
    }
    return a.a.AD_LOADED;
  }
  
  final List a(List paramList, String paramString)
  {
    if (paramList != null)
    {
      boolean bool1 = paramList.isEmpty();
      if (!bool1)
      {
        Object localObject = a;
        if (localObject != null)
        {
          localObject = new java/util/ArrayList;
          ((ArrayList)localObject).<init>();
          paramList = paramList.iterator();
          for (;;)
          {
            boolean bool2 = paramList.hasNext();
            if (!bool2) {
              break;
            }
            String str1 = (String)paramList.next();
            long l1 = SystemClock.uptimeMillis();
            Long localLong = a;
            long l2 = localLong.longValue();
            l1 -= l2;
            String str2 = String.valueOf(l1);
            str1 = str1.replace("%%LOAD_DURATION_MS%%", str2);
            String str3 = "%%LOAD_RESULT%%";
            str2 = Uri.encode(paramString);
            str1 = str1.replace(str3, str2);
            ((List)localObject).add(str1);
          }
          return (List)localObject;
        }
      }
    }
    return null;
  }
  
  final void a(Context paramContext, MoPubError paramMoPubError)
  {
    if (paramContext != null)
    {
      Object localObject = a;
      if (localObject != null)
      {
        paramMoPubError = a(paramMoPubError);
        localObject = b.getAfterLoadUrls();
        paramMoPubError = a.a.a(paramMoPubError);
        TrackingRequest.makeTrackingHttpRequest(a((List)localObject, paramMoPubError), paramContext);
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */