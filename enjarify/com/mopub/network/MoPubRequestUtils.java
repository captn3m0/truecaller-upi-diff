package com.mopub.network;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.volley.toolbox.HurlStack.UrlRewriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

public class MoPubRequestUtils
{
  public static int chooseMethod(String paramString)
  {
    boolean bool = isMoPubRequest(paramString);
    if (bool) {
      return 1;
    }
    return 0;
  }
  
  public static Map convertQueryToMap(Context paramContext, String paramString)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramString);
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    paramContext = Uri.parse(Networking.getUrlRewriter(paramContext).rewriteUrl(paramString));
    paramString = paramContext.getQueryParameterNames().iterator();
    for (;;)
    {
      boolean bool = paramString.hasNext();
      if (!bool) {
        break;
      }
      String str1 = (String)paramString.next();
      List localList = paramContext.getQueryParameters(str1);
      String str2 = TextUtils.join(",", localList);
      localHashMap.put(str1, str2);
    }
    return localHashMap;
  }
  
  public static String generateBodyFromParams(Map paramMap, String paramString)
  {
    Preconditions.checkNotNull(paramString);
    boolean bool1 = isMoPubRequest(paramString);
    if ((bool1) && (paramMap != null))
    {
      bool1 = paramMap.isEmpty();
      if (!bool1)
      {
        paramString = new org/json/JSONObject;
        paramString.<init>();
        Iterator localIterator = paramMap.keySet().iterator();
        for (;;)
        {
          boolean bool2 = localIterator.hasNext();
          if (!bool2) {
            break;
          }
          String str1 = (String)localIterator.next();
          try
          {
            localObject = paramMap.get(str1);
            paramString.put(str1, localObject);
          }
          catch (JSONException localJSONException)
          {
            Object localObject = new java/lang/StringBuilder;
            String str2 = "Unable to add ";
            ((StringBuilder)localObject).<init>(str2);
            ((StringBuilder)localObject).append(str1);
            ((StringBuilder)localObject).append(" to JSON body.");
            str1 = ((StringBuilder)localObject).toString();
            MoPubLog.d(str1);
          }
        }
        return paramString.toString();
      }
    }
    return null;
  }
  
  public static boolean isMoPubRequest(String paramString)
  {
    Preconditions.checkNotNull(paramString);
    String str = "http://ads.mopub.com";
    boolean bool1 = paramString.startsWith(str);
    if (!bool1)
    {
      str = "https://ads.mopub.com";
      boolean bool2 = paramString.startsWith(str);
      if (!bool2) {
        return false;
      }
    }
    return true;
  }
  
  public static String truncateQueryParamsIfPost(String paramString)
  {
    Preconditions.checkNotNull(paramString);
    boolean bool = isMoPubRequest(paramString);
    if (!bool) {
      return paramString;
    }
    int i = paramString.indexOf('?');
    int j = -1;
    if (i == j) {
      return paramString;
    }
    return paramString.substring(0, i);
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.MoPubRequestUtils
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */