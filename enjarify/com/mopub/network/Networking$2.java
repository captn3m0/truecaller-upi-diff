package com.mopub.network;

import android.graphics.Bitmap;
import android.support.v4.f.g;
import com.mopub.volley.toolbox.ImageLoader.ImageCache;

final class Networking$2
  implements ImageLoader.ImageCache
{
  Networking$2(g paramg) {}
  
  public final Bitmap getBitmap(String paramString)
  {
    return (Bitmap)a.get(paramString);
  }
  
  public final void putBitmap(String paramString, Bitmap paramBitmap)
  {
    a.put(paramString, paramBitmap);
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.Networking.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */