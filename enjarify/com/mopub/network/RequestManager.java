package com.mopub.network;

import android.os.Handler;
import android.os.Looper;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.volley.Request;

public abstract class RequestManager
{
  protected Request a;
  protected RequestManager.RequestFactory b;
  protected BackoffPolicy c;
  protected Handler d;
  
  public RequestManager(Looper paramLooper)
  {
    Handler localHandler = new android/os/Handler;
    localHandler.<init>(paramLooper);
    d = localHandler;
  }
  
  private void b()
  {
    a = null;
    b = null;
    c = null;
  }
  
  abstract Request a();
  
  public void cancelRequest()
  {
    MoPubRequestQueue localMoPubRequestQueue = Networking.getRequestQueue();
    if (localMoPubRequestQueue != null)
    {
      Request localRequest = a;
      if (localRequest != null) {
        localMoPubRequestQueue.cancel(localRequest);
      }
    }
    b();
  }
  
  public boolean isAtCapacity()
  {
    Request localRequest = a;
    return localRequest != null;
  }
  
  public void makeRequest(RequestManager.RequestFactory paramRequestFactory, BackoffPolicy paramBackoffPolicy)
  {
    Preconditions.checkNotNull(paramRequestFactory);
    Preconditions.checkNotNull(paramBackoffPolicy);
    cancelRequest();
    b = paramRequestFactory;
    c = paramBackoffPolicy;
    paramRequestFactory = a();
    a = paramRequestFactory;
    paramRequestFactory = Networking.getRequestQueue();
    if (paramRequestFactory == null)
    {
      MoPubLog.d("MoPubRequest queue is null. Clearing request.");
      b();
      return;
    }
    paramBackoffPolicy = c;
    int i = paramBackoffPolicy.getRetryCount();
    if (i == 0)
    {
      paramBackoffPolicy = a;
      paramRequestFactory.add(paramBackoffPolicy);
      return;
    }
    paramBackoffPolicy = a;
    int j = c.getBackoffMs();
    paramRequestFactory.addDelayedRequest(paramBackoffPolicy, j);
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.RequestManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */