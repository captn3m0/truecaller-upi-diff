package com.mopub.network;

import com.mopub.common.logging.MoPubLog;
import com.mopub.volley.VolleyError;

final class TrackingRequest$1
  implements TrackingRequest.Listener
{
  TrackingRequest$1(TrackingRequest.Listener paramListener, String paramString) {}
  
  public final void onErrorResponse(VolleyError paramVolleyError)
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("Failed to hit tracking endpoint: ");
    String str = b;
    ((StringBuilder)localObject).append(str);
    MoPubLog.d(((StringBuilder)localObject).toString());
    localObject = a;
    if (localObject != null) {
      ((TrackingRequest.Listener)localObject).onErrorResponse(paramVolleyError);
    }
  }
  
  public final void onResponse(String paramString)
  {
    String str = String.valueOf(paramString);
    MoPubLog.d("Successfully hit tracking endpoint: ".concat(str));
    TrackingRequest.Listener localListener = a;
    if (localListener != null) {
      localListener.onResponse(paramString);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.TrackingRequest.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */