package com.mopub.network;

import java.net.InetAddress;

public class InetAddressUtils
{
  public static InetAddress getInetAddressByName(String paramString)
  {
    return InetAddress.getByName(paramString);
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.InetAddressUtils
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */