package com.mopub.network;

import com.mopub.common.MoPub.BrowserAgent;
import com.mopub.common.util.DateAndTime;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.json.JSONObject;

public class AdResponse
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private final MoPub.BrowserAgent A;
  private final Map B;
  private final long C;
  private final String a;
  private final String b;
  private final String c;
  private final String d;
  private final String e;
  private final String f;
  private final String g;
  private final String h;
  private final Integer i;
  private final boolean j;
  private final String k;
  private final List l;
  private final String m;
  private final String n;
  private final List o;
  private final List p;
  private final List q;
  private final String r;
  private final Integer s;
  private final Integer t;
  private final Integer u;
  private final Integer v;
  private final String w;
  private final String x;
  private final JSONObject y;
  private final String z;
  
  private AdResponse(AdResponse.Builder paramBuilder)
  {
    Object localObject = AdResponse.Builder.a(paramBuilder);
    a = ((String)localObject);
    localObject = AdResponse.Builder.b(paramBuilder);
    b = ((String)localObject);
    localObject = AdResponse.Builder.c(paramBuilder);
    c = ((String)localObject);
    localObject = AdResponse.Builder.d(paramBuilder);
    d = ((String)localObject);
    localObject = AdResponse.Builder.e(paramBuilder);
    e = ((String)localObject);
    localObject = AdResponse.Builder.f(paramBuilder);
    f = ((String)localObject);
    localObject = AdResponse.Builder.g(paramBuilder);
    g = ((String)localObject);
    localObject = AdResponse.Builder.h(paramBuilder);
    h = ((String)localObject);
    localObject = AdResponse.Builder.i(paramBuilder);
    i = ((Integer)localObject);
    boolean bool = AdResponse.Builder.j(paramBuilder);
    j = bool;
    localObject = AdResponse.Builder.k(paramBuilder);
    k = ((String)localObject);
    localObject = AdResponse.Builder.l(paramBuilder);
    l = ((List)localObject);
    localObject = AdResponse.Builder.m(paramBuilder);
    m = ((String)localObject);
    localObject = AdResponse.Builder.n(paramBuilder);
    n = ((String)localObject);
    localObject = AdResponse.Builder.o(paramBuilder);
    o = ((List)localObject);
    localObject = AdResponse.Builder.p(paramBuilder);
    p = ((List)localObject);
    localObject = AdResponse.Builder.q(paramBuilder);
    q = ((List)localObject);
    localObject = AdResponse.Builder.r(paramBuilder);
    r = ((String)localObject);
    localObject = AdResponse.Builder.s(paramBuilder);
    s = ((Integer)localObject);
    localObject = AdResponse.Builder.t(paramBuilder);
    t = ((Integer)localObject);
    localObject = AdResponse.Builder.u(paramBuilder);
    u = ((Integer)localObject);
    localObject = AdResponse.Builder.v(paramBuilder);
    v = ((Integer)localObject);
    localObject = AdResponse.Builder.w(paramBuilder);
    w = ((String)localObject);
    localObject = AdResponse.Builder.x(paramBuilder);
    x = ((String)localObject);
    localObject = AdResponse.Builder.y(paramBuilder);
    y = ((JSONObject)localObject);
    localObject = AdResponse.Builder.z(paramBuilder);
    z = ((String)localObject);
    localObject = AdResponse.Builder.A(paramBuilder);
    A = ((MoPub.BrowserAgent)localObject);
    paramBuilder = AdResponse.Builder.B(paramBuilder);
    B = paramBuilder;
    long l1 = DateAndTime.now().getTime();
    C = l1;
  }
  
  public Integer getAdTimeoutMillis(int paramInt)
  {
    Integer localInteger = u;
    if (localInteger != null)
    {
      int i1 = localInteger.intValue();
      int i2 = 1000;
      if (i1 >= i2) {
        return u;
      }
    }
    return Integer.valueOf(paramInt);
  }
  
  public String getAdType()
  {
    return a;
  }
  
  public String getAdUnitId()
  {
    return b;
  }
  
  public List getAfterLoadFailUrls()
  {
    return q;
  }
  
  public List getAfterLoadSuccessUrls()
  {
    return p;
  }
  
  public List getAfterLoadUrls()
  {
    return o;
  }
  
  public String getBeforeLoadUrl()
  {
    return n;
  }
  
  public MoPub.BrowserAgent getBrowserAgent()
  {
    return A;
  }
  
  public String getClickTrackingUrl()
  {
    return k;
  }
  
  public String getCustomEventClassName()
  {
    return z;
  }
  
  public String getDspCreativeId()
  {
    return w;
  }
  
  public String getFailoverUrl()
  {
    return m;
  }
  
  public String getFullAdType()
  {
    return c;
  }
  
  public Integer getHeight()
  {
    return t;
  }
  
  public List getImpressionTrackingUrls()
  {
    return l;
  }
  
  public JSONObject getJsonBody()
  {
    return y;
  }
  
  public String getNetworkType()
  {
    return d;
  }
  
  public Integer getRefreshTimeMillis()
  {
    return v;
  }
  
  public String getRequestId()
  {
    return r;
  }
  
  public String getRewardedCurrencies()
  {
    return g;
  }
  
  public Integer getRewardedDuration()
  {
    return i;
  }
  
  public String getRewardedVideoCompletionUrl()
  {
    return h;
  }
  
  public String getRewardedVideoCurrencyAmount()
  {
    return f;
  }
  
  public String getRewardedVideoCurrencyName()
  {
    return e;
  }
  
  public Map getServerExtras()
  {
    TreeMap localTreeMap = new java/util/TreeMap;
    Map localMap = B;
    localTreeMap.<init>(localMap);
    return localTreeMap;
  }
  
  public String getStringBody()
  {
    return x;
  }
  
  public long getTimestamp()
  {
    return C;
  }
  
  public Integer getWidth()
  {
    return s;
  }
  
  public boolean hasJson()
  {
    JSONObject localJSONObject = y;
    return localJSONObject != null;
  }
  
  public boolean shouldRewardOnClick()
  {
    return j;
  }
  
  public AdResponse.Builder toBuilder()
  {
    AdResponse.Builder localBuilder = new com/mopub/network/AdResponse$Builder;
    localBuilder.<init>();
    Object localObject = a;
    localBuilder = localBuilder.setAdType((String)localObject);
    localObject = d;
    localBuilder = localBuilder.setNetworkType((String)localObject);
    localObject = e;
    localBuilder = localBuilder.setRewardedVideoCurrencyName((String)localObject);
    localObject = f;
    localBuilder = localBuilder.setRewardedVideoCurrencyAmount((String)localObject);
    localObject = g;
    localBuilder = localBuilder.setRewardedCurrencies((String)localObject);
    localObject = h;
    localBuilder = localBuilder.setRewardedVideoCompletionUrl((String)localObject);
    localObject = i;
    localBuilder = localBuilder.setRewardedDuration((Integer)localObject);
    boolean bool = j;
    localBuilder = localBuilder.setShouldRewardOnClick(bool);
    localObject = k;
    localBuilder = localBuilder.setClickTrackingUrl((String)localObject);
    localObject = l;
    localBuilder = localBuilder.setImpressionTrackingUrls((List)localObject);
    localObject = m;
    localBuilder = localBuilder.setFailoverUrl((String)localObject);
    localObject = n;
    localBuilder = localBuilder.setBeforeLoadUrl((String)localObject);
    localObject = o;
    localBuilder = localBuilder.setAfterLoadUrls((List)localObject);
    localObject = p;
    localBuilder = localBuilder.setAfterLoadSuccessUrls((List)localObject);
    localObject = q;
    localBuilder = localBuilder.setAfterLoadFailUrls((List)localObject);
    localObject = s;
    Integer localInteger = t;
    localBuilder = localBuilder.setDimensions((Integer)localObject, localInteger);
    localObject = u;
    localBuilder = localBuilder.setAdTimeoutDelayMilliseconds((Integer)localObject);
    localObject = v;
    localBuilder = localBuilder.setRefreshTimeMilliseconds((Integer)localObject);
    localObject = w;
    localBuilder = localBuilder.setDspCreativeId((String)localObject);
    localObject = x;
    localBuilder = localBuilder.setResponseBody((String)localObject);
    localObject = y;
    localBuilder = localBuilder.setJsonBody((JSONObject)localObject);
    localObject = z;
    localBuilder = localBuilder.setCustomEventClassName((String)localObject);
    localObject = A;
    localBuilder = localBuilder.setBrowserAgent((MoPub.BrowserAgent)localObject);
    localObject = B;
    return localBuilder.setServerExtras((Map)localObject);
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.AdResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */