package com.mopub.network;

import com.mopub.common.util.ResponseHeader;
import com.mopub.volley.Request;
import com.mopub.volley.toolbox.HttpResponse;
import com.mopub.volley.toolbox.HurlStack;
import com.mopub.volley.toolbox.HurlStack.UrlRewriter;
import java.util.Map;
import java.util.TreeMap;
import javax.net.ssl.SSLSocketFactory;

public class RequestQueueHttpStack
  extends HurlStack
{
  private final String a;
  
  public RequestQueueHttpStack(String paramString)
  {
    this(paramString, null);
  }
  
  public RequestQueueHttpStack(String paramString, HurlStack.UrlRewriter paramUrlRewriter)
  {
    this(paramString, paramUrlRewriter, null);
  }
  
  public RequestQueueHttpStack(String paramString, HurlStack.UrlRewriter paramUrlRewriter, SSLSocketFactory paramSSLSocketFactory)
  {
    super(paramUrlRewriter, paramSSLSocketFactory);
    a = paramString;
  }
  
  public HttpResponse executeRequest(Request paramRequest, Map paramMap)
  {
    if (paramMap != null)
    {
      boolean bool = paramMap.isEmpty();
      if (!bool) {}
    }
    else
    {
      paramMap = new java/util/TreeMap;
      paramMap.<init>();
    }
    String str1 = ResponseHeader.USER_AGENT.getKey();
    String str2 = a;
    paramMap.put(str1, str2);
    return super.executeRequest(paramRequest, paramMap);
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.RequestQueueHttpStack
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */