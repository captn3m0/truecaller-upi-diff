package com.mopub.network;

import android.os.Handler;
import com.mopub.common.Preconditions;
import com.mopub.volley.Cache;
import com.mopub.volley.Network;
import com.mopub.volley.Request;
import com.mopub.volley.RequestQueue;
import com.mopub.volley.RequestQueue.RequestFilter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class MoPubRequestQueue
  extends RequestQueue
{
  private final Map a;
  
  MoPubRequestQueue(Cache paramCache, Network paramNetwork)
  {
    super(paramCache, paramNetwork);
    paramCache = new java/util/HashMap;
    paramCache.<init>(10);
    a = paramCache;
  }
  
  public void addDelayedRequest(Request paramRequest, int paramInt)
  {
    Preconditions.checkNotNull(paramRequest);
    MoPubRequestQueue.a locala = new com/mopub/network/MoPubRequestQueue$a;
    locala.<init>(this, paramRequest, paramInt);
    Preconditions.checkNotNull(locala);
    Object localObject = a;
    paramInt = ((Map)localObject).containsKey(paramRequest);
    if (paramInt != 0) {
      cancel(paramRequest);
    }
    localObject = b;
    Runnable localRunnable = c;
    long l = a;
    ((Handler)localObject).postDelayed(localRunnable, l);
    a.put(paramRequest, locala);
  }
  
  public void cancel(Request paramRequest)
  {
    Preconditions.checkNotNull(paramRequest);
    MoPubRequestQueue.2 local2 = new com/mopub/network/MoPubRequestQueue$2;
    local2.<init>(this, paramRequest);
    cancelAll(local2);
  }
  
  public void cancelAll(RequestQueue.RequestFilter paramRequestFilter)
  {
    Preconditions.checkNotNull(paramRequestFilter);
    super.cancelAll(paramRequestFilter);
    Iterator localIterator = a.entrySet().iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject1 = (Map.Entry)localIterator.next();
      Object localObject2 = (Request)((Map.Entry)localObject1).getKey();
      boolean bool2 = paramRequestFilter.apply((Request)localObject2);
      if (bool2)
      {
        ((Request)((Map.Entry)localObject1).getKey()).cancel();
        localObject1 = (MoPubRequestQueue.a)((Map.Entry)localObject1).getValue();
        localObject2 = b;
        localObject1 = c;
        ((Handler)localObject2).removeCallbacks((Runnable)localObject1);
        localIterator.remove();
      }
    }
  }
  
  public void cancelAll(Object paramObject)
  {
    Preconditions.checkNotNull(paramObject);
    super.cancelAll(paramObject);
    MoPubRequestQueue.1 local1 = new com/mopub/network/MoPubRequestQueue$1;
    local1.<init>(this, paramObject);
    cancelAll(local1);
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.MoPubRequestQueue
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */