package com.mopub.network;

import android.os.Handler;
import com.mopub.volley.Request;

final class MoPubRequestQueue$a
{
  final int a;
  final Handler b;
  final Runnable c;
  
  MoPubRequestQueue$a(MoPubRequestQueue paramMoPubRequestQueue, Request paramRequest, int paramInt)
  {
    this(paramMoPubRequestQueue, paramRequest, paramInt, localHandler);
  }
  
  private MoPubRequestQueue$a(MoPubRequestQueue paramMoPubRequestQueue, Request paramRequest, int paramInt, Handler paramHandler)
  {
    a = paramInt;
    b = paramHandler;
    MoPubRequestQueue.a.1 local1 = new com/mopub/network/MoPubRequestQueue$a$1;
    local1.<init>(this, paramMoPubRequestQueue, paramRequest);
    c = local1;
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.MoPubRequestQueue.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */