package com.mopub.network;

import android.net.SSLCertificateSocketFactory;
import android.os.Build.VERSION;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Reflection.MethodBuilder;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class CustomSSLSocketFactory
  extends SSLSocketFactory
{
  private SSLSocketFactory a;
  
  private static void a(Socket paramSocket)
  {
    boolean bool = paramSocket instanceof SSLSocket;
    if (bool)
    {
      paramSocket = (SSLSocket)paramSocket;
      String[] arrayOfString = paramSocket.getSupportedProtocols();
      paramSocket.setEnabledProtocols(arrayOfString);
    }
  }
  
  public static CustomSSLSocketFactory getDefault(int paramInt)
  {
    CustomSSLSocketFactory localCustomSSLSocketFactory = new com/mopub/network/CustomSSLSocketFactory;
    localCustomSSLSocketFactory.<init>();
    SSLSocketFactory localSSLSocketFactory = SSLCertificateSocketFactory.getDefault(paramInt, null);
    a = localSSLSocketFactory;
    return localCustomSSLSocketFactory;
  }
  
  public Socket createSocket()
  {
    Object localObject = a;
    if (localObject != null)
    {
      localObject = ((SSLSocketFactory)localObject).createSocket();
      a((Socket)localObject);
      return (Socket)localObject;
    }
    localObject = new java/net/SocketException;
    ((SocketException)localObject).<init>("SSLSocketFactory was null. Unable to create socket.");
    throw ((Throwable)localObject);
  }
  
  public Socket createSocket(String paramString, int paramInt)
  {
    SSLSocketFactory localSSLSocketFactory = a;
    if (localSSLSocketFactory != null)
    {
      paramString = localSSLSocketFactory.createSocket(paramString, paramInt);
      a(paramString);
      return paramString;
    }
    paramString = new java/net/SocketException;
    paramString.<init>("SSLSocketFactory was null. Unable to create socket.");
    throw paramString;
  }
  
  public Socket createSocket(String paramString, int paramInt1, InetAddress paramInetAddress, int paramInt2)
  {
    SSLSocketFactory localSSLSocketFactory = a;
    if (localSSLSocketFactory != null)
    {
      paramString = localSSLSocketFactory.createSocket(paramString, paramInt1, paramInetAddress, paramInt2);
      a(paramString);
      return paramString;
    }
    paramString = new java/net/SocketException;
    paramString.<init>("SSLSocketFactory was null. Unable to create socket.");
    throw paramString;
  }
  
  public Socket createSocket(InetAddress paramInetAddress, int paramInt)
  {
    SSLSocketFactory localSSLSocketFactory = a;
    if (localSSLSocketFactory != null)
    {
      paramInetAddress = localSSLSocketFactory.createSocket(paramInetAddress, paramInt);
      a(paramInetAddress);
      return paramInetAddress;
    }
    paramInetAddress = new java/net/SocketException;
    paramInetAddress.<init>("SSLSocketFactory was null. Unable to create socket.");
    throw paramInetAddress;
  }
  
  public Socket createSocket(InetAddress paramInetAddress1, int paramInt1, InetAddress paramInetAddress2, int paramInt2)
  {
    SSLSocketFactory localSSLSocketFactory = a;
    if (localSSLSocketFactory != null)
    {
      paramInetAddress1 = localSSLSocketFactory.createSocket(paramInetAddress1, paramInt1, paramInetAddress2, paramInt2);
      a(paramInetAddress1);
      return paramInetAddress1;
    }
    paramInetAddress1 = new java/net/SocketException;
    paramInetAddress1.<init>("SSLSocketFactory was null. Unable to create socket.");
    throw paramInetAddress1;
  }
  
  public Socket createSocket(Socket paramSocket, String paramString, int paramInt, boolean paramBoolean)
  {
    Object localObject1 = a;
    if (localObject1 != null)
    {
      int i = Build.VERSION.SDK_INT;
      int j = 23;
      if (i < j)
      {
        if ((paramBoolean) && (paramSocket != null)) {
          paramSocket.close();
        }
        paramSocket = a;
        Object localObject2 = InetAddressUtils.getInetAddressByName(paramString);
        paramSocket = paramSocket.createSocket((InetAddress)localObject2, paramInt);
        a(paramSocket);
        Preconditions.checkNotNull(paramSocket);
        Object localObject3 = a;
        if (localObject3 != null)
        {
          paramBoolean = paramSocket instanceof SSLSocket;
          if (paramBoolean)
          {
            localObject2 = paramSocket;
            localObject2 = (SSLSocket)paramSocket;
            localObject3 = (SSLCertificateSocketFactory)localObject3;
            Preconditions.checkNotNull(localObject3);
            Preconditions.checkNotNull(localObject2);
            i = Build.VERSION.SDK_INT;
            j = 17;
            if (i >= j) {
              ((SSLCertificateSocketFactory)localObject3).setHostname((Socket)localObject2, paramString);
            } else {
              try
              {
                localObject3 = new com/mopub/common/util/Reflection$MethodBuilder;
                localObject1 = "setHostname";
                ((Reflection.MethodBuilder)localObject3).<init>(localObject2, (String)localObject1);
                localObject1 = String.class;
                localObject3 = ((Reflection.MethodBuilder)localObject3).addParam((Class)localObject1, paramString);
                ((Reflection.MethodBuilder)localObject3).execute();
              }
              catch (Exception localException)
              {
                localObject3 = "Unable to call setHostname() on the socket";
                MoPubLog.d((String)localObject3);
              }
            }
            Preconditions.checkNotNull(localObject2);
            ((SSLSocket)localObject2).startHandshake();
            localObject3 = HttpsURLConnection.getDefaultHostnameVerifier();
            localObject2 = ((SSLSocket)localObject2).getSession();
            boolean bool = ((HostnameVerifier)localObject3).verify(paramString, (SSLSession)localObject2);
            if (!bool)
            {
              paramSocket = new javax/net/ssl/SSLHandshakeException;
              paramSocket.<init>("Server Name Identification failed.");
              throw paramSocket;
            }
          }
          return (Socket)paramSocket;
        }
        paramSocket = new java/net/SocketException;
        paramSocket.<init>("SSLSocketFactory was null. Unable to create socket.");
        throw paramSocket;
      }
      paramSocket = a.createSocket(paramSocket, paramString, paramInt, paramBoolean);
      a(paramSocket);
      return paramSocket;
    }
    paramSocket = new java/net/SocketException;
    paramSocket.<init>("SSLSocketFactory was null. Unable to create socket.");
    throw paramSocket;
  }
  
  public String[] getDefaultCipherSuites()
  {
    SSLSocketFactory localSSLSocketFactory = a;
    if (localSSLSocketFactory == null) {
      return new String[0];
    }
    return localSSLSocketFactory.getDefaultCipherSuites();
  }
  
  public String[] getSupportedCipherSuites()
  {
    SSLSocketFactory localSSLSocketFactory = a;
    if (localSSLSocketFactory == null) {
      return new String[0];
    }
    return localSSLSocketFactory.getSupportedCipherSuites();
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.CustomSSLSocketFactory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */