package com.mopub.network;

import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.ResponseHeader;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HeaderUtils
{
  private static Integer a(String paramString)
  {
    try
    {
      int i = Integer.parseInt(paramString);
      return Integer.valueOf(i);
    }
    catch (Exception localException1)
    {
      NumberFormat localNumberFormat = NumberFormat.getInstance(Locale.US);
      boolean bool = true;
      localNumberFormat.setParseIntegerOnly(bool);
      try
      {
        paramString = paramString.trim();
        paramString = localNumberFormat.parse(paramString);
        int j = paramString.intValue();
        return Integer.valueOf(j);
      }
      catch (Exception localException2) {}
    }
    return null;
  }
  
  static List a(JSONObject paramJSONObject, ResponseHeader paramResponseHeader)
  {
    Preconditions.checkNotNull(paramJSONObject);
    Preconditions.checkNotNull(paramResponseHeader);
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    String str1 = paramResponseHeader.getKey();
    paramJSONObject = paramJSONObject.optJSONArray(str1);
    if (paramJSONObject == null) {
      return localArrayList;
    }
    int i = 0;
    str1 = null;
    for (;;)
    {
      int j = paramJSONObject.length();
      if (i >= j) {
        break;
      }
      try
      {
        localObject = paramJSONObject.getString(i);
        localArrayList.add(localObject);
      }
      catch (JSONException localJSONException)
      {
        Object localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>("Unable to parse item ");
        ((StringBuilder)localObject).append(i);
        ((StringBuilder)localObject).append(" from ");
        String str2 = paramResponseHeader.getKey();
        ((StringBuilder)localObject).append(str2);
        localObject = ((StringBuilder)localObject).toString();
        MoPubLog.d((String)localObject);
      }
      i += 1;
    }
    return localArrayList;
  }
  
  static String b(JSONObject paramJSONObject, ResponseHeader paramResponseHeader)
  {
    paramJSONObject = extractPercentHeader(paramJSONObject, paramResponseHeader);
    if (paramJSONObject != null) {
      return paramJSONObject.toString();
    }
    return null;
  }
  
  public static boolean extractBooleanHeader(JSONObject paramJSONObject, ResponseHeader paramResponseHeader, boolean paramBoolean)
  {
    paramJSONObject = extractHeader(paramJSONObject, paramResponseHeader);
    if (paramJSONObject == null) {
      return paramBoolean;
    }
    return paramJSONObject.equals("1");
  }
  
  public static String extractHeader(JSONObject paramJSONObject, ResponseHeader paramResponseHeader)
  {
    Preconditions.checkNotNull(paramResponseHeader);
    if (paramJSONObject == null) {
      return "";
    }
    paramResponseHeader = paramResponseHeader.getKey();
    return paramJSONObject.optString(paramResponseHeader);
  }
  
  public static Integer extractIntegerHeader(JSONObject paramJSONObject, ResponseHeader paramResponseHeader)
  {
    return a(extractHeader(paramJSONObject, paramResponseHeader));
  }
  
  public static Integer extractPercentHeader(JSONObject paramJSONObject, ResponseHeader paramResponseHeader)
  {
    paramJSONObject = extractHeader(paramJSONObject, paramResponseHeader);
    paramResponseHeader = null;
    if (paramJSONObject == null) {
      return null;
    }
    String str1 = "%";
    String str2 = "";
    paramJSONObject = a(paramJSONObject.replace(str1, str2));
    if (paramJSONObject != null)
    {
      int i = paramJSONObject.intValue();
      if (i >= 0)
      {
        i = paramJSONObject.intValue();
        int j = 100;
        if (i <= j) {
          return paramJSONObject;
        }
      }
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.HeaderUtils
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */