package com.mopub.network;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;
import com.mopub.volley.RequestQueue;
import com.mopub.volley.toolbox.ImageLoader;
import com.mopub.volley.toolbox.ImageLoader.ImageCache;
import com.mopub.volley.toolbox.ImageLoader.ImageContainer;
import com.mopub.volley.toolbox.ImageLoader.ImageListener;

public class MaxWidthImageLoader
  extends ImageLoader
{
  private final int c;
  
  public MaxWidthImageLoader(RequestQueue paramRequestQueue, Context paramContext, ImageLoader.ImageCache paramImageCache)
  {
    super(paramRequestQueue, paramImageCache);
    paramRequestQueue = ((WindowManager)paramContext.getSystemService("window")).getDefaultDisplay();
    paramContext = new android/graphics/Point;
    paramContext.<init>();
    paramRequestQueue.getSize(paramContext);
    int i = x;
    int j = y;
    i = Math.min(i, j);
    c = i;
  }
  
  public ImageLoader.ImageContainer get(String paramString, ImageLoader.ImageListener paramImageListener)
  {
    int i = c;
    return super.get(paramString, paramImageListener, i, 0);
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.MaxWidthImageLoader
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */