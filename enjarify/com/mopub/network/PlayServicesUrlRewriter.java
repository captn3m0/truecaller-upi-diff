package com.mopub.network;

import android.net.Uri;
import com.mopub.common.ClientMetadata;
import com.mopub.common.MoPub;
import com.mopub.common.privacy.AdvertisingId;
import com.mopub.common.privacy.MoPubIdentifier;
import com.mopub.volley.toolbox.HurlStack.UrlRewriter;

public class PlayServicesUrlRewriter
  implements HurlStack.UrlRewriter
{
  public static final String DO_NOT_TRACK_TEMPLATE = "mp_tmpl_do_not_track";
  public static final String UDID_TEMPLATE = "mp_tmpl_advertising_id";
  
  public String rewriteUrl(String paramString)
  {
    Object localObject = "mp_tmpl_advertising_id";
    boolean bool1 = paramString.contains((CharSequence)localObject);
    if (!bool1)
    {
      localObject = "mp_tmpl_do_not_track";
      bool1 = paramString.contains((CharSequence)localObject);
      if (!bool1) {
        return paramString;
      }
    }
    localObject = ClientMetadata.getInstance();
    if (localObject == null) {
      return paramString;
    }
    localObject = ((ClientMetadata)localObject).getMoPubIdentifier().getAdvertisingInfo();
    boolean bool2 = MoPub.canCollectPersonalInformation();
    String str1 = Uri.encode(((AdvertisingId)localObject).getIdWithPrefix(bool2));
    paramString = paramString.replace("mp_tmpl_advertising_id", str1);
    String str2 = "mp_tmpl_do_not_track";
    bool1 = ((AdvertisingId)localObject).isDoNotTrack();
    if (bool1) {
      localObject = "1";
    } else {
      localObject = "0";
    }
    return paramString.replace(str2, (CharSequence)localObject);
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.PlayServicesUrlRewriter
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */