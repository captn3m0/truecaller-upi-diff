package com.mopub.network;

import com.mopub.volley.Response.ErrorListener;

public abstract interface MultiAdRequest$Listener
  extends Response.ErrorListener
{
  public abstract void onSuccessResponse(MultiAdResponse paramMultiAdResponse);
}

/* Location:
 * Qualified Name:     com.mopub.network.MultiAdRequest.Listener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */