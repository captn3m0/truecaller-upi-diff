package com.mopub.network;

public enum MoPubNetworkError$Reason
{
  static
  {
    Object localObject = new com/mopub/network/MoPubNetworkError$Reason;
    ((Reason)localObject).<init>("WARMING_UP", 0);
    WARMING_UP = (Reason)localObject;
    localObject = new com/mopub/network/MoPubNetworkError$Reason;
    int i = 1;
    ((Reason)localObject).<init>("NO_FILL", i);
    NO_FILL = (Reason)localObject;
    localObject = new com/mopub/network/MoPubNetworkError$Reason;
    int j = 2;
    ((Reason)localObject).<init>("BAD_HEADER_DATA", j);
    BAD_HEADER_DATA = (Reason)localObject;
    localObject = new com/mopub/network/MoPubNetworkError$Reason;
    int k = 3;
    ((Reason)localObject).<init>("BAD_BODY", k);
    BAD_BODY = (Reason)localObject;
    localObject = new com/mopub/network/MoPubNetworkError$Reason;
    int m = 4;
    ((Reason)localObject).<init>("TRACKING_FAILURE", m);
    TRACKING_FAILURE = (Reason)localObject;
    localObject = new com/mopub/network/MoPubNetworkError$Reason;
    int n = 5;
    ((Reason)localObject).<init>("UNSPECIFIED", n);
    UNSPECIFIED = (Reason)localObject;
    localObject = new Reason[6];
    Reason localReason = WARMING_UP;
    localObject[0] = localReason;
    localReason = NO_FILL;
    localObject[i] = localReason;
    localReason = BAD_HEADER_DATA;
    localObject[j] = localReason;
    localReason = BAD_BODY;
    localObject[k] = localReason;
    localReason = TRACKING_FAILURE;
    localObject[m] = localReason;
    localReason = UNSPECIFIED;
    localObject[n] = localReason;
    a = (Reason[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.mopub.network.MoPubNetworkError.Reason
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */