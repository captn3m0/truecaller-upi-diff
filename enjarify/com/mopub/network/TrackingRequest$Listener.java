package com.mopub.network;

import com.mopub.volley.Response.ErrorListener;

public abstract interface TrackingRequest$Listener
  extends Response.ErrorListener
{
  public abstract void onResponse(String paramString);
}

/* Location:
 * Qualified Name:     com.mopub.network.TrackingRequest.Listener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */