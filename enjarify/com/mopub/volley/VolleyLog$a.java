package com.mopub.volley;

import android.os.SystemClock;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class VolleyLog$a
{
  public static final boolean ENABLED = VolleyLog.DEBUG;
  private final List a;
  private boolean b;
  
  VolleyLog$a()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    a = localArrayList;
    b = false;
  }
  
  public final void add(String paramString, long paramLong)
  {
    try
    {
      boolean bool = b;
      if (!bool)
      {
        List localList = a;
        VolleyLog.a.a locala = new com/mopub/volley/VolleyLog$a$a;
        long l = SystemClock.elapsedRealtime();
        locala.<init>(paramString, paramLong, l);
        localList.add(locala);
        return;
      }
      paramString = new java/lang/IllegalStateException;
      String str = "Marker added to finished log";
      paramString.<init>(str);
      throw paramString;
    }
    finally {}
  }
  
  protected final void finalize()
  {
    boolean bool = b;
    if (!bool)
    {
      finish("Request on the loose");
      String str = "Marker log finalized without finish() - uncaught exit point for request";
      Object[] arrayOfObject = new Object[0];
      VolleyLog.e(str, arrayOfObject);
    }
  }
  
  public final void finish(String paramString)
  {
    int i = 1;
    try
    {
      b = i;
      Object localObject1 = a;
      int j = ((List)localObject1).size();
      long l1 = 0L;
      long l2;
      if (j == 0)
      {
        l2 = l1;
      }
      else
      {
        localObject1 = a;
        localObject1 = ((List)localObject1).get(0);
        localObject1 = (VolleyLog.a.a)localObject1;
        l2 = time;
        localObject1 = a;
        List localList = a;
        k = localList.size() - i;
        localObject1 = ((List)localObject1).get(k);
        localObject1 = (VolleyLog.a.a)localObject1;
        long l3 = time;
        l2 = l3 - l2;
      }
      boolean bool1 = l2 < l1;
      if (!bool1) {
        return;
      }
      localObject1 = a;
      localObject1 = ((List)localObject1).get(0);
      localObject1 = (VolleyLog.a.a)localObject1;
      long l4 = time;
      Object localObject2 = "(%-4d ms) %s";
      int k = 2;
      Object localObject3 = new Object[k];
      Long localLong = Long.valueOf(l2);
      localObject3[0] = localLong;
      localObject3[i] = paramString;
      VolleyLog.d((String)localObject2, (Object[])localObject3);
      paramString = a;
      paramString = paramString.iterator();
      for (;;)
      {
        boolean bool2 = paramString.hasNext();
        if (!bool2) {
          break;
        }
        localObject2 = paramString.next();
        localObject2 = (VolleyLog.a.a)localObject2;
        l2 = time;
        localObject3 = "(+%-4d) [%2d] %s";
        int m = 3;
        Object[] arrayOfObject = new Object[m];
        l4 = l2 - l4;
        localObject1 = Long.valueOf(l4);
        arrayOfObject[0] = localObject1;
        l4 = thread;
        localObject1 = Long.valueOf(l4);
        arrayOfObject[i] = localObject1;
        localObject1 = name;
        arrayOfObject[k] = localObject1;
        VolleyLog.d((String)localObject3, arrayOfObject);
        l4 = l2;
      }
      return;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.VolleyLog.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */