package com.mopub.volley;

public enum Request$Priority
{
  static
  {
    Object localObject = new com/mopub/volley/Request$Priority;
    ((Priority)localObject).<init>("LOW", 0);
    LOW = (Priority)localObject;
    localObject = new com/mopub/volley/Request$Priority;
    int i = 1;
    ((Priority)localObject).<init>("NORMAL", i);
    NORMAL = (Priority)localObject;
    localObject = new com/mopub/volley/Request$Priority;
    int j = 2;
    ((Priority)localObject).<init>("HIGH", j);
    HIGH = (Priority)localObject;
    localObject = new com/mopub/volley/Request$Priority;
    int k = 3;
    ((Priority)localObject).<init>("IMMEDIATE", k);
    IMMEDIATE = (Priority)localObject;
    localObject = new Priority[4];
    Priority localPriority = LOW;
    localObject[0] = localPriority;
    localPriority = NORMAL;
    localObject[i] = localPriority;
    localPriority = HIGH;
    localObject[j] = localPriority;
    localPriority = IMMEDIATE;
    localObject[k] = localPriority;
    a = (Priority[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.Request.Priority
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */