package com.mopub.volley;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class NetworkResponse
{
  public final List allHeaders;
  public final byte[] data;
  public final Map headers;
  public final long networkTimeMs;
  public final boolean notModified;
  public final int statusCode;
  
  private NetworkResponse(int paramInt, byte[] paramArrayOfByte, Map paramMap, List paramList, boolean paramBoolean, long paramLong)
  {
    statusCode = paramInt;
    data = paramArrayOfByte;
    headers = paramMap;
    List localList;
    if (paramList == null)
    {
      paramInt = 0;
      localList = null;
      allHeaders = null;
    }
    else
    {
      localList = Collections.unmodifiableList(paramList);
      allHeaders = localList;
    }
    notModified = paramBoolean;
    networkTimeMs = paramLong;
  }
  
  public NetworkResponse(int paramInt, byte[] paramArrayOfByte, Map paramMap, boolean paramBoolean)
  {
    this(paramInt, paramArrayOfByte, paramMap, paramBoolean, 0L);
  }
  
  public NetworkResponse(int paramInt, byte[] paramArrayOfByte, Map paramMap, boolean paramBoolean, long paramLong)
  {
    this(paramInt, paramArrayOfByte, paramMap, localList, paramBoolean, paramLong);
  }
  
  public NetworkResponse(int paramInt, byte[] paramArrayOfByte, boolean paramBoolean, long paramLong, List paramList)
  {
    this(paramInt, paramArrayOfByte, localMap, paramList, paramBoolean, paramLong);
  }
  
  public NetworkResponse(byte[] paramArrayOfByte)
  {
    this(200, paramArrayOfByte, false, 0L, localList);
  }
  
  public NetworkResponse(byte[] paramArrayOfByte, Map paramMap)
  {
    this(200, paramArrayOfByte, paramMap, false, 0L);
  }
  
  private static List a(Map paramMap)
  {
    if (paramMap == null) {
      return null;
    }
    boolean bool1 = paramMap.isEmpty();
    if (bool1) {
      return Collections.emptyList();
    }
    ArrayList localArrayList = new java/util/ArrayList;
    int i = paramMap.size();
    localArrayList.<init>(i);
    paramMap = paramMap.entrySet().iterator();
    for (;;)
    {
      boolean bool2 = paramMap.hasNext();
      if (!bool2) {
        break;
      }
      Object localObject = (Map.Entry)paramMap.next();
      Header localHeader = new com/mopub/volley/Header;
      String str = (String)((Map.Entry)localObject).getKey();
      localObject = (String)((Map.Entry)localObject).getValue();
      localHeader.<init>(str, (String)localObject);
      localArrayList.add(localHeader);
    }
    return localArrayList;
  }
  
  private static Map a(List paramList)
  {
    if (paramList == null) {
      return null;
    }
    boolean bool1 = paramList.isEmpty();
    if (bool1) {
      return Collections.emptyMap();
    }
    TreeMap localTreeMap = new java/util/TreeMap;
    Object localObject = String.CASE_INSENSITIVE_ORDER;
    localTreeMap.<init>((Comparator)localObject);
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool2 = paramList.hasNext();
      if (!bool2) {
        break;
      }
      localObject = (Header)paramList.next();
      String str = ((Header)localObject).getName();
      localObject = ((Header)localObject).getValue();
      localTreeMap.put(str, localObject);
    }
    return localTreeMap;
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.NetworkResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */