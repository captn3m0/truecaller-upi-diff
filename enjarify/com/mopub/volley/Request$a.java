package com.mopub.volley;

abstract interface Request$a
{
  public abstract void onNoUsableResponseReceived(Request paramRequest);
  
  public abstract void onResponseReceived(Request paramRequest, Response paramResponse);
}

/* Location:
 * Qualified Name:     com.mopub.volley.Request.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */