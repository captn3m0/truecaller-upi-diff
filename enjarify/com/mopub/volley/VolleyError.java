package com.mopub.volley;

public class VolleyError
  extends Exception
{
  long a;
  public final NetworkResponse networkResponse;
  
  public VolleyError()
  {
    networkResponse = null;
  }
  
  public VolleyError(NetworkResponse paramNetworkResponse)
  {
    networkResponse = paramNetworkResponse;
  }
  
  public VolleyError(String paramString)
  {
    super(paramString);
    networkResponse = null;
  }
  
  public VolleyError(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
    networkResponse = null;
  }
  
  public VolleyError(Throwable paramThrowable)
  {
    super(paramThrowable);
    networkResponse = null;
  }
  
  public long getNetworkTimeMs()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.VolleyError
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */