package com.mopub.volley;

import android.os.Handler;
import java.util.concurrent.Executor;

public class ExecutorDelivery
  implements ResponseDelivery
{
  private final Executor a;
  
  public ExecutorDelivery(Handler paramHandler)
  {
    ExecutorDelivery.1 local1 = new com/mopub/volley/ExecutorDelivery$1;
    local1.<init>(this, paramHandler);
    a = local1;
  }
  
  public ExecutorDelivery(Executor paramExecutor)
  {
    a = paramExecutor;
  }
  
  public void postError(Request paramRequest, VolleyError paramVolleyError)
  {
    paramRequest.addMarker("post-error");
    paramVolleyError = Response.error(paramVolleyError);
    Executor localExecutor = a;
    ExecutorDelivery.a locala = new com/mopub/volley/ExecutorDelivery$a;
    locala.<init>(this, paramRequest, paramVolleyError, null);
    localExecutor.execute(locala);
  }
  
  public void postResponse(Request paramRequest, Response paramResponse)
  {
    postResponse(paramRequest, paramResponse, null);
  }
  
  public void postResponse(Request paramRequest, Response paramResponse, Runnable paramRunnable)
  {
    paramRequest.markDelivered();
    paramRequest.addMarker("post-response");
    Executor localExecutor = a;
    ExecutorDelivery.a locala = new com/mopub/volley/ExecutorDelivery$a;
    locala.<init>(this, paramRequest, paramResponse, paramRunnable);
    localExecutor.execute(locala);
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.ExecutorDelivery
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */