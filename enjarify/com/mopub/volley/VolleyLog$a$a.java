package com.mopub.volley;

final class VolleyLog$a$a
{
  public final String name;
  public final long thread;
  public final long time;
  
  public VolleyLog$a$a(String paramString, long paramLong1, long paramLong2)
  {
    name = paramString;
    thread = paramLong1;
    time = paramLong2;
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.VolleyLog.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */