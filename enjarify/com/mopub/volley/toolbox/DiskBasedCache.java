package com.mopub.volley.toolbox;

import android.os.SystemClock;
import com.mopub.volley.Cache;
import com.mopub.volley.Cache.Entry;
import com.mopub.volley.Header;
import com.mopub.volley.VolleyLog;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class DiskBasedCache
  implements Cache
{
  private final Map a;
  private long b;
  private final File c;
  private final int d;
  
  public DiskBasedCache(File paramFile)
  {
    this(paramFile, 5242880);
  }
  
  public DiskBasedCache(File paramFile, int paramInt)
  {
    LinkedHashMap localLinkedHashMap = new java/util/LinkedHashMap;
    localLinkedHashMap.<init>(16, 0.75F, true);
    a = localLinkedHashMap;
    b = 0L;
    c = paramFile;
    d = paramInt;
  }
  
  static int a(InputStream paramInputStream)
  {
    int i = c(paramInputStream) << 0 | 0x0;
    int j = c(paramInputStream) << 8;
    i |= j;
    j = c(paramInputStream) << 16;
    i |= j;
    return c(paramInputStream) << 24 | i;
  }
  
  private static InputStream a(File paramFile)
  {
    FileInputStream localFileInputStream = new java/io/FileInputStream;
    localFileInputStream.<init>(paramFile);
    return localFileInputStream;
  }
  
  static String a(DiskBasedCache.b paramb)
  {
    long l = b(paramb);
    paramb = a(paramb, l);
    String str = new java/lang/String;
    str.<init>(paramb, "UTF-8");
    return str;
  }
  
  private static String a(String paramString)
  {
    int i = paramString.length() / 2;
    String str = String.valueOf(paramString.substring(0, i).hashCode());
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(str);
    paramString = String.valueOf(paramString.substring(i).hashCode());
    localStringBuilder.append(paramString);
    return localStringBuilder.toString();
  }
  
  static void a(OutputStream paramOutputStream, int paramInt)
  {
    int i = paramInt >> 0 & 0xFF;
    paramOutputStream.write(i);
    i = paramInt >> 8 & 0xFF;
    paramOutputStream.write(i);
    i = paramInt >> 16 & 0xFF;
    paramOutputStream.write(i);
    paramInt = paramInt >> 24 & 0xFF;
    paramOutputStream.write(paramInt);
  }
  
  static void a(OutputStream paramOutputStream, long paramLong)
  {
    int i = (byte)(int)(paramLong >>> 0);
    paramOutputStream.write(i);
    i = (byte)(int)(paramLong >>> 8);
    paramOutputStream.write(i);
    i = (byte)(int)(paramLong >>> 16);
    paramOutputStream.write(i);
    i = (byte)(int)(paramLong >>> 24);
    paramOutputStream.write(i);
    i = (byte)(int)(paramLong >>> 32);
    paramOutputStream.write(i);
    i = (byte)(int)(paramLong >>> 40);
    paramOutputStream.write(i);
    i = (byte)(int)(paramLong >>> 48);
    paramOutputStream.write(i);
    int j = (byte)(int)(paramLong >>> 56);
    paramOutputStream.write(j);
  }
  
  static void a(OutputStream paramOutputStream, String paramString)
  {
    paramString = paramString.getBytes("UTF-8");
    long l = paramString.length;
    a(paramOutputStream, l);
    int i = paramString.length;
    paramOutputStream.write(paramString, 0, i);
  }
  
  private void a(String paramString, DiskBasedCache.a parama)
  {
    Object localObject = a;
    boolean bool = ((Map)localObject).containsKey(paramString);
    if (!bool)
    {
      long l1 = b;
      long l2 = a;
      l1 += l2;
      b = l1;
    }
    else
    {
      localObject = (DiskBasedCache.a)a.get(paramString);
      long l3 = b;
      long l4 = a;
      long l5 = a;
      l4 -= l5;
      l3 += l4;
      b = l3;
    }
    a.put(paramString, parama);
  }
  
  static void a(List paramList, OutputStream paramOutputStream)
  {
    if (paramList != null)
    {
      int i = paramList.size();
      a(paramOutputStream, i);
      paramList = paramList.iterator();
      for (;;)
      {
        boolean bool = paramList.hasNext();
        if (!bool) {
          break;
        }
        Object localObject = (Header)paramList.next();
        String str = ((Header)localObject).getName();
        a(paramOutputStream, str);
        localObject = ((Header)localObject).getValue();
        a(paramOutputStream, (String)localObject);
      }
      return;
    }
    a(paramOutputStream, 0);
  }
  
  private static byte[] a(DiskBasedCache.b paramb, long paramLong)
  {
    long l1 = paramb.a();
    long l2 = 0L;
    boolean bool1 = paramLong < l2;
    if (!bool1)
    {
      boolean bool2 = paramLong < l1;
      if (!bool2)
      {
        int i = (int)paramLong;
        long l3 = i;
        boolean bool3 = l3 < paramLong;
        if (!bool3)
        {
          localObject = new byte[i];
          DataInputStream localDataInputStream = new java/io/DataInputStream;
          localDataInputStream.<init>(paramb);
          localDataInputStream.readFully((byte[])localObject);
          return (byte[])localObject;
        }
      }
    }
    paramb = new java/io/IOException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("streamToBytes length=");
    localStringBuilder.append(paramLong);
    localStringBuilder.append(", maxLength=");
    localStringBuilder.append(l1);
    Object localObject = localStringBuilder.toString();
    paramb.<init>((String)localObject);
    throw paramb;
  }
  
  static long b(InputStream paramInputStream)
  {
    long l1 = c(paramInputStream);
    long l2 = 255L;
    l1 = (l1 & l2) << 0 | 0L;
    long l3 = (c(paramInputStream) & l2) << 8;
    l1 |= l3;
    l3 = (c(paramInputStream) & l2) << 16;
    l1 |= l3;
    l3 = (c(paramInputStream) & l2) << 24;
    l1 |= l3;
    l3 = (c(paramInputStream) & l2) << 32;
    l1 |= l3;
    l3 = (c(paramInputStream) & l2) << 40;
    l1 |= l3;
    l3 = (c(paramInputStream) & l2) << 48;
    l1 |= l3;
    l3 = c(paramInputStream);
    l2 = (l2 & l3) << 56;
    return l1 | l2;
  }
  
  static List b(DiskBasedCache.b paramb)
  {
    int i = a(paramb);
    if (i >= 0)
    {
      Object localObject;
      if (i == 0)
      {
        localObject = Collections.emptyList();
      }
      else
      {
        localObject = new java/util/ArrayList;
        ((ArrayList)localObject).<init>();
      }
      int j = 0;
      while (j < i)
      {
        String str1 = a(paramb).intern();
        String str2 = a(paramb).intern();
        Header localHeader = new com/mopub/volley/Header;
        localHeader.<init>(str1, str2);
        ((List)localObject).add(localHeader);
        j += 1;
      }
      return (List)localObject;
    }
    paramb = new java/io/IOException;
    String str3 = String.valueOf(i);
    str3 = "readHeaderList size=".concat(str3);
    paramb.<init>(str3);
    throw paramb;
  }
  
  private void b(String paramString)
  {
    Map localMap = a;
    paramString = (DiskBasedCache.a)localMap.remove(paramString);
    if (paramString != null)
    {
      long l1 = b;
      long l2 = a;
      l1 -= l2;
      b = l1;
    }
  }
  
  private static int c(InputStream paramInputStream)
  {
    int i = paramInputStream.read();
    int j = -1;
    if (i != j) {
      return i;
    }
    paramInputStream = new java/io/EOFException;
    paramInputStream.<init>();
    throw paramInputStream;
  }
  
  public void clear()
  {
    try
    {
      Object localObject1 = c;
      localObject1 = ((File)localObject1).listFiles();
      Object[] arrayOfObject = null;
      if (localObject1 != null)
      {
        int i = localObject1.length;
        int j = 0;
        while (j < i)
        {
          Object localObject3 = localObject1[j];
          ((File)localObject3).delete();
          j += 1;
        }
      }
      localObject1 = a;
      ((Map)localObject1).clear();
      long l = 0L;
      b = l;
      localObject1 = "Cache cleared.";
      arrayOfObject = new Object[0];
      VolleyLog.d((String)localObject1, arrayOfObject);
      return;
    }
    finally {}
  }
  
  /* Error */
  public Cache.Entry get(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 34	com/mopub/volley/toolbox/DiskBasedCache:a	Ljava/util/Map;
    //   6: astore_2
    //   7: aload_2
    //   8: aload_1
    //   9: invokeinterface 125 2 0
    //   14: astore_2
    //   15: aload_2
    //   16: checkcast 119	com/mopub/volley/toolbox/DiskBasedCache$a
    //   19: astore_2
    //   20: aload_2
    //   21: ifnonnull +7 -> 28
    //   24: aload_0
    //   25: monitorexit
    //   26: aconst_null
    //   27: areturn
    //   28: aload_0
    //   29: aload_1
    //   30: invokevirtual 256	com/mopub/volley/toolbox/DiskBasedCache:getFileForKey	(Ljava/lang/String;)Ljava/io/File;
    //   33: astore_3
    //   34: iconst_1
    //   35: istore 4
    //   37: iconst_2
    //   38: istore 5
    //   40: new 164	com/mopub/volley/toolbox/DiskBasedCache$b
    //   43: astore 6
    //   45: new 259	java/io/BufferedInputStream
    //   48: astore 7
    //   50: aload_3
    //   51: invokestatic 262	com/mopub/volley/toolbox/DiskBasedCache:a	(Ljava/io/File;)Ljava/io/InputStream;
    //   54: astore 8
    //   56: aload 7
    //   58: aload 8
    //   60: invokespecial 263	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   63: aload_3
    //   64: invokevirtual 265	java/io/File:length	()J
    //   67: lstore 9
    //   69: aload 6
    //   71: aload 7
    //   73: lload 9
    //   75: invokespecial 268	com/mopub/volley/toolbox/DiskBasedCache$b:<init>	(Ljava/io/InputStream;J)V
    //   78: aload 6
    //   80: invokestatic 271	com/mopub/volley/toolbox/DiskBasedCache$a:a	(Lcom/mopub/volley/toolbox/DiskBasedCache$b;)Lcom/mopub/volley/toolbox/DiskBasedCache$a;
    //   83: astore 7
    //   85: aload 7
    //   87: getfield 274	com/mopub/volley/toolbox/DiskBasedCache$a:b	Ljava/lang/String;
    //   90: astore 8
    //   92: aload_1
    //   93: aload 8
    //   95: invokestatic 280	android/text/TextUtils:equals	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    //   98: istore 11
    //   100: iload 11
    //   102: ifne +69 -> 171
    //   105: ldc_w 282
    //   108: astore_2
    //   109: iconst_3
    //   110: istore 11
    //   112: iload 11
    //   114: anewarray 4	java/lang/Object
    //   117: astore 8
    //   119: aload_3
    //   120: invokevirtual 286	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   123: astore 12
    //   125: aload 8
    //   127: iconst_0
    //   128: aload 12
    //   130: aastore
    //   131: aload 8
    //   133: iload 4
    //   135: aload_1
    //   136: aastore
    //   137: aload 7
    //   139: getfield 274	com/mopub/volley/toolbox/DiskBasedCache$a:b	Ljava/lang/String;
    //   142: astore 7
    //   144: aload 8
    //   146: iload 5
    //   148: aload 7
    //   150: aastore
    //   151: aload_2
    //   152: aload 8
    //   154: invokestatic 252	com/mopub/volley/VolleyLog:d	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   157: aload_0
    //   158: aload_1
    //   159: invokespecial 288	com/mopub/volley/toolbox/DiskBasedCache:b	(Ljava/lang/String;)V
    //   162: aload 6
    //   164: invokevirtual 291	com/mopub/volley/toolbox/DiskBasedCache$b:close	()V
    //   167: aload_0
    //   168: monitorexit
    //   169: aconst_null
    //   170: areturn
    //   171: aload 6
    //   173: invokevirtual 167	com/mopub/volley/toolbox/DiskBasedCache$b:a	()J
    //   176: lstore 13
    //   178: aload 6
    //   180: lload 13
    //   182: invokestatic 54	com/mopub/volley/toolbox/DiskBasedCache:a	(Lcom/mopub/volley/toolbox/DiskBasedCache$b;J)[B
    //   185: astore 7
    //   187: new 293	com/mopub/volley/Cache$Entry
    //   190: astore 8
    //   192: aload 8
    //   194: invokespecial 294	com/mopub/volley/Cache$Entry:<init>	()V
    //   197: aload 8
    //   199: aload 7
    //   201: putfield 298	com/mopub/volley/Cache$Entry:data	[B
    //   204: aload_2
    //   205: getfield 300	com/mopub/volley/toolbox/DiskBasedCache$a:c	Ljava/lang/String;
    //   208: astore 7
    //   210: aload 8
    //   212: aload 7
    //   214: putfield 303	com/mopub/volley/Cache$Entry:etag	Ljava/lang/String;
    //   217: aload_2
    //   218: getfield 305	com/mopub/volley/toolbox/DiskBasedCache$a:d	J
    //   221: lstore 15
    //   223: aload 8
    //   225: lload 15
    //   227: putfield 308	com/mopub/volley/Cache$Entry:serverDate	J
    //   230: aload_2
    //   231: getfield 311	com/mopub/volley/toolbox/DiskBasedCache$a:e	J
    //   234: lstore 15
    //   236: aload 8
    //   238: lload 15
    //   240: putfield 314	com/mopub/volley/Cache$Entry:lastModified	J
    //   243: aload_2
    //   244: getfield 317	com/mopub/volley/toolbox/DiskBasedCache$a:f	J
    //   247: lstore 15
    //   249: aload 8
    //   251: lload 15
    //   253: putfield 320	com/mopub/volley/Cache$Entry:ttl	J
    //   256: aload_2
    //   257: getfield 323	com/mopub/volley/toolbox/DiskBasedCache$a:g	J
    //   260: lstore 15
    //   262: aload 8
    //   264: lload 15
    //   266: putfield 326	com/mopub/volley/Cache$Entry:softTtl	J
    //   269: aload_2
    //   270: getfield 330	com/mopub/volley/toolbox/DiskBasedCache$a:h	Ljava/util/List;
    //   273: astore 7
    //   275: aload 7
    //   277: invokestatic 335	com/mopub/volley/toolbox/HttpHeaderParser:a	(Ljava/util/List;)Ljava/util/Map;
    //   280: astore 7
    //   282: aload 8
    //   284: aload 7
    //   286: putfield 338	com/mopub/volley/Cache$Entry:responseHeaders	Ljava/util/Map;
    //   289: aload_2
    //   290: getfield 330	com/mopub/volley/toolbox/DiskBasedCache$a:h	Ljava/util/List;
    //   293: astore_2
    //   294: aload_2
    //   295: invokestatic 342	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
    //   298: astore_2
    //   299: aload 8
    //   301: aload_2
    //   302: putfield 345	com/mopub/volley/Cache$Entry:allResponseHeaders	Ljava/util/List;
    //   305: aload 6
    //   307: invokevirtual 291	com/mopub/volley/toolbox/DiskBasedCache$b:close	()V
    //   310: aload_0
    //   311: monitorexit
    //   312: aload 8
    //   314: areturn
    //   315: astore_2
    //   316: aload 6
    //   318: invokevirtual 291	com/mopub/volley/toolbox/DiskBasedCache$b:close	()V
    //   321: aload_2
    //   322: athrow
    //   323: astore_2
    //   324: ldc_w 347
    //   327: astore 6
    //   329: iload 5
    //   331: anewarray 4	java/lang/Object
    //   334: astore 17
    //   336: aload_3
    //   337: invokevirtual 286	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   340: astore_3
    //   341: aload 17
    //   343: iconst_0
    //   344: aload_3
    //   345: aastore
    //   346: aload_2
    //   347: invokevirtual 348	java/io/IOException:toString	()Ljava/lang/String;
    //   350: astore_2
    //   351: aload 17
    //   353: iload 4
    //   355: aload_2
    //   356: aastore
    //   357: aload 6
    //   359: aload 17
    //   361: invokestatic 252	com/mopub/volley/VolleyLog:d	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   364: aload_0
    //   365: aload_1
    //   366: invokevirtual 350	com/mopub/volley/toolbox/DiskBasedCache:remove	(Ljava/lang/String;)V
    //   369: aload_0
    //   370: monitorexit
    //   371: aconst_null
    //   372: areturn
    //   373: astore_1
    //   374: aload_0
    //   375: monitorexit
    //   376: aload_1
    //   377: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	378	0	this	DiskBasedCache
    //   0	378	1	paramString	String
    //   6	296	2	localObject1	Object
    //   315	7	2	localObject2	Object
    //   323	24	2	localIOException	IOException
    //   350	6	2	str1	String
    //   33	312	3	localObject3	Object
    //   35	319	4	i	int
    //   38	292	5	j	int
    //   43	315	6	localObject4	Object
    //   48	237	7	localObject5	Object
    //   54	259	8	localObject6	Object
    //   67	7	9	l1	long
    //   98	3	11	bool	boolean
    //   110	3	11	k	int
    //   123	6	12	str2	String
    //   176	5	13	l2	long
    //   221	44	15	l3	long
    //   334	26	17	arrayOfObject	Object[]
    // Exception table:
    //   from	to	target	type
    //   78	83	315	finally
    //   85	90	315	finally
    //   93	98	315	finally
    //   112	117	315	finally
    //   119	123	315	finally
    //   128	131	315	finally
    //   135	137	315	finally
    //   137	142	315	finally
    //   148	151	315	finally
    //   152	157	315	finally
    //   158	162	315	finally
    //   171	176	315	finally
    //   180	185	315	finally
    //   187	190	315	finally
    //   192	197	315	finally
    //   199	204	315	finally
    //   204	208	315	finally
    //   212	217	315	finally
    //   217	221	315	finally
    //   225	230	315	finally
    //   230	234	315	finally
    //   238	243	315	finally
    //   243	247	315	finally
    //   251	256	315	finally
    //   256	260	315	finally
    //   264	269	315	finally
    //   269	273	315	finally
    //   275	280	315	finally
    //   284	289	315	finally
    //   289	293	315	finally
    //   294	298	315	finally
    //   301	305	315	finally
    //   40	43	323	java/io/IOException
    //   45	48	323	java/io/IOException
    //   50	54	323	java/io/IOException
    //   58	63	323	java/io/IOException
    //   63	67	323	java/io/IOException
    //   73	78	323	java/io/IOException
    //   162	167	323	java/io/IOException
    //   305	310	323	java/io/IOException
    //   316	321	323	java/io/IOException
    //   321	323	323	java/io/IOException
    //   2	6	373	finally
    //   8	14	373	finally
    //   15	19	373	finally
    //   29	33	373	finally
    //   40	43	373	finally
    //   45	48	373	finally
    //   50	54	373	finally
    //   58	63	373	finally
    //   63	67	373	finally
    //   73	78	373	finally
    //   162	167	373	finally
    //   305	310	373	finally
    //   316	321	373	finally
    //   321	323	373	finally
    //   329	334	373	finally
    //   336	340	373	finally
    //   344	346	373	finally
    //   346	350	373	finally
    //   355	357	373	finally
    //   359	364	373	finally
    //   365	369	373	finally
  }
  
  public File getFileForKey(String paramString)
  {
    File localFile1 = new java/io/File;
    File localFile2 = c;
    paramString = a(paramString);
    localFile1.<init>(localFile2, paramString);
    return localFile1;
  }
  
  public void initialize()
  {
    for (;;)
    {
      int i;
      Object localObject3;
      long l;
      DiskBasedCache.b localb;
      Object localObject4;
      InputStream localInputStream;
      try
      {
        Object localObject1 = c;
        boolean bool = ((File)localObject1).exists();
        i = 0;
        if (!bool)
        {
          localObject1 = c;
          bool = ((File)localObject1).mkdirs();
          if (!bool)
          {
            localObject1 = "Unable to create cache dir %s";
            j = 1;
            Object[] arrayOfObject = new Object[j];
            localObject3 = c;
            localObject3 = ((File)localObject3).getAbsolutePath();
            arrayOfObject[0] = localObject3;
            VolleyLog.e((String)localObject1, arrayOfObject);
          }
          return;
        }
        localObject1 = c;
        localObject1 = ((File)localObject1).listFiles();
        if (localObject1 == null) {
          return;
        }
        int j = localObject1.length;
        if (i < j) {
          localObject3 = localObject1[i];
        }
      }
      finally {}
      try
      {
        l = ((File)localObject3).length();
        localb = new com/mopub/volley/toolbox/DiskBasedCache$b;
        localObject4 = new java/io/BufferedInputStream;
        localInputStream = a((File)localObject3);
        ((BufferedInputStream)localObject4).<init>(localInputStream);
        localb.<init>((InputStream)localObject4, l);
        try
        {
          localObject4 = DiskBasedCache.a.a(localb);
          a = l;
          String str = b;
          a(str, (DiskBasedCache.a)localObject4);
        }
        finally
        {
          localb.close();
        }
      }
      catch (IOException localIOException)
      {
        continue;
      }
      ((File)localObject3).delete();
      i += 1;
    }
  }
  
  public void invalidate(String paramString, boolean paramBoolean)
  {
    try
    {
      Cache.Entry localEntry = get(paramString);
      if (localEntry != null)
      {
        long l = 0L;
        softTtl = l;
        if (paramBoolean) {
          ttl = l;
        }
        put(paramString, localEntry);
      }
      return;
    }
    finally {}
  }
  
  public void put(String paramString, Cache.Entry paramEntry)
  {
    DiskBasedCache localDiskBasedCache = this;
    Object localObject1 = paramString;
    Object localObject3 = paramEntry;
    for (;;)
    {
      Object localObject4;
      Object localObject5;
      Object[] arrayOfObject;
      int j;
      float f3;
      Object localObject10;
      boolean bool6;
      int n;
      boolean bool7;
      try
      {
        localObject4 = data;
        int i = localObject4.length;
        long l1 = b;
        long l2 = i;
        l1 += l2;
        i = d;
        long l3 = i;
        float f1 = 0.0F;
        String str = null;
        boolean bool2 = l1 < l3;
        if (!bool2)
        {
          boolean bool3 = VolleyLog.DEBUG;
          if (bool3)
          {
            localObject5 = "Pruning old cache entries.";
            arrayOfObject = new Object[0];
            VolleyLog.v((String)localObject5, arrayOfObject);
          }
          l1 = b;
          l3 = SystemClock.elapsedRealtime();
          Object localObject6 = a;
          localObject6 = ((Map)localObject6).entrySet();
          localObject6 = ((Set)localObject6).iterator();
          int k = 0;
          boolean bool4 = ((Iterator)localObject6).hasNext();
          int m = 2;
          long l4;
          if (bool4)
          {
            Object localObject7 = ((Iterator)localObject6).next();
            localObject7 = (Map.Entry)localObject7;
            localObject7 = ((Map.Entry)localObject7).getValue();
            localObject7 = (DiskBasedCache.a)localObject7;
            Object localObject8 = b;
            localObject8 = localDiskBasedCache.getFileForKey((String)localObject8);
            boolean bool5 = ((File)localObject8).delete();
            Object localObject9;
            if (bool5)
            {
              l4 = l1;
              l5 = b;
              localObject9 = localObject6;
              long l6 = a;
              l5 -= l6;
              b = l5;
            }
            else
            {
              l4 = l1;
              localObject9 = localObject6;
              localObject4 = "Could not delete cache entry for key=%s, filename=%s";
              localObject5 = new Object[m];
              str = b;
              arrayOfObject = null;
              localObject5[0] = str;
              str = b;
              str = a(str);
              bool2 = true;
              localObject5[bool2] = str;
              VolleyLog.d((String)localObject4, (Object[])localObject5);
            }
            ((Iterator)localObject9).remove();
            k += 1;
            long l5 = b + l2;
            float f2 = (float)l5;
            j = d;
            f3 = j;
            f1 = 0.9F;
            f3 *= f1;
            bool1 = f2 < f3;
            if (!bool1)
            {
              l1 = l4;
              localObject6 = localObject9;
              f1 = 0.0F;
              str = null;
            }
          }
          else
          {
            l4 = l1;
          }
          boolean bool1 = VolleyLog.DEBUG;
          if (bool1)
          {
            localObject4 = "pruned %d files, %d bytes, %d ms";
            j = 3;
            f3 = 4.2E-45F;
            localObject5 = new Object[j];
            localObject10 = Integer.valueOf(k);
            arrayOfObject = null;
            localObject5[0] = localObject10;
            l2 = b - l4;
            localObject10 = Long.valueOf(l2);
            bool6 = true;
            localObject5[bool6] = localObject10;
            l2 = SystemClock.elapsedRealtime() - l3;
            localObject10 = Long.valueOf(l2);
            localObject5[m] = localObject10;
            VolleyLog.v((String)localObject4, (Object[])localObject5);
          }
        }
        localObject4 = getFileForKey(paramString);
      }
      finally {}
      try
      {
        localObject5 = new java/io/BufferedOutputStream;
        localObject10 = new java/io/FileOutputStream;
        ((FileOutputStream)localObject10).<init>((File)localObject4);
        ((BufferedOutputStream)localObject5).<init>((OutputStream)localObject10);
        localObject10 = new com/mopub/volley/toolbox/DiskBasedCache$a;
        ((DiskBasedCache.a)localObject10).<init>((String)localObject1, (Cache.Entry)localObject3);
        bool6 = ((DiskBasedCache.a)localObject10).a((OutputStream)localObject5);
        if (bool6)
        {
          localObject3 = data;
          ((BufferedOutputStream)localObject5).write((byte[])localObject3);
          ((BufferedOutputStream)localObject5).close();
          localDiskBasedCache.a((String)localObject1, (DiskBasedCache.a)localObject10);
          return;
        }
        ((BufferedOutputStream)localObject5).close();
        localObject1 = "Failed to write header for %s";
        n = 1;
        localObject5 = new Object[n];
        localObject3 = ((File)localObject4).getAbsolutePath();
        arrayOfObject = null;
        localObject5[0] = localObject3;
        VolleyLog.d((String)localObject1, (Object[])localObject5);
        localObject1 = new java/io/IOException;
        ((IOException)localObject1).<init>();
        throw ((Throwable)localObject1);
      }
      catch (IOException localIOException) {}
    }
    bool7 = ((File)localObject4).delete();
    if (!bool7)
    {
      localObject1 = "Could not clean up file %s";
      n = 1;
      localObject3 = new Object[n];
      localObject4 = ((File)localObject4).getAbsolutePath();
      j = 0;
      f3 = 0.0F;
      localObject5 = null;
      localObject3[0] = localObject4;
      VolleyLog.d((String)localObject1, (Object[])localObject3);
    }
  }
  
  public void remove(String paramString)
  {
    try
    {
      Object localObject = getFileForKey(paramString);
      boolean bool = ((File)localObject).delete();
      b(paramString);
      if (!bool)
      {
        localObject = "Could not delete cache entry for key=%s, filename=%s";
        int i = 2;
        Object[] arrayOfObject = new Object[i];
        int j = 0;
        arrayOfObject[0] = paramString;
        j = 1;
        paramString = a(paramString);
        arrayOfObject[j] = paramString;
        VolleyLog.d((String)localObject, arrayOfObject);
      }
      return;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.DiskBasedCache
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */