package com.mopub.volley.toolbox;

import android.graphics.Bitmap;
import java.util.HashMap;
import java.util.LinkedList;

public class ImageLoader$ImageContainer
{
  private Bitmap b;
  private final ImageLoader.ImageListener c;
  private final String d;
  private final String e;
  
  public ImageLoader$ImageContainer(ImageLoader paramImageLoader, Bitmap paramBitmap, String paramString1, String paramString2, ImageLoader.ImageListener paramImageListener)
  {
    b = paramBitmap;
    e = paramString1;
    d = paramString2;
    c = paramImageListener;
  }
  
  public void cancelRequest()
  {
    Object localObject = c;
    if (localObject == null) {
      return;
    }
    localObject = ImageLoader.a(a);
    String str = d;
    localObject = (ImageLoader.a)((HashMap)localObject).get(str);
    if (localObject != null)
    {
      boolean bool = ((ImageLoader.a)localObject).removeContainerAndCancelIfNecessary(this);
      if (bool)
      {
        localObject = ImageLoader.a(a);
        str = d;
        ((HashMap)localObject).remove(str);
      }
      return;
    }
    localObject = ImageLoader.b(a);
    str = d;
    localObject = (ImageLoader.a)((HashMap)localObject).get(str);
    if (localObject != null)
    {
      ((ImageLoader.a)localObject).removeContainerAndCancelIfNecessary(this);
      localObject = b;
      int i = ((LinkedList)localObject).size();
      if (i == 0)
      {
        localObject = ImageLoader.b(a);
        str = d;
        ((HashMap)localObject).remove(str);
      }
    }
  }
  
  public Bitmap getBitmap()
  {
    return b;
  }
  
  public String getRequestUrl()
  {
    return e;
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.ImageLoader.ImageContainer
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */