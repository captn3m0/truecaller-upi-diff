package com.mopub.volley.toolbox;

import android.graphics.Bitmap;
import com.mopub.volley.Request;
import com.mopub.volley.VolleyError;
import java.util.LinkedList;

final class ImageLoader$a
{
  Bitmap a;
  final LinkedList b;
  private final Request d;
  private VolleyError e;
  
  public ImageLoader$a(ImageLoader paramImageLoader, Request paramRequest, ImageLoader.ImageContainer paramImageContainer)
  {
    paramImageLoader = new java/util/LinkedList;
    paramImageLoader.<init>();
    b = paramImageLoader;
    d = paramRequest;
    b.add(paramImageContainer);
  }
  
  public final void addContainer(ImageLoader.ImageContainer paramImageContainer)
  {
    b.add(paramImageContainer);
  }
  
  public final VolleyError getError()
  {
    return e;
  }
  
  public final boolean removeContainerAndCancelIfNecessary(ImageLoader.ImageContainer paramImageContainer)
  {
    LinkedList localLinkedList = b;
    localLinkedList.remove(paramImageContainer);
    paramImageContainer = b;
    int i = paramImageContainer.size();
    if (i == 0)
    {
      d.cancel();
      return true;
    }
    return false;
  }
  
  public final void setError(VolleyError paramVolleyError)
  {
    e = paramVolleyError;
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.ImageLoader.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */