package com.mopub.volley.toolbox;

import android.os.Handler;
import android.os.Looper;
import com.mopub.volley.Cache;
import com.mopub.volley.NetworkResponse;
import com.mopub.volley.Request;
import com.mopub.volley.Request.Priority;
import com.mopub.volley.Response;

public class ClearCacheRequest
  extends Request
{
  private final Cache a;
  private final Runnable b;
  
  public ClearCacheRequest(Cache paramCache, Runnable paramRunnable)
  {
    super(0, null, null);
    a = paramCache;
    b = paramRunnable;
  }
  
  public void deliverResponse(Object paramObject) {}
  
  public Request.Priority getPriority()
  {
    return Request.Priority.IMMEDIATE;
  }
  
  public boolean isCanceled()
  {
    a.clear();
    Object localObject1 = b;
    if (localObject1 != null)
    {
      localObject1 = new android/os/Handler;
      Object localObject2 = Looper.getMainLooper();
      ((Handler)localObject1).<init>((Looper)localObject2);
      localObject2 = b;
      ((Handler)localObject1).postAtFrontOfQueue((Runnable)localObject2);
    }
    return true;
  }
  
  public final Response parseNetworkResponse(NetworkResponse paramNetworkResponse)
  {
    return null;
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.ClearCacheRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */