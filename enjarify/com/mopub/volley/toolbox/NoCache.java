package com.mopub.volley.toolbox;

import com.mopub.volley.Cache;
import com.mopub.volley.Cache.Entry;

public class NoCache
  implements Cache
{
  public void clear() {}
  
  public Cache.Entry get(String paramString)
  {
    return null;
  }
  
  public void initialize() {}
  
  public void invalidate(String paramString, boolean paramBoolean) {}
  
  public void put(String paramString, Cache.Entry paramEntry) {}
  
  public void remove(String paramString) {}
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.NoCache
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */