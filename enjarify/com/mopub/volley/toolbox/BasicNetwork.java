package com.mopub.volley.toolbox;

import android.os.SystemClock;
import com.mopub.volley.AuthFailureError;
import com.mopub.volley.Cache.Entry;
import com.mopub.volley.ClientError;
import com.mopub.volley.Header;
import com.mopub.volley.Network;
import com.mopub.volley.NetworkError;
import com.mopub.volley.NetworkResponse;
import com.mopub.volley.NoConnectionError;
import com.mopub.volley.Request;
import com.mopub.volley.RetryPolicy;
import com.mopub.volley.ServerError;
import com.mopub.volley.TimeoutError;
import com.mopub.volley.VolleyError;
import com.mopub.volley.VolleyLog;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

public class BasicNetwork
  implements Network
{
  protected static final boolean a = VolleyLog.DEBUG;
  protected final HttpStack b;
  protected final ByteArrayPool c;
  private final BaseHttpStack d;
  
  public BasicNetwork(BaseHttpStack paramBaseHttpStack)
  {
    this(paramBaseHttpStack, localByteArrayPool);
  }
  
  public BasicNetwork(BaseHttpStack paramBaseHttpStack, ByteArrayPool paramByteArrayPool)
  {
    d = paramBaseHttpStack;
    b = paramBaseHttpStack;
    c = paramByteArrayPool;
  }
  
  public BasicNetwork(HttpStack paramHttpStack)
  {
    this(paramHttpStack, localByteArrayPool);
  }
  
  public BasicNetwork(HttpStack paramHttpStack, ByteArrayPool paramByteArrayPool)
  {
    b = paramHttpStack;
    a locala = new com/mopub/volley/toolbox/a;
    locala.<init>(paramHttpStack);
    d = locala;
    c = paramByteArrayPool;
  }
  
  private static List a(List paramList, Cache.Entry paramEntry)
  {
    TreeSet localTreeSet = new java/util/TreeSet;
    Object localObject1 = String.CASE_INSENSITIVE_ORDER;
    localTreeSet.<init>((Comparator)localObject1);
    boolean bool1 = paramList.isEmpty();
    boolean bool2;
    Object localObject2;
    if (!bool1)
    {
      localObject1 = paramList.iterator();
      for (;;)
      {
        bool2 = ((Iterator)localObject1).hasNext();
        if (!bool2) {
          break;
        }
        localObject2 = ((Header)((Iterator)localObject1).next()).getName();
        localTreeSet.add(localObject2);
      }
    }
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>(paramList);
    paramList = allResponseHeaders;
    boolean bool3;
    boolean bool4;
    if (paramList != null)
    {
      paramList = allResponseHeaders;
      bool3 = paramList.isEmpty();
      if (!bool3)
      {
        paramList = allResponseHeaders.iterator();
        for (;;)
        {
          bool4 = paramList.hasNext();
          if (!bool4) {
            break;
          }
          paramEntry = (Header)paramList.next();
          localObject2 = paramEntry.getName();
          bool2 = localTreeSet.contains(localObject2);
          if (!bool2) {
            ((List)localObject1).add(paramEntry);
          }
        }
      }
    }
    else
    {
      paramList = responseHeaders;
      bool3 = paramList.isEmpty();
      if (!bool3)
      {
        paramList = responseHeaders.entrySet().iterator();
        for (;;)
        {
          bool4 = paramList.hasNext();
          if (!bool4) {
            break;
          }
          paramEntry = (Map.Entry)paramList.next();
          localObject2 = paramEntry.getKey();
          bool2 = localTreeSet.contains(localObject2);
          if (!bool2)
          {
            localObject2 = new com/mopub/volley/Header;
            String str = (String)paramEntry.getKey();
            paramEntry = (String)paramEntry.getValue();
            ((Header)localObject2).<init>(str, paramEntry);
            ((List)localObject1).add(localObject2);
          }
        }
      }
    }
    return (List)localObject1;
  }
  
  private static void a(String paramString, Request paramRequest, VolleyError paramVolleyError)
  {
    Object localObject = paramRequest.getRetryPolicy();
    int i = paramRequest.getTimeoutMs();
    int j = 1;
    int k = 2;
    try
    {
      ((RetryPolicy)localObject).retry(paramVolleyError);
      localObject = new Object[k];
      localObject[0] = paramString;
      paramString = Integer.valueOf(i);
      localObject[j] = paramString;
      paramString = String.format("%s-retry [timeout=%s]", (Object[])localObject);
      paramRequest.addMarker(paramString);
      return;
    }
    catch (VolleyError paramVolleyError)
    {
      localObject = new Object[k];
      localObject[0] = paramString;
      paramString = Integer.valueOf(i);
      localObject[j] = paramString;
      paramString = String.format("%s-timeout-giveup [timeout=%s]", (Object[])localObject);
      paramRequest.addMarker(paramString);
      throw paramVolleyError;
    }
  }
  
  private byte[] a(InputStream paramInputStream, int paramInt)
  {
    PoolingByteArrayOutputStream localPoolingByteArrayOutputStream = new com/mopub/volley/toolbox/PoolingByteArrayOutputStream;
    Object localObject1 = c;
    localPoolingByteArrayOutputStream.<init>((ByteArrayPool)localObject1, paramInt);
    paramInt = 0;
    Object localObject2 = null;
    localObject1 = null;
    if (paramInputStream != null) {
      try
      {
        Object localObject3 = c;
        int i = 1024;
        localObject1 = ((ByteArrayPool)localObject3).getBuf(i);
        for (;;)
        {
          int j = paramInputStream.read((byte[])localObject1);
          i = -1;
          if (j == i) {
            break;
          }
          localPoolingByteArrayOutputStream.write((byte[])localObject1, 0, j);
        }
        localObject3 = localPoolingByteArrayOutputStream.toByteArray();
        if (paramInputStream != null) {
          try
          {
            paramInputStream.close();
          }
          catch (IOException localIOException1)
          {
            paramInputStream = "Error occurred when closing InputStream";
            localObject2 = new Object[0];
            VolleyLog.v(paramInputStream, (Object[])localObject2);
          }
        }
        c.returnBuf((byte[])localObject1);
        localPoolingByteArrayOutputStream.close();
        return (byte[])localObject3;
      }
      finally
      {
        break label146;
      }
    }
    ServerError localServerError = new com/mopub/volley/ServerError;
    localServerError.<init>();
    throw localServerError;
    label146:
    if (paramInputStream != null) {
      try
      {
        paramInputStream.close();
      }
      catch (IOException localIOException2)
      {
        paramInputStream = new Object[0];
        localObject2 = "Error occurred when closing InputStream";
        VolleyLog.v((String)localObject2, paramInputStream);
      }
    }
    c.returnBuf((byte[])localObject1);
    localPoolingByteArrayOutputStream.close();
    throw localServerError;
  }
  
  public NetworkResponse performRequest(Request paramRequest)
  {
    BasicNetwork localBasicNetwork = this;
    Object localObject1 = paramRequest;
    long l1 = SystemClock.elapsedRealtime();
    Object localObject2;
    for (;;)
    {
      localObject2 = Collections.emptyList();
      int i = 1;
      int j = 2;
      Object localObject3 = null;
      try
      {
        Object localObject6;
        Object localObject7;
        int k;
        int i1;
        Object localObject8;
        long l7;
        Object localObject9;
        Object localObject10;
        try
        {
          Object localObject4 = paramRequest.getCacheEntry();
          long l2;
          long l3;
          boolean bool1;
          long l4;
          if (localObject4 == null)
          {
            localObject4 = Collections.emptyMap();
          }
          else
          {
            localObject6 = new java/util/HashMap;
            ((HashMap)localObject6).<init>();
            localObject7 = etag;
            if (localObject7 != null)
            {
              localObject7 = "If-None-Match";
              String str2 = etag;
              ((Map)localObject6).put(localObject7, str2);
            }
            l2 = lastModified;
            l3 = 0L;
            bool1 = l2 < l3;
            if (bool1)
            {
              localObject7 = "If-Modified-Since";
              l4 = lastModified;
              localObject4 = HttpHeaderParser.a(l4);
              ((Map)localObject6).put(localObject7, localObject4);
            }
            localObject4 = localObject6;
          }
          localObject6 = d;
          localObject6 = ((BaseHttpStack)localObject6).executeRequest((Request)localObject1, (Map)localObject4);
          int i4;
          try
          {
            k = ((HttpResponse)localObject6).getStatusCode();
            localObject2 = ((HttpResponse)localObject6).getHeaders();
            int m = 304;
            if (k == m)
            {
              localObject4 = paramRequest.getCacheEntry();
              if (localObject4 == null)
              {
                localObject4 = new com/mopub/volley/NetworkResponse;
                i1 = 304;
                bool1 = false;
                boolean bool4 = true;
                l2 = SystemClock.elapsedRealtime();
                long l5 = l2 - l1;
                localObject8 = localObject4;
                ((NetworkResponse)localObject4).<init>(i1, null, bool4, l5, (List)localObject2);
                return (NetworkResponse)localObject4;
              }
              List localList = a((List)localObject2, (Cache.Entry)localObject4);
              localObject7 = new com/mopub/volley/NetworkResponse;
              int i2 = 304;
              localObject4 = data;
              boolean bool5 = true;
              l4 = SystemClock.elapsedRealtime();
              long l6 = l4 - l1;
              ((NetworkResponse)localObject7).<init>(i2, (byte[])localObject4, bool5, l6, localList);
              return (NetworkResponse)localObject7;
            }
            localObject4 = ((HttpResponse)localObject6).getContent();
            int i3;
            if (localObject4 != null)
            {
              i3 = ((HttpResponse)localObject6).getContentLength();
              localObject4 = localBasicNetwork.a((InputStream)localObject4, i3);
              localObject3 = localObject4;
            }
            else
            {
              localObject4 = new byte[0];
              localObject3 = localObject4;
            }
            l3 = SystemClock.elapsedRealtime() - l1;
            boolean bool2 = a;
            if (!bool2)
            {
              l7 = 3000L;
              bool2 = l3 < l7;
              if (!bool2) {}
            }
            else
            {
              localObject4 = "HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]";
              i3 = 5;
              localObject7 = new Object[i3];
              localObject7[0] = localObject1;
              localObject8 = Long.valueOf(l3);
              localObject7[i] = localObject8;
              if (localObject3 != null)
              {
                i4 = localObject3.length;
                localObject8 = Integer.valueOf(i4);
              }
              else
              {
                localObject8 = "null";
              }
              localObject7[j] = localObject8;
              i4 = 3;
              localObject9 = Integer.valueOf(k);
              localObject7[i4] = localObject9;
              i4 = 4;
              localObject9 = paramRequest.getRetryPolicy();
              i1 = ((RetryPolicy)localObject9).getCurrentRetryCount();
              localObject9 = Integer.valueOf(i1);
              localObject7[i4] = localObject9;
              VolleyLog.d((String)localObject4, (Object[])localObject7);
            }
            n = 200;
            if (k >= n)
            {
              n = 299;
              if (k <= n)
              {
                localObject4 = new com/mopub/volley/NetworkResponse;
                i1 = 0;
                localObject9 = null;
                l7 = SystemClock.elapsedRealtime() - l1;
                localObject7 = localObject4;
                localObject8 = localObject3;
                localObject10 = localObject2;
                ((NetworkResponse)localObject4).<init>(k, (byte[])localObject3, false, l7, (List)localObject2);
                return (NetworkResponse)localObject4;
              }
            }
            localObject4 = new java/io/IOException;
            ((IOException)localObject4).<init>();
            throw ((Throwable)localObject4);
          }
          catch (IOException localIOException1)
          {
            localObject10 = localObject2;
            localObject8 = localObject3;
          }
          if (localObject6 == null) {
            break label942;
          }
        }
        catch (IOException localIOException2)
        {
          localObject10 = localObject2;
          localObject6 = null;
          i4 = 0;
          localObject8 = null;
        }
        int n = ((HttpResponse)localObject6).getStatusCode();
        localObject2 = "Unexpected response code %d for %s";
        Object[] arrayOfObject = new Object[j];
        localObject3 = Integer.valueOf(n);
        arrayOfObject[0] = localObject3;
        localObject3 = paramRequest.getUrl();
        arrayOfObject[i] = localObject3;
        VolleyLog.e((String)localObject2, arrayOfObject);
        if (localObject8 != null)
        {
          localObject2 = new com/mopub/volley/NetworkResponse;
          i1 = 0;
          localObject9 = null;
          long l8 = SystemClock.elapsedRealtime();
          l7 = l8 - l1;
          localObject7 = localObject2;
          k = n;
          ((NetworkResponse)localObject2).<init>(n, (byte[])localObject8, false, l7, (List)localObject10);
          i = 401;
          if (n != i)
          {
            i = 403;
            if (n != i)
            {
              i = 400;
              if (n >= i)
              {
                i = 499;
                if (n <= i)
                {
                  localObject5 = new com/mopub/volley/ClientError;
                  ((ClientError)localObject5).<init>((NetworkResponse)localObject2);
                  throw ((Throwable)localObject5);
                }
              }
              i = 500;
              if (n >= i)
              {
                i = 599;
                if (n <= i)
                {
                  boolean bool3 = paramRequest.shouldRetryServerErrors();
                  if (bool3)
                  {
                    localObject5 = "server";
                    localObject11 = new com/mopub/volley/ServerError;
                    ((ServerError)localObject11).<init>((NetworkResponse)localObject2);
                    a((String)localObject5, (Request)localObject1, (VolleyError)localObject11);
                    continue;
                  }
                  localObject5 = new com/mopub/volley/ServerError;
                  ((ServerError)localObject5).<init>((NetworkResponse)localObject2);
                  throw ((Throwable)localObject5);
                }
              }
              localObject5 = new com/mopub/volley/ServerError;
              ((ServerError)localObject5).<init>((NetworkResponse)localObject2);
              throw ((Throwable)localObject5);
            }
          }
          localObject5 = "auth";
          Object localObject11 = new com/mopub/volley/AuthFailureError;
          ((AuthFailureError)localObject11).<init>((NetworkResponse)localObject2);
          a((String)localObject5, (Request)localObject1, (VolleyError)localObject11);
          continue;
        }
        localObject5 = "network";
        localObject2 = new com/mopub/volley/NetworkError;
        ((NetworkError)localObject2).<init>();
        a((String)localObject5, (Request)localObject1, (VolleyError)localObject2);
      }
      catch (MalformedURLException localMalformedURLException)
      {
        Object localObject5;
        RuntimeException localRuntimeException = new java/lang/RuntimeException;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>("Bad URL ");
        localObject1 = paramRequest.getUrl();
        localStringBuilder.append((String)localObject1);
        localObject1 = localStringBuilder.toString();
        localRuntimeException.<init>((String)localObject1, localMalformedURLException);
        throw localRuntimeException;
      }
      catch (SocketTimeoutException localSocketTimeoutException)
      {
        label942:
        String str1 = "socket";
        localObject2 = new com/mopub/volley/TimeoutError;
        ((TimeoutError)localObject2).<init>();
        a(str1, (Request)localObject1, (VolleyError)localObject2);
      }
      localObject1 = new com/mopub/volley/NoConnectionError;
      ((NoConnectionError)localObject1).<init>((Throwable)localObject5);
      throw ((Throwable)localObject1);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.BasicNetwork
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */