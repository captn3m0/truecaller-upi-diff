package com.mopub.volley.toolbox;

import com.mopub.volley.Cache.Entry;
import com.mopub.volley.Header;
import com.mopub.volley.NetworkResponse;
import com.mopub.volley.VolleyLog;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;

public class HttpHeaderParser
{
  static String a(long paramLong)
  {
    SimpleDateFormat localSimpleDateFormat = a();
    Date localDate = new java/util/Date;
    localDate.<init>(paramLong);
    return localSimpleDateFormat.format(localDate);
  }
  
  private static SimpleDateFormat a()
  {
    SimpleDateFormat localSimpleDateFormat = new java/text/SimpleDateFormat;
    Locale localLocale = Locale.US;
    localSimpleDateFormat.<init>("EEE, dd MMM yyyy HH:mm:ss zzz", localLocale);
    TimeZone localTimeZone = TimeZone.getTimeZone("GMT");
    localSimpleDateFormat.setTimeZone(localTimeZone);
    return localSimpleDateFormat;
  }
  
  static List a(Map paramMap)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    int i = paramMap.size();
    localArrayList.<init>(i);
    paramMap = paramMap.entrySet().iterator();
    for (;;)
    {
      boolean bool = paramMap.hasNext();
      if (!bool) {
        break;
      }
      Object localObject = (Map.Entry)paramMap.next();
      Header localHeader = new com/mopub/volley/Header;
      String str = (String)((Map.Entry)localObject).getKey();
      localObject = (String)((Map.Entry)localObject).getValue();
      localHeader.<init>(str, (String)localObject);
      localArrayList.add(localHeader);
    }
    return localArrayList;
  }
  
  static Map a(List paramList)
  {
    TreeMap localTreeMap = new java/util/TreeMap;
    Object localObject = String.CASE_INSENSITIVE_ORDER;
    localTreeMap.<init>((Comparator)localObject);
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      localObject = (Header)paramList.next();
      String str = ((Header)localObject).getName();
      localObject = ((Header)localObject).getValue();
      localTreeMap.put(str, localObject);
    }
    return localTreeMap;
  }
  
  public static Cache.Entry parseCacheHeaders(NetworkResponse paramNetworkResponse)
  {
    Object localObject1 = paramNetworkResponse;
    long l1 = System.currentTimeMillis();
    Map localMap = headers;
    Object localObject2 = (String)localMap.get("Date");
    long l2;
    if (localObject2 != null) {
      l2 = parseDateAsEpoch((String)localObject2);
    } else {
      l2 = 0L;
    }
    localObject2 = (String)localMap.get("Cache-Control");
    int i = 0;
    byte[] arrayOfByte = null;
    long l3;
    long l4;
    if (localObject2 != null)
    {
      String str1 = ",";
      localObject2 = ((String)localObject2).split(str1);
      l3 = 0L;
      int j = 0;
      l4 = 0L;
      for (;;)
      {
        int k = localObject2.length;
        if (i >= k) {
          break label284;
        }
        localObject3 = localObject2[i].trim();
        String str2 = "no-cache";
        boolean bool1 = ((String)localObject3).equals(str2);
        if (bool1) {
          break;
        }
        str2 = "no-store";
        bool1 = ((String)localObject3).equals(str2);
        if (bool1) {
          break;
        }
        str2 = "max-age=";
        bool1 = ((String)localObject3).startsWith(str2);
        int m;
        if (bool1) {
          m = 8;
        }
        try
        {
          str2 = ((String)localObject3).substring(m);
          l3 = Long.parseLong(str2);
        }
        catch (Exception localException)
        {
          boolean bool2;
          int n;
          boolean bool3;
          int i1;
          long l5;
          long l6;
          long l7;
          long l8;
          boolean bool4;
          for (;;) {}
        }
        str2 = "stale-while-revalidate=";
        bool2 = ((String)localObject3).startsWith(str2);
        if (bool2)
        {
          n = 23;
          str2 = ((String)localObject3).substring(n);
          l4 = Long.parseLong(str2);
        }
        else
        {
          str2 = "must-revalidate";
          bool3 = ((String)localObject3).equals(str2);
          if (!bool3)
          {
            str2 = "proxy-revalidate";
            bool3 = ((String)localObject3).equals(str2);
            if (!bool3) {}
          }
          else
          {
            j = 1;
          }
        }
        i += 1;
      }
      return null;
      label284:
      i = j;
      i1 = 1;
    }
    else
    {
      l3 = 0L;
      l4 = 0L;
      i1 = 0;
    }
    localObject2 = (String)localMap.get("Expires");
    if (localObject2 != null) {
      l5 = parseDateAsEpoch((String)localObject2);
    } else {
      l5 = 0L;
    }
    localObject2 = (String)localMap.get("Last-Modified");
    if (localObject2 != null)
    {
      l6 = parseDateAsEpoch((String)localObject2);
      l7 = l6;
    }
    else
    {
      l7 = 0L;
    }
    localObject2 = (String)localMap.get("ETag");
    if (i1 != 0)
    {
      l5 = 1000L;
      l3 *= l5;
      l1 += l3;
      if (i != 0)
      {
        l5 = l1;
      }
      else
      {
        Long.signum(l4);
        l4 = l4 * l5 + l1;
        l5 = l4;
      }
    }
    else
    {
      l8 = 0L;
      bool4 = l2 < l8;
      if (bool4)
      {
        bool4 = l5 < l2;
        if (!bool4)
        {
          l5 = l5 - l2 + l1;
          l1 = l5;
          break label489;
        }
      }
      l1 = l8;
      l5 = l8;
    }
    label489:
    Object localObject3 = new com/mopub/volley/Cache$Entry;
    ((Cache.Entry)localObject3).<init>();
    arrayOfByte = data;
    data = arrayOfByte;
    etag = ((String)localObject2);
    softTtl = l1;
    ttl = l5;
    serverDate = l2;
    l1 = l7;
    lastModified = l7;
    responseHeaders = localMap;
    localObject1 = allHeaders;
    allResponseHeaders = ((List)localObject1);
    return (Cache.Entry)localObject3;
  }
  
  public static String parseCharset(Map paramMap)
  {
    return parseCharset(paramMap, "ISO-8859-1");
  }
  
  public static String parseCharset(Map paramMap, String paramString)
  {
    String str1 = "Content-Type";
    paramMap = (String)paramMap.get(str1);
    if (paramMap != null)
    {
      str1 = ";";
      paramMap = paramMap.split(str1);
      int i = 1;
      int j = 1;
      for (;;)
      {
        int k = paramMap.length;
        if (j >= k) {
          break;
        }
        Object localObject = paramMap[j].trim();
        String str2 = "=";
        localObject = ((String)localObject).split(str2);
        int m = localObject.length;
        int n = 2;
        if (m == n)
        {
          str2 = localObject[0];
          String str3 = "charset";
          boolean bool = str2.equals(str3);
          if (bool) {
            return localObject[i];
          }
        }
        j += 1;
      }
    }
    return paramString;
  }
  
  public static long parseDateAsEpoch(String paramString)
  {
    try
    {
      Object localObject = a();
      localObject = ((SimpleDateFormat)localObject).parse(paramString);
      return ((Date)localObject).getTime();
    }
    catch (ParseException localParseException)
    {
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = paramString;
      VolleyLog.e(localParseException, "Unable to parse dateStr: %s, falling back to 0", arrayOfObject);
    }
    return 0L;
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.HttpHeaderParser
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */