package com.mopub.volley.toolbox;

import android.graphics.Bitmap;

public abstract interface ImageLoader$ImageCache
{
  public abstract Bitmap getBitmap(String paramString);
  
  public abstract void putBitmap(String paramString, Bitmap paramBitmap);
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.ImageLoader.ImageCache
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */