package com.mopub.volley.toolbox;

import com.mopub.volley.Response.ErrorListener;

public abstract interface ImageLoader$ImageListener
  extends Response.ErrorListener
{
  public abstract void onResponse(ImageLoader.ImageContainer paramImageContainer, boolean paramBoolean);
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.ImageLoader.ImageListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */