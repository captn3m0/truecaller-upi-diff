package com.mopub.volley.toolbox;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Handler;
import android.os.Looper;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.mopub.volley.Request;
import com.mopub.volley.RequestQueue;
import com.mopub.volley.Response.ErrorListener;
import java.util.HashMap;

public class ImageLoader
{
  final ImageLoader.ImageCache a;
  final HashMap b;
  private final RequestQueue c;
  private int d = 100;
  private final HashMap e;
  private final Handler f;
  private Runnable g;
  
  public ImageLoader(RequestQueue paramRequestQueue, ImageLoader.ImageCache paramImageCache)
  {
    Object localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    b = ((HashMap)localObject);
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    e = ((HashMap)localObject);
    localObject = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    ((Handler)localObject).<init>(localLooper);
    f = ((Handler)localObject);
    c = paramRequestQueue;
    a = paramImageCache;
  }
  
  private static String a(String paramString, int paramInt1, int paramInt2, ImageView.ScaleType paramScaleType)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    int i = paramString.length() + 12;
    localStringBuilder.<init>(i);
    localStringBuilder.append("#W");
    localStringBuilder.append(paramInt1);
    localStringBuilder.append("#H");
    localStringBuilder.append(paramInt2);
    localStringBuilder.append("#S");
    paramInt1 = paramScaleType.ordinal();
    localStringBuilder.append(paramInt1);
    localStringBuilder.append(paramString);
    return localStringBuilder.toString();
  }
  
  private static void a()
  {
    Object localObject = Looper.myLooper();
    Looper localLooper = Looper.getMainLooper();
    if (localObject == localLooper) {
      return;
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("ImageLoader must be invoked from the main thread.");
    throw ((Throwable)localObject);
  }
  
  public static ImageLoader.ImageListener getImageListener(ImageView paramImageView, int paramInt1, int paramInt2)
  {
    ImageLoader.1 local1 = new com/mopub/volley/toolbox/ImageLoader$1;
    local1.<init>(paramInt2, paramImageView, paramInt1);
    return local1;
  }
  
  final void a(String paramString, ImageLoader.a parama)
  {
    HashMap localHashMap = e;
    localHashMap.put(paramString, parama);
    paramString = g;
    if (paramString == null)
    {
      paramString = new com/mopub/volley/toolbox/ImageLoader$4;
      paramString.<init>(this);
      g = paramString;
      paramString = f;
      parama = g;
      int i = d;
      long l = i;
      paramString.postDelayed(parama, l);
    }
  }
  
  public ImageLoader.ImageContainer get(String paramString, ImageLoader.ImageListener paramImageListener)
  {
    return get(paramString, paramImageListener, 0, 0);
  }
  
  public ImageLoader.ImageContainer get(String paramString, ImageLoader.ImageListener paramImageListener, int paramInt1, int paramInt2)
  {
    ImageView.ScaleType localScaleType = ImageView.ScaleType.CENTER_INSIDE;
    return get(paramString, paramImageListener, paramInt1, paramInt2, localScaleType);
  }
  
  public ImageLoader.ImageContainer get(String paramString, ImageLoader.ImageListener paramImageListener, int paramInt1, int paramInt2, ImageView.ScaleType paramScaleType)
  {
    a();
    String str = a(paramString, paramInt1, paramInt2, paramScaleType);
    Object localObject1 = a;
    Object localObject2 = ((ImageLoader.ImageCache)localObject1).getBitmap(str);
    boolean bool = true;
    if (localObject2 != null)
    {
      ImageLoader.ImageContainer localImageContainer = new com/mopub/volley/toolbox/ImageLoader$ImageContainer;
      localObject1 = localImageContainer;
      localObject3 = this;
      localObject4 = paramString;
      localImageContainer.<init>(this, (Bitmap)localObject2, paramString, null, null);
      paramImageListener.onResponse(localImageContainer, bool);
      return localImageContainer;
    }
    Object localObject5 = new com/mopub/volley/toolbox/ImageLoader$ImageContainer;
    localObject2 = null;
    localObject1 = localObject5;
    Object localObject3 = this;
    Object localObject4 = paramString;
    ((ImageLoader.ImageContainer)localObject5).<init>(this, null, paramString, str, paramImageListener);
    paramImageListener.onResponse((ImageLoader.ImageContainer)localObject5, bool);
    localObject1 = (ImageLoader.a)b.get(str);
    if (localObject1 != null)
    {
      ((ImageLoader.a)localObject1).addContainer((ImageLoader.ImageContainer)localObject5);
      return (ImageLoader.ImageContainer)localObject5;
    }
    localObject1 = new com/mopub/volley/toolbox/ImageRequest;
    ImageLoader.2 local2 = new com/mopub/volley/toolbox/ImageLoader$2;
    local2.<init>(this, str);
    Bitmap.Config localConfig = Bitmap.Config.RGB_565;
    localObject3 = new com/mopub/volley/toolbox/ImageLoader$3;
    ((ImageLoader.3)localObject3).<init>(this, str);
    localObject2 = localObject5;
    localObject5 = localObject3;
    ((ImageRequest)localObject1).<init>(paramString, local2, paramInt1, paramInt2, paramScaleType, localConfig, (Response.ErrorListener)localObject3);
    c.add((Request)localObject1);
    localObject3 = b;
    localObject4 = new com/mopub/volley/toolbox/ImageLoader$a;
    ((ImageLoader.a)localObject4).<init>(this, (Request)localObject1, (ImageLoader.ImageContainer)localObject2);
    ((HashMap)localObject3).put(str, localObject4);
    return (ImageLoader.ImageContainer)localObject2;
  }
  
  public boolean isCached(String paramString, int paramInt1, int paramInt2)
  {
    ImageView.ScaleType localScaleType = ImageView.ScaleType.CENTER_INSIDE;
    return isCached(paramString, paramInt1, paramInt2, localScaleType);
  }
  
  public boolean isCached(String paramString, int paramInt1, int paramInt2, ImageView.ScaleType paramScaleType)
  {
    a();
    paramString = a(paramString, paramInt1, paramInt2, paramScaleType);
    ImageLoader.ImageCache localImageCache = a;
    paramString = localImageCache.getBitmap(paramString);
    return paramString != null;
  }
  
  public void setBatchedResponseDelay(int paramInt)
  {
    d = paramInt;
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.ImageLoader
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */