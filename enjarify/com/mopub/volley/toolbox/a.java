package com.mopub.volley.toolbox;

import com.mopub.volley.Request;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.conn.ConnectTimeoutException;

final class a
  extends BaseHttpStack
{
  private final HttpStack a;
  
  a(HttpStack paramHttpStack)
  {
    a = paramHttpStack;
  }
  
  public final HttpResponse executeRequest(Request paramRequest, Map paramMap)
  {
    try
    {
      Object localObject = a;
      paramRequest = ((HttpStack)localObject).performRequest(paramRequest, paramMap);
      paramMap = paramRequest.getStatusLine();
      int i = paramMap.getStatusCode();
      localObject = paramRequest.getAllHeaders();
      ArrayList localArrayList = new java/util/ArrayList;
      int j = localObject.length;
      localArrayList.<init>(j);
      j = localObject.length;
      int k = 0;
      while (k < j)
      {
        String str1 = localObject[k];
        com.mopub.volley.Header localHeader = new com/mopub/volley/Header;
        String str2 = str1.getName();
        str1 = str1.getValue();
        localHeader.<init>(str2, str1);
        localArrayList.add(localHeader);
        k += 1;
      }
      localObject = paramRequest.getEntity();
      if (localObject == null)
      {
        paramRequest = new com/mopub/volley/toolbox/HttpResponse;
        paramRequest.<init>(i, localArrayList);
        return paramRequest;
      }
      localObject = paramRequest.getEntity();
      long l1 = ((HttpEntity)localObject).getContentLength();
      long l2 = (int)l1;
      boolean bool = l2 < l1;
      if (!bool)
      {
        localObject = new com/mopub/volley/toolbox/HttpResponse;
        k = (int)paramRequest.getEntity().getContentLength();
        paramRequest = paramRequest.getEntity().getContent();
        ((HttpResponse)localObject).<init>(i, localArrayList, k, paramRequest);
        return (HttpResponse)localObject;
      }
      paramRequest = new java/io/IOException;
      paramMap = String.valueOf(l1);
      paramMap = "Response too large: ".concat(paramMap);
      paramRequest.<init>(paramMap);
      throw paramRequest;
    }
    catch (ConnectTimeoutException paramRequest)
    {
      paramMap = new java/net/SocketTimeoutException;
      paramRequest = paramRequest.getMessage();
      paramMap.<init>(paramRequest);
      throw paramMap;
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */