package com.mopub.volley.toolbox;

import java.util.Comparator;

final class ByteArrayPool$1
  implements Comparator
{
  public final int compare(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
  {
    int i = paramArrayOfByte1.length;
    int j = paramArrayOfByte2.length;
    return i - j;
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.ByteArrayPool.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */