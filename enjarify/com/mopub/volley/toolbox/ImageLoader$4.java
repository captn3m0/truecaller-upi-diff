package com.mopub.volley.toolbox;

import android.graphics.Bitmap;
import com.mopub.volley.VolleyError;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

final class ImageLoader$4
  implements Runnable
{
  ImageLoader$4(ImageLoader paramImageLoader) {}
  
  public final void run()
  {
    Iterator localIterator1 = ImageLoader.b(a).values().iterator();
    boolean bool1 = localIterator1.hasNext();
    if (bool1)
    {
      ImageLoader.a locala = (ImageLoader.a)localIterator1.next();
      Iterator localIterator2 = b.iterator();
      for (;;)
      {
        boolean bool2 = localIterator2.hasNext();
        if (!bool2) {
          break;
        }
        Object localObject1 = (ImageLoader.ImageContainer)localIterator2.next();
        Object localObject2 = ImageLoader.ImageContainer.a((ImageLoader.ImageContainer)localObject1);
        if (localObject2 != null)
        {
          localObject2 = locala.getError();
          if (localObject2 == null)
          {
            localObject2 = a;
            ImageLoader.ImageContainer.a((ImageLoader.ImageContainer)localObject1, (Bitmap)localObject2);
            localObject2 = ImageLoader.ImageContainer.a((ImageLoader.ImageContainer)localObject1);
            ((ImageLoader.ImageListener)localObject2).onResponse((ImageLoader.ImageContainer)localObject1, false);
          }
          else
          {
            localObject1 = ImageLoader.ImageContainer.a((ImageLoader.ImageContainer)localObject1);
            localObject2 = locala.getError();
            ((ImageLoader.ImageListener)localObject1).onErrorResponse((VolleyError)localObject2);
          }
        }
      }
    }
    ImageLoader.b(a).clear();
    ImageLoader.c(a);
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.ImageLoader.4
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */