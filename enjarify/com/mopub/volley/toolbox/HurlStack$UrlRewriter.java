package com.mopub.volley.toolbox;

public abstract interface HurlStack$UrlRewriter
{
  public abstract String rewriteUrl(String paramString);
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.HurlStack.UrlRewriter
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */