package com.mopub.volley.toolbox;

import com.mopub.volley.NetworkResponse;
import com.mopub.volley.ParseError;
import com.mopub.volley.Response;
import com.mopub.volley.Response.ErrorListener;
import com.mopub.volley.Response.Listener;
import com.mopub.volley.VolleyError;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonObjectRequest
  extends JsonRequest
{
  public JsonObjectRequest(int paramInt, String paramString, JSONObject paramJSONObject, Response.Listener paramListener, Response.ErrorListener paramErrorListener)
  {
    super(paramInt, paramString, paramJSONObject, paramListener, paramErrorListener);
  }
  
  public JsonObjectRequest(String paramString, JSONObject paramJSONObject, Response.Listener paramListener, Response.ErrorListener paramErrorListener)
  {
    this(i, paramString, paramJSONObject, paramListener, paramErrorListener);
  }
  
  public final Response parseNetworkResponse(NetworkResponse paramNetworkResponse)
  {
    Object localObject1;
    try
    {
      localObject1 = new java/lang/String;
      Object localObject2 = data;
      Object localObject3 = headers;
      String str = "utf-8";
      localObject3 = HttpHeaderParser.parseCharset((Map)localObject3, str);
      ((String)localObject1).<init>((byte[])localObject2, (String)localObject3);
      localObject2 = new org/json/JSONObject;
      ((JSONObject)localObject2).<init>((String)localObject1);
      paramNetworkResponse = HttpHeaderParser.parseCacheHeaders(paramNetworkResponse);
      return Response.success(localObject2, paramNetworkResponse);
    }
    catch (JSONException paramNetworkResponse)
    {
      localObject1 = new com/mopub/volley/ParseError;
      ((ParseError)localObject1).<init>(paramNetworkResponse);
      return Response.error((VolleyError)localObject1);
    }
    catch (UnsupportedEncodingException paramNetworkResponse)
    {
      localObject1 = new com/mopub/volley/ParseError;
      ((ParseError)localObject1).<init>(paramNetworkResponse);
    }
    return Response.error((VolleyError)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.JsonObjectRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */