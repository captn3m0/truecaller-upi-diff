package com.mopub.volley.toolbox;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

public class NetworkImageView
  extends ImageView
{
  private String a;
  private int b;
  private int c;
  private ImageLoader d;
  private ImageLoader.ImageContainer e;
  
  public NetworkImageView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public NetworkImageView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public NetworkImageView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  private void a()
  {
    int i = b;
    if (i != 0)
    {
      setImageResource(i);
      return;
    }
    setImageBitmap(null);
  }
  
  private void a(boolean paramBoolean)
  {
    int i = getWidth();
    int j = getHeight();
    ImageView.ScaleType localScaleType = getScaleType();
    Object localObject1 = getLayoutParams();
    boolean bool = true;
    NetworkImageView.1 local1 = null;
    int k;
    Object localObject2;
    int n;
    if (localObject1 != null)
    {
      localObject1 = getLayoutParams();
      k = width;
      m = -2;
      if (k == m)
      {
        k = 1;
      }
      else
      {
        k = 0;
        localObject1 = null;
      }
      localObject2 = getLayoutParams();
      n = height;
      if (n == m) {
        m = 1;
      } else {
        m = 0;
      }
    }
    else
    {
      k = 0;
      localObject1 = null;
      m = 0;
    }
    if ((k == 0) || (m == 0))
    {
      bool = false;
      localObject3 = null;
    }
    if ((i == 0) && (j == 0) && (!bool)) {
      return;
    }
    Object localObject3 = a;
    bool = TextUtils.isEmpty((CharSequence)localObject3);
    if (bool)
    {
      localImageContainer = e;
      if (localImageContainer != null)
      {
        localImageContainer.cancelRequest();
        paramBoolean = false;
        localImageContainer = null;
        e = null;
      }
      a();
      return;
    }
    localObject3 = e;
    if (localObject3 != null)
    {
      localObject3 = ((ImageLoader.ImageContainer)localObject3).getRequestUrl();
      if (localObject3 != null)
      {
        localObject3 = e.getRequestUrl();
        localObject2 = a;
        bool = ((String)localObject3).equals(localObject2);
        if (bool) {
          return;
        }
        localObject3 = e;
        ((ImageLoader.ImageContainer)localObject3).cancelRequest();
        a();
      }
    }
    if (k != 0) {
      i = 0;
    }
    if (m != 0)
    {
      n = 0;
      localObject2 = null;
    }
    else
    {
      n = j;
    }
    localObject1 = d;
    localObject3 = a;
    local1 = new com/mopub/volley/toolbox/NetworkImageView$1;
    local1.<init>(this, paramBoolean);
    int m = i;
    ImageLoader.ImageContainer localImageContainer = ((ImageLoader)localObject1).get((String)localObject3, local1, i, n, localScaleType);
    e = localImageContainer;
  }
  
  protected void drawableStateChanged()
  {
    super.drawableStateChanged();
    invalidate();
  }
  
  protected void onDetachedFromWindow()
  {
    ImageLoader.ImageContainer localImageContainer = e;
    if (localImageContainer != null)
    {
      localImageContainer.cancelRequest();
      localImageContainer = null;
      setImageBitmap(null);
      e = null;
    }
    super.onDetachedFromWindow();
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    a(true);
  }
  
  public void setDefaultImageResId(int paramInt)
  {
    b = paramInt;
  }
  
  public void setErrorImageResId(int paramInt)
  {
    c = paramInt;
  }
  
  public void setImageUrl(String paramString, ImageLoader paramImageLoader)
  {
    a = paramString;
    d = paramImageLoader;
    a(false);
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.NetworkImageView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */