package com.mopub.volley.toolbox;

import com.mopub.volley.Request;
import java.util.Map;
import org.apache.http.HttpResponse;

public abstract interface HttpStack
{
  public abstract HttpResponse performRequest(Request paramRequest, Map paramMap);
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.HttpStack
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */