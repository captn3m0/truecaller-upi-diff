package com.mopub.volley.toolbox;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.mopub.volley.AuthFailureError;

public class AndroidAuthenticator
  implements Authenticator
{
  private final AccountManager a;
  private final Account b;
  private final String c;
  private final boolean d;
  
  private AndroidAuthenticator(AccountManager paramAccountManager, Account paramAccount, String paramString, boolean paramBoolean)
  {
    a = paramAccountManager;
    b = paramAccount;
    c = paramString;
    d = paramBoolean;
  }
  
  public AndroidAuthenticator(Context paramContext, Account paramAccount, String paramString)
  {
    this(paramContext, paramAccount, paramString, false);
  }
  
  public AndroidAuthenticator(Context paramContext, Account paramAccount, String paramString, boolean paramBoolean)
  {
    this(paramContext, paramAccount, paramString, paramBoolean);
  }
  
  public Account getAccount()
  {
    return b;
  }
  
  public String getAuthToken()
  {
    Object localObject1 = a;
    Object localObject2 = b;
    String str = c;
    boolean bool1 = d;
    localObject1 = ((AccountManager)localObject1).getAuthToken((Account)localObject2, str, bool1, null, null);
    try
    {
      localObject2 = ((AccountManagerFuture)localObject1).getResult();
      localObject2 = (Bundle)localObject2;
      str = null;
      bool1 = ((AccountManagerFuture)localObject1).isDone();
      if (bool1)
      {
        boolean bool2 = ((AccountManagerFuture)localObject1).isCancelled();
        if (!bool2)
        {
          localObject1 = "intent";
          bool2 = ((Bundle)localObject2).containsKey((String)localObject1);
          if (!bool2)
          {
            localObject1 = "authtoken";
            str = ((Bundle)localObject2).getString((String)localObject1);
          }
          else
          {
            localObject1 = (Intent)((Bundle)localObject2).getParcelable("intent");
            localObject2 = new com/mopub/volley/AuthFailureError;
            ((AuthFailureError)localObject2).<init>((Intent)localObject1);
            throw ((Throwable)localObject2);
          }
        }
      }
      if (str != null) {
        return str;
      }
      localObject1 = new com/mopub/volley/AuthFailureError;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Got null auth token for type: ");
      str = c;
      ((StringBuilder)localObject2).append(str);
      localObject2 = ((StringBuilder)localObject2).toString();
      ((AuthFailureError)localObject1).<init>((String)localObject2);
      throw ((Throwable)localObject1);
    }
    catch (Exception localException)
    {
      localObject2 = new com/mopub/volley/AuthFailureError;
      ((AuthFailureError)localObject2).<init>("Error while retrieving auth token", localException);
      throw ((Throwable)localObject2);
    }
  }
  
  public String getAuthTokenType()
  {
    return c;
  }
  
  public void invalidateAuthToken(String paramString)
  {
    AccountManager localAccountManager = a;
    String str = b.type;
    localAccountManager.invalidateAuthToken(str, paramString);
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.AndroidAuthenticator
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */