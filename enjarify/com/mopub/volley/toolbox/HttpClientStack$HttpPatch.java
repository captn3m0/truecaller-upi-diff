package com.mopub.volley.toolbox;

import java.net.URI;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;

public final class HttpClientStack$HttpPatch
  extends HttpEntityEnclosingRequestBase
{
  public static final String METHOD_NAME = "PATCH";
  
  public HttpClientStack$HttpPatch() {}
  
  public HttpClientStack$HttpPatch(String paramString)
  {
    paramString = URI.create(paramString);
    setURI(paramString);
  }
  
  public HttpClientStack$HttpPatch(URI paramURI)
  {
    setURI(paramURI);
  }
  
  public final String getMethod()
  {
    return "PATCH";
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.HttpClientStack.HttpPatch
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */