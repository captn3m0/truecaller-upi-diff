package com.mopub.volley.toolbox;

import android.graphics.Bitmap;
import com.mopub.volley.Response.Listener;
import java.util.HashMap;

final class ImageLoader$2
  implements Response.Listener
{
  ImageLoader$2(ImageLoader paramImageLoader, String paramString) {}
  
  public final void onResponse(Bitmap paramBitmap)
  {
    ImageLoader localImageLoader = b;
    String str = a;
    a.putBitmap(str, paramBitmap);
    ImageLoader.a locala = (ImageLoader.a)b.remove(str);
    if (locala != null)
    {
      a = paramBitmap;
      localImageLoader.a(str, locala);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.ImageLoader.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */