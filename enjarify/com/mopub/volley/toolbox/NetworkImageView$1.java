package com.mopub.volley.toolbox;

import com.mopub.volley.VolleyError;

final class NetworkImageView$1
  implements ImageLoader.ImageListener
{
  NetworkImageView$1(NetworkImageView paramNetworkImageView, boolean paramBoolean) {}
  
  public final void onErrorResponse(VolleyError paramVolleyError)
  {
    paramVolleyError = b;
    int i = NetworkImageView.a(paramVolleyError);
    if (i != 0)
    {
      paramVolleyError = b;
      int j = NetworkImageView.a(paramVolleyError);
      paramVolleyError.setImageResource(j);
    }
  }
  
  public final void onResponse(ImageLoader.ImageContainer paramImageContainer, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      paramBoolean = a;
      if (paramBoolean)
      {
        localObject = b;
        NetworkImageView.1.1 local1 = new com/mopub/volley/toolbox/NetworkImageView$1$1;
        local1.<init>(this, paramImageContainer);
        ((NetworkImageView)localObject).post(local1);
        return;
      }
    }
    Object localObject = paramImageContainer.getBitmap();
    if (localObject != null)
    {
      localObject = b;
      paramImageContainer = paramImageContainer.getBitmap();
      ((NetworkImageView)localObject).setImageBitmap(paramImageContainer);
      return;
    }
    paramImageContainer = b;
    int i = NetworkImageView.b(paramImageContainer);
    if (i != 0)
    {
      paramImageContainer = b;
      paramBoolean = NetworkImageView.b(paramImageContainer);
      paramImageContainer.setImageResource(paramBoolean);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.NetworkImageView.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */