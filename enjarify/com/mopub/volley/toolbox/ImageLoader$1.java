package com.mopub.volley.toolbox;

import android.widget.ImageView;
import com.mopub.volley.VolleyError;

final class ImageLoader$1
  implements ImageLoader.ImageListener
{
  ImageLoader$1(int paramInt1, ImageView paramImageView, int paramInt2) {}
  
  public final void onErrorResponse(VolleyError paramVolleyError)
  {
    int i = a;
    if (i != 0)
    {
      ImageView localImageView = b;
      localImageView.setImageResource(i);
    }
  }
  
  public final void onResponse(ImageLoader.ImageContainer paramImageContainer, boolean paramBoolean)
  {
    Object localObject = paramImageContainer.getBitmap();
    if (localObject != null)
    {
      localObject = b;
      paramImageContainer = paramImageContainer.getBitmap();
      ((ImageView)localObject).setImageBitmap(paramImageContainer);
      return;
    }
    int i = c;
    if (i != 0)
    {
      localObject = b;
      ((ImageView)localObject).setImageResource(i);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.ImageLoader.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */