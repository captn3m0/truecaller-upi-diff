package com.mopub.volley.toolbox;

import com.mopub.volley.Request;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.ProtocolVersion;
import org.apache.http.StatusLine;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicStatusLine;

public abstract class BaseHttpStack
  implements HttpStack
{
  public abstract HttpResponse executeRequest(Request paramRequest, Map paramMap);
  
  public final org.apache.http.HttpResponse performRequest(Request paramRequest, Map paramMap)
  {
    paramRequest = executeRequest(paramRequest, paramMap);
    paramMap = new org/apache/http/ProtocolVersion;
    int i = 1;
    paramMap.<init>("HTTP", i, i);
    Object localObject1 = new org/apache/http/message/BasicStatusLine;
    i = paramRequest.getStatusCode();
    Object localObject2 = "";
    ((BasicStatusLine)localObject1).<init>(paramMap, i, (String)localObject2);
    paramMap = new org/apache/http/message/BasicHttpResponse;
    paramMap.<init>((StatusLine)localObject1);
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    Object localObject3 = paramRequest.getHeaders().iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject3).hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (com.mopub.volley.Header)((Iterator)localObject3).next();
      BasicHeader localBasicHeader = new org/apache/http/message/BasicHeader;
      String str = ((com.mopub.volley.Header)localObject2).getName();
      localObject2 = ((com.mopub.volley.Header)localObject2).getValue();
      localBasicHeader.<init>(str, (String)localObject2);
      ((List)localObject1).add(localBasicHeader);
    }
    i = ((List)localObject1).size();
    localObject3 = new org.apache.http.Header[i];
    localObject1 = (org.apache.http.Header[])((List)localObject1).toArray((Object[])localObject3);
    paramMap.setHeaders((org.apache.http.Header[])localObject1);
    localObject1 = paramRequest.getContent();
    if (localObject1 != null)
    {
      localObject3 = new org/apache/http/entity/BasicHttpEntity;
      ((BasicHttpEntity)localObject3).<init>();
      ((BasicHttpEntity)localObject3).setContent((InputStream)localObject1);
      int j = paramRequest.getContentLength();
      long l = j;
      ((BasicHttpEntity)localObject3).setContentLength(l);
      paramMap.setEntity((HttpEntity)localObject3);
    }
    return paramMap;
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.BaseHttpStack
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */