package com.mopub.volley.toolbox;

import com.mopub.volley.NetworkResponse;
import com.mopub.volley.Request;
import com.mopub.volley.Response;
import com.mopub.volley.Response.ErrorListener;
import com.mopub.volley.Response.Listener;
import java.io.UnsupportedEncodingException;
import java.util.Map;

public class StringRequest
  extends Request
{
  private final Object a;
  private Response.Listener b;
  
  public StringRequest(int paramInt, String paramString, Response.Listener paramListener, Response.ErrorListener paramErrorListener)
  {
    super(paramInt, paramString, paramErrorListener);
    Object localObject = new java/lang/Object;
    localObject.<init>();
    a = localObject;
    b = paramListener;
  }
  
  public StringRequest(String paramString, Response.Listener paramListener, Response.ErrorListener paramErrorListener)
  {
    this(0, paramString, paramListener, paramErrorListener);
  }
  
  public void cancel()
  {
    super.cancel();
    Object localObject1 = a;
    Object localObject2 = null;
    try
    {
      b = null;
      return;
    }
    finally {}
  }
  
  public final Response parseNetworkResponse(NetworkResponse paramNetworkResponse)
  {
    String str;
    try
    {
      str = new java/lang/String;
      arrayOfByte = data;
      Object localObject = headers;
      localObject = HttpHeaderParser.parseCharset((Map)localObject);
      str.<init>(arrayOfByte, (String)localObject);
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      str = new java/lang/String;
      byte[] arrayOfByte = data;
      str.<init>(arrayOfByte);
    }
    paramNetworkResponse = HttpHeaderParser.parseCacheHeaders(paramNetworkResponse);
    return Response.success(str, paramNetworkResponse);
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.StringRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */