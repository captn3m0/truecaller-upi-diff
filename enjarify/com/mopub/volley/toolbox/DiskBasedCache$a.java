package com.mopub.volley.toolbox;

import com.mopub.volley.Cache.Entry;
import com.mopub.volley.VolleyLog;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

final class DiskBasedCache$a
{
  long a;
  final String b;
  final String c;
  final long d;
  final long e;
  final long f;
  final long g;
  final List h;
  
  DiskBasedCache$a(String paramString, Cache.Entry paramEntry)
  {
    this(paramString, str, l1, l2, l3, l4, (List)localObject2);
    long l5 = data.length;
    a = l5;
  }
  
  private DiskBasedCache$a(String paramString1, String paramString2, long paramLong1, long paramLong2, long paramLong3, long paramLong4, List paramList)
  {
    b = paramString1;
    paramString1 = "";
    boolean bool = paramString1.equals(paramString2);
    if (bool) {
      paramString2 = null;
    }
    c = paramString2;
    d = paramLong1;
    e = paramLong2;
    f = paramLong3;
    g = paramLong4;
    h = paramList;
  }
  
  static a a(DiskBasedCache.b paramb)
  {
    int i = DiskBasedCache.a(paramb);
    int j = 538247942;
    if (i == j)
    {
      String str1 = DiskBasedCache.a(paramb);
      String str2 = DiskBasedCache.a(paramb);
      long l1 = DiskBasedCache.b(paramb);
      long l2 = DiskBasedCache.b(paramb);
      long l3 = DiskBasedCache.b(paramb);
      long l4 = DiskBasedCache.b(paramb);
      List localList = DiskBasedCache.b(paramb);
      paramb = new com/mopub/volley/toolbox/DiskBasedCache$a;
      paramb.<init>(str1, str2, l1, l2, l3, l4, localList);
      return paramb;
    }
    paramb = new java/io/IOException;
    paramb.<init>();
    throw paramb;
  }
  
  final boolean a(OutputStream paramOutputStream)
  {
    int i = 538247942;
    boolean bool = true;
    try
    {
      DiskBasedCache.a(paramOutputStream, i);
      Object localObject = b;
      DiskBasedCache.a(paramOutputStream, (String)localObject);
      localObject = c;
      if (localObject == null) {
        localObject = "";
      } else {
        localObject = c;
      }
      DiskBasedCache.a(paramOutputStream, (String)localObject);
      long l = d;
      DiskBasedCache.a(paramOutputStream, l);
      l = e;
      DiskBasedCache.a(paramOutputStream, l);
      l = f;
      DiskBasedCache.a(paramOutputStream, l);
      l = g;
      DiskBasedCache.a(paramOutputStream, l);
      localObject = h;
      DiskBasedCache.a((List)localObject, paramOutputStream);
      paramOutputStream.flush();
      return bool;
    }
    catch (IOException paramOutputStream)
    {
      Object[] arrayOfObject = new Object[bool];
      paramOutputStream = paramOutputStream.toString();
      arrayOfObject[0] = paramOutputStream;
      VolleyLog.d("%s", arrayOfObject);
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.DiskBasedCache.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */