package com.mopub.volley.toolbox;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;

public final class HttpResponse
{
  private final int a;
  private final List b;
  private final int c;
  private final InputStream d;
  
  public HttpResponse(int paramInt, List paramList)
  {
    this(paramInt, paramList, -1, null);
  }
  
  public HttpResponse(int paramInt1, List paramList, int paramInt2, InputStream paramInputStream)
  {
    a = paramInt1;
    b = paramList;
    c = paramInt2;
    d = paramInputStream;
  }
  
  public final InputStream getContent()
  {
    return d;
  }
  
  public final int getContentLength()
  {
    return c;
  }
  
  public final List getHeaders()
  {
    return Collections.unmodifiableList(b);
  }
  
  public final int getStatusCode()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.HttpResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */