package com.mopub.volley.toolbox;

import java.io.ByteArrayOutputStream;

public class PoolingByteArrayOutputStream
  extends ByteArrayOutputStream
{
  private final ByteArrayPool a;
  
  public PoolingByteArrayOutputStream(ByteArrayPool paramByteArrayPool)
  {
    this(paramByteArrayPool, 256);
  }
  
  public PoolingByteArrayOutputStream(ByteArrayPool paramByteArrayPool, int paramInt)
  {
    a = paramByteArrayPool;
    paramByteArrayPool = a;
    paramInt = Math.max(paramInt, 256);
    paramByteArrayPool = paramByteArrayPool.getBuf(paramInt);
    buf = paramByteArrayPool;
  }
  
  private void a(int paramInt)
  {
    int i = count + paramInt;
    byte[] arrayOfByte1 = buf;
    int j = arrayOfByte1.length;
    if (i <= j) {
      return;
    }
    Object localObject = a;
    j = (count + paramInt) * 2;
    byte[] arrayOfByte2 = ((ByteArrayPool)localObject).getBuf(j);
    localObject = buf;
    j = count;
    System.arraycopy(localObject, 0, arrayOfByte2, 0, j);
    localObject = a;
    arrayOfByte1 = buf;
    ((ByteArrayPool)localObject).returnBuf(arrayOfByte1);
    buf = arrayOfByte2;
  }
  
  public void close()
  {
    ByteArrayPool localByteArrayPool = a;
    byte[] arrayOfByte = buf;
    localByteArrayPool.returnBuf(arrayOfByte);
    buf = null;
    super.close();
  }
  
  public void finalize()
  {
    ByteArrayPool localByteArrayPool = a;
    byte[] arrayOfByte = buf;
    localByteArrayPool.returnBuf(arrayOfByte);
  }
  
  public void write(int paramInt)
  {
    int i = 1;
    try
    {
      a(i);
      super.write(paramInt);
      return;
    }
    finally {}
  }
  
  public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    try
    {
      a(paramInt2);
      super.write(paramArrayOfByte, paramInt1, paramInt2);
      return;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.PoolingByteArrayOutputStream
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */