package com.mopub.volley.toolbox;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class ByteArrayPool
{
  protected static final Comparator a;
  private final List b;
  private final List c;
  private int d;
  private final int e;
  
  static
  {
    ByteArrayPool.1 local1 = new com/mopub/volley/toolbox/ByteArrayPool$1;
    local1.<init>();
    a = local1;
  }
  
  public ByteArrayPool(int paramInt)
  {
    Object localObject = new java/util/LinkedList;
    ((LinkedList)localObject).<init>();
    b = ((List)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>(64);
    c = ((List)localObject);
    d = 0;
    e = paramInt;
  }
  
  private void a()
  {
    try
    {
      for (;;)
      {
        int i = d;
        int j = e;
        if (i <= j) {
          break;
        }
        Object localObject1 = b;
        j = 0;
        List localList = null;
        localObject1 = ((List)localObject1).remove(0);
        localObject1 = (byte[])localObject1;
        localList = c;
        localList.remove(localObject1);
        j = d;
        i = localObject1.length;
        j -= i;
        d = j;
      }
      return;
    }
    finally {}
  }
  
  public byte[] getBuf(int paramInt)
  {
    int i = 0;
    try
    {
      for (;;)
      {
        Object localObject1 = c;
        int j = ((List)localObject1).size();
        if (i >= j) {
          break;
        }
        localObject1 = c;
        localObject1 = ((List)localObject1).get(i);
        localObject1 = (byte[])localObject1;
        int k = localObject1.length;
        if (k >= paramInt)
        {
          paramInt = d;
          k = localObject1.length;
          paramInt -= k;
          d = paramInt;
          localObject2 = c;
          ((List)localObject2).remove(i);
          localObject2 = b;
          ((List)localObject2).remove(localObject1);
          return (byte[])localObject1;
        }
        i += 1;
      }
      Object localObject2 = new byte[paramInt];
      return (byte[])localObject2;
    }
    finally {}
  }
  
  public void returnBuf(byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte != null) {
      try
      {
        int i = paramArrayOfByte.length;
        int j = e;
        if (i <= j)
        {
          List localList = b;
          localList.add(paramArrayOfByte);
          localList = c;
          Object localObject = a;
          i = Collections.binarySearch(localList, paramArrayOfByte, (Comparator)localObject);
          if (i < 0) {
            i = -i + -1;
          }
          localObject = c;
          ((List)localObject).add(i, paramArrayOfByte);
          i = d;
          int k = paramArrayOfByte.length;
          i += k;
          d = i;
          a();
          return;
        }
      }
      finally {}
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.ByteArrayPool
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */