package com.mopub.volley.toolbox;

import com.mopub.volley.Request;
import com.mopub.volley.Response.ErrorListener;
import com.mopub.volley.Response.Listener;
import com.mopub.volley.VolleyError;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class RequestFuture
  implements Response.ErrorListener, Response.Listener, Future
{
  private Request a;
  private boolean b = false;
  private Object c;
  private VolleyError d;
  
  private Object a(Long paramLong)
  {
    try
    {
      VolleyError localVolleyError = d;
      if (localVolleyError == null)
      {
        boolean bool1 = b;
        if (bool1)
        {
          paramLong = c;
          return paramLong;
        }
        long l1 = 0L;
        if (paramLong == null)
        {
          wait(l1);
        }
        else
        {
          long l2 = paramLong.longValue();
          boolean bool2 = l2 < l1;
          if (bool2)
          {
            l1 = paramLong.longValue();
            wait(l1);
          }
        }
        paramLong = d;
        if (paramLong == null)
        {
          boolean bool3 = b;
          if (bool3)
          {
            paramLong = c;
            return paramLong;
          }
          paramLong = new java/util/concurrent/TimeoutException;
          paramLong.<init>();
          throw paramLong;
        }
        paramLong = new java/util/concurrent/ExecutionException;
        localVolleyError = d;
        paramLong.<init>(localVolleyError);
        throw paramLong;
      }
      paramLong = new java/util/concurrent/ExecutionException;
      localVolleyError = d;
      paramLong.<init>(localVolleyError);
      throw paramLong;
    }
    finally {}
  }
  
  public static RequestFuture newFuture()
  {
    RequestFuture localRequestFuture = new com/mopub/volley/toolbox/RequestFuture;
    localRequestFuture.<init>();
    return localRequestFuture;
  }
  
  public boolean cancel(boolean paramBoolean)
  {
    try
    {
      Request localRequest = a;
      if (localRequest == null) {
        return false;
      }
      paramBoolean = isDone();
      if (!paramBoolean)
      {
        localRequest = a;
        localRequest.cancel();
        return true;
      }
      return false;
    }
    finally {}
  }
  
  public Object get()
  {
    Object localObject = null;
    try
    {
      return a(null);
    }
    catch (TimeoutException localTimeoutException)
    {
      AssertionError localAssertionError = new java/lang/AssertionError;
      localAssertionError.<init>(localTimeoutException);
      throw localAssertionError;
    }
  }
  
  public Object get(long paramLong, TimeUnit paramTimeUnit)
  {
    Long localLong = Long.valueOf(TimeUnit.MILLISECONDS.convert(paramLong, paramTimeUnit));
    return a(localLong);
  }
  
  public boolean isCancelled()
  {
    Request localRequest = a;
    if (localRequest == null) {
      return false;
    }
    return localRequest.isCanceled();
  }
  
  /* Error */
  public boolean isDone()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 24	com/mopub/volley/toolbox/RequestFuture:b	Z
    //   6: istore_1
    //   7: iload_1
    //   8: ifne +32 -> 40
    //   11: aload_0
    //   12: getfield 26	com/mopub/volley/toolbox/RequestFuture:d	Lcom/mopub/volley/VolleyError;
    //   15: astore_2
    //   16: aload_2
    //   17: ifnonnull +23 -> 40
    //   20: aload_0
    //   21: invokevirtual 87	com/mopub/volley/toolbox/RequestFuture:isCancelled	()Z
    //   24: istore_1
    //   25: iload_1
    //   26: ifeq +6 -> 32
    //   29: goto +11 -> 40
    //   32: iconst_0
    //   33: istore_1
    //   34: aconst_null
    //   35: astore_2
    //   36: aload_0
    //   37: monitorexit
    //   38: iload_1
    //   39: ireturn
    //   40: iconst_1
    //   41: istore_1
    //   42: goto -6 -> 36
    //   45: astore_2
    //   46: aload_0
    //   47: monitorexit
    //   48: aload_2
    //   49: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	50	0	this	RequestFuture
    //   6	36	1	bool	boolean
    //   15	21	2	localVolleyError	VolleyError
    //   45	4	2	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   2	6	45	finally
    //   11	15	45	finally
    //   20	24	45	finally
  }
  
  public void onErrorResponse(VolleyError paramVolleyError)
  {
    try
    {
      d = paramVolleyError;
      notifyAll();
      return;
    }
    finally {}
  }
  
  public void onResponse(Object paramObject)
  {
    boolean bool = true;
    try
    {
      b = bool;
      c = paramObject;
      notifyAll();
      return;
    }
    finally {}
  }
  
  public void setRequest(Request paramRequest)
  {
    a = paramRequest;
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.RequestFuture
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */