package com.mopub.volley.toolbox;

import com.mopub.volley.NetworkResponse;
import com.mopub.volley.Request;
import com.mopub.volley.Response;
import com.mopub.volley.Response.ErrorListener;
import com.mopub.volley.Response.Listener;
import com.mopub.volley.VolleyLog;
import java.io.UnsupportedEncodingException;

public abstract class JsonRequest
  extends Request
{
  private static final String a;
  private final Object b;
  private Response.Listener c;
  private final String d;
  
  static
  {
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = "utf-8";
    a = String.format("application/json; charset=%s", arrayOfObject);
  }
  
  public JsonRequest(int paramInt, String paramString1, String paramString2, Response.Listener paramListener, Response.ErrorListener paramErrorListener)
  {
    super(paramInt, paramString1, paramErrorListener);
    Object localObject = new java/lang/Object;
    localObject.<init>();
    b = localObject;
    c = paramListener;
    d = paramString2;
  }
  
  public JsonRequest(String paramString1, String paramString2, Response.Listener paramListener, Response.ErrorListener paramErrorListener)
  {
    this(-1, paramString1, paramString2, paramListener, paramErrorListener);
  }
  
  public void cancel()
  {
    super.cancel();
    Object localObject1 = b;
    Object localObject2 = null;
    try
    {
      c = null;
      return;
    }
    finally {}
  }
  
  public void deliverResponse(Object paramObject)
  {
    synchronized (b)
    {
      Response.Listener localListener = c;
      if (localListener != null) {
        localListener.onResponse(paramObject);
      }
      return;
    }
  }
  
  public byte[] getBody()
  {
    try
    {
      String str1 = d;
      if (str1 == null) {
        return null;
      }
      str1 = d;
      localObject = "utf-8";
      return str1.getBytes((String)localObject);
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      Object localObject = new Object[2];
      String str2 = d;
      localObject[0] = str2;
      localObject[1] = "utf-8";
      VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", (Object[])localObject);
    }
    return null;
  }
  
  public String getBodyContentType()
  {
    return a;
  }
  
  public byte[] getPostBody()
  {
    return getBody();
  }
  
  public String getPostBodyContentType()
  {
    return getBodyContentType();
  }
  
  public abstract Response parseNetworkResponse(NetworkResponse paramNetworkResponse);
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.JsonRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */