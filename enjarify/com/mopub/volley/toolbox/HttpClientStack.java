package com.mopub.volley.toolbox;

import com.mopub.volley.Request;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpTrace;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.params.HttpConnectionParams;

public class HttpClientStack
  implements HttpStack
{
  protected final HttpClient a;
  
  public HttpClientStack(HttpClient paramHttpClient)
  {
    a = paramHttpClient;
  }
  
  private static void a(HttpEntityEnclosingRequestBase paramHttpEntityEnclosingRequestBase, Request paramRequest)
  {
    paramRequest = paramRequest.getBody();
    if (paramRequest != null)
    {
      ByteArrayEntity localByteArrayEntity = new org/apache/http/entity/ByteArrayEntity;
      localByteArrayEntity.<init>(paramRequest);
      paramHttpEntityEnclosingRequestBase.setEntity(localByteArrayEntity);
    }
  }
  
  private static void a(HttpUriRequest paramHttpUriRequest, Map paramMap)
  {
    Iterator localIterator = paramMap.keySet().iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      String str1 = (String)localIterator.next();
      String str2 = (String)paramMap.get(str1);
      paramHttpUriRequest.setHeader(str1, str2);
    }
  }
  
  public HttpResponse performRequest(Request paramRequest, Map paramMap)
  {
    int i = paramRequest.getMethod();
    Object localObject1;
    Object localObject2;
    Object localObject3;
    switch (i)
    {
    default: 
      paramRequest = new java/lang/IllegalStateException;
      paramRequest.<init>("Unknown request method.");
      throw paramRequest;
    case 7: 
      localObject1 = new com/mopub/volley/toolbox/HttpClientStack$HttpPatch;
      localObject2 = paramRequest.getUrl();
      ((HttpClientStack.HttpPatch)localObject1).<init>((String)localObject2);
      localObject2 = "Content-Type";
      localObject3 = paramRequest.getBodyContentType();
      ((HttpClientStack.HttpPatch)localObject1).addHeader((String)localObject2, (String)localObject3);
      a((HttpEntityEnclosingRequestBase)localObject1, paramRequest);
      break;
    case 6: 
      localObject1 = new org/apache/http/client/methods/HttpTrace;
      localObject2 = paramRequest.getUrl();
      ((HttpTrace)localObject1).<init>((String)localObject2);
      break;
    case 5: 
      localObject1 = new org/apache/http/client/methods/HttpOptions;
      localObject2 = paramRequest.getUrl();
      ((HttpOptions)localObject1).<init>((String)localObject2);
      break;
    case 4: 
      localObject1 = new org/apache/http/client/methods/HttpHead;
      localObject2 = paramRequest.getUrl();
      ((HttpHead)localObject1).<init>((String)localObject2);
      break;
    case 3: 
      localObject1 = new org/apache/http/client/methods/HttpDelete;
      localObject2 = paramRequest.getUrl();
      ((HttpDelete)localObject1).<init>((String)localObject2);
      break;
    case 2: 
      localObject1 = new org/apache/http/client/methods/HttpPut;
      localObject2 = paramRequest.getUrl();
      ((HttpPut)localObject1).<init>((String)localObject2);
      localObject2 = "Content-Type";
      localObject3 = paramRequest.getBodyContentType();
      ((HttpPut)localObject1).addHeader((String)localObject2, (String)localObject3);
      a((HttpEntityEnclosingRequestBase)localObject1, paramRequest);
      break;
    case 1: 
      localObject1 = new org/apache/http/client/methods/HttpPost;
      localObject2 = paramRequest.getUrl();
      ((HttpPost)localObject1).<init>((String)localObject2);
      localObject2 = "Content-Type";
      localObject3 = paramRequest.getBodyContentType();
      ((HttpPost)localObject1).addHeader((String)localObject2, (String)localObject3);
      a((HttpEntityEnclosingRequestBase)localObject1, paramRequest);
      break;
    case 0: 
      localObject1 = new org/apache/http/client/methods/HttpGet;
      localObject2 = paramRequest.getUrl();
      ((HttpGet)localObject1).<init>((String)localObject2);
      break;
    case -1: 
      localObject1 = paramRequest.getPostBody();
      if (localObject1 != null)
      {
        localObject2 = new org/apache/http/client/methods/HttpPost;
        localObject3 = paramRequest.getUrl();
        ((HttpPost)localObject2).<init>((String)localObject3);
        String str = paramRequest.getPostBodyContentType();
        ((HttpPost)localObject2).addHeader("Content-Type", str);
        localObject3 = new org/apache/http/entity/ByteArrayEntity;
        ((ByteArrayEntity)localObject3).<init>((byte[])localObject1);
        ((HttpPost)localObject2).setEntity((HttpEntity)localObject3);
        localObject1 = localObject2;
      }
      else
      {
        localObject1 = new org/apache/http/client/methods/HttpGet;
        localObject2 = paramRequest.getUrl();
        ((HttpGet)localObject1).<init>((String)localObject2);
      }
      break;
    }
    a((HttpUriRequest)localObject1, paramMap);
    paramMap = paramRequest.getHeaders();
    a((HttpUriRequest)localObject1, paramMap);
    paramMap = ((HttpUriRequest)localObject1).getParams();
    int j = paramRequest.getTimeoutMs();
    HttpConnectionParams.setConnectionTimeout(paramMap, 5000);
    HttpConnectionParams.setSoTimeout(paramMap, j);
    return a.execute((HttpUriRequest)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.HttpClientStack
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */