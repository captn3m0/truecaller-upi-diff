package com.mopub.volley.toolbox;

import com.mopub.volley.NetworkResponse;
import com.mopub.volley.ParseError;
import com.mopub.volley.Response;
import com.mopub.volley.Response.ErrorListener;
import com.mopub.volley.Response.Listener;
import com.mopub.volley.VolleyError;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;

public class JsonArrayRequest
  extends JsonRequest
{
  public JsonArrayRequest(int paramInt, String paramString, JSONArray paramJSONArray, Response.Listener paramListener, Response.ErrorListener paramErrorListener)
  {
    super(paramInt, paramString, paramJSONArray, paramListener, paramErrorListener);
  }
  
  public JsonArrayRequest(String paramString, Response.Listener paramListener, Response.ErrorListener paramErrorListener)
  {
    super(0, paramString, null, paramListener, paramErrorListener);
  }
  
  public final Response parseNetworkResponse(NetworkResponse paramNetworkResponse)
  {
    Object localObject1;
    try
    {
      localObject1 = new java/lang/String;
      Object localObject2 = data;
      Object localObject3 = headers;
      String str = "utf-8";
      localObject3 = HttpHeaderParser.parseCharset((Map)localObject3, str);
      ((String)localObject1).<init>((byte[])localObject2, (String)localObject3);
      localObject2 = new org/json/JSONArray;
      ((JSONArray)localObject2).<init>((String)localObject1);
      paramNetworkResponse = HttpHeaderParser.parseCacheHeaders(paramNetworkResponse);
      return Response.success(localObject2, paramNetworkResponse);
    }
    catch (JSONException paramNetworkResponse)
    {
      localObject1 = new com/mopub/volley/ParseError;
      ((ParseError)localObject1).<init>(paramNetworkResponse);
      return Response.error((VolleyError)localObject1);
    }
    catch (UnsupportedEncodingException paramNetworkResponse)
    {
      localObject1 = new com/mopub/volley/ParseError;
      ((ParseError)localObject1).<init>(paramNetworkResponse);
    }
    return Response.error((VolleyError)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.JsonArrayRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */