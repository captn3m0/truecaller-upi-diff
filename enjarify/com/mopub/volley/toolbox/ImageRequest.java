package com.mopub.volley.toolbox;

import android.graphics.Bitmap.Config;
import android.widget.ImageView.ScaleType;
import com.mopub.volley.DefaultRetryPolicy;
import com.mopub.volley.Request;
import com.mopub.volley.Request.Priority;
import com.mopub.volley.Response.ErrorListener;
import com.mopub.volley.Response.Listener;

public class ImageRequest
  extends Request
{
  public static final float DEFAULT_IMAGE_BACKOFF_MULT = 2.0F;
  public static final int DEFAULT_IMAGE_MAX_RETRIES = 2;
  public static final int DEFAULT_IMAGE_TIMEOUT_MS = 1000;
  private static final Object g;
  private final Object a;
  private Response.Listener b;
  private final Bitmap.Config c;
  private final int d;
  private final int e;
  private final ImageView.ScaleType f;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    g = localObject;
  }
  
  public ImageRequest(String paramString, Response.Listener paramListener, int paramInt1, int paramInt2, Bitmap.Config paramConfig, Response.ErrorListener paramErrorListener)
  {
    this(paramString, paramListener, paramInt1, paramInt2, localScaleType, paramConfig, paramErrorListener);
  }
  
  public ImageRequest(String paramString, Response.Listener paramListener, int paramInt1, int paramInt2, ImageView.ScaleType paramScaleType, Bitmap.Config paramConfig, Response.ErrorListener paramErrorListener)
  {
    super(0, paramString, paramErrorListener);
    paramString = new java/lang/Object;
    paramString.<init>();
    a = paramString;
    paramString = new com/mopub/volley/DefaultRetryPolicy;
    paramString.<init>(1000, 2, 2.0F);
    setRetryPolicy(paramString);
    b = paramListener;
    c = paramConfig;
    d = paramInt1;
    e = paramInt2;
    f = paramScaleType;
  }
  
  private static int a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    double d1 = paramInt1;
    double d2 = paramInt3;
    Double.isNaN(d1);
    Double.isNaN(d2);
    d1 /= d2;
    double d3 = paramInt2;
    double d4 = paramInt4;
    Double.isNaN(d3);
    Double.isNaN(d4);
    d3 /= d4;
    d3 = Math.min(d1, d3);
    paramInt3 = 1065353216;
    float f2;
    for (float f1 = 1.0F;; f1 = f2)
    {
      paramInt4 = 1073741824;
      f2 = 2.0F * f1;
      d1 = f2;
      boolean bool = d1 < d3;
      if (bool) {
        break;
      }
    }
    return (int)f1;
  }
  
  private static int a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, ImageView.ScaleType paramScaleType)
  {
    if ((paramInt1 == 0) && (paramInt2 == 0)) {
      return paramInt3;
    }
    ImageView.ScaleType localScaleType1 = ImageView.ScaleType.FIT_XY;
    if (paramScaleType == localScaleType1)
    {
      if (paramInt1 == 0) {
        return paramInt3;
      }
      return paramInt1;
    }
    if (paramInt1 == 0)
    {
      double d1 = paramInt2;
      double d2 = paramInt4;
      Double.isNaN(d1);
      Double.isNaN(d2);
      d1 /= d2;
      d3 = paramInt3;
      Double.isNaN(d3);
      return (int)(d3 * d1);
    }
    if (paramInt2 == 0) {
      return paramInt1;
    }
    double d4 = paramInt4;
    double d3 = paramInt3;
    Double.isNaN(d4);
    Double.isNaN(d3);
    d4 /= d3;
    ImageView.ScaleType localScaleType2 = ImageView.ScaleType.CENTER_CROP;
    if (paramScaleType == localScaleType2)
    {
      d3 = paramInt1;
      Double.isNaN(d3);
      d3 *= d4;
      d5 = paramInt2;
      paramInt2 = d3 < d5;
      if (paramInt2 < 0)
      {
        Double.isNaN(d5);
        d5 /= d4;
        paramInt1 = (int)d5;
      }
      return paramInt1;
    }
    d3 = paramInt1;
    Double.isNaN(d3);
    d3 *= d4;
    double d5 = paramInt2;
    paramInt2 = d3 < d5;
    if (paramInt2 > 0)
    {
      Double.isNaN(d5);
      d5 /= d4;
      paramInt1 = (int)d5;
    }
    return paramInt1;
  }
  
  public void cancel()
  {
    super.cancel();
    Object localObject1 = a;
    Object localObject2 = null;
    try
    {
      b = null;
      return;
    }
    finally {}
  }
  
  public Request.Priority getPriority()
  {
    return Request.Priority.LOW;
  }
  
  /* Error */
  public final com.mopub.volley.Response parseNetworkResponse(com.mopub.volley.NetworkResponse paramNetworkResponse)
  {
    // Byte code:
    //   0: getstatic 32	com/mopub/volley/toolbox/ImageRequest:g	Ljava/lang/Object;
    //   3: astore_2
    //   4: aload_2
    //   5: monitorenter
    //   6: iconst_1
    //   7: istore_3
    //   8: aload_1
    //   9: getfield 111	com/mopub/volley/NetworkResponse:data	[B
    //   12: astore 4
    //   14: new 113	android/graphics/BitmapFactory$Options
    //   17: astore 5
    //   19: aload 5
    //   21: invokespecial 114	android/graphics/BitmapFactory$Options:<init>	()V
    //   24: aload_0
    //   25: getfield 63	com/mopub/volley/toolbox/ImageRequest:d	I
    //   28: istore 6
    //   30: iload 6
    //   32: ifne +47 -> 79
    //   35: aload_0
    //   36: getfield 65	com/mopub/volley/toolbox/ImageRequest:e	I
    //   39: istore 6
    //   41: iload 6
    //   43: ifne +36 -> 79
    //   46: aload_0
    //   47: getfield 61	com/mopub/volley/toolbox/ImageRequest:c	Landroid/graphics/Bitmap$Config;
    //   50: astore 7
    //   52: aload 5
    //   54: aload 7
    //   56: putfield 117	android/graphics/BitmapFactory$Options:inPreferredConfig	Landroid/graphics/Bitmap$Config;
    //   59: aload 4
    //   61: arraylength
    //   62: istore 6
    //   64: aload 4
    //   66: iconst_0
    //   67: iload 6
    //   69: aload 5
    //   71: invokestatic 123	android/graphics/BitmapFactory:decodeByteArray	([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   74: astore 4
    //   76: goto +202 -> 278
    //   79: aload 5
    //   81: iload_3
    //   82: putfield 127	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
    //   85: aload 4
    //   87: arraylength
    //   88: istore 6
    //   90: aload 4
    //   92: iconst_0
    //   93: iload 6
    //   95: aload 5
    //   97: invokestatic 123	android/graphics/BitmapFactory:decodeByteArray	([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   100: pop
    //   101: aload 5
    //   103: getfield 130	android/graphics/BitmapFactory$Options:outWidth	I
    //   106: istore 6
    //   108: aload 5
    //   110: getfield 133	android/graphics/BitmapFactory$Options:outHeight	I
    //   113: istore 8
    //   115: aload_0
    //   116: getfield 63	com/mopub/volley/toolbox/ImageRequest:d	I
    //   119: istore 9
    //   121: aload_0
    //   122: getfield 65	com/mopub/volley/toolbox/ImageRequest:e	I
    //   125: istore 10
    //   127: aload_0
    //   128: getfield 67	com/mopub/volley/toolbox/ImageRequest:f	Landroid/widget/ImageView$ScaleType;
    //   131: astore 11
    //   133: iload 9
    //   135: iload 10
    //   137: iload 6
    //   139: iload 8
    //   141: aload 11
    //   143: invokestatic 136	com/mopub/volley/toolbox/ImageRequest:a	(IIIILandroid/widget/ImageView$ScaleType;)I
    //   146: istore 9
    //   148: aload_0
    //   149: getfield 65	com/mopub/volley/toolbox/ImageRequest:e	I
    //   152: istore 10
    //   154: aload_0
    //   155: getfield 63	com/mopub/volley/toolbox/ImageRequest:d	I
    //   158: istore 12
    //   160: aload_0
    //   161: getfield 67	com/mopub/volley/toolbox/ImageRequest:f	Landroid/widget/ImageView$ScaleType;
    //   164: astore 13
    //   166: iload 10
    //   168: iload 12
    //   170: iload 8
    //   172: iload 6
    //   174: aload 13
    //   176: invokestatic 136	com/mopub/volley/toolbox/ImageRequest:a	(IIIILandroid/widget/ImageView$ScaleType;)I
    //   179: istore 10
    //   181: aload 5
    //   183: iconst_0
    //   184: putfield 127	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
    //   187: iload 6
    //   189: iload 8
    //   191: iload 9
    //   193: iload 10
    //   195: invokestatic 139	com/mopub/volley/toolbox/ImageRequest:a	(IIII)I
    //   198: istore 6
    //   200: aload 5
    //   202: iload 6
    //   204: putfield 142	android/graphics/BitmapFactory$Options:inSampleSize	I
    //   207: aload 4
    //   209: arraylength
    //   210: istore 6
    //   212: aload 4
    //   214: iconst_0
    //   215: iload 6
    //   217: aload 5
    //   219: invokestatic 123	android/graphics/BitmapFactory:decodeByteArray	([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   222: astore 4
    //   224: aload 4
    //   226: ifnull +52 -> 278
    //   229: aload 4
    //   231: invokevirtual 146	android/graphics/Bitmap:getWidth	()I
    //   234: istore 14
    //   236: iload 14
    //   238: iload 9
    //   240: if_icmpgt +17 -> 257
    //   243: aload 4
    //   245: invokevirtual 149	android/graphics/Bitmap:getHeight	()I
    //   248: istore 14
    //   250: iload 14
    //   252: iload 10
    //   254: if_icmple +24 -> 278
    //   257: aload 4
    //   259: iload 9
    //   261: iload 10
    //   263: iload_3
    //   264: invokestatic 153	android/graphics/Bitmap:createScaledBitmap	(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    //   267: astore 5
    //   269: aload 4
    //   271: invokevirtual 156	android/graphics/Bitmap:recycle	()V
    //   274: aload 5
    //   276: astore 4
    //   278: aload 4
    //   280: ifnonnull +23 -> 303
    //   283: new 158	com/mopub/volley/ParseError
    //   286: astore 4
    //   288: aload 4
    //   290: aload_1
    //   291: invokespecial 161	com/mopub/volley/ParseError:<init>	(Lcom/mopub/volley/NetworkResponse;)V
    //   294: aload 4
    //   296: invokestatic 167	com/mopub/volley/Response:error	(Lcom/mopub/volley/VolleyError;)Lcom/mopub/volley/Response;
    //   299: astore_1
    //   300: goto +17 -> 317
    //   303: aload_1
    //   304: invokestatic 173	com/mopub/volley/toolbox/HttpHeaderParser:parseCacheHeaders	(Lcom/mopub/volley/NetworkResponse;)Lcom/mopub/volley/Cache$Entry;
    //   307: astore 5
    //   309: aload 4
    //   311: aload 5
    //   313: invokestatic 177	com/mopub/volley/Response:success	(Ljava/lang/Object;Lcom/mopub/volley/Cache$Entry;)Lcom/mopub/volley/Response;
    //   316: astore_1
    //   317: aload_2
    //   318: monitorexit
    //   319: aload_1
    //   320: areturn
    //   321: astore_1
    //   322: goto +75 -> 397
    //   325: astore 4
    //   327: ldc -77
    //   329: astore 5
    //   331: iconst_2
    //   332: istore 6
    //   334: iload 6
    //   336: anewarray 26	java/lang/Object
    //   339: astore 7
    //   341: aload_1
    //   342: getfield 111	com/mopub/volley/NetworkResponse:data	[B
    //   345: astore_1
    //   346: aload_1
    //   347: arraylength
    //   348: istore 15
    //   350: iload 15
    //   352: invokestatic 185	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   355: astore_1
    //   356: aload 7
    //   358: iconst_0
    //   359: aload_1
    //   360: aastore
    //   361: aload_0
    //   362: invokevirtual 189	com/mopub/volley/toolbox/ImageRequest:getUrl	()Ljava/lang/String;
    //   365: astore_1
    //   366: aload 7
    //   368: iload_3
    //   369: aload_1
    //   370: aastore
    //   371: aload 5
    //   373: aload 7
    //   375: invokestatic 194	com/mopub/volley/VolleyLog:e	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   378: new 158	com/mopub/volley/ParseError
    //   381: astore_1
    //   382: aload_1
    //   383: aload 4
    //   385: invokespecial 197	com/mopub/volley/ParseError:<init>	(Ljava/lang/Throwable;)V
    //   388: aload_1
    //   389: invokestatic 167	com/mopub/volley/Response:error	(Lcom/mopub/volley/VolleyError;)Lcom/mopub/volley/Response;
    //   392: astore_1
    //   393: aload_2
    //   394: monitorexit
    //   395: aload_1
    //   396: areturn
    //   397: aload_2
    //   398: monitorexit
    //   399: aload_1
    //   400: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	401	0	this	ImageRequest
    //   0	401	1	paramNetworkResponse	com.mopub.volley.NetworkResponse
    //   3	395	2	localObject1	Object
    //   7	362	3	bool	boolean
    //   12	298	4	localObject2	Object
    //   325	59	4	localOutOfMemoryError	OutOfMemoryError
    //   17	355	5	localObject3	Object
    //   28	307	6	i	int
    //   50	324	7	localObject4	Object
    //   113	77	8	j	int
    //   119	141	9	k	int
    //   125	137	10	m	int
    //   131	11	11	localScaleType1	ImageView.ScaleType
    //   158	11	12	n	int
    //   164	11	13	localScaleType2	ImageView.ScaleType
    //   234	21	14	i1	int
    //   348	3	15	i2	int
    // Exception table:
    //   from	to	target	type
    //   8	12	321	finally
    //   14	17	321	finally
    //   19	24	321	finally
    //   24	28	321	finally
    //   35	39	321	finally
    //   46	50	321	finally
    //   54	59	321	finally
    //   59	62	321	finally
    //   69	74	321	finally
    //   81	85	321	finally
    //   85	88	321	finally
    //   95	101	321	finally
    //   101	106	321	finally
    //   108	113	321	finally
    //   115	119	321	finally
    //   121	125	321	finally
    //   127	131	321	finally
    //   141	146	321	finally
    //   148	152	321	finally
    //   154	158	321	finally
    //   160	164	321	finally
    //   174	179	321	finally
    //   183	187	321	finally
    //   193	198	321	finally
    //   202	207	321	finally
    //   207	210	321	finally
    //   217	222	321	finally
    //   229	234	321	finally
    //   243	248	321	finally
    //   263	267	321	finally
    //   269	274	321	finally
    //   283	286	321	finally
    //   290	294	321	finally
    //   294	299	321	finally
    //   303	307	321	finally
    //   311	316	321	finally
    //   317	319	321	finally
    //   334	339	321	finally
    //   341	345	321	finally
    //   346	348	321	finally
    //   350	355	321	finally
    //   359	361	321	finally
    //   361	365	321	finally
    //   369	371	321	finally
    //   373	378	321	finally
    //   378	381	321	finally
    //   383	388	321	finally
    //   388	392	321	finally
    //   393	395	321	finally
    //   397	399	321	finally
    //   8	12	325	java/lang/OutOfMemoryError
    //   14	17	325	java/lang/OutOfMemoryError
    //   19	24	325	java/lang/OutOfMemoryError
    //   24	28	325	java/lang/OutOfMemoryError
    //   35	39	325	java/lang/OutOfMemoryError
    //   46	50	325	java/lang/OutOfMemoryError
    //   54	59	325	java/lang/OutOfMemoryError
    //   59	62	325	java/lang/OutOfMemoryError
    //   69	74	325	java/lang/OutOfMemoryError
    //   81	85	325	java/lang/OutOfMemoryError
    //   85	88	325	java/lang/OutOfMemoryError
    //   95	101	325	java/lang/OutOfMemoryError
    //   101	106	325	java/lang/OutOfMemoryError
    //   108	113	325	java/lang/OutOfMemoryError
    //   115	119	325	java/lang/OutOfMemoryError
    //   121	125	325	java/lang/OutOfMemoryError
    //   127	131	325	java/lang/OutOfMemoryError
    //   141	146	325	java/lang/OutOfMemoryError
    //   148	152	325	java/lang/OutOfMemoryError
    //   154	158	325	java/lang/OutOfMemoryError
    //   160	164	325	java/lang/OutOfMemoryError
    //   174	179	325	java/lang/OutOfMemoryError
    //   183	187	325	java/lang/OutOfMemoryError
    //   193	198	325	java/lang/OutOfMemoryError
    //   202	207	325	java/lang/OutOfMemoryError
    //   207	210	325	java/lang/OutOfMemoryError
    //   217	222	325	java/lang/OutOfMemoryError
    //   229	234	325	java/lang/OutOfMemoryError
    //   243	248	325	java/lang/OutOfMemoryError
    //   263	267	325	java/lang/OutOfMemoryError
    //   269	274	325	java/lang/OutOfMemoryError
    //   283	286	325	java/lang/OutOfMemoryError
    //   290	294	325	java/lang/OutOfMemoryError
    //   294	299	325	java/lang/OutOfMemoryError
    //   303	307	325	java/lang/OutOfMemoryError
    //   311	316	325	java/lang/OutOfMemoryError
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.ImageRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */