package com.mopub.volley.toolbox;

import com.google.firebase.perf.network.FirebasePerfUrlConnection;
import com.mopub.volley.Header;
import com.mopub.volley.Request;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;

public class HurlStack
  extends BaseHttpStack
{
  private final HurlStack.UrlRewriter a;
  private final SSLSocketFactory b;
  
  public HurlStack()
  {
    this(null);
  }
  
  public HurlStack(HurlStack.UrlRewriter paramUrlRewriter)
  {
    this(paramUrlRewriter, null);
  }
  
  public HurlStack(HurlStack.UrlRewriter paramUrlRewriter, SSLSocketFactory paramSSLSocketFactory)
  {
    a = paramUrlRewriter;
    b = paramSSLSocketFactory;
  }
  
  private static InputStream a(HttpURLConnection paramHttpURLConnection)
  {
    try
    {
      paramHttpURLConnection = paramHttpURLConnection.getInputStream();
    }
    catch (IOException localIOException)
    {
      paramHttpURLConnection = paramHttpURLConnection.getErrorStream();
    }
    return paramHttpURLConnection;
  }
  
  private static List a(Map paramMap)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    int i = paramMap.size();
    localArrayList.<init>(i);
    paramMap = paramMap.entrySet().iterator();
    Map.Entry localEntry;
    do
    {
      boolean bool1 = paramMap.hasNext();
      if (!bool1) {
        break;
      }
      localEntry = (Map.Entry)paramMap.next();
      localObject = localEntry.getKey();
    } while (localObject == null);
    Object localObject = ((List)localEntry.getValue()).iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject).hasNext();
      if (!bool2) {
        break;
      }
      String str1 = (String)((Iterator)localObject).next();
      Header localHeader = new com/mopub/volley/Header;
      String str2 = (String)localEntry.getKey();
      localHeader.<init>(str2, str1);
      localArrayList.add(localHeader);
    }
    return localArrayList;
  }
  
  private static void a(HttpURLConnection paramHttpURLConnection, Request paramRequest)
  {
    byte[] arrayOfByte = paramRequest.getBody();
    if (arrayOfByte != null) {
      a(paramHttpURLConnection, paramRequest, arrayOfByte);
    }
  }
  
  private static void a(HttpURLConnection paramHttpURLConnection, Request paramRequest, byte[] paramArrayOfByte)
  {
    paramHttpURLConnection.setDoOutput(true);
    paramRequest = paramRequest.getBodyContentType();
    paramHttpURLConnection.addRequestProperty("Content-Type", paramRequest);
    paramRequest = new java/io/DataOutputStream;
    paramHttpURLConnection = paramHttpURLConnection.getOutputStream();
    paramRequest.<init>(paramHttpURLConnection);
    paramRequest.write(paramArrayOfByte);
    paramRequest.close();
  }
  
  public HttpResponse executeRequest(Request paramRequest, Map paramMap)
  {
    Object localObject1 = paramRequest.getUrl();
    Object localObject2 = new java/util/HashMap;
    ((HashMap)localObject2).<init>();
    Map localMap = paramRequest.getHeaders();
    ((HashMap)localObject2).putAll(localMap);
    ((HashMap)localObject2).putAll(paramMap);
    paramMap = a;
    if (paramMap != null)
    {
      paramMap = paramMap.rewriteUrl((String)localObject1);
      if (paramMap == null)
      {
        paramRequest = new java/io/IOException;
        paramMap = String.valueOf(localObject1);
        paramMap = "URL blocked by rewriter: ".concat(paramMap);
        paramRequest.<init>(paramMap);
        throw paramRequest;
      }
    }
    else
    {
      paramMap = (Map)localObject1;
    }
    localObject1 = new java/net/URL;
    ((URL)localObject1).<init>(paramMap);
    paramMap = (HttpURLConnection)FirebasePerfUrlConnection.instrument(((URL)localObject1).openConnection());
    boolean bool1 = HttpURLConnection.getFollowRedirects();
    paramMap.setInstanceFollowRedirects(bool1);
    int i = paramRequest.getTimeoutMs();
    paramMap.setConnectTimeout(i);
    paramMap.setReadTimeout(i);
    i = 0;
    localMap = null;
    paramMap.setUseCaches(false);
    boolean bool2 = true;
    paramMap.setDoInput(bool2);
    Object localObject3 = "https";
    localObject1 = ((URL)localObject1).getProtocol();
    boolean bool3 = ((String)localObject3).equals(localObject1);
    if (bool3)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        localObject3 = paramMap;
        localObject3 = (HttpsURLConnection)paramMap;
        ((HttpsURLConnection)localObject3).setSSLSocketFactory((SSLSocketFactory)localObject1);
      }
    }
    localObject1 = ((HashMap)localObject2).keySet().iterator();
    for (;;)
    {
      boolean bool4 = ((Iterator)localObject1).hasNext();
      if (!bool4) {
        break;
      }
      localObject3 = (String)((Iterator)localObject1).next();
      String str = (String)((HashMap)localObject2).get(localObject3);
      paramMap.addRequestProperty((String)localObject3, str);
    }
    int j = paramRequest.getMethod();
    switch (j)
    {
    default: 
      paramRequest = new java/lang/IllegalStateException;
      paramRequest.<init>("Unknown method type.");
      throw paramRequest;
    case 7: 
      localObject1 = "PATCH";
      paramMap.setRequestMethod((String)localObject1);
      a(paramMap, paramRequest);
      break;
    case 6: 
      localObject1 = "TRACE";
      paramMap.setRequestMethod((String)localObject1);
      break;
    case 5: 
      localObject1 = "OPTIONS";
      paramMap.setRequestMethod((String)localObject1);
      break;
    case 4: 
      localObject1 = "HEAD";
      paramMap.setRequestMethod((String)localObject1);
      break;
    case 3: 
      localObject1 = "DELETE";
      paramMap.setRequestMethod((String)localObject1);
      break;
    case 2: 
      localObject1 = "PUT";
      paramMap.setRequestMethod((String)localObject1);
      a(paramMap, paramRequest);
      break;
    case 1: 
      localObject1 = "POST";
      paramMap.setRequestMethod((String)localObject1);
      a(paramMap, paramRequest);
      break;
    case 0: 
      localObject1 = "GET";
      paramMap.setRequestMethod((String)localObject1);
      break;
    case -1: 
      localObject1 = paramRequest.getPostBody();
      if (localObject1 != null)
      {
        localObject2 = "POST";
        paramMap.setRequestMethod((String)localObject2);
        a(paramMap, paramRequest, (byte[])localObject1);
      }
      break;
    }
    j = paramMap.getResponseCode();
    int k = -1;
    if (j != k)
    {
      int m = paramRequest.getMethod();
      k = 4;
      if (m != k)
      {
        m = 100;
        if (m <= j)
        {
          m = 200;
          if (j < m) {}
        }
        else
        {
          m = 204;
          if (j != m)
          {
            m = 304;
            if (j != m) {
              i = 1;
            }
          }
        }
      }
      if (i == 0)
      {
        paramRequest = new com/mopub/volley/toolbox/HttpResponse;
        paramMap = a(paramMap.getHeaderFields());
        paramRequest.<init>(j, paramMap);
        return paramRequest;
      }
      paramRequest = new com/mopub/volley/toolbox/HttpResponse;
      localObject2 = a(paramMap.getHeaderFields());
      i = paramMap.getContentLength();
      paramMap = a(paramMap);
      paramRequest.<init>(j, (List)localObject2, i, paramMap);
      return paramRequest;
    }
    paramRequest = new java/io/IOException;
    paramRequest.<init>("Could not retrieve response code from HttpUrlConnection.");
    throw paramRequest;
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.HurlStack
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */