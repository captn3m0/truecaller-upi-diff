package com.mopub.volley.toolbox;

public abstract interface Authenticator
{
  public abstract String getAuthToken();
  
  public abstract void invalidateAuthToken(String paramString);
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.Authenticator
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */