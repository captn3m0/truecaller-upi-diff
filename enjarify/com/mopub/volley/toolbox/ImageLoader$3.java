package com.mopub.volley.toolbox;

import com.mopub.volley.Response.ErrorListener;
import com.mopub.volley.VolleyError;
import java.util.HashMap;

final class ImageLoader$3
  implements Response.ErrorListener
{
  ImageLoader$3(ImageLoader paramImageLoader, String paramString) {}
  
  public final void onErrorResponse(VolleyError paramVolleyError)
  {
    ImageLoader localImageLoader = b;
    String str = a;
    ImageLoader.a locala = (ImageLoader.a)b.remove(str);
    if (locala != null)
    {
      locala.setError(paramVolleyError);
      localImageLoader.a(str, locala);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.ImageLoader.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */