package com.mopub.volley.toolbox;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.http.AndroidHttpClient;
import android.os.Build.VERSION;
import com.mopub.volley.Network;
import com.mopub.volley.RequestQueue;
import java.io.File;

public class Volley
{
  private static RequestQueue a(Context paramContext, Network paramNetwork)
  {
    File localFile = new java/io/File;
    paramContext = paramContext.getCacheDir();
    localFile.<init>(paramContext, "volley");
    paramContext = new com/mopub/volley/RequestQueue;
    DiskBasedCache localDiskBasedCache = new com/mopub/volley/toolbox/DiskBasedCache;
    localDiskBasedCache.<init>(localFile);
    paramContext.<init>(localDiskBasedCache, paramNetwork);
    paramContext.start();
    return paramContext;
  }
  
  public static RequestQueue newRequestQueue(Context paramContext)
  {
    return newRequestQueue(paramContext, null);
  }
  
  public static RequestQueue newRequestQueue(Context paramContext, BaseHttpStack paramBaseHttpStack)
  {
    int j;
    if (paramBaseHttpStack == null)
    {
      int i = Build.VERSION.SDK_INT;
      j = 9;
      if (i >= j)
      {
        paramBaseHttpStack = new com/mopub/volley/toolbox/BasicNetwork;
        localObject1 = new com/mopub/volley/toolbox/HurlStack;
        ((HurlStack)localObject1).<init>();
        paramBaseHttpStack.<init>((BaseHttpStack)localObject1);
        localObject1 = paramBaseHttpStack;
        break label161;
      }
      paramBaseHttpStack = "volley/0";
    }
    try
    {
      localObject1 = paramContext.getPackageName();
      localObject2 = paramContext.getPackageManager();
      StringBuilder localStringBuilder = null;
      localObject2 = ((PackageManager)localObject2).getPackageInfo((String)localObject1, 0);
      localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append((String)localObject1);
      localObject1 = "/";
      localStringBuilder.append((String)localObject1);
      j = versionCode;
      localStringBuilder.append(j);
      paramBaseHttpStack = localStringBuilder.toString();
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      Object localObject2;
      for (;;) {}
    }
    Object localObject1 = new com/mopub/volley/toolbox/BasicNetwork;
    localObject2 = new com/mopub/volley/toolbox/HttpClientStack;
    paramBaseHttpStack = AndroidHttpClient.newInstance(paramBaseHttpStack);
    ((HttpClientStack)localObject2).<init>(paramBaseHttpStack);
    ((BasicNetwork)localObject1).<init>((HttpStack)localObject2);
    break label161;
    localObject1 = new com/mopub/volley/toolbox/BasicNetwork;
    ((BasicNetwork)localObject1).<init>(paramBaseHttpStack);
    label161:
    return a(paramContext, (Network)localObject1);
  }
  
  public static RequestQueue newRequestQueue(Context paramContext, HttpStack paramHttpStack)
  {
    if (paramHttpStack == null) {
      return newRequestQueue(paramContext, null);
    }
    BasicNetwork localBasicNetwork = new com/mopub/volley/toolbox/BasicNetwork;
    localBasicNetwork.<init>(paramHttpStack);
    return a(paramContext, localBasicNetwork);
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.Volley
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */