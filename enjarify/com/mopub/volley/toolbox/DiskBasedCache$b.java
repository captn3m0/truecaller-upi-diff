package com.mopub.volley.toolbox;

import java.io.FilterInputStream;
import java.io.InputStream;

final class DiskBasedCache$b
  extends FilterInputStream
{
  private final long a;
  private long b;
  
  DiskBasedCache$b(InputStream paramInputStream, long paramLong)
  {
    super(paramInputStream);
    a = paramLong;
  }
  
  final long a()
  {
    long l1 = a;
    long l2 = b;
    return l1 - l2;
  }
  
  public final int read()
  {
    int i = super.read();
    int j = -1;
    if (i != j)
    {
      long l1 = b;
      long l2 = 1L;
      l1 += l2;
      b = l1;
    }
    return i;
  }
  
  public final int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = super.read(paramArrayOfByte, paramInt1, paramInt2);
    paramInt1 = -1;
    if (i != paramInt1)
    {
      long l1 = b;
      long l2 = i;
      l1 += l2;
      b = l1;
    }
    return i;
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.toolbox.DiskBasedCache.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */