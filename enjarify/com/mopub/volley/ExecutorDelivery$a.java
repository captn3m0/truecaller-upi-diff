package com.mopub.volley;

final class ExecutorDelivery$a
  implements Runnable
{
  private final Request b;
  private final Response c;
  private final Runnable d;
  
  public ExecutorDelivery$a(ExecutorDelivery paramExecutorDelivery, Request paramRequest, Response paramResponse, Runnable paramRunnable)
  {
    b = paramRequest;
    c = paramResponse;
    d = paramRunnable;
  }
  
  public final void run()
  {
    Object localObject1 = b;
    boolean bool = ((Request)localObject1).isCanceled();
    if (bool)
    {
      b.a("canceled-at-delivery");
      return;
    }
    localObject1 = c;
    bool = ((Response)localObject1).isSuccess();
    Object localObject2;
    if (bool)
    {
      localObject1 = b;
      localObject2 = c.result;
      ((Request)localObject1).deliverResponse(localObject2);
    }
    else
    {
      localObject1 = b;
      localObject2 = c.error;
      ((Request)localObject1).deliverError((VolleyError)localObject2);
    }
    localObject1 = c;
    bool = intermediate;
    if (bool)
    {
      localObject1 = b;
      localObject2 = "intermediate-response";
      ((Request)localObject1).addMarker((String)localObject2);
    }
    else
    {
      localObject1 = b;
      localObject2 = "done";
      ((Request)localObject1).a((String)localObject2);
    }
    localObject1 = d;
    if (localObject1 != null) {
      ((Runnable)localObject1).run();
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.ExecutorDelivery.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */