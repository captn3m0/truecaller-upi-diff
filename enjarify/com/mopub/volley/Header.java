package com.mopub.volley;

import android.text.TextUtils;

public final class Header
{
  private final String a;
  private final String b;
  
  public Header(String paramString1, String paramString2)
  {
    a = paramString1;
    b = paramString2;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    if (paramObject != null)
    {
      Object localObject1 = getClass();
      Object localObject2 = paramObject.getClass();
      if (localObject1 == localObject2)
      {
        paramObject = (Header)paramObject;
        localObject1 = a;
        localObject2 = a;
        boolean bool2 = TextUtils.equals((CharSequence)localObject1, (CharSequence)localObject2);
        if (bool2)
        {
          localObject1 = b;
          paramObject = b;
          boolean bool3 = TextUtils.equals((CharSequence)localObject1, (CharSequence)paramObject);
          if (bool3) {
            return bool1;
          }
        }
        return false;
      }
    }
    return false;
  }
  
  public final String getName()
  {
    return a;
  }
  
  public final String getValue()
  {
    return b;
  }
  
  public final int hashCode()
  {
    int i = a.hashCode() * 31;
    int j = b.hashCode();
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Header[name=");
    String str = a;
    localStringBuilder.append(str);
    localStringBuilder.append(",value=");
    str = b;
    localStringBuilder.append(str);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.Header
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */