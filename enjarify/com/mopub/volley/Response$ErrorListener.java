package com.mopub.volley;

public abstract interface Response$ErrorListener
{
  public abstract void onErrorResponse(VolleyError paramVolleyError);
}

/* Location:
 * Qualified Name:     com.mopub.volley.Response.ErrorListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */