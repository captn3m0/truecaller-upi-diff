package com.mopub.volley;

public abstract interface RetryPolicy
{
  public abstract int getCurrentRetryCount();
  
  public abstract int getCurrentTimeout();
  
  public abstract void retry(VolleyError paramVolleyError);
}

/* Location:
 * Qualified Name:     com.mopub.volley.RetryPolicy
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */