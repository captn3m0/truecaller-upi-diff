package com.mopub.volley;

public class DefaultRetryPolicy
  implements RetryPolicy
{
  public static final float DEFAULT_BACKOFF_MULT = 1.0F;
  public static final int DEFAULT_MAX_RETRIES = 1;
  public static final int DEFAULT_TIMEOUT_MS = 2500;
  private int a;
  private int b;
  private final int c;
  private final float d;
  
  public DefaultRetryPolicy()
  {
    this(2500, 1, 1.0F);
  }
  
  public DefaultRetryPolicy(int paramInt1, int paramInt2, float paramFloat)
  {
    a = paramInt1;
    c = paramInt2;
    d = paramFloat;
  }
  
  public float getBackoffMultiplier()
  {
    return d;
  }
  
  public int getCurrentRetryCount()
  {
    return b;
  }
  
  public int getCurrentTimeout()
  {
    return a;
  }
  
  public void retry(VolleyError paramVolleyError)
  {
    int i = b;
    int j = 1;
    i += j;
    b = i;
    i = a;
    float f1 = i;
    float f2 = i;
    float f3 = d;
    f2 *= f3;
    f1 += f2;
    i = (int)f1;
    a = i;
    i = b;
    int k = c;
    if (i > k) {
      j = 0;
    }
    if (j != 0) {
      return;
    }
    throw paramVolleyError;
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.DefaultRetryPolicy
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */