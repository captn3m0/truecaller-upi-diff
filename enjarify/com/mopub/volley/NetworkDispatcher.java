package com.mopub.volley;

import android.net.TrafficStats;
import android.os.Build.VERSION;
import android.os.Process;
import android.os.SystemClock;
import java.util.concurrent.BlockingQueue;

public class NetworkDispatcher
  extends Thread
{
  private final BlockingQueue a;
  private final Network b;
  private final Cache c;
  private final ResponseDelivery d;
  private volatile boolean e = false;
  
  public NetworkDispatcher(BlockingQueue paramBlockingQueue, Network paramNetwork, Cache paramCache, ResponseDelivery paramResponseDelivery)
  {
    a = paramBlockingQueue;
    b = paramNetwork;
    c = paramCache;
    d = paramResponseDelivery;
  }
  
  public void quit()
  {
    e = true;
    interrupt();
  }
  
  public void run()
  {
    int i = 10;
    Process.setThreadPriority(i);
    try
    {
      boolean bool1;
      do
      {
        for (;;)
        {
          Object localObject1 = a;
          localObject1 = ((BlockingQueue)localObject1).take();
          localObject1 = (Request)localObject1;
          long l1 = SystemClock.elapsedRealtime();
          Object localObject2 = "network-queue-take";
          try
          {
            ((Request)localObject1).addMarker((String)localObject2);
            boolean bool2 = ((Request)localObject1).isCanceled();
            if (bool2)
            {
              localObject2 = "network-discard-cancelled";
              ((Request)localObject1).a((String)localObject2);
              ((Request)localObject1).a();
            }
            else
            {
              int j = Build.VERSION.SDK_INT;
              int k = 14;
              if (j >= k)
              {
                j = ((Request)localObject1).getTrafficStatsTag();
                TrafficStats.setThreadStatsTag(j);
              }
              localObject2 = b;
              localObject2 = ((Network)localObject2).performRequest((Request)localObject1);
              localObject3 = "network-http-complete";
              ((Request)localObject1).addMarker((String)localObject3);
              boolean bool3 = notModified;
              if (bool3)
              {
                bool3 = ((Request)localObject1).hasHadResponseDelivered();
                if (bool3)
                {
                  localObject2 = "not-modified";
                  ((Request)localObject1).a((String)localObject2);
                  ((Request)localObject1).a();
                  continue;
                }
              }
              localObject2 = ((Request)localObject1).parseNetworkResponse((NetworkResponse)localObject2);
              localObject3 = "network-parse-complete";
              ((Request)localObject1).addMarker((String)localObject3);
              bool3 = ((Request)localObject1).shouldCache();
              if (bool3)
              {
                localObject3 = cacheEntry;
                if (localObject3 != null)
                {
                  localObject3 = c;
                  localObject4 = ((Request)localObject1).getCacheKey();
                  localEntry = cacheEntry;
                  ((Cache)localObject3).put((String)localObject4, localEntry);
                  localObject3 = "network-cache-written";
                  ((Request)localObject1).addMarker((String)localObject3);
                }
              }
              ((Request)localObject1).markDelivered();
              localObject3 = d;
              ((ResponseDelivery)localObject3).postResponse((Request)localObject1, (Response)localObject2);
              ((Request)localObject1).a((Response)localObject2);
            }
          }
          catch (Exception localException)
          {
            Object localObject3 = "Unhandled exception %s";
            int m = 1;
            Object localObject4 = new Object[m];
            Cache.Entry localEntry = null;
            String str = localException.toString();
            localObject4[0] = str;
            VolleyLog.e(localException, (String)localObject3, (Object[])localObject4);
            localObject3 = new com/mopub/volley/VolleyError;
            ((VolleyError)localObject3).<init>(localException);
            long l2 = SystemClock.elapsedRealtime() - l1;
            a = l2;
            localObject5 = d;
            ((ResponseDelivery)localObject5).postError((Request)localObject1, (VolleyError)localObject3);
            ((Request)localObject1).a();
          }
          catch (VolleyError localVolleyError)
          {
            long l3 = SystemClock.elapsedRealtime() - l1;
            a = l3;
            Object localObject5 = Request.a(localVolleyError);
            ResponseDelivery localResponseDelivery = d;
            localResponseDelivery.postError((Request)localObject1, (VolleyError)localObject5);
            ((Request)localObject1).a();
          }
        }
      } while (!bool1);
    }
    catch (InterruptedException localInterruptedException)
    {
      bool1 = e;
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.NetworkDispatcher
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */