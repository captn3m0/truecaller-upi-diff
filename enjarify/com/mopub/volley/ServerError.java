package com.mopub.volley;

public class ServerError
  extends VolleyError
{
  public ServerError() {}
  
  public ServerError(NetworkResponse paramNetworkResponse)
  {
    super(paramNetworkResponse);
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.ServerError
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */