package com.mopub.volley;

import android.content.Intent;

public class AuthFailureError
  extends VolleyError
{
  private Intent b;
  
  public AuthFailureError() {}
  
  public AuthFailureError(Intent paramIntent)
  {
    b = paramIntent;
  }
  
  public AuthFailureError(NetworkResponse paramNetworkResponse)
  {
    super(paramNetworkResponse);
  }
  
  public AuthFailureError(String paramString)
  {
    super(paramString);
  }
  
  public AuthFailureError(String paramString, Exception paramException)
  {
    super(paramString, paramException);
  }
  
  public String getMessage()
  {
    Intent localIntent = b;
    if (localIntent != null) {
      return "User needs to (re)enter credentials.";
    }
    return super.getMessage();
  }
  
  public Intent getResolutionIntent()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.AuthFailureError
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */