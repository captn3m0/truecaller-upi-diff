package com.mopub.volley;

import java.util.concurrent.BlockingQueue;

final class CacheDispatcher$1
  implements Runnable
{
  CacheDispatcher$1(CacheDispatcher paramCacheDispatcher, Request paramRequest) {}
  
  public final void run()
  {
    try
    {
      Object localObject = b;
      localObject = CacheDispatcher.a((CacheDispatcher)localObject);
      Request localRequest = a;
      ((BlockingQueue)localObject).put(localRequest);
      return;
    }
    catch (InterruptedException localInterruptedException)
    {
      Thread.currentThread().interrupt();
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.CacheDispatcher.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */