package com.mopub.volley;

public abstract interface Network
{
  public abstract NetworkResponse performRequest(Request paramRequest);
}

/* Location:
 * Qualified Name:     com.mopub.volley.Network
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */