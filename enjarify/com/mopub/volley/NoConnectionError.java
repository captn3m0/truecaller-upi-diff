package com.mopub.volley;

public class NoConnectionError
  extends NetworkError
{
  public NoConnectionError() {}
  
  public NoConnectionError(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.NoConnectionError
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */