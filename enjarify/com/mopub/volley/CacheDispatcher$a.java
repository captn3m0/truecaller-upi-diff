package com.mopub.volley;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

final class CacheDispatcher$a
  implements Request.a
{
  private final Map a;
  private final CacheDispatcher b;
  
  CacheDispatcher$a(CacheDispatcher paramCacheDispatcher)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    a = localHashMap;
    b = paramCacheDispatcher;
  }
  
  final boolean a(Request paramRequest)
  {
    try
    {
      String str1 = paramRequest.getCacheKey();
      Object localObject = a;
      boolean bool1 = ((Map)localObject).containsKey(str1);
      boolean bool2 = true;
      if (bool1)
      {
        localObject = a;
        localObject = ((Map)localObject).get(str1);
        localObject = (List)localObject;
        if (localObject == null)
        {
          localObject = new java/util/ArrayList;
          ((ArrayList)localObject).<init>();
        }
        str2 = "waiting-for-response";
        paramRequest.addMarker(str2);
        ((List)localObject).add(paramRequest);
        paramRequest = a;
        paramRequest.put(str1, localObject);
        bool3 = VolleyLog.DEBUG;
        if (bool3)
        {
          paramRequest = "Request for cacheKey=%s is in flight, putting on hold.";
          localObject = new Object[bool2];
          localObject[0] = str1;
          VolleyLog.d(paramRequest, (Object[])localObject);
        }
        return bool2;
      }
      localObject = a;
      String str2 = null;
      ((Map)localObject).put(str1, null);
      paramRequest.a(this);
      boolean bool3 = VolleyLog.DEBUG;
      if (bool3)
      {
        paramRequest = "new request, sending to network %s";
        localObject = new Object[bool2];
        localObject[0] = str1;
        VolleyLog.d(paramRequest, (Object[])localObject);
      }
      return false;
    }
    finally {}
  }
  
  public final void onNoUsableResponseReceived(Request paramRequest)
  {
    try
    {
      paramRequest = paramRequest.getCacheKey();
      Object localObject1 = a;
      localObject1 = ((Map)localObject1).remove(paramRequest);
      localObject1 = (List)localObject1;
      if (localObject1 != null)
      {
        boolean bool = ((List)localObject1).isEmpty();
        if (!bool)
        {
          bool = VolleyLog.DEBUG;
          int i = 1;
          if (bool)
          {
            localObject2 = "%d waiting requests for cacheKey=%s; resend to network";
            int j = 2;
            localObject3 = new Object[j];
            int k = ((List)localObject1).size();
            Integer localInteger = Integer.valueOf(k);
            localObject3[0] = localInteger;
            localObject3[i] = paramRequest;
            VolleyLog.v((String)localObject2, (Object[])localObject3);
          }
          Object localObject2 = ((List)localObject1).remove(0);
          localObject2 = (Request)localObject2;
          Object localObject3 = a;
          ((Map)localObject3).put(paramRequest, localObject1);
          ((Request)localObject2).a(this);
          try
          {
            paramRequest = b;
            paramRequest = CacheDispatcher.a(paramRequest);
            paramRequest.put(localObject2);
            return;
          }
          catch (InterruptedException paramRequest)
          {
            localObject1 = "Couldn't add request to queue. %s";
            localObject2 = new Object[i];
            paramRequest = paramRequest.toString();
            localObject2[0] = paramRequest;
            VolleyLog.e((String)localObject1, (Object[])localObject2);
            paramRequest = Thread.currentThread();
            paramRequest.interrupt();
            paramRequest = b;
            paramRequest.quit();
          }
        }
      }
      return;
    }
    finally {}
  }
  
  public final void onResponseReceived(Request paramRequest, Response paramResponse)
  {
    Object localObject1 = cacheEntry;
    if (localObject1 != null)
    {
      localObject1 = cacheEntry;
      boolean bool1 = ((Cache.Entry)localObject1).isExpired();
      if (!bool1)
      {
        paramRequest = paramRequest.getCacheKey();
        try
        {
          localObject1 = a;
          localObject1 = ((Map)localObject1).remove(paramRequest);
          localObject1 = (List)localObject1;
          if (localObject1 != null)
          {
            boolean bool2 = VolleyLog.DEBUG;
            Object localObject2;
            if (bool2)
            {
              localObject2 = "Releasing %d waiting requests for cacheKey=%s.";
              int i = 2;
              Object[] arrayOfObject = new Object[i];
              int j = ((List)localObject1).size();
              Integer localInteger = Integer.valueOf(j);
              arrayOfObject[0] = localInteger;
              int k = 1;
              arrayOfObject[k] = paramRequest;
              VolleyLog.v((String)localObject2, arrayOfObject);
            }
            paramRequest = ((List)localObject1).iterator();
            for (;;)
            {
              bool1 = paramRequest.hasNext();
              if (!bool1) {
                break;
              }
              localObject1 = (Request)paramRequest.next();
              localObject2 = CacheDispatcher.b(b);
              ((ResponseDelivery)localObject2).postResponse((Request)localObject1, paramResponse);
            }
          }
          return;
        }
        finally {}
      }
    }
    onNoUsableResponseReceived(paramRequest);
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.CacheDispatcher.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */