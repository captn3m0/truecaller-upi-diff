package com.mopub.volley;

public class ClientError
  extends ServerError
{
  public ClientError() {}
  
  public ClientError(NetworkResponse paramNetworkResponse)
  {
    super(paramNetworkResponse);
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.ClientError
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */