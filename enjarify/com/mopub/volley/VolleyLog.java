package com.mopub.volley;

import android.util.Log;
import java.util.Locale;

public class VolleyLog
{
  public static boolean DEBUG = Log.isLoggable("Volley", 2);
  public static String TAG = "Volley";
  private static final String a = VolleyLog.class.getName();
  
  private static String a(String paramString, Object... paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      localObject1 = Locale.US;
      paramString = String.format((Locale)localObject1, paramString, paramVarArgs);
    }
    paramVarArgs = new java/lang/Throwable;
    paramVarArgs.<init>();
    paramVarArgs = paramVarArgs.fillInStackTrace().getStackTrace();
    Object localObject1 = "<unknown>";
    int i = 2;
    int j = 2;
    int n;
    for (;;)
    {
      int k = paramVarArgs.length;
      n = 1;
      if (j >= k) {
        break;
      }
      localObject2 = paramVarArgs[j].getClassName();
      String str = a;
      boolean bool = ((String)localObject2).equals(str);
      if (!bool)
      {
        localObject1 = paramVarArgs[j].getClassName();
        int m = ((String)localObject1).lastIndexOf('.') + n;
        localObject1 = ((String)localObject1).substring(m);
        m = ((String)localObject1).lastIndexOf('$') + n;
        localObject1 = ((String)localObject1).substring(m);
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        ((StringBuilder)localObject2).append((String)localObject1);
        ((StringBuilder)localObject2).append(".");
        paramVarArgs = paramVarArgs[j].getMethodName();
        ((StringBuilder)localObject2).append(paramVarArgs);
        localObject1 = ((StringBuilder)localObject2).toString();
        break;
      }
      j += 1;
    }
    paramVarArgs = Locale.US;
    Object localObject2 = new Object[3];
    Long localLong = Long.valueOf(Thread.currentThread().getId());
    localObject2[0] = localLong;
    localObject2[n] = localObject1;
    localObject2[i] = paramString;
    return String.format(paramVarArgs, "[%d] %s: %s", (Object[])localObject2);
  }
  
  public static void d(String paramString, Object... paramVarArgs)
  {
    a(paramString, paramVarArgs);
  }
  
  public static void e(String paramString, Object... paramVarArgs)
  {
    a(paramString, paramVarArgs);
  }
  
  public static void e(Throwable paramThrowable, String paramString, Object... paramVarArgs)
  {
    a(paramString, paramVarArgs);
  }
  
  public static void setTag(String paramString)
  {
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramString;
    d("Changing log tag to %s", arrayOfObject);
    TAG = paramString;
    DEBUG = Log.isLoggable(paramString, 2);
  }
  
  public static void v(String paramString, Object... paramVarArgs)
  {
    boolean bool = DEBUG;
    if (bool) {
      a(paramString, paramVarArgs);
    }
  }
  
  public static void wtf(String paramString, Object... paramVarArgs)
  {
    String str = TAG;
    paramString = a(paramString, paramVarArgs);
    Log.wtf(str, paramString);
  }
  
  public static void wtf(Throwable paramThrowable, String paramString, Object... paramVarArgs)
  {
    String str = TAG;
    paramString = a(paramString, paramVarArgs);
    Log.wtf(str, paramString, paramThrowable);
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.VolleyLog
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */