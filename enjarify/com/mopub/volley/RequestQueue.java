package com.mopub.volley;

import android.os.Handler;
import android.os.Looper;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class RequestQueue
{
  private final AtomicInteger a;
  private final Set b;
  private final PriorityBlockingQueue c;
  private final PriorityBlockingQueue d;
  private final Cache e;
  private final Network f;
  private final ResponseDelivery g;
  private final NetworkDispatcher[] h;
  private CacheDispatcher i;
  private final List j;
  
  public RequestQueue(Cache paramCache, Network paramNetwork)
  {
    this(paramCache, paramNetwork, 4);
  }
  
  public RequestQueue(Cache paramCache, Network paramNetwork, int paramInt)
  {
    this(paramCache, paramNetwork, paramInt, localExecutorDelivery);
  }
  
  public RequestQueue(Cache paramCache, Network paramNetwork, int paramInt, ResponseDelivery paramResponseDelivery)
  {
    Object localObject = new java/util/concurrent/atomic/AtomicInteger;
    ((AtomicInteger)localObject).<init>();
    a = ((AtomicInteger)localObject);
    localObject = new java/util/HashSet;
    ((HashSet)localObject).<init>();
    b = ((Set)localObject);
    localObject = new java/util/concurrent/PriorityBlockingQueue;
    ((PriorityBlockingQueue)localObject).<init>();
    c = ((PriorityBlockingQueue)localObject);
    localObject = new java/util/concurrent/PriorityBlockingQueue;
    ((PriorityBlockingQueue)localObject).<init>();
    d = ((PriorityBlockingQueue)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    j = ((List)localObject);
    e = paramCache;
    f = paramNetwork;
    paramCache = new NetworkDispatcher[paramInt];
    h = paramCache;
    g = paramResponseDelivery;
  }
  
  final void a(Request paramRequest)
  {
    synchronized (b)
    {
      ??? = b;
      ((Set)???).remove(paramRequest);
      synchronized (j)
      {
        ??? = j;
        ??? = ((List)???).iterator();
        for (;;)
        {
          boolean bool = ((Iterator)???).hasNext();
          if (!bool) {
            break;
          }
          Object localObject3 = ((Iterator)???).next();
          localObject3 = (RequestQueue.RequestFinishedListener)localObject3;
          ((RequestQueue.RequestFinishedListener)localObject3).onRequestFinished(paramRequest);
        }
        return;
      }
    }
  }
  
  public Request add(Request paramRequest)
  {
    paramRequest.setRequestQueue(this);
    synchronized (b)
    {
      Set localSet = b;
      localSet.add(paramRequest);
      int k = getSequenceNumber();
      paramRequest.setSequence(k);
      ??? = "add-to-queue";
      paramRequest.addMarker((String)???);
      boolean bool = paramRequest.shouldCache();
      if (!bool)
      {
        d.add(paramRequest);
        return paramRequest;
      }
      c.add(paramRequest);
      return paramRequest;
    }
  }
  
  public void addRequestFinishedListener(RequestQueue.RequestFinishedListener paramRequestFinishedListener)
  {
    synchronized (j)
    {
      List localList2 = j;
      localList2.add(paramRequestFinishedListener);
      return;
    }
  }
  
  public void cancelAll(RequestQueue.RequestFilter paramRequestFilter)
  {
    synchronized (b)
    {
      Object localObject1 = b;
      localObject1 = ((Set)localObject1).iterator();
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        Object localObject2 = ((Iterator)localObject1).next();
        localObject2 = (Request)localObject2;
        boolean bool2 = paramRequestFilter.apply((Request)localObject2);
        if (bool2) {
          ((Request)localObject2).cancel();
        }
      }
      return;
    }
  }
  
  public void cancelAll(Object paramObject)
  {
    if (paramObject != null)
    {
      RequestQueue.1 local1 = new com/mopub/volley/RequestQueue$1;
      local1.<init>(this, paramObject);
      cancelAll(local1);
      return;
    }
    paramObject = new java/lang/IllegalArgumentException;
    ((IllegalArgumentException)paramObject).<init>("Cannot cancelAll with a null tag");
    throw ((Throwable)paramObject);
  }
  
  public Cache getCache()
  {
    return e;
  }
  
  public int getSequenceNumber()
  {
    return a.incrementAndGet();
  }
  
  public void removeRequestFinishedListener(RequestQueue.RequestFinishedListener paramRequestFinishedListener)
  {
    synchronized (j)
    {
      List localList2 = j;
      localList2.remove(paramRequestFinishedListener);
      return;
    }
  }
  
  public void start()
  {
    stop();
    CacheDispatcher localCacheDispatcher = new com/mopub/volley/CacheDispatcher;
    Object localObject1 = c;
    Object localObject2 = d;
    Object localObject3 = e;
    Object localObject4 = g;
    localCacheDispatcher.<init>((BlockingQueue)localObject1, (BlockingQueue)localObject2, (Cache)localObject3, (ResponseDelivery)localObject4);
    i = localCacheDispatcher;
    i.start();
    int k = 0;
    localCacheDispatcher = null;
    for (;;)
    {
      localObject1 = h;
      int m = localObject1.length;
      if (k >= m) {
        break;
      }
      localObject1 = new com/mopub/volley/NetworkDispatcher;
      localObject2 = d;
      localObject3 = f;
      localObject4 = e;
      ResponseDelivery localResponseDelivery = g;
      ((NetworkDispatcher)localObject1).<init>((BlockingQueue)localObject2, (Network)localObject3, (Cache)localObject4, localResponseDelivery);
      localObject2 = h;
      localObject2[k] = localObject1;
      ((NetworkDispatcher)localObject1).start();
      k += 1;
    }
  }
  
  public void stop()
  {
    Object localObject1 = i;
    if (localObject1 != null) {
      ((CacheDispatcher)localObject1).quit();
    }
    localObject1 = h;
    int k = localObject1.length;
    int m = 0;
    while (m < k)
    {
      Object localObject2 = localObject1[m];
      if (localObject2 != null) {
        ((NetworkDispatcher)localObject2).quit();
      }
      m += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.RequestQueue
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */