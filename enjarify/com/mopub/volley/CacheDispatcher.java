package com.mopub.volley;

import android.os.Process;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

public class CacheDispatcher
  extends Thread
{
  private static final boolean a = VolleyLog.DEBUG;
  private final BlockingQueue b;
  private final BlockingQueue c;
  private final Cache d;
  private final ResponseDelivery e;
  private volatile boolean f = false;
  private final CacheDispatcher.a g;
  
  public CacheDispatcher(BlockingQueue paramBlockingQueue1, BlockingQueue paramBlockingQueue2, Cache paramCache, ResponseDelivery paramResponseDelivery)
  {
    b = paramBlockingQueue1;
    c = paramBlockingQueue2;
    d = paramCache;
    e = paramResponseDelivery;
    paramBlockingQueue1 = new com/mopub/volley/CacheDispatcher$a;
    paramBlockingQueue1.<init>(this);
    g = paramBlockingQueue1;
  }
  
  public void quit()
  {
    f = true;
    interrupt();
  }
  
  public void run()
  {
    boolean bool1 = a;
    boolean bool3;
    Object localObject2;
    if (bool1)
    {
      localObject1 = "start new dispatcher";
      bool3 = false;
      localObject2 = new Object[0];
      VolleyLog.v((String)localObject1, (Object[])localObject2);
    }
    int i = 10;
    Process.setThreadPriority(i);
    Object localObject1 = d;
    ((Cache)localObject1).initialize();
    try
    {
      boolean bool2;
      do
      {
        for (;;)
        {
          localObject1 = b;
          localObject1 = ((BlockingQueue)localObject1).take();
          localObject1 = (Request)localObject1;
          localObject2 = "cache-queue-take";
          ((Request)localObject1).addMarker((String)localObject2);
          bool3 = ((Request)localObject1).isCanceled();
          if (bool3)
          {
            localObject2 = "cache-discard-canceled";
            ((Request)localObject1).a((String)localObject2);
          }
          else
          {
            localObject2 = d;
            Object localObject3 = ((Request)localObject1).getCacheKey();
            localObject2 = ((Cache)localObject2).get((String)localObject3);
            if (localObject2 == null)
            {
              localObject2 = "cache-miss";
              ((Request)localObject1).addMarker((String)localObject2);
              localObject2 = g;
              bool3 = ((CacheDispatcher.a)localObject2).a((Request)localObject1);
              if (!bool3)
              {
                localObject2 = c;
                ((BlockingQueue)localObject2).put(localObject1);
              }
            }
            else
            {
              boolean bool4 = ((Cache.Entry)localObject2).isExpired();
              if (bool4)
              {
                localObject3 = "cache-hit-expired";
                ((Request)localObject1).addMarker((String)localObject3);
                ((Request)localObject1).setCacheEntry((Cache.Entry)localObject2);
                localObject2 = g;
                bool3 = ((CacheDispatcher.a)localObject2).a((Request)localObject1);
                if (!bool3)
                {
                  localObject2 = c;
                  ((BlockingQueue)localObject2).put(localObject1);
                }
              }
              else
              {
                localObject3 = "cache-hit";
                ((Request)localObject1).addMarker((String)localObject3);
                localObject3 = new com/mopub/volley/NetworkResponse;
                Object localObject4 = data;
                Map localMap = responseHeaders;
                ((NetworkResponse)localObject3).<init>((byte[])localObject4, localMap);
                localObject3 = ((Request)localObject1).parseNetworkResponse((NetworkResponse)localObject3);
                localObject4 = "cache-hit-parsed";
                ((Request)localObject1).addMarker((String)localObject4);
                boolean bool5 = ((Cache.Entry)localObject2).refreshNeeded();
                if (!bool5)
                {
                  localObject2 = e;
                  ((ResponseDelivery)localObject2).postResponse((Request)localObject1, (Response)localObject3);
                }
                else
                {
                  localObject4 = "cache-hit-refresh-needed";
                  ((Request)localObject1).addMarker((String)localObject4);
                  ((Request)localObject1).setCacheEntry((Cache.Entry)localObject2);
                  bool3 = true;
                  intermediate = bool3;
                  localObject2 = g;
                  bool3 = ((CacheDispatcher.a)localObject2).a((Request)localObject1);
                  if (!bool3)
                  {
                    localObject2 = e;
                    localObject4 = new com/mopub/volley/CacheDispatcher$1;
                    ((CacheDispatcher.1)localObject4).<init>(this, (Request)localObject1);
                    ((ResponseDelivery)localObject2).postResponse((Request)localObject1, (Response)localObject3, (Runnable)localObject4);
                  }
                  else
                  {
                    localObject2 = e;
                    ((ResponseDelivery)localObject2).postResponse((Request)localObject1, (Response)localObject3);
                  }
                }
              }
            }
          }
        }
      } while (!bool2);
    }
    catch (InterruptedException localInterruptedException)
    {
      bool2 = f;
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.CacheDispatcher
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */