package com.mopub.volley;

public abstract interface RequestQueue$RequestFilter
{
  public abstract boolean apply(Request paramRequest);
}

/* Location:
 * Qualified Name:     com.mopub.volley.RequestQueue.RequestFilter
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */