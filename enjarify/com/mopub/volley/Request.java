package com.mopub.volley;

import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public abstract class Request
  implements Comparable
{
  private final VolleyLog.a a;
  private final int b;
  private final String c;
  private final int d;
  private final Object e;
  private Response.ErrorListener f;
  private Integer g;
  private RequestQueue h;
  private boolean i;
  private boolean j;
  private boolean k;
  private boolean l;
  private RetryPolicy m;
  private Cache.Entry n;
  private Object o;
  private Request.a p;
  
  public Request(int paramInt, String paramString, Response.ErrorListener paramErrorListener)
  {
    boolean bool = VolleyLog.a.ENABLED;
    if (bool)
    {
      localObject1 = new com/mopub/volley/VolleyLog$a;
      ((VolleyLog.a)localObject1).<init>();
    }
    else
    {
      bool = false;
      localObject1 = null;
    }
    a = ((VolleyLog.a)localObject1);
    Object localObject1 = new java/lang/Object;
    localObject1.<init>();
    e = localObject1;
    i = true;
    bool = false;
    localObject1 = null;
    j = false;
    k = false;
    l = false;
    n = null;
    b = paramInt;
    c = paramString;
    f = paramErrorListener;
    Object localObject2 = new com/mopub/volley/DefaultRetryPolicy;
    ((DefaultRetryPolicy)localObject2).<init>();
    setRetryPolicy((RetryPolicy)localObject2);
    paramInt = TextUtils.isEmpty(paramString);
    int i1;
    if (paramInt == 0)
    {
      localObject2 = Uri.parse(paramString);
      if (localObject2 != null)
      {
        localObject2 = ((Uri)localObject2).getHost();
        if (localObject2 != null) {
          i1 = ((String)localObject2).hashCode();
        }
      }
    }
    d = i1;
  }
  
  public Request(String paramString, Response.ErrorListener paramErrorListener)
  {
    this(-1, paramString, paramErrorListener);
  }
  
  protected static VolleyError a(VolleyError paramVolleyError)
  {
    return paramVolleyError;
  }
  
  private static byte[] a(Map paramMap, String paramString)
  {
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    try
    {
      paramMap = paramMap.entrySet();
      paramMap = paramMap.iterator();
      for (;;)
      {
        boolean bool = paramMap.hasNext();
        if (!bool) {
          break;
        }
        Object localObject2 = paramMap.next();
        localObject2 = (Map.Entry)localObject2;
        Object localObject3 = ((Map.Entry)localObject2).getKey();
        localObject3 = (String)localObject3;
        localObject3 = URLEncoder.encode((String)localObject3, paramString);
        ((StringBuilder)localObject1).append((String)localObject3);
        char c2 = '=';
        ((StringBuilder)localObject1).append(c2);
        localObject2 = ((Map.Entry)localObject2).getValue();
        localObject2 = (String)localObject2;
        localObject2 = URLEncoder.encode((String)localObject2, paramString);
        ((StringBuilder)localObject1).append((String)localObject2);
        char c1 = '&';
        ((StringBuilder)localObject1).append(c1);
      }
      paramMap = ((StringBuilder)localObject1).toString();
      return paramMap.getBytes(paramString);
    }
    catch (UnsupportedEncodingException paramMap)
    {
      localObject1 = new java/lang/RuntimeException;
      paramString = String.valueOf(paramString);
      paramString = "Encoding not supported: ".concat(paramString);
      ((RuntimeException)localObject1).<init>(paramString, paramMap);
      throw ((Throwable)localObject1);
    }
  }
  
  final void a()
  {
    synchronized (e)
    {
      Request.a locala = p;
      if (locala != null) {
        locala.onNoUsableResponseReceived(this);
      }
      return;
    }
  }
  
  final void a(Request.a parama)
  {
    synchronized (e)
    {
      p = parama;
      return;
    }
  }
  
  final void a(Response paramResponse)
  {
    synchronized (e)
    {
      Request.a locala = p;
      if (locala != null) {
        locala.onResponseReceived(this, paramResponse);
      }
      return;
    }
  }
  
  final void a(String paramString)
  {
    Object localObject1 = h;
    if (localObject1 != null) {
      ((RequestQueue)localObject1).a(this);
    }
    boolean bool = VolleyLog.a.ENABLED;
    if (bool)
    {
      localObject1 = Thread.currentThread();
      long l1 = ((Thread)localObject1).getId();
      Object localObject2 = Looper.myLooper();
      Object localObject3 = Looper.getMainLooper();
      if (localObject2 != localObject3)
      {
        localObject2 = new android/os/Handler;
        localObject3 = Looper.getMainLooper();
        ((Handler)localObject2).<init>((Looper)localObject3);
        localObject3 = new com/mopub/volley/Request$1;
        ((Request.1)localObject3).<init>(this, paramString, l1);
        ((Handler)localObject2).post((Runnable)localObject3);
        return;
      }
      localObject2 = a;
      ((VolleyLog.a)localObject2).add(paramString, l1);
      paramString = a;
      localObject1 = toString();
      paramString.finish((String)localObject1);
    }
  }
  
  public void addMarker(String paramString)
  {
    boolean bool = VolleyLog.a.ENABLED;
    if (bool)
    {
      VolleyLog.a locala = a;
      Thread localThread = Thread.currentThread();
      long l1 = localThread.getId();
      locala.add(paramString, l1);
    }
  }
  
  public void cancel()
  {
    Object localObject1 = e;
    boolean bool = true;
    try
    {
      j = bool;
      bool = false;
      Object localObject2 = null;
      f = null;
      return;
    }
    finally {}
  }
  
  public int compareTo(Request paramRequest)
  {
    Request.Priority localPriority1 = getPriority();
    Request.Priority localPriority2 = paramRequest.getPriority();
    if (localPriority1 == localPriority2)
    {
      i1 = g.intValue();
      i2 = g.intValue();
      return i1 - i2;
    }
    int i2 = localPriority2.ordinal();
    int i1 = localPriority1.ordinal();
    return i2 - i1;
  }
  
  public void deliverError(VolleyError paramVolleyError)
  {
    synchronized (e)
    {
      Response.ErrorListener localErrorListener = f;
      if (localErrorListener != null) {
        localErrorListener.onErrorResponse(paramVolleyError);
      }
      return;
    }
  }
  
  protected abstract void deliverResponse(Object paramObject);
  
  public byte[] getBody()
  {
    Map localMap = getParams();
    if (localMap != null)
    {
      int i1 = localMap.size();
      if (i1 > 0) {
        return a(localMap, "UTF-8");
      }
    }
    return null;
  }
  
  public String getBodyContentType()
  {
    return "application/x-www-form-urlencoded; charset=UTF-8";
  }
  
  public Cache.Entry getCacheEntry()
  {
    return n;
  }
  
  public String getCacheKey()
  {
    return getUrl();
  }
  
  public Response.ErrorListener getErrorListener()
  {
    return f;
  }
  
  public Map getHeaders()
  {
    return Collections.emptyMap();
  }
  
  public int getMethod()
  {
    return b;
  }
  
  protected Map getParams()
  {
    return null;
  }
  
  public byte[] getPostBody()
  {
    Map localMap = getParams();
    if (localMap != null)
    {
      int i1 = localMap.size();
      if (i1 > 0) {
        return a(localMap, "UTF-8");
      }
    }
    return null;
  }
  
  public String getPostBodyContentType()
  {
    return getBodyContentType();
  }
  
  public Request.Priority getPriority()
  {
    return Request.Priority.NORMAL;
  }
  
  public RetryPolicy getRetryPolicy()
  {
    return m;
  }
  
  public final int getSequence()
  {
    Object localObject = g;
    if (localObject != null) {
      return ((Integer)localObject).intValue();
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("getSequence called before setSequence");
    throw ((Throwable)localObject);
  }
  
  public Object getTag()
  {
    return o;
  }
  
  public final int getTimeoutMs()
  {
    return m.getCurrentTimeout();
  }
  
  public int getTrafficStatsTag()
  {
    return d;
  }
  
  public String getUrl()
  {
    return c;
  }
  
  public boolean hasHadResponseDelivered()
  {
    synchronized (e)
    {
      boolean bool = k;
      return bool;
    }
  }
  
  public boolean isCanceled()
  {
    synchronized (e)
    {
      boolean bool = j;
      return bool;
    }
  }
  
  public void markDelivered()
  {
    Object localObject1 = e;
    boolean bool = true;
    try
    {
      k = bool;
      return;
    }
    finally {}
  }
  
  protected abstract Response parseNetworkResponse(NetworkResponse paramNetworkResponse);
  
  public Request setCacheEntry(Cache.Entry paramEntry)
  {
    n = paramEntry;
    return this;
  }
  
  public Request setRequestQueue(RequestQueue paramRequestQueue)
  {
    h = paramRequestQueue;
    return this;
  }
  
  public Request setRetryPolicy(RetryPolicy paramRetryPolicy)
  {
    m = paramRetryPolicy;
    return this;
  }
  
  public final Request setSequence(int paramInt)
  {
    Integer localInteger = Integer.valueOf(paramInt);
    g = localInteger;
    return this;
  }
  
  public final Request setShouldCache(boolean paramBoolean)
  {
    i = paramBoolean;
    return this;
  }
  
  public final Request setShouldRetryServerErrors(boolean paramBoolean)
  {
    l = paramBoolean;
    return this;
  }
  
  public Request setTag(Object paramObject)
  {
    o = paramObject;
    return this;
  }
  
  public final boolean shouldCache()
  {
    return i;
  }
  
  public final boolean shouldRetryServerErrors()
  {
    return l;
  }
  
  public String toString()
  {
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("0x");
    int i1 = getTrafficStatsTag();
    Object localObject2 = Integer.toHexString(i1);
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    boolean bool = j;
    if (bool) {
      str = "[X] ";
    } else {
      str = "[ ] ";
    }
    ((StringBuilder)localObject2).append(str);
    String str = getUrl();
    ((StringBuilder)localObject2).append(str);
    ((StringBuilder)localObject2).append(" ");
    ((StringBuilder)localObject2).append((String)localObject1);
    ((StringBuilder)localObject2).append(" ");
    localObject1 = getPriority();
    ((StringBuilder)localObject2).append(localObject1);
    ((StringBuilder)localObject2).append(" ");
    localObject1 = g;
    ((StringBuilder)localObject2).append(localObject1);
    return ((StringBuilder)localObject2).toString();
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.Request
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */