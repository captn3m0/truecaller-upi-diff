package com.mopub.volley;

public class Response
{
  public final Cache.Entry cacheEntry;
  public final VolleyError error;
  public boolean intermediate = false;
  public final Object result;
  
  private Response(VolleyError paramVolleyError)
  {
    result = null;
    cacheEntry = null;
    error = paramVolleyError;
  }
  
  private Response(Object paramObject, Cache.Entry paramEntry)
  {
    result = paramObject;
    cacheEntry = paramEntry;
    error = null;
  }
  
  public static Response error(VolleyError paramVolleyError)
  {
    Response localResponse = new com/mopub/volley/Response;
    localResponse.<init>(paramVolleyError);
    return localResponse;
  }
  
  public static Response success(Object paramObject, Cache.Entry paramEntry)
  {
    Response localResponse = new com/mopub/volley/Response;
    localResponse.<init>(paramObject, paramEntry);
    return localResponse;
  }
  
  public boolean isSuccess()
  {
    VolleyError localVolleyError = error;
    return localVolleyError == null;
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.Response
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */