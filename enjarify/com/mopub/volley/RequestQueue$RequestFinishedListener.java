package com.mopub.volley;

public abstract interface RequestQueue$RequestFinishedListener
{
  public abstract void onRequestFinished(Request paramRequest);
}

/* Location:
 * Qualified Name:     com.mopub.volley.RequestQueue.RequestFinishedListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */