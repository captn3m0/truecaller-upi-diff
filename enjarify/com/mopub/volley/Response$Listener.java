package com.mopub.volley;

public abstract interface Response$Listener
{
  public abstract void onResponse(Object paramObject);
}

/* Location:
 * Qualified Name:     com.mopub.volley.Response.Listener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */