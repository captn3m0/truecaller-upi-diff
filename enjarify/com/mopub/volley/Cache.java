package com.mopub.volley;

public abstract interface Cache
{
  public abstract void clear();
  
  public abstract Cache.Entry get(String paramString);
  
  public abstract void initialize();
  
  public abstract void invalidate(String paramString, boolean paramBoolean);
  
  public abstract void put(String paramString, Cache.Entry paramEntry);
  
  public abstract void remove(String paramString);
}

/* Location:
 * Qualified Name:     com.mopub.volley.Cache
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */