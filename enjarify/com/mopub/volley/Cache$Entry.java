package com.mopub.volley;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class Cache$Entry
{
  public List allResponseHeaders;
  public byte[] data;
  public String etag;
  public long lastModified;
  public Map responseHeaders;
  public long serverDate;
  public long softTtl;
  public long ttl;
  
  public Cache$Entry()
  {
    Map localMap = Collections.emptyMap();
    responseHeaders = localMap;
  }
  
  public boolean isExpired()
  {
    long l1 = ttl;
    long l2 = System.currentTimeMillis();
    boolean bool = l1 < l2;
    return bool;
  }
  
  public boolean refreshNeeded()
  {
    long l1 = softTtl;
    long l2 = System.currentTimeMillis();
    boolean bool = l1 < l2;
    return bool;
  }
}

/* Location:
 * Qualified Name:     com.mopub.volley.Cache.Entry
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */