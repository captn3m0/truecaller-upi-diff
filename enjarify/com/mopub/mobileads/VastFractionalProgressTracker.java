package com.mopub.mobileads;

import com.mopub.common.Preconditions;
import java.io.Serializable;
import java.util.Locale;

public class VastFractionalProgressTracker
  extends VastTracker
  implements Serializable, Comparable
{
  private static final long serialVersionUID;
  private final float a;
  
  public VastFractionalProgressTracker(VastTracker.a parama, String paramString, float paramFloat)
  {
    super(parama, paramString);
    parama = null;
    boolean bool = paramFloat < 0.0F;
    if (!bool)
    {
      bool = true;
    }
    else
    {
      bool = false;
      parama = null;
    }
    Preconditions.checkArgument(bool);
    a = paramFloat;
  }
  
  public VastFractionalProgressTracker(String paramString, float paramFloat)
  {
    this(locala, paramString, paramFloat);
  }
  
  public int compareTo(VastFractionalProgressTracker paramVastFractionalProgressTracker)
  {
    float f = paramVastFractionalProgressTracker.trackingFraction();
    double d1 = trackingFraction();
    double d2 = f;
    return Double.compare(d1, d2);
  }
  
  public String toString()
  {
    Locale localLocale = Locale.US;
    Object[] arrayOfObject = new Object[2];
    Object localObject = Float.valueOf(a);
    arrayOfObject[0] = localObject;
    localObject = getContent();
    arrayOfObject[1] = localObject;
    return String.format(localLocale, "%2f: %s", arrayOfObject);
  }
  
  public float trackingFraction()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastFractionalProgressTracker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */