package com.mopub.mobileads;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import com.mopub.common.Preconditions;
import com.mopub.common.UrlAction;
import com.mopub.common.UrlHandler;
import com.mopub.common.UrlHandler.Builder;
import com.mopub.common.UrlHandler.ResultActions;
import com.mopub.network.TrackingRequest;
import java.io.Serializable;
import java.util.List;

public class VastCompanionAdConfig
  implements Serializable
{
  private static final long serialVersionUID;
  private final int a;
  private final int b;
  private final n c;
  private final String d;
  private final List e;
  private final List f;
  
  public VastCompanionAdConfig(int paramInt1, int paramInt2, n paramn, String paramString, List paramList1, List paramList2)
  {
    Preconditions.checkNotNull(paramn);
    Preconditions.checkNotNull(paramList1, "clickTrackers cannot be null");
    Preconditions.checkNotNull(paramList2, "creativeViewTrackers cannot be null");
    a = paramInt1;
    b = paramInt2;
    c = paramn;
    d = paramString;
    e = paramList1;
    f = paramList2;
  }
  
  final void a(Context paramContext, int paramInt)
  {
    Preconditions.checkNotNull(paramContext);
    List localList = f;
    Integer localInteger = Integer.valueOf(paramInt);
    TrackingRequest.makeVastTrackingHttpRequest(localList, null, localInteger, null, paramContext);
  }
  
  final void a(Context paramContext, String paramString1, String paramString2)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkArgument(paramContext instanceof Activity, "context must be an activity");
    Object localObject1 = c;
    Object localObject2 = d;
    paramString1 = ((n)localObject1).getCorrectClickThroughUrl((String)localObject2, paramString1);
    boolean bool = TextUtils.isEmpty(paramString1);
    if (bool) {
      return;
    }
    localObject1 = new com/mopub/common/UrlHandler$Builder;
    ((UrlHandler.Builder)localObject1).<init>();
    localObject2 = UrlAction.IGNORE_ABOUT_SCHEME;
    UrlAction[] arrayOfUrlAction = new UrlAction[6];
    UrlAction localUrlAction = UrlAction.OPEN_APP_MARKET;
    arrayOfUrlAction[0] = localUrlAction;
    localUrlAction = UrlAction.OPEN_NATIVE_BROWSER;
    arrayOfUrlAction[1] = localUrlAction;
    localUrlAction = UrlAction.OPEN_IN_APP_BROWSER;
    arrayOfUrlAction[2] = localUrlAction;
    localUrlAction = UrlAction.HANDLE_SHARE_TWEET;
    arrayOfUrlAction[3] = localUrlAction;
    localUrlAction = UrlAction.FOLLOW_DEEP_LINK_WITH_FALLBACK;
    arrayOfUrlAction[4] = localUrlAction;
    localUrlAction = UrlAction.FOLLOW_DEEP_LINK;
    arrayOfUrlAction[5] = localUrlAction;
    localObject1 = ((UrlHandler.Builder)localObject1).withSupportedUrlActions((UrlAction)localObject2, arrayOfUrlAction);
    localObject2 = new com/mopub/mobileads/VastCompanionAdConfig$1;
    ((VastCompanionAdConfig.1)localObject2).<init>(this, paramString2, paramContext);
    ((UrlHandler.Builder)localObject1).withResultActions((UrlHandler.ResultActions)localObject2).withDspCreativeId(paramString2).withoutMoPubBrowser().build().handleUrl(paramContext, paramString1);
  }
  
  public void addClickTrackers(List paramList)
  {
    Preconditions.checkNotNull(paramList, "clickTrackers cannot be null");
    e.addAll(paramList);
  }
  
  public void addCreativeViewTrackers(List paramList)
  {
    Preconditions.checkNotNull(paramList, "creativeViewTrackers cannot be null");
    f.addAll(paramList);
  }
  
  public String getClickThroughUrl()
  {
    return d;
  }
  
  public List getClickTrackers()
  {
    return e;
  }
  
  public List getCreativeViewTrackers()
  {
    return f;
  }
  
  public int getHeight()
  {
    return b;
  }
  
  public n getVastResource()
  {
    return c;
  }
  
  public int getWidth()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastCompanionAdConfig
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */