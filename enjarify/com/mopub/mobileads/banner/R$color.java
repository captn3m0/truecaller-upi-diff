package com.mopub.mobileads.banner;

public final class R$color
{
  public static final int notification_action_color_filter = 2131100296;
  public static final int notification_icon_bg_color = 2131100298;
  public static final int notification_material_background_media_default_color = 2131100299;
  public static final int primary_text_default_material_dark = 2131100417;
  public static final int ripple_material_light = 2131100477;
  public static final int secondary_text_default_material_dark = 2131100487;
  public static final int secondary_text_default_material_light = 2131100488;
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.banner.R.color
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */