package com.mopub.mobileads;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.DeviceUtils.ForceOrientation;
import com.mopub.common.util.Dips;
import com.mopub.mobileads.resource.DrawableConstants.GradientStrip;

public class VastVideoGradientStripWidget
  extends ImageView
{
  DeviceUtils.ForceOrientation a;
  private int b;
  private boolean c;
  private boolean d;
  
  public VastVideoGradientStripWidget(Context paramContext, GradientDrawable.Orientation paramOrientation, DeviceUtils.ForceOrientation paramForceOrientation, boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3)
  {
    super(paramContext);
    a = paramForceOrientation;
    b = paramInt1;
    c = paramBoolean;
    paramForceOrientation = new android/graphics/drawable/GradientDrawable;
    int[] arrayOfInt = new int[2];
    paramInt1 = DrawableConstants.GradientStrip.START_COLOR;
    arrayOfInt[0] = paramInt1;
    paramInt1 = DrawableConstants.GradientStrip.END_COLOR;
    arrayOfInt[1] = paramInt1;
    paramForceOrientation.<init>(paramOrientation, arrayOfInt);
    setImageDrawable(paramForceOrientation);
    paramOrientation = new android/widget/RelativeLayout$LayoutParams;
    int i = Dips.dipsToIntPixels(72.0F, paramContext);
    paramOrientation.<init>(-1, i);
    paramOrientation.addRule(paramInt2, paramInt3);
    setLayoutParams(paramOrientation);
    b();
  }
  
  private void b()
  {
    boolean bool = d;
    if (bool)
    {
      bool = c;
      if (bool)
      {
        i = b;
        setVisibility(i);
        return;
      }
      setVisibility(8);
      return;
    }
    Object localObject = a;
    DeviceUtils.ForceOrientation localForceOrientation = DeviceUtils.ForceOrientation.FORCE_PORTRAIT;
    int j = 4;
    if (localObject == localForceOrientation)
    {
      setVisibility(j);
      return;
    }
    localObject = a;
    localForceOrientation = DeviceUtils.ForceOrientation.FORCE_LANDSCAPE;
    if (localObject == localForceOrientation)
    {
      setVisibility(0);
      return;
    }
    localObject = getResources().getConfiguration();
    int i = orientation;
    switch (i)
    {
    default: 
      MoPubLog.d("Unrecognized screen orientation: do not show gradient strip widget");
      setVisibility(j);
      return;
    case 3: 
      MoPubLog.d("Screen orientation is deprecated ORIENTATION_SQUARE: do not show gradient strip widget");
      setVisibility(j);
      return;
    case 2: 
      setVisibility(0);
      return;
    case 1: 
      setVisibility(j);
      return;
    }
    MoPubLog.d("Screen orientation undefined: do not show gradient strip widget");
    setVisibility(j);
  }
  
  final void a()
  {
    d = true;
    b();
  }
  
  protected void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    b();
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastVideoGradientStripWidget
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */