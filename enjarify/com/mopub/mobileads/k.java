package com.mopub.mobileads;

import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Strings;
import com.mopub.mobileads.util.XmlUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.w3c.dom.Node;

final class k
{
  public static final String ICON = "Icon";
  public static final String ICONS = "Icons";
  final Node a;
  
  k(Node paramNode)
  {
    Preconditions.checkNotNull(paramNode);
    a = paramNode;
  }
  
  private static void a(List paramList1, List paramList2, float paramFloat)
  {
    Preconditions.checkNotNull(paramList1, "trackers cannot be null");
    String str = "urls cannot be null";
    Preconditions.checkNotNull(paramList2, str);
    paramList2 = paramList2.iterator();
    for (;;)
    {
      boolean bool = paramList2.hasNext();
      if (!bool) {
        break;
      }
      str = (String)paramList2.next();
      VastFractionalProgressTracker localVastFractionalProgressTracker = new com/mopub/mobileads/VastFractionalProgressTracker;
      localVastFractionalProgressTracker.<init>(str, paramFloat);
      paramList1.add(localVastFractionalProgressTracker);
    }
  }
  
  private List b(String paramString)
  {
    Preconditions.checkNotNull(paramString);
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject = a;
    String str1 = "TrackingEvents";
    localObject = XmlUtils.getFirstMatchingChildNode((Node)localObject, str1);
    if (localObject == null) {
      return localArrayList;
    }
    str1 = "Tracking";
    String str2 = "event";
    paramString = Collections.singletonList(paramString);
    paramString = XmlUtils.getMatchingChildNodes((Node)localObject, str1, str2, paramString).iterator();
    for (;;)
    {
      boolean bool = paramString.hasNext();
      if (!bool) {
        break;
      }
      localObject = XmlUtils.getNodeValue((Node)paramString.next());
      if (localObject != null) {
        localArrayList.add(localObject);
      }
    }
    return localArrayList;
  }
  
  final List a()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject1 = b("firstQuartile");
    a(localArrayList, (List)localObject1, 0.25F);
    localObject1 = b("midpoint");
    a(localArrayList, (List)localObject1, 0.5F);
    localObject1 = b("thirdQuartile");
    int i = 1061158912;
    float f1 = 0.75F;
    a(localArrayList, (List)localObject1, f1);
    localObject1 = a;
    Object localObject2 = "TrackingEvents";
    localObject1 = XmlUtils.getFirstMatchingChildNode((Node)localObject1, (String)localObject2);
    if (localObject1 != null)
    {
      localObject2 = "Tracking";
      String str = "event";
      Object localObject3 = Collections.singletonList("progress");
      localObject1 = XmlUtils.getMatchingChildNodes((Node)localObject1, (String)localObject2, str, (List)localObject3).iterator();
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        localObject2 = (Node)((Iterator)localObject1).next();
        str = XmlUtils.getAttributeValue((Node)localObject2, "offset");
        if (str != null)
        {
          str = str.trim();
          boolean bool2 = Strings.isPercentageTracker(str);
          if (bool2)
          {
            localObject2 = XmlUtils.getNodeValue((Node)localObject2);
            localObject3 = "%";
            Object localObject4 = "";
            try
            {
              localObject3 = str.replace((CharSequence)localObject3, (CharSequence)localObject4);
              f2 = Float.parseFloat((String)localObject3) / 100.0F;
              localObject4 = null;
              bool3 = f2 < 0.0F;
              if (!bool3)
              {
                localObject4 = new com/mopub/mobileads/VastFractionalProgressTracker;
                ((VastFractionalProgressTracker)localObject4).<init>((String)localObject2, f2);
                localArrayList.add(localObject4);
              }
            }
            catch (NumberFormatException localNumberFormatException)
            {
              bool2 = true;
              float f2 = Float.MIN_VALUE;
              localObject3 = new Object[bool2];
              boolean bool3 = false;
              localObject4 = null;
              localObject3[0] = str;
              localObject2 = String.format("Failed to parse VAST progress tracker %s", (Object[])localObject3);
              MoPubLog.d((String)localObject2);
            }
          }
        }
      }
    }
    Collections.sort(localArrayList);
    return localArrayList;
  }
  
  final List a(String paramString)
  {
    paramString = b(paramString);
    ArrayList localArrayList = new java/util/ArrayList;
    int i = paramString.size();
    localArrayList.<init>(i);
    paramString = paramString.iterator();
    for (;;)
    {
      boolean bool = paramString.hasNext();
      if (!bool) {
        break;
      }
      String str = (String)paramString.next();
      VastTracker localVastTracker = new com/mopub/mobileads/VastTracker;
      localVastTracker.<init>(str);
      localArrayList.add(localVastTracker);
    }
    return localArrayList;
  }
  
  final List b()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject1 = b("start").iterator();
    boolean bool1;
    Object localObject3;
    for (;;)
    {
      bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (String)((Iterator)localObject1).next();
      localObject3 = new com/mopub/mobileads/VastAbsoluteProgressTracker;
      ((VastAbsoluteProgressTracker)localObject3).<init>((String)localObject2, 0);
      localArrayList.add(localObject3);
    }
    localObject1 = a;
    Object localObject2 = "TrackingEvents";
    localObject1 = XmlUtils.getFirstMatchingChildNode((Node)localObject1, (String)localObject2);
    if (localObject1 != null)
    {
      localObject3 = "event";
      Object localObject4 = Collections.singletonList("progress");
      localObject2 = XmlUtils.getMatchingChildNodes((Node)localObject1, "Tracking", (String)localObject3, (List)localObject4).iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject2).hasNext();
        if (!bool2) {
          break;
        }
        localObject3 = (Node)((Iterator)localObject2).next();
        localObject4 = XmlUtils.getAttributeValue((Node)localObject3, "offset");
        if (localObject4 != null)
        {
          localObject4 = ((String)localObject4).trim();
          boolean bool3 = Strings.isAbsoluteTracker((String)localObject4);
          if (bool3)
          {
            localObject3 = XmlUtils.getNodeValue((Node)localObject3);
            try
            {
              localObject5 = Strings.parseAbsoluteOffset((String)localObject4);
              if (localObject5 != null)
              {
                int j = ((Integer)localObject5).intValue();
                if (j >= 0)
                {
                  VastAbsoluteProgressTracker localVastAbsoluteProgressTracker = new com/mopub/mobileads/VastAbsoluteProgressTracker;
                  i = ((Integer)localObject5).intValue();
                  localVastAbsoluteProgressTracker.<init>((String)localObject3, i);
                  localArrayList.add(localVastAbsoluteProgressTracker);
                }
              }
            }
            catch (NumberFormatException localNumberFormatException)
            {
              int i = 1;
              Object localObject5 = new Object[i];
              localObject5[0] = localObject4;
              localObject3 = String.format("Failed to parse VAST progress tracker %s", (Object[])localObject5);
              MoPubLog.d((String)localObject3);
            }
          }
        }
      }
      localObject2 = "Tracking";
      localObject3 = "event";
      localObject4 = Collections.singletonList("creativeView");
      localObject1 = XmlUtils.getMatchingChildNodes((Node)localObject1, (String)localObject2, (String)localObject3, (List)localObject4).iterator();
      for (;;)
      {
        bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        localObject2 = XmlUtils.getNodeValue((Node)((Iterator)localObject1).next());
        if (localObject2 != null)
        {
          localObject3 = new com/mopub/mobileads/VastAbsoluteProgressTracker;
          ((VastAbsoluteProgressTracker)localObject3).<init>((String)localObject2, 0);
          localArrayList.add(localObject3);
        }
      }
    }
    Collections.sort(localArrayList);
    return localArrayList;
  }
  
  final List c()
  {
    Object localObject = b("pause");
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    localObject = ((List)localObject).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject).hasNext();
      if (!bool1) {
        break;
      }
      String str = (String)((Iterator)localObject).next();
      VastTracker localVastTracker = new com/mopub/mobileads/VastTracker;
      boolean bool2 = true;
      localVastTracker.<init>(str, bool2);
      localArrayList.add(localVastTracker);
    }
    return localArrayList;
  }
  
  final List d()
  {
    Object localObject = b("resume");
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    localObject = ((List)localObject).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject).hasNext();
      if (!bool1) {
        break;
      }
      String str = (String)((Iterator)localObject).next();
      VastTracker localVastTracker = new com/mopub/mobileads/VastTracker;
      boolean bool2 = true;
      localVastTracker.<init>(str, bool2);
      localArrayList.add(localVastTracker);
    }
    return localArrayList;
  }
  
  final List e()
  {
    List localList1 = a("close");
    List localList2 = a("closeLinear");
    localList1.addAll(localList2);
    return localList1;
  }
  
  final List f()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject = a;
    String str = "VideoClicks";
    localObject = XmlUtils.getFirstMatchingChildNode((Node)localObject, str);
    if (localObject == null) {
      return localArrayList;
    }
    str = "ClickTracking";
    localObject = XmlUtils.getMatchingChildNodes((Node)localObject, str).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject).hasNext();
      if (!bool) {
        break;
      }
      str = XmlUtils.getNodeValue((Node)((Iterator)localObject).next());
      if (str != null)
      {
        VastTracker localVastTracker = new com/mopub/mobileads/VastTracker;
        localVastTracker.<init>(str);
        localArrayList.add(localVastTracker);
      }
    }
    return localArrayList;
  }
  
  final String g()
  {
    String str1 = XmlUtils.getAttributeValue(a, "skipoffset");
    if (str1 == null) {
      return null;
    }
    String str2 = str1.trim();
    boolean bool = str2.isEmpty();
    if (bool) {
      return null;
    }
    return str1.trim();
  }
  
  final List h()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject1 = a;
    Object localObject2 = "Icons";
    localObject1 = XmlUtils.getFirstMatchingChildNode((Node)localObject1, (String)localObject2);
    if (localObject1 == null) {
      return localArrayList;
    }
    localObject2 = "Icon";
    localObject1 = XmlUtils.getMatchingChildNodes((Node)localObject1, (String)localObject2).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (Node)((Iterator)localObject1).next();
      VastIconXmlManager localVastIconXmlManager = new com/mopub/mobileads/VastIconXmlManager;
      localVastIconXmlManager.<init>((Node)localObject2);
      localArrayList.add(localVastIconXmlManager);
    }
    return localArrayList;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */