package com.mopub.mobileads;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.mopub.common.MoPubBrowser;
import com.mopub.common.UrlAction;
import com.mopub.common.UrlHandler.ResultActions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Intents;

final class VastCompanionAdConfig$1
  implements UrlHandler.ResultActions
{
  VastCompanionAdConfig$1(VastCompanionAdConfig paramVastCompanionAdConfig, String paramString, Context paramContext) {}
  
  public final void urlHandlingFailed(String paramString, UrlAction paramUrlAction) {}
  
  public final void urlHandlingSucceeded(String paramString, UrlAction paramUrlAction)
  {
    Object localObject = UrlAction.OPEN_IN_APP_BROWSER;
    if (paramUrlAction == localObject)
    {
      paramUrlAction = new android/os/Bundle;
      paramUrlAction.<init>();
      localObject = "URL";
      paramUrlAction.putString((String)localObject, paramString);
      paramString = a;
      boolean bool = TextUtils.isEmpty(paramString);
      if (!bool)
      {
        paramString = "mopub-dsp-creative-id";
        localObject = a;
        paramUrlAction.putString(paramString, (String)localObject);
      }
      paramString = MoPubBrowser.class;
      localObject = b;
      paramUrlAction = Intents.getStartActivityIntent((Context)localObject, paramString, paramUrlAction);
      try
      {
        localObject = b;
        localObject = (Activity)localObject;
        int i = c;
        ((Activity)localObject).startActivityForResult(paramUrlAction, i);
        return;
      }
      catch (ActivityNotFoundException localActivityNotFoundException)
      {
        paramUrlAction = new java/lang/StringBuilder;
        localObject = "Activity ";
        paramUrlAction.<init>((String)localObject);
        paramString = paramString.getName();
        paramUrlAction.append(paramString);
        paramUrlAction.append(" not found. Did you declare it in your AndroidManifest.xml?");
        paramString = paramUrlAction.toString();
        MoPubLog.d(paramString);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastCompanionAdConfig.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */