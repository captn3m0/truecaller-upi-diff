package com.mopub.mobileads;

import android.content.Context;
import java.util.Map;

public abstract class CustomEventBanner
{
  boolean a = true;
  
  protected abstract void loadBanner(Context paramContext, CustomEventBanner.CustomEventBannerListener paramCustomEventBannerListener, Map paramMap1, Map paramMap2);
  
  protected abstract void onInvalidate();
  
  protected void trackMpxAndThirdPartyImpressions() {}
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.CustomEventBanner
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */