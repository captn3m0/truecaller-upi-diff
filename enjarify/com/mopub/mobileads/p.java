package com.mopub.mobileads;

import com.mopub.common.Preconditions;
import com.mopub.mobileads.util.XmlUtils;
import org.w3c.dom.Node;

final class p
  extends g
{
  p(Node paramNode)
  {
    super(paramNode);
    Preconditions.checkNotNull(paramNode);
  }
  
  final String f()
  {
    return XmlUtils.getNodeValue(XmlUtils.getFirstMatchingChildNode(a, "VASTAdTagURI"));
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */