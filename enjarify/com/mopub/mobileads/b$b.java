package com.mopub.mobileads;

import android.content.Context;
import android.graphics.Rect;
import android.os.SystemClock;
import android.view.View;
import com.mopub.common.util.Dips;

final class b$b
  implements Runnable
{
  b$b(b paramb) {}
  
  public final void run()
  {
    Object localObject1 = a;
    boolean bool1 = i;
    if (bool1) {
      return;
    }
    localObject1 = a;
    int i = 0;
    h = false;
    localObject1 = e;
    Object localObject2 = a.d;
    Object localObject3 = a.c;
    boolean bool2 = true;
    long l1;
    long l2;
    if (localObject3 != null)
    {
      int j = ((View)localObject3).getVisibility();
      if (j == 0)
      {
        localObject2 = ((View)localObject2).getParent();
        if (localObject2 != null)
        {
          int k = ((View)localObject3).getWidth();
          if (k > 0)
          {
            k = ((View)localObject3).getHeight();
            if (k > 0)
            {
              localObject2 = d;
              boolean bool3 = ((View)localObject3).getGlobalVisibleRect((Rect)localObject2);
              if (bool3)
              {
                localObject2 = d;
                float f1 = ((Rect)localObject2).width();
                Object localObject4 = ((View)localObject3).getContext();
                int m = Dips.pixelsToIntDips(f1, (Context)localObject4);
                localObject4 = d;
                j = ((Rect)localObject4).height();
                float f2 = j;
                localObject3 = ((View)localObject3).getContext();
                int n = Dips.pixelsToIntDips(f2, (Context)localObject3);
                m *= n;
                l1 = m;
                l2 = a;
                bool1 = l1 < l2;
                if (!bool1)
                {
                  bool1 = true;
                  break label248;
                }
              }
              bool1 = false;
              localObject1 = null;
              break label248;
            }
          }
          bool1 = false;
          localObject1 = null;
          break label248;
        }
      }
    }
    bool1 = false;
    localObject1 = null;
    label248:
    if (bool1)
    {
      localObject1 = a.e;
      bool1 = ((b.a)localObject1).a();
      if (!bool1)
      {
        localObject1 = a.e;
        l1 = SystemClock.uptimeMillis();
        c = l1;
      }
      localObject1 = a.e;
      boolean bool4 = ((b.a)localObject1).a();
      if (bool4)
      {
        l1 = SystemClock.uptimeMillis();
        l2 = c;
        l1 -= l2;
        l2 = b;
        bool1 = l1 < l2;
        if (!bool1) {
          i = 1;
        }
      }
      if (i != 0)
      {
        localObject1 = a.f;
        if (localObject1 != null)
        {
          a.f.onVisibilityChanged();
          localObject1 = a;
          i = bool2;
        }
      }
    }
    localObject1 = a;
    bool1 = i;
    if (!bool1)
    {
      localObject1 = a;
      ((b)localObject1).a();
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */