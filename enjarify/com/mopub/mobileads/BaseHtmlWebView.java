package com.mopub.mobileads;

import android.content.Context;
import android.webkit.WebSettings;
import com.mopub.common.AdReport;
import com.mopub.common.logging.MoPubLog;

public class BaseHtmlWebView
  extends BaseWebView
  implements ViewGestureDetector.UserClickListener
{
  private final ViewGestureDetector b;
  private boolean c;
  
  public BaseHtmlWebView(Context paramContext, AdReport paramAdReport)
  {
    super(paramContext);
    setHorizontalScrollBarEnabled(false);
    setHorizontalScrollbarOverlay(false);
    setVerticalScrollBarEnabled(false);
    setVerticalScrollbarOverlay(false);
    getSettings().setSupportZoom(false);
    Object localObject = getSettings();
    boolean bool = true;
    ((WebSettings)localObject).setJavaScriptEnabled(bool);
    localObject = new com/mopub/mobileads/ViewGestureDetector;
    ((ViewGestureDetector)localObject).<init>(paramContext, this, paramAdReport);
    b = ((ViewGestureDetector)localObject);
    b.setUserClickListener(this);
    enablePlugins(bool);
    setBackgroundColor(0);
  }
  
  public void init()
  {
    BaseHtmlWebView.1 local1 = new com/mopub/mobileads/BaseHtmlWebView$1;
    local1.<init>(this);
    setOnTouchListener(local1);
  }
  
  public void loadUrl(String paramString)
  {
    if (paramString == null) {
      return;
    }
    String str = "javascript:";
    boolean bool = paramString.startsWith(str);
    if (bool)
    {
      super.loadUrl(paramString);
      return;
    }
    paramString = String.valueOf(paramString);
    MoPubLog.d("Loading url: ".concat(paramString));
  }
  
  public void onResetUserClick()
  {
    c = false;
  }
  
  public void onUserClick()
  {
    c = true;
  }
  
  public void stopLoading()
  {
    boolean bool = a;
    String str;
    if (bool)
    {
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      str = BaseHtmlWebView.class.getSimpleName();
      ((StringBuilder)localObject).append(str);
      ((StringBuilder)localObject).append("#stopLoading() called after destroy()");
      MoPubLog.w(((StringBuilder)localObject).toString());
      return;
    }
    Object localObject = getSettings();
    if (localObject == null)
    {
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      str = BaseHtmlWebView.class.getSimpleName();
      ((StringBuilder)localObject).append(str);
      ((StringBuilder)localObject).append("#getSettings() returned null");
      MoPubLog.w(((StringBuilder)localObject).toString());
      return;
    }
    ((WebSettings)localObject).setJavaScriptEnabled(false);
    super.stopLoading();
    ((WebSettings)localObject).setJavaScriptEnabled(true);
  }
  
  public boolean wasClicked()
  {
    return c;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.BaseHtmlWebView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */