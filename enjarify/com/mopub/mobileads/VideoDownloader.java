package com.mopub.mobileads;

import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.AsyncTasks;
import java.lang.ref.WeakReference;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

public class VideoDownloader
{
  private static final Deque a;
  
  static
  {
    ArrayDeque localArrayDeque = new java/util/ArrayDeque;
    localArrayDeque.<init>();
    a = localArrayDeque;
  }
  
  private static boolean a(WeakReference paramWeakReference)
  {
    if (paramWeakReference == null) {
      return false;
    }
    paramWeakReference = (VideoDownloader.b)paramWeakReference.get();
    if (paramWeakReference == null) {
      return false;
    }
    return paramWeakReference.cancel(true);
  }
  
  public static void cache(String paramString, VideoDownloader.a parama)
  {
    Preconditions.checkNotNull(parama);
    if (paramString == null)
    {
      MoPubLog.d("VideoDownloader attempted to cache video with null url.");
      parama.onComplete(false);
      return;
    }
    VideoDownloader.b localb = new com/mopub/mobileads/VideoDownloader$b;
    localb.<init>(parama);
    int i = 1;
    try
    {
      String[] arrayOfString = new String[i];
      arrayOfString[0] = paramString;
      AsyncTasks.safeExecuteOnExecutor(localb, arrayOfString);
      return;
    }
    catch (Exception localException)
    {
      parama.onComplete(false);
    }
  }
  
  public static void cancelAllDownloaderTasks()
  {
    Iterator localIterator = a.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      WeakReference localWeakReference = (WeakReference)localIterator.next();
      a(localWeakReference);
    }
    a.clear();
  }
  
  public static void cancelLastDownloadTask()
  {
    Deque localDeque = a;
    boolean bool = localDeque.isEmpty();
    if (bool) {
      return;
    }
    a((WeakReference)a.peekLast());
    a.removeLast();
  }
  
  public static void clearDownloaderTasks()
  {
    a.clear();
  }
  
  public static Deque getDownloaderTasks()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VideoDownloader
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */