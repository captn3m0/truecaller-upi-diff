package com.mopub.mobileads;

public abstract interface HtmlWebViewListener
{
  public abstract void onClicked();
  
  public abstract void onCollapsed();
  
  public abstract void onFailed(MoPubErrorCode paramMoPubErrorCode);
  
  public abstract void onLoaded(BaseHtmlWebView paramBaseHtmlWebView);
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.HtmlWebViewListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */