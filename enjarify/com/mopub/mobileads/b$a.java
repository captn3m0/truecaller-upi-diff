package com.mopub.mobileads;

import android.graphics.Rect;

final class b$a
{
  int a;
  int b;
  long c = Long.MIN_VALUE;
  final Rect d;
  
  b$a(int paramInt1, int paramInt2)
  {
    Rect localRect = new android/graphics/Rect;
    localRect.<init>();
    d = localRect;
    a = paramInt1;
    b = paramInt2;
  }
  
  final boolean a()
  {
    long l1 = c;
    long l2 = Long.MIN_VALUE;
    boolean bool = l1 < l2;
    return bool;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */