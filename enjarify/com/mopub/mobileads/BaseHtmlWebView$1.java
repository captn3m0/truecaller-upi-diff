package com.mopub.mobileads;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

final class BaseHtmlWebView$1
  implements View.OnTouchListener
{
  BaseHtmlWebView$1(BaseHtmlWebView paramBaseHtmlWebView) {}
  
  public final boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    paramView = BaseHtmlWebView.a(a);
    paramView.sendTouchEvent(paramMotionEvent);
    int i = paramMotionEvent.getAction();
    int j = 2;
    return i == j;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.BaseHtmlWebView.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */