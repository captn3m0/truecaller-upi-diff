package com.mopub.mobileads.dfp.adapters;

public final class R$attr
{
  public static final int adSize = 2130968613;
  public static final int adSizes = 2130968614;
  public static final int adUnitId = 2130968615;
  public static final int alpha = 2130968634;
  public static final int coordinatorLayoutStyle = 2130968860;
  public static final int fastScrollEnabled = 2130968968;
  public static final int fastScrollHorizontalThumbDrawable = 2130968969;
  public static final int fastScrollHorizontalTrackDrawable = 2130968970;
  public static final int fastScrollVerticalThumbDrawable = 2130968971;
  public static final int fastScrollVerticalTrackDrawable = 2130968972;
  public static final int font = 2130968998;
  public static final int fontProviderAuthority = 2130969000;
  public static final int fontProviderCerts = 2130969001;
  public static final int fontProviderFetchStrategy = 2130969002;
  public static final int fontProviderFetchTimeout = 2130969003;
  public static final int fontProviderPackage = 2130969004;
  public static final int fontProviderQuery = 2130969005;
  public static final int fontStyle = 2130969006;
  public static final int fontVariationSettings = 2130969007;
  public static final int fontWeight = 2130969008;
  public static final int keylines = 2130969079;
  public static final int layoutManager = 2130969101;
  public static final int layout_anchor = 2130969102;
  public static final int layout_anchorGravity = 2130969103;
  public static final int layout_behavior = 2130969104;
  public static final int layout_dodgeInsetEdges = 2130969149;
  public static final int layout_insetEdge = 2130969158;
  public static final int layout_keyline = 2130969159;
  public static final int reverseLayout = 2130969371;
  public static final int spanCount = 2130969412;
  public static final int stackFromEnd = 2130969426;
  public static final int statusBarBackground = 2130969433;
  public static final int ttcIndex = 2130969645;
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.dfp.adapters.R.attr
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */