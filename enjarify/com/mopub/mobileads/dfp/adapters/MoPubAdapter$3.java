package com.mopub.mobileads.dfp.adapters;

import com.mopub.common.SdkInitializationListener;
import com.mopub.common.logging.MoPubLog;
import com.mopub.mobileads.MoPubInterstitial;
import com.mopub.mobileads.MoPubView;
import com.mopub.nativeads.MoPubNative;
import com.mopub.nativeads.RequestParameters;

final class MoPubAdapter$3
  implements SdkInitializationListener
{
  MoPubAdapter$3(MoPubAdapter paramMoPubAdapter, MoPubView paramMoPubView, MoPubInterstitial paramMoPubInterstitial, MoPubNative paramMoPubNative) {}
  
  public final void onInitializationFinished()
  {
    MoPubLog.d("MoPub SDK initialized.");
    Object localObject = a;
    if (localObject != null)
    {
      ((MoPubView)localObject).loadAd();
      return;
    }
    localObject = b;
    if (localObject != null)
    {
      ((MoPubInterstitial)localObject).load();
      return;
    }
    localObject = c;
    if (localObject != null)
    {
      localObject = MoPubAdapter.e(d);
      if (localObject != null)
      {
        localObject = c;
        RequestParameters localRequestParameters = MoPubAdapter.e(d);
        ((MoPubNative)localObject).makeRequest(localRequestParameters);
        return;
      }
      localObject = c;
      ((MoPubNative)localObject).makeRequest();
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.dfp.adapters.MoPubAdapter.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */