package com.mopub.mobileads.dfp.adapters;

public final class BuildConfig
{
  public static final String APPLICATION_ID = "com.mopub.mobileads.dfp.adapters";
  public static final String BUILD_TYPE = "release";
  public static final boolean DEBUG = false;
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 5040100;
  public static final String VERSION_NAME = "5.4.1.0";
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.dfp.adapters.BuildConfig
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */