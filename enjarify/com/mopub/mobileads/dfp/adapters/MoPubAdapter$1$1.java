package com.mopub.mobileads.dfp.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.MediationNativeListener;
import com.google.android.gms.ads.mediation.NativeAdMapper;
import com.mopub.nativeads.StaticNativeAd;
import java.util.HashMap;

final class MoPubAdapter$1$1
  implements DrawableDownloadListener
{
  MoPubAdapter$1$1(MoPubAdapter.1 param1, StaticNativeAd paramStaticNativeAd) {}
  
  public final void onDownloadFailure()
  {
    MediationNativeListener localMediationNativeListener = b.a;
    MoPubAdapter localMoPubAdapter = b.c;
    localMediationNativeListener.onAdFailedToLoad(localMoPubAdapter, 0);
  }
  
  public final void onDownloadSuccess(HashMap paramHashMap)
  {
    try
    {
      localObject1 = new com/mopub/mobileads/dfp/adapters/MoPubNativeAppInstallAdMapper;
      Object localObject2 = a;
      Object localObject3 = b;
      localObject3 = c;
      int i = MoPubAdapter.b((MoPubAdapter)localObject3);
      Object localObject4 = b;
      localObject4 = c;
      int j = MoPubAdapter.c((MoPubAdapter)localObject4);
      ((MoPubNativeAppInstallAdMapper)localObject1).<init>((StaticNativeAd)localObject2, paramHashMap, i, j);
      localObject2 = new android/widget/ImageView;
      localObject3 = b;
      localObject3 = b;
      ((ImageView)localObject2).<init>((Context)localObject3);
      localObject3 = "image_key";
      paramHashMap = paramHashMap.get(localObject3);
      paramHashMap = (Drawable)paramHashMap;
      ((ImageView)localObject2).setImageDrawable(paramHashMap);
      ((MoPubNativeAppInstallAdMapper)localObject1).setMediaView((View)localObject2);
      paramHashMap = b;
      paramHashMap = a;
      localObject2 = b;
      localObject2 = c;
      paramHashMap.onAdLoaded((MediationNativeAdapter)localObject2, (NativeAdMapper)localObject1);
      return;
    }
    catch (Exception localException)
    {
      paramHashMap = MoPubAdapter.TAG;
      paramHashMap = b.a;
      Object localObject1 = b.c;
      paramHashMap.onAdFailedToLoad((MediationNativeAdapter)localObject1, 0);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.dfp.adapters.MoPubAdapter.1.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */