package com.mopub.mobileads.dfp.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import com.google.android.gms.ads.mediation.NativeAppInstallAdMapper;
import com.mopub.common.util.Drawables;
import com.mopub.nativeads.NativeImageHelper;
import com.mopub.nativeads.StaticNativeAd;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MoPubNativeAppInstallAdMapper
  extends NativeAppInstallAdMapper
{
  private StaticNativeAd a;
  private int b;
  private ImageView c;
  private int d;
  
  public MoPubNativeAppInstallAdMapper(StaticNativeAd paramStaticNativeAd, HashMap paramHashMap, int paramInt1, int paramInt2)
  {
    a = paramStaticNativeAd;
    paramStaticNativeAd = a.getTitle();
    setHeadline(paramStaticNativeAd);
    paramStaticNativeAd = a.getText();
    setBody(paramStaticNativeAd);
    paramStaticNativeAd = a.getCallToAction();
    setCallToAction(paramStaticNativeAd);
    b = paramInt1;
    d = paramInt2;
    double d1 = 1.0D;
    Object localObject;
    String str;
    if (paramHashMap != null)
    {
      paramStaticNativeAd = new com/mopub/mobileads/dfp/adapters/MoPubNativeMappedImage;
      localObject = (Drawable)paramHashMap.get("icon_key");
      str = a.getIconImageUrl();
      paramStaticNativeAd.<init>((Drawable)localObject, str, d1);
      setIcon(paramStaticNativeAd);
      paramStaticNativeAd = new java/util/ArrayList;
      paramStaticNativeAd.<init>();
      localObject = new com/mopub/mobileads/dfp/adapters/MoPubNativeMappedImage;
      paramHashMap = (Drawable)paramHashMap.get("image_key");
      str = a.getMainImageUrl();
      ((MoPubNativeMappedImage)localObject).<init>(paramHashMap, str, d1);
      paramStaticNativeAd.add(localObject);
      setImages(paramStaticNativeAd);
    }
    else
    {
      paramStaticNativeAd = new com/mopub/mobileads/dfp/adapters/MoPubNativeMappedImage;
      paramHashMap = a.getIconImageUrl();
      localObject = null;
      paramStaticNativeAd.<init>(null, paramHashMap, d1);
      setIcon(paramStaticNativeAd);
      paramStaticNativeAd = new java/util/ArrayList;
      paramStaticNativeAd.<init>();
      paramHashMap = new com/mopub/mobileads/dfp/adapters/MoPubNativeMappedImage;
      str = a.getMainImageUrl();
      paramHashMap.<init>(null, str, d1);
      paramStaticNativeAd.add(paramHashMap);
      setImages(paramStaticNativeAd);
    }
    boolean bool = true;
    setOverrideClickHandling(bool);
    setOverrideImpressionRecording(bool);
  }
  
  public void handleClick(View paramView) {}
  
  public void recordImpression() {}
  
  public void trackView(View paramView)
  {
    Object localObject1 = a;
    ((StaticNativeAd)localObject1).prepare(paramView);
    boolean bool1 = paramView instanceof ViewGroup;
    if (!bool1) {
      return;
    }
    localObject1 = paramView;
    localObject1 = (ViewGroup)paramView;
    int i = ((ViewGroup)localObject1).getChildCount() + -1;
    Object localObject2 = ((ViewGroup)localObject1).getChildAt(i);
    boolean bool2 = localObject2 instanceof FrameLayout;
    if (bool2)
    {
      paramView = ((View)paramView).getContext();
      if (paramView == null) {
        return;
      }
      Object localObject3 = new android/widget/ImageView;
      ((ImageView)localObject3).<init>(paramView);
      c = ((ImageView)localObject3);
      localObject3 = a.getPrivacyInformationIconImageUrl();
      String str = a.getPrivacyInformationIconClickThroughUrl();
      if (localObject3 == null)
      {
        localObject3 = c;
        localObject4 = Drawables.NATIVE_PRIVACY_INFORMATION_ICON.createDrawable(paramView);
        ((ImageView)localObject3).setImageDrawable((Drawable)localObject4);
      }
      else
      {
        localObject4 = c;
        NativeImageHelper.loadImageView((String)localObject3, (ImageView)localObject4);
      }
      localObject3 = c;
      Object localObject4 = new com/mopub/mobileads/dfp/adapters/MoPubNativeAppInstallAdMapper$1;
      ((MoPubNativeAppInstallAdMapper.1)localObject4).<init>(this, paramView, str);
      ((ImageView)localObject3).setOnClickListener((View.OnClickListener)localObject4);
      localObject3 = c;
      str = null;
      ((ImageView)localObject3).setVisibility(0);
      localObject2 = (ViewGroup)localObject2;
      localObject3 = c;
      ((ViewGroup)localObject2).addView((View)localObject3);
      paramView = paramView.getResources().getDisplayMetrics();
      float f1 = density;
      i = d;
      float f2 = i * f1;
      double d1 = f2;
      double d2 = 0.5D;
      Double.isNaN(d1);
      d1 += d2;
      int k = (int)d1;
      localObject2 = new android/widget/FrameLayout$LayoutParams;
      ((FrameLayout.LayoutParams)localObject2).<init>(k, k);
      k = b;
      int j = 8388661;
      switch (k)
      {
      default: 
        gravity = j;
        break;
      case 3: 
        k = 8388691;
        f1 = 1.175506E-38F;
        gravity = k;
        break;
      case 2: 
        k = 8388693;
        f1 = 1.1755063E-38F;
        gravity = k;
        break;
      case 1: 
        gravity = j;
        break;
      case 0: 
        k = 8388659;
        f1 = 1.1755015E-38F;
        gravity = k;
      }
      c.setLayoutParams((ViewGroup.LayoutParams)localObject2);
      ((ViewGroup)localObject1).requestLayout();
      return;
    }
    paramView = MoPubAdapter.TAG;
  }
  
  public void untrackView(View paramView)
  {
    super.untrackView(paramView);
    Object localObject = a;
    ((StaticNativeAd)localObject).clear(paramView);
    paramView = c;
    if (paramView != null)
    {
      paramView = (ViewGroup)paramView.getParent();
      if (paramView != null)
      {
        paramView = (ViewGroup)c.getParent();
        localObject = c;
        paramView.removeView((View)localObject);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.dfp.adapters.MoPubNativeAppInstallAdMapper
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */