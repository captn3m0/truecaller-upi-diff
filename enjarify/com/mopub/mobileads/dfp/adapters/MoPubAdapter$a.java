package com.mopub.mobileads.dfp.adapters;

import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;
import com.google.android.gms.ads.mediation.MediationBannerListener;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubView;
import com.mopub.mobileads.MoPubView.BannerAdListener;

final class MoPubAdapter$a
  implements MoPubView.BannerAdListener
{
  private MediationBannerListener b;
  
  public MoPubAdapter$a(MoPubAdapter paramMoPubAdapter, MediationBannerListener paramMediationBannerListener)
  {
    b = paramMediationBannerListener;
  }
  
  public final void onBannerClicked(MoPubView paramMoPubView)
  {
    paramMoPubView = b;
    MoPubAdapter localMoPubAdapter = a;
    paramMoPubView.onAdClicked(localMoPubAdapter);
    paramMoPubView = b;
    localMoPubAdapter = a;
    paramMoPubView.onAdLeftApplication(localMoPubAdapter);
  }
  
  public final void onBannerCollapsed(MoPubView paramMoPubView)
  {
    paramMoPubView = b;
    MoPubAdapter localMoPubAdapter = a;
    paramMoPubView.onAdClosed(localMoPubAdapter);
  }
  
  public final void onBannerExpanded(MoPubView paramMoPubView)
  {
    paramMoPubView = b;
    MoPubAdapter localMoPubAdapter = a;
    paramMoPubView.onAdOpened(localMoPubAdapter);
  }
  
  public final void onBannerFailed(MoPubView paramMoPubView, MoPubErrorCode paramMoPubErrorCode)
  {
    try
    {
      paramMoPubView = MoPubAdapter.4.b;
      int i = paramMoPubErrorCode.ordinal();
      int j = paramMoPubView[i];
      switch (j)
      {
      default: 
        paramMoPubView = b;
        break;
      case 3: 
        paramMoPubView = b;
        paramMoPubErrorCode = a;
        k = 1;
        paramMoPubView.onAdFailedToLoad(paramMoPubErrorCode, k);
        return;
      case 2: 
        paramMoPubView = b;
        paramMoPubErrorCode = a;
        k = 2;
        paramMoPubView.onAdFailedToLoad(paramMoPubErrorCode, k);
        return;
      case 1: 
        paramMoPubView = b;
        paramMoPubErrorCode = a;
        k = 3;
        paramMoPubView.onAdFailedToLoad(paramMoPubErrorCode, k);
        return;
      }
      paramMoPubErrorCode = a;
      int k = 0;
      paramMoPubView.onAdFailedToLoad(paramMoPubErrorCode, 0);
      return;
    }
    catch (NoClassDefFoundError localNoClassDefFoundError) {}
  }
  
  public final void onBannerLoaded(MoPubView paramMoPubView)
  {
    Object localObject = MoPubAdapter.d(a);
    int i = k;
    int j = paramMoPubView.getAdWidth();
    if (i == j)
    {
      localObject = MoPubAdapter.d(a);
      i = l;
      int k = paramMoPubView.getAdHeight();
      if (i == k) {}
    }
    else
    {
      paramMoPubView = MoPubAdapter.TAG;
    }
    paramMoPubView = b;
    localObject = a;
    paramMoPubView.onAdLoaded((MediationBannerAdapter)localObject);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.dfp.adapters.MoPubAdapter.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */