package com.mopub.mobileads.dfp.adapters;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import com.mopub.common.UrlAction;
import com.mopub.common.UrlHandler;
import com.mopub.common.UrlHandler.Builder;

final class MoPubNativeAppInstallAdMapper$1
  implements View.OnClickListener
{
  MoPubNativeAppInstallAdMapper$1(MoPubNativeAppInstallAdMapper paramMoPubNativeAppInstallAdMapper, Context paramContext, String paramString) {}
  
  public final void onClick(View paramView)
  {
    paramView = new com/mopub/common/UrlHandler$Builder;
    paramView.<init>();
    Object localObject1 = UrlAction.IGNORE_ABOUT_SCHEME;
    Object localObject2 = new UrlAction[5];
    UrlAction localUrlAction = UrlAction.OPEN_NATIVE_BROWSER;
    localObject2[0] = localUrlAction;
    localUrlAction = UrlAction.OPEN_IN_APP_BROWSER;
    localObject2[1] = localUrlAction;
    localUrlAction = UrlAction.HANDLE_SHARE_TWEET;
    localObject2[2] = localUrlAction;
    localUrlAction = UrlAction.FOLLOW_DEEP_LINK_WITH_FALLBACK;
    localObject2[3] = localUrlAction;
    localUrlAction = UrlAction.FOLLOW_DEEP_LINK;
    localObject2[4] = localUrlAction;
    paramView = paramView.withSupportedUrlActions((UrlAction)localObject1, (UrlAction[])localObject2).build();
    localObject1 = a;
    localObject2 = b;
    paramView.handleUrl((Context)localObject1, (String)localObject2);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.dfp.adapters.MoPubNativeAppInstallAdMapper.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */