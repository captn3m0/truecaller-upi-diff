package com.mopub.mobileads.dfp.adapters;

public final class R$layout
{
  public static final int browser_actions_context_menu_page = 2131558507;
  public static final int browser_actions_context_menu_row = 2131558508;
  public static final int notification_action = 2131559045;
  public static final int notification_action_tombstone = 2131559046;
  public static final int notification_media_action = 2131559049;
  public static final int notification_media_cancel_action = 2131559050;
  public static final int notification_template_big_media = 2131559051;
  public static final int notification_template_big_media_custom = 2131559052;
  public static final int notification_template_big_media_narrow = 2131559053;
  public static final int notification_template_big_media_narrow_custom = 2131559054;
  public static final int notification_template_custom_big = 2131559055;
  public static final int notification_template_icon_group = 2131559056;
  public static final int notification_template_lines_media = 2131559057;
  public static final int notification_template_media = 2131559058;
  public static final int notification_template_media_custom = 2131559059;
  public static final int notification_template_part_chronometer = 2131559060;
  public static final int notification_template_part_time = 2131559061;
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.dfp.adapters.R.layout
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */