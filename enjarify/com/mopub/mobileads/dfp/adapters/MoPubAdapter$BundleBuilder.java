package com.mopub.mobileads.dfp.adapters;

import android.os.Bundle;

public final class MoPubAdapter$BundleBuilder
{
  private int a;
  
  public final Bundle build()
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    int i = a;
    localBundle.putInt("privacy_icon_size_dp", i);
    return localBundle;
  }
  
  public final BundleBuilder setPrivacyIconSize(int paramInt)
  {
    a = paramInt;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.dfp.adapters.MoPubAdapter.BundleBuilder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */