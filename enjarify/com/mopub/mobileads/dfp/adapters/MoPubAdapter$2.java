package com.mopub.mobileads.dfp.adapters;

import android.view.View;
import com.google.android.gms.ads.mediation.MediationNativeListener;
import com.mopub.nativeads.NativeAd.MoPubNativeEventListener;

final class MoPubAdapter$2
  implements NativeAd.MoPubNativeEventListener
{
  MoPubAdapter$2(MoPubAdapter paramMoPubAdapter, MediationNativeListener paramMediationNativeListener) {}
  
  public final void onClick(View paramView)
  {
    paramView = a;
    MoPubAdapter localMoPubAdapter = b;
    paramView.onAdClicked(localMoPubAdapter);
    paramView = a;
    localMoPubAdapter = b;
    paramView.onAdOpened(localMoPubAdapter);
    paramView = a;
    localMoPubAdapter = b;
    paramView.onAdLeftApplication(localMoPubAdapter);
    paramView = MoPubAdapter.TAG;
  }
  
  public final void onImpression(View paramView)
  {
    paramView = a;
    MoPubAdapter localMoPubAdapter = b;
    paramView.onAdImpression(localMoPubAdapter);
    paramView = MoPubAdapter.TAG;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.dfp.adapters.MoPubAdapter.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */