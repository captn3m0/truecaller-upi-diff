package com.mopub.mobileads.dfp.adapters;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.google.firebase.perf.network.FirebasePerfUrlConnection;
import java.net.URL;
import java.util.concurrent.Callable;

final class DownloadDrawablesAsync$1
  implements Callable
{
  DownloadDrawablesAsync$1(DownloadDrawablesAsync paramDownloadDrawablesAsync, URL paramURL) {}
  
  public final Drawable call()
  {
    Bitmap localBitmap = BitmapFactory.decodeStream(FirebasePerfUrlConnection.openStream(a));
    localBitmap.setDensity(160);
    BitmapDrawable localBitmapDrawable = new android/graphics/drawable/BitmapDrawable;
    Resources localResources = Resources.getSystem();
    localBitmapDrawable.<init>(localResources, localBitmap);
    return localBitmapDrawable;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.dfp.adapters.DownloadDrawablesAsync.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */