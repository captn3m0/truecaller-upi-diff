package com.mopub.mobileads.dfp.adapters;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class DownloadDrawablesAsync
  extends AsyncTask
{
  public static final String KEY_ICON = "icon_key";
  public static final String KEY_IMAGE = "image_key";
  private DrawableDownloadListener a;
  
  public DownloadDrawablesAsync(DrawableDownloadListener paramDrawableDownloadListener)
  {
    a = paramDrawableDownloadListener;
  }
  
  private HashMap a(Object... paramVarArgs)
  {
    paramVarArgs = (HashMap)paramVarArgs[0];
    Object localObject1 = Executors.newCachedThreadPool();
    Object localObject2 = (URL)paramVarArgs.get("image_key");
    localObject2 = a((URL)localObject2, (ExecutorService)localObject1);
    String str = "icon_key";
    paramVarArgs = (URL)paramVarArgs.get(str);
    paramVarArgs = a(paramVarArgs, (ExecutorService)localObject1);
    try
    {
      localObject1 = TimeUnit.SECONDS;
      long l = 10;
      localObject1 = ((Future)localObject2).get(l, (TimeUnit)localObject1);
      localObject1 = (Drawable)localObject1;
      localObject2 = TimeUnit.SECONDS;
      paramVarArgs = paramVarArgs.get(l, (TimeUnit)localObject2);
      paramVarArgs = (Drawable)paramVarArgs;
      localObject2 = new java/util/HashMap;
      ((HashMap)localObject2).<init>();
      str = "image_key";
      ((HashMap)localObject2).put(str, localObject1);
      localObject1 = "icon_key";
      ((HashMap)localObject2).put(localObject1, paramVarArgs);
      return (HashMap)localObject2;
    }
    catch (InterruptedException|ExecutionException|TimeoutException localInterruptedException)
    {
      paramVarArgs = MoPubAdapter.TAG;
    }
    return null;
  }
  
  private Future a(URL paramURL, ExecutorService paramExecutorService)
  {
    DownloadDrawablesAsync.1 local1 = new com/mopub/mobileads/dfp/adapters/DownloadDrawablesAsync$1;
    local1.<init>(this, paramURL);
    return paramExecutorService.submit(local1);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.dfp.adapters.DownloadDrawablesAsync
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */