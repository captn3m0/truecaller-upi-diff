package com.mopub.mobileads.dfp.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;
import com.google.android.gms.ads.mediation.MediationBannerListener;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.MediationNativeListener;
import com.google.android.gms.ads.mediation.NativeMediationAdRequest;
import com.mopub.common.MoPub;
import com.mopub.common.SdkConfiguration.Builder;
import com.mopub.common.SdkInitializationListener;
import com.mopub.common.logging.MoPubLog;
import com.mopub.mobileads.MoPubInterstitial;
import com.mopub.mobileads.MoPubView;
import com.mopub.nativeads.MoPubAdRenderer;
import com.mopub.nativeads.MoPubNative;
import com.mopub.nativeads.MoPubStaticNativeAdRenderer;
import com.mopub.nativeads.NativeAd.MoPubNativeEventListener;
import com.mopub.nativeads.RequestParameters;
import com.mopub.nativeads.RequestParameters.Builder;
import com.mopub.nativeads.RequestParameters.NativeAdAsset;
import com.mopub.nativeads.ViewBinder.Builder;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumSet;

public class MoPubAdapter
  implements MediationBannerAdapter, MediationInterstitialAdapter, MediationNativeAdapter
{
  public static final double DEFAULT_MOPUB_IMAGE_SCALE = 1.0D;
  public static final String TAG = "MoPubAdapter";
  private MoPubView a;
  private AdSize b;
  private MoPubInterstitial c;
  private int d;
  private int e;
  private NativeAd.MoPubNativeEventListener f;
  private RequestParameters g;
  
  private static int a(Date paramDate)
  {
    int i = Integer.parseInt((String)DateFormat.format("yyyy", paramDate));
    return Calendar.getInstance().get(1) - i;
  }
  
  private SdkInitializationListener a(MoPubView paramMoPubView, MoPubInterstitial paramMoPubInterstitial, MoPubNative paramMoPubNative)
  {
    MoPubAdapter.3 local3 = new com/mopub/mobileads/dfp/adapters/MoPubAdapter$3;
    local3.<init>(this, paramMoPubView, paramMoPubInterstitial, paramMoPubNative);
    return local3;
  }
  
  private static String a(MediationAdRequest paramMediationAdRequest, boolean paramBoolean)
  {
    Object localObject1 = paramMediationAdRequest.getBirthday();
    Object localObject2 = "";
    if (localObject1 != null)
    {
      i = a((Date)localObject1);
      localObject2 = new java/lang/StringBuilder;
      str1 = "m_age:";
      ((StringBuilder)localObject2).<init>(str1);
      localObject1 = Integer.toString(i);
      ((StringBuilder)localObject2).append((String)localObject1);
      localObject2 = ((StringBuilder)localObject2).toString();
    }
    int i = paramMediationAdRequest.getGender();
    String str1 = "";
    int j = -1;
    if (i != j)
    {
      j = 2;
      if (i == j)
      {
        str1 = "m_gender:f";
      }
      else
      {
        j = 1;
        if (i == j) {
          str1 = "m_gender:m";
        }
      }
    }
    localObject1 = new java/lang/StringBuilder;
    String str2 = "gmext,";
    ((StringBuilder)localObject1).<init>(str2);
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject2 = ",";
    ((StringBuilder)localObject1).append((String)localObject2);
    ((StringBuilder)localObject1).append(str1);
    if (paramBoolean)
    {
      bool = a(paramMediationAdRequest);
      if (bool) {
        return ((StringBuilder)localObject1).toString();
      }
      return "";
    }
    boolean bool = a(paramMediationAdRequest);
    if (bool) {
      return "";
    }
    return ((StringBuilder)localObject1).toString();
  }
  
  private void a(Context paramContext, String paramString, MoPubView paramMoPubView, MoPubInterstitial paramMoPubInterstitial, MoPubNative paramMoPubNative)
  {
    SdkConfiguration.Builder localBuilder = new com/mopub/common/SdkConfiguration$Builder;
    localBuilder.<init>(paramString);
    paramString = localBuilder.build();
    paramMoPubView = a(paramMoPubView, paramMoPubInterstitial, paramMoPubNative);
    MoPub.initializeSdk(paramContext, paramString, paramMoPubView);
  }
  
  private static boolean a(MediationAdRequest paramMediationAdRequest)
  {
    Date localDate = paramMediationAdRequest.getBirthday();
    if (localDate == null)
    {
      int i = paramMediationAdRequest.getGender();
      int j = -1;
      if (i == j)
      {
        paramMediationAdRequest = paramMediationAdRequest.getLocation();
        if (paramMediationAdRequest == null) {
          return false;
        }
      }
    }
    return true;
  }
  
  public View getBannerView()
  {
    return a;
  }
  
  public void onDestroy()
  {
    Object localObject = c;
    if (localObject != null)
    {
      ((MoPubInterstitial)localObject).destroy();
      c = null;
    }
    localObject = a;
    if (localObject != null)
    {
      ((MoPubView)localObject).destroy();
      a = null;
    }
  }
  
  public void onPause() {}
  
  public void onResume() {}
  
  public void requestBannerAd(Context paramContext, MediationBannerListener paramMediationBannerListener, Bundle paramBundle1, AdSize paramAdSize, MediationAdRequest paramMediationAdRequest, Bundle paramBundle2)
  {
    paramBundle2 = "adUnitId";
    String str = paramBundle1.getString(paramBundle2);
    b = paramAdSize;
    paramBundle1 = new com/mopub/mobileads/MoPubView;
    paramBundle1.<init>(paramContext);
    a = paramBundle1;
    paramBundle1 = a;
    paramAdSize = new com/mopub/mobileads/dfp/adapters/MoPubAdapter$a;
    paramAdSize.<init>(this, paramMediationBannerListener);
    paramBundle1.setBannerAdListener(paramAdSize);
    paramMediationBannerListener = a;
    paramMediationBannerListener.setAdUnitId(str);
    boolean bool1 = paramMediationAdRequest.isTesting();
    boolean bool2 = true;
    if (bool1)
    {
      paramMediationBannerListener = a;
      paramMediationBannerListener.setTesting(bool2);
    }
    paramMediationBannerListener = paramMediationAdRequest.getLocation();
    if (paramMediationBannerListener != null)
    {
      paramMediationBannerListener = a;
      paramAdSize = paramMediationAdRequest.getLocation();
      paramMediationBannerListener.setLocation(paramAdSize);
    }
    paramMediationBannerListener = a;
    paramAdSize = a(paramMediationAdRequest, false);
    paramMediationBannerListener.setKeywords(paramAdSize);
    paramMediationBannerListener = a;
    paramBundle1 = a(paramMediationAdRequest, bool2);
    paramMediationBannerListener.setUserDataKeywords(paramBundle1);
    bool1 = MoPub.isSdkInitialized();
    if (bool1)
    {
      a.loadAd();
      return;
    }
    MoPubView localMoPubView = a;
    a(paramContext, str, localMoPubView, null, null);
  }
  
  public void requestInterstitialAd(Context paramContext, MediationInterstitialListener paramMediationInterstitialListener, Bundle paramBundle1, MediationAdRequest paramMediationAdRequest, Bundle paramBundle2)
  {
    String str = paramBundle1.getString("adUnitId");
    paramBundle1 = new com/mopub/mobileads/MoPubInterstitial;
    paramBundle2 = paramContext;
    paramBundle2 = (Activity)paramContext;
    paramBundle1.<init>(paramBundle2, str);
    c = paramBundle1;
    paramBundle1 = c;
    paramBundle2 = new com/mopub/mobileads/dfp/adapters/MoPubAdapter$b;
    paramBundle2.<init>(this, paramMediationInterstitialListener);
    paramBundle1.setInterstitialAdListener(paramBundle2);
    boolean bool1 = paramMediationAdRequest.isTesting();
    boolean bool2 = true;
    if (bool1)
    {
      paramMediationInterstitialListener = c;
      paramMediationInterstitialListener.setTesting(bool2);
    }
    paramMediationInterstitialListener = c;
    paramBundle2 = a(paramMediationAdRequest, false);
    paramMediationInterstitialListener.setKeywords(paramBundle2);
    paramMediationInterstitialListener = c;
    paramBundle1 = a(paramMediationAdRequest, bool2);
    paramMediationInterstitialListener.setKeywords(paramBundle1);
    bool1 = MoPub.isSdkInitialized();
    if (bool1)
    {
      c.load();
      return;
    }
    MoPubInterstitial localMoPubInterstitial = c;
    a(paramContext, str, null, localMoPubInterstitial, null);
  }
  
  public void requestNativeAd(Context paramContext, MediationNativeListener paramMediationNativeListener, Bundle paramBundle1, NativeMediationAdRequest paramNativeMediationAdRequest, Bundle paramBundle2)
  {
    String str1 = "adUnitId";
    String str2 = paramBundle1.getString(str1);
    paramBundle1 = paramNativeMediationAdRequest.getNativeAdOptions();
    int i = 1;
    int j;
    if (paramBundle1 != null)
    {
      j = d;
      d = j;
    }
    else
    {
      d = i;
    }
    if (paramBundle2 != null)
    {
      paramBundle1 = "privacy_icon_size_dp";
      j = paramBundle2.getInt(paramBundle1);
      k = 10;
      if (j < k)
      {
        e = k;
      }
      else
      {
        k = 30;
        if (j > k) {
          e = k;
        } else {
          e = j;
        }
      }
    }
    else
    {
      j = 20;
      e = j;
    }
    paramBundle1 = new com/mopub/mobileads/dfp/adapters/MoPubAdapter$1;
    paramBundle1.<init>(this, paramMediationNativeListener, paramContext);
    if (str2 == null)
    {
      paramMediationNativeListener.onAdFailedToLoad(this, i);
      return;
    }
    MoPubNative localMoPubNative = new com/mopub/nativeads/MoPubNative;
    localMoPubNative.<init>(paramContext, str2, paramBundle1);
    paramBundle1 = new com/mopub/nativeads/ViewBinder$Builder;
    int k = 0;
    paramBundle1.<init>(0);
    paramBundle1 = paramBundle1.build();
    Object localObject1 = new com/mopub/nativeads/MoPubStaticNativeAdRenderer;
    ((MoPubStaticNativeAdRenderer)localObject1).<init>(paramBundle1);
    localMoPubNative.registerAdRenderer((MoPubAdRenderer)localObject1);
    paramBundle1 = RequestParameters.NativeAdAsset.TITLE;
    localObject1 = RequestParameters.NativeAdAsset.TEXT;
    Object localObject2 = RequestParameters.NativeAdAsset.CALL_TO_ACTION_TEXT;
    RequestParameters.NativeAdAsset localNativeAdAsset1 = RequestParameters.NativeAdAsset.MAIN_IMAGE;
    RequestParameters.NativeAdAsset localNativeAdAsset2 = RequestParameters.NativeAdAsset.ICON_IMAGE;
    paramBundle1 = EnumSet.of(paramBundle1, (Enum)localObject1, (Enum)localObject2, localNativeAdAsset1, localNativeAdAsset2);
    localObject1 = new com/mopub/nativeads/RequestParameters$Builder;
    ((RequestParameters.Builder)localObject1).<init>();
    paramBundle2 = a(paramNativeMediationAdRequest, false);
    paramBundle2 = ((RequestParameters.Builder)localObject1).keywords(paramBundle2);
    str1 = a(paramNativeMediationAdRequest, i);
    paramBundle2 = paramBundle2.userDataKeywords(str1);
    paramNativeMediationAdRequest = paramNativeMediationAdRequest.getLocation();
    paramNativeMediationAdRequest = paramBundle2.location(paramNativeMediationAdRequest);
    paramBundle1 = paramNativeMediationAdRequest.desiredAssets(paramBundle1).build();
    g = paramBundle1;
    boolean bool = MoPub.isSdkInitialized();
    if (bool)
    {
      paramContext = g;
      localMoPubNative.makeRequest(paramContext);
    }
    else
    {
      localNativeAdAsset1 = null;
      localNativeAdAsset2 = null;
      localObject1 = this;
      localObject2 = paramContext;
      a(paramContext, str2, null, null, localMoPubNative);
    }
    paramContext = new com/mopub/mobileads/dfp/adapters/MoPubAdapter$2;
    paramContext.<init>(this, paramMediationNativeListener);
    f = paramContext;
  }
  
  public void showInterstitial()
  {
    MoPubInterstitial localMoPubInterstitial = c;
    boolean bool = localMoPubInterstitial.isReady();
    if (bool)
    {
      c.show();
      return;
    }
    MoPubLog.i("Interstitial was not ready. Unable to load the interstitial");
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.dfp.adapters.MoPubAdapter
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */