package com.mopub.mobileads.dfp.adapters;

import android.content.Context;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.MediationNativeListener;
import com.mopub.nativeads.MoPubNative.MoPubNativeNetworkListener;
import com.mopub.nativeads.NativeAd;
import com.mopub.nativeads.NativeAd.MoPubNativeEventListener;
import com.mopub.nativeads.NativeErrorCode;
import com.mopub.nativeads.StaticNativeAd;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

final class MoPubAdapter$1
  implements MoPubNative.MoPubNativeNetworkListener
{
  MoPubAdapter$1(MoPubAdapter paramMoPubAdapter, MediationNativeListener paramMediationNativeListener, Context paramContext) {}
  
  public final void onNativeFail(NativeErrorCode paramNativeErrorCode)
  {
    Object localObject = MoPubAdapter.4.a;
    int i = paramNativeErrorCode.ordinal();
    i = localObject[i];
    int j = 1;
    MoPubAdapter localMoPubAdapter = null;
    switch (i)
    {
    default: 
      paramNativeErrorCode = a;
      localObject = c;
      paramNativeErrorCode.onAdFailedToLoad((MediationNativeAdapter)localObject, 0);
      return;
    case 4: 
      paramNativeErrorCode = a;
      localObject = c;
      paramNativeErrorCode.onAdFailedToLoad((MediationNativeAdapter)localObject, 0);
      return;
    case 3: 
      paramNativeErrorCode = a;
      localMoPubAdapter = c;
      paramNativeErrorCode.onAdFailedToLoad(localMoPubAdapter, j);
      return;
    case 2: 
      paramNativeErrorCode = a;
      localMoPubAdapter = c;
      paramNativeErrorCode.onAdFailedToLoad(localMoPubAdapter, j);
      return;
    }
    paramNativeErrorCode = a;
    localObject = c;
    paramNativeErrorCode.onAdFailedToLoad((MediationNativeAdapter)localObject, 3);
  }
  
  public final void onNativeLoad(NativeAd paramNativeAd)
  {
    NativeAd.MoPubNativeEventListener localMoPubNativeEventListener = MoPubAdapter.a(c);
    paramNativeAd.setMoPubNativeEventListener(localMoPubNativeEventListener);
    paramNativeAd = paramNativeAd.getBaseNativeAd();
    boolean bool = paramNativeAd instanceof StaticNativeAd;
    if (bool)
    {
      paramNativeAd = (StaticNativeAd)paramNativeAd;
      bool = false;
      localMoPubNativeEventListener = null;
      try
      {
        localObject1 = new java/util/HashMap;
        ((HashMap)localObject1).<init>();
        Object localObject2 = "icon_key";
        try
        {
          localObject3 = new java/net/URL;
          String str = paramNativeAd.getIconImageUrl();
          ((URL)localObject3).<init>(str);
          ((HashMap)localObject1).put(localObject2, localObject3);
          localObject2 = "image_key";
          localObject3 = new java/net/URL;
          str = paramNativeAd.getMainImageUrl();
          ((URL)localObject3).<init>(str);
          ((HashMap)localObject1).put(localObject2, localObject3);
        }
        catch (MalformedURLException localMalformedURLException)
        {
          localObject2 = MoPubAdapter.TAG;
          localObject2 = a;
          localObject3 = c;
          ((MediationNativeListener)localObject2).onAdFailedToLoad((MediationNativeAdapter)localObject3, 0);
        }
        localObject2 = new com/mopub/mobileads/dfp/adapters/DownloadDrawablesAsync;
        Object localObject3 = new com/mopub/mobileads/dfp/adapters/MoPubAdapter$1$1;
        ((MoPubAdapter.1.1)localObject3).<init>(this, paramNativeAd);
        ((DownloadDrawablesAsync)localObject2).<init>((DrawableDownloadListener)localObject3);
        int i = 1;
        paramNativeAd = new Object[i];
        paramNativeAd[0] = localObject1;
        ((DownloadDrawablesAsync)localObject2).execute(paramNativeAd);
        return;
      }
      catch (Exception localException)
      {
        paramNativeAd = MoPubAdapter.TAG;
        paramNativeAd = a;
        Object localObject1 = c;
        paramNativeAd.onAdFailedToLoad((MediationNativeAdapter)localObject1, 0);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.dfp.adapters.MoPubAdapter.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */