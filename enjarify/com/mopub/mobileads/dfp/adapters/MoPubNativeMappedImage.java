package com.mopub.mobileads.dfp.adapters;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import com.google.android.gms.ads.formats.NativeAd.Image;

public class MoPubNativeMappedImage
  extends NativeAd.Image
{
  private Drawable a;
  private Uri b;
  private double c;
  
  public MoPubNativeMappedImage(Drawable paramDrawable, String paramString, double paramDouble)
  {
    a = paramDrawable;
    paramDrawable = Uri.parse(paramString);
    b = paramDrawable;
    c = paramDouble;
  }
  
  public Drawable getDrawable()
  {
    return a;
  }
  
  public double getScale()
  {
    return c;
  }
  
  public Uri getUri()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.dfp.adapters.MoPubNativeMappedImage
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */