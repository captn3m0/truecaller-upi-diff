package com.mopub.mobileads.dfp.adapters;

import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubInterstitial;
import com.mopub.mobileads.MoPubInterstitial.InterstitialAdListener;

final class MoPubAdapter$b
  implements MoPubInterstitial.InterstitialAdListener
{
  private MediationInterstitialListener b;
  
  public MoPubAdapter$b(MoPubAdapter paramMoPubAdapter, MediationInterstitialListener paramMediationInterstitialListener)
  {
    b = paramMediationInterstitialListener;
  }
  
  public final void onInterstitialClicked(MoPubInterstitial paramMoPubInterstitial)
  {
    paramMoPubInterstitial = b;
    MoPubAdapter localMoPubAdapter = a;
    paramMoPubInterstitial.onAdClicked(localMoPubAdapter);
    paramMoPubInterstitial = b;
    localMoPubAdapter = a;
    paramMoPubInterstitial.onAdLeftApplication(localMoPubAdapter);
  }
  
  public final void onInterstitialDismissed(MoPubInterstitial paramMoPubInterstitial)
  {
    paramMoPubInterstitial = b;
    MoPubAdapter localMoPubAdapter = a;
    paramMoPubInterstitial.onAdClosed(localMoPubAdapter);
  }
  
  public final void onInterstitialFailed(MoPubInterstitial paramMoPubInterstitial, MoPubErrorCode paramMoPubErrorCode)
  {
    try
    {
      paramMoPubInterstitial = MoPubAdapter.4.b;
      int i = paramMoPubErrorCode.ordinal();
      int j = paramMoPubInterstitial[i];
      switch (j)
      {
      default: 
        paramMoPubInterstitial = b;
        break;
      case 3: 
        paramMoPubInterstitial = b;
        paramMoPubErrorCode = a;
        k = 1;
        paramMoPubInterstitial.onAdFailedToLoad(paramMoPubErrorCode, k);
        return;
      case 2: 
        paramMoPubInterstitial = b;
        paramMoPubErrorCode = a;
        k = 2;
        paramMoPubInterstitial.onAdFailedToLoad(paramMoPubErrorCode, k);
        return;
      case 1: 
        paramMoPubInterstitial = b;
        paramMoPubErrorCode = a;
        k = 3;
        paramMoPubInterstitial.onAdFailedToLoad(paramMoPubErrorCode, k);
        return;
      }
      paramMoPubErrorCode = a;
      int k = 0;
      paramMoPubInterstitial.onAdFailedToLoad(paramMoPubErrorCode, 0);
      return;
    }
    catch (NoClassDefFoundError localNoClassDefFoundError) {}
  }
  
  public final void onInterstitialLoaded(MoPubInterstitial paramMoPubInterstitial)
  {
    paramMoPubInterstitial = b;
    MoPubAdapter localMoPubAdapter = a;
    paramMoPubInterstitial.onAdLoaded(localMoPubAdapter);
  }
  
  public final void onInterstitialShown(MoPubInterstitial paramMoPubInterstitial)
  {
    paramMoPubInterstitial = b;
    MoPubAdapter localMoPubAdapter = a;
    paramMoPubInterstitial.onAdOpened(localMoPubAdapter);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.dfp.adapters.MoPubAdapter.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */