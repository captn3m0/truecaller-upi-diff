package com.mopub.mobileads.dfp.adapters;

import java.util.HashMap;

public abstract interface DrawableDownloadListener
{
  public abstract void onDownloadFailure();
  
  public abstract void onDownloadSuccess(HashMap paramHashMap);
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.dfp.adapters.DrawableDownloadListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */