package com.mopub.mobileads;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.view.View;
import android.widget.ImageView;
import com.mopub.common.ExternalViewabilitySessionManager;

final class VastVideoViewController$5
  implements MediaPlayer.OnPreparedListener
{
  VastVideoViewController$5(VastVideoViewController paramVastVideoViewController, VastVideoView paramVastVideoView) {}
  
  public final void onPrepared(MediaPlayer paramMediaPlayer)
  {
    paramMediaPlayer = b;
    int i = VastVideoViewController.h(paramMediaPlayer).getDuration();
    VastVideoViewController.a(paramMediaPlayer, i);
    paramMediaPlayer = VastVideoViewController.b(b);
    Object localObject1 = b.getLayout();
    Object localObject2 = b;
    int j = VastVideoViewController.e((VastVideoViewController)localObject2);
    paramMediaPlayer.onVideoPrepared((View)localObject1, j);
    VastVideoViewController.i(b);
    paramMediaPlayer = VastVideoViewController.j(b);
    if (paramMediaPlayer != null)
    {
      paramMediaPlayer = b;
      boolean bool = VastVideoViewController.k(paramMediaPlayer);
      if (!bool) {}
    }
    else
    {
      paramMediaPlayer = a;
      localObject1 = VastVideoViewController.l(b);
      localObject2 = VastVideoViewController.f(b).getDiskMediaFileUrl();
      paramMediaPlayer.prepareBlurredLastVideoFrame((ImageView)localObject1, (String)localObject2);
    }
    paramMediaPlayer = VastVideoViewController.n(b);
    i = b.a.getDuration();
    j = VastVideoViewController.m(b);
    paramMediaPlayer.calibrateAndMakeVisible(i, j);
    paramMediaPlayer = VastVideoViewController.o(b);
    i = VastVideoViewController.m(b);
    paramMediaPlayer.calibrateAndMakeVisible(i);
    VastVideoViewController.p(b);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastVideoViewController.5
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */