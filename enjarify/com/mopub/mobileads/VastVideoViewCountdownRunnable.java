package com.mopub.mobileads;

import android.os.Handler;
import com.mopub.common.Preconditions;

public class VastVideoViewCountdownRunnable
  extends RepeatingHandlerRunnable
{
  private final VastVideoViewController c;
  
  public VastVideoViewCountdownRunnable(VastVideoViewController paramVastVideoViewController, Handler paramHandler)
  {
    super(paramHandler);
    Preconditions.checkNotNull(paramHandler);
    Preconditions.checkNotNull(paramVastVideoViewController);
    c = paramVastVideoViewController;
  }
  
  public void doWork()
  {
    Object localObject1 = c;
    boolean bool = j;
    Object localObject2;
    if (bool)
    {
      localObject2 = d;
      int j = h;
      localObject1 = a;
      k = ((VastVideoView)localObject1).getCurrentPosition();
      ((VastVideoRadialCountdownWidget)localObject2).updateCountdownProgress(j, k);
    }
    localObject1 = c;
    bool = i;
    if (!bool)
    {
      localObject2 = a;
      int i = ((VastVideoView)localObject2).getCurrentPosition();
      k = h;
      if (i >= k)
      {
        k = 1;
        break label91;
      }
    }
    int k = 0;
    localObject1 = null;
    label91:
    if (k != 0)
    {
      localObject1 = c;
      ((VastVideoViewController)localObject1).a();
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastVideoViewCountdownRunnable
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */