package com.mopub.mobileads;

final class HtmlBannerWebView$a
  implements HtmlWebViewListener
{
  private final CustomEventBanner.CustomEventBannerListener a;
  
  public HtmlBannerWebView$a(CustomEventBanner.CustomEventBannerListener paramCustomEventBannerListener)
  {
    a = paramCustomEventBannerListener;
  }
  
  public final void onClicked()
  {
    a.onBannerClicked();
  }
  
  public final void onCollapsed()
  {
    a.onBannerCollapsed();
  }
  
  public final void onFailed(MoPubErrorCode paramMoPubErrorCode)
  {
    a.onBannerFailed(paramMoPubErrorCode);
  }
  
  public final void onLoaded(BaseHtmlWebView paramBaseHtmlWebView)
  {
    a.onBannerLoaded(paramBaseHtmlWebView);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.HtmlBannerWebView.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */