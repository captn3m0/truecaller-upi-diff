package com.mopub.mobileads;

import com.mopub.common.logging.MoPubLog;

final class VastManager$1
  implements VideoDownloader.a
{
  VastManager$1(VastManager paramVastManager, VastVideoConfig paramVastVideoConfig) {}
  
  public final void onComplete(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      Object localObject = a;
      paramBoolean = VastManager.a((VastVideoConfig)localObject);
      if (paramBoolean)
      {
        localObject = VastManager.a(b);
        VastVideoConfig localVastVideoConfig = a;
        ((VastManager.VastManagerListener)localObject).onVastVideoConfigurationPrepared(localVastVideoConfig);
        return;
      }
    }
    MoPubLog.d("Failed to download VAST video.");
    VastManager.a(b).onVastVideoConfigurationPrepared(null);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastManager.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */