package com.mopub.mobileads;

import android.view.View;

public abstract interface CustomEventBanner$CustomEventBannerListener
{
  public abstract void onBannerClicked();
  
  public abstract void onBannerCollapsed();
  
  public abstract void onBannerExpanded();
  
  public abstract void onBannerFailed(MoPubErrorCode paramMoPubErrorCode);
  
  public abstract void onBannerImpression();
  
  public abstract void onBannerLoaded(View paramView);
  
  public abstract void onLeaveApplication();
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.CustomEventBanner.CustomEventBannerListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */