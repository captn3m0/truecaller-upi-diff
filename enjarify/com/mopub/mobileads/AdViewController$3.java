package com.mopub.mobileads;

import android.view.View;
import android.widget.FrameLayout.LayoutParams;

final class AdViewController$3
  implements Runnable
{
  AdViewController$3(AdViewController paramAdViewController, View paramView) {}
  
  public final void run()
  {
    MoPubView localMoPubView = b.getMoPubView();
    if (localMoPubView == null) {
      return;
    }
    localMoPubView.removeAllViews();
    View localView = a;
    FrameLayout.LayoutParams localLayoutParams = AdViewController.a(b, localView);
    localMoPubView.addView(localView, localLayoutParams);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.AdViewController.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */