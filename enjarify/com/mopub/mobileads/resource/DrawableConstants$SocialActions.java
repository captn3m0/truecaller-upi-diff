package com.mopub.mobileads.resource;

public class DrawableConstants$SocialActions
{
  public static final int ADS_BY_LEFT_MARGIN_DIPS = 6;
  public static final int SOCIAL_ACTIONS_LEFT_MARGIN_DIPS = 16;
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.resource.DrawableConstants.SocialActions
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */