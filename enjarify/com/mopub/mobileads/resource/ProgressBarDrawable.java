package com.mopub.mobileads.resource;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Dips;

public class ProgressBarDrawable
  extends BaseWidgetDrawable
{
  private final Paint a;
  private final Paint b;
  private int c;
  private int d;
  private int e;
  private int f;
  private float g;
  private final int h;
  
  public ProgressBarDrawable(Context paramContext)
  {
    Paint localPaint = new android/graphics/Paint;
    localPaint.<init>();
    a = localPaint;
    a.setColor(-1);
    a.setAlpha(128);
    localPaint = a;
    Paint.Style localStyle1 = DrawableConstants.ProgressBar.BACKGROUND_STYLE;
    localPaint.setStyle(localStyle1);
    localPaint = a;
    boolean bool = true;
    localPaint.setAntiAlias(bool);
    localPaint = new android/graphics/Paint;
    localPaint.<init>();
    b = localPaint;
    localPaint = b;
    int i = DrawableConstants.ProgressBar.PROGRESS_COLOR;
    localPaint.setColor(i);
    b.setAlpha(255);
    localPaint = b;
    Paint.Style localStyle2 = DrawableConstants.ProgressBar.PROGRESS_STYLE;
    localPaint.setStyle(localStyle2);
    b.setAntiAlias(bool);
    int j = Dips.dipsToIntPixels(4.0F, paramContext);
    h = j;
  }
  
  public void draw(Canvas paramCanvas)
  {
    Rect localRect = getBounds();
    Object localObject = a;
    paramCanvas.drawRect(localRect, (Paint)localObject);
    float f1 = e;
    float f2 = c;
    f1 /= f2;
    float f3 = getBoundsleft;
    float f4 = getBoundstop;
    localObject = getBounds();
    int i = right;
    f2 = i;
    float f5 = f2 * f1;
    localRect = getBounds();
    float f6 = bottom;
    Paint localPaint = b;
    paramCanvas.drawRect(f3, f4, f5, f6, localPaint);
    int j = d;
    if (j > 0)
    {
      i = c;
      if (j < i)
      {
        f1 = getBoundsright;
        f2 = g;
        f3 = f1 * f2;
        f4 = getBoundstop;
        f1 = h;
        f5 = f3 + f1;
        localRect = getBounds();
        j = bottom;
        f6 = j;
        localPaint = b;
        paramCanvas.drawRect(f3, f4, f5, f6, localPaint);
      }
    }
  }
  
  public void forceCompletion()
  {
    int i = c;
    e = i;
  }
  
  public int getCurrentProgress()
  {
    return e;
  }
  
  public float getSkipRatio()
  {
    return g;
  }
  
  public void reset()
  {
    f = 0;
  }
  
  public void setDurationAndSkipOffset(int paramInt1, int paramInt2)
  {
    c = paramInt1;
    d = paramInt2;
    float f1 = d;
    float f2 = c;
    f1 /= f2;
    g = f1;
  }
  
  public void setProgress(int paramInt)
  {
    int i = f;
    if (paramInt >= i)
    {
      e = paramInt;
      f = paramInt;
    }
    else if (paramInt != 0)
    {
      String str = "Progress not monotonically increasing: last = %d, current = %d";
      int j = 2;
      Object[] arrayOfObject = new Object[j];
      Integer localInteger = Integer.valueOf(i);
      arrayOfObject[0] = localInteger;
      i = 1;
      Object localObject = Integer.valueOf(paramInt);
      arrayOfObject[i] = localObject;
      localObject = String.format(str, arrayOfObject);
      MoPubLog.d((String)localObject);
      forceCompletion();
    }
    invalidateSelf();
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.resource.ProgressBarDrawable
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */