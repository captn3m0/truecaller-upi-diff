package com.mopub.mobileads.resource;

import android.graphics.Paint.Cap;
import android.graphics.Typeface;

public class DrawableConstants$CloseButton
{
  public static final String DEFAULT_CLOSE_BUTTON_TEXT = "";
  public static final int EDGE_PADDING = 6;
  public static final int IMAGE_PADDING_DIPS = 15;
  public static final Paint.Cap STROKE_CAP = Paint.Cap.ROUND;
  public static final int STROKE_COLOR = 255;
  public static final float STROKE_WIDTH = 8.0F;
  public static final int TEXT_COLOR = 255;
  public static final int TEXT_RIGHT_MARGIN_DIPS = 0;
  public static final float TEXT_SIZE_SP = 20.0F;
  public static final Typeface TEXT_TYPEFACE = Typeface.create("Helvetica", 0);
  public static final int WIDGET_HEIGHT_DIPS = 56;
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.resource.DrawableConstants.CloseButton
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */