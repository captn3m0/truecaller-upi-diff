package com.mopub.mobileads.resource;

import android.graphics.Paint.Align;
import android.graphics.Paint.Style;

public class DrawableConstants$RadialCountdown
{
  public static final int BACKGROUND_ALPHA = 128;
  public static final int BACKGROUND_COLOR = 255;
  public static final Paint.Style BACKGROUND_STYLE = Paint.Style.STROKE;
  public static final int CIRCLE_STROKE_WIDTH_DIPS = 3;
  public static final int PADDING_DIPS = 5;
  public static final int PROGRESS_ALPHA = 255;
  public static final int PROGRESS_COLOR = 255;
  public static final Paint.Style PROGRESS_STYLE = Paint.Style.STROKE;
  public static final int RIGHT_MARGIN_DIPS = 16;
  public static final int SIDE_LENGTH_DIPS = 45;
  public static final float START_ANGLE = -90.0F;
  public static final Paint.Align TEXT_ALIGN = Paint.Align.CENTER;
  public static final int TEXT_COLOR = 255;
  public static final float TEXT_SIZE_SP = 18.0F;
  public static final int TOP_MARGIN_DIPS = 16;
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.resource.DrawableConstants.RadialCountdown
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */