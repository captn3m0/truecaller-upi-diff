package com.mopub.mobileads.resource;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import com.mopub.common.util.Dips;

public class CtaButtonDrawable
  extends BaseWidgetDrawable
{
  private final Paint a;
  private final Paint b;
  private final Paint c;
  private final RectF d;
  private final Rect e;
  private final int f;
  private String g;
  
  public CtaButtonDrawable(Context paramContext)
  {
    int i = Dips.dipsToIntPixels(2.0F, paramContext);
    float f1 = Dips.dipsToFloatPixels(15.0F, paramContext);
    Object localObject1 = new android/graphics/Paint;
    ((Paint)localObject1).<init>();
    a = ((Paint)localObject1);
    a.setColor(-16777216);
    localObject1 = a;
    int j = 51;
    ((Paint)localObject1).setAlpha(j);
    localObject1 = a;
    Paint.Style localStyle1 = DrawableConstants.CtaButton.BACKGROUND_STYLE;
    ((Paint)localObject1).setStyle(localStyle1);
    localObject1 = a;
    boolean bool = true;
    ((Paint)localObject1).setAntiAlias(bool);
    localObject1 = new android/graphics/Paint;
    ((Paint)localObject1).<init>();
    b = ((Paint)localObject1);
    localObject1 = b;
    int k = -1;
    ((Paint)localObject1).setColor(k);
    b.setAlpha(j);
    localObject1 = b;
    Paint.Style localStyle2 = DrawableConstants.CtaButton.OUTLINE_STYLE;
    ((Paint)localObject1).setStyle(localStyle2);
    localObject1 = b;
    float f2 = i;
    ((Paint)localObject1).setStrokeWidth(f2);
    b.setAntiAlias(bool);
    Object localObject2 = new android/graphics/Paint;
    ((Paint)localObject2).<init>();
    c = ((Paint)localObject2);
    c.setColor(k);
    localObject2 = c;
    localObject1 = DrawableConstants.CtaButton.TEXT_ALIGN;
    ((Paint)localObject2).setTextAlign((Paint.Align)localObject1);
    localObject2 = c;
    localObject1 = DrawableConstants.CtaButton.TEXT_TYPEFACE;
    ((Paint)localObject2).setTypeface((Typeface)localObject1);
    c.setTextSize(f1);
    c.setAntiAlias(bool);
    localObject2 = new android/graphics/Rect;
    ((Rect)localObject2).<init>();
    e = ((Rect)localObject2);
    g = "Learn More";
    localObject2 = new android/graphics/RectF;
    ((RectF)localObject2).<init>();
    d = ((RectF)localObject2);
    int m = Dips.dipsToIntPixels(6.0F, paramContext);
    f = m;
  }
  
  public void draw(Canvas paramCanvas)
  {
    Object localObject = d;
    Rect localRect = getBounds();
    ((RectF)localObject).set(localRect);
    localObject = d;
    int i = f;
    float f1 = i;
    float f2 = i;
    Paint localPaint = a;
    paramCanvas.drawRoundRect((RectF)localObject, f1, f2, localPaint);
    localObject = d;
    i = f;
    f1 = i;
    f2 = i;
    localPaint = b;
    paramCanvas.drawRoundRect((RectF)localObject, f1, f2, localPaint);
    localObject = c;
    localRect = e;
    String str = g;
    a(paramCanvas, (Paint)localObject, localRect, str);
  }
  
  public String getCtaText()
  {
    return g;
  }
  
  public void setCtaText(String paramString)
  {
    g = paramString;
    invalidateSelf();
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.resource.CtaButtonDrawable
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */