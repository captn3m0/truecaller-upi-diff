package com.mopub.mobileads.resource;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Numbers;

public class RadialCountdownDrawable
  extends BaseWidgetDrawable
{
  private final Paint a;
  private final Paint b;
  private final Paint c;
  private Rect d;
  private int e;
  private int f;
  private float g;
  
  public RadialCountdownDrawable(Context paramContext)
  {
    int i = Dips.dipsToIntPixels(3.0F, paramContext);
    float f1 = Dips.dipsToFloatPixels(18.0F, paramContext);
    Object localObject = new android/graphics/Paint;
    ((Paint)localObject).<init>();
    a = ((Paint)localObject);
    localObject = a;
    int j = -1;
    ((Paint)localObject).setColor(j);
    a.setAlpha(128);
    localObject = a;
    Paint.Style localStyle1 = DrawableConstants.RadialCountdown.BACKGROUND_STYLE;
    ((Paint)localObject).setStyle(localStyle1);
    localObject = a;
    float f2 = i;
    ((Paint)localObject).setStrokeWidth(f2);
    localObject = a;
    boolean bool = true;
    ((Paint)localObject).setAntiAlias(bool);
    localObject = new android/graphics/Paint;
    ((Paint)localObject).<init>();
    b = ((Paint)localObject);
    b.setColor(j);
    b.setAlpha(255);
    localObject = b;
    Paint.Style localStyle2 = DrawableConstants.RadialCountdown.PROGRESS_STYLE;
    ((Paint)localObject).setStyle(localStyle2);
    b.setStrokeWidth(f2);
    b.setAntiAlias(bool);
    Paint localPaint = new android/graphics/Paint;
    localPaint.<init>();
    c = localPaint;
    c.setColor(j);
    localPaint = c;
    localObject = DrawableConstants.RadialCountdown.TEXT_ALIGN;
    localPaint.setTextAlign((Paint.Align)localObject);
    c.setTextSize(f1);
    c.setAntiAlias(bool);
    paramContext = new android/graphics/Rect;
    paramContext.<init>();
    d = paramContext;
  }
  
  public void draw(Canvas paramCanvas)
  {
    int i = getBounds().centerX();
    int j = getBounds().centerY();
    int k = Math.min(i, j);
    float f1 = i;
    float f2 = j;
    float f3 = k;
    Object localObject1 = a;
    paramCanvas.drawCircle(f1, f2, f3, (Paint)localObject1);
    Object localObject2 = String.valueOf(f);
    Paint localPaint1 = c;
    Rect localRect = d;
    a(paramCanvas, localPaint1, localRect, (String)localObject2);
    RectF localRectF = new android/graphics/RectF;
    localObject2 = getBounds();
    localRectF.<init>((Rect)localObject2);
    float f4 = g;
    Paint localPaint2 = b;
    localObject1 = paramCanvas;
    paramCanvas.drawArc(localRectF, -90.0F, f4, false, localPaint2);
  }
  
  public int getInitialCountdownMilliseconds()
  {
    return e;
  }
  
  public void setInitialCountdown(int paramInt)
  {
    e = paramInt;
  }
  
  public void updateCountdownProgress(int paramInt)
  {
    int i = (int)Numbers.convertMillisecondsToSecondsRoundedUp(e - paramInt);
    f = i;
    float f1 = paramInt * 360.0F;
    float f2 = e;
    f1 /= f2;
    g = f1;
    invalidateSelf();
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.resource.RadialCountdownDrawable
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */