package com.mopub.mobileads.resource;

import android.graphics.Color;

public class DrawableConstants$GradientStrip
{
  public static final int END_COLOR = Color.argb(0, 0, 0, 0);
  public static final int GRADIENT_STRIP_HEIGHT_DIPS = 72;
  public static final int START_COLOR = Color.argb(102, 0, 0, 0);
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.resource.DrawableConstants.GradientStrip
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */