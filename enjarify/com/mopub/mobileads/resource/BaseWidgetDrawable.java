package com.mopub.mobileads.resource;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public abstract class BaseWidgetDrawable
  extends Drawable
{
  protected final void a(Canvas paramCanvas, Paint paramPaint, Rect paramRect, String paramString)
  {
    int i = paramString.length();
    paramPaint.getTextBounds(paramString, 0, i, paramRect);
    float f1 = paramPaint.descent();
    float f2 = paramPaint.ascent();
    f1 = (f1 - f2) / 2.0F;
    f2 = paramPaint.descent();
    f1 -= f2;
    f2 = getBounds().centerX();
    float f3 = getBounds().centerY() + f1;
    paramCanvas.drawText(paramString, f2, f3, paramPaint);
  }
  
  public int getOpacity()
  {
    return 0;
  }
  
  public void setAlpha(int paramInt) {}
  
  public void setColorFilter(ColorFilter paramColorFilter) {}
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.resource.BaseWidgetDrawable
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */