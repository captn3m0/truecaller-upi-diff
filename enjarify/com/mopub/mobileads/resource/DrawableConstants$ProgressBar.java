package com.mopub.mobileads.resource;

import android.graphics.Color;
import android.graphics.Paint.Style;

public class DrawableConstants$ProgressBar
{
  public static final int BACKGROUND_ALPHA = 128;
  public static final int BACKGROUND_COLOR = 255;
  public static final Paint.Style BACKGROUND_STYLE = Paint.Style.FILL;
  public static final int HEIGHT_DIPS = 4;
  public static final int NUGGET_WIDTH_DIPS = 4;
  public static final int PROGRESS_ALPHA = 255;
  public static final int PROGRESS_COLOR = Color.parseColor("#FFCC4D");
  public static final Paint.Style PROGRESS_STYLE = Paint.Style.FILL;
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.resource.DrawableConstants.ProgressBar
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */