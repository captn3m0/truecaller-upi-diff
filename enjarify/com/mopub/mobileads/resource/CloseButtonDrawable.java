package com.mopub.mobileads.resource;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Rect;

public class CloseButtonDrawable
  extends BaseWidgetDrawable
{
  private final Paint a;
  private final float b;
  
  public CloseButtonDrawable()
  {
    this(8.0F);
  }
  
  public CloseButtonDrawable(float paramFloat)
  {
    float f = paramFloat / 2.0F;
    b = f;
    Object localObject = new android/graphics/Paint;
    ((Paint)localObject).<init>();
    a = ((Paint)localObject);
    a.setColor(-1);
    a.setStrokeWidth(paramFloat);
    Paint localPaint = a;
    localObject = DrawableConstants.CloseButton.STROKE_CAP;
    localPaint.setStrokeCap((Paint.Cap)localObject);
  }
  
  public void draw(Canvas paramCanvas)
  {
    int i = getBounds().width();
    int j = getBounds().height();
    float f1 = b;
    float f2 = f1 + 0.0F;
    float f3 = j;
    float f4 = f3 - f1;
    float f5 = i;
    float f6 = f5 - f1;
    float f7 = f1 + 0.0F;
    Paint localPaint = a;
    paramCanvas.drawLine(f2, f4, f6, f7, localPaint);
    f1 = b;
    f2 = f1 + 0.0F;
    f4 = f1 + 0.0F;
    f6 = f5 - f1;
    f7 = f3 - f1;
    localPaint = a;
    paramCanvas.drawLine(f2, f4, f6, f7, localPaint);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.resource.CloseButtonDrawable
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */