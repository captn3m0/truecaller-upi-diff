package com.mopub.mobileads.resource;

import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Typeface;

public class DrawableConstants$CtaButton
{
  public static final int BACKGROUND_ALPHA = 51;
  public static final int BACKGROUND_COLOR = -16777216;
  public static final Paint.Style BACKGROUND_STYLE = Paint.Style.FILL;
  public static final int CORNER_RADIUS_DIPS = 6;
  public static final String DEFAULT_CTA_TEXT = "Learn More";
  public static final int HEIGHT_DIPS = 38;
  public static final int MARGIN_DIPS = 16;
  public static final int OUTLINE_ALPHA = 51;
  public static final int OUTLINE_COLOR = 255;
  public static final int OUTLINE_STROKE_WIDTH_DIPS = 2;
  public static final Paint.Style OUTLINE_STYLE = Paint.Style.STROKE;
  public static final Paint.Align TEXT_ALIGN = Paint.Align.CENTER;
  public static final int TEXT_COLOR = 255;
  public static final float TEXT_SIZE_SP = 15.0F;
  public static final Typeface TEXT_TYPEFACE = Typeface.create("Helvetica", 0);
  public static final int WIDTH_DIPS = 150;
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.resource.DrawableConstants.CtaButton
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */