package com.mopub.mobileads;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.util.Base64;
import android.view.View;
import android.widget.Toast;
import com.mopub.common.AdReport;
import com.mopub.common.util.DateAndTime;
import com.mopub.common.util.Intents;
import com.mopub.exceptions.IntentNotResolvableException;
import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class AdAlertReporter
{
  private final String a;
  private final View b;
  private final Context c;
  private Intent d;
  private String e;
  private String f;
  
  public AdAlertReporter(Context paramContext, View paramView, AdReport paramAdReport)
  {
    b = paramView;
    c = paramContext;
    paramContext = new java/text/SimpleDateFormat;
    Object localObject1 = Locale.US;
    paramContext.<init>("M/d/yy hh:mm:ss a z", (Locale)localObject1);
    paramView = DateAndTime.now();
    paramContext = paramContext.format(paramView);
    a = paramContext;
    paramContext = new android/content/Intent;
    paramContext.<init>("android.intent.action.SENDTO");
    d = paramContext;
    paramContext = d;
    paramView = Uri.parse("mailto:creative-review@mopub.com");
    paramContext.setData(paramView);
    paramContext = b;
    boolean bool1 = true;
    localObject1 = null;
    if (paramContext != null)
    {
      paramContext = paramContext.getRootView();
      if (paramContext != null)
      {
        paramContext = b.getRootView();
        boolean bool2 = paramContext.isDrawingCacheEnabled();
        paramContext.setDrawingCacheEnabled(bool1);
        localObject2 = paramContext.getDrawingCache();
        if (localObject2 != null)
        {
          localObject1 = Bitmap.createBitmap((Bitmap)localObject2);
          paramContext.setDrawingCacheEnabled(bool2);
        }
      }
    }
    paramContext = a((Bitmap)localObject1);
    e = "";
    localObject1 = "";
    f = ((String)localObject1);
    if (paramAdReport != null)
    {
      localObject1 = paramAdReport.toString();
      e = ((String)localObject1);
      paramAdReport = paramAdReport.getResponseString();
      f = paramAdReport;
    }
    paramAdReport = d;
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("New creative violation report - ");
    Object localObject2 = a;
    ((StringBuilder)localObject3).append((String)localObject2);
    localObject3 = ((StringBuilder)localObject3).toString();
    paramAdReport.putExtra("android.intent.extra.SUBJECT", (String)localObject3);
    paramAdReport = new String[3];
    localObject3 = e;
    paramAdReport[0] = localObject3;
    localObject1 = f;
    paramAdReport[bool1] = localObject1;
    paramAdReport[2] = paramContext;
    a(paramAdReport);
  }
  
  private static String a(Bitmap paramBitmap)
  {
    if (paramBitmap != null) {
      try
      {
        ByteArrayOutputStream localByteArrayOutputStream = new java/io/ByteArrayOutputStream;
        localByteArrayOutputStream.<init>();
        Bitmap.CompressFormat localCompressFormat = Bitmap.CompressFormat.JPEG;
        int i = 25;
        paramBitmap.compress(localCompressFormat, i, localByteArrayOutputStream);
        paramBitmap = localByteArrayOutputStream.toByteArray();
        localByteArrayOutputStream = null;
        paramBitmap = Base64.encodeToString(paramBitmap, 0);
      }
      catch (Exception localException) {}
    } else {
      paramBitmap = null;
    }
    return paramBitmap;
  }
  
  private void a(String... paramVarArgs)
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    int i = 0;
    for (;;)
    {
      int j = 3;
      if (i >= j) {
        break;
      }
      String str = paramVarArgs[i];
      ((StringBuilder)localObject).append(str);
      j = 2;
      if (i != j)
      {
        str = "\n=================\n";
        ((StringBuilder)localObject).append(str);
      }
      i += 1;
    }
    paramVarArgs = d;
    localObject = ((StringBuilder)localObject).toString();
    paramVarArgs.putExtra("android.intent.extra.TEXT", (String)localObject);
  }
  
  public void send()
  {
    try
    {
      Context localContext = c;
      Intent localIntent = d;
      Intents.startActivity(localContext, localIntent);
      return;
    }
    catch (IntentNotResolvableException localIntentNotResolvableException)
    {
      Toast.makeText(c, "No email client available", 0).show();
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.AdAlertReporter
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */