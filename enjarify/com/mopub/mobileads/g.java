package com.mopub.mobileads;

import android.text.TextUtils;
import com.mopub.common.Preconditions;
import com.mopub.mobileads.util.XmlUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.w3c.dom.Node;

abstract class g
{
  protected final Node a;
  
  g(Node paramNode)
  {
    Preconditions.checkNotNull(paramNode);
    a = paramNode;
  }
  
  final List a()
  {
    Object localObject = XmlUtils.getMatchingChildNodes(a, "Impression");
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    localObject = ((List)localObject).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject).hasNext();
      if (!bool1) {
        break;
      }
      String str = XmlUtils.getNodeValue((Node)((Iterator)localObject).next());
      boolean bool2 = TextUtils.isEmpty(str);
      if (!bool2)
      {
        VastTracker localVastTracker = new com/mopub/mobileads/VastTracker;
        localVastTracker.<init>(str);
        localArrayList.add(localVastTracker);
      }
    }
    return localArrayList;
  }
  
  final List b()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject = a;
    String str = "Error";
    localObject = XmlUtils.getMatchingChildNodes((Node)localObject, str);
    if (localObject == null) {
      return localArrayList;
    }
    localObject = ((List)localObject).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject).hasNext();
      if (!bool1) {
        break;
      }
      str = XmlUtils.getNodeValue((Node)((Iterator)localObject).next());
      boolean bool2 = TextUtils.isEmpty(str);
      if (!bool2)
      {
        VastTracker localVastTracker = new com/mopub/mobileads/VastTracker;
        boolean bool3 = true;
        localVastTracker.<init>(str, bool3);
        localArrayList.add(localVastTracker);
      }
    }
    return localArrayList;
  }
  
  final List c()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject1 = a;
    Object localObject2 = "Creatives";
    localObject1 = XmlUtils.getFirstMatchingChildNode((Node)localObject1, (String)localObject2);
    if (localObject1 == null) {
      return localArrayList;
    }
    localObject2 = "Creative";
    localObject1 = XmlUtils.getMatchingChildNodes((Node)localObject1, (String)localObject2);
    if (localObject1 == null) {
      return localArrayList;
    }
    localObject1 = ((List)localObject1).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (Node)((Iterator)localObject1).next();
      Object localObject3 = "Linear";
      localObject2 = XmlUtils.getFirstMatchingChildNode((Node)localObject2, (String)localObject3);
      if (localObject2 != null)
      {
        localObject3 = new com/mopub/mobileads/k;
        ((k)localObject3).<init>((Node)localObject2);
        localArrayList.add(localObject3);
      }
    }
    return localArrayList;
  }
  
  final List d()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject1 = a;
    Object localObject2 = "Creatives";
    localObject1 = XmlUtils.getFirstMatchingChildNode((Node)localObject1, (String)localObject2);
    if (localObject1 == null) {
      return localArrayList;
    }
    localObject2 = "Creative";
    localObject1 = XmlUtils.getMatchingChildNodes((Node)localObject1, (String)localObject2);
    if (localObject1 == null) {
      return localArrayList;
    }
    localObject1 = ((List)localObject1).iterator();
    Object localObject3;
    do
    {
      do
      {
        boolean bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        localObject2 = (Node)((Iterator)localObject1).next();
        localObject3 = "CompanionAds";
        localObject2 = XmlUtils.getFirstMatchingChildNode((Node)localObject2, (String)localObject3);
      } while (localObject2 == null);
      localObject3 = "Companion";
      localObject2 = XmlUtils.getMatchingChildNodes((Node)localObject2, (String)localObject3);
    } while (localObject2 == null);
    localObject2 = ((List)localObject2).iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject2).hasNext();
      if (!bool2) {
        break;
      }
      localObject3 = (Node)((Iterator)localObject2).next();
      h localh = new com/mopub/mobileads/h;
      localh.<init>((Node)localObject3);
      localArrayList.add(localh);
    }
    return localArrayList;
  }
  
  final VastExtensionParentXmlManager e()
  {
    Node localNode = a;
    Object localObject = "Extensions";
    localNode = XmlUtils.getFirstMatchingChildNode(localNode, (String)localObject);
    if (localNode == null) {
      return null;
    }
    localObject = new com/mopub/mobileads/VastExtensionParentXmlManager;
    ((VastExtensionParentXmlManager)localObject).<init>(localNode);
    return (VastExtensionParentXmlManager)localObject;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */