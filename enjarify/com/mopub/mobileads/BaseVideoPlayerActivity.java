package com.mopub.mobileads;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import com.mopub.common.logging.MoPubLog;

public class BaseVideoPlayerActivity
  extends Activity
{
  public static final String VIDEO_CLASS_EXTRAS_KEY = "video_view_class_name";
  public static final String VIDEO_URL = "video_url";
  
  public static Intent createIntentNativeVideo(Context paramContext, long paramLong, VastVideoConfig paramVastVideoConfig)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, MraidVideoPlayerActivity.class);
    localIntent.setFlags(268435456);
    localIntent.putExtra("video_view_class_name", "native");
    localIntent.putExtra("native_video_id", paramLong);
    localIntent.putExtra("native_vast_video_config", paramVastVideoConfig);
    return localIntent;
  }
  
  public static void startMraid(Context paramContext, String paramString)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, MraidVideoPlayerActivity.class);
    int i = 268435456;
    localIntent.setFlags(i);
    String str1 = "mraid";
    localIntent.putExtra("video_view_class_name", str1);
    String str2 = "video_url";
    localIntent.putExtra(str2, paramString);
    try
    {
      paramContext.startActivity(localIntent);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      MoPubLog.d("Activity MraidVideoPlayerActivity not found. Did you declare it in your AndroidManifest.xml?");
    }
  }
  
  public static void startNativeVideo(Context paramContext, long paramLong, VastVideoConfig paramVastVideoConfig)
  {
    Intent localIntent = createIntentNativeVideo(paramContext, paramLong, paramVastVideoConfig);
    try
    {
      paramContext.startActivity(localIntent);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      MoPubLog.d("Activity MraidVideoPlayerActivity not found. Did you declare it in your AndroidManifest.xml?");
    }
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    AudioManager localAudioManager = (AudioManager)getSystemService("audio");
    if (localAudioManager != null) {
      localAudioManager.abandonAudioFocus(null);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.BaseVideoPlayerActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */