package com.mopub.mobileads;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Intents;
import com.mopub.common.util.Reflection;
import com.mopub.mraid.MraidVideoViewController;

public class MraidVideoPlayerActivity
  extends BaseVideoPlayerActivity
  implements BaseVideoViewController.BaseVideoViewControllerListener
{
  private BaseVideoViewController a;
  private long b;
  
  private BaseVideoViewController a(Bundle paramBundle)
  {
    Object localObject1 = getIntent().getStringExtra("video_view_class_name");
    Object localObject2 = "vast";
    boolean bool1 = ((String)localObject2).equals(localObject1);
    Object localObject3;
    if (bool1)
    {
      localObject1 = new com/mopub/mobileads/VastVideoViewController;
      Bundle localBundle = getIntent().getExtras();
      long l = b;
      localObject3 = localObject1;
      ((VastVideoViewController)localObject1).<init>(this, localBundle, paramBundle, l, this);
      return (BaseVideoViewController)localObject1;
    }
    localObject2 = "mraid";
    bool1 = ((String)localObject2).equals(localObject1);
    if (bool1)
    {
      localObject1 = new com/mopub/mraid/MraidVideoViewController;
      localObject2 = getIntent().getExtras();
      ((MraidVideoViewController)localObject1).<init>(this, (Bundle)localObject2, paramBundle, this);
      return (BaseVideoViewController)localObject1;
    }
    localObject2 = "native";
    bool1 = ((String)localObject2).equals(localObject1);
    if (bool1)
    {
      int i = 4;
      localObject2 = new Class[i];
      localObject2[0] = Context.class;
      int j = 1;
      localObject2[j] = Bundle.class;
      int k = 2;
      localObject2[k] = Bundle.class;
      int m = 3;
      localObject2[m] = BaseVideoViewController.BaseVideoViewControllerListener.class;
      localObject1 = new Object[i];
      localObject1[0] = this;
      localObject3 = getIntent().getExtras();
      localObject1[j] = localObject3;
      localObject1[k] = paramBundle;
      localObject1[m] = this;
      paramBundle = "com.mopub.nativeads.NativeVideoViewController";
      boolean bool2 = Reflection.classFound(paramBundle);
      if (bool2)
      {
        paramBundle = "com.mopub.nativeads.NativeVideoViewController";
        localObject3 = BaseVideoViewController.class;
        try
        {
          paramBundle = Reflection.instantiateClassWithConstructor(paramBundle, (Class)localObject3, (Class[])localObject2, (Object[])localObject1);
          return (BaseVideoViewController)paramBundle;
        }
        catch (Exception localException)
        {
          paramBundle = new java/lang/IllegalStateException;
          paramBundle.<init>("Missing native video module");
          throw paramBundle;
        }
      }
      paramBundle = new java/lang/IllegalStateException;
      paramBundle.<init>("Missing native video module");
      throw paramBundle;
    }
    paramBundle = new java/lang/IllegalStateException;
    localObject1 = String.valueOf(localObject1);
    localObject1 = "Unsupported video type: ".concat((String)localObject1);
    paramBundle.<init>((String)localObject1);
    throw paramBundle;
  }
  
  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    paramIntent = a;
    if (paramIntent != null) {
      paramIntent.a(paramInt1, paramInt2);
    }
  }
  
  public void onBackPressed()
  {
    BaseVideoViewController localBaseVideoViewController = a;
    if (localBaseVideoViewController != null)
    {
      boolean bool = localBaseVideoViewController.backButtonEnabled();
      if (bool)
      {
        super.onBackPressed();
        localBaseVideoViewController = a;
        localBaseVideoViewController.onBackPressed();
      }
    }
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    BaseVideoViewController localBaseVideoViewController = a;
    if (localBaseVideoViewController != null) {
      localBaseVideoViewController.onConfigurationChanged(paramConfiguration);
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = 1;
    requestWindowFeature(i);
    Object localObject = getWindow();
    int j = 1024;
    ((Window)localObject).addFlags(j);
    localObject = getIntent();
    String str = "broadcastIdentifier";
    long l1 = -1;
    long l2 = ((Intent)localObject).getLongExtra(str, l1);
    b = l2;
    try
    {
      paramBundle = a(paramBundle);
      a = paramBundle;
      a.onCreate();
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      l2 = b;
      BaseBroadcastReceiver.broadcastAction(this, l2, "com.mopub.action.interstitial.fail");
      finish();
    }
  }
  
  protected void onDestroy()
  {
    BaseVideoViewController localBaseVideoViewController = a;
    if (localBaseVideoViewController != null) {
      localBaseVideoViewController.onDestroy();
    }
    super.onDestroy();
  }
  
  public void onFinish()
  {
    finish();
  }
  
  protected void onPause()
  {
    BaseVideoViewController localBaseVideoViewController = a;
    if (localBaseVideoViewController != null) {
      localBaseVideoViewController.onPause();
    }
    super.onPause();
  }
  
  protected void onResume()
  {
    super.onResume();
    BaseVideoViewController localBaseVideoViewController = a;
    if (localBaseVideoViewController != null) {
      localBaseVideoViewController.onResume();
    }
  }
  
  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    BaseVideoViewController localBaseVideoViewController = a;
    if (localBaseVideoViewController != null) {
      localBaseVideoViewController.onSaveInstanceState(paramBundle);
    }
  }
  
  public void onSetContentView(View paramView)
  {
    setContentView(paramView);
  }
  
  public void onSetRequestedOrientation(int paramInt)
  {
    setRequestedOrientation(paramInt);
  }
  
  public void onStartActivityForResult(Class paramClass, int paramInt, Bundle paramBundle)
  {
    if (paramClass == null) {
      return;
    }
    paramBundle = Intents.getStartActivityIntent(this, paramClass, paramBundle);
    try
    {
      startActivityForResult(paramBundle, paramInt);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>("Activity ");
      paramClass = paramClass.getName();
      localStringBuilder.append(paramClass);
      localStringBuilder.append(" not found. Did you declare it in your AndroidManifest.xml?");
      MoPubLog.d(localStringBuilder.toString());
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.MraidVideoPlayerActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */