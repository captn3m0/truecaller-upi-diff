package com.mopub.mobileads;

import android.text.TextUtils;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class VastMacroHelper
{
  private final List a;
  private final Map b;
  
  public VastMacroHelper(List paramList)
  {
    Preconditions.checkNotNull(paramList, "uris cannot be null");
    a = paramList;
    paramList = new java/util/HashMap;
    paramList.<init>();
    b = paramList;
    paramList = b;
    l locall = l.CACHEBUSTING;
    Object localObject = Locale.US;
    Object[] arrayOfObject = new Object[1];
    Long localLong = Long.valueOf(Math.round(Math.random() * 1.0E8D));
    arrayOfObject[0] = localLong;
    localObject = String.format((Locale)localObject, "%08d", arrayOfObject);
    paramList.put(locall, localObject);
  }
  
  public List getUris()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Iterator localIterator = a.iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      String str1 = (String)localIterator.next();
      boolean bool2 = TextUtils.isEmpty(str1);
      if (!bool2)
      {
        l[] arrayOfl = l.values();
        int i = arrayOfl.length;
        int j = 0;
        while (j < i)
        {
          Object localObject = arrayOfl[j];
          String str2 = (String)b.get(localObject);
          if (str2 == null) {
            str2 = "";
          }
          StringBuilder localStringBuilder = new java/lang/StringBuilder;
          String str3 = "\\[";
          localStringBuilder.<init>(str3);
          localObject = ((l)localObject).name();
          localStringBuilder.append((String)localObject);
          localStringBuilder.append("\\]");
          localObject = localStringBuilder.toString();
          str1 = str1.replaceAll((String)localObject, str2);
          j += 1;
        }
        localArrayList.add(str1);
      }
    }
    return localArrayList;
  }
  
  public VastMacroHelper withAssetUri(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      String str = "UTF-8";
      try
      {
        paramString = URLEncoder.encode(paramString, str);
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        localObject = "Failed to encode url";
        MoPubLog.w((String)localObject, localUnsupportedEncodingException);
      }
      Map localMap = b;
      Object localObject = l.ASSETURI;
      localMap.put(localObject, paramString);
    }
    return this;
  }
  
  public VastMacroHelper withContentPlayHead(Integer paramInteger)
  {
    if (paramInteger != null)
    {
      int i = paramInteger.intValue();
      Object localObject1 = "%02d:%02d:%02d.%03d";
      int j = 4;
      Object localObject2 = new Object[j];
      Object localObject3 = TimeUnit.MILLISECONDS;
      long l1 = i;
      localObject3 = Long.valueOf(((TimeUnit)localObject3).toHours(l1));
      localObject2[0] = localObject3;
      long l2 = TimeUnit.MILLISECONDS.toMinutes(l1);
      localObject3 = TimeUnit.HOURS;
      long l3 = 1L;
      long l4 = ((TimeUnit)localObject3).toMinutes(l3);
      l2 %= l4;
      localObject3 = Long.valueOf(l2);
      localObject2[1] = localObject3;
      long l5 = TimeUnit.MILLISECONDS.toSeconds(l1);
      TimeUnit localTimeUnit = TimeUnit.MINUTES;
      long l6 = localTimeUnit.toSeconds(l3);
      l5 %= l6;
      localObject3 = Long.valueOf(l5);
      localObject2[2] = localObject3;
      int k = 3;
      i %= 1000;
      paramInteger = Integer.valueOf(i);
      localObject2[k] = paramInteger;
      paramInteger = String.format((String)localObject1, (Object[])localObject2);
      boolean bool = TextUtils.isEmpty(paramInteger);
      if (!bool)
      {
        localObject1 = b;
        localObject2 = l.CONTENTPLAYHEAD;
        ((Map)localObject1).put(localObject2, paramInteger);
      }
    }
    return this;
  }
  
  public VastMacroHelper withErrorCode(VastErrorCode paramVastErrorCode)
  {
    if (paramVastErrorCode != null)
    {
      Map localMap = b;
      l locall = l.ERRORCODE;
      paramVastErrorCode = a;
      localMap.put(locall, paramVastErrorCode);
    }
    return this;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastMacroHelper
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */