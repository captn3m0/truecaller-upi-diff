package com.mopub.mobileads;

import android.content.Context;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Utils;
import com.mopub.mobileads.resource.ProgressBarDrawable;

public class VastVideoProgressBarWidget
  extends ImageView
{
  private ProgressBarDrawable a;
  private final int b;
  
  public VastVideoProgressBarWidget(Context paramContext)
  {
    super(paramContext);
    int i = (int)Utils.generateUniqueId();
    setId(i);
    ProgressBarDrawable localProgressBarDrawable = new com/mopub/mobileads/resource/ProgressBarDrawable;
    localProgressBarDrawable.<init>(paramContext);
    a = localProgressBarDrawable;
    localProgressBarDrawable = a;
    setImageDrawable(localProgressBarDrawable);
    int j = Dips.dipsToIntPixels(4.0F, paramContext);
    b = j;
  }
  
  public void calibrateAndMakeVisible(int paramInt1, int paramInt2)
  {
    a.setDurationAndSkipOffset(paramInt1, paramInt2);
    setVisibility(0);
  }
  
  ProgressBarDrawable getImageViewDrawable()
  {
    return a;
  }
  
  public void reset()
  {
    a.reset();
    a.setProgress(0);
  }
  
  public void setAnchorId(int paramInt)
  {
    RelativeLayout.LayoutParams localLayoutParams = new android/widget/RelativeLayout$LayoutParams;
    int i = b;
    localLayoutParams.<init>(-1, i);
    localLayoutParams.addRule(8, paramInt);
    setLayoutParams(localLayoutParams);
  }
  
  void setImageViewDrawable(ProgressBarDrawable paramProgressBarDrawable)
  {
    a = paramProgressBarDrawable;
  }
  
  public void updateProgress(int paramInt)
  {
    a.setProgress(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastVideoProgressBarWidget
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */