package com.mopub.mobileads;

import com.mopub.common.Preconditions;
import com.mopub.mobileads.util.XmlUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.w3c.dom.Node;

public class VastExtensionParentXmlManager
{
  private final Node a;
  
  VastExtensionParentXmlManager(Node paramNode)
  {
    Preconditions.checkNotNull(paramNode);
    a = paramNode;
  }
  
  final List a()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject1 = a;
    Object localObject2 = "Extension";
    localObject1 = XmlUtils.getMatchingChildNodes((Node)localObject1, (String)localObject2);
    if (localObject1 == null) {
      return localArrayList;
    }
    localObject1 = ((List)localObject1).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (Node)((Iterator)localObject1).next();
      VastExtensionXmlManager localVastExtensionXmlManager = new com/mopub/mobileads/VastExtensionXmlManager;
      localVastExtensionXmlManager.<init>((Node)localObject2);
      localArrayList.add(localVastExtensionXmlManager);
    }
    return localArrayList;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastExtensionParentXmlManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */