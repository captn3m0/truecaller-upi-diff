package com.mopub.mobileads;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import com.mopub.common.AdFormat;
import com.mopub.common.AdReport;
import com.mopub.common.MoPub;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.ManifestUtils;
import com.mopub.common.util.Reflection;
import com.mopub.common.util.Reflection.MethodBuilder;
import com.mopub.common.util.Visibility;
import com.mopub.mobileads.factories.AdViewControllerFactory;
import com.mopub.network.AdLoader;
import com.mopub.network.AdResponse;
import com.mopub.network.TrackingRequest;
import java.util.Map;
import java.util.TreeMap;

public class MoPubView
  extends FrameLayout
{
  protected AdViewController a;
  protected Object b;
  MoPubView.BannerAdListener c;
  private Context d;
  private int e;
  private BroadcastReceiver f;
  
  public MoPubView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public MoPubView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    ManifestUtils.checkWebViewActivitiesDeclared(paramContext);
    d = paramContext;
    int i = getVisibility();
    e = i;
    setHorizontalScrollBarEnabled(false);
    setVerticalScrollBarEnabled(false);
    paramContext = AdViewControllerFactory.create(paramContext, this);
    a = paramContext;
    paramContext = new com/mopub/mobileads/MoPubView$1;
    paramContext.<init>(this);
    f = paramContext;
    paramContext = new android/content/IntentFilter;
    paramContext.<init>("android.intent.action.SCREEN_OFF");
    paramContext.addAction("android.intent.action.USER_PRESENT");
    paramAttributeSet = d;
    BroadcastReceiver localBroadcastReceiver = f;
    paramAttributeSet.registerReceiver(localBroadcastReceiver, paramContext);
  }
  
  private void h()
  {
    Object localObject1 = b;
    if (localObject1 != null) {
      try
      {
        localObject2 = new com/mopub/common/util/Reflection$MethodBuilder;
        String str = "invalidate";
        ((Reflection.MethodBuilder)localObject2).<init>(localObject1, str);
        localObject1 = ((Reflection.MethodBuilder)localObject2).setAccessible();
        ((Reflection.MethodBuilder)localObject1).execute();
        return;
      }
      catch (Exception localException)
      {
        Object localObject2 = "Error invalidating adapter";
        MoPubLog.e((String)localObject2, localException);
      }
    }
  }
  
  private void setAdVisibility(int paramInt)
  {
    AdViewController localAdViewController = a;
    if (localAdViewController == null) {
      return;
    }
    paramInt = Visibility.isScreenVisible(paramInt);
    if (paramInt != 0)
    {
      a.b();
      return;
    }
    a.a(false);
  }
  
  protected final void a()
  {
    Object localObject1 = a;
    if (localObject1 != null)
    {
      Object localObject2 = e;
      if (localObject2 != null)
      {
        localObject2 = e.getClickTrackingUrl();
        localObject1 = a;
        TrackingRequest.makeTrackingHttpRequest((String)localObject2, (Context)localObject1);
      }
      localObject1 = c;
      if (localObject1 != null) {
        ((MoPubView.BannerAdListener)localObject1).onBannerClicked(this);
      }
    }
  }
  
  protected final void a(String paramString, Map paramMap)
  {
    Object localObject1 = a;
    if (localObject1 == null) {
      return;
    }
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool)
    {
      MoPubLog.d("Couldn't invoke custom event because the server did not specify one.");
      paramString = MoPubErrorCode.ADAPTER_NOT_FOUND;
      a(paramString);
      return;
    }
    localObject1 = b;
    if (localObject1 != null) {
      h();
    }
    MoPubLog.d("Loading custom event adapter.");
    localObject1 = "com.mopub.mobileads.factories.CustomEventBannerAdapterFactory";
    bool = Reflection.classFound((String)localObject1);
    if (bool)
    {
      localObject1 = "com.mopub.mobileads.factories.CustomEventBannerAdapterFactory";
      try
      {
        localObject1 = Class.forName((String)localObject1);
        Object localObject2 = new com/mopub/common/util/Reflection$MethodBuilder;
        String str = "create";
        ((Reflection.MethodBuilder)localObject2).<init>(null, str);
        localObject1 = ((Reflection.MethodBuilder)localObject2).setStatic((Class)localObject1);
        localObject2 = MoPubView.class;
        localObject1 = ((Reflection.MethodBuilder)localObject1).addParam((Class)localObject2, this);
        localObject2 = String.class;
        paramString = ((Reflection.MethodBuilder)localObject1).addParam((Class)localObject2, paramString);
        localObject1 = Map.class;
        paramString = paramString.addParam((Class)localObject1, paramMap);
        paramMap = Long.TYPE;
        localObject1 = a;
        long l = ((AdViewController)localObject1).getBroadcastIdentifier();
        localObject1 = Long.valueOf(l);
        paramString = paramString.addParam(paramMap, localObject1);
        paramMap = AdReport.class;
        localObject1 = a;
        localObject1 = ((AdViewController)localObject1).getAdReport();
        paramString = paramString.addParam(paramMap, localObject1);
        paramString = paramString.execute();
        b = paramString;
        paramString = new com/mopub/common/util/Reflection$MethodBuilder;
        paramMap = b;
        localObject1 = "loadAd";
        paramString.<init>(paramMap, (String)localObject1);
        paramString = paramString.setAccessible();
        paramString.execute();
        return;
      }
      catch (Exception paramString)
      {
        MoPubLog.e("Error loading custom event", paramString);
        return;
      }
    }
    MoPubLog.e("Could not load custom event -- missing banner module");
  }
  
  protected final boolean a(MoPubErrorCode paramMoPubErrorCode)
  {
    AdViewController localAdViewController = a;
    if (localAdViewController == null) {
      return false;
    }
    return localAdViewController.a(paramMoPubErrorCode);
  }
  
  protected final void b()
  {
    MoPubLog.d("Tracking impression for native adapter.");
    Object localObject1 = a;
    if (localObject1 != null)
    {
      Object localObject2 = e;
      if (localObject2 != null)
      {
        localObject2 = e.getImpressionTrackingUrls();
        localObject1 = a;
        TrackingRequest.makeTrackingHttpRequest((Iterable)localObject2, (Context)localObject1);
      }
    }
  }
  
  protected final void b(MoPubErrorCode paramMoPubErrorCode)
  {
    MoPubView.BannerAdListener localBannerAdListener = c;
    if (localBannerAdListener != null) {
      localBannerAdListener.onBannerFailed(this, paramMoPubErrorCode);
    }
  }
  
  protected final void c()
  {
    Object localObject = a;
    if (localObject != null)
    {
      ((AdViewController)localObject).c();
      AdLoader localAdLoader = d;
      if (localAdLoader == null)
      {
        localObject = "mAdLoader is not supposed to be null";
        MoPubLog.w((String)localObject);
      }
      else
      {
        d.creativeDownloadSuccess();
        localAdLoader = null;
        d = null;
      }
    }
    MoPubLog.d("adLoaded");
    localObject = c;
    if (localObject != null) {
      ((MoPubView.BannerAdListener)localObject).onBannerLoaded(this);
    }
  }
  
  final void d()
  {
    AdViewController localAdViewController = a;
    if (localAdViewController != null) {
      localAdViewController.a(false);
    }
  }
  
  public void destroy()
  {
    try
    {
      localObject = d;
      localBroadcastReceiver = f;
      ((Context)localObject).unregisterReceiver(localBroadcastReceiver);
    }
    catch (Exception localException)
    {
      localObject = "Failed to unregister screen state broadcast receiver (never registered).";
      MoPubLog.d((String)localObject);
    }
    removeAllViews();
    Object localObject = a;
    BroadcastReceiver localBroadcastReceiver = null;
    if (localObject != null)
    {
      boolean bool = f;
      if (!bool)
      {
        ((AdViewController)localObject).a();
        ((AdViewController)localObject).a(false);
        ((AdViewController)localObject).d();
        b = null;
        a = null;
        c = null;
        bool = true;
        f = bool;
      }
      a = null;
    }
    localObject = b;
    if (localObject != null)
    {
      h();
      b = null;
    }
  }
  
  final void e()
  {
    AdViewController localAdViewController = a;
    if (localAdViewController != null) {
      localAdViewController.b();
    }
  }
  
  final void f()
  {
    AdViewController localAdViewController = a;
    if (localAdViewController != null)
    {
      h = true;
      localAdViewController.a(false);
    }
  }
  
  public void forceRefresh()
  {
    Object localObject = b;
    if (localObject != null)
    {
      h();
      localObject = null;
      b = null;
    }
    localObject = a;
    if (localObject != null)
    {
      ((AdViewController)localObject).a();
      ((AdViewController)localObject).loadAd();
    }
  }
  
  final void g()
  {
    AdViewController localAdViewController = a;
    if (localAdViewController != null)
    {
      h = false;
      localAdViewController.b();
    }
  }
  
  public Activity getActivity()
  {
    return (Activity)d;
  }
  
  public AdFormat getAdFormat()
  {
    return AdFormat.BANNER;
  }
  
  public int getAdHeight()
  {
    AdViewController localAdViewController = a;
    if (localAdViewController != null) {
      return localAdViewController.getAdHeight();
    }
    return 0;
  }
  
  final Integer getAdTimeoutDelay$61acf5d5()
  {
    AdViewController localAdViewController = a;
    int i = 10000;
    if (localAdViewController == null) {
      return Integer.valueOf(i);
    }
    AdResponse localAdResponse = e;
    if (localAdResponse == null) {
      return Integer.valueOf(i);
    }
    return e.getAdTimeoutMillis(i);
  }
  
  public String getAdUnitId()
  {
    AdViewController localAdViewController = a;
    if (localAdViewController != null) {
      return localAdViewController.getAdUnitId();
    }
    return null;
  }
  
  AdViewController getAdViewController()
  {
    return a;
  }
  
  public int getAdWidth()
  {
    AdViewController localAdViewController = a;
    if (localAdViewController != null) {
      return localAdViewController.getAdWidth();
    }
    return 0;
  }
  
  public boolean getAutorefreshEnabled()
  {
    AdViewController localAdViewController = a;
    if (localAdViewController != null) {
      return localAdViewController.getCurrentAutoRefreshStatus();
    }
    MoPubLog.d("Can't get autorefresh status for destroyed MoPubView. Returning false.");
    return false;
  }
  
  public MoPubView.BannerAdListener getBannerAdListener()
  {
    return c;
  }
  
  public String getClickTrackingUrl()
  {
    return null;
  }
  
  public String getKeywords()
  {
    AdViewController localAdViewController = a;
    if (localAdViewController != null) {
      return localAdViewController.getKeywords();
    }
    return null;
  }
  
  public Map getLocalExtras()
  {
    Object localObject1 = a;
    if (localObject1 != null)
    {
      Object localObject2 = j;
      if (localObject2 != null)
      {
        localObject2 = new java/util/TreeMap;
        localObject1 = j;
        ((TreeMap)localObject2).<init>((Map)localObject1);
        return (Map)localObject2;
      }
      localObject1 = new java/util/TreeMap;
      ((TreeMap)localObject1).<init>();
      return (Map)localObject1;
    }
    localObject1 = new java/util/TreeMap;
    ((TreeMap)localObject1).<init>();
    return (Map)localObject1;
  }
  
  public Location getLocation()
  {
    AdViewController localAdViewController = a;
    if (localAdViewController != null)
    {
      boolean bool = MoPub.canCollectPersonalInformation();
      if (bool) {
        return a.getLocation();
      }
    }
    return null;
  }
  
  public String getResponseString()
  {
    return null;
  }
  
  public boolean getTesting()
  {
    AdViewController localAdViewController = a;
    if (localAdViewController != null) {
      return localAdViewController.getTesting();
    }
    MoPubLog.d("Can't get testing status for destroyed MoPubView. Returning false.");
    return false;
  }
  
  public String getUserDataKeywords()
  {
    AdViewController localAdViewController = a;
    if (localAdViewController != null)
    {
      boolean bool = MoPub.canCollectPersonalInformation();
      if (bool) {
        return a.getUserDataKeywords();
      }
    }
    return null;
  }
  
  public void loadAd()
  {
    AdViewController localAdViewController = a;
    if (localAdViewController != null) {
      localAdViewController.loadAd();
    }
  }
  
  protected void onWindowVisibilityChanged(int paramInt)
  {
    boolean bool = Visibility.hasScreenVisibilityChanged(e, paramInt);
    if (bool)
    {
      e = paramInt;
      paramInt = e;
      setAdVisibility(paramInt);
    }
  }
  
  public void setAdContentView(View paramView)
  {
    AdViewController localAdViewController = a;
    if (localAdViewController != null)
    {
      Handler localHandler = g;
      AdViewController.3 local3 = new com/mopub/mobileads/AdViewController$3;
      local3.<init>(localAdViewController, paramView);
      localHandler.post(local3);
    }
  }
  
  public void setAdUnitId(String paramString)
  {
    AdViewController localAdViewController = a;
    if (localAdViewController != null) {
      localAdViewController.setAdUnitId(paramString);
    }
  }
  
  public void setAutorefreshEnabled(boolean paramBoolean)
  {
    AdViewController localAdViewController = a;
    if (localAdViewController != null)
    {
      k = paramBoolean;
      localAdViewController.a(paramBoolean);
    }
  }
  
  public void setBannerAdListener(MoPubView.BannerAdListener paramBannerAdListener)
  {
    c = paramBannerAdListener;
  }
  
  public void setKeywords(String paramString)
  {
    AdViewController localAdViewController = a;
    if (localAdViewController != null) {
      localAdViewController.setKeywords(paramString);
    }
  }
  
  public void setLocalExtras(Map paramMap)
  {
    AdViewController localAdViewController = a;
    if (localAdViewController != null)
    {
      TreeMap localTreeMap;
      if (paramMap != null)
      {
        localTreeMap = new java/util/TreeMap;
        localTreeMap.<init>(paramMap);
      }
      else
      {
        localTreeMap = new java/util/TreeMap;
        localTreeMap.<init>();
      }
      j = localTreeMap;
    }
  }
  
  public void setLocation(Location paramLocation)
  {
    AdViewController localAdViewController = a;
    if (localAdViewController != null)
    {
      boolean bool = MoPub.canCollectPersonalInformation();
      if (bool)
      {
        localAdViewController = a;
        localAdViewController.setLocation(paramLocation);
      }
    }
  }
  
  public void setTesting(boolean paramBoolean)
  {
    AdViewController localAdViewController = a;
    if (localAdViewController != null) {
      localAdViewController.setTesting(paramBoolean);
    }
  }
  
  public void setTimeout(int paramInt) {}
  
  public void setUserDataKeywords(String paramString)
  {
    AdViewController localAdViewController = a;
    if (localAdViewController != null)
    {
      boolean bool = MoPub.canCollectPersonalInformation();
      if (bool)
      {
        localAdViewController = a;
        localAdViewController.setUserDataKeywords(paramString);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.MoPubView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */