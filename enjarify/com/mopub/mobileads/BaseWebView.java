package com.mopub.mobileads;

import android.content.Context;
import android.os.Build.VERSION;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import com.mopub.common.util.Views;
import com.mopub.mobileads.util.WebViews;

public class BaseWebView
  extends WebView
{
  private static boolean b = false;
  protected boolean a;
  
  public BaseWebView(Context paramContext)
  {
    super(paramContext);
    paramContext = null;
    enablePlugins(false);
    getSettings().setAllowFileAccess(false);
    getSettings().setAllowContentAccess(false);
    getSettings().setAllowFileAccessFromFileURLs(false);
    Object localObject1 = getSettings();
    ((WebSettings)localObject1).setAllowUniversalAccessFromFileURLs(false);
    WebViews.setDisableJSChromeClient(this);
    boolean bool = b;
    if (!bool)
    {
      localObject1 = getContext();
      int i = Build.VERSION.SDK_INT;
      int j = 19;
      int k = 1;
      if (i == j)
      {
        WebView localWebView = new android/webkit/WebView;
        Object localObject2 = ((Context)localObject1).getApplicationContext();
        localWebView.<init>((Context)localObject2);
        localWebView.setBackgroundColor(0);
        String str1 = "";
        String str2 = "text/html";
        String str3 = "UTF-8";
        localWebView.loadDataWithBaseURL(null, str1, str2, str3, null);
        paramContext = new android/view/WindowManager$LayoutParams;
        paramContext.<init>();
        width = k;
        height = k;
        type = 2005;
        flags = 16777240;
        format = -2;
        j = 8388659;
        gravity = j;
        localObject2 = "window";
        localObject1 = (WindowManager)((Context)localObject1).getSystemService((String)localObject2);
        ((WindowManager)localObject1).addView(localWebView, paramContext);
      }
      b = k;
    }
  }
  
  public void destroy()
  {
    boolean bool = a;
    if (bool) {
      return;
    }
    a = true;
    Views.removeFromParent(this);
    removeAllViews();
    super.destroy();
  }
  
  public void enablePlugins(boolean paramBoolean)
  {
    int i = Build.VERSION.SDK_INT;
    int j = 18;
    if (i >= j) {
      return;
    }
    if (paramBoolean)
    {
      localWebSettings = getSettings();
      localPluginState = WebSettings.PluginState.ON;
      localWebSettings.setPluginState(localPluginState);
      return;
    }
    WebSettings localWebSettings = getSettings();
    WebSettings.PluginState localPluginState = WebSettings.PluginState.OFF;
    localWebSettings.setPluginState(localPluginState);
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    WebViews.manageThirdPartyCookies(this);
  }
  
  void setIsDestroyed(boolean paramBoolean)
  {
    a = paramBoolean;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.BaseWebView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */