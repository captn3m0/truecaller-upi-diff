package com.mopub.mobileads;

import android.os.AsyncTask;
import com.mopub.common.logging.MoPubLog;
import java.lang.ref.WeakReference;
import java.util.Deque;

final class VideoDownloader$b
  extends AsyncTask
{
  private final VideoDownloader.a a;
  private final WeakReference b;
  
  VideoDownloader$b(VideoDownloader.a parama)
  {
    a = parama;
    parama = new java/lang/ref/WeakReference;
    parama.<init>(this);
    b = parama;
    parama = VideoDownloader.a();
    WeakReference localWeakReference = b;
    parama.add(localWeakReference);
  }
  
  /* Error */
  private static Boolean a(String... paramVarArgs)
  {
    // Byte code:
    //   0: aload_0
    //   1: ifnull +332 -> 333
    //   4: aload_0
    //   5: arraylength
    //   6: istore_1
    //   7: iload_1
    //   8: ifeq +325 -> 333
    //   11: iconst_0
    //   12: istore_1
    //   13: aconst_null
    //   14: astore_2
    //   15: aload_0
    //   16: iconst_0
    //   17: aaload
    //   18: astore_3
    //   19: aload_3
    //   20: ifnonnull +6 -> 26
    //   23: goto +310 -> 333
    //   26: aload_0
    //   27: iconst_0
    //   28: aaload
    //   29: astore_0
    //   30: iconst_0
    //   31: istore 4
    //   33: aconst_null
    //   34: astore_3
    //   35: aload_0
    //   36: invokestatic 38	com/mopub/common/MoPubHttpUrlConnection:getHttpUrlConnection	(Ljava/lang/String;)Ljava/net/HttpURLConnection;
    //   39: astore 5
    //   41: new 40	java/io/BufferedInputStream
    //   44: astore 6
    //   46: aload 5
    //   48: invokevirtual 46	java/net/HttpURLConnection:getInputStream	()Ljava/io/InputStream;
    //   51: astore 7
    //   53: aload 6
    //   55: aload 7
    //   57: invokespecial 49	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   60: aload 5
    //   62: invokevirtual 53	java/net/HttpURLConnection:getResponseCode	()I
    //   65: istore 4
    //   67: sipush 200
    //   70: istore 8
    //   72: iload 4
    //   74: iload 8
    //   76: if_icmplt +136 -> 212
    //   79: sipush 300
    //   82: istore 8
    //   84: iload 4
    //   86: iload 8
    //   88: if_icmplt +6 -> 94
    //   91: goto +121 -> 212
    //   94: aload 5
    //   96: invokevirtual 58	java/net/HttpURLConnection:getContentLength	()I
    //   99: istore 4
    //   101: ldc 59
    //   103: istore 8
    //   105: iload 4
    //   107: iload 8
    //   109: if_icmple +72 -> 181
    //   112: ldc 62
    //   114: astore_0
    //   115: iconst_2
    //   116: istore 9
    //   118: iload 9
    //   120: anewarray 65	java/lang/Object
    //   123: astore 10
    //   125: iload 4
    //   127: invokestatic 71	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   130: astore_3
    //   131: aload 10
    //   133: iconst_0
    //   134: aload_3
    //   135: aastore
    //   136: iconst_1
    //   137: istore_1
    //   138: iload 8
    //   140: invokestatic 71	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   143: astore_3
    //   144: aload 10
    //   146: iload_1
    //   147: aload_3
    //   148: aastore
    //   149: aload_0
    //   150: aload 10
    //   152: invokestatic 78	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   155: astore_0
    //   156: aload_0
    //   157: invokestatic 84	com/mopub/common/logging/MoPubLog:d	(Ljava/lang/String;)V
    //   160: getstatic 90	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   163: astore_0
    //   164: aload 6
    //   166: invokestatic 96	com/mopub/common/util/Streams:closeStream	(Ljava/io/Closeable;)V
    //   169: aload 5
    //   171: ifnull +8 -> 179
    //   174: aload 5
    //   176: invokevirtual 99	java/net/HttpURLConnection:disconnect	()V
    //   179: aload_0
    //   180: areturn
    //   181: aload_0
    //   182: aload 6
    //   184: invokestatic 105	com/mopub/common/CacheService:putToDiskCache	(Ljava/lang/String;Ljava/io/InputStream;)Z
    //   187: istore 11
    //   189: iload 11
    //   191: invokestatic 108	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   194: astore_0
    //   195: aload 6
    //   197: invokestatic 96	com/mopub/common/util/Streams:closeStream	(Ljava/io/Closeable;)V
    //   200: aload 5
    //   202: ifnull +8 -> 210
    //   205: aload 5
    //   207: invokevirtual 99	java/net/HttpURLConnection:disconnect	()V
    //   210: aload_0
    //   211: areturn
    //   212: ldc 110
    //   214: astore_0
    //   215: iload 4
    //   217: invokestatic 113	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   220: astore_2
    //   221: aload_0
    //   222: aload_2
    //   223: invokevirtual 117	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   226: astore_0
    //   227: aload_0
    //   228: invokestatic 84	com/mopub/common/logging/MoPubLog:d	(Ljava/lang/String;)V
    //   231: getstatic 90	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   234: astore_0
    //   235: aload 6
    //   237: invokestatic 96	com/mopub/common/util/Streams:closeStream	(Ljava/io/Closeable;)V
    //   240: aload 5
    //   242: ifnull +8 -> 250
    //   245: aload 5
    //   247: invokevirtual 99	java/net/HttpURLConnection:disconnect	()V
    //   250: aload_0
    //   251: areturn
    //   252: astore_0
    //   253: goto +63 -> 316
    //   256: astore_0
    //   257: aload 6
    //   259: astore_3
    //   260: goto +28 -> 288
    //   263: astore_0
    //   264: aload_3
    //   265: astore 6
    //   267: goto +49 -> 316
    //   270: astore_0
    //   271: goto +17 -> 288
    //   274: astore_0
    //   275: aconst_null
    //   276: astore 5
    //   278: aconst_null
    //   279: astore 6
    //   281: goto +35 -> 316
    //   284: astore_0
    //   285: aconst_null
    //   286: astore 5
    //   288: ldc 119
    //   290: astore_2
    //   291: aload_2
    //   292: aload_0
    //   293: invokestatic 122	com/mopub/common/logging/MoPubLog:d	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   296: getstatic 90	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   299: astore_0
    //   300: aload_3
    //   301: invokestatic 96	com/mopub/common/util/Streams:closeStream	(Ljava/io/Closeable;)V
    //   304: aload 5
    //   306: ifnull +8 -> 314
    //   309: aload 5
    //   311: invokevirtual 99	java/net/HttpURLConnection:disconnect	()V
    //   314: aload_0
    //   315: areturn
    //   316: aload 6
    //   318: invokestatic 96	com/mopub/common/util/Streams:closeStream	(Ljava/io/Closeable;)V
    //   321: aload 5
    //   323: ifnull +8 -> 331
    //   326: aload 5
    //   328: invokevirtual 99	java/net/HttpURLConnection:disconnect	()V
    //   331: aload_0
    //   332: athrow
    //   333: ldc 124
    //   335: invokestatic 84	com/mopub/common/logging/MoPubLog:d	(Ljava/lang/String;)V
    //   338: getstatic 90	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   341: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	342	0	paramVarArgs	String[]
    //   6	141	1	i	int
    //   14	278	2	str	String
    //   18	283	3	localObject1	Object
    //   31	185	4	j	int
    //   39	288	5	localHttpURLConnection	java.net.HttpURLConnection
    //   44	273	6	localObject2	Object
    //   51	5	7	localInputStream	java.io.InputStream
    //   70	69	8	k	int
    //   116	3	9	m	int
    //   123	28	10	arrayOfObject	Object[]
    //   187	3	11	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   60	65	252	finally
    //   94	99	252	finally
    //   118	123	252	finally
    //   125	130	252	finally
    //   134	136	252	finally
    //   138	143	252	finally
    //   147	149	252	finally
    //   150	155	252	finally
    //   156	160	252	finally
    //   160	163	252	finally
    //   182	187	252	finally
    //   189	194	252	finally
    //   215	220	252	finally
    //   222	226	252	finally
    //   227	231	252	finally
    //   231	234	252	finally
    //   60	65	256	java/lang/Exception
    //   94	99	256	java/lang/Exception
    //   118	123	256	java/lang/Exception
    //   125	130	256	java/lang/Exception
    //   134	136	256	java/lang/Exception
    //   138	143	256	java/lang/Exception
    //   147	149	256	java/lang/Exception
    //   150	155	256	java/lang/Exception
    //   156	160	256	java/lang/Exception
    //   160	163	256	java/lang/Exception
    //   182	187	256	java/lang/Exception
    //   189	194	256	java/lang/Exception
    //   215	220	256	java/lang/Exception
    //   222	226	256	java/lang/Exception
    //   227	231	256	java/lang/Exception
    //   231	234	256	java/lang/Exception
    //   41	44	263	finally
    //   46	51	263	finally
    //   55	60	263	finally
    //   292	296	263	finally
    //   296	299	263	finally
    //   41	44	270	java/lang/Exception
    //   46	51	270	java/lang/Exception
    //   55	60	270	java/lang/Exception
    //   35	39	274	finally
    //   35	39	284	java/lang/Exception
  }
  
  protected final void onCancelled()
  {
    MoPubLog.d("VideoDownloader task was cancelled.");
    Deque localDeque = VideoDownloader.a();
    WeakReference localWeakReference = b;
    localDeque.remove(localWeakReference);
    a.onComplete(false);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VideoDownloader.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */