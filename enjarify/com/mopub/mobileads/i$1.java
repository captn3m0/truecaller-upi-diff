package com.mopub.mobileads;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.mopub.common.MoPubBrowser;
import com.mopub.common.UrlAction;
import com.mopub.common.UrlHandler.ResultActions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Intents;
import com.mopub.exceptions.IntentNotResolvableException;

final class i$1
  implements UrlHandler.ResultActions
{
  i$1(i parami, String paramString, Context paramContext) {}
  
  public final void urlHandlingFailed(String paramString, UrlAction paramUrlAction) {}
  
  public final void urlHandlingSucceeded(String paramString, UrlAction paramUrlAction)
  {
    Object localObject = UrlAction.OPEN_IN_APP_BROWSER;
    if (paramUrlAction == localObject)
    {
      paramUrlAction = new android/os/Bundle;
      paramUrlAction.<init>();
      localObject = "URL";
      paramUrlAction.putString((String)localObject, paramString);
      paramString = a;
      boolean bool = TextUtils.isEmpty(paramString);
      if (!bool)
      {
        paramString = "mopub-dsp-creative-id";
        localObject = a;
        paramUrlAction.putString(paramString, (String)localObject);
      }
      paramString = b;
      localObject = MoPubBrowser.class;
      paramString = Intents.getStartActivityIntent(paramString, (Class)localObject, paramUrlAction);
      try
      {
        paramUrlAction = b;
        Intents.startActivity(paramUrlAction, paramString);
        return;
      }
      catch (IntentNotResolvableException localIntentNotResolvableException)
      {
        paramString = localIntentNotResolvableException.getMessage();
        MoPubLog.d(paramString);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.i.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */