package com.mopub.mobileads;

import android.content.Context;
import android.webkit.WebView;
import android.webkit.WebViewClient;

final class VastVideoViewController$10
  extends WebViewClient
{
  VastVideoViewController$10(VastVideoViewController paramVastVideoViewController, i parami) {}
  
  public final boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
  {
    paramWebView = a;
    Context localContext = b.mContext;
    String str = VastVideoViewController.f(b).getDspCreativeId();
    paramWebView.a(localContext, paramString, str);
    return true;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastVideoViewController.10
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */