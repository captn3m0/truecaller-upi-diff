package com.mopub.mobileads;

import com.mopub.common.UrlHandler.MoPubSchemeListener;

final class d$1
  implements UrlHandler.MoPubSchemeListener
{
  d$1(d paramd) {}
  
  public final void onClose()
  {
    d.b(a).onCollapsed();
  }
  
  public final void onFailLoad()
  {
    d.a(a).stopLoading();
    HtmlWebViewListener localHtmlWebViewListener = d.b(a);
    MoPubErrorCode localMoPubErrorCode = MoPubErrorCode.UNSPECIFIED;
    localHtmlWebViewListener.onFailed(localMoPubErrorCode);
  }
  
  public final void onFinishLoad()
  {
    HtmlWebViewListener localHtmlWebViewListener = d.b(a);
    BaseHtmlWebView localBaseHtmlWebView = d.a(a);
    localHtmlWebViewListener.onLoaded(localBaseHtmlWebView);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.d.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */