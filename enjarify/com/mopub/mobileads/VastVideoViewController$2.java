package com.mopub.mobileads;

import android.content.Context;
import android.webkit.WebView;
import android.webkit.WebViewClient;

final class VastVideoViewController$2
  extends WebViewClient
{
  VastVideoViewController$2(VastVideoViewController paramVastVideoViewController, VastCompanionAdConfig paramVastCompanionAdConfig, Context paramContext) {}
  
  public final boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
  {
    paramWebView = a;
    Context localContext = b;
    String str = VastVideoViewController.f(c).getDspCreativeId();
    paramWebView.a(localContext, paramString, str);
    return true;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastVideoViewController.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */