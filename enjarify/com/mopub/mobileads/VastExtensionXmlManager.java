package com.mopub.mobileads;

import com.mopub.common.Preconditions;
import com.mopub.mobileads.util.XmlUtils;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.w3c.dom.Node;

public class VastExtensionXmlManager
{
  public static final String AD_VERIFICATIONS = "AdVerifications";
  public static final String AVID = "AVID";
  public static final String ID = "id";
  public static final String MOAT = "Moat";
  public static final String TYPE = "type";
  public static final String VENDOR = "vendor";
  public static final String VERIFICATION = "Verification";
  public static final String VIDEO_VIEWABILITY_TRACKER = "MoPubViewabilityTracker";
  final Node a;
  
  public VastExtensionXmlManager(Node paramNode)
  {
    Preconditions.checkNotNull(paramNode);
    a = paramNode;
  }
  
  final Set a()
  {
    Object localObject1 = XmlUtils.getFirstMatchingChildNode(a, "AdVerifications");
    if (localObject1 == null) {
      return null;
    }
    Object localObject2 = "Verification";
    Object localObject3 = "vendor";
    Object localObject4 = Collections.singletonList("Moat");
    localObject1 = XmlUtils.getMatchingChildNodes((Node)localObject1, (String)localObject2, (String)localObject3, (List)localObject4);
    if (localObject1 != null)
    {
      boolean bool1 = ((List)localObject1).isEmpty();
      if (!bool1)
      {
        localObject2 = new com/mopub/mobileads/e;
        ((e)localObject2).<init>((List)localObject1);
        localObject1 = new java/util/HashSet;
        ((HashSet)localObject1).<init>();
        localObject2 = a.iterator();
        for (;;)
        {
          boolean bool2 = ((Iterator)localObject2).hasNext();
          if (!bool2) {
            break;
          }
          localObject3 = (Node)((Iterator)localObject2).next();
          if (localObject3 != null)
          {
            localObject4 = "ViewableImpression";
            localObject3 = XmlUtils.getFirstMatchingChildNode((Node)localObject3, (String)localObject4);
            if (localObject3 != null)
            {
              boolean bool3 = ((Node)localObject3).hasAttributes();
              if (bool3)
              {
                localObject4 = XmlUtils.getAttributeValue((Node)localObject3, "id");
                localObject3 = XmlUtils.getNodeValue((Node)localObject3);
                Locale localLocale = Locale.US;
                String str = "<ViewableImpression id=\"%s\"><![CDATA[%s]]</ViewableImpression>";
                int i = 2;
                Object[] arrayOfObject = new Object[i];
                arrayOfObject[0] = localObject4;
                bool3 = true;
                arrayOfObject[bool3] = localObject3;
                localObject3 = String.format(localLocale, str, arrayOfObject);
                break label208;
              }
            }
            bool2 = false;
            localObject3 = null;
            label208:
            if (localObject3 != null) {
              ((Set)localObject1).add(localObject3);
            }
          }
        }
        return (Set)localObject1;
      }
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastExtensionXmlManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */