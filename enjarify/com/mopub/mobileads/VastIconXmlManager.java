package com.mopub.mobileads;

import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Strings;
import com.mopub.mobileads.util.XmlUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.w3c.dom.Node;

public class VastIconXmlManager
{
  public static final String DURATION = "duration";
  public static final String HEIGHT = "height";
  public static final String ICON_CLICKS = "IconClicks";
  public static final String ICON_CLICK_THROUGH = "IconClickThrough";
  public static final String ICON_CLICK_TRACKING = "IconClickTracking";
  public static final String ICON_VIEW_TRACKING = "IconViewTracking";
  public static final String OFFSET = "offset";
  public static final String WIDTH = "width";
  final VastResourceXmlManager a;
  private final Node b;
  
  VastIconXmlManager(Node paramNode)
  {
    Preconditions.checkNotNull(paramNode);
    b = paramNode;
    VastResourceXmlManager localVastResourceXmlManager = new com/mopub/mobileads/VastResourceXmlManager;
    localVastResourceXmlManager.<init>(paramNode);
    a = localVastResourceXmlManager;
  }
  
  final Integer a()
  {
    return XmlUtils.getAttributeValueAsInt(b, "width");
  }
  
  final Integer b()
  {
    return XmlUtils.getAttributeValueAsInt(b, "height");
  }
  
  final Integer c()
  {
    Object localObject = b;
    String str = "offset";
    localObject = XmlUtils.getAttributeValue((Node)localObject, str);
    try
    {
      localObject = Strings.parseAbsoluteOffset((String)localObject);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      str = "Invalid VAST icon offset format: %s:";
      int i = 1;
      Object[] arrayOfObject = new Object[i];
      arrayOfObject[0] = localObject;
      MoPubLog.d(String.format(str, arrayOfObject));
      localObject = null;
    }
    return (Integer)localObject;
  }
  
  final Integer d()
  {
    Object localObject = b;
    String str = "duration";
    localObject = XmlUtils.getAttributeValue((Node)localObject, str);
    try
    {
      localObject = Strings.parseAbsoluteOffset((String)localObject);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      str = "Invalid VAST icon duration format: %s:";
      int i = 1;
      Object[] arrayOfObject = new Object[i];
      arrayOfObject[0] = localObject;
      MoPubLog.d(String.format(str, arrayOfObject));
      localObject = null;
    }
    return (Integer)localObject;
  }
  
  final List e()
  {
    Object localObject = XmlUtils.getFirstMatchingChildNode(b, "IconClicks");
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    if (localObject == null) {
      return localArrayList;
    }
    String str = "IconClickTracking";
    localObject = XmlUtils.getMatchingChildNodes((Node)localObject, str).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject).hasNext();
      if (!bool) {
        break;
      }
      str = XmlUtils.getNodeValue((Node)((Iterator)localObject).next());
      if (str != null)
      {
        VastTracker localVastTracker = new com/mopub/mobileads/VastTracker;
        localVastTracker.<init>(str);
        localArrayList.add(localVastTracker);
      }
    }
    return localArrayList;
  }
  
  final String f()
  {
    Node localNode = b;
    String str = "IconClicks";
    localNode = XmlUtils.getFirstMatchingChildNode(localNode, str);
    if (localNode == null) {
      return null;
    }
    return XmlUtils.getNodeValue(XmlUtils.getFirstMatchingChildNode(localNode, "IconClickThrough"));
  }
  
  final List g()
  {
    Object localObject = XmlUtils.getMatchingChildNodes(b, "IconViewTracking");
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    localObject = ((List)localObject).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject).hasNext();
      if (!bool) {
        break;
      }
      String str = XmlUtils.getNodeValue((Node)((Iterator)localObject).next());
      if (str != null)
      {
        VastTracker localVastTracker = new com/mopub/mobileads/VastTracker;
        localVastTracker.<init>(str);
        localArrayList.add(localVastTracker);
      }
    }
    return localArrayList;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastIconXmlManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */