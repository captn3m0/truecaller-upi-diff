package com.mopub.mobileads;

import com.mopub.common.AdFormat;
import com.mopub.common.util.ResponseHeader;
import com.mopub.network.HeaderUtils;
import org.json.JSONObject;

public class AdTypeTranslator
{
  public static final String BANNER_SUFFIX = "_banner";
  public static final String INTERSTITIAL_SUFFIX = "_interstitial";
  
  public static String getCustomEventName(AdFormat paramAdFormat, String paramString1, String paramString2, JSONObject paramJSONObject)
  {
    String str = "custom";
    boolean bool1 = str.equalsIgnoreCase(paramString1);
    if (bool1)
    {
      paramAdFormat = ResponseHeader.CUSTOM_EVENT_NAME;
      return HeaderUtils.extractHeader(paramJSONObject, paramAdFormat);
    }
    paramJSONObject = "json";
    boolean bool2 = paramJSONObject.equalsIgnoreCase(paramString1);
    if (bool2) {
      return AdTypeTranslator.CustomEventType.MOPUB_NATIVE.toString();
    }
    paramJSONObject = "json_video";
    bool2 = paramJSONObject.equalsIgnoreCase(paramString1);
    if (bool2) {
      return AdTypeTranslator.CustomEventType.MOPUB_VIDEO_NATIVE.toString();
    }
    paramJSONObject = "rewarded_video";
    bool2 = paramJSONObject.equalsIgnoreCase(paramString1);
    if (bool2) {
      return AdTypeTranslator.CustomEventType.MOPUB_REWARDED_VIDEO.toString();
    }
    paramJSONObject = "rewarded_playable";
    bool2 = paramJSONObject.equalsIgnoreCase(paramString1);
    if (bool2) {
      return AdTypeTranslator.CustomEventType.MOPUB_REWARDED_PLAYABLE.toString();
    }
    paramJSONObject = "html";
    bool2 = paramJSONObject.equalsIgnoreCase(paramString1);
    if (!bool2)
    {
      paramJSONObject = "mraid";
      bool2 = paramJSONObject.equalsIgnoreCase(paramString1);
      if (!bool2)
      {
        paramAdFormat = "interstitial";
        bool3 = paramAdFormat.equalsIgnoreCase(paramString1);
        if (bool3)
        {
          paramAdFormat = new java/lang/StringBuilder;
          paramAdFormat.<init>();
          paramAdFormat.append(paramString2);
          paramAdFormat.append("_interstitial");
          return AdTypeTranslator.CustomEventType.a(paramAdFormat.toString()).toString();
        }
        paramAdFormat = new java/lang/StringBuilder;
        paramAdFormat.<init>();
        paramAdFormat.append(paramString1);
        paramAdFormat.append("_banner");
        return AdTypeTranslator.CustomEventType.a(paramAdFormat.toString()).toString();
      }
    }
    paramString2 = AdFormat.INTERSTITIAL;
    boolean bool3 = paramString2.equals(paramAdFormat);
    if (bool3)
    {
      paramAdFormat = new java/lang/StringBuilder;
      paramAdFormat.<init>();
      paramAdFormat.append(paramString1);
      paramString1 = "_interstitial";
      paramAdFormat.append(paramString1);
      paramAdFormat = AdTypeTranslator.CustomEventType.a(paramAdFormat.toString());
    }
    else
    {
      paramAdFormat = new java/lang/StringBuilder;
      paramAdFormat.<init>();
      paramAdFormat.append(paramString1);
      paramString1 = "_banner";
      paramAdFormat.append(paramString1);
      paramAdFormat = AdTypeTranslator.CustomEventType.a(paramAdFormat.toString());
    }
    return paramAdFormat.toString();
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.AdTypeTranslator
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */