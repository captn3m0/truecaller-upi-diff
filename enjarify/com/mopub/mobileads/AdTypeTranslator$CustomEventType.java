package com.mopub.mobileads;

public enum AdTypeTranslator$CustomEventType
{
  private final String a;
  private final String b;
  private final boolean c;
  
  static
  {
    CustomEventType localCustomEventType = new com/mopub/mobileads/AdTypeTranslator$CustomEventType;
    Object localObject1 = localCustomEventType;
    localCustomEventType.<init>("GOOGLE_PLAY_SERVICES_BANNER", 0, "admob_native_banner", "com.mopub.mobileads.GooglePlayServicesBanner", false);
    GOOGLE_PLAY_SERVICES_BANNER = localCustomEventType;
    localObject1 = new com/mopub/mobileads/AdTypeTranslator$CustomEventType;
    ((CustomEventType)localObject1).<init>("GOOGLE_PLAY_SERVICES_INTERSTITIAL", 1, "admob_full_interstitial", "com.mopub.mobileads.GooglePlayServicesInterstitial", false);
    GOOGLE_PLAY_SERVICES_INTERSTITIAL = (CustomEventType)localObject1;
    localObject1 = new com/mopub/mobileads/AdTypeTranslator$CustomEventType;
    Object localObject2 = localObject1;
    ((CustomEventType)localObject1).<init>("MILLENNIAL_BANNER", 2, "millennial_native_banner", "com.mopub.mobileads.MillennialBanner", false);
    MILLENNIAL_BANNER = (CustomEventType)localObject1;
    localObject1 = new com/mopub/mobileads/AdTypeTranslator$CustomEventType;
    ((CustomEventType)localObject1).<init>("MILLENNIAL_INTERSTITIAL", 3, "millennial_full_interstitial", "com.mopub.mobileads.MillennialInterstitial", false);
    MILLENNIAL_INTERSTITIAL = (CustomEventType)localObject1;
    localObject1 = new com/mopub/mobileads/AdTypeTranslator$CustomEventType;
    boolean bool1 = true;
    localObject2 = localObject1;
    ((CustomEventType)localObject1).<init>("MRAID_BANNER", 4, "mraid_banner", "com.mopub.mraid.MraidBanner", bool1);
    MRAID_BANNER = (CustomEventType)localObject1;
    localObject1 = new com/mopub/mobileads/AdTypeTranslator$CustomEventType;
    boolean bool2 = true;
    ((CustomEventType)localObject1).<init>("MRAID_INTERSTITIAL", 5, "mraid_interstitial", "com.mopub.mraid.MraidInterstitial", bool2);
    MRAID_INTERSTITIAL = (CustomEventType)localObject1;
    localObject1 = new com/mopub/mobileads/AdTypeTranslator$CustomEventType;
    localObject2 = localObject1;
    ((CustomEventType)localObject1).<init>("HTML_BANNER", 6, "html_banner", "com.mopub.mobileads.HtmlBanner", bool1);
    HTML_BANNER = (CustomEventType)localObject1;
    localObject1 = new com/mopub/mobileads/AdTypeTranslator$CustomEventType;
    ((CustomEventType)localObject1).<init>("HTML_INTERSTITIAL", 7, "html_interstitial", "com.mopub.mobileads.HtmlInterstitial", bool2);
    HTML_INTERSTITIAL = (CustomEventType)localObject1;
    localObject1 = new com/mopub/mobileads/AdTypeTranslator$CustomEventType;
    localObject2 = localObject1;
    ((CustomEventType)localObject1).<init>("VAST_VIDEO_INTERSTITIAL", 8, "vast_interstitial", "com.mopub.mobileads.VastVideoInterstitial", bool1);
    VAST_VIDEO_INTERSTITIAL = (CustomEventType)localObject1;
    localObject1 = new com/mopub/mobileads/AdTypeTranslator$CustomEventType;
    ((CustomEventType)localObject1).<init>("MOPUB_NATIVE", 9, "mopub_native", "com.mopub.nativeads.MoPubCustomEventNative", bool2);
    MOPUB_NATIVE = (CustomEventType)localObject1;
    localObject1 = new com/mopub/mobileads/AdTypeTranslator$CustomEventType;
    localObject2 = localObject1;
    ((CustomEventType)localObject1).<init>("MOPUB_VIDEO_NATIVE", 10, "mopub_video_native", "com.mopub.nativeads.MoPubCustomEventVideoNative", bool1);
    MOPUB_VIDEO_NATIVE = (CustomEventType)localObject1;
    localObject1 = new com/mopub/mobileads/AdTypeTranslator$CustomEventType;
    ((CustomEventType)localObject1).<init>("MOPUB_REWARDED_VIDEO", 11, "rewarded_video", "com.mopub.mobileads.MoPubRewardedVideo", bool2);
    MOPUB_REWARDED_VIDEO = (CustomEventType)localObject1;
    localObject1 = new com/mopub/mobileads/AdTypeTranslator$CustomEventType;
    localObject2 = localObject1;
    ((CustomEventType)localObject1).<init>("MOPUB_REWARDED_PLAYABLE", 12, "rewarded_playable", "com.mopub.mobileads.MoPubRewardedPlayable", bool1);
    MOPUB_REWARDED_PLAYABLE = (CustomEventType)localObject1;
    localObject1 = new com/mopub/mobileads/AdTypeTranslator$CustomEventType;
    ((CustomEventType)localObject1).<init>("UNSPECIFIED", 13, "", null, false);
    UNSPECIFIED = (CustomEventType)localObject1;
    localObject1 = new CustomEventType[14];
    localObject2 = GOOGLE_PLAY_SERVICES_BANNER;
    localObject1[0] = localObject2;
    localObject2 = GOOGLE_PLAY_SERVICES_INTERSTITIAL;
    localObject1[1] = localObject2;
    localObject2 = MILLENNIAL_BANNER;
    localObject1[2] = localObject2;
    localObject2 = MILLENNIAL_INTERSTITIAL;
    localObject1[3] = localObject2;
    localObject2 = MRAID_BANNER;
    localObject1[4] = localObject2;
    localObject2 = MRAID_INTERSTITIAL;
    localObject1[5] = localObject2;
    localObject2 = HTML_BANNER;
    localObject1[6] = localObject2;
    localObject2 = HTML_INTERSTITIAL;
    localObject1[7] = localObject2;
    localObject2 = VAST_VIDEO_INTERSTITIAL;
    localObject1[8] = localObject2;
    localObject2 = MOPUB_NATIVE;
    localObject1[9] = localObject2;
    localObject2 = MOPUB_VIDEO_NATIVE;
    localObject1[10] = localObject2;
    localObject2 = MOPUB_REWARDED_VIDEO;
    localObject1[11] = localObject2;
    localObject2 = MOPUB_REWARDED_PLAYABLE;
    localObject1[12] = localObject2;
    localObject2 = UNSPECIFIED;
    localObject1[13] = localObject2;
    d = (CustomEventType[])localObject1;
  }
  
  private AdTypeTranslator$CustomEventType(String paramString2, String paramString3, boolean paramBoolean)
  {
    a = paramString2;
    b = paramString3;
    c = paramBoolean;
  }
  
  public static boolean isMoPubSpecific(String paramString)
  {
    CustomEventType[] arrayOfCustomEventType = values();
    int i = arrayOfCustomEventType.length;
    int j = 0;
    while (j < i)
    {
      localCustomEventType = arrayOfCustomEventType[j];
      String str = b;
      if (str != null)
      {
        boolean bool = str.equals(paramString);
        if (bool) {
          break label59;
        }
      }
      j += 1;
    }
    CustomEventType localCustomEventType = UNSPECIFIED;
    label59:
    return c;
  }
  
  public final String toString()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.AdTypeTranslator.CustomEventType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */