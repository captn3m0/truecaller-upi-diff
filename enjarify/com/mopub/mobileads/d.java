package com.mopub.mobileads;

import android.content.Context;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.mopub.common.UrlAction;
import com.mopub.common.UrlHandler;
import com.mopub.common.UrlHandler.Builder;
import com.mopub.common.UrlHandler.MoPubSchemeListener;
import com.mopub.common.UrlHandler.ResultActions;
import java.util.EnumSet;

final class d
  extends WebViewClient
{
  private final EnumSet a;
  private final Context b;
  private final String c;
  private final HtmlWebViewListener d;
  private final BaseHtmlWebView e;
  private final String f;
  
  d(HtmlWebViewListener paramHtmlWebViewListener, BaseHtmlWebView paramBaseHtmlWebView, String paramString1, String paramString2)
  {
    Object localObject = UrlAction.HANDLE_MOPUB_SCHEME;
    UrlAction[] arrayOfUrlAction = new UrlAction[8];
    UrlAction localUrlAction = UrlAction.IGNORE_ABOUT_SCHEME;
    arrayOfUrlAction[0] = localUrlAction;
    localUrlAction = UrlAction.HANDLE_PHONE_SCHEME;
    arrayOfUrlAction[1] = localUrlAction;
    localUrlAction = UrlAction.OPEN_APP_MARKET;
    arrayOfUrlAction[2] = localUrlAction;
    localUrlAction = UrlAction.OPEN_NATIVE_BROWSER;
    arrayOfUrlAction[3] = localUrlAction;
    localUrlAction = UrlAction.OPEN_IN_APP_BROWSER;
    arrayOfUrlAction[4] = localUrlAction;
    localUrlAction = UrlAction.HANDLE_SHARE_TWEET;
    arrayOfUrlAction[5] = localUrlAction;
    localUrlAction = UrlAction.FOLLOW_DEEP_LINK_WITH_FALLBACK;
    arrayOfUrlAction[6] = localUrlAction;
    localUrlAction = UrlAction.FOLLOW_DEEP_LINK;
    arrayOfUrlAction[7] = localUrlAction;
    localObject = EnumSet.of((Enum)localObject, arrayOfUrlAction);
    a = ((EnumSet)localObject);
    d = paramHtmlWebViewListener;
    e = paramBaseHtmlWebView;
    f = paramString1;
    c = paramString2;
    paramHtmlWebViewListener = paramBaseHtmlWebView.getContext();
    b = paramHtmlWebViewListener;
  }
  
  public final boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
  {
    paramWebView = new com/mopub/common/UrlHandler$Builder;
    paramWebView.<init>();
    Object localObject = c;
    paramWebView = paramWebView.withDspCreativeId((String)localObject);
    localObject = a;
    paramWebView = paramWebView.withSupportedUrlActions((EnumSet)localObject);
    localObject = new com/mopub/mobileads/d$2;
    ((d.2)localObject).<init>(this);
    paramWebView = paramWebView.withResultActions((UrlHandler.ResultActions)localObject);
    localObject = new com/mopub/mobileads/d$1;
    ((d.1)localObject).<init>(this);
    paramWebView = paramWebView.withMoPubSchemeListener((UrlHandler.MoPubSchemeListener)localObject).build();
    localObject = b;
    boolean bool = e.wasClicked();
    paramWebView.handleUrl((Context)localObject, paramString, bool);
    return true;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */