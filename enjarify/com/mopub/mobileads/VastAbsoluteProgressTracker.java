package com.mopub.mobileads;

import com.mopub.common.Preconditions;
import java.io.Serializable;
import java.util.Locale;

public class VastAbsoluteProgressTracker
  extends VastTracker
  implements Serializable, Comparable
{
  private static final long serialVersionUID;
  private final int a;
  
  public VastAbsoluteProgressTracker(VastTracker.a parama, String paramString, int paramInt)
  {
    super(parama, paramString);
    boolean bool;
    if (paramInt >= 0)
    {
      bool = true;
    }
    else
    {
      bool = false;
      parama = null;
    }
    Preconditions.checkArgument(bool);
    a = paramInt;
  }
  
  public VastAbsoluteProgressTracker(String paramString, int paramInt)
  {
    this(locala, paramString, paramInt);
  }
  
  public int compareTo(VastAbsoluteProgressTracker paramVastAbsoluteProgressTracker)
  {
    int i = paramVastAbsoluteProgressTracker.getTrackingMilliseconds();
    return getTrackingMilliseconds() - i;
  }
  
  public int getTrackingMilliseconds()
  {
    return a;
  }
  
  public String toString()
  {
    Locale localLocale = Locale.US;
    Object[] arrayOfObject = new Object[2];
    Object localObject = Integer.valueOf(a);
    arrayOfObject[0] = localObject;
    localObject = getContent();
    arrayOfObject[1] = localObject;
    return String.format(localLocale, "%dms: %s", arrayOfObject);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastAbsoluteProgressTracker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */