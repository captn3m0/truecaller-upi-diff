package com.mopub.mobileads;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.mopub.common.CacheService;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.AsyncTasks;

public class VastManager
  implements VastXmlManagerAggregator.b
{
  private VastManager.VastManagerListener a;
  private VastXmlManagerAggregator b;
  private String c;
  private double d;
  private int e;
  private final boolean f;
  
  public VastManager(Context paramContext, boolean paramBoolean)
  {
    Preconditions.checkNotNull(paramContext, "context cannot be null");
    Display localDisplay = ((WindowManager)paramContext.getSystemService("window")).getDefaultDisplay();
    int i = localDisplay.getWidth();
    int j = localDisplay.getHeight();
    paramContext = paramContext.getResources().getDisplayMetrics();
    float f1 = density;
    boolean bool = f1 < 0.0F;
    if (!bool)
    {
      m = 1065353216;
      f1 = 1.0F;
    }
    int k = Math.max(i, j);
    j = Math.min(i, j);
    double d1 = k;
    double d2 = j;
    Double.isNaN(d1);
    Double.isNaN(d2);
    d1 /= d2;
    d = d1;
    float f2 = k / f1;
    float f3 = j / f1;
    int m = (int)(f2 * f3);
    e = m;
    f = paramBoolean;
  }
  
  private static boolean b(VastVideoConfig paramVastVideoConfig)
  {
    Preconditions.checkNotNull(paramVastVideoConfig, "vastVideoConfig cannot be null");
    String str = paramVastVideoConfig.getNetworkMediaFileUrl();
    boolean bool = CacheService.containsKeyDiskCache(str);
    if (bool)
    {
      str = CacheService.getFilePathDiskCache(str);
      paramVastVideoConfig.setDiskMediaFileUrl(str);
      return true;
    }
    return false;
  }
  
  public void cancel()
  {
    VastXmlManagerAggregator localVastXmlManagerAggregator = b;
    if (localVastXmlManagerAggregator != null)
    {
      boolean bool = true;
      localVastXmlManagerAggregator.cancel(bool);
      localVastXmlManagerAggregator = null;
      b = null;
    }
  }
  
  public void onAggregationComplete(VastVideoConfig paramVastVideoConfig)
  {
    Object localObject = a;
    if (localObject != null)
    {
      if (paramVastVideoConfig == null)
      {
        ((VastManager.VastManagerListener)localObject).onVastVideoConfigurationPrepared(null);
        return;
      }
      localObject = c;
      boolean bool = TextUtils.isEmpty((CharSequence)localObject);
      if (!bool)
      {
        localObject = c;
        paramVastVideoConfig.setDspCreativeId((String)localObject);
      }
      bool = f;
      if (bool)
      {
        bool = b(paramVastVideoConfig);
        if (!bool)
        {
          localObject = new com/mopub/mobileads/VastManager$1;
          ((VastManager.1)localObject).<init>(this, paramVastVideoConfig);
          VideoDownloader.cache(paramVastVideoConfig.getNetworkMediaFileUrl(), (VideoDownloader.a)localObject);
          return;
        }
      }
      a.onVastVideoConfigurationPrepared(paramVastVideoConfig);
      return;
    }
    paramVastVideoConfig = new java/lang/IllegalStateException;
    paramVastVideoConfig.<init>("mVastManagerListener cannot be null here. Did you call prepareVastVideoConfiguration()?");
    throw paramVastVideoConfig;
  }
  
  public void prepareVastVideoConfiguration(String paramString1, VastManager.VastManagerListener paramVastManagerListener, String paramString2, Context paramContext)
  {
    Preconditions.checkNotNull(paramVastManagerListener, "vastManagerListener cannot be null");
    Preconditions.checkNotNull(paramContext, "context cannot be null");
    VastXmlManagerAggregator localVastXmlManagerAggregator = b;
    if (localVastXmlManagerAggregator == null)
    {
      a = paramVastManagerListener;
      paramVastManagerListener = new com/mopub/mobileads/VastXmlManagerAggregator;
      double d1 = d;
      int i = e;
      Context localContext = paramContext.getApplicationContext();
      paramVastManagerListener.<init>(this, d1, i, localContext);
      b = paramVastManagerListener;
      c = paramString2;
      try
      {
        paramVastManagerListener = b;
        int j = 1;
        paramString2 = new String[j];
        paramContext = null;
        paramString2[0] = paramString1;
        AsyncTasks.safeExecuteOnExecutor(paramVastManagerListener, paramString2);
        return;
      }
      catch (Exception paramString1)
      {
        MoPubLog.d("Failed to aggregate vast xml", paramString1);
        paramString1 = a;
        paramVastManagerListener = null;
        paramString1.onVastVideoConfigurationPrepared(null);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */