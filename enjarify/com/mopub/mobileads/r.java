package com.mopub.mobileads;

 enum r
{
  private final String a;
  
  static
  {
    Object localObject = new com/mopub/mobileads/r;
    ((r)localObject).<init>("START", 0, "start");
    START = (r)localObject;
    localObject = new com/mopub/mobileads/r;
    int i = 1;
    ((r)localObject).<init>("FIRST_QUARTILE", i, "firstQuartile");
    FIRST_QUARTILE = (r)localObject;
    localObject = new com/mopub/mobileads/r;
    int j = 2;
    ((r)localObject).<init>("MIDPOINT", j, "midpoint");
    MIDPOINT = (r)localObject;
    localObject = new com/mopub/mobileads/r;
    int k = 3;
    ((r)localObject).<init>("THIRD_QUARTILE", k, "thirdQuartile");
    THIRD_QUARTILE = (r)localObject;
    localObject = new com/mopub/mobileads/r;
    int m = 4;
    ((r)localObject).<init>("COMPLETE", m, "complete");
    COMPLETE = (r)localObject;
    localObject = new com/mopub/mobileads/r;
    int n = 5;
    ((r)localObject).<init>("COMPANION_AD_VIEW", n, "companionAdView");
    COMPANION_AD_VIEW = (r)localObject;
    localObject = new com/mopub/mobileads/r;
    int i1 = 6;
    ((r)localObject).<init>("COMPANION_AD_CLICK", i1, "companionAdClick");
    COMPANION_AD_CLICK = (r)localObject;
    localObject = new com/mopub/mobileads/r;
    int i2 = 7;
    ((r)localObject).<init>("UNKNOWN", i2, "");
    UNKNOWN = (r)localObject;
    localObject = new r[8];
    r localr = START;
    localObject[0] = localr;
    localr = FIRST_QUARTILE;
    localObject[i] = localr;
    localr = MIDPOINT;
    localObject[j] = localr;
    localr = THIRD_QUARTILE;
    localObject[k] = localr;
    localr = COMPLETE;
    localObject[m] = localr;
    localr = COMPANION_AD_VIEW;
    localObject[n] = localr;
    localr = COMPANION_AD_CLICK;
    localObject[i1] = localr;
    localr = UNKNOWN;
    localObject[i2] = localr;
    b = (r[])localObject;
  }
  
  private r(String paramString1)
  {
    a = paramString1;
  }
  
  public static r fromString(String paramString)
  {
    if (paramString == null) {
      return UNKNOWN;
    }
    r[] arrayOfr = values();
    int i = arrayOfr.length;
    int j = 0;
    while (j < i)
    {
      r localr = arrayOfr[j];
      String str = localr.getName();
      boolean bool = paramString.equals(str);
      if (bool) {
        return localr;
      }
      j += 1;
    }
    return UNKNOWN;
  }
  
  public final String getName()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */