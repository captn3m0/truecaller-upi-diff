package com.mopub.mobileads;

public abstract interface MoPubError
{
  public static final int ER_ADAPTER_NOT_FOUND = 1;
  public static final int ER_INVALID_DATA = 3;
  public static final int ER_SUCCESS = 0;
  public static final int ER_TIMEOUT = 2;
  public static final int ER_UNSPECIFIED = 10000;
  
  public abstract int getIntCode();
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.MoPubError
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */