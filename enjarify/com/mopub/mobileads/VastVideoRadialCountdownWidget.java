package com.mopub.mobileads;

import android.content.Context;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Utils;
import com.mopub.mobileads.resource.RadialCountdownDrawable;

public class VastVideoRadialCountdownWidget
  extends ImageView
{
  private RadialCountdownDrawable a;
  private int b;
  
  public VastVideoRadialCountdownWidget(Context paramContext)
  {
    super(paramContext);
    int i = (int)Utils.generateUniqueId();
    setId(i);
    int j = Dips.dipsToIntPixels(45.0F, paramContext);
    float f = 16.0F;
    int k = Dips.dipsToIntPixels(f, paramContext);
    i = Dips.dipsToIntPixels(f, paramContext);
    int m = Dips.dipsToIntPixels(5.0F, paramContext);
    RadialCountdownDrawable localRadialCountdownDrawable = new com/mopub/mobileads/resource/RadialCountdownDrawable;
    localRadialCountdownDrawable.<init>(paramContext);
    a = localRadialCountdownDrawable;
    paramContext = a;
    setImageDrawable(paramContext);
    setPadding(m, m, m, m);
    paramContext = new android/widget/RelativeLayout$LayoutParams;
    paramContext.<init>(j, j);
    paramContext.setMargins(0, k, i, 0);
    paramContext.addRule(11);
    setLayoutParams(paramContext);
  }
  
  public void calibrateAndMakeVisible(int paramInt)
  {
    a.setInitialCountdown(paramInt);
    setVisibility(0);
  }
  
  public RadialCountdownDrawable getImageViewDrawable()
  {
    return a;
  }
  
  public void setImageViewDrawable(RadialCountdownDrawable paramRadialCountdownDrawable)
  {
    a = paramRadialCountdownDrawable;
  }
  
  public void updateCountdownProgress(int paramInt1, int paramInt2)
  {
    int i = b;
    if (paramInt2 >= i)
    {
      paramInt1 -= paramInt2;
      if (paramInt1 < 0)
      {
        setVisibility(8);
        return;
      }
      RadialCountdownDrawable localRadialCountdownDrawable = a;
      localRadialCountdownDrawable.updateCountdownProgress(paramInt2);
      b = paramInt2;
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastVideoRadialCountdownWidget
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */