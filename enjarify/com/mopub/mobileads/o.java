package com.mopub.mobileads;

import android.content.Context;
import android.webkit.WebSettings;
import com.mopub.common.Preconditions;
import com.mopub.common.util.Utils;
import com.mopub.network.Networking;

final class o
  extends BaseWebView
{
  o.a b;
  
  private o(Context paramContext)
  {
    super(paramContext);
    setHorizontalScrollBarEnabled(false);
    setHorizontalScrollbarOverlay(false);
    setVerticalScrollBarEnabled(false);
    setVerticalScrollbarOverlay(false);
    getSettings().setSupportZoom(false);
    setScrollBarStyle(0);
    WebSettings localWebSettings = getSettings();
    boolean bool = true;
    localWebSettings.setJavaScriptEnabled(bool);
    enablePlugins(bool);
    setBackgroundColor(0);
    paramContext = new com/mopub/mobileads/o$b;
    paramContext.<init>(this);
    setOnTouchListener(paramContext);
    int i = (int)Utils.generateUniqueId();
    setId(i);
  }
  
  static o a(Context paramContext, n paramn)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramn);
    o localo = new com/mopub/mobileads/o;
    localo.<init>(paramContext);
    paramn.initializeWebView(localo);
    return localo;
  }
  
  final void a(String paramString)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str1 = Networking.getBaseUrlScheme();
    localStringBuilder.append(str1);
    localStringBuilder.append("://ads.mopub.com/");
    String str2 = localStringBuilder.toString();
    loadDataWithBaseURL(str2, paramString, "text/html", "utf-8", null);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */