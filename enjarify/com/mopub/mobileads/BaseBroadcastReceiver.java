package com.mopub.mobileads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.d;
import com.mopub.common.Preconditions;

public abstract class BaseBroadcastReceiver
  extends BroadcastReceiver
{
  private final long a;
  private Context b;
  
  public BaseBroadcastReceiver(long paramLong)
  {
    a = paramLong;
  }
  
  public static void broadcastAction(Context paramContext, long paramLong, String paramString)
  {
    Preconditions.checkNotNull(paramContext, "context cannot be null");
    Preconditions.checkNotNull(paramString, "action cannot be null");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramString);
    localIntent.putExtra("broadcastIdentifier", paramLong);
    d.a(paramContext.getApplicationContext()).a(localIntent);
  }
  
  public abstract IntentFilter getIntentFilter();
  
  public void register(BroadcastReceiver paramBroadcastReceiver, Context paramContext)
  {
    b = paramContext;
    paramContext = d.a(b);
    IntentFilter localIntentFilter = getIntentFilter();
    paramContext.a(paramBroadcastReceiver, localIntentFilter);
  }
  
  public boolean shouldConsumeBroadcast(Intent paramIntent)
  {
    Preconditions.checkNotNull(paramIntent, "intent cannot be null");
    String str = "broadcastIdentifier";
    long l1 = -1;
    long l2 = paramIntent.getLongExtra(str, l1);
    long l3 = a;
    boolean bool = l3 < l2;
    return !bool;
  }
  
  public void unregister(BroadcastReceiver paramBroadcastReceiver)
  {
    Object localObject = b;
    if ((localObject != null) && (paramBroadcastReceiver != null))
    {
      localObject = d.a((Context)localObject);
      ((d)localObject).a(paramBroadcastReceiver);
      paramBroadcastReceiver = null;
      b = null;
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.BaseBroadcastReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */