package com.mopub.mobileads;

import android.widget.ImageView;
import com.mopub.common.logging.MoPubLog;
import com.mopub.volley.VolleyError;
import com.mopub.volley.toolbox.ImageLoader.ImageContainer;
import com.mopub.volley.toolbox.ImageLoader.ImageListener;

final class VastVideoCloseButtonWidget$1
  implements ImageLoader.ImageListener
{
  VastVideoCloseButtonWidget$1(VastVideoCloseButtonWidget paramVastVideoCloseButtonWidget, String paramString) {}
  
  public final void onErrorResponse(VolleyError paramVolleyError)
  {
    MoPubLog.d("Failed to load image.", paramVolleyError);
  }
  
  public final void onResponse(ImageLoader.ImageContainer paramImageContainer, boolean paramBoolean)
  {
    paramImageContainer = paramImageContainer.getBitmap();
    if (paramImageContainer != null)
    {
      VastVideoCloseButtonWidget.a(b).setImageBitmap(paramImageContainer);
      return;
    }
    Object[] arrayOfObject = new Object[1];
    String str = a;
    arrayOfObject[0] = str;
    MoPubLog.d(String.format("%s returned null bitmap", arrayOfObject));
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastVideoCloseButtonWidget.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */