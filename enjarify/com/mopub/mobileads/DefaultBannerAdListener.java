package com.mopub.mobileads;

public class DefaultBannerAdListener
  implements MoPubView.BannerAdListener
{
  public void onBannerClicked(MoPubView paramMoPubView) {}
  
  public void onBannerCollapsed(MoPubView paramMoPubView) {}
  
  public void onBannerExpanded(MoPubView paramMoPubView) {}
  
  public void onBannerFailed(MoPubView paramMoPubView, MoPubErrorCode paramMoPubErrorCode) {}
  
  public void onBannerLoaded(MoPubView paramMoPubView) {}
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.DefaultBannerAdListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */