package com.mopub.mobileads;

import com.mopub.common.logging.MoPubLog;

final class CustomEventBannerAdapter$1
  implements Runnable
{
  CustomEventBannerAdapter$1(CustomEventBannerAdapter paramCustomEventBannerAdapter) {}
  
  public final void run()
  {
    MoPubLog.d("Third-party network timed out.");
    CustomEventBannerAdapter localCustomEventBannerAdapter = a;
    MoPubErrorCode localMoPubErrorCode = MoPubErrorCode.NETWORK_TIMEOUT;
    localCustomEventBannerAdapter.onBannerFailed(localMoPubErrorCode);
    a.invalidate();
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.CustomEventBannerAdapter.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */