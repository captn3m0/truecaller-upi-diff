package com.mopub.mobileads;

import android.os.Handler;
import com.mopub.common.ExternalViewabilitySessionManager;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class WebViewCacheService
{
  static final WebViewCacheService.a a;
  private static final Map b;
  private static Handler c;
  
  static
  {
    Object localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    b = Collections.synchronizedMap((Map)localObject);
    localObject = new com/mopub/mobileads/WebViewCacheService$a;
    ((WebViewCacheService.a)localObject).<init>((byte)0);
    a = (WebViewCacheService.a)localObject;
    localObject = new android/os/Handler;
    ((Handler)localObject).<init>();
    c = (Handler)localObject;
  }
  
  static void a()
  {
    synchronized (WebViewCacheService.class)
    {
      Object localObject1 = b;
      localObject1 = ((Map)localObject1).entrySet();
      localObject1 = ((Set)localObject1).iterator();
      Object localObject3;
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        localObject3 = ((Iterator)localObject1).next();
        localObject3 = (Map.Entry)localObject3;
        Object localObject4 = ((Map.Entry)localObject3).getValue();
        localObject4 = (WebViewCacheService.Config)localObject4;
        localObject4 = ((WebViewCacheService.Config)localObject4).getWeakInterstitial();
        localObject4 = ((WeakReference)localObject4).get();
        if (localObject4 == null)
        {
          localObject3 = ((Map.Entry)localObject3).getValue();
          localObject3 = (WebViewCacheService.Config)localObject3;
          localObject3 = ((WebViewCacheService.Config)localObject3).getViewabilityManager();
          ((ExternalViewabilitySessionManager)localObject3).endDisplaySession();
          ((Iterator)localObject1).remove();
        }
      }
      localObject1 = b;
      boolean bool2 = ((Map)localObject1).isEmpty();
      if (!bool2)
      {
        localObject1 = c;
        localObject3 = a;
        ((Handler)localObject1).removeCallbacks((Runnable)localObject3);
        localObject1 = c;
        localObject3 = a;
        long l = 900000L;
        ((Handler)localObject1).postDelayed((Runnable)localObject3, l);
      }
      return;
    }
  }
  
  public static void clearAll()
  {
    b.clear();
    Handler localHandler = c;
    WebViewCacheService.a locala = a;
    localHandler.removeCallbacks(locala);
  }
  
  public static WebViewCacheService.Config popWebViewConfig(Long paramLong)
  {
    Preconditions.checkNotNull(paramLong);
    return (WebViewCacheService.Config)b.remove(paramLong);
  }
  
  public static void storeWebViewConfig(Long paramLong, Interstitial paramInterstitial, BaseWebView paramBaseWebView, ExternalViewabilitySessionManager paramExternalViewabilitySessionManager)
  {
    Preconditions.checkNotNull(paramLong);
    Preconditions.checkNotNull(paramInterstitial);
    Preconditions.checkNotNull(paramBaseWebView);
    a();
    Map localMap = b;
    int i = localMap.size();
    int j = 50;
    if (i >= j)
    {
      MoPubLog.w("Unable to cache web view. Please destroy some via MoPubInterstitial#destroy() and try again.");
      return;
    }
    localMap = b;
    WebViewCacheService.Config localConfig = new com/mopub/mobileads/WebViewCacheService$Config;
    localConfig.<init>(paramBaseWebView, paramInterstitial, paramExternalViewabilitySessionManager);
    localMap.put(paramLong, localConfig);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.WebViewCacheService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */