package com.mopub.mobileads;

import com.mopub.common.Preconditions;

public enum VastErrorCode
{
  final String a;
  
  static
  {
    Object localObject = new com/mopub/mobileads/VastErrorCode;
    ((VastErrorCode)localObject).<init>("XML_PARSING_ERROR", 0, "100");
    XML_PARSING_ERROR = (VastErrorCode)localObject;
    localObject = new com/mopub/mobileads/VastErrorCode;
    int i = 1;
    ((VastErrorCode)localObject).<init>("WRAPPER_TIMEOUT", i, "301");
    WRAPPER_TIMEOUT = (VastErrorCode)localObject;
    localObject = new com/mopub/mobileads/VastErrorCode;
    int j = 2;
    ((VastErrorCode)localObject).<init>("NO_ADS_VAST_RESPONSE", j, "303");
    NO_ADS_VAST_RESPONSE = (VastErrorCode)localObject;
    localObject = new com/mopub/mobileads/VastErrorCode;
    int k = 3;
    ((VastErrorCode)localObject).<init>("GENERAL_LINEAR_AD_ERROR", k, "400");
    GENERAL_LINEAR_AD_ERROR = (VastErrorCode)localObject;
    localObject = new com/mopub/mobileads/VastErrorCode;
    int m = 4;
    ((VastErrorCode)localObject).<init>("GENERAL_COMPANION_AD_ERROR", m, "600");
    GENERAL_COMPANION_AD_ERROR = (VastErrorCode)localObject;
    localObject = new com/mopub/mobileads/VastErrorCode;
    int n = 5;
    ((VastErrorCode)localObject).<init>("UNDEFINED_ERROR", n, "900");
    UNDEFINED_ERROR = (VastErrorCode)localObject;
    localObject = new VastErrorCode[6];
    VastErrorCode localVastErrorCode = XML_PARSING_ERROR;
    localObject[0] = localVastErrorCode;
    localVastErrorCode = WRAPPER_TIMEOUT;
    localObject[i] = localVastErrorCode;
    localVastErrorCode = NO_ADS_VAST_RESPONSE;
    localObject[j] = localVastErrorCode;
    localVastErrorCode = GENERAL_LINEAR_AD_ERROR;
    localObject[k] = localVastErrorCode;
    localVastErrorCode = GENERAL_COMPANION_AD_ERROR;
    localObject[m] = localVastErrorCode;
    localVastErrorCode = UNDEFINED_ERROR;
    localObject[n] = localVastErrorCode;
    b = (VastErrorCode[])localObject;
  }
  
  private VastErrorCode(String paramString1)
  {
    Preconditions.checkNotNull(paramString1, "errorCode cannot be null");
    a = paramString1;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastErrorCode
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */