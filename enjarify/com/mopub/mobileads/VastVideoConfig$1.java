package com.mopub.mobileads;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.os.Bundle;
import com.mopub.common.MoPubBrowser;
import com.mopub.common.Preconditions;
import com.mopub.common.UrlAction;
import com.mopub.common.UrlHandler.ResultActions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Intents;
import com.mopub.exceptions.IntentNotResolvableException;

final class VastVideoConfig$1
  implements UrlHandler.ResultActions
{
  VastVideoConfig$1(VastVideoConfig paramVastVideoConfig, Context paramContext, Integer paramInteger) {}
  
  public final void urlHandlingFailed(String paramString, UrlAction paramUrlAction) {}
  
  public final void urlHandlingSucceeded(String paramString, UrlAction paramUrlAction)
  {
    Object localObject = UrlAction.OPEN_IN_APP_BROWSER;
    if (paramUrlAction == localObject)
    {
      paramUrlAction = new android/os/Bundle;
      paramUrlAction.<init>();
      paramUrlAction.putString("URL", paramString);
      localObject = VastVideoConfig.a(c);
      paramUrlAction.putString("mopub-dsp-creative-id", (String)localObject);
      paramString = MoPubBrowser.class;
      localObject = a;
      paramUrlAction = Intents.getStartActivityIntent((Context)localObject, paramString, paramUrlAction);
      try
      {
        localObject = a;
        boolean bool = localObject instanceof Activity;
        if (bool)
        {
          localObject = b;
          Preconditions.checkNotNull(localObject);
          localObject = a;
          localObject = (Activity)localObject;
          Integer localInteger = b;
          int i = localInteger.intValue();
          ((Activity)localObject).startActivityForResult(paramUrlAction, i);
          return;
        }
        localObject = a;
        Intents.startActivity((Context)localObject, paramUrlAction);
        return;
      }
      catch (IntentNotResolvableException localIntentNotResolvableException)
      {
        paramUrlAction = new java/lang/StringBuilder;
        localObject = "Activity ";
        paramUrlAction.<init>((String)localObject);
        paramString = paramString.getName();
        paramUrlAction.append(paramString);
        paramUrlAction.append(" not found. Did you declare it in your AndroidManifest.xml?");
        paramString = paramUrlAction.toString();
        MoPubLog.d(paramString);
      }
      catch (ActivityNotFoundException localActivityNotFoundException)
      {
        paramUrlAction = new java/lang/StringBuilder;
        paramUrlAction.<init>("Activity ");
        paramString = paramString.getName();
        paramUrlAction.append(paramString);
        paramUrlAction.append(" not found. Did you declare it in your AndroidManifest.xml?");
        MoPubLog.d(paramUrlAction.toString());
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastVideoConfig.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */