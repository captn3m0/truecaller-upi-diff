package com.mopub.mobileads;

import com.mopub.common.Preconditions;
import java.io.Serializable;

public class VastTracker
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private final VastTracker.a a;
  private final String b;
  private boolean c;
  private boolean d;
  
  public VastTracker(VastTracker.a parama, String paramString)
  {
    Preconditions.checkNotNull(parama);
    Preconditions.checkNotNull(paramString);
    a = parama;
    b = paramString;
  }
  
  public VastTracker(String paramString)
  {
    this(locala, paramString);
  }
  
  public VastTracker(String paramString, boolean paramBoolean)
  {
    this(paramString);
    d = paramBoolean;
  }
  
  public String getContent()
  {
    return b;
  }
  
  public VastTracker.a getMessageType()
  {
    return a;
  }
  
  public boolean isRepeatable()
  {
    return d;
  }
  
  public boolean isTracked()
  {
    return c;
  }
  
  public void setTracked()
  {
    c = true;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastTracker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */