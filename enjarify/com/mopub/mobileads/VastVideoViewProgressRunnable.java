package com.mopub.mobileads;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import com.mopub.common.ExternalViewabilitySession.VideoEvent;
import com.mopub.common.ExternalViewabilitySessionManager;
import com.mopub.common.Preconditions;
import com.mopub.network.TrackingRequest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class VastVideoViewProgressRunnable
  extends RepeatingHandlerRunnable
{
  private final VastVideoViewController c;
  private final VastVideoConfig d;
  
  public VastVideoViewProgressRunnable(VastVideoViewController paramVastVideoViewController, VastVideoConfig paramVastVideoConfig, Handler paramHandler)
  {
    super(paramHandler);
    Preconditions.checkNotNull(paramVastVideoViewController);
    Preconditions.checkNotNull(paramVastVideoConfig);
    c = paramVastVideoViewController;
    d = paramVastVideoConfig;
    paramVastVideoViewController = new java/util/ArrayList;
    paramVastVideoViewController.<init>();
    paramVastVideoConfig = new com/mopub/mobileads/VastFractionalProgressTracker;
    paramHandler = VastTracker.a.QUARTILE_EVENT;
    String str = ExternalViewabilitySession.VideoEvent.AD_STARTED.name();
    paramVastVideoConfig.<init>(paramHandler, str, 0.0F);
    paramVastVideoViewController.add(paramVastVideoConfig);
    paramVastVideoConfig = new com/mopub/mobileads/VastFractionalProgressTracker;
    paramHandler = VastTracker.a.QUARTILE_EVENT;
    str = ExternalViewabilitySession.VideoEvent.AD_IMPRESSED.name();
    paramVastVideoConfig.<init>(paramHandler, str, 0.0F);
    paramVastVideoViewController.add(paramVastVideoConfig);
    paramVastVideoConfig = new com/mopub/mobileads/VastFractionalProgressTracker;
    paramHandler = VastTracker.a.QUARTILE_EVENT;
    str = ExternalViewabilitySession.VideoEvent.AD_VIDEO_FIRST_QUARTILE.name();
    paramVastVideoConfig.<init>(paramHandler, str, 0.25F);
    paramVastVideoViewController.add(paramVastVideoConfig);
    paramVastVideoConfig = new com/mopub/mobileads/VastFractionalProgressTracker;
    paramHandler = VastTracker.a.QUARTILE_EVENT;
    str = ExternalViewabilitySession.VideoEvent.AD_VIDEO_MIDPOINT.name();
    paramVastVideoConfig.<init>(paramHandler, str, 0.5F);
    paramVastVideoViewController.add(paramVastVideoConfig);
    paramVastVideoConfig = new com/mopub/mobileads/VastFractionalProgressTracker;
    paramHandler = VastTracker.a.QUARTILE_EVENT;
    str = ExternalViewabilitySession.VideoEvent.AD_VIDEO_THIRD_QUARTILE.name();
    paramVastVideoConfig.<init>(paramHandler, str, 0.75F);
    paramVastVideoViewController.add(paramVastVideoConfig);
    d.addFractionalTrackers(paramVastVideoViewController);
  }
  
  public void doWork()
  {
    Object localObject1 = c.a;
    int i = ((VastVideoView)localObject1).getDuration();
    VastVideoView localVastVideoView = c.a;
    int j = localVastVideoView.getCurrentPosition();
    Object localObject2 = c;
    Object localObject3 = c;
    localObject2 = a;
    int k = ((VastVideoView)localObject2).getCurrentPosition();
    ((VastVideoProgressBarWidget)localObject3).updateProgress(k);
    if (i > 0)
    {
      localObject2 = d;
      localObject1 = ((VastVideoConfig)localObject2).getUntriggeredTrackersBefore(j, i);
      boolean bool1 = ((List)localObject1).isEmpty();
      boolean bool2;
      Object localObject4;
      Object localObject5;
      Object localObject6;
      if (!bool1)
      {
        localObject2 = new java/util/ArrayList;
        ((ArrayList)localObject2).<init>();
        localObject1 = ((List)localObject1).iterator();
        for (;;)
        {
          bool2 = ((Iterator)localObject1).hasNext();
          if (!bool2) {
            break;
          }
          localObject3 = (VastTracker)((Iterator)localObject1).next();
          localObject4 = ((VastTracker)localObject3).getMessageType();
          localObject5 = VastTracker.a.TRACKING_URL;
          if (localObject4 == localObject5)
          {
            localObject4 = ((VastTracker)localObject3).getContent();
            ((List)localObject2).add(localObject4);
          }
          else
          {
            localObject4 = ((VastTracker)localObject3).getMessageType();
            localObject5 = VastTracker.a.QUARTILE_EVENT;
            if (localObject4 == localObject5)
            {
              localObject4 = c;
              localObject5 = ((VastTracker)localObject3).getContent();
              localObject5 = (ExternalViewabilitySession.VideoEvent)Enum.valueOf(ExternalViewabilitySession.VideoEvent.class, (String)localObject5);
              localObject6 = b;
              localObject4 = a;
              int i1 = ((VastVideoView)localObject4).getCurrentPosition();
              ((ExternalViewabilitySessionManager)localObject6).recordVideoEvent((ExternalViewabilitySession.VideoEvent)localObject5, i1);
            }
          }
          ((VastTracker)localObject3).setTracked();
        }
        localObject1 = new com/mopub/mobileads/VastMacroHelper;
        ((VastMacroHelper)localObject1).<init>((List)localObject2);
        localObject2 = c.b();
        localObject1 = ((VastMacroHelper)localObject1).withAssetUri((String)localObject2);
        localObject2 = Integer.valueOf(j);
        localObject1 = ((VastMacroHelper)localObject1).withContentPlayHead((Integer)localObject2).getUris();
        localObject2 = c.mContext;
        TrackingRequest.makeTrackingHttpRequest((Iterable)localObject1, (Context)localObject2);
      }
      localObject1 = c;
      localObject2 = e;
      if (localObject2 != null)
      {
        localObject2 = e;
        int m = c;
        if (j >= m)
        {
          localObject2 = g;
          bool2 = false;
          ((View)localObject2).setVisibility(0);
          localObject2 = e;
          localObject3 = mContext;
          localObject4 = ((VastVideoViewController)localObject1).b();
          Preconditions.checkNotNull(localObject3);
          Preconditions.checkNotNull(localObject4);
          localObject2 = g;
          localObject5 = null;
          localObject6 = Integer.valueOf(j);
          TrackingRequest.makeVastTrackingHttpRequest((List)localObject2, null, (Integer)localObject6, (String)localObject4, (Context)localObject3);
          localObject2 = e.d;
          if (localObject2 == null) {
            return;
          }
          localObject2 = e;
          m = c;
          localObject3 = e.d;
          int n = ((Integer)localObject3).intValue();
          m += n;
          if (j < m) {
            return;
          }
          localObject1 = g;
          j = 8;
          ((View)localObject1).setVisibility(j);
          return;
        }
      }
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastVideoViewProgressRunnable
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */