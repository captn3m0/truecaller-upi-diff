package com.mopub.mobileads;

import android.content.Context;
import com.mopub.common.BaseUrlGenerator;
import com.mopub.common.ClientMetadata;

final class c
  extends BaseUrlGenerator
{
  private Context a;
  private String b;
  private String c;
  private String d;
  private Boolean e;
  private boolean g;
  private boolean h;
  
  c(Context paramContext)
  {
    a = paramContext;
  }
  
  public final String generateUrlString(String paramString)
  {
    Object localObject = ClientMetadata.getInstance(a);
    String str = "/m/open";
    a(paramString, str);
    b("6");
    paramString = ((ClientMetadata)localObject).getAppVersion();
    c(paramString);
    b();
    paramString = "id";
    localObject = a.getPackageName();
    b(paramString, (String)localObject);
    boolean bool = h;
    if (bool)
    {
      paramString = "st";
      localObject = Boolean.TRUE;
      a(paramString, (Boolean)localObject);
    }
    b("nv", "5.4.1");
    localObject = b;
    b("current_consent_status", (String)localObject);
    localObject = c;
    b("consented_vendor_list_version", (String)localObject);
    localObject = d;
    b("consented_privacy_policy_version", (String)localObject);
    localObject = e;
    a("gdpr_applies", (Boolean)localObject);
    localObject = Boolean.valueOf(g);
    a("force_gdpr_applies", (Boolean)localObject);
    return f.toString();
  }
  
  public final c withConsentedPrivacyPolicyVersion(String paramString)
  {
    d = paramString;
    return this;
  }
  
  public final c withConsentedVendorListVersion(String paramString)
  {
    c = paramString;
    return this;
  }
  
  public final c withCurrentConsentStatus(String paramString)
  {
    b = paramString;
    return this;
  }
  
  public final c withForceGdprApplies(boolean paramBoolean)
  {
    g = paramBoolean;
    return this;
  }
  
  public final c withGdprApplies(Boolean paramBoolean)
  {
    e = paramBoolean;
    return this;
  }
  
  public final c withSessionTracker(boolean paramBoolean)
  {
    h = paramBoolean;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */