package com.mopub.mobileads;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import com.mopub.common.ExternalViewabilitySession.VideoEvent;
import com.mopub.common.ExternalViewabilitySessionManager;

final class VastVideoViewController$7
  implements MediaPlayer.OnErrorListener
{
  VastVideoViewController$7(VastVideoViewController paramVastVideoViewController) {}
  
  public final boolean onError(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
  {
    paramMediaPlayer = VastVideoViewController.b(a);
    ExternalViewabilitySession.VideoEvent localVideoEvent = ExternalViewabilitySession.VideoEvent.RECORD_AD_ERROR;
    paramInt2 = a.a.getCurrentPosition();
    paramMediaPlayer.recordVideoEvent(localVideoEvent, paramInt2);
    VastVideoViewController.q(a);
    a.a();
    a.videoError(false);
    VastVideoViewController.y(a);
    paramMediaPlayer = VastVideoViewController.f(a);
    Context localContext = a.mContext;
    VastErrorCode localVastErrorCode = VastErrorCode.GENERAL_LINEAR_AD_ERROR;
    int i = a.a.getCurrentPosition();
    paramMediaPlayer.handleError(localContext, localVastErrorCode, i);
    return false;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastVideoViewController.7
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */