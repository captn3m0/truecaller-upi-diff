package com.mopub.mobileads;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Views;
import java.lang.ref.WeakReference;

final class b
{
  final ViewTreeObserver.OnPreDrawListener a;
  WeakReference b;
  final View c;
  final View d;
  final b.a e;
  b.c f;
  final Handler g;
  boolean h;
  boolean i;
  private final b.b j;
  
  public b(Context paramContext, View paramView1, View paramView2, int paramInt1, int paramInt2)
  {
    Preconditions.checkNotNull(paramView1);
    Preconditions.checkNotNull(paramView2);
    d = paramView1;
    c = paramView2;
    paramView1 = new com/mopub/mobileads/b$a;
    paramView1.<init>(paramInt1, paramInt2);
    e = paramView1;
    paramView1 = new android/os/Handler;
    paramView1.<init>();
    g = paramView1;
    paramView1 = new com/mopub/mobileads/b$b;
    paramView1.<init>(this);
    j = paramView1;
    paramView1 = new com/mopub/mobileads/b$1;
    paramView1.<init>(this);
    a = paramView1;
    paramView1 = new java/lang/ref/WeakReference;
    boolean bool1 = false;
    paramView1.<init>(null);
    b = paramView1;
    paramView1 = c;
    paramView2 = (ViewTreeObserver)b.get();
    if (paramView2 != null)
    {
      bool1 = paramView2.isAlive();
      if (bool1) {}
    }
    else
    {
      paramContext = Views.getTopmostView(paramContext, paramView1);
      if (paramContext == null)
      {
        MoPubLog.d("Unable to set Visibility Tracker due to no available root view.");
        return;
      }
      paramContext = paramContext.getViewTreeObserver();
      boolean bool2 = paramContext.isAlive();
      if (!bool2)
      {
        MoPubLog.w("Visibility Tracker was unable to track views because the root view tree observer was not alive");
        return;
      }
      paramView1 = new java/lang/ref/WeakReference;
      paramView1.<init>(paramContext);
      b = paramView1;
      paramView1 = a;
      paramContext.addOnPreDrawListener(paramView1);
    }
  }
  
  final void a()
  {
    boolean bool = h;
    if (bool) {
      return;
    }
    h = true;
    Handler localHandler = g;
    b.b localb = j;
    localHandler.postDelayed(localb, 100);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */