package com.mopub.mobileads;

import com.mopub.common.Preconditions;
import com.mopub.mobileads.util.XmlUtils;
import org.w3c.dom.Node;

public class VastResourceXmlManager
{
  public static final String CREATIVE_TYPE = "creativeType";
  public static final String HTML_RESOURCE = "HTMLResource";
  public static final String IFRAME_RESOURCE = "IFrameResource";
  public static final String STATIC_RESOURCE = "StaticResource";
  final Node a;
  
  VastResourceXmlManager(Node paramNode)
  {
    Preconditions.checkNotNull(paramNode);
    a = paramNode;
  }
  
  final String a()
  {
    return XmlUtils.getNodeValue(XmlUtils.getFirstMatchingChildNode(a, "StaticResource"));
  }
  
  final String b()
  {
    return XmlUtils.getNodeValue(XmlUtils.getFirstMatchingChildNode(a, "IFrameResource"));
  }
  
  final String c()
  {
    return XmlUtils.getNodeValue(XmlUtils.getFirstMatchingChildNode(a, "HTMLResource"));
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastResourceXmlManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */