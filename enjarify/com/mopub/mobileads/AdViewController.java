package com.mopub.mobileads;

import android.content.Context;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout.LayoutParams;
import com.mopub.common.AdFormat;
import com.mopub.common.AdReport;
import com.mopub.common.AdUrlGenerator;
import com.mopub.common.ClientMetadata;
import com.mopub.common.MoPub;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.DeviceUtils;
import com.mopub.common.util.Utils;
import com.mopub.mraid.MraidNativeCommandHandler;
import com.mopub.network.AdLoader;
import com.mopub.network.AdLoader.Listener;
import com.mopub.network.AdResponse;
import com.mopub.network.MoPubNetworkError;
import com.mopub.network.MoPubNetworkError.Reason;
import com.mopub.volley.NetworkResponse;
import com.mopub.volley.Request;
import com.mopub.volley.VolleyError;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

public class AdViewController
{
  private static final FrameLayout.LayoutParams l;
  private static final WeakHashMap m;
  Context a;
  MoPubView b;
  WebViewAdUrlGenerator c;
  AdLoader d;
  AdResponse e;
  boolean f;
  Handler g;
  boolean h;
  int i;
  Map j;
  boolean k;
  private final long n;
  private Request o;
  private final AdLoader.Listener p;
  private String q;
  private final Runnable r;
  private boolean s;
  private String t;
  private String u;
  private Location v;
  private boolean w;
  private boolean x;
  private String y;
  private Integer z;
  
  static
  {
    Object localObject = new android/widget/FrameLayout$LayoutParams;
    int i1 = -2;
    ((FrameLayout.LayoutParams)localObject).<init>(i1, i1, 17);
    l = (FrameLayout.LayoutParams)localObject;
    localObject = new java/util/WeakHashMap;
    ((WeakHashMap)localObject).<init>();
    m = (WeakHashMap)localObject;
  }
  
  public AdViewController(Context paramContext, MoPubView paramMoPubView)
  {
    int i1 = 1;
    i = i1;
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    j = localHashMap;
    s = i1;
    k = i1;
    a = paramContext;
    b = paramMoPubView;
    long l1 = Utils.generateUniqueId();
    n = l1;
    paramContext = new com/mopub/mobileads/WebViewAdUrlGenerator;
    paramMoPubView = a.getApplicationContext();
    i1 = MraidNativeCommandHandler.isStorePictureSupported(a);
    paramContext.<init>(paramMoPubView, i1);
    c = paramContext;
    paramContext = new com/mopub/mobileads/AdViewController$1;
    paramContext.<init>(this);
    p = paramContext;
    paramContext = new com/mopub/mobileads/AdViewController$2;
    paramContext.<init>(this);
    r = paramContext;
    paramContext = Integer.valueOf(60000);
    z = paramContext;
    paramContext = new android/os/Handler;
    paramContext.<init>();
    g = paramContext;
  }
  
  private void a(String paramString, MoPubError paramMoPubError)
  {
    if (paramString == null)
    {
      paramString = MoPubErrorCode.NO_FILL;
      b(paramString);
      return;
    }
    Object localObject = "javascript:";
    boolean bool1 = paramString.startsWith((String)localObject);
    if (!bool1)
    {
      String str = String.valueOf(paramString);
      localObject = "Loading url: ".concat(str);
      MoPubLog.d((String)localObject);
    }
    localObject = o;
    if (localObject != null)
    {
      paramString = y;
      boolean bool2 = TextUtils.isEmpty(paramString);
      if (!bool2)
      {
        paramString = new java/lang/StringBuilder;
        paramString.<init>("Already loading an ad for ");
        paramMoPubError = y;
        paramString.append(paramMoPubError);
        paramMoPubError = ", wait to finish.";
        paramString.append(paramMoPubError);
        paramString = paramString.toString();
        MoPubLog.i(paramString);
      }
      return;
    }
    b(paramString, paramMoPubError);
  }
  
  private void b(MoPubErrorCode paramMoPubErrorCode)
  {
    MoPubLog.i("Ad failed to load.");
    a();
    MoPubView localMoPubView = getMoPubView();
    if (localMoPubView == null) {
      return;
    }
    c();
    localMoPubView.b(paramMoPubErrorCode);
  }
  
  private void b(String paramString, MoPubError paramMoPubError)
  {
    Object localObject1 = getMoPubView();
    if (localObject1 != null)
    {
      Object localObject2 = a;
      if (localObject2 != null) {
        try
        {
          localObject2 = d;
          if (localObject2 != null)
          {
            localObject2 = d;
            boolean bool = ((AdLoader)localObject2).hasMoreAds();
            if (bool) {}
          }
          else
          {
            AdLoader localAdLoader = new com/mopub/network/AdLoader;
            AdFormat localAdFormat = ((MoPubView)localObject1).getAdFormat();
            String str = y;
            Context localContext = a;
            AdLoader.Listener localListener = p;
            localObject1 = localAdLoader;
            localObject2 = paramString;
            localAdLoader.<init>(paramString, localAdFormat, str, localContext, localListener);
            d = localAdLoader;
          }
          paramString = d.loadNextAd(paramMoPubError);
          o = paramString;
          return;
        }
        finally {}
      }
    }
    MoPubLog.d("Can't load an ad in this ad view because it was destroyed.");
    a();
  }
  
  private void e()
  {
    x = true;
    String str = y;
    boolean bool = TextUtils.isEmpty(str);
    if (bool)
    {
      MoPubLog.d("Can't load an ad in this ad view because the ad unit ID is not set. Did you forget to call setAdUnitId()?");
      return;
    }
    bool = g();
    if (!bool)
    {
      MoPubLog.d("Can't load an ad because there is no network connectivity.");
      c();
      return;
    }
    str = f();
    a(str, null);
  }
  
  private String f()
  {
    WebViewAdUrlGenerator localWebViewAdUrlGenerator = c;
    Location localLocation = null;
    if (localWebViewAdUrlGenerator == null) {
      return null;
    }
    boolean bool = MoPub.canCollectPersonalInformation();
    Object localObject = c;
    String str = y;
    localObject = ((WebViewAdUrlGenerator)localObject).withAdUnitId(str);
    str = t;
    localObject = ((AdUrlGenerator)localObject).withKeywords(str);
    if (bool) {
      str = u;
    } else {
      str = null;
    }
    localObject = ((AdUrlGenerator)localObject).withUserDataKeywords(str);
    if (bool) {
      localLocation = v;
    }
    ((AdUrlGenerator)localObject).withLocation(localLocation);
    return c.generateUrlString("ads.mopub.com");
  }
  
  private boolean g()
  {
    Object localObject = a;
    if (localObject == null) {
      return false;
    }
    String str = "android.permission.ACCESS_NETWORK_STATE";
    boolean bool1 = DeviceUtils.isPermissionGranted((Context)localObject, str);
    boolean bool2 = true;
    if (!bool1) {
      return bool2;
    }
    localObject = (ConnectivityManager)a.getSystemService("connectivity");
    NetworkInfo localNetworkInfo = null;
    if (localObject != null) {
      localNetworkInfo = ((ConnectivityManager)localObject).getActiveNetworkInfo();
    }
    if (localNetworkInfo != null)
    {
      bool1 = localNetworkInfo.isConnected();
      if (bool1) {
        return bool2;
      }
    }
    return false;
  }
  
  public static void setShouldHonorServerDimensions(View paramView)
  {
    WeakHashMap localWeakHashMap = m;
    Boolean localBoolean = Boolean.TRUE;
    localWeakHashMap.put(paramView, localBoolean);
  }
  
  final void a()
  {
    Request localRequest = o;
    if (localRequest != null)
    {
      boolean bool = localRequest.isCanceled();
      if (!bool)
      {
        localRequest = o;
        localRequest.cancel();
      }
      o = null;
    }
    d = null;
  }
  
  final void a(AdResponse paramAdResponse)
  {
    i = 1;
    e = paramAdResponse;
    Object localObject = paramAdResponse.getCustomEventClassName();
    q = ((String)localObject);
    localObject = e.getRefreshTimeMillis();
    z = ((Integer)localObject);
    o = null;
    localObject = b;
    String str = paramAdResponse.getCustomEventClassName();
    paramAdResponse = paramAdResponse.getServerExtras();
    Preconditions.checkNotNull(paramAdResponse);
    if (localObject == null)
    {
      paramAdResponse = "Can't load an ad in this ad view because it was destroyed.";
      MoPubLog.d(paramAdResponse);
    }
    else
    {
      ((MoPubView)localObject).a(str, paramAdResponse);
    }
    c();
  }
  
  final void a(VolleyError paramVolleyError)
  {
    boolean bool1 = paramVolleyError instanceof MoPubNetworkError;
    if (bool1)
    {
      localObject1 = paramVolleyError;
      localObject1 = (MoPubNetworkError)paramVolleyError;
      localObject2 = ((MoPubNetworkError)localObject1).getRefreshTimeMillis();
      if (localObject2 != null)
      {
        localObject1 = ((MoPubNetworkError)localObject1).getRefreshTimeMillis();
        z = ((Integer)localObject1);
      }
    }
    Object localObject1 = a;
    Object localObject2 = networkResponse;
    int i1;
    if (bool1)
    {
      localObject3 = AdViewController.4.a;
      paramVolleyError = ((MoPubNetworkError)paramVolleyError).getReason();
      int i2 = paramVolleyError.ordinal();
      i2 = localObject3[i2];
      switch (i2)
      {
      default: 
        paramVolleyError = MoPubErrorCode.UNSPECIFIED;
        break;
      case 2: 
        paramVolleyError = MoPubErrorCode.NO_FILL;
        break;
      case 1: 
        paramVolleyError = MoPubErrorCode.WARMUP;
        break;
      }
    }
    else if (localObject2 == null)
    {
      boolean bool2 = DeviceUtils.isNetworkAvailable((Context)localObject1);
      if (!bool2) {
        paramVolleyError = MoPubErrorCode.NO_CONNECTION;
      } else {
        paramVolleyError = MoPubErrorCode.UNSPECIFIED;
      }
    }
    else
    {
      paramVolleyError = networkResponse;
      int i3 = statusCode;
      i1 = 400;
      if (i3 >= i1) {
        paramVolleyError = MoPubErrorCode.SERVER_ERROR;
      } else {
        paramVolleyError = MoPubErrorCode.UNSPECIFIED;
      }
    }
    Object localObject3 = MoPubErrorCode.SERVER_ERROR;
    if (paramVolleyError == localObject3)
    {
      i1 = i + 1;
      i = i1;
    }
    b(paramVolleyError);
  }
  
  final void a(boolean paramBoolean)
  {
    boolean bool = x;
    if (bool)
    {
      bool = s;
      if (bool != paramBoolean)
      {
        bool = true;
        break label28;
      }
    }
    bool = false;
    String str1 = null;
    label28:
    if (bool)
    {
      if (paramBoolean) {
        str1 = "enabled";
      } else {
        str1 = "disabled";
      }
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      String str2 = "Refresh ";
      localStringBuilder.<init>(str2);
      localStringBuilder.append(str1);
      localStringBuilder.append(" for ad unit (");
      str1 = y;
      localStringBuilder.append(str1);
      localStringBuilder.append(").");
      str1 = localStringBuilder.toString();
      MoPubLog.d(str1);
    }
    s = paramBoolean;
    paramBoolean = x;
    if (paramBoolean)
    {
      paramBoolean = s;
      if (paramBoolean)
      {
        c();
        return;
      }
    }
    paramBoolean = s;
    if (!paramBoolean) {
      d();
    }
  }
  
  final boolean a(MoPubErrorCode paramMoPubErrorCode)
  {
    AdLoader localAdLoader = d;
    if (localAdLoader != null)
    {
      boolean bool = localAdLoader.hasMoreAds();
      if (bool)
      {
        a("", paramMoPubErrorCode);
        return true;
      }
    }
    paramMoPubErrorCode = MoPubErrorCode.NO_FILL;
    b(paramMoPubErrorCode);
    return false;
  }
  
  final void b()
  {
    boolean bool = k;
    if (bool)
    {
      bool = h;
      if (!bool)
      {
        bool = true;
        a(bool);
      }
    }
  }
  
  final void c()
  {
    d();
    boolean bool = s;
    if (bool)
    {
      Object localObject = z;
      if (localObject != null)
      {
        int i1 = ((Integer)localObject).intValue();
        if (i1 > 0)
        {
          localObject = g;
          Runnable localRunnable = r;
          Integer localInteger = z;
          int i2 = localInteger.intValue();
          long l1 = i2;
          int i3 = i;
          double d1 = i3;
          double d2 = Math.pow(1.5D, d1);
          long l2 = d2;
          l1 *= l2;
          long l3 = Math.min(600000L, l1);
          ((Handler)localObject).postDelayed(localRunnable, l3);
        }
      }
    }
  }
  
  final void d()
  {
    Handler localHandler = g;
    Runnable localRunnable = r;
    localHandler.removeCallbacks(localRunnable);
  }
  
  public int getAdHeight()
  {
    Object localObject = e;
    if (localObject != null)
    {
      localObject = ((AdResponse)localObject).getHeight();
      if (localObject != null) {
        return e.getHeight().intValue();
      }
    }
    return 0;
  }
  
  public AdReport getAdReport()
  {
    String str = y;
    if (str != null)
    {
      Object localObject = e;
      if (localObject != null)
      {
        localObject = new com/mopub/common/AdReport;
        ClientMetadata localClientMetadata = ClientMetadata.getInstance(a);
        AdResponse localAdResponse = e;
        ((AdReport)localObject).<init>(str, localClientMetadata, localAdResponse);
        return (AdReport)localObject;
      }
    }
    return null;
  }
  
  public String getAdUnitId()
  {
    return y;
  }
  
  public int getAdWidth()
  {
    Object localObject = e;
    if (localObject != null)
    {
      localObject = ((AdResponse)localObject).getWidth();
      if (localObject != null) {
        return e.getWidth().intValue();
      }
    }
    return 0;
  }
  
  public boolean getAutorefreshEnabled()
  {
    return getCurrentAutoRefreshStatus();
  }
  
  public long getBroadcastIdentifier()
  {
    return n;
  }
  
  public boolean getCurrentAutoRefreshStatus()
  {
    return s;
  }
  
  public String getCustomEventClassName()
  {
    return q;
  }
  
  public String getKeywords()
  {
    return t;
  }
  
  public Location getLocation()
  {
    boolean bool = MoPub.canCollectPersonalInformation();
    if (!bool) {
      return null;
    }
    return v;
  }
  
  public MoPubView getMoPubView()
  {
    return b;
  }
  
  public boolean getTesting()
  {
    return w;
  }
  
  public String getUserDataKeywords()
  {
    boolean bool = MoPub.canCollectPersonalInformation();
    if (!bool) {
      return null;
    }
    return u;
  }
  
  public void loadAd()
  {
    i = 1;
    e();
  }
  
  public void reload()
  {
    loadAd();
  }
  
  public void setAdUnitId(String paramString)
  {
    y = paramString;
  }
  
  public void setKeywords(String paramString)
  {
    t = paramString;
  }
  
  public void setLocation(Location paramLocation)
  {
    boolean bool = MoPub.canCollectPersonalInformation();
    if (!bool)
    {
      v = null;
      return;
    }
    v = paramLocation;
  }
  
  public void setTesting(boolean paramBoolean)
  {
    w = paramBoolean;
  }
  
  public void setUserDataKeywords(String paramString)
  {
    boolean bool = MoPub.canCollectPersonalInformation();
    if (!bool)
    {
      u = null;
      return;
    }
    u = paramString;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.AdViewController
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */