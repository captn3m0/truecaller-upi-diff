package com.mopub.mobileads;

import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Strings;
import com.mopub.mobileads.util.XmlUtils;
import org.w3c.dom.Node;

public class VideoViewabilityTrackerXmlManager
{
  public static final String PERCENT_VIEWABLE = "percentViewable";
  public static final String VIEWABLE_PLAYTIME = "viewablePlaytime";
  final Node a;
  
  VideoViewabilityTrackerXmlManager(Node paramNode)
  {
    Preconditions.checkNotNull(paramNode);
    a = paramNode;
  }
  
  final Integer a()
  {
    Object localObject = XmlUtils.getAttributeValue(a, "viewablePlaytime");
    if (localObject == null) {
      return null;
    }
    boolean bool = Strings.isAbsoluteTracker((String)localObject);
    int j = 1;
    String str;
    Object[] arrayOfObject;
    int i;
    if (bool) {
      try
      {
        localObject = Strings.parseAbsoluteOffset((String)localObject);
      }
      catch (NumberFormatException localNumberFormatException1)
      {
        str = "Invalid VAST viewablePlaytime format for \"HH:MM:SS[.mmm]\": %s:";
        arrayOfObject = new Object[j];
        arrayOfObject[0] = localObject;
        localObject = String.format(str, arrayOfObject);
        MoPubLog.d((String)localObject);
        break label123;
      }
    } else {
      try
      {
        float f1 = Float.parseFloat((String)localObject);
        float f2 = 1000.0F;
        f1 *= f2;
        i = (int)f1;
        localObject = Integer.valueOf(i);
      }
      catch (NumberFormatException localNumberFormatException2)
      {
        str = "Invalid VAST viewablePlaytime format for \"SS[.mmm]\": %s:";
        arrayOfObject = new Object[j];
        arrayOfObject[0] = localObject;
        localObject = String.format(str, arrayOfObject);
        MoPubLog.d((String)localObject);
      }
    }
    label123:
    localObject = null;
    if (localObject != null)
    {
      i = ((Integer)localObject).intValue();
      if (i >= 0) {
        return (Integer)localObject;
      }
    }
    return null;
  }
  
  final Integer b()
  {
    Object localObject1 = XmlUtils.getAttributeValue(a, "percentViewable");
    if (localObject1 == null) {
      return null;
    }
    String str = "%";
    Object localObject2 = "";
    int i;
    int j;
    try
    {
      str = ((String)localObject1).replace(str, (CharSequence)localObject2);
      float f = Float.parseFloat(str);
      i = (int)f;
      localObject1 = Integer.valueOf(i);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      str = "Invalid VAST percentViewable format for \"d{1,3}%%\": %s:";
      j = 1;
      localObject2 = new Object[j];
      localObject2[0] = localObject1;
      MoPubLog.d(String.format(str, (Object[])localObject2));
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      i = ((Integer)localObject1).intValue();
      if (i >= 0)
      {
        i = ((Integer)localObject1).intValue();
        j = 100;
        if (i <= j) {
          return (Integer)localObject1;
        }
      }
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VideoViewabilityTrackerXmlManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */