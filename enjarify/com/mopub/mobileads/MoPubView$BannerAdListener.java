package com.mopub.mobileads;

public abstract interface MoPubView$BannerAdListener
{
  public abstract void onBannerClicked(MoPubView paramMoPubView);
  
  public abstract void onBannerCollapsed(MoPubView paramMoPubView);
  
  public abstract void onBannerExpanded(MoPubView paramMoPubView);
  
  public abstract void onBannerFailed(MoPubView paramMoPubView, MoPubErrorCode paramMoPubErrorCode);
  
  public abstract void onBannerLoaded(MoPubView paramMoPubView);
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.MoPubView.BannerAdListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */