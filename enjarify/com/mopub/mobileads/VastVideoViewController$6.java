package com.mopub.mobileads;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.view.View;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.mopub.common.ExternalViewabilitySession.VideoEvent;
import com.mopub.common.ExternalViewabilitySessionManager;

final class VastVideoViewController$6
  implements MediaPlayer.OnCompletionListener
{
  VastVideoViewController$6(VastVideoViewController paramVastVideoViewController, VastVideoView paramVastVideoView, Context paramContext) {}
  
  public final void onCompletion(MediaPlayer paramMediaPlayer)
  {
    VastVideoViewController.q(c);
    c.a();
    paramMediaPlayer = c;
    Context localContext = null;
    paramMediaPlayer.videoCompleted(false);
    VastVideoViewController.r(c);
    paramMediaPlayer = VastVideoViewController.f(c);
    boolean bool1 = paramMediaPlayer.isRewardedVideo();
    Object localObject;
    if (bool1)
    {
      paramMediaPlayer = c;
      localObject = "com.mopub.action.rewardedvideo.complete";
      paramMediaPlayer.a((String)localObject);
    }
    paramMediaPlayer = c;
    bool1 = VastVideoViewController.s(paramMediaPlayer);
    if (!bool1)
    {
      paramMediaPlayer = VastVideoViewController.f(c);
      int i = paramMediaPlayer.getRemainingProgressTrackerCount();
      if (i == 0)
      {
        paramMediaPlayer = VastVideoViewController.b(c);
        localObject = ExternalViewabilitySession.VideoEvent.AD_COMPLETE;
        int k = c.a.getCurrentPosition();
        paramMediaPlayer.recordVideoEvent((ExternalViewabilitySession.VideoEvent)localObject, k);
        paramMediaPlayer = VastVideoViewController.f(c);
        localObject = c.mContext;
        VastVideoView localVastVideoView = c.a;
        k = localVastVideoView.getCurrentPosition();
        paramMediaPlayer.handleComplete((Context)localObject, k);
      }
    }
    a.setVisibility(4);
    paramMediaPlayer = VastVideoViewController.n(c);
    int m = 8;
    paramMediaPlayer.setVisibility(m);
    paramMediaPlayer = c;
    boolean bool2 = VastVideoViewController.k(paramMediaPlayer);
    if (bool2)
    {
      paramMediaPlayer = VastVideoViewController.l(c).getDrawable();
      if (paramMediaPlayer != null)
      {
        paramMediaPlayer = VastVideoViewController.l(c);
        localObject = ImageView.ScaleType.CENTER_CROP;
        paramMediaPlayer.setScaleType((ImageView.ScaleType)localObject);
        paramMediaPlayer = VastVideoViewController.l(c);
        paramMediaPlayer.setVisibility(0);
      }
    }
    else
    {
      paramMediaPlayer = VastVideoViewController.g(c);
      paramMediaPlayer.setVisibility(m);
    }
    VastVideoViewController.t(c).a();
    VastVideoViewController.u(c).a();
    paramMediaPlayer = VastVideoViewController.v(c);
    m = 1;
    b = m;
    c = m;
    paramMediaPlayer.a();
    paramMediaPlayer = VastVideoViewController.j(c);
    if (paramMediaPlayer != null)
    {
      paramMediaPlayer = b.getResources().getConfiguration();
      int j = orientation;
      if (j == m)
      {
        paramMediaPlayer = VastVideoViewController.w(c);
        paramMediaPlayer.setVisibility(0);
      }
      else
      {
        paramMediaPlayer = VastVideoViewController.x(c);
        paramMediaPlayer.setVisibility(0);
      }
      paramMediaPlayer = VastVideoViewController.j(c);
      localContext = b;
      m = VastVideoViewController.e(c);
      paramMediaPlayer.a(localContext, m);
      return;
    }
    paramMediaPlayer = VastVideoViewController.l(c).getDrawable();
    if (paramMediaPlayer != null)
    {
      paramMediaPlayer = VastVideoViewController.l(c);
      paramMediaPlayer.setVisibility(0);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastVideoViewController.6
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */