package com.mopub.mobileads;

public abstract interface ViewGestureDetector$UserClickListener
{
  public abstract void onResetUserClick();
  
  public abstract void onUserClick();
  
  public abstract boolean wasClicked();
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.ViewGestureDetector.UserClickListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */