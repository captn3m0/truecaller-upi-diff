package com.mopub.mobileads;

import android.text.TextUtils;
import com.mopub.common.Preconditions;
import com.mopub.mobileads.util.XmlUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.w3c.dom.Node;

final class h
{
  final Node a;
  final VastResourceXmlManager b;
  
  h(Node paramNode)
  {
    Preconditions.checkNotNull(paramNode, "companionNode cannot be null");
    a = paramNode;
    VastResourceXmlManager localVastResourceXmlManager = new com/mopub/mobileads/VastResourceXmlManager;
    localVastResourceXmlManager.<init>(paramNode);
    b = localVastResourceXmlManager;
  }
  
  final Integer a()
  {
    return XmlUtils.getAttributeValueAsInt(a, "width");
  }
  
  final Integer b()
  {
    return XmlUtils.getAttributeValueAsInt(a, "height");
  }
  
  final String c()
  {
    return XmlUtils.getNodeValue(XmlUtils.getFirstMatchingChildNode(a, "CompanionClickThrough"));
  }
  
  final List d()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject = a;
    String str = "CompanionClickTracking";
    localObject = XmlUtils.getMatchingChildNodes((Node)localObject, str);
    if (localObject == null) {
      return localArrayList;
    }
    localObject = ((List)localObject).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject).hasNext();
      if (!bool1) {
        break;
      }
      str = XmlUtils.getNodeValue((Node)((Iterator)localObject).next());
      boolean bool2 = TextUtils.isEmpty(str);
      if (!bool2)
      {
        VastTracker localVastTracker = new com/mopub/mobileads/VastTracker;
        localVastTracker.<init>(str);
        localArrayList.add(localVastTracker);
      }
    }
    return localArrayList;
  }
  
  final List e()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject1 = a;
    String str = "TrackingEvents";
    localObject1 = XmlUtils.getFirstMatchingChildNode((Node)localObject1, str);
    if (localObject1 == null) {
      return localArrayList;
    }
    str = "Tracking";
    Object localObject2 = "event";
    List localList = Collections.singletonList("creativeView");
    localObject1 = XmlUtils.getMatchingChildNodes((Node)localObject1, str, (String)localObject2, localList).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      str = XmlUtils.getNodeValue((Node)((Iterator)localObject1).next());
      localObject2 = new com/mopub/mobileads/VastTracker;
      ((VastTracker)localObject2).<init>(str);
      localArrayList.add(localObject2);
    }
    return localArrayList;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */