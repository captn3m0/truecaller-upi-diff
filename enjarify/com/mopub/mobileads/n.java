package com.mopub.mobileads;

import com.mopub.common.Preconditions;
import com.mopub.mobileads.util.XmlUtils;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

final class n
  implements Serializable
{
  private static final List a = Arrays.asList(tmp18_5);
  private static final List b = Arrays.asList(new String[] { "application/x-javascript" });
  private static final long serialVersionUID;
  private String c;
  private n.b d;
  private n.a e;
  private int f;
  private int g;
  
  static
  {
    String[] tmp4_1 = new String[4];
    String[] tmp5_4 = tmp4_1;
    String[] tmp5_4 = tmp4_1;
    tmp5_4[0] = "image/jpeg";
    tmp5_4[1] = "image/png";
    tmp5_4[2] = "image/bmp";
    String[] tmp18_5 = tmp5_4;
    tmp18_5[3] = "image/gif";
  }
  
  private n(String paramString, n.b paramb, n.a parama, int paramInt1, int paramInt2)
  {
    Preconditions.checkNotNull(paramString);
    Preconditions.checkNotNull(paramb);
    Preconditions.checkNotNull(parama);
    c = paramString;
    d = paramb;
    e = parama;
    f = paramInt1;
    g = paramInt2;
  }
  
  static n a(VastResourceXmlManager paramVastResourceXmlManager, n.b paramb, int paramInt1, int paramInt2)
  {
    Preconditions.checkNotNull(paramVastResourceXmlManager);
    Preconditions.checkNotNull(paramb);
    Object localObject1 = paramVastResourceXmlManager.b();
    Object localObject2 = paramVastResourceXmlManager.c();
    Object localObject3 = paramVastResourceXmlManager.a();
    paramVastResourceXmlManager = XmlUtils.getAttributeValue(XmlUtils.getFirstMatchingChildNode(a, "StaticResource"), "creativeType");
    boolean bool1;
    if (paramVastResourceXmlManager != null)
    {
      paramVastResourceXmlManager = paramVastResourceXmlManager.toLowerCase();
    }
    else
    {
      bool1 = false;
      paramVastResourceXmlManager = null;
    }
    Object localObject4 = n.b.STATIC_RESOURCE;
    if ((paramb == localObject4) && (localObject3 != null) && (paramVastResourceXmlManager != null))
    {
      localObject4 = a;
      boolean bool2 = ((List)localObject4).contains(paramVastResourceXmlManager);
      if (!bool2)
      {
        localObject4 = b;
        bool2 = ((List)localObject4).contains(paramVastResourceXmlManager);
        if (!bool2) {}
      }
      else
      {
        localObject1 = a;
        bool1 = ((List)localObject1).contains(paramVastResourceXmlManager);
        if (bool1)
        {
          paramVastResourceXmlManager = n.a.IMAGE;
          localObject4 = paramVastResourceXmlManager;
          break label211;
        }
        paramVastResourceXmlManager = n.a.JAVASCRIPT;
        localObject4 = paramVastResourceXmlManager;
        break label211;
      }
    }
    paramVastResourceXmlManager = n.b.HTML_RESOURCE;
    if ((paramb == paramVastResourceXmlManager) && (localObject2 != null))
    {
      paramVastResourceXmlManager = n.a.NONE;
      localObject4 = paramVastResourceXmlManager;
      localObject3 = localObject2;
    }
    else
    {
      paramVastResourceXmlManager = n.b.IFRAME_RESOURCE;
      if ((paramb != paramVastResourceXmlManager) || (localObject1 == null)) {
        break label231;
      }
      paramVastResourceXmlManager = n.a.NONE;
      localObject4 = paramVastResourceXmlManager;
      localObject3 = localObject1;
    }
    label211:
    paramVastResourceXmlManager = new com/mopub/mobileads/n;
    localObject2 = paramVastResourceXmlManager;
    paramVastResourceXmlManager.<init>((String)localObject3, paramb, (n.a)localObject4, paramInt1, paramInt2);
    return paramVastResourceXmlManager;
    label231:
    return null;
  }
  
  public final String getCorrectClickThroughUrl(String paramString1, String paramString2)
  {
    Object localObject = n.1.a;
    int i = d.ordinal();
    int j = localObject[i];
    i = 0;
    switch (j)
    {
    default: 
      return null;
    case 2: 
    case 3: 
      return paramString2;
    }
    localObject = n.a.IMAGE;
    n.a locala = e;
    if (localObject == locala) {
      return paramString1;
    }
    paramString1 = n.a.JAVASCRIPT;
    localObject = e;
    if (paramString1 == localObject) {
      return paramString2;
    }
    return null;
  }
  
  public final n.a getCreativeType()
  {
    return e;
  }
  
  public final String getResource()
  {
    return c;
  }
  
  public final n.b getType()
  {
    return d;
  }
  
  public final void initializeWebView(o paramo)
  {
    Preconditions.checkNotNull(paramo);
    Object localObject1 = d;
    Object localObject2 = n.b.IFRAME_RESOURCE;
    if (localObject1 == localObject2)
    {
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("<iframe frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" style=\"border: 0px; margin: 0px;\" width=\"");
      int i = f;
      ((StringBuilder)localObject1).append(i);
      ((StringBuilder)localObject1).append("\" height=\"");
      i = g;
      ((StringBuilder)localObject1).append(i);
      ((StringBuilder)localObject1).append("\" src=\"");
      localObject2 = c;
      ((StringBuilder)localObject1).append((String)localObject2);
      ((StringBuilder)localObject1).append("\"></iframe>");
      localObject1 = ((StringBuilder)localObject1).toString();
      paramo.a((String)localObject1);
      return;
    }
    localObject1 = d;
    localObject2 = n.b.HTML_RESOURCE;
    if (localObject1 == localObject2)
    {
      localObject1 = c;
      paramo.a((String)localObject1);
      return;
    }
    localObject1 = d;
    localObject2 = n.b.STATIC_RESOURCE;
    if (localObject1 == localObject2)
    {
      localObject1 = e;
      localObject2 = n.a.IMAGE;
      if (localObject1 == localObject2)
      {
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>("<html><head></head><body style=\"margin:0;padding:0\"><img src=\"");
        localObject2 = c;
        ((StringBuilder)localObject1).append((String)localObject2);
        ((StringBuilder)localObject1).append("\" width=\"100%\" style=\"max-width:100%;max-height:100%;\" /></body></html>");
        localObject1 = ((StringBuilder)localObject1).toString();
        paramo.a((String)localObject1);
        return;
      }
      localObject1 = e;
      localObject2 = n.a.JAVASCRIPT;
      if (localObject1 == localObject2)
      {
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>("<script src=\"");
        localObject2 = c;
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject2 = "\"></script>";
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject1 = ((StringBuilder)localObject1).toString();
        paramo.a((String)localObject1);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */