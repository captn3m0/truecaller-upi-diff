package com.mopub.mobileads;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Utils;
import com.mopub.mobileads.resource.CloseButtonDrawable;
import com.mopub.mobileads.resource.DrawableConstants.CloseButton;
import com.mopub.network.Networking;
import com.mopub.volley.toolbox.ImageLoader;

public class VastVideoCloseButtonWidget
  extends RelativeLayout
{
  TextView a;
  final ImageLoader b;
  private ImageView c;
  private CloseButtonDrawable d;
  private final int e;
  private final int f;
  private final int g;
  private final int h;
  
  public VastVideoCloseButtonWidget(Context paramContext)
  {
    super(paramContext);
    int i = (int)Utils.generateUniqueId();
    setId(i);
    int j = Dips.dipsToIntPixels(6.0F, paramContext);
    e = j;
    j = Dips.dipsToIntPixels(15.0F, paramContext);
    g = j;
    j = Dips.dipsToIntPixels(56.0F, paramContext);
    h = j;
    j = Dips.dipsToIntPixels(0.0F, paramContext);
    f = j;
    Object localObject1 = new com/mopub/mobileads/resource/CloseButtonDrawable;
    ((CloseButtonDrawable)localObject1).<init>();
    d = ((CloseButtonDrawable)localObject1);
    paramContext = Networking.getImageLoader(paramContext);
    b = paramContext;
    paramContext = new android/widget/ImageView;
    localObject1 = getContext();
    paramContext.<init>((Context)localObject1);
    c = paramContext;
    paramContext = c;
    i = (int)Utils.generateUniqueId();
    paramContext.setId(i);
    paramContext = new android/widget/RelativeLayout$LayoutParams;
    j = h;
    paramContext.<init>(j, j);
    j = 11;
    paramContext.addRule(j);
    Object localObject2 = c;
    Object localObject3 = d;
    ((ImageView)localObject2).setImageDrawable((Drawable)localObject3);
    localObject2 = c;
    int k = g;
    int m = e;
    int n = k + m;
    m += k;
    ((ImageView)localObject2).setPadding(k, n, m, k);
    localObject2 = c;
    addView((View)localObject2, paramContext);
    paramContext = new android/widget/TextView;
    localObject2 = getContext();
    paramContext.<init>((Context)localObject2);
    a = paramContext;
    a.setSingleLine();
    paramContext = a;
    localObject2 = TextUtils.TruncateAt.END;
    paramContext.setEllipsize((TextUtils.TruncateAt)localObject2);
    a.setTextColor(-1);
    a.setTextSize(20.0F);
    paramContext = a;
    localObject2 = DrawableConstants.CloseButton.TEXT_TYPEFACE;
    paramContext.setTypeface((Typeface)localObject2);
    a.setText("");
    paramContext = new android/widget/RelativeLayout$LayoutParams;
    i = -2;
    paramContext.<init>(i, i);
    paramContext.addRule(15);
    k = c.getId();
    paramContext.addRule(0, k);
    localObject3 = a;
    n = e;
    ((TextView)localObject3).setPadding(0, n, 0, 0);
    k = f;
    paramContext.setMargins(0, 0, k, 0);
    localObject3 = a;
    addView((View)localObject3, paramContext);
    paramContext = new android/widget/RelativeLayout$LayoutParams;
    k = h;
    paramContext.<init>(i, k);
    paramContext.addRule(j);
    setLayoutParams(paramContext);
  }
  
  ImageView getImageView()
  {
    return c;
  }
  
  TextView getTextView()
  {
    return a;
  }
  
  void setImageView(ImageView paramImageView)
  {
    c = paramImageView;
  }
  
  void setOnTouchListenerToContent(View.OnTouchListener paramOnTouchListener)
  {
    c.setOnTouchListener(paramOnTouchListener);
    a.setOnTouchListener(paramOnTouchListener);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastVideoCloseButtonWidget
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */