package com.mopub.mobileads;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.VideoView;
import com.mopub.common.ExternalViewabilitySession.VideoEvent;
import com.mopub.common.ExternalViewabilitySessionManager;
import com.mopub.common.Preconditions;
import com.mopub.common.util.DeviceUtils.ForceOrientation;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Utils;
import com.mopub.mobileads.resource.CtaButtonDrawable;
import com.mopub.volley.toolbox.ImageLoader;
import com.mopub.volley.toolbox.ImageLoader.ImageListener;
import java.util.Map;

public class VastVideoViewController
  extends BaseVideoViewController
{
  public static final int WEBVIEW_PADDING = 16;
  private boolean A;
  private boolean B;
  private boolean C;
  private int D;
  private boolean E;
  final VastVideoView a;
  ExternalViewabilitySessionManager b;
  VastVideoProgressBarWidget c;
  VastVideoRadialCountdownWidget d;
  final i e;
  final Map f;
  final View g;
  int h = 5000;
  boolean i;
  boolean j;
  private final VastVideoConfig k;
  private VastVideoGradientStripWidget l;
  private VastVideoGradientStripWidget m;
  private ImageView n;
  private VastVideoCtaButtonWidget o;
  private VastVideoCloseButtonWidget p;
  private VastCompanionAdConfig q;
  private final View r;
  private final View s;
  private View t;
  private final View u;
  private final VastVideoViewProgressRunnable v;
  private final VastVideoViewCountdownRunnable w;
  private final View.OnTouchListener x;
  private int y;
  private boolean z;
  
  VastVideoViewController(Activity paramActivity, Bundle paramBundle1, Bundle paramBundle2, long paramLong, BaseVideoViewController.BaseVideoViewControllerListener paramBaseVideoViewControllerListener)
  {
    super(paramActivity, (Long)localObject1, paramBaseVideoViewControllerListener);
    boolean bool1 = false;
    localObject1 = null;
    B = false;
    j = false;
    C = false;
    E = false;
    int i1 = -1;
    y = i1;
    int i2;
    if (paramBundle2 != null)
    {
      paramBaseVideoViewControllerListener = paramBundle2.getSerializable("resumed_vast_config");
    }
    else
    {
      i2 = 0;
      paramBaseVideoViewControllerListener = null;
    }
    Object localObject2 = "vast_video_config";
    paramBundle1 = paramBundle1.getSerializable((String)localObject2);
    int i4;
    if (paramBaseVideoViewControllerListener != null)
    {
      boolean bool2 = paramBaseVideoViewControllerListener instanceof VastVideoConfig;
      if (bool2)
      {
        paramBaseVideoViewControllerListener = (VastVideoConfig)paramBaseVideoViewControllerListener;
        k = paramBaseVideoViewControllerListener;
        paramBundle1 = "current_position";
        i4 = paramBundle2.getInt(paramBundle1, i1);
        y = i4;
        break label166;
      }
    }
    if (paramBundle1 != null)
    {
      boolean bool4 = paramBundle1 instanceof VastVideoConfig;
      if (bool4)
      {
        paramBundle1 = (VastVideoConfig)paramBundle1;
        k = paramBundle1;
        label166:
        paramBundle1 = k.getDiskMediaFileUrl();
        if (paramBundle1 != null)
        {
          paramBundle1 = k;
          paramBundle2 = paramActivity.getResources().getConfiguration();
          int i6 = orientation;
          paramBundle1 = paramBundle1.getVastCompanionAd(i6);
          q = paramBundle1;
          paramBundle1 = k.getSocialActionsCompanionAds();
          f = paramBundle1;
          paramBundle1 = k.getVastIconConfig();
          e = paramBundle1;
          paramBundle1 = new com/mopub/mobileads/VastVideoViewController$1;
          paramBundle1.<init>(this, paramActivity);
          x = paramBundle1;
          getLayout().setBackgroundColor(-16777216);
          paramBundle1 = new android/widget/ImageView;
          paramBundle1.<init>(paramActivity);
          n = paramBundle1;
          paramBundle1 = n;
          i6 = 4;
          paramBundle1.setVisibility(i6);
          paramBundle1 = new android/widget/RelativeLayout$LayoutParams;
          paramBundle1.<init>(i1, i1);
          Object localObject3 = getLayout();
          paramBaseVideoViewControllerListener = n;
          ((ViewGroup)localObject3).addView(paramBaseVideoViewControllerListener, paramBundle1);
          paramBundle1 = k.getDiskMediaFileUrl();
          if (paramBundle1 != null)
          {
            paramBundle1 = new com/mopub/mobileads/VastVideoView;
            paramBundle1.<init>(paramActivity);
            long l1 = Utils.generateUniqueId();
            i2 = (int)l1;
            paramBundle1.setId(i2);
            localObject3 = new com/mopub/mobileads/VastVideoViewController$5;
            ((VastVideoViewController.5)localObject3).<init>(this, paramBundle1);
            paramBundle1.setOnPreparedListener((MediaPlayer.OnPreparedListener)localObject3);
            localObject3 = x;
            paramBundle1.setOnTouchListener((View.OnTouchListener)localObject3);
            localObject3 = new com/mopub/mobileads/VastVideoViewController$6;
            ((VastVideoViewController.6)localObject3).<init>(this, paramBundle1, paramActivity);
            paramBundle1.setOnCompletionListener((MediaPlayer.OnCompletionListener)localObject3);
            localObject3 = new com/mopub/mobileads/VastVideoViewController$7;
            ((VastVideoViewController.7)localObject3).<init>(this);
            paramBundle1.setOnErrorListener((MediaPlayer.OnErrorListener)localObject3);
            localObject3 = k.getDiskMediaFileUrl();
            paramBundle1.setVideoPath((String)localObject3);
            paramBundle1.setVisibility(0);
            a = paramBundle1;
            a.requestFocus();
            paramBundle1 = new com/mopub/common/ExternalViewabilitySessionManager;
            paramBundle1.<init>(paramActivity);
            b = paramBundle1;
            paramBundle1 = b;
            localObject3 = a;
            paramBaseVideoViewControllerListener = k;
            paramBundle1.createVideoSession(paramActivity, (View)localObject3, paramBaseVideoViewControllerListener);
            paramBundle1 = b;
            localObject3 = n;
            paramBundle1.registerVideoObstruction((View)localObject3);
            paramBundle1 = k.getVastCompanionAd(2);
            paramBundle1 = a(paramActivity, paramBundle1);
            r = paramBundle1;
            paramBundle1 = k;
            i1 = 1;
            paramBundle1 = paramBundle1.getVastCompanionAd(i1);
            paramBundle1 = a(paramActivity, paramBundle1);
            s = paramBundle1;
            paramBundle1 = q;
            boolean bool5;
            if (paramBundle1 != null) {
              bool5 = true;
            } else {
              bool5 = false;
            }
            paramBundle1 = new com/mopub/mobileads/VastVideoGradientStripWidget;
            Object localObject4 = GradientDrawable.Orientation.TOP_BOTTOM;
            DeviceUtils.ForceOrientation localForceOrientation = k.getCustomForceOrientation();
            int i8 = 0;
            VastVideoCtaButtonWidget localVastVideoCtaButtonWidget = null;
            int i9 = 6;
            int i10 = getLayout().getId();
            localObject2 = paramBundle1;
            paramBundle1.<init>(paramActivity, (GradientDrawable.Orientation)localObject4, localForceOrientation, bool5, 0, i9, i10);
            l = paramBundle1;
            paramBundle1 = getLayout();
            paramBaseVideoViewControllerListener = l;
            paramBundle1.addView(paramBaseVideoViewControllerListener);
            paramBundle1 = b;
            paramBaseVideoViewControllerListener = l;
            paramBundle1.registerVideoObstruction(paramBaseVideoViewControllerListener);
            paramBundle1 = new com/mopub/mobileads/VastVideoProgressBarWidget;
            paramBundle1.<init>(paramActivity);
            c = paramBundle1;
            paramBundle1 = c;
            i2 = a.getId();
            paramBundle1.setAnchorId(i2);
            c.setVisibility(i6);
            paramBundle1 = getLayout();
            paramBaseVideoViewControllerListener = c;
            paramBundle1.addView(paramBaseVideoViewControllerListener);
            paramBundle1 = b;
            paramBaseVideoViewControllerListener = c;
            paramBundle1.registerVideoObstruction(paramBaseVideoViewControllerListener);
            paramBundle1 = q;
            if (paramBundle1 != null) {
              bool5 = true;
            } else {
              bool5 = false;
            }
            paramBundle1 = new com/mopub/mobileads/VastVideoGradientStripWidget;
            localObject4 = GradientDrawable.Orientation.BOTTOM_TOP;
            localForceOrientation = k.getCustomForceOrientation();
            i8 = 8;
            i9 = 2;
            i10 = c.getId();
            localObject2 = paramBundle1;
            paramBundle1.<init>(paramActivity, (GradientDrawable.Orientation)localObject4, localForceOrientation, bool5, i8, i9, i10);
            m = paramBundle1;
            paramBundle1 = getLayout();
            paramBaseVideoViewControllerListener = m;
            paramBundle1.addView(paramBaseVideoViewControllerListener);
            paramBundle1 = b;
            paramBaseVideoViewControllerListener = m;
            paramBundle1.registerVideoObstruction(paramBaseVideoViewControllerListener);
            paramBundle1 = new com/mopub/mobileads/VastVideoRadialCountdownWidget;
            paramBundle1.<init>(paramActivity);
            d = paramBundle1;
            d.setVisibility(i6);
            paramBundle1 = getLayout();
            paramBaseVideoViewControllerListener = d;
            paramBundle1.addView(paramBaseVideoViewControllerListener);
            paramBundle1 = b;
            paramBaseVideoViewControllerListener = d;
            paramBundle1.registerVideoObstruction(paramBaseVideoViewControllerListener);
            paramBundle1 = e;
            Preconditions.checkNotNull(paramActivity);
            if (paramBundle1 == null)
            {
              paramBundle1 = new android/view/View;
              paramBundle1.<init>(paramActivity);
            }
            else
            {
              paramBaseVideoViewControllerListener = e;
              paramBaseVideoViewControllerListener = o.a(paramActivity, paramBaseVideoViewControllerListener);
              localObject2 = new com/mopub/mobileads/VastVideoViewController$9;
              ((VastVideoViewController.9)localObject2).<init>(this, paramBundle1, paramActivity);
              b = ((o.a)localObject2);
              localObject2 = new com/mopub/mobileads/VastVideoViewController$10;
              ((VastVideoViewController.10)localObject2).<init>(this, paramBundle1);
              paramBaseVideoViewControllerListener.setWebViewClient((WebViewClient)localObject2);
              paramBaseVideoViewControllerListener.setVisibility(i6);
              paramBundle2 = new android/widget/RelativeLayout$LayoutParams;
              float f1 = a;
              int i3 = Dips.asIntPixels(f1, paramActivity);
              i4 = Dips.asIntPixels(b, paramActivity);
              paramBundle2.<init>(i3, i4);
              f2 = 12.0F;
              i3 = Dips.dipsToIntPixels(f2, paramActivity);
              i4 = Dips.dipsToIntPixels(f2, paramActivity);
              paramBundle2.setMargins(i3, i4, 0, 0);
              getLayout().addView(paramBaseVideoViewControllerListener, paramBundle2);
              b.registerVideoObstruction(paramBaseVideoViewControllerListener);
              paramBundle1 = paramBaseVideoViewControllerListener;
            }
            g = paramBundle1;
            paramBundle1 = g.getViewTreeObserver();
            paramBundle2 = new com/mopub/mobileads/VastVideoViewController$4;
            paramBundle2.<init>(this, paramActivity);
            paramBundle1.addOnGlobalLayoutListener(paramBundle2);
            paramBundle1 = q;
            if (paramBundle1 != null) {
              bool1 = true;
            }
            boolean bool3 = TextUtils.isEmpty(k.getClickThroughUrl()) ^ i1;
            paramBundle2 = new com/mopub/mobileads/VastVideoCtaButtonWidget;
            localObject3 = a;
            i1 = ((VastVideoView)localObject3).getId();
            paramBundle2.<init>(paramActivity, i1, bool1, bool3);
            o = paramBundle2;
            paramBundle1 = getLayout();
            paramBundle2 = o;
            paramBundle1.addView(paramBundle2);
            paramBundle1 = b;
            paramBundle2 = o;
            paramBundle1.registerVideoObstruction(paramBundle2);
            paramBundle1 = o;
            paramBundle2 = x;
            paramBundle1.setOnTouchListener(paramBundle2);
            paramBundle1 = k.getCustomCtaText();
            if (paramBundle1 != null)
            {
              paramBundle2 = o.a;
              paramBundle2.setCtaText(paramBundle1);
            }
            int i11 = Dips.dipsToIntPixels(38.0F, paramActivity);
            paramBundle1 = f;
            paramBundle2 = "socialActions";
            paramBundle1 = paramBundle1.get(paramBundle2);
            localObject4 = paramBundle1;
            localObject4 = (VastCompanionAdConfig)paramBundle1;
            int i7 = 6;
            localVastVideoCtaButtonWidget = o;
            i9 = 4;
            i10 = 16;
            localObject2 = this;
            paramBundle1 = a(paramActivity, (VastCompanionAdConfig)localObject4, i11, i7, localVastVideoCtaButtonWidget, i9, i10);
            u = paramBundle1;
            paramBundle1 = new com/mopub/mobileads/VastVideoCloseButtonWidget;
            paramBundle1.<init>(paramActivity);
            p = paramBundle1;
            paramActivity = p;
            int i5 = 8;
            float f2 = 1.1E-44F;
            paramActivity.setVisibility(i5);
            paramActivity = getLayout();
            paramBundle1 = p;
            paramActivity.addView(paramBundle1);
            paramActivity = b;
            paramBundle1 = p;
            paramActivity.registerVideoObstruction(paramBundle1);
            paramActivity = new com/mopub/mobileads/VastVideoViewController$8;
            paramActivity.<init>(this);
            paramBundle1 = p;
            paramBundle1.setOnTouchListenerToContent(paramActivity);
            paramActivity = k.getCustomSkipText();
            if (paramActivity != null)
            {
              paramBundle1 = p;
              paramBundle2 = a;
              if (paramBundle2 != null)
              {
                paramBundle1 = a;
                paramBundle1.setText(paramActivity);
              }
            }
            paramActivity = k.getCustomCloseIconUrl();
            if (paramActivity != null)
            {
              paramBundle1 = p;
              paramBundle2 = b;
              localObject1 = new com/mopub/mobileads/VastVideoCloseButtonWidget$1;
              ((VastVideoCloseButtonWidget.1)localObject1).<init>(paramBundle1, paramActivity);
              paramBundle2.get(paramActivity, (ImageLoader.ImageListener)localObject1);
            }
            paramActivity = new android/os/Handler;
            paramBundle1 = Looper.getMainLooper();
            paramActivity.<init>(paramBundle1);
            paramBundle1 = new com/mopub/mobileads/VastVideoViewProgressRunnable;
            paramBundle2 = k;
            paramBundle1.<init>(this, paramBundle2, paramActivity);
            v = paramBundle1;
            paramBundle1 = new com/mopub/mobileads/VastVideoViewCountdownRunnable;
            paramBundle1.<init>(this, paramActivity);
            w = paramBundle1;
            return;
          }
          paramActivity = new java/lang/IllegalStateException;
          paramActivity.<init>("VastVideoConfig does not have a video disk path");
          throw paramActivity;
        }
        paramActivity = new java/lang/IllegalStateException;
        paramActivity.<init>("VastVideoConfig does not have a video disk path");
        throw paramActivity;
      }
    }
    paramActivity = new java/lang/IllegalStateException;
    paramActivity.<init>("VastVideoConfig is invalid");
    throw paramActivity;
  }
  
  private View a(Context paramContext, VastCompanionAdConfig paramVastCompanionAdConfig)
  {
    Preconditions.checkNotNull(paramContext);
    int i1 = 4;
    if (paramVastCompanionAdConfig == null)
    {
      paramVastCompanionAdConfig = new android/view/View;
      paramVastCompanionAdConfig.<init>(paramContext);
      paramVastCompanionAdConfig.setVisibility(i1);
      return paramVastCompanionAdConfig;
    }
    RelativeLayout localRelativeLayout = new android/widget/RelativeLayout;
    localRelativeLayout.<init>(paramContext);
    localRelativeLayout.setGravity(17);
    Object localObject = new android/widget/RelativeLayout$LayoutParams;
    int i2 = -1;
    ((RelativeLayout.LayoutParams)localObject).<init>(i2, i2);
    getLayout().addView(localRelativeLayout, (ViewGroup.LayoutParams)localObject);
    b.registerVideoObstruction(localRelativeLayout);
    localObject = b(paramContext, paramVastCompanionAdConfig);
    ((o)localObject).setVisibility(i1);
    RelativeLayout.LayoutParams localLayoutParams = new android/widget/RelativeLayout$LayoutParams;
    int i3 = Dips.dipsToIntPixels(paramVastCompanionAdConfig.getWidth() + 16, paramContext);
    int i4 = Dips.dipsToIntPixels(paramVastCompanionAdConfig.getHeight() + 16, paramContext);
    localLayoutParams.<init>(i3, i4);
    localLayoutParams.addRule(13, i2);
    localRelativeLayout.addView((View)localObject, localLayoutParams);
    b.registerVideoObstruction((View)localObject);
    return (View)localObject;
  }
  
  private o b(Context paramContext, VastCompanionAdConfig paramVastCompanionAdConfig)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramVastCompanionAdConfig);
    Object localObject1 = paramVastCompanionAdConfig.getVastResource();
    localObject1 = o.a(paramContext, (n)localObject1);
    Object localObject2 = new com/mopub/mobileads/VastVideoViewController$11;
    ((VastVideoViewController.11)localObject2).<init>(this, paramVastCompanionAdConfig, paramContext);
    b = ((o.a)localObject2);
    localObject2 = new com/mopub/mobileads/VastVideoViewController$2;
    ((VastVideoViewController.2)localObject2).<init>(this, paramVastCompanionAdConfig, paramContext);
    ((o)localObject1).setWebViewClient((WebViewClient)localObject2);
    return (o)localObject1;
  }
  
  private void c()
  {
    v.stop();
    w.stop();
  }
  
  final View a(Context paramContext, VastCompanionAdConfig paramVastCompanionAdConfig, int paramInt1, int paramInt2, View paramView, int paramInt3, int paramInt4)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramView);
    if (paramVastCompanionAdConfig == null)
    {
      paramVastCompanionAdConfig = new android/view/View;
      paramVastCompanionAdConfig.<init>(paramContext);
      paramVastCompanionAdConfig.setVisibility(4);
      return paramVastCompanionAdConfig;
    }
    C = true;
    Object localObject = o;
    boolean bool = C;
    ((VastVideoCtaButtonWidget)localObject).setHasSocialActions(bool);
    localObject = b(paramContext, paramVastCompanionAdConfig);
    int i1 = Dips.dipsToIntPixels(paramVastCompanionAdConfig.getWidth(), paramContext);
    int i2 = Dips.dipsToIntPixels(paramVastCompanionAdConfig.getHeight(), paramContext);
    paramInt1 = (paramInt1 - i2) / 2;
    paramInt4 = Dips.dipsToIntPixels(paramInt4, paramContext);
    RelativeLayout.LayoutParams localLayoutParams = new android/widget/RelativeLayout$LayoutParams;
    localLayoutParams.<init>(i1, i2);
    i2 = paramView.getId();
    localLayoutParams.addRule(paramInt2, i2);
    paramInt2 = paramView.getId();
    localLayoutParams.addRule(6, paramInt2);
    localLayoutParams.setMargins(paramInt4, paramInt1, 0, 0);
    paramVastCompanionAdConfig = new android/widget/RelativeLayout;
    paramVastCompanionAdConfig.<init>(paramContext);
    paramVastCompanionAdConfig.setGravity(16);
    paramContext = new android/widget/RelativeLayout$LayoutParams;
    paramInt1 = -2;
    paramContext.<init>(paramInt1, paramInt1);
    paramVastCompanionAdConfig.addView((View)localObject, paramContext);
    b.registerVideoObstruction((View)localObject);
    getLayout().addView(paramVastCompanionAdConfig, localLayoutParams);
    b.registerVideoObstruction(paramVastCompanionAdConfig);
    ((o)localObject).setVisibility(paramInt3);
    return (View)localObject;
  }
  
  final void a()
  {
    boolean bool = true;
    i = bool;
    d.setVisibility(8);
    p.setVisibility(0);
    VastVideoCtaButtonWidget localVastVideoCtaButtonWidget = o;
    b = bool;
    localVastVideoCtaButtonWidget.a();
    u.setVisibility(0);
  }
  
  final void a(int paramInt1, int paramInt2)
  {
    int i1 = 1;
    if (paramInt1 == i1)
    {
      paramInt1 = -1;
      if (paramInt2 == paramInt1)
      {
        BaseVideoViewController.BaseVideoViewControllerListener localBaseVideoViewControllerListener = mBaseVideoViewControllerListener;
        localBaseVideoViewControllerListener.onFinish();
      }
    }
  }
  
  final String b()
  {
    VastVideoConfig localVastVideoConfig = k;
    if (localVastVideoConfig == null) {
      return null;
    }
    return localVastVideoConfig.getNetworkMediaFileUrl();
  }
  
  public boolean backButtonEnabled()
  {
    return i;
  }
  
  protected final VideoView getVideoView()
  {
    return a;
  }
  
  protected final void onBackPressed()
  {
    boolean bool = z;
    if (!bool)
    {
      ExternalViewabilitySessionManager localExternalViewabilitySessionManager = b;
      ExternalViewabilitySession.VideoEvent localVideoEvent = ExternalViewabilitySession.VideoEvent.AD_SKIPPED;
      VastVideoView localVastVideoView = a;
      int i1 = localVastVideoView.getCurrentPosition();
      localExternalViewabilitySessionManager.recordVideoEvent(localVideoEvent, i1);
    }
  }
  
  protected final void onConfigurationChanged(Configuration paramConfiguration)
  {
    paramConfiguration = mContext.getResources().getConfiguration();
    int i1 = orientation;
    Object localObject = k.getVastCompanionAd(i1);
    q = ((VastCompanionAdConfig)localObject);
    localObject = r;
    int i2 = ((View)localObject).getVisibility();
    if (i2 != 0)
    {
      localObject = s;
      i2 = ((View)localObject).getVisibility();
      if (i2 != 0) {}
    }
    else
    {
      i2 = 1;
      int i3 = 0;
      int i4 = 4;
      if (i1 == i2)
      {
        r.setVisibility(i4);
        paramConfiguration = s;
        paramConfiguration.setVisibility(0);
      }
      else
      {
        s.setVisibility(i4);
        paramConfiguration = r;
        paramConfiguration.setVisibility(0);
      }
      paramConfiguration = q;
      if (paramConfiguration != null)
      {
        localObject = mContext;
        i3 = D;
        paramConfiguration.a((Context)localObject, i3);
      }
    }
  }
  
  protected final void onCreate()
  {
    super.onCreate();
    Object localObject1 = VastVideoViewController.3.a;
    Object localObject2 = k.getCustomForceOrientation();
    int i1 = ((DeviceUtils.ForceOrientation)localObject2).ordinal();
    int i2 = localObject1[i1];
    switch (i2)
    {
    default: 
      break;
    case 2: 
      localObject1 = mBaseVideoViewControllerListener;
      i1 = 6;
      ((BaseVideoViewController.BaseVideoViewControllerListener)localObject1).onSetRequestedOrientation(i1);
      break;
    case 1: 
      localObject1 = mBaseVideoViewControllerListener;
      i1 = 1;
      ((BaseVideoViewController.BaseVideoViewControllerListener)localObject1).onSetRequestedOrientation(i1);
    }
    localObject1 = k;
    localObject2 = mContext;
    int i3 = a.getCurrentPosition();
    ((VastVideoConfig)localObject1).handleImpression((Context)localObject2, i3);
    a("com.mopub.action.interstitial.show");
  }
  
  protected final void onDestroy()
  {
    c();
    ExternalViewabilitySessionManager localExternalViewabilitySessionManager = b;
    ExternalViewabilitySession.VideoEvent localVideoEvent = ExternalViewabilitySession.VideoEvent.AD_STOPPED;
    int i1 = a.getCurrentPosition();
    localExternalViewabilitySessionManager.recordVideoEvent(localVideoEvent, i1);
    b.endVideoSession();
    a("com.mopub.action.interstitial.dismiss");
    a.onDestroy();
  }
  
  protected final void onPause()
  {
    c();
    int i1 = a.getCurrentPosition();
    y = i1;
    Object localObject1 = a;
    ((VastVideoView)localObject1).pause();
    boolean bool = z;
    if (!bool)
    {
      bool = E;
      if (!bool)
      {
        localObject1 = b;
        Object localObject2 = ExternalViewabilitySession.VideoEvent.AD_PAUSED;
        VastVideoView localVastVideoView = a;
        int i2 = localVastVideoView.getCurrentPosition();
        ((ExternalViewabilitySessionManager)localObject1).recordVideoEvent((ExternalViewabilitySession.VideoEvent)localObject2, i2);
        localObject1 = k;
        localObject2 = mContext;
        i2 = y;
        ((VastVideoConfig)localObject1).handlePause((Context)localObject2, i2);
      }
    }
  }
  
  protected final void onResume()
  {
    v.startRepeating(50);
    Object localObject1 = w;
    long l1 = 250L;
    ((VastVideoViewCountdownRunnable)localObject1).startRepeating(l1);
    int i1 = y;
    Object localObject2;
    int i3;
    if (i1 > 0)
    {
      localObject1 = b;
      localObject2 = ExternalViewabilitySession.VideoEvent.AD_PLAYING;
      i3 = y;
      ((ExternalViewabilitySessionManager)localObject1).recordVideoEvent((ExternalViewabilitySession.VideoEvent)localObject2, i3);
      localObject1 = a;
      i4 = y;
      ((VastVideoView)localObject1).seekTo(i4);
    }
    else
    {
      localObject1 = b;
      localObject2 = ExternalViewabilitySession.VideoEvent.AD_LOADED;
      VastVideoView localVastVideoView = a;
      i3 = localVastVideoView.getCurrentPosition();
      ((ExternalViewabilitySessionManager)localObject1).recordVideoEvent((ExternalViewabilitySession.VideoEvent)localObject2, i3);
    }
    boolean bool = z;
    if (!bool)
    {
      localObject1 = a;
      ((VastVideoView)localObject1).start();
    }
    int i2 = y;
    int i4 = -1;
    if (i2 != i4)
    {
      localObject1 = k;
      localObject2 = mContext;
      i3 = y;
      ((VastVideoConfig)localObject1).handleResume((Context)localObject2, i3);
    }
  }
  
  protected final void onSaveInstanceState(Bundle paramBundle)
  {
    int i1 = y;
    paramBundle.putInt("current_position", i1);
    VastVideoConfig localVastVideoConfig = k;
    paramBundle.putSerializable("resumed_vast_config", localVastVideoConfig);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastVideoViewController
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */