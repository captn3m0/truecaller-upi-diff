package com.mopub.mobileads;

import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask.Status;
import android.widget.ImageView;
import android.widget.VideoView;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.AsyncTasks;

public class VastVideoView
  extends VideoView
{
  private VastVideoBlurLastVideoFrameTask a;
  private MediaMetadataRetriever b;
  
  public VastVideoView(Context paramContext)
  {
    super(paramContext);
    Preconditions.checkNotNull(paramContext, "context cannot be null");
    paramContext = new android/media/MediaMetadataRetriever;
    paramContext.<init>();
    b = paramContext;
  }
  
  VastVideoBlurLastVideoFrameTask getBlurLastVideoFrameTask()
  {
    return a;
  }
  
  public void onDestroy()
  {
    Object localObject = a;
    if (localObject != null)
    {
      localObject = ((VastVideoBlurLastVideoFrameTask)localObject).getStatus();
      AsyncTask.Status localStatus = AsyncTask.Status.FINISHED;
      if (localObject != localStatus)
      {
        localObject = a;
        boolean bool = true;
        ((VastVideoBlurLastVideoFrameTask)localObject).cancel(bool);
      }
    }
  }
  
  public void prepareBlurredLastVideoFrame(ImageView paramImageView, String paramString)
  {
    Object localObject = b;
    if (localObject != null)
    {
      VastVideoBlurLastVideoFrameTask localVastVideoBlurLastVideoFrameTask = new com/mopub/mobileads/VastVideoBlurLastVideoFrameTask;
      int i = getDuration();
      localVastVideoBlurLastVideoFrameTask.<init>((MediaMetadataRetriever)localObject, paramImageView, i);
      a = localVastVideoBlurLastVideoFrameTask;
      try
      {
        paramImageView = a;
        int j = 1;
        localObject = new String[j];
        localVastVideoBlurLastVideoFrameTask = null;
        localObject[0] = paramString;
        AsyncTasks.safeExecuteOnExecutor(paramImageView, (Object[])localObject);
        return;
      }
      catch (Exception paramImageView)
      {
        paramString = "Failed to blur last video frame";
        MoPubLog.d(paramString, paramImageView);
      }
    }
  }
  
  void setBlurLastVideoFrameTask(VastVideoBlurLastVideoFrameTask paramVastVideoBlurLastVideoFrameTask)
  {
    a = paramVastVideoBlurLastVideoFrameTask;
  }
  
  void setMediaMetadataRetriever(MediaMetadataRetriever paramMediaMetadataRetriever)
  {
    b = paramMediaMetadataRetriever;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastVideoView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */