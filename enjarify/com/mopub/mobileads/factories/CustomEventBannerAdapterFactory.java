package com.mopub.mobileads.factories;

import com.mopub.common.AdReport;
import com.mopub.mobileads.CustomEventBannerAdapter;
import com.mopub.mobileads.MoPubView;
import java.util.Map;

public class CustomEventBannerAdapterFactory
{
  protected static CustomEventBannerAdapterFactory a;
  
  static
  {
    CustomEventBannerAdapterFactory localCustomEventBannerAdapterFactory = new com/mopub/mobileads/factories/CustomEventBannerAdapterFactory;
    localCustomEventBannerAdapterFactory.<init>();
    a = localCustomEventBannerAdapterFactory;
  }
  
  public static CustomEventBannerAdapter create(MoPubView paramMoPubView, String paramString, Map paramMap, long paramLong, AdReport paramAdReport)
  {
    CustomEventBannerAdapter localCustomEventBannerAdapter = new com/mopub/mobileads/CustomEventBannerAdapter;
    localCustomEventBannerAdapter.<init>(paramMoPubView, paramString, paramMap, paramLong, paramAdReport);
    return localCustomEventBannerAdapter;
  }
  
  public static void setInstance(CustomEventBannerAdapterFactory paramCustomEventBannerAdapterFactory)
  {
    a = paramCustomEventBannerAdapterFactory;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.factories.CustomEventBannerAdapterFactory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */