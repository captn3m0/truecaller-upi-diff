package com.mopub.mobileads.factories;

import android.content.Context;
import com.mopub.mobileads.MoPubView;

public class MoPubViewFactory
{
  protected static MoPubViewFactory a;
  
  static
  {
    MoPubViewFactory localMoPubViewFactory = new com/mopub/mobileads/factories/MoPubViewFactory;
    localMoPubViewFactory.<init>();
    a = localMoPubViewFactory;
  }
  
  public static MoPubView create(Context paramContext)
  {
    MoPubView localMoPubView = new com/mopub/mobileads/MoPubView;
    localMoPubView.<init>(paramContext);
    return localMoPubView;
  }
  
  public static void setInstance(MoPubViewFactory paramMoPubViewFactory)
  {
    a = paramMoPubViewFactory;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.factories.MoPubViewFactory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */