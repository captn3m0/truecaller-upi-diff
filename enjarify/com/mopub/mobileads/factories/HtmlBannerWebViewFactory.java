package com.mopub.mobileads.factories;

import android.content.Context;
import com.mopub.common.AdReport;
import com.mopub.mobileads.CustomEventBanner.CustomEventBannerListener;
import com.mopub.mobileads.HtmlBannerWebView;

public class HtmlBannerWebViewFactory
{
  protected static HtmlBannerWebViewFactory a;
  
  static
  {
    HtmlBannerWebViewFactory localHtmlBannerWebViewFactory = new com/mopub/mobileads/factories/HtmlBannerWebViewFactory;
    localHtmlBannerWebViewFactory.<init>();
    a = localHtmlBannerWebViewFactory;
  }
  
  public static HtmlBannerWebView create(Context paramContext, AdReport paramAdReport, CustomEventBanner.CustomEventBannerListener paramCustomEventBannerListener, String paramString)
  {
    return a.internalCreate(paramContext, paramAdReport, paramCustomEventBannerListener, paramString);
  }
  
  public static void setInstance(HtmlBannerWebViewFactory paramHtmlBannerWebViewFactory)
  {
    a = paramHtmlBannerWebViewFactory;
  }
  
  public HtmlBannerWebView internalCreate(Context paramContext, AdReport paramAdReport, CustomEventBanner.CustomEventBannerListener paramCustomEventBannerListener, String paramString)
  {
    HtmlBannerWebView localHtmlBannerWebView = new com/mopub/mobileads/HtmlBannerWebView;
    localHtmlBannerWebView.<init>(paramContext, paramAdReport);
    paramContext = paramAdReport.getDspCreativeId();
    localHtmlBannerWebView.init(paramCustomEventBannerListener, paramString, paramContext);
    return localHtmlBannerWebView;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.factories.HtmlBannerWebViewFactory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */