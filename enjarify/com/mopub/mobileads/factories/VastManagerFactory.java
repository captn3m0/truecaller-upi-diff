package com.mopub.mobileads.factories;

import android.content.Context;
import com.mopub.mobileads.VastManager;

public class VastManagerFactory
{
  protected static VastManagerFactory a;
  
  static
  {
    VastManagerFactory localVastManagerFactory = new com/mopub/mobileads/factories/VastManagerFactory;
    localVastManagerFactory.<init>();
    a = localVastManagerFactory;
  }
  
  public static VastManager create(Context paramContext)
  {
    return a.internalCreate(paramContext, true);
  }
  
  public static VastManager create(Context paramContext, boolean paramBoolean)
  {
    return a.internalCreate(paramContext, paramBoolean);
  }
  
  public static void setInstance(VastManagerFactory paramVastManagerFactory)
  {
    a = paramVastManagerFactory;
  }
  
  public VastManager internalCreate(Context paramContext, boolean paramBoolean)
  {
    VastManager localVastManager = new com/mopub/mobileads/VastManager;
    localVastManager.<init>(paramContext, paramBoolean);
    return localVastManager;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.factories.VastManagerFactory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */