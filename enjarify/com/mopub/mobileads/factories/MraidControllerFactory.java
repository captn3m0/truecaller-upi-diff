package com.mopub.mobileads.factories;

import android.content.Context;
import com.mopub.common.AdReport;
import com.mopub.mraid.MraidController;
import com.mopub.mraid.PlacementType;

public class MraidControllerFactory
{
  protected static MraidControllerFactory a;
  
  static
  {
    MraidControllerFactory localMraidControllerFactory = new com/mopub/mobileads/factories/MraidControllerFactory;
    localMraidControllerFactory.<init>();
    a = localMraidControllerFactory;
  }
  
  public static MraidController create(Context paramContext, AdReport paramAdReport, PlacementType paramPlacementType)
  {
    MraidController localMraidController = new com/mopub/mraid/MraidController;
    localMraidController.<init>(paramContext, paramAdReport, paramPlacementType);
    return localMraidController;
  }
  
  public static void setInstance(MraidControllerFactory paramMraidControllerFactory)
  {
    a = paramMraidControllerFactory;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.factories.MraidControllerFactory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */