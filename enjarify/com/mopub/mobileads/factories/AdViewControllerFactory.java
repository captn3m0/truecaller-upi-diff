package com.mopub.mobileads.factories;

import android.content.Context;
import com.mopub.mobileads.AdViewController;
import com.mopub.mobileads.MoPubView;

public class AdViewControllerFactory
{
  protected static AdViewControllerFactory a;
  
  static
  {
    AdViewControllerFactory localAdViewControllerFactory = new com/mopub/mobileads/factories/AdViewControllerFactory;
    localAdViewControllerFactory.<init>();
    a = localAdViewControllerFactory;
  }
  
  public static AdViewController create(Context paramContext, MoPubView paramMoPubView)
  {
    AdViewController localAdViewController = new com/mopub/mobileads/AdViewController;
    localAdViewController.<init>(paramContext, paramMoPubView);
    return localAdViewController;
  }
  
  public static void setInstance(AdViewControllerFactory paramAdViewControllerFactory)
  {
    a = paramAdViewControllerFactory;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.factories.AdViewControllerFactory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */