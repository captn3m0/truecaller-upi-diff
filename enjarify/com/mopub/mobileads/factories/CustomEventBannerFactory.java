package com.mopub.mobileads.factories;

import com.mopub.mobileads.CustomEventBanner;
import java.lang.reflect.Constructor;

public class CustomEventBannerFactory
{
  private static CustomEventBannerFactory a;
  
  static
  {
    CustomEventBannerFactory localCustomEventBannerFactory = new com/mopub/mobileads/factories/CustomEventBannerFactory;
    localCustomEventBannerFactory.<init>();
    a = localCustomEventBannerFactory;
  }
  
  public static CustomEventBanner create(String paramString)
  {
    paramString = Class.forName(paramString).asSubclass(CustomEventBanner.class).getDeclaredConstructor(null);
    paramString.setAccessible(true);
    Object[] arrayOfObject = new Object[0];
    return (CustomEventBanner)paramString.newInstance(arrayOfObject);
  }
  
  public static void setInstance(CustomEventBannerFactory paramCustomEventBannerFactory)
  {
    a = paramCustomEventBannerFactory;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.factories.CustomEventBannerFactory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */