package com.mopub.mobileads.base;

public final class R$drawable
{
  public static final int notification_action_background = 2131234779;
  public static final int notification_bg = 2131234780;
  public static final int notification_bg_low = 2131234781;
  public static final int notification_bg_low_normal = 2131234782;
  public static final int notification_bg_low_pressed = 2131234783;
  public static final int notification_bg_normal = 2131234784;
  public static final int notification_bg_normal_pressed = 2131234785;
  public static final int notification_icon_background = 2131234786;
  public static final int notification_template_icon_bg = 2131234788;
  public static final int notification_template_icon_low_bg = 2131234789;
  public static final int notification_tile_bg = 2131234790;
  public static final int notify_panel_notification_icon_bg = 2131234791;
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.base.R.drawable
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */