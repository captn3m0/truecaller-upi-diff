package com.mopub.mobileads.base;

public final class BuildConfig
{
  public static final String APPLICATION_ID = "com.mopub.mobileads.base";
  public static final String BUILD_TYPE = "release";
  public static final boolean DEBUG = false;
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 65;
  public static final String VERSION_NAME = "5.4.1";
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.base.BuildConfig
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */