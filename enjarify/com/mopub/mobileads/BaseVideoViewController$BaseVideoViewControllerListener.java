package com.mopub.mobileads;

import android.os.Bundle;
import android.view.View;

public abstract interface BaseVideoViewController$BaseVideoViewControllerListener
{
  public abstract void onFinish();
  
  public abstract void onSetContentView(View paramView);
  
  public abstract void onSetRequestedOrientation(int paramInt);
  
  public abstract void onStartActivityForResult(Class paramClass, int paramInt, Bundle paramBundle);
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.BaseVideoViewController.BaseVideoViewControllerListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */