package com.mopub.mobileads;

import android.content.Context;
import com.mopub.common.AdReport;

public class HtmlBannerWebView
  extends BaseHtmlWebView
{
  public static final String EXTRA_AD_CLICK_DATA = "com.mopub.intent.extra.AD_CLICK_DATA";
  
  public HtmlBannerWebView(Context paramContext, AdReport paramAdReport)
  {
    super(paramContext, paramAdReport);
  }
  
  public void init(CustomEventBanner.CustomEventBannerListener paramCustomEventBannerListener, String paramString1, String paramString2)
  {
    super.init();
    d locald = new com/mopub/mobileads/d;
    HtmlBannerWebView.a locala = new com/mopub/mobileads/HtmlBannerWebView$a;
    locala.<init>(paramCustomEventBannerListener);
    locald.<init>(locala, this, paramString1, paramString2);
    setWebViewClient(locald);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.HtmlBannerWebView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */