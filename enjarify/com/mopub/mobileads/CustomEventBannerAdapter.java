package com.mopub.mobileads;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import com.mopub.common.AdReport;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.mobileads.factories.CustomEventBannerFactory;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.TreeMap;

public class CustomEventBannerAdapter
  implements CustomEventBanner.CustomEventBannerListener
{
  public static final int DEFAULT_BANNER_TIMEOUT_DELAY = 10000;
  private boolean a;
  private MoPubView b;
  private Context c;
  private CustomEventBanner d;
  private Map e;
  private Map f;
  private final Handler g;
  private final Runnable h;
  private int i;
  private int j;
  private boolean k;
  private b l;
  
  public CustomEventBannerAdapter(MoPubView paramMoPubView, String paramString, Map paramMap, long paramLong, AdReport paramAdReport)
  {
    int m = -1 << -1;
    i = m;
    j = m;
    m = 0;
    k = false;
    Preconditions.checkNotNull(paramMap);
    Object localObject = new android/os/Handler;
    ((Handler)localObject).<init>();
    g = ((Handler)localObject);
    b = paramMoPubView;
    paramMoPubView = paramMoPubView.getContext();
    c = paramMoPubView;
    paramMoPubView = new com/mopub/mobileads/CustomEventBannerAdapter$1;
    paramMoPubView.<init>(this);
    h = paramMoPubView;
    localObject = String.valueOf(paramString);
    paramMoPubView = "Attempting to invoke custom event: ".concat((String)localObject);
    MoPubLog.d(paramMoPubView);
    try
    {
      paramMoPubView = CustomEventBannerFactory.create(paramString);
      d = paramMoPubView;
      paramMoPubView = new java/util/TreeMap;
      paramMoPubView.<init>(paramMap);
      f = paramMoPubView;
      paramMoPubView = (String)f.get("banner-impression-min-pixels");
      paramString = f;
      paramMap = "banner-impression-min-ms";
      paramString = (String)paramString.get(paramMap);
      boolean bool = TextUtils.isEmpty(paramMoPubView);
      if (!bool)
      {
        bool = TextUtils.isEmpty(paramString);
        if (!bool)
        {
          try
          {
            n = Integer.parseInt(paramMoPubView);
            i = n;
          }
          catch (NumberFormatException localNumberFormatException1)
          {
            paramMoPubView = "Cannot parse integer from header banner-impression-min-pixels";
            MoPubLog.d(paramMoPubView);
          }
          try
          {
            n = Integer.parseInt(paramString);
            j = n;
          }
          catch (NumberFormatException localNumberFormatException2)
          {
            paramMoPubView = "Cannot parse integer from header banner-impression-min-ms";
            MoPubLog.d(paramMoPubView);
          }
          int n = i;
          if (n > 0)
          {
            n = j;
            if (n >= 0)
            {
              n = 1;
              k = n;
            }
          }
        }
      }
      paramMoPubView = b.getLocalExtras();
      e = paramMoPubView;
      paramMoPubView = b.getLocation();
      if (paramMoPubView != null)
      {
        paramMoPubView = e;
        paramString = "location";
        paramMap = b.getLocation();
        paramMoPubView.put(paramString, paramMap);
      }
      paramMoPubView = e;
      paramMap = Long.valueOf(paramLong);
      paramMoPubView.put("broadcastIdentifier", paramMap);
      e.put("mopub-intent-ad-report", paramAdReport);
      paramMoPubView = e;
      paramMap = Integer.valueOf(b.getAdWidth());
      paramMoPubView.put("com_mopub_ad_width", paramMap);
      paramMoPubView = e;
      paramMap = Integer.valueOf(b.getAdHeight());
      paramMoPubView.put("com_mopub_ad_height", paramMap);
      paramMoPubView = e;
      paramMap = Boolean.valueOf(k);
      paramMoPubView.put("banner-impression-pixel-count-enabled", paramMap);
      return;
    }
    catch (Exception localException)
    {
      paramMoPubView = new java/lang/StringBuilder;
      paramMoPubView.<init>("Couldn't locate or instantiate custom event: ");
      paramMoPubView.append(paramString);
      paramMoPubView.append(".");
      MoPubLog.d(paramMoPubView.toString());
      paramMoPubView = b;
      paramString = MoPubErrorCode.ADAPTER_NOT_FOUND;
      paramMoPubView.a(paramString);
    }
  }
  
  private void a()
  {
    Handler localHandler = g;
    Runnable localRunnable = h;
    localHandler.removeCallbacks(localRunnable);
  }
  
  void invalidate()
  {
    CustomEventBanner localCustomEventBanner = d;
    if (localCustomEventBanner != null) {
      try
      {
        localCustomEventBanner.onInvalidate();
      }
      catch (Exception localException1)
      {
        str = "Invalidating a custom event banner threw an exception";
        MoPubLog.d(str, localException1);
      }
    }
    b localb = l;
    String str = null;
    if (localb != null)
    {
      try
      {
        localObject = g;
        boolean bool = false;
        ViewTreeObserver.OnPreDrawListener localOnPreDrawListener = null;
        ((Handler)localObject).removeMessages(0);
        h = false;
        localObject = b;
        localObject = ((WeakReference)localObject).get();
        localObject = (ViewTreeObserver)localObject;
        if (localObject != null)
        {
          bool = ((ViewTreeObserver)localObject).isAlive();
          if (bool)
          {
            localOnPreDrawListener = a;
            ((ViewTreeObserver)localObject).removeOnPreDrawListener(localOnPreDrawListener);
          }
        }
        localObject = b;
        ((WeakReference)localObject).clear();
        f = null;
      }
      catch (Exception localException2)
      {
        Object localObject = "Destroying a banner visibility tracker threw an exception";
        MoPubLog.d((String)localObject, localException2);
      }
      l = null;
    }
    c = null;
    d = null;
    e = null;
    f = null;
    a = true;
  }
  
  void loadAd()
  {
    boolean bool = a;
    if (!bool)
    {
      Object localObject1 = d;
      if (localObject1 != null)
      {
        localObject1 = g;
        Object localObject2 = h;
        Object localObject3 = b;
        int m;
        if (localObject3 == null)
        {
          m = 10000;
        }
        else
        {
          localObject3 = ((MoPubView)localObject3).getAdTimeoutDelay$61acf5d5();
          m = ((Integer)localObject3).intValue();
        }
        long l1 = m;
        ((Handler)localObject1).postDelayed((Runnable)localObject2, l1);
        try
        {
          localObject1 = d;
          localObject2 = c;
          localObject3 = e;
          Map localMap = f;
          ((CustomEventBanner)localObject1).loadBanner((Context)localObject2, this, (Map)localObject3, localMap);
          return;
        }
        catch (Exception localException)
        {
          MoPubLog.d("Loading a custom event banner threw an exception.", localException);
          MoPubErrorCode localMoPubErrorCode = MoPubErrorCode.INTERNAL_ERROR;
          onBannerFailed(localMoPubErrorCode);
          return;
        }
      }
    }
  }
  
  public void onBannerClicked()
  {
    boolean bool = a;
    if (bool) {
      return;
    }
    MoPubView localMoPubView = b;
    if (localMoPubView != null) {
      localMoPubView.a();
    }
  }
  
  public void onBannerCollapsed()
  {
    boolean bool = a;
    if (bool) {
      return;
    }
    b.g();
    MoPubView localMoPubView = b;
    MoPubView.BannerAdListener localBannerAdListener = c;
    if (localBannerAdListener != null)
    {
      localBannerAdListener = c;
      localBannerAdListener.onBannerCollapsed(localMoPubView);
    }
  }
  
  public void onBannerExpanded()
  {
    boolean bool = a;
    if (bool) {
      return;
    }
    b.f();
    MoPubView localMoPubView = b;
    MoPubView.BannerAdListener localBannerAdListener = c;
    if (localBannerAdListener != null)
    {
      localBannerAdListener = c;
      localBannerAdListener.onBannerExpanded(localMoPubView);
    }
  }
  
  public void onBannerFailed(MoPubErrorCode paramMoPubErrorCode)
  {
    boolean bool = a;
    if (bool) {
      return;
    }
    a();
    MoPubView localMoPubView = b;
    if (localMoPubView != null)
    {
      if (paramMoPubErrorCode == null) {
        paramMoPubErrorCode = MoPubErrorCode.UNSPECIFIED;
      }
      localMoPubView = b;
      localMoPubView.a(paramMoPubErrorCode);
    }
  }
  
  public void onBannerImpression()
  {
    boolean bool = a;
    if (bool) {
      return;
    }
    Object localObject = b;
    if (localObject != null)
    {
      localObject = d;
      if (localObject != null)
      {
        bool = a;
        if (!bool)
        {
          localObject = b;
          ((MoPubView)localObject).b();
          bool = k;
          if (bool)
          {
            localObject = d;
            ((CustomEventBanner)localObject).trackMpxAndThirdPartyImpressions();
          }
        }
      }
    }
  }
  
  public void onBannerLoaded(View paramView)
  {
    boolean bool1 = a;
    if (bool1) {
      return;
    }
    a();
    Object localObject1 = b;
    if (localObject1 != null)
    {
      ((MoPubView)localObject1).c();
      bool1 = k;
      if (bool1)
      {
        localObject1 = d;
        if (localObject1 != null)
        {
          bool1 = a;
          if (bool1)
          {
            b.d();
            localObject1 = new com/mopub/mobileads/b;
            Context localContext = c;
            MoPubView localMoPubView = b;
            int m = i;
            int n = j;
            Object localObject2 = localObject1;
            ((b)localObject1).<init>(localContext, localMoPubView, paramView, m, n);
            l = ((b)localObject1);
            localObject1 = l;
            localObject2 = new com/mopub/mobileads/CustomEventBannerAdapter$2;
            ((CustomEventBannerAdapter.2)localObject2).<init>(this);
            f = ((b.c)localObject2);
          }
        }
      }
      localObject1 = b;
      ((MoPubView)localObject1).setAdContentView(paramView);
      bool1 = k;
      if (!bool1)
      {
        localObject1 = d;
        if (localObject1 != null)
        {
          bool1 = a;
          if (bool1)
          {
            boolean bool2 = paramView instanceof HtmlBannerWebView;
            if (!bool2)
            {
              paramView = b;
              paramView.b();
            }
          }
        }
      }
    }
  }
  
  public void onLeaveApplication()
  {
    onBannerClicked();
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.CustomEventBannerAdapter
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */