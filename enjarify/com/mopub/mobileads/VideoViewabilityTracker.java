package com.mopub.mobileads;

public class VideoViewabilityTracker
  extends VastTracker
{
  private final int a;
  private final int b;
  
  public VideoViewabilityTracker(int paramInt1, int paramInt2, String paramString)
  {
    super(paramString);
    a = paramInt1;
    b = paramInt2;
  }
  
  public int getPercentViewable()
  {
    return b;
  }
  
  public int getViewablePlaytimeMS()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VideoViewabilityTracker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */