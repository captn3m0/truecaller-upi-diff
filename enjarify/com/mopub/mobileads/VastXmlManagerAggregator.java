package com.mopub.mobileads;

import android.content.Context;
import android.graphics.Point;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.view.Display;
import android.view.WindowManager;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Dips;
import com.mopub.mobileads.util.XmlUtils;
import com.mopub.network.Networking;
import com.mopub.network.TrackingRequest;
import java.io.StringReader;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

public class VastXmlManagerAggregator
  extends AsyncTask
{
  public static final String ADS_BY_AD_SLOT_ID = "adsBy";
  public static final String SOCIAL_ACTIONS_AD_SLOT_ID = "socialActions";
  private static final List a = Arrays.asList(new String[] { "video/mp4", "video/3gpp" });
  private final WeakReference b;
  private final double c;
  private final int d;
  private final Context e;
  private int f;
  
  VastXmlManagerAggregator(VastXmlManagerAggregator.b paramb, double paramDouble, int paramInt, Context paramContext)
  {
    Preconditions.checkNotNull(paramb);
    Preconditions.checkNotNull(paramContext);
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramb);
    b = localWeakReference;
    c = paramDouble;
    d = paramInt;
    paramb = paramContext.getApplicationContext();
    e = paramb;
  }
  
  private double a(int paramInt1, int paramInt2)
  {
    double d1 = paramInt1;
    double d2 = paramInt2;
    Double.isNaN(d1);
    Double.isNaN(d2);
    d1 /= d2;
    paramInt1 *= paramInt2;
    d2 = c;
    d1 /= d2;
    double d3 = paramInt1;
    d2 = d;
    Double.isNaN(d3);
    Double.isNaN(d2);
    d3 /= d2;
    d1 = Math.abs(Math.log(d1)) * 70.0D;
    d3 = Math.abs(Math.log(d3)) * 30.0D;
    return d1 + d3;
  }
  
  private VastCompanionAdConfig a(List paramList, VastXmlManagerAggregator.a parama)
  {
    VastXmlManagerAggregator localVastXmlManagerAggregator = this;
    Object localObject1 = paramList;
    VastXmlManagerAggregator.a locala = parama;
    Preconditions.checkNotNull(paramList, "managers cannot be null");
    Preconditions.checkNotNull(parama, "orientation cannot be null");
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>(paramList);
    localObject1 = n.b.values();
    int i = localObject1.length;
    double d1 = 1.0D / 0.0D;
    int j = 0;
    double d2 = d1;
    Object localObject3 = null;
    int k = 0;
    Object localObject4 = null;
    float f1 = 0.0F;
    Object localObject5 = null;
    Object localObject7;
    Object localObject8;
    Object localObject9;
    while (j < i)
    {
      n.b localb = localObject1[j];
      localObject6 = ((List)localObject2).iterator();
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject6).hasNext();
        if (!bool1) {
          break;
        }
        localObject7 = (h)((Iterator)localObject6).next();
        localObject8 = ((h)localObject7).a();
        localObject9 = ((h)localObject7).b();
        if (localObject8 != null)
        {
          int m = ((Integer)localObject8).intValue();
          localObject10 = localObject1;
          int n = 300;
          float f2 = 4.2E-43F;
          if ((m >= n) && (localObject9 != null))
          {
            n = ((Integer)localObject9).intValue();
            m = 250;
            float f3 = 3.5E-43F;
            if (n >= m)
            {
              n = ((Integer)localObject8).intValue();
              m = ((Integer)localObject9).intValue();
              localObject11 = localObject2;
              localObject2 = new android/graphics/Point;
              ((Point)localObject2).<init>(n, m);
              Object localObject12 = localObject2;
              localObject2 = e;
              i1 = i;
              Object localObject13 = "window";
              localObject2 = ((WindowManager)((Context)localObject2).getSystemService((String)localObject13)).getDefaultDisplay();
              i = ((Display)localObject2).getWidth();
              int i2 = ((Display)localObject2).getHeight();
              f2 = n;
              localObject14 = localObject4;
              localObject4 = e;
              n = Dips.dipsToIntPixels(f2, (Context)localObject4);
              f3 = m;
              localObject4 = e;
              m = Dips.dipsToIntPixels(f3, (Context)localObject4);
              localObject4 = VastXmlManagerAggregator.a.LANDSCAPE;
              if (localObject4 == locala)
              {
                k = Math.max(i, i2);
                i2 = Math.min(i, i2);
              }
              else
              {
                k = Math.min(i, i2);
                i2 = Math.max(i, i2);
              }
              i = k + -16;
              if (n <= i)
              {
                i = i2 + -16;
                if (m <= i)
                {
                  localObject15 = localObject5;
                  localObject16 = localObject6;
                  break label704;
                }
              }
              localObject13 = new android/graphics/Point;
              ((Point)localObject13).<init>();
              localObject15 = localObject5;
              localObject5 = n.b.HTML_RESOURCE;
              if (localObject5 == localb)
              {
                n = Math.min(k, n);
                x = n;
                n = Math.min(i2, m);
                y = n;
                localObject16 = localObject6;
              }
              else
              {
                f2 = n;
                f1 = k;
                f1 = f2 / f1;
                f3 = m;
                localObject16 = localObject6;
                float f4 = i2;
                f4 = f3 / f4;
                boolean bool3 = f1 < f4;
                if (!bool3)
                {
                  x = k;
                  f3 /= f1;
                  n = (int)f3;
                  y = n;
                }
                else
                {
                  f2 /= f4;
                  n = (int)f2;
                  x = n;
                  y = i2;
                }
              }
              n = x + -16;
              x = n;
              n = y + -16;
              y = n;
              n = x;
              if (n >= 0)
              {
                n = y;
                if (n >= 0)
                {
                  f2 = x;
                  localObject2 = e;
                  n = Dips.pixelsToIntDips(f2, (Context)localObject2);
                  x = n;
                  f2 = y;
                  localObject2 = e;
                  n = Dips.pixelsToIntDips(f2, (Context)localObject2);
                  y = n;
                  localObject5 = localObject13;
                  break label708;
                }
              }
              label704:
              localObject5 = localObject12;
              label708:
              localObject1 = b;
              i2 = x;
              i = y;
              localObject4 = n.a((VastResourceXmlManager)localObject1, localb, i2, i);
              if (localObject4 == null) {
                break label907;
              }
              localObject1 = VastXmlManagerAggregator.a.PORTRAIT;
              double d3;
              if (localObject1 == locala)
              {
                n = ((Integer)localObject9).intValue();
                i2 = ((Integer)localObject8).intValue();
                d3 = localVastXmlManagerAggregator.a(n, i2);
              }
              else
              {
                n = ((Integer)localObject8).intValue();
                i2 = ((Integer)localObject9).intValue();
                d3 = localVastXmlManagerAggregator.a(n, i2);
              }
              boolean bool2 = d3 < d2;
              if (bool2)
              {
                d2 = d3;
                localObject3 = localObject7;
              }
              else
              {
                localObject4 = localObject14;
                localObject5 = localObject15;
              }
              localObject1 = localObject10;
              localObject2 = localObject11;
              i = i1;
              localObject6 = localObject16;
              continue;
            }
            localObject11 = localObject2;
            i1 = i;
            localObject14 = localObject4;
            localObject15 = localObject5;
            localObject16 = localObject6;
            break label907;
          }
        }
        else
        {
          localObject10 = localObject1;
        }
        localObject11 = localObject2;
        i1 = i;
        localObject14 = localObject4;
        localObject15 = localObject5;
        Object localObject16 = localObject6;
        label907:
        localObject1 = localObject10;
        localObject2 = localObject11;
        i = i1;
        localObject4 = localObject14;
        localObject5 = localObject15;
        localObject6 = localObject16;
      }
      Object localObject10 = localObject1;
      Object localObject11 = localObject2;
      int i1 = i;
      Object localObject14 = localObject4;
      Object localObject15 = localObject5;
      if (localObject3 == null)
      {
        j += 1;
      }
      else
      {
        localObject6 = localObject4;
        break label979;
      }
    }
    Object localObject6 = localObject4;
    label979:
    if (localObject3 != null)
    {
      localObject1 = new com/mopub/mobileads/VastCompanionAdConfig;
      int i3 = x;
      int i4 = y;
      localObject7 = ((h)localObject3).c();
      localObject8 = ((h)localObject3).d();
      localObject9 = ((h)localObject3).e();
      ((VastCompanionAdConfig)localObject1).<init>(i3, i4, (n)localObject6, (String)localObject7, (List)localObject8, (List)localObject9);
      return (VastCompanionAdConfig)localObject1;
    }
    return null;
  }
  
  private VastVideoConfig a(j paramj, List paramList)
  {
    Preconditions.checkNotNull(paramj);
    Preconditions.checkNotNull(paramList);
    Object localObject1 = paramj.c().iterator();
    do
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      localObject2 = null;
      if (!bool1) {
        break;
      }
      localObject3 = (k)((Iterator)localObject1).next();
      localObject4 = new java/util/ArrayList;
      ((ArrayList)localObject4).<init>();
      localObject5 = a;
      Object localObject6 = "MediaFiles";
      localObject5 = XmlUtils.getFirstMatchingChildNode((Node)localObject5, (String)localObject6);
      if (localObject5 != null)
      {
        localObject6 = "MediaFile";
        localObject5 = XmlUtils.getMatchingChildNodes((Node)localObject5, (String)localObject6).iterator();
        for (;;)
        {
          boolean bool2 = ((Iterator)localObject5).hasNext();
          if (!bool2) {
            break;
          }
          localObject6 = (Node)((Iterator)localObject5).next();
          m localm = new com/mopub/mobileads/m;
          localm.<init>((Node)localObject6);
          ((List)localObject4).add(localm);
        }
      }
      localObject4 = a((List)localObject4);
    } while (localObject4 == null);
    localObject1 = new com/mopub/mobileads/VastVideoConfig;
    ((VastVideoConfig)localObject1).<init>();
    Object localObject5 = paramj.a();
    ((VastVideoConfig)localObject1).addImpressionTrackers((List)localObject5);
    a((k)localObject3, (VastVideoConfig)localObject1);
    Object localObject3 = a;
    localObject5 = "VideoClicks";
    localObject3 = XmlUtils.getFirstMatchingChildNode((Node)localObject3, (String)localObject5);
    if (localObject3 != null)
    {
      localObject3 = XmlUtils.getFirstMatchingChildNode((Node)localObject3, "ClickThrough");
      localObject2 = XmlUtils.getNodeValue((Node)localObject3);
    }
    ((VastVideoConfig)localObject1).setClickThroughUrl((String)localObject2);
    ((VastVideoConfig)localObject1).setNetworkMediaFileUrl((String)localObject4);
    localObject3 = paramj.d();
    Object localObject2 = VastXmlManagerAggregator.a.LANDSCAPE;
    localObject2 = a((List)localObject3, (VastXmlManagerAggregator.a)localObject2);
    Object localObject4 = VastXmlManagerAggregator.a.PORTRAIT;
    localObject4 = a((List)localObject3, (VastXmlManagerAggregator.a)localObject4);
    ((VastVideoConfig)localObject1).setVastCompanionAd((VastCompanionAdConfig)localObject2, (VastCompanionAdConfig)localObject4);
    localObject3 = b((List)localObject3);
    ((VastVideoConfig)localObject1).setSocialActionsCompanionAds((Map)localObject3);
    localObject3 = paramj.b();
    paramList.addAll((Collection)localObject3);
    ((VastVideoConfig)localObject1).addErrorTrackers(paramList);
    a(paramj, (VastVideoConfig)localObject1);
    b(paramj, (VastVideoConfig)localObject1);
    return (VastVideoConfig)localObject1;
    return null;
  }
  
  private VastVideoConfig a(String paramString, List paramList)
  {
    Preconditions.checkNotNull(paramString, "vastXml cannot be null");
    Preconditions.checkNotNull(paramList, "errorTrackers cannot be null");
    Object localObject1 = new com/mopub/mobileads/q;
    ((q)localObject1).<init>();
    Object localObject2 = null;
    Object localObject3 = "xmlString cannot be null";
    try
    {
      Preconditions.checkNotNull(paramString, (String)localObject3);
      localObject3 = "<\\?.*\\?>";
      String str = "";
      paramString = paramString.replaceFirst((String)localObject3, str);
      localObject3 = new java/lang/StringBuilder;
      str = "<MPMoVideoXMLDocRoot>";
      ((StringBuilder)localObject3).<init>(str);
      ((StringBuilder)localObject3).append(paramString);
      paramString = "</MPMoVideoXMLDocRoot>";
      ((StringBuilder)localObject3).append(paramString);
      paramString = ((StringBuilder)localObject3).toString();
      localObject3 = DocumentBuilderFactory.newInstance();
      int i = 1;
      ((DocumentBuilderFactory)localObject3).setCoalescing(i);
      localObject3 = ((DocumentBuilderFactory)localObject3).newDocumentBuilder();
      Object localObject4 = new org/xml/sax/InputSource;
      StringReader localStringReader = new java/io/StringReader;
      localStringReader.<init>(paramString);
      ((InputSource)localObject4).<init>(localStringReader);
      paramString = ((DocumentBuilder)localObject3).parse((InputSource)localObject4);
      a = paramString;
      paramString = ((q)localObject1).a();
      localObject3 = e;
      boolean bool1 = paramString.isEmpty();
      localStringReader = null;
      int j;
      Object localObject5;
      if (bool1)
      {
        localObject4 = ((q)localObject1).b();
        if (localObject4 != null)
        {
          localObject4 = Collections.singletonList(((q)localObject1).b());
          j = f;
          if (j > 0) {
            localObject5 = VastErrorCode.NO_ADS_VAST_RESPONSE;
          } else {
            localObject5 = VastErrorCode.UNDEFINED_ERROR;
          }
          TrackingRequest.makeVastTrackingHttpRequest((List)localObject4, (VastErrorCode)localObject5, null, null, (Context)localObject3);
          bool3 = true;
          break label248;
        }
      }
      boolean bool3 = false;
      localObject3 = null;
      label248:
      if (bool3) {
        return null;
      }
      paramString = paramString.iterator();
      do
      {
        do
        {
          do
          {
            do
            {
              bool3 = paramString.hasNext();
              if (!bool3) {
                break;
              }
              localObject3 = (f)paramString.next();
              localObject4 = a;
              localObject5 = "sequence";
              localObject4 = XmlUtils.getAttributeValue((Node)localObject4, (String)localObject5);
              bool1 = a((String)localObject4);
            } while (!bool1);
            localObject4 = a;
            localObject5 = "InLine";
            localObject4 = XmlUtils.getFirstMatchingChildNode((Node)localObject4, (String)localObject5);
            if (localObject4 != null)
            {
              localObject5 = new com/mopub/mobileads/j;
              ((j)localObject5).<init>((Node)localObject4);
            }
            else
            {
              j = 0;
              localObject5 = null;
            }
            if (localObject5 != null)
            {
              localObject4 = a((j)localObject5, paramList);
              if (localObject4 != null)
              {
                a((q)localObject1, (VastVideoConfig)localObject4);
                return (VastVideoConfig)localObject4;
              }
            }
            localObject3 = a;
            localObject4 = "Wrapper";
            localObject3 = XmlUtils.getFirstMatchingChildNode((Node)localObject3, (String)localObject4);
            if (localObject3 != null)
            {
              localObject4 = new com/mopub/mobileads/p;
              ((p)localObject4).<init>((Node)localObject3);
            }
            else
            {
              bool1 = false;
              localObject4 = null;
            }
          } while (localObject4 == null);
          localObject3 = new java/util/ArrayList;
          ((ArrayList)localObject3).<init>(paramList);
          localObject5 = ((p)localObject4).b();
          ((List)localObject3).addAll((Collection)localObject5);
          localObject5 = a((p)localObject4, (List)localObject3);
        } while (localObject5 == null);
        localObject3 = a((String)localObject5, (List)localObject3);
      } while (localObject3 == null);
      paramString = ((p)localObject4).a();
      ((VastVideoConfig)localObject3).addImpressionTrackers(paramString);
      paramString = ((p)localObject4).c().iterator();
      for (;;)
      {
        bool4 = paramString.hasNext();
        if (!bool4) {
          break;
        }
        paramList = (k)paramString.next();
        a(paramList, (VastVideoConfig)localObject3);
      }
      a((g)localObject4, (VastVideoConfig)localObject3);
      b((g)localObject4, (VastVideoConfig)localObject3);
      paramString = ((p)localObject4).d();
      boolean bool4 = ((VastVideoConfig)localObject3).hasCompanionAd();
      if (!bool4)
      {
        paramList = VastXmlManagerAggregator.a.LANDSCAPE;
        paramList = a(paramString, paramList);
        localObject2 = VastXmlManagerAggregator.a.PORTRAIT;
        localObject2 = a(paramString, (VastXmlManagerAggregator.a)localObject2);
        ((VastVideoConfig)localObject3).setVastCompanionAd(paramList, (VastCompanionAdConfig)localObject2);
      }
      else
      {
        int k = 2;
        paramList = ((VastVideoConfig)localObject3).getVastCompanionAd(k);
        localObject2 = ((VastVideoConfig)localObject3).getVastCompanionAd(i);
        if ((paramList != null) && (localObject2 != null))
        {
          localObject4 = paramString.iterator();
          for (;;)
          {
            boolean bool2 = ((Iterator)localObject4).hasNext();
            if (!bool2) {
              break;
            }
            localObject5 = (h)((Iterator)localObject4).next();
            Object localObject6 = b.a();
            boolean bool6 = TextUtils.isEmpty((CharSequence)localObject6);
            if (bool6)
            {
              localObject6 = b.c();
              bool6 = TextUtils.isEmpty((CharSequence)localObject6);
              if (bool6)
              {
                localObject6 = b.b();
                bool6 = TextUtils.isEmpty((CharSequence)localObject6);
                if (bool6)
                {
                  bool6 = false;
                  localObject6 = null;
                  break label771;
                }
              }
            }
            bool6 = true;
            label771:
            if (!bool6)
            {
              localObject6 = ((h)localObject5).d();
              paramList.addClickTrackers((List)localObject6);
              localObject6 = ((h)localObject5).e();
              paramList.addCreativeViewTrackers((List)localObject6);
              localObject6 = ((h)localObject5).d();
              ((VastCompanionAdConfig)localObject2).addClickTrackers((List)localObject6);
              localObject5 = ((h)localObject5).e();
              ((VastCompanionAdConfig)localObject2).addCreativeViewTrackers((List)localObject5);
            }
          }
        }
      }
      paramList = ((VastVideoConfig)localObject3).getSocialActionsCompanionAds();
      boolean bool5 = paramList.isEmpty();
      if (bool5)
      {
        paramString = b(paramString);
        ((VastVideoConfig)localObject3).setSocialActionsCompanionAds(paramString);
      }
      a((q)localObject1, (VastVideoConfig)localObject3);
      return (VastVideoConfig)localObject3;
      return null;
    }
    catch (Exception paramString)
    {
      MoPubLog.d("Failed to parse VAST XML", paramString);
      paramString = VastErrorCode.XML_PARSING_ERROR;
      localObject1 = e;
      TrackingRequest.makeVastTrackingHttpRequest(paramList, paramString, null, null, (Context)localObject1);
    }
    return null;
  }
  
  private VastVideoConfig a(String... paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      int i = paramVarArgs.length;
      if (i != 0)
      {
        i = 0;
        ArrayList localArrayList = null;
        String str = paramVarArgs[0];
        if (str != null) {
          try
          {
            paramVarArgs = paramVarArgs[0];
            localArrayList = new java/util/ArrayList;
            localArrayList.<init>();
            return a(paramVarArgs, localArrayList);
          }
          catch (Exception paramVarArgs)
          {
            MoPubLog.d("Unable to generate VastVideoConfig.", paramVarArgs);
            return null;
          }
        }
      }
    }
    return null;
  }
  
  private String a(p paramp, List paramList)
  {
    paramp = paramp.f();
    String str = null;
    if (paramp == null) {
      return null;
    }
    try
    {
      str = b(paramp);
    }
    catch (Exception paramp)
    {
      Object localObject = "Failed to follow VAST redirect";
      MoPubLog.d((String)localObject, paramp);
      boolean bool = paramList.isEmpty();
      if (!bool)
      {
        paramp = VastErrorCode.WRAPPER_TIMEOUT;
        localObject = e;
        TrackingRequest.makeVastTrackingHttpRequest(paramList, paramp, null, null, (Context)localObject);
      }
    }
    return str;
  }
  
  private String a(List paramList)
  {
    Preconditions.checkNotNull(paramList, "managers cannot be null");
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>(paramList);
    paramList = localArrayList.iterator();
    double d1 = 1.0D / 0.0D;
    Object localObject1 = null;
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject2 = (m)paramList.next();
      Object localObject3 = XmlUtils.getAttributeValue(a, "type");
      String str = XmlUtils.getNodeValue(a);
      Object localObject4 = a;
      boolean bool2 = ((List)localObject4).contains(localObject3);
      if ((bool2) && (str != null))
      {
        localObject3 = XmlUtils.getAttributeValueAsInt(a, "width");
        localObject2 = a;
        localObject4 = "height";
        localObject2 = XmlUtils.getAttributeValueAsInt((Node)localObject2, (String)localObject4);
        if (localObject3 != null)
        {
          int k = ((Integer)localObject3).intValue();
          if ((k > 0) && (localObject2 != null))
          {
            k = ((Integer)localObject2).intValue();
            if (k > 0)
            {
              int j = ((Integer)localObject3).intValue();
              int i = ((Integer)localObject2).intValue();
              double d2 = a(j, i);
              boolean bool3 = d2 < d1;
              if (bool3)
              {
                d1 = d2;
                localObject1 = str;
              }
            }
          }
        }
      }
      else
      {
        paramList.remove();
      }
    }
    return (String)localObject1;
  }
  
  private static void a(g paramg, VastVideoConfig paramVastVideoConfig)
  {
    Preconditions.checkNotNull(paramg);
    Preconditions.checkNotNull(paramVastVideoConfig);
    Object localObject1 = paramVastVideoConfig.getVideoViewabilityTracker();
    if (localObject1 != null) {
      return;
    }
    paramg = paramg.e();
    if (paramg != null)
    {
      paramg = paramg.a().iterator();
      Object localObject2;
      Object localObject3;
      boolean bool2;
      do
      {
        bool1 = paramg.hasNext();
        if (!bool1) {
          break;
        }
        localObject1 = (VastExtensionXmlManager)paramg.next();
        localObject2 = "MoPub";
        localObject3 = a;
        String str = "type";
        localObject3 = XmlUtils.getAttributeValue((Node)localObject3, str);
        bool2 = ((String)localObject2).equals(localObject3);
      } while (!bool2);
      paramg = XmlUtils.getFirstMatchingChildNode(a, "MoPubViewabilityTracker");
      boolean bool1 = false;
      localObject1 = null;
      if (paramg != null)
      {
        localObject2 = new com/mopub/mobileads/VideoViewabilityTrackerXmlManager;
        ((VideoViewabilityTrackerXmlManager)localObject2).<init>(paramg);
        paramg = ((VideoViewabilityTrackerXmlManager)localObject2).a();
        localObject3 = ((VideoViewabilityTrackerXmlManager)localObject2).b();
        localObject2 = XmlUtils.getNodeValue(a);
        if ((paramg != null) && (localObject3 != null))
        {
          boolean bool3 = TextUtils.isEmpty((CharSequence)localObject2);
          if (!bool3)
          {
            localObject1 = new com/mopub/mobileads/VideoViewabilityTracker;
            int i = paramg.intValue();
            int j = ((Integer)localObject3).intValue();
            ((VideoViewabilityTracker)localObject1).<init>(i, j, (String)localObject2);
          }
        }
      }
      paramVastVideoConfig.setVideoViewabilityTracker((VideoViewabilityTracker)localObject1);
      return;
    }
  }
  
  private static void a(k paramk, VastVideoConfig paramVastVideoConfig)
  {
    Preconditions.checkNotNull(paramk, "linearXmlManager cannot be null");
    Preconditions.checkNotNull(paramVastVideoConfig, "vastVideoConfig cannot be null");
    Object localObject = paramk.b();
    paramVastVideoConfig.addAbsoluteTrackers((List)localObject);
    localObject = paramk.a();
    paramVastVideoConfig.addFractionalTrackers((List)localObject);
    localObject = paramk.c();
    paramVastVideoConfig.addPauseTrackers((List)localObject);
    localObject = paramk.d();
    paramVastVideoConfig.addResumeTrackers((List)localObject);
    localObject = paramk.a("complete");
    paramVastVideoConfig.addCompleteTrackers((List)localObject);
    localObject = paramk.e();
    paramVastVideoConfig.addCloseTrackers((List)localObject);
    localObject = paramk.a("skip");
    paramVastVideoConfig.addSkipTrackers((List)localObject);
    localObject = paramk.f();
    paramVastVideoConfig.addClickTrackers((List)localObject);
    localObject = paramVastVideoConfig.getSkipOffsetString();
    if (localObject == null)
    {
      localObject = paramk.g();
      paramVastVideoConfig.setSkipOffset((String)localObject);
    }
    localObject = paramVastVideoConfig.getVastIconConfig();
    if (localObject == null)
    {
      paramk = c(paramk.h());
      paramVastVideoConfig.setVastIconConfig(paramk);
    }
  }
  
  private static void a(q paramq, VastVideoConfig paramVastVideoConfig)
  {
    Preconditions.checkNotNull(paramq, "xmlManager cannot be null");
    Preconditions.checkNotNull(paramVastVideoConfig, "vastVideoConfig cannot be null");
    Object localObject = paramq.c();
    paramVastVideoConfig.addImpressionTrackers((List)localObject);
    localObject = paramVastVideoConfig.getCustomCtaText();
    if (localObject == null)
    {
      localObject = paramq.d();
      paramVastVideoConfig.setCustomCtaText((String)localObject);
    }
    localObject = paramVastVideoConfig.getCustomSkipText();
    if (localObject == null)
    {
      localObject = paramq.e();
      paramVastVideoConfig.setCustomSkipText((String)localObject);
    }
    localObject = paramVastVideoConfig.getCustomCloseIconUrl();
    if (localObject == null)
    {
      localObject = paramq.f();
      paramVastVideoConfig.setCustomCloseIconUrl((String)localObject);
    }
    boolean bool = paramVastVideoConfig.isCustomForceOrientationSet();
    if (!bool)
    {
      paramq = paramq.g();
      paramVastVideoConfig.setCustomForceOrientation(paramq);
    }
  }
  
  private static boolean a(String paramString)
  {
    boolean bool1 = TextUtils.isEmpty(paramString);
    boolean bool2 = true;
    if (bool1) {
      return bool2;
    }
    try
    {
      int j = Integer.parseInt(paramString);
      int i = 2;
      if (j < i) {
        return bool2;
      }
      return false;
    }
    catch (NumberFormatException localNumberFormatException) {}
    return bool2;
  }
  
  /* Error */
  private String b(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic 47	com/mopub/common/Preconditions:checkNotNull	(Ljava/lang/Object;)V
    //   4: aload_0
    //   5: getfield 397	com/mopub/mobileads/VastXmlManagerAggregator:f	I
    //   8: istore_2
    //   9: aconst_null
    //   10: astore_3
    //   11: bipush 10
    //   13: istore 4
    //   15: iload_2
    //   16: iload 4
    //   18: if_icmpge +92 -> 110
    //   21: iload_2
    //   22: iconst_1
    //   23: iadd
    //   24: istore_2
    //   25: aload_0
    //   26: iload_2
    //   27: putfield 397	com/mopub/mobileads/VastXmlManagerAggregator:f	I
    //   30: aload_1
    //   31: invokestatic 671	com/mopub/common/MoPubHttpUrlConnection:getHttpUrlConnection	(Ljava/lang/String;)Ljava/net/HttpURLConnection;
    //   34: astore_1
    //   35: new 673	java/io/BufferedInputStream
    //   38: astore 5
    //   40: aload_1
    //   41: invokevirtual 679	java/net/HttpURLConnection:getInputStream	()Ljava/io/InputStream;
    //   44: astore 6
    //   46: aload 5
    //   48: aload 6
    //   50: invokespecial 682	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   53: aload 5
    //   55: invokestatic 688	com/mopub/common/util/Strings:fromStream	(Ljava/io/InputStream;)Ljava/lang/String;
    //   58: astore_3
    //   59: aload 5
    //   61: invokestatic 694	com/mopub/common/util/Streams:closeStream	(Ljava/io/Closeable;)V
    //   64: aload_1
    //   65: ifnull +7 -> 72
    //   68: aload_1
    //   69: invokevirtual 697	java/net/HttpURLConnection:disconnect	()V
    //   72: aload_3
    //   73: areturn
    //   74: astore 7
    //   76: aload 5
    //   78: astore_3
    //   79: aload 7
    //   81: astore 5
    //   83: goto +12 -> 95
    //   86: astore 5
    //   88: goto +7 -> 95
    //   91: astore 5
    //   93: aconst_null
    //   94: astore_1
    //   95: aload_3
    //   96: invokestatic 694	com/mopub/common/util/Streams:closeStream	(Ljava/io/Closeable;)V
    //   99: aload_1
    //   100: ifnull +7 -> 107
    //   103: aload_1
    //   104: invokevirtual 697	java/net/HttpURLConnection:disconnect	()V
    //   107: aload 5
    //   109: athrow
    //   110: aconst_null
    //   111: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	112	0	this	VastXmlManagerAggregator
    //   0	112	1	paramString	String
    //   8	19	2	i	int
    //   10	86	3	localObject1	Object
    //   13	6	4	j	int
    //   38	44	5	localObject2	Object
    //   86	1	5	localObject3	Object
    //   91	17	5	localObject4	Object
    //   44	5	6	localInputStream	java.io.InputStream
    //   74	6	7	localObject5	Object
    // Exception table:
    //   from	to	target	type
    //   53	58	74	finally
    //   35	38	86	finally
    //   40	44	86	finally
    //   48	53	86	finally
    //   30	34	91	finally
  }
  
  private static Map b(List paramList)
  {
    Object localObject1 = paramList;
    Preconditions.checkNotNull(paramList, "managers cannot be null");
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    localObject1 = paramList.iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      h localh = (h)((Iterator)localObject1).next();
      Integer localInteger1 = localh.a();
      Integer localInteger2 = localh.b();
      if ((localInteger1 != null) && (localInteger2 != null))
      {
        String str1 = XmlUtils.getAttributeValue(a, "adSlotID");
        Object localObject2 = "adsBy";
        boolean bool2 = ((String)localObject2).equals(str1);
        int k = 10;
        int m = 50;
        if (bool2)
        {
          int i = localInteger1.intValue();
          n = 25;
          if (i < n) {
            continue;
          }
          i = localInteger1.intValue();
          n = 75;
          if (i > n) {
            continue;
          }
          i = localInteger2.intValue();
          if (i < k) {
            continue;
          }
          i = localInteger2.intValue();
          if (i <= m) {}
        }
        else
        {
          localObject2 = "socialActions";
          boolean bool3 = ((String)localObject2).equals(str1);
          if (!bool3) {
            continue;
          }
          int j = localInteger1.intValue();
          if (j < m) {
            continue;
          }
          j = localInteger1.intValue();
          n = 150;
          if (j > n) {
            continue;
          }
          j = localInteger2.intValue();
          if (j < k) {
            continue;
          }
          j = localInteger2.intValue();
          if (j > m) {
            continue;
          }
        }
        localObject2 = b;
        n.b localb = n.b.HTML_RESOURCE;
        m = localInteger1.intValue();
        int n = localInteger2.intValue();
        n localn = n.a((VastResourceXmlManager)localObject2, localb, m, n);
        if (localn != null)
        {
          localObject2 = new com/mopub/mobileads/VastCompanionAdConfig;
          int i1 = localInteger1.intValue();
          int i2 = localInteger2.intValue();
          String str2 = localh.c();
          List localList1 = localh.d();
          List localList2 = localh.e();
          ((VastCompanionAdConfig)localObject2).<init>(i1, i2, localn, str2, localList1, localList2);
          localHashMap.put(str1, localObject2);
        }
      }
    }
    return localHashMap;
  }
  
  private static void b(g paramg, VastVideoConfig paramVastVideoConfig)
  {
    paramg = paramg.e();
    if (paramg != null)
    {
      paramg = paramg.a().iterator();
      for (;;)
      {
        boolean bool1 = paramg.hasNext();
        if (!bool1) {
          break;
        }
        Object localObject1 = (VastExtensionXmlManager)paramg.next();
        if (localObject1 != null)
        {
          Object localObject2 = a;
          Object localObject3 = "AVID";
          localObject2 = XmlUtils.getFirstMatchingChildNode((Node)localObject2, (String)localObject3);
          if (localObject2 == null)
          {
            localObject2 = null;
          }
          else
          {
            localObject3 = new com/mopub/mobileads/a;
            ((a)localObject3).<init>((Node)localObject2);
            localObject2 = new java/util/HashSet;
            ((HashSet)localObject2).<init>();
            localObject3 = a;
            Object localObject4 = "AdVerifications";
            localObject3 = XmlUtils.getFirstMatchingChildNode((Node)localObject3, (String)localObject4);
            if (localObject3 != null)
            {
              localObject4 = "Verification";
              localObject3 = XmlUtils.getMatchingChildNodes((Node)localObject3, (String)localObject4);
              if (localObject3 != null)
              {
                localObject3 = ((List)localObject3).iterator();
                for (;;)
                {
                  boolean bool2 = ((Iterator)localObject3).hasNext();
                  if (!bool2) {
                    break;
                  }
                  localObject4 = (Node)((Iterator)localObject3).next();
                  String str = "JavaScriptResource";
                  localObject4 = XmlUtils.getFirstMatchingChildNode((Node)localObject4, str);
                  if (localObject4 != null)
                  {
                    localObject4 = XmlUtils.getNodeValue((Node)localObject4);
                    ((Set)localObject2).add(localObject4);
                  }
                }
              }
            }
          }
          paramVastVideoConfig.addAvidJavascriptResources((Set)localObject2);
          localObject1 = ((VastExtensionXmlManager)localObject1).a();
          paramVastVideoConfig.addMoatImpressionPixels((Set)localObject1);
        }
      }
    }
  }
  
  private static i c(List paramList)
  {
    Object localObject = paramList;
    Preconditions.checkNotNull(paramList, "managers cannot be null");
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>(paramList);
    localObject = n.b.values();
    int i = localObject.length;
    int j = 0;
    while (j < i)
    {
      n.b localb = localObject[j];
      Iterator localIterator = localArrayList.iterator();
      VastIconXmlManager localVastIconXmlManager;
      n localn;
      do
      {
        Integer localInteger1;
        Integer localInteger2;
        int k;
        int m;
        do
        {
          do
          {
            do
            {
              do
              {
                do
                {
                  boolean bool = localIterator.hasNext();
                  if (!bool) {
                    break;
                  }
                  localVastIconXmlManager = (VastIconXmlManager)localIterator.next();
                  localInteger1 = localVastIconXmlManager.a();
                  localInteger2 = localVastIconXmlManager.b();
                } while (localInteger1 == null);
                k = localInteger1.intValue();
              } while (k <= 0);
              k = localInteger1.intValue();
              m = 300;
            } while ((k > m) || (localInteger2 == null));
            k = localInteger2.intValue();
          } while (k <= 0);
          k = localInteger2.intValue();
        } while (k > m);
        VastResourceXmlManager localVastResourceXmlManager = a;
        int n = localInteger1.intValue();
        int i1 = localInteger2.intValue();
        localn = n.a(localVastResourceXmlManager, localb, n, i1);
      } while (localn == null);
      localObject = new com/mopub/mobileads/i;
      int i2 = localVastIconXmlManager.a().intValue();
      int i3 = localVastIconXmlManager.b().intValue();
      Integer localInteger3 = localVastIconXmlManager.c();
      Integer localInteger4 = localVastIconXmlManager.d();
      List localList1 = localVastIconXmlManager.e();
      String str = localVastIconXmlManager.f();
      List localList2 = localVastIconXmlManager.g();
      ((i)localObject).<init>(i2, i3, localInteger3, localInteger4, localn, localList1, str, localList2);
      return (i)localObject;
      j += 1;
    }
    return null;
  }
  
  protected void onCancelled()
  {
    VastXmlManagerAggregator.b localb = (VastXmlManagerAggregator.b)b.get();
    if (localb != null) {
      localb.onAggregationComplete(null);
    }
  }
  
  protected void onPreExecute()
  {
    Networking.getUserAgent(e);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastXmlManagerAggregator
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */