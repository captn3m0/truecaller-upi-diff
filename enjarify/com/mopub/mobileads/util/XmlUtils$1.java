package com.mopub.mobileads.util;

import org.w3c.dom.Node;

final class XmlUtils$1
  implements XmlUtils.NodeProcessor
{
  public final String process(Node paramNode)
  {
    return XmlUtils.getNodeValue(paramNode);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.util.XmlUtils.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */