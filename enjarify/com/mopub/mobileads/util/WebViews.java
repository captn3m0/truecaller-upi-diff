package com.mopub.mobileads.util;

import android.os.Build.VERSION;
import android.webkit.CookieManager;
import android.webkit.WebView;
import com.mopub.common.MoPub;
import com.mopub.common.Preconditions;

public class WebViews
{
  public static void manageThirdPartyCookies(WebView paramWebView)
  {
    Preconditions.checkNotNull(paramWebView);
    CookieManager localCookieManager = CookieManager.getInstance();
    int i = Build.VERSION.SDK_INT;
    int j = 21;
    if (i >= j)
    {
      boolean bool = MoPub.canCollectPersonalInformation();
      localCookieManager.setAcceptThirdPartyCookies(paramWebView, bool);
    }
  }
  
  public static void manageWebCookies()
  {
    CookieManager localCookieManager = CookieManager.getInstance();
    boolean bool = MoPub.canCollectPersonalInformation();
    if (bool)
    {
      bool = true;
      localCookieManager.setAcceptCookie(bool);
      CookieManager.setAcceptFileSchemeCookies(bool);
      return;
    }
    localCookieManager.setAcceptCookie(false);
    CookieManager.setAcceptFileSchemeCookies(false);
    int i = Build.VERSION.SDK_INT;
    int j = 21;
    if (i >= j)
    {
      localCookieManager.removeSessionCookies(null);
      localCookieManager.removeAllCookies(null);
      localCookieManager.flush();
      return;
    }
    localCookieManager.removeSessionCookie();
    localCookieManager.removeAllCookie();
  }
  
  public static void onPause(WebView paramWebView, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      paramWebView.stopLoading();
      String str = "";
      paramWebView.loadUrl(str);
    }
    paramWebView.onPause();
  }
  
  public static void setDisableJSChromeClient(WebView paramWebView)
  {
    WebViews.1 local1 = new com/mopub/mobileads/util/WebViews$1;
    local1.<init>();
    paramWebView.setWebChromeClient(local1);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.util.WebViews
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */