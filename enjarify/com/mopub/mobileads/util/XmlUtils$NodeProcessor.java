package com.mopub.mobileads.util;

import org.w3c.dom.Node;

public abstract interface XmlUtils$NodeProcessor
{
  public abstract Object process(Node paramNode);
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.util.XmlUtils.NodeProcessor
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */