package com.mopub.mobileads.util;

import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import com.mopub.common.logging.MoPubLog;

final class WebViews$1
  extends WebChromeClient
{
  public final boolean onJsAlert(WebView paramWebView, String paramString1, String paramString2, JsResult paramJsResult)
  {
    MoPubLog.d(paramString2);
    paramJsResult.confirm();
    return true;
  }
  
  public final boolean onJsBeforeUnload(WebView paramWebView, String paramString1, String paramString2, JsResult paramJsResult)
  {
    MoPubLog.d(paramString2);
    paramJsResult.confirm();
    return true;
  }
  
  public final boolean onJsConfirm(WebView paramWebView, String paramString1, String paramString2, JsResult paramJsResult)
  {
    MoPubLog.d(paramString2);
    paramJsResult.confirm();
    return true;
  }
  
  public final boolean onJsPrompt(WebView paramWebView, String paramString1, String paramString2, String paramString3, JsPromptResult paramJsPromptResult)
  {
    MoPubLog.d(paramString2);
    paramJsPromptResult.confirm();
    return true;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.util.WebViews.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */