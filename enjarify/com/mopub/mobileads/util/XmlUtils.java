package com.mopub.mobileads.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlUtils
{
  public static String getAttributeValue(Node paramNode, String paramString)
  {
    if ((paramNode != null) && (paramString != null))
    {
      paramNode = paramNode.getAttributes().getNamedItem(paramString);
      if (paramNode != null) {
        return paramNode.getNodeValue();
      }
      return null;
    }
    return null;
  }
  
  public static Integer getAttributeValueAsInt(Node paramNode, String paramString)
  {
    if ((paramNode != null) && (paramString != null)) {
      try
      {
        paramNode = getAttributeValue(paramNode, paramString);
        int i = Integer.parseInt(paramNode);
        return Integer.valueOf(i);
      }
      catch (NumberFormatException localNumberFormatException)
      {
        return null;
      }
    }
    return null;
  }
  
  public static Object getFirstMatchFromDocument(Document paramDocument, String paramString1, String paramString2, String paramString3, XmlUtils.NodeProcessor paramNodeProcessor)
  {
    if (paramDocument == null) {
      return null;
    }
    paramDocument = paramDocument.getElementsByTagName(paramString1);
    if (paramDocument == null) {
      return null;
    }
    int i = 0;
    paramString1 = null;
    int j;
    Object localObject;
    if (paramString3 == null)
    {
      paramString3 = null;
    }
    else
    {
      j = 1;
      localObject = new String[j];
      localObject[0] = paramString3;
      paramString3 = Arrays.asList((Object[])localObject);
    }
    for (;;)
    {
      j = paramDocument.getLength();
      if (i >= j) {
        break;
      }
      localObject = paramDocument.item(i);
      if (localObject != null)
      {
        boolean bool = nodeMatchesAttributeFilter((Node)localObject, paramString2, paramString3);
        if (bool)
        {
          localObject = paramNodeProcessor.process((Node)localObject);
          if (localObject != null) {
            return localObject;
          }
        }
      }
      i += 1;
    }
    return null;
  }
  
  public static Node getFirstMatchingChildNode(Node paramNode, String paramString)
  {
    return getFirstMatchingChildNode(paramNode, paramString, null, null);
  }
  
  public static Node getFirstMatchingChildNode(Node paramNode, String paramString1, String paramString2, List paramList)
  {
    if ((paramNode != null) && (paramString1 != null))
    {
      paramNode = getMatchingChildNodes(paramNode, paramString1, paramString2, paramList);
      if (paramNode != null)
      {
        boolean bool = paramNode.isEmpty();
        if (!bool) {
          return (Node)paramNode.get(0);
        }
      }
      return null;
    }
    return null;
  }
  
  public static String getFirstMatchingStringData(Document paramDocument, String paramString)
  {
    return getFirstMatchingStringData(paramDocument, paramString, null, null);
  }
  
  public static String getFirstMatchingStringData(Document paramDocument, String paramString1, String paramString2, String paramString3)
  {
    XmlUtils.1 local1 = new com/mopub/mobileads/util/XmlUtils$1;
    local1.<init>();
    return (String)getFirstMatchFromDocument(paramDocument, paramString1, paramString2, paramString3, local1);
  }
  
  public static List getListFromDocument(Document paramDocument, String paramString1, String paramString2, String paramString3, XmlUtils.NodeProcessor paramNodeProcessor)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    if (paramDocument == null) {
      return localArrayList;
    }
    paramDocument = paramDocument.getElementsByTagName(paramString1);
    if (paramDocument == null) {
      return localArrayList;
    }
    int i = 0;
    paramString1 = null;
    int j;
    Object localObject;
    if (paramString3 == null)
    {
      paramString3 = null;
    }
    else
    {
      j = 1;
      localObject = new String[j];
      localObject[0] = paramString3;
      paramString3 = Arrays.asList((Object[])localObject);
    }
    for (;;)
    {
      j = paramDocument.getLength();
      if (i >= j) {
        break;
      }
      localObject = paramDocument.item(i);
      if (localObject != null)
      {
        boolean bool = nodeMatchesAttributeFilter((Node)localObject, paramString2, paramString3);
        if (bool)
        {
          localObject = paramNodeProcessor.process((Node)localObject);
          if (localObject != null) {
            localArrayList.add(localObject);
          }
        }
      }
      i += 1;
    }
    return localArrayList;
  }
  
  public static List getMatchingChildNodes(Node paramNode, String paramString)
  {
    return getMatchingChildNodes(paramNode, paramString, null, null);
  }
  
  public static List getMatchingChildNodes(Node paramNode, String paramString1, String paramString2, List paramList)
  {
    if ((paramNode != null) && (paramString1 != null))
    {
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      paramNode = paramNode.getChildNodes();
      int i = 0;
      for (;;)
      {
        int j = paramNode.getLength();
        if (i >= j) {
          break;
        }
        Node localNode = paramNode.item(i);
        String str = localNode.getNodeName();
        boolean bool = str.equals(paramString1);
        if (bool)
        {
          bool = nodeMatchesAttributeFilter(localNode, paramString2, paramList);
          if (bool) {
            localArrayList.add(localNode);
          }
        }
        i += 1;
      }
      return localArrayList;
    }
    return null;
  }
  
  public static String getNodeValue(Node paramNode)
  {
    if (paramNode != null)
    {
      Object localObject = paramNode.getFirstChild();
      if (localObject != null)
      {
        localObject = paramNode.getFirstChild().getNodeValue();
        if (localObject != null) {
          return paramNode.getFirstChild().getNodeValue().trim();
        }
      }
    }
    return null;
  }
  
  public static List getNodesWithElementAndAttribute(Document paramDocument, String paramString1, String paramString2, String paramString3)
  {
    XmlUtils.3 local3 = new com/mopub/mobileads/util/XmlUtils$3;
    local3.<init>();
    return getListFromDocument(paramDocument, paramString1, paramString2, paramString3, local3);
  }
  
  public static List getStringDataAsList(Document paramDocument, String paramString)
  {
    return getStringDataAsList(paramDocument, paramString, null, null);
  }
  
  public static List getStringDataAsList(Document paramDocument, String paramString1, String paramString2, String paramString3)
  {
    XmlUtils.2 local2 = new com/mopub/mobileads/util/XmlUtils$2;
    local2.<init>();
    return getListFromDocument(paramDocument, paramString1, paramString2, paramString3, local2);
  }
  
  public static boolean nodeMatchesAttributeFilter(Node paramNode, String paramString, List paramList)
  {
    boolean bool1 = true;
    if ((paramString != null) && (paramList != null))
    {
      paramNode = paramNode.getAttributes();
      if (paramNode != null)
      {
        paramNode = paramNode.getNamedItem(paramString);
        if (paramNode != null)
        {
          paramNode = paramNode.getNodeValue();
          boolean bool2 = paramList.contains(paramNode);
          if (bool2) {
            return bool1;
          }
        }
      }
      return false;
    }
    return bool1;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.util.XmlUtils
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */