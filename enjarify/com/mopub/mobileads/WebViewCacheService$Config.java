package com.mopub.mobileads;

import com.mopub.common.ExternalViewabilitySessionManager;
import java.lang.ref.WeakReference;

public class WebViewCacheService$Config
{
  private final BaseWebView a;
  private final WeakReference b;
  private final ExternalViewabilitySessionManager c;
  
  WebViewCacheService$Config(BaseWebView paramBaseWebView, Interstitial paramInterstitial, ExternalViewabilitySessionManager paramExternalViewabilitySessionManager)
  {
    a = paramBaseWebView;
    paramBaseWebView = new java/lang/ref/WeakReference;
    paramBaseWebView.<init>(paramInterstitial);
    b = paramBaseWebView;
    c = paramExternalViewabilitySessionManager;
  }
  
  public ExternalViewabilitySessionManager getViewabilityManager()
  {
    return c;
  }
  
  public WeakReference getWeakInterstitial()
  {
    return b;
  }
  
  public BaseWebView getWebView()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.WebViewCacheService.Config
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */