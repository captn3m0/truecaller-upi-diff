package com.mopub.mobileads;

import android.os.Handler;
import com.mopub.common.Preconditions;

public abstract class RepeatingHandlerRunnable
  implements Runnable
{
  protected final Handler a;
  protected volatile long b;
  private volatile boolean c;
  
  public RepeatingHandlerRunnable(Handler paramHandler)
  {
    Preconditions.checkNotNull(paramHandler);
    a = paramHandler;
  }
  
  public abstract void doWork();
  
  public boolean isRunning()
  {
    return c;
  }
  
  public void run()
  {
    boolean bool = c;
    if (bool)
    {
      doWork();
      Handler localHandler = a;
      long l = b;
      localHandler.postDelayed(this, l);
    }
  }
  
  public void startRepeating(long paramLong)
  {
    boolean bool1 = true;
    long l = 0L;
    boolean bool2 = paramLong < l;
    boolean bool3;
    if (bool2) {
      bool3 = true;
    } else {
      bool3 = false;
    }
    String str = "intervalMillis must be greater than 0. Saw: %d";
    Object[] arrayOfObject = new Object[bool1];
    Long localLong = Long.valueOf(paramLong);
    arrayOfObject[0] = localLong;
    Preconditions.checkArgument(bool3, str, arrayOfObject);
    b = paramLong;
    boolean bool4 = c;
    if (!bool4)
    {
      c = bool1;
      Handler localHandler = a;
      localHandler.post(this);
    }
  }
  
  public void stop()
  {
    c = false;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.RepeatingHandlerRunnable
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */