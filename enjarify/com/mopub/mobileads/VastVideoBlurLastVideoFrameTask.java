package com.mopub.mobileads;

import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.widget.ImageView;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.ImageUtils;

public class VastVideoBlurLastVideoFrameTask
  extends AsyncTask
{
  private final MediaMetadataRetriever a;
  private final ImageView b;
  private int c;
  private Bitmap d;
  private Bitmap e;
  
  public VastVideoBlurLastVideoFrameTask(MediaMetadataRetriever paramMediaMetadataRetriever, ImageView paramImageView, int paramInt)
  {
    a = paramMediaMetadataRetriever;
    b = paramImageView;
    c = paramInt;
  }
  
  private Boolean a(String... paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      int i = paramVarArgs.length;
      if (i != 0)
      {
        i = 0;
        MediaMetadataRetriever localMediaMetadataRetriever = null;
        String str = paramVarArgs[0];
        if (str != null) {
          try
          {
            paramVarArgs = paramVarArgs[0];
            localMediaMetadataRetriever = a;
            localMediaMetadataRetriever.setDataSource(paramVarArgs);
            paramVarArgs = a;
            i = c * 1000;
            int j = 200000;
            i -= j;
            long l = i;
            int k = 3;
            paramVarArgs = paramVarArgs.getFrameAtTime(l, k);
            d = paramVarArgs;
            paramVarArgs = d;
            if (paramVarArgs == null) {
              return Boolean.FALSE;
            }
            paramVarArgs = d;
            i = 4;
            paramVarArgs = ImageUtils.applyFastGaussianBlurToBitmap(paramVarArgs, i);
            e = paramVarArgs;
            return Boolean.TRUE;
          }
          catch (Exception paramVarArgs)
          {
            MoPubLog.d("Failed to blur last video frame", paramVarArgs);
            return Boolean.FALSE;
          }
        }
      }
    }
    return Boolean.FALSE;
  }
  
  protected void onCancelled()
  {
    MoPubLog.d("VastVideoBlurLastVideoFrameTask was cancelled.");
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastVideoBlurLastVideoFrameTask
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */