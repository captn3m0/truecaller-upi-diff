package com.mopub.mobileads;

import android.content.Context;
import com.mopub.common.AdUrlGenerator;
import com.mopub.common.BaseUrlGenerator;
import com.mopub.common.ClientMetadata;
import com.mopub.common.ExternalViewabilitySessionManager.ViewabilityVendor;

public class WebViewAdUrlGenerator
  extends AdUrlGenerator
{
  private final boolean g;
  
  public WebViewAdUrlGenerator(Context paramContext, boolean paramBoolean)
  {
    super(paramContext);
    g = paramBoolean;
  }
  
  public String generateUrlString(String paramString)
  {
    a(paramString, "/m/ad");
    b("6");
    paramString = ClientMetadata.getInstance(a);
    a(paramString);
    a();
    boolean bool = g;
    a(bool);
    paramString = ExternalViewabilitySessionManager.ViewabilityVendor.getEnabledVendorKey();
    a(paramString);
    return f.toString();
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.WebViewAdUrlGenerator
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */