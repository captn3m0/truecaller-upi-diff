package com.mopub.mobileads;

 enum AdAlertGestureListener$a
{
  static
  {
    Object localObject = new com/mopub/mobileads/AdAlertGestureListener$a;
    ((a)localObject).<init>("UNSET", 0);
    UNSET = (a)localObject;
    localObject = new com/mopub/mobileads/AdAlertGestureListener$a;
    int i = 1;
    ((a)localObject).<init>("GOING_RIGHT", i);
    GOING_RIGHT = (a)localObject;
    localObject = new com/mopub/mobileads/AdAlertGestureListener$a;
    int j = 2;
    ((a)localObject).<init>("GOING_LEFT", j);
    GOING_LEFT = (a)localObject;
    localObject = new com/mopub/mobileads/AdAlertGestureListener$a;
    int k = 3;
    ((a)localObject).<init>("FINISHED", k);
    FINISHED = (a)localObject;
    localObject = new com/mopub/mobileads/AdAlertGestureListener$a;
    int m = 4;
    ((a)localObject).<init>("FAILED", m);
    FAILED = (a)localObject;
    localObject = new a[5];
    a locala = UNSET;
    localObject[0] = locala;
    locala = GOING_RIGHT;
    localObject[i] = locala;
    locala = GOING_LEFT;
    localObject[j] = locala;
    locala = FINISHED;
    localObject[k] = locala;
    locala = FAILED;
    localObject[m] = locala;
    a = (a[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.AdAlertGestureListener.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */