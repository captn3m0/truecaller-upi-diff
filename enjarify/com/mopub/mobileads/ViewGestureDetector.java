package com.mopub.mobileads;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.mopub.common.AdReport;
import com.mopub.common.logging.MoPubLog;

public class ViewGestureDetector
  extends GestureDetector
{
  private final View a;
  private AdAlertGestureListener b;
  private ViewGestureDetector.UserClickListener c;
  
  public ViewGestureDetector(Context paramContext, View paramView, AdReport paramAdReport)
  {
    this(paramContext, paramView, localAdAlertGestureListener);
  }
  
  private ViewGestureDetector(Context paramContext, View paramView, AdAlertGestureListener paramAdAlertGestureListener)
  {
    super(paramContext, paramAdAlertGestureListener);
    b = paramAdAlertGestureListener;
    a = paramView;
    setIsLongpressEnabled(false);
  }
  
  public void sendTouchEvent(MotionEvent paramMotionEvent)
  {
    int i = paramMotionEvent.getAction();
    Object localObject1;
    Object localObject2;
    switch (i)
    {
    default: 
      break;
    case 2: 
      localObject1 = a;
      int j = 0;
      localObject2 = null;
      if ((paramMotionEvent != null) && (localObject1 != null))
      {
        float f1 = paramMotionEvent.getX();
        float f2 = paramMotionEvent.getY();
        boolean bool2 = f1 < 0.0F;
        if (!bool2)
        {
          int k = ((View)localObject1).getWidth();
          float f3 = k;
          boolean bool3 = f1 < f3;
          if (!bool3)
          {
            bool3 = f2 < 0.0F;
            if (!bool3)
            {
              float f4 = ((View)localObject1).getHeight();
              boolean bool1 = f2 < f4;
              if (!bool1) {
                j = 1;
              }
            }
          }
        }
      }
      if (j != 0)
      {
        onTouchEvent(paramMotionEvent);
        return;
      }
      paramMotionEvent = b;
      paramMotionEvent.a();
      break;
    case 1: 
      paramMotionEvent = c;
      if (paramMotionEvent != null)
      {
        paramMotionEvent.onUserClick();
      }
      else
      {
        paramMotionEvent = "View's onUserClick() is not registered.";
        MoPubLog.d(paramMotionEvent);
      }
      paramMotionEvent = b;
      localObject1 = c;
      localObject2 = AdAlertGestureListener.a.FINISHED;
      if (localObject1 == localObject2)
      {
        localObject1 = new com/mopub/mobileads/AdAlertReporter;
        localObject2 = d.getContext();
        View localView = d;
        AdReport localAdReport = a;
        ((AdAlertReporter)localObject1).<init>((Context)localObject2, localView, localAdReport);
        b = ((AdAlertReporter)localObject1);
        localObject1 = b;
        ((AdAlertReporter)localObject1).send();
      }
      paramMotionEvent.a();
      return;
    case 0: 
      onTouchEvent(paramMotionEvent);
      return;
    }
  }
  
  public void setUserClickListener(ViewGestureDetector.UserClickListener paramUserClickListener)
  {
    c = paramUserClickListener;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.ViewGestureDetector
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */