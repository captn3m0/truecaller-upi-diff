package com.mopub.mobileads;

import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import com.mopub.common.AdReport;

public class AdAlertGestureListener
  extends GestureDetector.SimpleOnGestureListener
{
  final AdReport a;
  AdAlertReporter b;
  AdAlertGestureListener.a c;
  View d;
  private float e;
  private float f;
  private boolean g;
  private boolean h;
  private int i;
  private float j;
  
  AdAlertGestureListener(View paramView, AdReport paramAdReport)
  {
    float f1 = 100.0F;
    e = f1;
    AdAlertGestureListener.a locala = AdAlertGestureListener.a.UNSET;
    c = locala;
    if (paramView != null)
    {
      int k = paramView.getWidth();
      if (k > 0)
      {
        k = paramView.getWidth();
        float f2 = k;
        float f3 = 3.0F;
        f2 /= f3;
        f1 = Math.min(f1, f2);
        e = f1;
      }
    }
    d = paramView;
    a = paramAdReport;
  }
  
  final void a()
  {
    i = 0;
    AdAlertGestureListener.a locala = AdAlertGestureListener.a.UNSET;
    c = locala;
  }
  
  public boolean onScroll(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
  {
    Object localObject = c;
    AdAlertGestureListener.a locala = AdAlertGestureListener.a.FINISHED;
    if (localObject == locala) {
      return super.onScroll(paramMotionEvent1, paramMotionEvent2, paramFloat1, paramFloat2);
    }
    float f1 = paramMotionEvent1.getY();
    f1 = Math.abs(paramMotionEvent2.getY() - f1);
    int k = 1120403456;
    float f2 = 100.0F;
    int n = 0;
    int i1 = 1;
    boolean bool3 = f1 < f2;
    if (bool3)
    {
      bool3 = true;
      f1 = Float.MIN_VALUE;
    }
    else
    {
      bool3 = false;
      f1 = 0.0F;
      localObject = null;
    }
    if (bool3)
    {
      localObject = AdAlertGestureListener.a.FAILED;
      c = ((AdAlertGestureListener.a)localObject);
      return super.onScroll(paramMotionEvent1, paramMotionEvent2, paramFloat1, paramFloat2);
    }
    localObject = AdAlertGestureListener.1.a;
    locala = c;
    k = locala.ordinal();
    int i2 = localObject[k];
    float f3;
    boolean bool2;
    switch (i2)
    {
    default: 
      break;
    case 3: 
      f1 = paramMotionEvent2.getX();
      boolean bool1 = g;
      int m;
      if (bool1)
      {
        bool1 = true;
        f2 = Float.MIN_VALUE;
      }
      else
      {
        f2 = j;
        f3 = e;
        f2 -= f3;
        bool1 = f1 < f2;
        if (!bool1)
        {
          h = false;
          g = i1;
          m = i + i1;
          i = m;
          m = i;
          int i3 = 4;
          f3 = 5.6E-45F;
          if (m >= i3)
          {
            locala = AdAlertGestureListener.a.FINISHED;
            c = locala;
          }
          m = 1;
          f2 = Float.MIN_VALUE;
        }
        else
        {
          m = 0;
          f2 = 0.0F;
          locala = null;
        }
      }
      if (m != 0)
      {
        f2 = f;
        bool2 = f1 < f2;
        if (bool2) {
          n = 1;
        }
        if (n != 0)
        {
          locala = AdAlertGestureListener.a.GOING_RIGHT;
          c = locala;
          j = f1;
        }
      }
      break;
    case 2: 
      f1 = paramMotionEvent2.getX();
      bool2 = h;
      if (bool2)
      {
        bool2 = true;
        f2 = Float.MIN_VALUE;
      }
      else
      {
        f2 = j;
        f3 = e;
        f2 += f3;
        bool2 = f1 < f2;
        if (!bool2)
        {
          g = false;
          h = i1;
          bool2 = true;
          f2 = Float.MIN_VALUE;
        }
        else
        {
          bool2 = false;
          f2 = 0.0F;
          locala = null;
        }
      }
      if (bool2)
      {
        f2 = f;
        bool2 = f1 < f2;
        if (bool2) {
          n = 1;
        }
        if (n != 0)
        {
          locala = AdAlertGestureListener.a.GOING_LEFT;
          c = locala;
          j = f1;
        }
      }
      break;
    case 1: 
      f1 = paramMotionEvent1.getX();
      j = f1;
      f1 = paramMotionEvent2.getX();
      f2 = j;
      boolean bool4 = f1 < f2;
      if (bool4)
      {
        localObject = AdAlertGestureListener.a.GOING_RIGHT;
        c = ((AdAlertGestureListener.a)localObject);
      }
      break;
    }
    f1 = paramMotionEvent2.getX();
    f = f1;
    return super.onScroll(paramMotionEvent1, paramMotionEvent2, paramFloat1, paramFloat2);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.AdAlertGestureListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */