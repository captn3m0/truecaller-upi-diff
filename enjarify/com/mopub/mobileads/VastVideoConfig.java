package com.mopub.mobileads;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import com.mopub.common.Preconditions;
import com.mopub.common.Preconditions.NoThrow;
import com.mopub.common.UrlAction;
import com.mopub.common.UrlHandler;
import com.mopub.common.UrlHandler.Builder;
import com.mopub.common.UrlHandler.ResultActions;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.DeviceUtils.ForceOrientation;
import com.mopub.common.util.Strings;
import com.mopub.network.TrackingRequest;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

public class VastVideoConfig
  implements Serializable
{
  private static final long serialVersionUID = 2L;
  private final Set A;
  private String B;
  private String C;
  private String D;
  private boolean E;
  private final ArrayList a;
  private final ArrayList b;
  private final ArrayList c;
  private final ArrayList d;
  private final ArrayList e;
  private final ArrayList f;
  private final ArrayList g;
  private final ArrayList h;
  private final ArrayList i;
  private final ArrayList j;
  private String k;
  private String l;
  private String m;
  private String n;
  private VastCompanionAdConfig o;
  private VastCompanionAdConfig p;
  private Map q;
  private i r;
  private boolean s;
  private String t;
  private String u;
  private String v;
  private DeviceUtils.ForceOrientation w;
  private VideoViewabilityTracker x;
  private final Map y;
  private final Set z;
  
  public VastVideoConfig()
  {
    Object localObject = DeviceUtils.ForceOrientation.FORCE_LANDSCAPE;
    w = ((DeviceUtils.ForceOrientation)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    a = ((ArrayList)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    b = ((ArrayList)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    c = ((ArrayList)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    d = ((ArrayList)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    e = ((ArrayList)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    f = ((ArrayList)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    g = ((ArrayList)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    h = ((ArrayList)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    i = ((ArrayList)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    j = ((ArrayList)localObject);
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    q = ((Map)localObject);
    s = false;
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    y = ((Map)localObject);
    localObject = new java/util/HashSet;
    ((HashSet)localObject).<init>();
    z = ((Set)localObject);
    localObject = new java/util/HashSet;
    ((HashSet)localObject).<init>();
    A = ((Set)localObject);
  }
  
  private static List a(List paramList)
  {
    Preconditions.checkNotNull(paramList);
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      String str = (String)paramList.next();
      VastTracker localVastTracker = new com/mopub/mobileads/VastTracker;
      localVastTracker.<init>(str);
      localArrayList.add(localVastTracker);
    }
    return localArrayList;
  }
  
  private void a(Context paramContext, int paramInt, Integer paramInteger)
  {
    Preconditions.checkNotNull(paramContext, "context cannot be null");
    Object localObject1 = i;
    Object localObject2 = Integer.valueOf(paramInt);
    Object localObject3 = l;
    TrackingRequest.makeVastTrackingHttpRequest((List)localObject1, null, (Integer)localObject2, (String)localObject3, paramContext);
    localObject2 = k;
    paramInt = TextUtils.isEmpty((CharSequence)localObject2);
    if (paramInt != 0) {
      return;
    }
    localObject2 = new com/mopub/common/UrlHandler$Builder;
    ((UrlHandler.Builder)localObject2).<init>();
    localObject1 = B;
    localObject2 = ((UrlHandler.Builder)localObject2).withDspCreativeId((String)localObject1);
    localObject1 = UrlAction.IGNORE_ABOUT_SCHEME;
    localObject3 = new UrlAction[6];
    UrlAction localUrlAction = UrlAction.OPEN_APP_MARKET;
    localObject3[0] = localUrlAction;
    localUrlAction = UrlAction.OPEN_NATIVE_BROWSER;
    localObject3[1] = localUrlAction;
    localUrlAction = UrlAction.OPEN_IN_APP_BROWSER;
    localObject3[2] = localUrlAction;
    localUrlAction = UrlAction.HANDLE_SHARE_TWEET;
    localObject3[3] = localUrlAction;
    localUrlAction = UrlAction.FOLLOW_DEEP_LINK_WITH_FALLBACK;
    localObject3[4] = localUrlAction;
    localUrlAction = UrlAction.FOLLOW_DEEP_LINK;
    localObject3[5] = localUrlAction;
    localObject2 = ((UrlHandler.Builder)localObject2).withSupportedUrlActions((UrlAction)localObject1, (UrlAction[])localObject3);
    localObject1 = new com/mopub/mobileads/VastVideoConfig$1;
    ((VastVideoConfig.1)localObject1).<init>(this, paramContext, paramInteger);
    localObject2 = ((UrlHandler.Builder)localObject2).withResultActions((UrlHandler.ResultActions)localObject1).withoutMoPubBrowser().build();
    paramInteger = k;
    ((UrlHandler)localObject2).handleUrl(paramContext, paramInteger);
  }
  
  private void a(List paramList, float paramFloat)
  {
    Preconditions.checkNotNull(paramList);
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      String str = (String)paramList.next();
      VastFractionalProgressTracker localVastFractionalProgressTracker = new com/mopub/mobileads/VastFractionalProgressTracker;
      localVastFractionalProgressTracker.<init>(str, paramFloat);
      localArrayList.add(localVastFractionalProgressTracker);
    }
    addFractionalTrackers(localArrayList);
  }
  
  private void b(List paramList)
  {
    Preconditions.checkNotNull(paramList);
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      String str = (String)paramList.next();
      VastAbsoluteProgressTracker localVastAbsoluteProgressTracker = new com/mopub/mobileads/VastAbsoluteProgressTracker;
      localVastAbsoluteProgressTracker.<init>(str, 0);
      localArrayList.add(localVastAbsoluteProgressTracker);
    }
    addAbsoluteTrackers(localArrayList);
  }
  
  public void addAbsoluteTrackers(List paramList)
  {
    Preconditions.checkNotNull(paramList, "absoluteTrackers cannot be null");
    c.addAll(paramList);
    Collections.sort(c);
  }
  
  public void addAvidJavascriptResources(Set paramSet)
  {
    if (paramSet != null)
    {
      Set localSet = z;
      localSet.addAll(paramSet);
    }
  }
  
  public void addClickTrackers(List paramList)
  {
    Preconditions.checkNotNull(paramList, "clickTrackers cannot be null");
    i.addAll(paramList);
  }
  
  public void addCloseTrackers(List paramList)
  {
    Preconditions.checkNotNull(paramList, "closeTrackers cannot be null");
    g.addAll(paramList);
  }
  
  public void addCompleteTrackers(List paramList)
  {
    Preconditions.checkNotNull(paramList, "completeTrackers cannot be null");
    f.addAll(paramList);
  }
  
  public void addErrorTrackers(List paramList)
  {
    Preconditions.checkNotNull(paramList, "errorTrackers cannot be null");
    j.addAll(paramList);
  }
  
  public void addExternalViewabilityTrackers(Map paramMap)
  {
    if (paramMap != null)
    {
      Map localMap = y;
      localMap.putAll(paramMap);
    }
  }
  
  public void addFractionalTrackers(List paramList)
  {
    Preconditions.checkNotNull(paramList, "fractionalTrackers cannot be null");
    b.addAll(paramList);
    Collections.sort(b);
  }
  
  public void addImpressionTrackers(List paramList)
  {
    Preconditions.checkNotNull(paramList, "impressionTrackers cannot be null");
    a.addAll(paramList);
  }
  
  public void addMoatImpressionPixels(Set paramSet)
  {
    if (paramSet != null)
    {
      Set localSet = A;
      localSet.addAll(paramSet);
    }
  }
  
  public void addPauseTrackers(List paramList)
  {
    Preconditions.checkNotNull(paramList, "pauseTrackers cannot be null");
    d.addAll(paramList);
  }
  
  public void addResumeTrackers(List paramList)
  {
    Preconditions.checkNotNull(paramList, "resumeTrackers cannot be null");
    e.addAll(paramList);
  }
  
  public void addSkipTrackers(List paramList)
  {
    Preconditions.checkNotNull(paramList, "skipTrackers cannot be null");
    h.addAll(paramList);
  }
  
  public void addVideoTrackers(JSONObject paramJSONObject)
  {
    if (paramJSONObject == null) {
      return;
    }
    JSONArray localJSONArray = paramJSONObject.optJSONArray("urls");
    String str1 = "events";
    paramJSONObject = paramJSONObject.optJSONArray(str1);
    if ((localJSONArray != null) && (paramJSONObject != null))
    {
      str1 = null;
      int i1 = 0;
      for (;;)
      {
        int i2 = paramJSONObject.length();
        if (i1 >= i2) {
          break;
        }
        Object localObject1 = paramJSONObject.optString(i1);
        Preconditions.checkNotNull(localJSONArray);
        Object localObject2;
        int i4;
        Object localObject3;
        if (localObject1 == null)
        {
          localObject2 = null;
        }
        else
        {
          localObject2 = new java/util/ArrayList;
          ((ArrayList)localObject2).<init>();
          i4 = 0;
          localr = null;
          for (;;)
          {
            int i5 = localJSONArray.length();
            if (i4 >= i5) {
              break;
            }
            localObject3 = localJSONArray.optString(i4);
            if (localObject3 != null)
            {
              String str2 = "%%VIDEO_EVENT%%";
              localObject3 = ((String)localObject3).replace(str2, (CharSequence)localObject1);
              ((List)localObject2).add(localObject3);
            }
            i4 += 1;
          }
        }
        r localr = r.fromString((String)localObject1);
        if ((localObject1 != null) && (localObject2 != null))
        {
          localObject3 = VastVideoConfig.2.a;
          i4 = localr.ordinal();
          i4 = localObject3[i4];
          boolean bool;
          int i3;
          float f1;
          switch (i4)
          {
          default: 
            localObject2 = "Encountered unknown video tracking event: ";
            localObject1 = String.valueOf(localObject1);
            localObject1 = ((String)localObject2).concat((String)localObject1);
            MoPubLog.d((String)localObject1);
            break;
          case 7: 
            Preconditions.checkNotNull(localObject2);
            bool = hasCompanionAd();
            if (bool)
            {
              localObject1 = a((List)localObject2);
              o.addClickTrackers((List)localObject1);
              localObject2 = p;
              ((VastCompanionAdConfig)localObject2).addClickTrackers((List)localObject1);
            }
            break;
          case 6: 
            Preconditions.checkNotNull(localObject2);
            bool = hasCompanionAd();
            if (bool)
            {
              localObject1 = a((List)localObject2);
              o.addCreativeViewTrackers((List)localObject1);
              localObject2 = p;
              ((VastCompanionAdConfig)localObject2).addCreativeViewTrackers((List)localObject1);
            }
            break;
          case 5: 
            Preconditions.checkNotNull(localObject2);
            localObject1 = a((List)localObject2);
            addCompleteTrackers((List)localObject1);
            break;
          case 4: 
            i3 = 1061158912;
            f1 = 0.75F;
            a((List)localObject2, f1);
            break;
          case 3: 
            i3 = 1056964608;
            f1 = 0.5F;
            a((List)localObject2, f1);
            break;
          case 2: 
            i3 = 1048576000;
            f1 = 0.25F;
            a((List)localObject2, f1);
            break;
          case 1: 
            b((List)localObject2);
          }
        }
        i1 += 1;
      }
      return;
    }
  }
  
  public ArrayList getAbsoluteTrackers()
  {
    return c;
  }
  
  public Set getAvidJavascriptResources()
  {
    return z;
  }
  
  public String getClickThroughUrl()
  {
    return k;
  }
  
  public List getClickTrackers()
  {
    return i;
  }
  
  public List getCloseTrackers()
  {
    return g;
  }
  
  public List getCompleteTrackers()
  {
    return f;
  }
  
  public String getCustomCloseIconUrl()
  {
    return v;
  }
  
  public String getCustomCtaText()
  {
    return t;
  }
  
  public DeviceUtils.ForceOrientation getCustomForceOrientation()
  {
    return w;
  }
  
  public String getCustomSkipText()
  {
    return u;
  }
  
  public String getDiskMediaFileUrl()
  {
    return m;
  }
  
  public String getDspCreativeId()
  {
    return B;
  }
  
  public List getErrorTrackers()
  {
    return j;
  }
  
  public Map getExternalViewabilityTrackers()
  {
    return y;
  }
  
  public ArrayList getFractionalTrackers()
  {
    return b;
  }
  
  public List getImpressionTrackers()
  {
    return a;
  }
  
  public Set getMoatImpressionPixels()
  {
    return A;
  }
  
  public String getNetworkMediaFileUrl()
  {
    return l;
  }
  
  public List getPauseTrackers()
  {
    return d;
  }
  
  public String getPrivacyInformationIconClickthroughUrl()
  {
    return D;
  }
  
  public String getPrivacyInformationIconImageUrl()
  {
    return C;
  }
  
  public int getRemainingProgressTrackerCount()
  {
    int i1 = -1 >>> 1;
    return getUntriggeredTrackersBefore(i1, i1).size();
  }
  
  public List getResumeTrackers()
  {
    return e;
  }
  
  public Integer getSkipOffsetMillis(int paramInt)
  {
    Object localObject = n;
    if (localObject != null)
    {
      int i1 = 1;
      try
      {
        boolean bool = Strings.isAbsoluteTracker((String)localObject);
        String str1;
        int i3;
        if (bool)
        {
          localObject = n;
          localObject = Strings.parseAbsoluteOffset((String)localObject);
        }
        else
        {
          localObject = n;
          bool = Strings.isPercentageTracker((String)localObject);
          if (!bool) {
            break label137;
          }
          localObject = n;
          str1 = "%";
          String str2 = "";
          localObject = ((String)localObject).replace(str1, str2);
          float f1 = Float.parseFloat((String)localObject);
          i3 = 1120403456;
          f1 /= 100.0F;
          float f2 = paramInt * f1;
          int i2 = Math.round(f2);
          localObject = Integer.valueOf(i2);
        }
        if (localObject != null)
        {
          i3 = ((Integer)localObject).intValue();
          if (i3 < paramInt) {
            return (Integer)localObject;
          }
          return Integer.valueOf(paramInt);
          label137:
          str3 = "Invalid VAST skipoffset format: %s";
          localObject = new Object[i1];
          str1 = n;
          localObject[0] = str1;
          str3 = String.format(str3, (Object[])localObject);
          MoPubLog.d(str3);
          return null;
        }
      }
      catch (NumberFormatException localNumberFormatException)
      {
        localObject = new Object[i1];
        String str4 = n;
        localObject[0] = str4;
        String str3 = String.format("Failed to parse skipoffset %s", (Object[])localObject);
        MoPubLog.d(str3);
      }
    }
    return null;
  }
  
  public String getSkipOffsetString()
  {
    return n;
  }
  
  public List getSkipTrackers()
  {
    return h;
  }
  
  public Map getSocialActionsCompanionAds()
  {
    return q;
  }
  
  public List getUntriggeredTrackersBefore(int paramInt1, int paramInt2)
  {
    int i1 = 0;
    float f1;
    ArrayList localArrayList1;
    if (paramInt2 > 0)
    {
      bool1 = true;
      f1 = Float.MIN_VALUE;
    }
    else
    {
      bool1 = false;
      f1 = 0.0F;
      localArrayList1 = null;
    }
    boolean bool1 = Preconditions.NoThrow.checkArgument(bool1);
    if ((bool1) && (paramInt1 >= 0))
    {
      f1 = paramInt1;
      float f2 = paramInt2;
      f1 /= f2;
      ArrayList localArrayList2 = new java/util/ArrayList;
      localArrayList2.<init>();
      Object localObject1 = new com/mopub/mobileads/VastAbsoluteProgressTracker;
      ((VastAbsoluteProgressTracker)localObject1).<init>("", paramInt1);
      Object localObject2 = c;
      paramInt1 = ((ArrayList)localObject2).size();
      int i3 = 0;
      while (i3 < paramInt1)
      {
        VastAbsoluteProgressTracker localVastAbsoluteProgressTracker = (VastAbsoluteProgressTracker)c.get(i3);
        int i4 = localVastAbsoluteProgressTracker.compareTo((VastAbsoluteProgressTracker)localObject1);
        if (i4 > 0) {
          break;
        }
        boolean bool3 = localVastAbsoluteProgressTracker.isTracked();
        if (!bool3) {
          localArrayList2.add(localVastAbsoluteProgressTracker);
        }
        i3 += 1;
      }
      localObject2 = new com/mopub/mobileads/VastFractionalProgressTracker;
      localObject1 = "";
      ((VastFractionalProgressTracker)localObject2).<init>((String)localObject1, f1);
      localArrayList1 = b;
      int i2 = localArrayList1.size();
      while (i1 < i2)
      {
        localObject1 = (VastFractionalProgressTracker)b.get(i1);
        i3 = ((VastFractionalProgressTracker)localObject1).compareTo((VastFractionalProgressTracker)localObject2);
        if (i3 > 0) {
          break;
        }
        boolean bool2 = ((VastFractionalProgressTracker)localObject1).isTracked();
        if (!bool2) {
          localArrayList2.add(localObject1);
        }
        i1 += 1;
      }
      return localArrayList2;
    }
    return Collections.emptyList();
  }
  
  public VastCompanionAdConfig getVastCompanionAd(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return o;
    case 2: 
      return o;
    }
    return p;
  }
  
  public i getVastIconConfig()
  {
    return r;
  }
  
  public VideoViewabilityTracker getVideoViewabilityTracker()
  {
    return x;
  }
  
  public void handleClickForResult(Activity paramActivity, int paramInt1, int paramInt2)
  {
    Integer localInteger = Integer.valueOf(paramInt2);
    a(paramActivity, paramInt1, localInteger);
  }
  
  public void handleClickWithoutResult(Context paramContext, int paramInt)
  {
    paramContext = paramContext.getApplicationContext();
    a(paramContext, paramInt, null);
  }
  
  public void handleClose(Context paramContext, int paramInt)
  {
    Preconditions.checkNotNull(paramContext, "context cannot be null");
    ArrayList localArrayList = g;
    Object localObject = Integer.valueOf(paramInt);
    String str = l;
    TrackingRequest.makeVastTrackingHttpRequest(localArrayList, null, (Integer)localObject, str, paramContext);
    localArrayList = h;
    Integer localInteger = Integer.valueOf(paramInt);
    localObject = l;
    TrackingRequest.makeVastTrackingHttpRequest(localArrayList, null, localInteger, (String)localObject, paramContext);
  }
  
  public void handleComplete(Context paramContext, int paramInt)
  {
    Preconditions.checkNotNull(paramContext, "context cannot be null");
    ArrayList localArrayList = f;
    Integer localInteger = Integer.valueOf(paramInt);
    String str = l;
    TrackingRequest.makeVastTrackingHttpRequest(localArrayList, null, localInteger, str, paramContext);
  }
  
  public void handleError(Context paramContext, VastErrorCode paramVastErrorCode, int paramInt)
  {
    Preconditions.checkNotNull(paramContext, "context cannot be null");
    ArrayList localArrayList = j;
    Integer localInteger = Integer.valueOf(paramInt);
    String str = l;
    TrackingRequest.makeVastTrackingHttpRequest(localArrayList, paramVastErrorCode, localInteger, str, paramContext);
  }
  
  public void handleImpression(Context paramContext, int paramInt)
  {
    Preconditions.checkNotNull(paramContext, "context cannot be null");
    ArrayList localArrayList = a;
    Integer localInteger = Integer.valueOf(paramInt);
    String str = l;
    TrackingRequest.makeVastTrackingHttpRequest(localArrayList, null, localInteger, str, paramContext);
  }
  
  public void handlePause(Context paramContext, int paramInt)
  {
    Preconditions.checkNotNull(paramContext, "context cannot be null");
    ArrayList localArrayList = d;
    Integer localInteger = Integer.valueOf(paramInt);
    String str = l;
    TrackingRequest.makeVastTrackingHttpRequest(localArrayList, null, localInteger, str, paramContext);
  }
  
  public void handleResume(Context paramContext, int paramInt)
  {
    Preconditions.checkNotNull(paramContext, "context cannot be null");
    ArrayList localArrayList = e;
    Integer localInteger = Integer.valueOf(paramInt);
    String str = l;
    TrackingRequest.makeVastTrackingHttpRequest(localArrayList, null, localInteger, str, paramContext);
  }
  
  public boolean hasCompanionAd()
  {
    VastCompanionAdConfig localVastCompanionAdConfig = o;
    if (localVastCompanionAdConfig != null)
    {
      localVastCompanionAdConfig = p;
      if (localVastCompanionAdConfig != null) {
        return true;
      }
    }
    return false;
  }
  
  public boolean isCustomForceOrientationSet()
  {
    return E;
  }
  
  public boolean isRewardedVideo()
  {
    return s;
  }
  
  public void setClickThroughUrl(String paramString)
  {
    k = paramString;
  }
  
  public void setCustomCloseIconUrl(String paramString)
  {
    if (paramString != null) {
      v = paramString;
    }
  }
  
  public void setCustomCtaText(String paramString)
  {
    if (paramString != null) {
      t = paramString;
    }
  }
  
  public void setCustomForceOrientation(DeviceUtils.ForceOrientation paramForceOrientation)
  {
    if (paramForceOrientation != null)
    {
      DeviceUtils.ForceOrientation localForceOrientation = DeviceUtils.ForceOrientation.UNDEFINED;
      if (paramForceOrientation != localForceOrientation)
      {
        w = paramForceOrientation;
        boolean bool = true;
        E = bool;
      }
    }
  }
  
  public void setCustomSkipText(String paramString)
  {
    if (paramString != null) {
      u = paramString;
    }
  }
  
  public void setDiskMediaFileUrl(String paramString)
  {
    m = paramString;
  }
  
  public void setDspCreativeId(String paramString)
  {
    B = paramString;
  }
  
  public void setIsRewardedVideo(boolean paramBoolean)
  {
    s = paramBoolean;
  }
  
  public void setNetworkMediaFileUrl(String paramString)
  {
    l = paramString;
  }
  
  public void setPrivacyInformationIconClickthroughUrl(String paramString)
  {
    D = paramString;
  }
  
  public void setPrivacyInformationIconImageUrl(String paramString)
  {
    C = paramString;
  }
  
  public void setSkipOffset(String paramString)
  {
    if (paramString != null) {
      n = paramString;
    }
  }
  
  public void setSocialActionsCompanionAds(Map paramMap)
  {
    q = paramMap;
  }
  
  public void setVastCompanionAd(VastCompanionAdConfig paramVastCompanionAdConfig1, VastCompanionAdConfig paramVastCompanionAdConfig2)
  {
    o = paramVastCompanionAdConfig1;
    p = paramVastCompanionAdConfig2;
  }
  
  public void setVastIconConfig(i parami)
  {
    r = parami;
  }
  
  public void setVideoViewabilityTracker(VideoViewabilityTracker paramVideoViewabilityTracker)
  {
    if (paramVideoViewabilityTracker != null) {
      x = paramVideoViewabilityTracker;
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastVideoConfig
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */