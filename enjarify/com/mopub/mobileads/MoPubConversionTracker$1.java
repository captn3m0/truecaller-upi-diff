package com.mopub.mobileads;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.mopub.network.TrackingRequest.Listener;
import com.mopub.volley.VolleyError;

final class MoPubConversionTracker$1
  implements TrackingRequest.Listener
{
  MoPubConversionTracker$1(MoPubConversionTracker paramMoPubConversionTracker) {}
  
  public final void onErrorResponse(VolleyError paramVolleyError) {}
  
  public final void onResponse(String paramString)
  {
    paramString = MoPubConversionTracker.c(a).edit();
    String str = MoPubConversionTracker.b(a);
    paramString = paramString.putBoolean(str, true);
    str = MoPubConversionTracker.a(a);
    paramString.putBoolean(str, false).apply();
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.MoPubConversionTracker.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */