package com.mopub.mobileads;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.VideoView;
import com.mopub.common.Preconditions;
import com.mopub.common.logging.MoPubLog;

public abstract class BaseVideoViewController
{
  private final RelativeLayout a;
  private Long b;
  public final BaseVideoViewController.BaseVideoViewControllerListener mBaseVideoViewControllerListener;
  public final Context mContext;
  
  protected BaseVideoViewController(Context paramContext, Long paramLong, BaseVideoViewController.BaseVideoViewControllerListener paramBaseVideoViewControllerListener)
  {
    Preconditions.checkNotNull(paramBaseVideoViewControllerListener);
    mContext = paramContext;
    b = paramLong;
    mBaseVideoViewControllerListener = paramBaseVideoViewControllerListener;
    paramContext = new android/widget/RelativeLayout;
    paramLong = mContext;
    paramContext.<init>(paramLong);
    a = paramContext;
  }
  
  void a(int paramInt1, int paramInt2) {}
  
  final void a(String paramString)
  {
    Long localLong = b;
    if (localLong != null)
    {
      Context localContext = mContext;
      long l = localLong.longValue();
      BaseBroadcastReceiver.broadcastAction(localContext, l, paramString);
      return;
    }
    MoPubLog.w("Tried to broadcast a video event without a broadcast identifier to send to.");
  }
  
  public boolean backButtonEnabled()
  {
    return true;
  }
  
  public ViewGroup getLayout()
  {
    return a;
  }
  
  protected abstract VideoView getVideoView();
  
  protected abstract void onBackPressed();
  
  protected abstract void onConfigurationChanged(Configuration paramConfiguration);
  
  protected void onCreate()
  {
    Object localObject = new android/widget/RelativeLayout$LayoutParams;
    ((RelativeLayout.LayoutParams)localObject).<init>(-1, -2);
    ((RelativeLayout.LayoutParams)localObject).addRule(13);
    RelativeLayout localRelativeLayout = a;
    VideoView localVideoView = getVideoView();
    localRelativeLayout.addView(localVideoView, 0, (ViewGroup.LayoutParams)localObject);
    localObject = mBaseVideoViewControllerListener;
    localRelativeLayout = a;
    ((BaseVideoViewController.BaseVideoViewControllerListener)localObject).onSetContentView(localRelativeLayout);
  }
  
  protected abstract void onDestroy();
  
  protected abstract void onPause();
  
  protected abstract void onResume();
  
  protected abstract void onSaveInstanceState(Bundle paramBundle);
  
  public final void videoCompleted(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      BaseVideoViewController.BaseVideoViewControllerListener localBaseVideoViewControllerListener = mBaseVideoViewControllerListener;
      localBaseVideoViewControllerListener.onFinish();
    }
  }
  
  public final void videoError(boolean paramBoolean)
  {
    MoPubLog.e("Video cannot be played.");
    String str = "com.mopub.action.interstitial.fail";
    a(str);
    if (paramBoolean)
    {
      BaseVideoViewController.BaseVideoViewControllerListener localBaseVideoViewControllerListener = mBaseVideoViewControllerListener;
      localBaseVideoViewControllerListener.onFinish();
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.BaseVideoViewController
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */