package com.mopub.mobileads;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.mopub.common.MoPub;
import com.mopub.common.Preconditions;
import com.mopub.common.SharedPreferencesHelper;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.privacy.ConsentData;
import com.mopub.common.privacy.ConsentStatus;
import com.mopub.common.privacy.PersonalInfoManager;
import com.mopub.network.TrackingRequest;
import com.mopub.network.TrackingRequest.Listener;

public class MoPubConversionTracker
{
  private final Context a;
  private final String b;
  private final String c;
  private SharedPreferences d;
  
  public MoPubConversionTracker(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext);
    paramContext = paramContext.getApplicationContext();
    a = paramContext;
    paramContext = a.getPackageName();
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    ((StringBuilder)localObject).append(paramContext);
    ((StringBuilder)localObject).append(" wantToTrack");
    localObject = ((StringBuilder)localObject).toString();
    b = ((String)localObject);
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    ((StringBuilder)localObject).append(paramContext);
    ((StringBuilder)localObject).append(" tracked");
    paramContext = ((StringBuilder)localObject).toString();
    c = paramContext;
    paramContext = SharedPreferencesHelper.getSharedPreferences(a);
    d = paramContext;
  }
  
  public void reportAppOpen()
  {
    reportAppOpen(false);
  }
  
  public void reportAppOpen(boolean paramBoolean)
  {
    Object localObject1 = MoPub.getPersonalInformationManager();
    if (localObject1 == null)
    {
      MoPubLog.w("Cannot report app open until initialization is done");
      return;
    }
    boolean bool2;
    if (!paramBoolean)
    {
      localObject2 = d;
      localObject3 = c;
      bool1 = false;
      localObject4 = null;
      bool2 = ((SharedPreferences)localObject2).getBoolean((String)localObject3, false);
      if (bool2)
      {
        MoPubLog.d("Conversion already tracked");
        return;
      }
    }
    if (!paramBoolean)
    {
      bool2 = MoPub.canCollectPersonalInformation();
      if (!bool2)
      {
        localObject5 = d.edit();
        localObject1 = b;
        ((SharedPreferences.Editor)localObject5).putBoolean((String)localObject1, true).apply();
        return;
      }
    }
    Object localObject2 = ((PersonalInfoManager)localObject1).getConsentData();
    Object localObject3 = new com/mopub/mobileads/c;
    Object localObject4 = a;
    ((c)localObject3).<init>((Context)localObject4);
    localObject4 = ((PersonalInfoManager)localObject1).gdprApplies();
    localObject3 = ((c)localObject3).withGdprApplies((Boolean)localObject4);
    boolean bool1 = ((ConsentData)localObject2).isForceGdprApplies();
    localObject3 = ((c)localObject3).withForceGdprApplies(bool1);
    localObject1 = ((PersonalInfoManager)localObject1).getPersonalInfoConsentStatus().getValue();
    localObject1 = ((c)localObject3).withCurrentConsentStatus((String)localObject1);
    localObject3 = ((ConsentData)localObject2).getConsentedPrivacyPolicyVersion();
    localObject1 = ((c)localObject1).withConsentedPrivacyPolicyVersion((String)localObject3);
    localObject2 = ((ConsentData)localObject2).getConsentedVendorListVersion();
    Object localObject5 = ((c)localObject1).withConsentedVendorListVersion((String)localObject2).withSessionTracker(paramBoolean).generateUrlString("ads.mopub.com");
    localObject1 = a;
    localObject2 = new com/mopub/mobileads/MoPubConversionTracker$1;
    ((MoPubConversionTracker.1)localObject2).<init>(this);
    TrackingRequest.makeTrackingHttpRequest((String)localObject5, (Context)localObject1, (TrackingRequest.Listener)localObject2);
  }
  
  public boolean shouldTrack()
  {
    Object localObject = MoPub.getPersonalInformationManager();
    if (localObject == null) {
      return false;
    }
    boolean bool = ((PersonalInfoManager)localObject).canCollectPersonalInformation();
    if (bool)
    {
      localObject = d;
      String str = b;
      bool = ((SharedPreferences)localObject).getBoolean(str, false);
      if (bool) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.MoPubConversionTracker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */