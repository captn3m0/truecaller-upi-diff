package com.mopub.mobileads;

import android.content.Context;
import android.text.TextUtils;
import com.mopub.common.Preconditions;
import com.mopub.common.UrlAction;
import com.mopub.common.UrlHandler;
import com.mopub.common.UrlHandler.Builder;
import com.mopub.common.UrlHandler.ResultActions;
import java.io.Serializable;
import java.util.List;

final class i
  implements Serializable
{
  private static final long serialVersionUID;
  final int a;
  final int b;
  final int c;
  final Integer d;
  final n e;
  final List f;
  final List g;
  private final String h;
  
  i(int paramInt1, int paramInt2, Integer paramInteger1, Integer paramInteger2, n paramn, List paramList1, String paramString, List paramList2)
  {
    Preconditions.checkNotNull(paramn);
    Preconditions.checkNotNull(paramList1);
    Preconditions.checkNotNull(paramList2);
    a = paramInt1;
    b = paramInt2;
    if (paramInteger1 == null) {
      paramInt1 = 0;
    } else {
      paramInt1 = paramInteger1.intValue();
    }
    c = paramInt1;
    d = paramInteger2;
    e = paramn;
    f = paramList1;
    h = paramString;
    g = paramList2;
  }
  
  final void a(Context paramContext, String paramString1, String paramString2)
  {
    Preconditions.checkNotNull(paramContext);
    Object localObject1 = e;
    Object localObject2 = h;
    paramString1 = ((n)localObject1).getCorrectClickThroughUrl((String)localObject2, paramString1);
    boolean bool = TextUtils.isEmpty(paramString1);
    if (bool) {
      return;
    }
    localObject1 = new com/mopub/common/UrlHandler$Builder;
    ((UrlHandler.Builder)localObject1).<init>();
    localObject2 = UrlAction.IGNORE_ABOUT_SCHEME;
    UrlAction[] arrayOfUrlAction = new UrlAction[2];
    UrlAction localUrlAction = UrlAction.OPEN_NATIVE_BROWSER;
    arrayOfUrlAction[0] = localUrlAction;
    localUrlAction = UrlAction.OPEN_IN_APP_BROWSER;
    arrayOfUrlAction[1] = localUrlAction;
    localObject1 = ((UrlHandler.Builder)localObject1).withSupportedUrlActions((UrlAction)localObject2, arrayOfUrlAction);
    localObject2 = new com/mopub/mobileads/i$1;
    ((i.1)localObject2).<init>(this, paramString2, paramContext);
    ((UrlHandler.Builder)localObject1).withResultActions((UrlHandler.ResultActions)localObject2).withoutMoPubBrowser().build().handleUrl(paramContext, paramString1);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */