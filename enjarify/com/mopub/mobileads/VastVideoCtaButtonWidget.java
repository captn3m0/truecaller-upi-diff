package com.mopub.mobileads;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Utils;
import com.mopub.mobileads.resource.CtaButtonDrawable;

public class VastVideoCtaButtonWidget
  extends ImageView
{
  CtaButtonDrawable a;
  boolean b;
  boolean c;
  private final RelativeLayout.LayoutParams d;
  private final RelativeLayout.LayoutParams e;
  private boolean f;
  private boolean g;
  private boolean h;
  
  public VastVideoCtaButtonWidget(Context paramContext, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
  {
    super(paramContext);
    f = paramBoolean1;
    g = paramBoolean2;
    h = false;
    paramBoolean2 = (int)Utils.generateUniqueId();
    setId(paramBoolean2);
    paramBoolean1 = Dips.dipsToIntPixels(150.0F, paramContext);
    paramBoolean2 = Dips.dipsToIntPixels(38.0F, paramContext);
    int i = Dips.dipsToIntPixels(16.0F, paramContext);
    CtaButtonDrawable localCtaButtonDrawable = new com/mopub/mobileads/resource/CtaButtonDrawable;
    localCtaButtonDrawable.<init>(paramContext);
    a = localCtaButtonDrawable;
    paramContext = a;
    setImageDrawable(paramContext);
    paramContext = new android/widget/RelativeLayout$LayoutParams;
    paramContext.<init>(paramBoolean1, paramBoolean2);
    d = paramContext;
    d.setMargins(i, i, i, i);
    d.addRule(8, paramInt);
    d.addRule(7, paramInt);
    paramContext = new android/widget/RelativeLayout$LayoutParams;
    paramContext.<init>(paramBoolean1, paramBoolean2);
    e = paramContext;
    e.setMargins(i, i, i, i);
    e.addRule(12);
    e.addRule(11);
    a();
  }
  
  final void a()
  {
    boolean bool = g;
    int j = 8;
    if (!bool)
    {
      setVisibility(j);
      return;
    }
    bool = b;
    if (!bool)
    {
      setVisibility(4);
      return;
    }
    bool = c;
    if (bool)
    {
      bool = f;
      if (bool)
      {
        bool = h;
        if (!bool)
        {
          setVisibility(j);
          return;
        }
      }
    }
    Object localObject = getResources().getConfiguration();
    int i = orientation;
    switch (i)
    {
    default: 
      MoPubLog.d("Unrecognized screen orientation: CTA button widget defaulting to portrait layout");
      localObject = e;
      setLayoutParams((ViewGroup.LayoutParams)localObject);
      break;
    case 3: 
      MoPubLog.d("Screen orientation is deprecated ORIENTATION_SQUARE: CTA button widget defaulting to portrait layout");
      localObject = e;
      setLayoutParams((ViewGroup.LayoutParams)localObject);
      break;
    case 2: 
      localObject = d;
      setLayoutParams((ViewGroup.LayoutParams)localObject);
      break;
    case 1: 
      localObject = e;
      setLayoutParams((ViewGroup.LayoutParams)localObject);
      break;
    case 0: 
      MoPubLog.d("Screen orientation undefined: CTA button widget defaulting to portrait layout");
      localObject = e;
      setLayoutParams((ViewGroup.LayoutParams)localObject);
    }
    setVisibility(0);
  }
  
  String getCtaText()
  {
    return a.getCtaText();
  }
  
  boolean getHasSocialActions()
  {
    return h;
  }
  
  protected void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    a();
  }
  
  void setHasSocialActions(boolean paramBoolean)
  {
    h = paramBoolean;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastVideoCtaButtonWidget
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */