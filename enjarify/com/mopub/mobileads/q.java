package com.mopub.mobileads;

import android.text.TextUtils;
import com.mopub.common.util.DeviceUtils.ForceOrientation;
import com.mopub.mobileads.util.XmlUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

final class q
{
  Document a;
  
  final List a()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject = a;
    if (localObject == null) {
      return localArrayList;
    }
    localObject = ((Document)localObject).getElementsByTagName("Ad");
    int i = 0;
    for (;;)
    {
      int j = ((NodeList)localObject).getLength();
      if (i >= j) {
        break;
      }
      f localf = new com/mopub/mobileads/f;
      Node localNode = ((NodeList)localObject).item(i);
      localf.<init>(localNode);
      localArrayList.add(localf);
      i += 1;
    }
    return localArrayList;
  }
  
  final VastTracker b()
  {
    Object localObject = a;
    VastTracker localVastTracker = null;
    if (localObject == null) {
      return null;
    }
    String str = "Error";
    localObject = XmlUtils.getFirstMatchingStringData((Document)localObject, str);
    boolean bool = TextUtils.isEmpty((CharSequence)localObject);
    if (bool) {
      return null;
    }
    localVastTracker = new com/mopub/mobileads/VastTracker;
    localVastTracker.<init>((String)localObject);
    return localVastTracker;
  }
  
  final List c()
  {
    Object localObject = XmlUtils.getStringDataAsList(a, "MP_TRACKING_URL");
    ArrayList localArrayList = new java/util/ArrayList;
    int i = ((List)localObject).size();
    localArrayList.<init>(i);
    localObject = ((List)localObject).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject).hasNext();
      if (!bool) {
        break;
      }
      String str = (String)((Iterator)localObject).next();
      VastTracker localVastTracker = new com/mopub/mobileads/VastTracker;
      localVastTracker.<init>(str);
      localArrayList.add(localVastTracker);
    }
    return localArrayList;
  }
  
  final String d()
  {
    Object localObject = a;
    String str = "MoPubCtaText";
    localObject = XmlUtils.getFirstMatchingStringData((Document)localObject, str);
    if (localObject != null)
    {
      int i = ((String)localObject).length();
      int j = 15;
      if (i <= j) {
        return (String)localObject;
      }
    }
    return null;
  }
  
  final String e()
  {
    Object localObject = a;
    String str = "MoPubSkipText";
    localObject = XmlUtils.getFirstMatchingStringData((Document)localObject, str);
    if (localObject != null)
    {
      int i = ((String)localObject).length();
      int j = 8;
      if (i <= j) {
        return (String)localObject;
      }
    }
    return null;
  }
  
  final String f()
  {
    return XmlUtils.getFirstMatchingStringData(a, "MoPubCloseIcon");
  }
  
  final DeviceUtils.ForceOrientation g()
  {
    return DeviceUtils.ForceOrientation.getForceOrientation(XmlUtils.getFirstMatchingStringData(a, "MoPubForceOrientation"));
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */