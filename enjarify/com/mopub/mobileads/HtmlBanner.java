package com.mopub.mobileads;

import android.app.Activity;
import android.content.Context;
import com.mopub.common.AdReport;
import com.mopub.common.ExternalViewabilitySessionManager;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.JavaScriptWebViewCallbacks;
import com.mopub.mobileads.factories.HtmlBannerWebViewFactory;
import com.mopub.network.Networking;
import java.lang.ref.WeakReference;
import java.util.Map;

public class HtmlBanner
  extends CustomEventBanner
{
  private HtmlBannerWebView b;
  private ExternalViewabilitySessionManager c;
  private boolean d = false;
  private WeakReference e;
  
  protected final void loadBanner(Context paramContext, CustomEventBanner.CustomEventBannerListener paramCustomEventBannerListener, Map paramMap1, Map paramMap2)
  {
    Object localObject1 = paramMap1.get("banner-impression-pixel-count-enabled");
    boolean bool1 = localObject1 instanceof Boolean;
    if (bool1)
    {
      localObject1 = (Boolean)localObject1;
      bool2 = ((Boolean)localObject1).booleanValue();
      d = bool2;
    }
    localObject1 = "html-response-body";
    boolean bool2 = paramMap2.containsKey(localObject1);
    if (bool2)
    {
      localObject1 = paramMap2.get("html-response-body");
      Object localObject2 = localObject1;
      localObject2 = (String)localObject1;
      paramMap2 = (String)paramMap2.get("clickthrough-url");
      localObject1 = "mopub-intent-ad-report";
      try
      {
        paramMap1 = paramMap1.get(localObject1);
        paramMap1 = (AdReport)paramMap1;
        paramCustomEventBannerListener = HtmlBannerWebViewFactory.create(paramContext, paramMap1, paramCustomEventBannerListener, paramMap2);
        b = paramCustomEventBannerListener;
        paramCustomEventBannerListener = b;
        AdViewController.setShouldHonorServerDimensions(paramCustomEventBannerListener);
        boolean bool3 = paramContext instanceof Activity;
        if (bool3)
        {
          paramContext = (Activity)paramContext;
          paramCustomEventBannerListener = new java/lang/ref/WeakReference;
          paramCustomEventBannerListener.<init>(paramContext);
          e = paramCustomEventBannerListener;
          paramCustomEventBannerListener = new com/mopub/common/ExternalViewabilitySessionManager;
          paramCustomEventBannerListener.<init>(paramContext);
          c = paramCustomEventBannerListener;
          paramCustomEventBannerListener = c;
          paramMap1 = b;
          boolean bool4 = d;
          paramCustomEventBannerListener.createDisplaySession(paramContext, paramMap1, bool4);
        }
        else
        {
          paramContext = "Unable to start viewability session for HTML banner: Context provided was not an Activity.";
          MoPubLog.d(paramContext);
        }
        HtmlBannerWebView localHtmlBannerWebView = b;
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>();
        paramCustomEventBannerListener = Networking.getBaseUrlScheme();
        paramContext.append(paramCustomEventBannerListener);
        paramContext.append("://ads.mopub.com/");
        String str = paramContext.toString();
        localHtmlBannerWebView.loadDataWithBaseURL(str, (String)localObject2, "text/html", "utf-8", null);
        return;
      }
      catch (ClassCastException localClassCastException)
      {
        MoPubLog.e("LocalExtras contained an incorrect type.");
        paramContext = MoPubErrorCode.INTERNAL_ERROR;
        paramCustomEventBannerListener.onBannerFailed(paramContext);
        return;
      }
    }
    paramContext = MoPubErrorCode.NETWORK_INVALID_STATE;
    paramCustomEventBannerListener.onBannerFailed(paramContext);
  }
  
  protected final void onInvalidate()
  {
    Object localObject = c;
    if (localObject != null)
    {
      ((ExternalViewabilitySessionManager)localObject).endDisplaySession();
      c = null;
    }
    localObject = b;
    if (localObject != null)
    {
      ((HtmlBannerWebView)localObject).destroy();
      b = null;
    }
  }
  
  protected final void trackMpxAndThirdPartyImpressions()
  {
    Object localObject = b;
    if (localObject == null) {
      return;
    }
    String str = JavaScriptWebViewCallbacks.WEB_VIEW_DID_APPEAR.getUrl();
    ((HtmlBannerWebView)localObject).loadUrl(str);
    boolean bool = d;
    if (bool)
    {
      localObject = c;
      if (localObject != null)
      {
        localObject = e;
        if (localObject != null)
        {
          localObject = (Activity)((WeakReference)localObject).get();
          if (localObject != null)
          {
            c.startDeferredDisplaySession((Activity)localObject);
            return;
          }
          localObject = "Lost the activity for deferred Viewability tracking. Dropping session.";
          MoPubLog.d((String)localObject);
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.HtmlBanner
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */