package com.mopub.mobileads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.mopub.common.util.Visibility;

final class MoPubView$1
  extends BroadcastReceiver
{
  MoPubView$1(MoPubView paramMoPubView) {}
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    paramContext = a;
    boolean bool1 = Visibility.isScreenVisible(MoPubView.a(paramContext));
    if ((bool1) && (paramIntent != null))
    {
      paramContext = paramIntent.getAction();
      paramIntent = "android.intent.action.USER_PRESENT";
      boolean bool2 = paramIntent.equals(paramContext);
      if (bool2)
      {
        MoPubView.a(a, 0);
        return;
      }
      paramIntent = "android.intent.action.SCREEN_OFF";
      bool1 = paramIntent.equals(paramContext);
      if (bool1)
      {
        paramContext = a;
        int i = 8;
        MoPubView.a(paramContext, i);
      }
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.MoPubView.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */