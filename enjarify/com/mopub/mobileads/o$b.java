package com.mopub.mobileads;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

final class o$b
  implements View.OnTouchListener
{
  private boolean b;
  
  o$b(o paramo) {}
  
  public final boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    int i = paramMotionEvent.getAction();
    paramMotionEvent = null;
    boolean bool;
    switch (i)
    {
    default: 
      break;
    case 1: 
      bool = b;
      if (!bool) {
        return false;
      }
      b = false;
      paramView = a.b;
      if (paramView != null)
      {
        paramView = a.b;
        paramView.onVastWebViewClick();
      }
      break;
    case 0: 
      bool = true;
      b = bool;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.o.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */