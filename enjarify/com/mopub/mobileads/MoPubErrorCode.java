package com.mopub.mobileads;

public enum MoPubErrorCode
  implements MoPubError
{
  private final String a;
  
  static
  {
    Object localObject = new com/mopub/mobileads/MoPubErrorCode;
    ((MoPubErrorCode)localObject).<init>("AD_SUCCESS", 0, "ad successfully loaded.");
    AD_SUCCESS = (MoPubErrorCode)localObject;
    localObject = new com/mopub/mobileads/MoPubErrorCode;
    int i = 1;
    ((MoPubErrorCode)localObject).<init>("DO_NOT_TRACK", i, "Do not track is enabled.");
    DO_NOT_TRACK = (MoPubErrorCode)localObject;
    localObject = new com/mopub/mobileads/MoPubErrorCode;
    int j = 2;
    ((MoPubErrorCode)localObject).<init>("UNSPECIFIED", j, "Unspecified error.");
    UNSPECIFIED = (MoPubErrorCode)localObject;
    localObject = new com/mopub/mobileads/MoPubErrorCode;
    int k = 3;
    ((MoPubErrorCode)localObject).<init>("NO_FILL", k, "No ads found.");
    NO_FILL = (MoPubErrorCode)localObject;
    localObject = new com/mopub/mobileads/MoPubErrorCode;
    int m = 4;
    ((MoPubErrorCode)localObject).<init>("WARMUP", m, "Ad unit is warming up. Try again in a few minutes.");
    WARMUP = (MoPubErrorCode)localObject;
    localObject = new com/mopub/mobileads/MoPubErrorCode;
    int n = 5;
    ((MoPubErrorCode)localObject).<init>("SERVER_ERROR", n, "Unable to connect to MoPub adserver.");
    SERVER_ERROR = (MoPubErrorCode)localObject;
    localObject = new com/mopub/mobileads/MoPubErrorCode;
    int i1 = 6;
    ((MoPubErrorCode)localObject).<init>("INTERNAL_ERROR", i1, "Unable to serve ad due to invalid internal state.");
    INTERNAL_ERROR = (MoPubErrorCode)localObject;
    localObject = new com/mopub/mobileads/MoPubErrorCode;
    int i2 = 7;
    ((MoPubErrorCode)localObject).<init>("CANCELLED", i2, "Ad request was cancelled.");
    CANCELLED = (MoPubErrorCode)localObject;
    localObject = new com/mopub/mobileads/MoPubErrorCode;
    int i3 = 8;
    ((MoPubErrorCode)localObject).<init>("NO_CONNECTION", i3, "No internet connection detected.");
    NO_CONNECTION = (MoPubErrorCode)localObject;
    localObject = new com/mopub/mobileads/MoPubErrorCode;
    int i4 = 9;
    ((MoPubErrorCode)localObject).<init>("ADAPTER_NOT_FOUND", i4, "Unable to find Native Network or Custom Event adapter.");
    ADAPTER_NOT_FOUND = (MoPubErrorCode)localObject;
    localObject = new com/mopub/mobileads/MoPubErrorCode;
    int i5 = 10;
    ((MoPubErrorCode)localObject).<init>("ADAPTER_CONFIGURATION_ERROR", i5, "Native Network or Custom Event adapter was configured incorrectly.");
    ADAPTER_CONFIGURATION_ERROR = (MoPubErrorCode)localObject;
    localObject = new com/mopub/mobileads/MoPubErrorCode;
    int i6 = 11;
    ((MoPubErrorCode)localObject).<init>("EXPIRED", i6, "Ad expired since it was not shown within 4 hours.");
    EXPIRED = (MoPubErrorCode)localObject;
    localObject = new com/mopub/mobileads/MoPubErrorCode;
    ((MoPubErrorCode)localObject).<init>("NETWORK_TIMEOUT", 12, "Third-party network failed to respond in a timely manner.");
    NETWORK_TIMEOUT = (MoPubErrorCode)localObject;
    localObject = new com/mopub/mobileads/MoPubErrorCode;
    ((MoPubErrorCode)localObject).<init>("NETWORK_NO_FILL", 13, "Third-party network failed to provide an ad.");
    NETWORK_NO_FILL = (MoPubErrorCode)localObject;
    localObject = new com/mopub/mobileads/MoPubErrorCode;
    ((MoPubErrorCode)localObject).<init>("NETWORK_INVALID_STATE", 14, "Third-party network failed due to invalid internal state.");
    NETWORK_INVALID_STATE = (MoPubErrorCode)localObject;
    localObject = new com/mopub/mobileads/MoPubErrorCode;
    ((MoPubErrorCode)localObject).<init>("MRAID_LOAD_ERROR", 15, "Error loading MRAID ad.");
    MRAID_LOAD_ERROR = (MoPubErrorCode)localObject;
    localObject = new com/mopub/mobileads/MoPubErrorCode;
    ((MoPubErrorCode)localObject).<init>("VIDEO_CACHE_ERROR", 16, "Error creating a cache to store downloaded videos.");
    VIDEO_CACHE_ERROR = (MoPubErrorCode)localObject;
    localObject = new com/mopub/mobileads/MoPubErrorCode;
    ((MoPubErrorCode)localObject).<init>("VIDEO_DOWNLOAD_ERROR", 17, "Error downloading video.");
    VIDEO_DOWNLOAD_ERROR = (MoPubErrorCode)localObject;
    localObject = new com/mopub/mobileads/MoPubErrorCode;
    ((MoPubErrorCode)localObject).<init>("GDPR_DOES_NOT_APPLY", 18, "GDPR does not apply. Ignoring consent-related actions.");
    GDPR_DOES_NOT_APPLY = (MoPubErrorCode)localObject;
    localObject = new com/mopub/mobileads/MoPubErrorCode;
    ((MoPubErrorCode)localObject).<init>("REWARDED_CURRENCIES_PARSING_ERROR", 19, "Error parsing rewarded currencies JSON header.");
    REWARDED_CURRENCIES_PARSING_ERROR = (MoPubErrorCode)localObject;
    localObject = new com/mopub/mobileads/MoPubErrorCode;
    ((MoPubErrorCode)localObject).<init>("REWARD_NOT_SELECTED", 20, "Reward not selected for rewarded ad.");
    REWARD_NOT_SELECTED = (MoPubErrorCode)localObject;
    localObject = new com/mopub/mobileads/MoPubErrorCode;
    ((MoPubErrorCode)localObject).<init>("VIDEO_NOT_AVAILABLE", 21, "No video loaded for ad unit.");
    VIDEO_NOT_AVAILABLE = (MoPubErrorCode)localObject;
    localObject = new com/mopub/mobileads/MoPubErrorCode;
    ((MoPubErrorCode)localObject).<init>("VIDEO_PLAYBACK_ERROR", 22, "Error playing a video.");
    VIDEO_PLAYBACK_ERROR = (MoPubErrorCode)localObject;
    localObject = new MoPubErrorCode[23];
    MoPubErrorCode localMoPubErrorCode = AD_SUCCESS;
    localObject[0] = localMoPubErrorCode;
    localMoPubErrorCode = DO_NOT_TRACK;
    localObject[i] = localMoPubErrorCode;
    localMoPubErrorCode = UNSPECIFIED;
    localObject[j] = localMoPubErrorCode;
    localMoPubErrorCode = NO_FILL;
    localObject[k] = localMoPubErrorCode;
    localMoPubErrorCode = WARMUP;
    localObject[m] = localMoPubErrorCode;
    localMoPubErrorCode = SERVER_ERROR;
    localObject[n] = localMoPubErrorCode;
    localMoPubErrorCode = INTERNAL_ERROR;
    localObject[i1] = localMoPubErrorCode;
    localMoPubErrorCode = CANCELLED;
    localObject[i2] = localMoPubErrorCode;
    localMoPubErrorCode = NO_CONNECTION;
    localObject[i3] = localMoPubErrorCode;
    localMoPubErrorCode = ADAPTER_NOT_FOUND;
    localObject[i4] = localMoPubErrorCode;
    localMoPubErrorCode = ADAPTER_CONFIGURATION_ERROR;
    localObject[i5] = localMoPubErrorCode;
    localMoPubErrorCode = EXPIRED;
    localObject[i6] = localMoPubErrorCode;
    localMoPubErrorCode = NETWORK_TIMEOUT;
    localObject[12] = localMoPubErrorCode;
    localMoPubErrorCode = NETWORK_NO_FILL;
    localObject[13] = localMoPubErrorCode;
    localMoPubErrorCode = NETWORK_INVALID_STATE;
    localObject[14] = localMoPubErrorCode;
    localMoPubErrorCode = MRAID_LOAD_ERROR;
    localObject[15] = localMoPubErrorCode;
    localMoPubErrorCode = VIDEO_CACHE_ERROR;
    localObject[16] = localMoPubErrorCode;
    localMoPubErrorCode = VIDEO_DOWNLOAD_ERROR;
    localObject[17] = localMoPubErrorCode;
    localMoPubErrorCode = GDPR_DOES_NOT_APPLY;
    localObject[18] = localMoPubErrorCode;
    localMoPubErrorCode = REWARDED_CURRENCIES_PARSING_ERROR;
    localObject[19] = localMoPubErrorCode;
    localMoPubErrorCode = REWARD_NOT_SELECTED;
    localObject[20] = localMoPubErrorCode;
    localMoPubErrorCode = VIDEO_NOT_AVAILABLE;
    localObject[21] = localMoPubErrorCode;
    localMoPubErrorCode = VIDEO_PLAYBACK_ERROR;
    localObject[22] = localMoPubErrorCode;
    b = (MoPubErrorCode[])localObject;
  }
  
  private MoPubErrorCode(String paramString1)
  {
    a = paramString1;
  }
  
  public final int getIntCode()
  {
    int[] arrayOfInt = MoPubErrorCode.1.a;
    int i = ordinal();
    int j = arrayOfInt[i];
    switch (j)
    {
    default: 
      return 10000;
    case 3: 
      return 0;
    case 2: 
      return 1;
    }
    return 2;
  }
  
  public final String toString()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.MoPubErrorCode
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */