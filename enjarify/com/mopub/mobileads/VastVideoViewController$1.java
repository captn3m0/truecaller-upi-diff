package com.mopub.mobileads;

import android.app.Activity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import com.mopub.common.ExternalViewabilitySession.VideoEvent;
import com.mopub.common.ExternalViewabilitySessionManager;

final class VastVideoViewController$1
  implements View.OnTouchListener
{
  VastVideoViewController$1(VastVideoViewController paramVastVideoViewController, Activity paramActivity) {}
  
  public final boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    int i = paramMotionEvent.getAction();
    int j = 1;
    if (i == j)
    {
      paramView = b;
      boolean bool1 = VastVideoViewController.a(paramView);
      if (bool1)
      {
        paramView = VastVideoViewController.b(b);
        Object localObject1 = ExternalViewabilitySession.VideoEvent.AD_CLICK_THRU;
        int k = b.a.getCurrentPosition();
        paramView.recordVideoEvent((ExternalViewabilitySession.VideoEvent)localObject1, k);
        VastVideoViewController.c(b);
        b.a("com.mopub.action.interstitial.click");
        paramView = VastVideoViewController.f(b);
        localObject1 = a;
        Object localObject2 = b;
        boolean bool2 = VastVideoViewController.d((VastVideoViewController)localObject2);
        int m;
        if (bool2)
        {
          localObject2 = b;
          m = VastVideoViewController.e((VastVideoViewController)localObject2);
        }
        else
        {
          localObject2 = b.a;
          m = ((VastVideoView)localObject2).getCurrentPosition();
        }
        paramView.handleClickForResult((Activity)localObject1, m, j);
      }
    }
    return j;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastVideoViewController.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */