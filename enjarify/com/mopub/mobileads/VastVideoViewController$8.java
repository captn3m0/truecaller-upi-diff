package com.mopub.mobileads;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import com.mopub.common.ExternalViewabilitySession.VideoEvent;
import com.mopub.common.ExternalViewabilitySessionManager;

final class VastVideoViewController$8
  implements View.OnTouchListener
{
  VastVideoViewController$8(VastVideoViewController paramVastVideoViewController) {}
  
  public final boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    paramView = a;
    boolean bool1 = VastVideoViewController.d(paramView);
    int i;
    if (bool1)
    {
      paramView = a;
      i = VastVideoViewController.e(paramView);
    }
    else
    {
      paramView = a.a;
      i = paramView.getCurrentPosition();
    }
    int j = paramMotionEvent.getAction();
    int k = 1;
    if (j == k)
    {
      VastVideoViewController.c(a);
      paramMotionEvent = a;
      boolean bool2 = VastVideoViewController.d(paramMotionEvent);
      if (!bool2)
      {
        paramMotionEvent = VastVideoViewController.b(a);
        localObject = ExternalViewabilitySession.VideoEvent.AD_SKIPPED;
        VastVideoView localVastVideoView = a.a;
        int m = localVastVideoView.getCurrentPosition();
        paramMotionEvent.recordVideoEvent((ExternalViewabilitySession.VideoEvent)localObject, m);
      }
      paramMotionEvent = VastVideoViewController.f(a);
      Object localObject = a.mContext;
      paramMotionEvent.handleClose((Context)localObject, i);
      paramView = a.mBaseVideoViewControllerListener;
      paramView.onFinish();
    }
    return k;
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastVideoViewController.8
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */