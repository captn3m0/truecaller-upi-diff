package com.mopub.mobileads;

import android.content.Context;
import com.mopub.network.TrackingRequest;
import java.util.List;

final class VastVideoViewController$11
  implements o.a
{
  VastVideoViewController$11(VastVideoViewController paramVastVideoViewController, VastCompanionAdConfig paramVastCompanionAdConfig, Context paramContext) {}
  
  public final void onVastWebViewClick()
  {
    c.a("com.mopub.action.interstitial.click");
    Object localObject1 = a.getClickTrackers();
    Object localObject2 = Integer.valueOf(VastVideoViewController.e(c));
    Object localObject3 = b;
    TrackingRequest.makeVastTrackingHttpRequest((List)localObject1, null, (Integer)localObject2, null, (Context)localObject3);
    localObject1 = a;
    localObject2 = b;
    localObject3 = VastVideoViewController.f(c).getDspCreativeId();
    ((VastCompanionAdConfig)localObject1).a((Context)localObject2, null, (String)localObject3);
  }
}

/* Location:
 * Qualified Name:     com.mopub.mobileads.VastVideoViewController.11
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */