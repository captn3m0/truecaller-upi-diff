package com.mopub.exceptions;

public class UrlParseException
  extends Exception
{
  public UrlParseException(String paramString)
  {
    super(paramString);
  }
  
  public UrlParseException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}

/* Location:
 * Qualified Name:     com.mopub.exceptions.UrlParseException
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */