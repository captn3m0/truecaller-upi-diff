package com.mopub.exceptions;

public class IntentNotResolvableException
  extends Exception
{
  public IntentNotResolvableException(String paramString)
  {
    super(paramString);
  }
  
  public IntentNotResolvableException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}

/* Location:
 * Qualified Name:     com.mopub.exceptions.IntentNotResolvableException
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */