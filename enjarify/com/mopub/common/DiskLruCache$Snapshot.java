package com.mopub.common;

import java.io.Closeable;
import java.io.InputStream;

public final class DiskLruCache$Snapshot
  implements Closeable
{
  private final String b;
  private final long c;
  private final InputStream[] d;
  private final long[] e;
  
  private DiskLruCache$Snapshot(DiskLruCache paramDiskLruCache, String paramString, long paramLong, InputStream[] paramArrayOfInputStream, long[] paramArrayOfLong)
  {
    b = paramString;
    c = paramLong;
    d = paramArrayOfInputStream;
    e = paramArrayOfLong;
  }
  
  public final void close()
  {
    InputStream[] arrayOfInputStream = d;
    int i = arrayOfInputStream.length;
    int j = 0;
    while (j < i)
    {
      InputStream localInputStream = arrayOfInputStream[j];
      DiskLruCacheUtil.a(localInputStream);
      j += 1;
    }
  }
  
  public final DiskLruCache.Editor edit()
  {
    DiskLruCache localDiskLruCache = a;
    String str = b;
    long l = c;
    return DiskLruCache.a(localDiskLruCache, str, l);
  }
  
  public final InputStream getInputStream(int paramInt)
  {
    return d[paramInt];
  }
  
  public final long getLength(int paramInt)
  {
    return e[paramInt];
  }
  
  public final String getString(int paramInt)
  {
    return DiskLruCache.a(getInputStream(paramInt));
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.DiskLruCache.Snapshot
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */