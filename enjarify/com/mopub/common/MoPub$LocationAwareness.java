package com.mopub.common;

public enum MoPub$LocationAwareness
{
  static
  {
    Object localObject = new com/mopub/common/MoPub$LocationAwareness;
    ((LocationAwareness)localObject).<init>("NORMAL", 0);
    NORMAL = (LocationAwareness)localObject;
    localObject = new com/mopub/common/MoPub$LocationAwareness;
    int i = 1;
    ((LocationAwareness)localObject).<init>("TRUNCATED", i);
    TRUNCATED = (LocationAwareness)localObject;
    localObject = new com/mopub/common/MoPub$LocationAwareness;
    int j = 2;
    ((LocationAwareness)localObject).<init>("DISABLED", j);
    DISABLED = (LocationAwareness)localObject;
    localObject = new LocationAwareness[3];
    LocationAwareness localLocationAwareness = NORMAL;
    localObject[0] = localLocationAwareness;
    localLocationAwareness = TRUNCATED;
    localObject[i] = localLocationAwareness;
    localLocationAwareness = DISABLED;
    localObject[j] = localLocationAwareness;
    a = (LocationAwareness[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.MoPub.LocationAwareness
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */