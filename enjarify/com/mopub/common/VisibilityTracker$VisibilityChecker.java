package com.mopub.common;

import android.graphics.Rect;
import android.os.SystemClock;
import android.view.View;

public class VisibilityTracker$VisibilityChecker
{
  private final Rect a;
  
  public VisibilityTracker$VisibilityChecker()
  {
    Rect localRect = new android/graphics/Rect;
    localRect.<init>();
    a = localRect;
  }
  
  public boolean hasRequiredTimeElapsed(long paramLong, int paramInt)
  {
    long l = SystemClock.uptimeMillis() - paramLong;
    paramLong = paramInt;
    paramInt = l < paramLong;
    return paramInt >= 0;
  }
  
  public boolean isVisible(View paramView1, View paramView2, int paramInt, Integer paramInteger)
  {
    if (paramView2 != null)
    {
      int i = paramView2.getVisibility();
      if (i == 0)
      {
        paramView1 = paramView1.getParent();
        if (paramView1 != null)
        {
          paramView1 = a;
          boolean bool1 = paramView2.getGlobalVisibleRect(paramView1);
          if (!bool1) {
            return false;
          }
          long l1 = a.height();
          paramView1 = a;
          long l2 = paramView1.width();
          l1 *= l2;
          l2 = paramView2.getHeight();
          int j = paramView2.getWidth();
          long l3 = j;
          l2 *= l3;
          l3 = 0L;
          boolean bool2 = l2 < l3;
          if (!bool2) {
            return false;
          }
          j = 1;
          if (paramInteger != null)
          {
            int k = paramInteger.intValue();
            if (k > 0)
            {
              k = paramInteger.intValue();
              l4 = k;
              bool3 = l1 < l4;
              if (!bool3) {
                return j;
              }
              return false;
            }
          }
          long l5 = 100;
          l1 *= l5;
          long l4 = paramInt * l2;
          boolean bool3 = l1 < l4;
          if (!bool3) {
            return j;
          }
          return false;
        }
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.VisibilityTracker.VisibilityChecker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */