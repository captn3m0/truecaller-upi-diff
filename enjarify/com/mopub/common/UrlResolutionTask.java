package com.mopub.common;

import android.net.Uri;
import android.os.AsyncTask;
import com.google.firebase.perf.network.FirebasePerfUrlConnection;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.AsyncTasks;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;

public class UrlResolutionTask
  extends AsyncTask
{
  private final UrlResolutionTask.a a;
  
  private UrlResolutionTask(UrlResolutionTask.a parama)
  {
    a = parama;
  }
  
  private static String a(String paramString)
  {
    Object localObject1 = new java/net/URL;
    ((URL)localObject1).<init>(paramString);
    Object localObject2 = null;
    try
    {
      localObject1 = ((URL)localObject1).openConnection();
      localObject1 = FirebasePerfUrlConnection.instrument(localObject1);
      localObject1 = (URLConnection)localObject1;
      localObject1 = (HttpURLConnection)localObject1;
      localObject2 = null;
      try
      {
        ((HttpURLConnection)localObject1).setInstanceFollowRedirects(false);
        paramString = a(paramString, (HttpURLConnection)localObject1);
        if (localObject1 != null)
        {
          localObject2 = ((HttpURLConnection)localObject1).getInputStream();
          if (localObject2 != null) {
            try
            {
              ((InputStream)localObject2).close();
            }
            catch (IOException localIOException1)
            {
              localObject2 = "IOException when closing httpUrlConnection. Ignoring.";
              MoPubLog.d((String)localObject2);
            }
          }
          ((HttpURLConnection)localObject1).disconnect();
        }
        return paramString;
      }
      finally
      {
        localObject2 = localObject1;
      }
      if (localObject2 == null) {
        break label117;
      }
    }
    finally {}
    localObject1 = ((HttpURLConnection)localObject2).getInputStream();
    if (localObject1 != null) {
      try
      {
        ((InputStream)localObject1).close();
      }
      catch (IOException localIOException2)
      {
        localObject1 = "IOException when closing httpUrlConnection. Ignoring.";
        MoPubLog.d((String)localObject1);
      }
    }
    ((HttpURLConnection)localObject2).disconnect();
    label117:
    throw paramString;
  }
  
  private static String a(String paramString, HttpURLConnection paramHttpURLConnection)
  {
    URI localURI = new java/net/URI;
    localURI.<init>(paramString);
    int i = paramHttpURLConnection.getResponseCode();
    String str = "location";
    paramHttpURLConnection = paramHttpURLConnection.getHeaderField(str);
    int j = 300;
    if (i >= j)
    {
      j = 400;
      if (i < j) {
        try
        {
          localURI = localURI.resolve(paramHttpURLConnection);
          paramString = localURI.toString();
        }
        catch (NullPointerException localNullPointerException)
        {
          StringBuilder localStringBuilder2 = new java/lang/StringBuilder;
          localStringBuilder2.<init>("Invalid URL redirection. baseUrl=");
          localStringBuilder2.append(paramString);
          localStringBuilder2.append("\n redirectUrl=");
          localStringBuilder2.append(paramHttpURLConnection);
          MoPubLog.d(localStringBuilder2.toString());
          throw localNullPointerException;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
          StringBuilder localStringBuilder1 = new java/lang/StringBuilder;
          localStringBuilder1.<init>("Invalid URL redirection. baseUrl=");
          localStringBuilder1.append(paramString);
          localStringBuilder1.append("\n redirectUrl=");
          localStringBuilder1.append(paramHttpURLConnection);
          MoPubLog.d(localStringBuilder1.toString());
          paramString = new java/net/URISyntaxException;
          paramString.<init>(paramHttpURLConnection, "Unable to parse invalid URL");
          throw paramString;
        }
      }
    }
    paramString = null;
    return paramString;
  }
  
  private static String a(String... paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      int i = paramVarArgs.length;
      if (i != 0)
      {
        i = 0;
        try
        {
          paramVarArgs = paramVarArgs[0];
          boolean bool = false;
          Object localObject1 = null;
          while (paramVarArgs != null)
          {
            int j = 10;
            if (i >= j) {
              break;
            }
            localObject1 = UrlAction.OPEN_IN_APP_BROWSER;
            Uri localUri = Uri.parse(paramVarArgs);
            bool = ((UrlAction)localObject1).shouldTryHandlingUrl(localUri);
            if (!bool) {
              return paramVarArgs;
            }
            localObject1 = UrlAction.OPEN_NATIVE_BROWSER;
            localUri = Uri.parse(paramVarArgs);
            bool = ((UrlAction)localObject1).shouldTryHandlingUrl(localUri);
            if (bool) {
              return paramVarArgs;
            }
            localObject1 = a(paramVarArgs);
            i += 1;
            Object localObject2 = localObject1;
            localObject1 = paramVarArgs;
            paramVarArgs = (String[])localObject2;
          }
          return (String)localObject1;
        }
        catch (NullPointerException localNullPointerException)
        {
          return null;
        }
        catch (URISyntaxException localURISyntaxException)
        {
          return null;
        }
        catch (IOException localIOException)
        {
          return null;
        }
      }
    }
    return null;
  }
  
  public static void getResolvedUrl(String paramString, UrlResolutionTask.a parama)
  {
    UrlResolutionTask localUrlResolutionTask = new com/mopub/common/UrlResolutionTask;
    localUrlResolutionTask.<init>(parama);
    int i = 1;
    try
    {
      String[] arrayOfString = new String[i];
      arrayOfString[0] = paramString;
      AsyncTasks.safeExecuteOnExecutor(localUrlResolutionTask, arrayOfString);
      return;
    }
    catch (Exception paramString)
    {
      parama.onFailure("Failed to resolve url", paramString);
    }
  }
  
  protected void onCancelled()
  {
    super.onCancelled();
    a.onFailure("Task for resolving url was cancelled", null);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.UrlResolutionTask
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */