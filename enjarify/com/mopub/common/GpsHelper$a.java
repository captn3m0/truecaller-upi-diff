package com.mopub.common;

import android.content.Context;
import android.os.AsyncTask;
import com.mopub.common.factories.MethodBuilderFactory;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Reflection.MethodBuilder;
import java.lang.ref.WeakReference;

final class GpsHelper$a
  extends AsyncTask
{
  private WeakReference a;
  private WeakReference b;
  
  public GpsHelper$a(Context paramContext, GpsHelper.GpsHelperListener paramGpsHelperListener)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramContext);
    a = localWeakReference;
    paramContext = new java/lang/ref/WeakReference;
    paramContext.<init>(paramGpsHelperListener);
    b = paramContext;
  }
  
  private Void a()
  {
    try
    {
      localObject1 = a;
      localObject1 = ((WeakReference)localObject1).get();
      localObject1 = (Context)localObject1;
      if (localObject1 == null) {
        return null;
      }
      Object localObject2 = "getAdvertisingIdInfo";
      localObject2 = MethodBuilderFactory.create(null, (String)localObject2);
      Object localObject3 = GpsHelper.a();
      localObject3 = Class.forName((String)localObject3);
      localObject2 = ((Reflection.MethodBuilder)localObject2).setStatic((Class)localObject3);
      localObject3 = Context.class;
      localObject1 = ((Reflection.MethodBuilder)localObject2).addParam((Class)localObject3, localObject1);
      ((Reflection.MethodBuilder)localObject1).execute();
    }
    catch (Exception localException)
    {
      Object localObject1 = "Unable to obtain Google AdvertisingIdClient.Info via reflection.";
      MoPubLog.d((String)localObject1);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.GpsHelper.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */