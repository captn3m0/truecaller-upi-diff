package com.mopub.common.logging;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class MoPubLog
{
  public static final String LOGGER_NAMESPACE = "com.mopub";
  private static final Logger a = Logger.getLogger("com.mopub");
  private static final MoPubLog.a b;
  
  static
  {
    Object localObject1 = new com/mopub/common/logging/MoPubLog$a;
    int i = 0;
    ((MoPubLog.a)localObject1).<init>((byte)0);
    b = (MoPubLog.a)localObject1;
    a.setUseParentHandlers(false);
    localObject1 = a;
    Object localObject2 = Level.ALL;
    ((Logger)localObject1).setLevel((Level)localObject2);
    localObject1 = b;
    localObject2 = Level.FINE;
    ((MoPubLog.a)localObject1).setLevel((Level)localObject2);
    localObject1 = LogManager.getLogManager();
    localObject2 = a;
    ((LogManager)localObject1).addLogger((Logger)localObject2);
    localObject1 = a;
    localObject2 = b;
    Handler[] arrayOfHandler = ((Logger)localObject1).getHandlers();
    int j = arrayOfHandler.length;
    while (i < j)
    {
      Handler localHandler = arrayOfHandler[i];
      boolean bool = localHandler.equals(localObject2);
      if (bool) {
        return;
      }
      i += 1;
    }
    ((Logger)localObject1).addHandler((Handler)localObject2);
  }
  
  public static void c(String paramString)
  {
    c(paramString, null);
  }
  
  public static void c(String paramString, Throwable paramThrowable)
  {
    Logger localLogger = a;
    Level localLevel = Level.FINEST;
    localLogger.log(localLevel, paramString, paramThrowable);
  }
  
  public static void d(String paramString)
  {
    d(paramString, null);
  }
  
  public static void d(String paramString, Throwable paramThrowable)
  {
    Logger localLogger = a;
    Level localLevel = Level.CONFIG;
    localLogger.log(localLevel, paramString, paramThrowable);
  }
  
  public static void e(String paramString)
  {
    e(paramString, null);
  }
  
  public static void e(String paramString, Throwable paramThrowable)
  {
    Logger localLogger = a;
    Level localLevel = Level.SEVERE;
    localLogger.log(localLevel, paramString, paramThrowable);
  }
  
  public static void i(String paramString)
  {
    i(paramString, null);
  }
  
  public static void i(String paramString, Throwable paramThrowable)
  {
    Logger localLogger = a;
    Level localLevel = Level.INFO;
    localLogger.log(localLevel, paramString, paramThrowable);
  }
  
  public static void setSdkHandlerLevel(Level paramLevel)
  {
    b.setLevel(paramLevel);
  }
  
  public static void v(String paramString)
  {
    v(paramString, null);
  }
  
  public static void v(String paramString, Throwable paramThrowable)
  {
    Logger localLogger = a;
    Level localLevel = Level.FINE;
    localLogger.log(localLevel, paramString, paramThrowable);
  }
  
  public static void w(String paramString)
  {
    w(paramString, null);
  }
  
  public static void w(String paramString, Throwable paramThrowable)
  {
    Logger localLogger = a;
    Level localLevel = Level.WARNING;
    localLogger.log(localLevel, paramString, paramThrowable);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.logging.MoPubLog
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */