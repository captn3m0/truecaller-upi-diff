package com.mopub.common.logging;

import android.util.Log;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

final class MoPubLog$a
  extends Handler
{
  private static final Map a;
  
  static
  {
    Object localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>(7);
    a = (Map)localObject;
    Level localLevel = Level.FINEST;
    int i = 2;
    Integer localInteger1 = Integer.valueOf(i);
    ((Map)localObject).put(localLevel, localInteger1);
    localObject = a;
    localLevel = Level.FINER;
    localInteger1 = Integer.valueOf(i);
    ((Map)localObject).put(localLevel, localInteger1);
    localObject = a;
    localLevel = Level.FINE;
    Integer localInteger2 = Integer.valueOf(i);
    ((Map)localObject).put(localLevel, localInteger2);
    localObject = a;
    localLevel = Level.CONFIG;
    localInteger2 = Integer.valueOf(3);
    ((Map)localObject).put(localLevel, localInteger2);
    localObject = a;
    localLevel = Level.INFO;
    localInteger2 = Integer.valueOf(4);
    ((Map)localObject).put(localLevel, localInteger2);
    localObject = a;
    localLevel = Level.WARNING;
    localInteger2 = Integer.valueOf(5);
    ((Map)localObject).put(localLevel, localInteger2);
    localObject = a;
    localLevel = Level.SEVERE;
    localInteger2 = Integer.valueOf(6);
    ((Map)localObject).put(localLevel, localInteger2);
  }
  
  public final void close() {}
  
  public final void flush() {}
  
  public final void publish(LogRecord paramLogRecord)
  {
    boolean bool = isLoggable(paramLogRecord);
    if (bool)
    {
      Object localObject1 = a;
      Object localObject2 = paramLogRecord.getLevel();
      bool = ((Map)localObject1).containsKey(localObject2);
      int i;
      if (bool)
      {
        localObject1 = a;
        localObject2 = paramLogRecord.getLevel();
        localObject1 = (Integer)((Map)localObject1).get(localObject2);
        i = ((Integer)localObject1).intValue();
      }
      else
      {
        i = 2;
      }
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      Object localObject3 = paramLogRecord.getMessage();
      ((StringBuilder)localObject2).append((String)localObject3);
      localObject3 = "\n";
      ((StringBuilder)localObject2).append((String)localObject3);
      localObject2 = ((StringBuilder)localObject2).toString();
      paramLogRecord = paramLogRecord.getThrown();
      if (paramLogRecord != null)
      {
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        ((StringBuilder)localObject3).append((String)localObject2);
        paramLogRecord = Log.getStackTraceString(paramLogRecord);
        ((StringBuilder)localObject3).append(paramLogRecord);
        localObject2 = ((StringBuilder)localObject3).toString();
      }
      paramLogRecord = "MoPub";
      Log.println(i, paramLogRecord, (String)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.logging.MoPubLog.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */