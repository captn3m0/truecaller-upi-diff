package com.mopub.common;

public enum MoPub$BrowserAgent
{
  static
  {
    Object localObject = new com/mopub/common/MoPub$BrowserAgent;
    ((BrowserAgent)localObject).<init>("IN_APP", 0);
    IN_APP = (BrowserAgent)localObject;
    localObject = new com/mopub/common/MoPub$BrowserAgent;
    int i = 1;
    ((BrowserAgent)localObject).<init>("NATIVE", i);
    NATIVE = (BrowserAgent)localObject;
    localObject = new BrowserAgent[2];
    BrowserAgent localBrowserAgent = IN_APP;
    localObject[0] = localBrowserAgent;
    localBrowserAgent = NATIVE;
    localObject[i] = localBrowserAgent;
    a = (BrowserAgent[])localObject;
  }
  
  public static BrowserAgent fromHeader(Integer paramInteger)
  {
    if (paramInteger == null) {
      return IN_APP;
    }
    int i = paramInteger.intValue();
    int j = 1;
    if (i == j) {
      return NATIVE;
    }
    return IN_APP;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.MoPub.BrowserAgent
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */