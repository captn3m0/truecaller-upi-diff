package com.mopub.common;

import android.os.Handler;
import android.os.Looper;

final class d
  implements SdkInitializationListener
{
  private SdkInitializationListener a;
  private int b;
  
  public d(SdkInitializationListener paramSdkInitializationListener, int paramInt)
  {
    Preconditions.checkNotNull(paramSdkInitializationListener);
    a = paramSdkInitializationListener;
    b = paramInt;
  }
  
  public final void onInitializationFinished()
  {
    int i = b + -1;
    b = i;
    i = b;
    if (i <= 0)
    {
      Handler localHandler = new android/os/Handler;
      Object localObject = Looper.getMainLooper();
      localHandler.<init>((Looper)localObject);
      localObject = new com/mopub/common/d$1;
      ((d.1)localObject).<init>(this);
      localHandler.post((Runnable)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */