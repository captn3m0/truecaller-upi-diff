package com.mopub.common;

import android.content.Context;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.AsyncTasks;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class AdvancedBiddingTokens
  implements a
{
  private List a;
  private final SdkInitializationListener b;
  
  public AdvancedBiddingTokens(SdkInitializationListener paramSdkInitializationListener)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    a = localArrayList;
    b = paramSdkInitializationListener;
  }
  
  final JSONObject a(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext);
    Object localObject1 = a;
    boolean bool1 = ((List)localObject1).isEmpty();
    if (bool1) {
      return null;
    }
    localObject1 = new org/json/JSONObject;
    ((JSONObject)localObject1).<init>();
    Iterator localIterator = a.iterator();
    for (;;)
    {
      boolean bool2 = localIterator.hasNext();
      if (!bool2) {
        break;
      }
      Object localObject2 = (MoPubAdvancedBidder)localIterator.next();
      try
      {
        localObject3 = new org/json/JSONObject;
        ((JSONObject)localObject3).<init>();
        str1 = "token";
        String str2 = ((MoPubAdvancedBidder)localObject2).getToken(paramContext);
        ((JSONObject)localObject3).put(str1, str2);
        str1 = ((MoPubAdvancedBidder)localObject2).getCreativeNetworkName();
        ((JSONObject)localObject1).put(str1, localObject3);
      }
      catch (JSONException localJSONException)
      {
        Object localObject3 = new java/lang/StringBuilder;
        String str1 = "JSON parsing failed for creative network name: ";
        ((StringBuilder)localObject3).<init>(str1);
        localObject2 = ((MoPubAdvancedBidder)localObject2).getCreativeNetworkName();
        ((StringBuilder)localObject3).append((String)localObject2);
        localObject2 = ((StringBuilder)localObject3).toString();
        MoPubLog.d((String)localObject2);
      }
    }
    return (JSONObject)localObject1;
  }
  
  public void addAdvancedBidders(List paramList)
  {
    Preconditions.checkNotNull(paramList);
    AdvancedBiddingTokens.a locala = new com/mopub/common/AdvancedBiddingTokens$a;
    locala.<init>(paramList, this);
    paramList = new Void[0];
    AsyncTasks.safeExecuteOnExecutor(locala, paramList);
  }
  
  public void onAdvancedBiddersInitialized(List paramList)
  {
    Preconditions.checkNotNull(paramList);
    a = paramList;
    paramList = b;
    if (paramList != null) {
      paramList.onInitializationFinished();
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.AdvancedBiddingTokens
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */