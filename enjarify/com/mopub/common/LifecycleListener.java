package com.mopub.common;

import android.app.Activity;

public abstract interface LifecycleListener
{
  public abstract void onBackPressed(Activity paramActivity);
  
  public abstract void onCreate(Activity paramActivity);
  
  public abstract void onDestroy(Activity paramActivity);
  
  public abstract void onPause(Activity paramActivity);
  
  public abstract void onRestart(Activity paramActivity);
  
  public abstract void onResume(Activity paramActivity);
  
  public abstract void onStart(Activity paramActivity);
  
  public abstract void onStop(Activity paramActivity);
}

/* Location:
 * Qualified Name:     com.mopub.common.LifecycleListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */