package com.mopub.common;

public abstract interface DoubleTimeTracker$Clock
{
  public abstract long elapsedRealTime();
}

/* Location:
 * Qualified Name:     com.mopub.common.DoubleTimeTracker.Clock
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */