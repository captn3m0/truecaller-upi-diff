package com.mopub.common;

public enum LocationService$LocationAwareness
{
  static
  {
    Object localObject = new com/mopub/common/LocationService$LocationAwareness;
    ((LocationAwareness)localObject).<init>("NORMAL", 0);
    NORMAL = (LocationAwareness)localObject;
    localObject = new com/mopub/common/LocationService$LocationAwareness;
    int i = 1;
    ((LocationAwareness)localObject).<init>("TRUNCATED", i);
    TRUNCATED = (LocationAwareness)localObject;
    localObject = new com/mopub/common/LocationService$LocationAwareness;
    int j = 2;
    ((LocationAwareness)localObject).<init>("DISABLED", j);
    DISABLED = (LocationAwareness)localObject;
    localObject = new LocationAwareness[3];
    LocationAwareness localLocationAwareness = NORMAL;
    localObject[0] = localLocationAwareness;
    localLocationAwareness = TRUNCATED;
    localObject[i] = localLocationAwareness;
    localLocationAwareness = DISABLED;
    localObject[j] = localLocationAwareness;
    a = (LocationAwareness[])localObject;
  }
  
  public static LocationAwareness fromMoPubLocationAwareness(MoPub.LocationAwareness paramLocationAwareness)
  {
    MoPub.LocationAwareness localLocationAwareness = MoPub.LocationAwareness.DISABLED;
    if (paramLocationAwareness == localLocationAwareness) {
      return DISABLED;
    }
    localLocationAwareness = MoPub.LocationAwareness.TRUNCATED;
    if (paramLocationAwareness == localLocationAwareness) {
      return TRUNCATED;
    }
    return NORMAL;
  }
  
  public final MoPub.LocationAwareness getNewLocationAwareness()
  {
    LocationAwareness localLocationAwareness = TRUNCATED;
    if (this == localLocationAwareness) {
      return MoPub.LocationAwareness.TRUNCATED;
    }
    localLocationAwareness = DISABLED;
    if (this == localLocationAwareness) {
      return MoPub.LocationAwareness.DISABLED;
    }
    return MoPub.LocationAwareness.NORMAL;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.LocationService.LocationAwareness
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */