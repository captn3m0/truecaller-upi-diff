package com.mopub.common;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Reflection;
import com.mopub.common.util.Reflection.MethodBuilder;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

final class b
  implements ExternalViewabilitySession
{
  private static Object a;
  private static Object b;
  private static Boolean c;
  private static boolean d;
  private Object e;
  private Object f;
  
  private void a(ExternalViewabilitySession.VideoEvent paramVideoEvent, String paramString)
  {
    Object localObject1 = new com/mopub/common/util/Reflection$MethodBuilder;
    Object localObject2 = f;
    String str = "getAvidVideoPlaybackListener";
    ((Reflection.MethodBuilder)localObject1).<init>(localObject2, str);
    localObject1 = ((Reflection.MethodBuilder)localObject1).execute();
    localObject2 = new com/mopub/common/util/Reflection$MethodBuilder;
    paramVideoEvent = paramVideoEvent.getAvidMethodName();
    ((Reflection.MethodBuilder)localObject2).<init>(localObject1, paramVideoEvent);
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      paramVideoEvent = String.class;
      ((Reflection.MethodBuilder)localObject2).addParam(paramVideoEvent, paramString);
    }
    ((Reflection.MethodBuilder)localObject2).execute();
  }
  
  static boolean a()
  {
    boolean bool1 = d;
    if (!bool1)
    {
      Object localObject1 = c;
      if (localObject1 == null)
      {
        bool1 = Reflection.classFound("com.integralads.avid.library.mopub.session.AvidAdSessionManager");
        c = Boolean.valueOf(bool1);
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>("Avid is ");
        Object localObject2 = c;
        boolean bool2 = ((Boolean)localObject2).booleanValue();
        if (bool2) {
          localObject2 = "";
        } else {
          localObject2 = "un";
        }
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject2 = "available via reflection.";
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject1 = ((StringBuilder)localObject1).toString();
        MoPubLog.d((String)localObject1);
      }
      localObject1 = c;
      bool1 = ((Boolean)localObject1).booleanValue();
      if (bool1) {
        return true;
      }
    }
    return false;
  }
  
  static void b()
  {
    d = true;
  }
  
  private static Object c()
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject1 = "com.integralads.avid.library.mopub.session.ExternalAvidAdSessionContext";
      Object localObject2 = Object.class;
      int i = 2;
      try
      {
        Class[] arrayOfClass = new Class[i];
        Object localObject3 = String.class;
        arrayOfClass[0] = localObject3;
        localObject3 = Boolean.TYPE;
        int j = 1;
        arrayOfClass[j] = localObject3;
        localObject4 = new Object[i];
        localObject3 = "5.4.1";
        localObject4[0] = localObject3;
        localObject3 = Boolean.TRUE;
        localObject4[j] = localObject3;
        localObject1 = Reflection.instantiateClassWithConstructor((String)localObject1, (Class)localObject2, arrayOfClass, (Object[])localObject4);
        a = localObject1;
      }
      catch (Exception localException)
      {
        localObject2 = new java/lang/StringBuilder;
        Object localObject4 = "Unable to generate Avid deferred ad session context: ";
        ((StringBuilder)localObject2).<init>((String)localObject4);
        String str = localException.getMessage();
        ((StringBuilder)localObject2).append(str);
        str = ((StringBuilder)localObject2).toString();
        MoPubLog.d(str);
      }
    }
    return a;
  }
  
  private static Object d()
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = "com.integralads.avid.library.mopub.session.ExternalAvidAdSessionContext";
      Object localObject2 = Object.class;
      int i = 1;
      try
      {
        Class[] arrayOfClass = new Class[i];
        Object localObject3 = String.class;
        arrayOfClass[0] = localObject3;
        localObject4 = new Object[i];
        localObject3 = "5.4.1";
        localObject4[0] = localObject3;
        localObject1 = Reflection.instantiateClassWithConstructor((String)localObject1, (Class)localObject2, arrayOfClass, (Object[])localObject4);
        b = localObject1;
      }
      catch (Exception localException)
      {
        localObject2 = new java/lang/StringBuilder;
        Object localObject4 = "Unable to generate Avid ad session context: ";
        ((StringBuilder)localObject2).<init>((String)localObject4);
        String str = localException.getMessage();
        ((StringBuilder)localObject2).append(str);
        str = ((StringBuilder)localObject2).toString();
        MoPubLog.d(str);
      }
    }
    return b;
  }
  
  public final Boolean createDisplaySession(Context paramContext, WebView paramWebView, boolean paramBoolean)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramWebView);
    boolean bool = a();
    Object localObject1 = null;
    if (!bool) {
      return null;
    }
    Object localObject2;
    if (paramBoolean) {
      localObject2 = c();
    } else {
      localObject2 = d();
    }
    bool = paramContext instanceof Activity;
    Object localObject3;
    if (bool)
    {
      localObject3 = paramContext;
      localObject3 = (Activity)paramContext;
    }
    else
    {
      bool = false;
      localObject3 = null;
    }
    try
    {
      Object localObject4 = new com/mopub/common/util/Reflection$MethodBuilder;
      String str = "startAvidDisplayAdSession";
      ((Reflection.MethodBuilder)localObject4).<init>(null, str);
      localObject1 = "com.integralads.avid.library.mopub.session.AvidAdSessionManager";
      localObject1 = ((Reflection.MethodBuilder)localObject4).setStatic((String)localObject1);
      localObject4 = Context.class;
      paramContext = ((Reflection.MethodBuilder)localObject1).addParam((Class)localObject4, paramContext);
      localObject1 = "com.integralads.avid.library.mopub.session.ExternalAvidAdSessionContext";
      paramContext = paramContext.addParam((String)localObject1, localObject2);
      paramContext = paramContext.execute();
      e = paramContext;
      paramContext = new com/mopub/common/util/Reflection$MethodBuilder;
      localObject2 = e;
      localObject1 = "registerAdView";
      paramContext.<init>(localObject2, (String)localObject1);
      localObject2 = View.class;
      paramContext = paramContext.addParam((Class)localObject2, paramWebView);
      paramWebView = Activity.class;
      paramContext = paramContext.addParam(paramWebView, localObject3);
      paramContext.execute();
      return Boolean.TRUE;
    }
    catch (Exception paramContext)
    {
      paramWebView = new java/lang/StringBuilder;
      paramWebView.<init>("Unable to execute Avid start display session: ");
      paramContext = paramContext.getMessage();
      paramWebView.append(paramContext);
      MoPubLog.d(paramWebView.toString());
    }
    return Boolean.FALSE;
  }
  
  public final Boolean createVideoSession(Activity paramActivity, View paramView, Set paramSet, Map paramMap)
  {
    Preconditions.checkNotNull(paramActivity);
    Preconditions.checkNotNull(paramView);
    Preconditions.checkNotNull(paramSet);
    Preconditions.checkNotNull(paramMap);
    boolean bool1 = a();
    Object localObject1 = null;
    if (!bool1) {
      return null;
    }
    try
    {
      Object localObject2 = new com/mopub/common/util/Reflection$MethodBuilder;
      Object localObject3 = "startAvidManagedVideoAdSession";
      ((Reflection.MethodBuilder)localObject2).<init>(null, (String)localObject3);
      localObject1 = "com.integralads.avid.library.mopub.session.AvidAdSessionManager";
      localObject2 = ((Reflection.MethodBuilder)localObject2).setStatic((String)localObject1);
      localObject1 = Context.class;
      localObject2 = ((Reflection.MethodBuilder)localObject2).addParam((Class)localObject1, paramActivity);
      localObject1 = "com.integralads.avid.library.mopub.session.ExternalAvidAdSessionContext";
      localObject3 = d();
      localObject2 = ((Reflection.MethodBuilder)localObject2).addParam((String)localObject1, localObject3);
      localObject2 = ((Reflection.MethodBuilder)localObject2).execute();
      f = localObject2;
      localObject2 = new com/mopub/common/util/Reflection$MethodBuilder;
      localObject1 = f;
      localObject3 = "registerAdView";
      ((Reflection.MethodBuilder)localObject2).<init>(localObject1, (String)localObject3);
      localObject1 = View.class;
      paramView = ((Reflection.MethodBuilder)localObject2).addParam((Class)localObject1, paramView);
      localObject2 = Activity.class;
      paramActivity = paramView.addParam((Class)localObject2, paramActivity);
      paramActivity.execute();
      paramActivity = "avid";
      paramActivity = paramMap.get(paramActivity);
      paramActivity = (CharSequence)paramActivity;
      boolean bool2 = TextUtils.isEmpty(paramActivity);
      if (!bool2)
      {
        paramActivity = new com/mopub/common/util/Reflection$MethodBuilder;
        paramView = f;
        localObject2 = "injectJavaScriptResource";
        paramActivity.<init>(paramView, (String)localObject2);
        paramView = String.class;
        localObject2 = "avid";
        paramMap = paramMap.get(localObject2);
        paramActivity = paramActivity.addParam(paramView, paramMap);
        paramActivity.execute();
      }
      paramActivity = paramSet.iterator();
      for (;;)
      {
        boolean bool3 = paramActivity.hasNext();
        if (!bool3) {
          break;
        }
        paramView = paramActivity.next();
        paramView = (String)paramView;
        boolean bool4 = TextUtils.isEmpty(paramView);
        if (!bool4)
        {
          paramSet = new com/mopub/common/util/Reflection$MethodBuilder;
          paramMap = f;
          localObject2 = "injectJavaScriptResource";
          paramSet.<init>(paramMap, (String)localObject2);
          paramMap = String.class;
          paramView = paramSet.addParam(paramMap, paramView);
          paramView.execute();
        }
      }
      return Boolean.TRUE;
    }
    catch (Exception paramActivity)
    {
      paramView = new java/lang/StringBuilder;
      paramView.<init>("Unable to execute Avid start video session: ");
      paramActivity = paramActivity.getMessage();
      paramView.append(paramActivity);
      MoPubLog.d(paramView.toString());
    }
    return Boolean.FALSE;
  }
  
  public final Boolean endDisplaySession()
  {
    boolean bool = a();
    if (!bool) {
      return null;
    }
    Object localObject1 = e;
    if (localObject1 == null)
    {
      MoPubLog.d("Avid DisplayAdSession unexpectedly null.");
      return Boolean.FALSE;
    }
    try
    {
      localObject2 = new com/mopub/common/util/Reflection$MethodBuilder;
      String str2 = "endSession";
      ((Reflection.MethodBuilder)localObject2).<init>(localObject1, str2);
      ((Reflection.MethodBuilder)localObject2).execute();
      return Boolean.TRUE;
    }
    catch (Exception localException)
    {
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Unable to execute Avid end session: ");
      String str1 = localException.getMessage();
      ((StringBuilder)localObject2).append(str1);
      MoPubLog.d(((StringBuilder)localObject2).toString());
    }
    return Boolean.FALSE;
  }
  
  public final Boolean endVideoSession()
  {
    boolean bool = a();
    if (!bool) {
      return null;
    }
    Object localObject1 = f;
    if (localObject1 == null)
    {
      MoPubLog.d("Avid VideoAdSession unexpectedly null.");
      return Boolean.FALSE;
    }
    try
    {
      localObject2 = new com/mopub/common/util/Reflection$MethodBuilder;
      String str2 = "endSession";
      ((Reflection.MethodBuilder)localObject2).<init>(localObject1, str2);
      ((Reflection.MethodBuilder)localObject2).execute();
      return Boolean.TRUE;
    }
    catch (Exception localException)
    {
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Unable to execute Avid end video session: ");
      String str1 = localException.getMessage();
      ((StringBuilder)localObject2).append(str1);
      MoPubLog.d(((StringBuilder)localObject2).toString());
    }
    return Boolean.FALSE;
  }
  
  public final String getName()
  {
    return "AVID";
  }
  
  public final Boolean initialize(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext);
    boolean bool = a();
    if (!bool) {
      return null;
    }
    return Boolean.TRUE;
  }
  
  public final Boolean invalidate()
  {
    boolean bool = a();
    if (!bool) {
      return null;
    }
    e = null;
    f = null;
    return Boolean.TRUE;
  }
  
  public final Boolean onVideoPrepared(View paramView, int paramInt)
  {
    Preconditions.checkNotNull(paramView);
    boolean bool = a();
    if (!bool) {
      return null;
    }
    return Boolean.TRUE;
  }
  
  public final Boolean recordVideoEvent(ExternalViewabilitySession.VideoEvent paramVideoEvent, int paramInt)
  {
    Preconditions.checkNotNull(paramVideoEvent);
    paramInt = a();
    Object localObject1 = null;
    if (paramInt == 0) {
      return null;
    }
    Object localObject2 = f;
    if (localObject2 == null)
    {
      MoPubLog.d("Avid VideoAdSession unexpectedly null.");
      return Boolean.FALSE;
    }
    try
    {
      localObject2 = b.1.a;
      int i = paramVideoEvent.ordinal();
      paramInt = localObject2[i];
      switch (paramInt)
      {
      default: 
        localObject2 = "Unexpected video event type: ";
        break;
      case 13: 
        localObject2 = "error";
        a(paramVideoEvent, (String)localObject2);
        return Boolean.TRUE;
      case 1: 
      case 2: 
      case 3: 
      case 4: 
      case 5: 
      case 6: 
      case 7: 
      case 8: 
      case 9: 
      case 10: 
      case 11: 
      case 12: 
        a(paramVideoEvent, null);
        return Boolean.TRUE;
      }
      localObject1 = String.valueOf(paramVideoEvent);
      localObject2 = ((String)localObject2).concat((String)localObject1);
      MoPubLog.d((String)localObject2);
      return Boolean.FALSE;
    }
    catch (Exception localException)
    {
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("Unable to execute Avid video event for ");
      paramVideoEvent = paramVideoEvent.getAvidMethodName();
      ((StringBuilder)localObject1).append(paramVideoEvent);
      ((StringBuilder)localObject1).append(": ");
      paramVideoEvent = localException.getMessage();
      ((StringBuilder)localObject1).append(paramVideoEvent);
      MoPubLog.d(((StringBuilder)localObject1).toString());
    }
    return Boolean.FALSE;
  }
  
  public final Boolean registerVideoObstruction(View paramView)
  {
    Preconditions.checkNotNull(paramView);
    boolean bool = a();
    if (!bool) {
      return null;
    }
    Object localObject = f;
    if (localObject == null)
    {
      MoPubLog.d("Avid VideoAdSession unexpectedly null.");
      return Boolean.FALSE;
    }
    try
    {
      Reflection.MethodBuilder localMethodBuilder = new com/mopub/common/util/Reflection$MethodBuilder;
      String str = "registerFriendlyObstruction";
      localMethodBuilder.<init>(localObject, str);
      localObject = View.class;
      paramView = localMethodBuilder.addParam((Class)localObject, paramView);
      paramView.execute();
      return Boolean.TRUE;
    }
    catch (Exception paramView)
    {
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>("Unable to register Avid video obstructions: ");
      paramView = paramView.getMessage();
      ((StringBuilder)localObject).append(paramView);
      MoPubLog.d(((StringBuilder)localObject).toString());
    }
    return Boolean.FALSE;
  }
  
  public final Boolean startDeferredDisplaySession(Activity paramActivity)
  {
    boolean bool = a();
    Object localObject1 = null;
    if (!bool) {
      return null;
    }
    Object localObject2 = e;
    if (localObject2 == null)
    {
      MoPubLog.d("Avid DisplayAdSession unexpectedly null.");
      return Boolean.FALSE;
    }
    try
    {
      localObject2 = new com/mopub/common/util/Reflection$MethodBuilder;
      String str = "getInstance";
      ((Reflection.MethodBuilder)localObject2).<init>(null, str);
      localObject1 = "com.integralads.avid.library.mopub.AvidManager";
      localObject2 = ((Reflection.MethodBuilder)localObject2).setStatic((String)localObject1);
      localObject2 = ((Reflection.MethodBuilder)localObject2).execute();
      localObject1 = new com/mopub/common/util/Reflection$MethodBuilder;
      str = "registerActivity";
      ((Reflection.MethodBuilder)localObject1).<init>(localObject2, str);
      localObject2 = Activity.class;
      paramActivity = ((Reflection.MethodBuilder)localObject1).addParam((Class)localObject2, paramActivity);
      paramActivity.execute();
      paramActivity = new com/mopub/common/util/Reflection$MethodBuilder;
      localObject2 = e;
      localObject1 = "getAvidDeferredAdSessionListener";
      paramActivity.<init>(localObject2, (String)localObject1);
      paramActivity = paramActivity.execute();
      if (paramActivity == null)
      {
        paramActivity = "Avid AdSessionListener unexpectedly null.";
        MoPubLog.d(paramActivity);
        return Boolean.FALSE;
      }
      localObject2 = new com/mopub/common/util/Reflection$MethodBuilder;
      localObject1 = "recordReadyEvent";
      ((Reflection.MethodBuilder)localObject2).<init>(paramActivity, (String)localObject1);
      ((Reflection.MethodBuilder)localObject2).execute();
      return Boolean.TRUE;
    }
    catch (Exception paramActivity)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Unable to execute Avid record deferred session: ");
      paramActivity = paramActivity.getMessage();
      ((StringBuilder)localObject2).append(paramActivity);
      MoPubLog.d(((StringBuilder)localObject2).toString());
    }
    return Boolean.FALSE;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */