package com.mopub.common;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.webkit.WebView;
import com.mopub.common.logging.MoPubLog;
import com.mopub.mobileads.VastVideoConfig;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class ExternalViewabilitySessionManager
{
  private final Set a;
  
  public ExternalViewabilitySessionManager(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext);
    Object localObject1 = new java/util/HashSet;
    ((HashSet)localObject1).<init>();
    a = ((Set)localObject1);
    localObject1 = a;
    Object localObject2 = new com/mopub/common/b;
    ((b)localObject2).<init>();
    ((Set)localObject1).add(localObject2);
    localObject1 = a;
    localObject2 = new com/mopub/common/f;
    ((f)localObject2).<init>();
    ((Set)localObject1).add(localObject2);
    a(paramContext);
  }
  
  private void a(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext);
    Iterator localIterator = a.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      ExternalViewabilitySession localExternalViewabilitySession = (ExternalViewabilitySession)localIterator.next();
      Boolean localBoolean = localExternalViewabilitySession.initialize(paramContext);
      String str = "initialize";
      a(localExternalViewabilitySession, str, localBoolean, false);
    }
  }
  
  private static void a(ExternalViewabilitySession paramExternalViewabilitySession, String paramString, Boolean paramBoolean, boolean paramBoolean1)
  {
    Preconditions.checkNotNull(paramExternalViewabilitySession);
    Preconditions.checkNotNull(paramString);
    if (paramBoolean == null) {
      return;
    }
    boolean bool = paramBoolean.booleanValue();
    if (bool) {
      paramBoolean = "";
    } else {
      paramBoolean = "failed to ";
    }
    Locale localLocale = Locale.US;
    String str = "%s viewability event: %s%s.";
    int i = 3;
    Object[] arrayOfObject = new Object[i];
    paramExternalViewabilitySession = paramExternalViewabilitySession.getName();
    arrayOfObject[0] = paramExternalViewabilitySession;
    arrayOfObject[1] = paramBoolean;
    int j = 2;
    arrayOfObject[j] = paramString;
    paramExternalViewabilitySession = String.format(localLocale, str, arrayOfObject);
    if (paramBoolean1)
    {
      MoPubLog.v(paramExternalViewabilitySession);
      return;
    }
    MoPubLog.d(paramExternalViewabilitySession);
  }
  
  public void createDisplaySession(Context paramContext, WebView paramWebView)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramWebView);
    createDisplaySession(paramContext, paramWebView, false);
  }
  
  public void createDisplaySession(Context paramContext, WebView paramWebView, boolean paramBoolean)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramWebView);
    Iterator localIterator = a.iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      ExternalViewabilitySession localExternalViewabilitySession = (ExternalViewabilitySession)localIterator.next();
      Boolean localBoolean = localExternalViewabilitySession.createDisplaySession(paramContext, paramWebView, paramBoolean);
      String str = "start display session";
      boolean bool2 = true;
      a(localExternalViewabilitySession, str, localBoolean, bool2);
    }
  }
  
  public void createVideoSession(Activity paramActivity, View paramView, VastVideoConfig paramVastVideoConfig)
  {
    Preconditions.checkNotNull(paramActivity);
    Preconditions.checkNotNull(paramView);
    Preconditions.checkNotNull(paramVastVideoConfig);
    Iterator localIterator = a.iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      ExternalViewabilitySession localExternalViewabilitySession = (ExternalViewabilitySession)localIterator.next();
      Object localObject1 = new java/util/HashSet;
      ((HashSet)localObject1).<init>();
      boolean bool2 = localExternalViewabilitySession instanceof b;
      if (bool2)
      {
        localObject2 = paramVastVideoConfig.getAvidJavascriptResources();
        ((Set)localObject1).addAll((Collection)localObject2);
      }
      else
      {
        bool2 = localExternalViewabilitySession instanceof f;
        if (bool2)
        {
          localObject2 = paramVastVideoConfig.getMoatImpressionPixels();
          ((Set)localObject1).addAll((Collection)localObject2);
        }
      }
      Object localObject2 = paramVastVideoConfig.getExternalViewabilityTrackers();
      localObject1 = ((ExternalViewabilitySession)localExternalViewabilitySession).createVideoSession(paramActivity, paramView, (Set)localObject1, (Map)localObject2);
      localObject2 = "start video session";
      boolean bool3 = true;
      a((ExternalViewabilitySession)localExternalViewabilitySession, (String)localObject2, (Boolean)localObject1, bool3);
    }
  }
  
  public void endDisplaySession()
  {
    Iterator localIterator = a.iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      ExternalViewabilitySession localExternalViewabilitySession = (ExternalViewabilitySession)localIterator.next();
      Boolean localBoolean = localExternalViewabilitySession.endDisplaySession();
      String str = "end display session";
      boolean bool2 = true;
      a(localExternalViewabilitySession, str, localBoolean, bool2);
    }
  }
  
  public void endVideoSession()
  {
    Iterator localIterator = a.iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      ExternalViewabilitySession localExternalViewabilitySession = (ExternalViewabilitySession)localIterator.next();
      Boolean localBoolean = localExternalViewabilitySession.endVideoSession();
      String str = "end video session";
      boolean bool2 = true;
      a(localExternalViewabilitySession, str, localBoolean, bool2);
    }
  }
  
  public void invalidate()
  {
    Iterator localIterator = a.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      ExternalViewabilitySession localExternalViewabilitySession = (ExternalViewabilitySession)localIterator.next();
      Boolean localBoolean = localExternalViewabilitySession.invalidate();
      String str = "invalidate";
      a(localExternalViewabilitySession, str, localBoolean, false);
    }
  }
  
  public void onVideoPrepared(View paramView, int paramInt)
  {
    Preconditions.checkNotNull(paramView);
    Iterator localIterator = a.iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      ExternalViewabilitySession localExternalViewabilitySession = (ExternalViewabilitySession)localIterator.next();
      Boolean localBoolean = localExternalViewabilitySession.onVideoPrepared(paramView, paramInt);
      String str = "on video prepared";
      boolean bool2 = true;
      a(localExternalViewabilitySession, str, localBoolean, bool2);
    }
  }
  
  public void recordVideoEvent(ExternalViewabilitySession.VideoEvent paramVideoEvent, int paramInt)
  {
    Preconditions.checkNotNull(paramVideoEvent);
    Iterator localIterator = a.iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      ExternalViewabilitySession localExternalViewabilitySession = (ExternalViewabilitySession)localIterator.next();
      Boolean localBoolean = localExternalViewabilitySession.recordVideoEvent(paramVideoEvent, paramInt);
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>("record video event (");
      String str = paramVideoEvent.name();
      ((StringBuilder)localObject).append(str);
      str = ")";
      ((StringBuilder)localObject).append(str);
      localObject = ((StringBuilder)localObject).toString();
      boolean bool2 = true;
      a(localExternalViewabilitySession, (String)localObject, localBoolean, bool2);
    }
  }
  
  public void registerVideoObstruction(View paramView)
  {
    Preconditions.checkNotNull(paramView);
    Iterator localIterator = a.iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      ExternalViewabilitySession localExternalViewabilitySession = (ExternalViewabilitySession)localIterator.next();
      Boolean localBoolean = localExternalViewabilitySession.registerVideoObstruction(paramView);
      String str = "register friendly obstruction";
      boolean bool2 = true;
      a(localExternalViewabilitySession, str, localBoolean, bool2);
    }
  }
  
  public void startDeferredDisplaySession(Activity paramActivity)
  {
    Iterator localIterator = a.iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      ExternalViewabilitySession localExternalViewabilitySession = (ExternalViewabilitySession)localIterator.next();
      Boolean localBoolean = localExternalViewabilitySession.startDeferredDisplaySession(paramActivity);
      String str = "record deferred session";
      boolean bool2 = true;
      a(localExternalViewabilitySession, str, localBoolean, bool2);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.ExternalViewabilitySessionManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */