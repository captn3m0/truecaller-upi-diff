package com.mopub.common;

import com.mopub.common.logging.MoPubLog;

public class DoubleTimeTracker
{
  private volatile DoubleTimeTracker.a a;
  private long b;
  private long c;
  private final DoubleTimeTracker.Clock d;
  
  public DoubleTimeTracker()
  {
    this(localb);
  }
  
  public DoubleTimeTracker(DoubleTimeTracker.Clock paramClock)
  {
    d = paramClock;
    paramClock = DoubleTimeTracker.a.PAUSED;
    a = paramClock;
  }
  
  private long a()
  {
    try
    {
      Object localObject1 = a;
      DoubleTimeTracker.a locala = DoubleTimeTracker.a.PAUSED;
      if (localObject1 == locala) {
        return 0L;
      }
      localObject1 = d;
      long l1 = ((DoubleTimeTracker.Clock)localObject1).elapsedRealTime();
      long l2 = b;
      l1 -= l2;
      return l1;
    }
    finally {}
  }
  
  public double getInterval()
  {
    try
    {
      long l1 = c;
      long l2 = a();
      double d1 = l1 + l2;
      return d1;
    }
    finally {}
  }
  
  public void pause()
  {
    try
    {
      Object localObject1 = a;
      DoubleTimeTracker.a locala = DoubleTimeTracker.a.PAUSED;
      if (localObject1 == locala)
      {
        localObject1 = "DoubleTimeTracker already paused.";
        MoPubLog.v((String)localObject1);
        return;
      }
      long l1 = c;
      long l2 = a();
      l1 += l2;
      c = l1;
      l1 = 0L;
      b = l1;
      localObject1 = DoubleTimeTracker.a.PAUSED;
      a = ((DoubleTimeTracker.a)localObject1);
      return;
    }
    finally {}
  }
  
  public void start()
  {
    try
    {
      Object localObject1 = a;
      DoubleTimeTracker.a locala = DoubleTimeTracker.a.STARTED;
      if (localObject1 == locala)
      {
        localObject1 = "DoubleTimeTracker already started.";
        MoPubLog.v((String)localObject1);
        return;
      }
      localObject1 = DoubleTimeTracker.a.STARTED;
      a = ((DoubleTimeTracker.a)localObject1);
      localObject1 = d;
      long l = ((DoubleTimeTracker.Clock)localObject1).elapsedRealTime();
      b = l;
      return;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.DoubleTimeTracker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */