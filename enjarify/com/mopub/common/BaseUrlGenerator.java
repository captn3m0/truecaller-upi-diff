package com.mopub.common;

import android.graphics.Point;
import android.net.Uri;
import android.text.TextUtils;
import com.mopub.network.Networking;

public abstract class BaseUrlGenerator
{
  private boolean a;
  protected StringBuilder f;
  
  private String a()
  {
    boolean bool = a;
    if (bool)
    {
      a = false;
      return "?";
    }
    return "&";
  }
  
  protected final void a(Point paramPoint)
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    int i = x;
    ((StringBuilder)localObject).append(i);
    localObject = ((StringBuilder)localObject).toString();
    b("w", (String)localObject);
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    int j = y;
    ((StringBuilder)localObject).append(j);
    paramPoint = ((StringBuilder)localObject).toString();
    b("h", paramPoint);
  }
  
  protected final void a(String paramString, Boolean paramBoolean)
  {
    if (paramBoolean == null) {
      return;
    }
    Object localObject = f;
    String str = a();
    ((StringBuilder)localObject).append(str);
    f.append(paramString);
    paramString = f;
    localObject = "=";
    paramString.append((String)localObject);
    paramString = f;
    boolean bool = paramBoolean.booleanValue();
    if (bool) {
      paramBoolean = "1";
    } else {
      paramBoolean = "0";
    }
    paramString.append(paramBoolean);
  }
  
  protected final void a(String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    String str = Networking.getScheme();
    localStringBuilder.<init>(str);
    localStringBuilder.append("://");
    localStringBuilder.append(paramString1);
    localStringBuilder.append(paramString2);
    f = localStringBuilder;
    a = true;
  }
  
  protected final void a(boolean paramBoolean)
  {
    String str1 = "android_perms_ext_storage";
    String str2;
    if (paramBoolean) {
      str2 = "1";
    } else {
      str2 = "0";
    }
    b(str1, str2);
  }
  
  protected final void a(String... paramVarArgs)
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    int i = 0;
    int j;
    for (;;)
    {
      j = 2;
      if (i >= j) {
        break;
      }
      String str = paramVarArgs[i];
      ((StringBuilder)localObject).append(str);
      str = ",";
      ((StringBuilder)localObject).append(str);
      i += 1;
    }
    paramVarArgs = paramVarArgs[j];
    ((StringBuilder)localObject).append(paramVarArgs);
    localObject = ((StringBuilder)localObject).toString();
    b("dn", (String)localObject);
  }
  
  protected final void b()
  {
    b("udid", "mp_tmpl_advertising_id");
    b("dnt", "mp_tmpl_do_not_track");
  }
  
  protected final void b(String paramString)
  {
    b("v", paramString);
  }
  
  protected final void b(String paramString1, String paramString2)
  {
    boolean bool = TextUtils.isEmpty(paramString2);
    if (bool) {
      return;
    }
    StringBuilder localStringBuilder = f;
    String str = a();
    localStringBuilder.append(str);
    f.append(paramString1);
    f.append("=");
    paramString1 = f;
    paramString2 = Uri.encode(paramString2);
    paramString1.append(paramString2);
  }
  
  protected final void c(String paramString)
  {
    b("av", paramString);
  }
  
  public abstract String generateUrlString(String paramString);
}

/* Location:
 * Qualified Name:     com.mopub.common.BaseUrlGenerator
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */