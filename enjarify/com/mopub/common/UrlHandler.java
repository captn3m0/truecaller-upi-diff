package com.mopub.common;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.mopub.common.logging.MoPubLog;
import com.mopub.exceptions.IntentNotResolvableException;
import com.mopub.network.TrackingRequest;
import java.util.EnumSet;
import java.util.Iterator;

public class UrlHandler
{
  private static final UrlHandler.ResultActions c;
  private static final UrlHandler.MoPubSchemeListener d;
  UrlHandler.MoPubSchemeListener a;
  boolean b;
  private EnumSet e;
  private UrlHandler.ResultActions f;
  private String g;
  private boolean h;
  private boolean i;
  
  static
  {
    Object localObject = new com/mopub/common/UrlHandler$1;
    ((UrlHandler.1)localObject).<init>();
    c = (UrlHandler.ResultActions)localObject;
    localObject = new com/mopub/common/UrlHandler$2;
    ((UrlHandler.2)localObject).<init>();
    d = (UrlHandler.MoPubSchemeListener)localObject;
  }
  
  private UrlHandler(EnumSet paramEnumSet, UrlHandler.ResultActions paramResultActions, UrlHandler.MoPubSchemeListener paramMoPubSchemeListener, boolean paramBoolean, String paramString)
  {
    paramEnumSet = EnumSet.copyOf(paramEnumSet);
    e = paramEnumSet;
    f = paramResultActions;
    a = paramMoPubSchemeListener;
    b = paramBoolean;
    g = paramString;
    h = false;
    i = false;
  }
  
  private void a(String paramString1, UrlAction paramUrlAction, String paramString2, Throwable paramThrowable)
  {
    Preconditions.checkNotNull(paramString2);
    if (paramUrlAction == null) {
      paramUrlAction = UrlAction.NOOP;
    }
    MoPubLog.d(paramString2, paramThrowable);
    f.urlHandlingFailed(paramString1, paramUrlAction);
  }
  
  public boolean handleResolvedUrl(Context paramContext, String paramString, boolean paramBoolean, Iterable paramIterable)
  {
    UrlHandler localUrlHandler = this;
    String str1 = paramString;
    boolean bool1 = TextUtils.isEmpty(paramString);
    if (bool1)
    {
      a(paramString, null, "Attempted to handle empty url.", null);
      return false;
    }
    Object localObject1 = UrlAction.NOOP;
    Uri localUri = Uri.parse(paramString);
    Object localObject3 = e;
    Iterator localIterator = ((EnumSet)localObject3).iterator();
    label252:
    Object localObject2;
    for (;;)
    {
      boolean bool2 = localIterator.hasNext();
      if (!bool2) {
        break;
      }
      localObject3 = localIterator.next();
      Object localObject4 = localObject3;
      localObject4 = (UrlAction)localObject3;
      bool2 = ((UrlAction)localObject4).shouldTryHandlingUrl(localUri);
      if (bool2) {
        try
        {
          String str2 = g;
          localObject3 = localObject4;
          localObject5 = this;
          Object localObject6 = localUri;
          ((UrlAction)localObject4).handleUrl(this, paramContext, localUri, paramBoolean, str2);
          bool1 = h;
          bool2 = true;
          if (!bool1)
          {
            bool1 = i;
            if (!bool1)
            {
              localObject1 = UrlAction.IGNORE_ABOUT_SCHEME;
              bool1 = ((UrlAction)localObject1).equals(localObject4);
              if (!bool1)
              {
                localObject1 = UrlAction.HANDLE_MOPUB_SCHEME;
                bool1 = ((UrlAction)localObject1).equals(localObject4);
                if (!bool1)
                {
                  localObject5 = paramContext;
                  try
                  {
                    TrackingRequest.makeTrackingHttpRequest(paramIterable, paramContext);
                    localObject1 = f;
                    localObject6 = localUri.toString();
                    ((UrlHandler.ResultActions)localObject1).urlHandlingSucceeded((String)localObject6, (UrlAction)localObject4);
                    h = bool2;
                  }
                  catch (IntentNotResolvableException localIntentNotResolvableException1)
                  {
                    break label252;
                  }
                }
              }
            }
          }
          return bool2;
        }
        catch (IntentNotResolvableException localIntentNotResolvableException2)
        {
          localObject5 = paramContext;
          localObject3 = localIntentNotResolvableException2.getMessage();
          MoPubLog.d((String)localObject3, localIntentNotResolvableException2);
          localObject2 = localObject4;
        }
      } else {
        localObject5 = paramContext;
      }
    }
    Object localObject5 = String.valueOf(paramString);
    localObject3 = "Link ignored. Unable to handle url: ".concat((String)localObject5);
    a(str1, (UrlAction)localObject2, (String)localObject3, null);
    return false;
  }
  
  public void handleUrl(Context paramContext, String paramString)
  {
    Preconditions.checkNotNull(paramContext);
    handleUrl(paramContext, paramString, true);
  }
  
  public void handleUrl(Context paramContext, String paramString, boolean paramBoolean)
  {
    Preconditions.checkNotNull(paramContext);
    handleUrl(paramContext, paramString, paramBoolean, null);
  }
  
  public void handleUrl(Context paramContext, String paramString, boolean paramBoolean, Iterable paramIterable)
  {
    Preconditions.checkNotNull(paramContext);
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool)
    {
      a(paramString, null, "Attempted to handle empty url.", null);
      return;
    }
    UrlHandler.3 local3 = new com/mopub/common/UrlHandler$3;
    local3.<init>(this, paramContext, paramBoolean, paramIterable, paramString);
    UrlResolutionTask.getResolvedUrl(paramString, local3);
    i = true;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.UrlHandler
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */