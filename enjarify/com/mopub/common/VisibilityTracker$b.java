package com.mopub.common;

import android.view.View;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

final class VisibilityTracker$b
  implements Runnable
{
  private final ArrayList b;
  private final ArrayList c;
  
  VisibilityTracker$b(VisibilityTracker paramVisibilityTracker)
  {
    paramVisibilityTracker = new java/util/ArrayList;
    paramVisibilityTracker.<init>();
    c = paramVisibilityTracker;
    paramVisibilityTracker = new java/util/ArrayList;
    paramVisibilityTracker.<init>();
    b = paramVisibilityTracker;
  }
  
  public final void run()
  {
    VisibilityTracker.a(a);
    Object localObject1 = VisibilityTracker.b(a).entrySet().iterator();
    Object localObject2;
    Object localObject3;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (Map.Entry)((Iterator)localObject1).next();
      localObject3 = (View)((Map.Entry)localObject2).getKey();
      Object localObject4 = (VisibilityTracker.a)((Map.Entry)localObject2).getValue();
      int i = a;
      VisibilityTracker.a locala = (VisibilityTracker.a)((Map.Entry)localObject2).getValue();
      int j = b;
      Integer localInteger = getValuee;
      localObject2 = getValued;
      VisibilityTracker.VisibilityChecker localVisibilityChecker = VisibilityTracker.c(a);
      boolean bool2 = localVisibilityChecker.isVisible((View)localObject2, (View)localObject3, i, localInteger);
      if (bool2)
      {
        localObject2 = b;
        ((ArrayList)localObject2).add(localObject3);
      }
      else
      {
        localObject4 = VisibilityTracker.c(a);
        localInteger = null;
        bool1 = ((VisibilityTracker.VisibilityChecker)localObject4).isVisible((View)localObject2, (View)localObject3, j, null);
        if (!bool1)
        {
          localObject2 = c;
          ((ArrayList)localObject2).add(localObject3);
        }
      }
    }
    localObject1 = VisibilityTracker.d(a);
    if (localObject1 != null)
    {
      localObject1 = VisibilityTracker.d(a);
      localObject2 = b;
      localObject3 = c;
      ((VisibilityTracker.VisibilityTrackerListener)localObject1).onVisibilityChanged((List)localObject2, (List)localObject3);
    }
    b.clear();
    c.clear();
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.VisibilityTracker.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */