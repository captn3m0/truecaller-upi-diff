package com.mopub.common;

import android.os.Build.VERSION;
import com.mopub.common.privacy.AdvertisingId;
import com.mopub.common.privacy.MoPubIdentifier;
import com.mopub.network.AdResponse;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AdReport
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private final AdResponse a;
  private final String b;
  private final String c;
  private final String d;
  private final Locale e;
  private final AdvertisingId f;
  
  public AdReport(String paramString, ClientMetadata paramClientMetadata, AdResponse paramAdResponse)
  {
    b = paramString;
    paramString = paramClientMetadata.getSdkVersion();
    c = paramString;
    paramString = paramClientMetadata.getDeviceModel();
    d = paramString;
    paramString = paramClientMetadata.getDeviceLocale();
    e = paramString;
    paramString = paramClientMetadata.getMoPubIdentifier().getAdvertisingInfo();
    f = paramString;
    a = paramAdResponse;
  }
  
  private static void a(StringBuilder paramStringBuilder, String paramString1, String paramString2)
  {
    paramStringBuilder.append(paramString1);
    paramStringBuilder.append(" : ");
    paramStringBuilder.append(paramString2);
    paramStringBuilder.append("\n");
  }
  
  public String getDspCreativeId()
  {
    return a.getDspCreativeId();
  }
  
  public String getResponseString()
  {
    return a.getStringBody();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder1 = new java/lang/StringBuilder;
    localStringBuilder1.<init>();
    Object localObject1 = c;
    a(localStringBuilder1, "sdk_version", (String)localObject1);
    localObject1 = a.getDspCreativeId();
    a(localStringBuilder1, "creative_id", (String)localObject1);
    int i = Build.VERSION.SDK_INT;
    localObject1 = Integer.toString(i);
    a(localStringBuilder1, "platform_version", (String)localObject1);
    localObject1 = d;
    a(localStringBuilder1, "device_model", (String)localObject1);
    localObject1 = b;
    a(localStringBuilder1, "ad_unit_id", (String)localObject1);
    Object localObject2 = "device_locale";
    localObject1 = e;
    Object localObject3 = null;
    if (localObject1 == null)
    {
      i = 0;
      localObject1 = null;
    }
    else
    {
      localObject1 = ((Locale)localObject1).toString();
    }
    a(localStringBuilder1, (String)localObject2, (String)localObject1);
    localObject1 = f;
    boolean bool2 = MoPub.canCollectPersonalInformation();
    localObject1 = ((AdvertisingId)localObject1).getIdentifier(bool2);
    a(localStringBuilder1, "device_id", (String)localObject1);
    localObject1 = a.getNetworkType();
    a(localStringBuilder1, "network_type", (String)localObject1);
    a(localStringBuilder1, "platform", "android");
    localObject2 = "timestamp";
    localObject1 = a;
    long l1 = ((AdResponse)localObject1).getTimestamp();
    long l2 = -1;
    boolean bool1 = l1 < l2;
    if (bool1)
    {
      localObject1 = new java/text/SimpleDateFormat;
      Locale localLocale = Locale.US;
      ((SimpleDateFormat)localObject1).<init>("M/d/yy hh:mm:ss a z", localLocale);
      localObject3 = new java/util/Date;
      ((Date)localObject3).<init>(l1);
      localObject3 = ((SimpleDateFormat)localObject1).format((Date)localObject3);
    }
    a(localStringBuilder1, (String)localObject2, (String)localObject3);
    localObject1 = a.getAdType();
    a(localStringBuilder1, "ad_type", (String)localObject1);
    localObject2 = a.getWidth();
    localObject1 = a.getHeight();
    localObject3 = "ad_size";
    StringBuilder localStringBuilder2 = new java/lang/StringBuilder;
    String str = "{";
    localStringBuilder2.<init>(str);
    if (localObject2 == null) {
      localObject2 = "0";
    }
    localStringBuilder2.append(localObject2);
    localObject2 = ", ";
    localStringBuilder2.append((String)localObject2);
    if (localObject1 == null) {
      localObject1 = "0";
    }
    localStringBuilder2.append(localObject1);
    localStringBuilder2.append("}");
    localObject2 = localStringBuilder2.toString();
    a(localStringBuilder1, (String)localObject3, (String)localObject2);
    return localStringBuilder1.toString();
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.AdReport
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */