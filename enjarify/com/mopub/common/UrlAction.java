package com.mopub.common;

import android.content.Context;
import android.net.Uri;
import com.mopub.common.logging.MoPubLog;
import com.mopub.exceptions.IntentNotResolvableException;

public enum UrlAction
{
  private final boolean a;
  
  static
  {
    Object localObject = new com/mopub/common/UrlAction$1;
    ((UrlAction.1)localObject).<init>("HANDLE_MOPUB_SCHEME");
    HANDLE_MOPUB_SCHEME = (UrlAction)localObject;
    localObject = new com/mopub/common/UrlAction$3;
    ((UrlAction.3)localObject).<init>("IGNORE_ABOUT_SCHEME");
    IGNORE_ABOUT_SCHEME = (UrlAction)localObject;
    localObject = new com/mopub/common/UrlAction$4;
    ((UrlAction.4)localObject).<init>("HANDLE_PHONE_SCHEME");
    HANDLE_PHONE_SCHEME = (UrlAction)localObject;
    localObject = new com/mopub/common/UrlAction$5;
    ((UrlAction.5)localObject).<init>("OPEN_NATIVE_BROWSER");
    OPEN_NATIVE_BROWSER = (UrlAction)localObject;
    localObject = new com/mopub/common/UrlAction$6;
    ((UrlAction.6)localObject).<init>("OPEN_APP_MARKET");
    OPEN_APP_MARKET = (UrlAction)localObject;
    localObject = new com/mopub/common/UrlAction$7;
    ((UrlAction.7)localObject).<init>("OPEN_IN_APP_BROWSER");
    OPEN_IN_APP_BROWSER = (UrlAction)localObject;
    localObject = new com/mopub/common/UrlAction$8;
    ((UrlAction.8)localObject).<init>("HANDLE_SHARE_TWEET");
    HANDLE_SHARE_TWEET = (UrlAction)localObject;
    localObject = new com/mopub/common/UrlAction$9;
    ((UrlAction.9)localObject).<init>("FOLLOW_DEEP_LINK_WITH_FALLBACK");
    FOLLOW_DEEP_LINK_WITH_FALLBACK = (UrlAction)localObject;
    localObject = new com/mopub/common/UrlAction$10;
    ((UrlAction.10)localObject).<init>("FOLLOW_DEEP_LINK");
    FOLLOW_DEEP_LINK = (UrlAction)localObject;
    localObject = new com/mopub/common/UrlAction$2;
    ((UrlAction.2)localObject).<init>("NOOP");
    NOOP = (UrlAction)localObject;
    localObject = new UrlAction[10];
    UrlAction localUrlAction = HANDLE_MOPUB_SCHEME;
    localObject[0] = localUrlAction;
    localUrlAction = IGNORE_ABOUT_SCHEME;
    localObject[1] = localUrlAction;
    localUrlAction = HANDLE_PHONE_SCHEME;
    localObject[2] = localUrlAction;
    localUrlAction = OPEN_NATIVE_BROWSER;
    localObject[3] = localUrlAction;
    localUrlAction = OPEN_APP_MARKET;
    localObject[4] = localUrlAction;
    localUrlAction = OPEN_IN_APP_BROWSER;
    localObject[5] = localUrlAction;
    localUrlAction = HANDLE_SHARE_TWEET;
    localObject[6] = localUrlAction;
    localUrlAction = FOLLOW_DEEP_LINK_WITH_FALLBACK;
    localObject[7] = localUrlAction;
    localUrlAction = FOLLOW_DEEP_LINK;
    localObject[8] = localUrlAction;
    localUrlAction = NOOP;
    localObject[9] = localUrlAction;
    b = (UrlAction[])localObject;
  }
  
  private UrlAction(boolean paramBoolean)
  {
    a = paramBoolean;
  }
  
  protected abstract void a(Context paramContext, Uri paramUri, UrlHandler paramUrlHandler, String paramString);
  
  public void handleUrl(UrlHandler paramUrlHandler, Context paramContext, Uri paramUri, boolean paramBoolean, String paramString)
  {
    String str1 = String.valueOf(paramUri);
    String str2 = "Ad event URL: ".concat(str1);
    MoPubLog.d(str2);
    boolean bool = a;
    if ((bool) && (!paramBoolean))
    {
      paramUrlHandler = new com/mopub/exceptions/IntentNotResolvableException;
      paramUrlHandler.<init>("Attempted to handle action without user interaction.");
      throw paramUrlHandler;
    }
    a(paramContext, paramUri, paramUrlHandler, paramString);
  }
  
  public abstract boolean shouldTryHandlingUrl(Uri paramUri);
}

/* Location:
 * Qualified Name:     com.mopub.common.UrlAction
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */