package com.mopub.common.factories;

import com.mopub.common.util.Reflection.MethodBuilder;

public class MethodBuilderFactory
{
  protected static MethodBuilderFactory a;
  
  static
  {
    MethodBuilderFactory localMethodBuilderFactory = new com/mopub/common/factories/MethodBuilderFactory;
    localMethodBuilderFactory.<init>();
    a = localMethodBuilderFactory;
  }
  
  public static Reflection.MethodBuilder create(Object paramObject, String paramString)
  {
    Reflection.MethodBuilder localMethodBuilder = new com/mopub/common/util/Reflection$MethodBuilder;
    localMethodBuilder.<init>(paramObject, paramString);
    return localMethodBuilder;
  }
  
  public static void setInstance(MethodBuilderFactory paramMethodBuilderFactory)
  {
    a = paramMethodBuilderFactory;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.factories.MethodBuilderFactory
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */