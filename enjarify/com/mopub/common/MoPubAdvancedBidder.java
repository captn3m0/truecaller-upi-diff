package com.mopub.common;

import android.content.Context;

public abstract interface MoPubAdvancedBidder
{
  public abstract String getCreativeNetworkName();
  
  public abstract String getToken(Context paramContext);
}

/* Location:
 * Qualified Name:     com.mopub.common.MoPubAdvancedBidder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */