package com.mopub.common;

import android.content.Context;
import android.net.Uri;
import com.mopub.common.util.Intents;
import com.mopub.exceptions.IntentNotResolvableException;
import com.mopub.exceptions.UrlParseException;

 enum UrlAction$5
{
  UrlAction$5()
  {
    super(paramString, 3, true, (byte)0);
  }
  
  protected final void a(Context paramContext, Uri paramUri, UrlHandler paramUrlHandler, String paramString)
  {
    paramString = String.valueOf(paramUri);
    paramUrlHandler = "Unable to load mopub native browser url: ".concat(paramString);
    try
    {
      paramUri = Intents.intentForNativeBrowserScheme(paramUri);
      Intents.launchIntentForUserClick(paramContext, paramUri, paramUrlHandler);
      return;
    }
    catch (UrlParseException paramContext)
    {
      paramUri = new com/mopub/exceptions/IntentNotResolvableException;
      paramString = new java/lang/StringBuilder;
      paramString.<init>();
      paramString.append(paramUrlHandler);
      paramString.append("\n\t");
      paramContext = paramContext.getMessage();
      paramString.append(paramContext);
      paramContext = paramString.toString();
      paramUri.<init>(paramContext);
      throw paramUri;
    }
  }
  
  public final boolean shouldTryHandlingUrl(Uri paramUri)
  {
    paramUri = paramUri.getScheme();
    Object localObject = "http";
    boolean bool = ((String)localObject).equalsIgnoreCase(paramUri);
    if (!bool)
    {
      localObject = "https";
      bool = ((String)localObject).equalsIgnoreCase(paramUri);
      if (!bool) {
        return "mopubnativebrowser".equalsIgnoreCase(paramUri);
      }
    }
    paramUri = MoPub.getBrowserAgent();
    localObject = MoPub.BrowserAgent.NATIVE;
    return paramUri == localObject;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.UrlAction.5
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */