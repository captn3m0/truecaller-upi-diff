package com.mopub.common;

public class GpsHelper$AdvertisingInfo
{
  public final String advertisingId;
  public final boolean limitAdTracking;
  
  public GpsHelper$AdvertisingInfo(String paramString, boolean paramBoolean)
  {
    advertisingId = paramString;
    limitAdTracking = paramBoolean;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.GpsHelper.AdvertisingInfo
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */