package com.mopub.common;

import java.lang.annotation.Annotation;

public @interface VisibleForTesting {}

/* Location:
 * Qualified Name:     com.mopub.common.VisibleForTesting
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */