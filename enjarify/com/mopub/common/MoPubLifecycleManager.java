package com.mopub.common;

import android.app.Activity;
import com.mopub.common.privacy.PersonalInfoManager;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class MoPubLifecycleManager
  implements LifecycleListener
{
  private static MoPubLifecycleManager a;
  private final Set b;
  private final WeakReference c;
  
  private MoPubLifecycleManager(Activity paramActivity)
  {
    Object localObject = new java/util/HashSet;
    ((HashSet)localObject).<init>();
    b = ((Set)localObject);
    localObject = new java/lang/ref/WeakReference;
    ((WeakReference)localObject).<init>(paramActivity);
    c = ((WeakReference)localObject);
  }
  
  public static MoPubLifecycleManager getInstance(Activity paramActivity)
  {
    synchronized (MoPubLifecycleManager.class)
    {
      MoPubLifecycleManager localMoPubLifecycleManager = a;
      if (localMoPubLifecycleManager == null)
      {
        localMoPubLifecycleManager = new com/mopub/common/MoPubLifecycleManager;
        localMoPubLifecycleManager.<init>(paramActivity);
        a = localMoPubLifecycleManager;
      }
      paramActivity = a;
      return paramActivity;
    }
  }
  
  public void addLifecycleListener(LifecycleListener paramLifecycleListener)
  {
    if (paramLifecycleListener == null) {
      return;
    }
    Object localObject = b;
    boolean bool = ((Set)localObject).add(paramLifecycleListener);
    if (bool)
    {
      localObject = (Activity)c.get();
      if (localObject != null)
      {
        paramLifecycleListener.onCreate((Activity)localObject);
        paramLifecycleListener.onStart((Activity)localObject);
      }
    }
  }
  
  public void onBackPressed(Activity paramActivity)
  {
    Iterator localIterator = b.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      LifecycleListener localLifecycleListener = (LifecycleListener)localIterator.next();
      localLifecycleListener.onBackPressed(paramActivity);
    }
  }
  
  public void onCreate(Activity paramActivity)
  {
    Iterator localIterator = b.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      LifecycleListener localLifecycleListener = (LifecycleListener)localIterator.next();
      localLifecycleListener.onCreate(paramActivity);
    }
  }
  
  public void onDestroy(Activity paramActivity)
  {
    Iterator localIterator = b.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      LifecycleListener localLifecycleListener = (LifecycleListener)localIterator.next();
      localLifecycleListener.onDestroy(paramActivity);
    }
  }
  
  public void onPause(Activity paramActivity)
  {
    Iterator localIterator = b.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      LifecycleListener localLifecycleListener = (LifecycleListener)localIterator.next();
      localLifecycleListener.onPause(paramActivity);
    }
  }
  
  public void onRestart(Activity paramActivity)
  {
    Iterator localIterator = b.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      LifecycleListener localLifecycleListener = (LifecycleListener)localIterator.next();
      localLifecycleListener.onRestart(paramActivity);
    }
  }
  
  public void onResume(Activity paramActivity)
  {
    Object localObject = MoPub.getPersonalInformationManager();
    boolean bool;
    LifecycleListener localLifecycleListener;
    if (localObject != null)
    {
      bool = false;
      localLifecycleListener = null;
      ((PersonalInfoManager)localObject).requestSync(false);
    }
    localObject = b.iterator();
    for (;;)
    {
      bool = ((Iterator)localObject).hasNext();
      if (!bool) {
        break;
      }
      localLifecycleListener = (LifecycleListener)((Iterator)localObject).next();
      localLifecycleListener.onResume(paramActivity);
    }
  }
  
  public void onStart(Activity paramActivity)
  {
    Iterator localIterator = b.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      LifecycleListener localLifecycleListener = (LifecycleListener)localIterator.next();
      localLifecycleListener.onStart(paramActivity);
    }
  }
  
  public void onStop(Activity paramActivity)
  {
    Iterator localIterator = b.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      LifecycleListener localLifecycleListener = (LifecycleListener)localIterator.next();
      localLifecycleListener.onStop(paramActivity);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.MoPubLifecycleManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */