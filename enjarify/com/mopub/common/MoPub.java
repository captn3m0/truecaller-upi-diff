package com.mopub.common;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.privacy.PersonalInfoManager;
import com.mopub.common.util.Reflection;
import com.mopub.common.util.Reflection.MethodBuilder;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.json.JSONObject;

public class MoPub
{
  public static final String SDK_VERSION = "5.4.1";
  private static volatile MoPub.LocationAwareness a = MoPub.LocationAwareness.NORMAL;
  private static volatile int b = 6;
  private static volatile long c = 60000L;
  private static volatile MoPub.BrowserAgent d = MoPub.BrowserAgent.IN_APP;
  private static volatile boolean e = false;
  private static boolean f = false;
  private static Method g;
  private static boolean h = true;
  private static boolean i = false;
  private static boolean j = false;
  private static AdvancedBiddingTokens k;
  private static PersonalInfoManager l;
  
  static String a(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext);
    boolean bool = isAdvancedBiddingEnabled();
    if (bool)
    {
      AdvancedBiddingTokens localAdvancedBiddingTokens = k;
      if (localAdvancedBiddingTokens != null)
      {
        Preconditions.checkNotNull(paramContext);
        paramContext = localAdvancedBiddingTokens.a(paramContext);
        if (paramContext == null) {
          return null;
        }
        return paramContext.toString();
      }
    }
    return null;
  }
  
  private static void a(Activity paramActivity)
  {
    boolean bool1 = f;
    boolean bool2 = true;
    String str;
    if (!bool1)
    {
      f = bool2;
      localObject = "com.mopub.mobileads.MoPubRewardedVideoManager";
      try
      {
        localObject = Class.forName((String)localObject);
        str = "updateActivity";
        Class[] arrayOfClass = new Class[bool2];
        Class localClass = Activity.class;
        arrayOfClass[0] = localClass;
        localObject = Reflection.getDeclaredMethodWithTraversal((Class)localObject, str, arrayOfClass);
        g = (Method)localObject;
      }
      catch (NoSuchMethodException localNoSuchMethodException) {}catch (ClassNotFoundException localClassNotFoundException) {}
    }
    Object localObject = g;
    if (localObject != null)
    {
      str = null;
      try
      {
        Object[] arrayOfObject = new Object[bool2];
        arrayOfObject[0] = paramActivity;
        ((Method)localObject).invoke(null, arrayOfObject);
        return;
      }
      catch (InvocationTargetException paramActivity)
      {
        localObject = "Error while attempting to access the update activity method - this should not have happened";
        MoPubLog.e((String)localObject, paramActivity);
      }
      catch (IllegalAccessException paramActivity)
      {
        MoPubLog.e("Error while attempting to access the update activity method - this should not have happened", paramActivity);
        return;
      }
    }
  }
  
  private static void b(SdkInitializationListener paramSdkInitializationListener)
  {
    j = false;
    i = true;
    Handler localHandler = new android/os/Handler;
    Object localObject = Looper.getMainLooper();
    localHandler.<init>((Looper)localObject);
    localObject = new com/mopub/common/MoPub$1;
    ((MoPub.1)localObject).<init>(paramSdkInitializationListener);
    localHandler.post((Runnable)localObject);
  }
  
  public static boolean canCollectPersonalInformation()
  {
    PersonalInfoManager localPersonalInfoManager = l;
    if (localPersonalInfoManager != null)
    {
      boolean bool = localPersonalInfoManager.canCollectPersonalInformation();
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public static void disableViewability(ExternalViewabilitySessionManager.ViewabilityVendor paramViewabilityVendor)
  {
    Preconditions.checkNotNull(paramViewabilityVendor);
    paramViewabilityVendor.disable();
  }
  
  public static MoPub.BrowserAgent getBrowserAgent()
  {
    Preconditions.checkNotNull(d);
    return d;
  }
  
  public static MoPub.LocationAwareness getLocationAwareness()
  {
    Preconditions.checkNotNull(a);
    return a;
  }
  
  public static int getLocationPrecision()
  {
    return b;
  }
  
  public static long getMinimumLocationRefreshTimeMillis()
  {
    return c;
  }
  
  public static PersonalInfoManager getPersonalInformationManager()
  {
    return l;
  }
  
  public static void initializeSdk(Context paramContext, SdkConfiguration paramSdkConfiguration, SdkInitializationListener paramSdkInitializationListener)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramSdkConfiguration);
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("Initializing MoPub with ad unit: ");
    Object localObject3 = paramSdkConfiguration.getAdUnitId();
    ((StringBuilder)localObject1).append((String)localObject3);
    localObject1 = ((StringBuilder)localObject1).toString();
    MoPubLog.d((String)localObject1);
    boolean bool = paramContext instanceof Activity;
    if (bool)
    {
      localObject1 = paramContext;
      localObject1 = (Activity)paramContext;
      Preconditions.checkNotNull(localObject1);
      Preconditions.checkNotNull(paramSdkConfiguration);
      try
      {
        localObject3 = new com/mopub/common/util/Reflection$MethodBuilder;
        Object localObject4 = null;
        String str = "initializeRewardedVideo";
        ((Reflection.MethodBuilder)localObject3).<init>(null, str);
        localObject4 = "com.mopub.mobileads.MoPubRewardedVideos";
        localObject4 = Class.forName((String)localObject4);
        localObject3 = ((Reflection.MethodBuilder)localObject3).setStatic((Class)localObject4);
        localObject3 = ((Reflection.MethodBuilder)localObject3).setAccessible();
        localObject4 = Activity.class;
        localObject1 = ((Reflection.MethodBuilder)localObject3).addParam((Class)localObject4, localObject1);
        localObject3 = SdkConfiguration.class;
        localObject1 = ((Reflection.MethodBuilder)localObject1).addParam((Class)localObject3, paramSdkConfiguration);
        ((Reflection.MethodBuilder)localObject1).execute();
      }
      catch (Exception localException)
      {
        localObject3 = "Error while initializing rewarded video";
        MoPubLog.e((String)localObject3, localException);
      }
      catch (NoSuchMethodException localNoSuchMethodException)
      {
        localObject2 = "initializeRewardedVideo was called without the rewarded video module";
        MoPubLog.w((String)localObject2);
      }
      catch (ClassNotFoundException localClassNotFoundException)
      {
        localObject2 = "initializeRewardedVideo was called without the rewarded video module";
        MoPubLog.w((String)localObject2);
      }
    }
    bool = i;
    if (bool)
    {
      MoPubLog.d("MoPub SDK is already initialized");
      b(paramSdkInitializationListener);
      return;
    }
    bool = j;
    if (bool)
    {
      MoPubLog.d("MoPub SDK is currently initializing.");
      return;
    }
    Object localObject2 = Looper.getMainLooper();
    localObject3 = Looper.myLooper();
    if (localObject2 != localObject3)
    {
      MoPubLog.e("MoPub can only be initialized on the main thread.");
      return;
    }
    j = true;
    localObject2 = new com/mopub/common/MoPub$a;
    ((MoPub.a)localObject2).<init>(paramSdkInitializationListener);
    paramSdkInitializationListener = new com/mopub/common/d;
    paramSdkInitializationListener.<init>((SdkInitializationListener)localObject2, 2);
    localObject2 = new com/mopub/common/privacy/PersonalInfoManager;
    localObject3 = paramSdkConfiguration.getAdUnitId();
    ((PersonalInfoManager)localObject2).<init>((Context)paramContext, (String)localObject3, paramSdkInitializationListener);
    l = (PersonalInfoManager)localObject2;
    ClientMetadata.getInstance((Context)paramContext);
    paramContext = new com/mopub/common/AdvancedBiddingTokens;
    paramContext.<init>(paramSdkInitializationListener);
    k = paramContext;
    paramSdkConfiguration = paramSdkConfiguration.getAdvancedBidders();
    paramContext.addAdvancedBidders(paramSdkConfiguration);
  }
  
  public static boolean isAdvancedBiddingEnabled()
  {
    return h;
  }
  
  public static boolean isSdkInitialized()
  {
    return i;
  }
  
  public static void onBackPressed(Activity paramActivity)
  {
    MoPubLifecycleManager.getInstance(paramActivity).onBackPressed(paramActivity);
  }
  
  public static void onCreate(Activity paramActivity)
  {
    MoPubLifecycleManager.getInstance(paramActivity).onCreate(paramActivity);
    a(paramActivity);
  }
  
  public static void onDestroy(Activity paramActivity)
  {
    MoPubLifecycleManager.getInstance(paramActivity).onDestroy(paramActivity);
  }
  
  public static void onPause(Activity paramActivity)
  {
    MoPubLifecycleManager.getInstance(paramActivity).onPause(paramActivity);
  }
  
  public static void onRestart(Activity paramActivity)
  {
    MoPubLifecycleManager.getInstance(paramActivity).onRestart(paramActivity);
    a(paramActivity);
  }
  
  public static void onResume(Activity paramActivity)
  {
    MoPubLifecycleManager.getInstance(paramActivity).onResume(paramActivity);
    a(paramActivity);
  }
  
  public static void onStart(Activity paramActivity)
  {
    MoPubLifecycleManager.getInstance(paramActivity).onStart(paramActivity);
    a(paramActivity);
  }
  
  public static void onStop(Activity paramActivity)
  {
    MoPubLifecycleManager.getInstance(paramActivity).onStop(paramActivity);
  }
  
  public static void resetBrowserAgent()
  {
    d = MoPub.BrowserAgent.IN_APP;
    e = false;
  }
  
  public static void setAdvancedBiddingEnabled(boolean paramBoolean)
  {
    h = paramBoolean;
  }
  
  public static void setBrowserAgent(MoPub.BrowserAgent paramBrowserAgent)
  {
    Preconditions.checkNotNull(paramBrowserAgent);
    d = paramBrowserAgent;
    e = true;
  }
  
  public static void setBrowserAgentFromAdServer(MoPub.BrowserAgent paramBrowserAgent)
  {
    Preconditions.checkNotNull(paramBrowserAgent);
    boolean bool = e;
    if (bool)
    {
      paramBrowserAgent = new java/lang/StringBuilder;
      paramBrowserAgent.<init>("Browser agent already overridden by client with value ");
      MoPub.BrowserAgent localBrowserAgent = d;
      paramBrowserAgent.append(localBrowserAgent);
      MoPubLog.w(paramBrowserAgent.toString());
      return;
    }
    d = paramBrowserAgent;
  }
  
  public static void setLocationAwareness(MoPub.LocationAwareness paramLocationAwareness)
  {
    Preconditions.checkNotNull(paramLocationAwareness);
    a = paramLocationAwareness;
  }
  
  public static void setLocationPrecision(int paramInt)
  {
    b = Math.min(Math.max(0, paramInt), 6);
  }
  
  public static void setMinimumLocationRefreshTimeMillis(long paramLong)
  {
    c = paramLong;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.MoPub
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */