package com.mopub.common;

import android.content.Context;
import android.net.Uri;
import com.mopub.common.util.Intents;

 enum UrlAction$7
{
  UrlAction$7()
  {
    super(paramString, 5, true, (byte)0);
  }
  
  protected final void a(Context paramContext, Uri paramUri, UrlHandler paramUrlHandler, String paramString)
  {
    boolean bool = b;
    if (!bool) {
      Intents.showMoPubBrowserForUrl(paramContext, paramUri, paramString);
    }
  }
  
  public final boolean shouldTryHandlingUrl(Uri paramUri)
  {
    paramUri = paramUri.getScheme();
    String str = "http";
    boolean bool1 = str.equalsIgnoreCase(paramUri);
    if (!bool1)
    {
      str = "https";
      boolean bool2 = str.equalsIgnoreCase(paramUri);
      if (!bool2) {
        return false;
      }
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.UrlAction.7
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */