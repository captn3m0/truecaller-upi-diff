package com.mopub.common;

public abstract interface CloseableLayout$OnCloseListener
{
  public abstract void onClose();
}

/* Location:
 * Qualified Name:     com.mopub.common.CloseableLayout.OnCloseListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */