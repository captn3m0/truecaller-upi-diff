package com.mopub.common;

public final class Preconditions$NoThrow
{
  private static volatile boolean a = false;
  
  public static boolean checkArgument(boolean paramBoolean)
  {
    boolean bool = a;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = "";
    return Preconditions.a(paramBoolean, bool, "Illegal argument", arrayOfObject);
  }
  
  public static boolean checkArgument(boolean paramBoolean, String paramString)
  {
    boolean bool = a;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = "";
    return Preconditions.a(paramBoolean, bool, paramString, arrayOfObject);
  }
  
  public static boolean checkArgument(boolean paramBoolean, String paramString, Object... paramVarArgs)
  {
    boolean bool = a;
    return Preconditions.a(paramBoolean, bool, paramString, paramVarArgs);
  }
  
  public static boolean checkNotNull(Object paramObject)
  {
    boolean bool = a;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = "";
    return Preconditions.a(paramObject, bool, "Object can not be null.", arrayOfObject);
  }
  
  public static boolean checkNotNull(Object paramObject, String paramString)
  {
    boolean bool = a;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = "";
    return Preconditions.a(paramObject, bool, paramString, arrayOfObject);
  }
  
  public static boolean checkNotNull(Object paramObject, String paramString, Object... paramVarArgs)
  {
    boolean bool = a;
    return Preconditions.a(paramObject, bool, paramString, paramVarArgs);
  }
  
  public static boolean checkState(boolean paramBoolean)
  {
    boolean bool = a;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = "";
    return Preconditions.b(paramBoolean, bool, "Illegal state.", arrayOfObject);
  }
  
  public static boolean checkState(boolean paramBoolean, String paramString)
  {
    boolean bool = a;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = "";
    return Preconditions.b(paramBoolean, bool, paramString, arrayOfObject);
  }
  
  public static boolean checkState(boolean paramBoolean, String paramString, Object... paramVarArgs)
  {
    boolean bool = a;
    return Preconditions.b(paramBoolean, bool, paramString, paramVarArgs);
  }
  
  public static boolean checkUiThread()
  {
    boolean bool = a;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = "";
    return Preconditions.a(bool, "This method must be called from the UI thread.", arrayOfObject);
  }
  
  public static boolean checkUiThread(String paramString)
  {
    boolean bool = a;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = "";
    return Preconditions.a(bool, paramString, arrayOfObject);
  }
  
  public static boolean checkUiThread(String paramString, Object... paramVarArgs)
  {
    return Preconditions.a(false, paramString, paramVarArgs);
  }
  
  public static void setStrictMode(boolean paramBoolean)
  {
    a = paramBoolean;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.Preconditions.NoThrow
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */