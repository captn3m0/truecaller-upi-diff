package com.mopub.common;

import android.content.Context;
import android.net.Uri;
import com.mopub.common.util.Intents;
import com.mopub.exceptions.IntentNotResolvableException;
import com.mopub.network.TrackingRequest;

 enum UrlAction$9
{
  UrlAction$9()
  {
    super(paramString, 7, true, (byte)0);
  }
  
  protected final void a(Context paramContext, Uri paramUri, UrlHandler paramUrlHandler, String paramString)
  {
    paramString = "navigate";
    Object localObject = paramUri.getHost();
    boolean bool1 = paramString.equalsIgnoreCase((String)localObject);
    if (bool1)
    {
      paramString = "primaryUrl";
      try
      {
        paramString = paramUri.getQueryParameter(paramString);
        localObject = "primaryTrackingUrl";
        localObject = paramUri.getQueryParameters((String)localObject);
        String str1 = "fallbackUrl";
        str1 = paramUri.getQueryParameter(str1);
        String str2 = "fallbackTrackingUrl";
        paramUri = paramUri.getQueryParameters(str2);
        if (paramString != null)
        {
          paramString = Uri.parse(paramString);
          boolean bool2 = shouldTryHandlingUrl(paramString);
          if (!bool2) {
            try
            {
              Intents.launchApplicationUrl(paramContext, paramString);
              TrackingRequest.makeTrackingHttpRequest((Iterable)localObject, paramContext);
              return;
            }
            catch (IntentNotResolvableException localIntentNotResolvableException)
            {
              if (str1 != null)
              {
                paramString = Uri.parse(str1);
                bool1 = shouldTryHandlingUrl(paramString);
                if (!bool1)
                {
                  paramUrlHandler.handleUrl(paramContext, str1, true, paramUri);
                  return;
                }
                paramContext = new com/mopub/exceptions/IntentNotResolvableException;
                paramContext.<init>("Deeplink+ URL had another Deeplink+ URL as the 'fallbackUrl'.");
                throw paramContext;
              }
              paramContext = new com/mopub/exceptions/IntentNotResolvableException;
              paramContext.<init>("Unable to handle 'primaryUrl' for Deeplink+ and 'fallbackUrl' was missing.");
              throw paramContext;
            }
          }
          paramContext = new com/mopub/exceptions/IntentNotResolvableException;
          paramContext.<init>("Deeplink+ had another Deeplink+ as the 'primaryUrl'.");
          throw paramContext;
        }
        paramContext = new com/mopub/exceptions/IntentNotResolvableException;
        paramContext.<init>("Deeplink+ did not have 'primaryUrl' query param.");
        throw paramContext;
      }
      catch (UnsupportedOperationException localUnsupportedOperationException)
      {
        paramContext = new com/mopub/exceptions/IntentNotResolvableException;
        paramContext.<init>("Deeplink+ URL was not a hierarchical URI.");
        throw paramContext;
      }
    }
    paramContext = new com/mopub/exceptions/IntentNotResolvableException;
    paramContext.<init>("Deeplink+ URL did not have 'navigate' as the host.");
    throw paramContext;
  }
  
  public final boolean shouldTryHandlingUrl(Uri paramUri)
  {
    paramUri = paramUri.getScheme();
    return "deeplink+".equalsIgnoreCase(paramUri);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.UrlAction.9
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */