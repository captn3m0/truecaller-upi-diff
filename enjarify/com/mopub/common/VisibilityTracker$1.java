package com.mopub.common;

import android.view.ViewTreeObserver.OnPreDrawListener;

final class VisibilityTracker$1
  implements ViewTreeObserver.OnPreDrawListener
{
  VisibilityTracker$1(VisibilityTracker paramVisibilityTracker) {}
  
  public final boolean onPreDraw()
  {
    a.scheduleVisibilityCheck();
    return true;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.VisibilityTracker.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */