package com.mopub.common;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

final class DiskLruCache$Editor$a
  extends FilterOutputStream
{
  private DiskLruCache$Editor$a(DiskLruCache.Editor paramEditor, OutputStream paramOutputStream)
  {
    super(paramOutputStream);
  }
  
  public final void close()
  {
    try
    {
      OutputStream localOutputStream = out;
      localOutputStream.close();
      return;
    }
    catch (IOException localIOException)
    {
      DiskLruCache.Editor.c(a);
    }
  }
  
  public final void flush()
  {
    try
    {
      OutputStream localOutputStream = out;
      localOutputStream.flush();
      return;
    }
    catch (IOException localIOException)
    {
      DiskLruCache.Editor.c(a);
    }
  }
  
  public final void write(int paramInt)
  {
    try
    {
      OutputStream localOutputStream = out;
      localOutputStream.write(paramInt);
      return;
    }
    catch (IOException localIOException)
    {
      DiskLruCache.Editor.c(a);
    }
  }
  
  public final void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    try
    {
      OutputStream localOutputStream = out;
      localOutputStream.write(paramArrayOfByte, paramInt1, paramInt2);
      return;
    }
    catch (IOException localIOException)
    {
      DiskLruCache.Editor.c(a);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.DiskLruCache.Editor.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */