package com.mopub.common;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Build.VERSION;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.privacy.MoPubIdentifier;
import com.mopub.common.util.DeviceUtils;
import com.mopub.common.util.Dips;
import java.util.Locale;

public class ClientMetadata
{
  private static volatile ClientMetadata i;
  private String a;
  private String b;
  private String c;
  private String d;
  private String e;
  private String f;
  private String g;
  private final MoPubIdentifier h;
  private final String j;
  private final String k;
  private final String l;
  private final String m;
  private final String n;
  private final String o;
  private final String p;
  private String q;
  private final Context r;
  private final ConnectivityManager s;
  
  public ClientMetadata(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext);
    paramContext = paramContext.getApplicationContext();
    r = paramContext;
    paramContext = (ConnectivityManager)r.getSystemService("connectivity");
    s = paramContext;
    paramContext = Build.MANUFACTURER;
    j = paramContext;
    paramContext = Build.MODEL;
    k = paramContext;
    paramContext = Build.PRODUCT;
    l = paramContext;
    paramContext = Build.VERSION.RELEASE;
    m = paramContext;
    n = "5.4.1";
    paramContext = a(r);
    o = paramContext;
    paramContext = r.getPackageManager();
    Object localObject1 = r.getPackageName();
    p = ((String)localObject1);
    localObject1 = null;
    int i1;
    int i2;
    try
    {
      localObject2 = p;
      i1 = 0;
      localObject2 = paramContext.getApplicationInfo((String)localObject2, 0);
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      i2 = 0;
      localObject2 = null;
    }
    if (localObject2 != null)
    {
      paramContext = (String)paramContext.getApplicationLabel((ApplicationInfo)localObject2);
      q = paramContext;
    }
    paramContext = r;
    Object localObject2 = "phone";
    paramContext = (TelephonyManager)paramContext.getSystemService((String)localObject2);
    if (paramContext != null)
    {
      localObject2 = paramContext.getNetworkOperator();
      a = ((String)localObject2);
      localObject2 = paramContext.getNetworkOperator();
      b = ((String)localObject2);
      i2 = paramContext.getPhoneType();
      i1 = 2;
      int i4 = 5;
      if (i2 == i1)
      {
        i2 = paramContext.getSimState();
        if (i2 == i4)
        {
          localObject2 = paramContext.getSimOperator();
          a = ((String)localObject2);
          localObject2 = paramContext.getSimOperator();
          c = ((String)localObject2);
        }
      }
      boolean bool = MoPub.canCollectPersonalInformation();
      if (bool)
      {
        localObject2 = paramContext.getNetworkCountryIso();
        d = ((String)localObject2);
        localObject2 = paramContext.getSimCountryIso();
        e = ((String)localObject2);
      }
      else
      {
        d = "";
        localObject2 = "";
        e = ((String)localObject2);
      }
      try
      {
        localObject2 = paramContext.getNetworkOperatorName();
        f = ((String)localObject2);
        int i3 = paramContext.getSimState();
        if (i3 == i4)
        {
          paramContext = paramContext.getSimOperatorName();
          g = paramContext;
        }
      }
      catch (SecurityException localSecurityException)
      {
        f = null;
        g = null;
      }
    }
    paramContext = new com/mopub/common/privacy/MoPubIdentifier;
    localObject1 = r;
    paramContext.<init>((Context)localObject1);
    h = paramContext;
  }
  
  private static String a(Context paramContext)
  {
    try
    {
      String str = paramContext.getPackageName();
      paramContext = paramContext.getPackageManager();
      paramContext = paramContext.getPackageInfo(str, 0);
      return versionName;
    }
    catch (Exception localException)
    {
      MoPubLog.d("Failed to retrieve PackageInfo#versionName.");
    }
    return null;
  }
  
  public static void clearForTesting()
  {
    i = null;
  }
  
  public static String getCurrentLanguage(Context paramContext)
  {
    String str1 = Locale.getDefault().getLanguage().trim();
    paramContext = getResourcesgetConfigurationlocale;
    if (paramContext != null)
    {
      String str2 = paramContext.getLanguage().trim();
      boolean bool = str2.isEmpty();
      if (!bool)
      {
        paramContext = paramContext.getLanguage();
        str1 = paramContext.trim();
      }
    }
    return str1;
  }
  
  public static ClientMetadata getInstance()
  {
    ClientMetadata localClientMetadata1 = i;
    if (localClientMetadata1 == null) {
      synchronized (ClientMetadata.class)
      {
        localClientMetadata1 = i;
      }
    }
    return localClientMetadata2;
  }
  
  public static ClientMetadata getInstance(Context paramContext)
  {
    ClientMetadata localClientMetadata = i;
    if (localClientMetadata == null) {
      synchronized (ClientMetadata.class)
      {
        localClientMetadata = i;
        if (localClientMetadata == null)
        {
          localClientMetadata = new com/mopub/common/ClientMetadata;
          localClientMetadata.<init>(paramContext);
          i = localClientMetadata;
        }
      }
    }
    return localClientMetadata;
  }
  
  public static void setInstance(ClientMetadata paramClientMetadata)
  {
    synchronized (ClientMetadata.class)
    {
      i = paramClientMetadata;
      return;
    }
  }
  
  public ClientMetadata.MoPubNetworkType getActiveNetworkType()
  {
    Object localObject1 = r;
    String str = "android.permission.ACCESS_NETWORK_STATE";
    boolean bool1 = DeviceUtils.isPermissionGranted((Context)localObject1, str);
    if (!bool1) {
      return ClientMetadata.MoPubNetworkType.UNKNOWN;
    }
    localObject1 = s.getActiveNetworkInfo();
    if (localObject1 != null)
    {
      boolean bool3 = ((NetworkInfo)localObject1).isConnected();
      if (bool3)
      {
        int i3 = Build.VERSION.SDK_INT;
        int i4 = 21;
        if (i3 < i4)
        {
          int i1 = ((NetworkInfo)localObject1).getType();
          i3 = 9;
          if (i1 == i3) {
            return ClientMetadata.MoPubNetworkType.ETHERNET;
          }
        }
        else
        {
          localObject1 = s.getAllNetworks();
          i3 = localObject1.length;
          i4 = 0;
          while (i4 < i3)
          {
            Object localObject2 = localObject1[i4];
            ConnectivityManager localConnectivityManager = s;
            localObject2 = localConnectivityManager.getNetworkCapabilities((Network)localObject2);
            if (localObject2 != null)
            {
              int i5 = 3;
              boolean bool5 = ((NetworkCapabilities)localObject2).hasTransport(i5);
              if (bool5) {
                return ClientMetadata.MoPubNetworkType.ETHERNET;
              }
            }
            i4 += 1;
          }
        }
        localObject1 = s;
        i3 = 1;
        localObject1 = ((ConnectivityManager)localObject1).getNetworkInfo(i3);
        if (localObject1 != null)
        {
          boolean bool2 = ((NetworkInfo)localObject1).isConnected();
          if (bool2) {
            return ClientMetadata.MoPubNetworkType.WIFI;
          }
        }
        localObject1 = s.getNetworkInfo(0);
        if (localObject1 != null)
        {
          boolean bool4 = ((NetworkInfo)localObject1).isConnected();
          if (bool4)
          {
            int i2 = ((NetworkInfo)localObject1).getSubtype();
            switch (i2)
            {
            default: 
              return ClientMetadata.MoPubNetworkType.MOBILE;
            case 13: 
            case 15: 
              return ClientMetadata.MoPubNetworkType.GGGG;
            case 3: 
            case 5: 
            case 6: 
            case 8: 
            case 9: 
            case 10: 
            case 12: 
            case 14: 
              return ClientMetadata.MoPubNetworkType.GGG;
            }
            return ClientMetadata.MoPubNetworkType.GG;
          }
        }
        return ClientMetadata.MoPubNetworkType.UNKNOWN;
      }
    }
    return ClientMetadata.MoPubNetworkType.UNKNOWN;
  }
  
  public String getAppName()
  {
    return q;
  }
  
  public String getAppPackageName()
  {
    return p;
  }
  
  public String getAppVersion()
  {
    return o;
  }
  
  public float getDensity()
  {
    return r.getResources().getDisplayMetrics().density;
  }
  
  public Point getDeviceDimensions()
  {
    Object localObject = r;
    boolean bool = Preconditions.NoThrow.checkNotNull(localObject);
    if (bool) {
      return DeviceUtils.getDeviceDimensions(r);
    }
    localObject = new android/graphics/Point;
    ((Point)localObject).<init>(0, 0);
    return (Point)localObject;
  }
  
  public Locale getDeviceLocale()
  {
    return r.getResources().getConfiguration().locale;
  }
  
  public String getDeviceManufacturer()
  {
    return j;
  }
  
  public String getDeviceModel()
  {
    return k;
  }
  
  public String getDeviceOsVersion()
  {
    return m;
  }
  
  public String getDeviceProduct()
  {
    return l;
  }
  
  public int getDeviceScreenHeightDip()
  {
    return Dips.screenHeightAsIntDips(r);
  }
  
  public int getDeviceScreenWidthDip()
  {
    return Dips.screenWidthAsIntDips(r);
  }
  
  public String getIsoCountryCode()
  {
    boolean bool = MoPub.canCollectPersonalInformation();
    if (bool) {
      return d;
    }
    return "";
  }
  
  public MoPubIdentifier getMoPubIdentifier()
  {
    return h;
  }
  
  public String getNetworkOperator()
  {
    return b;
  }
  
  public String getNetworkOperatorForUrl()
  {
    return a;
  }
  
  public String getNetworkOperatorName()
  {
    return f;
  }
  
  public String getOrientationString()
  {
    Configuration localConfiguration = r.getResources().getConfiguration();
    int i1 = orientation;
    String str = "u";
    int i2 = 1;
    if (i1 == i2)
    {
      str = "p";
    }
    else
    {
      i2 = 2;
      if (i1 == i2)
      {
        str = "l";
      }
      else
      {
        i2 = 3;
        if (i1 == i2) {
          str = "s";
        }
      }
    }
    return str;
  }
  
  public String getSdkVersion()
  {
    return n;
  }
  
  public String getSimIsoCountryCode()
  {
    boolean bool = MoPub.canCollectPersonalInformation();
    if (bool) {
      return e;
    }
    return "";
  }
  
  public String getSimOperator()
  {
    return c;
  }
  
  public String getSimOperatorName()
  {
    return g;
  }
  
  public void repopulateCountryData()
  {
    Object localObject = r;
    String str = "phone";
    localObject = (TelephonyManager)((Context)localObject).getSystemService(str);
    boolean bool = MoPub.canCollectPersonalInformation();
    if ((bool) && (localObject != null))
    {
      str = ((TelephonyManager)localObject).getNetworkCountryIso();
      d = str;
      localObject = ((TelephonyManager)localObject).getSimCountryIso();
      e = ((String)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.ClientMetadata
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */