package com.mopub.common;

public abstract interface CacheService$DiskLruCacheGetListener
{
  public abstract void onComplete(String paramString, byte[] paramArrayOfByte);
}

/* Location:
 * Qualified Name:     com.mopub.common.CacheService.DiskLruCacheGetListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */