package com.mopub.common;

import android.os.SystemClock;

final class DoubleTimeTracker$b
  implements DoubleTimeTracker.Clock
{
  public final long elapsedRealTime()
  {
    return SystemClock.elapsedRealtime();
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.DoubleTimeTracker.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */