package com.mopub.common;

final class MoPub$a
  implements SdkInitializationListener
{
  private SdkInitializationListener a;
  
  MoPub$a(SdkInitializationListener paramSdkInitializationListener)
  {
    a = paramSdkInitializationListener;
  }
  
  public final void onInitializationFinished()
  {
    MoPub.a(a);
    a = null;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.MoPub.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */