package com.mopub.common;

import android.content.Context;
import android.net.Uri;
import com.mopub.exceptions.IntentNotResolvableException;

 enum UrlAction$1
{
  UrlAction$1()
  {
    super(paramString, 0, false, (byte)0);
  }
  
  protected final void a(Context paramContext, Uri paramUri, UrlHandler paramUrlHandler, String paramString)
  {
    paramContext = paramUri.getHost();
    paramUrlHandler = a;
    paramString = "finishLoad";
    boolean bool1 = paramString.equalsIgnoreCase(paramContext);
    if (bool1)
    {
      paramUrlHandler.onFinishLoad();
      return;
    }
    paramString = "close";
    bool1 = paramString.equalsIgnoreCase(paramContext);
    if (bool1)
    {
      paramUrlHandler.onClose();
      return;
    }
    paramString = "failLoad";
    boolean bool2 = paramString.equalsIgnoreCase(paramContext);
    if (bool2)
    {
      paramUrlHandler.onFailLoad();
      return;
    }
    paramContext = new com/mopub/exceptions/IntentNotResolvableException;
    paramUri = String.valueOf(paramUri);
    paramUri = "Could not handle MoPub Scheme url: ".concat(paramUri);
    paramContext.<init>(paramUri);
    throw paramContext;
  }
  
  public final boolean shouldTryHandlingUrl(Uri paramUri)
  {
    paramUri = paramUri.getScheme();
    return "mopub".equalsIgnoreCase(paramUri);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.UrlAction.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */