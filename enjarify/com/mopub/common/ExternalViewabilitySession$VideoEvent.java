package com.mopub.common;

public enum ExternalViewabilitySession$VideoEvent
{
  private String a;
  private String b;
  
  static
  {
    Object localObject = new com/mopub/common/ExternalViewabilitySession$VideoEvent;
    ((VideoEvent)localObject).<init>("AD_LOADED", 0, null, "recordAdLoadedEvent");
    AD_LOADED = (VideoEvent)localObject;
    localObject = new com/mopub/common/ExternalViewabilitySession$VideoEvent;
    int i = 1;
    ((VideoEvent)localObject).<init>("AD_STARTED", i, "AD_EVT_START", "recordAdStartedEvent");
    AD_STARTED = (VideoEvent)localObject;
    localObject = new com/mopub/common/ExternalViewabilitySession$VideoEvent;
    int j = 2;
    ((VideoEvent)localObject).<init>("AD_STOPPED", j, "AD_EVT_STOPPED", "recordAdStoppedEvent");
    AD_STOPPED = (VideoEvent)localObject;
    localObject = new com/mopub/common/ExternalViewabilitySession$VideoEvent;
    int k = 3;
    ((VideoEvent)localObject).<init>("AD_PAUSED", k, "AD_EVT_PAUSED", "recordAdPausedEvent");
    AD_PAUSED = (VideoEvent)localObject;
    localObject = new com/mopub/common/ExternalViewabilitySession$VideoEvent;
    int m = 4;
    ((VideoEvent)localObject).<init>("AD_PLAYING", m, "AD_EVT_PLAYING", "recordAdPlayingEvent");
    AD_PLAYING = (VideoEvent)localObject;
    localObject = new com/mopub/common/ExternalViewabilitySession$VideoEvent;
    int n = 5;
    ((VideoEvent)localObject).<init>("AD_SKIPPED", n, "AD_EVT_SKIPPED", "recordAdSkippedEvent");
    AD_SKIPPED = (VideoEvent)localObject;
    localObject = new com/mopub/common/ExternalViewabilitySession$VideoEvent;
    int i1 = 6;
    ((VideoEvent)localObject).<init>("AD_IMPRESSED", i1, null, "recordAdImpressionEvent");
    AD_IMPRESSED = (VideoEvent)localObject;
    localObject = new com/mopub/common/ExternalViewabilitySession$VideoEvent;
    int i2 = 7;
    ((VideoEvent)localObject).<init>("AD_CLICK_THRU", i2, null, "recordAdClickThruEvent");
    AD_CLICK_THRU = (VideoEvent)localObject;
    localObject = new com/mopub/common/ExternalViewabilitySession$VideoEvent;
    int i3 = 8;
    ((VideoEvent)localObject).<init>("AD_VIDEO_FIRST_QUARTILE", i3, "AD_EVT_FIRST_QUARTILE", "recordAdVideoFirstQuartileEvent");
    AD_VIDEO_FIRST_QUARTILE = (VideoEvent)localObject;
    localObject = new com/mopub/common/ExternalViewabilitySession$VideoEvent;
    int i4 = 9;
    ((VideoEvent)localObject).<init>("AD_VIDEO_MIDPOINT", i4, "AD_EVT_MID_POINT", "recordAdVideoMidpointEvent");
    AD_VIDEO_MIDPOINT = (VideoEvent)localObject;
    localObject = new com/mopub/common/ExternalViewabilitySession$VideoEvent;
    ((VideoEvent)localObject).<init>("AD_VIDEO_THIRD_QUARTILE", 10, "AD_EVT_THIRD_QUARTILE", "recordAdVideoThirdQuartileEvent");
    AD_VIDEO_THIRD_QUARTILE = (VideoEvent)localObject;
    localObject = new com/mopub/common/ExternalViewabilitySession$VideoEvent;
    int i5 = 11;
    ((VideoEvent)localObject).<init>("AD_COMPLETE", i5, "AD_EVT_COMPLETE", "recordAdCompleteEvent");
    AD_COMPLETE = (VideoEvent)localObject;
    localObject = new com/mopub/common/ExternalViewabilitySession$VideoEvent;
    int i6 = 12;
    ((VideoEvent)localObject).<init>("RECORD_AD_ERROR", i6, null, "recordAdError");
    RECORD_AD_ERROR = (VideoEvent)localObject;
    localObject = new VideoEvent[13];
    VideoEvent localVideoEvent = AD_LOADED;
    localObject[0] = localVideoEvent;
    localVideoEvent = AD_STARTED;
    localObject[i] = localVideoEvent;
    localVideoEvent = AD_STOPPED;
    localObject[j] = localVideoEvent;
    localVideoEvent = AD_PAUSED;
    localObject[k] = localVideoEvent;
    localVideoEvent = AD_PLAYING;
    localObject[m] = localVideoEvent;
    localVideoEvent = AD_SKIPPED;
    localObject[n] = localVideoEvent;
    localVideoEvent = AD_IMPRESSED;
    localObject[i1] = localVideoEvent;
    localVideoEvent = AD_CLICK_THRU;
    localObject[i2] = localVideoEvent;
    localVideoEvent = AD_VIDEO_FIRST_QUARTILE;
    localObject[i3] = localVideoEvent;
    localVideoEvent = AD_VIDEO_MIDPOINT;
    localObject[i4] = localVideoEvent;
    localVideoEvent = AD_VIDEO_THIRD_QUARTILE;
    localObject[10] = localVideoEvent;
    localVideoEvent = AD_COMPLETE;
    localObject[i5] = localVideoEvent;
    localVideoEvent = RECORD_AD_ERROR;
    localObject[i6] = localVideoEvent;
    c = (VideoEvent[])localObject;
  }
  
  private ExternalViewabilitySession$VideoEvent(String paramString2, String paramString3)
  {
    a = paramString2;
    b = paramString3;
  }
  
  public final String getAvidMethodName()
  {
    return b;
  }
  
  public final String getMoatEnumName()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.ExternalViewabilitySession.VideoEvent
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */