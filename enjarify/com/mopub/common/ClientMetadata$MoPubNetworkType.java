package com.mopub.common;

public enum ClientMetadata$MoPubNetworkType
{
  private final int a;
  
  static
  {
    Object localObject = new com/mopub/common/ClientMetadata$MoPubNetworkType;
    ((MoPubNetworkType)localObject).<init>("UNKNOWN", 0, 0);
    UNKNOWN = (MoPubNetworkType)localObject;
    localObject = new com/mopub/common/ClientMetadata$MoPubNetworkType;
    int i = 1;
    ((MoPubNetworkType)localObject).<init>("ETHERNET", i, i);
    ETHERNET = (MoPubNetworkType)localObject;
    localObject = new com/mopub/common/ClientMetadata$MoPubNetworkType;
    int j = 2;
    ((MoPubNetworkType)localObject).<init>("WIFI", j, j);
    WIFI = (MoPubNetworkType)localObject;
    localObject = new com/mopub/common/ClientMetadata$MoPubNetworkType;
    int k = 3;
    ((MoPubNetworkType)localObject).<init>("MOBILE", k, k);
    MOBILE = (MoPubNetworkType)localObject;
    localObject = new com/mopub/common/ClientMetadata$MoPubNetworkType;
    int m = 4;
    ((MoPubNetworkType)localObject).<init>("GG", m, m);
    GG = (MoPubNetworkType)localObject;
    localObject = new com/mopub/common/ClientMetadata$MoPubNetworkType;
    int n = 5;
    ((MoPubNetworkType)localObject).<init>("GGG", n, n);
    GGG = (MoPubNetworkType)localObject;
    localObject = new com/mopub/common/ClientMetadata$MoPubNetworkType;
    int i1 = 6;
    ((MoPubNetworkType)localObject).<init>("GGGG", i1, i1);
    GGGG = (MoPubNetworkType)localObject;
    localObject = new MoPubNetworkType[7];
    MoPubNetworkType localMoPubNetworkType = UNKNOWN;
    localObject[0] = localMoPubNetworkType;
    localMoPubNetworkType = ETHERNET;
    localObject[i] = localMoPubNetworkType;
    localMoPubNetworkType = WIFI;
    localObject[j] = localMoPubNetworkType;
    localMoPubNetworkType = MOBILE;
    localObject[k] = localMoPubNetworkType;
    localMoPubNetworkType = GG;
    localObject[m] = localMoPubNetworkType;
    localMoPubNetworkType = GGG;
    localObject[n] = localMoPubNetworkType;
    localMoPubNetworkType = GGGG;
    localObject[i1] = localMoPubNetworkType;
    b = (MoPubNetworkType[])localObject;
  }
  
  private ClientMetadata$MoPubNetworkType(int paramInt1)
  {
    a = paramInt1;
  }
  
  public final int getId()
  {
    return a;
  }
  
  public final String toString()
  {
    return Integer.toString(a);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.ClientMetadata.MoPubNetworkType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */