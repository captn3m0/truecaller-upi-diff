package com.mopub.common;

import android.os.Build.VERSION;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

final class MoPubBrowser$5
  extends WebChromeClient
{
  MoPubBrowser$5(MoPubBrowser paramMoPubBrowser) {}
  
  public final void onProgressChanged(WebView paramWebView, int paramInt)
  {
    int i = 100;
    Object localObject;
    if (paramInt == i)
    {
      localObject = a;
      paramWebView = paramWebView.getUrl();
      ((MoPubBrowser)localObject).setTitle(paramWebView);
    }
    else
    {
      paramWebView = a;
      localObject = "Loading...";
      paramWebView.setTitle((CharSequence)localObject);
    }
    paramWebView = a;
    boolean bool = MoPubBrowser.b(paramWebView);
    if (bool)
    {
      int j = Build.VERSION.SDK_INT;
      int k = 24;
      if (j < k)
      {
        paramWebView = a;
        paramInt *= 100;
        paramWebView.setProgress(paramInt);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.MoPubBrowser.5
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */