package com.mopub.common;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.mopub.common.util.Drawables;
import com.mopub.mobileads.BaseWebView;
import com.mopub.mobileads.util.WebViews;

public class MoPubBrowser
  extends Activity
{
  public static final String DESTINATION_URL_KEY = "URL";
  public static final String DSP_CREATIVE_ID = "mopub-dsp-creative-id";
  public static final int MOPUB_BROWSER_REQUEST_CODE = 1;
  private WebView a;
  private ImageButton b;
  private ImageButton c;
  private ImageButton d;
  private ImageButton e;
  private boolean f;
  
  private ImageButton a(Drawable paramDrawable)
  {
    ImageButton localImageButton = new android/widget/ImageButton;
    localImageButton.<init>(this);
    LinearLayout.LayoutParams localLayoutParams = new android/widget/LinearLayout$LayoutParams;
    int i = -2;
    localLayoutParams.<init>(i, i, 1.0F);
    gravity = 16;
    localImageButton.setLayoutParams(localLayoutParams);
    localImageButton.setImageDrawable(paramDrawable);
    return localImageButton;
  }
  
  public void finish()
  {
    ((ViewGroup)getWindow().getDecorView()).removeAllViews();
    super.finish();
  }
  
  public ImageButton getBackButton()
  {
    return b;
  }
  
  public ImageButton getCloseButton()
  {
    return e;
  }
  
  public ImageButton getForwardButton()
  {
    return c;
  }
  
  public ImageButton getRefreshButton()
  {
    return d;
  }
  
  public WebView getWebView()
  {
    return a;
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = -1;
    setResult(i);
    Object localObject1 = getWindow();
    int j = 2;
    boolean bool = ((Window)localObject1).requestFeature(j);
    f = bool;
    bool = f;
    if (bool)
    {
      localObject1 = getWindow();
      ((Window)localObject1).setFeatureInt(j, i);
    }
    localObject1 = new android/widget/LinearLayout;
    ((LinearLayout)localObject1).<init>(this);
    LinearLayout.LayoutParams localLayoutParams = new android/widget/LinearLayout$LayoutParams;
    localLayoutParams.<init>(i, i);
    ((LinearLayout)localObject1).setLayoutParams(localLayoutParams);
    int k = 1;
    ((LinearLayout)localObject1).setOrientation(k);
    RelativeLayout localRelativeLayout = new android/widget/RelativeLayout;
    localRelativeLayout.<init>(this);
    Object localObject2 = new android/widget/LinearLayout$LayoutParams;
    int m = -2;
    ((LinearLayout.LayoutParams)localObject2).<init>(i, m);
    localRelativeLayout.setLayoutParams((ViewGroup.LayoutParams)localObject2);
    ((LinearLayout)localObject1).addView(localRelativeLayout);
    localObject2 = new android/widget/LinearLayout;
    ((LinearLayout)localObject2).<init>(this);
    ((LinearLayout)localObject2).setId(k);
    RelativeLayout.LayoutParams localLayoutParams1 = new android/widget/RelativeLayout$LayoutParams;
    localLayoutParams1.<init>(i, m);
    localLayoutParams1.addRule(12);
    ((LinearLayout)localObject2).setLayoutParams(localLayoutParams1);
    Object localObject3 = Drawables.BACKGROUND.createDrawable(this);
    ((LinearLayout)localObject2).setBackgroundDrawable((Drawable)localObject3);
    localRelativeLayout.addView((View)localObject2);
    localObject3 = Drawables.UNLEFT_ARROW.createDrawable(this);
    localObject3 = a((Drawable)localObject3);
    b = ((ImageButton)localObject3);
    localObject3 = Drawables.UNRIGHT_ARROW.createDrawable(this);
    localObject3 = a((Drawable)localObject3);
    c = ((ImageButton)localObject3);
    localObject3 = Drawables.REFRESH.createDrawable(this);
    localObject3 = a((Drawable)localObject3);
    d = ((ImageButton)localObject3);
    localObject3 = Drawables.CLOSE.createDrawable(this);
    localObject3 = a((Drawable)localObject3);
    e = ((ImageButton)localObject3);
    localObject3 = b;
    ((LinearLayout)localObject2).addView((View)localObject3);
    localObject3 = c;
    ((LinearLayout)localObject2).addView((View)localObject3);
    localObject3 = d;
    ((LinearLayout)localObject2).addView((View)localObject3);
    localObject3 = e;
    ((LinearLayout)localObject2).addView((View)localObject3);
    localObject2 = new com/mopub/mobileads/BaseWebView;
    ((BaseWebView)localObject2).<init>(this);
    a = ((WebView)localObject2);
    localObject2 = new android/widget/RelativeLayout$LayoutParams;
    ((RelativeLayout.LayoutParams)localObject2).<init>(i, i);
    ((RelativeLayout.LayoutParams)localObject2).addRule(j, k);
    a.setLayoutParams((ViewGroup.LayoutParams)localObject2);
    paramBundle = a;
    localRelativeLayout.addView(paramBundle);
    setContentView((View)localObject1);
    paramBundle = a.getSettings();
    paramBundle.setJavaScriptEnabled(k);
    paramBundle.setSupportZoom(k);
    paramBundle.setBuiltInZoomControls(k);
    paramBundle.setUseWideViewPort(k);
    paramBundle = a;
    localObject1 = getIntent().getStringExtra("URL");
    paramBundle.loadUrl((String)localObject1);
    paramBundle = a;
    localObject1 = new com/mopub/common/c;
    ((c)localObject1).<init>(this);
    paramBundle.setWebViewClient((WebViewClient)localObject1);
    b.setBackgroundColor(0);
    paramBundle = b;
    Object localObject4 = new com/mopub/common/MoPubBrowser$1;
    ((MoPubBrowser.1)localObject4).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject4);
    c.setBackgroundColor(0);
    paramBundle = c;
    localObject4 = new com/mopub/common/MoPubBrowser$2;
    ((MoPubBrowser.2)localObject4).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject4);
    d.setBackgroundColor(0);
    paramBundle = d;
    localObject4 = new com/mopub/common/MoPubBrowser$3;
    ((MoPubBrowser.3)localObject4).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject4);
    e.setBackgroundColor(0);
    paramBundle = e;
    localObject1 = new com/mopub/common/MoPubBrowser$4;
    ((MoPubBrowser.4)localObject1).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject1);
    CookieSyncManager.createInstance(this);
    CookieSyncManager.getInstance().startSync();
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    a.destroy();
    a = null;
  }
  
  protected void onPause()
  {
    super.onPause();
    CookieSyncManager.getInstance().stopSync();
    a.setWebChromeClient(null);
    WebView localWebView = a;
    boolean bool = isFinishing();
    WebViews.onPause(localWebView, bool);
  }
  
  protected void onResume()
  {
    super.onResume();
    CookieSyncManager.getInstance().startSync();
    WebView localWebView = a;
    MoPubBrowser.5 local5 = new com/mopub/common/MoPubBrowser$5;
    local5.<init>(this);
    localWebView.setWebChromeClient(local5);
    a.onResume();
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.MoPubBrowser
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */