package com.mopub.common;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class DiskLruCache
  implements Closeable
{
  static final Pattern a = Pattern.compile("[a-z0-9_-]{1,64}");
  private static final OutputStream p;
  final ThreadPoolExecutor b;
  private final File c;
  private final File d;
  private final File e;
  private final File f;
  private final int g;
  private long h;
  private final int i;
  private long j;
  private Writer k;
  private final LinkedHashMap l;
  private int m;
  private long n;
  private final Callable o;
  
  static
  {
    DiskLruCache.2 local2 = new com/mopub/common/DiskLruCache$2;
    local2.<init>();
    p = local2;
  }
  
  private DiskLruCache(File paramFile, int paramInt1, int paramInt2, long paramLong)
  {
    long l1 = 0L;
    j = l1;
    LinkedHashMap localLinkedHashMap = new java/util/LinkedHashMap;
    localLinkedHashMap.<init>(0, 0.75F, true);
    l = localLinkedHashMap;
    n = l1;
    Object localObject = new java/util/concurrent/ThreadPoolExecutor;
    TimeUnit localTimeUnit = TimeUnit.SECONDS;
    LinkedBlockingQueue localLinkedBlockingQueue = new java/util/concurrent/LinkedBlockingQueue;
    localLinkedBlockingQueue.<init>();
    ((ThreadPoolExecutor)localObject).<init>(0, 1, 60, localTimeUnit, localLinkedBlockingQueue);
    b = ((ThreadPoolExecutor)localObject);
    localObject = new com/mopub/common/DiskLruCache$1;
    ((DiskLruCache.1)localObject).<init>(this);
    o = ((Callable)localObject);
    c = paramFile;
    g = paramInt1;
    localObject = new java/io/File;
    ((File)localObject).<init>(paramFile, "journal");
    d = ((File)localObject);
    localObject = new java/io/File;
    ((File)localObject).<init>(paramFile, "journal.tmp");
    e = ((File)localObject);
    localObject = new java/io/File;
    ((File)localObject).<init>(paramFile, "journal.bkp");
    f = ((File)localObject);
    i = paramInt2;
    h = paramLong;
  }
  
  private DiskLruCache.Editor a(String paramString, long paramLong)
  {
    try
    {
      f();
      a(paramString);
      Object localObject1 = l;
      localObject1 = ((LinkedHashMap)localObject1).get(paramString);
      localObject1 = (DiskLruCache.a)localObject1;
      long l1 = -1;
      boolean bool = paramLong < l1;
      if (bool) {
        if (localObject1 != null)
        {
          l1 = e;
          bool = l1 < paramLong;
          if (!bool) {}
        }
        else
        {
          return null;
        }
      }
      Writer localWriter = null;
      if (localObject1 == null)
      {
        localObject1 = new com/mopub/common/DiskLruCache$a;
        ((DiskLruCache.a)localObject1).<init>(this, paramString, (byte)0);
        localObject2 = l;
        ((LinkedHashMap)localObject2).put(paramString, localObject1);
      }
      else
      {
        localObject2 = d;
        if (localObject2 != null) {
          return null;
        }
      }
      Object localObject2 = new com/mopub/common/DiskLruCache$Editor;
      ((DiskLruCache.Editor)localObject2).<init>(this, (DiskLruCache.a)localObject1, (byte)0);
      d = ((DiskLruCache.Editor)localObject2);
      localWriter = k;
      localObject1 = new java/lang/StringBuilder;
      String str = "DIRTY ";
      ((StringBuilder)localObject1).<init>(str);
      ((StringBuilder)localObject1).append(paramString);
      char c1 = '\n';
      ((StringBuilder)localObject1).append(c1);
      paramString = ((StringBuilder)localObject1).toString();
      localWriter.write(paramString);
      paramString = k;
      paramString.flush();
      return (DiskLruCache.Editor)localObject2;
    }
    finally {}
  }
  
  private void a(DiskLruCache.Editor paramEditor, boolean paramBoolean)
  {
    try
    {
      Object localObject1 = DiskLruCache.Editor.a(paramEditor);
      Object localObject2 = d;
      if (localObject2 == paramEditor)
      {
        int i1 = 0;
        localObject2 = null;
        File localFile;
        Object localObject3;
        Object localObject4;
        if (paramBoolean)
        {
          int i2 = c;
          if (i2 == 0)
          {
            i2 = 0;
            localFile = null;
            int i3;
            for (;;)
            {
              int i4 = i;
              if (i2 >= i4) {
                break label147;
              }
              localObject3 = DiskLruCache.Editor.b(paramEditor);
              int i5 = localObject3[i2];
              if (i5 == 0) {
                break;
              }
              localObject3 = ((DiskLruCache.a)localObject1).getDirtyFile(i2);
              boolean bool3 = ((File)localObject3).exists();
              if (!bool3)
              {
                paramEditor.abort();
                return;
              }
              i2 += 1;
            }
            paramEditor.abort();
            paramEditor = new java/lang/IllegalStateException;
            localObject4 = "Newly created entry didn't create value for index ";
            localObject1 = String.valueOf(i3);
            localObject4 = ((String)localObject4).concat((String)localObject1);
            paramEditor.<init>((String)localObject4);
            throw paramEditor;
          }
        }
        for (;;)
        {
          label147:
          i6 = i;
          if (i1 >= i6) {
            break;
          }
          paramEditor = ((DiskLruCache.a)localObject1).getDirtyFile(i1);
          if (paramBoolean)
          {
            boolean bool1 = paramEditor.exists();
            if (bool1)
            {
              localFile = ((DiskLruCache.a)localObject1).getCleanFile(i1);
              paramEditor.renameTo(localFile);
              paramEditor = b;
              long l1 = paramEditor[i1];
              long l2 = localFile.length();
              paramEditor = b;
              paramEditor[i1] = l2;
              long l3 = j - l1 + l2;
              j = l3;
            }
          }
          else
          {
            a(paramEditor);
          }
          i1 += 1;
        }
        int i6 = m;
        i1 = 1;
        i6 += i1;
        m = i6;
        i6 = 0;
        paramEditor = null;
        d = null;
        boolean bool4 = c | paramBoolean;
        char c1 = '\n';
        if (bool4)
        {
          c = i1;
          paramEditor = k;
          localObject2 = new java/lang/StringBuilder;
          localObject3 = "CLEAN ";
          ((StringBuilder)localObject2).<init>((String)localObject3);
          localObject3 = a;
          ((StringBuilder)localObject2).append((String)localObject3);
          localObject3 = ((DiskLruCache.a)localObject1).getLengths();
          ((StringBuilder)localObject2).append((String)localObject3);
          ((StringBuilder)localObject2).append(c1);
          localObject2 = ((StringBuilder)localObject2).toString();
          paramEditor.write((String)localObject2);
          if (paramBoolean)
          {
            l4 = n;
            long l5 = 1L + l4;
            n = l5;
            e = l4;
          }
        }
        else
        {
          paramEditor = l;
          localObject4 = a;
          paramEditor.remove(localObject4);
          paramEditor = k;
          localObject4 = new java/lang/StringBuilder;
          localObject2 = "REMOVE ";
          ((StringBuilder)localObject4).<init>((String)localObject2);
          localObject1 = a;
          ((StringBuilder)localObject4).append((String)localObject1);
          ((StringBuilder)localObject4).append(c1);
          localObject4 = ((StringBuilder)localObject4).toString();
          paramEditor.write((String)localObject4);
        }
        paramEditor = k;
        paramEditor.flush();
        long l4 = j;
        long l6 = h;
        boolean bool2 = l4 < l6;
        if (!bool2)
        {
          bool4 = e();
          if (!bool4) {}
        }
        else
        {
          paramEditor = b;
          localObject4 = o;
          paramEditor.submit((Callable)localObject4);
        }
        return;
      }
      paramEditor = new java/lang/IllegalStateException;
      paramEditor.<init>();
      throw paramEditor;
    }
    finally {}
  }
  
  private static void a(File paramFile)
  {
    boolean bool1 = paramFile.exists();
    if (bool1)
    {
      boolean bool2 = paramFile.delete();
      if (!bool2)
      {
        paramFile = new java/io/IOException;
        paramFile.<init>();
        throw paramFile;
      }
    }
  }
  
  private static void a(File paramFile1, File paramFile2, boolean paramBoolean)
  {
    if (paramBoolean) {
      a(paramFile2);
    }
    boolean bool = paramFile1.renameTo(paramFile2);
    if (bool) {
      return;
    }
    paramFile1 = new java/io/IOException;
    paramFile1.<init>();
    throw paramFile1;
  }
  
  private static void a(String paramString)
  {
    Object localObject = a.matcher(paramString);
    boolean bool = ((Matcher)localObject).matches();
    if (bool) {
      return;
    }
    localObject = new java/lang/IllegalArgumentException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("keys must match regex [a-z0-9_-]{1,64}: \"");
    localStringBuilder.append(paramString);
    localStringBuilder.append("\"");
    paramString = localStringBuilder.toString();
    ((IllegalArgumentException)localObject).<init>(paramString);
    throw ((Throwable)localObject);
  }
  
  private void b()
  {
    locale = new com/mopub/common/e;
    Object localObject1 = new java/io/FileInputStream;
    Object localObject3 = d;
    ((FileInputStream)localObject1).<init>((File)localObject3);
    localObject3 = DiskLruCacheUtil.a;
    locale.<init>((InputStream)localObject1, (Charset)localObject3);
    for (;;)
    {
      try
      {
        localObject1 = locale.readLine();
        localObject3 = locale.readLine();
        localObject4 = locale.readLine();
        str1 = locale.readLine();
        str2 = locale.readLine();
        localObject5 = "libcore.io.DiskLruCache";
        boolean bool1 = ((String)localObject5).equals(localObject1);
        if (bool1)
        {
          localObject5 = "1";
          bool1 = ((String)localObject5).equals(localObject3);
          if (bool1)
          {
            i1 = g;
            localObject5 = Integer.toString(i1);
            boolean bool2 = ((String)localObject5).equals(localObject4);
            if (bool2)
            {
              int i2 = i;
              localObject4 = Integer.toString(i2);
              boolean bool3 = ((String)localObject4).equals(str1);
              if (bool3)
              {
                localObject4 = "";
                bool3 = ((String)localObject4).equals(str2);
                if (bool3)
                {
                  i3 = 0;
                  localObject1 = null;
                  i4 = 0;
                  localObject3 = null;
                }
              }
            }
          }
        }
      }
      finally
      {
        Object localObject4;
        String str1;
        String str2;
        Object localObject5;
        int i1;
        int i3;
        int i4;
        int i5;
        int i6;
        String str3;
        DiskLruCacheUtil.a(locale);
      }
      try
      {
        localObject4 = locale.readLine();
        i5 = 32;
        i6 = ((String)localObject4).indexOf(i5);
        i1 = -1;
        if (i6 != i1)
        {
          int i7 = i6 + 1;
          i5 = ((String)localObject4).indexOf(i5, i7);
          if (i5 == i1)
          {
            str3 = ((String)localObject4).substring(i7);
            int i8 = 6;
            if (i6 == i8)
            {
              localObject6 = "REMOVE";
              boolean bool6 = ((String)localObject4).startsWith((String)localObject6);
              if (bool6)
              {
                localObject4 = l;
                ((LinkedHashMap)localObject4).remove(str3);
                continue;
              }
            }
          }
          else
          {
            str3 = ((String)localObject4).substring(i7, i5);
          }
          Object localObject6 = l;
          localObject6 = ((LinkedHashMap)localObject6).get(str3);
          localObject6 = (DiskLruCache.a)localObject6;
          Object localObject7;
          if (localObject6 == null)
          {
            localObject6 = new com/mopub/common/DiskLruCache$a;
            ((DiskLruCache.a)localObject6).<init>(this, str3, (byte)0);
            localObject7 = l;
            ((LinkedHashMap)localObject7).put(str3, localObject6);
          }
          i7 = 5;
          if ((i5 != i1) && (i6 == i7))
          {
            localObject7 = "CLEAN";
            boolean bool7 = ((String)localObject4).startsWith((String)localObject7);
            if (bool7)
            {
              i5 += 1;
              localObject4 = ((String)localObject4).substring(i5);
              str1 = " ";
              localObject4 = ((String)localObject4).split(str1);
              i5 = 1;
              c = i5;
              i5 = 0;
              str1 = null;
              d = null;
              ((DiskLruCache.a)localObject6).a((String[])localObject4);
              continue;
            }
          }
          if ((i5 == i1) && (i6 == i7))
          {
            str3 = "DIRTY";
            boolean bool5 = ((String)localObject4).startsWith(str3);
            if (bool5)
            {
              localObject4 = new com/mopub/common/DiskLruCache$Editor;
              ((DiskLruCache.Editor)localObject4).<init>(this, (DiskLruCache.a)localObject6, (byte)0);
              d = ((DiskLruCache.Editor)localObject4);
              continue;
            }
          }
          if (i5 == i1)
          {
            i5 = 4;
            if (i6 == i5)
            {
              str1 = "READ";
              boolean bool4 = ((String)localObject4).startsWith(str1);
              if (bool4)
              {
                i4 += 1;
                continue;
              }
            }
          }
          localObject1 = new java/io/IOException;
          str1 = "unexpected journal line: ";
          localObject4 = String.valueOf(localObject4);
          localObject4 = str1.concat((String)localObject4);
          ((IOException)localObject1).<init>((String)localObject4);
          throw ((Throwable)localObject1);
        }
        else
        {
          localObject1 = new java/io/IOException;
          str1 = "unexpected journal line: ";
          localObject4 = String.valueOf(localObject4);
          localObject4 = str1.concat((String)localObject4);
          ((IOException)localObject1).<init>((String)localObject4);
          throw ((Throwable)localObject1);
        }
      }
      catch (EOFException localEOFException) {}
    }
    localObject1 = l;
    i3 = ((LinkedHashMap)localObject1).size();
    i4 -= i3;
    m = i4;
    DiskLruCacheUtil.a(locale);
    return;
    localObject4 = new java/io/IOException;
    localObject5 = new java/lang/StringBuilder;
    str3 = "unexpected journal header: [";
    ((StringBuilder)localObject5).<init>(str3);
    ((StringBuilder)localObject5).append((String)localObject1);
    localObject1 = ", ";
    ((StringBuilder)localObject5).append((String)localObject1);
    ((StringBuilder)localObject5).append((String)localObject3);
    localObject1 = ", ";
    ((StringBuilder)localObject5).append((String)localObject1);
    ((StringBuilder)localObject5).append(str1);
    localObject1 = ", ";
    ((StringBuilder)localObject5).append((String)localObject1);
    ((StringBuilder)localObject5).append(str2);
    localObject1 = "]";
    ((StringBuilder)localObject5).append((String)localObject1);
    localObject1 = ((StringBuilder)localObject5).toString();
    ((IOException)localObject4).<init>((String)localObject1);
    throw ((Throwable)localObject4);
  }
  
  private void c()
  {
    a(e);
    Iterator localIterator = l.values().iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      DiskLruCache.a locala = (DiskLruCache.a)localIterator.next();
      Object localObject = d;
      int i1 = 0;
      if (localObject == null) {
        for (;;)
        {
          i2 = i;
          if (i1 >= i2) {
            break;
          }
          long l1 = j;
          localObject = b;
          long l2 = localObject[i1];
          l1 += l2;
          j = l1;
          i1 += 1;
        }
      }
      int i2 = 0;
      localObject = null;
      d = null;
      for (;;)
      {
        i2 = i;
        if (i1 >= i2) {
          break;
        }
        a(locala.getCleanFile(i1));
        localObject = locala.getDirtyFile(i1);
        a((File)localObject);
        i1 += 1;
      }
      localIterator.remove();
    }
  }
  
  /* Error */
  private void d()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 147	com/mopub/common/DiskLruCache:k	Ljava/io/Writer;
    //   6: astore_1
    //   7: aload_1
    //   8: ifnull +12 -> 20
    //   11: aload_0
    //   12: getfield 147	com/mopub/common/DiskLruCache:k	Ljava/io/Writer;
    //   15: astore_1
    //   16: aload_1
    //   17: invokevirtual 398	java/io/Writer:close	()V
    //   20: new 400	java/io/BufferedWriter
    //   23: astore_1
    //   24: new 402	java/io/OutputStreamWriter
    //   27: astore_2
    //   28: new 404	java/io/FileOutputStream
    //   31: astore_3
    //   32: aload_0
    //   33: getfield 106	com/mopub/common/DiskLruCache:e	Ljava/io/File;
    //   36: astore 4
    //   38: aload_3
    //   39: aload 4
    //   41: invokespecial 405	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   44: getstatic 295	com/mopub/common/DiskLruCacheUtil:a	Ljava/nio/charset/Charset;
    //   47: astore 4
    //   49: aload_2
    //   50: aload_3
    //   51: aload 4
    //   53: invokespecial 408	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V
    //   56: aload_1
    //   57: aload_2
    //   58: invokespecial 411	java/io/BufferedWriter:<init>	(Ljava/io/Writer;)V
    //   61: ldc_w 301
    //   64: astore_2
    //   65: aload_1
    //   66: aload_2
    //   67: invokevirtual 170	java/io/Writer:write	(Ljava/lang/String;)V
    //   70: ldc_w 413
    //   73: astore_2
    //   74: aload_1
    //   75: aload_2
    //   76: invokevirtual 170	java/io/Writer:write	(Ljava/lang/String;)V
    //   79: ldc_w 307
    //   82: astore_2
    //   83: aload_1
    //   84: aload_2
    //   85: invokevirtual 170	java/io/Writer:write	(Ljava/lang/String;)V
    //   88: ldc_w 413
    //   91: astore_2
    //   92: aload_1
    //   93: aload_2
    //   94: invokevirtual 170	java/io/Writer:write	(Ljava/lang/String;)V
    //   97: aload_0
    //   98: getfield 93	com/mopub/common/DiskLruCache:g	I
    //   101: istore 5
    //   103: iload 5
    //   105: invokestatic 311	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   108: astore_2
    //   109: aload_1
    //   110: aload_2
    //   111: invokevirtual 170	java/io/Writer:write	(Ljava/lang/String;)V
    //   114: ldc_w 413
    //   117: astore_2
    //   118: aload_1
    //   119: aload_2
    //   120: invokevirtual 170	java/io/Writer:write	(Ljava/lang/String;)V
    //   123: aload_0
    //   124: getfield 112	com/mopub/common/DiskLruCache:i	I
    //   127: istore 5
    //   129: iload 5
    //   131: invokestatic 311	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   134: astore_2
    //   135: aload_1
    //   136: aload_2
    //   137: invokevirtual 170	java/io/Writer:write	(Ljava/lang/String;)V
    //   140: ldc_w 413
    //   143: astore_2
    //   144: aload_1
    //   145: aload_2
    //   146: invokevirtual 170	java/io/Writer:write	(Ljava/lang/String;)V
    //   149: ldc_w 413
    //   152: astore_2
    //   153: aload_1
    //   154: aload_2
    //   155: invokevirtual 170	java/io/Writer:write	(Ljava/lang/String;)V
    //   158: aload_0
    //   159: getfield 62	com/mopub/common/DiskLruCache:l	Ljava/util/LinkedHashMap;
    //   162: astore_2
    //   163: aload_2
    //   164: invokevirtual 378	java/util/LinkedHashMap:values	()Ljava/util/Collection;
    //   167: astore_2
    //   168: aload_2
    //   169: invokeinterface 384 1 0
    //   174: astore_2
    //   175: aload_2
    //   176: invokeinterface 389 1 0
    //   181: istore 6
    //   183: iload 6
    //   185: ifeq +144 -> 329
    //   188: aload_2
    //   189: invokeinterface 393 1 0
    //   194: astore_3
    //   195: aload_3
    //   196: checkcast 128	com/mopub/common/DiskLruCache$a
    //   199: astore_3
    //   200: aload_3
    //   201: getfield 140	com/mopub/common/DiskLruCache$a:d	Lcom/mopub/common/DiskLruCache$Editor;
    //   204: astore 4
    //   206: bipush 10
    //   208: istore 7
    //   210: aload 4
    //   212: ifnull +53 -> 265
    //   215: new 149	java/lang/StringBuilder
    //   218: astore 4
    //   220: ldc -105
    //   222: astore 8
    //   224: aload 4
    //   226: aload 8
    //   228: invokespecial 153	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   231: aload_3
    //   232: getfield 245	com/mopub/common/DiskLruCache$a:a	Ljava/lang/String;
    //   235: astore_3
    //   236: aload 4
    //   238: aload_3
    //   239: invokevirtual 157	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   242: pop
    //   243: aload 4
    //   245: iload 7
    //   247: invokevirtual 161	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   250: pop
    //   251: aload 4
    //   253: invokevirtual 165	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   256: astore_3
    //   257: aload_1
    //   258: aload_3
    //   259: invokevirtual 170	java/io/Writer:write	(Ljava/lang/String;)V
    //   262: goto -87 -> 175
    //   265: new 149	java/lang/StringBuilder
    //   268: astore 4
    //   270: ldc -14
    //   272: astore 8
    //   274: aload 4
    //   276: aload 8
    //   278: invokespecial 153	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   281: aload_3
    //   282: getfield 245	com/mopub/common/DiskLruCache$a:a	Ljava/lang/String;
    //   285: astore 8
    //   287: aload 4
    //   289: aload 8
    //   291: invokevirtual 157	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   294: pop
    //   295: aload_3
    //   296: invokevirtual 248	com/mopub/common/DiskLruCache$a:getLengths	()Ljava/lang/String;
    //   299: astore_3
    //   300: aload 4
    //   302: aload_3
    //   303: invokevirtual 157	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   306: pop
    //   307: aload 4
    //   309: iload 7
    //   311: invokevirtual 161	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   314: pop
    //   315: aload 4
    //   317: invokevirtual 165	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   320: astore_3
    //   321: aload_1
    //   322: aload_3
    //   323: invokevirtual 170	java/io/Writer:write	(Ljava/lang/String;)V
    //   326: goto -151 -> 175
    //   329: aload_1
    //   330: invokevirtual 398	java/io/Writer:close	()V
    //   333: aload_0
    //   334: getfield 102	com/mopub/common/DiskLruCache:d	Ljava/io/File;
    //   337: astore_1
    //   338: aload_1
    //   339: invokevirtual 203	java/io/File:exists	()Z
    //   342: istore 9
    //   344: iconst_1
    //   345: istore 5
    //   347: iload 9
    //   349: ifeq +20 -> 369
    //   352: aload_0
    //   353: getfield 102	com/mopub/common/DiskLruCache:d	Ljava/io/File;
    //   356: astore_1
    //   357: aload_0
    //   358: getfield 110	com/mopub/common/DiskLruCache:f	Ljava/io/File;
    //   361: astore_3
    //   362: aload_1
    //   363: aload_3
    //   364: iload 5
    //   366: invokestatic 416	com/mopub/common/DiskLruCache:a	(Ljava/io/File;Ljava/io/File;Z)V
    //   369: aload_0
    //   370: getfield 106	com/mopub/common/DiskLruCache:e	Ljava/io/File;
    //   373: astore_1
    //   374: aload_0
    //   375: getfield 102	com/mopub/common/DiskLruCache:d	Ljava/io/File;
    //   378: astore_3
    //   379: aconst_null
    //   380: astore 4
    //   382: aload_1
    //   383: aload_3
    //   384: iconst_0
    //   385: invokestatic 416	com/mopub/common/DiskLruCache:a	(Ljava/io/File;Ljava/io/File;Z)V
    //   388: aload_0
    //   389: getfield 110	com/mopub/common/DiskLruCache:f	Ljava/io/File;
    //   392: astore_1
    //   393: aload_1
    //   394: invokevirtual 268	java/io/File:delete	()Z
    //   397: pop
    //   398: new 400	java/io/BufferedWriter
    //   401: astore_1
    //   402: new 402	java/io/OutputStreamWriter
    //   405: astore_3
    //   406: new 404	java/io/FileOutputStream
    //   409: astore 4
    //   411: aload_0
    //   412: getfield 102	com/mopub/common/DiskLruCache:d	Ljava/io/File;
    //   415: astore 10
    //   417: aload 4
    //   419: aload 10
    //   421: iload 5
    //   423: invokespecial 419	java/io/FileOutputStream:<init>	(Ljava/io/File;Z)V
    //   426: getstatic 295	com/mopub/common/DiskLruCacheUtil:a	Ljava/nio/charset/Charset;
    //   429: astore_2
    //   430: aload_3
    //   431: aload 4
    //   433: aload_2
    //   434: invokespecial 408	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V
    //   437: aload_1
    //   438: aload_3
    //   439: invokespecial 411	java/io/BufferedWriter:<init>	(Ljava/io/Writer;)V
    //   442: aload_0
    //   443: aload_1
    //   444: putfield 147	com/mopub/common/DiskLruCache:k	Ljava/io/Writer;
    //   447: aload_0
    //   448: monitorexit
    //   449: return
    //   450: astore_2
    //   451: aload_1
    //   452: invokevirtual 398	java/io/Writer:close	()V
    //   455: aload_2
    //   456: athrow
    //   457: astore_1
    //   458: aload_0
    //   459: monitorexit
    //   460: aload_1
    //   461: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	462	0	this	DiskLruCache
    //   6	446	1	localObject1	Object
    //   457	4	1	localObject2	Object
    //   27	407	2	localObject3	Object
    //   450	6	2	localObject4	Object
    //   31	408	3	localObject5	Object
    //   36	396	4	localObject6	Object
    //   101	321	5	i1	int
    //   181	3	6	bool1	boolean
    //   208	102	7	c1	char
    //   222	68	8	str	String
    //   342	6	9	bool2	boolean
    //   415	5	10	localFile	File
    // Exception table:
    //   from	to	target	type
    //   66	70	450	finally
    //   75	79	450	finally
    //   84	88	450	finally
    //   93	97	450	finally
    //   97	101	450	finally
    //   103	108	450	finally
    //   110	114	450	finally
    //   119	123	450	finally
    //   123	127	450	finally
    //   129	134	450	finally
    //   136	140	450	finally
    //   145	149	450	finally
    //   154	158	450	finally
    //   158	162	450	finally
    //   163	167	450	finally
    //   168	174	450	finally
    //   175	181	450	finally
    //   188	194	450	finally
    //   195	199	450	finally
    //   200	204	450	finally
    //   215	218	450	finally
    //   226	231	450	finally
    //   231	235	450	finally
    //   238	243	450	finally
    //   245	251	450	finally
    //   251	256	450	finally
    //   258	262	450	finally
    //   265	268	450	finally
    //   276	281	450	finally
    //   281	285	450	finally
    //   289	295	450	finally
    //   295	299	450	finally
    //   302	307	450	finally
    //   309	315	450	finally
    //   315	320	450	finally
    //   322	326	450	finally
    //   2	6	457	finally
    //   11	15	457	finally
    //   16	20	457	finally
    //   20	23	457	finally
    //   24	27	457	finally
    //   28	31	457	finally
    //   32	36	457	finally
    //   39	44	457	finally
    //   44	47	457	finally
    //   51	56	457	finally
    //   57	61	457	finally
    //   329	333	457	finally
    //   333	337	457	finally
    //   338	342	457	finally
    //   352	356	457	finally
    //   357	361	457	finally
    //   364	369	457	finally
    //   369	373	457	finally
    //   374	378	457	finally
    //   384	388	457	finally
    //   388	392	457	finally
    //   393	398	457	finally
    //   398	401	457	finally
    //   402	405	457	finally
    //   406	409	457	finally
    //   411	415	457	finally
    //   421	426	457	finally
    //   426	429	457	finally
    //   433	437	457	finally
    //   438	442	457	finally
    //   443	447	457	finally
    //   451	455	457	finally
    //   455	457	457	finally
  }
  
  private boolean e()
  {
    int i1 = m;
    int i2 = 2000;
    if (i1 >= i2)
    {
      LinkedHashMap localLinkedHashMap = l;
      i2 = localLinkedHashMap.size();
      if (i1 >= i2) {
        return true;
      }
    }
    return false;
  }
  
  private void f()
  {
    Object localObject = k;
    if (localObject != null) {
      return;
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("cache is closed");
    throw ((Throwable)localObject);
  }
  
  private void g()
  {
    for (;;)
    {
      long l1 = j;
      long l2 = h;
      boolean bool = l1 < l2;
      if (!bool) {
        break;
      }
      String str = (String)((Map.Entry)l.entrySet().iterator().next()).getKey();
      remove(str);
    }
  }
  
  public static DiskLruCache open(File paramFile, int paramInt1, int paramInt2, long paramLong)
  {
    long l1 = 0L;
    boolean bool1 = paramLong < l1;
    if (bool1)
    {
      if (paramInt2 > 0)
      {
        Object localObject1 = new java/io/File;
        Object localObject2 = "journal.bkp";
        ((File)localObject1).<init>(paramFile, (String)localObject2);
        boolean bool2 = ((File)localObject1).exists();
        Object localObject3;
        if (bool2)
        {
          localObject2 = new java/io/File;
          localObject3 = "journal";
          ((File)localObject2).<init>(paramFile, (String)localObject3);
          bool1 = ((File)localObject2).exists();
          if (bool1)
          {
            ((File)localObject1).delete();
          }
          else
          {
            bool1 = false;
            localObject3 = null;
            a((File)localObject1, (File)localObject2, false);
          }
        }
        localObject1 = new com/mopub/common/DiskLruCache;
        Object localObject4 = localObject1;
        Object localObject5 = paramFile;
        int i1 = paramInt1;
        ((DiskLruCache)localObject1).<init>(paramFile, paramInt1, paramInt2, paramLong);
        localObject2 = d;
        bool2 = ((File)localObject2).exists();
        if (bool2) {
          try
          {
            ((DiskLruCache)localObject1).b();
            ((DiskLruCache)localObject1).c();
            localObject2 = new java/io/BufferedWriter;
            localObject3 = new java/io/OutputStreamWriter;
            localObject4 = new java/io/FileOutputStream;
            localObject5 = d;
            i1 = 1;
            ((FileOutputStream)localObject4).<init>((File)localObject5, i1);
            localObject5 = DiskLruCacheUtil.a;
            ((OutputStreamWriter)localObject3).<init>((OutputStream)localObject4, (Charset)localObject5);
            ((BufferedWriter)localObject2).<init>((Writer)localObject3);
            k = ((Writer)localObject2);
            return (DiskLruCache)localObject1;
          }
          catch (IOException localIOException)
          {
            localObject3 = System.out;
            localObject4 = new java/lang/StringBuilder;
            ((StringBuilder)localObject4).<init>("DiskLruCache ");
            ((StringBuilder)localObject4).append(paramFile);
            localObject5 = " is corrupt: ";
            ((StringBuilder)localObject4).append((String)localObject5);
            String str = localIOException.getMessage();
            ((StringBuilder)localObject4).append(str);
            ((StringBuilder)localObject4).append(", removing");
            str = ((StringBuilder)localObject4).toString();
            ((PrintStream)localObject3).println(str);
            ((DiskLruCache)localObject1).delete();
          }
        }
        paramFile.mkdirs();
        localObject1 = new com/mopub/common/DiskLruCache;
        localObject4 = localObject1;
        localObject5 = paramFile;
        int i2 = paramInt1;
        ((DiskLruCache)localObject1).<init>(paramFile, paramInt1, paramInt2, paramLong);
        ((DiskLruCache)localObject1).d();
        return (DiskLruCache)localObject1;
      }
      paramFile = new java/lang/IllegalArgumentException;
      paramFile.<init>("valueCount <= 0");
      throw paramFile;
    }
    paramFile = new java/lang/IllegalArgumentException;
    paramFile.<init>("maxSize <= 0");
    throw paramFile;
  }
  
  public final void close()
  {
    try
    {
      Object localObject1 = k;
      if (localObject1 == null) {
        return;
      }
      localObject1 = new java/util/ArrayList;
      Object localObject3 = l;
      localObject3 = ((LinkedHashMap)localObject3).values();
      ((ArrayList)localObject1).<init>((Collection)localObject3);
      localObject1 = ((ArrayList)localObject1).iterator();
      for (;;)
      {
        boolean bool = ((Iterator)localObject1).hasNext();
        if (!bool) {
          break;
        }
        localObject3 = ((Iterator)localObject1).next();
        localObject3 = (DiskLruCache.a)localObject3;
        DiskLruCache.Editor localEditor = d;
        if (localEditor != null)
        {
          localObject3 = d;
          ((DiskLruCache.Editor)localObject3).abort();
        }
      }
      g();
      localObject1 = k;
      ((Writer)localObject1).close();
      localObject1 = null;
      k = null;
      return;
    }
    finally {}
  }
  
  public final void delete()
  {
    close();
    DiskLruCacheUtil.a(c);
  }
  
  public final DiskLruCache.Editor edit(String paramString)
  {
    return a(paramString, -1);
  }
  
  public final void flush()
  {
    try
    {
      f();
      g();
      Writer localWriter = k;
      localWriter.flush();
      return;
    }
    finally {}
  }
  
  /* Error */
  public final DiskLruCache.Snapshot get(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial 119	com/mopub/common/DiskLruCache:f	()V
    //   6: aload_1
    //   7: invokestatic 122	com/mopub/common/DiskLruCache:a	(Ljava/lang/String;)V
    //   10: aload_0
    //   11: getfield 62	com/mopub/common/DiskLruCache:l	Ljava/util/LinkedHashMap;
    //   14: astore_2
    //   15: aload_2
    //   16: aload_1
    //   17: invokevirtual 126	java/util/LinkedHashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   20: astore_2
    //   21: aload_2
    //   22: checkcast 128	com/mopub/common/DiskLruCache$a
    //   25: astore_2
    //   26: iconst_0
    //   27: istore_3
    //   28: aconst_null
    //   29: astore 4
    //   31: aload_2
    //   32: ifnonnull +7 -> 39
    //   35: aload_0
    //   36: monitorexit
    //   37: aconst_null
    //   38: areturn
    //   39: aload_2
    //   40: getfield 192	com/mopub/common/DiskLruCache$a:c	Z
    //   43: istore 5
    //   45: iload 5
    //   47: ifne +7 -> 54
    //   50: aload_0
    //   51: monitorexit
    //   52: aconst_null
    //   53: areturn
    //   54: aload_0
    //   55: getfield 112	com/mopub/common/DiskLruCache:i	I
    //   58: istore 5
    //   60: iload 5
    //   62: anewarray 487	java/io/InputStream
    //   65: astore 6
    //   67: iconst_0
    //   68: istore 5
    //   70: aconst_null
    //   71: astore 7
    //   73: iconst_0
    //   74: istore 8
    //   76: aconst_null
    //   77: astore 9
    //   79: aload_0
    //   80: getfield 112	com/mopub/common/DiskLruCache:i	I
    //   83: istore 10
    //   85: iload 8
    //   87: iload 10
    //   89: if_icmpge +39 -> 128
    //   92: new 291	java/io/FileInputStream
    //   95: astore 11
    //   97: aload_2
    //   98: iload 8
    //   100: invokevirtual 224	com/mopub/common/DiskLruCache$a:getCleanFile	(I)Ljava/io/File;
    //   103: astore 12
    //   105: aload 11
    //   107: aload 12
    //   109: invokespecial 293	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   112: aload 6
    //   114: iload 8
    //   116: aload 11
    //   118: aastore
    //   119: iload 8
    //   121: iconst_1
    //   122: iadd
    //   123: istore 8
    //   125: goto -46 -> 79
    //   128: aload_0
    //   129: getfield 240	com/mopub/common/DiskLruCache:m	I
    //   132: iconst_1
    //   133: iadd
    //   134: istore_3
    //   135: aload_0
    //   136: iload_3
    //   137: putfield 240	com/mopub/common/DiskLruCache:m	I
    //   140: aload_0
    //   141: getfield 147	com/mopub/common/DiskLruCache:k	Ljava/io/Writer;
    //   144: astore 4
    //   146: new 149	java/lang/StringBuilder
    //   149: astore 7
    //   151: ldc_w 489
    //   154: astore 9
    //   156: aload 7
    //   158: aload 9
    //   160: invokespecial 153	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   163: aload 7
    //   165: aload_1
    //   166: invokevirtual 157	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   169: pop
    //   170: bipush 10
    //   172: istore 8
    //   174: aload 7
    //   176: iload 8
    //   178: invokevirtual 161	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   181: pop
    //   182: aload 7
    //   184: invokevirtual 165	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   187: astore 7
    //   189: aload 4
    //   191: aload 7
    //   193: invokevirtual 492	java/io/Writer:append	(Ljava/lang/CharSequence;)Ljava/io/Writer;
    //   196: pop
    //   197: aload_0
    //   198: invokespecial 257	com/mopub/common/DiskLruCache:e	()Z
    //   201: istore_3
    //   202: iload_3
    //   203: ifeq +23 -> 226
    //   206: aload_0
    //   207: getfield 82	com/mopub/common/DiskLruCache:b	Ljava/util/concurrent/ThreadPoolExecutor;
    //   210: astore 4
    //   212: aload_0
    //   213: getfield 89	com/mopub/common/DiskLruCache:o	Ljava/util/concurrent/Callable;
    //   216: astore 7
    //   218: aload 4
    //   220: aload 7
    //   222: invokevirtual 261	java/util/concurrent/ThreadPoolExecutor:submit	(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    //   225: pop
    //   226: new 494	com/mopub/common/DiskLruCache$Snapshot
    //   229: astore 4
    //   231: aload_2
    //   232: getfield 130	com/mopub/common/DiskLruCache$a:e	J
    //   235: lstore 13
    //   237: aload_2
    //   238: getfield 231	com/mopub/common/DiskLruCache$a:b	[J
    //   241: astore 15
    //   243: aload 4
    //   245: astore 9
    //   247: aload_0
    //   248: astore 11
    //   250: aload_1
    //   251: astore 12
    //   253: aload 4
    //   255: aload_0
    //   256: aload_1
    //   257: lload 13
    //   259: aload 6
    //   261: aload 15
    //   263: iconst_0
    //   264: invokespecial 497	com/mopub/common/DiskLruCache$Snapshot:<init>	(Lcom/mopub/common/DiskLruCache;Ljava/lang/String;J[Ljava/io/InputStream;[JB)V
    //   267: aload_0
    //   268: monitorexit
    //   269: aload 4
    //   271: areturn
    //   272: pop
    //   273: aload_0
    //   274: getfield 112	com/mopub/common/DiskLruCache:i	I
    //   277: istore 16
    //   279: iload 5
    //   281: iload 16
    //   283: if_icmpge +32 -> 315
    //   286: aload 6
    //   288: iload 5
    //   290: aaload
    //   291: astore_1
    //   292: aload_1
    //   293: ifnull +22 -> 315
    //   296: aload 6
    //   298: iload 5
    //   300: aaload
    //   301: astore_1
    //   302: aload_1
    //   303: invokestatic 364	com/mopub/common/DiskLruCacheUtil:a	(Ljava/io/Closeable;)V
    //   306: iload 5
    //   308: iconst_1
    //   309: iadd
    //   310: istore 5
    //   312: goto -39 -> 273
    //   315: aload_0
    //   316: monitorexit
    //   317: aconst_null
    //   318: areturn
    //   319: astore_1
    //   320: aload_0
    //   321: monitorexit
    //   322: aload_1
    //   323: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	324	0	this	DiskLruCache
    //   0	324	1	paramString	String
    //   14	224	2	localObject1	Object
    //   27	110	3	i1	int
    //   201	2	3	bool1	boolean
    //   29	241	4	localObject2	Object
    //   43	3	5	bool2	boolean
    //   58	253	5	i2	int
    //   65	232	6	arrayOfInputStream	InputStream[]
    //   71	150	7	localObject3	Object
    //   74	103	8	i3	int
    //   77	169	9	localObject4	Object
    //   83	7	10	i4	int
    //   95	154	11	localObject5	Object
    //   103	149	12	localObject6	Object
    //   235	23	13	l1	long
    //   241	21	15	arrayOfLong	long[]
    //   277	7	16	i5	int
    //   272	1	18	localFileNotFoundException	java.io.FileNotFoundException
    // Exception table:
    //   from	to	target	type
    //   79	83	272	java/io/FileNotFoundException
    //   92	95	272	java/io/FileNotFoundException
    //   98	103	272	java/io/FileNotFoundException
    //   107	112	272	java/io/FileNotFoundException
    //   116	119	272	java/io/FileNotFoundException
    //   2	6	319	finally
    //   6	10	319	finally
    //   10	14	319	finally
    //   16	20	319	finally
    //   21	25	319	finally
    //   39	43	319	finally
    //   54	58	319	finally
    //   60	65	319	finally
    //   79	83	319	finally
    //   92	95	319	finally
    //   98	103	319	finally
    //   107	112	319	finally
    //   116	119	319	finally
    //   128	132	319	finally
    //   136	140	319	finally
    //   140	144	319	finally
    //   146	149	319	finally
    //   158	163	319	finally
    //   165	170	319	finally
    //   176	182	319	finally
    //   182	187	319	finally
    //   191	197	319	finally
    //   197	201	319	finally
    //   206	210	319	finally
    //   212	216	319	finally
    //   220	226	319	finally
    //   226	229	319	finally
    //   231	235	319	finally
    //   237	241	319	finally
    //   263	267	319	finally
    //   273	277	319	finally
    //   288	291	319	finally
    //   298	301	319	finally
    //   302	306	319	finally
  }
  
  public final File getDirectory()
  {
    return c;
  }
  
  public final long getMaxSize()
  {
    try
    {
      long l1 = h;
      return l1;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  /* Error */
  public final boolean isClosed()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 147	com/mopub/common/DiskLruCache:k	Ljava/io/Writer;
    //   6: astore_1
    //   7: aload_1
    //   8: ifnonnull +9 -> 17
    //   11: iconst_1
    //   12: istore_2
    //   13: aload_0
    //   14: monitorexit
    //   15: iload_2
    //   16: ireturn
    //   17: iconst_0
    //   18: istore_2
    //   19: aconst_null
    //   20: astore_1
    //   21: goto -8 -> 13
    //   24: astore_1
    //   25: aload_0
    //   26: monitorexit
    //   27: aload_1
    //   28: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	29	0	this	DiskLruCache
    //   6	15	1	localWriter	Writer
    //   24	4	1	localObject	Object
    //   12	7	2	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   2	6	24	finally
  }
  
  public final boolean remove(String paramString)
  {
    try
    {
      f();
      a(paramString);
      Object localObject1 = l;
      localObject1 = ((LinkedHashMap)localObject1).get(paramString);
      localObject1 = (DiskLruCache.a)localObject1;
      int i1 = 0;
      String str1 = null;
      if (localObject1 != null)
      {
        Object localObject2 = d;
        if (localObject2 == null)
        {
          for (;;)
          {
            int i2 = i;
            if (i1 >= i2) {
              break;
            }
            localObject2 = ((DiskLruCache.a)localObject1).getCleanFile(i1);
            boolean bool1 = ((File)localObject2).exists();
            if (bool1)
            {
              bool1 = ((File)localObject2).delete();
              if (!bool1)
              {
                paramString = new java/io/IOException;
                localObject1 = "failed to delete ";
                str1 = String.valueOf(localObject2);
                localObject1 = ((String)localObject1).concat(str1);
                paramString.<init>((String)localObject1);
                throw paramString;
              }
            }
            long l1 = j;
            long[] arrayOfLong = b;
            long l2 = arrayOfLong[i1];
            l1 -= l2;
            j = l1;
            localObject2 = b;
            long l3 = 0L;
            localObject2[i1] = l3;
            i1 += 1;
          }
          int i3 = m;
          i1 = 1;
          int i4;
          i3 += i1;
          m = i4;
          localObject1 = k;
          localObject2 = new java/lang/StringBuilder;
          String str2 = "REMOVE ";
          ((StringBuilder)localObject2).<init>(str2);
          ((StringBuilder)localObject2).append(paramString);
          char c1 = '\n';
          ((StringBuilder)localObject2).append(c1);
          localObject2 = ((StringBuilder)localObject2).toString();
          ((Writer)localObject1).append((CharSequence)localObject2);
          localObject1 = l;
          ((LinkedHashMap)localObject1).remove(paramString);
          boolean bool2 = e();
          if (bool2)
          {
            paramString = b;
            localObject1 = o;
            paramString.submit((Callable)localObject1);
          }
          return i1;
        }
      }
      return false;
    }
    finally {}
  }
  
  public final void setMaxSize(long paramLong)
  {
    try
    {
      h = paramLong;
      ThreadPoolExecutor localThreadPoolExecutor = b;
      Callable localCallable = o;
      localThreadPoolExecutor.submit(localCallable);
      return;
    }
    finally {}
  }
  
  public final long size()
  {
    try
    {
      long l1 = j;
      return l1;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.DiskLruCache
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */