package com.mopub.common;

import java.util.EnumSet;

public class UrlHandler$Builder
{
  private EnumSet a;
  private UrlHandler.ResultActions b;
  private UrlHandler.MoPubSchemeListener c;
  private boolean d;
  private String e;
  
  public UrlHandler$Builder()
  {
    Object localObject = EnumSet.of(UrlAction.NOOP);
    a = ((EnumSet)localObject);
    localObject = UrlHandler.a();
    b = ((UrlHandler.ResultActions)localObject);
    localObject = UrlHandler.b();
    c = ((UrlHandler.MoPubSchemeListener)localObject);
    d = false;
  }
  
  public UrlHandler build()
  {
    UrlHandler localUrlHandler = new com/mopub/common/UrlHandler;
    EnumSet localEnumSet = a;
    UrlHandler.ResultActions localResultActions = b;
    UrlHandler.MoPubSchemeListener localMoPubSchemeListener = c;
    boolean bool = d;
    String str = e;
    localUrlHandler.<init>(localEnumSet, localResultActions, localMoPubSchemeListener, bool, str, (byte)0);
    return localUrlHandler;
  }
  
  public Builder withDspCreativeId(String paramString)
  {
    e = paramString;
    return this;
  }
  
  public Builder withMoPubSchemeListener(UrlHandler.MoPubSchemeListener paramMoPubSchemeListener)
  {
    c = paramMoPubSchemeListener;
    return this;
  }
  
  public Builder withResultActions(UrlHandler.ResultActions paramResultActions)
  {
    b = paramResultActions;
    return this;
  }
  
  public Builder withSupportedUrlActions(UrlAction paramUrlAction, UrlAction... paramVarArgs)
  {
    paramUrlAction = EnumSet.of(paramUrlAction, paramVarArgs);
    a = paramUrlAction;
    return this;
  }
  
  public Builder withSupportedUrlActions(EnumSet paramEnumSet)
  {
    paramEnumSet = EnumSet.copyOf(paramEnumSet);
    a = paramEnumSet;
    return this;
  }
  
  public Builder withoutMoPubBrowser()
  {
    d = true;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.UrlHandler.Builder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */