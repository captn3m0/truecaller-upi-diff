package com.mopub.common;

public abstract interface UrlHandler$ResultActions
{
  public abstract void urlHandlingFailed(String paramString, UrlAction paramUrlAction);
  
  public abstract void urlHandlingSucceeded(String paramString, UrlAction paramUrlAction);
}

/* Location:
 * Qualified Name:     com.mopub.common.UrlHandler.ResultActions
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */