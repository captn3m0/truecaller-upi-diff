package com.mopub.common;

import java.util.List;

public abstract interface VisibilityTracker$VisibilityTrackerListener
{
  public abstract void onVisibilityChanged(List paramList1, List paramList2);
}

/* Location:
 * Qualified Name:     com.mopub.common.VisibilityTracker.VisibilityTrackerListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */