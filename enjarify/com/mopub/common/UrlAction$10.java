package com.mopub.common;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.mopub.common.util.Intents;
import com.mopub.exceptions.IntentNotResolvableException;
import java.net.URISyntaxException;

 enum UrlAction$10
{
  UrlAction$10()
  {
    super(paramString, 8, true, (byte)0);
  }
  
  protected final void a(Context paramContext, Uri paramUri, UrlHandler paramUrlHandler, String paramString)
  {
    paramUrlHandler = "intent";
    paramString = paramUri.getScheme();
    boolean bool = paramUrlHandler.equalsIgnoreCase(paramString);
    if (bool) {
      try
      {
        paramUrlHandler = paramUri.toString();
        int i = 1;
        paramUrlHandler = Intent.parseUri(paramUrlHandler, i);
        Intents.launchApplicationIntent(paramContext, paramUrlHandler);
        return;
      }
      catch (URISyntaxException localURISyntaxException)
      {
        paramContext = new com/mopub/exceptions/IntentNotResolvableException;
        paramUrlHandler = new java/lang/StringBuilder;
        paramUrlHandler.<init>("Intent uri had invalid syntax: ");
        paramUri = paramUri.toString();
        paramUrlHandler.append(paramUri);
        paramUri = paramUrlHandler.toString();
        paramContext.<init>(paramUri);
        throw paramContext;
      }
    }
    Intents.launchApplicationUrl(paramContext, paramUri);
  }
  
  public final boolean shouldTryHandlingUrl(Uri paramUri)
  {
    paramUri = paramUri.getScheme();
    boolean bool = TextUtils.isEmpty(paramUri);
    return !bool;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.UrlAction.10
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */