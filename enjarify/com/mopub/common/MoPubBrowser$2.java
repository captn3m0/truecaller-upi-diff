package com.mopub.common;

import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;

final class MoPubBrowser$2
  implements View.OnClickListener
{
  MoPubBrowser$2(MoPubBrowser paramMoPubBrowser) {}
  
  public final void onClick(View paramView)
  {
    paramView = MoPubBrowser.a(a);
    boolean bool = paramView.canGoForward();
    if (bool)
    {
      paramView = MoPubBrowser.a(a);
      paramView.goForward();
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.MoPubBrowser.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */