package com.mopub.common;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;

public final class DiskLruCache$Editor
{
  private final DiskLruCache.a b;
  private final boolean[] c;
  private boolean d;
  private boolean e;
  
  private DiskLruCache$Editor(DiskLruCache paramDiskLruCache, DiskLruCache.a parama)
  {
    b = parama;
    boolean bool = c;
    int i;
    if (bool)
    {
      i = 0;
      paramDiskLruCache = null;
    }
    else
    {
      i = DiskLruCache.f(paramDiskLruCache);
      paramDiskLruCache = new boolean[i];
    }
    c = paramDiskLruCache;
  }
  
  public final void abort()
  {
    DiskLruCache.a(a, this, false);
  }
  
  public final void abortUnlessCommitted()
  {
    boolean bool = e;
    if (!bool) {
      try
      {
        abort();
        return;
      }
      catch (IOException localIOException) {}
    }
  }
  
  public final void commit()
  {
    boolean bool1 = d;
    boolean bool2 = true;
    DiskLruCache localDiskLruCache;
    if (bool1)
    {
      DiskLruCache.a(a, this, false);
      localDiskLruCache = a;
      String str = b.a;
      localDiskLruCache.remove(str);
    }
    else
    {
      localDiskLruCache = a;
      DiskLruCache.a(localDiskLruCache, this, bool2);
    }
    e = bool2;
  }
  
  public final String getString(int paramInt)
  {
    InputStream localInputStream = newInputStream(paramInt);
    if (localInputStream != null) {
      return DiskLruCache.a(localInputStream);
    }
    return null;
  }
  
  public final InputStream newInputStream(int paramInt)
  {
    synchronized (a)
    {
      Object localObject1 = b;
      localObject1 = d;
      if (localObject1 == this)
      {
        localObject1 = b;
        boolean bool = c;
        if (!bool) {
          return null;
        }
        try
        {
          localObject1 = new java/io/FileInputStream;
          DiskLruCache.a locala = b;
          localObject2 = locala.getCleanFile(paramInt);
          ((FileInputStream)localObject1).<init>((File)localObject2);
          return (InputStream)localObject1;
        }
        catch (FileNotFoundException localFileNotFoundException)
        {
          return null;
        }
      }
      Object localObject2 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject2).<init>();
      throw ((Throwable)localObject2);
    }
  }
  
  public final OutputStream newOutputStream(int paramInt)
  {
    synchronized (a)
    {
      Object localObject1 = b;
      localObject1 = d;
      if (localObject1 == this)
      {
        localObject1 = b;
        boolean bool = c;
        int i;
        if (!bool)
        {
          localObject1 = c;
          i = 1;
          localObject1[paramInt] = i;
        }
        localObject1 = b;
        localObject2 = ((DiskLruCache.a)localObject1).getDirtyFile(paramInt);
        try
        {
          localObject1 = new java/io/FileOutputStream;
          ((FileOutputStream)localObject1).<init>((File)localObject2);
        }
        catch (FileNotFoundException localFileNotFoundException1)
        {
          localObject1 = a;
          localObject1 = DiskLruCache.g((DiskLruCache)localObject1);
          ((File)localObject1).mkdirs();
        }
        try
        {
          localObject1 = new java/io/FileOutputStream;
          ((FileOutputStream)localObject1).<init>((File)localObject2);
          localObject2 = new com/mopub/common/DiskLruCache$Editor$a;
          i = 0;
          ((DiskLruCache.Editor.a)localObject2).<init>(this, (OutputStream)localObject1, (byte)0);
          return (OutputStream)localObject2;
        }
        catch (FileNotFoundException localFileNotFoundException2)
        {
          localObject2 = DiskLruCache.a();
          return (OutputStream)localObject2;
        }
      }
      Object localObject2 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject2).<init>();
      throw ((Throwable)localObject2);
    }
  }
  
  public final void set(int paramInt, String paramString)
  {
    Object localObject1 = null;
    try
    {
      OutputStreamWriter localOutputStreamWriter = new java/io/OutputStreamWriter;
      OutputStream localOutputStream = newOutputStream(paramInt);
      Charset localCharset = DiskLruCacheUtil.b;
      localOutputStreamWriter.<init>(localOutputStream, localCharset);
      try
      {
        localOutputStreamWriter.write(paramString);
        DiskLruCacheUtil.a(localOutputStreamWriter);
        return;
      }
      finally
      {
        localObject1 = localOutputStreamWriter;
      }
      DiskLruCacheUtil.a((Closeable)localObject1);
    }
    finally {}
    throw ((Throwable)localObject3);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.DiskLruCache.Editor
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */