package com.mopub.common;

import android.os.Looper;
import com.mopub.common.logging.MoPubLog;
import java.util.IllegalFormatException;

public final class Preconditions
{
  public static final String EMPTY_ARGUMENTS = "";
  
  private static String a(String paramString, Object... paramVarArgs)
  {
    paramString = String.valueOf(paramString);
    try
    {
      return String.format(paramString, paramVarArgs);
    }
    catch (IllegalFormatException paramVarArgs)
    {
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>("MoPub preconditions had a format exception: ");
      paramVarArgs = paramVarArgs.getMessage();
      localStringBuilder.append(paramVarArgs);
      MoPubLog.e(localStringBuilder.toString());
    }
    return paramString;
  }
  
  private static boolean b(Object paramObject, boolean paramBoolean, String paramString, Object... paramVarArgs)
  {
    if (paramObject != null) {
      return true;
    }
    paramObject = a(paramString, paramVarArgs);
    if (!paramBoolean)
    {
      MoPubLog.e((String)paramObject);
      return false;
    }
    NullPointerException localNullPointerException = new java/lang/NullPointerException;
    localNullPointerException.<init>((String)paramObject);
    throw localNullPointerException;
  }
  
  private static boolean b(boolean paramBoolean, String paramString, Object... paramVarArgs)
  {
    Looper localLooper1 = Looper.getMainLooper();
    Looper localLooper2 = Looper.myLooper();
    boolean bool = localLooper1.equals(localLooper2);
    if (bool) {
      return true;
    }
    paramString = a(paramString, paramVarArgs);
    if (!paramBoolean)
    {
      MoPubLog.e(paramString);
      return false;
    }
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>(paramString);
    throw localIllegalStateException;
  }
  
  private static boolean c(boolean paramBoolean1, boolean paramBoolean2, String paramString, Object... paramVarArgs)
  {
    if (paramBoolean1) {
      return true;
    }
    String str = a(paramString, paramVarArgs);
    if (!paramBoolean2)
    {
      MoPubLog.e(str);
      return false;
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    localIllegalArgumentException.<init>(str);
    throw localIllegalArgumentException;
  }
  
  public static void checkArgument(boolean paramBoolean)
  {
    boolean bool = true;
    Object[] arrayOfObject = new Object[bool];
    arrayOfObject[0] = "";
    c(paramBoolean, bool, "Illegal argument.", arrayOfObject);
  }
  
  public static void checkArgument(boolean paramBoolean, String paramString)
  {
    boolean bool = true;
    Object[] arrayOfObject = new Object[bool];
    arrayOfObject[0] = "";
    c(paramBoolean, bool, paramString, arrayOfObject);
  }
  
  public static void checkArgument(boolean paramBoolean, String paramString, Object... paramVarArgs)
  {
    c(paramBoolean, true, paramString, paramVarArgs);
  }
  
  public static void checkNotNull(Object paramObject)
  {
    boolean bool = true;
    Object[] arrayOfObject = new Object[bool];
    arrayOfObject[0] = "";
    b(paramObject, bool, "Object can not be null.", arrayOfObject);
  }
  
  public static void checkNotNull(Object paramObject, String paramString)
  {
    boolean bool = true;
    Object[] arrayOfObject = new Object[bool];
    arrayOfObject[0] = "";
    b(paramObject, bool, paramString, arrayOfObject);
  }
  
  public static void checkNotNull(Object paramObject, String paramString, Object... paramVarArgs)
  {
    b(paramObject, true, paramString, paramVarArgs);
  }
  
  public static void checkState(boolean paramBoolean)
  {
    boolean bool = true;
    Object[] arrayOfObject = new Object[bool];
    arrayOfObject[0] = "";
    d(paramBoolean, bool, "Illegal state.", arrayOfObject);
  }
  
  public static void checkState(boolean paramBoolean, String paramString)
  {
    boolean bool = true;
    Object[] arrayOfObject = new Object[bool];
    arrayOfObject[0] = "";
    d(paramBoolean, bool, paramString, arrayOfObject);
  }
  
  public static void checkState(boolean paramBoolean, String paramString, Object... paramVarArgs)
  {
    d(paramBoolean, true, paramString, paramVarArgs);
  }
  
  public static void checkUiThread()
  {
    boolean bool = true;
    Object[] arrayOfObject = new Object[bool];
    arrayOfObject[0] = "";
    b(bool, "This method must be called from the UI thread.", arrayOfObject);
  }
  
  public static void checkUiThread(String paramString)
  {
    boolean bool = true;
    Object[] arrayOfObject = new Object[bool];
    arrayOfObject[0] = "";
    b(bool, paramString, arrayOfObject);
  }
  
  public static void checkUiThread(String paramString, Object... paramVarArgs)
  {
    b(true, paramString, paramVarArgs);
  }
  
  private static boolean d(boolean paramBoolean1, boolean paramBoolean2, String paramString, Object... paramVarArgs)
  {
    if (paramBoolean1) {
      return true;
    }
    String str = a(paramString, paramVarArgs);
    if (!paramBoolean2)
    {
      MoPubLog.e(str);
      return false;
    }
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>(str);
    throw localIllegalStateException;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.Preconditions
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */