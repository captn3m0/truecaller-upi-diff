package com.mopub.common;

public enum AdFormat
{
  static
  {
    Object localObject = new com/mopub/common/AdFormat;
    ((AdFormat)localObject).<init>("BANNER", 0);
    BANNER = (AdFormat)localObject;
    localObject = new com/mopub/common/AdFormat;
    int i = 1;
    ((AdFormat)localObject).<init>("INTERSTITIAL", i);
    INTERSTITIAL = (AdFormat)localObject;
    localObject = new com/mopub/common/AdFormat;
    int j = 2;
    ((AdFormat)localObject).<init>("NATIVE", j);
    NATIVE = (AdFormat)localObject;
    localObject = new com/mopub/common/AdFormat;
    int k = 3;
    ((AdFormat)localObject).<init>("REWARDED_VIDEO", k);
    REWARDED_VIDEO = (AdFormat)localObject;
    localObject = new AdFormat[4];
    AdFormat localAdFormat = BANNER;
    localObject[0] = localAdFormat;
    localAdFormat = INTERSTITIAL;
    localObject[i] = localAdFormat;
    localAdFormat = NATIVE;
    localObject[j] = localAdFormat;
    localAdFormat = REWARDED_VIDEO;
    localObject[k] = localAdFormat;
    a = (AdFormat[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.AdFormat
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */