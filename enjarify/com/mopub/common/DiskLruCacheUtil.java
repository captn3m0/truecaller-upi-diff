package com.mopub.common;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;
import java.nio.charset.Charset;

public final class DiskLruCacheUtil
{
  static final Charset a = Charset.forName("US-ASCII");
  static final Charset b = Charset.forName("UTF-8");
  
  static String a(Reader paramReader)
  {
    try
    {
      Object localObject1 = new java/io/StringWriter;
      ((StringWriter)localObject1).<init>();
      int i = 1024;
      char[] arrayOfChar = new char[i];
      for (;;)
      {
        int j = paramReader.read(arrayOfChar);
        int k = -1;
        if (j == k) {
          break;
        }
        k = 0;
        ((StringWriter)localObject1).write(arrayOfChar, 0, j);
      }
      localObject1 = ((StringWriter)localObject1).toString();
      return (String)localObject1;
    }
    finally
    {
      paramReader.close();
    }
  }
  
  static void a(Closeable paramCloseable)
  {
    if (paramCloseable != null) {
      try
      {
        paramCloseable.close();
        return;
      }
      catch (RuntimeException localRuntimeException)
      {
        throw localRuntimeException;
      }
      catch (Exception localException) {}
    }
  }
  
  static void a(File paramFile)
  {
    Object localObject = paramFile.listFiles();
    if (localObject != null)
    {
      int i = localObject.length;
      int j = 0;
      while (j < i)
      {
        File localFile = localObject[j];
        boolean bool = localFile.isDirectory();
        if (bool) {
          a(localFile);
        }
        bool = localFile.delete();
        if (bool)
        {
          j += 1;
        }
        else
        {
          paramFile = new java/io/IOException;
          localObject = String.valueOf(localFile);
          localObject = "failed to delete file: ".concat((String)localObject);
          paramFile.<init>((String)localObject);
          throw paramFile;
        }
      }
      return;
    }
    localObject = new java/io/IOException;
    paramFile = String.valueOf(paramFile);
    paramFile = "not a readable directory: ".concat(paramFile);
    ((IOException)localObject).<init>(paramFile);
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.DiskLruCacheUtil
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */