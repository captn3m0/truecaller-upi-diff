package com.mopub.common;

abstract interface UrlResolutionTask$a
{
  public abstract void onFailure(String paramString, Throwable paramThrowable);
  
  public abstract void onSuccess(String paramString);
}

/* Location:
 * Qualified Name:     com.mopub.common.UrlResolutionTask.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */