package com.mopub.common;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SdkConfiguration
{
  private final String a;
  private final List b;
  private final MediationSettings[] c;
  private final List d;
  
  private SdkConfiguration(String paramString, List paramList1, MediationSettings[] paramArrayOfMediationSettings, List paramList2)
  {
    Preconditions.checkNotNull(paramString);
    Preconditions.checkNotNull(paramList1);
    a = paramString;
    b = paramList1;
    c = paramArrayOfMediationSettings;
    d = paramList2;
  }
  
  public String getAdUnitId()
  {
    return a;
  }
  
  public List getAdvancedBidders()
  {
    return Collections.unmodifiableList(b);
  }
  
  public MediationSettings[] getMediationSettings()
  {
    MediationSettings[] arrayOfMediationSettings = c;
    int i = arrayOfMediationSettings.length;
    return (MediationSettings[])Arrays.copyOf(arrayOfMediationSettings, i);
  }
  
  public List getNetworksToInit()
  {
    List localList = d;
    if (localList == null) {
      return null;
    }
    return Collections.unmodifiableList(localList);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.SdkConfiguration
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */