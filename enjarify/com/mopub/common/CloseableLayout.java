package com.mopub.common;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.widget.FrameLayout;
import com.mopub.common.util.Dips;
import com.mopub.common.util.Drawables;

public class CloseableLayout
  extends FrameLayout
{
  private final int a;
  private CloseableLayout.OnCloseListener b;
  private final StateListDrawable c;
  private CloseableLayout.ClosePosition d;
  private final int e;
  private final int f;
  private final int g;
  private boolean h;
  private final Rect i;
  private final Rect j;
  private final Rect k;
  private final Rect l;
  private boolean m;
  private CloseableLayout.a n;
  
  public CloseableLayout(Context paramContext)
  {
    this(paramContext, null, 0);
  }
  
  public CloseableLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public CloseableLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    paramAttributeSet = new android/graphics/Rect;
    paramAttributeSet.<init>();
    i = paramAttributeSet;
    paramAttributeSet = new android/graphics/Rect;
    paramAttributeSet.<init>();
    j = paramAttributeSet;
    paramAttributeSet = new android/graphics/Rect;
    paramAttributeSet.<init>();
    k = paramAttributeSet;
    paramAttributeSet = new android/graphics/Rect;
    paramAttributeSet.<init>();
    l = paramAttributeSet;
    paramAttributeSet = new android/graphics/drawable/StateListDrawable;
    paramAttributeSet.<init>();
    c = paramAttributeSet;
    paramAttributeSet = CloseableLayout.ClosePosition.TOP_RIGHT;
    d = paramAttributeSet;
    paramAttributeSet = c;
    int[] arrayOfInt = SELECTED_STATE_SET;
    Drawable localDrawable = Drawables.INTERSTITIAL_CLOSE_BUTTON_PRESSED.createDrawable(paramContext);
    paramAttributeSet.addState(arrayOfInt, localDrawable);
    paramAttributeSet = c;
    arrayOfInt = EMPTY_STATE_SET;
    localDrawable = Drawables.INTERSTITIAL_CLOSE_BUTTON_NORMAL.createDrawable(paramContext);
    paramAttributeSet.addState(arrayOfInt, localDrawable);
    paramAttributeSet = c;
    arrayOfInt = EMPTY_STATE_SET;
    paramAttributeSet.setState(arrayOfInt);
    c.setCallback(this);
    int i1 = ViewConfiguration.get(paramContext).getScaledTouchSlop();
    a = i1;
    i1 = Dips.asIntPixels(50.0F, paramContext);
    e = i1;
    i1 = Dips.asIntPixels(30.0F, paramContext);
    f = i1;
    int i2 = Dips.asIntPixels(8.0F, paramContext);
    g = i2;
    setWillNotDraw(false);
    m = true;
  }
  
  private static void a(CloseableLayout.ClosePosition paramClosePosition, int paramInt, Rect paramRect1, Rect paramRect2)
  {
    Gravity.apply(a, paramInt, paramInt, paramRect1, paramRect2);
  }
  
  private boolean a()
  {
    int[] arrayOfInt1 = c.getState();
    int[] arrayOfInt2 = SELECTED_STATE_SET;
    return arrayOfInt1 == arrayOfInt2;
  }
  
  private boolean a(int paramInt1, int paramInt2, int paramInt3)
  {
    Rect localRect1 = j;
    int i1 = left - paramInt3;
    if (paramInt1 >= i1)
    {
      localRect1 = j;
      i1 = top - paramInt3;
      if (paramInt2 >= i1)
      {
        localRect1 = j;
        i1 = right + paramInt3;
        if (paramInt1 < i1)
        {
          Rect localRect2 = j;
          paramInt1 = bottom + paramInt3;
          if (paramInt2 < paramInt1) {
            return true;
          }
        }
      }
    }
    return false;
  }
  
  private void setClosePressed(boolean paramBoolean)
  {
    boolean bool = a();
    if (paramBoolean == bool) {
      return;
    }
    StateListDrawable localStateListDrawable = c;
    if (paramBoolean) {
      localObject = SELECTED_STATE_SET;
    } else {
      localObject = EMPTY_STATE_SET;
    }
    localStateListDrawable.setState((int[])localObject);
    Object localObject = j;
    invalidate((Rect)localObject);
  }
  
  public void applyCloseRegionBounds(CloseableLayout.ClosePosition paramClosePosition, Rect paramRect1, Rect paramRect2)
  {
    int i1 = e;
    a(paramClosePosition, i1, paramRect1, paramRect2);
  }
  
  public void draw(Canvas paramCanvas)
  {
    super.draw(paramCanvas);
    boolean bool = h;
    if (bool)
    {
      bool = false;
      h = false;
      Rect localRect1 = i;
      int i1 = getWidth();
      int i2 = getHeight();
      localRect1.set(0, 0, i1, i2);
      localObject = d;
      localRect1 = i;
      Rect localRect2 = j;
      applyCloseRegionBounds((CloseableLayout.ClosePosition)localObject, localRect1, localRect2);
      localObject = l;
      localRect1 = j;
      ((Rect)localObject).set(localRect1);
      localObject = l;
      int i3 = g;
      ((Rect)localObject).inset(i3, i3);
      localObject = d;
      localRect1 = l;
      localRect2 = k;
      i2 = f;
      a((CloseableLayout.ClosePosition)localObject, i2, localRect1, localRect2);
      localObject = c;
      localRect1 = k;
      ((StateListDrawable)localObject).setBounds(localRect1);
    }
    Object localObject = c;
    bool = ((StateListDrawable)localObject).isVisible();
    if (bool)
    {
      localObject = c;
      ((StateListDrawable)localObject).draw(paramCanvas);
    }
  }
  
  Rect getCloseBounds()
  {
    return j;
  }
  
  public boolean isCloseVisible()
  {
    return c.isVisible();
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    int i1 = paramMotionEvent.getAction();
    if (i1 != 0) {
      return false;
    }
    i1 = (int)paramMotionEvent.getX();
    int i2 = (int)paramMotionEvent.getY();
    return a(i1, i2, 0);
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    h = true;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    float f1 = paramMotionEvent.getX();
    int i1 = (int)f1;
    int i3 = (int)paramMotionEvent.getY();
    int i4 = a;
    boolean bool1 = a(i1, i3, i4);
    i3 = 0;
    if (bool1)
    {
      bool1 = m;
      i4 = 1;
      if (!bool1)
      {
        StateListDrawable localStateListDrawable = c;
        bool1 = localStateListDrawable.isVisible();
        if (!bool1)
        {
          bool1 = false;
          f1 = 0.0F;
          localStateListDrawable = null;
          break label84;
        }
      }
      bool1 = true;
      f1 = Float.MIN_VALUE;
      label84:
      if (bool1)
      {
        int i5 = paramMotionEvent.getAction();
        int i2 = 3;
        f1 = 4.2E-45F;
        if (i5 != i2) {
          switch (i5)
          {
          default: 
            break;
          case 1: 
            boolean bool2 = a();
            if (!bool2) {
              break;
            }
            paramMotionEvent = n;
            if (paramMotionEvent == null)
            {
              paramMotionEvent = new com/mopub/common/CloseableLayout$a;
              paramMotionEvent.<init>(this, (byte)0);
              n = paramMotionEvent;
            }
            paramMotionEvent = n;
            i2 = ViewConfiguration.getPressedStateDuration();
            long l1 = i2;
            postDelayed(paramMotionEvent, l1);
            playSoundEffect(0);
            paramMotionEvent = b;
            if (paramMotionEvent == null) {
              break;
            }
            paramMotionEvent.onClose();
            break;
          case 0: 
            setClosePressed(i4);
            break;
          }
        } else {
          setClosePressed(false);
        }
        return i4;
      }
    }
    setClosePressed(false);
    super.onTouchEvent(paramMotionEvent);
    return false;
  }
  
  public void setCloseAlwaysInteractable(boolean paramBoolean)
  {
    m = paramBoolean;
  }
  
  void setCloseBoundChanged(boolean paramBoolean)
  {
    h = paramBoolean;
  }
  
  void setCloseBounds(Rect paramRect)
  {
    j.set(paramRect);
  }
  
  public void setClosePosition(CloseableLayout.ClosePosition paramClosePosition)
  {
    Preconditions.checkNotNull(paramClosePosition);
    d = paramClosePosition;
    h = true;
    invalidate();
  }
  
  public void setCloseVisible(boolean paramBoolean)
  {
    StateListDrawable localStateListDrawable = c;
    paramBoolean = localStateListDrawable.setVisible(paramBoolean, false);
    if (paramBoolean)
    {
      Rect localRect = j;
      invalidate(localRect);
    }
  }
  
  public void setOnCloseListener(CloseableLayout.OnCloseListener paramOnCloseListener)
  {
    b = paramOnCloseListener;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.CloseableLayout
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */