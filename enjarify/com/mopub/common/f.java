package com.mopub.common;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Reflection;
import com.mopub.common.util.Reflection.MethodBuilder;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class f
  implements ExternalViewabilitySession
{
  private static Boolean a;
  private static boolean b = false;
  private static boolean c = false;
  private static final Map d;
  private Object e;
  private Object f;
  private Map g;
  private boolean h;
  
  static
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    d = localHashMap;
    localHashMap.put("moatClientLevel1", "level1");
    d.put("moatClientLevel2", "level2");
    d.put("moatClientLevel3", "level3");
    d.put("moatClientLevel4", "level4");
    d.put("moatClientSlicer1", "slicer1");
    d.put("moatClientSlicer2", "slicer2");
  }
  
  f()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    g = localHashMap;
  }
  
  static boolean a()
  {
    boolean bool1 = b;
    if (!bool1)
    {
      Object localObject1 = a;
      if (localObject1 == null)
      {
        bool1 = Reflection.classFound("com.moat.analytics.mobile.mpub.MoatFactory");
        a = Boolean.valueOf(bool1);
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>("Moat is ");
        Object localObject2 = a;
        boolean bool2 = ((Boolean)localObject2).booleanValue();
        if (bool2) {
          localObject2 = "";
        } else {
          localObject2 = "un";
        }
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject2 = "available via reflection.";
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject1 = ((StringBuilder)localObject1).toString();
        MoPubLog.d((String)localObject1);
      }
      localObject1 = a;
      bool1 = ((Boolean)localObject1).booleanValue();
      if (bool1) {
        return true;
      }
    }
    return false;
  }
  
  static void b()
  {
    b = true;
  }
  
  public final Boolean createDisplaySession(Context paramContext, WebView paramWebView, boolean paramBoolean)
  {
    Preconditions.checkNotNull(paramContext);
    boolean bool = a();
    Object localObject = null;
    if (!bool) {
      return null;
    }
    try
    {
      paramContext = new com/mopub/common/util/Reflection$MethodBuilder;
      String str1 = "create";
      paramContext.<init>(null, str1);
      localObject = "com.moat.analytics.mobile.mpub.MoatFactory";
      paramContext = paramContext.setStatic((String)localObject);
      paramContext = paramContext.execute();
      localObject = new com/mopub/common/util/Reflection$MethodBuilder;
      str1 = "createWebAdTracker";
      ((Reflection.MethodBuilder)localObject).<init>(paramContext, str1);
      paramContext = WebView.class;
      paramContext = ((Reflection.MethodBuilder)localObject).addParam(paramContext, paramWebView);
      paramContext = paramContext.execute();
      e = paramContext;
      if (!paramBoolean)
      {
        paramContext = new com/mopub/common/util/Reflection$MethodBuilder;
        paramWebView = e;
        String str2 = "startTracking";
        paramContext.<init>(paramWebView, str2);
        paramContext.execute();
      }
      return Boolean.TRUE;
    }
    catch (Exception paramContext)
    {
      paramWebView = new java/lang/StringBuilder;
      paramWebView.<init>("Unable to execute Moat start display session: ");
      paramContext = paramContext.getMessage();
      paramWebView.append(paramContext);
      MoPubLog.d(paramWebView.toString());
    }
    return Boolean.FALSE;
  }
  
  public final Boolean createVideoSession(Activity paramActivity, View paramView, Set paramSet, Map paramMap)
  {
    Preconditions.checkNotNull(paramActivity);
    Preconditions.checkNotNull(paramView);
    Preconditions.checkNotNull(paramSet);
    Preconditions.checkNotNull(paramMap);
    boolean bool1 = a();
    paramView = null;
    if (!bool1) {
      return null;
    }
    paramActivity = (String)paramMap.get("moat");
    g.clear();
    g.put("partnerCode", "mopubinapphtmvideo468906546585");
    paramMap = g;
    Object localObject1 = ";";
    paramSet = TextUtils.join((CharSequence)localObject1, paramSet);
    paramMap.put("zMoatVASTIDs", paramSet);
    boolean bool2 = TextUtils.isEmpty(paramActivity);
    int j = 1;
    Object localObject2;
    Object localObject3;
    if (!bool2)
    {
      paramActivity = Uri.parse(paramActivity);
      paramSet = paramActivity.getPathSegments();
      int k = paramSet.size();
      int m;
      if (k > 0)
      {
        localObject1 = (CharSequence)paramSet.get(0);
        m = TextUtils.isEmpty((CharSequence)localObject1);
        if (m == 0)
        {
          localObject1 = g;
          localObject2 = "partnerCode";
          paramSet = paramSet.get(0);
          ((Map)localObject1).put(localObject2, paramSet);
        }
      }
      paramActivity = paramActivity.getFragment();
      bool2 = TextUtils.isEmpty(paramActivity);
      if (!bool2)
      {
        paramSet = "&";
        paramActivity = paramActivity.split(paramSet);
        int i = paramActivity.length;
        m = 0;
        localObject1 = null;
        while (m < i)
        {
          localObject2 = paramActivity[m];
          localObject3 = "=";
          localObject2 = ((String)localObject2).split((String)localObject3);
          int i1 = localObject2.length;
          int i2 = 2;
          if (i1 >= i2)
          {
            localObject3 = localObject2[0];
            localObject2 = localObject2[j];
            boolean bool4 = TextUtils.isEmpty((CharSequence)localObject3);
            if (!bool4)
            {
              bool4 = TextUtils.isEmpty((CharSequence)localObject2);
              if (!bool4)
              {
                Map localMap1 = d;
                bool4 = localMap1.containsKey(localObject3);
                if (bool4)
                {
                  localMap1 = g;
                  Map localMap2 = d;
                  localObject3 = localMap2.get(localObject3);
                  localMap1.put(localObject3, localObject2);
                }
              }
            }
          }
          int n;
          m += 1;
        }
      }
    }
    paramActivity = g;
    paramSet = "partnerCode";
    paramActivity = (String)paramActivity.get(paramSet);
    boolean bool3 = TextUtils.isEmpty(paramActivity);
    if (bool3)
    {
      MoPubLog.d("partnerCode was empty when starting Moat video session");
      return Boolean.FALSE;
    }
    paramSet = "com.moat.analytics.mobile.mpub.ReactiveVideoTrackerPlugin";
    localObject1 = Object.class;
    try
    {
      localObject2 = new Class[j];
      localObject3 = String.class;
      localObject2[0] = localObject3;
      paramMap = new Object[j];
      paramMap[0] = paramActivity;
      paramActivity = Reflection.instantiateClassWithConstructor(paramSet, (Class)localObject1, (Class[])localObject2, paramMap);
      paramSet = new com/mopub/common/util/Reflection$MethodBuilder;
      paramMap = "create";
      paramSet.<init>(null, paramMap);
      paramView = "com.moat.analytics.mobile.mpub.MoatFactory";
      paramView = paramSet.setStatic(paramView);
      paramView = paramView.execute();
      paramSet = new com/mopub/common/util/Reflection$MethodBuilder;
      paramMap = "createCustomTracker";
      paramSet.<init>(paramView, paramMap);
      paramView = "com.moat.analytics.mobile.mpub.MoatPlugin";
      paramActivity = paramSet.addParam(paramView, paramActivity);
      paramActivity = paramActivity.execute();
      f = paramActivity;
      return Boolean.TRUE;
    }
    catch (Exception paramActivity)
    {
      paramView = new java/lang/StringBuilder;
      paramView.<init>("Unable to execute Moat start video session: ");
      paramActivity = paramActivity.getMessage();
      paramView.append(paramActivity);
      MoPubLog.d(paramView.toString());
    }
    return Boolean.FALSE;
  }
  
  public final Boolean endDisplaySession()
  {
    boolean bool = a();
    if (!bool) {
      return null;
    }
    Object localObject1 = e;
    if (localObject1 == null)
    {
      MoPubLog.d("Moat WebAdTracker unexpectedly null.");
      return Boolean.FALSE;
    }
    try
    {
      localObject2 = new com/mopub/common/util/Reflection$MethodBuilder;
      String str2 = "stopTracking";
      ((Reflection.MethodBuilder)localObject2).<init>(localObject1, str2);
      ((Reflection.MethodBuilder)localObject2).execute();
      return Boolean.TRUE;
    }
    catch (Exception localException)
    {
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Unable to execute Moat end session: ");
      String str1 = localException.getMessage();
      ((StringBuilder)localObject2).append(str1);
      MoPubLog.d(((StringBuilder)localObject2).toString());
    }
    return Boolean.FALSE;
  }
  
  public final Boolean endVideoSession()
  {
    boolean bool = a();
    if (!bool) {
      return null;
    }
    Object localObject1 = f;
    if (localObject1 == null)
    {
      MoPubLog.d("Moat VideoAdTracker unexpectedly null.");
      return Boolean.FALSE;
    }
    try
    {
      localObject2 = new com/mopub/common/util/Reflection$MethodBuilder;
      String str2 = "stopTracking";
      ((Reflection.MethodBuilder)localObject2).<init>(localObject1, str2);
      ((Reflection.MethodBuilder)localObject2).execute();
      return Boolean.TRUE;
    }
    catch (Exception localException)
    {
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Unable to execute Moat end video session: ");
      String str1 = localException.getMessage();
      ((StringBuilder)localObject2).append(str1);
      MoPubLog.d(((StringBuilder)localObject2).toString());
    }
    return Boolean.FALSE;
  }
  
  public final String getName()
  {
    return "Moat";
  }
  
  public final Boolean initialize(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext);
    boolean bool1 = a();
    Object localObject1 = null;
    if (!bool1) {
      return null;
    }
    bool1 = c;
    if (bool1) {
      return Boolean.TRUE;
    }
    bool1 = paramContext instanceof Activity;
    if (bool1) {
      paramContext = ((Activity)paramContext).getApplication();
    }
    try
    {
      paramContext = paramContext.getApplicationContext();
      paramContext = (Application)paramContext;
      Object localObject2 = "com.moat.analytics.mobile.mpub.MoatOptions";
      Object localObject3 = Object.class;
      try
      {
        localObject2 = Reflection.instantiateClassWithEmptyConstructor((String)localObject2, (Class)localObject3);
        localObject3 = localObject2.getClass();
        String str1 = "disableAdIdCollection";
        localObject3 = ((Class)localObject3).getField(str1);
        boolean bool2 = true;
        ((Field)localObject3).setBoolean(localObject2, bool2);
        localObject3 = localObject2.getClass();
        String str2 = "disableLocationServices";
        localObject3 = ((Class)localObject3).getField(str2);
        ((Field)localObject3).setBoolean(localObject2, bool2);
        localObject3 = new com/mopub/common/util/Reflection$MethodBuilder;
        str2 = "getInstance";
        ((Reflection.MethodBuilder)localObject3).<init>(null, str2);
        localObject1 = "com.moat.analytics.mobile.mpub.MoatAnalytics";
        localObject1 = ((Reflection.MethodBuilder)localObject3).setStatic((String)localObject1);
        localObject1 = ((Reflection.MethodBuilder)localObject1).execute();
        localObject3 = new com/mopub/common/util/Reflection$MethodBuilder;
        str2 = "start";
        ((Reflection.MethodBuilder)localObject3).<init>(localObject1, str2);
        localObject1 = "com.moat.analytics.mobile.mpub.MoatOptions";
        localObject2 = ((Reflection.MethodBuilder)localObject3).addParam((String)localObject1, localObject2);
        localObject1 = Application.class;
        paramContext = ((Reflection.MethodBuilder)localObject2).addParam((Class)localObject1, paramContext);
        paramContext.execute();
        c = bool2;
        return Boolean.TRUE;
      }
      catch (Exception paramContext)
      {
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("Unable to initialize Moat: ");
        paramContext = paramContext.getMessage();
        ((StringBuilder)localObject2).append(paramContext);
        MoPubLog.d(((StringBuilder)localObject2).toString());
        return Boolean.FALSE;
      }
      return Boolean.FALSE;
    }
    catch (ClassCastException localClassCastException)
    {
      MoPubLog.d("Unable to initialize Moat, error obtaining application context.");
    }
  }
  
  public final Boolean invalidate()
  {
    boolean bool = a();
    if (!bool) {
      return null;
    }
    e = null;
    f = null;
    g.clear();
    return Boolean.TRUE;
  }
  
  public final Boolean onVideoPrepared(View paramView, int paramInt)
  {
    Preconditions.checkNotNull(paramView);
    boolean bool1 = a();
    if (!bool1) {
      return null;
    }
    Object localObject1 = f;
    if (localObject1 == null)
    {
      MoPubLog.d("Moat VideoAdTracker unexpectedly null.");
      return Boolean.FALSE;
    }
    boolean bool2 = h;
    if (bool2) {
      return Boolean.FALSE;
    }
    try
    {
      Object localObject2 = new com/mopub/common/util/Reflection$MethodBuilder;
      Object localObject3 = "trackVideoAd";
      ((Reflection.MethodBuilder)localObject2).<init>(localObject1, (String)localObject3);
      localObject1 = Map.class;
      localObject3 = g;
      localObject1 = ((Reflection.MethodBuilder)localObject2).addParam((Class)localObject1, localObject3);
      localObject2 = Integer.class;
      localObject4 = Integer.valueOf(paramInt);
      localObject4 = ((Reflection.MethodBuilder)localObject1).addParam((Class)localObject2, localObject4);
      localObject1 = View.class;
      paramView = ((Reflection.MethodBuilder)localObject4).addParam((Class)localObject1, paramView);
      paramView.execute();
      boolean bool3 = true;
      h = bool3;
      return Boolean.TRUE;
    }
    catch (Exception paramView)
    {
      Object localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>("Unable to execute Moat onVideoPrepared: ");
      paramView = paramView.getMessage();
      ((StringBuilder)localObject4).append(paramView);
      MoPubLog.d(((StringBuilder)localObject4).toString());
    }
    return Boolean.FALSE;
  }
  
  public final Boolean recordVideoEvent(ExternalViewabilitySession.VideoEvent paramVideoEvent, int paramInt)
  {
    Preconditions.checkNotNull(paramVideoEvent);
    boolean bool = a();
    Object localObject1 = null;
    if (!bool) {
      return null;
    }
    Object localObject2 = f;
    if (localObject2 == null)
    {
      MoPubLog.d("Moat VideoAdTracker unexpectedly null.");
      return Boolean.FALSE;
    }
    try
    {
      localObject2 = f.1.a;
      int j = paramVideoEvent.ordinal();
      int i = localObject2[j];
      switch (i)
      {
      default: 
        localObject3 = new java/lang/StringBuilder;
        break;
      case 10: 
      case 11: 
      case 12: 
      case 13: 
        return null;
      case 1: 
      case 2: 
      case 3: 
      case 4: 
      case 5: 
      case 6: 
      case 7: 
      case 8: 
      case 9: 
        localObject2 = paramVideoEvent.getMoatEnumName();
        if (localObject2 != null)
        {
          localObject2 = "com.moat.analytics.mobile.mpub.MoatAdEventType";
          localObject2 = Class.forName((String)localObject2);
          localObject1 = Enum.class;
          localObject1 = ((Class)localObject2).asSubclass((Class)localObject1);
          String str = paramVideoEvent.getMoatEnumName();
          localObject1 = Enum.valueOf((Class)localObject1, str);
          str = "com.moat.analytics.mobile.mpub.MoatAdEvent";
          Class localClass = Object.class;
          int k = 2;
          Class[] arrayOfClass = new Class[k];
          arrayOfClass[0] = localObject2;
          localObject2 = Integer.class;
          int m = 1;
          arrayOfClass[m] = localObject2;
          localObject2 = new Object[k];
          localObject2[0] = localObject1;
          localObject3 = Integer.valueOf(paramInt);
          localObject2[m] = localObject3;
          localObject3 = Reflection.instantiateClassWithConstructor(str, localClass, arrayOfClass, (Object[])localObject2);
          localObject2 = new com/mopub/common/util/Reflection$MethodBuilder;
          localObject1 = f;
          str = "dispatchEvent";
          ((Reflection.MethodBuilder)localObject2).<init>(localObject1, str);
          localObject1 = "com.moat.analytics.mobile.mpub.MoatAdEvent";
          localObject3 = ((Reflection.MethodBuilder)localObject2).addParam((String)localObject1, localObject3);
          ((Reflection.MethodBuilder)localObject3).execute();
        }
        return Boolean.TRUE;
      }
      localObject2 = "Unexpected video event: ";
      ((StringBuilder)localObject3).<init>((String)localObject2);
      localObject2 = paramVideoEvent.getMoatEnumName();
      ((StringBuilder)localObject3).append((String)localObject2);
      Object localObject3 = ((StringBuilder)localObject3).toString();
      MoPubLog.d((String)localObject3);
      return Boolean.FALSE;
    }
    catch (Exception localException)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Video event ");
      paramVideoEvent = paramVideoEvent.getMoatEnumName();
      ((StringBuilder)localObject2).append(paramVideoEvent);
      ((StringBuilder)localObject2).append(" failed. ");
      paramVideoEvent = localException.getMessage();
      ((StringBuilder)localObject2).append(paramVideoEvent);
      MoPubLog.d(((StringBuilder)localObject2).toString());
    }
    return Boolean.FALSE;
  }
  
  public final Boolean registerVideoObstruction(View paramView)
  {
    Preconditions.checkNotNull(paramView);
    boolean bool = a();
    if (!bool) {
      return null;
    }
    return Boolean.TRUE;
  }
  
  public final Boolean startDeferredDisplaySession(Activity paramActivity)
  {
    boolean bool = a();
    if (!bool) {
      return null;
    }
    paramActivity = e;
    if (paramActivity == null)
    {
      MoPubLog.d("MoatWebAdTracker unexpectedly null.");
      return Boolean.FALSE;
    }
    try
    {
      localObject = new com/mopub/common/util/Reflection$MethodBuilder;
      String str = "startTracking";
      ((Reflection.MethodBuilder)localObject).<init>(paramActivity, str);
      ((Reflection.MethodBuilder)localObject).execute();
      return Boolean.TRUE;
    }
    catch (Exception paramActivity)
    {
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>("Unable to record deferred display session for Moat: ");
      paramActivity = paramActivity.getMessage();
      ((StringBuilder)localObject).append(paramActivity);
      MoPubLog.d(((StringBuilder)localObject).toString());
    }
    return Boolean.FALSE;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */