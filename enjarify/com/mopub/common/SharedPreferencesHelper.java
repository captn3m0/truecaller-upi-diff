package com.mopub.common;

import android.content.Context;
import android.content.SharedPreferences;

public final class SharedPreferencesHelper
{
  public static final String DEFAULT_PREFERENCE_NAME = "mopubSettings";
  
  public static SharedPreferences getSharedPreferences(Context paramContext)
  {
    Preconditions.checkNotNull(paramContext);
    return paramContext.getSharedPreferences("mopubSettings", 0);
  }
  
  public static SharedPreferences getSharedPreferences(Context paramContext, String paramString)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramString);
    return paramContext.getSharedPreferences(paramString, 0);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.SharedPreferencesHelper
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */