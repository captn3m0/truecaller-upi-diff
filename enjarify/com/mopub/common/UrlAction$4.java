package com.mopub.common;

import android.content.Context;
import android.net.Uri;
import com.mopub.common.util.Intents;

 enum UrlAction$4
{
  UrlAction$4()
  {
    super(paramString, 2, true, (byte)0);
  }
  
  protected final void a(Context paramContext, Uri paramUri, UrlHandler paramUrlHandler, String paramString)
  {
    paramUrlHandler = new java/lang/StringBuilder;
    paramUrlHandler.<init>("Could not handle intent with URI: ");
    paramUrlHandler.append(paramUri);
    paramUrlHandler.append("\n\tIs this intent supported on your phone?");
    paramUrlHandler = paramUrlHandler.toString();
    Intents.launchActionViewIntent(paramContext, paramUri, paramUrlHandler);
  }
  
  public final boolean shouldTryHandlingUrl(Uri paramUri)
  {
    paramUri = paramUri.getScheme();
    String str = "tel";
    boolean bool1 = str.equalsIgnoreCase(paramUri);
    if (!bool1)
    {
      str = "voicemail";
      bool1 = str.equalsIgnoreCase(paramUri);
      if (!bool1)
      {
        str = "sms";
        bool1 = str.equalsIgnoreCase(paramUri);
        if (!bool1)
        {
          str = "mailto";
          bool1 = str.equalsIgnoreCase(paramUri);
          if (!bool1)
          {
            str = "geo";
            bool1 = str.equalsIgnoreCase(paramUri);
            if (!bool1)
            {
              str = "google.streetview";
              boolean bool2 = str.equalsIgnoreCase(paramUri);
              if (!bool2) {
                return false;
              }
            }
          }
        }
      }
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.UrlAction.4
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */