package com.mopub.common;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.Drawables;
import java.util.EnumSet;

final class c
  extends WebViewClient
{
  private static final EnumSet a;
  private MoPubBrowser b;
  
  static
  {
    UrlAction localUrlAction1 = UrlAction.HANDLE_PHONE_SCHEME;
    UrlAction[] arrayOfUrlAction = new UrlAction[5];
    UrlAction localUrlAction2 = UrlAction.OPEN_APP_MARKET;
    arrayOfUrlAction[0] = localUrlAction2;
    localUrlAction2 = UrlAction.OPEN_IN_APP_BROWSER;
    arrayOfUrlAction[1] = localUrlAction2;
    localUrlAction2 = UrlAction.HANDLE_SHARE_TWEET;
    arrayOfUrlAction[2] = localUrlAction2;
    localUrlAction2 = UrlAction.FOLLOW_DEEP_LINK_WITH_FALLBACK;
    arrayOfUrlAction[3] = localUrlAction2;
    localUrlAction2 = UrlAction.FOLLOW_DEEP_LINK;
    arrayOfUrlAction[4] = localUrlAction2;
    a = EnumSet.of(localUrlAction1, arrayOfUrlAction);
  }
  
  public c(MoPubBrowser paramMoPubBrowser)
  {
    b = paramMoPubBrowser;
  }
  
  public final void onPageFinished(WebView paramWebView, String paramString)
  {
    super.onPageFinished(paramWebView, paramString);
    boolean bool1 = paramWebView.canGoBack();
    if (bool1)
    {
      paramString = Drawables.LEFT_ARROW;
      localObject = b;
      paramString = paramString.createDrawable((Context)localObject);
    }
    else
    {
      paramString = Drawables.UNLEFT_ARROW;
      localObject = b;
      paramString = paramString.createDrawable((Context)localObject);
    }
    Object localObject = b.getBackButton();
    ((ImageButton)localObject).setImageDrawable(paramString);
    boolean bool2 = paramWebView.canGoForward();
    if (bool2)
    {
      paramWebView = Drawables.RIGHT_ARROW;
      paramString = b;
      paramWebView = paramWebView.createDrawable(paramString);
    }
    else
    {
      paramWebView = Drawables.UNRIGHT_ARROW;
      paramString = b;
      paramWebView = paramWebView.createDrawable(paramString);
    }
    b.getForwardButton().setImageDrawable(paramWebView);
  }
  
  public final void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap)
  {
    super.onPageStarted(paramWebView, paramString, paramBitmap);
  }
  
  public final void onReceivedError(WebView paramWebView, int paramInt, String paramString1, String paramString2)
  {
    String str = String.valueOf(paramString1);
    MoPubLog.d("MoPubBrowser error: ".concat(str));
  }
  
  public final boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool) {
      return false;
    }
    paramWebView = new com/mopub/common/UrlHandler$Builder;
    paramWebView.<init>();
    Object localObject = a;
    paramWebView = paramWebView.withSupportedUrlActions((EnumSet)localObject).withoutMoPubBrowser();
    localObject = new com/mopub/common/c$1;
    ((c.1)localObject).<init>(this);
    paramWebView = paramWebView.withResultActions((UrlHandler.ResultActions)localObject).build();
    localObject = b.getApplicationContext();
    return paramWebView.handleResolvedUrl((Context)localObject, paramString, true, null);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */