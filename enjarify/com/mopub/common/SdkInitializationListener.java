package com.mopub.common;

public abstract interface SdkInitializationListener
{
  public abstract void onInitializationFinished();
}

/* Location:
 * Qualified Name:     com.mopub.common.SdkInitializationListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */