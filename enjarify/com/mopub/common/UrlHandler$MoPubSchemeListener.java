package com.mopub.common;

public abstract interface UrlHandler$MoPubSchemeListener
{
  public abstract void onClose();
  
  public abstract void onFailLoad();
  
  public abstract void onFinishLoad();
}

/* Location:
 * Qualified Name:     com.mopub.common.UrlHandler.MoPubSchemeListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */