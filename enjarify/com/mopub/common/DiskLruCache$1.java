package com.mopub.common;

import java.util.concurrent.Callable;

final class DiskLruCache$1
  implements Callable
{
  DiskLruCache$1(DiskLruCache paramDiskLruCache) {}
  
  public final Void call()
  {
    synchronized (a)
    {
      Object localObject1 = a;
      localObject1 = DiskLruCache.a((DiskLruCache)localObject1);
      if (localObject1 == null) {
        return null;
      }
      localObject1 = a;
      DiskLruCache.b((DiskLruCache)localObject1);
      localObject1 = a;
      boolean bool = DiskLruCache.c((DiskLruCache)localObject1);
      if (bool)
      {
        localObject1 = a;
        DiskLruCache.d((DiskLruCache)localObject1);
        localObject1 = a;
        DiskLruCache.e((DiskLruCache)localObject1);
      }
      return null;
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.DiskLruCache.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */