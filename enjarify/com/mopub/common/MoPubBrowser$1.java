package com.mopub.common;

import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;

final class MoPubBrowser$1
  implements View.OnClickListener
{
  MoPubBrowser$1(MoPubBrowser paramMoPubBrowser) {}
  
  public final void onClick(View paramView)
  {
    paramView = MoPubBrowser.a(a);
    boolean bool = paramView.canGoBack();
    if (bool)
    {
      paramView = MoPubBrowser.a(a);
      paramView.goBack();
    }
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.MoPubBrowser.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */