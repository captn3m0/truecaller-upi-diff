package com.mopub.common;

public enum CloseableLayout$ClosePosition
{
  final int a;
  
  static
  {
    Object localObject = new com/mopub/common/CloseableLayout$ClosePosition;
    ((ClosePosition)localObject).<init>("TOP_LEFT", 0, 51);
    TOP_LEFT = (ClosePosition)localObject;
    localObject = new com/mopub/common/CloseableLayout$ClosePosition;
    int i = 1;
    ((ClosePosition)localObject).<init>("TOP_CENTER", i, 49);
    TOP_CENTER = (ClosePosition)localObject;
    localObject = new com/mopub/common/CloseableLayout$ClosePosition;
    int j = 2;
    ((ClosePosition)localObject).<init>("TOP_RIGHT", j, 53);
    TOP_RIGHT = (ClosePosition)localObject;
    localObject = new com/mopub/common/CloseableLayout$ClosePosition;
    int k = 3;
    ((ClosePosition)localObject).<init>("CENTER", k, 17);
    CENTER = (ClosePosition)localObject;
    localObject = new com/mopub/common/CloseableLayout$ClosePosition;
    int m = 4;
    ((ClosePosition)localObject).<init>("BOTTOM_LEFT", m, 83);
    BOTTOM_LEFT = (ClosePosition)localObject;
    localObject = new com/mopub/common/CloseableLayout$ClosePosition;
    int n = 5;
    ((ClosePosition)localObject).<init>("BOTTOM_CENTER", n, 81);
    BOTTOM_CENTER = (ClosePosition)localObject;
    localObject = new com/mopub/common/CloseableLayout$ClosePosition;
    int i1 = 6;
    ((ClosePosition)localObject).<init>("BOTTOM_RIGHT", i1, 85);
    BOTTOM_RIGHT = (ClosePosition)localObject;
    localObject = new ClosePosition[7];
    ClosePosition localClosePosition = TOP_LEFT;
    localObject[0] = localClosePosition;
    localClosePosition = TOP_CENTER;
    localObject[i] = localClosePosition;
    localClosePosition = TOP_RIGHT;
    localObject[j] = localClosePosition;
    localClosePosition = CENTER;
    localObject[k] = localClosePosition;
    localClosePosition = BOTTOM_LEFT;
    localObject[m] = localClosePosition;
    localClosePosition = BOTTOM_CENTER;
    localObject[n] = localClosePosition;
    localClosePosition = BOTTOM_RIGHT;
    localObject[i1] = localClosePosition;
    b = (ClosePosition[])localObject;
  }
  
  private CloseableLayout$ClosePosition(int paramInt1)
  {
    a = paramInt1;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.CloseableLayout.ClosePosition
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */