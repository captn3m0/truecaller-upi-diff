package com.mopub.common;

import com.mopub.common.logging.MoPubLog;

public enum ExternalViewabilitySessionManager$ViewabilityVendor
{
  static
  {
    Object localObject = new com/mopub/common/ExternalViewabilitySessionManager$ViewabilityVendor;
    ((ViewabilityVendor)localObject).<init>("AVID", 0);
    AVID = (ViewabilityVendor)localObject;
    localObject = new com/mopub/common/ExternalViewabilitySessionManager$ViewabilityVendor;
    int i = 1;
    ((ViewabilityVendor)localObject).<init>("MOAT", i);
    MOAT = (ViewabilityVendor)localObject;
    localObject = new com/mopub/common/ExternalViewabilitySessionManager$ViewabilityVendor;
    int j = 2;
    ((ViewabilityVendor)localObject).<init>("ALL", j);
    ALL = (ViewabilityVendor)localObject;
    localObject = new ViewabilityVendor[3];
    ViewabilityVendor localViewabilityVendor = AVID;
    localObject[0] = localViewabilityVendor;
    localViewabilityVendor = MOAT;
    localObject[i] = localViewabilityVendor;
    localViewabilityVendor = ALL;
    localObject[j] = localViewabilityVendor;
    a = (ViewabilityVendor[])localObject;
  }
  
  public static ViewabilityVendor fromKey(String paramString)
  {
    Preconditions.checkNotNull(paramString);
    int i = paramString.hashCode();
    String str;
    boolean bool2;
    switch (i)
    {
    default: 
      break;
    case 51: 
      str = "3";
      boolean bool1 = paramString.equals(str);
      if (bool1) {
        int j = 2;
      }
      break;
    case 50: 
      str = "2";
      bool2 = paramString.equals(str);
      if (bool2) {
        bool2 = true;
      }
      break;
    case 49: 
      str = "1";
      bool2 = paramString.equals(str);
      if (bool2)
      {
        bool2 = false;
        paramString = null;
      }
      break;
    }
    int k = -1;
    switch (k)
    {
    default: 
      return null;
    case 2: 
      return ALL;
    case 1: 
      return MOAT;
    }
    return AVID;
  }
  
  public static String getEnabledVendorKey()
  {
    boolean bool1 = b.a();
    boolean bool2 = f.a();
    String str = "0";
    if ((bool1) && (bool2)) {
      str = "3";
    } else if (bool1) {
      str = "1";
    } else if (bool2) {
      str = "2";
    }
    return str;
  }
  
  public final void disable()
  {
    int[] arrayOfInt = ExternalViewabilitySessionManager.1.a;
    int i = ordinal();
    int j = arrayOfInt[i];
    switch (j)
    {
    default: 
      str = String.valueOf(this);
      MoPubLog.d("Attempted to disable an invalid viewability vendor: ".concat(str));
      return;
    case 3: 
      b.b();
      f.b();
      break;
    case 2: 
      f.b();
      break;
    case 1: 
      b.b();
    }
    String str = String.valueOf(this);
    MoPubLog.d("Disabled viewability for ".concat(str));
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.ExternalViewabilitySessionManager.ViewabilityVendor
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */