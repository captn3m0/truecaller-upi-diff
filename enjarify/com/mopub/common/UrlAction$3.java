package com.mopub.common;

import android.content.Context;
import android.net.Uri;
import com.mopub.common.logging.MoPubLog;

 enum UrlAction$3
{
  UrlAction$3()
  {
    super(paramString, 1, false, (byte)0);
  }
  
  protected final void a(Context paramContext, Uri paramUri, UrlHandler paramUrlHandler, String paramString)
  {
    MoPubLog.d("Link to about page ignored.");
  }
  
  public final boolean shouldTryHandlingUrl(Uri paramUri)
  {
    paramUri = paramUri.getScheme();
    return "about".equalsIgnoreCase(paramUri);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.UrlAction.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */