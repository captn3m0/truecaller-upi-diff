package com.mopub.common;

import com.google.firebase.perf.network.FirebasePerfUrlConnection;
import com.mopub.common.logging.MoPubLog;
import com.mopub.network.Networking;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;

public abstract class MoPubHttpUrlConnection
  extends HttpURLConnection
{
  private static boolean a(String paramString)
  {
    String str = "UTF-8";
    try
    {
      URLDecoder.decode(paramString, str);
      return false;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      paramString = String.valueOf(paramString);
      MoPubLog.w("Url is improperly encoded: ".concat(paramString));
    }
    return true;
  }
  
  private static boolean b(String paramString)
  {
    try
    {
      URI localURI = new java/net/URI;
      localURI.<init>(paramString);
      return false;
    }
    catch (URISyntaxException localURISyntaxException) {}
    return true;
  }
  
  private static URI c(String paramString)
  {
    try
    {
      URL localURL = new java/net/URL;
      localURL.<init>(paramString);
      URI localURI = new java/net/URI;
      String str1 = localURL.getProtocol();
      String str2 = localURL.getUserInfo();
      String str3 = localURL.getHost();
      int i = localURL.getPort();
      String str4 = localURL.getPath();
      String str5 = localURL.getQuery();
      String str6 = localURL.getRef();
      localURI.<init>(str1, str2, str3, i, str4, str5, str6);
      return localURI;
    }
    catch (Exception localException)
    {
      paramString = String.valueOf(paramString);
      MoPubLog.w("Failed to encode url: ".concat(paramString));
      throw localException;
    }
  }
  
  public static HttpURLConnection getHttpUrlConnection(String paramString)
  {
    Preconditions.checkNotNull(paramString);
    boolean bool = a(paramString);
    if (!bool) {}
    try
    {
      paramString = urlEncode(paramString);
    }
    catch (Exception localException)
    {
      Object localObject;
      String str;
      int i;
      for (;;) {}
    }
    localObject = new java/net/URL;
    ((URL)localObject).<init>(paramString);
    paramString = (HttpURLConnection)FirebasePerfUrlConnection.instrument(((URL)localObject).openConnection());
    str = Networking.getCachedUserAgent();
    paramString.setRequestProperty("user-agent", str);
    i = 10000;
    paramString.setConnectTimeout(i);
    paramString.setReadTimeout(i);
    return paramString;
    localObject = new java/lang/IllegalArgumentException;
    paramString = String.valueOf(paramString);
    paramString = "URL is improperly encoded: ".concat(paramString);
    ((IllegalArgumentException)localObject).<init>(paramString);
    throw ((Throwable)localObject);
  }
  
  public static String urlEncode(String paramString)
  {
    Preconditions.checkNotNull(paramString);
    boolean bool = a(paramString);
    if (!bool)
    {
      bool = b(paramString);
      if (bool)
      {
        paramString = c(paramString);
      }
      else
      {
        localObject = new java/net/URI;
        ((URI)localObject).<init>(paramString);
        paramString = (String)localObject;
      }
      return paramString.toURL().toString();
    }
    Object localObject = new java/io/UnsupportedEncodingException;
    paramString = String.valueOf(paramString);
    paramString = "URL is improperly encoded: ".concat(paramString);
    ((UnsupportedEncodingException)localObject).<init>(paramString);
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.MoPubHttpUrlConnection
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */