package com.mopub.common;

import com.mopub.common.util.MoPubCollections;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SdkConfiguration$Builder
{
  private String a;
  private final List b;
  private MediationSettings[] c;
  private List d;
  
  public SdkConfiguration$Builder(String paramString)
  {
    a = paramString;
    paramString = new java/util/ArrayList;
    paramString.<init>();
    b = paramString;
    paramString = new MediationSettings[0];
    c = paramString;
  }
  
  public SdkConfiguration build()
  {
    SdkConfiguration localSdkConfiguration = new com/mopub/common/SdkConfiguration;
    String str = a;
    List localList1 = b;
    MediationSettings[] arrayOfMediationSettings = c;
    List localList2 = d;
    localSdkConfiguration.<init>(str, localList1, arrayOfMediationSettings, localList2, (byte)0);
    return localSdkConfiguration;
  }
  
  public Builder withAdvancedBidder(Class paramClass)
  {
    Preconditions.checkNotNull(paramClass);
    b.add(paramClass);
    return this;
  }
  
  public Builder withAdvancedBidders(Collection paramCollection)
  {
    Preconditions.NoThrow.checkNotNull(paramCollection);
    MoPubCollections.addAllNonNull(b, paramCollection);
    return this;
  }
  
  public Builder withMediationSettings(MediationSettings... paramVarArgs)
  {
    Preconditions.checkNotNull(paramVarArgs);
    c = paramVarArgs;
    return this;
  }
  
  public Builder withNetworksToInit(List paramList)
  {
    if (paramList == null) {
      return this;
    }
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    d = localArrayList;
    MoPubCollections.addAllNonNull(d, paramList);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.SdkConfiguration.Builder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */