package com.mopub.common;

public enum CreativeOrientation
{
  static
  {
    Object localObject = new com/mopub/common/CreativeOrientation;
    ((CreativeOrientation)localObject).<init>("PORTRAIT", 0);
    PORTRAIT = (CreativeOrientation)localObject;
    localObject = new com/mopub/common/CreativeOrientation;
    int i = 1;
    ((CreativeOrientation)localObject).<init>("LANDSCAPE", i);
    LANDSCAPE = (CreativeOrientation)localObject;
    localObject = new com/mopub/common/CreativeOrientation;
    int j = 2;
    ((CreativeOrientation)localObject).<init>("UNDEFINED", j);
    UNDEFINED = (CreativeOrientation)localObject;
    localObject = new CreativeOrientation[3];
    CreativeOrientation localCreativeOrientation = PORTRAIT;
    localObject[0] = localCreativeOrientation;
    localCreativeOrientation = LANDSCAPE;
    localObject[i] = localCreativeOrientation;
    localCreativeOrientation = UNDEFINED;
    localObject[j] = localCreativeOrientation;
    a = (CreativeOrientation[])localObject;
  }
  
  public static CreativeOrientation fromHeader(String paramString)
  {
    String str = "l";
    boolean bool1 = str.equalsIgnoreCase(paramString);
    if (bool1) {
      return LANDSCAPE;
    }
    str = "p";
    boolean bool2 = str.equalsIgnoreCase(paramString);
    if (bool2) {
      return PORTRAIT;
    }
    return UNDEFINED;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.CreativeOrientation
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */