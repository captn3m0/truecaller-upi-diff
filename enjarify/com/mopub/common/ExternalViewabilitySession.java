package com.mopub.common;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.webkit.WebView;
import java.util.Map;
import java.util.Set;

public abstract interface ExternalViewabilitySession
{
  public abstract Boolean createDisplaySession(Context paramContext, WebView paramWebView, boolean paramBoolean);
  
  public abstract Boolean createVideoSession(Activity paramActivity, View paramView, Set paramSet, Map paramMap);
  
  public abstract Boolean endDisplaySession();
  
  public abstract Boolean endVideoSession();
  
  public abstract String getName();
  
  public abstract Boolean initialize(Context paramContext);
  
  public abstract Boolean invalidate();
  
  public abstract Boolean onVideoPrepared(View paramView, int paramInt);
  
  public abstract Boolean recordVideoEvent(ExternalViewabilitySession.VideoEvent paramVideoEvent, int paramInt);
  
  public abstract Boolean registerVideoObstruction(View paramView);
  
  public abstract Boolean startDeferredDisplaySession(Activity paramActivity);
}

/* Location:
 * Qualified Name:     com.mopub.common.ExternalViewabilitySession
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */