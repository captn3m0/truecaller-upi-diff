package com.mopub.common;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.mopub.common.util.Intents;
import com.mopub.exceptions.IntentNotResolvableException;
import com.mopub.exceptions.UrlParseException;

 enum UrlAction$8
{
  UrlAction$8()
  {
    super(paramString, 6, true, (byte)0);
  }
  
  protected final void a(Context paramContext, Uri paramUri, UrlHandler paramUrlHandler, String paramString)
  {
    Preconditions.checkNotNull(paramContext);
    Preconditions.checkNotNull(paramUri);
    paramString = String.valueOf(paramUri);
    paramUrlHandler = "Could not handle share tweet intent with URI ".concat(paramString);
    try
    {
      paramUri = Intents.intentForShareTweet(paramUri);
      paramString = "Share via";
      paramUri = Intent.createChooser(paramUri, paramString);
      Intents.launchIntentForUserClick(paramContext, paramUri, paramUrlHandler);
      return;
    }
    catch (UrlParseException paramContext)
    {
      paramUri = new com/mopub/exceptions/IntentNotResolvableException;
      paramString = new java/lang/StringBuilder;
      paramString.<init>();
      paramString.append(paramUrlHandler);
      paramString.append("\n\t");
      paramContext = paramContext.getMessage();
      paramString.append(paramContext);
      paramContext = paramString.toString();
      paramUri.<init>(paramContext);
      throw paramUri;
    }
  }
  
  public final boolean shouldTryHandlingUrl(Uri paramUri)
  {
    Preconditions.checkNotNull(paramUri);
    String str1 = "mopubshare";
    String str2 = paramUri.getScheme();
    boolean bool1 = str1.equalsIgnoreCase(str2);
    if (bool1)
    {
      str1 = "tweet";
      paramUri = paramUri.getHost();
      boolean bool2 = str1.equalsIgnoreCase(paramUri);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.UrlAction.8
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */