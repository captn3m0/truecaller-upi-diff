package com.mopub.common;

import android.webkit.WebView;

final class c$1
  implements UrlHandler.ResultActions
{
  c$1(c paramc) {}
  
  public final void urlHandlingFailed(String paramString, UrlAction paramUrlAction) {}
  
  public final void urlHandlingSucceeded(String paramString, UrlAction paramUrlAction)
  {
    UrlAction localUrlAction = UrlAction.OPEN_IN_APP_BROWSER;
    boolean bool = paramUrlAction.equals(localUrlAction);
    if (bool)
    {
      c.a(a).getWebView().loadUrl(paramString);
      return;
    }
    c.a(a).finish();
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.c.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */