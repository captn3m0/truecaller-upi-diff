package com.mopub.common.privacy;

import com.mopub.volley.Response.ErrorListener;

public abstract interface ConsentDialogRequest$Listener
  extends Response.ErrorListener
{
  public abstract void onSuccess(b paramb);
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.ConsentDialogRequest.Listener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */