package com.mopub.common.privacy;

public abstract interface ConsentStatusChangeListener
{
  public abstract void onConsentStateChange(ConsentStatus paramConsentStatus1, ConsentStatus paramConsentStatus2, boolean paramBoolean);
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.ConsentStatusChangeListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */