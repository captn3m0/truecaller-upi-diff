package com.mopub.common.privacy;

import android.content.Context;
import com.mopub.common.ClientMetadata;
import com.mopub.common.SdkInitializationListener;
import com.mopub.common.logging.MoPubLog;
import com.mopub.mobileads.MoPubConversionTracker;

final class PersonalInfoManager$5
  implements SdkInitializationListener
{
  PersonalInfoManager$5(PersonalInfoManager paramPersonalInfoManager) {}
  
  public final void onInitializationFinished()
  {
    MoPubLog.d("MoPubIdentifier initialized.");
    Object localObject1 = ClientMetadata.getInstance(PersonalInfoManager.b(a)).getMoPubIdentifier().getAdvertisingInfo();
    boolean bool1 = PersonalInfoManager.c(a);
    Boolean localBoolean = a.gdprApplies();
    Long localLong = PersonalInfoManager.d(a);
    long l = PersonalInfoManager.e(a);
    Object localObject2 = PersonalInfoManager.a(a);
    String str = f;
    boolean bool2 = ((AdvertisingId)localObject1).isDoNotTrack();
    boolean bool3 = PersonalInfoManager.a(bool1, localBoolean, false, localLong, l, str, bool2);
    if (!bool3)
    {
      localObject1 = PersonalInfoManager.f(a);
      if (localObject1 != null)
      {
        PersonalInfoManager.f(a).onInitializationFinished();
        localObject1 = a;
        PersonalInfoManager.g((PersonalInfoManager)localObject1);
      }
    }
    else
    {
      localObject1 = a;
      ((PersonalInfoManager)localObject1).a();
    }
    localObject1 = new com/mopub/mobileads/MoPubConversionTracker;
    localObject2 = PersonalInfoManager.b(a);
    ((MoPubConversionTracker)localObject1).<init>((Context)localObject2);
    ((MoPubConversionTracker)localObject1).reportAppOpen(true);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.PersonalInfoManager.5
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */