package com.mopub.common.privacy;

import com.mopub.mobileads.MoPubErrorCode;

final class PersonalInfoManager$2
  implements Runnable
{
  PersonalInfoManager$2(PersonalInfoManager paramPersonalInfoManager, ConsentDialogListener paramConsentDialogListener) {}
  
  public final void run()
  {
    ConsentDialogListener localConsentDialogListener = a;
    MoPubErrorCode localMoPubErrorCode = MoPubErrorCode.DO_NOT_TRACK;
    localConsentDialogListener.onConsentDialogLoadFailed(localMoPubErrorCode);
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.PersonalInfoManager.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */