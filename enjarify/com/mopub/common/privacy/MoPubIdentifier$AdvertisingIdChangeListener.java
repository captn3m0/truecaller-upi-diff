package com.mopub.common.privacy;

public abstract interface MoPubIdentifier$AdvertisingIdChangeListener
{
  public abstract void onIdChanged(AdvertisingId paramAdvertisingId1, AdvertisingId paramAdvertisingId2);
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.MoPubIdentifier.AdvertisingIdChangeListener
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */