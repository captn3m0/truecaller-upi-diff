package com.mopub.common.privacy;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;
import com.mopub.common.Preconditions;
import com.mopub.common.SdkInitializationListener;
import com.mopub.common.SharedPreferencesHelper;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.util.AsyncTasks;
import java.util.Calendar;

public class MoPubIdentifier
{
  AdvertisingId a;
  final Context b;
  boolean c;
  volatile SdkInitializationListener d;
  private MoPubIdentifier.AdvertisingIdChangeListener e;
  private boolean f;
  
  public MoPubIdentifier(Context paramContext)
  {
    this(paramContext, (byte)0);
  }
  
  private MoPubIdentifier(Context paramContext, byte paramByte)
  {
    Preconditions.checkNotNull(paramContext);
    b = paramContext;
    e = null;
    paramContext = a(b);
    a = paramContext;
    paramContext = a;
    if (paramContext == null)
    {
      paramContext = AdvertisingId.b();
      a = paramContext;
    }
    b();
  }
  
  private static AdvertisingId a(Context paramContext)
  {
    synchronized (MoPubIdentifier.class)
    {
      Preconditions.checkNotNull(paramContext);
      Object localObject = Calendar.getInstance();
      String str1 = "com.mopub.settings.identifier";
      try
      {
        paramContext = SharedPreferencesHelper.getSharedPreferences(paramContext, str1);
        str1 = "privacy.identifier.ifa";
        String str2 = "";
        String str3 = paramContext.getString(str1, str2);
        str1 = "privacy.identifier.mopub";
        str2 = "";
        String str4 = paramContext.getString(str1, str2);
        str1 = "privacy.identifier.time";
        long l1 = ((Calendar)localObject).getTimeInMillis();
        long l2 = paramContext.getLong(str1, l1);
        localObject = "privacy.limit.ad.tracking";
        str1 = null;
        boolean bool1 = paramContext.getBoolean((String)localObject, false);
        boolean bool2 = TextUtils.isEmpty(str3);
        if (!bool2)
        {
          bool2 = TextUtils.isEmpty(str4);
          if (!bool2)
          {
            paramContext = new com/mopub/common/privacy/AdvertisingId;
            paramContext.<init>(str3, str4, bool1, l2);
            return paramContext;
          }
        }
      }
      catch (ClassCastException localClassCastException)
      {
        paramContext = "Cannot read identifier from shared preferences";
        MoPubLog.e(paramContext);
        return null;
      }
    }
  }
  
  private static void a(Context paramContext, AdvertisingId paramAdvertisingId)
  {
    synchronized (MoPubIdentifier.class)
    {
      Preconditions.checkNotNull(paramContext);
      Preconditions.checkNotNull(paramAdvertisingId);
      String str1 = "com.mopub.settings.identifier";
      paramContext = SharedPreferencesHelper.getSharedPreferences(paramContext, str1);
      paramContext = paramContext.edit();
      str1 = "privacy.limit.ad.tracking";
      boolean bool = d;
      paramContext.putBoolean(str1, bool);
      str1 = "privacy.identifier.ifa";
      String str2 = b;
      paramContext.putString(str1, str2);
      str1 = "privacy.identifier.mopub";
      str2 = c;
      paramContext.putString(str1, str2);
      str1 = "privacy.identifier.time";
      paramAdvertisingId = a;
      long l = paramAdvertisingId.getTimeInMillis();
      paramContext.putLong(str1, l);
      paramContext.apply();
      return;
    }
  }
  
  private void b()
  {
    boolean bool = f;
    if (bool) {
      return;
    }
    f = true;
    MoPubIdentifier.a locala = new com/mopub/common/privacy/MoPubIdentifier$a;
    locala.<init>(this, (byte)0);
    Void[] arrayOfVoid = new Void[0];
    AsyncTasks.safeExecuteOnExecutor(locala, arrayOfVoid);
  }
  
  final void a()
  {
    try
    {
      SdkInitializationListener localSdkInitializationListener = d;
      if (localSdkInitializationListener != null)
      {
        d = null;
        localSdkInitializationListener.onInitializationFinished();
      }
      return;
    }
    finally {}
  }
  
  final void a(AdvertisingId paramAdvertisingId)
  {
    AdvertisingId localAdvertisingId = a;
    a = paramAdvertisingId;
    paramAdvertisingId = b;
    Object localObject = a;
    a(paramAdvertisingId, (AdvertisingId)localObject);
    paramAdvertisingId = a;
    boolean bool = paramAdvertisingId.equals(localAdvertisingId);
    if (bool)
    {
      bool = c;
      if (bool) {}
    }
    else
    {
      paramAdvertisingId = a;
      Preconditions.checkNotNull(paramAdvertisingId);
      localObject = e;
      if (localObject != null) {
        ((MoPubIdentifier.AdvertisingIdChangeListener)localObject).onIdChanged(localAdvertisingId, paramAdvertisingId);
      }
    }
    c = true;
    a();
  }
  
  public AdvertisingId getAdvertisingInfo()
  {
    AdvertisingId localAdvertisingId = a;
    b();
    return localAdvertisingId;
  }
  
  public void setIdChangeListener(MoPubIdentifier.AdvertisingIdChangeListener paramAdvertisingIdChangeListener)
  {
    e = paramAdvertisingIdChangeListener;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.MoPubIdentifier
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */