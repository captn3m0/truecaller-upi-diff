package com.mopub.common.privacy;

public class SyncResponse$Builder
{
  private String a;
  private String b;
  private String c;
  private String d;
  private String e;
  private String f;
  private String g;
  private String h;
  private String i;
  private String j;
  private String k;
  private String l;
  private String m;
  private String n;
  private String o;
  
  public SyncResponse build()
  {
    SyncResponse localSyncResponse = new com/mopub/common/privacy/SyncResponse;
    String str1 = a;
    String str2 = b;
    String str3 = c;
    String str4 = d;
    String str5 = e;
    String str6 = f;
    String str7 = g;
    String str8 = h;
    String str9 = i;
    String str10 = j;
    String str11 = k;
    String str12 = l;
    String str13 = m;
    String str14 = n;
    String str15 = o;
    localSyncResponse.<init>(str1, str2, str3, str4, str5, str6, str7, str8, str9, str10, str11, str12, str13, str14, str15, (byte)0);
    return localSyncResponse;
  }
  
  public Builder setCallAgainAfterSecs(String paramString)
  {
    m = paramString;
    return this;
  }
  
  public Builder setConsentChangeReason(String paramString)
  {
    o = paramString;
    return this;
  }
  
  public Builder setCurrentPrivacyPolicyLink(String paramString)
  {
    j = paramString;
    return this;
  }
  
  public Builder setCurrentPrivacyPolicyVersion(String paramString)
  {
    i = paramString;
    return this;
  }
  
  public Builder setCurrentVendorListIabFormat(String paramString)
  {
    k = paramString;
    return this;
  }
  
  public Builder setCurrentVendorListIabHash(String paramString)
  {
    l = paramString;
    return this;
  }
  
  public Builder setCurrentVendorListLink(String paramString)
  {
    h = paramString;
    return this;
  }
  
  public Builder setCurrentVendorListVersion(String paramString)
  {
    g = paramString;
    return this;
  }
  
  public Builder setExtras(String paramString)
  {
    n = paramString;
    return this;
  }
  
  public Builder setForceExplicitNo(String paramString)
  {
    b = paramString;
    return this;
  }
  
  public Builder setForceGdprApplies(String paramString)
  {
    f = paramString;
    return this;
  }
  
  public Builder setInvalidateConsent(String paramString)
  {
    c = paramString;
    return this;
  }
  
  public Builder setIsGdprRegion(String paramString)
  {
    a = paramString;
    return this;
  }
  
  public Builder setIsWhitelisted(String paramString)
  {
    e = paramString;
    return this;
  }
  
  public Builder setReacquireConsent(String paramString)
  {
    d = paramString;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.mopub.common.privacy.SyncResponse.Builder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */